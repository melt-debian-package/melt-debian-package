/** GENERATED MELT TIMESTAMP FILE meltbuild-sources/warmelt-macro+melttime.h 
** NEVER EDIT OR MOVE THIS, IT IS GENERATED & PARSED! **/
/* These identifiers are generated in warmelt-outobj.melt 
 & handled in melt-runtime.c carefully. */


/* This warmelt-macro+melttime.h is included from warmelt-macro+meltdesc.c only. */
#if meltmod_warmelt_macro_mds__c1444a96ec0081bca6c9674216b679d1
/* MELT generation timestamp for meltbuild-sources/warmelt-macro */

#ifdef __cplusplus
/* these symbols are extern "C" since dlsym-ed */
extern "C" const char melt_gen_timestamp[] ;
extern "C" const long long melt_gen_timenum ;
extern "C" const char melt_build_timestamp[] ;
extern "C" {
#endif /*__cplusplus */

		 
/*MELT BOOTSTRAP*/
const char melt_gen_timestamp[]="Wed Oct 10 13:49:40 2012 CEST";
const long long melt_gen_timenum=1349869780;

		 const char melt_build_timestamp[]= __DATE__ "@" __TIME__
		 #ifdef __cplusplus
		 " (in C++)"
		 #else
		 " (in C)"
		 #endif /*__cplusplus*/
					;
		 

		 #ifdef __cplusplus
		 }  /* end extern C timestamp */
		 #endif /*__cplusplus */

		 #else /* ! meltmod_warmelt_macro_mds__c1444a96ec0081bca6c9674216b679d1 */
		 #error invalid timestamp file for meltbuild-sources/warmelt-macro 
		 #endif /* meltmod_warmelt_macro_mds__c1444a96ec0081bca6c9674216b679d1 */
		 
