/* GCC MELT GENERATED FILE warmelt-first+02.c - DO NOT EDIT */
/* secondary MELT generated C file of rank #2 */
#include "melt-run.h"


/* used hash from melt-run.h when compiling this file: */
MELT_EXTERN const char meltrun_used_md5_melt_f2[] =
  MELT_RUN_HASHMD5 /* from melt-run.h */ ;


/**** warmelt-first+02.c declarations ****/


/***************************************************
***
    Copyright 2008, 2009, 2010, 2011, 2012 Free Software Foundation, Inc.
    Contributed by Basile Starynkevitch <basile@starynkevitch.net>

    This file is part of GCC.

    GCC is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3, or (at your option)
    any later version.

    GCC is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with GCC; see the file COPYING3.  If not see
    <http://www.gnu.org/licenses/>.
***
****************************************************/

/* initial MELT module */
#define MELT_HAS_INITIAL_ENVIRONMENT 0	/*initial */

struct melt_callframe_st;	/*defined in melt-runtime.h */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_1_warmelt_first_IS_EMPTY_STRING (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_2_warmelt_first_IS_NON_EMPTY_STRING (meltclosure_ptr_t meltclosp_,
					      melt_ptr_t meltfirstargp_,
					      const melt_argdescr_cell_t
					      meltxargdescr_[],
					      union meltparam_un
					      *meltxargtab_,
					      const melt_argdescr_cell_t
					      meltxresdescr_[],
					      union meltparam_un
					      *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_3_warmelt_first_INSTALL_CTYPE_DESCR (meltclosure_ptr_t meltclosp_,
					      melt_ptr_t meltfirstargp_,
					      const melt_argdescr_cell_t
					      meltxargdescr_[],
					      union meltparam_un
					      *meltxargtab_,
					      const melt_argdescr_cell_t
					      meltxresdescr_[],
					      union meltparam_un
					      *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_4_warmelt_first_ADD_NEW_SYMBOL_TOKEN (meltclosure_ptr_t meltclosp_,
					       melt_ptr_t meltfirstargp_,
					       const melt_argdescr_cell_t
					       meltxargdescr_[],
					       union meltparam_un
					       *meltxargtab_,
					       const melt_argdescr_cell_t
					       meltxresdescr_[],
					       union meltparam_un
					       *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_5_warmelt_first_ADD_NEW_KEYWORD_TOKEN (meltclosure_ptr_t meltclosp_,
						melt_ptr_t meltfirstargp_,
						const melt_argdescr_cell_t
						meltxargdescr_[],
						union meltparam_un
						*meltxargtab_,
						const melt_argdescr_cell_t
						meltxresdescr_[],
						union meltparam_un
						*meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_6_warmelt_first_INTERN_SYMBOL (meltclosure_ptr_t meltclosp_,
					melt_ptr_t meltfirstargp_,
					const melt_argdescr_cell_t
					meltxargdescr_[],
					union meltparam_un *meltxargtab_,
					const melt_argdescr_cell_t
					meltxresdescr_[],
					union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_7_warmelt_first_INTERN_KEYWORD (meltclosure_ptr_t meltclosp_,
					 melt_ptr_t meltfirstargp_,
					 const melt_argdescr_cell_t
					 meltxargdescr_[],
					 union meltparam_un *meltxargtab_,
					 const melt_argdescr_cell_t
					 meltxresdescr_[],
					 union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_8_warmelt_first_CLONE_SYMBOL (meltclosure_ptr_t meltclosp_,
				       melt_ptr_t meltfirstargp_,
				       const melt_argdescr_cell_t
				       meltxargdescr_[],
				       union meltparam_un *meltxargtab_,
				       const melt_argdescr_cell_t
				       meltxresdescr_[],
				       union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_9_warmelt_first_INITFRESH_CONTENVMAKER (meltclosure_ptr_t meltclosp_,
						 melt_ptr_t meltfirstargp_,
						 const melt_argdescr_cell_t
						 meltxargdescr_[],
						 union meltparam_un
						 *meltxargtab_,
						 const melt_argdescr_cell_t
						 meltxresdescr_[],
						 union meltparam_un
						 *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_10_warmelt_first_INITVALUE_EXPORTER (meltclosure_ptr_t meltclosp_,
					      melt_ptr_t meltfirstargp_,
					      const melt_argdescr_cell_t
					      meltxargdescr_[],
					      union meltparam_un
					      *meltxargtab_,
					      const melt_argdescr_cell_t
					      meltxresdescr_[],
					      union meltparam_un
					      *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_11_warmelt_first_INITVALUE_IMPORTER (meltclosure_ptr_t meltclosp_,
					      melt_ptr_t meltfirstargp_,
					      const melt_argdescr_cell_t
					      meltxargdescr_[],
					      union meltparam_un
					      *meltxargtab_,
					      const melt_argdescr_cell_t
					      meltxresdescr_[],
					      union meltparam_un
					      *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_12_warmelt_first_INITMACRO_EXPORTER (meltclosure_ptr_t meltclosp_,
					      melt_ptr_t meltfirstargp_,
					      const melt_argdescr_cell_t
					      meltxargdescr_[],
					      union meltparam_un
					      *meltxargtab_,
					      const melt_argdescr_cell_t
					      meltxresdescr_[],
					      union meltparam_un
					      *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_13_warmelt_first_INITPATMACRO_EXPORTER (meltclosure_ptr_t meltclosp_,
						 melt_ptr_t meltfirstargp_,
						 const melt_argdescr_cell_t
						 meltxargdescr_[],
						 union meltparam_un
						 *meltxargtab_,
						 const melt_argdescr_cell_t
						 meltxresdescr_[],
						 union meltparam_un
						 *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_14_warmelt_first_INIT_EXITFINALIZER (meltclosure_ptr_t meltclosp_,
					      melt_ptr_t meltfirstargp_,
					      const melt_argdescr_cell_t
					      meltxargdescr_[],
					      union meltparam_un
					      *meltxargtab_,
					      const melt_argdescr_cell_t
					      meltxresdescr_[],
					      union meltparam_un
					      *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_15_warmelt_first_AT_EXIT_FIRST (meltclosure_ptr_t meltclosp_,
					 melt_ptr_t meltfirstargp_,
					 const melt_argdescr_cell_t
					 meltxargdescr_[],
					 union meltparam_un *meltxargtab_,
					 const melt_argdescr_cell_t
					 meltxresdescr_[],
					 union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_16_warmelt_first_AT_EXIT_LAST (meltclosure_ptr_t meltclosp_,
					melt_ptr_t meltfirstargp_,
					const melt_argdescr_cell_t
					meltxargdescr_[],
					union meltparam_un *meltxargtab_,
					const melt_argdescr_cell_t
					meltxresdescr_[],
					union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_17_warmelt_first_END_MELT_PASS_RUNNER (meltclosure_ptr_t meltclosp_,
						melt_ptr_t meltfirstargp_,
						const melt_argdescr_cell_t
						meltxargdescr_[],
						union meltparam_un
						*meltxargtab_,
						const melt_argdescr_cell_t
						meltxresdescr_[],
						union meltparam_un
						*meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_18_warmelt_first_LAMBDA___1__ (meltclosure_ptr_t meltclosp_,
					melt_ptr_t meltfirstargp_,
					const melt_argdescr_cell_t
					meltxargdescr_[],
					union meltparam_un *meltxargtab_,
					const melt_argdescr_cell_t
					meltxresdescr_[],
					union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_19_warmelt_first_LAMBDA___2__ (meltclosure_ptr_t meltclosp_,
					melt_ptr_t meltfirstargp_,
					const melt_argdescr_cell_t
					meltxargdescr_[],
					union meltparam_un *meltxargtab_,
					const melt_argdescr_cell_t
					meltxresdescr_[],
					union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_20_warmelt_first_LAMBDA___3__ (meltclosure_ptr_t meltclosp_,
					melt_ptr_t meltfirstargp_,
					const melt_argdescr_cell_t
					meltxargdescr_[],
					union meltparam_un *meltxargtab_,
					const melt_argdescr_cell_t
					meltxresdescr_[],
					union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_21_warmelt_first_AT_END_MELT_PASS_FIRST (meltclosure_ptr_t
						  meltclosp_,
						  melt_ptr_t meltfirstargp_,
						  const melt_argdescr_cell_t
						  meltxargdescr_[],
						  union meltparam_un
						  *meltxargtab_,
						  const melt_argdescr_cell_t
						  meltxresdescr_[],
						  union meltparam_un
						  *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_22_warmelt_first_AT_END_MELT_PASS_LAST (meltclosure_ptr_t meltclosp_,
						 melt_ptr_t meltfirstargp_,
						 const melt_argdescr_cell_t
						 meltxargdescr_[],
						 union meltparam_un
						 *meltxargtab_,
						 const melt_argdescr_cell_t
						 meltxresdescr_[],
						 union meltparam_un
						 *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_23_warmelt_first_INIT_UNITSTARTER (meltclosure_ptr_t meltclosp_,
					    melt_ptr_t meltfirstargp_,
					    const melt_argdescr_cell_t
					    meltxargdescr_[],
					    union meltparam_un *meltxargtab_,
					    const melt_argdescr_cell_t
					    meltxresdescr_[],
					    union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_24_warmelt_first_LAMBDA___4__ (meltclosure_ptr_t meltclosp_,
					melt_ptr_t meltfirstargp_,
					const melt_argdescr_cell_t
					meltxargdescr_[],
					union meltparam_un *meltxargtab_,
					const melt_argdescr_cell_t
					meltxresdescr_[],
					union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_25_warmelt_first_LAMBDA___5__ (meltclosure_ptr_t meltclosp_,
					melt_ptr_t meltfirstargp_,
					const melt_argdescr_cell_t
					meltxargdescr_[],
					union meltparam_un *meltxargtab_,
					const melt_argdescr_cell_t
					meltxresdescr_[],
					union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_26_warmelt_first_LAMBDA___6__ (meltclosure_ptr_t meltclosp_,
					melt_ptr_t meltfirstargp_,
					const melt_argdescr_cell_t
					meltxargdescr_[],
					union meltparam_un *meltxargtab_,
					const melt_argdescr_cell_t
					meltxresdescr_[],
					union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_27_warmelt_first_AT_START_UNIT_FIRST (meltclosure_ptr_t meltclosp_,
					       melt_ptr_t meltfirstargp_,
					       const melt_argdescr_cell_t
					       meltxargdescr_[],
					       union meltparam_un
					       *meltxargtab_,
					       const melt_argdescr_cell_t
					       meltxresdescr_[],
					       union meltparam_un
					       *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_28_warmelt_first_AT_START_UNIT_LAST (meltclosure_ptr_t meltclosp_,
					      melt_ptr_t meltfirstargp_,
					      const melt_argdescr_cell_t
					      meltxargdescr_[],
					      union meltparam_un
					      *meltxargtab_,
					      const melt_argdescr_cell_t
					      meltxresdescr_[],
					      union meltparam_un
					      *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_29_warmelt_first_INIT_UNITFINISHER (meltclosure_ptr_t meltclosp_,
					     melt_ptr_t meltfirstargp_,
					     const melt_argdescr_cell_t
					     meltxargdescr_[],
					     union meltparam_un *meltxargtab_,
					     const melt_argdescr_cell_t
					     meltxresdescr_[],
					     union meltparam_un
					     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_30_warmelt_first_LAMBDA___7__ (meltclosure_ptr_t meltclosp_,
					melt_ptr_t meltfirstargp_,
					const melt_argdescr_cell_t
					meltxargdescr_[],
					union meltparam_un *meltxargtab_,
					const melt_argdescr_cell_t
					meltxresdescr_[],
					union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_31_warmelt_first_LAMBDA___8__ (meltclosure_ptr_t meltclosp_,
					melt_ptr_t meltfirstargp_,
					const melt_argdescr_cell_t
					meltxargdescr_[],
					union meltparam_un *meltxargtab_,
					const melt_argdescr_cell_t
					meltxresdescr_[],
					union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_32_warmelt_first_LAMBDA___9__ (meltclosure_ptr_t meltclosp_,
					melt_ptr_t meltfirstargp_,
					const melt_argdescr_cell_t
					meltxargdescr_[],
					union meltparam_un *meltxargtab_,
					const melt_argdescr_cell_t
					meltxresdescr_[],
					union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_33_warmelt_first_AT_FINISH_UNIT_FIRST (meltclosure_ptr_t meltclosp_,
						melt_ptr_t meltfirstargp_,
						const melt_argdescr_cell_t
						meltxargdescr_[],
						union meltparam_un
						*meltxargtab_,
						const melt_argdescr_cell_t
						meltxresdescr_[],
						union meltparam_un
						*meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_34_warmelt_first_AT_FINISH_UNIT_LAST (meltclosure_ptr_t meltclosp_,
					       melt_ptr_t meltfirstargp_,
					       const melt_argdescr_cell_t
					       meltxargdescr_[],
					       union meltparam_un
					       *meltxargtab_,
					       const melt_argdescr_cell_t
					       meltxresdescr_[],
					       union meltparam_un
					       *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_35_warmelt_first_INIT_OPTIONSETTER (meltclosure_ptr_t meltclosp_,
					     melt_ptr_t meltfirstargp_,
					     const melt_argdescr_cell_t
					     meltxargdescr_[],
					     union meltparam_un *meltxargtab_,
					     const melt_argdescr_cell_t
					     meltxresdescr_[],
					     union meltparam_un
					     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_36_warmelt_first_REGISTER_OPTION (meltclosure_ptr_t meltclosp_,
					   melt_ptr_t meltfirstargp_,
					   const melt_argdescr_cell_t
					   meltxargdescr_[],
					   union meltparam_un *meltxargtab_,
					   const melt_argdescr_cell_t
					   meltxresdescr_[],
					   union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_37_warmelt_first_OPTION_HELPER_FUN (meltclosure_ptr_t meltclosp_,
					     melt_ptr_t meltfirstargp_,
					     const melt_argdescr_cell_t
					     meltxargdescr_[],
					     union meltparam_un *meltxargtab_,
					     const melt_argdescr_cell_t
					     meltxresdescr_[],
					     union meltparam_un
					     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_38_warmelt_first_LAMBDA___10__ (meltclosure_ptr_t meltclosp_,
					 melt_ptr_t meltfirstargp_,
					 const melt_argdescr_cell_t
					 meltxargdescr_[],
					 union meltparam_un *meltxargtab_,
					 const melt_argdescr_cell_t
					 meltxresdescr_[],
					 union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_39_warmelt_first_MAPOBJECT_EVERY (meltclosure_ptr_t meltclosp_,
					   melt_ptr_t meltfirstargp_,
					   const melt_argdescr_cell_t
					   meltxargdescr_[],
					   union meltparam_un *meltxargtab_,
					   const melt_argdescr_cell_t
					   meltxresdescr_[],
					   union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_40_warmelt_first_MAPOBJECT_ITERATE_TEST (meltclosure_ptr_t
						  meltclosp_,
						  melt_ptr_t meltfirstargp_,
						  const melt_argdescr_cell_t
						  meltxargdescr_[],
						  union meltparam_un
						  *meltxargtab_,
						  const melt_argdescr_cell_t
						  meltxresdescr_[],
						  union meltparam_un
						  *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_41_warmelt_first_LIST_EVERY (meltclosure_ptr_t meltclosp_,
				      melt_ptr_t meltfirstargp_,
				      const melt_argdescr_cell_t
				      meltxargdescr_[],
				      union meltparam_un *meltxargtab_,
				      const melt_argdescr_cell_t
				      meltxresdescr_[],
				      union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_42_warmelt_first_LIST_ITERATE_TEST (meltclosure_ptr_t meltclosp_,
					     melt_ptr_t meltfirstargp_,
					     const melt_argdescr_cell_t
					     meltxargdescr_[],
					     union meltparam_un *meltxargtab_,
					     const melt_argdescr_cell_t
					     meltxresdescr_[],
					     union meltparam_un
					     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_43_warmelt_first_LIST_APPEND2LIST (meltclosure_ptr_t meltclosp_,
					    melt_ptr_t meltfirstargp_,
					    const melt_argdescr_cell_t
					    meltxargdescr_[],
					    union meltparam_un *meltxargtab_,
					    const melt_argdescr_cell_t
					    meltxresdescr_[],
					    union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_44_warmelt_first_LIST_MAP (meltclosure_ptr_t meltclosp_,
				    melt_ptr_t meltfirstargp_,
				    const melt_argdescr_cell_t
				    meltxargdescr_[],
				    union meltparam_un *meltxargtab_,
				    const melt_argdescr_cell_t
				    meltxresdescr_[],
				    union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_45_warmelt_first_LIST_FIND (meltclosure_ptr_t meltclosp_,
				     melt_ptr_t meltfirstargp_,
				     const melt_argdescr_cell_t
				     meltxargdescr_[],
				     union meltparam_un *meltxargtab_,
				     const melt_argdescr_cell_t
				     meltxresdescr_[],
				     union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_46_warmelt_first_LIST_TO_MULTIPLE (meltclosure_ptr_t meltclosp_,
					    melt_ptr_t meltfirstargp_,
					    const melt_argdescr_cell_t
					    meltxargdescr_[],
					    union meltparam_un *meltxargtab_,
					    const melt_argdescr_cell_t
					    meltxresdescr_[],
					    union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_47_warmelt_first_LAMBDA___11__ (meltclosure_ptr_t meltclosp_,
					 melt_ptr_t meltfirstargp_,
					 const melt_argdescr_cell_t
					 meltxargdescr_[],
					 union meltparam_un *meltxargtab_,
					 const melt_argdescr_cell_t
					 meltxresdescr_[],
					 union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_48_warmelt_first_PAIRLIST_TO_MULTIPLE (meltclosure_ptr_t meltclosp_,
						melt_ptr_t meltfirstargp_,
						const melt_argdescr_cell_t
						meltxargdescr_[],
						union meltparam_un
						*meltxargtab_,
						const melt_argdescr_cell_t
						meltxresdescr_[],
						union meltparam_un
						*meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_49_warmelt_first_MULTIPLE_ITERATE_TEST (meltclosure_ptr_t meltclosp_,
						 melt_ptr_t meltfirstargp_,
						 const melt_argdescr_cell_t
						 meltxargdescr_[],
						 union meltparam_un
						 *meltxargtab_,
						 const melt_argdescr_cell_t
						 meltxresdescr_[],
						 union meltparam_un
						 *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_50_warmelt_first_MULTIPLE_MAP (meltclosure_ptr_t meltclosp_,
					melt_ptr_t meltfirstargp_,
					const melt_argdescr_cell_t
					meltxargdescr_[],
					union meltparam_un *meltxargtab_,
					const melt_argdescr_cell_t
					meltxresdescr_[],
					union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_51_warmelt_first_MULTIPLE_TO_LIST (meltclosure_ptr_t meltclosp_,
					    melt_ptr_t meltfirstargp_,
					    const melt_argdescr_cell_t
					    meltxargdescr_[],
					    union meltparam_un *meltxargtab_,
					    const melt_argdescr_cell_t
					    meltxresdescr_[],
					    union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_52_warmelt_first_CLOSURE_EVERY (meltclosure_ptr_t meltclosp_,
					 melt_ptr_t meltfirstargp_,
					 const melt_argdescr_cell_t
					 meltxargdescr_[],
					 union meltparam_un *meltxargtab_,
					 const melt_argdescr_cell_t
					 meltxresdescr_[],
					 union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_53_warmelt_first_ROUTINE_EVERY (meltclosure_ptr_t meltclosp_,
					 melt_ptr_t meltfirstargp_,
					 const melt_argdescr_cell_t
					 meltxargdescr_[],
					 union meltparam_un *meltxargtab_,
					 const melt_argdescr_cell_t
					 meltxresdescr_[],
					 union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_54_warmelt_first_INSTALL_METHOD (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_55_warmelt_first_COMPARE_OBJ_RANKED (meltclosure_ptr_t meltclosp_,
					      melt_ptr_t meltfirstargp_,
					      const melt_argdescr_cell_t
					      meltxargdescr_[],
					      union meltparam_un
					      *meltxargtab_,
					      const melt_argdescr_cell_t
					      meltxresdescr_[],
					      union meltparam_un
					      *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_56_warmelt_first_MAPOBJECT_SORTED_ATTRIBUTE_TUPLE (meltclosure_ptr_t
							    meltclosp_,
							    melt_ptr_t
							    meltfirstargp_,
							    const
							    melt_argdescr_cell_t
							    meltxargdescr_[],
							    union meltparam_un
							    *meltxargtab_,
							    const
							    melt_argdescr_cell_t
							    meltxresdescr_[],
							    union meltparam_un
							    *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_57_warmelt_first_LAMBDA___12__ (meltclosure_ptr_t meltclosp_,
					 melt_ptr_t meltfirstargp_,
					 const melt_argdescr_cell_t
					 meltxargdescr_[],
					 union meltparam_un *meltxargtab_,
					 const melt_argdescr_cell_t
					 meltxresdescr_[],
					 union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_58_warmelt_first_LAMBDA___13__ (meltclosure_ptr_t meltclosp_,
					 melt_ptr_t meltfirstargp_,
					 const melt_argdescr_cell_t
					 meltxargdescr_[],
					 union meltparam_un *meltxargtab_,
					 const melt_argdescr_cell_t
					 meltxresdescr_[],
					 union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_59_warmelt_first_LAMBDA___14__ (meltclosure_ptr_t meltclosp_,
					 melt_ptr_t meltfirstargp_,
					 const melt_argdescr_cell_t
					 meltxargdescr_[],
					 union meltparam_un *meltxargtab_,
					 const melt_argdescr_cell_t
					 meltxresdescr_[],
					 union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_60_warmelt_first_FRESH_ENV (meltclosure_ptr_t meltclosp_,
				     melt_ptr_t meltfirstargp_,
				     const melt_argdescr_cell_t
				     meltxargdescr_[],
				     union meltparam_un *meltxargtab_,
				     const melt_argdescr_cell_t
				     meltxresdescr_[],
				     union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_61_warmelt_first_FIND_ENV (meltclosure_ptr_t meltclosp_,
				    melt_ptr_t meltfirstargp_,
				    const melt_argdescr_cell_t
				    meltxargdescr_[],
				    union meltparam_un *meltxargtab_,
				    const melt_argdescr_cell_t
				    meltxresdescr_[],
				    union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_62_warmelt_first_FIND_ENV_DEBUG (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_63_warmelt_first_FIND_ENCLOSING_ENV (meltclosure_ptr_t meltclosp_,
					      melt_ptr_t meltfirstargp_,
					      const melt_argdescr_cell_t
					      meltxargdescr_[],
					      union meltparam_un
					      *meltxargtab_,
					      const melt_argdescr_cell_t
					      meltxresdescr_[],
					      union meltparam_un
					      *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_64_warmelt_first_PUT_ENV (meltclosure_ptr_t meltclosp_,
				   melt_ptr_t meltfirstargp_,
				   const melt_argdescr_cell_t
				   meltxargdescr_[],
				   union meltparam_un *meltxargtab_,
				   const melt_argdescr_cell_t
				   meltxresdescr_[],
				   union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_65_warmelt_first_OVERWRITE_ENV (meltclosure_ptr_t meltclosp_,
					 melt_ptr_t meltfirstargp_,
					 const melt_argdescr_cell_t
					 meltxargdescr_[],
					 union meltparam_un *meltxargtab_,
					 const melt_argdescr_cell_t
					 meltxresdescr_[],
					 union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_66_warmelt_first_POST_INITIALIZATION (meltclosure_ptr_t meltclosp_,
					       melt_ptr_t meltfirstargp_,
					       const melt_argdescr_cell_t
					       meltxargdescr_[],
					       union meltparam_un
					       *meltxargtab_,
					       const melt_argdescr_cell_t
					       meltxresdescr_[],
					       union meltparam_un
					       *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_67_warmelt_first_RETRIEVE_DICTIONNARY_CTYPE_GTY (meltclosure_ptr_t
							  meltclosp_,
							  melt_ptr_t
							  meltfirstargp_,
							  const
							  melt_argdescr_cell_t
							  meltxargdescr_[],
							  union meltparam_un
							  *meltxargtab_,
							  const
							  melt_argdescr_cell_t
							  meltxresdescr_[],
							  union meltparam_un
							  *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_68_warmelt_first_RETRIEVE_DICTIONNARY_CTYPE (meltclosure_ptr_t
						      meltclosp_,
						      melt_ptr_t
						      meltfirstargp_,
						      const
						      melt_argdescr_cell_t
						      meltxargdescr_[],
						      union meltparam_un
						      *meltxargtab_,
						      const
						      melt_argdescr_cell_t
						      meltxresdescr_[],
						      union meltparam_un
						      *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_69_warmelt_first_DEBUG_MSG_FUN (meltclosure_ptr_t meltclosp_,
					 melt_ptr_t meltfirstargp_,
					 const melt_argdescr_cell_t
					 meltxargdescr_[],
					 union meltparam_un *meltxargtab_,
					 const melt_argdescr_cell_t
					 meltxresdescr_[],
					 union meltparam_un *meltxrestab_);



MELT_EXTERN void *melt_start_this_module (void *);


/* define different names when debugging or not */
#if MELT_HAVE_DEBUG
MELT_EXTERN const char meltmodule_warmelt_first__melt_have_debug_enabled[];
#define melt_have_debug_string meltmodule_warmelt_first__melt_have_debug_enabled
#else /*!MELT_HAVE_DEBUG */
MELT_EXTERN const char meltmodule_warmelt_first__melt_have_debug_disabled[];
#define melt_have_debug_string meltmodule_warmelt_first__melt_have_debug_disabled
#endif /*!MELT_HAVE_DEBUG */


struct frame_melt_start_this_module_st;
void MELT_MODULE_VISIBILITY meltmod__warmelt_first__initialmeltchunk_0 (struct
									frame_melt_start_this_module_st
									*,
									char
									*);
void MELT_MODULE_VISIBILITY meltmod__warmelt_first__initialmeltchunk_1 (struct
									frame_melt_start_this_module_st
									*,
									char
									*);
void MELT_MODULE_VISIBILITY meltmod__warmelt_first__initialmeltchunk_2 (struct
									frame_melt_start_this_module_st
									*,
									char
									*);
void MELT_MODULE_VISIBILITY meltmod__warmelt_first__initialmeltchunk_3 (struct
									frame_melt_start_this_module_st
									*,
									char
									*);
void MELT_MODULE_VISIBILITY meltmod__warmelt_first__initialmeltchunk_4 (struct
									frame_melt_start_this_module_st
									*,
									char
									*);
void MELT_MODULE_VISIBILITY meltmod__warmelt_first__initialmeltchunk_5 (struct
									frame_melt_start_this_module_st
									*,
									char
									*);
void MELT_MODULE_VISIBILITY meltmod__warmelt_first__initialmeltchunk_6 (struct
									frame_melt_start_this_module_st
									*,
									char
									*);
void MELT_MODULE_VISIBILITY meltmod__warmelt_first__initialmeltchunk_7 (struct
									frame_melt_start_this_module_st
									*,
									char
									*);
void MELT_MODULE_VISIBILITY meltmod__warmelt_first__initialmeltchunk_8 (struct
									frame_melt_start_this_module_st
									*,
									char
									*);
void MELT_MODULE_VISIBILITY meltmod__warmelt_first__initialmeltchunk_9 (struct
									frame_melt_start_this_module_st
									*,
									char
									*);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_first__initialmeltchunk_10 (struct
					     frame_melt_start_this_module_st
					     *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_first__initialmeltchunk_11 (struct
					     frame_melt_start_this_module_st
					     *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_first__initialmeltchunk_12 (struct
					     frame_melt_start_this_module_st
					     *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_first__initialmeltchunk_13 (struct
					     frame_melt_start_this_module_st
					     *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_first__initialmeltchunk_14 (struct
					     frame_melt_start_this_module_st
					     *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_first__initialmeltchunk_15 (struct
					     frame_melt_start_this_module_st
					     *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_first__initialmeltchunk_16 (struct
					     frame_melt_start_this_module_st
					     *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_first__initialmeltchunk_17 (struct
					     frame_melt_start_this_module_st
					     *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_first__initialmeltchunk_18 (struct
					     frame_melt_start_this_module_st
					     *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_first__initialmeltchunk_19 (struct
					     frame_melt_start_this_module_st
					     *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_first__initialmeltchunk_20 (struct
					     frame_melt_start_this_module_st
					     *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_first__initialmeltchunk_21 (struct
					     frame_melt_start_this_module_st
					     *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_first__initialmeltchunk_22 (struct
					     frame_melt_start_this_module_st
					     *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_first__initialmeltchunk_23 (struct
					     frame_melt_start_this_module_st
					     *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_first__initialmeltchunk_24 (struct
					     frame_melt_start_this_module_st
					     *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_first__initialmeltchunk_25 (struct
					     frame_melt_start_this_module_st
					     *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_first__initialmeltchunk_26 (struct
					     frame_melt_start_this_module_st
					     *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_first__initialmeltchunk_27 (struct
					     frame_melt_start_this_module_st
					     *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_first__initialmeltchunk_28 (struct
					     frame_melt_start_this_module_st
					     *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_first__initialmeltchunk_29 (struct
					     frame_melt_start_this_module_st
					     *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_first__initialmeltchunk_30 (struct
					     frame_melt_start_this_module_st
					     *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_first__initialmeltchunk_31 (struct
					     frame_melt_start_this_module_st
					     *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_first__initialmeltchunk_32 (struct
					     frame_melt_start_this_module_st
					     *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_first__initialmeltchunk_33 (struct
					     frame_melt_start_this_module_st
					     *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_first__initialmeltchunk_34 (struct
					     frame_melt_start_this_module_st
					     *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_first__initialmeltchunk_35 (struct
					     frame_melt_start_this_module_st
					     *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_first__initialmeltchunk_36 (struct
					     frame_melt_start_this_module_st
					     *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_first__initialmeltchunk_37 (struct
					     frame_melt_start_this_module_st
					     *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_first__initialmeltchunk_38 (struct
					     frame_melt_start_this_module_st
					     *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_first__initialmeltchunk_39 (struct
					     frame_melt_start_this_module_st
					     *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_first__initialmeltchunk_40 (struct
					     frame_melt_start_this_module_st
					     *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_first__initialmeltchunk_41 (struct
					     frame_melt_start_this_module_st
					     *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_first__initialmeltchunk_42 (struct
					     frame_melt_start_this_module_st
					     *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_first__initialmeltchunk_43 (struct
					     frame_melt_start_this_module_st
					     *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_first__initialmeltchunk_44 (struct
					     frame_melt_start_this_module_st
					     *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_first__initialmeltchunk_45 (struct
					     frame_melt_start_this_module_st
					     *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_first__initialmeltchunk_46 (struct
					     frame_melt_start_this_module_st
					     *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_first__initialmeltchunk_47 (struct
					     frame_melt_start_this_module_st
					     *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_first__initialmeltchunk_48 (struct
					     frame_melt_start_this_module_st
					     *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_first__initialmeltchunk_49 (struct
					     frame_melt_start_this_module_st
					     *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_first__initialmeltchunk_50 (struct
					     frame_melt_start_this_module_st
					     *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_first__initialmeltchunk_51 (struct
					     frame_melt_start_this_module_st
					     *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_first__initialmeltchunk_52 (struct
					     frame_melt_start_this_module_st
					     *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_first__initialmeltchunk_53 (struct
					     frame_melt_start_this_module_st
					     *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_first__initialmeltchunk_54 (struct
					     frame_melt_start_this_module_st
					     *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_first__initialmeltchunk_55 (struct
					     frame_melt_start_this_module_st
					     *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_first__initialmeltchunk_56 (struct
					     frame_melt_start_this_module_st
					     *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_first__initialmeltchunk_57 (struct
					     frame_melt_start_this_module_st
					     *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_first__initialmeltchunk_58 (struct
					     frame_melt_start_this_module_st
					     *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_first__initialmeltchunk_59 (struct
					     frame_melt_start_this_module_st
					     *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_first__forward_or_mark_module_start_frame (struct
							    melt_callframe_st
							    *fp, int marking);
#define meltmarking_melt_start_this_module  meltmod__warmelt_first__forward_or_mark_module_start_frame



/**** warmelt-first+02.c implementations ****/




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_40_warmelt_first_MAPOBJECT_ITERATE_TEST (meltclosure_ptr_t
						  meltclosp_,
						  melt_ptr_t meltfirstargp_,
						  const melt_argdescr_cell_t
						  meltxargdescr_[],
						  union meltparam_un
						  *meltxargtab_,
						  const melt_argdescr_cell_t
						  meltxresdescr_[],
						  union meltparam_un
						  *meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_40_warmelt_first_MAPOBJECT_ITERATE_TEST_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_40_warmelt_first_MAPOBJECT_ITERATE_TEST_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 10
    melt_ptr_t mcfr_varptr[10];
#define MELTFRAM_NBVARNUM 3
    long mcfr_varnum[3];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_40_warmelt_first_MAPOBJECT_ITERATE_TEST is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_40_warmelt_first_MAPOBJECT_ITERATE_TEST_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 10; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_40_warmelt_first_MAPOBJECT_ITERATE_TEST nbval 10*/
  meltfram__.mcfr_nbvar = 10 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("MAPOBJECT_ITERATE_TEST", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-first.melt:3486:/ getarg");
 /*_.MAP__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.F__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.F__V3*/ meltfptr[2])) != NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {

    MELT_LOCATION ("warmelt-first.melt:3493:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#IS_MAPOBJECT__L1*/ meltfnum[0] =
      /*is_mapobject: */
      (melt_magic_discr ((melt_ptr_t) ( /*_.MAP__V2*/ meltfptr[1])) ==
       MELTOBMAG_MAPOBJECTS);;
    MELT_LOCATION ("warmelt-first.melt:3493:/ cond");
    /*cond */ if ( /*_#IS_MAPOBJECT__L1*/ meltfnum[0])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-first.melt:3494:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
   /*_#IS_CLOSURE__L2*/ meltfnum[1] =
	    (melt_magic_discr ((melt_ptr_t) ( /*_.F__V3*/ meltfptr[2])) ==
	     MELTOBMAG_CLOSURE);;
	  MELT_LOCATION ("warmelt-first.melt:3494:/ cond");
	  /*cond */ if ( /*_#IS_CLOSURE__L2*/ meltfnum[1])	/*then */
	    {
	      /*^cond.then */
	      /*^block */
	      /*anyblock */
	      {

		/*citerblock FOREACH_IN_MAPOBJECT */
		{
		  /* foreach_in_mapobject meltcit1__EACHOBMAP : */ int
		    meltcit1__EACHOBMAP_ix = 0, meltcit1__EACHOBMAP_siz = 0;
		  for (meltcit1__EACHOBMAP_ix = 0;
		       /* we retrieve in meltcit1__EACHOBMAP_siz the size at each iteration since it could change. */
		       meltcit1__EACHOBMAP_ix >= 0
		       && (meltcit1__EACHOBMAP_siz =
			   melt_size_mapobjects ((meltmapobjects_ptr_t)
						 /*_.MAP__V2*/ meltfptr[1])) >
		       0
		       && meltcit1__EACHOBMAP_ix < meltcit1__EACHOBMAP_siz;
		       meltcit1__EACHOBMAP_ix++)
		    {
    /*_.CURAT__V4*/ meltfptr[3] = NULL;
    /*_.CURVAL__V5*/ meltfptr[4] = NULL;
		      /*_.CURAT__V4*/ meltfptr[3] =
			(melt_ptr_t) (((meltmapobjects_ptr_t) /*_.MAP__V2*/
				       meltfptr[1])->
				      entab[meltcit1__EACHOBMAP_ix].e_at);
		      if ( /*_.CURAT__V4*/ meltfptr[3] == HTAB_DELETED_ENTRY)
			{				     /*_.CURAT__V4*/
			  meltfptr[3] = NULL;
			  continue;
			};
		      if (! /*_.CURAT__V4*/ meltfptr[3])
			continue;
		      /*_.CURVAL__V5*/ meltfptr[4] =
			((meltmapobjects_ptr_t) /*_.MAP__V2*/ meltfptr[1])->
			entab[meltcit1__EACHOBMAP_ix].e_va;
		      if (! /*_.CURVAL__V5*/ meltfptr[4])
			continue;



		      MELT_LOCATION ("warmelt-first.melt:3500:/ checksignal");
		      MELT_CHECK_SIGNAL ();
		      ;
		      MELT_LOCATION ("warmelt-first.melt:3498:/ quasiblock");


		      /*^multiapply */
		      /*multiapply 2args, 1x.res */
		      {
			union meltparam_un argtab[1];

			union meltparam_un restab[1];
			memset (&restab, 0, sizeof (restab));
			memset (&argtab, 0, sizeof (argtab));
			/*^multiapply.arg */
			argtab[0].meltbp_aptr =
			  (melt_ptr_t *) & /*_.CURVAL__V5*/ meltfptr[4];
			/*^multiapply.xres */
			restab[0].meltbp_aptr =
			  (melt_ptr_t *) & /*_.OTHER__V8*/ meltfptr[7];
			/*^multiapply.appl */
			/*_.TEST__V7*/ meltfptr[6] =
			  melt_apply ((meltclosure_ptr_t)
				      ( /*_.F__V3*/ meltfptr[2]),
				      (melt_ptr_t) ( /*_.CURAT__V4*/
						    meltfptr[3]),
				      (MELTBPARSTR_PTR ""), argtab,
				      (MELTBPARSTR_PTR ""), restab);
		      }
		      ;
		      /*^quasiblock */


		      MELT_LOCATION ("warmelt-first.melt:3501:/ checksignal");
		      MELT_CHECK_SIGNAL ();
		      ;
      /*_#NULL__L3*/ meltfnum[2] =
			(( /*_.TEST__V7*/ meltfptr[6]) == NULL);;
		      MELT_LOCATION ("warmelt-first.melt:3501:/ cond");
		      /*cond */ if ( /*_#NULL__L3*/ meltfnum[2])	/*then */
			{
			  /*^cond.then */
			  /*^block */
			  /*anyblock */
			  {

			    MELT_LOCATION
			      ("warmelt-first.melt:3502:/ checksignal");
			    MELT_CHECK_SIGNAL ();
			    ;
			    /*^quasiblock */


			    /*_.RETVAL___V1*/ meltfptr[0] =
			      /*_.CURAT__V4*/ meltfptr[3];;
			    MELT_LOCATION
			      ("warmelt-first.melt:3502:/ putxtraresult");
			    if (!meltxrestab_ || !meltxresdescr_)
			      goto labend_rout;
			    if (meltxresdescr_[0] != MELTBPAR_PTR)
			      goto labend_rout;
			    if (meltxrestab_[0].meltbp_aptr)
			      *(meltxrestab_[0].meltbp_aptr) =
				(melt_ptr_t) ( /*_.CURVAL__V5*/ meltfptr[4]);
			    ;
			    /*^putxtraresult */
			    if (!meltxrestab_ || !meltxresdescr_)
			      goto labend_rout;
			    if (meltxresdescr_[1] != MELTBPAR_PTR)
			      goto labend_rout;
			    if (meltxrestab_[1].meltbp_aptr)
			      *(meltxrestab_[1].meltbp_aptr) =
				(melt_ptr_t) ( /*_.OTHER__V8*/ meltfptr[7]);
			    ;
			    /*^finalreturn */
			    ;
			    /*finalret */ goto labend_rout;
			    /*_.IF___V9*/ meltfptr[8] =
			      /*_.RETURN___V10*/ meltfptr[9];;
			    /*epilog */

			    MELT_LOCATION ("warmelt-first.melt:3501:/ clear");
		  /*clear *//*_.RETURN___V10*/ meltfptr[9] = 0;
			  }
			  ;
			}
		      else
			{	/*^cond.else */

       /*_.IF___V9*/ meltfptr[8] = NULL;;
			}
		      ;
		      /*^compute */
		      /*_.MULTI___V6*/ meltfptr[5] =
			/*_.IF___V9*/ meltfptr[8];;

		      MELT_LOCATION ("warmelt-first.melt:3498:/ clear");
		/*clear *//*_#NULL__L3*/ meltfnum[2] = 0;
		      /*^clear */
		/*clear *//*_.IF___V9*/ meltfptr[8] = 0;

		      /*^clear */
		/*clear *//*_.OTHER__V8*/ meltfptr[7] = 0;
		      /* foreach_in_mapobject end meltcit1__EACHOBMAP */
    /*_.CURAT__V4*/ meltfptr[3] = NULL;
    /*_.CURVAL__V5*/ meltfptr[4] = NULL;
		    }


		  /*citerepilog */

		  MELT_LOCATION ("warmelt-first.melt:3495:/ clear");
		/*clear *//*_.CURAT__V4*/ meltfptr[3] = 0;
		  /*^clear */
		/*clear *//*_.CURVAL__V5*/ meltfptr[4] = 0;
		  /*^clear */
		/*clear *//*_.MULTI___V6*/ meltfptr[5] = 0;
		}		/*endciterblock FOREACH_IN_MAPOBJECT */
		;
		/*epilog */
	      }
	      ;
	    }			/*noelse */
	  ;
	  /*epilog */

	  MELT_LOCATION ("warmelt-first.melt:3493:/ clear");
	     /*clear *//*_#IS_CLOSURE__L2*/ meltfnum[1] = 0;
	}
	;
      }				/*noelse */
    ;
    /*epilog */

    MELT_LOCATION ("warmelt-first.melt:3486:/ clear");
	   /*clear *//*_#IS_MAPOBJECT__L1*/ meltfnum[0] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("MAPOBJECT_ITERATE_TEST", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_40_warmelt_first_MAPOBJECT_ITERATE_TEST_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_40_warmelt_first_MAPOBJECT_ITERATE_TEST */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_41_warmelt_first_LIST_EVERY (meltclosure_ptr_t meltclosp_,
				      melt_ptr_t meltfirstargp_,
				      const melt_argdescr_cell_t
				      meltxargdescr_[],
				      union meltparam_un * meltxargtab_,
				      const melt_argdescr_cell_t
				      meltxresdescr_[],
				      union meltparam_un * meltxrestab_)
{
  long current_blocklevel_signals_meltrout_41_warmelt_first_LIST_EVERY_melt =
    melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_41_warmelt_first_LIST_EVERY_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 6
    melt_ptr_t mcfr_varptr[6];
#define MELTFRAM_NBVARNUM 2
    long mcfr_varnum[2];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_41_warmelt_first_LIST_EVERY is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_41_warmelt_first_LIST_EVERY_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 6; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_41_warmelt_first_LIST_EVERY nbval 6*/
  meltfram__.mcfr_nbvar = 6 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("LIST_EVERY", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-first.melt:3533:/ getarg");
 /*_.LIS__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.F__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.F__V3*/ meltfptr[2])) != NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {

    MELT_LOCATION ("warmelt-first.melt:3535:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#IS_LIST__L1*/ meltfnum[0] =
      (melt_magic_discr ((melt_ptr_t) ( /*_.LIS__V2*/ meltfptr[1])) ==
       MELTOBMAG_LIST);;
    MELT_LOCATION ("warmelt-first.melt:3535:/ cond");
    /*cond */ if ( /*_#IS_LIST__L1*/ meltfnum[0])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-first.melt:3536:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
   /*_#IS_CLOSURE__L2*/ meltfnum[1] =
	    (melt_magic_discr ((melt_ptr_t) ( /*_.F__V3*/ meltfptr[2])) ==
	     MELTOBMAG_CLOSURE);;
	  MELT_LOCATION ("warmelt-first.melt:3536:/ cond");
	  /*cond */ if ( /*_#IS_CLOSURE__L2*/ meltfnum[1])	/*then */
	    {
	      /*^cond.then */
	      /*^block */
	      /*anyblock */
	      {

		/*citerblock FOREACH_IN_LIST */
		{
		  /* start foreach_in_list meltcit1__EACHLIST */
		  for ( /*_.CURPAIR__V4*/ meltfptr[3] =
		       melt_list_first ((melt_ptr_t) /*_.LIS__V2*/
					meltfptr[1]);
		       melt_magic_discr ((melt_ptr_t) /*_.CURPAIR__V4*/
					 meltfptr[3]) == MELTOBMAG_PAIR;
		       /*_.CURPAIR__V4*/ meltfptr[3] =
		       melt_pair_tail ((melt_ptr_t) /*_.CURPAIR__V4*/
				       meltfptr[3]))
		    {
		      /*_.CURCOMP__V5*/ meltfptr[4] =
			melt_pair_head ((melt_ptr_t) /*_.CURPAIR__V4*/
					meltfptr[3]);


		      MELT_LOCATION ("warmelt-first.melt:3540:/ checksignal");
		      MELT_CHECK_SIGNAL ();
		      ;
		      /*^apply */
		      /*apply */
		      {
			/*_.F__V6*/ meltfptr[5] =
			  melt_apply ((meltclosure_ptr_t)
				      ( /*_.F__V3*/ meltfptr[2]),
				      (melt_ptr_t) ( /*_.CURCOMP__V5*/
						    meltfptr[4]), (""),
				      (union meltparam_un *) 0, "",
				      (union meltparam_un *) 0);
		      }
		      ;
		    }		/* end foreach_in_list meltcit1__EACHLIST */
     /*_.CURPAIR__V4*/ meltfptr[3] = NULL;
     /*_.CURCOMP__V5*/ meltfptr[4] = NULL;


		  /*citerepilog */

		  MELT_LOCATION ("warmelt-first.melt:3537:/ clear");
		/*clear *//*_.CURPAIR__V4*/ meltfptr[3] = 0;
		  /*^clear */
		/*clear *//*_.CURCOMP__V5*/ meltfptr[4] = 0;
		  /*^clear */
		/*clear *//*_.F__V6*/ meltfptr[5] = 0;
		}		/*endciterblock FOREACH_IN_LIST */
		;
		/*epilog */
	      }
	      ;
	    }			/*noelse */
	  ;
	  /*epilog */

	  MELT_LOCATION ("warmelt-first.melt:3535:/ clear");
	     /*clear *//*_#IS_CLOSURE__L2*/ meltfnum[1] = 0;
	}
	;
      }				/*noelse */
    ;
    /*epilog */

    MELT_LOCATION ("warmelt-first.melt:3533:/ clear");
	   /*clear *//*_#IS_LIST__L1*/ meltfnum[0] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("LIST_EVERY", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_41_warmelt_first_LIST_EVERY_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*noretval */ NULL);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_41_warmelt_first_LIST_EVERY */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_42_warmelt_first_LIST_ITERATE_TEST (meltclosure_ptr_t meltclosp_,
					     melt_ptr_t meltfirstargp_,
					     const melt_argdescr_cell_t
					     meltxargdescr_[],
					     union meltparam_un *
					     meltxargtab_,
					     const melt_argdescr_cell_t
					     meltxresdescr_[],
					     union meltparam_un *
					     meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_42_warmelt_first_LIST_ITERATE_TEST_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_42_warmelt_first_LIST_ITERATE_TEST_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 10
    melt_ptr_t mcfr_varptr[10];
#define MELTFRAM_NBVARNUM 3
    long mcfr_varnum[3];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_42_warmelt_first_LIST_ITERATE_TEST is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_42_warmelt_first_LIST_ITERATE_TEST_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 10; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_42_warmelt_first_LIST_ITERATE_TEST nbval 10*/
  meltfram__.mcfr_nbvar = 10 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("LIST_ITERATE_TEST", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-first.melt:3544:/ getarg");
 /*_.LIS__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.F__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.F__V3*/ meltfptr[2])) != NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {

    MELT_LOCATION ("warmelt-first.melt:3548:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#IS_LIST__L1*/ meltfnum[0] =
      (melt_magic_discr ((melt_ptr_t) ( /*_.LIS__V2*/ meltfptr[1])) ==
       MELTOBMAG_LIST);;
    MELT_LOCATION ("warmelt-first.melt:3548:/ cond");
    /*cond */ if ( /*_#IS_LIST__L1*/ meltfnum[0])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-first.melt:3549:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
   /*_#IS_CLOSURE__L2*/ meltfnum[1] =
	    (melt_magic_discr ((melt_ptr_t) ( /*_.F__V3*/ meltfptr[2])) ==
	     MELTOBMAG_CLOSURE);;
	  MELT_LOCATION ("warmelt-first.melt:3549:/ cond");
	  /*cond */ if ( /*_#IS_CLOSURE__L2*/ meltfnum[1])	/*then */
	    {
	      /*^cond.then */
	      /*^block */
	      /*anyblock */
	      {

		/*citerblock FOREACH_IN_LIST */
		{
		  /* start foreach_in_list meltcit1__EACHLIST */
		  for ( /*_.CURPAIR__V4*/ meltfptr[3] =
		       melt_list_first ((melt_ptr_t) /*_.LIS__V2*/
					meltfptr[1]);
		       melt_magic_discr ((melt_ptr_t) /*_.CURPAIR__V4*/
					 meltfptr[3]) == MELTOBMAG_PAIR;
		       /*_.CURPAIR__V4*/ meltfptr[3] =
		       melt_pair_tail ((melt_ptr_t) /*_.CURPAIR__V4*/
				       meltfptr[3]))
		    {
		      /*_.CURCOMP__V5*/ meltfptr[4] =
			melt_pair_head ((melt_ptr_t) /*_.CURPAIR__V4*/
					meltfptr[3]);


		      MELT_LOCATION ("warmelt-first.melt:3555:/ checksignal");
		      MELT_CHECK_SIGNAL ();
		      ;
		      MELT_LOCATION ("warmelt-first.melt:3553:/ quasiblock");


		      /*^multiapply */
		      /*multiapply 2args, 1x.res */
		      {
			union meltparam_un argtab[1];

			union meltparam_un restab[1];
			memset (&restab, 0, sizeof (restab));
			memset (&argtab, 0, sizeof (argtab));
			/*^multiapply.arg */
			argtab[0].meltbp_aptr =
			  (melt_ptr_t *) & /*_.CURPAIR__V4*/ meltfptr[3];
			/*^multiapply.xres */
			restab[0].meltbp_aptr =
			  (melt_ptr_t *) & /*_.OTHER__V8*/ meltfptr[7];
			/*^multiapply.appl */
			/*_.TEST__V7*/ meltfptr[6] =
			  melt_apply ((meltclosure_ptr_t)
				      ( /*_.F__V3*/ meltfptr[2]),
				      (melt_ptr_t) ( /*_.CURCOMP__V5*/
						    meltfptr[4]),
				      (MELTBPARSTR_PTR ""), argtab,
				      (MELTBPARSTR_PTR ""), restab);
		      }
		      ;
		      /*^quasiblock */


		      MELT_LOCATION ("warmelt-first.melt:3556:/ checksignal");
		      MELT_CHECK_SIGNAL ();
		      ;
      /*_#NULL__L3*/ meltfnum[2] =
			(( /*_.TEST__V7*/ meltfptr[6]) == NULL);;
		      MELT_LOCATION ("warmelt-first.melt:3556:/ cond");
		      /*cond */ if ( /*_#NULL__L3*/ meltfnum[2])	/*then */
			{
			  /*^cond.then */
			  /*^block */
			  /*anyblock */
			  {

			    /*^checksignal */
			    MELT_CHECK_SIGNAL ();
			    ;
			    /*^quasiblock */


			    /*_.RETVAL___V1*/ meltfptr[0] =
			      /*_.CURCOMP__V5*/ meltfptr[4];;
			    MELT_LOCATION
			      ("warmelt-first.melt:3556:/ putxtraresult");
			    if (!meltxrestab_ || !meltxresdescr_)
			      goto labend_rout;
			    if (meltxresdescr_[0] != MELTBPAR_PTR)
			      goto labend_rout;
			    if (meltxrestab_[0].meltbp_aptr)
			      *(meltxrestab_[0].meltbp_aptr) =
				(melt_ptr_t) ( /*_.OTHER__V8*/ meltfptr[7]);
			    ;
			    /*^finalreturn */
			    ;
			    /*finalret */ goto labend_rout;
			    /*_.IF___V9*/ meltfptr[8] =
			      /*_.RETURN___V10*/ meltfptr[9];;
			    /*epilog */

			    MELT_LOCATION ("warmelt-first.melt:3556:/ clear");
		  /*clear *//*_.RETURN___V10*/ meltfptr[9] = 0;
			  }
			  ;
			}
		      else
			{	/*^cond.else */

       /*_.IF___V9*/ meltfptr[8] = NULL;;
			}
		      ;
		      /*^compute */
		      /*_.MULTI___V6*/ meltfptr[5] =
			/*_.IF___V9*/ meltfptr[8];;

		      MELT_LOCATION ("warmelt-first.melt:3553:/ clear");
		/*clear *//*_#NULL__L3*/ meltfnum[2] = 0;
		      /*^clear */
		/*clear *//*_.IF___V9*/ meltfptr[8] = 0;

		      /*^clear */
		/*clear *//*_.OTHER__V8*/ meltfptr[7] = 0;
		    }		/* end foreach_in_list meltcit1__EACHLIST */
     /*_.CURPAIR__V4*/ meltfptr[3] = NULL;
     /*_.CURCOMP__V5*/ meltfptr[4] = NULL;


		  /*citerepilog */

		  MELT_LOCATION ("warmelt-first.melt:3550:/ clear");
		/*clear *//*_.CURPAIR__V4*/ meltfptr[3] = 0;
		  /*^clear */
		/*clear *//*_.CURCOMP__V5*/ meltfptr[4] = 0;
		  /*^clear */
		/*clear *//*_.MULTI___V6*/ meltfptr[5] = 0;
		}		/*endciterblock FOREACH_IN_LIST */
		;
		/*epilog */
	      }
	      ;
	    }			/*noelse */
	  ;
	  /*epilog */

	  MELT_LOCATION ("warmelt-first.melt:3548:/ clear");
	     /*clear *//*_#IS_CLOSURE__L2*/ meltfnum[1] = 0;
	}
	;
      }				/*noelse */
    ;
    /*epilog */

    MELT_LOCATION ("warmelt-first.melt:3544:/ clear");
	   /*clear *//*_#IS_LIST__L1*/ meltfnum[0] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("LIST_ITERATE_TEST", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_42_warmelt_first_LIST_ITERATE_TEST_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_42_warmelt_first_LIST_ITERATE_TEST */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_43_warmelt_first_LIST_APPEND2LIST (meltclosure_ptr_t meltclosp_,
					    melt_ptr_t meltfirstargp_,
					    const melt_argdescr_cell_t
					    meltxargdescr_[],
					    union meltparam_un * meltxargtab_,
					    const melt_argdescr_cell_t
					    meltxresdescr_[],
					    union meltparam_un * meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_43_warmelt_first_LIST_APPEND2LIST_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_43_warmelt_first_LIST_APPEND2LIST_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 11
    melt_ptr_t mcfr_varptr[11];
#define MELTFRAM_NBVARNUM 4
    long mcfr_varnum[4];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_43_warmelt_first_LIST_APPEND2LIST is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_43_warmelt_first_LIST_APPEND2LIST_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 11; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_43_warmelt_first_LIST_APPEND2LIST nbval 11*/
  meltfram__.mcfr_nbvar = 11 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("LIST_APPEND2LIST", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-first.melt:3560:/ getarg");
 /*_.DLIST__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.SLIST__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.SLIST__V3*/ meltfptr[2])) !=
	      NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {

    MELT_LOCATION ("warmelt-first.melt:3564:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#IS_LIST__L1*/ meltfnum[0] =
      (melt_magic_discr ((melt_ptr_t) ( /*_.SLIST__V3*/ meltfptr[2])) ==
       MELTOBMAG_LIST);;
    /*^compute */
 /*_#NOT__L2*/ meltfnum[1] =
      (!( /*_#IS_LIST__L1*/ meltfnum[0]));;
    MELT_LOCATION ("warmelt-first.melt:3564:/ cond");
    /*cond */ if ( /*_#NOT__L2*/ meltfnum[1])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  /*^checksignal */
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^quasiblock */


	  /*_.RETVAL___V1*/ meltfptr[0] = /*_.DLIST__V2*/ meltfptr[1];;

	  {
	    MELT_LOCATION ("warmelt-first.melt:3564:/ locexp");
	    /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
	    if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	      melt_warn_for_no_expected_secondary_results ();
	    /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
	    ;
	  }
	  ;
	  /*^finalreturn */
	  ;
	  /*finalret */ goto labend_rout;
	  /*_.IF___V4*/ meltfptr[3] = /*_.RETURN___V5*/ meltfptr[4];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-first.melt:3564:/ clear");
	     /*clear *//*_.RETURN___V5*/ meltfptr[4] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

  /*_.IF___V4*/ meltfptr[3] = NULL;;
      }
    ;
    MELT_LOCATION ("warmelt-first.melt:3565:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#IS_LIST__L3*/ meltfnum[2] =
      (melt_magic_discr ((melt_ptr_t) ( /*_.DLIST__V2*/ meltfptr[1])) ==
       MELTOBMAG_LIST);;
    /*^compute */
 /*_#NOT__L4*/ meltfnum[3] =
      (!( /*_#IS_LIST__L3*/ meltfnum[2]));;
    MELT_LOCATION ("warmelt-first.melt:3565:/ cond");
    /*cond */ if ( /*_#NOT__L4*/ meltfnum[3])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

   /*_.MAKE_LIST__V7*/ meltfptr[6] =
	    (meltgc_new_list
	     ((meltobject_ptr_t)
	      (( /*!DISCR_LIST */ meltfrout->tabval[0]))));;
	  MELT_LOCATION ("warmelt-first.melt:3565:/ compute");
	  /*_.DLIST__V2*/ meltfptr[1] = /*_.SETQ___V8*/ meltfptr[7] =
	    /*_.MAKE_LIST__V7*/ meltfptr[6];;
	  /*_.IF___V6*/ meltfptr[4] = /*_.SETQ___V8*/ meltfptr[7];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-first.melt:3565:/ clear");
	     /*clear *//*_.MAKE_LIST__V7*/ meltfptr[6] = 0;
	  /*^clear */
	     /*clear *//*_.SETQ___V8*/ meltfptr[7] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

  /*_.IF___V6*/ meltfptr[4] = NULL;;
      }
    ;
    /*citerblock FOREACH_IN_LIST */
    {
      /* start foreach_in_list meltcit1__EACHLIST */
      for ( /*_.CURPAIR__V9*/ meltfptr[6] =
	   melt_list_first ((melt_ptr_t) /*_.SLIST__V3*/ meltfptr[2]);
	   melt_magic_discr ((melt_ptr_t) /*_.CURPAIR__V9*/ meltfptr[6]) ==
	   MELTOBMAG_PAIR;
	   /*_.CURPAIR__V9*/ meltfptr[6] =
	   melt_pair_tail ((melt_ptr_t) /*_.CURPAIR__V9*/ meltfptr[6]))
	{
	  /*_.CURCOMP__V10*/ meltfptr[7] =
	    melt_pair_head ((melt_ptr_t) /*_.CURPAIR__V9*/ meltfptr[6]);



	  {
	    MELT_LOCATION ("warmelt-first.melt:3569:/ locexp");
	    meltgc_append_list ((melt_ptr_t) ( /*_.DLIST__V2*/ meltfptr[1]),
				(melt_ptr_t) ( /*_.CURCOMP__V10*/
					      meltfptr[7]));
	  }
	  ;
	}			/* end foreach_in_list meltcit1__EACHLIST */
     /*_.CURPAIR__V9*/ meltfptr[6] = NULL;
     /*_.CURCOMP__V10*/ meltfptr[7] = NULL;


      /*citerepilog */

      MELT_LOCATION ("warmelt-first.melt:3566:/ clear");
	    /*clear *//*_.CURPAIR__V9*/ meltfptr[6] = 0;
      /*^clear */
	    /*clear *//*_.CURCOMP__V10*/ meltfptr[7] = 0;
    }				/*endciterblock FOREACH_IN_LIST */
    ;
    MELT_LOCATION ("warmelt-first.melt:3570:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.DLIST__V2*/ meltfptr[1];;

    {
      MELT_LOCATION ("warmelt-first.melt:3570:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    MELT_LOCATION ("warmelt-first.melt:3560:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.RETURN___V11*/ meltfptr[10];;

    {
      MELT_LOCATION ("warmelt-first.melt:3560:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_#IS_LIST__L1*/ meltfnum[0] = 0;
    /*^clear */
	   /*clear *//*_#NOT__L2*/ meltfnum[1] = 0;
    /*^clear */
	   /*clear *//*_.IF___V4*/ meltfptr[3] = 0;
    /*^clear */
	   /*clear *//*_#IS_LIST__L3*/ meltfnum[2] = 0;
    /*^clear */
	   /*clear *//*_#NOT__L4*/ meltfnum[3] = 0;
    /*^clear */
	   /*clear *//*_.IF___V6*/ meltfptr[4] = 0;
    /*^clear */
	   /*clear *//*_.RETURN___V11*/ meltfptr[10] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("LIST_APPEND2LIST", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_43_warmelt_first_LIST_APPEND2LIST_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_43_warmelt_first_LIST_APPEND2LIST */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_44_warmelt_first_LIST_MAP (meltclosure_ptr_t meltclosp_,
				    melt_ptr_t meltfirstargp_,
				    const melt_argdescr_cell_t
				    meltxargdescr_[],
				    union meltparam_un * meltxargtab_,
				    const melt_argdescr_cell_t
				    meltxresdescr_[],
				    union meltparam_un * meltxrestab_)
{
  long current_blocklevel_signals_meltrout_44_warmelt_first_LIST_MAP_melt =
    melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_44_warmelt_first_LIST_MAP_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 14
    melt_ptr_t mcfr_varptr[14];
#define MELTFRAM_NBVARNUM 4
    long mcfr_varnum[4];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_44_warmelt_first_LIST_MAP is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_44_warmelt_first_LIST_MAP_st *) meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 14; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_44_warmelt_first_LIST_MAP nbval 14*/
  meltfram__.mcfr_nbvar = 14 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("LIST_MAP", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-first.melt:3575:/ getarg");
 /*_.LIS__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.F__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.F__V3*/ meltfptr[2])) != NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {

    MELT_LOCATION ("warmelt-first.melt:3579:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#IS_LIST__L1*/ meltfnum[0] =
      (melt_magic_discr ((melt_ptr_t) ( /*_.LIS__V2*/ meltfptr[1])) ==
       MELTOBMAG_LIST);;
    MELT_LOCATION ("warmelt-first.melt:3579:/ cond");
    /*cond */ if ( /*_#IS_LIST__L1*/ meltfnum[0])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-first.melt:3580:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
   /*_#IS_CLOSURE__L2*/ meltfnum[1] =
	    (melt_magic_discr ((melt_ptr_t) ( /*_.F__V3*/ meltfptr[2])) ==
	     MELTOBMAG_CLOSURE);;
	  MELT_LOCATION ("warmelt-first.melt:3580:/ cond");
	  /*cond */ if ( /*_#IS_CLOSURE__L2*/ meltfnum[1])	/*then */
	    {
	      /*^cond.then */
	      /*^block */
	      /*anyblock */
	      {

		MELT_LOCATION ("warmelt-first.melt:3581:/ quasiblock");


     /*_.RESLIS__V7*/ meltfptr[6] =
		  (meltgc_new_list
		   ((meltobject_ptr_t)
		    (( /*!DISCR_LIST */ meltfrout->tabval[0]))));;
		/*^compute */
     /*_.CURPAIR__V8*/ meltfptr[7] =
		  (melt_list_first
		   ((melt_ptr_t) ( /*_.LIS__V2*/ meltfptr[1])));;
		MELT_LOCATION ("warmelt-first.melt:3583:/ loop");
		/*loop */
		{
		labloop_LISLOOP_1:;
				/*^loopbody */

		  /*^block */
		  /*anyblock */
		  {

		    /*^checksignal */
		    MELT_CHECK_SIGNAL ();
		    ;
		    MELT_LOCATION ("warmelt-first.melt:3584:/ checksignal");
		    MELT_CHECK_SIGNAL ();
		    ;
       /*_#IS_PAIR__L3*/ meltfnum[2] =
		      (melt_magic_discr
		       ((melt_ptr_t) ( /*_.CURPAIR__V8*/ meltfptr[7])) ==
		       MELTOBMAG_PAIR);;
		    /*^compute */
       /*_#NOT__L4*/ meltfnum[3] =
		      (!( /*_#IS_PAIR__L3*/ meltfnum[2]));;
		    MELT_LOCATION ("warmelt-first.melt:3584:/ cond");
		    /*cond */ if ( /*_#NOT__L4*/ meltfnum[3])	/*then */
		      {
			/*^cond.then */
			/*^block */
			/*anyblock */
			{

			  /*^quasiblock */


			  /*^compute */
			  /*_.LISLOOP__V10*/ meltfptr[9] =
			    /*_.RESLIS__V7*/ meltfptr[6];;

			  /*^exit */
			  /*exit */
			  {
			    goto labexit_LISLOOP_1;
			  }
			  ;
			  /*epilog */
			}
			;
		      }		/*noelse */
		    ;
		    MELT_LOCATION ("warmelt-first.melt:3585:/ quasiblock");


       /*_.CURELEM__V11*/ meltfptr[10] =
		      (melt_pair_head
		       ((melt_ptr_t) ( /*_.CURPAIR__V8*/ meltfptr[7])));;
		    MELT_LOCATION ("warmelt-first.melt:3586:/ checksignal");
		    MELT_CHECK_SIGNAL ();
		    ;
		    /*^apply */
		    /*apply */
		    {
		      /*_.F__V12*/ meltfptr[11] =
			melt_apply ((meltclosure_ptr_t)
				    ( /*_.F__V3*/ meltfptr[2]),
				    (melt_ptr_t) ( /*_.CURELEM__V11*/
						  meltfptr[10]), (""),
				    (union meltparam_un *) 0, "",
				    (union meltparam_un *) 0);
		    }
		    ;

		    {
		      /*^locexp */
		      meltgc_append_list ((melt_ptr_t)
					  ( /*_.RESLIS__V7*/ meltfptr[6]),
					  (melt_ptr_t) ( /*_.F__V12*/
							meltfptr[11]));
		    }
		    ;

		    MELT_LOCATION ("warmelt-first.melt:3585:/ clear");
		 /*clear *//*_.CURELEM__V11*/ meltfptr[10] = 0;
		    /*^clear */
		 /*clear *//*_.F__V12*/ meltfptr[11] = 0;
       /*_.PAIR_TAIL__V13*/ meltfptr[10] =
		      (melt_pair_tail
		       ((melt_ptr_t) ( /*_.CURPAIR__V8*/ meltfptr[7])));;
		    MELT_LOCATION ("warmelt-first.melt:3587:/ compute");
		    /*_.CURPAIR__V8*/ meltfptr[7] =
		      /*_.SETQ___V14*/ meltfptr[11] =
		      /*_.PAIR_TAIL__V13*/ meltfptr[10];;
		    MELT_LOCATION ("warmelt-first.melt:3583:/ checksignal");
		    MELT_CHECK_SIGNAL ();
		    ;
		    /*epilog */

		    /*^clear */
		 /*clear *//*_#IS_PAIR__L3*/ meltfnum[2] = 0;
		    /*^clear */
		 /*clear *//*_#NOT__L4*/ meltfnum[3] = 0;
		    /*^clear */
		 /*clear *//*_.PAIR_TAIL__V13*/ meltfptr[10] = 0;
		    /*^clear */
		 /*clear *//*_.SETQ___V14*/ meltfptr[11] = 0;
		  }
		  ;
		  ;
		  goto labloop_LISLOOP_1;
		labexit_LISLOOP_1:;
				/*^loopepilog */
		  /*loopepilog */
		  /*_.FOREVER___V9*/ meltfptr[8] =
		    /*_.LISLOOP__V10*/ meltfptr[9];;
		}
		;
		/*^compute */
		/*_.LET___V6*/ meltfptr[5] = /*_.FOREVER___V9*/ meltfptr[8];;

		MELT_LOCATION ("warmelt-first.melt:3581:/ clear");
	       /*clear *//*_.RESLIS__V7*/ meltfptr[6] = 0;
		/*^clear */
	       /*clear *//*_.CURPAIR__V8*/ meltfptr[7] = 0;
		/*^clear */
	       /*clear *//*_.FOREVER___V9*/ meltfptr[8] = 0;
		/*_.IF___V5*/ meltfptr[4] = /*_.LET___V6*/ meltfptr[5];;
		/*epilog */

		MELT_LOCATION ("warmelt-first.melt:3580:/ clear");
	       /*clear *//*_.LET___V6*/ meltfptr[5] = 0;
	      }
	      ;
	    }
	  else
	    {			/*^cond.else */

    /*_.IF___V5*/ meltfptr[4] = NULL;;
	    }
	  ;
	  /*^compute */
	  /*_.IF___V4*/ meltfptr[3] = /*_.IF___V5*/ meltfptr[4];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-first.melt:3579:/ clear");
	     /*clear *//*_#IS_CLOSURE__L2*/ meltfnum[1] = 0;
	  /*^clear */
	     /*clear *//*_.IF___V5*/ meltfptr[4] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

  /*_.IF___V4*/ meltfptr[3] = NULL;;
      }
    ;
    MELT_LOCATION ("warmelt-first.melt:3575:/ quasiblock");


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.IF___V4*/ meltfptr[3];;

    {
      MELT_LOCATION ("warmelt-first.melt:3575:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_#IS_LIST__L1*/ meltfnum[0] = 0;
    /*^clear */
	   /*clear *//*_.IF___V4*/ meltfptr[3] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("LIST_MAP", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_44_warmelt_first_LIST_MAP_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_44_warmelt_first_LIST_MAP */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_45_warmelt_first_LIST_FIND (meltclosure_ptr_t meltclosp_,
				     melt_ptr_t meltfirstargp_,
				     const melt_argdescr_cell_t
				     meltxargdescr_[],
				     union meltparam_un * meltxargtab_,
				     const melt_argdescr_cell_t
				     meltxresdescr_[],
				     union meltparam_un * meltxrestab_)
{
  long current_blocklevel_signals_meltrout_45_warmelt_first_LIST_FIND_melt =
    melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_45_warmelt_first_LIST_FIND_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 25
    melt_ptr_t mcfr_varptr[25];
#define MELTFRAM_NBVARNUM 7
    long mcfr_varnum[7];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_45_warmelt_first_LIST_FIND is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_45_warmelt_first_LIST_FIND_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 25; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_45_warmelt_first_LIST_FIND nbval 25*/
  meltfram__.mcfr_nbvar = 25 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("LIST_FIND", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-first.melt:3592:/ getarg");
 /*_.LIS__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.X__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.X__V3*/ meltfptr[2])) != NULL);


  /*getarg#2 */
  /*^getarg */
  if (meltxargdescr_[1] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.F__V4*/ meltfptr[3] =
    (meltxargtab_[1].meltbp_aptr) ? (*(meltxargtab_[1].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.F__V4*/ meltfptr[3])) != NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {

    MELT_LOCATION ("warmelt-first.melt:3595:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#IS_LIST__L1*/ meltfnum[0] =
      (melt_magic_discr ((melt_ptr_t) ( /*_.LIS__V2*/ meltfptr[1])) ==
       MELTOBMAG_LIST);;
    MELT_LOCATION ("warmelt-first.melt:3595:/ cond");
    /*cond */ if ( /*_#IS_LIST__L1*/ meltfnum[0])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-first.melt:3596:/ quasiblock");


   /*_.CURPAIR__V7*/ meltfptr[6] =
	    (melt_list_first ((melt_ptr_t) ( /*_.LIS__V2*/ meltfptr[1])));;
	  MELT_LOCATION ("warmelt-first.melt:3597:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
   /*_#IS_CLOSURE__L2*/ meltfnum[1] =
	    (melt_magic_discr ((melt_ptr_t) ( /*_.F__V4*/ meltfptr[3])) ==
	     MELTOBMAG_CLOSURE);;
	  MELT_LOCATION ("warmelt-first.melt:3597:/ cond");
	  /*cond */ if ( /*_#IS_CLOSURE__L2*/ meltfnum[1])	/*then */
	    {
	      /*^cond.then */
	      /*^block */
	      /*anyblock */
	      {

		MELT_LOCATION ("warmelt-first.melt:3598:/ loop");
		/*loop */
		{
		labloop_LISLOOP_2:;
				/*^loopbody */

		  /*^block */
		  /*anyblock */
		  {

		    /*^checksignal */
		    MELT_CHECK_SIGNAL ();
		    ;
		    MELT_LOCATION ("warmelt-first.melt:3599:/ checksignal");
		    MELT_CHECK_SIGNAL ();
		    ;
       /*_#IS_PAIR__L3*/ meltfnum[2] =
		      (melt_magic_discr
		       ((melt_ptr_t) ( /*_.CURPAIR__V7*/ meltfptr[6])) ==
		       MELTOBMAG_PAIR);;
		    /*^compute */
       /*_#NOT__L4*/ meltfnum[3] =
		      (!( /*_#IS_PAIR__L3*/ meltfnum[2]));;
		    MELT_LOCATION ("warmelt-first.melt:3599:/ cond");
		    /*cond */ if ( /*_#NOT__L4*/ meltfnum[3])	/*then */
		      {
			/*^cond.then */
			/*^block */
			/*anyblock */
			{

			  /*^quasiblock */


			  /*^compute */
	 /*_.LISLOOP__V10*/ meltfptr[9] = NULL;;

			  /*^exit */
			  /*exit */
			  {
			    goto labexit_LISLOOP_2;
			  }
			  ;
			  /*epilog */
			}
			;
		      }		/*noelse */
		    ;
		    MELT_LOCATION ("warmelt-first.melt:3600:/ quasiblock");


       /*_.CURELEM__V12*/ meltfptr[11] =
		      (melt_pair_head
		       ((melt_ptr_t) ( /*_.CURPAIR__V7*/ meltfptr[6])));;
		    MELT_LOCATION ("warmelt-first.melt:3601:/ checksignal");
		    MELT_CHECK_SIGNAL ();
		    ;
		    /*^apply */
		    /*apply */
		    {
		      union meltparam_un argtab[1];
		      memset (&argtab, 0, sizeof (argtab));
		      /*^apply.arg */
		      argtab[0].meltbp_aptr =
			(melt_ptr_t *) & /*_.X__V3*/ meltfptr[2];
		      /*_.T__V13*/ meltfptr[12] =
			melt_apply ((meltclosure_ptr_t)
				    ( /*_.F__V4*/ meltfptr[3]),
				    (melt_ptr_t) ( /*_.CURELEM__V12*/
						  meltfptr[11]),
				    (MELTBPARSTR_PTR ""), argtab, "",
				    (union meltparam_un *) 0);
		    }
		    ;
		    MELT_LOCATION ("warmelt-first.melt:3602:/ checksignal");
		    MELT_CHECK_SIGNAL ();
		    ;
		    /*^cond */
		    /*cond */ if ( /*_.T__V13*/ meltfptr[12])	/*then */
		      {
			/*^cond.then */
			/*^block */
			/*anyblock */
			{

			  /*^checksignal */
			  MELT_CHECK_SIGNAL ();
			  ;
			  /*^quasiblock */


			  /*_.RETVAL___V1*/ meltfptr[0] =
			    /*_.T__V13*/ meltfptr[12];;

			  {
			    MELT_LOCATION
			      ("warmelt-first.melt:3602:/ locexp");
			    /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
			    if (meltxresdescr_ && meltxresdescr_[0]
				&& meltxrestab_)
			      melt_warn_for_no_expected_secondary_results ();
			    /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
			    ;
			  }
			  ;
			  /*^finalreturn */
			  ;
			  /*finalret */ goto labend_rout;
			  /*_.IF___V14*/ meltfptr[13] =
			    /*_.RETURN___V15*/ meltfptr[14];;
			  /*epilog */

			  MELT_LOCATION ("warmelt-first.melt:3602:/ clear");
		   /*clear *//*_.RETURN___V15*/ meltfptr[14] = 0;
			}
			;
		      }
		    else
		      {		/*^cond.else */

	/*_.IF___V14*/ meltfptr[13] = NULL;;
		      }
		    ;
		    /*^compute */
       /*_.PAIR_TAIL__V16*/ meltfptr[14] =
		      (melt_pair_tail
		       ((melt_ptr_t) ( /*_.CURPAIR__V7*/ meltfptr[6])));;
		    MELT_LOCATION ("warmelt-first.melt:3603:/ compute");
		    /*_.CURPAIR__V7*/ meltfptr[6] =
		      /*_.SETQ___V17*/ meltfptr[16] =
		      /*_.PAIR_TAIL__V16*/ meltfptr[14];;
		    /*_.LET___V11*/ meltfptr[10] =
		      /*_.SETQ___V17*/ meltfptr[16];;

		    MELT_LOCATION ("warmelt-first.melt:3600:/ clear");
		 /*clear *//*_.CURELEM__V12*/ meltfptr[11] = 0;
		    /*^clear */
		 /*clear *//*_.T__V13*/ meltfptr[12] = 0;
		    /*^clear */
		 /*clear *//*_.IF___V14*/ meltfptr[13] = 0;
		    /*^clear */
		 /*clear *//*_.PAIR_TAIL__V16*/ meltfptr[14] = 0;
		    /*^clear */
		 /*clear *//*_.SETQ___V17*/ meltfptr[16] = 0;
		    MELT_LOCATION ("warmelt-first.melt:3598:/ checksignal");
		    MELT_CHECK_SIGNAL ();
		    ;
		    /*epilog */

		    /*^clear */
		 /*clear *//*_#IS_PAIR__L3*/ meltfnum[2] = 0;
		    /*^clear */
		 /*clear *//*_#NOT__L4*/ meltfnum[3] = 0;
		    /*^clear */
		 /*clear *//*_.LET___V11*/ meltfptr[10] = 0;
		  }
		  ;
		  ;
		  goto labloop_LISLOOP_2;
		labexit_LISLOOP_2:;
				/*^loopepilog */
		  /*loopepilog */
		  /*_.FOREVER___V9*/ meltfptr[8] =
		    /*_.LISLOOP__V10*/ meltfptr[9];;
		}
		;
		/*^compute */
		/*_.IFELSE___V8*/ meltfptr[7] =
		  /*_.FOREVER___V9*/ meltfptr[8];;
		/*epilog */

		MELT_LOCATION ("warmelt-first.melt:3597:/ clear");
	       /*clear *//*_.FOREVER___V9*/ meltfptr[8] = 0;
	      }
	      ;
	    }
	  else
	    {			/*^cond.else */

	      /*^block */
	      /*anyblock */
	      {

		MELT_LOCATION ("warmelt-first.melt:3605:/ loop");
		/*loop */
		{
		labloop_MEMLOOP_1:;
				/*^loopbody */

		  /*^block */
		  /*anyblock */
		  {

		    /*^checksignal */
		    MELT_CHECK_SIGNAL ();
		    ;
		    MELT_LOCATION ("warmelt-first.melt:3606:/ checksignal");
		    MELT_CHECK_SIGNAL ();
		    ;
       /*_#IS_PAIR__L5*/ meltfnum[2] =
		      (melt_magic_discr
		       ((melt_ptr_t) ( /*_.CURPAIR__V7*/ meltfptr[6])) ==
		       MELTOBMAG_PAIR);;
		    /*^compute */
       /*_#NOT__L6*/ meltfnum[3] =
		      (!( /*_#IS_PAIR__L5*/ meltfnum[2]));;
		    MELT_LOCATION ("warmelt-first.melt:3606:/ cond");
		    /*cond */ if ( /*_#NOT__L6*/ meltfnum[3])	/*then */
		      {
			/*^cond.then */
			/*^block */
			/*anyblock */
			{

			  /*^quasiblock */


			  /*^compute */
	 /*_.MEMLOOP__V19*/ meltfptr[12] = NULL;;

			  /*^exit */
			  /*exit */
			  {
			    goto labexit_MEMLOOP_1;
			  }
			  ;
			  /*epilog */
			}
			;
		      }		/*noelse */
		    ;
		    MELT_LOCATION ("warmelt-first.melt:3607:/ quasiblock");


       /*_.CURELEM__V21*/ meltfptr[14] =
		      (melt_pair_head
		       ((melt_ptr_t) ( /*_.CURPAIR__V7*/ meltfptr[6])));;
		    MELT_LOCATION ("warmelt-first.melt:3608:/ checksignal");
		    MELT_CHECK_SIGNAL ();
		    ;
       /*_#__L7*/ meltfnum[6] =
		      (( /*_.CURELEM__V21*/ meltfptr[14]) ==
		       ( /*_.X__V3*/ meltfptr[2]));;
		    MELT_LOCATION ("warmelt-first.melt:3608:/ cond");
		    /*cond */ if ( /*_#__L7*/ meltfnum[6])	/*then */
		      {
			/*^cond.then */
			/*^block */
			/*anyblock */
			{

			  /*^checksignal */
			  MELT_CHECK_SIGNAL ();
			  ;
			  /*^quasiblock */


			  /*_.RETVAL___V1*/ meltfptr[0] =
			    /*_.CURELEM__V21*/ meltfptr[14];;

			  {
			    MELT_LOCATION
			      ("warmelt-first.melt:3608:/ locexp");
			    /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
			    if (meltxresdescr_ && meltxresdescr_[0]
				&& meltxrestab_)
			      melt_warn_for_no_expected_secondary_results ();
			    /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
			    ;
			  }
			  ;
			  /*^finalreturn */
			  ;
			  /*finalret */ goto labend_rout;
			  /*_.IF___V22*/ meltfptr[16] =
			    /*_.RETURN___V23*/ meltfptr[10];;
			  /*epilog */

			  MELT_LOCATION ("warmelt-first.melt:3608:/ clear");
		   /*clear *//*_.RETURN___V23*/ meltfptr[10] = 0;
			}
			;
		      }
		    else
		      {		/*^cond.else */

	/*_.IF___V22*/ meltfptr[16] = NULL;;
		      }
		    ;
		    /*^compute */
       /*_.PAIR_TAIL__V24*/ meltfptr[8] =
		      (melt_pair_tail
		       ((melt_ptr_t) ( /*_.CURPAIR__V7*/ meltfptr[6])));;
		    MELT_LOCATION ("warmelt-first.melt:3609:/ compute");
		    /*_.CURPAIR__V7*/ meltfptr[6] =
		      /*_.SETQ___V25*/ meltfptr[10] =
		      /*_.PAIR_TAIL__V24*/ meltfptr[8];;
		    /*_.LET___V20*/ meltfptr[13] =
		      /*_.SETQ___V25*/ meltfptr[10];;

		    MELT_LOCATION ("warmelt-first.melt:3607:/ clear");
		 /*clear *//*_.CURELEM__V21*/ meltfptr[14] = 0;
		    /*^clear */
		 /*clear *//*_#__L7*/ meltfnum[6] = 0;
		    /*^clear */
		 /*clear *//*_.IF___V22*/ meltfptr[16] = 0;
		    /*^clear */
		 /*clear *//*_.PAIR_TAIL__V24*/ meltfptr[8] = 0;
		    /*^clear */
		 /*clear *//*_.SETQ___V25*/ meltfptr[10] = 0;
		    MELT_LOCATION ("warmelt-first.melt:3605:/ checksignal");
		    MELT_CHECK_SIGNAL ();
		    ;
		    /*epilog */

		    /*^clear */
		 /*clear *//*_#IS_PAIR__L5*/ meltfnum[2] = 0;
		    /*^clear */
		 /*clear *//*_#NOT__L6*/ meltfnum[3] = 0;
		    /*^clear */
		 /*clear *//*_.LET___V20*/ meltfptr[13] = 0;
		  }
		  ;
		  ;
		  goto labloop_MEMLOOP_1;
		labexit_MEMLOOP_1:;
				/*^loopepilog */
		  /*loopepilog */
		  /*_.FOREVER___V18*/ meltfptr[11] =
		    /*_.MEMLOOP__V19*/ meltfptr[12];;
		}
		;
		/*^compute */
		/*_.IFELSE___V8*/ meltfptr[7] =
		  /*_.FOREVER___V18*/ meltfptr[11];;
		/*epilog */

		MELT_LOCATION ("warmelt-first.melt:3597:/ clear");
	       /*clear *//*_.FOREVER___V18*/ meltfptr[11] = 0;
	      }
	      ;
	    }
	  ;
	  /*_.LET___V6*/ meltfptr[5] = /*_.IFELSE___V8*/ meltfptr[7];;

	  MELT_LOCATION ("warmelt-first.melt:3596:/ clear");
	     /*clear *//*_.CURPAIR__V7*/ meltfptr[6] = 0;
	  /*^clear */
	     /*clear *//*_#IS_CLOSURE__L2*/ meltfnum[1] = 0;
	  /*^clear */
	     /*clear *//*_.IFELSE___V8*/ meltfptr[7] = 0;
	  /*_.IF___V5*/ meltfptr[4] = /*_.LET___V6*/ meltfptr[5];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-first.melt:3595:/ clear");
	     /*clear *//*_.LET___V6*/ meltfptr[5] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

  /*_.IF___V5*/ meltfptr[4] = NULL;;
      }
    ;
    MELT_LOCATION ("warmelt-first.melt:3592:/ quasiblock");


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.IF___V5*/ meltfptr[4];;

    {
      MELT_LOCATION ("warmelt-first.melt:3592:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_#IS_LIST__L1*/ meltfnum[0] = 0;
    /*^clear */
	   /*clear *//*_.IF___V5*/ meltfptr[4] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("LIST_FIND", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_45_warmelt_first_LIST_FIND_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_45_warmelt_first_LIST_FIND */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_46_warmelt_first_LIST_TO_MULTIPLE (meltclosure_ptr_t meltclosp_,
					    melt_ptr_t meltfirstargp_,
					    const melt_argdescr_cell_t
					    meltxargdescr_[],
					    union meltparam_un * meltxargtab_,
					    const melt_argdescr_cell_t
					    meltxresdescr_[],
					    union meltparam_un * meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_46_warmelt_first_LIST_TO_MULTIPLE_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_46_warmelt_first_LIST_TO_MULTIPLE_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 14
    melt_ptr_t mcfr_varptr[14];
#define MELTFRAM_NBVARNUM 3
    long mcfr_varnum[3];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_46_warmelt_first_LIST_TO_MULTIPLE is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_46_warmelt_first_LIST_TO_MULTIPLE_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 14; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_46_warmelt_first_LIST_TO_MULTIPLE nbval 14*/
  meltfram__.mcfr_nbvar = 14 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("LIST_TO_MULTIPLE", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-first.melt:3615:/ getarg");
 /*_.LIS__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.DISC__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.DISC__V3*/ meltfptr[2])) !=
	      NULL);


  /*getarg#2 */
  /*^getarg */
  if (meltxargdescr_[1] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.F__V4*/ meltfptr[3] =
    (meltxargtab_[1].meltbp_aptr) ? (*(meltxargtab_[1].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.F__V4*/ meltfptr[3])) != NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {

    MELT_LOCATION ("warmelt-first.melt:3616:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#NULL__L1*/ meltfnum[0] =
      (( /*_.DISC__V3*/ meltfptr[2]) == NULL);;
    MELT_LOCATION ("warmelt-first.melt:3616:/ cond");
    /*cond */ if ( /*_#NULL__L1*/ meltfnum[0])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  /*^compute */
	  /*_.DISC__V3*/ meltfptr[2] = /*_.SETQ___V6*/ meltfptr[5] =
	    ( /*!DISCR_MULTIPLE */ meltfrout->tabval[0]);;
	  /*_.IF___V5*/ meltfptr[4] = /*_.SETQ___V6*/ meltfptr[5];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-first.melt:3616:/ clear");
	     /*clear *//*_.SETQ___V6*/ meltfptr[5] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

  /*_.IF___V5*/ meltfptr[4] = NULL;;
      }
    ;
    MELT_LOCATION ("warmelt-first.melt:3617:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#IS_LIST__L2*/ meltfnum[1] =
      (melt_magic_discr ((melt_ptr_t) ( /*_.LIS__V2*/ meltfptr[1])) ==
       MELTOBMAG_LIST);;
    MELT_LOCATION ("warmelt-first.melt:3617:/ cond");
    /*cond */ if ( /*_#IS_LIST__L2*/ meltfnum[1])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-first.melt:3618:/ quasiblock");


   /*_#LN__L3*/ meltfnum[2] =
	    (melt_list_length ((melt_ptr_t) ( /*_.LIS__V2*/ meltfptr[1])));;
	  /*^compute */
   /*_.TUP__V9*/ meltfptr[8] =
	    (meltgc_new_multiple
	     ((meltobject_ptr_t) ( /*_.DISC__V3*/ meltfptr[2]),
	      ( /*_#LN__L3*/ meltfnum[2])));;
	  /*^compute */
   /*_.IXB__V10*/ meltfptr[9] =
	    (meltgc_new_int
	     ((meltobject_ptr_t)
	      (( /*!DISCR_INTEGER */ meltfrout->tabval[1])), (0)));;
	  /*^compute */
   /*_.CURPAIR__V11*/ meltfptr[10] =
	    (melt_list_first ((melt_ptr_t) ( /*_.LIS__V2*/ meltfptr[1])));;
	  MELT_LOCATION ("warmelt-first.melt:3624:/ quasiblock");


	  /*^newclosure */
		   /*newclosure *//*_.LAMBDA___V13*/ meltfptr[12] =
	    (melt_ptr_t)
	    meltgc_new_closure ((meltobject_ptr_t)
				(((melt_ptr_t)
				  (MELT_PREDEF (DISCR_CLOSURE)))),
				(meltroutine_ptr_t) (( /*!konst_3 */
						      meltfrout->tabval[3])),
				(3));
	  ;
	  /*^putclosedv */
	  /*putclosv */
	  melt_assertmsg ("putclosv checkclo",
			  melt_magic_discr ((melt_ptr_t)
					    ( /*_.LAMBDA___V13*/
					     meltfptr[12])) ==
			  MELTOBMAG_CLOSURE);
	  melt_assertmsg ("putclosv checkoff", 0 >= 0
			  && 0 <
			  melt_closure_size ((melt_ptr_t)
					     ( /*_.LAMBDA___V13*/
					      meltfptr[12])));
	  ((meltclosure_ptr_t) /*_.LAMBDA___V13*/ meltfptr[12])->tabval[0] =
	    (melt_ptr_t) ( /*_.IXB__V10*/ meltfptr[9]);
	  ;
	  /*^putclosedv */
	  /*putclosv */
	  melt_assertmsg ("putclosv checkclo",
			  melt_magic_discr ((melt_ptr_t)
					    ( /*_.LAMBDA___V13*/
					     meltfptr[12])) ==
			  MELTOBMAG_CLOSURE);
	  melt_assertmsg ("putclosv checkoff", 1 >= 0
			  && 1 <
			  melt_closure_size ((melt_ptr_t)
					     ( /*_.LAMBDA___V13*/
					      meltfptr[12])));
	  ((meltclosure_ptr_t) /*_.LAMBDA___V13*/ meltfptr[12])->tabval[1] =
	    (melt_ptr_t) ( /*_.F__V4*/ meltfptr[3]);
	  ;
	  /*^putclosedv */
	  /*putclosv */
	  melt_assertmsg ("putclosv checkclo",
			  melt_magic_discr ((melt_ptr_t)
					    ( /*_.LAMBDA___V13*/
					     meltfptr[12])) ==
			  MELTOBMAG_CLOSURE);
	  melt_assertmsg ("putclosv checkoff", 2 >= 0
			  && 2 <
			  melt_closure_size ((melt_ptr_t)
					     ( /*_.LAMBDA___V13*/
					      meltfptr[12])));
	  ((meltclosure_ptr_t) /*_.LAMBDA___V13*/ meltfptr[12])->tabval[2] =
	    (melt_ptr_t) ( /*_.TUP__V9*/ meltfptr[8]);
	  ;
	  /*_.LAMBDA___V12*/ meltfptr[11] = /*_.LAMBDA___V13*/ meltfptr[12];;
	  MELT_LOCATION ("warmelt-first.melt:3622:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^apply */
	  /*apply */
	  {
	    union meltparam_un argtab[1];
	    memset (&argtab, 0, sizeof (argtab));
	    /*^apply.arg */
	    argtab[0].meltbp_aptr =
	      (melt_ptr_t *) & /*_.LAMBDA___V12*/ meltfptr[11];
	    /*_.LIST_EVERY__V14*/ meltfptr[13] =
	      melt_apply ((meltclosure_ptr_t)
			  (( /*!LIST_EVERY */ meltfrout->tabval[2])),
			  (melt_ptr_t) ( /*_.LIS__V2*/ meltfptr[1]),
			  (MELTBPARSTR_PTR ""), argtab, "",
			  (union meltparam_un *) 0);
	  }
	  ;
	  /*_.LET___V8*/ meltfptr[7] = /*_.TUP__V9*/ meltfptr[8];;

	  MELT_LOCATION ("warmelt-first.melt:3618:/ clear");
	     /*clear *//*_#LN__L3*/ meltfnum[2] = 0;
	  /*^clear */
	     /*clear *//*_.TUP__V9*/ meltfptr[8] = 0;
	  /*^clear */
	     /*clear *//*_.IXB__V10*/ meltfptr[9] = 0;
	  /*^clear */
	     /*clear *//*_.CURPAIR__V11*/ meltfptr[10] = 0;
	  /*^clear */
	     /*clear *//*_.LAMBDA___V12*/ meltfptr[11] = 0;
	  /*^clear */
	     /*clear *//*_.LIST_EVERY__V14*/ meltfptr[13] = 0;
	  /*_.IF___V7*/ meltfptr[5] = /*_.LET___V8*/ meltfptr[7];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-first.melt:3617:/ clear");
	     /*clear *//*_.LET___V8*/ meltfptr[7] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

  /*_.IF___V7*/ meltfptr[5] = NULL;;
      }
    ;
    MELT_LOCATION ("warmelt-first.melt:3615:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.IF___V7*/ meltfptr[5];;

    {
      MELT_LOCATION ("warmelt-first.melt:3615:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_#NULL__L1*/ meltfnum[0] = 0;
    /*^clear */
	   /*clear *//*_.IF___V5*/ meltfptr[4] = 0;
    /*^clear */
	   /*clear *//*_#IS_LIST__L2*/ meltfnum[1] = 0;
    /*^clear */
	   /*clear *//*_.IF___V7*/ meltfptr[5] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("LIST_TO_MULTIPLE", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_46_warmelt_first_LIST_TO_MULTIPLE_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_46_warmelt_first_LIST_TO_MULTIPLE */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_47_warmelt_first_LAMBDA___11__ (meltclosure_ptr_t meltclosp_,
					 melt_ptr_t meltfirstargp_,
					 const melt_argdescr_cell_t
					 meltxargdescr_[],
					 union meltparam_un * meltxargtab_,
					 const melt_argdescr_cell_t
					 meltxresdescr_[],
					 union meltparam_un * meltxrestab_)
{
  long current_blocklevel_signals_meltrout_47_warmelt_first_LAMBDA___11___melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_47_warmelt_first_LAMBDA___11___st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 4
    melt_ptr_t mcfr_varptr[4];
#define MELTFRAM_NBVARNUM 3
    long mcfr_varnum[3];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_47_warmelt_first_LAMBDA___11__ is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_47_warmelt_first_LAMBDA___11___st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 4; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_47_warmelt_first_LAMBDA___11__ nbval 4*/
  meltfram__.mcfr_nbvar = 4 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("LAMBDA_", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-first.melt:3624:/ getarg");
 /*_.C__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {

    MELT_LOCATION ("warmelt-first.melt:3625:/ quasiblock");


 /*_#IX__L1*/ meltfnum[0] =
      (melt_get_int ((melt_ptr_t) (( /*~IXB */ meltfclos->tabval[0]))));;
    MELT_LOCATION ("warmelt-first.melt:3626:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#IS_CLOSURE__L2*/ meltfnum[1] =
      (melt_magic_discr ((melt_ptr_t) (( /*~F */ meltfclos->tabval[1]))) ==
       MELTOBMAG_CLOSURE);;
    MELT_LOCATION ("warmelt-first.melt:3626:/ cond");
    /*cond */ if ( /*_#IS_CLOSURE__L2*/ meltfnum[1])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  /*^checksignal */
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^apply */
	  /*apply */
	  {
	    /*_.F__V4*/ meltfptr[3] =
	      melt_apply ((meltclosure_ptr_t)
			  (( /*~F */ meltfclos->tabval[1])),
			  (melt_ptr_t) ( /*_.C__V2*/ meltfptr[1]), (""),
			  (union meltparam_un *) 0, "",
			  (union meltparam_un *) 0);
	  }
	  ;
	  /*_.TC__V3*/ meltfptr[2] = /*_.F__V4*/ meltfptr[3];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-first.melt:3626:/ clear");
	     /*clear *//*_.F__V4*/ meltfptr[3] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

	/*_.TC__V3*/ meltfptr[2] = /*_.C__V2*/ meltfptr[1];;
      }
    ;
    /*^compute */
 /*_#I__L3*/ meltfnum[2] =
      (( /*_#IX__L1*/ meltfnum[0]) + (1));;

    {
      MELT_LOCATION ("warmelt-first.melt:3628:/ locexp");
      melt_put_int ((melt_ptr_t) (( /*~IXB */ meltfclos->tabval[0])),
		    ( /*_#I__L3*/ meltfnum[2]));
    }
    ;

    {
      MELT_LOCATION ("warmelt-first.melt:3629:/ locexp");
      meltgc_multiple_put_nth ((melt_ptr_t)
			       (( /*~TUP */ meltfclos->tabval[2])),
			       ( /*_#IX__L1*/ meltfnum[0]),
			       (melt_ptr_t) ( /*_.TC__V3*/ meltfptr[2]));
    }
    ;

    MELT_LOCATION ("warmelt-first.melt:3625:/ clear");
	   /*clear *//*_#IX__L1*/ meltfnum[0] = 0;
    /*^clear */
	   /*clear *//*_#IS_CLOSURE__L2*/ meltfnum[1] = 0;
    /*^clear */
	   /*clear *//*_.TC__V3*/ meltfptr[2] = 0;
    /*^clear */
	   /*clear *//*_#I__L3*/ meltfnum[2] = 0;
    /*epilog */
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("LAMBDA_", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_47_warmelt_first_LAMBDA___11___melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*noretval */ NULL);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_47_warmelt_first_LAMBDA___11__ */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_48_warmelt_first_PAIRLIST_TO_MULTIPLE (meltclosure_ptr_t meltclosp_,
						melt_ptr_t meltfirstargp_,
						const melt_argdescr_cell_t
						meltxargdescr_[],
						union meltparam_un *
						meltxargtab_,
						const melt_argdescr_cell_t
						meltxresdescr_[],
						union meltparam_un *
						meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_48_warmelt_first_PAIRLIST_TO_MULTIPLE_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_48_warmelt_first_PAIRLIST_TO_MULTIPLE_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 25
    melt_ptr_t mcfr_varptr[25];
#define MELTFRAM_NBVARNUM 12
    long mcfr_varnum[12];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_48_warmelt_first_PAIRLIST_TO_MULTIPLE is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_48_warmelt_first_PAIRLIST_TO_MULTIPLE_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 25; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_48_warmelt_first_PAIRLIST_TO_MULTIPLE nbval 25*/
  meltfram__.mcfr_nbvar = 25 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("PAIRLIST_TO_MULTIPLE", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-first.melt:3635:/ getarg");
 /*_.PAIR__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.DISC__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.DISC__V3*/ meltfptr[2])) !=
	      NULL);


  /*getarg#2 */
  /*^getarg */
  if (meltxargdescr_[1] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.F__V4*/ meltfptr[3] =
    (meltxargtab_[1].meltbp_aptr) ? (*(meltxargtab_[1].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.F__V4*/ meltfptr[3])) != NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {

    MELT_LOCATION ("warmelt-first.melt:3641:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#NULL__L1*/ meltfnum[0] =
      (( /*_.DISC__V3*/ meltfptr[2]) == NULL);;
    MELT_LOCATION ("warmelt-first.melt:3641:/ cond");
    /*cond */ if ( /*_#NULL__L1*/ meltfnum[0])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  /*^compute */
	  /*_.DISC__V3*/ meltfptr[2] = /*_.SETQ___V6*/ meltfptr[5] =
	    ( /*!DISCR_MULTIPLE */ meltfrout->tabval[0]);;
	  /*_.IF___V5*/ meltfptr[4] = /*_.SETQ___V6*/ meltfptr[5];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-first.melt:3641:/ clear");
	     /*clear *//*_.SETQ___V6*/ meltfptr[5] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

  /*_.IF___V5*/ meltfptr[4] = NULL;;
      }
    ;
    MELT_LOCATION ("warmelt-first.melt:3642:/ quasiblock");


 /*_#LN__L2*/ meltfnum[1] = 0;;
    MELT_LOCATION ("warmelt-first.melt:3643:/ quasiblock");


    /*_.CURPAIR__V9*/ meltfptr[8] = /*_.PAIR__V2*/ meltfptr[1];;
    MELT_LOCATION ("warmelt-first.melt:3644:/ loop");
    /*loop */
    {
    labloop_LOOPLN_1:;		/*^loopbody */

      /*^block */
      /*anyblock */
      {

	/*^checksignal */
	MELT_CHECK_SIGNAL ();
	;
	MELT_LOCATION ("warmelt-first.melt:3645:/ checksignal");
	MELT_CHECK_SIGNAL ();
	;
   /*_#IS_PAIR__L3*/ meltfnum[2] =
	  (melt_magic_discr ((melt_ptr_t) ( /*_.CURPAIR__V9*/ meltfptr[8])) ==
	   MELTOBMAG_PAIR);;
	/*^compute */
   /*_#NOT__L4*/ meltfnum[3] =
	  (!( /*_#IS_PAIR__L3*/ meltfnum[2]));;
	MELT_LOCATION ("warmelt-first.melt:3645:/ cond");
	/*cond */ if ( /*_#NOT__L4*/ meltfnum[3])	/*then */
	  {
	    /*^cond.then */
	    /*^block */
	    /*anyblock */
	    {

	      /*^quasiblock */


	      /*^compute */
     /*_.LOOPLN__V11*/ meltfptr[10] = NULL;;

	      /*^exit */
	      /*exit */
	      {
		goto labexit_LOOPLN_1;
	      }
	      ;
	      /*epilog */
	    }
	    ;
	  }			/*noelse */
	;
   /*_#I__L5*/ meltfnum[4] =
	  (( /*_#LN__L2*/ meltfnum[1]) + (1));;
	MELT_LOCATION ("warmelt-first.melt:3646:/ compute");
	/*_#LN__L2*/ meltfnum[1] = /*_#SETQ___L6*/ meltfnum[5] =
	  /*_#I__L5*/ meltfnum[4];;
   /*_.PAIR_TAIL__V12*/ meltfptr[11] =
	  (melt_pair_tail ((melt_ptr_t) ( /*_.CURPAIR__V9*/ meltfptr[8])));;
	MELT_LOCATION ("warmelt-first.melt:3647:/ compute");
	/*_.CURPAIR__V9*/ meltfptr[8] = /*_.SETQ___V13*/ meltfptr[12] =
	  /*_.PAIR_TAIL__V12*/ meltfptr[11];;
	MELT_LOCATION ("warmelt-first.melt:3644:/ checksignal");
	MELT_CHECK_SIGNAL ();
	;
	/*epilog */

	/*^clear */
	     /*clear *//*_#IS_PAIR__L3*/ meltfnum[2] = 0;
	/*^clear */
	     /*clear *//*_#NOT__L4*/ meltfnum[3] = 0;
	/*^clear */
	     /*clear *//*_#I__L5*/ meltfnum[4] = 0;
	/*^clear */
	     /*clear *//*_#SETQ___L6*/ meltfnum[5] = 0;
	/*^clear */
	     /*clear *//*_.PAIR_TAIL__V12*/ meltfptr[11] = 0;
	/*^clear */
	     /*clear *//*_.SETQ___V13*/ meltfptr[12] = 0;
      }
      ;
      ;
      goto labloop_LOOPLN_1;
    labexit_LOOPLN_1:;		/*^loopepilog */
      /*loopepilog */
      /*_.FOREVER___V10*/ meltfptr[9] = /*_.LOOPLN__V11*/ meltfptr[10];;
    }
    ;
    /*^compute */
    /*_.LET___V8*/ meltfptr[7] = /*_.FOREVER___V10*/ meltfptr[9];;

    MELT_LOCATION ("warmelt-first.melt:3643:/ clear");
	   /*clear *//*_.CURPAIR__V9*/ meltfptr[8] = 0;
    /*^clear */
	   /*clear *//*_.FOREVER___V10*/ meltfptr[9] = 0;
    MELT_LOCATION ("warmelt-first.melt:3648:/ quasiblock");


 /*_.TUP__V15*/ meltfptr[12] =
      (meltgc_new_multiple
       ((meltobject_ptr_t) ( /*_.DISC__V3*/ meltfptr[2]),
	( /*_#LN__L2*/ meltfnum[1])));;
    /*^compute */
 /*_#IX__L7*/ meltfnum[2] = 0;;
    /*^compute */
    /*_.CURPAIR__V16*/ meltfptr[8] = /*_.PAIR__V2*/ meltfptr[1];;
    MELT_LOCATION ("warmelt-first.melt:3652:/ loop");
    /*loop */
    {
    labloop_LOOPFI_1:;		/*^loopbody */

      /*^block */
      /*anyblock */
      {

	/*^checksignal */
	MELT_CHECK_SIGNAL ();
	;
	MELT_LOCATION ("warmelt-first.melt:3653:/ checksignal");
	MELT_CHECK_SIGNAL ();
	;
   /*_#IS_PAIR__L8*/ meltfnum[3] =
	  (melt_magic_discr ((melt_ptr_t) ( /*_.CURPAIR__V16*/ meltfptr[8]))
	   == MELTOBMAG_PAIR);;
	/*^compute */
   /*_#NOT__L9*/ meltfnum[4] =
	  (!( /*_#IS_PAIR__L8*/ meltfnum[3]));;
	MELT_LOCATION ("warmelt-first.melt:3653:/ cond");
	/*cond */ if ( /*_#NOT__L9*/ meltfnum[4])	/*then */
	  {
	    /*^cond.then */
	    /*^block */
	    /*anyblock */
	    {

	      /*^quasiblock */


	      /*^compute */
     /*_.LOOPFI__V18*/ meltfptr[17] = NULL;;

	      /*^exit */
	      /*exit */
	      {
		goto labexit_LOOPFI_1;
	      }
	      ;
	      /*epilog */
	    }
	    ;
	  }			/*noelse */
	;
	MELT_LOCATION ("warmelt-first.melt:3654:/ quasiblock");


   /*_.C__V20*/ meltfptr[19] =
	  (melt_pair_head ((melt_ptr_t) ( /*_.CURPAIR__V16*/ meltfptr[8])));;
	MELT_LOCATION ("warmelt-first.melt:3655:/ checksignal");
	MELT_CHECK_SIGNAL ();
	;
   /*_#IS_CLOSURE__L10*/ meltfnum[5] =
	  (melt_magic_discr ((melt_ptr_t) ( /*_.F__V4*/ meltfptr[3])) ==
	   MELTOBMAG_CLOSURE);;
	MELT_LOCATION ("warmelt-first.melt:3655:/ cond");
	/*cond */ if ( /*_#IS_CLOSURE__L10*/ meltfnum[5])	/*then */
	  {
	    /*^cond.then */
	    /*^block */
	    /*anyblock */
	    {

	      /*^checksignal */
	      MELT_CHECK_SIGNAL ();
	      ;
	      /*^apply */
	      /*apply */
	      {
		/*_.F__V22*/ meltfptr[21] =
		  melt_apply ((meltclosure_ptr_t) ( /*_.F__V4*/ meltfptr[3]),
			      (melt_ptr_t) ( /*_.C__V20*/ meltfptr[19]), (""),
			      (union meltparam_un *) 0, "",
			      (union meltparam_un *) 0);
	      }
	      ;
	      /*_.TC__V21*/ meltfptr[20] = /*_.F__V22*/ meltfptr[21];;
	      /*epilog */

	      MELT_LOCATION ("warmelt-first.melt:3655:/ clear");
	       /*clear *//*_.F__V22*/ meltfptr[21] = 0;
	    }
	    ;
	  }
	else
	  {			/*^cond.else */

	    /*_.TC__V21*/ meltfptr[20] = /*_.C__V20*/ meltfptr[19];;
	  }
	;

	{
	  MELT_LOCATION ("warmelt-first.melt:3656:/ locexp");
	  meltgc_multiple_put_nth ((melt_ptr_t)
				   ( /*_.TUP__V15*/ meltfptr[12]),
				   ( /*_#IX__L7*/ meltfnum[2]),
				   (melt_ptr_t) ( /*_.TC__V21*/
						 meltfptr[20]));
	}
	;
   /*_#I__L11*/ meltfnum[10] =
	  (( /*_#IX__L7*/ meltfnum[2]) + (1));;
	MELT_LOCATION ("warmelt-first.melt:3657:/ compute");
	/*_#IX__L7*/ meltfnum[2] = /*_#SETQ___L12*/ meltfnum[11] =
	  /*_#I__L11*/ meltfnum[10];;
   /*_.PAIR_TAIL__V23*/ meltfptr[21] =
	  (melt_pair_tail ((melt_ptr_t) ( /*_.CURPAIR__V16*/ meltfptr[8])));;
	MELT_LOCATION ("warmelt-first.melt:3658:/ compute");
	/*_.CURPAIR__V16*/ meltfptr[8] = /*_.SETQ___V24*/ meltfptr[23] =
	  /*_.PAIR_TAIL__V23*/ meltfptr[21];;
	/*_.LET___V19*/ meltfptr[18] = /*_.SETQ___V24*/ meltfptr[23];;

	MELT_LOCATION ("warmelt-first.melt:3654:/ clear");
	     /*clear *//*_.C__V20*/ meltfptr[19] = 0;
	/*^clear */
	     /*clear *//*_#IS_CLOSURE__L10*/ meltfnum[5] = 0;
	/*^clear */
	     /*clear *//*_.TC__V21*/ meltfptr[20] = 0;
	/*^clear */
	     /*clear *//*_#I__L11*/ meltfnum[10] = 0;
	/*^clear */
	     /*clear *//*_#SETQ___L12*/ meltfnum[11] = 0;
	/*^clear */
	     /*clear *//*_.PAIR_TAIL__V23*/ meltfptr[21] = 0;
	/*^clear */
	     /*clear *//*_.SETQ___V24*/ meltfptr[23] = 0;
	MELT_LOCATION ("warmelt-first.melt:3652:/ checksignal");
	MELT_CHECK_SIGNAL ();
	;
	/*epilog */

	/*^clear */
	     /*clear *//*_#IS_PAIR__L8*/ meltfnum[3] = 0;
	/*^clear */
	     /*clear *//*_#NOT__L9*/ meltfnum[4] = 0;
	/*^clear */
	     /*clear *//*_.LET___V19*/ meltfptr[18] = 0;
      }
      ;
      ;
      goto labloop_LOOPFI_1;
    labexit_LOOPFI_1:;		/*^loopepilog */
      /*loopepilog */
      /*_.FOREVER___V17*/ meltfptr[9] = /*_.LOOPFI__V18*/ meltfptr[17];;
    }
    ;
    MELT_LOCATION ("warmelt-first.melt:3659:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.TUP__V15*/ meltfptr[12];;

    {
      MELT_LOCATION ("warmelt-first.melt:3659:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*_.LET___V14*/ meltfptr[11] = /*_.RETURN___V25*/ meltfptr[19];;

    MELT_LOCATION ("warmelt-first.melt:3648:/ clear");
	   /*clear *//*_.TUP__V15*/ meltfptr[12] = 0;
    /*^clear */
	   /*clear *//*_#IX__L7*/ meltfnum[2] = 0;
    /*^clear */
	   /*clear *//*_.CURPAIR__V16*/ meltfptr[8] = 0;
    /*^clear */
	   /*clear *//*_.FOREVER___V17*/ meltfptr[9] = 0;
    /*^clear */
	   /*clear *//*_.RETURN___V25*/ meltfptr[19] = 0;
    /*_.LET___V7*/ meltfptr[5] = /*_.LET___V14*/ meltfptr[11];;

    MELT_LOCATION ("warmelt-first.melt:3642:/ clear");
	   /*clear *//*_#LN__L2*/ meltfnum[1] = 0;
    /*^clear */
	   /*clear *//*_.LET___V8*/ meltfptr[7] = 0;
    /*^clear */
	   /*clear *//*_.LET___V14*/ meltfptr[11] = 0;
    MELT_LOCATION ("warmelt-first.melt:3635:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.LET___V7*/ meltfptr[5];;

    {
      MELT_LOCATION ("warmelt-first.melt:3635:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_#NULL__L1*/ meltfnum[0] = 0;
    /*^clear */
	   /*clear *//*_.IF___V5*/ meltfptr[4] = 0;
    /*^clear */
	   /*clear *//*_.LET___V7*/ meltfptr[5] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("PAIRLIST_TO_MULTIPLE", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_48_warmelt_first_PAIRLIST_TO_MULTIPLE_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_48_warmelt_first_PAIRLIST_TO_MULTIPLE */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_49_warmelt_first_MULTIPLE_ITERATE_TEST (meltclosure_ptr_t meltclosp_,
						 melt_ptr_t meltfirstargp_,
						 const melt_argdescr_cell_t
						 meltxargdescr_[],
						 union meltparam_un *
						 meltxargtab_,
						 const melt_argdescr_cell_t
						 meltxresdescr_[],
						 union meltparam_un *
						 meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_49_warmelt_first_MULTIPLE_ITERATE_TEST_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_49_warmelt_first_MULTIPLE_ITERATE_TEST_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 9
    melt_ptr_t mcfr_varptr[9];
#define MELTFRAM_NBVARNUM 4
    long mcfr_varnum[4];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_49_warmelt_first_MULTIPLE_ITERATE_TEST is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_49_warmelt_first_MULTIPLE_ITERATE_TEST_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 9; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_49_warmelt_first_MULTIPLE_ITERATE_TEST nbval 9*/
  meltfram__.mcfr_nbvar = 9 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("MULTIPLE_ITERATE_TEST", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-first.melt:3700:/ getarg");
 /*_.TUP__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.F__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.F__V3*/ meltfptr[2])) != NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {

    MELT_LOCATION ("warmelt-first.melt:3704:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#IS_MULTIPLE__L1*/ meltfnum[0] =
      (melt_magic_discr ((melt_ptr_t) ( /*_.TUP__V2*/ meltfptr[1])) ==
       MELTOBMAG_MULTIPLE);;
    MELT_LOCATION ("warmelt-first.melt:3704:/ cond");
    /*cond */ if ( /*_#IS_MULTIPLE__L1*/ meltfnum[0])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-first.melt:3705:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
   /*_#IS_CLOSURE__L2*/ meltfnum[1] =
	    (melt_magic_discr ((melt_ptr_t) ( /*_.F__V3*/ meltfptr[2])) ==
	     MELTOBMAG_CLOSURE);;
	  MELT_LOCATION ("warmelt-first.melt:3705:/ cond");
	  /*cond */ if ( /*_#IS_CLOSURE__L2*/ meltfnum[1])	/*then */
	    {
	      /*^cond.then */
	      /*^block */
	      /*anyblock */
	      {

		/*citerblock FOREACH_IN_MULTIPLE */
		{
		  /* start foreach_in_multiple meltcit1__EACHTUP */
		  long meltcit1__EACHTUP_ln =
		    melt_multiple_length ((melt_ptr_t) /*_.TUP__V2*/
					  meltfptr[1]);
		  for ( /*_#IX__L3*/ meltfnum[2] = 0;
		       ( /*_#IX__L3*/ meltfnum[2] >= 0)
		       && ( /*_#IX__L3*/ meltfnum[2] < meltcit1__EACHTUP_ln);
	/*_#IX__L3*/ meltfnum[2]++)
		    {
		      /*_.COMP__V4*/ meltfptr[3] =
			melt_multiple_nth ((melt_ptr_t)
					   ( /*_.TUP__V2*/ meltfptr[1]),
					   /*_#IX__L3*/ meltfnum[2]);



		      MELT_LOCATION ("warmelt-first.melt:3711:/ checksignal");
		      MELT_CHECK_SIGNAL ();
		      ;
		      MELT_LOCATION ("warmelt-first.melt:3709:/ quasiblock");


		      /*^multiapply */
		      /*multiapply 2args, 1x.res */
		      {
			union meltparam_un argtab[1];

			union meltparam_un restab[1];
			memset (&restab, 0, sizeof (restab));
			memset (&argtab, 0, sizeof (argtab));
			/*^multiapply.arg */
			argtab[0].meltbp_long = /*_#IX__L3*/ meltfnum[2];
			/*^multiapply.xres */
			restab[0].meltbp_aptr =
			  (melt_ptr_t *) & /*_.OTHER__V7*/ meltfptr[6];
			/*^multiapply.appl */
			/*_.TEST__V6*/ meltfptr[5] =
			  melt_apply ((meltclosure_ptr_t)
				      ( /*_.F__V3*/ meltfptr[2]),
				      (melt_ptr_t) ( /*_.COMP__V4*/
						    meltfptr[3]),
				      (MELTBPARSTR_LONG ""), argtab,
				      (MELTBPARSTR_PTR ""), restab);
		      }
		      ;
		      /*^quasiblock */


		      MELT_LOCATION ("warmelt-first.melt:3712:/ checksignal");
		      MELT_CHECK_SIGNAL ();
		      ;
      /*_#NULL__L4*/ meltfnum[3] =
			(( /*_.TEST__V6*/ meltfptr[5]) == NULL);;
		      MELT_LOCATION ("warmelt-first.melt:3712:/ cond");
		      /*cond */ if ( /*_#NULL__L4*/ meltfnum[3])	/*then */
			{
			  /*^cond.then */
			  /*^block */
			  /*anyblock */
			  {

			    /*^checksignal */
			    MELT_CHECK_SIGNAL ();
			    ;
			    /*^quasiblock */


			    /*_.RETVAL___V1*/ meltfptr[0] =
			      /*_.COMP__V4*/ meltfptr[3];;
			    MELT_LOCATION
			      ("warmelt-first.melt:3712:/ putxtraresult");
			    if (!meltxrestab_ || !meltxresdescr_)
			      goto labend_rout;
			    if (meltxresdescr_[0] != MELTBPAR_LONG)
			      goto labend_rout;
			    if (meltxrestab_[0].meltbp_longptr)
			      *(meltxrestab_[0].meltbp_longptr) =
				( /*_#IX__L3*/ meltfnum[2]);
			    ;
			    /*^putxtraresult */
			    if (!meltxrestab_ || !meltxresdescr_)
			      goto labend_rout;
			    if (meltxresdescr_[1] != MELTBPAR_PTR)
			      goto labend_rout;
			    if (meltxrestab_[1].meltbp_aptr)
			      *(meltxrestab_[1].meltbp_aptr) =
				(melt_ptr_t) ( /*_.OTHER__V7*/ meltfptr[6]);
			    ;
			    /*^finalreturn */
			    ;
			    /*finalret */ goto labend_rout;
			    /*_.IF___V8*/ meltfptr[7] =
			      /*_.RETURN___V9*/ meltfptr[8];;
			    /*epilog */

			    MELT_LOCATION ("warmelt-first.melt:3712:/ clear");
		  /*clear *//*_.RETURN___V9*/ meltfptr[8] = 0;
			  }
			  ;
			}
		      else
			{	/*^cond.else */

       /*_.IF___V8*/ meltfptr[7] = NULL;;
			}
		      ;
		      /*^compute */
		      /*_.MULTI___V5*/ meltfptr[4] =
			/*_.IF___V8*/ meltfptr[7];;

		      MELT_LOCATION ("warmelt-first.melt:3709:/ clear");
		/*clear *//*_#NULL__L4*/ meltfnum[3] = 0;
		      /*^clear */
		/*clear *//*_.IF___V8*/ meltfptr[7] = 0;

		      /*^clear */
		/*clear *//*_.OTHER__V7*/ meltfptr[6] = 0;
		      if ( /*_#IX__L3*/ meltfnum[2] < 0)
			break;
		    }		/* end  foreach_in_multiple meltcit1__EACHTUP */

		  /*citerepilog */

		  MELT_LOCATION ("warmelt-first.melt:3706:/ clear");
		/*clear *//*_.COMP__V4*/ meltfptr[3] = 0;
		  /*^clear */
		/*clear *//*_#IX__L3*/ meltfnum[2] = 0;
		  /*^clear */
		/*clear *//*_.MULTI___V5*/ meltfptr[4] = 0;
		}		/*endciterblock FOREACH_IN_MULTIPLE */
		;
		/*epilog */
	      }
	      ;
	    }			/*noelse */
	  ;
	  /*epilog */

	  MELT_LOCATION ("warmelt-first.melt:3704:/ clear");
	     /*clear *//*_#IS_CLOSURE__L2*/ meltfnum[1] = 0;
	}
	;
      }				/*noelse */
    ;
    /*epilog */

    MELT_LOCATION ("warmelt-first.melt:3700:/ clear");
	   /*clear *//*_#IS_MULTIPLE__L1*/ meltfnum[0] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("MULTIPLE_ITERATE_TEST", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_49_warmelt_first_MULTIPLE_ITERATE_TEST_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_49_warmelt_first_MULTIPLE_ITERATE_TEST */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_50_warmelt_first_MULTIPLE_MAP (meltclosure_ptr_t meltclosp_,
					melt_ptr_t meltfirstargp_,
					const melt_argdescr_cell_t
					meltxargdescr_[],
					union meltparam_un * meltxargtab_,
					const melt_argdescr_cell_t
					meltxresdescr_[],
					union meltparam_un * meltxrestab_)
{
  long current_blocklevel_signals_meltrout_50_warmelt_first_MULTIPLE_MAP_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_50_warmelt_first_MULTIPLE_MAP_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 11
    melt_ptr_t mcfr_varptr[11];
#define MELTFRAM_NBVARNUM 7
    long mcfr_varnum[7];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_50_warmelt_first_MULTIPLE_MAP is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_50_warmelt_first_MULTIPLE_MAP_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 11; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_50_warmelt_first_MULTIPLE_MAP nbval 11*/
  meltfram__.mcfr_nbvar = 11 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("MULTIPLE_MAP", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-first.melt:3717:/ getarg");
 /*_.TUP__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.F__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.F__V3*/ meltfptr[2])) != NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {

    MELT_LOCATION ("warmelt-first.melt:3720:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#IS_MULTIPLE__L1*/ meltfnum[0] =
      (melt_magic_discr ((melt_ptr_t) ( /*_.TUP__V2*/ meltfptr[1])) ==
       MELTOBMAG_MULTIPLE);;
    MELT_LOCATION ("warmelt-first.melt:3720:/ cond");
    /*cond */ if ( /*_#IS_MULTIPLE__L1*/ meltfnum[0])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-first.melt:3721:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
   /*_#IS_CLOSURE__L2*/ meltfnum[1] =
	    (melt_magic_discr ((melt_ptr_t) ( /*_.F__V3*/ meltfptr[2])) ==
	     MELTOBMAG_CLOSURE);;
	  MELT_LOCATION ("warmelt-first.melt:3721:/ cond");
	  /*cond */ if ( /*_#IS_CLOSURE__L2*/ meltfnum[1])	/*then */
	    {
	      /*^cond.then */
	      /*^block */
	      /*anyblock */
	      {

		MELT_LOCATION ("warmelt-first.melt:3722:/ quasiblock");


     /*_#LN__L3*/ meltfnum[2] =
		  (melt_multiple_length
		   ((melt_ptr_t) ( /*_.TUP__V2*/ meltfptr[1])));;
		/*^compute */
     /*_#IX__L4*/ meltfnum[3] = 0;;
		/*^compute */
     /*_.RES__V7*/ meltfptr[6] =
		  (meltgc_new_multiple
		   ((meltobject_ptr_t)
		    (( /*!DISCR_MULTIPLE */ meltfrout->tabval[0])),
		    ( /*_#LN__L3*/ meltfnum[2])));;
		MELT_LOCATION ("warmelt-first.melt:3726:/ loop");
		/*loop */
		{
		labloop_TUPLOOP_1:;
				/*^loopbody */

		  /*^block */
		  /*anyblock */
		  {

		    /*^checksignal */
		    MELT_CHECK_SIGNAL ();
		    ;
		    MELT_LOCATION ("warmelt-first.melt:3727:/ checksignal");
		    MELT_CHECK_SIGNAL ();
		    ;
       /*_#I__L5*/ meltfnum[4] =
		      (( /*_#IX__L4*/ meltfnum[3]) >=
		       ( /*_#LN__L3*/ meltfnum[2]));;
		    MELT_LOCATION ("warmelt-first.melt:3727:/ cond");
		    /*cond */ if ( /*_#I__L5*/ meltfnum[4])	/*then */
		      {
			/*^cond.then */
			/*^block */
			/*anyblock */
			{

			  /*^quasiblock */


			  /*^compute */
			  /*_.TUPLOOP__V9*/ meltfptr[8] =
			    /*_.RES__V7*/ meltfptr[6];;

			  /*^exit */
			  /*exit */
			  {
			    goto labexit_TUPLOOP_1;
			  }
			  ;
			  /*epilog */
			}
			;
		      }		/*noelse */
		    ;
		    MELT_LOCATION ("warmelt-first.melt:3728:/ quasiblock");


       /*_.CURCOMP__V10*/ meltfptr[9] =
		      (melt_multiple_nth
		       ((melt_ptr_t) ( /*_.TUP__V2*/ meltfptr[1]),
			( /*_#IX__L4*/ meltfnum[3])));;
		    MELT_LOCATION ("warmelt-first.melt:3729:/ checksignal");
		    MELT_CHECK_SIGNAL ();
		    ;
		    /*^apply */
		    /*apply */
		    {
		      union meltparam_un argtab[1];
		      memset (&argtab, 0, sizeof (argtab));
		      /*^apply.arg */
		      argtab[0].meltbp_long = /*_#IX__L4*/ meltfnum[3];
		      /*_.F__V11*/ meltfptr[10] =
			melt_apply ((meltclosure_ptr_t)
				    ( /*_.F__V3*/ meltfptr[2]),
				    (melt_ptr_t) ( /*_.CURCOMP__V10*/
						  meltfptr[9]),
				    (MELTBPARSTR_LONG ""), argtab, "",
				    (union meltparam_un *) 0);
		    }
		    ;

		    {
		      /*^locexp */
		      meltgc_multiple_put_nth ((melt_ptr_t)
					       ( /*_.RES__V7*/ meltfptr[6]),
					       ( /*_#IX__L4*/ meltfnum[3]),
					       (melt_ptr_t) ( /*_.F__V11*/
							     meltfptr[10]));
		    }
		    ;

		    MELT_LOCATION ("warmelt-first.melt:3728:/ clear");
		 /*clear *//*_.CURCOMP__V10*/ meltfptr[9] = 0;
		    /*^clear */
		 /*clear *//*_.F__V11*/ meltfptr[10] = 0;
       /*_#I__L6*/ meltfnum[5] =
		      (( /*_#IX__L4*/ meltfnum[3]) + (1));;
		    MELT_LOCATION ("warmelt-first.melt:3730:/ compute");
		    /*_#IX__L4*/ meltfnum[3] = /*_#SETQ___L7*/ meltfnum[6] =
		      /*_#I__L6*/ meltfnum[5];;
		    MELT_LOCATION ("warmelt-first.melt:3726:/ checksignal");
		    MELT_CHECK_SIGNAL ();
		    ;
		    /*epilog */

		    /*^clear */
		 /*clear *//*_#I__L5*/ meltfnum[4] = 0;
		    /*^clear */
		 /*clear *//*_#I__L6*/ meltfnum[5] = 0;
		    /*^clear */
		 /*clear *//*_#SETQ___L7*/ meltfnum[6] = 0;
		  }
		  ;
		  ;
		  goto labloop_TUPLOOP_1;
		labexit_TUPLOOP_1:;
				/*^loopepilog */
		  /*loopepilog */
		  /*_.FOREVER___V8*/ meltfptr[7] =
		    /*_.TUPLOOP__V9*/ meltfptr[8];;
		}
		;
		/*^compute */
		/*_.LET___V6*/ meltfptr[5] = /*_.FOREVER___V8*/ meltfptr[7];;

		MELT_LOCATION ("warmelt-first.melt:3722:/ clear");
	       /*clear *//*_#LN__L3*/ meltfnum[2] = 0;
		/*^clear */
	       /*clear *//*_#IX__L4*/ meltfnum[3] = 0;
		/*^clear */
	       /*clear *//*_.RES__V7*/ meltfptr[6] = 0;
		/*^clear */
	       /*clear *//*_.FOREVER___V8*/ meltfptr[7] = 0;
		/*_.IF___V5*/ meltfptr[4] = /*_.LET___V6*/ meltfptr[5];;
		/*epilog */

		MELT_LOCATION ("warmelt-first.melt:3721:/ clear");
	       /*clear *//*_.LET___V6*/ meltfptr[5] = 0;
	      }
	      ;
	    }
	  else
	    {			/*^cond.else */

    /*_.IF___V5*/ meltfptr[4] = NULL;;
	    }
	  ;
	  /*^compute */
	  /*_.IF___V4*/ meltfptr[3] = /*_.IF___V5*/ meltfptr[4];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-first.melt:3720:/ clear");
	     /*clear *//*_#IS_CLOSURE__L2*/ meltfnum[1] = 0;
	  /*^clear */
	     /*clear *//*_.IF___V5*/ meltfptr[4] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

  /*_.IF___V4*/ meltfptr[3] = NULL;;
      }
    ;
    MELT_LOCATION ("warmelt-first.melt:3717:/ quasiblock");


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.IF___V4*/ meltfptr[3];;

    {
      MELT_LOCATION ("warmelt-first.melt:3717:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_#IS_MULTIPLE__L1*/ meltfnum[0] = 0;
    /*^clear */
	   /*clear *//*_.IF___V4*/ meltfptr[3] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("MULTIPLE_MAP", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_50_warmelt_first_MULTIPLE_MAP_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_50_warmelt_first_MULTIPLE_MAP */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_51_warmelt_first_MULTIPLE_TO_LIST (meltclosure_ptr_t meltclosp_,
					    melt_ptr_t meltfirstargp_,
					    const melt_argdescr_cell_t
					    meltxargdescr_[],
					    union meltparam_un * meltxargtab_,
					    const melt_argdescr_cell_t
					    meltxresdescr_[],
					    union meltparam_un * meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_51_warmelt_first_MULTIPLE_TO_LIST_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_51_warmelt_first_MULTIPLE_TO_LIST_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 12
    melt_ptr_t mcfr_varptr[12];
#define MELTFRAM_NBVARNUM 4
    long mcfr_varnum[4];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_51_warmelt_first_MULTIPLE_TO_LIST is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_51_warmelt_first_MULTIPLE_TO_LIST_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 12; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_51_warmelt_first_MULTIPLE_TO_LIST nbval 12*/
  meltfram__.mcfr_nbvar = 12 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("MULTIPLE_TO_LIST", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-first.melt:3734:/ getarg");
 /*_.TUP__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.DISC__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.DISC__V3*/ meltfptr[2])) !=
	      NULL);


  /*getarg#2 */
  /*^getarg */
  if (meltxargdescr_[1] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.TRANSF__V4*/ meltfptr[3] =
    (meltxargtab_[1].meltbp_aptr) ? (*(meltxargtab_[1].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.TRANSF__V4*/ meltfptr[3])) !=
	      NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {

    MELT_LOCATION ("warmelt-first.melt:3738:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#NULL__L1*/ meltfnum[0] =
      (( /*_.DISC__V3*/ meltfptr[2]) == NULL);;
    MELT_LOCATION ("warmelt-first.melt:3738:/ cond");
    /*cond */ if ( /*_#NULL__L1*/ meltfnum[0])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  /*^compute */
	  /*_.DISC__V3*/ meltfptr[2] = /*_.SETQ___V6*/ meltfptr[5] =
	    ( /*!DISCR_LIST */ meltfrout->tabval[0]);;
	  /*_.IF___V5*/ meltfptr[4] = /*_.SETQ___V6*/ meltfptr[5];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-first.melt:3738:/ clear");
	     /*clear *//*_.SETQ___V6*/ meltfptr[5] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

  /*_.IF___V5*/ meltfptr[4] = NULL;;
      }
    ;
    MELT_LOCATION ("warmelt-first.melt:3739:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#IS_MULTIPLE__L2*/ meltfnum[1] =
      (melt_magic_discr ((melt_ptr_t) ( /*_.TUP__V2*/ meltfptr[1])) ==
       MELTOBMAG_MULTIPLE);;
    MELT_LOCATION ("warmelt-first.melt:3739:/ cond");
    /*cond */ if ( /*_#IS_MULTIPLE__L2*/ meltfnum[1])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-first.melt:3740:/ quasiblock");


   /*_.LIS__V9*/ meltfptr[8] =
	    (meltgc_new_list
	     ((meltobject_ptr_t) ( /*_.DISC__V3*/ meltfptr[2])));;
	  /*citerblock FOREACH_IN_MULTIPLE */
	  {
	    /* start foreach_in_multiple meltcit1__EACHTUP */
	    long meltcit1__EACHTUP_ln =
	      melt_multiple_length ((melt_ptr_t) /*_.TUP__V2*/ meltfptr[1]);
	    for ( /*_#IX__L3*/ meltfnum[2] = 0;
		 ( /*_#IX__L3*/ meltfnum[2] >= 0)
		 && ( /*_#IX__L3*/ meltfnum[2] < meltcit1__EACHTUP_ln);
	/*_#IX__L3*/ meltfnum[2]++)
	      {
		/*_.COMP__V10*/ meltfptr[9] =
		  melt_multiple_nth ((melt_ptr_t)
				     ( /*_.TUP__V2*/ meltfptr[1]),
				     /*_#IX__L3*/ meltfnum[2]);



		MELT_LOCATION ("warmelt-first.melt:3744:/ checksignal");
		MELT_CHECK_SIGNAL ();
		;
    /*_#IS_CLOSURE__L4*/ meltfnum[3] =
		  (melt_magic_discr
		   ((melt_ptr_t) ( /*_.TRANSF__V4*/ meltfptr[3])) ==
		   MELTOBMAG_CLOSURE);;
		MELT_LOCATION ("warmelt-first.melt:3744:/ cond");
		/*cond */ if ( /*_#IS_CLOSURE__L4*/ meltfnum[3])	/*then */
		  {
		    /*^cond.then */
		    /*^block */
		    /*anyblock */
		    {

		      MELT_LOCATION ("warmelt-first.melt:3745:/ checksignal");
		      MELT_CHECK_SIGNAL ();
		      ;
		      /*^apply */
		      /*apply */
		      {
			/*_.TRANSF__V11*/ meltfptr[10] =
			  melt_apply ((meltclosure_ptr_t)
				      ( /*_.TRANSF__V4*/ meltfptr[3]),
				      (melt_ptr_t) ( /*_.COMP__V10*/
						    meltfptr[9]), (""),
				      (union meltparam_un *) 0, "",
				      (union meltparam_un *) 0);
		      }
		      ;

		      {
			/*^locexp */
			meltgc_append_list ((melt_ptr_t)
					    ( /*_.LIS__V9*/ meltfptr[8]),
					    (melt_ptr_t) ( /*_.TRANSF__V11*/
							  meltfptr[10]));
		      }
		      ;
		      /*epilog */

		      MELT_LOCATION ("warmelt-first.melt:3744:/ clear");
		/*clear *//*_.TRANSF__V11*/ meltfptr[10] = 0;
		    }
		    ;
		  }
		else
		  {		/*^cond.else */

		    /*^block */
		    /*anyblock */
		    {


		      {
			MELT_LOCATION ("warmelt-first.melt:3746:/ locexp");
			meltgc_append_list ((melt_ptr_t)
					    ( /*_.LIS__V9*/ meltfptr[8]),
					    (melt_ptr_t) ( /*_.COMP__V10*/
							  meltfptr[9]));
		      }
		      ;
		      /*epilog */
		    }
		    ;
		  }
		;
		if ( /*_#IX__L3*/ meltfnum[2] < 0)
		  break;
	      }			/* end  foreach_in_multiple meltcit1__EACHTUP */

	    /*citerepilog */

	    MELT_LOCATION ("warmelt-first.melt:3741:/ clear");
	      /*clear *//*_.COMP__V10*/ meltfptr[9] = 0;
	    /*^clear */
	      /*clear *//*_#IX__L3*/ meltfnum[2] = 0;
	    /*^clear */
	      /*clear *//*_#IS_CLOSURE__L4*/ meltfnum[3] = 0;
	  }			/*endciterblock FOREACH_IN_MULTIPLE */
	  ;
	  MELT_LOCATION ("warmelt-first.melt:3747:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^quasiblock */


	  /*_.RETVAL___V1*/ meltfptr[0] = /*_.LIS__V9*/ meltfptr[8];;

	  {
	    MELT_LOCATION ("warmelt-first.melt:3747:/ locexp");
	    /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
	    if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	      melt_warn_for_no_expected_secondary_results ();
	    /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
	    ;
	  }
	  ;
	  /*^finalreturn */
	  ;
	  /*finalret */ goto labend_rout;
	  /*_.LET___V8*/ meltfptr[7] = /*_.RETURN___V12*/ meltfptr[10];;

	  MELT_LOCATION ("warmelt-first.melt:3740:/ clear");
	     /*clear *//*_.LIS__V9*/ meltfptr[8] = 0;
	  /*^clear */
	     /*clear *//*_.RETURN___V12*/ meltfptr[10] = 0;
	  /*_.IF___V7*/ meltfptr[5] = /*_.LET___V8*/ meltfptr[7];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-first.melt:3739:/ clear");
	     /*clear *//*_.LET___V8*/ meltfptr[7] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

  /*_.IF___V7*/ meltfptr[5] = NULL;;
      }
    ;
    MELT_LOCATION ("warmelt-first.melt:3734:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.IF___V7*/ meltfptr[5];;

    {
      MELT_LOCATION ("warmelt-first.melt:3734:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_#NULL__L1*/ meltfnum[0] = 0;
    /*^clear */
	   /*clear *//*_.IF___V5*/ meltfptr[4] = 0;
    /*^clear */
	   /*clear *//*_#IS_MULTIPLE__L2*/ meltfnum[1] = 0;
    /*^clear */
	   /*clear *//*_.IF___V7*/ meltfptr[5] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("MULTIPLE_TO_LIST", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_51_warmelt_first_MULTIPLE_TO_LIST_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_51_warmelt_first_MULTIPLE_TO_LIST */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_52_warmelt_first_CLOSURE_EVERY (meltclosure_ptr_t meltclosp_,
					 melt_ptr_t meltfirstargp_,
					 const melt_argdescr_cell_t
					 meltxargdescr_[],
					 union meltparam_un * meltxargtab_,
					 const melt_argdescr_cell_t
					 meltxresdescr_[],
					 union meltparam_un * meltxrestab_)
{
  long current_blocklevel_signals_meltrout_52_warmelt_first_CLOSURE_EVERY_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_52_warmelt_first_CLOSURE_EVERY_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 10
    melt_ptr_t mcfr_varptr[10];
#define MELTFRAM_NBVARNUM 7
    long mcfr_varnum[7];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_52_warmelt_first_CLOSURE_EVERY is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_52_warmelt_first_CLOSURE_EVERY_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 10; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_52_warmelt_first_CLOSURE_EVERY nbval 10*/
  meltfram__.mcfr_nbvar = 10 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("CLOSURE_EVERY", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-first.melt:3752:/ getarg");
 /*_.CLO__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.F__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.F__V3*/ meltfptr[2])) != NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {

    MELT_LOCATION ("warmelt-first.melt:3754:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#IS_CLOSURE__L1*/ meltfnum[0] =
      (melt_magic_discr ((melt_ptr_t) ( /*_.CLO__V2*/ meltfptr[1])) ==
       MELTOBMAG_CLOSURE);;
    MELT_LOCATION ("warmelt-first.melt:3754:/ cond");
    /*cond */ if ( /*_#IS_CLOSURE__L1*/ meltfnum[0])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-first.melt:3755:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
   /*_#IS_CLOSURE__L2*/ meltfnum[1] =
	    (melt_magic_discr ((melt_ptr_t) ( /*_.F__V3*/ meltfptr[2])) ==
	     MELTOBMAG_CLOSURE);;
	  MELT_LOCATION ("warmelt-first.melt:3755:/ cond");
	  /*cond */ if ( /*_#IS_CLOSURE__L2*/ meltfnum[1])	/*then */
	    {
	      /*^cond.then */
	      /*^block */
	      /*anyblock */
	      {

		MELT_LOCATION ("warmelt-first.melt:3756:/ quasiblock");


     /*_#LN__L3*/ meltfnum[2] =
		  (melt_closure_size
		   ((melt_ptr_t) ( /*_.CLO__V2*/ meltfptr[1])));;
		/*^compute */
     /*_#IX__L4*/ meltfnum[3] = 0;;
		MELT_LOCATION ("warmelt-first.melt:3758:/ loop");
		/*loop */
		{
		labloop_CLOLOOP_1:;
				/*^loopbody */

		  /*^block */
		  /*anyblock */
		  {

		    /*^checksignal */
		    MELT_CHECK_SIGNAL ();
		    ;
		    MELT_LOCATION ("warmelt-first.melt:3759:/ checksignal");
		    MELT_CHECK_SIGNAL ();
		    ;
       /*_#I__L5*/ meltfnum[4] =
		      (( /*_#IX__L4*/ meltfnum[3]) >=
		       ( /*_#LN__L3*/ meltfnum[2]));;
		    MELT_LOCATION ("warmelt-first.melt:3759:/ cond");
		    /*cond */ if ( /*_#I__L5*/ meltfnum[4])	/*then */
		      {
			/*^cond.then */
			/*^block */
			/*anyblock */
			{

			  /*^quasiblock */


			  /*^compute */
	 /*_.CLOLOOP__V8*/ meltfptr[7] = NULL;;

			  /*^exit */
			  /*exit */
			  {
			    goto labexit_CLOLOOP_1;
			  }
			  ;
			  /*epilog */
			}
			;
		      }		/*noelse */
		    ;
       /*_.CLOSURE_NTH__V9*/ meltfptr[8] =
		      (melt_closure_nth
		       ((melt_ptr_t) ( /*_.CLO__V2*/ meltfptr[1]),
			(int) ( /*_#IX__L4*/ meltfnum[3])));;
		    MELT_LOCATION ("warmelt-first.melt:3760:/ checksignal");
		    MELT_CHECK_SIGNAL ();
		    ;
		    /*^apply */
		    /*apply */
		    {
		      union meltparam_un argtab[1];
		      memset (&argtab, 0, sizeof (argtab));
		      /*^apply.arg */
		      argtab[0].meltbp_long = /*_#IX__L4*/ meltfnum[3];
		      /*_.F__V10*/ meltfptr[9] =
			melt_apply ((meltclosure_ptr_t)
				    ( /*_.F__V3*/ meltfptr[2]),
				    (melt_ptr_t) ( /*_.CLOSURE_NTH__V9*/
						  meltfptr[8]),
				    (MELTBPARSTR_LONG ""), argtab, "",
				    (union meltparam_un *) 0);
		    }
		    ;
       /*_#I__L6*/ meltfnum[5] =
		      (( /*_#IX__L4*/ meltfnum[3]) + (1));;
		    MELT_LOCATION ("warmelt-first.melt:3761:/ compute");
		    /*_#IX__L4*/ meltfnum[3] = /*_#SETQ___L7*/ meltfnum[6] =
		      /*_#I__L6*/ meltfnum[5];;
		    MELT_LOCATION ("warmelt-first.melt:3758:/ checksignal");
		    MELT_CHECK_SIGNAL ();
		    ;
		    /*epilog */

		    /*^clear */
		 /*clear *//*_#I__L5*/ meltfnum[4] = 0;
		    /*^clear */
		 /*clear *//*_.CLOSURE_NTH__V9*/ meltfptr[8] = 0;
		    /*^clear */
		 /*clear *//*_.F__V10*/ meltfptr[9] = 0;
		    /*^clear */
		 /*clear *//*_#I__L6*/ meltfnum[5] = 0;
		    /*^clear */
		 /*clear *//*_#SETQ___L7*/ meltfnum[6] = 0;
		  }
		  ;
		  ;
		  goto labloop_CLOLOOP_1;
		labexit_CLOLOOP_1:;
				/*^loopepilog */
		  /*loopepilog */
		  /*_.FOREVER___V7*/ meltfptr[6] =
		    /*_.CLOLOOP__V8*/ meltfptr[7];;
		}
		;
		/*^compute */
		/*_.LET___V6*/ meltfptr[5] = /*_.FOREVER___V7*/ meltfptr[6];;

		MELT_LOCATION ("warmelt-first.melt:3756:/ clear");
	       /*clear *//*_#LN__L3*/ meltfnum[2] = 0;
		/*^clear */
	       /*clear *//*_#IX__L4*/ meltfnum[3] = 0;
		/*^clear */
	       /*clear *//*_.FOREVER___V7*/ meltfptr[6] = 0;
		/*_.IF___V5*/ meltfptr[4] = /*_.LET___V6*/ meltfptr[5];;
		/*epilog */

		MELT_LOCATION ("warmelt-first.melt:3755:/ clear");
	       /*clear *//*_.LET___V6*/ meltfptr[5] = 0;
	      }
	      ;
	    }
	  else
	    {			/*^cond.else */

    /*_.IF___V5*/ meltfptr[4] = NULL;;
	    }
	  ;
	  /*^compute */
	  /*_.IF___V4*/ meltfptr[3] = /*_.IF___V5*/ meltfptr[4];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-first.melt:3754:/ clear");
	     /*clear *//*_#IS_CLOSURE__L2*/ meltfnum[1] = 0;
	  /*^clear */
	     /*clear *//*_.IF___V5*/ meltfptr[4] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

  /*_.IF___V4*/ meltfptr[3] = NULL;;
      }
    ;
    MELT_LOCATION ("warmelt-first.melt:3752:/ quasiblock");


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.IF___V4*/ meltfptr[3];;

    {
      MELT_LOCATION ("warmelt-first.melt:3752:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_#IS_CLOSURE__L1*/ meltfnum[0] = 0;
    /*^clear */
	   /*clear *//*_.IF___V4*/ meltfptr[3] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("CLOSURE_EVERY", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_52_warmelt_first_CLOSURE_EVERY_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_52_warmelt_first_CLOSURE_EVERY */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_53_warmelt_first_ROUTINE_EVERY (meltclosure_ptr_t meltclosp_,
					 melt_ptr_t meltfirstargp_,
					 const melt_argdescr_cell_t
					 meltxargdescr_[],
					 union meltparam_un * meltxargtab_,
					 const melt_argdescr_cell_t
					 meltxresdescr_[],
					 union meltparam_un * meltxrestab_)
{
  long current_blocklevel_signals_meltrout_53_warmelt_first_ROUTINE_EVERY_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_53_warmelt_first_ROUTINE_EVERY_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 10
    melt_ptr_t mcfr_varptr[10];
#define MELTFRAM_NBVARNUM 7
    long mcfr_varnum[7];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_53_warmelt_first_ROUTINE_EVERY is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_53_warmelt_first_ROUTINE_EVERY_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 10; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_53_warmelt_first_ROUTINE_EVERY nbval 10*/
  meltfram__.mcfr_nbvar = 10 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("ROUTINE_EVERY", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-first.melt:3765:/ getarg");
 /*_.ROU__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.F__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.F__V3*/ meltfptr[2])) != NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {

    MELT_LOCATION ("warmelt-first.melt:3767:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#IS_ROUTINE__L1*/ meltfnum[0] =
      (melt_magic_discr ((melt_ptr_t) ( /*_.ROU__V2*/ meltfptr[1])) ==
       MELTOBMAG_ROUTINE);;
    MELT_LOCATION ("warmelt-first.melt:3767:/ cond");
    /*cond */ if ( /*_#IS_ROUTINE__L1*/ meltfnum[0])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-first.melt:3768:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
   /*_#IS_CLOSURE__L2*/ meltfnum[1] =
	    (melt_magic_discr ((melt_ptr_t) ( /*_.F__V3*/ meltfptr[2])) ==
	     MELTOBMAG_CLOSURE);;
	  MELT_LOCATION ("warmelt-first.melt:3768:/ cond");
	  /*cond */ if ( /*_#IS_CLOSURE__L2*/ meltfnum[1])	/*then */
	    {
	      /*^cond.then */
	      /*^block */
	      /*anyblock */
	      {

		MELT_LOCATION ("warmelt-first.melt:3769:/ quasiblock");


     /*_#LN__L3*/ meltfnum[2] =
		  (melt_routine_size
		   ((melt_ptr_t) ( /*_.ROU__V2*/ meltfptr[1])));;
		/*^compute */
     /*_#IX__L4*/ meltfnum[3] = 0;;
		MELT_LOCATION ("warmelt-first.melt:3771:/ loop");
		/*loop */
		{
		labloop_ROULOOP_1:;
				/*^loopbody */

		  /*^block */
		  /*anyblock */
		  {

		    /*^checksignal */
		    MELT_CHECK_SIGNAL ();
		    ;
		    MELT_LOCATION ("warmelt-first.melt:3772:/ checksignal");
		    MELT_CHECK_SIGNAL ();
		    ;
       /*_#I__L5*/ meltfnum[4] =
		      (( /*_#IX__L4*/ meltfnum[3]) >=
		       ( /*_#LN__L3*/ meltfnum[2]));;
		    MELT_LOCATION ("warmelt-first.melt:3772:/ cond");
		    /*cond */ if ( /*_#I__L5*/ meltfnum[4])	/*then */
		      {
			/*^cond.then */
			/*^block */
			/*anyblock */
			{

			  /*^quasiblock */


			  /*^compute */
	 /*_.ROULOOP__V8*/ meltfptr[7] = NULL;;

			  /*^exit */
			  /*exit */
			  {
			    goto labexit_ROULOOP_1;
			  }
			  ;
			  /*epilog */
			}
			;
		      }		/*noelse */
		    ;
       /*_.ROUTINE_NTH__V9*/ meltfptr[8] =
		      (melt_routine_nth
		       ((melt_ptr_t) ( /*_.ROU__V2*/ meltfptr[1]),
			(int) ( /*_#IX__L4*/ meltfnum[3])));;
		    MELT_LOCATION ("warmelt-first.melt:3773:/ checksignal");
		    MELT_CHECK_SIGNAL ();
		    ;
		    /*^apply */
		    /*apply */
		    {
		      union meltparam_un argtab[1];
		      memset (&argtab, 0, sizeof (argtab));
		      /*^apply.arg */
		      argtab[0].meltbp_long = /*_#IX__L4*/ meltfnum[3];
		      /*_.F__V10*/ meltfptr[9] =
			melt_apply ((meltclosure_ptr_t)
				    ( /*_.F__V3*/ meltfptr[2]),
				    (melt_ptr_t) ( /*_.ROUTINE_NTH__V9*/
						  meltfptr[8]),
				    (MELTBPARSTR_LONG ""), argtab, "",
				    (union meltparam_un *) 0);
		    }
		    ;
       /*_#I__L6*/ meltfnum[5] =
		      (( /*_#IX__L4*/ meltfnum[3]) + (1));;
		    MELT_LOCATION ("warmelt-first.melt:3774:/ compute");
		    /*_#IX__L4*/ meltfnum[3] = /*_#SETQ___L7*/ meltfnum[6] =
		      /*_#I__L6*/ meltfnum[5];;
		    MELT_LOCATION ("warmelt-first.melt:3771:/ checksignal");
		    MELT_CHECK_SIGNAL ();
		    ;
		    /*epilog */

		    /*^clear */
		 /*clear *//*_#I__L5*/ meltfnum[4] = 0;
		    /*^clear */
		 /*clear *//*_.ROUTINE_NTH__V9*/ meltfptr[8] = 0;
		    /*^clear */
		 /*clear *//*_.F__V10*/ meltfptr[9] = 0;
		    /*^clear */
		 /*clear *//*_#I__L6*/ meltfnum[5] = 0;
		    /*^clear */
		 /*clear *//*_#SETQ___L7*/ meltfnum[6] = 0;
		  }
		  ;
		  ;
		  goto labloop_ROULOOP_1;
		labexit_ROULOOP_1:;
				/*^loopepilog */
		  /*loopepilog */
		  /*_.FOREVER___V7*/ meltfptr[6] =
		    /*_.ROULOOP__V8*/ meltfptr[7];;
		}
		;
		/*^compute */
		/*_.LET___V6*/ meltfptr[5] = /*_.FOREVER___V7*/ meltfptr[6];;

		MELT_LOCATION ("warmelt-first.melt:3769:/ clear");
	       /*clear *//*_#LN__L3*/ meltfnum[2] = 0;
		/*^clear */
	       /*clear *//*_#IX__L4*/ meltfnum[3] = 0;
		/*^clear */
	       /*clear *//*_.FOREVER___V7*/ meltfptr[6] = 0;
		/*_.IF___V5*/ meltfptr[4] = /*_.LET___V6*/ meltfptr[5];;
		/*epilog */

		MELT_LOCATION ("warmelt-first.melt:3768:/ clear");
	       /*clear *//*_.LET___V6*/ meltfptr[5] = 0;
	      }
	      ;
	    }
	  else
	    {			/*^cond.else */

    /*_.IF___V5*/ meltfptr[4] = NULL;;
	    }
	  ;
	  /*^compute */
	  /*_.IF___V4*/ meltfptr[3] = /*_.IF___V5*/ meltfptr[4];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-first.melt:3767:/ clear");
	     /*clear *//*_#IS_CLOSURE__L2*/ meltfnum[1] = 0;
	  /*^clear */
	     /*clear *//*_.IF___V5*/ meltfptr[4] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

  /*_.IF___V4*/ meltfptr[3] = NULL;;
      }
    ;
    MELT_LOCATION ("warmelt-first.melt:3765:/ quasiblock");


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.IF___V4*/ meltfptr[3];;

    {
      MELT_LOCATION ("warmelt-first.melt:3765:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_#IS_ROUTINE__L1*/ meltfnum[0] = 0;
    /*^clear */
	   /*clear *//*_.IF___V4*/ meltfptr[3] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("ROUTINE_EVERY", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_53_warmelt_first_ROUTINE_EVERY_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_53_warmelt_first_ROUTINE_EVERY */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_54_warmelt_first_INSTALL_METHOD (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un * meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un * meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_54_warmelt_first_INSTALL_METHOD_melt =
    melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_54_warmelt_first_INSTALL_METHOD_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 10
    melt_ptr_t mcfr_varptr[10];
#define MELTFRAM_NBVARNUM 6
    long mcfr_varnum[6];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_54_warmelt_first_INSTALL_METHOD is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_54_warmelt_first_INSTALL_METHOD_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 10; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_54_warmelt_first_INSTALL_METHOD nbval 10*/
  meltfram__.mcfr_nbvar = 10 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("INSTALL_METHOD", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-first.melt:3779:/ getarg");
 /*_.DIS__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.SEL__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.SEL__V3*/ meltfptr[2])) != NULL);


  /*getarg#2 */
  /*^getarg */
  if (meltxargdescr_[1] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.FUN__V4*/ meltfptr[3] =
    (meltxargtab_[1].meltbp_aptr) ? (*(meltxargtab_[1].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.FUN__V4*/ meltfptr[3])) != NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {

    MELT_LOCATION ("warmelt-first.melt:3782:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#IS_A__L1*/ meltfnum[0] =
      melt_is_instance_of ((melt_ptr_t) ( /*_.DIS__V2*/ meltfptr[1]),
			   (melt_ptr_t) (( /*!CLASS_DISCRIMINANT */
					  meltfrout->tabval[0])));;
    MELT_LOCATION ("warmelt-first.melt:3782:/ cond");
    /*cond */ if ( /*_#IS_A__L1*/ meltfnum[0])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-first.melt:3783:/ quasiblock");


	  /*^getslot */
	  {
	    melt_ptr_t slot = NULL, obj = NULL;
	    obj = (melt_ptr_t) ( /*_.DIS__V2*/ meltfptr[1]) /*=obj*/ ;
	    melt_object_get_field (slot, obj, 1, "NAMED_NAME");
    /*_.DISNAME__V5*/ meltfptr[4] = slot;
	  };
	  ;
	  MELT_LOCATION ("warmelt-first.melt:3786:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
   /*_#IS_A__L2*/ meltfnum[1] =
	    melt_is_instance_of ((melt_ptr_t) ( /*_.SEL__V3*/ meltfptr[2]),
				 (melt_ptr_t) (( /*!CLASS_SELECTOR */
						meltfrout->tabval[1])));;
	  MELT_LOCATION ("warmelt-first.melt:3786:/ cond");
	  /*cond */ if ( /*_#IS_A__L2*/ meltfnum[1])	/*then */
	    {
	      /*^cond.then */
	      /*^block */
	      /*anyblock */
	      {

		MELT_LOCATION ("warmelt-first.melt:3787:/ quasiblock");


		/*^getslot */
		{
		  melt_ptr_t slot = NULL, obj = NULL;
		  obj = (melt_ptr_t) ( /*_.SEL__V3*/ meltfptr[2]) /*=obj*/ ;
		  melt_object_get_field (slot, obj, 1, "NAMED_NAME");
      /*_.SELNAME__V6*/ meltfptr[5] = slot;
		};
		;
		MELT_LOCATION ("warmelt-first.melt:3790:/ checksignal");
		MELT_CHECK_SIGNAL ();
		;
     /*_#IS_CLOSURE__L3*/ meltfnum[2] =
		  (melt_magic_discr
		   ((melt_ptr_t) ( /*_.FUN__V4*/ meltfptr[3])) ==
		   MELTOBMAG_CLOSURE);;
		MELT_LOCATION ("warmelt-first.melt:3790:/ cond");
		/*cond */ if ( /*_#IS_CLOSURE__L3*/ meltfnum[2])	/*then */
		  {
		    /*^cond.then */
		    /*^block */
		    /*anyblock */
		    {

		      MELT_LOCATION ("warmelt-first.melt:3791:/ quasiblock");


		      MELT_LOCATION ("warmelt-first.melt:3793:/ getslot");
		      {
			melt_ptr_t slot = NULL, obj = NULL;
			obj =
			  (melt_ptr_t) ( /*_.DIS__V2*/ meltfptr[1]) /*=obj*/ ;
			melt_object_get_field (slot, obj, 2,
					       "DISC_METHODICT");
	/*_.MAPDICT__V7*/ meltfptr[6] = slot;
		      };
		      ;
		      MELT_LOCATION ("warmelt-first.melt:3794:/ checksignal");
		      MELT_CHECK_SIGNAL ();
		      ;
       /*_#IS_MAPOBJECT__L4*/ meltfnum[3] =
			/*is_mapobject: */
			(melt_magic_discr
			 ((melt_ptr_t) ( /*_.MAPDICT__V7*/ meltfptr[6])) ==
			 MELTOBMAG_MAPOBJECTS);;
		      MELT_LOCATION ("warmelt-first.melt:3794:/ cond");
		      /*cond */ if ( /*_#IS_MAPOBJECT__L4*/ meltfnum[3])	/*then */
			{
			  /*^cond.then */
			  /*^block */
			  /*anyblock */
			  {


			    {
			      MELT_LOCATION
				("warmelt-first.melt:3795:/ locexp");
			      meltgc_put_mapobjects ((meltmapobjects_ptr_t)
						     ( /*_.MAPDICT__V7*/
						      meltfptr[6]),
						     (meltobject_ptr_t) ( /*_.SEL__V3*/ meltfptr[2]),
						     (melt_ptr_t) ( /*_.FUN__V4*/ meltfptr[3]));
			    }
			    ;
			    /*epilog */
			  }
			  ;
			}
		      else
			{
			  MELT_LOCATION
			    ("warmelt-first.melt:3794:/ cond.else");

			  /*^block */
			  /*anyblock */
			  {

			    MELT_LOCATION
			      ("warmelt-first.melt:3796:/ quasiblock");


	 /*_.NEWMAPDICT__V8*/ meltfptr[7] =
			      (meltgc_new_mapobjects
			       ((meltobject_ptr_t)
				(( /*!DISCR_METHOD_MAP */ meltfrout->
				  tabval[2])), (35)));;
			    MELT_LOCATION
			      ("warmelt-first.melt:3797:/ checksignal");
			    MELT_CHECK_SIGNAL ();
			    ;
			    /*^quasiblock */


			    /*^putslot */
			    /*putslot */
			    melt_assertmsg
			      ("putslot checkobj @DISC_METHODICT",
			       melt_magic_discr ((melt_ptr_t)
						 ( /*_.DIS__V2*/ meltfptr[1]))
			       == MELTOBMAG_OBJECT);
			    melt_putfield_object (( /*_.DIS__V2*/
						   meltfptr[1]), (2),
						  ( /*_.NEWMAPDICT__V8*/
						   meltfptr[7]),
						  "DISC_METHODICT");
			    ;
			    /*^touch */
			    meltgc_touch ( /*_.DIS__V2*/ meltfptr[1]);
			    ;
			    /*^touchobj */

			    melt_dbgtrace_written_object ( /*_.DIS__V2*/
							  meltfptr[1],
							  "put-fields");
			    ;


			    {
			      MELT_LOCATION
				("warmelt-first.melt:3798:/ locexp");
			      meltgc_put_mapobjects ((meltmapobjects_ptr_t)
						     ( /*_.NEWMAPDICT__V8*/
						      meltfptr[7]),
						     (meltobject_ptr_t) ( /*_.SEL__V3*/ meltfptr[2]),
						     (melt_ptr_t) ( /*_.FUN__V4*/ meltfptr[3]));
			    }
			    ;

			    MELT_LOCATION ("warmelt-first.melt:3796:/ clear");
		   /*clear *//*_.NEWMAPDICT__V8*/ meltfptr[7] = 0;
			    /*epilog */
			  }
			  ;
			}
		      ;

		      MELT_LOCATION ("warmelt-first.melt:3791:/ clear");
		 /*clear *//*_.MAPDICT__V7*/ meltfptr[6] = 0;
		      /*^clear */
		 /*clear *//*_#IS_MAPOBJECT__L4*/ meltfnum[3] = 0;
		      /*epilog */
		    }
		    ;
		  }
		else
		  {
		    MELT_LOCATION ("warmelt-first.melt:3790:/ cond.else");

		    /*^block */
		    /*anyblock */
		    {


		      {
			MELT_LOCATION ("warmelt-first.melt:3801:/ locexp");

#if MELT_HAVE_DEBUG
			if (melt_need_debug (0))
			  melt_dbgshortbacktrace (("INSTALL_METHOD failing on non-function"), (20));
#endif
			;
		      }
		      ;

		      {
			MELT_LOCATION ("warmelt-first.melt:3804:/ locexp");
			/* ERROR_NON_FUN__1 */
			error
			  ("MELT INSTALL_METHOD ERROR [#%ld] non-function in discriminant %s for selector %s",
			   melt_dbgcounter,
			   melt_string_str ((melt_ptr_t) /*_.DISNAME__V5*/
					    meltfptr[4]),
			   melt_string_str ((melt_ptr_t) /*_.SELNAME__V6*/
					    meltfptr[5]));
			;
		      }
		      ;
		      MELT_LOCATION ("warmelt-first.melt:3800:/ quasiblock");


		      /*epilog */
		    }
		    ;
		  }
		;

		MELT_LOCATION ("warmelt-first.melt:3787:/ clear");
	       /*clear *//*_.SELNAME__V6*/ meltfptr[5] = 0;
		/*^clear */
	       /*clear *//*_#IS_CLOSURE__L3*/ meltfnum[2] = 0;
		/*epilog */
	      }
	      ;
	    }
	  else
	    {
	      MELT_LOCATION ("warmelt-first.melt:3786:/ cond.else");

	      /*^block */
	      /*anyblock */
	      {

		MELT_LOCATION ("warmelt-first.melt:3811:/ checksignal");
		MELT_CHECK_SIGNAL ();
		;
     /*_#IS_A__L5*/ meltfnum[3] =
		  melt_is_instance_of ((melt_ptr_t)
				       ( /*_.SEL__V3*/ meltfptr[2]),
				       (melt_ptr_t) (( /*!CLASS_NAMED */
						      meltfrout->
						      tabval[3])));;
		MELT_LOCATION ("warmelt-first.melt:3811:/ cond");
		/*cond */ if ( /*_#IS_A__L5*/ meltfnum[3])	/*then */
		  {
		    /*^cond.then */
		    /*^block */
		    /*anyblock */
		    {

		      MELT_LOCATION ("warmelt-first.melt:3812:/ quasiblock");


		      /*^getslot */
		      {
			melt_ptr_t slot = NULL, obj = NULL;
			obj =
			  (melt_ptr_t) ( /*_.SEL__V3*/ meltfptr[2]) /*=obj*/ ;
			melt_object_get_field (slot, obj, 1, "NAMED_NAME");
	/*_.BADSELNAM__V9*/ meltfptr[7] = slot;
		      };
		      ;

		      {
			MELT_LOCATION ("warmelt-first.melt:3814:/ locexp");

#if MELT_HAVE_DEBUG
			if (melt_need_debug (0))
			  melt_dbgshortbacktrace (("INSTALL_METHOD failing with bad named selector"), (20));
#endif
			;
		      }
		      ;

		      {
			MELT_LOCATION ("warmelt-first.melt:3817:/ locexp");
			/* ERROR_BAD_NAMED_SEL__1 */
			error
			  ("MELT INSTALL_METHOD ERROR [#%ld] bad named selector %s in discriminant %s",
			   melt_dbgcounter,
			   melt_string_str ((melt_ptr_t) /*_.BADSELNAM__V9*/
					    meltfptr[7]),
			   melt_string_str ((melt_ptr_t) /*_.DISNAME__V5*/
					    meltfptr[4]));
			;
		      }
		      ;

		      MELT_LOCATION ("warmelt-first.melt:3812:/ clear");
		 /*clear *//*_.BADSELNAM__V9*/ meltfptr[7] = 0;
		      /*epilog */
		    }
		    ;
		  }
		else
		  {
		    MELT_LOCATION ("warmelt-first.melt:3811:/ cond.else");

		    /*^block */
		    /*anyblock */
		    {


		      {
			MELT_LOCATION ("warmelt-first.melt:3825:/ locexp");

#if MELT_HAVE_DEBUG
			if (melt_need_debug (0))
			  melt_dbgshortbacktrace (("INSTALL_METHOD failing with bad selector"), (20));
#endif
			;
		      }
		      ;

		      {
			MELT_LOCATION ("warmelt-first.melt:3828:/ locexp");
			/* ERROR_BAD_SEL__1 */
			error
			  ("MELT INSTALL_METHOD ERROR [#%ld] bad selector in discriminant %s",
			   melt_dbgcounter,
			   melt_string_str ((melt_ptr_t) /*_.DISNAME__V5*/
					    meltfptr[4]));
			;
		      }
		      ;
		      MELT_LOCATION ("warmelt-first.melt:3824:/ quasiblock");


		      /*epilog */
		    }
		    ;
		  }
		;
		/*epilog */

		MELT_LOCATION ("warmelt-first.melt:3786:/ clear");
	       /*clear *//*_#IS_A__L5*/ meltfnum[3] = 0;
	      }
	      ;
	    }
	  ;

	  MELT_LOCATION ("warmelt-first.melt:3783:/ clear");
	     /*clear *//*_.DISNAME__V5*/ meltfptr[4] = 0;
	  /*^clear */
	     /*clear *//*_#IS_A__L2*/ meltfnum[1] = 0;
	  /*epilog */
	}
	;
      }
    else
      {
	MELT_LOCATION ("warmelt-first.melt:3782:/ cond.else");

	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-first.melt:3837:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
   /*_#IS_A__L6*/ meltfnum[2] =
	    melt_is_instance_of ((melt_ptr_t) ( /*_.DIS__V2*/ meltfptr[1]),
				 (melt_ptr_t) (( /*!CLASS_NAMED */ meltfrout->
						tabval[3])));;
	  MELT_LOCATION ("warmelt-first.melt:3837:/ cond");
	  /*cond */ if ( /*_#IS_A__L6*/ meltfnum[2])	/*then */
	    {
	      /*^cond.then */
	      /*^block */
	      /*anyblock */
	      {

		MELT_LOCATION ("warmelt-first.melt:3838:/ quasiblock");


		/*^getslot */
		{
		  melt_ptr_t slot = NULL, obj = NULL;
		  obj = (melt_ptr_t) ( /*_.DIS__V2*/ meltfptr[1]) /*=obj*/ ;
		  melt_object_get_field (slot, obj, 1, "NAMED_NAME");
      /*_.BADDISNAM__V10*/ meltfptr[6] = slot;
		};
		;

		{
		  MELT_LOCATION ("warmelt-first.melt:3840:/ locexp");

#if MELT_HAVE_DEBUG
		  if (melt_need_debug (0))
		    melt_dbgshortbacktrace (("INSTALL_METHOD failing with bad named discriminant"), (20));
#endif
		  ;
		}
		;

		{
		  MELT_LOCATION ("warmelt-first.melt:3843:/ locexp");
		  /* ERROR_BAD_NAMED_DIS__1 */
		  error
		    ("MELT INSTALL_METHOD ERROR [#%ld] bad named discriminant %s",
		     melt_dbgcounter,
		     melt_string_str ((melt_ptr_t) /*_.BADDISNAM__V10*/
				      meltfptr[6]));
		  ;
		}
		;

		MELT_LOCATION ("warmelt-first.melt:3838:/ clear");
	       /*clear *//*_.BADDISNAM__V10*/ meltfptr[6] = 0;
		/*epilog */
	      }
	      ;
	    }
	  else
	    {
	      MELT_LOCATION ("warmelt-first.melt:3837:/ cond.else");

	      /*^block */
	      /*anyblock */
	      {


		{
		  MELT_LOCATION ("warmelt-first.melt:3850:/ locexp");

#if MELT_HAVE_DEBUG
		  if (melt_need_debug (0))
		    melt_dbgshortbacktrace (("INSTALL_METHOD failing with bad discriminant"), (20));
#endif
		  ;
		}
		;

		{
		  MELT_LOCATION ("warmelt-first.melt:3853:/ locexp");
		  /* ERROR_BAD_DIS__1 */
		  error ("MELT INSTALL_METHOD ERROR [#%ld] bad discriminant",
			 melt_dbgcounter);
		  ;
		}
		;
		MELT_LOCATION ("warmelt-first.melt:3849:/ quasiblock");


		/*epilog */
	      }
	      ;
	    }
	  ;
	  /*epilog */

	  MELT_LOCATION ("warmelt-first.melt:3782:/ clear");
	     /*clear *//*_#IS_A__L6*/ meltfnum[2] = 0;
	}
	;
      }
    ;
    /*epilog */

    MELT_LOCATION ("warmelt-first.melt:3779:/ clear");
	   /*clear *//*_#IS_A__L1*/ meltfnum[0] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("INSTALL_METHOD", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_54_warmelt_first_INSTALL_METHOD_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*noretval */ NULL);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_54_warmelt_first_INSTALL_METHOD */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_55_warmelt_first_COMPARE_OBJ_RANKED (meltclosure_ptr_t meltclosp_,
					      melt_ptr_t meltfirstargp_,
					      const melt_argdescr_cell_t
					      meltxargdescr_[],
					      union meltparam_un *
					      meltxargtab_,
					      const melt_argdescr_cell_t
					      meltxresdescr_[],
					      union meltparam_un *
					      meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_55_warmelt_first_COMPARE_OBJ_RANKED_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_55_warmelt_first_COMPARE_OBJ_RANKED_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 76
    melt_ptr_t mcfr_varptr[76];
#define MELTFRAM_NBVARNUM 25
    long mcfr_varnum[25];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_55_warmelt_first_COMPARE_OBJ_RANKED is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_55_warmelt_first_COMPARE_OBJ_RANKED_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 76; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_55_warmelt_first_COMPARE_OBJ_RANKED nbval 76*/
  meltfram__.mcfr_nbvar = 76 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("COMPARE_OBJ_RANKED", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-first.melt:3863:/ getarg");
 /*_.X1__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.BXRK1__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.BXRK1__V3*/ meltfptr[2])) !=
	      NULL);


  /*getarg#2 */
  /*^getarg */
  if (meltxargdescr_[1] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.X2__V4*/ meltfptr[3] =
    (meltxargtab_[1].meltbp_aptr) ? (*(meltxargtab_[1].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.X2__V4*/ meltfptr[3])) != NULL);


  /*getarg#3 */
  /*^getarg */
  if (meltxargdescr_[2] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.BXRK2__V5*/ meltfptr[4] =
    (meltxargtab_[2].meltbp_aptr) ? (*(meltxargtab_[2].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.BXRK2__V5*/ meltfptr[4])) !=
	      NULL);


  /*getarg#4 */
  /*^getarg */
  if (meltxargdescr_[3] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.VLESS__V6*/ meltfptr[5] =
    (meltxargtab_[3].meltbp_aptr) ? (*(meltxargtab_[3].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.VLESS__V6*/ meltfptr[5])) !=
	      NULL);


  /*getarg#5 */
  /*^getarg */
  if (meltxargdescr_[4] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.VEQ__V7*/ meltfptr[6] =
    (meltxargtab_[4].meltbp_aptr) ? (*(meltxargtab_[4].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.VEQ__V7*/ meltfptr[6])) != NULL);


  /*getarg#6 */
  /*^getarg */
  if (meltxargdescr_[5] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.VGREAT__V8*/ meltfptr[7] =
    (meltxargtab_[5].meltbp_aptr) ? (*(meltxargtab_[5].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.VGREAT__V8*/ meltfptr[7])) !=
	      NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {

    MELT_LOCATION ("warmelt-first.melt:3864:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#__L1*/ meltfnum[0] =
      (( /*_.X1__V2*/ meltfptr[1]) == ( /*_.X2__V4*/ meltfptr[3]));;
    MELT_LOCATION ("warmelt-first.melt:3864:/ cond");
    /*cond */ if ( /*_#__L1*/ meltfnum[0])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-first.melt:3865:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^quasiblock */


	  /*_.RETVAL___V1*/ meltfptr[0] = /*_.VEQ__V7*/ meltfptr[6];;

	  {
	    MELT_LOCATION ("warmelt-first.melt:3865:/ locexp");
	    /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
	    if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	      melt_warn_for_no_expected_secondary_results ();
	    /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
	    ;
	  }
	  ;
	  /*^finalreturn */
	  ;
	  /*finalret */ goto labend_rout;
	  /*_.IFELSE___V9*/ meltfptr[8] = /*_.RETURN___V10*/ meltfptr[9];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-first.melt:3864:/ clear");
	     /*clear *//*_.RETURN___V10*/ meltfptr[9] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-first.melt:3866:/ quasiblock");


   /*_.X1DIS__V12*/ meltfptr[11] =
	    ((melt_ptr_t)
	     (melt_discr ((melt_ptr_t) ( /*_.X1__V2*/ meltfptr[1]))));;
	  /*^compute */
   /*_.X2DIS__V13*/ meltfptr[12] =
	    ((melt_ptr_t)
	     (melt_discr ((melt_ptr_t) ( /*_.X2__V4*/ meltfptr[3]))));;
	  /*^compute */
   /*_#RK1__L2*/ meltfnum[1] =
	    (melt_get_int ((melt_ptr_t) ( /*_.BXRK1__V3*/ meltfptr[2])));;
	  /*^compute */
   /*_#RK2__L3*/ meltfnum[2] =
	    (melt_get_int ((melt_ptr_t) ( /*_.BXRK2__V5*/ meltfptr[4])));;
	  MELT_LOCATION ("warmelt-first.melt:3871:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
   /*_#__L4*/ meltfnum[3] =
	    (( /*_.X1DIS__V12*/ meltfptr[11]) !=
	     ( /*_.X2DIS__V13*/ meltfptr[12]));;
	  MELT_LOCATION ("warmelt-first.melt:3871:/ cond");
	  /*cond */ if ( /*_#__L4*/ meltfnum[3])	/*then */
	    {
	      /*^cond.then */
	      /*^block */
	      /*anyblock */
	      {

		MELT_LOCATION ("warmelt-first.melt:3872:/ checksignal");
		MELT_CHECK_SIGNAL ();
		;
		/*^getslot */
		{
		  melt_ptr_t slot = NULL, obj = NULL;
		  obj =
		    (melt_ptr_t) ( /*_.X1DIS__V12*/ meltfptr[11]) /*=obj*/ ;
		  melt_object_get_field (slot, obj, 1, "NAMED_NAME");
      /*_.NAMED_NAME__V15*/ meltfptr[14] = slot;
		};
		;
		MELT_LOCATION ("warmelt-first.melt:3873:/ getslot");
		{
		  melt_ptr_t slot = NULL, obj = NULL;
		  obj =
		    (melt_ptr_t) ( /*_.X2DIS__V13*/ meltfptr[12]) /*=obj*/ ;
		  melt_object_get_field (slot, obj, 1, "NAMED_NAME");
      /*_.NAMED_NAME__V16*/ meltfptr[15] = slot;
		};
		;
     /*_#STRING___L5*/ meltfnum[4] =
		  melt_string_less ((melt_ptr_t)
				    ( /*_.NAMED_NAME__V15*/ meltfptr[14]),
				    (melt_ptr_t) ( /*_.NAMED_NAME__V16*/
						  meltfptr[15]));;
		MELT_LOCATION ("warmelt-first.melt:3872:/ cond");
		/*cond */ if ( /*_#STRING___L5*/ meltfnum[4])	/*then */
		  {
		    /*^cond.then */
		    /*^block */
		    /*anyblock */
		    {

		      MELT_LOCATION ("warmelt-first.melt:3874:/ checksignal");
		      MELT_CHECK_SIGNAL ();
		      ;
		      /*^quasiblock */


		      /*_.RETVAL___V1*/ meltfptr[0] =
			/*_.VLESS__V6*/ meltfptr[5];;

		      {
			MELT_LOCATION ("warmelt-first.melt:3874:/ locexp");
			/*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
			if (meltxresdescr_ && meltxresdescr_[0]
			    && meltxrestab_)
			  melt_warn_for_no_expected_secondary_results ();
			/* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
			;
		      }
		      ;
		      /*^finalreturn */
		      ;
		      /*finalret */ goto labend_rout;
		      /*_.IFELSE___V17*/ meltfptr[16] =
			/*_.RETURN___V18*/ meltfptr[17];;
		      /*epilog */

		      MELT_LOCATION ("warmelt-first.melt:3872:/ clear");
		 /*clear *//*_.RETURN___V18*/ meltfptr[17] = 0;
		    }
		    ;
		  }
		else
		  {		/*^cond.else */

		    /*^block */
		    /*anyblock */
		    {

		      MELT_LOCATION ("warmelt-first.melt:3875:/ checksignal");
		      MELT_CHECK_SIGNAL ();
		      ;
		      /*^quasiblock */


		      /*_.RETVAL___V1*/ meltfptr[0] =
			/*_.VGREAT__V8*/ meltfptr[7];;

		      {
			MELT_LOCATION ("warmelt-first.melt:3875:/ locexp");
			/*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
			if (meltxresdescr_ && meltxresdescr_[0]
			    && meltxrestab_)
			  melt_warn_for_no_expected_secondary_results ();
			/* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
			;
		      }
		      ;
		      /*^finalreturn */
		      ;
		      /*finalret */ goto labend_rout;
		      /*_.IFELSE___V17*/ meltfptr[16] =
			/*_.RETURN___V19*/ meltfptr[17];;
		      /*epilog */

		      MELT_LOCATION ("warmelt-first.melt:3872:/ clear");
		 /*clear *//*_.RETURN___V19*/ meltfptr[17] = 0;
		    }
		    ;
		  }
		;
		/*_.IFELSE___V14*/ meltfptr[13] =
		  /*_.IFELSE___V17*/ meltfptr[16];;
		/*epilog */

		MELT_LOCATION ("warmelt-first.melt:3871:/ clear");
	       /*clear *//*_.NAMED_NAME__V15*/ meltfptr[14] = 0;
		/*^clear */
	       /*clear *//*_.NAMED_NAME__V16*/ meltfptr[15] = 0;
		/*^clear */
	       /*clear *//*_#STRING___L5*/ meltfnum[4] = 0;
		/*^clear */
	       /*clear *//*_.IFELSE___V17*/ meltfptr[16] = 0;
	      }
	      ;
	    }
	  else
	    {			/*^cond.else */

	      /*^block */
	      /*anyblock */
	      {

		MELT_LOCATION ("warmelt-first.melt:3877:/ checksignal");
		MELT_CHECK_SIGNAL ();
		;
     /*_#IS_A__L6*/ meltfnum[4] =
		  melt_is_instance_of ((melt_ptr_t)
				       ( /*_.X1__V2*/ meltfptr[1]),
				       (melt_ptr_t) (( /*!CLASS_CLONED_SYMBOL */ meltfrout->tabval[0])));;
		MELT_LOCATION ("warmelt-first.melt:3877:/ cond");
		/*cond */ if ( /*_#IS_A__L6*/ meltfnum[4])	/*then */
		  {
		    /*^cond.then */
		    /*^block */
		    /*anyblock */
		    {

		      MELT_LOCATION ("warmelt-first.melt:3878:/ quasiblock");


		      /*^getslot */
		      {
			melt_ptr_t slot = NULL, obj = NULL;
			obj =
			  (melt_ptr_t) ( /*_.X1__V2*/ meltfptr[1]) /*=obj*/ ;
			melt_object_get_field (slot, obj, 1, "NAMED_NAME");
	/*_.N1__V22*/ meltfptr[15] = slot;
		      };
		      ;
		      MELT_LOCATION ("warmelt-first.melt:3879:/ getslot");
		      {
			melt_ptr_t slot = NULL, obj = NULL;
			obj =
			  (melt_ptr_t) ( /*_.X2__V4*/ meltfptr[3]) /*=obj*/ ;
			melt_object_get_field (slot, obj, 1, "NAMED_NAME");
	/*_.N2__V23*/ meltfptr[16] = slot;
		      };
		      ;
		      MELT_LOCATION ("warmelt-first.melt:3880:/ checksignal");
		      MELT_CHECK_SIGNAL ();
		      ;
       /*_#STRING___L7*/ meltfnum[6] =
			melt_string_less ((melt_ptr_t)
					  ( /*_.N1__V22*/ meltfptr[15]),
					  (melt_ptr_t) ( /*_.N2__V23*/
							meltfptr[16]));;
		      MELT_LOCATION ("warmelt-first.melt:3880:/ cond");
		      /*cond */ if ( /*_#STRING___L7*/ meltfnum[6])	/*then */
			{
			  /*^cond.then */
			  /*^block */
			  /*anyblock */
			  {

			    MELT_LOCATION
			      ("warmelt-first.melt:3881:/ checksignal");
			    MELT_CHECK_SIGNAL ();
			    ;
			    /*^quasiblock */


			    /*_.RETVAL___V1*/ meltfptr[0] =
			      /*_.VLESS__V6*/ meltfptr[5];;

			    {
			      MELT_LOCATION
				("warmelt-first.melt:3881:/ locexp");
			      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
			      if (meltxresdescr_ && meltxresdescr_[0]
				  && meltxrestab_)
				melt_warn_for_no_expected_secondary_results
				  ();
			      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
			      ;
			    }
			    ;
			    /*^finalreturn */
			    ;
			    /*finalret */ goto labend_rout;
			    /*_.IFELSE___V24*/ meltfptr[23] =
			      /*_.RETURN___V25*/ meltfptr[24];;
			    /*epilog */

			    MELT_LOCATION ("warmelt-first.melt:3880:/ clear");
		   /*clear *//*_.RETURN___V25*/ meltfptr[24] = 0;
			  }
			  ;
			}
		      else
			{	/*^cond.else */

			  /*^block */
			  /*anyblock */
			  {

			    MELT_LOCATION
			      ("warmelt-first.melt:3882:/ checksignal");
			    MELT_CHECK_SIGNAL ();
			    ;
	 /*_#STRING___L8*/ meltfnum[7] =
			      melt_string_less ((melt_ptr_t)
						( /*_.N2__V23*/ meltfptr[16]),
						(melt_ptr_t) ( /*_.N1__V22*/
							      meltfptr[15]));;
			    MELT_LOCATION ("warmelt-first.melt:3882:/ cond");
			    /*cond */ if ( /*_#STRING___L8*/ meltfnum[7])	/*then */
			      {
				/*^cond.then */
				/*^block */
				/*anyblock */
				{

				  MELT_LOCATION
				    ("warmelt-first.melt:3883:/ checksignal");
				  MELT_CHECK_SIGNAL ();
				  ;
				  /*^quasiblock */


				  /*_.RETVAL___V1*/ meltfptr[0] =
				    /*_.VGREAT__V8*/ meltfptr[7];;

				  {
				    MELT_LOCATION
				      ("warmelt-first.melt:3883:/ locexp");
				    /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
				    if (meltxresdescr_ && meltxresdescr_[0]
					&& meltxrestab_)
				      melt_warn_for_no_expected_secondary_results
					();
				    /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
				    ;
				  }
				  ;
				  /*^finalreturn */
				  ;
				  /*finalret */ goto labend_rout;
				  /*_.IFELSE___V26*/ meltfptr[24] =
				    /*_.RETURN___V27*/ meltfptr[26];;
				  /*epilog */

				  MELT_LOCATION
				    ("warmelt-first.melt:3882:/ clear");
		     /*clear *//*_.RETURN___V27*/ meltfptr[26] =
				    0;
				}
				;
			      }
			    else
			      {	/*^cond.else */

				/*^block */
				/*anyblock */
				{

				  MELT_LOCATION
				    ("warmelt-first.melt:3885:/ quasiblock");


				  MELT_LOCATION
				    ("warmelt-first.melt:3886:/ getslot");
				  {
				    melt_ptr_t slot = NULL, obj = NULL;
				    obj =
				      (melt_ptr_t) ( /*_.X1__V2*/ meltfptr[1])
				      /*=obj*/ ;
				    melt_object_get_field (slot, obj, 3,
							   "CSYM_URANK");
	    /*_.YR1__V29*/ meltfptr[28] = slot;
				  };
				  ;
				  MELT_LOCATION
				    ("warmelt-first.melt:3887:/ getslot");
				  {
				    melt_ptr_t slot = NULL, obj = NULL;
				    obj =
				      (melt_ptr_t) ( /*_.X2__V4*/ meltfptr[3])
				      /*=obj*/ ;
				    melt_object_get_field (slot, obj, 3,
							   "CSYM_URANK");
	    /*_.YR2__V30*/ meltfptr[29] = slot;
				  };
				  ;
	   /*_#NR1__L9*/ meltfnum[8] =
				    (melt_get_int
				     ((melt_ptr_t)
				      ( /*_.YR1__V29*/ meltfptr[28])));;
				  /*^compute */
	   /*_#NR2__L10*/ meltfnum[9] =
				    (melt_get_int
				     ((melt_ptr_t)
				      ( /*_.YR2__V30*/ meltfptr[29])));;
				  MELT_LOCATION
				    ("warmelt-first.melt:3891:/ checksignal");
				  MELT_CHECK_SIGNAL ();
				  ;
	   /*_#I__L11*/ meltfnum[10] =
				    (( /*_#NR1__L9*/ meltfnum[8]) <
				     ( /*_#NR2__L10*/ meltfnum[9]));;
				  MELT_LOCATION
				    ("warmelt-first.melt:3891:/ cond");
				  /*cond */ if ( /*_#I__L11*/ meltfnum[10])	/*then */
				    {
				      /*^cond.then */
				      /*^block */
				      /*anyblock */
				      {

					MELT_LOCATION
					  ("warmelt-first.melt:3892:/ checksignal");
					MELT_CHECK_SIGNAL ();
					;
					/*^quasiblock */


					/*_.RETVAL___V1*/ meltfptr[0] =
					  /*_.VLESS__V6*/ meltfptr[5];;

					{
					  MELT_LOCATION
					    ("warmelt-first.melt:3892:/ locexp");
					  /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
					  if (meltxresdescr_
					      && meltxresdescr_[0]
					      && meltxrestab_)
					    melt_warn_for_no_expected_secondary_results
					      ();
					  /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
					  ;
					}
					;
					/*^finalreturn */
					;
					/*finalret */ goto labend_rout;
					/*_.IFELSE___V31*/ meltfptr[30] =
					  /*_.RETURN___V32*/ meltfptr[31];;
					/*epilog */

					MELT_LOCATION
					  ("warmelt-first.melt:3891:/ clear");
		       /*clear *//*_.RETURN___V32*/
					  meltfptr[31] = 0;
				      }
				      ;
				    }
				  else
				    {	/*^cond.else */

				      /*^block */
				      /*anyblock */
				      {

					MELT_LOCATION
					  ("warmelt-first.melt:3893:/ checksignal");
					MELT_CHECK_SIGNAL ();
					;
	     /*_#I__L12*/ meltfnum[11] =
					  (( /*_#NR1__L9*/ meltfnum[8]) >
					   ( /*_#NR2__L10*/ meltfnum[9]));;
					MELT_LOCATION
					  ("warmelt-first.melt:3893:/ cond");
					/*cond */ if ( /*_#I__L12*/ meltfnum[11])	/*then */
					  {
					    /*^cond.then */
					    /*^block */
					    /*anyblock */
					    {

					      MELT_LOCATION
						("warmelt-first.melt:3894:/ checksignal");
					      MELT_CHECK_SIGNAL ();
					      ;
					      /*^quasiblock */


					      /*_.RETVAL___V1*/ meltfptr[0] =
						/*_.VGREAT__V8*/ meltfptr[7];;

					      {
						MELT_LOCATION
						  ("warmelt-first.melt:3894:/ locexp");
						/*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
						if (meltxresdescr_
						    && meltxresdescr_[0]
						    && meltxrestab_)
						  melt_warn_for_no_expected_secondary_results
						    ();
						/* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
						;
					      }
					      ;
					      /*^finalreturn */
					      ;
					      /*finalret */ goto labend_rout;
					      /*_.IFELSE___V33*/ meltfptr[31]
						=
						/*_.RETURN___V34*/
						meltfptr[33];;
					      /*epilog */

					      MELT_LOCATION
						("warmelt-first.melt:3893:/ clear");
			 /*clear *//*_.RETURN___V34*/
						meltfptr[33] = 0;
					    }
					    ;
					  }
					else
					  {	/*^cond.else */

					    /*^block */
					    /*anyblock */
					    {


#if MELT_HAVE_DEBUG
					      MELT_LOCATION
						("warmelt-first.melt:3897:/ cppif.then");
					      /*^block */
					      /*anyblock */
					      {

						/*^checksignal */
						MELT_CHECK_SIGNAL ();
						;
						/*^cond */
						/*cond */ if (( /*nil */ NULL))	/*then */
						  {
						    /*^cond.then */
						    /*_.IFELSE___V36*/
						      meltfptr[35] =
						      ( /*nil */ NULL);;
						  }
						else
						  {
						    MELT_LOCATION
						      ("warmelt-first.melt:3897:/ cond.else");

						    /*^block */
						    /*anyblock */
						    {




						      {
							/*^locexp */
							melt_assert_failed (("corrupted same cloned symbols"), ("warmelt-first.melt") ? ("warmelt-first.melt") : __FILE__, (3897) ? (3897) : __LINE__, __FUNCTION__);
							;
						      }
						      ;
			     /*clear *//*_.IFELSE___V36*/
							meltfptr[35] = 0;
						      /*epilog */
						    }
						    ;
						  }
						;
						/*^compute */
						/*_.IFCPP___V35*/ meltfptr[33]
						  =
						  /*_.IFELSE___V36*/
						  meltfptr[35];;
						/*epilog */

						MELT_LOCATION
						  ("warmelt-first.melt:3897:/ clear");
			   /*clear *//*_.IFELSE___V36*/
						  meltfptr[35] = 0;
					      }

#else /*MELT_HAVE_DEBUG */
					      /*^cppif.else */
					      /*_.IFCPP___V35*/ meltfptr[33] =
						( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
					      ;
					      MELT_LOCATION
						("warmelt-first.melt:3898:/ checksignal");
					      MELT_CHECK_SIGNAL ();
					      ;
					      /*^quasiblock */


					      /*_.RETVAL___V1*/ meltfptr[0] =
						( /*nil */ NULL);;

					      {
						MELT_LOCATION
						  ("warmelt-first.melt:3898:/ locexp");
						/*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
						if (meltxresdescr_
						    && meltxresdescr_[0]
						    && meltxrestab_)
						  melt_warn_for_no_expected_secondary_results
						    ();
						/* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
						;
					      }
					      ;
					      /*^finalreturn */
					      ;
					      /*finalret */ goto labend_rout;
					      MELT_LOCATION
						("warmelt-first.melt:3895:/ quasiblock");


					      /*_.PROGN___V38*/ meltfptr[37] =
						/*_.RETURN___V37*/
						meltfptr[35];;
					      /*^compute */
					      /*_.IFELSE___V33*/ meltfptr[31]
						=
						/*_.PROGN___V38*/
						meltfptr[37];;
					      /*epilog */

					      MELT_LOCATION
						("warmelt-first.melt:3893:/ clear");
			 /*clear *//*_.IFCPP___V35*/
						meltfptr[33] = 0;
					      /*^clear */
			 /*clear *//*_.RETURN___V37*/
						meltfptr[35] = 0;
					      /*^clear */
			 /*clear *//*_.PROGN___V38*/
						meltfptr[37] = 0;
					    }
					    ;
					  }
					;
					/*_.IFELSE___V31*/ meltfptr[30] =
					  /*_.IFELSE___V33*/ meltfptr[31];;
					/*epilog */

					MELT_LOCATION
					  ("warmelt-first.melt:3891:/ clear");
		       /*clear *//*_#I__L12*/ meltfnum[11] =
					  0;
					/*^clear */
		       /*clear *//*_.IFELSE___V33*/
					  meltfptr[31] = 0;
				      }
				      ;
				    }
				  ;
				  /*_.LET___V28*/ meltfptr[26] =
				    /*_.IFELSE___V31*/ meltfptr[30];;

				  MELT_LOCATION
				    ("warmelt-first.melt:3885:/ clear");
		     /*clear *//*_.YR1__V29*/ meltfptr[28] = 0;
				  /*^clear */
		     /*clear *//*_.YR2__V30*/ meltfptr[29] = 0;
				  /*^clear */
		     /*clear *//*_#NR1__L9*/ meltfnum[8] = 0;
				  /*^clear */
		     /*clear *//*_#NR2__L10*/ meltfnum[9] = 0;
				  /*^clear */
		     /*clear *//*_#I__L11*/ meltfnum[10] = 0;
				  /*^clear */
		     /*clear *//*_.IFELSE___V31*/ meltfptr[30] =
				    0;
				  MELT_LOCATION
				    ("warmelt-first.melt:3884:/ quasiblock");


				  /*_.PROGN___V39*/ meltfptr[33] =
				    /*_.LET___V28*/ meltfptr[26];;
				  /*^compute */
				  /*_.IFELSE___V26*/ meltfptr[24] =
				    /*_.PROGN___V39*/ meltfptr[33];;
				  /*epilog */

				  MELT_LOCATION
				    ("warmelt-first.melt:3882:/ clear");
		     /*clear *//*_.LET___V28*/ meltfptr[26] = 0;
				  /*^clear */
		     /*clear *//*_.PROGN___V39*/ meltfptr[33] =
				    0;
				}
				;
			      }
			    ;
			    /*_.IFELSE___V24*/ meltfptr[23] =
			      /*_.IFELSE___V26*/ meltfptr[24];;
			    /*epilog */

			    MELT_LOCATION ("warmelt-first.melt:3880:/ clear");
		   /*clear *//*_#STRING___L8*/ meltfnum[7] = 0;
			    /*^clear */
		   /*clear *//*_.IFELSE___V26*/ meltfptr[24] = 0;
			  }
			  ;
			}
		      ;
		      /*_.LET___V21*/ meltfptr[14] =
			/*_.IFELSE___V24*/ meltfptr[23];;

		      MELT_LOCATION ("warmelt-first.melt:3878:/ clear");
		 /*clear *//*_.N1__V22*/ meltfptr[15] = 0;
		      /*^clear */
		 /*clear *//*_.N2__V23*/ meltfptr[16] = 0;
		      /*^clear */
		 /*clear *//*_#STRING___L7*/ meltfnum[6] = 0;
		      /*^clear */
		 /*clear *//*_.IFELSE___V24*/ meltfptr[23] = 0;
		      /*_.IFELSE___V20*/ meltfptr[17] =
			/*_.LET___V21*/ meltfptr[14];;
		      /*epilog */

		      MELT_LOCATION ("warmelt-first.melt:3877:/ clear");
		 /*clear *//*_.LET___V21*/ meltfptr[14] = 0;
		    }
		    ;
		  }
		else
		  {		/*^cond.else */

		    /*^block */
		    /*anyblock */
		    {

		      MELT_LOCATION ("warmelt-first.melt:3901:/ checksignal");
		      MELT_CHECK_SIGNAL ();
		      ;
       /*_#IS_A__L13*/ meltfnum[11] =
			melt_is_instance_of ((melt_ptr_t)
					     ( /*_.X1__V2*/ meltfptr[1]),
					     (melt_ptr_t) (( /*!CLASS_NAMED */
							    meltfrout->
							    tabval[1])));;
		      MELT_LOCATION ("warmelt-first.melt:3901:/ cond");
		      /*cond */ if ( /*_#IS_A__L13*/ meltfnum[11])	/*then */
			{
			  /*^cond.then */
			  /*^block */
			  /*anyblock */
			  {

			    MELT_LOCATION
			      ("warmelt-first.melt:3902:/ quasiblock");


			    /*^getslot */
			    {
			      melt_ptr_t slot = NULL, obj = NULL;
			      obj =
				(melt_ptr_t) ( /*_.X1__V2*/ meltfptr[1])
				/*=obj*/ ;
			      melt_object_get_field (slot, obj, 1,
						     "NAMED_NAME");
	  /*_.N1__V42*/ meltfptr[31] = slot;
			    };
			    ;
			    MELT_LOCATION
			      ("warmelt-first.melt:3903:/ getslot");
			    {
			      melt_ptr_t slot = NULL, obj = NULL;
			      obj =
				(melt_ptr_t) ( /*_.X2__V4*/ meltfptr[3])
				/*=obj*/ ;
			      melt_object_get_field (slot, obj, 1,
						     "NAMED_NAME");
	  /*_.N2__V43*/ meltfptr[28] = slot;
			    };
			    ;
			    MELT_LOCATION
			      ("warmelt-first.melt:3905:/ checksignal");
			    MELT_CHECK_SIGNAL ();
			    ;
	 /*_#STRING___L14*/ meltfnum[8] =
			      melt_string_less ((melt_ptr_t)
						( /*_.N1__V42*/ meltfptr[31]),
						(melt_ptr_t) ( /*_.N2__V43*/
							      meltfptr[28]));;
			    MELT_LOCATION ("warmelt-first.melt:3905:/ cond");
			    /*cond */ if ( /*_#STRING___L14*/ meltfnum[8])	/*then */
			      {
				/*^cond.then */
				/*^block */
				/*anyblock */
				{

				  MELT_LOCATION
				    ("warmelt-first.melt:3906:/ checksignal");
				  MELT_CHECK_SIGNAL ();
				  ;
				  /*^quasiblock */


				  /*_.RETVAL___V1*/ meltfptr[0] =
				    /*_.VLESS__V6*/ meltfptr[5];;

				  {
				    MELT_LOCATION
				      ("warmelt-first.melt:3906:/ locexp");
				    /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
				    if (meltxresdescr_ && meltxresdescr_[0]
					&& meltxrestab_)
				      melt_warn_for_no_expected_secondary_results
					();
				    /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
				    ;
				  }
				  ;
				  /*^finalreturn */
				  ;
				  /*finalret */ goto labend_rout;
				  /*_.IFELSE___V44*/ meltfptr[29] =
				    /*_.RETURN___V45*/ meltfptr[30];;
				  /*epilog */

				  MELT_LOCATION
				    ("warmelt-first.melt:3905:/ clear");
		     /*clear *//*_.RETURN___V45*/ meltfptr[30] =
				    0;
				}
				;
			      }
			    else
			      {	/*^cond.else */

				/*^block */
				/*anyblock */
				{

				  MELT_LOCATION
				    ("warmelt-first.melt:3907:/ checksignal");
				  MELT_CHECK_SIGNAL ();
				  ;
	   /*_#STRING___L15*/ meltfnum[9] =
				    melt_string_less ((melt_ptr_t)
						      ( /*_.N2__V43*/
						       meltfptr[28]),
						      (melt_ptr_t) ( /*_.N1__V42*/ meltfptr[31]));;
				  MELT_LOCATION
				    ("warmelt-first.melt:3907:/ cond");
				  /*cond */ if ( /*_#STRING___L15*/ meltfnum[9])	/*then */
				    {
				      /*^cond.then */
				      /*^block */
				      /*anyblock */
				      {

					MELT_LOCATION
					  ("warmelt-first.melt:3908:/ checksignal");
					MELT_CHECK_SIGNAL ();
					;
					/*^quasiblock */


					/*_.RETVAL___V1*/ meltfptr[0] =
					  /*_.VGREAT__V8*/ meltfptr[7];;

					{
					  MELT_LOCATION
					    ("warmelt-first.melt:3908:/ locexp");
					  /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
					  if (meltxresdescr_
					      && meltxresdescr_[0]
					      && meltxrestab_)
					    melt_warn_for_no_expected_secondary_results
					      ();
					  /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
					  ;
					}
					;
					/*^finalreturn */
					;
					/*finalret */ goto labend_rout;
					/*_.IFELSE___V46*/ meltfptr[26] =
					  /*_.RETURN___V47*/ meltfptr[33];;
					/*epilog */

					MELT_LOCATION
					  ("warmelt-first.melt:3907:/ clear");
		       /*clear *//*_.RETURN___V47*/
					  meltfptr[33] = 0;
				      }
				      ;
				    }
				  else
				    {	/*^cond.else */

				      /*^block */
				      /*anyblock */
				      {

					MELT_LOCATION
					  ("warmelt-first.melt:3909:/ checksignal");
					MELT_CHECK_SIGNAL ();
					;
	     /*_#I__L16*/ meltfnum[10] =
					  (( /*_#RK1__L2*/ meltfnum[1]) <
					   ( /*_#RK2__L3*/ meltfnum[2]));;
					MELT_LOCATION
					  ("warmelt-first.melt:3909:/ cond");
					/*cond */ if ( /*_#I__L16*/ meltfnum[10])	/*then */
					  {
					    /*^cond.then */
					    /*^block */
					    /*anyblock */
					    {

					      MELT_LOCATION
						("warmelt-first.melt:3910:/ checksignal");
					      MELT_CHECK_SIGNAL ();
					      ;
					      /*^quasiblock */


					      /*_.RETVAL___V1*/ meltfptr[0] =
						/*_.VLESS__V6*/ meltfptr[5];;

					      {
						MELT_LOCATION
						  ("warmelt-first.melt:3910:/ locexp");
						/*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
						if (meltxresdescr_
						    && meltxresdescr_[0]
						    && meltxrestab_)
						  melt_warn_for_no_expected_secondary_results
						    ();
						/* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
						;
					      }
					      ;
					      /*^finalreturn */
					      ;
					      /*finalret */ goto labend_rout;
					      /*_.IFELSE___V48*/ meltfptr[24]
						=
						/*_.RETURN___V49*/
						meltfptr[15];;
					      /*epilog */

					      MELT_LOCATION
						("warmelt-first.melt:3909:/ clear");
			 /*clear *//*_.RETURN___V49*/
						meltfptr[15] = 0;
					    }
					    ;
					  }
					else
					  {	/*^cond.else */

					    /*^block */
					    /*anyblock */
					    {

					      MELT_LOCATION
						("warmelt-first.melt:3911:/ checksignal");
					      MELT_CHECK_SIGNAL ();
					      ;
	       /*_#I__L17*/ meltfnum[7] =
						(( /*_#RK1__L2*/ meltfnum[1])
						 >
						 ( /*_#RK2__L3*/
						  meltfnum[2]));;
					      MELT_LOCATION
						("warmelt-first.melt:3911:/ cond");
					      /*cond */ if ( /*_#I__L17*/ meltfnum[7])	/*then */
						{
						  /*^cond.then */
						  /*^block */
						  /*anyblock */
						  {

						    MELT_LOCATION
						      ("warmelt-first.melt:3912:/ checksignal");
						    MELT_CHECK_SIGNAL ();
						    ;
						    /*^quasiblock */


						    /*_.RETVAL___V1*/
						      meltfptr[0] =
						      /*_.VGREAT__V8*/
						      meltfptr[7];;

						    {
						      MELT_LOCATION
							("warmelt-first.melt:3912:/ locexp");
						      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
						      if (meltxresdescr_
							  && meltxresdescr_[0]
							  && meltxrestab_)
							melt_warn_for_no_expected_secondary_results
							  ();
						      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
						      ;
						    }
						    ;
						    /*^finalreturn */
						    ;
						    /*finalret */ goto
						      labend_rout;
						    /*_.IFELSE___V50*/
						      meltfptr[16] =
						      /*_.RETURN___V51*/
						      meltfptr[23];;
						    /*epilog */

						    MELT_LOCATION
						      ("warmelt-first.melt:3911:/ clear");
			   /*clear *//*_.RETURN___V51*/
						      meltfptr[23] = 0;
						  }
						  ;
						}
					      else
						{	/*^cond.else */

						  /*^block */
						  /*anyblock */
						  {

						    MELT_LOCATION
						      ("warmelt-first.melt:3914:/ checksignal");
						    MELT_CHECK_SIGNAL ();
						    ;
						    /*^quasiblock */


						    /*_.RETVAL___V1*/
						      meltfptr[0] =
						      /*_.VEQ__V7*/
						      meltfptr[6];;

						    {
						      MELT_LOCATION
							("warmelt-first.melt:3914:/ locexp");
						      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
						      if (meltxresdescr_
							  && meltxresdescr_[0]
							  && meltxrestab_)
							melt_warn_for_no_expected_secondary_results
							  ();
						      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
						      ;
						    }
						    ;
						    /*^finalreturn */
						    ;
						    /*finalret */ goto
						      labend_rout;
						    MELT_LOCATION
						      ("warmelt-first.melt:3913:/ quasiblock");


						    /*_.PROGN___V53*/
						      meltfptr[30] =
						      /*_.RETURN___V52*/
						      meltfptr[14];;
						    /*^compute */
						    /*_.IFELSE___V50*/
						      meltfptr[16] =
						      /*_.PROGN___V53*/
						      meltfptr[30];;
						    /*epilog */

						    MELT_LOCATION
						      ("warmelt-first.melt:3911:/ clear");
			   /*clear *//*_.RETURN___V52*/
						      meltfptr[14] = 0;
						    /*^clear */
			   /*clear *//*_.PROGN___V53*/
						      meltfptr[30] = 0;
						  }
						  ;
						}
					      ;
					      /*_.IFELSE___V48*/ meltfptr[24]
						=
						/*_.IFELSE___V50*/
						meltfptr[16];;
					      /*epilog */

					      MELT_LOCATION
						("warmelt-first.melt:3909:/ clear");
			 /*clear *//*_#I__L17*/
						meltfnum[7] = 0;
					      /*^clear */
			 /*clear *//*_.IFELSE___V50*/
						meltfptr[16] = 0;
					    }
					    ;
					  }
					;
					/*_.IFELSE___V46*/ meltfptr[26] =
					  /*_.IFELSE___V48*/ meltfptr[24];;
					/*epilog */

					MELT_LOCATION
					  ("warmelt-first.melt:3907:/ clear");
		       /*clear *//*_#I__L16*/ meltfnum[10] =
					  0;
					/*^clear */
		       /*clear *//*_.IFELSE___V48*/
					  meltfptr[24] = 0;
				      }
				      ;
				    }
				  ;
				  /*_.IFELSE___V44*/ meltfptr[29] =
				    /*_.IFELSE___V46*/ meltfptr[26];;
				  /*epilog */

				  MELT_LOCATION
				    ("warmelt-first.melt:3905:/ clear");
		     /*clear *//*_#STRING___L15*/ meltfnum[9] =
				    0;
				  /*^clear */
		     /*clear *//*_.IFELSE___V46*/ meltfptr[26] =
				    0;
				}
				;
			      }
			    ;
			    /*_.LET___V41*/ meltfptr[37] =
			      /*_.IFELSE___V44*/ meltfptr[29];;

			    MELT_LOCATION ("warmelt-first.melt:3902:/ clear");
		   /*clear *//*_.N1__V42*/ meltfptr[31] = 0;
			    /*^clear */
		   /*clear *//*_.N2__V43*/ meltfptr[28] = 0;
			    /*^clear */
		   /*clear *//*_#STRING___L14*/ meltfnum[8] = 0;
			    /*^clear */
		   /*clear *//*_.IFELSE___V44*/ meltfptr[29] = 0;
			    /*_.IFELSE___V40*/ meltfptr[35] =
			      /*_.LET___V41*/ meltfptr[37];;
			    /*epilog */

			    MELT_LOCATION ("warmelt-first.melt:3901:/ clear");
		   /*clear *//*_.LET___V41*/ meltfptr[37] = 0;
			  }
			  ;
			}
		      else
			{	/*^cond.else */

			  /*^block */
			  /*anyblock */
			  {

			    MELT_LOCATION
			      ("warmelt-first.melt:3917:/ checksignal");
			    MELT_CHECK_SIGNAL ();
			    ;
	 /*_#IS_A__L18*/ meltfnum[6] =
			      melt_is_instance_of ((melt_ptr_t)
						   ( /*_.X1__V2*/
						    meltfptr[1]),
						   (melt_ptr_t) (( /*!CLASS_ANY_BINDING */ meltfrout->tabval[2])));;
			    MELT_LOCATION ("warmelt-first.melt:3917:/ cond");
			    /*cond */ if ( /*_#IS_A__L18*/ meltfnum[6])	/*then */
			      {
				/*^cond.then */
				/*^block */
				/*anyblock */
				{

				  MELT_LOCATION
				    ("warmelt-first.melt:3918:/ quasiblock");


				  /*^getslot */
				  {
				    melt_ptr_t slot = NULL, obj = NULL;
				    obj =
				      (melt_ptr_t) ( /*_.X1__V2*/ meltfptr[1])
				      /*=obj*/ ;
				    melt_object_get_field (slot, obj, 0,
							   "BINDER");
	    /*_.BSY1__V56*/ meltfptr[23] = slot;
				  };
				  ;
				  MELT_LOCATION
				    ("warmelt-first.melt:3919:/ getslot");
				  {
				    melt_ptr_t slot = NULL, obj = NULL;
				    obj =
				      (melt_ptr_t) ( /*_.X1__V2*/ meltfptr[1])
				      /*=obj*/ ;
				    melt_object_get_field (slot, obj, 0,
							   "BINDER");
	    /*_.BSY2__V57*/ meltfptr[14] = slot;
				  };
				  ;
				  MELT_LOCATION
				    ("warmelt-first.melt:3920:/ checksignal");
				  MELT_CHECK_SIGNAL ();
				  ;
				  /*^apply */
				  /*apply */
				  {
				    union meltparam_un argtab[6];
				    memset (&argtab, 0, sizeof (argtab));
				    /*^apply.arg */
				    argtab[0].meltbp_aptr =
				      (melt_ptr_t *) & /*_.BXRK1__V3*/
				      meltfptr[2];
				    /*^apply.arg */
				    argtab[1].meltbp_aptr =
				      (melt_ptr_t *) & /*_.BSY2__V57*/
				      meltfptr[14];
				    /*^apply.arg */
				    argtab[2].meltbp_aptr =
				      (melt_ptr_t *) & /*_.BXRK2__V5*/
				      meltfptr[4];
				    /*^apply.arg */
				    argtab[3].meltbp_aptr =
				      (melt_ptr_t *) & /*_.VLESS__V6*/
				      meltfptr[5];
				    /*^apply.arg */
				    argtab[4].meltbp_aptr =
				      (melt_ptr_t *) & /*_.VEQ__V7*/
				      meltfptr[6];
				    /*^apply.arg */
				    argtab[5].meltbp_aptr =
				      (melt_ptr_t *) & /*_.VGREAT__V8*/
				      meltfptr[7];
				    /*_.COMPARE_OBJ_RANKED__V58*/ meltfptr[30]
				      =
				      melt_apply ((meltclosure_ptr_t)
						  (( /*!COMPARE_OBJ_RANKED */
						    meltfrout->tabval[3])),
						  (melt_ptr_t) ( /*_.BSY1__V56*/ meltfptr[23]), (MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_PTR ""), argtab, "", (union meltparam_un *) 0);
				  }
				  ;
				  /*^checksignal */
				  MELT_CHECK_SIGNAL ();
				  ;
				  /*^quasiblock */


				  /*_.RETVAL___V1*/ meltfptr[0] =
				    /*_.COMPARE_OBJ_RANKED__V58*/
				    meltfptr[30];;

				  {
				    MELT_LOCATION
				      ("warmelt-first.melt:3920:/ locexp");
				    /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
				    if (meltxresdescr_ && meltxresdescr_[0]
					&& meltxrestab_)
				      melt_warn_for_no_expected_secondary_results
					();
				    /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
				    ;
				  }
				  ;
				  /*^finalreturn */
				  ;
				  /*finalret */ goto labend_rout;
				  /*_.LET___V55*/ meltfptr[15] =
				    /*_.RETURN___V59*/ meltfptr[16];;

				  MELT_LOCATION
				    ("warmelt-first.melt:3918:/ clear");
		     /*clear *//*_.BSY1__V56*/ meltfptr[23] = 0;
				  /*^clear */
		     /*clear *//*_.BSY2__V57*/ meltfptr[14] = 0;
				  /*^clear */
		     /*clear *//*_.COMPARE_OBJ_RANKED__V58*/
				    meltfptr[30] = 0;
				  /*^clear */
		     /*clear *//*_.RETURN___V59*/ meltfptr[16] =
				    0;
				  /*_.IFELSE___V54*/ meltfptr[33] =
				    /*_.LET___V55*/ meltfptr[15];;
				  /*epilog */

				  MELT_LOCATION
				    ("warmelt-first.melt:3917:/ clear");
		     /*clear *//*_.LET___V55*/ meltfptr[15] = 0;
				}
				;
			      }
			    else
			      {	/*^cond.else */

				/*^block */
				/*anyblock */
				{

				  MELT_LOCATION
				    ("warmelt-first.melt:3921:/ checksignal");
				  MELT_CHECK_SIGNAL ();
				  ;
	   /*_#IS_STRING__L19*/ meltfnum[7] =
				    (melt_magic_discr
				     ((melt_ptr_t)
				      ( /*_.X1__V2*/ meltfptr[1])) ==
				     MELTOBMAG_STRING);;
				  MELT_LOCATION
				    ("warmelt-first.melt:3921:/ cond");
				  /*cond */ if ( /*_#IS_STRING__L19*/ meltfnum[7])	/*then */
				    {
				      /*^cond.then */
				      /*^block */
				      /*anyblock */
				      {

					MELT_LOCATION
					  ("warmelt-first.melt:3922:/ checksignal");
					MELT_CHECK_SIGNAL ();
					;
	     /*_#STRING___L20*/ meltfnum[10] =
					  melt_string_less ((melt_ptr_t)
							    ( /*_.X1__V2*/
							     meltfptr[1]),
							    (melt_ptr_t) ( /*_.X2__V4*/ meltfptr[3]));;
					MELT_LOCATION
					  ("warmelt-first.melt:3922:/ cond");
					/*cond */ if ( /*_#STRING___L20*/ meltfnum[10])	/*then */
					  {
					    /*^cond.then */
					    /*^block */
					    /*anyblock */
					    {

					      MELT_LOCATION
						("warmelt-first.melt:3923:/ checksignal");
					      MELT_CHECK_SIGNAL ();
					      ;
					      /*^quasiblock */


					      /*_.RETVAL___V1*/ meltfptr[0] =
						/*_.VLESS__V6*/ meltfptr[5];;

					      {
						MELT_LOCATION
						  ("warmelt-first.melt:3923:/ locexp");
						/*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
						if (meltxresdescr_
						    && meltxresdescr_[0]
						    && meltxrestab_)
						  melt_warn_for_no_expected_secondary_results
						    ();
						/* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
						;
					      }
					      ;
					      /*^finalreturn */
					      ;
					      /*finalret */ goto labend_rout;
					      /*_.IFELSE___V61*/ meltfptr[26]
						=
						/*_.RETURN___V62*/
						meltfptr[31];;
					      /*epilog */

					      MELT_LOCATION
						("warmelt-first.melt:3922:/ clear");
			 /*clear *//*_.RETURN___V62*/
						meltfptr[31] = 0;
					    }
					    ;
					  }
					else
					  {	/*^cond.else */

					    /*^block */
					    /*anyblock */
					    {

					      MELT_LOCATION
						("warmelt-first.melt:3924:/ checksignal");
					      MELT_CHECK_SIGNAL ();
					      ;
	       /*_#STRING___L21*/ meltfnum[9] =
						melt_string_less ((melt_ptr_t)
								  ( /*_.X2__V4*/ meltfptr[3]), (melt_ptr_t) ( /*_.X1__V2*/ meltfptr[1]));;
					      MELT_LOCATION
						("warmelt-first.melt:3924:/ cond");
					      /*cond */ if ( /*_#STRING___L21*/ meltfnum[9])	/*then */
						{
						  /*^cond.then */
						  /*^block */
						  /*anyblock */
						  {

						    MELT_LOCATION
						      ("warmelt-first.melt:3925:/ checksignal");
						    MELT_CHECK_SIGNAL ();
						    ;
						    /*^quasiblock */


						    /*_.RETVAL___V1*/
						      meltfptr[0] =
						      /*_.VGREAT__V8*/
						      meltfptr[7];;

						    {
						      MELT_LOCATION
							("warmelt-first.melt:3925:/ locexp");
						      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
						      if (meltxresdescr_
							  && meltxresdescr_[0]
							  && meltxrestab_)
							melt_warn_for_no_expected_secondary_results
							  ();
						      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
						      ;
						    }
						    ;
						    /*^finalreturn */
						    ;
						    /*finalret */ goto
						      labend_rout;
						    /*_.IFELSE___V63*/
						      meltfptr[28] =
						      /*_.RETURN___V64*/
						      meltfptr[29];;
						    /*epilog */

						    MELT_LOCATION
						      ("warmelt-first.melt:3924:/ clear");
			   /*clear *//*_.RETURN___V64*/
						      meltfptr[29] = 0;
						  }
						  ;
						}
					      else
						{	/*^cond.else */

						  /*^block */
						  /*anyblock */
						  {

						    MELT_LOCATION
						      ("warmelt-first.melt:3926:/ checksignal");
						    MELT_CHECK_SIGNAL ();
						    ;
		 /*_#I__L22*/ meltfnum[8] =
						      (( /*_#RK1__L2*/
							meltfnum[1]) <
						       ( /*_#RK2__L3*/
							meltfnum[2]));;
						    MELT_LOCATION
						      ("warmelt-first.melt:3926:/ cond");
						    /*cond */ if ( /*_#I__L22*/ meltfnum[8])	/*then */
						      {
							/*^cond.then */
							/*^block */
							/*anyblock */
							{

							  MELT_LOCATION
							    ("warmelt-first.melt:3927:/ checksignal");
							  MELT_CHECK_SIGNAL
							    ();
							  ;
							  /*^quasiblock */


							  /*_.RETVAL___V1*/
							    meltfptr[0] =
							    /*_.VLESS__V6*/
							    meltfptr[5];;

							  {
							    MELT_LOCATION
							      ("warmelt-first.melt:3927:/ locexp");
							    /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
							    if (meltxresdescr_
								&&
								meltxresdescr_
								[0]
								&&
								meltxrestab_)
							      melt_warn_for_no_expected_secondary_results
								();
							    /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
							    ;
							  }
							  ;
							  /*^finalreturn */
							  ;
							  /*finalret */ goto
							    labend_rout;
							  /*_.IFELSE___V65*/
							    meltfptr[37] =
							    /*_.RETURN___V66*/
							    meltfptr[23];;
							  /*epilog */

							  MELT_LOCATION
							    ("warmelt-first.melt:3926:/ clear");
			     /*clear *//*_.RETURN___V66*/
							    meltfptr[23] = 0;
							}
							;
						      }
						    else
						      {	/*^cond.else */

							/*^block */
							/*anyblock */
							{

							  MELT_LOCATION
							    ("warmelt-first.melt:3928:/ checksignal");
							  MELT_CHECK_SIGNAL
							    ();
							  ;
		   /*_#I__L23*/
							    meltfnum[22] =
							    (( /*_#RK1__L2*/
							      meltfnum[1]) >
							     ( /*_#RK2__L3*/
							      meltfnum[2]));;
							  MELT_LOCATION
							    ("warmelt-first.melt:3928:/ cond");
							  /*cond */ if ( /*_#I__L23*/ meltfnum[22])	/*then */
							    {
							      /*^cond.then */
							      /*^block */
							      /*anyblock */
							      {

								MELT_LOCATION
								  ("warmelt-first.melt:3929:/ checksignal");
								MELT_CHECK_SIGNAL
								  ();
								;
								/*^quasiblock */


								/*_.RETVAL___V1*/
								  meltfptr[0]
								  =
								  /*_.VGREAT__V8*/
								  meltfptr
								  [7];;

								{
								  MELT_LOCATION
								    ("warmelt-first.melt:3929:/ locexp");
								  /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
								  if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
								    melt_warn_for_no_expected_secondary_results
								      ();
								  /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
								  ;
								}
								;
								/*^finalreturn */
								;
								/*finalret */
								  goto
								  labend_rout;
								/*_.IFELSE___V67*/
								  meltfptr[14]
								  =
								  /*_.RETURN___V68*/
								  meltfptr
								  [30];;
								/*epilog */

								MELT_LOCATION
								  ("warmelt-first.melt:3928:/ clear");
			       /*clear *//*_.RETURN___V68*/
								  meltfptr
								  [30] = 0;
							      }
							      ;
							    }
							  else
							    {	/*^cond.else */

							      /*^block */
							      /*anyblock */
							      {

								MELT_LOCATION
								  ("warmelt-first.melt:3931:/ checksignal");
								MELT_CHECK_SIGNAL
								  ();
								;
								/*^quasiblock */


								/*_.RETVAL___V1*/
								  meltfptr[0]
								  =
								  /*_.VEQ__V7*/
								  meltfptr
								  [6];;

								{
								  MELT_LOCATION
								    ("warmelt-first.melt:3931:/ locexp");
								  /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
								  if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
								    melt_warn_for_no_expected_secondary_results
								      ();
								  /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
								  ;
								}
								;
								/*^finalreturn */
								;
								/*finalret */
								  goto
								  labend_rout;
								MELT_LOCATION
								  ("warmelt-first.melt:3930:/ quasiblock");


								/*_.PROGN___V70*/
								  meltfptr[15]
								  =
								  /*_.RETURN___V69*/
								  meltfptr
								  [16];;
								/*^compute */
								/*_.IFELSE___V67*/
								  meltfptr[14]
								  =
								  /*_.PROGN___V70*/
								  meltfptr
								  [15];;
								/*epilog */

								MELT_LOCATION
								  ("warmelt-first.melt:3928:/ clear");
			       /*clear *//*_.RETURN___V69*/
								  meltfptr
								  [16] = 0;
								/*^clear */
			       /*clear *//*_.PROGN___V70*/
								  meltfptr
								  [15] = 0;
							      }
							      ;
							    }
							  ;
							  /*_.IFELSE___V65*/
							    meltfptr[37] =
							    /*_.IFELSE___V67*/
							    meltfptr[14];;
							  /*epilog */

							  MELT_LOCATION
							    ("warmelt-first.melt:3926:/ clear");
			     /*clear *//*_#I__L23*/
							    meltfnum[22] = 0;
							  /*^clear */
			     /*clear *//*_.IFELSE___V67*/
							    meltfptr[14] = 0;
							}
							;
						      }
						    ;
						    /*_.IFELSE___V63*/
						      meltfptr[28] =
						      /*_.IFELSE___V65*/
						      meltfptr[37];;
						    /*epilog */

						    MELT_LOCATION
						      ("warmelt-first.melt:3924:/ clear");
			   /*clear *//*_#I__L22*/
						      meltfnum[8] = 0;
						    /*^clear */
			   /*clear *//*_.IFELSE___V65*/
						      meltfptr[37] = 0;
						  }
						  ;
						}
					      ;
					      /*_.IFELSE___V61*/ meltfptr[26]
						=
						/*_.IFELSE___V63*/
						meltfptr[28];;
					      /*epilog */

					      MELT_LOCATION
						("warmelt-first.melt:3922:/ clear");
			 /*clear *//*_#STRING___L21*/
						meltfnum[9] = 0;
					      /*^clear */
			 /*clear *//*_.IFELSE___V63*/
						meltfptr[28] = 0;
					    }
					    ;
					  }
					;
					/*_.IFELSE___V60*/ meltfptr[24] =
					  /*_.IFELSE___V61*/ meltfptr[26];;
					/*epilog */

					MELT_LOCATION
					  ("warmelt-first.melt:3921:/ clear");
		       /*clear *//*_#STRING___L20*/
					  meltfnum[10] = 0;
					/*^clear */
		       /*clear *//*_.IFELSE___V61*/
					  meltfptr[26] = 0;
				      }
				      ;
				    }
				  else
				    {	/*^cond.else */

				      /*^block */
				      /*anyblock */
				      {

					MELT_LOCATION
					  ("warmelt-first.melt:3932:/ checksignal");
					MELT_CHECK_SIGNAL ();
					;
	     /*_#I__L24*/ meltfnum[22] =
					  (( /*_#RK1__L2*/ meltfnum[1]) <
					   ( /*_#RK2__L3*/ meltfnum[2]));;
					MELT_LOCATION
					  ("warmelt-first.melt:3932:/ cond");
					/*cond */ if ( /*_#I__L24*/ meltfnum[22])	/*then */
					  {
					    /*^cond.then */
					    /*^block */
					    /*anyblock */
					    {

					      MELT_LOCATION
						("warmelt-first.melt:3933:/ checksignal");
					      MELT_CHECK_SIGNAL ();
					      ;
					      /*^quasiblock */


					      /*_.RETVAL___V1*/ meltfptr[0] =
						/*_.VLESS__V6*/ meltfptr[5];;

					      {
						MELT_LOCATION
						  ("warmelt-first.melt:3933:/ locexp");
						/*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
						if (meltxresdescr_
						    && meltxresdescr_[0]
						    && meltxrestab_)
						  melt_warn_for_no_expected_secondary_results
						    ();
						/* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
						;
					      }
					      ;
					      /*^finalreturn */
					      ;
					      /*finalret */ goto labend_rout;
					      /*_.IFELSE___V71*/ meltfptr[31]
						=
						/*_.RETURN___V72*/
						meltfptr[29];;
					      /*epilog */

					      MELT_LOCATION
						("warmelt-first.melt:3932:/ clear");
			 /*clear *//*_.RETURN___V72*/
						meltfptr[29] = 0;
					    }
					    ;
					  }
					else
					  {	/*^cond.else */

					    /*^block */
					    /*anyblock */
					    {

					      MELT_LOCATION
						("warmelt-first.melt:3934:/ checksignal");
					      MELT_CHECK_SIGNAL ();
					      ;
	       /*_#I__L25*/ meltfnum[8] =
						(( /*_#RK1__L2*/ meltfnum[1])
						 >
						 ( /*_#RK2__L3*/
						  meltfnum[2]));;
					      MELT_LOCATION
						("warmelt-first.melt:3934:/ cond");
					      /*cond */ if ( /*_#I__L25*/ meltfnum[8])	/*then */
						{
						  /*^cond.then */
						  /*^block */
						  /*anyblock */
						  {

						    MELT_LOCATION
						      ("warmelt-first.melt:3935:/ checksignal");
						    MELT_CHECK_SIGNAL ();
						    ;
						    /*^quasiblock */


						    /*_.RETVAL___V1*/
						      meltfptr[0] =
						      /*_.VGREAT__V8*/
						      meltfptr[7];;

						    {
						      MELT_LOCATION
							("warmelt-first.melt:3935:/ locexp");
						      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
						      if (meltxresdescr_
							  && meltxresdescr_[0]
							  && meltxrestab_)
							melt_warn_for_no_expected_secondary_results
							  ();
						      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
						      ;
						    }
						    ;
						    /*^finalreturn */
						    ;
						    /*finalret */ goto
						      labend_rout;
						    /*_.IFELSE___V73*/
						      meltfptr[23] =
						      /*_.RETURN___V74*/
						      meltfptr[30];;
						    /*epilog */

						    MELT_LOCATION
						      ("warmelt-first.melt:3934:/ clear");
			   /*clear *//*_.RETURN___V74*/
						      meltfptr[30] = 0;
						  }
						  ;
						}
					      else
						{	/*^cond.else */

						  /*^block */
						  /*anyblock */
						  {

						    MELT_LOCATION
						      ("warmelt-first.melt:3937:/ checksignal");
						    MELT_CHECK_SIGNAL ();
						    ;
						    /*^quasiblock */


						    /*_.RETVAL___V1*/
						      meltfptr[0] =
						      /*_.VEQ__V7*/
						      meltfptr[6];;

						    {
						      MELT_LOCATION
							("warmelt-first.melt:3937:/ locexp");
						      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
						      if (meltxresdescr_
							  && meltxresdescr_[0]
							  && meltxrestab_)
							melt_warn_for_no_expected_secondary_results
							  ();
						      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
						      ;
						    }
						    ;
						    /*^finalreturn */
						    ;
						    /*finalret */ goto
						      labend_rout;
						    MELT_LOCATION
						      ("warmelt-first.melt:3936:/ quasiblock");


						    /*_.PROGN___V76*/
						      meltfptr[15] =
						      /*_.RETURN___V75*/
						      meltfptr[16];;
						    /*^compute */
						    /*_.IFELSE___V73*/
						      meltfptr[23] =
						      /*_.PROGN___V76*/
						      meltfptr[15];;
						    /*epilog */

						    MELT_LOCATION
						      ("warmelt-first.melt:3934:/ clear");
			   /*clear *//*_.RETURN___V75*/
						      meltfptr[16] = 0;
						    /*^clear */
			   /*clear *//*_.PROGN___V76*/
						      meltfptr[15] = 0;
						  }
						  ;
						}
					      ;
					      /*_.IFELSE___V71*/ meltfptr[31]
						=
						/*_.IFELSE___V73*/
						meltfptr[23];;
					      /*epilog */

					      MELT_LOCATION
						("warmelt-first.melt:3932:/ clear");
			 /*clear *//*_#I__L25*/
						meltfnum[8] = 0;
					      /*^clear */
			 /*clear *//*_.IFELSE___V73*/
						meltfptr[23] = 0;
					    }
					    ;
					  }
					;
					/*_.IFELSE___V60*/ meltfptr[24] =
					  /*_.IFELSE___V71*/ meltfptr[31];;
					/*epilog */

					MELT_LOCATION
					  ("warmelt-first.melt:3921:/ clear");
		       /*clear *//*_#I__L24*/ meltfnum[22] =
					  0;
					/*^clear */
		       /*clear *//*_.IFELSE___V71*/
					  meltfptr[31] = 0;
				      }
				      ;
				    }
				  ;
				  /*_.IFELSE___V54*/ meltfptr[33] =
				    /*_.IFELSE___V60*/ meltfptr[24];;
				  /*epilog */

				  MELT_LOCATION
				    ("warmelt-first.melt:3917:/ clear");
		     /*clear *//*_#IS_STRING__L19*/ meltfnum[7] =
				    0;
				  /*^clear */
		     /*clear *//*_.IFELSE___V60*/ meltfptr[24] =
				    0;
				}
				;
			      }
			    ;
			    /*_.IFELSE___V40*/ meltfptr[35] =
			      /*_.IFELSE___V54*/ meltfptr[33];;
			    /*epilog */

			    MELT_LOCATION ("warmelt-first.melt:3901:/ clear");
		   /*clear *//*_#IS_A__L18*/ meltfnum[6] = 0;
			    /*^clear */
		   /*clear *//*_.IFELSE___V54*/ meltfptr[33] = 0;
			  }
			  ;
			}
		      ;
		      /*_.IFELSE___V20*/ meltfptr[17] =
			/*_.IFELSE___V40*/ meltfptr[35];;
		      /*epilog */

		      MELT_LOCATION ("warmelt-first.melt:3877:/ clear");
		 /*clear *//*_#IS_A__L13*/ meltfnum[11] = 0;
		      /*^clear */
		 /*clear *//*_.IFELSE___V40*/ meltfptr[35] = 0;
		    }
		    ;
		  }
		;
		/*_.IFELSE___V14*/ meltfptr[13] =
		  /*_.IFELSE___V20*/ meltfptr[17];;
		/*epilog */

		MELT_LOCATION ("warmelt-first.melt:3871:/ clear");
	       /*clear *//*_#IS_A__L6*/ meltfnum[4] = 0;
		/*^clear */
	       /*clear *//*_.IFELSE___V20*/ meltfptr[17] = 0;
	      }
	      ;
	    }
	  ;
	  /*_.LET___V11*/ meltfptr[9] = /*_.IFELSE___V14*/ meltfptr[13];;

	  MELT_LOCATION ("warmelt-first.melt:3866:/ clear");
	     /*clear *//*_.X1DIS__V12*/ meltfptr[11] = 0;
	  /*^clear */
	     /*clear *//*_.X2DIS__V13*/ meltfptr[12] = 0;
	  /*^clear */
	     /*clear *//*_#RK1__L2*/ meltfnum[1] = 0;
	  /*^clear */
	     /*clear *//*_#RK2__L3*/ meltfnum[2] = 0;
	  /*^clear */
	     /*clear *//*_#__L4*/ meltfnum[3] = 0;
	  /*^clear */
	     /*clear *//*_.IFELSE___V14*/ meltfptr[13] = 0;
	  /*_.IFELSE___V9*/ meltfptr[8] = /*_.LET___V11*/ meltfptr[9];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-first.melt:3864:/ clear");
	     /*clear *//*_.LET___V11*/ meltfptr[9] = 0;
	}
	;
      }
    ;
    MELT_LOCATION ("warmelt-first.melt:3863:/ quasiblock");


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.IFELSE___V9*/ meltfptr[8];;

    {
      MELT_LOCATION ("warmelt-first.melt:3863:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_#__L1*/ meltfnum[0] = 0;
    /*^clear */
	   /*clear *//*_.IFELSE___V9*/ meltfptr[8] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("COMPARE_OBJ_RANKED", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_55_warmelt_first_COMPARE_OBJ_RANKED_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_55_warmelt_first_COMPARE_OBJ_RANKED */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_56_warmelt_first_MAPOBJECT_SORTED_ATTRIBUTE_TUPLE (meltclosure_ptr_t
							    meltclosp_,
							    melt_ptr_t
							    meltfirstargp_,
							    const
							    melt_argdescr_cell_t
							    meltxargdescr_[],
							    union meltparam_un
							    * meltxargtab_,
							    const
							    melt_argdescr_cell_t
							    meltxresdescr_[],
							    union meltparam_un
							    * meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_56_warmelt_first_MAPOBJECT_SORTED_ATTRIBUTE_TUPLE_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_56_warmelt_first_MAPOBJECT_SORTED_ATTRIBUTE_TUPLE_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 18
    melt_ptr_t mcfr_varptr[18];
#define MELTFRAM_NBVARNUM 1
    long mcfr_varnum[1];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_56_warmelt_first_MAPOBJECT_SORTED_ATTRIBUTE_TUPLE is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct
	 frame_meltrout_56_warmelt_first_MAPOBJECT_SORTED_ATTRIBUTE_TUPLE_st
	 *) meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 18; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_56_warmelt_first_MAPOBJECT_SORTED_ATTRIBUTE_TUPLE nbval 18*/
  meltfram__.mcfr_nbvar = 18 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("MAPOBJECT_SORTED_ATTRIBUTE_TUPLE", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-first.melt:3945:/ getarg");
 /*_.MAPO__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {

    MELT_LOCATION ("warmelt-first.melt:3948:/ quasiblock");


 /*_#MAPCOUNT__L1*/ meltfnum[0] =
      (melt_count_mapobjects
       ((meltmapobjects_ptr_t) ( /*_.MAPO__V2*/ meltfptr[1])));;
    /*^compute */
 /*_.COUNTBOX__V4*/ meltfptr[3] =
      (meltgc_new_int
       ((meltobject_ptr_t) (( /*!DISCR_INTEGER */ meltfrout->tabval[0])),
	(0)));;
    /*^compute */
 /*_.BOXEDONE__V5*/ meltfptr[4] =
      (meltgc_new_int
       ((meltobject_ptr_t) (( /*!DISCR_INTEGER */ meltfrout->tabval[0])),
	(1)));;
    /*^compute */
 /*_.BOXEDZERO__V6*/ meltfptr[5] =
      (meltgc_new_int
       ((meltobject_ptr_t) (( /*!DISCR_INTEGER */ meltfrout->tabval[0])),
	(0)));;
    /*^compute */
 /*_.BOXEDMINUSONE__V7*/ meltfptr[6] =
      (meltgc_new_int
       ((meltobject_ptr_t) (( /*!DISCR_INTEGER */ meltfrout->tabval[0])),
	(-1)));;
    /*^compute */
 /*_.TUPL__V8*/ meltfptr[7] =
      (meltgc_new_multiple
       ((meltobject_ptr_t) (( /*!DISCR_MULTIPLE */ meltfrout->tabval[1])),
	( /*_#MAPCOUNT__L1*/ meltfnum[0])));;
    MELT_LOCATION ("warmelt-first.melt:3959:/ quasiblock");


    /*^newclosure */
		 /*newclosure *//*_.LAMBDA___V10*/ meltfptr[9] =
      (melt_ptr_t)
      meltgc_new_closure ((meltobject_ptr_t)
			  (((melt_ptr_t) (MELT_PREDEF (DISCR_CLOSURE)))),
			  (meltroutine_ptr_t) (( /*!konst_4 */ meltfrout->
						tabval[4])), (2));
    ;
    /*^putclosedv */
    /*putclosv */
    melt_assertmsg ("putclosv checkclo",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.LAMBDA___V10*/ meltfptr[9])) ==
		    MELTOBMAG_CLOSURE);
    melt_assertmsg ("putclosv checkoff", 0 >= 0
		    && 0 <
		    melt_closure_size ((melt_ptr_t)
				       ( /*_.LAMBDA___V10*/ meltfptr[9])));
    ((meltclosure_ptr_t) /*_.LAMBDA___V10*/ meltfptr[9])->tabval[0] =
      (melt_ptr_t) ( /*_.COUNTBOX__V4*/ meltfptr[3]);
    ;
    /*^putclosedv */
    /*putclosv */
    melt_assertmsg ("putclosv checkclo",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.LAMBDA___V10*/ meltfptr[9])) ==
		    MELTOBMAG_CLOSURE);
    melt_assertmsg ("putclosv checkoff", 1 >= 0
		    && 1 <
		    melt_closure_size ((melt_ptr_t)
				       ( /*_.LAMBDA___V10*/ meltfptr[9])));
    ((meltclosure_ptr_t) /*_.LAMBDA___V10*/ meltfptr[9])->tabval[1] =
      (melt_ptr_t) ( /*_.TUPL__V8*/ meltfptr[7]);
    ;
    /*_.LAMBDA___V9*/ meltfptr[8] = /*_.LAMBDA___V10*/ meltfptr[9];;
    MELT_LOCATION ("warmelt-first.melt:3957:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.LAMBDA___V9*/ meltfptr[8];
      /*_.MAPOBJECT_EVERY__V11*/ meltfptr[10] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!MAPOBJECT_EVERY */ meltfrout->tabval[2])),
		    (melt_ptr_t) ( /*_.MAPO__V2*/ meltfptr[1]),
		    (MELTBPARSTR_PTR ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-first.melt:3968:/ quasiblock");


    MELT_LOCATION ("warmelt-first.melt:3971:/ quasiblock");


    /*^newclosure */
		 /*newclosure *//*_.LAMBDA___V14*/ meltfptr[13] =
      (melt_ptr_t)
      meltgc_new_closure ((meltobject_ptr_t)
			  (((melt_ptr_t) (MELT_PREDEF (DISCR_CLOSURE)))),
			  (meltroutine_ptr_t) (( /*!konst_6 */ meltfrout->
						tabval[6])), (3));
    ;
    /*^putclosedv */
    /*putclosv */
    melt_assertmsg ("putclosv checkclo",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.LAMBDA___V14*/ meltfptr[13])) ==
		    MELTOBMAG_CLOSURE);
    melt_assertmsg ("putclosv checkoff", 0 >= 0
		    && 0 <
		    melt_closure_size ((melt_ptr_t)
				       ( /*_.LAMBDA___V14*/ meltfptr[13])));
    ((meltclosure_ptr_t) /*_.LAMBDA___V14*/ meltfptr[13])->tabval[0] =
      (melt_ptr_t) ( /*_.BOXEDMINUSONE__V7*/ meltfptr[6]);
    ;
    /*^putclosedv */
    /*putclosv */
    melt_assertmsg ("putclosv checkclo",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.LAMBDA___V14*/ meltfptr[13])) ==
		    MELTOBMAG_CLOSURE);
    melt_assertmsg ("putclosv checkoff", 1 >= 0
		    && 1 <
		    melt_closure_size ((melt_ptr_t)
				       ( /*_.LAMBDA___V14*/ meltfptr[13])));
    ((meltclosure_ptr_t) /*_.LAMBDA___V14*/ meltfptr[13])->tabval[1] =
      (melt_ptr_t) ( /*_.BOXEDZERO__V6*/ meltfptr[5]);
    ;
    /*^putclosedv */
    /*putclosv */
    melt_assertmsg ("putclosv checkclo",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.LAMBDA___V14*/ meltfptr[13])) ==
		    MELTOBMAG_CLOSURE);
    melt_assertmsg ("putclosv checkoff", 2 >= 0
		    && 2 <
		    melt_closure_size ((melt_ptr_t)
				       ( /*_.LAMBDA___V14*/ meltfptr[13])));
    ((meltclosure_ptr_t) /*_.LAMBDA___V14*/ meltfptr[13])->tabval[2] =
      (melt_ptr_t) ( /*_.BOXEDONE__V5*/ meltfptr[4]);
    ;
    /*_.LAMBDA___V13*/ meltfptr[12] = /*_.LAMBDA___V14*/ meltfptr[13];;
    /*^compute */
 /*_.SORTUPL__V15*/ meltfptr[14] =
      meltgc_sort_multiple ((melt_ptr_t) ( /*_.TUPL__V8*/ meltfptr[7]),
			    (melt_ptr_t) ( /*_.LAMBDA___V13*/ meltfptr[12]),
			    (melt_ptr_t) (( /*!DISCR_MULTIPLE */ meltfrout->
					   tabval[1])));;
    MELT_LOCATION ("warmelt-first.melt:3985:/ quasiblock");


    /*^newclosure */
		 /*newclosure *//*_.LAMBDA___V17*/ meltfptr[16] =
      (melt_ptr_t)
      meltgc_new_closure ((meltobject_ptr_t)
			  (((melt_ptr_t) (MELT_PREDEF (DISCR_CLOSURE)))),
			  (meltroutine_ptr_t) (( /*!konst_8 */ meltfrout->
						tabval[8])), (0));
    ;
    /*_.LAMBDA___V16*/ meltfptr[15] = /*_.LAMBDA___V17*/ meltfptr[16];;
    MELT_LOCATION ("warmelt-first.melt:3985:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & /*_.LAMBDA___V16*/ meltfptr[15];
      /*_.MULTIPLE_MAP__V18*/ meltfptr[17] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!MULTIPLE_MAP */ meltfrout->tabval[7])),
		    (melt_ptr_t) ( /*_.SORTUPL__V15*/ meltfptr[14]),
		    (MELTBPARSTR_PTR ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;
    /*_.LET___V12*/ meltfptr[11] = /*_.MULTIPLE_MAP__V18*/ meltfptr[17];;

    MELT_LOCATION ("warmelt-first.melt:3968:/ clear");
	   /*clear *//*_.LAMBDA___V13*/ meltfptr[12] = 0;
    /*^clear */
	   /*clear *//*_.SORTUPL__V15*/ meltfptr[14] = 0;
    /*^clear */
	   /*clear *//*_.LAMBDA___V16*/ meltfptr[15] = 0;
    /*^clear */
	   /*clear *//*_.MULTIPLE_MAP__V18*/ meltfptr[17] = 0;
    /*_.LET___V3*/ meltfptr[2] = /*_.LET___V12*/ meltfptr[11];;

    MELT_LOCATION ("warmelt-first.melt:3948:/ clear");
	   /*clear *//*_#MAPCOUNT__L1*/ meltfnum[0] = 0;
    /*^clear */
	   /*clear *//*_.COUNTBOX__V4*/ meltfptr[3] = 0;
    /*^clear */
	   /*clear *//*_.BOXEDONE__V5*/ meltfptr[4] = 0;
    /*^clear */
	   /*clear *//*_.BOXEDZERO__V6*/ meltfptr[5] = 0;
    /*^clear */
	   /*clear *//*_.BOXEDMINUSONE__V7*/ meltfptr[6] = 0;
    /*^clear */
	   /*clear *//*_.TUPL__V8*/ meltfptr[7] = 0;
    /*^clear */
	   /*clear *//*_.LAMBDA___V9*/ meltfptr[8] = 0;
    /*^clear */
	   /*clear *//*_.MAPOBJECT_EVERY__V11*/ meltfptr[10] = 0;
    /*^clear */
	   /*clear *//*_.LET___V12*/ meltfptr[11] = 0;
    MELT_LOCATION ("warmelt-first.melt:3945:/ quasiblock");


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.LET___V3*/ meltfptr[2];;

    {
      MELT_LOCATION ("warmelt-first.melt:3945:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.LET___V3*/ meltfptr[2] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("MAPOBJECT_SORTED_ATTRIBUTE_TUPLE", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_56_warmelt_first_MAPOBJECT_SORTED_ATTRIBUTE_TUPLE_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_56_warmelt_first_MAPOBJECT_SORTED_ATTRIBUTE_TUPLE */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_57_warmelt_first_LAMBDA___12__ (meltclosure_ptr_t meltclosp_,
					 melt_ptr_t meltfirstargp_,
					 const melt_argdescr_cell_t
					 meltxargdescr_[],
					 union meltparam_un * meltxargtab_,
					 const melt_argdescr_cell_t
					 meltxresdescr_[],
					 union meltparam_un * meltxrestab_)
{
  long current_blocklevel_signals_meltrout_57_warmelt_first_LAMBDA___12___melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_57_warmelt_first_LAMBDA___12___st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 6
    melt_ptr_t mcfr_varptr[6];
#define MELTFRAM_NBVARNUM 2
    long mcfr_varnum[2];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_57_warmelt_first_LAMBDA___12__ is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_57_warmelt_first_LAMBDA___12___st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 6; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_57_warmelt_first_LAMBDA___12__ nbval 6*/
  meltfram__.mcfr_nbvar = 6 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("LAMBDA_", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-first.melt:3959:/ getarg");
 /*_.AT__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.VA__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.VA__V3*/ meltfptr[2])) != NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {

    MELT_LOCATION ("warmelt-first.melt:3960:/ quasiblock");


 /*_#CURCOUNT__L1*/ meltfnum[0] =
      (melt_get_int ((melt_ptr_t) (( /*~COUNTBOX */ meltfclos->tabval[0]))));;
    /*^compute */
 /*_.MAKE_INTEGERBOX__V4*/ meltfptr[3] =
      (meltgc_new_int
       ((meltobject_ptr_t) (( /*!DISCR_INTEGER */ meltfrout->tabval[0])),
	( /*_#CURCOUNT__L1*/ meltfnum[0])));;
    MELT_LOCATION ("warmelt-first.melt:3961:/ blockmultialloc");
    /*multiallocblock */
    {
      struct meltletrec_1_st
      {
	struct MELT_MULTIPLE_STRUCT (3) rtup_0__TUPLREC__x1;
	long meltletrec_1_endgap;
      } *meltletrec_1_ptr = 0;
      meltletrec_1_ptr =
	(struct meltletrec_1_st *)
	meltgc_allocate (sizeof (struct meltletrec_1_st), 0);
      /*^blockmultialloc.initfill */
      /*inimult rtup_0__TUPLREC__x1 */
 /*_.TUPLREC___V6*/ meltfptr[5] =
	(melt_ptr_t) & meltletrec_1_ptr->rtup_0__TUPLREC__x1;
      meltletrec_1_ptr->rtup_0__TUPLREC__x1.discr =
	(meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (DISCR_MULTIPLE))));
      meltletrec_1_ptr->rtup_0__TUPLREC__x1.nbval = 3;


      /*^putuple */
      /*putupl#1 */
      melt_assertmsg ("putupl [:3961] #1 checktup",
		      melt_magic_discr ((melt_ptr_t)
					( /*_.TUPLREC___V6*/ meltfptr[5])) ==
		      MELTOBMAG_MULTIPLE);
      melt_assertmsg ("putupl [:3961] #1 checkoff",
		      (0 >= 0
		       && 0 <
		       melt_multiple_length ((melt_ptr_t)
					     ( /*_.TUPLREC___V6*/
					      meltfptr[5]))));
      ((meltmultiple_ptr_t) ( /*_.TUPLREC___V6*/ meltfptr[5]))->tabval[0] =
	(melt_ptr_t) ( /*_.AT__V2*/ meltfptr[1]);
      ;
      /*^putuple */
      /*putupl#2 */
      melt_assertmsg ("putupl [:3961] #2 checktup",
		      melt_magic_discr ((melt_ptr_t)
					( /*_.TUPLREC___V6*/ meltfptr[5])) ==
		      MELTOBMAG_MULTIPLE);
      melt_assertmsg ("putupl [:3961] #2 checkoff",
		      (1 >= 0
		       && 1 <
		       melt_multiple_length ((melt_ptr_t)
					     ( /*_.TUPLREC___V6*/
					      meltfptr[5]))));
      ((meltmultiple_ptr_t) ( /*_.TUPLREC___V6*/ meltfptr[5]))->tabval[1] =
	(melt_ptr_t) ( /*_.VA__V3*/ meltfptr[2]);
      ;
      /*^putuple */
      /*putupl#3 */
      melt_assertmsg ("putupl [:3961] #3 checktup",
		      melt_magic_discr ((melt_ptr_t)
					( /*_.TUPLREC___V6*/ meltfptr[5])) ==
		      MELTOBMAG_MULTIPLE);
      melt_assertmsg ("putupl [:3961] #3 checkoff",
		      (2 >= 0
		       && 2 <
		       melt_multiple_length ((melt_ptr_t)
					     ( /*_.TUPLREC___V6*/
					      meltfptr[5]))));
      ((meltmultiple_ptr_t) ( /*_.TUPLREC___V6*/ meltfptr[5]))->tabval[2] =
	(melt_ptr_t) ( /*_.MAKE_INTEGERBOX__V4*/ meltfptr[3]);
      ;
      /*^touch */
      meltgc_touch ( /*_.TUPLREC___V6*/ meltfptr[5]);
      ;
      /*_.ENT__V5*/ meltfptr[4] = /*_.TUPLREC___V6*/ meltfptr[5];;
      /*epilog */

      MELT_LOCATION ("warmelt-first.melt:3961:/ clear");
	    /*clear *//*_.TUPLREC___V6*/ meltfptr[5] = 0;
      /*^clear */
	    /*clear *//*_.TUPLREC___V6*/ meltfptr[5] = 0;
    }				/*end multiallocblock */
    ;

    {
      MELT_LOCATION ("warmelt-first.melt:3964:/ locexp");
      meltgc_multiple_put_nth ((melt_ptr_t)
			       (( /*~TUPL */ meltfclos->tabval[1])),
			       ( /*_#CURCOUNT__L1*/ meltfnum[0]),
			       (melt_ptr_t) ( /*_.ENT__V5*/ meltfptr[4]));
    }
    ;
 /*_#I__L2*/ meltfnum[1] =
      (( /*_#CURCOUNT__L1*/ meltfnum[0]) + (1));;

    {
      MELT_LOCATION ("warmelt-first.melt:3965:/ locexp");
      melt_put_int ((melt_ptr_t) (( /*~COUNTBOX */ meltfclos->tabval[0])),
		    ( /*_#I__L2*/ meltfnum[1]));
    }
    ;

    MELT_LOCATION ("warmelt-first.melt:3960:/ clear");
	   /*clear *//*_#CURCOUNT__L1*/ meltfnum[0] = 0;
    /*^clear */
	   /*clear *//*_.MAKE_INTEGERBOX__V4*/ meltfptr[3] = 0;
    /*^clear */
	   /*clear *//*_.ENT__V5*/ meltfptr[4] = 0;
    /*^clear */
	   /*clear *//*_#I__L2*/ meltfnum[1] = 0;
    /*epilog */
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("LAMBDA_", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_57_warmelt_first_LAMBDA___12___melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*noretval */ NULL);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_57_warmelt_first_LAMBDA___12__ */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_58_warmelt_first_LAMBDA___13__ (meltclosure_ptr_t meltclosp_,
					 melt_ptr_t meltfirstargp_,
					 const melt_argdescr_cell_t
					 meltxargdescr_[],
					 union meltparam_un * meltxargtab_,
					 const melt_argdescr_cell_t
					 meltxresdescr_[],
					 union meltparam_un * meltxrestab_)
{
  long current_blocklevel_signals_meltrout_58_warmelt_first_LAMBDA___13___melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_58_warmelt_first_LAMBDA___13___st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 12
    melt_ptr_t mcfr_varptr[12];
/*no varnum*/
#define MELTFRAM_NBVARNUM /*none*/0
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_58_warmelt_first_LAMBDA___13__ is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_58_warmelt_first_LAMBDA___13___st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 12; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_58_warmelt_first_LAMBDA___13__ nbval 12*/
  meltfram__.mcfr_nbvar = 12 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("LAMBDA_", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-first.melt:3971:/ getarg");
 /*_.E1__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.E2__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.E2__V3*/ meltfptr[2])) != NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {

    MELT_LOCATION ("warmelt-first.melt:3972:/ quasiblock");


 /*_.E1AT__V5*/ meltfptr[4] =
      (melt_multiple_nth ((melt_ptr_t) ( /*_.E1__V2*/ meltfptr[1]), (0)));;
    /*^compute */
 /*_.E1VA__V6*/ meltfptr[5] =
      (melt_multiple_nth ((melt_ptr_t) ( /*_.E1__V2*/ meltfptr[1]), (1)));;
    /*^compute */
 /*_.E1RK__V7*/ meltfptr[6] =
      (melt_multiple_nth ((melt_ptr_t) ( /*_.E1__V2*/ meltfptr[1]), (2)));;
    /*^compute */
 /*_.E2AT__V8*/ meltfptr[7] =
      (melt_multiple_nth ((melt_ptr_t) ( /*_.E2__V3*/ meltfptr[2]), (0)));;
    /*^compute */
 /*_.E2VA__V9*/ meltfptr[8] =
      (melt_multiple_nth ((melt_ptr_t) ( /*_.E2__V3*/ meltfptr[2]), (1)));;
    /*^compute */
 /*_.E2RK__V10*/ meltfptr[9] =
      (melt_multiple_nth ((melt_ptr_t) ( /*_.E2__V3*/ meltfptr[2]), (2)));;
    MELT_LOCATION ("warmelt-first.melt:3980:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[6];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.E1RK__V7*/ meltfptr[6];
      /*^apply.arg */
      argtab[1].meltbp_aptr = (melt_ptr_t *) & /*_.E2AT__V8*/ meltfptr[7];
      /*^apply.arg */
      argtab[2].meltbp_aptr = (melt_ptr_t *) & /*_.E2RK__V10*/ meltfptr[9];
      /*^apply.arg */
      argtab[3].meltbp_aptr =
	(melt_ptr_t *) & ( /*~BOXEDMINUSONE */ meltfclos->tabval[0]);
      /*^apply.arg */
      argtab[4].meltbp_aptr =
	(melt_ptr_t *) & ( /*~BOXEDZERO */ meltfclos->tabval[1]);
      /*^apply.arg */
      argtab[5].meltbp_aptr =
	(melt_ptr_t *) & ( /*~BOXEDONE */ meltfclos->tabval[2]);
      /*_.COMPARE_OBJ_RANKED__V11*/ meltfptr[10] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!COMPARE_OBJ_RANKED */ meltfrout->tabval[0])),
		    (melt_ptr_t) ( /*_.E1AT__V5*/ meltfptr[4]),
		    (MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_PTR
		     MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_PTR ""),
		    argtab, "", (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-first.melt:3979:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] =
      /*_.COMPARE_OBJ_RANKED__V11*/ meltfptr[10];;
    MELT_LOCATION ("warmelt-first.melt:3979:/ putxtraresult");
    if (!meltxrestab_ || !meltxresdescr_)
      goto labend_rout;
    if (meltxresdescr_[0] != MELTBPAR_PTR)
      goto labend_rout;
    if (meltxrestab_[0].meltbp_aptr)
      *(meltxrestab_[0].meltbp_aptr) = (melt_ptr_t) (( /*nil */ NULL));
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*_.LET___V4*/ meltfptr[3] = /*_.RETURN___V12*/ meltfptr[11];;

    MELT_LOCATION ("warmelt-first.melt:3972:/ clear");
	   /*clear *//*_.E1AT__V5*/ meltfptr[4] = 0;
    /*^clear */
	   /*clear *//*_.E1VA__V6*/ meltfptr[5] = 0;
    /*^clear */
	   /*clear *//*_.E1RK__V7*/ meltfptr[6] = 0;
    /*^clear */
	   /*clear *//*_.E2AT__V8*/ meltfptr[7] = 0;
    /*^clear */
	   /*clear *//*_.E2VA__V9*/ meltfptr[8] = 0;
    /*^clear */
	   /*clear *//*_.E2RK__V10*/ meltfptr[9] = 0;
    /*^clear */
	   /*clear *//*_.COMPARE_OBJ_RANKED__V11*/ meltfptr[10] = 0;
    /*^clear */
	   /*clear *//*_.RETURN___V12*/ meltfptr[11] = 0;
    MELT_LOCATION ("warmelt-first.melt:3971:/ quasiblock");


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.LET___V4*/ meltfptr[3];;

    {
      MELT_LOCATION ("warmelt-first.melt:3971:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.LET___V4*/ meltfptr[3] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("LAMBDA_", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_58_warmelt_first_LAMBDA___13___melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_58_warmelt_first_LAMBDA___13__ */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_59_warmelt_first_LAMBDA___14__ (meltclosure_ptr_t meltclosp_,
					 melt_ptr_t meltfirstargp_,
					 const melt_argdescr_cell_t
					 meltxargdescr_[],
					 union meltparam_un * meltxargtab_,
					 const melt_argdescr_cell_t
					 meltxresdescr_[],
					 union meltparam_un * meltxrestab_)
{
  long current_blocklevel_signals_meltrout_59_warmelt_first_LAMBDA___14___melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_59_warmelt_first_LAMBDA___14___st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 3
    melt_ptr_t mcfr_varptr[3];
/*no varnum*/
#define MELTFRAM_NBVARNUM /*none*/0
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_59_warmelt_first_LAMBDA___14__ is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_59_warmelt_first_LAMBDA___14___st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 3; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_59_warmelt_first_LAMBDA___14__ nbval 3*/
  meltfram__.mcfr_nbvar = 3 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("LAMBDA_", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-first.melt:3985:/ getarg");
 /*_.EL__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {

 /*_.MULTIPLE_NTH__V3*/ meltfptr[2] =
      (melt_multiple_nth ((melt_ptr_t) ( /*_.EL__V2*/ meltfptr[1]), (0)));;
    MELT_LOCATION ("warmelt-first.melt:3985:/ quasiblock");


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.MULTIPLE_NTH__V3*/ meltfptr[2];;

    {
      MELT_LOCATION ("warmelt-first.melt:3985:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.MULTIPLE_NTH__V3*/ meltfptr[2] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("LAMBDA_", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_59_warmelt_first_LAMBDA___14___melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_59_warmelt_first_LAMBDA___14__ */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_60_warmelt_first_FRESH_ENV (meltclosure_ptr_t meltclosp_,
				     melt_ptr_t meltfirstargp_,
				     const melt_argdescr_cell_t
				     meltxargdescr_[],
				     union meltparam_un * meltxargtab_,
				     const melt_argdescr_cell_t
				     meltxresdescr_[],
				     union meltparam_un * meltxrestab_)
{
  long current_blocklevel_signals_meltrout_60_warmelt_first_FRESH_ENV_melt =
    melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_60_warmelt_first_FRESH_ENV_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 11
    melt_ptr_t mcfr_varptr[11];
#define MELTFRAM_NBVARNUM 3
    long mcfr_varnum[3];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_60_warmelt_first_FRESH_ENV is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_60_warmelt_first_FRESH_ENV_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 11; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_60_warmelt_first_FRESH_ENV nbval 11*/
  meltfram__.mcfr_nbvar = 11 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("FRESH_ENV", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-first.melt:3999:/ getarg");
 /*_.PARENV__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.DESCR__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.DESCR__V3*/ meltfptr[2])) !=
	      NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {

    MELT_LOCATION ("warmelt-first.melt:4003:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#NULL__L1*/ meltfnum[0] =
      (( /*_.PARENV__V2*/ meltfptr[1]) == NULL);;
    MELT_LOCATION ("warmelt-first.melt:4003:/ cond");
    /*cond */ if ( /*_#NULL__L1*/ meltfnum[0])	/*then */
      {
	/*^cond.then */
	/*_#OR___L2*/ meltfnum[1] = /*_#NULL__L1*/ meltfnum[0];;
      }
    else
      {
	MELT_LOCATION ("warmelt-first.melt:4003:/ cond.else");

	/*^block */
	/*anyblock */
	{

   /*_#IS_A__L3*/ meltfnum[2] =
	    melt_is_instance_of ((melt_ptr_t) ( /*_.PARENV__V2*/ meltfptr[1]),
				 (melt_ptr_t) (( /*!CLASS_ENVIRONMENT */
						meltfrout->tabval[0])));;
	  /*^compute */
	  /*_#OR___L2*/ meltfnum[1] = /*_#IS_A__L3*/ meltfnum[2];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-first.melt:4003:/ clear");
	     /*clear *//*_#IS_A__L3*/ meltfnum[2] = 0;
	}
	;
      }
    ;
    /*^cond */
    /*cond */ if ( /*_#OR___L2*/ meltfnum[1])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-first.melt:4004:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^cond */
	  /*cond */ if ( /*_.DESCR__V3*/ meltfptr[2])	/*then */
	    {
	      /*^cond.then */
	      /*^block */
	      /*anyblock */
	      {

		MELT_LOCATION ("warmelt-first.melt:4005:/ checksignal");
		MELT_CHECK_SIGNAL ();
		;
     /*_.MAKE_MAPOBJECT__V6*/ meltfptr[5] =
		  (meltgc_new_mapobjects
		   ((meltobject_ptr_t)
		    (( /*!DISCR_MAP_OBJECTS */ meltfrout->tabval[2])),
		    (26)));;
		MELT_LOCATION ("warmelt-first.melt:4005:/ quasiblock");


		/*^rawallocobj */
		/*rawallocobj */
		{
		  melt_ptr_t newobj = 0;
		  melt_raw_object_create (newobj,
					  (melt_ptr_t) (( /*!CLASS_DESCRIBED_ENVIRONMENT */ meltfrout->tabval[1])), (4), "CLASS_DESCRIBED_ENVIRONMENT");
      /*_.INST__V8*/ meltfptr[7] =
		    newobj;
		};
		;
		/*^putslot */
		/*putslot */
		melt_assertmsg ("putslot checkobj @ENV_BIND",
				melt_magic_discr ((melt_ptr_t)
						  ( /*_.INST__V8*/
						   meltfptr[7])) ==
				MELTOBMAG_OBJECT);
		melt_putfield_object (( /*_.INST__V8*/ meltfptr[7]), (0),
				      ( /*_.MAKE_MAPOBJECT__V6*/ meltfptr[5]),
				      "ENV_BIND");
		;
		/*^putslot */
		/*putslot */
		melt_assertmsg ("putslot checkobj @ENV_PREV",
				melt_magic_discr ((melt_ptr_t)
						  ( /*_.INST__V8*/
						   meltfptr[7])) ==
				MELTOBMAG_OBJECT);
		melt_putfield_object (( /*_.INST__V8*/ meltfptr[7]), (1),
				      ( /*_.PARENV__V2*/ meltfptr[1]),
				      "ENV_PREV");
		;
		/*^putslot */
		/*putslot */
		melt_assertmsg ("putslot checkobj @DENV_DESCR",
				melt_magic_discr ((melt_ptr_t)
						  ( /*_.INST__V8*/
						   meltfptr[7])) ==
				MELTOBMAG_OBJECT);
		melt_putfield_object (( /*_.INST__V8*/ meltfptr[7]), (3),
				      ( /*_.DESCR__V3*/ meltfptr[2]),
				      "DENV_DESCR");
		;
		/*^touchobj */

		melt_dbgtrace_written_object ( /*_.INST__V8*/ meltfptr[7],
					      "newly made instance");
		;
		/*_.INST___V7*/ meltfptr[6] = /*_.INST__V8*/ meltfptr[7];;
		/*^compute */
		/*_.IFELSE___V5*/ meltfptr[4] = /*_.INST___V7*/ meltfptr[6];;
		/*epilog */

		MELT_LOCATION ("warmelt-first.melt:4004:/ clear");
	       /*clear *//*_.MAKE_MAPOBJECT__V6*/ meltfptr[5] = 0;
		/*^clear */
	       /*clear *//*_.INST___V7*/ meltfptr[6] = 0;
	      }
	      ;
	    }
	  else
	    {			/*^cond.else */

	      /*^block */
	      /*anyblock */
	      {

		MELT_LOCATION ("warmelt-first.melt:4009:/ checksignal");
		MELT_CHECK_SIGNAL ();
		;
     /*_.MAKE_MAPOBJECT__V9*/ meltfptr[5] =
		  (meltgc_new_mapobjects
		   ((meltobject_ptr_t)
		    (( /*!DISCR_MAP_OBJECTS */ meltfrout->tabval[2])), (6)));;
		MELT_LOCATION ("warmelt-first.melt:4009:/ quasiblock");


		/*^rawallocobj */
		/*rawallocobj */
		{
		  melt_ptr_t newobj = 0;
		  melt_raw_object_create (newobj,
					  (melt_ptr_t) (( /*!CLASS_ENVIRONMENT */ meltfrout->tabval[0])), (3), "CLASS_ENVIRONMENT");
      /*_.INST__V11*/ meltfptr[10] =
		    newobj;
		};
		;
		/*^putslot */
		/*putslot */
		melt_assertmsg ("putslot checkobj @ENV_BIND",
				melt_magic_discr ((melt_ptr_t)
						  ( /*_.INST__V11*/
						   meltfptr[10])) ==
				MELTOBMAG_OBJECT);
		melt_putfield_object (( /*_.INST__V11*/ meltfptr[10]), (0),
				      ( /*_.MAKE_MAPOBJECT__V9*/ meltfptr[5]),
				      "ENV_BIND");
		;
		/*^putslot */
		/*putslot */
		melt_assertmsg ("putslot checkobj @ENV_PREV",
				melt_magic_discr ((melt_ptr_t)
						  ( /*_.INST__V11*/
						   meltfptr[10])) ==
				MELTOBMAG_OBJECT);
		melt_putfield_object (( /*_.INST__V11*/ meltfptr[10]), (1),
				      ( /*_.PARENV__V2*/ meltfptr[1]),
				      "ENV_PREV");
		;
		/*^touchobj */

		melt_dbgtrace_written_object ( /*_.INST__V11*/ meltfptr[10],
					      "newly made instance");
		;
		/*_.INST___V10*/ meltfptr[6] = /*_.INST__V11*/ meltfptr[10];;
		/*^compute */
		/*_.IFELSE___V5*/ meltfptr[4] = /*_.INST___V10*/ meltfptr[6];;
		/*epilog */

		MELT_LOCATION ("warmelt-first.melt:4004:/ clear");
	       /*clear *//*_.MAKE_MAPOBJECT__V9*/ meltfptr[5] = 0;
		/*^clear */
	       /*clear *//*_.INST___V10*/ meltfptr[6] = 0;
	      }
	      ;
	    }
	  ;
	  /*_.IF___V4*/ meltfptr[3] = /*_.IFELSE___V5*/ meltfptr[4];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-first.melt:4003:/ clear");
	     /*clear *//*_.IFELSE___V5*/ meltfptr[4] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

  /*_.IF___V4*/ meltfptr[3] = NULL;;
      }
    ;
    MELT_LOCATION ("warmelt-first.melt:3999:/ quasiblock");


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.IF___V4*/ meltfptr[3];;

    {
      MELT_LOCATION ("warmelt-first.melt:3999:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_#NULL__L1*/ meltfnum[0] = 0;
    /*^clear */
	   /*clear *//*_#OR___L2*/ meltfnum[1] = 0;
    /*^clear */
	   /*clear *//*_.IF___V4*/ meltfptr[3] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("FRESH_ENV", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_60_warmelt_first_FRESH_ENV_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_60_warmelt_first_FRESH_ENV */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_61_warmelt_first_FIND_ENV (meltclosure_ptr_t meltclosp_,
				    melt_ptr_t meltfirstargp_,
				    const melt_argdescr_cell_t
				    meltxargdescr_[],
				    union meltparam_un * meltxargtab_,
				    const melt_argdescr_cell_t
				    meltxresdescr_[],
				    union meltparam_un * meltxrestab_)
{
  long current_blocklevel_signals_meltrout_61_warmelt_first_FIND_ENV_melt =
    melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_61_warmelt_first_FIND_ENV_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 19
    melt_ptr_t mcfr_varptr[19];
#define MELTFRAM_NBVARNUM 6
    long mcfr_varnum[6];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_61_warmelt_first_FIND_ENV is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_61_warmelt_first_FIND_ENV_st *) meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 19; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_61_warmelt_first_FIND_ENV nbval 19*/
  meltfram__.mcfr_nbvar = 19 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("FIND_ENV", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-first.melt:4021:/ getarg");
 /*_.ENV__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.BINDER__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.BINDER__V3*/ meltfptr[2])) !=
	      NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-first.melt:4023:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L1*/ meltfnum[0] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.ENV__V2*/ meltfptr[1]),
			     (melt_ptr_t) (( /*!CLASS_ENVIRONMENT */
					    meltfrout->tabval[0])));;
      MELT_LOCATION ("warmelt-first.melt:4023:/ cond");
      /*cond */ if ( /*_#IS_A__L1*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V5*/ meltfptr[4] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-first.melt:4023:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check arg env"),
				  ("warmelt-first.melt")
				  ? ("warmelt-first.melt") : __FILE__,
				  (4023) ? (4023) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V5*/ meltfptr[4] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V4*/ meltfptr[3] = /*_.IFELSE___V5*/ meltfptr[4];;
      /*epilog */

      MELT_LOCATION ("warmelt-first.melt:4023:/ clear");
	     /*clear *//*_#IS_A__L1*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V5*/ meltfptr[4] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V4*/ meltfptr[3] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-first.melt:4024:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_OBJECT__L2*/ meltfnum[0] =
	(melt_magic_discr ((melt_ptr_t) ( /*_.BINDER__V3*/ meltfptr[2])) ==
	 MELTOBMAG_OBJECT);;
      MELT_LOCATION ("warmelt-first.melt:4024:/ cond");
      /*cond */ if ( /*_#IS_OBJECT__L2*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V7*/ meltfptr[6] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-first.melt:4024:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check arg binder"),
				  ("warmelt-first.melt")
				  ? ("warmelt-first.melt") : __FILE__,
				  (4024) ? (4024) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V7*/ meltfptr[6] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V6*/ meltfptr[4] = /*_.IFELSE___V7*/ meltfptr[6];;
      /*epilog */

      MELT_LOCATION ("warmelt-first.melt:4024:/ clear");
	     /*clear *//*_#IS_OBJECT__L2*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V7*/ meltfptr[6] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V6*/ meltfptr[4] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-first.melt:4025:/ loop");
    /*loop */
    {
    labloop_FINDLOOP_1:;	/*^loopbody */

      /*^block */
      /*anyblock */
      {

	/*^checksignal */
	MELT_CHECK_SIGNAL ();
	;
	MELT_LOCATION ("warmelt-first.melt:4027:/ checksignal");
	MELT_CHECK_SIGNAL ();
	;
   /*_#NULL__L3*/ meltfnum[0] =
	  (( /*_.ENV__V2*/ meltfptr[1]) == NULL);;
	MELT_LOCATION ("warmelt-first.melt:4027:/ cond");
	/*cond */ if ( /*_#NULL__L3*/ meltfnum[0])	/*then */
	  {
	    /*^cond.then */
	    /*^block */
	    /*anyblock */
	    {

	      MELT_LOCATION ("warmelt-first.melt:4028:/ quasiblock");


	      /*^compute */
	      /*_.FINDLOOP__V9*/ meltfptr[8] = ( /*nil */ NULL);;

	      /*^exit */
	      /*exit */
	      {
		goto labexit_FINDLOOP_1;
	      }
	      ;
	      /*epilog */
	    }
	    ;
	  }			/*noelse */
	;

#if MELT_HAVE_DEBUG
	MELT_LOCATION ("warmelt-first.melt:4029:/ cppif.then");
	/*^block */
	/*anyblock */
	{

	  /*^checksignal */
	  MELT_CHECK_SIGNAL ();
	  ;
     /*_#IS_OBJECT__L4*/ meltfnum[3] =
	    (melt_magic_discr ((melt_ptr_t) ( /*_.ENV__V2*/ meltfptr[1])) ==
	     MELTOBMAG_OBJECT);;
	  MELT_LOCATION ("warmelt-first.melt:4029:/ cond");
	  /*cond */ if ( /*_#IS_OBJECT__L4*/ meltfnum[3])	/*then */
	    {
	      /*^cond.then */
	      /*_.IFELSE___V11*/ meltfptr[10] = ( /*nil */ NULL);;
	    }
	  else
	    {
	      MELT_LOCATION ("warmelt-first.melt:4029:/ cond.else");

	      /*^block */
	      /*anyblock */
	      {




		{
		  /*^locexp */
		  melt_assert_failed (("check env obj"),
				      ("warmelt-first.melt")
				      ? ("warmelt-first.melt") : __FILE__,
				      (4029) ? (4029) : __LINE__,
				      __FUNCTION__);
		  ;
		}
		;
		 /*clear *//*_.IFELSE___V11*/ meltfptr[10] = 0;
		/*epilog */
	      }
	      ;
	    }
	  ;
	  /*^compute */
	  /*_.IFCPP___V10*/ meltfptr[9] = /*_.IFELSE___V11*/ meltfptr[10];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-first.melt:4029:/ clear");
	       /*clear *//*_#IS_OBJECT__L4*/ meltfnum[3] = 0;
	  /*^clear */
	       /*clear *//*_.IFELSE___V11*/ meltfptr[10] = 0;
	}

#else /*MELT_HAVE_DEBUG */
	/*^cppif.else */
	/*_.IFCPP___V10*/ meltfptr[9] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
	;

#if MELT_HAVE_DEBUG
	MELT_LOCATION ("warmelt-first.melt:4030:/ cppif.then");
	/*^block */
	/*anyblock */
	{

	  /*^checksignal */
	  MELT_CHECK_SIGNAL ();
	  ;
     /*_#IS_A__L5*/ meltfnum[3] =
	    melt_is_instance_of ((melt_ptr_t) ( /*_.ENV__V2*/ meltfptr[1]),
				 (melt_ptr_t) (( /*!CLASS_ENVIRONMENT */
						meltfrout->tabval[0])));;
	  MELT_LOCATION ("warmelt-first.melt:4030:/ cond");
	  /*cond */ if ( /*_#IS_A__L5*/ meltfnum[3])	/*then */
	    {
	      /*^cond.then */
	      /*_.IFELSE___V13*/ meltfptr[12] = ( /*nil */ NULL);;
	    }
	  else
	    {
	      MELT_LOCATION ("warmelt-first.melt:4030:/ cond.else");

	      /*^block */
	      /*anyblock */
	      {




		{
		  /*^locexp */
		  melt_assert_failed (("check good env"),
				      ("warmelt-first.melt")
				      ? ("warmelt-first.melt") : __FILE__,
				      (4030) ? (4030) : __LINE__,
				      __FUNCTION__);
		  ;
		}
		;
		 /*clear *//*_.IFELSE___V13*/ meltfptr[12] = 0;
		/*epilog */
	      }
	      ;
	    }
	  ;
	  /*^compute */
	  /*_.IFCPP___V12*/ meltfptr[10] = /*_.IFELSE___V13*/ meltfptr[12];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-first.melt:4030:/ clear");
	       /*clear *//*_#IS_A__L5*/ meltfnum[3] = 0;
	  /*^clear */
	       /*clear *//*_.IFELSE___V13*/ meltfptr[12] = 0;
	}

#else /*MELT_HAVE_DEBUG */
	/*^cppif.else */
	/*_.IFCPP___V12*/ meltfptr[10] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
	;
	MELT_LOCATION ("warmelt-first.melt:4031:/ quasiblock");


	MELT_LOCATION ("warmelt-first.melt:4032:/ getslot");
	{
	  melt_ptr_t slot = NULL, obj = NULL;
	  obj = (melt_ptr_t) ( /*_.ENV__V2*/ meltfptr[1]) /*=obj*/ ;
	  melt_object_get_field (slot, obj, 0, "ENV_BIND");
    /*_.BINDMAP__V14*/ meltfptr[12] = slot;
	};
	;

#if MELT_HAVE_DEBUG
	MELT_LOCATION ("warmelt-first.melt:4033:/ cppif.then");
	/*^block */
	/*anyblock */
	{

	  /*^checksignal */
	  MELT_CHECK_SIGNAL ();
	  ;
     /*_#IS_MAPOBJECT__L6*/ meltfnum[3] =
	    /*is_mapobject: */
	    (melt_magic_discr
	     ((melt_ptr_t) ( /*_.BINDMAP__V14*/ meltfptr[12])) ==
	     MELTOBMAG_MAPOBJECTS);;
	  MELT_LOCATION ("warmelt-first.melt:4033:/ cond");
	  /*cond */ if ( /*_#IS_MAPOBJECT__L6*/ meltfnum[3])	/*then */
	    {
	      /*^cond.then */
	      /*_.IFELSE___V16*/ meltfptr[15] = ( /*nil */ NULL);;
	    }
	  else
	    {
	      MELT_LOCATION ("warmelt-first.melt:4033:/ cond.else");

	      /*^block */
	      /*anyblock */
	      {




		{
		  /*^locexp */
		  melt_assert_failed (("check bindmap"),
				      ("warmelt-first.melt")
				      ? ("warmelt-first.melt") : __FILE__,
				      (4033) ? (4033) : __LINE__,
				      __FUNCTION__);
		  ;
		}
		;
		 /*clear *//*_.IFELSE___V16*/ meltfptr[15] = 0;
		/*epilog */
	      }
	      ;
	    }
	  ;
	  /*^compute */
	  /*_.IFCPP___V15*/ meltfptr[14] = /*_.IFELSE___V16*/ meltfptr[15];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-first.melt:4033:/ clear");
	       /*clear *//*_#IS_MAPOBJECT__L6*/ meltfnum[3] = 0;
	  /*^clear */
	       /*clear *//*_.IFELSE___V16*/ meltfptr[15] = 0;
	}

#else /*MELT_HAVE_DEBUG */
	/*^cppif.else */
	/*_.IFCPP___V15*/ meltfptr[14] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
	;
	MELT_LOCATION ("warmelt-first.melt:4034:/ quasiblock");


   /*_.BND__V17*/ meltfptr[15] =
	  /*mapobject_get */
	  melt_get_mapobjects ((meltmapobjects_ptr_t)
			       ( /*_.BINDMAP__V14*/ meltfptr[12]),
			       (meltobject_ptr_t) ( /*_.BINDER__V3*/
						   meltfptr[2]));;
	MELT_LOCATION ("warmelt-first.melt:4036:/ checksignal");
	MELT_CHECK_SIGNAL ();
	;
	/*^cond */
	/*cond */ if ( /*_.BND__V17*/ meltfptr[15])	/*then */
	  {
	    /*^cond.then */
	    /*^block */
	    /*anyblock */
	    {

	      MELT_LOCATION ("warmelt-first.melt:4037:/ quasiblock");


	      /*^compute */
	      /*_.FINDLOOP__V9*/ meltfptr[8] = /*_.BND__V17*/ meltfptr[15];;

	      /*^exit */
	      /*exit */
	      {
		goto labexit_FINDLOOP_1;
	      }
	      ;
	      /*epilog */
	    }
	    ;
	  }			/*noelse */
	;

	MELT_LOCATION ("warmelt-first.melt:4034:/ clear");
	     /*clear *//*_.BND__V17*/ meltfptr[15] = 0;

	MELT_LOCATION ("warmelt-first.melt:4031:/ clear");
	     /*clear *//*_.BINDMAP__V14*/ meltfptr[12] = 0;
	/*^clear */
	     /*clear *//*_.IFCPP___V15*/ meltfptr[14] = 0;
	MELT_LOCATION ("warmelt-first.melt:4039:/ getslot");
	{
	  melt_ptr_t slot = NULL, obj = NULL;
	  obj = (melt_ptr_t) ( /*_.ENV__V2*/ meltfptr[1]) /*=obj*/ ;
	  melt_object_get_field (slot, obj, 1, "ENV_PREV");
    /*_.ENV_PREV__V18*/ meltfptr[15] = slot;
	};
	;
	/*^compute */
	/*_.ENV__V2*/ meltfptr[1] = /*_.SETQ___V19*/ meltfptr[12] =
	  /*_.ENV_PREV__V18*/ meltfptr[15];;
	MELT_LOCATION ("warmelt-first.melt:4025:/ checksignal");
	MELT_CHECK_SIGNAL ();
	;
	/*epilog */

	/*^clear */
	     /*clear *//*_#NULL__L3*/ meltfnum[0] = 0;
	/*^clear */
	     /*clear *//*_.IFCPP___V10*/ meltfptr[9] = 0;
	/*^clear */
	     /*clear *//*_.IFCPP___V12*/ meltfptr[10] = 0;
	/*^clear */
	     /*clear *//*_.ENV_PREV__V18*/ meltfptr[15] = 0;
	/*^clear */
	     /*clear *//*_.SETQ___V19*/ meltfptr[12] = 0;
      }
      ;
      ;
      goto labloop_FINDLOOP_1;
    labexit_FINDLOOP_1:;	/*^loopepilog */
      /*loopepilog */
      /*_.FOREVER___V8*/ meltfptr[6] = /*_.FINDLOOP__V9*/ meltfptr[8];;
    }
    ;
    MELT_LOCATION ("warmelt-first.melt:4021:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.FOREVER___V8*/ meltfptr[6];;

    {
      MELT_LOCATION ("warmelt-first.melt:4021:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V4*/ meltfptr[3] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V6*/ meltfptr[4] = 0;
    /*^clear */
	   /*clear *//*_.FOREVER___V8*/ meltfptr[6] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("FIND_ENV", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_61_warmelt_first_FIND_ENV_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_61_warmelt_first_FIND_ENV */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_62_warmelt_first_FIND_ENV_DEBUG (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un * meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un * meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_62_warmelt_first_FIND_ENV_DEBUG_melt =
    melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_62_warmelt_first_FIND_ENV_DEBUG_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 19
    melt_ptr_t mcfr_varptr[19];
#define MELTFRAM_NBVARNUM 6
    long mcfr_varnum[6];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_62_warmelt_first_FIND_ENV_DEBUG is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_62_warmelt_first_FIND_ENV_DEBUG_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 19; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_62_warmelt_first_FIND_ENV_DEBUG nbval 19*/
  meltfram__.mcfr_nbvar = 19 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("FIND_ENV_DEBUG", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-first.melt:4044:/ getarg");
 /*_.ENV__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.BINDER__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.BINDER__V3*/ meltfptr[2])) !=
	      NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-first.melt:4045:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L1*/ meltfnum[0] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.ENV__V2*/ meltfptr[1]),
			     (melt_ptr_t) (( /*!CLASS_ENVIRONMENT */
					    meltfrout->tabval[0])));;
      MELT_LOCATION ("warmelt-first.melt:4045:/ cond");
      /*cond */ if ( /*_#IS_A__L1*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V5*/ meltfptr[4] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-first.melt:4045:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check arg env"),
				  ("warmelt-first.melt")
				  ? ("warmelt-first.melt") : __FILE__,
				  (4045) ? (4045) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V5*/ meltfptr[4] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V4*/ meltfptr[3] = /*_.IFELSE___V5*/ meltfptr[4];;
      /*epilog */

      MELT_LOCATION ("warmelt-first.melt:4045:/ clear");
	     /*clear *//*_#IS_A__L1*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V5*/ meltfptr[4] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V4*/ meltfptr[3] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-first.melt:4046:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_OBJECT__L2*/ meltfnum[0] =
	(melt_magic_discr ((melt_ptr_t) ( /*_.BINDER__V3*/ meltfptr[2])) ==
	 MELTOBMAG_OBJECT);;
      MELT_LOCATION ("warmelt-first.melt:4046:/ cond");
      /*cond */ if ( /*_#IS_OBJECT__L2*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V7*/ meltfptr[6] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-first.melt:4046:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check arg binder"),
				  ("warmelt-first.melt")
				  ? ("warmelt-first.melt") : __FILE__,
				  (4046) ? (4046) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V7*/ meltfptr[6] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V6*/ meltfptr[4] = /*_.IFELSE___V7*/ meltfptr[6];;
      /*epilog */

      MELT_LOCATION ("warmelt-first.melt:4046:/ clear");
	     /*clear *//*_#IS_OBJECT__L2*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V7*/ meltfptr[6] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V6*/ meltfptr[4] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-first.melt:4047:/ loop");
    /*loop */
    {
    labloop_FINDLOOP_2:;	/*^loopbody */

      /*^block */
      /*anyblock */
      {

	/*^checksignal */
	MELT_CHECK_SIGNAL ();
	;
	MELT_LOCATION ("warmelt-first.melt:4049:/ checksignal");
	MELT_CHECK_SIGNAL ();
	;
   /*_#NULL__L3*/ meltfnum[0] =
	  (( /*_.ENV__V2*/ meltfptr[1]) == NULL);;
	MELT_LOCATION ("warmelt-first.melt:4049:/ cond");
	/*cond */ if ( /*_#NULL__L3*/ meltfnum[0])	/*then */
	  {
	    /*^cond.then */
	    /*^block */
	    /*anyblock */
	    {

	      MELT_LOCATION ("warmelt-first.melt:4050:/ quasiblock");


	      /*^compute */
	      /*_.FINDLOOP__V9*/ meltfptr[8] = ( /*nil */ NULL);;

	      /*^exit */
	      /*exit */
	      {
		goto labexit_FINDLOOP_2;
	      }
	      ;
	      /*epilog */
	    }
	    ;
	  }			/*noelse */
	;

#if MELT_HAVE_DEBUG
	MELT_LOCATION ("warmelt-first.melt:4051:/ cppif.then");
	/*^block */
	/*anyblock */
	{

	  /*^checksignal */
	  MELT_CHECK_SIGNAL ();
	  ;
     /*_#IS_OBJECT__L4*/ meltfnum[3] =
	    (melt_magic_discr ((melt_ptr_t) ( /*_.ENV__V2*/ meltfptr[1])) ==
	     MELTOBMAG_OBJECT);;
	  MELT_LOCATION ("warmelt-first.melt:4051:/ cond");
	  /*cond */ if ( /*_#IS_OBJECT__L4*/ meltfnum[3])	/*then */
	    {
	      /*^cond.then */
	      /*_.IFELSE___V11*/ meltfptr[10] = ( /*nil */ NULL);;
	    }
	  else
	    {
	      MELT_LOCATION ("warmelt-first.melt:4051:/ cond.else");

	      /*^block */
	      /*anyblock */
	      {




		{
		  /*^locexp */
		  melt_assert_failed (("check env obj"),
				      ("warmelt-first.melt")
				      ? ("warmelt-first.melt") : __FILE__,
				      (4051) ? (4051) : __LINE__,
				      __FUNCTION__);
		  ;
		}
		;
		 /*clear *//*_.IFELSE___V11*/ meltfptr[10] = 0;
		/*epilog */
	      }
	      ;
	    }
	  ;
	  /*^compute */
	  /*_.IFCPP___V10*/ meltfptr[9] = /*_.IFELSE___V11*/ meltfptr[10];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-first.melt:4051:/ clear");
	       /*clear *//*_#IS_OBJECT__L4*/ meltfnum[3] = 0;
	  /*^clear */
	       /*clear *//*_.IFELSE___V11*/ meltfptr[10] = 0;
	}

#else /*MELT_HAVE_DEBUG */
	/*^cppif.else */
	/*_.IFCPP___V10*/ meltfptr[9] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
	;

#if MELT_HAVE_DEBUG
	MELT_LOCATION ("warmelt-first.melt:4052:/ cppif.then");
	/*^block */
	/*anyblock */
	{

	  /*^checksignal */
	  MELT_CHECK_SIGNAL ();
	  ;
     /*_#IS_A__L5*/ meltfnum[3] =
	    melt_is_instance_of ((melt_ptr_t) ( /*_.ENV__V2*/ meltfptr[1]),
				 (melt_ptr_t) (( /*!CLASS_ENVIRONMENT */
						meltfrout->tabval[0])));;
	  MELT_LOCATION ("warmelt-first.melt:4052:/ cond");
	  /*cond */ if ( /*_#IS_A__L5*/ meltfnum[3])	/*then */
	    {
	      /*^cond.then */
	      /*_.IFELSE___V13*/ meltfptr[12] = ( /*nil */ NULL);;
	    }
	  else
	    {
	      MELT_LOCATION ("warmelt-first.melt:4052:/ cond.else");

	      /*^block */
	      /*anyblock */
	      {




		{
		  /*^locexp */
		  melt_assert_failed (("check good env"),
				      ("warmelt-first.melt")
				      ? ("warmelt-first.melt") : __FILE__,
				      (4052) ? (4052) : __LINE__,
				      __FUNCTION__);
		  ;
		}
		;
		 /*clear *//*_.IFELSE___V13*/ meltfptr[12] = 0;
		/*epilog */
	      }
	      ;
	    }
	  ;
	  /*^compute */
	  /*_.IFCPP___V12*/ meltfptr[10] = /*_.IFELSE___V13*/ meltfptr[12];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-first.melt:4052:/ clear");
	       /*clear *//*_#IS_A__L5*/ meltfnum[3] = 0;
	  /*^clear */
	       /*clear *//*_.IFELSE___V13*/ meltfptr[12] = 0;
	}

#else /*MELT_HAVE_DEBUG */
	/*^cppif.else */
	/*_.IFCPP___V12*/ meltfptr[10] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
	;
	MELT_LOCATION ("warmelt-first.melt:4053:/ quasiblock");


	MELT_LOCATION ("warmelt-first.melt:4054:/ getslot");
	{
	  melt_ptr_t slot = NULL, obj = NULL;
	  obj = (melt_ptr_t) ( /*_.ENV__V2*/ meltfptr[1]) /*=obj*/ ;
	  melt_object_get_field (slot, obj, 0, "ENV_BIND");
    /*_.BINDMAP__V14*/ meltfptr[12] = slot;
	};
	;

#if MELT_HAVE_DEBUG
	MELT_LOCATION ("warmelt-first.melt:4055:/ cppif.then");
	/*^block */
	/*anyblock */
	{

	  /*^checksignal */
	  MELT_CHECK_SIGNAL ();
	  ;
     /*_#IS_MAPOBJECT__L6*/ meltfnum[3] =
	    /*is_mapobject: */
	    (melt_magic_discr
	     ((melt_ptr_t) ( /*_.BINDMAP__V14*/ meltfptr[12])) ==
	     MELTOBMAG_MAPOBJECTS);;
	  MELT_LOCATION ("warmelt-first.melt:4055:/ cond");
	  /*cond */ if ( /*_#IS_MAPOBJECT__L6*/ meltfnum[3])	/*then */
	    {
	      /*^cond.then */
	      /*_.IFELSE___V16*/ meltfptr[15] = ( /*nil */ NULL);;
	    }
	  else
	    {
	      MELT_LOCATION ("warmelt-first.melt:4055:/ cond.else");

	      /*^block */
	      /*anyblock */
	      {




		{
		  /*^locexp */
		  melt_assert_failed (("check bindmap"),
				      ("warmelt-first.melt")
				      ? ("warmelt-first.melt") : __FILE__,
				      (4055) ? (4055) : __LINE__,
				      __FUNCTION__);
		  ;
		}
		;
		 /*clear *//*_.IFELSE___V16*/ meltfptr[15] = 0;
		/*epilog */
	      }
	      ;
	    }
	  ;
	  /*^compute */
	  /*_.IFCPP___V15*/ meltfptr[14] = /*_.IFELSE___V16*/ meltfptr[15];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-first.melt:4055:/ clear");
	       /*clear *//*_#IS_MAPOBJECT__L6*/ meltfnum[3] = 0;
	  /*^clear */
	       /*clear *//*_.IFELSE___V16*/ meltfptr[15] = 0;
	}

#else /*MELT_HAVE_DEBUG */
	/*^cppif.else */
	/*_.IFCPP___V15*/ meltfptr[14] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
	;
	MELT_LOCATION ("warmelt-first.melt:4056:/ quasiblock");


   /*_.BND__V17*/ meltfptr[15] =
	  /*mapobject_get */
	  melt_get_mapobjects ((meltmapobjects_ptr_t)
			       ( /*_.BINDMAP__V14*/ meltfptr[12]),
			       (meltobject_ptr_t) ( /*_.BINDER__V3*/
						   meltfptr[2]));;
	MELT_LOCATION ("warmelt-first.melt:4058:/ checksignal");
	MELT_CHECK_SIGNAL ();
	;
	/*^cond */
	/*cond */ if ( /*_.BND__V17*/ meltfptr[15])	/*then */
	  {
	    /*^cond.then */
	    /*^block */
	    /*anyblock */
	    {

	      MELT_LOCATION ("warmelt-first.melt:4059:/ quasiblock");


	      /*^compute */
	      /*_.FINDLOOP__V9*/ meltfptr[8] = /*_.BND__V17*/ meltfptr[15];;

	      /*^exit */
	      /*exit */
	      {
		goto labexit_FINDLOOP_2;
	      }
	      ;
	      /*epilog */
	    }
	    ;
	  }			/*noelse */
	;

	MELT_LOCATION ("warmelt-first.melt:4056:/ clear");
	     /*clear *//*_.BND__V17*/ meltfptr[15] = 0;

	MELT_LOCATION ("warmelt-first.melt:4053:/ clear");
	     /*clear *//*_.BINDMAP__V14*/ meltfptr[12] = 0;
	/*^clear */
	     /*clear *//*_.IFCPP___V15*/ meltfptr[14] = 0;
	MELT_LOCATION ("warmelt-first.melt:4061:/ getslot");
	{
	  melt_ptr_t slot = NULL, obj = NULL;
	  obj = (melt_ptr_t) ( /*_.ENV__V2*/ meltfptr[1]) /*=obj*/ ;
	  melt_object_get_field (slot, obj, 1, "ENV_PREV");
    /*_.ENV_PREV__V18*/ meltfptr[15] = slot;
	};
	;
	/*^compute */
	/*_.ENV__V2*/ meltfptr[1] = /*_.SETQ___V19*/ meltfptr[12] =
	  /*_.ENV_PREV__V18*/ meltfptr[15];;
	MELT_LOCATION ("warmelt-first.melt:4047:/ checksignal");
	MELT_CHECK_SIGNAL ();
	;
	/*epilog */

	/*^clear */
	     /*clear *//*_#NULL__L3*/ meltfnum[0] = 0;
	/*^clear */
	     /*clear *//*_.IFCPP___V10*/ meltfptr[9] = 0;
	/*^clear */
	     /*clear *//*_.IFCPP___V12*/ meltfptr[10] = 0;
	/*^clear */
	     /*clear *//*_.ENV_PREV__V18*/ meltfptr[15] = 0;
	/*^clear */
	     /*clear *//*_.SETQ___V19*/ meltfptr[12] = 0;
      }
      ;
      ;
      goto labloop_FINDLOOP_2;
    labexit_FINDLOOP_2:;	/*^loopepilog */
      /*loopepilog */
      /*_.FOREVER___V8*/ meltfptr[6] = /*_.FINDLOOP__V9*/ meltfptr[8];;
    }
    ;
    MELT_LOCATION ("warmelt-first.melt:4044:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.FOREVER___V8*/ meltfptr[6];;

    {
      MELT_LOCATION ("warmelt-first.melt:4044:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V4*/ meltfptr[3] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V6*/ meltfptr[4] = 0;
    /*^clear */
	   /*clear *//*_.FOREVER___V8*/ meltfptr[6] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("FIND_ENV_DEBUG", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_62_warmelt_first_FIND_ENV_DEBUG_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_62_warmelt_first_FIND_ENV_DEBUG */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_63_warmelt_first_FIND_ENCLOSING_ENV (meltclosure_ptr_t meltclosp_,
					      melt_ptr_t meltfirstargp_,
					      const melt_argdescr_cell_t
					      meltxargdescr_[],
					      union meltparam_un *
					      meltxargtab_,
					      const melt_argdescr_cell_t
					      meltxresdescr_[],
					      union meltparam_un *
					      meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_63_warmelt_first_FIND_ENCLOSING_ENV_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_63_warmelt_first_FIND_ENCLOSING_ENV_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 22
    melt_ptr_t mcfr_varptr[22];
#define MELTFRAM_NBVARNUM 4
    long mcfr_varnum[4];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_63_warmelt_first_FIND_ENCLOSING_ENV is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_63_warmelt_first_FIND_ENCLOSING_ENV_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 22; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_63_warmelt_first_FIND_ENCLOSING_ENV nbval 22*/
  meltfram__.mcfr_nbvar = 22 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("FIND_ENCLOSING_ENV", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-first.melt:4066:/ getarg");
 /*_.ENV__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.BINDER__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.BINDER__V3*/ meltfptr[2])) !=
	      NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-first.melt:4069:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L1*/ meltfnum[0] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.ENV__V2*/ meltfptr[1]),
			     (melt_ptr_t) (( /*!CLASS_ENVIRONMENT */
					    meltfrout->tabval[0])));;
      MELT_LOCATION ("warmelt-first.melt:4069:/ cond");
      /*cond */ if ( /*_#IS_A__L1*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V5*/ meltfptr[4] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-first.melt:4069:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check env"),
				  ("warmelt-first.melt")
				  ? ("warmelt-first.melt") : __FILE__,
				  (4069) ? (4069) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V5*/ meltfptr[4] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V4*/ meltfptr[3] = /*_.IFELSE___V5*/ meltfptr[4];;
      /*epilog */

      MELT_LOCATION ("warmelt-first.melt:4069:/ clear");
	     /*clear *//*_#IS_A__L1*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V5*/ meltfptr[4] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V4*/ meltfptr[3] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-first.melt:4070:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_OBJECT__L2*/ meltfnum[0] =
	(melt_magic_discr ((melt_ptr_t) ( /*_.BINDER__V3*/ meltfptr[2])) ==
	 MELTOBMAG_OBJECT);;
      MELT_LOCATION ("warmelt-first.melt:4070:/ cond");
      /*cond */ if ( /*_#IS_OBJECT__L2*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V7*/ meltfptr[6] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-first.melt:4070:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check binder"),
				  ("warmelt-first.melt")
				  ? ("warmelt-first.melt") : __FILE__,
				  (4070) ? (4070) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V7*/ meltfptr[6] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V6*/ meltfptr[4] = /*_.IFELSE___V7*/ meltfptr[6];;
      /*epilog */

      MELT_LOCATION ("warmelt-first.melt:4070:/ clear");
	     /*clear *//*_#IS_OBJECT__L2*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V7*/ meltfptr[6] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V6*/ meltfptr[4] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-first.melt:4071:/ quasiblock");


 /*_.PROCLIST__V9*/ meltfptr[8] =
      (meltgc_new_list
       ((meltobject_ptr_t) (( /*!DISCR_LIST */ meltfrout->tabval[1]))));;
    MELT_LOCATION ("warmelt-first.melt:4072:/ loop");
    /*loop */
    {
    labloop_FINDLOOP_3:;	/*^loopbody */

      /*^block */
      /*anyblock */
      {

	/*^checksignal */
	MELT_CHECK_SIGNAL ();
	;
	MELT_LOCATION ("warmelt-first.melt:4073:/ checksignal");
	MELT_CHECK_SIGNAL ();
	;
   /*_#IS_NOT_A__L3*/ meltfnum[0] =
	  !melt_is_instance_of ((melt_ptr_t) ( /*_.ENV__V2*/ meltfptr[1]),
				(melt_ptr_t) (( /*!CLASS_ENVIRONMENT */
					       meltfrout->tabval[0])));;
	MELT_LOCATION ("warmelt-first.melt:4073:/ cond");
	/*cond */ if ( /*_#IS_NOT_A__L3*/ meltfnum[0])	/*then */
	  {
	    /*^cond.then */
	    /*^block */
	    /*anyblock */
	    {

	      /*^quasiblock */


	      /*^compute */
     /*_.FINDLOOP__V11*/ meltfptr[10] = NULL;;

	      /*^exit */
	      /*exit */
	      {
		goto labexit_FINDLOOP_3;
	      }
	      ;
	      /*epilog */
	    }
	    ;
	  }			/*noelse */
	;
	MELT_LOCATION ("warmelt-first.melt:4074:/ quasiblock");


	/*^getslot */
	{
	  melt_ptr_t slot = NULL, obj = NULL;
	  obj = (melt_ptr_t) ( /*_.ENV__V2*/ meltfptr[1]) /*=obj*/ ;
	  melt_object_get_field (slot, obj, 0, "ENV_BIND");
    /*_.BINDMAP__V13*/ meltfptr[12] = slot;
	};
	;
	MELT_LOCATION ("warmelt-first.melt:4075:/ getslot");
	{
	  melt_ptr_t slot = NULL, obj = NULL;
	  obj = (melt_ptr_t) ( /*_.ENV__V2*/ meltfptr[1]) /*=obj*/ ;
	  melt_object_get_field (slot, obj, 2, "ENV_PROC");
    /*_.EPROC__V14*/ meltfptr[13] = slot;
	};
	;

#if MELT_HAVE_DEBUG
	MELT_LOCATION ("warmelt-first.melt:4077:/ cppif.then");
	/*^block */
	/*anyblock */
	{

	  /*^checksignal */
	  MELT_CHECK_SIGNAL ();
	  ;
     /*_#IS_MAPOBJECT__L4*/ meltfnum[3] =
	    /*is_mapobject: */
	    (melt_magic_discr
	     ((melt_ptr_t) ( /*_.BINDMAP__V13*/ meltfptr[12])) ==
	     MELTOBMAG_MAPOBJECTS);;
	  MELT_LOCATION ("warmelt-first.melt:4077:/ cond");
	  /*cond */ if ( /*_#IS_MAPOBJECT__L4*/ meltfnum[3])	/*then */
	    {
	      /*^cond.then */
	      /*_.IFELSE___V16*/ meltfptr[15] = ( /*nil */ NULL);;
	    }
	  else
	    {
	      MELT_LOCATION ("warmelt-first.melt:4077:/ cond.else");

	      /*^block */
	      /*anyblock */
	      {




		{
		  /*^locexp */
		  melt_assert_failed (("check bindmap"),
				      ("warmelt-first.melt")
				      ? ("warmelt-first.melt") : __FILE__,
				      (4077) ? (4077) : __LINE__,
				      __FUNCTION__);
		  ;
		}
		;
		 /*clear *//*_.IFELSE___V16*/ meltfptr[15] = 0;
		/*epilog */
	      }
	      ;
	    }
	  ;
	  /*^compute */
	  /*_.IFCPP___V15*/ meltfptr[14] = /*_.IFELSE___V16*/ meltfptr[15];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-first.melt:4077:/ clear");
	       /*clear *//*_#IS_MAPOBJECT__L4*/ meltfnum[3] = 0;
	  /*^clear */
	       /*clear *//*_.IFELSE___V16*/ meltfptr[15] = 0;
	}

#else /*MELT_HAVE_DEBUG */
	/*^cppif.else */
	/*_.IFCPP___V15*/ meltfptr[14] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
	;
	MELT_LOCATION ("warmelt-first.melt:4078:/ quasiblock");


   /*_.BND__V18*/ meltfptr[17] =
	  /*mapobject_get */
	  melt_get_mapobjects ((meltmapobjects_ptr_t)
			       ( /*_.BINDMAP__V13*/ meltfptr[12]),
			       (meltobject_ptr_t) ( /*_.BINDER__V3*/
						   meltfptr[2]));;
	MELT_LOCATION ("warmelt-first.melt:4079:/ checksignal");
	MELT_CHECK_SIGNAL ();
	;
	/*^cond */
	/*cond */ if ( /*_.BND__V18*/ meltfptr[17])	/*then */
	  {
	    /*^cond.then */
	    /*^block */
	    /*anyblock */
	    {

	      /*^checksignal */
	      MELT_CHECK_SIGNAL ();
	      ;
	      /*^quasiblock */


	      /*_.RETVAL___V1*/ meltfptr[0] = /*_.BND__V18*/ meltfptr[17];;
	      MELT_LOCATION ("warmelt-first.melt:4079:/ putxtraresult");
	      if (!meltxrestab_ || !meltxresdescr_)
		goto labend_rout;
	      if (meltxresdescr_[0] != MELTBPAR_PTR)
		goto labend_rout;
	      if (meltxrestab_[0].meltbp_aptr)
		*(meltxrestab_[0].meltbp_aptr) =
		  (melt_ptr_t) ( /*_.PROCLIST__V9*/ meltfptr[8]);
	      ;
	      /*^finalreturn */
	      ;
	      /*finalret */ goto labend_rout;
	      /*_.IF___V19*/ meltfptr[18] = /*_.RETURN___V20*/ meltfptr[19];;
	      /*epilog */

	      MELT_LOCATION ("warmelt-first.melt:4079:/ clear");
	       /*clear *//*_.RETURN___V20*/ meltfptr[19] = 0;
	    }
	    ;
	  }
	else
	  {			/*^cond.else */

    /*_.IF___V19*/ meltfptr[18] = NULL;;
	  }
	;
	/*^compute */
	/*_.LET___V17*/ meltfptr[15] = /*_.IF___V19*/ meltfptr[18];;

	MELT_LOCATION ("warmelt-first.melt:4078:/ clear");
	     /*clear *//*_.BND__V18*/ meltfptr[17] = 0;
	/*^clear */
	     /*clear *//*_.IF___V19*/ meltfptr[18] = 0;
	MELT_LOCATION ("warmelt-first.melt:4080:/ checksignal");
	MELT_CHECK_SIGNAL ();
	;
	/*^cond */
	/*cond */ if ( /*_.EPROC__V14*/ meltfptr[13])	/*then */
	  {
	    /*^cond.then */
	    /*^block */
	    /*anyblock */
	    {


	      {
		/*^locexp */
		meltgc_prepend_list ((melt_ptr_t)
				     ( /*_.PROCLIST__V9*/ meltfptr[8]),
				     (melt_ptr_t) ( /*_.EPROC__V14*/
						   meltfptr[13]));
	      }
	      ;
	      /*epilog */
	    }
	    ;
	  }			/*noelse */
	;
	MELT_LOCATION ("warmelt-first.melt:4081:/ getslot");
	{
	  melt_ptr_t slot = NULL, obj = NULL;
	  obj = (melt_ptr_t) ( /*_.ENV__V2*/ meltfptr[1]) /*=obj*/ ;
	  melt_object_get_field (slot, obj, 1, "ENV_PREV");
    /*_.ENV_PREV__V21*/ meltfptr[19] = slot;
	};
	;
	/*^compute */
	/*_.ENV__V2*/ meltfptr[1] = /*_.SETQ___V22*/ meltfptr[17] =
	  /*_.ENV_PREV__V21*/ meltfptr[19];;
	/*_.LET___V12*/ meltfptr[11] = /*_.SETQ___V22*/ meltfptr[17];;

	MELT_LOCATION ("warmelt-first.melt:4074:/ clear");
	     /*clear *//*_.BINDMAP__V13*/ meltfptr[12] = 0;
	/*^clear */
	     /*clear *//*_.EPROC__V14*/ meltfptr[13] = 0;
	/*^clear */
	     /*clear *//*_.IFCPP___V15*/ meltfptr[14] = 0;
	/*^clear */
	     /*clear *//*_.LET___V17*/ meltfptr[15] = 0;
	/*^clear */
	     /*clear *//*_.ENV_PREV__V21*/ meltfptr[19] = 0;
	/*^clear */
	     /*clear *//*_.SETQ___V22*/ meltfptr[17] = 0;
	MELT_LOCATION ("warmelt-first.melt:4072:/ checksignal");
	MELT_CHECK_SIGNAL ();
	;
	/*epilog */

	/*^clear */
	     /*clear *//*_#IS_NOT_A__L3*/ meltfnum[0] = 0;
	/*^clear */
	     /*clear *//*_.LET___V12*/ meltfptr[11] = 0;
      }
      ;
      ;
      goto labloop_FINDLOOP_3;
    labexit_FINDLOOP_3:;	/*^loopepilog */
      /*loopepilog */
      /*_.FOREVER___V10*/ meltfptr[9] = /*_.FINDLOOP__V11*/ meltfptr[10];;
    }
    ;
    /*^compute */
    /*_.LET___V8*/ meltfptr[6] = /*_.FOREVER___V10*/ meltfptr[9];;

    MELT_LOCATION ("warmelt-first.melt:4071:/ clear");
	   /*clear *//*_.PROCLIST__V9*/ meltfptr[8] = 0;
    /*^clear */
	   /*clear *//*_.FOREVER___V10*/ meltfptr[9] = 0;
    MELT_LOCATION ("warmelt-first.melt:4066:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.LET___V8*/ meltfptr[6];;

    {
      MELT_LOCATION ("warmelt-first.melt:4066:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V4*/ meltfptr[3] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V6*/ meltfptr[4] = 0;
    /*^clear */
	   /*clear *//*_.LET___V8*/ meltfptr[6] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("FIND_ENCLOSING_ENV", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_63_warmelt_first_FIND_ENCLOSING_ENV_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_63_warmelt_first_FIND_ENCLOSING_ENV */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_64_warmelt_first_PUT_ENV (meltclosure_ptr_t meltclosp_,
				   melt_ptr_t meltfirstargp_,
				   const melt_argdescr_cell_t
				   meltxargdescr_[],
				   union meltparam_un * meltxargtab_,
				   const melt_argdescr_cell_t
				   meltxresdescr_[],
				   union meltparam_un * meltxrestab_)
{
  long current_blocklevel_signals_meltrout_64_warmelt_first_PUT_ENV_melt =
    melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_64_warmelt_first_PUT_ENV_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 17
    melt_ptr_t mcfr_varptr[17];
#define MELTFRAM_NBVARNUM 10
    long mcfr_varnum[10];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_64_warmelt_first_PUT_ENV is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_64_warmelt_first_PUT_ENV_st *) meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 17; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_64_warmelt_first_PUT_ENV nbval 17*/
  meltfram__.mcfr_nbvar = 17 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("PUT_ENV", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-first.melt:4085:/ getarg");
 /*_.ENV__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.BINDING__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.BINDING__V3*/ meltfptr[2])) !=
	      NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-first.melt:4088:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_OBJECT__L1*/ meltfnum[0] =
	(melt_magic_discr ((melt_ptr_t) ( /*_.BINDING__V3*/ meltfptr[2])) ==
	 MELTOBMAG_OBJECT);;
      MELT_LOCATION ("warmelt-first.melt:4088:/ cond");
      /*cond */ if ( /*_#IS_OBJECT__L1*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V5*/ meltfptr[4] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-first.melt:4088:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check binding is obj"),
				  ("warmelt-first.melt")
				  ? ("warmelt-first.melt") : __FILE__,
				  (4088) ? (4088) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V5*/ meltfptr[4] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V4*/ meltfptr[3] = /*_.IFELSE___V5*/ meltfptr[4];;
      /*epilog */

      MELT_LOCATION ("warmelt-first.melt:4088:/ clear");
	     /*clear *//*_#IS_OBJECT__L1*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V5*/ meltfptr[4] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V4*/ meltfptr[3] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-first.melt:4089:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_OBJECT__L2*/ meltfnum[0] =
	(melt_magic_discr ((melt_ptr_t) ( /*_.ENV__V2*/ meltfptr[1])) ==
	 MELTOBMAG_OBJECT);;
      MELT_LOCATION ("warmelt-first.melt:4089:/ cond");
      /*cond */ if ( /*_#IS_OBJECT__L2*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V7*/ meltfptr[6] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-first.melt:4089:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check env is obj"),
				  ("warmelt-first.melt")
				  ? ("warmelt-first.melt") : __FILE__,
				  (4089) ? (4089) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V7*/ meltfptr[6] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V6*/ meltfptr[4] = /*_.IFELSE___V7*/ meltfptr[6];;
      /*epilog */

      MELT_LOCATION ("warmelt-first.melt:4089:/ clear");
	     /*clear *//*_#IS_OBJECT__L2*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V7*/ meltfptr[6] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V6*/ meltfptr[4] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-first.melt:4090:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L3*/ meltfnum[0] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.ENV__V2*/ meltfptr[1]),
			     (melt_ptr_t) (( /*!CLASS_ENVIRONMENT */
					    meltfrout->tabval[0])));;
      MELT_LOCATION ("warmelt-first.melt:4090:/ cond");
      /*cond */ if ( /*_#IS_A__L3*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V9*/ meltfptr[8] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-first.melt:4090:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check env"),
				  ("warmelt-first.melt")
				  ? ("warmelt-first.melt") : __FILE__,
				  (4090) ? (4090) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V9*/ meltfptr[8] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V8*/ meltfptr[6] = /*_.IFELSE___V9*/ meltfptr[8];;
      /*epilog */

      MELT_LOCATION ("warmelt-first.melt:4090:/ clear");
	     /*clear *//*_#IS_A__L3*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V9*/ meltfptr[8] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V8*/ meltfptr[6] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-first.melt:4091:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#IS_A__L4*/ meltfnum[0] =
      melt_is_instance_of ((melt_ptr_t) ( /*_.BINDING__V3*/ meltfptr[2]),
			   (melt_ptr_t) (( /*!CLASS_ANY_BINDING */ meltfrout->
					  tabval[1])));;
    /*^compute */
 /*_#NOT__L5*/ meltfnum[4] =
      (!( /*_#IS_A__L4*/ meltfnum[0]));;
    MELT_LOCATION ("warmelt-first.melt:4091:/ cond");
    /*cond */ if ( /*_#NOT__L5*/ meltfnum[4])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{


	  {
	    MELT_LOCATION ("warmelt-first.melt:4093:/ locexp");

#if MELT_HAVE_DEBUG
	    if (melt_need_debug (0))
	      melt_dbgshortbacktrace (("put_env invalid binding"), (15));
#endif
	    ;
	  }
	  ;
	  MELT_LOCATION ("warmelt-first.melt:4092:/ quasiblock");


	  /*epilog */
	}
	;
      }				/*noelse */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-first.melt:4094:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L6*/ meltfnum[5] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.BINDING__V3*/ meltfptr[2]),
			     (melt_ptr_t) (( /*!CLASS_ANY_BINDING */
					    meltfrout->tabval[1])));;
      MELT_LOCATION ("warmelt-first.melt:4094:/ cond");
      /*cond */ if ( /*_#IS_A__L6*/ meltfnum[5])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V11*/ meltfptr[10] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-first.melt:4094:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check binding"),
				  ("warmelt-first.melt")
				  ? ("warmelt-first.melt") : __FILE__,
				  (4094) ? (4094) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V11*/ meltfptr[10] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V10*/ meltfptr[8] = /*_.IFELSE___V11*/ meltfptr[10];;
      /*epilog */

      MELT_LOCATION ("warmelt-first.melt:4094:/ clear");
	     /*clear *//*_#IS_A__L6*/ meltfnum[5] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V11*/ meltfptr[10] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V10*/ meltfptr[8] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-first.melt:4095:/ quasiblock");


    /*^getslot */
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.ENV__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 0, "ENV_BIND");
  /*_.BINDMAP__V12*/ meltfptr[10] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-first.melt:4096:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.BINDING__V3*/ meltfptr[2]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 0, "BINDER");
  /*_.BINDERV__V13*/ meltfptr[12] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-first.melt:4098:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#IS_OBJECT__L7*/ meltfnum[5] =
      (melt_magic_discr ((melt_ptr_t) (( /*!BINDER */ meltfrout->tabval[2])))
       == MELTOBMAG_OBJECT);;
    /*^compute */
 /*_#NOT__L8*/ meltfnum[7] =
      (!( /*_#IS_OBJECT__L7*/ meltfnum[5]));;
    MELT_LOCATION ("warmelt-first.melt:4098:/ cond");
    /*cond */ if ( /*_#NOT__L8*/ meltfnum[7])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{


	  {
	    MELT_LOCATION ("warmelt-first.melt:4100:/ locexp");

#if MELT_HAVE_DEBUG
	    if (melt_need_debug (0))
	      melt_dbgshortbacktrace (("put_env  bad binder in binding"),
				      (5));
#endif
	    ;
	  }
	  ;
	  MELT_LOCATION ("warmelt-first.melt:4099:/ quasiblock");


	  /*epilog */
	}
	;
      }				/*noelse */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-first.melt:4101:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_MAPOBJECT__L9*/ meltfnum[8] =
	/*is_mapobject: */
	(melt_magic_discr ((melt_ptr_t) ( /*_.BINDMAP__V12*/ meltfptr[10])) ==
	 MELTOBMAG_MAPOBJECTS);;
      MELT_LOCATION ("warmelt-first.melt:4101:/ cond");
      /*cond */ if ( /*_#IS_MAPOBJECT__L9*/ meltfnum[8])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V15*/ meltfptr[14] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-first.melt:4101:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check bindmap"),
				  ("warmelt-first.melt")
				  ? ("warmelt-first.melt") : __FILE__,
				  (4101) ? (4101) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V15*/ meltfptr[14] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V14*/ meltfptr[13] = /*_.IFELSE___V15*/ meltfptr[14];;
      /*epilog */

      MELT_LOCATION ("warmelt-first.melt:4101:/ clear");
	     /*clear *//*_#IS_MAPOBJECT__L9*/ meltfnum[8] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V15*/ meltfptr[14] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V14*/ meltfptr[13] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-first.melt:4102:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_OBJECT__L10*/ meltfnum[8] =
	(melt_magic_discr ((melt_ptr_t) ( /*_.BINDERV__V13*/ meltfptr[12])) ==
	 MELTOBMAG_OBJECT);;
      MELT_LOCATION ("warmelt-first.melt:4102:/ cond");
      /*cond */ if ( /*_#IS_OBJECT__L10*/ meltfnum[8])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V17*/ meltfptr[16] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-first.melt:4102:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check binderv"),
				  ("warmelt-first.melt")
				  ? ("warmelt-first.melt") : __FILE__,
				  (4102) ? (4102) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V17*/ meltfptr[16] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V16*/ meltfptr[14] = /*_.IFELSE___V17*/ meltfptr[16];;
      /*epilog */

      MELT_LOCATION ("warmelt-first.melt:4102:/ clear");
	     /*clear *//*_#IS_OBJECT__L10*/ meltfnum[8] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V17*/ meltfptr[16] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V16*/ meltfptr[14] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

    {
      MELT_LOCATION ("warmelt-first.melt:4103:/ locexp");
      meltgc_put_mapobjects ((meltmapobjects_ptr_t)
			     ( /*_.BINDMAP__V12*/ meltfptr[10]),
			     (meltobject_ptr_t) ( /*_.BINDERV__V13*/
						 meltfptr[12]),
			     (melt_ptr_t) ( /*_.BINDING__V3*/ meltfptr[2]));
    }
    ;

    MELT_LOCATION ("warmelt-first.melt:4095:/ clear");
	   /*clear *//*_.BINDMAP__V12*/ meltfptr[10] = 0;
    /*^clear */
	   /*clear *//*_.BINDERV__V13*/ meltfptr[12] = 0;
    /*^clear */
	   /*clear *//*_#IS_OBJECT__L7*/ meltfnum[5] = 0;
    /*^clear */
	   /*clear *//*_#NOT__L8*/ meltfnum[7] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V14*/ meltfptr[13] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V16*/ meltfptr[14] = 0;
    MELT_LOCATION ("warmelt-first.melt:4085:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V4*/ meltfptr[3] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V6*/ meltfptr[4] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V8*/ meltfptr[6] = 0;
    /*^clear */
	   /*clear *//*_#IS_A__L4*/ meltfnum[0] = 0;
    /*^clear */
	   /*clear *//*_#NOT__L5*/ meltfnum[4] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V10*/ meltfptr[8] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("PUT_ENV", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_64_warmelt_first_PUT_ENV_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*noretval */ NULL);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_64_warmelt_first_PUT_ENV */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_65_warmelt_first_OVERWRITE_ENV (meltclosure_ptr_t meltclosp_,
					 melt_ptr_t meltfirstargp_,
					 const melt_argdescr_cell_t
					 meltxargdescr_[],
					 union meltparam_un * meltxargtab_,
					 const melt_argdescr_cell_t
					 meltxresdescr_[],
					 union meltparam_un * meltxrestab_)
{
  long current_blocklevel_signals_meltrout_65_warmelt_first_OVERWRITE_ENV_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_65_warmelt_first_OVERWRITE_ENV_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 20
    melt_ptr_t mcfr_varptr[20];
#define MELTFRAM_NBVARNUM 6
    long mcfr_varnum[6];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_65_warmelt_first_OVERWRITE_ENV is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_65_warmelt_first_OVERWRITE_ENV_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 20; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_65_warmelt_first_OVERWRITE_ENV nbval 20*/
  meltfram__.mcfr_nbvar = 20 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("OVERWRITE_ENV", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-first.melt:4107:/ getarg");
 /*_.ENV__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.BINDING__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.BINDING__V3*/ meltfptr[2])) !=
	      NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-first.melt:4111:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L1*/ meltfnum[0] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.ENV__V2*/ meltfptr[1]),
			     (melt_ptr_t) (( /*!CLASS_ENVIRONMENT */
					    meltfrout->tabval[0])));;
      MELT_LOCATION ("warmelt-first.melt:4111:/ cond");
      /*cond */ if ( /*_#IS_A__L1*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V5*/ meltfptr[4] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-first.melt:4111:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check env"),
				  ("warmelt-first.melt")
				  ? ("warmelt-first.melt") : __FILE__,
				  (4111) ? (4111) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V5*/ meltfptr[4] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V4*/ meltfptr[3] = /*_.IFELSE___V5*/ meltfptr[4];;
      /*epilog */

      MELT_LOCATION ("warmelt-first.melt:4111:/ clear");
	     /*clear *//*_#IS_A__L1*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V5*/ meltfptr[4] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V4*/ meltfptr[3] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-first.melt:4112:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L2*/ meltfnum[0] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.BINDING__V3*/ meltfptr[2]),
			     (melt_ptr_t) (( /*!CLASS_ANY_BINDING */
					    meltfrout->tabval[1])));;
      MELT_LOCATION ("warmelt-first.melt:4112:/ cond");
      /*cond */ if ( /*_#IS_A__L2*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V7*/ meltfptr[6] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-first.melt:4112:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check binding"),
				  ("warmelt-first.melt")
				  ? ("warmelt-first.melt") : __FILE__,
				  (4112) ? (4112) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V7*/ meltfptr[6] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V6*/ meltfptr[4] = /*_.IFELSE___V7*/ meltfptr[6];;
      /*epilog */

      MELT_LOCATION ("warmelt-first.melt:4112:/ clear");
	     /*clear *//*_#IS_A__L2*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V7*/ meltfptr[6] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V6*/ meltfptr[4] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-first.melt:4113:/ quasiblock");


    /*^getslot */
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.BINDING__V3*/ meltfptr[2]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 0, "BINDER");
  /*_.BINDERV__V9*/ meltfptr[8] = slot;
    };
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-first.melt:4114:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_OBJECT__L3*/ meltfnum[0] =
	(melt_magic_discr ((melt_ptr_t) ( /*_.BINDERV__V9*/ meltfptr[8])) ==
	 MELTOBMAG_OBJECT);;
      MELT_LOCATION ("warmelt-first.melt:4114:/ cond");
      /*cond */ if ( /*_#IS_OBJECT__L3*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V11*/ meltfptr[10] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-first.melt:4114:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check binderv"),
				  ("warmelt-first.melt")
				  ? ("warmelt-first.melt") : __FILE__,
				  (4114) ? (4114) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V11*/ meltfptr[10] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V10*/ meltfptr[9] = /*_.IFELSE___V11*/ meltfptr[10];;
      /*epilog */

      MELT_LOCATION ("warmelt-first.melt:4114:/ clear");
	     /*clear *//*_#IS_OBJECT__L3*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V11*/ meltfptr[10] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V10*/ meltfptr[9] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-first.melt:4115:/ loop");
    /*loop */
    {
    labloop_FINDLOOP_4:;	/*^loopbody */

      /*^block */
      /*anyblock */
      {

	/*^checksignal */
	MELT_CHECK_SIGNAL ();
	;
	MELT_LOCATION ("warmelt-first.melt:4116:/ checksignal");
	MELT_CHECK_SIGNAL ();
	;
   /*_#IS_A__L4*/ meltfnum[0] =
	  melt_is_instance_of ((melt_ptr_t) ( /*_.ENV__V2*/ meltfptr[1]),
			       (melt_ptr_t) (( /*!CLASS_ENVIRONMENT */
					      meltfrout->tabval[0])));;
	/*^compute */
   /*_#NOT__L5*/ meltfnum[4] =
	  (!( /*_#IS_A__L4*/ meltfnum[0]));;
	MELT_LOCATION ("warmelt-first.melt:4116:/ cond");
	/*cond */ if ( /*_#NOT__L5*/ meltfnum[4])	/*then */
	  {
	    /*^cond.then */
	    /*^block */
	    /*anyblock */
	    {

	      /*^quasiblock */


	      /*^compute */
     /*_.FINDLOOP__V13*/ meltfptr[12] = NULL;;

	      /*^exit */
	      /*exit */
	      {
		goto labexit_FINDLOOP_4;
	      }
	      ;
	      /*epilog */
	    }
	    ;
	  }			/*noelse */
	;
	MELT_LOCATION ("warmelt-first.melt:4117:/ quasiblock");


	/*^getslot */
	{
	  melt_ptr_t slot = NULL, obj = NULL;
	  obj = (melt_ptr_t) ( /*_.ENV__V2*/ meltfptr[1]) /*=obj*/ ;
	  melt_object_get_field (slot, obj, 0, "ENV_BIND");
    /*_.BINDMAP__V15*/ meltfptr[14] = slot;
	};
	;

#if MELT_HAVE_DEBUG
	MELT_LOCATION ("warmelt-first.melt:4118:/ cppif.then");
	/*^block */
	/*anyblock */
	{

	  /*^checksignal */
	  MELT_CHECK_SIGNAL ();
	  ;
     /*_#IS_MAPOBJECT__L6*/ meltfnum[5] =
	    /*is_mapobject: */
	    (melt_magic_discr
	     ((melt_ptr_t) ( /*_.BINDMAP__V15*/ meltfptr[14])) ==
	     MELTOBMAG_MAPOBJECTS);;
	  MELT_LOCATION ("warmelt-first.melt:4118:/ cond");
	  /*cond */ if ( /*_#IS_MAPOBJECT__L6*/ meltfnum[5])	/*then */
	    {
	      /*^cond.then */
	      /*_.IFELSE___V17*/ meltfptr[16] = ( /*nil */ NULL);;
	    }
	  else
	    {
	      MELT_LOCATION ("warmelt-first.melt:4118:/ cond.else");

	      /*^block */
	      /*anyblock */
	      {




		{
		  /*^locexp */
		  melt_assert_failed (("check bindmap"),
				      ("warmelt-first.melt")
				      ? ("warmelt-first.melt") : __FILE__,
				      (4118) ? (4118) : __LINE__,
				      __FUNCTION__);
		  ;
		}
		;
		 /*clear *//*_.IFELSE___V17*/ meltfptr[16] = 0;
		/*epilog */
	      }
	      ;
	    }
	  ;
	  /*^compute */
	  /*_.IFCPP___V16*/ meltfptr[15] = /*_.IFELSE___V17*/ meltfptr[16];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-first.melt:4118:/ clear");
	       /*clear *//*_#IS_MAPOBJECT__L6*/ meltfnum[5] = 0;
	  /*^clear */
	       /*clear *//*_.IFELSE___V17*/ meltfptr[16] = 0;
	}

#else /*MELT_HAVE_DEBUG */
	/*^cppif.else */
	/*_.IFCPP___V16*/ meltfptr[15] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
	;
	MELT_LOCATION ("warmelt-first.melt:4119:/ quasiblock");


   /*_.OLDBINDING__V18*/ meltfptr[16] =
	  /*mapobject_get */
	  melt_get_mapobjects ((meltmapobjects_ptr_t)
			       ( /*_.BINDMAP__V15*/ meltfptr[14]),
			       (meltobject_ptr_t) (( /*!BINDER */ meltfrout->
						    tabval[2])));;
	MELT_LOCATION ("warmelt-first.melt:4120:/ checksignal");
	MELT_CHECK_SIGNAL ();
	;
	/*^cond */
	/*cond */ if ( /*_.OLDBINDING__V18*/ meltfptr[16])	/*then */
	  {
	    /*^cond.then */
	    /*^block */
	    /*anyblock */
	    {


	      {
		MELT_LOCATION ("warmelt-first.melt:4122:/ locexp");
		meltgc_put_mapobjects ((meltmapobjects_ptr_t)
				       ( /*_.BINDMAP__V15*/ meltfptr[14]),
				       (meltobject_ptr_t) ( /*_.BINDERV__V9*/
							   meltfptr[8]),
				       (melt_ptr_t) ( /*_.BINDING__V3*/
						     meltfptr[2]));
	      }
	      ;
	      MELT_LOCATION ("warmelt-first.melt:4123:/ quasiblock");


	      /*^compute */
	      /*_.FINDLOOP__V13*/ meltfptr[12] =
		/*_.OLDBINDING__V18*/ meltfptr[16];;

	      /*^exit */
	      /*exit */
	      {
		goto labexit_FINDLOOP_4;
	      }
	      ;
	      MELT_LOCATION ("warmelt-first.melt:4121:/ quasiblock");


	      /*epilog */
	    }
	    ;
	  }			/*noelse */
	;

	MELT_LOCATION ("warmelt-first.melt:4119:/ clear");
	     /*clear *//*_.OLDBINDING__V18*/ meltfptr[16] = 0;
	MELT_LOCATION ("warmelt-first.melt:4125:/ getslot");
	{
	  melt_ptr_t slot = NULL, obj = NULL;
	  obj = (melt_ptr_t) ( /*_.ENV__V2*/ meltfptr[1]) /*=obj*/ ;
	  melt_object_get_field (slot, obj, 1, "ENV_PREV");
    /*_.ENV_PREV__V19*/ meltfptr[16] = slot;
	};
	;
	/*^compute */
	/*_.ENV__V2*/ meltfptr[1] = /*_.SETQ___V20*/ meltfptr[19] =
	  /*_.ENV_PREV__V19*/ meltfptr[16];;
	/*_.LET___V14*/ meltfptr[13] = /*_.SETQ___V20*/ meltfptr[19];;

	MELT_LOCATION ("warmelt-first.melt:4117:/ clear");
	     /*clear *//*_.BINDMAP__V15*/ meltfptr[14] = 0;
	/*^clear */
	     /*clear *//*_.IFCPP___V16*/ meltfptr[15] = 0;
	/*^clear */
	     /*clear *//*_.ENV_PREV__V19*/ meltfptr[16] = 0;
	/*^clear */
	     /*clear *//*_.SETQ___V20*/ meltfptr[19] = 0;
	MELT_LOCATION ("warmelt-first.melt:4115:/ checksignal");
	MELT_CHECK_SIGNAL ();
	;
	/*epilog */

	/*^clear */
	     /*clear *//*_#IS_A__L4*/ meltfnum[0] = 0;
	/*^clear */
	     /*clear *//*_#NOT__L5*/ meltfnum[4] = 0;
	/*^clear */
	     /*clear *//*_.LET___V14*/ meltfptr[13] = 0;
      }
      ;
      ;
      goto labloop_FINDLOOP_4;
    labexit_FINDLOOP_4:;	/*^loopepilog */
      /*loopepilog */
      /*_.FOREVER___V12*/ meltfptr[10] = /*_.FINDLOOP__V13*/ meltfptr[12];;
    }
    ;
    /*^compute */
    /*_.LET___V8*/ meltfptr[6] = /*_.FOREVER___V12*/ meltfptr[10];;

    MELT_LOCATION ("warmelt-first.melt:4113:/ clear");
	   /*clear *//*_.BINDERV__V9*/ meltfptr[8] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V10*/ meltfptr[9] = 0;
    /*^clear */
	   /*clear *//*_.FOREVER___V12*/ meltfptr[10] = 0;
    MELT_LOCATION ("warmelt-first.melt:4107:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.LET___V8*/ meltfptr[6];;

    {
      MELT_LOCATION ("warmelt-first.melt:4107:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V4*/ meltfptr[3] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V6*/ meltfptr[4] = 0;
    /*^clear */
	   /*clear *//*_.LET___V8*/ meltfptr[6] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("OVERWRITE_ENV", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_65_warmelt_first_OVERWRITE_ENV_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_65_warmelt_first_OVERWRITE_ENV */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_66_warmelt_first_POST_INITIALIZATION (meltclosure_ptr_t meltclosp_,
					       melt_ptr_t meltfirstargp_,
					       const melt_argdescr_cell_t
					       meltxargdescr_[],
					       union meltparam_un *
					       meltxargtab_,
					       const melt_argdescr_cell_t
					       meltxresdescr_[],
					       union meltparam_un *
					       meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_66_warmelt_first_POST_INITIALIZATION_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_66_warmelt_first_POST_INITIALIZATION_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 14
    melt_ptr_t mcfr_varptr[14];
#define MELTFRAM_NBVARNUM 5
    long mcfr_varnum[5];
/*others*/
    const char *loc_CSTRING__o0;
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_66_warmelt_first_POST_INITIALIZATION is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_66_warmelt_first_POST_INITIALIZATION_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 14; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_66_warmelt_first_POST_INITIALIZATION nbval 14*/
  meltfram__.mcfr_nbvar = 14 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("POST_INITIALIZATION", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-first.melt:4140:/ getarg");
 /*_.UNUSEDARG__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_CSTRING)
    goto lab_endgetargs;
 /*_?*/ meltfram__.loc_CSTRING__o0 = meltxargtab_[0].meltbp_cstring;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {

    MELT_LOCATION ("warmelt-first.melt:4141:/ quasiblock");


 /*_.CURMODENVCONT__V3*/ meltfptr[2] =
      /*quasi.cur.mod.env.cont */
      ( /*!konst_0 */ meltfrout->tabval[0]);;
    MELT_LOCATION ("warmelt-first.melt:4142:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#IS_A__L1*/ meltfnum[0] =
      melt_is_instance_of ((melt_ptr_t)
			   ( /*_.CURMODENVCONT__V3*/ meltfptr[2]),
			   (melt_ptr_t) (( /*!CLASS_REFERENCE */ meltfrout->
					  tabval[1])));;
    /*^compute */
 /*_#NOT__L2*/ meltfnum[1] =
      (!( /*_#IS_A__L1*/ meltfnum[0]));;
    MELT_LOCATION ("warmelt-first.melt:4142:/ cond");
    /*cond */ if ( /*_#NOT__L2*/ meltfnum[1])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

   /*_.DISCRIM__V5*/ meltfptr[4] =
	    ((melt_ptr_t)
	     (melt_discr
	      ((melt_ptr_t) ( /*_.CURMODENVCONT__V3*/ meltfptr[2]))));;
	  MELT_LOCATION ("warmelt-first.melt:4145:/ getslot");
	  {
	    melt_ptr_t slot = NULL, obj = NULL;
	    obj = (melt_ptr_t) ( /*_.DISCRIM__V5*/ meltfptr[4]) /*=obj*/ ;
	    melt_object_get_field (slot, obj, 1, "NAMED_NAME");
    /*_.NAMED_NAME__V6*/ meltfptr[5] = slot;
	  };
	  ;

	  {
	    MELT_LOCATION ("warmelt-first.melt:4144:/ locexp");
	    warning (0, "MELT WARNING MSG [#%ld]::: %s - %s", melt_dbgcounter,
		     ("post_initialization strange curmodenvcont of discr"),
		     melt_string_str ((melt_ptr_t)
				      ( /*_.NAMED_NAME__V6*/ meltfptr[5])));
	  }
	  ;
	  MELT_LOCATION ("warmelt-first.melt:4146:/ quasiblock");


   /*_.RETVAL___V1*/ meltfptr[0] = NULL;;

	  {
	    MELT_LOCATION ("warmelt-first.melt:4146:/ locexp");
	    /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
	    if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	      melt_warn_for_no_expected_secondary_results ();
	    /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
	    ;
	  }
	  ;
	  /*^finalreturn */
	  ;
	  /*finalret */ goto labend_rout;
	  MELT_LOCATION ("warmelt-first.melt:4143:/ quasiblock");


	  /*_.PROGN___V8*/ meltfptr[7] = /*_.RETURN___V7*/ meltfptr[6];;
	  /*^compute */
	  /*_.IF___V4*/ meltfptr[3] = /*_.PROGN___V8*/ meltfptr[7];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-first.melt:4142:/ clear");
	     /*clear *//*_.DISCRIM__V5*/ meltfptr[4] = 0;
	  /*^clear */
	     /*clear *//*_.NAMED_NAME__V6*/ meltfptr[5] = 0;
	  /*^clear */
	     /*clear *//*_.RETURN___V7*/ meltfptr[6] = 0;
	  /*^clear */
	     /*clear *//*_.PROGN___V8*/ meltfptr[7] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

  /*_.IF___V4*/ meltfptr[3] = NULL;;
      }
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-first.melt:4148:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L3*/ meltfnum[2] =
	melt_is_instance_of ((melt_ptr_t)
			     ( /*_.CURMODENVCONT__V3*/ meltfptr[2]),
			     (melt_ptr_t) (( /*!CLASS_REFERENCE */ meltfrout->
					    tabval[1])));;
      MELT_LOCATION ("warmelt-first.melt:4148:/ cond");
      /*cond */ if ( /*_#IS_A__L3*/ meltfnum[2])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V10*/ meltfptr[5] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-first.melt:4148:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check curmodenvcont"),
				  ("warmelt-first.melt")
				  ? ("warmelt-first.melt") : __FILE__,
				  (4148) ? (4148) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V10*/ meltfptr[5] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V9*/ meltfptr[4] = /*_.IFELSE___V10*/ meltfptr[5];;
      /*epilog */

      MELT_LOCATION ("warmelt-first.melt:4148:/ clear");
	     /*clear *//*_#IS_A__L3*/ meltfnum[2] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V10*/ meltfptr[5] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V9*/ meltfptr[4] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-first.melt:4149:/ quasiblock");


    /*^getslot */
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.CURMODENVCONT__V3*/ meltfptr[2]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 0, "REFERENCED_VALUE");
  /*_.CURMODENV__V11*/ meltfptr[6] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-first.melt:4150:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#IS_A__L4*/ meltfnum[2] =
      melt_is_instance_of ((melt_ptr_t) ( /*_.CURMODENV__V11*/ meltfptr[6]),
			   (melt_ptr_t) (( /*!CLASS_ENVIRONMENT */ meltfrout->
					  tabval[2])));;
    MELT_LOCATION ("warmelt-first.melt:4150:/ cond");
    /*cond */ if ( /*_#IS_A__L4*/ meltfnum[2])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-first.melt:4151:/ quasiblock");


	  /*^getslot */
	  {
	    melt_ptr_t slot = NULL, obj = NULL;
	    obj = (melt_ptr_t) ( /*_.CURMODENV__V11*/ meltfptr[6]) /*=obj*/ ;
	    melt_object_get_field (slot, obj, 0, "ENV_BIND");
    /*_.CURBINDMAP__V12*/ meltfptr[7] = slot;
	  };
	  ;
   /*_#MAPOBJECT_COUNT__L5*/ meltfnum[4] =
	    (melt_count_mapobjects
	     ((meltmapobjects_ptr_t) ( /*_.CURBINDMAP__V12*/ meltfptr[7])));;

	  {
	    MELT_LOCATION ("warmelt-first.melt:4153:/ locexp");
	    inform (UNKNOWN_LOCATION, "MELT INFORM [#%ld]: %s * %ld",
		    melt_dbgcounter, ("post_initialization boundvars num"),
		    ( /*_#MAPOBJECT_COUNT__L5*/ meltfnum[4]));
	  }
	  ;

	  MELT_LOCATION ("warmelt-first.melt:4151:/ clear");
	     /*clear *//*_.CURBINDMAP__V12*/ meltfptr[7] = 0;
	  /*^clear */
	     /*clear *//*_#MAPOBJECT_COUNT__L5*/ meltfnum[4] = 0;
	  /*epilog */
	}
	;
      }
    else
      {
	MELT_LOCATION ("warmelt-first.melt:4150:/ cond.else");

	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-first.melt:4156:/ quasiblock");


   /*_.CURMENVDISCR__V13*/ meltfptr[5] =
	    ((melt_ptr_t)
	     (melt_discr
	      ((melt_ptr_t) ( /*_.CURMODENV__V11*/ meltfptr[6]))));;
	  MELT_LOCATION ("warmelt-first.melt:4158:/ getslot");
	  {
	    melt_ptr_t slot = NULL, obj = NULL;
	    obj =
	      (melt_ptr_t) ( /*_.CURMENVDISCR__V13*/ meltfptr[5]) /*=obj*/ ;
	    melt_object_get_field (slot, obj, 1, "NAMED_NAME");
    /*_.NAMED_NAME__V14*/ meltfptr[7] = slot;
	  };
	  ;

	  {
	    MELT_LOCATION ("warmelt-first.melt:4157:/ locexp");
	    inform (UNKNOWN_LOCATION, ("MELT INFORM [#%ld]: %s - %s"),
		    melt_dbgcounter,
		    ("post_initialization strange curmodenv of discr"),
		    melt_string_str ((melt_ptr_t)
				     ( /*_.NAMED_NAME__V14*/ meltfptr[7])));
	  }
	  ;

	  MELT_LOCATION ("warmelt-first.melt:4156:/ clear");
	     /*clear *//*_.CURMENVDISCR__V13*/ meltfptr[5] = 0;
	  /*^clear */
	     /*clear *//*_.NAMED_NAME__V14*/ meltfptr[7] = 0;
	  /*epilog */
	}
	;
      }
    ;

    MELT_LOCATION ("warmelt-first.melt:4149:/ clear");
	   /*clear *//*_.CURMODENV__V11*/ meltfptr[6] = 0;
    /*^clear */
	   /*clear *//*_#IS_A__L4*/ meltfnum[2] = 0;

    MELT_LOCATION ("warmelt-first.melt:4141:/ clear");
	   /*clear *//*_.CURMODENVCONT__V3*/ meltfptr[2] = 0;
    /*^clear */
	   /*clear *//*_#IS_A__L1*/ meltfnum[0] = 0;
    /*^clear */
	   /*clear *//*_#NOT__L2*/ meltfnum[1] = 0;
    /*^clear */
	   /*clear *//*_.IF___V4*/ meltfptr[3] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V9*/ meltfptr[4] = 0;
    /*epilog */
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("POST_INITIALIZATION", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_66_warmelt_first_POST_INITIALIZATION_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_66_warmelt_first_POST_INITIALIZATION */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_67_warmelt_first_RETRIEVE_DICTIONNARY_CTYPE_GTY (meltclosure_ptr_t
							  meltclosp_,
							  melt_ptr_t
							  meltfirstargp_,
							  const
							  melt_argdescr_cell_t
							  meltxargdescr_[],
							  union meltparam_un *
							  meltxargtab_,
							  const
							  melt_argdescr_cell_t
							  meltxresdescr_[],
							  union meltparam_un *
							  meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_67_warmelt_first_RETRIEVE_DICTIONNARY_CTYPE_GTY_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_67_warmelt_first_RETRIEVE_DICTIONNARY_CTYPE_GTY_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 2
    melt_ptr_t mcfr_varptr[2];
/*no varnum*/
#define MELTFRAM_NBVARNUM /*none*/0
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_67_warmelt_first_RETRIEVE_DICTIONNARY_CTYPE_GTY is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct
	 frame_meltrout_67_warmelt_first_RETRIEVE_DICTIONNARY_CTYPE_GTY_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 2; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_67_warmelt_first_RETRIEVE_DICTIONNARY_CTYPE_GTY nbval 2*/
  meltfram__.mcfr_nbvar = 2 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("RETRIEVE_DICTIONNARY_CTYPE_GTY", meltcallcount);
/*getargs*/
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
  MELT_LOCATION ("warmelt-first.melt:4189:/ block");
/*anyblock*/
  {

    MELT_LOCATION ("warmelt-first.melt:4191:/ cond");
    /*cond */ if (
		   /*ifisa */
		   melt_is_instance_of ((melt_ptr_t)
					(( /*!CONTAINER_CTYPE_GTY_DICT */
					  meltfrout->tabval[0])),
					(melt_ptr_t) (( /*!CLASS_REFERENCE */
						       meltfrout->tabval[1])))
      )				/*then */
      {
	/*^cond.then */
	/*^getslot */
	{
	  melt_ptr_t slot = NULL, obj = NULL;
	  obj =
	    (melt_ptr_t) (( /*!CONTAINER_CTYPE_GTY_DICT */ meltfrout->
			   tabval[0])) /*=obj*/ ;
	  melt_object_get_field (slot, obj, 0, "REFERENCED_VALUE");
   /*_.REFERENCED_VALUE__V2*/ meltfptr[1] = slot;
	};
	;
      }
    else
      {				/*^cond.else */

  /*_.REFERENCED_VALUE__V2*/ meltfptr[1] = NULL;;
      }
    ;
    MELT_LOCATION ("warmelt-first.melt:4189:/ quasiblock");


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.REFERENCED_VALUE__V2*/ meltfptr[1];;

    {
      MELT_LOCATION ("warmelt-first.melt:4189:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.REFERENCED_VALUE__V2*/ meltfptr[1] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("RETRIEVE_DICTIONNARY_CTYPE_GTY", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_67_warmelt_first_RETRIEVE_DICTIONNARY_CTYPE_GTY_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_67_warmelt_first_RETRIEVE_DICTIONNARY_CTYPE_GTY */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_68_warmelt_first_RETRIEVE_DICTIONNARY_CTYPE (meltclosure_ptr_t
						      meltclosp_,
						      melt_ptr_t
						      meltfirstargp_,
						      const
						      melt_argdescr_cell_t
						      meltxargdescr_[],
						      union meltparam_un *
						      meltxargtab_,
						      const
						      melt_argdescr_cell_t
						      meltxresdescr_[],
						      union meltparam_un *
						      meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_68_warmelt_first_RETRIEVE_DICTIONNARY_CTYPE_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_68_warmelt_first_RETRIEVE_DICTIONNARY_CTYPE_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 2
    melt_ptr_t mcfr_varptr[2];
/*no varnum*/
#define MELTFRAM_NBVARNUM /*none*/0
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_68_warmelt_first_RETRIEVE_DICTIONNARY_CTYPE is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_68_warmelt_first_RETRIEVE_DICTIONNARY_CTYPE_st
	 *) meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 2; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_68_warmelt_first_RETRIEVE_DICTIONNARY_CTYPE nbval 2*/
  meltfram__.mcfr_nbvar = 2 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("RETRIEVE_DICTIONNARY_CTYPE", meltcallcount);
/*getargs*/
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
  MELT_LOCATION ("warmelt-first.melt:4194:/ block");
/*anyblock*/
  {

    MELT_LOCATION ("warmelt-first.melt:4196:/ cond");
    /*cond */ if (
		   /*ifisa */
		   melt_is_instance_of ((melt_ptr_t)
					(( /*!CONTAINER_CTYPE_DICT */
					  meltfrout->tabval[0])),
					(melt_ptr_t) (( /*!CLASS_REFERENCE */
						       meltfrout->tabval[1])))
      )				/*then */
      {
	/*^cond.then */
	/*^getslot */
	{
	  melt_ptr_t slot = NULL, obj = NULL;
	  obj =
	    (melt_ptr_t) (( /*!CONTAINER_CTYPE_DICT */ meltfrout->
			   tabval[0])) /*=obj*/ ;
	  melt_object_get_field (slot, obj, 0, "REFERENCED_VALUE");
   /*_.REFERENCED_VALUE__V2*/ meltfptr[1] = slot;
	};
	;
      }
    else
      {				/*^cond.else */

  /*_.REFERENCED_VALUE__V2*/ meltfptr[1] = NULL;;
      }
    ;
    MELT_LOCATION ("warmelt-first.melt:4194:/ quasiblock");


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.REFERENCED_VALUE__V2*/ meltfptr[1];;

    {
      MELT_LOCATION ("warmelt-first.melt:4194:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.REFERENCED_VALUE__V2*/ meltfptr[1] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("RETRIEVE_DICTIONNARY_CTYPE", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_68_warmelt_first_RETRIEVE_DICTIONNARY_CTYPE_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_68_warmelt_first_RETRIEVE_DICTIONNARY_CTYPE */



/**** end of warmelt-first+02.c ****/
