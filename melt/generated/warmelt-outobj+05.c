/* GCC MELT GENERATED FILE warmelt-outobj+05.c - DO NOT EDIT */
/* secondary MELT generated C file of rank #5 */
#include "melt-run.h"


/* used hash from melt-run.h when compiling this file: */
MELT_EXTERN const char meltrun_used_md5_melt_f5[] =
  MELT_RUN_HASHMD5 /* from melt-run.h */ ;


/**** warmelt-outobj+05.c declarations ****/


/***************************************************
***
    Copyright (C) 2008 - 2012 Free Software Foundation, Inc.
    Contributed by Basile Starynkevitch <basile@starynkevitch.net>

    This file is part of GCC.

    GCC is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3, or (at your option)
    any later version.

    GCC is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with GCC; see the file COPYING3.  If not see
    <http://www.gnu.org/licenses/>.
***
****************************************************/

/* ordinary MELT module */
#define MELT_HAS_INITIAL_ENVIRONMENT 1	/*usual */

struct melt_callframe_st;	/*defined in melt-runtime.h */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_1_warmelt_outobj_GET_CODE_BUFFER_LIMIT (meltclosure_ptr_t meltclosp_,
						 melt_ptr_t meltfirstargp_,
						 const melt_argdescr_cell_t
						 meltxargdescr_[],
						 union meltparam_un
						 *meltxargtab_,
						 const melt_argdescr_cell_t
						 meltxresdescr_[],
						 union meltparam_un
						 *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_2_warmelt_outobj_PUT_CODE_BUFFER_LIMIT (meltclosure_ptr_t meltclosp_,
						 melt_ptr_t meltfirstargp_,
						 const melt_argdescr_cell_t
						 meltxargdescr_[],
						 union meltparam_un
						 *meltxargtab_,
						 const melt_argdescr_cell_t
						 meltxresdescr_[],
						 union meltparam_un
						 *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_3_warmelt_outobj_CODE_BUFFER_LIMIT_OPTSET (meltclosure_ptr_t
						    meltclosp_,
						    melt_ptr_t meltfirstargp_,
						    const melt_argdescr_cell_t
						    meltxargdescr_[],
						    union meltparam_un
						    *meltxargtab_,
						    const melt_argdescr_cell_t
						    meltxresdescr_[],
						    union meltparam_un
						    *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_4_warmelt_outobj_OUTDECLINIT_ROOT (meltclosure_ptr_t meltclosp_,
					    melt_ptr_t meltfirstargp_,
					    const melt_argdescr_cell_t
					    meltxargdescr_[],
					    union meltparam_un *meltxargtab_,
					    const melt_argdescr_cell_t
					    meltxresdescr_[],
					    union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_5_warmelt_outobj_OUTPUCOD_OBJINIELEM (meltclosure_ptr_t meltclosp_,
					       melt_ptr_t meltfirstargp_,
					       const melt_argdescr_cell_t
					       meltxargdescr_[],
					       union meltparam_un
					       *meltxargtab_,
					       const melt_argdescr_cell_t
					       meltxresdescr_[],
					       union meltparam_un
					       *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_6_warmelt_outobj_OUTCINITFILL_ROOT (meltclosure_ptr_t meltclosp_,
					     melt_ptr_t meltfirstargp_,
					     const melt_argdescr_cell_t
					     meltxargdescr_[],
					     union meltparam_un *meltxargtab_,
					     const melt_argdescr_cell_t
					     meltxresdescr_[],
					     union meltparam_un
					     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_7_warmelt_outobj_OUTCINITPREDEF_ROOT (meltclosure_ptr_t meltclosp_,
					       melt_ptr_t meltfirstargp_,
					       const melt_argdescr_cell_t
					       meltxargdescr_[],
					       union meltparam_un
					       *meltxargtab_,
					       const melt_argdescr_cell_t
					       meltxresdescr_[],
					       union meltparam_un
					       *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_8_warmelt_outobj_OUTPUT_PREDEF (meltclosure_ptr_t meltclosp_,
					 melt_ptr_t meltfirstargp_,
					 const melt_argdescr_cell_t
					 meltxargdescr_[],
					 union meltparam_un *meltxargtab_,
					 const melt_argdescr_cell_t
					 meltxresdescr_[],
					 union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_9_warmelt_outobj_OUTPUCOD_PREDEF (meltclosure_ptr_t meltclosp_,
					   melt_ptr_t meltfirstargp_,
					   const melt_argdescr_cell_t
					   meltxargdescr_[],
					   union meltparam_un *meltxargtab_,
					   const melt_argdescr_cell_t
					   meltxresdescr_[],
					   union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_10_warmelt_outobj_OUTPUCOD_NIL (meltclosure_ptr_t meltclosp_,
					 melt_ptr_t meltfirstargp_,
					 const melt_argdescr_cell_t
					 meltxargdescr_[],
					 union meltparam_un *meltxargtab_,
					 const melt_argdescr_cell_t
					 meltxresdescr_[],
					 union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_11_warmelt_outobj_OUTPUCOD_OBJEXPANDPUREVAL (meltclosure_ptr_t
						      meltclosp_,
						      melt_ptr_t
						      meltfirstargp_,
						      const
						      melt_argdescr_cell_t
						      meltxargdescr_[],
						      union meltparam_un
						      *meltxargtab_,
						      const
						      melt_argdescr_cell_t
						      meltxresdescr_[],
						      union meltparam_un
						      *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_12_warmelt_outobj_OUTDECLINIT_OBJINITOBJECT (meltclosure_ptr_t
						      meltclosp_,
						      melt_ptr_t
						      meltfirstargp_,
						      const
						      melt_argdescr_cell_t
						      meltxargdescr_[],
						      union meltparam_un
						      *meltxargtab_,
						      const
						      melt_argdescr_cell_t
						      meltxresdescr_[],
						      union meltparam_un
						      *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_13_warmelt_outobj_OUTCINITFILL_OBJINITOBJECT (meltclosure_ptr_t
						       meltclosp_,
						       melt_ptr_t
						       meltfirstargp_,
						       const
						       melt_argdescr_cell_t
						       meltxargdescr_[],
						       union meltparam_un
						       *meltxargtab_,
						       const
						       melt_argdescr_cell_t
						       meltxresdescr_[],
						       union meltparam_un
						       *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_14_warmelt_outobj_OUTCINITPREDEF_OBJINITOBJECT (meltclosure_ptr_t
							 meltclosp_,
							 melt_ptr_t
							 meltfirstargp_,
							 const
							 melt_argdescr_cell_t
							 meltxargdescr_[],
							 union meltparam_un
							 *meltxargtab_,
							 const
							 melt_argdescr_cell_t
							 meltxresdescr_[],
							 union meltparam_un
							 *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_15_warmelt_outobj_OUTDECLINIT_OBJINITMULTIPLE (meltclosure_ptr_t
							meltclosp_,
							melt_ptr_t
							meltfirstargp_,
							const
							melt_argdescr_cell_t
							meltxargdescr_[],
							union meltparam_un
							*meltxargtab_,
							const
							melt_argdescr_cell_t
							meltxresdescr_[],
							union meltparam_un
							*meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_16_warmelt_outobj_OUTCINITFILL_OBJINITMULTIPLE (meltclosure_ptr_t
							 meltclosp_,
							 melt_ptr_t
							 meltfirstargp_,
							 const
							 melt_argdescr_cell_t
							 meltxargdescr_[],
							 union meltparam_un
							 *meltxargtab_,
							 const
							 melt_argdescr_cell_t
							 meltxresdescr_[],
							 union meltparam_un
							 *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_17_warmelt_outobj_OUTDECLINIT_OBJINITCLOSURE (meltclosure_ptr_t
						       meltclosp_,
						       melt_ptr_t
						       meltfirstargp_,
						       const
						       melt_argdescr_cell_t
						       meltxargdescr_[],
						       union meltparam_un
						       *meltxargtab_,
						       const
						       melt_argdescr_cell_t
						       meltxresdescr_[],
						       union meltparam_un
						       *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_18_warmelt_outobj_OUTCINITFILL_OBJINITCLOSURE (meltclosure_ptr_t
							meltclosp_,
							melt_ptr_t
							meltfirstargp_,
							const
							melt_argdescr_cell_t
							meltxargdescr_[],
							union meltparam_un
							*meltxargtab_,
							const
							melt_argdescr_cell_t
							meltxresdescr_[],
							union meltparam_un
							*meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_19_warmelt_outobj_OUTDECLINIT_OBJINITROUTINE (meltclosure_ptr_t
						       meltclosp_,
						       melt_ptr_t
						       meltfirstargp_,
						       const
						       melt_argdescr_cell_t
						       meltxargdescr_[],
						       union meltparam_un
						       *meltxargtab_,
						       const
						       melt_argdescr_cell_t
						       meltxresdescr_[],
						       union meltparam_un
						       *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_20_warmelt_outobj_OUTCINITFILL_OBJINITROUTINE (meltclosure_ptr_t
							meltclosp_,
							melt_ptr_t
							meltfirstargp_,
							const
							melt_argdescr_cell_t
							meltxargdescr_[],
							union meltparam_un
							*meltxargtab_,
							const
							melt_argdescr_cell_t
							meltxresdescr_[],
							union meltparam_un
							*meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_21_warmelt_outobj_OUTDECLINIT_OBJINITSTRING (meltclosure_ptr_t
						      meltclosp_,
						      melt_ptr_t
						      meltfirstargp_,
						      const
						      melt_argdescr_cell_t
						      meltxargdescr_[],
						      union meltparam_un
						      *meltxargtab_,
						      const
						      melt_argdescr_cell_t
						      meltxresdescr_[],
						      union meltparam_un
						      *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_22_warmelt_outobj_OUTCINITFILL_OBJINITSTRING (meltclosure_ptr_t
						       meltclosp_,
						       melt_ptr_t
						       meltfirstargp_,
						       const
						       melt_argdescr_cell_t
						       meltxargdescr_[],
						       union meltparam_un
						       *meltxargtab_,
						       const
						       melt_argdescr_cell_t
						       meltxresdescr_[],
						       union meltparam_un
						       *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_23_warmelt_outobj_OUTDECLINIT_OBJINITBOXEDINTEGER (meltclosure_ptr_t
							    meltclosp_,
							    melt_ptr_t
							    meltfirstargp_,
							    const
							    melt_argdescr_cell_t
							    meltxargdescr_[],
							    union meltparam_un
							    *meltxargtab_,
							    const
							    melt_argdescr_cell_t
							    meltxresdescr_[],
							    union meltparam_un
							    *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_24_warmelt_outobj_OUTCINITFILL_OBJINITBOXEDINTEGER (meltclosure_ptr_t
							     meltclosp_,
							     melt_ptr_t
							     meltfirstargp_,
							     const
							     melt_argdescr_cell_t
							     meltxargdescr_[],
							     union
							     meltparam_un
							     *meltxargtab_,
							     const
							     melt_argdescr_cell_t
							     meltxresdescr_[],
							     union
							     meltparam_un
							     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_25_warmelt_outobj_OUTDECLINIT_OBJINITPAIR (meltclosure_ptr_t
						    meltclosp_,
						    melt_ptr_t meltfirstargp_,
						    const melt_argdescr_cell_t
						    meltxargdescr_[],
						    union meltparam_un
						    *meltxargtab_,
						    const melt_argdescr_cell_t
						    meltxresdescr_[],
						    union meltparam_un
						    *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_26_warmelt_outobj_OUTCINITFILL_OBJINITPAIR (meltclosure_ptr_t
						     meltclosp_,
						     melt_ptr_t
						     meltfirstargp_,
						     const
						     melt_argdescr_cell_t
						     meltxargdescr_[],
						     union meltparam_un
						     *meltxargtab_,
						     const
						     melt_argdescr_cell_t
						     meltxresdescr_[],
						     union meltparam_un
						     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_27_warmelt_outobj_OUTDECLINIT_OBJINITLIST (meltclosure_ptr_t
						    meltclosp_,
						    melt_ptr_t meltfirstargp_,
						    const melt_argdescr_cell_t
						    meltxargdescr_[],
						    union meltparam_un
						    *meltxargtab_,
						    const melt_argdescr_cell_t
						    meltxresdescr_[],
						    union meltparam_un
						    *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_28_warmelt_outobj_OUTCINITFILL_OBJINITLIST (meltclosure_ptr_t
						     meltclosp_,
						     melt_ptr_t
						     meltfirstargp_,
						     const
						     melt_argdescr_cell_t
						     meltxargdescr_[],
						     union meltparam_un
						     *meltxargtab_,
						     const
						     melt_argdescr_cell_t
						     meltxresdescr_[],
						     union meltparam_un
						     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_29_warmelt_outobj_OUTPUCOD_ANYDISCR (meltclosure_ptr_t meltclosp_,
					      melt_ptr_t meltfirstargp_,
					      const melt_argdescr_cell_t
					      meltxargdescr_[],
					      union meltparam_un
					      *meltxargtab_,
					      const melt_argdescr_cell_t
					      meltxresdescr_[],
					      union meltparam_un
					      *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_30_warmelt_outobj_OUTPUCOD_NULL (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_31_warmelt_outobj_OUTPUCOD_CATCHALL_ROOT (meltclosure_ptr_t
						   meltclosp_,
						   melt_ptr_t meltfirstargp_,
						   const melt_argdescr_cell_t
						   meltxargdescr_[],
						   union meltparam_un
						   *meltxargtab_,
						   const melt_argdescr_cell_t
						   meltxresdescr_[],
						   union meltparam_un
						   *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_32_warmelt_outobj_OUTPUT_RAW_LOCATION (meltclosure_ptr_t meltclosp_,
						melt_ptr_t meltfirstargp_,
						const melt_argdescr_cell_t
						meltxargdescr_[],
						union meltparam_un
						*meltxargtab_,
						const melt_argdescr_cell_t
						meltxresdescr_[],
						union meltparam_un
						*meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_33_warmelt_outobj_LINE_AND_FILE_OF_LOCATION (meltclosure_ptr_t
						      meltclosp_,
						      melt_ptr_t
						      meltfirstargp_,
						      const
						      melt_argdescr_cell_t
						      meltxargdescr_[],
						      union meltparam_un
						      *meltxargtab_,
						      const
						      melt_argdescr_cell_t
						      meltxresdescr_[],
						      union meltparam_un
						      *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_34_warmelt_outobj_OUTPUT_LOCATION (meltclosure_ptr_t meltclosp_,
					    melt_ptr_t meltfirstargp_,
					    const melt_argdescr_cell_t
					    meltxargdescr_[],
					    union meltparam_un *meltxargtab_,
					    const melt_argdescr_cell_t
					    meltxresdescr_[],
					    union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_35_warmelt_outobj_OUTPUT_CURFRAME_DECLSTRUCT (meltclosure_ptr_t
						       meltclosp_,
						       melt_ptr_t
						       meltfirstargp_,
						       const
						       melt_argdescr_cell_t
						       meltxargdescr_[],
						       union meltparam_un
						       *meltxargtab_,
						       const
						       melt_argdescr_cell_t
						       meltxresdescr_[],
						       union meltparam_un
						       *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_36_warmelt_outobj_LAMBDA___1__ (meltclosure_ptr_t meltclosp_,
					 melt_ptr_t meltfirstargp_,
					 const melt_argdescr_cell_t
					 meltxargdescr_[],
					 union meltparam_un *meltxargtab_,
					 const melt_argdescr_cell_t
					 meltxresdescr_[],
					 union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_37_warmelt_outobj_OUTPUCOD_MARKER (meltclosure_ptr_t meltclosp_,
					    melt_ptr_t meltfirstargp_,
					    const melt_argdescr_cell_t
					    meltxargdescr_[],
					    union meltparam_un *meltxargtab_,
					    const melt_argdescr_cell_t
					    meltxresdescr_[],
					    union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_38_warmelt_outobj_LAMBDA___2__ (meltclosure_ptr_t meltclosp_,
					 melt_ptr_t meltfirstargp_,
					 const melt_argdescr_cell_t
					 meltxargdescr_[],
					 union meltparam_un *meltxargtab_,
					 const melt_argdescr_cell_t
					 meltxresdescr_[],
					 union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_39_warmelt_outobj_OUTPUT_CURFRAME_DECLSTRUCT_INIT (meltclosure_ptr_t
							    meltclosp_,
							    melt_ptr_t
							    meltfirstargp_,
							    const
							    melt_argdescr_cell_t
							    meltxargdescr_[],
							    union meltparam_un
							    *meltxargtab_,
							    const
							    melt_argdescr_cell_t
							    meltxresdescr_[],
							    union meltparam_un
							    *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_40_warmelt_outobj_OUTPUCOD_PROCROUTINE (meltclosure_ptr_t meltclosp_,
						 melt_ptr_t meltfirstargp_,
						 const melt_argdescr_cell_t
						 meltxargdescr_[],
						 union meltparam_un
						 *meltxargtab_,
						 const melt_argdescr_cell_t
						 meltxresdescr_[],
						 union meltparam_un
						 *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_41_warmelt_outobj_LAMBDA___3__ (meltclosure_ptr_t meltclosp_,
					 melt_ptr_t meltfirstargp_,
					 const melt_argdescr_cell_t
					 meltxargdescr_[],
					 union meltparam_un *meltxargtab_,
					 const melt_argdescr_cell_t
					 meltxresdescr_[],
					 union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_42_warmelt_outobj_OUTPUT_CURFRAME_CDAT_STRUCT (meltclosure_ptr_t
							meltclosp_,
							melt_ptr_t
							meltfirstargp_,
							const
							melt_argdescr_cell_t
							meltxargdescr_[],
							union meltparam_un
							*meltxargtab_,
							const
							melt_argdescr_cell_t
							meltxresdescr_[],
							union meltparam_un
							*meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_43_warmelt_outobj_OUTPUT_CURFRAME_CDAT_FILL (meltclosure_ptr_t
						      meltclosp_,
						      melt_ptr_t
						      meltfirstargp_,
						      const
						      melt_argdescr_cell_t
						      meltxargdescr_[],
						      union meltparam_un
						      *meltxargtab_,
						      const
						      melt_argdescr_cell_t
						      meltxresdescr_[],
						      union meltparam_un
						      *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_44_warmelt_outobj_OUTPUCOD_INITIALMODULEROUTINE (meltclosure_ptr_t
							  meltclosp_,
							  melt_ptr_t
							  meltfirstargp_,
							  const
							  melt_argdescr_cell_t
							  meltxargdescr_[],
							  union meltparam_un
							  *meltxargtab_,
							  const
							  melt_argdescr_cell_t
							  meltxresdescr_[],
							  union meltparam_un
							  *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_45_warmelt_outobj_LAMBDA___4__ (meltclosure_ptr_t meltclosp_,
					 melt_ptr_t meltfirstargp_,
					 const melt_argdescr_cell_t
					 meltxargdescr_[],
					 union meltparam_un *meltxargtab_,
					 const melt_argdescr_cell_t
					 meltxresdescr_[],
					 union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_46_warmelt_outobj_OUTPUCOD_INITIALEXTENSIONROUTINE (meltclosure_ptr_t
							     meltclosp_,
							     melt_ptr_t
							     meltfirstargp_,
							     const
							     melt_argdescr_cell_t
							     meltxargdescr_[],
							     union
							     meltparam_un
							     *meltxargtab_,
							     const
							     melt_argdescr_cell_t
							     meltxresdescr_[],
							     union
							     meltparam_un
							     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_47_warmelt_outobj_LAMBDA___5__ (meltclosure_ptr_t meltclosp_,
					 melt_ptr_t meltfirstargp_,
					 const melt_argdescr_cell_t
					 meltxargdescr_[],
					 union meltparam_un *meltxargtab_,
					 const melt_argdescr_cell_t
					 meltxresdescr_[],
					 union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_48_warmelt_outobj_OUTPUCOD_GETARG (meltclosure_ptr_t meltclosp_,
					    melt_ptr_t meltfirstargp_,
					    const melt_argdescr_cell_t
					    meltxargdescr_[],
					    union meltparam_un *meltxargtab_,
					    const melt_argdescr_cell_t
					    meltxresdescr_[],
					    union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_49_warmelt_outobj_OUTPUCOD_GETARGREST (meltclosure_ptr_t meltclosp_,
						melt_ptr_t meltfirstargp_,
						const melt_argdescr_cell_t
						meltxargdescr_[],
						union meltparam_un
						*meltxargtab_,
						const melt_argdescr_cell_t
						meltxresdescr_[],
						union meltparam_un
						*meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_50_warmelt_outobj_OUTPUCOD_OBJLOCV (meltclosure_ptr_t meltclosp_,
					     melt_ptr_t meltfirstargp_,
					     const melt_argdescr_cell_t
					     meltxargdescr_[],
					     union meltparam_un *meltxargtab_,
					     const melt_argdescr_cell_t
					     meltxresdescr_[],
					     union meltparam_un
					     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_51_warmelt_outobj_OUTPUCOD_OBJCLOCCV (meltclosure_ptr_t meltclosp_,
					       melt_ptr_t meltfirstargp_,
					       const melt_argdescr_cell_t
					       meltxargdescr_[],
					       union meltparam_un
					       *meltxargtab_,
					       const melt_argdescr_cell_t
					       meltxresdescr_[],
					       union meltparam_un
					       *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_52_warmelt_outobj_OUTPUCOD_OBJCONSTV (meltclosure_ptr_t meltclosp_,
					       melt_ptr_t meltfirstargp_,
					       const melt_argdescr_cell_t
					       meltxargdescr_[],
					       union meltparam_un
					       *meltxargtab_,
					       const melt_argdescr_cell_t
					       meltxresdescr_[],
					       union meltparam_un
					       *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_53_warmelt_outobj_OUTPUT_CODE_INSTRUCTIONS_LIST (meltclosure_ptr_t
							  meltclosp_,
							  melt_ptr_t
							  meltfirstargp_,
							  const
							  melt_argdescr_cell_t
							  meltxargdescr_[],
							  union meltparam_un
							  *meltxargtab_,
							  const
							  melt_argdescr_cell_t
							  meltxresdescr_[],
							  union meltparam_un
							  *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_54_warmelt_outobj_OUTPUCOD_OBJCHECKSIGNAL (meltclosure_ptr_t
						    meltclosp_,
						    melt_ptr_t meltfirstargp_,
						    const melt_argdescr_cell_t
						    meltxargdescr_[],
						    union meltparam_un
						    *meltxargtab_,
						    const melt_argdescr_cell_t
						    meltxresdescr_[],
						    union meltparam_un
						    *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_55_warmelt_outobj_OUTPUCOD_OBJANYBLOCK (meltclosure_ptr_t meltclosp_,
						 melt_ptr_t meltfirstargp_,
						 const melt_argdescr_cell_t
						 meltxargdescr_[],
						 union meltparam_un
						 *meltxargtab_,
						 const melt_argdescr_cell_t
						 meltxresdescr_[],
						 union meltparam_un
						 *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_56_warmelt_outobj_OUTPUCOD_OBJMULTIALLOCBLOCK (meltclosure_ptr_t
							meltclosp_,
							melt_ptr_t
							meltfirstargp_,
							const
							melt_argdescr_cell_t
							meltxargdescr_[],
							union meltparam_un
							*meltxargtab_,
							const
							melt_argdescr_cell_t
							meltxresdescr_[],
							union meltparam_un
							*meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_57_warmelt_outobj_OUTPUCOD_OBJCITERBLOCK (meltclosure_ptr_t
						   meltclosp_,
						   melt_ptr_t meltfirstargp_,
						   const melt_argdescr_cell_t
						   meltxargdescr_[],
						   union meltparam_un
						   *meltxargtab_,
						   const melt_argdescr_cell_t
						   meltxresdescr_[],
						   union meltparam_un
						   *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_58_warmelt_outobj_OUTPUCOD_OBJCOMMENTINSTR (meltclosure_ptr_t
						     meltclosp_,
						     melt_ptr_t
						     meltfirstargp_,
						     const
						     melt_argdescr_cell_t
						     meltxargdescr_[],
						     union meltparam_un
						     *meltxargtab_,
						     const
						     melt_argdescr_cell_t
						     meltxresdescr_[],
						     union meltparam_un
						     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_59_warmelt_outobj_OUTPUCOD_OBJCOMMENTEDBLOCK (meltclosure_ptr_t
						       meltclosp_,
						       melt_ptr_t
						       meltfirstargp_,
						       const
						       melt_argdescr_cell_t
						       meltxargdescr_[],
						       union meltparam_un
						       *meltxargtab_,
						       const
						       melt_argdescr_cell_t
						       meltxresdescr_[],
						       union meltparam_un
						       *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_60_warmelt_outobj_OUTPUCOD_OBJLABELINSTR (meltclosure_ptr_t
						   meltclosp_,
						   melt_ptr_t meltfirstargp_,
						   const melt_argdescr_cell_t
						   meltxargdescr_[],
						   union meltparam_un
						   *meltxargtab_,
						   const melt_argdescr_cell_t
						   meltxresdescr_[],
						   union meltparam_un
						   *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_61_warmelt_outobj_OUTPUCOD_OBJGOTOINSTR (meltclosure_ptr_t
						  meltclosp_,
						  melt_ptr_t meltfirstargp_,
						  const melt_argdescr_cell_t
						  meltxargdescr_[],
						  union meltparam_un
						  *meltxargtab_,
						  const melt_argdescr_cell_t
						  meltxresdescr_[],
						  union meltparam_un
						  *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_62_warmelt_outobj_ADD2SBUF_CLONSYM (meltclosure_ptr_t meltclosp_,
					     melt_ptr_t meltfirstargp_,
					     const melt_argdescr_cell_t
					     meltxargdescr_[],
					     union meltparam_un *meltxargtab_,
					     const melt_argdescr_cell_t
					     meltxresdescr_[],
					     union meltparam_un
					     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_63_warmelt_outobj_OUTPUCOD_OBJLOOP (meltclosure_ptr_t meltclosp_,
					     melt_ptr_t meltfirstargp_,
					     const melt_argdescr_cell_t
					     meltxargdescr_[],
					     union meltparam_un *meltxargtab_,
					     const melt_argdescr_cell_t
					     meltxresdescr_[],
					     union meltparam_un
					     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_64_warmelt_outobj_LAMBDA___6__ (meltclosure_ptr_t meltclosp_,
					 melt_ptr_t meltfirstargp_,
					 const melt_argdescr_cell_t
					 meltxargdescr_[],
					 union meltparam_un *meltxargtab_,
					 const melt_argdescr_cell_t
					 meltxresdescr_[],
					 union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_65_warmelt_outobj_LAMBDA___7__ (meltclosure_ptr_t meltclosp_,
					 melt_ptr_t meltfirstargp_,
					 const melt_argdescr_cell_t
					 meltxargdescr_[],
					 union meltparam_un *meltxargtab_,
					 const melt_argdescr_cell_t
					 meltxresdescr_[],
					 union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_66_warmelt_outobj_OUTPUCOD_OBJEXIT (meltclosure_ptr_t meltclosp_,
					     melt_ptr_t meltfirstargp_,
					     const melt_argdescr_cell_t
					     meltxargdescr_[],
					     union meltparam_un *meltxargtab_,
					     const melt_argdescr_cell_t
					     meltxresdescr_[],
					     union meltparam_un
					     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_67_warmelt_outobj_OUTPUCOD_OBJAGAIN (meltclosure_ptr_t meltclosp_,
					      melt_ptr_t meltfirstargp_,
					      const melt_argdescr_cell_t
					      meltxargdescr_[],
					      union meltparam_un
					      *meltxargtab_,
					      const melt_argdescr_cell_t
					      meltxresdescr_[],
					      union meltparam_un
					      *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_68_warmelt_outobj_OUTPUCOD_OBJCOMPUTE (meltclosure_ptr_t meltclosp_,
						melt_ptr_t meltfirstargp_,
						const melt_argdescr_cell_t
						meltxargdescr_[],
						union meltparam_un
						*meltxargtab_,
						const melt_argdescr_cell_t
						meltxresdescr_[],
						union meltparam_un
						*meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_69_warmelt_outobj_LAMBDA___8__ (meltclosure_ptr_t meltclosp_,
					 melt_ptr_t meltfirstargp_,
					 const melt_argdescr_cell_t
					 meltxargdescr_[],
					 union meltparam_un *meltxargtab_,
					 const melt_argdescr_cell_t
					 meltxresdescr_[],
					 union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_70_warmelt_outobj_LAMBDA___9__ (meltclosure_ptr_t meltclosp_,
					 melt_ptr_t meltfirstargp_,
					 const melt_argdescr_cell_t
					 meltxargdescr_[],
					 union meltparam_un *meltxargtab_,
					 const melt_argdescr_cell_t
					 meltxresdescr_[],
					 union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_71_warmelt_outobj_LAMBDA___10__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_72_warmelt_outobj_OUTPUCOD_OBJCOND (meltclosure_ptr_t meltclosp_,
					     melt_ptr_t meltfirstargp_,
					     const melt_argdescr_cell_t
					     meltxargdescr_[],
					     union meltparam_un *meltxargtab_,
					     const melt_argdescr_cell_t
					     meltxresdescr_[],
					     union meltparam_un
					     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_73_warmelt_outobj_OUTPUCOD_OBJCPPIF (meltclosure_ptr_t meltclosp_,
					      melt_ptr_t meltfirstargp_,
					      const melt_argdescr_cell_t
					      meltxargdescr_[],
					      union meltparam_un
					      *meltxargtab_,
					      const melt_argdescr_cell_t
					      meltxresdescr_[],
					      union meltparam_un
					      *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_74_warmelt_outobj_OUTPUCOD_OBJINTERNSYMBOL (meltclosure_ptr_t
						     meltclosp_,
						     melt_ptr_t
						     meltfirstargp_,
						     const
						     melt_argdescr_cell_t
						     meltxargdescr_[],
						     union meltparam_un
						     *meltxargtab_,
						     const
						     melt_argdescr_cell_t
						     meltxresdescr_[],
						     union meltparam_un
						     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_75_warmelt_outobj_OUTPUCOD_OBJINTERNKEYWORD (meltclosure_ptr_t
						      meltclosp_,
						      melt_ptr_t
						      meltfirstargp_,
						      const
						      melt_argdescr_cell_t
						      meltxargdescr_[],
						      union meltparam_un
						      *meltxargtab_,
						      const
						      melt_argdescr_cell_t
						      meltxresdescr_[],
						      union meltparam_un
						      *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_76_warmelt_outobj_OUTPUCOD_OBJGETNAMEDSYMBOL (meltclosure_ptr_t
						       meltclosp_,
						       melt_ptr_t
						       meltfirstargp_,
						       const
						       melt_argdescr_cell_t
						       meltxargdescr_[],
						       union meltparam_un
						       *meltxargtab_,
						       const
						       melt_argdescr_cell_t
						       meltxresdescr_[],
						       union meltparam_un
						       *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_77_warmelt_outobj_OUTPUCOD_OBJGETNAMEDKEYWORD (meltclosure_ptr_t
							meltclosp_,
							melt_ptr_t
							meltfirstargp_,
							const
							melt_argdescr_cell_t
							meltxargdescr_[],
							union meltparam_un
							*meltxargtab_,
							const
							melt_argdescr_cell_t
							meltxresdescr_[],
							union meltparam_un
							*meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_78_warmelt_outobj_OUTPUCOD_OBJAPPLY (meltclosure_ptr_t meltclosp_,
					      melt_ptr_t meltfirstargp_,
					      const melt_argdescr_cell_t
					      meltxargdescr_[],
					      union meltparam_un
					      *meltxargtab_,
					      const melt_argdescr_cell_t
					      meltxresdescr_[],
					      union meltparam_un
					      *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_79_warmelt_outobj_LAMBDA___11__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_80_warmelt_outobj_LAMBDA___12__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_81_warmelt_outobj_OUTPUCOD_OBJMSEND (meltclosure_ptr_t meltclosp_,
					      melt_ptr_t meltfirstargp_,
					      const melt_argdescr_cell_t
					      meltxargdescr_[],
					      union meltparam_un
					      *meltxargtab_,
					      const melt_argdescr_cell_t
					      meltxresdescr_[],
					      union meltparam_un
					      *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_82_warmelt_outobj_LAMBDA___13__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_83_warmelt_outobj_OUTPUCOD_OBJMULTIAPPLY (meltclosure_ptr_t
						   meltclosp_,
						   melt_ptr_t meltfirstargp_,
						   const melt_argdescr_cell_t
						   meltxargdescr_[],
						   union meltparam_un
						   *meltxargtab_,
						   const melt_argdescr_cell_t
						   meltxresdescr_[],
						   union meltparam_un
						   *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_84_warmelt_outobj_LAMBDA___14__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_85_warmelt_outobj_LAMBDA___15__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_86_warmelt_outobj_LAMBDA___16__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_87_warmelt_outobj_LAMBDA___17__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_88_warmelt_outobj_LAMBDA___18__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_89_warmelt_outobj_OUTPUCOD_OBJMULTIMSEND (meltclosure_ptr_t
						   meltclosp_,
						   melt_ptr_t meltfirstargp_,
						   const melt_argdescr_cell_t
						   meltxargdescr_[],
						   union meltparam_un
						   *meltxargtab_,
						   const melt_argdescr_cell_t
						   meltxresdescr_[],
						   union meltparam_un
						   *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_90_warmelt_outobj_LAMBDA___19__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_91_warmelt_outobj_LAMBDA___20__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_92_warmelt_outobj_LAMBDA___21__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_93_warmelt_outobj_LAMBDA___22__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_94_warmelt_outobj_LAMBDA___23__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_95_warmelt_outobj_OUTPUCOD_OBJCLEAR (meltclosure_ptr_t meltclosp_,
					      melt_ptr_t meltfirstargp_,
					      const melt_argdescr_cell_t
					      meltxargdescr_[],
					      union meltparam_un
					      *meltxargtab_,
					      const melt_argdescr_cell_t
					      meltxresdescr_[],
					      union meltparam_un
					      *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_96_warmelt_outobj_OUTPUCOD_OBJRAWALLOCOBJ (meltclosure_ptr_t
						    meltclosp_,
						    melt_ptr_t meltfirstargp_,
						    const melt_argdescr_cell_t
						    meltxargdescr_[],
						    union meltparam_un
						    *meltxargtab_,
						    const melt_argdescr_cell_t
						    meltxresdescr_[],
						    union meltparam_un
						    *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_97_warmelt_outobj_OUTPUCOD_OBJNEWCLOSURE (meltclosure_ptr_t
						   meltclosp_,
						   melt_ptr_t meltfirstargp_,
						   const melt_argdescr_cell_t
						   meltxargdescr_[],
						   union meltparam_un
						   *meltxargtab_,
						   const melt_argdescr_cell_t
						   meltxresdescr_[],
						   union meltparam_un
						   *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_98_warmelt_outobj_LAMBDA___24__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_99_warmelt_outobj_OUTPUCOD_OBJTOUCH (meltclosure_ptr_t meltclosp_,
					      melt_ptr_t meltfirstargp_,
					      const melt_argdescr_cell_t
					      meltxargdescr_[],
					      union meltparam_un
					      *meltxargtab_,
					      const melt_argdescr_cell_t
					      meltxresdescr_[],
					      union meltparam_un
					      *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_100_warmelt_outobj_OUTPUCOD_DBGTRACEWRITEOBJ (meltclosure_ptr_t
						       meltclosp_,
						       melt_ptr_t
						       meltfirstargp_,
						       const
						       melt_argdescr_cell_t
						       meltxargdescr_[],
						       union meltparam_un
						       *meltxargtab_,
						       const
						       melt_argdescr_cell_t
						       meltxresdescr_[],
						       union meltparam_un
						       *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_101_warmelt_outobj_OUTPUCOD_OBJPUTUPLE (meltclosure_ptr_t meltclosp_,
						 melt_ptr_t meltfirstargp_,
						 const melt_argdescr_cell_t
						 meltxargdescr_[],
						 union meltparam_un
						 *meltxargtab_,
						 const melt_argdescr_cell_t
						 meltxresdescr_[],
						 union meltparam_un
						 *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_102_warmelt_outobj_OUTPUCOD_OBJPUTPAIRHEAD (meltclosure_ptr_t
						     meltclosp_,
						     melt_ptr_t
						     meltfirstargp_,
						     const
						     melt_argdescr_cell_t
						     meltxargdescr_[],
						     union meltparam_un
						     *meltxargtab_,
						     const
						     melt_argdescr_cell_t
						     meltxresdescr_[],
						     union meltparam_un
						     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_103_warmelt_outobj_OUTPUCOD_OBJPUTPAIRTAIL (meltclosure_ptr_t
						     meltclosp_,
						     melt_ptr_t
						     meltfirstargp_,
						     const
						     melt_argdescr_cell_t
						     meltxargdescr_[],
						     union meltparam_un
						     *meltxargtab_,
						     const
						     melt_argdescr_cell_t
						     meltxresdescr_[],
						     union meltparam_un
						     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_104_warmelt_outobj_OUTPUCOD_OBJPUTLIST (meltclosure_ptr_t meltclosp_,
						 melt_ptr_t meltfirstargp_,
						 const melt_argdescr_cell_t
						 meltxargdescr_[],
						 union meltparam_un
						 *meltxargtab_,
						 const melt_argdescr_cell_t
						 meltxresdescr_[],
						 union meltparam_un
						 *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_105_warmelt_outobj_OUTPUCOD_OBJGETSLOT (meltclosure_ptr_t meltclosp_,
						 melt_ptr_t meltfirstargp_,
						 const melt_argdescr_cell_t
						 meltxargdescr_[],
						 union meltparam_un
						 *meltxargtab_,
						 const melt_argdescr_cell_t
						 meltxresdescr_[],
						 union meltparam_un
						 *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_106_warmelt_outobj_LAMBDA___25__ (meltclosure_ptr_t meltclosp_,
					   melt_ptr_t meltfirstargp_,
					   const melt_argdescr_cell_t
					   meltxargdescr_[],
					   union meltparam_un *meltxargtab_,
					   const melt_argdescr_cell_t
					   meltxresdescr_[],
					   union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_107_warmelt_outobj_OUTPUCOD_OBJPUTSLOT (meltclosure_ptr_t meltclosp_,
						 melt_ptr_t meltfirstargp_,
						 const melt_argdescr_cell_t
						 meltxargdescr_[],
						 union meltparam_un
						 *meltxargtab_,
						 const melt_argdescr_cell_t
						 meltxresdescr_[],
						 union meltparam_un
						 *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_108_warmelt_outobj_OUTPUCOD_OBJPUTCLOSUROUT (meltclosure_ptr_t
						      meltclosp_,
						      melt_ptr_t
						      meltfirstargp_,
						      const
						      melt_argdescr_cell_t
						      meltxargdescr_[],
						      union meltparam_un
						      *meltxargtab_,
						      const
						      melt_argdescr_cell_t
						      meltxresdescr_[],
						      union meltparam_un
						      *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_109_warmelt_outobj_OUTPUCOD_OBJPUTCLOSEDV (meltclosure_ptr_t
						    meltclosp_,
						    melt_ptr_t meltfirstargp_,
						    const melt_argdescr_cell_t
						    meltxargdescr_[],
						    union meltparam_un
						    *meltxargtab_,
						    const melt_argdescr_cell_t
						    meltxresdescr_[],
						    union meltparam_un
						    *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_110_warmelt_outobj_OUTPUCOD_OBJPUTCLOSEDNOTNULLV (meltclosure_ptr_t
							   meltclosp_,
							   melt_ptr_t
							   meltfirstargp_,
							   const
							   melt_argdescr_cell_t
							   meltxargdescr_[],
							   union meltparam_un
							   *meltxargtab_,
							   const
							   melt_argdescr_cell_t
							   meltxresdescr_[],
							   union meltparam_un
							   *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_111_warmelt_outobj_OUTPUCOD_OBJPUTROUTCONST (meltclosure_ptr_t
						      meltclosp_,
						      melt_ptr_t
						      meltfirstargp_,
						      const
						      melt_argdescr_cell_t
						      meltxargdescr_[],
						      union meltparam_un
						      *meltxargtab_,
						      const
						      melt_argdescr_cell_t
						      meltxresdescr_[],
						      union meltparam_un
						      *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_112_warmelt_outobj_OUTPUCOD_OBJPUTROUTCONSTNOTNULL (meltclosure_ptr_t
							     meltclosp_,
							     melt_ptr_t
							     meltfirstargp_,
							     const
							     melt_argdescr_cell_t
							     meltxargdescr_[],
							     union
							     meltparam_un
							     *meltxargtab_,
							     const
							     melt_argdescr_cell_t
							     meltxresdescr_[],
							     union
							     meltparam_un
							     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_113_warmelt_outobj_OUTPUCOD_OBJPUTXTRARESULT (meltclosure_ptr_t
						       meltclosp_,
						       melt_ptr_t
						       meltfirstargp_,
						       const
						       melt_argdescr_cell_t
						       meltxargdescr_[],
						       union meltparam_un
						       *meltxargtab_,
						       const
						       melt_argdescr_cell_t
						       meltxresdescr_[],
						       union meltparam_un
						       *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_114_warmelt_outobj_OUTPUCOD_OBJEXPV (meltclosure_ptr_t meltclosp_,
					      melt_ptr_t meltfirstargp_,
					      const melt_argdescr_cell_t
					      meltxargdescr_[],
					      union meltparam_un
					      *meltxargtab_,
					      const melt_argdescr_cell_t
					      meltxresdescr_[],
					      union meltparam_un
					      *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_115_warmelt_outobj_OUTPUCOD_OBJLOCATEDEXPV (meltclosure_ptr_t
						     meltclosp_,
						     melt_ptr_t
						     meltfirstargp_,
						     const
						     melt_argdescr_cell_t
						     meltxargdescr_[],
						     union meltparam_un
						     *meltxargtab_,
						     const
						     melt_argdescr_cell_t
						     meltxresdescr_[],
						     union meltparam_un
						     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_116_warmelt_outobj_OUTPUCOD_VERBATIMSTRING (meltclosure_ptr_t
						     meltclosp_,
						     melt_ptr_t
						     meltfirstargp_,
						     const
						     melt_argdescr_cell_t
						     meltxargdescr_[],
						     union meltparam_un
						     *meltxargtab_,
						     const
						     melt_argdescr_cell_t
						     meltxresdescr_[],
						     union meltparam_un
						     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_117_warmelt_outobj_OUTPUCOD_STRING (meltclosure_ptr_t meltclosp_,
					     melt_ptr_t meltfirstargp_,
					     const melt_argdescr_cell_t
					     meltxargdescr_[],
					     union meltparam_un *meltxargtab_,
					     const melt_argdescr_cell_t
					     meltxresdescr_[],
					     union meltparam_un
					     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_118_warmelt_outobj_OUTPUCOD_INTEGER (meltclosure_ptr_t meltclosp_,
					      melt_ptr_t meltfirstargp_,
					      const melt_argdescr_cell_t
					      meltxargdescr_[],
					      union meltparam_un
					      *meltxargtab_,
					      const melt_argdescr_cell_t
					      meltxresdescr_[],
					      union meltparam_un
					      *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_119_warmelt_outobj_OUTPUCOD_FINALRETURN (meltclosure_ptr_t
						  meltclosp_,
						  melt_ptr_t meltfirstargp_,
						  const melt_argdescr_cell_t
						  meltxargdescr_[],
						  union meltparam_un
						  *meltxargtab_,
						  const melt_argdescr_cell_t
						  meltxresdescr_[],
						  union meltparam_un
						  *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_120_warmelt_outobj_SORTED_NAMED_DICT_TUPLE (meltclosure_ptr_t
						     meltclosp_,
						     melt_ptr_t
						     meltfirstargp_,
						     const
						     melt_argdescr_cell_t
						     meltxargdescr_[],
						     union meltparam_un
						     *meltxargtab_,
						     const
						     melt_argdescr_cell_t
						     meltxresdescr_[],
						     union meltparam_un
						     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_121_warmelt_outobj_OUTPUT_EXPORTED_OFFSETS (meltclosure_ptr_t
						     meltclosp_,
						     melt_ptr_t
						     meltfirstargp_,
						     const
						     melt_argdescr_cell_t
						     meltxargdescr_[],
						     union meltparam_un
						     *meltxargtab_,
						     const
						     melt_argdescr_cell_t
						     meltxresdescr_[],
						     union meltparam_un
						     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_122_warmelt_outobj_NTH_SECUNDARY_FILE (meltclosure_ptr_t meltclosp_,
						melt_ptr_t meltfirstargp_,
						const melt_argdescr_cell_t
						meltxargdescr_[],
						union meltparam_un
						*meltxargtab_,
						const melt_argdescr_cell_t
						meltxresdescr_[],
						union meltparam_un
						*meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_123_warmelt_outobj_OUTPUT_MELT_DESCRIPTOR (meltclosure_ptr_t
						    meltclosp_,
						    melt_ptr_t meltfirstargp_,
						    const melt_argdescr_cell_t
						    meltxargdescr_[],
						    union meltparam_un
						    *meltxargtab_,
						    const melt_argdescr_cell_t
						    meltxresdescr_[],
						    union meltparam_un
						    *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_124_warmelt_outobj_SYNTESTGEN_ANY (meltclosure_ptr_t meltclosp_,
					    melt_ptr_t meltfirstargp_,
					    const melt_argdescr_cell_t
					    meltxargdescr_[],
					    union meltparam_un *meltxargtab_,
					    const melt_argdescr_cell_t
					    meltxresdescr_[],
					    union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_125_warmelt_outobj_SUBSTITUTE_FORMALS_FOR_SYNTEST (meltclosure_ptr_t
							    meltclosp_,
							    melt_ptr_t
							    meltfirstargp_,
							    const
							    melt_argdescr_cell_t
							    meltxargdescr_[],
							    union meltparam_un
							    *meltxargtab_,
							    const
							    melt_argdescr_cell_t
							    meltxresdescr_[],
							    union meltparam_un
							    *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_126_warmelt_outobj_EXPAND_TUPLE_FOR_SYNTEST (meltclosure_ptr_t
						      meltclosp_,
						      melt_ptr_t
						      meltfirstargp_,
						      const
						      melt_argdescr_cell_t
						      meltxargdescr_[],
						      union meltparam_un
						      *meltxargtab_,
						      const
						      melt_argdescr_cell_t
						      meltxresdescr_[],
						      union meltparam_un
						      *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_127_warmelt_outobj_SYNTESTGEN_PRIMITIVE (meltclosure_ptr_t
						  meltclosp_,
						  melt_ptr_t meltfirstargp_,
						  const melt_argdescr_cell_t
						  meltxargdescr_[],
						  union meltparam_un
						  *meltxargtab_,
						  const melt_argdescr_cell_t
						  meltxresdescr_[],
						  union meltparam_un
						  *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_128_warmelt_outobj_SYNTESTGEN_CITERATOR (meltclosure_ptr_t
						  meltclosp_,
						  melt_ptr_t meltfirstargp_,
						  const melt_argdescr_cell_t
						  meltxargdescr_[],
						  union meltparam_un
						  *meltxargtab_,
						  const melt_argdescr_cell_t
						  meltxresdescr_[],
						  union meltparam_un
						  *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_129_warmelt_outobj_SYNTESTGEN_CMATCHER (meltclosure_ptr_t meltclosp_,
						 melt_ptr_t meltfirstargp_,
						 const melt_argdescr_cell_t
						 meltxargdescr_[],
						 union meltparam_un
						 *meltxargtab_,
						 const melt_argdescr_cell_t
						 meltxresdescr_[],
						 union meltparam_un
						 *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_130_warmelt_outobj_EMIT_SYNTAX_TESTING_ROUTINE (meltclosure_ptr_t
							 meltclosp_,
							 melt_ptr_t
							 meltfirstargp_,
							 const
							 melt_argdescr_cell_t
							 meltxargdescr_[],
							 union meltparam_un
							 *meltxargtab_,
							 const
							 melt_argdescr_cell_t
							 meltxresdescr_[],
							 union meltparam_un
							 *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_131_warmelt_outobj_NORMADECLB_MACROEXPANDED_LIST (meltclosure_ptr_t
							   meltclosp_,
							   melt_ptr_t
							   meltfirstargp_,
							   const
							   melt_argdescr_cell_t
							   meltxargdescr_[],
							   union meltparam_un
							   *meltxargtab_,
							   const
							   melt_argdescr_cell_t
							   meltxresdescr_[],
							   union meltparam_un
							   *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_132_warmelt_outobj_LAMBDA___26__ (meltclosure_ptr_t meltclosp_,
					   melt_ptr_t meltfirstargp_,
					   const melt_argdescr_cell_t
					   meltxargdescr_[],
					   union meltparam_un *meltxargtab_,
					   const melt_argdescr_cell_t
					   meltxresdescr_[],
					   union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_133_warmelt_outobj_TRANSLATE_MACROEXPANDED_LIST (meltclosure_ptr_t
							  meltclosp_,
							  melt_ptr_t
							  meltfirstargp_,
							  const
							  melt_argdescr_cell_t
							  meltxargdescr_[],
							  union meltparam_un
							  *meltxargtab_,
							  const
							  melt_argdescr_cell_t
							  meltxresdescr_[],
							  union meltparam_un
							  *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_134_warmelt_outobj_LAMBDA___27__ (meltclosure_ptr_t meltclosp_,
					   melt_ptr_t meltfirstargp_,
					   const melt_argdescr_cell_t
					   meltxargdescr_[],
					   union meltparam_un *meltxargtab_,
					   const melt_argdescr_cell_t
					   meltxresdescr_[],
					   union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_135_warmelt_outobj_FATAL_COMPILE_ERROR (meltclosure_ptr_t meltclosp_,
						 melt_ptr_t meltfirstargp_,
						 const melt_argdescr_cell_t
						 meltxargdescr_[],
						 union meltparam_un
						 *meltxargtab_,
						 const melt_argdescr_cell_t
						 meltxresdescr_[],
						 union meltparam_un
						 *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_136_warmelt_outobj_COMPILE_LIST_SEXPR (meltclosure_ptr_t meltclosp_,
						melt_ptr_t meltfirstargp_,
						const melt_argdescr_cell_t
						meltxargdescr_[],
						union meltparam_un
						*meltxargtab_,
						const melt_argdescr_cell_t
						meltxresdescr_[],
						union meltparam_un
						*meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_137_warmelt_outobj_LAMBDA___28__ (meltclosure_ptr_t meltclosp_,
					   melt_ptr_t meltfirstargp_,
					   const melt_argdescr_cell_t
					   meltxargdescr_[],
					   union meltparam_un *meltxargtab_,
					   const melt_argdescr_cell_t
					   meltxresdescr_[],
					   union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_138_warmelt_outobj_AUTOBOX_NORMAL_RETURN (meltclosure_ptr_t
						   meltclosp_,
						   melt_ptr_t meltfirstargp_,
						   const melt_argdescr_cell_t
						   meltxargdescr_[],
						   union meltparam_un
						   *meltxargtab_,
						   const melt_argdescr_cell_t
						   meltxresdescr_[],
						   union meltparam_un
						   *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_139_warmelt_outobj_TRANSLATE_RUN_MELT_EXPRESSIONS (meltclosure_ptr_t
							    meltclosp_,
							    melt_ptr_t
							    meltfirstargp_,
							    const
							    melt_argdescr_cell_t
							    meltxargdescr_[],
							    union meltparam_un
							    *meltxargtab_,
							    const
							    melt_argdescr_cell_t
							    meltxresdescr_[],
							    union meltparam_un
							    *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_140_warmelt_outobj_LAMBDA___29__ (meltclosure_ptr_t meltclosp_,
					   melt_ptr_t meltfirstargp_,
					   const melt_argdescr_cell_t
					   meltxargdescr_[],
					   union meltparam_un *meltxargtab_,
					   const melt_argdescr_cell_t
					   meltxresdescr_[],
					   union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_141_warmelt_outobj_LAMBDA___30__ (meltclosure_ptr_t meltclosp_,
					   melt_ptr_t meltfirstargp_,
					   const melt_argdescr_cell_t
					   meltxargdescr_[],
					   union meltparam_un *meltxargtab_,
					   const melt_argdescr_cell_t
					   meltxresdescr_[],
					   union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_142_warmelt_outobj_LAMBDA___31__ (meltclosure_ptr_t meltclosp_,
					   melt_ptr_t meltfirstargp_,
					   const melt_argdescr_cell_t
					   meltxargdescr_[],
					   union meltparam_un *meltxargtab_,
					   const melt_argdescr_cell_t
					   meltxresdescr_[],
					   union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_143_warmelt_outobj_READ_MELT_EXPRESSIONS (meltclosure_ptr_t
						   meltclosp_,
						   melt_ptr_t meltfirstargp_,
						   const melt_argdescr_cell_t
						   meltxargdescr_[],
						   union meltparam_un
						   *meltxargtab_,
						   const melt_argdescr_cell_t
						   meltxresdescr_[],
						   union meltparam_un
						   *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_144_warmelt_outobj_TRANSLATE_TO_C_MODULE_MELT_SOURCES
(meltclosure_ptr_t meltclosp_, melt_ptr_t meltfirstargp_, const melt_argdescr_cell_t meltxargdescr_[],
union meltparam_un *meltxargtab_, const melt_argdescr_cell_t meltxresdescr_[],
union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_145_warmelt_outobj_GENERATE_GPLV3PLUS_COPYRIGHT_NOTICE_C_COMMENT
(meltclosure_ptr_t meltclosp_, melt_ptr_t meltfirstargp_, const melt_argdescr_cell_t meltxargdescr_[],
union meltparam_un *meltxargtab_, const melt_argdescr_cell_t meltxresdescr_[],
union meltparam_un *meltxrestab_);



MELT_EXTERN void *melt_start_this_module (void *);


/* define different names when debugging or not */
#if MELT_HAVE_DEBUG
MELT_EXTERN const char meltmodule_warmelt_outobj__melt_have_debug_enabled[];
#define melt_have_debug_string meltmodule_warmelt_outobj__melt_have_debug_enabled
#else /*!MELT_HAVE_DEBUG */
MELT_EXTERN const char meltmodule_warmelt_outobj__melt_have_debug_disabled[];
#define melt_have_debug_string meltmodule_warmelt_outobj__melt_have_debug_disabled
#endif /*!MELT_HAVE_DEBUG */


struct frame_melt_start_this_module_st;
void MELT_MODULE_VISIBILITY
meltmod__warmelt_outobj__initialmeltchunk_0 (struct
					     frame_melt_start_this_module_st
					     *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_outobj__initialmeltchunk_1 (struct
					     frame_melt_start_this_module_st
					     *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_outobj__initialmeltchunk_2 (struct
					     frame_melt_start_this_module_st
					     *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_outobj__initialmeltchunk_3 (struct
					     frame_melt_start_this_module_st
					     *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_outobj__initialmeltchunk_4 (struct
					     frame_melt_start_this_module_st
					     *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_outobj__initialmeltchunk_5 (struct
					     frame_melt_start_this_module_st
					     *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_outobj__initialmeltchunk_6 (struct
					     frame_melt_start_this_module_st
					     *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_outobj__initialmeltchunk_7 (struct
					     frame_melt_start_this_module_st
					     *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_outobj__initialmeltchunk_8 (struct
					     frame_melt_start_this_module_st
					     *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_outobj__initialmeltchunk_9 (struct
					     frame_melt_start_this_module_st
					     *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_outobj__initialmeltchunk_10 (struct
					      frame_melt_start_this_module_st
					      *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_outobj__initialmeltchunk_11 (struct
					      frame_melt_start_this_module_st
					      *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_outobj__initialmeltchunk_12 (struct
					      frame_melt_start_this_module_st
					      *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_outobj__initialmeltchunk_13 (struct
					      frame_melt_start_this_module_st
					      *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_outobj__initialmeltchunk_14 (struct
					      frame_melt_start_this_module_st
					      *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_outobj__initialmeltchunk_15 (struct
					      frame_melt_start_this_module_st
					      *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_outobj__initialmeltchunk_16 (struct
					      frame_melt_start_this_module_st
					      *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_outobj__initialmeltchunk_17 (struct
					      frame_melt_start_this_module_st
					      *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_outobj__forward_or_mark_module_start_frame (struct
							     melt_callframe_st
							     *fp,
							     int marking);
#define meltmarking_melt_start_this_module  meltmod__warmelt_outobj__forward_or_mark_module_start_frame



/**** warmelt-outobj+05.c implementations ****/




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_130_warmelt_outobj_EMIT_SYNTAX_TESTING_ROUTINE (meltclosure_ptr_t
							 meltclosp_,
							 melt_ptr_t
							 meltfirstargp_,
							 const
							 melt_argdescr_cell_t
							 meltxargdescr_[],
							 union meltparam_un
							 *meltxargtab_,
							 const
							 melt_argdescr_cell_t
							 meltxresdescr_[],
							 union meltparam_un
							 *meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_130_warmelt_outobj_EMIT_SYNTAX_TESTING_ROUTINE_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_130_warmelt_outobj_EMIT_SYNTAX_TESTING_ROUTINE_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 50
    melt_ptr_t mcfr_varptr[50];
#define MELTFRAM_NBVARNUM 18
    long mcfr_varnum[18];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_130_warmelt_outobj_EMIT_SYNTAX_TESTING_ROUTINE is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct
	 frame_meltrout_130_warmelt_outobj_EMIT_SYNTAX_TESTING_ROUTINE_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 50; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_130_warmelt_outobj_EMIT_SYNTAX_TESTING_ROUTINE nbval 50*/
  meltfram__.mcfr_nbvar = 50 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("EMIT_SYNTAX_TESTING_ROUTINE", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-outobj.melt:5327:/ getarg");
 /*_.GENDEVTUP__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.MODCTX__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.MODCTX__V3*/ meltfptr[2])) !=
	      NULL);


  /*getarg#2 */
  /*^getarg */
  if (meltxargdescr_[1] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.SBUF__V4*/ meltfptr[3] =
    (meltxargtab_[1].meltbp_aptr) ? (*(meltxargtab_[1].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.SBUF__V4*/ meltfptr[3])) !=
	      NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:5328:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L1*/ meltfnum[0] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-outobj.melt:5328:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L1*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-outobj.melt:5328:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[7];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-outobj.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 5328;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring =
		"emit_syntax_testing_routine gendevtup=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.GENDEVTUP__V2*/ meltfptr[1];
	      /*^apply.arg */
	      argtab[5].meltbp_cstring = " sbuf=";
	      /*^apply.arg */
	      argtab[6].meltbp_aptr =
		(melt_ptr_t *) & /*_.SBUF__V4*/ meltfptr[3];
	      /*_.MELT_DEBUG_FUN__V7*/ meltfptr[6] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V6*/ meltfptr[5] = /*_.MELT_DEBUG_FUN__V7*/ meltfptr[6];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-outobj.melt:5328:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V7*/ meltfptr[6] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V6*/ meltfptr[5] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-outobj.melt:5328:/ quasiblock");


      /*_.PROGN___V8*/ meltfptr[6] = /*_.IF___V6*/ meltfptr[5];;
      /*^compute */
      /*_.IFCPP___V5*/ meltfptr[4] = /*_.PROGN___V8*/ meltfptr[6];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:5328:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L1*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IF___V6*/ meltfptr[5] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V8*/ meltfptr[6] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V5*/ meltfptr[4] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:5330:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_MULTIPLE__L3*/ meltfnum[1] =
	(melt_magic_discr ((melt_ptr_t) ( /*_.GENDEVTUP__V2*/ meltfptr[1])) ==
	 MELTOBMAG_MULTIPLE);;
      MELT_LOCATION ("warmelt-outobj.melt:5330:/ cond");
      /*cond */ if ( /*_#IS_MULTIPLE__L3*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V10*/ meltfptr[6] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-outobj.melt:5330:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check gendevtup"),
				  ("warmelt-outobj.melt")
				  ? ("warmelt-outobj.melt") : __FILE__,
				  (5330) ? (5330) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V10*/ meltfptr[6] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V9*/ meltfptr[5] = /*_.IFELSE___V10*/ meltfptr[6];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:5330:/ clear");
	     /*clear *//*_#IS_MULTIPLE__L3*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V10*/ meltfptr[6] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V9*/ meltfptr[5] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:5331:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L4*/ meltfnum[0] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.MODCTX__V3*/ meltfptr[2]),
			     (melt_ptr_t) (( /*!CLASS_MODULE_CONTEXT */
					    meltfrout->tabval[1])));;
      MELT_LOCATION ("warmelt-outobj.melt:5331:/ cond");
      /*cond */ if ( /*_#IS_A__L4*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V12*/ meltfptr[11] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-outobj.melt:5331:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check modctx"),
				  ("warmelt-outobj.melt")
				  ? ("warmelt-outobj.melt") : __FILE__,
				  (5331) ? (5331) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V12*/ meltfptr[11] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V11*/ meltfptr[6] = /*_.IFELSE___V12*/ meltfptr[11];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:5331:/ clear");
	     /*clear *//*_#IS_A__L4*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V12*/ meltfptr[11] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V11*/ meltfptr[6] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:5332:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_STRBUF__L5*/ meltfnum[1] =
	(melt_magic_discr ((melt_ptr_t) ( /*_.SBUF__V4*/ meltfptr[3])) ==
	 MELTOBMAG_STRBUF);;
      MELT_LOCATION ("warmelt-outobj.melt:5332:/ cond");
      /*cond */ if ( /*_#IS_STRBUF__L5*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V14*/ meltfptr[13] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-outobj.melt:5332:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check sbuf"),
				  ("warmelt-outobj.melt")
				  ? ("warmelt-outobj.melt") : __FILE__,
				  (5332) ? (5332) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V14*/ meltfptr[13] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V13*/ meltfptr[11] = /*_.IFELSE___V14*/ meltfptr[13];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:5332:/ clear");
	     /*clear *//*_#IS_STRBUF__L5*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V14*/ meltfptr[13] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V13*/ meltfptr[11] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-outobj.melt:5333:/ quasiblock");


 /*_#NBGENDEV__L6*/ meltfnum[0] =
      (melt_multiple_length
       ((melt_ptr_t) ( /*_.GENDEVTUP__V2*/ meltfptr[1])));;
    MELT_LOCATION ("warmelt-outobj.melt:5335:/ cond");
    /*cond */ if (
		   /*ifisa */
		   melt_is_instance_of ((melt_ptr_t)
					( /*_.MODCTX__V3*/ meltfptr[2]),
					(melt_ptr_t) (( /*!CLASS_MODULE_CONTEXT */ meltfrout->tabval[1])))
      )				/*then */
      {
	/*^cond.then */
	/*^getslot */
	{
	  melt_ptr_t slot = NULL, obj = NULL;
	  obj = (melt_ptr_t) ( /*_.MODCTX__V3*/ meltfptr[2]) /*=obj*/ ;
	  melt_object_get_field (slot, obj, 0, "MOCX_MODULENAME");
   /*_.MODNAM__V15*/ meltfptr[13] = slot;
	};
	;
      }
    else
      {				/*^cond.else */

  /*_.MODNAM__V15*/ meltfptr[13] = NULL;;
      }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:5337:/ quasiblock");


 /*_.NABUF__V17*/ meltfptr[16] =
      (melt_ptr_t)
      meltgc_new_strbuf ((meltobject_ptr_t)
			 (( /*!DISCR_STRBUF */ meltfrout->tabval[2])),
			 (const char *) 0);;
    MELT_LOCATION ("warmelt-outobj.melt:5338:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_cstring = "melt_syntax_tester_";
      /*_.ADD2OUT__V18*/ meltfptr[17] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!ADD2OUT */ meltfrout->tabval[3])),
		    (melt_ptr_t) ( /*_.NABUF__V17*/ meltfptr[16]),
		    (MELTBPARSTR_CSTRING ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:5339:/ locexp");
      meltgc_add_strbuf_cident ((melt_ptr_t) ( /*_.NABUF__V17*/ meltfptr[16]),
				melt_string_str ((melt_ptr_t)
						 ( /*_.MODNAM__V15*/
						  meltfptr[13])));
    }
    ;
 /*_.STRBUF2STRING__V19*/ meltfptr[18] =
      (meltgc_new_stringdup
       ((meltobject_ptr_t)
	(( /*!DISCR_VERBATIM_STRING */ meltfrout->tabval[4])),
	melt_strbuf_str ((melt_ptr_t) ( /*_.NABUF__V17*/ meltfptr[16]))));;
    /*^compute */
    /*_.LET___V16*/ meltfptr[15] = /*_.STRBUF2STRING__V19*/ meltfptr[18];;

    MELT_LOCATION ("warmelt-outobj.melt:5337:/ clear");
	   /*clear *//*_.NABUF__V17*/ meltfptr[16] = 0;
    /*^clear */
	   /*clear *//*_.ADD2OUT__V18*/ meltfptr[17] = 0;
    /*^clear */
	   /*clear *//*_.STRBUF2STRING__V19*/ meltfptr[18] = 0;
    /*_.SYNTESTNAME__V20*/ meltfptr[16] = /*_.LET___V16*/ meltfptr[15];;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:5342:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L7*/ meltfnum[1] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-outobj.melt:5342:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L7*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L8*/ meltfnum[7] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-outobj.melt:5342:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L8*/ meltfnum[7];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-outobj.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 5342;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring =
		"emit_syntax_testing_routine syntestname=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.SYNTESTNAME__V20*/ meltfptr[16];
	      /*_.MELT_DEBUG_FUN__V23*/ meltfptr[22] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V22*/ meltfptr[18] =
	      /*_.MELT_DEBUG_FUN__V23*/ meltfptr[22];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-outobj.melt:5342:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L8*/ meltfnum[7] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V23*/ meltfptr[22] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V22*/ meltfptr[18] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-outobj.melt:5342:/ quasiblock");


      /*_.PROGN___V24*/ meltfptr[22] = /*_.IF___V22*/ meltfptr[18];;
      /*^compute */
      /*_.IFCPP___V21*/ meltfptr[17] = /*_.PROGN___V24*/ meltfptr[22];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:5342:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L7*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IF___V22*/ meltfptr[18] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V24*/ meltfptr[22] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V21*/ meltfptr[17] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:5343:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_STRING__L9*/ meltfnum[7] =
	(melt_magic_discr
	 ((melt_ptr_t) ( /*_.SYNTESTNAME__V20*/ meltfptr[16])) ==
	 MELTOBMAG_STRING);;
      MELT_LOCATION ("warmelt-outobj.melt:5343:/ cond");
      /*cond */ if ( /*_#IS_STRING__L9*/ meltfnum[7])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V26*/ meltfptr[22] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-outobj.melt:5343:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check syntestname"),
				  ("warmelt-outobj.melt")
				  ? ("warmelt-outobj.melt") : __FILE__,
				  (5343) ? (5343) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V26*/ meltfptr[22] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V25*/ meltfptr[18] = /*_.IFELSE___V26*/ meltfptr[22];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:5343:/ clear");
	     /*clear *//*_#IS_STRING__L9*/ meltfnum[7] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V26*/ meltfptr[22] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V25*/ meltfptr[18] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:5344:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.SBUF__V4*/ meltfptr[3]),
				(0), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:5345:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.SBUF__V4*/ meltfptr[3]),
				(0), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:5346:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.SBUF__V4*/ meltfptr[3]),
				(0), 0);
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:5347:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_cstring =
	"/****** emitted syntax checking for C generating devices *****/";
      /*_.ADD2OUT__V27*/ meltfptr[22] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!ADD2OUT */ meltfrout->tabval[3])),
		    (melt_ptr_t) ( /*_.SBUF__V4*/ meltfptr[3]),
		    (MELTBPARSTR_CSTRING ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:5348:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.SBUF__V4*/ meltfptr[3]),
				(0), 0);
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:5349:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_cstring = "#if MELT_HAVE_DEBUG";
      /*_.ADD2OUT__V28*/ meltfptr[27] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!ADD2OUT */ meltfrout->tabval[3])),
		    (melt_ptr_t) ( /*_.SBUF__V4*/ meltfptr[3]),
		    (MELTBPARSTR_CSTRING ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:5350:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.SBUF__V4*/ meltfptr[3]),
				(0), 0);
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:5351:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[7];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_cstring =
	"\n/* generated syntax checking routine for ";
      /*^apply.arg */
      argtab[1].meltbp_long = /*_#NBGENDEV__L6*/ meltfnum[0];
      /*^apply.arg */
      argtab[2].meltbp_cstring =
	" C generating devices */\nMELT_EXTERN void MELT_MODULE_VISIBILITY ";
      /*^apply.arg */
      argtab[3].meltbp_aptr =
	(melt_ptr_t *) & /*_.SYNTESTNAME__V20*/ meltfptr[16];
      /*^apply.arg */
      argtab[4].meltbp_cstring = " (void);\n\nvoid\n";
      /*^apply.arg */
      argtab[5].meltbp_aptr =
	(melt_ptr_t *) & /*_.SYNTESTNAME__V20*/ meltfptr[16];
      /*^apply.arg */
      argtab[6].meltbp_cstring = " (void) {\nif (1) return;\n";
      /*_.ADD2OUT__V29*/ meltfptr[28] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!ADD2OUT */ meltfrout->tabval[3])),
		    (melt_ptr_t) ( /*_.SBUF__V4*/ meltfptr[3]),
		    (MELTBPARSTR_CSTRING MELTBPARSTR_LONG MELTBPARSTR_CSTRING
		     MELTBPARSTR_PTR MELTBPARSTR_CSTRING MELTBPARSTR_PTR
		     MELTBPARSTR_CSTRING ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;
    /*citerblock FOREACH_IN_MULTIPLE */
    {
      /* start foreach_in_multiple meltcit1__EACHTUP */
      long meltcit1__EACHTUP_ln =
	melt_multiple_length ((melt_ptr_t) /*_.GENDEVTUP__V2*/ meltfptr[1]);
      for ( /*_#GIX__L10*/ meltfnum[1] = 0;
	   ( /*_#GIX__L10*/ meltfnum[1] >= 0)
	   && ( /*_#GIX__L10*/ meltfnum[1] < meltcit1__EACHTUP_ln);
	/*_#GIX__L10*/ meltfnum[1]++)
	{
	  /*_.CURGENDEV__V30*/ meltfptr[29] =
	    melt_multiple_nth ((melt_ptr_t)
			       ( /*_.GENDEVTUP__V2*/ meltfptr[1]),
			       /*_#GIX__L10*/ meltfnum[1]);




	  {
	    MELT_LOCATION ("warmelt-outobj.melt:5363:/ locexp");
	    meltgc_strbuf_add_indent ((melt_ptr_t)
				      ( /*_.SBUF__V4*/ meltfptr[3]), (1), 0);
	  }
	  ;

#if MELT_HAVE_DEBUG
	  MELT_LOCATION ("warmelt-outobj.melt:5364:/ cppif.then");
	  /*^block */
	  /*anyblock */
	  {


	    {
	      /*^locexp */
	      /*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	      melt_dbgcounter++;
#endif
	      ;
	    }
	    ;
	    /*^checksignal */
	    MELT_CHECK_SIGNAL ();
	    ;
    /*_#MELT_NEED_DBG__L11*/ meltfnum[7] =
	      /*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	      ( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	      0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	      ;;
	    MELT_LOCATION ("warmelt-outobj.melt:5364:/ cond");
	    /*cond */ if ( /*_#MELT_NEED_DBG__L11*/ meltfnum[7])	/*then */
	      {
		/*^cond.then */
		/*^block */
		/*anyblock */
		{

      /*_#THE_MELTCALLCOUNT__L12*/ meltfnum[11] =
#ifdef meltcallcount
		    meltcallcount	/* the_meltcallcount */
#else
		    0L
#endif /* meltcallcount the_meltcallcount */
		    ;;
		  MELT_LOCATION ("warmelt-outobj.melt:5364:/ checksignal");
		  MELT_CHECK_SIGNAL ();
		  ;
		  /*^apply */
		  /*apply */
		  {
		    union meltparam_un argtab[7];
		    memset (&argtab, 0, sizeof (argtab));
		    /*^apply.arg */
		    argtab[0].meltbp_long =
		      /*_#THE_MELTCALLCOUNT__L12*/ meltfnum[11];
		    /*^apply.arg */
		    argtab[1].meltbp_cstring = "warmelt-outobj.melt";
		    /*^apply.arg */
		    argtab[2].meltbp_long = 5364;
		    /*^apply.arg */
		    argtab[3].meltbp_cstring =
		      "emit_syntax_testing_routine curgendev=";
		    /*^apply.arg */
		    argtab[4].meltbp_aptr =
		      (melt_ptr_t *) & /*_.CURGENDEV__V30*/ meltfptr[29];
		    /*^apply.arg */
		    argtab[5].meltbp_cstring = "\n gix#";
		    /*^apply.arg */
		    argtab[6].meltbp_long = /*_#GIX__L10*/ meltfnum[1];
		    /*_.MELT_DEBUG_FUN__V33*/ meltfptr[32] =
		      melt_apply ((meltclosure_ptr_t)
				  (( /*!MELT_DEBUG_FUN */ meltfrout->
				    tabval[0])),
				  (melt_ptr_t) (( /*nil */ NULL)),
				  (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_PTR MELTBPARSTR_CSTRING
				   MELTBPARSTR_LONG ""), argtab, "",
				  (union meltparam_un *) 0);
		  }
		  ;
		  /*_.IF___V32*/ meltfptr[31] =
		    /*_.MELT_DEBUG_FUN__V33*/ meltfptr[32];;
		  /*epilog */

		  MELT_LOCATION ("warmelt-outobj.melt:5364:/ clear");
		/*clear *//*_#THE_MELTCALLCOUNT__L12*/ meltfnum[11] = 0;
		  /*^clear */
		/*clear *//*_.MELT_DEBUG_FUN__V33*/ meltfptr[32] = 0;
		}
		;
	      }
	    else
	      {			/*^cond.else */

     /*_.IF___V32*/ meltfptr[31] = NULL;;
	      }
	    ;
	    MELT_LOCATION ("warmelt-outobj.melt:5364:/ quasiblock");


	    /*_.PROGN___V34*/ meltfptr[32] = /*_.IF___V32*/ meltfptr[31];;
	    /*^compute */
	    /*_.IFCPP___V31*/ meltfptr[30] = /*_.PROGN___V34*/ meltfptr[32];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-outobj.melt:5364:/ clear");
	      /*clear *//*_#MELT_NEED_DBG__L11*/ meltfnum[7] = 0;
	    /*^clear */
	      /*clear *//*_.IF___V32*/ meltfptr[31] = 0;
	    /*^clear */
	      /*clear *//*_.PROGN___V34*/ meltfptr[32] = 0;
	  }

#else /*MELT_HAVE_DEBUG */
	  /*^cppif.else */
	  /*_.IFCPP___V31*/ meltfptr[30] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
	  ;

#if MELT_HAVE_DEBUG
	  MELT_LOCATION ("warmelt-outobj.melt:5365:/ cppif.then");
	  /*^block */
	  /*anyblock */
	  {

	    /*^checksignal */
	    MELT_CHECK_SIGNAL ();
	    ;
    /*_#IS_A__L13*/ meltfnum[11] =
	      melt_is_instance_of ((melt_ptr_t)
				   ( /*_.CURGENDEV__V30*/ meltfptr[29]),
				   (melt_ptr_t) (( /*!CLASS_SOURCE_GENERATOR_DEVICE */ meltfrout->tabval[5])));;
	    MELT_LOCATION ("warmelt-outobj.melt:5365:/ cond");
	    /*cond */ if ( /*_#IS_A__L13*/ meltfnum[11])	/*then */
	      {
		/*^cond.then */
		/*_.IFELSE___V36*/ meltfptr[32] = ( /*nil */ NULL);;
	      }
	    else
	      {
		MELT_LOCATION ("warmelt-outobj.melt:5365:/ cond.else");

		/*^block */
		/*anyblock */
		{




		  {
		    /*^locexp */
		    melt_assert_failed (("check curgendev"),
					("warmelt-outobj.melt")
					? ("warmelt-outobj.melt") : __FILE__,
					(5365) ? (5365) : __LINE__,
					__FUNCTION__);
		    ;
		  }
		  ;
		/*clear *//*_.IFELSE___V36*/ meltfptr[32] = 0;
		  /*epilog */
		}
		;
	      }
	    ;
	    /*^compute */
	    /*_.IFCPP___V35*/ meltfptr[31] = /*_.IFELSE___V36*/ meltfptr[32];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-outobj.melt:5365:/ clear");
	      /*clear *//*_#IS_A__L13*/ meltfnum[11] = 0;
	    /*^clear */
	      /*clear *//*_.IFELSE___V36*/ meltfptr[32] = 0;
	  }

#else /*MELT_HAVE_DEBUG */
	  /*^cppif.else */
	  /*_.IFCPP___V35*/ meltfptr[31] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
	  ;
	  MELT_LOCATION ("warmelt-outobj.melt:5366:/ quasiblock");


  /*_#SUCGIX__L14*/ meltfnum[7] =
	    (( /*_#GIX__L10*/ meltfnum[1]) + (1));;
	  MELT_LOCATION ("warmelt-outobj.melt:5367:/ cond");
	  /*cond */ if (
			 /*ifisa */
			 melt_is_instance_of ((melt_ptr_t)
					      ( /*_.CURGENDEV__V30*/
					       meltfptr[29]),
					      (melt_ptr_t) (( /*!CLASS_SOURCE_GENERATOR_DEVICE */ meltfrout->tabval[5])))
	    )			/*then */
	    {
	      /*^cond.then */
	      /*^getslot */
	      {
		melt_ptr_t slot = NULL, obj = NULL;
		obj =
		  (melt_ptr_t) ( /*_.CURGENDEV__V30*/ meltfptr[29]) /*=obj*/ ;
		melt_object_get_field (slot, obj, 3, "SRCGEN_REPR");
    /*_.REPR__V38*/ meltfptr[37] = slot;
	      };
	      ;
	    }
	  else
	    {			/*^cond.else */

   /*_.REPR__V38*/ meltfptr[37] = NULL;;
	    }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:5369:/ locexp");
	    meltgc_strbuf_add_indent ((melt_ptr_t)
				      ( /*_.SBUF__V4*/ meltfptr[3]), (0), 0);
	  }
	  ;
	  MELT_LOCATION ("warmelt-outobj.melt:5370:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^apply */
	  /*apply */
	  {
	    union meltparam_un argtab[3];
	    memset (&argtab, 0, sizeof (argtab));
	    /*^apply.arg */
	    argtab[0].meltbp_cstring = "/* generating device #";
	    /*^apply.arg */
	    argtab[1].meltbp_long = /*_#SUCGIX__L14*/ meltfnum[7];
	    /*^apply.arg */
	    argtab[2].meltbp_cstring = " */";
	    /*_.ADD2OUT__V39*/ meltfptr[38] =
	      melt_apply ((meltclosure_ptr_t)
			  (( /*!ADD2OUT */ meltfrout->tabval[3])),
			  (melt_ptr_t) ( /*_.SBUF__V4*/ meltfptr[3]),
			  (MELTBPARSTR_CSTRING MELTBPARSTR_LONG
			   MELTBPARSTR_CSTRING ""), argtab, "",
			  (union meltparam_un *) 0);
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:5371:/ locexp");
	    meltgc_strbuf_add_indent ((melt_ptr_t)
				      ( /*_.SBUF__V4*/ meltfptr[3]), (0), 0);
	  }
	  ;

#if MELT_HAVE_DEBUG
	  MELT_LOCATION ("warmelt-outobj.melt:5372:/ cppif.then");
	  /*^block */
	  /*anyblock */
	  {


	    {
	      /*^locexp */
	      /*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	      melt_dbgcounter++;
#endif
	      ;
	    }
	    ;
	    /*^checksignal */
	    MELT_CHECK_SIGNAL ();
	    ;
    /*_#MELT_NEED_DBG__L15*/ meltfnum[11] =
	      /*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	      ( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	      0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	      ;;
	    MELT_LOCATION ("warmelt-outobj.melt:5372:/ cond");
	    /*cond */ if ( /*_#MELT_NEED_DBG__L15*/ meltfnum[11])	/*then */
	      {
		/*^cond.then */
		/*^block */
		/*anyblock */
		{

      /*_#THE_MELTCALLCOUNT__L16*/ meltfnum[15] =
#ifdef meltcallcount
		    meltcallcount	/* the_meltcallcount */
#else
		    0L
#endif /* meltcallcount the_meltcallcount */
		    ;;
		  MELT_LOCATION ("warmelt-outobj.melt:5372:/ checksignal");
		  MELT_CHECK_SIGNAL ();
		  ;
		  /*^apply */
		  /*apply */
		  {
		    union meltparam_un argtab[5];
		    memset (&argtab, 0, sizeof (argtab));
		    /*^apply.arg */
		    argtab[0].meltbp_long =
		      /*_#THE_MELTCALLCOUNT__L16*/ meltfnum[15];
		    /*^apply.arg */
		    argtab[1].meltbp_cstring = "warmelt-outobj.melt";
		    /*^apply.arg */
		    argtab[2].meltbp_long = 5372;
		    /*^apply.arg */
		    argtab[3].meltbp_cstring =
		      "emit_syntax_testing_routine before syntax_test_generator repr=";
		    /*^apply.arg */
		    argtab[4].meltbp_aptr =
		      (melt_ptr_t *) & /*_.REPR__V38*/ meltfptr[37];
		    /*_.MELT_DEBUG_FUN__V42*/ meltfptr[41] =
		      melt_apply ((meltclosure_ptr_t)
				  (( /*!MELT_DEBUG_FUN */ meltfrout->
				    tabval[0])),
				  (melt_ptr_t) (( /*nil */ NULL)),
				  (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_PTR ""), argtab, "",
				  (union meltparam_un *) 0);
		  }
		  ;
		  /*_.IF___V41*/ meltfptr[40] =
		    /*_.MELT_DEBUG_FUN__V42*/ meltfptr[41];;
		  /*epilog */

		  MELT_LOCATION ("warmelt-outobj.melt:5372:/ clear");
		/*clear *//*_#THE_MELTCALLCOUNT__L16*/ meltfnum[15] = 0;
		  /*^clear */
		/*clear *//*_.MELT_DEBUG_FUN__V42*/ meltfptr[41] = 0;
		}
		;
	      }
	    else
	      {			/*^cond.else */

     /*_.IF___V41*/ meltfptr[40] = NULL;;
	      }
	    ;
	    MELT_LOCATION ("warmelt-outobj.melt:5372:/ quasiblock");


	    /*_.PROGN___V43*/ meltfptr[41] = /*_.IF___V41*/ meltfptr[40];;
	    /*^compute */
	    /*_.IFCPP___V40*/ meltfptr[39] = /*_.PROGN___V43*/ meltfptr[41];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-outobj.melt:5372:/ clear");
	      /*clear *//*_#MELT_NEED_DBG__L15*/ meltfnum[11] = 0;
	    /*^clear */
	      /*clear *//*_.IF___V41*/ meltfptr[40] = 0;
	    /*^clear */
	      /*clear *//*_.PROGN___V43*/ meltfptr[41] = 0;
	  }

#else /*MELT_HAVE_DEBUG */
	  /*^cppif.else */
	  /*_.IFCPP___V40*/ meltfptr[39] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
	  ;
	  MELT_LOCATION ("warmelt-outobj.melt:5373:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^msend */
	  /*msend */
	  {
	    union meltparam_un argtab[4];
	    memset (&argtab, 0, sizeof (argtab));
	    /*^ojbmsend.arg */
	    argtab[0].meltbp_aptr =
	      (melt_ptr_t *) & /*_.CURGENDEV__V30*/ meltfptr[29];
	    /*^ojbmsend.arg */
	    argtab[1].meltbp_aptr =
	      (melt_ptr_t *) & /*_.SBUF__V4*/ meltfptr[3];
	    /*^ojbmsend.arg */
	    argtab[2].meltbp_aptr =
	      (melt_ptr_t *) & /*_.MODCTX__V3*/ meltfptr[2];
	    /*^ojbmsend.arg */
	    argtab[3].meltbp_long = /*_#GIX__L10*/ meltfnum[1];
	    /*_.SYNTAX_TEST_GENERATOR__V44*/ meltfptr[40] =
	      meltgc_send ((melt_ptr_t) ( /*_.REPR__V38*/ meltfptr[37]),
			   (melt_ptr_t) (( /*!SYNTAX_TEST_GENERATOR */
					  meltfrout->tabval[6])),
			   (MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_PTR
			    MELTBPARSTR_LONG ""), argtab, "",
			   (union meltparam_un *) 0);
	  }
	  ;

#if MELT_HAVE_DEBUG
	  MELT_LOCATION ("warmelt-outobj.melt:5374:/ cppif.then");
	  /*^block */
	  /*anyblock */
	  {


	    {
	      /*^locexp */
	      /*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	      melt_dbgcounter++;
#endif
	      ;
	    }
	    ;
	    /*^checksignal */
	    MELT_CHECK_SIGNAL ();
	    ;
    /*_#MELT_NEED_DBG__L17*/ meltfnum[15] =
	      /*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	      ( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	      0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	      ;;
	    MELT_LOCATION ("warmelt-outobj.melt:5374:/ cond");
	    /*cond */ if ( /*_#MELT_NEED_DBG__L17*/ meltfnum[15])	/*then */
	      {
		/*^cond.then */
		/*^block */
		/*anyblock */
		{

      /*_#THE_MELTCALLCOUNT__L18*/ meltfnum[11] =
#ifdef meltcallcount
		    meltcallcount	/* the_meltcallcount */
#else
		    0L
#endif /* meltcallcount the_meltcallcount */
		    ;;
		  MELT_LOCATION ("warmelt-outobj.melt:5374:/ checksignal");
		  MELT_CHECK_SIGNAL ();
		  ;
		  /*^apply */
		  /*apply */
		  {
		    union meltparam_un argtab[8];
		    memset (&argtab, 0, sizeof (argtab));
		    /*^apply.arg */
		    argtab[0].meltbp_long =
		      /*_#THE_MELTCALLCOUNT__L18*/ meltfnum[11];
		    /*^apply.arg */
		    argtab[1].meltbp_cstring = "warmelt-outobj.melt";
		    /*^apply.arg */
		    argtab[2].meltbp_long = 5374;
		    /*^apply.arg */
		    argtab[3].meltbp_cstring =
		      "emit_syntax_testing_routine after syntax_test_generator repr=";
		    /*^apply.arg */
		    argtab[4].meltbp_aptr =
		      (melt_ptr_t *) & /*_.REPR__V38*/ meltfptr[37];
		    /*^apply.arg */
		    argtab[5].meltbp_cstring = "\n gix#";
		    /*^apply.arg */
		    argtab[6].meltbp_long = /*_#GIX__L10*/ meltfnum[1];
		    /*^apply.arg */
		    argtab[7].meltbp_cstring = "\n";
		    /*_.MELT_DEBUG_FUN__V47*/ meltfptr[46] =
		      melt_apply ((meltclosure_ptr_t)
				  (( /*!MELT_DEBUG_FUN */ meltfrout->
				    tabval[0])),
				  (melt_ptr_t) (( /*nil */ NULL)),
				  (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_PTR MELTBPARSTR_CSTRING
				   MELTBPARSTR_LONG MELTBPARSTR_CSTRING ""),
				  argtab, "", (union meltparam_un *) 0);
		  }
		  ;
		  /*_.IF___V46*/ meltfptr[45] =
		    /*_.MELT_DEBUG_FUN__V47*/ meltfptr[46];;
		  /*epilog */

		  MELT_LOCATION ("warmelt-outobj.melt:5374:/ clear");
		/*clear *//*_#THE_MELTCALLCOUNT__L18*/ meltfnum[11] = 0;
		  /*^clear */
		/*clear *//*_.MELT_DEBUG_FUN__V47*/ meltfptr[46] = 0;
		}
		;
	      }
	    else
	      {			/*^cond.else */

     /*_.IF___V46*/ meltfptr[45] = NULL;;
	      }
	    ;
	    MELT_LOCATION ("warmelt-outobj.melt:5374:/ quasiblock");


	    /*_.PROGN___V48*/ meltfptr[46] = /*_.IF___V46*/ meltfptr[45];;
	    /*^compute */
	    /*_.IFCPP___V45*/ meltfptr[41] = /*_.PROGN___V48*/ meltfptr[46];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-outobj.melt:5374:/ clear");
	      /*clear *//*_#MELT_NEED_DBG__L17*/ meltfnum[15] = 0;
	    /*^clear */
	      /*clear *//*_.IF___V46*/ meltfptr[45] = 0;
	    /*^clear */
	      /*clear *//*_.PROGN___V48*/ meltfptr[46] = 0;
	  }

#else /*MELT_HAVE_DEBUG */
	  /*^cppif.else */
	  /*_.IFCPP___V45*/ meltfptr[41] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
	  ;
	  /*^compute */
	  /*_.LET___V37*/ meltfptr[32] = /*_.IFCPP___V45*/ meltfptr[41];;

	  MELT_LOCATION ("warmelt-outobj.melt:5366:/ clear");
	    /*clear *//*_#SUCGIX__L14*/ meltfnum[7] = 0;
	  /*^clear */
	    /*clear *//*_.REPR__V38*/ meltfptr[37] = 0;
	  /*^clear */
	    /*clear *//*_.ADD2OUT__V39*/ meltfptr[38] = 0;
	  /*^clear */
	    /*clear *//*_.IFCPP___V40*/ meltfptr[39] = 0;
	  /*^clear */
	    /*clear *//*_.SYNTAX_TEST_GENERATOR__V44*/ meltfptr[40] = 0;
	  /*^clear */
	    /*clear *//*_.IFCPP___V45*/ meltfptr[41] = 0;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:5376:/ locexp");
	    meltgc_strbuf_add_indent ((melt_ptr_t)
				      ( /*_.SBUF__V4*/ meltfptr[3]), (0), 0);
	  }
	  ;
	  if ( /*_#GIX__L10*/ meltfnum[1] < 0)
	    break;
	}			/* end  foreach_in_multiple meltcit1__EACHTUP */

      /*citerepilog */

      MELT_LOCATION ("warmelt-outobj.melt:5360:/ clear");
	    /*clear *//*_.CURGENDEV__V30*/ meltfptr[29] = 0;
      /*^clear */
	    /*clear *//*_#GIX__L10*/ meltfnum[1] = 0;
      /*^clear */
	    /*clear *//*_.IFCPP___V31*/ meltfptr[30] = 0;
      /*^clear */
	    /*clear *//*_.IFCPP___V35*/ meltfptr[31] = 0;
      /*^clear */
	    /*clear *//*_.LET___V37*/ meltfptr[32] = 0;
    }				/*endciterblock FOREACH_IN_MULTIPLE */
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:5378:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.SBUF__V4*/ meltfptr[3]),
				(0), 0);
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:5379:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[3];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_cstring = "} /* end generated ";
      /*^apply.arg */
      argtab[1].meltbp_aptr =
	(melt_ptr_t *) & /*_.SYNTESTNAME__V20*/ meltfptr[16];
      /*^apply.arg */
      argtab[2].meltbp_cstring = " */";
      /*_.ADD2OUT__V49*/ meltfptr[45] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!ADD2OUT */ meltfrout->tabval[3])),
		    (melt_ptr_t) ( /*_.SBUF__V4*/ meltfptr[3]),
		    (MELTBPARSTR_CSTRING MELTBPARSTR_PTR MELTBPARSTR_CSTRING
		     ""), argtab, "", (union meltparam_un *) 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:5382:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.SBUF__V4*/ meltfptr[3]),
				(0), 0);
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:5383:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_cstring = "#endif /*MELT_HAVE_DEBUG syntaxcheck*/";
      /*_.ADD2OUT__V50*/ meltfptr[46] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!ADD2OUT */ meltfrout->tabval[3])),
		    (melt_ptr_t) ( /*_.SBUF__V4*/ meltfptr[3]),
		    (MELTBPARSTR_CSTRING ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:5384:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.SBUF__V4*/ meltfptr[3]),
				(0), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:5385:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.SBUF__V4*/ meltfptr[3]),
				(0), 0);
    }
    ;

    MELT_LOCATION ("warmelt-outobj.melt:5333:/ clear");
	   /*clear *//*_#NBGENDEV__L6*/ meltfnum[0] = 0;
    /*^clear */
	   /*clear *//*_.MODNAM__V15*/ meltfptr[13] = 0;
    /*^clear */
	   /*clear *//*_.LET___V16*/ meltfptr[15] = 0;
    /*^clear */
	   /*clear *//*_.SYNTESTNAME__V20*/ meltfptr[16] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V21*/ meltfptr[17] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V25*/ meltfptr[18] = 0;
    /*^clear */
	   /*clear *//*_.ADD2OUT__V27*/ meltfptr[22] = 0;
    /*^clear */
	   /*clear *//*_.ADD2OUT__V28*/ meltfptr[27] = 0;
    /*^clear */
	   /*clear *//*_.ADD2OUT__V29*/ meltfptr[28] = 0;
    /*^clear */
	   /*clear *//*_.ADD2OUT__V49*/ meltfptr[45] = 0;
    /*^clear */
	   /*clear *//*_.ADD2OUT__V50*/ meltfptr[46] = 0;
    MELT_LOCATION ("warmelt-outobj.melt:5327:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V5*/ meltfptr[4] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V9*/ meltfptr[5] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V11*/ meltfptr[6] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V13*/ meltfptr[11] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("EMIT_SYNTAX_TESTING_ROUTINE", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_130_warmelt_outobj_EMIT_SYNTAX_TESTING_ROUTINE_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*noretval */ NULL);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_130_warmelt_outobj_EMIT_SYNTAX_TESTING_ROUTINE */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_131_warmelt_outobj_NORMADECLB_MACROEXPANDED_LIST (meltclosure_ptr_t
							   meltclosp_,
							   melt_ptr_t
							   meltfirstargp_,
							   const
							   melt_argdescr_cell_t
							   meltxargdescr_[],
							   union meltparam_un
							   * meltxargtab_,
							   const
							   melt_argdescr_cell_t
							   meltxresdescr_[],
							   union meltparam_un
							   * meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_131_warmelt_outobj_NORMADECLB_MACROEXPANDED_LIST_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_131_warmelt_outobj_NORMADECLB_MACROEXPANDED_LIST_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 104
    melt_ptr_t mcfr_varptr[104];
#define MELTFRAM_NBVARNUM 38
    long mcfr_varnum[38];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_131_warmelt_outobj_NORMADECLB_MACROEXPANDED_LIST is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct
	 frame_meltrout_131_warmelt_outobj_NORMADECLB_MACROEXPANDED_LIST_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 104; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_131_warmelt_outobj_NORMADECLB_MACROEXPANDED_LIST nbval 104*/
  meltfram__.mcfr_nbvar = 104 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("NORMADECLB_MACROEXPANDED_LIST", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-outobj.melt:5392:/ getarg");
 /*_.XLIST__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.MODNAMSTR__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.MODNAMSTR__V3*/ meltfptr[2])) !=
	      NULL);


  /*getarg#2 */
  /*^getarg */
  if (meltxargdescr_[1] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.MODCTX__V4*/ meltfptr[3] =
    (meltxargtab_[1].meltbp_aptr) ? (*(meltxargtab_[1].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.MODCTX__V4*/ meltfptr[3])) !=
	      NULL);


  /*getarg#3 */
  /*^getarg */
  if (meltxargdescr_[2] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.NCX__V5*/ meltfptr[4] =
    (meltxargtab_[2].meltbp_aptr) ? (*(meltxargtab_[2].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.NCX__V5*/ meltfptr[4])) != NULL);


  /*getarg#4 */
  /*^getarg */
  if (meltxargdescr_[3] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.INIENV__V6*/ meltfptr[5] =
    (meltxargtab_[3].meltbp_aptr) ? (*(meltxargtab_[3].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.INIENV__V6*/ meltfptr[5])) !=
	      NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:5393:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L1*/ meltfnum[0] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-outobj.melt:5393:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L1*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-outobj.melt:5393:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[13];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-outobj.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 5393;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring =
		"normadeclb_macroexpanded_list start xlist= ";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.XLIST__V2*/ meltfptr[1];
	      /*^apply.arg */
	      argtab[5].meltbp_cstring = "\n* modnamstr=";
	      /*^apply.arg */
	      argtab[6].meltbp_aptr =
		(melt_ptr_t *) & /*_.MODNAMSTR__V3*/ meltfptr[2];
	      /*^apply.arg */
	      argtab[7].meltbp_cstring = "\n* modctx=";
	      /*^apply.arg */
	      argtab[8].meltbp_aptr =
		(melt_ptr_t *) & /*_.MODCTX__V4*/ meltfptr[3];
	      /*^apply.arg */
	      argtab[9].meltbp_cstring = "\n* ncx=";
	      /*^apply.arg */
	      argtab[10].meltbp_aptr =
		(melt_ptr_t *) & /*_.NCX__V5*/ meltfptr[4];
	      /*^apply.arg */
	      argtab[11].meltbp_cstring = "\n* inienv=";
	      /*^apply.arg */
	      argtab[12].meltbp_aptr =
		(melt_ptr_t *) & /*_.INIENV__V6*/ meltfptr[5];
	      /*_.MELT_DEBUG_FUN__V9*/ meltfptr[8] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V8*/ meltfptr[7] = /*_.MELT_DEBUG_FUN__V9*/ meltfptr[8];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-outobj.melt:5393:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V9*/ meltfptr[8] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V8*/ meltfptr[7] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-outobj.melt:5393:/ quasiblock");


      /*_.PROGN___V10*/ meltfptr[8] = /*_.IF___V8*/ meltfptr[7];;
      /*^compute */
      /*_.IFCPP___V7*/ meltfptr[6] = /*_.PROGN___V10*/ meltfptr[8];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:5393:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L1*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IF___V8*/ meltfptr[7] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V10*/ meltfptr[8] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V7*/ meltfptr[6] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:5398:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_LIST__L3*/ meltfnum[1] =
	(melt_magic_discr ((melt_ptr_t) ( /*_.XLIST__V2*/ meltfptr[1])) ==
	 MELTOBMAG_LIST);;
      MELT_LOCATION ("warmelt-outobj.melt:5398:/ cond");
      /*cond */ if ( /*_#IS_LIST__L3*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V12*/ meltfptr[8] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-outobj.melt:5398:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check xlist"),
				  ("warmelt-outobj.melt")
				  ? ("warmelt-outobj.melt") : __FILE__,
				  (5398) ? (5398) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V12*/ meltfptr[8] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V11*/ meltfptr[7] = /*_.IFELSE___V12*/ meltfptr[8];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:5398:/ clear");
	     /*clear *//*_#IS_LIST__L3*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V12*/ meltfptr[8] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V11*/ meltfptr[7] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:5399:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_STRING__L4*/ meltfnum[0] =
	(melt_magic_discr ((melt_ptr_t) ( /*_.MODNAMSTR__V3*/ meltfptr[2])) ==
	 MELTOBMAG_STRING);;
      MELT_LOCATION ("warmelt-outobj.melt:5399:/ cond");
      /*cond */ if ( /*_#IS_STRING__L4*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V14*/ meltfptr[13] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-outobj.melt:5399:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check modnamstr"),
				  ("warmelt-outobj.melt")
				  ? ("warmelt-outobj.melt") : __FILE__,
				  (5399) ? (5399) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V14*/ meltfptr[13] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V13*/ meltfptr[8] = /*_.IFELSE___V14*/ meltfptr[13];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:5399:/ clear");
	     /*clear *//*_#IS_STRING__L4*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V14*/ meltfptr[13] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V13*/ meltfptr[8] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:5400:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L5*/ meltfnum[1] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.MODCTX__V4*/ meltfptr[3]),
			     (melt_ptr_t) (( /*!CLASS_MODULE_CONTEXT */
					    meltfrout->tabval[1])));;
      MELT_LOCATION ("warmelt-outobj.melt:5400:/ cond");
      /*cond */ if ( /*_#IS_A__L5*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V16*/ meltfptr[15] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-outobj.melt:5400:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check modctx"),
				  ("warmelt-outobj.melt")
				  ? ("warmelt-outobj.melt") : __FILE__,
				  (5400) ? (5400) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V16*/ meltfptr[15] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V15*/ meltfptr[13] = /*_.IFELSE___V16*/ meltfptr[15];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:5400:/ clear");
	     /*clear *//*_#IS_A__L5*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V16*/ meltfptr[15] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V15*/ meltfptr[13] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:5401:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L6*/ meltfnum[0] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.NCX__V5*/ meltfptr[4]),
			     (melt_ptr_t) (( /*!CLASS_NORMALIZATION_CONTEXT */
					    meltfrout->tabval[2])));;
      MELT_LOCATION ("warmelt-outobj.melt:5401:/ cond");
      /*cond */ if ( /*_#IS_A__L6*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V18*/ meltfptr[17] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-outobj.melt:5401:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check ncx"),
				  ("warmelt-outobj.melt")
				  ? ("warmelt-outobj.melt") : __FILE__,
				  (5401) ? (5401) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V18*/ meltfptr[17] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V17*/ meltfptr[15] = /*_.IFELSE___V18*/ meltfptr[17];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:5401:/ clear");
	     /*clear *//*_#IS_A__L6*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V18*/ meltfptr[17] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V17*/ meltfptr[15] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:5402:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L7*/ meltfnum[1] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.INIENV__V6*/ meltfptr[5]),
			     (melt_ptr_t) (( /*!CLASS_ENVIRONMENT */
					    meltfrout->tabval[3])));;
      MELT_LOCATION ("warmelt-outobj.melt:5402:/ cond");
      /*cond */ if ( /*_#IS_A__L7*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V20*/ meltfptr[19] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-outobj.melt:5402:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check inienv"),
				  ("warmelt-outobj.melt")
				  ? ("warmelt-outobj.melt") : __FILE__,
				  (5402) ? (5402) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V20*/ meltfptr[19] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V19*/ meltfptr[17] = /*_.IFELSE___V20*/ meltfptr[19];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:5402:/ clear");
	     /*clear *//*_#IS_A__L7*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V20*/ meltfptr[19] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V19*/ meltfptr[17] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:5403:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L8*/ meltfnum[0] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-outobj.melt:5403:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L8*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L9*/ meltfnum[1] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-outobj.melt:5403:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L9*/ meltfnum[1];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-outobj.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 5403;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring =
		"normadeclb_macroexpanded_list modctx=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.MODCTX__V4*/ meltfptr[3];
	      /*_.MELT_DEBUG_FUN__V23*/ meltfptr[22] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V22*/ meltfptr[21] =
	      /*_.MELT_DEBUG_FUN__V23*/ meltfptr[22];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-outobj.melt:5403:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L9*/ meltfnum[1] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V23*/ meltfptr[22] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V22*/ meltfptr[21] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-outobj.melt:5403:/ quasiblock");


      /*_.PROGN___V24*/ meltfptr[22] = /*_.IF___V22*/ meltfptr[21];;
      /*^compute */
      /*_.IFCPP___V21*/ meltfptr[19] = /*_.PROGN___V24*/ meltfptr[22];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:5403:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L8*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IF___V22*/ meltfptr[21] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V24*/ meltfptr[22] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V21*/ meltfptr[19] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-outobj.melt:5404:/ quasiblock");


 /*_#STARTCLOCK__L10*/ meltfnum[1] = 0;;
    MELT_LOCATION ("warmelt-outobj.melt:5406:/ blockmultialloc");
    /*multiallocblock */
    {
      struct meltletrec_1_st
      {
	struct meltlist_st rlist_0__LIST_;
	long meltletrec_1_endgap;
      } *meltletrec_1_ptr = 0;
      meltletrec_1_ptr =
	(struct meltletrec_1_st *)
	meltgc_allocate (sizeof (struct meltletrec_1_st), 0);
      /*^blockmultialloc.initfill */
      /*inilist rlist_0__LIST_ */
   /*_.LIST___V27*/ meltfptr[26] =
	(melt_ptr_t) & meltletrec_1_ptr->rlist_0__LIST_;
      meltletrec_1_ptr->rlist_0__LIST_.discr =
	(meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (DISCR_LIST))));



      /*_.NORMLIST__V26*/ meltfptr[22] = /*_.LIST___V27*/ meltfptr[26];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:5406:/ clear");
	    /*clear *//*_.LIST___V27*/ meltfptr[26] = 0;
      /*^clear */
	    /*clear *//*_.LIST___V27*/ meltfptr[26] = 0;
    }				/*end multiallocblock */
    ;
    MELT_LOCATION ("warmelt-outobj.melt:5407:/ blockmultialloc");
    /*multiallocblock */
    {
      struct meltletrec_2_st
      {
	struct meltlist_st rlist_0__LIST_;
	long meltletrec_2_endgap;
      } *meltletrec_2_ptr = 0;
      meltletrec_2_ptr =
	(struct meltletrec_2_st *)
	meltgc_allocate (sizeof (struct meltletrec_2_st), 0);
      /*^blockmultialloc.initfill */
      /*inilist rlist_0__LIST_ */
   /*_.LIST___V29*/ meltfptr[28] =
	(melt_ptr_t) & meltletrec_2_ptr->rlist_0__LIST_;
      meltletrec_2_ptr->rlist_0__LIST_.discr =
	(meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (DISCR_LIST))));



      /*_.BINDLIST__V28*/ meltfptr[26] = /*_.LIST___V29*/ meltfptr[28];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:5407:/ clear");
	    /*clear *//*_.LIST___V29*/ meltfptr[28] = 0;
      /*^clear */
	    /*clear *//*_.LIST___V29*/ meltfptr[28] = 0;
    }				/*end multiallocblock */
    ;
 /*_.DECLBUF__V30*/ meltfptr[28] =
      (melt_ptr_t)
      meltgc_new_strbuf ((meltobject_ptr_t)
			 (( /*!DISCR_STRBUF */ meltfrout->tabval[4])),
			 (const char *) 0);;
    /*^compute */
 /*_.LIST_FIRST__V31*/ meltfptr[30] =
      (melt_list_first ((melt_ptr_t) ( /*_.XLIST__V2*/ meltfptr[1])));;
    /*^compute */
 /*_.FIRSTX__V32*/ meltfptr[31] =
      (melt_pair_head ((melt_ptr_t) ( /*_.LIST_FIRST__V31*/ meltfptr[30])));;
    MELT_LOCATION ("warmelt-outobj.melt:5410:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#IS_A__L11*/ meltfnum[0] =
      melt_is_instance_of ((melt_ptr_t) ( /*_.FIRSTX__V32*/ meltfptr[31]),
			   (melt_ptr_t) (( /*!CLASS_SOURCE */ meltfrout->
					  tabval[5])));;
    MELT_LOCATION ("warmelt-outobj.melt:5410:/ cond");
    /*cond */ if ( /*_#IS_A__L11*/ meltfnum[0])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  /*^cond */
	  /*cond */ if (
			 /*ifisa */
			 melt_is_instance_of ((melt_ptr_t)
					      ( /*_.FIRSTX__V32*/
					       meltfptr[31]),
					      (melt_ptr_t) (( /*!CLASS_LOCATED */ meltfrout->tabval[6])))
	    )			/*then */
	    {
	      /*^cond.then */
	      /*^getslot */
	      {
		melt_ptr_t slot = NULL, obj = NULL;
		obj =
		  (melt_ptr_t) ( /*_.FIRSTX__V32*/ meltfptr[31]) /*=obj*/ ;
		melt_object_get_field (slot, obj, 1, "LOCA_LOCATION");
     /*_.LOCA_LOCATION__V34*/ meltfptr[33] = slot;
	      };
	      ;
	    }
	  else
	    {			/*^cond.else */

    /*_.LOCA_LOCATION__V34*/ meltfptr[33] = NULL;;
	    }
	  ;
	  /*^compute */
	  /*_.FIRSTLOC__V33*/ meltfptr[32] =
	    /*_.LOCA_LOCATION__V34*/ meltfptr[33];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-outobj.melt:5410:/ clear");
	     /*clear *//*_.LOCA_LOCATION__V34*/ meltfptr[33] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

  /*_.FIRSTLOC__V33*/ meltfptr[32] = NULL;;
      }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:5411:/ cond");
    /*cond */ if (
		   /*ifisa */
		   melt_is_instance_of ((melt_ptr_t)
					( /*_.MODCTX__V4*/ meltfptr[3]),
					(melt_ptr_t) (( /*!CLASS_MODULE_CONTEXT */ meltfrout->tabval[1])))
      )				/*then */
      {
	/*^cond.then */
	/*^getslot */
	{
	  melt_ptr_t slot = NULL, obj = NULL;
	  obj = (melt_ptr_t) ( /*_.MODCTX__V4*/ meltfptr[3]) /*=obj*/ ;
	  melt_object_get_field (slot, obj, 11, "MOCX_ERRORHANDLER");
   /*_.ERRORHDLR__V35*/ meltfptr[33] = slot;
	};
	;
      }
    else
      {				/*^cond.else */

  /*_.ERRORHDLR__V35*/ meltfptr[33] = NULL;;
      }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:5412:/ cond");
    /*cond */ if (
		   /*ifisa */
		   melt_is_instance_of ((melt_ptr_t)
					( /*_.MODCTX__V4*/ meltfptr[3]),
					(melt_ptr_t) (( /*!CLASS_MODULE_CONTEXT */ meltfrout->tabval[1])))
      )				/*then */
      {
	/*^cond.then */
	/*^getslot */
	{
	  melt_ptr_t slot = NULL, obj = NULL;
	  obj = (melt_ptr_t) ( /*_.MODCTX__V4*/ meltfptr[3]) /*=obj*/ ;
	  melt_object_get_field (slot, obj, 3, "MOCX_INITIALENV");
   /*_.MODINIENV__V36*/ meltfptr[35] = slot;
	};
	;
      }
    else
      {				/*^cond.else */

  /*_.MODINIENV__V36*/ meltfptr[35] = NULL;;
      }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:5413:/ cond");
    /*cond */ if (
		   /*ifisa */
		   melt_is_instance_of ((melt_ptr_t)
					( /*_.NCX__V5*/ meltfptr[4]),
					(melt_ptr_t) (( /*!CLASS_NORMALIZATION_CONTEXT */ meltfrout->tabval[2])))
      )				/*then */
      {
	/*^cond.then */
	/*^getslot */
	{
	  melt_ptr_t slot = NULL, obj = NULL;
	  obj = (melt_ptr_t) ( /*_.NCX__V5*/ meltfptr[4]) /*=obj*/ ;
	  melt_object_get_field (slot, obj, 0, "NCTX_INITPROC");
   /*_.INIPROC__V37*/ meltfptr[36] = slot;
	};
	;
      }
    else
      {				/*^cond.else */

  /*_.INIPROC__V37*/ meltfptr[36] = NULL;;
      }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:5414:/ cond");
    /*cond */ if (
		   /*ifisa */
		   melt_is_instance_of ((melt_ptr_t)
					( /*_.MODCTX__V4*/ meltfptr[3]),
					(melt_ptr_t) (( /*!CLASS_MODULE_CONTEXT */ meltfrout->tabval[1])))
      )				/*then */
      {
	/*^cond.then */
	/*^getslot */
	{
	  melt_ptr_t slot = NULL, obj = NULL;
	  obj = (melt_ptr_t) ( /*_.MODCTX__V4*/ meltfptr[3]) /*=obj*/ ;
	  melt_object_get_field (slot, obj, 8, "MOCX_CHEADERLIST");
   /*_.CHEADLIST__V38*/ meltfptr[37] = slot;
	};
	;
      }
    else
      {				/*^cond.else */

  /*_.CHEADLIST__V38*/ meltfptr[37] = NULL;;
      }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:5416:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*^rawallocobj */
    /*rawallocobj */
    {
      melt_ptr_t newobj = 0;
      melt_raw_object_create (newobj,
			      (melt_ptr_t) (( /*!CLASS_SOURCE_UPDATE_CURRENT_MODULE_ENVIRONMENT_CONTAINER */ meltfrout->tabval[7])), (3), "CLASS_SOURCE_UPDATE_CURRENT_MODULE_ENVIRONMENT_CONTAINER");
  /*_.INST__V40*/ meltfptr[39] =
	newobj;
    };
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @LOCA_LOCATION",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V40*/ meltfptr[39])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V40*/ meltfptr[39]), (1),
			  ( /*_.FIRSTLOC__V33*/ meltfptr[32]),
			  "LOCA_LOCATION");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @SUCME_COMMENT",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V40*/ meltfptr[39])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V40*/ meltfptr[39]), (2),
			  (( /*!konst_8 */ meltfrout->tabval[8])),
			  "SUCME_COMMENT");
    ;
    /*^touchobj */

    melt_dbgtrace_written_object ( /*_.INST__V40*/ meltfptr[39],
				  "newly made instance");
    ;
    /*_.UCMEB1__V39*/ meltfptr[38] = /*_.INST__V40*/ meltfptr[39];;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:5421:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L12*/ meltfnum[11] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-outobj.melt:5421:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L12*/ meltfnum[11])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L13*/ meltfnum[12] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-outobj.melt:5421:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long =
		/*_#THE_MELTCALLCOUNT__L13*/ meltfnum[12];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-outobj.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 5421;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring =
		"normadeclb_macroexpanded_list modctx=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.MODCTX__V4*/ meltfptr[3];
	      /*_.MELT_DEBUG_FUN__V43*/ meltfptr[42] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V42*/ meltfptr[41] =
	      /*_.MELT_DEBUG_FUN__V43*/ meltfptr[42];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-outobj.melt:5421:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L13*/ meltfnum[12] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V43*/ meltfptr[42] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V42*/ meltfptr[41] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-outobj.melt:5421:/ quasiblock");


      /*_.PROGN___V44*/ meltfptr[42] = /*_.IF___V42*/ meltfptr[41];;
      /*^compute */
      /*_.IFCPP___V41*/ meltfptr[40] = /*_.PROGN___V44*/ meltfptr[42];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:5421:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L12*/ meltfnum[11] = 0;
      /*^clear */
	     /*clear *//*_.IF___V42*/ meltfptr[41] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V44*/ meltfptr[42] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V41*/ meltfptr[40] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:5422:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L14*/ meltfnum[12] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-outobj.melt:5422:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L14*/ meltfnum[12])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L15*/ meltfnum[11] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-outobj.melt:5422:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long =
		/*_#THE_MELTCALLCOUNT__L15*/ meltfnum[11];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-outobj.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 5422;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring =
		"normadeclb_macroexpanded_list @@cheadlist=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.CHEADLIST__V38*/ meltfptr[37];
	      /*_.MELT_DEBUG_FUN__V47*/ meltfptr[46] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V46*/ meltfptr[42] =
	      /*_.MELT_DEBUG_FUN__V47*/ meltfptr[46];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-outobj.melt:5422:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L15*/ meltfnum[11] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V47*/ meltfptr[46] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V46*/ meltfptr[42] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-outobj.melt:5422:/ quasiblock");


      /*_.PROGN___V48*/ meltfptr[46] = /*_.IF___V46*/ meltfptr[42];;
      /*^compute */
      /*_.IFCPP___V45*/ meltfptr[41] = /*_.PROGN___V48*/ meltfptr[46];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:5422:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L14*/ meltfnum[12] = 0;
      /*^clear */
	     /*clear *//*_.IF___V46*/ meltfptr[42] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V48*/ meltfptr[46] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V45*/ meltfptr[41] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:5424:/ locexp");
      /* normadeclb_macroexpanded_list STARTMODNAM__1 */
#if HAVE_CLOCK
		/*_#STARTCLOCK__L10*/ meltfnum[1] = (long) clock ();
#endif /*HAVE_CLOCK */
#if MELT_HAVE_DEBUG
      inform (UNKNOWN_LOCATION,
	      "MELT [#%ld] generating C code of module %s",
	      melt_dbgcounter,
	      melt_string_str ((melt_ptr_t) /*_.MODNAMSTR__V3*/ meltfptr[2]));
#else
      inform (UNKNOWN_LOCATION,
	      "MELT generating C code of module %s",
	      melt_string_str ((melt_ptr_t) /*_.MODNAMSTR__V3*/ meltfptr[2]));
#endif /* MELT_HAVE_DEBUG */
      ;
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:5438:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t)
				( /*_.DECLBUF__V30*/ meltfptr[28]), (0), 0);
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:5442:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#IS_A__L16*/ meltfnum[11] =
      melt_is_instance_of ((melt_ptr_t) ( /*_.FIRSTX__V32*/ meltfptr[31]),
			   (melt_ptr_t) (( /*!CLASS_SOURCE_COMMENT */
					  meltfrout->tabval[9])));;
    MELT_LOCATION ("warmelt-outobj.melt:5442:/ cond");
    /*cond */ if ( /*_#IS_A__L16*/ meltfnum[11])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

   /*_.LIST_POPFIRST__V49*/ meltfptr[42] =
	    (meltgc_popfirst_list
	     ((melt_ptr_t) ( /*_.XLIST__V2*/ meltfptr[1])));;
	  MELT_LOCATION ("warmelt-outobj.melt:5444:/ quasiblock");


	  /*^getslot */
	  {
	    melt_ptr_t slot = NULL, obj = NULL;
	    obj = (melt_ptr_t) ( /*_.FIRSTX__V32*/ meltfptr[31]) /*=obj*/ ;
	    melt_object_get_field (slot, obj, 1, "LOCA_LOCATION");
    /*_.SLOC__V50*/ meltfptr[46] = slot;
	  };
	  ;
	  MELT_LOCATION ("warmelt-outobj.melt:5445:/ getslot");
	  {
	    melt_ptr_t slot = NULL, obj = NULL;
	    obj = (melt_ptr_t) ( /*_.FIRSTX__V32*/ meltfptr[31]) /*=obj*/ ;
	    melt_object_get_field (slot, obj, 2, "SCOMM_STR");
    /*_.SCOMM__V51*/ meltfptr[50] = slot;
	  };
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:5447:/ locexp");
	    meltgc_strbuf_add_indent ((melt_ptr_t)
				      ( /*_.DECLBUF__V30*/ meltfptr[28]), (0),
				      0);
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:5448:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t)
				 ( /*_.DECLBUF__V30*/ meltfptr[28]),
				 ("/***************************************************"));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:5451:/ locexp");
	    meltgc_strbuf_add_indent ((melt_ptr_t)
				      ( /*_.DECLBUF__V30*/ meltfptr[28]), (0),
				      0);
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:5452:/ locexp");
	    meltgc_add_strbuf_ccomment ((melt_ptr_t)
					( /*_.DECLBUF__V30*/ meltfptr[28]),
					melt_string_str ((melt_ptr_t)
							 ( /*_.SCOMM__V51*/
							  meltfptr[50])));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:5453:/ locexp");
	    meltgc_strbuf_add_indent ((melt_ptr_t)
				      ( /*_.DECLBUF__V30*/ meltfptr[28]), (0),
				      0);
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:5454:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t)
				 ( /*_.DECLBUF__V30*/ meltfptr[28]),
				 ("****************************************************/"));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:5457:/ locexp");
	    meltgc_strbuf_add_indent ((melt_ptr_t)
				      ( /*_.DECLBUF__V30*/ meltfptr[28]), (0),
				      0);
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:5458:/ locexp");
	    meltgc_strbuf_add_indent ((melt_ptr_t)
				      ( /*_.DECLBUF__V30*/ meltfptr[28]), (0),
				      0);
	  }
	  ;

	  MELT_LOCATION ("warmelt-outobj.melt:5444:/ clear");
	     /*clear *//*_.SLOC__V50*/ meltfptr[46] = 0;
	  /*^clear */
	     /*clear *//*_.SCOMM__V51*/ meltfptr[50] = 0;
	  MELT_LOCATION ("warmelt-outobj.melt:5442:/ quasiblock");


	  /*epilog */

	  /*^clear */
	     /*clear *//*_.LIST_POPFIRST__V49*/ meltfptr[42] = 0;
	}
	;
      }				/*noelse */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:5461:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L17*/ meltfnum[12] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-outobj.melt:5461:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L17*/ meltfnum[12])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L18*/ meltfnum[17] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-outobj.melt:5461:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[7];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long =
		/*_#THE_MELTCALLCOUNT__L18*/ meltfnum[17];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-outobj.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 5461;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring =
		"normadeclb_macroexpanded_list modctx=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.MODCTX__V4*/ meltfptr[3];
	      /*^apply.arg */
	      argtab[5].meltbp_cstring = "\n with @@cheadlist=";
	      /*^apply.arg */
	      argtab[6].meltbp_aptr =
		(melt_ptr_t *) & /*_.CHEADLIST__V38*/ meltfptr[37];
	      /*_.MELT_DEBUG_FUN__V54*/ meltfptr[42] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V53*/ meltfptr[50] =
	      /*_.MELT_DEBUG_FUN__V54*/ meltfptr[42];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-outobj.melt:5461:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L18*/ meltfnum[17] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V54*/ meltfptr[42] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V53*/ meltfptr[50] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-outobj.melt:5461:/ quasiblock");


      /*_.PROGN___V55*/ meltfptr[42] = /*_.IF___V53*/ meltfptr[50];;
      /*^compute */
      /*_.IFCPP___V52*/ meltfptr[46] = /*_.PROGN___V55*/ meltfptr[42];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:5461:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L17*/ meltfnum[12] = 0;
      /*^clear */
	     /*clear *//*_.IF___V53*/ meltfptr[50] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V55*/ meltfptr[42] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V52*/ meltfptr[46] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-outobj.melt:5464:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^cond */
    /*cond */ if ( /*_.MODINIENV__V36*/ meltfptr[35])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{


#if MELT_HAVE_DEBUG
	  MELT_LOCATION ("warmelt-outobj.melt:5466:/ cppif.then");
	  /*^block */
	  /*anyblock */
	  {


	    {
	      /*^locexp */
	      /*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	      melt_dbgcounter++;
#endif
	      ;
	    }
	    ;
	    /*^checksignal */
	    MELT_CHECK_SIGNAL ();
	    ;
     /*_#MELT_NEED_DBG__L19*/ meltfnum[17] =
	      /*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	      ( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	      0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	      ;;
	    MELT_LOCATION ("warmelt-outobj.melt:5466:/ cond");
	    /*cond */ if ( /*_#MELT_NEED_DBG__L19*/ meltfnum[17])	/*then */
	      {
		/*^cond.then */
		/*^block */
		/*anyblock */
		{

       /*_#THE_MELTCALLCOUNT__L20*/ meltfnum[12] =
#ifdef meltcallcount
		    meltcallcount	/* the_meltcallcount */
#else
		    0L
#endif /* meltcallcount the_meltcallcount */
		    ;;
		  MELT_LOCATION ("warmelt-outobj.melt:5466:/ checksignal");
		  MELT_CHECK_SIGNAL ();
		  ;
		  /*^apply */
		  /*apply */
		  {
		    union meltparam_un argtab[5];
		    memset (&argtab, 0, sizeof (argtab));
		    /*^apply.arg */
		    argtab[0].meltbp_long =
		      /*_#THE_MELTCALLCOUNT__L20*/ meltfnum[12];
		    /*^apply.arg */
		    argtab[1].meltbp_cstring = "warmelt-outobj.melt";
		    /*^apply.arg */
		    argtab[2].meltbp_long = 5466;
		    /*^apply.arg */
		    argtab[3].meltbp_cstring =
		      "normadeclb_macroexpanded_list usual modinienv=";
		    /*^apply.arg */
		    argtab[4].meltbp_aptr =
		      (melt_ptr_t *) & /*_.MODINIENV__V36*/ meltfptr[35];
		    /*_.MELT_DEBUG_FUN__V58*/ meltfptr[57] =
		      melt_apply ((meltclosure_ptr_t)
				  (( /*!MELT_DEBUG_FUN */ meltfrout->
				    tabval[0])),
				  (melt_ptr_t) (( /*nil */ NULL)),
				  (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_PTR ""), argtab, "",
				  (union meltparam_un *) 0);
		  }
		  ;
		  /*_.IF___V57*/ meltfptr[42] =
		    /*_.MELT_DEBUG_FUN__V58*/ meltfptr[57];;
		  /*epilog */

		  MELT_LOCATION ("warmelt-outobj.melt:5466:/ clear");
		 /*clear *//*_#THE_MELTCALLCOUNT__L20*/ meltfnum[12] = 0;
		  /*^clear */
		 /*clear *//*_.MELT_DEBUG_FUN__V58*/ meltfptr[57] = 0;
		}
		;
	      }
	    else
	      {			/*^cond.else */

      /*_.IF___V57*/ meltfptr[42] = NULL;;
	      }
	    ;
	    MELT_LOCATION ("warmelt-outobj.melt:5466:/ quasiblock");


	    /*_.PROGN___V59*/ meltfptr[57] = /*_.IF___V57*/ meltfptr[42];;
	    /*^compute */
	    /*_.IFCPP___V56*/ meltfptr[50] = /*_.PROGN___V59*/ meltfptr[57];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-outobj.melt:5466:/ clear");
	       /*clear *//*_#MELT_NEED_DBG__L19*/ meltfnum[17] = 0;
	    /*^clear */
	       /*clear *//*_.IF___V57*/ meltfptr[42] = 0;
	    /*^clear */
	       /*clear *//*_.PROGN___V59*/ meltfptr[57] = 0;
	  }

#else /*MELT_HAVE_DEBUG */
	  /*^cppif.else */
	  /*_.IFCPP___V56*/ meltfptr[50] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:5467:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t)
				 ( /*_.DECLBUF__V30*/ meltfptr[28]),
				 ("/* ordinary MELT module */"));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:5468:/ locexp");
	    meltgc_strbuf_add_indent ((melt_ptr_t)
				      ( /*_.DECLBUF__V30*/ meltfptr[28]), (0),
				      0);
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:5469:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t)
				 ( /*_.DECLBUF__V30*/ meltfptr[28]),
				 ("#define MELT_HAS_INITIAL_ENVIRONMENT 1 /*usual*/"));
	  }
	  ;
	  MELT_LOCATION ("warmelt-outobj.melt:5465:/ quasiblock");


	  /*epilog */

	  MELT_LOCATION ("warmelt-outobj.melt:5464:/ clear");
	     /*clear *//*_.IFCPP___V56*/ meltfptr[50] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

	/*^block */
	/*anyblock */
	{


#if MELT_HAVE_DEBUG
	  MELT_LOCATION ("warmelt-outobj.melt:5471:/ cppif.then");
	  /*^block */
	  /*anyblock */
	  {


	    {
	      /*^locexp */
	      /*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	      melt_dbgcounter++;
#endif
	      ;
	    }
	    ;
	    /*^checksignal */
	    MELT_CHECK_SIGNAL ();
	    ;
     /*_#MELT_NEED_DBG__L21*/ meltfnum[12] =
	      /*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	      ( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	      0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	      ;;
	    MELT_LOCATION ("warmelt-outobj.melt:5471:/ cond");
	    /*cond */ if ( /*_#MELT_NEED_DBG__L21*/ meltfnum[12])	/*then */
	      {
		/*^cond.then */
		/*^block */
		/*anyblock */
		{

       /*_#THE_MELTCALLCOUNT__L22*/ meltfnum[17] =
#ifdef meltcallcount
		    meltcallcount	/* the_meltcallcount */
#else
		    0L
#endif /* meltcallcount the_meltcallcount */
		    ;;
		  MELT_LOCATION ("warmelt-outobj.melt:5471:/ checksignal");
		  MELT_CHECK_SIGNAL ();
		  ;
		  /*^apply */
		  /*apply */
		  {
		    union meltparam_un argtab[5];
		    memset (&argtab, 0, sizeof (argtab));
		    /*^apply.arg */
		    argtab[0].meltbp_long =
		      /*_#THE_MELTCALLCOUNT__L22*/ meltfnum[17];
		    /*^apply.arg */
		    argtab[1].meltbp_cstring = "warmelt-outobj.melt";
		    /*^apply.arg */
		    argtab[2].meltbp_long = 5471;
		    /*^apply.arg */
		    argtab[3].meltbp_cstring =
		      "normadeclb_macroexpanded_list initial modinienv=";
		    /*^apply.arg */
		    argtab[4].meltbp_aptr =
		      (melt_ptr_t *) & /*_.MODINIENV__V36*/ meltfptr[35];
		    /*_.MELT_DEBUG_FUN__V62*/ meltfptr[50] =
		      melt_apply ((meltclosure_ptr_t)
				  (( /*!MELT_DEBUG_FUN */ meltfrout->
				    tabval[0])),
				  (melt_ptr_t) (( /*nil */ NULL)),
				  (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_PTR ""), argtab, "",
				  (union meltparam_un *) 0);
		  }
		  ;
		  /*_.IF___V61*/ meltfptr[57] =
		    /*_.MELT_DEBUG_FUN__V62*/ meltfptr[50];;
		  /*epilog */

		  MELT_LOCATION ("warmelt-outobj.melt:5471:/ clear");
		 /*clear *//*_#THE_MELTCALLCOUNT__L22*/ meltfnum[17] = 0;
		  /*^clear */
		 /*clear *//*_.MELT_DEBUG_FUN__V62*/ meltfptr[50] = 0;
		}
		;
	      }
	    else
	      {			/*^cond.else */

      /*_.IF___V61*/ meltfptr[57] = NULL;;
	      }
	    ;
	    MELT_LOCATION ("warmelt-outobj.melt:5471:/ quasiblock");


	    /*_.PROGN___V63*/ meltfptr[50] = /*_.IF___V61*/ meltfptr[57];;
	    /*^compute */
	    /*_.IFCPP___V60*/ meltfptr[42] = /*_.PROGN___V63*/ meltfptr[50];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-outobj.melt:5471:/ clear");
	       /*clear *//*_#MELT_NEED_DBG__L21*/ meltfnum[12] = 0;
	    /*^clear */
	       /*clear *//*_.IF___V61*/ meltfptr[57] = 0;
	    /*^clear */
	       /*clear *//*_.PROGN___V63*/ meltfptr[50] = 0;
	  }

#else /*MELT_HAVE_DEBUG */
	  /*^cppif.else */
	  /*_.IFCPP___V60*/ meltfptr[42] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:5472:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t)
				 ( /*_.DECLBUF__V30*/ meltfptr[28]),
				 ("/* initial MELT module */"));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:5473:/ locexp");
	    meltgc_strbuf_add_indent ((melt_ptr_t)
				      ( /*_.DECLBUF__V30*/ meltfptr[28]), (0),
				      0);
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:5474:/ locexp");

	    /* CHECK_WARMELT_FIRST_BOOTSTRAPPING_CHUNK__1 */
	    melt_checkmsg ("bootstrapping first file",
			   melt_flag_bootstrapping
			   && melt_string_str ((melt_ptr_t)
					       /*_.MODNAMSTR__V3*/
					       meltfptr[2])
			   &&
			   strstr (melt_string_str
				   ((melt_ptr_t) /*_.MODNAMSTR__V3*/
				    meltfptr[2]), "first"));
	    ;
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:5482:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t)
				 ( /*_.DECLBUF__V30*/ meltfptr[28]),
				 ("#define MELT_HAS_INITIAL_ENVIRONMENT 0 /*initial*/"));
	  }
	  ;
	  MELT_LOCATION ("warmelt-outobj.melt:5470:/ quasiblock");


	  /*epilog */

	  MELT_LOCATION ("warmelt-outobj.melt:5464:/ clear");
	     /*clear *//*_.IFCPP___V60*/ meltfptr[42] = 0;
	}
	;
      }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:5484:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t)
				( /*_.DECLBUF__V30*/ meltfptr[28]), (0), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:5485:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t)
				( /*_.DECLBUF__V30*/ meltfptr[28]), (0), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:5486:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.DECLBUF__V30*/ meltfptr[28]),
			   ("struct melt_callframe_st; /*defined in melt-runtime.h*/"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:5487:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t)
				( /*_.DECLBUF__V30*/ meltfptr[28]), (0), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:5488:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t)
				( /*_.DECLBUF__V30*/ meltfptr[28]), (0), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:5489:/ locexp");
      meltgc_prepend_list ((melt_ptr_t) ( /*_.XLIST__V2*/ meltfptr[1]),
			   (melt_ptr_t) ( /*_.UCMEB1__V39*/ meltfptr[38]));
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:5494:/ quasiblock");


    /*^newclosure */
		 /*newclosure *//*_.LAMBDA___V65*/ meltfptr[50] =
      (melt_ptr_t)
      meltgc_new_closure ((meltobject_ptr_t)
			  (((melt_ptr_t) (MELT_PREDEF (DISCR_CLOSURE)))),
			  (meltroutine_ptr_t) (( /*!konst_18 */ meltfrout->
						tabval[18])), (6));
    ;
    /*^putclosedv */
    /*putclosv */
    melt_assertmsg ("putclosv checkclo",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.LAMBDA___V65*/ meltfptr[50])) ==
		    MELTOBMAG_CLOSURE);
    melt_assertmsg ("putclosv checkoff", 0 >= 0
		    && 0 <
		    melt_closure_size ((melt_ptr_t)
				       ( /*_.LAMBDA___V65*/ meltfptr[50])));
    ((meltclosure_ptr_t) /*_.LAMBDA___V65*/ meltfptr[50])->tabval[0] =
      (melt_ptr_t) ( /*_.DECLBUF__V30*/ meltfptr[28]);
    ;
    /*^putclosedv */
    /*putclosv */
    melt_assertmsg ("putclosv checkclo",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.LAMBDA___V65*/ meltfptr[50])) ==
		    MELTOBMAG_CLOSURE);
    melt_assertmsg ("putclosv checkoff", 1 >= 0
		    && 1 <
		    melt_closure_size ((melt_ptr_t)
				       ( /*_.LAMBDA___V65*/ meltfptr[50])));
    ((meltclosure_ptr_t) /*_.LAMBDA___V65*/ meltfptr[50])->tabval[1] =
      (melt_ptr_t) ( /*_.INIENV__V6*/ meltfptr[5]);
    ;
    /*^putclosedv */
    /*putclosv */
    melt_assertmsg ("putclosv checkclo",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.LAMBDA___V65*/ meltfptr[50])) ==
		    MELTOBMAG_CLOSURE);
    melt_assertmsg ("putclosv checkoff", 2 >= 0
		    && 2 <
		    melt_closure_size ((melt_ptr_t)
				       ( /*_.LAMBDA___V65*/ meltfptr[50])));
    ((meltclosure_ptr_t) /*_.LAMBDA___V65*/ meltfptr[50])->tabval[2] =
      (melt_ptr_t) ( /*_.NCX__V5*/ meltfptr[4]);
    ;
    /*^putclosedv */
    /*putclosv */
    melt_assertmsg ("putclosv checkclo",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.LAMBDA___V65*/ meltfptr[50])) ==
		    MELTOBMAG_CLOSURE);
    melt_assertmsg ("putclosv checkoff", 3 >= 0
		    && 3 <
		    melt_closure_size ((melt_ptr_t)
				       ( /*_.LAMBDA___V65*/ meltfptr[50])));
    ((meltclosure_ptr_t) /*_.LAMBDA___V65*/ meltfptr[50])->tabval[3] =
      (melt_ptr_t) ( /*_.INIPROC__V37*/ meltfptr[36]);
    ;
    /*^putclosedv */
    /*putclosv */
    melt_assertmsg ("putclosv checkclo",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.LAMBDA___V65*/ meltfptr[50])) ==
		    MELTOBMAG_CLOSURE);
    melt_assertmsg ("putclosv checkoff", 4 >= 0
		    && 4 <
		    melt_closure_size ((melt_ptr_t)
				       ( /*_.LAMBDA___V65*/ meltfptr[50])));
    ((meltclosure_ptr_t) /*_.LAMBDA___V65*/ meltfptr[50])->tabval[4] =
      (melt_ptr_t) ( /*_.NORMLIST__V26*/ meltfptr[22]);
    ;
    /*^putclosedv */
    /*putclosv */
    melt_assertmsg ("putclosv checkclo",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.LAMBDA___V65*/ meltfptr[50])) ==
		    MELTOBMAG_CLOSURE);
    melt_assertmsg ("putclosv checkoff", 5 >= 0
		    && 5 <
		    melt_closure_size ((melt_ptr_t)
				       ( /*_.LAMBDA___V65*/ meltfptr[50])));
    ((meltclosure_ptr_t) /*_.LAMBDA___V65*/ meltfptr[50])->tabval[5] =
      (melt_ptr_t) ( /*_.BINDLIST__V28*/ meltfptr[26]);
    ;
    /*_.LAMBDA___V64*/ meltfptr[57] = /*_.LAMBDA___V65*/ meltfptr[50];;
    MELT_LOCATION ("warmelt-outobj.melt:5492:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & /*_.LAMBDA___V64*/ meltfptr[57];
      /*_.LIST_EVERY__V66*/ meltfptr[42] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!LIST_EVERY */ meltfrout->tabval[10])),
		    (melt_ptr_t) ( /*_.XLIST__V2*/ meltfptr[1]),
		    (MELTBPARSTR_PTR ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:5527:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L23*/ meltfnum[17] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-outobj.melt:5527:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L23*/ meltfnum[17])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L24*/ meltfnum[12] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-outobj.melt:5527:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[7];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long =
		/*_#THE_MELTCALLCOUNT__L24*/ meltfnum[12];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-outobj.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 5527;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring =
		"normadeclb_macroexpanded_list @@cheadlist=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.CHEADLIST__V38*/ meltfptr[37];
	      /*^apply.arg */
	      argtab[5].meltbp_cstring = "\n modctx=";
	      /*^apply.arg */
	      argtab[6].meltbp_aptr =
		(melt_ptr_t *) & /*_.MODCTX__V4*/ meltfptr[3];
	      /*_.MELT_DEBUG_FUN__V69*/ meltfptr[68] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V68*/ meltfptr[67] =
	      /*_.MELT_DEBUG_FUN__V69*/ meltfptr[68];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-outobj.melt:5527:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L24*/ meltfnum[12] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V69*/ meltfptr[68] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V68*/ meltfptr[67] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-outobj.melt:5527:/ quasiblock");


      /*_.PROGN___V70*/ meltfptr[68] = /*_.IF___V68*/ meltfptr[67];;
      /*^compute */
      /*_.IFCPP___V67*/ meltfptr[66] = /*_.PROGN___V70*/ meltfptr[68];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:5527:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L23*/ meltfnum[17] = 0;
      /*^clear */
	     /*clear *//*_.IF___V68*/ meltfptr[67] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V70*/ meltfptr[68] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V67*/ meltfptr[66] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-outobj.melt:5529:/ quasiblock");


    MELT_LOCATION ("warmelt-outobj.melt:5530:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & ( /*!DISCR_MULTIPLE */ meltfrout->tabval[20]);
      /*_.CHEADTUP__V72*/ meltfptr[68] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!LIST_TO_MULTIPLE */ meltfrout->tabval[19])),
		    (melt_ptr_t) ( /*_.CHEADLIST__V38*/ meltfptr[37]),
		    (MELTBPARSTR_PTR ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;
 /*_#NBCHEAD__L25*/ meltfnum[12] =
      (melt_multiple_length
       ((melt_ptr_t) ( /*_.CHEADTUP__V72*/ meltfptr[68])));;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:5533:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L26*/ meltfnum[17] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-outobj.melt:5533:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L26*/ meltfnum[17])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L27*/ meltfnum[26] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-outobj.melt:5533:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[9];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long =
		/*_#THE_MELTCALLCOUNT__L27*/ meltfnum[26];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-outobj.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 5533;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring =
		"normadeclb_macroexpanded_list cheadtup=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.CHEADTUP__V72*/ meltfptr[68];
	      /*^apply.arg */
	      argtab[5].meltbp_cstring = "\n nbchead=";
	      /*^apply.arg */
	      argtab[6].meltbp_long = /*_#NBCHEAD__L25*/ meltfnum[12];
	      /*^apply.arg */
	      argtab[7].meltbp_cstring = "\n @@cheadlist=";
	      /*^apply.arg */
	      argtab[8].meltbp_aptr =
		(melt_ptr_t *) & /*_.CHEADLIST__V38*/ meltfptr[37];
	      /*_.MELT_DEBUG_FUN__V75*/ meltfptr[74] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V74*/ meltfptr[73] =
	      /*_.MELT_DEBUG_FUN__V75*/ meltfptr[74];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-outobj.melt:5533:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L27*/ meltfnum[26] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V75*/ meltfptr[74] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V74*/ meltfptr[73] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-outobj.melt:5533:/ quasiblock");


      /*_.PROGN___V76*/ meltfptr[74] = /*_.IF___V74*/ meltfptr[73];;
      /*^compute */
      /*_.IFCPP___V73*/ meltfptr[72] = /*_.PROGN___V76*/ meltfptr[74];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:5533:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L26*/ meltfnum[17] = 0;
      /*^clear */
	     /*clear *//*_.IF___V74*/ meltfptr[73] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V76*/ meltfptr[74] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V73*/ meltfptr[72] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-outobj.melt:5535:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#I__L28*/ meltfnum[26] =
      (( /*_#NBCHEAD__L25*/ meltfnum[12]) > (0));;
    MELT_LOCATION ("warmelt-outobj.melt:5535:/ cond");
    /*cond */ if ( /*_#I__L28*/ meltfnum[26])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{


	  {
	    MELT_LOCATION ("warmelt-outobj.melt:5537:/ locexp");
	    meltgc_strbuf_add_indent ((melt_ptr_t)
				      ( /*_.DECLBUF__V30*/ meltfptr[28]), (0),
				      0);
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:5538:/ locexp");
	    meltgc_strbuf_add_indent ((melt_ptr_t)
				      ( /*_.DECLBUF__V30*/ meltfptr[28]), (0),
				      0);
	  }
	  ;
	  MELT_LOCATION ("warmelt-outobj.melt:5539:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^apply */
	  /*apply */
	  {
	    union meltparam_un argtab[3];
	    memset (&argtab, 0, sizeof (argtab));
	    /*^apply.arg */
	    argtab[0].meltbp_cstring = "/***** ";
	    /*^apply.arg */
	    argtab[1].meltbp_long = /*_#NBCHEAD__L25*/ meltfnum[12];
	    /*^apply.arg */
	    argtab[2].meltbp_cstring = " extra C headers *****/";
	    /*_.ADD2OUT__V77*/ meltfptr[73] =
	      melt_apply ((meltclosure_ptr_t)
			  (( /*!ADD2OUT */ meltfrout->tabval[21])),
			  (melt_ptr_t) ( /*_.DECLBUF__V30*/ meltfptr[28]),
			  (MELTBPARSTR_CSTRING MELTBPARSTR_LONG
			   MELTBPARSTR_CSTRING ""), argtab, "",
			  (union meltparam_un *) 0);
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:5540:/ locexp");
	    meltgc_strbuf_add_indent ((melt_ptr_t)
				      ( /*_.DECLBUF__V30*/ meltfptr[28]), (0),
				      0);
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:5541:/ locexp");
	    meltgc_strbuf_add_indent ((melt_ptr_t)
				      ( /*_.DECLBUF__V30*/ meltfptr[28]), (0),
				      0);
	  }
	  ;
	  /*citerblock FOREACH_IN_MULTIPLE */
	  {
	    /* start foreach_in_multiple meltcit1__EACHTUP */
	    long meltcit1__EACHTUP_ln =
	      melt_multiple_length ((melt_ptr_t) /*_.CHEADTUP__V72*/
				    meltfptr[68]);
	    for ( /*_#HIX__L29*/ meltfnum[17] = 0;
		 ( /*_#HIX__L29*/ meltfnum[17] >= 0)
		 && ( /*_#HIX__L29*/ meltfnum[17] < meltcit1__EACHTUP_ln);
	/*_#HIX__L29*/ meltfnum[17]++)
	      {
		/*_.CURCHEAD__V78*/ meltfptr[74] =
		  melt_multiple_nth ((melt_ptr_t)
				     ( /*_.CHEADTUP__V72*/ meltfptr[68]),
				     /*_#HIX__L29*/ meltfnum[17]);




#if MELT_HAVE_DEBUG
		MELT_LOCATION ("warmelt-outobj.melt:5545:/ cppif.then");
		/*^block */
		/*anyblock */
		{


		  {
		    /*^locexp */
		    /*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
		    melt_dbgcounter++;
#endif
		    ;
		  }
		  ;
		  /*^checksignal */
		  MELT_CHECK_SIGNAL ();
		  ;
      /*_#MELT_NEED_DBG__L30*/ meltfnum[29] =
		    /*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
		    ( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
		    0		/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
		    ;;
		  MELT_LOCATION ("warmelt-outobj.melt:5545:/ cond");
		  /*cond */ if ( /*_#MELT_NEED_DBG__L30*/ meltfnum[29])	/*then */
		    {
		      /*^cond.then */
		      /*^block */
		      /*anyblock */
		      {

	/*_#THE_MELTCALLCOUNT__L31*/ meltfnum[30] =
#ifdef meltcallcount
			  meltcallcount	/* the_meltcallcount */
#else
			  0L
#endif /* meltcallcount the_meltcallcount */
			  ;;
			MELT_LOCATION
			  ("warmelt-outobj.melt:5545:/ checksignal");
			MELT_CHECK_SIGNAL ();
			;
			/*^apply */
			/*apply */
			{
			  union meltparam_un argtab[7];
			  memset (&argtab, 0, sizeof (argtab));
			  /*^apply.arg */
			  argtab[0].meltbp_long =
			    /*_#THE_MELTCALLCOUNT__L31*/ meltfnum[30];
			  /*^apply.arg */
			  argtab[1].meltbp_cstring = "warmelt-outobj.melt";
			  /*^apply.arg */
			  argtab[2].meltbp_long = 5545;
			  /*^apply.arg */
			  argtab[3].meltbp_cstring =
			    "normadeclb_macroexpanded_list curchead=";
			  /*^apply.arg */
			  argtab[4].meltbp_aptr =
			    (melt_ptr_t *) & /*_.CURCHEAD__V78*/ meltfptr[74];
			  /*^apply.arg */
			  argtab[5].meltbp_cstring = " hix=";
			  /*^apply.arg */
			  argtab[6].meltbp_long = /*_#HIX__L29*/ meltfnum[17];
			  /*_.MELT_DEBUG_FUN__V81*/ meltfptr[80] =
			    melt_apply ((meltclosure_ptr_t)
					(( /*!MELT_DEBUG_FUN */ meltfrout->
					  tabval[0])),
					(melt_ptr_t) (( /*nil */ NULL)),
					(MELTBPARSTR_LONG MELTBPARSTR_CSTRING
					 MELTBPARSTR_LONG MELTBPARSTR_CSTRING
					 MELTBPARSTR_PTR MELTBPARSTR_CSTRING
					 MELTBPARSTR_LONG ""), argtab, "",
					(union meltparam_un *) 0);
			}
			;
			/*_.IF___V80*/ meltfptr[79] =
			  /*_.MELT_DEBUG_FUN__V81*/ meltfptr[80];;
			/*epilog */

			MELT_LOCATION ("warmelt-outobj.melt:5545:/ clear");
		  /*clear *//*_#THE_MELTCALLCOUNT__L31*/ meltfnum[30] =
			  0;
			/*^clear */
		  /*clear *//*_.MELT_DEBUG_FUN__V81*/ meltfptr[80] = 0;
		      }
		      ;
		    }
		  else
		    {		/*^cond.else */

       /*_.IF___V80*/ meltfptr[79] = NULL;;
		    }
		  ;
		  MELT_LOCATION ("warmelt-outobj.melt:5545:/ quasiblock");


		  /*_.PROGN___V82*/ meltfptr[80] =
		    /*_.IF___V80*/ meltfptr[79];;
		  /*^compute */
		  /*_.IFCPP___V79*/ meltfptr[78] =
		    /*_.PROGN___V82*/ meltfptr[80];;
		  /*epilog */

		  MELT_LOCATION ("warmelt-outobj.melt:5545:/ clear");
		/*clear *//*_#MELT_NEED_DBG__L30*/ meltfnum[29] = 0;
		  /*^clear */
		/*clear *//*_.IF___V80*/ meltfptr[79] = 0;
		  /*^clear */
		/*clear *//*_.PROGN___V82*/ meltfptr[80] = 0;
		}

#else /*MELT_HAVE_DEBUG */
		/*^cppif.else */
		/*_.IFCPP___V79*/ meltfptr[78] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
		;

#if MELT_HAVE_DEBUG
		MELT_LOCATION ("warmelt-outobj.melt:5546:/ cppif.then");
		/*^block */
		/*anyblock */
		{

		  /*^checksignal */
		  MELT_CHECK_SIGNAL ();
		  ;
      /*_#IS_A__L32*/ meltfnum[30] =
		    melt_is_instance_of ((melt_ptr_t)
					 ( /*_.CURCHEAD__V78*/ meltfptr[74]),
					 (melt_ptr_t) (( /*!CLASS_SOURCE_CHEADER */ meltfrout->tabval[22])));;
		  MELT_LOCATION ("warmelt-outobj.melt:5546:/ cond");
		  /*cond */ if ( /*_#IS_A__L32*/ meltfnum[30])	/*then */
		    {
		      /*^cond.then */
		      /*_.IFELSE___V84*/ meltfptr[80] = ( /*nil */ NULL);;
		    }
		  else
		    {
		      MELT_LOCATION ("warmelt-outobj.melt:5546:/ cond.else");

		      /*^block */
		      /*anyblock */
		      {




			{
			  /*^locexp */
			  melt_assert_failed (("check curchead"),
					      ("warmelt-outobj.melt")
					      ? ("warmelt-outobj.melt") :
					      __FILE__,
					      (5546) ? (5546) : __LINE__,
					      __FUNCTION__);
			  ;
			}
			;
		  /*clear *//*_.IFELSE___V84*/ meltfptr[80] = 0;
			/*epilog */
		      }
		      ;
		    }
		  ;
		  /*^compute */
		  /*_.IFCPP___V83*/ meltfptr[79] =
		    /*_.IFELSE___V84*/ meltfptr[80];;
		  /*epilog */

		  MELT_LOCATION ("warmelt-outobj.melt:5546:/ clear");
		/*clear *//*_#IS_A__L32*/ meltfnum[30] = 0;
		  /*^clear */
		/*clear *//*_.IFELSE___V84*/ meltfptr[80] = 0;
		}

#else /*MELT_HAVE_DEBUG */
		/*^cppif.else */
		/*_.IFCPP___V83*/ meltfptr[79] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
		;

		{
		  MELT_LOCATION ("warmelt-outobj.melt:5547:/ locexp");
		  meltgc_strbuf_add_indent ((melt_ptr_t)
					    ( /*_.DECLBUF__V30*/
					     meltfptr[28]), (0), 0);
		}
		;
		MELT_LOCATION ("warmelt-outobj.melt:5548:/ checksignal");
		MELT_CHECK_SIGNAL ();
		;
		/*^apply */
		/*apply */
		{
		  union meltparam_un argtab[3];
		  memset (&argtab, 0, sizeof (argtab));
		  /*^apply.arg */
		  argtab[0].meltbp_cstring = "/** header #";
		  /*^apply.arg */
		  argtab[1].meltbp_long = /*_#HIX__L29*/ meltfnum[17];
		  /*^apply.arg */
		  argtab[2].meltbp_cstring = ": **/";
		  /*_.ADD2OUT__V85*/ meltfptr[80] =
		    melt_apply ((meltclosure_ptr_t)
				(( /*!ADD2OUT */ meltfrout->tabval[21])),
				(melt_ptr_t) ( /*_.DECLBUF__V30*/
					      meltfptr[28]),
				(MELTBPARSTR_CSTRING MELTBPARSTR_LONG
				 MELTBPARSTR_CSTRING ""), argtab, "",
				(union meltparam_un *) 0);
		}
		;

		{
		  MELT_LOCATION ("warmelt-outobj.melt:5549:/ locexp");
		  meltgc_strbuf_add_indent ((melt_ptr_t)
					    ( /*_.DECLBUF__V30*/
					     meltfptr[28]), (0), 0);
		}
		;
		MELT_LOCATION ("warmelt-outobj.melt:5550:/ quasiblock");


		/*^cond */
		/*cond */ if (
			       /*ifisa */
			       melt_is_instance_of ((melt_ptr_t)
						    ( /*_.CURCHEAD__V78*/
						     meltfptr[74]),
						    (melt_ptr_t) (( /*!CLASS_LOCATED */ meltfrout->tabval[6])))
		  )		/*then */
		  {
		    /*^cond.then */
		    /*^getslot */
		    {
		      melt_ptr_t slot = NULL, obj = NULL;
		      obj =
			(melt_ptr_t) ( /*_.CURCHEAD__V78*/ meltfptr[74])
			/*=obj*/ ;
		      melt_object_get_field (slot, obj, 1, "LOCA_LOCATION");
      /*_.HLOC__V86*/ meltfptr[85] = slot;
		    };
		    ;
		  }
		else
		  {		/*^cond.else */

     /*_.HLOC__V86*/ meltfptr[85] = NULL;;
		  }
		;
		MELT_LOCATION ("warmelt-outobj.melt:5551:/ cond");
		/*cond */ if (
			       /*ifisa */
			       melt_is_instance_of ((melt_ptr_t)
						    ( /*_.CURCHEAD__V78*/
						     meltfptr[74]),
						    (melt_ptr_t) (( /*!CLASS_SOURCE_CHEADER */ meltfrout->tabval[22])))
		  )		/*then */
		  {
		    /*^cond.then */
		    /*^getslot */
		    {
		      melt_ptr_t slot = NULL, obj = NULL;
		      obj =
			(melt_ptr_t) ( /*_.CURCHEAD__V78*/ meltfptr[74])
			/*=obj*/ ;
		      melt_object_get_field (slot, obj, 2,
					     "SCHEADER_CODESTRING");
      /*_.CHSTR__V87*/ meltfptr[86] = slot;
		    };
		    ;
		  }
		else
		  {		/*^cond.else */

     /*_.CHSTR__V87*/ meltfptr[86] = NULL;;
		  }
		;
		MELT_LOCATION ("warmelt-outobj.melt:5553:/ checksignal");
		MELT_CHECK_SIGNAL ();
		;
		/*^cond */
		/*cond */ if ( /*_.HLOC__V86*/ meltfptr[85])	/*then */
		  {
		    /*^cond.then */
		    /*^block */
		    /*anyblock */
		    {

		      MELT_LOCATION
			("warmelt-outobj.melt:5554:/ checksignal");
		      MELT_CHECK_SIGNAL ();
		      ;
		      /*^apply */
		      /*apply */
		      {
			union meltparam_un argtab[3];
			memset (&argtab, 0, sizeof (argtab));
			/*^apply.arg */
			argtab[0].meltbp_aptr =
			  (melt_ptr_t *) & /*_.DECLBUF__V30*/ meltfptr[28];
			/*^apply.arg */
			argtab[1].meltbp_long = 0;
			/*^apply.arg */
			argtab[2].meltbp_cstring = "cheader";
			/*_.OUTPUT_RAW_LOCATION__V89*/ meltfptr[88] =
			  melt_apply ((meltclosure_ptr_t)
				      (( /*!OUTPUT_RAW_LOCATION */ meltfrout->
					tabval[23])),
				      (melt_ptr_t) ( /*_.HLOC__V86*/
						    meltfptr[85]),
				      (MELTBPARSTR_PTR MELTBPARSTR_LONG
				       MELTBPARSTR_CSTRING ""), argtab, "",
				      (union meltparam_un *) 0);
		      }
		      ;
		      /*_.IF___V88*/ meltfptr[87] =
			/*_.OUTPUT_RAW_LOCATION__V89*/ meltfptr[88];;
		      /*epilog */

		      MELT_LOCATION ("warmelt-outobj.melt:5553:/ clear");
		/*clear *//*_.OUTPUT_RAW_LOCATION__V89*/ meltfptr[88] =
			0;
		    }
		    ;
		  }
		else
		  {		/*^cond.else */

     /*_.IF___V88*/ meltfptr[87] = NULL;;
		  }
		;
		MELT_LOCATION ("warmelt-outobj.melt:5555:/ checksignal");
		MELT_CHECK_SIGNAL ();
		;
		/*^apply */
		/*apply */
		{
		  union meltparam_un argtab[1];
		  memset (&argtab, 0, sizeof (argtab));
		  /*^apply.arg */
		  argtab[0].meltbp_aptr =
		    (melt_ptr_t *) & /*_.CHSTR__V87*/ meltfptr[86];
		  /*_.ADD2OUT__V90*/ meltfptr[88] =
		    melt_apply ((meltclosure_ptr_t)
				(( /*!ADD2OUT */ meltfrout->tabval[21])),
				(melt_ptr_t) ( /*_.DECLBUF__V30*/
					      meltfptr[28]),
				(MELTBPARSTR_PTR ""), argtab, "",
				(union meltparam_un *) 0);
		}
		;

		{
		  MELT_LOCATION ("warmelt-outobj.melt:5556:/ locexp");
		  meltgc_strbuf_add_indent ((melt_ptr_t)
					    ( /*_.DECLBUF__V30*/
					     meltfptr[28]), (0), 0);
		}
		;

		MELT_LOCATION ("warmelt-outobj.melt:5550:/ clear");
	      /*clear *//*_.HLOC__V86*/ meltfptr[85] = 0;
		/*^clear */
	      /*clear *//*_.CHSTR__V87*/ meltfptr[86] = 0;
		/*^clear */
	      /*clear *//*_.IF___V88*/ meltfptr[87] = 0;
		/*^clear */
	      /*clear *//*_.ADD2OUT__V90*/ meltfptr[88] = 0;
		if ( /*_#HIX__L29*/ meltfnum[17] < 0)
		  break;
	      }			/* end  foreach_in_multiple meltcit1__EACHTUP */

	    /*citerepilog */

	    MELT_LOCATION ("warmelt-outobj.melt:5542:/ clear");
	      /*clear *//*_.CURCHEAD__V78*/ meltfptr[74] = 0;
	    /*^clear */
	      /*clear *//*_#HIX__L29*/ meltfnum[17] = 0;
	    /*^clear */
	      /*clear *//*_.IFCPP___V79*/ meltfptr[78] = 0;
	    /*^clear */
	      /*clear *//*_.IFCPP___V83*/ meltfptr[79] = 0;
	    /*^clear */
	      /*clear *//*_.ADD2OUT__V85*/ meltfptr[80] = 0;
	  }			/*endciterblock FOREACH_IN_MULTIPLE */
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:5559:/ locexp");
	    meltgc_strbuf_add_indent ((melt_ptr_t)
				      ( /*_.DECLBUF__V30*/ meltfptr[28]), (0),
				      0);
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:5560:/ locexp");
	    meltgc_strbuf_add_indent ((melt_ptr_t)
				      ( /*_.DECLBUF__V30*/ meltfptr[28]), (0),
				      0);
	  }
	  ;
	  MELT_LOCATION ("warmelt-outobj.melt:5561:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^apply */
	  /*apply */
	  {
	    union meltparam_un argtab[3];
	    memset (&argtab, 0, sizeof (argtab));
	    /*^apply.arg */
	    argtab[0].meltbp_cstring = "/***** end of ";
	    /*^apply.arg */
	    argtab[1].meltbp_long = /*_#NBCHEAD__L25*/ meltfnum[12];
	    /*^apply.arg */
	    argtab[2].meltbp_cstring = " extra C headers *****/";
	    /*_.ADD2OUT__V91*/ meltfptr[85] =
	      melt_apply ((meltclosure_ptr_t)
			  (( /*!ADD2OUT */ meltfrout->tabval[21])),
			  (melt_ptr_t) ( /*_.DECLBUF__V30*/ meltfptr[28]),
			  (MELTBPARSTR_CSTRING MELTBPARSTR_LONG
			   MELTBPARSTR_CSTRING ""), argtab, "",
			  (union meltparam_un *) 0);
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:5562:/ locexp");
	    meltgc_strbuf_add_indent ((melt_ptr_t)
				      ( /*_.DECLBUF__V30*/ meltfptr[28]), (0),
				      0);
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:5563:/ locexp");
	    meltgc_strbuf_add_indent ((melt_ptr_t)
				      ( /*_.DECLBUF__V30*/ meltfptr[28]), (0),
				      0);
	  }
	  ;
	  MELT_LOCATION ("warmelt-outobj.melt:5536:/ quasiblock");


	  /*epilog */

	  MELT_LOCATION ("warmelt-outobj.melt:5535:/ clear");
	     /*clear *//*_.ADD2OUT__V77*/ meltfptr[73] = 0;
	  /*^clear */
	     /*clear *//*_.ADD2OUT__V91*/ meltfptr[85] = 0;
	}
	;
      }				/*noelse */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:5565:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L33*/ meltfnum[29] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-outobj.melt:5565:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L33*/ meltfnum[29])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L34*/ meltfnum[30] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-outobj.melt:5565:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long =
		/*_#THE_MELTCALLCOUNT__L34*/ meltfnum[30];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-outobj.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 5565;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring =
		"normadeclb_macroexpanded_list  @@cheadlist=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.CHEADLIST__V38*/ meltfptr[37];
	      /*_.MELT_DEBUG_FUN__V94*/ meltfptr[88] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V93*/ meltfptr[87] =
	      /*_.MELT_DEBUG_FUN__V94*/ meltfptr[88];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-outobj.melt:5565:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L34*/ meltfnum[30] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V94*/ meltfptr[88] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V93*/ meltfptr[87] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-outobj.melt:5565:/ quasiblock");


      /*_.PROGN___V95*/ meltfptr[73] = /*_.IF___V93*/ meltfptr[87];;
      /*^compute */
      /*_.IFCPP___V92*/ meltfptr[86] = /*_.PROGN___V95*/ meltfptr[73];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:5565:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L33*/ meltfnum[29] = 0;
      /*^clear */
	     /*clear *//*_.IF___V93*/ meltfptr[87] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V95*/ meltfptr[73] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V92*/ meltfptr[86] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    /*^compute */
    /*_.LET___V71*/ meltfptr[67] = /*_.IFCPP___V92*/ meltfptr[86];;

    MELT_LOCATION ("warmelt-outobj.melt:5529:/ clear");
	   /*clear *//*_.CHEADTUP__V72*/ meltfptr[68] = 0;
    /*^clear */
	   /*clear *//*_#NBCHEAD__L25*/ meltfnum[12] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V73*/ meltfptr[72] = 0;
    /*^clear */
	   /*clear *//*_#I__L28*/ meltfnum[26] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V92*/ meltfptr[86] = 0;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:5568:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L35*/ meltfnum[30] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.INIPROC__V37*/ meltfptr[36]),
			     (melt_ptr_t) (( /*!CLASS_NREP_INITPROC */
					    meltfrout->tabval[24])));;
      MELT_LOCATION ("warmelt-outobj.melt:5568:/ cond");
      /*cond */ if ( /*_#IS_A__L35*/ meltfnum[30])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V97*/ meltfptr[88] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-outobj.melt:5568:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check iniproc"),
				  ("warmelt-outobj.melt")
				  ? ("warmelt-outobj.melt") : __FILE__,
				  (5568) ? (5568) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V97*/ meltfptr[88] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V96*/ meltfptr[85] = /*_.IFELSE___V97*/ meltfptr[88];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:5568:/ clear");
	     /*clear *//*_#IS_A__L35*/ meltfnum[30] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V97*/ meltfptr[88] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V96*/ meltfptr[85] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:5569:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_LIST__L36*/ meltfnum[29] =
	(melt_magic_discr ((melt_ptr_t) ( /*_.XLIST__V2*/ meltfptr[1])) ==
	 MELTOBMAG_LIST);;
      MELT_LOCATION ("warmelt-outobj.melt:5569:/ cond");
      /*cond */ if ( /*_#IS_LIST__L36*/ meltfnum[29])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V99*/ meltfptr[73] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-outobj.melt:5569:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check xlist"),
				  ("warmelt-outobj.melt")
				  ? ("warmelt-outobj.melt") : __FILE__,
				  (5569) ? (5569) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V99*/ meltfptr[73] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V98*/ meltfptr[87] = /*_.IFELSE___V99*/ meltfptr[73];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:5569:/ clear");
	     /*clear *//*_#IS_LIST__L36*/ meltfnum[29] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V99*/ meltfptr[73] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V98*/ meltfptr[87] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:5571:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L37*/ meltfnum[12] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-outobj.melt:5571:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L37*/ meltfnum[12])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L38*/ meltfnum[26] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-outobj.melt:5571:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[11];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long =
		/*_#THE_MELTCALLCOUNT__L38*/ meltfnum[26];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-outobj.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 5571;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring =
		"normadeclb_macroexpanded_list gives normlist=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.NORMLIST__V26*/ meltfptr[22];
	      /*^apply.arg */
	      argtab[5].meltbp_cstring = "\n bindlist=";
	      /*^apply.arg */
	      argtab[6].meltbp_aptr =
		(melt_ptr_t *) & /*_.BINDLIST__V28*/ meltfptr[26];
	      /*^apply.arg */
	      argtab[7].meltbp_cstring = "\n declbuf=";
	      /*^apply.arg */
	      argtab[8].meltbp_aptr =
		(melt_ptr_t *) & /*_.DECLBUF__V30*/ meltfptr[28];
	      /*^apply.arg */
	      argtab[9].meltbp_cstring = "\n startclock=";
	      /*^apply.arg */
	      argtab[10].meltbp_long = /*_#STARTCLOCK__L10*/ meltfnum[1];
	      /*_.MELT_DEBUG_FUN__V102*/ meltfptr[86] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V101*/ meltfptr[72] =
	      /*_.MELT_DEBUG_FUN__V102*/ meltfptr[86];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-outobj.melt:5571:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L38*/ meltfnum[26] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V102*/ meltfptr[86] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V101*/ meltfptr[72] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-outobj.melt:5571:/ quasiblock");


      /*_.PROGN___V103*/ meltfptr[88] = /*_.IF___V101*/ meltfptr[72];;
      /*^compute */
      /*_.IFCPP___V100*/ meltfptr[68] = /*_.PROGN___V103*/ meltfptr[88];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:5571:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L37*/ meltfnum[12] = 0;
      /*^clear */
	     /*clear *//*_.IF___V101*/ meltfptr[72] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V103*/ meltfptr[88] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V100*/ meltfptr[68] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-outobj.melt:5575:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.NORMLIST__V26*/ meltfptr[22];;
    MELT_LOCATION ("warmelt-outobj.melt:5575:/ putxtraresult");
    if (!meltxrestab_ || !meltxresdescr_)
      goto labend_rout;
    if (meltxresdescr_[0] != MELTBPAR_PTR)
      goto labend_rout;
    if (meltxrestab_[0].meltbp_aptr)
      *(meltxrestab_[0].meltbp_aptr) =
	(melt_ptr_t) ( /*_.BINDLIST__V28*/ meltfptr[26]);
    ;
    /*^putxtraresult */
    if (!meltxrestab_ || !meltxresdescr_)
      goto labend_rout;
    if (meltxresdescr_[1] != MELTBPAR_PTR)
      goto labend_rout;
    if (meltxrestab_[1].meltbp_aptr)
      *(meltxrestab_[1].meltbp_aptr) =
	(melt_ptr_t) ( /*_.DECLBUF__V30*/ meltfptr[28]);
    ;
    /*^putxtraresult */
    if (!meltxrestab_ || !meltxresdescr_)
      goto labend_rout;
    if (meltxresdescr_[2] != MELTBPAR_LONG)
      goto labend_rout;
    if (meltxrestab_[2].meltbp_longptr)
      *(meltxrestab_[2].meltbp_longptr) =
	( /*_#STARTCLOCK__L10*/ meltfnum[1]);
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*_.LET___V25*/ meltfptr[21] = /*_.RETURN___V104*/ meltfptr[73];;

    MELT_LOCATION ("warmelt-outobj.melt:5404:/ clear");
	   /*clear *//*_#STARTCLOCK__L10*/ meltfnum[1] = 0;
    /*^clear */
	   /*clear *//*_.NORMLIST__V26*/ meltfptr[22] = 0;
    /*^clear */
	   /*clear *//*_.BINDLIST__V28*/ meltfptr[26] = 0;
    /*^clear */
	   /*clear *//*_.DECLBUF__V30*/ meltfptr[28] = 0;
    /*^clear */
	   /*clear *//*_.LIST_FIRST__V31*/ meltfptr[30] = 0;
    /*^clear */
	   /*clear *//*_.FIRSTX__V32*/ meltfptr[31] = 0;
    /*^clear */
	   /*clear *//*_#IS_A__L11*/ meltfnum[0] = 0;
    /*^clear */
	   /*clear *//*_.FIRSTLOC__V33*/ meltfptr[32] = 0;
    /*^clear */
	   /*clear *//*_.ERRORHDLR__V35*/ meltfptr[33] = 0;
    /*^clear */
	   /*clear *//*_.MODINIENV__V36*/ meltfptr[35] = 0;
    /*^clear */
	   /*clear *//*_.INIPROC__V37*/ meltfptr[36] = 0;
    /*^clear */
	   /*clear *//*_.CHEADLIST__V38*/ meltfptr[37] = 0;
    /*^clear */
	   /*clear *//*_.UCMEB1__V39*/ meltfptr[38] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V41*/ meltfptr[40] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V45*/ meltfptr[41] = 0;
    /*^clear */
	   /*clear *//*_#IS_A__L16*/ meltfnum[11] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V52*/ meltfptr[46] = 0;
    /*^clear */
	   /*clear *//*_.LAMBDA___V64*/ meltfptr[57] = 0;
    /*^clear */
	   /*clear *//*_.LIST_EVERY__V66*/ meltfptr[42] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V67*/ meltfptr[66] = 0;
    /*^clear */
	   /*clear *//*_.LET___V71*/ meltfptr[67] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V96*/ meltfptr[85] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V98*/ meltfptr[87] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V100*/ meltfptr[68] = 0;
    /*^clear */
	   /*clear *//*_.RETURN___V104*/ meltfptr[73] = 0;
    MELT_LOCATION ("warmelt-outobj.melt:5392:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.LET___V25*/ meltfptr[21];;

    {
      MELT_LOCATION ("warmelt-outobj.melt:5392:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V7*/ meltfptr[6] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V11*/ meltfptr[7] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V13*/ meltfptr[8] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V15*/ meltfptr[13] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V17*/ meltfptr[15] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V19*/ meltfptr[17] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V21*/ meltfptr[19] = 0;
    /*^clear */
	   /*clear *//*_.LET___V25*/ meltfptr[21] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("NORMADECLB_MACROEXPANDED_LIST", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_131_warmelt_outobj_NORMADECLB_MACROEXPANDED_LIST_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_131_warmelt_outobj_NORMADECLB_MACROEXPANDED_LIST */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_132_warmelt_outobj_LAMBDA___26__ (meltclosure_ptr_t meltclosp_,
					   melt_ptr_t meltfirstargp_,
					   const melt_argdescr_cell_t
					   meltxargdescr_[],
					   union meltparam_un * meltxargtab_,
					   const melt_argdescr_cell_t
					   meltxresdescr_[],
					   union meltparam_un * meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_132_warmelt_outobj_LAMBDA___26___melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_132_warmelt_outobj_LAMBDA___26___st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 22
    melt_ptr_t mcfr_varptr[22];
#define MELTFRAM_NBVARNUM 13
    long mcfr_varnum[13];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_132_warmelt_outobj_LAMBDA___26__ is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_132_warmelt_outobj_LAMBDA___26___st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 22; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_132_warmelt_outobj_LAMBDA___26__ nbval 22*/
  meltfram__.mcfr_nbvar = 22 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("LAMBDA_", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-outobj.melt:5494:/ getarg");
 /*_.SEXP__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_LONG)
    goto lab_endgetargs;
 /*_#IX__L1*/ meltfnum[0] = meltxargtab_[0].meltbp_long;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:5495:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L2*/ meltfnum[1] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-outobj.melt:5495:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L2*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L3*/ meltfnum[2] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-outobj.melt:5495:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[7];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L3*/ meltfnum[2];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-outobj.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 5495;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring =
		"normadeclb_macroexpanded_list sexp=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.SEXP__V2*/ meltfptr[1];
	      /*^apply.arg */
	      argtab[5].meltbp_cstring = " ix=";
	      /*^apply.arg */
	      argtab[6].meltbp_long = /*_#IX__L1*/ meltfnum[0];
	      /*_.MELT_DEBUG_FUN__V5*/ meltfptr[4] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V4*/ meltfptr[3] = /*_.MELT_DEBUG_FUN__V5*/ meltfptr[4];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-outobj.melt:5495:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L3*/ meltfnum[2] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V5*/ meltfptr[4] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V4*/ meltfptr[3] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-outobj.melt:5495:/ quasiblock");


      /*_.PROGN___V6*/ meltfptr[4] = /*_.IF___V4*/ meltfptr[3];;
      /*^compute */
      /*_.IFCPP___V3*/ meltfptr[2] = /*_.PROGN___V6*/ meltfptr[4];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:5495:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L2*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IF___V4*/ meltfptr[3] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V6*/ meltfptr[4] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V3*/ meltfptr[2] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-outobj.melt:5496:/ quasiblock");


    MELT_LOCATION ("warmelt-outobj.melt:5497:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#IS_A__L4*/ meltfnum[2] =
      melt_is_instance_of ((melt_ptr_t) ( /*_.SEXP__V2*/ meltfptr[1]),
			   (melt_ptr_t) (( /*!CLASS_LOCATED */ meltfrout->
					  tabval[1])));;
    MELT_LOCATION ("warmelt-outobj.melt:5497:/ cond");
    /*cond */ if ( /*_#IS_A__L4*/ meltfnum[2])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  /*^getslot */
	  {
	    melt_ptr_t slot = NULL, obj = NULL;
	    obj = (melt_ptr_t) ( /*_.SEXP__V2*/ meltfptr[1]) /*=obj*/ ;
	    melt_object_get_field (slot, obj, 1, "LOCA_LOCATION");
    /*_.LOCA_LOCATION__V8*/ meltfptr[4] = slot;
	  };
	  ;
	  /*_.PSLOC__V7*/ meltfptr[3] = /*_.LOCA_LOCATION__V8*/ meltfptr[4];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-outobj.melt:5497:/ clear");
	     /*clear *//*_.LOCA_LOCATION__V8*/ meltfptr[4] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

  /*_.PSLOC__V7*/ meltfptr[3] = NULL;;
      }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:5501:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#IS_A__L5*/ meltfnum[1] =
      melt_is_instance_of ((melt_ptr_t) ( /*_.SEXP__V2*/ meltfptr[1]),
			   (melt_ptr_t) (( /*!CLASS_SOURCE_COMMENT */
					  meltfrout->tabval[2])));;
    MELT_LOCATION ("warmelt-outobj.melt:5501:/ cond");
    /*cond */ if ( /*_#IS_A__L5*/ meltfnum[1])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-outobj.melt:5502:/ quasiblock");


	  /*^getslot */
	  {
	    melt_ptr_t slot = NULL, obj = NULL;
	    obj = (melt_ptr_t) ( /*_.SEXP__V2*/ meltfptr[1]) /*=obj*/ ;
	    melt_object_get_field (slot, obj, 1, "LOCA_LOCATION");
    /*_.SLOC__V9*/ meltfptr[4] = slot;
	  };
	  ;
	  MELT_LOCATION ("warmelt-outobj.melt:5503:/ getslot");
	  {
	    melt_ptr_t slot = NULL, obj = NULL;
	    obj = (melt_ptr_t) ( /*_.SEXP__V2*/ meltfptr[1]) /*=obj*/ ;
	    melt_object_get_field (slot, obj, 2, "SCOMM_STR");
    /*_.SCOMM__V10*/ meltfptr[9] = slot;
	  };
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:5505:/ locexp");
	    meltgc_strbuf_add_indent ((melt_ptr_t)
				      (( /*~DECLBUF */ meltfclos->tabval[0])),
				      (0), 0);
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:5506:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t)
				 (( /*~DECLBUF */ meltfclos->tabval[0])),
				 ("/**!!** "));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:5507:/ locexp");
	    meltgc_add_strbuf_ccomment ((melt_ptr_t)
					(( /*~DECLBUF */ meltfclos->
					  tabval[0])),
					melt_string_str ((melt_ptr_t)
							 ( /*_.SCOMM__V10*/
							  meltfptr[9])));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:5508:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t)
				 (( /*~DECLBUF */ meltfclos->tabval[0])),
				 ("**!!**/"));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:5509:/ locexp");
	    meltgc_strbuf_add_indent ((melt_ptr_t)
				      (( /*~DECLBUF */ meltfclos->tabval[0])),
				      (0), 0);
	  }
	  ;

	  MELT_LOCATION ("warmelt-outobj.melt:5502:/ clear");
	     /*clear *//*_.SLOC__V9*/ meltfptr[4] = 0;
	  /*^clear */
	     /*clear *//*_.SCOMM__V10*/ meltfptr[9] = 0;
	  /*epilog */
	}
	;
      }
    else
      {
	MELT_LOCATION ("warmelt-outobj.melt:5501:/ cond.else");

	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-outobj.melt:5514:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  MELT_LOCATION ("warmelt-outobj.melt:5512:/ quasiblock");


	  /*^multimsend */
	  /*multimsend */
	  {
	    union meltparam_un argtab[3];
	    union meltparam_un restab[1];
	    memset (&argtab, 0, sizeof (argtab));
	    memset (&restab, 0, sizeof (restab));
	    /*^multimsend.arg */
	    argtab[0].meltbp_aptr = (melt_ptr_t *) & ( /*~INIENV */ meltfclos->tabval[1]);	/*^multimsend.arg */
	    argtab[1].meltbp_aptr = (melt_ptr_t *) & ( /*~NCX */ meltfclos->tabval[2]);	/*^multimsend.arg */
	    argtab[2].meltbp_aptr =
	      (melt_ptr_t *) & /*_.PSLOC__V7*/ meltfptr[3];
	    /*^multimsend.xres */
	    restab[0].meltbp_aptr = (melt_ptr_t *) & /*_.NBIND__V12*/ meltfptr[9];	/*^multimsend.send */
	    /*_.NEXP__V11*/ meltfptr[4] =
	      meltgc_send ((melt_ptr_t) ( /*_.SEXP__V2*/ meltfptr[1]),
			   ((melt_ptr_t)
			    (( /*!NORMAL_EXP */ meltfrout->tabval[3]))),
			   (MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_PTR
			    ""), argtab, (MELTBPARSTR_PTR ""), restab);
	  }
	  ;
	  /*^quasiblock */



#if MELT_HAVE_DEBUG
	  MELT_LOCATION ("warmelt-outobj.melt:5515:/ cppif.then");
	  /*^block */
	  /*anyblock */
	  {


	    {
	      /*^locexp */
	      /*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	      melt_dbgcounter++;
#endif
	      ;
	    }
	    ;
	    /*^checksignal */
	    MELT_CHECK_SIGNAL ();
	    ;
     /*_#MELT_NEED_DBG__L6*/ meltfnum[5] =
	      /*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	      ( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	      0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	      ;;
	    MELT_LOCATION ("warmelt-outobj.melt:5515:/ cond");
	    /*cond */ if ( /*_#MELT_NEED_DBG__L6*/ meltfnum[5])	/*then */
	      {
		/*^cond.then */
		/*^block */
		/*anyblock */
		{

       /*_#THE_MELTCALLCOUNT__L7*/ meltfnum[6] =
#ifdef meltcallcount
		    meltcallcount	/* the_meltcallcount */
#else
		    0L
#endif /* meltcallcount the_meltcallcount */
		    ;;
		  MELT_LOCATION ("warmelt-outobj.melt:5515:/ checksignal");
		  MELT_CHECK_SIGNAL ();
		  ;
		  /*^apply */
		  /*apply */
		  {
		    union meltparam_un argtab[7];
		    memset (&argtab, 0, sizeof (argtab));
		    /*^apply.arg */
		    argtab[0].meltbp_long =
		      /*_#THE_MELTCALLCOUNT__L7*/ meltfnum[6];
		    /*^apply.arg */
		    argtab[1].meltbp_cstring = "warmelt-outobj.melt";
		    /*^apply.arg */
		    argtab[2].meltbp_long = 5515;
		    /*^apply.arg */
		    argtab[3].meltbp_cstring =
		      "normadeclb_macroexpanded_list nexp=";
		    /*^apply.arg */
		    argtab[4].meltbp_aptr =
		      (melt_ptr_t *) & /*_.NEXP__V11*/ meltfptr[4];
		    /*^apply.arg */
		    argtab[5].meltbp_cstring = " nbind=";
		    /*^apply.arg */
		    argtab[6].meltbp_aptr =
		      (melt_ptr_t *) & /*_.NBIND__V12*/ meltfptr[9];
		    /*_.MELT_DEBUG_FUN__V15*/ meltfptr[14] =
		      melt_apply ((meltclosure_ptr_t)
				  (( /*!MELT_DEBUG_FUN */ meltfrout->
				    tabval[0])),
				  (melt_ptr_t) (( /*nil */ NULL)),
				  (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_PTR MELTBPARSTR_CSTRING
				   MELTBPARSTR_PTR ""), argtab, "",
				  (union meltparam_un *) 0);
		  }
		  ;
		  /*_.IF___V14*/ meltfptr[13] =
		    /*_.MELT_DEBUG_FUN__V15*/ meltfptr[14];;
		  /*epilog */

		  MELT_LOCATION ("warmelt-outobj.melt:5515:/ clear");
		 /*clear *//*_#THE_MELTCALLCOUNT__L7*/ meltfnum[6] = 0;
		  /*^clear */
		 /*clear *//*_.MELT_DEBUG_FUN__V15*/ meltfptr[14] = 0;
		}
		;
	      }
	    else
	      {			/*^cond.else */

      /*_.IF___V14*/ meltfptr[13] = NULL;;
	      }
	    ;
	    MELT_LOCATION ("warmelt-outobj.melt:5515:/ quasiblock");


	    /*_.PROGN___V16*/ meltfptr[14] = /*_.IF___V14*/ meltfptr[13];;
	    /*^compute */
	    /*_.IFCPP___V13*/ meltfptr[12] = /*_.PROGN___V16*/ meltfptr[14];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-outobj.melt:5515:/ clear");
	       /*clear *//*_#MELT_NEED_DBG__L6*/ meltfnum[5] = 0;
	    /*^clear */
	       /*clear *//*_.IF___V14*/ meltfptr[13] = 0;
	    /*^clear */
	       /*clear *//*_.PROGN___V16*/ meltfptr[14] = 0;
	  }

#else /*MELT_HAVE_DEBUG */
	  /*^cppif.else */
	  /*_.IFCPP___V13*/ meltfptr[12] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
	  ;
	  MELT_LOCATION ("warmelt-outobj.melt:5516:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^checksignal */
	  MELT_CHECK_SIGNAL ();
	  ;
   /*_#IS_A__L8*/ meltfnum[6] =
	    melt_is_instance_of ((melt_ptr_t) ( /*_.NEXP__V11*/ meltfptr[4]),
				 (melt_ptr_t) (( /*!CLASS_NREP */ meltfrout->
						tabval[4])));;
	  MELT_LOCATION ("warmelt-outobj.melt:5516:/ cond");
	  /*cond */ if ( /*_#IS_A__L8*/ meltfnum[6])	/*then */
	    {
	      /*^cond.then */
	      /*^block */
	      /*anyblock */
	      {

     /*_#IS_A__L10*/ meltfnum[9] =
		  melt_is_instance_of ((melt_ptr_t)
				       ( /*_.NEXP__V11*/ meltfptr[4]),
				       (melt_ptr_t) (( /*!CLASS_NREP_ANYPROC */ meltfrout->tabval[5])));;
		/*^compute */
     /*_#NOT__L11*/ meltfnum[10] =
		  (!( /*_#IS_A__L10*/ meltfnum[9]));;
		/*^compute */
		/*_#IF___L9*/ meltfnum[5] = /*_#NOT__L11*/ meltfnum[10];;
		/*epilog */

		MELT_LOCATION ("warmelt-outobj.melt:5516:/ clear");
	       /*clear *//*_#IS_A__L10*/ meltfnum[9] = 0;
		/*^clear */
	       /*clear *//*_#NOT__L11*/ meltfnum[10] = 0;
	      }
	      ;
	    }
	  else
	    {			/*^cond.else */

    /*_#IF___L9*/ meltfnum[5] = 0;;
	    }
	  ;
	  MELT_LOCATION ("warmelt-outobj.melt:5516:/ cond");
	  /*cond */ if ( /*_#IF___L9*/ meltfnum[5])	/*then */
	    {
	      /*^cond.then */
	      /*^block */
	      /*anyblock */
	      {

		MELT_LOCATION ("warmelt-outobj.melt:5518:/ quasiblock");


		/*^checksignal */
		MELT_CHECK_SIGNAL ();
		;
		/*^apply */
		/*apply */
		{
		  union meltparam_un argtab[2];
		  memset (&argtab, 0, sizeof (argtab));
		  /*^apply.arg */
		  argtab[0].meltbp_aptr =
		    (melt_ptr_t *) & /*_.NBIND__V12*/ meltfptr[9];
		  /*^apply.arg */
		  argtab[1].meltbp_aptr =
		    (melt_ptr_t *) & /*_.PSLOC__V7*/ meltfptr[3];
		  /*_.WNEXP__V17*/ meltfptr[13] =
		    melt_apply ((meltclosure_ptr_t)
				(( /*!WRAP_NORMAL_LET1 */ meltfrout->
				  tabval[6])),
				(melt_ptr_t) ( /*_.NEXP__V11*/ meltfptr[4]),
				(MELTBPARSTR_PTR MELTBPARSTR_PTR ""), argtab,
				"", (union meltparam_un *) 0);
		}
		;

#if MELT_HAVE_DEBUG
		MELT_LOCATION ("warmelt-outobj.melt:5519:/ cppif.then");
		/*^block */
		/*anyblock */
		{


		  {
		    /*^locexp */
		    /*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
		    melt_dbgcounter++;
#endif
		    ;
		  }
		  ;
		  /*^checksignal */
		  MELT_CHECK_SIGNAL ();
		  ;
       /*_#MELT_NEED_DBG__L12*/ meltfnum[9] =
		    /*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
		    ( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
		    0		/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
		    ;;
		  MELT_LOCATION ("warmelt-outobj.melt:5519:/ cond");
		  /*cond */ if ( /*_#MELT_NEED_DBG__L12*/ meltfnum[9])	/*then */
		    {
		      /*^cond.then */
		      /*^block */
		      /*anyblock */
		      {

	 /*_#THE_MELTCALLCOUNT__L13*/ meltfnum[10] =
#ifdef meltcallcount
			  meltcallcount	/* the_meltcallcount */
#else
			  0L
#endif /* meltcallcount the_meltcallcount */
			  ;;
			MELT_LOCATION
			  ("warmelt-outobj.melt:5519:/ checksignal");
			MELT_CHECK_SIGNAL ();
			;
			/*^apply */
			/*apply */
			{
			  union meltparam_un argtab[5];
			  memset (&argtab, 0, sizeof (argtab));
			  /*^apply.arg */
			  argtab[0].meltbp_long =
			    /*_#THE_MELTCALLCOUNT__L13*/ meltfnum[10];
			  /*^apply.arg */
			  argtab[1].meltbp_cstring = "warmelt-outobj.melt";
			  /*^apply.arg */
			  argtab[2].meltbp_long = 5519;
			  /*^apply.arg */
			  argtab[3].meltbp_cstring =
			    "normadeclb_macroexpanded_list wnexp=";
			  /*^apply.arg */
			  argtab[4].meltbp_aptr =
			    (melt_ptr_t *) & /*_.WNEXP__V17*/ meltfptr[13];
			  /*_.MELT_DEBUG_FUN__V20*/ meltfptr[19] =
			    melt_apply ((meltclosure_ptr_t)
					(( /*!MELT_DEBUG_FUN */ meltfrout->
					  tabval[0])),
					(melt_ptr_t) (( /*nil */ NULL)),
					(MELTBPARSTR_LONG MELTBPARSTR_CSTRING
					 MELTBPARSTR_LONG MELTBPARSTR_CSTRING
					 MELTBPARSTR_PTR ""), argtab, "",
					(union meltparam_un *) 0);
			}
			;
			/*_.IF___V19*/ meltfptr[18] =
			  /*_.MELT_DEBUG_FUN__V20*/ meltfptr[19];;
			/*epilog */

			MELT_LOCATION ("warmelt-outobj.melt:5519:/ clear");
		   /*clear *//*_#THE_MELTCALLCOUNT__L13*/ meltfnum[10] =
			  0;
			/*^clear */
		   /*clear *//*_.MELT_DEBUG_FUN__V20*/ meltfptr[19] = 0;
		      }
		      ;
		    }
		  else
		    {		/*^cond.else */

	/*_.IF___V19*/ meltfptr[18] = NULL;;
		    }
		  ;
		  MELT_LOCATION ("warmelt-outobj.melt:5519:/ quasiblock");


		  /*_.PROGN___V21*/ meltfptr[19] =
		    /*_.IF___V19*/ meltfptr[18];;
		  /*^compute */
		  /*_.IFCPP___V18*/ meltfptr[14] =
		    /*_.PROGN___V21*/ meltfptr[19];;
		  /*epilog */

		  MELT_LOCATION ("warmelt-outobj.melt:5519:/ clear");
		 /*clear *//*_#MELT_NEED_DBG__L12*/ meltfnum[9] = 0;
		  /*^clear */
		 /*clear *//*_.IF___V19*/ meltfptr[18] = 0;
		  /*^clear */
		 /*clear *//*_.PROGN___V21*/ meltfptr[19] = 0;
		}

#else /*MELT_HAVE_DEBUG */
		/*^cppif.else */
		/*_.IFCPP___V18*/ meltfptr[14] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
		;
		MELT_LOCATION ("warmelt-outobj.melt:5520:/ getslot");
		{
		  melt_ptr_t slot = NULL, obj = NULL;
		  obj =
		    (melt_ptr_t) (( /*~INIPROC */ meltfclos->
				   tabval[3])) /*=obj*/ ;
		  melt_object_get_field (slot, obj, 2, "NINIT_TOPL");
      /*_.NINIT_TOPL__V22*/ meltfptr[18] = slot;
		};
		;

		{
		  /*^locexp */
		  meltgc_append_list ((melt_ptr_t)
				      ( /*_.NINIT_TOPL__V22*/ meltfptr[18]),
				      (melt_ptr_t) ( /*_.WNEXP__V17*/
						    meltfptr[13]));
		}
		;

		MELT_LOCATION ("warmelt-outobj.melt:5518:/ clear");
	       /*clear *//*_.WNEXP__V17*/ meltfptr[13] = 0;
		/*^clear */
	       /*clear *//*_.IFCPP___V18*/ meltfptr[14] = 0;
		/*^clear */
	       /*clear *//*_.NINIT_TOPL__V22*/ meltfptr[18] = 0;
		/*epilog */
	      }
	      ;
	    }			/*noelse */
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:5523:/ locexp");
	    meltgc_append_list ((melt_ptr_t)
				(( /*~NORMLIST */ meltfclos->tabval[4])),
				(melt_ptr_t) ( /*_.NEXP__V11*/ meltfptr[4]));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:5524:/ locexp");
	    meltgc_append_list ((melt_ptr_t)
				(( /*~BINDLIST */ meltfclos->tabval[5])),
				(melt_ptr_t) ( /*_.NBIND__V12*/ meltfptr[9]));
	  }
	  ;
	  MELT_LOCATION ("warmelt-outobj.melt:5512:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;

	  /*^clear */
	     /*clear *//*_.IFCPP___V13*/ meltfptr[12] = 0;
	  /*^clear */
	     /*clear *//*_#IS_A__L8*/ meltfnum[6] = 0;
	  /*^clear */
	     /*clear *//*_#IF___L9*/ meltfnum[5] = 0;

	  /*^clear */
	     /*clear *//*_.NBIND__V12*/ meltfptr[9] = 0;
	  /*epilog */
	}
	;
      }
    ;

    MELT_LOCATION ("warmelt-outobj.melt:5496:/ clear");
	   /*clear *//*_#IS_A__L4*/ meltfnum[2] = 0;
    /*^clear */
	   /*clear *//*_.PSLOC__V7*/ meltfptr[3] = 0;
    /*^clear */
	   /*clear *//*_#IS_A__L5*/ meltfnum[1] = 0;
    MELT_LOCATION ("warmelt-outobj.melt:5494:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V3*/ meltfptr[2] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("LAMBDA_", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_132_warmelt_outobj_LAMBDA___26___melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*noretval */ NULL);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_132_warmelt_outobj_LAMBDA___26__ */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_133_warmelt_outobj_TRANSLATE_MACROEXPANDED_LIST (meltclosure_ptr_t
							  meltclosp_,
							  melt_ptr_t
							  meltfirstargp_,
							  const
							  melt_argdescr_cell_t
							  meltxargdescr_[],
							  union meltparam_un *
							  meltxargtab_,
							  const
							  melt_argdescr_cell_t
							  meltxresdescr_[],
							  union meltparam_un *
							  meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_133_warmelt_outobj_TRANSLATE_MACROEXPANDED_LIST_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_133_warmelt_outobj_TRANSLATE_MACROEXPANDED_LIST_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 195
    melt_ptr_t mcfr_varptr[195];
#define MELTFRAM_NBVARNUM 105
    long mcfr_varnum[105];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_133_warmelt_outobj_TRANSLATE_MACROEXPANDED_LIST is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct
	 frame_meltrout_133_warmelt_outobj_TRANSLATE_MACROEXPANDED_LIST_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 195; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_133_warmelt_outobj_TRANSLATE_MACROEXPANDED_LIST nbval 195*/
  meltfram__.mcfr_nbvar = 195 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("TRANSLATE_MACROEXPANDED_LIST", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-outobj.melt:5583:/ getarg");
 /*_.XLIST__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.MODNAMSTR__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.MODNAMSTR__V3*/ meltfptr[2])) !=
	      NULL);


  /*getarg#2 */
  /*^getarg */
  if (meltxargdescr_[1] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.MODCTX__V4*/ meltfptr[3] =
    (meltxargtab_[1].meltbp_aptr) ? (*(meltxargtab_[1].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.MODCTX__V4*/ meltfptr[3])) !=
	      NULL);


  /*getarg#3 */
  /*^getarg */
  if (meltxargdescr_[2] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.NCX__V5*/ meltfptr[4] =
    (meltxargtab_[2].meltbp_aptr) ? (*(meltxargtab_[2].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.NCX__V5*/ meltfptr[4])) != NULL);


  /*getarg#4 */
  /*^getarg */
  if (meltxargdescr_[3] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.INIENV__V6*/ meltfptr[5] =
    (meltxargtab_[3].meltbp_aptr) ? (*(meltxargtab_[3].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.INIENV__V6*/ meltfptr[5])) !=
	      NULL);


  /*getarg#5 */
  /*^getarg */
  if (meltxargdescr_[4] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.NORMALISTHANDLER__V7*/ meltfptr[6] =
    (meltxargtab_[4].meltbp_aptr) ? (*(meltxargtab_[4].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr
	      ((melt_ptr_t) ( /*_.NORMALISTHANDLER__V7*/ meltfptr[6])) !=
	      NULL);


  /*getarg#6 */
  /*^getarg */
  if (meltxargdescr_[5] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.INIPROCTRANSL__V8*/ meltfptr[7] =
    (meltxargtab_[5].meltbp_aptr) ? (*(meltxargtab_[5].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.INIPROCTRANSL__V8*/ meltfptr[7]))
	      != NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:5601:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L1*/ meltfnum[0] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-outobj.melt:5601:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L1*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-outobj.melt:5601:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[11];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-outobj.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 5601;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring =
		"translate_macroexpanded_list modnamstr=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.MODNAMSTR__V3*/ meltfptr[2];
	      /*^apply.arg */
	      argtab[5].meltbp_cstring = "\n* modctx=";
	      /*^apply.arg */
	      argtab[6].meltbp_aptr =
		(melt_ptr_t *) & /*_.MODCTX__V4*/ meltfptr[3];
	      /*^apply.arg */
	      argtab[7].meltbp_cstring = "\n* inienv=";
	      /*^apply.arg */
	      argtab[8].meltbp_aptr =
		(melt_ptr_t *) & /*_.INIENV__V6*/ meltfptr[5];
	      /*^apply.arg */
	      argtab[9].meltbp_cstring = "\n* iniproctransl=";
	      /*^apply.arg */
	      argtab[10].meltbp_aptr =
		(melt_ptr_t *) & /*_.INIPROCTRANSL__V8*/ meltfptr[7];
	      /*_.MELT_DEBUG_FUN__V11*/ meltfptr[10] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V10*/ meltfptr[9] =
	      /*_.MELT_DEBUG_FUN__V11*/ meltfptr[10];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-outobj.melt:5601:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V11*/ meltfptr[10] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V10*/ meltfptr[9] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-outobj.melt:5601:/ quasiblock");


      /*_.PROGN___V12*/ meltfptr[10] = /*_.IF___V10*/ meltfptr[9];;
      /*^compute */
      /*_.IFCPP___V9*/ meltfptr[8] = /*_.PROGN___V12*/ meltfptr[10];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:5601:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L1*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IF___V10*/ meltfptr[9] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V12*/ meltfptr[10] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V9*/ meltfptr[8] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:5604:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_LIST__L3*/ meltfnum[1] =
	(melt_magic_discr ((melt_ptr_t) ( /*_.XLIST__V2*/ meltfptr[1])) ==
	 MELTOBMAG_LIST);;
      MELT_LOCATION ("warmelt-outobj.melt:5604:/ cond");
      /*cond */ if ( /*_#IS_LIST__L3*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V14*/ meltfptr[10] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-outobj.melt:5604:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check xlist"),
				  ("warmelt-outobj.melt")
				  ? ("warmelt-outobj.melt") : __FILE__,
				  (5604) ? (5604) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V14*/ meltfptr[10] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V13*/ meltfptr[9] = /*_.IFELSE___V14*/ meltfptr[10];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:5604:/ clear");
	     /*clear *//*_#IS_LIST__L3*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V14*/ meltfptr[10] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V13*/ meltfptr[9] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:5605:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_STRING__L4*/ meltfnum[0] =
	(melt_magic_discr ((melt_ptr_t) ( /*_.MODNAMSTR__V3*/ meltfptr[2])) ==
	 MELTOBMAG_STRING);;
      MELT_LOCATION ("warmelt-outobj.melt:5605:/ cond");
      /*cond */ if ( /*_#IS_STRING__L4*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V16*/ meltfptr[15] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-outobj.melt:5605:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check modnamstr"),
				  ("warmelt-outobj.melt")
				  ? ("warmelt-outobj.melt") : __FILE__,
				  (5605) ? (5605) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V16*/ meltfptr[15] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V15*/ meltfptr[10] = /*_.IFELSE___V16*/ meltfptr[15];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:5605:/ clear");
	     /*clear *//*_#IS_STRING__L4*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V16*/ meltfptr[15] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V15*/ meltfptr[10] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:5606:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L5*/ meltfnum[1] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.MODCTX__V4*/ meltfptr[3]),
			     (melt_ptr_t) (( /*!CLASS_MODULE_CONTEXT */
					    meltfrout->tabval[1])));;
      MELT_LOCATION ("warmelt-outobj.melt:5606:/ cond");
      /*cond */ if ( /*_#IS_A__L5*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V18*/ meltfptr[17] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-outobj.melt:5606:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check modctx"),
				  ("warmelt-outobj.melt")
				  ? ("warmelt-outobj.melt") : __FILE__,
				  (5606) ? (5606) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V18*/ meltfptr[17] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V17*/ meltfptr[15] = /*_.IFELSE___V18*/ meltfptr[17];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:5606:/ clear");
	     /*clear *//*_#IS_A__L5*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V18*/ meltfptr[17] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V17*/ meltfptr[15] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:5607:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L6*/ meltfnum[0] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.NCX__V5*/ meltfptr[4]),
			     (melt_ptr_t) (( /*!CLASS_NORMALIZATION_CONTEXT */
					    meltfrout->tabval[2])));;
      MELT_LOCATION ("warmelt-outobj.melt:5607:/ cond");
      /*cond */ if ( /*_#IS_A__L6*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V20*/ meltfptr[19] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-outobj.melt:5607:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check ncx"),
				  ("warmelt-outobj.melt")
				  ? ("warmelt-outobj.melt") : __FILE__,
				  (5607) ? (5607) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V20*/ meltfptr[19] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V19*/ meltfptr[17] = /*_.IFELSE___V20*/ meltfptr[19];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:5607:/ clear");
	     /*clear *//*_#IS_A__L6*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V20*/ meltfptr[19] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V19*/ meltfptr[17] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:5608:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L7*/ meltfnum[1] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.INIENV__V6*/ meltfptr[5]),
			     (melt_ptr_t) (( /*!CLASS_ENVIRONMENT */
					    meltfrout->tabval[3])));;
      MELT_LOCATION ("warmelt-outobj.melt:5608:/ cond");
      /*cond */ if ( /*_#IS_A__L7*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V22*/ meltfptr[21] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-outobj.melt:5608:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check inienv"),
				  ("warmelt-outobj.melt")
				  ? ("warmelt-outobj.melt") : __FILE__,
				  (5608) ? (5608) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V22*/ meltfptr[21] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V21*/ meltfptr[19] = /*_.IFELSE___V22*/ meltfptr[21];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:5608:/ clear");
	     /*clear *//*_#IS_A__L7*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V22*/ meltfptr[21] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V21*/ meltfptr[19] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:5609:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#STRING_DYNLOADED_SUFFIXED__L8*/ meltfnum[0] =
	/*string_dynloaded_suffixed: */
	(melt_string_is_ending ((melt_ptr_t) /*_.MODNAMSTR__V3*/ meltfptr[2],
				MELT_DYNLOADED_SUFFIX));;
      /*^compute */
   /*_#NOT__L9*/ meltfnum[1] =
	(!( /*_#STRING_DYNLOADED_SUFFIXED__L8*/ meltfnum[0]));;
      MELT_LOCATION ("warmelt-outobj.melt:5609:/ cond");
      /*cond */ if ( /*_#NOT__L9*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V24*/ meltfptr[23] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-outobj.melt:5609:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("translate_macroexpanded_list modulename not ended with MELT_DYNLOADED_SUFFIX."), ("warmelt-outobj.melt") ? ("warmelt-outobj.melt") : __FILE__, (5609) ? (5609) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V24*/ meltfptr[23] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V23*/ meltfptr[21] = /*_.IFELSE___V24*/ meltfptr[23];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:5609:/ clear");
	     /*clear *//*_#STRING_DYNLOADED_SUFFIXED__L8*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_#NOT__L9*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V24*/ meltfptr[23] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V23*/ meltfptr[21] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:5611:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#STRING_SUFFIXED__L10*/ meltfnum[0] =
	/*string_suffixed: */
	(melt_string_is_ending
	 ((melt_ptr_t) /*_.MODNAMSTR__V3*/ meltfptr[2], ".melt"));;
      /*^compute */
   /*_#NOT__L11*/ meltfnum[1] =
	(!( /*_#STRING_SUFFIXED__L10*/ meltfnum[0]));;
      MELT_LOCATION ("warmelt-outobj.melt:5611:/ cond");
      /*cond */ if ( /*_#NOT__L11*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V26*/ meltfptr[25] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-outobj.melt:5611:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("translate_macroexpanded_list modulename not ended with .melt"), ("warmelt-outobj.melt") ? ("warmelt-outobj.melt") : __FILE__, (5611) ? (5611) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V26*/ meltfptr[25] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V25*/ meltfptr[23] = /*_.IFELSE___V26*/ meltfptr[25];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:5611:/ clear");
	     /*clear *//*_#STRING_SUFFIXED__L10*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_#NOT__L11*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V26*/ meltfptr[25] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V25*/ meltfptr[23] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:5613:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#STRING_SUFFIXED__L12*/ meltfnum[0] =
	/*string_suffixed: */
	(melt_string_is_ending
	 ((melt_ptr_t) /*_.MODNAMSTR__V3*/ meltfptr[2], ".c"));;
      /*^compute */
   /*_#NOT__L13*/ meltfnum[1] =
	(!( /*_#STRING_SUFFIXED__L12*/ meltfnum[0]));;
      MELT_LOCATION ("warmelt-outobj.melt:5613:/ cond");
      /*cond */ if ( /*_#NOT__L13*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V28*/ meltfptr[27] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-outobj.melt:5613:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("translate_macroexpanded_list modulename not ended with .c"), ("warmelt-outobj.melt") ? ("warmelt-outobj.melt") : __FILE__, (5613) ? (5613) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V28*/ meltfptr[27] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V27*/ meltfptr[25] = /*_.IFELSE___V28*/ meltfptr[27];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:5613:/ clear");
	     /*clear *//*_#STRING_SUFFIXED__L12*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_#NOT__L13*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V28*/ meltfptr[27] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V27*/ meltfptr[25] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-outobj.melt:5615:/ quasiblock");


 /*_#ENDCLOCK__L14*/ meltfnum[0] = 0;;
    MELT_LOCATION ("warmelt-outobj.melt:5617:/ cond");
    /*cond */ if (
		   /*ifisa */
		   melt_is_instance_of ((melt_ptr_t)
					( /*_.MODCTX__V4*/ meltfptr[3]),
					(melt_ptr_t) (( /*!CLASS_MODULE_CONTEXT */ meltfrout->tabval[1])))
      )				/*then */
      {
	/*^cond.then */
	/*^getslot */
	{
	  melt_ptr_t slot = NULL, obj = NULL;
	  obj = (melt_ptr_t) ( /*_.MODCTX__V4*/ meltfptr[3]) /*=obj*/ ;
	  melt_object_get_field (slot, obj, 11, "MOCX_ERRORHANDLER");
   /*_.ERRORHDLR__V30*/ meltfptr[29] = slot;
	};
	;
      }
    else
      {				/*^cond.else */

  /*_.ERRORHDLR__V30*/ meltfptr[29] = NULL;;
      }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:5618:/ cond");
    /*cond */ if (
		   /*ifisa */
		   melt_is_instance_of ((melt_ptr_t)
					( /*_.MODCTX__V4*/ meltfptr[3]),
					(melt_ptr_t) (( /*!CLASS_MODULE_CONTEXT */ meltfrout->tabval[1])))
      )				/*then */
      {
	/*^cond.then */
	/*^getslot */
	{
	  melt_ptr_t slot = NULL, obj = NULL;
	  obj = (melt_ptr_t) ( /*_.MODCTX__V4*/ meltfptr[3]) /*=obj*/ ;
	  melt_object_get_field (slot, obj, 9, "MOCX_GENDEVLIST");
   /*_.GENDEVLIST__V31*/ meltfptr[30] = slot;
	};
	;
      }
    else
      {				/*^cond.else */

  /*_.GENDEVLIST__V31*/ meltfptr[30] = NULL;;
      }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:5619:/ cond");
    /*cond */ if (
		   /*ifisa */
		   melt_is_instance_of ((melt_ptr_t)
					( /*_.MODCTX__V4*/ meltfptr[3]),
					(melt_ptr_t) (( /*!CLASS_MODULE_CONTEXT */ meltfrout->tabval[1])))
      )				/*then */
      {
	/*^cond.then */
	/*^getslot */
	{
	  melt_ptr_t slot = NULL, obj = NULL;
	  obj = (melt_ptr_t) ( /*_.MODCTX__V4*/ meltfptr[3]) /*=obj*/ ;
	  melt_object_get_field (slot, obj, 3, "MOCX_INITIALENV");
   /*_.MODINIENV__V32*/ meltfptr[31] = slot;
	};
	;
      }
    else
      {				/*^cond.else */

  /*_.MODINIENV__V32*/ meltfptr[31] = NULL;;
      }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:5620:/ cond");
    /*cond */ if (
		   /*ifisa */
		   melt_is_instance_of ((melt_ptr_t)
					( /*_.NCX__V5*/ meltfptr[4]),
					(melt_ptr_t) (( /*!CLASS_NORMALIZATION_CONTEXT */ meltfrout->tabval[2])))
      )				/*then */
      {
	/*^cond.then */
	/*^getslot */
	{
	  melt_ptr_t slot = NULL, obj = NULL;
	  obj = (melt_ptr_t) ( /*_.NCX__V5*/ meltfptr[4]) /*=obj*/ ;
	  melt_object_get_field (slot, obj, 0, "NCTX_INITPROC");
   /*_.INIPROC__V33*/ meltfptr[32] = slot;
	};
	;
      }
    else
      {				/*^cond.else */

  /*_.INIPROC__V33*/ meltfptr[32] = NULL;;
      }
    ;
    /*^compute */
 /*_.IMPLBUF__V34*/ meltfptr[33] =
      (melt_ptr_t)
      meltgc_new_strbuf ((meltobject_ptr_t)
			 (( /*!DISCR_STRBUF */ meltfrout->tabval[4])),
			 (const char *) 0);;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:5623:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L15*/ meltfnum[1] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-outobj.melt:5623:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L15*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L16*/ meltfnum[15] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-outobj.melt:5623:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[9];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long =
		/*_#THE_MELTCALLCOUNT__L16*/ meltfnum[15];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-outobj.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 5623;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring =
		"translate_macroexpanded_list modinienv=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.MODINIENV__V32*/ meltfptr[31];
	      /*^apply.arg */
	      argtab[5].meltbp_cstring = " modnamstr=";
	      /*^apply.arg */
	      argtab[6].meltbp_aptr =
		(melt_ptr_t *) & /*_.MODNAMSTR__V3*/ meltfptr[2];
	      /*^apply.arg */
	      argtab[7].meltbp_cstring =
		" before normadeclb_macroexpanded_list modctx=";
	      /*^apply.arg */
	      argtab[8].meltbp_aptr =
		(melt_ptr_t *) & /*_.MODCTX__V4*/ meltfptr[3];
	      /*_.MELT_DEBUG_FUN__V37*/ meltfptr[36] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V36*/ meltfptr[35] =
	      /*_.MELT_DEBUG_FUN__V37*/ meltfptr[36];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-outobj.melt:5623:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L16*/ meltfnum[15] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V37*/ meltfptr[36] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V36*/ meltfptr[35] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-outobj.melt:5623:/ quasiblock");


      /*_.PROGN___V38*/ meltfptr[36] = /*_.IF___V36*/ meltfptr[35];;
      /*^compute */
      /*_.IFCPP___V35*/ meltfptr[34] = /*_.PROGN___V38*/ meltfptr[36];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:5623:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L15*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IF___V36*/ meltfptr[35] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V38*/ meltfptr[36] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V35*/ meltfptr[34] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-outobj.melt:5627:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    MELT_LOCATION ("warmelt-outobj.melt:5625:/ quasiblock");


    /*^multiapply */
    /*multiapply 5args, 3x.res */
    {
      union meltparam_un argtab[4];

      union meltparam_un restab[3];
      memset (&restab, 0, sizeof (restab));
      memset (&argtab, 0, sizeof (argtab));
      /*^multiapply.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.MODNAMSTR__V3*/ meltfptr[2];	/*^multiapply.arg */
      argtab[1].meltbp_aptr = (melt_ptr_t *) & /*_.MODCTX__V4*/ meltfptr[3];	/*^multiapply.arg */
      argtab[2].meltbp_aptr = (melt_ptr_t *) & /*_.NCX__V5*/ meltfptr[4];	/*^multiapply.arg */
      argtab[3].meltbp_aptr = (melt_ptr_t *) & /*_.INIENV__V6*/ meltfptr[5];
      /*^multiapply.xres */
      restab[0].meltbp_aptr =
	(melt_ptr_t *) & /*_.BINDLIST__V41*/ meltfptr[40];
      /*^multiapply.xres */
      restab[1].meltbp_aptr =
	(melt_ptr_t *) & /*_.DECLBUF__V42*/ meltfptr[41];
      /*^multiapply.xres */
      restab[2].meltbp_longptr = & /*_#STARTCLOCK__L17*/ meltfnum[15];
      /*^multiapply.appl */
      /*_.NORMLIST__V40*/ meltfptr[36] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!NORMADECLB_MACROEXPANDED_LIST */ meltfrout->
		      tabval[5])),
		    (melt_ptr_t) ( /*_.XLIST__V2*/ meltfptr[1]),
		    (MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_PTR
		     MELTBPARSTR_PTR ""), argtab,
		    (MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_LONG ""),
		    restab);
    }
    ;
    /*^quasiblock */



#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:5628:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L18*/ meltfnum[1] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-outobj.melt:5628:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L18*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L19*/ meltfnum[18] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-outobj.melt:5628:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[7];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long =
		/*_#THE_MELTCALLCOUNT__L19*/ meltfnum[18];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-outobj.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 5628;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring =
		"translate_macroexpanded_list normadeclb_macroexpanded_list give normlist=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.NORMLIST__V40*/ meltfptr[36];
	      /*^apply.arg */
	      argtab[5].meltbp_cstring = "\n bindlist=";
	      /*^apply.arg */
	      argtab[6].meltbp_aptr =
		(melt_ptr_t *) & /*_.BINDLIST__V41*/ meltfptr[40];
	      /*_.MELT_DEBUG_FUN__V45*/ meltfptr[44] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V44*/ meltfptr[43] =
	      /*_.MELT_DEBUG_FUN__V45*/ meltfptr[44];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-outobj.melt:5628:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L19*/ meltfnum[18] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V45*/ meltfptr[44] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V44*/ meltfptr[43] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-outobj.melt:5628:/ quasiblock");


      /*_.PROGN___V46*/ meltfptr[44] = /*_.IF___V44*/ meltfptr[43];;
      /*^compute */
      /*_.IFCPP___V43*/ meltfptr[42] = /*_.PROGN___V46*/ meltfptr[44];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:5628:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L18*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IF___V44*/ meltfptr[43] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V46*/ meltfptr[44] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V43*/ meltfptr[42] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:5630:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_LIST__L20*/ meltfnum[18] =
	(melt_magic_discr ((melt_ptr_t) ( /*_.NORMLIST__V40*/ meltfptr[36]))
	 == MELTOBMAG_LIST);;
      MELT_LOCATION ("warmelt-outobj.melt:5630:/ cond");
      /*cond */ if ( /*_#IS_LIST__L20*/ meltfnum[18])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V48*/ meltfptr[44] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-outobj.melt:5630:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check normlist"),
				  ("warmelt-outobj.melt")
				  ? ("warmelt-outobj.melt") : __FILE__,
				  (5630) ? (5630) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V48*/ meltfptr[44] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V47*/ meltfptr[43] = /*_.IFELSE___V48*/ meltfptr[44];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:5630:/ clear");
	     /*clear *//*_#IS_LIST__L20*/ meltfnum[18] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V48*/ meltfptr[44] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V47*/ meltfptr[43] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:5631:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_LIST__L21*/ meltfnum[1] =
	(melt_magic_discr ((melt_ptr_t) ( /*_.BINDLIST__V41*/ meltfptr[40]))
	 == MELTOBMAG_LIST);;
      MELT_LOCATION ("warmelt-outobj.melt:5631:/ cond");
      /*cond */ if ( /*_#IS_LIST__L21*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V50*/ meltfptr[49] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-outobj.melt:5631:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check bindlist"),
				  ("warmelt-outobj.melt")
				  ? ("warmelt-outobj.melt") : __FILE__,
				  (5631) ? (5631) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V50*/ meltfptr[49] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V49*/ meltfptr[44] = /*_.IFELSE___V50*/ meltfptr[49];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:5631:/ clear");
	     /*clear *//*_#IS_LIST__L21*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V50*/ meltfptr[49] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V49*/ meltfptr[44] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:5632:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_STRBUF__L22*/ meltfnum[18] =
	(melt_magic_discr ((melt_ptr_t) ( /*_.DECLBUF__V42*/ meltfptr[41])) ==
	 MELTOBMAG_STRBUF);;
      MELT_LOCATION ("warmelt-outobj.melt:5632:/ cond");
      /*cond */ if ( /*_#IS_STRBUF__L22*/ meltfnum[18])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V52*/ meltfptr[51] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-outobj.melt:5632:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check declbuf"),
				  ("warmelt-outobj.melt")
				  ? ("warmelt-outobj.melt") : __FILE__,
				  (5632) ? (5632) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V52*/ meltfptr[51] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V51*/ meltfptr[49] = /*_.IFELSE___V52*/ meltfptr[51];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:5632:/ clear");
	     /*clear *//*_#IS_STRBUF__L22*/ meltfnum[18] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V52*/ meltfptr[51] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V51*/ meltfptr[49] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:5633:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L23*/ meltfnum[1] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-outobj.melt:5633:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L23*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L24*/ meltfnum[18] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-outobj.melt:5633:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[9];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long =
		/*_#THE_MELTCALLCOUNT__L24*/ meltfnum[18];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-outobj.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 5633;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring =
		"translate_macroexpanded_list after normadeclb_macroexpanded_list modctx=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.MODCTX__V4*/ meltfptr[3];
	      /*^apply.arg */
	      argtab[5].meltbp_cstring = "\n normalisthandler=";
	      /*^apply.arg */
	      argtab[6].meltbp_aptr =
		(melt_ptr_t *) & /*_.NORMALISTHANDLER__V7*/ meltfptr[6];
	      /*^apply.arg */
	      argtab[7].meltbp_cstring = "\n normlist=";
	      /*^apply.arg */
	      argtab[8].meltbp_aptr =
		(melt_ptr_t *) & /*_.NORMLIST__V40*/ meltfptr[36];
	      /*_.MELT_DEBUG_FUN__V55*/ meltfptr[54] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V54*/ meltfptr[53] =
	      /*_.MELT_DEBUG_FUN__V55*/ meltfptr[54];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-outobj.melt:5633:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L24*/ meltfnum[18] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V55*/ meltfptr[54] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V54*/ meltfptr[53] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-outobj.melt:5633:/ quasiblock");


      /*_.PROGN___V56*/ meltfptr[54] = /*_.IF___V54*/ meltfptr[53];;
      /*^compute */
      /*_.IFCPP___V53*/ meltfptr[51] = /*_.PROGN___V56*/ meltfptr[54];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:5633:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L23*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IF___V54*/ meltfptr[53] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V56*/ meltfptr[54] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V53*/ meltfptr[51] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-outobj.melt:5636:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#IS_CLOSURE__L25*/ meltfnum[18] =
      (melt_magic_discr
       ((melt_ptr_t) ( /*_.NORMALISTHANDLER__V7*/ meltfptr[6])) ==
       MELTOBMAG_CLOSURE);;
    MELT_LOCATION ("warmelt-outobj.melt:5636:/ cond");
    /*cond */ if ( /*_#IS_CLOSURE__L25*/ meltfnum[18])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{


#if MELT_HAVE_DEBUG
	  MELT_LOCATION ("warmelt-outobj.melt:5637:/ cppif.then");
	  /*^block */
	  /*anyblock */
	  {


	    {
	      /*^locexp */
	      /*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	      melt_dbgcounter++;
#endif
	      ;
	    }
	    ;
	    /*^checksignal */
	    MELT_CHECK_SIGNAL ();
	    ;
     /*_#MELT_NEED_DBG__L26*/ meltfnum[1] =
	      /*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	      ( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	      0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	      ;;
	    MELT_LOCATION ("warmelt-outobj.melt:5637:/ cond");
	    /*cond */ if ( /*_#MELT_NEED_DBG__L26*/ meltfnum[1])	/*then */
	      {
		/*^cond.then */
		/*^block */
		/*anyblock */
		{

       /*_#THE_MELTCALLCOUNT__L27*/ meltfnum[26] =
#ifdef meltcallcount
		    meltcallcount	/* the_meltcallcount */
#else
		    0L
#endif /* meltcallcount the_meltcallcount */
		    ;;
		  /*^compute */
       /*_#LIST_LENGTH__L28*/ meltfnum[27] =
		    (melt_list_length
		     ((melt_ptr_t) ( /*_.NORMLIST__V40*/ meltfptr[36])));;
		  /*^compute */
       /*_#LIST_LENGTH__L29*/ meltfnum[28] =
		    (melt_list_length
		     ((melt_ptr_t) ( /*_.BINDLIST__V41*/ meltfptr[40])));;
		  MELT_LOCATION ("warmelt-outobj.melt:5637:/ checksignal");
		  MELT_CHECK_SIGNAL ();
		  ;
		  /*^apply */
		  /*apply */
		  {
		    union meltparam_un argtab[11];
		    memset (&argtab, 0, sizeof (argtab));
		    /*^apply.arg */
		    argtab[0].meltbp_long =
		      /*_#THE_MELTCALLCOUNT__L27*/ meltfnum[26];
		    /*^apply.arg */
		    argtab[1].meltbp_cstring = "warmelt-outobj.melt";
		    /*^apply.arg */
		    argtab[2].meltbp_long = 5637;
		    /*^apply.arg */
		    argtab[3].meltbp_cstring =
		      "translate_macroexpanded_list before calling normalisthandler normlist=";
		    /*^apply.arg */
		    argtab[4].meltbp_aptr =
		      (melt_ptr_t *) & /*_.NORMLIST__V40*/ meltfptr[36];
		    /*^apply.arg */
		    argtab[5].meltbp_cstring = "\n of length ";
		    /*^apply.arg */
		    argtab[6].meltbp_long =
		      /*_#LIST_LENGTH__L28*/ meltfnum[27];
		    /*^apply.arg */
		    argtab[7].meltbp_cstring = "\n bindlist=";
		    /*^apply.arg */
		    argtab[8].meltbp_aptr =
		      (melt_ptr_t *) & /*_.BINDLIST__V41*/ meltfptr[40];
		    /*^apply.arg */
		    argtab[9].meltbp_cstring = "\n of length ";
		    /*^apply.arg */
		    argtab[10].meltbp_long =
		      /*_#LIST_LENGTH__L29*/ meltfnum[28];
		    /*_.MELT_DEBUG_FUN__V60*/ meltfptr[59] =
		      melt_apply ((meltclosure_ptr_t)
				  (( /*!MELT_DEBUG_FUN */ meltfrout->
				    tabval[0])),
				  (melt_ptr_t) (( /*nil */ NULL)),
				  (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_PTR MELTBPARSTR_CSTRING
				   MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_PTR MELTBPARSTR_CSTRING
				   MELTBPARSTR_LONG ""), argtab, "",
				  (union meltparam_un *) 0);
		  }
		  ;
		  /*_.IF___V59*/ meltfptr[58] =
		    /*_.MELT_DEBUG_FUN__V60*/ meltfptr[59];;
		  /*epilog */

		  MELT_LOCATION ("warmelt-outobj.melt:5637:/ clear");
		 /*clear *//*_#THE_MELTCALLCOUNT__L27*/ meltfnum[26] = 0;
		  /*^clear */
		 /*clear *//*_#LIST_LENGTH__L28*/ meltfnum[27] = 0;
		  /*^clear */
		 /*clear *//*_#LIST_LENGTH__L29*/ meltfnum[28] = 0;
		  /*^clear */
		 /*clear *//*_.MELT_DEBUG_FUN__V60*/ meltfptr[59] = 0;
		}
		;
	      }
	    else
	      {			/*^cond.else */

      /*_.IF___V59*/ meltfptr[58] = NULL;;
	      }
	    ;
	    MELT_LOCATION ("warmelt-outobj.melt:5637:/ quasiblock");


	    /*_.PROGN___V61*/ meltfptr[59] = /*_.IF___V59*/ meltfptr[58];;
	    /*^compute */
	    /*_.IFCPP___V58*/ meltfptr[54] = /*_.PROGN___V61*/ meltfptr[59];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-outobj.melt:5637:/ clear");
	       /*clear *//*_#MELT_NEED_DBG__L26*/ meltfnum[1] = 0;
	    /*^clear */
	       /*clear *//*_.IF___V59*/ meltfptr[58] = 0;
	    /*^clear */
	       /*clear *//*_.PROGN___V61*/ meltfptr[59] = 0;
	  }

#else /*MELT_HAVE_DEBUG */
	  /*^cppif.else */
	  /*_.IFCPP___V58*/ meltfptr[54] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
	  ;
	  MELT_LOCATION ("warmelt-outobj.melt:5640:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^apply */
	  /*apply */
	  {
	    union meltparam_un argtab[4];
	    memset (&argtab, 0, sizeof (argtab));
	    /*^apply.arg */
	    argtab[0].meltbp_aptr =
	      (melt_ptr_t *) & /*_.BINDLIST__V41*/ meltfptr[40];
	    /*^apply.arg */
	    argtab[1].meltbp_aptr =
	      (melt_ptr_t *) & /*_.MODCTX__V4*/ meltfptr[3];
	    /*^apply.arg */
	    argtab[2].meltbp_aptr =
	      (melt_ptr_t *) & /*_.NCX__V5*/ meltfptr[4];
	    /*^apply.arg */
	    argtab[3].meltbp_aptr =
	      (melt_ptr_t *) & /*_.INIENV__V6*/ meltfptr[5];
	    /*_.NORMALISTHANDLER__V62*/ meltfptr[58] =
	      melt_apply ((meltclosure_ptr_t)
			  ( /*_.NORMALISTHANDLER__V7*/ meltfptr[6]),
			  (melt_ptr_t) ( /*_.NORMLIST__V40*/ meltfptr[36]),
			  (MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_PTR
			   MELTBPARSTR_PTR ""), argtab, "",
			  (union meltparam_un *) 0);
	  }
	  ;

#if MELT_HAVE_DEBUG
	  MELT_LOCATION ("warmelt-outobj.melt:5641:/ cppif.then");
	  /*^block */
	  /*anyblock */
	  {


	    {
	      /*^locexp */
	      /*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	      melt_dbgcounter++;
#endif
	      ;
	    }
	    ;
	    /*^checksignal */
	    MELT_CHECK_SIGNAL ();
	    ;
     /*_#MELT_NEED_DBG__L30*/ meltfnum[26] =
	      /*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	      ( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	      0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	      ;;
	    MELT_LOCATION ("warmelt-outobj.melt:5641:/ cond");
	    /*cond */ if ( /*_#MELT_NEED_DBG__L30*/ meltfnum[26])	/*then */
	      {
		/*^cond.then */
		/*^block */
		/*anyblock */
		{

       /*_#THE_MELTCALLCOUNT__L31*/ meltfnum[27] =
#ifdef meltcallcount
		    meltcallcount	/* the_meltcallcount */
#else
		    0L
#endif /* meltcallcount the_meltcallcount */
		    ;;
		  /*^compute */
       /*_#LIST_LENGTH__L32*/ meltfnum[28] =
		    (melt_list_length
		     ((melt_ptr_t) ( /*_.NORMLIST__V40*/ meltfptr[36])));;
		  /*^compute */
       /*_#LIST_LENGTH__L33*/ meltfnum[1] =
		    (melt_list_length
		     ((melt_ptr_t) ( /*_.BINDLIST__V41*/ meltfptr[40])));;
		  MELT_LOCATION ("warmelt-outobj.melt:5641:/ checksignal");
		  MELT_CHECK_SIGNAL ();
		  ;
		  /*^apply */
		  /*apply */
		  {
		    union meltparam_un argtab[11];
		    memset (&argtab, 0, sizeof (argtab));
		    /*^apply.arg */
		    argtab[0].meltbp_long =
		      /*_#THE_MELTCALLCOUNT__L31*/ meltfnum[27];
		    /*^apply.arg */
		    argtab[1].meltbp_cstring = "warmelt-outobj.melt";
		    /*^apply.arg */
		    argtab[2].meltbp_long = 5641;
		    /*^apply.arg */
		    argtab[3].meltbp_cstring =
		      "translate_macroexpanded_list after calling normalisthandler normlist=";
		    /*^apply.arg */
		    argtab[4].meltbp_aptr =
		      (melt_ptr_t *) & /*_.NORMLIST__V40*/ meltfptr[36];
		    /*^apply.arg */
		    argtab[5].meltbp_cstring = "\n of length ";
		    /*^apply.arg */
		    argtab[6].meltbp_long =
		      /*_#LIST_LENGTH__L32*/ meltfnum[28];
		    /*^apply.arg */
		    argtab[7].meltbp_cstring = "\n bindlist=";
		    /*^apply.arg */
		    argtab[8].meltbp_aptr =
		      (melt_ptr_t *) & /*_.BINDLIST__V41*/ meltfptr[40];
		    /*^apply.arg */
		    argtab[9].meltbp_cstring = "\n of length ";
		    /*^apply.arg */
		    argtab[10].meltbp_long =
		      /*_#LIST_LENGTH__L33*/ meltfnum[1];
		    /*_.MELT_DEBUG_FUN__V65*/ meltfptr[64] =
		      melt_apply ((meltclosure_ptr_t)
				  (( /*!MELT_DEBUG_FUN */ meltfrout->
				    tabval[0])),
				  (melt_ptr_t) (( /*nil */ NULL)),
				  (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_PTR MELTBPARSTR_CSTRING
				   MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_PTR MELTBPARSTR_CSTRING
				   MELTBPARSTR_LONG ""), argtab, "",
				  (union meltparam_un *) 0);
		  }
		  ;
		  /*_.IF___V64*/ meltfptr[63] =
		    /*_.MELT_DEBUG_FUN__V65*/ meltfptr[64];;
		  /*epilog */

		  MELT_LOCATION ("warmelt-outobj.melt:5641:/ clear");
		 /*clear *//*_#THE_MELTCALLCOUNT__L31*/ meltfnum[27] = 0;
		  /*^clear */
		 /*clear *//*_#LIST_LENGTH__L32*/ meltfnum[28] = 0;
		  /*^clear */
		 /*clear *//*_#LIST_LENGTH__L33*/ meltfnum[1] = 0;
		  /*^clear */
		 /*clear *//*_.MELT_DEBUG_FUN__V65*/ meltfptr[64] = 0;
		}
		;
	      }
	    else
	      {			/*^cond.else */

      /*_.IF___V64*/ meltfptr[63] = NULL;;
	      }
	    ;
	    MELT_LOCATION ("warmelt-outobj.melt:5641:/ quasiblock");


	    /*_.PROGN___V66*/ meltfptr[64] = /*_.IF___V64*/ meltfptr[63];;
	    /*^compute */
	    /*_.IFCPP___V63*/ meltfptr[59] = /*_.PROGN___V66*/ meltfptr[64];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-outobj.melt:5641:/ clear");
	       /*clear *//*_#MELT_NEED_DBG__L30*/ meltfnum[26] = 0;
	    /*^clear */
	       /*clear *//*_.IF___V64*/ meltfptr[63] = 0;
	    /*^clear */
	       /*clear *//*_.PROGN___V66*/ meltfptr[64] = 0;
	  }

#else /*MELT_HAVE_DEBUG */
	  /*^cppif.else */
	  /*_.IFCPP___V63*/ meltfptr[59] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
	  ;
	  MELT_LOCATION ("warmelt-outobj.melt:5636:/ quasiblock");


	  /*_.PROGN___V67*/ meltfptr[63] = /*_.IFCPP___V63*/ meltfptr[59];;
	  /*^compute */
	  /*_.IF___V57*/ meltfptr[53] = /*_.PROGN___V67*/ meltfptr[63];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-outobj.melt:5636:/ clear");
	     /*clear *//*_.IFCPP___V58*/ meltfptr[54] = 0;
	  /*^clear */
	     /*clear *//*_.NORMALISTHANDLER__V62*/ meltfptr[58] = 0;
	  /*^clear */
	     /*clear *//*_.IFCPP___V63*/ meltfptr[59] = 0;
	  /*^clear */
	     /*clear *//*_.PROGN___V67*/ meltfptr[63] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

  /*_.IF___V57*/ meltfptr[53] = NULL;;
      }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:5647:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#MELT_ERROR_COUNTER__L34*/ meltfnum[27] =
      melt_error_counter;;
    MELT_LOCATION ("warmelt-outobj.melt:5647:/ cond");
    /*cond */ if ( /*_#MELT_ERROR_COUNTER__L34*/ meltfnum[27])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-outobj.melt:5648:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
   /*_#IS_CLOSURE__L35*/ meltfnum[28] =
	    (melt_magic_discr
	     ((melt_ptr_t) ( /*_.ERRORHDLR__V30*/ meltfptr[29])) ==
	     MELTOBMAG_CLOSURE);;
	  MELT_LOCATION ("warmelt-outobj.melt:5648:/ cond");
	  /*cond */ if ( /*_#IS_CLOSURE__L35*/ meltfnum[28])	/*then */
	    {
	      /*^cond.then */
	      /*^block */
	      /*anyblock */
	      {

		MELT_LOCATION ("warmelt-outobj.melt:5649:/ checksignal");
		MELT_CHECK_SIGNAL ();
		;
		/*^apply */
		/*apply */
		{
		  /*_.ERRORHDLR__V70*/ meltfptr[58] =
		    melt_apply ((meltclosure_ptr_t)
				( /*_.ERRORHDLR__V30*/ meltfptr[29]),
				(melt_ptr_t) (( /*!konst_6 */ meltfrout->
					       tabval[6])), (""),
				(union meltparam_un *) 0, "",
				(union meltparam_un *) 0);
		}
		;
		/*_.IF___V69*/ meltfptr[54] =
		  /*_.ERRORHDLR__V70*/ meltfptr[58];;
		/*epilog */

		MELT_LOCATION ("warmelt-outobj.melt:5648:/ clear");
	       /*clear *//*_.ERRORHDLR__V70*/ meltfptr[58] = 0;
	      }
	      ;
	    }
	  else
	    {			/*^cond.else */

    /*_.IF___V69*/ meltfptr[54] = NULL;;
	    }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:5651:/ locexp");
	    /* translate_macroexpanded_list ERROREDNORMAL_WARNCHK__1 */
	    warning (0,
		     "MELT translation of %s got after normalization %ld MELT errors",
		     melt_string_str ((melt_ptr_t) /*_.MODNAMSTR__V3*/
				      meltfptr[2]), melt_error_counter);
	    ;
	  }
	  ;
	  MELT_LOCATION ("warmelt-outobj.melt:5655:/ quasiblock");


   /*_.RETVAL___V1*/ meltfptr[0] = NULL;;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:5655:/ locexp");
	    /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
	    if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	      melt_warn_for_no_expected_secondary_results ();
	    /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
	    ;
	  }
	  ;
	  /*^finalreturn */
	  ;
	  /*finalret */ goto labend_rout;
	  MELT_LOCATION ("warmelt-outobj.melt:5647:/ quasiblock");


	  /*_.PROGN___V72*/ meltfptr[63] = /*_.RETURN___V71*/ meltfptr[59];;
	  /*^compute */
	  /*_.IF___V68*/ meltfptr[64] = /*_.PROGN___V72*/ meltfptr[63];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-outobj.melt:5647:/ clear");
	     /*clear *//*_#IS_CLOSURE__L35*/ meltfnum[28] = 0;
	  /*^clear */
	     /*clear *//*_.IF___V69*/ meltfptr[54] = 0;
	  /*^clear */
	     /*clear *//*_.RETURN___V71*/ meltfptr[59] = 0;
	  /*^clear */
	     /*clear *//*_.PROGN___V72*/ meltfptr[63] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

  /*_.IF___V68*/ meltfptr[64] = NULL;;
      }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:5658:/ quasiblock");


    /*^getslot */
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.NCX__V5*/ meltfptr[4]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 1, "NCTX_PROCLIST");
  /*_.PROLIST__V74*/ meltfptr[54] = slot;
    };
    ;
 /*_.OBJLIST__V75*/ meltfptr[59] =
      (meltgc_new_list
       ((meltobject_ptr_t) (( /*!DISCR_LIST */ meltfrout->tabval[7]))));;
    /*^compute */
 /*_#LIST_LENGTH__L36*/ meltfnum[1] =
      (melt_list_length ((melt_ptr_t) ( /*_.XLIST__V2*/ meltfptr[1])));;
    /*^compute */
 /*_#I__L37*/ meltfnum[26] =
      ((20) * ( /*_#LIST_LENGTH__L36*/ meltfnum[1]));;
    /*^compute */
 /*_#I__L38*/ meltfnum[28] =
      ((10) + ( /*_#I__L37*/ meltfnum[26]));;
    /*^compute */
 /*_.COMPICACHE__V76*/ meltfptr[63] =
      (meltgc_new_mapobjects
       ((meltobject_ptr_t) (( /*!DISCR_MAP_OBJECTS */ meltfrout->tabval[8])),
	( /*_#I__L38*/ meltfnum[28])));;
    /*^compute */
 /*_.COUNTBOX__V77*/ meltfptr[76] =
      (meltgc_new_int
       ((meltobject_ptr_t) (( /*!DISCR_INTEGER */ meltfrout->tabval[9])),
	(0)));;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:5663:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L39*/ meltfnum[38] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-outobj.melt:5663:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L39*/ meltfnum[38])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L40*/ meltfnum[39] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-outobj.melt:5663:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long =
		/*_#THE_MELTCALLCOUNT__L40*/ meltfnum[39];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-outobj.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 5663;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring =
		"translate_macroexpanded_list prolist=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.PROLIST__V74*/ meltfptr[54];
	      /*_.MELT_DEBUG_FUN__V80*/ meltfptr[79] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V79*/ meltfptr[78] =
	      /*_.MELT_DEBUG_FUN__V80*/ meltfptr[79];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-outobj.melt:5663:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L40*/ meltfnum[39] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V80*/ meltfptr[79] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V79*/ meltfptr[78] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-outobj.melt:5663:/ quasiblock");


      /*_.PROGN___V81*/ meltfptr[79] = /*_.IF___V79*/ meltfptr[78];;
      /*^compute */
      /*_.IFCPP___V78*/ meltfptr[77] = /*_.PROGN___V81*/ meltfptr[79];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:5663:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L39*/ meltfnum[38] = 0;
      /*^clear */
	     /*clear *//*_.IF___V79*/ meltfptr[78] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V81*/ meltfptr[79] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V78*/ meltfptr[77] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:5666:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_LIST__L41*/ meltfnum[39] =
	(melt_magic_discr ((melt_ptr_t) ( /*_.PROLIST__V74*/ meltfptr[54])) ==
	 MELTOBMAG_LIST);;
      MELT_LOCATION ("warmelt-outobj.melt:5666:/ cond");
      /*cond */ if ( /*_#IS_LIST__L41*/ meltfnum[39])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V83*/ meltfptr[79] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-outobj.melt:5666:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check prolist"),
				  ("warmelt-outobj.melt")
				  ? ("warmelt-outobj.melt") : __FILE__,
				  (5666) ? (5666) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V83*/ meltfptr[79] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V82*/ meltfptr[78] = /*_.IFELSE___V83*/ meltfptr[79];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:5666:/ clear");
	     /*clear *//*_#IS_LIST__L41*/ meltfnum[39] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V83*/ meltfptr[79] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V82*/ meltfptr[78] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-outobj.melt:5669:/ quasiblock");


    /*^newclosure */
		 /*newclosure *//*_.LAMBDA___V85*/ meltfptr[84] =
      (melt_ptr_t)
      meltgc_new_closure ((meltobject_ptr_t)
			  (((melt_ptr_t) (MELT_PREDEF (DISCR_CLOSURE)))),
			  (meltroutine_ptr_t) (( /*!konst_14 */ meltfrout->
						tabval[14])), (4));
    ;
    /*^putclosedv */
    /*putclosv */
    melt_assertmsg ("putclosv checkclo",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.LAMBDA___V85*/ meltfptr[84])) ==
		    MELTOBMAG_CLOSURE);
    melt_assertmsg ("putclosv checkoff", 0 >= 0
		    && 0 <
		    melt_closure_size ((melt_ptr_t)
				       ( /*_.LAMBDA___V85*/ meltfptr[84])));
    ((meltclosure_ptr_t) /*_.LAMBDA___V85*/ meltfptr[84])->tabval[0] =
      (melt_ptr_t) ( /*_.COUNTBOX__V77*/ meltfptr[76]);
    ;
    /*^putclosedv */
    /*putclosv */
    melt_assertmsg ("putclosv checkclo",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.LAMBDA___V85*/ meltfptr[84])) ==
		    MELTOBMAG_CLOSURE);
    melt_assertmsg ("putclosv checkoff", 1 >= 0
		    && 1 <
		    melt_closure_size ((melt_ptr_t)
				       ( /*_.LAMBDA___V85*/ meltfptr[84])));
    ((meltclosure_ptr_t) /*_.LAMBDA___V85*/ meltfptr[84])->tabval[1] =
      (melt_ptr_t) ( /*_.MODCTX__V4*/ meltfptr[3]);
    ;
    /*^putclosedv */
    /*putclosv */
    melt_assertmsg ("putclosv checkclo",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.LAMBDA___V85*/ meltfptr[84])) ==
		    MELTOBMAG_CLOSURE);
    melt_assertmsg ("putclosv checkoff", 2 >= 0
		    && 2 <
		    melt_closure_size ((melt_ptr_t)
				       ( /*_.LAMBDA___V85*/ meltfptr[84])));
    ((meltclosure_ptr_t) /*_.LAMBDA___V85*/ meltfptr[84])->tabval[2] =
      (melt_ptr_t) ( /*_.COMPICACHE__V76*/ meltfptr[63]);
    ;
    /*^putclosedv */
    /*putclosv */
    melt_assertmsg ("putclosv checkclo",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.LAMBDA___V85*/ meltfptr[84])) ==
		    MELTOBMAG_CLOSURE);
    melt_assertmsg ("putclosv checkoff", 3 >= 0
		    && 3 <
		    melt_closure_size ((melt_ptr_t)
				       ( /*_.LAMBDA___V85*/ meltfptr[84])));
    ((meltclosure_ptr_t) /*_.LAMBDA___V85*/ meltfptr[84])->tabval[3] =
      (melt_ptr_t) ( /*_.OBJLIST__V75*/ meltfptr[59]);
    ;
    /*_.LAMBDA___V84*/ meltfptr[79] = /*_.LAMBDA___V85*/ meltfptr[84];;
    MELT_LOCATION ("warmelt-outobj.melt:5667:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & /*_.LAMBDA___V84*/ meltfptr[79];
      /*_.LIST_EVERY__V86*/ meltfptr[85] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!LIST_EVERY */ meltfrout->tabval[10])),
		    (melt_ptr_t) ( /*_.PROLIST__V74*/ meltfptr[54]),
		    (MELTBPARSTR_PTR ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:5676:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#MELT_ERROR_COUNTER__L42*/ meltfnum[38] =
      melt_error_counter;;
    MELT_LOCATION ("warmelt-outobj.melt:5676:/ cond");
    /*cond */ if ( /*_#MELT_ERROR_COUNTER__L42*/ meltfnum[38])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-outobj.melt:5677:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
   /*_#IS_CLOSURE__L43*/ meltfnum[39] =
	    (melt_magic_discr
	     ((melt_ptr_t) ( /*_.ERRORHDLR__V30*/ meltfptr[29])) ==
	     MELTOBMAG_CLOSURE);;
	  MELT_LOCATION ("warmelt-outobj.melt:5677:/ cond");
	  /*cond */ if ( /*_#IS_CLOSURE__L43*/ meltfnum[39])	/*then */
	    {
	      /*^cond.then */
	      /*^block */
	      /*anyblock */
	      {

		MELT_LOCATION ("warmelt-outobj.melt:5678:/ checksignal");
		MELT_CHECK_SIGNAL ();
		;
		/*^apply */
		/*apply */
		{
		  /*_.ERRORHDLR__V89*/ meltfptr[88] =
		    melt_apply ((meltclosure_ptr_t)
				( /*_.ERRORHDLR__V30*/ meltfptr[29]),
				(melt_ptr_t) (( /*!konst_15 */ meltfrout->
					       tabval[15])), (""),
				(union meltparam_un *) 0, "",
				(union meltparam_un *) 0);
		}
		;
		/*_.IF___V88*/ meltfptr[87] =
		  /*_.ERRORHDLR__V89*/ meltfptr[88];;
		/*epilog */

		MELT_LOCATION ("warmelt-outobj.melt:5677:/ clear");
	       /*clear *//*_.ERRORHDLR__V89*/ meltfptr[88] = 0;
	      }
	      ;
	    }
	  else
	    {			/*^cond.else */

    /*_.IF___V88*/ meltfptr[87] = NULL;;
	    }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:5680:/ locexp");
	    /* translate_macroexpanded_list ERROREDGENER_WARNCHK__1 */
	    warning (0,
		     "MELT translation of %s got after generation %ld MELT errors",
		     melt_string_str ((melt_ptr_t) /*_.MODNAMSTR__V3*/
				      meltfptr[2]), melt_error_counter);
	    ;
	  }
	  ;
	  MELT_LOCATION ("warmelt-outobj.melt:5684:/ quasiblock");


   /*_.RETVAL___V1*/ meltfptr[0] = NULL;;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:5684:/ locexp");
	    /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
	    if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	      melt_warn_for_no_expected_secondary_results ();
	    /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
	    ;
	  }
	  ;
	  /*^finalreturn */
	  ;
	  /*finalret */ goto labend_rout;
	  MELT_LOCATION ("warmelt-outobj.melt:5676:/ quasiblock");


	  /*_.PROGN___V91*/ meltfptr[90] = /*_.RETURN___V90*/ meltfptr[88];;
	  /*^compute */
	  /*_.IF___V87*/ meltfptr[86] = /*_.PROGN___V91*/ meltfptr[90];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-outobj.melt:5676:/ clear");
	     /*clear *//*_#IS_CLOSURE__L43*/ meltfnum[39] = 0;
	  /*^clear */
	     /*clear *//*_.IF___V88*/ meltfptr[87] = 0;
	  /*^clear */
	     /*clear *//*_.RETURN___V90*/ meltfptr[88] = 0;
	  /*^clear */
	     /*clear *//*_.PROGN___V91*/ meltfptr[90] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

  /*_.IF___V87*/ meltfptr[86] = NULL;;
      }
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:5687:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L44*/ meltfnum[39] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-outobj.melt:5687:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L44*/ meltfnum[39])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L45*/ meltfnum[44] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-outobj.melt:5687:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long =
		/*_#THE_MELTCALLCOUNT__L45*/ meltfnum[44];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-outobj.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 5687;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring =
		"translate_macroexpanded_list objlist=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.OBJLIST__V75*/ meltfptr[59];
	      /*_.MELT_DEBUG_FUN__V94*/ meltfptr[90] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V93*/ meltfptr[88] =
	      /*_.MELT_DEBUG_FUN__V94*/ meltfptr[90];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-outobj.melt:5687:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L45*/ meltfnum[44] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V94*/ meltfptr[90] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V93*/ meltfptr[88] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-outobj.melt:5687:/ quasiblock");


      /*_.PROGN___V95*/ meltfptr[90] = /*_.IF___V93*/ meltfptr[88];;
      /*^compute */
      /*_.IFCPP___V92*/ meltfptr[87] = /*_.PROGN___V95*/ meltfptr[90];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:5687:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L44*/ meltfnum[39] = 0;
      /*^clear */
	     /*clear *//*_.IF___V93*/ meltfptr[88] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V95*/ meltfptr[90] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V92*/ meltfptr[87] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:5688:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_LIST__L46*/ meltfnum[44] =
	(melt_magic_discr ((melt_ptr_t) ( /*_.OBJLIST__V75*/ meltfptr[59])) ==
	 MELTOBMAG_LIST);;
      MELT_LOCATION ("warmelt-outobj.melt:5688:/ cond");
      /*cond */ if ( /*_#IS_LIST__L46*/ meltfnum[44])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V97*/ meltfptr[90] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-outobj.melt:5688:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check objlist"),
				  ("warmelt-outobj.melt")
				  ? ("warmelt-outobj.melt") : __FILE__,
				  (5688) ? (5688) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V97*/ meltfptr[90] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V96*/ meltfptr[88] = /*_.IFELSE___V97*/ meltfptr[90];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:5688:/ clear");
	     /*clear *//*_#IS_LIST__L46*/ meltfnum[44] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V97*/ meltfptr[90] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V96*/ meltfptr[88] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-outobj.melt:5689:/ quasiblock");


    /*^getslot */
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.NCX__V5*/ meltfptr[4]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 0, "NCTX_INITPROC");
  /*_.INIPRO__V99*/ meltfptr[98] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-outobj.melt:5690:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.NCX__V5*/ meltfptr[4]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 2, "NCTX_DATALIST");
  /*_.INIDATA__V100*/ meltfptr[99] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-outobj.melt:5691:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.NCX__V5*/ meltfptr[4]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 3, "NCTX_VALUELIST");
  /*_.IMPORTVALUES__V101*/ meltfptr[100] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-outobj.melt:5692:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.NCX__V5*/ meltfptr[4]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 14, "NCTX_PROCURMODENVLIST");
  /*_.PROCURMODENVLIST__V102*/ meltfptr[101] = slot;
    };
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:5694:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L47*/ meltfnum[39] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-outobj.melt:5694:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L47*/ meltfnum[39])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L48*/ meltfnum[44] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    /*^compute */
     /*_.DISCRIM__V105*/ meltfptr[104] =
	      ((melt_ptr_t)
	       (melt_discr
		((melt_ptr_t) ( /*_.INIPRO__V99*/ meltfptr[98]))));;
	    MELT_LOCATION ("warmelt-outobj.melt:5694:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[11];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long =
		/*_#THE_MELTCALLCOUNT__L48*/ meltfnum[44];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-outobj.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 5694;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring =
		"translate_macroexpanded_list before compiling initproc inipro=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.INIPRO__V99*/ meltfptr[98];
	      /*^apply.arg */
	      argtab[5].meltbp_cstring = "\n of discrim=";
	      /*^apply.arg */
	      argtab[6].meltbp_aptr =
		(melt_ptr_t *) & /*_.DISCRIM__V105*/ meltfptr[104];
	      /*^apply.arg */
	      argtab[7].meltbp_cstring = "\n iniproctransl=";
	      /*^apply.arg */
	      argtab[8].meltbp_aptr =
		(melt_ptr_t *) & /*_.INIPROCTRANSL__V8*/ meltfptr[7];
	      /*^apply.arg */
	      argtab[9].meltbp_cstring = "\n normlist=";
	      /*^apply.arg */
	      argtab[10].meltbp_aptr =
		(melt_ptr_t *) & /*_.NORMLIST__V40*/ meltfptr[36];
	      /*_.MELT_DEBUG_FUN__V106*/ meltfptr[105] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V104*/ meltfptr[103] =
	      /*_.MELT_DEBUG_FUN__V106*/ meltfptr[105];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-outobj.melt:5694:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L48*/ meltfnum[44] = 0;
	    /*^clear */
	       /*clear *//*_.DISCRIM__V105*/ meltfptr[104] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V106*/ meltfptr[105] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V104*/ meltfptr[103] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-outobj.melt:5694:/ quasiblock");


      /*_.PROGN___V107*/ meltfptr[104] = /*_.IF___V104*/ meltfptr[103];;
      /*^compute */
      /*_.IFCPP___V103*/ meltfptr[102] = /*_.PROGN___V107*/ meltfptr[104];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:5694:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L47*/ meltfnum[39] = 0;
      /*^clear */
	     /*clear *//*_.IF___V104*/ meltfptr[103] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V107*/ meltfptr[104] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V103*/ meltfptr[102] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:5697:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
      /*^cond */
      /*cond */ if (
		     /*ifisa */
		     melt_is_instance_of ((melt_ptr_t)
					  ( /*_.INIPRO__V99*/ meltfptr[98]),
					  (melt_ptr_t) (( /*!CLASS_NREP_ANYPROC */ meltfrout->tabval[16])))
	)			/*then */
	{
	  /*^cond.then */
	  /*^getslot */
	  {
	    melt_ptr_t slot = NULL, obj = NULL;
	    obj = (melt_ptr_t) ( /*_.INIPRO__V99*/ meltfptr[98]) /*=obj*/ ;
	    melt_object_get_field (slot, obj, 1, "NPROC_BODY");
     /*_.NPROC_BODY__V109*/ meltfptr[103] = slot;
	  };
	  ;
	}
      else
	{			/*^cond.else */

    /*_.NPROC_BODY__V109*/ meltfptr[103] = NULL;;
	}
      ;
      /*^compute */
   /*_#NULL__L49*/ meltfnum[44] =
	(( /*_.NPROC_BODY__V109*/ meltfptr[103]) == NULL);;
      MELT_LOCATION ("warmelt-outobj.melt:5697:/ cond");
      /*cond */ if ( /*_#NULL__L49*/ meltfnum[44])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V110*/ meltfptr[104] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-outobj.melt:5697:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("null iniprobody"),
				  ("warmelt-outobj.melt")
				  ? ("warmelt-outobj.melt") : __FILE__,
				  (5697) ? (5697) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V110*/ meltfptr[104] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V108*/ meltfptr[105] = /*_.IFELSE___V110*/ meltfptr[104];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:5697:/ clear");
	     /*clear *//*_.NPROC_BODY__V109*/ meltfptr[103] = 0;
      /*^clear */
	     /*clear *//*_#NULL__L49*/ meltfnum[44] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V110*/ meltfptr[104] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V108*/ meltfptr[105] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-outobj.melt:5698:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^cond */
    /*cond */ if (
		   /*ifisa */
		   melt_is_instance_of ((melt_ptr_t)
					( /*_.INIPRO__V99*/ meltfptr[98]),
					(melt_ptr_t) (( /*!CLASS_NREP_ANYPROC */ meltfrout->tabval[16])))
      )				/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  /*^putslot */
	  /*putslot */
	  melt_assertmsg ("putslot checkobj @NPROC_BODY",
			  melt_magic_discr ((melt_ptr_t)
					    ( /*_.INIPRO__V99*/ meltfptr[98]))
			  == MELTOBMAG_OBJECT);
	  melt_putfield_object (( /*_.INIPRO__V99*/ meltfptr[98]), (1),
				( /*_.NORMLIST__V40*/ meltfptr[36]),
				"NPROC_BODY");
	  ;
	  /*^touch */
	  meltgc_touch ( /*_.INIPRO__V99*/ meltfptr[98]);
	  ;
	  /*^touchobj */

	  melt_dbgtrace_written_object ( /*_.INIPRO__V99*/ meltfptr[98],
					"put-fields");
	  ;
	  /*epilog */
	}
	;
      }				/*noelse */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:5699:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L50*/ meltfnum[39] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-outobj.melt:5699:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L50*/ meltfnum[39])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L51*/ meltfnum[44] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-outobj.melt:5699:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long =
		/*_#THE_MELTCALLCOUNT__L51*/ meltfnum[44];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-outobj.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 5699;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring =
		"translate_macroexpanded_list unpdated inipro=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.INIPRO__V99*/ meltfptr[98];
	      /*_.MELT_DEBUG_FUN__V113*/ meltfptr[112] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V112*/ meltfptr[104] =
	      /*_.MELT_DEBUG_FUN__V113*/ meltfptr[112];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-outobj.melt:5699:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L51*/ meltfnum[44] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V113*/ meltfptr[112] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V112*/ meltfptr[104] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-outobj.melt:5699:/ quasiblock");


      /*_.PROGN___V114*/ meltfptr[112] = /*_.IF___V112*/ meltfptr[104];;
      /*^compute */
      /*_.IFCPP___V111*/ meltfptr[103] = /*_.PROGN___V114*/ meltfptr[112];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:5699:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L50*/ meltfnum[39] = 0;
      /*^clear */
	     /*clear *//*_.IF___V112*/ meltfptr[104] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V114*/ meltfptr[112] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V111*/ meltfptr[103] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:5700:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L52*/ meltfnum[44] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.INIPRO__V99*/ meltfptr[98]),
			     (melt_ptr_t) (( /*!CLASS_NREP_INITPROC */
					    meltfrout->tabval[17])));;
      MELT_LOCATION ("warmelt-outobj.melt:5700:/ cond");
      /*cond */ if ( /*_#IS_A__L52*/ meltfnum[44])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V116*/ meltfptr[112] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-outobj.melt:5700:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check inipro"),
				  ("warmelt-outobj.melt")
				  ? ("warmelt-outobj.melt") : __FILE__,
				  (5700) ? (5700) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V116*/ meltfptr[112] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V115*/ meltfptr[104] = /*_.IFELSE___V116*/ meltfptr[112];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:5700:/ clear");
	     /*clear *//*_#IS_A__L52*/ meltfnum[44] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V116*/ meltfptr[112] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V115*/ meltfptr[104] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:5701:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_CLOSURE__L53*/ meltfnum[39] =
	(melt_magic_discr
	 ((melt_ptr_t) ( /*_.INIPROCTRANSL__V8*/ meltfptr[7])) ==
	 MELTOBMAG_CLOSURE);;
      MELT_LOCATION ("warmelt-outobj.melt:5701:/ cond");
      /*cond */ if ( /*_#IS_CLOSURE__L53*/ meltfnum[39])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V118*/ meltfptr[117] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-outobj.melt:5701:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check iniproctransl"),
				  ("warmelt-outobj.melt")
				  ? ("warmelt-outobj.melt") : __FILE__,
				  (5701) ? (5701) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V118*/ meltfptr[117] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V117*/ meltfptr[112] = /*_.IFELSE___V118*/ meltfptr[117];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:5701:/ clear");
	     /*clear *//*_#IS_CLOSURE__L53*/ meltfnum[39] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V118*/ meltfptr[117] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V117*/ meltfptr[112] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:5702:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L54*/ meltfnum[44] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-outobj.melt:5702:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L54*/ meltfnum[44])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L55*/ meltfnum[39] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-outobj.melt:5702:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[7];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long =
		/*_#THE_MELTCALLCOUNT__L55*/ meltfnum[39];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-outobj.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 5702;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring =
		"translate_macroexpanded_list inipro= ";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.INIPRO__V99*/ meltfptr[98];
	      /*^apply.arg */
	      argtab[5].meltbp_cstring = "\n before calling iniproctransl=";
	      /*^apply.arg */
	      argtab[6].meltbp_aptr =
		(melt_ptr_t *) & /*_.INIPROCTRANSL__V8*/ meltfptr[7];
	      /*_.MELT_DEBUG_FUN__V121*/ meltfptr[120] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V120*/ meltfptr[119] =
	      /*_.MELT_DEBUG_FUN__V121*/ meltfptr[120];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-outobj.melt:5702:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L55*/ meltfnum[39] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V121*/ meltfptr[120] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V120*/ meltfptr[119] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-outobj.melt:5702:/ quasiblock");


      /*_.PROGN___V122*/ meltfptr[120] = /*_.IF___V120*/ meltfptr[119];;
      /*^compute */
      /*_.IFCPP___V119*/ meltfptr[117] = /*_.PROGN___V122*/ meltfptr[120];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:5702:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L54*/ meltfnum[44] = 0;
      /*^clear */
	     /*clear *//*_.IF___V120*/ meltfptr[119] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V122*/ meltfptr[120] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V119*/ meltfptr[117] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-outobj.melt:5704:/ quasiblock");


    MELT_LOCATION ("warmelt-outobj.melt:5705:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[5];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.MODCTX__V4*/ meltfptr[3];
      /*^apply.arg */
      argtab[1].meltbp_aptr =
	(melt_ptr_t *) & /*_.INIDATA__V100*/ meltfptr[99];
      /*^apply.arg */
      argtab[2].meltbp_aptr =
	(melt_ptr_t *) & /*_.COMPICACHE__V76*/ meltfptr[63];
      /*^apply.arg */
      argtab[3].meltbp_aptr =
	(melt_ptr_t *) & /*_.PROCURMODENVLIST__V102*/ meltfptr[101];
      /*^apply.arg */
      argtab[4].meltbp_aptr =
	(melt_ptr_t *) & /*_.IMPORTVALUES__V101*/ meltfptr[100];
      /*_.INIOBJ__V124*/ meltfptr[120] =
	melt_apply ((meltclosure_ptr_t)
		    ( /*_.INIPROCTRANSL__V8*/ meltfptr[7]),
		    (melt_ptr_t) ( /*_.INIPRO__V99*/ meltfptr[98]),
		    (MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_PTR
		     MELTBPARSTR_PTR MELTBPARSTR_PTR ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:5707:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L56*/ meltfnum[39] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-outobj.melt:5707:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L56*/ meltfnum[39])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L57*/ meltfnum[44] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-outobj.melt:5707:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[7];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long =
		/*_#THE_MELTCALLCOUNT__L57*/ meltfnum[44];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-outobj.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 5707;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring =
		"translate_macroexpanded_list iniobj=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.INIOBJ__V124*/ meltfptr[120];
	      /*^apply.arg */
	      argtab[5].meltbp_cstring = "\n objlist=";
	      /*^apply.arg */
	      argtab[6].meltbp_aptr =
		(melt_ptr_t *) & /*_.OBJLIST__V75*/ meltfptr[59];
	      /*_.MELT_DEBUG_FUN__V127*/ meltfptr[126] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V126*/ meltfptr[125] =
	      /*_.MELT_DEBUG_FUN__V127*/ meltfptr[126];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-outobj.melt:5707:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L57*/ meltfnum[44] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V127*/ meltfptr[126] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V126*/ meltfptr[125] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-outobj.melt:5707:/ quasiblock");


      /*_.PROGN___V128*/ meltfptr[126] = /*_.IF___V126*/ meltfptr[125];;
      /*^compute */
      /*_.IFCPP___V125*/ meltfptr[124] = /*_.PROGN___V128*/ meltfptr[126];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:5707:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L56*/ meltfnum[39] = 0;
      /*^clear */
	     /*clear *//*_.IF___V126*/ meltfptr[125] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V128*/ meltfptr[126] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V125*/ meltfptr[124] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    /*citerblock FOREACH_IN_LIST */
    {
      /* start foreach_in_list meltcit1__EACHLIST */
      for ( /*_.PAIREL__V129*/ meltfptr[125] =
	   melt_list_first ((melt_ptr_t) /*_.OBJLIST__V75*/ meltfptr[59]);
	   melt_magic_discr ((melt_ptr_t) /*_.PAIREL__V129*/ meltfptr[125]) ==
	   MELTOBMAG_PAIR;
	   /*_.PAIREL__V129*/ meltfptr[125] =
	   melt_pair_tail ((melt_ptr_t) /*_.PAIREL__V129*/ meltfptr[125]))
	{
	  /*_.OBEL__V130*/ meltfptr[126] =
	    melt_pair_head ((melt_ptr_t) /*_.PAIREL__V129*/ meltfptr[125]);



#if MELT_HAVE_DEBUG
	  MELT_LOCATION ("warmelt-outobj.melt:5713:/ cppif.then");
	  /*^block */
	  /*anyblock */
	  {


	    {
	      /*^locexp */
	      /*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	      melt_dbgcounter++;
#endif
	      ;
	    }
	    ;
	    /*^checksignal */
	    MELT_CHECK_SIGNAL ();
	    ;
    /*_#MELT_NEED_DBG__L58*/ meltfnum[44] =
	      /*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	      ( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	      0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	      ;;
	    MELT_LOCATION ("warmelt-outobj.melt:5713:/ cond");
	    /*cond */ if ( /*_#MELT_NEED_DBG__L58*/ meltfnum[44])	/*then */
	      {
		/*^cond.then */
		/*^block */
		/*anyblock */
		{

      /*_#THE_MELTCALLCOUNT__L59*/ meltfnum[39] =
#ifdef meltcallcount
		    meltcallcount	/* the_meltcallcount */
#else
		    0L
#endif /* meltcallcount the_meltcallcount */
		    ;;
		  MELT_LOCATION ("warmelt-outobj.melt:5713:/ checksignal");
		  MELT_CHECK_SIGNAL ();
		  ;
		  /*^apply */
		  /*apply */
		  {
		    union meltparam_un argtab[5];
		    memset (&argtab, 0, sizeof (argtab));
		    /*^apply.arg */
		    argtab[0].meltbp_long =
		      /*_#THE_MELTCALLCOUNT__L59*/ meltfnum[39];
		    /*^apply.arg */
		    argtab[1].meltbp_cstring = "warmelt-outobj.melt";
		    /*^apply.arg */
		    argtab[2].meltbp_long = 5713;
		    /*^apply.arg */
		    argtab[3].meltbp_cstring =
		      "translate_macroexpanded_list obel=";
		    /*^apply.arg */
		    argtab[4].meltbp_aptr =
		      (melt_ptr_t *) & /*_.OBEL__V130*/ meltfptr[126];
		    /*_.MELT_DEBUG_FUN__V133*/ meltfptr[132] =
		      melt_apply ((meltclosure_ptr_t)
				  (( /*!MELT_DEBUG_FUN */ meltfrout->
				    tabval[0])),
				  (melt_ptr_t) (( /*nil */ NULL)),
				  (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_PTR ""), argtab, "",
				  (union meltparam_un *) 0);
		  }
		  ;
		  /*_.IF___V132*/ meltfptr[131] =
		    /*_.MELT_DEBUG_FUN__V133*/ meltfptr[132];;
		  /*epilog */

		  MELT_LOCATION ("warmelt-outobj.melt:5713:/ clear");
		/*clear *//*_#THE_MELTCALLCOUNT__L59*/ meltfnum[39] = 0;
		  /*^clear */
		/*clear *//*_.MELT_DEBUG_FUN__V133*/ meltfptr[132] = 0;
		}
		;
	      }
	    else
	      {			/*^cond.else */

     /*_.IF___V132*/ meltfptr[131] = NULL;;
	      }
	    ;
	    MELT_LOCATION ("warmelt-outobj.melt:5713:/ quasiblock");


	    /*_.PROGN___V134*/ meltfptr[132] = /*_.IF___V132*/ meltfptr[131];;
	    /*^compute */
	    /*_.IFCPP___V131*/ meltfptr[130] =
	      /*_.PROGN___V134*/ meltfptr[132];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-outobj.melt:5713:/ clear");
	      /*clear *//*_#MELT_NEED_DBG__L58*/ meltfnum[44] = 0;
	    /*^clear */
	      /*clear *//*_.IF___V132*/ meltfptr[131] = 0;
	    /*^clear */
	      /*clear *//*_.PROGN___V134*/ meltfptr[132] = 0;
	  }

#else /*MELT_HAVE_DEBUG */
	  /*^cppif.else */
	  /*_.IFCPP___V131*/ meltfptr[130] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
	  ;
	  MELT_LOCATION ("warmelt-outobj.melt:5715:/ quasiblock");


	  MELT_LOCATION ("warmelt-outobj.melt:5716:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
  /*_#IS_A__L60*/ meltfnum[39] =
	    melt_is_instance_of ((melt_ptr_t)
				 ( /*_.OBEL__V130*/ meltfptr[126]),
				 (melt_ptr_t) (( /*!CLASS_PROCROUTINEOBJ */
						meltfrout->tabval[18])));;
	  MELT_LOCATION ("warmelt-outobj.melt:5716:/ cond");
	  /*cond */ if ( /*_#IS_A__L60*/ meltfnum[39])	/*then */
	    {
	      /*^cond.then */
	      /*^block */
	      /*anyblock */
	      {

		MELT_LOCATION ("warmelt-outobj.melt:5717:/ cond");
		/*cond */ if (
			       /*ifisa */
			       melt_is_instance_of ((melt_ptr_t)
						    ( /*_.OBEL__V130*/
						     meltfptr[126]),
						    (melt_ptr_t) (( /*!CLASS_PROCROUTINEOBJ */ meltfrout->tabval[18])))
		  )		/*then */
		  {
		    /*^cond.then */
		    /*^getslot */
		    {
		      melt_ptr_t slot = NULL, obj = NULL;
		      obj =
			(melt_ptr_t) ( /*_.OBEL__V130*/ meltfptr[126])
			/*=obj*/ ;
		      melt_object_get_field (slot, obj, 14, "OPROUT_FILENUM");
      /*_.OPROUT_FILENUM__V136*/ meltfptr[132] = slot;
		    };
		    ;
		  }
		else
		  {		/*^cond.else */

     /*_.OPROUT_FILENUM__V136*/ meltfptr[132] = NULL;;
		  }
		;
		/*^compute */
    /*_#GET_INT__L62*/ meltfnum[61] =
		  (melt_get_int
		   ((melt_ptr_t)
		    ( /*_.OPROUT_FILENUM__V136*/ meltfptr[132])));;
		/*^compute */
		/*_#FILNUM__L61*/ meltfnum[44] =
		  /*_#GET_INT__L62*/ meltfnum[61];;
		/*epilog */

		MELT_LOCATION ("warmelt-outobj.melt:5716:/ clear");
	      /*clear *//*_.OPROUT_FILENUM__V136*/ meltfptr[132] = 0;
		/*^clear */
	      /*clear *//*_#GET_INT__L62*/ meltfnum[61] = 0;
	      }
	      ;
	    }
	  else
	    {			/*^cond.else */

   /*_#FILNUM__L61*/ meltfnum[44] = 0;;
	    }
	  ;
	  MELT_LOCATION ("warmelt-outobj.melt:5719:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^cond */
	  /*cond */ if ( /*_#FILNUM__L61*/ meltfnum[44])	/*then */
	    {
	      /*^cond.then */
	      /*^block */
	      /*anyblock */
	      {

		/*^checksignal */
		MELT_CHECK_SIGNAL ();
		;
		/*^apply */
		/*apply */
		{
		  union meltparam_un argtab[3];
		  memset (&argtab, 0, sizeof (argtab));
		  /*^apply.arg */
		  argtab[0].meltbp_aptr =
		    (melt_ptr_t *) & /*_.MODNAMSTR__V3*/ meltfptr[2];
		  /*^apply.arg */
		  argtab[1].meltbp_aptr =
		    (melt_ptr_t *) & /*_.DECLBUF__V42*/ meltfptr[41];
		  /*^apply.arg */
		  argtab[2].meltbp_long = /*_#FILNUM__L61*/ meltfnum[44];
		  /*_.NTH_SECUNDARY_FILE__V138*/ meltfptr[137] =
		    melt_apply ((meltclosure_ptr_t)
				(( /*!NTH_SECUNDARY_FILE */ meltfrout->
				  tabval[19])),
				(melt_ptr_t) ( /*_.MODCTX__V4*/ meltfptr[3]),
				(MELTBPARSTR_PTR MELTBPARSTR_PTR
				 MELTBPARSTR_LONG ""), argtab, "",
				(union meltparam_un *) 0);
		}
		;
		/*_.SECFIL__V137*/ meltfptr[132] =
		  /*_.NTH_SECUNDARY_FILE__V138*/ meltfptr[137];;
		/*epilog */

		MELT_LOCATION ("warmelt-outobj.melt:5719:/ clear");
	      /*clear *//*_.NTH_SECUNDARY_FILE__V138*/ meltfptr[137] = 0;
	      }
	      ;
	    }
	  else
	    {			/*^cond.else */

   /*_.SECFIL__V137*/ meltfptr[132] = NULL;;
	    }
	  ;
	  MELT_LOCATION ("warmelt-outobj.melt:5721:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^cond */
	  /*cond */ if ( /*_.SECFIL__V137*/ meltfptr[132])	/*then */
	    {
	      /*^cond.then */
	      /*^block */
	      /*anyblock */
	      {

		/*^cond */
		/*cond */ if (
			       /*ifisa */
			       melt_is_instance_of ((melt_ptr_t)
						    ( /*_.SECFIL__V137*/
						     meltfptr[132]),
						    (melt_ptr_t) (( /*!CLASS_SECONDARY_C_FILE */ meltfrout->tabval[20])))
		  )		/*then */
		  {
		    /*^cond.then */
		    /*^getslot */
		    {
		      melt_ptr_t slot = NULL, obj = NULL;
		      obj =
			(melt_ptr_t) ( /*_.SECFIL__V137*/ meltfptr[132])
			/*=obj*/ ;
		      melt_object_get_field (slot, obj, 3, "SECFIL_IMPLBUF");
      /*_.SECFIL_IMPLBUF__V140*/ meltfptr[139] = slot;
		    };
		    ;
		  }
		else
		  {		/*^cond.else */

     /*_.SECFIL_IMPLBUF__V140*/ meltfptr[139] = NULL;;
		  }
		;
		/*^compute */
		/*_.OUTIMPLBUF__V139*/ meltfptr[137] =
		  /*_.SECFIL_IMPLBUF__V140*/ meltfptr[139];;
		/*epilog */

		MELT_LOCATION ("warmelt-outobj.melt:5721:/ clear");
	      /*clear *//*_.SECFIL_IMPLBUF__V140*/ meltfptr[139] = 0;
	      }
	      ;
	    }
	  else
	    {			/*^cond.else */

	      /*_.OUTIMPLBUF__V139*/ meltfptr[137] =
		/*_.IMPLBUF__V34*/ meltfptr[33];;
	    }
	  ;
	  MELT_LOCATION ("warmelt-outobj.melt:5723:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^msend */
	  /*msend */
	  {
	    union meltparam_un argtab[3];
	    memset (&argtab, 0, sizeof (argtab));
	    /*^ojbmsend.arg */
	    argtab[0].meltbp_aptr =
	      (melt_ptr_t *) & /*_.DECLBUF__V42*/ meltfptr[41];
	    /*^ojbmsend.arg */
	    argtab[1].meltbp_aptr =
	      (melt_ptr_t *) & /*_.OUTIMPLBUF__V139*/ meltfptr[137];
	    /*^ojbmsend.arg */
	    argtab[2].meltbp_long = 0;
	    /*_.OUTPUT_C_CODE__V141*/ meltfptr[139] =
	      meltgc_send ((melt_ptr_t) ( /*_.OBEL__V130*/ meltfptr[126]),
			   (melt_ptr_t) (( /*!OUTPUT_C_CODE */ meltfrout->
					  tabval[21])),
			   (MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_LONG
			    ""), argtab, "", (union meltparam_un *) 0);
	  }
	  ;
	  /*_.LET___V135*/ meltfptr[131] =
	    /*_.OUTPUT_C_CODE__V141*/ meltfptr[139];;

	  MELT_LOCATION ("warmelt-outobj.melt:5715:/ clear");
	    /*clear *//*_#IS_A__L60*/ meltfnum[39] = 0;
	  /*^clear */
	    /*clear *//*_#FILNUM__L61*/ meltfnum[44] = 0;
	  /*^clear */
	    /*clear *//*_.SECFIL__V137*/ meltfptr[132] = 0;
	  /*^clear */
	    /*clear *//*_.OUTIMPLBUF__V139*/ meltfptr[137] = 0;
	  /*^clear */
	    /*clear *//*_.OUTPUT_C_CODE__V141*/ meltfptr[139] = 0;
	}			/* end foreach_in_list meltcit1__EACHLIST */
     /*_.PAIREL__V129*/ meltfptr[125] = NULL;
     /*_.OBEL__V130*/ meltfptr[126] = NULL;


      /*citerepilog */

      MELT_LOCATION ("warmelt-outobj.melt:5710:/ clear");
	    /*clear *//*_.PAIREL__V129*/ meltfptr[125] = 0;
      /*^clear */
	    /*clear *//*_.OBEL__V130*/ meltfptr[126] = 0;
      /*^clear */
	    /*clear *//*_.IFCPP___V131*/ meltfptr[130] = 0;
      /*^clear */
	    /*clear *//*_.LET___V135*/ meltfptr[131] = 0;
    }				/*endciterblock FOREACH_IN_LIST */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:5725:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L63*/ meltfnum[61] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-outobj.melt:5725:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L63*/ meltfnum[61])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L64*/ meltfnum[39] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-outobj.melt:5725:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[7];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long =
		/*_#THE_MELTCALLCOUNT__L64*/ meltfnum[39];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-outobj.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 5725;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring =
		"translate_macroexpanded_list final modnamstr=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.MODNAMSTR__V3*/ meltfptr[2];
	      /*^apply.arg */
	      argtab[5].meltbp_cstring = " iniobj=";
	      /*^apply.arg */
	      argtab[6].meltbp_aptr =
		(melt_ptr_t *) & /*_.INIOBJ__V124*/ meltfptr[120];
	      /*_.MELT_DEBUG_FUN__V144*/ meltfptr[139] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V143*/ meltfptr[137] =
	      /*_.MELT_DEBUG_FUN__V144*/ meltfptr[139];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-outobj.melt:5725:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L64*/ meltfnum[39] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V144*/ meltfptr[139] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V143*/ meltfptr[137] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-outobj.melt:5725:/ quasiblock");


      /*_.PROGN___V145*/ meltfptr[139] = /*_.IF___V143*/ meltfptr[137];;
      /*^compute */
      /*_.IFCPP___V142*/ meltfptr[132] = /*_.PROGN___V145*/ meltfptr[139];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:5725:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L63*/ meltfnum[61] = 0;
      /*^clear */
	     /*clear *//*_.IF___V143*/ meltfptr[137] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V145*/ meltfptr[139] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V142*/ meltfptr[132] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:5726:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t)
				( /*_.IMPLBUF__V34*/ meltfptr[33]), (0), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:5727:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t)
				( /*_.IMPLBUF__V34*/ meltfptr[33]), (0), 0);
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:5729:/ quasiblock");


    /*^checksignal */
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & ( /*!DISCR_MULTIPLE */ meltfrout->tabval[23]);
      /*_.GENDEVTUP__V147*/ meltfptr[139] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!LIST_TO_MULTIPLE */ meltfrout->tabval[22])),
		    (melt_ptr_t) ( /*_.GENDEVLIST__V31*/ meltfptr[30]),
		    (MELTBPARSTR_PTR ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;
 /*_#NBGENDEV__L65*/ meltfnum[44] =
      (melt_multiple_length
       ((melt_ptr_t) ( /*_.GENDEVTUP__V147*/ meltfptr[139])));;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:5732:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L66*/ meltfnum[39] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-outobj.melt:5732:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L66*/ meltfnum[39])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L67*/ meltfnum[61] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-outobj.melt:5732:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[7];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long =
		/*_#THE_MELTCALLCOUNT__L67*/ meltfnum[61];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-outobj.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 5732;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring =
		"translate_macroexpanded_list before emit_syntax_testing_routine gendevtup=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.GENDEVTUP__V147*/ meltfptr[139];
	      /*^apply.arg */
	      argtab[5].meltbp_cstring = " gendevlist=";
	      /*^apply.arg */
	      argtab[6].meltbp_aptr =
		(melt_ptr_t *) & /*_.GENDEVLIST__V31*/ meltfptr[30];
	      /*_.MELT_DEBUG_FUN__V150*/ meltfptr[149] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V149*/ meltfptr[148] =
	      /*_.MELT_DEBUG_FUN__V150*/ meltfptr[149];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-outobj.melt:5732:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L67*/ meltfnum[61] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V150*/ meltfptr[149] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V149*/ meltfptr[148] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-outobj.melt:5732:/ quasiblock");


      /*_.PROGN___V151*/ meltfptr[149] = /*_.IF___V149*/ meltfptr[148];;
      /*^compute */
      /*_.IFCPP___V148*/ meltfptr[147] = /*_.PROGN___V151*/ meltfptr[149];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:5732:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L66*/ meltfnum[39] = 0;
      /*^clear */
	     /*clear *//*_.IF___V149*/ meltfptr[148] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V151*/ meltfptr[149] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V148*/ meltfptr[147] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-outobj.melt:5734:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^cond */
    /*cond */ if ( /*_#NBGENDEV__L65*/ meltfnum[44])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-outobj.melt:5735:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^apply */
	  /*apply */
	  {
	    union meltparam_un argtab[2];
	    memset (&argtab, 0, sizeof (argtab));
	    /*^apply.arg */
	    argtab[0].meltbp_aptr =
	      (melt_ptr_t *) & /*_.MODCTX__V4*/ meltfptr[3];
	    /*^apply.arg */
	    argtab[1].meltbp_aptr =
	      (melt_ptr_t *) & /*_.IMPLBUF__V34*/ meltfptr[33];
	    /*_.EMIT_SYNTAX_TESTING_ROUTINE__V153*/ meltfptr[149] =
	      melt_apply ((meltclosure_ptr_t)
			  (( /*!EMIT_SYNTAX_TESTING_ROUTINE */ meltfrout->
			    tabval[24])),
			  (melt_ptr_t) ( /*_.GENDEVTUP__V147*/ meltfptr[139]),
			  (MELTBPARSTR_PTR MELTBPARSTR_PTR ""), argtab, "",
			  (union meltparam_un *) 0);
	  }
	  ;
	  /*_.IFELSE___V152*/ meltfptr[148] =
	    /*_.EMIT_SYNTAX_TESTING_ROUTINE__V153*/ meltfptr[149];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-outobj.melt:5734:/ clear");
	     /*clear *//*_.EMIT_SYNTAX_TESTING_ROUTINE__V153*/ meltfptr[149] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

	/*^block */
	/*anyblock */
	{




	  {
	    MELT_LOCATION ("warmelt-outobj.melt:5736:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t)
				 ( /*_.IMPLBUF__V34*/ meltfptr[33]),
				 ("/*no syntax testing generated*/"));
	  }
	  ;
	     /*clear *//*_.IFELSE___V152*/ meltfptr[148] = 0;
	  /*epilog */
	}
	;
      }
    ;
    /*^compute */
    /*_.LET___V146*/ meltfptr[137] = /*_.IFELSE___V152*/ meltfptr[148];;

    MELT_LOCATION ("warmelt-outobj.melt:5729:/ clear");
	   /*clear *//*_.GENDEVTUP__V147*/ meltfptr[139] = 0;
    /*^clear */
	   /*clear *//*_#NBGENDEV__L65*/ meltfnum[44] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V148*/ meltfptr[147] = 0;
    /*^clear */
	   /*clear *//*_.IFELSE___V152*/ meltfptr[148] = 0;

    {
      MELT_LOCATION ("warmelt-outobj.melt:5739:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t)
				( /*_.IMPLBUF__V34*/ meltfptr[33]), (0), 0);
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:5741:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^msend */
    /*msend */
    {
      union meltparam_un argtab[3];
      memset (&argtab, 0, sizeof (argtab));
      /*^ojbmsend.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & /*_.DECLBUF__V42*/ meltfptr[41];
      /*^ojbmsend.arg */
      argtab[1].meltbp_aptr =
	(melt_ptr_t *) & /*_.IMPLBUF__V34*/ meltfptr[33];
      /*^ojbmsend.arg */
      argtab[2].meltbp_long = 0;
      /*_.OUTPUT_C_CODE__V154*/ meltfptr[149] =
	meltgc_send ((melt_ptr_t) ( /*_.INIOBJ__V124*/ meltfptr[120]),
		     (melt_ptr_t) (( /*!OUTPUT_C_CODE */ meltfrout->
				    tabval[21])),
		     (MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_LONG ""),
		     argtab, "", (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:5742:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[2];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & /*_.DECLBUF__V42*/ meltfptr[41];
      /*^apply.arg */
      argtab[1].meltbp_aptr =
	(melt_ptr_t *) & /*_.IMPLBUF__V34*/ meltfptr[33];
      /*_.OUTPUT_EXPORTED_OFFSETS__V155*/ meltfptr[139] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!OUTPUT_EXPORTED_OFFSETS */ meltfrout->tabval[25])),
		    (melt_ptr_t) ( /*_.MODCTX__V4*/ meltfptr[3]),
		    (MELTBPARSTR_PTR MELTBPARSTR_PTR ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:5744:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#STRBUF_USEDLENGTH__L68*/ meltfnum[61] =
      melt_strbuf_usedlength ((melt_ptr_t)
			      ( /*_.DECLBUF__V42*/ meltfptr[41]));;
    MELT_LOCATION ("warmelt-outobj.melt:5744:/ cond");
    /*cond */ if (
		   /*ifisa */
		   melt_is_instance_of ((melt_ptr_t)
					(( /*!BUFFER_LIMIT_CONT */ meltfrout->
					  tabval[26])),
					(melt_ptr_t) (( /*!CLASS_REFERENCE */
						       meltfrout->
						       tabval[27])))
      )				/*then */
      {
	/*^cond.then */
	/*^getslot */
	{
	  melt_ptr_t slot = NULL, obj = NULL;
	  obj =
	    (melt_ptr_t) (( /*!BUFFER_LIMIT_CONT */ meltfrout->
			   tabval[26])) /*=obj*/ ;
	  melt_object_get_field (slot, obj, 0, "REFERENCED_VALUE");
   /*_.REFERENCED_VALUE__V156*/ meltfptr[147] = slot;
	};
	;
      }
    else
      {				/*^cond.else */

  /*_.REFERENCED_VALUE__V156*/ meltfptr[147] = NULL;;
      }
    ;
    /*^compute */
 /*_#GET_INT__L69*/ meltfnum[39] =
      (melt_get_int
       ((melt_ptr_t) ( /*_.REFERENCED_VALUE__V156*/ meltfptr[147])));;
    /*^compute */
 /*_#IRAW__L70*/ meltfnum[44] =
      (( /*_#GET_INT__L69*/ meltfnum[39]) / (2));;
    /*^compute */
 /*_#I__L71*/ meltfnum[70] =
      (( /*_#STRBUF_USEDLENGTH__L68*/ meltfnum[61]) >
       ( /*_#IRAW__L70*/ meltfnum[44]));;
    MELT_LOCATION ("warmelt-outobj.melt:5744:/ cond");
    /*cond */ if ( /*_#I__L71*/ meltfnum[70])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{


	  {
	    MELT_LOCATION ("warmelt-outobj.melt:5745:/ locexp");
	    warning (0, "MELT WARNING MSG [#%ld]::: %s - %s", melt_dbgcounter,
		     ("very large declaration string buffer for module "),
		     melt_string_str ((melt_ptr_t)
				      ( /*_.MODNAMSTR__V3*/ meltfptr[2])));
	  }
	  ;
	  /*epilog */
	}
	;
      }				/*noelse */
    ;
    MELT_LOCATION ("warmelt-outobj.melt:5746:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#STRBUF_USEDLENGTH__L72*/ meltfnum[71] =
      melt_strbuf_usedlength ((melt_ptr_t)
			      ( /*_.IMPLBUF__V34*/ meltfptr[33]));;
    MELT_LOCATION ("warmelt-outobj.melt:5746:/ cond");
    /*cond */ if (
		   /*ifisa */
		   melt_is_instance_of ((melt_ptr_t)
					(( /*!BUFFER_LIMIT_CONT */ meltfrout->
					  tabval[26])),
					(melt_ptr_t) (( /*!CLASS_REFERENCE */
						       meltfrout->
						       tabval[27])))
      )				/*then */
      {
	/*^cond.then */
	/*^getslot */
	{
	  melt_ptr_t slot = NULL, obj = NULL;
	  obj =
	    (melt_ptr_t) (( /*!BUFFER_LIMIT_CONT */ meltfrout->
			   tabval[26])) /*=obj*/ ;
	  melt_object_get_field (slot, obj, 0, "REFERENCED_VALUE");
   /*_.REFERENCED_VALUE__V157*/ meltfptr[148] = slot;
	};
	;
      }
    else
      {				/*^cond.else */

  /*_.REFERENCED_VALUE__V157*/ meltfptr[148] = NULL;;
      }
    ;
    /*^compute */
 /*_#GET_INT__L73*/ meltfnum[72] =
      (melt_get_int
       ((melt_ptr_t) ( /*_.REFERENCED_VALUE__V157*/ meltfptr[148])));;
    /*^compute */
 /*_#IRAW__L74*/ meltfnum[73] =
      (( /*_#GET_INT__L73*/ meltfnum[72]) / (2));;
    /*^compute */
 /*_#I__L75*/ meltfnum[74] =
      (( /*_#STRBUF_USEDLENGTH__L72*/ meltfnum[71]) >
       ( /*_#IRAW__L74*/ meltfnum[73]));;
    MELT_LOCATION ("warmelt-outobj.melt:5746:/ cond");
    /*cond */ if ( /*_#I__L75*/ meltfnum[74])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{


	  {
	    MELT_LOCATION ("warmelt-outobj.melt:5747:/ locexp");
	    warning (0, "MELT WARNING MSG [#%ld]::: %s - %s", melt_dbgcounter,
		     ("very large implementation string buffer module for "),
		     melt_string_str ((melt_ptr_t)
				      ( /*_.MODNAMSTR__V3*/ meltfptr[2])));
	  }
	  ;
	  /*epilog */
	}
	;
      }				/*noelse */
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:5750:/ locexp");
      /* translate_macroexpanded_list OUTPUTCFILE__1: */
      melt_output_cfile_decl_impl
	((melt_ptr_t) ( /*_.MODNAMSTR__V3*/ meltfptr[2]),
	 (melt_ptr_t) ( /*_.DECLBUF__V42*/ meltfptr[41]),
	 (melt_ptr_t) ( /*_.IMPLBUF__V34*/ meltfptr[33]));
      ;
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:5757:/ quasiblock");


    /*^cond */
    /*cond */ if (
		   /*ifisa */
		   melt_is_instance_of ((melt_ptr_t)
					( /*_.MODCTX__V4*/ meltfptr[3]),
					(melt_ptr_t) (( /*!CLASS_MODULE_CONTEXT */ meltfrout->tabval[1])))
      )				/*then */
      {
	/*^cond.then */
	/*^getslot */
	{
	  melt_ptr_t slot = NULL, obj = NULL;
	  obj = (melt_ptr_t) ( /*_.MODCTX__V4*/ meltfptr[3]) /*=obj*/ ;
	  melt_object_get_field (slot, obj, 5, "MOCX_FILETUPLE");
   /*_.SECFILES__V159*/ meltfptr[158] = slot;
	};
	;
      }
    else
      {				/*^cond.else */

  /*_.SECFILES__V159*/ meltfptr[158] = NULL;;
      }
    ;
    /*^compute */
 /*_#NBSECFILES__L76*/ meltfnum[75] = 0;;
    /*^compute */
 /*_#LGSECFILES__L77*/ meltfnum[76] =
      (melt_multiple_length
       ((melt_ptr_t) ( /*_.SECFILES__V159*/ meltfptr[158])));;
    /*^compute */
 /*_#HISECFILERK__L78*/ meltfnum[77] = 0;;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:5762:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L79*/ meltfnum[78] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-outobj.melt:5762:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L79*/ meltfnum[78])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L80*/ meltfnum[79] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-outobj.melt:5762:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long =
		/*_#THE_MELTCALLCOUNT__L80*/ meltfnum[79];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-outobj.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 5762;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring =
		"translate_macroexpanded_list secfiles=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.SECFILES__V159*/ meltfptr[158];
	      /*_.MELT_DEBUG_FUN__V162*/ meltfptr[161] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V161*/ meltfptr[160] =
	      /*_.MELT_DEBUG_FUN__V162*/ meltfptr[161];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-outobj.melt:5762:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L80*/ meltfnum[79] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V162*/ meltfptr[161] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V161*/ meltfptr[160] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-outobj.melt:5762:/ quasiblock");


      /*_.PROGN___V163*/ meltfptr[161] = /*_.IF___V161*/ meltfptr[160];;
      /*^compute */
      /*_.IFCPP___V160*/ meltfptr[159] = /*_.PROGN___V163*/ meltfptr[161];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:5762:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L79*/ meltfnum[78] = 0;
      /*^clear */
	     /*clear *//*_.IF___V161*/ meltfptr[160] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V163*/ meltfptr[161] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V160*/ meltfptr[159] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    /*citerblock FOREACH_IN_MULTIPLE */
    {
      /* start foreach_in_multiple meltcit2__EACHTUP */
      long meltcit2__EACHTUP_ln =
	melt_multiple_length ((melt_ptr_t) /*_.SECFILES__V159*/
			      meltfptr[158]);
      for ( /*_#FILIX__L81*/ meltfnum[79] = 0;
	   ( /*_#FILIX__L81*/ meltfnum[79] >= 0)
	   && ( /*_#FILIX__L81*/ meltfnum[79] < meltcit2__EACHTUP_ln);
	/*_#FILIX__L81*/ meltfnum[79]++)
	{
	  /*_.CURFIL__V164*/ meltfptr[160] =
	    melt_multiple_nth ((melt_ptr_t)
			       ( /*_.SECFILES__V159*/ meltfptr[158]),
			       /*_#FILIX__L81*/ meltfnum[79]);



	  MELT_LOCATION ("warmelt-outobj.melt:5766:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^cond */
	  /*cond */ if ( /*_.CURFIL__V164*/ meltfptr[160])	/*then */
	    {
	      /*^cond.then */
	      /*^block */
	      /*anyblock */
	      {


#if MELT_HAVE_DEBUG
		MELT_LOCATION ("warmelt-outobj.melt:5768:/ cppif.then");
		/*^block */
		/*anyblock */
		{

		  /*^checksignal */
		  MELT_CHECK_SIGNAL ();
		  ;
      /*_#IS_A__L82*/ meltfnum[78] =
		    melt_is_instance_of ((melt_ptr_t)
					 ( /*_.CURFIL__V164*/ meltfptr[160]),
					 (melt_ptr_t) (( /*!CLASS_SECONDARY_C_FILE */ meltfrout->tabval[20])));;
		  MELT_LOCATION ("warmelt-outobj.melt:5768:/ cond");
		  /*cond */ if ( /*_#IS_A__L82*/ meltfnum[78])	/*then */
		    {
		      /*^cond.then */
		      /*_.IFELSE___V166*/ meltfptr[165] = ( /*nil */ NULL);;
		    }
		  else
		    {
		      MELT_LOCATION ("warmelt-outobj.melt:5768:/ cond.else");

		      /*^block */
		      /*anyblock */
		      {




			{
			  /*^locexp */
			  melt_assert_failed (("check curfil"),
					      ("warmelt-outobj.melt")
					      ? ("warmelt-outobj.melt") :
					      __FILE__,
					      (5768) ? (5768) : __LINE__,
					      __FUNCTION__);
			  ;
			}
			;
		  /*clear *//*_.IFELSE___V166*/ meltfptr[165] = 0;
			/*epilog */
		      }
		      ;
		    }
		  ;
		  /*^compute */
		  /*_.IFCPP___V165*/ meltfptr[161] =
		    /*_.IFELSE___V166*/ meltfptr[165];;
		  /*epilog */

		  MELT_LOCATION ("warmelt-outobj.melt:5768:/ clear");
		/*clear *//*_#IS_A__L82*/ meltfnum[78] = 0;
		  /*^clear */
		/*clear *//*_.IFELSE___V166*/ meltfptr[165] = 0;
		}

#else /*MELT_HAVE_DEBUG */
		/*^cppif.else */
		/*_.IFCPP___V165*/ meltfptr[161] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
		;

#if MELT_HAVE_DEBUG
		MELT_LOCATION ("warmelt-outobj.melt:5770:/ cppif.then");
		/*^block */
		/*anyblock */
		{

		  /*^checksignal */
		  MELT_CHECK_SIGNAL ();
		  ;
      /*_#GET_INT__L83*/ meltfnum[78] =
		    (melt_get_int
		     ((melt_ptr_t) ( /*_.CURFIL__V164*/ meltfptr[160])));;
		  /*^compute */
      /*_#I__L84*/ meltfnum[83] =
		    (( /*_#GET_INT__L83*/ meltfnum[78]) ==
		     ( /*_#FILIX__L81*/ meltfnum[79]));;
		  MELT_LOCATION ("warmelt-outobj.melt:5770:/ cond");
		  /*cond */ if ( /*_#I__L84*/ meltfnum[83])	/*then */
		    {
		      /*^cond.then */
		      /*_.IFELSE___V168*/ meltfptr[167] = ( /*nil */ NULL);;
		    }
		  else
		    {
		      MELT_LOCATION ("warmelt-outobj.melt:5770:/ cond.else");

		      /*^block */
		      /*anyblock */
		      {




			{
			  /*^locexp */
			  melt_assert_failed (("check curfil index"),
					      ("warmelt-outobj.melt")
					      ? ("warmelt-outobj.melt") :
					      __FILE__,
					      (5770) ? (5770) : __LINE__,
					      __FUNCTION__);
			  ;
			}
			;
		  /*clear *//*_.IFELSE___V168*/ meltfptr[167] = 0;
			/*epilog */
		      }
		      ;
		    }
		  ;
		  /*^compute */
		  /*_.IFCPP___V167*/ meltfptr[165] =
		    /*_.IFELSE___V168*/ meltfptr[167];;
		  /*epilog */

		  MELT_LOCATION ("warmelt-outobj.melt:5770:/ clear");
		/*clear *//*_#GET_INT__L83*/ meltfnum[78] = 0;
		  /*^clear */
		/*clear *//*_#I__L84*/ meltfnum[83] = 0;
		  /*^clear */
		/*clear *//*_.IFELSE___V168*/ meltfptr[167] = 0;
		}

#else /*MELT_HAVE_DEBUG */
		/*^cppif.else */
		/*_.IFCPP___V167*/ meltfptr[165] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
		;
		MELT_LOCATION ("warmelt-outobj.melt:5772:/ compute");
		/*_#HISECFILERK__L78*/ meltfnum[77] =
		  /*_#SETQ___L85*/ meltfnum[78] =
		  /*_#FILIX__L81*/ meltfnum[79];;

		{
		  MELT_LOCATION ("warmelt-outobj.melt:5773:/ locexp");
		   /*increment *//*_#NBSECFILES__L76*/ meltfnum[75] += 1;
		  ;
		}
		;
		MELT_LOCATION ("warmelt-outobj.melt:5775:/ quasiblock");


		/*^cond */
		/*cond */ if (
			       /*ifisa */
			       melt_is_instance_of ((melt_ptr_t)
						    ( /*_.CURFIL__V164*/
						     meltfptr[160]),
						    (melt_ptr_t) (( /*!CLASS_SECONDARY_C_FILE */ meltfrout->tabval[20])))
		  )		/*then */
		  {
		    /*^cond.then */
		    /*^getslot */
		    {
		      melt_ptr_t slot = NULL, obj = NULL;
		      obj =
			(melt_ptr_t) ( /*_.CURFIL__V164*/ meltfptr[160])
			/*=obj*/ ;
		      melt_object_get_field (slot, obj, 1, "SECFIL_PATH");
      /*_.SECFILPATH__V169*/ meltfptr[167] = slot;
		    };
		    ;
		  }
		else
		  {		/*^cond.else */

     /*_.SECFILPATH__V169*/ meltfptr[167] = NULL;;
		  }
		;
		MELT_LOCATION ("warmelt-outobj.melt:5776:/ cond");
		/*cond */ if (
			       /*ifisa */
			       melt_is_instance_of ((melt_ptr_t)
						    ( /*_.CURFIL__V164*/
						     meltfptr[160]),
						    (melt_ptr_t) (( /*!CLASS_SECONDARY_C_FILE */ meltfrout->tabval[20])))
		  )		/*then */
		  {
		    /*^cond.then */
		    /*^getslot */
		    {
		      melt_ptr_t slot = NULL, obj = NULL;
		      obj =
			(melt_ptr_t) ( /*_.CURFIL__V164*/ meltfptr[160])
			/*=obj*/ ;
		      melt_object_get_field (slot, obj, 2, "SECFIL_DECLBUF");
      /*_.SECDECLBUF__V170*/ meltfptr[169] = slot;
		    };
		    ;
		  }
		else
		  {		/*^cond.else */

     /*_.SECDECLBUF__V170*/ meltfptr[169] = NULL;;
		  }
		;
		MELT_LOCATION ("warmelt-outobj.melt:5777:/ cond");
		/*cond */ if (
			       /*ifisa */
			       melt_is_instance_of ((melt_ptr_t)
						    ( /*_.CURFIL__V164*/
						     meltfptr[160]),
						    (melt_ptr_t) (( /*!CLASS_SECONDARY_C_FILE */ meltfrout->tabval[20])))
		  )		/*then */
		  {
		    /*^cond.then */
		    /*^getslot */
		    {
		      melt_ptr_t slot = NULL, obj = NULL;
		      obj =
			(melt_ptr_t) ( /*_.CURFIL__V164*/ meltfptr[160])
			/*=obj*/ ;
		      melt_object_get_field (slot, obj, 3, "SECFIL_IMPLBUF");
      /*_.SECIMPLBUF__V171*/ meltfptr[170] = slot;
		    };
		    ;
		  }
		else
		  {		/*^cond.else */

     /*_.SECIMPLBUF__V171*/ meltfptr[170] = NULL;;
		  }
		;
		MELT_LOCATION ("warmelt-outobj.melt:5780:/ checksignal");
		MELT_CHECK_SIGNAL ();
		;
    /*_#STRBUF_USEDLENGTH__L86*/ meltfnum[83] =
		  melt_strbuf_usedlength ((melt_ptr_t)
					  ( /*_.SECDECLBUF__V170*/
					   meltfptr[169]));;
		MELT_LOCATION ("warmelt-outobj.melt:5780:/ cond");
		/*cond */ if (
			       /*ifisa */
			       melt_is_instance_of ((melt_ptr_t)
						    (( /*!BUFFER_LIMIT_CONT */
						      meltfrout->tabval[26])),
						    (melt_ptr_t) (( /*!CLASS_REFERENCE */ meltfrout->tabval[27])))
		  )		/*then */
		  {
		    /*^cond.then */
		    /*^getslot */
		    {
		      melt_ptr_t slot = NULL, obj = NULL;
		      obj =
			(melt_ptr_t) (( /*!BUFFER_LIMIT_CONT */ meltfrout->
				       tabval[26])) /*=obj*/ ;
		      melt_object_get_field (slot, obj, 0,
					     "REFERENCED_VALUE");
      /*_.REFERENCED_VALUE__V172*/ meltfptr[171] = slot;
		    };
		    ;
		  }
		else
		  {		/*^cond.else */

     /*_.REFERENCED_VALUE__V172*/ meltfptr[171] = NULL;;
		  }
		;
		/*^compute */
    /*_#GET_INT__L87*/ meltfnum[86] =
		  (melt_get_int
		   ((melt_ptr_t)
		    ( /*_.REFERENCED_VALUE__V172*/ meltfptr[171])));;
		/*^compute */
    /*_#IRAW__L88*/ meltfnum[87] =
		  (( /*_#GET_INT__L87*/ meltfnum[86]) / (2));;
		/*^compute */
    /*_#I__L89*/ meltfnum[88] =
		  (( /*_#STRBUF_USEDLENGTH__L86*/ meltfnum[83]) >
		   ( /*_#IRAW__L88*/ meltfnum[87]));;
		MELT_LOCATION ("warmelt-outobj.melt:5780:/ cond");
		/*cond */ if ( /*_#I__L89*/ meltfnum[88])	/*then */
		  {
		    /*^cond.then */
		    /*^block */
		    /*anyblock */
		    {


		      {
			MELT_LOCATION ("warmelt-outobj.melt:5781:/ locexp");
			warning (0, "MELT WARNING MSG [#%ld]::: %s - %s",
				 melt_dbgcounter,
				 ("very large declaration string buffer for secondary file "),
				 melt_string_str ((melt_ptr_t)
						  ( /*_.SECFILPATH__V169*/
						   meltfptr[167])));
		      }
		      ;
		      /*epilog */
		    }
		    ;
		  }		/*noelse */
		;
		MELT_LOCATION ("warmelt-outobj.melt:5782:/ checksignal");
		MELT_CHECK_SIGNAL ();
		;
    /*_#STRBUF_USEDLENGTH__L90*/ meltfnum[89] =
		  melt_strbuf_usedlength ((melt_ptr_t)
					  ( /*_.SECIMPLBUF__V171*/
					   meltfptr[170]));;
		MELT_LOCATION ("warmelt-outobj.melt:5782:/ cond");
		/*cond */ if (
			       /*ifisa */
			       melt_is_instance_of ((melt_ptr_t)
						    (( /*!BUFFER_LIMIT_CONT */
						      meltfrout->tabval[26])),
						    (melt_ptr_t) (( /*!CLASS_REFERENCE */ meltfrout->tabval[27])))
		  )		/*then */
		  {
		    /*^cond.then */
		    /*^getslot */
		    {
		      melt_ptr_t slot = NULL, obj = NULL;
		      obj =
			(melt_ptr_t) (( /*!BUFFER_LIMIT_CONT */ meltfrout->
				       tabval[26])) /*=obj*/ ;
		      melt_object_get_field (slot, obj, 0,
					     "REFERENCED_VALUE");
      /*_.REFERENCED_VALUE__V173*/ meltfptr[172] = slot;
		    };
		    ;
		  }
		else
		  {		/*^cond.else */

     /*_.REFERENCED_VALUE__V173*/ meltfptr[172] = NULL;;
		  }
		;
		/*^compute */
    /*_#GET_INT__L91*/ meltfnum[90] =
		  (melt_get_int
		   ((melt_ptr_t)
		    ( /*_.REFERENCED_VALUE__V173*/ meltfptr[172])));;
		/*^compute */
    /*_#IRAW__L92*/ meltfnum[91] =
		  (( /*_#GET_INT__L91*/ meltfnum[90]) / (2));;
		/*^compute */
    /*_#I__L93*/ meltfnum[92] =
		  (( /*_#STRBUF_USEDLENGTH__L90*/ meltfnum[89]) >
		   ( /*_#IRAW__L92*/ meltfnum[91]));;
		MELT_LOCATION ("warmelt-outobj.melt:5782:/ cond");
		/*cond */ if ( /*_#I__L93*/ meltfnum[92])	/*then */
		  {
		    /*^cond.then */
		    /*^block */
		    /*anyblock */
		    {


		      {
			MELT_LOCATION ("warmelt-outobj.melt:5783:/ locexp");
			warning (0, "MELT WARNING MSG [#%ld]::: %s - %s",
				 melt_dbgcounter,
				 ("very large implementation string buffer for secondary file "),
				 melt_string_str ((melt_ptr_t)
						  ( /*_.SECFILPATH__V169*/
						   meltfptr[167])));
		      }
		      ;
		      /*epilog */
		    }
		    ;
		  }		/*noelse */
		;

		{
		  MELT_LOCATION ("warmelt-outobj.melt:5787:/ locexp");
		  /* translate_macroexpanded_list SECFILOUT__1: */
		  melt_output_cfile_decl_impl_secondary
		    ((melt_ptr_t) ( /*_.SECFILPATH__V169*/ meltfptr[167]),
		     (melt_ptr_t) ( /*_.SECDECLBUF__V170*/ meltfptr[169]),
		     (melt_ptr_t) ( /*_.SECIMPLBUF__V171*/ meltfptr[170]),
			/*_#FILIX__L81*/ meltfnum[79]);
		  ;
		}
		;

		MELT_LOCATION ("warmelt-outobj.melt:5775:/ clear");
	      /*clear *//*_.SECFILPATH__V169*/ meltfptr[167] = 0;
		/*^clear */
	      /*clear *//*_.SECDECLBUF__V170*/ meltfptr[169] = 0;
		/*^clear */
	      /*clear *//*_.SECIMPLBUF__V171*/ meltfptr[170] = 0;
		/*^clear */
	      /*clear *//*_#STRBUF_USEDLENGTH__L86*/ meltfnum[83] = 0;
		/*^clear */
	      /*clear *//*_.REFERENCED_VALUE__V172*/ meltfptr[171] = 0;
		/*^clear */
	      /*clear *//*_#GET_INT__L87*/ meltfnum[86] = 0;
		/*^clear */
	      /*clear *//*_#IRAW__L88*/ meltfnum[87] = 0;
		/*^clear */
	      /*clear *//*_#I__L89*/ meltfnum[88] = 0;
		/*^clear */
	      /*clear *//*_#STRBUF_USEDLENGTH__L90*/ meltfnum[89] = 0;
		/*^clear */
	      /*clear *//*_.REFERENCED_VALUE__V173*/ meltfptr[172] = 0;
		/*^clear */
	      /*clear *//*_#GET_INT__L91*/ meltfnum[90] = 0;
		/*^clear */
	      /*clear *//*_#IRAW__L92*/ meltfnum[91] = 0;
		/*^clear */
	      /*clear *//*_#I__L93*/ meltfnum[92] = 0;
		MELT_LOCATION ("warmelt-outobj.melt:5767:/ quasiblock");


		/*epilog */

		MELT_LOCATION ("warmelt-outobj.melt:5766:/ clear");
	      /*clear *//*_.IFCPP___V165*/ meltfptr[161] = 0;
		/*^clear */
	      /*clear *//*_.IFCPP___V167*/ meltfptr[165] = 0;
		/*^clear */
	      /*clear *//*_#SETQ___L85*/ meltfnum[78] = 0;
	      }
	      ;
	    }			/*noelse */
	  ;
	  if ( /*_#FILIX__L81*/ meltfnum[79] < 0)
	    break;
	}			/* end  foreach_in_multiple meltcit2__EACHTUP */

      /*citerepilog */

      MELT_LOCATION ("warmelt-outobj.melt:5763:/ clear");
	    /*clear *//*_.CURFIL__V164*/ meltfptr[160] = 0;
      /*^clear */
	    /*clear *//*_#FILIX__L81*/ meltfnum[79] = 0;
    }				/*endciterblock FOREACH_IN_MULTIPLE */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:5799:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L94*/ meltfnum[83] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-outobj.melt:5799:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L94*/ meltfnum[83])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L95*/ meltfnum[86] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-outobj.melt:5799:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long =
		/*_#THE_MELTCALLCOUNT__L95*/ meltfnum[86];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-outobj.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 5799;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring =
		"translate_macroexpanded_list before output descrfil modctx=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.MODCTX__V4*/ meltfptr[3];
	      /*_.MELT_DEBUG_FUN__V176*/ meltfptr[170] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V175*/ meltfptr[169] =
	      /*_.MELT_DEBUG_FUN__V176*/ meltfptr[170];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-outobj.melt:5799:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L95*/ meltfnum[86] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V176*/ meltfptr[170] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V175*/ meltfptr[169] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-outobj.melt:5799:/ quasiblock");


      /*_.PROGN___V177*/ meltfptr[171] = /*_.IF___V175*/ meltfptr[169];;
      /*^compute */
      /*_.IFCPP___V174*/ meltfptr[167] = /*_.PROGN___V177*/ meltfptr[171];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:5799:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L94*/ meltfnum[83] = 0;
      /*^clear */
	     /*clear *//*_.IF___V175*/ meltfptr[169] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V177*/ meltfptr[171] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V174*/ meltfptr[167] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-outobj.melt:5800:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[2];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & /*_.SECFILES__V159*/ meltfptr[158];
      /*^apply.arg */
      argtab[1].meltbp_aptr = (melt_ptr_t *) & /*_.MODCTX__V4*/ meltfptr[3];
      /*_.OUTPUT_MELT_DESCRIPTOR__V178*/ meltfptr[172] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!OUTPUT_MELT_DESCRIPTOR */ meltfrout->tabval[28])),
		    (melt_ptr_t) ( /*_.MODNAMSTR__V3*/ meltfptr[2]),
		    (MELTBPARSTR_PTR MELTBPARSTR_PTR ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;
 /*_#I__L96*/ meltfnum[87] =
      (( /*_#HISECFILERK__L78*/ meltfnum[77]) + (1));;
    /*^compute */
 /*_#I__L97*/ meltfnum[88] =
      (( /*_#HISECFILERK__L78*/ meltfnum[77]) + (25));;
    /*citerblock FOREACH_LONG_UPTO */
    {
      /* foreach_long_upto start meltcit3__EACHLONG */
      long meltcit3__EACHLONG_min = /*_#I__L96*/ meltfnum[87];
      long meltcit3__EACHLONG_max = /*_#I__L97*/ meltfnum[88];
      long meltcit3__EACHLONG_cur = 0;
      for (meltcit3__EACHLONG_cur = meltcit3__EACHLONG_min;
	   meltcit3__EACHLONG_cur <= meltcit3__EACHLONG_max;
	   meltcit3__EACHLONG_cur++)
	{
	    /*_#DELFILIX__L98*/ meltfnum[89] = meltcit3__EACHLONG_cur;



	  MELT_LOCATION ("warmelt-outobj.melt:5805:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
  /*_#I__L99*/ meltfnum[90] =
	    (( /*_#DELFILIX__L98*/ meltfnum[89]) > (0));;
	  MELT_LOCATION ("warmelt-outobj.melt:5805:/ cond");
	  /*cond */ if ( /*_#I__L99*/ meltfnum[90])	/*then */
	    {
	      /*^cond.then */
	      /*^block */
	      /*anyblock */
	      {

		MELT_LOCATION ("warmelt-outobj.melt:5806:/ quasiblock");


    /*_.DELFILNAM__V179*/ meltfptr[161] =
		  meltgc_new_string_generated_c_filename	/*  generated_c_filename */
		  ((meltobject_ptr_t)
		   (( /*!DISCR_STRING */ meltfrout->tabval[29])),
		   melt_string_str ((melt_ptr_t)
				    ( /*_.MODNAMSTR__V3*/ meltfptr[2])),
		   melt_string_str ((melt_ptr_t) (( /*nil */ NULL))),
		   ( /*_#DELFILIX__L98*/ meltfnum[89]));;
		/*^compute */
    /*_.BAKSBUF__V180*/ meltfptr[165] =
		  (melt_ptr_t)
		  meltgc_new_strbuf ((meltobject_ptr_t)
				     (( /*!DISCR_STRBUF */ meltfrout->
				       tabval[4])), (const char *) 0);;
		MELT_LOCATION ("warmelt-outobj.melt:5809:/ checksignal");
		MELT_CHECK_SIGNAL ();
		;
		/*^apply */
		/*apply */
		{
		  union meltparam_un argtab[2];
		  memset (&argtab, 0, sizeof (argtab));
		  /*^apply.arg */
		  argtab[0].meltbp_aptr =
		    (melt_ptr_t *) & /*_.DELFILNAM__V179*/ meltfptr[161];
		  /*^apply.arg */
		  argtab[1].meltbp_cstring = "~";
		  /*_.ADD2OUT__V181*/ meltfptr[170] =
		    melt_apply ((meltclosure_ptr_t)
				(( /*!ADD2OUT */ meltfrout->tabval[30])),
				(melt_ptr_t) ( /*_.BAKSBUF__V180*/
					      meltfptr[165]),
				(MELTBPARSTR_PTR MELTBPARSTR_CSTRING ""),
				argtab, "", (union meltparam_un *) 0);
		}
		;
		MELT_LOCATION ("warmelt-outobj.melt:5810:/ quasiblock");


    /*_.BAKFILNAM__V182*/ meltfptr[169] =
		  (meltgc_new_stringdup
		   ((meltobject_ptr_t)
		    (( /*!DISCR_STRING */ meltfrout->tabval[29])),
		    melt_strbuf_str ((melt_ptr_t)
				     ( /*_.BAKSBUF__V180*/ meltfptr[165]))));;

		{
		  MELT_LOCATION ("warmelt-outobj.melt:5814:/ locexp");
		  /*translate_macroexpanded_list BACKUPCHK__1 */
		  {
		    const char *BACKUPCHK__1_delfilnamstr =
		      melt_string_str ((melt_ptr_t) /*_.DELFILNAM__V179*/
				       meltfptr[161]);
		    const char *BACKUPCHK__1_bakfilnamstr =
		      melt_string_str ((melt_ptr_t) /*_.BAKFILNAM__V182*/
				       meltfptr[169]);
		    if (BACKUPCHK__1_delfilnamstr && BACKUPCHK__1_bakfilnamstr
			&& !access (BACKUPCHK__1_delfilnamstr, F_OK))
		      {
			if (!rename
			    (BACKUPCHK__1_delfilnamstr,
			     BACKUPCHK__1_bakfilnamstr))
			  inform (UNKNOWN_LOCATION,
				  "MELT backing up previous generated file %s as %s",
				  BACKUPCHK__1_delfilnamstr,
				  BACKUPCHK__1_bakfilnamstr);
		      }
		    BACKUPCHK__1_delfilnamstr = NULL;
		    BACKUPCHK__1_bakfilnamstr = NULL;
		  } /*end translate_macroexpanded_list BACKUPCHK__1 */ ;
		}
		;

		MELT_LOCATION ("warmelt-outobj.melt:5810:/ clear");
	      /*clear *//*_.BAKFILNAM__V182*/ meltfptr[169] = 0;

		MELT_LOCATION ("warmelt-outobj.melt:5806:/ clear");
	      /*clear *//*_.DELFILNAM__V179*/ meltfptr[161] = 0;
		/*^clear */
	      /*clear *//*_.BAKSBUF__V180*/ meltfptr[165] = 0;
		/*^clear */
	      /*clear *//*_.ADD2OUT__V181*/ meltfptr[170] = 0;
		/*epilog */
	      }
	      ;
	    }			/*noelse */
	  ;
	}			/*end foreach_long_upto meltcit3__EACHLONG */

      /*citerepilog */

      MELT_LOCATION ("warmelt-outobj.melt:5802:/ clear");
	    /*clear *//*_#DELFILIX__L98*/ meltfnum[89] = 0;
      /*^clear */
	    /*clear *//*_#I__L99*/ meltfnum[90] = 0;
    }				/*endciterblock FOREACH_LONG_UPTO */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:5830:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L100*/ meltfnum[91] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-outobj.melt:5830:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L100*/ meltfnum[91])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L101*/ meltfnum[92] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-outobj.melt:5830:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long =
		/*_#THE_MELTCALLCOUNT__L101*/ meltfnum[92];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-outobj.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 5830;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring =
		"translate_macroexpanded_list before endmodnamchk modctx=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.MODCTX__V4*/ meltfptr[3];
	      /*_.MELT_DEBUG_FUN__V185*/ meltfptr[161] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V184*/ meltfptr[169] =
	      /*_.MELT_DEBUG_FUN__V185*/ meltfptr[161];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-outobj.melt:5830:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L101*/ meltfnum[92] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V185*/ meltfptr[161] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V184*/ meltfptr[169] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-outobj.melt:5830:/ quasiblock");


      /*_.PROGN___V186*/ meltfptr[165] = /*_.IF___V184*/ meltfptr[169];;
      /*^compute */
      /*_.IFCPP___V183*/ meltfptr[171] = /*_.PROGN___V186*/ meltfptr[165];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:5830:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L100*/ meltfnum[91] = 0;
      /*^clear */
	     /*clear *//*_.IF___V184*/ meltfptr[169] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V186*/ meltfptr[165] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V183*/ meltfptr[171] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:5833:/ locexp");
      /* translate_macroexpanded_list ENDMODNAM__1 */
#if HAVE_CLOCK && defined (CLOCKS_PER_SEC)
	      /*_#ENDCLOCK__L14*/ meltfnum[0] = (long) clock ();
      if (melt_flag_bootstrapping)
	inform (UNKNOWN_LOCATION,
		"MELT generated C code of module %s with %ld secondary files in %ld CPU millisec [#%ld].",
		melt_string_str ((melt_ptr_t) /*_.MODNAMSTR__V3*/
				 meltfptr[2]), /*_#NBSECFILES__L76*/
		meltfnum[75],
		( /*_#ENDCLOCK__L14*/ meltfnum[0] -
		 /*_#STARTCLOCK__L17*/ meltfnum[15]) / (CLOCKS_PER_SEC /
							1000),
		melt_dbgcounter);
      else
	inform (UNKNOWN_LOCATION,
		"MELT generated C code of module %s with %ld secondary files in %ld CPU millisec.",
		melt_string_str ((melt_ptr_t) /*_.MODNAMSTR__V3*/
				 meltfptr[2]), /*_#NBSECFILES__L76*/
		meltfnum[75],
		( /*_#ENDCLOCK__L14*/ meltfnum[0] -
		 /*_#STARTCLOCK__L17*/ meltfnum[15]) / (CLOCKS_PER_SEC /
							1000));
#else /* no clock */
      inform (UNKNOWN_LOCATION,
	      "MELT generated C code of module %s with %ld secondary files",
	      melt_string_str ((melt_ptr_t) /*_.MODNAMSTR__V3*/ meltfptr[2]),
	      /*_#NBSECFILES__L76*/ meltfnum[75]);
#endif /* HAVE_CLOCK && CLOCKS_PER_SEC */
      /* end translate_macroexpanded_list ENDMODNAM__1 */ ;
    }
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:5852:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L102*/ meltfnum[78] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-outobj.melt:5852:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L102*/ meltfnum[78])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L103*/ meltfnum[86] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-outobj.melt:5852:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long =
		/*_#THE_MELTCALLCOUNT__L103*/ meltfnum[86];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-outobj.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 5852;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring =
		"translate_macroexpanded_list ending modctx=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.MODCTX__V4*/ meltfptr[3];
	      /*_.MELT_DEBUG_FUN__V189*/ meltfptr[169] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V188*/ meltfptr[161] =
	      /*_.MELT_DEBUG_FUN__V189*/ meltfptr[169];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-outobj.melt:5852:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L103*/ meltfnum[86] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V189*/ meltfptr[169] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V188*/ meltfptr[161] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-outobj.melt:5852:/ quasiblock");


      /*_.PROGN___V190*/ meltfptr[165] = /*_.IF___V188*/ meltfptr[161];;
      /*^compute */
      /*_.IFCPP___V187*/ meltfptr[170] = /*_.PROGN___V190*/ meltfptr[165];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:5852:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L102*/ meltfnum[78] = 0;
      /*^clear */
	     /*clear *//*_.IF___V188*/ meltfptr[161] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V190*/ meltfptr[165] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V187*/ meltfptr[170] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:5853:/ locexp");

#if MELT_HAVE_DEBUG
      if (melt_need_debug (0))
	melt_dbgshortbacktrace (("translate_macroexpanded_list ended"), (25));
#endif
      ;
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:5854:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#MELT_ERROR_COUNTER__L104*/ meltfnum[83] =
      melt_error_counter;;
    MELT_LOCATION ("warmelt-outobj.melt:5854:/ cond");
    /*cond */ if ( /*_#MELT_ERROR_COUNTER__L104*/ meltfnum[83])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-outobj.melt:5855:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
   /*_#IS_CLOSURE__L105*/ meltfnum[92] =
	    (melt_magic_discr
	     ((melt_ptr_t) ( /*_.ERRORHDLR__V30*/ meltfptr[29])) ==
	     MELTOBMAG_CLOSURE);;
	  MELT_LOCATION ("warmelt-outobj.melt:5855:/ cond");
	  /*cond */ if ( /*_#IS_CLOSURE__L105*/ meltfnum[92])	/*then */
	    {
	      /*^cond.then */
	      /*^block */
	      /*anyblock */
	      {

		MELT_LOCATION ("warmelt-outobj.melt:5856:/ checksignal");
		MELT_CHECK_SIGNAL ();
		;
		/*^apply */
		/*apply */
		{
		  /*_.ERRORHDLR__V193*/ meltfptr[165] =
		    melt_apply ((meltclosure_ptr_t)
				( /*_.ERRORHDLR__V30*/ meltfptr[29]),
				(melt_ptr_t) (( /*!konst_31 */ meltfrout->
					       tabval[31])), (""),
				(union meltparam_un *) 0, "",
				(union meltparam_un *) 0);
		}
		;
		/*_.IF___V192*/ meltfptr[161] =
		  /*_.ERRORHDLR__V193*/ meltfptr[165];;
		/*epilog */

		MELT_LOCATION ("warmelt-outobj.melt:5855:/ clear");
	       /*clear *//*_.ERRORHDLR__V193*/ meltfptr[165] = 0;
	      }
	      ;
	    }
	  else
	    {			/*^cond.else */

    /*_.IF___V192*/ meltfptr[161] = NULL;;
	    }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:5858:/ locexp");
	    /* translate_macroexpanded_list ERROREDNORMAL_WARNCHK__2 */
	    warning (0,
		     "MELT translation of %s got after emission %ld MELT errors",
		     melt_string_str ((melt_ptr_t) /*_.MODNAMSTR__V3*/
				      meltfptr[2]), melt_error_counter);
	    ;
	  }
	  ;
	  MELT_LOCATION ("warmelt-outobj.melt:5862:/ quasiblock");


   /*_.RETVAL___V1*/ meltfptr[0] = NULL;;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:5862:/ locexp");
	    /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
	    if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	      melt_warn_for_no_expected_secondary_results ();
	    /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
	    ;
	  }
	  ;
	  /*^finalreturn */
	  ;
	  /*finalret */ goto labend_rout;
	  MELT_LOCATION ("warmelt-outobj.melt:5854:/ quasiblock");


	  /*_.PROGN___V195*/ meltfptr[194] =
	    /*_.RETURN___V194*/ meltfptr[165];;
	  /*^compute */
	  /*_.IF___V191*/ meltfptr[169] = /*_.PROGN___V195*/ meltfptr[194];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-outobj.melt:5854:/ clear");
	     /*clear *//*_#IS_CLOSURE__L105*/ meltfnum[92] = 0;
	  /*^clear */
	     /*clear *//*_.IF___V192*/ meltfptr[161] = 0;
	  /*^clear */
	     /*clear *//*_.RETURN___V194*/ meltfptr[165] = 0;
	  /*^clear */
	     /*clear *//*_.PROGN___V195*/ meltfptr[194] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

  /*_.IF___V191*/ meltfptr[169] = NULL;;
      }
    ;
    /*^compute */
    /*_.LET___V158*/ meltfptr[157] = /*_.IF___V191*/ meltfptr[169];;

    MELT_LOCATION ("warmelt-outobj.melt:5757:/ clear");
	   /*clear *//*_.SECFILES__V159*/ meltfptr[158] = 0;
    /*^clear */
	   /*clear *//*_#NBSECFILES__L76*/ meltfnum[75] = 0;
    /*^clear */
	   /*clear *//*_#LGSECFILES__L77*/ meltfnum[76] = 0;
    /*^clear */
	   /*clear *//*_#HISECFILERK__L78*/ meltfnum[77] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V160*/ meltfptr[159] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V174*/ meltfptr[167] = 0;
    /*^clear */
	   /*clear *//*_.OUTPUT_MELT_DESCRIPTOR__V178*/ meltfptr[172] = 0;
    /*^clear */
	   /*clear *//*_#I__L96*/ meltfnum[87] = 0;
    /*^clear */
	   /*clear *//*_#I__L97*/ meltfnum[88] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V183*/ meltfptr[171] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V187*/ meltfptr[170] = 0;
    /*^clear */
	   /*clear *//*_#MELT_ERROR_COUNTER__L104*/ meltfnum[83] = 0;
    /*^clear */
	   /*clear *//*_.IF___V191*/ meltfptr[169] = 0;
    /*_.LET___V123*/ meltfptr[119] = /*_.LET___V158*/ meltfptr[157];;

    MELT_LOCATION ("warmelt-outobj.melt:5704:/ clear");
	   /*clear *//*_.INIOBJ__V124*/ meltfptr[120] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V125*/ meltfptr[124] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V142*/ meltfptr[132] = 0;
    /*^clear */
	   /*clear *//*_.LET___V146*/ meltfptr[137] = 0;
    /*^clear */
	   /*clear *//*_.OUTPUT_C_CODE__V154*/ meltfptr[149] = 0;
    /*^clear */
	   /*clear *//*_.OUTPUT_EXPORTED_OFFSETS__V155*/ meltfptr[139] = 0;
    /*^clear */
	   /*clear *//*_#STRBUF_USEDLENGTH__L68*/ meltfnum[61] = 0;
    /*^clear */
	   /*clear *//*_.REFERENCED_VALUE__V156*/ meltfptr[147] = 0;
    /*^clear */
	   /*clear *//*_#GET_INT__L69*/ meltfnum[39] = 0;
    /*^clear */
	   /*clear *//*_#IRAW__L70*/ meltfnum[44] = 0;
    /*^clear */
	   /*clear *//*_#I__L71*/ meltfnum[70] = 0;
    /*^clear */
	   /*clear *//*_#STRBUF_USEDLENGTH__L72*/ meltfnum[71] = 0;
    /*^clear */
	   /*clear *//*_.REFERENCED_VALUE__V157*/ meltfptr[148] = 0;
    /*^clear */
	   /*clear *//*_#GET_INT__L73*/ meltfnum[72] = 0;
    /*^clear */
	   /*clear *//*_#IRAW__L74*/ meltfnum[73] = 0;
    /*^clear */
	   /*clear *//*_#I__L75*/ meltfnum[74] = 0;
    /*^clear */
	   /*clear *//*_.LET___V158*/ meltfptr[157] = 0;
    /*_.LET___V98*/ meltfptr[90] = /*_.LET___V123*/ meltfptr[119];;

    MELT_LOCATION ("warmelt-outobj.melt:5689:/ clear");
	   /*clear *//*_.INIPRO__V99*/ meltfptr[98] = 0;
    /*^clear */
	   /*clear *//*_.INIDATA__V100*/ meltfptr[99] = 0;
    /*^clear */
	   /*clear *//*_.IMPORTVALUES__V101*/ meltfptr[100] = 0;
    /*^clear */
	   /*clear *//*_.PROCURMODENVLIST__V102*/ meltfptr[101] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V103*/ meltfptr[102] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V108*/ meltfptr[105] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V111*/ meltfptr[103] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V115*/ meltfptr[104] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V117*/ meltfptr[112] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V119*/ meltfptr[117] = 0;
    /*^clear */
	   /*clear *//*_.LET___V123*/ meltfptr[119] = 0;
    /*_.LET___V73*/ meltfptr[58] = /*_.LET___V98*/ meltfptr[90];;

    MELT_LOCATION ("warmelt-outobj.melt:5658:/ clear");
	   /*clear *//*_.PROLIST__V74*/ meltfptr[54] = 0;
    /*^clear */
	   /*clear *//*_.OBJLIST__V75*/ meltfptr[59] = 0;
    /*^clear */
	   /*clear *//*_#LIST_LENGTH__L36*/ meltfnum[1] = 0;
    /*^clear */
	   /*clear *//*_#I__L37*/ meltfnum[26] = 0;
    /*^clear */
	   /*clear *//*_#I__L38*/ meltfnum[28] = 0;
    /*^clear */
	   /*clear *//*_.COMPICACHE__V76*/ meltfptr[63] = 0;
    /*^clear */
	   /*clear *//*_.COUNTBOX__V77*/ meltfptr[76] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V78*/ meltfptr[77] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V82*/ meltfptr[78] = 0;
    /*^clear */
	   /*clear *//*_.LAMBDA___V84*/ meltfptr[79] = 0;
    /*^clear */
	   /*clear *//*_.LIST_EVERY__V86*/ meltfptr[85] = 0;
    /*^clear */
	   /*clear *//*_#MELT_ERROR_COUNTER__L42*/ meltfnum[38] = 0;
    /*^clear */
	   /*clear *//*_.IF___V87*/ meltfptr[86] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V92*/ meltfptr[87] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V96*/ meltfptr[88] = 0;
    /*^clear */
	   /*clear *//*_.LET___V98*/ meltfptr[90] = 0;
    MELT_LOCATION ("warmelt-outobj.melt:5625:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*_.MULTI___V39*/ meltfptr[35] = /*_.LET___V73*/ meltfptr[58];;

    MELT_LOCATION ("warmelt-outobj.melt:5625:/ clear");
	   /*clear *//*_.IFCPP___V43*/ meltfptr[42] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V47*/ meltfptr[43] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V49*/ meltfptr[44] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V51*/ meltfptr[49] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V53*/ meltfptr[51] = 0;
    /*^clear */
	   /*clear *//*_#IS_CLOSURE__L25*/ meltfnum[18] = 0;
    /*^clear */
	   /*clear *//*_.IF___V57*/ meltfptr[53] = 0;
    /*^clear */
	   /*clear *//*_#MELT_ERROR_COUNTER__L34*/ meltfnum[27] = 0;
    /*^clear */
	   /*clear *//*_.IF___V68*/ meltfptr[64] = 0;
    /*^clear */
	   /*clear *//*_.LET___V73*/ meltfptr[58] = 0;

    /*^clear */
	   /*clear *//*_.BINDLIST__V41*/ meltfptr[40] = 0;
    /*^clear */
	   /*clear *//*_.DECLBUF__V42*/ meltfptr[41] = 0;
    /*^clear */
	   /*clear *//*_#STARTCLOCK__L17*/ meltfnum[15] = 0;
    /*_.LET___V29*/ meltfptr[27] = /*_.MULTI___V39*/ meltfptr[35];;

    MELT_LOCATION ("warmelt-outobj.melt:5615:/ clear");
	   /*clear *//*_#ENDCLOCK__L14*/ meltfnum[0] = 0;
    /*^clear */
	   /*clear *//*_.ERRORHDLR__V30*/ meltfptr[29] = 0;
    /*^clear */
	   /*clear *//*_.GENDEVLIST__V31*/ meltfptr[30] = 0;
    /*^clear */
	   /*clear *//*_.MODINIENV__V32*/ meltfptr[31] = 0;
    /*^clear */
	   /*clear *//*_.INIPROC__V33*/ meltfptr[32] = 0;
    /*^clear */
	   /*clear *//*_.IMPLBUF__V34*/ meltfptr[33] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V35*/ meltfptr[34] = 0;
    /*^clear */
	   /*clear *//*_.MULTI___V39*/ meltfptr[35] = 0;
    MELT_LOCATION ("warmelt-outobj.melt:5583:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.LET___V29*/ meltfptr[27];;

    {
      MELT_LOCATION ("warmelt-outobj.melt:5583:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V9*/ meltfptr[8] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V13*/ meltfptr[9] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V15*/ meltfptr[10] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V17*/ meltfptr[15] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V19*/ meltfptr[17] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V21*/ meltfptr[19] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V23*/ meltfptr[21] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V25*/ meltfptr[23] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V27*/ meltfptr[25] = 0;
    /*^clear */
	   /*clear *//*_.LET___V29*/ meltfptr[27] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("TRANSLATE_MACROEXPANDED_LIST", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_133_warmelt_outobj_TRANSLATE_MACROEXPANDED_LIST_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_133_warmelt_outobj_TRANSLATE_MACROEXPANDED_LIST */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_134_warmelt_outobj_LAMBDA___27__ (meltclosure_ptr_t meltclosp_,
					   melt_ptr_t meltfirstargp_,
					   const melt_argdescr_cell_t
					   meltxargdescr_[],
					   union meltparam_un * meltxargtab_,
					   const melt_argdescr_cell_t
					   meltxresdescr_[],
					   union meltparam_un * meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_134_warmelt_outobj_LAMBDA___27___melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_134_warmelt_outobj_LAMBDA___27___st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 9
    melt_ptr_t mcfr_varptr[9];
#define MELTFRAM_NBVARNUM 6
    long mcfr_varnum[6];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_134_warmelt_outobj_LAMBDA___27__ is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_134_warmelt_outobj_LAMBDA___27___st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 9; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_134_warmelt_outobj_LAMBDA___27__ nbval 9*/
  meltfram__.mcfr_nbvar = 9 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("LAMBDA_", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-outobj.melt:5669:/ getarg");
 /*_.PRO__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:5670:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L1*/ meltfnum[0] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.PRO__V2*/ meltfptr[1]),
			     (melt_ptr_t) (( /*!CLASS_NREP_ANYPROC */
					    meltfrout->tabval[0])));;
      MELT_LOCATION ("warmelt-outobj.melt:5670:/ cond");
      /*cond */ if ( /*_#IS_A__L1*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V4*/ meltfptr[3] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-outobj.melt:5670:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check pro"),
				  ("warmelt-outobj.melt")
				  ? ("warmelt-outobj.melt") : __FILE__,
				  (5670) ? (5670) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V4*/ meltfptr[3] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V3*/ meltfptr[2] = /*_.IFELSE___V4*/ meltfptr[3];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:5670:/ clear");
	     /*clear *//*_#IS_A__L1*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V4*/ meltfptr[3] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V3*/ meltfptr[2] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    /*^compute */
 /*_#GET_INT__L2*/ meltfnum[0] =
      (melt_get_int ((melt_ptr_t) (( /*~COUNTBOX */ meltfclos->tabval[0]))));;
    /*^compute */
 /*_#I__L3*/ meltfnum[2] =
      (( /*_#GET_INT__L2*/ meltfnum[0]) + (1));;

    {
      MELT_LOCATION ("warmelt-outobj.melt:5671:/ locexp");
      melt_put_int ((melt_ptr_t) (( /*~COUNTBOX */ meltfclos->tabval[0])),
		    ( /*_#I__L3*/ meltfnum[2]));
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:5672:/ quasiblock");


 /*_#GET_INT__L4*/ meltfnum[3] =
      (melt_get_int ((melt_ptr_t) (( /*~COUNTBOX */ meltfclos->tabval[0]))));;
    MELT_LOCATION ("warmelt-outobj.melt:5672:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[3];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & ( /*~MODCTX */ meltfclos->tabval[1]);
      /*^apply.arg */
      argtab[1].meltbp_aptr =
	(melt_ptr_t *) & ( /*~COMPICACHE */ meltfclos->tabval[2]);
      /*^apply.arg */
      argtab[2].meltbp_long = /*_#GET_INT__L4*/ meltfnum[3];
      /*_.OBJPRO__V5*/ meltfptr[3] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!COMPILE2OBJ_PROCEDURE */ meltfrout->tabval[1])),
		    (melt_ptr_t) ( /*_.PRO__V2*/ meltfptr[1]),
		    (MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_LONG ""),
		    argtab, "", (union meltparam_un *) 0);
    }
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:5673:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L5*/ meltfnum[4] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-outobj.melt:5673:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L5*/ meltfnum[4])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L6*/ meltfnum[5] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-outobj.melt:5673:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[7];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L6*/ meltfnum[5];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-outobj.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 5673;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring =
		"translate_macroexpanded_list objpro=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.OBJPRO__V5*/ meltfptr[3];
	      /*^apply.arg */
	      argtab[5].meltbp_cstring = " pro=";
	      /*^apply.arg */
	      argtab[6].meltbp_aptr =
		(melt_ptr_t *) & /*_.PRO__V2*/ meltfptr[1];
	      /*_.MELT_DEBUG_FUN__V8*/ meltfptr[7] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[2])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V7*/ meltfptr[6] = /*_.MELT_DEBUG_FUN__V8*/ meltfptr[7];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-outobj.melt:5673:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L6*/ meltfnum[5] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V8*/ meltfptr[7] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V7*/ meltfptr[6] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-outobj.melt:5673:/ quasiblock");


      /*_.PROGN___V9*/ meltfptr[7] = /*_.IF___V7*/ meltfptr[6];;
      /*^compute */
      /*_.IFCPP___V6*/ meltfptr[5] = /*_.PROGN___V9*/ meltfptr[7];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:5673:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L5*/ meltfnum[4] = 0;
      /*^clear */
	     /*clear *//*_.IF___V7*/ meltfptr[6] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V9*/ meltfptr[7] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V6*/ meltfptr[5] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:5674:/ locexp");
      meltgc_append_list ((melt_ptr_t)
			  (( /*~OBJLIST */ meltfclos->tabval[3])),
			  (melt_ptr_t) ( /*_.OBJPRO__V5*/ meltfptr[3]));
    }
    ;

    MELT_LOCATION ("warmelt-outobj.melt:5672:/ clear");
	   /*clear *//*_#GET_INT__L4*/ meltfnum[3] = 0;
    /*^clear */
	   /*clear *//*_.OBJPRO__V5*/ meltfptr[3] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V6*/ meltfptr[5] = 0;
    MELT_LOCATION ("warmelt-outobj.melt:5669:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V3*/ meltfptr[2] = 0;
    /*^clear */
	   /*clear *//*_#GET_INT__L2*/ meltfnum[0] = 0;
    /*^clear */
	   /*clear *//*_#I__L3*/ meltfnum[2] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("LAMBDA_", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_134_warmelt_outobj_LAMBDA___27___melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*noretval */ NULL);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_134_warmelt_outobj_LAMBDA___27__ */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_135_warmelt_outobj_FATAL_COMPILE_ERROR (meltclosure_ptr_t meltclosp_,
						 melt_ptr_t meltfirstargp_,
						 const melt_argdescr_cell_t
						 meltxargdescr_[],
						 union meltparam_un *
						 meltxargtab_,
						 const melt_argdescr_cell_t
						 meltxresdescr_[],
						 union meltparam_un *
						 meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_135_warmelt_outobj_FATAL_COMPILE_ERROR_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_135_warmelt_outobj_FATAL_COMPILE_ERROR_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 3
    melt_ptr_t mcfr_varptr[3];
#define MELTFRAM_NBVARNUM 1
    long mcfr_varnum[1];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_135_warmelt_outobj_FATAL_COMPILE_ERROR is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_135_warmelt_outobj_FATAL_COMPILE_ERROR_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 3; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_135_warmelt_outobj_FATAL_COMPILE_ERROR nbval 3*/
  meltfram__.mcfr_nbvar = 3 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("FATAL_COMPILE_ERROR", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-outobj.melt:5869:/ getarg");
 /*_.MODNAMSTR__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.V__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.V__V3*/ meltfptr[2])) != NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {

    MELT_LOCATION ("warmelt-outobj.melt:5870:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#IS_STRING__L1*/ meltfnum[0] =
      (melt_magic_discr ((melt_ptr_t) ( /*_.V__V3*/ meltfptr[2])) ==
       MELTOBMAG_STRING);;
    MELT_LOCATION ("warmelt-outobj.melt:5870:/ cond");
    /*cond */ if ( /*_#IS_STRING__L1*/ meltfnum[0])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{


	  {
	    MELT_LOCATION ("warmelt-outobj.melt:5873:/ locexp");
	    /*fatal_compile_error FATALERRORMSG_CHK__1 */
	    melt_fatal_error
	      ("MELT failed to compile module %s (%ld errors): %s",
	       melt_string_str ((melt_ptr_t) /*_.MODNAMSTR__V2*/ meltfptr[1]),
	       melt_error_counter,
	       melt_string_str ((melt_ptr_t) /*_.V__V3*/ meltfptr[2]));
	    ;
	  }
	  ;
	  /*epilog */
	}
	;
      }
    else
      {
	MELT_LOCATION ("warmelt-outobj.melt:5870:/ cond.else");

	/*^block */
	/*anyblock */
	{


	  {
	    MELT_LOCATION ("warmelt-outobj.melt:5881:/ locexp");
	    /* fatal_compile_error FATALERROR_CHK__1 */
	    melt_fatal_error ("MELT failed to compile module %s (%ld errors)",
			      melt_string_str ((melt_ptr_t)
					       /*_.MODNAMSTR__V2*/
					       meltfptr[1]),
			      melt_error_counter);
	    ;
	  }
	  ;
	  /*epilog */
	}
	;
      }
    ;
    /*epilog */

    MELT_LOCATION ("warmelt-outobj.melt:5869:/ clear");
	   /*clear *//*_#IS_STRING__L1*/ meltfnum[0] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("FATAL_COMPILE_ERROR", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_135_warmelt_outobj_FATAL_COMPILE_ERROR_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*noretval */ NULL);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_135_warmelt_outobj_FATAL_COMPILE_ERROR */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_136_warmelt_outobj_COMPILE_LIST_SEXPR (meltclosure_ptr_t meltclosp_,
						melt_ptr_t meltfirstargp_,
						const melt_argdescr_cell_t
						meltxargdescr_[],
						union meltparam_un *
						meltxargtab_,
						const melt_argdescr_cell_t
						meltxresdescr_[],
						union meltparam_un *
						meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_136_warmelt_outobj_COMPILE_LIST_SEXPR_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_136_warmelt_outobj_COMPILE_LIST_SEXPR_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 62
    melt_ptr_t mcfr_varptr[62];
#define MELTFRAM_NBVARNUM 27
    long mcfr_varnum[27];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_136_warmelt_outobj_COMPILE_LIST_SEXPR is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_136_warmelt_outobj_COMPILE_LIST_SEXPR_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 62; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_136_warmelt_outobj_COMPILE_LIST_SEXPR nbval 62*/
  meltfram__.mcfr_nbvar = 62 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("COMPILE_LIST_SEXPR", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-outobj.melt:5890:/ getarg");
 /*_.LSEXP__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.INIENV__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.INIENV__V3*/ meltfptr[2])) !=
	      NULL);


  /*getarg#2 */
  /*^getarg */
  if (meltxargdescr_[1] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.MODNAMSTR__V4*/ meltfptr[3] =
    (meltxargtab_[1].meltbp_aptr) ? (*(meltxargtab_[1].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.MODNAMSTR__V4*/ meltfptr[3])) !=
	      NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


    {
      MELT_LOCATION ("warmelt-outobj.melt:5891:/ locexp");
      debugeputs (("starting  compile_list_sexpr"));
    }
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:5892:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L1*/ meltfnum[0] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-outobj.melt:5892:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L1*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-outobj.melt:5892:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[9];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-outobj.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 5892;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "@*@@@@ compile_list_sexpr lsexp=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.LSEXP__V2*/ meltfptr[1];
	      /*^apply.arg */
	      argtab[5].meltbp_cstring = "\n@ compile_list_sexpr  inienv=";
	      /*^apply.arg */
	      argtab[6].meltbp_aptr =
		(melt_ptr_t *) & /*_.INIENV__V3*/ meltfptr[2];
	      /*^apply.arg */
	      argtab[7].meltbp_cstring = "\n@ compile_list_sexpr  modnamstr=";
	      /*^apply.arg */
	      argtab[8].meltbp_aptr =
		(melt_ptr_t *) & /*_.MODNAMSTR__V4*/ meltfptr[3];
	      /*_.MELT_DEBUG_FUN__V7*/ meltfptr[6] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V6*/ meltfptr[5] = /*_.MELT_DEBUG_FUN__V7*/ meltfptr[6];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-outobj.melt:5892:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V7*/ meltfptr[6] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V6*/ meltfptr[5] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-outobj.melt:5892:/ quasiblock");


      /*_.PROGN___V8*/ meltfptr[6] = /*_.IF___V6*/ meltfptr[5];;
      /*^compute */
      /*_.IFCPP___V5*/ meltfptr[4] = /*_.PROGN___V8*/ meltfptr[6];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:5892:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L1*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IF___V6*/ meltfptr[5] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V8*/ meltfptr[6] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V5*/ meltfptr[4] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:5894:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_LIST__L3*/ meltfnum[1] =
	(melt_magic_discr ((melt_ptr_t) ( /*_.LSEXP__V2*/ meltfptr[1])) ==
	 MELTOBMAG_LIST);;
      MELT_LOCATION ("warmelt-outobj.melt:5894:/ cond");
      /*cond */ if ( /*_#IS_LIST__L3*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V10*/ meltfptr[6] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-outobj.melt:5894:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check lsexp"),
				  ("warmelt-outobj.melt")
				  ? ("warmelt-outobj.melt") : __FILE__,
				  (5894) ? (5894) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V10*/ meltfptr[6] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V9*/ meltfptr[5] = /*_.IFELSE___V10*/ meltfptr[6];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:5894:/ clear");
	     /*clear *//*_#IS_LIST__L3*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V10*/ meltfptr[6] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V9*/ meltfptr[5] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:5895:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_STRING__L4*/ meltfnum[0] =
	(melt_magic_discr ((melt_ptr_t) ( /*_.MODNAMSTR__V4*/ meltfptr[3])) ==
	 MELTOBMAG_STRING);;
      MELT_LOCATION ("warmelt-outobj.melt:5895:/ cond");
      /*cond */ if ( /*_#IS_STRING__L4*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V12*/ meltfptr[11] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-outobj.melt:5895:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check modnamstr"),
				  ("warmelt-outobj.melt")
				  ? ("warmelt-outobj.melt") : __FILE__,
				  (5895) ? (5895) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V12*/ meltfptr[11] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V11*/ meltfptr[6] = /*_.IFELSE___V12*/ meltfptr[11];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:5895:/ clear");
	     /*clear *//*_#IS_STRING__L4*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V12*/ meltfptr[11] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V11*/ meltfptr[6] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:5896:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#NULL__L5*/ meltfnum[1] =
	(( /*_.INIENV__V3*/ meltfptr[2]) == NULL);;
      MELT_LOCATION ("warmelt-outobj.melt:5896:/ cond");
      /*cond */ if ( /*_#NULL__L5*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_#OR___L6*/ meltfnum[0] = /*_#NULL__L5*/ meltfnum[1];;
	}
      else
	{
	  MELT_LOCATION ("warmelt-outobj.melt:5896:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {

     /*_#IS_A__L7*/ meltfnum[6] =
	      melt_is_instance_of ((melt_ptr_t)
				   ( /*_.INIENV__V3*/ meltfptr[2]),
				   (melt_ptr_t) (( /*!CLASS_ENVIRONMENT */
						  meltfrout->tabval[1])));;
	    /*^compute */
	    /*_#OR___L6*/ meltfnum[0] = /*_#IS_A__L7*/ meltfnum[6];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-outobj.melt:5896:/ clear");
	       /*clear *//*_#IS_A__L7*/ meltfnum[6] = 0;
	  }
	  ;
	}
      ;
      /*^cond */
      /*cond */ if ( /*_#OR___L6*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V14*/ meltfptr[13] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-outobj.melt:5896:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check inienv"),
				  ("warmelt-outobj.melt")
				  ? ("warmelt-outobj.melt") : __FILE__,
				  (5896) ? (5896) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V14*/ meltfptr[13] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V13*/ meltfptr[11] = /*_.IFELSE___V14*/ meltfptr[13];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:5896:/ clear");
	     /*clear *//*_#NULL__L5*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_#OR___L6*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V14*/ meltfptr[13] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V13*/ meltfptr[11] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:5897:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#STRING_DYNLOADED_SUFFIXED__L8*/ meltfnum[6] =
	/*string_dynloaded_suffixed: */
	(melt_string_is_ending ((melt_ptr_t) /*_.MODNAMSTR__V4*/ meltfptr[3],
				MELT_DYNLOADED_SUFFIX));;
      /*^compute */
   /*_#NOT__L9*/ meltfnum[1] =
	(!( /*_#STRING_DYNLOADED_SUFFIXED__L8*/ meltfnum[6]));;
      MELT_LOCATION ("warmelt-outobj.melt:5897:/ cond");
      /*cond */ if ( /*_#NOT__L9*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V16*/ meltfptr[15] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-outobj.melt:5897:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("compile_list_sexpr modulename not ended with MELT_DYNLOADED_SUFFIX."), ("warmelt-outobj.melt") ? ("warmelt-outobj.melt") : __FILE__, (5897) ? (5897) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V16*/ meltfptr[15] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V15*/ meltfptr[13] = /*_.IFELSE___V16*/ meltfptr[15];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:5897:/ clear");
	     /*clear *//*_#STRING_DYNLOADED_SUFFIXED__L8*/ meltfnum[6] = 0;
      /*^clear */
	     /*clear *//*_#NOT__L9*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V16*/ meltfptr[15] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V15*/ meltfptr[13] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:5899:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#STRING_SUFFIXED__L10*/ meltfnum[0] =
	/*string_suffixed: */
	(melt_string_is_ending
	 ((melt_ptr_t) /*_.MODNAMSTR__V4*/ meltfptr[3], ".melt"));;
      /*^compute */
   /*_#NOT__L11*/ meltfnum[6] =
	(!( /*_#STRING_SUFFIXED__L10*/ meltfnum[0]));;
      MELT_LOCATION ("warmelt-outobj.melt:5899:/ cond");
      /*cond */ if ( /*_#NOT__L11*/ meltfnum[6])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V18*/ meltfptr[17] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-outobj.melt:5899:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("compile_list_sexpr modulename not ended with .melt"), ("warmelt-outobj.melt") ? ("warmelt-outobj.melt") : __FILE__, (5899) ? (5899) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V18*/ meltfptr[17] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V17*/ meltfptr[15] = /*_.IFELSE___V18*/ meltfptr[17];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:5899:/ clear");
	     /*clear *//*_#STRING_SUFFIXED__L10*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_#NOT__L11*/ meltfnum[6] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V18*/ meltfptr[17] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V17*/ meltfptr[15] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:5901:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#STRING_SUFFIXED__L12*/ meltfnum[1] =
	/*string_suffixed: */
	(melt_string_is_ending
	 ((melt_ptr_t) /*_.MODNAMSTR__V4*/ meltfptr[3], ".c"));;
      /*^compute */
   /*_#NOT__L13*/ meltfnum[0] =
	(!( /*_#STRING_SUFFIXED__L12*/ meltfnum[1]));;
      MELT_LOCATION ("warmelt-outobj.melt:5901:/ cond");
      /*cond */ if ( /*_#NOT__L13*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V20*/ meltfptr[19] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-outobj.melt:5901:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("compile_list_sexpr modulename not ended with .c"), ("warmelt-outobj.melt") ? ("warmelt-outobj.melt") : __FILE__, (5901) ? (5901) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V20*/ meltfptr[19] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V19*/ meltfptr[17] = /*_.IFELSE___V20*/ meltfptr[19];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:5901:/ clear");
	     /*clear *//*_#STRING_SUFFIXED__L12*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_#NOT__L13*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V20*/ meltfptr[19] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V19*/ meltfptr[17] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-outobj.melt:5903:/ quasiblock");


 /*_.MODNAKEDNAME__V22*/ meltfptr[21] =
      (meltgc_new_string_nakedbasename
       ((meltobject_ptr_t) (( /*!DISCR_STRING */ meltfrout->tabval[2])),
	melt_string_str ((melt_ptr_t) ( /*_.MODNAMSTR__V4*/ meltfptr[3]))));;
    MELT_LOCATION ("warmelt-outobj.melt:5905:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_.MAKE_MAPSTRING__V23*/ meltfptr[22] =
      (meltgc_new_mapstrings
       ((meltobject_ptr_t) (( /*!DISCR_MAP_STRINGS */ meltfrout->tabval[4])),
	(390)));;
    /*^compute */
 /*_.MAKE_MAPSTRING__V24*/ meltfptr[23] =
      (meltgc_new_mapstrings
       ((meltobject_ptr_t) (( /*!DISCR_MAP_STRINGS */ meltfrout->tabval[4])),
	(140)));;
    /*^compute */
 /*_.MAKE_INTEGERBOX__V25*/ meltfptr[24] =
      (meltgc_new_int
       ((meltobject_ptr_t) (( /*!DISCR_INTEGER */ meltfrout->tabval[5])),
	(0)));;
    /*^compute */
 /*_.MAKE_LIST__V26*/ meltfptr[25] =
      (meltgc_new_list
       ((meltobject_ptr_t) (( /*!DISCR_LIST */ meltfrout->tabval[6]))));;
    /*^compute */
 /*_.MAKE_LIST__V27*/ meltfptr[26] =
      (meltgc_new_list
       ((meltobject_ptr_t) (( /*!DISCR_LIST */ meltfrout->tabval[6]))));;
    /*^compute */
 /*_.MAKE_LIST__V28*/ meltfptr[27] =
      (meltgc_new_list
       ((meltobject_ptr_t) (( /*!DISCR_LIST */ meltfrout->tabval[6]))));;
    MELT_LOCATION ("warmelt-outobj.melt:5917:/ quasiblock");


    /*^newclosure */
		 /*newclosure *//*_.LAMBDA___V30*/ meltfptr[29] =
      (melt_ptr_t)
      meltgc_new_closure ((meltobject_ptr_t)
			  (((melt_ptr_t) (MELT_PREDEF (DISCR_CLOSURE)))),
			  (meltroutine_ptr_t) (( /*!konst_8 */ meltfrout->
						tabval[8])), (1));
    ;
    /*^putclosedv */
    /*putclosv */
    melt_assertmsg ("putclosv checkclo",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.LAMBDA___V30*/ meltfptr[29])) ==
		    MELTOBMAG_CLOSURE);
    melt_assertmsg ("putclosv checkoff", 0 >= 0
		    && 0 <
		    melt_closure_size ((melt_ptr_t)
				       ( /*_.LAMBDA___V30*/ meltfptr[29])));
    ((meltclosure_ptr_t) /*_.LAMBDA___V30*/ meltfptr[29])->tabval[0] =
      (melt_ptr_t) ( /*_.MODNAMSTR__V4*/ meltfptr[3]);
    ;
    /*_.LAMBDA___V29*/ meltfptr[28] = /*_.LAMBDA___V30*/ meltfptr[29];;
    MELT_LOCATION ("warmelt-outobj.melt:5905:/ quasiblock");


    /*^rawallocobj */
    /*rawallocobj */
    {
      melt_ptr_t newobj = 0;
      melt_raw_object_create (newobj,
			      (melt_ptr_t) (( /*!CLASS_MODULE_CONTEXT */
					     meltfrout->tabval[3])), (12),
			      "CLASS_MODULE_CONTEXT");
  /*_.INST__V32*/ meltfptr[31] =
	newobj;
    };
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @MOCX_MODULENAME",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V32*/ meltfptr[31])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V32*/ meltfptr[31]), (0),
			  ( /*_.MODNAKEDNAME__V22*/ meltfptr[21]),
			  "MOCX_MODULENAME");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @MOCX_EXPFIELDICT",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V32*/ meltfptr[31])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V32*/ meltfptr[31]), (1),
			  ( /*_.MAKE_MAPSTRING__V23*/ meltfptr[22]),
			  "MOCX_EXPFIELDICT");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @MOCX_EXPCLASSDICT",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V32*/ meltfptr[31])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V32*/ meltfptr[31]), (2),
			  ( /*_.MAKE_MAPSTRING__V24*/ meltfptr[23]),
			  "MOCX_EXPCLASSDICT");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @MOCX_INITIALENV",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V32*/ meltfptr[31])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V32*/ meltfptr[31]), (3),
			  ( /*_.INIENV__V3*/ meltfptr[2]), "MOCX_INITIALENV");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @MOCX_FUNCOUNT",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V32*/ meltfptr[31])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V32*/ meltfptr[31]), (4),
			  ( /*_.MAKE_INTEGERBOX__V25*/ meltfptr[24]),
			  "MOCX_FUNCOUNT");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @MOCX_FILETUPLE",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V32*/ meltfptr[31])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V32*/ meltfptr[31]), (5),
			  (( /*nil */ NULL)), "MOCX_FILETUPLE");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @MOCX_CHEADERLIST",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V32*/ meltfptr[31])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V32*/ meltfptr[31]), (8),
			  ( /*_.MAKE_LIST__V26*/ meltfptr[25]),
			  "MOCX_CHEADERLIST");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @MOCX_PACKAGEPCLIST",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V32*/ meltfptr[31])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V32*/ meltfptr[31]), (10),
			  ( /*_.MAKE_LIST__V27*/ meltfptr[26]),
			  "MOCX_PACKAGEPCLIST");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @MOCX_GENDEVLIST",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V32*/ meltfptr[31])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V32*/ meltfptr[31]), (9),
			  ( /*_.MAKE_LIST__V28*/ meltfptr[27]),
			  "MOCX_GENDEVLIST");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @MOCX_ERRORHANDLER",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V32*/ meltfptr[31])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V32*/ meltfptr[31]), (11),
			  ( /*_.LAMBDA___V29*/ meltfptr[28]),
			  "MOCX_ERRORHANDLER");
    ;
    /*^touchobj */

    melt_dbgtrace_written_object ( /*_.INST__V32*/ meltfptr[31],
				  "newly made instance");
    ;
    /*_.MODCTX__V31*/ meltfptr[30] = /*_.INST__V32*/ meltfptr[31];;
    MELT_LOCATION ("warmelt-outobj.melt:5919:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      /*_.NCX__V33*/ meltfptr[32] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!CREATE_NORMCONTEXT */ meltfrout->tabval[9])),
		    (melt_ptr_t) ( /*_.MODCTX__V31*/ meltfptr[30]), (""),
		    (union meltparam_un *) 0, "", (union meltparam_un *) 0);
    }
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:5921:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L14*/ meltfnum[6] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-outobj.melt:5921:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L14*/ meltfnum[6])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L15*/ meltfnum[1] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    /*^compute */
     /*_.DISCRIM__V36*/ meltfptr[35] =
	      ((melt_ptr_t)
	       (melt_discr
		((melt_ptr_t) ( /*_.MODCTX__V31*/ meltfptr[30]))));;
	    MELT_LOCATION ("warmelt-outobj.melt:5921:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[7];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long =
		/*_#THE_MELTCALLCOUNT__L15*/ meltfnum[1];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-outobj.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 5921;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "compile_list_sexpr modctx=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.MODCTX__V31*/ meltfptr[30];
	      /*^apply.arg */
	      argtab[5].meltbp_cstring = "\n of class ";
	      /*^apply.arg */
	      argtab[6].meltbp_aptr =
		(melt_ptr_t *) & /*_.DISCRIM__V36*/ meltfptr[35];
	      /*_.MELT_DEBUG_FUN__V37*/ meltfptr[36] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V35*/ meltfptr[34] =
	      /*_.MELT_DEBUG_FUN__V37*/ meltfptr[36];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-outobj.melt:5921:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L15*/ meltfnum[1] = 0;
	    /*^clear */
	       /*clear *//*_.DISCRIM__V36*/ meltfptr[35] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V37*/ meltfptr[36] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V35*/ meltfptr[34] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-outobj.melt:5921:/ quasiblock");


      /*_.PROGN___V38*/ meltfptr[35] = /*_.IF___V35*/ meltfptr[34];;
      /*^compute */
      /*_.IFCPP___V34*/ meltfptr[33] = /*_.PROGN___V38*/ meltfptr[35];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:5921:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L14*/ meltfnum[6] = 0;
      /*^clear */
	     /*clear *//*_.IF___V35*/ meltfptr[34] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V38*/ meltfptr[35] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V34*/ meltfptr[33] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-outobj.melt:5925:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#NULL__L16*/ meltfnum[0] =
      (( /*_.INIENV__V3*/ meltfptr[2]) == NULL);;
    MELT_LOCATION ("warmelt-outobj.melt:5925:/ cond");
    /*cond */ if ( /*_#NULL__L16*/ meltfnum[0])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{


	  {
	    MELT_LOCATION ("warmelt-outobj.melt:5928:/ locexp");
	    /* CHECKBOOTSTRAPPING_CHK__1 */
	    if (!melt_flag_bootstrapping)
	      melt_fatal_error
		("lack of initial environment can only happen during MELT bootstrapping in module %s",
		 melt_string_str ((melt_ptr_t) /*_.MODNAKEDNAME__V22*/
				  meltfptr[21]));
	    ;
	  }
	  ;
	  MELT_LOCATION ("warmelt-outobj.melt:5934:/ compute");
	  /*_.INIENV__V3*/ meltfptr[2] = /*_.SETQ___V40*/ meltfptr[34] =
	    ( /*!INITIAL_ENVIRONMENT */ meltfrout->tabval[10]);;
	  MELT_LOCATION ("warmelt-outobj.melt:5925:/ quasiblock");


	  /*_.PROGN___V41*/ meltfptr[35] = /*_.SETQ___V40*/ meltfptr[34];;
	  /*^compute */
	  /*_.IF___V39*/ meltfptr[36] = /*_.PROGN___V41*/ meltfptr[35];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-outobj.melt:5925:/ clear");
	     /*clear *//*_.SETQ___V40*/ meltfptr[34] = 0;
	  /*^clear */
	     /*clear *//*_.PROGN___V41*/ meltfptr[35] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

  /*_.IF___V39*/ meltfptr[36] = NULL;;
      }
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:5935:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L17*/ meltfnum[1] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-outobj.melt:5935:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L17*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L18*/ meltfnum[6] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-outobj.melt:5935:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long =
		/*_#THE_MELTCALLCOUNT__L18*/ meltfnum[6];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-outobj.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 5935;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "compile_list_sexpr initial ncx=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.NCX__V33*/ meltfptr[32];
	      /*_.MELT_DEBUG_FUN__V44*/ meltfptr[43] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V43*/ meltfptr[35] =
	      /*_.MELT_DEBUG_FUN__V44*/ meltfptr[43];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-outobj.melt:5935:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L18*/ meltfnum[6] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V44*/ meltfptr[43] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V43*/ meltfptr[35] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-outobj.melt:5935:/ quasiblock");


      /*_.PROGN___V45*/ meltfptr[43] = /*_.IF___V43*/ meltfptr[35];;
      /*^compute */
      /*_.IFCPP___V42*/ meltfptr[34] = /*_.PROGN___V45*/ meltfptr[43];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:5935:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L17*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IF___V43*/ meltfptr[35] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V45*/ meltfptr[43] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V42*/ meltfptr[34] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:5936:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L19*/ meltfnum[6] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.NCX__V33*/ meltfptr[32]),
			     (melt_ptr_t) (( /*!CLASS_NORMALIZATION_CONTEXT */
					    meltfrout->tabval[11])));;
      MELT_LOCATION ("warmelt-outobj.melt:5936:/ cond");
      /*cond */ if ( /*_#IS_A__L19*/ meltfnum[6])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V47*/ meltfptr[43] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-outobj.melt:5936:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check ncx"),
				  ("warmelt-outobj.melt")
				  ? ("warmelt-outobj.melt") : __FILE__,
				  (5936) ? (5936) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V47*/ meltfptr[43] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V46*/ meltfptr[35] = /*_.IFELSE___V47*/ meltfptr[43];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:5936:/ clear");
	     /*clear *//*_#IS_A__L19*/ meltfnum[6] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V47*/ meltfptr[43] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V46*/ meltfptr[35] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-outobj.melt:5937:/ quasiblock");


    /*^checksignal */
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[3];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.INIENV__V3*/ meltfptr[2];
      /*^apply.arg */
      argtab[1].meltbp_aptr =
	(melt_ptr_t *) & ( /*!MACROEXPAND_1 */ meltfrout->tabval[13]);
      /*^apply.arg */
      argtab[2].meltbp_aptr = (melt_ptr_t *) & /*_.MODCTX__V31*/ meltfptr[30];
      /*_.XLIST__V49*/ meltfptr[48] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!MACROEXPAND_TOPLEVEL_LIST */ meltfrout->
		      tabval[12])),
		    (melt_ptr_t) ( /*_.LSEXP__V2*/ meltfptr[1]),
		    (MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_PTR ""),
		    argtab, "", (union meltparam_un *) 0);
    }
    ;
 /*_#LENXLIST__L20*/ meltfnum[1] =
      (melt_list_length ((melt_ptr_t) ( /*_.XLIST__V49*/ meltfptr[48])));;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:5940:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L21*/ meltfnum[6] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-outobj.melt:5940:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L21*/ meltfnum[6])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L22*/ meltfnum[21] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-outobj.melt:5940:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long =
		/*_#THE_MELTCALLCOUNT__L22*/ meltfnum[21];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-outobj.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 5940;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring =
		"compile_list_sexpr after macroexpansion modctx=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.MODCTX__V31*/ meltfptr[30];
	      /*_.MELT_DEBUG_FUN__V52*/ meltfptr[51] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V51*/ meltfptr[50] =
	      /*_.MELT_DEBUG_FUN__V52*/ meltfptr[51];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-outobj.melt:5940:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L22*/ meltfnum[21] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V52*/ meltfptr[51] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V51*/ meltfptr[50] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-outobj.melt:5940:/ quasiblock");


      /*_.PROGN___V53*/ meltfptr[51] = /*_.IF___V51*/ meltfptr[50];;
      /*^compute */
      /*_.IFCPP___V50*/ meltfptr[49] = /*_.PROGN___V53*/ meltfptr[51];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:5940:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L21*/ meltfnum[6] = 0;
      /*^clear */
	     /*clear *//*_.IF___V51*/ meltfptr[50] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V53*/ meltfptr[51] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V50*/ meltfptr[49] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-outobj.melt:5942:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#I__L23*/ meltfnum[21] =
      (( /*_#LENXLIST__L20*/ meltfnum[1]) <= (3));;
    MELT_LOCATION ("warmelt-outobj.melt:5942:/ cond");
    /*cond */ if ( /*_#I__L23*/ meltfnum[21])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{


	  {
	    MELT_LOCATION ("warmelt-outobj.melt:5944:/ locexp");
	    /* compile_list_sexpr WARNSHORTCHK__1 */
	    warning (0, "MELT expanded few (%d) expressions",
		     (int) /*_#LENXLIST__L20*/ meltfnum[1]);
	    ;
	  }
	  ;
	  /*epilog */
	}
	;
      }				/*noelse */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:5947:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L24*/ meltfnum[6] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-outobj.melt:5947:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L24*/ meltfnum[6])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L25*/ meltfnum[24] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-outobj.melt:5947:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long =
		/*_#THE_MELTCALLCOUNT__L25*/ meltfnum[24];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-outobj.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 5947;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring =
		"compile_list_sexpr before translation modctx=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.MODCTX__V31*/ meltfptr[30];
	      /*_.MELT_DEBUG_FUN__V56*/ meltfptr[55] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V55*/ meltfptr[51] =
	      /*_.MELT_DEBUG_FUN__V56*/ meltfptr[55];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-outobj.melt:5947:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L25*/ meltfnum[24] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V56*/ meltfptr[55] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V55*/ meltfptr[51] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-outobj.melt:5947:/ quasiblock");


      /*_.PROGN___V57*/ meltfptr[55] = /*_.IF___V55*/ meltfptr[51];;
      /*^compute */
      /*_.IFCPP___V54*/ meltfptr[50] = /*_.PROGN___V57*/ meltfptr[55];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:5947:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L24*/ meltfnum[6] = 0;
      /*^clear */
	     /*clear *//*_.IF___V55*/ meltfptr[51] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V57*/ meltfptr[55] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V54*/ meltfptr[50] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-outobj.melt:5949:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[6];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & /*_.MODNAMSTR__V4*/ meltfptr[3];
      /*^apply.arg */
      argtab[1].meltbp_aptr = (melt_ptr_t *) & /*_.MODCTX__V31*/ meltfptr[30];
      /*^apply.arg */
      argtab[2].meltbp_aptr = (melt_ptr_t *) & /*_.NCX__V33*/ meltfptr[32];
      /*^apply.arg */
      argtab[3].meltbp_aptr = (melt_ptr_t *) & /*_.INIENV__V3*/ meltfptr[2];
      /*^apply.arg */
      argtab[4].meltbp_aptr = /*nil */ (melt_ptr_t *) NULL;
      /*^apply.arg */
      argtab[5].meltbp_aptr =
	(melt_ptr_t *) & ( /*!COMPILE2OBJ_INITPROC */ meltfrout->tabval[15]);
      /*_.TRANSLATE_MACROEXPANDED_LIST__V58*/ meltfptr[51] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!TRANSLATE_MACROEXPANDED_LIST */ meltfrout->
		      tabval[14])),
		    (melt_ptr_t) ( /*_.XLIST__V49*/ meltfptr[48]),
		    (MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_PTR
		     MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_PTR ""),
		    argtab, "", (union meltparam_un *) 0);
    }
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:5950:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L26*/ meltfnum[24] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-outobj.melt:5950:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L26*/ meltfnum[24])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L27*/ meltfnum[6] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-outobj.melt:5950:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long =
		/*_#THE_MELTCALLCOUNT__L27*/ meltfnum[6];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-outobj.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 5950;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring =
		"compile_list_sexpr after translation modctx=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.MODCTX__V31*/ meltfptr[30];
	      /*_.MELT_DEBUG_FUN__V61*/ meltfptr[60] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V60*/ meltfptr[59] =
	      /*_.MELT_DEBUG_FUN__V61*/ meltfptr[60];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-outobj.melt:5950:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L27*/ meltfnum[6] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V61*/ meltfptr[60] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V60*/ meltfptr[59] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-outobj.melt:5950:/ quasiblock");


      /*_.PROGN___V62*/ meltfptr[60] = /*_.IF___V60*/ meltfptr[59];;
      /*^compute */
      /*_.IFCPP___V59*/ meltfptr[55] = /*_.PROGN___V62*/ meltfptr[60];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:5950:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L26*/ meltfnum[24] = 0;
      /*^clear */
	     /*clear *//*_.IF___V60*/ meltfptr[59] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V62*/ meltfptr[60] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V59*/ meltfptr[55] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    /*^compute */
    /*_.LET___V48*/ meltfptr[43] = /*_.IFCPP___V59*/ meltfptr[55];;

    MELT_LOCATION ("warmelt-outobj.melt:5937:/ clear");
	   /*clear *//*_.XLIST__V49*/ meltfptr[48] = 0;
    /*^clear */
	   /*clear *//*_#LENXLIST__L20*/ meltfnum[1] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V50*/ meltfptr[49] = 0;
    /*^clear */
	   /*clear *//*_#I__L23*/ meltfnum[21] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V54*/ meltfptr[50] = 0;
    /*^clear */
	   /*clear *//*_.TRANSLATE_MACROEXPANDED_LIST__V58*/ meltfptr[51] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V59*/ meltfptr[55] = 0;
    /*_.LET___V21*/ meltfptr[19] = /*_.LET___V48*/ meltfptr[43];;

    MELT_LOCATION ("warmelt-outobj.melt:5903:/ clear");
	   /*clear *//*_.MODNAKEDNAME__V22*/ meltfptr[21] = 0;
    /*^clear */
	   /*clear *//*_.MAKE_MAPSTRING__V23*/ meltfptr[22] = 0;
    /*^clear */
	   /*clear *//*_.MAKE_MAPSTRING__V24*/ meltfptr[23] = 0;
    /*^clear */
	   /*clear *//*_.MAKE_INTEGERBOX__V25*/ meltfptr[24] = 0;
    /*^clear */
	   /*clear *//*_.MAKE_LIST__V26*/ meltfptr[25] = 0;
    /*^clear */
	   /*clear *//*_.MAKE_LIST__V27*/ meltfptr[26] = 0;
    /*^clear */
	   /*clear *//*_.MAKE_LIST__V28*/ meltfptr[27] = 0;
    /*^clear */
	   /*clear *//*_.LAMBDA___V29*/ meltfptr[28] = 0;
    /*^clear */
	   /*clear *//*_.MODCTX__V31*/ meltfptr[30] = 0;
    /*^clear */
	   /*clear *//*_.NCX__V33*/ meltfptr[32] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V34*/ meltfptr[33] = 0;
    /*^clear */
	   /*clear *//*_#NULL__L16*/ meltfnum[0] = 0;
    /*^clear */
	   /*clear *//*_.IF___V39*/ meltfptr[36] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V42*/ meltfptr[34] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V46*/ meltfptr[35] = 0;
    /*^clear */
	   /*clear *//*_.LET___V48*/ meltfptr[43] = 0;
    MELT_LOCATION ("warmelt-outobj.melt:5890:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.LET___V21*/ meltfptr[19];;

    {
      MELT_LOCATION ("warmelt-outobj.melt:5890:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V5*/ meltfptr[4] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V9*/ meltfptr[5] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V11*/ meltfptr[6] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V13*/ meltfptr[11] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V15*/ meltfptr[13] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V17*/ meltfptr[15] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V19*/ meltfptr[17] = 0;
    /*^clear */
	   /*clear *//*_.LET___V21*/ meltfptr[19] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("COMPILE_LIST_SEXPR", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_136_warmelt_outobj_COMPILE_LIST_SEXPR_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_136_warmelt_outobj_COMPILE_LIST_SEXPR */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_137_warmelt_outobj_LAMBDA___28__ (meltclosure_ptr_t meltclosp_,
					   melt_ptr_t meltfirstargp_,
					   const melt_argdescr_cell_t
					   meltxargdescr_[],
					   union meltparam_un * meltxargtab_,
					   const melt_argdescr_cell_t
					   meltxresdescr_[],
					   union meltparam_un * meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_137_warmelt_outobj_LAMBDA___28___melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_137_warmelt_outobj_LAMBDA___28___st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 3
    melt_ptr_t mcfr_varptr[3];
/*no varnum*/
#define MELTFRAM_NBVARNUM /*none*/0
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_137_warmelt_outobj_LAMBDA___28__ is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_137_warmelt_outobj_LAMBDA___28___st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 3; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_137_warmelt_outobj_LAMBDA___28__ nbval 3*/
  meltfram__.mcfr_nbvar = 3 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("LAMBDA_", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-outobj.melt:5917:/ getarg");
 /*_.V__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {

    /*^checksignal */
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.V__V2*/ meltfptr[1];
      /*_.FATAL_COMPILE_ERROR__V3*/ meltfptr[2] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!FATAL_COMPILE_ERROR */ meltfrout->tabval[0])),
		    (melt_ptr_t) (( /*~MODNAMSTR */ meltfclos->tabval[0])),
		    (MELTBPARSTR_PTR ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] =
      /*_.FATAL_COMPILE_ERROR__V3*/ meltfptr[2];;

    {
      MELT_LOCATION ("warmelt-outobj.melt:5917:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.FATAL_COMPILE_ERROR__V3*/ meltfptr[2] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("LAMBDA_", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_137_warmelt_outobj_LAMBDA___28___melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_137_warmelt_outobj_LAMBDA___28__ */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_138_warmelt_outobj_AUTOBOX_NORMAL_RETURN (meltclosure_ptr_t
						   meltclosp_,
						   melt_ptr_t meltfirstargp_,
						   const melt_argdescr_cell_t
						   meltxargdescr_[],
						   union meltparam_un *
						   meltxargtab_,
						   const melt_argdescr_cell_t
						   meltxresdescr_[],
						   union meltparam_un *
						   meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_138_warmelt_outobj_AUTOBOX_NORMAL_RETURN_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_138_warmelt_outobj_AUTOBOX_NORMAL_RETURN_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 99
    melt_ptr_t mcfr_varptr[99];
#define MELTFRAM_NBVARNUM 22
    long mcfr_varnum[22];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_138_warmelt_outobj_AUTOBOX_NORMAL_RETURN is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_138_warmelt_outobj_AUTOBOX_NORMAL_RETURN_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 99; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_138_warmelt_outobj_AUTOBOX_NORMAL_RETURN nbval 99*/
  meltfram__.mcfr_nbvar = 99 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("AUTOBOX_NORMAL_RETURN", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-outobj.melt:5971:/ getarg");
 /*_.NEXP__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.NBINDS__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.NBINDS__V3*/ meltfptr[2])) !=
	      NULL);


  /*getarg#2 */
  /*^getarg */
  if (meltxargdescr_[1] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.CTYP__V4*/ meltfptr[3] =
    (meltxargtab_[1].meltbp_aptr) ? (*(meltxargtab_[1].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.CTYP__V4*/ meltfptr[3])) !=
	      NULL);


  /*getarg#3 */
  /*^getarg */
  if (meltxargdescr_[2] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.NCX__V5*/ meltfptr[4] =
    (meltxargtab_[2].meltbp_aptr) ? (*(meltxargtab_[2].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.NCX__V5*/ meltfptr[4])) != NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:5972:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L1*/ meltfnum[0] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-outobj.melt:5972:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L1*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-outobj.melt:5972:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[11];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-outobj.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 5972;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "autobox_normal_return nexp=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.NEXP__V2*/ meltfptr[1];
	      /*^apply.arg */
	      argtab[5].meltbp_cstring = " nbinds=";
	      /*^apply.arg */
	      argtab[6].meltbp_aptr =
		(melt_ptr_t *) & /*_.NBINDS__V3*/ meltfptr[2];
	      /*^apply.arg */
	      argtab[7].meltbp_cstring = " ctyp=";
	      /*^apply.arg */
	      argtab[8].meltbp_aptr =
		(melt_ptr_t *) & /*_.CTYP__V4*/ meltfptr[3];
	      /*^apply.arg */
	      argtab[9].meltbp_cstring = " ncx=";
	      /*^apply.arg */
	      argtab[10].meltbp_aptr =
		(melt_ptr_t *) & /*_.NCX__V5*/ meltfptr[4];
	      /*_.MELT_DEBUG_FUN__V8*/ meltfptr[7] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V7*/ meltfptr[6] = /*_.MELT_DEBUG_FUN__V8*/ meltfptr[7];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-outobj.melt:5972:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V8*/ meltfptr[7] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V7*/ meltfptr[6] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-outobj.melt:5972:/ quasiblock");


      /*_.PROGN___V9*/ meltfptr[7] = /*_.IF___V7*/ meltfptr[6];;
      /*^compute */
      /*_.IFCPP___V6*/ meltfptr[5] = /*_.PROGN___V9*/ meltfptr[7];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:5972:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L1*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IF___V7*/ meltfptr[6] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V9*/ meltfptr[7] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V6*/ meltfptr[5] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-outobj.melt:5973:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#NULL__L3*/ meltfnum[1] =
      (( /*_.NBINDS__V3*/ meltfptr[2]) == NULL);;
    MELT_LOCATION ("warmelt-outobj.melt:5973:/ cond");
    /*cond */ if ( /*_#NULL__L3*/ meltfnum[1])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

   /*_.MAKE_LIST__V11*/ meltfptr[7] =
	    (meltgc_new_list
	     ((meltobject_ptr_t)
	      (( /*!DISCR_LIST */ meltfrout->tabval[1]))));;
	  MELT_LOCATION ("warmelt-outobj.melt:5973:/ compute");
	  /*_.NBINDS__V3*/ meltfptr[2] = /*_.SETQ___V12*/ meltfptr[11] =
	    /*_.MAKE_LIST__V11*/ meltfptr[7];;
	  /*_.IF___V10*/ meltfptr[6] = /*_.SETQ___V12*/ meltfptr[11];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-outobj.melt:5973:/ clear");
	     /*clear *//*_.MAKE_LIST__V11*/ meltfptr[7] = 0;
	  /*^clear */
	     /*clear *//*_.SETQ___V12*/ meltfptr[11] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

  /*_.IF___V10*/ meltfptr[6] = NULL;;
      }
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:5974:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L4*/ meltfnum[0] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.CTYP__V4*/ meltfptr[3]),
			     (melt_ptr_t) (( /*!CLASS_CTYPE */ meltfrout->
					    tabval[2])));;
      MELT_LOCATION ("warmelt-outobj.melt:5974:/ cond");
      /*cond */ if ( /*_#IS_A__L4*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V14*/ meltfptr[11] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-outobj.melt:5974:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check ctyp"),
				  ("warmelt-outobj.melt")
				  ? ("warmelt-outobj.melt") : __FILE__,
				  (5974) ? (5974) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V14*/ meltfptr[11] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V13*/ meltfptr[7] = /*_.IFELSE___V14*/ meltfptr[11];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:5974:/ clear");
	     /*clear *//*_#IS_A__L4*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V14*/ meltfptr[11] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V13*/ meltfptr[7] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:5975:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_LIST__L5*/ meltfnum[0] =
	(melt_magic_discr ((melt_ptr_t) ( /*_.NBINDS__V3*/ meltfptr[2])) ==
	 MELTOBMAG_LIST);;
      MELT_LOCATION ("warmelt-outobj.melt:5975:/ cond");
      /*cond */ if ( /*_#IS_LIST__L5*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V16*/ meltfptr[15] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-outobj.melt:5975:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check nbind"),
				  ("warmelt-outobj.melt")
				  ? ("warmelt-outobj.melt") : __FILE__,
				  (5975) ? (5975) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V16*/ meltfptr[15] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V15*/ meltfptr[11] = /*_.IFELSE___V16*/ meltfptr[15];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:5975:/ clear");
	     /*clear *//*_#IS_LIST__L5*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V16*/ meltfptr[15] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V15*/ meltfptr[11] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-outobj.melt:5976:/ quasiblock");


    /*^checksignal */
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      /*_.CSYM__V18*/ meltfptr[17] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!CLONE_SYMBOL */ meltfrout->tabval[3])),
		    (melt_ptr_t) (( /*!konst_4_RETAUTOBOXVAL */ meltfrout->
				   tabval[4])), (""),
		    (union meltparam_un *) 0, "", (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:5977:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*^rawallocobj */
    /*rawallocobj */
    {
      melt_ptr_t newobj = 0;
      melt_raw_object_create (newobj,
			      (melt_ptr_t) (( /*!CLASS_NORMAL_LET_BINDING */
					     meltfrout->tabval[5])), (4),
			      "CLASS_NORMAL_LET_BINDING");
  /*_.INST__V20*/ meltfptr[19] =
	newobj;
    };
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @LETBIND_LOC",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V20*/ meltfptr[19])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V20*/ meltfptr[19]), (3),
			  (( /*nil */ NULL)), "LETBIND_LOC");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @BINDER",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V20*/ meltfptr[19])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V20*/ meltfptr[19]), (0),
			  ( /*_.CSYM__V18*/ meltfptr[17]), "BINDER");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @LETBIND_TYPE",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V20*/ meltfptr[19])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V20*/ meltfptr[19]), (1),
			  (( /*!CTYPE_VALUE */ meltfrout->tabval[6])),
			  "LETBIND_TYPE");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @LETBIND_EXPR",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V20*/ meltfptr[19])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V20*/ meltfptr[19]), (2),
			  (( /*nil */ NULL)), "LETBIND_EXPR");
    ;
    /*^touchobj */

    melt_dbgtrace_written_object ( /*_.INST__V20*/ meltfptr[19],
				  "newly made instance");
    ;
    /*_.CBIND__V19*/ meltfptr[18] = /*_.INST__V20*/ meltfptr[19];;
    MELT_LOCATION ("warmelt-outobj.melt:5983:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*^rawallocobj */
    /*rawallocobj */
    {
      melt_ptr_t newobj = 0;
      melt_raw_object_create (newobj,
			      (melt_ptr_t) (( /*!CLASS_NREP_LOCSYMOCC */
					     meltfrout->tabval[7])), (4),
			      "CLASS_NREP_LOCSYMOCC");
  /*_.INST__V22*/ meltfptr[21] =
	newobj;
    };
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @NREP_LOC",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V22*/ meltfptr[21])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V22*/ meltfptr[21]), (0),
			  (( /*nil */ NULL)), "NREP_LOC");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @NOCC_CTYP",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V22*/ meltfptr[21])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V22*/ meltfptr[21]), (2),
			  (( /*!CTYPE_VALUE */ meltfrout->tabval[6])),
			  "NOCC_CTYP");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @NOCC_SYMB",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V22*/ meltfptr[21])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V22*/ meltfptr[21]), (1),
			  ( /*_.CSYM__V18*/ meltfptr[17]), "NOCC_SYMB");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @NOCC_BIND",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V22*/ meltfptr[21])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V22*/ meltfptr[21]), (3),
			  ( /*_.CBIND__V19*/ meltfptr[18]), "NOCC_BIND");
    ;
    /*^touchobj */

    melt_dbgtrace_written_object ( /*_.INST__V22*/ meltfptr[21],
				  "newly made instance");
    ;
    /*_.CLOCC__V21*/ meltfptr[20] = /*_.INST__V22*/ meltfptr[21];;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:5989:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L6*/ meltfnum[0] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-outobj.melt:5989:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L6*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L7*/ meltfnum[6] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-outobj.melt:5989:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[7];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L7*/ meltfnum[6];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-outobj.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 5989;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "autobox_normal_return clocc=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.CLOCC__V21*/ meltfptr[20];
	      /*^apply.arg */
	      argtab[5].meltbp_cstring = " incomplete cbind=";
	      /*^apply.arg */
	      argtab[6].meltbp_aptr =
		(melt_ptr_t *) & /*_.CBIND__V19*/ meltfptr[18];
	      /*_.MELT_DEBUG_FUN__V25*/ meltfptr[24] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V24*/ meltfptr[23] =
	      /*_.MELT_DEBUG_FUN__V25*/ meltfptr[24];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-outobj.melt:5989:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L7*/ meltfnum[6] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V25*/ meltfptr[24] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V24*/ meltfptr[23] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-outobj.melt:5989:/ quasiblock");


      /*_.PROGN___V26*/ meltfptr[24] = /*_.IF___V24*/ meltfptr[23];;
      /*^compute */
      /*_.IFCPP___V23*/ meltfptr[22] = /*_.PROGN___V26*/ meltfptr[24];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:5989:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L6*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IF___V24*/ meltfptr[23] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V26*/ meltfptr[24] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V23*/ meltfptr[22] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-outobj.melt:5991:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#__L8*/ meltfnum[6] =
      (( /*_.CTYP__V4*/ meltfptr[3]) ==
       (( /*!CTYPE_VALUE */ meltfrout->tabval[6])));;
    MELT_LOCATION ("warmelt-outobj.melt:5991:/ cond");
    /*cond */ if ( /*_#__L8*/ meltfnum[6])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-outobj.melt:5992:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
   /*_#IS_NOT_A__L9*/ meltfnum[0] =
	    !melt_is_instance_of ((melt_ptr_t) ( /*_.NEXP__V2*/ meltfptr[1]),
				  (melt_ptr_t) (( /*!CLASS_NREP_RETURN */
						 meltfrout->tabval[8])));;
	  MELT_LOCATION ("warmelt-outobj.melt:5992:/ cond");
	  /*cond */ if ( /*_#IS_NOT_A__L9*/ meltfnum[0])	/*then */
	    {
	      /*^cond.then */
	      /*^block */
	      /*anyblock */
	      {

		MELT_LOCATION ("warmelt-outobj.melt:5993:/ quasiblock");


		/*^checksignal */
		MELT_CHECK_SIGNAL ();
		;
		/*^quasiblock */


		/*^rawallocobj */
		/*rawallocobj */
		{
		  melt_ptr_t newobj = 0;
		  melt_raw_object_create (newobj,
					  (melt_ptr_t) (( /*!CLASS_NREP_RETURN */ meltfrout->tabval[8])), (3), "CLASS_NREP_RETURN");
      /*_.INST__V31*/ meltfptr[30] =
		    newobj;
		};
		;
		/*^putslot */
		/*putslot */
		melt_assertmsg ("putslot checkobj @NRET_MAIN",
				melt_magic_discr ((melt_ptr_t)
						  ( /*_.INST__V31*/
						   meltfptr[30])) ==
				MELTOBMAG_OBJECT);
		melt_putfield_object (( /*_.INST__V31*/ meltfptr[30]), (1),
				      ( /*_.NEXP__V2*/ meltfptr[1]),
				      "NRET_MAIN");
		;
		/*^touchobj */

		melt_dbgtrace_written_object ( /*_.INST__V31*/ meltfptr[30],
					      "newly made instance");
		;
		/*_.NRET__V30*/ meltfptr[29] = /*_.INST__V31*/ meltfptr[30];;

#if MELT_HAVE_DEBUG
		MELT_LOCATION ("warmelt-outobj.melt:5997:/ cppif.then");
		/*^block */
		/*anyblock */
		{


		  {
		    /*^locexp */
		    /*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
		    melt_dbgcounter++;
#endif
		    ;
		  }
		  ;
		  /*^checksignal */
		  MELT_CHECK_SIGNAL ();
		  ;
       /*_#MELT_NEED_DBG__L10*/ meltfnum[9] =
		    /*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
		    ( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
		    0		/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
		    ;;
		  MELT_LOCATION ("warmelt-outobj.melt:5997:/ cond");
		  /*cond */ if ( /*_#MELT_NEED_DBG__L10*/ meltfnum[9])	/*then */
		    {
		      /*^cond.then */
		      /*^block */
		      /*anyblock */
		      {

	 /*_#THE_MELTCALLCOUNT__L11*/ meltfnum[10] =
#ifdef meltcallcount
			  meltcallcount	/* the_meltcallcount */
#else
			  0L
#endif /* meltcallcount the_meltcallcount */
			  ;;
			MELT_LOCATION
			  ("warmelt-outobj.melt:5997:/ checksignal");
			MELT_CHECK_SIGNAL ();
			;
			/*^apply */
			/*apply */
			{
			  union meltparam_un argtab[7];
			  memset (&argtab, 0, sizeof (argtab));
			  /*^apply.arg */
			  argtab[0].meltbp_long =
			    /*_#THE_MELTCALLCOUNT__L11*/ meltfnum[10];
			  /*^apply.arg */
			  argtab[1].meltbp_cstring = "warmelt-outobj.melt";
			  /*^apply.arg */
			  argtab[2].meltbp_long = 5997;
			  /*^apply.arg */
			  argtab[3].meltbp_cstring =
			    "autobox_normal_return gives nret=";
			  /*^apply.arg */
			  argtab[4].meltbp_aptr =
			    (melt_ptr_t *) & /*_.NRET__V30*/ meltfptr[29];
			  /*^apply.arg */
			  argtab[5].meltbp_cstring = " nbinds=";
			  /*^apply.arg */
			  argtab[6].meltbp_aptr =
			    (melt_ptr_t *) & /*_.NBINDS__V3*/ meltfptr[2];
			  /*_.MELT_DEBUG_FUN__V34*/ meltfptr[33] =
			    melt_apply ((meltclosure_ptr_t)
					(( /*!MELT_DEBUG_FUN */ meltfrout->
					  tabval[0])),
					(melt_ptr_t) (( /*nil */ NULL)),
					(MELTBPARSTR_LONG MELTBPARSTR_CSTRING
					 MELTBPARSTR_LONG MELTBPARSTR_CSTRING
					 MELTBPARSTR_PTR MELTBPARSTR_CSTRING
					 MELTBPARSTR_PTR ""), argtab, "",
					(union meltparam_un *) 0);
			}
			;
			/*_.IF___V33*/ meltfptr[32] =
			  /*_.MELT_DEBUG_FUN__V34*/ meltfptr[33];;
			/*epilog */

			MELT_LOCATION ("warmelt-outobj.melt:5997:/ clear");
		   /*clear *//*_#THE_MELTCALLCOUNT__L11*/ meltfnum[10] =
			  0;
			/*^clear */
		   /*clear *//*_.MELT_DEBUG_FUN__V34*/ meltfptr[33] = 0;
		      }
		      ;
		    }
		  else
		    {		/*^cond.else */

	/*_.IF___V33*/ meltfptr[32] = NULL;;
		    }
		  ;
		  MELT_LOCATION ("warmelt-outobj.melt:5997:/ quasiblock");


		  /*_.PROGN___V35*/ meltfptr[33] =
		    /*_.IF___V33*/ meltfptr[32];;
		  /*^compute */
		  /*_.IFCPP___V32*/ meltfptr[31] =
		    /*_.PROGN___V35*/ meltfptr[33];;
		  /*epilog */

		  MELT_LOCATION ("warmelt-outobj.melt:5997:/ clear");
		 /*clear *//*_#MELT_NEED_DBG__L10*/ meltfnum[9] = 0;
		  /*^clear */
		 /*clear *//*_.IF___V33*/ meltfptr[32] = 0;
		  /*^clear */
		 /*clear *//*_.PROGN___V35*/ meltfptr[33] = 0;
		}

#else /*MELT_HAVE_DEBUG */
		/*^cppif.else */
		/*_.IFCPP___V32*/ meltfptr[31] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
		;
		MELT_LOCATION ("warmelt-outobj.melt:5998:/ checksignal");
		MELT_CHECK_SIGNAL ();
		;
		/*^quasiblock */


		/*_.RETVAL___V1*/ meltfptr[0] = /*_.NRET__V30*/ meltfptr[29];;
		MELT_LOCATION ("warmelt-outobj.melt:5998:/ putxtraresult");
		if (!meltxrestab_ || !meltxresdescr_)
		  goto labend_rout;
		if (meltxresdescr_[0] != MELTBPAR_PTR)
		  goto labend_rout;
		if (meltxrestab_[0].meltbp_aptr)
		  *(meltxrestab_[0].meltbp_aptr) =
		    (melt_ptr_t) ( /*_.NBINDS__V3*/ meltfptr[2]);
		;
		/*^finalreturn */
		;
		/*finalret */ goto labend_rout;
		/*_.LET___V29*/ meltfptr[28] =
		  /*_.RETURN___V36*/ meltfptr[32];;

		MELT_LOCATION ("warmelt-outobj.melt:5993:/ clear");
	       /*clear *//*_.NRET__V30*/ meltfptr[29] = 0;
		/*^clear */
	       /*clear *//*_.IFCPP___V32*/ meltfptr[31] = 0;
		/*^clear */
	       /*clear *//*_.RETURN___V36*/ meltfptr[32] = 0;
		/*_.IFELSE___V28*/ meltfptr[24] =
		  /*_.LET___V29*/ meltfptr[28];;
		/*epilog */

		MELT_LOCATION ("warmelt-outobj.melt:5992:/ clear");
	       /*clear *//*_.LET___V29*/ meltfptr[28] = 0;
	      }
	      ;
	    }
	  else
	    {			/*^cond.else */

	      /*^block */
	      /*anyblock */
	      {

		MELT_LOCATION ("warmelt-outobj.melt:5999:/ checksignal");
		MELT_CHECK_SIGNAL ();
		;
		/*^quasiblock */


		/*_.RETVAL___V1*/ meltfptr[0] = /*_.NEXP__V2*/ meltfptr[1];;
		MELT_LOCATION ("warmelt-outobj.melt:5999:/ putxtraresult");
		if (!meltxrestab_ || !meltxresdescr_)
		  goto labend_rout;
		if (meltxresdescr_[0] != MELTBPAR_PTR)
		  goto labend_rout;
		if (meltxrestab_[0].meltbp_aptr)
		  *(meltxrestab_[0].meltbp_aptr) =
		    (melt_ptr_t) ( /*_.NBINDS__V3*/ meltfptr[2]);
		;
		/*^finalreturn */
		;
		/*finalret */ goto labend_rout;
		/*_.IFELSE___V28*/ meltfptr[24] =
		  /*_.RETURN___V37*/ meltfptr[33];;
		/*epilog */

		MELT_LOCATION ("warmelt-outobj.melt:5992:/ clear");
	       /*clear *//*_.RETURN___V37*/ meltfptr[33] = 0;
	      }
	      ;
	    }
	  ;
	  /*_.IFELSE___V27*/ meltfptr[23] = /*_.IFELSE___V28*/ meltfptr[24];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-outobj.melt:5991:/ clear");
	     /*clear *//*_#IS_NOT_A__L9*/ meltfnum[0] = 0;
	  /*^clear */
	     /*clear *//*_.IFELSE___V28*/ meltfptr[24] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-outobj.melt:6001:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
   /*_#__L12*/ meltfnum[10] =
	    (( /*_.CTYP__V4*/ meltfptr[3]) ==
	     (( /*!CTYPE_LONG */ meltfrout->tabval[9])));;
	  MELT_LOCATION ("warmelt-outobj.melt:6001:/ cond");
	  /*cond */ if ( /*_#__L12*/ meltfnum[10])	/*then */
	    {
	      /*^cond.then */
	      /*^block */
	      /*anyblock */
	      {

		MELT_LOCATION ("warmelt-outobj.melt:6002:/ quasiblock");


		/*^checksignal */
		MELT_CHECK_SIGNAL ();
		;
     /*_.MAKE_STRINGCONST__V40*/ meltfptr[32] =
		  (meltgc_new_stringdup
		   ((meltobject_ptr_t)
		    (( /*!DISCR_VERBATIM_STRING */ meltfrout->tabval[11])),
		    ("/*autobox long*/ meltgc_new_int((meltobject_ptr_t)MELT_PREDEF(DISCR_CONSTANT_\
INTEGER), ")));;
		/*^compute */
     /*_.MAKE_STRINGCONST__V41*/ meltfptr[28] =
		  (meltgc_new_stringdup
		   ((meltobject_ptr_t)
		    (( /*!DISCR_VERBATIM_STRING */ meltfrout->tabval[11])),
		    (")")));;
		MELT_LOCATION ("warmelt-outobj.melt:6006:/ blockmultialloc");
		/*multiallocblock */
		{
		  struct meltletrec_1_st
		  {
		    struct MELT_MULTIPLE_STRUCT (3) rtup_0__TUPLREC__x5;
		    long meltletrec_1_endgap;
		  } *meltletrec_1_ptr = 0;
		  meltletrec_1_ptr =
		    (struct meltletrec_1_st *)
		    meltgc_allocate (sizeof (struct meltletrec_1_st), 0);
		  /*^blockmultialloc.initfill */
		  /*inimult rtup_0__TUPLREC__x5 */
 /*_.TUPLREC___V43*/ meltfptr[24] =
		    (melt_ptr_t) & meltletrec_1_ptr->rtup_0__TUPLREC__x5;
		  meltletrec_1_ptr->rtup_0__TUPLREC__x5.discr =
		    (meltobject_ptr_t) (((melt_ptr_t)
					 (MELT_PREDEF (DISCR_MULTIPLE))));
		  meltletrec_1_ptr->rtup_0__TUPLREC__x5.nbval = 3;


		  /*^putuple */
		  /*putupl#11 */
		  melt_assertmsg ("putupl [:6006] #11 checktup",
				  melt_magic_discr ((melt_ptr_t)
						    ( /*_.TUPLREC___V43*/
						     meltfptr[24])) ==
				  MELTOBMAG_MULTIPLE);
		  melt_assertmsg ("putupl [:6006] #11 checkoff",
				  (0 >= 0
				   && 0 <
				   melt_multiple_length ((melt_ptr_t)
							 ( /*_.TUPLREC___V43*/
							  meltfptr[24]))));
		  ((meltmultiple_ptr_t) ( /*_.TUPLREC___V43*/ meltfptr[24]))->
		    tabval[0] =
		    (melt_ptr_t) ( /*_.MAKE_STRINGCONST__V40*/ meltfptr[32]);
		  ;
		  /*^putuple */
		  /*putupl#12 */
		  melt_assertmsg ("putupl [:6006] #12 checktup",
				  melt_magic_discr ((melt_ptr_t)
						    ( /*_.TUPLREC___V43*/
						     meltfptr[24])) ==
				  MELTOBMAG_MULTIPLE);
		  melt_assertmsg ("putupl [:6006] #12 checkoff",
				  (1 >= 0
				   && 1 <
				   melt_multiple_length ((melt_ptr_t)
							 ( /*_.TUPLREC___V43*/
							  meltfptr[24]))));
		  ((meltmultiple_ptr_t) ( /*_.TUPLREC___V43*/ meltfptr[24]))->
		    tabval[1] = (melt_ptr_t) ( /*_.NEXP__V2*/ meltfptr[1]);
		  ;
		  /*^putuple */
		  /*putupl#13 */
		  melt_assertmsg ("putupl [:6006] #13 checktup",
				  melt_magic_discr ((melt_ptr_t)
						    ( /*_.TUPLREC___V43*/
						     meltfptr[24])) ==
				  MELTOBMAG_MULTIPLE);
		  melt_assertmsg ("putupl [:6006] #13 checkoff",
				  (2 >= 0
				   && 2 <
				   melt_multiple_length ((melt_ptr_t)
							 ( /*_.TUPLREC___V43*/
							  meltfptr[24]))));
		  ((meltmultiple_ptr_t) ( /*_.TUPLREC___V43*/ meltfptr[24]))->
		    tabval[2] =
		    (melt_ptr_t) ( /*_.MAKE_STRINGCONST__V41*/ meltfptr[28]);
		  ;
		  /*^touch */
		  meltgc_touch ( /*_.TUPLREC___V43*/ meltfptr[24]);
		  ;
		  /*_.TUPLE___V42*/ meltfptr[33] =
		    /*_.TUPLREC___V43*/ meltfptr[24];;
		  /*epilog */

		  MELT_LOCATION ("warmelt-outobj.melt:6006:/ clear");
		/*clear *//*_.TUPLREC___V43*/ meltfptr[24] = 0;
		  /*^clear */
		/*clear *//*_.TUPLREC___V43*/ meltfptr[24] = 0;
		}		/*end multiallocblock */
		;
		MELT_LOCATION ("warmelt-outobj.melt:6002:/ quasiblock");


		/*^rawallocobj */
		/*rawallocobj */
		{
		  melt_ptr_t newobj = 0;
		  melt_raw_object_create (newobj,
					  (melt_ptr_t) (( /*!CLASS_NREP_CHUNK */ meltfrout->tabval[10])), (4), "CLASS_NREP_CHUNK");
      /*_.INST__V45*/ meltfptr[44] =
		    newobj;
		};
		;
		/*^putslot */
		/*putslot */
		melt_assertmsg ("putslot checkobj @NREP_LOC",
				melt_magic_discr ((melt_ptr_t)
						  ( /*_.INST__V45*/
						   meltfptr[44])) ==
				MELTOBMAG_OBJECT);
		melt_putfield_object (( /*_.INST__V45*/ meltfptr[44]), (0),
				      (( /*nil */ NULL)), "NREP_LOC");
		;
		/*^putslot */
		/*putslot */
		melt_assertmsg ("putslot checkobj @NCHUNK_EXPANSION",
				melt_magic_discr ((melt_ptr_t)
						  ( /*_.INST__V45*/
						   meltfptr[44])) ==
				MELTOBMAG_OBJECT);
		melt_putfield_object (( /*_.INST__V45*/ meltfptr[44]), (2),
				      ( /*_.TUPLE___V42*/ meltfptr[33]),
				      "NCHUNK_EXPANSION");
		;
		/*^putslot */
		/*putslot */
		melt_assertmsg ("putslot checkobj @NCHUNK_OPER",
				melt_magic_discr ((melt_ptr_t)
						  ( /*_.INST__V45*/
						   meltfptr[44])) ==
				MELTOBMAG_OBJECT);
		melt_putfield_object (( /*_.INST__V45*/ meltfptr[44]), (3),
				      (( /*!konst_12_AUTOBOXLONG */
					meltfrout->tabval[12])),
				      "NCHUNK_OPER");
		;
		/*^putslot */
		/*putslot */
		melt_assertmsg ("putslot checkobj @NEXPR_CTYP",
				melt_magic_discr ((melt_ptr_t)
						  ( /*_.INST__V45*/
						   meltfptr[44])) ==
				MELTOBMAG_OBJECT);
		melt_putfield_object (( /*_.INST__V45*/ meltfptr[44]), (1),
				      (( /*!CTYPE_VALUE */ meltfrout->
					tabval[6])), "NEXPR_CTYP");
		;
		/*^touchobj */

		melt_dbgtrace_written_object ( /*_.INST__V45*/ meltfptr[44],
					      "newly made instance");
		;
		/*_.NCHK__V44*/ meltfptr[24] = /*_.INST__V45*/ meltfptr[44];;
		MELT_LOCATION ("warmelt-outobj.melt:6016:/ checksignal");
		MELT_CHECK_SIGNAL ();
		;
		/*^quasiblock */


		/*^rawallocobj */
		/*rawallocobj */
		{
		  melt_ptr_t newobj = 0;
		  melt_raw_object_create (newobj,
					  (melt_ptr_t) (( /*!CLASS_NREP_RETURN */ meltfrout->tabval[8])), (3), "CLASS_NREP_RETURN");
      /*_.INST__V47*/ meltfptr[46] =
		    newobj;
		};
		;
		/*^putslot */
		/*putslot */
		melt_assertmsg ("putslot checkobj @NRET_MAIN",
				melt_magic_discr ((melt_ptr_t)
						  ( /*_.INST__V47*/
						   meltfptr[46])) ==
				MELTOBMAG_OBJECT);
		melt_putfield_object (( /*_.INST__V47*/ meltfptr[46]), (1),
				      ( /*_.CLOCC__V21*/ meltfptr[20]),
				      "NRET_MAIN");
		;
		/*^touchobj */

		melt_dbgtrace_written_object ( /*_.INST__V47*/ meltfptr[46],
					      "newly made instance");
		;
		/*_.NRET__V46*/ meltfptr[45] = /*_.INST__V47*/ meltfptr[46];;
		MELT_LOCATION ("warmelt-outobj.melt:6020:/ checksignal");
		MELT_CHECK_SIGNAL ();
		;
		/*^cond */
		/*cond */ if (
			       /*ifisa */
			       melt_is_instance_of ((melt_ptr_t)
						    ( /*_.CBIND__V19*/
						     meltfptr[18]),
						    (melt_ptr_t) (( /*!CLASS_LET_BINDING */ meltfrout->tabval[13])))
		  )		/*then */
		  {
		    /*^cond.then */
		    /*^block */
		    /*anyblock */
		    {

		      /*^putslot */
		      /*putslot */
		      melt_assertmsg ("putslot checkobj @LETBIND_EXPR",
				      melt_magic_discr ((melt_ptr_t)
							( /*_.CBIND__V19*/
							 meltfptr[18])) ==
				      MELTOBMAG_OBJECT);
		      melt_putfield_object (( /*_.CBIND__V19*/ meltfptr[18]),
					    (2),
					    ( /*_.NCHK__V44*/ meltfptr[24]),
					    "LETBIND_EXPR");
		      ;
		      /*^touch */
		      meltgc_touch ( /*_.CBIND__V19*/ meltfptr[18]);
		      ;
		      /*^touchobj */

		      melt_dbgtrace_written_object ( /*_.CBIND__V19*/
						    meltfptr[18],
						    "put-fields");
		      ;
		      /*epilog */
		    }
		    ;
		  }		/*noelse */
		;

		{
		  MELT_LOCATION ("warmelt-outobj.melt:6021:/ locexp");
		  meltgc_append_list ((melt_ptr_t)
				      ( /*_.NBINDS__V3*/ meltfptr[2]),
				      (melt_ptr_t) ( /*_.CBIND__V19*/
						    meltfptr[18]));
		}
		;

#if MELT_HAVE_DEBUG
		MELT_LOCATION ("warmelt-outobj.melt:6022:/ cppif.then");
		/*^block */
		/*anyblock */
		{


		  {
		    /*^locexp */
		    /*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
		    melt_dbgcounter++;
#endif
		    ;
		  }
		  ;
		  /*^checksignal */
		  MELT_CHECK_SIGNAL ();
		  ;
       /*_#MELT_NEED_DBG__L13*/ meltfnum[9] =
		    /*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
		    ( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
		    0		/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
		    ;;
		  MELT_LOCATION ("warmelt-outobj.melt:6022:/ cond");
		  /*cond */ if ( /*_#MELT_NEED_DBG__L13*/ meltfnum[9])	/*then */
		    {
		      /*^cond.then */
		      /*^block */
		      /*anyblock */
		      {

	 /*_#THE_MELTCALLCOUNT__L14*/ meltfnum[0] =
#ifdef meltcallcount
			  meltcallcount	/* the_meltcallcount */
#else
			  0L
#endif /* meltcallcount the_meltcallcount */
			  ;;
			MELT_LOCATION
			  ("warmelt-outobj.melt:6022:/ checksignal");
			MELT_CHECK_SIGNAL ();
			;
			/*^apply */
			/*apply */
			{
			  union meltparam_un argtab[7];
			  memset (&argtab, 0, sizeof (argtab));
			  /*^apply.arg */
			  argtab[0].meltbp_long =
			    /*_#THE_MELTCALLCOUNT__L14*/ meltfnum[0];
			  /*^apply.arg */
			  argtab[1].meltbp_cstring = "warmelt-outobj.melt";
			  /*^apply.arg */
			  argtab[2].meltbp_long = 6022;
			  /*^apply.arg */
			  argtab[3].meltbp_cstring =
			    "autobox_normal_return long return nret=";
			  /*^apply.arg */
			  argtab[4].meltbp_aptr =
			    (melt_ptr_t *) & /*_.NRET__V46*/ meltfptr[45];
			  /*^apply.arg */
			  argtab[5].meltbp_cstring = " nbinds=";
			  /*^apply.arg */
			  argtab[6].meltbp_aptr =
			    (melt_ptr_t *) & /*_.NBINDS__V3*/ meltfptr[2];
			  /*_.MELT_DEBUG_FUN__V50*/ meltfptr[49] =
			    melt_apply ((meltclosure_ptr_t)
					(( /*!MELT_DEBUG_FUN */ meltfrout->
					  tabval[0])),
					(melt_ptr_t) (( /*nil */ NULL)),
					(MELTBPARSTR_LONG MELTBPARSTR_CSTRING
					 MELTBPARSTR_LONG MELTBPARSTR_CSTRING
					 MELTBPARSTR_PTR MELTBPARSTR_CSTRING
					 MELTBPARSTR_PTR ""), argtab, "",
					(union meltparam_un *) 0);
			}
			;
			/*_.IF___V49*/ meltfptr[48] =
			  /*_.MELT_DEBUG_FUN__V50*/ meltfptr[49];;
			/*epilog */

			MELT_LOCATION ("warmelt-outobj.melt:6022:/ clear");
		   /*clear *//*_#THE_MELTCALLCOUNT__L14*/ meltfnum[0] =
			  0;
			/*^clear */
		   /*clear *//*_.MELT_DEBUG_FUN__V50*/ meltfptr[49] = 0;
		      }
		      ;
		    }
		  else
		    {		/*^cond.else */

	/*_.IF___V49*/ meltfptr[48] = NULL;;
		    }
		  ;
		  MELT_LOCATION ("warmelt-outobj.melt:6022:/ quasiblock");


		  /*_.PROGN___V51*/ meltfptr[49] =
		    /*_.IF___V49*/ meltfptr[48];;
		  /*^compute */
		  /*_.IFCPP___V48*/ meltfptr[47] =
		    /*_.PROGN___V51*/ meltfptr[49];;
		  /*epilog */

		  MELT_LOCATION ("warmelt-outobj.melt:6022:/ clear");
		 /*clear *//*_#MELT_NEED_DBG__L13*/ meltfnum[9] = 0;
		  /*^clear */
		 /*clear *//*_.IF___V49*/ meltfptr[48] = 0;
		  /*^clear */
		 /*clear *//*_.PROGN___V51*/ meltfptr[49] = 0;
		}

#else /*MELT_HAVE_DEBUG */
		/*^cppif.else */
		/*_.IFCPP___V48*/ meltfptr[47] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
		;
		MELT_LOCATION ("warmelt-outobj.melt:6023:/ checksignal");
		MELT_CHECK_SIGNAL ();
		;
		/*^quasiblock */


		/*_.RETVAL___V1*/ meltfptr[0] = /*_.NRET__V46*/ meltfptr[45];;
		MELT_LOCATION ("warmelt-outobj.melt:6023:/ putxtraresult");
		if (!meltxrestab_ || !meltxresdescr_)
		  goto labend_rout;
		if (meltxresdescr_[0] != MELTBPAR_PTR)
		  goto labend_rout;
		if (meltxrestab_[0].meltbp_aptr)
		  *(meltxrestab_[0].meltbp_aptr) =
		    (melt_ptr_t) ( /*_.NBINDS__V3*/ meltfptr[2]);
		;
		/*^finalreturn */
		;
		/*finalret */ goto labend_rout;
		/*_.LET___V39*/ meltfptr[31] =
		  /*_.RETURN___V52*/ meltfptr[48];;

		MELT_LOCATION ("warmelt-outobj.melt:6002:/ clear");
	       /*clear *//*_.MAKE_STRINGCONST__V40*/ meltfptr[32] = 0;
		/*^clear */
	       /*clear *//*_.MAKE_STRINGCONST__V41*/ meltfptr[28] = 0;
		/*^clear */
	       /*clear *//*_.TUPLE___V42*/ meltfptr[33] = 0;
		/*^clear */
	       /*clear *//*_.NCHK__V44*/ meltfptr[24] = 0;
		/*^clear */
	       /*clear *//*_.NRET__V46*/ meltfptr[45] = 0;
		/*^clear */
	       /*clear *//*_.IFCPP___V48*/ meltfptr[47] = 0;
		/*^clear */
	       /*clear *//*_.RETURN___V52*/ meltfptr[48] = 0;
		/*_.IFELSE___V38*/ meltfptr[29] =
		  /*_.LET___V39*/ meltfptr[31];;
		/*epilog */

		MELT_LOCATION ("warmelt-outobj.melt:6001:/ clear");
	       /*clear *//*_.LET___V39*/ meltfptr[31] = 0;
	      }
	      ;
	    }
	  else
	    {			/*^cond.else */

	      /*^block */
	      /*anyblock */
	      {

		MELT_LOCATION ("warmelt-outobj.melt:6026:/ checksignal");
		MELT_CHECK_SIGNAL ();
		;
     /*_#__L15*/ meltfnum[0] =
		  (( /*_.CTYP__V4*/ meltfptr[3]) ==
		   (( /*!CTYPE_CSTRING */ meltfrout->tabval[14])));;
		MELT_LOCATION ("warmelt-outobj.melt:6026:/ cond");
		/*cond */ if ( /*_#__L15*/ meltfnum[0])	/*then */
		  {
		    /*^cond.then */
		    /*^block */
		    /*anyblock */
		    {

		      MELT_LOCATION ("warmelt-outobj.melt:6027:/ quasiblock");


		      /*^checksignal */
		      MELT_CHECK_SIGNAL ();
		      ;
       /*_.MAKE_STRINGCONST__V55*/ meltfptr[28] =
			(meltgc_new_stringdup
			 ((meltobject_ptr_t)
			  (( /*!DISCR_VERBATIM_STRING */ meltfrout->
			    tabval[11])),
			  ("/*autobox cstring*/ meltgc_new_stringdup((meltobject_ptr_t)MELT_PREDEF\
(DISCR_STRING), ")));;
		      /*^compute */
       /*_.MAKE_STRINGCONST__V56*/ meltfptr[33] =
			(meltgc_new_stringdup
			 ((meltobject_ptr_t)
			  (( /*!DISCR_VERBATIM_STRING */ meltfrout->
			    tabval[11])), (")")));;
		      MELT_LOCATION
			("warmelt-outobj.melt:6031:/ blockmultialloc");
		      /*multiallocblock */
		      {
			struct meltletrec_2_st
			{
			  struct MELT_MULTIPLE_STRUCT (3) rtup_0__TUPLREC__x6;
			  long meltletrec_2_endgap;
			} *meltletrec_2_ptr = 0;
			meltletrec_2_ptr =
			  (struct meltletrec_2_st *)
			  meltgc_allocate (sizeof (struct meltletrec_2_st),
					   0);
			/*^blockmultialloc.initfill */
			/*inimult rtup_0__TUPLREC__x6 */
 /*_.TUPLREC___V58*/ meltfptr[45] =
			  (melt_ptr_t) &
			  meltletrec_2_ptr->rtup_0__TUPLREC__x6;
			meltletrec_2_ptr->rtup_0__TUPLREC__x6.discr =
			  (meltobject_ptr_t) (((melt_ptr_t)
					       (MELT_PREDEF
						(DISCR_MULTIPLE))));
			meltletrec_2_ptr->rtup_0__TUPLREC__x6.nbval = 3;


			/*^putuple */
			/*putupl#14 */
			melt_assertmsg ("putupl [:6031] #14 checktup",
					melt_magic_discr ((melt_ptr_t)
							  ( /*_.TUPLREC___V58*/ meltfptr[45])) == MELTOBMAG_MULTIPLE);
			melt_assertmsg ("putupl [:6031] #14 checkoff",
					(0 >= 0
					 && 0 <
					 melt_multiple_length ((melt_ptr_t)
							       ( /*_.TUPLREC___V58*/ meltfptr[45]))));
			((meltmultiple_ptr_t)
			 ( /*_.TUPLREC___V58*/ meltfptr[45]))->tabval[0] =
     (melt_ptr_t) ( /*_.MAKE_STRINGCONST__V55*/ meltfptr[28]);
			;
			/*^putuple */
			/*putupl#15 */
			melt_assertmsg ("putupl [:6031] #15 checktup",
					melt_magic_discr ((melt_ptr_t)
							  ( /*_.TUPLREC___V58*/ meltfptr[45])) == MELTOBMAG_MULTIPLE);
			melt_assertmsg ("putupl [:6031] #15 checkoff",
					(1 >= 0
					 && 1 <
					 melt_multiple_length ((melt_ptr_t)
							       ( /*_.TUPLREC___V58*/ meltfptr[45]))));
			((meltmultiple_ptr_t)
			 ( /*_.TUPLREC___V58*/ meltfptr[45]))->tabval[1] =
     (melt_ptr_t) ( /*_.NEXP__V2*/ meltfptr[1]);
			;
			/*^putuple */
			/*putupl#16 */
			melt_assertmsg ("putupl [:6031] #16 checktup",
					melt_magic_discr ((melt_ptr_t)
							  ( /*_.TUPLREC___V58*/ meltfptr[45])) == MELTOBMAG_MULTIPLE);
			melt_assertmsg ("putupl [:6031] #16 checkoff",
					(2 >= 0
					 && 2 <
					 melt_multiple_length ((melt_ptr_t)
							       ( /*_.TUPLREC___V58*/ meltfptr[45]))));
			((meltmultiple_ptr_t)
			 ( /*_.TUPLREC___V58*/ meltfptr[45]))->tabval[2] =
     (melt_ptr_t) ( /*_.MAKE_STRINGCONST__V56*/ meltfptr[33]);
			;
			/*^touch */
			meltgc_touch ( /*_.TUPLREC___V58*/ meltfptr[45]);
			;
			/*_.TUPLE___V57*/ meltfptr[24] =
			  /*_.TUPLREC___V58*/ meltfptr[45];;
			/*epilog */

			MELT_LOCATION ("warmelt-outobj.melt:6031:/ clear");
		  /*clear *//*_.TUPLREC___V58*/ meltfptr[45] = 0;
			/*^clear */
		  /*clear *//*_.TUPLREC___V58*/ meltfptr[45] = 0;
		      }		/*end multiallocblock */
		      ;
		      MELT_LOCATION ("warmelt-outobj.melt:6027:/ quasiblock");


		      /*^rawallocobj */
		      /*rawallocobj */
		      {
			melt_ptr_t newobj = 0;
			melt_raw_object_create (newobj,
						(melt_ptr_t) (( /*!CLASS_NREP_CHUNK */ meltfrout->tabval[10])), (4), "CLASS_NREP_CHUNK");
	/*_.INST__V60*/ meltfptr[48] =
			  newobj;
		      };
		      ;
		      /*^putslot */
		      /*putslot */
		      melt_assertmsg ("putslot checkobj @NREP_LOC",
				      melt_magic_discr ((melt_ptr_t)
							( /*_.INST__V60*/
							 meltfptr[48])) ==
				      MELTOBMAG_OBJECT);
		      melt_putfield_object (( /*_.INST__V60*/ meltfptr[48]),
					    (0), (( /*nil */ NULL)),
					    "NREP_LOC");
		      ;
		      /*^putslot */
		      /*putslot */
		      melt_assertmsg ("putslot checkobj @NCHUNK_EXPANSION",
				      melt_magic_discr ((melt_ptr_t)
							( /*_.INST__V60*/
							 meltfptr[48])) ==
				      MELTOBMAG_OBJECT);
		      melt_putfield_object (( /*_.INST__V60*/ meltfptr[48]),
					    (2),
					    ( /*_.TUPLE___V57*/ meltfptr[24]),
					    "NCHUNK_EXPANSION");
		      ;
		      /*^putslot */
		      /*putslot */
		      melt_assertmsg ("putslot checkobj @NCHUNK_OPER",
				      melt_magic_discr ((melt_ptr_t)
							( /*_.INST__V60*/
							 meltfptr[48])) ==
				      MELTOBMAG_OBJECT);
		      melt_putfield_object (( /*_.INST__V60*/ meltfptr[48]),
					    (3),
					    (( /*!konst_15_AUTOBOXSTRING */
					      meltfrout->tabval[15])),
					    "NCHUNK_OPER");
		      ;
		      /*^putslot */
		      /*putslot */
		      melt_assertmsg ("putslot checkobj @NEXPR_CTYP",
				      melt_magic_discr ((melt_ptr_t)
							( /*_.INST__V60*/
							 meltfptr[48])) ==
				      MELTOBMAG_OBJECT);
		      melt_putfield_object (( /*_.INST__V60*/ meltfptr[48]),
					    (1),
					    (( /*!CTYPE_VALUE */ meltfrout->
					      tabval[6])), "NEXPR_CTYP");
		      ;
		      /*^touchobj */

		      melt_dbgtrace_written_object ( /*_.INST__V60*/
						    meltfptr[48],
						    "newly made instance");
		      ;
		      /*_.NCHK__V59*/ meltfptr[47] =
			/*_.INST__V60*/ meltfptr[48];;
		      MELT_LOCATION
			("warmelt-outobj.melt:6041:/ checksignal");
		      MELT_CHECK_SIGNAL ();
		      ;
		      /*^quasiblock */


		      /*^rawallocobj */
		      /*rawallocobj */
		      {
			melt_ptr_t newobj = 0;
			melt_raw_object_create (newobj,
						(melt_ptr_t) (( /*!CLASS_NREP_RETURN */ meltfrout->tabval[8])), (3), "CLASS_NREP_RETURN");
	/*_.INST__V62*/ meltfptr[45] =
			  newobj;
		      };
		      ;
		      /*^putslot */
		      /*putslot */
		      melt_assertmsg ("putslot checkobj @NRET_MAIN",
				      melt_magic_discr ((melt_ptr_t)
							( /*_.INST__V62*/
							 meltfptr[45])) ==
				      MELTOBMAG_OBJECT);
		      melt_putfield_object (( /*_.INST__V62*/ meltfptr[45]),
					    (1),
					    ( /*_.CLOCC__V21*/ meltfptr[20]),
					    "NRET_MAIN");
		      ;
		      /*^touchobj */

		      melt_dbgtrace_written_object ( /*_.INST__V62*/
						    meltfptr[45],
						    "newly made instance");
		      ;
		      /*_.NRET__V61*/ meltfptr[31] =
			/*_.INST__V62*/ meltfptr[45];;
		      MELT_LOCATION
			("warmelt-outobj.melt:6045:/ checksignal");
		      MELT_CHECK_SIGNAL ();
		      ;
		      /*^cond */
		      /*cond */ if (
				     /*ifisa */
				     melt_is_instance_of ((melt_ptr_t)
							  ( /*_.CBIND__V19*/
							   meltfptr[18]),
							  (melt_ptr_t) (( /*!CLASS_LET_BINDING */ meltfrout->tabval[13])))
			)	/*then */
			{
			  /*^cond.then */
			  /*^block */
			  /*anyblock */
			  {

			    /*^putslot */
			    /*putslot */
			    melt_assertmsg ("putslot checkobj @LETBIND_EXPR",
					    melt_magic_discr ((melt_ptr_t)
							      ( /*_.CBIND__V19*/ meltfptr[18])) == MELTOBMAG_OBJECT);
			    melt_putfield_object (( /*_.CBIND__V19*/
						   meltfptr[18]), (2),
						  ( /*_.NCHK__V59*/
						   meltfptr[47]),
						  "LETBIND_EXPR");
			    ;
			    /*^touch */
			    meltgc_touch ( /*_.CBIND__V19*/ meltfptr[18]);
			    ;
			    /*^touchobj */

			    melt_dbgtrace_written_object ( /*_.CBIND__V19*/
							  meltfptr[18],
							  "put-fields");
			    ;
			    /*epilog */
			  }
			  ;
			}	/*noelse */
		      ;

		      {
			MELT_LOCATION ("warmelt-outobj.melt:6046:/ locexp");
			meltgc_append_list ((melt_ptr_t)
					    ( /*_.NBINDS__V3*/ meltfptr[2]),
					    (melt_ptr_t) ( /*_.CBIND__V19*/
							  meltfptr[18]));
		      }
		      ;

#if MELT_HAVE_DEBUG
		      MELT_LOCATION ("warmelt-outobj.melt:6047:/ cppif.then");
		      /*^block */
		      /*anyblock */
		      {


			{
			  /*^locexp */
			  /*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
			  melt_dbgcounter++;
#endif
			  ;
			}
			;
			/*^checksignal */
			MELT_CHECK_SIGNAL ();
			;
	 /*_#MELT_NEED_DBG__L16*/ meltfnum[9] =
			  /*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
			  ( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
			  0	/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
			  ;;
			MELT_LOCATION ("warmelt-outobj.melt:6047:/ cond");
			/*cond */ if ( /*_#MELT_NEED_DBG__L16*/ meltfnum[9])	/*then */
			  {
			    /*^cond.then */
			    /*^block */
			    /*anyblock */
			    {

	   /*_#THE_MELTCALLCOUNT__L17*/ meltfnum[16] =
#ifdef meltcallcount
				meltcallcount	/* the_meltcallcount */
#else
				0L
#endif /* meltcallcount the_meltcallcount */
				;;
			      MELT_LOCATION
				("warmelt-outobj.melt:6047:/ checksignal");
			      MELT_CHECK_SIGNAL ();
			      ;
			      /*^apply */
			      /*apply */
			      {
				union meltparam_un argtab[7];
				memset (&argtab, 0, sizeof (argtab));
				/*^apply.arg */
				argtab[0].meltbp_long =
				  /*_#THE_MELTCALLCOUNT__L17*/ meltfnum[16];
				/*^apply.arg */
				argtab[1].meltbp_cstring =
				  "warmelt-outobj.melt";
				/*^apply.arg */
				argtab[2].meltbp_long = 6047;
				/*^apply.arg */
				argtab[3].meltbp_cstring =
				  "autobox_normal_return cstring return nret=";
				/*^apply.arg */
				argtab[4].meltbp_aptr =
				  (melt_ptr_t *) & /*_.NRET__V61*/
				  meltfptr[31];
				/*^apply.arg */
				argtab[5].meltbp_cstring = " nbinds=";
				/*^apply.arg */
				argtab[6].meltbp_aptr =
				  (melt_ptr_t *) & /*_.NBINDS__V3*/
				  meltfptr[2];
				/*_.MELT_DEBUG_FUN__V65*/ meltfptr[64] =
				  melt_apply ((meltclosure_ptr_t)
					      (( /*!MELT_DEBUG_FUN */
						meltfrout->tabval[0])),
					      (melt_ptr_t) (( /*nil */ NULL)),
					      (MELTBPARSTR_LONG
					       MELTBPARSTR_CSTRING
					       MELTBPARSTR_LONG
					       MELTBPARSTR_CSTRING
					       MELTBPARSTR_PTR
					       MELTBPARSTR_CSTRING
					       MELTBPARSTR_PTR ""), argtab,
					      "", (union meltparam_un *) 0);
			      }
			      ;
			      /*_.IF___V64*/ meltfptr[63] =
				/*_.MELT_DEBUG_FUN__V65*/ meltfptr[64];;
			      /*epilog */

			      MELT_LOCATION
				("warmelt-outobj.melt:6047:/ clear");
		     /*clear *//*_#THE_MELTCALLCOUNT__L17*/
				meltfnum[16] = 0;
			      /*^clear */
		     /*clear *//*_.MELT_DEBUG_FUN__V65*/ meltfptr[64]
				= 0;
			    }
			    ;
			  }
			else
			  {	/*^cond.else */

	  /*_.IF___V64*/ meltfptr[63] = NULL;;
			  }
			;
			MELT_LOCATION
			  ("warmelt-outobj.melt:6047:/ quasiblock");


			/*_.PROGN___V66*/ meltfptr[64] =
			  /*_.IF___V64*/ meltfptr[63];;
			/*^compute */
			/*_.IFCPP___V63*/ meltfptr[62] =
			  /*_.PROGN___V66*/ meltfptr[64];;
			/*epilog */

			MELT_LOCATION ("warmelt-outobj.melt:6047:/ clear");
		   /*clear *//*_#MELT_NEED_DBG__L16*/ meltfnum[9] = 0;
			/*^clear */
		   /*clear *//*_.IF___V64*/ meltfptr[63] = 0;
			/*^clear */
		   /*clear *//*_.PROGN___V66*/ meltfptr[64] = 0;
		      }

#else /*MELT_HAVE_DEBUG */
		      /*^cppif.else */
		      /*_.IFCPP___V63*/ meltfptr[62] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
		      ;
		      MELT_LOCATION
			("warmelt-outobj.melt:6048:/ checksignal");
		      MELT_CHECK_SIGNAL ();
		      ;
		      /*^quasiblock */


		      /*_.RETVAL___V1*/ meltfptr[0] =
			/*_.NRET__V61*/ meltfptr[31];;
		      MELT_LOCATION
			("warmelt-outobj.melt:6048:/ putxtraresult");
		      if (!meltxrestab_ || !meltxresdescr_)
			goto labend_rout;
		      if (meltxresdescr_[0] != MELTBPAR_PTR)
			goto labend_rout;
		      if (meltxrestab_[0].meltbp_aptr)
			*(meltxrestab_[0].meltbp_aptr) =
			  (melt_ptr_t) ( /*_.NBINDS__V3*/ meltfptr[2]);
		      ;
		      /*^finalreturn */
		      ;
		      /*finalret */ goto labend_rout;
		      /*_.LET___V54*/ meltfptr[32] =
			/*_.RETURN___V67*/ meltfptr[63];;

		      MELT_LOCATION ("warmelt-outobj.melt:6027:/ clear");
		 /*clear *//*_.MAKE_STRINGCONST__V55*/ meltfptr[28] = 0;
		      /*^clear */
		 /*clear *//*_.MAKE_STRINGCONST__V56*/ meltfptr[33] = 0;
		      /*^clear */
		 /*clear *//*_.TUPLE___V57*/ meltfptr[24] = 0;
		      /*^clear */
		 /*clear *//*_.NCHK__V59*/ meltfptr[47] = 0;
		      /*^clear */
		 /*clear *//*_.NRET__V61*/ meltfptr[31] = 0;
		      /*^clear */
		 /*clear *//*_.IFCPP___V63*/ meltfptr[62] = 0;
		      /*^clear */
		 /*clear *//*_.RETURN___V67*/ meltfptr[63] = 0;
		      /*_.IFELSE___V53*/ meltfptr[49] =
			/*_.LET___V54*/ meltfptr[32];;
		      /*epilog */

		      MELT_LOCATION ("warmelt-outobj.melt:6026:/ clear");
		 /*clear *//*_.LET___V54*/ meltfptr[32] = 0;
		    }
		    ;
		  }
		else
		  {		/*^cond.else */

		    /*^block */
		    /*anyblock */
		    {

		      MELT_LOCATION
			("warmelt-outobj.melt:6051:/ checksignal");
		      MELT_CHECK_SIGNAL ();
		      ;
       /*_#IS_A__L18*/ meltfnum[16] =
			melt_is_instance_of ((melt_ptr_t)
					     ( /*_.CTYP__V4*/ meltfptr[3]),
					     (melt_ptr_t) (( /*!CLASS_CTYPE_GTY */ meltfrout->tabval[16])));;
		      MELT_LOCATION ("warmelt-outobj.melt:6051:/ cond");
		      /*cond */ if ( /*_#IS_A__L18*/ meltfnum[16])	/*then */
			{
			  /*^cond.then */
			  /*^block */
			  /*anyblock */
			  {

			    MELT_LOCATION
			      ("warmelt-outobj.melt:6052:/ quasiblock");


			    /*^cond */
			    /*cond */ if (
					   /*ifisa */
					   melt_is_instance_of ((melt_ptr_t)
								( /*_.CTYP__V4*/ meltfptr[3]),
								(melt_ptr_t) (( /*!CLASS_CTYPE_GTY */ meltfrout->tabval[16])))
			      )	/*then */
			      {
				/*^cond.then */
				/*^getslot */
				{
				  melt_ptr_t slot = NULL, obj = NULL;
				  obj =
				    (melt_ptr_t) ( /*_.CTYP__V4*/ meltfptr[3])
				    /*=obj*/ ;
				  melt_object_get_field (slot, obj, 17,
							 "CTYPG_BOXDISCR");
	   /*_.DIS__V70*/ meltfptr[33] = slot;
				};
				;
			      }
			    else
			      {	/*^cond.else */

	  /*_.DIS__V70*/ meltfptr[33] = NULL;;
			      }
			    ;
			    MELT_LOCATION ("warmelt-outobj.melt:6053:/ cond");
			    /*cond */ if (
					   /*ifisa */
					   melt_is_instance_of ((melt_ptr_t)
								( /*_.CTYP__V4*/ meltfptr[3]),
								(melt_ptr_t) (( /*!CLASS_CTYPE_GTY */ meltfrout->tabval[16])))
			      )	/*then */
			      {
				/*^cond.then */
				/*^getslot */
				{
				  melt_ptr_t slot = NULL, obj = NULL;
				  obj =
				    (melt_ptr_t) ( /*_.CTYP__V4*/ meltfptr[3])
				    /*=obj*/ ;
				  melt_object_get_field (slot, obj, 20,
							 "CTYPG_BOXFUN");
	   /*_.BOXF__V71*/ meltfptr[24] = slot;
				};
				;
			      }
			    else
			      {	/*^cond.else */

	  /*_.BOXF__V71*/ meltfptr[24] = NULL;;
			      }
			    ;
			    MELT_LOCATION ("warmelt-outobj.melt:6054:/ cond");
			    /*cond */ if (
					   /*ifisa */
					   melt_is_instance_of ((melt_ptr_t)
								( /*_.CTYP__V4*/ meltfptr[3]),
								(melt_ptr_t) (( /*!CLASS_NAMED */ meltfrout->tabval[17])))
			      )	/*then */
			      {
				/*^cond.then */
				/*^getslot */
				{
				  melt_ptr_t slot = NULL, obj = NULL;
				  obj =
				    (melt_ptr_t) ( /*_.CTYP__V4*/ meltfptr[3])
				    /*=obj*/ ;
				  melt_object_get_field (slot, obj, 1,
							 "NAMED_NAME");
	   /*_.CTYNAM__V72*/ meltfptr[47] = slot;
				};
				;
			      }
			    else
			      {	/*^cond.else */

	  /*_.CTYNAM__V72*/ meltfptr[47] = NULL;;
			      }
			    ;
			    MELT_LOCATION
			      ("warmelt-outobj.melt:6055:/ checksignal");
			    MELT_CHECK_SIGNAL ();
			    ;
	 /*_.MAKE_STRINGCONST__V73*/ meltfptr[31] =
			      (meltgc_new_stringdup
			       ((meltobject_ptr_t)
				(( /*!DISCR_VERBATIM_STRING */ meltfrout->
				  tabval[11])), ("/*autobox-gty ")));;
			    /*^compute */
	 /*_.MAKE_STRING__V74*/ meltfptr[62] =
			      (meltgc_new_stringdup
			       ((meltobject_ptr_t)
				(( /*!DISCR_VERBATIM_STRING */ meltfrout->
				  tabval[11])),
				melt_string_str ((melt_ptr_t)
						 ( /*_.CTYNAM__V72*/
						  meltfptr[47]))));;
			    /*^compute */
	 /*_.MAKE_STRINGCONST__V75*/ meltfptr[63] =
			      (meltgc_new_stringdup
			       ((meltobject_ptr_t)
				(( /*!DISCR_VERBATIM_STRING */ meltfrout->
				  tabval[11])), ("*/ ")));;
			    /*^compute */
	 /*_.MAKE_STRING__V76*/ meltfptr[32] =
			      (meltgc_new_stringdup
			       ((meltobject_ptr_t)
				(( /*!DISCR_VERBATIM_STRING */ meltfrout->
				  tabval[11])),
				melt_string_str ((melt_ptr_t)
						 ( /*_.BOXF__V71*/
						  meltfptr[24]))));;
			    /*^compute */
	 /*_.MAKE_STRINGCONST__V77*/ meltfptr[76] =
			      (meltgc_new_stringdup
			       ((meltobject_ptr_t)
				(( /*!DISCR_VERBATIM_STRING */ meltfrout->
				  tabval[11])),
				(" ((meltobject_ptr_t)MELT_PREDEF (")));;
			    /*^compute */
	 /*_.MAKE_STRING__V78*/ meltfptr[77] =
			      (meltgc_new_stringdup
			       ((meltobject_ptr_t)
				(( /*!DISCR_VERBATIM_STRING */ meltfrout->
				  tabval[11])),
				melt_string_str ((melt_ptr_t)
						 ( /*_.DIS__V70*/
						  meltfptr[33]))));;
			    /*^compute */
	 /*_.MAKE_STRINGCONST__V79*/ meltfptr[78] =
			      (meltgc_new_stringdup
			       ((meltobject_ptr_t)
				(( /*!DISCR_VERBATIM_STRING */ meltfrout->
				  tabval[11])), ("), ")));;
			    /*^compute */
	 /*_.MAKE_STRINGCONST__V80*/ meltfptr[79] =
			      (meltgc_new_stringdup
			       ((meltobject_ptr_t)
				(( /*!DISCR_VERBATIM_STRING */ meltfrout->
				  tabval[11])), (")")));;
			    MELT_LOCATION
			      ("warmelt-outobj.melt:6059:/ blockmultialloc");
			    /*multiallocblock */
			    {
			      struct meltletrec_3_st
			      {
				struct MELT_MULTIPLE_STRUCT (9)
				  rtup_0__TUPLREC__x7;
				long meltletrec_3_endgap;
			      } *meltletrec_3_ptr = 0;
			      meltletrec_3_ptr =
				(struct meltletrec_3_st *)
				meltgc_allocate (sizeof
						 (struct meltletrec_3_st), 0);
			      /*^blockmultialloc.initfill */
			      /*inimult rtup_0__TUPLREC__x7 */
 /*_.TUPLREC___V82*/ meltfptr[81] =
				(melt_ptr_t) &
				meltletrec_3_ptr->rtup_0__TUPLREC__x7;
			      meltletrec_3_ptr->rtup_0__TUPLREC__x7.discr =
				(meltobject_ptr_t) (((melt_ptr_t)
						     (MELT_PREDEF
						      (DISCR_MULTIPLE))));
			      meltletrec_3_ptr->rtup_0__TUPLREC__x7.nbval = 9;


			      /*^putuple */
			      /*putupl#17 */
			      melt_assertmsg ("putupl [:6059] #17 checktup",
					      melt_magic_discr ((melt_ptr_t)
								( /*_.TUPLREC___V82*/ meltfptr[81])) == MELTOBMAG_MULTIPLE);
			      melt_assertmsg ("putupl [:6059] #17 checkoff",
					      (0 >= 0
					       && 0 <
					       melt_multiple_length ((melt_ptr_t) ( /*_.TUPLREC___V82*/ meltfptr[81]))));
			      ((meltmultiple_ptr_t)
			       ( /*_.TUPLREC___V82*/ meltfptr[81]))->
	   tabval[0] =
	   (melt_ptr_t) ( /*_.MAKE_STRINGCONST__V73*/ meltfptr[31]);
			      ;
			      /*^putuple */
			      /*putupl#18 */
			      melt_assertmsg ("putupl [:6059] #18 checktup",
					      melt_magic_discr ((melt_ptr_t)
								( /*_.TUPLREC___V82*/ meltfptr[81])) == MELTOBMAG_MULTIPLE);
			      melt_assertmsg ("putupl [:6059] #18 checkoff",
					      (1 >= 0
					       && 1 <
					       melt_multiple_length ((melt_ptr_t) ( /*_.TUPLREC___V82*/ meltfptr[81]))));
			      ((meltmultiple_ptr_t)
			       ( /*_.TUPLREC___V82*/ meltfptr[81]))->
	   tabval[1] = (melt_ptr_t) ( /*_.MAKE_STRING__V74*/ meltfptr[62]);
			      ;
			      /*^putuple */
			      /*putupl#19 */
			      melt_assertmsg ("putupl [:6059] #19 checktup",
					      melt_magic_discr ((melt_ptr_t)
								( /*_.TUPLREC___V82*/ meltfptr[81])) == MELTOBMAG_MULTIPLE);
			      melt_assertmsg ("putupl [:6059] #19 checkoff",
					      (2 >= 0
					       && 2 <
					       melt_multiple_length ((melt_ptr_t) ( /*_.TUPLREC___V82*/ meltfptr[81]))));
			      ((meltmultiple_ptr_t)
			       ( /*_.TUPLREC___V82*/ meltfptr[81]))->
	   tabval[2] =
	   (melt_ptr_t) ( /*_.MAKE_STRINGCONST__V75*/ meltfptr[63]);
			      ;
			      /*^putuple */
			      /*putupl#20 */
			      melt_assertmsg ("putupl [:6059] #20 checktup",
					      melt_magic_discr ((melt_ptr_t)
								( /*_.TUPLREC___V82*/ meltfptr[81])) == MELTOBMAG_MULTIPLE);
			      melt_assertmsg ("putupl [:6059] #20 checkoff",
					      (3 >= 0
					       && 3 <
					       melt_multiple_length ((melt_ptr_t) ( /*_.TUPLREC___V82*/ meltfptr[81]))));
			      ((meltmultiple_ptr_t)
			       ( /*_.TUPLREC___V82*/ meltfptr[81]))->
	   tabval[3] = (melt_ptr_t) ( /*_.MAKE_STRING__V76*/ meltfptr[32]);
			      ;
			      /*^putuple */
			      /*putupl#21 */
			      melt_assertmsg ("putupl [:6059] #21 checktup",
					      melt_magic_discr ((melt_ptr_t)
								( /*_.TUPLREC___V82*/ meltfptr[81])) == MELTOBMAG_MULTIPLE);
			      melt_assertmsg ("putupl [:6059] #21 checkoff",
					      (4 >= 0
					       && 4 <
					       melt_multiple_length ((melt_ptr_t) ( /*_.TUPLREC___V82*/ meltfptr[81]))));
			      ((meltmultiple_ptr_t)
			       ( /*_.TUPLREC___V82*/ meltfptr[81]))->
	   tabval[4] =
	   (melt_ptr_t) ( /*_.MAKE_STRINGCONST__V77*/ meltfptr[76]);
			      ;
			      /*^putuple */
			      /*putupl#22 */
			      melt_assertmsg ("putupl [:6059] #22 checktup",
					      melt_magic_discr ((melt_ptr_t)
								( /*_.TUPLREC___V82*/ meltfptr[81])) == MELTOBMAG_MULTIPLE);
			      melt_assertmsg ("putupl [:6059] #22 checkoff",
					      (5 >= 0
					       && 5 <
					       melt_multiple_length ((melt_ptr_t) ( /*_.TUPLREC___V82*/ meltfptr[81]))));
			      ((meltmultiple_ptr_t)
			       ( /*_.TUPLREC___V82*/ meltfptr[81]))->
	   tabval[5] = (melt_ptr_t) ( /*_.MAKE_STRING__V78*/ meltfptr[77]);
			      ;
			      /*^putuple */
			      /*putupl#23 */
			      melt_assertmsg ("putupl [:6059] #23 checktup",
					      melt_magic_discr ((melt_ptr_t)
								( /*_.TUPLREC___V82*/ meltfptr[81])) == MELTOBMAG_MULTIPLE);
			      melt_assertmsg ("putupl [:6059] #23 checkoff",
					      (6 >= 0
					       && 6 <
					       melt_multiple_length ((melt_ptr_t) ( /*_.TUPLREC___V82*/ meltfptr[81]))));
			      ((meltmultiple_ptr_t)
			       ( /*_.TUPLREC___V82*/ meltfptr[81]))->
	   tabval[6] =
	   (melt_ptr_t) ( /*_.MAKE_STRINGCONST__V79*/ meltfptr[78]);
			      ;
			      /*^putuple */
			      /*putupl#24 */
			      melt_assertmsg ("putupl [:6059] #24 checktup",
					      melt_magic_discr ((melt_ptr_t)
								( /*_.TUPLREC___V82*/ meltfptr[81])) == MELTOBMAG_MULTIPLE);
			      melt_assertmsg ("putupl [:6059] #24 checkoff",
					      (7 >= 0
					       && 7 <
					       melt_multiple_length ((melt_ptr_t) ( /*_.TUPLREC___V82*/ meltfptr[81]))));
			      ((meltmultiple_ptr_t)
			       ( /*_.TUPLREC___V82*/ meltfptr[81]))->
	   tabval[7] = (melt_ptr_t) ( /*_.NEXP__V2*/ meltfptr[1]);
			      ;
			      /*^putuple */
			      /*putupl#25 */
			      melt_assertmsg ("putupl [:6059] #25 checktup",
					      melt_magic_discr ((melt_ptr_t)
								( /*_.TUPLREC___V82*/ meltfptr[81])) == MELTOBMAG_MULTIPLE);
			      melt_assertmsg ("putupl [:6059] #25 checkoff",
					      (8 >= 0
					       && 8 <
					       melt_multiple_length ((melt_ptr_t) ( /*_.TUPLREC___V82*/ meltfptr[81]))));
			      ((meltmultiple_ptr_t)
			       ( /*_.TUPLREC___V82*/ meltfptr[81]))->
	   tabval[8] =
	   (melt_ptr_t) ( /*_.MAKE_STRINGCONST__V80*/ meltfptr[79]);
			      ;
			      /*^touch */
			      meltgc_touch ( /*_.TUPLREC___V82*/
					    meltfptr[81]);
			      ;
			      /*_.TUPLE___V81*/ meltfptr[80] =
				/*_.TUPLREC___V82*/ meltfptr[81];;
			      /*epilog */

			      MELT_LOCATION
				("warmelt-outobj.melt:6059:/ clear");
		    /*clear *//*_.TUPLREC___V82*/ meltfptr[81] = 0;
			      /*^clear */
		    /*clear *//*_.TUPLREC___V82*/ meltfptr[81] = 0;
			    }	/*end multiallocblock */
			    ;
			    MELT_LOCATION
			      ("warmelt-outobj.melt:6055:/ quasiblock");


			    /*^rawallocobj */
			    /*rawallocobj */
			    {
			      melt_ptr_t newobj = 0;
			      melt_raw_object_create (newobj,
						      (melt_ptr_t) (( /*!CLASS_NREP_CHUNK */ meltfrout->tabval[10])), (4), "CLASS_NREP_CHUNK");
	  /*_.INST__V84*/ meltfptr[83] =
				newobj;
			    };
			    ;
			    /*^putslot */
			    /*putslot */
			    melt_assertmsg ("putslot checkobj @NREP_LOC",
					    melt_magic_discr ((melt_ptr_t)
							      ( /*_.INST__V84*/ meltfptr[83])) == MELTOBMAG_OBJECT);
			    melt_putfield_object (( /*_.INST__V84*/
						   meltfptr[83]), (0),
						  (( /*nil */ NULL)),
						  "NREP_LOC");
			    ;
			    /*^putslot */
			    /*putslot */
			    melt_assertmsg
			      ("putslot checkobj @NCHUNK_EXPANSION",
			       melt_magic_discr ((melt_ptr_t)
						 ( /*_.INST__V84*/
						  meltfptr[83])) ==
			       MELTOBMAG_OBJECT);
			    melt_putfield_object (( /*_.INST__V84*/
						   meltfptr[83]), (2),
						  ( /*_.TUPLE___V81*/
						   meltfptr[80]),
						  "NCHUNK_EXPANSION");
			    ;
			    /*^putslot */
			    /*putslot */
			    melt_assertmsg ("putslot checkobj @NCHUNK_OPER",
					    melt_magic_discr ((melt_ptr_t)
							      ( /*_.INST__V84*/ meltfptr[83])) == MELTOBMAG_OBJECT);
			    melt_putfield_object (( /*_.INST__V84*/
						   meltfptr[83]), (3),
						  (( /*!konst_18_AUTOBOXGTY */
						    meltfrout->tabval[18])),
						  "NCHUNK_OPER");
			    ;
			    /*^putslot */
			    /*putslot */
			    melt_assertmsg ("putslot checkobj @NEXPR_CTYP",
					    melt_magic_discr ((melt_ptr_t)
							      ( /*_.INST__V84*/ meltfptr[83])) == MELTOBMAG_OBJECT);
			    melt_putfield_object (( /*_.INST__V84*/
						   meltfptr[83]), (1),
						  (( /*!CTYPE_VALUE */
						    meltfrout->tabval[6])),
						  "NEXPR_CTYP");
			    ;
			    /*^touchobj */

			    melt_dbgtrace_written_object ( /*_.INST__V84*/
							  meltfptr[83],
							  "newly made instance");
			    ;
			    /*_.NCHK__V83*/ meltfptr[81] =
			      /*_.INST__V84*/ meltfptr[83];;
			    MELT_LOCATION
			      ("warmelt-outobj.melt:6081:/ checksignal");
			    MELT_CHECK_SIGNAL ();
			    ;
			    /*^quasiblock */


			    /*^rawallocobj */
			    /*rawallocobj */
			    {
			      melt_ptr_t newobj = 0;
			      melt_raw_object_create (newobj,
						      (melt_ptr_t) (( /*!CLASS_NREP_RETURN */ meltfrout->tabval[8])), (3), "CLASS_NREP_RETURN");
	  /*_.INST__V86*/ meltfptr[85] =
				newobj;
			    };
			    ;
			    /*^putslot */
			    /*putslot */
			    melt_assertmsg ("putslot checkobj @NRET_MAIN",
					    melt_magic_discr ((melt_ptr_t)
							      ( /*_.INST__V86*/ meltfptr[85])) == MELTOBMAG_OBJECT);
			    melt_putfield_object (( /*_.INST__V86*/
						   meltfptr[85]), (1),
						  ( /*_.CLOCC__V21*/
						   meltfptr[20]),
						  "NRET_MAIN");
			    ;
			    /*^touchobj */

			    melt_dbgtrace_written_object ( /*_.INST__V86*/
							  meltfptr[85],
							  "newly made instance");
			    ;
			    /*_.NRET__V85*/ meltfptr[84] =
			      /*_.INST__V86*/ meltfptr[85];;
			    MELT_LOCATION
			      ("warmelt-outobj.melt:6085:/ checksignal");
			    MELT_CHECK_SIGNAL ();
			    ;
			    /*^cond */
			    /*cond */ if (
					   /*ifisa */
					   melt_is_instance_of ((melt_ptr_t)
								( /*_.CBIND__V19*/ meltfptr[18]),
								(melt_ptr_t) (( /*!CLASS_LET_BINDING */ meltfrout->tabval[13])))
			      )	/*then */
			      {
				/*^cond.then */
				/*^block */
				/*anyblock */
				{

				  /*^putslot */
				  /*putslot */
				  melt_assertmsg
				    ("putslot checkobj @LETBIND_EXPR",
				     melt_magic_discr ((melt_ptr_t)
						       ( /*_.CBIND__V19*/
							meltfptr[18])) ==
				     MELTOBMAG_OBJECT);
				  melt_putfield_object (( /*_.CBIND__V19*/
							 meltfptr[18]), (2),
							( /*_.NCHK__V83*/
							 meltfptr[81]),
							"LETBIND_EXPR");
				  ;
				  /*^touch */
				  meltgc_touch ( /*_.CBIND__V19*/
						meltfptr[18]);
				  ;
				  /*^touchobj */

				  melt_dbgtrace_written_object ( /*_.CBIND__V19*/ meltfptr[18], "put-fields");
				  ;
				  /*epilog */
				}
				;
			      }	/*noelse */
			    ;

			    {
			      MELT_LOCATION
				("warmelt-outobj.melt:6086:/ locexp");
			      meltgc_append_list ((melt_ptr_t)
						  ( /*_.NBINDS__V3*/
						   meltfptr[2]),
						  (melt_ptr_t) ( /*_.CBIND__V19*/ meltfptr[18]));
			    }
			    ;

#if MELT_HAVE_DEBUG
			    MELT_LOCATION
			      ("warmelt-outobj.melt:6087:/ cppif.then");
			    /*^block */
			    /*anyblock */
			    {


			      {
				/*^locexp */
				/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
				melt_dbgcounter++;
#endif
				;
			      }
			      ;
			      /*^checksignal */
			      MELT_CHECK_SIGNAL ();
			      ;
	   /*_#MELT_NEED_DBG__L19*/ meltfnum[9] =
				/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
				( /*melt_need_dbg */
				 melt_need_debug ((int) 0))
#else
				0	/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
				;;
			      MELT_LOCATION
				("warmelt-outobj.melt:6087:/ cond");
			      /*cond */ if ( /*_#MELT_NEED_DBG__L19*/ meltfnum[9])	/*then */
				{
				  /*^cond.then */
				  /*^block */
				  /*anyblock */
				  {

	     /*_#THE_MELTCALLCOUNT__L20*/ meltfnum[19]
				      =
#ifdef meltcallcount
				      meltcallcount	/* the_meltcallcount */
#else
				      0L
#endif /* meltcallcount the_meltcallcount */
				      ;;
				    MELT_LOCATION
				      ("warmelt-outobj.melt:6087:/ checksignal");
				    MELT_CHECK_SIGNAL ();
				    ;
				    /*^apply */
				    /*apply */
				    {
				      union meltparam_un argtab[7];
				      memset (&argtab, 0, sizeof (argtab));
				      /*^apply.arg */
				      argtab[0].meltbp_long =
					/*_#THE_MELTCALLCOUNT__L20*/
					meltfnum[19];
				      /*^apply.arg */
				      argtab[1].meltbp_cstring =
					"warmelt-outobj.melt";
				      /*^apply.arg */
				      argtab[2].meltbp_long = 6087;
				      /*^apply.arg */
				      argtab[3].meltbp_cstring =
					"autobox_normal_return gtyctype return nret=";
				      /*^apply.arg */
				      argtab[4].meltbp_aptr =
					(melt_ptr_t *) & /*_.NRET__V85*/
					meltfptr[84];
				      /*^apply.arg */
				      argtab[5].meltbp_cstring = " nbinds=";
				      /*^apply.arg */
				      argtab[6].meltbp_aptr =
					(melt_ptr_t *) & /*_.NBINDS__V3*/
					meltfptr[2];
				      /*_.MELT_DEBUG_FUN__V89*/ meltfptr[88] =
					melt_apply ((meltclosure_ptr_t)
						    (( /*!MELT_DEBUG_FUN */
						      meltfrout->tabval[0])),
						    (melt_ptr_t) (( /*nil */
								   NULL)),
						    (MELTBPARSTR_LONG
						     MELTBPARSTR_CSTRING
						     MELTBPARSTR_LONG
						     MELTBPARSTR_CSTRING
						     MELTBPARSTR_PTR
						     MELTBPARSTR_CSTRING
						     MELTBPARSTR_PTR ""),
						    argtab, "",
						    (union meltparam_un *) 0);
				    }
				    ;
				    /*_.IF___V88*/ meltfptr[87] =
				      /*_.MELT_DEBUG_FUN__V89*/ meltfptr[88];;
				    /*epilog */

				    MELT_LOCATION
				      ("warmelt-outobj.melt:6087:/ clear");
		       /*clear *//*_#THE_MELTCALLCOUNT__L20*/
				      meltfnum[19] = 0;
				    /*^clear */
		       /*clear *//*_.MELT_DEBUG_FUN__V89*/
				      meltfptr[88] = 0;
				  }
				  ;
				}
			      else
				{	/*^cond.else */

	    /*_.IF___V88*/ meltfptr[87] = NULL;;
				}
			      ;
			      MELT_LOCATION
				("warmelt-outobj.melt:6087:/ quasiblock");


			      /*_.PROGN___V90*/ meltfptr[88] =
				/*_.IF___V88*/ meltfptr[87];;
			      /*^compute */
			      /*_.IFCPP___V87*/ meltfptr[86] =
				/*_.PROGN___V90*/ meltfptr[88];;
			      /*epilog */

			      MELT_LOCATION
				("warmelt-outobj.melt:6087:/ clear");
		     /*clear *//*_#MELT_NEED_DBG__L19*/ meltfnum[9] =
				0;
			      /*^clear */
		     /*clear *//*_.IF___V88*/ meltfptr[87] = 0;
			      /*^clear */
		     /*clear *//*_.PROGN___V90*/ meltfptr[88] = 0;
			    }

#else /*MELT_HAVE_DEBUG */
			    /*^cppif.else */
			    /*_.IFCPP___V87*/ meltfptr[86] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
			    ;
			    MELT_LOCATION
			      ("warmelt-outobj.melt:6088:/ checksignal");
			    MELT_CHECK_SIGNAL ();
			    ;
			    /*^quasiblock */


			    /*_.RETVAL___V1*/ meltfptr[0] =
			      /*_.NRET__V85*/ meltfptr[84];;
			    MELT_LOCATION
			      ("warmelt-outobj.melt:6088:/ putxtraresult");
			    if (!meltxrestab_ || !meltxresdescr_)
			      goto labend_rout;
			    if (meltxresdescr_[0] != MELTBPAR_PTR)
			      goto labend_rout;
			    if (meltxrestab_[0].meltbp_aptr)
			      *(meltxrestab_[0].meltbp_aptr) =
				(melt_ptr_t) ( /*_.NBINDS__V3*/ meltfptr[2]);
			    ;
			    /*^finalreturn */
			    ;
			    /*finalret */ goto labend_rout;
			    /*_.LET___V69*/ meltfptr[28] =
			      /*_.RETURN___V91*/ meltfptr[87];;

			    MELT_LOCATION
			      ("warmelt-outobj.melt:6052:/ clear");
		   /*clear *//*_.DIS__V70*/ meltfptr[33] = 0;
			    /*^clear */
		   /*clear *//*_.BOXF__V71*/ meltfptr[24] = 0;
			    /*^clear */
		   /*clear *//*_.CTYNAM__V72*/ meltfptr[47] = 0;
			    /*^clear */
		   /*clear *//*_.MAKE_STRINGCONST__V73*/ meltfptr[31]
			      = 0;
			    /*^clear */
		   /*clear *//*_.MAKE_STRING__V74*/ meltfptr[62] = 0;
			    /*^clear */
		   /*clear *//*_.MAKE_STRINGCONST__V75*/ meltfptr[63]
			      = 0;
			    /*^clear */
		   /*clear *//*_.MAKE_STRING__V76*/ meltfptr[32] = 0;
			    /*^clear */
		   /*clear *//*_.MAKE_STRINGCONST__V77*/ meltfptr[76]
			      = 0;
			    /*^clear */
		   /*clear *//*_.MAKE_STRING__V78*/ meltfptr[77] = 0;
			    /*^clear */
		   /*clear *//*_.MAKE_STRINGCONST__V79*/ meltfptr[78]
			      = 0;
			    /*^clear */
		   /*clear *//*_.MAKE_STRINGCONST__V80*/ meltfptr[79]
			      = 0;
			    /*^clear */
		   /*clear *//*_.TUPLE___V81*/ meltfptr[80] = 0;
			    /*^clear */
		   /*clear *//*_.NCHK__V83*/ meltfptr[81] = 0;
			    /*^clear */
		   /*clear *//*_.NRET__V85*/ meltfptr[84] = 0;
			    /*^clear */
		   /*clear *//*_.IFCPP___V87*/ meltfptr[86] = 0;
			    /*^clear */
		   /*clear *//*_.RETURN___V91*/ meltfptr[87] = 0;
			    /*_.IFELSE___V68*/ meltfptr[64] =
			      /*_.LET___V69*/ meltfptr[28];;
			    /*epilog */

			    MELT_LOCATION
			      ("warmelt-outobj.melt:6051:/ clear");
		   /*clear *//*_.LET___V69*/ meltfptr[28] = 0;
			  }
			  ;
			}
		      else
			{	/*^cond.else */

			  /*^block */
			  /*anyblock */
			  {


#if MELT_HAVE_DEBUG
			    MELT_LOCATION
			      ("warmelt-outobj.melt:6092:/ cppif.then");
			    /*^block */
			    /*anyblock */
			    {


			      {
				/*^locexp */
				/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
				melt_dbgcounter++;
#endif
				;
			      }
			      ;
			      /*^checksignal */
			      MELT_CHECK_SIGNAL ();
			      ;
	   /*_#MELT_NEED_DBG__L21*/ meltfnum[19] =
				/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
				( /*melt_need_dbg */
				 melt_need_debug ((int) 0))
#else
				0	/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
				;;
			      MELT_LOCATION
				("warmelt-outobj.melt:6092:/ cond");
			      /*cond */ if ( /*_#MELT_NEED_DBG__L21*/ meltfnum[19])	/*then */
				{
				  /*^cond.then */
				  /*^block */
				  /*anyblock */
				  {

	     /*_#THE_MELTCALLCOUNT__L22*/ meltfnum[9] =
#ifdef meltcallcount
				      meltcallcount	/* the_meltcallcount */
#else
				      0L
#endif /* meltcallcount the_meltcallcount */
				      ;;
				    /*^compute */
	     /*_.DISCRIM__V94*/ meltfptr[24] =
				      ((melt_ptr_t)
				       (melt_discr
					((melt_ptr_t)
					 ( /*_.CTYP__V4*/ meltfptr[3]))));;
				    MELT_LOCATION
				      ("warmelt-outobj.melt:6092:/ checksignal");
				    MELT_CHECK_SIGNAL ();
				    ;
				    /*^apply */
				    /*apply */
				    {
				      union meltparam_un argtab[7];
				      memset (&argtab, 0, sizeof (argtab));
				      /*^apply.arg */
				      argtab[0].meltbp_long =
					/*_#THE_MELTCALLCOUNT__L22*/
					meltfnum[9];
				      /*^apply.arg */
				      argtab[1].meltbp_cstring =
					"warmelt-outobj.melt";
				      /*^apply.arg */
				      argtab[2].meltbp_long = 6092;
				      /*^apply.arg */
				      argtab[3].meltbp_cstring =
					"autobox_normal_return unexpected ctyp=";
				      /*^apply.arg */
				      argtab[4].meltbp_aptr =
					(melt_ptr_t *) & /*_.CTYP__V4*/
					meltfptr[3];
				      /*^apply.arg */
				      argtab[5].meltbp_cstring =
					"\n of discrim ";
				      /*^apply.arg */
				      argtab[6].meltbp_aptr =
					(melt_ptr_t *) & /*_.DISCRIM__V94*/
					meltfptr[24];
				      /*_.MELT_DEBUG_FUN__V95*/ meltfptr[47] =
					melt_apply ((meltclosure_ptr_t)
						    (( /*!MELT_DEBUG_FUN */
						      meltfrout->tabval[0])),
						    (melt_ptr_t) (( /*nil */
								   NULL)),
						    (MELTBPARSTR_LONG
						     MELTBPARSTR_CSTRING
						     MELTBPARSTR_LONG
						     MELTBPARSTR_CSTRING
						     MELTBPARSTR_PTR
						     MELTBPARSTR_CSTRING
						     MELTBPARSTR_PTR ""),
						    argtab, "",
						    (union meltparam_un *) 0);
				    }
				    ;
				    /*_.IF___V93*/ meltfptr[33] =
				      /*_.MELT_DEBUG_FUN__V95*/ meltfptr[47];;
				    /*epilog */

				    MELT_LOCATION
				      ("warmelt-outobj.melt:6092:/ clear");
		       /*clear *//*_#THE_MELTCALLCOUNT__L22*/
				      meltfnum[9] = 0;
				    /*^clear */
		       /*clear *//*_.DISCRIM__V94*/ meltfptr[24]
				      = 0;
				    /*^clear */
		       /*clear *//*_.MELT_DEBUG_FUN__V95*/
				      meltfptr[47] = 0;
				  }
				  ;
				}
			      else
				{	/*^cond.else */

	    /*_.IF___V93*/ meltfptr[33] = NULL;;
				}
			      ;
			      MELT_LOCATION
				("warmelt-outobj.melt:6092:/ quasiblock");


			      /*_.PROGN___V96*/ meltfptr[31] =
				/*_.IF___V93*/ meltfptr[33];;
			      /*^compute */
			      /*_.IFCPP___V92*/ meltfptr[88] =
				/*_.PROGN___V96*/ meltfptr[31];;
			      /*epilog */

			      MELT_LOCATION
				("warmelt-outobj.melt:6092:/ clear");
		     /*clear *//*_#MELT_NEED_DBG__L21*/ meltfnum[19]
				= 0;
			      /*^clear */
		     /*clear *//*_.IF___V93*/ meltfptr[33] = 0;
			      /*^clear */
		     /*clear *//*_.PROGN___V96*/ meltfptr[31] = 0;
			    }

#else /*MELT_HAVE_DEBUG */
			    /*^cppif.else */
			    /*_.IFCPP___V92*/ meltfptr[88] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
			    ;

#if MELT_HAVE_DEBUG
			    MELT_LOCATION
			      ("warmelt-outobj.melt:6093:/ cppif.then");
			    /*^block */
			    /*anyblock */
			    {

			      /*^checksignal */
			      MELT_CHECK_SIGNAL ();
			      ;
			      /*^cond */
			      /*cond */ if (( /*nil */ NULL))	/*then */
				{
				  /*^cond.then */
				  /*_.IFELSE___V98*/ meltfptr[63] =
				    ( /*nil */ NULL);;
				}
			      else
				{
				  MELT_LOCATION
				    ("warmelt-outobj.melt:6093:/ cond.else");

				  /*^block */
				  /*anyblock */
				  {




				    {
				      /*^locexp */
				      melt_assert_failed (("autobox_normal_return unexpected ctype"), ("warmelt-outobj.melt") ? ("warmelt-outobj.melt") : __FILE__, (6093) ? (6093) : __LINE__, __FUNCTION__);
				      ;
				    }
				    ;
		       /*clear *//*_.IFELSE___V98*/ meltfptr[63]
				      = 0;
				    /*epilog */
				  }
				  ;
				}
			      ;
			      /*^compute */
			      /*_.IFCPP___V97*/ meltfptr[62] =
				/*_.IFELSE___V98*/ meltfptr[63];;
			      /*epilog */

			      MELT_LOCATION
				("warmelt-outobj.melt:6093:/ clear");
		     /*clear *//*_.IFELSE___V98*/ meltfptr[63] = 0;
			    }

#else /*MELT_HAVE_DEBUG */
			    /*^cppif.else */
			    /*_.IFCPP___V97*/ meltfptr[62] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
			    ;
			    MELT_LOCATION
			      ("warmelt-outobj.melt:6091:/ quasiblock");


			    /*_.PROGN___V99*/ meltfptr[32] =
			      /*_.IFCPP___V97*/ meltfptr[62];;
			    /*^compute */
			    /*_.IFELSE___V68*/ meltfptr[64] =
			      /*_.PROGN___V99*/ meltfptr[32];;
			    /*epilog */

			    MELT_LOCATION
			      ("warmelt-outobj.melt:6051:/ clear");
		   /*clear *//*_.IFCPP___V92*/ meltfptr[88] = 0;
			    /*^clear */
		   /*clear *//*_.IFCPP___V97*/ meltfptr[62] = 0;
			    /*^clear */
		   /*clear *//*_.PROGN___V99*/ meltfptr[32] = 0;
			  }
			  ;
			}
		      ;
		      /*_.IFELSE___V53*/ meltfptr[49] =
			/*_.IFELSE___V68*/ meltfptr[64];;
		      /*epilog */

		      MELT_LOCATION ("warmelt-outobj.melt:6026:/ clear");
		 /*clear *//*_#IS_A__L18*/ meltfnum[16] = 0;
		      /*^clear */
		 /*clear *//*_.IFELSE___V68*/ meltfptr[64] = 0;
		    }
		    ;
		  }
		;
		/*_.IFELSE___V38*/ meltfptr[29] =
		  /*_.IFELSE___V53*/ meltfptr[49];;
		/*epilog */

		MELT_LOCATION ("warmelt-outobj.melt:6001:/ clear");
	       /*clear *//*_#__L15*/ meltfnum[0] = 0;
		/*^clear */
	       /*clear *//*_.IFELSE___V53*/ meltfptr[49] = 0;
	      }
	      ;
	    }
	  ;
	  /*_.IFELSE___V27*/ meltfptr[23] = /*_.IFELSE___V38*/ meltfptr[29];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-outobj.melt:5991:/ clear");
	     /*clear *//*_#__L12*/ meltfnum[10] = 0;
	  /*^clear */
	     /*clear *//*_.IFELSE___V38*/ meltfptr[29] = 0;
	}
	;
      }
    ;
    /*_.LET___V17*/ meltfptr[15] = /*_.IFELSE___V27*/ meltfptr[23];;

    MELT_LOCATION ("warmelt-outobj.melt:5976:/ clear");
	   /*clear *//*_.CSYM__V18*/ meltfptr[17] = 0;
    /*^clear */
	   /*clear *//*_.CBIND__V19*/ meltfptr[18] = 0;
    /*^clear */
	   /*clear *//*_.CLOCC__V21*/ meltfptr[20] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V23*/ meltfptr[22] = 0;
    /*^clear */
	   /*clear *//*_#__L8*/ meltfnum[6] = 0;
    /*^clear */
	   /*clear *//*_.IFELSE___V27*/ meltfptr[23] = 0;
    MELT_LOCATION ("warmelt-outobj.melt:5971:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.LET___V17*/ meltfptr[15];;

    {
      MELT_LOCATION ("warmelt-outobj.melt:5971:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V6*/ meltfptr[5] = 0;
    /*^clear */
	   /*clear *//*_#NULL__L3*/ meltfnum[1] = 0;
    /*^clear */
	   /*clear *//*_.IF___V10*/ meltfptr[6] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V13*/ meltfptr[7] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V15*/ meltfptr[11] = 0;
    /*^clear */
	   /*clear *//*_.LET___V17*/ meltfptr[15] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("AUTOBOX_NORMAL_RETURN", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_138_warmelt_outobj_AUTOBOX_NORMAL_RETURN_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_138_warmelt_outobj_AUTOBOX_NORMAL_RETURN */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_139_warmelt_outobj_TRANSLATE_RUN_MELT_EXPRESSIONS (meltclosure_ptr_t
							    meltclosp_,
							    melt_ptr_t
							    meltfirstargp_,
							    const
							    melt_argdescr_cell_t
							    meltxargdescr_[],
							    union meltparam_un
							    * meltxargtab_,
							    const
							    melt_argdescr_cell_t
							    meltxresdescr_[],
							    union meltparam_un
							    * meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_139_warmelt_outobj_TRANSLATE_RUN_MELT_EXPRESSIONS_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_139_warmelt_outobj_TRANSLATE_RUN_MELT_EXPRESSIONS_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 154
    melt_ptr_t mcfr_varptr[154];
#define MELTFRAM_NBVARNUM 64
    long mcfr_varnum[64];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_139_warmelt_outobj_TRANSLATE_RUN_MELT_EXPRESSIONS is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct
	 frame_meltrout_139_warmelt_outobj_TRANSLATE_RUN_MELT_EXPRESSIONS_st
	 *) meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 154; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_139_warmelt_outobj_TRANSLATE_RUN_MELT_EXPRESSIONS nbval 154*/
  meltfram__.mcfr_nbvar = 154 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("TRANSLATE_RUN_MELT_EXPRESSIONS", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-outobj.melt:6098:/ getarg");
 /*_.EXPRS__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.ENV__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.ENV__V3*/ meltfptr[2])) != NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:6103:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L1*/ meltfnum[0] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-outobj.melt:6103:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L1*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-outobj.melt:6103:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[7];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-outobj.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 6103;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring =
		"translate_run_melt_expressions start exprs=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.EXPRS__V2*/ meltfptr[1];
	      /*^apply.arg */
	      argtab[5].meltbp_cstring = "\n*env=";
	      /*^apply.arg */
	      argtab[6].meltbp_aptr =
		(melt_ptr_t *) & /*_.ENV__V3*/ meltfptr[2];
	      /*_.MELT_DEBUG_FUN__V6*/ meltfptr[5] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V5*/ meltfptr[4] = /*_.MELT_DEBUG_FUN__V6*/ meltfptr[5];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-outobj.melt:6103:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V6*/ meltfptr[5] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V5*/ meltfptr[4] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-outobj.melt:6103:/ quasiblock");


      /*_.PROGN___V7*/ meltfptr[5] = /*_.IF___V5*/ meltfptr[4];;
      /*^compute */
      /*_.IFCPP___V4*/ meltfptr[3] = /*_.PROGN___V7*/ meltfptr[5];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:6103:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L1*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IF___V5*/ meltfptr[4] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V7*/ meltfptr[5] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V4*/ meltfptr[3] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:6104:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L3*/ meltfnum[1] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.ENV__V3*/ meltfptr[2]),
			     (melt_ptr_t) (( /*!CLASS_ENVIRONMENT */
					    meltfrout->tabval[1])));;
      MELT_LOCATION ("warmelt-outobj.melt:6104:/ cond");
      /*cond */ if ( /*_#IS_A__L3*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V9*/ meltfptr[5] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-outobj.melt:6104:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check env"),
				  ("warmelt-outobj.melt")
				  ? ("warmelt-outobj.melt") : __FILE__,
				  (6104) ? (6104) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V9*/ meltfptr[5] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V8*/ meltfptr[4] = /*_.IFELSE___V9*/ meltfptr[5];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:6104:/ clear");
	     /*clear *//*_#IS_A__L3*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V9*/ meltfptr[5] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V8*/ meltfptr[4] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-outobj.melt:6105:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#IS_LIST__L4*/ meltfnum[0] =
      (melt_magic_discr ((melt_ptr_t) ( /*_.EXPRS__V2*/ meltfptr[1])) ==
       MELTOBMAG_LIST);;
    /*^compute */
 /*_#NOT__L5*/ meltfnum[1] =
      (!( /*_#IS_LIST__L4*/ meltfnum[0]));;
    MELT_LOCATION ("warmelt-outobj.melt:6105:/ cond");
    /*cond */ if ( /*_#NOT__L5*/ meltfnum[1])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{


#if MELT_HAVE_DEBUG
	  MELT_LOCATION ("warmelt-outobj.melt:6106:/ cppif.then");
	  /*^block */
	  /*anyblock */
	  {


	    {
	      /*^locexp */
	      /*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	      melt_dbgcounter++;
#endif
	      ;
	    }
	    ;
	    /*^checksignal */
	    MELT_CHECK_SIGNAL ();
	    ;
     /*_#MELT_NEED_DBG__L6*/ meltfnum[5] =
	      /*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	      ( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	      0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	      ;;
	    MELT_LOCATION ("warmelt-outobj.melt:6106:/ cond");
	    /*cond */ if ( /*_#MELT_NEED_DBG__L6*/ meltfnum[5])	/*then */
	      {
		/*^cond.then */
		/*^block */
		/*anyblock */
		{

       /*_#THE_MELTCALLCOUNT__L7*/ meltfnum[6] =
#ifdef meltcallcount
		    meltcallcount	/* the_meltcallcount */
#else
		    0L
#endif /* meltcallcount the_meltcallcount */
		    ;;
		  MELT_LOCATION ("warmelt-outobj.melt:6106:/ checksignal");
		  MELT_CHECK_SIGNAL ();
		  ;
		  /*^apply */
		  /*apply */
		  {
		    union meltparam_un argtab[6];
		    memset (&argtab, 0, sizeof (argtab));
		    /*^apply.arg */
		    argtab[0].meltbp_long =
		      /*_#THE_MELTCALLCOUNT__L7*/ meltfnum[6];
		    /*^apply.arg */
		    argtab[1].meltbp_cstring = "warmelt-outobj.melt";
		    /*^apply.arg */
		    argtab[2].meltbp_long = 6106;
		    /*^apply.arg */
		    argtab[3].meltbp_cstring =
		      "translate_run_melt_expressions gets ";
		    /*^apply.arg */
		    argtab[4].meltbp_aptr =
		      (melt_ptr_t *) & /*_.EXPRS__V2*/ meltfptr[1];
		    /*^apply.arg */
		    argtab[5].meltbp_cstring = " and gives null";
		    /*_.MELT_DEBUG_FUN__V13*/ meltfptr[12] =
		      melt_apply ((meltclosure_ptr_t)
				  (( /*!MELT_DEBUG_FUN */ meltfrout->
				    tabval[0])),
				  (melt_ptr_t) (( /*nil */ NULL)),
				  (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_PTR MELTBPARSTR_CSTRING ""),
				  argtab, "", (union meltparam_un *) 0);
		  }
		  ;
		  /*_.IF___V12*/ meltfptr[11] =
		    /*_.MELT_DEBUG_FUN__V13*/ meltfptr[12];;
		  /*epilog */

		  MELT_LOCATION ("warmelt-outobj.melt:6106:/ clear");
		 /*clear *//*_#THE_MELTCALLCOUNT__L7*/ meltfnum[6] = 0;
		  /*^clear */
		 /*clear *//*_.MELT_DEBUG_FUN__V13*/ meltfptr[12] = 0;
		}
		;
	      }
	    else
	      {			/*^cond.else */

      /*_.IF___V12*/ meltfptr[11] = NULL;;
	      }
	    ;
	    MELT_LOCATION ("warmelt-outobj.melt:6106:/ quasiblock");


	    /*_.PROGN___V14*/ meltfptr[12] = /*_.IF___V12*/ meltfptr[11];;
	    /*^compute */
	    /*_.IFCPP___V11*/ meltfptr[10] = /*_.PROGN___V14*/ meltfptr[12];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-outobj.melt:6106:/ clear");
	       /*clear *//*_#MELT_NEED_DBG__L6*/ meltfnum[5] = 0;
	    /*^clear */
	       /*clear *//*_.IF___V12*/ meltfptr[11] = 0;
	    /*^clear */
	       /*clear *//*_.PROGN___V14*/ meltfptr[12] = 0;
	  }

#else /*MELT_HAVE_DEBUG */
	  /*^cppif.else */
	  /*_.IFCPP___V11*/ meltfptr[10] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
	  ;
	  MELT_LOCATION ("warmelt-outobj.melt:6107:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^quasiblock */


	  /*_.RETVAL___V1*/ meltfptr[0] = ( /*nil */ NULL);;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:6107:/ locexp");
	    /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
	    if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	      melt_warn_for_no_expected_secondary_results ();
	    /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
	    ;
	  }
	  ;
	  /*^finalreturn */
	  ;
	  /*finalret */ goto labend_rout;
	  MELT_LOCATION ("warmelt-outobj.melt:6105:/ quasiblock");


	  /*_.PROGN___V16*/ meltfptr[12] = /*_.RETURN___V15*/ meltfptr[11];;
	  /*^compute */
	  /*_.IF___V10*/ meltfptr[5] = /*_.PROGN___V16*/ meltfptr[12];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-outobj.melt:6105:/ clear");
	     /*clear *//*_.IFCPP___V11*/ meltfptr[10] = 0;
	  /*^clear */
	     /*clear *//*_.RETURN___V15*/ meltfptr[11] = 0;
	  /*^clear */
	     /*clear *//*_.PROGN___V16*/ meltfptr[12] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

  /*_.IF___V10*/ meltfptr[5] = NULL;;
      }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:6108:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_.LIST_FIRST__V17*/ meltfptr[10] =
      (melt_list_first ((melt_ptr_t) ( /*_.EXPRS__V2*/ meltfptr[1])));;
    /*^compute */
 /*_#NULL__L8*/ meltfnum[6] =
      (( /*_.LIST_FIRST__V17*/ meltfptr[10]) == NULL);;
    MELT_LOCATION ("warmelt-outobj.melt:6108:/ cond");
    /*cond */ if ( /*_#NULL__L8*/ meltfnum[6])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{


#if MELT_HAVE_DEBUG
	  MELT_LOCATION ("warmelt-outobj.melt:6109:/ cppif.then");
	  /*^block */
	  /*anyblock */
	  {


	    {
	      /*^locexp */
	      /*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	      melt_dbgcounter++;
#endif
	      ;
	    }
	    ;
	    /*^checksignal */
	    MELT_CHECK_SIGNAL ();
	    ;
     /*_#MELT_NEED_DBG__L9*/ meltfnum[5] =
	      /*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	      ( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	      0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	      ;;
	    MELT_LOCATION ("warmelt-outobj.melt:6109:/ cond");
	    /*cond */ if ( /*_#MELT_NEED_DBG__L9*/ meltfnum[5])	/*then */
	      {
		/*^cond.then */
		/*^block */
		/*anyblock */
		{

       /*_#THE_MELTCALLCOUNT__L10*/ meltfnum[9] =
#ifdef meltcallcount
		    meltcallcount	/* the_meltcallcount */
#else
		    0L
#endif /* meltcallcount the_meltcallcount */
		    ;;
		  MELT_LOCATION ("warmelt-outobj.melt:6109:/ checksignal");
		  MELT_CHECK_SIGNAL ();
		  ;
		  /*^apply */
		  /*apply */
		  {
		    union meltparam_un argtab[4];
		    memset (&argtab, 0, sizeof (argtab));
		    /*^apply.arg */
		    argtab[0].meltbp_long =
		      /*_#THE_MELTCALLCOUNT__L10*/ meltfnum[9];
		    /*^apply.arg */
		    argtab[1].meltbp_cstring = "warmelt-outobj.melt";
		    /*^apply.arg */
		    argtab[2].meltbp_long = 6109;
		    /*^apply.arg */
		    argtab[3].meltbp_cstring =
		      "translate_run_melt_expressions gets empty list and gives null";
		    /*_.MELT_DEBUG_FUN__V21*/ meltfptr[20] =
		      melt_apply ((meltclosure_ptr_t)
				  (( /*!MELT_DEBUG_FUN */ meltfrout->
				    tabval[0])),
				  (melt_ptr_t) (( /*nil */ NULL)),
				  (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_LONG MELTBPARSTR_CSTRING ""),
				  argtab, "", (union meltparam_un *) 0);
		  }
		  ;
		  /*_.IF___V20*/ meltfptr[19] =
		    /*_.MELT_DEBUG_FUN__V21*/ meltfptr[20];;
		  /*epilog */

		  MELT_LOCATION ("warmelt-outobj.melt:6109:/ clear");
		 /*clear *//*_#THE_MELTCALLCOUNT__L10*/ meltfnum[9] = 0;
		  /*^clear */
		 /*clear *//*_.MELT_DEBUG_FUN__V21*/ meltfptr[20] = 0;
		}
		;
	      }
	    else
	      {			/*^cond.else */

      /*_.IF___V20*/ meltfptr[19] = NULL;;
	      }
	    ;
	    MELT_LOCATION ("warmelt-outobj.melt:6109:/ quasiblock");


	    /*_.PROGN___V22*/ meltfptr[20] = /*_.IF___V20*/ meltfptr[19];;
	    /*^compute */
	    /*_.IFCPP___V19*/ meltfptr[12] = /*_.PROGN___V22*/ meltfptr[20];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-outobj.melt:6109:/ clear");
	       /*clear *//*_#MELT_NEED_DBG__L9*/ meltfnum[5] = 0;
	    /*^clear */
	       /*clear *//*_.IF___V20*/ meltfptr[19] = 0;
	    /*^clear */
	       /*clear *//*_.PROGN___V22*/ meltfptr[20] = 0;
	  }

#else /*MELT_HAVE_DEBUG */
	  /*^cppif.else */
	  /*_.IFCPP___V19*/ meltfptr[12] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
	  ;
	  MELT_LOCATION ("warmelt-outobj.melt:6110:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^quasiblock */


	  /*_.RETVAL___V1*/ meltfptr[0] = ( /*nil */ NULL);;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:6110:/ locexp");
	    /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
	    if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	      melt_warn_for_no_expected_secondary_results ();
	    /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
	    ;
	  }
	  ;
	  /*^finalreturn */
	  ;
	  /*finalret */ goto labend_rout;
	  MELT_LOCATION ("warmelt-outobj.melt:6108:/ quasiblock");


	  /*_.PROGN___V24*/ meltfptr[20] = /*_.RETURN___V23*/ meltfptr[19];;
	  /*^compute */
	  /*_.IF___V18*/ meltfptr[11] = /*_.PROGN___V24*/ meltfptr[20];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-outobj.melt:6108:/ clear");
	     /*clear *//*_.IFCPP___V19*/ meltfptr[12] = 0;
	  /*^clear */
	     /*clear *//*_.RETURN___V23*/ meltfptr[19] = 0;
	  /*^clear */
	     /*clear *//*_.PROGN___V24*/ meltfptr[20] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

  /*_.IF___V18*/ meltfptr[11] = NULL;;
      }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:6111:/ quasiblock");


 /*_#NUM__L11*/ meltfnum[9] = 0;;
    /*^compute */
 /*_#STARTERRCOUNT__L12*/ meltfnum[5] =
      melt_error_counter;;
    /*^compute */
    /*_.NAKEDBASNAM__V26*/ meltfptr[19] = ( /*nil */ NULL);;
    MELT_LOCATION ("warmelt-outobj.melt:6114:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*^rawallocobj */
    /*rawallocobj */
    {
      melt_ptr_t newobj = 0;
      melt_raw_object_create (newobj,
			      (melt_ptr_t) (( /*!CLASS_REFERENCE */
					     meltfrout->tabval[2])), (1),
			      "CLASS_REFERENCE");
  /*_.INST__V28*/ meltfptr[27] =
	newobj;
    };
    ;
    /*^touchobj */

    melt_dbgtrace_written_object ( /*_.INST__V28*/ meltfptr[27],
				  "newly made instance");
    ;
    /*_.REFERR__V27*/ meltfptr[20] = /*_.INST__V28*/ meltfptr[27];;
    MELT_LOCATION ("warmelt-outobj.melt:6116:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*^rawallocobj */
    /*rawallocobj */
    {
      melt_ptr_t newobj = 0;
      melt_raw_object_create (newobj,
			      (melt_ptr_t) (( /*!CLASS_REFERENCE */
					     meltfrout->tabval[2])), (1),
			      "CLASS_REFERENCE");
  /*_.INST__V30*/ meltfptr[29] =
	newobj;
    };
    ;
    /*^touchobj */

    melt_dbgtrace_written_object ( /*_.INST__V30*/ meltfptr[29],
				  "newly made instance");
    ;
    /*_.REFNORMLIST__V29*/ meltfptr[28] = /*_.INST__V30*/ meltfptr[29];;

    {
      MELT_LOCATION ("warmelt-outobj.melt:6118:/ locexp");
      /* translate_run_melt_expressions NUMCHK__1 */
      char basbuf_NUMCHK__1[64];
      long num_NUMCHK__1 = 0;
      memset (basbuf_NUMCHK__1, 0, sizeof (basbuf_NUMCHK__1));
      melt_count_runtime_extensions++;
      num_NUMCHK__1 = melt_count_runtime_extensions;
      if (melt_count_runtime_extensions > MELT_MAX_RUNTIME_EXTENSIONS)
	melt_fatal_error ("too many %ld MELT runtime extensions",
			  num_NUMCHK__1);
      snprintf (basbuf_NUMCHK__1, sizeof (basbuf_NUMCHK__1),
		"MELTrun%04lx", num_NUMCHK__1);
		/*_.NAKEDBASNAM__V26*/ meltfptr[19] =
	meltgc_new_string_raw_len ((meltobject_ptr_t)
			 MELT_PREDEF (DISCR_STRING),
				   basbuf_NUMCHK__1, -1);
      ;
    }
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:6133:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L13*/ meltfnum[12] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-outobj.melt:6133:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L13*/ meltfnum[12])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L14*/ meltfnum[13] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-outobj.melt:6133:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[9];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long =
		/*_#THE_MELTCALLCOUNT__L14*/ meltfnum[13];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-outobj.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 6133;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring =
		"translate_run_melt_expressions nakedbasnam=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.NAKEDBASNAM__V26*/ meltfptr[19];
	      /*^apply.arg */
	      argtab[5].meltbp_cstring = " num=";
	      /*^apply.arg */
	      argtab[6].meltbp_long = /*_#NUM__L11*/ meltfnum[9];
	      /*^apply.arg */
	      argtab[7].meltbp_cstring = " starterrcount=";
	      /*^apply.arg */
	      argtab[8].meltbp_long = /*_#STARTERRCOUNT__L12*/ meltfnum[5];
	      /*_.MELT_DEBUG_FUN__V33*/ meltfptr[32] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V32*/ meltfptr[31] =
	      /*_.MELT_DEBUG_FUN__V33*/ meltfptr[32];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-outobj.melt:6133:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L14*/ meltfnum[13] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V33*/ meltfptr[32] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V32*/ meltfptr[31] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-outobj.melt:6133:/ quasiblock");


      /*_.PROGN___V34*/ meltfptr[32] = /*_.IF___V32*/ meltfptr[31];;
      /*^compute */
      /*_.IFCPP___V31*/ meltfptr[30] = /*_.PROGN___V34*/ meltfptr[32];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:6133:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L13*/ meltfnum[12] = 0;
      /*^clear */
	     /*clear *//*_.IF___V32*/ meltfptr[31] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V34*/ meltfptr[32] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V31*/ meltfptr[30] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-outobj.melt:6134:/ quasiblock");


    MELT_LOCATION ("warmelt-outobj.melt:6136:/ quasiblock");


    /*^newclosure */
		 /*newclosure *//*_.LAMBDA___V37*/ meltfptr[36] =
      (melt_ptr_t)
      meltgc_new_closure ((meltobject_ptr_t)
			  (((melt_ptr_t) (MELT_PREDEF (DISCR_CLOSURE)))),
			  (meltroutine_ptr_t) (( /*!konst_5 */ meltfrout->
						tabval[5])), (2));
    ;
    /*^putclosedv */
    /*putclosv */
    melt_assertmsg ("putclosv checkclo",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.LAMBDA___V37*/ meltfptr[36])) ==
		    MELTOBMAG_CLOSURE);
    melt_assertmsg ("putclosv checkoff", 0 >= 0
		    && 0 <
		    melt_closure_size ((melt_ptr_t)
				       ( /*_.LAMBDA___V37*/ meltfptr[36])));
    ((meltclosure_ptr_t) /*_.LAMBDA___V37*/ meltfptr[36])->tabval[0] =
      (melt_ptr_t) ( /*_.NAKEDBASNAM__V26*/ meltfptr[19]);
    ;
    /*^putclosedv */
    /*putclosv */
    melt_assertmsg ("putclosv checkclo",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.LAMBDA___V37*/ meltfptr[36])) ==
		    MELTOBMAG_CLOSURE);
    melt_assertmsg ("putclosv checkoff", 1 >= 0
		    && 1 <
		    melt_closure_size ((melt_ptr_t)
				       ( /*_.LAMBDA___V37*/ meltfptr[36])));
    ((meltclosure_ptr_t) /*_.LAMBDA___V37*/ meltfptr[36])->tabval[1] =
      (melt_ptr_t) ( /*_.REFERR__V27*/ meltfptr[20]);
    ;
    /*_.RUNERRORHDLR__V36*/ meltfptr[32] = /*_.LAMBDA___V37*/ meltfptr[36];;
    MELT_LOCATION ("warmelt-outobj.melt:6155:/ blockmultialloc");
    /*multiallocblock */
    {
      struct meltletrec_1_st
      {
	struct meltpair_st rpair_0__PAIROFLIST_x1;
	struct meltlist_st rlist_1__LIST_;
	long meltletrec_1_endgap;
      } *meltletrec_1_ptr = 0;
      meltletrec_1_ptr =
	(struct meltletrec_1_st *)
	meltgc_allocate (sizeof (struct meltletrec_1_st), 0);
      /*^blockmultialloc.initfill */
      /*inipair rpair_0__PAIROFLIST_x1 */
   /*_.PAIROFLIST__V39*/ meltfptr[38] =
	(melt_ptr_t) & meltletrec_1_ptr->rpair_0__PAIROFLIST_x1;
      meltletrec_1_ptr->rpair_0__PAIROFLIST_x1.discr =
	(meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (DISCR_PAIR))));

      /*inilist rlist_1__LIST_ */
   /*_.LIST___V40*/ meltfptr[39] =
	(melt_ptr_t) & meltletrec_1_ptr->rlist_1__LIST_;
      meltletrec_1_ptr->rlist_1__LIST_.discr =
	(meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (DISCR_LIST))));



      /*^putpairhead */
      /*putpairhead */
      melt_assertmsg ("putpairhead /1 checkpair",
		      melt_magic_discr ((melt_ptr_t)
					( /*_.PAIROFLIST__V39*/ meltfptr[38]))
		      == MELTOBMAG_PAIR);
      ((meltpair_ptr_t) ( /*_.PAIROFLIST__V39*/ meltfptr[38]))->hd =
	(melt_ptr_t) (( /*nil */ NULL));
      ;
      /*^touch */
      meltgc_touch ( /*_.PAIROFLIST__V39*/ meltfptr[38]);
      ;
      /*^putlist */
      /*putlist */
      melt_assertmsg ("putlist checklist",
		      melt_magic_discr ((melt_ptr_t)
					( /*_.LIST___V40*/ meltfptr[39])) ==
		      MELTOBMAG_LIST);
      ((meltlist_ptr_t) ( /*_.LIST___V40*/ meltfptr[39]))->first =
	(meltpair_ptr_t) ( /*_.PAIROFLIST__V39*/ meltfptr[38]);
      ((meltlist_ptr_t) ( /*_.LIST___V40*/ meltfptr[39]))->last =
	(meltpair_ptr_t) ( /*_.PAIROFLIST__V39*/ meltfptr[38]);
      ;
      /*^touch */
      meltgc_touch ( /*_.LIST___V40*/ meltfptr[39]);
      ;
      /*_.LITVALIST__V38*/ meltfptr[37] = /*_.LIST___V40*/ meltfptr[39];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:6155:/ clear");
	    /*clear *//*_.PAIROFLIST__V39*/ meltfptr[38] = 0;
      /*^clear */
	    /*clear *//*_.LIST___V40*/ meltfptr[39] = 0;
      /*^clear */
	    /*clear *//*_.PAIROFLIST__V39*/ meltfptr[38] = 0;
      /*^clear */
	    /*clear *//*_.LIST___V40*/ meltfptr[39] = 0;
    }				/*end multiallocblock */
    ;
    MELT_LOCATION ("warmelt-outobj.melt:6157:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_.MAKE_MAPSTRING__V41*/ meltfptr[38] =
      (meltgc_new_mapstrings
       ((meltobject_ptr_t) (( /*!DISCR_MAP_STRINGS */ meltfrout->tabval[7])),
	(71)));;
    /*^compute */
 /*_.MAKE_MAPSTRING__V42*/ meltfptr[39] =
      (meltgc_new_mapstrings
       ((meltobject_ptr_t) (( /*!DISCR_MAP_STRINGS */ meltfrout->tabval[7])),
	(19)));;
    /*^compute */
 /*_.MAKE_INTEGERBOX__V43*/ meltfptr[42] =
      (meltgc_new_int
       ((meltobject_ptr_t) (( /*!DISCR_INTEGER */ meltfrout->tabval[8])),
	(0)));;
    /*^compute */
 /*_.MAKE_LIST__V44*/ meltfptr[43] =
      (meltgc_new_list
       ((meltobject_ptr_t) (( /*!DISCR_LIST */ meltfrout->tabval[9]))));;
    /*^compute */
 /*_.MAKE_LIST__V45*/ meltfptr[44] =
      (meltgc_new_list
       ((meltobject_ptr_t) (( /*!DISCR_LIST */ meltfrout->tabval[9]))));;
    /*^compute */
 /*_.MAKE_LIST__V46*/ meltfptr[45] =
      (meltgc_new_list
       ((meltobject_ptr_t) (( /*!DISCR_LIST */ meltfrout->tabval[9]))));;
    /*^compute */
 /*_.MAKE_INTEGERBOX__V47*/ meltfptr[46] =
      (meltgc_new_int
       ((meltobject_ptr_t) (( /*!DISCR_INTEGER */ meltfrout->tabval[8])),
	(1)));;
    /*^compute */
 /*_.MAKE_MAPOBJECT__V48*/ meltfptr[47] =
      (meltgc_new_mapobjects
       ((meltobject_ptr_t) (( /*!DISCR_MAP_OBJECTS */ meltfrout->tabval[10])),
	(53)));;
    MELT_LOCATION ("warmelt-outobj.melt:6157:/ quasiblock");


    /*^rawallocobj */
    /*rawallocobj */
    {
      melt_ptr_t newobj = 0;
      melt_raw_object_create (newobj,
			      (melt_ptr_t) (( /*!CLASS_RUNNING_EXTENSION_MODULE_CONTEXT */ meltfrout->tabval[6])), (15), "CLASS_RUNNING_EXTENSION_MODULE_CONTEXT");
  /*_.INST__V50*/ meltfptr[49] =
	newobj;
    };
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @MOCX_MODULENAME",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V50*/ meltfptr[49])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V50*/ meltfptr[49]), (0),
			  ( /*_.NAKEDBASNAM__V26*/ meltfptr[19]),
			  "MOCX_MODULENAME");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @MOCX_EXPFIELDICT",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V50*/ meltfptr[49])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V50*/ meltfptr[49]), (1),
			  ( /*_.MAKE_MAPSTRING__V41*/ meltfptr[38]),
			  "MOCX_EXPFIELDICT");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @MOCX_EXPCLASSDICT",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V50*/ meltfptr[49])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V50*/ meltfptr[49]), (2),
			  ( /*_.MAKE_MAPSTRING__V42*/ meltfptr[39]),
			  "MOCX_EXPCLASSDICT");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @MOCX_INITIALENV",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V50*/ meltfptr[49])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V50*/ meltfptr[49]), (3),
			  ( /*_.ENV__V3*/ meltfptr[2]), "MOCX_INITIALENV");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @MOCX_FUNCOUNT",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V50*/ meltfptr[49])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V50*/ meltfptr[49]), (4),
			  ( /*_.MAKE_INTEGERBOX__V43*/ meltfptr[42]),
			  "MOCX_FUNCOUNT");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @MOCX_FILETUPLE",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V50*/ meltfptr[49])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V50*/ meltfptr[49]), (5),
			  (( /*nil */ NULL)), "MOCX_FILETUPLE");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @MOCX_CHEADERLIST",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V50*/ meltfptr[49])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V50*/ meltfptr[49]), (8),
			  ( /*_.MAKE_LIST__V44*/ meltfptr[43]),
			  "MOCX_CHEADERLIST");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @MOCX_PACKAGEPCLIST",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V50*/ meltfptr[49])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V50*/ meltfptr[49]), (10),
			  ( /*_.MAKE_LIST__V45*/ meltfptr[44]),
			  "MOCX_PACKAGEPCLIST");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @MOCX_GENDEVLIST",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V50*/ meltfptr[49])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V50*/ meltfptr[49]), (9),
			  ( /*_.MAKE_LIST__V46*/ meltfptr[45]),
			  "MOCX_GENDEVLIST");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @MOCX_ERRORHANDLER",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V50*/ meltfptr[49])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V50*/ meltfptr[49]), (11),
			  ( /*_.RUNERRORHDLR__V36*/ meltfptr[32]),
			  "MOCX_ERRORHANDLER");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @MORCX_LITERVALIST",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V50*/ meltfptr[49])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V50*/ meltfptr[49]), (12),
			  ( /*_.LITVALIST__V38*/ meltfptr[37]),
			  "MORCX_LITERVALIST");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @MORCX_COUNTLITVAL",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V50*/ meltfptr[49])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V50*/ meltfptr[49]), (14),
			  ( /*_.MAKE_INTEGERBOX__V47*/ meltfptr[46]),
			  "MORCX_COUNTLITVAL");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @MORCX_LITEROBJMAP",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V50*/ meltfptr[49])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V50*/ meltfptr[49]), (13),
			  ( /*_.MAKE_MAPOBJECT__V48*/ meltfptr[47]),
			  "MORCX_LITEROBJMAP");
    ;
    /*^touchobj */

    melt_dbgtrace_written_object ( /*_.INST__V50*/ meltfptr[49],
				  "newly made instance");
    ;
    /*_.MODCTX__V49*/ meltfptr[48] = /*_.INST__V50*/ meltfptr[49];;
    MELT_LOCATION ("warmelt-outobj.melt:6175:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.ENV__V3*/ meltfptr[2];
      /*_.NCX__V51*/ meltfptr[50] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!CREATE_NORMAL_EXTENDING_CONTEXT */ meltfrout->
		      tabval[11])),
		    (melt_ptr_t) ( /*_.MODCTX__V49*/ meltfptr[48]),
		    (MELTBPARSTR_PTR ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:6177:/ quasiblock");


 /*_.LS__V53*/ meltfptr[52] =
      (meltgc_new_list
       ((meltobject_ptr_t) (( /*!DISCR_LIST */ meltfrout->tabval[9]))));;
    MELT_LOCATION ("warmelt-outobj.melt:6180:/ quasiblock");


    /*^newclosure */
		 /*newclosure *//*_.LAMBDA___V55*/ meltfptr[54] =
      (melt_ptr_t)
      meltgc_new_closure ((meltobject_ptr_t)
			  (((melt_ptr_t) (MELT_PREDEF (DISCR_CLOSURE)))),
			  (meltroutine_ptr_t) (( /*!konst_16 */ meltfrout->
						tabval[16])), (3));
    ;
    /*^putclosedv */
    /*putclosv */
    melt_assertmsg ("putclosv checkclo",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.LAMBDA___V55*/ meltfptr[54])) ==
		    MELTOBMAG_CLOSURE);
    melt_assertmsg ("putclosv checkoff", 0 >= 0
		    && 0 <
		    melt_closure_size ((melt_ptr_t)
				       ( /*_.LAMBDA___V55*/ meltfptr[54])));
    ((meltclosure_ptr_t) /*_.LAMBDA___V55*/ meltfptr[54])->tabval[0] =
      (melt_ptr_t) ( /*_.LS__V53*/ meltfptr[52]);
    ;
    /*^putclosedv */
    /*putclosv */
    melt_assertmsg ("putclosv checkclo",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.LAMBDA___V55*/ meltfptr[54])) ==
		    MELTOBMAG_CLOSURE);
    melt_assertmsg ("putclosv checkoff", 1 >= 0
		    && 1 <
		    melt_closure_size ((melt_ptr_t)
				       ( /*_.LAMBDA___V55*/ meltfptr[54])));
    ((meltclosure_ptr_t) /*_.LAMBDA___V55*/ meltfptr[54])->tabval[1] =
      (melt_ptr_t) ( /*_.NAKEDBASNAM__V26*/ meltfptr[19]);
    ;
    /*^putclosedv */
    /*putclosv */
    melt_assertmsg ("putclosv checkclo",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.LAMBDA___V55*/ meltfptr[54])) ==
		    MELTOBMAG_CLOSURE);
    melt_assertmsg ("putclosv checkoff", 2 >= 0
		    && 2 <
		    melt_closure_size ((melt_ptr_t)
				       ( /*_.LAMBDA___V55*/ meltfptr[54])));
    ((meltclosure_ptr_t) /*_.LAMBDA___V55*/ meltfptr[54])->tabval[2] =
      (melt_ptr_t) ( /*_.RUNERRORHDLR__V36*/ meltfptr[32]);
    ;
    /*_.ADDEXP__V54*/ meltfptr[53] = /*_.LAMBDA___V55*/ meltfptr[54];;
    MELT_LOCATION ("warmelt-outobj.melt:6212:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#NULL__L15*/ meltfnum[13] =
      (( /*_.EXPRS__V2*/ meltfptr[1]) == NULL);;
    MELT_LOCATION ("warmelt-outobj.melt:6212:/ cond");
    /*cond */ if ( /*_#NULL__L15*/ meltfnum[13])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{




	  {
	    MELT_LOCATION ("warmelt-outobj.melt:6213:/ locexp");
	    /*void */ (void) 0;
	  }
	  ;
	     /*clear *//*_.IFELSE___V56*/ meltfptr[55] = 0;
	  /*epilog */
	}
	;
      }
    else
      {
	MELT_LOCATION ("warmelt-outobj.melt:6212:/ cond.else");

	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-outobj.melt:6214:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
   /*_#IS_MULTIPLE__L16*/ meltfnum[12] =
	    (melt_magic_discr ((melt_ptr_t) ( /*_.EXPRS__V2*/ meltfptr[1])) ==
	     MELTOBMAG_MULTIPLE);;
	  MELT_LOCATION ("warmelt-outobj.melt:6214:/ cond");
	  /*cond */ if ( /*_#IS_MULTIPLE__L16*/ meltfnum[12])	/*then */
	    {
	      /*^cond.then */
	      /*^block */
	      /*anyblock */
	      {

		/*citerblock FOREACH_IN_MULTIPLE */
		{
		  /* start foreach_in_multiple meltcit1__EACHTUP */
		  long meltcit1__EACHTUP_ln =
		    melt_multiple_length ((melt_ptr_t) /*_.EXPRS__V2*/
					  meltfptr[1]);
		  for ( /*_#IX__L17*/ meltfnum[16] = 0;
		       ( /*_#IX__L17*/ meltfnum[16] >= 0)
		       && ( /*_#IX__L17*/ meltfnum[16] <
			   meltcit1__EACHTUP_ln);
	/*_#IX__L17*/ meltfnum[16]++)
		    {
		      /*_.CUREXP__V58*/ meltfptr[57] =
			melt_multiple_nth ((melt_ptr_t)
					   ( /*_.EXPRS__V2*/ meltfptr[1]),
					   /*_#IX__L17*/ meltfnum[16]);



		      MELT_LOCATION
			("warmelt-outobj.melt:6218:/ checksignal");
		      MELT_CHECK_SIGNAL ();
		      ;
		      /*^apply */
		      /*apply */
		      {
			union meltparam_un argtab[1];
			memset (&argtab, 0, sizeof (argtab));
			/*^apply.arg */
			argtab[0].meltbp_long = /*_#IX__L17*/ meltfnum[16];
			/*_.ADDEXP__V59*/ meltfptr[58] =
			  melt_apply ((meltclosure_ptr_t)
				      ( /*_.ADDEXP__V54*/ meltfptr[53]),
				      (melt_ptr_t) ( /*_.CUREXP__V58*/
						    meltfptr[57]),
				      (MELTBPARSTR_LONG ""), argtab, "",
				      (union meltparam_un *) 0);
		      }
		      ;
		      /*_.IFELSE___V57*/ meltfptr[56] =
			/*_.ADDEXP__V59*/ meltfptr[58];;
		      if ( /*_#IX__L17*/ meltfnum[16] < 0)
			break;
		    }		/* end  foreach_in_multiple meltcit1__EACHTUP */

		  /*citerepilog */

		  MELT_LOCATION ("warmelt-outobj.melt:6215:/ clear");
		/*clear *//*_.CUREXP__V58*/ meltfptr[57] = 0;
		  /*^clear */
		/*clear *//*_#IX__L17*/ meltfnum[16] = 0;
		  /*^clear */
		/*clear *//*_.ADDEXP__V59*/ meltfptr[58] = 0;
		}		/*endciterblock FOREACH_IN_MULTIPLE */
		;
		/*epilog */
	      }
	      ;
	    }
	  else
	    {
	      MELT_LOCATION ("warmelt-outobj.melt:6214:/ cond.else");

	      /*^block */
	      /*anyblock */
	      {

		MELT_LOCATION ("warmelt-outobj.melt:6220:/ checksignal");
		MELT_CHECK_SIGNAL ();
		;
     /*_#IS_LIST__L18*/ meltfnum[17] =
		  (melt_magic_discr
		   ((melt_ptr_t) ( /*_.EXPRS__V2*/ meltfptr[1])) ==
		   MELTOBMAG_LIST);;
		MELT_LOCATION ("warmelt-outobj.melt:6220:/ cond");
		/*cond */ if ( /*_#IS_LIST__L18*/ meltfnum[17])	/*then */
		  {
		    /*^cond.then */
		    /*^block */
		    /*anyblock */
		    {

		      MELT_LOCATION ("warmelt-outobj.melt:6221:/ quasiblock");


       /*_#IX__L19*/ meltfnum[18] = 0;;
		      /*citerblock FOREACH_IN_LIST */
		      {
			/* start foreach_in_list meltcit2__EACHLIST */
			for ( /*_.CURPAIR__V61*/ meltfptr[60] =
			     melt_list_first ((melt_ptr_t) /*_.EXPRS__V2*/
					      meltfptr[1]);
			     melt_magic_discr ((melt_ptr_t) /*_.CURPAIR__V61*/
					       meltfptr[60]) ==
			     MELTOBMAG_PAIR;
			     /*_.CURPAIR__V61*/ meltfptr[60] =
			     melt_pair_tail ((melt_ptr_t) /*_.CURPAIR__V61*/
					     meltfptr[60]))
			  {
			    /*_.CUREXP__V62*/ meltfptr[61] =
			      melt_pair_head ((melt_ptr_t) /*_.CURPAIR__V61*/
					      meltfptr[60]);


	/*_#I__L20*/ meltfnum[19] =
			      (( /*_#IX__L19*/ meltfnum[18]) - (1));;
			    MELT_LOCATION
			      ("warmelt-outobj.melt:6225:/ compute");
			    /*_#IX__L19*/ meltfnum[18] =
			      /*_#SETQ___L21*/ meltfnum[20] =
			      /*_#I__L20*/ meltfnum[19];;
			    MELT_LOCATION
			      ("warmelt-outobj.melt:6226:/ checksignal");
			    MELT_CHECK_SIGNAL ();
			    ;
			    /*^apply */
			    /*apply */
			    {
			      union meltparam_un argtab[1];
			      memset (&argtab, 0, sizeof (argtab));
			      /*^apply.arg */
			      argtab[0].meltbp_long =
				/*_#IX__L19*/ meltfnum[18];
			      /*_.ADDEXP__V63*/ meltfptr[62] =
				melt_apply ((meltclosure_ptr_t)
					    ( /*_.ADDEXP__V54*/ meltfptr[53]),
					    (melt_ptr_t) ( /*_.CUREXP__V62*/
							  meltfptr[61]),
					    (MELTBPARSTR_LONG ""), argtab, "",
					    (union meltparam_un *) 0);
			    }
			    ;
			    /*_.IFELSE___V60*/ meltfptr[59] =
			      /*_.ADDEXP__V63*/ meltfptr[62];;
			  }	/* end foreach_in_list meltcit2__EACHLIST */
     /*_.CURPAIR__V61*/ meltfptr[60] = NULL;
     /*_.CUREXP__V62*/ meltfptr[61] = NULL;


			/*citerepilog */

			MELT_LOCATION ("warmelt-outobj.melt:6222:/ clear");
		  /*clear *//*_.CURPAIR__V61*/ meltfptr[60] = 0;
			/*^clear */
		  /*clear *//*_.CUREXP__V62*/ meltfptr[61] = 0;
			/*^clear */
		  /*clear *//*_#I__L20*/ meltfnum[19] = 0;
			/*^clear */
		  /*clear *//*_#SETQ___L21*/ meltfnum[20] = 0;
			/*^clear */
		  /*clear *//*_.ADDEXP__V63*/ meltfptr[62] = 0;
		      }		/*endciterblock FOREACH_IN_LIST */
		      ;

		      MELT_LOCATION ("warmelt-outobj.melt:6221:/ clear");
		 /*clear *//*_#IX__L19*/ meltfnum[18] = 0;
		      /*epilog */
		    }
		    ;
		  }
		else
		  {
		    MELT_LOCATION ("warmelt-outobj.melt:6220:/ cond.else");

		    /*^block */
		    /*anyblock */
		    {

		      MELT_LOCATION
			("warmelt-outobj.melt:6227:/ checksignal");
		      MELT_CHECK_SIGNAL ();
		      ;
       /*_#IS_A__L22*/ meltfnum[18] =
			melt_is_instance_of ((melt_ptr_t)
					     ( /*_.EXPRS__V2*/ meltfptr[1]),
					     (melt_ptr_t) (( /*!CLASS_SEXPR */
							    meltfrout->
							    tabval[17])));;
		      MELT_LOCATION ("warmelt-outobj.melt:6227:/ cond");
		      /*cond */ if ( /*_#IS_A__L22*/ meltfnum[18])	/*then */
			{
			  /*^cond.then */
			  /*^block */
			  /*anyblock */
			  {

			    MELT_LOCATION
			      ("warmelt-outobj.melt:6228:/ checksignal");
			    MELT_CHECK_SIGNAL ();
			    ;
			    /*^apply */
			    /*apply */
			    {
			      union meltparam_un argtab[1];
			      memset (&argtab, 0, sizeof (argtab));
			      /*^apply.arg */
			      argtab[0].meltbp_long = -999999;
			      /*_.ADDEXP__V65*/ meltfptr[64] =
				melt_apply ((meltclosure_ptr_t)
					    ( /*_.ADDEXP__V54*/ meltfptr[53]),
					    (melt_ptr_t) ( /*_.EXPRS__V2*/
							  meltfptr[1]),
					    (MELTBPARSTR_LONG ""), argtab, "",
					    (union meltparam_un *) 0);
			    }
			    ;
			    /*_.IFELSE___V64*/ meltfptr[63] =
			      /*_.ADDEXP__V65*/ meltfptr[64];;
			    /*epilog */

			    MELT_LOCATION
			      ("warmelt-outobj.melt:6227:/ clear");
		   /*clear *//*_.ADDEXP__V65*/ meltfptr[64] = 0;
			  }
			  ;
			}
		      else
			{	/*^cond.else */

			  /*^block */
			  /*anyblock */
			  {

			    MELT_LOCATION
			      ("warmelt-outobj.melt:6230:/ checksignal");
			    MELT_CHECK_SIGNAL ();
			    ;
			    /*^apply */
			    /*apply */
			    {
			      /*_.RUNERRORHDLR__V66*/ meltfptr[64] =
				melt_apply ((meltclosure_ptr_t)
					    ( /*_.RUNERRORHDLR__V36*/
					     meltfptr[32]),
					    (melt_ptr_t) (( /*!konst_18 */
							   meltfrout->
							   tabval[18])), (""),
					    (union meltparam_un *) 0, "",
					    (union meltparam_un *) 0);
			    }
			    ;
			    MELT_LOCATION
			      ("warmelt-outobj.melt:6229:/ quasiblock");


			    /*_.PROGN___V67*/ meltfptr[66] =
			      /*_.RUNERRORHDLR__V66*/ meltfptr[64];;
			    /*^compute */
			    /*_.IFELSE___V64*/ meltfptr[63] =
			      /*_.PROGN___V67*/ meltfptr[66];;
			    /*epilog */

			    MELT_LOCATION
			      ("warmelt-outobj.melt:6227:/ clear");
		   /*clear *//*_.RUNERRORHDLR__V66*/ meltfptr[64] =
			      0;
			    /*^clear */
		   /*clear *//*_.PROGN___V67*/ meltfptr[66] = 0;
			  }
			  ;
			}
		      ;
		      /*_.IFELSE___V60*/ meltfptr[59] =
			/*_.IFELSE___V64*/ meltfptr[63];;
		      /*epilog */

		      MELT_LOCATION ("warmelt-outobj.melt:6220:/ clear");
		 /*clear *//*_#IS_A__L22*/ meltfnum[18] = 0;
		      /*^clear */
		 /*clear *//*_.IFELSE___V64*/ meltfptr[63] = 0;
		    }
		    ;
		  }
		;
		/*_.IFELSE___V57*/ meltfptr[56] =
		  /*_.IFELSE___V60*/ meltfptr[59];;
		/*epilog */

		MELT_LOCATION ("warmelt-outobj.melt:6214:/ clear");
	       /*clear *//*_#IS_LIST__L18*/ meltfnum[17] = 0;
		/*^clear */
	       /*clear *//*_.IFELSE___V60*/ meltfptr[59] = 0;
	      }
	      ;
	    }
	  ;
	  /*_.IFELSE___V56*/ meltfptr[55] = /*_.IFELSE___V57*/ meltfptr[56];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-outobj.melt:6212:/ clear");
	     /*clear *//*_#IS_MULTIPLE__L16*/ meltfnum[12] = 0;
	  /*^clear */
	     /*clear *//*_.IFELSE___V57*/ meltfptr[56] = 0;
	}
	;
      }
    ;
    /*_.LET___V52*/ meltfptr[51] = /*_.LS__V53*/ meltfptr[52];;

    MELT_LOCATION ("warmelt-outobj.melt:6177:/ clear");
	   /*clear *//*_.LS__V53*/ meltfptr[52] = 0;
    /*^clear */
	   /*clear *//*_.ADDEXP__V54*/ meltfptr[53] = 0;
    /*^clear */
	   /*clear *//*_#NULL__L15*/ meltfnum[13] = 0;
    /*^clear */
	   /*clear *//*_.IFELSE___V56*/ meltfptr[55] = 0;
    /*_.LSEXP__V68*/ meltfptr[64] = /*_.LET___V52*/ meltfptr[51];;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:6234:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L23*/ meltfnum[18] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-outobj.melt:6234:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L23*/ meltfnum[18])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L24*/ meltfnum[17] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-outobj.melt:6234:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[9];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long =
		/*_#THE_MELTCALLCOUNT__L24*/ meltfnum[17];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-outobj.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 6234;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring =
		"translate_run_melt_expressions modctx=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.MODCTX__V49*/ meltfptr[48];
	      /*^apply.arg */
	      argtab[5].meltbp_cstring = "\n ncx=";
	      /*^apply.arg */
	      argtab[6].meltbp_aptr =
		(melt_ptr_t *) & /*_.NCX__V51*/ meltfptr[50];
	      /*^apply.arg */
	      argtab[7].meltbp_cstring = "\n lsexp=";
	      /*^apply.arg */
	      argtab[8].meltbp_aptr =
		(melt_ptr_t *) & /*_.LSEXP__V68*/ meltfptr[64];
	      /*_.MELT_DEBUG_FUN__V71*/ meltfptr[59] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V70*/ meltfptr[63] =
	      /*_.MELT_DEBUG_FUN__V71*/ meltfptr[59];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-outobj.melt:6234:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L24*/ meltfnum[17] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V71*/ meltfptr[59] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V70*/ meltfptr[63] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-outobj.melt:6234:/ quasiblock");


      /*_.PROGN___V72*/ meltfptr[56] = /*_.IF___V70*/ meltfptr[63];;
      /*^compute */
      /*_.IFCPP___V69*/ meltfptr[66] = /*_.PROGN___V72*/ meltfptr[56];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:6234:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L23*/ meltfnum[18] = 0;
      /*^clear */
	     /*clear *//*_.IF___V70*/ meltfptr[63] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V72*/ meltfptr[56] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V69*/ meltfptr[66] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-outobj.melt:6235:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^cond */
    /*cond */ if (
		   /*ifisa */
		   melt_is_instance_of ((melt_ptr_t)
					( /*_.REFERR__V27*/ meltfptr[20]),
					(melt_ptr_t) (( /*!CLASS_REFERENCE */
						       meltfrout->tabval[2])))
      )				/*then */
      {
	/*^cond.then */
	/*^getslot */
	{
	  melt_ptr_t slot = NULL, obj = NULL;
	  obj = (melt_ptr_t) ( /*_.REFERR__V27*/ meltfptr[20]) /*=obj*/ ;
	  melt_object_get_field (slot, obj, 0, "REFERENCED_VALUE");
   /*_.REFERENCED_VALUE__V73*/ meltfptr[52] = slot;
	};
	;
      }
    else
      {				/*^cond.else */

  /*_.REFERENCED_VALUE__V73*/ meltfptr[52] = NULL;;
      }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:6235:/ cond");
    /*cond */ if ( /*_.REFERENCED_VALUE__V73*/ meltfptr[52])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-outobj.melt:6236:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^apply */
	  /*apply */
	  {
	    /*_.RUNERRORHDLR__V75*/ meltfptr[55] =
	      melt_apply ((meltclosure_ptr_t)
			  ( /*_.RUNERRORHDLR__V36*/ meltfptr[32]),
			  (melt_ptr_t) (( /*!konst_19 */ meltfrout->
					 tabval[19])), (""),
			  (union meltparam_un *) 0, "",
			  (union meltparam_un *) 0);
	  }
	  ;
	  MELT_LOCATION ("warmelt-outobj.melt:6237:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^quasiblock */


	  /*_.RETVAL___V1*/ meltfptr[0] = ( /*nil */ NULL);;
	  MELT_LOCATION ("warmelt-outobj.melt:6237:/ putxtraresult");
	  if (!meltxrestab_ || !meltxresdescr_)
	    goto labend_rout;
	  if (meltxresdescr_[0] != MELTBPAR_PTR)
	    goto labend_rout;
	  if (meltxrestab_[0].meltbp_aptr)
	    *(meltxrestab_[0].meltbp_aptr) = (melt_ptr_t) (( /*nil */ NULL));
	  ;
	  /*^finalreturn */
	  ;
	  /*finalret */ goto labend_rout;
	  MELT_LOCATION ("warmelt-outobj.melt:6235:/ quasiblock");


	  /*_.PROGN___V77*/ meltfptr[63] = /*_.RETURN___V76*/ meltfptr[59];;
	  /*^compute */
	  /*_.IF___V74*/ meltfptr[53] = /*_.PROGN___V77*/ meltfptr[63];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-outobj.melt:6235:/ clear");
	     /*clear *//*_.RUNERRORHDLR__V75*/ meltfptr[55] = 0;
	  /*^clear */
	     /*clear *//*_.RETURN___V76*/ meltfptr[59] = 0;
	  /*^clear */
	     /*clear *//*_.PROGN___V77*/ meltfptr[63] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

  /*_.IF___V74*/ meltfptr[53] = NULL;;
      }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:6238:/ quasiblock");


    MELT_LOCATION ("warmelt-outobj.melt:6239:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[3];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.ENV__V3*/ meltfptr[2];
      /*^apply.arg */
      argtab[1].meltbp_aptr =
	(melt_ptr_t *) & ( /*!MACROEXPAND_1 */ meltfrout->tabval[21]);
      /*^apply.arg */
      argtab[2].meltbp_aptr = (melt_ptr_t *) & /*_.MODCTX__V49*/ meltfptr[48];
      /*_.XLIST__V79*/ meltfptr[55] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!MACROEXPAND_TOPLEVEL_LIST */ meltfrout->
		      tabval[20])),
		    (melt_ptr_t) ( /*_.LSEXP__V68*/ meltfptr[64]),
		    (MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_PTR ""),
		    argtab, "", (union meltparam_un *) 0);
    }
    ;
 /*_#LENXLIST__L25*/ meltfnum[12] =
      (melt_list_length ((melt_ptr_t) ( /*_.XLIST__V79*/ meltfptr[55])));;
    MELT_LOCATION ("warmelt-outobj.melt:6243:/ quasiblock");


    /*^newclosure */
		 /*newclosure *//*_.LAMBDA___V81*/ meltfptr[63] =
      (melt_ptr_t)
      meltgc_new_closure ((meltobject_ptr_t)
			  (((melt_ptr_t) (MELT_PREDEF (DISCR_CLOSURE)))),
			  (meltroutine_ptr_t) (( /*!konst_31 */ meltfrout->
						tabval[31])), (1));
    ;
    /*^putclosedv */
    /*putclosv */
    melt_assertmsg ("putclosv checkclo",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.LAMBDA___V81*/ meltfptr[63])) ==
		    MELTOBMAG_CLOSURE);
    melt_assertmsg ("putclosv checkoff", 0 >= 0
		    && 0 <
		    melt_closure_size ((melt_ptr_t)
				       ( /*_.LAMBDA___V81*/ meltfptr[63])));
    ((meltclosure_ptr_t) /*_.LAMBDA___V81*/ meltfptr[63])->tabval[0] =
      (melt_ptr_t) ( /*_.REFNORMLIST__V29*/ meltfptr[28]);
    ;
    /*_.NORMALEXTEND__V80*/ meltfptr[59] = /*_.LAMBDA___V81*/ meltfptr[63];;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:6323:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L26*/ meltfnum[13] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-outobj.melt:6323:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L26*/ meltfnum[13])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L27*/ meltfnum[17] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-outobj.melt:6323:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long =
		/*_#THE_MELTCALLCOUNT__L27*/ meltfnum[17];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-outobj.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 6323;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring =
		"translate_run_melt_expressions xlist=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.XLIST__V79*/ meltfptr[55];
	      /*_.MELT_DEBUG_FUN__V84*/ meltfptr[83] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V83*/ meltfptr[82] =
	      /*_.MELT_DEBUG_FUN__V84*/ meltfptr[83];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-outobj.melt:6323:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L27*/ meltfnum[17] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V84*/ meltfptr[83] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V83*/ meltfptr[82] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-outobj.melt:6323:/ quasiblock");


      /*_.PROGN___V85*/ meltfptr[83] = /*_.IF___V83*/ meltfptr[82];;
      /*^compute */
      /*_.IFCPP___V82*/ meltfptr[81] = /*_.PROGN___V85*/ meltfptr[83];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:6323:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L26*/ meltfnum[13] = 0;
      /*^clear */
	     /*clear *//*_.IF___V83*/ meltfptr[82] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V85*/ meltfptr[83] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V82*/ meltfptr[81] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-outobj.melt:6324:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^cond */
    /*cond */ if (
		   /*ifisa */
		   melt_is_instance_of ((melt_ptr_t)
					( /*_.REFERR__V27*/ meltfptr[20]),
					(melt_ptr_t) (( /*!CLASS_REFERENCE */
						       meltfrout->tabval[2])))
      )				/*then */
      {
	/*^cond.then */
	/*^getslot */
	{
	  melt_ptr_t slot = NULL, obj = NULL;
	  obj = (melt_ptr_t) ( /*_.REFERR__V27*/ meltfptr[20]) /*=obj*/ ;
	  melt_object_get_field (slot, obj, 0, "REFERENCED_VALUE");
   /*_.REFERENCED_VALUE__V86*/ meltfptr[82] = slot;
	};
	;
      }
    else
      {				/*^cond.else */

  /*_.REFERENCED_VALUE__V86*/ meltfptr[82] = NULL;;
      }
    ;
    /*^compute */
 /*_#NOTNULL__L28*/ meltfnum[18] =
      (( /*_.REFERENCED_VALUE__V86*/ meltfptr[82]) != NULL);;
    MELT_LOCATION ("warmelt-outobj.melt:6324:/ cond");
    /*cond */ if ( /*_#NOTNULL__L28*/ meltfnum[18])	/*then */
      {
	/*^cond.then */
	/*_#OR___L29*/ meltfnum[17] = /*_#NOTNULL__L28*/ meltfnum[18];;
      }
    else
      {
	MELT_LOCATION ("warmelt-outobj.melt:6324:/ cond.else");

	/*^block */
	/*anyblock */
	{

   /*_#MELT_ERROR_COUNTER__L30*/ meltfnum[13] =
	    melt_error_counter;;
	  /*^compute */
   /*_#I__L31*/ meltfnum[30] =
	    (( /*_#MELT_ERROR_COUNTER__L30*/ meltfnum[13]) >
	     ( /*_#STARTERRCOUNT__L12*/ meltfnum[5]));;
	  /*^compute */
	  /*_#OR___L29*/ meltfnum[17] = /*_#I__L31*/ meltfnum[30];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-outobj.melt:6324:/ clear");
	     /*clear *//*_#MELT_ERROR_COUNTER__L30*/ meltfnum[13] = 0;
	  /*^clear */
	     /*clear *//*_#I__L31*/ meltfnum[30] = 0;
	}
	;
      }
    ;
    /*^cond */
    /*cond */ if ( /*_#OR___L29*/ meltfnum[17])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-outobj.melt:6325:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^apply */
	  /*apply */
	  {
	    /*_.RUNERRORHDLR__V88*/ meltfptr[87] =
	      melt_apply ((meltclosure_ptr_t)
			  ( /*_.RUNERRORHDLR__V36*/ meltfptr[32]),
			  (melt_ptr_t) (( /*!konst_32 */ meltfrout->
					 tabval[32])), (""),
			  (union meltparam_un *) 0, "",
			  (union meltparam_un *) 0);
	  }
	  ;
	  MELT_LOCATION ("warmelt-outobj.melt:6326:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^quasiblock */


	  /*_.RETVAL___V1*/ meltfptr[0] = ( /*nil */ NULL);;
	  MELT_LOCATION ("warmelt-outobj.melt:6326:/ putxtraresult");
	  if (!meltxrestab_ || !meltxresdescr_)
	    goto labend_rout;
	  if (meltxresdescr_[0] != MELTBPAR_PTR)
	    goto labend_rout;
	  if (meltxrestab_[0].meltbp_aptr)
	    *(meltxrestab_[0].meltbp_aptr) = (melt_ptr_t) (( /*nil */ NULL));
	  ;
	  /*^finalreturn */
	  ;
	  /*finalret */ goto labend_rout;
	  MELT_LOCATION ("warmelt-outobj.melt:6324:/ quasiblock");


	  /*_.PROGN___V90*/ meltfptr[89] = /*_.RETURN___V89*/ meltfptr[88];;
	  /*^compute */
	  /*_.IF___V87*/ meltfptr[83] = /*_.PROGN___V90*/ meltfptr[89];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-outobj.melt:6324:/ clear");
	     /*clear *//*_.RUNERRORHDLR__V88*/ meltfptr[87] = 0;
	  /*^clear */
	     /*clear *//*_.RETURN___V89*/ meltfptr[88] = 0;
	  /*^clear */
	     /*clear *//*_.PROGN___V90*/ meltfptr[89] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

  /*_.IF___V87*/ meltfptr[83] = NULL;;
      }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:6327:/ quasiblock");


 /*_.BASENAME__V92*/ meltfptr[88] =
      (meltgc_new_string_tempname_suffixed
       ((meltobject_ptr_t) (( /*!DISCR_STRING */ meltfrout->tabval[33])),
	melt_string_str ((melt_ptr_t) ( /*_.NAKEDBASNAM__V26*/ meltfptr[19])),
	("_eXt")));;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:6329:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L32*/ meltfnum[13] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-outobj.melt:6329:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L32*/ meltfnum[13])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L33*/ meltfnum[30] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-outobj.melt:6329:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[7];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long =
		/*_#THE_MELTCALLCOUNT__L33*/ meltfnum[30];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-outobj.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 6329;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring =
		"translate_run_melt_expressions basename=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.BASENAME__V92*/ meltfptr[88];
	      /*^apply.arg */
	      argtab[5].meltbp_cstring = " refnormlist=";
	      /*^apply.arg */
	      argtab[6].meltbp_aptr =
		(melt_ptr_t *) & /*_.REFNORMLIST__V29*/ meltfptr[28];
	      /*_.MELT_DEBUG_FUN__V95*/ meltfptr[94] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V94*/ meltfptr[93] =
	      /*_.MELT_DEBUG_FUN__V95*/ meltfptr[94];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-outobj.melt:6329:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L33*/ meltfnum[30] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V95*/ meltfptr[94] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V94*/ meltfptr[93] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-outobj.melt:6329:/ quasiblock");


      /*_.PROGN___V96*/ meltfptr[94] = /*_.IF___V94*/ meltfptr[93];;
      /*^compute */
      /*_.IFCPP___V93*/ meltfptr[89] = /*_.PROGN___V96*/ meltfptr[94];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:6329:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L32*/ meltfnum[13] = 0;
      /*^clear */
	     /*clear *//*_.IF___V94*/ meltfptr[93] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V96*/ meltfptr[94] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V93*/ meltfptr[89] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-outobj.melt:6332:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[6];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & /*_.BASENAME__V92*/ meltfptr[88];
      /*^apply.arg */
      argtab[1].meltbp_aptr = (melt_ptr_t *) & /*_.MODCTX__V49*/ meltfptr[48];
      /*^apply.arg */
      argtab[2].meltbp_aptr = (melt_ptr_t *) & /*_.NCX__V51*/ meltfptr[50];
      /*^apply.arg */
      argtab[3].meltbp_aptr = (melt_ptr_t *) & /*_.ENV__V3*/ meltfptr[2];
      /*^apply.arg */
      argtab[4].meltbp_aptr =
	(melt_ptr_t *) & /*_.NORMALEXTEND__V80*/ meltfptr[59];
      /*^apply.arg */
      argtab[5].meltbp_aptr =
	(melt_ptr_t *) & ( /*!COMPILE2OBJ_INITEXTENDPROC */ meltfrout->
			  tabval[35]);
      /*_.TRANSLATE_MACROEXPANDED_LIST__V97*/ meltfptr[93] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!TRANSLATE_MACROEXPANDED_LIST */ meltfrout->
		      tabval[34])),
		    (melt_ptr_t) ( /*_.XLIST__V79*/ meltfptr[55]),
		    (MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_PTR
		     MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_PTR ""),
		    argtab, "", (union meltparam_un *) 0);
    }
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:6334:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L34*/ meltfnum[30] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-outobj.melt:6334:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L34*/ meltfnum[30])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L35*/ meltfnum[13] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-outobj.melt:6334:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[8];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long =
		/*_#THE_MELTCALLCOUNT__L35*/ meltfnum[13];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-outobj.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 6334;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring =
		"translate_run_melt_expressions after translation";
	      /*^apply.arg */
	      argtab[4].meltbp_cstring = " refnormlist=";
	      /*^apply.arg */
	      argtab[5].meltbp_aptr =
		(melt_ptr_t *) & /*_.REFNORMLIST__V29*/ meltfptr[28];
	      /*^apply.arg */
	      argtab[6].meltbp_cstring = " to nakedbasnam=";
	      /*^apply.arg */
	      argtab[7].meltbp_aptr =
		(melt_ptr_t *) & /*_.NAKEDBASNAM__V26*/ meltfptr[19];
	      /*_.MELT_DEBUG_FUN__V100*/ meltfptr[99] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_CSTRING MELTBPARSTR_PTR
			     MELTBPARSTR_CSTRING MELTBPARSTR_PTR ""), argtab,
			    "", (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V99*/ meltfptr[98] =
	      /*_.MELT_DEBUG_FUN__V100*/ meltfptr[99];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-outobj.melt:6334:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L35*/ meltfnum[13] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V100*/ meltfptr[99] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V99*/ meltfptr[98] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-outobj.melt:6334:/ quasiblock");


      /*_.PROGN___V101*/ meltfptr[99] = /*_.IF___V99*/ meltfptr[98];;
      /*^compute */
      /*_.IFCPP___V98*/ meltfptr[94] = /*_.PROGN___V101*/ meltfptr[99];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:6334:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L34*/ meltfnum[30] = 0;
      /*^clear */
	     /*clear *//*_.IF___V99*/ meltfptr[98] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V101*/ meltfptr[99] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V98*/ meltfptr[94] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-outobj.melt:6337:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^cond */
    /*cond */ if (
		   /*ifisa */
		   melt_is_instance_of ((melt_ptr_t)
					( /*_.REFERR__V27*/ meltfptr[20]),
					(melt_ptr_t) (( /*!CLASS_REFERENCE */
						       meltfrout->tabval[2])))
      )				/*then */
      {
	/*^cond.then */
	/*^getslot */
	{
	  melt_ptr_t slot = NULL, obj = NULL;
	  obj = (melt_ptr_t) ( /*_.REFERR__V27*/ meltfptr[20]) /*=obj*/ ;
	  melt_object_get_field (slot, obj, 0, "REFERENCED_VALUE");
   /*_.REFERENCED_VALUE__V102*/ meltfptr[98] = slot;
	};
	;
      }
    else
      {				/*^cond.else */

  /*_.REFERENCED_VALUE__V102*/ meltfptr[98] = NULL;;
      }
    ;
    /*^compute */
 /*_#NOTNULL__L36*/ meltfnum[13] =
      (( /*_.REFERENCED_VALUE__V102*/ meltfptr[98]) != NULL);;
    MELT_LOCATION ("warmelt-outobj.melt:6337:/ cond");
    /*cond */ if ( /*_#NOTNULL__L36*/ meltfnum[13])	/*then */
      {
	/*^cond.then */
	/*_#OR___L37*/ meltfnum[30] = /*_#NOTNULL__L36*/ meltfnum[13];;
      }
    else
      {
	MELT_LOCATION ("warmelt-outobj.melt:6337:/ cond.else");

	/*^block */
	/*anyblock */
	{

   /*_#MELT_ERROR_COUNTER__L38*/ meltfnum[37] =
	    melt_error_counter;;
	  /*^compute */
   /*_#I__L39*/ meltfnum[38] =
	    (( /*_#MELT_ERROR_COUNTER__L38*/ meltfnum[37]) >
	     ( /*_#STARTERRCOUNT__L12*/ meltfnum[5]));;
	  /*^compute */
	  /*_#OR___L37*/ meltfnum[30] = /*_#I__L39*/ meltfnum[38];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-outobj.melt:6337:/ clear");
	     /*clear *//*_#MELT_ERROR_COUNTER__L38*/ meltfnum[37] = 0;
	  /*^clear */
	     /*clear *//*_#I__L39*/ meltfnum[38] = 0;
	}
	;
      }
    ;
    /*^cond */
    /*cond */ if ( /*_#OR___L37*/ meltfnum[30])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-outobj.melt:6338:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^apply */
	  /*apply */
	  {
	    /*_.RUNERRORHDLR__V104*/ meltfptr[103] =
	      melt_apply ((meltclosure_ptr_t)
			  ( /*_.RUNERRORHDLR__V36*/ meltfptr[32]),
			  (melt_ptr_t) (( /*!konst_36 */ meltfrout->
					 tabval[36])), (""),
			  (union meltparam_un *) 0, "",
			  (union meltparam_un *) 0);
	  }
	  ;
	  MELT_LOCATION ("warmelt-outobj.melt:6339:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^quasiblock */


	  /*_.RETVAL___V1*/ meltfptr[0] = ( /*nil */ NULL);;
	  MELT_LOCATION ("warmelt-outobj.melt:6339:/ putxtraresult");
	  if (!meltxrestab_ || !meltxresdescr_)
	    goto labend_rout;
	  if (meltxresdescr_[0] != MELTBPAR_PTR)
	    goto labend_rout;
	  if (meltxrestab_[0].meltbp_aptr)
	    *(meltxrestab_[0].meltbp_aptr) = (melt_ptr_t) (( /*nil */ NULL));
	  ;
	  /*^finalreturn */
	  ;
	  /*finalret */ goto labend_rout;
	  MELT_LOCATION ("warmelt-outobj.melt:6337:/ quasiblock");


	  /*_.PROGN___V106*/ meltfptr[105] =
	    /*_.RETURN___V105*/ meltfptr[104];;
	  /*^compute */
	  /*_.IF___V103*/ meltfptr[99] = /*_.PROGN___V106*/ meltfptr[105];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-outobj.melt:6337:/ clear");
	     /*clear *//*_.RUNERRORHDLR__V104*/ meltfptr[103] = 0;
	  /*^clear */
	     /*clear *//*_.RETURN___V105*/ meltfptr[104] = 0;
	  /*^clear */
	     /*clear *//*_.PROGN___V106*/ meltfptr[105] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

  /*_.IF___V103*/ meltfptr[99] = NULL;;
      }
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:6341:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L40*/ meltfnum[37] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-outobj.melt:6341:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L40*/ meltfnum[37])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L41*/ meltfnum[38] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-outobj.melt:6341:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[7];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long =
		/*_#THE_MELTCALLCOUNT__L41*/ meltfnum[38];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-outobj.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 6341;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring =
		"translate_run_melt_expressions before compiling quicklybuilt flavor\
 of basename=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.BASENAME__V92*/ meltfptr[88];
	      /*^apply.arg */
	      argtab[5].meltbp_cstring = "\n modctx=";
	      /*^apply.arg */
	      argtab[6].meltbp_aptr =
		(melt_ptr_t *) & /*_.MODCTX__V49*/ meltfptr[48];
	      /*_.MELT_DEBUG_FUN__V109*/ meltfptr[105] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V108*/ meltfptr[104] =
	      /*_.MELT_DEBUG_FUN__V109*/ meltfptr[105];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-outobj.melt:6341:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L41*/ meltfnum[38] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V109*/ meltfptr[105] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V108*/ meltfptr[104] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-outobj.melt:6341:/ quasiblock");


      /*_.PROGN___V110*/ meltfptr[105] = /*_.IF___V108*/ meltfptr[104];;
      /*^compute */
      /*_.IFCPP___V107*/ meltfptr[103] = /*_.PROGN___V110*/ meltfptr[105];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:6341:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L40*/ meltfnum[37] = 0;
      /*^clear */
	     /*clear *//*_.IF___V108*/ meltfptr[104] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V110*/ meltfptr[105] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V107*/ meltfptr[103] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:6342:/ locexp");
      /*generate_flavored_melt_module */
	melt_compile_source (melt_string_str
			     ((melt_ptr_t) /*_.BASENAME__V92*/ meltfptr[88]),
			     melt_string_str ((melt_ptr_t) /*_.BASENAME__V92*/
					      meltfptr[88]),
			     NULL, melt_string_str ((melt_ptr_t) ( /*!konst_37 */ meltfrout->tabval[37])));	/*generate_flavored_melt_module */
      ;
    }
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:6343:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L42*/ meltfnum[38] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-outobj.melt:6343:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L42*/ meltfnum[38])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L43*/ meltfnum[37] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-outobj.melt:6343:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long =
		/*_#THE_MELTCALLCOUNT__L43*/ meltfnum[37];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-outobj.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 6343;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring =
		"translate_run_melt_expressions after compiling runextend flavor of\
 basename=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.BASENAME__V92*/ meltfptr[88];
	      /*_.MELT_DEBUG_FUN__V113*/ meltfptr[112] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V112*/ meltfptr[105] =
	      /*_.MELT_DEBUG_FUN__V113*/ meltfptr[112];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-outobj.melt:6343:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L43*/ meltfnum[37] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V113*/ meltfptr[112] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V112*/ meltfptr[105] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-outobj.melt:6343:/ quasiblock");


      /*_.PROGN___V114*/ meltfptr[112] = /*_.IF___V112*/ meltfptr[105];;
      /*^compute */
      /*_.IFCPP___V111*/ meltfptr[104] = /*_.PROGN___V114*/ meltfptr[112];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:6343:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L42*/ meltfnum[38] = 0;
      /*^clear */
	     /*clear *//*_.IF___V112*/ meltfptr[105] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V114*/ meltfptr[112] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V111*/ meltfptr[104] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:6345:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_LIST__L44*/ meltfnum[37] =
	(melt_magic_discr ((melt_ptr_t) ( /*_.LITVALIST__V38*/ meltfptr[37]))
	 == MELTOBMAG_LIST);;
      MELT_LOCATION ("warmelt-outobj.melt:6345:/ cond");
      /*cond */ if ( /*_#IS_LIST__L44*/ meltfnum[37])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V116*/ meltfptr[112] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-outobj.melt:6345:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check litvalist"),
				  ("warmelt-outobj.melt")
				  ? ("warmelt-outobj.melt") : __FILE__,
				  (6345) ? (6345) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V116*/ meltfptr[112] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V115*/ meltfptr[105] = /*_.IFELSE___V116*/ meltfptr[112];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:6345:/ clear");
	     /*clear *//*_#IS_LIST__L44*/ meltfnum[37] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V116*/ meltfptr[112] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V115*/ meltfptr[105] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:6346:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L45*/ meltfnum[38] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-outobj.melt:6346:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L45*/ meltfnum[38])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L46*/ meltfnum[37] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-outobj.melt:6346:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long =
		/*_#THE_MELTCALLCOUNT__L46*/ meltfnum[37];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-outobj.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 6346;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring =
		"translate_run_melt_expressions after generation litvalist=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.LITVALIST__V38*/ meltfptr[37];
	      /*_.MELT_DEBUG_FUN__V119*/ meltfptr[118] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V118*/ meltfptr[117] =
	      /*_.MELT_DEBUG_FUN__V119*/ meltfptr[118];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-outobj.melt:6346:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L46*/ meltfnum[37] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V119*/ meltfptr[118] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V118*/ meltfptr[117] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-outobj.melt:6346:/ quasiblock");


      /*_.PROGN___V120*/ meltfptr[118] = /*_.IF___V118*/ meltfptr[117];;
      /*^compute */
      /*_.IFCPP___V117*/ meltfptr[112] = /*_.PROGN___V120*/ meltfptr[118];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:6346:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L45*/ meltfnum[38] = 0;
      /*^clear */
	     /*clear *//*_.IF___V118*/ meltfptr[117] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V120*/ meltfptr[118] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V117*/ meltfptr[112] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-outobj.melt:6347:/ quasiblock");


 /*_#NBLITVAL__L47*/ meltfnum[37] =
      (melt_list_length ((melt_ptr_t) ( /*_.LITVALIST__V38*/ meltfptr[37])));;
    /*^compute */
 /*_.TUPLITVAL__V122*/ meltfptr[118] =
      (meltgc_new_multiple
       ((meltobject_ptr_t) (( /*!DISCR_MULTIPLE */ meltfrout->tabval[38])),
	( /*_#NBLITVAL__L47*/ meltfnum[37])));;
    /*^compute */
 /*_#IX__L48*/ meltfnum[38] = 0;;
    MELT_LOCATION ("warmelt-outobj.melt:6350:/ cond");
    /*cond */ if (
		   /*ifisa */
		   melt_is_instance_of ((melt_ptr_t)
					( /*_.MODCTX__V49*/ meltfptr[48]),
					(melt_ptr_t) (( /*!CLASS_MODULE_CONTEXT */ meltfrout->tabval[39])))
      )				/*then */
      {
	/*^cond.then */
	/*^getslot */
	{
	  melt_ptr_t slot = NULL, obj = NULL;
	  obj = (melt_ptr_t) ( /*_.MODCTX__V49*/ meltfptr[48]) /*=obj*/ ;
	  melt_object_get_field (slot, obj, 5, "MOCX_FILETUPLE");
   /*_.FILTUP__V123*/ meltfptr[122] = slot;
	};
	;
      }
    else
      {				/*^cond.else */

  /*_.FILTUP__V123*/ meltfptr[122] = NULL;;
      }
    ;
    /*^compute */
 /*_#NBFILE__L49*/ meltfnum[48] =
      (melt_multiple_length
       ((melt_ptr_t) ( /*_.FILTUP__V123*/ meltfptr[122])));;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:6353:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L50*/ meltfnum[49] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-outobj.melt:6353:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L50*/ meltfnum[49])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L51*/ meltfnum[50] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-outobj.melt:6353:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long =
		/*_#THE_MELTCALLCOUNT__L51*/ meltfnum[50];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-outobj.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 6353;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring =
		"translate_run_melt_expressions filtup=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.FILTUP__V123*/ meltfptr[122];
	      /*_.MELT_DEBUG_FUN__V126*/ meltfptr[125] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V125*/ meltfptr[124] =
	      /*_.MELT_DEBUG_FUN__V126*/ meltfptr[125];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-outobj.melt:6353:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L51*/ meltfnum[50] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V126*/ meltfptr[125] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V125*/ meltfptr[124] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-outobj.melt:6353:/ quasiblock");


      /*_.PROGN___V127*/ meltfptr[125] = /*_.IF___V125*/ meltfptr[124];;
      /*^compute */
      /*_.IFCPP___V124*/ meltfptr[123] = /*_.PROGN___V127*/ meltfptr[125];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:6353:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L50*/ meltfnum[49] = 0;
      /*^clear */
	     /*clear *//*_.IF___V125*/ meltfptr[124] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V127*/ meltfptr[125] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V124*/ meltfptr[123] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    /*citerblock FOREACH_IN_LIST */
    {
      /* start foreach_in_list meltcit3__EACHLIST */
      for ( /*_.CURPAIR__V128*/ meltfptr[124] =
	   melt_list_first ((melt_ptr_t) /*_.LITVALIST__V38*/ meltfptr[37]);
	   melt_magic_discr ((melt_ptr_t) /*_.CURPAIR__V128*/ meltfptr[124])
	   == MELTOBMAG_PAIR;
	   /*_.CURPAIR__V128*/ meltfptr[124] =
	   melt_pair_tail ((melt_ptr_t) /*_.CURPAIR__V128*/ meltfptr[124]))
	{
	  /*_.CURLITVAL__V129*/ meltfptr[125] =
	    melt_pair_head ((melt_ptr_t) /*_.CURPAIR__V128*/ meltfptr[124]);



#if MELT_HAVE_DEBUG
	  MELT_LOCATION ("warmelt-outobj.melt:6357:/ cppif.then");
	  /*^block */
	  /*anyblock */
	  {


	    {
	      /*^locexp */
	      /*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	      melt_dbgcounter++;
#endif
	      ;
	    }
	    ;
	    /*^checksignal */
	    MELT_CHECK_SIGNAL ();
	    ;
    /*_#MELT_NEED_DBG__L52*/ meltfnum[50] =
	      /*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	      ( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	      0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	      ;;
	    MELT_LOCATION ("warmelt-outobj.melt:6357:/ cond");
	    /*cond */ if ( /*_#MELT_NEED_DBG__L52*/ meltfnum[50])	/*then */
	      {
		/*^cond.then */
		/*^block */
		/*anyblock */
		{

      /*_#THE_MELTCALLCOUNT__L53*/ meltfnum[49] =
#ifdef meltcallcount
		    meltcallcount	/* the_meltcallcount */
#else
		    0L
#endif /* meltcallcount the_meltcallcount */
		    ;;
		  MELT_LOCATION ("warmelt-outobj.melt:6357:/ checksignal");
		  MELT_CHECK_SIGNAL ();
		  ;
		  /*^apply */
		  /*apply */
		  {
		    union meltparam_un argtab[7];
		    memset (&argtab, 0, sizeof (argtab));
		    /*^apply.arg */
		    argtab[0].meltbp_long =
		      /*_#THE_MELTCALLCOUNT__L53*/ meltfnum[49];
		    /*^apply.arg */
		    argtab[1].meltbp_cstring = "warmelt-outobj.melt";
		    /*^apply.arg */
		    argtab[2].meltbp_long = 6357;
		    /*^apply.arg */
		    argtab[3].meltbp_cstring =
		      "translate_run_melt_expressions ix=";
		    /*^apply.arg */
		    argtab[4].meltbp_long = /*_#IX__L48*/ meltfnum[38];
		    /*^apply.arg */
		    argtab[5].meltbp_cstring = " curlitval=";
		    /*^apply.arg */
		    argtab[6].meltbp_aptr =
		      (melt_ptr_t *) & /*_.CURLITVAL__V129*/ meltfptr[125];
		    /*_.MELT_DEBUG_FUN__V132*/ meltfptr[131] =
		      melt_apply ((meltclosure_ptr_t)
				  (( /*!MELT_DEBUG_FUN */ meltfrout->
				    tabval[0])),
				  (melt_ptr_t) (( /*nil */ NULL)),
				  (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_PTR ""), argtab, "",
				  (union meltparam_un *) 0);
		  }
		  ;
		  /*_.IF___V131*/ meltfptr[130] =
		    /*_.MELT_DEBUG_FUN__V132*/ meltfptr[131];;
		  /*epilog */

		  MELT_LOCATION ("warmelt-outobj.melt:6357:/ clear");
		/*clear *//*_#THE_MELTCALLCOUNT__L53*/ meltfnum[49] = 0;
		  /*^clear */
		/*clear *//*_.MELT_DEBUG_FUN__V132*/ meltfptr[131] = 0;
		}
		;
	      }
	    else
	      {			/*^cond.else */

     /*_.IF___V131*/ meltfptr[130] = NULL;;
	      }
	    ;
	    MELT_LOCATION ("warmelt-outobj.melt:6357:/ quasiblock");


	    /*_.PROGN___V133*/ meltfptr[131] = /*_.IF___V131*/ meltfptr[130];;
	    /*^compute */
	    /*_.IFCPP___V130*/ meltfptr[129] =
	      /*_.PROGN___V133*/ meltfptr[131];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-outobj.melt:6357:/ clear");
	      /*clear *//*_#MELT_NEED_DBG__L52*/ meltfnum[50] = 0;
	    /*^clear */
	      /*clear *//*_.IF___V131*/ meltfptr[130] = 0;
	    /*^clear */
	      /*clear *//*_.PROGN___V133*/ meltfptr[131] = 0;
	  }

#else /*MELT_HAVE_DEBUG */
	  /*^cppif.else */
	  /*_.IFCPP___V130*/ meltfptr[129] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
	  ;
	  MELT_LOCATION ("warmelt-outobj.melt:6359:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^cond */
	  /*cond */ if ( /*_.CURLITVAL__V129*/ meltfptr[125])	/*then */
	    {
	      /*^cond.then */
	      /*^block */
	      /*anyblock */
	      {


#if MELT_HAVE_DEBUG
		MELT_LOCATION ("warmelt-outobj.melt:6360:/ cppif.then");
		/*^block */
		/*anyblock */
		{

		  /*^checksignal */
		  MELT_CHECK_SIGNAL ();
		  ;
      /*_#IS_A__L54*/ meltfnum[49] =
		    melt_is_instance_of ((melt_ptr_t)
					 ( /*_.CURLITVAL__V129*/
					  meltfptr[125]),
					 (melt_ptr_t) (( /*!CLASS_LITERAL_VALUE */ meltfrout->tabval[40])));;
		  MELT_LOCATION ("warmelt-outobj.melt:6360:/ cond");
		  /*cond */ if ( /*_#IS_A__L54*/ meltfnum[49])	/*then */
		    {
		      /*^cond.then */
		      /*_.IFELSE___V135*/ meltfptr[131] = ( /*nil */ NULL);;
		    }
		  else
		    {
		      MELT_LOCATION ("warmelt-outobj.melt:6360:/ cond.else");

		      /*^block */
		      /*anyblock */
		      {




			{
			  /*^locexp */
			  melt_assert_failed (("check curlitval"),
					      ("warmelt-outobj.melt")
					      ? ("warmelt-outobj.melt") :
					      __FILE__,
					      (6360) ? (6360) : __LINE__,
					      __FUNCTION__);
			  ;
			}
			;
		  /*clear *//*_.IFELSE___V135*/ meltfptr[131] = 0;
			/*epilog */
		      }
		      ;
		    }
		  ;
		  /*^compute */
		  /*_.IFCPP___V134*/ meltfptr[130] =
		    /*_.IFELSE___V135*/ meltfptr[131];;
		  /*epilog */

		  MELT_LOCATION ("warmelt-outobj.melt:6360:/ clear");
		/*clear *//*_#IS_A__L54*/ meltfnum[49] = 0;
		  /*^clear */
		/*clear *//*_.IFELSE___V135*/ meltfptr[131] = 0;
		}

#else /*MELT_HAVE_DEBUG */
		/*^cppif.else */
		/*_.IFCPP___V134*/ meltfptr[130] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
		;
		MELT_LOCATION ("warmelt-outobj.melt:6362:/ quasiblock");


		/*^cond */
		/*cond */ if (
			       /*ifisa */
			       melt_is_instance_of ((melt_ptr_t)
						    ( /*_.CURLITVAL__V129*/
						     meltfptr[125]),
						    (melt_ptr_t) (( /*!CLASS_LITERAL_VALUE */ meltfrout->tabval[40])))
		  )		/*then */
		  {
		    /*^cond.then */
		    /*^getslot */
		    {
		      melt_ptr_t slot = NULL, obj = NULL;
		      obj =
			(melt_ptr_t) ( /*_.CURLITVAL__V129*/ meltfptr[125])
			/*=obj*/ ;
		      melt_object_get_field (slot, obj, 1, "LITV_VALUE");
      /*_.VAL__V136*/ meltfptr[131] = slot;
		    };
		    ;
		  }
		else
		  {		/*^cond.else */

     /*_.VAL__V136*/ meltfptr[131] = NULL;;
		  }
		;
		MELT_LOCATION ("warmelt-outobj.melt:6363:/ cond");
		/*cond */ if (
			       /*ifisa */
			       melt_is_instance_of ((melt_ptr_t)
						    ( /*_.CURLITVAL__V129*/
						     meltfptr[125]),
						    (melt_ptr_t) (( /*!CLASS_LITERAL_VALUE */ meltfrout->tabval[40])))
		  )		/*then */
		  {
		    /*^cond.then */
		    /*^getslot */
		    {
		      melt_ptr_t slot = NULL, obj = NULL;
		      obj =
			(melt_ptr_t) ( /*_.CURLITVAL__V129*/ meltfptr[125])
			/*=obj*/ ;
		      melt_object_get_field (slot, obj, 2, "LITV_RANK");
      /*_.BXRK__V137*/ meltfptr[136] = slot;
		    };
		    ;
		  }
		else
		  {		/*^cond.else */

     /*_.BXRK__V137*/ meltfptr[136] = NULL;;
		  }
		;

#if MELT_HAVE_DEBUG
		MELT_LOCATION ("warmelt-outobj.melt:6365:/ cppif.then");
		/*^block */
		/*anyblock */
		{

		  /*^checksignal */
		  MELT_CHECK_SIGNAL ();
		  ;
      /*_#GET_INT__L55*/ meltfnum[50] =
		    (melt_get_int
		     ((melt_ptr_t) ( /*_.BXRK__V137*/ meltfptr[136])));;
		  /*^compute */
      /*_#I__L56*/ meltfnum[49] =
		    (( /*_#GET_INT__L55*/ meltfnum[50]) ==
		     ( /*_#IX__L48*/ meltfnum[38]));;
		  MELT_LOCATION ("warmelt-outobj.melt:6365:/ cond");
		  /*cond */ if ( /*_#I__L56*/ meltfnum[49])	/*then */
		    {
		      /*^cond.then */
		      /*_.IFELSE___V139*/ meltfptr[138] = ( /*nil */ NULL);;
		    }
		  else
		    {
		      MELT_LOCATION ("warmelt-outobj.melt:6365:/ cond.else");

		      /*^block */
		      /*anyblock */
		      {




			{
			  /*^locexp */
			  melt_assert_failed (("check bxrk"),
					      ("warmelt-outobj.melt")
					      ? ("warmelt-outobj.melt") :
					      __FILE__,
					      (6365) ? (6365) : __LINE__,
					      __FUNCTION__);
			  ;
			}
			;
		  /*clear *//*_.IFELSE___V139*/ meltfptr[138] = 0;
			/*epilog */
		      }
		      ;
		    }
		  ;
		  /*^compute */
		  /*_.IFCPP___V138*/ meltfptr[137] =
		    /*_.IFELSE___V139*/ meltfptr[138];;
		  /*epilog */

		  MELT_LOCATION ("warmelt-outobj.melt:6365:/ clear");
		/*clear *//*_#GET_INT__L55*/ meltfnum[50] = 0;
		  /*^clear */
		/*clear *//*_#I__L56*/ meltfnum[49] = 0;
		  /*^clear */
		/*clear *//*_.IFELSE___V139*/ meltfptr[138] = 0;
		}

#else /*MELT_HAVE_DEBUG */
		/*^cppif.else */
		/*_.IFCPP___V138*/ meltfptr[137] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
		;

		{
		  MELT_LOCATION ("warmelt-outobj.melt:6366:/ locexp");
		  meltgc_multiple_put_nth ((melt_ptr_t)
					   ( /*_.TUPLITVAL__V122*/
					    meltfptr[118]),
					   ( /*_#IX__L48*/ meltfnum[38]),
					   (melt_ptr_t) ( /*_.VAL__V136*/
							 meltfptr[131]));
		}
		;

		MELT_LOCATION ("warmelt-outobj.melt:6362:/ clear");
	      /*clear *//*_.VAL__V136*/ meltfptr[131] = 0;
		/*^clear */
	      /*clear *//*_.BXRK__V137*/ meltfptr[136] = 0;
		/*^clear */
	      /*clear *//*_.IFCPP___V138*/ meltfptr[137] = 0;
		MELT_LOCATION ("warmelt-outobj.melt:6359:/ quasiblock");


		/*epilog */

		/*^clear */
	      /*clear *//*_.IFCPP___V134*/ meltfptr[130] = 0;
	      }
	      ;
	    }			/*noelse */
	  ;
  /*_#I__L57*/ meltfnum[50] =
	    (( /*_#IX__L48*/ meltfnum[38]) + (1));;
	  MELT_LOCATION ("warmelt-outobj.melt:6368:/ compute");
	  /*_#IX__L48*/ meltfnum[38] = /*_#SETQ___L58*/ meltfnum[49] =
	    /*_#I__L57*/ meltfnum[50];;
	}			/* end foreach_in_list meltcit3__EACHLIST */
     /*_.CURPAIR__V128*/ meltfptr[124] = NULL;
     /*_.CURLITVAL__V129*/ meltfptr[125] = NULL;


      /*citerepilog */

      MELT_LOCATION ("warmelt-outobj.melt:6354:/ clear");
	    /*clear *//*_.CURPAIR__V128*/ meltfptr[124] = 0;
      /*^clear */
	    /*clear *//*_.CURLITVAL__V129*/ meltfptr[125] = 0;
      /*^clear */
	    /*clear *//*_.IFCPP___V130*/ meltfptr[129] = 0;
      /*^clear */
	    /*clear *//*_#I__L57*/ meltfnum[50] = 0;
      /*^clear */
	    /*clear *//*_#SETQ___L58*/ meltfnum[49] = 0;
    }				/*endciterblock FOREACH_IN_LIST */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:6369:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L59*/ meltfnum[58] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-outobj.melt:6369:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L59*/ meltfnum[58])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L60*/ meltfnum[59] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-outobj.melt:6369:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long =
		/*_#THE_MELTCALLCOUNT__L60*/ meltfnum[59];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-outobj.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 6369;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring =
		"translate_run_melt_expressions tuplitval=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.TUPLITVAL__V122*/ meltfptr[118];
	      /*_.MELT_DEBUG_FUN__V142*/ meltfptr[136] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V141*/ meltfptr[131] =
	      /*_.MELT_DEBUG_FUN__V142*/ meltfptr[136];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-outobj.melt:6369:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L60*/ meltfnum[59] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V142*/ meltfptr[136] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V141*/ meltfptr[131] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-outobj.melt:6369:/ quasiblock");


      /*_.PROGN___V143*/ meltfptr[137] = /*_.IF___V141*/ meltfptr[131];;
      /*^compute */
      /*_.IFCPP___V140*/ meltfptr[138] = /*_.PROGN___V143*/ meltfptr[137];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:6369:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L59*/ meltfnum[58] = 0;
      /*^clear */
	     /*clear *//*_.IF___V141*/ meltfptr[131] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V143*/ meltfptr[137] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V140*/ meltfptr[138] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:6370:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L61*/ meltfnum[59] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-outobj.melt:6370:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L61*/ meltfnum[59])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L62*/ meltfnum[58] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-outobj.melt:6370:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[9];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long =
		/*_#THE_MELTCALLCOUNT__L62*/ meltfnum[58];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-outobj.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 6370;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring =
		"before melt_run_extension basename=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.BASENAME__V92*/ meltfptr[88];
	      /*^apply.arg */
	      argtab[5].meltbp_cstring = "\n* env=";
	      /*^apply.arg */
	      argtab[6].meltbp_aptr =
		(melt_ptr_t *) & /*_.ENV__V3*/ meltfptr[2];
	      /*^apply.arg */
	      argtab[7].meltbp_cstring = "\n* tuplitval=";
	      /*^apply.arg */
	      argtab[8].meltbp_aptr =
		(melt_ptr_t *) & /*_.TUPLITVAL__V122*/ meltfptr[118];
	      /*_.MELT_DEBUG_FUN__V146*/ meltfptr[131] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V145*/ meltfptr[136] =
	      /*_.MELT_DEBUG_FUN__V146*/ meltfptr[131];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-outobj.melt:6370:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L62*/ meltfnum[58] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V146*/ meltfptr[131] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V145*/ meltfptr[136] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-outobj.melt:6370:/ quasiblock");


      /*_.PROGN___V147*/ meltfptr[137] = /*_.IF___V145*/ meltfptr[136];;
      /*^compute */
      /*_.IFCPP___V144*/ meltfptr[130] = /*_.PROGN___V147*/ meltfptr[137];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:6370:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L61*/ meltfnum[59] = 0;
      /*^clear */
	     /*clear *//*_.IF___V145*/ meltfptr[136] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V147*/ meltfptr[137] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V144*/ meltfptr[130] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-outobj.melt:6373:/ quasiblock");


 /*_.RESULT__V149*/ meltfptr[136] =
      /* melt_run_extension */ meltgc_run_c_extension
      ((melt_ptr_t) /*_.BASENAME__V92*/ meltfptr[88],
       (melt_ptr_t) /*_.ENV__V3*/ meltfptr[2],
       (melt_ptr_t) /*_.TUPLITVAL__V122*/ meltfptr[118]);;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:6375:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L63*/ meltfnum[58] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-outobj.melt:6375:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L63*/ meltfnum[58])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L64*/ meltfnum[59] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-outobj.melt:6375:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[7];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long =
		/*_#THE_MELTCALLCOUNT__L64*/ meltfnum[59];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-outobj.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 6375;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "after melt_run_extension basename=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.BASENAME__V92*/ meltfptr[88];
	      /*^apply.arg */
	      argtab[5].meltbp_cstring =
		"\n* translate_run_melt_expressions result=";
	      /*^apply.arg */
	      argtab[6].meltbp_aptr =
		(melt_ptr_t *) & /*_.RESULT__V149*/ meltfptr[136];
	      /*_.MELT_DEBUG_FUN__V152*/ meltfptr[151] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V151*/ meltfptr[150] =
	      /*_.MELT_DEBUG_FUN__V152*/ meltfptr[151];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-outobj.melt:6375:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L64*/ meltfnum[59] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V152*/ meltfptr[151] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V151*/ meltfptr[150] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-outobj.melt:6375:/ quasiblock");


      /*_.PROGN___V153*/ meltfptr[151] = /*_.IF___V151*/ meltfptr[150];;
      /*^compute */
      /*_.IFCPP___V150*/ meltfptr[137] = /*_.PROGN___V153*/ meltfptr[151];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:6375:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L63*/ meltfnum[58] = 0;
      /*^clear */
	     /*clear *//*_.IF___V151*/ meltfptr[150] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V153*/ meltfptr[151] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V150*/ meltfptr[137] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-outobj.melt:6377:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.RESULT__V149*/ meltfptr[136];;

    {
      MELT_LOCATION ("warmelt-outobj.melt:6377:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*_.LET___V148*/ meltfptr[131] = /*_.RETURN___V154*/ meltfptr[150];;

    MELT_LOCATION ("warmelt-outobj.melt:6373:/ clear");
	   /*clear *//*_.RESULT__V149*/ meltfptr[136] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V150*/ meltfptr[137] = 0;
    /*^clear */
	   /*clear *//*_.RETURN___V154*/ meltfptr[150] = 0;
    /*_.LET___V121*/ meltfptr[117] = /*_.LET___V148*/ meltfptr[131];;

    MELT_LOCATION ("warmelt-outobj.melt:6347:/ clear");
	   /*clear *//*_#NBLITVAL__L47*/ meltfnum[37] = 0;
    /*^clear */
	   /*clear *//*_.TUPLITVAL__V122*/ meltfptr[118] = 0;
    /*^clear */
	   /*clear *//*_#IX__L48*/ meltfnum[38] = 0;
    /*^clear */
	   /*clear *//*_.FILTUP__V123*/ meltfptr[122] = 0;
    /*^clear */
	   /*clear *//*_#NBFILE__L49*/ meltfnum[48] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V124*/ meltfptr[123] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V140*/ meltfptr[138] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V144*/ meltfptr[130] = 0;
    /*^clear */
	   /*clear *//*_.LET___V148*/ meltfptr[131] = 0;
    /*_.LET___V91*/ meltfptr[87] = /*_.LET___V121*/ meltfptr[117];;

    MELT_LOCATION ("warmelt-outobj.melt:6327:/ clear");
	   /*clear *//*_.BASENAME__V92*/ meltfptr[88] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V93*/ meltfptr[89] = 0;
    /*^clear */
	   /*clear *//*_.TRANSLATE_MACROEXPANDED_LIST__V97*/ meltfptr[93] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V98*/ meltfptr[94] = 0;
    /*^clear */
	   /*clear *//*_.REFERENCED_VALUE__V102*/ meltfptr[98] = 0;
    /*^clear */
	   /*clear *//*_#NOTNULL__L36*/ meltfnum[13] = 0;
    /*^clear */
	   /*clear *//*_#OR___L37*/ meltfnum[30] = 0;
    /*^clear */
	   /*clear *//*_.IF___V103*/ meltfptr[99] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V107*/ meltfptr[103] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V111*/ meltfptr[104] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V115*/ meltfptr[105] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V117*/ meltfptr[112] = 0;
    /*^clear */
	   /*clear *//*_.LET___V121*/ meltfptr[117] = 0;
    /*_.LET___V78*/ meltfptr[56] = /*_.LET___V91*/ meltfptr[87];;

    MELT_LOCATION ("warmelt-outobj.melt:6238:/ clear");
	   /*clear *//*_.XLIST__V79*/ meltfptr[55] = 0;
    /*^clear */
	   /*clear *//*_#LENXLIST__L25*/ meltfnum[12] = 0;
    /*^clear */
	   /*clear *//*_.NORMALEXTEND__V80*/ meltfptr[59] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V82*/ meltfptr[81] = 0;
    /*^clear */
	   /*clear *//*_.REFERENCED_VALUE__V86*/ meltfptr[82] = 0;
    /*^clear */
	   /*clear *//*_#NOTNULL__L28*/ meltfnum[18] = 0;
    /*^clear */
	   /*clear *//*_#OR___L29*/ meltfnum[17] = 0;
    /*^clear */
	   /*clear *//*_.IF___V87*/ meltfptr[83] = 0;
    /*^clear */
	   /*clear *//*_.LET___V91*/ meltfptr[87] = 0;
    /*_.LET___V35*/ meltfptr[31] = /*_.LET___V78*/ meltfptr[56];;

    MELT_LOCATION ("warmelt-outobj.melt:6134:/ clear");
	   /*clear *//*_.RUNERRORHDLR__V36*/ meltfptr[32] = 0;
    /*^clear */
	   /*clear *//*_.LITVALIST__V38*/ meltfptr[37] = 0;
    /*^clear */
	   /*clear *//*_.MAKE_MAPSTRING__V41*/ meltfptr[38] = 0;
    /*^clear */
	   /*clear *//*_.MAKE_MAPSTRING__V42*/ meltfptr[39] = 0;
    /*^clear */
	   /*clear *//*_.MAKE_INTEGERBOX__V43*/ meltfptr[42] = 0;
    /*^clear */
	   /*clear *//*_.MAKE_LIST__V44*/ meltfptr[43] = 0;
    /*^clear */
	   /*clear *//*_.MAKE_LIST__V45*/ meltfptr[44] = 0;
    /*^clear */
	   /*clear *//*_.MAKE_LIST__V46*/ meltfptr[45] = 0;
    /*^clear */
	   /*clear *//*_.MAKE_INTEGERBOX__V47*/ meltfptr[46] = 0;
    /*^clear */
	   /*clear *//*_.MAKE_MAPOBJECT__V48*/ meltfptr[47] = 0;
    /*^clear */
	   /*clear *//*_.MODCTX__V49*/ meltfptr[48] = 0;
    /*^clear */
	   /*clear *//*_.NCX__V51*/ meltfptr[50] = 0;
    /*^clear */
	   /*clear *//*_.LET___V52*/ meltfptr[51] = 0;
    /*^clear */
	   /*clear *//*_.LSEXP__V68*/ meltfptr[64] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V69*/ meltfptr[66] = 0;
    /*^clear */
	   /*clear *//*_.REFERENCED_VALUE__V73*/ meltfptr[52] = 0;
    /*^clear */
	   /*clear *//*_.IF___V74*/ meltfptr[53] = 0;
    /*^clear */
	   /*clear *//*_.LET___V78*/ meltfptr[56] = 0;
    /*_.LET___V25*/ meltfptr[12] = /*_.LET___V35*/ meltfptr[31];;

    MELT_LOCATION ("warmelt-outobj.melt:6111:/ clear");
	   /*clear *//*_#NUM__L11*/ meltfnum[9] = 0;
    /*^clear */
	   /*clear *//*_#STARTERRCOUNT__L12*/ meltfnum[5] = 0;
    /*^clear */
	   /*clear *//*_.NAKEDBASNAM__V26*/ meltfptr[19] = 0;
    /*^clear */
	   /*clear *//*_.REFERR__V27*/ meltfptr[20] = 0;
    /*^clear */
	   /*clear *//*_.REFNORMLIST__V29*/ meltfptr[28] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V31*/ meltfptr[30] = 0;
    /*^clear */
	   /*clear *//*_.LET___V35*/ meltfptr[31] = 0;
    MELT_LOCATION ("warmelt-outobj.melt:6098:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.LET___V25*/ meltfptr[12];;

    {
      MELT_LOCATION ("warmelt-outobj.melt:6098:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V4*/ meltfptr[3] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V8*/ meltfptr[4] = 0;
    /*^clear */
	   /*clear *//*_#IS_LIST__L4*/ meltfnum[0] = 0;
    /*^clear */
	   /*clear *//*_#NOT__L5*/ meltfnum[1] = 0;
    /*^clear */
	   /*clear *//*_.IF___V10*/ meltfptr[5] = 0;
    /*^clear */
	   /*clear *//*_.LIST_FIRST__V17*/ meltfptr[10] = 0;
    /*^clear */
	   /*clear *//*_#NULL__L8*/ meltfnum[6] = 0;
    /*^clear */
	   /*clear *//*_.IF___V18*/ meltfptr[11] = 0;
    /*^clear */
	   /*clear *//*_.LET___V25*/ meltfptr[12] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("TRANSLATE_RUN_MELT_EXPRESSIONS", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_139_warmelt_outobj_TRANSLATE_RUN_MELT_EXPRESSIONS_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_139_warmelt_outobj_TRANSLATE_RUN_MELT_EXPRESSIONS */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_140_warmelt_outobj_LAMBDA___29__ (meltclosure_ptr_t meltclosp_,
					   melt_ptr_t meltfirstargp_,
					   const melt_argdescr_cell_t
					   meltxargdescr_[],
					   union meltparam_un * meltxargtab_,
					   const melt_argdescr_cell_t
					   meltxresdescr_[],
					   union meltparam_un * meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_140_warmelt_outobj_LAMBDA___29___melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_140_warmelt_outobj_LAMBDA___29___st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 7
    melt_ptr_t mcfr_varptr[7];
#define MELTFRAM_NBVARNUM 3
    long mcfr_varnum[3];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_140_warmelt_outobj_LAMBDA___29__ is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_140_warmelt_outobj_LAMBDA___29___st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 7; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_140_warmelt_outobj_LAMBDA___29__ nbval 7*/
  meltfram__.mcfr_nbvar = 7 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("LAMBDA_", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-outobj.melt:6136:/ getarg");
 /*_.V__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:6137:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L1*/ meltfnum[0] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-outobj.melt:6137:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L1*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-outobj.melt:6137:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-outobj.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 6137;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "runerrorhdlr v=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.V__V2*/ meltfptr[1];
	      /*_.MELT_DEBUG_FUN__V5*/ meltfptr[4] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V4*/ meltfptr[3] = /*_.MELT_DEBUG_FUN__V5*/ meltfptr[4];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-outobj.melt:6137:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V5*/ meltfptr[4] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V4*/ meltfptr[3] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-outobj.melt:6137:/ quasiblock");


      /*_.PROGN___V6*/ meltfptr[4] = /*_.IF___V4*/ meltfptr[3];;
      /*^compute */
      /*_.IFCPP___V3*/ meltfptr[2] = /*_.PROGN___V6*/ meltfptr[4];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:6137:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L1*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IF___V4*/ meltfptr[3] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V6*/ meltfptr[4] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V3*/ meltfptr[2] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-outobj.melt:6139:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#IS_STRING__L3*/ meltfnum[1] =
      (melt_magic_discr ((melt_ptr_t) ( /*_.V__V2*/ meltfptr[1])) ==
       MELTOBMAG_STRING);;
    MELT_LOCATION ("warmelt-outobj.melt:6139:/ cond");
    /*cond */ if ( /*_#IS_STRING__L3*/ meltfnum[1])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{


	  {
	    MELT_LOCATION ("warmelt-outobj.melt:6141:/ locexp");
	    /* translate_run_melt_expressions WARNRUNSTR_CHK__1 */
	    warning (0, "MELT running expression {%s} error - %s",
		     melt_string_str ((melt_ptr_t)
				      ( /*~NAKEDBASNAM */ meltfclos->
				       tabval[0])),
		     melt_string_str ((melt_ptr_t) /*_.V__V2*/ meltfptr[1]));;
	  }
	  ;
	  /*epilog */
	}
	;
      }
    else
      {
	MELT_LOCATION ("warmelt-outobj.melt:6139:/ cond.else");

	/*^block */
	/*anyblock */
	{


	  {
	    MELT_LOCATION ("warmelt-outobj.melt:6148:/ locexp");
	    /* translate_run_melt_expressions WARNRUN_CHK__1 */
	    warning (0, "MELT running expression {%s} errored",
		     melt_string_str ((melt_ptr_t)
				      ( /*~NAKEDBASNAM */ meltfclos->
				       tabval[0])));;
	  }
	  ;
	  MELT_LOCATION ("warmelt-outobj.melt:6146:/ quasiblock");


	  /*epilog */
	}
	;
      }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:6153:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^cond */
    /*cond */ if ( /*_.V__V2*/ meltfptr[1])	/*then */
      {
	/*^cond.then */
	/*_.OR___V7*/ meltfptr[3] = /*_.V__V2*/ meltfptr[1];;
      }
    else
      {
	MELT_LOCATION ("warmelt-outobj.melt:6153:/ cond.else");

	/*_.OR___V7*/ meltfptr[3] =
	  ( /*!konst_1_TRUE */ meltfrout->tabval[1]);;
      }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:6153:/ cond");
    /*cond */ if (
		   /*ifisa */
		   melt_is_instance_of ((melt_ptr_t)
					(( /*~REFERR */ meltfclos->
					  tabval[1])),
					(melt_ptr_t) (( /*!CLASS_REFERENCE */
						       meltfrout->tabval[2])))
      )				/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  /*^putslot */
	  /*putslot */
	  melt_assertmsg ("putslot checkobj @REFERENCED_VALUE",
			  melt_magic_discr ((melt_ptr_t)
					    (( /*~REFERR */ meltfclos->
					      tabval[1]))) ==
			  MELTOBMAG_OBJECT);
	  melt_putfield_object ((( /*~REFERR */ meltfclos->tabval[1])), (0),
				( /*_.OR___V7*/ meltfptr[3]),
				"REFERENCED_VALUE");
	  ;
	  /*^touch */
	  meltgc_touch (( /*~REFERR */ meltfclos->tabval[1]));
	  ;
	  /*^touchobj */

	  melt_dbgtrace_written_object (( /*~REFERR */ meltfclos->tabval[1]),
					"put-fields");
	  ;
	  /*epilog */
	}
	;
      }				/*noelse */
    ;
    MELT_LOCATION ("warmelt-outobj.melt:6136:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V3*/ meltfptr[2] = 0;
    /*^clear */
	   /*clear *//*_#IS_STRING__L3*/ meltfnum[1] = 0;
    /*^clear */
	   /*clear *//*_.OR___V7*/ meltfptr[3] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("LAMBDA_", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_140_warmelt_outobj_LAMBDA___29___melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*noretval */ NULL);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_140_warmelt_outobj_LAMBDA___29__ */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_141_warmelt_outobj_LAMBDA___30__ (meltclosure_ptr_t meltclosp_,
					   melt_ptr_t meltfirstargp_,
					   const melt_argdescr_cell_t
					   meltxargdescr_[],
					   union meltparam_un * meltxargtab_,
					   const melt_argdescr_cell_t
					   meltxresdescr_[],
					   union meltparam_un * meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_141_warmelt_outobj_LAMBDA___30___melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_141_warmelt_outobj_LAMBDA___30___st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 23
    melt_ptr_t mcfr_varptr[23];
#define MELTFRAM_NBVARNUM 11
    long mcfr_varnum[11];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_141_warmelt_outobj_LAMBDA___30__ is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_141_warmelt_outobj_LAMBDA___30___st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 23; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_141_warmelt_outobj_LAMBDA___30__ nbval 23*/
  meltfram__.mcfr_nbvar = 23 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("LAMBDA_", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-outobj.melt:6180:/ getarg");
 /*_.CUREXP__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_LONG)
    goto lab_endgetargs;
 /*_#IX__L1*/ meltfnum[0] = meltxargtab_[0].meltbp_long;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {

    MELT_LOCATION ("warmelt-outobj.melt:6182:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#NULL__L2*/ meltfnum[1] =
      (( /*_.CUREXP__V2*/ meltfptr[1]) == NULL);;
    MELT_LOCATION ("warmelt-outobj.melt:6182:/ cond");
    /*cond */ if ( /*_#NULL__L2*/ meltfnum[1])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{




	  {
	    MELT_LOCATION ("warmelt-outobj.melt:6183:/ locexp");
	    /*void */ (void) 0;
	  }
	  ;
	     /*clear *//*_.IFELSE___V3*/ meltfptr[2] = 0;
	  /*epilog */
	}
	;
      }
    else
      {
	MELT_LOCATION ("warmelt-outobj.melt:6182:/ cond.else");

	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-outobj.melt:6184:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
   /*_#IS_A__L3*/ meltfnum[2] =
	    melt_is_instance_of ((melt_ptr_t) ( /*_.CUREXP__V2*/ meltfptr[1]),
				 (melt_ptr_t) (( /*!CLASS_SEXPR */ meltfrout->
						tabval[0])));;
	  MELT_LOCATION ("warmelt-outobj.melt:6184:/ cond");
	  /*cond */ if ( /*_#IS_A__L3*/ meltfnum[2])	/*then */
	    {
	      /*^cond.then */
	      /*^block */
	      /*anyblock */
	      {




		{
		  MELT_LOCATION ("warmelt-outobj.melt:6185:/ locexp");
		  meltgc_append_list ((melt_ptr_t)
				      (( /*~LS */ meltfclos->tabval[0])),
				      (melt_ptr_t) ( /*_.CUREXP__V2*/
						    meltfptr[1]));
		}
		;
	       /*clear *//*_.IFELSE___V4*/ meltfptr[3] = 0;
		/*epilog */
	      }
	      ;
	    }
	  else
	    {
	      MELT_LOCATION ("warmelt-outobj.melt:6184:/ cond.else");

	      /*^block */
	      /*anyblock */
	      {

		MELT_LOCATION ("warmelt-outobj.melt:6186:/ checksignal");
		MELT_CHECK_SIGNAL ();
		;
     /*_#IS_A__L4*/ meltfnum[3] =
		  melt_is_instance_of ((melt_ptr_t)
				       ( /*_.CUREXP__V2*/ meltfptr[1]),
				       (melt_ptr_t) (( /*!CLASS_SYMBOL */
						      meltfrout->
						      tabval[1])));;
		MELT_LOCATION ("warmelt-outobj.melt:6186:/ cond");
		/*cond */ if ( /*_#IS_A__L4*/ meltfnum[3])	/*then */
		  {
		    /*^cond.then */
		    /*^block */
		    /*anyblock */
		    {




		      {
			MELT_LOCATION ("warmelt-outobj.melt:6187:/ locexp");
			meltgc_append_list ((melt_ptr_t)
					    (( /*~LS */ meltfclos->
					      tabval[0])),
					    (melt_ptr_t) ( /*_.CUREXP__V2*/
							  meltfptr[1]));
		      }
		      ;
		 /*clear *//*_.IFELSE___V5*/ meltfptr[4] = 0;
		      /*epilog */
		    }
		    ;
		  }
		else
		  {
		    MELT_LOCATION ("warmelt-outobj.melt:6186:/ cond.else");

		    /*^block */
		    /*anyblock */
		    {

		      MELT_LOCATION
			("warmelt-outobj.melt:6188:/ checksignal");
		      MELT_CHECK_SIGNAL ();
		      ;
       /*_#IS_STRING__L5*/ meltfnum[4] =
			(melt_magic_discr
			 ((melt_ptr_t) ( /*_.CUREXP__V2*/ meltfptr[1])) ==
			 MELTOBMAG_STRING);;
		      MELT_LOCATION ("warmelt-outobj.melt:6188:/ cond");
		      /*cond */ if ( /*_#IS_STRING__L5*/ meltfnum[4])	/*then */
			{
			  /*^cond.then */
			  /*^block */
			  /*anyblock */
			  {




			    {
			      MELT_LOCATION
				("warmelt-outobj.melt:6189:/ locexp");
			      meltgc_append_list ((melt_ptr_t)
						  (( /*~LS */ meltfclos->
						    tabval[0])),
						  (melt_ptr_t) ( /*_.CUREXP__V2*/ meltfptr[1]));
			    }
			    ;
		   /*clear *//*_.IFELSE___V6*/ meltfptr[5] = 0;
			    /*epilog */
			  }
			  ;
			}
		      else
			{
			  MELT_LOCATION
			    ("warmelt-outobj.melt:6188:/ cond.else");

			  /*^block */
			  /*anyblock */
			  {

			    MELT_LOCATION
			      ("warmelt-outobj.melt:6190:/ checksignal");
			    MELT_CHECK_SIGNAL ();
			    ;
	 /*_#IS_INTEGERBOX__L6*/ meltfnum[5] =
			      (melt_magic_discr
			       ((melt_ptr_t) ( /*_.CUREXP__V2*/ meltfptr[1]))
			       == MELTOBMAG_INT);;
			    MELT_LOCATION ("warmelt-outobj.melt:6190:/ cond");
			    /*cond */ if ( /*_#IS_INTEGERBOX__L6*/ meltfnum[5])	/*then */
			      {
				/*^cond.then */
				/*^block */
				/*anyblock */
				{




				  {
				    MELT_LOCATION
				      ("warmelt-outobj.melt:6191:/ locexp");
				    meltgc_append_list ((melt_ptr_t)
							(( /*~LS */
							  meltfclos->
							  tabval[0])),
							(melt_ptr_t) ( /*_.CUREXP__V2*/ meltfptr[1]));
				  }
				  ;
		     /*clear *//*_.IFELSE___V7*/ meltfptr[6] = 0;
				  /*epilog */
				}
				;
			      }
			    else
			      {
				MELT_LOCATION
				  ("warmelt-outobj.melt:6190:/ cond.else");

				/*^block */
				/*anyblock */
				{

				  MELT_LOCATION
				    ("warmelt-outobj.melt:6192:/ checksignal");
				  MELT_CHECK_SIGNAL ();
				  ;
	   /*_#IS_CLOSURE__L7*/ meltfnum[6] =
				    (melt_magic_discr
				     ((melt_ptr_t)
				      ( /*_.CUREXP__V2*/ meltfptr[1])) ==
				     MELTOBMAG_CLOSURE);;
				  MELT_LOCATION
				    ("warmelt-outobj.melt:6192:/ cond");
				  /*cond */ if ( /*_#IS_CLOSURE__L7*/ meltfnum[6])	/*then */
				    {
				      /*^cond.then */
				      /*^block */
				      /*anyblock */
				      {


#if MELT_HAVE_DEBUG
					MELT_LOCATION
					  ("warmelt-outobj.melt:6193:/ cppif.then");
					/*^block */
					/*anyblock */
					{


					  {
					    /*^locexp */
					    /*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
					    melt_dbgcounter++;
#endif
					    ;
					  }
					  ;
					  /*^checksignal */
					  MELT_CHECK_SIGNAL ();
					  ;
	       /*_#MELT_NEED_DBG__L8*/ meltfnum[7]
					    =
					    /*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
					    ( /*melt_need_dbg */
					     melt_need_debug ((int) 0))
#else
					    0	/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
					    ;;
					  MELT_LOCATION
					    ("warmelt-outobj.melt:6193:/ cond");
					  /*cond */ if ( /*_#MELT_NEED_DBG__L8*/ meltfnum[7])	/*then */
					    {
					      /*^cond.then */
					      /*^block */
					      /*anyblock */
					      {

		 /*_#THE_MELTCALLCOUNT__L9*/
						  meltfnum[8] =
#ifdef meltcallcount
						  meltcallcount	/* the_meltcallcount */
#else
						  0L
#endif /* meltcallcount the_meltcallcount */
						  ;;
						MELT_LOCATION
						  ("warmelt-outobj.melt:6193:/ checksignal");
						MELT_CHECK_SIGNAL ();
						;
						/*^apply */
						/*apply */
						{
						  union meltparam_un
						    argtab[7];
						  memset (&argtab, 0,
							  sizeof (argtab));
						  /*^apply.arg */
						  argtab[0].meltbp_long =
						    /*_#THE_MELTCALLCOUNT__L9*/
						    meltfnum[8];
						  /*^apply.arg */
						  argtab[1].meltbp_cstring =
						    "warmelt-outobj.melt";
						  /*^apply.arg */
						  argtab[2].meltbp_long =
						    6193;
						  /*^apply.arg */
						  argtab[3].meltbp_cstring =
						    "translate_run_melt_expressions closure curexp=";
						  /*^apply.arg */
						  argtab[4].meltbp_aptr =
						    (melt_ptr_t *) &
						    /*_.CUREXP__V2*/
						    meltfptr[1];
						  /*^apply.arg */
						  argtab[5].meltbp_cstring =
						    " ix=";
						  /*^apply.arg */
						  argtab[6].meltbp_long =
						    /*_#IX__L1*/ meltfnum[0];
						  /*_.MELT_DEBUG_FUN__V11*/
						    meltfptr[10] =
						    melt_apply ((meltclosure_ptr_t) (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[2])), (melt_ptr_t) (( /*nil */ NULL)), (MELTBPARSTR_LONG MELTBPARSTR_CSTRING MELTBPARSTR_LONG MELTBPARSTR_CSTRING MELTBPARSTR_PTR MELTBPARSTR_CSTRING MELTBPARSTR_LONG ""), argtab, "", (union meltparam_un *) 0);
						}
						;
						/*_.IF___V10*/ meltfptr[9] =
						  /*_.MELT_DEBUG_FUN__V11*/
						  meltfptr[10];;
						/*epilog */

						MELT_LOCATION
						  ("warmelt-outobj.melt:6193:/ clear");
			   /*clear *//*_#THE_MELTCALLCOUNT__L9*/
						  meltfnum[8] = 0;
						/*^clear */
			   /*clear *//*_.MELT_DEBUG_FUN__V11*/
						  meltfptr[10] = 0;
					      }
					      ;
					    }
					  else
					    {	/*^cond.else */

		/*_.IF___V10*/ meltfptr[9] =
						NULL;;
					    }
					  ;
					  MELT_LOCATION
					    ("warmelt-outobj.melt:6193:/ quasiblock");


					  /*_.PROGN___V12*/ meltfptr[10] =
					    /*_.IF___V10*/ meltfptr[9];;
					  /*^compute */
					  /*_.IFCPP___V9*/ meltfptr[8] =
					    /*_.PROGN___V12*/ meltfptr[10];;
					  /*epilog */

					  MELT_LOCATION
					    ("warmelt-outobj.melt:6193:/ clear");
			 /*clear *//*_#MELT_NEED_DBG__L8*/
					    meltfnum[7] = 0;
					  /*^clear */
			 /*clear *//*_.IF___V10*/ meltfptr[9]
					    = 0;
					  /*^clear */
			 /*clear *//*_.PROGN___V12*/
					    meltfptr[10] = 0;
					}

#else /*MELT_HAVE_DEBUG */
					/*^cppif.else */
					/*_.IFCPP___V9*/ meltfptr[8] =
					  ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
					;
					MELT_LOCATION
					  ("warmelt-outobj.melt:6194:/ checksignal");
					MELT_CHECK_SIGNAL ();
					;
					/*^apply */
					/*apply */
					{
					  /*_.CUREXP__V13*/ meltfptr[9] =
					    melt_apply ((meltclosure_ptr_t)
							( /*_.CUREXP__V2*/
							 meltfptr[1]),
							(melt_ptr_t) (( /*~LS */ meltfclos->tabval[0])), (""), (union meltparam_un *) 0, "", (union meltparam_un *) 0);
					}
					;
					MELT_LOCATION
					  ("warmelt-outobj.melt:6192:/ quasiblock");


					/*_.PROGN___V14*/ meltfptr[10] =
					  /*_.CUREXP__V13*/ meltfptr[9];;
					/*^compute */
					/*_.IFELSE___V8*/ meltfptr[7] =
					  /*_.PROGN___V14*/ meltfptr[10];;
					/*epilog */

					MELT_LOCATION
					  ("warmelt-outobj.melt:6192:/ clear");
		       /*clear *//*_.IFCPP___V9*/ meltfptr[8]
					  = 0;
					/*^clear */
		       /*clear *//*_.CUREXP__V13*/
					  meltfptr[9] = 0;
					/*^clear */
		       /*clear *//*_.PROGN___V14*/
					  meltfptr[10] = 0;
				      }
				      ;
				    }
				  else
				    {	/*^cond.else */

				      /*^block */
				      /*anyblock */
				      {


#if MELT_HAVE_DEBUG
					MELT_LOCATION
					  ("warmelt-outobj.melt:6196:/ cppif.then");
					/*^block */
					/*anyblock */
					{


					  {
					    /*^locexp */
					    /*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
					    melt_dbgcounter++;
#endif
					    ;
					  }
					  ;
					  /*^checksignal */
					  MELT_CHECK_SIGNAL ();
					  ;
	       /*_#MELT_NEED_DBG__L10*/ meltfnum[8]
					    =
					    /*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
					    ( /*melt_need_dbg */
					     melt_need_debug ((int) 0))
#else
					    0	/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
					    ;;
					  MELT_LOCATION
					    ("warmelt-outobj.melt:6196:/ cond");
					  /*cond */ if ( /*_#MELT_NEED_DBG__L10*/ meltfnum[8])	/*then */
					    {
					      /*^cond.then */
					      /*^block */
					      /*anyblock */
					      {

		 /*_#THE_MELTCALLCOUNT__L11*/
						  meltfnum[7] =
#ifdef meltcallcount
						  meltcallcount	/* the_meltcallcount */
#else
						  0L
#endif /* meltcallcount the_meltcallcount */
						  ;;
						MELT_LOCATION
						  ("warmelt-outobj.melt:6196:/ checksignal");
						MELT_CHECK_SIGNAL ();
						;
						/*^apply */
						/*apply */
						{
						  union meltparam_un
						    argtab[7];
						  memset (&argtab, 0,
							  sizeof (argtab));
						  /*^apply.arg */
						  argtab[0].meltbp_long =
						    /*_#THE_MELTCALLCOUNT__L11*/
						    meltfnum[7];
						  /*^apply.arg */
						  argtab[1].meltbp_cstring =
						    "warmelt-outobj.melt";
						  /*^apply.arg */
						  argtab[2].meltbp_long =
						    6196;
						  /*^apply.arg */
						  argtab[3].meltbp_cstring =
						    "translate_run_melt_expressions bad curexp=";
						  /*^apply.arg */
						  argtab[4].meltbp_aptr =
						    (melt_ptr_t *) &
						    /*_.CUREXP__V2*/
						    meltfptr[1];
						  /*^apply.arg */
						  argtab[5].meltbp_cstring =
						    " ix=";
						  /*^apply.arg */
						  argtab[6].meltbp_long =
						    /*_#IX__L1*/ meltfnum[0];
						  /*_.MELT_DEBUG_FUN__V17*/
						    meltfptr[10] =
						    melt_apply ((meltclosure_ptr_t) (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[2])), (melt_ptr_t) (( /*nil */ NULL)), (MELTBPARSTR_LONG MELTBPARSTR_CSTRING MELTBPARSTR_LONG MELTBPARSTR_CSTRING MELTBPARSTR_PTR MELTBPARSTR_CSTRING MELTBPARSTR_LONG ""), argtab, "", (union meltparam_un *) 0);
						}
						;
						/*_.IF___V16*/ meltfptr[9] =
						  /*_.MELT_DEBUG_FUN__V17*/
						  meltfptr[10];;
						/*epilog */

						MELT_LOCATION
						  ("warmelt-outobj.melt:6196:/ clear");
			   /*clear *//*_#THE_MELTCALLCOUNT__L11*/
						  meltfnum[7] = 0;
						/*^clear */
			   /*clear *//*_.MELT_DEBUG_FUN__V17*/
						  meltfptr[10] = 0;
					      }
					      ;
					    }
					  else
					    {	/*^cond.else */

		/*_.IF___V16*/ meltfptr[9] =
						NULL;;
					    }
					  ;
					  MELT_LOCATION
					    ("warmelt-outobj.melt:6196:/ quasiblock");


					  /*_.PROGN___V18*/ meltfptr[10] =
					    /*_.IF___V16*/ meltfptr[9];;
					  /*^compute */
					  /*_.IFCPP___V15*/ meltfptr[8] =
					    /*_.PROGN___V18*/ meltfptr[10];;
					  /*epilog */

					  MELT_LOCATION
					    ("warmelt-outobj.melt:6196:/ clear");
			 /*clear *//*_#MELT_NEED_DBG__L10*/
					    meltfnum[8] = 0;
					  /*^clear */
			 /*clear *//*_.IF___V16*/ meltfptr[9]
					    = 0;
					  /*^clear */
			 /*clear *//*_.PROGN___V18*/
					    meltfptr[10] = 0;
					}

#else /*MELT_HAVE_DEBUG */
					/*^cppif.else */
					/*_.IFCPP___V15*/ meltfptr[8] =
					  ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
					;
					MELT_LOCATION
					  ("warmelt-outobj.melt:6197:/ quasiblock");


	     /*_.CURDIS__V20*/ meltfptr[10] =
					  ((melt_ptr_t)
					   (melt_discr
					    ((melt_ptr_t)
					     ( /*_.CUREXP__V2*/
					      meltfptr[1]))));;
					MELT_LOCATION
					  ("warmelt-outobj.melt:6198:/ cond");
					/*cond */ if (
						       /*ifisa */
						       melt_is_instance_of ((melt_ptr_t) ( /*_.CURDIS__V20*/ meltfptr[10]),
									    (melt_ptr_t) (( /*!CLASS_NAMED */ meltfrout->tabval[3])))
					  )	/*then */
					  {
					    /*^cond.then */
					    /*^getslot */
					    {
					      melt_ptr_t slot = NULL, obj =
						NULL;
					      obj =
						(melt_ptr_t) ( /*_.CURDIS__V20*/ meltfptr[10]) /*=obj*/ ;
					      melt_object_get_field (slot,
								     obj, 1,
								     "NAMED_NAME");
	       /*_.CURDISNAME__V21*/
						meltfptr[20] = slot;
					    };
					    ;
					  }
					else
					  {	/*^cond.else */

	      /*_.CURDISNAME__V21*/ meltfptr[20]
					      = NULL;;
					  }
					;

					{
					  MELT_LOCATION
					    ("warmelt-outobj.melt:6201:/ locexp");
					  /* translate_run_melt_expressions BADEXP_CHK__1 */
					  warning (0,
						   "MELT run %s expression #%d has bad disciminant %s",
						   melt_string_str ((melt_ptr_t) ( /*~NAKEDBASNAM */ meltfclos->tabval[1])),
						   1 +
						   (int) /*_#IX__L1*/
						   meltfnum[0],
						   melt_string_str ((melt_ptr_t) /*_.CURDISNAME__V21*/ meltfptr[20]));
					  ;
					}
					;
					MELT_LOCATION
					  ("warmelt-outobj.melt:6207:/ checksignal");
					MELT_CHECK_SIGNAL ();
					;
					/*^apply */
					/*apply */
					{
					  /*_.RUNERRORHDLR__V22*/ meltfptr[21]
					    =
					    melt_apply ((meltclosure_ptr_t)
							(( /*~RUNERRORHDLR */
							  meltfclos->
							  tabval[2])),
							(melt_ptr_t) (( /*!konst_4 */ meltfrout->tabval[4])), (""), (union meltparam_un *) 0, "", (union meltparam_un *) 0);
					}
					;
					/*_.LET___V19*/ meltfptr[9] =
					  /*_.RUNERRORHDLR__V22*/
					  meltfptr[21];;

					MELT_LOCATION
					  ("warmelt-outobj.melt:6197:/ clear");
		       /*clear *//*_.CURDIS__V20*/
					  meltfptr[10] = 0;
					/*^clear */
		       /*clear *//*_.CURDISNAME__V21*/
					  meltfptr[20] = 0;
					/*^clear */
		       /*clear *//*_.RUNERRORHDLR__V22*/
					  meltfptr[21] = 0;
					MELT_LOCATION
					  ("warmelt-outobj.melt:6195:/ quasiblock");


					/*_.PROGN___V23*/ meltfptr[10] =
					  /*_.LET___V19*/ meltfptr[9];;
					/*^compute */
					/*_.IFELSE___V8*/ meltfptr[7] =
					  /*_.PROGN___V23*/ meltfptr[10];;
					/*epilog */

					MELT_LOCATION
					  ("warmelt-outobj.melt:6192:/ clear");
		       /*clear *//*_.IFCPP___V15*/
					  meltfptr[8] = 0;
					/*^clear */
		       /*clear *//*_.LET___V19*/ meltfptr[9]
					  = 0;
					/*^clear */
		       /*clear *//*_.PROGN___V23*/
					  meltfptr[10] = 0;
				      }
				      ;
				    }
				  ;
				  /*_.IFELSE___V7*/ meltfptr[6] =
				    /*_.IFELSE___V8*/ meltfptr[7];;
				  /*epilog */

				  MELT_LOCATION
				    ("warmelt-outobj.melt:6190:/ clear");
		     /*clear *//*_#IS_CLOSURE__L7*/ meltfnum[6] =
				    0;
				  /*^clear */
		     /*clear *//*_.IFELSE___V8*/ meltfptr[7] = 0;
				}
				;
			      }
			    ;
			    /*_.IFELSE___V6*/ meltfptr[5] =
			      /*_.IFELSE___V7*/ meltfptr[6];;
			    /*epilog */

			    MELT_LOCATION
			      ("warmelt-outobj.melt:6188:/ clear");
		   /*clear *//*_#IS_INTEGERBOX__L6*/ meltfnum[5] = 0;
			    /*^clear */
		   /*clear *//*_.IFELSE___V7*/ meltfptr[6] = 0;
			  }
			  ;
			}
		      ;
		      /*_.IFELSE___V5*/ meltfptr[4] =
			/*_.IFELSE___V6*/ meltfptr[5];;
		      /*epilog */

		      MELT_LOCATION ("warmelt-outobj.melt:6186:/ clear");
		 /*clear *//*_#IS_STRING__L5*/ meltfnum[4] = 0;
		      /*^clear */
		 /*clear *//*_.IFELSE___V6*/ meltfptr[5] = 0;
		    }
		    ;
		  }
		;
		/*_.IFELSE___V4*/ meltfptr[3] =
		  /*_.IFELSE___V5*/ meltfptr[4];;
		/*epilog */

		MELT_LOCATION ("warmelt-outobj.melt:6184:/ clear");
	       /*clear *//*_#IS_A__L4*/ meltfnum[3] = 0;
		/*^clear */
	       /*clear *//*_.IFELSE___V5*/ meltfptr[4] = 0;
	      }
	      ;
	    }
	  ;
	  /*_.IFELSE___V3*/ meltfptr[2] = /*_.IFELSE___V4*/ meltfptr[3];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-outobj.melt:6182:/ clear");
	     /*clear *//*_#IS_A__L3*/ meltfnum[2] = 0;
	  /*^clear */
	     /*clear *//*_.IFELSE___V4*/ meltfptr[3] = 0;
	}
	;
      }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:6180:/ quasiblock");


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.IFELSE___V3*/ meltfptr[2];;

    {
      MELT_LOCATION ("warmelt-outobj.melt:6180:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_#NULL__L2*/ meltfnum[1] = 0;
    /*^clear */
	   /*clear *//*_.IFELSE___V3*/ meltfptr[2] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("LAMBDA_", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_141_warmelt_outobj_LAMBDA___30___melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_141_warmelt_outobj_LAMBDA___30__ */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_142_warmelt_outobj_LAMBDA___31__ (meltclosure_ptr_t meltclosp_,
					   melt_ptr_t meltfirstargp_,
					   const melt_argdescr_cell_t
					   meltxargdescr_[],
					   union meltparam_un * meltxargtab_,
					   const melt_argdescr_cell_t
					   meltxresdescr_[],
					   union meltparam_un * meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_142_warmelt_outobj_LAMBDA___31___melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_142_warmelt_outobj_LAMBDA___31___st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 90
    melt_ptr_t mcfr_varptr[90];
#define MELTFRAM_NBVARNUM 28
    long mcfr_varnum[28];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_142_warmelt_outobj_LAMBDA___31__ is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_142_warmelt_outobj_LAMBDA___31___st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 90; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_142_warmelt_outobj_LAMBDA___31__ nbval 90*/
  meltfram__.mcfr_nbvar = 90 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("LAMBDA_", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-outobj.melt:6243:/ getarg");
 /*_.NORMLIST__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.BINDLIST__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.BINDLIST__V3*/ meltfptr[2])) !=
	      NULL);


  /*getarg#2 */
  /*^getarg */
  if (meltxargdescr_[1] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.MODCTX__V4*/ meltfptr[3] =
    (meltxargtab_[1].meltbp_aptr) ? (*(meltxargtab_[1].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.MODCTX__V4*/ meltfptr[3])) !=
	      NULL);


  /*getarg#3 */
  /*^getarg */
  if (meltxargdescr_[2] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.NCX__V5*/ meltfptr[4] =
    (meltxargtab_[2].meltbp_aptr) ? (*(meltxargtab_[2].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.NCX__V5*/ meltfptr[4])) != NULL);


  /*getarg#4 */
  /*^getarg */
  if (meltxargdescr_[3] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.INIENV__V6*/ meltfptr[5] =
    (meltxargtab_[3].meltbp_aptr) ? (*(meltxargtab_[3].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.INIENV__V6*/ meltfptr[5])) !=
	      NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:6244:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L1*/ meltfnum[0] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-outobj.melt:6244:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L1*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-outobj.melt:6246:/ cond");
	    /*cond */ if (
			   /*ifisa */
			   melt_is_instance_of ((melt_ptr_t)
						(( /*~REFNORMLIST */
						  meltfclos->tabval[0])),
						(melt_ptr_t) (( /*!CLASS_REFERENCE */ meltfrout->tabval[1])))
	      )			/*then */
	      {
		/*^cond.then */
		/*^getslot */
		{
		  melt_ptr_t slot = NULL, obj = NULL;
		  obj =
		    (melt_ptr_t) (( /*~REFNORMLIST */ meltfclos->
				   tabval[0])) /*=obj*/ ;
		  melt_object_get_field (slot, obj, 0, "REFERENCED_VALUE");
       /*_.REFERENCED_VALUE__V9*/ meltfptr[8] = slot;
		};
		;
	      }
	    else
	      {			/*^cond.else */

      /*_.REFERENCED_VALUE__V9*/ meltfptr[8] = NULL;;
	      }
	    ;
	    MELT_LOCATION ("warmelt-outobj.melt:6244:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[11];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-outobj.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 6244;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring =
		"translate_run_melt_expressions/normalextend normlist=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.NORMLIST__V2*/ meltfptr[1];
	      /*^apply.arg */
	      argtab[5].meltbp_cstring = "\n bindlist=";
	      /*^apply.arg */
	      argtab[6].meltbp_aptr =
		(melt_ptr_t *) & /*_.BINDLIST__V3*/ meltfptr[2];
	      /*^apply.arg */
	      argtab[7].meltbp_cstring = "\n old !refnormlist=";
	      /*^apply.arg */
	      argtab[8].meltbp_aptr =
		(melt_ptr_t *) & /*_.REFERENCED_VALUE__V9*/ meltfptr[8];
	      /*^apply.arg */
	      argtab[9].meltbp_cstring = "\n ncx=";
	      /*^apply.arg */
	      argtab[10].meltbp_aptr =
		(melt_ptr_t *) & /*_.NCX__V5*/ meltfptr[4];
	      /*_.MELT_DEBUG_FUN__V10*/ meltfptr[9] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V8*/ meltfptr[7] =
	      /*_.MELT_DEBUG_FUN__V10*/ meltfptr[9];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-outobj.melt:6244:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1] = 0;
	    /*^clear */
	       /*clear *//*_.REFERENCED_VALUE__V9*/ meltfptr[8] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V10*/ meltfptr[9] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V8*/ meltfptr[7] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-outobj.melt:6244:/ quasiblock");


      /*_.PROGN___V11*/ meltfptr[8] = /*_.IF___V8*/ meltfptr[7];;
      /*^compute */
      /*_.IFCPP___V7*/ meltfptr[6] = /*_.PROGN___V11*/ meltfptr[8];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:6244:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L1*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IF___V8*/ meltfptr[7] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V11*/ meltfptr[8] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V7*/ meltfptr[6] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:6248:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#__L3*/ meltfnum[1] =
	(( /*_.BINDLIST__V3*/ meltfptr[2]) !=
	 ( /*_.NORMLIST__V2*/ meltfptr[1]));;
      MELT_LOCATION ("warmelt-outobj.melt:6248:/ cond");
      /*cond */ if ( /*_#__L3*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V13*/ meltfptr[7] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-outobj.melt:6248:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check bindlist != normlist"),
				  ("warmelt-outobj.melt")
				  ? ("warmelt-outobj.melt") : __FILE__,
				  (6248) ? (6248) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V13*/ meltfptr[7] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V12*/ meltfptr[9] = /*_.IFELSE___V13*/ meltfptr[7];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:6248:/ clear");
	     /*clear *//*_#__L3*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V13*/ meltfptr[7] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V12*/ meltfptr[9] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:6249:/ locexp");

#if MELT_HAVE_DEBUG
      if (melt_need_debug (0))
	melt_dbgshortbacktrace (("translate_run_melt_expressions/normalextend"), (13));
#endif
      ;
    }
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:6250:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
      /*^cond */
      /*cond */ if (
		     /*ifisa */
		     melt_is_instance_of ((melt_ptr_t)
					  (( /*~REFNORMLIST */ meltfclos->
					    tabval[0])),
					  (melt_ptr_t) (( /*!CLASS_REFERENCE */ meltfrout->tabval[1])))
	)			/*then */
	{
	  /*^cond.then */
	  /*^getslot */
	  {
	    melt_ptr_t slot = NULL, obj = NULL;
	    obj =
	      (melt_ptr_t) (( /*~REFNORMLIST */ meltfclos->
			     tabval[0])) /*=obj*/ ;
	    melt_object_get_field (slot, obj, 0, "REFERENCED_VALUE");
     /*_.REFERENCED_VALUE__V15*/ meltfptr[7] = slot;
	  };
	  ;
	}
      else
	{			/*^cond.else */

    /*_.REFERENCED_VALUE__V15*/ meltfptr[7] = NULL;;
	}
      ;
      /*^compute */
   /*_#NULL__L4*/ meltfnum[0] =
	(( /*_.REFERENCED_VALUE__V15*/ meltfptr[7]) == NULL);;
      MELT_LOCATION ("warmelt-outobj.melt:6250:/ cond");
      /*cond */ if ( /*_#NULL__L4*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V16*/ meltfptr[15] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-outobj.melt:6250:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check null !refnormlist"),
				  ("warmelt-outobj.melt")
				  ? ("warmelt-outobj.melt") : __FILE__,
				  (6250) ? (6250) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V16*/ meltfptr[15] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V14*/ meltfptr[8] = /*_.IFELSE___V16*/ meltfptr[15];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:6250:/ clear");
	     /*clear *//*_.REFERENCED_VALUE__V15*/ meltfptr[7] = 0;
      /*^clear */
	     /*clear *//*_#NULL__L4*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V16*/ meltfptr[15] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V14*/ meltfptr[8] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-outobj.melt:6251:/ quasiblock");


 /*_.LASTNORMPAIR__V18*/ meltfptr[15] =
      (melt_list_last ((melt_ptr_t) ( /*_.NORMLIST__V2*/ meltfptr[1])));;
    /*^compute */
 /*_.LASTBINDPAIR__V19*/ meltfptr[18] =
      (melt_list_last ((melt_ptr_t) ( /*_.BINDLIST__V3*/ meltfptr[2])));;
    /*^compute */
 /*_.LASTNORM__V20*/ meltfptr[19] =
      (melt_pair_head
       (melt_list_last ((melt_ptr_t) ( /*_.NORMLIST__V2*/ meltfptr[1]))));;
    /*^compute */
 /*_.LASTBINDS__V21*/ meltfptr[20] =
      (melt_pair_head
       (melt_list_last ((melt_ptr_t) ( /*_.BINDLIST__V3*/ meltfptr[2]))));;
    MELT_LOCATION ("warmelt-outobj.melt:6255:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^msend */
    /*msend */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^ojbmsend.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.INIENV__V6*/ meltfptr[5];
      /*_.LASTCTYP__V22*/ meltfptr[21] =
	meltgc_send ((melt_ptr_t) ( /*_.LASTNORM__V20*/ meltfptr[19]),
		     (melt_ptr_t) (( /*!GET_CTYPE */ meltfrout->tabval[2])),
		     (MELTBPARSTR_PTR ""), argtab, "",
		     (union meltparam_un *) 0);
    }
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:6257:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L5*/ meltfnum[1] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-outobj.melt:6257:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L5*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L6*/ meltfnum[0] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-outobj.melt:6257:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[11];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L6*/ meltfnum[0];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-outobj.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 6257;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring =
		"translate_run_melt_expressions/normalextend lastnorm=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.LASTNORM__V20*/ meltfptr[19];
	      /*^apply.arg */
	      argtab[5].meltbp_cstring = " lastnormpair=";
	      /*^apply.arg */
	      argtab[6].meltbp_aptr =
		(melt_ptr_t *) & /*_.LASTNORMPAIR__V18*/ meltfptr[15];
	      /*^apply.arg */
	      argtab[7].meltbp_cstring = " lastbinds=";
	      /*^apply.arg */
	      argtab[8].meltbp_aptr =
		(melt_ptr_t *) & /*_.LASTBINDS__V21*/ meltfptr[20];
	      /*^apply.arg */
	      argtab[9].meltbp_cstring = " lastctyp=";
	      /*^apply.arg */
	      argtab[10].meltbp_aptr =
		(melt_ptr_t *) & /*_.LASTCTYP__V22*/ meltfptr[21];
	      /*_.MELT_DEBUG_FUN__V25*/ meltfptr[24] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V24*/ meltfptr[23] =
	      /*_.MELT_DEBUG_FUN__V25*/ meltfptr[24];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-outobj.melt:6257:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L6*/ meltfnum[0] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V25*/ meltfptr[24] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V24*/ meltfptr[23] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-outobj.melt:6257:/ quasiblock");


      /*_.PROGN___V26*/ meltfptr[24] = /*_.IF___V24*/ meltfptr[23];;
      /*^compute */
      /*_.IFCPP___V23*/ meltfptr[22] = /*_.PROGN___V26*/ meltfptr[24];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:6257:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L5*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IF___V24*/ meltfptr[23] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V26*/ meltfptr[24] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V23*/ meltfptr[22] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:6261:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L7*/ meltfnum[0] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.LASTCTYP__V22*/ meltfptr[21]),
			     (melt_ptr_t) (( /*!CLASS_CTYPE */ meltfrout->
					    tabval[3])));;
      MELT_LOCATION ("warmelt-outobj.melt:6261:/ cond");
      /*cond */ if ( /*_#IS_A__L7*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V28*/ meltfptr[24] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-outobj.melt:6261:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check lastctyp"),
				  ("warmelt-outobj.melt")
				  ? ("warmelt-outobj.melt") : __FILE__,
				  (6261) ? (6261) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V28*/ meltfptr[24] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V27*/ meltfptr[23] = /*_.IFELSE___V28*/ meltfptr[24];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:6261:/ clear");
	     /*clear *//*_#IS_A__L7*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V28*/ meltfptr[24] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V27*/ meltfptr[23] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-outobj.melt:6263:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#__L8*/ meltfnum[1] =
      (( /*_.LASTCTYP__V22*/ meltfptr[21]) ==
       (( /*!CTYPE_VALUE */ meltfrout->tabval[4])));;
    MELT_LOCATION ("warmelt-outobj.melt:6263:/ cond");
    /*cond */ if ( /*_#__L8*/ meltfnum[1])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{


#if MELT_HAVE_DEBUG
	  MELT_LOCATION ("warmelt-outobj.melt:6264:/ cppif.then");
	  /*^block */
	  /*anyblock */
	  {


	    {
	      /*^locexp */
	      /*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	      melt_dbgcounter++;
#endif
	      ;
	    }
	    ;
	    /*^checksignal */
	    MELT_CHECK_SIGNAL ();
	    ;
     /*_#MELT_NEED_DBG__L9*/ meltfnum[0] =
	      /*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	      ( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	      0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	      ;;
	    MELT_LOCATION ("warmelt-outobj.melt:6264:/ cond");
	    /*cond */ if ( /*_#MELT_NEED_DBG__L9*/ meltfnum[0])	/*then */
	      {
		/*^cond.then */
		/*^block */
		/*anyblock */
		{

       /*_#THE_MELTCALLCOUNT__L10*/ meltfnum[9] =
#ifdef meltcallcount
		    meltcallcount	/* the_meltcallcount */
#else
		    0L
#endif /* meltcallcount the_meltcallcount */
		    ;;
		  /*^compute */
       /*_.DISCRIM__V32*/ meltfptr[31] =
		    ((melt_ptr_t)
		     (melt_discr
		      ((melt_ptr_t) ( /*_.LASTNORM__V20*/ meltfptr[19]))));;
		  MELT_LOCATION ("warmelt-outobj.melt:6264:/ checksignal");
		  MELT_CHECK_SIGNAL ();
		  ;
		  /*^apply */
		  /*apply */
		  {
		    union meltparam_un argtab[11];
		    memset (&argtab, 0, sizeof (argtab));
		    /*^apply.arg */
		    argtab[0].meltbp_long =
		      /*_#THE_MELTCALLCOUNT__L10*/ meltfnum[9];
		    /*^apply.arg */
		    argtab[1].meltbp_cstring = "warmelt-outobj.melt";
		    /*^apply.arg */
		    argtab[2].meltbp_long = 6264;
		    /*^apply.arg */
		    argtab[3].meltbp_cstring =
		      "translate_run_melt_expressions/normalextend value lastnorm=";
		    /*^apply.arg */
		    argtab[4].meltbp_aptr =
		      (melt_ptr_t *) & /*_.LASTNORM__V20*/ meltfptr[19];
		    /*^apply.arg */
		    argtab[5].meltbp_cstring = "\n of discrim=";
		    /*^apply.arg */
		    argtab[6].meltbp_aptr =
		      (melt_ptr_t *) & /*_.DISCRIM__V32*/ meltfptr[31];
		    /*^apply.arg */
		    argtab[7].meltbp_cstring = "\n lastnormpair=";
		    /*^apply.arg */
		    argtab[8].meltbp_aptr =
		      (melt_ptr_t *) & /*_.LASTNORMPAIR__V18*/ meltfptr[15];
		    /*^apply.arg */
		    argtab[9].meltbp_cstring = "\n normlist=";
		    /*^apply.arg */
		    argtab[10].meltbp_aptr =
		      (melt_ptr_t *) & /*_.NORMLIST__V2*/ meltfptr[1];
		    /*_.MELT_DEBUG_FUN__V33*/ meltfptr[32] =
		      melt_apply ((meltclosure_ptr_t)
				  (( /*!MELT_DEBUG_FUN */ meltfrout->
				    tabval[0])),
				  (melt_ptr_t) (( /*nil */ NULL)),
				  (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_PTR MELTBPARSTR_CSTRING
				   MELTBPARSTR_PTR MELTBPARSTR_CSTRING
				   MELTBPARSTR_PTR MELTBPARSTR_CSTRING
				   MELTBPARSTR_PTR ""), argtab, "",
				  (union meltparam_un *) 0);
		  }
		  ;
		  /*_.IF___V31*/ meltfptr[30] =
		    /*_.MELT_DEBUG_FUN__V33*/ meltfptr[32];;
		  /*epilog */

		  MELT_LOCATION ("warmelt-outobj.melt:6264:/ clear");
		 /*clear *//*_#THE_MELTCALLCOUNT__L10*/ meltfnum[9] = 0;
		  /*^clear */
		 /*clear *//*_.DISCRIM__V32*/ meltfptr[31] = 0;
		  /*^clear */
		 /*clear *//*_.MELT_DEBUG_FUN__V33*/ meltfptr[32] = 0;
		}
		;
	      }
	    else
	      {			/*^cond.else */

      /*_.IF___V31*/ meltfptr[30] = NULL;;
	      }
	    ;
	    MELT_LOCATION ("warmelt-outobj.melt:6264:/ quasiblock");


	    /*_.PROGN___V34*/ meltfptr[31] = /*_.IF___V31*/ meltfptr[30];;
	    /*^compute */
	    /*_.IFCPP___V30*/ meltfptr[29] = /*_.PROGN___V34*/ meltfptr[31];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-outobj.melt:6264:/ clear");
	       /*clear *//*_#MELT_NEED_DBG__L9*/ meltfnum[0] = 0;
	    /*^clear */
	       /*clear *//*_.IF___V31*/ meltfptr[30] = 0;
	    /*^clear */
	       /*clear *//*_.PROGN___V34*/ meltfptr[31] = 0;
	  }

#else /*MELT_HAVE_DEBUG */
	  /*^cppif.else */
	  /*_.IFCPP___V30*/ meltfptr[29] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
	  ;
	  MELT_LOCATION ("warmelt-outobj.melt:6267:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
   /*_#IS_NOT_A__L11*/ meltfnum[9] =
	    !melt_is_instance_of ((melt_ptr_t)
				  ( /*_.LASTNORM__V20*/ meltfptr[19]),
				  (melt_ptr_t) (( /*!CLASS_NREP_RETURN */
						 meltfrout->tabval[5])));;
	  MELT_LOCATION ("warmelt-outobj.melt:6267:/ cond");
	  /*cond */ if ( /*_#IS_NOT_A__L11*/ meltfnum[9])	/*then */
	    {
	      /*^cond.then */
	      /*^block */
	      /*anyblock */
	      {

		MELT_LOCATION ("warmelt-outobj.melt:6268:/ quasiblock");


		/*^checksignal */
		MELT_CHECK_SIGNAL ();
		;
		/*^quasiblock */


		/*^rawallocobj */
		/*rawallocobj */
		{
		  melt_ptr_t newobj = 0;
		  melt_raw_object_create (newobj,
					  (melt_ptr_t) (( /*!CLASS_NREP_RETURN */ meltfrout->tabval[5])), (3), "CLASS_NREP_RETURN");
      /*_.INST__V38*/ meltfptr[37] =
		    newobj;
		};
		;
		/*^putslot */
		/*putslot */
		melt_assertmsg ("putslot checkobj @NRET_MAIN",
				melt_magic_discr ((melt_ptr_t)
						  ( /*_.INST__V38*/
						   meltfptr[37])) ==
				MELTOBMAG_OBJECT);
		melt_putfield_object (( /*_.INST__V38*/ meltfptr[37]), (1),
				      ( /*_.LASTNORM__V20*/ meltfptr[19]),
				      "NRET_MAIN");
		;
		/*^touchobj */

		melt_dbgtrace_written_object ( /*_.INST__V38*/ meltfptr[37],
					      "newly made instance");
		;
		/*_.NRET__V37*/ meltfptr[31] = /*_.INST__V38*/ meltfptr[37];;

		{
		  MELT_LOCATION ("warmelt-outobj.melt:6272:/ locexp");
		  meltgc_pair_set_head ((melt_ptr_t)
					( /*_.LASTNORMPAIR__V18*/
					 meltfptr[15]),
					( /*_.NRET__V37*/ meltfptr[31]));
		}
		;

#if MELT_HAVE_DEBUG
		MELT_LOCATION ("warmelt-outobj.melt:6273:/ cppif.then");
		/*^block */
		/*anyblock */
		{


		  {
		    /*^locexp */
		    /*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
		    melt_dbgcounter++;
#endif
		    ;
		  }
		  ;
		  /*^checksignal */
		  MELT_CHECK_SIGNAL ();
		  ;
       /*_#MELT_NEED_DBG__L12*/ meltfnum[0] =
		    /*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
		    ( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
		    0		/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
		    ;;
		  MELT_LOCATION ("warmelt-outobj.melt:6273:/ cond");
		  /*cond */ if ( /*_#MELT_NEED_DBG__L12*/ meltfnum[0])	/*then */
		    {
		      /*^cond.then */
		      /*^block */
		      /*anyblock */
		      {

	 /*_#THE_MELTCALLCOUNT__L13*/ meltfnum[12] =
#ifdef meltcallcount
			  meltcallcount	/* the_meltcallcount */
#else
			  0L
#endif /* meltcallcount the_meltcallcount */
			  ;;
			MELT_LOCATION
			  ("warmelt-outobj.melt:6273:/ checksignal");
			MELT_CHECK_SIGNAL ();
			;
			/*^apply */
			/*apply */
			{
			  union meltparam_un argtab[9];
			  memset (&argtab, 0, sizeof (argtab));
			  /*^apply.arg */
			  argtab[0].meltbp_long =
			    /*_#THE_MELTCALLCOUNT__L13*/ meltfnum[12];
			  /*^apply.arg */
			  argtab[1].meltbp_cstring = "warmelt-outobj.melt";
			  /*^apply.arg */
			  argtab[2].meltbp_long = 6273;
			  /*^apply.arg */
			  argtab[3].meltbp_cstring =
			    "translate_run_melt_expressions/normalextend replaced lastnorm=";
			  /*^apply.arg */
			  argtab[4].meltbp_aptr =
			    (melt_ptr_t *) & /*_.LASTNORM__V20*/ meltfptr[19];
			  /*^apply.arg */
			  argtab[5].meltbp_cstring = "\n with nret=";
			  /*^apply.arg */
			  argtab[6].meltbp_aptr =
			    (melt_ptr_t *) & /*_.NRET__V37*/ meltfptr[31];
			  /*^apply.arg */
			  argtab[7].meltbp_cstring = "\n in lastnormpair=";
			  /*^apply.arg */
			  argtab[8].meltbp_aptr =
			    (melt_ptr_t *) & /*_.LASTNORMPAIR__V18*/
			    meltfptr[15];
			  /*_.MELT_DEBUG_FUN__V41*/ meltfptr[40] =
			    melt_apply ((meltclosure_ptr_t)
					(( /*!MELT_DEBUG_FUN */ meltfrout->
					  tabval[0])),
					(melt_ptr_t) (( /*nil */ NULL)),
					(MELTBPARSTR_LONG MELTBPARSTR_CSTRING
					 MELTBPARSTR_LONG MELTBPARSTR_CSTRING
					 MELTBPARSTR_PTR MELTBPARSTR_CSTRING
					 MELTBPARSTR_PTR MELTBPARSTR_CSTRING
					 MELTBPARSTR_PTR ""), argtab, "",
					(union meltparam_un *) 0);
			}
			;
			/*_.IF___V40*/ meltfptr[39] =
			  /*_.MELT_DEBUG_FUN__V41*/ meltfptr[40];;
			/*epilog */

			MELT_LOCATION ("warmelt-outobj.melt:6273:/ clear");
		   /*clear *//*_#THE_MELTCALLCOUNT__L13*/ meltfnum[12] =
			  0;
			/*^clear */
		   /*clear *//*_.MELT_DEBUG_FUN__V41*/ meltfptr[40] = 0;
		      }
		      ;
		    }
		  else
		    {		/*^cond.else */

	/*_.IF___V40*/ meltfptr[39] = NULL;;
		    }
		  ;
		  MELT_LOCATION ("warmelt-outobj.melt:6273:/ quasiblock");


		  /*_.PROGN___V42*/ meltfptr[40] =
		    /*_.IF___V40*/ meltfptr[39];;
		  /*^compute */
		  /*_.IFCPP___V39*/ meltfptr[38] =
		    /*_.PROGN___V42*/ meltfptr[40];;
		  /*epilog */

		  MELT_LOCATION ("warmelt-outobj.melt:6273:/ clear");
		 /*clear *//*_#MELT_NEED_DBG__L12*/ meltfnum[0] = 0;
		  /*^clear */
		 /*clear *//*_.IF___V40*/ meltfptr[39] = 0;
		  /*^clear */
		 /*clear *//*_.PROGN___V42*/ meltfptr[40] = 0;
		}

#else /*MELT_HAVE_DEBUG */
		/*^cppif.else */
		/*_.IFCPP___V39*/ meltfptr[38] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
		;
		MELT_LOCATION ("warmelt-outobj.melt:6276:/ compute");
		/*_.LASTNORM__V20*/ meltfptr[19] =
		  /*_.SETQ___V43*/ meltfptr[39] =
		  /*_.NRET__V37*/ meltfptr[31];;
		/*_.LET___V36*/ meltfptr[30] = /*_.SETQ___V43*/ meltfptr[39];;

		MELT_LOCATION ("warmelt-outobj.melt:6268:/ clear");
	       /*clear *//*_.NRET__V37*/ meltfptr[31] = 0;
		/*^clear */
	       /*clear *//*_.IFCPP___V39*/ meltfptr[38] = 0;
		/*^clear */
	       /*clear *//*_.SETQ___V43*/ meltfptr[39] = 0;
		MELT_LOCATION ("warmelt-outobj.melt:6267:/ quasiblock");


		/*_.PROGN___V44*/ meltfptr[40] =
		  /*_.LET___V36*/ meltfptr[30];;
		/*^compute */
		/*_.IF___V35*/ meltfptr[32] = /*_.PROGN___V44*/ meltfptr[40];;
		/*epilog */

		MELT_LOCATION ("warmelt-outobj.melt:6267:/ clear");
	       /*clear *//*_.LET___V36*/ meltfptr[30] = 0;
		/*^clear */
	       /*clear *//*_.PROGN___V44*/ meltfptr[40] = 0;
	      }
	      ;
	    }
	  else
	    {			/*^cond.else */

    /*_.IF___V35*/ meltfptr[32] = NULL;;
	    }
	  ;
	  MELT_LOCATION ("warmelt-outobj.melt:6263:/ quasiblock");


	  /*_.PROGN___V45*/ meltfptr[31] = /*_.IF___V35*/ meltfptr[32];;
	  /*^compute */
	  /*_.IFELSE___V29*/ meltfptr[24] = /*_.PROGN___V45*/ meltfptr[31];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-outobj.melt:6263:/ clear");
	     /*clear *//*_.IFCPP___V30*/ meltfptr[29] = 0;
	  /*^clear */
	     /*clear *//*_#IS_NOT_A__L11*/ meltfnum[9] = 0;
	  /*^clear */
	     /*clear *//*_.IF___V35*/ meltfptr[32] = 0;
	  /*^clear */
	     /*clear *//*_.PROGN___V45*/ meltfptr[31] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

	/*^block */
	/*anyblock */
	{


#if MELT_HAVE_DEBUG
	  MELT_LOCATION ("warmelt-outobj.melt:6279:/ cppif.then");
	  /*^block */
	  /*anyblock */
	  {


	    {
	      /*^locexp */
	      /*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	      melt_dbgcounter++;
#endif
	      ;
	    }
	    ;
	    /*^checksignal */
	    MELT_CHECK_SIGNAL ();
	    ;
     /*_#MELT_NEED_DBG__L14*/ meltfnum[12] =
	      /*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	      ( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	      0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	      ;;
	    MELT_LOCATION ("warmelt-outobj.melt:6279:/ cond");
	    /*cond */ if ( /*_#MELT_NEED_DBG__L14*/ meltfnum[12])	/*then */
	      {
		/*^cond.then */
		/*^block */
		/*anyblock */
		{

       /*_#THE_MELTCALLCOUNT__L15*/ meltfnum[0] =
#ifdef meltcallcount
		    meltcallcount	/* the_meltcallcount */
#else
		    0L
#endif /* meltcallcount the_meltcallcount */
		    ;;
		  /*^compute */
       /*_.DISCRIM__V48*/ meltfptr[30] =
		    ((melt_ptr_t)
		     (melt_discr
		      ((melt_ptr_t) ( /*_.LASTNORM__V20*/ meltfptr[19]))));;
		  MELT_LOCATION ("warmelt-outobj.melt:6279:/ checksignal");
		  MELT_CHECK_SIGNAL ();
		  ;
		  /*^apply */
		  /*apply */
		  {
		    union meltparam_un argtab[9];
		    memset (&argtab, 0, sizeof (argtab));
		    /*^apply.arg */
		    argtab[0].meltbp_long =
		      /*_#THE_MELTCALLCOUNT__L15*/ meltfnum[0];
		    /*^apply.arg */
		    argtab[1].meltbp_cstring = "warmelt-outobj.melt";
		    /*^apply.arg */
		    argtab[2].meltbp_long = 6279;
		    /*^apply.arg */
		    argtab[3].meltbp_cstring =
		      "translate_run_melt_expressions/normalextend nonvalue lastnorm=";
		    /*^apply.arg */
		    argtab[4].meltbp_aptr =
		      (melt_ptr_t *) & /*_.LASTNORM__V20*/ meltfptr[19];
		    /*^apply.arg */
		    argtab[5].meltbp_cstring = " of discrim ";
		    /*^apply.arg */
		    argtab[6].meltbp_aptr =
		      (melt_ptr_t *) & /*_.DISCRIM__V48*/ meltfptr[30];
		    /*^apply.arg */
		    argtab[7].meltbp_cstring = "\n lastctyp=";
		    /*^apply.arg */
		    argtab[8].meltbp_aptr =
		      (melt_ptr_t *) & /*_.LASTCTYP__V22*/ meltfptr[21];
		    /*_.MELT_DEBUG_FUN__V49*/ meltfptr[40] =
		      melt_apply ((meltclosure_ptr_t)
				  (( /*!MELT_DEBUG_FUN */ meltfrout->
				    tabval[0])),
				  (melt_ptr_t) (( /*nil */ NULL)),
				  (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_PTR MELTBPARSTR_CSTRING
				   MELTBPARSTR_PTR MELTBPARSTR_CSTRING
				   MELTBPARSTR_PTR ""), argtab, "",
				  (union meltparam_un *) 0);
		  }
		  ;
		  /*_.IF___V47*/ meltfptr[39] =
		    /*_.MELT_DEBUG_FUN__V49*/ meltfptr[40];;
		  /*epilog */

		  MELT_LOCATION ("warmelt-outobj.melt:6279:/ clear");
		 /*clear *//*_#THE_MELTCALLCOUNT__L15*/ meltfnum[0] = 0;
		  /*^clear */
		 /*clear *//*_.DISCRIM__V48*/ meltfptr[30] = 0;
		  /*^clear */
		 /*clear *//*_.MELT_DEBUG_FUN__V49*/ meltfptr[40] = 0;
		}
		;
	      }
	    else
	      {			/*^cond.else */

      /*_.IF___V47*/ meltfptr[39] = NULL;;
	      }
	    ;
	    MELT_LOCATION ("warmelt-outobj.melt:6279:/ quasiblock");


	    /*_.PROGN___V50*/ meltfptr[29] = /*_.IF___V47*/ meltfptr[39];;
	    /*^compute */
	    /*_.IFCPP___V46*/ meltfptr[38] = /*_.PROGN___V50*/ meltfptr[29];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-outobj.melt:6279:/ clear");
	       /*clear *//*_#MELT_NEED_DBG__L14*/ meltfnum[12] = 0;
	    /*^clear */
	       /*clear *//*_.IF___V47*/ meltfptr[39] = 0;
	    /*^clear */
	       /*clear *//*_.PROGN___V50*/ meltfptr[29] = 0;
	  }

#else /*MELT_HAVE_DEBUG */
	  /*^cppif.else */
	  /*_.IFCPP___V46*/ meltfptr[38] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
	  ;
	  MELT_LOCATION ("warmelt-outobj.melt:6283:/ cond");
	  /*cond */ if (
			 /*ifisa */
			 melt_is_instance_of ((melt_ptr_t)
					      ( /*_.LASTCTYP__V22*/
					       meltfptr[21]),
					      (melt_ptr_t) (( /*!CLASS_NAMED */ meltfrout->tabval[6])))
	    )			/*then */
	    {
	      /*^cond.then */
	      /*^getslot */
	      {
		melt_ptr_t slot = NULL, obj = NULL;
		obj =
		  (melt_ptr_t) ( /*_.LASTCTYP__V22*/ meltfptr[21]) /*=obj*/ ;
		melt_object_get_field (slot, obj, 1, "NAMED_NAME");
     /*_.NAMED_NAME__V51*/ meltfptr[32] = slot;
	      };
	      ;
	    }
	  else
	    {			/*^cond.else */

    /*_.NAMED_NAME__V51*/ meltfptr[32] = NULL;;
	    }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:6281:/ locexp");
	    warning (0, "MELT WARNING MSG [#%ld]::: %s - %s", melt_dbgcounter,
		     ("runtime expressions ending with a non-value expression, auto-boxing it."),
		     melt_string_str ((melt_ptr_t)
				      ( /*_.NAMED_NAME__V51*/ meltfptr[32])));
	  }
	  ;
	  MELT_LOCATION ("warmelt-outobj.melt:6286:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  MELT_LOCATION ("warmelt-outobj.melt:6284:/ quasiblock");


	  /*^multiapply */
	  /*multiapply 4args, 1x.res */
	  {
	    union meltparam_un argtab[3];

	    union meltparam_un restab[1];
	    memset (&restab, 0, sizeof (restab));
	    memset (&argtab, 0, sizeof (argtab));
	    /*^multiapply.arg */
	    argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.LASTBINDS__V21*/ meltfptr[20];	/*^multiapply.arg */
	    argtab[1].meltbp_aptr = (melt_ptr_t *) & /*_.LASTCTYP__V22*/ meltfptr[21];	/*^multiapply.arg */
	    argtab[2].meltbp_aptr =
	      (melt_ptr_t *) & /*_.NCX__V5*/ meltfptr[4];
	    /*^multiapply.xres */
	    restab[0].meltbp_aptr =
	      (melt_ptr_t *) & /*_.NEWBINDS__V54*/ meltfptr[40];
	    /*^multiapply.appl */
	    /*_.NEWEXP__V53*/ meltfptr[30] =
	      melt_apply ((meltclosure_ptr_t)
			  (( /*!AUTOBOX_NORMAL_RETURN */ meltfrout->
			    tabval[7])),
			  (melt_ptr_t) ( /*_.LASTNORM__V20*/ meltfptr[19]),
			  (MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_PTR
			   ""), argtab, (MELTBPARSTR_PTR ""), restab);
	  }
	  ;
	  /*^quasiblock */



#if MELT_HAVE_DEBUG
	  MELT_LOCATION ("warmelt-outobj.melt:6287:/ cppif.then");
	  /*^block */
	  /*anyblock */
	  {


	    {
	      /*^locexp */
	      /*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	      melt_dbgcounter++;
#endif
	      ;
	    }
	    ;
	    /*^checksignal */
	    MELT_CHECK_SIGNAL ();
	    ;
     /*_#MELT_NEED_DBG__L16*/ meltfnum[9] =
	      /*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	      ( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	      0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	      ;;
	    MELT_LOCATION ("warmelt-outobj.melt:6287:/ cond");
	    /*cond */ if ( /*_#MELT_NEED_DBG__L16*/ meltfnum[9])	/*then */
	      {
		/*^cond.then */
		/*^block */
		/*anyblock */
		{

       /*_#THE_MELTCALLCOUNT__L17*/ meltfnum[0] =
#ifdef meltcallcount
		    meltcallcount	/* the_meltcallcount */
#else
		    0L
#endif /* meltcallcount the_meltcallcount */
		    ;;
		  MELT_LOCATION ("warmelt-outobj.melt:6287:/ checksignal");
		  MELT_CHECK_SIGNAL ();
		  ;
		  /*^apply */
		  /*apply */
		  {
		    union meltparam_un argtab[7];
		    memset (&argtab, 0, sizeof (argtab));
		    /*^apply.arg */
		    argtab[0].meltbp_long =
		      /*_#THE_MELTCALLCOUNT__L17*/ meltfnum[0];
		    /*^apply.arg */
		    argtab[1].meltbp_cstring = "warmelt-outobj.melt";
		    /*^apply.arg */
		    argtab[2].meltbp_long = 6287;
		    /*^apply.arg */
		    argtab[3].meltbp_cstring =
		      "translate_run_melt_expressions/normalextend autoboxed newexp=";
		    /*^apply.arg */
		    argtab[4].meltbp_aptr =
		      (melt_ptr_t *) & /*_.NEWEXP__V53*/ meltfptr[30];
		    /*^apply.arg */
		    argtab[5].meltbp_cstring = " newbinds=";
		    /*^apply.arg */
		    argtab[6].meltbp_aptr =
		      (melt_ptr_t *) & /*_.NEWBINDS__V54*/ meltfptr[40];
		    /*_.MELT_DEBUG_FUN__V57*/ meltfptr[56] =
		      melt_apply ((meltclosure_ptr_t)
				  (( /*!MELT_DEBUG_FUN */ meltfrout->
				    tabval[0])),
				  (melt_ptr_t) (( /*nil */ NULL)),
				  (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_PTR MELTBPARSTR_CSTRING
				   MELTBPARSTR_PTR ""), argtab, "",
				  (union meltparam_un *) 0);
		  }
		  ;
		  /*_.IF___V56*/ meltfptr[29] =
		    /*_.MELT_DEBUG_FUN__V57*/ meltfptr[56];;
		  /*epilog */

		  MELT_LOCATION ("warmelt-outobj.melt:6287:/ clear");
		 /*clear *//*_#THE_MELTCALLCOUNT__L17*/ meltfnum[0] = 0;
		  /*^clear */
		 /*clear *//*_.MELT_DEBUG_FUN__V57*/ meltfptr[56] = 0;
		}
		;
	      }
	    else
	      {			/*^cond.else */

      /*_.IF___V56*/ meltfptr[29] = NULL;;
	      }
	    ;
	    MELT_LOCATION ("warmelt-outobj.melt:6287:/ quasiblock");


	    /*_.PROGN___V58*/ meltfptr[56] = /*_.IF___V56*/ meltfptr[29];;
	    /*^compute */
	    /*_.IFCPP___V55*/ meltfptr[39] = /*_.PROGN___V58*/ meltfptr[56];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-outobj.melt:6287:/ clear");
	       /*clear *//*_#MELT_NEED_DBG__L16*/ meltfnum[9] = 0;
	    /*^clear */
	       /*clear *//*_.IF___V56*/ meltfptr[29] = 0;
	    /*^clear */
	       /*clear *//*_.PROGN___V58*/ meltfptr[56] = 0;
	  }

#else /*MELT_HAVE_DEBUG */
	  /*^cppif.else */
	  /*_.IFCPP___V55*/ meltfptr[39] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:6289:/ locexp");
	    meltgc_pair_set_head ((melt_ptr_t)
				  ( /*_.LASTNORMPAIR__V18*/ meltfptr[15]),
				  ( /*_.NEWEXP__V53*/ meltfptr[30]));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:6290:/ locexp");
	    meltgc_pair_set_head ((melt_ptr_t)
				  ( /*_.LASTBINDPAIR__V19*/ meltfptr[18]),
				  ( /*_.NEWBINDS__V54*/ meltfptr[40]));
	  }
	  ;
	  MELT_LOCATION ("warmelt-outobj.melt:6291:/ compute");
	  /*_.LASTBINDS__V21*/ meltfptr[20] = /*_.SETQ___V59*/ meltfptr[29] =
	    /*_.NEWBINDS__V54*/ meltfptr[40];;
	  MELT_LOCATION ("warmelt-outobj.melt:6292:/ compute");
	  /*_.LASTNORM__V20*/ meltfptr[19] = /*_.SETQ___V60*/ meltfptr[56] =
	    /*_.NEWEXP__V53*/ meltfptr[30];;
	  MELT_LOCATION ("warmelt-outobj.melt:6284:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*_.MULTI___V52*/ meltfptr[31] = /*_.SETQ___V60*/ meltfptr[56];;

	  MELT_LOCATION ("warmelt-outobj.melt:6284:/ clear");
	     /*clear *//*_.IFCPP___V55*/ meltfptr[39] = 0;
	  /*^clear */
	     /*clear *//*_.SETQ___V59*/ meltfptr[29] = 0;
	  /*^clear */
	     /*clear *//*_.SETQ___V60*/ meltfptr[56] = 0;

	  /*^clear */
	     /*clear *//*_.NEWBINDS__V54*/ meltfptr[40] = 0;
	  MELT_LOCATION ("warmelt-outobj.melt:6278:/ quasiblock");


	  /*_.PROGN___V61*/ meltfptr[39] = /*_.MULTI___V52*/ meltfptr[31];;
	  /*^compute */
	  /*_.IFELSE___V29*/ meltfptr[24] = /*_.PROGN___V61*/ meltfptr[39];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-outobj.melt:6263:/ clear");
	     /*clear *//*_.IFCPP___V46*/ meltfptr[38] = 0;
	  /*^clear */
	     /*clear *//*_.NAMED_NAME__V51*/ meltfptr[32] = 0;
	  /*^clear */
	     /*clear *//*_.MULTI___V52*/ meltfptr[31] = 0;
	  /*^clear */
	     /*clear *//*_.PROGN___V61*/ meltfptr[39] = 0;
	}
	;
      }
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:6295:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L18*/ meltfnum[12] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-outobj.melt:6295:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L18*/ meltfnum[12])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L19*/ meltfnum[0] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-outobj.melt:6295:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[13];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long =
		/*_#THE_MELTCALLCOUNT__L19*/ meltfnum[0];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-outobj.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 6295;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring =
		"translate_run_melt_expressions/normalextend lastbinds=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.LASTBINDS__V21*/ meltfptr[20];
	      /*^apply.arg */
	      argtab[5].meltbp_cstring = "\n lastnorm=";
	      /*^apply.arg */
	      argtab[6].meltbp_aptr =
		(melt_ptr_t *) & /*_.LASTNORM__V20*/ meltfptr[19];
	      /*^apply.arg */
	      argtab[7].meltbp_cstring = "\n lastnormpair=";
	      /*^apply.arg */
	      argtab[8].meltbp_aptr =
		(melt_ptr_t *) & /*_.LASTNORMPAIR__V18*/ meltfptr[15];
	      /*^apply.arg */
	      argtab[9].meltbp_cstring = "\n normlist=";
	      /*^apply.arg */
	      argtab[10].meltbp_aptr =
		(melt_ptr_t *) & /*_.NORMLIST__V2*/ meltfptr[1];
	      /*^apply.arg */
	      argtab[11].meltbp_cstring = "\n bindlist=";
	      /*^apply.arg */
	      argtab[12].meltbp_aptr =
		(melt_ptr_t *) & /*_.BINDLIST__V3*/ meltfptr[2];
	      /*_.MELT_DEBUG_FUN__V64*/ meltfptr[30] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V63*/ meltfptr[56] =
	      /*_.MELT_DEBUG_FUN__V64*/ meltfptr[30];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-outobj.melt:6295:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L19*/ meltfnum[0] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V64*/ meltfptr[30] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V63*/ meltfptr[56] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-outobj.melt:6295:/ quasiblock");


      /*_.PROGN___V65*/ meltfptr[40] = /*_.IF___V63*/ meltfptr[56];;
      /*^compute */
      /*_.IFCPP___V62*/ meltfptr[29] = /*_.PROGN___V65*/ meltfptr[40];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:6295:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L18*/ meltfnum[12] = 0;
      /*^clear */
	     /*clear *//*_.IF___V63*/ meltfptr[56] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V65*/ meltfptr[40] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V62*/ meltfptr[29] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-outobj.melt:6299:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_.LIST_FIRST__V66*/ meltfptr[38] =
      (melt_list_first ((melt_ptr_t) ( /*_.LASTBINDS__V21*/ meltfptr[20])));;
    MELT_LOCATION ("warmelt-outobj.melt:6299:/ cond");
    /*cond */ if ( /*_.LIST_FIRST__V66*/ meltfptr[38])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-outobj.melt:6300:/ quasiblock");


	  /*^checksignal */
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^apply */
	  /*apply */
	  {
	    union meltparam_un argtab[2];
	    memset (&argtab, 0, sizeof (argtab));
	    /*^apply.arg */
	    argtab[0].meltbp_aptr =
	      (melt_ptr_t *) & /*_.LASTBINDS__V21*/ meltfptr[20];
	    /*^apply.arg */
	    argtab[1].meltbp_aptr = /*nil */ (melt_ptr_t *) NULL;
	    /*_.NFINLET__V69*/ meltfptr[39] =
	      melt_apply ((meltclosure_ptr_t)
			  (( /*!WRAP_NORMAL_LET1 */ meltfrout->tabval[8])),
			  (melt_ptr_t) ( /*_.LASTNORM__V20*/ meltfptr[19]),
			  (MELTBPARSTR_PTR MELTBPARSTR_PTR ""), argtab, "",
			  (union meltparam_un *) 0);
	  }
	  ;

#if MELT_HAVE_DEBUG
	  MELT_LOCATION ("warmelt-outobj.melt:6302:/ cppif.then");
	  /*^block */
	  /*anyblock */
	  {


	    {
	      /*^locexp */
	      /*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	      melt_dbgcounter++;
#endif
	      ;
	    }
	    ;
	    /*^checksignal */
	    MELT_CHECK_SIGNAL ();
	    ;
     /*_#MELT_NEED_DBG__L20*/ meltfnum[9] =
	      /*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	      ( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	      0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	      ;;
	    MELT_LOCATION ("warmelt-outobj.melt:6302:/ cond");
	    /*cond */ if ( /*_#MELT_NEED_DBG__L20*/ meltfnum[9])	/*then */
	      {
		/*^cond.then */
		/*^block */
		/*anyblock */
		{

       /*_#THE_MELTCALLCOUNT__L21*/ meltfnum[0] =
#ifdef meltcallcount
		    meltcallcount	/* the_meltcallcount */
#else
		    0L
#endif /* meltcallcount the_meltcallcount */
		    ;;
		  MELT_LOCATION ("warmelt-outobj.melt:6302:/ checksignal");
		  MELT_CHECK_SIGNAL ();
		  ;
		  /*^apply */
		  /*apply */
		  {
		    union meltparam_un argtab[5];
		    memset (&argtab, 0, sizeof (argtab));
		    /*^apply.arg */
		    argtab[0].meltbp_long =
		      /*_#THE_MELTCALLCOUNT__L21*/ meltfnum[0];
		    /*^apply.arg */
		    argtab[1].meltbp_cstring = "warmelt-outobj.melt";
		    /*^apply.arg */
		    argtab[2].meltbp_long = 6302;
		    /*^apply.arg */
		    argtab[3].meltbp_cstring =
		      "translate_run_melt_expressions/normalextend nfinlet=";
		    /*^apply.arg */
		    argtab[4].meltbp_aptr =
		      (melt_ptr_t *) & /*_.NFINLET__V69*/ meltfptr[39];
		    /*_.MELT_DEBUG_FUN__V72*/ meltfptr[40] =
		      melt_apply ((meltclosure_ptr_t)
				  (( /*!MELT_DEBUG_FUN */ meltfrout->
				    tabval[0])),
				  (melt_ptr_t) (( /*nil */ NULL)),
				  (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_PTR ""), argtab, "",
				  (union meltparam_un *) 0);
		  }
		  ;
		  /*_.IF___V71*/ meltfptr[56] =
		    /*_.MELT_DEBUG_FUN__V72*/ meltfptr[40];;
		  /*epilog */

		  MELT_LOCATION ("warmelt-outobj.melt:6302:/ clear");
		 /*clear *//*_#THE_MELTCALLCOUNT__L21*/ meltfnum[0] = 0;
		  /*^clear */
		 /*clear *//*_.MELT_DEBUG_FUN__V72*/ meltfptr[40] = 0;
		}
		;
	      }
	    else
	      {			/*^cond.else */

      /*_.IF___V71*/ meltfptr[56] = NULL;;
	      }
	    ;
	    MELT_LOCATION ("warmelt-outobj.melt:6302:/ quasiblock");


	    /*_.PROGN___V73*/ meltfptr[40] = /*_.IF___V71*/ meltfptr[56];;
	    /*^compute */
	    /*_.IFCPP___V70*/ meltfptr[30] = /*_.PROGN___V73*/ meltfptr[40];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-outobj.melt:6302:/ clear");
	       /*clear *//*_#MELT_NEED_DBG__L20*/ meltfnum[9] = 0;
	    /*^clear */
	       /*clear *//*_.IF___V71*/ meltfptr[56] = 0;
	    /*^clear */
	       /*clear *//*_.PROGN___V73*/ meltfptr[40] = 0;
	  }

#else /*MELT_HAVE_DEBUG */
	  /*^cppif.else */
	  /*_.IFCPP___V70*/ meltfptr[30] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
	  ;

#if MELT_HAVE_DEBUG
	  MELT_LOCATION ("warmelt-outobj.melt:6303:/ cppif.then");
	  /*^block */
	  /*anyblock */
	  {


	    {
	      /*^locexp */
	      /*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	      melt_dbgcounter++;
#endif
	      ;
	    }
	    ;
	    /*^checksignal */
	    MELT_CHECK_SIGNAL ();
	    ;
     /*_#MELT_NEED_DBG__L22*/ meltfnum[12] =
	      /*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	      ( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	      0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	      ;;
	    MELT_LOCATION ("warmelt-outobj.melt:6303:/ cond");
	    /*cond */ if ( /*_#MELT_NEED_DBG__L22*/ meltfnum[12])	/*then */
	      {
		/*^cond.then */
		/*^block */
		/*anyblock */
		{

       /*_#THE_MELTCALLCOUNT__L23*/ meltfnum[0] =
#ifdef meltcallcount
		    meltcallcount	/* the_meltcallcount */
#else
		    0L
#endif /* meltcallcount the_meltcallcount */
		    ;;
		  MELT_LOCATION ("warmelt-outobj.melt:6303:/ checksignal");
		  MELT_CHECK_SIGNAL ();
		  ;
		  /*^apply */
		  /*apply */
		  {
		    union meltparam_un argtab[11];
		    memset (&argtab, 0, sizeof (argtab));
		    /*^apply.arg */
		    argtab[0].meltbp_long =
		      /*_#THE_MELTCALLCOUNT__L23*/ meltfnum[0];
		    /*^apply.arg */
		    argtab[1].meltbp_cstring = "warmelt-outobj.melt";
		    /*^apply.arg */
		    argtab[2].meltbp_long = 6303;
		    /*^apply.arg */
		    argtab[3].meltbp_cstring =
		      "translate_run_melt_expressions/normalextend before lastnormpair=";
		    /*^apply.arg */
		    argtab[4].meltbp_aptr =
		      (melt_ptr_t *) & /*_.LASTNORMPAIR__V18*/ meltfptr[15];
		    /*^apply.arg */
		    argtab[5].meltbp_cstring = "\n lastbindpair=";
		    /*^apply.arg */
		    argtab[6].meltbp_aptr =
		      (melt_ptr_t *) & /*_.LASTBINDPAIR__V19*/ meltfptr[18];
		    /*^apply.arg */
		    argtab[7].meltbp_cstring = "\n normlist=";
		    /*^apply.arg */
		    argtab[8].meltbp_aptr =
		      (melt_ptr_t *) & /*_.NORMLIST__V2*/ meltfptr[1];
		    /*^apply.arg */
		    argtab[9].meltbp_cstring = "\n bindlist=";
		    /*^apply.arg */
		    argtab[10].meltbp_aptr =
		      (melt_ptr_t *) & /*_.BINDLIST__V3*/ meltfptr[2];
		    /*_.MELT_DEBUG_FUN__V76*/ meltfptr[75] =
		      melt_apply ((meltclosure_ptr_t)
				  (( /*!MELT_DEBUG_FUN */ meltfrout->
				    tabval[0])),
				  (melt_ptr_t) (( /*nil */ NULL)),
				  (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_PTR MELTBPARSTR_CSTRING
				   MELTBPARSTR_PTR MELTBPARSTR_CSTRING
				   MELTBPARSTR_PTR MELTBPARSTR_CSTRING
				   MELTBPARSTR_PTR ""), argtab, "",
				  (union meltparam_un *) 0);
		  }
		  ;
		  /*_.IF___V75*/ meltfptr[40] =
		    /*_.MELT_DEBUG_FUN__V76*/ meltfptr[75];;
		  /*epilog */

		  MELT_LOCATION ("warmelt-outobj.melt:6303:/ clear");
		 /*clear *//*_#THE_MELTCALLCOUNT__L23*/ meltfnum[0] = 0;
		  /*^clear */
		 /*clear *//*_.MELT_DEBUG_FUN__V76*/ meltfptr[75] = 0;
		}
		;
	      }
	    else
	      {			/*^cond.else */

      /*_.IF___V75*/ meltfptr[40] = NULL;;
	      }
	    ;
	    MELT_LOCATION ("warmelt-outobj.melt:6303:/ quasiblock");


	    /*_.PROGN___V77*/ meltfptr[75] = /*_.IF___V75*/ meltfptr[40];;
	    /*^compute */
	    /*_.IFCPP___V74*/ meltfptr[56] = /*_.PROGN___V77*/ meltfptr[75];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-outobj.melt:6303:/ clear");
	       /*clear *//*_#MELT_NEED_DBG__L22*/ meltfnum[12] = 0;
	    /*^clear */
	       /*clear *//*_.IF___V75*/ meltfptr[40] = 0;
	    /*^clear */
	       /*clear *//*_.PROGN___V77*/ meltfptr[75] = 0;
	  }

#else /*MELT_HAVE_DEBUG */
	  /*^cppif.else */
	  /*_.IFCPP___V74*/ meltfptr[56] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:6307:/ locexp");
	    meltgc_pair_set_head ((melt_ptr_t)
				  ( /*_.LASTNORMPAIR__V18*/ meltfptr[15]),
				  ( /*_.NFINLET__V69*/ meltfptr[39]));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:6308:/ locexp");
	    meltgc_pair_set_head ((melt_ptr_t)
				  ( /*_.LASTBINDPAIR__V19*/ meltfptr[18]),
				  (( /*nil */ NULL)));
	  }
	  ;

#if MELT_HAVE_DEBUG
	  MELT_LOCATION ("warmelt-outobj.melt:6309:/ cppif.then");
	  /*^block */
	  /*anyblock */
	  {


	    {
	      /*^locexp */
	      /*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	      melt_dbgcounter++;
#endif
	      ;
	    }
	    ;
	    /*^checksignal */
	    MELT_CHECK_SIGNAL ();
	    ;
     /*_#MELT_NEED_DBG__L24*/ meltfnum[9] =
	      /*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	      ( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	      0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	      ;;
	    MELT_LOCATION ("warmelt-outobj.melt:6309:/ cond");
	    /*cond */ if ( /*_#MELT_NEED_DBG__L24*/ meltfnum[9])	/*then */
	      {
		/*^cond.then */
		/*^block */
		/*anyblock */
		{

       /*_#THE_MELTCALLCOUNT__L25*/ meltfnum[0] =
#ifdef meltcallcount
		    meltcallcount	/* the_meltcallcount */
#else
		    0L
#endif /* meltcallcount the_meltcallcount */
		    ;;
		  MELT_LOCATION ("warmelt-outobj.melt:6309:/ checksignal");
		  MELT_CHECK_SIGNAL ();
		  ;
		  /*^apply */
		  /*apply */
		  {
		    union meltparam_un argtab[11];
		    memset (&argtab, 0, sizeof (argtab));
		    /*^apply.arg */
		    argtab[0].meltbp_long =
		      /*_#THE_MELTCALLCOUNT__L25*/ meltfnum[0];
		    /*^apply.arg */
		    argtab[1].meltbp_cstring = "warmelt-outobj.melt";
		    /*^apply.arg */
		    argtab[2].meltbp_long = 6309;
		    /*^apply.arg */
		    argtab[3].meltbp_cstring =
		      "translate_run_melt_expressions/normalextend updated lastnormpair=";
		    /*^apply.arg */
		    argtab[4].meltbp_aptr =
		      (melt_ptr_t *) & /*_.LASTNORMPAIR__V18*/ meltfptr[15];
		    /*^apply.arg */
		    argtab[5].meltbp_cstring = "\n lastbindpair=";
		    /*^apply.arg */
		    argtab[6].meltbp_aptr =
		      (melt_ptr_t *) & /*_.LASTBINDPAIR__V19*/ meltfptr[18];
		    /*^apply.arg */
		    argtab[7].meltbp_cstring = "\n normlist=";
		    /*^apply.arg */
		    argtab[8].meltbp_aptr =
		      (melt_ptr_t *) & /*_.NORMLIST__V2*/ meltfptr[1];
		    /*^apply.arg */
		    argtab[9].meltbp_cstring = "\n bindlist=";
		    /*^apply.arg */
		    argtab[10].meltbp_aptr =
		      (melt_ptr_t *) & /*_.BINDLIST__V3*/ meltfptr[2];
		    /*_.MELT_DEBUG_FUN__V80*/ meltfptr[79] =
		      melt_apply ((meltclosure_ptr_t)
				  (( /*!MELT_DEBUG_FUN */ meltfrout->
				    tabval[0])),
				  (melt_ptr_t) (( /*nil */ NULL)),
				  (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_PTR MELTBPARSTR_CSTRING
				   MELTBPARSTR_PTR MELTBPARSTR_CSTRING
				   MELTBPARSTR_PTR MELTBPARSTR_CSTRING
				   MELTBPARSTR_PTR ""), argtab, "",
				  (union meltparam_un *) 0);
		  }
		  ;
		  /*_.IF___V79*/ meltfptr[75] =
		    /*_.MELT_DEBUG_FUN__V80*/ meltfptr[79];;
		  /*epilog */

		  MELT_LOCATION ("warmelt-outobj.melt:6309:/ clear");
		 /*clear *//*_#THE_MELTCALLCOUNT__L25*/ meltfnum[0] = 0;
		  /*^clear */
		 /*clear *//*_.MELT_DEBUG_FUN__V80*/ meltfptr[79] = 0;
		}
		;
	      }
	    else
	      {			/*^cond.else */

      /*_.IF___V79*/ meltfptr[75] = NULL;;
	      }
	    ;
	    MELT_LOCATION ("warmelt-outobj.melt:6309:/ quasiblock");


	    /*_.PROGN___V81*/ meltfptr[79] = /*_.IF___V79*/ meltfptr[75];;
	    /*^compute */
	    /*_.IFCPP___V78*/ meltfptr[40] = /*_.PROGN___V81*/ meltfptr[79];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-outobj.melt:6309:/ clear");
	       /*clear *//*_#MELT_NEED_DBG__L24*/ meltfnum[9] = 0;
	    /*^clear */
	       /*clear *//*_.IF___V79*/ meltfptr[75] = 0;
	    /*^clear */
	       /*clear *//*_.PROGN___V81*/ meltfptr[79] = 0;
	  }

#else /*MELT_HAVE_DEBUG */
	  /*^cppif.else */
	  /*_.IFCPP___V78*/ meltfptr[40] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
	  ;

#if MELT_HAVE_DEBUG
	  MELT_LOCATION ("warmelt-outobj.melt:6313:/ cppif.then");
	  /*^block */
	  /*anyblock */
	  {

	    /*^checksignal */
	    MELT_CHECK_SIGNAL ();
	    ;
     /*_#__L26*/ meltfnum[12] =
	      (( /*_.BINDLIST__V3*/ meltfptr[2]) !=
	       ( /*_.NORMLIST__V2*/ meltfptr[1]));;
	    MELT_LOCATION ("warmelt-outobj.melt:6313:/ cond");
	    /*cond */ if ( /*_#__L26*/ meltfnum[12])	/*then */
	      {
		/*^cond.then */
		/*_.IFELSE___V83*/ meltfptr[79] = ( /*nil */ NULL);;
	      }
	    else
	      {
		MELT_LOCATION ("warmelt-outobj.melt:6313:/ cond.else");

		/*^block */
		/*anyblock */
		{




		  {
		    /*^locexp */
		    melt_assert_failed (("check bindlist != normlist"),
					("warmelt-outobj.melt")
					? ("warmelt-outobj.melt") : __FILE__,
					(6313) ? (6313) : __LINE__,
					__FUNCTION__);
		    ;
		  }
		  ;
		 /*clear *//*_.IFELSE___V83*/ meltfptr[79] = 0;
		  /*epilog */
		}
		;
	      }
	    ;
	    /*^compute */
	    /*_.IFCPP___V82*/ meltfptr[75] = /*_.IFELSE___V83*/ meltfptr[79];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-outobj.melt:6313:/ clear");
	       /*clear *//*_#__L26*/ meltfnum[12] = 0;
	    /*^clear */
	       /*clear *//*_.IFELSE___V83*/ meltfptr[79] = 0;
	  }

#else /*MELT_HAVE_DEBUG */
	  /*^cppif.else */
	  /*_.IFCPP___V82*/ meltfptr[75] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
	  ;
	  MELT_LOCATION ("warmelt-outobj.melt:6314:/ compute");
	  /*_.LASTNORM__V20*/ meltfptr[19] = /*_.SETQ___V84*/ meltfptr[79] =
	    /*_.NFINLET__V69*/ meltfptr[39];;
	  MELT_LOCATION ("warmelt-outobj.melt:6315:/ compute");
	  /*_.LASTBINDS__V21*/ meltfptr[20] = /*_.SETQ___V85*/ meltfptr[84] =
	    ( /*nil */ NULL);;
	  /*_.LET___V68*/ meltfptr[31] = /*_.SETQ___V85*/ meltfptr[84];;

	  MELT_LOCATION ("warmelt-outobj.melt:6300:/ clear");
	     /*clear *//*_.NFINLET__V69*/ meltfptr[39] = 0;
	  /*^clear */
	     /*clear *//*_.IFCPP___V70*/ meltfptr[30] = 0;
	  /*^clear */
	     /*clear *//*_.IFCPP___V74*/ meltfptr[56] = 0;
	  /*^clear */
	     /*clear *//*_.IFCPP___V78*/ meltfptr[40] = 0;
	  /*^clear */
	     /*clear *//*_.IFCPP___V82*/ meltfptr[75] = 0;
	  /*^clear */
	     /*clear *//*_.SETQ___V84*/ meltfptr[79] = 0;
	  /*^clear */
	     /*clear *//*_.SETQ___V85*/ meltfptr[84] = 0;
	  /*_.IF___V67*/ meltfptr[32] = /*_.LET___V68*/ meltfptr[31];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-outobj.melt:6299:/ clear");
	     /*clear *//*_.LET___V68*/ meltfptr[31] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

  /*_.IF___V67*/ meltfptr[32] = NULL;;
      }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:6317:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^cond */
    /*cond */ if (
		   /*ifisa */
		   melt_is_instance_of ((melt_ptr_t)
					(( /*~REFNORMLIST */ meltfclos->
					  tabval[0])),
					(melt_ptr_t) (( /*!CLASS_REFERENCE */
						       meltfrout->tabval[1])))
      )				/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  /*^putslot */
	  /*putslot */
	  melt_assertmsg ("putslot checkobj @REFERENCED_VALUE",
			  melt_magic_discr ((melt_ptr_t)
					    (( /*~REFNORMLIST */ meltfclos->
					      tabval[0]))) ==
			  MELTOBMAG_OBJECT);
	  melt_putfield_object ((( /*~REFNORMLIST */ meltfclos->tabval[0])),
				(0), ( /*_.NORMLIST__V2*/ meltfptr[1]),
				"REFERENCED_VALUE");
	  ;
	  /*^touch */
	  meltgc_touch (( /*~REFNORMLIST */ meltfclos->tabval[0]));
	  ;
	  /*^touchobj */

	  melt_dbgtrace_written_object (( /*~REFNORMLIST */ meltfclos->
					 tabval[0]), "put-fields");
	  ;
	  /*epilog */
	}
	;
      }				/*noelse */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:6318:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L27*/ meltfnum[0] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-outobj.melt:6318:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L27*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L28*/ meltfnum[9] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-outobj.melt:6318:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[7];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long =
		/*_#THE_MELTCALLCOUNT__L28*/ meltfnum[9];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-outobj.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 6318;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring =
		"translate_run_melt_expressions/normalextend final normlist=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.NORMLIST__V2*/ meltfptr[1];
	      /*^apply.arg */
	      argtab[5].meltbp_cstring = "\n final bindlist=";
	      /*^apply.arg */
	      argtab[6].meltbp_aptr =
		(melt_ptr_t *) & /*_.BINDLIST__V3*/ meltfptr[2];
	      /*_.MELT_DEBUG_FUN__V88*/ meltfptr[56] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V87*/ meltfptr[30] =
	      /*_.MELT_DEBUG_FUN__V88*/ meltfptr[56];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-outobj.melt:6318:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L28*/ meltfnum[9] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V88*/ meltfptr[56] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V87*/ meltfptr[30] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-outobj.melt:6318:/ quasiblock");


      /*_.PROGN___V89*/ meltfptr[40] = /*_.IF___V87*/ meltfptr[30];;
      /*^compute */
      /*_.IFCPP___V86*/ meltfptr[39] = /*_.PROGN___V89*/ meltfptr[40];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:6318:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L27*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IF___V87*/ meltfptr[30] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V89*/ meltfptr[40] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V86*/ meltfptr[39] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-outobj.melt:6320:/ quasiblock");


 /*_.RETVAL___V1*/ meltfptr[0] = NULL;;

    {
      MELT_LOCATION ("warmelt-outobj.melt:6320:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*_.LET___V17*/ meltfptr[7] = /*_.RETURN___V90*/ meltfptr[75];;

    MELT_LOCATION ("warmelt-outobj.melt:6251:/ clear");
	   /*clear *//*_.LASTNORMPAIR__V18*/ meltfptr[15] = 0;
    /*^clear */
	   /*clear *//*_.LASTBINDPAIR__V19*/ meltfptr[18] = 0;
    /*^clear */
	   /*clear *//*_.LASTNORM__V20*/ meltfptr[19] = 0;
    /*^clear */
	   /*clear *//*_.LASTBINDS__V21*/ meltfptr[20] = 0;
    /*^clear */
	   /*clear *//*_.LASTCTYP__V22*/ meltfptr[21] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V23*/ meltfptr[22] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V27*/ meltfptr[23] = 0;
    /*^clear */
	   /*clear *//*_#__L8*/ meltfnum[1] = 0;
    /*^clear */
	   /*clear *//*_.IFELSE___V29*/ meltfptr[24] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V62*/ meltfptr[29] = 0;
    /*^clear */
	   /*clear *//*_.LIST_FIRST__V66*/ meltfptr[38] = 0;
    /*^clear */
	   /*clear *//*_.IF___V67*/ meltfptr[32] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V86*/ meltfptr[39] = 0;
    /*^clear */
	   /*clear *//*_.RETURN___V90*/ meltfptr[75] = 0;
    MELT_LOCATION ("warmelt-outobj.melt:6243:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.LET___V17*/ meltfptr[7];;

    {
      MELT_LOCATION ("warmelt-outobj.melt:6243:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V7*/ meltfptr[6] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V12*/ meltfptr[9] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V14*/ meltfptr[8] = 0;
    /*^clear */
	   /*clear *//*_.LET___V17*/ meltfptr[7] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("LAMBDA_", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_142_warmelt_outobj_LAMBDA___31___melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_142_warmelt_outobj_LAMBDA___31__ */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_143_warmelt_outobj_READ_MELT_EXPRESSIONS (meltclosure_ptr_t
						   meltclosp_,
						   melt_ptr_t meltfirstargp_,
						   const melt_argdescr_cell_t
						   meltxargdescr_[],
						   union meltparam_un *
						   meltxargtab_,
						   const melt_argdescr_cell_t
						   meltxresdescr_[],
						   union meltparam_un *
						   meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_143_warmelt_outobj_READ_MELT_EXPRESSIONS_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_143_warmelt_outobj_READ_MELT_EXPRESSIONS_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 33
    melt_ptr_t mcfr_varptr[33];
#define MELTFRAM_NBVARNUM 22
    long mcfr_varnum[22];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_143_warmelt_outobj_READ_MELT_EXPRESSIONS is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_143_warmelt_outobj_READ_MELT_EXPRESSIONS_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 33; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_143_warmelt_outobj_READ_MELT_EXPRESSIONS nbval 33*/
  meltfram__.mcfr_nbvar = 33 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("READ_MELT_EXPRESSIONS", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-outobj.melt:6383:/ getarg");
 /*_.SRC__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.RLIST__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.RLIST__V3*/ meltfptr[2])) !=
	      NULL);


  /*getarg#2 */
  /*^getarg */
  if (meltxargdescr_[1] != MELTBPAR_LONG)
    goto lab_endgetargs;
 /*_#DEPTH__L1*/ meltfnum[0] = meltxargtab_[1].meltbp_long;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:6384:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L2*/ meltfnum[1] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-outobj.melt:6384:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L2*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L3*/ meltfnum[2] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    /*^compute */
     /*_#LIST_LENGTH__L4*/ meltfnum[3] =
	      (melt_list_length
	       ((melt_ptr_t) ( /*_.RLIST__V3*/ meltfptr[2])));;
	    MELT_LOCATION ("warmelt-outobj.melt:6384:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[9];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L3*/ meltfnum[2];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-outobj.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 6384;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "read_melt_expressions src=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.SRC__V2*/ meltfptr[1];
	      /*^apply.arg */
	      argtab[5].meltbp_cstring = " depth=";
	      /*^apply.arg */
	      argtab[6].meltbp_long = /*_#DEPTH__L1*/ meltfnum[0];
	      /*^apply.arg */
	      argtab[7].meltbp_cstring = " rlist.len=";
	      /*^apply.arg */
	      argtab[8].meltbp_long = /*_#LIST_LENGTH__L4*/ meltfnum[3];
	      /*_.MELT_DEBUG_FUN__V6*/ meltfptr[5] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V5*/ meltfptr[4] = /*_.MELT_DEBUG_FUN__V6*/ meltfptr[5];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-outobj.melt:6384:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L3*/ meltfnum[2] = 0;
	    /*^clear */
	       /*clear *//*_#LIST_LENGTH__L4*/ meltfnum[3] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V6*/ meltfptr[5] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V5*/ meltfptr[4] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-outobj.melt:6384:/ quasiblock");


      /*_.PROGN___V7*/ meltfptr[5] = /*_.IF___V5*/ meltfptr[4];;
      /*^compute */
      /*_.IFCPP___V4*/ meltfptr[3] = /*_.PROGN___V7*/ meltfptr[5];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:6384:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L2*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IF___V5*/ meltfptr[4] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V7*/ meltfptr[5] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V4*/ meltfptr[3] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:6385:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_LIST__L5*/ meltfnum[2] =
	(melt_magic_discr ((melt_ptr_t) ( /*_.RLIST__V3*/ meltfptr[2])) ==
	 MELTOBMAG_LIST);;
      MELT_LOCATION ("warmelt-outobj.melt:6385:/ cond");
      /*cond */ if ( /*_#IS_LIST__L5*/ meltfnum[2])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V9*/ meltfptr[5] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-outobj.melt:6385:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check rlist"),
				  ("warmelt-outobj.melt")
				  ? ("warmelt-outobj.melt") : __FILE__,
				  (6385) ? (6385) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V9*/ meltfptr[5] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V8*/ meltfptr[4] = /*_.IFELSE___V9*/ meltfptr[5];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:6385:/ clear");
	     /*clear *//*_#IS_LIST__L5*/ meltfnum[2] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V9*/ meltfptr[5] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V8*/ meltfptr[4] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:6386:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#I__L6*/ meltfnum[3] =
	(( /*_#DEPTH__L1*/ meltfnum[0]) < (100));;
      MELT_LOCATION ("warmelt-outobj.melt:6386:/ cond");
      /*cond */ if ( /*_#I__L6*/ meltfnum[3])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V11*/ meltfptr[10] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-outobj.melt:6386:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check depth"),
				  ("warmelt-outobj.melt")
				  ? ("warmelt-outobj.melt") : __FILE__,
				  (6386) ? (6386) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V11*/ meltfptr[10] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V10*/ meltfptr[5] = /*_.IFELSE___V11*/ meltfptr[10];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:6386:/ clear");
	     /*clear *//*_#I__L6*/ meltfnum[3] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V11*/ meltfptr[10] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V10*/ meltfptr[5] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-outobj.melt:6388:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#NULL__L7*/ meltfnum[1] =
      (( /*_.SRC__V2*/ meltfptr[1]) == NULL);;
    MELT_LOCATION ("warmelt-outobj.melt:6388:/ cond");
    /*cond */ if ( /*_#NULL__L7*/ meltfnum[1])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-outobj.melt:6389:/ quasiblock");


   /*_.RETVAL___V1*/ meltfptr[0] = NULL;;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:6389:/ locexp");
	    /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
	    if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	      melt_warn_for_no_expected_secondary_results ();
	    /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
	    ;
	  }
	  ;
	  /*^finalreturn */
	  ;
	  /*finalret */ goto labend_rout;
	  /*_.IFELSE___V12*/ meltfptr[10] = /*_.RETURN___V13*/ meltfptr[12];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-outobj.melt:6388:/ clear");
	     /*clear *//*_.RETURN___V13*/ meltfptr[12] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-outobj.melt:6390:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
   /*_#IS_STRING__L8*/ meltfnum[2] =
	    (melt_magic_discr ((melt_ptr_t) ( /*_.SRC__V2*/ meltfptr[1])) ==
	     MELTOBMAG_STRING);;
	  MELT_LOCATION ("warmelt-outobj.melt:6390:/ cond");
	  /*cond */ if ( /*_#IS_STRING__L8*/ meltfnum[2])	/*then */
	    {
	      /*^cond.then */
	      /*^block */
	      /*anyblock */
	      {


		{
		  MELT_LOCATION ("warmelt-outobj.melt:6391:/ locexp");
		  inform (UNKNOWN_LOCATION, "MELT INFORM [#%ld]: %s - %s",
			  melt_dbgcounter, ("reading from MELT file source"),
			  melt_string_str ((melt_ptr_t)
					   ( /*_.SRC__V2*/ meltfptr[1])));
		}
		;
     /*_.READ_FILE__V15*/ meltfptr[14] =
		  (meltgc_read_file
		   (melt_string_str
		    ((melt_ptr_t) ( /*_.SRC__V2*/ meltfptr[1])),
		    (char *) 0));;
		MELT_LOCATION ("warmelt-outobj.melt:6392:/ checksignal");
		MELT_CHECK_SIGNAL ();
		;
		/*^apply */
		/*apply */
		{
		  union meltparam_un argtab[1];
		  memset (&argtab, 0, sizeof (argtab));
		  /*^apply.arg */
		  argtab[0].meltbp_aptr =
		    (melt_ptr_t *) & /*_.READ_FILE__V15*/ meltfptr[14];
		  /*_.LIST_APPEND2LIST__V16*/ meltfptr[15] =
		    melt_apply ((meltclosure_ptr_t)
				(( /*!LIST_APPEND2LIST */ meltfrout->
				  tabval[1])),
				(melt_ptr_t) ( /*_.RLIST__V3*/ meltfptr[2]),
				(MELTBPARSTR_PTR ""), argtab, "",
				(union meltparam_un *) 0);
		}
		;
		MELT_LOCATION ("warmelt-outobj.melt:6390:/ quasiblock");


		/*_.PROGN___V17*/ meltfptr[16] =
		  /*_.LIST_APPEND2LIST__V16*/ meltfptr[15];;
		/*^compute */
		/*_.IFELSE___V14*/ meltfptr[12] =
		  /*_.PROGN___V17*/ meltfptr[16];;
		/*epilog */

		MELT_LOCATION ("warmelt-outobj.melt:6390:/ clear");
	       /*clear *//*_.READ_FILE__V15*/ meltfptr[14] = 0;
		/*^clear */
	       /*clear *//*_.LIST_APPEND2LIST__V16*/ meltfptr[15] = 0;
		/*^clear */
	       /*clear *//*_.PROGN___V17*/ meltfptr[16] = 0;
	      }
	      ;
	    }
	  else
	    {			/*^cond.else */

	      /*^block */
	      /*anyblock */
	      {

		MELT_LOCATION ("warmelt-outobj.melt:6393:/ checksignal");
		MELT_CHECK_SIGNAL ();
		;
     /*_#IS_LIST__L9*/ meltfnum[3] =
		  (melt_magic_discr
		   ((melt_ptr_t) ( /*_.SRC__V2*/ meltfptr[1])) ==
		   MELTOBMAG_LIST);;
		MELT_LOCATION ("warmelt-outobj.melt:6393:/ cond");
		/*cond */ if ( /*_#IS_LIST__L9*/ meltfnum[3])	/*then */
		  {
		    /*^cond.then */
		    /*^block */
		    /*anyblock */
		    {

		      /*citerblock FOREACH_IN_LIST */
		      {
			/* start foreach_in_list meltcit1__EACHLIST */
			for ( /*_.CURPAIR__V18*/ meltfptr[14] =
			     melt_list_first ((melt_ptr_t) /*_.SRC__V2*/
					      meltfptr[1]);
			     melt_magic_discr ((melt_ptr_t) /*_.CURPAIR__V18*/
					       meltfptr[14]) ==
			     MELTOBMAG_PAIR;
			     /*_.CURPAIR__V18*/ meltfptr[14] =
			     melt_pair_tail ((melt_ptr_t) /*_.CURPAIR__V18*/
					     meltfptr[14]))
			  {
			    /*_.CURSRC__V19*/ meltfptr[15] =
			      melt_pair_head ((melt_ptr_t) /*_.CURPAIR__V18*/
					      meltfptr[14]);


	/*_#I__L10*/ meltfnum[9] =
			      (( /*_#DEPTH__L1*/ meltfnum[0]) + (1));;
			    MELT_LOCATION
			      ("warmelt-outobj.melt:6397:/ checksignal");
			    MELT_CHECK_SIGNAL ();
			    ;
			    /*^apply */
			    /*apply */
			    {
			      union meltparam_un argtab[2];
			      memset (&argtab, 0, sizeof (argtab));
			      /*^apply.arg */
			      argtab[0].meltbp_aptr =
				(melt_ptr_t *) & /*_.RLIST__V3*/ meltfptr[2];
			      /*^apply.arg */
			      argtab[1].meltbp_long =
				/*_#I__L10*/ meltfnum[9];
			      /*_.READ_MELT_EXPRESSIONS__V20*/ meltfptr[16] =
				melt_apply ((meltclosure_ptr_t)
					    (( /*!READ_MELT_EXPRESSIONS */
					      meltfrout->tabval[2])),
					    (melt_ptr_t) ( /*_.CURSRC__V19*/
							  meltfptr[15]),
					    (MELTBPARSTR_PTR MELTBPARSTR_LONG
					     ""), argtab, "",
					    (union meltparam_un *) 0);
			    }
			    ;
			    /*_.IFELSE___V14*/ meltfptr[12] =
			      /*_.READ_MELT_EXPRESSIONS__V20*/ meltfptr[16];;
			  }	/* end foreach_in_list meltcit1__EACHLIST */
     /*_.CURPAIR__V18*/ meltfptr[14] = NULL;
     /*_.CURSRC__V19*/ meltfptr[15] = NULL;


			/*citerepilog */

			MELT_LOCATION ("warmelt-outobj.melt:6394:/ clear");
		  /*clear *//*_.CURPAIR__V18*/ meltfptr[14] = 0;
			/*^clear */
		  /*clear *//*_.CURSRC__V19*/ meltfptr[15] = 0;
			/*^clear */
		  /*clear *//*_#I__L10*/ meltfnum[9] = 0;
			/*^clear */
		  /*clear *//*_.READ_MELT_EXPRESSIONS__V20*/
			  meltfptr[16] = 0;
		      }		/*endciterblock FOREACH_IN_LIST */
		      ;
		      /*epilog */
		    }
		    ;
		  }
		else
		  {
		    MELT_LOCATION ("warmelt-outobj.melt:6393:/ cond.else");

		    /*^block */
		    /*anyblock */
		    {

		      MELT_LOCATION
			("warmelt-outobj.melt:6398:/ checksignal");
		      MELT_CHECK_SIGNAL ();
		      ;
       /*_#IS_MULTIPLE__L11*/ meltfnum[10] =
			(melt_magic_discr
			 ((melt_ptr_t) ( /*_.SRC__V2*/ meltfptr[1])) ==
			 MELTOBMAG_MULTIPLE);;
		      MELT_LOCATION ("warmelt-outobj.melt:6398:/ cond");
		      /*cond */ if ( /*_#IS_MULTIPLE__L11*/ meltfnum[10])	/*then */
			{
			  /*^cond.then */
			  /*^block */
			  /*anyblock */
			  {

			    /*citerblock FOREACH_IN_MULTIPLE */
			    {
			      /* start foreach_in_multiple meltcit2__EACHTUP */
			      long meltcit2__EACHTUP_ln =
				melt_multiple_length ((melt_ptr_t)
						      /*_.SRC__V2*/
						      meltfptr[1]);
			      for ( /*_#SIX__L12*/ meltfnum[11] = 0;
				   ( /*_#SIX__L12*/ meltfnum[11] >= 0)
				   && ( /*_#SIX__L12*/ meltfnum[11] <
				       meltcit2__EACHTUP_ln);
	/*_#SIX__L12*/ meltfnum[11]++)
				{
				  /*_.CURSRC__V21*/ meltfptr[20] =
				    melt_multiple_nth ((melt_ptr_t)
						       ( /*_.SRC__V2*/
							meltfptr[1]),
						       /*_#SIX__L12*/
						       meltfnum[11]);



	  /*_#I__L13*/ meltfnum[12] =
				    (( /*_#DEPTH__L1*/ meltfnum[0]) + (1));;
				  MELT_LOCATION
				    ("warmelt-outobj.melt:6402:/ checksignal");
				  MELT_CHECK_SIGNAL ();
				  ;
				  /*^apply */
				  /*apply */
				  {
				    union meltparam_un argtab[2];
				    memset (&argtab, 0, sizeof (argtab));
				    /*^apply.arg */
				    argtab[0].meltbp_aptr =
				      (melt_ptr_t *) & /*_.RLIST__V3*/
				      meltfptr[2];
				    /*^apply.arg */
				    argtab[1].meltbp_long =
				      /*_#I__L13*/ meltfnum[12];
				    /*_.READ_MELT_EXPRESSIONS__V22*/
				      meltfptr[21] =
				      melt_apply ((meltclosure_ptr_t)
						  (( /*!READ_MELT_EXPRESSIONS */ meltfrout->tabval[2])), (melt_ptr_t) ( /*_.CURSRC__V21*/ meltfptr[20]), (MELTBPARSTR_PTR MELTBPARSTR_LONG ""), argtab, "", (union meltparam_un *) 0);
				  }
				  ;
				  /*_.IFELSE___V14*/ meltfptr[12] =
				    /*_.READ_MELT_EXPRESSIONS__V22*/
				    meltfptr[21];;
				  if ( /*_#SIX__L12*/ meltfnum[11] < 0)
				    break;
				}	/* end  foreach_in_multiple meltcit2__EACHTUP */

			      /*citerepilog */

			      MELT_LOCATION
				("warmelt-outobj.melt:6399:/ clear");
		    /*clear *//*_.CURSRC__V21*/ meltfptr[20] = 0;
			      /*^clear */
		    /*clear *//*_#SIX__L12*/ meltfnum[11] = 0;
			      /*^clear */
		    /*clear *//*_#I__L13*/ meltfnum[12] = 0;
			      /*^clear */
		    /*clear *//*_.READ_MELT_EXPRESSIONS__V22*/
				meltfptr[21] = 0;
			    }	/*endciterblock FOREACH_IN_MULTIPLE */
			    ;
			    /*epilog */
			  }
			  ;
			}
		      else
			{
			  MELT_LOCATION
			    ("warmelt-outobj.melt:6398:/ cond.else");

			  /*^block */
			  /*anyblock */
			  {

			    MELT_LOCATION
			      ("warmelt-outobj.melt:6403:/ checksignal");
			    MELT_CHECK_SIGNAL ();
			    ;
	 /*_#IS_OBJECT__L14*/ meltfnum[13] =
			      (melt_magic_discr
			       ((melt_ptr_t) ( /*_.SRC__V2*/ meltfptr[1])) ==
			       MELTOBMAG_OBJECT);;
			    MELT_LOCATION ("warmelt-outobj.melt:6403:/ cond");
			    /*cond */ if ( /*_#IS_OBJECT__L14*/ meltfnum[13])	/*then */
			      {
				/*^cond.then */
				/*^block */
				/*anyblock */
				{




				  {
				    MELT_LOCATION
				      ("warmelt-outobj.melt:6404:/ locexp");
				    meltgc_append_list ((melt_ptr_t)
							( /*_.RLIST__V3*/
							 meltfptr[2]),
							(melt_ptr_t) ( /*_.SRC__V2*/ meltfptr[1]));
				  }
				  ;
		     /*clear *//*_.IFELSE___V14*/ meltfptr[12] =
				    0;
				  /*epilog */
				}
				;
			      }
			    else
			      {
				MELT_LOCATION
				  ("warmelt-outobj.melt:6403:/ cond.else");

				/*^block */
				/*anyblock */
				{

				  MELT_LOCATION
				    ("warmelt-outobj.melt:6405:/ checksignal");
				  MELT_CHECK_SIGNAL ();
				  ;
	   /*_#IS_CLOSURE__L15*/ meltfnum[14] =
				    (melt_magic_discr
				     ((melt_ptr_t)
				      ( /*_.SRC__V2*/ meltfptr[1])) ==
				     MELTOBMAG_CLOSURE);;
				  MELT_LOCATION
				    ("warmelt-outobj.melt:6405:/ cond");
				  /*cond */ if ( /*_#IS_CLOSURE__L15*/ meltfnum[14])	/*then */
				    {
				      /*^cond.then */
				      /*^block */
				      /*anyblock */
				      {

					MELT_LOCATION
					  ("warmelt-outobj.melt:6406:/ quasiblock");


	     /*_#I__L16*/ meltfnum[15] =
					  (( /*_#DEPTH__L1*/ meltfnum[0]) +
					   (1));;
					MELT_LOCATION
					  ("warmelt-outobj.melt:6406:/ checksignal");
					MELT_CHECK_SIGNAL ();
					;
					/*^apply */
					/*apply */
					{
					  union meltparam_un argtab[1];
					  memset (&argtab, 0,
						  sizeof (argtab));
					  /*^apply.arg */
					  argtab[0].meltbp_long =
					    /*_#I__L16*/ meltfnum[15];
					  /*_.XSRC__V23*/ meltfptr[22] =
					    melt_apply ((meltclosure_ptr_t)
							( /*_.SRC__V2*/
							 meltfptr[1]),
							(melt_ptr_t) ( /*_.RLIST__V3*/ meltfptr[2]), (MELTBPARSTR_LONG ""), argtab, "", (union meltparam_un *) 0);
					}
					;

#if MELT_HAVE_DEBUG
					MELT_LOCATION
					  ("warmelt-outobj.melt:6408:/ cppif.then");
					/*^block */
					/*anyblock */
					{


					  {
					    /*^locexp */
					    /*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
					    melt_dbgcounter++;
#endif
					    ;
					  }
					  ;
					  /*^checksignal */
					  MELT_CHECK_SIGNAL ();
					  ;
	       /*_#MELT_NEED_DBG__L17*/
					    meltfnum[16] =
					    /*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
					    ( /*melt_need_dbg */
					     melt_need_debug ((int) 0))
#else
					    0	/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
					    ;;
					  MELT_LOCATION
					    ("warmelt-outobj.melt:6408:/ cond");
					  /*cond */ if ( /*_#MELT_NEED_DBG__L17*/ meltfnum[16])	/*then */
					    {
					      /*^cond.then */
					      /*^block */
					      /*anyblock */
					      {

		 /*_#THE_MELTCALLCOUNT__L18*/
						  meltfnum[17] =
#ifdef meltcallcount
						  meltcallcount	/* the_meltcallcount */
#else
						  0L
#endif /* meltcallcount the_meltcallcount */
						  ;;
						MELT_LOCATION
						  ("warmelt-outobj.melt:6408:/ checksignal");
						MELT_CHECK_SIGNAL ();
						;
						/*^apply */
						/*apply */
						{
						  union meltparam_un
						    argtab[7];
						  memset (&argtab, 0,
							  sizeof (argtab));
						  /*^apply.arg */
						  argtab[0].meltbp_long =
						    /*_#THE_MELTCALLCOUNT__L18*/
						    meltfnum[17];
						  /*^apply.arg */
						  argtab[1].meltbp_cstring =
						    "warmelt-outobj.melt";
						  /*^apply.arg */
						  argtab[2].meltbp_long =
						    6408;
						  /*^apply.arg */
						  argtab[3].meltbp_cstring =
						    "read_melt_expressions xsrc=";
						  /*^apply.arg */
						  argtab[4].meltbp_aptr =
						    (melt_ptr_t *) &
						    /*_.XSRC__V23*/
						    meltfptr[22];
						  /*^apply.arg */
						  argtab[5].meltbp_cstring =
						    " depth=";
						  /*^apply.arg */
						  argtab[6].meltbp_long =
						    /*_#DEPTH__L1*/
						    meltfnum[0];
						  /*_.MELT_DEBUG_FUN__V26*/
						    meltfptr[25] =
						    melt_apply ((meltclosure_ptr_t) (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])), (melt_ptr_t) (( /*nil */ NULL)), (MELTBPARSTR_LONG MELTBPARSTR_CSTRING MELTBPARSTR_LONG MELTBPARSTR_CSTRING MELTBPARSTR_PTR MELTBPARSTR_CSTRING MELTBPARSTR_LONG ""), argtab, "", (union meltparam_un *) 0);
						}
						;
						/*_.IF___V25*/ meltfptr[24] =
						  /*_.MELT_DEBUG_FUN__V26*/
						  meltfptr[25];;
						/*epilog */

						MELT_LOCATION
						  ("warmelt-outobj.melt:6408:/ clear");
			   /*clear *//*_#THE_MELTCALLCOUNT__L18*/
						  meltfnum[17] = 0;
						/*^clear */
			   /*clear *//*_.MELT_DEBUG_FUN__V26*/
						  meltfptr[25] = 0;
					      }
					      ;
					    }
					  else
					    {	/*^cond.else */

		/*_.IF___V25*/ meltfptr[24] =
						NULL;;
					    }
					  ;
					  MELT_LOCATION
					    ("warmelt-outobj.melt:6408:/ quasiblock");


					  /*_.PROGN___V27*/ meltfptr[25] =
					    /*_.IF___V25*/ meltfptr[24];;
					  /*^compute */
					  /*_.IFCPP___V24*/ meltfptr[23] =
					    /*_.PROGN___V27*/ meltfptr[25];;
					  /*epilog */

					  MELT_LOCATION
					    ("warmelt-outobj.melt:6408:/ clear");
			 /*clear *//*_#MELT_NEED_DBG__L17*/
					    meltfnum[16] = 0;
					  /*^clear */
			 /*clear *//*_.IF___V25*/
					    meltfptr[24] = 0;
					  /*^clear */
			 /*clear *//*_.PROGN___V27*/
					    meltfptr[25] = 0;
					}

#else /*MELT_HAVE_DEBUG */
					/*^cppif.else */
					/*_.IFCPP___V24*/ meltfptr[23] =
					  ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
					;
					MELT_LOCATION
					  ("warmelt-outobj.melt:6409:/ checksignal");
					MELT_CHECK_SIGNAL ();
					;
	     /*_#NULL__L19*/ meltfnum[17] =
					  (( /*_.XSRC__V23*/ meltfptr[22]) ==
					   NULL);;
					MELT_LOCATION
					  ("warmelt-outobj.melt:6409:/ cond");
					/*cond */ if ( /*_#NULL__L19*/ meltfnum[17])	/*then */
					  {
					    /*^cond.then */
					    /*^block */
					    /*anyblock */
					    {

					      /*^quasiblock */


	       /*_.RETVAL___V1*/ meltfptr[0] =
						NULL;;

					      {
						MELT_LOCATION
						  ("warmelt-outobj.melt:6409:/ locexp");
						/*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
						if (meltxresdescr_
						    && meltxresdescr_[0]
						    && meltxrestab_)
						  melt_warn_for_no_expected_secondary_results
						    ();
						/* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
						;
					      }
					      ;
					      /*^finalreturn */
					      ;
					      /*finalret */ goto labend_rout;
					      /*_.IF___V28*/ meltfptr[24] =
						/*_.RETURN___V29*/
						meltfptr[25];;
					      /*epilog */

					      MELT_LOCATION
						("warmelt-outobj.melt:6409:/ clear");
			 /*clear *//*_.RETURN___V29*/
						meltfptr[25] = 0;
					    }
					    ;
					  }
					else
					  {	/*^cond.else */

	      /*_.IF___V28*/ meltfptr[24] =
					      NULL;;
					  }
					;

#if MELT_HAVE_DEBUG
					MELT_LOCATION
					  ("warmelt-outobj.melt:6410:/ cppif.then");
					/*^block */
					/*anyblock */
					{

					  /*^checksignal */
					  MELT_CHECK_SIGNAL ();
					  ;
	       /*_#IS_A__L20*/ meltfnum[16] =
					    melt_is_instance_of ((melt_ptr_t)
								 ( /*_.XSRC__V23*/ meltfptr[22]), (melt_ptr_t) (( /*!CLASS_SEXPR */ meltfrout->tabval[4])));;
					  MELT_LOCATION
					    ("warmelt-outobj.melt:6410:/ cond");
					  /*cond */ if ( /*_#IS_A__L20*/ meltfnum[16])	/*then */
					    {
					      /*^cond.then */
					      /*_#OR___L21*/ meltfnum[20] =
						/*_#IS_A__L20*/ meltfnum[16];;
					    }
					  else
					    {
					      MELT_LOCATION
						("warmelt-outobj.melt:6410:/ cond.else");

					      /*^block */
					      /*anyblock */
					      {

		 /*_#IS_A__L22*/ meltfnum[21] =
						  melt_is_instance_of ((melt_ptr_t) ( /*_.XSRC__V23*/ meltfptr[22]), (melt_ptr_t) (( /*!CLASS_SYMBOL */ meltfrout->tabval[3])));;
						/*^compute */
						/*_#OR___L21*/ meltfnum[20] =
						  /*_#IS_A__L22*/
						  meltfnum[21];;
						/*epilog */

						MELT_LOCATION
						  ("warmelt-outobj.melt:6410:/ clear");
			   /*clear *//*_#IS_A__L22*/
						  meltfnum[21] = 0;
					      }
					      ;
					    }
					  ;
					  /*^cond */
					  /*cond */ if ( /*_#OR___L21*/ meltfnum[20])	/*then */
					    {
					      /*^cond.then */
					      /*_.IFELSE___V31*/ meltfptr[30]
						= ( /*nil */ NULL);;
					    }
					  else
					    {
					      MELT_LOCATION
						("warmelt-outobj.melt:6410:/ cond.else");

					      /*^block */
					      /*anyblock */
					      {




						{
						  /*^locexp */
						  melt_assert_failed (("check xsrc"), ("warmelt-outobj.melt") ? ("warmelt-outobj.melt") : __FILE__, (6410) ? (6410) : __LINE__, __FUNCTION__);
						  ;
						}
						;
			   /*clear *//*_.IFELSE___V31*/
						  meltfptr[30] = 0;
						/*epilog */
					      }
					      ;
					    }
					  ;
					  /*^compute */
					  /*_.IFCPP___V30*/ meltfptr[25] =
					    /*_.IFELSE___V31*/ meltfptr[30];;
					  /*epilog */

					  MELT_LOCATION
					    ("warmelt-outobj.melt:6410:/ clear");
			 /*clear *//*_#IS_A__L20*/
					    meltfnum[16] = 0;
					  /*^clear */
			 /*clear *//*_#OR___L21*/
					    meltfnum[20] = 0;
					  /*^clear */
			 /*clear *//*_.IFELSE___V31*/
					    meltfptr[30] = 0;
					}

#else /*MELT_HAVE_DEBUG */
					/*^cppif.else */
					/*_.IFCPP___V30*/ meltfptr[25] =
					  ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
					;

					{
					  MELT_LOCATION
					    ("warmelt-outobj.melt:6411:/ locexp");
					  meltgc_append_list ((melt_ptr_t)
							      ( /*_.RLIST__V3*/ meltfptr[2]), (melt_ptr_t) ( /*_.XSRC__V23*/ meltfptr[22]));
					}
					;
					/*_.IFELSE___V14*/ meltfptr[12] =
					  /*_.IFCPP___V30*/ meltfptr[25];;

					MELT_LOCATION
					  ("warmelt-outobj.melt:6406:/ clear");
		       /*clear *//*_#I__L16*/ meltfnum[15] =
					  0;
					/*^clear */
		       /*clear *//*_.XSRC__V23*/ meltfptr[22]
					  = 0;
					/*^clear */
		       /*clear *//*_.IFCPP___V24*/
					  meltfptr[23] = 0;
					/*^clear */
		       /*clear *//*_#NULL__L19*/ meltfnum[17]
					  = 0;
					/*^clear */
		       /*clear *//*_.IF___V28*/ meltfptr[24]
					  = 0;
					/*^clear */
		       /*clear *//*_.IFCPP___V30*/
					  meltfptr[25] = 0;
					/*epilog */
				      }
				      ;
				    }
				  else
				    {
				      MELT_LOCATION
					("warmelt-outobj.melt:6405:/ cond.else");

				      /*^block */
				      /*anyblock */
				      {

	     /*_.DISCRIM__V32*/ meltfptr[30] =
					  ((melt_ptr_t)
					   (melt_discr
					    ((melt_ptr_t)
					     ( /*_.SRC__V2*/ meltfptr[1]))));;
					MELT_LOCATION
					  ("warmelt-outobj.melt:6414:/ cond");
					/*cond */ if (
						       /*ifisa */
						       melt_is_instance_of ((melt_ptr_t) ( /*_.DISCRIM__V32*/ meltfptr[30]),
									    (melt_ptr_t) (( /*!CLASS_NAMED */ meltfrout->tabval[5])))
					  )	/*then */
					  {
					    /*^cond.then */
					    /*^getslot */
					    {
					      melt_ptr_t slot = NULL, obj =
						NULL;
					      obj =
						(melt_ptr_t) ( /*_.DISCRIM__V32*/ meltfptr[30]) /*=obj*/ ;
					      melt_object_get_field (slot,
								     obj, 1,
								     "NAMED_NAME");
	       /*_.NAMED_NAME__V33*/
						meltfptr[22] = slot;
					    };
					    ;
					  }
					else
					  {	/*^cond.else */

	      /*_.NAMED_NAME__V33*/ meltfptr[22]
					      = NULL;;
					  }
					;

					{
					  MELT_LOCATION
					    ("warmelt-outobj.melt:6413:/ locexp");
					  melt_error_str ((melt_ptr_t)
							  (( /*nil */ NULL)),
							  ("invalid MELT expression to read, with discriminant"),
							  (melt_ptr_t) ( /*_.NAMED_NAME__V33*/ meltfptr[22]));
					}
					;
					MELT_LOCATION
					  ("warmelt-outobj.melt:6412:/ quasiblock");


					/*epilog */

					MELT_LOCATION
					  ("warmelt-outobj.melt:6405:/ clear");
		       /*clear *//*_.DISCRIM__V32*/
					  meltfptr[30] = 0;
					/*^clear */
		       /*clear *//*_.NAMED_NAME__V33*/
					  meltfptr[22] = 0;
				      }
				      ;
				    }
				  ;
				  /*epilog */

				  MELT_LOCATION
				    ("warmelt-outobj.melt:6403:/ clear");
		     /*clear *//*_#IS_CLOSURE__L15*/ meltfnum[14]
				    = 0;
				}
				;
			      }
			    ;
			    /*epilog */

			    MELT_LOCATION
			      ("warmelt-outobj.melt:6398:/ clear");
		   /*clear *//*_#IS_OBJECT__L14*/ meltfnum[13] = 0;
			  }
			  ;
			}
		      ;
		      /*epilog */

		      MELT_LOCATION ("warmelt-outobj.melt:6393:/ clear");
		 /*clear *//*_#IS_MULTIPLE__L11*/ meltfnum[10] = 0;
		    }
		    ;
		  }
		;
		/*epilog */

		MELT_LOCATION ("warmelt-outobj.melt:6390:/ clear");
	       /*clear *//*_#IS_LIST__L9*/ meltfnum[3] = 0;
	      }
	      ;
	    }
	  ;
	  /*_.IFELSE___V12*/ meltfptr[10] = /*_.IFELSE___V14*/ meltfptr[12];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-outobj.melt:6388:/ clear");
	     /*clear *//*_#IS_STRING__L8*/ meltfnum[2] = 0;
	  /*^clear */
	     /*clear *//*_.IFELSE___V14*/ meltfptr[12] = 0;
	}
	;
      }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:6383:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.IFELSE___V12*/ meltfptr[10];;

    {
      MELT_LOCATION ("warmelt-outobj.melt:6383:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V4*/ meltfptr[3] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V8*/ meltfptr[4] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V10*/ meltfptr[5] = 0;
    /*^clear */
	   /*clear *//*_#NULL__L7*/ meltfnum[1] = 0;
    /*^clear */
	   /*clear *//*_.IFELSE___V12*/ meltfptr[10] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("READ_MELT_EXPRESSIONS", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_143_warmelt_outobj_READ_MELT_EXPRESSIONS_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_143_warmelt_outobj_READ_MELT_EXPRESSIONS */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_144_warmelt_outobj_TRANSLATE_TO_C_MODULE_MELT_SOURCES
  (meltclosure_ptr_t meltclosp_, melt_ptr_t meltfirstargp_,
   const melt_argdescr_cell_t meltxargdescr_[],
   union meltparam_un * meltxargtab_,
   const melt_argdescr_cell_t meltxresdescr_[],
   union meltparam_un * meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_144_warmelt_outobj_TRANSLATE_TO_C_MODULE_MELT_SOURCES_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_144_warmelt_outobj_TRANSLATE_TO_C_MODULE_MELT_SOURCES_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 39
    melt_ptr_t mcfr_varptr[39];
#define MELTFRAM_NBVARNUM 25
    long mcfr_varnum[25];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_144_warmelt_outobj_TRANSLATE_TO_C_MODULE_MELT_SOURCES is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct
	 frame_meltrout_144_warmelt_outobj_TRANSLATE_TO_C_MODULE_MELT_SOURCES_st
	 *) meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 39; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_144_warmelt_outobj_TRANSLATE_TO_C_MODULE_MELT_SOURCES nbval 39*/
  meltfram__.mcfr_nbvar = 39 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("TRANSLATE_TO_C_MODULE_MELT_SOURCES", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-outobj.melt:6421:/ getarg");
 /*_.SOURCES__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.MODSRCNAME__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.MODSRCNAME__V3*/ meltfptr[2])) !=
	      NULL);


  /*getarg#2 */
  /*^getarg */
  if (meltxargdescr_[1] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.CURENV__V4*/ meltfptr[3] =
    (meltxargtab_[1].meltbp_aptr) ? (*(meltxargtab_[1].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.CURENV__V4*/ meltfptr[3])) !=
	      NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:6422:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L1*/ meltfnum[0] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-outobj.melt:6422:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L1*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-outobj.melt:6422:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[7];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-outobj.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 6422;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring =
		"translate_to_c_module_melt_sources sources=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.SOURCES__V2*/ meltfptr[1];
	      /*^apply.arg */
	      argtab[5].meltbp_cstring = " modsrcname=";
	      /*^apply.arg */
	      argtab[6].meltbp_aptr =
		(melt_ptr_t *) & /*_.MODSRCNAME__V3*/ meltfptr[2];
	      /*_.MELT_DEBUG_FUN__V7*/ meltfptr[6] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V6*/ meltfptr[5] = /*_.MELT_DEBUG_FUN__V7*/ meltfptr[6];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-outobj.melt:6422:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V7*/ meltfptr[6] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V6*/ meltfptr[5] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-outobj.melt:6422:/ quasiblock");


      /*_.PROGN___V8*/ meltfptr[6] = /*_.IF___V6*/ meltfptr[5];;
      /*^compute */
      /*_.IFCPP___V5*/ meltfptr[4] = /*_.PROGN___V8*/ meltfptr[6];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:6422:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L1*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IF___V6*/ meltfptr[5] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V8*/ meltfptr[6] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V5*/ meltfptr[4] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:6423:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L3*/ meltfnum[1] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.CURENV__V4*/ meltfptr[3]),
			     (melt_ptr_t) (( /*!CLASS_ENVIRONMENT */
					    meltfrout->tabval[1])));;
      MELT_LOCATION ("warmelt-outobj.melt:6423:/ cond");
      /*cond */ if ( /*_#IS_A__L3*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V10*/ meltfptr[6] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-outobj.melt:6423:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check curenv"),
				  ("warmelt-outobj.melt")
				  ? ("warmelt-outobj.melt") : __FILE__,
				  (6423) ? (6423) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V10*/ meltfptr[6] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V9*/ meltfptr[5] = /*_.IFELSE___V10*/ meltfptr[6];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:6423:/ clear");
	     /*clear *//*_#IS_A__L3*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V10*/ meltfptr[6] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V9*/ meltfptr[5] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:6424:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_STRING__L4*/ meltfnum[0] =
	(melt_magic_discr ((melt_ptr_t) ( /*_.MODSRCNAME__V3*/ meltfptr[2]))
	 == MELTOBMAG_STRING);;
      MELT_LOCATION ("warmelt-outobj.melt:6424:/ cond");
      /*cond */ if ( /*_#IS_STRING__L4*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V12*/ meltfptr[11] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-outobj.melt:6424:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check modsrcname"),
				  ("warmelt-outobj.melt")
				  ? ("warmelt-outobj.melt") : __FILE__,
				  (6424) ? (6424) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V12*/ meltfptr[11] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V11*/ meltfptr[6] = /*_.IFELSE___V12*/ meltfptr[11];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:6424:/ clear");
	     /*clear *//*_#IS_STRING__L4*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V12*/ meltfptr[11] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V11*/ meltfptr[6] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-outobj.melt:6425:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#STRING_DYNLOADED_SUFFIXED__L5*/ meltfnum[1] =
      /*string_dynloaded_suffixed: */
      (melt_string_is_ending ((melt_ptr_t) /*_.MODSRCNAME__V3*/ meltfptr[2],
			      MELT_DYNLOADED_SUFFIX));;
    MELT_LOCATION ("warmelt-outobj.melt:6425:/ cond");
    /*cond */ if ( /*_#STRING_DYNLOADED_SUFFIXED__L5*/ meltfnum[1])	/*then */
      {
	/*^cond.then */
	/*_#OR___L6*/ meltfnum[0] =
	  /*_#STRING_DYNLOADED_SUFFIXED__L5*/ meltfnum[1];;
      }
    else
      {
	MELT_LOCATION ("warmelt-outobj.melt:6425:/ cond.else");

	/*^block */
	/*anyblock */
	{

   /*_#STRING_SUFFIXED__L7*/ meltfnum[6] =
	    /*string_suffixed: */
	    (melt_string_is_ending
	     ((melt_ptr_t) /*_.MODSRCNAME__V3*/ meltfptr[2], ".melt"));;
	  MELT_LOCATION ("warmelt-outobj.melt:6425:/ cond");
	  /*cond */ if ( /*_#STRING_SUFFIXED__L7*/ meltfnum[6])	/*then */
	    {
	      /*^cond.then */
	      /*_#OR___L8*/ meltfnum[7] =
		/*_#STRING_SUFFIXED__L7*/ meltfnum[6];;
	    }
	  else
	    {
	      MELT_LOCATION ("warmelt-outobj.melt:6425:/ cond.else");

	      /*^block */
	      /*anyblock */
	      {

     /*_#STRING_SUFFIXED__L9*/ meltfnum[8] =
		  /*string_suffixed: */
		  (melt_string_is_ending
		   ((melt_ptr_t) /*_.MODSRCNAME__V3*/ meltfptr[2], ".c"));;
		/*^compute */
		/*_#OR___L8*/ meltfnum[7] =
		  /*_#STRING_SUFFIXED__L9*/ meltfnum[8];;
		/*epilog */

		MELT_LOCATION ("warmelt-outobj.melt:6425:/ clear");
	       /*clear *//*_#STRING_SUFFIXED__L9*/ meltfnum[8] = 0;
	      }
	      ;
	    }
	  ;
	  /*_#OR___L6*/ meltfnum[0] = /*_#OR___L8*/ meltfnum[7];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-outobj.melt:6425:/ clear");
	     /*clear *//*_#STRING_SUFFIXED__L7*/ meltfnum[6] = 0;
	  /*^clear */
	     /*clear *//*_#OR___L8*/ meltfnum[7] = 0;
	}
	;
      }
    ;
    /*^cond */
    /*cond */ if ( /*_#OR___L6*/ meltfnum[0])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{


	  {
	    MELT_LOCATION ("warmelt-outobj.melt:6428:/ locexp");
	    error ("MELT ERROR MSG [#%ld]::: %s - %s", melt_dbgcounter,
		   ("invalid MELT source name - should not be suffixed"),
		   melt_string_str ((melt_ptr_t)
				    ( /*_.MODSRCNAME__V3*/ meltfptr[2])));
	  }
	  ;
	  /*epilog */
	}
	;
      }				/*noelse */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:6430:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#STRING_DYNLOADED_SUFFIXED__L10*/ meltfnum[8] =
	/*string_dynloaded_suffixed: */
	(melt_string_is_ending ((melt_ptr_t) /*_.MODSRCNAME__V3*/ meltfptr[2],
				MELT_DYNLOADED_SUFFIX));;
      /*^compute */
   /*_#NOT__L11*/ meltfnum[6] =
	(!( /*_#STRING_DYNLOADED_SUFFIXED__L10*/ meltfnum[8]));;
      MELT_LOCATION ("warmelt-outobj.melt:6430:/ cond");
      /*cond */ if ( /*_#NOT__L11*/ meltfnum[6])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V14*/ meltfptr[13] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-outobj.melt:6430:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("translate_to_c_module_melt_sources modsrcname not ended with MELT_DYNLOADED_S\
UFFIX"), ("warmelt-outobj.melt") ? ("warmelt-outobj.melt") : __FILE__,
				  (6430) ? (6430) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V14*/ meltfptr[13] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V13*/ meltfptr[11] = /*_.IFELSE___V14*/ meltfptr[13];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:6430:/ clear");
	     /*clear *//*_#STRING_DYNLOADED_SUFFIXED__L10*/ meltfnum[8] = 0;
      /*^clear */
	     /*clear *//*_#NOT__L11*/ meltfnum[6] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V14*/ meltfptr[13] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V13*/ meltfptr[11] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:6432:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#STRING_SUFFIXED__L12*/ meltfnum[7] =
	/*string_suffixed: */
	(melt_string_is_ending
	 ((melt_ptr_t) /*_.MODSRCNAME__V3*/ meltfptr[2], ".melt"));;
      /*^compute */
   /*_#NOT__L13*/ meltfnum[8] =
	(!( /*_#STRING_SUFFIXED__L12*/ meltfnum[7]));;
      MELT_LOCATION ("warmelt-outobj.melt:6432:/ cond");
      /*cond */ if ( /*_#NOT__L13*/ meltfnum[8])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V16*/ meltfptr[15] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-outobj.melt:6432:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("translate_to_c_module_melt_sources modulename not ended with .melt"), ("warmelt-outobj.melt") ? ("warmelt-outobj.melt") : __FILE__, (6432) ? (6432) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V16*/ meltfptr[15] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V15*/ meltfptr[13] = /*_.IFELSE___V16*/ meltfptr[15];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:6432:/ clear");
	     /*clear *//*_#STRING_SUFFIXED__L12*/ meltfnum[7] = 0;
      /*^clear */
	     /*clear *//*_#NOT__L13*/ meltfnum[8] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V16*/ meltfptr[15] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V15*/ meltfptr[13] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:6434:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#STRING_SUFFIXED__L14*/ meltfnum[6] =
	/*string_suffixed: */
	(melt_string_is_ending
	 ((melt_ptr_t) /*_.MODSRCNAME__V3*/ meltfptr[2], ".c"));;
      /*^compute */
   /*_#NOT__L15*/ meltfnum[7] =
	(!( /*_#STRING_SUFFIXED__L14*/ meltfnum[6]));;
      MELT_LOCATION ("warmelt-outobj.melt:6434:/ cond");
      /*cond */ if ( /*_#NOT__L15*/ meltfnum[7])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V18*/ meltfptr[17] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-outobj.melt:6434:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("translate_to_c_module_melt_sources modulename not ended with .c"), ("warmelt-outobj.melt") ? ("warmelt-outobj.melt") : __FILE__, (6434) ? (6434) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V18*/ meltfptr[17] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V17*/ meltfptr[15] = /*_.IFELSE___V18*/ meltfptr[17];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:6434:/ clear");
	     /*clear *//*_#STRING_SUFFIXED__L14*/ meltfnum[6] = 0;
      /*^clear */
	     /*clear *//*_#NOT__L15*/ meltfnum[7] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V18*/ meltfptr[17] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V17*/ meltfptr[15] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-outobj.melt:6436:/ quasiblock");


 /*_#ISVALIDMODSRC__L16*/ meltfnum[8] = 0;;
    /*^compute */
 /*_.RLIST__V20*/ meltfptr[19] =
      (meltgc_new_list
       ((meltobject_ptr_t) (( /*!DISCR_LIST */ meltfrout->tabval[2]))));;

    {
      MELT_LOCATION ("warmelt-outobj.melt:6441:/ locexp");
      /* translate_to_c_module_melt_sources SETISVALIDMODSRCCH__1 */
      {
	const char *modsrcstr =
	  melt_string_str ((melt_ptr_t) /*_.MODSRCNAME__V3*/ meltfptr[2]);
	const char *modsrcbase = modsrcstr ? (lbasename (modsrcstr)) : NULL;
	if (modsrcbase)
       /*_#ISVALIDMODSRC__L16*/ meltfnum[8] =
	    (strchr (modsrcbase, '.') == NULL)
	    && (strchr (modsrcbase, '+') == NULL);
      }				/* end translate_to_c_module_melt_sources SETISVALIDMODSRCCH__1 */
      ;
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:6449:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#NOT__L17*/ meltfnum[6] =
      (!( /*_#ISVALIDMODSRC__L16*/ meltfnum[8]));;
    MELT_LOCATION ("warmelt-outobj.melt:6449:/ cond");
    /*cond */ if ( /*_#NOT__L17*/ meltfnum[6])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{


#if MELT_HAVE_DEBUG
	  MELT_LOCATION ("warmelt-outobj.melt:6451:/ cppif.then");
	  /*^block */
	  /*anyblock */
	  {


	    {
	      /*^locexp */
	      /*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	      melt_dbgcounter++;
#endif
	      ;
	    }
	    ;
	    /*^checksignal */
	    MELT_CHECK_SIGNAL ();
	    ;
     /*_#MELT_NEED_DBG__L18*/ meltfnum[7] =
	      /*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	      ( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	      0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	      ;;
	    MELT_LOCATION ("warmelt-outobj.melt:6451:/ cond");
	    /*cond */ if ( /*_#MELT_NEED_DBG__L18*/ meltfnum[7])	/*then */
	      {
		/*^cond.then */
		/*^block */
		/*anyblock */
		{

       /*_#THE_MELTCALLCOUNT__L19*/ meltfnum[18] =
#ifdef meltcallcount
		    meltcallcount	/* the_meltcallcount */
#else
		    0L
#endif /* meltcallcount the_meltcallcount */
		    ;;
		  MELT_LOCATION ("warmelt-outobj.melt:6451:/ checksignal");
		  MELT_CHECK_SIGNAL ();
		  ;
		  /*^apply */
		  /*apply */
		  {
		    union meltparam_un argtab[5];
		    memset (&argtab, 0, sizeof (argtab));
		    /*^apply.arg */
		    argtab[0].meltbp_long =
		      /*_#THE_MELTCALLCOUNT__L19*/ meltfnum[18];
		    /*^apply.arg */
		    argtab[1].meltbp_cstring = "warmelt-outobj.melt";
		    /*^apply.arg */
		    argtab[2].meltbp_long = 6451;
		    /*^apply.arg */
		    argtab[3].meltbp_cstring =
		      "translate_to_c_module_melt_sources bad generated modsrcname=";
		    /*^apply.arg */
		    argtab[4].meltbp_aptr =
		      (melt_ptr_t *) & /*_.MODSRCNAME__V3*/ meltfptr[2];
		    /*_.MELT_DEBUG_FUN__V24*/ meltfptr[23] =
		      melt_apply ((meltclosure_ptr_t)
				  (( /*!MELT_DEBUG_FUN */ meltfrout->
				    tabval[0])),
				  (melt_ptr_t) (( /*nil */ NULL)),
				  (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_PTR ""), argtab, "",
				  (union meltparam_un *) 0);
		  }
		  ;
		  /*_.IF___V23*/ meltfptr[22] =
		    /*_.MELT_DEBUG_FUN__V24*/ meltfptr[23];;
		  /*epilog */

		  MELT_LOCATION ("warmelt-outobj.melt:6451:/ clear");
		 /*clear *//*_#THE_MELTCALLCOUNT__L19*/ meltfnum[18] = 0;
		  /*^clear */
		 /*clear *//*_.MELT_DEBUG_FUN__V24*/ meltfptr[23] = 0;
		}
		;
	      }
	    else
	      {			/*^cond.else */

      /*_.IF___V23*/ meltfptr[22] = NULL;;
	      }
	    ;
	    MELT_LOCATION ("warmelt-outobj.melt:6451:/ quasiblock");


	    /*_.PROGN___V25*/ meltfptr[23] = /*_.IF___V23*/ meltfptr[22];;
	    /*^compute */
	    /*_.IFCPP___V22*/ meltfptr[21] = /*_.PROGN___V25*/ meltfptr[23];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-outobj.melt:6451:/ clear");
	       /*clear *//*_#MELT_NEED_DBG__L18*/ meltfnum[7] = 0;
	    /*^clear */
	       /*clear *//*_.IF___V23*/ meltfptr[22] = 0;
	    /*^clear */
	       /*clear *//*_.PROGN___V25*/ meltfptr[23] = 0;
	  }

#else /*MELT_HAVE_DEBUG */
	  /*^cppif.else */
	  /*_.IFCPP___V22*/ meltfptr[21] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:6452:/ locexp");
	    error ("MELT ERROR MSG [#%ld]::: %s - %s", melt_dbgcounter,
		   ("invalid generated source name [basename should have no dot or plus]"),
		   melt_string_str ((melt_ptr_t)
				    ( /*_.MODSRCNAME__V3*/ meltfptr[2])));
	  }
	  ;
	  MELT_LOCATION ("warmelt-outobj.melt:6455:/ quasiblock");


   /*_.RETVAL___V1*/ meltfptr[0] = NULL;;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:6455:/ locexp");
	    /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
	    if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	      melt_warn_for_no_expected_secondary_results ();
	    /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
	    ;
	  }
	  ;
	  /*^finalreturn */
	  ;
	  /*finalret */ goto labend_rout;
	  MELT_LOCATION ("warmelt-outobj.melt:6450:/ quasiblock");


	  /*_.PROGN___V27*/ meltfptr[23] = /*_.RETURN___V26*/ meltfptr[22];;
	  /*^compute */
	  /*_.IF___V21*/ meltfptr[20] = /*_.PROGN___V27*/ meltfptr[23];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-outobj.melt:6449:/ clear");
	     /*clear *//*_.IFCPP___V22*/ meltfptr[21] = 0;
	  /*^clear */
	     /*clear *//*_.RETURN___V26*/ meltfptr[22] = 0;
	  /*^clear */
	     /*clear *//*_.PROGN___V27*/ meltfptr[23] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

  /*_.IF___V21*/ meltfptr[20] = NULL;;
      }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:6457:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[2];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.RLIST__V20*/ meltfptr[19];
      /*^apply.arg */
      argtab[1].meltbp_long = 0;
      /*_.READ_MELT_EXPRESSIONS__V28*/ meltfptr[21] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!READ_MELT_EXPRESSIONS */ meltfrout->tabval[3])),
		    (melt_ptr_t) ( /*_.SOURCES__V2*/ meltfptr[1]),
		    (MELTBPARSTR_PTR MELTBPARSTR_LONG ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:6458:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L20*/ meltfnum[18] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-outobj.melt:6458:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L20*/ meltfnum[18])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L21*/ meltfnum[7] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-outobj.melt:6458:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long =
		/*_#THE_MELTCALLCOUNT__L21*/ meltfnum[7];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-outobj.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 6458;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring =
		"after read translate_to_c_module_melt_sources rlist=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.RLIST__V20*/ meltfptr[19];
	      /*_.MELT_DEBUG_FUN__V31*/ meltfptr[30] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V30*/ meltfptr[23] =
	      /*_.MELT_DEBUG_FUN__V31*/ meltfptr[30];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-outobj.melt:6458:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L21*/ meltfnum[7] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V31*/ meltfptr[30] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V30*/ meltfptr[23] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-outobj.melt:6458:/ quasiblock");


      /*_.PROGN___V32*/ meltfptr[30] = /*_.IF___V30*/ meltfptr[23];;
      /*^compute */
      /*_.IFCPP___V29*/ meltfptr[22] = /*_.PROGN___V32*/ meltfptr[30];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:6458:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L20*/ meltfnum[18] = 0;
      /*^clear */
	     /*clear *//*_.IF___V30*/ meltfptr[23] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V32*/ meltfptr[30] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V29*/ meltfptr[22] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:6459:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#LIST_LENGTH__L22*/ meltfnum[7] =
	(melt_list_length ((melt_ptr_t) ( /*_.RLIST__V20*/ meltfptr[19])));;
      /*^compute */
   /*_#I__L23*/ meltfnum[18] =
	(( /*_#LIST_LENGTH__L22*/ meltfnum[7]) > (0));;
      MELT_LOCATION ("warmelt-outobj.melt:6459:/ cond");
      /*cond */ if ( /*_#I__L23*/ meltfnum[18])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V34*/ meltfptr[30] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-outobj.melt:6459:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check non empty rlist"),
				  ("warmelt-outobj.melt")
				  ? ("warmelt-outobj.melt") : __FILE__,
				  (6459) ? (6459) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V34*/ meltfptr[30] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V33*/ meltfptr[23] = /*_.IFELSE___V34*/ meltfptr[30];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:6459:/ clear");
	     /*clear *//*_#LIST_LENGTH__L22*/ meltfnum[7] = 0;
      /*^clear */
	     /*clear *//*_#I__L23*/ meltfnum[18] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V34*/ meltfptr[30] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V33*/ meltfptr[23] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-outobj.melt:6460:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[2];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.CURENV__V4*/ meltfptr[3];
      /*^apply.arg */
      argtab[1].meltbp_aptr =
	(melt_ptr_t *) & /*_.MODSRCNAME__V3*/ meltfptr[2];
      /*_.COMPILE_LIST_SEXPR__V35*/ meltfptr[30] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!COMPILE_LIST_SEXPR */ meltfrout->tabval[4])),
		    (melt_ptr_t) ( /*_.RLIST__V20*/ meltfptr[19]),
		    (MELTBPARSTR_PTR MELTBPARSTR_PTR ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:6461:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L24*/ meltfnum[7] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-outobj.melt:6461:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L24*/ meltfnum[7])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L25*/ meltfnum[18] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-outobj.melt:6461:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[6];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long =
		/*_#THE_MELTCALLCOUNT__L25*/ meltfnum[18];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-outobj.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 6461;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring =
		"translate_to_c_module_melt_sources done modsrcname=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.MODSRCNAME__V3*/ meltfptr[2];
	      /*^apply.arg */
	      argtab[5].meltbp_cstring = "\n";
	      /*_.MELT_DEBUG_FUN__V38*/ meltfptr[37] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR MELTBPARSTR_CSTRING ""), argtab,
			    "", (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V37*/ meltfptr[36] =
	      /*_.MELT_DEBUG_FUN__V38*/ meltfptr[37];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-outobj.melt:6461:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L25*/ meltfnum[18] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V38*/ meltfptr[37] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V37*/ meltfptr[36] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-outobj.melt:6461:/ quasiblock");


      /*_.PROGN___V39*/ meltfptr[37] = /*_.IF___V37*/ meltfptr[36];;
      /*^compute */
      /*_.IFCPP___V36*/ meltfptr[35] = /*_.PROGN___V39*/ meltfptr[37];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:6461:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L24*/ meltfnum[7] = 0;
      /*^clear */
	     /*clear *//*_.IF___V37*/ meltfptr[36] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V39*/ meltfptr[37] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V36*/ meltfptr[35] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    /*^compute */
    /*_.LET___V19*/ meltfptr[17] = /*_.IFCPP___V36*/ meltfptr[35];;

    MELT_LOCATION ("warmelt-outobj.melt:6436:/ clear");
	   /*clear *//*_#ISVALIDMODSRC__L16*/ meltfnum[8] = 0;
    /*^clear */
	   /*clear *//*_.RLIST__V20*/ meltfptr[19] = 0;
    /*^clear */
	   /*clear *//*_#NOT__L17*/ meltfnum[6] = 0;
    /*^clear */
	   /*clear *//*_.IF___V21*/ meltfptr[20] = 0;
    /*^clear */
	   /*clear *//*_.READ_MELT_EXPRESSIONS__V28*/ meltfptr[21] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V29*/ meltfptr[22] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V33*/ meltfptr[23] = 0;
    /*^clear */
	   /*clear *//*_.COMPILE_LIST_SEXPR__V35*/ meltfptr[30] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V36*/ meltfptr[35] = 0;
    MELT_LOCATION ("warmelt-outobj.melt:6421:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.LET___V19*/ meltfptr[17];;

    {
      MELT_LOCATION ("warmelt-outobj.melt:6421:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V5*/ meltfptr[4] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V9*/ meltfptr[5] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V11*/ meltfptr[6] = 0;
    /*^clear */
	   /*clear *//*_#STRING_DYNLOADED_SUFFIXED__L5*/ meltfnum[1] = 0;
    /*^clear */
	   /*clear *//*_#OR___L6*/ meltfnum[0] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V13*/ meltfptr[11] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V15*/ meltfptr[13] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V17*/ meltfptr[15] = 0;
    /*^clear */
	   /*clear *//*_.LET___V19*/ meltfptr[17] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("TRANSLATE_TO_C_MODULE_MELT_SOURCES", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_144_warmelt_outobj_TRANSLATE_TO_C_MODULE_MELT_SOURCES_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_144_warmelt_outobj_TRANSLATE_TO_C_MODULE_MELT_SOURCES */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_145_warmelt_outobj_GENERATE_GPLV3PLUS_COPYRIGHT_NOTICE_C_COMMENT
  (meltclosure_ptr_t meltclosp_, melt_ptr_t meltfirstargp_,
   const melt_argdescr_cell_t meltxargdescr_[],
   union meltparam_un * meltxargtab_,
   const melt_argdescr_cell_t meltxresdescr_[],
   union meltparam_un * meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_145_warmelt_outobj_GENERATE_GPLV3PLUS_COPYRIGHT_NOTICE_C_COMMENT_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_145_warmelt_outobj_GENERATE_GPLV3PLUS_COPYRIGHT_NOTICE_C_COMMENT_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 5
    melt_ptr_t mcfr_varptr[5];
#define MELTFRAM_NBVARNUM 3
    long mcfr_varnum[3];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_145_warmelt_outobj_GENERATE_GPLV3PLUS_COPYRIGHT_NOTICE_C_COMMENT is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct
	 frame_meltrout_145_warmelt_outobj_GENERATE_GPLV3PLUS_COPYRIGHT_NOTICE_C_COMMENT_st
	 *) meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 5; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_145_warmelt_outobj_GENERATE_GPLV3PLUS_COPYRIGHT_NOTICE_C_COMMENT nbval 5*/
  meltfram__.mcfr_nbvar = 5 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("GENERATE_GPLV3PLUS_COPYRIGHT_NOTICE_C_COMMENT",
		    meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-outobj.melt:6466:/ getarg");
 /*_.SBUF__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.NAME__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.NAME__V3*/ meltfptr[2])) !=
	      NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:6469:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_STRBUF__L1*/ meltfnum[0] =
	(melt_magic_discr ((melt_ptr_t) ( /*_.SBUF__V2*/ meltfptr[1])) ==
	 MELTOBMAG_STRBUF);;
      MELT_LOCATION ("warmelt-outobj.melt:6469:/ cond");
      /*cond */ if ( /*_#IS_STRBUF__L1*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V5*/ meltfptr[4] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-outobj.melt:6469:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check sbuf"),
				  ("warmelt-outobj.melt")
				  ? ("warmelt-outobj.melt") : __FILE__,
				  (6469) ? (6469) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V5*/ meltfptr[4] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V4*/ meltfptr[3] = /*_.IFELSE___V5*/ meltfptr[4];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:6469:/ clear");
	     /*clear *//*_#IS_STRBUF__L1*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V5*/ meltfptr[4] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V4*/ meltfptr[3] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:6470:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V2*/ meltfptr[1]),
			   ("/** Copyright (C) "));
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:6471:/ quasiblock");


 /*_#YEAR__L2*/ meltfnum[0] = 0;;

    {
      MELT_LOCATION ("warmelt-outobj.melt:6474:/ locexp");
      /*+ generate_gplv3plus_copyright_notice_c_comment GETYEAR__1 */
      {
	time_t GETYEAR__1_now = 0;
	struct tm *GETYEAR__1_tm = NULL;
	/*_#YEAR__L2*/ meltfnum[0] = atol (__DATE__ + 7);
	time (&GETYEAR__1_now);
	if (GETYEAR__1_now > 0)
	  GETYEAR__1_tm = localtime (&GETYEAR__1_now);
	if (GETYEAR__1_tm)
	   /*_#YEAR__L2*/ meltfnum[0] = GETYEAR__1_tm->tm_year + 1900;
      } /*- GETYEAR__1*/ ;
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:6484:/ locexp");
      meltgc_add_strbuf_dec ((melt_ptr_t) ( /*_.SBUF__V2*/ meltfptr[1]),
			     ( /*_#YEAR__L2*/ meltfnum[0]));
    }
    ;

    MELT_LOCATION ("warmelt-outobj.melt:6471:/ clear");
	   /*clear *//*_#YEAR__L2*/ meltfnum[0] = 0;

    {
      MELT_LOCATION ("warmelt-outobj.melt:6486:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V2*/ meltfptr[1]),
			   (" Free Software Foundation, Inc."));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:6487:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.SBUF__V2*/ meltfptr[1]),
				(0), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:6488:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V2*/ meltfptr[1]),
			   ("  This generated file "));
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:6489:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#IS_STRING__L3*/ meltfnum[0] =
      (melt_magic_discr ((melt_ptr_t) ( /*_.NAME__V3*/ meltfptr[2])) ==
       MELTOBMAG_STRING);;
    MELT_LOCATION ("warmelt-outobj.melt:6489:/ cond");
    /*cond */ if ( /*_#IS_STRING__L3*/ meltfnum[0])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{


	  {
	    MELT_LOCATION ("warmelt-outobj.melt:6492:/ locexp");
	    /*ADDBASENAME__1 + */
	    meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V2*/ meltfptr[1]),
			       lbasename (melt_string_str
					  ((melt_ptr_t)
					   ( /*_.NAME__V3*/ meltfptr[2]))));
	    /*ADDBASENAME__1 - */ ;
	  }
	  ;
	  /*epilog */
	}
	;
      }				/*noelse */
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:6497:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V2*/ meltfptr[1]),
			   (" is part of GCC."));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:6498:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.SBUF__V2*/ meltfptr[1]),
				(0), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:6499:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.SBUF__V2*/ meltfptr[1]),
				(0), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:6500:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V2*/ meltfptr[1]),
			   ("                  [DON\'T EDIT THIS GENERATED FILE]\
\n\n    GCC is free software; you can redistribute it and/or modify\
\
\n    it under the terms of the GNU General Public License as published\
 by\n    the Free Software Foundation; either version 3, or (at your\
 option)\n    any later version.\n\
\n    GCC is distributed in the hope that it will be useful,\
\n    but WITHOUT ANY WARRANTY; without even the implied warranty of\
\
\n    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\
\
\n    GNU General Public License for more details.\
\n\n    You should have received a copy of the GNU General Public License\
\
\n    along with GCC; see the file COPYING3.  If not see\
\n    <http://www.gnu.org/licenses/>.\
\n**/"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:6516:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.SBUF__V2*/ meltfptr[1]),
				(0), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:6517:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.SBUF__V2*/ meltfptr[1]),
				(0), 0);
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:6466:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V4*/ meltfptr[3] = 0;
    /*^clear */
	   /*clear *//*_#IS_STRING__L3*/ meltfnum[0] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("GENERATE_GPLV3PLUS_COPYRIGHT_NOTICE_C_COMMENT",
		  meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_145_warmelt_outobj_GENERATE_GPLV3PLUS_COPYRIGHT_NOTICE_C_COMMENT_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*noretval */ NULL);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_145_warmelt_outobj_GENERATE_GPLV3PLUS_COPYRIGHT_NOTICE_C_COMMENT */



/**** end of warmelt-outobj+05.c ****/
