/** GENERATED MELT DESCRIPTOR FILE meltbuild-sources/warmelt-genobj+meltdesc.c 
** NEVER EDIT OR MOVE THIS, IT IS GENERATED & PARSED! **/
/* These identifiers are generated in warmelt-outobj.melt 
 & handled in melt-runtime.c carefully. */

	     #ifdef __cplusplus
	     /* explicitly declare as extern "C" our dlsym-ed symbols */
	     extern "C" const char melt_versionmeltstr[]	    ;
	     extern "C" const char melt_genversionstr[]		    ;
	     extern "C" const char melt_modulename[]		    ;
	     extern "C" const char melt_modulerealpath[]	    ;
	     extern "C" const char melt_prepromd5meltrun[]	    ;
	     extern "C" const char melt_primaryhexmd5[]		    ;
	     extern "C" const char* const melt_secondaryhexmd5tab[] ;
	     extern "C" const int melt_lastsecfileindex		    ;
	     extern "C" const char melt_cumulated_hexmd5[]	    ;

	     extern "C" {
	     #endif /*__cplusplus */
	     
/* version of the GCC compiler & MELT runtime generating this */
const char melt_genversionstr[]="4.8.0 20121010 (experimental) [melt-branch revision 192289] MELT_0\
.9.7-rc4"

	     #ifdef __cplusplus
	     " (in C++)"
	     #else
	     " (in C)"
	     #endif
					;
	     
const char melt_versionmeltstr[]="0.9.7-rc4 [melt-branch_revision_192289]";

/* source name & real path of the module */
/*MELTMODULENAME meltbuild-sources/warmelt-genobj */
const char melt_modulename[]="warmelt-genobj";
const char melt_modulerealpath[]="/usr/local/libexec/gcc-melt/gcc/x86_64-unknown-linux-gnu/4.8.0/melt\
-modules/0.9.7-rc4/warmelt-genobj";

/* hash of preprocessed melt-run.h generating this */
const char melt_prepromd5meltrun[]="f66a30d9bc2c10de00b1532482ad91ed";
/* hexmd5checksum of primary C file */
const char melt_primaryhexmd5[]="c4b6bc48ec19d85c8a5b2f2695a6d526";

/* hexmd5checksum of secondary C files */
const char* const melt_secondaryhexmd5tab[]={
 /*nosecfile*/ (const char*)0,
 /*sechexmd5checksum meltbuild-sources/warmelt-genobj+01.c #1 */ "7859a0d504d1a856c03fc6e011386469",
 /*sechexmd5checksum meltbuild-sources/warmelt-genobj+02.c #2 */ "f38797448c6f3c066595b7a8d00240ed",
 /*sechexmd5checksum meltbuild-sources/warmelt-genobj+03.c #3 */ "7e2fe9caa2edba93ca022547a3a3f4ee",
 /*sechexmd5checksum meltbuild-sources/warmelt-genobj+04.c #4 */ "a1ea49b3548115ea459d9aaeff96177e",
 /*sechexmd5checksum meltbuild-sources/warmelt-genobj+05.c #5 */ "f2bc17c18ef4acd370e5aafe2aae81ce",
 /*sechexmd5checksum meltbuild-sources/warmelt-genobj+06.c #6 */ "6573ffd71aec9f6836f8e78c0c6d3caf",
 (const char*)0 };

/* last index of secondary files */
const int melt_lastsecfileindex=6;

/* cumulated checksum of primary & secondary files */
const char melt_cumulated_hexmd5[]="1840ba92f8a65557e514aa5198344586" ;

/* include the timestamp file */
#define meltmod_warmelt_genobj_mds__1840ba92f8a65557e514aa5198344586 1
#include "warmelt-genobj+melttime.h"
	 

		 #ifdef __cplusplus
		 }	  /* end extern C descriptor */
		 #endif /*__cplusplus */
		 
/* end of melt descriptor file */
