/* GCC MELT GENERATED FILE warmelt-debug+01.c - DO NOT EDIT */
/* secondary MELT generated C file of rank #1 */
#include "melt-run.h"


/* used hash from melt-run.h when compiling this file: */
MELT_EXTERN const char meltrun_used_md5_melt_f1[] =
  MELT_RUN_HASHMD5 /* from melt-run.h */ ;


/**** warmelt-debug+01.c declarations ****/


/***************************************************
***
    Copyright 2009 - 2012 Free Software Foundation, Inc.
    Contributed by Basile Starynkevitch <basile@starynkevitch.net>

    This file is part of GCC.

    GCC is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3, or (at your option)
    any later version.

    GCC is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with GCC; see the file COPYING3.  If not see
    <http://www.gnu.org/licenses/>.
***
****************************************************/

/* ordinary MELT module */
#define MELT_HAS_INITIAL_ENVIRONMENT 1	/*usual */

struct melt_callframe_st;	/*defined in melt-runtime.h */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_1_warmelt_debug_DEBUG_MSG_FUN (meltclosure_ptr_t meltclosp_,
					melt_ptr_t meltfirstargp_,
					const melt_argdescr_cell_t
					meltxargdescr_[],
					union meltparam_un *meltxargtab_,
					const melt_argdescr_cell_t
					meltxresdescr_[],
					union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_2_warmelt_debug_REGISTER_TREE_DEBUG_FUN (meltclosure_ptr_t
						  meltclosp_,
						  melt_ptr_t meltfirstargp_,
						  const melt_argdescr_cell_t
						  meltxargdescr_[],
						  union meltparam_un
						  *meltxargtab_,
						  const melt_argdescr_cell_t
						  meltxresdescr_[],
						  union meltparam_un
						  *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_3_warmelt_debug_REGISTER_GIMPLE_DEBUG_FUN (meltclosure_ptr_t
						    meltclosp_,
						    melt_ptr_t meltfirstargp_,
						    const melt_argdescr_cell_t
						    meltxargdescr_[],
						    union meltparam_un
						    *meltxargtab_,
						    const melt_argdescr_cell_t
						    meltxresdescr_[],
						    union meltparam_un
						    *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_4_warmelt_debug_REGISTER_GIMPLESEQ_DEBUG_FUN (meltclosure_ptr_t
						       meltclosp_,
						       melt_ptr_t
						       meltfirstargp_,
						       const
						       melt_argdescr_cell_t
						       meltxargdescr_[],
						       union meltparam_un
						       *meltxargtab_,
						       const
						       melt_argdescr_cell_t
						       meltxresdescr_[],
						       union meltparam_un
						       *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_5_warmelt_debug_REGISTER_EDGE_DEBUG_FUN (meltclosure_ptr_t
						  meltclosp_,
						  melt_ptr_t meltfirstargp_,
						  const melt_argdescr_cell_t
						  meltxargdescr_[],
						  union meltparam_un
						  *meltxargtab_,
						  const melt_argdescr_cell_t
						  meltxresdescr_[],
						  union meltparam_un
						  *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_6_warmelt_debug_REGISTER_BASICBLOCK_DEBUG_FUN (meltclosure_ptr_t
							meltclosp_,
							melt_ptr_t
							meltfirstargp_,
							const
							melt_argdescr_cell_t
							meltxargdescr_[],
							union meltparam_un
							*meltxargtab_,
							const
							melt_argdescr_cell_t
							meltxresdescr_[],
							union meltparam_un
							*meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_7_warmelt_debug_REGISTER_LOOP_DEBUG_FUN (meltclosure_ptr_t
						  meltclosp_,
						  melt_ptr_t meltfirstargp_,
						  const melt_argdescr_cell_t
						  meltxargdescr_[],
						  union meltparam_un
						  *meltxargtab_,
						  const melt_argdescr_cell_t
						  meltxresdescr_[],
						  union meltparam_un
						  *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_8_warmelt_debug_MELT_DEBUG_FUN (meltclosure_ptr_t meltclosp_,
					 melt_ptr_t meltfirstargp_,
					 const melt_argdescr_cell_t
					 meltxargdescr_[],
					 union meltparam_un *meltxargtab_,
					 const melt_argdescr_cell_t
					 meltxresdescr_[],
					 union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_9_warmelt_debug_DBG_OUTOBJECT (meltclosure_ptr_t meltclosp_,
					melt_ptr_t meltfirstargp_,
					const melt_argdescr_cell_t
					meltxargdescr_[],
					union meltparam_un *meltxargtab_,
					const melt_argdescr_cell_t
					meltxresdescr_[],
					union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_10_warmelt_debug_DBG_OUT (meltclosure_ptr_t meltclosp_,
				   melt_ptr_t meltfirstargp_,
				   const melt_argdescr_cell_t
				   meltxargdescr_[],
				   union meltparam_un *meltxargtab_,
				   const melt_argdescr_cell_t
				   meltxresdescr_[],
				   union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_11_warmelt_debug_DBGOUT_FIELDS (meltclosure_ptr_t meltclosp_,
					 melt_ptr_t meltfirstargp_,
					 const melt_argdescr_cell_t
					 meltxargdescr_[],
					 union meltparam_un *meltxargtab_,
					 const melt_argdescr_cell_t
					 meltxresdescr_[],
					 union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_12_warmelt_debug_DBGOUTAGAIN_FIELDS (meltclosure_ptr_t meltclosp_,
					      melt_ptr_t meltfirstargp_,
					      const melt_argdescr_cell_t
					      meltxargdescr_[],
					      union meltparam_un
					      *meltxargtab_,
					      const melt_argdescr_cell_t
					      meltxresdescr_[],
					      union meltparam_un
					      *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_13_warmelt_debug_DBGOUT_NULL_METHOD (meltclosure_ptr_t meltclosp_,
					      melt_ptr_t meltfirstargp_,
					      const melt_argdescr_cell_t
					      meltxargdescr_[],
					      union meltparam_un
					      *meltxargtab_,
					      const melt_argdescr_cell_t
					      meltxresdescr_[],
					      union meltparam_un
					      *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_14_warmelt_debug_DBGOUT_STRING_METHOD (meltclosure_ptr_t meltclosp_,
						melt_ptr_t meltfirstargp_,
						const melt_argdescr_cell_t
						meltxargdescr_[],
						union meltparam_un
						*meltxargtab_,
						const melt_argdescr_cell_t
						meltxresdescr_[],
						union meltparam_un
						*meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_15_warmelt_debug_DBGOUT_STRBUF_METHOD (meltclosure_ptr_t meltclosp_,
						melt_ptr_t meltfirstargp_,
						const melt_argdescr_cell_t
						meltxargdescr_[],
						union meltparam_un
						*meltxargtab_,
						const melt_argdescr_cell_t
						meltxresdescr_[],
						union meltparam_un
						*meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_16_warmelt_debug_DBGOUT_INTEGER_METHOD (meltclosure_ptr_t meltclosp_,
						 melt_ptr_t meltfirstargp_,
						 const melt_argdescr_cell_t
						 meltxargdescr_[],
						 union meltparam_un
						 *meltxargtab_,
						 const melt_argdescr_cell_t
						 meltxresdescr_[],
						 union meltparam_un
						 *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_17_warmelt_debug_DBGOUT_MIXINT_METHOD (meltclosure_ptr_t meltclosp_,
						melt_ptr_t meltfirstargp_,
						const melt_argdescr_cell_t
						meltxargdescr_[],
						union meltparam_un
						*meltxargtab_,
						const melt_argdescr_cell_t
						meltxresdescr_[],
						union meltparam_un
						*meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_18_warmelt_debug_DBGOUT_MIXLOC_METHOD (meltclosure_ptr_t meltclosp_,
						melt_ptr_t meltfirstargp_,
						const melt_argdescr_cell_t
						meltxargdescr_[],
						union meltparam_un
						*meltxargtab_,
						const melt_argdescr_cell_t
						meltxresdescr_[],
						union meltparam_un
						*meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_19_warmelt_debug_DBGOUT_MIXBIGINT_METHOD (meltclosure_ptr_t
						   meltclosp_,
						   melt_ptr_t meltfirstargp_,
						   const melt_argdescr_cell_t
						   meltxargdescr_[],
						   union meltparam_un
						   *meltxargtab_,
						   const melt_argdescr_cell_t
						   meltxresdescr_[],
						   union meltparam_un
						   *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_20_warmelt_debug_DBGOUT_MULTIPLE_METHOD (meltclosure_ptr_t
						  meltclosp_,
						  melt_ptr_t meltfirstargp_,
						  const melt_argdescr_cell_t
						  meltxargdescr_[],
						  union meltparam_un
						  *meltxargtab_,
						  const melt_argdescr_cell_t
						  meltxresdescr_[],
						  union meltparam_un
						  *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_21_warmelt_debug_DBGOUT_ROUTINE_METHOD (meltclosure_ptr_t meltclosp_,
						 melt_ptr_t meltfirstargp_,
						 const melt_argdescr_cell_t
						 meltxargdescr_[],
						 union meltparam_un
						 *meltxargtab_,
						 const melt_argdescr_cell_t
						 meltxresdescr_[],
						 union meltparam_un
						 *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_22_warmelt_debug_LAMBDA___1__ (meltclosure_ptr_t meltclosp_,
					melt_ptr_t meltfirstargp_,
					const melt_argdescr_cell_t
					meltxargdescr_[],
					union meltparam_un *meltxargtab_,
					const melt_argdescr_cell_t
					meltxresdescr_[],
					union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_23_warmelt_debug_DBGOUT_CLOSURE_METHOD (meltclosure_ptr_t meltclosp_,
						 melt_ptr_t meltfirstargp_,
						 const melt_argdescr_cell_t
						 meltxargdescr_[],
						 union meltparam_un
						 *meltxargtab_,
						 const melt_argdescr_cell_t
						 meltxresdescr_[],
						 union meltparam_un
						 *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_24_warmelt_debug_DBGOUT_LIST_METHOD (meltclosure_ptr_t meltclosp_,
					      melt_ptr_t meltfirstargp_,
					      const melt_argdescr_cell_t
					      meltxargdescr_[],
					      union meltparam_un
					      *meltxargtab_,
					      const melt_argdescr_cell_t
					      meltxresdescr_[],
					      union meltparam_un
					      *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_25_warmelt_debug_DBGOUT_PAIR_METHOD (meltclosure_ptr_t meltclosp_,
					      melt_ptr_t meltfirstargp_,
					      const melt_argdescr_cell_t
					      meltxargdescr_[],
					      union meltparam_un
					      *meltxargtab_,
					      const melt_argdescr_cell_t
					      meltxresdescr_[],
					      union meltparam_un
					      *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_26_warmelt_debug_DBGOUT_MAPOBJECT_METHOD (meltclosure_ptr_t
						   meltclosp_,
						   melt_ptr_t meltfirstargp_,
						   const melt_argdescr_cell_t
						   meltxargdescr_[],
						   union meltparam_un
						   *meltxargtab_,
						   const melt_argdescr_cell_t
						   meltxresdescr_[],
						   union meltparam_un
						   *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_27_warmelt_debug_LAMBDA___2__ (meltclosure_ptr_t meltclosp_,
					melt_ptr_t meltfirstargp_,
					const melt_argdescr_cell_t
					meltxargdescr_[],
					union meltparam_un *meltxargtab_,
					const melt_argdescr_cell_t
					meltxresdescr_[],
					union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_28_warmelt_debug_LAMBDA___3__ (meltclosure_ptr_t meltclosp_,
					melt_ptr_t meltfirstargp_,
					const melt_argdescr_cell_t
					meltxargdescr_[],
					union meltparam_un *meltxargtab_,
					const melt_argdescr_cell_t
					meltxresdescr_[],
					union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_29_warmelt_debug_LAMBDA___4__ (meltclosure_ptr_t meltclosp_,
					melt_ptr_t meltfirstargp_,
					const melt_argdescr_cell_t
					meltxargdescr_[],
					union meltparam_un *meltxargtab_,
					const melt_argdescr_cell_t
					meltxresdescr_[],
					union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_30_warmelt_debug_DBGOUT_MAPSTRING_METHOD (meltclosure_ptr_t
						   meltclosp_,
						   melt_ptr_t meltfirstargp_,
						   const melt_argdescr_cell_t
						   meltxargdescr_[],
						   union meltparam_un
						   *meltxargtab_,
						   const melt_argdescr_cell_t
						   meltxresdescr_[],
						   union meltparam_un
						   *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_31_warmelt_debug_LAMBDA___5__ (meltclosure_ptr_t meltclosp_,
					melt_ptr_t meltfirstargp_,
					const melt_argdescr_cell_t
					meltxargdescr_[],
					union meltparam_un *meltxargtab_,
					const melt_argdescr_cell_t
					meltxresdescr_[],
					union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_32_warmelt_debug_LAMBDA___6__ (meltclosure_ptr_t meltclosp_,
					melt_ptr_t meltfirstargp_,
					const melt_argdescr_cell_t
					meltxargdescr_[],
					union meltparam_un *meltxargtab_,
					const melt_argdescr_cell_t
					meltxresdescr_[],
					union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_33_warmelt_debug_LAMBDA___7__ (meltclosure_ptr_t meltclosp_,
					melt_ptr_t meltfirstargp_,
					const melt_argdescr_cell_t
					meltxargdescr_[],
					union meltparam_un *meltxargtab_,
					const melt_argdescr_cell_t
					meltxresdescr_[],
					union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_34_warmelt_debug_DBGOUT_BUCKETLONG_METHOD (meltclosure_ptr_t
						    meltclosp_,
						    melt_ptr_t meltfirstargp_,
						    const melt_argdescr_cell_t
						    meltxargdescr_[],
						    union meltparam_un
						    *meltxargtab_,
						    const melt_argdescr_cell_t
						    meltxresdescr_[],
						    union meltparam_un
						    *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_35_warmelt_debug_DBGOUT_ANYOBJECT_METHOD (meltclosure_ptr_t
						   meltclosp_,
						   melt_ptr_t meltfirstargp_,
						   const melt_argdescr_cell_t
						   meltxargdescr_[],
						   union meltparam_un
						   *meltxargtab_,
						   const melt_argdescr_cell_t
						   meltxresdescr_[],
						   union meltparam_un
						   *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_36_warmelt_debug_DBGOUT_ANYRECV_METHOD (meltclosure_ptr_t meltclosp_,
						 melt_ptr_t meltfirstargp_,
						 const melt_argdescr_cell_t
						 meltxargdescr_[],
						 union meltparam_un
						 *meltxargtab_,
						 const melt_argdescr_cell_t
						 meltxresdescr_[],
						 union meltparam_un
						 *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_37_warmelt_debug_DBGOUTAGAIN_ANYOBJECT_METHOD (meltclosure_ptr_t
							meltclosp_,
							melt_ptr_t
							meltfirstargp_,
							const
							melt_argdescr_cell_t
							meltxargdescr_[],
							union meltparam_un
							*meltxargtab_,
							const
							melt_argdescr_cell_t
							meltxresdescr_[],
							union meltparam_un
							*meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_38_warmelt_debug_DBGOUT_NAMEDOBJECT_METHOD (meltclosure_ptr_t
						     meltclosp_,
						     melt_ptr_t
						     meltfirstargp_,
						     const
						     melt_argdescr_cell_t
						     meltxargdescr_[],
						     union meltparam_un
						     *meltxargtab_,
						     const
						     melt_argdescr_cell_t
						     meltxresdescr_[],
						     union meltparam_un
						     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_39_warmelt_debug_DBGOUTAGAIN_NAMEDOBJECT_METHOD (meltclosure_ptr_t
							  meltclosp_,
							  melt_ptr_t
							  meltfirstargp_,
							  const
							  melt_argdescr_cell_t
							  meltxargdescr_[],
							  union meltparam_un
							  *meltxargtab_,
							  const
							  melt_argdescr_cell_t
							  meltxresdescr_[],
							  union meltparam_un
							  *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_40_warmelt_debug_DBGOUTAGAIN_SYMBOL_METHOD (meltclosure_ptr_t
						     meltclosp_,
						     melt_ptr_t
						     meltfirstargp_,
						     const
						     melt_argdescr_cell_t
						     meltxargdescr_[],
						     union meltparam_un
						     *meltxargtab_,
						     const
						     melt_argdescr_cell_t
						     meltxresdescr_[],
						     union meltparam_un
						     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_41_warmelt_debug_DBGOUT_SYMBOL_METHOD (meltclosure_ptr_t meltclosp_,
						melt_ptr_t meltfirstargp_,
						const melt_argdescr_cell_t
						meltxargdescr_[],
						union meltparam_un
						*meltxargtab_,
						const melt_argdescr_cell_t
						meltxresdescr_[],
						union meltparam_un
						*meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_42_warmelt_debug_DBGOUT_CLASS_METHOD (meltclosure_ptr_t meltclosp_,
					       melt_ptr_t meltfirstargp_,
					       const melt_argdescr_cell_t
					       meltxargdescr_[],
					       union meltparam_un
					       *meltxargtab_,
					       const melt_argdescr_cell_t
					       meltxresdescr_[],
					       union meltparam_un
					       *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_43_warmelt_debug_DBGOUTAGAIN_KEYWORD_METHOD (meltclosure_ptr_t
						      meltclosp_,
						      melt_ptr_t
						      meltfirstargp_,
						      const
						      melt_argdescr_cell_t
						      meltxargdescr_[],
						      union meltparam_un
						      *meltxargtab_,
						      const
						      melt_argdescr_cell_t
						      meltxresdescr_[],
						      union meltparam_un
						      *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_44_warmelt_debug_DBGOUT_KEYWORD_METHOD (meltclosure_ptr_t meltclosp_,
						 melt_ptr_t meltfirstargp_,
						 const melt_argdescr_cell_t
						 meltxargdescr_[],
						 union meltparam_un
						 *meltxargtab_,
						 const melt_argdescr_cell_t
						 meltxresdescr_[],
						 union meltparam_un
						 *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_45_warmelt_debug_DBGOUTAGAIN_CLONED_SYMBOL_METHOD (meltclosure_ptr_t
							    meltclosp_,
							    melt_ptr_t
							    meltfirstargp_,
							    const
							    melt_argdescr_cell_t
							    meltxargdescr_[],
							    union meltparam_un
							    *meltxargtab_,
							    const
							    melt_argdescr_cell_t
							    meltxresdescr_[],
							    union meltparam_un
							    *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_46_warmelt_debug_DBGOUT_CLONED_SYMBOL_METHOD (meltclosure_ptr_t
						       meltclosp_,
						       melt_ptr_t
						       meltfirstargp_,
						       const
						       melt_argdescr_cell_t
						       meltxargdescr_[],
						       union meltparam_un
						       *meltxargtab_,
						       const
						       melt_argdescr_cell_t
						       meltxresdescr_[],
						       union meltparam_un
						       *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_47_warmelt_debug_DBGOUT_ENVIRONMENT_METHOD (meltclosure_ptr_t
						     meltclosp_,
						     melt_ptr_t
						     meltfirstargp_,
						     const
						     melt_argdescr_cell_t
						     meltxargdescr_[],
						     union meltparam_un
						     *meltxargtab_,
						     const
						     melt_argdescr_cell_t
						     meltxresdescr_[],
						     union meltparam_un
						     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_48_warmelt_debug_DBGOUTAGAIN_CTYPE_METHOD (meltclosure_ptr_t
						    meltclosp_,
						    melt_ptr_t meltfirstargp_,
						    const melt_argdescr_cell_t
						    meltxargdescr_[],
						    union meltparam_un
						    *meltxargtab_,
						    const melt_argdescr_cell_t
						    meltxresdescr_[],
						    union meltparam_un
						    *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_49_warmelt_debug_DBGOUT_CTYPE_METHOD (meltclosure_ptr_t meltclosp_,
					       melt_ptr_t meltfirstargp_,
					       const melt_argdescr_cell_t
					       meltxargdescr_[],
					       union meltparam_un
					       *meltxargtab_,
					       const melt_argdescr_cell_t
					       meltxresdescr_[],
					       union meltparam_un
					       *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_50_warmelt_debug_DBGOUT_ANYBINDING_METHOD (meltclosure_ptr_t
						    meltclosp_,
						    melt_ptr_t meltfirstargp_,
						    const melt_argdescr_cell_t
						    meltxargdescr_[],
						    union meltparam_un
						    *meltxargtab_,
						    const melt_argdescr_cell_t
						    meltxresdescr_[],
						    union meltparam_un
						    *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_51_warmelt_debug_DISPLAY_DEBUG_MESSAGE (meltclosure_ptr_t meltclosp_,
						 melt_ptr_t meltfirstargp_,
						 const melt_argdescr_cell_t
						 meltxargdescr_[],
						 union meltparam_un
						 *meltxargtab_,
						 const melt_argdescr_cell_t
						 meltxresdescr_[],
						 union meltparam_un
						 *meltxrestab_);



MELT_EXTERN void *melt_start_this_module (void *);


/* define different names when debugging or not */
#if MELT_HAVE_DEBUG
MELT_EXTERN const char meltmodule_warmelt_debug__melt_have_debug_enabled[];
#define melt_have_debug_string meltmodule_warmelt_debug__melt_have_debug_enabled
#else /*!MELT_HAVE_DEBUG */
MELT_EXTERN const char meltmodule_warmelt_debug__melt_have_debug_disabled[];
#define melt_have_debug_string meltmodule_warmelt_debug__melt_have_debug_disabled
#endif /*!MELT_HAVE_DEBUG */


struct frame_melt_start_this_module_st;
void MELT_MODULE_VISIBILITY meltmod__warmelt_debug__initialmeltchunk_0 (struct
									frame_melt_start_this_module_st
									*,
									char
									*);
void MELT_MODULE_VISIBILITY meltmod__warmelt_debug__initialmeltchunk_1 (struct
									frame_melt_start_this_module_st
									*,
									char
									*);
void MELT_MODULE_VISIBILITY meltmod__warmelt_debug__initialmeltchunk_2 (struct
									frame_melt_start_this_module_st
									*,
									char
									*);
void MELT_MODULE_VISIBILITY meltmod__warmelt_debug__initialmeltchunk_3 (struct
									frame_melt_start_this_module_st
									*,
									char
									*);
void MELT_MODULE_VISIBILITY meltmod__warmelt_debug__initialmeltchunk_4 (struct
									frame_melt_start_this_module_st
									*,
									char
									*);
void MELT_MODULE_VISIBILITY meltmod__warmelt_debug__initialmeltchunk_5 (struct
									frame_melt_start_this_module_st
									*,
									char
									*);
void MELT_MODULE_VISIBILITY meltmod__warmelt_debug__initialmeltchunk_6 (struct
									frame_melt_start_this_module_st
									*,
									char
									*);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_debug__forward_or_mark_module_start_frame (struct
							    melt_callframe_st
							    *fp, int marking);
#define meltmarking_melt_start_this_module  meltmod__warmelt_debug__forward_or_mark_module_start_frame



/**** warmelt-debug+01.c implementations ****/




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_5_warmelt_debug_REGISTER_EDGE_DEBUG_FUN (meltclosure_ptr_t
						  meltclosp_,
						  melt_ptr_t meltfirstargp_,
						  const melt_argdescr_cell_t
						  meltxargdescr_[],
						  union meltparam_un
						  *meltxargtab_,
						  const melt_argdescr_cell_t
						  meltxresdescr_[],
						  union meltparam_un
						  *meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_5_warmelt_debug_REGISTER_EDGE_DEBUG_FUN_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_5_warmelt_debug_REGISTER_EDGE_DEBUG_FUN_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 4
    melt_ptr_t mcfr_varptr[4];
#define MELTFRAM_NBVARNUM 3
    long mcfr_varnum[3];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_5_warmelt_debug_REGISTER_EDGE_DEBUG_FUN is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_5_warmelt_debug_REGISTER_EDGE_DEBUG_FUN_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 4; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_5_warmelt_debug_REGISTER_EDGE_DEBUG_FUN nbval 4*/
  meltfram__.mcfr_nbvar = 4 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("REGISTER_EDGE_DEBUG_FUN", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-debug.melt:233:/ getarg");
 /*_.F__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {

    MELT_LOCATION ("warmelt-debug.melt:238:/ quasiblock");


    /*^cond */
    /*cond */ if (
		   /*ifisa */
		   melt_is_instance_of ((melt_ptr_t)
					(( /*!EDGE_DEBUG_FUNCONT */
					  meltfrout->tabval[0])),
					(melt_ptr_t) (( /*!CLASS_REFERENCE */
						       meltfrout->tabval[1])))
      )				/*then */
      {
	/*^cond.then */
	/*^getslot */
	{
	  melt_ptr_t slot = NULL, obj = NULL;
	  obj =
	    (melt_ptr_t) (( /*!EDGE_DEBUG_FUNCONT */ meltfrout->
			   tabval[0])) /*=obj*/ ;
	  melt_object_get_field (slot, obj, 0, "REFERENCED_VALUE");
   /*_.OLDCONT__V4*/ meltfptr[3] = slot;
	};
	;
      }
    else
      {				/*^cond.else */

  /*_.OLDCONT__V4*/ meltfptr[3] = NULL;;
      }
    ;
    MELT_LOCATION ("warmelt-debug.melt:240:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#NULL__L1*/ meltfnum[0] =
      (( /*_.F__V2*/ meltfptr[1]) == NULL);;
    MELT_LOCATION ("warmelt-debug.melt:240:/ cond");
    /*cond */ if ( /*_#NULL__L1*/ meltfnum[0])	/*then */
      {
	/*^cond.then */
	/*_#OR___L2*/ meltfnum[1] = /*_#NULL__L1*/ meltfnum[0];;
      }
    else
      {
	MELT_LOCATION ("warmelt-debug.melt:240:/ cond.else");

	/*^block */
	/*anyblock */
	{

   /*_#IS_CLOSURE__L3*/ meltfnum[2] =
	    (melt_magic_discr ((melt_ptr_t) ( /*_.F__V2*/ meltfptr[1])) ==
	     MELTOBMAG_CLOSURE);;
	  /*^compute */
	  /*_#OR___L2*/ meltfnum[1] = /*_#IS_CLOSURE__L3*/ meltfnum[2];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-debug.melt:240:/ clear");
	     /*clear *//*_#IS_CLOSURE__L3*/ meltfnum[2] = 0;
	}
	;
      }
    ;
    /*^cond */
    /*cond */ if ( /*_#OR___L2*/ meltfnum[1])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-debug.melt:241:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^cond */
	  /*cond */ if (
			 /*ifisa */
			 melt_is_instance_of ((melt_ptr_t)
					      (( /*!EDGE_DEBUG_FUNCONT */
						meltfrout->tabval[0])),
					      (melt_ptr_t) (( /*!CLASS_REFERENCE */ meltfrout->tabval[1])))
	    )			/*then */
	    {
	      /*^cond.then */
	      /*^block */
	      /*anyblock */
	      {

		/*^putslot */
		/*putslot */
		melt_assertmsg ("putslot checkobj @REFERENCED_VALUE",
				melt_magic_discr ((melt_ptr_t)
						  (( /*!EDGE_DEBUG_FUNCONT */
						    meltfrout->tabval[0]))) ==
				MELTOBMAG_OBJECT);
		melt_putfield_object ((( /*!EDGE_DEBUG_FUNCONT */ meltfrout->
					tabval[0])), (0),
				      ( /*_.F__V2*/ meltfptr[1]),
				      "REFERENCED_VALUE");
		;
		/*^touch */
		meltgc_touch (( /*!EDGE_DEBUG_FUNCONT */ meltfrout->
			       tabval[0]));
		;
		/*^touchobj */

		melt_dbgtrace_written_object (( /*!EDGE_DEBUG_FUNCONT */
					       meltfrout->tabval[0]),
					      "put-fields");
		;
		/*epilog */
	      }
	      ;
	    }			/*noelse */
	  ;
	  /*epilog */
	}
	;
      }				/*noelse */
    ;
    /*_.LET___V3*/ meltfptr[2] = /*_.OLDCONT__V4*/ meltfptr[3];;

    MELT_LOCATION ("warmelt-debug.melt:238:/ clear");
	   /*clear *//*_.OLDCONT__V4*/ meltfptr[3] = 0;
    /*^clear */
	   /*clear *//*_#NULL__L1*/ meltfnum[0] = 0;
    /*^clear */
	   /*clear *//*_#OR___L2*/ meltfnum[1] = 0;
    MELT_LOCATION ("warmelt-debug.melt:233:/ quasiblock");


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.LET___V3*/ meltfptr[2];;

    {
      MELT_LOCATION ("warmelt-debug.melt:233:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.LET___V3*/ meltfptr[2] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("REGISTER_EDGE_DEBUG_FUN", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_5_warmelt_debug_REGISTER_EDGE_DEBUG_FUN_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_5_warmelt_debug_REGISTER_EDGE_DEBUG_FUN */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_6_warmelt_debug_REGISTER_BASICBLOCK_DEBUG_FUN (meltclosure_ptr_t
							meltclosp_,
							melt_ptr_t
							meltfirstargp_,
							const
							melt_argdescr_cell_t
							meltxargdescr_[],
							union meltparam_un *
							meltxargtab_,
							const
							melt_argdescr_cell_t
							meltxresdescr_[],
							union meltparam_un *
							meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_6_warmelt_debug_REGISTER_BASICBLOCK_DEBUG_FUN_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_6_warmelt_debug_REGISTER_BASICBLOCK_DEBUG_FUN_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 4
    melt_ptr_t mcfr_varptr[4];
#define MELTFRAM_NBVARNUM 3
    long mcfr_varnum[3];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_6_warmelt_debug_REGISTER_BASICBLOCK_DEBUG_FUN is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct
	 frame_meltrout_6_warmelt_debug_REGISTER_BASICBLOCK_DEBUG_FUN_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 4; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_6_warmelt_debug_REGISTER_BASICBLOCK_DEBUG_FUN nbval 4*/
  meltfram__.mcfr_nbvar = 4 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("REGISTER_BASICBLOCK_DEBUG_FUN", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-debug.melt:246:/ getarg");
 /*_.F__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {

    MELT_LOCATION ("warmelt-debug.melt:251:/ quasiblock");


    /*^cond */
    /*cond */ if (
		   /*ifisa */
		   melt_is_instance_of ((melt_ptr_t)
					(( /*!BASICBLOCK_DEBUG_FUNCONT */
					  meltfrout->tabval[0])),
					(melt_ptr_t) (( /*!CLASS_REFERENCE */
						       meltfrout->tabval[1])))
      )				/*then */
      {
	/*^cond.then */
	/*^getslot */
	{
	  melt_ptr_t slot = NULL, obj = NULL;
	  obj =
	    (melt_ptr_t) (( /*!BASICBLOCK_DEBUG_FUNCONT */ meltfrout->
			   tabval[0])) /*=obj*/ ;
	  melt_object_get_field (slot, obj, 0, "REFERENCED_VALUE");
   /*_.OLDCONT__V4*/ meltfptr[3] = slot;
	};
	;
      }
    else
      {				/*^cond.else */

  /*_.OLDCONT__V4*/ meltfptr[3] = NULL;;
      }
    ;
    MELT_LOCATION ("warmelt-debug.melt:253:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#NULL__L1*/ meltfnum[0] =
      (( /*_.F__V2*/ meltfptr[1]) == NULL);;
    MELT_LOCATION ("warmelt-debug.melt:253:/ cond");
    /*cond */ if ( /*_#NULL__L1*/ meltfnum[0])	/*then */
      {
	/*^cond.then */
	/*_#OR___L2*/ meltfnum[1] = /*_#NULL__L1*/ meltfnum[0];;
      }
    else
      {
	MELT_LOCATION ("warmelt-debug.melt:253:/ cond.else");

	/*^block */
	/*anyblock */
	{

   /*_#IS_CLOSURE__L3*/ meltfnum[2] =
	    (melt_magic_discr ((melt_ptr_t) ( /*_.F__V2*/ meltfptr[1])) ==
	     MELTOBMAG_CLOSURE);;
	  /*^compute */
	  /*_#OR___L2*/ meltfnum[1] = /*_#IS_CLOSURE__L3*/ meltfnum[2];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-debug.melt:253:/ clear");
	     /*clear *//*_#IS_CLOSURE__L3*/ meltfnum[2] = 0;
	}
	;
      }
    ;
    /*^cond */
    /*cond */ if ( /*_#OR___L2*/ meltfnum[1])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-debug.melt:254:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^cond */
	  /*cond */ if (
			 /*ifisa */
			 melt_is_instance_of ((melt_ptr_t)
					      (( /*!BASICBLOCK_DEBUG_FUNCONT */ meltfrout->tabval[0])),
					      (melt_ptr_t) (( /*!CLASS_REFERENCE */ meltfrout->tabval[1])))
	    )			/*then */
	    {
	      /*^cond.then */
	      /*^block */
	      /*anyblock */
	      {

		/*^putslot */
		/*putslot */
		melt_assertmsg ("putslot checkobj @REFERENCED_VALUE",
				melt_magic_discr ((melt_ptr_t)
						  (( /*!BASICBLOCK_DEBUG_FUNCONT */ meltfrout->tabval[0]))) == MELTOBMAG_OBJECT);
		melt_putfield_object ((( /*!BASICBLOCK_DEBUG_FUNCONT */
					meltfrout->tabval[0])), (0),
				      ( /*_.F__V2*/ meltfptr[1]),
				      "REFERENCED_VALUE");
		;
		/*^touch */
		meltgc_touch (( /*!BASICBLOCK_DEBUG_FUNCONT */ meltfrout->
			       tabval[0]));
		;
		/*^touchobj */

		melt_dbgtrace_written_object (( /*!BASICBLOCK_DEBUG_FUNCONT */
					       meltfrout->tabval[0]),
					      "put-fields");
		;
		/*epilog */
	      }
	      ;
	    }			/*noelse */
	  ;
	  /*epilog */
	}
	;
      }				/*noelse */
    ;
    /*_.LET___V3*/ meltfptr[2] = /*_.OLDCONT__V4*/ meltfptr[3];;

    MELT_LOCATION ("warmelt-debug.melt:251:/ clear");
	   /*clear *//*_.OLDCONT__V4*/ meltfptr[3] = 0;
    /*^clear */
	   /*clear *//*_#NULL__L1*/ meltfnum[0] = 0;
    /*^clear */
	   /*clear *//*_#OR___L2*/ meltfnum[1] = 0;
    MELT_LOCATION ("warmelt-debug.melt:246:/ quasiblock");


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.LET___V3*/ meltfptr[2];;

    {
      MELT_LOCATION ("warmelt-debug.melt:246:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.LET___V3*/ meltfptr[2] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("REGISTER_BASICBLOCK_DEBUG_FUN", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_6_warmelt_debug_REGISTER_BASICBLOCK_DEBUG_FUN_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_6_warmelt_debug_REGISTER_BASICBLOCK_DEBUG_FUN */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_7_warmelt_debug_REGISTER_LOOP_DEBUG_FUN (meltclosure_ptr_t
						  meltclosp_,
						  melt_ptr_t meltfirstargp_,
						  const melt_argdescr_cell_t
						  meltxargdescr_[],
						  union meltparam_un *
						  meltxargtab_,
						  const melt_argdescr_cell_t
						  meltxresdescr_[],
						  union meltparam_un *
						  meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_7_warmelt_debug_REGISTER_LOOP_DEBUG_FUN_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_7_warmelt_debug_REGISTER_LOOP_DEBUG_FUN_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 4
    melt_ptr_t mcfr_varptr[4];
#define MELTFRAM_NBVARNUM 3
    long mcfr_varnum[3];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_7_warmelt_debug_REGISTER_LOOP_DEBUG_FUN is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_7_warmelt_debug_REGISTER_LOOP_DEBUG_FUN_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 4; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_7_warmelt_debug_REGISTER_LOOP_DEBUG_FUN nbval 4*/
  meltfram__.mcfr_nbvar = 4 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("REGISTER_LOOP_DEBUG_FUN", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-debug.melt:259:/ getarg");
 /*_.F__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {

    MELT_LOCATION ("warmelt-debug.melt:264:/ quasiblock");


    /*^cond */
    /*cond */ if (
		   /*ifisa */
		   melt_is_instance_of ((melt_ptr_t)
					(( /*!LOOP_DEBUG_FUNCONT */
					  meltfrout->tabval[0])),
					(melt_ptr_t) (( /*!CLASS_REFERENCE */
						       meltfrout->tabval[1])))
      )				/*then */
      {
	/*^cond.then */
	/*^getslot */
	{
	  melt_ptr_t slot = NULL, obj = NULL;
	  obj =
	    (melt_ptr_t) (( /*!LOOP_DEBUG_FUNCONT */ meltfrout->
			   tabval[0])) /*=obj*/ ;
	  melt_object_get_field (slot, obj, 0, "REFERENCED_VALUE");
   /*_.OLDCONT__V4*/ meltfptr[3] = slot;
	};
	;
      }
    else
      {				/*^cond.else */

  /*_.OLDCONT__V4*/ meltfptr[3] = NULL;;
      }
    ;
    MELT_LOCATION ("warmelt-debug.melt:266:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#NULL__L1*/ meltfnum[0] =
      (( /*_.F__V2*/ meltfptr[1]) == NULL);;
    MELT_LOCATION ("warmelt-debug.melt:266:/ cond");
    /*cond */ if ( /*_#NULL__L1*/ meltfnum[0])	/*then */
      {
	/*^cond.then */
	/*_#OR___L2*/ meltfnum[1] = /*_#NULL__L1*/ meltfnum[0];;
      }
    else
      {
	MELT_LOCATION ("warmelt-debug.melt:266:/ cond.else");

	/*^block */
	/*anyblock */
	{

   /*_#IS_CLOSURE__L3*/ meltfnum[2] =
	    (melt_magic_discr ((melt_ptr_t) ( /*_.F__V2*/ meltfptr[1])) ==
	     MELTOBMAG_CLOSURE);;
	  /*^compute */
	  /*_#OR___L2*/ meltfnum[1] = /*_#IS_CLOSURE__L3*/ meltfnum[2];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-debug.melt:266:/ clear");
	     /*clear *//*_#IS_CLOSURE__L3*/ meltfnum[2] = 0;
	}
	;
      }
    ;
    /*^cond */
    /*cond */ if ( /*_#OR___L2*/ meltfnum[1])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-debug.melt:267:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^cond */
	  /*cond */ if (
			 /*ifisa */
			 melt_is_instance_of ((melt_ptr_t)
					      (( /*!LOOP_DEBUG_FUNCONT */
						meltfrout->tabval[0])),
					      (melt_ptr_t) (( /*!CLASS_REFERENCE */ meltfrout->tabval[1])))
	    )			/*then */
	    {
	      /*^cond.then */
	      /*^block */
	      /*anyblock */
	      {

		/*^putslot */
		/*putslot */
		melt_assertmsg ("putslot checkobj @REFERENCED_VALUE",
				melt_magic_discr ((melt_ptr_t)
						  (( /*!LOOP_DEBUG_FUNCONT */
						    meltfrout->tabval[0]))) ==
				MELTOBMAG_OBJECT);
		melt_putfield_object ((( /*!LOOP_DEBUG_FUNCONT */ meltfrout->
					tabval[0])), (0),
				      ( /*_.F__V2*/ meltfptr[1]),
				      "REFERENCED_VALUE");
		;
		/*^touch */
		meltgc_touch (( /*!LOOP_DEBUG_FUNCONT */ meltfrout->
			       tabval[0]));
		;
		/*^touchobj */

		melt_dbgtrace_written_object (( /*!LOOP_DEBUG_FUNCONT */
					       meltfrout->tabval[0]),
					      "put-fields");
		;
		/*epilog */
	      }
	      ;
	    }			/*noelse */
	  ;
	  /*epilog */
	}
	;
      }				/*noelse */
    ;
    /*_.LET___V3*/ meltfptr[2] = /*_.OLDCONT__V4*/ meltfptr[3];;

    MELT_LOCATION ("warmelt-debug.melt:264:/ clear");
	   /*clear *//*_.OLDCONT__V4*/ meltfptr[3] = 0;
    /*^clear */
	   /*clear *//*_#NULL__L1*/ meltfnum[0] = 0;
    /*^clear */
	   /*clear *//*_#OR___L2*/ meltfnum[1] = 0;
    MELT_LOCATION ("warmelt-debug.melt:259:/ quasiblock");


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.LET___V3*/ meltfptr[2];;

    {
      MELT_LOCATION ("warmelt-debug.melt:259:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.LET___V3*/ meltfptr[2] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("REGISTER_LOOP_DEBUG_FUN", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_7_warmelt_debug_REGISTER_LOOP_DEBUG_FUN_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_7_warmelt_debug_REGISTER_LOOP_DEBUG_FUN */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_8_warmelt_debug_MELT_DEBUG_FUN (meltclosure_ptr_t meltclosp_,
					 melt_ptr_t meltfirstargp_,
					 const melt_argdescr_cell_t
					 meltxargdescr_[],
					 union meltparam_un * meltxargtab_,
					 const melt_argdescr_cell_t
					 meltxresdescr_[],
					 union meltparam_un * meltxrestab_)
{
  /*variadic */ int variad_MELT_DEBUG_FUN_ix = 0, variad_MELT_DEBUG_FUN_len =
    melt_argdescr_length (meltxargdescr_);
#define melt_variadic_length  (0+variad_MELT_DEBUG_FUN_len)
#define melt_variadic_index variad_MELT_DEBUG_FUN_ix

  long current_blocklevel_signals_meltrout_8_warmelt_debug_MELT_DEBUG_FUN_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_8_warmelt_debug_MELT_DEBUG_FUN_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 69
    melt_ptr_t mcfr_varptr[69];
#define MELTFRAM_NBVARNUM 33
    long mcfr_varnum[33];
/*others*/
    const char *loc_CSTRING__o0;
    const char *loc_CSTRING__o1;
    tree loc_TREE__o2;
    gimple loc_GIMPLE__o3;
    gimple_seq loc_GIMPLE_SEQ__o4;
    edge loc_EDGE__o5;
    loop_p loc_LOOP__o6;
    tree loc_TREE__o7;
    gimple loc_GIMPLE__o8;
    gimple_seq loc_GIMPLE_SEQ__o9;
    edge loc_EDGE__o10;
    basic_block loc_BASIC_BLOCK__o11;
    loop_p loc_LOOP__o12;
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_8_warmelt_debug_MELT_DEBUG_FUN is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_8_warmelt_debug_MELT_DEBUG_FUN_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 69; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      if (meltframptr_->loc_TREE__o2)
	gt_ggc_mx_tree_node (meltframptr_->loc_TREE__o2);
      if (meltframptr_->loc_GIMPLE__o3)
	gt_ggc_mx_gimple_statement_d (meltframptr_->loc_GIMPLE__o3);
      if (meltframptr_->loc_GIMPLE_SEQ__o4)
	gt_ggc_mx_gimple_seq_d (meltframptr_->loc_GIMPLE_SEQ__o4);
      if (meltframptr_->loc_EDGE__o5)
	gt_ggc_mx_edge_def (meltframptr_->loc_EDGE__o5);
      if (meltframptr_->loc_LOOP__o6)
	gt_ggc_mx_loop (meltframptr_->loc_LOOP__o6);
      if (meltframptr_->loc_TREE__o7)
	gt_ggc_mx_tree_node (meltframptr_->loc_TREE__o7);
      if (meltframptr_->loc_GIMPLE__o8)
	gt_ggc_mx_gimple_statement_d (meltframptr_->loc_GIMPLE__o8);
      if (meltframptr_->loc_GIMPLE_SEQ__o9)
	gt_ggc_mx_gimple_seq_d (meltframptr_->loc_GIMPLE_SEQ__o9);
      if (meltframptr_->loc_EDGE__o10)
	gt_ggc_mx_edge_def (meltframptr_->loc_EDGE__o10);
      if (meltframptr_->loc_BASIC_BLOCK__o11)
	gt_ggc_mx_basic_block_def (meltframptr_->loc_BASIC_BLOCK__o11);
      if (meltframptr_->loc_LOOP__o12)
	gt_ggc_mx_loop (meltframptr_->loc_LOOP__o12);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_8_warmelt_debug_MELT_DEBUG_FUN nbval 69*/
  meltfram__.mcfr_nbvar = 69 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("MELT_DEBUG_FUN", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-debug.melt:280:/ getarg");
 /*_.NOTHING__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_LONG)
    {				/*getargrest */
      variad_MELT_DEBUG_FUN_ix = variad_MELT_DEBUG_FUN_len;
      goto lab_endgetargs;
    }
  variad_MELT_DEBUG_FUN_ix = 1;
 /*_#COUNT__L1*/ meltfnum[0] = meltxargtab_[0].meltbp_long;

  /*getarg#2 */
  /*^getarg */
  if (meltxargdescr_[1] != MELTBPAR_CSTRING)
    {				/*getargrest */
      variad_MELT_DEBUG_FUN_ix = variad_MELT_DEBUG_FUN_len;
      goto lab_endgetargs;
    }
  variad_MELT_DEBUG_FUN_ix = 2;
 /*_?*/ meltfram__.loc_CSTRING__o0 = meltxargtab_[1].meltbp_cstring;

  /*getarg#3 */
  /*^getarg */
  if (meltxargdescr_[2] != MELTBPAR_LONG)
    {				/*getargrest */
      variad_MELT_DEBUG_FUN_ix = variad_MELT_DEBUG_FUN_len;
      goto lab_endgetargs;
    }
  variad_MELT_DEBUG_FUN_ix = 3;
 /*_#LINENO__L2*/ meltfnum[1] = meltxargtab_[2].meltbp_long;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {

    MELT_LOCATION ("warmelt-debug.melt:281:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#MELT_HAS_FLAG_DEBUG_SET__L3*/ meltfnum[2] =
      /*MELT_HAS_FLAG_DEBUG_SET */ melt_flag_debug;;
    MELT_LOCATION ("warmelt-debug.melt:281:/ cond");
    /*cond */ if ( /*_#MELT_HAS_FLAG_DEBUG_SET__L3*/ meltfnum[2])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-debug.melt:282:/ quasiblock");


   /*_#DBGCOUNTER__L4*/ meltfnum[3] = 0;;
	  MELT_LOCATION ("warmelt-debug.melt:283:/ cond");
	  /*cond */ if (
			 /*ifisa */
			 melt_is_instance_of ((melt_ptr_t)
					      (( /*!INITIAL_SYSTEM_DATA */
						meltfrout->tabval[0])),
					      (melt_ptr_t) (( /*!CLASS_SYSTEM_DATA */ meltfrout->tabval[1])))
	    )			/*then */
	    {
	      /*^cond.then */
	      /*^getslot */
	      {
		melt_ptr_t slot = NULL, obj = NULL;
		obj =
		  (melt_ptr_t) (( /*!INITIAL_SYSTEM_DATA */ meltfrout->
				 tabval[0])) /*=obj*/ ;
		melt_object_get_field (slot, obj, 20, "SYSDATA_DUMPFILE");
     /*_.DUMPF__V3*/ meltfptr[2] = slot;
	      };
	      ;
	    }
	  else
	    {			/*^cond.else */

    /*_.DUMPF__V3*/ meltfptr[2] = NULL;;
	    }
	  ;
	  MELT_LOCATION ("warmelt-debug.melt:284:/ cond");
	  /*cond */ if (
			 /*ifisa */
			 melt_is_instance_of ((melt_ptr_t)
					      (( /*!INITIAL_SYSTEM_DATA */
						meltfrout->tabval[0])),
					      (melt_ptr_t) (( /*!CLASS_SYSTEM_DATA */ meltfrout->tabval[1])))
	    )			/*then */
	    {
	      /*^cond.then */
	      /*^getslot */
	      {
		melt_ptr_t slot = NULL, obj = NULL;
		obj =
		  (melt_ptr_t) (( /*!INITIAL_SYSTEM_DATA */ meltfrout->
				 tabval[0])) /*=obj*/ ;
		melt_object_get_field (slot, obj, 19, "SYSDATA_STDERR");
     /*_.STDERRF__V4*/ meltfptr[3] = slot;
	      };
	      ;
	    }
	  else
	    {			/*^cond.else */

    /*_.STDERRF__V4*/ meltfptr[3] = NULL;;
	    }
	  ;
	  /*^compute */
	  /*_.OUT__V5*/ meltfptr[4] = /*_.DUMPF__V3*/ meltfptr[2];;
	  /*^compute */
   /*_.OCCMAP__V6*/ meltfptr[5] =
	    (meltgc_new_mapobjects
	     ((meltobject_ptr_t)
	      (( /*!DISCR_MAP_OBJECTS */ meltfrout->tabval[2])), (50)));;
	  /*^compute */
   /*_#DEBUG_DEPTH__L5*/ meltfnum[4] =
	    (long) (melt_debug_depth ());;
	  /*^compute */
   /*_.BOXEDMAXDEPTH__V7*/ meltfptr[6] =
	    (meltgc_new_int
	     ((meltobject_ptr_t)
	      (( /*!DISCR_INTEGER */ meltfrout->tabval[3])),
	      ( /*_#DEBUG_DEPTH__L5*/ meltfnum[4])));;

	  {
	    MELT_LOCATION ("warmelt-debug.melt:290:/ locexp");
					/* melt_debug_fun GETDBGCOUNTER__2 *//*_#DBGCOUNTER__L4*/
	      meltfnum[3] = melt_dbgcounter;
	    ;
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-debug.melt:294:/ locexp");
	    /* melt_debug_fun ENSUREGOODOUT__2 */
	    {
	      static int nbwarn;
	      if (melt_get_file ((melt_ptr_t) /*_.OUT__V5*/ meltfptr[4]) ==
		  NULL)
		{
		  gcc_assert (melt_get_file
			      ((melt_ptr_t) /*_.STDERRF__V4*/ meltfptr[3]) ==
			      stderr);
		  /*_.OUT__V5*/ meltfptr[4] = /*_.STDERRF__V4*/ meltfptr[3];
		  if (nbwarn++ <= 0)
		    {
		      inform (UNKNOWN_LOCATION,
			      "MELT debug output goes to stderr because we have no dump_file.");
		    }
		}
	    } /* end melt_debug_fun ENSUREGOODOUT__2 */ ;
	  }
	  ;
	  MELT_LOCATION ("warmelt-debug.melt:305:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
   /*_#NULL__L6*/ meltfnum[5] =
	    (( /*_.OUT__V5*/ meltfptr[4]) == NULL);;
	  MELT_LOCATION ("warmelt-debug.melt:305:/ cond");
	  /*cond */ if ( /*_#NULL__L6*/ meltfnum[5])	/*then */
	    {
	      /*^cond.then */
	      /*^block */
	      /*anyblock */
	      {

		MELT_LOCATION ("warmelt-debug.melt:306:/ cond");
		/*cond */ if (
			       /*ifisa */
			       melt_is_instance_of ((melt_ptr_t)
						    (( /*!INITIAL_SYSTEM_DATA */ meltfrout->tabval[0])),
						    (melt_ptr_t) (( /*!CLASS_SYSTEM_DATA */ meltfrout->tabval[1])))
		  )		/*then */
		  {
		    /*^cond.then */
		    /*^getslot */
		    {
		      melt_ptr_t slot = NULL, obj = NULL;
		      obj =
			(melt_ptr_t) (( /*!INITIAL_SYSTEM_DATA */ meltfrout->
				       tabval[0])) /*=obj*/ ;
		      melt_object_get_field (slot, obj, 19, "SYSDATA_STDERR");
       /*_.SYSDATA_STDERR__V9*/ meltfptr[8] = slot;
		    };
		    ;
		  }
		else
		  {		/*^cond.else */

      /*_.SYSDATA_STDERR__V9*/ meltfptr[8] = NULL;;
		  }
		;
		MELT_LOCATION ("warmelt-debug.melt:306:/ compute");
		/*_.OUT__V5*/ meltfptr[4] = /*_.SETQ___V10*/ meltfptr[9] =
		  /*_.SYSDATA_STDERR__V9*/ meltfptr[8];;
		/*_.IF___V8*/ meltfptr[7] = /*_.SETQ___V10*/ meltfptr[9];;
		/*epilog */

		MELT_LOCATION ("warmelt-debug.melt:305:/ clear");
	       /*clear *//*_.SYSDATA_STDERR__V9*/ meltfptr[8] = 0;
		/*^clear */
	       /*clear *//*_.SETQ___V10*/ meltfptr[9] = 0;
	      }
	      ;
	    }
	  else
	    {			/*^cond.else */

    /*_.IF___V8*/ meltfptr[7] = NULL;;
	    }
	  ;
	  MELT_LOCATION ("warmelt-debug.melt:310:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
   /*_#NULL__L7*/ meltfnum[6] =
	    (( /*_.OUT__V5*/ meltfptr[4]) == NULL);;
	  MELT_LOCATION ("warmelt-debug.melt:310:/ cond");
	  /*cond */ if ( /*_#NULL__L7*/ meltfnum[6])	/*then */
	    {
	      /*^cond.then */
	      /*^block */
	      /*anyblock */
	      {

     /*_.MAKE_STRBUF__V12*/ meltfptr[9] =
		  (melt_ptr_t)
		  meltgc_new_strbuf ((meltobject_ptr_t)
				     (( /*!DISCR_STRBUF */ meltfrout->
				       tabval[4])), (const char *) 0);;
		MELT_LOCATION ("warmelt-debug.melt:311:/ compute");
		/*_.OUT__V5*/ meltfptr[4] = /*_.SETQ___V13*/ meltfptr[12] =
		  /*_.MAKE_STRBUF__V12*/ meltfptr[9];;
		/*_.IF___V11*/ meltfptr[8] = /*_.SETQ___V13*/ meltfptr[12];;
		/*epilog */

		MELT_LOCATION ("warmelt-debug.melt:310:/ clear");
	       /*clear *//*_.MAKE_STRBUF__V12*/ meltfptr[9] = 0;
		/*^clear */
	       /*clear *//*_.SETQ___V13*/ meltfptr[12] = 0;
	      }
	      ;
	    }
	  else
	    {			/*^cond.else */

    /*_.IF___V11*/ meltfptr[8] = NULL;;
	    }
	  ;

#if MELT_HAVE_DEBUG
	  MELT_LOCATION ("warmelt-debug.melt:312:/ cppif.then");
	  /*^block */
	  /*anyblock */
	  {

	    /*^checksignal */
	    MELT_CHECK_SIGNAL ();
	    ;
     /*_#IS_OUT__L8*/ meltfnum[7] =
	      (melt_is_out ((melt_ptr_t) /*_.OUT__V5*/ meltfptr[4]));;
	    MELT_LOCATION ("warmelt-debug.melt:312:/ cond");
	    /*cond */ if ( /*_#IS_OUT__L8*/ meltfnum[7])	/*then */
	      {
		/*^cond.then */
		/*_.IFELSE___V15*/ meltfptr[12] = ( /*nil */ NULL);;
	      }
	    else
	      {
		MELT_LOCATION ("warmelt-debug.melt:312:/ cond.else");

		/*^block */
		/*anyblock */
		{




		  {
		    /*^locexp */
		    melt_assert_failed (("check good out"),
					("warmelt-debug.melt")
					? ("warmelt-debug.melt") : __FILE__,
					(312) ? (312) : __LINE__,
					__FUNCTION__);
		    ;
		  }
		  ;
		 /*clear *//*_.IFELSE___V15*/ meltfptr[12] = 0;
		  /*epilog */
		}
		;
	      }
	    ;
	    /*^compute */
	    /*_.IFCPP___V14*/ meltfptr[9] = /*_.IFELSE___V15*/ meltfptr[12];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-debug.melt:312:/ clear");
	       /*clear *//*_#IS_OUT__L8*/ meltfnum[7] = 0;
	    /*^clear */
	       /*clear *//*_.IFELSE___V15*/ meltfptr[12] = 0;
	  }

#else /*MELT_HAVE_DEBUG */
	  /*^cppif.else */
	  /*_.IFCPP___V14*/ meltfptr[9] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
	  ;
	  /*citerblock BLOCK_SIGNALS */
	  {
	    /* block_signals meltcit1__BLKSIGNAL start */
	    long meltcit1__BLKSIGNAL_lev = melt_blocklevel_signals;
	    melt_blocklevel_signals = meltcit1__BLKSIGNAL_lev + 1;



	    MELT_LOCATION ("warmelt-debug.melt:317:/ quasiblock");


	    MELT_LOCATION ("warmelt-debug.melt:318:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^quasiblock */


	    /*^rawallocobj */
	    /*rawallocobj */
	    {
	      melt_ptr_t newobj = 0;
	      melt_raw_object_create (newobj,
				      (melt_ptr_t) (( /*!CLASS_DEBUG_INFORMATION */ meltfrout->tabval[5])), (3), "CLASS_DEBUG_INFORMATION");
     /*_.INST__V17*/ meltfptr[16] =
		newobj;
	    };
	    ;
	    /*^putslot */
	    /*putslot */
	    melt_assertmsg ("putslot checkobj @DBGI_OUT",
			    melt_magic_discr ((melt_ptr_t)
					      ( /*_.INST__V17*/ meltfptr[16]))
			    == MELTOBMAG_OBJECT);
	    melt_putfield_object (( /*_.INST__V17*/ meltfptr[16]), (0),
				  ( /*_.OUT__V5*/ meltfptr[4]), "DBGI_OUT");
	    ;
	    /*^putslot */
	    /*putslot */
	    melt_assertmsg ("putslot checkobj @DBGI_OCCMAP",
			    melt_magic_discr ((melt_ptr_t)
					      ( /*_.INST__V17*/ meltfptr[16]))
			    == MELTOBMAG_OBJECT);
	    melt_putfield_object (( /*_.INST__V17*/ meltfptr[16]), (1),
				  ( /*_.OCCMAP__V6*/ meltfptr[5]),
				  "DBGI_OCCMAP");
	    ;
	    /*^putslot */
	    /*putslot */
	    melt_assertmsg ("putslot checkobj @DBGI_MAXDEPTH",
			    melt_magic_discr ((melt_ptr_t)
					      ( /*_.INST__V17*/ meltfptr[16]))
			    == MELTOBMAG_OBJECT);
	    melt_putfield_object (( /*_.INST__V17*/ meltfptr[16]), (2),
				  ( /*_.BOXEDMAXDEPTH__V7*/ meltfptr[6]),
				  "DBGI_MAXDEPTH");
	    ;
	    /*^touchobj */

	    melt_dbgtrace_written_object ( /*_.INST__V17*/ meltfptr[16],
					  "newly made instance");
	    ;
	    /*_.DBGI__V16*/ meltfptr[12] = /*_.INST__V17*/ meltfptr[16];;
	    /*^compute */
    /*_#FRAMDEPTH__L9*/ meltfnum[7] =
	      (melt_curframdepth ());;
	    /*^compute */
    /*_#OUTLEN__L10*/ meltfnum[9] =
	      melt_output_length ((melt_ptr_t) /*_.OUT__V5*/ meltfptr[4]);;
	    /*^compute */
	    /*_#LASTSTROUTLEN__L11*/ meltfnum[10] =
	      /*_#OUTLEN__L10*/ meltfnum[9];;
	    /*^compute */
	    /*_#INITOUTLEN__L12*/ meltfnum[11] =
	      /*_#OUTLEN__L10*/ meltfnum[9];;
	    /*^compute */
    /*_#I__L13*/ meltfnum[12] =
	      (( /*_#FRAMDEPTH__L9*/ meltfnum[7]) - (1));;
	    MELT_LOCATION ("warmelt-debug.melt:327:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_cstring = "!!!!****####";
	      /*^apply.arg */
	      argtab[1].meltbp_long = /*_#DBGCOUNTER__L4*/ meltfnum[3];
	      /*^apply.arg */
	      argtab[2].meltbp_cstring = "#^";
	      /*^apply.arg */
	      argtab[3].meltbp_long = /*_#I__L13*/ meltfnum[12];
	      /*^apply.arg */
	      argtab[4].meltbp_cstring = ":";
	      /*_.ADD2OUT__V18*/ meltfptr[17] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!ADD2OUT */ meltfrout->tabval[6])),
			    (melt_ptr_t) ( /*_.OUT__V5*/ meltfptr[4]),
			    (MELTBPARSTR_CSTRING MELTBPARSTR_LONG
			     MELTBPARSTR_CSTRING MELTBPARSTR_LONG
			     MELTBPARSTR_CSTRING ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    MELT_LOCATION ("warmelt-debug.melt:328:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^cond */
	    /*cond */ if ( /*_?*/ meltfram__.loc_CSTRING__o0)	/*then */
	      {
		/*^cond.then */
		/*^block */
		/*anyblock */
		{

		  MELT_LOCATION ("warmelt-debug.melt:329:/ checksignal");
		  MELT_CHECK_SIGNAL ();
		  ;
		  /*^apply */
		  /*apply */
		  {
		    union meltparam_un argtab[4];
		    memset (&argtab, 0, sizeof (argtab));
		    /*^apply.arg */
		    argtab[0].meltbp_cstring =
		      /*_?*/ meltfram__.loc_CSTRING__o0;
		    /*^apply.arg */
		    argtab[1].meltbp_cstring = ":";
		    /*^apply.arg */
		    argtab[2].meltbp_long = /*_#LINENO__L2*/ meltfnum[1];
		    /*^apply.arg */
		    argtab[3].meltbp_cstring = ":";
		    /*_.ADD2OUT__V20*/ meltfptr[19] =
		      melt_apply ((meltclosure_ptr_t)
				  (( /*!ADD2OUT */ meltfrout->tabval[6])),
				  (melt_ptr_t) ( /*_.OUT__V5*/ meltfptr[4]),
				  (MELTBPARSTR_CSTRING MELTBPARSTR_CSTRING
				   MELTBPARSTR_LONG MELTBPARSTR_CSTRING ""),
				  argtab, "", (union meltparam_un *) 0);
		  }
		  ;
		  /*_.IF___V19*/ meltfptr[18] =
		    /*_.ADD2OUT__V20*/ meltfptr[19];;
		  /*epilog */

		  MELT_LOCATION ("warmelt-debug.melt:328:/ clear");
		/*clear *//*_.ADD2OUT__V20*/ meltfptr[19] = 0;
		}
		;
	      }
	    else
	      {			/*^cond.else */

     /*_.IF___V19*/ meltfptr[18] = NULL;;
	      }
	    ;
	    MELT_LOCATION ("warmelt-debug.melt:330:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
    /*_#I__L14*/ meltfnum[13] =
	      (( /*_#COUNT__L1*/ meltfnum[0]) > (0));;
	    MELT_LOCATION ("warmelt-debug.melt:330:/ cond");
	    /*cond */ if ( /*_#I__L14*/ meltfnum[13])	/*then */
	      {
		/*^cond.then */
		/*^block */
		/*anyblock */
		{

		  MELT_LOCATION ("warmelt-debug.melt:332:/ checksignal");
		  MELT_CHECK_SIGNAL ();
		  ;
		  /*^apply */
		  /*apply */
		  {
		    union meltparam_un argtab[3];
		    memset (&argtab, 0, sizeof (argtab));
		    /*^apply.arg */
		    argtab[0].meltbp_cstring = " !";
		    /*^apply.arg */
		    argtab[1].meltbp_long = /*_#COUNT__L1*/ meltfnum[0];
		    /*^apply.arg */
		    argtab[2].meltbp_cstring = ": ";
		    /*_.ADD2OUT__V22*/ meltfptr[21] =
		      melt_apply ((meltclosure_ptr_t)
				  (( /*!ADD2OUT */ meltfrout->tabval[6])),
				  (melt_ptr_t) ( /*_.OUT__V5*/ meltfptr[4]),
				  (MELTBPARSTR_CSTRING MELTBPARSTR_LONG
				   MELTBPARSTR_CSTRING ""), argtab, "",
				  (union meltparam_un *) 0);
		  }
		  ;
		  MELT_LOCATION ("warmelt-debug.melt:331:/ quasiblock");


		  /*_.PROGN___V23*/ meltfptr[22] =
		    /*_.ADD2OUT__V22*/ meltfptr[21];;
		  /*^compute */
		  /*_.IF___V21*/ meltfptr[19] =
		    /*_.PROGN___V23*/ meltfptr[22];;
		  /*epilog */

		  MELT_LOCATION ("warmelt-debug.melt:330:/ clear");
		/*clear *//*_.ADD2OUT__V22*/ meltfptr[21] = 0;
		  /*^clear */
		/*clear *//*_.PROGN___V23*/ meltfptr[22] = 0;
		}
		;
	      }
	    else
	      {			/*^cond.else */

     /*_.IF___V21*/ meltfptr[19] = NULL;;
	      }
	    ;
	    MELT_LOCATION ("warmelt-debug.melt:333:/ loop");
	    /*loop */
	    {
	    labloop_ARGLOOP_1:;/*^loopbody */

	      /*^block */
	      /*anyblock */
	      {

		/*^checksignal */
		MELT_CHECK_SIGNAL ();
		;
		MELT_LOCATION ("warmelt-debug.melt:335:/ cond");
		/*cond */ if ( /*ifvariadic nomore */ variad_MELT_DEBUG_FUN_ix == variad_MELT_DEBUG_FUN_len)	/*then */
		  {
		    /*^cond.then */
		    /*^block */
		    /*anyblock */
		    {

		      /*^compute */

		      /*consume variadic  ! */ variad_MELT_DEBUG_FUN_ix += 0;;
		      MELT_LOCATION ("warmelt-debug.melt:337:/ quasiblock");


		      /*^compute */
	/*_.ARGLOOP__V25*/ meltfptr[22] = NULL;;

		      /*^exit */
		      /*exit */
		      {
			goto labexit_ARGLOOP_1;
		      }
		      ;
		      /*epilog */
		    }
		    ;
		  }
		else
		  {
		    MELT_LOCATION ("warmelt-debug.melt:335:/ cond.else");

		    /*^block */
		    /*anyblock */
		    {

		      /*^cond */
		      /*cond */ if ( /*ifvariadic arg#1 */ variad_MELT_DEBUG_FUN_ix >= 0 && variad_MELT_DEBUG_FUN_ix + 1 <= variad_MELT_DEBUG_FUN_len && meltxargdescr_[variad_MELT_DEBUG_FUN_ix] == MELTBPAR_LONG)	/*then */
			{
			  /*^cond.then */
			  /*^block */
			  /*anyblock */
			  {

			    /*^compute */
	  /*_#L__L15*/ meltfnum[14] =
			      /*variadic argument stuff */
			      meltxargtab_[variad_MELT_DEBUG_FUN_ix +
					   0].meltbp_long;;
			    /*^compute */

			    /*consume variadic LONG ! */
			      variad_MELT_DEBUG_FUN_ix += 1;;

			    {
			      MELT_LOCATION
				("warmelt-debug.melt:339:/ locexp");
			      meltgc_add_out_dec ((melt_ptr_t)
						  ( /*_.OUT__V5*/
						   meltfptr[4]),
						  ( /*_#L__L15*/
						   meltfnum[14]));
			    }
			    ;
			    /*epilog */

			    MELT_LOCATION ("warmelt-debug.melt:335:/ clear");
		    /*clear *//*_#L__L15*/ meltfnum[14] = 0;
			  }
			  ;
			}
		      else
			{	/*^cond.else */

			  /*^block */
			  /*anyblock */
			  {

			    /*^cond */
			    /*cond */ if ( /*ifvariadic arg#1 */ variad_MELT_DEBUG_FUN_ix >= 0 && variad_MELT_DEBUG_FUN_ix + 1 <= variad_MELT_DEBUG_FUN_len && meltxargdescr_[variad_MELT_DEBUG_FUN_ix] == MELTBPAR_CSTRING)	/*then */
			      {
				/*^cond.then */
				/*^block */
				/*anyblock */
				{

				  /*^compute */
	    /*_?*/ meltfram__.loc_CSTRING__o1 =
				    /*variadic argument stuff */
				    meltxargtab_[variad_MELT_DEBUG_FUN_ix +
						 0].meltbp_cstring;;
				  /*^compute */

				  /*consume variadic CSTRING ! */
				    variad_MELT_DEBUG_FUN_ix += 1;;
				  MELT_LOCATION
				    ("warmelt-debug.melt:341:/ quasiblock");


	    /*_#CUROUTLEN__L16*/ meltfnum[14] =
				    melt_output_length ((melt_ptr_t)
							/*_.OUT__V5*/
							meltfptr[4]);;
				  MELT_LOCATION
				    ("warmelt-debug.melt:343:/ checksignal");
				  MELT_CHECK_SIGNAL ();
				  ;
				  /*^block */
				  /*anyblock */
				  {

				    /*^objgoto */
				    /*objgoto */ goto mtch1_0;
				    ;

				  /*objlabel */ mtch1_0:;
				    MELT_LOCATION
				      ("warmelt-debug.melt:344:/ objlabel");
				    ;
				    /*^cond */
				    /*cond */ if (
						   /*STRPREFIXED_mtch1__1 test */ ( /*_?*/ meltfram__.loc_CSTRING__o1 && " " && !strncmp ( /*_?*/ meltfram__.loc_CSTRING__o1, " ", strlen (" "))))	/*then */
				      {
					/*^cond.then */
					/*^block */
					/*anyblock */
					{


					  {
					    /*^locexp */
					    ;
					  }
					  ;
					  /*^objgoto */
					  /*objgoto */ goto mtch1_1;
					  ;
					}
					;
				      }
				    else
				      {	/*^cond.else */

					/*^block */
					/*anyblock */
					{

					  MELT_LOCATION
					    ("warmelt-debug.melt:347:/ objgoto");
					  /*objgoto */ goto mtch1_2;
					  ;
					}
					;
				      }
				    ;

				  /*objlabel */ mtch1_1:;
				    MELT_LOCATION
				      ("warmelt-debug.melt:344:/ objlabel");
				    ;
				    /*^quasiblock */


				    MELT_LOCATION
				      ("warmelt-debug.melt:345:/ checksignal");
				    MELT_CHECK_SIGNAL ();
				    ;
	     /*_#I__L17*/ meltfnum[16] =
				      (( /*_#LASTSTROUTLEN__L11*/
					meltfnum[10]) + (70));;
				    /*^compute */
	     /*_#I__L18*/ meltfnum[17] =
				      (( /*_#CUROUTLEN__L16*/ meltfnum[14]) >
				       ( /*_#I__L17*/ meltfnum[16]));;
				    MELT_LOCATION
				      ("warmelt-debug.melt:345:/ cond");
				    /*cond */ if ( /*_#I__L18*/ meltfnum[17])	/*then */
				      {
					/*^cond.then */
					/*^block */
					/*anyblock */
					{


					  {
					    MELT_LOCATION
					      ("warmelt-debug.melt:346:/ locexp");
					    meltgc_out_add_indent ((melt_ptr_t) ( /*_.OUT__V5*/ meltfptr[4]), (0), 0);;
					  }
					  ;
					  /*epilog */
					}
					;
				      }	/*noelse */
				    ;

				    MELT_LOCATION
				      ("warmelt-debug.melt:344:/ clear");
		       /*clear *//*_#I__L17*/ meltfnum[16] = 0;
				    /*^clear */
		       /*clear *//*_#I__L18*/ meltfnum[17] = 0;
				    /*^objgoto */
				    /*objgoto */ goto mtch1__end /*endmatch */
				      ;
				    ;

				  /*objlabel */ mtch1_2:;
				    MELT_LOCATION
				      ("warmelt-debug.melt:347:/ objlabel");
				    ;
				    /*^quasiblock */



				    {
				      /*^locexp */
				      /*void */ (void) 0;
				    }
				    ;

				    /*^objgoto */
				    /*objgoto */ goto mtch1__end /*endmatch */
				      ;
				    ;

				  /*objlabel */ mtch1__end:;
				    MELT_LOCATION
				      ("warmelt-debug.melt:343:/ objlabel");
				    ;
				  }
				  ;

				  MELT_LOCATION
				    ("warmelt-debug.melt:341:/ clear");
		      /*clear *//*_#CUROUTLEN__L16*/ meltfnum[14]
				    = 0;
	    /*_#OUTPUT_LENGTH__L19*/ meltfnum[16] =
				    melt_output_length ((melt_ptr_t)
							/*_.OUT__V5*/
							meltfptr[4]);;
				  MELT_LOCATION
				    ("warmelt-debug.melt:349:/ compute");
				  /*_#LASTSTROUTLEN__L11*/ meltfnum[10] =
				    /*_#SETQ___L20*/ meltfnum[17] =
				    /*_#OUTPUT_LENGTH__L19*/ meltfnum[16];;

				  {
				    MELT_LOCATION
				      ("warmelt-debug.melt:350:/ locexp");
				    meltgc_add_out ((melt_ptr_t)
						    ( /*_.OUT__V5*/
						     meltfptr[4]),
						    ( /*_?*/ meltfram__.
						     loc_CSTRING__o1));
				  }
				  ;
				  MELT_LOCATION
				    ("warmelt-debug.melt:335:/ checksignal");
				  MELT_CHECK_SIGNAL ();
				  ;
				  /*epilog */

				  /*^clear */
		      /*clear *//*_?*/ meltfram__.
				    loc_CSTRING__o1 = 0;
				  /*^clear */
		      /*clear *//*_#OUTPUT_LENGTH__L19*/
				    meltfnum[16] = 0;
				  /*^clear */
		      /*clear *//*_#SETQ___L20*/ meltfnum[17] = 0;
				}
				;
			      }
			    else
			      {	/*^cond.else */

				/*^block */
				/*anyblock */
				{

				  /*^cond */
				  /*cond */ if ( /*ifvariadic arg#1 */ variad_MELT_DEBUG_FUN_ix >= 0 && variad_MELT_DEBUG_FUN_ix + 1 <= variad_MELT_DEBUG_FUN_len && meltxargdescr_[variad_MELT_DEBUG_FUN_ix] == MELTBPAR_PTR)	/*then */
				    {
				      /*^cond.then */
				      /*^block */
				      /*anyblock */
				      {

					/*^compute */
	      /*_.VAL__V26*/ meltfptr[25] =
					  /*variadic argument value */
					  ((meltxargtab_
					    [variad_MELT_DEBUG_FUN_ix +
					     0].
					    meltbp_aptr)
					   ? (*
					      (meltxargtab_
					       [variad_MELT_DEBUG_FUN_ix +
						0].meltbp_aptr)) : NULL);;
					/*^compute */

					/*consume variadic Value ! */
					  variad_MELT_DEBUG_FUN_ix += 1;;

					{
					  MELT_LOCATION
					    ("warmelt-debug.melt:353:/ locexp");
					  meltgc_add_out ((melt_ptr_t)
							  ( /*_.OUT__V5*/
							   meltfptr[4]),
							  (" "));
					}
					;
					MELT_LOCATION
					  ("warmelt-debug.melt:355:/ checksignal");
					MELT_CHECK_SIGNAL ();
					;
	      /*_#NULL__L21*/ meltfnum[14] =
					  (( /*_.VAL__V26*/ meltfptr[25]) ==
					   NULL);;
					MELT_LOCATION
					  ("warmelt-debug.melt:355:/ cond");
					/*cond */ if ( /*_#NULL__L21*/ meltfnum[14])	/*then */
					  {
					    /*^cond.then */
					    /*^block */
					    /*anyblock */
					    {




					      {
						MELT_LOCATION
						  ("warmelt-debug.melt:356:/ locexp");
						meltgc_add_out ((melt_ptr_t)
								( /*_.OUT__V5*/ meltfptr[4]), ("(**nil**)"));
					      }
					      ;
			  /*clear *//*_.IFELSE___V27*/
						meltfptr[26] = 0;
					      /*epilog */
					    }
					    ;
					  }
					else
					  {
					    MELT_LOCATION
					      ("warmelt-debug.melt:355:/ cond.else");

					    /*^block */
					    /*anyblock */
					    {

					      MELT_LOCATION
						("warmelt-debug.melt:357:/ checksignal");
					      MELT_CHECK_SIGNAL ();
					      ;
		/*_.DISCRIM__V28*/ meltfptr[27]
						=
						((melt_ptr_t)
						 (melt_discr
						  ((melt_ptr_t)
						   ( /*_.VAL__V26*/
						    meltfptr[25]))));;
					      /*^compute */
		/*_#__L22*/ meltfnum[16] =
						(( /*_.DISCRIM__V28*/
						  meltfptr[27]) ==
						 (( /*!DISCR_DEBUG_CLOSURE */
						   meltfrout->tabval[8])));;
					      MELT_LOCATION
						("warmelt-debug.melt:357:/ cond");
					      /*cond */ if ( /*_#__L22*/ meltfnum[16])	/*then */
						{
						  /*^cond.then */
						  /*^block */
						  /*anyblock */
						  {

						    MELT_LOCATION
						      ("warmelt-debug.melt:358:/ cond");
						    /*cond */ if ( /*ifvariadic nomore */ variad_MELT_DEBUG_FUN_ix == variad_MELT_DEBUG_FUN_len)	/*then */
						      {
							/*^cond.then */
							/*^block */
							/*anyblock */
							{

							  /*^compute */

							  /*consume variadic  ! */
							    variad_MELT_DEBUG_FUN_ix
							    += 0;;
							  MELT_LOCATION
							    ("warmelt-debug.melt:362:/ checksignal");
							  MELT_CHECK_SIGNAL
							    ();
							  ;
							  /*^apply */
							  /*apply */
							  {
							    union meltparam_un
							      argtab[2];
							    memset (&argtab,
								    0,
								    sizeof
								    (argtab));
							    /*^apply.arg */
							    argtab[0].
							      meltbp_aptr =
							      (melt_ptr_t *) &
							      /*_.DBGI__V16*/
							      meltfptr[12];
							    /*^apply.arg */
							    argtab[1].
							      meltbp_long = 0;
							    /*_.DBG_OUT__V30*/
							      meltfptr[29] =
							      melt_apply ((meltclosure_ptr_t) (( /*!DBG_OUT */ meltfrout->tabval[9])), (melt_ptr_t) ( /*_.VAL__V26*/ meltfptr[25]), (MELTBPARSTR_PTR MELTBPARSTR_LONG ""), argtab, "", (union meltparam_un *) 0);
							  }
							  ;
							  /*epilog */

							  MELT_LOCATION
							    ("warmelt-debug.melt:358:/ clear");
			      /*clear *//*_.DBG_OUT__V30*/
							    meltfptr[29] = 0;
							}
							;
						      }
						    else
						      {	/*^cond.else */

							/*^block */
							/*anyblock */
							{

							  /*^cond */
							  /*cond */ if ( /*ifvariadic arg#1 */ variad_MELT_DEBUG_FUN_ix >= 0 && variad_MELT_DEBUG_FUN_ix + 1 <= variad_MELT_DEBUG_FUN_len && meltxargdescr_[variad_MELT_DEBUG_FUN_ix] == MELTBPAR_PTR)	/*then */
							    {
							      /*^cond.then */
							      /*^block */
							      /*anyblock */
							      {

								/*^compute */
		      /*_.VV__V31*/
								  meltfptr
								  [29] =
								  /*variadic argument value */
								  ((meltxargtab_[variad_MELT_DEBUG_FUN_ix + 0].meltbp_aptr) ? (*(meltxargtab_[variad_MELT_DEBUG_FUN_ix + 0].meltbp_aptr)) : NULL);;
								/*^compute */

								/*consume variadic Value ! */
								  variad_MELT_DEBUG_FUN_ix
								  += 1;;
								MELT_LOCATION
								  ("warmelt-debug.melt:365:/ checksignal");
								MELT_CHECK_SIGNAL
								  ();
								;
								/*^apply */
								/*apply */
								{
								  union
								    meltparam_un
								    argtab[1];
								  memset
								    (&argtab,
								     0,
								     sizeof
								     (argtab));
								  /*^apply.arg */
								  argtab[0].
								    meltbp_aptr
								    =
								    (melt_ptr_t
								     *) &
								    /*_.VV__V31*/
								    meltfptr
								    [29];
								  /*_.VAL__V32*/
								    meltfptr
								    [31] =
								    melt_apply
								    ((meltclosure_ptr_t) ( /*_.VAL__V26*/ meltfptr[25]), (melt_ptr_t) ( /*_.DBGI__V16*/ meltfptr[12]), (MELTBPARSTR_PTR ""), argtab, "", (union meltparam_un *) 0);
								}
								;
								/*epilog */

								MELT_LOCATION
								  ("warmelt-debug.melt:358:/ clear");
				/*clear *//*_.VV__V31*/
								  meltfptr
								  [29] = 0;
								/*^clear */
				/*clear *//*_.VAL__V32*/
								  meltfptr
								  [31] = 0;
							      }
							      ;
							    }
							  else
							    {	/*^cond.else */

							      /*^block */
							      /*anyblock */
							      {

								/*^cond */
								/*cond */ if ( /*ifvariadic arg#1 */ variad_MELT_DEBUG_FUN_ix >= 0 && variad_MELT_DEBUG_FUN_ix + 1 <= variad_MELT_DEBUG_FUN_len && meltxargdescr_[variad_MELT_DEBUG_FUN_ix] == MELTBPAR_LONG)	/*then */
								  {
								    /*^cond.then */
								    /*^block */
								    /*anyblock */
								    {

								      /*^compute */
			/*_#LL__L23*/
									meltfnum
									[17] =
									/*variadic argument stuff */
									meltxargtab_
									[variad_MELT_DEBUG_FUN_ix
									 +
									 0].
									meltbp_long;;
								      /*^compute */

								      /*consume variadic LONG ! */
									variad_MELT_DEBUG_FUN_ix
									+= 1;;
								      MELT_LOCATION
									("warmelt-debug.melt:367:/ checksignal");
								      MELT_CHECK_SIGNAL
									();
								      ;
								      /*^apply */
								      /*apply */
								      {
									union
									  meltparam_un
									  argtab
									  [1];
									memset
									  (&argtab,
									   0,
									   sizeof
									   (argtab));
									/*^apply.arg */
									argtab
									  [0].
									  meltbp_long
									  =
									  /*_#LL__L23*/
									  meltfnum
									  [17];
									/*_.VAL__V33*/
									  meltfptr
									  [29]
									  =
									  melt_apply
									  ((meltclosure_ptr_t) ( /*_.VAL__V26*/ meltfptr[25]), (melt_ptr_t) ( /*_.DBGI__V16*/ meltfptr[12]), (MELTBPARSTR_LONG ""), argtab, "", (union meltparam_un *) 0);
								      }
								      ;
								      /*epilog */

								      MELT_LOCATION
									("warmelt-debug.melt:358:/ clear");
				  /*clear *//*_#LL__L23*/
									meltfnum
									[17]
									= 0;
								      /*^clear */
				  /*clear *//*_.VAL__V33*/
									meltfptr
									[29]
									= 0;
								    }
								    ;
								  }
								else
								  {	/*^cond.else */

								    /*^block */
								    /*anyblock */
								    {

								      /*^cond */
								      /*cond */ if ( /*ifvariadic arg#1 */ variad_MELT_DEBUG_FUN_ix >= 0 && variad_MELT_DEBUG_FUN_ix + 1 <= variad_MELT_DEBUG_FUN_len && meltxargdescr_[variad_MELT_DEBUG_FUN_ix] == MELTBPAR_TREE)	/*then */
									{
									  /*^cond.then */
									  /*^block */
									  /*anyblock */
									  {

									    /*^compute */
			  /*_?*/
									      meltfram__.
									      loc_TREE__o2
									      =
									      /*variadic argument stuff */
									      meltxargtab_
									      [variad_MELT_DEBUG_FUN_ix
									       +
									       0].
									      meltbp_tree;;
									    /*^compute */

									    /*consume variadic TREE ! */
									      variad_MELT_DEBUG_FUN_ix
									      +=
									      1;;
									    MELT_LOCATION
									      ("warmelt-debug.melt:369:/ checksignal");
									    MELT_CHECK_SIGNAL
									      ();
									    ;
									    /*^apply */
									    /*apply */
									    {
									      union
										meltparam_un
										argtab
										[1];
									      memset
										(&argtab,
										 0,
										 sizeof
										 (argtab));
									      /*^apply.arg */
									      argtab
										[0].
										meltbp_tree
										=
										/*_?*/
										meltfram__.
										loc_TREE__o2;
									      /*_.VAL__V34*/
										meltfptr
										[31]
										=
										melt_apply
										((meltclosure_ptr_t) ( /*_.VAL__V26*/ meltfptr[25]), (melt_ptr_t) ( /*_.DBGI__V16*/ meltfptr[12]), (MELTBPARSTR_TREE ""), argtab, "", (union meltparam_un *) 0);
									    }
									    ;
									    /*epilog */

									    MELT_LOCATION
									      ("warmelt-debug.melt:358:/ clear");
				    /*clear *//*_?*/
									      meltfram__.
									      loc_TREE__o2
									      =
									      0;
									    /*^clear */
				    /*clear *//*_.VAL__V34*/
									      meltfptr
									      [31]
									      =
									      0;
									  }
									  ;
									}
								      else
									{	/*^cond.else */

									  /*^block */
									  /*anyblock */
									  {

									    /*^cond */
									    /*cond */ if ( /*ifvariadic arg#1 */ variad_MELT_DEBUG_FUN_ix >= 0 && variad_MELT_DEBUG_FUN_ix + 1 <= variad_MELT_DEBUG_FUN_len && meltxargdescr_[variad_MELT_DEBUG_FUN_ix] == MELTBPAR_GIMPLE)	/*then */
									      {
										/*^cond.then */
										/*^block */
										/*anyblock */
										{

										  /*^compute */
			    /*_?*/
										    meltfram__.
										    loc_GIMPLE__o3
										    =
										    /*variadic argument stuff */
										    meltxargtab_
										    [variad_MELT_DEBUG_FUN_ix
										     +
										     0].
										    meltbp_gimple;;
										  /*^compute */

										  /*consume variadic GIMPLE ! */
										    variad_MELT_DEBUG_FUN_ix
										    +=
										    1;;
										  MELT_LOCATION
										    ("warmelt-debug.melt:371:/ checksignal");
										  MELT_CHECK_SIGNAL
										    ();
										  ;
										  /*^apply */
										  /*apply */
										  {
										    union
										      meltparam_un
										      argtab
										      [1];
										    memset
										      (&argtab,
										       0,
										       sizeof
										       (argtab));
										    /*^apply.arg */
										    argtab
										      [0].
										      meltbp_gimple
										      =
										      /*_?*/
										      meltfram__.
										      loc_GIMPLE__o3;
										    /*_.VAL__V35*/
										      meltfptr
										      [29]
										      =
										      melt_apply
										      ((meltclosure_ptr_t) ( /*_.VAL__V26*/ meltfptr[25]), (melt_ptr_t) ( /*_.DBGI__V16*/ meltfptr[12]), (MELTBPARSTR_GIMPLE ""), argtab, "", (union meltparam_un *) 0);
										  }
										  ;
										  /*epilog */

										  MELT_LOCATION
										    ("warmelt-debug.melt:358:/ clear");
				      /*clear *//*_?*/
										    meltfram__.
										    loc_GIMPLE__o3
										    =
										    0;
										  /*^clear */
				      /*clear *//*_.VAL__V35*/
										    meltfptr
										    [29]
										    =
										    0;
										}
										;
									      }
									    else
									      {	/*^cond.else */

										/*^block */
										/*anyblock */
										{

										  /*^cond */
										  /*cond */ if ( /*ifvariadic arg#1 */ variad_MELT_DEBUG_FUN_ix >= 0 && variad_MELT_DEBUG_FUN_ix + 1 <= variad_MELT_DEBUG_FUN_len && meltxargdescr_[variad_MELT_DEBUG_FUN_ix] == MELTBPAR_GIMPLESEQ)	/*then */
										    {
										      /*^cond.then */
										      /*^block */
										      /*anyblock */
										      {

											/*^compute */
			      /*_?*/
											  meltfram__.
											  loc_GIMPLE_SEQ__o4
											  =
											  /*variadic argument stuff */
											  meltxargtab_
											  [variad_MELT_DEBUG_FUN_ix
											   +
											   0].
											  meltbp_gimpleseq;;
											/*^compute */

											/*consume variadic GIMPLE_SEQ ! */
											  variad_MELT_DEBUG_FUN_ix
											  +=
											  1;;
											MELT_LOCATION
											  ("warmelt-debug.melt:373:/ checksignal");
											MELT_CHECK_SIGNAL
											  ();
											;
											/*^apply */
											/*apply */
											{
											  union
											    meltparam_un
											    argtab
											    [1];
											  memset
											    (&argtab,
											     0,
											     sizeof
											     (argtab));
											  /*^apply.arg */
											  argtab
											    [0].
											    meltbp_gimpleseq
											    =
											    /*_?*/
											    meltfram__.
											    loc_GIMPLE_SEQ__o4;
											  /*_.VAL__V36*/
											    meltfptr
											    [31]
											    =
											    melt_apply
											    ((meltclosure_ptr_t) ( /*_.VAL__V26*/ meltfptr[25]), (melt_ptr_t) ( /*_.DBGI__V16*/ meltfptr[12]), (MELTBPARSTR_GIMPLESEQ ""), argtab, "", (union meltparam_un *) 0);
											}
											;
											/*epilog */

											MELT_LOCATION
											  ("warmelt-debug.melt:358:/ clear");
					/*clear *//*_?*/
											  meltfram__.
											  loc_GIMPLE_SEQ__o4
											  =
											  0;
											/*^clear */
					/*clear *//*_.VAL__V36*/
											  meltfptr
											  [31]
											  =
											  0;
										      }
										      ;
										    }
										  else
										    {	/*^cond.else */

										      /*^block */
										      /*anyblock */
										      {

											/*^cond */
											/*cond */ if ( /*ifvariadic arg#1 */ variad_MELT_DEBUG_FUN_ix >= 0 && variad_MELT_DEBUG_FUN_ix + 1 <= variad_MELT_DEBUG_FUN_len && meltxargdescr_[variad_MELT_DEBUG_FUN_ix] == MELTBPAR_EDGE)	/*then */
											  {
											    /*^cond.then */
											    /*^block */
											    /*anyblock */
											    {

/*^compute*/
/*_?*/
												meltfram__.
												loc_EDGE__o5
												=
												/*variadic argument stuff */
												meltxargtab_
												[variad_MELT_DEBUG_FUN_ix
												 +
												 0].
												meltbp_edge;;
/*^compute*/

											      /*consume variadic EDGE ! */
												variad_MELT_DEBUG_FUN_ix
												+=
												1;;
											      MELT_LOCATION
												("warmelt-debug.melt:375:/ checksignal");
											      MELT_CHECK_SIGNAL
												();
											      ;
/*^apply*/
/*apply*/
											      {
												union
												  meltparam_un
												  argtab
												  [1];
												memset
												  (&argtab,
												   0,
												   sizeof
												   (argtab));
												/*^apply.arg */
												argtab
												  [0].
												  meltbp_edge
												  =
												  /*_?*/
												  meltfram__.
												  loc_EDGE__o5;
												/*_.VAL__V37*/
												  meltfptr
												  [29]
												  =
												  melt_apply
												  ((meltclosure_ptr_t) ( /*_.VAL__V26*/ meltfptr[25]), (melt_ptr_t) ( /*_.DBGI__V16*/ meltfptr[12]), (MELTBPARSTR_EDGE ""), argtab, "", (union meltparam_un *) 0);
											      }
											      ;
/*epilog*/

											      MELT_LOCATION
												("warmelt-debug.melt:358:/ clear");
	  /*clear*//*_?*/
												meltfram__.
												loc_EDGE__o5
												=
												0;
/*^clear*/
	  /*clear*//*_.VAL__V37*/
												meltfptr
												[29]
												=
												0;
											    }
											    ;
											  }
											else
											  {	/*^cond.else */

											    /*^block */
											    /*anyblock */
											    {

/*^cond*/
											      /*cond*/ if ( /*ifvariadic arg#1 */ variad_MELT_DEBUG_FUN_ix >= 0 && variad_MELT_DEBUG_FUN_ix + 1 <= variad_MELT_DEBUG_FUN_len && meltxargdescr_[variad_MELT_DEBUG_FUN_ix] == MELTBPAR_LOOP)	/*then */
												{
/*^cond.then*/
/*^block*/
												  /*anyblock */
												  {

												    /*^compute */
  /*_?*/
												      meltfram__.
												      loc_LOOP__o6
												      =
												      /*variadic argument stuff */
												      meltxargtab_
												      [variad_MELT_DEBUG_FUN_ix
												       +
												       0].
												      meltbp_loop;;
												    /*^compute */

												    /*consume variadic LOOP ! */
												      variad_MELT_DEBUG_FUN_ix
												      +=
												      1;;
												    MELT_LOCATION
												      ("warmelt-debug.melt:377:/ checksignal");
												    MELT_CHECK_SIGNAL
												      ();
												    ;
												    /*^apply */
												    /*apply */
												    {
												      union
													meltparam_un
													argtab
													[1];
												      memset
													(&argtab,
													 0,
													 sizeof
													 (argtab));
												      /*^apply.arg */
												      argtab
													[0].
													meltbp_loop
													=
													/*_?*/
													meltfram__.
													loc_LOOP__o6;
												      /*_.VAL__V38*/
													meltfptr
													[31]
													=
													melt_apply
													((meltclosure_ptr_t) ( /*_.VAL__V26*/ meltfptr[25]), (melt_ptr_t) ( /*_.DBGI__V16*/ meltfptr[12]), (MELTBPARSTR_LOOP ""), argtab, "", (union meltparam_un *) 0);
												    }
												    ;
												    /*epilog */

												    MELT_LOCATION
												      ("warmelt-debug.melt:358:/ clear");
	    /*clear *//*_?*/
												      meltfram__.
												      loc_LOOP__o6
												      =
												      0;
												    /*^clear */
	    /*clear *//*_.VAL__V38*/
												      meltfptr
												      [31]
												      =
												      0;
												  }
												  ;
												}	/*noelse */
											      ;
/*epilog*/
											    }
											    ;
											  }
											;
											/*epilog */
										      }
										      ;
										    }
										  ;
										  /*epilog */
										}
										;
									      }
									    ;
									    /*epilog */
									  }
									  ;
									}
								      ;
								      /*epilog */
								    }
								    ;
								  }
								;
								/*epilog */
							      }
							      ;
							    }
							  ;
							  /*epilog */
							}
							;
						      }
						    ;

						    {
						      MELT_LOCATION
							("warmelt-debug.melt:379:/ locexp");
						      /*void */ (void) 0;
						    }
						    ;
						    MELT_LOCATION
						      ("warmelt-debug.melt:357:/ quasiblock");


						    /*epilog */
						  }
						  ;
						}
					      else
						{	/*^cond.else */

						  /*^block */
						  /*anyblock */
						  {

						    MELT_LOCATION
						      ("warmelt-debug.melt:382:/ checksignal");
						    MELT_CHECK_SIGNAL ();
						    ;
						    /*^apply */
						    /*apply */
						    {
						      union meltparam_un
							argtab[2];
						      memset (&argtab, 0,
							      sizeof
							      (argtab));
						      /*^apply.arg */
						      argtab[0].meltbp_aptr =
							(melt_ptr_t *) &
							/*_.DBGI__V16*/
							meltfptr[12];
						      /*^apply.arg */
						      argtab[1].meltbp_long =
							0;
						      /*_.DBG_OUT__V39*/
							meltfptr[29] =
							melt_apply ((meltclosure_ptr_t) (( /*!DBG_OUT */ meltfrout->tabval[9])), (melt_ptr_t) ( /*_.VAL__V26*/ meltfptr[25]), (MELTBPARSTR_PTR MELTBPARSTR_LONG ""), argtab, "", (union meltparam_un *) 0);
						    }
						    ;
						    MELT_LOCATION
						      ("warmelt-debug.melt:381:/ quasiblock");


						    /*_.PROGN___V40*/
						      meltfptr[31] =
						      /*_.DBG_OUT__V39*/
						      meltfptr[29];;
						    /*^compute */
						    /*_.IFELSE___V29*/
						      meltfptr[28] =
						      /*_.PROGN___V40*/
						      meltfptr[31];;
						    /*epilog */

						    MELT_LOCATION
						      ("warmelt-debug.melt:357:/ clear");
			    /*clear *//*_.DBG_OUT__V39*/
						      meltfptr[29] = 0;
						    /*^clear */
			    /*clear *//*_.PROGN___V40*/
						      meltfptr[31] = 0;
						  }
						  ;
						}
					      ;
					      /*_.IFELSE___V27*/ meltfptr[26]
						=
						/*_.IFELSE___V29*/
						meltfptr[28];;
					      /*epilog */

					      MELT_LOCATION
						("warmelt-debug.melt:355:/ clear");
			  /*clear *//*_.DISCRIM__V28*/
						meltfptr[27] = 0;
					      /*^clear */
			  /*clear *//*_#__L22*/
						meltfnum[16] = 0;
					      /*^clear */
			  /*clear *//*_.IFELSE___V29*/
						meltfptr[28] = 0;
					    }
					    ;
					  }
					;
					MELT_LOCATION
					  ("warmelt-debug.melt:335:/ checksignal");
					MELT_CHECK_SIGNAL ();
					;
					/*epilog */

					/*^clear */
			/*clear *//*_.VAL__V26*/ meltfptr[25]
					  = 0;
					/*^clear */
			/*clear *//*_#NULL__L21*/ meltfnum[14]
					  = 0;
					/*^clear */
			/*clear *//*_.IFELSE___V27*/
					  meltfptr[26] = 0;
				      }
				      ;
				    }
				  else
				    {	/*^cond.else */

				      /*^block */
				      /*anyblock */
				      {

					/*^cond */
					/*cond */ if ( /*ifvariadic arg#1 */ variad_MELT_DEBUG_FUN_ix >= 0 && variad_MELT_DEBUG_FUN_ix + 1 <= variad_MELT_DEBUG_FUN_len && meltxargdescr_[variad_MELT_DEBUG_FUN_ix] == MELTBPAR_TREE)	/*then */
					  {
					    /*^cond.then */
					    /*^block */
					    /*anyblock */
					    {

					      /*^compute */
		/*_?*/ meltfram__.loc_TREE__o7 =
						/*variadic argument stuff */
						meltxargtab_
						[variad_MELT_DEBUG_FUN_ix +
						 0].meltbp_tree;;
					      /*^compute */

					      /*consume variadic TREE ! */
						variad_MELT_DEBUG_FUN_ix +=
						1;;
					      MELT_LOCATION
						("warmelt-debug.melt:384:/ quasiblock");


					      /*^cond */
					      /*cond */ if (
							     /*ifisa */
							     melt_is_instance_of
							     ((melt_ptr_t)
							      (( /*!TREE_DEBUG_FUNCONT */ meltfrout->tabval[10])),
							      (melt_ptr_t) (( /*!CLASS_REFERENCE */ meltfrout->tabval[11])))
						)	/*then */
						{
						  /*^cond.then */
						  /*^getslot */
						  {
						    melt_ptr_t slot =
						      NULL, obj = NULL;
						    obj =
						      (melt_ptr_t) (( /*!TREE_DEBUG_FUNCONT */ meltfrout->tabval[10])) /*=obj*/ ;
						    melt_object_get_field
						      (slot, obj, 0,
						       "REFERENCED_VALUE");
		  /*_.TDF__V42*/
						      meltfptr[31] = slot;
						  };
						  ;
						}
					      else
						{	/*^cond.else */

		 /*_.TDF__V42*/ meltfptr[31]
						    = NULL;;
						}
					      ;
					      MELT_LOCATION
						("warmelt-debug.melt:386:/ checksignal");
					      MELT_CHECK_SIGNAL ();
					      ;
		/*_#IS_CLOSURE__L24*/
						meltfnum[17] =
						(melt_magic_discr
						 ((melt_ptr_t)
						  ( /*_.TDF__V42*/
						   meltfptr[31])) ==
						 MELTOBMAG_CLOSURE);;
					      MELT_LOCATION
						("warmelt-debug.melt:386:/ cond");
					      /*cond */ if ( /*_#IS_CLOSURE__L24*/ meltfnum[17])	/*then */
						{
						  /*^cond.then */
						  /*^block */
						  /*anyblock */
						  {

						    MELT_LOCATION
						      ("warmelt-debug.melt:387:/ checksignal");
						    MELT_CHECK_SIGNAL ();
						    ;
						    /*^apply */
						    /*apply */
						    {
						      union meltparam_un
							argtab[1];
						      memset (&argtab, 0,
							      sizeof
							      (argtab));
						      /*^apply.arg */
						      argtab[0].meltbp_tree =
							/*_?*/
							meltfram__.
							loc_TREE__o7;
						      /*_.TDF__V44*/
							meltfptr[28] =
							melt_apply ((meltclosure_ptr_t) ( /*_.TDF__V42*/ meltfptr[31]), (melt_ptr_t) ( /*_.DBGI__V16*/ meltfptr[12]), (MELTBPARSTR_TREE ""), argtab, "", (union meltparam_un *) 0);
						    }
						    ;
						    /*_.IFELSE___V43*/
						      meltfptr[27] =
						      /*_.TDF__V44*/
						      meltfptr[28];;
						    /*epilog */

						    MELT_LOCATION
						      ("warmelt-debug.melt:386:/ clear");
			    /*clear *//*_.TDF__V44*/
						      meltfptr[28] = 0;
						  }
						  ;
						}
					      else
						{	/*^cond.else */

						  /*^block */
						  /*anyblock */
						  {

						    MELT_LOCATION
						      ("warmelt-debug.melt:388:/ checksignal");
						    MELT_CHECK_SIGNAL ();
						    ;
						    /*^cond */
						    /*cond */ if ( /*_?*/ meltfram__.loc_TREE__o7)	/*then */
						      {
							/*^cond.then */
							/*^block */
							/*anyblock */
							{




							  {
							    MELT_LOCATION
							      ("warmelt-debug.melt:389:/ locexp");
							    meltgc_add_out ((melt_ptr_t) ( /*_.OUT__V5*/ meltfptr[4]), (" ?*tree*?"));
							  }
							  ;
			      /*clear *//*_.IFELSE___V43*/
							    meltfptr[27] = 0;
							  /*epilog */
							}
							;
						      }
						    else
						      {
							MELT_LOCATION
							  ("warmelt-debug.melt:388:/ cond.else");

							/*^block */
							/*anyblock */
							{




							  {
							    MELT_LOCATION
							      ("warmelt-debug.melt:390:/ locexp");
							    meltgc_add_out ((melt_ptr_t) ( /*_.OUT__V5*/ meltfptr[4]), (" ?*nulltree*?"));
							  }
							  ;
			      /*clear *//*_.IFELSE___V43*/
							    meltfptr[27] = 0;
							  /*epilog */
							}
							;
						      }
						    ;
						    /*epilog */
						  }
						  ;
						}
					      ;
					      /*^compute */
					      /*_.LET___V41*/ meltfptr[29] =
						/*_.IFELSE___V43*/
						meltfptr[27];;

					      MELT_LOCATION
						("warmelt-debug.melt:384:/ clear");
			  /*clear *//*_.TDF__V42*/
						meltfptr[31] = 0;
					      /*^clear */
			  /*clear *//*_#IS_CLOSURE__L24*/
						meltfnum[17] = 0;
					      /*^clear */
			  /*clear *//*_.IFELSE___V43*/
						meltfptr[27] = 0;
					      /*epilog */

					      MELT_LOCATION
						("warmelt-debug.melt:335:/ clear");
			  /*clear *//*_?*/ meltfram__.
						loc_TREE__o7 = 0;
					      /*^clear */
			  /*clear *//*_.LET___V41*/
						meltfptr[29] = 0;
					    }
					    ;
					  }
					else
					  {	/*^cond.else */

					    /*^block */
					    /*anyblock */
					    {

					      /*^cond */
					      /*cond */ if ( /*ifvariadic arg#1 */ variad_MELT_DEBUG_FUN_ix >= 0 && variad_MELT_DEBUG_FUN_ix + 1 <= variad_MELT_DEBUG_FUN_len && meltxargdescr_[variad_MELT_DEBUG_FUN_ix] == MELTBPAR_GIMPLE)	/*then */
						{
						  /*^cond.then */
						  /*^block */
						  /*anyblock */
						  {

						    /*^compute */
		  /*_?*/ meltfram__.
						      loc_GIMPLE__o8 =
						      /*variadic argument stuff */
						      meltxargtab_
						      [variad_MELT_DEBUG_FUN_ix
						       + 0].meltbp_gimple;;
						    /*^compute */

						    /*consume variadic GIMPLE ! */
						      variad_MELT_DEBUG_FUN_ix
						      += 1;;
						    MELT_LOCATION
						      ("warmelt-debug.melt:392:/ quasiblock");


						    /*^cond */
						    /*cond */ if (
								   /*ifisa */
								   melt_is_instance_of
								   ((melt_ptr_t) (( /*!GIMPLE_DEBUG_FUNCONT */ meltfrout->tabval[12])),
								    (melt_ptr_t) (( /*!CLASS_REFERENCE */ meltfrout->tabval[11])))
						      )	/*then */
						      {
							/*^cond.then */
							/*^getslot */
							{
							  melt_ptr_t slot =
							    NULL, obj = NULL;
							  obj =
							    (melt_ptr_t) (( /*!GIMPLE_DEBUG_FUNCONT */ meltfrout->tabval[12])) /*=obj*/ ;
							  melt_object_get_field
							    (slot, obj, 0,
							     "REFERENCED_VALUE");
		    /*_.GDF__V46*/
							    meltfptr
							    [26] = slot;
							};
							;
						      }
						    else
						      {	/*^cond.else */

		   /*_.GDF__V46*/
							  meltfptr
							  [26] = NULL;;
						      }
						    ;
						    MELT_LOCATION
						      ("warmelt-debug.melt:394:/ checksignal");
						    MELT_CHECK_SIGNAL ();
						    ;
		  /*_#IS_CLOSURE__L25*/
						      meltfnum[16] =
						      (melt_magic_discr
						       ((melt_ptr_t)
							( /*_.GDF__V46*/
							 meltfptr[26])) ==
						       MELTOBMAG_CLOSURE);;
						    MELT_LOCATION
						      ("warmelt-debug.melt:394:/ cond");
						    /*cond */ if ( /*_#IS_CLOSURE__L25*/ meltfnum[16])	/*then */
						      {
							/*^cond.then */
							/*^block */
							/*anyblock */
							{

							  MELT_LOCATION
							    ("warmelt-debug.melt:395:/ checksignal");
							  MELT_CHECK_SIGNAL
							    ();
							  ;
							  /*^apply */
							  /*apply */
							  {
							    union meltparam_un
							      argtab[1];
							    memset (&argtab,
								    0,
								    sizeof
								    (argtab));
							    /*^apply.arg */
							    argtab[0].
							      meltbp_gimple =
							      /*_?*/
							      meltfram__.
							      loc_GIMPLE__o8;
							    /*_.GDF__V48*/
							      meltfptr[31] =
							      melt_apply ((meltclosure_ptr_t) ( /*_.GDF__V46*/ meltfptr[26]), (melt_ptr_t) ( /*_.DBGI__V16*/ meltfptr[12]), (MELTBPARSTR_GIMPLE ""), argtab, "", (union meltparam_un *) 0);
							  }
							  ;
							  /*_.IFELSE___V47*/
							    meltfptr[28] =
							    /*_.GDF__V48*/
							    meltfptr[31];;
							  /*epilog */

							  MELT_LOCATION
							    ("warmelt-debug.melt:394:/ clear");
			      /*clear *//*_.GDF__V48*/
							    meltfptr[31] = 0;
							}
							;
						      }
						    else
						      {	/*^cond.else */

							/*^block */
							/*anyblock */
							{

							  MELT_LOCATION
							    ("warmelt-debug.melt:396:/ checksignal");
							  MELT_CHECK_SIGNAL
							    ();
							  ;
							  /*^cond */
							  /*cond */ if ( /*_?*/ meltfram__.loc_GIMPLE__o8)	/*then */
							    {
							      /*^cond.then */
							      /*^block */
							      /*anyblock */
							      {




								{
								  MELT_LOCATION
								    ("warmelt-debug.melt:397:/ locexp");
								  meltgc_add_out
								    ((melt_ptr_t) ( /*_.OUT__V5*/ meltfptr[4]), (" ?*gimple*?"));
								}
								;
				/*clear *//*_.IFELSE___V47*/
								  meltfptr
								  [28] = 0;
								/*epilog */
							      }
							      ;
							    }
							  else
							    {
							      MELT_LOCATION
								("warmelt-debug.melt:396:/ cond.else");

							      /*^block */
							      /*anyblock */
							      {




								{
								  MELT_LOCATION
								    ("warmelt-debug.melt:398:/ locexp");
								  meltgc_add_out
								    ((melt_ptr_t) ( /*_.OUT__V5*/ meltfptr[4]), (" ?*nullgimple*?"));
								}
								;
				/*clear *//*_.IFELSE___V47*/
								  meltfptr
								  [28] = 0;
								/*epilog */
							      }
							      ;
							    }
							  ;
							  /*epilog */
							}
							;
						      }
						    ;
						    /*^compute */
						    /*_.LET___V45*/
						      meltfptr[25] =
						      /*_.IFELSE___V47*/
						      meltfptr[28];;

						    MELT_LOCATION
						      ("warmelt-debug.melt:392:/ clear");
			    /*clear *//*_.GDF__V46*/
						      meltfptr[26] = 0;
						    /*^clear */
			    /*clear *//*_#IS_CLOSURE__L25*/
						      meltfnum[16] = 0;
						    /*^clear */
			    /*clear *//*_.IFELSE___V47*/
						      meltfptr[28] = 0;
						    /*epilog */

						    MELT_LOCATION
						      ("warmelt-debug.melt:335:/ clear");
			    /*clear *//*_?*/
						      meltfram__.
						      loc_GIMPLE__o8 = 0;
						    /*^clear */
			    /*clear *//*_.LET___V45*/
						      meltfptr[25] = 0;
						  }
						  ;
						}
					      else
						{	/*^cond.else */

						  /*^block */
						  /*anyblock */
						  {

						    /*^cond */
						    /*cond */ if ( /*ifvariadic arg#1 */ variad_MELT_DEBUG_FUN_ix >= 0 && variad_MELT_DEBUG_FUN_ix + 1 <= variad_MELT_DEBUG_FUN_len && meltxargdescr_[variad_MELT_DEBUG_FUN_ix] == MELTBPAR_GIMPLESEQ)	/*then */
						      {
							/*^cond.then */
							/*^block */
							/*anyblock */
							{

							  /*^compute */
		    /*_?*/ meltfram__.
							    loc_GIMPLE_SEQ__o9
							    =
							    /*variadic argument stuff */
							    meltxargtab_
							    [variad_MELT_DEBUG_FUN_ix
							     +
							     0].
							    meltbp_gimpleseq;;
							  /*^compute */

							  /*consume variadic GIMPLE_SEQ ! */
							    variad_MELT_DEBUG_FUN_ix
							    += 1;;
							  MELT_LOCATION
							    ("warmelt-debug.melt:400:/ quasiblock");


							  /*^cond */
							  /*cond */ if (
									 /*ifisa */
									 melt_is_instance_of
									 ((melt_ptr_t) (( /*!GIMPLESEQ_DEBUG_FUNCONT */ meltfrout->tabval[13])),
									  (melt_ptr_t) (( /*!CLASS_REFERENCE */ meltfrout->tabval[11])))
							    )	/*then */
							    {
							      /*^cond.then */
							      /*^getslot */
							      {
								melt_ptr_t
								  slot =
								  NULL, obj =
								  NULL;
								obj =
								  (melt_ptr_t)
								  (( /*!GIMPLESEQ_DEBUG_FUNCONT */ meltfrout->tabval[13])) /*=obj*/ ;
								melt_object_get_field
								  (slot, obj,
								   0,
								   "REFERENCED_VALUE");
		      /*_.GSDF__V50*/
								  meltfptr
								  [29] = slot;
							      };
							      ;
							    }
							  else
							    {	/*^cond.else */

		     /*_.GSDF__V50*/
								meltfptr
								[29] = NULL;;
							    }
							  ;
							  MELT_LOCATION
							    ("warmelt-debug.melt:402:/ checksignal");
							  MELT_CHECK_SIGNAL
							    ();
							  ;
		    /*_#IS_CLOSURE__L26*/
							    meltfnum[14] =
							    (melt_magic_discr
							     ((melt_ptr_t)
							      ( /*_.GSDF__V50*/ meltfptr[29])) == MELTOBMAG_CLOSURE);;
							  MELT_LOCATION
							    ("warmelt-debug.melt:402:/ cond");
							  /*cond */ if ( /*_#IS_CLOSURE__L26*/ meltfnum[14])	/*then */
							    {
							      /*^cond.then */
							      /*^block */
							      /*anyblock */
							      {

								MELT_LOCATION
								  ("warmelt-debug.melt:403:/ checksignal");
								MELT_CHECK_SIGNAL
								  ();
								;
								/*^apply */
								/*apply */
								{
								  union
								    meltparam_un
								    argtab[1];
								  memset
								    (&argtab,
								     0,
								     sizeof
								     (argtab));
								  /*^apply.arg */
								  argtab[0].
								    meltbp_gimpleseq
								    =
								    /*_?*/
								    meltfram__.
								    loc_GIMPLE_SEQ__o9;
								  /*_.GSDF__V52*/
								    meltfptr
								    [26] =
								    melt_apply
								    ((meltclosure_ptr_t) ( /*_.GSDF__V50*/ meltfptr[29]), (melt_ptr_t) ( /*_.DBGI__V16*/ meltfptr[12]), (MELTBPARSTR_GIMPLESEQ ""), argtab, "", (union meltparam_un *) 0);
								}
								;
								/*_.IFELSE___V51*/
								  meltfptr[31]
								  =
								  /*_.GSDF__V52*/
								  meltfptr
								  [26];;
								/*epilog */

								MELT_LOCATION
								  ("warmelt-debug.melt:402:/ clear");
				/*clear *//*_.GSDF__V52*/
								  meltfptr
								  [26] = 0;
							      }
							      ;
							    }
							  else
							    {	/*^cond.else */

							      /*^block */
							      /*anyblock */
							      {

								MELT_LOCATION
								  ("warmelt-debug.melt:404:/ checksignal");
								MELT_CHECK_SIGNAL
								  ();
								;
								/*^cond */
								/*cond */ if ( /*_?*/ meltfram__.loc_GIMPLE_SEQ__o9)	/*then */
								  {
								    /*^cond.then */
								    /*^block */
								    /*anyblock */
								    {




								      {
									MELT_LOCATION
									  ("warmelt-debug.melt:405:/ locexp");
									meltgc_add_out
									  ((melt_ptr_t) ( /*_.OUT__V5*/ meltfptr[4]), (" ?*gimpleseq*?"));
								      }
								      ;
				  /*clear *//*_.IFELSE___V51*/
									meltfptr
									[31]
									= 0;
								      /*epilog */
								    }
								    ;
								  }
								else
								  {
								    MELT_LOCATION
								      ("warmelt-debug.melt:404:/ cond.else");

								    /*^block */
								    /*anyblock */
								    {




								      {
									MELT_LOCATION
									  ("warmelt-debug.melt:406:/ locexp");
									meltgc_add_out
									  ((melt_ptr_t) ( /*_.OUT__V5*/ meltfptr[4]), (" ?*nullgimpleseq*?"));
								      }
								      ;
				  /*clear *//*_.IFELSE___V51*/
									meltfptr
									[31]
									= 0;
								      /*epilog */
								    }
								    ;
								  }
								;
								/*epilog */
							      }
							      ;
							    }
							  ;
							  /*^compute */
							  /*_.LET___V49*/
							    meltfptr[27] =
							    /*_.IFELSE___V51*/
							    meltfptr[31];;

							  MELT_LOCATION
							    ("warmelt-debug.melt:400:/ clear");
			      /*clear *//*_.GSDF__V50*/
							    meltfptr[29] = 0;
							  /*^clear */
			      /*clear *//*_#IS_CLOSURE__L26*/
							    meltfnum[14] = 0;
							  /*^clear */
			      /*clear *//*_.IFELSE___V51*/
							    meltfptr[31] = 0;
							  /*epilog */

							  MELT_LOCATION
							    ("warmelt-debug.melt:335:/ clear");
			      /*clear *//*_?*/
							    meltfram__.
							    loc_GIMPLE_SEQ__o9
							    = 0;
							  /*^clear */
			      /*clear *//*_.LET___V49*/
							    meltfptr[27] = 0;
							}
							;
						      }
						    else
						      {	/*^cond.else */

							/*^block */
							/*anyblock */
							{

							  /*^cond */
							  /*cond */ if ( /*ifvariadic arg#1 */ variad_MELT_DEBUG_FUN_ix >= 0 && variad_MELT_DEBUG_FUN_ix + 1 <= variad_MELT_DEBUG_FUN_len && meltxargdescr_[variad_MELT_DEBUG_FUN_ix] == MELTBPAR_EDGE)	/*then */
							    {
							      /*^cond.then */
							      /*^block */
							      /*anyblock */
							      {

								/*^compute */
		      /*_?*/
								  meltfram__.
								  loc_EDGE__o10
								  =
								  /*variadic argument stuff */
								  meltxargtab_
								  [variad_MELT_DEBUG_FUN_ix
								   +
								   0].
								  meltbp_edge;;
								/*^compute */

								/*consume variadic EDGE ! */
								  variad_MELT_DEBUG_FUN_ix
								  += 1;;
								MELT_LOCATION
								  ("warmelt-debug.melt:408:/ quasiblock");


								/*^cond */
								/*cond */ if (
									       /*ifisa */
									       melt_is_instance_of
									       ((melt_ptr_t) (( /*!EDGE_DEBUG_FUNCONT */ meltfrout->tabval[14])),
										(melt_ptr_t) (( /*!CLASS_REFERENCE */ meltfrout->tabval[11])))
								  )	/*then */
								  {
								    /*^cond.then */
								    /*^getslot */
								    {
								      melt_ptr_t
									slot =
									NULL,
									obj =
									NULL;
								      obj =
									(melt_ptr_t)
									(( /*!EDGE_DEBUG_FUNCONT */ meltfrout->tabval[14])) /*=obj*/ ;
								      melt_object_get_field
									(slot,
									 obj,
									 0,
									 "REFERENCED_VALUE");
			/*_.EDF__V54*/
									meltfptr
									[25]
									=
									slot;
								    };
								    ;
								  }
								else
								  {	/*^cond.else */

		       /*_.EDF__V54*/
								      meltfptr
								      [25]
								      = NULL;;
								  }
								;
								MELT_LOCATION
								  ("warmelt-debug.melt:410:/ checksignal");
								MELT_CHECK_SIGNAL
								  ();
								;
		      /*_#IS_CLOSURE__L27*/
								  meltfnum
								  [17] =
								  (melt_magic_discr ((melt_ptr_t) ( /*_.EDF__V54*/ meltfptr[25])) == MELTOBMAG_CLOSURE);;
								MELT_LOCATION
								  ("warmelt-debug.melt:410:/ cond");
								/*cond */ if ( /*_#IS_CLOSURE__L27*/ meltfnum[17])	/*then */
								  {
								    /*^cond.then */
								    /*^block */
								    /*anyblock */
								    {

								      MELT_LOCATION
									("warmelt-debug.melt:411:/ checksignal");
								      MELT_CHECK_SIGNAL
									();
								      ;
								      /*^apply */
								      /*apply */
								      {
									union
									  meltparam_un
									  argtab
									  [1];
									memset
									  (&argtab,
									   0,
									   sizeof
									   (argtab));
									/*^apply.arg */
									argtab
									  [0].
									  meltbp_edge
									  =
									  /*_?*/
									  meltfram__.
									  loc_EDGE__o10;
									/*_.EDF__V56*/
									  meltfptr
									  [29]
									  =
									  melt_apply
									  ((meltclosure_ptr_t) ( /*_.EDF__V54*/ meltfptr[25]), (melt_ptr_t) ( /*_.DBGI__V16*/ meltfptr[12]), (MELTBPARSTR_EDGE ""), argtab, "", (union meltparam_un *) 0);
								      }
								      ;
								      /*_.IFELSE___V55*/
									meltfptr
									[26] =
									/*_.EDF__V56*/
									meltfptr
									[29];;
								      /*epilog */

								      MELT_LOCATION
									("warmelt-debug.melt:410:/ clear");
				  /*clear *//*_.EDF__V56*/
									meltfptr
									[29]
									= 0;
								    }
								    ;
								  }
								else
								  {	/*^cond.else */

								    /*^block */
								    /*anyblock */
								    {

								      MELT_LOCATION
									("warmelt-debug.melt:412:/ checksignal");
								      MELT_CHECK_SIGNAL
									();
								      ;
								      /*^cond */
								      /*cond */ if ( /*_?*/ meltfram__.loc_EDGE__o10)	/*then */
									{
									  /*^cond.then */
									  /*^block */
									  /*anyblock */
									  {




									    {
									      MELT_LOCATION
										("warmelt-debug.melt:413:/ locexp");
									      meltgc_add_out
										((melt_ptr_t) ( /*_.OUT__V5*/ meltfptr[4]), (" ?*edge*?"));
									    }
									    ;
				    /*clear *//*_.IFELSE___V55*/
									      meltfptr
									      [26]
									      =
									      0;
									    /*epilog */
									  }
									  ;
									}
								      else
									{
									  MELT_LOCATION
									    ("warmelt-debug.melt:412:/ cond.else");

									  /*^block */
									  /*anyblock */
									  {




									    {
									      MELT_LOCATION
										("warmelt-debug.melt:414:/ locexp");
									      meltgc_add_out
										((melt_ptr_t) ( /*_.OUT__V5*/ meltfptr[4]), (" ?*nulledge*?"));
									    }
									    ;
				    /*clear *//*_.IFELSE___V55*/
									      meltfptr
									      [26]
									      =
									      0;
									    /*epilog */
									  }
									  ;
									}
								      ;
								      /*epilog */
								    }
								    ;
								  }
								;
								/*^compute */
								/*_.LET___V53*/
								  meltfptr[28]
								  =
								  /*_.IFELSE___V55*/
								  meltfptr
								  [26];;

								MELT_LOCATION
								  ("warmelt-debug.melt:408:/ clear");
				/*clear *//*_.EDF__V54*/
								  meltfptr
								  [25] = 0;
								/*^clear */
				/*clear *//*_#IS_CLOSURE__L27*/
								  meltfnum
								  [17] = 0;
								/*^clear */
				/*clear *//*_.IFELSE___V55*/
								  meltfptr
								  [26] = 0;
								/*epilog */

								MELT_LOCATION
								  ("warmelt-debug.melt:335:/ clear");
				/*clear *//*_?*/
								  meltfram__.
								  loc_EDGE__o10
								  = 0;
								/*^clear */
				/*clear *//*_.LET___V53*/
								  meltfptr
								  [28] = 0;
							      }
							      ;
							    }
							  else
							    {	/*^cond.else */

							      /*^block */
							      /*anyblock */
							      {

								/*^cond */
								/*cond */ if ( /*ifvariadic arg#1 */ variad_MELT_DEBUG_FUN_ix >= 0 && variad_MELT_DEBUG_FUN_ix + 1 <= variad_MELT_DEBUG_FUN_len && meltxargdescr_[variad_MELT_DEBUG_FUN_ix] == MELTBPAR_BB)	/*then */
								  {
								    /*^cond.then */
								    /*^block */
								    /*anyblock */
								    {

								      /*^compute */
			/*_?*/
									meltfram__.
									loc_BASIC_BLOCK__o11
									=
									/*variadic argument stuff */
									meltxargtab_
									[variad_MELT_DEBUG_FUN_ix
									 +
									 0].
									meltbp_bb;;
								      /*^compute */

								      /*consume variadic BASIC_BLOCK ! */
									variad_MELT_DEBUG_FUN_ix
									+= 1;;
								      MELT_LOCATION
									("warmelt-debug.melt:416:/ quasiblock");


								      /*^cond */
								      /*cond */ if (
										     /*ifisa */
										     melt_is_instance_of
										     ((melt_ptr_t) (( /*!BASICBLOCK_DEBUG_FUNCONT */ meltfrout->tabval[15])),
										      (melt_ptr_t) (( /*!CLASS_REFERENCE */ meltfrout->tabval[11])))
									)	/*then */
									{
									  /*^cond.then */
									  /*^getslot */
									  {
									    melt_ptr_t
									      slot
									      =
									      NULL,
									      obj
									      =
									      NULL;
									    obj
									      =
									      (melt_ptr_t)
									      (( /*!BASICBLOCK_DEBUG_FUNCONT */ meltfrout->tabval[15])) /*=obj*/ ;
									    melt_object_get_field
									      (slot,
									       obj,
									       0,
									       "REFERENCED_VALUE");
			  /*_.BDF__V58*/
									      meltfptr
									      [27]
									      =
									      slot;
									  };
									  ;
									}
								      else
									{	/*^cond.else */

			 /*_.BDF__V58*/
									    meltfptr
									    [27]
									    =
									    NULL;;
									}
								      ;
								      MELT_LOCATION
									("warmelt-debug.melt:418:/ checksignal");
								      MELT_CHECK_SIGNAL
									();
								      ;
			/*_#IS_CLOSURE__L28*/
									meltfnum
									[16] =
									(melt_magic_discr ((melt_ptr_t) ( /*_.BDF__V58*/ meltfptr[27])) == MELTOBMAG_CLOSURE);;
								      MELT_LOCATION
									("warmelt-debug.melt:418:/ cond");
								      /*cond */ if ( /*_#IS_CLOSURE__L28*/ meltfnum[16])	/*then */
									{
									  /*^cond.then */
									  /*^block */
									  /*anyblock */
									  {

									    MELT_LOCATION
									      ("warmelt-debug.melt:419:/ checksignal");
									    MELT_CHECK_SIGNAL
									      ();
									    ;
									    /*^apply */
									    /*apply */
									    {
									      union
										meltparam_un
										argtab
										[1];
									      memset
										(&argtab,
										 0,
										 sizeof
										 (argtab));
									      /*^apply.arg */
									      argtab
										[0].
										meltbp_bb
										=
										/*_?*/
										meltfram__.
										loc_BASIC_BLOCK__o11;
									      /*_.BDF__V60*/
										meltfptr
										[25]
										=
										melt_apply
										((meltclosure_ptr_t) ( /*_.BDF__V58*/ meltfptr[27]), (melt_ptr_t) ( /*_.DBGI__V16*/ meltfptr[12]), (MELTBPARSTR_BB ""), argtab, "", (union meltparam_un *) 0);
									    }
									    ;
									    /*_.IFELSE___V59*/
									      meltfptr
									      [29]
									      =
									      /*_.BDF__V60*/
									      meltfptr
									      [25];;
									    /*epilog */

									    MELT_LOCATION
									      ("warmelt-debug.melt:418:/ clear");
				    /*clear *//*_.BDF__V60*/
									      meltfptr
									      [25]
									      =
									      0;
									  }
									  ;
									}
								      else
									{	/*^cond.else */

									  /*^block */
									  /*anyblock */
									  {

									    MELT_LOCATION
									      ("warmelt-debug.melt:420:/ checksignal");
									    MELT_CHECK_SIGNAL
									      ();
									    ;
									    /*^cond */
									    /*cond */ if ( /*_?*/ meltfram__.loc_BASIC_BLOCK__o11)	/*then */
									      {
										/*^cond.then */
										/*^block */
										/*anyblock */
										{




										  {
										    MELT_LOCATION
										      ("warmelt-debug.melt:421:/ locexp");
										    meltgc_add_out
										      ((melt_ptr_t) ( /*_.OUT__V5*/ meltfptr[4]), (" ?*basicblock*?"));
										  }
										  ;
				      /*clear *//*_.IFELSE___V59*/
										    meltfptr
										    [29]
										    =
										    0;
										  /*epilog */
										}
										;
									      }
									    else
									      {
										MELT_LOCATION
										  ("warmelt-debug.melt:420:/ cond.else");

										/*^block */
										/*anyblock */
										{




										  {
										    MELT_LOCATION
										      ("warmelt-debug.melt:422:/ locexp");
										    meltgc_add_out
										      ((melt_ptr_t) ( /*_.OUT__V5*/ meltfptr[4]), (" ?*nullbasicblock*?"));
										  }
										  ;
				      /*clear *//*_.IFELSE___V59*/
										    meltfptr
										    [29]
										    =
										    0;
										  /*epilog */
										}
										;
									      }
									    ;
									    /*epilog */
									  }
									  ;
									}
								      ;
								      /*^compute */
								      /*_.LET___V57*/
									meltfptr
									[31] =
									/*_.IFELSE___V59*/
									meltfptr
									[29];;

								      MELT_LOCATION
									("warmelt-debug.melt:416:/ clear");
				  /*clear *//*_.BDF__V58*/
									meltfptr
									[27]
									= 0;
								      /*^clear */
				  /*clear *//*_#IS_CLOSURE__L28*/
									meltfnum
									[16]
									= 0;
								      /*^clear */
				  /*clear *//*_.IFELSE___V59*/
									meltfptr
									[29]
									= 0;
								      /*epilog */

								      MELT_LOCATION
									("warmelt-debug.melt:335:/ clear");
				  /*clear *//*_?*/
									meltfram__.
									loc_BASIC_BLOCK__o11
									= 0;
								      /*^clear */
				  /*clear *//*_.LET___V57*/
									meltfptr
									[31]
									= 0;
								    }
								    ;
								  }
								else
								  {	/*^cond.else */

								    /*^block */
								    /*anyblock */
								    {

								      /*^cond */
								      /*cond */ if ( /*ifvariadic arg#1 */ variad_MELT_DEBUG_FUN_ix >= 0 && variad_MELT_DEBUG_FUN_ix + 1 <= variad_MELT_DEBUG_FUN_len && meltxargdescr_[variad_MELT_DEBUG_FUN_ix] == MELTBPAR_LOOP)	/*then */
									{
									  /*^cond.then */
									  /*^block */
									  /*anyblock */
									  {

									    /*^compute */
			  /*_?*/
									      meltfram__.
									      loc_LOOP__o12
									      =
									      /*variadic argument stuff */
									      meltxargtab_
									      [variad_MELT_DEBUG_FUN_ix
									       +
									       0].
									      meltbp_loop;;
									    /*^compute */

									    /*consume variadic LOOP ! */
									      variad_MELT_DEBUG_FUN_ix
									      +=
									      1;;
									    MELT_LOCATION
									      ("warmelt-debug.melt:424:/ quasiblock");


									    /*^cond */
									    /*cond */ if (
											   /*ifisa */
											   melt_is_instance_of
											   ((melt_ptr_t) (( /*!LOOP_DEBUG_FUNCONT */ meltfrout->tabval[16])),
											    (melt_ptr_t) (( /*!CLASS_REFERENCE */ meltfrout->tabval[11])))
									      )	/*then */
									      {
										/*^cond.then */
										/*^getslot */
										{
										  melt_ptr_t
										    slot
										    =
										    NULL,
										    obj
										    =
										    NULL;
										  obj
										    =
										    (melt_ptr_t)
										    (( /*!LOOP_DEBUG_FUNCONT */ meltfrout->tabval[16])) /*=obj*/ ;
										  melt_object_get_field
										    (slot,
										     obj,
										     0,
										     "REFERENCED_VALUE");
			    /*_.LDF__V62*/
										    meltfptr
										    [28]
										    =
										    slot;
										};
										;
									      }
									    else
									      {	/*^cond.else */

			   /*_.LDF__V62*/
										  meltfptr
										  [28]
										  =
										  NULL;;
									      }
									    ;
									    MELT_LOCATION
									      ("warmelt-debug.melt:426:/ checksignal");
									    MELT_CHECK_SIGNAL
									      ();
									    ;
			  /*_#IS_CLOSURE__L29*/
									      meltfnum
									      [14]
									      =
									      (melt_magic_discr ((melt_ptr_t) ( /*_.LDF__V62*/ meltfptr[28])) == MELTOBMAG_CLOSURE);;
									    MELT_LOCATION
									      ("warmelt-debug.melt:426:/ cond");
									    /*cond */ if ( /*_#IS_CLOSURE__L29*/ meltfnum[14])	/*then */
									      {
										/*^cond.then */
										/*^block */
										/*anyblock */
										{

										  MELT_LOCATION
										    ("warmelt-debug.melt:427:/ checksignal");
										  MELT_CHECK_SIGNAL
										    ();
										  ;
										  /*^apply */
										  /*apply */
										  {
										    union
										      meltparam_un
										      argtab
										      [1];
										    memset
										      (&argtab,
										       0,
										       sizeof
										       (argtab));
										    /*^apply.arg */
										    argtab
										      [0].
										      meltbp_loop
										      =
										      /*_?*/
										      meltfram__.
										      loc_LOOP__o12;
										    /*_.LDF__V64*/
										      meltfptr
										      [27]
										      =
										      melt_apply
										      ((meltclosure_ptr_t) ( /*_.LDF__V62*/ meltfptr[28]), (melt_ptr_t) ( /*_.DBGI__V16*/ meltfptr[12]), (MELTBPARSTR_LOOP ""), argtab, "", (union meltparam_un *) 0);
										  }
										  ;
										  /*_.IFELSE___V63*/
										    meltfptr
										    [25]
										    =
										    /*_.LDF__V64*/
										    meltfptr
										    [27];;
										  /*epilog */

										  MELT_LOCATION
										    ("warmelt-debug.melt:426:/ clear");
				      /*clear *//*_.LDF__V64*/
										    meltfptr
										    [27]
										    =
										    0;
										}
										;
									      }
									    else
									      {	/*^cond.else */

										/*^block */
										/*anyblock */
										{

										  MELT_LOCATION
										    ("warmelt-debug.melt:428:/ checksignal");
										  MELT_CHECK_SIGNAL
										    ();
										  ;
										  /*^cond */
										  /*cond */ if ( /*_?*/ meltfram__.loc_LOOP__o12)	/*then */
										    {
										      /*^cond.then */
										      /*^block */
										      /*anyblock */
										      {




											{
											  MELT_LOCATION
											    ("warmelt-debug.melt:429:/ locexp");
											  meltgc_add_out
											    ((melt_ptr_t) ( /*_.OUT__V5*/ meltfptr[4]), (" ?*loop*?"));
											}
											;
					/*clear *//*_.IFELSE___V63*/
											  meltfptr
											  [25]
											  =
											  0;
											/*epilog */
										      }
										      ;
										    }
										  else
										    {
										      MELT_LOCATION
											("warmelt-debug.melt:428:/ cond.else");

										      /*^block */
										      /*anyblock */
										      {




											{
											  MELT_LOCATION
											    ("warmelt-debug.melt:430:/ locexp");
											  meltgc_add_out
											    ((melt_ptr_t) ( /*_.OUT__V5*/ meltfptr[4]), (" ?*loop*?"));
											}
											;
					/*clear *//*_.IFELSE___V63*/
											  meltfptr
											  [25]
											  =
											  0;
											/*epilog */
										      }
										      ;
										    }
										  ;
										  /*epilog */
										}
										;
									      }
									    ;
									    /*^compute */
									    /*_.LET___V61*/
									      meltfptr
									      [26]
									      =
									      /*_.IFELSE___V63*/
									      meltfptr
									      [25];;

									    MELT_LOCATION
									      ("warmelt-debug.melt:424:/ clear");
				    /*clear *//*_.LDF__V62*/
									      meltfptr
									      [28]
									      =
									      0;
									    /*^clear */
				    /*clear *//*_#IS_CLOSURE__L29*/
									      meltfnum
									      [14]
									      =
									      0;
									    /*^clear */
				    /*clear *//*_.IFELSE___V63*/
									      meltfptr
									      [25]
									      =
									      0;
									    /*epilog */

									    MELT_LOCATION
									      ("warmelt-debug.melt:335:/ clear");
				    /*clear *//*_?*/
									      meltfram__.
									      loc_LOOP__o12
									      =
									      0;
									    /*^clear */
				    /*clear *//*_.LET___V61*/
									      meltfptr
									      [26]
									      =
									      0;
									  }
									  ;
									}
								      else
									{	/*^cond.else */

									  /*^block */
									  /*anyblock */
									  {

									    MELT_LOCATION
									      ("warmelt-debug.melt:432:/ quasiblock");


			  /*_.VCTYP__V66*/
									      meltfptr
									      [31]
									      =
									      /*variadic_type_code */
#ifdef melt_variadic_index
									      (((melt_variadic_index + 0) >= 0 && (melt_variadic_index + 0) < melt_variadic_length) ? melt_code_to_ctype (meltxargdescr_[melt_variadic_index + 0] & MELT_ARGDESCR_MAX) : NULL)
#else
									      NULL	/* no variadic_ctype outside of variadic functions */
#endif /*melt_variadic_index */
									      ;;
									    MELT_LOCATION
									      ("warmelt-debug.melt:433:/ cond");
									    /*cond */ if (
											   /*ifisa */
											   melt_is_instance_of
											   ((melt_ptr_t) ( /*_.VCTYP__V66*/ meltfptr[31]),
											    (melt_ptr_t) (( /*!CLASS_NAMED */ meltfrout->tabval[17])))
									      )	/*then */
									      {
										/*^cond.then */
										/*^getslot */
										{
										  melt_ptr_t
										    slot
										    =
										    NULL,
										    obj
										    =
										    NULL;
										  obj
										    =
										    (melt_ptr_t)
										    ( /*_.VCTYP__V66*/ meltfptr[31]) /*=obj*/ ;
										  melt_object_get_field
										    (slot,
										     obj,
										     1,
										     "NAMED_NAME");
			    /*_.VCTYPNAME__V67*/
										    meltfptr
										    [27]
										    =
										    slot;
										};
										;
									      }
									    else
									      {	/*^cond.else */

			   /*_.VCTYPNAME__V67*/
										  meltfptr
										  [27]
										  =
										  NULL;;
									      }
									    ;

									    {
									      MELT_LOCATION
										("warmelt-debug.melt:435:/ locexp");
									      /* WARNBADCTYPE__1 */
									      warning
										(0,
										 "MELT invalid ctype %s in (DEBUG ...) file %s line %d",
										 melt_string_str
										 ((melt_ptr_t) /*_.VCTYPNAME__V67*/ meltfptr[27]),
										 /*_?*/
										 meltfram__.
										 loc_CSTRING__o0,
										 (int)
										 /*_#LINENO__L2*/
										 meltfnum
										 [1]);
									      ;
									    }
									    ;
									    MELT_LOCATION
									      ("warmelt-debug.melt:441:/ cond");
									    /*cond */ if (
											   /*ifisa */
											   melt_is_instance_of
											   ((melt_ptr_t) ( /*_.VCTYP__V66*/ meltfptr[31]),
											    (melt_ptr_t) (( /*!CLASS_CTYPE */ meltfrout->tabval[18])))
									      )	/*then */
									      {
										/*^cond.then */
										/*^getslot */
										{
										  melt_ptr_t
										    slot
										    =
										    NULL,
										    obj
										    =
										    NULL;
										  obj
										    =
										    (melt_ptr_t)
										    ( /*_.VCTYP__V66*/ meltfptr[31]) /*=obj*/ ;
										  melt_object_get_field
										    (slot,
										     obj,
										     2,
										     "CTYPE_KEYWORD");
			    /*_.CTYPE_KEYWORD__V68*/
										    meltfptr
										    [28]
										    =
										    slot;
										};
										;
									      }
									    else
									      {	/*^cond.else */

			   /*_.CTYPE_KEYWORD__V68*/
										  meltfptr
										  [28]
										  =
										  NULL;;
									      }
									    ;
									    MELT_LOCATION
									      ("warmelt-debug.melt:441:/ checksignal");
									    MELT_CHECK_SIGNAL
									      ();
									    ;
									    /*^apply */
									    /*apply */
									    {
									      union
										meltparam_un
										argtab
										[3];
									      memset
										(&argtab,
										 0,
										 sizeof
										 (argtab));
									      /*^apply.arg */
									      argtab
										[0].
										meltbp_cstring
										=
										"??:";
									      /*^apply.arg */
									      argtab
										[1].
										meltbp_aptr
										=
										(melt_ptr_t
										 *)
										&
										/*_.CTYPE_KEYWORD__V68*/
										meltfptr
										[28];
									      /*^apply.arg */
									      argtab
										[2].
										meltbp_cstring
										=
										"?? ";
									      /*_.ADD2OUT__V69*/
										meltfptr
										[25]
										=
										melt_apply
										((meltclosure_ptr_t) (( /*!ADD2OUT */ meltfrout->tabval[6])), (melt_ptr_t) ( /*_.OUT__V5*/ meltfptr[4]), (MELTBPARSTR_CSTRING MELTBPARSTR_PTR MELTBPARSTR_CSTRING ""), argtab, "", (union meltparam_un *) 0);
									    }
									    ;
									    /*_.LET___V65*/
									      meltfptr
									      [29]
									      =
									      /*_.ADD2OUT__V69*/
									      meltfptr
									      [25];;

									    MELT_LOCATION
									      ("warmelt-debug.melt:432:/ clear");
				    /*clear *//*_.VCTYP__V66*/
									      meltfptr
									      [31]
									      =
									      0;
									    /*^clear */
				    /*clear *//*_.VCTYPNAME__V67*/
									      meltfptr
									      [27]
									      =
									      0;
									    /*^clear */
				    /*clear *//*_.CTYPE_KEYWORD__V68*/
									      meltfptr
									      [28]
									      =
									      0;
									    /*^clear */
				    /*clear *//*_.ADD2OUT__V69*/
									      meltfptr
									      [25]
									      =
									      0;
									    /*epilog */

									    MELT_LOCATION
									      ("warmelt-debug.melt:335:/ clear");
				    /*clear *//*_.LET___V65*/
									      meltfptr
									      [29]
									      =
									      0;
									  }
									  ;
									}
								      ;
								      /*epilog */
								    }
								    ;
								  }
								;
								/*epilog */
							      }
							      ;
							    }
							  ;
							  /*epilog */
							}
							;
						      }
						    ;
						    /*epilog */
						  }
						  ;
						}
					      ;
					      /*epilog */
					    }
					    ;
					  }
					;
					/*epilog */
				      }
				      ;
				    }
				  ;
				  /*epilog */
				}
				;
			      }
			    ;
			    /*epilog */
			  }
			  ;
			}
		      ;
		      /*epilog */
		    }
		    ;
		  }
		;
      /*_#OUTPUT_LENGTH__L30*/ meltfnum[17] =
		  melt_output_length ((melt_ptr_t) /*_.OUT__V5*/
				      meltfptr[4]);;
		MELT_LOCATION ("warmelt-debug.melt:443:/ compute");
		/*_#OUTLEN__L10*/ meltfnum[9] =
		  /*_#SETQ___L31*/ meltfnum[16] =
		  /*_#OUTPUT_LENGTH__L30*/ meltfnum[17];;
		MELT_LOCATION ("warmelt-debug.melt:333:/ checksignal");
		MELT_CHECK_SIGNAL ();
		;
		/*epilog */

		/*^clear */
		/*clear *//*_#OUTPUT_LENGTH__L30*/ meltfnum[17] = 0;
		/*^clear */
		/*clear *//*_#SETQ___L31*/ meltfnum[16] = 0;
	      }
	      ;
	      ;
	      goto labloop_ARGLOOP_1;
	    labexit_ARGLOOP_1:;/*^loopepilog */
	      /*loopepilog */
	      /*_.FOREVER___V24*/ meltfptr[21] =
		/*_.ARGLOOP__V25*/ meltfptr[22];;
	    }
	    ;
	    MELT_LOCATION ("warmelt-debug.melt:446:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
    /*_#I__L32*/ meltfnum[14] =
	      (( /*_#INITOUTLEN__L12*/ meltfnum[11]) + (200));;
	    /*^compute */
    /*_#I__L33*/ meltfnum[17] =
	      (( /*_#OUTLEN__L10*/ meltfnum[9]) >
	       ( /*_#I__L32*/ meltfnum[14]));;
	    MELT_LOCATION ("warmelt-debug.melt:446:/ cond");
	    /*cond */ if ( /*_#I__L33*/ meltfnum[17])	/*then */
	      {
		/*^cond.then */
		/*^block */
		/*anyblock */
		{


		  {
		    MELT_LOCATION ("warmelt-debug.melt:448:/ locexp");
		    meltgc_add_out ((melt_ptr_t) ( /*_.OUT__V5*/ meltfptr[4]),
				    (" .##."));
		  }
		  ;

		  {
		    MELT_LOCATION ("warmelt-debug.melt:449:/ locexp");
		    meltgc_out_add_indent ((melt_ptr_t)
					   ( /*_.OUT__V5*/ meltfptr[4]), (0),
					   0);;
		  }
		  ;
		  MELT_LOCATION ("warmelt-debug.melt:447:/ quasiblock");


		  /*epilog */
		}
		;
	      }			/*noelse */
	    ;

	    {
	      MELT_LOCATION ("warmelt-debug.melt:450:/ locexp");
	      meltgc_out_add_indent ((melt_ptr_t)
				     ( /*_.OUT__V5*/ meltfptr[4]), (0), 0);;
	    }
	    ;

	    MELT_LOCATION ("warmelt-debug.melt:317:/ clear");
	      /*clear *//*_.DBGI__V16*/ meltfptr[12] = 0;
	    /*^clear */
	      /*clear *//*_#FRAMDEPTH__L9*/ meltfnum[7] = 0;
	    /*^clear */
	      /*clear *//*_#OUTLEN__L10*/ meltfnum[9] = 0;
	    /*^clear */
	      /*clear *//*_#LASTSTROUTLEN__L11*/ meltfnum[10] = 0;
	    /*^clear */
	      /*clear *//*_#INITOUTLEN__L12*/ meltfnum[11] = 0;
	    /*^clear */
	      /*clear *//*_#I__L13*/ meltfnum[12] = 0;
	    /*^clear */
	      /*clear *//*_.ADD2OUT__V18*/ meltfptr[17] = 0;
	    /*^clear */
	      /*clear *//*_.IF___V19*/ meltfptr[18] = 0;
	    /*^clear */
	      /*clear *//*_#I__L14*/ meltfnum[13] = 0;
	    /*^clear */
	      /*clear *//*_.IF___V21*/ meltfptr[19] = 0;
	    /*^clear */
	      /*clear *//*_.FOREVER___V24*/ meltfptr[21] = 0;
	    /*^clear */
	      /*clear *//*_#I__L32*/ meltfnum[14] = 0;
	    /*^clear */
	      /*clear *//*_#I__L33*/ meltfnum[17] = 0;
	    /* block_signals meltcit1__BLKSIGNAL end */
	    melt_blocklevel_signals = meltcit1__BLKSIGNAL_lev;
	    MELT_CHECK_SIGNAL ();


	    /*citerepilog */
	  }			/*endciterblock BLOCK_SIGNALS */
	  ;

	  MELT_LOCATION ("warmelt-debug.melt:282:/ clear");
	     /*clear *//*_#DBGCOUNTER__L4*/ meltfnum[3] = 0;
	  /*^clear */
	     /*clear *//*_.DUMPF__V3*/ meltfptr[2] = 0;
	  /*^clear */
	     /*clear *//*_.STDERRF__V4*/ meltfptr[3] = 0;
	  /*^clear */
	     /*clear *//*_.OUT__V5*/ meltfptr[4] = 0;
	  /*^clear */
	     /*clear *//*_.OCCMAP__V6*/ meltfptr[5] = 0;
	  /*^clear */
	     /*clear *//*_#DEBUG_DEPTH__L5*/ meltfnum[4] = 0;
	  /*^clear */
	     /*clear *//*_.BOXEDMAXDEPTH__V7*/ meltfptr[6] = 0;
	  /*^clear */
	     /*clear *//*_#NULL__L6*/ meltfnum[5] = 0;
	  /*^clear */
	     /*clear *//*_.IF___V8*/ meltfptr[7] = 0;
	  /*^clear */
	     /*clear *//*_#NULL__L7*/ meltfnum[6] = 0;
	  /*^clear */
	     /*clear *//*_.IF___V11*/ meltfptr[8] = 0;
	  /*^clear */
	     /*clear *//*_.IFCPP___V14*/ meltfptr[9] = 0;
	  /*epilog */
	}
	;
      }				/*noelse */
    ;
    /*epilog */

    MELT_LOCATION ("warmelt-debug.melt:280:/ clear");
	   /*clear *//*_#MELT_HAS_FLAG_DEBUG_SET__L3*/ meltfnum[2] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("MELT_DEBUG_FUN", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_8_warmelt_debug_MELT_DEBUG_FUN_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*noretval */ NULL);
#undef melt_variadic_length
#undef melt_variadic_index

#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_8_warmelt_debug_MELT_DEBUG_FUN */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_9_warmelt_debug_DBG_OUTOBJECT (meltclosure_ptr_t meltclosp_,
					melt_ptr_t meltfirstargp_,
					const melt_argdescr_cell_t
					meltxargdescr_[],
					union meltparam_un * meltxargtab_,
					const melt_argdescr_cell_t
					meltxresdescr_[],
					union meltparam_un * meltxrestab_)
{
  long current_blocklevel_signals_meltrout_9_warmelt_debug_DBG_OUTOBJECT_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_9_warmelt_debug_DBG_OUTOBJECT_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 15
    melt_ptr_t mcfr_varptr[15];
#define MELTFRAM_NBVARNUM 6
    long mcfr_varnum[6];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_9_warmelt_debug_DBG_OUTOBJECT is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_9_warmelt_debug_DBG_OUTOBJECT_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 15; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_9_warmelt_debug_DBG_OUTOBJECT nbval 15*/
  meltfram__.mcfr_nbvar = 15 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("DBG_OUTOBJECT", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-debug.melt:454:/ getarg");
 /*_.OBJ__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.DBGI__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.DBGI__V3*/ meltfptr[2])) !=
	      NULL);


  /*getarg#2 */
  /*^getarg */
  if (meltxargdescr_[1] != MELTBPAR_LONG)
    goto lab_endgetargs;
 /*_#DEPTH__L1*/ meltfnum[0] = meltxargtab_[1].meltbp_long;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-debug.melt:457:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L2*/ meltfnum[1] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.DBGI__V3*/ meltfptr[2]),
			     (melt_ptr_t) (( /*!CLASS_DEBUG_INFORMATION */
					    meltfrout->tabval[0])));;
      MELT_LOCATION ("warmelt-debug.melt:457:/ cond");
      /*cond */ if ( /*_#IS_A__L2*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V5*/ meltfptr[4] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-debug.melt:457:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check dbgi"),
				  ("warmelt-debug.melt")
				  ? ("warmelt-debug.melt") : __FILE__,
				  (457) ? (457) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V5*/ meltfptr[4] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V4*/ meltfptr[3] = /*_.IFELSE___V5*/ meltfptr[4];;
      /*epilog */

      MELT_LOCATION ("warmelt-debug.melt:457:/ clear");
	     /*clear *//*_#IS_A__L2*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V5*/ meltfptr[4] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V4*/ meltfptr[3] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-debug.melt:458:/ quasiblock");


    /*^getslot */
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.DBGI__V3*/ meltfptr[2]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 1, "DBGI_OCCMAP");
  /*_.OCCMAP__V7*/ meltfptr[6] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-debug.melt:459:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#IS_MAPOBJECT__L3*/ meltfnum[1] =
      /*is_mapobject: */
      (melt_magic_discr ((melt_ptr_t) ( /*_.OCCMAP__V7*/ meltfptr[6])) ==
       MELTOBMAG_MAPOBJECTS);;
    MELT_LOCATION ("warmelt-debug.melt:459:/ cond");
    /*cond */ if ( /*_#IS_MAPOBJECT__L3*/ meltfnum[1])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-debug.melt:460:/ quasiblock");


   /*_.OCC__V10*/ meltfptr[9] =
	    /*mapobject_get */
	    melt_get_mapobjects ((meltmapobjects_ptr_t)
				 ( /*_.OCCMAP__V7*/ meltfptr[6]),
				 (meltobject_ptr_t) ( /*_.OBJ__V2*/
						     meltfptr[1]));;
	  MELT_LOCATION ("warmelt-debug.melt:462:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
   /*_#IS_INTEGERBOX__L4*/ meltfnum[3] =
	    (melt_magic_discr ((melt_ptr_t) ( /*_.OCC__V10*/ meltfptr[9])) ==
	     MELTOBMAG_INT);;
	  MELT_LOCATION ("warmelt-debug.melt:462:/ cond");
	  /*cond */ if ( /*_#IS_INTEGERBOX__L4*/ meltfnum[3])	/*then */
	    {
	      /*^cond.then */
	      /*^block */
	      /*anyblock */
	      {

		MELT_LOCATION ("warmelt-debug.melt:464:/ checksignal");
		MELT_CHECK_SIGNAL ();
		;
		/*^msend */
		/*msend */
		{
		  union meltparam_un argtab[2];
		  memset (&argtab, 0, sizeof (argtab));
		  /*^ojbmsend.arg */
		  argtab[0].meltbp_aptr =
		    (melt_ptr_t *) & /*_.DBGI__V3*/ meltfptr[2];
		  /*^ojbmsend.arg */
		  argtab[1].meltbp_long = /*_#DEPTH__L1*/ meltfnum[0];
		  /*_.DBG_OUTPUTAGAIN__V12*/ meltfptr[11] =
		    meltgc_send ((melt_ptr_t) ( /*_.OBJ__V2*/ meltfptr[1]),
				 (melt_ptr_t) (( /*!DBG_OUTPUTAGAIN */
						meltfrout->tabval[1])),
				 (MELTBPARSTR_PTR MELTBPARSTR_LONG ""),
				 argtab, "", (union meltparam_un *) 0);
		}
		;
     /*_#GET_INT__L5*/ meltfnum[4] =
		  (melt_get_int
		   ((melt_ptr_t) ( /*_.OCC__V10*/ meltfptr[9])));;
		/*^compute */
     /*_#I__L6*/ meltfnum[5] =
		  (( /*_#GET_INT__L5*/ meltfnum[4]) + (1));;

		{
		  MELT_LOCATION ("warmelt-debug.melt:465:/ locexp");
		  melt_put_int ((melt_ptr_t) ( /*_.OCC__V10*/ meltfptr[9]),
				( /*_#I__L6*/ meltfnum[5]));
		}
		;
		MELT_LOCATION ("warmelt-debug.melt:463:/ quasiblock");


		/*epilog */

		MELT_LOCATION ("warmelt-debug.melt:462:/ clear");
	       /*clear *//*_.DBG_OUTPUTAGAIN__V12*/ meltfptr[11] = 0;
		/*^clear */
	       /*clear *//*_#GET_INT__L5*/ meltfnum[4] = 0;
		/*^clear */
	       /*clear *//*_#I__L6*/ meltfnum[5] = 0;
	      }
	      ;
	    }
	  else
	    {			/*^cond.else */

	      /*^block */
	      /*anyblock */
	      {

		MELT_LOCATION ("warmelt-debug.melt:467:/ quasiblock");


     /*_.NEWOCC__V14*/ meltfptr[13] =
		  (meltgc_new_int
		   ((meltobject_ptr_t)
		    (( /*!DISCR_INTEGER */ meltfrout->tabval[2])), (1)));;

		{
		  MELT_LOCATION ("warmelt-debug.melt:468:/ locexp");
		  meltgc_put_mapobjects ((meltmapobjects_ptr_t)
					 ( /*_.OCCMAP__V7*/ meltfptr[6]),
					 (meltobject_ptr_t) ( /*_.OBJ__V2*/
							     meltfptr[1]),
					 (melt_ptr_t) ( /*_.NEWOCC__V14*/
						       meltfptr[13]));
		}
		;
		MELT_LOCATION ("warmelt-debug.melt:470:/ checksignal");
		MELT_CHECK_SIGNAL ();
		;
		/*^msend */
		/*msend */
		{
		  union meltparam_un argtab[2];
		  memset (&argtab, 0, sizeof (argtab));
		  /*^ojbmsend.arg */
		  argtab[0].meltbp_aptr =
		    (melt_ptr_t *) & /*_.DBGI__V3*/ meltfptr[2];
		  /*^ojbmsend.arg */
		  argtab[1].meltbp_long = /*_#DEPTH__L1*/ meltfnum[0];
		  /*_.DBG_OUTPUT__V15*/ meltfptr[14] =
		    meltgc_send ((melt_ptr_t) ( /*_.OBJ__V2*/ meltfptr[1]),
				 (melt_ptr_t) (( /*!DBG_OUTPUT */ meltfrout->
						tabval[3])),
				 (MELTBPARSTR_PTR MELTBPARSTR_LONG ""),
				 argtab, "", (union meltparam_un *) 0);
		}
		;
		/*_.LET___V13*/ meltfptr[11] =
		  /*_.DBG_OUTPUT__V15*/ meltfptr[14];;

		MELT_LOCATION ("warmelt-debug.melt:467:/ clear");
	       /*clear *//*_.NEWOCC__V14*/ meltfptr[13] = 0;
		/*^clear */
	       /*clear *//*_.DBG_OUTPUT__V15*/ meltfptr[14] = 0;
		/*_.IFELSE___V11*/ meltfptr[10] =
		  /*_.LET___V13*/ meltfptr[11];;
		/*epilog */

		MELT_LOCATION ("warmelt-debug.melt:462:/ clear");
	       /*clear *//*_.LET___V13*/ meltfptr[11] = 0;
	      }
	      ;
	    }
	  ;
	  /*_.LET___V9*/ meltfptr[8] = /*_.IFELSE___V11*/ meltfptr[10];;

	  MELT_LOCATION ("warmelt-debug.melt:460:/ clear");
	     /*clear *//*_.OCC__V10*/ meltfptr[9] = 0;
	  /*^clear */
	     /*clear *//*_#IS_INTEGERBOX__L4*/ meltfnum[3] = 0;
	  /*^clear */
	     /*clear *//*_.IFELSE___V11*/ meltfptr[10] = 0;
	  /*_.IF___V8*/ meltfptr[7] = /*_.LET___V9*/ meltfptr[8];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-debug.melt:459:/ clear");
	     /*clear *//*_.LET___V9*/ meltfptr[8] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

  /*_.IF___V8*/ meltfptr[7] = NULL;;
      }
    ;
    /*^compute */
    /*_.LET___V6*/ meltfptr[4] = /*_.IF___V8*/ meltfptr[7];;

    MELT_LOCATION ("warmelt-debug.melt:458:/ clear");
	   /*clear *//*_.OCCMAP__V7*/ meltfptr[6] = 0;
    /*^clear */
	   /*clear *//*_#IS_MAPOBJECT__L3*/ meltfnum[1] = 0;
    /*^clear */
	   /*clear *//*_.IF___V8*/ meltfptr[7] = 0;
    MELT_LOCATION ("warmelt-debug.melt:454:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.LET___V6*/ meltfptr[4];;

    {
      MELT_LOCATION ("warmelt-debug.melt:454:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V4*/ meltfptr[3] = 0;
    /*^clear */
	   /*clear *//*_.LET___V6*/ meltfptr[4] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("DBG_OUTOBJECT", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_9_warmelt_debug_DBG_OUTOBJECT_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_9_warmelt_debug_DBG_OUTOBJECT */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_10_warmelt_debug_DBG_OUT (meltclosure_ptr_t meltclosp_,
				   melt_ptr_t meltfirstargp_,
				   const melt_argdescr_cell_t
				   meltxargdescr_[],
				   union meltparam_un * meltxargtab_,
				   const melt_argdescr_cell_t
				   meltxresdescr_[],
				   union meltparam_un * meltxrestab_)
{
  long current_blocklevel_signals_meltrout_10_warmelt_debug_DBG_OUT_melt =
    melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_10_warmelt_debug_DBG_OUT_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 17
    melt_ptr_t mcfr_varptr[17];
#define MELTFRAM_NBVARNUM 10
    long mcfr_varnum[10];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_10_warmelt_debug_DBG_OUT is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_10_warmelt_debug_DBG_OUT_st *) meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 17; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_10_warmelt_debug_DBG_OUT nbval 17*/
  meltfram__.mcfr_nbvar = 17 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("DBG_OUT", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-debug.melt:474:/ getarg");
 /*_.OBJ__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.DBGI__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.DBGI__V3*/ meltfptr[2])) !=
	      NULL);


  /*getarg#2 */
  /*^getarg */
  if (meltxargdescr_[1] != MELTBPAR_LONG)
    goto lab_endgetargs;
 /*_#DEPTH__L1*/ meltfnum[0] = meltxargtab_[1].meltbp_long;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-debug.melt:477:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L2*/ meltfnum[1] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.DBGI__V3*/ meltfptr[2]),
			     (melt_ptr_t) (( /*!CLASS_DEBUG_INFORMATION */
					    meltfrout->tabval[0])));;
      MELT_LOCATION ("warmelt-debug.melt:477:/ cond");
      /*cond */ if ( /*_#IS_A__L2*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V5*/ meltfptr[4] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-debug.melt:477:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check dbgi"),
				  ("warmelt-debug.melt")
				  ? ("warmelt-debug.melt") : __FILE__,
				  (477) ? (477) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V5*/ meltfptr[4] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V4*/ meltfptr[3] = /*_.IFELSE___V5*/ meltfptr[4];;
      /*epilog */

      MELT_LOCATION ("warmelt-debug.melt:477:/ clear");
	     /*clear *//*_#IS_A__L2*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V5*/ meltfptr[4] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V4*/ meltfptr[3] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

    {
      MELT_LOCATION ("warmelt-debug.melt:478:/ locexp");
      melt_check_call_frames (MELT_ANYWHERE, "start dbg_out");;
    }
    ;
    MELT_LOCATION ("warmelt-debug.melt:479:/ quasiblock");


    /*^getslot */
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.DBGI__V3*/ meltfptr[2]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 0, "DBGI_OUT");
  /*_.OUT__V7*/ meltfptr[6] = slot;
    };
    ;
 /*_.DISCR__V8*/ meltfptr[7] =
      ((melt_ptr_t)
       (melt_discr ((melt_ptr_t) ( /*_.OBJ__V2*/ meltfptr[1]))));;
    MELT_LOCATION ("warmelt-debug.melt:481:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.DBGI__V3*/ meltfptr[2]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 2, "DBGI_MAXDEPTH");
  /*_.DBGI_MAXDEPTH__V9*/ meltfptr[8] = slot;
    };
    ;
 /*_#MAXDEPTH__L3*/ meltfnum[1] =
      (melt_get_int ((melt_ptr_t) ( /*_.DBGI_MAXDEPTH__V9*/ meltfptr[8])));;
    MELT_LOCATION ("warmelt-debug.melt:483:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#NULL__L4*/ meltfnum[3] =
      (( /*_.OUT__V7*/ meltfptr[6]) == NULL);;
    MELT_LOCATION ("warmelt-debug.melt:483:/ cond");
    /*cond */ if ( /*_#NULL__L4*/ meltfnum[3])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-debug.melt:484:/ quasiblock");


   /*_.RETVAL___V1*/ meltfptr[0] = NULL;;

	  {
	    MELT_LOCATION ("warmelt-debug.melt:484:/ locexp");
	    /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
	    if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	      melt_warn_for_no_expected_secondary_results ();
	    /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
	    ;
	  }
	  ;
	  /*^finalreturn */
	  ;
	  /*finalret */ goto labend_rout;
	  /*_.IF___V10*/ meltfptr[9] = /*_.RETURN___V11*/ meltfptr[10];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-debug.melt:483:/ clear");
	     /*clear *//*_.RETURN___V11*/ meltfptr[10] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

  /*_.IF___V10*/ meltfptr[9] = NULL;;
      }
    ;
    MELT_LOCATION ("warmelt-debug.melt:485:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#MELT_REALLY_NEED_DBGLIM__L5*/ meltfnum[4] =
      ( /*MELT_REALLY_NEED_DBGLIM */
       melt_need_debug_limit ((int) /*_#DEPTH__L1*/ meltfnum[0],
			      (int) /*_#MAXDEPTH__L3*/ meltfnum[1]));;
    MELT_LOCATION ("warmelt-debug.melt:485:/ cond");
    /*cond */ if ( /*_#MELT_REALLY_NEED_DBGLIM__L5*/ meltfnum[4])	/*then */
      {
	/*^cond.then */
	/*_#OR___L6*/ meltfnum[5] =
	  /*_#MELT_REALLY_NEED_DBGLIM__L5*/ meltfnum[4];;
      }
    else
      {
	MELT_LOCATION ("warmelt-debug.melt:485:/ cond.else");

	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-debug.melt:486:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
   /*_#I__L7*/ meltfnum[6] =
	    (( /*_#DEPTH__L1*/ meltfnum[0]) <
	     ( /*_#MAXDEPTH__L3*/ meltfnum[1]));;
	  MELT_LOCATION ("warmelt-debug.melt:486:/ cond");
	  /*cond */ if ( /*_#I__L7*/ meltfnum[6])	/*then */
	    {
	      /*^cond.then */
	      /*^block */
	      /*anyblock */
	      {

     /*_#IS_A__L9*/ meltfnum[8] =
		  melt_is_instance_of ((melt_ptr_t)
				       ( /*_.DBGI__V3*/ meltfptr[2]),
				       (melt_ptr_t) (( /*!CLASS_DEBUG_OUTPUT_INFORMATION */ meltfrout->tabval[1])));;
		/*^compute */
		/*_#IF___L8*/ meltfnum[7] = /*_#IS_A__L9*/ meltfnum[8];;
		/*epilog */

		MELT_LOCATION ("warmelt-debug.melt:486:/ clear");
	       /*clear *//*_#IS_A__L9*/ meltfnum[8] = 0;
	      }
	      ;
	    }
	  else
	    {			/*^cond.else */

    /*_#IF___L8*/ meltfnum[7] = 0;;
	    }
	  ;
	  /*^compute */
	  /*_#OR___L6*/ meltfnum[5] = /*_#IF___L8*/ meltfnum[7];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-debug.melt:485:/ clear");
	     /*clear *//*_#I__L7*/ meltfnum[6] = 0;
	  /*^clear */
	     /*clear *//*_#IF___L8*/ meltfnum[7] = 0;
	}
	;
      }
    ;
    /*^cond */
    /*cond */ if ( /*_#OR___L6*/ meltfnum[5])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-debug.melt:489:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
   /*_#IS_OBJECT__L10*/ meltfnum[8] =
	    (melt_magic_discr ((melt_ptr_t) ( /*_.OBJ__V2*/ meltfptr[1])) ==
	     MELTOBMAG_OBJECT);;
	  MELT_LOCATION ("warmelt-debug.melt:489:/ cond");
	  /*cond */ if ( /*_#IS_OBJECT__L10*/ meltfnum[8])	/*then */
	    {
	      /*^cond.then */
	      /*^block */
	      /*anyblock */
	      {

		MELT_LOCATION ("warmelt-debug.melt:490:/ checksignal");
		MELT_CHECK_SIGNAL ();
		;
		/*^apply */
		/*apply */
		{
		  union meltparam_un argtab[2];
		  memset (&argtab, 0, sizeof (argtab));
		  /*^apply.arg */
		  argtab[0].meltbp_aptr =
		    (melt_ptr_t *) & /*_.DBGI__V3*/ meltfptr[2];
		  /*^apply.arg */
		  argtab[1].meltbp_long = /*_#DEPTH__L1*/ meltfnum[0];
		  /*_.DBG_OUTOBJECT__V14*/ meltfptr[13] =
		    melt_apply ((meltclosure_ptr_t)
				(( /*!DBG_OUTOBJECT */ meltfrout->tabval[2])),
				(melt_ptr_t) ( /*_.OBJ__V2*/ meltfptr[1]),
				(MELTBPARSTR_PTR MELTBPARSTR_LONG ""), argtab,
				"", (union meltparam_un *) 0);
		}
		;
		/*_.IFELSE___V13*/ meltfptr[12] =
		  /*_.DBG_OUTOBJECT__V14*/ meltfptr[13];;
		/*epilog */

		MELT_LOCATION ("warmelt-debug.melt:489:/ clear");
	       /*clear *//*_.DBG_OUTOBJECT__V14*/ meltfptr[13] = 0;
	      }
	      ;
	    }
	  else
	    {			/*^cond.else */

	      /*^block */
	      /*anyblock */
	      {

		MELT_LOCATION ("warmelt-debug.melt:491:/ checksignal");
		MELT_CHECK_SIGNAL ();
		;
		/*^cond */
		/*cond */ if ( /*_.OBJ__V2*/ meltfptr[1])	/*then */
		  {
		    /*^cond.then */
		    /*^block */
		    /*anyblock */
		    {

		      MELT_LOCATION ("warmelt-debug.melt:492:/ checksignal");
		      MELT_CHECK_SIGNAL ();
		      ;
		      /*^msend */
		      /*msend */
		      {
			union meltparam_un argtab[2];
			memset (&argtab, 0, sizeof (argtab));
			/*^ojbmsend.arg */
			argtab[0].meltbp_aptr =
			  (melt_ptr_t *) & /*_.DBGI__V3*/ meltfptr[2];
			/*^ojbmsend.arg */
			argtab[1].meltbp_long = /*_#DEPTH__L1*/ meltfnum[0];
			/*_.DBG_OUTPUT__V16*/ meltfptr[15] =
			  meltgc_send ((melt_ptr_t)
				       ( /*_.OBJ__V2*/ meltfptr[1]),
				       (melt_ptr_t) (( /*!DBG_OUTPUT */
						      meltfrout->tabval[3])),
				       (MELTBPARSTR_PTR MELTBPARSTR_LONG ""),
				       argtab, "", (union meltparam_un *) 0);
		      }
		      ;
		      /*_.IFELSE___V15*/ meltfptr[13] =
			/*_.DBG_OUTPUT__V16*/ meltfptr[15];;
		      /*epilog */

		      MELT_LOCATION ("warmelt-debug.melt:491:/ clear");
		 /*clear *//*_.DBG_OUTPUT__V16*/ meltfptr[15] = 0;
		    }
		    ;
		  }
		else
		  {		/*^cond.else */

		    /*^block */
		    /*anyblock */
		    {


		      {
			MELT_LOCATION ("warmelt-debug.melt:494:/ locexp");
			meltgc_add_out ((melt_ptr_t)
					( /*_.OUT__V7*/ meltfptr[6]), ("()"));
		      }
		      ;
		      MELT_LOCATION ("warmelt-debug.melt:493:/ quasiblock");


		      /*epilog */
		    }
		    ;
		  }
		;
		/*_.IFELSE___V13*/ meltfptr[12] =
		  /*_.IFELSE___V15*/ meltfptr[13];;
		/*epilog */

		MELT_LOCATION ("warmelt-debug.melt:489:/ clear");
	       /*clear *//*_.IFELSE___V15*/ meltfptr[13] = 0;
	      }
	      ;
	    }
	  ;
	  MELT_LOCATION ("warmelt-debug.melt:487:/ quasiblock");


	  /*_.PROGN___V17*/ meltfptr[15] = /*_.IFELSE___V13*/ meltfptr[12];;
	  /*^compute */
	  /*_.IFELSE___V12*/ meltfptr[10] = /*_.PROGN___V17*/ meltfptr[15];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-debug.melt:485:/ clear");
	     /*clear *//*_#IS_OBJECT__L10*/ meltfnum[8] = 0;
	  /*^clear */
	     /*clear *//*_.IFELSE___V13*/ meltfptr[12] = 0;
	  /*^clear */
	     /*clear *//*_.PROGN___V17*/ meltfptr[15] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

	/*^block */
	/*anyblock */
	{


	  {
	    MELT_LOCATION ("warmelt-debug.melt:498:/ locexp");
	    meltgc_add_out ((melt_ptr_t) ( /*_.OUT__V7*/ meltfptr[6]),
			    (".?."));
	  }
	  ;
	  MELT_LOCATION ("warmelt-debug.melt:497:/ quasiblock");


	  /*epilog */
	}
	;
      }
    ;
    /*_.LET___V6*/ meltfptr[4] = /*_.IFELSE___V12*/ meltfptr[10];;

    MELT_LOCATION ("warmelt-debug.melt:479:/ clear");
	   /*clear *//*_.OUT__V7*/ meltfptr[6] = 0;
    /*^clear */
	   /*clear *//*_.DISCR__V8*/ meltfptr[7] = 0;
    /*^clear */
	   /*clear *//*_.DBGI_MAXDEPTH__V9*/ meltfptr[8] = 0;
    /*^clear */
	   /*clear *//*_#MAXDEPTH__L3*/ meltfnum[1] = 0;
    /*^clear */
	   /*clear *//*_#NULL__L4*/ meltfnum[3] = 0;
    /*^clear */
	   /*clear *//*_.IF___V10*/ meltfptr[9] = 0;
    /*^clear */
	   /*clear *//*_#MELT_REALLY_NEED_DBGLIM__L5*/ meltfnum[4] = 0;
    /*^clear */
	   /*clear *//*_#OR___L6*/ meltfnum[5] = 0;
    /*^clear */
	   /*clear *//*_.IFELSE___V12*/ meltfptr[10] = 0;
    MELT_LOCATION ("warmelt-debug.melt:474:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.LET___V6*/ meltfptr[4];;

    {
      MELT_LOCATION ("warmelt-debug.melt:474:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V4*/ meltfptr[3] = 0;
    /*^clear */
	   /*clear *//*_.LET___V6*/ meltfptr[4] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("DBG_OUT", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_10_warmelt_debug_DBG_OUT_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_10_warmelt_debug_DBG_OUT */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_11_warmelt_debug_DBGOUT_FIELDS (meltclosure_ptr_t meltclosp_,
					 melt_ptr_t meltfirstargp_,
					 const melt_argdescr_cell_t
					 meltxargdescr_[],
					 union meltparam_un * meltxargtab_,
					 const melt_argdescr_cell_t
					 meltxresdescr_[],
					 union meltparam_un * meltxrestab_)
{
  long current_blocklevel_signals_meltrout_11_warmelt_debug_DBGOUT_FIELDS_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_11_warmelt_debug_DBGOUT_FIELDS_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 21
    melt_ptr_t mcfr_varptr[21];
#define MELTFRAM_NBVARNUM 42
    long mcfr_varnum[42];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_11_warmelt_debug_DBGOUT_FIELDS is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_11_warmelt_debug_DBGOUT_FIELDS_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 21; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_11_warmelt_debug_DBGOUT_FIELDS nbval 21*/
  meltfram__.mcfr_nbvar = 21 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("DBGOUT_FIELDS", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-debug.melt:502:/ getarg");
 /*_.OBJ__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.DBGI__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.DBGI__V3*/ meltfptr[2])) !=
	      NULL);


  /*getarg#2 */
  /*^getarg */
  if (meltxargdescr_[1] != MELTBPAR_LONG)
    goto lab_endgetargs;
 /*_#DEPTH__L1*/ meltfnum[0] = meltxargtab_[1].meltbp_long;

  /*getarg#3 */
  /*^getarg */
  if (meltxargdescr_[2] != MELTBPAR_LONG)
    goto lab_endgetargs;
 /*_#FROMRANK__L2*/ meltfnum[1] = meltxargtab_[2].meltbp_long;

  /*getarg#4 */
  /*^getarg */
  if (meltxargdescr_[3] != MELTBPAR_LONG)
    goto lab_endgetargs;
 /*_#TORANK__L3*/ meltfnum[2] = meltxargtab_[3].meltbp_long;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-debug.melt:505:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L4*/ meltfnum[3] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.DBGI__V3*/ meltfptr[2]),
			     (melt_ptr_t) (( /*!CLASS_DEBUG_INFORMATION */
					    meltfrout->tabval[0])));;
      MELT_LOCATION ("warmelt-debug.melt:505:/ cond");
      /*cond */ if ( /*_#IS_A__L4*/ meltfnum[3])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V5*/ meltfptr[4] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-debug.melt:505:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check dbgi"),
				  ("warmelt-debug.melt")
				  ? ("warmelt-debug.melt") : __FILE__,
				  (505) ? (505) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V5*/ meltfptr[4] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V4*/ meltfptr[3] = /*_.IFELSE___V5*/ meltfptr[4];;
      /*epilog */

      MELT_LOCATION ("warmelt-debug.melt:505:/ clear");
	     /*clear *//*_#IS_A__L4*/ meltfnum[3] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V5*/ meltfptr[4] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V4*/ meltfptr[3] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-debug.melt:506:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_OBJECT__L5*/ meltfnum[3] =
	(melt_magic_discr ((melt_ptr_t) ( /*_.OBJ__V2*/ meltfptr[1])) ==
	 MELTOBMAG_OBJECT);;
      MELT_LOCATION ("warmelt-debug.melt:506:/ cond");
      /*cond */ if ( /*_#IS_OBJECT__L5*/ meltfnum[3])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V7*/ meltfptr[6] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-debug.melt:506:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check obj"),
				  ("warmelt-debug.melt")
				  ? ("warmelt-debug.melt") : __FILE__,
				  (506) ? (506) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V7*/ meltfptr[6] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V6*/ meltfptr[4] = /*_.IFELSE___V7*/ meltfptr[6];;
      /*epilog */

      MELT_LOCATION ("warmelt-debug.melt:506:/ clear");
	     /*clear *//*_#IS_OBJECT__L5*/ meltfnum[3] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V7*/ meltfptr[6] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V6*/ meltfptr[4] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-debug.melt:507:/ quasiblock");


 /*_#NBF__L6*/ meltfnum[3] =
      ((long)
       melt_object_length ((melt_ptr_t) ( /*_.OBJ__V2*/ meltfptr[1])));;
    /*^compute */
 /*_.CLA__V9*/ meltfptr[8] =
      ((melt_ptr_t)
       (melt_discr ((melt_ptr_t) ( /*_.OBJ__V2*/ meltfptr[1]))));;
    /*^compute */
    /*_#IX__L7*/ meltfnum[6] = /*_#FROMRANK__L2*/ meltfnum[1];;
    /*^compute */
 /*_#FLDCNT__L8*/ meltfnum[7] = 0;;
    MELT_LOCATION ("warmelt-debug.melt:511:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.CLA__V9*/ meltfptr[8]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 6, "CLASS_FIELDS");
  /*_.CLAFIELDSEQ__V10*/ meltfptr[9] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-debug.melt:512:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.DBGI__V3*/ meltfptr[2]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 0, "DBGI_OUT");
  /*_.OUT__V11*/ meltfptr[10] = slot;
    };
    ;
 /*_#OUTOFF__L9*/ meltfnum[8] =
      melt_output_length ((melt_ptr_t) /*_.OUT__V11*/ meltfptr[10]);;
    MELT_LOCATION ("warmelt-debug.melt:514:/ cond");
    /*cond */ if (
		   /*ifisa */
		   melt_is_instance_of ((melt_ptr_t)
					( /*_.DBGI__V3*/ meltfptr[2]),
					(melt_ptr_t) (( /*!CLASS_DEBUG_INFORMATION */ meltfrout->tabval[0])))
      )				/*then */
      {
	/*^cond.then */
	/*^getslot */
	{
	  melt_ptr_t slot = NULL, obj = NULL;
	  obj = (melt_ptr_t) ( /*_.DBGI__V3*/ meltfptr[2]) /*=obj*/ ;
	  melt_object_get_field (slot, obj, 2, "DBGI_MAXDEPTH");
   /*_.DBGI_MAXDEPTH__V12*/ meltfptr[11] = slot;
	};
	;
      }
    else
      {				/*^cond.else */

  /*_.DBGI_MAXDEPTH__V12*/ meltfptr[11] = NULL;;
      }
    ;
    /*^compute */
 /*_#MAXDEPTH__L10*/ meltfnum[9] =
      (melt_get_int ((melt_ptr_t) ( /*_.DBGI_MAXDEPTH__V12*/ meltfptr[11])));;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-debug.melt:516:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_OUT__L11*/ meltfnum[10] =
	(melt_is_out ((melt_ptr_t) /*_.OUT__V11*/ meltfptr[10]));;
      MELT_LOCATION ("warmelt-debug.melt:516:/ cond");
      /*cond */ if ( /*_#IS_OUT__L11*/ meltfnum[10])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V14*/ meltfptr[13] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-debug.melt:516:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check out"),
				  ("warmelt-debug.melt")
				  ? ("warmelt-debug.melt") : __FILE__,
				  (516) ? (516) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V14*/ meltfptr[13] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V13*/ meltfptr[12] = /*_.IFELSE___V14*/ meltfptr[13];;
      /*epilog */

      MELT_LOCATION ("warmelt-debug.melt:516:/ clear");
	     /*clear *//*_#IS_OUT__L11*/ meltfnum[10] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V14*/ meltfptr[13] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V13*/ meltfptr[12] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-debug.melt:518:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#I__L12*/ meltfnum[10] =
      (( /*_#IX__L7*/ meltfnum[6]) < (0));;
    MELT_LOCATION ("warmelt-debug.melt:518:/ cond");
    /*cond */ if ( /*_#I__L12*/ meltfnum[10])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  /*^compute */
	  /*_#IX__L7*/ meltfnum[6] = /*_#SETQ___L14*/ meltfnum[13] = 0;;
	  /*_#IF___L13*/ meltfnum[12] = /*_#SETQ___L14*/ meltfnum[13];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-debug.melt:518:/ clear");
	     /*clear *//*_#SETQ___L14*/ meltfnum[13] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

  /*_#IF___L13*/ meltfnum[12] = 0;;
      }
    ;
    MELT_LOCATION ("warmelt-debug.melt:519:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#MELT_REALLY_NEED_DBGLIM__L15*/ meltfnum[13] =
      ( /*MELT_REALLY_NEED_DBGLIM */
       melt_need_debug_limit ((int) /*_#DEPTH__L1*/ meltfnum[0],
			      (int) /*_#MAXDEPTH__L10*/ meltfnum[9]));;
    MELT_LOCATION ("warmelt-debug.melt:519:/ cond");
    /*cond */ if ( /*_#MELT_REALLY_NEED_DBGLIM__L15*/ meltfnum[13])	/*then */
      {
	/*^cond.then */
	/*_#OR___L16*/ meltfnum[15] =
	  /*_#MELT_REALLY_NEED_DBGLIM__L15*/ meltfnum[13];;
      }
    else
      {
	MELT_LOCATION ("warmelt-debug.melt:519:/ cond.else");

	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-debug.melt:521:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
   /*_#I__L17*/ meltfnum[16] =
	    (( /*_#DEPTH__L1*/ meltfnum[0]) <
	     ( /*_#MAXDEPTH__L10*/ meltfnum[9]));;
	  MELT_LOCATION ("warmelt-debug.melt:521:/ cond");
	  /*cond */ if ( /*_#I__L17*/ meltfnum[16])	/*then */
	    {
	      /*^cond.then */
	      /*^block */
	      /*anyblock */
	      {

     /*_#IS_A__L19*/ meltfnum[18] =
		  melt_is_instance_of ((melt_ptr_t)
				       ( /*_.DBGI__V3*/ meltfptr[2]),
				       (melt_ptr_t) (( /*!CLASS_DEBUG_OUTPUT_INFORMATION */ meltfrout->tabval[1])));;
		/*^compute */
		/*_#IF___L18*/ meltfnum[17] = /*_#IS_A__L19*/ meltfnum[18];;
		/*epilog */

		MELT_LOCATION ("warmelt-debug.melt:521:/ clear");
	       /*clear *//*_#IS_A__L19*/ meltfnum[18] = 0;
	      }
	      ;
	    }
	  else
	    {			/*^cond.else */

    /*_#IF___L18*/ meltfnum[17] = 0;;
	    }
	  ;
	  /*^compute */
	  /*_#OR___L16*/ meltfnum[15] = /*_#IF___L18*/ meltfnum[17];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-debug.melt:519:/ clear");
	     /*clear *//*_#I__L17*/ meltfnum[16] = 0;
	  /*^clear */
	     /*clear *//*_#IF___L18*/ meltfnum[17] = 0;
	}
	;
      }
    ;
    /*^cond */
    /*cond */ if ( /*_#OR___L16*/ meltfnum[15])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-debug.melt:522:/ loop");
	  /*loop */
	  {
	  labloop_FLDLOOP_1:;	/*^loopbody */

	    /*^block */
	    /*anyblock */
	    {

	      /*^checksignal */
	      MELT_CHECK_SIGNAL ();
	      ;
	      MELT_LOCATION ("warmelt-debug.melt:523:/ checksignal");
	      MELT_CHECK_SIGNAL ();
	      ;
     /*_#I__L20*/ meltfnum[18] =
		(( /*_#IX__L7*/ meltfnum[6]) >=
		 ( /*_#NBF__L6*/ meltfnum[3]));;
	      MELT_LOCATION ("warmelt-debug.melt:523:/ cond");
	      /*cond */ if ( /*_#I__L20*/ meltfnum[18])	/*then */
		{
		  /*^cond.then */
		  /*^block */
		  /*anyblock */
		  {

		    MELT_LOCATION ("warmelt-debug.melt:524:/ quasiblock");


		    /*^compute */
       /*_.FLDLOOP__V17*/ meltfptr[16] = NULL;;

		    /*^exit */
		    /*exit */
		    {
		      goto labexit_FLDLOOP_1;
		    }
		    ;
		    /*epilog */
		  }
		  ;
		}		/*noelse */
	      ;
	      MELT_LOCATION ("warmelt-debug.melt:525:/ checksignal");
	      MELT_CHECK_SIGNAL ();
	      ;
     /*_#I__L21*/ meltfnum[16] =
		(( /*_#TORANK__L3*/ meltfnum[2]) > (0));;
	      MELT_LOCATION ("warmelt-debug.melt:525:/ cond");
	      /*cond */ if ( /*_#I__L21*/ meltfnum[16])	/*then */
		{
		  /*^cond.then */
		  /*^block */
		  /*anyblock */
		  {

		    /*^checksignal */
		    MELT_CHECK_SIGNAL ();
		    ;
       /*_#I__L22*/ meltfnum[17] =
		      (( /*_#IX__L7*/ meltfnum[6]) >
		       ( /*_#TORANK__L3*/ meltfnum[2]));;
		    MELT_LOCATION ("warmelt-debug.melt:525:/ cond");
		    /*cond */ if ( /*_#I__L22*/ meltfnum[17])	/*then */
		      {
			/*^cond.then */
			/*^block */
			/*anyblock */
			{

			  MELT_LOCATION
			    ("warmelt-debug.melt:527:/ quasiblock");


			  /*^compute */
	 /*_.FLDLOOP__V17*/ meltfptr[16] = NULL;;

			  /*^exit */
			  /*exit */
			  {
			    goto labexit_FLDLOOP_1;
			  }
			  ;
			  /*epilog */
			}
			;
		      }		/*noelse */
		    ;
		    /*epilog */

		    MELT_LOCATION ("warmelt-debug.melt:525:/ clear");
		 /*clear *//*_#I__L22*/ meltfnum[17] = 0;
		  }
		  ;
		}		/*noelse */
	      ;
	      MELT_LOCATION ("warmelt-debug.melt:528:/ quasiblock");


     /*_.CURFLD__V18*/ meltfptr[17] =
		(melt_multiple_nth
		 ((melt_ptr_t) ( /*_.CLAFIELDSEQ__V10*/ meltfptr[9]),
		  ( /*_#IX__L7*/ meltfnum[6])));;
	      /*^compute */
     /*_.CURVAL__V19*/ meltfptr[18] =
		(melt_field_object
		 ((melt_ptr_t) ( /*_.OBJ__V2*/ meltfptr[1]),
		  ( /*_#IX__L7*/ meltfnum[6])));;
	      MELT_LOCATION ("warmelt-debug.melt:531:/ checksignal");
	      MELT_CHECK_SIGNAL ();
	      ;
     /*_#I__L23*/ meltfnum[17] =
		(( /*_#DEPTH__L1*/ meltfnum[0]) <= (1));;
	      MELT_LOCATION ("warmelt-debug.melt:531:/ cond");
	      /*cond */ if ( /*_#I__L23*/ meltfnum[17])	/*then */
		{
		  /*^cond.then */
		  /*_#OR___L24*/ meltfnum[23] = /*_#I__L23*/ meltfnum[17];;
		}
	      else
		{
		  MELT_LOCATION ("warmelt-debug.melt:531:/ cond.else");

		  /*^block */
		  /*anyblock */
		  {

       /*_#NOTNULL__L25*/ meltfnum[24] =
		      (( /*_.CURVAL__V19*/ meltfptr[18]) != NULL);;
		    /*^compute */
		    /*_#OR___L24*/ meltfnum[23] =
		      /*_#NOTNULL__L25*/ meltfnum[24];;
		    /*epilog */

		    MELT_LOCATION ("warmelt-debug.melt:531:/ clear");
		 /*clear *//*_#NOTNULL__L25*/ meltfnum[24] = 0;
		  }
		  ;
		}
	      ;
	      /*^cond */
	      /*cond */ if ( /*_#OR___L24*/ meltfnum[23])	/*then */
		{
		  /*^cond.then */
		  /*^block */
		  /*anyblock */
		  {

		    MELT_LOCATION ("warmelt-debug.melt:532:/ quasiblock");


       /*_#OUTCUROFF__L26*/ meltfnum[24] =
		      melt_output_length ((melt_ptr_t) /*_.OUT__V11*/
					  meltfptr[10]);;
		    /*^compute */
       /*_#I__L27*/ meltfnum[26] =
		      (( /*_#FLDCNT__L8*/ meltfnum[7]) + (1));;
		    MELT_LOCATION ("warmelt-debug.melt:535:/ compute");
		    /*_#FLDCNT__L8*/ meltfnum[7] =
		      /*_#SETQ___L28*/ meltfnum[27] =
		      /*_#I__L27*/ meltfnum[26];;
		    MELT_LOCATION ("warmelt-debug.melt:536:/ checksignal");
		    MELT_CHECK_SIGNAL ();
		    ;
       /*_#I__L29*/ meltfnum[28] =
		      (( /*_#OUTOFF__L9*/ meltfnum[8]) + (80));;
		    /*^compute */
       /*_#I__L30*/ meltfnum[29] =
		      (( /*_#OUTCUROFF__L26*/ meltfnum[24]) >
		       ( /*_#I__L29*/ meltfnum[28]));;
		    MELT_LOCATION ("warmelt-debug.melt:536:/ cond");
		    /*cond */ if ( /*_#I__L30*/ meltfnum[29])	/*then */
		      {
			/*^cond.then */
			/*_#OR___L31*/ meltfnum[30] =
			  /*_#I__L30*/ meltfnum[29];;
		      }
		    else
		      {
			MELT_LOCATION ("warmelt-debug.melt:536:/ cond.else");

			/*^block */
			/*anyblock */
			{

	 /*_#IRAW__L32*/ meltfnum[31] =
			    (( /*_#FLDCNT__L8*/ meltfnum[7]) % (2));;
			  /*^compute */
	 /*_#I__L33*/ meltfnum[32] =
			    (( /*_#IRAW__L32*/ meltfnum[31]) == (0));;
			  MELT_LOCATION ("warmelt-debug.melt:536:/ cond");
			  /*cond */ if ( /*_#I__L33*/ meltfnum[32])	/*then */
			    {
			      /*^cond.then */
			      /*_#OR___L34*/ meltfnum[33] =
				/*_#I__L33*/ meltfnum[32];;
			    }
			  else
			    {
			      MELT_LOCATION
				("warmelt-debug.melt:536:/ cond.else");

			      /*^block */
			      /*anyblock */
			      {

	   /*_#I__L35*/ meltfnum[34] =
				  (( /*_#DEPTH__L1*/ meltfnum[0]) <= (1));;
				/*^compute */
				/*_#OR___L34*/ meltfnum[33] =
				  /*_#I__L35*/ meltfnum[34];;
				/*epilog */

				MELT_LOCATION
				  ("warmelt-debug.melt:536:/ clear");
		     /*clear *//*_#I__L35*/ meltfnum[34] = 0;
			      }
			      ;
			    }
			  ;
			  /*_#OR___L31*/ meltfnum[30] =
			    /*_#OR___L34*/ meltfnum[33];;
			  /*epilog */

			  MELT_LOCATION ("warmelt-debug.melt:536:/ clear");
		   /*clear *//*_#IRAW__L32*/ meltfnum[31] = 0;
			  /*^clear */
		   /*clear *//*_#I__L33*/ meltfnum[32] = 0;
			  /*^clear */
		   /*clear *//*_#OR___L34*/ meltfnum[33] = 0;
			}
			;
		      }
		    ;
		    /*^cond */
		    /*cond */ if ( /*_#OR___L31*/ meltfnum[30])	/*then */
		      {
			/*^cond.then */
			/*^block */
			/*anyblock */
			{


			  {
			    MELT_LOCATION ("warmelt-debug.melt:542:/ locexp");
			    meltgc_out_add_indent ((melt_ptr_t)
						   ( /*_.OUT__V11*/
						    meltfptr[10]),
						   ( /*_#DEPTH__L1*/
						    meltfnum[0]), 0);;
			  }
			  ;
	 /*_#STRBUF_USEDLENGTH__L37*/ meltfnum[31] =
			    melt_strbuf_usedlength ((melt_ptr_t)
						    ( /*_.OUT__V11*/
						     meltfptr[10]));;
			  MELT_LOCATION ("warmelt-debug.melt:543:/ compute");
			  /*_#OUTOFF__L9*/ meltfnum[8] =
			    /*_#SETQ___L38*/ meltfnum[32] =
			    /*_#STRBUF_USEDLENGTH__L37*/ meltfnum[31];;
			  MELT_LOCATION
			    ("warmelt-debug.melt:541:/ quasiblock");


			  /*_#PROGN___L39*/ meltfnum[33] =
			    /*_#SETQ___L38*/ meltfnum[32];;
			  /*^compute */
			  /*_#IFELSE___L36*/ meltfnum[34] =
			    /*_#PROGN___L39*/ meltfnum[33];;
			  /*epilog */

			  MELT_LOCATION ("warmelt-debug.melt:536:/ clear");
		   /*clear *//*_#STRBUF_USEDLENGTH__L37*/ meltfnum[31]
			    = 0;
			  /*^clear */
		   /*clear *//*_#SETQ___L38*/ meltfnum[32] = 0;
			  /*^clear */
		   /*clear *//*_#PROGN___L39*/ meltfnum[33] = 0;
			}
			;
		      }
		    else
		      {		/*^cond.else */

			/*^block */
			/*anyblock */
			{




			  {
			    MELT_LOCATION ("warmelt-debug.melt:545:/ locexp");
			    meltgc_add_out ((melt_ptr_t)
					    ( /*_.OUT__V11*/ meltfptr[10]),
					    (" "));
			  }
			  ;
		   /*clear *//*_#IFELSE___L36*/ meltfnum[34] = 0;
			  /*epilog */
			}
			;
		      }
		    ;
		    MELT_LOCATION ("warmelt-debug.melt:547:/ getslot");
		    {
		      melt_ptr_t slot = NULL, obj = NULL;
		      obj =
			(melt_ptr_t) ( /*_.CURFLD__V18*/ meltfptr[17])
			/*=obj*/ ;
		      melt_object_get_field (slot, obj, 1, "NAMED_NAME");
	/*_.NAMED_NAME__V20*/ meltfptr[19] = slot;
		    };
		    ;

		    {
		      /*^locexp */
		      meltgc_add_out ((melt_ptr_t)
				      ( /*_.OUT__V11*/ meltfptr[10]),
				      melt_string_str ((melt_ptr_t)
						       ( /*_.NAMED_NAME__V20*/
							meltfptr[19])));
		    }
		    ;

		    {
		      MELT_LOCATION ("warmelt-debug.melt:548:/ locexp");
		      meltgc_add_out ((melt_ptr_t)
				      ( /*_.OUT__V11*/ meltfptr[10]), ("="));
		    }
		    ;
       /*_#I__L40*/ meltfnum[31] =
		      (( /*_#DEPTH__L1*/ meltfnum[0]) + (1));;
		    MELT_LOCATION ("warmelt-debug.melt:549:/ checksignal");
		    MELT_CHECK_SIGNAL ();
		    ;
		    /*^apply */
		    /*apply */
		    {
		      union meltparam_un argtab[2];
		      memset (&argtab, 0, sizeof (argtab));
		      /*^apply.arg */
		      argtab[0].meltbp_aptr =
			(melt_ptr_t *) & /*_.DBGI__V3*/ meltfptr[2];
		      /*^apply.arg */
		      argtab[1].meltbp_long = /*_#I__L40*/ meltfnum[31];
		      /*_.DBG_OUT__V21*/ meltfptr[20] =
			melt_apply ((meltclosure_ptr_t)
				    (( /*!DBG_OUT */ meltfrout->tabval[2])),
				    (melt_ptr_t) ( /*_.CURVAL__V19*/
						  meltfptr[18]),
				    (MELTBPARSTR_PTR MELTBPARSTR_LONG ""),
				    argtab, "", (union meltparam_un *) 0);
		    }
		    ;

		    {
		      MELT_LOCATION ("warmelt-debug.melt:550:/ locexp");
		      meltgc_out_add_indent ((melt_ptr_t)
					     ( /*_.OUT__V11*/ meltfptr[10]),
					     ( /*_#DEPTH__L1*/ meltfnum[0]),
					     64);;
		    }
		    ;

		    MELT_LOCATION ("warmelt-debug.melt:532:/ clear");
		 /*clear *//*_#OUTCUROFF__L26*/ meltfnum[24] = 0;
		    /*^clear */
		 /*clear *//*_#I__L27*/ meltfnum[26] = 0;
		    /*^clear */
		 /*clear *//*_#SETQ___L28*/ meltfnum[27] = 0;
		    /*^clear */
		 /*clear *//*_#I__L29*/ meltfnum[28] = 0;
		    /*^clear */
		 /*clear *//*_#I__L30*/ meltfnum[29] = 0;
		    /*^clear */
		 /*clear *//*_#OR___L31*/ meltfnum[30] = 0;
		    /*^clear */
		 /*clear *//*_#IFELSE___L36*/ meltfnum[34] = 0;
		    /*^clear */
		 /*clear *//*_.NAMED_NAME__V20*/ meltfptr[19] = 0;
		    /*^clear */
		 /*clear *//*_#I__L40*/ meltfnum[31] = 0;
		    /*^clear */
		 /*clear *//*_.DBG_OUT__V21*/ meltfptr[20] = 0;
		    /*epilog */
		  }
		  ;
		}		/*noelse */
	      ;

	      MELT_LOCATION ("warmelt-debug.melt:528:/ clear");
	       /*clear *//*_.CURFLD__V18*/ meltfptr[17] = 0;
	      /*^clear */
	       /*clear *//*_.CURVAL__V19*/ meltfptr[18] = 0;
	      /*^clear */
	       /*clear *//*_#I__L23*/ meltfnum[17] = 0;
	      /*^clear */
	       /*clear *//*_#OR___L24*/ meltfnum[23] = 0;
     /*_#I__L41*/ meltfnum[32] =
		(( /*_#IX__L7*/ meltfnum[6]) + (1));;
	      MELT_LOCATION ("warmelt-debug.melt:552:/ compute");
	      /*_#IX__L7*/ meltfnum[6] = /*_#SETQ___L42*/ meltfnum[33] =
		/*_#I__L41*/ meltfnum[32];;
	      MELT_LOCATION ("warmelt-debug.melt:522:/ checksignal");
	      MELT_CHECK_SIGNAL ();
	      ;
	      /*epilog */

	      /*^clear */
	       /*clear *//*_#I__L20*/ meltfnum[18] = 0;
	      /*^clear */
	       /*clear *//*_#I__L21*/ meltfnum[16] = 0;
	      /*^clear */
	       /*clear *//*_#I__L41*/ meltfnum[32] = 0;
	      /*^clear */
	       /*clear *//*_#SETQ___L42*/ meltfnum[33] = 0;
	    }
	    ;
	    ;
	    goto labloop_FLDLOOP_1;
	  labexit_FLDLOOP_1:;	/*^loopepilog */
	    /*loopepilog */
	    /*_.FOREVER___V16*/ meltfptr[15] =
	      /*_.FLDLOOP__V17*/ meltfptr[16];;
	  }
	  ;
	  /*^compute */
	  /*_.IF___V15*/ meltfptr[13] = /*_.FOREVER___V16*/ meltfptr[15];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-debug.melt:519:/ clear");
	     /*clear *//*_.FOREVER___V16*/ meltfptr[15] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

  /*_.IF___V15*/ meltfptr[13] = NULL;;
      }
    ;
    /*^compute */
    /*_.LET___V8*/ meltfptr[6] = /*_.IF___V15*/ meltfptr[13];;

    MELT_LOCATION ("warmelt-debug.melt:507:/ clear");
	   /*clear *//*_#NBF__L6*/ meltfnum[3] = 0;
    /*^clear */
	   /*clear *//*_.CLA__V9*/ meltfptr[8] = 0;
    /*^clear */
	   /*clear *//*_#IX__L7*/ meltfnum[6] = 0;
    /*^clear */
	   /*clear *//*_#FLDCNT__L8*/ meltfnum[7] = 0;
    /*^clear */
	   /*clear *//*_.CLAFIELDSEQ__V10*/ meltfptr[9] = 0;
    /*^clear */
	   /*clear *//*_.OUT__V11*/ meltfptr[10] = 0;
    /*^clear */
	   /*clear *//*_#OUTOFF__L9*/ meltfnum[8] = 0;
    /*^clear */
	   /*clear *//*_.DBGI_MAXDEPTH__V12*/ meltfptr[11] = 0;
    /*^clear */
	   /*clear *//*_#MAXDEPTH__L10*/ meltfnum[9] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V13*/ meltfptr[12] = 0;
    /*^clear */
	   /*clear *//*_#I__L12*/ meltfnum[10] = 0;
    /*^clear */
	   /*clear *//*_#IF___L13*/ meltfnum[12] = 0;
    /*^clear */
	   /*clear *//*_#MELT_REALLY_NEED_DBGLIM__L15*/ meltfnum[13] = 0;
    /*^clear */
	   /*clear *//*_#OR___L16*/ meltfnum[15] = 0;
    /*^clear */
	   /*clear *//*_.IF___V15*/ meltfptr[13] = 0;
    MELT_LOCATION ("warmelt-debug.melt:502:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.LET___V8*/ meltfptr[6];;

    {
      MELT_LOCATION ("warmelt-debug.melt:502:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V4*/ meltfptr[3] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V6*/ meltfptr[4] = 0;
    /*^clear */
	   /*clear *//*_.LET___V8*/ meltfptr[6] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("DBGOUT_FIELDS", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_11_warmelt_debug_DBGOUT_FIELDS_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_11_warmelt_debug_DBGOUT_FIELDS */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_12_warmelt_debug_DBGOUTAGAIN_FIELDS (meltclosure_ptr_t meltclosp_,
					      melt_ptr_t meltfirstargp_,
					      const melt_argdescr_cell_t
					      meltxargdescr_[],
					      union meltparam_un *
					      meltxargtab_,
					      const melt_argdescr_cell_t
					      meltxresdescr_[],
					      union meltparam_un *
					      meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_12_warmelt_debug_DBGOUTAGAIN_FIELDS_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_12_warmelt_debug_DBGOUTAGAIN_FIELDS_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 21
    melt_ptr_t mcfr_varptr[21];
#define MELTFRAM_NBVARNUM 24
    long mcfr_varnum[24];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_12_warmelt_debug_DBGOUTAGAIN_FIELDS is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_12_warmelt_debug_DBGOUTAGAIN_FIELDS_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 21; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_12_warmelt_debug_DBGOUTAGAIN_FIELDS nbval 21*/
  meltfram__.mcfr_nbvar = 21 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("DBGOUTAGAIN_FIELDS", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-debug.melt:557:/ getarg");
 /*_.OBJ__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.DBGI__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.DBGI__V3*/ meltfptr[2])) !=
	      NULL);


  /*getarg#2 */
  /*^getarg */
  if (meltxargdescr_[1] != MELTBPAR_LONG)
    goto lab_endgetargs;
 /*_#DEPTH__L1*/ meltfnum[0] = meltxargtab_[1].meltbp_long;

  /*getarg#3 */
  /*^getarg */
  if (meltxargdescr_[2] != MELTBPAR_LONG)
    goto lab_endgetargs;
 /*_#FROMRANK__L2*/ meltfnum[1] = meltxargtab_[2].meltbp_long;

  /*getarg#4 */
  /*^getarg */
  if (meltxargdescr_[3] != MELTBPAR_LONG)
    goto lab_endgetargs;
 /*_#TORANK__L3*/ meltfnum[2] = meltxargtab_[3].meltbp_long;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-debug.melt:561:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L4*/ meltfnum[3] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.DBGI__V3*/ meltfptr[2]),
			     (melt_ptr_t) (( /*!CLASS_DEBUG_INFORMATION */
					    meltfrout->tabval[0])));;
      MELT_LOCATION ("warmelt-debug.melt:561:/ cond");
      /*cond */ if ( /*_#IS_A__L4*/ meltfnum[3])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V5*/ meltfptr[4] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-debug.melt:561:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check dbgi"),
				  ("warmelt-debug.melt")
				  ? ("warmelt-debug.melt") : __FILE__,
				  (561) ? (561) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V5*/ meltfptr[4] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V4*/ meltfptr[3] = /*_.IFELSE___V5*/ meltfptr[4];;
      /*epilog */

      MELT_LOCATION ("warmelt-debug.melt:561:/ clear");
	     /*clear *//*_#IS_A__L4*/ meltfnum[3] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V5*/ meltfptr[4] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V4*/ meltfptr[3] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-debug.melt:562:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_OBJECT__L5*/ meltfnum[3] =
	(melt_magic_discr ((melt_ptr_t) ( /*_.OBJ__V2*/ meltfptr[1])) ==
	 MELTOBMAG_OBJECT);;
      MELT_LOCATION ("warmelt-debug.melt:562:/ cond");
      /*cond */ if ( /*_#IS_OBJECT__L5*/ meltfnum[3])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V7*/ meltfptr[6] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-debug.melt:562:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check obj"),
				  ("warmelt-debug.melt")
				  ? ("warmelt-debug.melt") : __FILE__,
				  (562) ? (562) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V7*/ meltfptr[6] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V6*/ meltfptr[4] = /*_.IFELSE___V7*/ meltfptr[6];;
      /*epilog */

      MELT_LOCATION ("warmelt-debug.melt:562:/ clear");
	     /*clear *//*_#IS_OBJECT__L5*/ meltfnum[3] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V7*/ meltfptr[6] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V6*/ meltfptr[4] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-debug.melt:563:/ quasiblock");


 /*_#NBF__L6*/ meltfnum[3] =
      ((long)
       melt_object_length ((melt_ptr_t) ( /*_.OBJ__V2*/ meltfptr[1])));;
    /*^compute */
 /*_.CLA__V9*/ meltfptr[8] =
      ((melt_ptr_t)
       (melt_discr ((melt_ptr_t) ( /*_.OBJ__V2*/ meltfptr[1]))));;
    /*^compute */
    /*_#IX__L7*/ meltfnum[6] = /*_#FROMRANK__L2*/ meltfnum[1];;
    MELT_LOCATION ("warmelt-debug.melt:566:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.CLA__V9*/ meltfptr[8]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 6, "CLASS_FIELDS");
  /*_.CLAFIELDSEQ__V10*/ meltfptr[9] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-debug.melt:567:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.DBGI__V3*/ meltfptr[2]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 0, "DBGI_OUT");
  /*_.OUT__V11*/ meltfptr[10] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-debug.melt:568:/ cond");
    /*cond */ if (
		   /*ifisa */
		   melt_is_instance_of ((melt_ptr_t)
					( /*_.DBGI__V3*/ meltfptr[2]),
					(melt_ptr_t) (( /*!CLASS_DEBUG_INFORMATION */ meltfrout->tabval[0])))
      )				/*then */
      {
	/*^cond.then */
	/*^getslot */
	{
	  melt_ptr_t slot = NULL, obj = NULL;
	  obj = (melt_ptr_t) ( /*_.DBGI__V3*/ meltfptr[2]) /*=obj*/ ;
	  melt_object_get_field (slot, obj, 2, "DBGI_MAXDEPTH");
   /*_.DBGI_MAXDEPTH__V12*/ meltfptr[11] = slot;
	};
	;
      }
    else
      {				/*^cond.else */

  /*_.DBGI_MAXDEPTH__V12*/ meltfptr[11] = NULL;;
      }
    ;
    /*^compute */
 /*_#MAXDEPTH__L8*/ meltfnum[7] =
      (melt_get_int ((melt_ptr_t) ( /*_.DBGI_MAXDEPTH__V12*/ meltfptr[11])));;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-debug.melt:570:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_OUT__L9*/ meltfnum[8] =
	(melt_is_out ((melt_ptr_t) /*_.OUT__V11*/ meltfptr[10]));;
      MELT_LOCATION ("warmelt-debug.melt:570:/ cond");
      /*cond */ if ( /*_#IS_OUT__L9*/ meltfnum[8])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V14*/ meltfptr[13] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-debug.melt:570:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check out"),
				  ("warmelt-debug.melt")
				  ? ("warmelt-debug.melt") : __FILE__,
				  (570) ? (570) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V14*/ meltfptr[13] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V13*/ meltfptr[12] = /*_.IFELSE___V14*/ meltfptr[13];;
      /*epilog */

      MELT_LOCATION ("warmelt-debug.melt:570:/ clear");
	     /*clear *//*_#IS_OUT__L9*/ meltfnum[8] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V14*/ meltfptr[13] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V13*/ meltfptr[12] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-debug.melt:571:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#I__L10*/ meltfnum[8] =
      (( /*_#IX__L7*/ meltfnum[6]) < (0));;
    MELT_LOCATION ("warmelt-debug.melt:571:/ cond");
    /*cond */ if ( /*_#I__L10*/ meltfnum[8])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  /*^compute */
	  /*_#IX__L7*/ meltfnum[6] = /*_#SETQ___L12*/ meltfnum[11] = 0;;
	  /*_#IF___L11*/ meltfnum[10] = /*_#SETQ___L12*/ meltfnum[11];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-debug.melt:571:/ clear");
	     /*clear *//*_#SETQ___L12*/ meltfnum[11] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

  /*_#IF___L11*/ meltfnum[10] = 0;;
      }
    ;
    MELT_LOCATION ("warmelt-debug.melt:572:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#MELT_REALLY_NEED_DBGLIM__L13*/ meltfnum[11] =
      ( /*MELT_REALLY_NEED_DBGLIM */
       melt_need_debug_limit ((int) /*_#DEPTH__L1*/ meltfnum[0],
			      (int) /*_#MAXDEPTH__L8*/ meltfnum[7]));;
    MELT_LOCATION ("warmelt-debug.melt:572:/ cond");
    /*cond */ if ( /*_#MELT_REALLY_NEED_DBGLIM__L13*/ meltfnum[11])	/*then */
      {
	/*^cond.then */
	/*_#OR___L14*/ meltfnum[13] =
	  /*_#MELT_REALLY_NEED_DBGLIM__L13*/ meltfnum[11];;
      }
    else
      {
	MELT_LOCATION ("warmelt-debug.melt:572:/ cond.else");

	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-debug.melt:574:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
   /*_#I__L15*/ meltfnum[14] =
	    (( /*_#DEPTH__L1*/ meltfnum[0]) <
	     ( /*_#MAXDEPTH__L8*/ meltfnum[7]));;
	  MELT_LOCATION ("warmelt-debug.melt:574:/ cond");
	  /*cond */ if ( /*_#I__L15*/ meltfnum[14])	/*then */
	    {
	      /*^cond.then */
	      /*^block */
	      /*anyblock */
	      {

     /*_#IS_A__L17*/ meltfnum[16] =
		  melt_is_instance_of ((melt_ptr_t)
				       ( /*_.DBGI__V3*/ meltfptr[2]),
				       (melt_ptr_t) (( /*!CLASS_DEBUG_OUTPUT_INFORMATION */ meltfrout->tabval[1])));;
		/*^compute */
		/*_#IF___L16*/ meltfnum[15] = /*_#IS_A__L17*/ meltfnum[16];;
		/*epilog */

		MELT_LOCATION ("warmelt-debug.melt:574:/ clear");
	       /*clear *//*_#IS_A__L17*/ meltfnum[16] = 0;
	      }
	      ;
	    }
	  else
	    {			/*^cond.else */

    /*_#IF___L16*/ meltfnum[15] = 0;;
	    }
	  ;
	  /*^compute */
	  /*_#OR___L14*/ meltfnum[13] = /*_#IF___L16*/ meltfnum[15];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-debug.melt:572:/ clear");
	     /*clear *//*_#I__L15*/ meltfnum[14] = 0;
	  /*^clear */
	     /*clear *//*_#IF___L16*/ meltfnum[15] = 0;
	}
	;
      }
    ;
    /*^cond */
    /*cond */ if ( /*_#OR___L14*/ meltfnum[13])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-debug.melt:575:/ loop");
	  /*loop */
	  {
	  labloop_FLDLOOP_2:;	/*^loopbody */

	    /*^block */
	    /*anyblock */
	    {

	      /*^checksignal */
	      MELT_CHECK_SIGNAL ();
	      ;
	      MELT_LOCATION ("warmelt-debug.melt:576:/ checksignal");
	      MELT_CHECK_SIGNAL ();
	      ;
     /*_#I__L18*/ meltfnum[16] =
		(( /*_#IX__L7*/ meltfnum[6]) >=
		 ( /*_#NBF__L6*/ meltfnum[3]));;
	      MELT_LOCATION ("warmelt-debug.melt:576:/ cond");
	      /*cond */ if ( /*_#I__L18*/ meltfnum[16])	/*then */
		{
		  /*^cond.then */
		  /*^block */
		  /*anyblock */
		  {

		    MELT_LOCATION ("warmelt-debug.melt:577:/ quasiblock");


		    /*^compute */
       /*_.FLDLOOP__V17*/ meltfptr[16] = NULL;;

		    /*^exit */
		    /*exit */
		    {
		      goto labexit_FLDLOOP_2;
		    }
		    ;
		    /*epilog */
		  }
		  ;
		}		/*noelse */
	      ;
	      MELT_LOCATION ("warmelt-debug.melt:578:/ checksignal");
	      MELT_CHECK_SIGNAL ();
	      ;
     /*_#I__L19*/ meltfnum[14] =
		(( /*_#TORANK__L3*/ meltfnum[2]) > (0));;
	      MELT_LOCATION ("warmelt-debug.melt:578:/ cond");
	      /*cond */ if ( /*_#I__L19*/ meltfnum[14])	/*then */
		{
		  /*^cond.then */
		  /*^block */
		  /*anyblock */
		  {

		    /*^checksignal */
		    MELT_CHECK_SIGNAL ();
		    ;
       /*_#I__L20*/ meltfnum[15] =
		      (( /*_#IX__L7*/ meltfnum[6]) >
		       ( /*_#TORANK__L3*/ meltfnum[2]));;
		    MELT_LOCATION ("warmelt-debug.melt:578:/ cond");
		    /*cond */ if ( /*_#I__L20*/ meltfnum[15])	/*then */
		      {
			/*^cond.then */
			/*^block */
			/*anyblock */
			{

			  MELT_LOCATION
			    ("warmelt-debug.melt:580:/ quasiblock");


			  /*^compute */
	 /*_.FLDLOOP__V17*/ meltfptr[16] = NULL;;

			  /*^exit */
			  /*exit */
			  {
			    goto labexit_FLDLOOP_2;
			  }
			  ;
			  /*epilog */
			}
			;
		      }		/*noelse */
		    ;
		    /*epilog */

		    MELT_LOCATION ("warmelt-debug.melt:578:/ clear");
		 /*clear *//*_#I__L20*/ meltfnum[15] = 0;
		  }
		  ;
		}		/*noelse */
	      ;
	      MELT_LOCATION ("warmelt-debug.melt:581:/ quasiblock");


     /*_.CURFLD__V18*/ meltfptr[17] =
		(melt_multiple_nth
		 ((melt_ptr_t) ( /*_.CLAFIELDSEQ__V10*/ meltfptr[9]),
		  ( /*_#IX__L7*/ meltfnum[6])));;
	      /*^compute */
     /*_.CURVAL__V19*/ meltfptr[18] =
		(melt_field_object
		 ((melt_ptr_t) ( /*_.OBJ__V2*/ meltfptr[1]),
		  ( /*_#IX__L7*/ meltfnum[6])));;
	      MELT_LOCATION ("warmelt-debug.melt:584:/ checksignal");
	      MELT_CHECK_SIGNAL ();
	      ;
	      /*^cond */
	      /*cond */ if ( /*_.CURVAL__V19*/ meltfptr[18])	/*then */
		{
		  /*^cond.then */
		  /*^block */
		  /*anyblock */
		  {


		    {
		      MELT_LOCATION ("warmelt-debug.melt:586:/ locexp");
		      meltgc_out_add_indent ((melt_ptr_t)
					     ( /*_.OUT__V11*/ meltfptr[10]),
					     ( /*_#DEPTH__L1*/ meltfnum[0]),
					     64);;
		    }
		    ;
		    MELT_LOCATION ("warmelt-debug.melt:588:/ getslot");
		    {
		      melt_ptr_t slot = NULL, obj = NULL;
		      obj =
			(melt_ptr_t) ( /*_.CURFLD__V18*/ meltfptr[17])
			/*=obj*/ ;
		      melt_object_get_field (slot, obj, 1, "NAMED_NAME");
	/*_.NAMED_NAME__V20*/ meltfptr[19] = slot;
		    };
		    ;

		    {
		      MELT_LOCATION ("warmelt-debug.melt:587:/ locexp");
		      meltgc_add_out ((melt_ptr_t)
				      ( /*_.OUT__V11*/ meltfptr[10]),
				      melt_string_str ((melt_ptr_t)
						       ( /*_.NAMED_NAME__V20*/
							meltfptr[19])));
		    }
		    ;

		    {
		      MELT_LOCATION ("warmelt-debug.melt:589:/ locexp");
		      meltgc_add_out ((melt_ptr_t)
				      ( /*_.OUT__V11*/ meltfptr[10]), ("="));
		    }
		    ;
       /*_#I__L22*/ meltfnum[21] =
		      (( /*_#DEPTH__L1*/ meltfnum[0]) + (1));;
		    MELT_LOCATION ("warmelt-debug.melt:590:/ checksignal");
		    MELT_CHECK_SIGNAL ();
		    ;
		    /*^msend */
		    /*msend */
		    {
		      union meltparam_un argtab[2];
		      memset (&argtab, 0, sizeof (argtab));
		      /*^ojbmsend.arg */
		      argtab[0].meltbp_aptr =
			(melt_ptr_t *) & /*_.DBGI__V3*/ meltfptr[2];
		      /*^ojbmsend.arg */
		      argtab[1].meltbp_long = /*_#I__L22*/ meltfnum[21];
		      /*_.DBG_OUTPUTAGAIN__V21*/ meltfptr[20] =
			meltgc_send ((melt_ptr_t)
				     ( /*_.CURVAL__V19*/ meltfptr[18]),
				     (melt_ptr_t) (( /*!DBG_OUTPUTAGAIN */
						    meltfrout->tabval[2])),
				     (MELTBPARSTR_PTR MELTBPARSTR_LONG ""),
				     argtab, "", (union meltparam_un *) 0);
		    }
		    ;

		    {
		      MELT_LOCATION ("warmelt-debug.melt:591:/ locexp");
		      meltgc_out_add_indent ((melt_ptr_t)
					     ( /*_.OUT__V11*/ meltfptr[10]),
					     ( /*_#DEPTH__L1*/ meltfnum[0]),
					     64);;
		    }
		    ;
		    MELT_LOCATION ("warmelt-debug.melt:585:/ quasiblock");


		    /*epilog */

		    MELT_LOCATION ("warmelt-debug.melt:584:/ clear");
		 /*clear *//*_.NAMED_NAME__V20*/ meltfptr[19] = 0;
		    /*^clear */
		 /*clear *//*_#I__L22*/ meltfnum[21] = 0;
		    /*^clear */
		 /*clear *//*_.DBG_OUTPUTAGAIN__V21*/ meltfptr[20] = 0;
		  }
		  ;
		}		/*noelse */
	      ;
     /*_#I__L23*/ meltfnum[21] =
		(( /*_#IX__L7*/ meltfnum[6]) + (1));;
	      MELT_LOCATION ("warmelt-debug.melt:593:/ compute");
	      /*_#IX__L7*/ meltfnum[6] = /*_#SETQ___L24*/ meltfnum[23] =
		/*_#I__L23*/ meltfnum[21];;
	      /*_#LET___L21*/ meltfnum[15] = /*_#SETQ___L24*/ meltfnum[23];;

	      MELT_LOCATION ("warmelt-debug.melt:581:/ clear");
	       /*clear *//*_.CURFLD__V18*/ meltfptr[17] = 0;
	      /*^clear */
	       /*clear *//*_.CURVAL__V19*/ meltfptr[18] = 0;
	      /*^clear */
	       /*clear *//*_#I__L23*/ meltfnum[21] = 0;
	      /*^clear */
	       /*clear *//*_#SETQ___L24*/ meltfnum[23] = 0;
	      MELT_LOCATION ("warmelt-debug.melt:575:/ checksignal");
	      MELT_CHECK_SIGNAL ();
	      ;
	      /*epilog */

	      /*^clear */
	       /*clear *//*_#I__L18*/ meltfnum[16] = 0;
	      /*^clear */
	       /*clear *//*_#I__L19*/ meltfnum[14] = 0;
	      /*^clear */
	       /*clear *//*_#LET___L21*/ meltfnum[15] = 0;
	    }
	    ;
	    ;
	    goto labloop_FLDLOOP_2;
	  labexit_FLDLOOP_2:;	/*^loopepilog */
	    /*loopepilog */
	    /*_.FOREVER___V16*/ meltfptr[15] =
	      /*_.FLDLOOP__V17*/ meltfptr[16];;
	  }
	  ;
	  /*^compute */
	  /*_.IF___V15*/ meltfptr[13] = /*_.FOREVER___V16*/ meltfptr[15];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-debug.melt:572:/ clear");
	     /*clear *//*_.FOREVER___V16*/ meltfptr[15] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

  /*_.IF___V15*/ meltfptr[13] = NULL;;
      }
    ;
    /*^compute */
    /*_.LET___V8*/ meltfptr[6] = /*_.IF___V15*/ meltfptr[13];;

    MELT_LOCATION ("warmelt-debug.melt:563:/ clear");
	   /*clear *//*_#NBF__L6*/ meltfnum[3] = 0;
    /*^clear */
	   /*clear *//*_.CLA__V9*/ meltfptr[8] = 0;
    /*^clear */
	   /*clear *//*_#IX__L7*/ meltfnum[6] = 0;
    /*^clear */
	   /*clear *//*_.CLAFIELDSEQ__V10*/ meltfptr[9] = 0;
    /*^clear */
	   /*clear *//*_.OUT__V11*/ meltfptr[10] = 0;
    /*^clear */
	   /*clear *//*_.DBGI_MAXDEPTH__V12*/ meltfptr[11] = 0;
    /*^clear */
	   /*clear *//*_#MAXDEPTH__L8*/ meltfnum[7] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V13*/ meltfptr[12] = 0;
    /*^clear */
	   /*clear *//*_#I__L10*/ meltfnum[8] = 0;
    /*^clear */
	   /*clear *//*_#IF___L11*/ meltfnum[10] = 0;
    /*^clear */
	   /*clear *//*_#MELT_REALLY_NEED_DBGLIM__L13*/ meltfnum[11] = 0;
    /*^clear */
	   /*clear *//*_#OR___L14*/ meltfnum[13] = 0;
    /*^clear */
	   /*clear *//*_.IF___V15*/ meltfptr[13] = 0;
    MELT_LOCATION ("warmelt-debug.melt:557:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.LET___V8*/ meltfptr[6];;

    {
      MELT_LOCATION ("warmelt-debug.melt:557:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V4*/ meltfptr[3] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V6*/ meltfptr[4] = 0;
    /*^clear */
	   /*clear *//*_.LET___V8*/ meltfptr[6] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("DBGOUTAGAIN_FIELDS", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_12_warmelt_debug_DBGOUTAGAIN_FIELDS_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_12_warmelt_debug_DBGOUTAGAIN_FIELDS */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_13_warmelt_debug_DBGOUT_NULL_METHOD (meltclosure_ptr_t meltclosp_,
					      melt_ptr_t meltfirstargp_,
					      const melt_argdescr_cell_t
					      meltxargdescr_[],
					      union meltparam_un *
					      meltxargtab_,
					      const melt_argdescr_cell_t
					      meltxresdescr_[],
					      union meltparam_un *
					      meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_13_warmelt_debug_DBGOUT_NULL_METHOD_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_13_warmelt_debug_DBGOUT_NULL_METHOD_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 4
    melt_ptr_t mcfr_varptr[4];
#define MELTFRAM_NBVARNUM 1
    long mcfr_varnum[1];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_13_warmelt_debug_DBGOUT_NULL_METHOD is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_13_warmelt_debug_DBGOUT_NULL_METHOD_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 4; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_13_warmelt_debug_DBGOUT_NULL_METHOD nbval 4*/
  meltfram__.mcfr_nbvar = 4 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("DBGOUT_NULL_METHOD", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-debug.melt:601:/ getarg");
 /*_.SELF__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.DBGI__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.DBGI__V3*/ meltfptr[2])) !=
	      NULL);


  /*getarg#2 */
  /*^getarg */
  if (meltxargdescr_[1] != MELTBPAR_LONG)
    goto lab_endgetargs;
 /*_#DEPTH__L1*/ meltfnum[0] = meltxargtab_[1].meltbp_long;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {

    MELT_LOCATION ("warmelt-debug.melt:602:/ quasiblock");


    /*^getslot */
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.DBGI__V3*/ meltfptr[2]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 0, "DBGI_OUT");
  /*_.OUT__V4*/ meltfptr[3] = slot;
    };
    ;

    {
      MELT_LOCATION ("warmelt-debug.melt:604:/ locexp");
      meltgc_add_out ((melt_ptr_t) ( /*_.OUT__V4*/ meltfptr[3]), ("()"));
    }
    ;

    MELT_LOCATION ("warmelt-debug.melt:602:/ clear");
	   /*clear *//*_.OUT__V4*/ meltfptr[3] = 0;
    /*epilog */
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("DBGOUT_NULL_METHOD", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_13_warmelt_debug_DBGOUT_NULL_METHOD_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*noretval */ NULL);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_13_warmelt_debug_DBGOUT_NULL_METHOD */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_14_warmelt_debug_DBGOUT_STRING_METHOD (meltclosure_ptr_t meltclosp_,
						melt_ptr_t meltfirstargp_,
						const melt_argdescr_cell_t
						meltxargdescr_[],
						union meltparam_un *
						meltxargtab_,
						const melt_argdescr_cell_t
						meltxresdescr_[],
						union meltparam_un *
						meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_14_warmelt_debug_DBGOUT_STRING_METHOD_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_14_warmelt_debug_DBGOUT_STRING_METHOD_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 8
    melt_ptr_t mcfr_varptr[8];
#define MELTFRAM_NBVARNUM 3
    long mcfr_varnum[3];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_14_warmelt_debug_DBGOUT_STRING_METHOD is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_14_warmelt_debug_DBGOUT_STRING_METHOD_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 8; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_14_warmelt_debug_DBGOUT_STRING_METHOD nbval 8*/
  meltfram__.mcfr_nbvar = 8 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("DBGOUT_STRING_METHOD", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-debug.melt:609:/ getarg");
 /*_.SELF__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.DBGI__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.DBGI__V3*/ meltfptr[2])) !=
	      NULL);


  /*getarg#2 */
  /*^getarg */
  if (meltxargdescr_[1] != MELTBPAR_LONG)
    goto lab_endgetargs;
 /*_#DEPTH__L1*/ meltfnum[0] = meltxargtab_[1].meltbp_long;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-debug.melt:610:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L2*/ meltfnum[1] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.DBGI__V3*/ meltfptr[2]),
			     (melt_ptr_t) (( /*!CLASS_DEBUG_INFORMATION */
					    meltfrout->tabval[0])));;
      MELT_LOCATION ("warmelt-debug.melt:610:/ cond");
      /*cond */ if ( /*_#IS_A__L2*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V5*/ meltfptr[4] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-debug.melt:610:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check dbgi"),
				  ("warmelt-debug.melt")
				  ? ("warmelt-debug.melt") : __FILE__,
				  (610) ? (610) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V5*/ meltfptr[4] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V4*/ meltfptr[3] = /*_.IFELSE___V5*/ meltfptr[4];;
      /*epilog */

      MELT_LOCATION ("warmelt-debug.melt:610:/ clear");
	     /*clear *//*_#IS_A__L2*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V5*/ meltfptr[4] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V4*/ meltfptr[3] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-debug.melt:611:/ quasiblock");


 /*_.DIS__V6*/ meltfptr[4] =
      ((melt_ptr_t)
       (melt_discr ((melt_ptr_t) ( /*_.SELF__V2*/ meltfptr[1]))));;
    MELT_LOCATION ("warmelt-debug.melt:612:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.DBGI__V3*/ meltfptr[2]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 0, "DBGI_OUT");
  /*_.SBUF__V7*/ meltfptr[6] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-debug.melt:614:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#__L3*/ meltfnum[1] =
      (( /*_.DIS__V6*/ meltfptr[4]) ==
       (( /*!DISCR_STRING */ meltfrout->tabval[1])));;
    MELT_LOCATION ("warmelt-debug.melt:614:/ cond");
    /*cond */ if ( /*_#__L3*/ meltfnum[1])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{


	  {
	    MELT_LOCATION ("warmelt-debug.melt:616:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V7*/ meltfptr[6]),
				 (" \""));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-debug.melt:617:/ locexp");
	    meltgc_add_strbuf_cstr ((melt_ptr_t)
				    ( /*_.SBUF__V7*/ meltfptr[6]),
				    melt_string_str ((melt_ptr_t)
						     ( /*_.SELF__V2*/
						      meltfptr[1])));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-debug.melt:618:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V7*/ meltfptr[6]),
				 ("\""));
	  }
	  ;
	  MELT_LOCATION ("warmelt-debug.melt:615:/ quasiblock");


	  /*epilog */
	}
	;
      }
    else
      {
	MELT_LOCATION ("warmelt-debug.melt:614:/ cond.else");

	/*^block */
	/*anyblock */
	{


	  {
	    MELT_LOCATION ("warmelt-debug.melt:621:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V7*/ meltfptr[6]),
				 (" |"));
	  }
	  ;
	  MELT_LOCATION ("warmelt-debug.melt:622:/ getslot");
	  {
	    melt_ptr_t slot = NULL, obj = NULL;
	    obj = (melt_ptr_t) ( /*_.DIS__V6*/ meltfptr[4]) /*=obj*/ ;
	    melt_object_get_field (slot, obj, 1, "NAMED_NAME");
    /*_.NAMED_NAME__V8*/ meltfptr[7] = slot;
	  };
	  ;

	  {
	    /*^locexp */
	    /*add2sbuf_string */
	      meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V7*/ meltfptr[6]),
				 melt_string_str ((melt_ptr_t)
						  ( /*_.NAMED_NAME__V8*/
						   meltfptr[7])));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-debug.melt:623:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V7*/ meltfptr[6]),
				 ("\""));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-debug.melt:624:/ locexp");
	    meltgc_add_strbuf_cstr ((melt_ptr_t)
				    ( /*_.SBUF__V7*/ meltfptr[6]),
				    melt_string_str ((melt_ptr_t)
						     ( /*_.SELF__V2*/
						      meltfptr[1])));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-debug.melt:625:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V7*/ meltfptr[6]),
				 ("\""));
	  }
	  ;
	  MELT_LOCATION ("warmelt-debug.melt:620:/ quasiblock");


	  /*epilog */

	  MELT_LOCATION ("warmelt-debug.melt:614:/ clear");
	     /*clear *//*_.NAMED_NAME__V8*/ meltfptr[7] = 0;
	}
	;
      }
    ;

    MELT_LOCATION ("warmelt-debug.melt:611:/ clear");
	   /*clear *//*_.DIS__V6*/ meltfptr[4] = 0;
    /*^clear */
	   /*clear *//*_.SBUF__V7*/ meltfptr[6] = 0;
    /*^clear */
	   /*clear *//*_#__L3*/ meltfnum[1] = 0;
    MELT_LOCATION ("warmelt-debug.melt:609:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V4*/ meltfptr[3] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("DBGOUT_STRING_METHOD", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_14_warmelt_debug_DBGOUT_STRING_METHOD_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*noretval */ NULL);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_14_warmelt_debug_DBGOUT_STRING_METHOD */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_15_warmelt_debug_DBGOUT_STRBUF_METHOD (meltclosure_ptr_t meltclosp_,
						melt_ptr_t meltfirstargp_,
						const melt_argdescr_cell_t
						meltxargdescr_[],
						union meltparam_un *
						meltxargtab_,
						const melt_argdescr_cell_t
						meltxresdescr_[],
						union meltparam_un *
						meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_15_warmelt_debug_DBGOUT_STRBUF_METHOD_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_15_warmelt_debug_DBGOUT_STRBUF_METHOD_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 12
    melt_ptr_t mcfr_varptr[12];
#define MELTFRAM_NBVARNUM 9
    long mcfr_varnum[9];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_15_warmelt_debug_DBGOUT_STRBUF_METHOD is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_15_warmelt_debug_DBGOUT_STRBUF_METHOD_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 12; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_15_warmelt_debug_DBGOUT_STRBUF_METHOD nbval 12*/
  meltfram__.mcfr_nbvar = 12 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("DBGOUT_STRBUF_METHOD", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-debug.melt:631:/ getarg");
 /*_.SELF__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.DBGI__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.DBGI__V3*/ meltfptr[2])) !=
	      NULL);


  /*getarg#2 */
  /*^getarg */
  if (meltxargdescr_[1] != MELTBPAR_LONG)
    goto lab_endgetargs;
 /*_#DEPTH__L1*/ meltfnum[0] = meltxargtab_[1].meltbp_long;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-debug.melt:632:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L2*/ meltfnum[1] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.DBGI__V3*/ meltfptr[2]),
			     (melt_ptr_t) (( /*!CLASS_DEBUG_INFORMATION */
					    meltfrout->tabval[0])));;
      MELT_LOCATION ("warmelt-debug.melt:632:/ cond");
      /*cond */ if ( /*_#IS_A__L2*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V5*/ meltfptr[4] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-debug.melt:632:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check dbgi"),
				  ("warmelt-debug.melt")
				  ? ("warmelt-debug.melt") : __FILE__,
				  (632) ? (632) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V5*/ meltfptr[4] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V4*/ meltfptr[3] = /*_.IFELSE___V5*/ meltfptr[4];;
      /*epilog */

      MELT_LOCATION ("warmelt-debug.melt:632:/ clear");
	     /*clear *//*_#IS_A__L2*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V5*/ meltfptr[4] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V4*/ meltfptr[3] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-debug.melt:633:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_STRBUF__L3*/ meltfnum[1] =
	(melt_magic_discr ((melt_ptr_t) ( /*_.SELF__V2*/ meltfptr[1])) ==
	 MELTOBMAG_STRBUF);;
      MELT_LOCATION ("warmelt-debug.melt:633:/ cond");
      /*cond */ if ( /*_#IS_STRBUF__L3*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V7*/ meltfptr[6] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-debug.melt:633:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check self"),
				  ("warmelt-debug.melt")
				  ? ("warmelt-debug.melt") : __FILE__,
				  (633) ? (633) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V7*/ meltfptr[6] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V6*/ meltfptr[4] = /*_.IFELSE___V7*/ meltfptr[6];;
      /*epilog */

      MELT_LOCATION ("warmelt-debug.melt:633:/ clear");
	     /*clear *//*_#IS_STRBUF__L3*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V7*/ meltfptr[6] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V6*/ meltfptr[4] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-debug.melt:634:/ quasiblock");


 /*_.DIS__V8*/ meltfptr[6] =
      ((melt_ptr_t)
       (melt_discr ((melt_ptr_t) ( /*_.SELF__V2*/ meltfptr[1]))));;
    MELT_LOCATION ("warmelt-debug.melt:635:/ cond");
    /*cond */ if (
		   /*ifisa */
		   melt_is_instance_of ((melt_ptr_t)
					( /*_.DIS__V8*/ meltfptr[6]),
					(melt_ptr_t) (( /*!CLASS_NAMED */
						       meltfrout->tabval[1])))
      )				/*then */
      {
	/*^cond.then */
	/*^getslot */
	{
	  melt_ptr_t slot = NULL, obj = NULL;
	  obj = (melt_ptr_t) ( /*_.DIS__V8*/ meltfptr[6]) /*=obj*/ ;
	  melt_object_get_field (slot, obj, 1, "NAMED_NAME");
   /*_.DISNAME__V9*/ meltfptr[8] = slot;
	};
	;
      }
    else
      {				/*^cond.else */

  /*_.DISNAME__V9*/ meltfptr[8] = NULL;;
      }
    ;
    MELT_LOCATION ("warmelt-debug.melt:636:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.DBGI__V3*/ meltfptr[2]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 0, "DBGI_OUT");
  /*_.SBUF__V10*/ meltfptr[9] = slot;
    };
    ;
 /*_#ULEN__L4*/ meltfnum[1] =
      melt_strbuf_usedlength ((melt_ptr_t) ( /*_.SELF__V2*/ meltfptr[1]));;
    MELT_LOCATION ("warmelt-debug.melt:639:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#I__L5*/ meltfnum[4] =
      (( /*_#ULEN__L4*/ meltfnum[1]) > (20));;
    MELT_LOCATION ("warmelt-debug.melt:639:/ cond");
    /*cond */ if ( /*_#I__L5*/ meltfnum[4])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{


	  {
	    /*^locexp */
	    meltgc_out_add_indent ((melt_ptr_t)
				   ( /*_.SBUF__V10*/ meltfptr[9]),
				   ( /*_#DEPTH__L1*/ meltfnum[0]), 64);;
	  }
	  ;
	  /*epilog */
	}
	;
      }				/*noelse */
    ;
    MELT_LOCATION ("warmelt-debug.melt:640:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[5];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_cstring = "strbuf.";
      /*^apply.arg */
      argtab[1].meltbp_aptr = (melt_ptr_t *) & /*_.DISNAME__V9*/ meltfptr[8];
      /*^apply.arg */
      argtab[2].meltbp_cstring = "[ulen=";
      /*^apply.arg */
      argtab[3].meltbp_long = /*_#ULEN__L4*/ meltfnum[1];
      /*^apply.arg */
      argtab[4].meltbp_cstring = "]<\"";
      /*_.ADD2OUT__V11*/ meltfptr[10] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!ADD2OUT */ meltfrout->tabval[2])),
		    (melt_ptr_t) ( /*_.SBUF__V10*/ meltfptr[9]),
		    (MELTBPARSTR_CSTRING MELTBPARSTR_PTR MELTBPARSTR_CSTRING
		     MELTBPARSTR_LONG MELTBPARSTR_CSTRING ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-debug.melt:641:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#I__L6*/ meltfnum[5] =
      (( /*_#DEPTH__L1*/ meltfnum[0]) <= (1));;
    MELT_LOCATION ("warmelt-debug.melt:641:/ cond");
    /*cond */ if ( /*_#I__L6*/ meltfnum[5])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-debug.melt:643:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
   /*_#I__L7*/ meltfnum[6] =
	    (( /*_#ULEN__L4*/ meltfnum[1]) < (4000));;
	  MELT_LOCATION ("warmelt-debug.melt:643:/ cond");
	  /*cond */ if ( /*_#I__L7*/ meltfnum[6])	/*then */
	    {
	      /*^cond.then */
	      /*^block */
	      /*anyblock */
	      {


		{
		  MELT_LOCATION ("warmelt-debug.melt:646:/ locexp");
		  /*dbgout_strbuf_method OUTALLSTRBUFCHK__1 */ meltgc_add_out_cstr_len
		    ((melt_ptr_t) /*_.SBUF__V10*/ meltfptr[9],
		     melt_strbuf_str ((melt_ptr_t) /*_.SELF__V2*/
				      meltfptr[1]),
		     (int) /*_#ULEN__L4*/ meltfnum[1]);
		  ;
		}
		;
		/*epilog */
	      }
	      ;
	    }
	  else
	    {
	      MELT_LOCATION ("warmelt-debug.melt:643:/ cond.else");

	      /*^block */
	      /*anyblock */
	      {


		{
		  MELT_LOCATION ("warmelt-debug.melt:655:/ locexp");
		  /* dbgout_strbuf_method OUTBIGSLICSTRBUFCHK__1 */
		  {
		    const int startslice = 1000;
		    const int endslice = 2500;
		    gcc_assert (startslice + endslice <
				(int) /*_#ULEN__L4*/ meltfnum[1] - 5);
		    meltgc_add_out_cstr_len ((melt_ptr_t) /*_.SBUF__V10*/
					     meltfptr[9],
					     melt_strbuf_str ((melt_ptr_t)
							      /*_.SELF__V2*/
							      meltfptr[1]),
					     startslice);
		    meltgc_out_printf
		      ((melt_ptr_t) /*_.SBUF__V10*/ meltfptr[9],
		       "\"\n ... %d bytes skipped\n ...\"",
		       (int) /*_#ULEN__L4*/ meltfnum[1] - (startslice +
							   endslice));
		    meltgc_add_out_cstr_len ((melt_ptr_t) /*_.SBUF__V10*/
					     meltfptr[9],
					     (melt_strbuf_str
					      ((melt_ptr_t) /*_.SELF__V2*/
					       meltfptr[1])) +
					     /*_#ULEN__L4*/ meltfnum[1] -
					     endslice,
					     endslice);;
		  }		/* end  dbgout_strbuf_method OUTBIGSLICSTRBUFCHK__1 */
		  ;
		}
		;
		MELT_LOCATION ("warmelt-debug.melt:652:/ quasiblock");


		/*epilog */
	      }
	      ;
	    }
	  ;
	  /*epilog */

	  MELT_LOCATION ("warmelt-debug.melt:641:/ clear");
	     /*clear *//*_#I__L7*/ meltfnum[6] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-debug.melt:676:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
   /*_#I__L8*/ meltfnum[6] =
	    (( /*_#ULEN__L4*/ meltfnum[1]) < (1000));;
	  MELT_LOCATION ("warmelt-debug.melt:676:/ cond");
	  /*cond */ if ( /*_#I__L8*/ meltfnum[6])	/*then */
	    {
	      /*^cond.then */
	      /*^block */
	      /*anyblock */
	      {


		{
		  MELT_LOCATION ("warmelt-debug.melt:679:/ locexp");
		  /* dbgout_strbuf_method OUTALLTINYSTRBUFCHK__1 */ meltgc_add_out_cstr_len
		    ((melt_ptr_t) /*_.SBUF__V10*/ meltfptr[9],
		     melt_strbuf_str ((melt_ptr_t) /*_.SELF__V2*/
				      meltfptr[1]),
		     (int) /*_#ULEN__L4*/ meltfnum[1]);
		  ;
		}
		;
		/*epilog */
	      }
	      ;
	    }
	  else
	    {
	      MELT_LOCATION ("warmelt-debug.melt:676:/ cond.else");

	      /*^block */
	      /*anyblock */
	      {


		{
		  MELT_LOCATION ("warmelt-debug.melt:689:/ locexp");
		  /*+ dbgout_strbuf_method OUTTINYSLICSTRBUFCHK__1 */
		  {
		    const int startslice = 100;
		    const int endslice = 550;
		    gcc_assert (startslice + endslice <
				(int) /*_#ULEN__L4*/ meltfnum[1] - 5);
		    meltgc_add_out_cstr_len ((melt_ptr_t) /*_.SBUF__V10*/
					     meltfptr[9],
					     melt_strbuf_str ((melt_ptr_t)
							      /*_.SELF__V2*/
							      meltfptr[1]),
					     startslice);
		    meltgc_out_printf
		      ((melt_ptr_t) /*_.SBUF__V10*/ meltfptr[9],
		       "\"\n ... %d bytes skipped\n ...\"",
		       (int) /*_#ULEN__L4*/ meltfnum[1] - (startslice +
							   endslice));
		    meltgc_add_out_cstr_len ((melt_ptr_t) /*_.SBUF__V10*/
					     meltfptr[9],
					     (melt_strbuf_str
					      ((melt_ptr_t) /*_.SELF__V2*/
					       meltfptr[1])) +
					     /*_#ULEN__L4*/ meltfnum[1] -
					     endslice,
					     endslice);;
		  }
	   /*-end  dbgout_strbuf_method OUTTINYSLICSTRBUFCHK__1*/
		  ;
		}
		;
		MELT_LOCATION ("warmelt-debug.melt:686:/ quasiblock");


		/*epilog */
	      }
	      ;
	    }
	  ;
	  /*epilog */

	  MELT_LOCATION ("warmelt-debug.melt:641:/ clear");
	     /*clear *//*_#I__L8*/ meltfnum[6] = 0;
	}
	;
      }
    ;
    MELT_LOCATION ("warmelt-debug.melt:708:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_cstring = "\">";
      /*_.ADD2OUT__V12*/ meltfptr[11] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!ADD2OUT */ meltfrout->tabval[2])),
		    (melt_ptr_t) ( /*_.SBUF__V10*/ meltfptr[9]),
		    (MELTBPARSTR_CSTRING ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-debug.melt:709:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#I__L9*/ meltfnum[6] =
      (( /*_#ULEN__L4*/ meltfnum[1]) > (60));;
    MELT_LOCATION ("warmelt-debug.melt:709:/ cond");
    /*cond */ if ( /*_#I__L9*/ meltfnum[6])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{


	  {
	    /*^locexp */
	    meltgc_out_add_indent ((melt_ptr_t)
				   ( /*_.SBUF__V10*/ meltfptr[9]),
				   ( /*_#DEPTH__L1*/ meltfnum[0]), 64);;
	  }
	  ;
	  /*epilog */
	}
	;
      }				/*noelse */
    ;

    MELT_LOCATION ("warmelt-debug.melt:634:/ clear");
	   /*clear *//*_.DIS__V8*/ meltfptr[6] = 0;
    /*^clear */
	   /*clear *//*_.DISNAME__V9*/ meltfptr[8] = 0;
    /*^clear */
	   /*clear *//*_.SBUF__V10*/ meltfptr[9] = 0;
    /*^clear */
	   /*clear *//*_#ULEN__L4*/ meltfnum[1] = 0;
    /*^clear */
	   /*clear *//*_#I__L5*/ meltfnum[4] = 0;
    /*^clear */
	   /*clear *//*_.ADD2OUT__V11*/ meltfptr[10] = 0;
    /*^clear */
	   /*clear *//*_#I__L6*/ meltfnum[5] = 0;
    /*^clear */
	   /*clear *//*_.ADD2OUT__V12*/ meltfptr[11] = 0;
    /*^clear */
	   /*clear *//*_#I__L9*/ meltfnum[6] = 0;
    MELT_LOCATION ("warmelt-debug.melt:631:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V4*/ meltfptr[3] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V6*/ meltfptr[4] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("DBGOUT_STRBUF_METHOD", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_15_warmelt_debug_DBGOUT_STRBUF_METHOD_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*noretval */ NULL);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_15_warmelt_debug_DBGOUT_STRBUF_METHOD */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_16_warmelt_debug_DBGOUT_INTEGER_METHOD (meltclosure_ptr_t meltclosp_,
						 melt_ptr_t meltfirstargp_,
						 const melt_argdescr_cell_t
						 meltxargdescr_[],
						 union meltparam_un *
						 meltxargtab_,
						 const melt_argdescr_cell_t
						 meltxresdescr_[],
						 union meltparam_un *
						 meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_16_warmelt_debug_DBGOUT_INTEGER_METHOD_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_16_warmelt_debug_DBGOUT_INTEGER_METHOD_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 12
    melt_ptr_t mcfr_varptr[12];
#define MELTFRAM_NBVARNUM 9
    long mcfr_varnum[9];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_16_warmelt_debug_DBGOUT_INTEGER_METHOD is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_16_warmelt_debug_DBGOUT_INTEGER_METHOD_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 12; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_16_warmelt_debug_DBGOUT_INTEGER_METHOD nbval 12*/
  meltfram__.mcfr_nbvar = 12 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("DBGOUT_INTEGER_METHOD", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-debug.melt:717:/ getarg");
 /*_.SELF__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.DBGI__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.DBGI__V3*/ meltfptr[2])) !=
	      NULL);


  /*getarg#2 */
  /*^getarg */
  if (meltxargdescr_[1] != MELTBPAR_LONG)
    goto lab_endgetargs;
 /*_#DEPTH__L1*/ meltfnum[0] = meltxargtab_[1].meltbp_long;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-debug.melt:718:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L2*/ meltfnum[1] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.DBGI__V3*/ meltfptr[2]),
			     (melt_ptr_t) (( /*!CLASS_DEBUG_INFORMATION */
					    meltfrout->tabval[0])));;
      MELT_LOCATION ("warmelt-debug.melt:718:/ cond");
      /*cond */ if ( /*_#IS_A__L2*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V5*/ meltfptr[4] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-debug.melt:718:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check dbgi"),
				  ("warmelt-debug.melt")
				  ? ("warmelt-debug.melt") : __FILE__,
				  (718) ? (718) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V5*/ meltfptr[4] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V4*/ meltfptr[3] = /*_.IFELSE___V5*/ meltfptr[4];;
      /*epilog */

      MELT_LOCATION ("warmelt-debug.melt:718:/ clear");
	     /*clear *//*_#IS_A__L2*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V5*/ meltfptr[4] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V4*/ meltfptr[3] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-debug.melt:719:/ quasiblock");


 /*_.DIS__V7*/ meltfptr[6] =
      ((melt_ptr_t)
       (melt_discr ((melt_ptr_t) ( /*_.SELF__V2*/ meltfptr[1]))));;
    MELT_LOCATION ("warmelt-debug.melt:720:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.DBGI__V3*/ meltfptr[2]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 0, "DBGI_OUT");
  /*_.SBUF__V8*/ meltfptr[7] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-debug.melt:723:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#__L3*/ meltfnum[1] =
      (( /*_.DIS__V7*/ meltfptr[6]) ==
       (( /*!DISCR_INTEGER */ meltfrout->tabval[1])));;
    MELT_LOCATION ("warmelt-debug.melt:723:/ cond");
    /*cond */ if ( /*_#__L3*/ meltfnum[1])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{


	  {
	    MELT_LOCATION ("warmelt-debug.melt:724:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V8*/ meltfptr[7]),
				 (" #"));
	  }
	  ;
   /*_#GET_INT__L4*/ meltfnum[3] =
	    (melt_get_int ((melt_ptr_t) ( /*_.SELF__V2*/ meltfptr[1])));;

	  {
	    MELT_LOCATION ("warmelt-debug.melt:725:/ locexp");
	    meltgc_add_strbuf_dec ((melt_ptr_t) ( /*_.SBUF__V8*/ meltfptr[7]),
				   ( /*_#GET_INT__L4*/ meltfnum[3]));
	  }
	  ;
	  MELT_LOCATION ("warmelt-debug.melt:723:/ quasiblock");


	  /*epilog */

	  /*^clear */
	     /*clear *//*_#GET_INT__L4*/ meltfnum[3] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-debug.melt:726:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^checksignal */
	  MELT_CHECK_SIGNAL ();
	  ;
   /*_#__L5*/ meltfnum[3] =
	    (( /*_.DIS__V7*/ meltfptr[6]) ==
	     (( /*!DISCR_CONSTANT_INTEGER */ meltfrout->tabval[2])));;
	  MELT_LOCATION ("warmelt-debug.melt:726:/ cond");
	  /*cond */ if ( /*_#__L5*/ meltfnum[3])	/*then */
	    {
	      /*^cond.then */
	      /*^block */
	      /*anyblock */
	      {

     /*_#I__L7*/ meltfnum[6] =
		  (( /*_#DEPTH__L1*/ meltfnum[0]) > (0));;
		/*^compute */
		/*_#IF___L6*/ meltfnum[5] = /*_#I__L7*/ meltfnum[6];;
		/*epilog */

		MELT_LOCATION ("warmelt-debug.melt:726:/ clear");
	       /*clear *//*_#I__L7*/ meltfnum[6] = 0;
	      }
	      ;
	    }
	  else
	    {			/*^cond.else */

    /*_#IF___L6*/ meltfnum[5] = 0;;
	    }
	  ;
	  MELT_LOCATION ("warmelt-debug.melt:726:/ cond");
	  /*cond */ if ( /*_#IF___L6*/ meltfnum[5])	/*then */
	    {
	      /*^cond.then */
	      /*^block */
	      /*anyblock */
	      {

     /*_#GET_INT__L8*/ meltfnum[6] =
		  (melt_get_int
		   ((melt_ptr_t) ( /*_.SELF__V2*/ meltfptr[1])));;
		MELT_LOCATION ("warmelt-debug.melt:727:/ checksignal");
		MELT_CHECK_SIGNAL ();
		;
		/*^apply */
		/*apply */
		{
		  union meltparam_un argtab[2];
		  memset (&argtab, 0, sizeof (argtab));
		  /*^apply.arg */
		  argtab[0].meltbp_cstring = " #\'";
		  /*^apply.arg */
		  argtab[1].meltbp_long = /*_#GET_INT__L8*/ meltfnum[6];
		  /*_.ADD2OUT__V11*/ meltfptr[10] =
		    melt_apply ((meltclosure_ptr_t)
				(( /*!ADD2OUT */ meltfrout->tabval[3])),
				(melt_ptr_t) ( /*_.SBUF__V8*/ meltfptr[7]),
				(MELTBPARSTR_CSTRING MELTBPARSTR_LONG ""),
				argtab, "", (union meltparam_un *) 0);
		}
		;
		/*_.IFELSE___V10*/ meltfptr[9] =
		  /*_.ADD2OUT__V11*/ meltfptr[10];;
		/*epilog */

		MELT_LOCATION ("warmelt-debug.melt:726:/ clear");
	       /*clear *//*_#GET_INT__L8*/ meltfnum[6] = 0;
		/*^clear */
	       /*clear *//*_.ADD2OUT__V11*/ meltfptr[10] = 0;
	      }
	      ;
	    }
	  else
	    {			/*^cond.else */

	      /*^block */
	      /*anyblock */
	      {


		{
		  MELT_LOCATION ("warmelt-debug.melt:729:/ locexp");
		  /*add2sbuf_strconst */
		    meltgc_add_strbuf ((melt_ptr_t)
				       ( /*_.SBUF__V8*/ meltfptr[7]), (" |"));
		}
		;
		MELT_LOCATION ("warmelt-debug.melt:730:/ getslot");
		{
		  melt_ptr_t slot = NULL, obj = NULL;
		  obj = (melt_ptr_t) ( /*_.DIS__V7*/ meltfptr[6]) /*=obj*/ ;
		  melt_object_get_field (slot, obj, 1, "NAMED_NAME");
      /*_.NAMED_NAME__V12*/ meltfptr[10] = slot;
		};
		;

		{
		  /*^locexp */
		  /*add2sbuf_string */
		    meltgc_add_strbuf ((melt_ptr_t)
				       ( /*_.SBUF__V8*/ meltfptr[7]),
				       melt_string_str ((melt_ptr_t)
							( /*_.NAMED_NAME__V12*/ meltfptr[10])));
		}
		;

		{
		  MELT_LOCATION ("warmelt-debug.melt:731:/ locexp");
		  /*add2sbuf_strconst */
		    meltgc_add_strbuf ((melt_ptr_t)
				       ( /*_.SBUF__V8*/ meltfptr[7]), ("#"));
		}
		;
     /*_#GET_INT__L9*/ meltfnum[6] =
		  (melt_get_int
		   ((melt_ptr_t) ( /*_.SELF__V2*/ meltfptr[1])));;

		{
		  MELT_LOCATION ("warmelt-debug.melt:732:/ locexp");
		  meltgc_add_strbuf_dec ((melt_ptr_t)
					 ( /*_.SBUF__V8*/ meltfptr[7]),
					 ( /*_#GET_INT__L9*/ meltfnum[6]));
		}
		;
		MELT_LOCATION ("warmelt-debug.melt:728:/ quasiblock");


		/*epilog */

		MELT_LOCATION ("warmelt-debug.melt:726:/ clear");
	       /*clear *//*_.NAMED_NAME__V12*/ meltfptr[10] = 0;
		/*^clear */
	       /*clear *//*_#GET_INT__L9*/ meltfnum[6] = 0;
	      }
	      ;
	    }
	  ;
	  /*_.IFELSE___V9*/ meltfptr[8] = /*_.IFELSE___V10*/ meltfptr[9];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-debug.melt:723:/ clear");
	     /*clear *//*_#__L5*/ meltfnum[3] = 0;
	  /*^clear */
	     /*clear *//*_#IF___L6*/ meltfnum[5] = 0;
	  /*^clear */
	     /*clear *//*_.IFELSE___V10*/ meltfptr[9] = 0;
	}
	;
      }
    ;
    /*_.LET___V6*/ meltfptr[4] = /*_.IFELSE___V9*/ meltfptr[8];;

    MELT_LOCATION ("warmelt-debug.melt:719:/ clear");
	   /*clear *//*_.DIS__V7*/ meltfptr[6] = 0;
    /*^clear */
	   /*clear *//*_.SBUF__V8*/ meltfptr[7] = 0;
    /*^clear */
	   /*clear *//*_#__L3*/ meltfnum[1] = 0;
    /*^clear */
	   /*clear *//*_.IFELSE___V9*/ meltfptr[8] = 0;
    MELT_LOCATION ("warmelt-debug.melt:717:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.LET___V6*/ meltfptr[4];;

    {
      MELT_LOCATION ("warmelt-debug.melt:717:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V4*/ meltfptr[3] = 0;
    /*^clear */
	   /*clear *//*_.LET___V6*/ meltfptr[4] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("DBGOUT_INTEGER_METHOD", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_16_warmelt_debug_DBGOUT_INTEGER_METHOD_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_16_warmelt_debug_DBGOUT_INTEGER_METHOD */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_17_warmelt_debug_DBGOUT_MIXINT_METHOD (meltclosure_ptr_t meltclosp_,
						melt_ptr_t meltfirstargp_,
						const melt_argdescr_cell_t
						meltxargdescr_[],
						union meltparam_un *
						meltxargtab_,
						const melt_argdescr_cell_t
						meltxresdescr_[],
						union meltparam_un *
						meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_17_warmelt_debug_DBGOUT_MIXINT_METHOD_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_17_warmelt_debug_DBGOUT_MIXINT_METHOD_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 13
    melt_ptr_t mcfr_varptr[13];
#define MELTFRAM_NBVARNUM 12
    long mcfr_varnum[12];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_17_warmelt_debug_DBGOUT_MIXINT_METHOD is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_17_warmelt_debug_DBGOUT_MIXINT_METHOD_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 13; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_17_warmelt_debug_DBGOUT_MIXINT_METHOD nbval 13*/
  meltfram__.mcfr_nbvar = 13 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("DBGOUT_MIXINT_METHOD", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-debug.melt:738:/ getarg");
 /*_.SELF__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.DBGI__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.DBGI__V3*/ meltfptr[2])) !=
	      NULL);


  /*getarg#2 */
  /*^getarg */
  if (meltxargdescr_[1] != MELTBPAR_LONG)
    goto lab_endgetargs;
 /*_#DEPTH__L1*/ meltfnum[0] = meltxargtab_[1].meltbp_long;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-debug.melt:739:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L2*/ meltfnum[1] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.DBGI__V3*/ meltfptr[2]),
			     (melt_ptr_t) (( /*!CLASS_DEBUG_INFORMATION */
					    meltfrout->tabval[0])));;
      MELT_LOCATION ("warmelt-debug.melt:739:/ cond");
      /*cond */ if ( /*_#IS_A__L2*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V5*/ meltfptr[4] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-debug.melt:739:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check dbgi"),
				  ("warmelt-debug.melt")
				  ? ("warmelt-debug.melt") : __FILE__,
				  (739) ? (739) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V5*/ meltfptr[4] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V4*/ meltfptr[3] = /*_.IFELSE___V5*/ meltfptr[4];;
      /*epilog */

      MELT_LOCATION ("warmelt-debug.melt:739:/ clear");
	     /*clear *//*_#IS_A__L2*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V5*/ meltfptr[4] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V4*/ meltfptr[3] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-debug.melt:740:/ quasiblock");


 /*_.DIS__V6*/ meltfptr[4] =
      ((melt_ptr_t)
       (melt_discr ((melt_ptr_t) ( /*_.SELF__V2*/ meltfptr[1]))));;
    MELT_LOCATION ("warmelt-debug.melt:741:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.DBGI__V3*/ meltfptr[2]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 0, "DBGI_OUT");
  /*_.SBUF__V7*/ meltfptr[6] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-debug.melt:742:/ cond");
    /*cond */ if (
		   /*ifisa */
		   melt_is_instance_of ((melt_ptr_t)
					( /*_.DBGI__V3*/ meltfptr[2]),
					(melt_ptr_t) (( /*!CLASS_DEBUG_INFORMATION */ meltfrout->tabval[0])))
      )				/*then */
      {
	/*^cond.then */
	/*^getslot */
	{
	  melt_ptr_t slot = NULL, obj = NULL;
	  obj = (melt_ptr_t) ( /*_.DBGI__V3*/ meltfptr[2]) /*=obj*/ ;
	  melt_object_get_field (slot, obj, 2, "DBGI_MAXDEPTH");
   /*_.DBGI_MAXDEPTH__V8*/ meltfptr[7] = slot;
	};
	;
      }
    else
      {				/*^cond.else */

  /*_.DBGI_MAXDEPTH__V8*/ meltfptr[7] = NULL;;
      }
    ;
    /*^compute */
 /*_#MAXDEPTH__L3*/ meltfnum[1] =
      (melt_get_int ((melt_ptr_t) ( /*_.DBGI_MAXDEPTH__V8*/ meltfptr[7])));;
    MELT_LOCATION ("warmelt-debug.melt:744:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#__L4*/ meltfnum[3] =
      (( /*_.DIS__V6*/ meltfptr[4]) ==
       (( /*!DISCR_MIXED_INTEGER */ meltfrout->tabval[1])));;
    MELT_LOCATION ("warmelt-debug.melt:744:/ cond");
    /*cond */ if ( /*_#__L4*/ meltfnum[3])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{


	  {
	    MELT_LOCATION ("warmelt-debug.melt:746:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V7*/ meltfptr[6]),
				 (" #["));
	  }
	  ;
   /*_#GET_INT__L5*/ meltfnum[4] =
	    (melt_get_int ((melt_ptr_t) ( /*_.SELF__V2*/ meltfptr[1])));;

	  {
	    MELT_LOCATION ("warmelt-debug.melt:747:/ locexp");
	    meltgc_add_strbuf_dec ((melt_ptr_t) ( /*_.SBUF__V7*/ meltfptr[6]),
				   ( /*_#GET_INT__L5*/ meltfnum[4]));
	  }
	  ;
	  MELT_LOCATION ("warmelt-debug.melt:745:/ quasiblock");


	  /*epilog */

	  MELT_LOCATION ("warmelt-debug.melt:744:/ clear");
	     /*clear *//*_#GET_INT__L5*/ meltfnum[4] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

	/*^block */
	/*anyblock */
	{


	  {
	    MELT_LOCATION ("warmelt-debug.melt:749:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V7*/ meltfptr[6]),
				 (" |"));
	  }
	  ;
	  MELT_LOCATION ("warmelt-debug.melt:750:/ getslot");
	  {
	    melt_ptr_t slot = NULL, obj = NULL;
	    obj = (melt_ptr_t) ( /*_.DIS__V6*/ meltfptr[4]) /*=obj*/ ;
	    melt_object_get_field (slot, obj, 1, "NAMED_NAME");
    /*_.NAMED_NAME__V9*/ meltfptr[8] = slot;
	  };
	  ;

	  {
	    /*^locexp */
	    /*add2sbuf_string */
	      meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V7*/ meltfptr[6]),
				 melt_string_str ((melt_ptr_t)
						  ( /*_.NAMED_NAME__V9*/
						   meltfptr[8])));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-debug.melt:751:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V7*/ meltfptr[6]),
				 ("#["));
	  }
	  ;
   /*_#GET_INT__L6*/ meltfnum[4] =
	    (melt_get_int ((melt_ptr_t) ( /*_.SELF__V2*/ meltfptr[1])));;

	  {
	    MELT_LOCATION ("warmelt-debug.melt:752:/ locexp");
	    meltgc_add_strbuf_dec ((melt_ptr_t) ( /*_.SBUF__V7*/ meltfptr[6]),
				   ( /*_#GET_INT__L6*/ meltfnum[4]));
	  }
	  ;
	  MELT_LOCATION ("warmelt-debug.melt:748:/ quasiblock");


	  /*epilog */

	  MELT_LOCATION ("warmelt-debug.melt:744:/ clear");
	     /*clear *//*_.NAMED_NAME__V9*/ meltfptr[8] = 0;
	  /*^clear */
	     /*clear *//*_#GET_INT__L6*/ meltfnum[4] = 0;
	}
	;
      }
    ;
    MELT_LOCATION ("warmelt-debug.melt:754:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#MELT_REALLY_NEED_DBGLIM__L7*/ meltfnum[4] =
      ( /*MELT_REALLY_NEED_DBGLIM */
       melt_need_debug_limit ((int) /*_#DEPTH__L1*/ meltfnum[0],
			      (int) /*_#MAXDEPTH__L3*/ meltfnum[1]));;
    MELT_LOCATION ("warmelt-debug.melt:754:/ cond");
    /*cond */ if ( /*_#MELT_REALLY_NEED_DBGLIM__L7*/ meltfnum[4])	/*then */
      {
	/*^cond.then */
	/*_#OR___L8*/ meltfnum[7] =
	  /*_#MELT_REALLY_NEED_DBGLIM__L7*/ meltfnum[4];;
      }
    else
      {
	MELT_LOCATION ("warmelt-debug.melt:754:/ cond.else");

	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-debug.melt:756:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
   /*_#I__L9*/ meltfnum[8] =
	    (( /*_#DEPTH__L1*/ meltfnum[0]) <
	     ( /*_#MAXDEPTH__L3*/ meltfnum[1]));;
	  MELT_LOCATION ("warmelt-debug.melt:756:/ cond");
	  /*cond */ if ( /*_#I__L9*/ meltfnum[8])	/*then */
	    {
	      /*^cond.then */
	      /*^block */
	      /*anyblock */
	      {

     /*_#IS_A__L11*/ meltfnum[10] =
		  melt_is_instance_of ((melt_ptr_t)
				       ( /*_.DBGI__V3*/ meltfptr[2]),
				       (melt_ptr_t) (( /*!CLASS_DEBUG_OUTPUT_INFORMATION */ meltfrout->tabval[2])));;
		/*^compute */
		/*_#IF___L10*/ meltfnum[9] = /*_#IS_A__L11*/ meltfnum[10];;
		/*epilog */

		MELT_LOCATION ("warmelt-debug.melt:756:/ clear");
	       /*clear *//*_#IS_A__L11*/ meltfnum[10] = 0;
	      }
	      ;
	    }
	  else
	    {			/*^cond.else */

    /*_#IF___L10*/ meltfnum[9] = 0;;
	    }
	  ;
	  /*^compute */
	  /*_#OR___L8*/ meltfnum[7] = /*_#IF___L10*/ meltfnum[9];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-debug.melt:754:/ clear");
	     /*clear *//*_#I__L9*/ meltfnum[8] = 0;
	  /*^clear */
	     /*clear *//*_#IF___L10*/ meltfnum[9] = 0;
	}
	;
      }
    ;
    /*^cond */
    /*cond */ if ( /*_#OR___L8*/ meltfnum[7])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{


	  {
	    MELT_LOCATION ("warmelt-debug.melt:758:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V7*/ meltfptr[6]),
				 (","));
	  }
	  ;
   /*_.MIXINT_VAL__V11*/ meltfptr[10] =
	    (melt_val_mixint ((melt_ptr_t) ( /*_.SELF__V2*/ meltfptr[1])));;
	  /*^compute */
   /*_#I__L12*/ meltfnum[10] =
	    (( /*_#DEPTH__L1*/ meltfnum[0]) + (1));;
	  MELT_LOCATION ("warmelt-debug.melt:759:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^apply */
	  /*apply */
	  {
	    union meltparam_un argtab[2];
	    memset (&argtab, 0, sizeof (argtab));
	    /*^apply.arg */
	    argtab[0].meltbp_aptr =
	      (melt_ptr_t *) & /*_.DBGI__V3*/ meltfptr[2];
	    /*^apply.arg */
	    argtab[1].meltbp_long = /*_#I__L12*/ meltfnum[10];
	    /*_.DBG_OUT__V12*/ meltfptr[11] =
	      melt_apply ((meltclosure_ptr_t)
			  (( /*!DBG_OUT */ meltfrout->tabval[3])),
			  (melt_ptr_t) ( /*_.MIXINT_VAL__V11*/ meltfptr[10]),
			  (MELTBPARSTR_PTR MELTBPARSTR_LONG ""), argtab, "",
			  (union meltparam_un *) 0);
	  }
	  ;
	  MELT_LOCATION ("warmelt-debug.melt:757:/ quasiblock");


	  /*_.PROGN___V13*/ meltfptr[12] = /*_.DBG_OUT__V12*/ meltfptr[11];;
	  /*^compute */
	  /*_.IFELSE___V10*/ meltfptr[8] = /*_.PROGN___V13*/ meltfptr[12];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-debug.melt:754:/ clear");
	     /*clear *//*_.MIXINT_VAL__V11*/ meltfptr[10] = 0;
	  /*^clear */
	     /*clear *//*_#I__L12*/ meltfnum[10] = 0;
	  /*^clear */
	     /*clear *//*_.DBG_OUT__V12*/ meltfptr[11] = 0;
	  /*^clear */
	     /*clear *//*_.PROGN___V13*/ meltfptr[12] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

	/*^block */
	/*anyblock */
	{




	  {
	    MELT_LOCATION ("warmelt-debug.melt:761:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V7*/ meltfptr[6]),
				 (",.."));
	  }
	  ;
	     /*clear *//*_.IFELSE___V10*/ meltfptr[8] = 0;
	  /*epilog */
	}
	;
      }
    ;

    {
      MELT_LOCATION ("warmelt-debug.melt:763:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V7*/ meltfptr[6]), ("]"));
    }
    ;

    MELT_LOCATION ("warmelt-debug.melt:740:/ clear");
	   /*clear *//*_.DIS__V6*/ meltfptr[4] = 0;
    /*^clear */
	   /*clear *//*_.SBUF__V7*/ meltfptr[6] = 0;
    /*^clear */
	   /*clear *//*_.DBGI_MAXDEPTH__V8*/ meltfptr[7] = 0;
    /*^clear */
	   /*clear *//*_#MAXDEPTH__L3*/ meltfnum[1] = 0;
    /*^clear */
	   /*clear *//*_#__L4*/ meltfnum[3] = 0;
    /*^clear */
	   /*clear *//*_#MELT_REALLY_NEED_DBGLIM__L7*/ meltfnum[4] = 0;
    /*^clear */
	   /*clear *//*_#OR___L8*/ meltfnum[7] = 0;
    /*^clear */
	   /*clear *//*_.IFELSE___V10*/ meltfptr[8] = 0;
    MELT_LOCATION ("warmelt-debug.melt:738:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V4*/ meltfptr[3] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("DBGOUT_MIXINT_METHOD", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_17_warmelt_debug_DBGOUT_MIXINT_METHOD_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*noretval */ NULL);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_17_warmelt_debug_DBGOUT_MIXINT_METHOD */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_18_warmelt_debug_DBGOUT_MIXLOC_METHOD (meltclosure_ptr_t meltclosp_,
						melt_ptr_t meltfirstargp_,
						const melt_argdescr_cell_t
						meltxargdescr_[],
						union meltparam_un *
						meltxargtab_,
						const melt_argdescr_cell_t
						meltxresdescr_[],
						union meltparam_un *
						meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_18_warmelt_debug_DBGOUT_MIXLOC_METHOD_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_18_warmelt_debug_DBGOUT_MIXLOC_METHOD_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 11
    melt_ptr_t mcfr_varptr[11];
#define MELTFRAM_NBVARNUM 12
    long mcfr_varnum[12];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_18_warmelt_debug_DBGOUT_MIXLOC_METHOD is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_18_warmelt_debug_DBGOUT_MIXLOC_METHOD_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 11; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_18_warmelt_debug_DBGOUT_MIXLOC_METHOD nbval 11*/
  meltfram__.mcfr_nbvar = 11 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("DBGOUT_MIXLOC_METHOD", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-debug.melt:768:/ getarg");
 /*_.SELF__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.DBGI__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.DBGI__V3*/ meltfptr[2])) !=
	      NULL);


  /*getarg#2 */
  /*^getarg */
  if (meltxargdescr_[1] != MELTBPAR_LONG)
    goto lab_endgetargs;
 /*_#DEPTH__L1*/ meltfnum[0] = meltxargtab_[1].meltbp_long;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-debug.melt:769:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L2*/ meltfnum[1] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.DBGI__V3*/ meltfptr[2]),
			     (melt_ptr_t) (( /*!CLASS_DEBUG_INFORMATION */
					    meltfrout->tabval[0])));;
      MELT_LOCATION ("warmelt-debug.melt:769:/ cond");
      /*cond */ if ( /*_#IS_A__L2*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V5*/ meltfptr[4] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-debug.melt:769:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check dbgi"),
				  ("warmelt-debug.melt")
				  ? ("warmelt-debug.melt") : __FILE__,
				  (769) ? (769) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V5*/ meltfptr[4] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V4*/ meltfptr[3] = /*_.IFELSE___V5*/ meltfptr[4];;
      /*epilog */

      MELT_LOCATION ("warmelt-debug.melt:769:/ clear");
	     /*clear *//*_#IS_A__L2*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V5*/ meltfptr[4] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V4*/ meltfptr[3] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-debug.melt:770:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_MIXLOC__L3*/ meltfnum[1] =
	(melt_magic_discr ((melt_ptr_t) ( /*_.SELF__V2*/ meltfptr[1])) ==
	 MELTOBMAG_MIXLOC);;
      MELT_LOCATION ("warmelt-debug.melt:770:/ cond");
      /*cond */ if ( /*_#IS_MIXLOC__L3*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V7*/ meltfptr[6] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-debug.melt:770:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check self mixloc"),
				  ("warmelt-debug.melt")
				  ? ("warmelt-debug.melt") : __FILE__,
				  (770) ? (770) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V7*/ meltfptr[6] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V6*/ meltfptr[4] = /*_.IFELSE___V7*/ meltfptr[6];;
      /*epilog */

      MELT_LOCATION ("warmelt-debug.melt:770:/ clear");
	     /*clear *//*_#IS_MIXLOC__L3*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V7*/ meltfptr[6] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V6*/ meltfptr[4] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-debug.melt:771:/ quasiblock");


 /*_.DIS__V8*/ meltfptr[6] =
      ((melt_ptr_t)
       (melt_discr ((melt_ptr_t) ( /*_.SELF__V2*/ meltfptr[1]))));;
    MELT_LOCATION ("warmelt-debug.melt:772:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.DBGI__V3*/ meltfptr[2]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 0, "DBGI_OUT");
  /*_.SBUF__V9*/ meltfptr[8] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-debug.melt:773:/ cond");
    /*cond */ if (
		   /*ifisa */
		   melt_is_instance_of ((melt_ptr_t)
					( /*_.DBGI__V3*/ meltfptr[2]),
					(melt_ptr_t) (( /*!CLASS_DEBUG_INFORMATION */ meltfrout->tabval[0])))
      )				/*then */
      {
	/*^cond.then */
	/*^getslot */
	{
	  melt_ptr_t slot = NULL, obj = NULL;
	  obj = (melt_ptr_t) ( /*_.DBGI__V3*/ meltfptr[2]) /*=obj*/ ;
	  melt_object_get_field (slot, obj, 2, "DBGI_MAXDEPTH");
   /*_.DBGI_MAXDEPTH__V10*/ meltfptr[9] = slot;
	};
	;
      }
    else
      {				/*^cond.else */

  /*_.DBGI_MAXDEPTH__V10*/ meltfptr[9] = NULL;;
      }
    ;
    /*^compute */
 /*_#MAXDEPTH__L4*/ meltfnum[1] =
      (melt_get_int ((melt_ptr_t) ( /*_.DBGI_MAXDEPTH__V10*/ meltfptr[9])));;
    MELT_LOCATION ("warmelt-debug.melt:775:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#__L5*/ meltfnum[4] =
      (( /*_.DIS__V8*/ meltfptr[6]) ==
       (( /*!DISCR_MIXED_LOCATION */ meltfrout->tabval[1])));;
    MELT_LOCATION ("warmelt-debug.melt:775:/ cond");
    /*cond */ if ( /*_#__L5*/ meltfnum[4])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{


	  {
	    MELT_LOCATION ("warmelt-debug.melt:777:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V9*/ meltfptr[8]),
				 (" #!["));
	  }
	  ;
   /*_#GET_INT__L6*/ meltfnum[5] =
	    (melt_get_int ((melt_ptr_t) ( /*_.SELF__V2*/ meltfptr[1])));;

	  {
	    MELT_LOCATION ("warmelt-debug.melt:778:/ locexp");
	    meltgc_add_strbuf_dec ((melt_ptr_t) ( /*_.SBUF__V9*/ meltfptr[8]),
				   ( /*_#GET_INT__L6*/ meltfnum[5]));
	  }
	  ;
	  MELT_LOCATION ("warmelt-debug.melt:776:/ quasiblock");


	  /*epilog */

	  MELT_LOCATION ("warmelt-debug.melt:775:/ clear");
	     /*clear *//*_#GET_INT__L6*/ meltfnum[5] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

	/*^block */
	/*anyblock */
	{


	  {
	    MELT_LOCATION ("warmelt-debug.melt:780:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V9*/ meltfptr[8]),
				 (" |"));
	  }
	  ;
	  MELT_LOCATION ("warmelt-debug.melt:781:/ getslot");
	  {
	    melt_ptr_t slot = NULL, obj = NULL;
	    obj = (melt_ptr_t) ( /*_.DIS__V8*/ meltfptr[6]) /*=obj*/ ;
	    melt_object_get_field (slot, obj, 1, "NAMED_NAME");
    /*_.NAMED_NAME__V11*/ meltfptr[10] = slot;
	  };
	  ;

	  {
	    /*^locexp */
	    /*add2sbuf_string */
	      meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V9*/ meltfptr[8]),
				 melt_string_str ((melt_ptr_t)
						  ( /*_.NAMED_NAME__V11*/
						   meltfptr[10])));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-debug.melt:782:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V9*/ meltfptr[8]),
				 ("#!["));
	  }
	  ;
   /*_#GET_INT__L7*/ meltfnum[5] =
	    (melt_get_int ((melt_ptr_t) ( /*_.SELF__V2*/ meltfptr[1])));;

	  {
	    MELT_LOCATION ("warmelt-debug.melt:783:/ locexp");
	    meltgc_add_strbuf_dec ((melt_ptr_t) ( /*_.SBUF__V9*/ meltfptr[8]),
				   ( /*_#GET_INT__L7*/ meltfnum[5]));
	  }
	  ;
	  MELT_LOCATION ("warmelt-debug.melt:779:/ quasiblock");


	  /*epilog */

	  MELT_LOCATION ("warmelt-debug.melt:775:/ clear");
	     /*clear *//*_.NAMED_NAME__V11*/ meltfptr[10] = 0;
	  /*^clear */
	     /*clear *//*_#GET_INT__L7*/ meltfnum[5] = 0;
	}
	;
      }
    ;
    MELT_LOCATION ("warmelt-debug.melt:785:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#MELT_REALLY_NEED_DBGLIM__L8*/ meltfnum[5] =
      ( /*MELT_REALLY_NEED_DBGLIM */
       melt_need_debug_limit ((int) /*_#DEPTH__L1*/ meltfnum[0],
			      (int) /*_#MAXDEPTH__L4*/ meltfnum[1]));;
    MELT_LOCATION ("warmelt-debug.melt:785:/ cond");
    /*cond */ if ( /*_#MELT_REALLY_NEED_DBGLIM__L8*/ meltfnum[5])	/*then */
      {
	/*^cond.then */
	/*_#OR___L9*/ meltfnum[8] =
	  /*_#MELT_REALLY_NEED_DBGLIM__L8*/ meltfnum[5];;
      }
    else
      {
	MELT_LOCATION ("warmelt-debug.melt:785:/ cond.else");

	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-debug.melt:787:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
   /*_#I__L10*/ meltfnum[9] =
	    (( /*_#DEPTH__L1*/ meltfnum[0]) <
	     ( /*_#MAXDEPTH__L4*/ meltfnum[1]));;
	  MELT_LOCATION ("warmelt-debug.melt:787:/ cond");
	  /*cond */ if ( /*_#I__L10*/ meltfnum[9])	/*then */
	    {
	      /*^cond.then */
	      /*^block */
	      /*anyblock */
	      {

     /*_#IS_A__L12*/ meltfnum[11] =
		  melt_is_instance_of ((melt_ptr_t)
				       ( /*_.DBGI__V3*/ meltfptr[2]),
				       (melt_ptr_t) (( /*!CLASS_DEBUG_OUTPUT_INFORMATION */ meltfrout->tabval[2])));;
		/*^compute */
		/*_#IF___L11*/ meltfnum[10] = /*_#IS_A__L12*/ meltfnum[11];;
		/*epilog */

		MELT_LOCATION ("warmelt-debug.melt:787:/ clear");
	       /*clear *//*_#IS_A__L12*/ meltfnum[11] = 0;
	      }
	      ;
	    }
	  else
	    {			/*^cond.else */

    /*_#IF___L11*/ meltfnum[10] = 0;;
	    }
	  ;
	  /*^compute */
	  /*_#OR___L9*/ meltfnum[8] = /*_#IF___L11*/ meltfnum[10];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-debug.melt:785:/ clear");
	     /*clear *//*_#I__L10*/ meltfnum[9] = 0;
	  /*^clear */
	     /*clear *//*_#IF___L11*/ meltfnum[10] = 0;
	}
	;
      }
    ;
    /*^cond */
    /*cond */ if ( /*_#OR___L9*/ meltfnum[8])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{


	  {
	    MELT_LOCATION ("warmelt-debug.melt:789:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V9*/ meltfptr[8]),
				 (","));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-debug.melt:790:/ locexp");
	    /*add2sbufmixloc */
	      if (melt_magic_discr
		  ((melt_ptr_t) ( /*_.SELF__V2*/ meltfptr[1])) ==
		  MELTOBMAG_MIXLOC)
	      {
		char smallcbuf[128];
		location_t loc =
		  melt_location_mixloc ((melt_ptr_t) /*_.SELF__V2*/
					meltfptr[1]);
		memset (smallcbuf, 0, sizeof (smallcbuf));
		snprintf (smallcbuf, sizeof (smallcbuf) - 1,
			  "{%.120s:%d:%d}",
			  LOCATION_FILE (loc), LOCATION_LINE (loc),
			  LOCATION_COLUMN (loc));
		meltgc_add_strbuf_raw ((melt_ptr_t)
				       ( /*_.SBUF__V9*/ meltfptr[8]),
				       smallcbuf);
	      } /*end add2sbufmixloc */ ;
	  }
	  ;
	  MELT_LOCATION ("warmelt-debug.melt:788:/ quasiblock");


	  /*epilog */
	}
	;
      }
    else
      {
	MELT_LOCATION ("warmelt-debug.melt:785:/ cond.else");

	/*^block */
	/*anyblock */
	{


	  {
	    MELT_LOCATION ("warmelt-debug.melt:792:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V9*/ meltfptr[8]),
				 (",.."));
	  }
	  ;
	  /*epilog */
	}
	;
      }
    ;

    {
      MELT_LOCATION ("warmelt-debug.melt:794:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V9*/ meltfptr[8]), ("]"));
    }
    ;

    MELT_LOCATION ("warmelt-debug.melt:771:/ clear");
	   /*clear *//*_.DIS__V8*/ meltfptr[6] = 0;
    /*^clear */
	   /*clear *//*_.SBUF__V9*/ meltfptr[8] = 0;
    /*^clear */
	   /*clear *//*_.DBGI_MAXDEPTH__V10*/ meltfptr[9] = 0;
    /*^clear */
	   /*clear *//*_#MAXDEPTH__L4*/ meltfnum[1] = 0;
    /*^clear */
	   /*clear *//*_#__L5*/ meltfnum[4] = 0;
    /*^clear */
	   /*clear *//*_#MELT_REALLY_NEED_DBGLIM__L8*/ meltfnum[5] = 0;
    /*^clear */
	   /*clear *//*_#OR___L9*/ meltfnum[8] = 0;
    MELT_LOCATION ("warmelt-debug.melt:768:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V4*/ meltfptr[3] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V6*/ meltfptr[4] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("DBGOUT_MIXLOC_METHOD", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_18_warmelt_debug_DBGOUT_MIXLOC_METHOD_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*noretval */ NULL);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_18_warmelt_debug_DBGOUT_MIXLOC_METHOD */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_19_warmelt_debug_DBGOUT_MIXBIGINT_METHOD (meltclosure_ptr_t
						   meltclosp_,
						   melt_ptr_t meltfirstargp_,
						   const melt_argdescr_cell_t
						   meltxargdescr_[],
						   union meltparam_un *
						   meltxargtab_,
						   const melt_argdescr_cell_t
						   meltxresdescr_[],
						   union meltparam_un *
						   meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_19_warmelt_debug_DBGOUT_MIXBIGINT_METHOD_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_19_warmelt_debug_DBGOUT_MIXBIGINT_METHOD_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 13
    melt_ptr_t mcfr_varptr[13];
#define MELTFRAM_NBVARNUM 11
    long mcfr_varnum[11];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_19_warmelt_debug_DBGOUT_MIXBIGINT_METHOD is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_19_warmelt_debug_DBGOUT_MIXBIGINT_METHOD_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 13; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_19_warmelt_debug_DBGOUT_MIXBIGINT_METHOD nbval 13*/
  meltfram__.mcfr_nbvar = 13 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("DBGOUT_MIXBIGINT_METHOD", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-debug.melt:800:/ getarg");
 /*_.SELF__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.DBGI__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.DBGI__V3*/ meltfptr[2])) !=
	      NULL);


  /*getarg#2 */
  /*^getarg */
  if (meltxargdescr_[1] != MELTBPAR_LONG)
    goto lab_endgetargs;
 /*_#DEPTH__L1*/ meltfnum[0] = meltxargtab_[1].meltbp_long;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-debug.melt:801:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L2*/ meltfnum[1] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.DBGI__V3*/ meltfptr[2]),
			     (melt_ptr_t) (( /*!CLASS_DEBUG_INFORMATION */
					    meltfrout->tabval[0])));;
      MELT_LOCATION ("warmelt-debug.melt:801:/ cond");
      /*cond */ if ( /*_#IS_A__L2*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V5*/ meltfptr[4] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-debug.melt:801:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check dbgi"),
				  ("warmelt-debug.melt")
				  ? ("warmelt-debug.melt") : __FILE__,
				  (801) ? (801) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V5*/ meltfptr[4] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V4*/ meltfptr[3] = /*_.IFELSE___V5*/ meltfptr[4];;
      /*epilog */

      MELT_LOCATION ("warmelt-debug.melt:801:/ clear");
	     /*clear *//*_#IS_A__L2*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V5*/ meltfptr[4] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V4*/ meltfptr[3] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-debug.melt:802:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_MIXBIGINT__L3*/ meltfnum[1] =
	(melt_magic_discr ((melt_ptr_t) ( /*_.SELF__V2*/ meltfptr[1])) ==
	 MELTOBMAG_MIXBIGINT);;
      MELT_LOCATION ("warmelt-debug.melt:802:/ cond");
      /*cond */ if ( /*_#IS_MIXBIGINT__L3*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V7*/ meltfptr[6] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-debug.melt:802:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check self mixbigint"),
				  ("warmelt-debug.melt")
				  ? ("warmelt-debug.melt") : __FILE__,
				  (802) ? (802) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V7*/ meltfptr[6] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V6*/ meltfptr[4] = /*_.IFELSE___V7*/ meltfptr[6];;
      /*epilog */

      MELT_LOCATION ("warmelt-debug.melt:802:/ clear");
	     /*clear *//*_#IS_MIXBIGINT__L3*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V7*/ meltfptr[6] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V6*/ meltfptr[4] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-debug.melt:803:/ quasiblock");


 /*_.DIS__V8*/ meltfptr[6] =
      ((melt_ptr_t)
       (melt_discr ((melt_ptr_t) ( /*_.SELF__V2*/ meltfptr[1]))));;
    MELT_LOCATION ("warmelt-debug.melt:804:/ cond");
    /*cond */ if (
		   /*ifisa */
		   melt_is_instance_of ((melt_ptr_t)
					( /*_.DBGI__V3*/ meltfptr[2]),
					(melt_ptr_t) (( /*!CLASS_DEBUG_INFORMATION */ meltfrout->tabval[0])))
      )				/*then */
      {
	/*^cond.then */
	/*^getslot */
	{
	  melt_ptr_t slot = NULL, obj = NULL;
	  obj = (melt_ptr_t) ( /*_.DBGI__V3*/ meltfptr[2]) /*=obj*/ ;
	  melt_object_get_field (slot, obj, 0, "DBGI_OUT");
   /*_.SBUF__V9*/ meltfptr[8] = slot;
	};
	;
      }
    else
      {				/*^cond.else */

  /*_.SBUF__V9*/ meltfptr[8] = NULL;;
      }
    ;
    MELT_LOCATION ("warmelt-debug.melt:805:/ cond");
    /*cond */ if (
		   /*ifisa */
		   melt_is_instance_of ((melt_ptr_t)
					( /*_.DBGI__V3*/ meltfptr[2]),
					(melt_ptr_t) (( /*!CLASS_DEBUG_INFORMATION */ meltfrout->tabval[0])))
      )				/*then */
      {
	/*^cond.then */
	/*^getslot */
	{
	  melt_ptr_t slot = NULL, obj = NULL;
	  obj = (melt_ptr_t) ( /*_.DBGI__V3*/ meltfptr[2]) /*=obj*/ ;
	  melt_object_get_field (slot, obj, 2, "DBGI_MAXDEPTH");
   /*_.DBGI_MAXDEPTH__V10*/ meltfptr[9] = slot;
	};
	;
      }
    else
      {				/*^cond.else */

  /*_.DBGI_MAXDEPTH__V10*/ meltfptr[9] = NULL;;
      }
    ;
    /*^compute */
 /*_#MAXDEPTH__L4*/ meltfnum[1] =
      (melt_get_int ((melt_ptr_t) ( /*_.DBGI_MAXDEPTH__V10*/ meltfptr[9])));;

    {
      MELT_LOCATION ("warmelt-debug.melt:807:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V9*/ meltfptr[8]),
			   (" |"));
    }
    ;
    MELT_LOCATION ("warmelt-debug.melt:808:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.DIS__V8*/ meltfptr[6]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 1, "NAMED_NAME");
  /*_.NAMED_NAME__V11*/ meltfptr[10] = slot;
    };
    ;

    {
      /*^locexp */
      /*add2sbuf_string */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V9*/ meltfptr[8]),
			   melt_string_str ((melt_ptr_t)
					    ( /*_.NAMED_NAME__V11*/
					     meltfptr[10])));
    }
    ;

    {
      MELT_LOCATION ("warmelt-debug.melt:809:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V9*/ meltfptr[8]),
			   ("#!["));
    }
    ;
    MELT_LOCATION ("warmelt-debug.melt:810:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#MELT_REALLY_NEED_DBGLIM__L5*/ meltfnum[4] =
      ( /*MELT_REALLY_NEED_DBGLIM */
       melt_need_debug_limit ((int) /*_#DEPTH__L1*/ meltfnum[0],
			      (int) /*_#MAXDEPTH__L4*/ meltfnum[1]));;
    MELT_LOCATION ("warmelt-debug.melt:810:/ cond");
    /*cond */ if ( /*_#MELT_REALLY_NEED_DBGLIM__L5*/ meltfnum[4])	/*then */
      {
	/*^cond.then */
	/*_#OR___L6*/ meltfnum[5] =
	  /*_#MELT_REALLY_NEED_DBGLIM__L5*/ meltfnum[4];;
      }
    else
      {
	MELT_LOCATION ("warmelt-debug.melt:810:/ cond.else");

	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-debug.melt:812:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
   /*_#I__L7*/ meltfnum[6] =
	    (( /*_#DEPTH__L1*/ meltfnum[0]) <
	     ( /*_#MAXDEPTH__L4*/ meltfnum[1]));;
	  MELT_LOCATION ("warmelt-debug.melt:812:/ cond");
	  /*cond */ if ( /*_#I__L7*/ meltfnum[6])	/*then */
	    {
	      /*^cond.then */
	      /*^block */
	      /*anyblock */
	      {

     /*_#IS_A__L9*/ meltfnum[8] =
		  melt_is_instance_of ((melt_ptr_t)
				       ( /*_.DBGI__V3*/ meltfptr[2]),
				       (melt_ptr_t) (( /*!CLASS_DEBUG_OUTPUT_INFORMATION */ meltfrout->tabval[1])));;
		/*^compute */
		/*_#IF___L8*/ meltfnum[7] = /*_#IS_A__L9*/ meltfnum[8];;
		/*epilog */

		MELT_LOCATION ("warmelt-debug.melt:812:/ clear");
	       /*clear *//*_#IS_A__L9*/ meltfnum[8] = 0;
	      }
	      ;
	    }
	  else
	    {			/*^cond.else */

    /*_#IF___L8*/ meltfnum[7] = 0;;
	    }
	  ;
	  /*^compute */
	  /*_#OR___L6*/ meltfnum[5] = /*_#IF___L8*/ meltfnum[7];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-debug.melt:810:/ clear");
	     /*clear *//*_#I__L7*/ meltfnum[6] = 0;
	  /*^clear */
	     /*clear *//*_#IF___L8*/ meltfnum[7] = 0;
	}
	;
      }
    ;
    /*^cond */
    /*cond */ if ( /*_#OR___L6*/ meltfnum[5])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

   /*_.MIXBIGINT_VAL__V12*/ meltfptr[11] =
	    melt_val_mixbigint ((melt_ptr_t) /*_.SELF__V2*/ meltfptr[1]);;
	  /*^compute */
   /*_#I__L10*/ meltfnum[8] =
	    (( /*_#DEPTH__L1*/ meltfnum[0]) + (1));;
	  MELT_LOCATION ("warmelt-debug.melt:814:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^apply */
	  /*apply */
	  {
	    union meltparam_un argtab[2];
	    memset (&argtab, 0, sizeof (argtab));
	    /*^apply.arg */
	    argtab[0].meltbp_aptr =
	      (melt_ptr_t *) & /*_.DBGI__V3*/ meltfptr[2];
	    /*^apply.arg */
	    argtab[1].meltbp_long = /*_#I__L10*/ meltfnum[8];
	    /*_.DBG_OUT__V13*/ meltfptr[12] =
	      melt_apply ((meltclosure_ptr_t)
			  (( /*!DBG_OUT */ meltfrout->tabval[2])),
			  (melt_ptr_t) ( /*_.MIXBIGINT_VAL__V12*/
					meltfptr[11]),
			  (MELTBPARSTR_PTR MELTBPARSTR_LONG ""), argtab, "",
			  (union meltparam_un *) 0);
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-debug.melt:815:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V9*/ meltfptr[8]),
				 (","));
	  }
	  ;
   /*_#I__L11*/ meltfnum[6] =
	    (( /*_#DEPTH__L1*/ meltfnum[0]) + (1));;

	  {
	    MELT_LOCATION ("warmelt-debug.melt:816:/ locexp");
	    meltgc_ppstrbuf_mixbigint ((melt_ptr_t) /*_.SBUF__V9*/
				       meltfptr[8], /*_#I__L11*/ meltfnum[6],
				       (melt_ptr_t) /*_.SELF__V2*/
				       meltfptr[1]);;
	  }
	  ;
	  MELT_LOCATION ("warmelt-debug.melt:813:/ quasiblock");


	  /*epilog */

	  MELT_LOCATION ("warmelt-debug.melt:810:/ clear");
	     /*clear *//*_.MIXBIGINT_VAL__V12*/ meltfptr[11] = 0;
	  /*^clear */
	     /*clear *//*_#I__L10*/ meltfnum[8] = 0;
	  /*^clear */
	     /*clear *//*_.DBG_OUT__V13*/ meltfptr[12] = 0;
	  /*^clear */
	     /*clear *//*_#I__L11*/ meltfnum[6] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

	/*^block */
	/*anyblock */
	{


	  {
	    MELT_LOCATION ("warmelt-debug.melt:818:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V9*/ meltfptr[8]),
				 (",.."));
	  }
	  ;
	  /*epilog */
	}
	;
      }
    ;

    {
      MELT_LOCATION ("warmelt-debug.melt:820:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V9*/ meltfptr[8]), ("]"));
    }
    ;

    MELT_LOCATION ("warmelt-debug.melt:803:/ clear");
	   /*clear *//*_.DIS__V8*/ meltfptr[6] = 0;
    /*^clear */
	   /*clear *//*_.SBUF__V9*/ meltfptr[8] = 0;
    /*^clear */
	   /*clear *//*_.DBGI_MAXDEPTH__V10*/ meltfptr[9] = 0;
    /*^clear */
	   /*clear *//*_#MAXDEPTH__L4*/ meltfnum[1] = 0;
    /*^clear */
	   /*clear *//*_.NAMED_NAME__V11*/ meltfptr[10] = 0;
    /*^clear */
	   /*clear *//*_#MELT_REALLY_NEED_DBGLIM__L5*/ meltfnum[4] = 0;
    /*^clear */
	   /*clear *//*_#OR___L6*/ meltfnum[5] = 0;
    MELT_LOCATION ("warmelt-debug.melt:800:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V4*/ meltfptr[3] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V6*/ meltfptr[4] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("DBGOUT_MIXBIGINT_METHOD", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_19_warmelt_debug_DBGOUT_MIXBIGINT_METHOD_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*noretval */ NULL);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_19_warmelt_debug_DBGOUT_MIXBIGINT_METHOD */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_20_warmelt_debug_DBGOUT_MULTIPLE_METHOD (meltclosure_ptr_t
						  meltclosp_,
						  melt_ptr_t meltfirstargp_,
						  const melt_argdescr_cell_t
						  meltxargdescr_[],
						  union meltparam_un *
						  meltxargtab_,
						  const melt_argdescr_cell_t
						  meltxresdescr_[],
						  union meltparam_un *
						  meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_20_warmelt_debug_DBGOUT_MULTIPLE_METHOD_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_20_warmelt_debug_DBGOUT_MULTIPLE_METHOD_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 10
    melt_ptr_t mcfr_varptr[10];
#define MELTFRAM_NBVARNUM 16
    long mcfr_varnum[16];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_20_warmelt_debug_DBGOUT_MULTIPLE_METHOD is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_20_warmelt_debug_DBGOUT_MULTIPLE_METHOD_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 10; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_20_warmelt_debug_DBGOUT_MULTIPLE_METHOD nbval 10*/
  meltfram__.mcfr_nbvar = 10 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("DBGOUT_MULTIPLE_METHOD", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-debug.melt:825:/ getarg");
 /*_.SELF__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.DBGI__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.DBGI__V3*/ meltfptr[2])) !=
	      NULL);


  /*getarg#2 */
  /*^getarg */
  if (meltxargdescr_[1] != MELTBPAR_LONG)
    goto lab_endgetargs;
 /*_#DEPTH__L1*/ meltfnum[0] = meltxargtab_[1].meltbp_long;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-debug.melt:826:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L2*/ meltfnum[1] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.DBGI__V3*/ meltfptr[2]),
			     (melt_ptr_t) (( /*!CLASS_DEBUG_INFORMATION */
					    meltfrout->tabval[0])));;
      MELT_LOCATION ("warmelt-debug.melt:826:/ cond");
      /*cond */ if ( /*_#IS_A__L2*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V5*/ meltfptr[4] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-debug.melt:826:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check dbgi"),
				  ("warmelt-debug.melt")
				  ? ("warmelt-debug.melt") : __FILE__,
				  (826) ? (826) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V5*/ meltfptr[4] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V4*/ meltfptr[3] = /*_.IFELSE___V5*/ meltfptr[4];;
      /*epilog */

      MELT_LOCATION ("warmelt-debug.melt:826:/ clear");
	     /*clear *//*_#IS_A__L2*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V5*/ meltfptr[4] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V4*/ meltfptr[3] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-debug.melt:827:/ quasiblock");


 /*_.DIS__V6*/ meltfptr[4] =
      ((melt_ptr_t)
       (melt_discr ((melt_ptr_t) ( /*_.SELF__V2*/ meltfptr[1]))));;
    MELT_LOCATION ("warmelt-debug.melt:828:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.DBGI__V3*/ meltfptr[2]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 0, "DBGI_OUT");
  /*_.SBUF__V7*/ meltfptr[6] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-debug.melt:830:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#__L3*/ meltfnum[1] =
      (( /*_.DIS__V6*/ meltfptr[4]) ==
       (( /*!DISCR_MULTIPLE */ meltfrout->tabval[1])));;
    MELT_LOCATION ("warmelt-debug.melt:830:/ cond");
    /*cond */ if ( /*_#__L3*/ meltfnum[1])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{


	  {
	    MELT_LOCATION ("warmelt-debug.melt:831:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V7*/ meltfptr[6]),
				 (" *"));
	  }
	  ;
	  /*epilog */
	}
	;
      }
    else
      {
	MELT_LOCATION ("warmelt-debug.melt:830:/ cond.else");

	/*^block */
	/*anyblock */
	{


	  {
	    MELT_LOCATION ("warmelt-debug.melt:833:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V7*/ meltfptr[6]),
				 (" |"));
	  }
	  ;
	  MELT_LOCATION ("warmelt-debug.melt:834:/ getslot");
	  {
	    melt_ptr_t slot = NULL, obj = NULL;
	    obj = (melt_ptr_t) ( /*_.DIS__V6*/ meltfptr[4]) /*=obj*/ ;
	    melt_object_get_field (slot, obj, 1, "NAMED_NAME");
    /*_.NAMED_NAME__V8*/ meltfptr[7] = slot;
	  };
	  ;

	  {
	    /*^locexp */
	    /*add2sbuf_string */
	      meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V7*/ meltfptr[6]),
				 melt_string_str ((melt_ptr_t)
						  ( /*_.NAMED_NAME__V8*/
						   meltfptr[7])));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-debug.melt:835:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V7*/ meltfptr[6]),
				 ("*"));
	  }
	  ;
	  MELT_LOCATION ("warmelt-debug.melt:832:/ quasiblock");


	  /*epilog */

	  MELT_LOCATION ("warmelt-debug.melt:830:/ clear");
	     /*clear *//*_.NAMED_NAME__V8*/ meltfptr[7] = 0;
	}
	;
      }
    ;
    MELT_LOCATION ("warmelt-debug.melt:836:/ quasiblock");


 /*_#LN__L4*/ meltfnum[3] =
      (melt_multiple_length ((melt_ptr_t) ( /*_.SELF__V2*/ meltfptr[1])));;

    {
      MELT_LOCATION ("warmelt-debug.melt:837:/ locexp");
      meltgc_add_strbuf_dec ((melt_ptr_t) ( /*_.SBUF__V7*/ meltfptr[6]),
			     ( /*_#LN__L4*/ meltfnum[3]));
    }
    ;

    {
      MELT_LOCATION ("warmelt-debug.melt:838:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V7*/ meltfptr[6]), ("["));
    }
    ;
    MELT_LOCATION ("warmelt-debug.melt:839:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#I__L5*/ meltfnum[4] =
      (( /*_#DEPTH__L1*/ meltfnum[0]) <= (0));;
    MELT_LOCATION ("warmelt-debug.melt:839:/ cond");
    /*cond */ if ( /*_#I__L5*/ meltfnum[4])	/*then */
      {
	/*^cond.then */
	/*_#OR___L6*/ meltfnum[5] = /*_#I__L5*/ meltfnum[4];;
      }
    else
      {
	MELT_LOCATION ("warmelt-debug.melt:839:/ cond.else");

	/*^block */
	/*anyblock */
	{

   /*_#MELT_REALLY_NEED_DEBUG__L7*/ meltfnum[6] =
	    /*MELT_REALLY_NEED_DEBUG */
	    (melt_need_debug ((int) /*_#DEPTH__L1*/ meltfnum[0]));;
	  /*^compute */
	  /*_#OR___L6*/ meltfnum[5] =
	    /*_#MELT_REALLY_NEED_DEBUG__L7*/ meltfnum[6];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-debug.melt:839:/ clear");
	     /*clear *//*_#MELT_REALLY_NEED_DEBUG__L7*/ meltfnum[6] = 0;
	}
	;
      }
    ;
    /*^cond */
    /*cond */ if ( /*_#OR___L6*/ meltfnum[5])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  /*citerblock FOREACH_IN_MULTIPLE */
	  {
	    /* start foreach_in_multiple meltcit1__EACHTUP */
	    long meltcit1__EACHTUP_ln =
	      melt_multiple_length ((melt_ptr_t) /*_.SELF__V2*/ meltfptr[1]);
	    for ( /*_#IX__L8*/ meltfnum[6] = 0;
		 ( /*_#IX__L8*/ meltfnum[6] >= 0)
		 && ( /*_#IX__L8*/ meltfnum[6] < meltcit1__EACHTUP_ln);
	/*_#IX__L8*/ meltfnum[6]++)
	      {
		/*_.CURCOMP__V9*/ meltfptr[7] =
		  melt_multiple_nth ((melt_ptr_t)
				     ( /*_.SELF__V2*/ meltfptr[1]),
				     /*_#IX__L8*/ meltfnum[6]);




		{
		  MELT_LOCATION ("warmelt-debug.melt:843:/ locexp");
		  meltgc_strbuf_add_indent ((melt_ptr_t)
					    ( /*_.SBUF__V7*/ meltfptr[6]),
					    ( /*_#DEPTH__L1*/ meltfnum[0]),
					    64);
		}
		;
		MELT_LOCATION ("warmelt-debug.melt:844:/ quasiblock");


    /*_#CURULEN__L9*/ meltfnum[8] =
		  melt_strbuf_usedlength ((melt_ptr_t)
					  ( /*_.SBUF__V7*/ meltfptr[6]));;
		/*^compute */
    /*_#I__L10*/ meltfnum[9] =
		  ((1) + ( /*_#DEPTH__L1*/ meltfnum[0]));;
		MELT_LOCATION ("warmelt-debug.melt:845:/ checksignal");
		MELT_CHECK_SIGNAL ();
		;
		/*^apply */
		/*apply */
		{
		  union meltparam_un argtab[2];
		  memset (&argtab, 0, sizeof (argtab));
		  /*^apply.arg */
		  argtab[0].meltbp_aptr =
		    (melt_ptr_t *) & /*_.DBGI__V3*/ meltfptr[2];
		  /*^apply.arg */
		  argtab[1].meltbp_long = /*_#I__L10*/ meltfnum[9];
		  /*_.DBG_OUT__V10*/ meltfptr[9] =
		    melt_apply ((meltclosure_ptr_t)
				(( /*!DBG_OUT */ meltfrout->tabval[2])),
				(melt_ptr_t) ( /*_.CURCOMP__V9*/ meltfptr[7]),
				(MELTBPARSTR_PTR MELTBPARSTR_LONG ""), argtab,
				"", (union meltparam_un *) 0);
		}
		;
		MELT_LOCATION ("warmelt-debug.melt:846:/ checksignal");
		MELT_CHECK_SIGNAL ();
		;
    /*_#STRBUF_USEDLENGTH__L11*/ meltfnum[10] =
		  melt_strbuf_usedlength ((melt_ptr_t)
					  ( /*_.SBUF__V7*/ meltfptr[6]));;
		/*^compute */
    /*_#I__L12*/ meltfnum[11] =
		  (( /*_#STRBUF_USEDLENGTH__L11*/ meltfnum[10]) -
		   ( /*_#CURULEN__L9*/ meltfnum[8]));;
		/*^compute */
    /*_#I__L13*/ meltfnum[12] =
		  (( /*_#I__L12*/ meltfnum[11]) > (100));;
		MELT_LOCATION ("warmelt-debug.melt:846:/ cond");
		/*cond */ if ( /*_#I__L13*/ meltfnum[12])	/*then */
		  {
		    /*^cond.then */
		    /*^block */
		    /*anyblock */
		    {

		      /*^checksignal */
		      MELT_CHECK_SIGNAL ();
		      ;
      /*_#I__L14*/ meltfnum[13] =
			(( /*_#LN__L4*/ meltfnum[3]) - (1));;
		      /*^compute */
      /*_#I__L15*/ meltfnum[14] =
			(( /*_#IX__L8*/ meltfnum[6]) <
			 ( /*_#I__L14*/ meltfnum[13]));;
		      MELT_LOCATION ("warmelt-debug.melt:846:/ cond");
		      /*cond */ if ( /*_#I__L15*/ meltfnum[14])	/*then */
			{
			  /*^cond.then */
			  /*^block */
			  /*anyblock */
			  {

	/*_#I__L16*/ meltfnum[15] =
			      ((1) + ( /*_#DEPTH__L1*/ meltfnum[0]));;

			    {
			      MELT_LOCATION
				("warmelt-debug.melt:848:/ locexp");
			      meltgc_strbuf_add_indent ((melt_ptr_t)
							( /*_.SBUF__V7*/
							 meltfptr[6]),
							( /*_#I__L16*/
							 meltfnum[15]), 0);
			    }
			    ;
			    /*epilog */

			    MELT_LOCATION ("warmelt-debug.melt:846:/ clear");
		  /*clear *//*_#I__L16*/ meltfnum[15] = 0;
			  }
			  ;
			}	/*noelse */
		      ;
		      /*epilog */

		      /*^clear */
		/*clear *//*_#I__L14*/ meltfnum[13] = 0;
		      /*^clear */
		/*clear *//*_#I__L15*/ meltfnum[14] = 0;
		    }
		    ;
		  }		/*noelse */
		;

		MELT_LOCATION ("warmelt-debug.melt:844:/ clear");
	      /*clear *//*_#CURULEN__L9*/ meltfnum[8] = 0;
		/*^clear */
	      /*clear *//*_#I__L10*/ meltfnum[9] = 0;
		/*^clear */
	      /*clear *//*_.DBG_OUT__V10*/ meltfptr[9] = 0;
		/*^clear */
	      /*clear *//*_#STRBUF_USEDLENGTH__L11*/ meltfnum[10] = 0;
		/*^clear */
	      /*clear *//*_#I__L12*/ meltfnum[11] = 0;
		/*^clear */
	      /*clear *//*_#I__L13*/ meltfnum[12] = 0;
		if ( /*_#IX__L8*/ meltfnum[6] < 0)
		  break;
	      }			/* end  foreach_in_multiple meltcit1__EACHTUP */

	    /*citerepilog */

	    MELT_LOCATION ("warmelt-debug.melt:840:/ clear");
	      /*clear *//*_.CURCOMP__V9*/ meltfptr[7] = 0;
	    /*^clear */
	      /*clear *//*_#IX__L8*/ meltfnum[6] = 0;
	  }			/*endciterblock FOREACH_IN_MULTIPLE */
	  ;
	  /*epilog */
	}
	;
      }
    else
      {
	MELT_LOCATION ("warmelt-debug.melt:839:/ cond.else");

	/*^block */
	/*anyblock */
	{


	  {
	    MELT_LOCATION ("warmelt-debug.melt:850:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V7*/ meltfptr[6]),
				 (".."));
	  }
	  ;
	  /*epilog */
	}
	;
      }
    ;

    {
      MELT_LOCATION ("warmelt-debug.melt:852:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V7*/ meltfptr[6]), ("]"));
    }
    ;

    MELT_LOCATION ("warmelt-debug.melt:836:/ clear");
	   /*clear *//*_#LN__L4*/ meltfnum[3] = 0;
    /*^clear */
	   /*clear *//*_#I__L5*/ meltfnum[4] = 0;
    /*^clear */
	   /*clear *//*_#OR___L6*/ meltfnum[5] = 0;

    MELT_LOCATION ("warmelt-debug.melt:827:/ clear");
	   /*clear *//*_.DIS__V6*/ meltfptr[4] = 0;
    /*^clear */
	   /*clear *//*_.SBUF__V7*/ meltfptr[6] = 0;
    /*^clear */
	   /*clear *//*_#__L3*/ meltfnum[1] = 0;
    MELT_LOCATION ("warmelt-debug.melt:825:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V4*/ meltfptr[3] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("DBGOUT_MULTIPLE_METHOD", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_20_warmelt_debug_DBGOUT_MULTIPLE_METHOD_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*noretval */ NULL);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_20_warmelt_debug_DBGOUT_MULTIPLE_METHOD */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_21_warmelt_debug_DBGOUT_ROUTINE_METHOD (meltclosure_ptr_t meltclosp_,
						 melt_ptr_t meltfirstargp_,
						 const melt_argdescr_cell_t
						 meltxargdescr_[],
						 union meltparam_un *
						 meltxargtab_,
						 const melt_argdescr_cell_t
						 meltxresdescr_[],
						 union meltparam_un *
						 meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_21_warmelt_debug_DBGOUT_ROUTINE_METHOD_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_21_warmelt_debug_DBGOUT_ROUTINE_METHOD_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 13
    melt_ptr_t mcfr_varptr[13];
#define MELTFRAM_NBVARNUM 5
    long mcfr_varnum[5];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_21_warmelt_debug_DBGOUT_ROUTINE_METHOD is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_21_warmelt_debug_DBGOUT_ROUTINE_METHOD_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 13; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_21_warmelt_debug_DBGOUT_ROUTINE_METHOD nbval 13*/
  meltfram__.mcfr_nbvar = 13 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("DBGOUT_ROUTINE_METHOD", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-debug.melt:858:/ getarg");
 /*_.SELF__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.DBGI__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.DBGI__V3*/ meltfptr[2])) !=
	      NULL);


  /*getarg#2 */
  /*^getarg */
  if (meltxargdescr_[1] != MELTBPAR_LONG)
    goto lab_endgetargs;
 /*_#DEPTH__L1*/ meltfnum[0] = meltxargtab_[1].meltbp_long;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-debug.melt:859:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L2*/ meltfnum[1] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.DBGI__V3*/ meltfptr[2]),
			     (melt_ptr_t) (( /*!CLASS_DEBUG_INFORMATION */
					    meltfrout->tabval[0])));;
      MELT_LOCATION ("warmelt-debug.melt:859:/ cond");
      /*cond */ if ( /*_#IS_A__L2*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V5*/ meltfptr[4] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-debug.melt:859:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check dbgi"),
				  ("warmelt-debug.melt")
				  ? ("warmelt-debug.melt") : __FILE__,
				  (859) ? (859) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V5*/ meltfptr[4] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V4*/ meltfptr[3] = /*_.IFELSE___V5*/ meltfptr[4];;
      /*epilog */

      MELT_LOCATION ("warmelt-debug.melt:859:/ clear");
	     /*clear *//*_#IS_A__L2*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V5*/ meltfptr[4] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V4*/ meltfptr[3] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-debug.melt:860:/ quasiblock");


 /*_.DIS__V6*/ meltfptr[4] =
      ((melt_ptr_t)
       (melt_discr ((melt_ptr_t) ( /*_.SELF__V2*/ meltfptr[1]))));;
    MELT_LOCATION ("warmelt-debug.melt:861:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.DBGI__V3*/ meltfptr[2]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 0, "DBGI_OUT");
  /*_.SBUF__V7*/ meltfptr[6] = slot;
    };
    ;
 /*_#I__L3*/ meltfnum[1] =
      (( /*_#DEPTH__L1*/ meltfnum[0]) + (1));;
    /*^compute */
 /*_.BOXDEPTHP1__V8*/ meltfptr[7] =
      (meltgc_new_int
       ((meltobject_ptr_t) (( /*!DISCR_INTEGER */ meltfrout->tabval[1])),
	( /*_#I__L3*/ meltfnum[1])));;
    MELT_LOCATION ("warmelt-debug.melt:864:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#__L4*/ meltfnum[3] =
      (( /*_.DIS__V6*/ meltfptr[4]) ==
       (( /*!DISCR_ROUTINE */ meltfrout->tabval[2])));;
    MELT_LOCATION ("warmelt-debug.melt:864:/ cond");
    /*cond */ if ( /*_#__L4*/ meltfnum[3])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{


	  {
	    MELT_LOCATION ("warmelt-debug.melt:865:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V7*/ meltfptr[6]),
				 (" *rou[%"));
	  }
	  ;
	  /*epilog */
	}
	;
      }
    else
      {
	MELT_LOCATION ("warmelt-debug.melt:864:/ cond.else");

	/*^block */
	/*anyblock */
	{


	  {
	    MELT_LOCATION ("warmelt-debug.melt:867:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V7*/ meltfptr[6]),
				 (" |"));
	  }
	  ;
	  MELT_LOCATION ("warmelt-debug.melt:868:/ getslot");
	  {
	    melt_ptr_t slot = NULL, obj = NULL;
	    obj = (melt_ptr_t) ( /*_.DIS__V6*/ meltfptr[4]) /*=obj*/ ;
	    melt_object_get_field (slot, obj, 1, "NAMED_NAME");
    /*_.NAMED_NAME__V9*/ meltfptr[8] = slot;
	  };
	  ;

	  {
	    /*^locexp */
	    /*add2sbuf_string */
	      meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V7*/ meltfptr[6]),
				 melt_string_str ((melt_ptr_t)
						  ( /*_.NAMED_NAME__V9*/
						   meltfptr[8])));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-debug.melt:869:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V7*/ meltfptr[6]),
				 ("[%"));
	  }
	  ;
	  MELT_LOCATION ("warmelt-debug.melt:866:/ quasiblock");


	  /*epilog */

	  MELT_LOCATION ("warmelt-debug.melt:864:/ clear");
	     /*clear *//*_.NAMED_NAME__V9*/ meltfptr[8] = 0;
	}
	;
      }
    ;

    {
      MELT_LOCATION ("warmelt-debug.melt:870:/ locexp");
      meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V7*/ meltfptr[6]),
			 melt_routine_descrstr ((melt_ptr_t)
						( /*_.SELF__V2*/
						 meltfptr[1])));
    }
    ;
    MELT_LOCATION ("warmelt-debug.melt:871:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#I__L5*/ meltfnum[4] =
      (( /*_#DEPTH__L1*/ meltfnum[0]) < (2));;
    MELT_LOCATION ("warmelt-debug.melt:871:/ cond");
    /*cond */ if ( /*_#I__L5*/ meltfnum[4])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-debug.melt:873:/ quasiblock");


	  /*^newclosure */
		   /*newclosure *//*_.LAMBDA___V12*/ meltfptr[11] =
	    (melt_ptr_t)
	    meltgc_new_closure ((meltobject_ptr_t)
				(((melt_ptr_t)
				  (MELT_PREDEF (DISCR_CLOSURE)))),
				(meltroutine_ptr_t) (( /*!konst_5 */
						      meltfrout->tabval[5])),
				(3));
	  ;
	  /*^putclosedv */
	  /*putclosv */
	  melt_assertmsg ("putclosv checkclo",
			  melt_magic_discr ((melt_ptr_t)
					    ( /*_.LAMBDA___V12*/
					     meltfptr[11])) ==
			  MELTOBMAG_CLOSURE);
	  melt_assertmsg ("putclosv checkoff", 0 >= 0
			  && 0 <
			  melt_closure_size ((melt_ptr_t)
					     ( /*_.LAMBDA___V12*/
					      meltfptr[11])));
	  ((meltclosure_ptr_t) /*_.LAMBDA___V12*/ meltfptr[11])->tabval[0] =
	    (melt_ptr_t) ( /*_.SBUF__V7*/ meltfptr[6]);
	  ;
	  /*^putclosedv */
	  /*putclosv */
	  melt_assertmsg ("putclosv checkclo",
			  melt_magic_discr ((melt_ptr_t)
					    ( /*_.LAMBDA___V12*/
					     meltfptr[11])) ==
			  MELTOBMAG_CLOSURE);
	  melt_assertmsg ("putclosv checkoff", 1 >= 0
			  && 1 <
			  melt_closure_size ((melt_ptr_t)
					     ( /*_.LAMBDA___V12*/
					      meltfptr[11])));
	  ((meltclosure_ptr_t) /*_.LAMBDA___V12*/ meltfptr[11])->tabval[1] =
	    (melt_ptr_t) ( /*_.BOXDEPTHP1__V8*/ meltfptr[7]);
	  ;
	  /*^putclosedv */
	  /*putclosv */
	  melt_assertmsg ("putclosv checkclo",
			  melt_magic_discr ((melt_ptr_t)
					    ( /*_.LAMBDA___V12*/
					     meltfptr[11])) ==
			  MELTOBMAG_CLOSURE);
	  melt_assertmsg ("putclosv checkoff", 2 >= 0
			  && 2 <
			  melt_closure_size ((melt_ptr_t)
					     ( /*_.LAMBDA___V12*/
					      meltfptr[11])));
	  ((meltclosure_ptr_t) /*_.LAMBDA___V12*/ meltfptr[11])->tabval[2] =
	    (melt_ptr_t) ( /*_.DBGI__V3*/ meltfptr[2]);
	  ;
	  /*_.LAMBDA___V11*/ meltfptr[10] = /*_.LAMBDA___V12*/ meltfptr[11];;
	  MELT_LOCATION ("warmelt-debug.melt:872:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^apply */
	  /*apply */
	  {
	    union meltparam_un argtab[1];
	    memset (&argtab, 0, sizeof (argtab));
	    /*^apply.arg */
	    argtab[0].meltbp_aptr =
	      (melt_ptr_t *) & /*_.LAMBDA___V11*/ meltfptr[10];
	    /*_.ROUTINE_EVERY__V13*/ meltfptr[12] =
	      melt_apply ((meltclosure_ptr_t)
			  (( /*!ROUTINE_EVERY */ meltfrout->tabval[3])),
			  (melt_ptr_t) ( /*_.SELF__V2*/ meltfptr[1]),
			  (MELTBPARSTR_PTR ""), argtab, "",
			  (union meltparam_un *) 0);
	  }
	  ;
	  /*_.IF___V10*/ meltfptr[8] = /*_.ROUTINE_EVERY__V13*/ meltfptr[12];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-debug.melt:871:/ clear");
	     /*clear *//*_.LAMBDA___V11*/ meltfptr[10] = 0;
	  /*^clear */
	     /*clear *//*_.ROUTINE_EVERY__V13*/ meltfptr[12] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

  /*_.IF___V10*/ meltfptr[8] = NULL;;
      }
    ;

    {
      MELT_LOCATION ("warmelt-debug.melt:878:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V7*/ meltfptr[6]),
			   ("%]"));
    }
    ;

    MELT_LOCATION ("warmelt-debug.melt:860:/ clear");
	   /*clear *//*_.DIS__V6*/ meltfptr[4] = 0;
    /*^clear */
	   /*clear *//*_.SBUF__V7*/ meltfptr[6] = 0;
    /*^clear */
	   /*clear *//*_#I__L3*/ meltfnum[1] = 0;
    /*^clear */
	   /*clear *//*_.BOXDEPTHP1__V8*/ meltfptr[7] = 0;
    /*^clear */
	   /*clear *//*_#__L4*/ meltfnum[3] = 0;
    /*^clear */
	   /*clear *//*_#I__L5*/ meltfnum[4] = 0;
    /*^clear */
	   /*clear *//*_.IF___V10*/ meltfptr[8] = 0;
    MELT_LOCATION ("warmelt-debug.melt:858:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V4*/ meltfptr[3] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("DBGOUT_ROUTINE_METHOD", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_21_warmelt_debug_DBGOUT_ROUTINE_METHOD_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*noretval */ NULL);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_21_warmelt_debug_DBGOUT_ROUTINE_METHOD */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_22_warmelt_debug_LAMBDA___1__ (meltclosure_ptr_t meltclosp_,
					melt_ptr_t meltfirstargp_,
					const melt_argdescr_cell_t
					meltxargdescr_[],
					union meltparam_un * meltxargtab_,
					const melt_argdescr_cell_t
					meltxresdescr_[],
					union meltparam_un * meltxrestab_)
{
  long current_blocklevel_signals_meltrout_22_warmelt_debug_LAMBDA___1___melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_22_warmelt_debug_LAMBDA___1___st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 3
    melt_ptr_t mcfr_varptr[3];
#define MELTFRAM_NBVARNUM 3
    long mcfr_varnum[3];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_22_warmelt_debug_LAMBDA___1__ is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_22_warmelt_debug_LAMBDA___1___st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 3; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_22_warmelt_debug_LAMBDA___1__ nbval 3*/
  meltfram__.mcfr_nbvar = 3 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("LAMBDA_", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-debug.melt:873:/ getarg");
 /*_.COMP__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_LONG)
    goto lab_endgetargs;
 /*_#IX__L1*/ meltfnum[0] = meltxargtab_[0].meltbp_long;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {

 /*_#GET_INT__L2*/ meltfnum[1] =
      (melt_get_int
       ((melt_ptr_t) (( /*~BOXDEPTHP1 */ meltfclos->tabval[1]))));;

    {
      MELT_LOCATION ("warmelt-debug.melt:874:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t)
				(( /*~SBUF */ meltfclos->tabval[0])),
				( /*_#GET_INT__L2*/ meltfnum[1]), 64);
    }
    ;
 /*_#GET_INT__L3*/ meltfnum[2] =
      (melt_get_int
       ((melt_ptr_t) (( /*~BOXDEPTHP1 */ meltfclos->tabval[1]))));;
    MELT_LOCATION ("warmelt-debug.melt:875:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[2];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & ( /*~DBGI */ meltfclos->tabval[2]);
      /*^apply.arg */
      argtab[1].meltbp_long = /*_#GET_INT__L3*/ meltfnum[2];
      /*_.DBG_OUT__V3*/ meltfptr[2] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!DBG_OUT */ meltfrout->tabval[0])),
		    (melt_ptr_t) ( /*_.COMP__V2*/ meltfptr[1]),
		    (MELTBPARSTR_PTR MELTBPARSTR_LONG ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-debug.melt:873:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.DBG_OUT__V3*/ meltfptr[2];;

    {
      MELT_LOCATION ("warmelt-debug.melt:873:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_#GET_INT__L2*/ meltfnum[1] = 0;
    /*^clear */
	   /*clear *//*_#GET_INT__L3*/ meltfnum[2] = 0;
    /*^clear */
	   /*clear *//*_.DBG_OUT__V3*/ meltfptr[2] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("LAMBDA_", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_22_warmelt_debug_LAMBDA___1___melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_22_warmelt_debug_LAMBDA___1__ */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_23_warmelt_debug_DBGOUT_CLOSURE_METHOD (meltclosure_ptr_t meltclosp_,
						 melt_ptr_t meltfirstargp_,
						 const melt_argdescr_cell_t
						 meltxargdescr_[],
						 union meltparam_un *
						 meltxargtab_,
						 const melt_argdescr_cell_t
						 meltxresdescr_[],
						 union meltparam_un *
						 meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_23_warmelt_debug_DBGOUT_CLOSURE_METHOD_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_23_warmelt_debug_DBGOUT_CLOSURE_METHOD_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 20
    melt_ptr_t mcfr_varptr[20];
#define MELTFRAM_NBVARNUM 24
    long mcfr_varnum[24];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_23_warmelt_debug_DBGOUT_CLOSURE_METHOD is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_23_warmelt_debug_DBGOUT_CLOSURE_METHOD_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 20; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_23_warmelt_debug_DBGOUT_CLOSURE_METHOD nbval 20*/
  meltfram__.mcfr_nbvar = 20 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("DBGOUT_CLOSURE_METHOD", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-debug.melt:885:/ getarg");
 /*_.SELF__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.DBGI__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.DBGI__V3*/ meltfptr[2])) !=
	      NULL);


  /*getarg#2 */
  /*^getarg */
  if (meltxargdescr_[1] != MELTBPAR_LONG)
    goto lab_endgetargs;
 /*_#DEPTH__L1*/ meltfnum[0] = meltxargtab_[1].meltbp_long;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-debug.melt:886:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L2*/ meltfnum[1] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.DBGI__V3*/ meltfptr[2]),
			     (melt_ptr_t) (( /*!CLASS_DEBUG_INFORMATION */
					    meltfrout->tabval[0])));;
      MELT_LOCATION ("warmelt-debug.melt:886:/ cond");
      /*cond */ if ( /*_#IS_A__L2*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V5*/ meltfptr[4] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-debug.melt:886:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check dbgi"),
				  ("warmelt-debug.melt")
				  ? ("warmelt-debug.melt") : __FILE__,
				  (886) ? (886) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V5*/ meltfptr[4] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V4*/ meltfptr[3] = /*_.IFELSE___V5*/ meltfptr[4];;
      /*epilog */

      MELT_LOCATION ("warmelt-debug.melt:886:/ clear");
	     /*clear *//*_#IS_A__L2*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V5*/ meltfptr[4] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V4*/ meltfptr[3] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-debug.melt:887:/ quasiblock");


 /*_.DIS__V6*/ meltfptr[4] =
      ((melt_ptr_t)
       (melt_discr ((melt_ptr_t) ( /*_.SELF__V2*/ meltfptr[1]))));;
    MELT_LOCATION ("warmelt-debug.melt:888:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.DBGI__V3*/ meltfptr[2]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 2, "DBGI_MAXDEPTH");
  /*_.DBGI_MAXDEPTH__V7*/ meltfptr[6] = slot;
    };
    ;
 /*_#OLDMAXDEPTH__L3*/ meltfnum[1] =
      (melt_get_int ((melt_ptr_t) ( /*_.DBGI_MAXDEPTH__V7*/ meltfptr[6])));;
    MELT_LOCATION ("warmelt-debug.melt:889:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.DBGI__V3*/ meltfptr[2]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 0, "DBGI_OUT");
  /*_.SBUF__V8*/ meltfptr[7] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-debug.melt:890:/ cond");
    /*cond */ if (
		   /*ifisa */
		   melt_is_instance_of ((melt_ptr_t)
					( /*_.DBGI__V3*/ meltfptr[2]),
					(melt_ptr_t) (( /*!CLASS_DEBUG_INFORMATION */ meltfrout->tabval[0])))
      )				/*then */
      {
	/*^cond.then */
	/*^getslot */
	{
	  melt_ptr_t slot = NULL, obj = NULL;
	  obj = (melt_ptr_t) ( /*_.DBGI__V3*/ meltfptr[2]) /*=obj*/ ;
	  melt_object_get_field (slot, obj, 2, "DBGI_MAXDEPTH");
   /*_.DBGI_MAXDEPTH__V9*/ meltfptr[8] = slot;
	};
	;
      }
    else
      {				/*^cond.else */

  /*_.DBGI_MAXDEPTH__V9*/ meltfptr[8] = NULL;;
      }
    ;
    /*^compute */
 /*_#MAXDEPTH__L4*/ meltfnum[3] =
      (melt_get_int ((melt_ptr_t) ( /*_.DBGI_MAXDEPTH__V9*/ meltfptr[8])));;
    MELT_LOCATION ("warmelt-debug.melt:892:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#__L5*/ meltfnum[4] =
      (( /*_.DIS__V6*/ meltfptr[4]) ==
       (( /*!DISCR_CLOSURE */ meltfrout->tabval[1])));;
    MELT_LOCATION ("warmelt-debug.melt:892:/ cond");
    /*cond */ if ( /*_#__L5*/ meltfnum[4])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{


	  {
	    MELT_LOCATION ("warmelt-debug.melt:893:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V8*/ meltfptr[7]),
				 (" *clo(<"));
	  }
	  ;
	  /*epilog */
	}
	;
      }
    else
      {
	MELT_LOCATION ("warmelt-debug.melt:892:/ cond.else");

	/*^block */
	/*anyblock */
	{


	  {
	    MELT_LOCATION ("warmelt-debug.melt:895:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V8*/ meltfptr[7]),
				 (" |"));
	  }
	  ;
	  MELT_LOCATION ("warmelt-debug.melt:896:/ getslot");
	  {
	    melt_ptr_t slot = NULL, obj = NULL;
	    obj = (melt_ptr_t) ( /*_.DIS__V6*/ meltfptr[4]) /*=obj*/ ;
	    melt_object_get_field (slot, obj, 1, "NAMED_NAME");
    /*_.NAMED_NAME__V10*/ meltfptr[9] = slot;
	  };
	  ;

	  {
	    /*^locexp */
	    /*add2sbuf_string */
	      meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V8*/ meltfptr[7]),
				 melt_string_str ((melt_ptr_t)
						  ( /*_.NAMED_NAME__V10*/
						   meltfptr[9])));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-debug.melt:897:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V8*/ meltfptr[7]),
				 ("*clo(<"));
	  }
	  ;
	  MELT_LOCATION ("warmelt-debug.melt:894:/ quasiblock");


	  /*epilog */

	  MELT_LOCATION ("warmelt-debug.melt:892:/ clear");
	     /*clear *//*_.NAMED_NAME__V10*/ meltfptr[9] = 0;
	}
	;
      }
    ;
    MELT_LOCATION ("warmelt-debug.melt:898:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.DBGI__V3*/ meltfptr[2]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 2, "DBGI_MAXDEPTH");
  /*_.DBGI_MAXDEPTH__V11*/ meltfptr[9] = slot;
    };
    ;

    {
      /*^locexp */
      melt_put_int ((melt_ptr_t) ( /*_.DBGI_MAXDEPTH__V11*/ meltfptr[9]),
		    (3));
    }
    ;
 /*_.CLOSURE_ROUTINE__V12*/ meltfptr[11] =
      (melt_closure_routine ((melt_ptr_t) ( /*_.SELF__V2*/ meltfptr[1])));;
    MELT_LOCATION ("warmelt-debug.melt:899:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[2];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.DBGI__V3*/ meltfptr[2];
      /*^apply.arg */
      argtab[1].meltbp_long = /*_#DEPTH__L1*/ meltfnum[0];
      /*_.DBG_OUT__V13*/ meltfptr[12] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!DBG_OUT */ meltfrout->tabval[2])),
		    (melt_ptr_t) ( /*_.CLOSURE_ROUTINE__V12*/ meltfptr[11]),
		    (MELTBPARSTR_PTR MELTBPARSTR_LONG ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-debug.melt:900:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.DBGI__V3*/ meltfptr[2]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 2, "DBGI_MAXDEPTH");
  /*_.DBGI_MAXDEPTH__V14*/ meltfptr[13] = slot;
    };
    ;

    {
      /*^locexp */
      melt_put_int ((melt_ptr_t) ( /*_.DBGI_MAXDEPTH__V14*/ meltfptr[13]),
		    ( /*_#OLDMAXDEPTH__L3*/ meltfnum[1]));
    }
    ;
    MELT_LOCATION ("warmelt-debug.melt:901:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^checksignal */
    MELT_CHECK_SIGNAL ();
    ;
 /*_#I__L6*/ meltfnum[5] =
      (( /*_#DEPTH__L1*/ meltfnum[0]) < (2));;
    MELT_LOCATION ("warmelt-debug.melt:901:/ cond");
    /*cond */ if ( /*_#I__L6*/ meltfnum[5])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

   /*_#MELT_REALLY_NEED_DBGLIM__L8*/ meltfnum[7] =
	    ( /*MELT_REALLY_NEED_DBGLIM */
	     melt_need_debug_limit ((int) /*_#DEPTH__L1*/ meltfnum[0],
				    (int) /*_#MAXDEPTH__L4*/ meltfnum[3]));;
	  MELT_LOCATION ("warmelt-debug.melt:902:/ cond");
	  /*cond */ if ( /*_#MELT_REALLY_NEED_DBGLIM__L8*/ meltfnum[7])	/*then */
	    {
	      /*^cond.then */
	      /*_#OR___L9*/ meltfnum[8] =
		/*_#MELT_REALLY_NEED_DBGLIM__L8*/ meltfnum[7];;
	    }
	  else
	    {
	      MELT_LOCATION ("warmelt-debug.melt:902:/ cond.else");

	      /*^block */
	      /*anyblock */
	      {

		MELT_LOCATION ("warmelt-debug.melt:904:/ checksignal");
		MELT_CHECK_SIGNAL ();
		;
     /*_#I__L10*/ meltfnum[9] =
		  (( /*_#DEPTH__L1*/ meltfnum[0]) <
		   ( /*_#MAXDEPTH__L4*/ meltfnum[3]));;
		MELT_LOCATION ("warmelt-debug.melt:904:/ cond");
		/*cond */ if ( /*_#I__L10*/ meltfnum[9])	/*then */
		  {
		    /*^cond.then */
		    /*^block */
		    /*anyblock */
		    {

       /*_#IS_A__L12*/ meltfnum[11] =
			melt_is_instance_of ((melt_ptr_t)
					     ( /*_.DBGI__V3*/ meltfptr[2]),
					     (melt_ptr_t) (( /*!CLASS_DEBUG_OUTPUT_INFORMATION */ meltfrout->tabval[3])));;
		      /*^compute */
		      /*_#IF___L11*/ meltfnum[10] =
			/*_#IS_A__L12*/ meltfnum[11];;
		      /*epilog */

		      MELT_LOCATION ("warmelt-debug.melt:904:/ clear");
		 /*clear *//*_#IS_A__L12*/ meltfnum[11] = 0;
		    }
		    ;
		  }
		else
		  {		/*^cond.else */

      /*_#IF___L11*/ meltfnum[10] = 0;;
		  }
		;
		/*^compute */
		/*_#OR___L9*/ meltfnum[8] = /*_#IF___L11*/ meltfnum[10];;
		/*epilog */

		MELT_LOCATION ("warmelt-debug.melt:902:/ clear");
	       /*clear *//*_#I__L10*/ meltfnum[9] = 0;
		/*^clear */
	       /*clear *//*_#IF___L11*/ meltfnum[10] = 0;
	      }
	      ;
	    }
	  ;
	  /*_#IF___L7*/ meltfnum[6] = /*_#OR___L9*/ meltfnum[8];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-debug.melt:901:/ clear");
	     /*clear *//*_#MELT_REALLY_NEED_DBGLIM__L8*/ meltfnum[7] = 0;
	  /*^clear */
	     /*clear *//*_#OR___L9*/ meltfnum[8] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

  /*_#IF___L7*/ meltfnum[6] = 0;;
      }
    ;
    MELT_LOCATION ("warmelt-debug.melt:901:/ cond");
    /*cond */ if ( /*_#IF___L7*/ meltfnum[6])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-debug.melt:906:/ quasiblock");


   /*_#IX__L13*/ meltfnum[11] = 0;;

	  {
	    MELT_LOCATION ("warmelt-debug.melt:907:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V8*/ meltfptr[7]),
				 ("/"));
	  }
	  ;
   /*_#CLOSURE_SIZE__L14*/ meltfnum[9] =
	    (melt_closure_size ((melt_ptr_t) ( /*_.SELF__V2*/ meltfptr[1])));;

	  {
	    MELT_LOCATION ("warmelt-debug.melt:908:/ locexp");
	    meltgc_add_strbuf_dec ((melt_ptr_t) ( /*_.SBUF__V8*/ meltfptr[7]),
				   ( /*_#CLOSURE_SIZE__L14*/ meltfnum[9]));
	  }
	  ;
	  MELT_LOCATION ("warmelt-debug.melt:909:/ loop");
	  /*loop */
	  {
	  labloop_OUTLOOP_1:;	/*^loopbody */

	    /*^block */
	    /*anyblock */
	    {

	      /*^checksignal */
	      MELT_CHECK_SIGNAL ();
	      ;
	      MELT_LOCATION ("warmelt-debug.melt:910:/ checksignal");
	      MELT_CHECK_SIGNAL ();
	      ;
     /*_#CLOSURE_SIZE__L15*/ meltfnum[10] =
		(melt_closure_size
		 ((melt_ptr_t) ( /*_.SELF__V2*/ meltfptr[1])));;
	      /*^compute */
     /*_#I__L16*/ meltfnum[7] =
		(( /*_#IX__L13*/ meltfnum[11]) >=
		 ( /*_#CLOSURE_SIZE__L15*/ meltfnum[10]));;
	      MELT_LOCATION ("warmelt-debug.melt:910:/ cond");
	      /*cond */ if ( /*_#I__L16*/ meltfnum[7])	/*then */
		{
		  /*^cond.then */
		  /*^block */
		  /*anyblock */
		  {

		    MELT_LOCATION ("warmelt-debug.melt:911:/ quasiblock");


		    /*^compute */
       /*_.OUTLOOP__V18*/ meltfptr[17] = NULL;;

		    /*^exit */
		    /*exit */
		    {
		      goto labexit_OUTLOOP_1;
		    }
		    ;
		    /*epilog */
		  }
		  ;
		}		/*noelse */
	      ;

	      {
		MELT_LOCATION ("warmelt-debug.melt:912:/ locexp");
		meltgc_strbuf_add_indent ((melt_ptr_t)
					  ( /*_.SBUF__V8*/ meltfptr[7]),
					  ( /*_#DEPTH__L1*/ meltfnum[0]), 64);
	      }
	      ;
	      MELT_LOCATION ("warmelt-debug.melt:913:/ quasiblock");


     /*_#CURULEN__L17*/ meltfnum[8] =
		melt_strbuf_usedlength ((melt_ptr_t)
					( /*_.SBUF__V8*/ meltfptr[7]));;
	      /*^compute */
     /*_.CLOSURE_NTH__V19*/ meltfptr[18] =
		(melt_closure_nth
		 ((melt_ptr_t) ( /*_.SELF__V2*/ meltfptr[1]),
		  (int) ( /*_#IX__L13*/ meltfnum[11])));;
	      /*^compute */
     /*_#I__L18*/ meltfnum[17] =
		(( /*_#DEPTH__L1*/ meltfnum[0]) + (2));;
	      MELT_LOCATION ("warmelt-debug.melt:914:/ checksignal");
	      MELT_CHECK_SIGNAL ();
	      ;
	      /*^apply */
	      /*apply */
	      {
		union meltparam_un argtab[2];
		memset (&argtab, 0, sizeof (argtab));
		/*^apply.arg */
		argtab[0].meltbp_aptr =
		  (melt_ptr_t *) & /*_.DBGI__V3*/ meltfptr[2];
		/*^apply.arg */
		argtab[1].meltbp_long = /*_#I__L18*/ meltfnum[17];
		/*_.DBG_OUT__V20*/ meltfptr[19] =
		  melt_apply ((meltclosure_ptr_t)
			      (( /*!DBG_OUT */ meltfrout->tabval[2])),
			      (melt_ptr_t) ( /*_.CLOSURE_NTH__V19*/
					    meltfptr[18]),
			      (MELTBPARSTR_PTR MELTBPARSTR_LONG ""), argtab,
			      "", (union meltparam_un *) 0);
	      }
	      ;
	      MELT_LOCATION ("warmelt-debug.melt:915:/ checksignal");
	      MELT_CHECK_SIGNAL ();
	      ;
     /*_#STRBUF_USEDLENGTH__L19*/ meltfnum[18] =
		melt_strbuf_usedlength ((melt_ptr_t)
					( /*_.SBUF__V8*/ meltfptr[7]));;
	      /*^compute */
     /*_#I__L20*/ meltfnum[19] =
		(( /*_#STRBUF_USEDLENGTH__L19*/ meltfnum[18]) -
		 ( /*_#CURULEN__L17*/ meltfnum[8]));;
	      /*^compute */
     /*_#I__L21*/ meltfnum[20] =
		(( /*_#I__L20*/ meltfnum[19]) > (100));;
	      MELT_LOCATION ("warmelt-debug.melt:915:/ cond");
	      /*cond */ if ( /*_#I__L21*/ meltfnum[20])	/*then */
		{
		  /*^cond.then */
		  /*^block */
		  /*anyblock */
		  {

       /*_#I__L22*/ meltfnum[21] =
		      ((1) + ( /*_#DEPTH__L1*/ meltfnum[0]));;

		    {
		      MELT_LOCATION ("warmelt-debug.melt:916:/ locexp");
		      meltgc_strbuf_add_indent ((melt_ptr_t)
						( /*_.SBUF__V8*/ meltfptr[7]),
						( /*_#I__L22*/ meltfnum[21]),
						0);
		    }
		    ;
		    /*epilog */

		    MELT_LOCATION ("warmelt-debug.melt:915:/ clear");
		 /*clear *//*_#I__L22*/ meltfnum[21] = 0;
		  }
		  ;
		}		/*noelse */
	      ;

	      MELT_LOCATION ("warmelt-debug.melt:913:/ clear");
	       /*clear *//*_#CURULEN__L17*/ meltfnum[8] = 0;
	      /*^clear */
	       /*clear *//*_.CLOSURE_NTH__V19*/ meltfptr[18] = 0;
	      /*^clear */
	       /*clear *//*_#I__L18*/ meltfnum[17] = 0;
	      /*^clear */
	       /*clear *//*_.DBG_OUT__V20*/ meltfptr[19] = 0;
	      /*^clear */
	       /*clear *//*_#STRBUF_USEDLENGTH__L19*/ meltfnum[18] = 0;
	      /*^clear */
	       /*clear *//*_#I__L20*/ meltfnum[19] = 0;
	      /*^clear */
	       /*clear *//*_#I__L21*/ meltfnum[20] = 0;
     /*_#I__L23*/ meltfnum[21] =
		(( /*_#IX__L13*/ meltfnum[11]) + (1));;
	      MELT_LOCATION ("warmelt-debug.melt:917:/ compute");
	      /*_#IX__L13*/ meltfnum[11] = /*_#SETQ___L24*/ meltfnum[8] =
		/*_#I__L23*/ meltfnum[21];;
	      MELT_LOCATION ("warmelt-debug.melt:909:/ checksignal");
	      MELT_CHECK_SIGNAL ();
	      ;
	      /*epilog */

	      /*^clear */
	       /*clear *//*_#CLOSURE_SIZE__L15*/ meltfnum[10] = 0;
	      /*^clear */
	       /*clear *//*_#I__L16*/ meltfnum[7] = 0;
	      /*^clear */
	       /*clear *//*_#I__L23*/ meltfnum[21] = 0;
	      /*^clear */
	       /*clear *//*_#SETQ___L24*/ meltfnum[8] = 0;
	    }
	    ;
	    ;
	    goto labloop_OUTLOOP_1;
	  labexit_OUTLOOP_1:;	/*^loopepilog */
	    /*loopepilog */
	    /*_.FOREVER___V17*/ meltfptr[16] =
	      /*_.OUTLOOP__V18*/ meltfptr[17];;
	  }
	  ;
	  /*^compute */
	  /*_.LET___V16*/ meltfptr[15] = /*_.FOREVER___V17*/ meltfptr[16];;

	  MELT_LOCATION ("warmelt-debug.melt:906:/ clear");
	     /*clear *//*_#IX__L13*/ meltfnum[11] = 0;
	  /*^clear */
	     /*clear *//*_#CLOSURE_SIZE__L14*/ meltfnum[9] = 0;
	  /*^clear */
	     /*clear *//*_.FOREVER___V17*/ meltfptr[16] = 0;
	  /*_.IFELSE___V15*/ meltfptr[14] = /*_.LET___V16*/ meltfptr[15];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-debug.melt:901:/ clear");
	     /*clear *//*_.LET___V16*/ meltfptr[15] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

	/*^block */
	/*anyblock */
	{




	  {
	    MELT_LOCATION ("warmelt-debug.melt:919:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V8*/ meltfptr[7]),
				 ("..."));
	  }
	  ;
	     /*clear *//*_.IFELSE___V15*/ meltfptr[14] = 0;
	  /*epilog */
	}
	;
      }
    ;

    {
      MELT_LOCATION ("warmelt-debug.melt:921:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V8*/ meltfptr[7]),
			   (" >)"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-debug.melt:922:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.SBUF__V8*/ meltfptr[7]),
				( /*_#DEPTH__L1*/ meltfnum[0]), 0);
    }
    ;

    MELT_LOCATION ("warmelt-debug.melt:887:/ clear");
	   /*clear *//*_.DIS__V6*/ meltfptr[4] = 0;
    /*^clear */
	   /*clear *//*_.DBGI_MAXDEPTH__V7*/ meltfptr[6] = 0;
    /*^clear */
	   /*clear *//*_#OLDMAXDEPTH__L3*/ meltfnum[1] = 0;
    /*^clear */
	   /*clear *//*_.SBUF__V8*/ meltfptr[7] = 0;
    /*^clear */
	   /*clear *//*_.DBGI_MAXDEPTH__V9*/ meltfptr[8] = 0;
    /*^clear */
	   /*clear *//*_#MAXDEPTH__L4*/ meltfnum[3] = 0;
    /*^clear */
	   /*clear *//*_#__L5*/ meltfnum[4] = 0;
    /*^clear */
	   /*clear *//*_.DBGI_MAXDEPTH__V11*/ meltfptr[9] = 0;
    /*^clear */
	   /*clear *//*_.CLOSURE_ROUTINE__V12*/ meltfptr[11] = 0;
    /*^clear */
	   /*clear *//*_.DBG_OUT__V13*/ meltfptr[12] = 0;
    /*^clear */
	   /*clear *//*_.DBGI_MAXDEPTH__V14*/ meltfptr[13] = 0;
    /*^clear */
	   /*clear *//*_#I__L6*/ meltfnum[5] = 0;
    /*^clear */
	   /*clear *//*_#IF___L7*/ meltfnum[6] = 0;
    /*^clear */
	   /*clear *//*_.IFELSE___V15*/ meltfptr[14] = 0;
    MELT_LOCATION ("warmelt-debug.melt:885:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V4*/ meltfptr[3] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("DBGOUT_CLOSURE_METHOD", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_23_warmelt_debug_DBGOUT_CLOSURE_METHOD_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*noretval */ NULL);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_23_warmelt_debug_DBGOUT_CLOSURE_METHOD */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_24_warmelt_debug_DBGOUT_LIST_METHOD (meltclosure_ptr_t meltclosp_,
					      melt_ptr_t meltfirstargp_,
					      const melt_argdescr_cell_t
					      meltxargdescr_[],
					      union meltparam_un *
					      meltxargtab_,
					      const melt_argdescr_cell_t
					      meltxresdescr_[],
					      union meltparam_un *
					      meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_24_warmelt_debug_DBGOUT_LIST_METHOD_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_24_warmelt_debug_DBGOUT_LIST_METHOD_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 17
    melt_ptr_t mcfr_varptr[17];
#define MELTFRAM_NBVARNUM 22
    long mcfr_varnum[22];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_24_warmelt_debug_DBGOUT_LIST_METHOD is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_24_warmelt_debug_DBGOUT_LIST_METHOD_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 17; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_24_warmelt_debug_DBGOUT_LIST_METHOD nbval 17*/
  meltfram__.mcfr_nbvar = 17 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("DBGOUT_LIST_METHOD", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-debug.melt:930:/ getarg");
 /*_.SELF__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.DBGI__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.DBGI__V3*/ meltfptr[2])) !=
	      NULL);


  /*getarg#2 */
  /*^getarg */
  if (meltxargdescr_[1] != MELTBPAR_LONG)
    goto lab_endgetargs;
 /*_#DEPTH__L1*/ meltfnum[0] = meltxargtab_[1].meltbp_long;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-debug.melt:931:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L2*/ meltfnum[1] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.DBGI__V3*/ meltfptr[2]),
			     (melt_ptr_t) (( /*!CLASS_DEBUG_INFORMATION */
					    meltfrout->tabval[0])));;
      MELT_LOCATION ("warmelt-debug.melt:931:/ cond");
      /*cond */ if ( /*_#IS_A__L2*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V5*/ meltfptr[4] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-debug.melt:931:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check dbgi"),
				  ("warmelt-debug.melt")
				  ? ("warmelt-debug.melt") : __FILE__,
				  (931) ? (931) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V5*/ meltfptr[4] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V4*/ meltfptr[3] = /*_.IFELSE___V5*/ meltfptr[4];;
      /*epilog */

      MELT_LOCATION ("warmelt-debug.melt:931:/ clear");
	     /*clear *//*_#IS_A__L2*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V5*/ meltfptr[4] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V4*/ meltfptr[3] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-debug.melt:932:/ quasiblock");


 /*_.DIS__V6*/ meltfptr[4] =
      ((melt_ptr_t)
       (melt_discr ((melt_ptr_t) ( /*_.SELF__V2*/ meltfptr[1]))));;
    MELT_LOCATION ("warmelt-debug.melt:933:/ cond");
    /*cond */ if (
		   /*ifisa */
		   melt_is_instance_of ((melt_ptr_t)
					( /*_.DBGI__V3*/ meltfptr[2]),
					(melt_ptr_t) (( /*!CLASS_DEBUG_INFORMATION */ meltfrout->tabval[0])))
      )				/*then */
      {
	/*^cond.then */
	/*^getslot */
	{
	  melt_ptr_t slot = NULL, obj = NULL;
	  obj = (melt_ptr_t) ( /*_.DBGI__V3*/ meltfptr[2]) /*=obj*/ ;
	  melt_object_get_field (slot, obj, 2, "DBGI_MAXDEPTH");
   /*_.DBGI_MAXDEPTH__V7*/ meltfptr[6] = slot;
	};
	;
      }
    else
      {				/*^cond.else */

  /*_.DBGI_MAXDEPTH__V7*/ meltfptr[6] = NULL;;
      }
    ;
    /*^compute */
 /*_#MAXDEPTH__L3*/ meltfnum[1] =
      (melt_get_int ((melt_ptr_t) ( /*_.DBGI_MAXDEPTH__V7*/ meltfptr[6])));;
    MELT_LOCATION ("warmelt-debug.melt:934:/ cond");
    /*cond */ if (
		   /*ifisa */
		   melt_is_instance_of ((melt_ptr_t)
					( /*_.DBGI__V3*/ meltfptr[2]),
					(melt_ptr_t) (( /*!CLASS_DEBUG_INFORMATION */ meltfrout->tabval[0])))
      )				/*then */
      {
	/*^cond.then */
	/*^getslot */
	{
	  melt_ptr_t slot = NULL, obj = NULL;
	  obj = (melt_ptr_t) ( /*_.DBGI__V3*/ meltfptr[2]) /*=obj*/ ;
	  melt_object_get_field (slot, obj, 0, "DBGI_OUT");
   /*_.SBUF__V8*/ meltfptr[7] = slot;
	};
	;
      }
    else
      {				/*^cond.else */

  /*_.SBUF__V8*/ meltfptr[7] = NULL;;
      }
    ;
    MELT_LOCATION ("warmelt-debug.melt:935:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#__L4*/ meltfnum[3] =
      (( /*_.DIS__V6*/ meltfptr[4]) ==
       (( /*!DISCR_LIST */ meltfrout->tabval[1])));;
    MELT_LOCATION ("warmelt-debug.melt:935:/ cond");
    /*cond */ if ( /*_#__L4*/ meltfnum[3])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{


	  {
	    MELT_LOCATION ("warmelt-debug.melt:936:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V8*/ meltfptr[7]),
				 (" *li("));
	  }
	  ;
	  /*epilog */
	}
	;
      }
    else
      {
	MELT_LOCATION ("warmelt-debug.melt:935:/ cond.else");

	/*^block */
	/*anyblock */
	{


	  {
	    MELT_LOCATION ("warmelt-debug.melt:938:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V8*/ meltfptr[7]),
				 (" |"));
	  }
	  ;
	  MELT_LOCATION ("warmelt-debug.melt:939:/ getslot");
	  {
	    melt_ptr_t slot = NULL, obj = NULL;
	    obj = (melt_ptr_t) ( /*_.DIS__V6*/ meltfptr[4]) /*=obj*/ ;
	    melt_object_get_field (slot, obj, 1, "NAMED_NAME");
    /*_.NAMED_NAME__V9*/ meltfptr[8] = slot;
	  };
	  ;

	  {
	    /*^locexp */
	    /*add2sbuf_string */
	      meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V8*/ meltfptr[7]),
				 melt_string_str ((melt_ptr_t)
						  ( /*_.NAMED_NAME__V9*/
						   meltfptr[8])));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-debug.melt:940:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V8*/ meltfptr[7]),
				 ("("));
	  }
	  ;
	  MELT_LOCATION ("warmelt-debug.melt:937:/ quasiblock");


	  /*epilog */

	  MELT_LOCATION ("warmelt-debug.melt:935:/ clear");
	     /*clear *//*_.NAMED_NAME__V9*/ meltfptr[8] = 0;
	}
	;
      }
    ;
    MELT_LOCATION ("warmelt-debug.melt:941:/ quasiblock");


 /*_.CURPAIR__V10*/ meltfptr[8] =
      (melt_list_first ((melt_ptr_t) ( /*_.SELF__V2*/ meltfptr[1])));;
    /*^compute */
 /*_#IX__L5*/ meltfnum[4] = 0;;
    MELT_LOCATION ("warmelt-debug.melt:946:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#MELT_REALLY_NEED_DBGLIM__L6*/ meltfnum[5] =
      ( /*MELT_REALLY_NEED_DBGLIM */
       melt_need_debug_limit ((int) /*_#DEPTH__L1*/ meltfnum[0],
			      (int) /*_#MAXDEPTH__L3*/ meltfnum[1]));;
    MELT_LOCATION ("warmelt-debug.melt:946:/ cond");
    /*cond */ if ( /*_#MELT_REALLY_NEED_DBGLIM__L6*/ meltfnum[5])	/*then */
      {
	/*^cond.then */
	/*_#OR___L7*/ meltfnum[6] =
	  /*_#MELT_REALLY_NEED_DBGLIM__L6*/ meltfnum[5];;
      }
    else
      {
	MELT_LOCATION ("warmelt-debug.melt:946:/ cond.else");

	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-debug.melt:948:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
   /*_#I__L8*/ meltfnum[7] =
	    (( /*_#DEPTH__L1*/ meltfnum[0]) <
	     ( /*_#MAXDEPTH__L3*/ meltfnum[1]));;
	  MELT_LOCATION ("warmelt-debug.melt:948:/ cond");
	  /*cond */ if ( /*_#I__L8*/ meltfnum[7])	/*then */
	    {
	      /*^cond.then */
	      /*^block */
	      /*anyblock */
	      {

     /*_#IS_A__L10*/ meltfnum[9] =
		  melt_is_instance_of ((melt_ptr_t)
				       ( /*_.DBGI__V3*/ meltfptr[2]),
				       (melt_ptr_t) (( /*!CLASS_DEBUG_OUTPUT_INFORMATION */ meltfrout->tabval[2])));;
		/*^compute */
		/*_#IF___L9*/ meltfnum[8] = /*_#IS_A__L10*/ meltfnum[9];;
		/*epilog */

		MELT_LOCATION ("warmelt-debug.melt:948:/ clear");
	       /*clear *//*_#IS_A__L10*/ meltfnum[9] = 0;
	      }
	      ;
	    }
	  else
	    {			/*^cond.else */

    /*_#IF___L9*/ meltfnum[8] = 0;;
	    }
	  ;
	  /*^compute */
	  /*_#OR___L7*/ meltfnum[6] = /*_#IF___L9*/ meltfnum[8];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-debug.melt:946:/ clear");
	     /*clear *//*_#I__L8*/ meltfnum[7] = 0;
	  /*^clear */
	     /*clear *//*_#IF___L9*/ meltfnum[8] = 0;
	}
	;
      }
    ;
    /*^cond */
    /*cond */ if ( /*_#OR___L7*/ meltfnum[6])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-debug.melt:949:/ loop");
	  /*loop */
	  {
	  labloop_LISTLOOP_1:;	/*^loopbody */

	    /*^block */
	    /*anyblock */
	    {

	      /*^checksignal */
	      MELT_CHECK_SIGNAL ();
	      ;
	      MELT_LOCATION ("warmelt-debug.melt:951:/ checksignal");
	      MELT_CHECK_SIGNAL ();
	      ;
     /*_#I__L11*/ meltfnum[9] =
		(( /*_#IX__L5*/ meltfnum[4]) > (300));;
	      MELT_LOCATION ("warmelt-debug.melt:951:/ cond");
	      /*cond */ if ( /*_#I__L11*/ meltfnum[9])	/*then */
		{
		  /*^cond.then */
		  /*^block */
		  /*anyblock */
		  {


		    {
		      MELT_LOCATION ("warmelt-debug.melt:953:/ locexp");
		      /*add2sbuf_strconst */
			meltgc_add_strbuf ((melt_ptr_t)
					   ( /*_.SBUF__V8*/ meltfptr[7]),
					   ("..."));
		    }
		    ;
		    MELT_LOCATION ("warmelt-debug.melt:954:/ quasiblock");


		    /*^compute */
       /*_.LISTLOOP__V13*/ meltfptr[12] = NULL;;

		    /*^exit */
		    /*exit */
		    {
		      goto labexit_LISTLOOP_1;
		    }
		    ;
		    MELT_LOCATION ("warmelt-debug.melt:952:/ quasiblock");


		    /*epilog */
		  }
		  ;
		}		/*noelse */
	      ;
	      MELT_LOCATION ("warmelt-debug.melt:955:/ checksignal");
	      MELT_CHECK_SIGNAL ();
	      ;
     /*_#IS_PAIR__L12*/ meltfnum[7] =
		(melt_magic_discr
		 ((melt_ptr_t) ( /*_.CURPAIR__V10*/ meltfptr[8])) ==
		 MELTOBMAG_PAIR);;
	      /*^compute */
     /*_#NOT__L13*/ meltfnum[8] =
		(!( /*_#IS_PAIR__L12*/ meltfnum[7]));;
	      MELT_LOCATION ("warmelt-debug.melt:955:/ cond");
	      /*cond */ if ( /*_#NOT__L13*/ meltfnum[8])	/*then */
		{
		  /*^cond.then */
		  /*^block */
		  /*anyblock */
		  {

		    /*^quasiblock */


		    /*^compute */
       /*_.LISTLOOP__V13*/ meltfptr[12] = NULL;;

		    /*^exit */
		    /*exit */
		    {
		      goto labexit_LISTLOOP_1;
		    }
		    ;
		    /*epilog */
		  }
		  ;
		}		/*noelse */
	      ;

	      {
		MELT_LOCATION ("warmelt-debug.melt:956:/ locexp");
		meltgc_strbuf_add_indent ((melt_ptr_t)
					  ( /*_.SBUF__V8*/ meltfptr[7]),
					  ( /*_#DEPTH__L1*/ meltfnum[0]), 64);
	      }
	      ;
	      MELT_LOCATION ("warmelt-debug.melt:957:/ quasiblock");


     /*_#CURULEN__L14*/ meltfnum[13] =
		melt_strbuf_usedlength ((melt_ptr_t)
					( /*_.SBUF__V8*/ meltfptr[7]));;
	      /*^compute */
     /*_.PAIR_HEAD__V14*/ meltfptr[13] =
		(melt_pair_head
		 ((melt_ptr_t) ( /*_.CURPAIR__V10*/ meltfptr[8])));;
	      /*^compute */
     /*_#I__L15*/ meltfnum[14] =
		(( /*_#DEPTH__L1*/ meltfnum[0]) + (1));;
	      MELT_LOCATION ("warmelt-debug.melt:958:/ checksignal");
	      MELT_CHECK_SIGNAL ();
	      ;
	      /*^apply */
	      /*apply */
	      {
		union meltparam_un argtab[2];
		memset (&argtab, 0, sizeof (argtab));
		/*^apply.arg */
		argtab[0].meltbp_aptr =
		  (melt_ptr_t *) & /*_.DBGI__V3*/ meltfptr[2];
		/*^apply.arg */
		argtab[1].meltbp_long = /*_#I__L15*/ meltfnum[14];
		/*_.DBG_OUT__V15*/ meltfptr[14] =
		  melt_apply ((meltclosure_ptr_t)
			      (( /*!DBG_OUT */ meltfrout->tabval[3])),
			      (melt_ptr_t) ( /*_.PAIR_HEAD__V14*/
					    meltfptr[13]),
			      (MELTBPARSTR_PTR MELTBPARSTR_LONG ""), argtab,
			      "", (union meltparam_un *) 0);
	      }
	      ;
     /*_.PAIR_TAIL__V16*/ meltfptr[15] =
		(melt_pair_tail
		 ((melt_ptr_t) ( /*_.CURPAIR__V10*/ meltfptr[8])));;
	      MELT_LOCATION ("warmelt-debug.melt:959:/ compute");
	      /*_.CURPAIR__V10*/ meltfptr[8] = /*_.SETQ___V17*/ meltfptr[16] =
		/*_.PAIR_TAIL__V16*/ meltfptr[15];;
	      MELT_LOCATION ("warmelt-debug.melt:961:/ checksignal");
	      MELT_CHECK_SIGNAL ();
	      ;
     /*_#NULL__L16*/ meltfnum[15] =
		(( /*_.CURPAIR__V10*/ meltfptr[8]) == NULL);;
	      MELT_LOCATION ("warmelt-debug.melt:961:/ cond");
	      /*cond */ if ( /*_#NULL__L16*/ meltfnum[15])	/*then */
		{
		  /*^cond.then */
		  /*^block */
		  /*anyblock */
		  {

		    MELT_LOCATION ("warmelt-debug.melt:962:/ quasiblock");


		    /*^compute */
       /*_.LISTLOOP__V13*/ meltfptr[12] = NULL;;

		    /*^exit */
		    /*exit */
		    {
		      goto labexit_LISTLOOP_1;
		    }
		    ;
		    /*epilog */
		  }
		  ;
		}		/*noelse */
	      ;
     /*_#I__L17*/ meltfnum[16] =
		(( /*_#IX__L5*/ meltfnum[4]) + (1));;
	      MELT_LOCATION ("warmelt-debug.melt:963:/ compute");
	      /*_#IX__L5*/ meltfnum[4] = /*_#SETQ___L18*/ meltfnum[17] =
		/*_#I__L17*/ meltfnum[16];;
	      MELT_LOCATION ("warmelt-debug.melt:964:/ checksignal");
	      MELT_CHECK_SIGNAL ();
	      ;
     /*_#STRBUF_USEDLENGTH__L19*/ meltfnum[18] =
		melt_strbuf_usedlength ((melt_ptr_t)
					( /*_.SBUF__V8*/ meltfptr[7]));;
	      /*^compute */
     /*_#I__L20*/ meltfnum[19] =
		(( /*_#STRBUF_USEDLENGTH__L19*/ meltfnum[18]) -
		 ( /*_#CURULEN__L14*/ meltfnum[13]));;
	      /*^compute */
     /*_#I__L21*/ meltfnum[20] =
		(( /*_#I__L20*/ meltfnum[19]) > (100));;
	      MELT_LOCATION ("warmelt-debug.melt:964:/ cond");
	      /*cond */ if ( /*_#I__L21*/ meltfnum[20])	/*then */
		{
		  /*^cond.then */
		  /*^block */
		  /*anyblock */
		  {

       /*_#I__L22*/ meltfnum[21] =
		      ((1) + ( /*_#DEPTH__L1*/ meltfnum[0]));;

		    {
		      MELT_LOCATION ("warmelt-debug.melt:965:/ locexp");
		      meltgc_strbuf_add_indent ((melt_ptr_t)
						( /*_.SBUF__V8*/ meltfptr[7]),
						( /*_#I__L22*/ meltfnum[21]),
						0);
		    }
		    ;
		    /*epilog */

		    MELT_LOCATION ("warmelt-debug.melt:964:/ clear");
		 /*clear *//*_#I__L22*/ meltfnum[21] = 0;
		  }
		  ;
		}		/*noelse */
	      ;

	      MELT_LOCATION ("warmelt-debug.melt:957:/ clear");
	       /*clear *//*_#CURULEN__L14*/ meltfnum[13] = 0;
	      /*^clear */
	       /*clear *//*_.PAIR_HEAD__V14*/ meltfptr[13] = 0;
	      /*^clear */
	       /*clear *//*_#I__L15*/ meltfnum[14] = 0;
	      /*^clear */
	       /*clear *//*_.DBG_OUT__V15*/ meltfptr[14] = 0;
	      /*^clear */
	       /*clear *//*_.PAIR_TAIL__V16*/ meltfptr[15] = 0;
	      /*^clear */
	       /*clear *//*_.SETQ___V17*/ meltfptr[16] = 0;
	      /*^clear */
	       /*clear *//*_#NULL__L16*/ meltfnum[15] = 0;
	      /*^clear */
	       /*clear *//*_#I__L17*/ meltfnum[16] = 0;
	      /*^clear */
	       /*clear *//*_#SETQ___L18*/ meltfnum[17] = 0;
	      /*^clear */
	       /*clear *//*_#STRBUF_USEDLENGTH__L19*/ meltfnum[18] = 0;
	      /*^clear */
	       /*clear *//*_#I__L20*/ meltfnum[19] = 0;
	      /*^clear */
	       /*clear *//*_#I__L21*/ meltfnum[20] = 0;
	      MELT_LOCATION ("warmelt-debug.melt:949:/ checksignal");
	      MELT_CHECK_SIGNAL ();
	      ;
	      /*epilog */

	      /*^clear */
	       /*clear *//*_#I__L11*/ meltfnum[9] = 0;
	      /*^clear */
	       /*clear *//*_#IS_PAIR__L12*/ meltfnum[7] = 0;
	      /*^clear */
	       /*clear *//*_#NOT__L13*/ meltfnum[8] = 0;
	    }
	    ;
	    ;
	    goto labloop_LISTLOOP_1;
	  labexit_LISTLOOP_1:;	/*^loopepilog */
	    /*loopepilog */
	    /*_.FOREVER___V12*/ meltfptr[11] =
	      /*_.LISTLOOP__V13*/ meltfptr[12];;
	  }
	  ;
	  /*^compute */
	  /*_.IF___V11*/ meltfptr[10] = /*_.FOREVER___V12*/ meltfptr[11];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-debug.melt:946:/ clear");
	     /*clear *//*_.FOREVER___V12*/ meltfptr[11] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

  /*_.IF___V11*/ meltfptr[10] = NULL;;
      }
    ;

    {
      MELT_LOCATION ("warmelt-debug.melt:968:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V8*/ meltfptr[7]), (")"));
    }
    ;

    MELT_LOCATION ("warmelt-debug.melt:941:/ clear");
	   /*clear *//*_.CURPAIR__V10*/ meltfptr[8] = 0;
    /*^clear */
	   /*clear *//*_#IX__L5*/ meltfnum[4] = 0;
    /*^clear */
	   /*clear *//*_#MELT_REALLY_NEED_DBGLIM__L6*/ meltfnum[5] = 0;
    /*^clear */
	   /*clear *//*_#OR___L7*/ meltfnum[6] = 0;
    /*^clear */
	   /*clear *//*_.IF___V11*/ meltfptr[10] = 0;

    MELT_LOCATION ("warmelt-debug.melt:932:/ clear");
	   /*clear *//*_.DIS__V6*/ meltfptr[4] = 0;
    /*^clear */
	   /*clear *//*_.DBGI_MAXDEPTH__V7*/ meltfptr[6] = 0;
    /*^clear */
	   /*clear *//*_#MAXDEPTH__L3*/ meltfnum[1] = 0;
    /*^clear */
	   /*clear *//*_.SBUF__V8*/ meltfptr[7] = 0;
    /*^clear */
	   /*clear *//*_#__L4*/ meltfnum[3] = 0;
    MELT_LOCATION ("warmelt-debug.melt:930:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V4*/ meltfptr[3] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("DBGOUT_LIST_METHOD", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_24_warmelt_debug_DBGOUT_LIST_METHOD_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*noretval */ NULL);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_24_warmelt_debug_DBGOUT_LIST_METHOD */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_25_warmelt_debug_DBGOUT_PAIR_METHOD (meltclosure_ptr_t meltclosp_,
					      melt_ptr_t meltfirstargp_,
					      const melt_argdescr_cell_t
					      meltxargdescr_[],
					      union meltparam_un *
					      meltxargtab_,
					      const melt_argdescr_cell_t
					      meltxresdescr_[],
					      union meltparam_un *
					      meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_25_warmelt_debug_DBGOUT_PAIR_METHOD_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_25_warmelt_debug_DBGOUT_PAIR_METHOD_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 15
    melt_ptr_t mcfr_varptr[15];
#define MELTFRAM_NBVARNUM 11
    long mcfr_varnum[11];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_25_warmelt_debug_DBGOUT_PAIR_METHOD is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_25_warmelt_debug_DBGOUT_PAIR_METHOD_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 15; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_25_warmelt_debug_DBGOUT_PAIR_METHOD nbval 15*/
  meltfram__.mcfr_nbvar = 15 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("DBGOUT_PAIR_METHOD", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-debug.melt:973:/ getarg");
 /*_.SELF__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.DBGI__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.DBGI__V3*/ meltfptr[2])) !=
	      NULL);


  /*getarg#2 */
  /*^getarg */
  if (meltxargdescr_[1] != MELTBPAR_LONG)
    goto lab_endgetargs;
 /*_#DEPTH__L1*/ meltfnum[0] = meltxargtab_[1].meltbp_long;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-debug.melt:974:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L2*/ meltfnum[1] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.DBGI__V3*/ meltfptr[2]),
			     (melt_ptr_t) (( /*!CLASS_DEBUG_INFORMATION */
					    meltfrout->tabval[0])));;
      MELT_LOCATION ("warmelt-debug.melt:974:/ cond");
      /*cond */ if ( /*_#IS_A__L2*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V5*/ meltfptr[4] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-debug.melt:974:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check dbgi"),
				  ("warmelt-debug.melt")
				  ? ("warmelt-debug.melt") : __FILE__,
				  (974) ? (974) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V5*/ meltfptr[4] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V4*/ meltfptr[3] = /*_.IFELSE___V5*/ meltfptr[4];;
      /*epilog */

      MELT_LOCATION ("warmelt-debug.melt:974:/ clear");
	     /*clear *//*_#IS_A__L2*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V5*/ meltfptr[4] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V4*/ meltfptr[3] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-debug.melt:975:/ quasiblock");


 /*_.DIS__V6*/ meltfptr[4] =
      ((melt_ptr_t)
       (melt_discr ((melt_ptr_t) ( /*_.SELF__V2*/ meltfptr[1]))));;
    MELT_LOCATION ("warmelt-debug.melt:976:/ cond");
    /*cond */ if (
		   /*ifisa */
		   melt_is_instance_of ((melt_ptr_t)
					( /*_.DBGI__V3*/ meltfptr[2]),
					(melt_ptr_t) (( /*!CLASS_DEBUG_INFORMATION */ meltfrout->tabval[0])))
      )				/*then */
      {
	/*^cond.then */
	/*^getslot */
	{
	  melt_ptr_t slot = NULL, obj = NULL;
	  obj = (melt_ptr_t) ( /*_.DBGI__V3*/ meltfptr[2]) /*=obj*/ ;
	  melt_object_get_field (slot, obj, 2, "DBGI_MAXDEPTH");
   /*_.DBGI_MAXDEPTH__V7*/ meltfptr[6] = slot;
	};
	;
      }
    else
      {				/*^cond.else */

  /*_.DBGI_MAXDEPTH__V7*/ meltfptr[6] = NULL;;
      }
    ;
    /*^compute */
 /*_#MAXDEPTH__L3*/ meltfnum[1] =
      (melt_get_int ((melt_ptr_t) ( /*_.DBGI_MAXDEPTH__V7*/ meltfptr[6])));;
    MELT_LOCATION ("warmelt-debug.melt:977:/ cond");
    /*cond */ if (
		   /*ifisa */
		   melt_is_instance_of ((melt_ptr_t)
					( /*_.DBGI__V3*/ meltfptr[2]),
					(melt_ptr_t) (( /*!CLASS_DEBUG_INFORMATION */ meltfrout->tabval[0])))
      )				/*then */
      {
	/*^cond.then */
	/*^getslot */
	{
	  melt_ptr_t slot = NULL, obj = NULL;
	  obj = (melt_ptr_t) ( /*_.DBGI__V3*/ meltfptr[2]) /*=obj*/ ;
	  melt_object_get_field (slot, obj, 0, "DBGI_OUT");
   /*_.SBUF__V8*/ meltfptr[7] = slot;
	};
	;
      }
    else
      {				/*^cond.else */

  /*_.SBUF__V8*/ meltfptr[7] = NULL;;
      }
    ;
    MELT_LOCATION ("warmelt-debug.melt:978:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#__L4*/ meltfnum[3] =
      (( /*_.DIS__V6*/ meltfptr[4]) ==
       (( /*!DISCR_PAIR */ meltfrout->tabval[1])));;
    MELT_LOCATION ("warmelt-debug.melt:978:/ cond");
    /*cond */ if ( /*_#__L4*/ meltfnum[3])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{


	  {
	    MELT_LOCATION ("warmelt-debug.melt:979:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V8*/ meltfptr[7]),
				 (" *pa(."));
	  }
	  ;
	  /*epilog */
	}
	;
      }
    else
      {
	MELT_LOCATION ("warmelt-debug.melt:978:/ cond.else");

	/*^block */
	/*anyblock */
	{


	  {
	    MELT_LOCATION ("warmelt-debug.melt:981:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V8*/ meltfptr[7]),
				 (" |"));
	  }
	  ;
	  MELT_LOCATION ("warmelt-debug.melt:982:/ getslot");
	  {
	    melt_ptr_t slot = NULL, obj = NULL;
	    obj = (melt_ptr_t) ( /*_.DIS__V6*/ meltfptr[4]) /*=obj*/ ;
	    melt_object_get_field (slot, obj, 1, "NAMED_NAME");
    /*_.NAMED_NAME__V9*/ meltfptr[8] = slot;
	  };
	  ;

	  {
	    /*^locexp */
	    /*add2sbuf_string */
	      meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V8*/ meltfptr[7]),
				 melt_string_str ((melt_ptr_t)
						  ( /*_.NAMED_NAME__V9*/
						   meltfptr[8])));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-debug.melt:983:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V8*/ meltfptr[7]),
				 ("(."));
	  }
	  ;
	  MELT_LOCATION ("warmelt-debug.melt:980:/ quasiblock");


	  /*epilog */

	  MELT_LOCATION ("warmelt-debug.melt:978:/ clear");
	     /*clear *//*_.NAMED_NAME__V9*/ meltfptr[8] = 0;
	}
	;
      }
    ;
    MELT_LOCATION ("warmelt-debug.melt:984:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#MELT_REALLY_NEED_DBGLIM__L5*/ meltfnum[4] =
      ( /*MELT_REALLY_NEED_DBGLIM */
       melt_need_debug_limit ((int) /*_#DEPTH__L1*/ meltfnum[0],
			      (int) /*_#MAXDEPTH__L3*/ meltfnum[1]));;
    MELT_LOCATION ("warmelt-debug.melt:984:/ cond");
    /*cond */ if ( /*_#MELT_REALLY_NEED_DBGLIM__L5*/ meltfnum[4])	/*then */
      {
	/*^cond.then */
	/*_#OR___L6*/ meltfnum[5] =
	  /*_#MELT_REALLY_NEED_DBGLIM__L5*/ meltfnum[4];;
      }
    else
      {
	MELT_LOCATION ("warmelt-debug.melt:984:/ cond.else");

	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-debug.melt:986:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
   /*_#I__L7*/ meltfnum[6] =
	    (( /*_#DEPTH__L1*/ meltfnum[0]) <
	     ( /*_#MAXDEPTH__L3*/ meltfnum[1]));;
	  MELT_LOCATION ("warmelt-debug.melt:986:/ cond");
	  /*cond */ if ( /*_#I__L7*/ meltfnum[6])	/*then */
	    {
	      /*^cond.then */
	      /*^block */
	      /*anyblock */
	      {

     /*_#IS_A__L9*/ meltfnum[8] =
		  melt_is_instance_of ((melt_ptr_t)
				       ( /*_.DBGI__V3*/ meltfptr[2]),
				       (melt_ptr_t) (( /*!CLASS_DEBUG_OUTPUT_INFORMATION */ meltfrout->tabval[2])));;
		/*^compute */
		/*_#IF___L8*/ meltfnum[7] = /*_#IS_A__L9*/ meltfnum[8];;
		/*epilog */

		MELT_LOCATION ("warmelt-debug.melt:986:/ clear");
	       /*clear *//*_#IS_A__L9*/ meltfnum[8] = 0;
	      }
	      ;
	    }
	  else
	    {			/*^cond.else */

    /*_#IF___L8*/ meltfnum[7] = 0;;
	    }
	  ;
	  /*^compute */
	  /*_#OR___L6*/ meltfnum[5] = /*_#IF___L8*/ meltfnum[7];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-debug.melt:984:/ clear");
	     /*clear *//*_#I__L7*/ meltfnum[6] = 0;
	  /*^clear */
	     /*clear *//*_#IF___L8*/ meltfnum[7] = 0;
	}
	;
      }
    ;
    /*^cond */
    /*cond */ if ( /*_#OR___L6*/ meltfnum[5])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

   /*_.PAIR_HEAD__V11*/ meltfptr[10] =
	    (melt_pair_head ((melt_ptr_t) ( /*_.SELF__V2*/ meltfptr[1])));;
	  /*^compute */
   /*_#I__L10*/ meltfnum[8] =
	    (( /*_#DEPTH__L1*/ meltfnum[0]) + (1));;
	  MELT_LOCATION ("warmelt-debug.melt:988:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^apply */
	  /*apply */
	  {
	    union meltparam_un argtab[2];
	    memset (&argtab, 0, sizeof (argtab));
	    /*^apply.arg */
	    argtab[0].meltbp_aptr =
	      (melt_ptr_t *) & /*_.DBGI__V3*/ meltfptr[2];
	    /*^apply.arg */
	    argtab[1].meltbp_long = /*_#I__L10*/ meltfnum[8];
	    /*_.DBG_OUT__V12*/ meltfptr[11] =
	      melt_apply ((meltclosure_ptr_t)
			  (( /*!DBG_OUT */ meltfrout->tabval[3])),
			  (melt_ptr_t) ( /*_.PAIR_HEAD__V11*/ meltfptr[10]),
			  (MELTBPARSTR_PTR MELTBPARSTR_LONG ""), argtab, "",
			  (union meltparam_un *) 0);
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-debug.melt:989:/ locexp");
	    meltgc_strbuf_add_indent ((melt_ptr_t)
				      ( /*_.SBUF__V8*/ meltfptr[7]),
				      ( /*_#DEPTH__L1*/ meltfnum[0]), 64);
	  }
	  ;
   /*_.PAIR_TAIL__V13*/ meltfptr[12] =
	    (melt_pair_tail ((melt_ptr_t) ( /*_.SELF__V2*/ meltfptr[1])));;
	  /*^compute */
   /*_#I__L11*/ meltfnum[6] =
	    (( /*_#DEPTH__L1*/ meltfnum[0]) + (1));;
	  MELT_LOCATION ("warmelt-debug.melt:990:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^apply */
	  /*apply */
	  {
	    union meltparam_un argtab[2];
	    memset (&argtab, 0, sizeof (argtab));
	    /*^apply.arg */
	    argtab[0].meltbp_aptr =
	      (melt_ptr_t *) & /*_.DBGI__V3*/ meltfptr[2];
	    /*^apply.arg */
	    argtab[1].meltbp_long = /*_#I__L11*/ meltfnum[6];
	    /*_.DBG_OUT__V14*/ meltfptr[13] =
	      melt_apply ((meltclosure_ptr_t)
			  (( /*!DBG_OUT */ meltfrout->tabval[3])),
			  (melt_ptr_t) ( /*_.PAIR_TAIL__V13*/ meltfptr[12]),
			  (MELTBPARSTR_PTR MELTBPARSTR_LONG ""), argtab, "",
			  (union meltparam_un *) 0);
	  }
	  ;
	  MELT_LOCATION ("warmelt-debug.melt:987:/ quasiblock");


	  /*_.PROGN___V15*/ meltfptr[14] = /*_.DBG_OUT__V14*/ meltfptr[13];;
	  /*^compute */
	  /*_.IF___V10*/ meltfptr[8] = /*_.PROGN___V15*/ meltfptr[14];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-debug.melt:984:/ clear");
	     /*clear *//*_.PAIR_HEAD__V11*/ meltfptr[10] = 0;
	  /*^clear */
	     /*clear *//*_#I__L10*/ meltfnum[8] = 0;
	  /*^clear */
	     /*clear *//*_.DBG_OUT__V12*/ meltfptr[11] = 0;
	  /*^clear */
	     /*clear *//*_.PAIR_TAIL__V13*/ meltfptr[12] = 0;
	  /*^clear */
	     /*clear *//*_#I__L11*/ meltfnum[6] = 0;
	  /*^clear */
	     /*clear *//*_.DBG_OUT__V14*/ meltfptr[13] = 0;
	  /*^clear */
	     /*clear *//*_.PROGN___V15*/ meltfptr[14] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

  /*_.IF___V10*/ meltfptr[8] = NULL;;
      }
    ;

    {
      MELT_LOCATION ("warmelt-debug.melt:991:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V8*/ meltfptr[7]),
			   (".)"));
    }
    ;

    MELT_LOCATION ("warmelt-debug.melt:975:/ clear");
	   /*clear *//*_.DIS__V6*/ meltfptr[4] = 0;
    /*^clear */
	   /*clear *//*_.DBGI_MAXDEPTH__V7*/ meltfptr[6] = 0;
    /*^clear */
	   /*clear *//*_#MAXDEPTH__L3*/ meltfnum[1] = 0;
    /*^clear */
	   /*clear *//*_.SBUF__V8*/ meltfptr[7] = 0;
    /*^clear */
	   /*clear *//*_#__L4*/ meltfnum[3] = 0;
    /*^clear */
	   /*clear *//*_#MELT_REALLY_NEED_DBGLIM__L5*/ meltfnum[4] = 0;
    /*^clear */
	   /*clear *//*_#OR___L6*/ meltfnum[5] = 0;
    /*^clear */
	   /*clear *//*_.IF___V10*/ meltfptr[8] = 0;
    MELT_LOCATION ("warmelt-debug.melt:973:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V4*/ meltfptr[3] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("DBGOUT_PAIR_METHOD", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_25_warmelt_debug_DBGOUT_PAIR_METHOD_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*noretval */ NULL);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_25_warmelt_debug_DBGOUT_PAIR_METHOD */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_26_warmelt_debug_DBGOUT_MAPOBJECT_METHOD (meltclosure_ptr_t
						   meltclosp_,
						   melt_ptr_t meltfirstargp_,
						   const melt_argdescr_cell_t
						   meltxargdescr_[],
						   union meltparam_un *
						   meltxargtab_,
						   const melt_argdescr_cell_t
						   meltxresdescr_[],
						   union meltparam_un *
						   meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_26_warmelt_debug_DBGOUT_MAPOBJECT_METHOD_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_26_warmelt_debug_DBGOUT_MAPOBJECT_METHOD_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 33
    melt_ptr_t mcfr_varptr[33];
#define MELTFRAM_NBVARNUM 22
    long mcfr_varnum[22];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_26_warmelt_debug_DBGOUT_MAPOBJECT_METHOD is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_26_warmelt_debug_DBGOUT_MAPOBJECT_METHOD_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 33; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_26_warmelt_debug_DBGOUT_MAPOBJECT_METHOD nbval 33*/
  meltfram__.mcfr_nbvar = 33 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("DBGOUT_MAPOBJECT_METHOD", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-debug.melt:998:/ getarg");
 /*_.SELF__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.DBGI__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.DBGI__V3*/ meltfptr[2])) !=
	      NULL);


  /*getarg#2 */
  /*^getarg */
  if (meltxargdescr_[1] != MELTBPAR_LONG)
    goto lab_endgetargs;
 /*_#DEPTH__L1*/ meltfnum[0] = meltxargtab_[1].meltbp_long;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-debug.melt:999:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L2*/ meltfnum[1] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.DBGI__V3*/ meltfptr[2]),
			     (melt_ptr_t) (( /*!CLASS_DEBUG_INFORMATION */
					    meltfrout->tabval[0])));;
      MELT_LOCATION ("warmelt-debug.melt:999:/ cond");
      /*cond */ if ( /*_#IS_A__L2*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V5*/ meltfptr[4] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-debug.melt:999:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check dbgi"),
				  ("warmelt-debug.melt")
				  ? ("warmelt-debug.melt") : __FILE__,
				  (999) ? (999) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V5*/ meltfptr[4] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V4*/ meltfptr[3] = /*_.IFELSE___V5*/ meltfptr[4];;
      /*epilog */

      MELT_LOCATION ("warmelt-debug.melt:999:/ clear");
	     /*clear *//*_#IS_A__L2*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V5*/ meltfptr[4] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V4*/ meltfptr[3] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-debug.melt:1000:/ quasiblock");


 /*_.DIS__V6*/ meltfptr[4] =
      ((melt_ptr_t)
       (melt_discr ((melt_ptr_t) ( /*_.SELF__V2*/ meltfptr[1]))));;
    MELT_LOCATION ("warmelt-debug.melt:1001:/ cond");
    /*cond */ if (
		   /*ifisa */
		   melt_is_instance_of ((melt_ptr_t)
					( /*_.DBGI__V3*/ meltfptr[2]),
					(melt_ptr_t) (( /*!CLASS_DEBUG_INFORMATION */ meltfrout->tabval[0])))
      )				/*then */
      {
	/*^cond.then */
	/*^getslot */
	{
	  melt_ptr_t slot = NULL, obj = NULL;
	  obj = (melt_ptr_t) ( /*_.DBGI__V3*/ meltfptr[2]) /*=obj*/ ;
	  melt_object_get_field (slot, obj, 0, "DBGI_OUT");
   /*_.OUT__V7*/ meltfptr[6] = slot;
	};
	;
      }
    else
      {				/*^cond.else */

  /*_.OUT__V7*/ meltfptr[6] = NULL;;
      }
    ;
    /*^compute */
 /*_#MAPCOUNT__L3*/ meltfnum[1] =
      (melt_count_mapobjects
       ((meltmapobjects_ptr_t) ( /*_.SELF__V2*/ meltfptr[1])));;
    MELT_LOCATION ("warmelt-debug.melt:1003:/ cond");
    /*cond */ if (
		   /*ifisa */
		   melt_is_instance_of ((melt_ptr_t)
					( /*_.DBGI__V3*/ meltfptr[2]),
					(melt_ptr_t) (( /*!CLASS_DEBUG_INFORMATION */ meltfrout->tabval[0])))
      )				/*then */
      {
	/*^cond.then */
	/*^getslot */
	{
	  melt_ptr_t slot = NULL, obj = NULL;
	  obj = (melt_ptr_t) ( /*_.DBGI__V3*/ meltfptr[2]) /*=obj*/ ;
	  melt_object_get_field (slot, obj, 2, "DBGI_MAXDEPTH");
   /*_.DBGI_MAXDEPTH__V8*/ meltfptr[7] = slot;
	};
	;
      }
    else
      {				/*^cond.else */

  /*_.DBGI_MAXDEPTH__V8*/ meltfptr[7] = NULL;;
      }
    ;
    /*^compute */
 /*_#MAXDEPTH__L4*/ meltfnum[3] =
      (melt_get_int ((melt_ptr_t) ( /*_.DBGI_MAXDEPTH__V8*/ meltfptr[7])));;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-debug.melt:1005:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_OUT__L5*/ meltfnum[4] =
	(melt_is_out ((melt_ptr_t) /*_.OUT__V7*/ meltfptr[6]));;
      MELT_LOCATION ("warmelt-debug.melt:1005:/ cond");
      /*cond */ if ( /*_#IS_OUT__L5*/ meltfnum[4])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V10*/ meltfptr[9] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-debug.melt:1005:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check out at start"),
				  ("warmelt-debug.melt")
				  ? ("warmelt-debug.melt") : __FILE__,
				  (1005) ? (1005) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V10*/ meltfptr[9] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V9*/ meltfptr[8] = /*_.IFELSE___V10*/ meltfptr[9];;
      /*epilog */

      MELT_LOCATION ("warmelt-debug.melt:1005:/ clear");
	     /*clear *//*_#IS_OUT__L5*/ meltfnum[4] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V10*/ meltfptr[9] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V9*/ meltfptr[8] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-debug.melt:1006:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#__L6*/ meltfnum[4] =
      (( /*_.DIS__V6*/ meltfptr[4]) ==
       (( /*!DISCR_MAP_OBJECTS */ meltfrout->tabval[1])));;
    MELT_LOCATION ("warmelt-debug.melt:1006:/ cond");
    /*cond */ if ( /*_#__L6*/ meltfnum[4])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{


	  {
	    MELT_LOCATION ("warmelt-debug.melt:1007:/ locexp");
	    meltgc_add_out ((melt_ptr_t) ( /*_.OUT__V7*/ meltfptr[6]),
			    (" {"));
	  }
	  ;
	  /*epilog */
	}
	;
      }
    else
      {
	MELT_LOCATION ("warmelt-debug.melt:1006:/ cond.else");

	/*^block */
	/*anyblock */
	{


	  {
	    MELT_LOCATION ("warmelt-debug.melt:1009:/ locexp");
	    meltgc_add_out ((melt_ptr_t) ( /*_.OUT__V7*/ meltfptr[6]),
			    (" |"));
	  }
	  ;
	  MELT_LOCATION ("warmelt-debug.melt:1010:/ getslot");
	  {
	    melt_ptr_t slot = NULL, obj = NULL;
	    obj = (melt_ptr_t) ( /*_.DIS__V6*/ meltfptr[4]) /*=obj*/ ;
	    melt_object_get_field (slot, obj, 1, "NAMED_NAME");
    /*_.NAMED_NAME__V11*/ meltfptr[9] = slot;
	  };
	  ;

	  {
	    /*^locexp */
	    meltgc_add_out ((melt_ptr_t) ( /*_.OUT__V7*/ meltfptr[6]),
			    melt_string_str ((melt_ptr_t)
					     ( /*_.NAMED_NAME__V11*/
					      meltfptr[9])));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-debug.melt:1011:/ locexp");
	    meltgc_add_out ((melt_ptr_t) ( /*_.OUT__V7*/ meltfptr[6]), ("{"));
	  }
	  ;
	  MELT_LOCATION ("warmelt-debug.melt:1008:/ quasiblock");


	  /*epilog */

	  MELT_LOCATION ("warmelt-debug.melt:1006:/ clear");
	     /*clear *//*_.NAMED_NAME__V11*/ meltfptr[9] = 0;
	}
	;
      }
    ;

    {
      MELT_LOCATION ("warmelt-debug.melt:1012:/ locexp");
      meltgc_add_out ((melt_ptr_t) ( /*_.OUT__V7*/ meltfptr[6]), ("/"));
    }
    ;
 /*_#MAPOBJECT_COUNT__L7*/ meltfnum[6] =
      (melt_count_mapobjects
       ((meltmapobjects_ptr_t) ( /*_.SELF__V2*/ meltfptr[1])));;

    {
      MELT_LOCATION ("warmelt-debug.melt:1013:/ locexp");
      meltgc_add_out_dec ((melt_ptr_t) ( /*_.OUT__V7*/ meltfptr[6]),
			  ( /*_#MAPOBJECT_COUNT__L7*/ meltfnum[6]));
    }
    ;
    MELT_LOCATION ("warmelt-debug.melt:1014:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#I__L8*/ meltfnum[7] =
      (( /*_#DEPTH__L1*/ meltfnum[0]) + (2));;
    /*^compute */
 /*_#MELT_REALLY_NEED_DBGLIM__L9*/ meltfnum[8] =
      ( /*MELT_REALLY_NEED_DBGLIM */
       melt_need_debug_limit ((int) /*_#I__L8*/ meltfnum[7],
			      (int) /*_#MAXDEPTH__L4*/ meltfnum[3]));;
    MELT_LOCATION ("warmelt-debug.melt:1014:/ cond");
    /*cond */ if ( /*_#MELT_REALLY_NEED_DBGLIM__L9*/ meltfnum[8])	/*then */
      {
	/*^cond.then */
	/*_#OR___L10*/ meltfnum[9] =
	  /*_#MELT_REALLY_NEED_DBGLIM__L9*/ meltfnum[8];;
      }
    else
      {
	MELT_LOCATION ("warmelt-debug.melt:1014:/ cond.else");

	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-debug.melt:1016:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
   /*_#I__L11*/ meltfnum[10] =
	    (( /*_#DEPTH__L1*/ meltfnum[0]) + (2));;
	  /*^compute */
   /*_#I__L12*/ meltfnum[11] =
	    (( /*_#I__L11*/ meltfnum[10]) <
	     ( /*_#MAXDEPTH__L4*/ meltfnum[3]));;
	  MELT_LOCATION ("warmelt-debug.melt:1016:/ cond");
	  /*cond */ if ( /*_#I__L12*/ meltfnum[11])	/*then */
	    {
	      /*^cond.then */
	      /*^block */
	      /*anyblock */
	      {

     /*_#IS_A__L14*/ meltfnum[13] =
		  melt_is_instance_of ((melt_ptr_t)
				       ( /*_.DBGI__V3*/ meltfptr[2]),
				       (melt_ptr_t) (( /*!CLASS_DEBUG_OUTPUT_INFORMATION */ meltfrout->tabval[2])));;
		/*^compute */
		/*_#IF___L13*/ meltfnum[12] = /*_#IS_A__L14*/ meltfnum[13];;
		/*epilog */

		MELT_LOCATION ("warmelt-debug.melt:1016:/ clear");
	       /*clear *//*_#IS_A__L14*/ meltfnum[13] = 0;
	      }
	      ;
	    }
	  else
	    {			/*^cond.else */

    /*_#IF___L13*/ meltfnum[12] = 0;;
	    }
	  ;
	  /*^compute */
	  /*_#OR___L10*/ meltfnum[9] = /*_#IF___L13*/ meltfnum[12];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-debug.melt:1014:/ clear");
	     /*clear *//*_#I__L11*/ meltfnum[10] = 0;
	  /*^clear */
	     /*clear *//*_#I__L12*/ meltfnum[11] = 0;
	  /*^clear */
	     /*clear *//*_#IF___L13*/ meltfnum[12] = 0;
	}
	;
      }
    ;
    /*^cond */
    /*cond */ if ( /*_#OR___L10*/ meltfnum[9])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-debug.melt:1017:/ quasiblock");


   /*_#I__L15*/ meltfnum[13] =
	    ((2) + ( /*_#DEPTH__L1*/ meltfnum[0]));;
	  /*^compute */
   /*_.NEXTDEPTHBOX__V14*/ meltfptr[13] =
	    (meltgc_new_int
	     ((meltobject_ptr_t)
	      (( /*!DISCR_INTEGER */ meltfrout->tabval[3])),
	      ( /*_#I__L15*/ meltfnum[13])));;
	  /*^compute */
   /*_.COUNTBOX__V15*/ meltfptr[14] =
	    (meltgc_new_int
	     ((meltobject_ptr_t)
	      (( /*!DISCR_INTEGER */ meltfrout->tabval[3])), (0)));;
	  /*^compute */
   /*_.TUPL__V16*/ meltfptr[15] =
	    (meltgc_new_multiple
	     ((meltobject_ptr_t)
	      (( /*!DISCR_MULTIPLE */ meltfrout->tabval[4])),
	      ( /*_#MAPCOUNT__L3*/ meltfnum[1])));;
	  /*^compute */
   /*_.AUX__V17*/ meltfptr[16] =
	    melt_auxdata_mapobjects ((melt_ptr_t)
				     ( /*_.SELF__V2*/ meltfptr[1]));;
	  MELT_LOCATION ("warmelt-debug.melt:1023:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^checksignal */
	  MELT_CHECK_SIGNAL ();
	  ;
   /*_#NOTNULL__L16*/ meltfnum[10] =
	    (( /*_.AUX__V17*/ meltfptr[16]) != NULL);;
	  MELT_LOCATION ("warmelt-debug.melt:1023:/ cond");
	  /*cond */ if ( /*_#NOTNULL__L16*/ meltfnum[10])	/*then */
	    {
	      /*^cond.then */
	      /*^block */
	      /*anyblock */
	      {

     /*_#I__L18*/ meltfnum[12] =
		  (( /*_#DEPTH__L1*/ meltfnum[0]) <= (1));;
		/*^compute */
		/*_#IF___L17*/ meltfnum[11] = /*_#I__L18*/ meltfnum[12];;
		/*epilog */

		MELT_LOCATION ("warmelt-debug.melt:1023:/ clear");
	       /*clear *//*_#I__L18*/ meltfnum[12] = 0;
	      }
	      ;
	    }
	  else
	    {			/*^cond.else */

    /*_#IF___L17*/ meltfnum[11] = 0;;
	    }
	  ;
	  MELT_LOCATION ("warmelt-debug.melt:1023:/ cond");
	  /*cond */ if ( /*_#IF___L17*/ meltfnum[11])	/*then */
	    {
	      /*^cond.then */
	      /*^block */
	      /*anyblock */
	      {

		MELT_LOCATION ("warmelt-debug.melt:1027:/ checksignal");
		MELT_CHECK_SIGNAL ();
		;
		/*^apply */
		/*apply */
		{
		  union meltparam_un argtab[1];
		  memset (&argtab, 0, sizeof (argtab));
		  /*^apply.arg */
		  argtab[0].meltbp_cstring = " aux:";
		  /*_.ADD2OUT__V18*/ meltfptr[17] =
		    melt_apply ((meltclosure_ptr_t)
				(( /*!ADD2OUT */ meltfrout->tabval[5])),
				(melt_ptr_t) ( /*_.OUT__V7*/ meltfptr[6]),
				(MELTBPARSTR_CSTRING ""), argtab, "",
				(union meltparam_un *) 0);
		}
		;
     /*_#I__L19*/ meltfnum[12] =
		  (( /*_#DEPTH__L1*/ meltfnum[0]) + (3));;
		MELT_LOCATION ("warmelt-debug.melt:1028:/ checksignal");
		MELT_CHECK_SIGNAL ();
		;
		/*^apply */
		/*apply */
		{
		  union meltparam_un argtab[2];
		  memset (&argtab, 0, sizeof (argtab));
		  /*^apply.arg */
		  argtab[0].meltbp_aptr =
		    (melt_ptr_t *) & /*_.DBGI__V3*/ meltfptr[2];
		  /*^apply.arg */
		  argtab[1].meltbp_long = /*_#I__L19*/ meltfnum[12];
		  /*_.DBG_OUT__V19*/ meltfptr[18] =
		    melt_apply ((meltclosure_ptr_t)
				(( /*!DBG_OUT */ meltfrout->tabval[6])),
				(melt_ptr_t) ( /*_.AUX__V17*/ meltfptr[16]),
				(MELTBPARSTR_PTR MELTBPARSTR_LONG ""), argtab,
				"", (union meltparam_un *) 0);
		}
		;
     /*_#I__L20*/ meltfnum[19] =
		  (( /*_#DEPTH__L1*/ meltfnum[0]) + (1));;

		{
		  MELT_LOCATION ("warmelt-debug.melt:1029:/ locexp");
		  meltgc_out_add_indent ((melt_ptr_t)
					 ( /*_.OUT__V7*/ meltfptr[6]),
					 ( /*_#I__L20*/ meltfnum[19]), 64);;
		}
		;
		MELT_LOCATION ("warmelt-debug.melt:1026:/ quasiblock");


		/*epilog */

		MELT_LOCATION ("warmelt-debug.melt:1023:/ clear");
	       /*clear *//*_.ADD2OUT__V18*/ meltfptr[17] = 0;
		/*^clear */
	       /*clear *//*_#I__L19*/ meltfnum[12] = 0;
		/*^clear */
	       /*clear *//*_.DBG_OUT__V19*/ meltfptr[18] = 0;
		/*^clear */
	       /*clear *//*_#I__L20*/ meltfnum[19] = 0;
	      }
	      ;
	    }			/*noelse */
	  ;
	  MELT_LOCATION ("warmelt-debug.melt:1034:/ quasiblock");


	  /*^newclosure */
		   /*newclosure *//*_.LAMBDA___V21*/ meltfptr[18] =
	    (melt_ptr_t)
	    meltgc_new_closure ((meltobject_ptr_t)
				(((melt_ptr_t)
				  (MELT_PREDEF (DISCR_CLOSURE)))),
				(meltroutine_ptr_t) (( /*!konst_9 */
						      meltfrout->tabval[9])),
				(2));
	  ;
	  /*^putclosedv */
	  /*putclosv */
	  melt_assertmsg ("putclosv checkclo",
			  melt_magic_discr ((melt_ptr_t)
					    ( /*_.LAMBDA___V21*/
					     meltfptr[18])) ==
			  MELTOBMAG_CLOSURE);
	  melt_assertmsg ("putclosv checkoff", 0 >= 0
			  && 0 <
			  melt_closure_size ((melt_ptr_t)
					     ( /*_.LAMBDA___V21*/
					      meltfptr[18])));
	  ((meltclosure_ptr_t) /*_.LAMBDA___V21*/ meltfptr[18])->tabval[0] =
	    (melt_ptr_t) ( /*_.COUNTBOX__V15*/ meltfptr[14]);
	  ;
	  /*^putclosedv */
	  /*putclosv */
	  melt_assertmsg ("putclosv checkclo",
			  melt_magic_discr ((melt_ptr_t)
					    ( /*_.LAMBDA___V21*/
					     meltfptr[18])) ==
			  MELTOBMAG_CLOSURE);
	  melt_assertmsg ("putclosv checkoff", 1 >= 0
			  && 1 <
			  melt_closure_size ((melt_ptr_t)
					     ( /*_.LAMBDA___V21*/
					      meltfptr[18])));
	  ((meltclosure_ptr_t) /*_.LAMBDA___V21*/ meltfptr[18])->tabval[1] =
	    (melt_ptr_t) ( /*_.TUPL__V16*/ meltfptr[15]);
	  ;
	  /*_.LAMBDA___V20*/ meltfptr[17] = /*_.LAMBDA___V21*/ meltfptr[18];;
	  MELT_LOCATION ("warmelt-debug.melt:1032:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^apply */
	  /*apply */
	  {
	    union meltparam_un argtab[1];
	    memset (&argtab, 0, sizeof (argtab));
	    /*^apply.arg */
	    argtab[0].meltbp_aptr =
	      (melt_ptr_t *) & /*_.LAMBDA___V20*/ meltfptr[17];
	    /*_.MAPOBJECT_EVERY__V22*/ meltfptr[21] =
	      melt_apply ((meltclosure_ptr_t)
			  (( /*!MAPOBJECT_EVERY */ meltfrout->tabval[7])),
			  (melt_ptr_t) ( /*_.SELF__V2*/ meltfptr[1]),
			  (MELTBPARSTR_PTR ""), argtab, "",
			  (union meltparam_un *) 0);
	  }
	  ;

#if MELT_HAVE_DEBUG
	  MELT_LOCATION ("warmelt-debug.melt:1042:/ cppif.then");
	  /*^block */
	  /*anyblock */
	  {

	    /*^checksignal */
	    MELT_CHECK_SIGNAL ();
	    ;
     /*_#IS_MULTIPLE__L21*/ meltfnum[12] =
	      (melt_magic_discr ((melt_ptr_t) ( /*_.TUPL__V16*/ meltfptr[15]))
	       == MELTOBMAG_MULTIPLE);;
	    MELT_LOCATION ("warmelt-debug.melt:1042:/ cond");
	    /*cond */ if ( /*_#IS_MULTIPLE__L21*/ meltfnum[12])	/*then */
	      {
		/*^cond.then */
		/*_.IFELSE___V24*/ meltfptr[23] = ( /*nil */ NULL);;
	      }
	    else
	      {
		MELT_LOCATION ("warmelt-debug.melt:1042:/ cond.else");

		/*^block */
		/*anyblock */
		{




		  {
		    /*^locexp */
		    melt_assert_failed (("check tupl"),
					("warmelt-debug.melt")
					? ("warmelt-debug.melt") : __FILE__,
					(1042) ? (1042) : __LINE__,
					__FUNCTION__);
		    ;
		  }
		  ;
		 /*clear *//*_.IFELSE___V24*/ meltfptr[23] = 0;
		  /*epilog */
		}
		;
	      }
	    ;
	    /*^compute */
	    /*_.IFCPP___V23*/ meltfptr[22] = /*_.IFELSE___V24*/ meltfptr[23];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-debug.melt:1042:/ clear");
	       /*clear *//*_#IS_MULTIPLE__L21*/ meltfnum[12] = 0;
	    /*^clear */
	       /*clear *//*_.IFELSE___V24*/ meltfptr[23] = 0;
	  }

#else /*MELT_HAVE_DEBUG */
	  /*^cppif.else */
	  /*_.IFCPP___V23*/ meltfptr[22] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
	  ;
	  MELT_LOCATION ("warmelt-debug.melt:1044:/ quasiblock");


	  MELT_LOCATION ("warmelt-debug.melt:1047:/ quasiblock");


	  /*^newclosure */
		   /*newclosure *//*_.LAMBDA___V27*/ meltfptr[26] =
	    (melt_ptr_t)
	    meltgc_new_closure ((meltobject_ptr_t)
				(((melt_ptr_t)
				  (MELT_PREDEF (DISCR_CLOSURE)))),
				(meltroutine_ptr_t) (( /*!konst_11 */
						      meltfrout->tabval[11])),
				(0));
	  ;
	  /*_.LAMBDA___V26*/ meltfptr[25] = /*_.LAMBDA___V27*/ meltfptr[26];;
	  /*^compute */
   /*_.SORTUPL__V28*/ meltfptr[27] =
	    meltgc_sort_multiple ((melt_ptr_t)
				  ( /*_.TUPL__V16*/ meltfptr[15]),
				  (melt_ptr_t) ( /*_.LAMBDA___V26*/
						meltfptr[25]),
				  (melt_ptr_t) (( /*!DISCR_MULTIPLE */
						 meltfrout->tabval[4])));;

#if MELT_HAVE_DEBUG
	  MELT_LOCATION ("warmelt-debug.melt:1060:/ cppif.then");
	  /*^block */
	  /*anyblock */
	  {

	    /*^checksignal */
	    MELT_CHECK_SIGNAL ();
	    ;
     /*_#IS_MULTIPLE__L22*/ meltfnum[19] =
	      (melt_magic_discr
	       ((melt_ptr_t) ( /*_.SORTUPL__V28*/ meltfptr[27])) ==
	       MELTOBMAG_MULTIPLE);;
	    MELT_LOCATION ("warmelt-debug.melt:1060:/ cond");
	    /*cond */ if ( /*_#IS_MULTIPLE__L22*/ meltfnum[19])	/*then */
	      {
		/*^cond.then */
		/*_.IFELSE___V30*/ meltfptr[29] = ( /*nil */ NULL);;
	      }
	    else
	      {
		MELT_LOCATION ("warmelt-debug.melt:1060:/ cond.else");

		/*^block */
		/*anyblock */
		{




		  {
		    /*^locexp */
		    melt_assert_failed (("check sortupl"),
					("warmelt-debug.melt")
					? ("warmelt-debug.melt") : __FILE__,
					(1060) ? (1060) : __LINE__,
					__FUNCTION__);
		    ;
		  }
		  ;
		 /*clear *//*_.IFELSE___V30*/ meltfptr[29] = 0;
		  /*epilog */
		}
		;
	      }
	    ;
	    /*^compute */
	    /*_.IFCPP___V29*/ meltfptr[28] = /*_.IFELSE___V30*/ meltfptr[29];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-debug.melt:1060:/ clear");
	       /*clear *//*_#IS_MULTIPLE__L22*/ meltfnum[19] = 0;
	    /*^clear */
	       /*clear *//*_.IFELSE___V30*/ meltfptr[29] = 0;
	  }

#else /*MELT_HAVE_DEBUG */
	  /*^cppif.else */
	  /*_.IFCPP___V29*/ meltfptr[28] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
	  ;
	  MELT_LOCATION ("warmelt-debug.melt:1063:/ quasiblock");


	  /*^newclosure */
		   /*newclosure *//*_.LAMBDA___V32*/ meltfptr[31] =
	    (melt_ptr_t)
	    meltgc_new_closure ((meltobject_ptr_t)
				(((melt_ptr_t)
				  (MELT_PREDEF (DISCR_CLOSURE)))),
				(meltroutine_ptr_t) (( /*!konst_15 */
						      meltfrout->tabval[15])),
				(3));
	  ;
	  /*^putclosedv */
	  /*putclosv */
	  melt_assertmsg ("putclosv checkclo",
			  melt_magic_discr ((melt_ptr_t)
					    ( /*_.LAMBDA___V32*/
					     meltfptr[31])) ==
			  MELTOBMAG_CLOSURE);
	  melt_assertmsg ("putclosv checkoff", 0 >= 0
			  && 0 <
			  melt_closure_size ((melt_ptr_t)
					     ( /*_.LAMBDA___V32*/
					      meltfptr[31])));
	  ((meltclosure_ptr_t) /*_.LAMBDA___V32*/ meltfptr[31])->tabval[0] =
	    (melt_ptr_t) ( /*_.NEXTDEPTHBOX__V14*/ meltfptr[13]);
	  ;
	  /*^putclosedv */
	  /*putclosv */
	  melt_assertmsg ("putclosv checkclo",
			  melt_magic_discr ((melt_ptr_t)
					    ( /*_.LAMBDA___V32*/
					     meltfptr[31])) ==
			  MELTOBMAG_CLOSURE);
	  melt_assertmsg ("putclosv checkoff", 1 >= 0
			  && 1 <
			  melt_closure_size ((melt_ptr_t)
					     ( /*_.LAMBDA___V32*/
					      meltfptr[31])));
	  ((meltclosure_ptr_t) /*_.LAMBDA___V32*/ meltfptr[31])->tabval[1] =
	    (melt_ptr_t) ( /*_.DBGI__V3*/ meltfptr[2]);
	  ;
	  /*^putclosedv */
	  /*putclosv */
	  melt_assertmsg ("putclosv checkclo",
			  melt_magic_discr ((melt_ptr_t)
					    ( /*_.LAMBDA___V32*/
					     meltfptr[31])) ==
			  MELTOBMAG_CLOSURE);
	  melt_assertmsg ("putclosv checkoff", 2 >= 0
			  && 2 <
			  melt_closure_size ((melt_ptr_t)
					     ( /*_.LAMBDA___V32*/
					      meltfptr[31])));
	  ((meltclosure_ptr_t) /*_.LAMBDA___V32*/ meltfptr[31])->tabval[2] =
	    (melt_ptr_t) ( /*_.OUT__V7*/ meltfptr[6]);
	  ;
	  /*_.LAMBDA___V31*/ meltfptr[29] = /*_.LAMBDA___V32*/ meltfptr[31];;
	  MELT_LOCATION ("warmelt-debug.melt:1061:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^apply */
	  /*apply */
	  {
	    union meltparam_un argtab[1];
	    memset (&argtab, 0, sizeof (argtab));
	    /*^apply.arg */
	    argtab[0].meltbp_aptr =
	      (melt_ptr_t *) & /*_.LAMBDA___V31*/ meltfptr[29];
	    /*_.MULTIPLE_EVERY__V33*/ meltfptr[32] =
	      melt_apply ((meltclosure_ptr_t)
			  (( /*!MULTIPLE_EVERY */ meltfrout->tabval[12])),
			  (melt_ptr_t) ( /*_.SORTUPL__V28*/ meltfptr[27]),
			  (MELTBPARSTR_PTR ""), argtab, "",
			  (union meltparam_un *) 0);
	  }
	  ;
	  /*_.LET___V25*/ meltfptr[23] =
	    /*_.MULTIPLE_EVERY__V33*/ meltfptr[32];;

	  MELT_LOCATION ("warmelt-debug.melt:1044:/ clear");
	     /*clear *//*_.LAMBDA___V26*/ meltfptr[25] = 0;
	  /*^clear */
	     /*clear *//*_.SORTUPL__V28*/ meltfptr[27] = 0;
	  /*^clear */
	     /*clear *//*_.IFCPP___V29*/ meltfptr[28] = 0;
	  /*^clear */
	     /*clear *//*_.LAMBDA___V31*/ meltfptr[29] = 0;
	  /*^clear */
	     /*clear *//*_.MULTIPLE_EVERY__V33*/ meltfptr[32] = 0;
	  /*_.LET___V13*/ meltfptr[12] = /*_.LET___V25*/ meltfptr[23];;

	  MELT_LOCATION ("warmelt-debug.melt:1017:/ clear");
	     /*clear *//*_#I__L15*/ meltfnum[13] = 0;
	  /*^clear */
	     /*clear *//*_.NEXTDEPTHBOX__V14*/ meltfptr[13] = 0;
	  /*^clear */
	     /*clear *//*_.COUNTBOX__V15*/ meltfptr[14] = 0;
	  /*^clear */
	     /*clear *//*_.TUPL__V16*/ meltfptr[15] = 0;
	  /*^clear */
	     /*clear *//*_.AUX__V17*/ meltfptr[16] = 0;
	  /*^clear */
	     /*clear *//*_#NOTNULL__L16*/ meltfnum[10] = 0;
	  /*^clear */
	     /*clear *//*_#IF___L17*/ meltfnum[11] = 0;
	  /*^clear */
	     /*clear *//*_.LAMBDA___V20*/ meltfptr[17] = 0;
	  /*^clear */
	     /*clear *//*_.MAPOBJECT_EVERY__V22*/ meltfptr[21] = 0;
	  /*^clear */
	     /*clear *//*_.IFCPP___V23*/ meltfptr[22] = 0;
	  /*^clear */
	     /*clear *//*_.LET___V25*/ meltfptr[23] = 0;
	  /*_.IF___V12*/ meltfptr[9] = /*_.LET___V13*/ meltfptr[12];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-debug.melt:1014:/ clear");
	     /*clear *//*_.LET___V13*/ meltfptr[12] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

  /*_.IF___V12*/ meltfptr[9] = NULL;;
      }
    ;

    {
      MELT_LOCATION ("warmelt-debug.melt:1081:/ locexp");
      meltgc_add_out ((melt_ptr_t) ( /*_.OUT__V7*/ meltfptr[6]), ("}"));
    }
    ;

    MELT_LOCATION ("warmelt-debug.melt:1000:/ clear");
	   /*clear *//*_.DIS__V6*/ meltfptr[4] = 0;
    /*^clear */
	   /*clear *//*_.OUT__V7*/ meltfptr[6] = 0;
    /*^clear */
	   /*clear *//*_#MAPCOUNT__L3*/ meltfnum[1] = 0;
    /*^clear */
	   /*clear *//*_.DBGI_MAXDEPTH__V8*/ meltfptr[7] = 0;
    /*^clear */
	   /*clear *//*_#MAXDEPTH__L4*/ meltfnum[3] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V9*/ meltfptr[8] = 0;
    /*^clear */
	   /*clear *//*_#__L6*/ meltfnum[4] = 0;
    /*^clear */
	   /*clear *//*_#MAPOBJECT_COUNT__L7*/ meltfnum[6] = 0;
    /*^clear */
	   /*clear *//*_#I__L8*/ meltfnum[7] = 0;
    /*^clear */
	   /*clear *//*_#MELT_REALLY_NEED_DBGLIM__L9*/ meltfnum[8] = 0;
    /*^clear */
	   /*clear *//*_#OR___L10*/ meltfnum[9] = 0;
    /*^clear */
	   /*clear *//*_.IF___V12*/ meltfptr[9] = 0;
    MELT_LOCATION ("warmelt-debug.melt:998:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V4*/ meltfptr[3] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("DBGOUT_MAPOBJECT_METHOD", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_26_warmelt_debug_DBGOUT_MAPOBJECT_METHOD_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*noretval */ NULL);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_26_warmelt_debug_DBGOUT_MAPOBJECT_METHOD */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_27_warmelt_debug_LAMBDA___2__ (meltclosure_ptr_t meltclosp_,
					melt_ptr_t meltfirstargp_,
					const melt_argdescr_cell_t
					meltxargdescr_[],
					union meltparam_un * meltxargtab_,
					const melt_argdescr_cell_t
					meltxresdescr_[],
					union meltparam_un * meltxrestab_)
{
  long current_blocklevel_signals_meltrout_27_warmelt_debug_LAMBDA___2___melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_27_warmelt_debug_LAMBDA___2___st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 6
    melt_ptr_t mcfr_varptr[6];
#define MELTFRAM_NBVARNUM 2
    long mcfr_varnum[2];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_27_warmelt_debug_LAMBDA___2__ is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_27_warmelt_debug_LAMBDA___2___st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 6; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_27_warmelt_debug_LAMBDA___2__ nbval 6*/
  meltfram__.mcfr_nbvar = 6 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("LAMBDA_", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-debug.melt:1034:/ getarg");
 /*_.AT__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.VA__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.VA__V3*/ meltfptr[2])) != NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {

    MELT_LOCATION ("warmelt-debug.melt:1035:/ quasiblock");


 /*_#CURCOUNT__L1*/ meltfnum[0] =
      (melt_get_int ((melt_ptr_t) (( /*~COUNTBOX */ meltfclos->tabval[0]))));;
    /*^compute */
 /*_.MAKE_INTEGERBOX__V4*/ meltfptr[3] =
      (meltgc_new_int
       ((meltobject_ptr_t) (( /*!DISCR_INTEGER */ meltfrout->tabval[0])),
	( /*_#CURCOUNT__L1*/ meltfnum[0])));;
    MELT_LOCATION ("warmelt-debug.melt:1036:/ blockmultialloc");
    /*multiallocblock */
    {
      struct meltletrec_1_st
      {
	struct MELT_MULTIPLE_STRUCT (3) rtup_0__TUPLREC__x1;
	long meltletrec_1_endgap;
      } *meltletrec_1_ptr = 0;
      meltletrec_1_ptr =
	(struct meltletrec_1_st *)
	meltgc_allocate (sizeof (struct meltletrec_1_st), 0);
      /*^blockmultialloc.initfill */
      /*inimult rtup_0__TUPLREC__x1 */
 /*_.TUPLREC___V6*/ meltfptr[5] =
	(melt_ptr_t) & meltletrec_1_ptr->rtup_0__TUPLREC__x1;
      meltletrec_1_ptr->rtup_0__TUPLREC__x1.discr =
	(meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (DISCR_MULTIPLE))));
      meltletrec_1_ptr->rtup_0__TUPLREC__x1.nbval = 3;


      /*^putuple */
      /*putupl#1 */
      melt_assertmsg ("putupl [:1036] #1 checktup",
		      melt_magic_discr ((melt_ptr_t)
					( /*_.TUPLREC___V6*/ meltfptr[5])) ==
		      MELTOBMAG_MULTIPLE);
      melt_assertmsg ("putupl [:1036] #1 checkoff",
		      (0 >= 0
		       && 0 <
		       melt_multiple_length ((melt_ptr_t)
					     ( /*_.TUPLREC___V6*/
					      meltfptr[5]))));
      ((meltmultiple_ptr_t) ( /*_.TUPLREC___V6*/ meltfptr[5]))->tabval[0] =
	(melt_ptr_t) ( /*_.AT__V2*/ meltfptr[1]);
      ;
      /*^putuple */
      /*putupl#2 */
      melt_assertmsg ("putupl [:1036] #2 checktup",
		      melt_magic_discr ((melt_ptr_t)
					( /*_.TUPLREC___V6*/ meltfptr[5])) ==
		      MELTOBMAG_MULTIPLE);
      melt_assertmsg ("putupl [:1036] #2 checkoff",
		      (1 >= 0
		       && 1 <
		       melt_multiple_length ((melt_ptr_t)
					     ( /*_.TUPLREC___V6*/
					      meltfptr[5]))));
      ((meltmultiple_ptr_t) ( /*_.TUPLREC___V6*/ meltfptr[5]))->tabval[1] =
	(melt_ptr_t) ( /*_.VA__V3*/ meltfptr[2]);
      ;
      /*^putuple */
      /*putupl#3 */
      melt_assertmsg ("putupl [:1036] #3 checktup",
		      melt_magic_discr ((melt_ptr_t)
					( /*_.TUPLREC___V6*/ meltfptr[5])) ==
		      MELTOBMAG_MULTIPLE);
      melt_assertmsg ("putupl [:1036] #3 checkoff",
		      (2 >= 0
		       && 2 <
		       melt_multiple_length ((melt_ptr_t)
					     ( /*_.TUPLREC___V6*/
					      meltfptr[5]))));
      ((meltmultiple_ptr_t) ( /*_.TUPLREC___V6*/ meltfptr[5]))->tabval[2] =
	(melt_ptr_t) ( /*_.MAKE_INTEGERBOX__V4*/ meltfptr[3]);
      ;
      /*^touch */
      meltgc_touch ( /*_.TUPLREC___V6*/ meltfptr[5]);
      ;
      /*_.ENT__V5*/ meltfptr[4] = /*_.TUPLREC___V6*/ meltfptr[5];;
      /*epilog */

      MELT_LOCATION ("warmelt-debug.melt:1036:/ clear");
	    /*clear *//*_.TUPLREC___V6*/ meltfptr[5] = 0;
      /*^clear */
	    /*clear *//*_.TUPLREC___V6*/ meltfptr[5] = 0;
    }				/*end multiallocblock */
    ;

    {
      MELT_LOCATION ("warmelt-debug.melt:1039:/ locexp");
      meltgc_multiple_put_nth ((melt_ptr_t)
			       (( /*~TUPL */ meltfclos->tabval[1])),
			       ( /*_#CURCOUNT__L1*/ meltfnum[0]),
			       (melt_ptr_t) ( /*_.ENT__V5*/ meltfptr[4]));
    }
    ;
 /*_#I__L2*/ meltfnum[1] =
      (( /*_#CURCOUNT__L1*/ meltfnum[0]) + (1));;

    {
      MELT_LOCATION ("warmelt-debug.melt:1040:/ locexp");
      melt_put_int ((melt_ptr_t) (( /*~COUNTBOX */ meltfclos->tabval[0])),
		    ( /*_#I__L2*/ meltfnum[1]));
    }
    ;

    MELT_LOCATION ("warmelt-debug.melt:1035:/ clear");
	   /*clear *//*_#CURCOUNT__L1*/ meltfnum[0] = 0;
    /*^clear */
	   /*clear *//*_.MAKE_INTEGERBOX__V4*/ meltfptr[3] = 0;
    /*^clear */
	   /*clear *//*_.ENT__V5*/ meltfptr[4] = 0;
    /*^clear */
	   /*clear *//*_#I__L2*/ meltfnum[1] = 0;
    /*epilog */
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("LAMBDA_", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_27_warmelt_debug_LAMBDA___2___melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*noretval */ NULL);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_27_warmelt_debug_LAMBDA___2__ */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_28_warmelt_debug_LAMBDA___3__ (meltclosure_ptr_t meltclosp_,
					melt_ptr_t meltfirstargp_,
					const melt_argdescr_cell_t
					meltxargdescr_[],
					union meltparam_un * meltxargtab_,
					const melt_argdescr_cell_t
					meltxresdescr_[],
					union meltparam_un * meltxrestab_)
{
  long current_blocklevel_signals_meltrout_28_warmelt_debug_LAMBDA___3___melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_28_warmelt_debug_LAMBDA___3___st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 11
    melt_ptr_t mcfr_varptr[11];
/*no varnum*/
#define MELTFRAM_NBVARNUM /*none*/0
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_28_warmelt_debug_LAMBDA___3__ is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_28_warmelt_debug_LAMBDA___3___st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 11; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_28_warmelt_debug_LAMBDA___3__ nbval 11*/
  meltfram__.mcfr_nbvar = 11 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("LAMBDA_", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-debug.melt:1047:/ getarg");
 /*_.E1__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.E2__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.E2__V3*/ meltfptr[2])) != NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {

    MELT_LOCATION ("warmelt-debug.melt:1048:/ quasiblock");


 /*_.E1AT__V5*/ meltfptr[4] =
      (melt_multiple_nth ((melt_ptr_t) ( /*_.E1__V2*/ meltfptr[1]), (0)));;
    /*^compute */
 /*_.E1VA__V6*/ meltfptr[5] =
      (melt_multiple_nth ((melt_ptr_t) ( /*_.E1__V2*/ meltfptr[1]), (1)));;
    /*^compute */
 /*_.E1RK__V7*/ meltfptr[6] =
      (melt_multiple_nth ((melt_ptr_t) ( /*_.E1__V2*/ meltfptr[1]), (2)));;
    /*^compute */
 /*_.E2AT__V8*/ meltfptr[7] =
      (melt_multiple_nth ((melt_ptr_t) ( /*_.E2__V3*/ meltfptr[2]), (0)));;
    /*^compute */
 /*_.E2VA__V9*/ meltfptr[8] =
      (melt_multiple_nth ((melt_ptr_t) ( /*_.E2__V3*/ meltfptr[2]), (1)));;
    /*^compute */
 /*_.E2RK__V10*/ meltfptr[9] =
      (melt_multiple_nth ((melt_ptr_t) ( /*_.E2__V3*/ meltfptr[2]), (2)));;
    MELT_LOCATION ("warmelt-debug.melt:1055:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[6];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.E1RK__V7*/ meltfptr[6];
      /*^apply.arg */
      argtab[1].meltbp_aptr = (melt_ptr_t *) & /*_.E2AT__V8*/ meltfptr[7];
      /*^apply.arg */
      argtab[2].meltbp_aptr = (melt_ptr_t *) & /*_.E2RK__V10*/ meltfptr[9];
      /*^apply.arg */
      argtab[3].meltbp_aptr =
	(melt_ptr_t *) & ( /*!konst_1 */ meltfrout->tabval[1]);
      /*^apply.arg */
      argtab[4].meltbp_aptr =
	(melt_ptr_t *) & ( /*!konst_2 */ meltfrout->tabval[2]);
      /*^apply.arg */
      argtab[5].meltbp_aptr =
	(melt_ptr_t *) & ( /*!konst_3 */ meltfrout->tabval[3]);
      /*_.COMPARE_OBJ_RANKED__V11*/ meltfptr[10] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!COMPARE_OBJ_RANKED */ meltfrout->tabval[0])),
		    (melt_ptr_t) ( /*_.E1AT__V5*/ meltfptr[4]),
		    (MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_PTR
		     MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_PTR ""),
		    argtab, "", (union meltparam_un *) 0);
    }
    ;
    /*_.LET___V4*/ meltfptr[3] = /*_.COMPARE_OBJ_RANKED__V11*/ meltfptr[10];;

    MELT_LOCATION ("warmelt-debug.melt:1048:/ clear");
	   /*clear *//*_.E1AT__V5*/ meltfptr[4] = 0;
    /*^clear */
	   /*clear *//*_.E1VA__V6*/ meltfptr[5] = 0;
    /*^clear */
	   /*clear *//*_.E1RK__V7*/ meltfptr[6] = 0;
    /*^clear */
	   /*clear *//*_.E2AT__V8*/ meltfptr[7] = 0;
    /*^clear */
	   /*clear *//*_.E2VA__V9*/ meltfptr[8] = 0;
    /*^clear */
	   /*clear *//*_.E2RK__V10*/ meltfptr[9] = 0;
    /*^clear */
	   /*clear *//*_.COMPARE_OBJ_RANKED__V11*/ meltfptr[10] = 0;
    MELT_LOCATION ("warmelt-debug.melt:1047:/ quasiblock");


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.LET___V4*/ meltfptr[3];;

    {
      MELT_LOCATION ("warmelt-debug.melt:1047:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.LET___V4*/ meltfptr[3] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("LAMBDA_", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_28_warmelt_debug_LAMBDA___3___melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_28_warmelt_debug_LAMBDA___3__ */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_29_warmelt_debug_LAMBDA___4__ (meltclosure_ptr_t meltclosp_,
					melt_ptr_t meltfirstargp_,
					const melt_argdescr_cell_t
					meltxargdescr_[],
					union meltparam_un * meltxargtab_,
					const melt_argdescr_cell_t
					meltxresdescr_[],
					union meltparam_un * meltxrestab_)
{
  long current_blocklevel_signals_meltrout_29_warmelt_debug_LAMBDA___4___melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_29_warmelt_debug_LAMBDA___4___st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 11
    melt_ptr_t mcfr_varptr[11];
#define MELTFRAM_NBVARNUM 6
    long mcfr_varnum[6];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_29_warmelt_debug_LAMBDA___4__ is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_29_warmelt_debug_LAMBDA___4___st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 11; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_29_warmelt_debug_LAMBDA___4__ nbval 11*/
  meltfram__.mcfr_nbvar = 11 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("LAMBDA_", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-debug.melt:1063:/ getarg");
 /*_.EL__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_LONG)
    goto lab_endgetargs;
 /*_#IX__L1*/ meltfnum[0] = meltxargtab_[0].meltbp_long;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {

    MELT_LOCATION ("warmelt-debug.melt:1064:/ quasiblock");


 /*_.ELAT__V3*/ meltfptr[2] =
      (melt_multiple_nth ((melt_ptr_t) ( /*_.EL__V2*/ meltfptr[1]), (0)));;
    /*^compute */
 /*_.ELVA__V4*/ meltfptr[3] =
      (melt_multiple_nth ((melt_ptr_t) ( /*_.EL__V2*/ meltfptr[1]), (1)));;
    MELT_LOCATION ("warmelt-debug.melt:1066:/ quasiblock");


 /*_#NEXTDEPTH__L2*/ meltfnum[1] =
      (melt_get_int
       ((melt_ptr_t) (( /*~NEXTDEPTHBOX */ meltfclos->tabval[0]))));;
    MELT_LOCATION ("warmelt-debug.melt:1067:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) (( /*~DBGI */ meltfclos->tabval[1])) /*=obj*/ ;
      melt_object_get_field (slot, obj, 2, "DBGI_MAXDEPTH");
  /*_.DBGI_MAXDEPTH__V5*/ meltfptr[4] = slot;
    };
    ;
 /*_#OLDMAXDEPTH__L3*/ meltfnum[2] =
      (melt_get_int ((melt_ptr_t) ( /*_.DBGI_MAXDEPTH__V5*/ meltfptr[4])));;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-debug.melt:1070:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_OUT__L4*/ meltfnum[3] =
	(melt_is_out ((melt_ptr_t) ( /*~OUT */ meltfclos->tabval[2])));;
      MELT_LOCATION ("warmelt-debug.melt:1070:/ cond");
      /*cond */ if ( /*_#IS_OUT__L4*/ meltfnum[3])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V7*/ meltfptr[6] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-debug.melt:1070:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check out"),
				  ("warmelt-debug.melt")
				  ? ("warmelt-debug.melt") : __FILE__,
				  (1070) ? (1070) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V7*/ meltfptr[6] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V6*/ meltfptr[5] = /*_.IFELSE___V7*/ meltfptr[6];;
      /*epilog */

      MELT_LOCATION ("warmelt-debug.melt:1070:/ clear");
	     /*clear *//*_#IS_OUT__L4*/ meltfnum[3] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V7*/ meltfptr[6] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V6*/ meltfptr[5] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

    {
      MELT_LOCATION ("warmelt-debug.melt:1071:/ locexp");
      meltgc_out_add_indent ((melt_ptr_t) (( /*~OUT */ meltfclos->tabval[2])),
			     ( /*_#NEXTDEPTH__L2*/ meltfnum[1]), 0);;
    }
    ;

    {
      MELT_LOCATION ("warmelt-debug.melt:1072:/ locexp");
      meltgc_add_out ((melt_ptr_t) (( /*~OUT */ meltfclos->tabval[2])),
		      ("**"));
    }
    ;
    MELT_LOCATION ("warmelt-debug.melt:1073:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) (( /*~DBGI */ meltfclos->tabval[1])) /*=obj*/ ;
      melt_object_get_field (slot, obj, 2, "DBGI_MAXDEPTH");
  /*_.DBGI_MAXDEPTH__V8*/ meltfptr[6] = slot;
    };
    ;

    {
      /*^locexp */
      melt_put_int ((melt_ptr_t) ( /*_.DBGI_MAXDEPTH__V8*/ meltfptr[6]), (0));
    }
    ;
    MELT_LOCATION ("warmelt-debug.melt:1074:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[2];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & ( /*~DBGI */ meltfclos->tabval[1]);
      /*^apply.arg */
      argtab[1].meltbp_long = /*_#NEXTDEPTH__L2*/ meltfnum[1];
      /*_.DBG_OUTOBJECT__V9*/ meltfptr[8] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!DBG_OUTOBJECT */ meltfrout->tabval[0])),
		    (melt_ptr_t) ( /*_.ELAT__V3*/ meltfptr[2]),
		    (MELTBPARSTR_PTR MELTBPARSTR_LONG ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-debug.melt:1075:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) (( /*~DBGI */ meltfclos->tabval[1])) /*=obj*/ ;
      melt_object_get_field (slot, obj, 2, "DBGI_MAXDEPTH");
  /*_.DBGI_MAXDEPTH__V10*/ meltfptr[9] = slot;
    };
    ;

    {
      /*^locexp */
      melt_put_int ((melt_ptr_t) ( /*_.DBGI_MAXDEPTH__V10*/ meltfptr[9]),
		    ( /*_#OLDMAXDEPTH__L3*/ meltfnum[2]));
    }
    ;

    {
      MELT_LOCATION ("warmelt-debug.melt:1076:/ locexp");
      meltgc_add_out ((melt_ptr_t) (( /*~OUT */ meltfclos->tabval[2])),
		      (" =="));
    }
    ;
 /*_#I__L5*/ meltfnum[3] =
      (( /*_#NEXTDEPTH__L2*/ meltfnum[1]) + (1));;

    {
      MELT_LOCATION ("warmelt-debug.melt:1077:/ locexp");
      meltgc_out_add_indent ((melt_ptr_t) (( /*~OUT */ meltfclos->tabval[2])),
			     ( /*_#I__L5*/ meltfnum[3]), 64);;
    }
    ;
 /*_#I__L6*/ meltfnum[5] =
      (( /*_#NEXTDEPTH__L2*/ meltfnum[1]) + (2));;
    MELT_LOCATION ("warmelt-debug.melt:1078:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[2];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & ( /*~DBGI */ meltfclos->tabval[1]);
      /*^apply.arg */
      argtab[1].meltbp_long = /*_#I__L6*/ meltfnum[5];
      /*_.DBG_OUT__V11*/ meltfptr[10] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!DBG_OUT */ meltfrout->tabval[1])),
		    (melt_ptr_t) ( /*_.ELVA__V4*/ meltfptr[3]),
		    (MELTBPARSTR_PTR MELTBPARSTR_LONG ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-debug.melt:1079:/ locexp");
      meltgc_add_out ((melt_ptr_t) (( /*~OUT */ meltfclos->tabval[2])),
		      ("; "));
    }
    ;

    MELT_LOCATION ("warmelt-debug.melt:1066:/ clear");
	   /*clear *//*_#NEXTDEPTH__L2*/ meltfnum[1] = 0;
    /*^clear */
	   /*clear *//*_.DBGI_MAXDEPTH__V5*/ meltfptr[4] = 0;
    /*^clear */
	   /*clear *//*_#OLDMAXDEPTH__L3*/ meltfnum[2] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V6*/ meltfptr[5] = 0;
    /*^clear */
	   /*clear *//*_.DBGI_MAXDEPTH__V8*/ meltfptr[6] = 0;
    /*^clear */
	   /*clear *//*_.DBG_OUTOBJECT__V9*/ meltfptr[8] = 0;
    /*^clear */
	   /*clear *//*_.DBGI_MAXDEPTH__V10*/ meltfptr[9] = 0;
    /*^clear */
	   /*clear *//*_#I__L5*/ meltfnum[3] = 0;
    /*^clear */
	   /*clear *//*_#I__L6*/ meltfnum[5] = 0;
    /*^clear */
	   /*clear *//*_.DBG_OUT__V11*/ meltfptr[10] = 0;

    MELT_LOCATION ("warmelt-debug.melt:1064:/ clear");
	   /*clear *//*_.ELAT__V3*/ meltfptr[2] = 0;
    /*^clear */
	   /*clear *//*_.ELVA__V4*/ meltfptr[3] = 0;
    /*epilog */
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("LAMBDA_", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_29_warmelt_debug_LAMBDA___4___melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*noretval */ NULL);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_29_warmelt_debug_LAMBDA___4__ */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_30_warmelt_debug_DBGOUT_MAPSTRING_METHOD (meltclosure_ptr_t
						   meltclosp_,
						   melt_ptr_t meltfirstargp_,
						   const melt_argdescr_cell_t
						   meltxargdescr_[],
						   union meltparam_un *
						   meltxargtab_,
						   const melt_argdescr_cell_t
						   meltxresdescr_[],
						   union meltparam_un *
						   meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_30_warmelt_debug_DBGOUT_MAPSTRING_METHOD_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_30_warmelt_debug_DBGOUT_MAPSTRING_METHOD_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 29
    melt_ptr_t mcfr_varptr[29];
#define MELTFRAM_NBVARNUM 20
    long mcfr_varnum[20];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_30_warmelt_debug_DBGOUT_MAPSTRING_METHOD is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_30_warmelt_debug_DBGOUT_MAPSTRING_METHOD_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 29; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_30_warmelt_debug_DBGOUT_MAPSTRING_METHOD nbval 29*/
  meltfram__.mcfr_nbvar = 29 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("DBGOUT_MAPSTRING_METHOD", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-debug.melt:1086:/ getarg");
 /*_.SELF__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.DBGI__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.DBGI__V3*/ meltfptr[2])) !=
	      NULL);


  /*getarg#2 */
  /*^getarg */
  if (meltxargdescr_[1] != MELTBPAR_LONG)
    goto lab_endgetargs;
 /*_#DEPTH__L1*/ meltfnum[0] = meltxargtab_[1].meltbp_long;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-debug.melt:1087:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L2*/ meltfnum[1] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.DBGI__V3*/ meltfptr[2]),
			     (melt_ptr_t) (( /*!CLASS_DEBUG_INFORMATION */
					    meltfrout->tabval[0])));;
      MELT_LOCATION ("warmelt-debug.melt:1087:/ cond");
      /*cond */ if ( /*_#IS_A__L2*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V5*/ meltfptr[4] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-debug.melt:1087:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check dbgi"),
				  ("warmelt-debug.melt")
				  ? ("warmelt-debug.melt") : __FILE__,
				  (1087) ? (1087) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V5*/ meltfptr[4] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V4*/ meltfptr[3] = /*_.IFELSE___V5*/ meltfptr[4];;
      /*epilog */

      MELT_LOCATION ("warmelt-debug.melt:1087:/ clear");
	     /*clear *//*_#IS_A__L2*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V5*/ meltfptr[4] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V4*/ meltfptr[3] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-debug.melt:1088:/ quasiblock");


 /*_.DIS__V6*/ meltfptr[4] =
      ((melt_ptr_t)
       (melt_discr ((melt_ptr_t) ( /*_.SELF__V2*/ meltfptr[1]))));;
    MELT_LOCATION ("warmelt-debug.melt:1089:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.DBGI__V3*/ meltfptr[2]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 0, "DBGI_OUT");
  /*_.SBUF__V7*/ meltfptr[6] = slot;
    };
    ;
 /*_#IX__L3*/ meltfnum[1] = 0;;
    /*^compute */
 /*_#MAPCOUNT__L4*/ meltfnum[3] =
      (melt_count_mapstrings
       ((struct meltmapstrings_st *) ( /*_.SELF__V2*/ meltfptr[1])));;
    /*^compute */
 /*_#I__L5*/ meltfnum[4] =
      ((2) + ( /*_#DEPTH__L1*/ meltfnum[0]));;
    /*^compute */
 /*_.NEXTDEPTHBOX__V8*/ meltfptr[7] =
      (meltgc_new_int
       ((meltobject_ptr_t) (( /*!DISCR_INTEGER */ meltfrout->tabval[1])),
	( /*_#I__L5*/ meltfnum[4])));;
    /*^compute */
 /*_.COUNTBOX__V9*/ meltfptr[8] =
      (meltgc_new_int
       ((meltobject_ptr_t) (( /*!DISCR_INTEGER */ meltfrout->tabval[1])),
	(0)));;
    /*^compute */
 /*_.TUPL__V10*/ meltfptr[9] =
      (meltgc_new_multiple
       ((meltobject_ptr_t) (( /*!DISCR_MULTIPLE */ meltfrout->tabval[2])),
	( /*_#MAPCOUNT__L4*/ meltfnum[3])));;
    MELT_LOCATION ("warmelt-debug.melt:1095:/ cond");
    /*cond */ if (
		   /*ifisa */
		   melt_is_instance_of ((melt_ptr_t)
					( /*_.DBGI__V3*/ meltfptr[2]),
					(melt_ptr_t) (( /*!CLASS_DEBUG_INFORMATION */ meltfrout->tabval[0])))
      )				/*then */
      {
	/*^cond.then */
	/*^getslot */
	{
	  melt_ptr_t slot = NULL, obj = NULL;
	  obj = (melt_ptr_t) ( /*_.DBGI__V3*/ meltfptr[2]) /*=obj*/ ;
	  melt_object_get_field (slot, obj, 2, "DBGI_MAXDEPTH");
   /*_.DBGI_MAXDEPTH__V11*/ meltfptr[10] = slot;
	};
	;
      }
    else
      {				/*^cond.else */

  /*_.DBGI_MAXDEPTH__V11*/ meltfptr[10] = NULL;;
      }
    ;
    /*^compute */
 /*_#MAXDEPTH__L6*/ meltfnum[5] =
      (melt_get_int ((melt_ptr_t) ( /*_.DBGI_MAXDEPTH__V11*/ meltfptr[10])));;
    MELT_LOCATION ("warmelt-debug.melt:1097:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#__L7*/ meltfnum[6] =
      (( /*_.DIS__V6*/ meltfptr[4]) ==
       (( /*!DISCR_MAP_STRINGS */ meltfrout->tabval[3])));;
    MELT_LOCATION ("warmelt-debug.melt:1097:/ cond");
    /*cond */ if ( /*_#__L7*/ meltfnum[6])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{


	  {
	    MELT_LOCATION ("warmelt-debug.melt:1098:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V7*/ meltfptr[6]),
				 (" <("));
	  }
	  ;
	  /*epilog */
	}
	;
      }
    else
      {
	MELT_LOCATION ("warmelt-debug.melt:1097:/ cond.else");

	/*^block */
	/*anyblock */
	{


	  {
	    MELT_LOCATION ("warmelt-debug.melt:1100:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V7*/ meltfptr[6]),
				 (" |"));
	  }
	  ;
	  MELT_LOCATION ("warmelt-debug.melt:1101:/ getslot");
	  {
	    melt_ptr_t slot = NULL, obj = NULL;
	    obj = (melt_ptr_t) ( /*_.DIS__V6*/ meltfptr[4]) /*=obj*/ ;
	    melt_object_get_field (slot, obj, 1, "NAMED_NAME");
    /*_.NAMED_NAME__V12*/ meltfptr[11] = slot;
	  };
	  ;

	  {
	    /*^locexp */
	    /*add2sbuf_string */
	      meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V7*/ meltfptr[6]),
				 melt_string_str ((melt_ptr_t)
						  ( /*_.NAMED_NAME__V12*/
						   meltfptr[11])));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-debug.melt:1102:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V7*/ meltfptr[6]),
				 ("<("));
	  }
	  ;
	  MELT_LOCATION ("warmelt-debug.melt:1099:/ quasiblock");


	  /*epilog */

	  MELT_LOCATION ("warmelt-debug.melt:1097:/ clear");
	     /*clear *//*_.NAMED_NAME__V12*/ meltfptr[11] = 0;
	}
	;
      }
    ;

    {
      MELT_LOCATION ("warmelt-debug.melt:1103:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V7*/ meltfptr[6]), ("/"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-debug.melt:1104:/ locexp");
      meltgc_add_strbuf_dec ((melt_ptr_t) ( /*_.SBUF__V7*/ meltfptr[6]),
			     ( /*_#MAPCOUNT__L4*/ meltfnum[3]));
    }
    ;
    MELT_LOCATION ("warmelt-debug.melt:1105:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#I__L8*/ meltfnum[7] =
      (( /*_#DEPTH__L1*/ meltfnum[0]) + (2));;
    /*^compute */
 /*_#MELT_REALLY_NEED_DBGLIM__L9*/ meltfnum[8] =
      ( /*MELT_REALLY_NEED_DBGLIM */
       melt_need_debug_limit ((int) /*_#I__L8*/ meltfnum[7],
			      (int) /*_#MAXDEPTH__L6*/ meltfnum[5]));;
    MELT_LOCATION ("warmelt-debug.melt:1105:/ cond");
    /*cond */ if ( /*_#MELT_REALLY_NEED_DBGLIM__L9*/ meltfnum[8])	/*then */
      {
	/*^cond.then */
	/*_#OR___L10*/ meltfnum[9] =
	  /*_#MELT_REALLY_NEED_DBGLIM__L9*/ meltfnum[8];;
      }
    else
      {
	MELT_LOCATION ("warmelt-debug.melt:1105:/ cond.else");

	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-debug.melt:1107:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
   /*_#I__L11*/ meltfnum[10] =
	    (( /*_#DEPTH__L1*/ meltfnum[0]) + (2));;
	  /*^compute */
   /*_#I__L12*/ meltfnum[11] =
	    (( /*_#I__L11*/ meltfnum[10]) <
	     ( /*_#MAXDEPTH__L6*/ meltfnum[5]));;
	  MELT_LOCATION ("warmelt-debug.melt:1107:/ cond");
	  /*cond */ if ( /*_#I__L12*/ meltfnum[11])	/*then */
	    {
	      /*^cond.then */
	      /*^block */
	      /*anyblock */
	      {

     /*_#IS_A__L14*/ meltfnum[13] =
		  melt_is_instance_of ((melt_ptr_t)
				       ( /*_.DBGI__V3*/ meltfptr[2]),
				       (melt_ptr_t) (( /*!CLASS_DEBUG_OUTPUT_INFORMATION */ meltfrout->tabval[4])));;
		/*^compute */
		/*_#IF___L13*/ meltfnum[12] = /*_#IS_A__L14*/ meltfnum[13];;
		/*epilog */

		MELT_LOCATION ("warmelt-debug.melt:1107:/ clear");
	       /*clear *//*_#IS_A__L14*/ meltfnum[13] = 0;
	      }
	      ;
	    }
	  else
	    {			/*^cond.else */

    /*_#IF___L13*/ meltfnum[12] = 0;;
	    }
	  ;
	  /*^compute */
	  /*_#OR___L10*/ meltfnum[9] = /*_#IF___L13*/ meltfnum[12];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-debug.melt:1105:/ clear");
	     /*clear *//*_#I__L11*/ meltfnum[10] = 0;
	  /*^clear */
	     /*clear *//*_#I__L12*/ meltfnum[11] = 0;
	  /*^clear */
	     /*clear *//*_#IF___L13*/ meltfnum[12] = 0;
	}
	;
      }
    ;
    /*^cond */
    /*cond */ if ( /*_#OR___L10*/ meltfnum[9])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-debug.melt:1108:/ quasiblock");


   /*_.AUX__V15*/ meltfptr[14] =
	    melt_auxdata_mapstrings ((melt_ptr_t) /*_.SELF__V2*/
				     meltfptr[1]);;
	  MELT_LOCATION ("warmelt-debug.melt:1111:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^checksignal */
	  MELT_CHECK_SIGNAL ();
	  ;
   /*_#NOTNULL__L15*/ meltfnum[13] =
	    (( /*_.AUX__V15*/ meltfptr[14]) != NULL);;
	  MELT_LOCATION ("warmelt-debug.melt:1111:/ cond");
	  /*cond */ if ( /*_#NOTNULL__L15*/ meltfnum[13])	/*then */
	    {
	      /*^cond.then */
	      /*^block */
	      /*anyblock */
	      {

     /*_#I__L17*/ meltfnum[11] =
		  (( /*_#DEPTH__L1*/ meltfnum[0]) <= (1));;
		/*^compute */
		/*_#IF___L16*/ meltfnum[10] = /*_#I__L17*/ meltfnum[11];;
		/*epilog */

		MELT_LOCATION ("warmelt-debug.melt:1111:/ clear");
	       /*clear *//*_#I__L17*/ meltfnum[11] = 0;
	      }
	      ;
	    }
	  else
	    {			/*^cond.else */

    /*_#IF___L16*/ meltfnum[10] = 0;;
	    }
	  ;
	  MELT_LOCATION ("warmelt-debug.melt:1111:/ cond");
	  /*cond */ if ( /*_#IF___L16*/ meltfnum[10])	/*then */
	    {
	      /*^cond.then */
	      /*^block */
	      /*anyblock */
	      {

		MELT_LOCATION ("warmelt-debug.melt:1115:/ checksignal");
		MELT_CHECK_SIGNAL ();
		;
		/*^apply */
		/*apply */
		{
		  union meltparam_un argtab[1];
		  memset (&argtab, 0, sizeof (argtab));
		  /*^apply.arg */
		  argtab[0].meltbp_cstring = " aux:";
		  /*_.ADD2OUT__V16*/ meltfptr[15] =
		    melt_apply ((meltclosure_ptr_t)
				(( /*!ADD2OUT */ meltfrout->tabval[5])),
				(melt_ptr_t) ( /*_.SBUF__V7*/ meltfptr[6]),
				(MELTBPARSTR_CSTRING ""), argtab, "",
				(union meltparam_un *) 0);
		}
		;
     /*_#I__L18*/ meltfnum[12] =
		  (( /*_#DEPTH__L1*/ meltfnum[0]) + (3));;
		MELT_LOCATION ("warmelt-debug.melt:1116:/ checksignal");
		MELT_CHECK_SIGNAL ();
		;
		/*^apply */
		/*apply */
		{
		  union meltparam_un argtab[2];
		  memset (&argtab, 0, sizeof (argtab));
		  /*^apply.arg */
		  argtab[0].meltbp_aptr =
		    (melt_ptr_t *) & /*_.DBGI__V3*/ meltfptr[2];
		  /*^apply.arg */
		  argtab[1].meltbp_long = /*_#I__L18*/ meltfnum[12];
		  /*_.DBG_OUT__V17*/ meltfptr[16] =
		    melt_apply ((meltclosure_ptr_t)
				(( /*!DBG_OUT */ meltfrout->tabval[6])),
				(melt_ptr_t) ( /*_.AUX__V15*/ meltfptr[14]),
				(MELTBPARSTR_PTR MELTBPARSTR_LONG ""), argtab,
				"", (union meltparam_un *) 0);
		}
		;
     /*_#I__L19*/ meltfnum[11] =
		  (( /*_#DEPTH__L1*/ meltfnum[0]) + (1));;

		{
		  MELT_LOCATION ("warmelt-debug.melt:1117:/ locexp");
		  meltgc_out_add_indent ((melt_ptr_t)
					 ( /*_.SBUF__V7*/ meltfptr[6]),
					 ( /*_#I__L19*/ meltfnum[11]), 0);;
		}
		;
		MELT_LOCATION ("warmelt-debug.melt:1114:/ quasiblock");


		/*epilog */

		MELT_LOCATION ("warmelt-debug.melt:1111:/ clear");
	       /*clear *//*_.ADD2OUT__V16*/ meltfptr[15] = 0;
		/*^clear */
	       /*clear *//*_#I__L18*/ meltfnum[12] = 0;
		/*^clear */
	       /*clear *//*_.DBG_OUT__V17*/ meltfptr[16] = 0;
		/*^clear */
	       /*clear *//*_#I__L19*/ meltfnum[11] = 0;
	      }
	      ;
	    }			/*noelse */
	  ;
	  MELT_LOCATION ("warmelt-debug.melt:1122:/ quasiblock");


	  /*^newclosure */
		   /*newclosure *//*_.LAMBDA___V19*/ meltfptr[16] =
	    (melt_ptr_t)
	    meltgc_new_closure ((meltobject_ptr_t)
				(((melt_ptr_t)
				  (MELT_PREDEF (DISCR_CLOSURE)))),
				(meltroutine_ptr_t) (( /*!konst_9 */
						      meltfrout->tabval[9])),
				(2));
	  ;
	  /*^putclosedv */
	  /*putclosv */
	  melt_assertmsg ("putclosv checkclo",
			  melt_magic_discr ((melt_ptr_t)
					    ( /*_.LAMBDA___V19*/
					     meltfptr[16])) ==
			  MELTOBMAG_CLOSURE);
	  melt_assertmsg ("putclosv checkoff", 0 >= 0
			  && 0 <
			  melt_closure_size ((melt_ptr_t)
					     ( /*_.LAMBDA___V19*/
					      meltfptr[16])));
	  ((meltclosure_ptr_t) /*_.LAMBDA___V19*/ meltfptr[16])->tabval[0] =
	    (melt_ptr_t) ( /*_.COUNTBOX__V9*/ meltfptr[8]);
	  ;
	  /*^putclosedv */
	  /*putclosv */
	  melt_assertmsg ("putclosv checkclo",
			  melt_magic_discr ((melt_ptr_t)
					    ( /*_.LAMBDA___V19*/
					     meltfptr[16])) ==
			  MELTOBMAG_CLOSURE);
	  melt_assertmsg ("putclosv checkoff", 1 >= 0
			  && 1 <
			  melt_closure_size ((melt_ptr_t)
					     ( /*_.LAMBDA___V19*/
					      meltfptr[16])));
	  ((meltclosure_ptr_t) /*_.LAMBDA___V19*/ meltfptr[16])->tabval[1] =
	    (melt_ptr_t) ( /*_.TUPL__V10*/ meltfptr[9]);
	  ;
	  /*_.LAMBDA___V18*/ meltfptr[15] = /*_.LAMBDA___V19*/ meltfptr[16];;
	  MELT_LOCATION ("warmelt-debug.melt:1120:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^apply */
	  /*apply */
	  {
	    union meltparam_un argtab[1];
	    memset (&argtab, 0, sizeof (argtab));
	    /*^apply.arg */
	    argtab[0].meltbp_aptr =
	      (melt_ptr_t *) & /*_.LAMBDA___V18*/ meltfptr[15];
	    /*_.MAPSTRING_EVERY__V20*/ meltfptr[19] =
	      melt_apply ((meltclosure_ptr_t)
			  (( /*!MAPSTRING_EVERY */ meltfrout->tabval[7])),
			  (melt_ptr_t) ( /*_.SELF__V2*/ meltfptr[1]),
			  (MELTBPARSTR_PTR ""), argtab, "",
			  (union meltparam_un *) 0);
	  }
	  ;
	  MELT_LOCATION ("warmelt-debug.melt:1131:/ quasiblock");


	  MELT_LOCATION ("warmelt-debug.melt:1134:/ quasiblock");


	  /*^newclosure */
		   /*newclosure *//*_.LAMBDA___V23*/ meltfptr[22] =
	    (melt_ptr_t)
	    meltgc_new_closure ((meltobject_ptr_t)
				(((melt_ptr_t)
				  (MELT_PREDEF (DISCR_CLOSURE)))),
				(meltroutine_ptr_t) (( /*!konst_11 */
						      meltfrout->tabval[11])),
				(0));
	  ;
	  /*_.LAMBDA___V22*/ meltfptr[21] = /*_.LAMBDA___V23*/ meltfptr[22];;
	  /*^compute */
   /*_.SORTUPL__V24*/ meltfptr[23] =
	    meltgc_sort_multiple ((melt_ptr_t) ( /*_.TUPL__V10*/ meltfptr[9]),
				  (melt_ptr_t) ( /*_.LAMBDA___V22*/
						meltfptr[21]),
				  (melt_ptr_t) (( /*!DISCR_MULTIPLE */
						 meltfrout->tabval[2])));;

#if MELT_HAVE_DEBUG
	  MELT_LOCATION ("warmelt-debug.melt:1147:/ cppif.then");
	  /*^block */
	  /*anyblock */
	  {

	    /*^checksignal */
	    MELT_CHECK_SIGNAL ();
	    ;
     /*_#IS_MULTIPLE__L20*/ meltfnum[12] =
	      (melt_magic_discr
	       ((melt_ptr_t) ( /*_.SORTUPL__V24*/ meltfptr[23])) ==
	       MELTOBMAG_MULTIPLE);;
	    MELT_LOCATION ("warmelt-debug.melt:1147:/ cond");
	    /*cond */ if ( /*_#IS_MULTIPLE__L20*/ meltfnum[12])	/*then */
	      {
		/*^cond.then */
		/*_.IFELSE___V26*/ meltfptr[25] = ( /*nil */ NULL);;
	      }
	    else
	      {
		MELT_LOCATION ("warmelt-debug.melt:1147:/ cond.else");

		/*^block */
		/*anyblock */
		{




		  {
		    /*^locexp */
		    melt_assert_failed (("check sortupl"),
					("warmelt-debug.melt")
					? ("warmelt-debug.melt") : __FILE__,
					(1147) ? (1147) : __LINE__,
					__FUNCTION__);
		    ;
		  }
		  ;
		 /*clear *//*_.IFELSE___V26*/ meltfptr[25] = 0;
		  /*epilog */
		}
		;
	      }
	    ;
	    /*^compute */
	    /*_.IFCPP___V25*/ meltfptr[24] = /*_.IFELSE___V26*/ meltfptr[25];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-debug.melt:1147:/ clear");
	       /*clear *//*_#IS_MULTIPLE__L20*/ meltfnum[12] = 0;
	    /*^clear */
	       /*clear *//*_.IFELSE___V26*/ meltfptr[25] = 0;
	  }

#else /*MELT_HAVE_DEBUG */
	  /*^cppif.else */
	  /*_.IFCPP___V25*/ meltfptr[24] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
	  ;
	  MELT_LOCATION ("warmelt-debug.melt:1150:/ quasiblock");


	  /*^newclosure */
		   /*newclosure *//*_.LAMBDA___V28*/ meltfptr[27] =
	    (melt_ptr_t)
	    meltgc_new_closure ((meltobject_ptr_t)
				(((melt_ptr_t)
				  (MELT_PREDEF (DISCR_CLOSURE)))),
				(meltroutine_ptr_t) (( /*!konst_14 */
						      meltfrout->tabval[14])),
				(3));
	  ;
	  /*^putclosedv */
	  /*putclosv */
	  melt_assertmsg ("putclosv checkclo",
			  melt_magic_discr ((melt_ptr_t)
					    ( /*_.LAMBDA___V28*/
					     meltfptr[27])) ==
			  MELTOBMAG_CLOSURE);
	  melt_assertmsg ("putclosv checkoff", 0 >= 0
			  && 0 <
			  melt_closure_size ((melt_ptr_t)
					     ( /*_.LAMBDA___V28*/
					      meltfptr[27])));
	  ((meltclosure_ptr_t) /*_.LAMBDA___V28*/ meltfptr[27])->tabval[0] =
	    (melt_ptr_t) ( /*_.NEXTDEPTHBOX__V8*/ meltfptr[7]);
	  ;
	  /*^putclosedv */
	  /*putclosv */
	  melt_assertmsg ("putclosv checkclo",
			  melt_magic_discr ((melt_ptr_t)
					    ( /*_.LAMBDA___V28*/
					     meltfptr[27])) ==
			  MELTOBMAG_CLOSURE);
	  melt_assertmsg ("putclosv checkoff", 1 >= 0
			  && 1 <
			  melt_closure_size ((melt_ptr_t)
					     ( /*_.LAMBDA___V28*/
					      meltfptr[27])));
	  ((meltclosure_ptr_t) /*_.LAMBDA___V28*/ meltfptr[27])->tabval[1] =
	    (melt_ptr_t) ( /*_.SBUF__V7*/ meltfptr[6]);
	  ;
	  /*^putclosedv */
	  /*putclosv */
	  melt_assertmsg ("putclosv checkclo",
			  melt_magic_discr ((melt_ptr_t)
					    ( /*_.LAMBDA___V28*/
					     meltfptr[27])) ==
			  MELTOBMAG_CLOSURE);
	  melt_assertmsg ("putclosv checkoff", 2 >= 0
			  && 2 <
			  melt_closure_size ((melt_ptr_t)
					     ( /*_.LAMBDA___V28*/
					      meltfptr[27])));
	  ((meltclosure_ptr_t) /*_.LAMBDA___V28*/ meltfptr[27])->tabval[2] =
	    (melt_ptr_t) ( /*_.DBGI__V3*/ meltfptr[2]);
	  ;
	  /*_.LAMBDA___V27*/ meltfptr[25] = /*_.LAMBDA___V28*/ meltfptr[27];;
	  MELT_LOCATION ("warmelt-debug.melt:1148:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^apply */
	  /*apply */
	  {
	    union meltparam_un argtab[1];
	    memset (&argtab, 0, sizeof (argtab));
	    /*^apply.arg */
	    argtab[0].meltbp_aptr =
	      (melt_ptr_t *) & /*_.LAMBDA___V27*/ meltfptr[25];
	    /*_.MULTIPLE_EVERY__V29*/ meltfptr[28] =
	      melt_apply ((meltclosure_ptr_t)
			  (( /*!MULTIPLE_EVERY */ meltfrout->tabval[12])),
			  (melt_ptr_t) ( /*_.SORTUPL__V24*/ meltfptr[23]),
			  (MELTBPARSTR_PTR ""), argtab, "",
			  (union meltparam_un *) 0);
	  }
	  ;
	  /*_.LET___V21*/ meltfptr[20] =
	    /*_.MULTIPLE_EVERY__V29*/ meltfptr[28];;

	  MELT_LOCATION ("warmelt-debug.melt:1131:/ clear");
	     /*clear *//*_.LAMBDA___V22*/ meltfptr[21] = 0;
	  /*^clear */
	     /*clear *//*_.SORTUPL__V24*/ meltfptr[23] = 0;
	  /*^clear */
	     /*clear *//*_.IFCPP___V25*/ meltfptr[24] = 0;
	  /*^clear */
	     /*clear *//*_.LAMBDA___V27*/ meltfptr[25] = 0;
	  /*^clear */
	     /*clear *//*_.MULTIPLE_EVERY__V29*/ meltfptr[28] = 0;
	  /*_.LET___V14*/ meltfptr[13] = /*_.LET___V21*/ meltfptr[20];;

	  MELT_LOCATION ("warmelt-debug.melt:1108:/ clear");
	     /*clear *//*_.AUX__V15*/ meltfptr[14] = 0;
	  /*^clear */
	     /*clear *//*_#NOTNULL__L15*/ meltfnum[13] = 0;
	  /*^clear */
	     /*clear *//*_#IF___L16*/ meltfnum[10] = 0;
	  /*^clear */
	     /*clear *//*_.LAMBDA___V18*/ meltfptr[15] = 0;
	  /*^clear */
	     /*clear *//*_.MAPSTRING_EVERY__V20*/ meltfptr[19] = 0;
	  /*^clear */
	     /*clear *//*_.LET___V21*/ meltfptr[20] = 0;
	  /*_.IF___V13*/ meltfptr[11] = /*_.LET___V14*/ meltfptr[13];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-debug.melt:1105:/ clear");
	     /*clear *//*_.LET___V14*/ meltfptr[13] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

  /*_.IF___V13*/ meltfptr[11] = NULL;;
      }
    ;

    {
      MELT_LOCATION ("warmelt-debug.melt:1164:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V7*/ meltfptr[6]),
			   (" )>"));
    }
    ;

    MELT_LOCATION ("warmelt-debug.melt:1088:/ clear");
	   /*clear *//*_.DIS__V6*/ meltfptr[4] = 0;
    /*^clear */
	   /*clear *//*_.SBUF__V7*/ meltfptr[6] = 0;
    /*^clear */
	   /*clear *//*_#IX__L3*/ meltfnum[1] = 0;
    /*^clear */
	   /*clear *//*_#MAPCOUNT__L4*/ meltfnum[3] = 0;
    /*^clear */
	   /*clear *//*_#I__L5*/ meltfnum[4] = 0;
    /*^clear */
	   /*clear *//*_.NEXTDEPTHBOX__V8*/ meltfptr[7] = 0;
    /*^clear */
	   /*clear *//*_.COUNTBOX__V9*/ meltfptr[8] = 0;
    /*^clear */
	   /*clear *//*_.TUPL__V10*/ meltfptr[9] = 0;
    /*^clear */
	   /*clear *//*_.DBGI_MAXDEPTH__V11*/ meltfptr[10] = 0;
    /*^clear */
	   /*clear *//*_#MAXDEPTH__L6*/ meltfnum[5] = 0;
    /*^clear */
	   /*clear *//*_#__L7*/ meltfnum[6] = 0;
    /*^clear */
	   /*clear *//*_#I__L8*/ meltfnum[7] = 0;
    /*^clear */
	   /*clear *//*_#MELT_REALLY_NEED_DBGLIM__L9*/ meltfnum[8] = 0;
    /*^clear */
	   /*clear *//*_#OR___L10*/ meltfnum[9] = 0;
    /*^clear */
	   /*clear *//*_.IF___V13*/ meltfptr[11] = 0;
    MELT_LOCATION ("warmelt-debug.melt:1086:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V4*/ meltfptr[3] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("DBGOUT_MAPSTRING_METHOD", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_30_warmelt_debug_DBGOUT_MAPSTRING_METHOD_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*noretval */ NULL);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_30_warmelt_debug_DBGOUT_MAPSTRING_METHOD */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_31_warmelt_debug_LAMBDA___5__ (meltclosure_ptr_t meltclosp_,
					melt_ptr_t meltfirstargp_,
					const melt_argdescr_cell_t
					meltxargdescr_[],
					union meltparam_un * meltxargtab_,
					const melt_argdescr_cell_t
					meltxresdescr_[],
					union meltparam_un * meltxrestab_)
{
  long current_blocklevel_signals_meltrout_31_warmelt_debug_LAMBDA___5___melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_31_warmelt_debug_LAMBDA___5___st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 6
    melt_ptr_t mcfr_varptr[6];
#define MELTFRAM_NBVARNUM 2
    long mcfr_varnum[2];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_31_warmelt_debug_LAMBDA___5__ is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_31_warmelt_debug_LAMBDA___5___st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 6; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_31_warmelt_debug_LAMBDA___5__ nbval 6*/
  meltfram__.mcfr_nbvar = 6 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("LAMBDA_", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-debug.melt:1122:/ getarg");
 /*_.STR__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.VAL__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.VAL__V3*/ meltfptr[2])) != NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {

    MELT_LOCATION ("warmelt-debug.melt:1123:/ quasiblock");


 /*_#CURCOUNT__L1*/ meltfnum[0] =
      (melt_get_int ((melt_ptr_t) (( /*~COUNTBOX */ meltfclos->tabval[0]))));;
    /*^compute */
 /*_.MAKE_INTEGERBOX__V4*/ meltfptr[3] =
      (meltgc_new_int
       ((meltobject_ptr_t) (( /*!DISCR_INTEGER */ meltfrout->tabval[0])),
	( /*_#CURCOUNT__L1*/ meltfnum[0])));;
    MELT_LOCATION ("warmelt-debug.melt:1124:/ blockmultialloc");
    /*multiallocblock */
    {
      struct meltletrec_1_st
      {
	struct MELT_MULTIPLE_STRUCT (3) rtup_0__TUPLREC__x2;
	long meltletrec_1_endgap;
      } *meltletrec_1_ptr = 0;
      meltletrec_1_ptr =
	(struct meltletrec_1_st *)
	meltgc_allocate (sizeof (struct meltletrec_1_st), 0);
      /*^blockmultialloc.initfill */
      /*inimult rtup_0__TUPLREC__x2 */
 /*_.TUPLREC___V6*/ meltfptr[5] =
	(melt_ptr_t) & meltletrec_1_ptr->rtup_0__TUPLREC__x2;
      meltletrec_1_ptr->rtup_0__TUPLREC__x2.discr =
	(meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (DISCR_MULTIPLE))));
      meltletrec_1_ptr->rtup_0__TUPLREC__x2.nbval = 3;


      /*^putuple */
      /*putupl#4 */
      melt_assertmsg ("putupl [:1124] #4 checktup",
		      melt_magic_discr ((melt_ptr_t)
					( /*_.TUPLREC___V6*/ meltfptr[5])) ==
		      MELTOBMAG_MULTIPLE);
      melt_assertmsg ("putupl [:1124] #4 checkoff",
		      (0 >= 0
		       && 0 <
		       melt_multiple_length ((melt_ptr_t)
					     ( /*_.TUPLREC___V6*/
					      meltfptr[5]))));
      ((meltmultiple_ptr_t) ( /*_.TUPLREC___V6*/ meltfptr[5]))->tabval[0] =
	(melt_ptr_t) ( /*_.STR__V2*/ meltfptr[1]);
      ;
      /*^putuple */
      /*putupl#5 */
      melt_assertmsg ("putupl [:1124] #5 checktup",
		      melt_magic_discr ((melt_ptr_t)
					( /*_.TUPLREC___V6*/ meltfptr[5])) ==
		      MELTOBMAG_MULTIPLE);
      melt_assertmsg ("putupl [:1124] #5 checkoff",
		      (1 >= 0
		       && 1 <
		       melt_multiple_length ((melt_ptr_t)
					     ( /*_.TUPLREC___V6*/
					      meltfptr[5]))));
      ((meltmultiple_ptr_t) ( /*_.TUPLREC___V6*/ meltfptr[5]))->tabval[1] =
	(melt_ptr_t) ( /*_.VAL__V3*/ meltfptr[2]);
      ;
      /*^putuple */
      /*putupl#6 */
      melt_assertmsg ("putupl [:1124] #6 checktup",
		      melt_magic_discr ((melt_ptr_t)
					( /*_.TUPLREC___V6*/ meltfptr[5])) ==
		      MELTOBMAG_MULTIPLE);
      melt_assertmsg ("putupl [:1124] #6 checkoff",
		      (2 >= 0
		       && 2 <
		       melt_multiple_length ((melt_ptr_t)
					     ( /*_.TUPLREC___V6*/
					      meltfptr[5]))));
      ((meltmultiple_ptr_t) ( /*_.TUPLREC___V6*/ meltfptr[5]))->tabval[2] =
	(melt_ptr_t) ( /*_.MAKE_INTEGERBOX__V4*/ meltfptr[3]);
      ;
      /*^touch */
      meltgc_touch ( /*_.TUPLREC___V6*/ meltfptr[5]);
      ;
      /*_.ENT__V5*/ meltfptr[4] = /*_.TUPLREC___V6*/ meltfptr[5];;
      /*epilog */

      MELT_LOCATION ("warmelt-debug.melt:1124:/ clear");
	    /*clear *//*_.TUPLREC___V6*/ meltfptr[5] = 0;
      /*^clear */
	    /*clear *//*_.TUPLREC___V6*/ meltfptr[5] = 0;
    }				/*end multiallocblock */
    ;

    {
      MELT_LOCATION ("warmelt-debug.melt:1127:/ locexp");
      meltgc_multiple_put_nth ((melt_ptr_t)
			       (( /*~TUPL */ meltfclos->tabval[1])),
			       ( /*_#CURCOUNT__L1*/ meltfnum[0]),
			       (melt_ptr_t) ( /*_.ENT__V5*/ meltfptr[4]));
    }
    ;
 /*_#I__L2*/ meltfnum[1] =
      (( /*_#CURCOUNT__L1*/ meltfnum[0]) + (1));;

    {
      MELT_LOCATION ("warmelt-debug.melt:1128:/ locexp");
      melt_put_int ((melt_ptr_t) (( /*~COUNTBOX */ meltfclos->tabval[0])),
		    ( /*_#I__L2*/ meltfnum[1]));
    }
    ;

    MELT_LOCATION ("warmelt-debug.melt:1123:/ clear");
	   /*clear *//*_#CURCOUNT__L1*/ meltfnum[0] = 0;
    /*^clear */
	   /*clear *//*_.MAKE_INTEGERBOX__V4*/ meltfptr[3] = 0;
    /*^clear */
	   /*clear *//*_.ENT__V5*/ meltfptr[4] = 0;
    /*^clear */
	   /*clear *//*_#I__L2*/ meltfnum[1] = 0;
    /*epilog */
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("LAMBDA_", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_31_warmelt_debug_LAMBDA___5___melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*noretval */ NULL);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_31_warmelt_debug_LAMBDA___5__ */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_32_warmelt_debug_LAMBDA___6__ (meltclosure_ptr_t meltclosp_,
					melt_ptr_t meltfirstargp_,
					const melt_argdescr_cell_t
					meltxargdescr_[],
					union meltparam_un * meltxargtab_,
					const melt_argdescr_cell_t
					meltxresdescr_[],
					union meltparam_un * meltxrestab_)
{
  long current_blocklevel_signals_meltrout_32_warmelt_debug_LAMBDA___6___melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_32_warmelt_debug_LAMBDA___6___st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 11
    melt_ptr_t mcfr_varptr[11];
/*no varnum*/
#define MELTFRAM_NBVARNUM /*none*/0
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_32_warmelt_debug_LAMBDA___6__ is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_32_warmelt_debug_LAMBDA___6___st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 11; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_32_warmelt_debug_LAMBDA___6__ nbval 11*/
  meltfram__.mcfr_nbvar = 11 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("LAMBDA_", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-debug.melt:1134:/ getarg");
 /*_.E1__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.E2__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.E2__V3*/ meltfptr[2])) != NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {

    MELT_LOCATION ("warmelt-debug.melt:1135:/ quasiblock");


 /*_.E1AT__V5*/ meltfptr[4] =
      (melt_multiple_nth ((melt_ptr_t) ( /*_.E1__V2*/ meltfptr[1]), (0)));;
    /*^compute */
 /*_.E1VA__V6*/ meltfptr[5] =
      (melt_multiple_nth ((melt_ptr_t) ( /*_.E1__V2*/ meltfptr[1]), (1)));;
    /*^compute */
 /*_.E1RK__V7*/ meltfptr[6] =
      (melt_multiple_nth ((melt_ptr_t) ( /*_.E1__V2*/ meltfptr[1]), (2)));;
    /*^compute */
 /*_.E2AT__V8*/ meltfptr[7] =
      (melt_multiple_nth ((melt_ptr_t) ( /*_.E2__V3*/ meltfptr[2]), (0)));;
    /*^compute */
 /*_.E2VA__V9*/ meltfptr[8] =
      (melt_multiple_nth ((melt_ptr_t) ( /*_.E2__V3*/ meltfptr[2]), (1)));;
    /*^compute */
 /*_.E2RK__V10*/ meltfptr[9] =
      (melt_multiple_nth ((melt_ptr_t) ( /*_.E2__V3*/ meltfptr[2]), (2)));;
    MELT_LOCATION ("warmelt-debug.melt:1142:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[6];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.E1RK__V7*/ meltfptr[6];
      /*^apply.arg */
      argtab[1].meltbp_aptr = (melt_ptr_t *) & /*_.E2AT__V8*/ meltfptr[7];
      /*^apply.arg */
      argtab[2].meltbp_aptr = (melt_ptr_t *) & /*_.E2RK__V10*/ meltfptr[9];
      /*^apply.arg */
      argtab[3].meltbp_aptr =
	(melt_ptr_t *) & ( /*!konst_1 */ meltfrout->tabval[1]);
      /*^apply.arg */
      argtab[4].meltbp_aptr =
	(melt_ptr_t *) & ( /*!konst_2 */ meltfrout->tabval[2]);
      /*^apply.arg */
      argtab[5].meltbp_aptr =
	(melt_ptr_t *) & ( /*!konst_3 */ meltfrout->tabval[3]);
      /*_.COMPARE_OBJ_RANKED__V11*/ meltfptr[10] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!COMPARE_OBJ_RANKED */ meltfrout->tabval[0])),
		    (melt_ptr_t) ( /*_.E1AT__V5*/ meltfptr[4]),
		    (MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_PTR
		     MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_PTR ""),
		    argtab, "", (union meltparam_un *) 0);
    }
    ;
    /*_.LET___V4*/ meltfptr[3] = /*_.COMPARE_OBJ_RANKED__V11*/ meltfptr[10];;

    MELT_LOCATION ("warmelt-debug.melt:1135:/ clear");
	   /*clear *//*_.E1AT__V5*/ meltfptr[4] = 0;
    /*^clear */
	   /*clear *//*_.E1VA__V6*/ meltfptr[5] = 0;
    /*^clear */
	   /*clear *//*_.E1RK__V7*/ meltfptr[6] = 0;
    /*^clear */
	   /*clear *//*_.E2AT__V8*/ meltfptr[7] = 0;
    /*^clear */
	   /*clear *//*_.E2VA__V9*/ meltfptr[8] = 0;
    /*^clear */
	   /*clear *//*_.E2RK__V10*/ meltfptr[9] = 0;
    /*^clear */
	   /*clear *//*_.COMPARE_OBJ_RANKED__V11*/ meltfptr[10] = 0;
    MELT_LOCATION ("warmelt-debug.melt:1134:/ quasiblock");


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.LET___V4*/ meltfptr[3];;

    {
      MELT_LOCATION ("warmelt-debug.melt:1134:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.LET___V4*/ meltfptr[3] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("LAMBDA_", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_32_warmelt_debug_LAMBDA___6___melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_32_warmelt_debug_LAMBDA___6__ */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_33_warmelt_debug_LAMBDA___7__ (meltclosure_ptr_t meltclosp_,
					melt_ptr_t meltfirstargp_,
					const melt_argdescr_cell_t
					meltxargdescr_[],
					union meltparam_un * meltxargtab_,
					const melt_argdescr_cell_t
					meltxresdescr_[],
					union meltparam_un * meltxrestab_)
{
  long current_blocklevel_signals_meltrout_33_warmelt_debug_LAMBDA___7___melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_33_warmelt_debug_LAMBDA___7___st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 9
    melt_ptr_t mcfr_varptr[9];
#define MELTFRAM_NBVARNUM 6
    long mcfr_varnum[6];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_33_warmelt_debug_LAMBDA___7__ is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_33_warmelt_debug_LAMBDA___7___st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 9; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_33_warmelt_debug_LAMBDA___7__ nbval 9*/
  meltfram__.mcfr_nbvar = 9 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("LAMBDA_", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-debug.melt:1150:/ getarg");
 /*_.EL__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_LONG)
    goto lab_endgetargs;
 /*_#IX__L1*/ meltfnum[0] = meltxargtab_[0].meltbp_long;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {

    MELT_LOCATION ("warmelt-debug.melt:1151:/ quasiblock");


 /*_.CURSTR__V4*/ meltfptr[3] =
      (melt_multiple_nth ((melt_ptr_t) ( /*_.EL__V2*/ meltfptr[1]), (0)));;
    /*^compute */
 /*_.CURVAL__V5*/ meltfptr[4] =
      (melt_multiple_nth ((melt_ptr_t) ( /*_.EL__V2*/ meltfptr[1]), (1)));;
    /*^compute */
 /*_#NEXTDEPTH__L2*/ meltfnum[1] =
      (melt_get_int
       ((melt_ptr_t) (( /*~NEXTDEPTHBOX */ meltfclos->tabval[0]))));;
    MELT_LOCATION ("warmelt-debug.melt:1155:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^checksignal */
    MELT_CHECK_SIGNAL ();
    ;
 /*_#IS_STRING__L3*/ meltfnum[2] =
      (melt_magic_discr ((melt_ptr_t) ( /*_.CURSTR__V4*/ meltfptr[3])) ==
       MELTOBMAG_STRING);;
    MELT_LOCATION ("warmelt-debug.melt:1155:/ cond");
    /*cond */ if ( /*_#IS_STRING__L3*/ meltfnum[2])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

   /*_#NOTNULL__L5*/ meltfnum[4] =
	    (( /*_.CURVAL__V5*/ meltfptr[4]) != NULL);;
	  /*^compute */
	  /*_#IF___L4*/ meltfnum[3] = /*_#NOTNULL__L5*/ meltfnum[4];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-debug.melt:1155:/ clear");
	     /*clear *//*_#NOTNULL__L5*/ meltfnum[4] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

  /*_#IF___L4*/ meltfnum[3] = 0;;
      }
    ;
    MELT_LOCATION ("warmelt-debug.melt:1155:/ cond");
    /*cond */ if ( /*_#IF___L4*/ meltfnum[3])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{


	  {
	    MELT_LOCATION ("warmelt-debug.melt:1157:/ locexp");
	    meltgc_strbuf_add_indent ((melt_ptr_t)
				      (( /*~SBUF */ meltfclos->tabval[1])),
				      ( /*_#NEXTDEPTH__L2*/ meltfnum[1]), 0);
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-debug.melt:1158:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t)
				 (( /*~SBUF */ meltfclos->tabval[1])),
				 ("!*"));
	  }
	  ;
	  MELT_LOCATION ("warmelt-debug.melt:1159:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^apply */
	  /*apply */
	  {
	    union meltparam_un argtab[2];
	    memset (&argtab, 0, sizeof (argtab));
	    /*^apply.arg */
	    argtab[0].meltbp_aptr =
	      (melt_ptr_t *) & ( /*~DBGI */ meltfclos->tabval[2]);
	    /*^apply.arg */
	    argtab[1].meltbp_long = /*_#NEXTDEPTH__L2*/ meltfnum[1];
	    /*_.DBG_OUT__V7*/ meltfptr[6] =
	      melt_apply ((meltclosure_ptr_t)
			  (( /*!DBG_OUT */ meltfrout->tabval[0])),
			  (melt_ptr_t) ( /*_.CURSTR__V4*/ meltfptr[3]),
			  (MELTBPARSTR_PTR MELTBPARSTR_LONG ""), argtab, "",
			  (union meltparam_un *) 0);
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-debug.melt:1160:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t)
				 (( /*~SBUF */ meltfclos->tabval[1])),
				 (" => "));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-debug.melt:1161:/ locexp");
	    meltgc_strbuf_add_indent ((melt_ptr_t)
				      (( /*~SBUF */ meltfclos->tabval[1])),
				      ( /*_#NEXTDEPTH__L2*/ meltfnum[1]), 64);
	  }
	  ;
   /*_#I__L6*/ meltfnum[4] =
	    (( /*_#NEXTDEPTH__L2*/ meltfnum[1]) + (2));;
	  MELT_LOCATION ("warmelt-debug.melt:1162:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^apply */
	  /*apply */
	  {
	    union meltparam_un argtab[2];
	    memset (&argtab, 0, sizeof (argtab));
	    /*^apply.arg */
	    argtab[0].meltbp_aptr =
	      (melt_ptr_t *) & ( /*~DBGI */ meltfclos->tabval[2]);
	    /*^apply.arg */
	    argtab[1].meltbp_long = /*_#I__L6*/ meltfnum[4];
	    /*_.DBG_OUT__V8*/ meltfptr[7] =
	      melt_apply ((meltclosure_ptr_t)
			  (( /*!DBG_OUT */ meltfrout->tabval[0])),
			  (melt_ptr_t) ( /*_.CURVAL__V5*/ meltfptr[4]),
			  (MELTBPARSTR_PTR MELTBPARSTR_LONG ""), argtab, "",
			  (union meltparam_un *) 0);
	  }
	  ;
	  MELT_LOCATION ("warmelt-debug.melt:1156:/ quasiblock");


	  /*_.PROGN___V9*/ meltfptr[8] = /*_.DBG_OUT__V8*/ meltfptr[7];;
	  /*^compute */
	  /*_.IF___V6*/ meltfptr[5] = /*_.PROGN___V9*/ meltfptr[8];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-debug.melt:1155:/ clear");
	     /*clear *//*_.DBG_OUT__V7*/ meltfptr[6] = 0;
	  /*^clear */
	     /*clear *//*_#I__L6*/ meltfnum[4] = 0;
	  /*^clear */
	     /*clear *//*_.DBG_OUT__V8*/ meltfptr[7] = 0;
	  /*^clear */
	     /*clear *//*_.PROGN___V9*/ meltfptr[8] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

  /*_.IF___V6*/ meltfptr[5] = NULL;;
      }
    ;
    /*^compute */
    /*_.LET___V3*/ meltfptr[2] = /*_.IF___V6*/ meltfptr[5];;

    MELT_LOCATION ("warmelt-debug.melt:1151:/ clear");
	   /*clear *//*_.CURSTR__V4*/ meltfptr[3] = 0;
    /*^clear */
	   /*clear *//*_.CURVAL__V5*/ meltfptr[4] = 0;
    /*^clear */
	   /*clear *//*_#NEXTDEPTH__L2*/ meltfnum[1] = 0;
    /*^clear */
	   /*clear *//*_#IS_STRING__L3*/ meltfnum[2] = 0;
    /*^clear */
	   /*clear *//*_#IF___L4*/ meltfnum[3] = 0;
    /*^clear */
	   /*clear *//*_.IF___V6*/ meltfptr[5] = 0;
    MELT_LOCATION ("warmelt-debug.melt:1150:/ quasiblock");


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.LET___V3*/ meltfptr[2];;

    {
      MELT_LOCATION ("warmelt-debug.melt:1150:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.LET___V3*/ meltfptr[2] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("LAMBDA_", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_33_warmelt_debug_LAMBDA___7___melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_33_warmelt_debug_LAMBDA___7__ */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_34_warmelt_debug_DBGOUT_BUCKETLONG_METHOD (meltclosure_ptr_t
						    meltclosp_,
						    melt_ptr_t meltfirstargp_,
						    const melt_argdescr_cell_t
						    meltxargdescr_[],
						    union meltparam_un *
						    meltxargtab_,
						    const melt_argdescr_cell_t
						    meltxresdescr_[],
						    union meltparam_un *
						    meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_34_warmelt_debug_DBGOUT_BUCKETLONG_METHOD_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_34_warmelt_debug_DBGOUT_BUCKETLONG_METHOD_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 26
    melt_ptr_t mcfr_varptr[26];
#define MELTFRAM_NBVARNUM 16
    long mcfr_varnum[16];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_34_warmelt_debug_DBGOUT_BUCKETLONG_METHOD is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_34_warmelt_debug_DBGOUT_BUCKETLONG_METHOD_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 26; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_34_warmelt_debug_DBGOUT_BUCKETLONG_METHOD nbval 26*/
  meltfram__.mcfr_nbvar = 26 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("DBGOUT_BUCKETLONG_METHOD", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-debug.melt:1170:/ getarg");
 /*_.SELF__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.DBGI__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.DBGI__V3*/ meltfptr[2])) !=
	      NULL);


  /*getarg#2 */
  /*^getarg */
  if (meltxargdescr_[1] != MELTBPAR_LONG)
    goto lab_endgetargs;
 /*_#DEPTH__L1*/ meltfnum[0] = meltxargtab_[1].meltbp_long;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-debug.melt:1171:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L2*/ meltfnum[1] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.DBGI__V3*/ meltfptr[2]),
			     (melt_ptr_t) (( /*!CLASS_DEBUG_INFORMATION */
					    meltfrout->tabval[0])));;
      MELT_LOCATION ("warmelt-debug.melt:1171:/ cond");
      /*cond */ if ( /*_#IS_A__L2*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V5*/ meltfptr[4] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-debug.melt:1171:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check dbgi"),
				  ("warmelt-debug.melt")
				  ? ("warmelt-debug.melt") : __FILE__,
				  (1171) ? (1171) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V5*/ meltfptr[4] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V4*/ meltfptr[3] = /*_.IFELSE___V5*/ meltfptr[4];;
      /*epilog */

      MELT_LOCATION ("warmelt-debug.melt:1171:/ clear");
	     /*clear *//*_#IS_A__L2*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V5*/ meltfptr[4] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V4*/ meltfptr[3] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-debug.melt:1172:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_BUCKETLONG__L3*/ meltfnum[1] =
	(melt_magic_discr ((melt_ptr_t) ( /*_.SELF__V2*/ meltfptr[1])) ==
	 MELTOBMAG_BUCKETLONGS);;
      MELT_LOCATION ("warmelt-debug.melt:1172:/ cond");
      /*cond */ if ( /*_#IS_BUCKETLONG__L3*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V7*/ meltfptr[6] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-debug.melt:1172:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check self"),
				  ("warmelt-debug.melt")
				  ? ("warmelt-debug.melt") : __FILE__,
				  (1172) ? (1172) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V7*/ meltfptr[6] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V6*/ meltfptr[4] = /*_.IFELSE___V7*/ meltfptr[6];;
      /*epilog */

      MELT_LOCATION ("warmelt-debug.melt:1172:/ clear");
	     /*clear *//*_#IS_BUCKETLONG__L3*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V7*/ meltfptr[6] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V6*/ meltfptr[4] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-debug.melt:1173:/ quasiblock");


 /*_.DIS__V9*/ meltfptr[8] =
      ((melt_ptr_t)
       (melt_discr ((melt_ptr_t) ( /*_.SELF__V2*/ meltfptr[1]))));;
    MELT_LOCATION ("warmelt-debug.melt:1174:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.DBGI__V3*/ meltfptr[2]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 0, "DBGI_OUT");
  /*_.SBUF__V10*/ meltfptr[9] = slot;
    };
    ;
 /*_#IX__L4*/ meltfnum[1] = 0;;
    /*^compute */
 /*_#NEXTDEPTH__L5*/ meltfnum[4] =
      ((2) + ( /*_#DEPTH__L1*/ meltfnum[0]));;
    /*^compute */
 /*_.NEXTDEPTHBOX__V11*/ meltfptr[10] =
      (meltgc_new_int
       ((meltobject_ptr_t) (( /*!DISCR_INTEGER */ meltfrout->tabval[1])),
	( /*_#NEXTDEPTH__L5*/ meltfnum[4])));;
    /*^compute */
 /*_.AUX__V12*/ meltfptr[11] =
      melt_longsbucket_aux ((melt_ptr_t) /*_.SELF__V2*/ meltfptr[1]);;
    MELT_LOCATION ("warmelt-debug.melt:1179:/ cond");
    /*cond */ if (
		   /*ifisa */
		   melt_is_instance_of ((melt_ptr_t)
					( /*_.DBGI__V3*/ meltfptr[2]),
					(melt_ptr_t) (( /*!CLASS_DEBUG_INFORMATION */ meltfrout->tabval[0])))
      )				/*then */
      {
	/*^cond.then */
	/*^getslot */
	{
	  melt_ptr_t slot = NULL, obj = NULL;
	  obj = (melt_ptr_t) ( /*_.DBGI__V3*/ meltfptr[2]) /*=obj*/ ;
	  melt_object_get_field (slot, obj, 2, "DBGI_MAXDEPTH");
   /*_.DBGI_MAXDEPTH__V13*/ meltfptr[12] = slot;
	};
	;
      }
    else
      {				/*^cond.else */

  /*_.DBGI_MAXDEPTH__V13*/ meltfptr[12] = NULL;;
      }
    ;
    /*^compute */
 /*_#MAXDEPTH__L6*/ meltfnum[5] =
      (melt_get_int ((melt_ptr_t) ( /*_.DBGI_MAXDEPTH__V13*/ meltfptr[12])));;
    /*^compute */
 /*_#XNUM__L7*/ meltfnum[6] =
      melt_longsbucket_xnum ((melt_ptr_t) /*_.SELF__V2*/ meltfptr[1]);;
    MELT_LOCATION ("warmelt-debug.melt:1182:/ cond");
    /*cond */ if (
		   /*ifisa */
		   melt_is_instance_of ((melt_ptr_t)
					( /*_.DIS__V9*/ meltfptr[8]),
					(melt_ptr_t) (( /*!CLASS_NAMED */
						       meltfrout->tabval[3])))
      )				/*then */
      {
	/*^cond.then */
	/*^getslot */
	{
	  melt_ptr_t slot = NULL, obj = NULL;
	  obj = (melt_ptr_t) ( /*_.DIS__V9*/ meltfptr[8]) /*=obj*/ ;
	  melt_object_get_field (slot, obj, 1, "NAMED_NAME");
   /*_.NAMED_NAME__V14*/ meltfptr[13] = slot;
	};
	;
      }
    else
      {				/*^cond.else */

  /*_.NAMED_NAME__V14*/ meltfptr[13] = NULL;;
      }
    ;
    MELT_LOCATION ("warmelt-debug.melt:1182:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[3];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_cstring = ".bucklong/";
      /*^apply.arg */
      argtab[1].meltbp_aptr =
	(melt_ptr_t *) & /*_.NAMED_NAME__V14*/ meltfptr[13];
      /*^apply.arg */
      argtab[2].meltbp_cstring = "!{";
      /*_.ADD2OUT__V15*/ meltfptr[14] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!ADD2OUT */ meltfrout->tabval[2])),
		    (melt_ptr_t) ( /*_.SBUF__V10*/ meltfptr[9]),
		    (MELTBPARSTR_CSTRING MELTBPARSTR_PTR MELTBPARSTR_CSTRING
		     ""), argtab, "", (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-debug.melt:1183:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#I__L8*/ meltfnum[7] =
      (( /*_#DEPTH__L1*/ meltfnum[0]) <= (0));;
    MELT_LOCATION ("warmelt-debug.melt:1183:/ cond");
    /*cond */ if ( /*_#I__L8*/ meltfnum[7])	/*then */
      {
	/*^cond.then */
	/*_#OR___L9*/ meltfnum[8] = /*_#I__L8*/ meltfnum[7];;
      }
    else
      {
	MELT_LOCATION ("warmelt-debug.melt:1183:/ cond.else");

	/*^block */
	/*anyblock */
	{

   /*_#NOTNULL__L10*/ meltfnum[9] =
	    (( /*_.AUX__V12*/ meltfptr[11]) != NULL);;
	  /*^compute */
	  /*_#OR___L9*/ meltfnum[8] = /*_#NOTNULL__L10*/ meltfnum[9];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-debug.melt:1183:/ clear");
	     /*clear *//*_#NOTNULL__L10*/ meltfnum[9] = 0;
	}
	;
      }
    ;
    /*^cond */
    /*cond */ if ( /*_#OR___L9*/ meltfnum[8])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{


	  {
	    MELT_LOCATION ("warmelt-debug.melt:1184:/ locexp");
	    meltgc_strbuf_add_indent ((melt_ptr_t)
				      ( /*_.SBUF__V10*/ meltfptr[9]),
				      ( /*_#NEXTDEPTH__L5*/ meltfnum[4]), 0);
	  }
	  ;
	  MELT_LOCATION ("warmelt-debug.melt:1185:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^apply */
	  /*apply */
	  {
	    union meltparam_un argtab[1];
	    memset (&argtab, 0, sizeof (argtab));
	    /*^apply.arg */
	    argtab[0].meltbp_cstring = "*aux= ";
	    /*_.ADD2OUT__V17*/ meltfptr[16] =
	      melt_apply ((meltclosure_ptr_t)
			  (( /*!ADD2OUT */ meltfrout->tabval[2])),
			  (melt_ptr_t) ( /*_.SBUF__V10*/ meltfptr[9]),
			  (MELTBPARSTR_CSTRING ""), argtab, "",
			  (union meltparam_un *) 0);
	  }
	  ;
   /*_#I__L11*/ meltfnum[9] =
	    (( /*_#NEXTDEPTH__L5*/ meltfnum[4]) + (2));;
	  MELT_LOCATION ("warmelt-debug.melt:1186:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^apply */
	  /*apply */
	  {
	    union meltparam_un argtab[2];
	    memset (&argtab, 0, sizeof (argtab));
	    /*^apply.arg */
	    argtab[0].meltbp_aptr =
	      (melt_ptr_t *) & /*_.DBGI__V3*/ meltfptr[2];
	    /*^apply.arg */
	    argtab[1].meltbp_long = /*_#I__L11*/ meltfnum[9];
	    /*_.DBG_OUT__V18*/ meltfptr[17] =
	      melt_apply ((meltclosure_ptr_t)
			  (( /*!DBG_OUT */ meltfrout->tabval[4])),
			  (melt_ptr_t) ( /*_.AUX__V12*/ meltfptr[11]),
			  (MELTBPARSTR_PTR MELTBPARSTR_LONG ""), argtab, "",
			  (union meltparam_un *) 0);
	  }
	  ;
	  MELT_LOCATION ("warmelt-debug.melt:1183:/ quasiblock");


	  /*_.PROGN___V19*/ meltfptr[18] = /*_.DBG_OUT__V18*/ meltfptr[17];;
	  /*^compute */
	  /*_.IF___V16*/ meltfptr[15] = /*_.PROGN___V19*/ meltfptr[18];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-debug.melt:1183:/ clear");
	     /*clear *//*_.ADD2OUT__V17*/ meltfptr[16] = 0;
	  /*^clear */
	     /*clear *//*_#I__L11*/ meltfnum[9] = 0;
	  /*^clear */
	     /*clear *//*_.DBG_OUT__V18*/ meltfptr[17] = 0;
	  /*^clear */
	     /*clear *//*_.PROGN___V19*/ meltfptr[18] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

  /*_.IF___V16*/ meltfptr[15] = NULL;;
      }
    ;
    MELT_LOCATION ("warmelt-debug.melt:1188:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#I__L12*/ meltfnum[9] =
      (( /*_#DEPTH__L1*/ meltfnum[0]) <= (0));;
    MELT_LOCATION ("warmelt-debug.melt:1188:/ cond");
    /*cond */ if ( /*_#I__L12*/ meltfnum[9])	/*then */
      {
	/*^cond.then */
	/*_#OR___L13*/ meltfnum[12] = /*_#I__L12*/ meltfnum[9];;
      }
    else
      {
	MELT_LOCATION ("warmelt-debug.melt:1188:/ cond.else");

	/*_#OR___L13*/ meltfnum[12] = /*_#XNUM__L7*/ meltfnum[6];;
      }
    ;
    MELT_LOCATION ("warmelt-debug.melt:1188:/ cond");
    /*cond */ if ( /*_#OR___L13*/ meltfnum[12])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{


	  {
	    MELT_LOCATION ("warmelt-debug.melt:1189:/ locexp");
	    meltgc_strbuf_add_indent ((melt_ptr_t)
				      ( /*_.SBUF__V10*/ meltfptr[9]),
				      ( /*_#NEXTDEPTH__L5*/ meltfnum[4]), 0);
	  }
	  ;
	  MELT_LOCATION ("warmelt-debug.melt:1190:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^apply */
	  /*apply */
	  {
	    union meltparam_un argtab[2];
	    memset (&argtab, 0, sizeof (argtab));
	    /*^apply.arg */
	    argtab[0].meltbp_cstring = "*xnum= ";
	    /*^apply.arg */
	    argtab[1].meltbp_long = /*_#XNUM__L7*/ meltfnum[6];
	    /*_.ADD2OUT__V21*/ meltfptr[17] =
	      melt_apply ((meltclosure_ptr_t)
			  (( /*!ADD2OUT */ meltfrout->tabval[2])),
			  (melt_ptr_t) ( /*_.SBUF__V10*/ meltfptr[9]),
			  (MELTBPARSTR_CSTRING MELTBPARSTR_LONG ""), argtab,
			  "", (union meltparam_un *) 0);
	  }
	  ;
	  MELT_LOCATION ("warmelt-debug.melt:1188:/ quasiblock");


	  /*_.PROGN___V22*/ meltfptr[18] = /*_.ADD2OUT__V21*/ meltfptr[17];;
	  /*^compute */
	  /*_.IF___V20*/ meltfptr[16] = /*_.PROGN___V22*/ meltfptr[18];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-debug.melt:1188:/ clear");
	     /*clear *//*_.ADD2OUT__V21*/ meltfptr[17] = 0;
	  /*^clear */
	     /*clear *//*_.PROGN___V22*/ meltfptr[18] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

  /*_.IF___V20*/ meltfptr[16] = NULL;;
      }
    ;
    /*citerblock FOREACH_IN_BUCKETLONG */
    {
      /*foreach_in_bucketlong meltcit1__EACHBUCKLONG */ unsigned
	meltcit1__EACHBUCKLONG_ix = 0, meltcit1__EACHBUCKLONG_cnt = 0;
  /*_#KEY__L14*/ meltfnum[13] = 0L;
  /*_.VAL__V23*/ meltfptr[17] = NULL;
      for (meltcit1__EACHBUCKLONG_ix = 0;
	   /* retrieve in meltcit1__EACHBUCKLONG iteration the count, it might change! */
	   (meltcit1__EACHBUCKLONG_cnt =
	    melt_longsbucket_count ((melt_ptr_t) /*_.SELF__V2*/ meltfptr[1]))
	   > 0
	   && meltcit1__EACHBUCKLONG_ix < meltcit1__EACHBUCKLONG_cnt;
	   meltcit1__EACHBUCKLONG_ix++)
	{
     /*_#KEY__L14*/ meltfnum[13] = 0L;
     /*_.VAL__V23*/ meltfptr[17] = NULL;
	  {
	    struct melt_bucketlongentry_st *meltcit1__EACHBUCKLONG_buent
	      =
	      ((struct meltbucketlongs_st *) /*_.SELF__V2*/ meltfptr[1])->
	      buckl_entab + meltcit1__EACHBUCKLONG_ix;
	    if (!meltcit1__EACHBUCKLONG_buent->ebl_va)
	      continue;
       /*_#KEY__L14*/ meltfnum[13] =
	      meltcit1__EACHBUCKLONG_buent->ebl_at;
       /*_.VAL__V23*/ meltfptr[17] =
	      meltcit1__EACHBUCKLONG_buent->ebl_va;
	    meltcit1__EACHBUCKLONG_buent = NULL;
	  }
	  /*foreach_in_bucketlong meltcit1__EACHBUCKLONG body: */




	  {
	    MELT_LOCATION ("warmelt-debug.melt:1195:/ locexp");
	    meltgc_strbuf_add_indent ((melt_ptr_t)
				      ( /*_.SBUF__V10*/ meltfptr[9]),
				      ( /*_#NEXTDEPTH__L5*/ meltfnum[4]), 0);
	  }
	  ;
	  MELT_LOCATION ("warmelt-debug.melt:1196:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^apply */
	  /*apply */
	  {
	    union meltparam_un argtab[3];
	    memset (&argtab, 0, sizeof (argtab));
	    /*^apply.arg */
	    argtab[0].meltbp_cstring = "* ";
	    /*^apply.arg */
	    argtab[1].meltbp_long = /*_#KEY__L14*/ meltfnum[13];
	    /*^apply.arg */
	    argtab[2].meltbp_cstring = "= ";
	    /*_.ADD2OUT__V24*/ meltfptr[18] =
	      melt_apply ((meltclosure_ptr_t)
			  (( /*!ADD2OUT */ meltfrout->tabval[2])),
			  (melt_ptr_t) ( /*_.SBUF__V10*/ meltfptr[9]),
			  (MELTBPARSTR_CSTRING MELTBPARSTR_LONG
			   MELTBPARSTR_CSTRING ""), argtab, "",
			  (union meltparam_un *) 0);
	  }
	  ;
	  MELT_LOCATION ("warmelt-debug.melt:1197:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^apply */
	  /*apply */
	  {
	    union meltparam_un argtab[2];
	    memset (&argtab, 0, sizeof (argtab));
	    /*^apply.arg */
	    argtab[0].meltbp_aptr =
	      (melt_ptr_t *) & /*_.DBGI__V3*/ meltfptr[2];
	    /*^apply.arg */
	    argtab[1].meltbp_long = /*_#NEXTDEPTH__L5*/ meltfnum[4];
	    /*_.DBG_OUT__V25*/ meltfptr[24] =
	      melt_apply ((meltclosure_ptr_t)
			  (( /*!DBG_OUT */ meltfrout->tabval[4])),
			  (melt_ptr_t) ( /*_.VAL__V23*/ meltfptr[17]),
			  (MELTBPARSTR_PTR MELTBPARSTR_LONG ""), argtab, "",
			  (union meltparam_un *) 0);
	  }
	  ;
	  /* ending foreach_in_bucketlong meltcit1__EACHBUCKLONG */
     /*_#KEY__L14*/ meltfnum[13] = 0L;
     /*_.VAL__V23*/ meltfptr[17] = NULL;
	}			/* end foreach_in_bucketlong meltcit1__EACHBUCKLONG_ix */


      /*citerepilog */

      MELT_LOCATION ("warmelt-debug.melt:1192:/ clear");
	    /*clear *//*_#KEY__L14*/ meltfnum[13] = 0;
      /*^clear */
	    /*clear *//*_.VAL__V23*/ meltfptr[17] = 0;
      /*^clear */
	    /*clear *//*_.ADD2OUT__V24*/ meltfptr[18] = 0;
      /*^clear */
	    /*clear *//*_.DBG_OUT__V25*/ meltfptr[24] = 0;
    }				/*endciterblock FOREACH_IN_BUCKETLONG */
    ;
    MELT_LOCATION ("warmelt-debug.melt:1199:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#BUCKETLONG_COUNT__L15*/ meltfnum[14] =
      melt_longsbucket_count ((melt_ptr_t) /*_.SELF__V2*/ meltfptr[1]);;
    MELT_LOCATION ("warmelt-debug.melt:1199:/ cond");
    /*cond */ if ( /*_#BUCKETLONG_COUNT__L15*/ meltfnum[14])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

   /*_#I__L16*/ meltfnum[15] =
	    ((1) + ( /*_#DEPTH__L1*/ meltfnum[0]));;

	  {
	    MELT_LOCATION ("warmelt-debug.melt:1200:/ locexp");
	    meltgc_strbuf_add_indent ((melt_ptr_t)
				      ( /*_.SBUF__V10*/ meltfptr[9]),
				      ( /*_#I__L16*/ meltfnum[15]), 0);
	  }
	  ;
	  /*epilog */

	  MELT_LOCATION ("warmelt-debug.melt:1199:/ clear");
	     /*clear *//*_#I__L16*/ meltfnum[15] = 0;
	}
	;
      }				/*noelse */
    ;
    MELT_LOCATION ("warmelt-debug.melt:1201:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_cstring = "}!";
      /*_.ADD2OUT__V26*/ meltfptr[25] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!ADD2OUT */ meltfrout->tabval[2])),
		    (melt_ptr_t) ( /*_.SBUF__V10*/ meltfptr[9]),
		    (MELTBPARSTR_CSTRING ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;
    /*_.LET___V8*/ meltfptr[6] = /*_.ADD2OUT__V26*/ meltfptr[25];;

    MELT_LOCATION ("warmelt-debug.melt:1173:/ clear");
	   /*clear *//*_.DIS__V9*/ meltfptr[8] = 0;
    /*^clear */
	   /*clear *//*_.SBUF__V10*/ meltfptr[9] = 0;
    /*^clear */
	   /*clear *//*_#IX__L4*/ meltfnum[1] = 0;
    /*^clear */
	   /*clear *//*_#NEXTDEPTH__L5*/ meltfnum[4] = 0;
    /*^clear */
	   /*clear *//*_.NEXTDEPTHBOX__V11*/ meltfptr[10] = 0;
    /*^clear */
	   /*clear *//*_.AUX__V12*/ meltfptr[11] = 0;
    /*^clear */
	   /*clear *//*_.DBGI_MAXDEPTH__V13*/ meltfptr[12] = 0;
    /*^clear */
	   /*clear *//*_#MAXDEPTH__L6*/ meltfnum[5] = 0;
    /*^clear */
	   /*clear *//*_#XNUM__L7*/ meltfnum[6] = 0;
    /*^clear */
	   /*clear *//*_.NAMED_NAME__V14*/ meltfptr[13] = 0;
    /*^clear */
	   /*clear *//*_.ADD2OUT__V15*/ meltfptr[14] = 0;
    /*^clear */
	   /*clear *//*_#I__L8*/ meltfnum[7] = 0;
    /*^clear */
	   /*clear *//*_#OR___L9*/ meltfnum[8] = 0;
    /*^clear */
	   /*clear *//*_.IF___V16*/ meltfptr[15] = 0;
    /*^clear */
	   /*clear *//*_#I__L12*/ meltfnum[9] = 0;
    /*^clear */
	   /*clear *//*_#OR___L13*/ meltfnum[12] = 0;
    /*^clear */
	   /*clear *//*_.IF___V20*/ meltfptr[16] = 0;
    /*^clear */
	   /*clear *//*_#BUCKETLONG_COUNT__L15*/ meltfnum[14] = 0;
    /*^clear */
	   /*clear *//*_.ADD2OUT__V26*/ meltfptr[25] = 0;
    MELT_LOCATION ("warmelt-debug.melt:1170:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.LET___V8*/ meltfptr[6];;

    {
      MELT_LOCATION ("warmelt-debug.melt:1170:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V4*/ meltfptr[3] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V6*/ meltfptr[4] = 0;
    /*^clear */
	   /*clear *//*_.LET___V8*/ meltfptr[6] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("DBGOUT_BUCKETLONG_METHOD", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_34_warmelt_debug_DBGOUT_BUCKETLONG_METHOD_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_34_warmelt_debug_DBGOUT_BUCKETLONG_METHOD */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_35_warmelt_debug_DBGOUT_ANYOBJECT_METHOD (meltclosure_ptr_t
						   meltclosp_,
						   melt_ptr_t meltfirstargp_,
						   const melt_argdescr_cell_t
						   meltxargdescr_[],
						   union meltparam_un *
						   meltxargtab_,
						   const melt_argdescr_cell_t
						   meltxresdescr_[],
						   union meltparam_un *
						   meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_35_warmelt_debug_DBGOUT_ANYOBJECT_METHOD_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_35_warmelt_debug_DBGOUT_ANYOBJECT_METHOD_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 10
    melt_ptr_t mcfr_varptr[10];
#define MELTFRAM_NBVARNUM 13
    long mcfr_varnum[13];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_35_warmelt_debug_DBGOUT_ANYOBJECT_METHOD is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_35_warmelt_debug_DBGOUT_ANYOBJECT_METHOD_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 10; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_35_warmelt_debug_DBGOUT_ANYOBJECT_METHOD nbval 10*/
  meltfram__.mcfr_nbvar = 10 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("DBGOUT_ANYOBJECT_METHOD", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-debug.melt:1207:/ getarg");
 /*_.SELF__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.DBGI__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.DBGI__V3*/ meltfptr[2])) !=
	      NULL);


  /*getarg#2 */
  /*^getarg */
  if (meltxargdescr_[1] != MELTBPAR_LONG)
    goto lab_endgetargs;
 /*_#DEPTH__L1*/ meltfnum[0] = meltxargtab_[1].meltbp_long;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-debug.melt:1208:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L2*/ meltfnum[1] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.DBGI__V3*/ meltfptr[2]),
			     (melt_ptr_t) (( /*!CLASS_DEBUG_INFORMATION */
					    meltfrout->tabval[0])));;
      MELT_LOCATION ("warmelt-debug.melt:1208:/ cond");
      /*cond */ if ( /*_#IS_A__L2*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V5*/ meltfptr[4] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-debug.melt:1208:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check dbgi"),
				  ("warmelt-debug.melt")
				  ? ("warmelt-debug.melt") : __FILE__,
				  (1208) ? (1208) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V5*/ meltfptr[4] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V4*/ meltfptr[3] = /*_.IFELSE___V5*/ meltfptr[4];;
      /*epilog */

      MELT_LOCATION ("warmelt-debug.melt:1208:/ clear");
	     /*clear *//*_#IS_A__L2*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V5*/ meltfptr[4] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V4*/ meltfptr[3] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-debug.melt:1209:/ quasiblock");


 /*_.DIS__V6*/ meltfptr[4] =
      ((melt_ptr_t)
       (melt_discr ((melt_ptr_t) ( /*_.SELF__V2*/ meltfptr[1]))));;
    MELT_LOCATION ("warmelt-debug.melt:1210:/ cond");
    /*cond */ if (
		   /*ifisa */
		   melt_is_instance_of ((melt_ptr_t)
					( /*_.DBGI__V3*/ meltfptr[2]),
					(melt_ptr_t) (( /*!CLASS_DEBUG_INFORMATION */ meltfrout->tabval[0])))
      )				/*then */
      {
	/*^cond.then */
	/*^getslot */
	{
	  melt_ptr_t slot = NULL, obj = NULL;
	  obj = (melt_ptr_t) ( /*_.DBGI__V3*/ meltfptr[2]) /*=obj*/ ;
	  melt_object_get_field (slot, obj, 2, "DBGI_MAXDEPTH");
   /*_.DBGI_MAXDEPTH__V7*/ meltfptr[6] = slot;
	};
	;
      }
    else
      {				/*^cond.else */

  /*_.DBGI_MAXDEPTH__V7*/ meltfptr[6] = NULL;;
      }
    ;
    /*^compute */
 /*_#MAXDEPTH__L3*/ meltfnum[1] =
      (melt_get_int ((melt_ptr_t) ( /*_.DBGI_MAXDEPTH__V7*/ meltfptr[6])));;
    MELT_LOCATION ("warmelt-debug.melt:1211:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.DBGI__V3*/ meltfptr[2]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 0, "DBGI_OUT");
  /*_.SBUF__V8*/ meltfptr[7] = slot;
    };
    ;

    {
      MELT_LOCATION ("warmelt-debug.melt:1212:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V8*/ meltfptr[7]), ("|"));
    }
    ;
    MELT_LOCATION ("warmelt-debug.melt:1213:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.DIS__V6*/ meltfptr[4]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 1, "NAMED_NAME");
  /*_.NAMED_NAME__V9*/ meltfptr[8] = slot;
    };
    ;

    {
      /*^locexp */
      /*add2sbuf_string */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V8*/ meltfptr[7]),
			   melt_string_str ((melt_ptr_t)
					    ( /*_.NAMED_NAME__V9*/
					     meltfptr[8])));
    }
    ;

    {
      MELT_LOCATION ("warmelt-debug.melt:1214:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V8*/ meltfptr[7]), ("/"));
    }
    ;
 /*_#OBJ_HASH__L4*/ meltfnum[3] =
      (melt_obj_hash ((melt_ptr_t) ( /*_.SELF__V2*/ meltfptr[1])));;

    {
      MELT_LOCATION ("warmelt-debug.melt:1215:/ locexp");
      meltgc_add_strbuf_hex ((melt_ptr_t) ( /*_.SBUF__V8*/ meltfptr[7]),
			     ( /*_#OBJ_HASH__L4*/ meltfnum[3]));
    }
    ;
    MELT_LOCATION ("warmelt-debug.melt:1216:/ quasiblock");


 /*_#ONUM__L5*/ meltfnum[4] =
      (melt_obj_num ((melt_ptr_t) ( /*_.SELF__V2*/ meltfptr[1])));;
    MELT_LOCATION ("warmelt-debug.melt:1218:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^cond */
    /*cond */ if ( /*_#ONUM__L5*/ meltfnum[4])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{


	  {
	    MELT_LOCATION ("warmelt-debug.melt:1220:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V8*/ meltfptr[7]),
				 ("#"));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-debug.melt:1221:/ locexp");
	    meltgc_add_strbuf_dec ((melt_ptr_t) ( /*_.SBUF__V8*/ meltfptr[7]),
				   ( /*_#ONUM__L5*/ meltfnum[4]));
	  }
	  ;
	  MELT_LOCATION ("warmelt-debug.melt:1219:/ quasiblock");


	  /*epilog */
	}
	;
      }				/*noelse */
    ;

    MELT_LOCATION ("warmelt-debug.melt:1216:/ clear");
	   /*clear *//*_#ONUM__L5*/ meltfnum[4] = 0;
    MELT_LOCATION ("warmelt-debug.melt:1223:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#I__L6*/ meltfnum[4] =
      (( /*_#DEPTH__L1*/ meltfnum[0]) + (1));;
    /*^compute */
 /*_#MELT_REALLY_NEED_DBGLIM__L7*/ meltfnum[6] =
      ( /*MELT_REALLY_NEED_DBGLIM */
       melt_need_debug_limit ((int) /*_#I__L6*/ meltfnum[4],
			      (int) /*_#MAXDEPTH__L3*/ meltfnum[1]));;
    MELT_LOCATION ("warmelt-debug.melt:1223:/ cond");
    /*cond */ if ( /*_#MELT_REALLY_NEED_DBGLIM__L7*/ meltfnum[6])	/*then */
      {
	/*^cond.then */
	/*_#OR___L8*/ meltfnum[7] =
	  /*_#MELT_REALLY_NEED_DBGLIM__L7*/ meltfnum[6];;
      }
    else
      {
	MELT_LOCATION ("warmelt-debug.melt:1223:/ cond.else");

	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-debug.melt:1225:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
   /*_#I__L9*/ meltfnum[8] =
	    (( /*_#DEPTH__L1*/ meltfnum[0]) + (1));;
	  /*^compute */
   /*_#I__L10*/ meltfnum[9] =
	    (( /*_#I__L9*/ meltfnum[8]) < ( /*_#MAXDEPTH__L3*/ meltfnum[1]));;
	  MELT_LOCATION ("warmelt-debug.melt:1225:/ cond");
	  /*cond */ if ( /*_#I__L10*/ meltfnum[9])	/*then */
	    {
	      /*^cond.then */
	      /*^block */
	      /*anyblock */
	      {

     /*_#IS_A__L12*/ meltfnum[11] =
		  melt_is_instance_of ((melt_ptr_t)
				       ( /*_.DBGI__V3*/ meltfptr[2]),
				       (melt_ptr_t) (( /*!CLASS_DEBUG_OUTPUT_INFORMATION */ meltfrout->tabval[1])));;
		/*^compute */
		/*_#IF___L11*/ meltfnum[10] = /*_#IS_A__L12*/ meltfnum[11];;
		/*epilog */

		MELT_LOCATION ("warmelt-debug.melt:1225:/ clear");
	       /*clear *//*_#IS_A__L12*/ meltfnum[11] = 0;
	      }
	      ;
	    }
	  else
	    {			/*^cond.else */

    /*_#IF___L11*/ meltfnum[10] = 0;;
	    }
	  ;
	  /*^compute */
	  /*_#OR___L8*/ meltfnum[7] = /*_#IF___L11*/ meltfnum[10];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-debug.melt:1223:/ clear");
	     /*clear *//*_#I__L9*/ meltfnum[8] = 0;
	  /*^clear */
	     /*clear *//*_#I__L10*/ meltfnum[9] = 0;
	  /*^clear */
	     /*clear *//*_#IF___L11*/ meltfnum[10] = 0;
	}
	;
      }
    ;
    /*^cond */
    /*cond */ if ( /*_#OR___L8*/ meltfnum[7])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{


	  {
	    MELT_LOCATION ("warmelt-debug.melt:1227:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V8*/ meltfptr[7]),
				 ("{"));
	  }
	  ;
   /*_#I__L13*/ meltfnum[11] =
	    (( /*_#DEPTH__L1*/ meltfnum[0]) + (1));;
	  MELT_LOCATION ("warmelt-debug.melt:1228:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^apply */
	  /*apply */
	  {
	    union meltparam_un argtab[4];
	    memset (&argtab, 0, sizeof (argtab));
	    /*^apply.arg */
	    argtab[0].meltbp_aptr =
	      (melt_ptr_t *) & /*_.DBGI__V3*/ meltfptr[2];
	    /*^apply.arg */
	    argtab[1].meltbp_long = /*_#I__L13*/ meltfnum[11];
	    /*^apply.arg */
	    argtab[2].meltbp_long = 0;
	    /*^apply.arg */
	    argtab[3].meltbp_long = 0;
	    /*_.DBGOUT_FIELDS__V10*/ meltfptr[9] =
	      melt_apply ((meltclosure_ptr_t)
			  (( /*!DBGOUT_FIELDS */ meltfrout->tabval[2])),
			  (melt_ptr_t) ( /*_.SELF__V2*/ meltfptr[1]),
			  (MELTBPARSTR_PTR MELTBPARSTR_LONG MELTBPARSTR_LONG
			   MELTBPARSTR_LONG ""), argtab, "",
			  (union meltparam_un *) 0);
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-debug.melt:1229:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V8*/ meltfptr[7]),
				 ("}"));
	  }
	  ;
	  MELT_LOCATION ("warmelt-debug.melt:1226:/ quasiblock");


	  /*epilog */

	  MELT_LOCATION ("warmelt-debug.melt:1223:/ clear");
	     /*clear *//*_#I__L13*/ meltfnum[11] = 0;
	  /*^clear */
	     /*clear *//*_.DBGOUT_FIELDS__V10*/ meltfptr[9] = 0;
	}
	;
      }				/*noelse */
    ;

    MELT_LOCATION ("warmelt-debug.melt:1209:/ clear");
	   /*clear *//*_.DIS__V6*/ meltfptr[4] = 0;
    /*^clear */
	   /*clear *//*_.DBGI_MAXDEPTH__V7*/ meltfptr[6] = 0;
    /*^clear */
	   /*clear *//*_#MAXDEPTH__L3*/ meltfnum[1] = 0;
    /*^clear */
	   /*clear *//*_.SBUF__V8*/ meltfptr[7] = 0;
    /*^clear */
	   /*clear *//*_.NAMED_NAME__V9*/ meltfptr[8] = 0;
    /*^clear */
	   /*clear *//*_#OBJ_HASH__L4*/ meltfnum[3] = 0;
    /*^clear */
	   /*clear *//*_#I__L6*/ meltfnum[4] = 0;
    /*^clear */
	   /*clear *//*_#MELT_REALLY_NEED_DBGLIM__L7*/ meltfnum[6] = 0;
    /*^clear */
	   /*clear *//*_#OR___L8*/ meltfnum[7] = 0;
    MELT_LOCATION ("warmelt-debug.melt:1207:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V4*/ meltfptr[3] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("DBGOUT_ANYOBJECT_METHOD", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_35_warmelt_debug_DBGOUT_ANYOBJECT_METHOD_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*noretval */ NULL);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_35_warmelt_debug_DBGOUT_ANYOBJECT_METHOD */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_36_warmelt_debug_DBGOUT_ANYRECV_METHOD (meltclosure_ptr_t meltclosp_,
						 melt_ptr_t meltfirstargp_,
						 const melt_argdescr_cell_t
						 meltxargdescr_[],
						 union meltparam_un *
						 meltxargtab_,
						 const melt_argdescr_cell_t
						 meltxresdescr_[],
						 union meltparam_un *
						 meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_36_warmelt_debug_DBGOUT_ANYRECV_METHOD_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_36_warmelt_debug_DBGOUT_ANYRECV_METHOD_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 8
    melt_ptr_t mcfr_varptr[8];
#define MELTFRAM_NBVARNUM 3
    long mcfr_varnum[3];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_36_warmelt_debug_DBGOUT_ANYRECV_METHOD is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_36_warmelt_debug_DBGOUT_ANYRECV_METHOD_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 8; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_36_warmelt_debug_DBGOUT_ANYRECV_METHOD nbval 8*/
  meltfram__.mcfr_nbvar = 8 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("DBGOUT_ANYRECV_METHOD", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-debug.melt:1235:/ getarg");
 /*_.SELF__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.DBGI__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.DBGI__V3*/ meltfptr[2])) !=
	      NULL);


  /*getarg#2 */
  /*^getarg */
  if (meltxargdescr_[1] != MELTBPAR_LONG)
    goto lab_endgetargs;
 /*_#DEPTH__L1*/ meltfnum[0] = meltxargtab_[1].meltbp_long;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-debug.melt:1236:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L2*/ meltfnum[1] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.DBGI__V3*/ meltfptr[2]),
			     (melt_ptr_t) (( /*!CLASS_DEBUG_INFORMATION */
					    meltfrout->tabval[0])));;
      MELT_LOCATION ("warmelt-debug.melt:1236:/ cond");
      /*cond */ if ( /*_#IS_A__L2*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V5*/ meltfptr[4] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-debug.melt:1236:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check dbgi"),
				  ("warmelt-debug.melt")
				  ? ("warmelt-debug.melt") : __FILE__,
				  (1236) ? (1236) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V5*/ meltfptr[4] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V4*/ meltfptr[3] = /*_.IFELSE___V5*/ meltfptr[4];;
      /*epilog */

      MELT_LOCATION ("warmelt-debug.melt:1236:/ clear");
	     /*clear *//*_#IS_A__L2*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V5*/ meltfptr[4] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V4*/ meltfptr[3] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-debug.melt:1237:/ quasiblock");


 /*_.DIS__V6*/ meltfptr[4] =
      ((melt_ptr_t)
       (melt_discr ((melt_ptr_t) ( /*_.SELF__V2*/ meltfptr[1]))));;
    MELT_LOCATION ("warmelt-debug.melt:1238:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.DBGI__V3*/ meltfptr[2]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 0, "DBGI_OUT");
  /*_.SBUF__V7*/ meltfptr[6] = slot;
    };
    ;

    {
      MELT_LOCATION ("warmelt-debug.melt:1239:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V7*/ meltfptr[6]),
			   (" ?."));
    }
    ;
    MELT_LOCATION ("warmelt-debug.melt:1240:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#IS_A__L3*/ meltfnum[1] =
      melt_is_instance_of ((melt_ptr_t) ( /*_.DIS__V6*/ meltfptr[4]),
			   (melt_ptr_t) (( /*!CLASS_NAMED */ meltfrout->
					  tabval[1])));;
    MELT_LOCATION ("warmelt-debug.melt:1240:/ cond");
    /*cond */ if ( /*_#IS_A__L3*/ meltfnum[1])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  /*^getslot */
	  {
	    melt_ptr_t slot = NULL, obj = NULL;
	    obj = (melt_ptr_t) ( /*_.DIS__V6*/ meltfptr[4]) /*=obj*/ ;
	    melt_object_get_field (slot, obj, 1, "NAMED_NAME");
    /*_.NAMED_NAME__V8*/ meltfptr[7] = slot;
	  };
	  ;

	  {
	    /*^locexp */
	    /*add2sbuf_string */
	      meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V7*/ meltfptr[6]),
				 melt_string_str ((melt_ptr_t)
						  ( /*_.NAMED_NAME__V8*/
						   meltfptr[7])));
	  }
	  ;
	  /*epilog */

	  /*^clear */
	     /*clear *//*_.NAMED_NAME__V8*/ meltfptr[7] = 0;
	}
	;
      }				/*noelse */
    ;

    {
      MELT_LOCATION ("warmelt-debug.melt:1241:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V7*/ meltfptr[6]),
			   (".? "));
    }
    ;

    MELT_LOCATION ("warmelt-debug.melt:1237:/ clear");
	   /*clear *//*_.DIS__V6*/ meltfptr[4] = 0;
    /*^clear */
	   /*clear *//*_.SBUF__V7*/ meltfptr[6] = 0;
    /*^clear */
	   /*clear *//*_#IS_A__L3*/ meltfnum[1] = 0;
    MELT_LOCATION ("warmelt-debug.melt:1235:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V4*/ meltfptr[3] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("DBGOUT_ANYRECV_METHOD", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_36_warmelt_debug_DBGOUT_ANYRECV_METHOD_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*noretval */ NULL);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_36_warmelt_debug_DBGOUT_ANYRECV_METHOD */



/**** end of warmelt-debug+01.c ****/
