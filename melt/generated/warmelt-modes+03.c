/* GCC MELT GENERATED FILE warmelt-modes+03.c - DO NOT EDIT */
/* secondary MELT generated C file of rank #3 */
#include "melt-run.h"


/* used hash from melt-run.h when compiling this file: */
MELT_EXTERN const char meltrun_used_md5_melt_f3[] =
  MELT_RUN_HASHMD5 /* from melt-run.h */ ;


/**** warmelt-modes+03.c declarations ****/


/***************************************************
***
    Copyright (C) 2011, 2012 Free Software Foundation, Inc.
    Contributed by Basile Starynkevitch <basile@starynkevitch.net>

    This file is part of GCC.

    GCC is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3, or (at your option)
    any later version.

    GCC is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with GCC; see the file COPYING3.  If not see
    <http://www.gnu.org/licenses/>.
***
****************************************************/

/* ordinary MELT module */
#define MELT_HAS_INITIAL_ENVIRONMENT 1	/*usual */

struct melt_callframe_st;	/*defined in melt-runtime.h */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_1_warmelt_modes_INSTALL_MELT_MODE (meltclosure_ptr_t meltclosp_,
					    melt_ptr_t meltfirstargp_,
					    const melt_argdescr_cell_t
					    meltxargdescr_[],
					    union meltparam_un *meltxargtab_,
					    const melt_argdescr_cell_t
					    meltxresdescr_[],
					    union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_2_warmelt_modes_RUNFILE_DOCMD (meltclosure_ptr_t meltclosp_,
					melt_ptr_t meltfirstargp_,
					const melt_argdescr_cell_t
					meltxargdescr_[],
					union meltparam_un *meltxargtab_,
					const melt_argdescr_cell_t
					meltxresdescr_[],
					union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_3_warmelt_modes_RUNDEBUG_DOCMD (meltclosure_ptr_t meltclosp_,
					 melt_ptr_t meltfirstargp_,
					 const melt_argdescr_cell_t
					 meltxargdescr_[],
					 union meltparam_un *meltxargtab_,
					 const melt_argdescr_cell_t
					 meltxresdescr_[],
					 union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_4_warmelt_modes_EVAL_DOCMD (meltclosure_ptr_t meltclosp_,
				     melt_ptr_t meltfirstargp_,
				     const melt_argdescr_cell_t
				     meltxargdescr_[],
				     union meltparam_un *meltxargtab_,
				     const melt_argdescr_cell_t
				     meltxresdescr_[],
				     union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_5_warmelt_modes_REPL_PROCESSOR (meltclosure_ptr_t meltclosp_,
					 melt_ptr_t meltfirstargp_,
					 const melt_argdescr_cell_t
					 meltxargdescr_[],
					 union meltparam_un *meltxargtab_,
					 const melt_argdescr_cell_t
					 meltxresdescr_[],
					 union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_6_warmelt_modes_REPL_DOCMD (meltclosure_ptr_t meltclosp_,
				     melt_ptr_t meltfirstargp_,
				     const melt_argdescr_cell_t
				     meltxargdescr_[],
				     union meltparam_un *meltxargtab_,
				     const melt_argdescr_cell_t
				     meltxresdescr_[],
				     union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_7_warmelt_modes_LAMBDA___1__ (meltclosure_ptr_t meltclosp_,
				       melt_ptr_t meltfirstargp_,
				       const melt_argdescr_cell_t
				       meltxargdescr_[],
				       union meltparam_un *meltxargtab_,
				       const melt_argdescr_cell_t
				       meltxresdescr_[],
				       union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_8_warmelt_modes_LAMBDA___2__ (meltclosure_ptr_t meltclosp_,
				       melt_ptr_t meltfirstargp_,
				       const melt_argdescr_cell_t
				       meltxargdescr_[],
				       union meltparam_un *meltxargtab_,
				       const melt_argdescr_cell_t
				       meltxresdescr_[],
				       union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_9_warmelt_modes_LAMBDA___3__ (meltclosure_ptr_t meltclosp_,
				       melt_ptr_t meltfirstargp_,
				       const melt_argdescr_cell_t
				       meltxargdescr_[],
				       union meltparam_un *meltxargtab_,
				       const melt_argdescr_cell_t
				       meltxresdescr_[],
				       union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_10_warmelt_modes_LAMBDA___4__ (meltclosure_ptr_t meltclosp_,
					melt_ptr_t meltfirstargp_,
					const melt_argdescr_cell_t
					meltxargdescr_[],
					union meltparam_un *meltxargtab_,
					const melt_argdescr_cell_t
					meltxresdescr_[],
					union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_11_warmelt_modes_LAMBDA___5__ (meltclosure_ptr_t meltclosp_,
					melt_ptr_t meltfirstargp_,
					const melt_argdescr_cell_t
					meltxargdescr_[],
					union meltparam_un *meltxargtab_,
					const melt_argdescr_cell_t
					meltxresdescr_[],
					union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_12_warmelt_modes_INCREMENT_MKDOC_COUNTER (meltclosure_ptr_t
						   meltclosp_,
						   melt_ptr_t meltfirstargp_,
						   const melt_argdescr_cell_t
						   meltxargdescr_[],
						   union meltparam_un
						   *meltxargtab_,
						   const melt_argdescr_cell_t
						   meltxresdescr_[],
						   union meltparam_un
						   *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_13_warmelt_modes_MAKEDOC_SCANINPUT (meltclosure_ptr_t meltclosp_,
					     melt_ptr_t meltfirstargp_,
					     const melt_argdescr_cell_t
					     meltxargdescr_[],
					     union meltparam_un *meltxargtab_,
					     const melt_argdescr_cell_t
					     meltxresdescr_[],
					     union meltparam_un
					     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_14_warmelt_modes_LAMBDA___6__ (meltclosure_ptr_t meltclosp_,
					melt_ptr_t meltfirstargp_,
					const melt_argdescr_cell_t
					meltxargdescr_[],
					union meltparam_un *meltxargtab_,
					const melt_argdescr_cell_t
					meltxresdescr_[],
					union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_15_warmelt_modes_LAMBDA___7__ (meltclosure_ptr_t meltclosp_,
					melt_ptr_t meltfirstargp_,
					const melt_argdescr_cell_t
					meltxargdescr_[],
					union meltparam_un *meltxargtab_,
					const melt_argdescr_cell_t
					meltxresdescr_[],
					union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_16_warmelt_modes_LAMBDA___8__ (meltclosure_ptr_t meltclosp_,
					melt_ptr_t meltfirstargp_,
					const melt_argdescr_cell_t
					meltxargdescr_[],
					union meltparam_un *meltxargtab_,
					const melt_argdescr_cell_t
					meltxresdescr_[],
					union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_17_warmelt_modes_LAMBDA___9__ (meltclosure_ptr_t meltclosp_,
					melt_ptr_t meltfirstargp_,
					const melt_argdescr_cell_t
					meltxargdescr_[],
					union meltparam_un *meltxargtab_,
					const melt_argdescr_cell_t
					meltxresdescr_[],
					union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_18_warmelt_modes_MAKEDOC_OUTDEFLOC (meltclosure_ptr_t meltclosp_,
					     melt_ptr_t meltfirstargp_,
					     const melt_argdescr_cell_t
					     meltxargdescr_[],
					     union meltparam_un *meltxargtab_,
					     const melt_argdescr_cell_t
					     meltxresdescr_[],
					     union meltparam_un
					     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_19_warmelt_modes_MAKEDOC_OUTFORMALS (meltclosure_ptr_t meltclosp_,
					      melt_ptr_t meltfirstargp_,
					      const melt_argdescr_cell_t
					      meltxargdescr_[],
					      union meltparam_un
					      *meltxargtab_,
					      const melt_argdescr_cell_t
					      meltxresdescr_[],
					      union meltparam_un
					      *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_20_warmelt_modes_MAKEDOC_OUTDOC (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_21_warmelt_modes_MAKEDOC_OUTCLASSDEF (meltclosure_ptr_t meltclosp_,
					       melt_ptr_t meltfirstargp_,
					       const melt_argdescr_cell_t
					       meltxargdescr_[],
					       union meltparam_un
					       *meltxargtab_,
					       const melt_argdescr_cell_t
					       meltxresdescr_[],
					       union meltparam_un
					       *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_22_warmelt_modes_MAKEDOC_GENMACRO (meltclosure_ptr_t meltclosp_,
					    melt_ptr_t meltfirstargp_,
					    const melt_argdescr_cell_t
					    meltxargdescr_[],
					    union meltparam_un *meltxargtab_,
					    const melt_argdescr_cell_t
					    meltxresdescr_[],
					    union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_23_warmelt_modes_LAMBDA___10__ (meltclosure_ptr_t meltclosp_,
					 melt_ptr_t meltfirstargp_,
					 const melt_argdescr_cell_t
					 meltxargdescr_[],
					 union meltparam_un *meltxargtab_,
					 const melt_argdescr_cell_t
					 meltxresdescr_[],
					 union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_24_warmelt_modes_MAKEDOC_GENPATMACRO (meltclosure_ptr_t meltclosp_,
					       melt_ptr_t meltfirstargp_,
					       const melt_argdescr_cell_t
					       meltxargdescr_[],
					       union meltparam_un
					       *meltxargtab_,
					       const melt_argdescr_cell_t
					       meltxresdescr_[],
					       union meltparam_un
					       *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_25_warmelt_modes_LAMBDA___11__ (meltclosure_ptr_t meltclosp_,
					 melt_ptr_t meltfirstargp_,
					 const melt_argdescr_cell_t
					 meltxargdescr_[],
					 union meltparam_un *meltxargtab_,
					 const melt_argdescr_cell_t
					 meltxresdescr_[],
					 union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_26_warmelt_modes_MAKEDOC_GENCLASS (meltclosure_ptr_t meltclosp_,
					    melt_ptr_t meltfirstargp_,
					    const melt_argdescr_cell_t
					    meltxargdescr_[],
					    union meltparam_un *meltxargtab_,
					    const melt_argdescr_cell_t
					    meltxresdescr_[],
					    union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_27_warmelt_modes_LAMBDA___12__ (meltclosure_ptr_t meltclosp_,
					 melt_ptr_t meltfirstargp_,
					 const melt_argdescr_cell_t
					 meltxargdescr_[],
					 union meltparam_un *meltxargtab_,
					 const melt_argdescr_cell_t
					 meltxresdescr_[],
					 union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_28_warmelt_modes_LAMBDA___13__ (meltclosure_ptr_t meltclosp_,
					 melt_ptr_t meltfirstargp_,
					 const melt_argdescr_cell_t
					 meltxargdescr_[],
					 union meltparam_un *meltxargtab_,
					 const melt_argdescr_cell_t
					 meltxresdescr_[],
					 union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_29_warmelt_modes_LAMBDA___14__ (meltclosure_ptr_t meltclosp_,
					 melt_ptr_t meltfirstargp_,
					 const melt_argdescr_cell_t
					 meltxargdescr_[],
					 union meltparam_un *meltxargtab_,
					 const melt_argdescr_cell_t
					 meltxresdescr_[],
					 union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_30_warmelt_modes_LAMBDA___15__ (meltclosure_ptr_t meltclosp_,
					 melt_ptr_t meltfirstargp_,
					 const melt_argdescr_cell_t
					 meltxargdescr_[],
					 union meltparam_un *meltxargtab_,
					 const melt_argdescr_cell_t
					 meltxresdescr_[],
					 union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_31_warmelt_modes_MAKEDOC_OUTPRIMITIVEDEF (meltclosure_ptr_t
						   meltclosp_,
						   melt_ptr_t meltfirstargp_,
						   const melt_argdescr_cell_t
						   meltxargdescr_[],
						   union meltparam_un
						   *meltxargtab_,
						   const melt_argdescr_cell_t
						   meltxresdescr_[],
						   union meltparam_un
						   *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_32_warmelt_modes_MAKEDOC_GENPRIMITIVE (meltclosure_ptr_t meltclosp_,
						melt_ptr_t meltfirstargp_,
						const melt_argdescr_cell_t
						meltxargdescr_[],
						union meltparam_un
						*meltxargtab_,
						const melt_argdescr_cell_t
						meltxresdescr_[],
						union meltparam_un
						*meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_33_warmelt_modes_LAMBDA___16__ (meltclosure_ptr_t meltclosp_,
					 melt_ptr_t meltfirstargp_,
					 const melt_argdescr_cell_t
					 meltxargdescr_[],
					 union meltparam_un *meltxargtab_,
					 const melt_argdescr_cell_t
					 meltxresdescr_[],
					 union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_34_warmelt_modes_MAKEDOC_OUTFUNCTIONDEF (meltclosure_ptr_t
						  meltclosp_,
						  melt_ptr_t meltfirstargp_,
						  const melt_argdescr_cell_t
						  meltxargdescr_[],
						  union meltparam_un
						  *meltxargtab_,
						  const melt_argdescr_cell_t
						  meltxresdescr_[],
						  union meltparam_un
						  *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_35_warmelt_modes_MAKEDOC_GENFUNCTION (meltclosure_ptr_t meltclosp_,
					       melt_ptr_t meltfirstargp_,
					       const melt_argdescr_cell_t
					       meltxargdescr_[],
					       union meltparam_un
					       *meltxargtab_,
					       const melt_argdescr_cell_t
					       meltxresdescr_[],
					       union meltparam_un
					       *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_36_warmelt_modes_LAMBDA___17__ (meltclosure_ptr_t meltclosp_,
					 melt_ptr_t meltfirstargp_,
					 const melt_argdescr_cell_t
					 meltxargdescr_[],
					 union meltparam_un *meltxargtab_,
					 const melt_argdescr_cell_t
					 meltxresdescr_[],
					 union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_37_warmelt_modes_MAKEDOC_GENCITERATOR (meltclosure_ptr_t meltclosp_,
						melt_ptr_t meltfirstargp_,
						const melt_argdescr_cell_t
						meltxargdescr_[],
						union meltparam_un
						*meltxargtab_,
						const melt_argdescr_cell_t
						meltxresdescr_[],
						union meltparam_un
						*meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_38_warmelt_modes_LAMBDA___18__ (meltclosure_ptr_t meltclosp_,
					 melt_ptr_t meltfirstargp_,
					 const melt_argdescr_cell_t
					 meltxargdescr_[],
					 union meltparam_un *meltxargtab_,
					 const melt_argdescr_cell_t
					 meltxresdescr_[],
					 union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_39_warmelt_modes_MAKEDOC_GENCMATCHER (meltclosure_ptr_t meltclosp_,
					       melt_ptr_t meltfirstargp_,
					       const melt_argdescr_cell_t
					       meltxargdescr_[],
					       union meltparam_un
					       *meltxargtab_,
					       const melt_argdescr_cell_t
					       meltxresdescr_[],
					       union meltparam_un
					       *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_40_warmelt_modes_LAMBDA___19__ (meltclosure_ptr_t meltclosp_,
					 melt_ptr_t meltfirstargp_,
					 const melt_argdescr_cell_t
					 meltxargdescr_[],
					 union meltparam_un *meltxargtab_,
					 const melt_argdescr_cell_t
					 meltxresdescr_[],
					 union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_41_warmelt_modes_MAKEDOC_GENOUTPUT (meltclosure_ptr_t meltclosp_,
					     melt_ptr_t meltfirstargp_,
					     const melt_argdescr_cell_t
					     meltxargdescr_[],
					     union meltparam_un *meltxargtab_,
					     const melt_argdescr_cell_t
					     meltxresdescr_[],
					     union meltparam_un
					     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_42_warmelt_modes_MAKEDOC_DOCMD (meltclosure_ptr_t meltclosp_,
					 melt_ptr_t meltfirstargp_,
					 const melt_argdescr_cell_t
					 meltxargdescr_[],
					 union meltparam_un *meltxargtab_,
					 const melt_argdescr_cell_t
					 meltxresdescr_[],
					 union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_43_warmelt_modes_LAMBDA___20__ (meltclosure_ptr_t meltclosp_,
					 melt_ptr_t meltfirstargp_,
					 const melt_argdescr_cell_t
					 meltxargdescr_[],
					 union meltparam_un *meltxargtab_,
					 const melt_argdescr_cell_t
					 meltxresdescr_[],
					 union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_44_warmelt_modes_SHOWVAR_DOCMD (meltclosure_ptr_t meltclosp_,
					 melt_ptr_t meltfirstargp_,
					 const melt_argdescr_cell_t
					 meltxargdescr_[],
					 union meltparam_un *meltxargtab_,
					 const melt_argdescr_cell_t
					 meltxresdescr_[],
					 union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_45_warmelt_modes_HELP_DOCMD (meltclosure_ptr_t meltclosp_,
				      melt_ptr_t meltfirstargp_,
				      const melt_argdescr_cell_t
				      meltxargdescr_[],
				      union meltparam_un *meltxargtab_,
				      const melt_argdescr_cell_t
				      meltxresdescr_[],
				      union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_46_warmelt_modes_NOP_DOCMD (meltclosure_ptr_t meltclosp_,
				     melt_ptr_t meltfirstargp_,
				     const melt_argdescr_cell_t
				     meltxargdescr_[],
				     union meltparam_un *meltxargtab_,
				     const melt_argdescr_cell_t
				     meltxresdescr_[],
				     union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_47_warmelt_modes_GENERATE_RUNTYPESUPPORT_ENUM_OBJMAGIC
(meltclosure_ptr_t meltclosp_, melt_ptr_t meltfirstargp_, const melt_argdescr_cell_t meltxargdescr_[],
union meltparam_un *meltxargtab_, const melt_argdescr_cell_t meltxresdescr_[],
union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_48_warmelt_modes_GENERATE_RUNTYPESUPPORT_GTY (meltclosure_ptr_t
						       meltclosp_,
						       melt_ptr_t
						       meltfirstargp_,
						       const
						       melt_argdescr_cell_t
						       meltxargdescr_[],
						       union meltparam_un
						       *meltxargtab_,
						       const
						       melt_argdescr_cell_t
						       meltxresdescr_[],
						       union meltparam_un
						       *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_49_warmelt_modes_GENERATE_RUNTYPESUPPORT_FORWCOPY_FUN
(meltclosure_ptr_t meltclosp_, melt_ptr_t meltfirstargp_, const melt_argdescr_cell_t meltxargdescr_[],
union meltparam_un *meltxargtab_, const melt_argdescr_cell_t meltxresdescr_[],
union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_50_warmelt_modes_GENERATE_RUNTYPESUPPORT_CLONING_FUN
(meltclosure_ptr_t meltclosp_, melt_ptr_t meltfirstargp_, const melt_argdescr_cell_t meltxargdescr_[],
union meltparam_un *meltxargtab_, const melt_argdescr_cell_t meltxresdescr_[],
union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_51_warmelt_modes_GENERATE_RUNTYPESUPPORT_SCANNING (meltclosure_ptr_t
							    meltclosp_,
							    melt_ptr_t
							    meltfirstargp_,
							    const
							    melt_argdescr_cell_t
							    meltxargdescr_[],
							    union meltparam_un
							    *meltxargtab_,
							    const
							    melt_argdescr_cell_t
							    meltxresdescr_[],
							    union meltparam_un
							    *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_52_warmelt_modes_GENERATE_RUNTYPESUPPORT_PARAM (meltclosure_ptr_t
							 meltclosp_,
							 melt_ptr_t
							 meltfirstargp_,
							 const
							 melt_argdescr_cell_t
							 meltxargdescr_[],
							 union meltparam_un
							 *meltxargtab_,
							 const
							 melt_argdescr_cell_t
							 meltxresdescr_[],
							 union meltparam_un
							 *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_53_warmelt_modes_GENERATE_RUNTYPESUPPORT_COD2CTYPE (meltclosure_ptr_t
							     meltclosp_,
							     melt_ptr_t
							     meltfirstargp_,
							     const
							     melt_argdescr_cell_t
							     meltxargdescr_[],
							     union
							     meltparam_un
							     *meltxargtab_,
							     const
							     melt_argdescr_cell_t
							     meltxresdescr_[],
							     union
							     meltparam_un
							     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_54_warmelt_modes_GENERATE_RUNTYPESUPPORT_MAG2STR (meltclosure_ptr_t
							   meltclosp_,
							   melt_ptr_t
							   meltfirstargp_,
							   const
							   melt_argdescr_cell_t
							   meltxargdescr_[],
							   union meltparam_un
							   *meltxargtab_,
							   const
							   melt_argdescr_cell_t
							   meltxresdescr_[],
							   union meltparam_un
							   *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_55_warmelt_modes_GENERATE_RUNTYPESUPPORT_BOXINGFUN (meltclosure_ptr_t
							     meltclosp_,
							     melt_ptr_t
							     meltfirstargp_,
							     const
							     melt_argdescr_cell_t
							     meltxargdescr_[],
							     union
							     meltparam_un
							     *meltxargtab_,
							     const
							     melt_argdescr_cell_t
							     meltxresdescr_[],
							     union
							     meltparam_un
							     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_56_warmelt_modes_GENERATE_RUNTYPESUPPORT_MAPFUN (meltclosure_ptr_t
							  meltclosp_,
							  melt_ptr_t
							  meltfirstargp_,
							  const
							  melt_argdescr_cell_t
							  meltxargdescr_[],
							  union meltparam_un
							  *meltxargtab_,
							  const
							  melt_argdescr_cell_t
							  meltxresdescr_[],
							  union meltparam_un
							  *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_57_warmelt_modes_GENERATE_RUNTYPESUPPORT_PREDEF_FIELDS
(meltclosure_ptr_t meltclosp_, melt_ptr_t meltfirstargp_, const melt_argdescr_cell_t meltxargdescr_[],
union meltparam_un *meltxargtab_, const melt_argdescr_cell_t meltxresdescr_[],
union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_58_warmelt_modes_RUNTYPESUPPORT_DOCMD (meltclosure_ptr_t meltclosp_,
						melt_ptr_t meltfirstargp_,
						const melt_argdescr_cell_t
						meltxargdescr_[],
						union meltparam_un
						*meltxargtab_,
						const melt_argdescr_cell_t
						meltxresdescr_[],
						union meltparam_un
						*meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_59_warmelt_modes_TRANSLATE_TO_FLAVORED_MODULE (meltclosure_ptr_t
							meltclosp_,
							melt_ptr_t
							meltfirstargp_,
							const
							melt_argdescr_cell_t
							meltxargdescr_[],
							union meltparam_un
							*meltxargtab_,
							const
							melt_argdescr_cell_t
							meltxresdescr_[],
							union meltparam_un
							*meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_60_warmelt_modes_TRANSLATETOMODULE_DOCMD (meltclosure_ptr_t
						   meltclosp_,
						   melt_ptr_t meltfirstargp_,
						   const melt_argdescr_cell_t
						   meltxargdescr_[],
						   union meltparam_un
						   *meltxargtab_,
						   const melt_argdescr_cell_t
						   meltxresdescr_[],
						   union meltparam_un
						   *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_61_warmelt_modes_TRANSLATEDEBUG_DOCMD (meltclosure_ptr_t meltclosp_,
						melt_ptr_t meltfirstargp_,
						const melt_argdescr_cell_t
						meltxargdescr_[],
						union meltparam_un
						*meltxargtab_,
						const melt_argdescr_cell_t
						meltxresdescr_[],
						union meltparam_un
						*meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_62_warmelt_modes_TRANSLATEQUICKLY_DOCMD (meltclosure_ptr_t
						  meltclosp_,
						  melt_ptr_t meltfirstargp_,
						  const melt_argdescr_cell_t
						  meltxargdescr_[],
						  union meltparam_un
						  *meltxargtab_,
						  const melt_argdescr_cell_t
						  meltxresdescr_[],
						  union meltparam_un
						  *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_63_warmelt_modes_TRANSLATEOPTIMIZED_DOCMD (meltclosure_ptr_t
						    meltclosp_,
						    melt_ptr_t meltfirstargp_,
						    const melt_argdescr_cell_t
						    meltxargdescr_[],
						    union meltparam_un
						    *meltxargtab_,
						    const melt_argdescr_cell_t
						    meltxresdescr_[],
						    union meltparam_un
						    *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_64_warmelt_modes_TRANSLATEFILE_DOCMD (meltclosure_ptr_t meltclosp_,
					       melt_ptr_t meltfirstargp_,
					       const melt_argdescr_cell_t
					       meltxargdescr_[],
					       union meltparam_un
					       *meltxargtab_,
					       const melt_argdescr_cell_t
					       meltxresdescr_[],
					       union meltparam_un
					       *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_65_warmelt_modes_TRANSLATEINIT_DOCMD (meltclosure_ptr_t meltclosp_,
					       melt_ptr_t meltfirstargp_,
					       const melt_argdescr_cell_t
					       meltxargdescr_[],
					       union meltparam_un
					       *meltxargtab_,
					       const melt_argdescr_cell_t
					       meltxresdescr_[],
					       union meltparam_un
					       *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_66_warmelt_modes_LAMBDA___21__ (meltclosure_ptr_t meltclosp_,
					 melt_ptr_t meltfirstargp_,
					 const melt_argdescr_cell_t
					 meltxargdescr_[],
					 union meltparam_un *meltxargtab_,
					 const melt_argdescr_cell_t
					 meltxresdescr_[],
					 union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_67_warmelt_modes_TRANSLATEINIT_FLAVORED (meltclosure_ptr_t
						  meltclosp_,
						  melt_ptr_t meltfirstargp_,
						  const melt_argdescr_cell_t
						  meltxargdescr_[],
						  union meltparam_un
						  *meltxargtab_,
						  const melt_argdescr_cell_t
						  meltxresdescr_[],
						  union meltparam_un
						  *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_68_warmelt_modes_LAMBDA___22__ (meltclosure_ptr_t meltclosp_,
					 melt_ptr_t meltfirstargp_,
					 const melt_argdescr_cell_t
					 meltxargdescr_[],
					 union meltparam_un *meltxargtab_,
					 const melt_argdescr_cell_t
					 meltxresdescr_[],
					 union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_69_warmelt_modes_TRANSLATEINITMODULE_DOCMD (meltclosure_ptr_t
						     meltclosp_,
						     melt_ptr_t
						     meltfirstargp_,
						     const
						     melt_argdescr_cell_t
						     meltxargdescr_[],
						     union meltparam_un
						     *meltxargtab_,
						     const
						     melt_argdescr_cell_t
						     meltxresdescr_[],
						     union meltparam_un
						     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_70_warmelt_modes_TRANSLATEINITMODULEQUICKLYBUILT_DOCMD
(meltclosure_ptr_t meltclosp_, melt_ptr_t meltfirstargp_, const melt_argdescr_cell_t meltxargdescr_[],
union meltparam_un *meltxargtab_, const melt_argdescr_cell_t meltxresdescr_[],
union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_71_warmelt_modes_TRANSLATEINITMODULEDEBUGNOLINE_DOCMD
(meltclosure_ptr_t meltclosp_, melt_ptr_t meltfirstargp_, const melt_argdescr_cell_t meltxargdescr_[],
union meltparam_un *meltxargtab_, const melt_argdescr_cell_t meltxresdescr_[],
union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_72_warmelt_modes_TRANSLATEINITMODULEOPTIMIZED_DOCMD
(meltclosure_ptr_t meltclosp_, melt_ptr_t meltfirstargp_, const melt_argdescr_cell_t meltxargdescr_[],
union meltparam_un *meltxargtab_, const melt_argdescr_cell_t meltxresdescr_[],
union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_73_warmelt_modes_REGISTER_INPUT_CHANNEL_HANDLER (meltclosure_ptr_t
							  meltclosp_,
							  melt_ptr_t
							  meltfirstargp_,
							  const
							  melt_argdescr_cell_t
							  meltxargdescr_[],
							  union meltparam_un
							  *meltxargtab_,
							  const
							  melt_argdescr_cell_t
							  meltxresdescr_[],
							  union meltparam_un
							  *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_74_warmelt_modes_HANDLE_ALARM_SIGNAL (meltclosure_ptr_t meltclosp_,
					       melt_ptr_t meltfirstargp_,
					       const melt_argdescr_cell_t
					       meltxargdescr_[],
					       union meltparam_un
					       *meltxargtab_,
					       const melt_argdescr_cell_t
					       meltxresdescr_[],
					       union meltparam_un
					       *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_75_warmelt_modes_LAMBDA___23__ (meltclosure_ptr_t meltclosp_,
					 melt_ptr_t meltfirstargp_,
					 const melt_argdescr_cell_t
					 meltxargdescr_[],
					 union meltparam_un *meltxargtab_,
					 const melt_argdescr_cell_t
					 meltxresdescr_[],
					 union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_76_warmelt_modes_REGISTER_ALARM_TIMER (meltclosure_ptr_t meltclosp_,
						melt_ptr_t meltfirstargp_,
						const melt_argdescr_cell_t
						meltxargdescr_[],
						union meltparam_un
						*meltxargtab_,
						const melt_argdescr_cell_t
						meltxresdescr_[],
						union meltparam_un
						*meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_77_warmelt_modes_UNREGISTER_ALARM_TIMER (meltclosure_ptr_t
						  meltclosp_,
						  melt_ptr_t meltfirstargp_,
						  const melt_argdescr_cell_t
						  meltxargdescr_[],
						  union meltparam_un
						  *meltxargtab_,
						  const melt_argdescr_cell_t
						  meltxresdescr_[],
						  union meltparam_un
						  *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_78_warmelt_modes_CHILD_PROCESS_SIGCHLD_HANDLER (meltclosure_ptr_t
							 meltclosp_,
							 melt_ptr_t
							 meltfirstargp_,
							 const
							 melt_argdescr_cell_t
							 meltxargdescr_[],
							 union meltparam_un
							 *meltxargtab_,
							 const
							 melt_argdescr_cell_t
							 meltxresdescr_[],
							 union meltparam_un
							 *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_79_warmelt_modes_REGISTER_CHILD_PROCESS_HANDLER (meltclosure_ptr_t
							  meltclosp_,
							  melt_ptr_t
							  meltfirstargp_,
							  const
							  melt_argdescr_cell_t
							  meltxargdescr_[],
							  union meltparam_un
							  *meltxargtab_,
							  const
							  melt_argdescr_cell_t
							  meltxresdescr_[],
							  union meltparam_un
							  *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_80_warmelt_modes_UNREGISTER_CHILD_PROCESS_HANDLER (meltclosure_ptr_t
							    meltclosp_,
							    melt_ptr_t
							    meltfirstargp_,
							    const
							    melt_argdescr_cell_t
							    meltxargdescr_[],
							    union meltparam_un
							    *meltxargtab_,
							    const
							    melt_argdescr_cell_t
							    meltxresdescr_[],
							    union meltparam_un
							    *meltxrestab_);



MELT_EXTERN void *melt_start_this_module (void *);


/* define different names when debugging or not */
#if MELT_HAVE_DEBUG
MELT_EXTERN const char meltmodule_warmelt_modes__melt_have_debug_enabled[];
#define melt_have_debug_string meltmodule_warmelt_modes__melt_have_debug_enabled
#else /*!MELT_HAVE_DEBUG */
MELT_EXTERN const char meltmodule_warmelt_modes__melt_have_debug_disabled[];
#define melt_have_debug_string meltmodule_warmelt_modes__melt_have_debug_disabled
#endif /*!MELT_HAVE_DEBUG */


struct frame_melt_start_this_module_st;
void MELT_MODULE_VISIBILITY meltmod__warmelt_modes__initialmeltchunk_0 (struct
									frame_melt_start_this_module_st
									*,
									char
									*);
void MELT_MODULE_VISIBILITY meltmod__warmelt_modes__initialmeltchunk_1 (struct
									frame_melt_start_this_module_st
									*,
									char
									*);
void MELT_MODULE_VISIBILITY meltmod__warmelt_modes__initialmeltchunk_2 (struct
									frame_melt_start_this_module_st
									*,
									char
									*);
void MELT_MODULE_VISIBILITY meltmod__warmelt_modes__initialmeltchunk_3 (struct
									frame_melt_start_this_module_st
									*,
									char
									*);
void MELT_MODULE_VISIBILITY meltmod__warmelt_modes__initialmeltchunk_4 (struct
									frame_melt_start_this_module_st
									*,
									char
									*);
void MELT_MODULE_VISIBILITY meltmod__warmelt_modes__initialmeltchunk_5 (struct
									frame_melt_start_this_module_st
									*,
									char
									*);
void MELT_MODULE_VISIBILITY meltmod__warmelt_modes__initialmeltchunk_6 (struct
									frame_melt_start_this_module_st
									*,
									char
									*);
void MELT_MODULE_VISIBILITY meltmod__warmelt_modes__initialmeltchunk_7 (struct
									frame_melt_start_this_module_st
									*,
									char
									*);
void MELT_MODULE_VISIBILITY meltmod__warmelt_modes__initialmeltchunk_8 (struct
									frame_melt_start_this_module_st
									*,
									char
									*);
void MELT_MODULE_VISIBILITY meltmod__warmelt_modes__initialmeltchunk_9 (struct
									frame_melt_start_this_module_st
									*,
									char
									*);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_modes__initialmeltchunk_10 (struct
					     frame_melt_start_this_module_st
					     *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_modes__initialmeltchunk_11 (struct
					     frame_melt_start_this_module_st
					     *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_modes__forward_or_mark_module_start_frame (struct
							    melt_callframe_st
							    *fp, int marking);
#define meltmarking_melt_start_this_module  meltmod__warmelt_modes__forward_or_mark_module_start_frame



/**** warmelt-modes+03.c implementations ****/




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_78_warmelt_modes_CHILD_PROCESS_SIGCHLD_HANDLER (meltclosure_ptr_t
							 meltclosp_,
							 melt_ptr_t
							 meltfirstargp_,
							 const
							 melt_argdescr_cell_t
							 meltxargdescr_[],
							 union meltparam_un
							 *meltxargtab_,
							 const
							 melt_argdescr_cell_t
							 meltxresdescr_[],
							 union meltparam_un
							 *meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_78_warmelt_modes_CHILD_PROCESS_SIGCHLD_HANDLER_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_78_warmelt_modes_CHILD_PROCESS_SIGCHLD_HANDLER_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 63
    melt_ptr_t mcfr_varptr[63];
#define MELTFRAM_NBVARNUM 27
    long mcfr_varnum[27];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_78_warmelt_modes_CHILD_PROCESS_SIGCHLD_HANDLER is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct
	 frame_meltrout_78_warmelt_modes_CHILD_PROCESS_SIGCHLD_HANDLER_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 63; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_78_warmelt_modes_CHILD_PROCESS_SIGCHLD_HANDLER nbval 63*/
  meltfram__.mcfr_nbvar = 63 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("CHILD_PROCESS_SIGCHLD_HANDLER", meltcallcount);
/*getargs*/
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
  MELT_LOCATION ("warmelt-modes.melt:4424:/ block");
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-modes.melt:4425:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L1*/ meltfnum[0] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-modes.melt:4425:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L1*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-modes.melt:4425:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-modes.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 4425;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring =
		"child_process_sigchld_handler childproc_bucket_reference=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & ( /*!CHILDPROC_BUCKET_REFERENCE */
				  meltfrout->tabval[1]);
	      /*_.MELT_DEBUG_FUN__V4*/ meltfptr[3] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V3*/ meltfptr[2] = /*_.MELT_DEBUG_FUN__V4*/ meltfptr[3];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-modes.melt:4425:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V4*/ meltfptr[3] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V3*/ meltfptr[2] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-modes.melt:4425:/ quasiblock");


      /*_.PROGN___V5*/ meltfptr[3] = /*_.IF___V3*/ meltfptr[2];;
      /*^compute */
      /*_.IFCPP___V2*/ meltfptr[1] = /*_.PROGN___V5*/ meltfptr[3];;
      /*epilog */

      MELT_LOCATION ("warmelt-modes.melt:4425:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L1*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IF___V3*/ meltfptr[2] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V5*/ meltfptr[3] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V2*/ meltfptr[1] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-modes.melt:4426:/ quasiblock");


 /*_.LISCHPH__V6*/ meltfptr[2] =
      (meltgc_new_list
       ((meltobject_ptr_t) (( /*!DISCR_LIST */ meltfrout->tabval[2]))));;
    /*^compute */
 /*_.LISWSTAT__V7*/ meltfptr[3] =
      (meltgc_new_list
       ((meltobject_ptr_t) (( /*!DISCR_LIST */ meltfrout->tabval[2]))));;
    MELT_LOCATION ("warmelt-modes.melt:4428:/ cond");
    /*cond */ if (
		   /*ifisa */
		   melt_is_instance_of ((melt_ptr_t)
					(( /*!CHILDPROC_BUCKET_REFERENCE */
					  meltfrout->tabval[1])),
					(melt_ptr_t) (( /*!CLASS_REFERENCE */
						       meltfrout->tabval[3])))
      )				/*then */
      {
	/*^cond.then */
	/*^getslot */
	{
	  melt_ptr_t slot = NULL, obj = NULL;
	  obj =
	    (melt_ptr_t) (( /*!CHILDPROC_BUCKET_REFERENCE */ meltfrout->
			   tabval[1])) /*=obj*/ ;
	  melt_object_get_field (slot, obj, 0, "REFERENCED_VALUE");
   /*_.BUCK__V8*/ meltfptr[7] = slot;
	};
	;
      }
    else
      {				/*^cond.else */

  /*_.BUCK__V8*/ meltfptr[7] = NULL;;
      }
    ;
    MELT_LOCATION ("warmelt-modes.melt:4430:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#IS_BUCKETLONG__L3*/ meltfnum[1] =
      (melt_magic_discr ((melt_ptr_t) ( /*_.BUCK__V8*/ meltfptr[7])) ==
       MELTOBMAG_BUCKETLONGS);;
    MELT_LOCATION ("warmelt-modes.melt:4430:/ cond");
    /*cond */ if ( /*_#IS_BUCKETLONG__L3*/ meltfnum[1])	/*then */
      {
	/*^cond.then */
	/*_.IFELSE___V9*/ meltfptr[8] = ( /*nil */ NULL);;
      }
    else
      {
	MELT_LOCATION ("warmelt-modes.melt:4430:/ cond.else");

	/*^block */
	/*anyblock */
	{

	  /*^quasiblock */


   /*_.RETVAL___V1*/ meltfptr[0] = NULL;;

	  {
	    MELT_LOCATION ("warmelt-modes.melt:4430:/ locexp");
	    /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
	    if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	      melt_warn_for_no_expected_secondary_results ();
	    /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
	    ;
	  }
	  ;
	  /*^finalreturn */
	  ;
	  /*finalret */ goto labend_rout;
	  /*^quasiblock */


	  /*_.PROGN___V11*/ meltfptr[10] = /*_.RETURN___V10*/ meltfptr[9];;
	  /*^compute */
	  /*_.IFELSE___V9*/ meltfptr[8] = /*_.PROGN___V11*/ meltfptr[10];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-modes.melt:4430:/ clear");
	     /*clear *//*_.RETURN___V10*/ meltfptr[9] = 0;
	  /*^clear */
	     /*clear *//*_.PROGN___V11*/ meltfptr[10] = 0;
	}
	;
      }
    ;
    /*citerblock BLOCK_SIGNALS */
    {
      /* block_signals meltcit1__BLKSIGNAL start */
      long meltcit1__BLKSIGNAL_lev = melt_blocklevel_signals;
      melt_blocklevel_signals = meltcit1__BLKSIGNAL_lev + 1;



      MELT_LOCATION ("warmelt-modes.melt:4433:/ quasiblock");


  /*_#WAITFAIL__L4*/ meltfnum[0] = 0;;
      /*^compute */
  /*_#PIDSTAT__L5*/ meltfnum[4] = 0;;

      {
	MELT_LOCATION ("warmelt-modes.melt:4438:/ locexp");
	/* child_process_sigchld_handler WAITPROBCHK__1 */
	/*_#WAITFAIL__L4*/ meltfnum[0] = melt_wait_for_probe (WNOHANG);
	;
      }
      ;
      MELT_LOCATION ("warmelt-modes.melt:4441:/ checksignal");
      MELT_CHECK_SIGNAL ();
      ;
      /*^cond */
      /*cond */ if ( /*_#WAITFAIL__L4*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V12*/ meltfptr[9] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-modes.melt:4441:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {


#if MELT_HAVE_DEBUG
	    MELT_LOCATION ("warmelt-modes.melt:4442:/ cppif.then");
	    /*^block */
	    /*anyblock */
	    {


	      {
		/*^locexp */
		/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
		melt_dbgcounter++;
#endif
		;
	      }
	      ;
	      /*^checksignal */
	      MELT_CHECK_SIGNAL ();
	      ;
      /*_#MELT_NEED_DBG__L6*/ meltfnum[5] =
		/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
		( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
		0		/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
		;;
	      MELT_LOCATION ("warmelt-modes.melt:4442:/ cond");
	      /*cond */ if ( /*_#MELT_NEED_DBG__L6*/ meltfnum[5])	/*then */
		{
		  /*^cond.then */
		  /*^block */
		  /*anyblock */
		  {

	/*_#THE_MELTCALLCOUNT__L7*/ meltfnum[6] =
#ifdef meltcallcount
		      meltcallcount	/* the_meltcallcount */
#else
		      0L
#endif /* meltcallcount the_meltcallcount */
		      ;;
		    MELT_LOCATION ("warmelt-modes.melt:4442:/ checksignal");
		    MELT_CHECK_SIGNAL ();
		    ;
		    /*^apply */
		    /*apply */
		    {
		      union meltparam_un argtab[4];
		      memset (&argtab, 0, sizeof (argtab));
		      /*^apply.arg */
		      argtab[0].meltbp_long =
			/*_#THE_MELTCALLCOUNT__L7*/ meltfnum[6];
		      /*^apply.arg */
		      argtab[1].meltbp_cstring = "warmelt-modes.melt";
		      /*^apply.arg */
		      argtab[2].meltbp_long = 4442;
		      /*^apply.arg */
		      argtab[3].meltbp_cstring =
			"child_process_sigchld_handler waited for probe";
		      /*_.MELT_DEBUG_FUN__V15*/ meltfptr[14] =
			melt_apply ((meltclosure_ptr_t)
				    (( /*!MELT_DEBUG_FUN */ meltfrout->
				      tabval[0])),
				    (melt_ptr_t) (( /*nil */ NULL)),
				    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				     MELTBPARSTR_LONG MELTBPARSTR_CSTRING ""),
				    argtab, "", (union meltparam_un *) 0);
		    }
		    ;
		    /*_.IF___V14*/ meltfptr[13] =
		      /*_.MELT_DEBUG_FUN__V15*/ meltfptr[14];;
		    /*epilog */

		    MELT_LOCATION ("warmelt-modes.melt:4442:/ clear");
		  /*clear *//*_#THE_MELTCALLCOUNT__L7*/ meltfnum[6] = 0;
		    /*^clear */
		  /*clear *//*_.MELT_DEBUG_FUN__V15*/ meltfptr[14] = 0;
		  }
		  ;
		}
	      else
		{		/*^cond.else */

       /*_.IF___V14*/ meltfptr[13] = NULL;;
		}
	      ;
	      MELT_LOCATION ("warmelt-modes.melt:4442:/ quasiblock");


	      /*_.PROGN___V16*/ meltfptr[14] = /*_.IF___V14*/ meltfptr[13];;
	      /*^compute */
	      /*_.IFCPP___V13*/ meltfptr[10] =
		/*_.PROGN___V16*/ meltfptr[14];;
	      /*epilog */

	      MELT_LOCATION ("warmelt-modes.melt:4442:/ clear");
		/*clear *//*_#MELT_NEED_DBG__L6*/ meltfnum[5] = 0;
	      /*^clear */
		/*clear *//*_.IF___V14*/ meltfptr[13] = 0;
	      /*^clear */
		/*clear *//*_.PROGN___V16*/ meltfptr[14] = 0;
	    }

#else /*MELT_HAVE_DEBUG */
	    /*^cppif.else */
	    /*_.IFCPP___V13*/ meltfptr[10] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
	    ;
	    MELT_LOCATION ("warmelt-modes.melt:4443:/ quasiblock");


    /*_.RETVAL___V1*/ meltfptr[0] = NULL;;

	    {
	      MELT_LOCATION ("warmelt-modes.melt:4443:/ locexp");
	      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
	      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
		melt_warn_for_no_expected_secondary_results ();
	      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
	      ;
	    }
	    ;
	    /*^finalreturn */
	    ;
	    /*finalret */ goto labend_rout;
	    MELT_LOCATION ("warmelt-modes.melt:4441:/ quasiblock");


	    /*_.PROGN___V18*/ meltfptr[14] = /*_.RETURN___V17*/ meltfptr[13];;
	    /*^compute */
	    /*_.IFELSE___V12*/ meltfptr[9] = /*_.PROGN___V18*/ meltfptr[14];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-modes.melt:4441:/ clear");
	      /*clear *//*_.IFCPP___V13*/ meltfptr[10] = 0;
	    /*^clear */
	      /*clear *//*_.RETURN___V17*/ meltfptr[13] = 0;
	    /*^clear */
	      /*clear *//*_.PROGN___V18*/ meltfptr[14] = 0;
	  }
	  ;
	}
      ;
      /*citerblock FOREACH_IN_BUCKETLONG */
      {
	/*foreach_in_bucketlong meltcit2__EACHBUCKLONG */ unsigned
	  meltcit2__EACHBUCKLONG_ix = 0, meltcit2__EACHBUCKLONG_cnt = 0;
  /*_#KEYPID__L8*/ meltfnum[6] = 0L;
  /*_.CHPH__V19*/ meltfptr[10] = NULL;
	for (meltcit2__EACHBUCKLONG_ix = 0;
	     /* retrieve in meltcit2__EACHBUCKLONG iteration the count, it might change! */
	     (meltcit2__EACHBUCKLONG_cnt =
	      melt_longsbucket_count ((melt_ptr_t) /*_.BUCK__V8*/
				      meltfptr[7])) > 0
	     && meltcit2__EACHBUCKLONG_ix < meltcit2__EACHBUCKLONG_cnt;
	     meltcit2__EACHBUCKLONG_ix++)
	  {
     /*_#KEYPID__L8*/ meltfnum[6] = 0L;
     /*_.CHPH__V19*/ meltfptr[10] = NULL;
	    {
	      struct melt_bucketlongentry_st *meltcit2__EACHBUCKLONG_buent
		=
		((struct meltbucketlongs_st *) /*_.BUCK__V8*/ meltfptr[7])->
		buckl_entab + meltcit2__EACHBUCKLONG_ix;
	      if (!meltcit2__EACHBUCKLONG_buent->ebl_va)
		continue;
       /*_#KEYPID__L8*/ meltfnum[6] =
		meltcit2__EACHBUCKLONG_buent->ebl_at;
       /*_.CHPH__V19*/ meltfptr[10] =
		meltcit2__EACHBUCKLONG_buent->ebl_va;
	      meltcit2__EACHBUCKLONG_buent = NULL;
	    }
	    /*foreach_in_bucketlong meltcit2__EACHBUCKLONG body: */




#if MELT_HAVE_DEBUG
	    MELT_LOCATION ("warmelt-modes.melt:4447:/ cppif.then");
	    /*^block */
	    /*anyblock */
	    {

	      /*^checksignal */
	      MELT_CHECK_SIGNAL ();
	      ;
     /*_#IS_A__L9*/ meltfnum[5] =
		melt_is_instance_of ((melt_ptr_t)
				     ( /*_.CHPH__V19*/ meltfptr[10]),
				     (melt_ptr_t) (( /*!CLASS_CHILD_PROCESS_HANDLER */ meltfrout->tabval[4])));;
	      MELT_LOCATION ("warmelt-modes.melt:4447:/ cond");
	      /*cond */ if ( /*_#IS_A__L9*/ meltfnum[5])	/*then */
		{
		  /*^cond.then */
		  /*_.IFELSE___V21*/ meltfptr[14] = ( /*nil */ NULL);;
		}
	      else
		{
		  MELT_LOCATION ("warmelt-modes.melt:4447:/ cond.else");

		  /*^block */
		  /*anyblock */
		  {




		    {
		      /*^locexp */
		      melt_assert_failed (("check chph"),
					  ("warmelt-modes.melt")
					  ? ("warmelt-modes.melt") : __FILE__,
					  (4447) ? (4447) : __LINE__,
					  __FUNCTION__);
		      ;
		    }
		    ;
		 /*clear *//*_.IFELSE___V21*/ meltfptr[14] = 0;
		    /*epilog */
		  }
		  ;
		}
	      ;
	      /*^compute */
	      /*_.IFCPP___V20*/ meltfptr[13] =
		/*_.IFELSE___V21*/ meltfptr[14];;
	      /*epilog */

	      MELT_LOCATION ("warmelt-modes.melt:4447:/ clear");
	       /*clear *//*_#IS_A__L9*/ meltfnum[5] = 0;
	      /*^clear */
	       /*clear *//*_.IFELSE___V21*/ meltfptr[14] = 0;
	    }

#else /*MELT_HAVE_DEBUG */
	    /*^cppif.else */
	    /*_.IFCPP___V20*/ meltfptr[13] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
	    ;

#if MELT_HAVE_DEBUG
	    MELT_LOCATION ("warmelt-modes.melt:4448:/ cppif.then");
	    /*^block */
	    /*anyblock */
	    {

	      /*^checksignal */
	      MELT_CHECK_SIGNAL ();
	      ;
	      MELT_LOCATION ("warmelt-modes.melt:4449:/ cond");
	      /*cond */ if (
			     /*ifisa */
			     melt_is_instance_of ((melt_ptr_t)
						  ( /*_.CHPH__V19*/
						   meltfptr[10]),
						  (melt_ptr_t) (( /*!CLASS_CHILD_PROCESS_HANDLER */ meltfrout->tabval[4])))
		)		/*then */
		{
		  /*^cond.then */
		  /*^getslot */
		  {
		    melt_ptr_t slot = NULL, obj = NULL;
		    obj =
		      (melt_ptr_t) ( /*_.CHPH__V19*/ meltfptr[10]) /*=obj*/ ;
		    melt_object_get_field (slot, obj, 1, "CHILPROH_PID");
       /*_.CHILPROH_PID__V23*/ meltfptr[22] = slot;
		  };
		  ;
		}
	      else
		{		/*^cond.else */

      /*_.CHILPROH_PID__V23*/ meltfptr[22] = NULL;;
		}
	      ;
	      /*^compute */
     /*_#GET_INT__L10*/ meltfnum[5] =
		(melt_get_int
		 ((melt_ptr_t) ( /*_.CHILPROH_PID__V23*/ meltfptr[22])));;
	      /*^compute */
     /*_#I__L11*/ meltfnum[10] =
		(( /*_#KEYPID__L8*/ meltfnum[6]) ==
		 ( /*_#GET_INT__L10*/ meltfnum[5]));;
	      MELT_LOCATION ("warmelt-modes.melt:4448:/ cond");
	      /*cond */ if ( /*_#I__L11*/ meltfnum[10])	/*then */
		{
		  /*^cond.then */
		  /*_.IFELSE___V24*/ meltfptr[23] = ( /*nil */ NULL);;
		}
	      else
		{
		  MELT_LOCATION ("warmelt-modes.melt:4448:/ cond.else");

		  /*^block */
		  /*anyblock */
		  {




		    {
		      /*^locexp */
		      melt_assert_failed (("check pid"),
					  ("warmelt-modes.melt")
					  ? ("warmelt-modes.melt") : __FILE__,
					  (4448) ? (4448) : __LINE__,
					  __FUNCTION__);
		      ;
		    }
		    ;
		 /*clear *//*_.IFELSE___V24*/ meltfptr[23] = 0;
		    /*epilog */
		  }
		  ;
		}
	      ;
	      /*^compute */
	      /*_.IFCPP___V22*/ meltfptr[14] =
		/*_.IFELSE___V24*/ meltfptr[23];;
	      /*epilog */

	      MELT_LOCATION ("warmelt-modes.melt:4448:/ clear");
	       /*clear *//*_.CHILPROH_PID__V23*/ meltfptr[22] = 0;
	      /*^clear */
	       /*clear *//*_#GET_INT__L10*/ meltfnum[5] = 0;
	      /*^clear */
	       /*clear *//*_#I__L11*/ meltfnum[10] = 0;
	      /*^clear */
	       /*clear *//*_.IFELSE___V24*/ meltfptr[23] = 0;
	    }

#else /*MELT_HAVE_DEBUG */
	    /*^cppif.else */
	    /*_.IFCPP___V22*/ meltfptr[14] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
	    ;

	    {
	      MELT_LOCATION ("warmelt-modes.melt:4452:/ locexp");
	      /* child_process_sigchld_handler WAITPIDCHK__1 */
	      {
		pid_t wpid = 0;
		int pstatus = 0;
	 /*_#WAITFAIL__L4*/ meltfnum[0] = 0L;
	 /*_#PIDSTAT__L5*/ meltfnum[4] = 0L;
		wpid =
		  waitpid ((pid_t) /*_#KEYPID__L8*/ meltfnum[6], &pstatus,
			   WNOHANG);
		/*_#WAITFAIL__L4*/ meltfnum[0] =
		  (wpid != (pid_t) /*_#KEYPID__L8*/ meltfnum[6]);
		if (! /*_#WAITFAIL__L4*/ meltfnum[0])
	   /*_#PIDSTAT__L5*/ meltfnum[4] = pstatus;
	      } /* child_process_sigchld_handler end WAITPIDCHK__1 */ ;
	    }
	    ;
	    MELT_LOCATION ("warmelt-modes.melt:4462:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^cond */
	    /*cond */ if ( /*_#WAITFAIL__L4*/ meltfnum[0])	/*then */
	      {
		/*^cond.then */
		/*_.IFELSE___V25*/ meltfptr[22] = ( /*nil */ NULL);;
	      }
	    else
	      {
		MELT_LOCATION ("warmelt-modes.melt:4462:/ cond.else");

		/*^block */
		/*anyblock */
		{


		  {
		    MELT_LOCATION ("warmelt-modes.melt:4463:/ locexp");
		    meltgc_append_list ((melt_ptr_t)
					( /*_.LISCHPH__V6*/ meltfptr[2]),
					(melt_ptr_t) ( /*_.CHPH__V19*/
						      meltfptr[10]));
		  }
		  ;
     /*_.MAKE_INTEGERBOX__V26*/ meltfptr[23] =
		    (meltgc_new_int
		     ((meltobject_ptr_t)
		      (( /*!DISCR_CONSTANT_INTEGER */ meltfrout->tabval[5])),
		      ( /*_#PIDSTAT__L5*/ meltfnum[4])));;

		  {
		    MELT_LOCATION ("warmelt-modes.melt:4464:/ locexp");
		    meltgc_append_list ((melt_ptr_t)
					( /*_.LISWSTAT__V7*/ meltfptr[3]),
					(melt_ptr_t) ( /*_.MAKE_INTEGERBOX__V26*/ meltfptr[23]));
		  }
		  ;
		  MELT_LOCATION ("warmelt-modes.melt:4462:/ quasiblock");


		  /*epilog */

		  /*^clear */
	       /*clear *//*_.MAKE_INTEGERBOX__V26*/ meltfptr[23] = 0;
		}
		;
	      }
	    ;
	    /* ending foreach_in_bucketlong meltcit2__EACHBUCKLONG */
     /*_#KEYPID__L8*/ meltfnum[6] = 0L;
     /*_.CHPH__V19*/ meltfptr[10] = NULL;
	  }			/* end foreach_in_bucketlong meltcit2__EACHBUCKLONG_ix */


	/*citerepilog */

	MELT_LOCATION ("warmelt-modes.melt:4444:/ clear");
	     /*clear *//*_#KEYPID__L8*/ meltfnum[6] = 0;
	/*^clear */
	     /*clear *//*_.CHPH__V19*/ meltfptr[10] = 0;
	/*^clear */
	     /*clear *//*_.IFCPP___V20*/ meltfptr[13] = 0;
	/*^clear */
	     /*clear *//*_.IFCPP___V22*/ meltfptr[14] = 0;
	/*^clear */
	     /*clear *//*_.IFELSE___V25*/ meltfptr[22] = 0;
      }				/*endciterblock FOREACH_IN_BUCKETLONG */
      ;

      MELT_LOCATION ("warmelt-modes.melt:4433:/ clear");
	    /*clear *//*_#WAITFAIL__L4*/ meltfnum[0] = 0;
      /*^clear */
	    /*clear *//*_#PIDSTAT__L5*/ meltfnum[4] = 0;
      /*^clear */
	    /*clear *//*_.IFELSE___V12*/ meltfptr[9] = 0;
      /* block_signals meltcit1__BLKSIGNAL end */
      melt_blocklevel_signals = meltcit1__BLKSIGNAL_lev;
      MELT_CHECK_SIGNAL ();


      /*citerepilog */
    }				/*endciterblock BLOCK_SIGNALS */
    ;
    MELT_LOCATION ("warmelt-modes.melt:4468:/ quasiblock");


    /*^checksignal */
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & ( /*!DISCR_MULTIPLE */ meltfrout->tabval[7]);
      /*_.TUPCHPH__V27*/ meltfptr[23] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!LIST_TO_MULTIPLE */ meltfrout->tabval[6])),
		    (melt_ptr_t) ( /*_.LISCHPH__V6*/ meltfptr[2]),
		    (MELTBPARSTR_PTR ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-modes.melt:4469:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & ( /*!DISCR_MULTIPLE */ meltfrout->tabval[7]);
      /*_.TUPWSTAT__V28*/ meltfptr[9] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!LIST_TO_MULTIPLE */ meltfrout->tabval[6])),
		    (melt_ptr_t) ( /*_.LISWSTAT__V7*/ meltfptr[3]),
		    (MELTBPARSTR_PTR ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;
    /*citerblock FOREACH_IN_MULTIPLE */
    {
      /* start foreach_in_multiple meltcit3__EACHTUP */
      long meltcit3__EACHTUP_ln =
	melt_multiple_length ((melt_ptr_t) /*_.TUPCHPH__V27*/ meltfptr[23]);
      for ( /*_#IX__L12*/ meltfnum[5] = 0;
	   ( /*_#IX__L12*/ meltfnum[5] >= 0)
	   && ( /*_#IX__L12*/ meltfnum[5] < meltcit3__EACHTUP_ln);
	/*_#IX__L12*/ meltfnum[5]++)
	{
	  /*_.CURCHPH__V29*/ meltfptr[28] =
	    melt_multiple_nth ((melt_ptr_t)
			       ( /*_.TUPCHPH__V27*/ meltfptr[23]),
			       /*_#IX__L12*/ meltfnum[5]);



	  MELT_LOCATION ("warmelt-modes.melt:4474:/ quasiblock");


  /*_.MULTIPLE_NTH__V31*/ meltfptr[30] =
	    (melt_multiple_nth
	     ((melt_ptr_t) ( /*_.TUPWSTAT__V28*/ meltfptr[9]),
	      ( /*_#IX__L12*/ meltfnum[5])));;
	  /*^compute */
  /*_#CURWSTAT__L13*/ meltfnum[10] =
	    (melt_get_int
	     ((melt_ptr_t) ( /*_.MULTIPLE_NTH__V31*/ meltfptr[30])));;
	  MELT_LOCATION ("warmelt-modes.melt:4475:/ cond");
	  /*cond */ if (
			 /*ifisa */
			 melt_is_instance_of ((melt_ptr_t)
					      ( /*_.CURCHPH__V29*/
					       meltfptr[28]),
					      (melt_ptr_t) (( /*!CLASS_CHILD_PROCESS_HANDLER */ meltfrout->tabval[4])))
	    )			/*then */
	    {
	      /*^cond.then */
	      /*^getslot */
	      {
		melt_ptr_t slot = NULL, obj = NULL;
		obj =
		  (melt_ptr_t) ( /*_.CURCHPH__V29*/ meltfptr[28]) /*=obj*/ ;
		melt_object_get_field (slot, obj, 1, "CHILPROH_PID");
    /*_.CHILPROH_PID__V32*/ meltfptr[31] = slot;
	      };
	      ;
	    }
	  else
	    {			/*^cond.else */

   /*_.CHILPROH_PID__V32*/ meltfptr[31] = NULL;;
	    }
	  ;
	  /*^compute */
  /*_#CURPID__L14*/ meltfnum[0] =
	    (melt_get_int
	     ((melt_ptr_t) ( /*_.CHILPROH_PID__V32*/ meltfptr[31])));;
	  MELT_LOCATION ("warmelt-modes.melt:4476:/ cond");
	  /*cond */ if (
			 /*ifisa */
			 melt_is_instance_of ((melt_ptr_t)
					      ( /*_.CURCHPH__V29*/
					       meltfptr[28]),
					      (melt_ptr_t) (( /*!CLASS_CHILD_PROCESS_HANDLER */ meltfrout->tabval[4])))
	    )			/*then */
	    {
	      /*^cond.then */
	      /*^getslot */
	      {
		melt_ptr_t slot = NULL, obj = NULL;
		obj =
		  (melt_ptr_t) ( /*_.CURCHPH__V29*/ meltfptr[28]) /*=obj*/ ;
		melt_object_get_field (slot, obj, 2, "CHILPROH_CLOS");
    /*_.CURCLOS__V33*/ meltfptr[32] = slot;
	      };
	      ;
	    }
	  else
	    {			/*^cond.else */

   /*_.CURCLOS__V33*/ meltfptr[32] = NULL;;
	    }
	  ;
	  /*^compute */
  /*_#EXITED__L15*/ meltfnum[4] = 0;;
	  /*^compute */
  /*_#EXITSTAT__L16*/ meltfnum[15] = 0;;
	  /*^compute */
  /*_#SIGNALED__L17*/ meltfnum[16] = 0;;
	  /*^compute */
  /*_#TERMSIG__L18*/ meltfnum[17] = 0;;
	  /*^compute */
	  /*_.TERMSIGNAME__V34*/ meltfptr[33] = ( /*nil */ NULL);;

#if MELT_HAVE_DEBUG
	  MELT_LOCATION ("warmelt-modes.melt:4483:/ cppif.then");
	  /*^block */
	  /*anyblock */
	  {


	    {
	      /*^locexp */
	      /*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	      melt_dbgcounter++;
#endif
	      ;
	    }
	    ;
	    /*^checksignal */
	    MELT_CHECK_SIGNAL ();
	    ;
    /*_#MELT_NEED_DBG__L19*/ meltfnum[18] =
	      /*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	      ( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	      0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	      ;;
	    MELT_LOCATION ("warmelt-modes.melt:4483:/ cond");
	    /*cond */ if ( /*_#MELT_NEED_DBG__L19*/ meltfnum[18])	/*then */
	      {
		/*^cond.then */
		/*^block */
		/*anyblock */
		{

      /*_#THE_MELTCALLCOUNT__L20*/ meltfnum[19] =
#ifdef meltcallcount
		    meltcallcount	/* the_meltcallcount */
#else
		    0L
#endif /* meltcallcount the_meltcallcount */
		    ;;
		  MELT_LOCATION ("warmelt-modes.melt:4483:/ checksignal");
		  MELT_CHECK_SIGNAL ();
		  ;
		  /*^apply */
		  /*apply */
		  {
		    union meltparam_un argtab[9];
		    memset (&argtab, 0, sizeof (argtab));
		    /*^apply.arg */
		    argtab[0].meltbp_long =
		      /*_#THE_MELTCALLCOUNT__L20*/ meltfnum[19];
		    /*^apply.arg */
		    argtab[1].meltbp_cstring = "warmelt-modes.melt";
		    /*^apply.arg */
		    argtab[2].meltbp_long = 4483;
		    /*^apply.arg */
		    argtab[3].meltbp_cstring =
		      "child_process_sigchld_handler curchph=";
		    /*^apply.arg */
		    argtab[4].meltbp_aptr =
		      (melt_ptr_t *) & /*_.CURCHPH__V29*/ meltfptr[28];
		    /*^apply.arg */
		    argtab[5].meltbp_cstring = " curwstat=";
		    /*^apply.arg */
		    argtab[6].meltbp_long = /*_#CURWSTAT__L13*/ meltfnum[10];
		    /*^apply.arg */
		    argtab[7].meltbp_cstring = " ix=";
		    /*^apply.arg */
		    argtab[8].meltbp_long = /*_#IX__L12*/ meltfnum[5];
		    /*_.MELT_DEBUG_FUN__V37*/ meltfptr[36] =
		      melt_apply ((meltclosure_ptr_t)
				  (( /*!MELT_DEBUG_FUN */ meltfrout->
				    tabval[0])),
				  (melt_ptr_t) (( /*nil */ NULL)),
				  (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_PTR MELTBPARSTR_CSTRING
				   MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_LONG ""), argtab, "",
				  (union meltparam_un *) 0);
		  }
		  ;
		  /*_.IF___V36*/ meltfptr[35] =
		    /*_.MELT_DEBUG_FUN__V37*/ meltfptr[36];;
		  /*epilog */

		  MELT_LOCATION ("warmelt-modes.melt:4483:/ clear");
		/*clear *//*_#THE_MELTCALLCOUNT__L20*/ meltfnum[19] = 0;
		  /*^clear */
		/*clear *//*_.MELT_DEBUG_FUN__V37*/ meltfptr[36] = 0;
		}
		;
	      }
	    else
	      {			/*^cond.else */

     /*_.IF___V36*/ meltfptr[35] = NULL;;
	      }
	    ;
	    MELT_LOCATION ("warmelt-modes.melt:4483:/ quasiblock");


	    /*_.PROGN___V38*/ meltfptr[36] = /*_.IF___V36*/ meltfptr[35];;
	    /*^compute */
	    /*_.IFCPP___V35*/ meltfptr[34] = /*_.PROGN___V38*/ meltfptr[36];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-modes.melt:4483:/ clear");
	      /*clear *//*_#MELT_NEED_DBG__L19*/ meltfnum[18] = 0;
	    /*^clear */
	      /*clear *//*_.IF___V36*/ meltfptr[35] = 0;
	    /*^clear */
	      /*clear *//*_.PROGN___V38*/ meltfptr[36] = 0;
	  }

#else /*MELT_HAVE_DEBUG */
	  /*^cppif.else */
	  /*_.IFCPP___V35*/ meltfptr[34] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
	  ;
	  /*citerblock BLOCK_SIGNALS */
	  {
	    /* block_signals meltcit4__BLKSIGNAL start */
	    long meltcit4__BLKSIGNAL_lev = melt_blocklevel_signals;
	    melt_blocklevel_signals = meltcit4__BLKSIGNAL_lev + 1;



	    MELT_LOCATION ("warmelt-modes.melt:4487:/ quasiblock");


	    /*^cond */
	    /*cond */ if (
			   /*ifisa */
			   melt_is_instance_of ((melt_ptr_t)
						(( /*!CHILDPROC_BUCKET_REFERENCE */ meltfrout->tabval[1])),
						(melt_ptr_t) (( /*!CLASS_REFERENCE */ meltfrout->tabval[3])))
	      )			/*then */
	      {
		/*^cond.then */
		/*^getslot */
		{
		  melt_ptr_t slot = NULL, obj = NULL;
		  obj =
		    (melt_ptr_t) (( /*!CHILDPROC_BUCKET_REFERENCE */
				   meltfrout->tabval[1])) /*=obj*/ ;
		  melt_object_get_field (slot, obj, 0, "REFERENCED_VALUE");
     /*_.CBUCK__V39*/ meltfptr[35] = slot;
		};
		;
	      }
	    else
	      {			/*^cond.else */

    /*_.CBUCK__V39*/ meltfptr[35] = NULL;;
	      }
	    ;
	    /*^compute */
   /*_.BUCKETLONG_REMOVE__V40*/ meltfptr[36] =
	      meltgc_longsbucket_remove ((melt_ptr_t) /*_.CBUCK__V39*/
					 meltfptr[35],
					 ( /*_#CURPID__L14*/ meltfnum[0]));;
	    MELT_LOCATION ("warmelt-modes.melt:4489:/ compute");
	    /*_.CBUCK__V39*/ meltfptr[35] = /*_.SETQ___V41*/ meltfptr[40] =
	      /*_.BUCKETLONG_REMOVE__V40*/ meltfptr[36];;
	    MELT_LOCATION ("warmelt-modes.melt:4490:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^cond */
	    /*cond */ if (
			   /*ifisa */
			   melt_is_instance_of ((melt_ptr_t)
						(( /*!CHILDPROC_BUCKET_REFERENCE */ meltfrout->tabval[1])),
						(melt_ptr_t) (( /*!CLASS_REFERENCE */ meltfrout->tabval[3])))
	      )			/*then */
	      {
		/*^cond.then */
		/*^block */
		/*anyblock */
		{

		  /*^putslot */
		  /*putslot */
		  melt_assertmsg ("putslot checkobj @REFERENCED_VALUE",
				  melt_magic_discr ((melt_ptr_t)
						    (( /*!CHILDPROC_BUCKET_REFERENCE */ meltfrout->tabval[1]))) == MELTOBMAG_OBJECT);
		  melt_putfield_object ((( /*!CHILDPROC_BUCKET_REFERENCE */
					  meltfrout->tabval[1])), (0),
					( /*_.CBUCK__V39*/ meltfptr[35]),
					"REFERENCED_VALUE");
		  ;
		  /*^touch */
		  meltgc_touch (( /*!CHILDPROC_BUCKET_REFERENCE */ meltfrout->
				 tabval[1]));
		  ;
		  /*^touchobj */

		  melt_dbgtrace_written_object (( /*!CHILDPROC_BUCKET_REFERENCE */ meltfrout->tabval[1]), "put-fields");
		  ;
		  /*epilog */
		}
		;
	      }			/*noelse */
	    ;

	    MELT_LOCATION ("warmelt-modes.melt:4487:/ clear");
	     /*clear *//*_.CBUCK__V39*/ meltfptr[35] = 0;
	    /*^clear */
	     /*clear *//*_.BUCKETLONG_REMOVE__V40*/ meltfptr[36] = 0;
	    /*^clear */
	     /*clear *//*_.SETQ___V41*/ meltfptr[40] = 0;
	    /* block_signals meltcit4__BLKSIGNAL end */
	    melt_blocklevel_signals = meltcit4__BLKSIGNAL_lev;
	    MELT_CHECK_SIGNAL ();


	    /*citerepilog */
	  }			/*endciterblock BLOCK_SIGNALS */
	  ;

	  {
	    MELT_LOCATION ("warmelt-modes.melt:4494:/ locexp");
	    /* child_process_sigchld_handler LOOKWSTATCHK__1 */
	    {
	      int wstat = (int) /*_#CURWSTAT__L13*/ meltfnum[10];
	      if (WIFEXITED (wstat))
		{
	    /*_#EXITED__L15*/ meltfnum[4] = 1;
	    /*_#EXITSTAT__L16*/ meltfnum[15] = WEXITSTATUS (wstat);
		}
	      else if (WIFSIGNALED (wstat))
		{
	    /*_#SIGNALED__L17*/ meltfnum[16] = 1;
	    /*_#TERMSIG__L18*/ meltfnum[17] = WTERMSIG (wstat);
	    /*_.TERMSIGNAME__V34*/ meltfptr[33] =
		    meltgc_new_string
		    ((meltobject_ptr_t) MELT_PREDEF (DISCR_STRING),
		     strsignal ( /*_#TERMSIG__L18*/ meltfnum[17]));
		}
	    } /* end child_process_sigchld_handler LOOKWSTATCHK__1 */ ;
	  }
	  ;
	  MELT_LOCATION ("warmelt-modes.melt:4510:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^cond */
	  /*cond */ if ( /*_#EXITED__L15*/ meltfnum[4])	/*then */
	    {
	      /*^cond.then */
	      /*^block */
	      /*anyblock */
	      {

		MELT_LOCATION ("warmelt-modes.melt:4512:/ checksignal");
		MELT_CHECK_SIGNAL ();
		;
    /*_#I__L21*/ meltfnum[19] =
		  (( /*_#EXITSTAT__L16*/ meltfnum[15]) == (0));;
		MELT_LOCATION ("warmelt-modes.melt:4512:/ cond");
		/*cond */ if ( /*_#I__L21*/ meltfnum[19])	/*then */
		  {
		    /*^cond.then */
		    /*^block */
		    /*anyblock */
		    {


#if MELT_HAVE_DEBUG
		      MELT_LOCATION ("warmelt-modes.melt:4514:/ cppif.then");
		      /*^block */
		      /*anyblock */
		      {


			{
			  /*^locexp */
			  /*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
			  melt_dbgcounter++;
#endif
			  ;
			}
			;
			/*^checksignal */
			MELT_CHECK_SIGNAL ();
			;
	/*_#MELT_NEED_DBG__L22*/ meltfnum[18] =
			  /*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
			  ( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
			  0	/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
			  ;;
			MELT_LOCATION ("warmelt-modes.melt:4514:/ cond");
			/*cond */ if ( /*_#MELT_NEED_DBG__L22*/ meltfnum[18])	/*then */
			  {
			    /*^cond.then */
			    /*^block */
			    /*anyblock */
			    {

	  /*_#THE_MELTCALLCOUNT__L23*/ meltfnum[22] =
#ifdef meltcallcount
				meltcallcount	/* the_meltcallcount */
#else
				0L
#endif /* meltcallcount the_meltcallcount */
				;;
			      MELT_LOCATION
				("warmelt-modes.melt:4514:/ checksignal");
			      MELT_CHECK_SIGNAL ();
			      ;
			      /*^apply */
			      /*apply */
			      {
				union meltparam_un argtab[5];
				memset (&argtab, 0, sizeof (argtab));
				/*^apply.arg */
				argtab[0].meltbp_long =
				  /*_#THE_MELTCALLCOUNT__L23*/ meltfnum[22];
				/*^apply.arg */
				argtab[1].meltbp_cstring =
				  "warmelt-modes.melt";
				/*^apply.arg */
				argtab[2].meltbp_long = 4514;
				/*^apply.arg */
				argtab[3].meltbp_cstring =
				  "child_process_sigchld_handler successful curchph=";
				/*^apply.arg */
				argtab[4].meltbp_aptr =
				  (melt_ptr_t *) & /*_.CURCHPH__V29*/
				  meltfptr[28];
				/*_.MELT_DEBUG_FUN__V46*/ meltfptr[45] =
				  melt_apply ((meltclosure_ptr_t)
					      (( /*!MELT_DEBUG_FUN */
						meltfrout->tabval[0])),
					      (melt_ptr_t) (( /*nil */ NULL)),
					      (MELTBPARSTR_LONG
					       MELTBPARSTR_CSTRING
					       MELTBPARSTR_LONG
					       MELTBPARSTR_CSTRING
					       MELTBPARSTR_PTR ""), argtab,
					      "", (union meltparam_un *) 0);
			      }
			      ;
			      /*_.IF___V45*/ meltfptr[44] =
				/*_.MELT_DEBUG_FUN__V46*/ meltfptr[45];;
			      /*epilog */

			      MELT_LOCATION
				("warmelt-modes.melt:4514:/ clear");
		    /*clear *//*_#THE_MELTCALLCOUNT__L23*/
				meltfnum[22] = 0;
			      /*^clear */
		    /*clear *//*_.MELT_DEBUG_FUN__V46*/ meltfptr[45]
				= 0;
			    }
			    ;
			  }
			else
			  {	/*^cond.else */

	 /*_.IF___V45*/ meltfptr[44] = NULL;;
			  }
			;
			MELT_LOCATION
			  ("warmelt-modes.melt:4514:/ quasiblock");


			/*_.PROGN___V47*/ meltfptr[45] =
			  /*_.IF___V45*/ meltfptr[44];;
			/*^compute */
			/*_.IFCPP___V44*/ meltfptr[40] =
			  /*_.PROGN___V47*/ meltfptr[45];;
			/*epilog */

			MELT_LOCATION ("warmelt-modes.melt:4514:/ clear");
		  /*clear *//*_#MELT_NEED_DBG__L22*/ meltfnum[18] = 0;
			/*^clear */
		  /*clear *//*_.IF___V45*/ meltfptr[44] = 0;
			/*^clear */
		  /*clear *//*_.PROGN___V47*/ meltfptr[45] = 0;
		      }

#else /*MELT_HAVE_DEBUG */
		      /*^cppif.else */
		      /*_.IFCPP___V44*/ meltfptr[40] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
		      ;
		      MELT_LOCATION ("warmelt-modes.melt:4516:/ checksignal");
		      MELT_CHECK_SIGNAL ();
		      ;
		      /*^apply */
		      /*apply */
		      {
			union meltparam_un argtab[1];
			memset (&argtab, 0, sizeof (argtab));
			/*^apply.arg */
			argtab[0].meltbp_aptr = /*nil */ (melt_ptr_t *) NULL;
			/*_.CURCLOS__V48*/ meltfptr[44] =
			  melt_apply ((meltclosure_ptr_t)
				      ( /*_.CURCLOS__V33*/ meltfptr[32]),
				      (melt_ptr_t) ( /*_.CURCHPH__V29*/
						    meltfptr[28]),
				      (MELTBPARSTR_PTR ""), argtab, "",
				      (union meltparam_un *) 0);
		      }
		      ;
		      MELT_LOCATION ("warmelt-modes.melt:4512:/ quasiblock");


		      /*_.PROGN___V49*/ meltfptr[45] =
			/*_.CURCLOS__V48*/ meltfptr[44];;
		      /*^compute */
		      /*_.IFELSE___V43*/ meltfptr[36] =
			/*_.PROGN___V49*/ meltfptr[45];;
		      /*epilog */

		      MELT_LOCATION ("warmelt-modes.melt:4512:/ clear");
		/*clear *//*_.IFCPP___V44*/ meltfptr[40] = 0;
		      /*^clear */
		/*clear *//*_.CURCLOS__V48*/ meltfptr[44] = 0;
		      /*^clear */
		/*clear *//*_.PROGN___V49*/ meltfptr[45] = 0;
		    }
		    ;
		  }
		else
		  {		/*^cond.else */

		    /*^block */
		    /*anyblock */
		    {


#if MELT_HAVE_DEBUG
		      MELT_LOCATION ("warmelt-modes.melt:4520:/ cppif.then");
		      /*^block */
		      /*anyblock */
		      {


			{
			  /*^locexp */
			  /*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
			  melt_dbgcounter++;
#endif
			  ;
			}
			;
			/*^checksignal */
			MELT_CHECK_SIGNAL ();
			;
	/*_#MELT_NEED_DBG__L24*/ meltfnum[22] =
			  /*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
			  ( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
			  0	/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
			  ;;
			MELT_LOCATION ("warmelt-modes.melt:4520:/ cond");
			/*cond */ if ( /*_#MELT_NEED_DBG__L24*/ meltfnum[22])	/*then */
			  {
			    /*^cond.then */
			    /*^block */
			    /*anyblock */
			    {

	  /*_#THE_MELTCALLCOUNT__L25*/ meltfnum[18] =
#ifdef meltcallcount
				meltcallcount	/* the_meltcallcount */
#else
				0L
#endif /* meltcallcount the_meltcallcount */
				;;
			      MELT_LOCATION
				("warmelt-modes.melt:4520:/ checksignal");
			      MELT_CHECK_SIGNAL ();
			      ;
			      /*^apply */
			      /*apply */
			      {
				union meltparam_un argtab[7];
				memset (&argtab, 0, sizeof (argtab));
				/*^apply.arg */
				argtab[0].meltbp_long =
				  /*_#THE_MELTCALLCOUNT__L25*/ meltfnum[18];
				/*^apply.arg */
				argtab[1].meltbp_cstring =
				  "warmelt-modes.melt";
				/*^apply.arg */
				argtab[2].meltbp_long = 4520;
				/*^apply.arg */
				argtab[3].meltbp_cstring =
				  "child_process_sigchld_handler failed curchph=";
				/*^apply.arg */
				argtab[4].meltbp_aptr =
				  (melt_ptr_t *) & /*_.CURCHPH__V29*/
				  meltfptr[28];
				/*^apply.arg */
				argtab[5].meltbp_cstring = " exitstat=";
				/*^apply.arg */
				argtab[6].meltbp_long =
				  /*_#EXITSTAT__L16*/ meltfnum[15];
				/*_.MELT_DEBUG_FUN__V52*/ meltfptr[45] =
				  melt_apply ((meltclosure_ptr_t)
					      (( /*!MELT_DEBUG_FUN */
						meltfrout->tabval[0])),
					      (melt_ptr_t) (( /*nil */ NULL)),
					      (MELTBPARSTR_LONG
					       MELTBPARSTR_CSTRING
					       MELTBPARSTR_LONG
					       MELTBPARSTR_CSTRING
					       MELTBPARSTR_PTR
					       MELTBPARSTR_CSTRING
					       MELTBPARSTR_LONG ""), argtab,
					      "", (union meltparam_un *) 0);
			      }
			      ;
			      /*_.IF___V51*/ meltfptr[44] =
				/*_.MELT_DEBUG_FUN__V52*/ meltfptr[45];;
			      /*epilog */

			      MELT_LOCATION
				("warmelt-modes.melt:4520:/ clear");
		    /*clear *//*_#THE_MELTCALLCOUNT__L25*/
				meltfnum[18] = 0;
			      /*^clear */
		    /*clear *//*_.MELT_DEBUG_FUN__V52*/ meltfptr[45]
				= 0;
			    }
			    ;
			  }
			else
			  {	/*^cond.else */

	 /*_.IF___V51*/ meltfptr[44] = NULL;;
			  }
			;
			MELT_LOCATION
			  ("warmelt-modes.melt:4520:/ quasiblock");


			/*_.PROGN___V53*/ meltfptr[45] =
			  /*_.IF___V51*/ meltfptr[44];;
			/*^compute */
			/*_.IFCPP___V50*/ meltfptr[40] =
			  /*_.PROGN___V53*/ meltfptr[45];;
			/*epilog */

			MELT_LOCATION ("warmelt-modes.melt:4520:/ clear");
		  /*clear *//*_#MELT_NEED_DBG__L24*/ meltfnum[22] = 0;
			/*^clear */
		  /*clear *//*_.IF___V51*/ meltfptr[44] = 0;
			/*^clear */
		  /*clear *//*_.PROGN___V53*/ meltfptr[45] = 0;
		      }

#else /*MELT_HAVE_DEBUG */
		      /*^cppif.else */
		      /*_.IFCPP___V50*/ meltfptr[40] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
		      ;
		      /*^compute */
      /*_.MAKE_INTEGERBOX__V54*/ meltfptr[44] =
			(meltgc_new_int
			 ((meltobject_ptr_t)
			  (( /*!DISCR_CONSTANT_INTEGER */ meltfrout->
			    tabval[5])),
			  ( /*_#EXITSTAT__L16*/ meltfnum[15])));;
		      MELT_LOCATION ("warmelt-modes.melt:4522:/ checksignal");
		      MELT_CHECK_SIGNAL ();
		      ;
		      /*^apply */
		      /*apply */
		      {
			union meltparam_un argtab[1];
			memset (&argtab, 0, sizeof (argtab));
			/*^apply.arg */
			argtab[0].meltbp_aptr =
			  (melt_ptr_t *) & /*_.MAKE_INTEGERBOX__V54*/
			  meltfptr[44];
			/*_.CURCLOS__V55*/ meltfptr[45] =
			  melt_apply ((meltclosure_ptr_t)
				      ( /*_.CURCLOS__V33*/ meltfptr[32]),
				      (melt_ptr_t) ( /*_.CURCHPH__V29*/
						    meltfptr[28]),
				      (MELTBPARSTR_PTR ""), argtab, "",
				      (union meltparam_un *) 0);
		      }
		      ;
		      MELT_LOCATION ("warmelt-modes.melt:4518:/ quasiblock");


		      /*_.PROGN___V56*/ meltfptr[55] =
			/*_.CURCLOS__V55*/ meltfptr[45];;
		      /*^compute */
		      /*_.IFELSE___V43*/ meltfptr[36] =
			/*_.PROGN___V56*/ meltfptr[55];;
		      /*epilog */

		      MELT_LOCATION ("warmelt-modes.melt:4512:/ clear");
		/*clear *//*_.IFCPP___V50*/ meltfptr[40] = 0;
		      /*^clear */
		/*clear *//*_.MAKE_INTEGERBOX__V54*/ meltfptr[44] = 0;
		      /*^clear */
		/*clear *//*_.CURCLOS__V55*/ meltfptr[45] = 0;
		      /*^clear */
		/*clear *//*_.PROGN___V56*/ meltfptr[55] = 0;
		    }
		    ;
		  }
		;
		/*_.IFELSE___V42*/ meltfptr[35] =
		  /*_.IFELSE___V43*/ meltfptr[36];;
		/*epilog */

		MELT_LOCATION ("warmelt-modes.melt:4510:/ clear");
	      /*clear *//*_#I__L21*/ meltfnum[19] = 0;
		/*^clear */
	      /*clear *//*_.IFELSE___V43*/ meltfptr[36] = 0;
	      }
	      ;
	    }
	  else
	    {			/*^cond.else */

	      /*^block */
	      /*anyblock */
	      {

		MELT_LOCATION ("warmelt-modes.melt:4526:/ checksignal");
		MELT_CHECK_SIGNAL ();
		;
		/*^cond */
		/*cond */ if ( /*_#SIGNALED__L17*/ meltfnum[16])	/*then */
		  {
		    /*^cond.then */
		    /*^block */
		    /*anyblock */
		    {


#if MELT_HAVE_DEBUG
		      MELT_LOCATION ("warmelt-modes.melt:4527:/ cppif.then");
		      /*^block */
		      /*anyblock */
		      {


			{
			  /*^locexp */
			  /*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
			  melt_dbgcounter++;
#endif
			  ;
			}
			;
			/*^checksignal */
			MELT_CHECK_SIGNAL ();
			;
	/*_#MELT_NEED_DBG__L26*/ meltfnum[18] =
			  /*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
			  ( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
			  0	/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
			  ;;
			MELT_LOCATION ("warmelt-modes.melt:4527:/ cond");
			/*cond */ if ( /*_#MELT_NEED_DBG__L26*/ meltfnum[18])	/*then */
			  {
			    /*^cond.then */
			    /*^block */
			    /*anyblock */
			    {

	  /*_#THE_MELTCALLCOUNT__L27*/ meltfnum[22] =
#ifdef meltcallcount
				meltcallcount	/* the_meltcallcount */
#else
				0L
#endif /* meltcallcount the_meltcallcount */
				;;
			      MELT_LOCATION
				("warmelt-modes.melt:4527:/ checksignal");
			      MELT_CHECK_SIGNAL ();
			      ;
			      /*^apply */
			      /*apply */
			      {
				union meltparam_un argtab[7];
				memset (&argtab, 0, sizeof (argtab));
				/*^apply.arg */
				argtab[0].meltbp_long =
				  /*_#THE_MELTCALLCOUNT__L27*/ meltfnum[22];
				/*^apply.arg */
				argtab[1].meltbp_cstring =
				  "warmelt-modes.melt";
				/*^apply.arg */
				argtab[2].meltbp_long = 4527;
				/*^apply.arg */
				argtab[3].meltbp_cstring =
				  "child_process_sigchld_handler signaled curchph=";
				/*^apply.arg */
				argtab[4].meltbp_aptr =
				  (melt_ptr_t *) & /*_.CURCHPH__V29*/
				  meltfptr[28];
				/*^apply.arg */
				argtab[5].meltbp_cstring = " termsigname=";
				/*^apply.arg */
				argtab[6].meltbp_aptr =
				  (melt_ptr_t *) & /*_.TERMSIGNAME__V34*/
				  meltfptr[33];
				/*_.MELT_DEBUG_FUN__V60*/ meltfptr[55] =
				  melt_apply ((meltclosure_ptr_t)
					      (( /*!MELT_DEBUG_FUN */
						meltfrout->tabval[0])),
					      (melt_ptr_t) (( /*nil */ NULL)),
					      (MELTBPARSTR_LONG
					       MELTBPARSTR_CSTRING
					       MELTBPARSTR_LONG
					       MELTBPARSTR_CSTRING
					       MELTBPARSTR_PTR
					       MELTBPARSTR_CSTRING
					       MELTBPARSTR_PTR ""), argtab,
					      "", (union meltparam_un *) 0);
			      }
			      ;
			      /*_.IF___V59*/ meltfptr[45] =
				/*_.MELT_DEBUG_FUN__V60*/ meltfptr[55];;
			      /*epilog */

			      MELT_LOCATION
				("warmelt-modes.melt:4527:/ clear");
		    /*clear *//*_#THE_MELTCALLCOUNT__L27*/
				meltfnum[22] = 0;
			      /*^clear */
		    /*clear *//*_.MELT_DEBUG_FUN__V60*/ meltfptr[55]
				= 0;
			    }
			    ;
			  }
			else
			  {	/*^cond.else */

	 /*_.IF___V59*/ meltfptr[45] = NULL;;
			  }
			;
			MELT_LOCATION
			  ("warmelt-modes.melt:4527:/ quasiblock");


			/*_.PROGN___V61*/ meltfptr[36] =
			  /*_.IF___V59*/ meltfptr[45];;
			/*^compute */
			/*_.IFCPP___V58*/ meltfptr[44] =
			  /*_.PROGN___V61*/ meltfptr[36];;
			/*epilog */

			MELT_LOCATION ("warmelt-modes.melt:4527:/ clear");
		  /*clear *//*_#MELT_NEED_DBG__L26*/ meltfnum[18] = 0;
			/*^clear */
		  /*clear *//*_.IF___V59*/ meltfptr[45] = 0;
			/*^clear */
		  /*clear *//*_.PROGN___V61*/ meltfptr[36] = 0;
		      }

#else /*MELT_HAVE_DEBUG */
		      /*^cppif.else */
		      /*_.IFCPP___V58*/ meltfptr[44] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
		      ;
		      MELT_LOCATION ("warmelt-modes.melt:4529:/ checksignal");
		      MELT_CHECK_SIGNAL ();
		      ;
		      /*^apply */
		      /*apply */
		      {
			union meltparam_un argtab[1];
			memset (&argtab, 0, sizeof (argtab));
			/*^apply.arg */
			argtab[0].meltbp_aptr =
			  (melt_ptr_t *) & /*_.TERMSIGNAME__V34*/
			  meltfptr[33];
			/*_.CURCLOS__V62*/ meltfptr[55] =
			  melt_apply ((meltclosure_ptr_t)
				      ( /*_.CURCLOS__V33*/ meltfptr[32]),
				      (melt_ptr_t) ( /*_.CURCHPH__V29*/
						    meltfptr[28]),
				      (MELTBPARSTR_PTR ""), argtab, "",
				      (union meltparam_un *) 0);
		      }
		      ;
		      MELT_LOCATION ("warmelt-modes.melt:4526:/ quasiblock");


		      /*_.PROGN___V63*/ meltfptr[45] =
			/*_.CURCLOS__V62*/ meltfptr[55];;
		      /*^compute */
		      /*_.IFELSE___V57*/ meltfptr[40] =
			/*_.PROGN___V63*/ meltfptr[45];;
		      /*epilog */

		      MELT_LOCATION ("warmelt-modes.melt:4526:/ clear");
		/*clear *//*_.IFCPP___V58*/ meltfptr[44] = 0;
		      /*^clear */
		/*clear *//*_.CURCLOS__V62*/ meltfptr[55] = 0;
		      /*^clear */
		/*clear *//*_.PROGN___V63*/ meltfptr[45] = 0;
		    }
		    ;
		  }
		else
		  {		/*^cond.else */

     /*_.IFELSE___V57*/ meltfptr[40] = NULL;;
		  }
		;
		/*^compute */
		/*_.IFELSE___V42*/ meltfptr[35] =
		  /*_.IFELSE___V57*/ meltfptr[40];;
		/*epilog */

		MELT_LOCATION ("warmelt-modes.melt:4510:/ clear");
	      /*clear *//*_.IFELSE___V57*/ meltfptr[40] = 0;
	      }
	      ;
	    }
	  ;
	  /*_.LET___V30*/ meltfptr[29] = /*_.IFELSE___V42*/ meltfptr[35];;

	  MELT_LOCATION ("warmelt-modes.melt:4474:/ clear");
	    /*clear *//*_.MULTIPLE_NTH__V31*/ meltfptr[30] = 0;
	  /*^clear */
	    /*clear *//*_#CURWSTAT__L13*/ meltfnum[10] = 0;
	  /*^clear */
	    /*clear *//*_.CHILPROH_PID__V32*/ meltfptr[31] = 0;
	  /*^clear */
	    /*clear *//*_#CURPID__L14*/ meltfnum[0] = 0;
	  /*^clear */
	    /*clear *//*_.CURCLOS__V33*/ meltfptr[32] = 0;
	  /*^clear */
	    /*clear *//*_#EXITED__L15*/ meltfnum[4] = 0;
	  /*^clear */
	    /*clear *//*_#EXITSTAT__L16*/ meltfnum[15] = 0;
	  /*^clear */
	    /*clear *//*_#SIGNALED__L17*/ meltfnum[16] = 0;
	  /*^clear */
	    /*clear *//*_#TERMSIG__L18*/ meltfnum[17] = 0;
	  /*^clear */
	    /*clear *//*_.TERMSIGNAME__V34*/ meltfptr[33] = 0;
	  /*^clear */
	    /*clear *//*_.IFCPP___V35*/ meltfptr[34] = 0;
	  /*^clear */
	    /*clear *//*_.IFELSE___V42*/ meltfptr[35] = 0;
	  if ( /*_#IX__L12*/ meltfnum[5] < 0)
	    break;
	}			/* end  foreach_in_multiple meltcit3__EACHTUP */

      /*citerepilog */

      MELT_LOCATION ("warmelt-modes.melt:4471:/ clear");
	    /*clear *//*_.CURCHPH__V29*/ meltfptr[28] = 0;
      /*^clear */
	    /*clear *//*_#IX__L12*/ meltfnum[5] = 0;
      /*^clear */
	    /*clear *//*_.LET___V30*/ meltfptr[29] = 0;
    }				/*endciterblock FOREACH_IN_MULTIPLE */
    ;

    MELT_LOCATION ("warmelt-modes.melt:4468:/ clear");
	   /*clear *//*_.TUPCHPH__V27*/ meltfptr[23] = 0;
    /*^clear */
	   /*clear *//*_.TUPWSTAT__V28*/ meltfptr[9] = 0;

    MELT_LOCATION ("warmelt-modes.melt:4426:/ clear");
	   /*clear *//*_.LISCHPH__V6*/ meltfptr[2] = 0;
    /*^clear */
	   /*clear *//*_.LISWSTAT__V7*/ meltfptr[3] = 0;
    /*^clear */
	   /*clear *//*_.BUCK__V8*/ meltfptr[7] = 0;
    /*^clear */
	   /*clear *//*_#IS_BUCKETLONG__L3*/ meltfnum[1] = 0;
    /*^clear */
	   /*clear *//*_.IFELSE___V9*/ meltfptr[8] = 0;
    MELT_LOCATION ("warmelt-modes.melt:4424:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V2*/ meltfptr[1] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("CHILD_PROCESS_SIGCHLD_HANDLER", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_78_warmelt_modes_CHILD_PROCESS_SIGCHLD_HANDLER_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_78_warmelt_modes_CHILD_PROCESS_SIGCHLD_HANDLER */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_79_warmelt_modes_REGISTER_CHILD_PROCESS_HANDLER (meltclosure_ptr_t
							  meltclosp_,
							  melt_ptr_t
							  meltfirstargp_,
							  const
							  melt_argdescr_cell_t
							  meltxargdescr_[],
							  union meltparam_un *
							  meltxargtab_,
							  const
							  melt_argdescr_cell_t
							  meltxresdescr_[],
							  union meltparam_un *
							  meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_79_warmelt_modes_REGISTER_CHILD_PROCESS_HANDLER_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_79_warmelt_modes_REGISTER_CHILD_PROCESS_HANDLER_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 32
    melt_ptr_t mcfr_varptr[32];
#define MELTFRAM_NBVARNUM 11
    long mcfr_varnum[11];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_79_warmelt_modes_REGISTER_CHILD_PROCESS_HANDLER is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct
	 frame_meltrout_79_warmelt_modes_REGISTER_CHILD_PROCESS_HANDLER_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 32; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_79_warmelt_modes_REGISTER_CHILD_PROCESS_HANDLER nbval 32*/
  meltfram__.mcfr_nbvar = 32 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("REGISTER_CHILD_PROCESS_HANDLER", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-modes.melt:4532:/ getarg");
 /*_.CLOS__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_LONG)
    goto lab_endgetargs;
 /*_#PID__L1*/ meltfnum[0] = meltxargtab_[0].meltbp_long;

  /*getarg#2 */
  /*^getarg */
  if (meltxargdescr_[1] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.DATA__V3*/ meltfptr[2] =
    (meltxargtab_[1].meltbp_aptr) ? (*(meltxargtab_[1].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.DATA__V3*/ meltfptr[2])) !=
	      NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-modes.melt:4536:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L2*/ meltfnum[1] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-modes.melt:4536:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L2*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L3*/ meltfnum[2] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-modes.melt:4536:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[9];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L3*/ meltfnum[2];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-modes.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 4536;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring =
		"register_child_process_handler clos=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.CLOS__V2*/ meltfptr[1];
	      /*^apply.arg */
	      argtab[5].meltbp_cstring = " pid=";
	      /*^apply.arg */
	      argtab[6].meltbp_long = /*_#PID__L1*/ meltfnum[0];
	      /*^apply.arg */
	      argtab[7].meltbp_cstring = " data=";
	      /*^apply.arg */
	      argtab[8].meltbp_aptr =
		(melt_ptr_t *) & /*_.DATA__V3*/ meltfptr[2];
	      /*_.MELT_DEBUG_FUN__V6*/ meltfptr[5] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V5*/ meltfptr[4] = /*_.MELT_DEBUG_FUN__V6*/ meltfptr[5];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-modes.melt:4536:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L3*/ meltfnum[2] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V6*/ meltfptr[5] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V5*/ meltfptr[4] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-modes.melt:4536:/ quasiblock");


      /*_.PROGN___V7*/ meltfptr[5] = /*_.IF___V5*/ meltfptr[4];;
      /*^compute */
      /*_.IFCPP___V4*/ meltfptr[3] = /*_.PROGN___V7*/ meltfptr[5];;
      /*epilog */

      MELT_LOCATION ("warmelt-modes.melt:4536:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L2*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IF___V5*/ meltfptr[4] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V7*/ meltfptr[5] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V4*/ meltfptr[3] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-modes.melt:4537:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#IS_CLOSURE__L4*/ meltfnum[2] =
      (melt_magic_discr ((melt_ptr_t) ( /*_.CLOS__V2*/ meltfptr[1])) ==
       MELTOBMAG_CLOSURE);;
    MELT_LOCATION ("warmelt-modes.melt:4537:/ cond");
    /*cond */ if ( /*_#IS_CLOSURE__L4*/ meltfnum[2])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-modes.melt:4538:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
   /*_#I__L5*/ meltfnum[1] =
	    (( /*_#PID__L1*/ meltfnum[0]) > (0));;
	  MELT_LOCATION ("warmelt-modes.melt:4538:/ cond");
	  /*cond */ if ( /*_#I__L5*/ meltfnum[1])	/*then */
	    {
	      /*^cond.then */
	      /*^block */
	      /*anyblock */
	      {

		MELT_LOCATION ("warmelt-modes.melt:4539:/ quasiblock");


     /*_#PIDISBAD__L6*/ meltfnum[5] = 0;;

		{
		  MELT_LOCATION ("warmelt-modes.melt:4543:/ locexp");
		  /* register_child_process_handler TRYKILLCHK__1 */
		  if (kill ((pid_t) /*_#PID__L1*/ meltfnum[0], 0))
	     /*_#PIDISBAD__L6*/ meltfnum[5] = -1L;
		  ;
		}
		;

#if MELT_HAVE_DEBUG
		MELT_LOCATION ("warmelt-modes.melt:4547:/ cppif.then");
		/*^block */
		/*anyblock */
		{


		  {
		    /*^locexp */
		    /*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
		    melt_dbgcounter++;
#endif
		    ;
		  }
		  ;
		  /*^checksignal */
		  MELT_CHECK_SIGNAL ();
		  ;
       /*_#MELT_NEED_DBG__L7*/ meltfnum[6] =
		    /*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
		    ( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
		    0		/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
		    ;;
		  MELT_LOCATION ("warmelt-modes.melt:4547:/ cond");
		  /*cond */ if ( /*_#MELT_NEED_DBG__L7*/ meltfnum[6])	/*then */
		    {
		      /*^cond.then */
		      /*^block */
		      /*anyblock */
		      {

	 /*_#THE_MELTCALLCOUNT__L8*/ meltfnum[7] =
#ifdef meltcallcount
			  meltcallcount	/* the_meltcallcount */
#else
			  0L
#endif /* meltcallcount the_meltcallcount */
			  ;;
			MELT_LOCATION
			  ("warmelt-modes.melt:4547:/ checksignal");
			MELT_CHECK_SIGNAL ();
			;
			/*^apply */
			/*apply */
			{
			  union meltparam_un argtab[5];
			  memset (&argtab, 0, sizeof (argtab));
			  /*^apply.arg */
			  argtab[0].meltbp_long =
			    /*_#THE_MELTCALLCOUNT__L8*/ meltfnum[7];
			  /*^apply.arg */
			  argtab[1].meltbp_cstring = "warmelt-modes.melt";
			  /*^apply.arg */
			  argtab[2].meltbp_long = 4547;
			  /*^apply.arg */
			  argtab[3].meltbp_cstring =
			    "register_child_process_handler pidisbad=";
			  /*^apply.arg */
			  argtab[4].meltbp_long =
			    /*_#PIDISBAD__L6*/ meltfnum[5];
			  /*_.MELT_DEBUG_FUN__V13*/ meltfptr[12] =
			    melt_apply ((meltclosure_ptr_t)
					(( /*!MELT_DEBUG_FUN */ meltfrout->
					  tabval[0])),
					(melt_ptr_t) (( /*nil */ NULL)),
					(MELTBPARSTR_LONG MELTBPARSTR_CSTRING
					 MELTBPARSTR_LONG MELTBPARSTR_CSTRING
					 MELTBPARSTR_LONG ""), argtab, "",
					(union meltparam_un *) 0);
			}
			;
			/*_.IF___V12*/ meltfptr[11] =
			  /*_.MELT_DEBUG_FUN__V13*/ meltfptr[12];;
			/*epilog */

			MELT_LOCATION ("warmelt-modes.melt:4547:/ clear");
		   /*clear *//*_#THE_MELTCALLCOUNT__L8*/ meltfnum[7] = 0;
			/*^clear */
		   /*clear *//*_.MELT_DEBUG_FUN__V13*/ meltfptr[12] = 0;
		      }
		      ;
		    }
		  else
		    {		/*^cond.else */

	/*_.IF___V12*/ meltfptr[11] = NULL;;
		    }
		  ;
		  MELT_LOCATION ("warmelt-modes.melt:4547:/ quasiblock");


		  /*_.PROGN___V14*/ meltfptr[12] =
		    /*_.IF___V12*/ meltfptr[11];;
		  /*^compute */
		  /*_.IFCPP___V11*/ meltfptr[10] =
		    /*_.PROGN___V14*/ meltfptr[12];;
		  /*epilog */

		  MELT_LOCATION ("warmelt-modes.melt:4547:/ clear");
		 /*clear *//*_#MELT_NEED_DBG__L7*/ meltfnum[6] = 0;
		  /*^clear */
		 /*clear *//*_.IF___V12*/ meltfptr[11] = 0;
		  /*^clear */
		 /*clear *//*_.PROGN___V14*/ meltfptr[12] = 0;
		}

#else /*MELT_HAVE_DEBUG */
		/*^cppif.else */
		/*_.IFCPP___V11*/ meltfptr[10] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
		;
		MELT_LOCATION ("warmelt-modes.melt:4548:/ checksignal");
		MELT_CHECK_SIGNAL ();
		;
		/*^cond */
		/*cond */ if ( /*_#PIDISBAD__L6*/ meltfnum[5])	/*then */
		  {
		    /*^cond.then */
		    /*^block */
		    /*anyblock */
		    {

		      /*^quasiblock */


       /*_.RETVAL___V1*/ meltfptr[0] = NULL;;

		      {
			MELT_LOCATION ("warmelt-modes.melt:4548:/ locexp");
			/*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
			if (meltxresdescr_ && meltxresdescr_[0]
			    && meltxrestab_)
			  melt_warn_for_no_expected_secondary_results ();
			/* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
			;
		      }
		      ;
		      /*^finalreturn */
		      ;
		      /*finalret */ goto labend_rout;
		      /*_.IF___V15*/ meltfptr[11] =
			/*_.RETURN___V16*/ meltfptr[12];;
		      /*epilog */

		      MELT_LOCATION ("warmelt-modes.melt:4548:/ clear");
		 /*clear *//*_.RETURN___V16*/ meltfptr[12] = 0;
		    }
		    ;
		  }
		else
		  {		/*^cond.else */

      /*_.IF___V15*/ meltfptr[11] = NULL;;
		  }
		;
		MELT_LOCATION ("warmelt-modes.melt:4549:/ quasiblock");


		/*^checksignal */
		MELT_CHECK_SIGNAL ();
		;
     /*_.MAKE_INTEGERBOX__V18*/ meltfptr[17] =
		  (meltgc_new_int
		   ((meltobject_ptr_t)
		    (( /*!DISCR_CONSTANT_INTEGER */ meltfrout->tabval[2])),
		    ( /*_#PID__L1*/ meltfnum[0])));;
		MELT_LOCATION ("warmelt-modes.melt:4549:/ quasiblock");


		/*^rawallocobj */
		/*rawallocobj */
		{
		  melt_ptr_t newobj = 0;
		  melt_raw_object_create (newobj,
					  (melt_ptr_t) (( /*!CLASS_CHILD_PROCESS_HANDLER */ meltfrout->tabval[1])), (4), "CLASS_CHILD_PROCESS_HANDLER");
      /*_.INST__V20*/ meltfptr[19] =
		    newobj;
		};
		;
		/*^putslot */
		/*putslot */
		melt_assertmsg ("putslot checkobj @CHILPROH_PID",
				melt_magic_discr ((melt_ptr_t)
						  ( /*_.INST__V20*/
						   meltfptr[19])) ==
				MELTOBMAG_OBJECT);
		melt_putfield_object (( /*_.INST__V20*/ meltfptr[19]), (1),
				      ( /*_.MAKE_INTEGERBOX__V18*/
				       meltfptr[17]), "CHILPROH_PID");
		;
		/*^putslot */
		/*putslot */
		melt_assertmsg ("putslot checkobj @CHILPROH_CLOS",
				melt_magic_discr ((melt_ptr_t)
						  ( /*_.INST__V20*/
						   meltfptr[19])) ==
				MELTOBMAG_OBJECT);
		melt_putfield_object (( /*_.INST__V20*/ meltfptr[19]), (2),
				      ( /*_.CLOS__V2*/ meltfptr[1]),
				      "CHILPROH_CLOS");
		;
		/*^putslot */
		/*putslot */
		melt_assertmsg ("putslot checkobj @CHILPROH_DATA",
				melt_magic_discr ((melt_ptr_t)
						  ( /*_.INST__V20*/
						   meltfptr[19])) ==
				MELTOBMAG_OBJECT);
		melt_putfield_object (( /*_.INST__V20*/ meltfptr[19]), (3),
				      ( /*_.DATA__V3*/ meltfptr[2]),
				      "CHILPROH_DATA");
		;
		/*^touchobj */

		melt_dbgtrace_written_object ( /*_.INST__V20*/ meltfptr[19],
					      "newly made instance");
		;
		/*_.CHPH__V19*/ meltfptr[18] = /*_.INST__V20*/ meltfptr[19];;

#if MELT_HAVE_DEBUG
		MELT_LOCATION ("warmelt-modes.melt:4555:/ cppif.then");
		/*^block */
		/*anyblock */
		{


		  {
		    /*^locexp */
		    /*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
		    melt_dbgcounter++;
#endif
		    ;
		  }
		  ;
		  /*^checksignal */
		  MELT_CHECK_SIGNAL ();
		  ;
       /*_#MELT_NEED_DBG__L9*/ meltfnum[7] =
		    /*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
		    ( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
		    0		/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
		    ;;
		  MELT_LOCATION ("warmelt-modes.melt:4555:/ cond");
		  /*cond */ if ( /*_#MELT_NEED_DBG__L9*/ meltfnum[7])	/*then */
		    {
		      /*^cond.then */
		      /*^block */
		      /*anyblock */
		      {

	 /*_#THE_MELTCALLCOUNT__L10*/ meltfnum[6] =
#ifdef meltcallcount
			  meltcallcount	/* the_meltcallcount */
#else
			  0L
#endif /* meltcallcount the_meltcallcount */
			  ;;
			MELT_LOCATION
			  ("warmelt-modes.melt:4555:/ checksignal");
			MELT_CHECK_SIGNAL ();
			;
			/*^apply */
			/*apply */
			{
			  union meltparam_un argtab[5];
			  memset (&argtab, 0, sizeof (argtab));
			  /*^apply.arg */
			  argtab[0].meltbp_long =
			    /*_#THE_MELTCALLCOUNT__L10*/ meltfnum[6];
			  /*^apply.arg */
			  argtab[1].meltbp_cstring = "warmelt-modes.melt";
			  /*^apply.arg */
			  argtab[2].meltbp_long = 4555;
			  /*^apply.arg */
			  argtab[3].meltbp_cstring =
			    "register_child_process_handler chph=";
			  /*^apply.arg */
			  argtab[4].meltbp_aptr =
			    (melt_ptr_t *) & /*_.CHPH__V19*/ meltfptr[18];
			  /*_.MELT_DEBUG_FUN__V23*/ meltfptr[22] =
			    melt_apply ((meltclosure_ptr_t)
					(( /*!MELT_DEBUG_FUN */ meltfrout->
					  tabval[0])),
					(melt_ptr_t) (( /*nil */ NULL)),
					(MELTBPARSTR_LONG MELTBPARSTR_CSTRING
					 MELTBPARSTR_LONG MELTBPARSTR_CSTRING
					 MELTBPARSTR_PTR ""), argtab, "",
					(union meltparam_un *) 0);
			}
			;
			/*_.IF___V22*/ meltfptr[21] =
			  /*_.MELT_DEBUG_FUN__V23*/ meltfptr[22];;
			/*epilog */

			MELT_LOCATION ("warmelt-modes.melt:4555:/ clear");
		   /*clear *//*_#THE_MELTCALLCOUNT__L10*/ meltfnum[6] =
			  0;
			/*^clear */
		   /*clear *//*_.MELT_DEBUG_FUN__V23*/ meltfptr[22] = 0;
		      }
		      ;
		    }
		  else
		    {		/*^cond.else */

	/*_.IF___V22*/ meltfptr[21] = NULL;;
		    }
		  ;
		  MELT_LOCATION ("warmelt-modes.melt:4555:/ quasiblock");


		  /*_.PROGN___V24*/ meltfptr[22] =
		    /*_.IF___V22*/ meltfptr[21];;
		  /*^compute */
		  /*_.IFCPP___V21*/ meltfptr[20] =
		    /*_.PROGN___V24*/ meltfptr[22];;
		  /*epilog */

		  MELT_LOCATION ("warmelt-modes.melt:4555:/ clear");
		 /*clear *//*_#MELT_NEED_DBG__L9*/ meltfnum[7] = 0;
		  /*^clear */
		 /*clear *//*_.IF___V22*/ meltfptr[21] = 0;
		  /*^clear */
		 /*clear *//*_.PROGN___V24*/ meltfptr[22] = 0;
		}

#else /*MELT_HAVE_DEBUG */
		/*^cppif.else */
		/*_.IFCPP___V21*/ meltfptr[20] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
		;
		/*citerblock BLOCK_SIGNALS */
		{
		  /* block_signals meltcit1__BLKSIGNAL start */
		  long meltcit1__BLKSIGNAL_lev = melt_blocklevel_signals;
		  melt_blocklevel_signals = meltcit1__BLKSIGNAL_lev + 1;



		  MELT_LOCATION ("warmelt-modes.melt:4558:/ quasiblock");


		  /*^cond */
		  /*cond */ if (
				 /*ifisa */
				 melt_is_instance_of ((melt_ptr_t)
						      (( /*!CHILDPROC_BUCKET_REFERENCE */ meltfrout->tabval[4])),
						      (melt_ptr_t) (( /*!CLASS_REFERENCE */ meltfrout->tabval[5])))
		    )		/*then */
		    {
		      /*^cond.then */
		      /*^getslot */
		      {
			melt_ptr_t slot = NULL, obj = NULL;
			obj =
			  (melt_ptr_t) (( /*!CHILDPROC_BUCKET_REFERENCE */
					 meltfrout->tabval[4])) /*=obj*/ ;
			melt_object_get_field (slot, obj, 0,
					       "REFERENCED_VALUE");
	/*_.REFERENCED_VALUE__V25*/ meltfptr[21] = slot;
		      };
		      ;
		    }
		  else
		    {		/*^cond.else */

       /*_.REFERENCED_VALUE__V25*/ meltfptr[21] = NULL;;
		    }
		  ;
		  MELT_LOCATION ("warmelt-modes.melt:4558:/ cond");
		  /*cond */ if ( /*_.REFERENCED_VALUE__V25*/ meltfptr[21])	/*then */
		    {
		      /*^cond.then */
		      /*_.BUCK__V26*/ meltfptr[22] =
			/*_.REFERENCED_VALUE__V25*/ meltfptr[21];;
		    }
		  else
		    {
		      MELT_LOCATION ("warmelt-modes.melt:4558:/ cond.else");

		      /*^block */
		      /*anyblock */
		      {

	/*_.MAKE_BUCKETLONG__V27*/ meltfptr[26] =
			  (meltgc_new_longsbucket
			   ((meltobject_ptr_t)
			    ( /*!DISCR_BUCKET_LONGS */ meltfrout->tabval[3]),
			    (13)));;
			/*^compute */
			/*_.BUCK__V26*/ meltfptr[22] =
			  /*_.MAKE_BUCKETLONG__V27*/ meltfptr[26];;
			/*epilog */

			MELT_LOCATION ("warmelt-modes.melt:4558:/ clear");
		  /*clear *//*_.MAKE_BUCKETLONG__V27*/ meltfptr[26] = 0;
		      }
		      ;
		    }
		  ;

#if MELT_HAVE_DEBUG
		  MELT_LOCATION ("warmelt-modes.melt:4561:/ cppif.then");
		  /*^block */
		  /*anyblock */
		  {

		    /*^checksignal */
		    MELT_CHECK_SIGNAL ();
		    ;
	/*_#IS_BUCKETLONG__L11*/ meltfnum[6] =
		      (melt_magic_discr
		       ((melt_ptr_t) ( /*_.BUCK__V26*/ meltfptr[22])) ==
		       MELTOBMAG_BUCKETLONGS);;
		    MELT_LOCATION ("warmelt-modes.melt:4561:/ cond");
		    /*cond */ if ( /*_#IS_BUCKETLONG__L11*/ meltfnum[6])	/*then */
		      {
			/*^cond.then */
			/*_.IFELSE___V29*/ meltfptr[28] = ( /*nil */ NULL);;
		      }
		    else
		      {
			MELT_LOCATION ("warmelt-modes.melt:4561:/ cond.else");

			/*^block */
			/*anyblock */
			{




			  {
			    /*^locexp */
			    melt_assert_failed (("check buck"),
						("warmelt-modes.melt")
						? ("warmelt-modes.melt") :
						__FILE__,
						(4561) ? (4561) : __LINE__,
						__FUNCTION__);
			    ;
			  }
			  ;
		    /*clear *//*_.IFELSE___V29*/ meltfptr[28] = 0;
			  /*epilog */
			}
			;
		      }
		    ;
		    /*^compute */
		    /*_.IFCPP___V28*/ meltfptr[26] =
		      /*_.IFELSE___V29*/ meltfptr[28];;
		    /*epilog */

		    MELT_LOCATION ("warmelt-modes.melt:4561:/ clear");
		  /*clear *//*_#IS_BUCKETLONG__L11*/ meltfnum[6] = 0;
		    /*^clear */
		  /*clear *//*_.IFELSE___V29*/ meltfptr[28] = 0;
		  }

#else /*MELT_HAVE_DEBUG */
		  /*^cppif.else */
		  /*_.IFCPP___V28*/ meltfptr[26] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
		  ;
		  /*^compute */
      /*_.BUCKETLONG_PUT__V30*/ meltfptr[28] =
		    meltgc_longsbucket_put ((melt_ptr_t) /*_.BUCK__V26*/
					    meltfptr[22],
					    ( /*_#PID__L1*/ meltfnum[0]),
					    (melt_ptr_t) /*_.CHPH__V19*/
					    meltfptr[18]);;
		  MELT_LOCATION ("warmelt-modes.melt:4562:/ compute");
		  /*_.BUCK__V26*/ meltfptr[22] =
		    /*_.SETQ___V31*/ meltfptr[30] =
		    /*_.BUCKETLONG_PUT__V30*/ meltfptr[28];;
		  MELT_LOCATION ("warmelt-modes.melt:4563:/ checksignal");
		  MELT_CHECK_SIGNAL ();
		  ;
		  /*^cond */
		  /*cond */ if (
				 /*ifisa */
				 melt_is_instance_of ((melt_ptr_t)
						      (( /*!CHILDPROC_BUCKET_REFERENCE */ meltfrout->tabval[4])),
						      (melt_ptr_t) (( /*!CLASS_REFERENCE */ meltfrout->tabval[5])))
		    )		/*then */
		    {
		      /*^cond.then */
		      /*^block */
		      /*anyblock */
		      {

			/*^putslot */
			/*putslot */
			melt_assertmsg ("putslot checkobj @REFERENCED_VALUE",
					melt_magic_discr ((melt_ptr_t)
							  (( /*!CHILDPROC_BUCKET_REFERENCE */ meltfrout->tabval[4]))) == MELTOBMAG_OBJECT);
			melt_putfield_object ((( /*!CHILDPROC_BUCKET_REFERENCE */ meltfrout->tabval[4])), (0), ( /*_.BUCK__V26*/ meltfptr[22]), "REFERENCED_VALUE");
			;
			/*^touch */
			meltgc_touch (( /*!CHILDPROC_BUCKET_REFERENCE */
				       meltfrout->tabval[4]));
			;
			/*^touchobj */

			melt_dbgtrace_written_object (( /*!CHILDPROC_BUCKET_REFERENCE */ meltfrout->tabval[4]), "put-fields");
			;
			/*epilog */
		      }
		      ;
		    }		/*noelse */
		  ;

		  MELT_LOCATION ("warmelt-modes.melt:4558:/ clear");
		/*clear *//*_.REFERENCED_VALUE__V25*/ meltfptr[21] = 0;
		  /*^clear */
		/*clear *//*_.BUCK__V26*/ meltfptr[22] = 0;
		  /*^clear */
		/*clear *//*_.IFCPP___V28*/ meltfptr[26] = 0;
		  /*^clear */
		/*clear *//*_.BUCKETLONG_PUT__V30*/ meltfptr[28] = 0;
		  /*^clear */
		/*clear *//*_.SETQ___V31*/ meltfptr[30] = 0;
		  MELT_LOCATION ("warmelt-modes.melt:4565:/ checksignal");
		  MELT_CHECK_SIGNAL ();
		  ;
		  /*^cond */
		  /*cond */ if (
				 /*ifisa */
				 melt_is_instance_of ((melt_ptr_t)
						      (( /*!INITIAL_SYSTEM_DATA */ meltfrout->tabval[6])),
						      (melt_ptr_t) (( /*!CLASS_SYSTEM_DATA */ meltfrout->tabval[8])))
		    )		/*then */
		    {
		      /*^cond.then */
		      /*^block */
		      /*anyblock */
		      {

			/*^putslot */
			/*putslot */
			melt_assertmsg
			  ("putslot checkobj @SYSDATA_CHILD_HOOK",
			   melt_magic_discr ((melt_ptr_t)
					     (( /*!INITIAL_SYSTEM_DATA */
					       meltfrout->tabval[6]))) ==
			   MELTOBMAG_OBJECT);
			melt_putfield_object ((( /*!INITIAL_SYSTEM_DATA */
						meltfrout->tabval[6])), (38),
					      (( /*!CHILD_PROCESS_SIGCHLD_HANDLER */ meltfrout->tabval[7])), "SYSDATA_CHILD_HOOK");
			;
			/*^touch */
			meltgc_touch (( /*!INITIAL_SYSTEM_DATA */ meltfrout->
				       tabval[6]));
			;
			/*^touchobj */

			melt_dbgtrace_written_object (( /*!INITIAL_SYSTEM_DATA */ meltfrout->tabval[6]), "put-fields");
			;
			/*epilog */
		      }
		      ;
		    }		/*noelse */
		  ;
		  /* block_signals meltcit1__BLKSIGNAL end */
		  melt_blocklevel_signals = meltcit1__BLKSIGNAL_lev;
		  MELT_CHECK_SIGNAL ();


		  /*citerepilog */
		}		/*endciterblock BLOCK_SIGNALS */
		;
		MELT_LOCATION ("warmelt-modes.melt:4568:/ checksignal");
		MELT_CHECK_SIGNAL ();
		;
		/*^quasiblock */


		/*_.RETVAL___V1*/ meltfptr[0] = /*_.CHPH__V19*/ meltfptr[18];;

		{
		  MELT_LOCATION ("warmelt-modes.melt:4568:/ locexp");
		  /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
		  if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
		    melt_warn_for_no_expected_secondary_results ();
		  /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
		  ;
		}
		;
		/*^finalreturn */
		;
		/*finalret */ goto labend_rout;
		/*_.LET___V17*/ meltfptr[12] =
		  /*_.RETURN___V32*/ meltfptr[21];;

		MELT_LOCATION ("warmelt-modes.melt:4549:/ clear");
	       /*clear *//*_.MAKE_INTEGERBOX__V18*/ meltfptr[17] = 0;
		/*^clear */
	       /*clear *//*_.CHPH__V19*/ meltfptr[18] = 0;
		/*^clear */
	       /*clear *//*_.IFCPP___V21*/ meltfptr[20] = 0;
		/*^clear */
	       /*clear *//*_.RETURN___V32*/ meltfptr[21] = 0;
		/*_.LET___V10*/ meltfptr[9] = /*_.LET___V17*/ meltfptr[12];;

		MELT_LOCATION ("warmelt-modes.melt:4539:/ clear");
	       /*clear *//*_#PIDISBAD__L6*/ meltfnum[5] = 0;
		/*^clear */
	       /*clear *//*_.IFCPP___V11*/ meltfptr[10] = 0;
		/*^clear */
	       /*clear *//*_.IF___V15*/ meltfptr[11] = 0;
		/*^clear */
	       /*clear *//*_.LET___V17*/ meltfptr[12] = 0;
		/*_.IF___V9*/ meltfptr[5] = /*_.LET___V10*/ meltfptr[9];;
		/*epilog */

		MELT_LOCATION ("warmelt-modes.melt:4538:/ clear");
	       /*clear *//*_.LET___V10*/ meltfptr[9] = 0;
	      }
	      ;
	    }
	  else
	    {			/*^cond.else */

    /*_.IF___V9*/ meltfptr[5] = NULL;;
	    }
	  ;
	  /*^compute */
	  /*_.IF___V8*/ meltfptr[4] = /*_.IF___V9*/ meltfptr[5];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-modes.melt:4537:/ clear");
	     /*clear *//*_#I__L5*/ meltfnum[1] = 0;
	  /*^clear */
	     /*clear *//*_.IF___V9*/ meltfptr[5] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

  /*_.IF___V8*/ meltfptr[4] = NULL;;
      }
    ;
    MELT_LOCATION ("warmelt-modes.melt:4532:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.IF___V8*/ meltfptr[4];;

    {
      MELT_LOCATION ("warmelt-modes.melt:4532:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V4*/ meltfptr[3] = 0;
    /*^clear */
	   /*clear *//*_#IS_CLOSURE__L4*/ meltfnum[2] = 0;
    /*^clear */
	   /*clear *//*_.IF___V8*/ meltfptr[4] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("REGISTER_CHILD_PROCESS_HANDLER", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_79_warmelt_modes_REGISTER_CHILD_PROCESS_HANDLER_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_79_warmelt_modes_REGISTER_CHILD_PROCESS_HANDLER */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_80_warmelt_modes_UNREGISTER_CHILD_PROCESS_HANDLER (meltclosure_ptr_t
							    meltclosp_,
							    melt_ptr_t
							    meltfirstargp_,
							    const
							    melt_argdescr_cell_t
							    meltxargdescr_[],
							    union meltparam_un
							    * meltxargtab_,
							    const
							    melt_argdescr_cell_t
							    meltxresdescr_[],
							    union meltparam_un
							    * meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_80_warmelt_modes_UNREGISTER_CHILD_PROCESS_HANDLER_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_80_warmelt_modes_UNREGISTER_CHILD_PROCESS_HANDLER_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 8
    melt_ptr_t mcfr_varptr[8];
#define MELTFRAM_NBVARNUM 2
    long mcfr_varnum[2];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_80_warmelt_modes_UNREGISTER_CHILD_PROCESS_HANDLER is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct
	 frame_meltrout_80_warmelt_modes_UNREGISTER_CHILD_PROCESS_HANDLER_st
	 *) meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 8; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_80_warmelt_modes_UNREGISTER_CHILD_PROCESS_HANDLER nbval 8*/
  meltfram__.mcfr_nbvar = 8 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("UNREGISTER_CHILD_PROCESS_HANDLER", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-modes.melt:4576:/ getarg");
 /*_.CHD__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-modes.melt:4578:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L1*/ meltfnum[0] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-modes.melt:4578:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L1*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-modes.melt:4578:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-modes.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 4578;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring =
		"unregister_child_process_handler chd=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.CHD__V2*/ meltfptr[1];
	      /*_.MELT_DEBUG_FUN__V5*/ meltfptr[4] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V4*/ meltfptr[3] = /*_.MELT_DEBUG_FUN__V5*/ meltfptr[4];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-modes.melt:4578:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V5*/ meltfptr[4] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V4*/ meltfptr[3] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-modes.melt:4578:/ quasiblock");


      /*_.PROGN___V6*/ meltfptr[4] = /*_.IF___V4*/ meltfptr[3];;
      /*^compute */
      /*_.IFCPP___V3*/ meltfptr[2] = /*_.PROGN___V6*/ meltfptr[4];;
      /*epilog */

      MELT_LOCATION ("warmelt-modes.melt:4578:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L1*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IF___V4*/ meltfptr[3] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V6*/ meltfptr[4] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V3*/ meltfptr[2] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-modes.melt:4579:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
      /*^cond */
      /*cond */ if (( /*nil */ NULL))	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V8*/ meltfptr[4] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-modes.melt:4579:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("@$@unimplemented unregister_child_process_handler"), ("warmelt-modes.melt") ? ("warmelt-modes.melt") : __FILE__, (4579) ? (4579) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V8*/ meltfptr[4] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V7*/ meltfptr[3] = /*_.IFELSE___V8*/ meltfptr[4];;
      /*epilog */

      MELT_LOCATION ("warmelt-modes.melt:4579:/ clear");
	     /*clear *//*_.IFELSE___V8*/ meltfptr[4] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V7*/ meltfptr[3] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-modes.melt:4576:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.IFCPP___V7*/ meltfptr[3];;

    {
      MELT_LOCATION ("warmelt-modes.melt:4576:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V3*/ meltfptr[2] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V7*/ meltfptr[3] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("UNREGISTER_CHILD_PROCESS_HANDLER", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_80_warmelt_modes_UNREGISTER_CHILD_PROCESS_HANDLER_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_80_warmelt_modes_UNREGISTER_CHILD_PROCESS_HANDLER */



/**** end of warmelt-modes+03.c ****/
