/* GCC MELT GENERATED FILE warmelt-debug+02.c - DO NOT EDIT */
/* secondary MELT generated C file of rank #2 */
#include "melt-run.h"


/* used hash from melt-run.h when compiling this file: */
MELT_EXTERN const char meltrun_used_md5_melt_f2[] =
  MELT_RUN_HASHMD5 /* from melt-run.h */ ;


/**** warmelt-debug+02.c declarations ****/


/***************************************************
***
    Copyright 2009 - 2012 Free Software Foundation, Inc.
    Contributed by Basile Starynkevitch <basile@starynkevitch.net>

    This file is part of GCC.

    GCC is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3, or (at your option)
    any later version.

    GCC is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with GCC; see the file COPYING3.  If not see
    <http://www.gnu.org/licenses/>.
***
****************************************************/

/* ordinary MELT module */
#define MELT_HAS_INITIAL_ENVIRONMENT 1	/*usual */

struct melt_callframe_st;	/*defined in melt-runtime.h */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_1_warmelt_debug_DEBUG_MSG_FUN (meltclosure_ptr_t meltclosp_,
					melt_ptr_t meltfirstargp_,
					const melt_argdescr_cell_t
					meltxargdescr_[],
					union meltparam_un *meltxargtab_,
					const melt_argdescr_cell_t
					meltxresdescr_[],
					union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_2_warmelt_debug_REGISTER_TREE_DEBUG_FUN (meltclosure_ptr_t
						  meltclosp_,
						  melt_ptr_t meltfirstargp_,
						  const melt_argdescr_cell_t
						  meltxargdescr_[],
						  union meltparam_un
						  *meltxargtab_,
						  const melt_argdescr_cell_t
						  meltxresdescr_[],
						  union meltparam_un
						  *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_3_warmelt_debug_REGISTER_GIMPLE_DEBUG_FUN (meltclosure_ptr_t
						    meltclosp_,
						    melt_ptr_t meltfirstargp_,
						    const melt_argdescr_cell_t
						    meltxargdescr_[],
						    union meltparam_un
						    *meltxargtab_,
						    const melt_argdescr_cell_t
						    meltxresdescr_[],
						    union meltparam_un
						    *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_4_warmelt_debug_REGISTER_GIMPLESEQ_DEBUG_FUN (meltclosure_ptr_t
						       meltclosp_,
						       melt_ptr_t
						       meltfirstargp_,
						       const
						       melt_argdescr_cell_t
						       meltxargdescr_[],
						       union meltparam_un
						       *meltxargtab_,
						       const
						       melt_argdescr_cell_t
						       meltxresdescr_[],
						       union meltparam_un
						       *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_5_warmelt_debug_REGISTER_EDGE_DEBUG_FUN (meltclosure_ptr_t
						  meltclosp_,
						  melt_ptr_t meltfirstargp_,
						  const melt_argdescr_cell_t
						  meltxargdescr_[],
						  union meltparam_un
						  *meltxargtab_,
						  const melt_argdescr_cell_t
						  meltxresdescr_[],
						  union meltparam_un
						  *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_6_warmelt_debug_REGISTER_BASICBLOCK_DEBUG_FUN (meltclosure_ptr_t
							meltclosp_,
							melt_ptr_t
							meltfirstargp_,
							const
							melt_argdescr_cell_t
							meltxargdescr_[],
							union meltparam_un
							*meltxargtab_,
							const
							melt_argdescr_cell_t
							meltxresdescr_[],
							union meltparam_un
							*meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_7_warmelt_debug_REGISTER_LOOP_DEBUG_FUN (meltclosure_ptr_t
						  meltclosp_,
						  melt_ptr_t meltfirstargp_,
						  const melt_argdescr_cell_t
						  meltxargdescr_[],
						  union meltparam_un
						  *meltxargtab_,
						  const melt_argdescr_cell_t
						  meltxresdescr_[],
						  union meltparam_un
						  *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_8_warmelt_debug_MELT_DEBUG_FUN (meltclosure_ptr_t meltclosp_,
					 melt_ptr_t meltfirstargp_,
					 const melt_argdescr_cell_t
					 meltxargdescr_[],
					 union meltparam_un *meltxargtab_,
					 const melt_argdescr_cell_t
					 meltxresdescr_[],
					 union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_9_warmelt_debug_DBG_OUTOBJECT (meltclosure_ptr_t meltclosp_,
					melt_ptr_t meltfirstargp_,
					const melt_argdescr_cell_t
					meltxargdescr_[],
					union meltparam_un *meltxargtab_,
					const melt_argdescr_cell_t
					meltxresdescr_[],
					union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_10_warmelt_debug_DBG_OUT (meltclosure_ptr_t meltclosp_,
				   melt_ptr_t meltfirstargp_,
				   const melt_argdescr_cell_t
				   meltxargdescr_[],
				   union meltparam_un *meltxargtab_,
				   const melt_argdescr_cell_t
				   meltxresdescr_[],
				   union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_11_warmelt_debug_DBGOUT_FIELDS (meltclosure_ptr_t meltclosp_,
					 melt_ptr_t meltfirstargp_,
					 const melt_argdescr_cell_t
					 meltxargdescr_[],
					 union meltparam_un *meltxargtab_,
					 const melt_argdescr_cell_t
					 meltxresdescr_[],
					 union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_12_warmelt_debug_DBGOUTAGAIN_FIELDS (meltclosure_ptr_t meltclosp_,
					      melt_ptr_t meltfirstargp_,
					      const melt_argdescr_cell_t
					      meltxargdescr_[],
					      union meltparam_un
					      *meltxargtab_,
					      const melt_argdescr_cell_t
					      meltxresdescr_[],
					      union meltparam_un
					      *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_13_warmelt_debug_DBGOUT_NULL_METHOD (meltclosure_ptr_t meltclosp_,
					      melt_ptr_t meltfirstargp_,
					      const melt_argdescr_cell_t
					      meltxargdescr_[],
					      union meltparam_un
					      *meltxargtab_,
					      const melt_argdescr_cell_t
					      meltxresdescr_[],
					      union meltparam_un
					      *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_14_warmelt_debug_DBGOUT_STRING_METHOD (meltclosure_ptr_t meltclosp_,
						melt_ptr_t meltfirstargp_,
						const melt_argdescr_cell_t
						meltxargdescr_[],
						union meltparam_un
						*meltxargtab_,
						const melt_argdescr_cell_t
						meltxresdescr_[],
						union meltparam_un
						*meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_15_warmelt_debug_DBGOUT_STRBUF_METHOD (meltclosure_ptr_t meltclosp_,
						melt_ptr_t meltfirstargp_,
						const melt_argdescr_cell_t
						meltxargdescr_[],
						union meltparam_un
						*meltxargtab_,
						const melt_argdescr_cell_t
						meltxresdescr_[],
						union meltparam_un
						*meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_16_warmelt_debug_DBGOUT_INTEGER_METHOD (meltclosure_ptr_t meltclosp_,
						 melt_ptr_t meltfirstargp_,
						 const melt_argdescr_cell_t
						 meltxargdescr_[],
						 union meltparam_un
						 *meltxargtab_,
						 const melt_argdescr_cell_t
						 meltxresdescr_[],
						 union meltparam_un
						 *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_17_warmelt_debug_DBGOUT_MIXINT_METHOD (meltclosure_ptr_t meltclosp_,
						melt_ptr_t meltfirstargp_,
						const melt_argdescr_cell_t
						meltxargdescr_[],
						union meltparam_un
						*meltxargtab_,
						const melt_argdescr_cell_t
						meltxresdescr_[],
						union meltparam_un
						*meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_18_warmelt_debug_DBGOUT_MIXLOC_METHOD (meltclosure_ptr_t meltclosp_,
						melt_ptr_t meltfirstargp_,
						const melt_argdescr_cell_t
						meltxargdescr_[],
						union meltparam_un
						*meltxargtab_,
						const melt_argdescr_cell_t
						meltxresdescr_[],
						union meltparam_un
						*meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_19_warmelt_debug_DBGOUT_MIXBIGINT_METHOD (meltclosure_ptr_t
						   meltclosp_,
						   melt_ptr_t meltfirstargp_,
						   const melt_argdescr_cell_t
						   meltxargdescr_[],
						   union meltparam_un
						   *meltxargtab_,
						   const melt_argdescr_cell_t
						   meltxresdescr_[],
						   union meltparam_un
						   *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_20_warmelt_debug_DBGOUT_MULTIPLE_METHOD (meltclosure_ptr_t
						  meltclosp_,
						  melt_ptr_t meltfirstargp_,
						  const melt_argdescr_cell_t
						  meltxargdescr_[],
						  union meltparam_un
						  *meltxargtab_,
						  const melt_argdescr_cell_t
						  meltxresdescr_[],
						  union meltparam_un
						  *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_21_warmelt_debug_DBGOUT_ROUTINE_METHOD (meltclosure_ptr_t meltclosp_,
						 melt_ptr_t meltfirstargp_,
						 const melt_argdescr_cell_t
						 meltxargdescr_[],
						 union meltparam_un
						 *meltxargtab_,
						 const melt_argdescr_cell_t
						 meltxresdescr_[],
						 union meltparam_un
						 *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_22_warmelt_debug_LAMBDA___1__ (meltclosure_ptr_t meltclosp_,
					melt_ptr_t meltfirstargp_,
					const melt_argdescr_cell_t
					meltxargdescr_[],
					union meltparam_un *meltxargtab_,
					const melt_argdescr_cell_t
					meltxresdescr_[],
					union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_23_warmelt_debug_DBGOUT_CLOSURE_METHOD (meltclosure_ptr_t meltclosp_,
						 melt_ptr_t meltfirstargp_,
						 const melt_argdescr_cell_t
						 meltxargdescr_[],
						 union meltparam_un
						 *meltxargtab_,
						 const melt_argdescr_cell_t
						 meltxresdescr_[],
						 union meltparam_un
						 *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_24_warmelt_debug_DBGOUT_LIST_METHOD (meltclosure_ptr_t meltclosp_,
					      melt_ptr_t meltfirstargp_,
					      const melt_argdescr_cell_t
					      meltxargdescr_[],
					      union meltparam_un
					      *meltxargtab_,
					      const melt_argdescr_cell_t
					      meltxresdescr_[],
					      union meltparam_un
					      *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_25_warmelt_debug_DBGOUT_PAIR_METHOD (meltclosure_ptr_t meltclosp_,
					      melt_ptr_t meltfirstargp_,
					      const melt_argdescr_cell_t
					      meltxargdescr_[],
					      union meltparam_un
					      *meltxargtab_,
					      const melt_argdescr_cell_t
					      meltxresdescr_[],
					      union meltparam_un
					      *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_26_warmelt_debug_DBGOUT_MAPOBJECT_METHOD (meltclosure_ptr_t
						   meltclosp_,
						   melt_ptr_t meltfirstargp_,
						   const melt_argdescr_cell_t
						   meltxargdescr_[],
						   union meltparam_un
						   *meltxargtab_,
						   const melt_argdescr_cell_t
						   meltxresdescr_[],
						   union meltparam_un
						   *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_27_warmelt_debug_LAMBDA___2__ (meltclosure_ptr_t meltclosp_,
					melt_ptr_t meltfirstargp_,
					const melt_argdescr_cell_t
					meltxargdescr_[],
					union meltparam_un *meltxargtab_,
					const melt_argdescr_cell_t
					meltxresdescr_[],
					union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_28_warmelt_debug_LAMBDA___3__ (meltclosure_ptr_t meltclosp_,
					melt_ptr_t meltfirstargp_,
					const melt_argdescr_cell_t
					meltxargdescr_[],
					union meltparam_un *meltxargtab_,
					const melt_argdescr_cell_t
					meltxresdescr_[],
					union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_29_warmelt_debug_LAMBDA___4__ (meltclosure_ptr_t meltclosp_,
					melt_ptr_t meltfirstargp_,
					const melt_argdescr_cell_t
					meltxargdescr_[],
					union meltparam_un *meltxargtab_,
					const melt_argdescr_cell_t
					meltxresdescr_[],
					union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_30_warmelt_debug_DBGOUT_MAPSTRING_METHOD (meltclosure_ptr_t
						   meltclosp_,
						   melt_ptr_t meltfirstargp_,
						   const melt_argdescr_cell_t
						   meltxargdescr_[],
						   union meltparam_un
						   *meltxargtab_,
						   const melt_argdescr_cell_t
						   meltxresdescr_[],
						   union meltparam_un
						   *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_31_warmelt_debug_LAMBDA___5__ (meltclosure_ptr_t meltclosp_,
					melt_ptr_t meltfirstargp_,
					const melt_argdescr_cell_t
					meltxargdescr_[],
					union meltparam_un *meltxargtab_,
					const melt_argdescr_cell_t
					meltxresdescr_[],
					union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_32_warmelt_debug_LAMBDA___6__ (meltclosure_ptr_t meltclosp_,
					melt_ptr_t meltfirstargp_,
					const melt_argdescr_cell_t
					meltxargdescr_[],
					union meltparam_un *meltxargtab_,
					const melt_argdescr_cell_t
					meltxresdescr_[],
					union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_33_warmelt_debug_LAMBDA___7__ (meltclosure_ptr_t meltclosp_,
					melt_ptr_t meltfirstargp_,
					const melt_argdescr_cell_t
					meltxargdescr_[],
					union meltparam_un *meltxargtab_,
					const melt_argdescr_cell_t
					meltxresdescr_[],
					union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_34_warmelt_debug_DBGOUT_BUCKETLONG_METHOD (meltclosure_ptr_t
						    meltclosp_,
						    melt_ptr_t meltfirstargp_,
						    const melt_argdescr_cell_t
						    meltxargdescr_[],
						    union meltparam_un
						    *meltxargtab_,
						    const melt_argdescr_cell_t
						    meltxresdescr_[],
						    union meltparam_un
						    *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_35_warmelt_debug_DBGOUT_ANYOBJECT_METHOD (meltclosure_ptr_t
						   meltclosp_,
						   melt_ptr_t meltfirstargp_,
						   const melt_argdescr_cell_t
						   meltxargdescr_[],
						   union meltparam_un
						   *meltxargtab_,
						   const melt_argdescr_cell_t
						   meltxresdescr_[],
						   union meltparam_un
						   *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_36_warmelt_debug_DBGOUT_ANYRECV_METHOD (meltclosure_ptr_t meltclosp_,
						 melt_ptr_t meltfirstargp_,
						 const melt_argdescr_cell_t
						 meltxargdescr_[],
						 union meltparam_un
						 *meltxargtab_,
						 const melt_argdescr_cell_t
						 meltxresdescr_[],
						 union meltparam_un
						 *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_37_warmelt_debug_DBGOUTAGAIN_ANYOBJECT_METHOD (meltclosure_ptr_t
							meltclosp_,
							melt_ptr_t
							meltfirstargp_,
							const
							melt_argdescr_cell_t
							meltxargdescr_[],
							union meltparam_un
							*meltxargtab_,
							const
							melt_argdescr_cell_t
							meltxresdescr_[],
							union meltparam_un
							*meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_38_warmelt_debug_DBGOUT_NAMEDOBJECT_METHOD (meltclosure_ptr_t
						     meltclosp_,
						     melt_ptr_t
						     meltfirstargp_,
						     const
						     melt_argdescr_cell_t
						     meltxargdescr_[],
						     union meltparam_un
						     *meltxargtab_,
						     const
						     melt_argdescr_cell_t
						     meltxresdescr_[],
						     union meltparam_un
						     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_39_warmelt_debug_DBGOUTAGAIN_NAMEDOBJECT_METHOD (meltclosure_ptr_t
							  meltclosp_,
							  melt_ptr_t
							  meltfirstargp_,
							  const
							  melt_argdescr_cell_t
							  meltxargdescr_[],
							  union meltparam_un
							  *meltxargtab_,
							  const
							  melt_argdescr_cell_t
							  meltxresdescr_[],
							  union meltparam_un
							  *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_40_warmelt_debug_DBGOUTAGAIN_SYMBOL_METHOD (meltclosure_ptr_t
						     meltclosp_,
						     melt_ptr_t
						     meltfirstargp_,
						     const
						     melt_argdescr_cell_t
						     meltxargdescr_[],
						     union meltparam_un
						     *meltxargtab_,
						     const
						     melt_argdescr_cell_t
						     meltxresdescr_[],
						     union meltparam_un
						     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_41_warmelt_debug_DBGOUT_SYMBOL_METHOD (meltclosure_ptr_t meltclosp_,
						melt_ptr_t meltfirstargp_,
						const melt_argdescr_cell_t
						meltxargdescr_[],
						union meltparam_un
						*meltxargtab_,
						const melt_argdescr_cell_t
						meltxresdescr_[],
						union meltparam_un
						*meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_42_warmelt_debug_DBGOUT_CLASS_METHOD (meltclosure_ptr_t meltclosp_,
					       melt_ptr_t meltfirstargp_,
					       const melt_argdescr_cell_t
					       meltxargdescr_[],
					       union meltparam_un
					       *meltxargtab_,
					       const melt_argdescr_cell_t
					       meltxresdescr_[],
					       union meltparam_un
					       *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_43_warmelt_debug_DBGOUTAGAIN_KEYWORD_METHOD (meltclosure_ptr_t
						      meltclosp_,
						      melt_ptr_t
						      meltfirstargp_,
						      const
						      melt_argdescr_cell_t
						      meltxargdescr_[],
						      union meltparam_un
						      *meltxargtab_,
						      const
						      melt_argdescr_cell_t
						      meltxresdescr_[],
						      union meltparam_un
						      *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_44_warmelt_debug_DBGOUT_KEYWORD_METHOD (meltclosure_ptr_t meltclosp_,
						 melt_ptr_t meltfirstargp_,
						 const melt_argdescr_cell_t
						 meltxargdescr_[],
						 union meltparam_un
						 *meltxargtab_,
						 const melt_argdescr_cell_t
						 meltxresdescr_[],
						 union meltparam_un
						 *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_45_warmelt_debug_DBGOUTAGAIN_CLONED_SYMBOL_METHOD (meltclosure_ptr_t
							    meltclosp_,
							    melt_ptr_t
							    meltfirstargp_,
							    const
							    melt_argdescr_cell_t
							    meltxargdescr_[],
							    union meltparam_un
							    *meltxargtab_,
							    const
							    melt_argdescr_cell_t
							    meltxresdescr_[],
							    union meltparam_un
							    *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_46_warmelt_debug_DBGOUT_CLONED_SYMBOL_METHOD (meltclosure_ptr_t
						       meltclosp_,
						       melt_ptr_t
						       meltfirstargp_,
						       const
						       melt_argdescr_cell_t
						       meltxargdescr_[],
						       union meltparam_un
						       *meltxargtab_,
						       const
						       melt_argdescr_cell_t
						       meltxresdescr_[],
						       union meltparam_un
						       *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_47_warmelt_debug_DBGOUT_ENVIRONMENT_METHOD (meltclosure_ptr_t
						     meltclosp_,
						     melt_ptr_t
						     meltfirstargp_,
						     const
						     melt_argdescr_cell_t
						     meltxargdescr_[],
						     union meltparam_un
						     *meltxargtab_,
						     const
						     melt_argdescr_cell_t
						     meltxresdescr_[],
						     union meltparam_un
						     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_48_warmelt_debug_DBGOUTAGAIN_CTYPE_METHOD (meltclosure_ptr_t
						    meltclosp_,
						    melt_ptr_t meltfirstargp_,
						    const melt_argdescr_cell_t
						    meltxargdescr_[],
						    union meltparam_un
						    *meltxargtab_,
						    const melt_argdescr_cell_t
						    meltxresdescr_[],
						    union meltparam_un
						    *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_49_warmelt_debug_DBGOUT_CTYPE_METHOD (meltclosure_ptr_t meltclosp_,
					       melt_ptr_t meltfirstargp_,
					       const melt_argdescr_cell_t
					       meltxargdescr_[],
					       union meltparam_un
					       *meltxargtab_,
					       const melt_argdescr_cell_t
					       meltxresdescr_[],
					       union meltparam_un
					       *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_50_warmelt_debug_DBGOUT_ANYBINDING_METHOD (meltclosure_ptr_t
						    meltclosp_,
						    melt_ptr_t meltfirstargp_,
						    const melt_argdescr_cell_t
						    meltxargdescr_[],
						    union meltparam_un
						    *meltxargtab_,
						    const melt_argdescr_cell_t
						    meltxresdescr_[],
						    union meltparam_un
						    *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_51_warmelt_debug_DISPLAY_DEBUG_MESSAGE (meltclosure_ptr_t meltclosp_,
						 melt_ptr_t meltfirstargp_,
						 const melt_argdescr_cell_t
						 meltxargdescr_[],
						 union meltparam_un
						 *meltxargtab_,
						 const melt_argdescr_cell_t
						 meltxresdescr_[],
						 union meltparam_un
						 *meltxrestab_);



MELT_EXTERN void *melt_start_this_module (void *);


/* define different names when debugging or not */
#if MELT_HAVE_DEBUG
MELT_EXTERN const char meltmodule_warmelt_debug__melt_have_debug_enabled[];
#define melt_have_debug_string meltmodule_warmelt_debug__melt_have_debug_enabled
#else /*!MELT_HAVE_DEBUG */
MELT_EXTERN const char meltmodule_warmelt_debug__melt_have_debug_disabled[];
#define melt_have_debug_string meltmodule_warmelt_debug__melt_have_debug_disabled
#endif /*!MELT_HAVE_DEBUG */


struct frame_melt_start_this_module_st;
void MELT_MODULE_VISIBILITY meltmod__warmelt_debug__initialmeltchunk_0 (struct
									frame_melt_start_this_module_st
									*,
									char
									*);
void MELT_MODULE_VISIBILITY meltmod__warmelt_debug__initialmeltchunk_1 (struct
									frame_melt_start_this_module_st
									*,
									char
									*);
void MELT_MODULE_VISIBILITY meltmod__warmelt_debug__initialmeltchunk_2 (struct
									frame_melt_start_this_module_st
									*,
									char
									*);
void MELT_MODULE_VISIBILITY meltmod__warmelt_debug__initialmeltchunk_3 (struct
									frame_melt_start_this_module_st
									*,
									char
									*);
void MELT_MODULE_VISIBILITY meltmod__warmelt_debug__initialmeltchunk_4 (struct
									frame_melt_start_this_module_st
									*,
									char
									*);
void MELT_MODULE_VISIBILITY meltmod__warmelt_debug__initialmeltchunk_5 (struct
									frame_melt_start_this_module_st
									*,
									char
									*);
void MELT_MODULE_VISIBILITY meltmod__warmelt_debug__initialmeltchunk_6 (struct
									frame_melt_start_this_module_st
									*,
									char
									*);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_debug__forward_or_mark_module_start_frame (struct
							    melt_callframe_st
							    *fp, int marking);
#define meltmarking_melt_start_this_module  meltmod__warmelt_debug__forward_or_mark_module_start_frame



/**** warmelt-debug+02.c implementations ****/




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_37_warmelt_debug_DBGOUTAGAIN_ANYOBJECT_METHOD (meltclosure_ptr_t
							meltclosp_,
							melt_ptr_t
							meltfirstargp_,
							const
							melt_argdescr_cell_t
							meltxargdescr_[],
							union meltparam_un
							*meltxargtab_,
							const
							melt_argdescr_cell_t
							meltxresdescr_[],
							union meltparam_un
							*meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_37_warmelt_debug_DBGOUTAGAIN_ANYOBJECT_METHOD_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_37_warmelt_debug_DBGOUTAGAIN_ANYOBJECT_METHOD_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 8
    melt_ptr_t mcfr_varptr[8];
#define MELTFRAM_NBVARNUM 4
    long mcfr_varnum[4];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_37_warmelt_debug_DBGOUTAGAIN_ANYOBJECT_METHOD is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct
	 frame_meltrout_37_warmelt_debug_DBGOUTAGAIN_ANYOBJECT_METHOD_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 8; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_37_warmelt_debug_DBGOUTAGAIN_ANYOBJECT_METHOD nbval 8*/
  meltfram__.mcfr_nbvar = 8 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("DBGOUTAGAIN_ANYOBJECT_METHOD", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-debug.melt:1246:/ getarg");
 /*_.SELF__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.DBGI__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.DBGI__V3*/ meltfptr[2])) !=
	      NULL);


  /*getarg#2 */
  /*^getarg */
  if (meltxargdescr_[1] != MELTBPAR_LONG)
    goto lab_endgetargs;
 /*_#DEPTH__L1*/ meltfnum[0] = meltxargtab_[1].meltbp_long;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-debug.melt:1247:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L2*/ meltfnum[1] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.DBGI__V3*/ meltfptr[2]),
			     (melt_ptr_t) (( /*!CLASS_DEBUG_INFORMATION */
					    meltfrout->tabval[0])));;
      MELT_LOCATION ("warmelt-debug.melt:1247:/ cond");
      /*cond */ if ( /*_#IS_A__L2*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V5*/ meltfptr[4] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-debug.melt:1247:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check dbgi"),
				  ("warmelt-debug.melt")
				  ? ("warmelt-debug.melt") : __FILE__,
				  (1247) ? (1247) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V5*/ meltfptr[4] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V4*/ meltfptr[3] = /*_.IFELSE___V5*/ meltfptr[4];;
      /*epilog */

      MELT_LOCATION ("warmelt-debug.melt:1247:/ clear");
	     /*clear *//*_#IS_A__L2*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V5*/ meltfptr[4] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V4*/ meltfptr[3] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-debug.melt:1248:/ quasiblock");


 /*_.DIS__V6*/ meltfptr[4] =
      ((melt_ptr_t)
       (melt_discr ((melt_ptr_t) ( /*_.SELF__V2*/ meltfptr[1]))));;
    MELT_LOCATION ("warmelt-debug.melt:1249:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.DBGI__V3*/ meltfptr[2]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 0, "DBGI_OUT");
  /*_.SBUF__V7*/ meltfptr[6] = slot;
    };
    ;
 /*_#ONUM__L3*/ meltfnum[1] =
      (melt_obj_num ((melt_ptr_t) ( /*_.SELF__V2*/ meltfptr[1])));;

    {
      MELT_LOCATION ("warmelt-debug.melt:1252:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V7*/ meltfptr[6]),
			   ("^^|"));
    }
    ;
    MELT_LOCATION ("warmelt-debug.melt:1253:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.DIS__V6*/ meltfptr[4]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 1, "NAMED_NAME");
  /*_.NAMED_NAME__V8*/ meltfptr[7] = slot;
    };
    ;

    {
      /*^locexp */
      /*add2sbuf_string */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V7*/ meltfptr[6]),
			   melt_string_str ((melt_ptr_t)
					    ( /*_.NAMED_NAME__V8*/
					     meltfptr[7])));
    }
    ;

    {
      MELT_LOCATION ("warmelt-debug.melt:1254:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V7*/ meltfptr[6]), ("/"));
    }
    ;
 /*_#OBJ_HASH__L4*/ meltfnum[3] =
      (melt_obj_hash ((melt_ptr_t) ( /*_.SELF__V2*/ meltfptr[1])));;

    {
      MELT_LOCATION ("warmelt-debug.melt:1255:/ locexp");
      meltgc_add_strbuf_hex ((melt_ptr_t) ( /*_.SBUF__V7*/ meltfptr[6]),
			     ( /*_#OBJ_HASH__L4*/ meltfnum[3]));
    }
    ;
    MELT_LOCATION ("warmelt-debug.melt:1256:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^cond */
    /*cond */ if ( /*_#ONUM__L3*/ meltfnum[1])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{


	  {
	    MELT_LOCATION ("warmelt-debug.melt:1258:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V7*/ meltfptr[6]),
				 ("#"));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-debug.melt:1259:/ locexp");
	    meltgc_add_strbuf_dec ((melt_ptr_t) ( /*_.SBUF__V7*/ meltfptr[6]),
				   ( /*_#ONUM__L3*/ meltfnum[1]));
	  }
	  ;
	  MELT_LOCATION ("warmelt-debug.melt:1257:/ quasiblock");


	  /*epilog */
	}
	;
      }				/*noelse */
    ;

    MELT_LOCATION ("warmelt-debug.melt:1248:/ clear");
	   /*clear *//*_.DIS__V6*/ meltfptr[4] = 0;
    /*^clear */
	   /*clear *//*_.SBUF__V7*/ meltfptr[6] = 0;
    /*^clear */
	   /*clear *//*_#ONUM__L3*/ meltfnum[1] = 0;
    /*^clear */
	   /*clear *//*_.NAMED_NAME__V8*/ meltfptr[7] = 0;
    /*^clear */
	   /*clear *//*_#OBJ_HASH__L4*/ meltfnum[3] = 0;
    MELT_LOCATION ("warmelt-debug.melt:1246:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V4*/ meltfptr[3] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("DBGOUTAGAIN_ANYOBJECT_METHOD", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_37_warmelt_debug_DBGOUTAGAIN_ANYOBJECT_METHOD_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*noretval */ NULL);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_37_warmelt_debug_DBGOUTAGAIN_ANYOBJECT_METHOD */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_38_warmelt_debug_DBGOUT_NAMEDOBJECT_METHOD (meltclosure_ptr_t
						     meltclosp_,
						     melt_ptr_t
						     meltfirstargp_,
						     const
						     melt_argdescr_cell_t
						     meltxargdescr_[],
						     union meltparam_un *
						     meltxargtab_,
						     const
						     melt_argdescr_cell_t
						     meltxresdescr_[],
						     union meltparam_un *
						     meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_38_warmelt_debug_DBGOUT_NAMEDOBJECT_METHOD_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_38_warmelt_debug_DBGOUT_NAMEDOBJECT_METHOD_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 17
    melt_ptr_t mcfr_varptr[17];
#define MELTFRAM_NBVARNUM 13
    long mcfr_varnum[13];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_38_warmelt_debug_DBGOUT_NAMEDOBJECT_METHOD is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_38_warmelt_debug_DBGOUT_NAMEDOBJECT_METHOD_st
	 *) meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 17; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_38_warmelt_debug_DBGOUT_NAMEDOBJECT_METHOD nbval 17*/
  meltfram__.mcfr_nbvar = 17 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("DBGOUT_NAMEDOBJECT_METHOD", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-debug.melt:1264:/ getarg");
 /*_.SELF__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.DBGI__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.DBGI__V3*/ meltfptr[2])) !=
	      NULL);


  /*getarg#2 */
  /*^getarg */
  if (meltxargdescr_[1] != MELTBPAR_LONG)
    goto lab_endgetargs;
 /*_#DEPTH__L1*/ meltfnum[0] = meltxargtab_[1].meltbp_long;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-debug.melt:1265:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L2*/ meltfnum[1] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.DBGI__V3*/ meltfptr[2]),
			     (melt_ptr_t) (( /*!CLASS_DEBUG_INFORMATION */
					    meltfrout->tabval[0])));;
      MELT_LOCATION ("warmelt-debug.melt:1265:/ cond");
      /*cond */ if ( /*_#IS_A__L2*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V5*/ meltfptr[4] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-debug.melt:1265:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check dbgi"),
				  ("warmelt-debug.melt")
				  ? ("warmelt-debug.melt") : __FILE__,
				  (1265) ? (1265) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V5*/ meltfptr[4] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V4*/ meltfptr[3] = /*_.IFELSE___V5*/ meltfptr[4];;
      /*epilog */

      MELT_LOCATION ("warmelt-debug.melt:1265:/ clear");
	     /*clear *//*_#IS_A__L2*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V5*/ meltfptr[4] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V4*/ meltfptr[3] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-debug.melt:1266:/ quasiblock");


 /*_.DIS__V6*/ meltfptr[4] =
      ((melt_ptr_t)
       (melt_discr ((melt_ptr_t) ( /*_.SELF__V2*/ meltfptr[1]))));;
    MELT_LOCATION ("warmelt-debug.melt:1267:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.DBGI__V3*/ meltfptr[2]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 0, "DBGI_OUT");
  /*_.OUT__V7*/ meltfptr[6] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-debug.melt:1268:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.SELF__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 1, "NAMED_NAME");
  /*_.ONAM__V8*/ meltfptr[7] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-debug.melt:1269:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.SELF__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 0, "PROP_TABLE");
  /*_.OPROP__V9*/ meltfptr[8] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-debug.melt:1270:/ cond");
    /*cond */ if (
		   /*ifisa */
		   melt_is_instance_of ((melt_ptr_t)
					( /*_.DBGI__V3*/ meltfptr[2]),
					(melt_ptr_t) (( /*!CLASS_DEBUG_INFORMATION */ meltfrout->tabval[0])))
      )				/*then */
      {
	/*^cond.then */
	/*^getslot */
	{
	  melt_ptr_t slot = NULL, obj = NULL;
	  obj = (melt_ptr_t) ( /*_.DBGI__V3*/ meltfptr[2]) /*=obj*/ ;
	  melt_object_get_field (slot, obj, 2, "DBGI_MAXDEPTH");
   /*_.DBGI_MAXDEPTH__V10*/ meltfptr[9] = slot;
	};
	;
      }
    else
      {				/*^cond.else */

  /*_.DBGI_MAXDEPTH__V10*/ meltfptr[9] = NULL;;
      }
    ;
    /*^compute */
 /*_#MAXDEPTH__L3*/ meltfnum[1] =
      (melt_get_int ((melt_ptr_t) ( /*_.DBGI_MAXDEPTH__V10*/ meltfptr[9])));;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-debug.melt:1272:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_OUT__L4*/ meltfnum[3] =
	(melt_is_out ((melt_ptr_t) /*_.OUT__V7*/ meltfptr[6]));;
      MELT_LOCATION ("warmelt-debug.melt:1272:/ cond");
      /*cond */ if ( /*_#IS_OUT__L4*/ meltfnum[3])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V12*/ meltfptr[11] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-debug.melt:1272:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check out"),
				  ("warmelt-debug.melt")
				  ? ("warmelt-debug.melt") : __FILE__,
				  (1272) ? (1272) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V12*/ meltfptr[11] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V11*/ meltfptr[10] = /*_.IFELSE___V12*/ meltfptr[11];;
      /*epilog */

      MELT_LOCATION ("warmelt-debug.melt:1272:/ clear");
	     /*clear *//*_#IS_OUT__L4*/ meltfnum[3] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V12*/ meltfptr[11] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V11*/ meltfptr[10] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

    {
      MELT_LOCATION ("warmelt-debug.melt:1273:/ locexp");
      meltgc_add_out ((melt_ptr_t) ( /*_.OUT__V7*/ meltfptr[6]), ("`"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-debug.melt:1274:/ locexp");
      meltgc_add_out ((melt_ptr_t) ( /*_.OUT__V7*/ meltfptr[6]),
		      melt_string_str ((melt_ptr_t)
				       ( /*_.ONAM__V8*/ meltfptr[7])));
    }
    ;

    {
      MELT_LOCATION ("warmelt-debug.melt:1275:/ locexp");
      meltgc_add_out ((melt_ptr_t) ( /*_.OUT__V7*/ meltfptr[6]), ("|"));
    }
    ;
    MELT_LOCATION ("warmelt-debug.melt:1276:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.DIS__V6*/ meltfptr[4]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 1, "NAMED_NAME");
  /*_.NAMED_NAME__V13*/ meltfptr[11] = slot;
    };
    ;

    {
      /*^locexp */
      meltgc_add_out ((melt_ptr_t) ( /*_.OUT__V7*/ meltfptr[6]),
		      melt_string_str ((melt_ptr_t)
				       ( /*_.NAMED_NAME__V13*/
					meltfptr[11])));
    }
    ;

    {
      MELT_LOCATION ("warmelt-debug.melt:1277:/ locexp");
      meltgc_add_out ((melt_ptr_t) ( /*_.OUT__V7*/ meltfptr[6]), ("/"));
    }
    ;
 /*_#OBJ_HASH__L5*/ meltfnum[3] =
      (melt_obj_hash ((melt_ptr_t) ( /*_.SELF__V2*/ meltfptr[1])));;

    {
      MELT_LOCATION ("warmelt-debug.melt:1278:/ locexp");
      meltgc_add_out_hex ((melt_ptr_t) ( /*_.OUT__V7*/ meltfptr[6]),
			  ( /*_#OBJ_HASH__L5*/ meltfnum[3]));
    }
    ;
    MELT_LOCATION ("warmelt-debug.melt:1279:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#MELT_REALLY_NEED_DBGLIM__L6*/ meltfnum[5] =
      ( /*MELT_REALLY_NEED_DBGLIM */
       melt_need_debug_limit ((int) /*_#DEPTH__L1*/ meltfnum[0],
			      (int) /*_#MAXDEPTH__L3*/ meltfnum[1]));;
    MELT_LOCATION ("warmelt-debug.melt:1279:/ cond");
    /*cond */ if ( /*_#MELT_REALLY_NEED_DBGLIM__L6*/ meltfnum[5])	/*then */
      {
	/*^cond.then */
	/*_#OR___L7*/ meltfnum[6] =
	  /*_#MELT_REALLY_NEED_DBGLIM__L6*/ meltfnum[5];;
      }
    else
      {
	MELT_LOCATION ("warmelt-debug.melt:1279:/ cond.else");

	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-debug.melt:1281:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
   /*_#I__L8*/ meltfnum[7] =
	    (( /*_#DEPTH__L1*/ meltfnum[0]) <
	     ( /*_#MAXDEPTH__L3*/ meltfnum[1]));;
	  MELT_LOCATION ("warmelt-debug.melt:1281:/ cond");
	  /*cond */ if ( /*_#I__L8*/ meltfnum[7])	/*then */
	    {
	      /*^cond.then */
	      /*^block */
	      /*anyblock */
	      {

     /*_#IS_A__L10*/ meltfnum[9] =
		  melt_is_instance_of ((melt_ptr_t)
				       ( /*_.DBGI__V3*/ meltfptr[2]),
				       (melt_ptr_t) (( /*!CLASS_DEBUG_OUTPUT_INFORMATION */ meltfrout->tabval[1])));;
		/*^compute */
		/*_#IF___L9*/ meltfnum[8] = /*_#IS_A__L10*/ meltfnum[9];;
		/*epilog */

		MELT_LOCATION ("warmelt-debug.melt:1281:/ clear");
	       /*clear *//*_#IS_A__L10*/ meltfnum[9] = 0;
	      }
	      ;
	    }
	  else
	    {			/*^cond.else */

    /*_#IF___L9*/ meltfnum[8] = 0;;
	    }
	  ;
	  /*^compute */
	  /*_#OR___L7*/ meltfnum[6] = /*_#IF___L9*/ meltfnum[8];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-debug.melt:1279:/ clear");
	     /*clear *//*_#I__L8*/ meltfnum[7] = 0;
	  /*^clear */
	     /*clear *//*_#IF___L9*/ meltfnum[8] = 0;
	}
	;
      }
    ;
    /*^cond */
    /*cond */ if ( /*_#OR___L7*/ meltfnum[6])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-debug.melt:1282:/ quasiblock");


   /*_#ONUM__L11*/ meltfnum[9] =
	    (melt_obj_num ((melt_ptr_t) ( /*_.SELF__V2*/ meltfptr[1])));;
	  MELT_LOCATION ("warmelt-debug.melt:1284:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^cond */
	  /*cond */ if ( /*_#ONUM__L11*/ meltfnum[9])	/*then */
	    {
	      /*^cond.then */
	      /*^block */
	      /*anyblock */
	      {


		{
		  MELT_LOCATION ("warmelt-debug.melt:1286:/ locexp");
		  meltgc_add_out ((melt_ptr_t) ( /*_.OUT__V7*/ meltfptr[6]),
				  ("#"));
		}
		;

		{
		  MELT_LOCATION ("warmelt-debug.melt:1287:/ locexp");
		  meltgc_add_out_dec ((melt_ptr_t)
				      ( /*_.OUT__V7*/ meltfptr[6]),
				      ( /*_#ONUM__L11*/ meltfnum[9]));
		}
		;
		MELT_LOCATION ("warmelt-debug.melt:1285:/ quasiblock");


		/*epilog */
	      }
	      ;
	    }			/*noelse */
	  ;

	  {
	    MELT_LOCATION ("warmelt-debug.melt:1288:/ locexp");
	    meltgc_add_out ((melt_ptr_t) ( /*_.OUT__V7*/ meltfptr[6]), ("{"));
	  }
	  ;
	  MELT_LOCATION ("warmelt-debug.melt:1289:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^cond */
	  /*cond */ if ( /*_.OPROP__V9*/ meltfptr[8])	/*then */
	    {
	      /*^cond.then */
	      /*^block */
	      /*anyblock */
	      {


		{
		  MELT_LOCATION ("warmelt-debug.melt:1291:/ locexp");
		  meltgc_add_out ((melt_ptr_t) ( /*_.OUT__V7*/ meltfptr[6]),
				  ("prop="));
		}
		;
     /*_#I__L12*/ meltfnum[7] =
		  (( /*_#DEPTH__L1*/ meltfnum[0]) + (3));;
		MELT_LOCATION ("warmelt-debug.melt:1292:/ checksignal");
		MELT_CHECK_SIGNAL ();
		;
		/*^apply */
		/*apply */
		{
		  union meltparam_un argtab[2];
		  memset (&argtab, 0, sizeof (argtab));
		  /*^apply.arg */
		  argtab[0].meltbp_aptr =
		    (melt_ptr_t *) & /*_.DBGI__V3*/ meltfptr[2];
		  /*^apply.arg */
		  argtab[1].meltbp_long = /*_#I__L12*/ meltfnum[7];
		  /*_.DBG_OUT__V15*/ meltfptr[14] =
		    melt_apply ((meltclosure_ptr_t)
				(( /*!DBG_OUT */ meltfrout->tabval[2])),
				(melt_ptr_t) ( /*_.OPROP__V9*/ meltfptr[8]),
				(MELTBPARSTR_PTR MELTBPARSTR_LONG ""), argtab,
				"", (union meltparam_un *) 0);
		}
		;
		MELT_LOCATION ("warmelt-debug.melt:1290:/ quasiblock");


		/*_.PROGN___V16*/ meltfptr[15] =
		  /*_.DBG_OUT__V15*/ meltfptr[14];;
		/*^compute */
		/*_.IF___V14*/ meltfptr[13] = /*_.PROGN___V16*/ meltfptr[15];;
		/*epilog */

		MELT_LOCATION ("warmelt-debug.melt:1289:/ clear");
	       /*clear *//*_#I__L12*/ meltfnum[7] = 0;
		/*^clear */
	       /*clear *//*_.DBG_OUT__V15*/ meltfptr[14] = 0;
		/*^clear */
	       /*clear *//*_.PROGN___V16*/ meltfptr[15] = 0;
	      }
	      ;
	    }
	  else
	    {			/*^cond.else */

    /*_.IF___V14*/ meltfptr[13] = NULL;;
	    }
	  ;
	  /*^compute */
   /*_#I__L13*/ meltfnum[8] =
	    (( /*_#DEPTH__L1*/ meltfnum[0]) + (3));;
	  MELT_LOCATION ("warmelt-debug.melt:1294:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^apply */
	  /*apply */
	  {
	    union meltparam_un argtab[4];
	    memset (&argtab, 0, sizeof (argtab));
	    /*^apply.arg */
	    argtab[0].meltbp_aptr =
	      (melt_ptr_t *) & /*_.DBGI__V3*/ meltfptr[2];
	    /*^apply.arg */
	    argtab[1].meltbp_long = /*_#I__L13*/ meltfnum[8];
	    /*^apply.arg */
	    argtab[2].meltbp_long = 2;
	    /*^apply.arg */
	    argtab[3].meltbp_long = 0;
	    /*_.DBGOUT_FIELDS__V17*/ meltfptr[14] =
	      melt_apply ((meltclosure_ptr_t)
			  (( /*!DBGOUT_FIELDS */ meltfrout->tabval[3])),
			  (melt_ptr_t) ( /*_.SELF__V2*/ meltfptr[1]),
			  (MELTBPARSTR_PTR MELTBPARSTR_LONG MELTBPARSTR_LONG
			   MELTBPARSTR_LONG ""), argtab, "",
			  (union meltparam_un *) 0);
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-debug.melt:1295:/ locexp");
	    meltgc_add_out ((melt_ptr_t) ( /*_.OUT__V7*/ meltfptr[6]), ("}"));
	  }
	  ;

	  MELT_LOCATION ("warmelt-debug.melt:1282:/ clear");
	     /*clear *//*_#ONUM__L11*/ meltfnum[9] = 0;
	  /*^clear */
	     /*clear *//*_.IF___V14*/ meltfptr[13] = 0;
	  /*^clear */
	     /*clear *//*_#I__L13*/ meltfnum[8] = 0;
	  /*^clear */
	     /*clear *//*_.DBGOUT_FIELDS__V17*/ meltfptr[14] = 0;
	  /*epilog */
	}
	;
      }				/*noelse */
    ;

    MELT_LOCATION ("warmelt-debug.melt:1266:/ clear");
	   /*clear *//*_.DIS__V6*/ meltfptr[4] = 0;
    /*^clear */
	   /*clear *//*_.OUT__V7*/ meltfptr[6] = 0;
    /*^clear */
	   /*clear *//*_.ONAM__V8*/ meltfptr[7] = 0;
    /*^clear */
	   /*clear *//*_.OPROP__V9*/ meltfptr[8] = 0;
    /*^clear */
	   /*clear *//*_.DBGI_MAXDEPTH__V10*/ meltfptr[9] = 0;
    /*^clear */
	   /*clear *//*_#MAXDEPTH__L3*/ meltfnum[1] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V11*/ meltfptr[10] = 0;
    /*^clear */
	   /*clear *//*_.NAMED_NAME__V13*/ meltfptr[11] = 0;
    /*^clear */
	   /*clear *//*_#OBJ_HASH__L5*/ meltfnum[3] = 0;
    /*^clear */
	   /*clear *//*_#MELT_REALLY_NEED_DBGLIM__L6*/ meltfnum[5] = 0;
    /*^clear */
	   /*clear *//*_#OR___L7*/ meltfnum[6] = 0;
    MELT_LOCATION ("warmelt-debug.melt:1264:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V4*/ meltfptr[3] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("DBGOUT_NAMEDOBJECT_METHOD", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_38_warmelt_debug_DBGOUT_NAMEDOBJECT_METHOD_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*noretval */ NULL);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_38_warmelt_debug_DBGOUT_NAMEDOBJECT_METHOD */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_39_warmelt_debug_DBGOUTAGAIN_NAMEDOBJECT_METHOD (meltclosure_ptr_t
							  meltclosp_,
							  melt_ptr_t
							  meltfirstargp_,
							  const
							  melt_argdescr_cell_t
							  meltxargdescr_[],
							  union meltparam_un *
							  meltxargtab_,
							  const
							  melt_argdescr_cell_t
							  meltxresdescr_[],
							  union meltparam_un *
							  meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_39_warmelt_debug_DBGOUTAGAIN_NAMEDOBJECT_METHOD_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_39_warmelt_debug_DBGOUTAGAIN_NAMEDOBJECT_METHOD_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 9
    melt_ptr_t mcfr_varptr[9];
#define MELTFRAM_NBVARNUM 3
    long mcfr_varnum[3];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_39_warmelt_debug_DBGOUTAGAIN_NAMEDOBJECT_METHOD is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct
	 frame_meltrout_39_warmelt_debug_DBGOUTAGAIN_NAMEDOBJECT_METHOD_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 9; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_39_warmelt_debug_DBGOUTAGAIN_NAMEDOBJECT_METHOD nbval 9*/
  meltfram__.mcfr_nbvar = 9 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("DBGOUTAGAIN_NAMEDOBJECT_METHOD", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-debug.melt:1301:/ getarg");
 /*_.SELF__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.DBGI__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.DBGI__V3*/ meltfptr[2])) !=
	      NULL);


  /*getarg#2 */
  /*^getarg */
  if (meltxargdescr_[1] != MELTBPAR_LONG)
    goto lab_endgetargs;
 /*_#DEPTH__L1*/ meltfnum[0] = meltxargtab_[1].meltbp_long;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-debug.melt:1302:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L2*/ meltfnum[1] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.DBGI__V3*/ meltfptr[2]),
			     (melt_ptr_t) (( /*!CLASS_DEBUG_INFORMATION */
					    meltfrout->tabval[0])));;
      MELT_LOCATION ("warmelt-debug.melt:1302:/ cond");
      /*cond */ if ( /*_#IS_A__L2*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V5*/ meltfptr[4] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-debug.melt:1302:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check dbgi"),
				  ("warmelt-debug.melt")
				  ? ("warmelt-debug.melt") : __FILE__,
				  (1302) ? (1302) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V5*/ meltfptr[4] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V4*/ meltfptr[3] = /*_.IFELSE___V5*/ meltfptr[4];;
      /*epilog */

      MELT_LOCATION ("warmelt-debug.melt:1302:/ clear");
	     /*clear *//*_#IS_A__L2*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V5*/ meltfptr[4] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V4*/ meltfptr[3] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-debug.melt:1303:/ quasiblock");


 /*_.DIS__V6*/ meltfptr[4] =
      ((melt_ptr_t)
       (melt_discr ((melt_ptr_t) ( /*_.SELF__V2*/ meltfptr[1]))));;
    MELT_LOCATION ("warmelt-debug.melt:1304:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.DBGI__V3*/ meltfptr[2]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 0, "DBGI_OUT");
  /*_.OUT__V7*/ meltfptr[6] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-debug.melt:1305:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.SELF__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 1, "NAMED_NAME");
  /*_.ONAM__V8*/ meltfptr[7] = slot;
    };
    ;

    {
      MELT_LOCATION ("warmelt-debug.melt:1307:/ locexp");
      meltgc_add_out ((melt_ptr_t) ( /*_.OUT__V7*/ meltfptr[6]), ("^^`"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-debug.melt:1308:/ locexp");
      meltgc_add_out ((melt_ptr_t) ( /*_.OUT__V7*/ meltfptr[6]),
		      melt_string_str ((melt_ptr_t)
				       ( /*_.ONAM__V8*/ meltfptr[7])));
    }
    ;

    {
      MELT_LOCATION ("warmelt-debug.melt:1309:/ locexp");
      meltgc_add_out ((melt_ptr_t) ( /*_.OUT__V7*/ meltfptr[6]), ("|"));
    }
    ;
    MELT_LOCATION ("warmelt-debug.melt:1310:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.DIS__V6*/ meltfptr[4]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 1, "NAMED_NAME");
  /*_.NAMED_NAME__V9*/ meltfptr[8] = slot;
    };
    ;

    {
      /*^locexp */
      meltgc_add_out ((melt_ptr_t) ( /*_.OUT__V7*/ meltfptr[6]),
		      melt_string_str ((melt_ptr_t)
				       ( /*_.NAMED_NAME__V9*/ meltfptr[8])));
    }
    ;

    {
      MELT_LOCATION ("warmelt-debug.melt:1311:/ locexp");
      meltgc_add_out ((melt_ptr_t) ( /*_.OUT__V7*/ meltfptr[6]), ("/"));
    }
    ;
 /*_#OBJ_HASH__L3*/ meltfnum[1] =
      (melt_obj_hash ((melt_ptr_t) ( /*_.SELF__V2*/ meltfptr[1])));;

    {
      MELT_LOCATION ("warmelt-debug.melt:1312:/ locexp");
      meltgc_add_out_hex ((melt_ptr_t) ( /*_.OUT__V7*/ meltfptr[6]),
			  ( /*_#OBJ_HASH__L3*/ meltfnum[1]));
    }
    ;

    MELT_LOCATION ("warmelt-debug.melt:1303:/ clear");
	   /*clear *//*_.DIS__V6*/ meltfptr[4] = 0;
    /*^clear */
	   /*clear *//*_.OUT__V7*/ meltfptr[6] = 0;
    /*^clear */
	   /*clear *//*_.ONAM__V8*/ meltfptr[7] = 0;
    /*^clear */
	   /*clear *//*_.NAMED_NAME__V9*/ meltfptr[8] = 0;
    /*^clear */
	   /*clear *//*_#OBJ_HASH__L3*/ meltfnum[1] = 0;
    MELT_LOCATION ("warmelt-debug.melt:1301:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V4*/ meltfptr[3] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("DBGOUTAGAIN_NAMEDOBJECT_METHOD", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_39_warmelt_debug_DBGOUTAGAIN_NAMEDOBJECT_METHOD_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*noretval */ NULL);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_39_warmelt_debug_DBGOUTAGAIN_NAMEDOBJECT_METHOD */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_40_warmelt_debug_DBGOUTAGAIN_SYMBOL_METHOD (meltclosure_ptr_t
						     meltclosp_,
						     melt_ptr_t
						     meltfirstargp_,
						     const
						     melt_argdescr_cell_t
						     meltxargdescr_[],
						     union meltparam_un *
						     meltxargtab_,
						     const
						     melt_argdescr_cell_t
						     meltxresdescr_[],
						     union meltparam_un *
						     meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_40_warmelt_debug_DBGOUTAGAIN_SYMBOL_METHOD_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_40_warmelt_debug_DBGOUTAGAIN_SYMBOL_METHOD_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 9
    melt_ptr_t mcfr_varptr[9];
#define MELTFRAM_NBVARNUM 5
    long mcfr_varnum[5];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_40_warmelt_debug_DBGOUTAGAIN_SYMBOL_METHOD is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_40_warmelt_debug_DBGOUTAGAIN_SYMBOL_METHOD_st
	 *) meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 9; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_40_warmelt_debug_DBGOUTAGAIN_SYMBOL_METHOD nbval 9*/
  meltfram__.mcfr_nbvar = 9 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("DBGOUTAGAIN_SYMBOL_METHOD", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-debug.melt:1318:/ getarg");
 /*_.SELF__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.DBGI__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.DBGI__V3*/ meltfptr[2])) !=
	      NULL);


  /*getarg#2 */
  /*^getarg */
  if (meltxargdescr_[1] != MELTBPAR_LONG)
    goto lab_endgetargs;
 /*_#DEPTH__L1*/ meltfnum[0] = meltxargtab_[1].meltbp_long;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-debug.melt:1319:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L2*/ meltfnum[1] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.DBGI__V3*/ meltfptr[2]),
			     (melt_ptr_t) (( /*!CLASS_DEBUG_INFORMATION */
					    meltfrout->tabval[0])));;
      MELT_LOCATION ("warmelt-debug.melt:1319:/ cond");
      /*cond */ if ( /*_#IS_A__L2*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V5*/ meltfptr[4] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-debug.melt:1319:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check dbgi"),
				  ("warmelt-debug.melt")
				  ? ("warmelt-debug.melt") : __FILE__,
				  (1319) ? (1319) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V5*/ meltfptr[4] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V4*/ meltfptr[3] = /*_.IFELSE___V5*/ meltfptr[4];;
      /*epilog */

      MELT_LOCATION ("warmelt-debug.melt:1319:/ clear");
	     /*clear *//*_#IS_A__L2*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V5*/ meltfptr[4] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V4*/ meltfptr[3] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-debug.melt:1320:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L3*/ meltfnum[1] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.SELF__V2*/ meltfptr[1]),
			     (melt_ptr_t) (( /*!CLASS_SYMBOL */ meltfrout->
					    tabval[1])));;
      MELT_LOCATION ("warmelt-debug.melt:1320:/ cond");
      /*cond */ if ( /*_#IS_A__L3*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V7*/ meltfptr[6] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-debug.melt:1320:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check self"),
				  ("warmelt-debug.melt")
				  ? ("warmelt-debug.melt") : __FILE__,
				  (1320) ? (1320) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V7*/ meltfptr[6] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V6*/ meltfptr[4] = /*_.IFELSE___V7*/ meltfptr[6];;
      /*epilog */

      MELT_LOCATION ("warmelt-debug.melt:1320:/ clear");
	     /*clear *//*_#IS_A__L3*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V7*/ meltfptr[6] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V6*/ meltfptr[4] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-debug.melt:1321:/ quasiblock");


    /*^getslot */
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.DBGI__V3*/ meltfptr[2]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 0, "DBGI_OUT");
  /*_.OUT__V8*/ meltfptr[6] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-debug.melt:1322:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.SELF__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 1, "NAMED_NAME");
  /*_.ONAM__V9*/ meltfptr[8] = slot;
    };
    ;
 /*_#SNUM__L4*/ meltfnum[1] =
      (melt_get_int ((melt_ptr_t) ( /*_.SELF__V2*/ meltfptr[1])));;

    {
      MELT_LOCATION ("warmelt-debug.melt:1325:/ locexp");
      meltgc_add_out ((melt_ptr_t) ( /*_.OUT__V8*/ meltfptr[6]), ("$"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-debug.melt:1326:/ locexp");
      meltgc_add_out ((melt_ptr_t) ( /*_.OUT__V8*/ meltfptr[6]),
		      melt_string_str ((melt_ptr_t)
				       ( /*_.ONAM__V9*/ meltfptr[8])));
    }
    ;

    {
      MELT_LOCATION ("warmelt-debug.melt:1327:/ locexp");
      meltgc_add_out ((melt_ptr_t) ( /*_.OUT__V8*/ meltfptr[6]), ("/"));
    }
    ;
 /*_#OBJ_HASH__L5*/ meltfnum[4] =
      (melt_obj_hash ((melt_ptr_t) ( /*_.SELF__V2*/ meltfptr[1])));;

    {
      MELT_LOCATION ("warmelt-debug.melt:1328:/ locexp");
      meltgc_add_out_hex ((melt_ptr_t) ( /*_.OUT__V8*/ meltfptr[6]),
			  ( /*_#OBJ_HASH__L5*/ meltfnum[4]));
    }
    ;
    MELT_LOCATION ("warmelt-debug.melt:1329:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^cond */
    /*cond */ if ( /*_#SNUM__L4*/ meltfnum[1])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{


	  {
	    MELT_LOCATION ("warmelt-debug.melt:1331:/ locexp");
	    meltgc_add_out ((melt_ptr_t) ( /*_.OUT__V8*/ meltfptr[6]), ("#"));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-debug.melt:1332:/ locexp");
	    meltgc_add_out_dec ((melt_ptr_t) ( /*_.OUT__V8*/ meltfptr[6]),
				( /*_#SNUM__L4*/ meltfnum[1]));
	  }
	  ;
	  MELT_LOCATION ("warmelt-debug.melt:1330:/ quasiblock");


	  /*epilog */
	}
	;
      }				/*noelse */
    ;

    MELT_LOCATION ("warmelt-debug.melt:1321:/ clear");
	   /*clear *//*_.OUT__V8*/ meltfptr[6] = 0;
    /*^clear */
	   /*clear *//*_.ONAM__V9*/ meltfptr[8] = 0;
    /*^clear */
	   /*clear *//*_#SNUM__L4*/ meltfnum[1] = 0;
    /*^clear */
	   /*clear *//*_#OBJ_HASH__L5*/ meltfnum[4] = 0;
    MELT_LOCATION ("warmelt-debug.melt:1318:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V4*/ meltfptr[3] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V6*/ meltfptr[4] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("DBGOUTAGAIN_SYMBOL_METHOD", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_40_warmelt_debug_DBGOUTAGAIN_SYMBOL_METHOD_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*noretval */ NULL);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_40_warmelt_debug_DBGOUTAGAIN_SYMBOL_METHOD */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_41_warmelt_debug_DBGOUT_SYMBOL_METHOD (meltclosure_ptr_t meltclosp_,
						melt_ptr_t meltfirstargp_,
						const melt_argdescr_cell_t
						meltxargdescr_[],
						union meltparam_un *
						meltxargtab_,
						const melt_argdescr_cell_t
						meltxresdescr_[],
						union meltparam_un *
						meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_41_warmelt_debug_DBGOUT_SYMBOL_METHOD_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_41_warmelt_debug_DBGOUT_SYMBOL_METHOD_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 10
    melt_ptr_t mcfr_varptr[10];
#define MELTFRAM_NBVARNUM 4
    long mcfr_varnum[4];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_41_warmelt_debug_DBGOUT_SYMBOL_METHOD is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_41_warmelt_debug_DBGOUT_SYMBOL_METHOD_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 10; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_41_warmelt_debug_DBGOUT_SYMBOL_METHOD nbval 10*/
  meltfram__.mcfr_nbvar = 10 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("DBGOUT_SYMBOL_METHOD", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-debug.melt:1336:/ getarg");
 /*_.SELF__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.DBGI__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.DBGI__V3*/ meltfptr[2])) !=
	      NULL);


  /*getarg#2 */
  /*^getarg */
  if (meltxargdescr_[1] != MELTBPAR_LONG)
    goto lab_endgetargs;
 /*_#DEPTH__L1*/ meltfnum[0] = meltxargtab_[1].meltbp_long;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-debug.melt:1337:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L2*/ meltfnum[1] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.DBGI__V3*/ meltfptr[2]),
			     (melt_ptr_t) (( /*!CLASS_DEBUG_INFORMATION */
					    meltfrout->tabval[0])));;
      MELT_LOCATION ("warmelt-debug.melt:1337:/ cond");
      /*cond */ if ( /*_#IS_A__L2*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V5*/ meltfptr[4] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-debug.melt:1337:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check dbgi"),
				  ("warmelt-debug.melt")
				  ? ("warmelt-debug.melt") : __FILE__,
				  (1337) ? (1337) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V5*/ meltfptr[4] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V4*/ meltfptr[3] = /*_.IFELSE___V5*/ meltfptr[4];;
      /*epilog */

      MELT_LOCATION ("warmelt-debug.melt:1337:/ clear");
	     /*clear *//*_#IS_A__L2*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V5*/ meltfptr[4] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V4*/ meltfptr[3] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-debug.melt:1338:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L3*/ meltfnum[1] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.SELF__V2*/ meltfptr[1]),
			     (melt_ptr_t) (( /*!CLASS_SYMBOL */ meltfrout->
					    tabval[1])));;
      MELT_LOCATION ("warmelt-debug.melt:1338:/ cond");
      /*cond */ if ( /*_#IS_A__L3*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V7*/ meltfptr[6] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-debug.melt:1338:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check self"),
				  ("warmelt-debug.melt")
				  ? ("warmelt-debug.melt") : __FILE__,
				  (1338) ? (1338) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V7*/ meltfptr[6] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V6*/ meltfptr[4] = /*_.IFELSE___V7*/ meltfptr[6];;
      /*epilog */

      MELT_LOCATION ("warmelt-debug.melt:1338:/ clear");
	     /*clear *//*_#IS_A__L3*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V7*/ meltfptr[6] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V6*/ meltfptr[4] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-debug.melt:1339:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#I__L4*/ meltfnum[1] =
      (( /*_#DEPTH__L1*/ meltfnum[0]) <= (0));;
    MELT_LOCATION ("warmelt-debug.melt:1339:/ cond");
    /*cond */ if ( /*_#I__L4*/ meltfnum[1])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-debug.melt:1340:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^apply */
	  /*apply */
	  {
	    union meltparam_un argtab[2];
	    memset (&argtab, 0, sizeof (argtab));
	    /*^apply.arg */
	    argtab[0].meltbp_aptr =
	      (melt_ptr_t *) & /*_.DBGI__V3*/ meltfptr[2];
	    /*^apply.arg */
	    argtab[1].meltbp_long = 0;
	    /*_.DBGOUT_NAMEDOBJECT_METHOD__V9*/ meltfptr[8] =
	      melt_apply ((meltclosure_ptr_t)
			  (( /*!DBGOUT_NAMEDOBJECT_METHOD */ meltfrout->
			    tabval[2])),
			  (melt_ptr_t) ( /*_.SELF__V2*/ meltfptr[1]),
			  (MELTBPARSTR_PTR MELTBPARSTR_LONG ""), argtab, "",
			  (union meltparam_un *) 0);
	  }
	  ;
	  /*_.IFELSE___V8*/ meltfptr[6] =
	    /*_.DBGOUT_NAMEDOBJECT_METHOD__V9*/ meltfptr[8];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-debug.melt:1339:/ clear");
	     /*clear *//*_.DBGOUT_NAMEDOBJECT_METHOD__V9*/ meltfptr[8] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-debug.melt:1341:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^apply */
	  /*apply */
	  {
	    union meltparam_un argtab[2];
	    memset (&argtab, 0, sizeof (argtab));
	    /*^apply.arg */
	    argtab[0].meltbp_aptr =
	      (melt_ptr_t *) & /*_.DBGI__V3*/ meltfptr[2];
	    /*^apply.arg */
	    argtab[1].meltbp_long = /*_#DEPTH__L1*/ meltfnum[0];
	    /*_.DBGOUTAGAIN_SYMBOL_METHOD__V10*/ meltfptr[8] =
	      melt_apply ((meltclosure_ptr_t)
			  (( /*!DBGOUTAGAIN_SYMBOL_METHOD */ meltfrout->
			    tabval[3])),
			  (melt_ptr_t) ( /*_.SELF__V2*/ meltfptr[1]),
			  (MELTBPARSTR_PTR MELTBPARSTR_LONG ""), argtab, "",
			  (union meltparam_un *) 0);
	  }
	  ;
	  /*_.IFELSE___V8*/ meltfptr[6] =
	    /*_.DBGOUTAGAIN_SYMBOL_METHOD__V10*/ meltfptr[8];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-debug.melt:1339:/ clear");
	     /*clear *//*_.DBGOUTAGAIN_SYMBOL_METHOD__V10*/ meltfptr[8] = 0;
	}
	;
      }
    ;
    MELT_LOCATION ("warmelt-debug.melt:1336:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.IFELSE___V8*/ meltfptr[6];;

    {
      MELT_LOCATION ("warmelt-debug.melt:1336:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V4*/ meltfptr[3] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V6*/ meltfptr[4] = 0;
    /*^clear */
	   /*clear *//*_#I__L4*/ meltfnum[1] = 0;
    /*^clear */
	   /*clear *//*_.IFELSE___V8*/ meltfptr[6] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("DBGOUT_SYMBOL_METHOD", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_41_warmelt_debug_DBGOUT_SYMBOL_METHOD_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_41_warmelt_debug_DBGOUT_SYMBOL_METHOD */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_42_warmelt_debug_DBGOUT_CLASS_METHOD (meltclosure_ptr_t meltclosp_,
					       melt_ptr_t meltfirstargp_,
					       const melt_argdescr_cell_t
					       meltxargdescr_[],
					       union meltparam_un *
					       meltxargtab_,
					       const melt_argdescr_cell_t
					       meltxresdescr_[],
					       union meltparam_un *
					       meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_42_warmelt_debug_DBGOUT_CLASS_METHOD_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_42_warmelt_debug_DBGOUT_CLASS_METHOD_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 10
    melt_ptr_t mcfr_varptr[10];
#define MELTFRAM_NBVARNUM 4
    long mcfr_varnum[4];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_42_warmelt_debug_DBGOUT_CLASS_METHOD is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_42_warmelt_debug_DBGOUT_CLASS_METHOD_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 10; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_42_warmelt_debug_DBGOUT_CLASS_METHOD nbval 10*/
  meltfram__.mcfr_nbvar = 10 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("DBGOUT_CLASS_METHOD", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-debug.melt:1346:/ getarg");
 /*_.SELF__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.DBGI__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.DBGI__V3*/ meltfptr[2])) !=
	      NULL);


  /*getarg#2 */
  /*^getarg */
  if (meltxargdescr_[1] != MELTBPAR_LONG)
    goto lab_endgetargs;
 /*_#DEPTH__L1*/ meltfnum[0] = meltxargtab_[1].meltbp_long;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-debug.melt:1347:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L2*/ meltfnum[1] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.DBGI__V3*/ meltfptr[2]),
			     (melt_ptr_t) (( /*!CLASS_DEBUG_INFORMATION */
					    meltfrout->tabval[0])));;
      MELT_LOCATION ("warmelt-debug.melt:1347:/ cond");
      /*cond */ if ( /*_#IS_A__L2*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V5*/ meltfptr[4] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-debug.melt:1347:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check dbgi"),
				  ("warmelt-debug.melt")
				  ? ("warmelt-debug.melt") : __FILE__,
				  (1347) ? (1347) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V5*/ meltfptr[4] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V4*/ meltfptr[3] = /*_.IFELSE___V5*/ meltfptr[4];;
      /*epilog */

      MELT_LOCATION ("warmelt-debug.melt:1347:/ clear");
	     /*clear *//*_#IS_A__L2*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V5*/ meltfptr[4] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V4*/ meltfptr[3] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-debug.melt:1348:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L3*/ meltfnum[1] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.SELF__V2*/ meltfptr[1]),
			     (melt_ptr_t) (( /*!CLASS_CLASS */ meltfrout->
					    tabval[1])));;
      MELT_LOCATION ("warmelt-debug.melt:1348:/ cond");
      /*cond */ if ( /*_#IS_A__L3*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V7*/ meltfptr[6] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-debug.melt:1348:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check self"),
				  ("warmelt-debug.melt")
				  ? ("warmelt-debug.melt") : __FILE__,
				  (1348) ? (1348) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V7*/ meltfptr[6] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V6*/ meltfptr[4] = /*_.IFELSE___V7*/ meltfptr[6];;
      /*epilog */

      MELT_LOCATION ("warmelt-debug.melt:1348:/ clear");
	     /*clear *//*_#IS_A__L3*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V7*/ meltfptr[6] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V6*/ meltfptr[4] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-debug.melt:1349:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#I__L4*/ meltfnum[1] =
      (( /*_#DEPTH__L1*/ meltfnum[0]) <= (0));;
    MELT_LOCATION ("warmelt-debug.melt:1349:/ cond");
    /*cond */ if ( /*_#I__L4*/ meltfnum[1])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-debug.melt:1350:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^apply */
	  /*apply */
	  {
	    union meltparam_un argtab[2];
	    memset (&argtab, 0, sizeof (argtab));
	    /*^apply.arg */
	    argtab[0].meltbp_aptr =
	      (melt_ptr_t *) & /*_.DBGI__V3*/ meltfptr[2];
	    /*^apply.arg */
	    argtab[1].meltbp_long = 0;
	    /*_.DBGOUT_NAMEDOBJECT_METHOD__V9*/ meltfptr[8] =
	      melt_apply ((meltclosure_ptr_t)
			  (( /*!DBGOUT_NAMEDOBJECT_METHOD */ meltfrout->
			    tabval[2])),
			  (melt_ptr_t) ( /*_.SELF__V2*/ meltfptr[1]),
			  (MELTBPARSTR_PTR MELTBPARSTR_LONG ""), argtab, "",
			  (union meltparam_un *) 0);
	  }
	  ;
	  /*_.IFELSE___V8*/ meltfptr[6] =
	    /*_.DBGOUT_NAMEDOBJECT_METHOD__V9*/ meltfptr[8];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-debug.melt:1349:/ clear");
	     /*clear *//*_.DBGOUT_NAMEDOBJECT_METHOD__V9*/ meltfptr[8] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-debug.melt:1351:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^apply */
	  /*apply */
	  {
	    union meltparam_un argtab[2];
	    memset (&argtab, 0, sizeof (argtab));
	    /*^apply.arg */
	    argtab[0].meltbp_aptr =
	      (melt_ptr_t *) & /*_.DBGI__V3*/ meltfptr[2];
	    /*^apply.arg */
	    argtab[1].meltbp_long = /*_#DEPTH__L1*/ meltfnum[0];
	    /*_.DBGOUTAGAIN_NAMEDOBJECT_METHOD__V10*/ meltfptr[8] =
	      melt_apply ((meltclosure_ptr_t)
			  (( /*!DBGOUTAGAIN_NAMEDOBJECT_METHOD */ meltfrout->
			    tabval[3])),
			  (melt_ptr_t) ( /*_.SELF__V2*/ meltfptr[1]),
			  (MELTBPARSTR_PTR MELTBPARSTR_LONG ""), argtab, "",
			  (union meltparam_un *) 0);
	  }
	  ;
	  /*_.IFELSE___V8*/ meltfptr[6] =
	    /*_.DBGOUTAGAIN_NAMEDOBJECT_METHOD__V10*/ meltfptr[8];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-debug.melt:1349:/ clear");
	     /*clear *//*_.DBGOUTAGAIN_NAMEDOBJECT_METHOD__V10*/ meltfptr[8] = 0;
	}
	;
      }
    ;
    MELT_LOCATION ("warmelt-debug.melt:1346:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.IFELSE___V8*/ meltfptr[6];;

    {
      MELT_LOCATION ("warmelt-debug.melt:1346:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V4*/ meltfptr[3] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V6*/ meltfptr[4] = 0;
    /*^clear */
	   /*clear *//*_#I__L4*/ meltfnum[1] = 0;
    /*^clear */
	   /*clear *//*_.IFELSE___V8*/ meltfptr[6] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("DBGOUT_CLASS_METHOD", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_42_warmelt_debug_DBGOUT_CLASS_METHOD_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_42_warmelt_debug_DBGOUT_CLASS_METHOD */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_43_warmelt_debug_DBGOUTAGAIN_KEYWORD_METHOD (meltclosure_ptr_t
						      meltclosp_,
						      melt_ptr_t
						      meltfirstargp_,
						      const
						      melt_argdescr_cell_t
						      meltxargdescr_[],
						      union meltparam_un *
						      meltxargtab_,
						      const
						      melt_argdescr_cell_t
						      meltxresdescr_[],
						      union meltparam_un *
						      meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_43_warmelt_debug_DBGOUTAGAIN_KEYWORD_METHOD_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_43_warmelt_debug_DBGOUTAGAIN_KEYWORD_METHOD_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 9
    melt_ptr_t mcfr_varptr[9];
#define MELTFRAM_NBVARNUM 4
    long mcfr_varnum[4];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_43_warmelt_debug_DBGOUTAGAIN_KEYWORD_METHOD is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_43_warmelt_debug_DBGOUTAGAIN_KEYWORD_METHOD_st
	 *) meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 9; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_43_warmelt_debug_DBGOUTAGAIN_KEYWORD_METHOD nbval 9*/
  meltfram__.mcfr_nbvar = 9 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("DBGOUTAGAIN_KEYWORD_METHOD", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-debug.melt:1356:/ getarg");
 /*_.SELF__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.DBGI__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.DBGI__V3*/ meltfptr[2])) !=
	      NULL);


  /*getarg#2 */
  /*^getarg */
  if (meltxargdescr_[1] != MELTBPAR_LONG)
    goto lab_endgetargs;
 /*_#DEPTH__L1*/ meltfnum[0] = meltxargtab_[1].meltbp_long;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-debug.melt:1357:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L2*/ meltfnum[1] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.DBGI__V3*/ meltfptr[2]),
			     (melt_ptr_t) (( /*!CLASS_DEBUG_INFORMATION */
					    meltfrout->tabval[0])));;
      MELT_LOCATION ("warmelt-debug.melt:1357:/ cond");
      /*cond */ if ( /*_#IS_A__L2*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V5*/ meltfptr[4] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-debug.melt:1357:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check dbgi"),
				  ("warmelt-debug.melt")
				  ? ("warmelt-debug.melt") : __FILE__,
				  (1357) ? (1357) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V5*/ meltfptr[4] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V4*/ meltfptr[3] = /*_.IFELSE___V5*/ meltfptr[4];;
      /*epilog */

      MELT_LOCATION ("warmelt-debug.melt:1357:/ clear");
	     /*clear *//*_#IS_A__L2*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V5*/ meltfptr[4] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V4*/ meltfptr[3] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-debug.melt:1358:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L3*/ meltfnum[1] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.SELF__V2*/ meltfptr[1]),
			     (melt_ptr_t) (( /*!CLASS_KEYWORD */ meltfrout->
					    tabval[1])));;
      MELT_LOCATION ("warmelt-debug.melt:1358:/ cond");
      /*cond */ if ( /*_#IS_A__L3*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V7*/ meltfptr[6] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-debug.melt:1358:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check self"),
				  ("warmelt-debug.melt")
				  ? ("warmelt-debug.melt") : __FILE__,
				  (1358) ? (1358) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V7*/ meltfptr[6] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V6*/ meltfptr[4] = /*_.IFELSE___V7*/ meltfptr[6];;
      /*epilog */

      MELT_LOCATION ("warmelt-debug.melt:1358:/ clear");
	     /*clear *//*_#IS_A__L3*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V7*/ meltfptr[6] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V6*/ meltfptr[4] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-debug.melt:1359:/ quasiblock");


    /*^getslot */
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.DBGI__V3*/ meltfptr[2]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 0, "DBGI_OUT");
  /*_.OUT__V8*/ meltfptr[6] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-debug.melt:1360:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.SELF__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 1, "NAMED_NAME");
  /*_.ONAM__V9*/ meltfptr[8] = slot;
    };
    ;

    {
      MELT_LOCATION ("warmelt-debug.melt:1362:/ locexp");
      meltgc_add_out ((melt_ptr_t) ( /*_.OUT__V8*/ meltfptr[6]), ("$:"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-debug.melt:1363:/ locexp");
      meltgc_add_out ((melt_ptr_t) ( /*_.OUT__V8*/ meltfptr[6]),
		      melt_string_str ((melt_ptr_t)
				       ( /*_.ONAM__V9*/ meltfptr[8])));
    }
    ;

    {
      MELT_LOCATION ("warmelt-debug.melt:1364:/ locexp");
      meltgc_add_out ((melt_ptr_t) ( /*_.OUT__V8*/ meltfptr[6]), ("/"));
    }
    ;
 /*_#OBJ_HASH__L4*/ meltfnum[1] =
      (melt_obj_hash ((melt_ptr_t) ( /*_.SELF__V2*/ meltfptr[1])));;

    {
      MELT_LOCATION ("warmelt-debug.melt:1365:/ locexp");
      meltgc_add_out_hex ((melt_ptr_t) ( /*_.OUT__V8*/ meltfptr[6]),
			  ( /*_#OBJ_HASH__L4*/ meltfnum[1]));
    }
    ;

    MELT_LOCATION ("warmelt-debug.melt:1359:/ clear");
	   /*clear *//*_.OUT__V8*/ meltfptr[6] = 0;
    /*^clear */
	   /*clear *//*_.ONAM__V9*/ meltfptr[8] = 0;
    /*^clear */
	   /*clear *//*_#OBJ_HASH__L4*/ meltfnum[1] = 0;
    MELT_LOCATION ("warmelt-debug.melt:1356:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V4*/ meltfptr[3] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V6*/ meltfptr[4] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("DBGOUTAGAIN_KEYWORD_METHOD", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_43_warmelt_debug_DBGOUTAGAIN_KEYWORD_METHOD_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*noretval */ NULL);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_43_warmelt_debug_DBGOUTAGAIN_KEYWORD_METHOD */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_44_warmelt_debug_DBGOUT_KEYWORD_METHOD (meltclosure_ptr_t meltclosp_,
						 melt_ptr_t meltfirstargp_,
						 const melt_argdescr_cell_t
						 meltxargdescr_[],
						 union meltparam_un *
						 meltxargtab_,
						 const melt_argdescr_cell_t
						 meltxresdescr_[],
						 union meltparam_un *
						 meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_44_warmelt_debug_DBGOUT_KEYWORD_METHOD_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_44_warmelt_debug_DBGOUT_KEYWORD_METHOD_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 10
    melt_ptr_t mcfr_varptr[10];
#define MELTFRAM_NBVARNUM 4
    long mcfr_varnum[4];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_44_warmelt_debug_DBGOUT_KEYWORD_METHOD is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_44_warmelt_debug_DBGOUT_KEYWORD_METHOD_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 10; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_44_warmelt_debug_DBGOUT_KEYWORD_METHOD nbval 10*/
  meltfram__.mcfr_nbvar = 10 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("DBGOUT_KEYWORD_METHOD", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-debug.melt:1369:/ getarg");
 /*_.SELF__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.DBGI__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.DBGI__V3*/ meltfptr[2])) !=
	      NULL);


  /*getarg#2 */
  /*^getarg */
  if (meltxargdescr_[1] != MELTBPAR_LONG)
    goto lab_endgetargs;
 /*_#DEPTH__L1*/ meltfnum[0] = meltxargtab_[1].meltbp_long;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-debug.melt:1370:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L2*/ meltfnum[1] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.DBGI__V3*/ meltfptr[2]),
			     (melt_ptr_t) (( /*!CLASS_DEBUG_INFORMATION */
					    meltfrout->tabval[0])));;
      MELT_LOCATION ("warmelt-debug.melt:1370:/ cond");
      /*cond */ if ( /*_#IS_A__L2*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V5*/ meltfptr[4] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-debug.melt:1370:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check dbgi"),
				  ("warmelt-debug.melt")
				  ? ("warmelt-debug.melt") : __FILE__,
				  (1370) ? (1370) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V5*/ meltfptr[4] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V4*/ meltfptr[3] = /*_.IFELSE___V5*/ meltfptr[4];;
      /*epilog */

      MELT_LOCATION ("warmelt-debug.melt:1370:/ clear");
	     /*clear *//*_#IS_A__L2*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V5*/ meltfptr[4] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V4*/ meltfptr[3] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-debug.melt:1371:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L3*/ meltfnum[1] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.SELF__V2*/ meltfptr[1]),
			     (melt_ptr_t) (( /*!CLASS_KEYWORD */ meltfrout->
					    tabval[1])));;
      MELT_LOCATION ("warmelt-debug.melt:1371:/ cond");
      /*cond */ if ( /*_#IS_A__L3*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V7*/ meltfptr[6] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-debug.melt:1371:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check self"),
				  ("warmelt-debug.melt")
				  ? ("warmelt-debug.melt") : __FILE__,
				  (1371) ? (1371) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V7*/ meltfptr[6] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V6*/ meltfptr[4] = /*_.IFELSE___V7*/ meltfptr[6];;
      /*epilog */

      MELT_LOCATION ("warmelt-debug.melt:1371:/ clear");
	     /*clear *//*_#IS_A__L3*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V7*/ meltfptr[6] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V6*/ meltfptr[4] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-debug.melt:1372:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#I__L4*/ meltfnum[1] =
      (( /*_#DEPTH__L1*/ meltfnum[0]) <= (0));;
    MELT_LOCATION ("warmelt-debug.melt:1372:/ cond");
    /*cond */ if ( /*_#I__L4*/ meltfnum[1])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-debug.melt:1373:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^apply */
	  /*apply */
	  {
	    union meltparam_un argtab[2];
	    memset (&argtab, 0, sizeof (argtab));
	    /*^apply.arg */
	    argtab[0].meltbp_aptr =
	      (melt_ptr_t *) & /*_.DBGI__V3*/ meltfptr[2];
	    /*^apply.arg */
	    argtab[1].meltbp_long = 0;
	    /*_.DBGOUT_NAMEDOBJECT_METHOD__V9*/ meltfptr[8] =
	      melt_apply ((meltclosure_ptr_t)
			  (( /*!DBGOUT_NAMEDOBJECT_METHOD */ meltfrout->
			    tabval[2])),
			  (melt_ptr_t) ( /*_.SELF__V2*/ meltfptr[1]),
			  (MELTBPARSTR_PTR MELTBPARSTR_LONG ""), argtab, "",
			  (union meltparam_un *) 0);
	  }
	  ;
	  /*_.IFELSE___V8*/ meltfptr[6] =
	    /*_.DBGOUT_NAMEDOBJECT_METHOD__V9*/ meltfptr[8];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-debug.melt:1372:/ clear");
	     /*clear *//*_.DBGOUT_NAMEDOBJECT_METHOD__V9*/ meltfptr[8] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-debug.melt:1374:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^apply */
	  /*apply */
	  {
	    union meltparam_un argtab[2];
	    memset (&argtab, 0, sizeof (argtab));
	    /*^apply.arg */
	    argtab[0].meltbp_aptr =
	      (melt_ptr_t *) & /*_.DBGI__V3*/ meltfptr[2];
	    /*^apply.arg */
	    argtab[1].meltbp_long = /*_#DEPTH__L1*/ meltfnum[0];
	    /*_.DBGOUTAGAIN_KEYWORD_METHOD__V10*/ meltfptr[8] =
	      melt_apply ((meltclosure_ptr_t)
			  (( /*!DBGOUTAGAIN_KEYWORD_METHOD */ meltfrout->
			    tabval[3])),
			  (melt_ptr_t) ( /*_.SELF__V2*/ meltfptr[1]),
			  (MELTBPARSTR_PTR MELTBPARSTR_LONG ""), argtab, "",
			  (union meltparam_un *) 0);
	  }
	  ;
	  /*_.IFELSE___V8*/ meltfptr[6] =
	    /*_.DBGOUTAGAIN_KEYWORD_METHOD__V10*/ meltfptr[8];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-debug.melt:1372:/ clear");
	     /*clear *//*_.DBGOUTAGAIN_KEYWORD_METHOD__V10*/ meltfptr[8] = 0;
	}
	;
      }
    ;
    MELT_LOCATION ("warmelt-debug.melt:1369:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.IFELSE___V8*/ meltfptr[6];;

    {
      MELT_LOCATION ("warmelt-debug.melt:1369:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V4*/ meltfptr[3] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V6*/ meltfptr[4] = 0;
    /*^clear */
	   /*clear *//*_#I__L4*/ meltfnum[1] = 0;
    /*^clear */
	   /*clear *//*_.IFELSE___V8*/ meltfptr[6] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("DBGOUT_KEYWORD_METHOD", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_44_warmelt_debug_DBGOUT_KEYWORD_METHOD_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_44_warmelt_debug_DBGOUT_KEYWORD_METHOD */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_45_warmelt_debug_DBGOUTAGAIN_CLONED_SYMBOL_METHOD (meltclosure_ptr_t
							    meltclosp_,
							    melt_ptr_t
							    meltfirstargp_,
							    const
							    melt_argdescr_cell_t
							    meltxargdescr_[],
							    union meltparam_un
							    * meltxargtab_,
							    const
							    melt_argdescr_cell_t
							    meltxresdescr_[],
							    union meltparam_un
							    * meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_45_warmelt_debug_DBGOUTAGAIN_CLONED_SYMBOL_METHOD_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_45_warmelt_debug_DBGOUTAGAIN_CLONED_SYMBOL_METHOD_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 7
    melt_ptr_t mcfr_varptr[7];
#define MELTFRAM_NBVARNUM 3
    long mcfr_varnum[3];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_45_warmelt_debug_DBGOUTAGAIN_CLONED_SYMBOL_METHOD is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct
	 frame_meltrout_45_warmelt_debug_DBGOUTAGAIN_CLONED_SYMBOL_METHOD_st
	 *) meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 7; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_45_warmelt_debug_DBGOUTAGAIN_CLONED_SYMBOL_METHOD nbval 7*/
  meltfram__.mcfr_nbvar = 7 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("DBGOUTAGAIN_CLONED_SYMBOL_METHOD", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-debug.melt:1379:/ getarg");
 /*_.SELF__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.DBGI__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.DBGI__V3*/ meltfptr[2])) !=
	      NULL);


  /*getarg#2 */
  /*^getarg */
  if (meltxargdescr_[1] != MELTBPAR_LONG)
    goto lab_endgetargs;
 /*_#DEPTH__L1*/ meltfnum[0] = meltxargtab_[1].meltbp_long;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {

    MELT_LOCATION ("warmelt-debug.melt:1380:/ quasiblock");


 /*_.DIS__V4*/ meltfptr[3] =
      ((melt_ptr_t)
       (melt_discr ((melt_ptr_t) ( /*_.SELF__V2*/ meltfptr[1]))));;
    MELT_LOCATION ("warmelt-debug.melt:1381:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.DBGI__V3*/ meltfptr[2]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 0, "DBGI_OUT");
  /*_.OUT__V5*/ meltfptr[4] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-debug.melt:1382:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.SELF__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 3, "CSYM_URANK");
  /*_.OURANK__V6*/ meltfptr[5] = slot;
    };
    ;
 /*_#LRK__L2*/ meltfnum[1] =
      (melt_get_int ((melt_ptr_t) ( /*_.OURANK__V6*/ meltfptr[5])));;
    MELT_LOCATION ("warmelt-debug.melt:1384:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.SELF__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 1, "NAMED_NAME");
  /*_.ONAM__V7*/ meltfptr[6] = slot;
    };
    ;

    {
      MELT_LOCATION ("warmelt-debug.melt:1385:/ locexp");
      meltgc_add_out ((melt_ptr_t) ( /*_.OUT__V5*/ meltfptr[4]), ("$$"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-debug.melt:1386:/ locexp");
      meltgc_add_out ((melt_ptr_t) ( /*_.OUT__V5*/ meltfptr[4]),
		      melt_string_str ((melt_ptr_t)
				       ( /*_.ONAM__V7*/ meltfptr[6])));
    }
    ;

    {
      MELT_LOCATION ("warmelt-debug.melt:1387:/ locexp");
      meltgc_add_out ((melt_ptr_t) ( /*_.OUT__V5*/ meltfptr[4]), (":"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-debug.melt:1388:/ locexp");
      meltgc_add_out_dec ((melt_ptr_t) ( /*_.OUT__V5*/ meltfptr[4]),
			  ( /*_#LRK__L2*/ meltfnum[1]));
    }
    ;

    {
      MELT_LOCATION ("warmelt-debug.melt:1389:/ locexp");
      meltgc_add_out ((melt_ptr_t) ( /*_.OUT__V5*/ meltfptr[4]), ("/"));
    }
    ;
 /*_#OBJ_HASH__L3*/ meltfnum[2] =
      (melt_obj_hash ((melt_ptr_t) ( /*_.SELF__V2*/ meltfptr[1])));;

    {
      MELT_LOCATION ("warmelt-debug.melt:1390:/ locexp");
      meltgc_add_out_hex ((melt_ptr_t) ( /*_.OUT__V5*/ meltfptr[4]),
			  ( /*_#OBJ_HASH__L3*/ meltfnum[2]));
    }
    ;

    MELT_LOCATION ("warmelt-debug.melt:1380:/ clear");
	   /*clear *//*_.DIS__V4*/ meltfptr[3] = 0;
    /*^clear */
	   /*clear *//*_.OUT__V5*/ meltfptr[4] = 0;
    /*^clear */
	   /*clear *//*_.OURANK__V6*/ meltfptr[5] = 0;
    /*^clear */
	   /*clear *//*_#LRK__L2*/ meltfnum[1] = 0;
    /*^clear */
	   /*clear *//*_.ONAM__V7*/ meltfptr[6] = 0;
    /*^clear */
	   /*clear *//*_#OBJ_HASH__L3*/ meltfnum[2] = 0;
    /*epilog */
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("DBGOUTAGAIN_CLONED_SYMBOL_METHOD", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_45_warmelt_debug_DBGOUTAGAIN_CLONED_SYMBOL_METHOD_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*noretval */ NULL);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_45_warmelt_debug_DBGOUTAGAIN_CLONED_SYMBOL_METHOD */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_46_warmelt_debug_DBGOUT_CLONED_SYMBOL_METHOD (meltclosure_ptr_t
						       meltclosp_,
						       melt_ptr_t
						       meltfirstargp_,
						       const
						       melt_argdescr_cell_t
						       meltxargdescr_[],
						       union meltparam_un *
						       meltxargtab_,
						       const
						       melt_argdescr_cell_t
						       meltxresdescr_[],
						       union meltparam_un *
						       meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_46_warmelt_debug_DBGOUT_CLONED_SYMBOL_METHOD_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_46_warmelt_debug_DBGOUT_CLONED_SYMBOL_METHOD_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 10
    melt_ptr_t mcfr_varptr[10];
#define MELTFRAM_NBVARNUM 4
    long mcfr_varnum[4];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_46_warmelt_debug_DBGOUT_CLONED_SYMBOL_METHOD is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_46_warmelt_debug_DBGOUT_CLONED_SYMBOL_METHOD_st
	 *) meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 10; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_46_warmelt_debug_DBGOUT_CLONED_SYMBOL_METHOD nbval 10*/
  meltfram__.mcfr_nbvar = 10 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("DBGOUT_CLONED_SYMBOL_METHOD", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-debug.melt:1394:/ getarg");
 /*_.SELF__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.DBGI__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.DBGI__V3*/ meltfptr[2])) !=
	      NULL);


  /*getarg#2 */
  /*^getarg */
  if (meltxargdescr_[1] != MELTBPAR_LONG)
    goto lab_endgetargs;
 /*_#DEPTH__L1*/ meltfnum[0] = meltxargtab_[1].meltbp_long;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-debug.melt:1395:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L2*/ meltfnum[1] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.DBGI__V3*/ meltfptr[2]),
			     (melt_ptr_t) (( /*!CLASS_DEBUG_INFORMATION */
					    meltfrout->tabval[0])));;
      MELT_LOCATION ("warmelt-debug.melt:1395:/ cond");
      /*cond */ if ( /*_#IS_A__L2*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V5*/ meltfptr[4] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-debug.melt:1395:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check dbgi"),
				  ("warmelt-debug.melt")
				  ? ("warmelt-debug.melt") : __FILE__,
				  (1395) ? (1395) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V5*/ meltfptr[4] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V4*/ meltfptr[3] = /*_.IFELSE___V5*/ meltfptr[4];;
      /*epilog */

      MELT_LOCATION ("warmelt-debug.melt:1395:/ clear");
	     /*clear *//*_#IS_A__L2*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V5*/ meltfptr[4] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V4*/ meltfptr[3] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-debug.melt:1396:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L3*/ meltfnum[1] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.SELF__V2*/ meltfptr[1]),
			     (melt_ptr_t) (( /*!CLASS_CLONED_SYMBOL */
					    meltfrout->tabval[1])));;
      MELT_LOCATION ("warmelt-debug.melt:1396:/ cond");
      /*cond */ if ( /*_#IS_A__L3*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V7*/ meltfptr[6] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-debug.melt:1396:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check self"),
				  ("warmelt-debug.melt")
				  ? ("warmelt-debug.melt") : __FILE__,
				  (1396) ? (1396) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V7*/ meltfptr[6] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V6*/ meltfptr[4] = /*_.IFELSE___V7*/ meltfptr[6];;
      /*epilog */

      MELT_LOCATION ("warmelt-debug.melt:1396:/ clear");
	     /*clear *//*_#IS_A__L3*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V7*/ meltfptr[6] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V6*/ meltfptr[4] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-debug.melt:1397:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#I__L4*/ meltfnum[1] =
      (( /*_#DEPTH__L1*/ meltfnum[0]) <= (0));;
    MELT_LOCATION ("warmelt-debug.melt:1397:/ cond");
    /*cond */ if ( /*_#I__L4*/ meltfnum[1])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-debug.melt:1398:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^apply */
	  /*apply */
	  {
	    union meltparam_un argtab[2];
	    memset (&argtab, 0, sizeof (argtab));
	    /*^apply.arg */
	    argtab[0].meltbp_aptr =
	      (melt_ptr_t *) & /*_.DBGI__V3*/ meltfptr[2];
	    /*^apply.arg */
	    argtab[1].meltbp_long = 0;
	    /*_.DBGOUT_NAMEDOBJECT_METHOD__V9*/ meltfptr[8] =
	      melt_apply ((meltclosure_ptr_t)
			  (( /*!DBGOUT_NAMEDOBJECT_METHOD */ meltfrout->
			    tabval[2])),
			  (melt_ptr_t) ( /*_.SELF__V2*/ meltfptr[1]),
			  (MELTBPARSTR_PTR MELTBPARSTR_LONG ""), argtab, "",
			  (union meltparam_un *) 0);
	  }
	  ;
	  /*_.IFELSE___V8*/ meltfptr[6] =
	    /*_.DBGOUT_NAMEDOBJECT_METHOD__V9*/ meltfptr[8];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-debug.melt:1397:/ clear");
	     /*clear *//*_.DBGOUT_NAMEDOBJECT_METHOD__V9*/ meltfptr[8] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-debug.melt:1399:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^apply */
	  /*apply */
	  {
	    union meltparam_un argtab[2];
	    memset (&argtab, 0, sizeof (argtab));
	    /*^apply.arg */
	    argtab[0].meltbp_aptr =
	      (melt_ptr_t *) & /*_.DBGI__V3*/ meltfptr[2];
	    /*^apply.arg */
	    argtab[1].meltbp_long = /*_#DEPTH__L1*/ meltfnum[0];
	    /*_.DBGOUTAGAIN_CLONED_SYMBOL_METHOD__V10*/ meltfptr[8] =
	      melt_apply ((meltclosure_ptr_t)
			  (( /*!DBGOUTAGAIN_CLONED_SYMBOL_METHOD */
			    meltfrout->tabval[3])),
			  (melt_ptr_t) ( /*_.SELF__V2*/ meltfptr[1]),
			  (MELTBPARSTR_PTR MELTBPARSTR_LONG ""), argtab, "",
			  (union meltparam_un *) 0);
	  }
	  ;
	  /*_.IFELSE___V8*/ meltfptr[6] =
	    /*_.DBGOUTAGAIN_CLONED_SYMBOL_METHOD__V10*/ meltfptr[8];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-debug.melt:1397:/ clear");
	     /*clear *//*_.DBGOUTAGAIN_CLONED_SYMBOL_METHOD__V10*/ meltfptr[8] =
	    0;
	}
	;
      }
    ;
    MELT_LOCATION ("warmelt-debug.melt:1394:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.IFELSE___V8*/ meltfptr[6];;

    {
      MELT_LOCATION ("warmelt-debug.melt:1394:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V4*/ meltfptr[3] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V6*/ meltfptr[4] = 0;
    /*^clear */
	   /*clear *//*_#I__L4*/ meltfnum[1] = 0;
    /*^clear */
	   /*clear *//*_.IFELSE___V8*/ meltfptr[6] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("DBGOUT_CLONED_SYMBOL_METHOD", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_46_warmelt_debug_DBGOUT_CLONED_SYMBOL_METHOD_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_46_warmelt_debug_DBGOUT_CLONED_SYMBOL_METHOD */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_47_warmelt_debug_DBGOUT_ENVIRONMENT_METHOD (meltclosure_ptr_t
						     meltclosp_,
						     melt_ptr_t
						     meltfirstargp_,
						     const
						     melt_argdescr_cell_t
						     meltxargdescr_[],
						     union meltparam_un *
						     meltxargtab_,
						     const
						     melt_argdescr_cell_t
						     meltxresdescr_[],
						     union meltparam_un *
						     meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_47_warmelt_debug_DBGOUT_ENVIRONMENT_METHOD_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_47_warmelt_debug_DBGOUT_ENVIRONMENT_METHOD_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 16
    melt_ptr_t mcfr_varptr[16];
#define MELTFRAM_NBVARNUM 27
    long mcfr_varnum[27];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_47_warmelt_debug_DBGOUT_ENVIRONMENT_METHOD is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_47_warmelt_debug_DBGOUT_ENVIRONMENT_METHOD_st
	 *) meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 16; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_47_warmelt_debug_DBGOUT_ENVIRONMENT_METHOD nbval 16*/
  meltfram__.mcfr_nbvar = 16 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("DBGOUT_ENVIRONMENT_METHOD", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-debug.melt:1405:/ getarg");
 /*_.SELF__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.DBGI__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.DBGI__V3*/ meltfptr[2])) !=
	      NULL);


  /*getarg#2 */
  /*^getarg */
  if (meltxargdescr_[1] != MELTBPAR_LONG)
    goto lab_endgetargs;
 /*_#DEPTH__L1*/ meltfnum[0] = meltxargtab_[1].meltbp_long;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-debug.melt:1406:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L2*/ meltfnum[1] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.DBGI__V3*/ meltfptr[2]),
			     (melt_ptr_t) (( /*!CLASS_DEBUG_INFORMATION */
					    meltfrout->tabval[0])));;
      MELT_LOCATION ("warmelt-debug.melt:1406:/ cond");
      /*cond */ if ( /*_#IS_A__L2*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V5*/ meltfptr[4] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-debug.melt:1406:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check dbgi"),
				  ("warmelt-debug.melt")
				  ? ("warmelt-debug.melt") : __FILE__,
				  (1406) ? (1406) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V5*/ meltfptr[4] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V4*/ meltfptr[3] = /*_.IFELSE___V5*/ meltfptr[4];;
      /*epilog */

      MELT_LOCATION ("warmelt-debug.melt:1406:/ clear");
	     /*clear *//*_#IS_A__L2*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V5*/ meltfptr[4] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V4*/ meltfptr[3] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-debug.melt:1407:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L3*/ meltfnum[1] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.SELF__V2*/ meltfptr[1]),
			     (melt_ptr_t) (( /*!CLASS_ENVIRONMENT */
					    meltfrout->tabval[1])));;
      MELT_LOCATION ("warmelt-debug.melt:1407:/ cond");
      /*cond */ if ( /*_#IS_A__L3*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V7*/ meltfptr[6] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-debug.melt:1407:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check self"),
				  ("warmelt-debug.melt")
				  ? ("warmelt-debug.melt") : __FILE__,
				  (1407) ? (1407) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V7*/ meltfptr[6] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V6*/ meltfptr[4] = /*_.IFELSE___V7*/ meltfptr[6];;
      /*epilog */

      MELT_LOCATION ("warmelt-debug.melt:1407:/ clear");
	     /*clear *//*_#IS_A__L3*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V7*/ meltfptr[6] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V6*/ meltfptr[4] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-debug.melt:1408:/ quasiblock");


 /*_.DIS__V8*/ meltfptr[6] =
      ((melt_ptr_t)
       (melt_discr ((melt_ptr_t) ( /*_.SELF__V2*/ meltfptr[1]))));;
    MELT_LOCATION ("warmelt-debug.melt:1409:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.DBGI__V3*/ meltfptr[2]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 0, "DBGI_OUT");
  /*_.OUT__V9*/ meltfptr[8] = slot;
    };
    ;
 /*_#ONUM__L4*/ meltfnum[1] =
      (melt_obj_num ((melt_ptr_t) ( /*_.SELF__V2*/ meltfptr[1])));;
    MELT_LOCATION ("warmelt-debug.melt:1412:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#__L5*/ meltfnum[4] =
      (( /*_.DIS__V8*/ meltfptr[6]) ==
       (( /*!CLASS_ENVIRONMENT */ meltfrout->tabval[1])));;
    MELT_LOCATION ("warmelt-debug.melt:1412:/ cond");
    /*cond */ if ( /*_#__L5*/ meltfnum[4])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{


	  {
	    MELT_LOCATION ("warmelt-debug.melt:1413:/ locexp");
	    meltgc_add_out ((melt_ptr_t) ( /*_.OUT__V9*/ meltfptr[8]),
			    ("env"));
	  }
	  ;
	  /*epilog */
	}
	;
      }
    else
      {
	MELT_LOCATION ("warmelt-debug.melt:1412:/ cond.else");

	/*^block */
	/*anyblock */
	{


	  {
	    MELT_LOCATION ("warmelt-debug.melt:1415:/ locexp");
	    meltgc_add_out ((melt_ptr_t) ( /*_.OUT__V9*/ meltfptr[8]), ("|"));
	  }
	  ;
	  MELT_LOCATION ("warmelt-debug.melt:1416:/ getslot");
	  {
	    melt_ptr_t slot = NULL, obj = NULL;
	    obj = (melt_ptr_t) ( /*_.DIS__V8*/ meltfptr[6]) /*=obj*/ ;
	    melt_object_get_field (slot, obj, 1, "NAMED_NAME");
    /*_.NAMED_NAME__V10*/ meltfptr[9] = slot;
	  };
	  ;

	  {
	    /*^locexp */
	    meltgc_add_out ((melt_ptr_t) ( /*_.OUT__V9*/ meltfptr[8]),
			    melt_string_str ((melt_ptr_t)
					     ( /*_.NAMED_NAME__V10*/
					      meltfptr[9])));
	  }
	  ;
	  MELT_LOCATION ("warmelt-debug.melt:1414:/ quasiblock");


	  /*epilog */

	  MELT_LOCATION ("warmelt-debug.melt:1412:/ clear");
	     /*clear *//*_.NAMED_NAME__V10*/ meltfptr[9] = 0;
	}
	;
      }
    ;

    {
      MELT_LOCATION ("warmelt-debug.melt:1418:/ locexp");
      meltgc_add_out ((melt_ptr_t) ( /*_.OUT__V9*/ meltfptr[8]), ("/"));
    }
    ;
 /*_#OBJ_HASH__L6*/ meltfnum[5] =
      (melt_obj_hash ((melt_ptr_t) ( /*_.SELF__V2*/ meltfptr[1])));;

    {
      MELT_LOCATION ("warmelt-debug.melt:1419:/ locexp");
      meltgc_add_out_hex ((melt_ptr_t) ( /*_.OUT__V9*/ meltfptr[8]),
			  ( /*_#OBJ_HASH__L6*/ meltfnum[5]));
    }
    ;
    MELT_LOCATION ("warmelt-debug.melt:1420:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^cond */
    /*cond */ if ( /*_#ONUM__L4*/ meltfnum[1])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{


	  {
	    MELT_LOCATION ("warmelt-debug.melt:1422:/ locexp");
	    meltgc_add_out ((melt_ptr_t) ( /*_.OUT__V9*/ meltfptr[8]), ("#"));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-debug.melt:1423:/ locexp");
	    meltgc_add_out_dec ((melt_ptr_t) ( /*_.OUT__V9*/ meltfptr[8]),
				( /*_#ONUM__L4*/ meltfnum[1]));
	  }
	  ;
	  MELT_LOCATION ("warmelt-debug.melt:1421:/ quasiblock");


	  /*epilog */
	}
	;
      }				/*noelse */
    ;

    {
      MELT_LOCATION ("warmelt-debug.melt:1424:/ locexp");
      meltgc_add_out ((melt_ptr_t) ( /*_.OUT__V9*/ meltfptr[8]), ("{"));
    }
    ;
    MELT_LOCATION ("warmelt-debug.melt:1425:/ quasiblock");


 /*_#OFFPREV__L7*/ meltfnum[6] =
      (melt_get_int ((melt_ptr_t) (( /*!ENV_PREV */ meltfrout->tabval[2]))));;
    MELT_LOCATION ("warmelt-debug.melt:1426:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.DBGI__V3*/ meltfptr[2]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 2, "DBGI_MAXDEPTH");
  /*_.DBGI_MAXDEPTH__V11*/ meltfptr[9] = slot;
    };
    ;
 /*_#OLDMAXDEPTH__L8*/ meltfnum[7] =
      (melt_get_int ((melt_ptr_t) ( /*_.DBGI_MAXDEPTH__V11*/ meltfptr[9])));;
    /*^compute */
 /*_#I__L9*/ meltfnum[8] =
      (melt_idiv (( /*_#OLDMAXDEPTH__L8*/ meltfnum[7]), (2)));;
    /*^compute */
 /*_#NEWMAXDEPTH__L10*/ meltfnum[9] =
      (( /*_#I__L9*/ meltfnum[8]) - (1));;
    MELT_LOCATION ("warmelt-debug.melt:1429:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#I__L11*/ meltfnum[10] =
      (( /*_#NEWMAXDEPTH__L10*/ meltfnum[9]) < (0));;
    MELT_LOCATION ("warmelt-debug.melt:1429:/ cond");
    /*cond */ if ( /*_#I__L11*/ meltfnum[10])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  /*^compute */
	  /*_#NEWMAXDEPTH__L10*/ meltfnum[9] = /*_#SETQ___L13*/ meltfnum[12] =
	    0;;
	  /*_#IF___L12*/ meltfnum[11] = /*_#SETQ___L13*/ meltfnum[12];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-debug.melt:1429:/ clear");
	     /*clear *//*_#SETQ___L13*/ meltfnum[12] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

  /*_#IF___L12*/ meltfnum[11] = 0;;
      }
    ;
    MELT_LOCATION ("warmelt-debug.melt:1430:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#MELT_REALLY_NEED_DBGLIM__L14*/ meltfnum[12] =
      ( /*MELT_REALLY_NEED_DBGLIM */
       melt_need_debug_limit ((int) /*_#DEPTH__L1*/ meltfnum[0],
			      (int) /*_#OLDMAXDEPTH__L8*/ meltfnum[7]));;
    MELT_LOCATION ("warmelt-debug.melt:1430:/ cond");
    /*cond */ if ( /*_#MELT_REALLY_NEED_DBGLIM__L14*/ meltfnum[12])	/*then */
      {
	/*^cond.then */
	/*_#OR___L15*/ meltfnum[14] =
	  /*_#MELT_REALLY_NEED_DBGLIM__L14*/ meltfnum[12];;
      }
    else
      {
	MELT_LOCATION ("warmelt-debug.melt:1430:/ cond.else");

	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-debug.melt:1432:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
   /*_#I__L16*/ meltfnum[15] =
	    (( /*_#DEPTH__L1*/ meltfnum[0]) <
	     ( /*_#OLDMAXDEPTH__L8*/ meltfnum[7]));;
	  MELT_LOCATION ("warmelt-debug.melt:1432:/ cond");
	  /*cond */ if ( /*_#I__L16*/ meltfnum[15])	/*then */
	    {
	      /*^cond.then */
	      /*^block */
	      /*anyblock */
	      {

     /*_#IS_A__L18*/ meltfnum[17] =
		  melt_is_instance_of ((melt_ptr_t)
				       ( /*_.DBGI__V3*/ meltfptr[2]),
				       (melt_ptr_t) (( /*!CLASS_DEBUG_OUTPUT_INFORMATION */ meltfrout->tabval[3])));;
		/*^compute */
		/*_#IF___L17*/ meltfnum[16] = /*_#IS_A__L18*/ meltfnum[17];;
		/*epilog */

		MELT_LOCATION ("warmelt-debug.melt:1432:/ clear");
	       /*clear *//*_#IS_A__L18*/ meltfnum[17] = 0;
	      }
	      ;
	    }
	  else
	    {			/*^cond.else */

    /*_#IF___L17*/ meltfnum[16] = 0;;
	    }
	  ;
	  /*^compute */
	  /*_#OR___L15*/ meltfnum[14] = /*_#IF___L17*/ meltfnum[16];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-debug.melt:1430:/ clear");
	     /*clear *//*_#I__L16*/ meltfnum[15] = 0;
	  /*^clear */
	     /*clear *//*_#IF___L17*/ meltfnum[16] = 0;
	}
	;
      }
    ;
    /*^cond */
    /*cond */ if ( /*_#OR___L15*/ meltfnum[14])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-debug.melt:1434:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
   /*_#I__L19*/ meltfnum[17] =
	    (( /*_#DEPTH__L1*/ meltfnum[0]) > (0));;
	  MELT_LOCATION ("warmelt-debug.melt:1434:/ cond");
	  /*cond */ if ( /*_#I__L19*/ meltfnum[17])	/*then */
	    {
	      /*^cond.then */
	      /*^block */
	      /*anyblock */
	      {

		/*^checksignal */
		MELT_CHECK_SIGNAL ();
		;
     /*_#I__L20*/ meltfnum[15] =
		  (( /*_#OLDMAXDEPTH__L8*/ meltfnum[7]) > (3));;
		MELT_LOCATION ("warmelt-debug.melt:1434:/ cond");
		/*cond */ if ( /*_#I__L20*/ meltfnum[15])	/*then */
		  {
		    /*^cond.then */
		    /*^block */
		    /*anyblock */
		    {

		      MELT_LOCATION ("warmelt-debug.melt:1435:/ getslot");
		      {
			melt_ptr_t slot = NULL, obj = NULL;
			obj =
			  (melt_ptr_t) ( /*_.DBGI__V3*/ meltfptr[2]) /*=obj*/
			  ;
			melt_object_get_field (slot, obj, 2, "DBGI_MAXDEPTH");
	/*_.DBGI_MAXDEPTH__V12*/ meltfptr[11] = slot;
		      };
		      ;

		      {
			/*^locexp */
			melt_put_int ((melt_ptr_t)
				      ( /*_.DBGI_MAXDEPTH__V12*/
				       meltfptr[11]),
				      ( /*_#NEWMAXDEPTH__L10*/ meltfnum[9]));
		      }
		      ;
		      /*epilog */

		      MELT_LOCATION ("warmelt-debug.melt:1434:/ clear");
		 /*clear *//*_.DBGI_MAXDEPTH__V12*/ meltfptr[11] = 0;
		    }
		    ;
		  }		/*noelse */
		;
		/*epilog */

		/*^clear */
	       /*clear *//*_#I__L20*/ meltfnum[15] = 0;
	      }
	      ;
	    }			/*noelse */
	  ;
   /*_#I__L21*/ meltfnum[16] =
	    (( /*_#DEPTH__L1*/ meltfnum[0]) + (1));;
	  MELT_LOCATION ("warmelt-debug.melt:1436:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^apply */
	  /*apply */
	  {
	    union meltparam_un argtab[4];
	    memset (&argtab, 0, sizeof (argtab));
	    /*^apply.arg */
	    argtab[0].meltbp_aptr =
	      (melt_ptr_t *) & /*_.DBGI__V3*/ meltfptr[2];
	    /*^apply.arg */
	    argtab[1].meltbp_long = /*_#I__L21*/ meltfnum[16];
	    /*^apply.arg */
	    argtab[2].meltbp_long = 0;
	    /*^apply.arg */
	    argtab[3].meltbp_long = /*_#OFFPREV__L7*/ meltfnum[6];
	    /*_.DBGOUT_FIELDS__V13*/ meltfptr[11] =
	      melt_apply ((meltclosure_ptr_t)
			  (( /*!DBGOUT_FIELDS */ meltfrout->tabval[4])),
			  (melt_ptr_t) ( /*_.SELF__V2*/ meltfptr[1]),
			  (MELTBPARSTR_PTR MELTBPARSTR_LONG MELTBPARSTR_LONG
			   MELTBPARSTR_LONG ""), argtab, "",
			  (union meltparam_un *) 0);
	  }
	  ;
	  MELT_LOCATION ("warmelt-debug.melt:1437:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
   /*_#MELT_REALLY_NEED_DBGLIM__L22*/ meltfnum[15] =
	    ( /*MELT_REALLY_NEED_DBGLIM */
	     melt_need_debug_limit ((int) /*_#DEPTH__L1*/ meltfnum[0],
				    (int) /*_#NEWMAXDEPTH__L10*/
				    meltfnum[9]));;
	  MELT_LOCATION ("warmelt-debug.melt:1437:/ cond");
	  /*cond */ if ( /*_#MELT_REALLY_NEED_DBGLIM__L22*/ meltfnum[15])	/*then */
	    {
	      /*^cond.then */
	      /*_#OR___L23*/ meltfnum[22] =
		/*_#MELT_REALLY_NEED_DBGLIM__L22*/ meltfnum[15];;
	    }
	  else
	    {
	      MELT_LOCATION ("warmelt-debug.melt:1437:/ cond.else");

	      /*^block */
	      /*anyblock */
	      {

		MELT_LOCATION ("warmelt-debug.melt:1439:/ checksignal");
		MELT_CHECK_SIGNAL ();
		;
     /*_#I__L24*/ meltfnum[23] =
		  (( /*_#DEPTH__L1*/ meltfnum[0]) <
		   ( /*_#NEWMAXDEPTH__L10*/ meltfnum[9]));;
		MELT_LOCATION ("warmelt-debug.melt:1439:/ cond");
		/*cond */ if ( /*_#I__L24*/ meltfnum[23])	/*then */
		  {
		    /*^cond.then */
		    /*^block */
		    /*anyblock */
		    {

       /*_#IS_A__L26*/ meltfnum[25] =
			melt_is_instance_of ((melt_ptr_t)
					     ( /*_.DBGI__V3*/ meltfptr[2]),
					     (melt_ptr_t) (( /*!CLASS_DEBUG_OUTPUT_INFORMATION */ meltfrout->tabval[3])));;
		      /*^compute */
		      /*_#IF___L25*/ meltfnum[24] =
			/*_#IS_A__L26*/ meltfnum[25];;
		      /*epilog */

		      MELT_LOCATION ("warmelt-debug.melt:1439:/ clear");
		 /*clear *//*_#IS_A__L26*/ meltfnum[25] = 0;
		    }
		    ;
		  }
		else
		  {		/*^cond.else */

      /*_#IF___L25*/ meltfnum[24] = 0;;
		  }
		;
		/*^compute */
		/*_#OR___L23*/ meltfnum[22] = /*_#IF___L25*/ meltfnum[24];;
		/*epilog */

		MELT_LOCATION ("warmelt-debug.melt:1437:/ clear");
	       /*clear *//*_#I__L24*/ meltfnum[23] = 0;
		/*^clear */
	       /*clear *//*_#IF___L25*/ meltfnum[24] = 0;
	      }
	      ;
	    }
	  ;
	  /*^cond */
	  /*cond */ if ( /*_#OR___L23*/ meltfnum[22])	/*then */
	    {
	      /*^cond.then */
	      /*^block */
	      /*anyblock */
	      {

     /*_#I__L27*/ meltfnum[25] =
		  (( /*_#DEPTH__L1*/ meltfnum[0]) + (2));;
		MELT_LOCATION ("warmelt-debug.melt:1440:/ checksignal");
		MELT_CHECK_SIGNAL ();
		;
		/*^apply */
		/*apply */
		{
		  union meltparam_un argtab[4];
		  memset (&argtab, 0, sizeof (argtab));
		  /*^apply.arg */
		  argtab[0].meltbp_aptr =
		    (melt_ptr_t *) & /*_.DBGI__V3*/ meltfptr[2];
		  /*^apply.arg */
		  argtab[1].meltbp_long = /*_#I__L27*/ meltfnum[25];
		  /*^apply.arg */
		  argtab[2].meltbp_long = /*_#OFFPREV__L7*/ meltfnum[6];
		  /*^apply.arg */
		  argtab[3].meltbp_long = 0;
		  /*_.DBGOUTAGAIN_FIELDS__V15*/ meltfptr[14] =
		    melt_apply ((meltclosure_ptr_t)
				(( /*!DBGOUTAGAIN_FIELDS */ meltfrout->
				  tabval[5])),
				(melt_ptr_t) ( /*_.SELF__V2*/ meltfptr[1]),
				(MELTBPARSTR_PTR MELTBPARSTR_LONG
				 MELTBPARSTR_LONG MELTBPARSTR_LONG ""),
				argtab, "", (union meltparam_un *) 0);
		}
		;
		/*_.IFELSE___V14*/ meltfptr[13] =
		  /*_.DBGOUTAGAIN_FIELDS__V15*/ meltfptr[14];;
		/*epilog */

		MELT_LOCATION ("warmelt-debug.melt:1437:/ clear");
	       /*clear *//*_#I__L27*/ meltfnum[25] = 0;
		/*^clear */
	       /*clear *//*_.DBGOUTAGAIN_FIELDS__V15*/ meltfptr[14] = 0;
	      }
	      ;
	    }
	  else
	    {			/*^cond.else */

	      /*^block */
	      /*anyblock */
	      {




		{
		  MELT_LOCATION ("warmelt-debug.melt:1441:/ locexp");
		  meltgc_add_out ((melt_ptr_t) ( /*_.OUT__V9*/ meltfptr[8]),
				  (".._.."));
		}
		;
	       /*clear *//*_.IFELSE___V14*/ meltfptr[13] = 0;
		/*epilog */
	      }
	      ;
	    }
	  ;
	  MELT_LOCATION ("warmelt-debug.melt:1442:/ getslot");
	  {
	    melt_ptr_t slot = NULL, obj = NULL;
	    obj = (melt_ptr_t) ( /*_.DBGI__V3*/ meltfptr[2]) /*=obj*/ ;
	    melt_object_get_field (slot, obj, 2, "DBGI_MAXDEPTH");
    /*_.DBGI_MAXDEPTH__V16*/ meltfptr[14] = slot;
	  };
	  ;

	  {
	    /*^locexp */
	    melt_put_int ((melt_ptr_t)
			  ( /*_.DBGI_MAXDEPTH__V16*/ meltfptr[14]),
			  ( /*_#OLDMAXDEPTH__L8*/ meltfnum[7]));
	  }
	  ;
	  MELT_LOCATION ("warmelt-debug.melt:1433:/ quasiblock");


	  /*epilog */

	  MELT_LOCATION ("warmelt-debug.melt:1430:/ clear");
	     /*clear *//*_#I__L19*/ meltfnum[17] = 0;
	  /*^clear */
	     /*clear *//*_#I__L21*/ meltfnum[16] = 0;
	  /*^clear */
	     /*clear *//*_.DBGOUT_FIELDS__V13*/ meltfptr[11] = 0;
	  /*^clear */
	     /*clear *//*_#MELT_REALLY_NEED_DBGLIM__L22*/ meltfnum[15] = 0;
	  /*^clear */
	     /*clear *//*_#OR___L23*/ meltfnum[22] = 0;
	  /*^clear */
	     /*clear *//*_.IFELSE___V14*/ meltfptr[13] = 0;
	  /*^clear */
	     /*clear *//*_.DBGI_MAXDEPTH__V16*/ meltfptr[14] = 0;
	}
	;
      }				/*noelse */
    ;

    MELT_LOCATION ("warmelt-debug.melt:1425:/ clear");
	   /*clear *//*_#OFFPREV__L7*/ meltfnum[6] = 0;
    /*^clear */
	   /*clear *//*_.DBGI_MAXDEPTH__V11*/ meltfptr[9] = 0;
    /*^clear */
	   /*clear *//*_#OLDMAXDEPTH__L8*/ meltfnum[7] = 0;
    /*^clear */
	   /*clear *//*_#I__L9*/ meltfnum[8] = 0;
    /*^clear */
	   /*clear *//*_#NEWMAXDEPTH__L10*/ meltfnum[9] = 0;
    /*^clear */
	   /*clear *//*_#I__L11*/ meltfnum[10] = 0;
    /*^clear */
	   /*clear *//*_#IF___L12*/ meltfnum[11] = 0;
    /*^clear */
	   /*clear *//*_#MELT_REALLY_NEED_DBGLIM__L14*/ meltfnum[12] = 0;
    /*^clear */
	   /*clear *//*_#OR___L15*/ meltfnum[14] = 0;

    {
      MELT_LOCATION ("warmelt-debug.melt:1444:/ locexp");
      meltgc_add_out ((melt_ptr_t) ( /*_.OUT__V9*/ meltfptr[8]), ("}"));
    }
    ;

    MELT_LOCATION ("warmelt-debug.melt:1408:/ clear");
	   /*clear *//*_.DIS__V8*/ meltfptr[6] = 0;
    /*^clear */
	   /*clear *//*_.OUT__V9*/ meltfptr[8] = 0;
    /*^clear */
	   /*clear *//*_#ONUM__L4*/ meltfnum[1] = 0;
    /*^clear */
	   /*clear *//*_#__L5*/ meltfnum[4] = 0;
    /*^clear */
	   /*clear *//*_#OBJ_HASH__L6*/ meltfnum[5] = 0;
    MELT_LOCATION ("warmelt-debug.melt:1405:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V4*/ meltfptr[3] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V6*/ meltfptr[4] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("DBGOUT_ENVIRONMENT_METHOD", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_47_warmelt_debug_DBGOUT_ENVIRONMENT_METHOD_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*noretval */ NULL);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_47_warmelt_debug_DBGOUT_ENVIRONMENT_METHOD */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_48_warmelt_debug_DBGOUTAGAIN_CTYPE_METHOD (meltclosure_ptr_t
						    meltclosp_,
						    melt_ptr_t meltfirstargp_,
						    const melt_argdescr_cell_t
						    meltxargdescr_[],
						    union meltparam_un *
						    meltxargtab_,
						    const melt_argdescr_cell_t
						    meltxresdescr_[],
						    union meltparam_un *
						    meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_48_warmelt_debug_DBGOUTAGAIN_CTYPE_METHOD_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_48_warmelt_debug_DBGOUTAGAIN_CTYPE_METHOD_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 6
    melt_ptr_t mcfr_varptr[6];
#define MELTFRAM_NBVARNUM 2
    long mcfr_varnum[2];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_48_warmelt_debug_DBGOUTAGAIN_CTYPE_METHOD is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_48_warmelt_debug_DBGOUTAGAIN_CTYPE_METHOD_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 6; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_48_warmelt_debug_DBGOUTAGAIN_CTYPE_METHOD nbval 6*/
  meltfram__.mcfr_nbvar = 6 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("DBGOUTAGAIN_CTYPE_METHOD", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-debug.melt:1450:/ getarg");
 /*_.SELF__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.DBGI__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.DBGI__V3*/ meltfptr[2])) !=
	      NULL);


  /*getarg#2 */
  /*^getarg */
  if (meltxargdescr_[1] != MELTBPAR_LONG)
    goto lab_endgetargs;
 /*_#DEPTH__L1*/ meltfnum[0] = meltxargtab_[1].meltbp_long;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {

    MELT_LOCATION ("warmelt-debug.melt:1451:/ quasiblock");


 /*_.DIS__V4*/ meltfptr[3] =
      ((melt_ptr_t)
       (melt_discr ((melt_ptr_t) ( /*_.SELF__V2*/ meltfptr[1]))));;
    MELT_LOCATION ("warmelt-debug.melt:1452:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.DBGI__V3*/ meltfptr[2]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 0, "DBGI_OUT");
  /*_.OUT__V5*/ meltfptr[4] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-debug.melt:1453:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.SELF__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 1, "NAMED_NAME");
  /*_.ONAM__V6*/ meltfptr[5] = slot;
    };
    ;

    {
      MELT_LOCATION ("warmelt-debug.melt:1454:/ locexp");
      meltgc_add_out ((melt_ptr_t) ( /*_.OUT__V5*/ meltfptr[4]), (" $!"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-debug.melt:1455:/ locexp");
      meltgc_add_out ((melt_ptr_t) ( /*_.OUT__V5*/ meltfptr[4]),
		      melt_string_str ((melt_ptr_t)
				       ( /*_.ONAM__V6*/ meltfptr[5])));
    }
    ;

    {
      MELT_LOCATION ("warmelt-debug.melt:1456:/ locexp");
      meltgc_add_out ((melt_ptr_t) ( /*_.OUT__V5*/ meltfptr[4]), ("!/"));
    }
    ;
 /*_#OBJ_HASH__L2*/ meltfnum[1] =
      (melt_obj_hash ((melt_ptr_t) ( /*_.SELF__V2*/ meltfptr[1])));;

    {
      MELT_LOCATION ("warmelt-debug.melt:1457:/ locexp");
      meltgc_add_out_hex ((melt_ptr_t) ( /*_.OUT__V5*/ meltfptr[4]),
			  ( /*_#OBJ_HASH__L2*/ meltfnum[1]));
    }
    ;

    MELT_LOCATION ("warmelt-debug.melt:1451:/ clear");
	   /*clear *//*_.DIS__V4*/ meltfptr[3] = 0;
    /*^clear */
	   /*clear *//*_.OUT__V5*/ meltfptr[4] = 0;
    /*^clear */
	   /*clear *//*_.ONAM__V6*/ meltfptr[5] = 0;
    /*^clear */
	   /*clear *//*_#OBJ_HASH__L2*/ meltfnum[1] = 0;
    /*epilog */
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("DBGOUTAGAIN_CTYPE_METHOD", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_48_warmelt_debug_DBGOUTAGAIN_CTYPE_METHOD_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*noretval */ NULL);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_48_warmelt_debug_DBGOUTAGAIN_CTYPE_METHOD */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_49_warmelt_debug_DBGOUT_CTYPE_METHOD (meltclosure_ptr_t meltclosp_,
					       melt_ptr_t meltfirstargp_,
					       const melt_argdescr_cell_t
					       meltxargdescr_[],
					       union meltparam_un *
					       meltxargtab_,
					       const melt_argdescr_cell_t
					       meltxresdescr_[],
					       union meltparam_un *
					       meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_49_warmelt_debug_DBGOUT_CTYPE_METHOD_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_49_warmelt_debug_DBGOUT_CTYPE_METHOD_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 10
    melt_ptr_t mcfr_varptr[10];
#define MELTFRAM_NBVARNUM 4
    long mcfr_varnum[4];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_49_warmelt_debug_DBGOUT_CTYPE_METHOD is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_49_warmelt_debug_DBGOUT_CTYPE_METHOD_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 10; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_49_warmelt_debug_DBGOUT_CTYPE_METHOD nbval 10*/
  meltfram__.mcfr_nbvar = 10 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("DBGOUT_CTYPE_METHOD", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-debug.melt:1462:/ getarg");
 /*_.SELF__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.DBGI__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.DBGI__V3*/ meltfptr[2])) !=
	      NULL);


  /*getarg#2 */
  /*^getarg */
  if (meltxargdescr_[1] != MELTBPAR_LONG)
    goto lab_endgetargs;
 /*_#DEPTH__L1*/ meltfnum[0] = meltxargtab_[1].meltbp_long;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-debug.melt:1463:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L2*/ meltfnum[1] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.DBGI__V3*/ meltfptr[2]),
			     (melt_ptr_t) (( /*!CLASS_DEBUG_INFORMATION */
					    meltfrout->tabval[0])));;
      MELT_LOCATION ("warmelt-debug.melt:1463:/ cond");
      /*cond */ if ( /*_#IS_A__L2*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V5*/ meltfptr[4] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-debug.melt:1463:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check dbgi"),
				  ("warmelt-debug.melt")
				  ? ("warmelt-debug.melt") : __FILE__,
				  (1463) ? (1463) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V5*/ meltfptr[4] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V4*/ meltfptr[3] = /*_.IFELSE___V5*/ meltfptr[4];;
      /*epilog */

      MELT_LOCATION ("warmelt-debug.melt:1463:/ clear");
	     /*clear *//*_#IS_A__L2*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V5*/ meltfptr[4] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V4*/ meltfptr[3] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-debug.melt:1464:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L3*/ meltfnum[1] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.SELF__V2*/ meltfptr[1]),
			     (melt_ptr_t) (( /*!CLASS_CTYPE */ meltfrout->
					    tabval[1])));;
      MELT_LOCATION ("warmelt-debug.melt:1464:/ cond");
      /*cond */ if ( /*_#IS_A__L3*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V7*/ meltfptr[6] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-debug.melt:1464:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check self"),
				  ("warmelt-debug.melt")
				  ? ("warmelt-debug.melt") : __FILE__,
				  (1464) ? (1464) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V7*/ meltfptr[6] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V6*/ meltfptr[4] = /*_.IFELSE___V7*/ meltfptr[6];;
      /*epilog */

      MELT_LOCATION ("warmelt-debug.melt:1464:/ clear");
	     /*clear *//*_#IS_A__L3*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V7*/ meltfptr[6] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V6*/ meltfptr[4] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-debug.melt:1465:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#I__L4*/ meltfnum[1] =
      (( /*_#DEPTH__L1*/ meltfnum[0]) <= (0));;
    MELT_LOCATION ("warmelt-debug.melt:1465:/ cond");
    /*cond */ if ( /*_#I__L4*/ meltfnum[1])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-debug.melt:1466:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^apply */
	  /*apply */
	  {
	    union meltparam_un argtab[2];
	    memset (&argtab, 0, sizeof (argtab));
	    /*^apply.arg */
	    argtab[0].meltbp_aptr =
	      (melt_ptr_t *) & /*_.DBGI__V3*/ meltfptr[2];
	    /*^apply.arg */
	    argtab[1].meltbp_long = 0;
	    /*_.DBGOUT_NAMEDOBJECT_METHOD__V9*/ meltfptr[8] =
	      melt_apply ((meltclosure_ptr_t)
			  (( /*!DBGOUT_NAMEDOBJECT_METHOD */ meltfrout->
			    tabval[2])),
			  (melt_ptr_t) ( /*_.SELF__V2*/ meltfptr[1]),
			  (MELTBPARSTR_PTR MELTBPARSTR_LONG ""), argtab, "",
			  (union meltparam_un *) 0);
	  }
	  ;
	  /*_.IFELSE___V8*/ meltfptr[6] =
	    /*_.DBGOUT_NAMEDOBJECT_METHOD__V9*/ meltfptr[8];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-debug.melt:1465:/ clear");
	     /*clear *//*_.DBGOUT_NAMEDOBJECT_METHOD__V9*/ meltfptr[8] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-debug.melt:1467:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^apply */
	  /*apply */
	  {
	    union meltparam_un argtab[2];
	    memset (&argtab, 0, sizeof (argtab));
	    /*^apply.arg */
	    argtab[0].meltbp_aptr =
	      (melt_ptr_t *) & /*_.DBGI__V3*/ meltfptr[2];
	    /*^apply.arg */
	    argtab[1].meltbp_long = /*_#DEPTH__L1*/ meltfnum[0];
	    /*_.DBGOUTAGAIN_CTYPE_METHOD__V10*/ meltfptr[8] =
	      melt_apply ((meltclosure_ptr_t)
			  (( /*!DBGOUTAGAIN_CTYPE_METHOD */ meltfrout->
			    tabval[3])),
			  (melt_ptr_t) ( /*_.SELF__V2*/ meltfptr[1]),
			  (MELTBPARSTR_PTR MELTBPARSTR_LONG ""), argtab, "",
			  (union meltparam_un *) 0);
	  }
	  ;
	  /*_.IFELSE___V8*/ meltfptr[6] =
	    /*_.DBGOUTAGAIN_CTYPE_METHOD__V10*/ meltfptr[8];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-debug.melt:1465:/ clear");
	     /*clear *//*_.DBGOUTAGAIN_CTYPE_METHOD__V10*/ meltfptr[8] = 0;
	}
	;
      }
    ;
    MELT_LOCATION ("warmelt-debug.melt:1462:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.IFELSE___V8*/ meltfptr[6];;

    {
      MELT_LOCATION ("warmelt-debug.melt:1462:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V4*/ meltfptr[3] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V6*/ meltfptr[4] = 0;
    /*^clear */
	   /*clear *//*_#I__L4*/ meltfnum[1] = 0;
    /*^clear */
	   /*clear *//*_.IFELSE___V8*/ meltfptr[6] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("DBGOUT_CTYPE_METHOD", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_49_warmelt_debug_DBGOUT_CTYPE_METHOD_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_49_warmelt_debug_DBGOUT_CTYPE_METHOD */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_50_warmelt_debug_DBGOUT_ANYBINDING_METHOD (meltclosure_ptr_t
						    meltclosp_,
						    melt_ptr_t meltfirstargp_,
						    const melt_argdescr_cell_t
						    meltxargdescr_[],
						    union meltparam_un *
						    meltxargtab_,
						    const melt_argdescr_cell_t
						    meltxresdescr_[],
						    union meltparam_un *
						    meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_50_warmelt_debug_DBGOUT_ANYBINDING_METHOD_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_50_warmelt_debug_DBGOUT_ANYBINDING_METHOD_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 16
    melt_ptr_t mcfr_varptr[16];
#define MELTFRAM_NBVARNUM 13
    long mcfr_varnum[13];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_50_warmelt_debug_DBGOUT_ANYBINDING_METHOD is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_50_warmelt_debug_DBGOUT_ANYBINDING_METHOD_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 16; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_50_warmelt_debug_DBGOUT_ANYBINDING_METHOD nbval 16*/
  meltfram__.mcfr_nbvar = 16 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("DBGOUT_ANYBINDING_METHOD", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-debug.melt:1472:/ getarg");
 /*_.SELF__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.DBGI__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.DBGI__V3*/ meltfptr[2])) !=
	      NULL);


  /*getarg#2 */
  /*^getarg */
  if (meltxargdescr_[1] != MELTBPAR_LONG)
    goto lab_endgetargs;
 /*_#DEPTH__L1*/ meltfnum[0] = meltxargtab_[1].meltbp_long;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-debug.melt:1473:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L2*/ meltfnum[1] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.DBGI__V3*/ meltfptr[2]),
			     (melt_ptr_t) (( /*!CLASS_DEBUG_INFORMATION */
					    meltfrout->tabval[0])));;
      MELT_LOCATION ("warmelt-debug.melt:1473:/ cond");
      /*cond */ if ( /*_#IS_A__L2*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V5*/ meltfptr[4] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-debug.melt:1473:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check dbgi"),
				  ("warmelt-debug.melt")
				  ? ("warmelt-debug.melt") : __FILE__,
				  (1473) ? (1473) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V5*/ meltfptr[4] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V4*/ meltfptr[3] = /*_.IFELSE___V5*/ meltfptr[4];;
      /*epilog */

      MELT_LOCATION ("warmelt-debug.melt:1473:/ clear");
	     /*clear *//*_#IS_A__L2*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V5*/ meltfptr[4] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V4*/ meltfptr[3] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-debug.melt:1474:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L3*/ meltfnum[1] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.SELF__V2*/ meltfptr[1]),
			     (melt_ptr_t) (( /*!CLASS_ANY_BINDING */
					    meltfrout->tabval[1])));;
      MELT_LOCATION ("warmelt-debug.melt:1474:/ cond");
      /*cond */ if ( /*_#IS_A__L3*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V7*/ meltfptr[6] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-debug.melt:1474:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check self"),
				  ("warmelt-debug.melt")
				  ? ("warmelt-debug.melt") : __FILE__,
				  (1474) ? (1474) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V7*/ meltfptr[6] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V6*/ meltfptr[4] = /*_.IFELSE___V7*/ meltfptr[6];;
      /*epilog */

      MELT_LOCATION ("warmelt-debug.melt:1474:/ clear");
	     /*clear *//*_#IS_A__L3*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V7*/ meltfptr[6] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V6*/ meltfptr[4] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-debug.melt:1475:/ quasiblock");


 /*_.DIS__V8*/ meltfptr[6] =
      ((melt_ptr_t)
       (melt_discr ((melt_ptr_t) ( /*_.SELF__V2*/ meltfptr[1]))));;
    MELT_LOCATION ("warmelt-debug.melt:1476:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.DBGI__V3*/ meltfptr[2]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 0, "DBGI_OUT");
  /*_.OUT__V9*/ meltfptr[8] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-debug.melt:1477:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.SELF__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 0, "BINDER");
  /*_.BINDERV__V10*/ meltfptr[9] = slot;
    };
    ;
 /*_#BINDNUM__L4*/ meltfnum[1] =
      (melt_get_int ((melt_ptr_t) ( /*_.SELF__V2*/ meltfptr[1])));;
    MELT_LOCATION ("warmelt-debug.melt:1479:/ cond");
    /*cond */ if (
		   /*ifisa */
		   melt_is_instance_of ((melt_ptr_t)
					( /*_.DBGI__V3*/ meltfptr[2]),
					(melt_ptr_t) (( /*!CLASS_DEBUG_INFORMATION */ meltfrout->tabval[0])))
      )				/*then */
      {
	/*^cond.then */
	/*^getslot */
	{
	  melt_ptr_t slot = NULL, obj = NULL;
	  obj = (melt_ptr_t) ( /*_.DBGI__V3*/ meltfptr[2]) /*=obj*/ ;
	  melt_object_get_field (slot, obj, 2, "DBGI_MAXDEPTH");
   /*_.DBGI_MAXDEPTH__V11*/ meltfptr[10] = slot;
	};
	;
      }
    else
      {				/*^cond.else */

  /*_.DBGI_MAXDEPTH__V11*/ meltfptr[10] = NULL;;
      }
    ;
    /*^compute */
 /*_#MAXDEPTH__L5*/ meltfnum[4] =
      (melt_get_int ((melt_ptr_t) ( /*_.DBGI_MAXDEPTH__V11*/ meltfptr[10])));;

    {
      MELT_LOCATION ("warmelt-debug.melt:1481:/ locexp");
      meltgc_out_add_indent ((melt_ptr_t) ( /*_.OUT__V9*/ meltfptr[8]),
			     ( /*_#DEPTH__L1*/ meltfnum[0]), 64);;
    }
    ;

    {
      MELT_LOCATION ("warmelt-debug.melt:1482:/ locexp");
      meltgc_add_out ((melt_ptr_t) ( /*_.OUT__V9*/ meltfptr[8]), ("[~"));
    }
    ;
    MELT_LOCATION ("warmelt-debug.melt:1483:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.DIS__V8*/ meltfptr[6]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 1, "NAMED_NAME");
  /*_.NAMED_NAME__V12*/ meltfptr[11] = slot;
    };
    ;

    {
      /*^locexp */
      meltgc_add_out ((melt_ptr_t) ( /*_.OUT__V9*/ meltfptr[8]),
		      melt_string_str ((melt_ptr_t)
				       ( /*_.NAMED_NAME__V12*/
					meltfptr[11])));
    }
    ;

    {
      MELT_LOCATION ("warmelt-debug.melt:1484:/ locexp");
      meltgc_add_out ((melt_ptr_t) ( /*_.OUT__V9*/ meltfptr[8]), ("/"));
    }
    ;
 /*_#OBJ_HASH__L6*/ meltfnum[5] =
      (melt_obj_hash ((melt_ptr_t) ( /*_.SELF__V2*/ meltfptr[1])));;

    {
      MELT_LOCATION ("warmelt-debug.melt:1485:/ locexp");
      meltgc_add_out_hex ((melt_ptr_t) ( /*_.OUT__V9*/ meltfptr[8]),
			  ( /*_#OBJ_HASH__L6*/ meltfnum[5]));
    }
    ;
    MELT_LOCATION ("warmelt-debug.melt:1486:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^cond */
    /*cond */ if ( /*_#BINDNUM__L4*/ meltfnum[1])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{


	  {
	    MELT_LOCATION ("warmelt-debug.melt:1488:/ locexp");
	    meltgc_add_out ((melt_ptr_t) ( /*_.OUT__V9*/ meltfptr[8]), ("#"));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-debug.melt:1489:/ locexp");
	    meltgc_add_out_dec ((melt_ptr_t) ( /*_.OUT__V9*/ meltfptr[8]),
				( /*_#BINDNUM__L4*/ meltfnum[1]));
	  }
	  ;
	  MELT_LOCATION ("warmelt-debug.melt:1487:/ quasiblock");


	  /*epilog */
	}
	;
      }				/*noelse */
    ;

    {
      MELT_LOCATION ("warmelt-debug.melt:1490:/ locexp");
      meltgc_add_out ((melt_ptr_t) ( /*_.OUT__V9*/ meltfptr[8]), (":"));
    }
    ;
 /*_#I__L7*/ meltfnum[6] =
      (( /*_#DEPTH__L1*/ meltfnum[0]) + (2));;
    MELT_LOCATION ("warmelt-debug.melt:1491:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^msend */
    /*msend */
    {
      union meltparam_un argtab[2];
      memset (&argtab, 0, sizeof (argtab));
      /*^ojbmsend.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.DBGI__V3*/ meltfptr[2];
      /*^ojbmsend.arg */
      argtab[1].meltbp_long = /*_#I__L7*/ meltfnum[6];
      /*_.DBG_OUTPUTAGAIN__V13*/ meltfptr[12] =
	meltgc_send ((melt_ptr_t) ( /*_.BINDERV__V10*/ meltfptr[9]),
		     (melt_ptr_t) (( /*!DBG_OUTPUTAGAIN */ meltfrout->
				    tabval[2])),
		     (MELTBPARSTR_PTR MELTBPARSTR_LONG ""), argtab, "",
		     (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-debug.melt:1492:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#MELT_REALLY_NEED_DBGLIM__L8*/ meltfnum[7] =
      ( /*MELT_REALLY_NEED_DBGLIM */
       melt_need_debug_limit ((int) /*_#DEPTH__L1*/ meltfnum[0],
			      (int) /*_#MAXDEPTH__L5*/ meltfnum[4]));;
    MELT_LOCATION ("warmelt-debug.melt:1492:/ cond");
    /*cond */ if ( /*_#MELT_REALLY_NEED_DBGLIM__L8*/ meltfnum[7])	/*then */
      {
	/*^cond.then */
	/*_#OR___L9*/ meltfnum[8] =
	  /*_#MELT_REALLY_NEED_DBGLIM__L8*/ meltfnum[7];;
      }
    else
      {
	MELT_LOCATION ("warmelt-debug.melt:1492:/ cond.else");

	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-debug.melt:1494:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
   /*_#I__L10*/ meltfnum[9] =
	    (( /*_#DEPTH__L1*/ meltfnum[0]) <
	     ( /*_#MAXDEPTH__L5*/ meltfnum[4]));;
	  MELT_LOCATION ("warmelt-debug.melt:1494:/ cond");
	  /*cond */ if ( /*_#I__L10*/ meltfnum[9])	/*then */
	    {
	      /*^cond.then */
	      /*^block */
	      /*anyblock */
	      {

     /*_#IS_A__L12*/ meltfnum[11] =
		  melt_is_instance_of ((melt_ptr_t)
				       ( /*_.DBGI__V3*/ meltfptr[2]),
				       (melt_ptr_t) (( /*!CLASS_DEBUG_OUTPUT_INFORMATION */ meltfrout->tabval[3])));;
		/*^compute */
		/*_#IF___L11*/ meltfnum[10] = /*_#IS_A__L12*/ meltfnum[11];;
		/*epilog */

		MELT_LOCATION ("warmelt-debug.melt:1494:/ clear");
	       /*clear *//*_#IS_A__L12*/ meltfnum[11] = 0;
	      }
	      ;
	    }
	  else
	    {			/*^cond.else */

    /*_#IF___L11*/ meltfnum[10] = 0;;
	    }
	  ;
	  /*^compute */
	  /*_#OR___L9*/ meltfnum[8] = /*_#IF___L11*/ meltfnum[10];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-debug.melt:1492:/ clear");
	     /*clear *//*_#I__L10*/ meltfnum[9] = 0;
	  /*^clear */
	     /*clear *//*_#IF___L11*/ meltfnum[10] = 0;
	}
	;
      }
    ;
    /*^cond */
    /*cond */ if ( /*_#OR___L9*/ meltfnum[8])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{


	  {
	    MELT_LOCATION ("warmelt-debug.melt:1496:/ locexp");
	    meltgc_add_out ((melt_ptr_t) ( /*_.OUT__V9*/ meltfptr[8]),
			    ("; "));
	  }
	  ;
   /*_#I__L13*/ meltfnum[11] =
	    (( /*_#DEPTH__L1*/ meltfnum[0]) + (1));;
	  MELT_LOCATION ("warmelt-debug.melt:1497:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^apply */
	  /*apply */
	  {
	    union meltparam_un argtab[4];
	    memset (&argtab, 0, sizeof (argtab));
	    /*^apply.arg */
	    argtab[0].meltbp_aptr =
	      (melt_ptr_t *) & /*_.DBGI__V3*/ meltfptr[2];
	    /*^apply.arg */
	    argtab[1].meltbp_long = /*_#I__L13*/ meltfnum[11];
	    /*^apply.arg */
	    argtab[2].meltbp_long = 1;
	    /*^apply.arg */
	    argtab[3].meltbp_long = 0;
	    /*_.DBGOUT_FIELDS__V15*/ meltfptr[14] =
	      melt_apply ((meltclosure_ptr_t)
			  (( /*!DBGOUT_FIELDS */ meltfrout->tabval[4])),
			  (melt_ptr_t) ( /*_.SELF__V2*/ meltfptr[1]),
			  (MELTBPARSTR_PTR MELTBPARSTR_LONG MELTBPARSTR_LONG
			   MELTBPARSTR_LONG ""), argtab, "",
			  (union meltparam_un *) 0);
	  }
	  ;
	  MELT_LOCATION ("warmelt-debug.melt:1495:/ quasiblock");


	  /*_.PROGN___V16*/ meltfptr[15] =
	    /*_.DBGOUT_FIELDS__V15*/ meltfptr[14];;
	  /*^compute */
	  /*_.IF___V14*/ meltfptr[13] = /*_.PROGN___V16*/ meltfptr[15];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-debug.melt:1492:/ clear");
	     /*clear *//*_#I__L13*/ meltfnum[11] = 0;
	  /*^clear */
	     /*clear *//*_.DBGOUT_FIELDS__V15*/ meltfptr[14] = 0;
	  /*^clear */
	     /*clear *//*_.PROGN___V16*/ meltfptr[15] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

  /*_.IF___V14*/ meltfptr[13] = NULL;;
      }
    ;

    {
      MELT_LOCATION ("warmelt-debug.melt:1499:/ locexp");
      meltgc_add_out ((melt_ptr_t) ( /*_.OUT__V9*/ meltfptr[8]), ("~]"));
    }
    ;

    MELT_LOCATION ("warmelt-debug.melt:1475:/ clear");
	   /*clear *//*_.DIS__V8*/ meltfptr[6] = 0;
    /*^clear */
	   /*clear *//*_.OUT__V9*/ meltfptr[8] = 0;
    /*^clear */
	   /*clear *//*_.BINDERV__V10*/ meltfptr[9] = 0;
    /*^clear */
	   /*clear *//*_#BINDNUM__L4*/ meltfnum[1] = 0;
    /*^clear */
	   /*clear *//*_.DBGI_MAXDEPTH__V11*/ meltfptr[10] = 0;
    /*^clear */
	   /*clear *//*_#MAXDEPTH__L5*/ meltfnum[4] = 0;
    /*^clear */
	   /*clear *//*_.NAMED_NAME__V12*/ meltfptr[11] = 0;
    /*^clear */
	   /*clear *//*_#OBJ_HASH__L6*/ meltfnum[5] = 0;
    /*^clear */
	   /*clear *//*_#I__L7*/ meltfnum[6] = 0;
    /*^clear */
	   /*clear *//*_.DBG_OUTPUTAGAIN__V13*/ meltfptr[12] = 0;
    /*^clear */
	   /*clear *//*_#MELT_REALLY_NEED_DBGLIM__L8*/ meltfnum[7] = 0;
    /*^clear */
	   /*clear *//*_#OR___L9*/ meltfnum[8] = 0;
    /*^clear */
	   /*clear *//*_.IF___V14*/ meltfptr[13] = 0;
    MELT_LOCATION ("warmelt-debug.melt:1472:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V4*/ meltfptr[3] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V6*/ meltfptr[4] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("DBGOUT_ANYBINDING_METHOD", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_50_warmelt_debug_DBGOUT_ANYBINDING_METHOD_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*noretval */ NULL);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_50_warmelt_debug_DBGOUT_ANYBINDING_METHOD */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_51_warmelt_debug_DISPLAY_DEBUG_MESSAGE (meltclosure_ptr_t meltclosp_,
						 melt_ptr_t meltfirstargp_,
						 const melt_argdescr_cell_t
						 meltxargdescr_[],
						 union meltparam_un *
						 meltxargtab_,
						 const melt_argdescr_cell_t
						 meltxresdescr_[],
						 union meltparam_un *
						 meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_51_warmelt_debug_DISPLAY_DEBUG_MESSAGE_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_51_warmelt_debug_DISPLAY_DEBUG_MESSAGE_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 8
    melt_ptr_t mcfr_varptr[8];
#define MELTFRAM_NBVARNUM 5
    long mcfr_varnum[5];
/*others*/
    const char *loc_CSTRING__o0;
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_51_warmelt_debug_DISPLAY_DEBUG_MESSAGE is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_51_warmelt_debug_DISPLAY_DEBUG_MESSAGE_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 8; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_51_warmelt_debug_DISPLAY_DEBUG_MESSAGE nbval 8*/
  meltfram__.mcfr_nbvar = 8 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("DISPLAY_DEBUG_MESSAGE", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-debug.melt:1509:/ getarg");
 /*_.VAL__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_CSTRING)
    goto lab_endgetargs;
 /*_?*/ meltfram__.loc_CSTRING__o0 = meltxargtab_[0].meltbp_cstring;

  /*getarg#2 */
  /*^getarg */
  if (meltxargdescr_[1] != MELTBPAR_LONG)
    goto lab_endgetargs;
 /*_#COUNT__L1*/ meltfnum[0] = meltxargtab_[1].meltbp_long;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {

    MELT_LOCATION ("warmelt-debug.melt:1511:/ quasiblock");


 /*_#DBGCOUNTER__L2*/ meltfnum[1] = 0;;
    /*^compute */
 /*_.SBUF__V3*/ meltfptr[2] =
      (melt_ptr_t)
      meltgc_new_strbuf ((meltobject_ptr_t)
			 (( /*!DISCR_STRBUF */ meltfrout->tabval[0])),
			 (const char *) 0);;
    /*^compute */
 /*_.OCCMAP__V4*/ meltfptr[3] =
      (meltgc_new_mapobjects
       ((meltobject_ptr_t) (( /*!DISCR_MAP_OBJECTS */ meltfrout->tabval[1])),
	(50)));;
    /*^compute */
 /*_.BOXEDMAXDEPTH__V5*/ meltfptr[4] =
      (meltgc_new_int
       ((meltobject_ptr_t) (( /*!DISCR_INTEGER */ meltfrout->tabval[2])),
	(14)));;
    MELT_LOCATION ("warmelt-debug.melt:1515:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*^rawallocobj */
    /*rawallocobj */
    {
      melt_ptr_t newobj = 0;
      melt_raw_object_create (newobj,
			      (melt_ptr_t) (( /*!CLASS_DEBUG_OUTPUT_INFORMATION */ meltfrout->tabval[3])), (3), "CLASS_DEBUG_OUTPUT_INFORMATION");
  /*_.INST__V7*/ meltfptr[6] =
	newobj;
    };
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @DBGI_OUT",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V7*/ meltfptr[6])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V7*/ meltfptr[6]), (0),
			  ( /*_.SBUF__V3*/ meltfptr[2]), "DBGI_OUT");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @DBGI_OCCMAP",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V7*/ meltfptr[6])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V7*/ meltfptr[6]), (1),
			  ( /*_.OCCMAP__V4*/ meltfptr[3]), "DBGI_OCCMAP");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @DBGI_MAXDEPTH",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V7*/ meltfptr[6])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V7*/ meltfptr[6]), (2),
			  ( /*_.BOXEDMAXDEPTH__V5*/ meltfptr[4]),
			  "DBGI_MAXDEPTH");
    ;
    /*^touchobj */

    melt_dbgtrace_written_object ( /*_.INST__V7*/ meltfptr[6],
				  "newly made instance");
    ;
    /*_.DBGI__V6*/ meltfptr[5] = /*_.INST__V7*/ meltfptr[6];;

    {
      MELT_LOCATION ("warmelt-debug.melt:1520:/ locexp");
 /*_#DBGCOUNTER__L2*/ meltfnum[1] = ++melt_dbgcounter;
    }
    ;

    {
      MELT_LOCATION ("warmelt-debug.melt:1521:/ locexp");
      melt_putnum (stderr, ("!*!#"), ( /*_#DBGCOUNTER__L2*/ meltfnum[1]),
		   ("/"));
    }
    ;
 /*_#THE_FRAMEDEPTH__L3*/ meltfnum[2] =
      (melt_curframdepth ());;
    /*^compute */
 /*_#I__L4*/ meltfnum[3] =
      (( /*_#THE_FRAMEDEPTH__L3*/ meltfnum[2]) - (1));;

    {
      MELT_LOCATION ("warmelt-debug.melt:1522:/ locexp");
      melt_putnum (stderr, (""), ( /*_#I__L4*/ meltfnum[3]), (":"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-debug.melt:1523:/ locexp");
      melt_puts (stderr, ( /*_?*/ meltfram__.loc_CSTRING__o0));
    }
    ;
    MELT_LOCATION ("warmelt-debug.melt:1524:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#I__L5*/ meltfnum[4] =
      (( /*_#COUNT__L1*/ meltfnum[0]) > (0));;
    MELT_LOCATION ("warmelt-debug.melt:1524:/ cond");
    /*cond */ if ( /*_#I__L5*/ meltfnum[4])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{


	  {
	    /*^locexp */
	    melt_putnum (stderr, (" !"), ( /*_#COUNT__L1*/ meltfnum[0]),
			 (": "));
	  }
	  ;
	  /*epilog */
	}
	;
      }				/*noelse */
    ;
    MELT_LOCATION ("warmelt-debug.melt:1525:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^msend */
    /*msend */
    {
      union meltparam_un argtab[2];
      memset (&argtab, 0, sizeof (argtab));
      /*^ojbmsend.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.DBGI__V6*/ meltfptr[5];
      /*^ojbmsend.arg */
      argtab[1].meltbp_long = 0;
      /*_.DBG_OUTPUT__V8*/ meltfptr[7] =
	meltgc_send ((melt_ptr_t) ( /*_.VAL__V2*/ meltfptr[1]),
		     (melt_ptr_t) (( /*!DBG_OUTPUT */ meltfrout->tabval[4])),
		     (MELTBPARSTR_PTR MELTBPARSTR_LONG ""), argtab, "",
		     (union meltparam_un *) 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-debug.melt:1526:/ locexp");
      melt_putstrbuf (stderr, (melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]));
    }
    ;

    {
      MELT_LOCATION ("warmelt-debug.melt:1527:/ locexp");
      melt_newlineflush (stderr);
    }
    ;

    MELT_LOCATION ("warmelt-debug.melt:1511:/ clear");
	   /*clear *//*_#DBGCOUNTER__L2*/ meltfnum[1] = 0;
    /*^clear */
	   /*clear *//*_.SBUF__V3*/ meltfptr[2] = 0;
    /*^clear */
	   /*clear *//*_.OCCMAP__V4*/ meltfptr[3] = 0;
    /*^clear */
	   /*clear *//*_.BOXEDMAXDEPTH__V5*/ meltfptr[4] = 0;
    /*^clear */
	   /*clear *//*_.DBGI__V6*/ meltfptr[5] = 0;
    /*^clear */
	   /*clear *//*_#THE_FRAMEDEPTH__L3*/ meltfnum[2] = 0;
    /*^clear */
	   /*clear *//*_#I__L4*/ meltfnum[3] = 0;
    /*^clear */
	   /*clear *//*_#I__L5*/ meltfnum[4] = 0;
    /*^clear */
	   /*clear *//*_.DBG_OUTPUT__V8*/ meltfptr[7] = 0;
    /*epilog */
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("DISPLAY_DEBUG_MESSAGE", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_51_warmelt_debug_DISPLAY_DEBUG_MESSAGE_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*noretval */ NULL);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_51_warmelt_debug_DISPLAY_DEBUG_MESSAGE */



/**** end of warmelt-debug+02.c ****/
