/* GCC MELT GENERATED FILE warmelt-debug.c - DO NOT EDIT */

/***+ warmelt-debug without options +***/
#include "melt-run.h"


/* used hash from melt-run.h when compiling this file: */
MELT_EXTERN const char meltrun_used_md5_melt[] =
  MELT_RUN_HASHMD5 /* from melt-run.h */ ;


/**** warmelt-debug declarations ****/


/***************************************************
***
    Copyright 2009 - 2012 Free Software Foundation, Inc.
    Contributed by Basile Starynkevitch <basile@starynkevitch.net>

    This file is part of GCC.

    GCC is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3, or (at your option)
    any later version.

    GCC is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with GCC; see the file COPYING3.  If not see
    <http://www.gnu.org/licenses/>.
***
****************************************************/

/* ordinary MELT module */
#define MELT_HAS_INITIAL_ENVIRONMENT 1	/*usual */

struct melt_callframe_st;	/*defined in melt-runtime.h */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_1_warmelt_debug_DEBUG_MSG_FUN (meltclosure_ptr_t meltclosp_,
					melt_ptr_t meltfirstargp_,
					const melt_argdescr_cell_t
					meltxargdescr_[],
					union meltparam_un *meltxargtab_,
					const melt_argdescr_cell_t
					meltxresdescr_[],
					union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_2_warmelt_debug_REGISTER_TREE_DEBUG_FUN (meltclosure_ptr_t
						  meltclosp_,
						  melt_ptr_t meltfirstargp_,
						  const melt_argdescr_cell_t
						  meltxargdescr_[],
						  union meltparam_un
						  *meltxargtab_,
						  const melt_argdescr_cell_t
						  meltxresdescr_[],
						  union meltparam_un
						  *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_3_warmelt_debug_REGISTER_GIMPLE_DEBUG_FUN (meltclosure_ptr_t
						    meltclosp_,
						    melt_ptr_t meltfirstargp_,
						    const melt_argdescr_cell_t
						    meltxargdescr_[],
						    union meltparam_un
						    *meltxargtab_,
						    const melt_argdescr_cell_t
						    meltxresdescr_[],
						    union meltparam_un
						    *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_4_warmelt_debug_REGISTER_GIMPLESEQ_DEBUG_FUN (meltclosure_ptr_t
						       meltclosp_,
						       melt_ptr_t
						       meltfirstargp_,
						       const
						       melt_argdescr_cell_t
						       meltxargdescr_[],
						       union meltparam_un
						       *meltxargtab_,
						       const
						       melt_argdescr_cell_t
						       meltxresdescr_[],
						       union meltparam_un
						       *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_5_warmelt_debug_REGISTER_EDGE_DEBUG_FUN (meltclosure_ptr_t
						  meltclosp_,
						  melt_ptr_t meltfirstargp_,
						  const melt_argdescr_cell_t
						  meltxargdescr_[],
						  union meltparam_un
						  *meltxargtab_,
						  const melt_argdescr_cell_t
						  meltxresdescr_[],
						  union meltparam_un
						  *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_6_warmelt_debug_REGISTER_BASICBLOCK_DEBUG_FUN (meltclosure_ptr_t
							meltclosp_,
							melt_ptr_t
							meltfirstargp_,
							const
							melt_argdescr_cell_t
							meltxargdescr_[],
							union meltparam_un
							*meltxargtab_,
							const
							melt_argdescr_cell_t
							meltxresdescr_[],
							union meltparam_un
							*meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_7_warmelt_debug_REGISTER_LOOP_DEBUG_FUN (meltclosure_ptr_t
						  meltclosp_,
						  melt_ptr_t meltfirstargp_,
						  const melt_argdescr_cell_t
						  meltxargdescr_[],
						  union meltparam_un
						  *meltxargtab_,
						  const melt_argdescr_cell_t
						  meltxresdescr_[],
						  union meltparam_un
						  *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_8_warmelt_debug_MELT_DEBUG_FUN (meltclosure_ptr_t meltclosp_,
					 melt_ptr_t meltfirstargp_,
					 const melt_argdescr_cell_t
					 meltxargdescr_[],
					 union meltparam_un *meltxargtab_,
					 const melt_argdescr_cell_t
					 meltxresdescr_[],
					 union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_9_warmelt_debug_DBG_OUTOBJECT (meltclosure_ptr_t meltclosp_,
					melt_ptr_t meltfirstargp_,
					const melt_argdescr_cell_t
					meltxargdescr_[],
					union meltparam_un *meltxargtab_,
					const melt_argdescr_cell_t
					meltxresdescr_[],
					union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_10_warmelt_debug_DBG_OUT (meltclosure_ptr_t meltclosp_,
				   melt_ptr_t meltfirstargp_,
				   const melt_argdescr_cell_t
				   meltxargdescr_[],
				   union meltparam_un *meltxargtab_,
				   const melt_argdescr_cell_t
				   meltxresdescr_[],
				   union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_11_warmelt_debug_DBGOUT_FIELDS (meltclosure_ptr_t meltclosp_,
					 melt_ptr_t meltfirstargp_,
					 const melt_argdescr_cell_t
					 meltxargdescr_[],
					 union meltparam_un *meltxargtab_,
					 const melt_argdescr_cell_t
					 meltxresdescr_[],
					 union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_12_warmelt_debug_DBGOUTAGAIN_FIELDS (meltclosure_ptr_t meltclosp_,
					      melt_ptr_t meltfirstargp_,
					      const melt_argdescr_cell_t
					      meltxargdescr_[],
					      union meltparam_un
					      *meltxargtab_,
					      const melt_argdescr_cell_t
					      meltxresdescr_[],
					      union meltparam_un
					      *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_13_warmelt_debug_DBGOUT_NULL_METHOD (meltclosure_ptr_t meltclosp_,
					      melt_ptr_t meltfirstargp_,
					      const melt_argdescr_cell_t
					      meltxargdescr_[],
					      union meltparam_un
					      *meltxargtab_,
					      const melt_argdescr_cell_t
					      meltxresdescr_[],
					      union meltparam_un
					      *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_14_warmelt_debug_DBGOUT_STRING_METHOD (meltclosure_ptr_t meltclosp_,
						melt_ptr_t meltfirstargp_,
						const melt_argdescr_cell_t
						meltxargdescr_[],
						union meltparam_un
						*meltxargtab_,
						const melt_argdescr_cell_t
						meltxresdescr_[],
						union meltparam_un
						*meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_15_warmelt_debug_DBGOUT_STRBUF_METHOD (meltclosure_ptr_t meltclosp_,
						melt_ptr_t meltfirstargp_,
						const melt_argdescr_cell_t
						meltxargdescr_[],
						union meltparam_un
						*meltxargtab_,
						const melt_argdescr_cell_t
						meltxresdescr_[],
						union meltparam_un
						*meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_16_warmelt_debug_DBGOUT_INTEGER_METHOD (meltclosure_ptr_t meltclosp_,
						 melt_ptr_t meltfirstargp_,
						 const melt_argdescr_cell_t
						 meltxargdescr_[],
						 union meltparam_un
						 *meltxargtab_,
						 const melt_argdescr_cell_t
						 meltxresdescr_[],
						 union meltparam_un
						 *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_17_warmelt_debug_DBGOUT_MIXINT_METHOD (meltclosure_ptr_t meltclosp_,
						melt_ptr_t meltfirstargp_,
						const melt_argdescr_cell_t
						meltxargdescr_[],
						union meltparam_un
						*meltxargtab_,
						const melt_argdescr_cell_t
						meltxresdescr_[],
						union meltparam_un
						*meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_18_warmelt_debug_DBGOUT_MIXLOC_METHOD (meltclosure_ptr_t meltclosp_,
						melt_ptr_t meltfirstargp_,
						const melt_argdescr_cell_t
						meltxargdescr_[],
						union meltparam_un
						*meltxargtab_,
						const melt_argdescr_cell_t
						meltxresdescr_[],
						union meltparam_un
						*meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_19_warmelt_debug_DBGOUT_MIXBIGINT_METHOD (meltclosure_ptr_t
						   meltclosp_,
						   melt_ptr_t meltfirstargp_,
						   const melt_argdescr_cell_t
						   meltxargdescr_[],
						   union meltparam_un
						   *meltxargtab_,
						   const melt_argdescr_cell_t
						   meltxresdescr_[],
						   union meltparam_un
						   *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_20_warmelt_debug_DBGOUT_MULTIPLE_METHOD (meltclosure_ptr_t
						  meltclosp_,
						  melt_ptr_t meltfirstargp_,
						  const melt_argdescr_cell_t
						  meltxargdescr_[],
						  union meltparam_un
						  *meltxargtab_,
						  const melt_argdescr_cell_t
						  meltxresdescr_[],
						  union meltparam_un
						  *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_21_warmelt_debug_DBGOUT_ROUTINE_METHOD (meltclosure_ptr_t meltclosp_,
						 melt_ptr_t meltfirstargp_,
						 const melt_argdescr_cell_t
						 meltxargdescr_[],
						 union meltparam_un
						 *meltxargtab_,
						 const melt_argdescr_cell_t
						 meltxresdescr_[],
						 union meltparam_un
						 *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_22_warmelt_debug_LAMBDA___1__ (meltclosure_ptr_t meltclosp_,
					melt_ptr_t meltfirstargp_,
					const melt_argdescr_cell_t
					meltxargdescr_[],
					union meltparam_un *meltxargtab_,
					const melt_argdescr_cell_t
					meltxresdescr_[],
					union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_23_warmelt_debug_DBGOUT_CLOSURE_METHOD (meltclosure_ptr_t meltclosp_,
						 melt_ptr_t meltfirstargp_,
						 const melt_argdescr_cell_t
						 meltxargdescr_[],
						 union meltparam_un
						 *meltxargtab_,
						 const melt_argdescr_cell_t
						 meltxresdescr_[],
						 union meltparam_un
						 *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_24_warmelt_debug_DBGOUT_LIST_METHOD (meltclosure_ptr_t meltclosp_,
					      melt_ptr_t meltfirstargp_,
					      const melt_argdescr_cell_t
					      meltxargdescr_[],
					      union meltparam_un
					      *meltxargtab_,
					      const melt_argdescr_cell_t
					      meltxresdescr_[],
					      union meltparam_un
					      *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_25_warmelt_debug_DBGOUT_PAIR_METHOD (meltclosure_ptr_t meltclosp_,
					      melt_ptr_t meltfirstargp_,
					      const melt_argdescr_cell_t
					      meltxargdescr_[],
					      union meltparam_un
					      *meltxargtab_,
					      const melt_argdescr_cell_t
					      meltxresdescr_[],
					      union meltparam_un
					      *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_26_warmelt_debug_DBGOUT_MAPOBJECT_METHOD (meltclosure_ptr_t
						   meltclosp_,
						   melt_ptr_t meltfirstargp_,
						   const melt_argdescr_cell_t
						   meltxargdescr_[],
						   union meltparam_un
						   *meltxargtab_,
						   const melt_argdescr_cell_t
						   meltxresdescr_[],
						   union meltparam_un
						   *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_27_warmelt_debug_LAMBDA___2__ (meltclosure_ptr_t meltclosp_,
					melt_ptr_t meltfirstargp_,
					const melt_argdescr_cell_t
					meltxargdescr_[],
					union meltparam_un *meltxargtab_,
					const melt_argdescr_cell_t
					meltxresdescr_[],
					union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_28_warmelt_debug_LAMBDA___3__ (meltclosure_ptr_t meltclosp_,
					melt_ptr_t meltfirstargp_,
					const melt_argdescr_cell_t
					meltxargdescr_[],
					union meltparam_un *meltxargtab_,
					const melt_argdescr_cell_t
					meltxresdescr_[],
					union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_29_warmelt_debug_LAMBDA___4__ (meltclosure_ptr_t meltclosp_,
					melt_ptr_t meltfirstargp_,
					const melt_argdescr_cell_t
					meltxargdescr_[],
					union meltparam_un *meltxargtab_,
					const melt_argdescr_cell_t
					meltxresdescr_[],
					union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_30_warmelt_debug_DBGOUT_MAPSTRING_METHOD (meltclosure_ptr_t
						   meltclosp_,
						   melt_ptr_t meltfirstargp_,
						   const melt_argdescr_cell_t
						   meltxargdescr_[],
						   union meltparam_un
						   *meltxargtab_,
						   const melt_argdescr_cell_t
						   meltxresdescr_[],
						   union meltparam_un
						   *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_31_warmelt_debug_LAMBDA___5__ (meltclosure_ptr_t meltclosp_,
					melt_ptr_t meltfirstargp_,
					const melt_argdescr_cell_t
					meltxargdescr_[],
					union meltparam_un *meltxargtab_,
					const melt_argdescr_cell_t
					meltxresdescr_[],
					union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_32_warmelt_debug_LAMBDA___6__ (meltclosure_ptr_t meltclosp_,
					melt_ptr_t meltfirstargp_,
					const melt_argdescr_cell_t
					meltxargdescr_[],
					union meltparam_un *meltxargtab_,
					const melt_argdescr_cell_t
					meltxresdescr_[],
					union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_33_warmelt_debug_LAMBDA___7__ (meltclosure_ptr_t meltclosp_,
					melt_ptr_t meltfirstargp_,
					const melt_argdescr_cell_t
					meltxargdescr_[],
					union meltparam_un *meltxargtab_,
					const melt_argdescr_cell_t
					meltxresdescr_[],
					union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_34_warmelt_debug_DBGOUT_BUCKETLONG_METHOD (meltclosure_ptr_t
						    meltclosp_,
						    melt_ptr_t meltfirstargp_,
						    const melt_argdescr_cell_t
						    meltxargdescr_[],
						    union meltparam_un
						    *meltxargtab_,
						    const melt_argdescr_cell_t
						    meltxresdescr_[],
						    union meltparam_un
						    *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_35_warmelt_debug_DBGOUT_ANYOBJECT_METHOD (meltclosure_ptr_t
						   meltclosp_,
						   melt_ptr_t meltfirstargp_,
						   const melt_argdescr_cell_t
						   meltxargdescr_[],
						   union meltparam_un
						   *meltxargtab_,
						   const melt_argdescr_cell_t
						   meltxresdescr_[],
						   union meltparam_un
						   *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_36_warmelt_debug_DBGOUT_ANYRECV_METHOD (meltclosure_ptr_t meltclosp_,
						 melt_ptr_t meltfirstargp_,
						 const melt_argdescr_cell_t
						 meltxargdescr_[],
						 union meltparam_un
						 *meltxargtab_,
						 const melt_argdescr_cell_t
						 meltxresdescr_[],
						 union meltparam_un
						 *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_37_warmelt_debug_DBGOUTAGAIN_ANYOBJECT_METHOD (meltclosure_ptr_t
							meltclosp_,
							melt_ptr_t
							meltfirstargp_,
							const
							melt_argdescr_cell_t
							meltxargdescr_[],
							union meltparam_un
							*meltxargtab_,
							const
							melt_argdescr_cell_t
							meltxresdescr_[],
							union meltparam_un
							*meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_38_warmelt_debug_DBGOUT_NAMEDOBJECT_METHOD (meltclosure_ptr_t
						     meltclosp_,
						     melt_ptr_t
						     meltfirstargp_,
						     const
						     melt_argdescr_cell_t
						     meltxargdescr_[],
						     union meltparam_un
						     *meltxargtab_,
						     const
						     melt_argdescr_cell_t
						     meltxresdescr_[],
						     union meltparam_un
						     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_39_warmelt_debug_DBGOUTAGAIN_NAMEDOBJECT_METHOD (meltclosure_ptr_t
							  meltclosp_,
							  melt_ptr_t
							  meltfirstargp_,
							  const
							  melt_argdescr_cell_t
							  meltxargdescr_[],
							  union meltparam_un
							  *meltxargtab_,
							  const
							  melt_argdescr_cell_t
							  meltxresdescr_[],
							  union meltparam_un
							  *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_40_warmelt_debug_DBGOUTAGAIN_SYMBOL_METHOD (meltclosure_ptr_t
						     meltclosp_,
						     melt_ptr_t
						     meltfirstargp_,
						     const
						     melt_argdescr_cell_t
						     meltxargdescr_[],
						     union meltparam_un
						     *meltxargtab_,
						     const
						     melt_argdescr_cell_t
						     meltxresdescr_[],
						     union meltparam_un
						     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_41_warmelt_debug_DBGOUT_SYMBOL_METHOD (meltclosure_ptr_t meltclosp_,
						melt_ptr_t meltfirstargp_,
						const melt_argdescr_cell_t
						meltxargdescr_[],
						union meltparam_un
						*meltxargtab_,
						const melt_argdescr_cell_t
						meltxresdescr_[],
						union meltparam_un
						*meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_42_warmelt_debug_DBGOUT_CLASS_METHOD (meltclosure_ptr_t meltclosp_,
					       melt_ptr_t meltfirstargp_,
					       const melt_argdescr_cell_t
					       meltxargdescr_[],
					       union meltparam_un
					       *meltxargtab_,
					       const melt_argdescr_cell_t
					       meltxresdescr_[],
					       union meltparam_un
					       *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_43_warmelt_debug_DBGOUTAGAIN_KEYWORD_METHOD (meltclosure_ptr_t
						      meltclosp_,
						      melt_ptr_t
						      meltfirstargp_,
						      const
						      melt_argdescr_cell_t
						      meltxargdescr_[],
						      union meltparam_un
						      *meltxargtab_,
						      const
						      melt_argdescr_cell_t
						      meltxresdescr_[],
						      union meltparam_un
						      *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_44_warmelt_debug_DBGOUT_KEYWORD_METHOD (meltclosure_ptr_t meltclosp_,
						 melt_ptr_t meltfirstargp_,
						 const melt_argdescr_cell_t
						 meltxargdescr_[],
						 union meltparam_un
						 *meltxargtab_,
						 const melt_argdescr_cell_t
						 meltxresdescr_[],
						 union meltparam_un
						 *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_45_warmelt_debug_DBGOUTAGAIN_CLONED_SYMBOL_METHOD (meltclosure_ptr_t
							    meltclosp_,
							    melt_ptr_t
							    meltfirstargp_,
							    const
							    melt_argdescr_cell_t
							    meltxargdescr_[],
							    union meltparam_un
							    *meltxargtab_,
							    const
							    melt_argdescr_cell_t
							    meltxresdescr_[],
							    union meltparam_un
							    *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_46_warmelt_debug_DBGOUT_CLONED_SYMBOL_METHOD (meltclosure_ptr_t
						       meltclosp_,
						       melt_ptr_t
						       meltfirstargp_,
						       const
						       melt_argdescr_cell_t
						       meltxargdescr_[],
						       union meltparam_un
						       *meltxargtab_,
						       const
						       melt_argdescr_cell_t
						       meltxresdescr_[],
						       union meltparam_un
						       *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_47_warmelt_debug_DBGOUT_ENVIRONMENT_METHOD (meltclosure_ptr_t
						     meltclosp_,
						     melt_ptr_t
						     meltfirstargp_,
						     const
						     melt_argdescr_cell_t
						     meltxargdescr_[],
						     union meltparam_un
						     *meltxargtab_,
						     const
						     melt_argdescr_cell_t
						     meltxresdescr_[],
						     union meltparam_un
						     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_48_warmelt_debug_DBGOUTAGAIN_CTYPE_METHOD (meltclosure_ptr_t
						    meltclosp_,
						    melt_ptr_t meltfirstargp_,
						    const melt_argdescr_cell_t
						    meltxargdescr_[],
						    union meltparam_un
						    *meltxargtab_,
						    const melt_argdescr_cell_t
						    meltxresdescr_[],
						    union meltparam_un
						    *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_49_warmelt_debug_DBGOUT_CTYPE_METHOD (meltclosure_ptr_t meltclosp_,
					       melt_ptr_t meltfirstargp_,
					       const melt_argdescr_cell_t
					       meltxargdescr_[],
					       union meltparam_un
					       *meltxargtab_,
					       const melt_argdescr_cell_t
					       meltxresdescr_[],
					       union meltparam_un
					       *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_50_warmelt_debug_DBGOUT_ANYBINDING_METHOD (meltclosure_ptr_t
						    meltclosp_,
						    melt_ptr_t meltfirstargp_,
						    const melt_argdescr_cell_t
						    meltxargdescr_[],
						    union meltparam_un
						    *meltxargtab_,
						    const melt_argdescr_cell_t
						    meltxresdescr_[],
						    union meltparam_un
						    *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_51_warmelt_debug_DISPLAY_DEBUG_MESSAGE (meltclosure_ptr_t meltclosp_,
						 melt_ptr_t meltfirstargp_,
						 const melt_argdescr_cell_t
						 meltxargdescr_[],
						 union meltparam_un
						 *meltxargtab_,
						 const melt_argdescr_cell_t
						 meltxresdescr_[],
						 union meltparam_un
						 *meltxrestab_);



MELT_EXTERN void *melt_start_this_module (void *);


/* define different names when debugging or not */
#if MELT_HAVE_DEBUG
MELT_EXTERN const char meltmodule_warmelt_debug__melt_have_debug_enabled[];
#define melt_have_debug_string meltmodule_warmelt_debug__melt_have_debug_enabled
#else /*!MELT_HAVE_DEBUG */
MELT_EXTERN const char meltmodule_warmelt_debug__melt_have_debug_disabled[];
#define melt_have_debug_string meltmodule_warmelt_debug__melt_have_debug_disabled
#endif /*!MELT_HAVE_DEBUG */


struct frame_melt_start_this_module_st;
void MELT_MODULE_VISIBILITY meltmod__warmelt_debug__initialmeltchunk_0 (struct
									frame_melt_start_this_module_st
									*,
									char
									*);
void MELT_MODULE_VISIBILITY meltmod__warmelt_debug__initialmeltchunk_1 (struct
									frame_melt_start_this_module_st
									*,
									char
									*);
void MELT_MODULE_VISIBILITY meltmod__warmelt_debug__initialmeltchunk_2 (struct
									frame_melt_start_this_module_st
									*,
									char
									*);
void MELT_MODULE_VISIBILITY meltmod__warmelt_debug__initialmeltchunk_3 (struct
									frame_melt_start_this_module_st
									*,
									char
									*);
void MELT_MODULE_VISIBILITY meltmod__warmelt_debug__initialmeltchunk_4 (struct
									frame_melt_start_this_module_st
									*,
									char
									*);
void MELT_MODULE_VISIBILITY meltmod__warmelt_debug__initialmeltchunk_5 (struct
									frame_melt_start_this_module_st
									*,
									char
									*);
void MELT_MODULE_VISIBILITY meltmod__warmelt_debug__initialmeltchunk_6 (struct
									frame_melt_start_this_module_st
									*,
									char
									*);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_debug__forward_or_mark_module_start_frame (struct
							    melt_callframe_st
							    *fp, int marking);
#define meltmarking_melt_start_this_module  meltmod__warmelt_debug__forward_or_mark_module_start_frame



/**** warmelt-debug implementations ****/




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_1_warmelt_debug_DEBUG_MSG_FUN (meltclosure_ptr_t meltclosp_,
					melt_ptr_t meltfirstargp_,
					const melt_argdescr_cell_t
					meltxargdescr_[],
					union meltparam_un *meltxargtab_,
					const melt_argdescr_cell_t
					meltxresdescr_[],
					union meltparam_un *meltxrestab_)
{
  long current_blocklevel_signals_meltrout_1_warmelt_debug_DEBUG_MSG_FUN_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_1_warmelt_debug_DEBUG_MSG_FUN_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 19
    melt_ptr_t mcfr_varptr[19];
#define MELTFRAM_NBVARNUM 16
    long mcfr_varnum[16];
/*others*/
    const char *loc_CSTRING__o0;
    const char *loc_CSTRING__o1;
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_1_warmelt_debug_DEBUG_MSG_FUN is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_1_warmelt_debug_DEBUG_MSG_FUN_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 19; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_1_warmelt_debug_DEBUG_MSG_FUN nbval 19*/
  meltfram__.mcfr_nbvar = 19 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("DEBUG_MSG_FUN", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-debug.melt:84:/ getarg");
 /*_.VAL__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_CSTRING)
    goto lab_endgetargs;
 /*_?*/ meltfram__.loc_CSTRING__o0 = meltxargtab_[0].meltbp_cstring;

  /*getarg#2 */
  /*^getarg */
  if (meltxargdescr_[1] != MELTBPAR_LONG)
    goto lab_endgetargs;
 /*_#COUNT__L1*/ meltfnum[0] = meltxargtab_[1].meltbp_long;

  /*getarg#3 */
  /*^getarg */
  if (meltxargdescr_[2] != MELTBPAR_CSTRING)
    goto lab_endgetargs;
 /*_?*/ meltfram__.loc_CSTRING__o1 = meltxargtab_[2].meltbp_cstring;

  /*getarg#4 */
  /*^getarg */
  if (meltxargdescr_[3] != MELTBPAR_LONG)
    goto lab_endgetargs;
 /*_#LINENO__L2*/ meltfnum[1] = meltxargtab_[3].meltbp_long;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


    {
      MELT_LOCATION ("warmelt-debug.melt:88:/ locexp");
      ++melt_dbgcounter;
    }
    ;
    MELT_LOCATION ("warmelt-debug.melt:89:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#MELT_HAS_FLAG_DEBUG_SET__L3*/ meltfnum[2] =
      /*MELT_HAS_FLAG_DEBUG_SET */ melt_flag_debug;;
    MELT_LOCATION ("warmelt-debug.melt:89:/ cond");
    /*cond */ if ( /*_#MELT_HAS_FLAG_DEBUG_SET__L3*/ meltfnum[2])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-debug.melt:90:/ quasiblock");


   /*_#DBGCOUNTER__L4*/ meltfnum[3] = 0;;
	  MELT_LOCATION ("warmelt-debug.melt:91:/ cond");
	  /*cond */ if (
			 /*ifisa */
			 melt_is_instance_of ((melt_ptr_t)
					      (( /*!INITIAL_SYSTEM_DATA */
						meltfrout->tabval[0])),
					      (melt_ptr_t) (( /*!CLASS_SYSTEM_DATA */ meltfrout->tabval[1])))
	    )			/*then */
	    {
	      /*^cond.then */
	      /*^getslot */
	      {
		melt_ptr_t slot = NULL, obj = NULL;
		obj =
		  (melt_ptr_t) (( /*!INITIAL_SYSTEM_DATA */ meltfrout->
				 tabval[0])) /*=obj*/ ;
		melt_object_get_field (slot, obj, 20, "SYSDATA_DUMPFILE");
     /*_.DUMPF__V3*/ meltfptr[2] = slot;
	      };
	      ;
	    }
	  else
	    {			/*^cond.else */

    /*_.DUMPF__V3*/ meltfptr[2] = NULL;;
	    }
	  ;
	  MELT_LOCATION ("warmelt-debug.melt:92:/ cond");
	  /*cond */ if (
			 /*ifisa */
			 melt_is_instance_of ((melt_ptr_t)
					      (( /*!INITIAL_SYSTEM_DATA */
						meltfrout->tabval[0])),
					      (melt_ptr_t) (( /*!CLASS_SYSTEM_DATA */ meltfrout->tabval[1])))
	    )			/*then */
	    {
	      /*^cond.then */
	      /*^getslot */
	      {
		melt_ptr_t slot = NULL, obj = NULL;
		obj =
		  (melt_ptr_t) (( /*!INITIAL_SYSTEM_DATA */ meltfrout->
				 tabval[0])) /*=obj*/ ;
		melt_object_get_field (slot, obj, 19, "SYSDATA_STDERR");
     /*_.STDERRF__V4*/ meltfptr[3] = slot;
	      };
	      ;
	    }
	  else
	    {			/*^cond.else */

    /*_.STDERRF__V4*/ meltfptr[3] = NULL;;
	    }
	  ;
	  /*^compute */
	  /*_.OUT__V5*/ meltfptr[4] = /*_.DUMPF__V3*/ meltfptr[2];;
	  /*^compute */
   /*_.OCCMAP__V6*/ meltfptr[5] =
	    (meltgc_new_mapobjects
	     ((meltobject_ptr_t)
	      (( /*!DISCR_MAP_OBJECTS */ meltfrout->tabval[2])), (50)));;
	  /*^compute */
   /*_#DEBUG_DEPTH__L5*/ meltfnum[4] =
	    (long) (melt_debug_depth ());;
	  /*^compute */
   /*_.BOXEDMAXDEPTH__V7*/ meltfptr[6] =
	    (meltgc_new_int
	     ((meltobject_ptr_t)
	      (( /*!DISCR_INTEGER */ meltfrout->tabval[3])),
	      ( /*_#DEBUG_DEPTH__L5*/ meltfnum[4])));;

	  {
	    MELT_LOCATION ("warmelt-debug.melt:98:/ locexp");
				      /*debug_msg_fun GETDBGCOUNTER__1 *//*_#DBGCOUNTER__L4*/
	      meltfnum[3] = melt_dbgcounter;
	    ;
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-debug.melt:102:/ locexp");
	    /* debug_msg_fun ENSUREGOODOUT__1 */
	    {
	      static int nbwarn_ENSUREGOODOUT__1;
	      if (melt_get_file ((melt_ptr_t) /*_.OUT__V5*/ meltfptr[4]) ==
		  NULL)
		{
		  gcc_assert (melt_get_file
			      ((melt_ptr_t) /*_.STDERRF__V4*/ meltfptr[3]) ==
			      stderr);
		  /*_.OUT__V5*/ meltfptr[4] = /*_.STDERRF__V4*/ meltfptr[3];
		  if (nbwarn_ENSUREGOODOUT__1++ <= 0)
		    {
		      inform (UNKNOWN_LOCATION,
			      "MELT debug output goes to stderr because we have no dump_file.");
		    }
		}
	    }			/*end  debug_msg_fun ENSUREGOODOUT__1 */
	    ;
	  }
	  ;
	  MELT_LOCATION ("warmelt-debug.melt:112:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
   /*_#NULL__L6*/ meltfnum[5] =
	    (( /*_.OUT__V5*/ meltfptr[4]) == NULL);;
	  MELT_LOCATION ("warmelt-debug.melt:112:/ cond");
	  /*cond */ if ( /*_#NULL__L6*/ meltfnum[5])	/*then */
	    {
	      /*^cond.then */
	      /*^block */
	      /*anyblock */
	      {

		MELT_LOCATION ("warmelt-debug.melt:113:/ cond");
		/*cond */ if (
			       /*ifisa */
			       melt_is_instance_of ((melt_ptr_t)
						    (( /*!INITIAL_SYSTEM_DATA */ meltfrout->tabval[0])),
						    (melt_ptr_t) (( /*!CLASS_SYSTEM_DATA */ meltfrout->tabval[1])))
		  )		/*then */
		  {
		    /*^cond.then */
		    /*^getslot */
		    {
		      melt_ptr_t slot = NULL, obj = NULL;
		      obj =
			(melt_ptr_t) (( /*!INITIAL_SYSTEM_DATA */ meltfrout->
				       tabval[0])) /*=obj*/ ;
		      melt_object_get_field (slot, obj, 19, "SYSDATA_STDERR");
       /*_.SYSDATA_STDERR__V9*/ meltfptr[8] = slot;
		    };
		    ;
		  }
		else
		  {		/*^cond.else */

      /*_.SYSDATA_STDERR__V9*/ meltfptr[8] = NULL;;
		  }
		;
		MELT_LOCATION ("warmelt-debug.melt:113:/ compute");
		/*_.OUT__V5*/ meltfptr[4] = /*_.SETQ___V10*/ meltfptr[9] =
		  /*_.SYSDATA_STDERR__V9*/ meltfptr[8];;
		/*_.IF___V8*/ meltfptr[7] = /*_.SETQ___V10*/ meltfptr[9];;
		/*epilog */

		MELT_LOCATION ("warmelt-debug.melt:112:/ clear");
	       /*clear *//*_.SYSDATA_STDERR__V9*/ meltfptr[8] = 0;
		/*^clear */
	       /*clear *//*_.SETQ___V10*/ meltfptr[9] = 0;
	      }
	      ;
	    }
	  else
	    {			/*^cond.else */

    /*_.IF___V8*/ meltfptr[7] = NULL;;
	    }
	  ;
	  MELT_LOCATION ("warmelt-debug.melt:117:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
   /*_#NULL__L7*/ meltfnum[6] =
	    (( /*_.OUT__V5*/ meltfptr[4]) == NULL);;
	  MELT_LOCATION ("warmelt-debug.melt:117:/ cond");
	  /*cond */ if ( /*_#NULL__L7*/ meltfnum[6])	/*then */
	    {
	      /*^cond.then */
	      /*^block */
	      /*anyblock */
	      {

     /*_.MAKE_STRBUF__V12*/ meltfptr[9] =
		  (melt_ptr_t)
		  meltgc_new_strbuf ((meltobject_ptr_t)
				     (( /*!DISCR_STRBUF */ meltfrout->
				       tabval[4])), (const char *) 0);;
		MELT_LOCATION ("warmelt-debug.melt:118:/ compute");
		/*_.OUT__V5*/ meltfptr[4] = /*_.SETQ___V13*/ meltfptr[12] =
		  /*_.MAKE_STRBUF__V12*/ meltfptr[9];;
		/*_.IF___V11*/ meltfptr[8] = /*_.SETQ___V13*/ meltfptr[12];;
		/*epilog */

		MELT_LOCATION ("warmelt-debug.melt:117:/ clear");
	       /*clear *//*_.MAKE_STRBUF__V12*/ meltfptr[9] = 0;
		/*^clear */
	       /*clear *//*_.SETQ___V13*/ meltfptr[12] = 0;
	      }
	      ;
	    }
	  else
	    {			/*^cond.else */

    /*_.IF___V11*/ meltfptr[8] = NULL;;
	    }
	  ;

#if MELT_HAVE_DEBUG
	  MELT_LOCATION ("warmelt-debug.melt:119:/ cppif.then");
	  /*^block */
	  /*anyblock */
	  {

	    /*^checksignal */
	    MELT_CHECK_SIGNAL ();
	    ;
     /*_#IS_OUT__L8*/ meltfnum[7] =
	      (melt_is_out ((melt_ptr_t) /*_.OUT__V5*/ meltfptr[4]));;
	    MELT_LOCATION ("warmelt-debug.melt:119:/ cond");
	    /*cond */ if ( /*_#IS_OUT__L8*/ meltfnum[7])	/*then */
	      {
		/*^cond.then */
		/*_.IFELSE___V15*/ meltfptr[12] = ( /*nil */ NULL);;
	      }
	    else
	      {
		MELT_LOCATION ("warmelt-debug.melt:119:/ cond.else");

		/*^block */
		/*anyblock */
		{




		  {
		    /*^locexp */
		    melt_assert_failed (("check good out"),
					("warmelt-debug.melt")
					? ("warmelt-debug.melt") : __FILE__,
					(119) ? (119) : __LINE__,
					__FUNCTION__);
		    ;
		  }
		  ;
		 /*clear *//*_.IFELSE___V15*/ meltfptr[12] = 0;
		  /*epilog */
		}
		;
	      }
	    ;
	    /*^compute */
	    /*_.IFCPP___V14*/ meltfptr[9] = /*_.IFELSE___V15*/ meltfptr[12];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-debug.melt:119:/ clear");
	       /*clear *//*_#IS_OUT__L8*/ meltfnum[7] = 0;
	    /*^clear */
	       /*clear *//*_.IFELSE___V15*/ meltfptr[12] = 0;
	  }

#else /*MELT_HAVE_DEBUG */
	  /*^cppif.else */
	  /*_.IFCPP___V14*/ meltfptr[9] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
	  ;
	  MELT_LOCATION ("warmelt-debug.melt:120:/ quasiblock");


	  MELT_LOCATION ("warmelt-debug.melt:121:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^quasiblock */


	  /*^rawallocobj */
	  /*rawallocobj */
	  {
	    melt_ptr_t newobj = 0;
	    melt_raw_object_create (newobj,
				    (melt_ptr_t) (( /*!CLASS_DEBUG_INFORMATION */ meltfrout->tabval[5])), (3), "CLASS_DEBUG_INFORMATION");
    /*_.INST__V17*/ meltfptr[16] =
	      newobj;
	  };
	  ;
	  /*^putslot */
	  /*putslot */
	  melt_assertmsg ("putslot checkobj @DBGI_OUT",
			  melt_magic_discr ((melt_ptr_t)
					    ( /*_.INST__V17*/ meltfptr[16]))
			  == MELTOBMAG_OBJECT);
	  melt_putfield_object (( /*_.INST__V17*/ meltfptr[16]), (0),
				( /*_.OUT__V5*/ meltfptr[4]), "DBGI_OUT");
	  ;
	  /*^putslot */
	  /*putslot */
	  melt_assertmsg ("putslot checkobj @DBGI_OCCMAP",
			  melt_magic_discr ((melt_ptr_t)
					    ( /*_.INST__V17*/ meltfptr[16]))
			  == MELTOBMAG_OBJECT);
	  melt_putfield_object (( /*_.INST__V17*/ meltfptr[16]), (1),
				( /*_.OCCMAP__V6*/ meltfptr[5]),
				"DBGI_OCCMAP");
	  ;
	  /*^putslot */
	  /*putslot */
	  melt_assertmsg ("putslot checkobj @DBGI_MAXDEPTH",
			  melt_magic_discr ((melt_ptr_t)
					    ( /*_.INST__V17*/ meltfptr[16]))
			  == MELTOBMAG_OBJECT);
	  melt_putfield_object (( /*_.INST__V17*/ meltfptr[16]), (2),
				( /*_.BOXEDMAXDEPTH__V7*/ meltfptr[6]),
				"DBGI_MAXDEPTH");
	  ;
	  /*^touchobj */

	  melt_dbgtrace_written_object ( /*_.INST__V17*/ meltfptr[16],
					"newly made instance");
	  ;
	  /*_.DBGI__V16*/ meltfptr[12] = /*_.INST__V17*/ meltfptr[16];;
	  /*^compute */
   /*_#FRAMDEPTH__L9*/ meltfnum[7] =
	    (melt_curframdepth ());;
	  /*^compute */
   /*_#INILEN__L10*/ meltfnum[9] =
	    melt_output_length ((melt_ptr_t) /*_.OUT__V5*/ meltfptr[4]);;

	  {
	    MELT_LOCATION ("warmelt-debug.melt:128:/ locexp");
	    meltgc_add_out ((melt_ptr_t) ( /*_.OUT__V5*/ meltfptr[4]),
			    ("!!!!****####"));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-debug.melt:129:/ locexp");
	    meltgc_add_out_dec ((melt_ptr_t) ( /*_.OUT__V5*/ meltfptr[4]),
				( /*_#DBGCOUNTER__L4*/ meltfnum[3]));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-debug.melt:130:/ locexp");
	    meltgc_add_out ((melt_ptr_t) ( /*_.OUT__V5*/ meltfptr[4]),
			    ("#^"));
	  }
	  ;
   /*_#I__L11*/ meltfnum[10] =
	    (( /*_#FRAMDEPTH__L9*/ meltfnum[7]) - (1));;

	  {
	    MELT_LOCATION ("warmelt-debug.melt:131:/ locexp");
	    meltgc_add_out_dec ((melt_ptr_t) ( /*_.OUT__V5*/ meltfptr[4]),
				( /*_#I__L11*/ meltfnum[10]));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-debug.melt:132:/ locexp");
	    meltgc_add_out ((melt_ptr_t) ( /*_.OUT__V5*/ meltfptr[4]), (":"));
	  }
	  ;
	  MELT_LOCATION ("warmelt-debug.melt:133:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^cond */
	  /*cond */ if ( /*_?*/ meltfram__.loc_CSTRING__o1)	/*then */
	    {
	      /*^cond.then */
	      /*^block */
	      /*anyblock */
	      {


		{
		  MELT_LOCATION ("warmelt-debug.melt:135:/ locexp");
		  meltgc_add_out ((melt_ptr_t) ( /*_.OUT__V5*/ meltfptr[4]),
				  ( /*_?*/ meltfram__.loc_CSTRING__o1));
		}
		;

		{
		  MELT_LOCATION ("warmelt-debug.melt:136:/ locexp");
		  meltgc_add_out ((melt_ptr_t) ( /*_.OUT__V5*/ meltfptr[4]),
				  (":"));
		}
		;

		{
		  MELT_LOCATION ("warmelt-debug.melt:137:/ locexp");
		  meltgc_add_out_dec ((melt_ptr_t)
				      ( /*_.OUT__V5*/ meltfptr[4]),
				      ( /*_#LINENO__L2*/ meltfnum[1]));
		}
		;

		{
		  MELT_LOCATION ("warmelt-debug.melt:138:/ locexp");
		  meltgc_add_out ((melt_ptr_t) ( /*_.OUT__V5*/ meltfptr[4]),
				  (":"));
		}
		;
		MELT_LOCATION ("warmelt-debug.melt:134:/ quasiblock");


		/*epilog */
	      }
	      ;
	    }			/*noelse */
	  ;

	  {
	    MELT_LOCATION ("warmelt-debug.melt:140:/ locexp");
	    meltgc_add_out ((melt_ptr_t) ( /*_.OUT__V5*/ meltfptr[4]),
			    ( /*_?*/ meltfram__.loc_CSTRING__o0));
	  }
	  ;
	  MELT_LOCATION ("warmelt-debug.melt:141:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
   /*_#I__L12*/ meltfnum[11] =
	    (( /*_#COUNT__L1*/ meltfnum[0]) > (0));;
	  MELT_LOCATION ("warmelt-debug.melt:141:/ cond");
	  /*cond */ if ( /*_#I__L12*/ meltfnum[11])	/*then */
	    {
	      /*^cond.then */
	      /*^block */
	      /*anyblock */
	      {


		{
		  MELT_LOCATION ("warmelt-debug.melt:143:/ locexp");
		  meltgc_add_out ((melt_ptr_t) ( /*_.OUT__V5*/ meltfptr[4]),
				  (" !"));
		}
		;

		{
		  MELT_LOCATION ("warmelt-debug.melt:144:/ locexp");
		  meltgc_add_out_dec ((melt_ptr_t)
				      ( /*_.OUT__V5*/ meltfptr[4]),
				      ( /*_#COUNT__L1*/ meltfnum[0]));
		}
		;

		{
		  MELT_LOCATION ("warmelt-debug.melt:145:/ locexp");
		  meltgc_add_out ((melt_ptr_t) ( /*_.OUT__V5*/ meltfptr[4]),
				  (": "));
		}
		;
		MELT_LOCATION ("warmelt-debug.melt:142:/ quasiblock");


		/*epilog */
	      }
	      ;
	    }			/*noelse */
	  ;
	  MELT_LOCATION ("warmelt-debug.melt:147:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^cond */
	  /*cond */ if ( /*_.VAL__V2*/ meltfptr[1])	/*then */
	    {
	      /*^cond.then */
	      /*^block */
	      /*anyblock */
	      {

		MELT_LOCATION ("warmelt-debug.melt:148:/ checksignal");
		MELT_CHECK_SIGNAL ();
		;
		/*^apply */
		/*apply */
		{
		  union meltparam_un argtab[2];
		  memset (&argtab, 0, sizeof (argtab));
		  /*^apply.arg */
		  argtab[0].meltbp_aptr =
		    (melt_ptr_t *) & /*_.DBGI__V16*/ meltfptr[12];
		  /*^apply.arg */
		  argtab[1].meltbp_long = 0;
		  /*_.DBG_OUT__V19*/ meltfptr[18] =
		    melt_apply ((meltclosure_ptr_t)
				(( /*!DBG_OUT */ meltfrout->tabval[6])),
				(melt_ptr_t) ( /*_.VAL__V2*/ meltfptr[1]),
				(MELTBPARSTR_PTR MELTBPARSTR_LONG ""), argtab,
				"", (union meltparam_un *) 0);
		}
		;
		/*_.IFELSE___V18*/ meltfptr[17] =
		  /*_.DBG_OUT__V19*/ meltfptr[18];;
		/*epilog */

		MELT_LOCATION ("warmelt-debug.melt:147:/ clear");
	       /*clear *//*_.DBG_OUT__V19*/ meltfptr[18] = 0;
	      }
	      ;
	    }
	  else
	    {			/*^cond.else */

	      /*^block */
	      /*anyblock */
	      {




		{
		  MELT_LOCATION ("warmelt-debug.melt:149:/ locexp");
		  meltgc_add_out ((melt_ptr_t) ( /*_.OUT__V5*/ meltfptr[4]),
				  ("() ;;;NIL!!!"));
		}
		;
	       /*clear *//*_.IFELSE___V18*/ meltfptr[17] = 0;
		/*epilog */
	      }
	      ;
	    }
	  ;

	  {
	    MELT_LOCATION ("warmelt-debug.melt:151:/ locexp");
	    meltgc_out_add_indent ((melt_ptr_t) ( /*_.OUT__V5*/ meltfptr[4]),
				   (0), 0);;
	  }
	  ;
	  MELT_LOCATION ("warmelt-debug.melt:152:/ quasiblock");


   /*_#FINALLEN__L13*/ meltfnum[12] =
	    melt_output_length ((melt_ptr_t) /*_.OUT__V5*/ meltfptr[4]);;
	  MELT_LOCATION ("warmelt-debug.melt:154:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
   /*_#I__L14*/ meltfnum[13] =
	    (( /*_#INILEN__L10*/ meltfnum[9]) + (200));;
	  /*^compute */
   /*_#I__L15*/ meltfnum[14] =
	    (( /*_#FINALLEN__L13*/ meltfnum[12]) >
	     ( /*_#I__L14*/ meltfnum[13]));;
	  MELT_LOCATION ("warmelt-debug.melt:154:/ cond");
	  /*cond */ if ( /*_#I__L15*/ meltfnum[14])	/*then */
	    {
	      /*^cond.then */
	      /*^block */
	      /*anyblock */
	      {


		{
		  MELT_LOCATION ("warmelt-debug.melt:156:/ locexp");
		  meltgc_add_out ((melt_ptr_t) ( /*_.OUT__V5*/ meltfptr[4]),
				  (" .###"));
		}
		;

		{
		  MELT_LOCATION ("warmelt-debug.melt:157:/ locexp");
		  meltgc_out_add_indent ((melt_ptr_t)
					 ( /*_.OUT__V5*/ meltfptr[4]), (0),
					 0);;
		}
		;
		MELT_LOCATION ("warmelt-debug.melt:155:/ quasiblock");


		/*epilog */
	      }
	      ;
	    }			/*noelse */
	  ;

	  MELT_LOCATION ("warmelt-debug.melt:152:/ clear");
	     /*clear *//*_#FINALLEN__L13*/ meltfnum[12] = 0;
	  /*^clear */
	     /*clear *//*_#I__L14*/ meltfnum[13] = 0;
	  /*^clear */
	     /*clear *//*_#I__L15*/ meltfnum[14] = 0;
	  MELT_LOCATION ("warmelt-debug.melt:159:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
   /*_#IS_STRBUF__L16*/ meltfnum[12] =
	    (melt_magic_discr ((melt_ptr_t) ( /*_.OUT__V5*/ meltfptr[4])) ==
	     MELTOBMAG_STRBUF);;
	  MELT_LOCATION ("warmelt-debug.melt:159:/ cond");
	  /*cond */ if ( /*_#IS_STRBUF__L16*/ meltfnum[12])	/*then */
	    {
	      /*^cond.then */
	      /*^block */
	      /*anyblock */
	      {


		{
		  MELT_LOCATION ("warmelt-debug.melt:161:/ locexp");
		  /* debug_msg_fun DEBUGSTRBUF__1 */
		  fprintf (stderr,
			   "\n**debug_msg thru strbuffer:\n%s\n",
			   melt_strbuf_str ((melt_ptr_t) /*_.OUT__V5*/
					    meltfptr[4]));
		  fflush (stderr);
		  ;
		}
		;
		MELT_LOCATION ("warmelt-debug.melt:160:/ quasiblock");


		/*epilog */
	      }
	      ;
	    }			/*noelse */
	  ;

	  MELT_LOCATION ("warmelt-debug.melt:120:/ clear");
	     /*clear *//*_.DBGI__V16*/ meltfptr[12] = 0;
	  /*^clear */
	     /*clear *//*_#FRAMDEPTH__L9*/ meltfnum[7] = 0;
	  /*^clear */
	     /*clear *//*_#INILEN__L10*/ meltfnum[9] = 0;
	  /*^clear */
	     /*clear *//*_#I__L11*/ meltfnum[10] = 0;
	  /*^clear */
	     /*clear *//*_#I__L12*/ meltfnum[11] = 0;
	  /*^clear */
	     /*clear *//*_.IFELSE___V18*/ meltfptr[17] = 0;
	  /*^clear */
	     /*clear *//*_#IS_STRBUF__L16*/ meltfnum[12] = 0;

	  MELT_LOCATION ("warmelt-debug.melt:90:/ clear");
	     /*clear *//*_#DBGCOUNTER__L4*/ meltfnum[3] = 0;
	  /*^clear */
	     /*clear *//*_.DUMPF__V3*/ meltfptr[2] = 0;
	  /*^clear */
	     /*clear *//*_.STDERRF__V4*/ meltfptr[3] = 0;
	  /*^clear */
	     /*clear *//*_.OUT__V5*/ meltfptr[4] = 0;
	  /*^clear */
	     /*clear *//*_.OCCMAP__V6*/ meltfptr[5] = 0;
	  /*^clear */
	     /*clear *//*_#DEBUG_DEPTH__L5*/ meltfnum[4] = 0;
	  /*^clear */
	     /*clear *//*_.BOXEDMAXDEPTH__V7*/ meltfptr[6] = 0;
	  /*^clear */
	     /*clear *//*_#NULL__L6*/ meltfnum[5] = 0;
	  /*^clear */
	     /*clear *//*_.IF___V8*/ meltfptr[7] = 0;
	  /*^clear */
	     /*clear *//*_#NULL__L7*/ meltfnum[6] = 0;
	  /*^clear */
	     /*clear *//*_.IF___V11*/ meltfptr[8] = 0;
	  /*^clear */
	     /*clear *//*_.IFCPP___V14*/ meltfptr[9] = 0;
	  /*epilog */
	}
	;
      }				/*noelse */
    ;
    MELT_LOCATION ("warmelt-debug.melt:84:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*epilog */

    /*^clear */
	   /*clear *//*_#MELT_HAS_FLAG_DEBUG_SET__L3*/ meltfnum[2] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("DEBUG_MSG_FUN", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_1_warmelt_debug_DEBUG_MSG_FUN_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*noretval */ NULL);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_1_warmelt_debug_DEBUG_MSG_FUN */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_2_warmelt_debug_REGISTER_TREE_DEBUG_FUN (meltclosure_ptr_t
						  meltclosp_,
						  melt_ptr_t meltfirstargp_,
						  const melt_argdescr_cell_t
						  meltxargdescr_[],
						  union meltparam_un *
						  meltxargtab_,
						  const melt_argdescr_cell_t
						  meltxresdescr_[],
						  union meltparam_un *
						  meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_2_warmelt_debug_REGISTER_TREE_DEBUG_FUN_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_2_warmelt_debug_REGISTER_TREE_DEBUG_FUN_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 4
    melt_ptr_t mcfr_varptr[4];
#define MELTFRAM_NBVARNUM 3
    long mcfr_varnum[3];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_2_warmelt_debug_REGISTER_TREE_DEBUG_FUN is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_2_warmelt_debug_REGISTER_TREE_DEBUG_FUN_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 4; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_2_warmelt_debug_REGISTER_TREE_DEBUG_FUN nbval 4*/
  meltfram__.mcfr_nbvar = 4 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("REGISTER_TREE_DEBUG_FUN", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-debug.melt:194:/ getarg");
 /*_.F__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {

    MELT_LOCATION ("warmelt-debug.melt:199:/ quasiblock");


    /*^cond */
    /*cond */ if (
		   /*ifisa */
		   melt_is_instance_of ((melt_ptr_t)
					(( /*!TREE_DEBUG_FUNCONT */
					  meltfrout->tabval[0])),
					(melt_ptr_t) (( /*!CLASS_REFERENCE */
						       meltfrout->tabval[1])))
      )				/*then */
      {
	/*^cond.then */
	/*^getslot */
	{
	  melt_ptr_t slot = NULL, obj = NULL;
	  obj =
	    (melt_ptr_t) (( /*!TREE_DEBUG_FUNCONT */ meltfrout->
			   tabval[0])) /*=obj*/ ;
	  melt_object_get_field (slot, obj, 0, "REFERENCED_VALUE");
   /*_.OLDCONT__V4*/ meltfptr[3] = slot;
	};
	;
      }
    else
      {				/*^cond.else */

  /*_.OLDCONT__V4*/ meltfptr[3] = NULL;;
      }
    ;
    MELT_LOCATION ("warmelt-debug.melt:201:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#NULL__L1*/ meltfnum[0] =
      (( /*_.F__V2*/ meltfptr[1]) == NULL);;
    MELT_LOCATION ("warmelt-debug.melt:201:/ cond");
    /*cond */ if ( /*_#NULL__L1*/ meltfnum[0])	/*then */
      {
	/*^cond.then */
	/*_#OR___L2*/ meltfnum[1] = /*_#NULL__L1*/ meltfnum[0];;
      }
    else
      {
	MELT_LOCATION ("warmelt-debug.melt:201:/ cond.else");

	/*^block */
	/*anyblock */
	{

   /*_#IS_CLOSURE__L3*/ meltfnum[2] =
	    (melt_magic_discr ((melt_ptr_t) ( /*_.F__V2*/ meltfptr[1])) ==
	     MELTOBMAG_CLOSURE);;
	  /*^compute */
	  /*_#OR___L2*/ meltfnum[1] = /*_#IS_CLOSURE__L3*/ meltfnum[2];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-debug.melt:201:/ clear");
	     /*clear *//*_#IS_CLOSURE__L3*/ meltfnum[2] = 0;
	}
	;
      }
    ;
    /*^cond */
    /*cond */ if ( /*_#OR___L2*/ meltfnum[1])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-debug.melt:202:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^cond */
	  /*cond */ if (
			 /*ifisa */
			 melt_is_instance_of ((melt_ptr_t)
					      (( /*!TREE_DEBUG_FUNCONT */
						meltfrout->tabval[0])),
					      (melt_ptr_t) (( /*!CLASS_REFERENCE */ meltfrout->tabval[1])))
	    )			/*then */
	    {
	      /*^cond.then */
	      /*^block */
	      /*anyblock */
	      {

		/*^putslot */
		/*putslot */
		melt_assertmsg ("putslot checkobj @REFERENCED_VALUE",
				melt_magic_discr ((melt_ptr_t)
						  (( /*!TREE_DEBUG_FUNCONT */
						    meltfrout->tabval[0]))) ==
				MELTOBMAG_OBJECT);
		melt_putfield_object ((( /*!TREE_DEBUG_FUNCONT */ meltfrout->
					tabval[0])), (0),
				      ( /*_.F__V2*/ meltfptr[1]),
				      "REFERENCED_VALUE");
		;
		/*^touch */
		meltgc_touch (( /*!TREE_DEBUG_FUNCONT */ meltfrout->
			       tabval[0]));
		;
		/*^touchobj */

		melt_dbgtrace_written_object (( /*!TREE_DEBUG_FUNCONT */
					       meltfrout->tabval[0]),
					      "put-fields");
		;
		/*epilog */
	      }
	      ;
	    }			/*noelse */
	  ;
	  /*epilog */
	}
	;
      }				/*noelse */
    ;
    /*_.LET___V3*/ meltfptr[2] = /*_.OLDCONT__V4*/ meltfptr[3];;

    MELT_LOCATION ("warmelt-debug.melt:199:/ clear");
	   /*clear *//*_.OLDCONT__V4*/ meltfptr[3] = 0;
    /*^clear */
	   /*clear *//*_#NULL__L1*/ meltfnum[0] = 0;
    /*^clear */
	   /*clear *//*_#OR___L2*/ meltfnum[1] = 0;
    MELT_LOCATION ("warmelt-debug.melt:194:/ quasiblock");


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.LET___V3*/ meltfptr[2];;

    {
      MELT_LOCATION ("warmelt-debug.melt:194:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.LET___V3*/ meltfptr[2] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("REGISTER_TREE_DEBUG_FUN", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_2_warmelt_debug_REGISTER_TREE_DEBUG_FUN_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_2_warmelt_debug_REGISTER_TREE_DEBUG_FUN */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_3_warmelt_debug_REGISTER_GIMPLE_DEBUG_FUN (meltclosure_ptr_t
						    meltclosp_,
						    melt_ptr_t meltfirstargp_,
						    const melt_argdescr_cell_t
						    meltxargdescr_[],
						    union meltparam_un *
						    meltxargtab_,
						    const melt_argdescr_cell_t
						    meltxresdescr_[],
						    union meltparam_un *
						    meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_3_warmelt_debug_REGISTER_GIMPLE_DEBUG_FUN_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_3_warmelt_debug_REGISTER_GIMPLE_DEBUG_FUN_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 4
    melt_ptr_t mcfr_varptr[4];
#define MELTFRAM_NBVARNUM 3
    long mcfr_varnum[3];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_3_warmelt_debug_REGISTER_GIMPLE_DEBUG_FUN is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_3_warmelt_debug_REGISTER_GIMPLE_DEBUG_FUN_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 4; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_3_warmelt_debug_REGISTER_GIMPLE_DEBUG_FUN nbval 4*/
  meltfram__.mcfr_nbvar = 4 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("REGISTER_GIMPLE_DEBUG_FUN", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-debug.melt:207:/ getarg");
 /*_.F__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {

    MELT_LOCATION ("warmelt-debug.melt:212:/ quasiblock");


    /*^cond */
    /*cond */ if (
		   /*ifisa */
		   melt_is_instance_of ((melt_ptr_t)
					(( /*!GIMPLE_DEBUG_FUNCONT */
					  meltfrout->tabval[0])),
					(melt_ptr_t) (( /*!CLASS_REFERENCE */
						       meltfrout->tabval[1])))
      )				/*then */
      {
	/*^cond.then */
	/*^getslot */
	{
	  melt_ptr_t slot = NULL, obj = NULL;
	  obj =
	    (melt_ptr_t) (( /*!GIMPLE_DEBUG_FUNCONT */ meltfrout->
			   tabval[0])) /*=obj*/ ;
	  melt_object_get_field (slot, obj, 0, "REFERENCED_VALUE");
   /*_.OLDCONT__V4*/ meltfptr[3] = slot;
	};
	;
      }
    else
      {				/*^cond.else */

  /*_.OLDCONT__V4*/ meltfptr[3] = NULL;;
      }
    ;
    MELT_LOCATION ("warmelt-debug.melt:214:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#NULL__L1*/ meltfnum[0] =
      (( /*_.F__V2*/ meltfptr[1]) == NULL);;
    MELT_LOCATION ("warmelt-debug.melt:214:/ cond");
    /*cond */ if ( /*_#NULL__L1*/ meltfnum[0])	/*then */
      {
	/*^cond.then */
	/*_#OR___L2*/ meltfnum[1] = /*_#NULL__L1*/ meltfnum[0];;
      }
    else
      {
	MELT_LOCATION ("warmelt-debug.melt:214:/ cond.else");

	/*^block */
	/*anyblock */
	{

   /*_#IS_CLOSURE__L3*/ meltfnum[2] =
	    (melt_magic_discr ((melt_ptr_t) ( /*_.F__V2*/ meltfptr[1])) ==
	     MELTOBMAG_CLOSURE);;
	  /*^compute */
	  /*_#OR___L2*/ meltfnum[1] = /*_#IS_CLOSURE__L3*/ meltfnum[2];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-debug.melt:214:/ clear");
	     /*clear *//*_#IS_CLOSURE__L3*/ meltfnum[2] = 0;
	}
	;
      }
    ;
    /*^cond */
    /*cond */ if ( /*_#OR___L2*/ meltfnum[1])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-debug.melt:215:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^cond */
	  /*cond */ if (
			 /*ifisa */
			 melt_is_instance_of ((melt_ptr_t)
					      (( /*!GIMPLE_DEBUG_FUNCONT */
						meltfrout->tabval[0])),
					      (melt_ptr_t) (( /*!CLASS_REFERENCE */ meltfrout->tabval[1])))
	    )			/*then */
	    {
	      /*^cond.then */
	      /*^block */
	      /*anyblock */
	      {

		/*^putslot */
		/*putslot */
		melt_assertmsg ("putslot checkobj @REFERENCED_VALUE",
				melt_magic_discr ((melt_ptr_t)
						  (( /*!GIMPLE_DEBUG_FUNCONT */ meltfrout->tabval[0]))) == MELTOBMAG_OBJECT);
		melt_putfield_object ((( /*!GIMPLE_DEBUG_FUNCONT */
					meltfrout->tabval[0])), (0),
				      ( /*_.F__V2*/ meltfptr[1]),
				      "REFERENCED_VALUE");
		;
		/*^touch */
		meltgc_touch (( /*!GIMPLE_DEBUG_FUNCONT */ meltfrout->
			       tabval[0]));
		;
		/*^touchobj */

		melt_dbgtrace_written_object (( /*!GIMPLE_DEBUG_FUNCONT */
					       meltfrout->tabval[0]),
					      "put-fields");
		;
		/*epilog */
	      }
	      ;
	    }			/*noelse */
	  ;
	  /*epilog */
	}
	;
      }				/*noelse */
    ;
    /*_.LET___V3*/ meltfptr[2] = /*_.OLDCONT__V4*/ meltfptr[3];;

    MELT_LOCATION ("warmelt-debug.melt:212:/ clear");
	   /*clear *//*_.OLDCONT__V4*/ meltfptr[3] = 0;
    /*^clear */
	   /*clear *//*_#NULL__L1*/ meltfnum[0] = 0;
    /*^clear */
	   /*clear *//*_#OR___L2*/ meltfnum[1] = 0;
    MELT_LOCATION ("warmelt-debug.melt:207:/ quasiblock");


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.LET___V3*/ meltfptr[2];;

    {
      MELT_LOCATION ("warmelt-debug.melt:207:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.LET___V3*/ meltfptr[2] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("REGISTER_GIMPLE_DEBUG_FUN", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_3_warmelt_debug_REGISTER_GIMPLE_DEBUG_FUN_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_3_warmelt_debug_REGISTER_GIMPLE_DEBUG_FUN */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_4_warmelt_debug_REGISTER_GIMPLESEQ_DEBUG_FUN (meltclosure_ptr_t
						       meltclosp_,
						       melt_ptr_t
						       meltfirstargp_,
						       const
						       melt_argdescr_cell_t
						       meltxargdescr_[],
						       union meltparam_un *
						       meltxargtab_,
						       const
						       melt_argdescr_cell_t
						       meltxresdescr_[],
						       union meltparam_un *
						       meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_4_warmelt_debug_REGISTER_GIMPLESEQ_DEBUG_FUN_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_4_warmelt_debug_REGISTER_GIMPLESEQ_DEBUG_FUN_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 4
    melt_ptr_t mcfr_varptr[4];
#define MELTFRAM_NBVARNUM 3
    long mcfr_varnum[3];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_4_warmelt_debug_REGISTER_GIMPLESEQ_DEBUG_FUN is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_4_warmelt_debug_REGISTER_GIMPLESEQ_DEBUG_FUN_st
	 *) meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 4; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_4_warmelt_debug_REGISTER_GIMPLESEQ_DEBUG_FUN nbval 4*/
  meltfram__.mcfr_nbvar = 4 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("REGISTER_GIMPLESEQ_DEBUG_FUN", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-debug.melt:220:/ getarg");
 /*_.F__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {

    MELT_LOCATION ("warmelt-debug.melt:225:/ quasiblock");


    /*^cond */
    /*cond */ if (
		   /*ifisa */
		   melt_is_instance_of ((melt_ptr_t)
					(( /*!GIMPLESEQ_DEBUG_FUNCONT */
					  meltfrout->tabval[0])),
					(melt_ptr_t) (( /*!CLASS_REFERENCE */
						       meltfrout->tabval[1])))
      )				/*then */
      {
	/*^cond.then */
	/*^getslot */
	{
	  melt_ptr_t slot = NULL, obj = NULL;
	  obj =
	    (melt_ptr_t) (( /*!GIMPLESEQ_DEBUG_FUNCONT */ meltfrout->
			   tabval[0])) /*=obj*/ ;
	  melt_object_get_field (slot, obj, 0, "REFERENCED_VALUE");
   /*_.OLDCONT__V4*/ meltfptr[3] = slot;
	};
	;
      }
    else
      {				/*^cond.else */

  /*_.OLDCONT__V4*/ meltfptr[3] = NULL;;
      }
    ;
    MELT_LOCATION ("warmelt-debug.melt:227:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#NULL__L1*/ meltfnum[0] =
      (( /*_.F__V2*/ meltfptr[1]) == NULL);;
    MELT_LOCATION ("warmelt-debug.melt:227:/ cond");
    /*cond */ if ( /*_#NULL__L1*/ meltfnum[0])	/*then */
      {
	/*^cond.then */
	/*_#OR___L2*/ meltfnum[1] = /*_#NULL__L1*/ meltfnum[0];;
      }
    else
      {
	MELT_LOCATION ("warmelt-debug.melt:227:/ cond.else");

	/*^block */
	/*anyblock */
	{

   /*_#IS_CLOSURE__L3*/ meltfnum[2] =
	    (melt_magic_discr ((melt_ptr_t) ( /*_.F__V2*/ meltfptr[1])) ==
	     MELTOBMAG_CLOSURE);;
	  /*^compute */
	  /*_#OR___L2*/ meltfnum[1] = /*_#IS_CLOSURE__L3*/ meltfnum[2];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-debug.melt:227:/ clear");
	     /*clear *//*_#IS_CLOSURE__L3*/ meltfnum[2] = 0;
	}
	;
      }
    ;
    /*^cond */
    /*cond */ if ( /*_#OR___L2*/ meltfnum[1])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-debug.melt:228:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^cond */
	  /*cond */ if (
			 /*ifisa */
			 melt_is_instance_of ((melt_ptr_t)
					      (( /*!GIMPLESEQ_DEBUG_FUNCONT */
						meltfrout->tabval[0])),
					      (melt_ptr_t) (( /*!CLASS_REFERENCE */ meltfrout->tabval[1])))
	    )			/*then */
	    {
	      /*^cond.then */
	      /*^block */
	      /*anyblock */
	      {

		/*^putslot */
		/*putslot */
		melt_assertmsg ("putslot checkobj @REFERENCED_VALUE",
				melt_magic_discr ((melt_ptr_t)
						  (( /*!GIMPLESEQ_DEBUG_FUNCONT */ meltfrout->tabval[0]))) == MELTOBMAG_OBJECT);
		melt_putfield_object ((( /*!GIMPLESEQ_DEBUG_FUNCONT */
					meltfrout->tabval[0])), (0),
				      ( /*_.F__V2*/ meltfptr[1]),
				      "REFERENCED_VALUE");
		;
		/*^touch */
		meltgc_touch (( /*!GIMPLESEQ_DEBUG_FUNCONT */ meltfrout->
			       tabval[0]));
		;
		/*^touchobj */

		melt_dbgtrace_written_object (( /*!GIMPLESEQ_DEBUG_FUNCONT */
					       meltfrout->tabval[0]),
					      "put-fields");
		;
		/*epilog */
	      }
	      ;
	    }			/*noelse */
	  ;
	  /*epilog */
	}
	;
      }				/*noelse */
    ;
    /*_.LET___V3*/ meltfptr[2] = /*_.OLDCONT__V4*/ meltfptr[3];;

    MELT_LOCATION ("warmelt-debug.melt:225:/ clear");
	   /*clear *//*_.OLDCONT__V4*/ meltfptr[3] = 0;
    /*^clear */
	   /*clear *//*_#NULL__L1*/ meltfnum[0] = 0;
    /*^clear */
	   /*clear *//*_#OR___L2*/ meltfnum[1] = 0;
    MELT_LOCATION ("warmelt-debug.melt:220:/ quasiblock");


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.LET___V3*/ meltfptr[2];;

    {
      MELT_LOCATION ("warmelt-debug.melt:220:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.LET___V3*/ meltfptr[2] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("REGISTER_GIMPLESEQ_DEBUG_FUN", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_4_warmelt_debug_REGISTER_GIMPLESEQ_DEBUG_FUN_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_4_warmelt_debug_REGISTER_GIMPLESEQ_DEBUG_FUN */






/****** emitted syntax checking for C generating devices *****/
#if MELT_HAVE_DEBUG

/* generated syntax checking routine for 4 C generating devices */
MELT_EXTERN void MELT_MODULE_VISIBILITY
melt_syntax_tester_warmelt_debug (void);

void
melt_syntax_tester_warmelt_debug (void)
{
  if (1)
    return;


/* generating device #1 */
/*primitive-syntax DEBUG_DEPTH*/
  {
    long primres_0 = (long) (melt_debug_depth ());
    if (primres_0)
      return;
  }				/*end primitive-syntax DEBUG_DEPTH */



/* generating device #2 */
/*primitive-syntax MELT_REALLY_NEED_DEBUG*/
  {
    long primf_0_DEPTH = 0;
    long primres_1 =
      /*MELT_REALLY_NEED_DEBUG */ (melt_need_debug ((int) primf_0_DEPTH));
    if (primres_1)
      return;
  }				/*end primitive-syntax MELT_REALLY_NEED_DEBUG */



/* generating device #3 */
/*primitive-syntax MELT_HAS_FLAG_DEBUG_SET*/
  {
    long primres_2 =
      /*MELT_HAS_FLAG_DEBUG_SET */ melt_flag_debug;
    if (primres_2)
      return;
  }				/*end primitive-syntax MELT_HAS_FLAG_DEBUG_SET */



/* generating device #4 */
/*primitive-syntax MELT_REALLY_NEED_DBGLIM*/
  {
    long primf_0_DEPTH = 0;
    long primf_1_LIMIT = 0;
    long primres_3 =
      ( /*MELT_REALLY_NEED_DBGLIM */
       melt_need_debug_limit ((int) primf_0_DEPTH, (int) primf_1_LIMIT));
    if (primres_3)
      return;
  }				/*end primitive-syntax MELT_REALLY_NEED_DBGLIM */


}				/* end generated melt_syntax_tester_warmelt_debug */
#endif /*MELT_HAVE_DEBUG syntaxcheck */




typedef struct frame_melt_start_this_module_st
{
  int mcfr_nbvar;
#if MELT_HAVE_DEBUG
  const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
  const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
  void (*mcfr_initforwmarkrout) (struct melt_callframe_st *, int);
  struct excepth_melt_st *mcfr_exh;
  struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 398
  melt_ptr_t mcfr_varptr[398];
/*no varnum*/
#define MELTFRAM_NBVARNUM /*none*/0
/*others*/
  long _spare_;
}
initial_frame_st;


static void
initialize_module_meltdata_warmelt_debug (initial_frame_st * iniframp__,
					  char meltpredefinited[])
{
#define meltfram__  (*iniframp__)

  struct cdata_st
  {
    struct MELT_OBJECT_STRUCT (3) dsym_1__CLASS_DEBUG_INFORMATION;
    struct MELT_STRING_STRUCT (23) dstr_2__CLASS_DEBUG_INFO;
    struct MELT_OBJECT_STRUCT (3) dsym_3__CLASS_ROOT;
    struct MELT_STRING_STRUCT (10) dstr_4__CLASS_ROOT;
    struct MELT_OBJECT_STRUCT (8) dobj_5__CLASS_DEBUG_INFORMATION;
    struct MELT_STRING_STRUCT (23) dstr_6__CLASS_DEBUG_INFO;
    struct MELT_MULTIPLE_STRUCT (1) dtup_7__CLASS_DEBUG_INFORMATION;
    struct MELT_MULTIPLE_STRUCT (3) dtup_8__CLASS_DEBUG_INFORMATION;
    struct MELT_OBJECT_STRUCT (3) dsym_9__DBGI_OUT;
    struct MELT_STRING_STRUCT (8) dstr_10__DBGI_OUT;
    struct MELT_OBJECT_STRUCT (4) dobj_11__DBGI_OUT;
    struct MELT_STRING_STRUCT (8) dstr_12__DBGI_OUT;
    struct MELT_OBJECT_STRUCT (3) dsym_13__DBGI_OCCMAP;
    struct MELT_STRING_STRUCT (11) dstr_14__DBGI_OCCMAP;
    struct MELT_OBJECT_STRUCT (4) dobj_15__DBGI_OCCMAP;
    struct MELT_STRING_STRUCT (11) dstr_16__DBGI_OCCMAP;
    struct MELT_OBJECT_STRUCT (3) dsym_17__DBGI_MAXDEPTH;
    struct MELT_STRING_STRUCT (13) dstr_18__DBGI_MAXDEPTH;
    struct MELT_OBJECT_STRUCT (4) dobj_19__DBGI_MAXDEPTH;
    struct MELT_STRING_STRUCT (13) dstr_20__DBGI_MAXDEPTH;
    struct MELT_OBJECT_STRUCT (3) dsym_21__CLASS_DEBUG_OUTPUT_INFORMATION;
    struct MELT_STRING_STRUCT (30) dstr_22__CLASS_DEBUG_OUTP;
    struct MELT_OBJECT_STRUCT (8) dobj_23__CLASS_DEBUG_OUTPUT_INFORMATION;
    struct MELT_STRING_STRUCT (30) dstr_24__CLASS_DEBUG_OUTP;
    struct MELT_MULTIPLE_STRUCT (2) dtup_25__CLASS_DEBUG_OUTPUT_INFORMATION;
    struct MELT_MULTIPLE_STRUCT (3) dtup_26__CLASS_DEBUG_OUTPUT_INFORMATION;
    struct MELT_OBJECT_STRUCT (3) dsym_27__DEBUG_DEPTH;
    struct MELT_STRING_STRUCT (11) dstr_28__DEBUG_DEPTH;
    struct MELT_OBJECT_STRUCT (5) dobj_29__DEBUG_DEPTH;
    struct MELT_MULTIPLE_STRUCT (1) dtup_30__DEBUG_DEPTH;
    struct MELT_MULTIPLE_STRUCT (0) dtup_31__DEBUG_DEPTH;
    struct MELT_STRING_STRUCT (28) dstr_32__long_melt_deb;
    struct MELT_STRING_STRUCT (11) dstr_33__DEBUG_DEPTH;
    struct MELT_OBJECT_STRUCT (3) dsym_34__MELT_REALLY_NEED_DEBUG;
    struct MELT_STRING_STRUCT (22) dstr_35__MELT_REALLY_NEED;
    struct MELT_OBJECT_STRUCT (5) dobj_36__MELT_REALLY_NEED_DEBUG;
    struct MELT_MULTIPLE_STRUCT (3) dtup_37__MELT_REALLY_NEED_DEBUG;
    struct MELT_MULTIPLE_STRUCT (1) dtup_38__MELT_REALLY_NEED_DEBUG;
    struct MELT_OBJECT_STRUCT (3) dsym_39__DEPTH;
    struct MELT_STRING_STRUCT (5) dstr_40__DEPTH;
    struct MELT_OBJECT_STRUCT (2) dobj_41;
    struct MELT_STRING_STRUCT (51) dstr_42__MELT_REALLY_NE;
    struct MELT_STRING_STRUCT (2) dstr_43__;
    struct MELT_STRING_STRUCT (22) dstr_44__MELT_REALLY_NEED;
    struct MELT_OBJECT_STRUCT (3) dsym_45__MELT_HAS_FLAG_DEBUG_SET;
    struct MELT_STRING_STRUCT (23) dstr_46__MELT_HAS_FLAG_DE;
    struct MELT_OBJECT_STRUCT (5) dobj_47__MELT_HAS_FLAG_DEBUG_SET;
    struct MELT_MULTIPLE_STRUCT (1) dtup_48__MELT_HAS_FLAG_DEBUG_SET;
    struct MELT_MULTIPLE_STRUCT (0) dtup_49__MELT_HAS_FLAG_DEBUG_SET;
    struct MELT_STRING_STRUCT (43) dstr_50__MELT_HAS_FLAG_;
    struct MELT_STRING_STRUCT (23) dstr_51__MELT_HAS_FLAG_DE;
    struct MELT_OBJECT_STRUCT (3) dsym_52__MELT_REALLY_NEED_DBGLIM;
    struct MELT_STRING_STRUCT (23) dstr_53__MELT_REALLY_NEED;
    struct MELT_OBJECT_STRUCT (5) dobj_54__MELT_REALLY_NEED_DBGLIM;
    struct MELT_MULTIPLE_STRUCT (5) dtup_55__MELT_REALLY_NEED_DBGLIM;
    struct MELT_MULTIPLE_STRUCT (2) dtup_56__MELT_REALLY_NEED_DBGLIM;
    struct MELT_OBJECT_STRUCT (2) dobj_57;
    struct MELT_OBJECT_STRUCT (3) dsym_58__LIMIT;
    struct MELT_STRING_STRUCT (5) dstr_59__LIMIT;
    struct MELT_OBJECT_STRUCT (2) dobj_60;
    struct MELT_STRING_STRUCT (59) dstr_61__MELT_REALLY_;
    struct MELT_STRING_STRUCT (8) dstr_62__int_;
    struct MELT_STRING_STRUCT (2) dstr_63__;
    struct MELT_STRING_STRUCT (23) dstr_64__MELT_REALLY_NEED;
    struct MELT_ROUTINE_STRUCT (7) drout_65__DEBUG_MSG_FUN;
    struct MELT_CLOSURE_STRUCT (0) dclo_66__DEBUG_MSG_FUN;
    struct MELT_OBJECT_STRUCT (3) dsym_67__INITIAL_SYSTEM_DATA;
    struct MELT_STRING_STRUCT (19) dstr_68__INITIAL_SYSTEM_D;
    struct MELT_OBJECT_STRUCT (3) dsym_69__CLASS_SYSTEM_DATA;
    struct MELT_STRING_STRUCT (17) dstr_70__CLASS_SYSTEM_DAT;
    struct MELT_OBJECT_STRUCT (3) dsym_71__DISCR_MAP_OBJECTS;
    struct MELT_STRING_STRUCT (17) dstr_72__DISCR_MAP_OBJECT;
    struct MELT_OBJECT_STRUCT (3) dsym_73__DISCR_INTEGER;
    struct MELT_STRING_STRUCT (13) dstr_74__DISCR_INTEGER;
    struct MELT_OBJECT_STRUCT (3) dsym_75__DISCR_STRBUF;
    struct MELT_STRING_STRUCT (12) dstr_76__DISCR_STRBUF;
    struct MELT_OBJECT_STRUCT (3) dsym_77__DBG_OUTPUT;
    struct MELT_STRING_STRUCT (10) dstr_78__DBG_OUTPUT;
    struct MELT_OBJECT_STRUCT (3) dsym_79__CLASS_SELECTOR;
    struct MELT_STRING_STRUCT (14) dstr_80__CLASS_SELECTOR;
    struct MELT_OBJECT_STRUCT (3) dsym_81__RECV;
    struct MELT_STRING_STRUCT (4) dstr_82__RECV;
    struct MELT_OBJECT_STRUCT (2) dobj_83;
    struct MELT_OBJECT_STRUCT (3) dsym_84__DBGINFO;
    struct MELT_STRING_STRUCT (7) dstr_85__DBGINFO;
    struct MELT_OBJECT_STRUCT (2) dobj_86;
    struct MELT_OBJECT_STRUCT (2) dobj_87;
    struct MELT_MULTIPLE_STRUCT (3) dtup_88__DBG_OUTPUT;
    struct MELT_OBJECT_STRUCT (4) dobj_89__DBG_OUTPUT;
    struct MELT_STRING_STRUCT (10) dstr_90__DBG_OUTPUT;
    struct MELT_OBJECT_STRUCT (3) dsym_91__DBG_OUTPUTAGAIN;
    struct MELT_STRING_STRUCT (15) dstr_92__DBG_OUTPUTAGAIN;
    struct MELT_OBJECT_STRUCT (2) dobj_93;
    struct MELT_OBJECT_STRUCT (2) dobj_94;
    struct MELT_OBJECT_STRUCT (2) dobj_95;
    struct MELT_MULTIPLE_STRUCT (3) dtup_96__DBG_OUTPUTAGAIN;
    struct MELT_OBJECT_STRUCT (4) dobj_97__DBG_OUTPUTAGAIN;
    struct MELT_STRING_STRUCT (15) dstr_98__DBG_OUTPUTAGAIN;
    struct MELT_OBJECT_STRUCT (3) dsym_99__TREE_DEBUG_FUNCONT;
    struct MELT_STRING_STRUCT (18) dstr_100__TREE_DEBUG_FUNCO;
    struct MELT_OBJECT_STRUCT (3) dsym_101__CLASS_REFERENCE;
    struct MELT_STRING_STRUCT (15) dstr_102__CLASS_REFERENCE;
    struct MELT_OBJECT_STRUCT (1) dobj_103__TREE_DEBUG_FUNCONT;
    struct MELT_ROUTINE_STRUCT (2) drout_104__REGISTER_TREE_DEBUG_FUN;
    struct MELT_CLOSURE_STRUCT (0) dclo_105__REGISTER_TREE_DEBUG_FUN;
    struct MELT_OBJECT_STRUCT (3) dsym_106__GIMPLE_DEBUG_FUNCONT;
    struct MELT_STRING_STRUCT (20) dstr_107__GIMPLE_DEBUG_FUN;
    struct MELT_OBJECT_STRUCT (1) dobj_108__GIMPLE_DEBUG_FUNCONT;
    struct MELT_ROUTINE_STRUCT (2) drout_109__REGISTER_GIMPLE_DEBUG_FUN;
    struct MELT_CLOSURE_STRUCT (0) dclo_110__REGISTER_GIMPLE_DEBUG_FUN;
    struct MELT_OBJECT_STRUCT (3) dsym_111__GIMPLESEQ_DEBUG_FUNCONT;
    struct MELT_STRING_STRUCT (23) dstr_112__GIMPLESEQ_DEBUG_;
    struct MELT_OBJECT_STRUCT (1) dobj_113__GIMPLESEQ_DEBUG_FUNCONT;
    struct MELT_ROUTINE_STRUCT (2) drout_114__REGISTER_GIMPLESEQ_DEBUG_FUN;
    struct MELT_CLOSURE_STRUCT (0) dclo_115__REGISTER_GIMPLESEQ_DEBUG_FUN;
    struct MELT_OBJECT_STRUCT (3) dsym_116__EDGE_DEBUG_FUNCONT;
    struct MELT_STRING_STRUCT (18) dstr_117__EDGE_DEBUG_FUNCO;
    struct MELT_OBJECT_STRUCT (1) dobj_118__EDGE_DEBUG_FUNCONT;
    struct MELT_ROUTINE_STRUCT (2) drout_119__REGISTER_EDGE_DEBUG_FUN;
    struct MELT_CLOSURE_STRUCT (0) dclo_120__REGISTER_EDGE_DEBUG_FUN;
    struct MELT_OBJECT_STRUCT (3) dsym_121__BASICBLOCK_DEBUG_FUNCONT;
    struct MELT_STRING_STRUCT (24) dstr_122__BASICBLOCK_DEBUG;
    struct MELT_OBJECT_STRUCT (1) dobj_123__BASICBLOCK_DEBUG_FUNCONT;
    struct MELT_ROUTINE_STRUCT (2) drout_124__REGISTER_BASICBLOCK_DEBUG_FUN;
    struct MELT_CLOSURE_STRUCT (0) dclo_125__REGISTER_BASICBLOCK_DEBUG_FUN;
    struct MELT_OBJECT_STRUCT (3) dsym_126__LOOP_DEBUG_FUNCONT;
    struct MELT_STRING_STRUCT (18) dstr_127__LOOP_DEBUG_FUNCO;
    struct MELT_OBJECT_STRUCT (1) dobj_128__LOOP_DEBUG_FUNCONT;
    struct MELT_ROUTINE_STRUCT (2) drout_129__REGISTER_LOOP_DEBUG_FUN;
    struct MELT_CLOSURE_STRUCT (0) dclo_130__REGISTER_LOOP_DEBUG_FUN;
    struct MELT_OBJECT_STRUCT (3) dsym_131__DISCR_DEBUG_CLOSURE;
    struct MELT_STRING_STRUCT (19) dstr_132__DISCR_DEBUG_CLOS;
    struct MELT_OBJECT_STRUCT (3) dsym_133__CLASS_DISCRIMINANT;
    struct MELT_STRING_STRUCT (18) dstr_134__CLASS_DISCRIMINA;
    struct MELT_OBJECT_STRUCT (5) dobj_135__DISCR_DEBUG_CLOSURE;
    struct MELT_OBJECT_STRUCT (3) dsym_136__DISCR_CLOSURE;
    struct MELT_STRING_STRUCT (13) dstr_137__DISCR_CLOSURE;
    struct MELT_STRING_STRUCT (19) dstr_138__DISCR_DEBUG_CLOS;
    struct MELT_ROUTINE_STRUCT (19) drout_139__MELT_DEBUG_FUN;
    struct MELT_CLOSURE_STRUCT (0) dclo_140__MELT_DEBUG_FUN;
    struct MELT_OBJECT_STRUCT (3) dsym_141__ADD2OUT;
    struct MELT_STRING_STRUCT (7) dstr_142__ADD2OUT;
    struct MELT_OBJECT_STRUCT (3) dsym_143__CSTRING_PREFIXED;
    struct MELT_STRING_STRUCT (16) dstr_144__CSTRING_PREFIXED;
    struct MELT_OBJECT_STRUCT (3) dsym_145__CLASS_NAMED;
    struct MELT_STRING_STRUCT (11) dstr_146__CLASS_NAMED;
    struct MELT_OBJECT_STRUCT (3) dsym_147__CLASS_CTYPE;
    struct MELT_STRING_STRUCT (11) dstr_148__CLASS_CTYPE;
    struct MELT_ROUTINE_STRUCT (4) drout_149__DBG_OUTOBJECT;
    struct MELT_CLOSURE_STRUCT (0) dclo_150__DBG_OUTOBJECT;
    struct MELT_ROUTINE_STRUCT (4) drout_151__DBG_OUT;
    struct MELT_CLOSURE_STRUCT (0) dclo_152__DBG_OUT;
    struct MELT_ROUTINE_STRUCT (3) drout_153__DBGOUT_FIELDS;
    struct MELT_CLOSURE_STRUCT (0) dclo_154__DBGOUT_FIELDS;
    struct MELT_ROUTINE_STRUCT (3) drout_155__DBGOUTAGAIN_FIELDS;
    struct MELT_CLOSURE_STRUCT (0) dclo_156__DBGOUTAGAIN_FIELDS;
    struct MELT_ROUTINE_STRUCT (0) drout_157__DBGOUT_NULL_METHOD;
    struct MELT_CLOSURE_STRUCT (0) dclo_158__DBGOUT_NULL_METHOD;
    struct MELT_OBJECT_STRUCT (3) dsym_159__INSTALL_METHOD;
    struct MELT_STRING_STRUCT (14) dstr_160__INSTALL_METHOD;
    struct MELT_OBJECT_STRUCT (3) dsym_161__DISCR_NULL_RECEIVER;
    struct MELT_STRING_STRUCT (19) dstr_162__DISCR_NULL_RECEI;
    struct MELT_ROUTINE_STRUCT (2) drout_163__DBGOUT_STRING_METHOD;
    struct MELT_CLOSURE_STRUCT (0) dclo_164__DBGOUT_STRING_METHOD;
    struct MELT_OBJECT_STRUCT (3) dsym_165__DISCR_STRING;
    struct MELT_STRING_STRUCT (12) dstr_166__DISCR_STRING;
    struct MELT_ROUTINE_STRUCT (3) drout_167__DBGOUT_STRBUF_METHOD;
    struct MELT_CLOSURE_STRUCT (0) dclo_168__DBGOUT_STRBUF_METHOD;
    struct MELT_ROUTINE_STRUCT (4) drout_169__DBGOUT_INTEGER_METHOD;
    struct MELT_CLOSURE_STRUCT (0) dclo_170__DBGOUT_INTEGER_METHOD;
    struct MELT_OBJECT_STRUCT (3) dsym_171__DISCR_CONSTANT_INTEGER;
    struct MELT_STRING_STRUCT (22) dstr_172__DISCR_CONSTANT_I;
    struct MELT_ROUTINE_STRUCT (4) drout_173__DBGOUT_MIXINT_METHOD;
    struct MELT_CLOSURE_STRUCT (0) dclo_174__DBGOUT_MIXINT_METHOD;
    struct MELT_OBJECT_STRUCT (3) dsym_175__DISCR_MIXED_INTEGER;
    struct MELT_STRING_STRUCT (19) dstr_176__DISCR_MIXED_INTE;
    struct MELT_ROUTINE_STRUCT (3) drout_177__DBGOUT_MIXLOC_METHOD;
    struct MELT_CLOSURE_STRUCT (0) dclo_178__DBGOUT_MIXLOC_METHOD;
    struct MELT_OBJECT_STRUCT (3) dsym_179__DISCR_MIXED_LOCATION;
    struct MELT_STRING_STRUCT (20) dstr_180__DISCR_MIXED_LOCA;
    struct MELT_ROUTINE_STRUCT (3) drout_181__DBGOUT_MIXBIGINT_METHOD;
    struct MELT_CLOSURE_STRUCT (0) dclo_182__DBGOUT_MIXBIGINT_METHOD;
    struct MELT_OBJECT_STRUCT (3) dsym_183__DISCR_MIXED_BIGINT;
    struct MELT_STRING_STRUCT (18) dstr_184__DISCR_MIXED_BIGI;
    struct MELT_ROUTINE_STRUCT (3) drout_185__DBGOUT_MULTIPLE_METHOD;
    struct MELT_CLOSURE_STRUCT (0) dclo_186__DBGOUT_MULTIPLE_METHOD;
    struct MELT_OBJECT_STRUCT (3) dsym_187__DISCR_MULTIPLE;
    struct MELT_STRING_STRUCT (14) dstr_188__DISCR_MULTIPLE;
    struct MELT_ROUTINE_STRUCT (6) drout_189__DBGOUT_ROUTINE_METHOD;
    struct MELT_CLOSURE_STRUCT (0) dclo_190__DBGOUT_ROUTINE_METHOD;
    struct MELT_OBJECT_STRUCT (3) dsym_191__DISCR_ROUTINE;
    struct MELT_STRING_STRUCT (13) dstr_192__DISCR_ROUTINE;
    struct MELT_OBJECT_STRUCT (3) dsym_193__ROUTINE_EVERY;
    struct MELT_STRING_STRUCT (13) dstr_194__ROUTINE_EVERY;
    struct MELT_ROUTINE_STRUCT (1) drout_195__LAMBDA_;
    struct MELT_ROUTINE_STRUCT (4) drout_196__DBGOUT_CLOSURE_METHOD;
    struct MELT_CLOSURE_STRUCT (0) dclo_197__DBGOUT_CLOSURE_METHOD;
    struct MELT_ROUTINE_STRUCT (4) drout_198__DBGOUT_LIST_METHOD;
    struct MELT_CLOSURE_STRUCT (0) dclo_199__DBGOUT_LIST_METHOD;
    struct MELT_OBJECT_STRUCT (3) dsym_200__DISCR_LIST;
    struct MELT_STRING_STRUCT (10) dstr_201__DISCR_LIST;
    struct MELT_ROUTINE_STRUCT (4) drout_202__DBGOUT_PAIR_METHOD;
    struct MELT_CLOSURE_STRUCT (0) dclo_203__DBGOUT_PAIR_METHOD;
    struct MELT_OBJECT_STRUCT (3) dsym_204__DISCR_PAIR;
    struct MELT_STRING_STRUCT (10) dstr_205__DISCR_PAIR;
    struct MELT_ROUTINE_STRUCT (16) drout_206__DBGOUT_MAPOBJECT_METHOD;
    struct MELT_CLOSURE_STRUCT (0) dclo_207__DBGOUT_MAPOBJECT_METHOD;
    struct MELT_OBJECT_STRUCT (3) dsym_208__MAPOBJECT_EVERY;
    struct MELT_STRING_STRUCT (15) dstr_209__MAPOBJECT_EVERY;
    struct MELT_ROUTINE_STRUCT (1) drout_210__LAMBDA_;
    struct MELT_ROUTINE_STRUCT (4) drout_211__LAMBDA_;
    struct MELT_OBJECT_STRUCT (3) dsym_212__COMPARE_OBJ_RANKED;
    struct MELT_STRING_STRUCT (18) dstr_213__COMPARE_OBJ_RANK;
    struct meltint_st dint_214__;
    struct meltint_st dint_215__;
    struct meltint_st dint_216__;
    struct MELT_OBJECT_STRUCT (3) dsym_217__MULTIPLE_EVERY;
    struct MELT_STRING_STRUCT (14) dstr_218__MULTIPLE_EVERY;
    struct MELT_ROUTINE_STRUCT (2) drout_219__LAMBDA_;
    struct MELT_ROUTINE_STRUCT (15) drout_220__DBGOUT_MAPSTRING_METHOD;
    struct MELT_CLOSURE_STRUCT (0) dclo_221__DBGOUT_MAPSTRING_METHOD;
    struct MELT_OBJECT_STRUCT (3) dsym_222__DISCR_MAP_STRINGS;
    struct MELT_STRING_STRUCT (17) dstr_223__DISCR_MAP_STRING;
    struct MELT_OBJECT_STRUCT (3) dsym_224__MAPSTRING_EVERY;
    struct MELT_STRING_STRUCT (15) dstr_225__MAPSTRING_EVERY;
    struct MELT_ROUTINE_STRUCT (1) drout_226__LAMBDA_;
    struct MELT_ROUTINE_STRUCT (4) drout_227__LAMBDA_;
    struct meltint_st dint_228__;
    struct meltint_st dint_229__;
    struct meltint_st dint_230__;
    struct MELT_ROUTINE_STRUCT (1) drout_231__LAMBDA_;
    struct MELT_ROUTINE_STRUCT (5) drout_232__DBGOUT_BUCKETLONG_METHOD;
    struct MELT_CLOSURE_STRUCT (0) dclo_233__DBGOUT_BUCKETLONG_METHOD;
    struct MELT_OBJECT_STRUCT (3) dsym_234__DISCR_BUCKET_LONGS;
    struct MELT_STRING_STRUCT (18) dstr_235__DISCR_BUCKET_LON;
    struct MELT_ROUTINE_STRUCT (3) drout_236__DBGOUT_ANYOBJECT_METHOD;
    struct MELT_CLOSURE_STRUCT (0) dclo_237__DBGOUT_ANYOBJECT_METHOD;
    struct MELT_ROUTINE_STRUCT (2) drout_238__DBGOUT_ANYRECV_METHOD;
    struct MELT_CLOSURE_STRUCT (0) dclo_239__DBGOUT_ANYRECV_METHOD;
    struct MELT_OBJECT_STRUCT (3) dsym_240__DISCR_ANY_RECEIVER;
    struct MELT_STRING_STRUCT (18) dstr_241__DISCR_ANY_RECEIV;
    struct MELT_ROUTINE_STRUCT (1) drout_242__DBGOUTAGAIN_ANYOBJECT_METHOD;
    struct MELT_CLOSURE_STRUCT (0) dclo_243__DBGOUTAGAIN_ANYOBJECT_METHOD;
    struct MELT_ROUTINE_STRUCT (4) drout_244__DBGOUT_NAMEDOBJECT_METHOD;
    struct MELT_CLOSURE_STRUCT (0) dclo_245__DBGOUT_NAMEDOBJECT_METHOD;
    struct MELT_OBJECT_STRUCT (3) dsym_246__DBGOUT_NAMEDOBJECT_METHOD;
    struct MELT_STRING_STRUCT (25) dstr_247__DBGOUT_NAMEDOBJE;
    struct MELT_ROUTINE_STRUCT (1) drout_248__DBGOUTAGAIN_NAMEDOBJECT_METHOD;
    struct MELT_CLOSURE_STRUCT (0) dclo_249__DBGOUTAGAIN_NAMEDOBJECT_METHOD;
    struct MELT_ROUTINE_STRUCT (2) drout_250__DBGOUTAGAIN_SYMBOL_METHOD;
    struct MELT_CLOSURE_STRUCT (0) dclo_251__DBGOUTAGAIN_SYMBOL_METHOD;
    struct MELT_OBJECT_STRUCT (3) dsym_252__CLASS_SYMBOL;
    struct MELT_STRING_STRUCT (12) dstr_253__CLASS_SYMBOL;
    struct MELT_ROUTINE_STRUCT (4) drout_254__DBGOUT_SYMBOL_METHOD;
    struct MELT_CLOSURE_STRUCT (0) dclo_255__DBGOUT_SYMBOL_METHOD;
    struct MELT_ROUTINE_STRUCT (4) drout_256__DBGOUT_CLASS_METHOD;
    struct MELT_CLOSURE_STRUCT (0) dclo_257__DBGOUT_CLASS_METHOD;
    struct MELT_OBJECT_STRUCT (3) dsym_258__CLASS_CLASS;
    struct MELT_STRING_STRUCT (11) dstr_259__CLASS_CLASS;
    struct MELT_ROUTINE_STRUCT (2) drout_260__DBGOUTAGAIN_KEYWORD_METHOD;
    struct MELT_CLOSURE_STRUCT (0) dclo_261__DBGOUTAGAIN_KEYWORD_METHOD;
    struct MELT_OBJECT_STRUCT (3) dsym_262__CLASS_KEYWORD;
    struct MELT_STRING_STRUCT (13) dstr_263__CLASS_KEYWORD;
    struct MELT_ROUTINE_STRUCT (4) drout_264__DBGOUT_KEYWORD_METHOD;
    struct MELT_CLOSURE_STRUCT (0) dclo_265__DBGOUT_KEYWORD_METHOD;
    struct MELT_ROUTINE_STRUCT (0)
      drout_266__DBGOUTAGAIN_CLONED_SYMBOL_METHOD;
    struct MELT_CLOSURE_STRUCT (0) dclo_267__DBGOUTAGAIN_CLONED_SYMBOL_METHOD;
    struct MELT_OBJECT_STRUCT (3) dsym_268__CLASS_CLONED_SYMBOL;
    struct MELT_STRING_STRUCT (19) dstr_269__CLASS_CLONED_SYM;
    struct MELT_ROUTINE_STRUCT (4) drout_270__DBGOUT_CLONED_SYMBOL_METHOD;
    struct MELT_CLOSURE_STRUCT (0) dclo_271__DBGOUT_CLONED_SYMBOL_METHOD;
    struct MELT_ROUTINE_STRUCT (6) drout_272__DBGOUT_ENVIRONMENT_METHOD;
    struct MELT_CLOSURE_STRUCT (0) dclo_273__DBGOUT_ENVIRONMENT_METHOD;
    struct MELT_OBJECT_STRUCT (3) dsym_274__CLASS_ENVIRONMENT;
    struct MELT_STRING_STRUCT (17) dstr_275__CLASS_ENVIRONMEN;
    struct MELT_OBJECT_STRUCT (3) dsym_276__ENV_PREV;
    struct MELT_STRING_STRUCT (8) dstr_277__ENV_PREV;
    struct MELT_ROUTINE_STRUCT (0) drout_278__DBGOUTAGAIN_CTYPE_METHOD;
    struct MELT_CLOSURE_STRUCT (0) dclo_279__DBGOUTAGAIN_CTYPE_METHOD;
    struct MELT_ROUTINE_STRUCT (4) drout_280__DBGOUT_CTYPE_METHOD;
    struct MELT_CLOSURE_STRUCT (0) dclo_281__DBGOUT_CTYPE_METHOD;
    struct MELT_ROUTINE_STRUCT (5) drout_282__DBGOUT_ANYBINDING_METHOD;
    struct MELT_CLOSURE_STRUCT (0) dclo_283__DBGOUT_ANYBINDING_METHOD;
    struct MELT_OBJECT_STRUCT (3) dsym_284__CLASS_ANY_BINDING;
    struct MELT_STRING_STRUCT (17) dstr_285__CLASS_ANY_BINDIN;
    struct MELT_ROUTINE_STRUCT (5) drout_286__DISPLAY_DEBUG_MESSAGE;
    struct MELT_CLOSURE_STRUCT (0) dclo_287__DISPLAY_DEBUG_MESSAGE;
    struct MELT_OBJECT_STRUCT (3) dsym_288__DBG_OUT;
    struct MELT_STRING_STRUCT (7) dstr_289__DBG_OUT;
    struct MELT_OBJECT_STRUCT (3) dsym_290__DBG_OUTOBJECT;
    struct MELT_STRING_STRUCT (13) dstr_291__DBG_OUTOBJECT;
    struct MELT_OBJECT_STRUCT (3) dsym_292__DBGOUT_FIELDS;
    struct MELT_STRING_STRUCT (13) dstr_293__DBGOUT_FIELDS;
    struct MELT_OBJECT_STRUCT (3) dsym_294__DBGOUTAGAIN_FIELDS;
    struct MELT_STRING_STRUCT (18) dstr_295__DBGOUTAGAIN_FIEL;
    struct MELT_OBJECT_STRUCT (3) dsym_296__DEBUG_MSG_FUN;
    struct MELT_STRING_STRUCT (13) dstr_297__DEBUG_MSG_FUN;
    struct MELT_OBJECT_STRUCT (3) dsym_298__DISPLAY_DEBUG_MESSAGE;
    struct MELT_STRING_STRUCT (21) dstr_299__DISPLAY_DEBUG_ME;
    struct MELT_OBJECT_STRUCT (3) dsym_300__MELT_DEBUG_FUN;
    struct MELT_STRING_STRUCT (14) dstr_301__MELT_DEBUG_FUN;
    struct MELT_OBJECT_STRUCT (3) dsym_302__REGISTER_BASICBLOCK_DEBUG_FUN;
    struct MELT_STRING_STRUCT (29) dstr_303__REGISTER_BASICBL;
    struct MELT_OBJECT_STRUCT (3) dsym_304__REGISTER_EDGE_DEBUG_FUN;
    struct MELT_STRING_STRUCT (23) dstr_305__REGISTER_EDGE_DE;
    struct MELT_OBJECT_STRUCT (3) dsym_306__REGISTER_GIMPLE_DEBUG_FUN;
    struct MELT_STRING_STRUCT (25) dstr_307__REGISTER_GIMPLE_;
    struct MELT_OBJECT_STRUCT (3) dsym_308__REGISTER_GIMPLESEQ_DEBUG_FUN;
    struct MELT_STRING_STRUCT (28) dstr_309__REGISTER_GIMPLES;
    struct MELT_OBJECT_STRUCT (3) dsym_310__REGISTER_LOOP_DEBUG_FUN;
    struct MELT_STRING_STRUCT (23) dstr_311__REGISTER_LOOP_DE;
    struct MELT_OBJECT_STRUCT (3) dsym_312__REGISTER_TREE_DEBUG_FUN;
    struct MELT_STRING_STRUCT (23) dstr_313__REGISTER_TREE_DE;
    long spare_;
  } *cdat = NULL;
  dbgprintf ("start initialize_module_meltdata_warmelt_debug iniframp__=%p",
	     (void *) iniframp__);
  (void) meltpredefinited;	/* avoid warning if non-used. */
  melt_assertmsg ("check module initial frame",
		  iniframp__->mcfr_nbvar == /*minihash */ -2505);
  cdat = (struct cdata_st *) meltgc_allocate (sizeof (*cdat), 0);
  melt_prohibit_garbcoll = TRUE;
  /*initial routine predef */
  /*initial routine fill */


/*iniobj dsym_1__CLASS_DEBUG_INFORMATION*/
  /*uniqueobj*/ if (! /*_.VALDATA___V47*/ meltfptr[46])
/*_.VALDATA___V47*/ meltfptr[46] =
      (melt_ptr_t) & cdat->dsym_1__CLASS_DEBUG_INFORMATION;
  if (MELT_LIKELY (!melt_prohibit_garbcoll))
    melt_assertmsg
      ("iniobj check.discr isobj dsym_1__CLASS_DEBUG_INFORMATION",
       melt_magic_discr ((melt_ptr_t)
			 (((melt_ptr_t) (MELT_PREDEF (CLASS_SYMBOL))))) ==
       MELTOBMAG_OBJECT);
  if (MELT_LIKELY (!melt_prohibit_garbcoll))
    melt_assertmsg
      ("iniobj check.discr objmagic dsym_1__CLASS_DEBUG_INFORMATION",
       ((meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (CLASS_SYMBOL)))))->
       meltobj_magic == MELTOBMAG_OBJECT);
  cdat->dsym_1__CLASS_DEBUG_INFORMATION.meltobj_class =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (CLASS_SYMBOL))));
  cdat->dsym_1__CLASS_DEBUG_INFORMATION.obj_hash = 190162903;
  cdat->dsym_1__CLASS_DEBUG_INFORMATION.obj_len = 3;


/*inistring dstr_2__CLASS_DEBUG_INFO*/
 /*_.VALSTR___V48*/ meltfptr[47] =
    (melt_ptr_t) & cdat->dstr_2__CLASS_DEBUG_INFO;
  cdat->dstr_2__CLASS_DEBUG_INFO.discr =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (DISCR_STRING))));
  /*small inistring */ strncpy (cdat->dstr_2__CLASS_DEBUG_INFO.val,
				"CLASS_DEBUG_INFORMATION",
				sizeof (cdat->dstr_2__CLASS_DEBUG_INFO.val) -
				1);
  cdat->dstr_2__CLASS_DEBUG_INFO.val[23] = (char) 0;
  cdat->dstr_2__CLASS_DEBUG_INFO.slen = 23;


/*iniobj dsym_3__CLASS_ROOT*/
  /*uniqueobj*/ if (! /*_.VALDATA___V49*/ meltfptr[48])
/*_.VALDATA___V49*/ meltfptr[48] =
      (melt_ptr_t) & cdat->dsym_3__CLASS_ROOT;
  if (MELT_LIKELY (!melt_prohibit_garbcoll))
    melt_assertmsg ("iniobj check.discr isobj dsym_3__CLASS_ROOT",
		    melt_magic_discr ((melt_ptr_t)
				      (((melt_ptr_t)
					(MELT_PREDEF (CLASS_SYMBOL))))) ==
		    MELTOBMAG_OBJECT);
  if (MELT_LIKELY (!melt_prohibit_garbcoll))
    melt_assertmsg ("iniobj check.discr objmagic dsym_3__CLASS_ROOT",
		    ((meltobject_ptr_t)
		     (((melt_ptr_t) (MELT_PREDEF (CLASS_SYMBOL)))))->
		    meltobj_magic == MELTOBMAG_OBJECT);
  cdat->dsym_3__CLASS_ROOT.meltobj_class =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (CLASS_SYMBOL))));
  cdat->dsym_3__CLASS_ROOT.obj_hash = 60568825;
  cdat->dsym_3__CLASS_ROOT.obj_len = 3;


/*inistring dstr_4__CLASS_ROOT*/
 /*_.VALSTR___V50*/ meltfptr[49] = (melt_ptr_t) & cdat->dstr_4__CLASS_ROOT;
  cdat->dstr_4__CLASS_ROOT.discr =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (DISCR_STRING))));
  /*small inistring */ strncpy (cdat->dstr_4__CLASS_ROOT.val, "CLASS_ROOT",
				sizeof (cdat->dstr_4__CLASS_ROOT.val) - 1);
  cdat->dstr_4__CLASS_ROOT.val[10] = (char) 0;
  cdat->dstr_4__CLASS_ROOT.slen = 10;

  MELT_LOCATION ("warmelt-debug.melt:39:/ iniobj");

/*iniobj dobj_5__CLASS_DEBUG_INFORMATION*/
/*_.VALDATA___V51*/ meltfptr[50] =
    (melt_ptr_t) & cdat->dobj_5__CLASS_DEBUG_INFORMATION;
  if (MELT_LIKELY (!melt_prohibit_garbcoll))
    melt_assertmsg
      ("iniobj check.discr isobj dobj_5__CLASS_DEBUG_INFORMATION",
       melt_magic_discr ((melt_ptr_t)
			 (((melt_ptr_t) (MELT_PREDEF (CLASS_CLASS))))) ==
       MELTOBMAG_OBJECT);
  if (MELT_LIKELY (!melt_prohibit_garbcoll))
    melt_assertmsg
      ("iniobj check.discr objmagic dobj_5__CLASS_DEBUG_INFORMATION",
       ((meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (CLASS_CLASS)))))->
       meltobj_magic == MELTOBMAG_OBJECT);
  cdat->dobj_5__CLASS_DEBUG_INFORMATION.meltobj_class =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (CLASS_CLASS))));
  cdat->dobj_5__CLASS_DEBUG_INFORMATION.obj_num = MELTOBMAG_OBJECT;
  cdat->dobj_5__CLASS_DEBUG_INFORMATION.obj_hash = 590007967;
  cdat->dobj_5__CLASS_DEBUG_INFORMATION.obj_len = 8;


/*inistring dstr_6__CLASS_DEBUG_INFO*/
 /*_.VALSTR___V52*/ meltfptr[51] =
    (melt_ptr_t) & cdat->dstr_6__CLASS_DEBUG_INFO;
  cdat->dstr_6__CLASS_DEBUG_INFO.discr =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (DISCR_STRING))));
  /*small inistring */ strncpy (cdat->dstr_6__CLASS_DEBUG_INFO.val,
				"CLASS_DEBUG_INFORMATION",
				sizeof (cdat->dstr_6__CLASS_DEBUG_INFO.val) -
				1);
  cdat->dstr_6__CLASS_DEBUG_INFO.val[23] = (char) 0;
  cdat->dstr_6__CLASS_DEBUG_INFO.slen = 23;

  /*inimult dtup_7__CLASS_DEBUG_INFORMATION */
 /*_.VALTUP___V53*/ meltfptr[52] =
    (melt_ptr_t) & cdat->dtup_7__CLASS_DEBUG_INFORMATION;
  cdat->dtup_7__CLASS_DEBUG_INFORMATION.discr =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (DISCR_CLASS_SEQUENCE))));
  cdat->dtup_7__CLASS_DEBUG_INFORMATION.nbval = 1;
  /*inimult dtup_8__CLASS_DEBUG_INFORMATION */
 /*_.VALTUP___V60*/ meltfptr[59] =
    (melt_ptr_t) & cdat->dtup_8__CLASS_DEBUG_INFORMATION;
  cdat->dtup_8__CLASS_DEBUG_INFORMATION.discr =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (DISCR_FIELD_SEQUENCE))));
  cdat->dtup_8__CLASS_DEBUG_INFORMATION.nbval = 3;

/*iniobj dsym_9__DBGI_OUT*/
  /*uniqueobj*/ if (! /*_.VALDATA___V61*/ meltfptr[60])
/*_.VALDATA___V61*/ meltfptr[60] = (melt_ptr_t) & cdat->dsym_9__DBGI_OUT;
  if (MELT_LIKELY (!melt_prohibit_garbcoll))
    melt_assertmsg ("iniobj check.discr isobj dsym_9__DBGI_OUT",
		    melt_magic_discr ((melt_ptr_t)
				      (((melt_ptr_t)
					(MELT_PREDEF (CLASS_SYMBOL))))) ==
		    MELTOBMAG_OBJECT);
  if (MELT_LIKELY (!melt_prohibit_garbcoll))
    melt_assertmsg ("iniobj check.discr objmagic dsym_9__DBGI_OUT",
		    ((meltobject_ptr_t)
		     (((melt_ptr_t) (MELT_PREDEF (CLASS_SYMBOL)))))->
		    meltobj_magic == MELTOBMAG_OBJECT);
  cdat->dsym_9__DBGI_OUT.meltobj_class =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (CLASS_SYMBOL))));
  cdat->dsym_9__DBGI_OUT.obj_hash = 639486128;
  cdat->dsym_9__DBGI_OUT.obj_len = 3;


/*inistring dstr_10__DBGI_OUT*/
 /*_.VALSTR___V62*/ meltfptr[61] = (melt_ptr_t) & cdat->dstr_10__DBGI_OUT;
  cdat->dstr_10__DBGI_OUT.discr =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (DISCR_STRING))));
  /*small inistring */ strncpy (cdat->dstr_10__DBGI_OUT.val, "DBGI_OUT",
				sizeof (cdat->dstr_10__DBGI_OUT.val) - 1);
  cdat->dstr_10__DBGI_OUT.val[8] = (char) 0;
  cdat->dstr_10__DBGI_OUT.slen = 8;

  /*^iniobj */

/*iniobj dobj_11__DBGI_OUT*/
/*_.VALDATA___V54*/ meltfptr[53] = (melt_ptr_t) & cdat->dobj_11__DBGI_OUT;
  if (MELT_LIKELY (!melt_prohibit_garbcoll))
    melt_assertmsg ("iniobj check.discr isobj dobj_11__DBGI_OUT",
		    melt_magic_discr ((melt_ptr_t)
				      (((melt_ptr_t)
					(MELT_PREDEF (CLASS_FIELD))))) ==
		    MELTOBMAG_OBJECT);
  if (MELT_LIKELY (!melt_prohibit_garbcoll))
    melt_assertmsg ("iniobj check.discr objmagic dobj_11__DBGI_OUT",
		    ((meltobject_ptr_t)
		     (((melt_ptr_t) (MELT_PREDEF (CLASS_FIELD)))))->
		    meltobj_magic == MELTOBMAG_OBJECT);
  cdat->dobj_11__DBGI_OUT.meltobj_class =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (CLASS_FIELD))));
  cdat->dobj_11__DBGI_OUT.obj_num = 0;
  cdat->dobj_11__DBGI_OUT.obj_hash = 830469026;
  cdat->dobj_11__DBGI_OUT.obj_len = 4;


/*inistring dstr_12__DBGI_OUT*/
 /*_.VALSTR___V55*/ meltfptr[54] = (melt_ptr_t) & cdat->dstr_12__DBGI_OUT;
  cdat->dstr_12__DBGI_OUT.discr =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (DISCR_STRING))));
  /*small inistring */ strncpy (cdat->dstr_12__DBGI_OUT.val, "DBGI_OUT",
				sizeof (cdat->dstr_12__DBGI_OUT.val) - 1);
  cdat->dstr_12__DBGI_OUT.val[8] = (char) 0;
  cdat->dstr_12__DBGI_OUT.slen = 8;


/*iniobj dsym_13__DBGI_OCCMAP*/
  /*uniqueobj*/ if (! /*_.VALDATA___V63*/ meltfptr[62])
/*_.VALDATA___V63*/ meltfptr[62] =
      (melt_ptr_t) & cdat->dsym_13__DBGI_OCCMAP;
  if (MELT_LIKELY (!melt_prohibit_garbcoll))
    melt_assertmsg ("iniobj check.discr isobj dsym_13__DBGI_OCCMAP",
		    melt_magic_discr ((melt_ptr_t)
				      (((melt_ptr_t)
					(MELT_PREDEF (CLASS_SYMBOL))))) ==
		    MELTOBMAG_OBJECT);
  if (MELT_LIKELY (!melt_prohibit_garbcoll))
    melt_assertmsg ("iniobj check.discr objmagic dsym_13__DBGI_OCCMAP",
		    ((meltobject_ptr_t)
		     (((melt_ptr_t) (MELT_PREDEF (CLASS_SYMBOL)))))->
		    meltobj_magic == MELTOBMAG_OBJECT);
  cdat->dsym_13__DBGI_OCCMAP.meltobj_class =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (CLASS_SYMBOL))));
  cdat->dsym_13__DBGI_OCCMAP.obj_hash = 701804047;
  cdat->dsym_13__DBGI_OCCMAP.obj_len = 3;


/*inistring dstr_14__DBGI_OCCMAP*/
 /*_.VALSTR___V64*/ meltfptr[63] = (melt_ptr_t) & cdat->dstr_14__DBGI_OCCMAP;
  cdat->dstr_14__DBGI_OCCMAP.discr =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (DISCR_STRING))));
  /*small inistring */ strncpy (cdat->dstr_14__DBGI_OCCMAP.val, "DBGI_OCCMAP",
				sizeof (cdat->dstr_14__DBGI_OCCMAP.val) - 1);
  cdat->dstr_14__DBGI_OCCMAP.val[11] = (char) 0;
  cdat->dstr_14__DBGI_OCCMAP.slen = 11;

  /*^iniobj */

/*iniobj dobj_15__DBGI_OCCMAP*/
/*_.VALDATA___V56*/ meltfptr[55] =
    (melt_ptr_t) & cdat->dobj_15__DBGI_OCCMAP;
  if (MELT_LIKELY (!melt_prohibit_garbcoll))
    melt_assertmsg ("iniobj check.discr isobj dobj_15__DBGI_OCCMAP",
		    melt_magic_discr ((melt_ptr_t)
				      (((melt_ptr_t)
					(MELT_PREDEF (CLASS_FIELD))))) ==
		    MELTOBMAG_OBJECT);
  if (MELT_LIKELY (!melt_prohibit_garbcoll))
    melt_assertmsg ("iniobj check.discr objmagic dobj_15__DBGI_OCCMAP",
		    ((meltobject_ptr_t)
		     (((melt_ptr_t) (MELT_PREDEF (CLASS_FIELD)))))->
		    meltobj_magic == MELTOBMAG_OBJECT);
  cdat->dobj_15__DBGI_OCCMAP.meltobj_class =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (CLASS_FIELD))));
  cdat->dobj_15__DBGI_OCCMAP.obj_num = 1;
  cdat->dobj_15__DBGI_OCCMAP.obj_hash = 8564027;
  cdat->dobj_15__DBGI_OCCMAP.obj_len = 4;


/*inistring dstr_16__DBGI_OCCMAP*/
 /*_.VALSTR___V57*/ meltfptr[56] = (melt_ptr_t) & cdat->dstr_16__DBGI_OCCMAP;
  cdat->dstr_16__DBGI_OCCMAP.discr =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (DISCR_STRING))));
  /*small inistring */ strncpy (cdat->dstr_16__DBGI_OCCMAP.val, "DBGI_OCCMAP",
				sizeof (cdat->dstr_16__DBGI_OCCMAP.val) - 1);
  cdat->dstr_16__DBGI_OCCMAP.val[11] = (char) 0;
  cdat->dstr_16__DBGI_OCCMAP.slen = 11;


/*iniobj dsym_17__DBGI_MAXDEPTH*/
  /*uniqueobj*/ if (! /*_.VALDATA___V65*/ meltfptr[64])
/*_.VALDATA___V65*/ meltfptr[64] =
      (melt_ptr_t) & cdat->dsym_17__DBGI_MAXDEPTH;
  if (MELT_LIKELY (!melt_prohibit_garbcoll))
    melt_assertmsg ("iniobj check.discr isobj dsym_17__DBGI_MAXDEPTH",
		    melt_magic_discr ((melt_ptr_t)
				      (((melt_ptr_t)
					(MELT_PREDEF (CLASS_SYMBOL))))) ==
		    MELTOBMAG_OBJECT);
  if (MELT_LIKELY (!melt_prohibit_garbcoll))
    melt_assertmsg ("iniobj check.discr objmagic dsym_17__DBGI_MAXDEPTH",
		    ((meltobject_ptr_t)
		     (((melt_ptr_t) (MELT_PREDEF (CLASS_SYMBOL)))))->
		    meltobj_magic == MELTOBMAG_OBJECT);
  cdat->dsym_17__DBGI_MAXDEPTH.meltobj_class =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (CLASS_SYMBOL))));
  cdat->dsym_17__DBGI_MAXDEPTH.obj_hash = 732580240;
  cdat->dsym_17__DBGI_MAXDEPTH.obj_len = 3;


/*inistring dstr_18__DBGI_MAXDEPTH*/
 /*_.VALSTR___V66*/ meltfptr[65] =
    (melt_ptr_t) & cdat->dstr_18__DBGI_MAXDEPTH;
  cdat->dstr_18__DBGI_MAXDEPTH.discr =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (DISCR_STRING))));
  /*small inistring */ strncpy (cdat->dstr_18__DBGI_MAXDEPTH.val,
				"DBGI_MAXDEPTH",
				sizeof (cdat->dstr_18__DBGI_MAXDEPTH.val) -
				1);
  cdat->dstr_18__DBGI_MAXDEPTH.val[13] = (char) 0;
  cdat->dstr_18__DBGI_MAXDEPTH.slen = 13;

  /*^iniobj */

/*iniobj dobj_19__DBGI_MAXDEPTH*/
/*_.VALDATA___V58*/ meltfptr[57] =
    (melt_ptr_t) & cdat->dobj_19__DBGI_MAXDEPTH;
  if (MELT_LIKELY (!melt_prohibit_garbcoll))
    melt_assertmsg ("iniobj check.discr isobj dobj_19__DBGI_MAXDEPTH",
		    melt_magic_discr ((melt_ptr_t)
				      (((melt_ptr_t)
					(MELT_PREDEF (CLASS_FIELD))))) ==
		    MELTOBMAG_OBJECT);
  if (MELT_LIKELY (!melt_prohibit_garbcoll))
    melt_assertmsg ("iniobj check.discr objmagic dobj_19__DBGI_MAXDEPTH",
		    ((meltobject_ptr_t)
		     (((melt_ptr_t) (MELT_PREDEF (CLASS_FIELD)))))->
		    meltobj_magic == MELTOBMAG_OBJECT);
  cdat->dobj_19__DBGI_MAXDEPTH.meltobj_class =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (CLASS_FIELD))));
  cdat->dobj_19__DBGI_MAXDEPTH.obj_num = 2;
  cdat->dobj_19__DBGI_MAXDEPTH.obj_hash = 298204063;
  cdat->dobj_19__DBGI_MAXDEPTH.obj_len = 4;


/*inistring dstr_20__DBGI_MAXDEPTH*/
 /*_.VALSTR___V59*/ meltfptr[58] =
    (melt_ptr_t) & cdat->dstr_20__DBGI_MAXDEPTH;
  cdat->dstr_20__DBGI_MAXDEPTH.discr =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (DISCR_STRING))));
  /*small inistring */ strncpy (cdat->dstr_20__DBGI_MAXDEPTH.val,
				"DBGI_MAXDEPTH",
				sizeof (cdat->dstr_20__DBGI_MAXDEPTH.val) -
				1);
  cdat->dstr_20__DBGI_MAXDEPTH.val[13] = (char) 0;
  cdat->dstr_20__DBGI_MAXDEPTH.slen = 13;


/*iniobj dsym_21__CLASS_DEBUG_OUTPUT_INFORMATION*/
  /*uniqueobj*/ if (! /*_.VALDATA___V67*/ meltfptr[66])
/*_.VALDATA___V67*/ meltfptr[66] =
      (melt_ptr_t) & cdat->dsym_21__CLASS_DEBUG_OUTPUT_INFORMATION;
  if (MELT_LIKELY (!melt_prohibit_garbcoll))
    melt_assertmsg
      ("iniobj check.discr isobj dsym_21__CLASS_DEBUG_OUTPUT_INFORMATION",
       melt_magic_discr ((melt_ptr_t)
			 (((melt_ptr_t) (MELT_PREDEF (CLASS_SYMBOL))))) ==
       MELTOBMAG_OBJECT);
  if (MELT_LIKELY (!melt_prohibit_garbcoll))
    melt_assertmsg
      ("iniobj check.discr objmagic dsym_21__CLASS_DEBUG_OUTPUT_INFORMATION",
       ((meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (CLASS_SYMBOL)))))->
       meltobj_magic == MELTOBMAG_OBJECT);
  cdat->dsym_21__CLASS_DEBUG_OUTPUT_INFORMATION.meltobj_class =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (CLASS_SYMBOL))));
  cdat->dsym_21__CLASS_DEBUG_OUTPUT_INFORMATION.obj_hash = 436769164;
  cdat->dsym_21__CLASS_DEBUG_OUTPUT_INFORMATION.obj_len = 3;


/*inistring dstr_22__CLASS_DEBUG_OUTP*/
 /*_.VALSTR___V68*/ meltfptr[67] =
    (melt_ptr_t) & cdat->dstr_22__CLASS_DEBUG_OUTP;
  cdat->dstr_22__CLASS_DEBUG_OUTP.discr =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (DISCR_STRING))));
  /*small inistring */ strncpy (cdat->dstr_22__CLASS_DEBUG_OUTP.val,
				"CLASS_DEBUG_OUTPUT_INFORMATION",
				sizeof (cdat->dstr_22__CLASS_DEBUG_OUTP.val) -
				1);
  cdat->dstr_22__CLASS_DEBUG_OUTP.val[30] = (char) 0;
  cdat->dstr_22__CLASS_DEBUG_OUTP.slen = 30;

  MELT_LOCATION ("warmelt-debug.melt:50:/ iniobj");

/*iniobj dobj_23__CLASS_DEBUG_OUTPUT_INFORMATION*/
/*_.VALDATA___V69*/ meltfptr[68] =
    (melt_ptr_t) & cdat->dobj_23__CLASS_DEBUG_OUTPUT_INFORMATION;
  if (MELT_LIKELY (!melt_prohibit_garbcoll))
    melt_assertmsg
      ("iniobj check.discr isobj dobj_23__CLASS_DEBUG_OUTPUT_INFORMATION",
       melt_magic_discr ((melt_ptr_t)
			 (((melt_ptr_t) (MELT_PREDEF (CLASS_CLASS))))) ==
       MELTOBMAG_OBJECT);
  if (MELT_LIKELY (!melt_prohibit_garbcoll))
    melt_assertmsg
      ("iniobj check.discr objmagic dobj_23__CLASS_DEBUG_OUTPUT_INFORMATION",
       ((meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (CLASS_CLASS)))))->
       meltobj_magic == MELTOBMAG_OBJECT);
  cdat->dobj_23__CLASS_DEBUG_OUTPUT_INFORMATION.meltobj_class =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (CLASS_CLASS))));
  cdat->dobj_23__CLASS_DEBUG_OUTPUT_INFORMATION.obj_num = MELTOBMAG_OBJECT;
  cdat->dobj_23__CLASS_DEBUG_OUTPUT_INFORMATION.obj_hash = 626529907;
  cdat->dobj_23__CLASS_DEBUG_OUTPUT_INFORMATION.obj_len = 8;


/*inistring dstr_24__CLASS_DEBUG_OUTP*/
 /*_.VALSTR___V70*/ meltfptr[69] =
    (melt_ptr_t) & cdat->dstr_24__CLASS_DEBUG_OUTP;
  cdat->dstr_24__CLASS_DEBUG_OUTP.discr =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (DISCR_STRING))));
  /*small inistring */ strncpy (cdat->dstr_24__CLASS_DEBUG_OUTP.val,
				"CLASS_DEBUG_OUTPUT_INFORMATION",
				sizeof (cdat->dstr_24__CLASS_DEBUG_OUTP.val) -
				1);
  cdat->dstr_24__CLASS_DEBUG_OUTP.val[30] = (char) 0;
  cdat->dstr_24__CLASS_DEBUG_OUTP.slen = 30;

  /*inimult dtup_25__CLASS_DEBUG_OUTPUT_INFORMATION */
 /*_.VALTUP___V71*/ meltfptr[70] =
    (melt_ptr_t) & cdat->dtup_25__CLASS_DEBUG_OUTPUT_INFORMATION;
  cdat->dtup_25__CLASS_DEBUG_OUTPUT_INFORMATION.discr =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (DISCR_CLASS_SEQUENCE))));
  cdat->dtup_25__CLASS_DEBUG_OUTPUT_INFORMATION.nbval = 2;
  /*inimult dtup_26__CLASS_DEBUG_OUTPUT_INFORMATION */
 /*_.VALTUP___V72*/ meltfptr[71] =
    (melt_ptr_t) & cdat->dtup_26__CLASS_DEBUG_OUTPUT_INFORMATION;
  cdat->dtup_26__CLASS_DEBUG_OUTPUT_INFORMATION.discr =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (DISCR_FIELD_SEQUENCE))));
  cdat->dtup_26__CLASS_DEBUG_OUTPUT_INFORMATION.nbval = 3;

/*iniobj dsym_27__DEBUG_DEPTH*/
  /*uniqueobj*/ if (! /*_.VALDATA___V73*/ meltfptr[72])
/*_.VALDATA___V73*/ meltfptr[72] =
      (melt_ptr_t) & cdat->dsym_27__DEBUG_DEPTH;
  if (MELT_LIKELY (!melt_prohibit_garbcoll))
    melt_assertmsg ("iniobj check.discr isobj dsym_27__DEBUG_DEPTH",
		    melt_magic_discr ((melt_ptr_t)
				      (((melt_ptr_t)
					(MELT_PREDEF (CLASS_SYMBOL))))) ==
		    MELTOBMAG_OBJECT);
  if (MELT_LIKELY (!melt_prohibit_garbcoll))
    melt_assertmsg ("iniobj check.discr objmagic dsym_27__DEBUG_DEPTH",
		    ((meltobject_ptr_t)
		     (((melt_ptr_t) (MELT_PREDEF (CLASS_SYMBOL)))))->
		    meltobj_magic == MELTOBMAG_OBJECT);
  cdat->dsym_27__DEBUG_DEPTH.meltobj_class =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (CLASS_SYMBOL))));
  cdat->dsym_27__DEBUG_DEPTH.obj_hash = 532594980;
  cdat->dsym_27__DEBUG_DEPTH.obj_len = 3;


/*inistring dstr_28__DEBUG_DEPTH*/
 /*_.VALSTR___V74*/ meltfptr[73] = (melt_ptr_t) & cdat->dstr_28__DEBUG_DEPTH;
  cdat->dstr_28__DEBUG_DEPTH.discr =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (DISCR_STRING))));
  /*small inistring */ strncpy (cdat->dstr_28__DEBUG_DEPTH.val, "DEBUG_DEPTH",
				sizeof (cdat->dstr_28__DEBUG_DEPTH.val) - 1);
  cdat->dstr_28__DEBUG_DEPTH.val[11] = (char) 0;
  cdat->dstr_28__DEBUG_DEPTH.slen = 11;

  MELT_LOCATION ("warmelt-debug.melt:61:/ iniobj");

/*iniobj dobj_29__DEBUG_DEPTH*/
/*_.VALDATA___V75*/ meltfptr[74] =
    (melt_ptr_t) & cdat->dobj_29__DEBUG_DEPTH;
  if (MELT_LIKELY (!melt_prohibit_garbcoll))
    melt_assertmsg ("iniobj check.discr isobj dobj_29__DEBUG_DEPTH",
		    melt_magic_discr ((melt_ptr_t)
				      (((melt_ptr_t)
					(MELT_PREDEF (CLASS_PRIMITIVE))))) ==
		    MELTOBMAG_OBJECT);
  if (MELT_LIKELY (!melt_prohibit_garbcoll))
    melt_assertmsg ("iniobj check.discr objmagic dobj_29__DEBUG_DEPTH",
		    ((meltobject_ptr_t)
		     (((melt_ptr_t) (MELT_PREDEF (CLASS_PRIMITIVE)))))->
		    meltobj_magic == MELTOBMAG_OBJECT);
  cdat->dobj_29__DEBUG_DEPTH.meltobj_class =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (CLASS_PRIMITIVE))));
  cdat->dobj_29__DEBUG_DEPTH.obj_hash = 464541799;
  cdat->dobj_29__DEBUG_DEPTH.obj_len = 5;

  /*inimult dtup_30__DEBUG_DEPTH */
 /*_.VALTUP___V79*/ meltfptr[78] = (melt_ptr_t) & cdat->dtup_30__DEBUG_DEPTH;
  cdat->dtup_30__DEBUG_DEPTH.discr =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (DISCR_MULTIPLE))));
  cdat->dtup_30__DEBUG_DEPTH.nbval = 1;
  /*inimult dtup_31__DEBUG_DEPTH */
 /*_.VALTUP___V77*/ meltfptr[76] = (melt_ptr_t) & cdat->dtup_31__DEBUG_DEPTH;
  cdat->dtup_31__DEBUG_DEPTH.discr =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (DISCR_MULTIPLE))));
  cdat->dtup_31__DEBUG_DEPTH.nbval = 0;

/*inistring dstr_32__long_melt_deb*/
 /*_.VALSTR___V78*/ meltfptr[77] =
    (melt_ptr_t) & cdat->dstr_32__long_melt_deb;
  cdat->dstr_32__long_melt_deb.discr =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (DISCR_VERBATIM_STRING))));
  /*small inistring */ strncpy (cdat->dstr_32__long_melt_deb.val,
				"(long) (melt_debug_depth ())",
				sizeof (cdat->dstr_32__long_melt_deb.val) -
				1);
  cdat->dstr_32__long_melt_deb.val[28] = (char) 0;
  cdat->dstr_32__long_melt_deb.slen = 28;


/*inistring dstr_33__DEBUG_DEPTH*/
 /*_.VALSTR___V76*/ meltfptr[75] = (melt_ptr_t) & cdat->dstr_33__DEBUG_DEPTH;
  cdat->dstr_33__DEBUG_DEPTH.discr =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (DISCR_STRING))));
  /*small inistring */ strncpy (cdat->dstr_33__DEBUG_DEPTH.val, "DEBUG_DEPTH",
				sizeof (cdat->dstr_33__DEBUG_DEPTH.val) - 1);
  cdat->dstr_33__DEBUG_DEPTH.val[11] = (char) 0;
  cdat->dstr_33__DEBUG_DEPTH.slen = 11;


/*iniobj dsym_34__MELT_REALLY_NEED_DEBUG*/
  /*uniqueobj*/ if (! /*_.VALDATA___V80*/ meltfptr[79])
/*_.VALDATA___V80*/ meltfptr[79] =
      (melt_ptr_t) & cdat->dsym_34__MELT_REALLY_NEED_DEBUG;
  if (MELT_LIKELY (!melt_prohibit_garbcoll))
    melt_assertmsg
      ("iniobj check.discr isobj dsym_34__MELT_REALLY_NEED_DEBUG",
       melt_magic_discr ((melt_ptr_t)
			 (((melt_ptr_t) (MELT_PREDEF (CLASS_SYMBOL))))) ==
       MELTOBMAG_OBJECT);
  if (MELT_LIKELY (!melt_prohibit_garbcoll))
    melt_assertmsg
      ("iniobj check.discr objmagic dsym_34__MELT_REALLY_NEED_DEBUG",
       ((meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (CLASS_SYMBOL)))))->
       meltobj_magic == MELTOBMAG_OBJECT);
  cdat->dsym_34__MELT_REALLY_NEED_DEBUG.meltobj_class =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (CLASS_SYMBOL))));
  cdat->dsym_34__MELT_REALLY_NEED_DEBUG.obj_hash = 542840019;
  cdat->dsym_34__MELT_REALLY_NEED_DEBUG.obj_len = 3;


/*inistring dstr_35__MELT_REALLY_NEED*/
 /*_.VALSTR___V81*/ meltfptr[80] =
    (melt_ptr_t) & cdat->dstr_35__MELT_REALLY_NEED;
  cdat->dstr_35__MELT_REALLY_NEED.discr =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (DISCR_STRING))));
  /*small inistring */ strncpy (cdat->dstr_35__MELT_REALLY_NEED.val,
				"MELT_REALLY_NEED_DEBUG",
				sizeof (cdat->dstr_35__MELT_REALLY_NEED.val) -
				1);
  cdat->dstr_35__MELT_REALLY_NEED.val[22] = (char) 0;
  cdat->dstr_35__MELT_REALLY_NEED.slen = 22;

  MELT_LOCATION ("warmelt-debug.melt:69:/ iniobj");

/*iniobj dobj_36__MELT_REALLY_NEED_DEBUG*/
/*_.VALDATA___V82*/ meltfptr[81] =
    (melt_ptr_t) & cdat->dobj_36__MELT_REALLY_NEED_DEBUG;
  if (MELT_LIKELY (!melt_prohibit_garbcoll))
    melt_assertmsg
      ("iniobj check.discr isobj dobj_36__MELT_REALLY_NEED_DEBUG",
       melt_magic_discr ((melt_ptr_t)
			 (((melt_ptr_t) (MELT_PREDEF (CLASS_PRIMITIVE))))) ==
       MELTOBMAG_OBJECT);
  if (MELT_LIKELY (!melt_prohibit_garbcoll))
    melt_assertmsg
      ("iniobj check.discr objmagic dobj_36__MELT_REALLY_NEED_DEBUG",
       ((meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (CLASS_PRIMITIVE)))))->
       meltobj_magic == MELTOBMAG_OBJECT);
  cdat->dobj_36__MELT_REALLY_NEED_DEBUG.meltobj_class =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (CLASS_PRIMITIVE))));
  cdat->dobj_36__MELT_REALLY_NEED_DEBUG.obj_hash = 239494879;
  cdat->dobj_36__MELT_REALLY_NEED_DEBUG.obj_len = 5;

  /*inimult dtup_37__MELT_REALLY_NEED_DEBUG */
 /*_.VALTUP___V90*/ meltfptr[89] =
    (melt_ptr_t) & cdat->dtup_37__MELT_REALLY_NEED_DEBUG;
  cdat->dtup_37__MELT_REALLY_NEED_DEBUG.discr =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (DISCR_MULTIPLE))));
  cdat->dtup_37__MELT_REALLY_NEED_DEBUG.nbval = 3;
  /*inimult dtup_38__MELT_REALLY_NEED_DEBUG */
 /*_.VALTUP___V87*/ meltfptr[86] =
    (melt_ptr_t) & cdat->dtup_38__MELT_REALLY_NEED_DEBUG;
  cdat->dtup_38__MELT_REALLY_NEED_DEBUG.discr =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (DISCR_MULTIPLE))));
  cdat->dtup_38__MELT_REALLY_NEED_DEBUG.nbval = 1;

/*iniobj dsym_39__DEPTH*/
  /*uniqueobj*/ if (! /*_.VALDATA___V85*/ meltfptr[84])
/*_.VALDATA___V85*/ meltfptr[84] = (melt_ptr_t) & cdat->dsym_39__DEPTH;
  if (MELT_LIKELY (!melt_prohibit_garbcoll))
    melt_assertmsg ("iniobj check.discr isobj dsym_39__DEPTH",
		    melt_magic_discr ((melt_ptr_t)
				      (((melt_ptr_t)
					(MELT_PREDEF (CLASS_SYMBOL))))) ==
		    MELTOBMAG_OBJECT);
  if (MELT_LIKELY (!melt_prohibit_garbcoll))
    melt_assertmsg ("iniobj check.discr objmagic dsym_39__DEPTH",
		    ((meltobject_ptr_t)
		     (((melt_ptr_t) (MELT_PREDEF (CLASS_SYMBOL)))))->
		    meltobj_magic == MELTOBMAG_OBJECT);
  cdat->dsym_39__DEPTH.meltobj_class =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (CLASS_SYMBOL))));
  cdat->dsym_39__DEPTH.obj_hash = 52029444;
  cdat->dsym_39__DEPTH.obj_len = 3;


/*inistring dstr_40__DEPTH*/
 /*_.VALSTR___V86*/ meltfptr[85] = (melt_ptr_t) & cdat->dstr_40__DEPTH;
  cdat->dstr_40__DEPTH.discr =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (DISCR_STRING))));
  /*small inistring */ strncpy (cdat->dstr_40__DEPTH.val, "DEPTH",
				sizeof (cdat->dstr_40__DEPTH.val) - 1);
  cdat->dstr_40__DEPTH.val[5] = (char) 0;
  cdat->dstr_40__DEPTH.slen = 5;

  /*^iniobj */

/*iniobj dobj_41*/
/*_.VALDATA___V84*/ meltfptr[83] = (melt_ptr_t) & cdat->dobj_41;
  if (MELT_LIKELY (!melt_prohibit_garbcoll))
    melt_assertmsg ("iniobj check.discr isobj dobj_41",
		    melt_magic_discr ((melt_ptr_t)
				      (((melt_ptr_t)
					(MELT_PREDEF
					 (CLASS_FORMAL_BINDING))))) ==
		    MELTOBMAG_OBJECT);
  if (MELT_LIKELY (!melt_prohibit_garbcoll))
    melt_assertmsg ("iniobj check.discr objmagic dobj_41",
		    ((meltobject_ptr_t)
		     (((melt_ptr_t) (MELT_PREDEF (CLASS_FORMAL_BINDING)))))->
		    meltobj_magic == MELTOBMAG_OBJECT);
  cdat->dobj_41.meltobj_class =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (CLASS_FORMAL_BINDING))));
  cdat->dobj_41.obj_num = 0;
  cdat->dobj_41.obj_hash = 832410161;
  cdat->dobj_41.obj_len = 2;


/*inistring dstr_42__MELT_REALLY_NE*/
 /*_.VALSTR___V88*/ meltfptr[87] =
    (melt_ptr_t) & cdat->dstr_42__MELT_REALLY_NE;
  cdat->dstr_42__MELT_REALLY_NE.discr =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (DISCR_VERBATIM_STRING))));
  /*small inistring */ strncpy (cdat->dstr_42__MELT_REALLY_NE.val,
				"/*MELT_REALLY_NEED_DEBUG*/ (melt_need_debug ((int) ",
				sizeof (cdat->dstr_42__MELT_REALLY_NE.val) -
				1);
  cdat->dstr_42__MELT_REALLY_NE.val[51] = (char) 0;
  cdat->dstr_42__MELT_REALLY_NE.slen = 51;


/*inistring dstr_43__*/
 /*_.VALSTR___V89*/ meltfptr[88] = (melt_ptr_t) & cdat->dstr_43__;
  cdat->dstr_43__.discr =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (DISCR_VERBATIM_STRING))));
  /*small inistring */ strncpy (cdat->dstr_43__.val, "))",
				sizeof (cdat->dstr_43__.val) - 1);
  cdat->dstr_43__.val[2] = (char) 0;
  cdat->dstr_43__.slen = 2;


/*inistring dstr_44__MELT_REALLY_NEED*/
 /*_.VALSTR___V83*/ meltfptr[82] =
    (melt_ptr_t) & cdat->dstr_44__MELT_REALLY_NEED;
  cdat->dstr_44__MELT_REALLY_NEED.discr =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (DISCR_STRING))));
  /*small inistring */ strncpy (cdat->dstr_44__MELT_REALLY_NEED.val,
				"MELT_REALLY_NEED_DEBUG",
				sizeof (cdat->dstr_44__MELT_REALLY_NEED.val) -
				1);
  cdat->dstr_44__MELT_REALLY_NEED.val[22] = (char) 0;
  cdat->dstr_44__MELT_REALLY_NEED.slen = 22;


/*iniobj dsym_45__MELT_HAS_FLAG_DEBUG_SET*/
  /*uniqueobj*/ if (! /*_.VALDATA___V91*/ meltfptr[90])
/*_.VALDATA___V91*/ meltfptr[90] =
      (melt_ptr_t) & cdat->dsym_45__MELT_HAS_FLAG_DEBUG_SET;
  if (MELT_LIKELY (!melt_prohibit_garbcoll))
    melt_assertmsg
      ("iniobj check.discr isobj dsym_45__MELT_HAS_FLAG_DEBUG_SET",
       melt_magic_discr ((melt_ptr_t)
			 (((melt_ptr_t) (MELT_PREDEF (CLASS_SYMBOL))))) ==
       MELTOBMAG_OBJECT);
  if (MELT_LIKELY (!melt_prohibit_garbcoll))
    melt_assertmsg
      ("iniobj check.discr objmagic dsym_45__MELT_HAS_FLAG_DEBUG_SET",
       ((meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (CLASS_SYMBOL)))))->
       meltobj_magic == MELTOBMAG_OBJECT);
  cdat->dsym_45__MELT_HAS_FLAG_DEBUG_SET.meltobj_class =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (CLASS_SYMBOL))));
  cdat->dsym_45__MELT_HAS_FLAG_DEBUG_SET.obj_hash = 125931725;
  cdat->dsym_45__MELT_HAS_FLAG_DEBUG_SET.obj_len = 3;


/*inistring dstr_46__MELT_HAS_FLAG_DE*/
 /*_.VALSTR___V92*/ meltfptr[91] =
    (melt_ptr_t) & cdat->dstr_46__MELT_HAS_FLAG_DE;
  cdat->dstr_46__MELT_HAS_FLAG_DE.discr =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (DISCR_STRING))));
  /*small inistring */ strncpy (cdat->dstr_46__MELT_HAS_FLAG_DE.val,
				"MELT_HAS_FLAG_DEBUG_SET",
				sizeof (cdat->dstr_46__MELT_HAS_FLAG_DE.val) -
				1);
  cdat->dstr_46__MELT_HAS_FLAG_DE.val[23] = (char) 0;
  cdat->dstr_46__MELT_HAS_FLAG_DE.slen = 23;

  MELT_LOCATION ("warmelt-debug.melt:74:/ iniobj");

/*iniobj dobj_47__MELT_HAS_FLAG_DEBUG_SET*/
/*_.VALDATA___V93*/ meltfptr[92] =
    (melt_ptr_t) & cdat->dobj_47__MELT_HAS_FLAG_DEBUG_SET;
  if (MELT_LIKELY (!melt_prohibit_garbcoll))
    melt_assertmsg
      ("iniobj check.discr isobj dobj_47__MELT_HAS_FLAG_DEBUG_SET",
       melt_magic_discr ((melt_ptr_t)
			 (((melt_ptr_t) (MELT_PREDEF (CLASS_PRIMITIVE))))) ==
       MELTOBMAG_OBJECT);
  if (MELT_LIKELY (!melt_prohibit_garbcoll))
    melt_assertmsg
      ("iniobj check.discr objmagic dobj_47__MELT_HAS_FLAG_DEBUG_SET",
       ((meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (CLASS_PRIMITIVE)))))->
       meltobj_magic == MELTOBMAG_OBJECT);
  cdat->dobj_47__MELT_HAS_FLAG_DEBUG_SET.meltobj_class =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (CLASS_PRIMITIVE))));
  cdat->dobj_47__MELT_HAS_FLAG_DEBUG_SET.obj_hash = 570600209;
  cdat->dobj_47__MELT_HAS_FLAG_DEBUG_SET.obj_len = 5;

  /*inimult dtup_48__MELT_HAS_FLAG_DEBUG_SET */
 /*_.VALTUP___V97*/ meltfptr[96] =
    (melt_ptr_t) & cdat->dtup_48__MELT_HAS_FLAG_DEBUG_SET;
  cdat->dtup_48__MELT_HAS_FLAG_DEBUG_SET.discr =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (DISCR_MULTIPLE))));
  cdat->dtup_48__MELT_HAS_FLAG_DEBUG_SET.nbval = 1;
  /*inimult dtup_49__MELT_HAS_FLAG_DEBUG_SET */
 /*_.VALTUP___V95*/ meltfptr[94] =
    (melt_ptr_t) & cdat->dtup_49__MELT_HAS_FLAG_DEBUG_SET;
  cdat->dtup_49__MELT_HAS_FLAG_DEBUG_SET.discr =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (DISCR_MULTIPLE))));
  cdat->dtup_49__MELT_HAS_FLAG_DEBUG_SET.nbval = 0;

/*inistring dstr_50__MELT_HAS_FLAG_*/
 /*_.VALSTR___V96*/ meltfptr[95] =
    (melt_ptr_t) & cdat->dstr_50__MELT_HAS_FLAG_;
  cdat->dstr_50__MELT_HAS_FLAG_.discr =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (DISCR_VERBATIM_STRING))));
  /*small inistring */ strncpy (cdat->dstr_50__MELT_HAS_FLAG_.val,
				"/*MELT_HAS_FLAG_DEBUG_SET*/ melt_flag_debug",
				sizeof (cdat->dstr_50__MELT_HAS_FLAG_.val) -
				1);
  cdat->dstr_50__MELT_HAS_FLAG_.val[43] = (char) 0;
  cdat->dstr_50__MELT_HAS_FLAG_.slen = 43;


/*inistring dstr_51__MELT_HAS_FLAG_DE*/
 /*_.VALSTR___V94*/ meltfptr[93] =
    (melt_ptr_t) & cdat->dstr_51__MELT_HAS_FLAG_DE;
  cdat->dstr_51__MELT_HAS_FLAG_DE.discr =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (DISCR_STRING))));
  /*small inistring */ strncpy (cdat->dstr_51__MELT_HAS_FLAG_DE.val,
				"MELT_HAS_FLAG_DEBUG_SET",
				sizeof (cdat->dstr_51__MELT_HAS_FLAG_DE.val) -
				1);
  cdat->dstr_51__MELT_HAS_FLAG_DE.val[23] = (char) 0;
  cdat->dstr_51__MELT_HAS_FLAG_DE.slen = 23;


/*iniobj dsym_52__MELT_REALLY_NEED_DBGLIM*/
  /*uniqueobj*/ if (! /*_.VALDATA___V98*/ meltfptr[97])
/*_.VALDATA___V98*/ meltfptr[97] =
      (melt_ptr_t) & cdat->dsym_52__MELT_REALLY_NEED_DBGLIM;
  if (MELT_LIKELY (!melt_prohibit_garbcoll))
    melt_assertmsg
      ("iniobj check.discr isobj dsym_52__MELT_REALLY_NEED_DBGLIM",
       melt_magic_discr ((melt_ptr_t)
			 (((melt_ptr_t) (MELT_PREDEF (CLASS_SYMBOL))))) ==
       MELTOBMAG_OBJECT);
  if (MELT_LIKELY (!melt_prohibit_garbcoll))
    melt_assertmsg
      ("iniobj check.discr objmagic dsym_52__MELT_REALLY_NEED_DBGLIM",
       ((meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (CLASS_SYMBOL)))))->
       meltobj_magic == MELTOBMAG_OBJECT);
  cdat->dsym_52__MELT_REALLY_NEED_DBGLIM.meltobj_class =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (CLASS_SYMBOL))));
  cdat->dsym_52__MELT_REALLY_NEED_DBGLIM.obj_hash = 1026196939;
  cdat->dsym_52__MELT_REALLY_NEED_DBGLIM.obj_len = 3;


/*inistring dstr_53__MELT_REALLY_NEED*/
 /*_.VALSTR___V99*/ meltfptr[98] =
    (melt_ptr_t) & cdat->dstr_53__MELT_REALLY_NEED;
  cdat->dstr_53__MELT_REALLY_NEED.discr =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (DISCR_STRING))));
  /*small inistring */ strncpy (cdat->dstr_53__MELT_REALLY_NEED.val,
				"MELT_REALLY_NEED_DBGLIM",
				sizeof (cdat->dstr_53__MELT_REALLY_NEED.val) -
				1);
  cdat->dstr_53__MELT_REALLY_NEED.val[23] = (char) 0;
  cdat->dstr_53__MELT_REALLY_NEED.slen = 23;

  MELT_LOCATION ("warmelt-debug.melt:77:/ iniobj");

/*iniobj dobj_54__MELT_REALLY_NEED_DBGLIM*/
/*_.VALDATA___V100*/ meltfptr[99] =
    (melt_ptr_t) & cdat->dobj_54__MELT_REALLY_NEED_DBGLIM;
  if (MELT_LIKELY (!melt_prohibit_garbcoll))
    melt_assertmsg
      ("iniobj check.discr isobj dobj_54__MELT_REALLY_NEED_DBGLIM",
       melt_magic_discr ((melt_ptr_t)
			 (((melt_ptr_t) (MELT_PREDEF (CLASS_PRIMITIVE))))) ==
       MELTOBMAG_OBJECT);
  if (MELT_LIKELY (!melt_prohibit_garbcoll))
    melt_assertmsg
      ("iniobj check.discr objmagic dobj_54__MELT_REALLY_NEED_DBGLIM",
       ((meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (CLASS_PRIMITIVE)))))->
       meltobj_magic == MELTOBMAG_OBJECT);
  cdat->dobj_54__MELT_REALLY_NEED_DBGLIM.meltobj_class =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (CLASS_PRIMITIVE))));
  cdat->dobj_54__MELT_REALLY_NEED_DBGLIM.obj_hash = 613324853;
  cdat->dobj_54__MELT_REALLY_NEED_DBGLIM.obj_len = 5;

  /*inimult dtup_55__MELT_REALLY_NEED_DBGLIM */
 /*_.VALTUP___V110*/ meltfptr[109] =
    (melt_ptr_t) & cdat->dtup_55__MELT_REALLY_NEED_DBGLIM;
  cdat->dtup_55__MELT_REALLY_NEED_DBGLIM.discr =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (DISCR_MULTIPLE))));
  cdat->dtup_55__MELT_REALLY_NEED_DBGLIM.nbval = 5;
  /*inimult dtup_56__MELT_REALLY_NEED_DBGLIM */
 /*_.VALTUP___V106*/ meltfptr[105] =
    (melt_ptr_t) & cdat->dtup_56__MELT_REALLY_NEED_DBGLIM;
  cdat->dtup_56__MELT_REALLY_NEED_DBGLIM.discr =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (DISCR_MULTIPLE))));
  cdat->dtup_56__MELT_REALLY_NEED_DBGLIM.nbval = 2;
  /*^iniobj */

/*iniobj dobj_57*/
/*_.VALDATA___V102*/ meltfptr[101] = (melt_ptr_t) & cdat->dobj_57;
  if (MELT_LIKELY (!melt_prohibit_garbcoll))
    melt_assertmsg ("iniobj check.discr isobj dobj_57",
		    melt_magic_discr ((melt_ptr_t)
				      (((melt_ptr_t)
					(MELT_PREDEF
					 (CLASS_FORMAL_BINDING))))) ==
		    MELTOBMAG_OBJECT);
  if (MELT_LIKELY (!melt_prohibit_garbcoll))
    melt_assertmsg ("iniobj check.discr objmagic dobj_57",
		    ((meltobject_ptr_t)
		     (((melt_ptr_t) (MELT_PREDEF (CLASS_FORMAL_BINDING)))))->
		    meltobj_magic == MELTOBMAG_OBJECT);
  cdat->dobj_57.meltobj_class =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (CLASS_FORMAL_BINDING))));
  cdat->dobj_57.obj_num = 0;
  cdat->dobj_57.obj_hash = 552659797;
  cdat->dobj_57.obj_len = 2;


/*iniobj dsym_58__LIMIT*/
  /*uniqueobj*/ if (! /*_.VALDATA___V104*/ meltfptr[103])
/*_.VALDATA___V104*/ meltfptr[103] = (melt_ptr_t) & cdat->dsym_58__LIMIT;
  if (MELT_LIKELY (!melt_prohibit_garbcoll))
    melt_assertmsg ("iniobj check.discr isobj dsym_58__LIMIT",
		    melt_magic_discr ((melt_ptr_t)
				      (((melt_ptr_t)
					(MELT_PREDEF (CLASS_SYMBOL))))) ==
		    MELTOBMAG_OBJECT);
  if (MELT_LIKELY (!melt_prohibit_garbcoll))
    melt_assertmsg ("iniobj check.discr objmagic dsym_58__LIMIT",
		    ((meltobject_ptr_t)
		     (((melt_ptr_t) (MELT_PREDEF (CLASS_SYMBOL)))))->
		    meltobj_magic == MELTOBMAG_OBJECT);
  cdat->dsym_58__LIMIT.meltobj_class =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (CLASS_SYMBOL))));
  cdat->dsym_58__LIMIT.obj_hash = 133458422;
  cdat->dsym_58__LIMIT.obj_len = 3;


/*inistring dstr_59__LIMIT*/
 /*_.VALSTR___V105*/ meltfptr[104] = (melt_ptr_t) & cdat->dstr_59__LIMIT;
  cdat->dstr_59__LIMIT.discr =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (DISCR_STRING))));
  /*small inistring */ strncpy (cdat->dstr_59__LIMIT.val, "LIMIT",
				sizeof (cdat->dstr_59__LIMIT.val) - 1);
  cdat->dstr_59__LIMIT.val[5] = (char) 0;
  cdat->dstr_59__LIMIT.slen = 5;

  /*^iniobj */

/*iniobj dobj_60*/
/*_.VALDATA___V103*/ meltfptr[102] = (melt_ptr_t) & cdat->dobj_60;
  if (MELT_LIKELY (!melt_prohibit_garbcoll))
    melt_assertmsg ("iniobj check.discr isobj dobj_60",
		    melt_magic_discr ((melt_ptr_t)
				      (((melt_ptr_t)
					(MELT_PREDEF
					 (CLASS_FORMAL_BINDING))))) ==
		    MELTOBMAG_OBJECT);
  if (MELT_LIKELY (!melt_prohibit_garbcoll))
    melt_assertmsg ("iniobj check.discr objmagic dobj_60",
		    ((meltobject_ptr_t)
		     (((melt_ptr_t) (MELT_PREDEF (CLASS_FORMAL_BINDING)))))->
		    meltobj_magic == MELTOBMAG_OBJECT);
  cdat->dobj_60.meltobj_class =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (CLASS_FORMAL_BINDING))));
  cdat->dobj_60.obj_num = 1;
  cdat->dobj_60.obj_hash = 691380199;
  cdat->dobj_60.obj_len = 2;


/*inistring dstr_61__MELT_REALLY_*/
 /*_.VALSTR___V107*/ meltfptr[106] =
    (melt_ptr_t) & cdat->dstr_61__MELT_REALLY_;
  cdat->dstr_61__MELT_REALLY_.discr =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (DISCR_VERBATIM_STRING))));
  /*small inistring */ strncpy (cdat->dstr_61__MELT_REALLY_.val,
				"( /*MELT_REALLY_NEED_DBGLIM*/ melt_need_debug_limit ((int) ",
				sizeof (cdat->dstr_61__MELT_REALLY_.val) - 1);
  cdat->dstr_61__MELT_REALLY_.val[59] = (char) 0;
  cdat->dstr_61__MELT_REALLY_.slen = 59;


/*inistring dstr_62__int_*/
 /*_.VALSTR___V108*/ meltfptr[107] = (melt_ptr_t) & cdat->dstr_62__int_;
  cdat->dstr_62__int_.discr =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (DISCR_VERBATIM_STRING))));
  /*small inistring */ strncpy (cdat->dstr_62__int_.val, ", (int) ",
				sizeof (cdat->dstr_62__int_.val) - 1);
  cdat->dstr_62__int_.val[8] = (char) 0;
  cdat->dstr_62__int_.slen = 8;


/*inistring dstr_63__*/
 /*_.VALSTR___V109*/ meltfptr[108] = (melt_ptr_t) & cdat->dstr_63__;
  cdat->dstr_63__.discr =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (DISCR_VERBATIM_STRING))));
  /*small inistring */ strncpy (cdat->dstr_63__.val, "))",
				sizeof (cdat->dstr_63__.val) - 1);
  cdat->dstr_63__.val[2] = (char) 0;
  cdat->dstr_63__.slen = 2;


/*inistring dstr_64__MELT_REALLY_NEED*/
 /*_.VALSTR___V101*/ meltfptr[100] =
    (melt_ptr_t) & cdat->dstr_64__MELT_REALLY_NEED;
  cdat->dstr_64__MELT_REALLY_NEED.discr =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (DISCR_STRING))));
  /*small inistring */ strncpy (cdat->dstr_64__MELT_REALLY_NEED.val,
				"MELT_REALLY_NEED_DBGLIM",
				sizeof (cdat->dstr_64__MELT_REALLY_NEED.val) -
				1);
  cdat->dstr_64__MELT_REALLY_NEED.val[23] = (char) 0;
  cdat->dstr_64__MELT_REALLY_NEED.slen = 23;

  /*inirout drout_65__DEBUG_MSG_FUN */
 /*_.VALROUT___V111*/ meltfptr[110] =
    (melt_ptr_t) & cdat->drout_65__DEBUG_MSG_FUN;
  cdat->drout_65__DEBUG_MSG_FUN.discr =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (DISCR_ROUTINE))));
  strncpy (cdat->drout_65__DEBUG_MSG_FUN.routdescr,
	   "DEBUG_MSG_FUN @warmelt-debug.melt:84", MELT_ROUTDESCR_LEN - 1);
  cdat->drout_65__DEBUG_MSG_FUN.nbval = 7;
  MELT_ROUTINE_SET_ROUTCODE (&cdat->drout_65__DEBUG_MSG_FUN,
			     meltrout_1_warmelt_debug_DEBUG_MSG_FUN);

  /*iniclos dclo_66__DEBUG_MSG_FUN */
 /*_.VALCLO___V120*/ meltfptr[119] =
    (melt_ptr_t) & cdat->dclo_66__DEBUG_MSG_FUN;
  cdat->dclo_66__DEBUG_MSG_FUN.discr =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (DISCR_CLOSURE))));
  cdat->dclo_66__DEBUG_MSG_FUN.nbval = 0;


/*iniobj dsym_67__INITIAL_SYSTEM_DATA*/
  /*uniqueobj*/ if (! /*_.VALDATA___V121*/ meltfptr[120])
/*_.VALDATA___V121*/ meltfptr[120] =
      (melt_ptr_t) & cdat->dsym_67__INITIAL_SYSTEM_DATA;
  if (MELT_LIKELY (!melt_prohibit_garbcoll))
    melt_assertmsg ("iniobj check.discr isobj dsym_67__INITIAL_SYSTEM_DATA",
		    melt_magic_discr ((melt_ptr_t)
				      (((melt_ptr_t)
					(MELT_PREDEF (CLASS_SYMBOL))))) ==
		    MELTOBMAG_OBJECT);
  if (MELT_LIKELY (!melt_prohibit_garbcoll))
    melt_assertmsg
      ("iniobj check.discr objmagic dsym_67__INITIAL_SYSTEM_DATA",
       ((meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (CLASS_SYMBOL)))))->
       meltobj_magic == MELTOBMAG_OBJECT);
  cdat->dsym_67__INITIAL_SYSTEM_DATA.meltobj_class =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (CLASS_SYMBOL))));
  cdat->dsym_67__INITIAL_SYSTEM_DATA.obj_hash = 771349808;
  cdat->dsym_67__INITIAL_SYSTEM_DATA.obj_len = 3;


/*inistring dstr_68__INITIAL_SYSTEM_D*/
 /*_.VALSTR___V122*/ meltfptr[121] =
    (melt_ptr_t) & cdat->dstr_68__INITIAL_SYSTEM_D;
  cdat->dstr_68__INITIAL_SYSTEM_D.discr =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (DISCR_STRING))));
  /*small inistring */ strncpy (cdat->dstr_68__INITIAL_SYSTEM_D.val,
				"INITIAL_SYSTEM_DATA",
				sizeof (cdat->dstr_68__INITIAL_SYSTEM_D.val) -
				1);
  cdat->dstr_68__INITIAL_SYSTEM_D.val[19] = (char) 0;
  cdat->dstr_68__INITIAL_SYSTEM_D.slen = 19;


/*iniobj dsym_69__CLASS_SYSTEM_DATA*/
  /*uniqueobj*/ if (! /*_.VALDATA___V123*/ meltfptr[122])
/*_.VALDATA___V123*/ meltfptr[122] =
      (melt_ptr_t) & cdat->dsym_69__CLASS_SYSTEM_DATA;
  if (MELT_LIKELY (!melt_prohibit_garbcoll))
    melt_assertmsg ("iniobj check.discr isobj dsym_69__CLASS_SYSTEM_DATA",
		    melt_magic_discr ((melt_ptr_t)
				      (((melt_ptr_t)
					(MELT_PREDEF (CLASS_SYMBOL))))) ==
		    MELTOBMAG_OBJECT);
  if (MELT_LIKELY (!melt_prohibit_garbcoll))
    melt_assertmsg ("iniobj check.discr objmagic dsym_69__CLASS_SYSTEM_DATA",
		    ((meltobject_ptr_t)
		     (((melt_ptr_t) (MELT_PREDEF (CLASS_SYMBOL)))))->
		    meltobj_magic == MELTOBMAG_OBJECT);
  cdat->dsym_69__CLASS_SYSTEM_DATA.meltobj_class =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (CLASS_SYMBOL))));
  cdat->dsym_69__CLASS_SYSTEM_DATA.obj_hash = 648451867;
  cdat->dsym_69__CLASS_SYSTEM_DATA.obj_len = 3;


/*inistring dstr_70__CLASS_SYSTEM_DAT*/
 /*_.VALSTR___V124*/ meltfptr[123] =
    (melt_ptr_t) & cdat->dstr_70__CLASS_SYSTEM_DAT;
  cdat->dstr_70__CLASS_SYSTEM_DAT.discr =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (DISCR_STRING))));
  /*small inistring */ strncpy (cdat->dstr_70__CLASS_SYSTEM_DAT.val,
				"CLASS_SYSTEM_DATA",
				sizeof (cdat->dstr_70__CLASS_SYSTEM_DAT.val) -
				1);
  cdat->dstr_70__CLASS_SYSTEM_DAT.val[17] = (char) 0;
  cdat->dstr_70__CLASS_SYSTEM_DAT.slen = 17;


/*iniobj dsym_71__DISCR_MAP_OBJECTS*/
  /*uniqueobj*/ if (! /*_.VALDATA___V125*/ meltfptr[124])
/*_.VALDATA___V125*/ meltfptr[124] =
      (melt_ptr_t) & cdat->dsym_71__DISCR_MAP_OBJECTS;
  if (MELT_LIKELY (!melt_prohibit_garbcoll))
    melt_assertmsg ("iniobj check.discr isobj dsym_71__DISCR_MAP_OBJECTS",
		    melt_magic_discr ((melt_ptr_t)
				      (((melt_ptr_t)
					(MELT_PREDEF (CLASS_SYMBOL))))) ==
		    MELTOBMAG_OBJECT);
  if (MELT_LIKELY (!melt_prohibit_garbcoll))
    melt_assertmsg ("iniobj check.discr objmagic dsym_71__DISCR_MAP_OBJECTS",
		    ((meltobject_ptr_t)
		     (((melt_ptr_t) (MELT_PREDEF (CLASS_SYMBOL)))))->
		    meltobj_magic == MELTOBMAG_OBJECT);
  cdat->dsym_71__DISCR_MAP_OBJECTS.meltobj_class =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (CLASS_SYMBOL))));
  cdat->dsym_71__DISCR_MAP_OBJECTS.obj_hash = 217147088;
  cdat->dsym_71__DISCR_MAP_OBJECTS.obj_len = 3;


/*inistring dstr_72__DISCR_MAP_OBJECT*/
 /*_.VALSTR___V126*/ meltfptr[125] =
    (melt_ptr_t) & cdat->dstr_72__DISCR_MAP_OBJECT;
  cdat->dstr_72__DISCR_MAP_OBJECT.discr =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (DISCR_STRING))));
  /*small inistring */ strncpy (cdat->dstr_72__DISCR_MAP_OBJECT.val,
				"DISCR_MAP_OBJECTS",
				sizeof (cdat->dstr_72__DISCR_MAP_OBJECT.val) -
				1);
  cdat->dstr_72__DISCR_MAP_OBJECT.val[17] = (char) 0;
  cdat->dstr_72__DISCR_MAP_OBJECT.slen = 17;


/*iniobj dsym_73__DISCR_INTEGER*/
  /*uniqueobj*/ if (! /*_.VALDATA___V127*/ meltfptr[126])
/*_.VALDATA___V127*/ meltfptr[126] =
      (melt_ptr_t) & cdat->dsym_73__DISCR_INTEGER;
  if (MELT_LIKELY (!melt_prohibit_garbcoll))
    melt_assertmsg ("iniobj check.discr isobj dsym_73__DISCR_INTEGER",
		    melt_magic_discr ((melt_ptr_t)
				      (((melt_ptr_t)
					(MELT_PREDEF (CLASS_SYMBOL))))) ==
		    MELTOBMAG_OBJECT);
  if (MELT_LIKELY (!melt_prohibit_garbcoll))
    melt_assertmsg ("iniobj check.discr objmagic dsym_73__DISCR_INTEGER",
		    ((meltobject_ptr_t)
		     (((melt_ptr_t) (MELT_PREDEF (CLASS_SYMBOL)))))->
		    meltobj_magic == MELTOBMAG_OBJECT);
  cdat->dsym_73__DISCR_INTEGER.meltobj_class =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (CLASS_SYMBOL))));
  cdat->dsym_73__DISCR_INTEGER.obj_hash = 119182103;
  cdat->dsym_73__DISCR_INTEGER.obj_len = 3;


/*inistring dstr_74__DISCR_INTEGER*/
 /*_.VALSTR___V128*/ meltfptr[127] =
    (melt_ptr_t) & cdat->dstr_74__DISCR_INTEGER;
  cdat->dstr_74__DISCR_INTEGER.discr =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (DISCR_STRING))));
  /*small inistring */ strncpy (cdat->dstr_74__DISCR_INTEGER.val,
				"DISCR_INTEGER",
				sizeof (cdat->dstr_74__DISCR_INTEGER.val) -
				1);
  cdat->dstr_74__DISCR_INTEGER.val[13] = (char) 0;
  cdat->dstr_74__DISCR_INTEGER.slen = 13;


/*iniobj dsym_75__DISCR_STRBUF*/
  /*uniqueobj*/ if (! /*_.VALDATA___V129*/ meltfptr[128])
/*_.VALDATA___V129*/ meltfptr[128] =
      (melt_ptr_t) & cdat->dsym_75__DISCR_STRBUF;
  if (MELT_LIKELY (!melt_prohibit_garbcoll))
    melt_assertmsg ("iniobj check.discr isobj dsym_75__DISCR_STRBUF",
		    melt_magic_discr ((melt_ptr_t)
				      (((melt_ptr_t)
					(MELT_PREDEF (CLASS_SYMBOL))))) ==
		    MELTOBMAG_OBJECT);
  if (MELT_LIKELY (!melt_prohibit_garbcoll))
    melt_assertmsg ("iniobj check.discr objmagic dsym_75__DISCR_STRBUF",
		    ((meltobject_ptr_t)
		     (((melt_ptr_t) (MELT_PREDEF (CLASS_SYMBOL)))))->
		    meltobj_magic == MELTOBMAG_OBJECT);
  cdat->dsym_75__DISCR_STRBUF.meltobj_class =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (CLASS_SYMBOL))));
  cdat->dsym_75__DISCR_STRBUF.obj_hash = 492035674;
  cdat->dsym_75__DISCR_STRBUF.obj_len = 3;


/*inistring dstr_76__DISCR_STRBUF*/
 /*_.VALSTR___V130*/ meltfptr[129] =
    (melt_ptr_t) & cdat->dstr_76__DISCR_STRBUF;
  cdat->dstr_76__DISCR_STRBUF.discr =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (DISCR_STRING))));
  /*small inistring */ strncpy (cdat->dstr_76__DISCR_STRBUF.val,
				"DISCR_STRBUF",
				sizeof (cdat->dstr_76__DISCR_STRBUF.val) - 1);
  cdat->dstr_76__DISCR_STRBUF.val[12] = (char) 0;
  cdat->dstr_76__DISCR_STRBUF.slen = 12;


/*iniobj dsym_77__DBG_OUTPUT*/
  /*uniqueobj*/ if (! /*_.VALDATA___V131*/ meltfptr[130])
/*_.VALDATA___V131*/ meltfptr[130] =
      (melt_ptr_t) & cdat->dsym_77__DBG_OUTPUT;
  if (MELT_LIKELY (!melt_prohibit_garbcoll))
    melt_assertmsg ("iniobj check.discr isobj dsym_77__DBG_OUTPUT",
		    melt_magic_discr ((melt_ptr_t)
				      (((melt_ptr_t)
					(MELT_PREDEF (CLASS_SYMBOL))))) ==
		    MELTOBMAG_OBJECT);
  if (MELT_LIKELY (!melt_prohibit_garbcoll))
    melt_assertmsg ("iniobj check.discr objmagic dsym_77__DBG_OUTPUT",
		    ((meltobject_ptr_t)
		     (((melt_ptr_t) (MELT_PREDEF (CLASS_SYMBOL)))))->
		    meltobj_magic == MELTOBMAG_OBJECT);
  cdat->dsym_77__DBG_OUTPUT.meltobj_class =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (CLASS_SYMBOL))));
  cdat->dsym_77__DBG_OUTPUT.obj_hash = 225394304;
  cdat->dsym_77__DBG_OUTPUT.obj_len = 3;


/*inistring dstr_78__DBG_OUTPUT*/
 /*_.VALSTR___V132*/ meltfptr[131] =
    (melt_ptr_t) & cdat->dstr_78__DBG_OUTPUT;
  cdat->dstr_78__DBG_OUTPUT.discr =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (DISCR_STRING))));
  /*small inistring */ strncpy (cdat->dstr_78__DBG_OUTPUT.val, "DBG_OUTPUT",
				sizeof (cdat->dstr_78__DBG_OUTPUT.val) - 1);
  cdat->dstr_78__DBG_OUTPUT.val[10] = (char) 0;
  cdat->dstr_78__DBG_OUTPUT.slen = 10;


/*iniobj dsym_79__CLASS_SELECTOR*/
  /*uniqueobj*/ if (! /*_.VALDATA___V133*/ meltfptr[132])
/*_.VALDATA___V133*/ meltfptr[132] =
      (melt_ptr_t) & cdat->dsym_79__CLASS_SELECTOR;
  if (MELT_LIKELY (!melt_prohibit_garbcoll))
    melt_assertmsg ("iniobj check.discr isobj dsym_79__CLASS_SELECTOR",
		    melt_magic_discr ((melt_ptr_t)
				      (((melt_ptr_t)
					(MELT_PREDEF (CLASS_SYMBOL))))) ==
		    MELTOBMAG_OBJECT);
  if (MELT_LIKELY (!melt_prohibit_garbcoll))
    melt_assertmsg ("iniobj check.discr objmagic dsym_79__CLASS_SELECTOR",
		    ((meltobject_ptr_t)
		     (((melt_ptr_t) (MELT_PREDEF (CLASS_SYMBOL)))))->
		    meltobj_magic == MELTOBMAG_OBJECT);
  cdat->dsym_79__CLASS_SELECTOR.meltobj_class =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (CLASS_SYMBOL))));
  cdat->dsym_79__CLASS_SELECTOR.obj_hash = 100646971;
  cdat->dsym_79__CLASS_SELECTOR.obj_len = 3;


/*inistring dstr_80__CLASS_SELECTOR*/
 /*_.VALSTR___V134*/ meltfptr[133] =
    (melt_ptr_t) & cdat->dstr_80__CLASS_SELECTOR;
  cdat->dstr_80__CLASS_SELECTOR.discr =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (DISCR_STRING))));
  /*small inistring */ strncpy (cdat->dstr_80__CLASS_SELECTOR.val,
				"CLASS_SELECTOR",
				sizeof (cdat->dstr_80__CLASS_SELECTOR.val) -
				1);
  cdat->dstr_80__CLASS_SELECTOR.val[14] = (char) 0;
  cdat->dstr_80__CLASS_SELECTOR.slen = 14;


/*iniobj dsym_81__RECV*/
  /*uniqueobj*/ if (! /*_.VALDATA___V135*/ meltfptr[134])
/*_.VALDATA___V135*/ meltfptr[134] = (melt_ptr_t) & cdat->dsym_81__RECV;
  if (MELT_LIKELY (!melt_prohibit_garbcoll))
    melt_assertmsg ("iniobj check.discr isobj dsym_81__RECV",
		    melt_magic_discr ((melt_ptr_t)
				      (((melt_ptr_t)
					(MELT_PREDEF (CLASS_SYMBOL))))) ==
		    MELTOBMAG_OBJECT);
  if (MELT_LIKELY (!melt_prohibit_garbcoll))
    melt_assertmsg ("iniobj check.discr objmagic dsym_81__RECV",
		    ((meltobject_ptr_t)
		     (((melt_ptr_t) (MELT_PREDEF (CLASS_SYMBOL)))))->
		    meltobj_magic == MELTOBMAG_OBJECT);
  cdat->dsym_81__RECV.meltobj_class =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (CLASS_SYMBOL))));
  cdat->dsym_81__RECV.obj_hash = 10842790;
  cdat->dsym_81__RECV.obj_len = 3;


/*inistring dstr_82__RECV*/
 /*_.VALSTR___V136*/ meltfptr[135] = (melt_ptr_t) & cdat->dstr_82__RECV;
  cdat->dstr_82__RECV.discr =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (DISCR_STRING))));
  /*small inistring */ strncpy (cdat->dstr_82__RECV.val, "RECV",
				sizeof (cdat->dstr_82__RECV.val) - 1);
  cdat->dstr_82__RECV.val[4] = (char) 0;
  cdat->dstr_82__RECV.slen = 4;

  MELT_LOCATION ("warmelt-debug.melt:173:/ iniobj");

/*iniobj dobj_83*/
/*_.VALDATA___V137*/ meltfptr[136] = (melt_ptr_t) & cdat->dobj_83;
  if (MELT_LIKELY (!melt_prohibit_garbcoll))
    melt_assertmsg ("iniobj check.discr isobj dobj_83",
		    melt_magic_discr ((melt_ptr_t)
				      (((melt_ptr_t)
					(MELT_PREDEF
					 (CLASS_FORMAL_BINDING))))) ==
		    MELTOBMAG_OBJECT);
  if (MELT_LIKELY (!melt_prohibit_garbcoll))
    melt_assertmsg ("iniobj check.discr objmagic dobj_83",
		    ((meltobject_ptr_t)
		     (((melt_ptr_t) (MELT_PREDEF (CLASS_FORMAL_BINDING)))))->
		    meltobj_magic == MELTOBMAG_OBJECT);
  cdat->dobj_83.meltobj_class =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (CLASS_FORMAL_BINDING))));
  cdat->dobj_83.obj_num = 0;
  cdat->dobj_83.obj_hash = 256457711;
  cdat->dobj_83.obj_len = 2;


/*iniobj dsym_84__DBGINFO*/
  /*uniqueobj*/ if (! /*_.VALDATA___V138*/ meltfptr[137])
/*_.VALDATA___V138*/ meltfptr[137] =
      (melt_ptr_t) & cdat->dsym_84__DBGINFO;
  if (MELT_LIKELY (!melt_prohibit_garbcoll))
    melt_assertmsg ("iniobj check.discr isobj dsym_84__DBGINFO",
		    melt_magic_discr ((melt_ptr_t)
				      (((melt_ptr_t)
					(MELT_PREDEF (CLASS_SYMBOL))))) ==
		    MELTOBMAG_OBJECT);
  if (MELT_LIKELY (!melt_prohibit_garbcoll))
    melt_assertmsg ("iniobj check.discr objmagic dsym_84__DBGINFO",
		    ((meltobject_ptr_t)
		     (((melt_ptr_t) (MELT_PREDEF (CLASS_SYMBOL)))))->
		    meltobj_magic == MELTOBMAG_OBJECT);
  cdat->dsym_84__DBGINFO.meltobj_class =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (CLASS_SYMBOL))));
  cdat->dsym_84__DBGINFO.obj_hash = 587771081;
  cdat->dsym_84__DBGINFO.obj_len = 3;


/*inistring dstr_85__DBGINFO*/
 /*_.VALSTR___V139*/ meltfptr[138] = (melt_ptr_t) & cdat->dstr_85__DBGINFO;
  cdat->dstr_85__DBGINFO.discr =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (DISCR_STRING))));
  /*small inistring */ strncpy (cdat->dstr_85__DBGINFO.val, "DBGINFO",
				sizeof (cdat->dstr_85__DBGINFO.val) - 1);
  cdat->dstr_85__DBGINFO.val[7] = (char) 0;
  cdat->dstr_85__DBGINFO.slen = 7;

  /*^iniobj */

/*iniobj dobj_86*/
/*_.VALDATA___V140*/ meltfptr[139] = (melt_ptr_t) & cdat->dobj_86;
  if (MELT_LIKELY (!melt_prohibit_garbcoll))
    melt_assertmsg ("iniobj check.discr isobj dobj_86",
		    melt_magic_discr ((melt_ptr_t)
				      (((melt_ptr_t)
					(MELT_PREDEF
					 (CLASS_FORMAL_BINDING))))) ==
		    MELTOBMAG_OBJECT);
  if (MELT_LIKELY (!melt_prohibit_garbcoll))
    melt_assertmsg ("iniobj check.discr objmagic dobj_86",
		    ((meltobject_ptr_t)
		     (((melt_ptr_t) (MELT_PREDEF (CLASS_FORMAL_BINDING)))))->
		    meltobj_magic == MELTOBMAG_OBJECT);
  cdat->dobj_86.meltobj_class =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (CLASS_FORMAL_BINDING))));
  cdat->dobj_86.obj_num = 1;
  cdat->dobj_86.obj_hash = 842524524;
  cdat->dobj_86.obj_len = 2;

  /*^iniobj */

/*iniobj dobj_87*/
/*_.VALDATA___V141*/ meltfptr[140] = (melt_ptr_t) & cdat->dobj_87;
  if (MELT_LIKELY (!melt_prohibit_garbcoll))
    melt_assertmsg ("iniobj check.discr isobj dobj_87",
		    melt_magic_discr ((melt_ptr_t)
				      (((melt_ptr_t)
					(MELT_PREDEF
					 (CLASS_FORMAL_BINDING))))) ==
		    MELTOBMAG_OBJECT);
  if (MELT_LIKELY (!melt_prohibit_garbcoll))
    melt_assertmsg ("iniobj check.discr objmagic dobj_87",
		    ((meltobject_ptr_t)
		     (((melt_ptr_t) (MELT_PREDEF (CLASS_FORMAL_BINDING)))))->
		    meltobj_magic == MELTOBMAG_OBJECT);
  cdat->dobj_87.meltobj_class =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (CLASS_FORMAL_BINDING))));
  cdat->dobj_87.obj_num = 2;
  cdat->dobj_87.obj_hash = 530578063;
  cdat->dobj_87.obj_len = 2;

  /*inimult dtup_88__DBG_OUTPUT */
 /*_.VALTUP___V142*/ meltfptr[141] =
    (melt_ptr_t) & cdat->dtup_88__DBG_OUTPUT;
  cdat->dtup_88__DBG_OUTPUT.discr =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (DISCR_MULTIPLE))));
  cdat->dtup_88__DBG_OUTPUT.nbval = 3;
  /*^iniobj */

/*iniobj dobj_89__DBG_OUTPUT*/
/*_.VALDATA___V118*/ meltfptr[117] =
    (melt_ptr_t) & cdat->dobj_89__DBG_OUTPUT;
  if (MELT_LIKELY (!melt_prohibit_garbcoll))
    melt_assertmsg ("iniobj check.discr isobj dobj_89__DBG_OUTPUT",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.CLASS_SELECTOR__V13*/
				       meltfptr[12])) == MELTOBMAG_OBJECT);
  if (MELT_LIKELY (!melt_prohibit_garbcoll))
    melt_assertmsg ("iniobj check.discr objmagic dobj_89__DBG_OUTPUT",
		    ((meltobject_ptr_t)
		     ( /*_.CLASS_SELECTOR__V13*/ meltfptr[12]))->
		    meltobj_magic == MELTOBMAG_OBJECT);
  cdat->dobj_89__DBG_OUTPUT.meltobj_class =
    (meltobject_ptr_t) ( /*_.CLASS_SELECTOR__V13*/ meltfptr[12]);
  cdat->dobj_89__DBG_OUTPUT.obj_hash = 261977353;
  cdat->dobj_89__DBG_OUTPUT.obj_len = 4;


/*inistring dstr_90__DBG_OUTPUT*/
 /*_.VALSTR___V119*/ meltfptr[118] =
    (melt_ptr_t) & cdat->dstr_90__DBG_OUTPUT;
  cdat->dstr_90__DBG_OUTPUT.discr =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (DISCR_STRING))));
  /*small inistring */ strncpy (cdat->dstr_90__DBG_OUTPUT.val, "DBG_OUTPUT",
				sizeof (cdat->dstr_90__DBG_OUTPUT.val) - 1);
  cdat->dstr_90__DBG_OUTPUT.val[10] = (char) 0;
  cdat->dstr_90__DBG_OUTPUT.slen = 10;


/*iniobj dsym_91__DBG_OUTPUTAGAIN*/
  /*uniqueobj*/ if (! /*_.VALDATA___V143*/ meltfptr[142])
/*_.VALDATA___V143*/ meltfptr[142] =
      (melt_ptr_t) & cdat->dsym_91__DBG_OUTPUTAGAIN;
  if (MELT_LIKELY (!melt_prohibit_garbcoll))
    melt_assertmsg ("iniobj check.discr isobj dsym_91__DBG_OUTPUTAGAIN",
		    melt_magic_discr ((melt_ptr_t)
				      (((melt_ptr_t)
					(MELT_PREDEF (CLASS_SYMBOL))))) ==
		    MELTOBMAG_OBJECT);
  if (MELT_LIKELY (!melt_prohibit_garbcoll))
    melt_assertmsg ("iniobj check.discr objmagic dsym_91__DBG_OUTPUTAGAIN",
		    ((meltobject_ptr_t)
		     (((melt_ptr_t) (MELT_PREDEF (CLASS_SYMBOL)))))->
		    meltobj_magic == MELTOBMAG_OBJECT);
  cdat->dsym_91__DBG_OUTPUTAGAIN.meltobj_class =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (CLASS_SYMBOL))));
  cdat->dsym_91__DBG_OUTPUTAGAIN.obj_hash = 28744821;
  cdat->dsym_91__DBG_OUTPUTAGAIN.obj_len = 3;


/*inistring dstr_92__DBG_OUTPUTAGAIN*/
 /*_.VALSTR___V144*/ meltfptr[143] =
    (melt_ptr_t) & cdat->dstr_92__DBG_OUTPUTAGAIN;
  cdat->dstr_92__DBG_OUTPUTAGAIN.discr =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (DISCR_STRING))));
  /*small inistring */ strncpy (cdat->dstr_92__DBG_OUTPUTAGAIN.val,
				"DBG_OUTPUTAGAIN",
				sizeof (cdat->dstr_92__DBG_OUTPUTAGAIN.val) -
				1);
  cdat->dstr_92__DBG_OUTPUTAGAIN.val[15] = (char) 0;
  cdat->dstr_92__DBG_OUTPUTAGAIN.slen = 15;

  MELT_LOCATION ("warmelt-debug.melt:182:/ iniobj");

/*iniobj dobj_93*/
/*_.VALDATA___V145*/ meltfptr[144] = (melt_ptr_t) & cdat->dobj_93;
  if (MELT_LIKELY (!melt_prohibit_garbcoll))
    melt_assertmsg ("iniobj check.discr isobj dobj_93",
		    melt_magic_discr ((melt_ptr_t)
				      (((melt_ptr_t)
					(MELT_PREDEF
					 (CLASS_FORMAL_BINDING))))) ==
		    MELTOBMAG_OBJECT);
  if (MELT_LIKELY (!melt_prohibit_garbcoll))
    melt_assertmsg ("iniobj check.discr objmagic dobj_93",
		    ((meltobject_ptr_t)
		     (((melt_ptr_t) (MELT_PREDEF (CLASS_FORMAL_BINDING)))))->
		    meltobj_magic == MELTOBMAG_OBJECT);
  cdat->dobj_93.meltobj_class =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (CLASS_FORMAL_BINDING))));
  cdat->dobj_93.obj_num = 0;
  cdat->dobj_93.obj_hash = 424513018;
  cdat->dobj_93.obj_len = 2;

  /*^iniobj */

/*iniobj dobj_94*/
/*_.VALDATA___V146*/ meltfptr[145] = (melt_ptr_t) & cdat->dobj_94;
  if (MELT_LIKELY (!melt_prohibit_garbcoll))
    melt_assertmsg ("iniobj check.discr isobj dobj_94",
		    melt_magic_discr ((melt_ptr_t)
				      (((melt_ptr_t)
					(MELT_PREDEF
					 (CLASS_FORMAL_BINDING))))) ==
		    MELTOBMAG_OBJECT);
  if (MELT_LIKELY (!melt_prohibit_garbcoll))
    melt_assertmsg ("iniobj check.discr objmagic dobj_94",
		    ((meltobject_ptr_t)
		     (((melt_ptr_t) (MELT_PREDEF (CLASS_FORMAL_BINDING)))))->
		    meltobj_magic == MELTOBMAG_OBJECT);
  cdat->dobj_94.meltobj_class =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (CLASS_FORMAL_BINDING))));
  cdat->dobj_94.obj_num = 1;
  cdat->dobj_94.obj_hash = 253179783;
  cdat->dobj_94.obj_len = 2;

  /*^iniobj */

/*iniobj dobj_95*/
/*_.VALDATA___V147*/ meltfptr[146] = (melt_ptr_t) & cdat->dobj_95;
  if (MELT_LIKELY (!melt_prohibit_garbcoll))
    melt_assertmsg ("iniobj check.discr isobj dobj_95",
		    melt_magic_discr ((melt_ptr_t)
				      (((melt_ptr_t)
					(MELT_PREDEF
					 (CLASS_FORMAL_BINDING))))) ==
		    MELTOBMAG_OBJECT);
  if (MELT_LIKELY (!melt_prohibit_garbcoll))
    melt_assertmsg ("iniobj check.discr objmagic dobj_95",
		    ((meltobject_ptr_t)
		     (((melt_ptr_t) (MELT_PREDEF (CLASS_FORMAL_BINDING)))))->
		    meltobj_magic == MELTOBMAG_OBJECT);
  cdat->dobj_95.meltobj_class =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (CLASS_FORMAL_BINDING))));
  cdat->dobj_95.obj_num = 2;
  cdat->dobj_95.obj_hash = 53389972;
  cdat->dobj_95.obj_len = 2;

  /*inimult dtup_96__DBG_OUTPUTAGAIN */
 /*_.VALTUP___V148*/ meltfptr[147] =
    (melt_ptr_t) & cdat->dtup_96__DBG_OUTPUTAGAIN;
  cdat->dtup_96__DBG_OUTPUTAGAIN.discr =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (DISCR_MULTIPLE))));
  cdat->dtup_96__DBG_OUTPUTAGAIN.nbval = 3;
  /*^iniobj */

/*iniobj dobj_97__DBG_OUTPUTAGAIN*/
/*_.VALDATA___V116*/ meltfptr[115] =
    (melt_ptr_t) & cdat->dobj_97__DBG_OUTPUTAGAIN;
  if (MELT_LIKELY (!melt_prohibit_garbcoll))
    melt_assertmsg ("iniobj check.discr isobj dobj_97__DBG_OUTPUTAGAIN",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.CLASS_SELECTOR__V13*/
				       meltfptr[12])) == MELTOBMAG_OBJECT);
  if (MELT_LIKELY (!melt_prohibit_garbcoll))
    melt_assertmsg ("iniobj check.discr objmagic dobj_97__DBG_OUTPUTAGAIN",
		    ((meltobject_ptr_t)
		     ( /*_.CLASS_SELECTOR__V13*/ meltfptr[12]))->
		    meltobj_magic == MELTOBMAG_OBJECT);
  cdat->dobj_97__DBG_OUTPUTAGAIN.meltobj_class =
    (meltobject_ptr_t) ( /*_.CLASS_SELECTOR__V13*/ meltfptr[12]);
  cdat->dobj_97__DBG_OUTPUTAGAIN.obj_hash = 36723111;
  cdat->dobj_97__DBG_OUTPUTAGAIN.obj_len = 4;


/*inistring dstr_98__DBG_OUTPUTAGAIN*/
 /*_.VALSTR___V117*/ meltfptr[116] =
    (melt_ptr_t) & cdat->dstr_98__DBG_OUTPUTAGAIN;
  cdat->dstr_98__DBG_OUTPUTAGAIN.discr =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (DISCR_STRING))));
  /*small inistring */ strncpy (cdat->dstr_98__DBG_OUTPUTAGAIN.val,
				"DBG_OUTPUTAGAIN",
				sizeof (cdat->dstr_98__DBG_OUTPUTAGAIN.val) -
				1);
  cdat->dstr_98__DBG_OUTPUTAGAIN.val[15] = (char) 0;
  cdat->dstr_98__DBG_OUTPUTAGAIN.slen = 15;


/*iniobj dsym_99__TREE_DEBUG_FUNCONT*/
  /*uniqueobj*/ if (! /*_.VALDATA___V149*/ meltfptr[148])
/*_.VALDATA___V149*/ meltfptr[148] =
      (melt_ptr_t) & cdat->dsym_99__TREE_DEBUG_FUNCONT;
  if (MELT_LIKELY (!melt_prohibit_garbcoll))
    melt_assertmsg ("iniobj check.discr isobj dsym_99__TREE_DEBUG_FUNCONT",
		    melt_magic_discr ((melt_ptr_t)
				      (((melt_ptr_t)
					(MELT_PREDEF (CLASS_SYMBOL))))) ==
		    MELTOBMAG_OBJECT);
  if (MELT_LIKELY (!melt_prohibit_garbcoll))
    melt_assertmsg ("iniobj check.discr objmagic dsym_99__TREE_DEBUG_FUNCONT",
		    ((meltobject_ptr_t)
		     (((melt_ptr_t) (MELT_PREDEF (CLASS_SYMBOL)))))->
		    meltobj_magic == MELTOBMAG_OBJECT);
  cdat->dsym_99__TREE_DEBUG_FUNCONT.meltobj_class =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (CLASS_SYMBOL))));
  cdat->dsym_99__TREE_DEBUG_FUNCONT.obj_hash = 374867797;
  cdat->dsym_99__TREE_DEBUG_FUNCONT.obj_len = 3;


/*inistring dstr_100__TREE_DEBUG_FUNCO*/
 /*_.VALSTR___V150*/ meltfptr[149] =
    (melt_ptr_t) & cdat->dstr_100__TREE_DEBUG_FUNCO;
  cdat->dstr_100__TREE_DEBUG_FUNCO.discr =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (DISCR_STRING))));
  /*small inistring */ strncpy (cdat->dstr_100__TREE_DEBUG_FUNCO.val,
				"TREE_DEBUG_FUNCONT",
				sizeof (cdat->dstr_100__TREE_DEBUG_FUNCO.
					val) - 1);
  cdat->dstr_100__TREE_DEBUG_FUNCO.val[18] = (char) 0;
  cdat->dstr_100__TREE_DEBUG_FUNCO.slen = 18;


/*iniobj dsym_101__CLASS_REFERENCE*/
  /*uniqueobj*/ if (! /*_.VALDATA___V151*/ meltfptr[150])
/*_.VALDATA___V151*/ meltfptr[150] =
      (melt_ptr_t) & cdat->dsym_101__CLASS_REFERENCE;
  if (MELT_LIKELY (!melt_prohibit_garbcoll))
    melt_assertmsg ("iniobj check.discr isobj dsym_101__CLASS_REFERENCE",
		    melt_magic_discr ((melt_ptr_t)
				      (((melt_ptr_t)
					(MELT_PREDEF (CLASS_SYMBOL))))) ==
		    MELTOBMAG_OBJECT);
  if (MELT_LIKELY (!melt_prohibit_garbcoll))
    melt_assertmsg ("iniobj check.discr objmagic dsym_101__CLASS_REFERENCE",
		    ((meltobject_ptr_t)
		     (((melt_ptr_t) (MELT_PREDEF (CLASS_SYMBOL)))))->
		    meltobj_magic == MELTOBMAG_OBJECT);
  cdat->dsym_101__CLASS_REFERENCE.meltobj_class =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (CLASS_SYMBOL))));
  cdat->dsym_101__CLASS_REFERENCE.obj_hash = 896623907;
  cdat->dsym_101__CLASS_REFERENCE.obj_len = 3;


/*inistring dstr_102__CLASS_REFERENCE*/
 /*_.VALSTR___V152*/ meltfptr[151] =
    (melt_ptr_t) & cdat->dstr_102__CLASS_REFERENCE;
  cdat->dstr_102__CLASS_REFERENCE.discr =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (DISCR_STRING))));
  /*small inistring */ strncpy (cdat->dstr_102__CLASS_REFERENCE.val,
				"CLASS_REFERENCE",
				sizeof (cdat->dstr_102__CLASS_REFERENCE.val) -
				1);
  cdat->dstr_102__CLASS_REFERENCE.val[15] = (char) 0;
  cdat->dstr_102__CLASS_REFERENCE.slen = 15;

  MELT_LOCATION ("warmelt-debug.melt:193:/ iniobj");

/*iniobj dobj_103__TREE_DEBUG_FUNCONT*/
/*_.VALDATA___V153*/ meltfptr[152] =
    (melt_ptr_t) & cdat->dobj_103__TREE_DEBUG_FUNCONT;
  if (MELT_LIKELY (!melt_prohibit_garbcoll))
    melt_assertmsg ("iniobj check.discr isobj dobj_103__TREE_DEBUG_FUNCONT",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.CLASS_REFERENCE__V14*/
				       meltfptr[13])) == MELTOBMAG_OBJECT);
  if (MELT_LIKELY (!melt_prohibit_garbcoll))
    melt_assertmsg
      ("iniobj check.discr objmagic dobj_103__TREE_DEBUG_FUNCONT",
       ((meltobject_ptr_t) ( /*_.CLASS_REFERENCE__V14*/ meltfptr[13]))->
       meltobj_magic == MELTOBMAG_OBJECT);
  cdat->dobj_103__TREE_DEBUG_FUNCONT.meltobj_class =
    (meltobject_ptr_t) ( /*_.CLASS_REFERENCE__V14*/ meltfptr[13]);
  cdat->dobj_103__TREE_DEBUG_FUNCONT.obj_hash = 785242464;
  cdat->dobj_103__TREE_DEBUG_FUNCONT.obj_len = 1;

  /*inirout drout_104__REGISTER_TREE_DEBUG_FUN */
 /*_.VALROUT___V154*/ meltfptr[153] =
    (melt_ptr_t) & cdat->drout_104__REGISTER_TREE_DEBUG_FUN;
  cdat->drout_104__REGISTER_TREE_DEBUG_FUN.discr =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (DISCR_ROUTINE))));
  strncpy (cdat->drout_104__REGISTER_TREE_DEBUG_FUN.routdescr,
	   "REGISTER_TREE_DEBUG_FUN @warmelt-debug.melt:194",
	   MELT_ROUTDESCR_LEN - 1);
  cdat->drout_104__REGISTER_TREE_DEBUG_FUN.nbval = 2;
  MELT_ROUTINE_SET_ROUTCODE (&cdat->drout_104__REGISTER_TREE_DEBUG_FUN,
			     meltrout_2_warmelt_debug_REGISTER_TREE_DEBUG_FUN);

  /*iniclos dclo_105__REGISTER_TREE_DEBUG_FUN */
 /*_.VALCLO___V155*/ meltfptr[154] =
    (melt_ptr_t) & cdat->dclo_105__REGISTER_TREE_DEBUG_FUN;
  cdat->dclo_105__REGISTER_TREE_DEBUG_FUN.discr =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (DISCR_CLOSURE))));
  cdat->dclo_105__REGISTER_TREE_DEBUG_FUN.nbval = 0;


/*iniobj dsym_106__GIMPLE_DEBUG_FUNCONT*/
  /*uniqueobj*/ if (! /*_.VALDATA___V156*/ meltfptr[155])
/*_.VALDATA___V156*/ meltfptr[155] =
      (melt_ptr_t) & cdat->dsym_106__GIMPLE_DEBUG_FUNCONT;
  if (MELT_LIKELY (!melt_prohibit_garbcoll))
    melt_assertmsg ("iniobj check.discr isobj dsym_106__GIMPLE_DEBUG_FUNCONT",
		    melt_magic_discr ((melt_ptr_t)
				      (((melt_ptr_t)
					(MELT_PREDEF (CLASS_SYMBOL))))) ==
		    MELTOBMAG_OBJECT);
  if (MELT_LIKELY (!melt_prohibit_garbcoll))
    melt_assertmsg
      ("iniobj check.discr objmagic dsym_106__GIMPLE_DEBUG_FUNCONT",
       ((meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (CLASS_SYMBOL)))))->
       meltobj_magic == MELTOBMAG_OBJECT);
  cdat->dsym_106__GIMPLE_DEBUG_FUNCONT.meltobj_class =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (CLASS_SYMBOL))));
  cdat->dsym_106__GIMPLE_DEBUG_FUNCONT.obj_hash = 51178959;
  cdat->dsym_106__GIMPLE_DEBUG_FUNCONT.obj_len = 3;


/*inistring dstr_107__GIMPLE_DEBUG_FUN*/
 /*_.VALSTR___V157*/ meltfptr[156] =
    (melt_ptr_t) & cdat->dstr_107__GIMPLE_DEBUG_FUN;
  cdat->dstr_107__GIMPLE_DEBUG_FUN.discr =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (DISCR_STRING))));
  /*small inistring */ strncpy (cdat->dstr_107__GIMPLE_DEBUG_FUN.val,
				"GIMPLE_DEBUG_FUNCONT",
				sizeof (cdat->dstr_107__GIMPLE_DEBUG_FUN.
					val) - 1);
  cdat->dstr_107__GIMPLE_DEBUG_FUN.val[20] = (char) 0;
  cdat->dstr_107__GIMPLE_DEBUG_FUN.slen = 20;

  MELT_LOCATION ("warmelt-debug.melt:206:/ iniobj");

/*iniobj dobj_108__GIMPLE_DEBUG_FUNCONT*/
/*_.VALDATA___V158*/ meltfptr[157] =
    (melt_ptr_t) & cdat->dobj_108__GIMPLE_DEBUG_FUNCONT;
  if (MELT_LIKELY (!melt_prohibit_garbcoll))
    melt_assertmsg ("iniobj check.discr isobj dobj_108__GIMPLE_DEBUG_FUNCONT",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.CLASS_REFERENCE__V14*/
				       meltfptr[13])) == MELTOBMAG_OBJECT);
  if (MELT_LIKELY (!melt_prohibit_garbcoll))
    melt_assertmsg
      ("iniobj check.discr objmagic dobj_108__GIMPLE_DEBUG_FUNCONT",
       ((meltobject_ptr_t) ( /*_.CLASS_REFERENCE__V14*/ meltfptr[13]))->
       meltobj_magic == MELTOBMAG_OBJECT);
  cdat->dobj_108__GIMPLE_DEBUG_FUNCONT.meltobj_class =
    (meltobject_ptr_t) ( /*_.CLASS_REFERENCE__V14*/ meltfptr[13]);
  cdat->dobj_108__GIMPLE_DEBUG_FUNCONT.obj_hash = 334234826;
  cdat->dobj_108__GIMPLE_DEBUG_FUNCONT.obj_len = 1;

  /*inirout drout_109__REGISTER_GIMPLE_DEBUG_FUN */
 /*_.VALROUT___V159*/ meltfptr[158] =
    (melt_ptr_t) & cdat->drout_109__REGISTER_GIMPLE_DEBUG_FUN;
  cdat->drout_109__REGISTER_GIMPLE_DEBUG_FUN.discr =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (DISCR_ROUTINE))));
  strncpy (cdat->drout_109__REGISTER_GIMPLE_DEBUG_FUN.routdescr,
	   "REGISTER_GIMPLE_DEBUG_FUN @warmelt-debug.melt:207",
	   MELT_ROUTDESCR_LEN - 1);
  cdat->drout_109__REGISTER_GIMPLE_DEBUG_FUN.nbval = 2;
  MELT_ROUTINE_SET_ROUTCODE (&cdat->drout_109__REGISTER_GIMPLE_DEBUG_FUN,
			     meltrout_3_warmelt_debug_REGISTER_GIMPLE_DEBUG_FUN);

  /*iniclos dclo_110__REGISTER_GIMPLE_DEBUG_FUN */
 /*_.VALCLO___V160*/ meltfptr[159] =
    (melt_ptr_t) & cdat->dclo_110__REGISTER_GIMPLE_DEBUG_FUN;
  cdat->dclo_110__REGISTER_GIMPLE_DEBUG_FUN.discr =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (DISCR_CLOSURE))));
  cdat->dclo_110__REGISTER_GIMPLE_DEBUG_FUN.nbval = 0;


/*iniobj dsym_111__GIMPLESEQ_DEBUG_FUNCONT*/
  /*uniqueobj*/ if (! /*_.VALDATA___V161*/ meltfptr[160])
/*_.VALDATA___V161*/ meltfptr[160] =
      (melt_ptr_t) & cdat->dsym_111__GIMPLESEQ_DEBUG_FUNCONT;
  if (MELT_LIKELY (!melt_prohibit_garbcoll))
    melt_assertmsg
      ("iniobj check.discr isobj dsym_111__GIMPLESEQ_DEBUG_FUNCONT",
       melt_magic_discr ((melt_ptr_t)
			 (((melt_ptr_t) (MELT_PREDEF (CLASS_SYMBOL))))) ==
       MELTOBMAG_OBJECT);
  if (MELT_LIKELY (!melt_prohibit_garbcoll))
    melt_assertmsg
      ("iniobj check.discr objmagic dsym_111__GIMPLESEQ_DEBUG_FUNCONT",
       ((meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (CLASS_SYMBOL)))))->
       meltobj_magic == MELTOBMAG_OBJECT);
  cdat->dsym_111__GIMPLESEQ_DEBUG_FUNCONT.meltobj_class =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (CLASS_SYMBOL))));
  cdat->dsym_111__GIMPLESEQ_DEBUG_FUNCONT.obj_hash = 813085508;
  cdat->dsym_111__GIMPLESEQ_DEBUG_FUNCONT.obj_len = 3;


/*inistring dstr_112__GIMPLESEQ_DEBUG_*/
 /*_.VALSTR___V162*/ meltfptr[161] =
    (melt_ptr_t) & cdat->dstr_112__GIMPLESEQ_DEBUG_;
  cdat->dstr_112__GIMPLESEQ_DEBUG_.discr =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (DISCR_STRING))));
  /*small inistring */ strncpy (cdat->dstr_112__GIMPLESEQ_DEBUG_.val,
				"GIMPLESEQ_DEBUG_FUNCONT",
				sizeof (cdat->dstr_112__GIMPLESEQ_DEBUG_.
					val) - 1);
  cdat->dstr_112__GIMPLESEQ_DEBUG_.val[23] = (char) 0;
  cdat->dstr_112__GIMPLESEQ_DEBUG_.slen = 23;

  MELT_LOCATION ("warmelt-debug.melt:219:/ iniobj");

/*iniobj dobj_113__GIMPLESEQ_DEBUG_FUNCONT*/
/*_.VALDATA___V163*/ meltfptr[162] =
    (melt_ptr_t) & cdat->dobj_113__GIMPLESEQ_DEBUG_FUNCONT;
  if (MELT_LIKELY (!melt_prohibit_garbcoll))
    melt_assertmsg
      ("iniobj check.discr isobj dobj_113__GIMPLESEQ_DEBUG_FUNCONT",
       melt_magic_discr ((melt_ptr_t)
			 ( /*_.CLASS_REFERENCE__V14*/ meltfptr[13])) ==
       MELTOBMAG_OBJECT);
  if (MELT_LIKELY (!melt_prohibit_garbcoll))
    melt_assertmsg
      ("iniobj check.discr objmagic dobj_113__GIMPLESEQ_DEBUG_FUNCONT",
       ((meltobject_ptr_t) ( /*_.CLASS_REFERENCE__V14*/ meltfptr[13]))->
       meltobj_magic == MELTOBMAG_OBJECT);
  cdat->dobj_113__GIMPLESEQ_DEBUG_FUNCONT.meltobj_class =
    (meltobject_ptr_t) ( /*_.CLASS_REFERENCE__V14*/ meltfptr[13]);
  cdat->dobj_113__GIMPLESEQ_DEBUG_FUNCONT.obj_hash = 1052358913;
  cdat->dobj_113__GIMPLESEQ_DEBUG_FUNCONT.obj_len = 1;

  /*inirout drout_114__REGISTER_GIMPLESEQ_DEBUG_FUN */
 /*_.VALROUT___V164*/ meltfptr[163] =
    (melt_ptr_t) & cdat->drout_114__REGISTER_GIMPLESEQ_DEBUG_FUN;
  cdat->drout_114__REGISTER_GIMPLESEQ_DEBUG_FUN.discr =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (DISCR_ROUTINE))));
  strncpy (cdat->drout_114__REGISTER_GIMPLESEQ_DEBUG_FUN.routdescr,
	   "REGISTER_GIMPLESEQ_DEBUG_FUN @warmelt-debug.melt:220",
	   MELT_ROUTDESCR_LEN - 1);
  cdat->drout_114__REGISTER_GIMPLESEQ_DEBUG_FUN.nbval = 2;
  MELT_ROUTINE_SET_ROUTCODE (&cdat->drout_114__REGISTER_GIMPLESEQ_DEBUG_FUN,
			     meltrout_4_warmelt_debug_REGISTER_GIMPLESEQ_DEBUG_FUN);

  /*iniclos dclo_115__REGISTER_GIMPLESEQ_DEBUG_FUN */
 /*_.VALCLO___V165*/ meltfptr[164] =
    (melt_ptr_t) & cdat->dclo_115__REGISTER_GIMPLESEQ_DEBUG_FUN;
  cdat->dclo_115__REGISTER_GIMPLESEQ_DEBUG_FUN.discr =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (DISCR_CLOSURE))));
  cdat->dclo_115__REGISTER_GIMPLESEQ_DEBUG_FUN.nbval = 0;


/*iniobj dsym_116__EDGE_DEBUG_FUNCONT*/
  /*uniqueobj*/ if (! /*_.VALDATA___V166*/ meltfptr[165])
/*_.VALDATA___V166*/ meltfptr[165] =
      (melt_ptr_t) & cdat->dsym_116__EDGE_DEBUG_FUNCONT;
  if (MELT_LIKELY (!melt_prohibit_garbcoll))
    melt_assertmsg ("iniobj check.discr isobj dsym_116__EDGE_DEBUG_FUNCONT",
		    melt_magic_discr ((melt_ptr_t)
				      (((melt_ptr_t)
					(MELT_PREDEF (CLASS_SYMBOL))))) ==
		    MELTOBMAG_OBJECT);
  if (MELT_LIKELY (!melt_prohibit_garbcoll))
    melt_assertmsg
      ("iniobj check.discr objmagic dsym_116__EDGE_DEBUG_FUNCONT",
       ((meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (CLASS_SYMBOL)))))->
       meltobj_magic == MELTOBMAG_OBJECT);
  cdat->dsym_116__EDGE_DEBUG_FUNCONT.meltobj_class =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (CLASS_SYMBOL))));
  cdat->dsym_116__EDGE_DEBUG_FUNCONT.obj_hash = 803948317;
  cdat->dsym_116__EDGE_DEBUG_FUNCONT.obj_len = 3;


/*inistring dstr_117__EDGE_DEBUG_FUNCO*/
 /*_.VALSTR___V167*/ meltfptr[166] =
    (melt_ptr_t) & cdat->dstr_117__EDGE_DEBUG_FUNCO;
  cdat->dstr_117__EDGE_DEBUG_FUNCO.discr =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (DISCR_STRING))));
  /*small inistring */ strncpy (cdat->dstr_117__EDGE_DEBUG_FUNCO.val,
				"EDGE_DEBUG_FUNCONT",
				sizeof (cdat->dstr_117__EDGE_DEBUG_FUNCO.
					val) - 1);
  cdat->dstr_117__EDGE_DEBUG_FUNCO.val[18] = (char) 0;
  cdat->dstr_117__EDGE_DEBUG_FUNCO.slen = 18;

  MELT_LOCATION ("warmelt-debug.melt:232:/ iniobj");

/*iniobj dobj_118__EDGE_DEBUG_FUNCONT*/
/*_.VALDATA___V168*/ meltfptr[167] =
    (melt_ptr_t) & cdat->dobj_118__EDGE_DEBUG_FUNCONT;
  if (MELT_LIKELY (!melt_prohibit_garbcoll))
    melt_assertmsg ("iniobj check.discr isobj dobj_118__EDGE_DEBUG_FUNCONT",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.CLASS_REFERENCE__V14*/
				       meltfptr[13])) == MELTOBMAG_OBJECT);
  if (MELT_LIKELY (!melt_prohibit_garbcoll))
    melt_assertmsg
      ("iniobj check.discr objmagic dobj_118__EDGE_DEBUG_FUNCONT",
       ((meltobject_ptr_t) ( /*_.CLASS_REFERENCE__V14*/ meltfptr[13]))->
       meltobj_magic == MELTOBMAG_OBJECT);
  cdat->dobj_118__EDGE_DEBUG_FUNCONT.meltobj_class =
    (meltobject_ptr_t) ( /*_.CLASS_REFERENCE__V14*/ meltfptr[13]);
  cdat->dobj_118__EDGE_DEBUG_FUNCONT.obj_hash = 439156231;
  cdat->dobj_118__EDGE_DEBUG_FUNCONT.obj_len = 1;

  /*inirout drout_119__REGISTER_EDGE_DEBUG_FUN */
 /*_.VALROUT___V169*/ meltfptr[168] =
    (melt_ptr_t) & cdat->drout_119__REGISTER_EDGE_DEBUG_FUN;
  cdat->drout_119__REGISTER_EDGE_DEBUG_FUN.discr =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (DISCR_ROUTINE))));
  strncpy (cdat->drout_119__REGISTER_EDGE_DEBUG_FUN.routdescr,
	   "REGISTER_EDGE_DEBUG_FUN @warmelt-debug.melt:233",
	   MELT_ROUTDESCR_LEN - 1);
  cdat->drout_119__REGISTER_EDGE_DEBUG_FUN.nbval = 2;
  MELT_ROUTINE_SET_ROUTCODE (&cdat->drout_119__REGISTER_EDGE_DEBUG_FUN,
			     meltrout_5_warmelt_debug_REGISTER_EDGE_DEBUG_FUN);

  /*iniclos dclo_120__REGISTER_EDGE_DEBUG_FUN */
 /*_.VALCLO___V170*/ meltfptr[169] =
    (melt_ptr_t) & cdat->dclo_120__REGISTER_EDGE_DEBUG_FUN;
  cdat->dclo_120__REGISTER_EDGE_DEBUG_FUN.discr =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (DISCR_CLOSURE))));
  cdat->dclo_120__REGISTER_EDGE_DEBUG_FUN.nbval = 0;


/*iniobj dsym_121__BASICBLOCK_DEBUG_FUNCONT*/
  /*uniqueobj*/ if (! /*_.VALDATA___V171*/ meltfptr[170])
/*_.VALDATA___V171*/ meltfptr[170] =
      (melt_ptr_t) & cdat->dsym_121__BASICBLOCK_DEBUG_FUNCONT;
  if (MELT_LIKELY (!melt_prohibit_garbcoll))
    melt_assertmsg
      ("iniobj check.discr isobj dsym_121__BASICBLOCK_DEBUG_FUNCONT",
       melt_magic_discr ((melt_ptr_t)
			 (((melt_ptr_t) (MELT_PREDEF (CLASS_SYMBOL))))) ==
       MELTOBMAG_OBJECT);
  if (MELT_LIKELY (!melt_prohibit_garbcoll))
    melt_assertmsg
      ("iniobj check.discr objmagic dsym_121__BASICBLOCK_DEBUG_FUNCONT",
       ((meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (CLASS_SYMBOL)))))->
       meltobj_magic == MELTOBMAG_OBJECT);
  cdat->dsym_121__BASICBLOCK_DEBUG_FUNCONT.meltobj_class =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (CLASS_SYMBOL))));
  cdat->dsym_121__BASICBLOCK_DEBUG_FUNCONT.obj_hash = 507389899;
  cdat->dsym_121__BASICBLOCK_DEBUG_FUNCONT.obj_len = 3;


/*inistring dstr_122__BASICBLOCK_DEBUG*/
 /*_.VALSTR___V172*/ meltfptr[171] =
    (melt_ptr_t) & cdat->dstr_122__BASICBLOCK_DEBUG;
  cdat->dstr_122__BASICBLOCK_DEBUG.discr =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (DISCR_STRING))));
  /*small inistring */ strncpy (cdat->dstr_122__BASICBLOCK_DEBUG.val,
				"BASICBLOCK_DEBUG_FUNCONT",
				sizeof (cdat->dstr_122__BASICBLOCK_DEBUG.
					val) - 1);
  cdat->dstr_122__BASICBLOCK_DEBUG.val[24] = (char) 0;
  cdat->dstr_122__BASICBLOCK_DEBUG.slen = 24;

  MELT_LOCATION ("warmelt-debug.melt:245:/ iniobj");

/*iniobj dobj_123__BASICBLOCK_DEBUG_FUNCONT*/
/*_.VALDATA___V173*/ meltfptr[172] =
    (melt_ptr_t) & cdat->dobj_123__BASICBLOCK_DEBUG_FUNCONT;
  if (MELT_LIKELY (!melt_prohibit_garbcoll))
    melt_assertmsg
      ("iniobj check.discr isobj dobj_123__BASICBLOCK_DEBUG_FUNCONT",
       melt_magic_discr ((melt_ptr_t)
			 ( /*_.CLASS_REFERENCE__V14*/ meltfptr[13])) ==
       MELTOBMAG_OBJECT);
  if (MELT_LIKELY (!melt_prohibit_garbcoll))
    melt_assertmsg
      ("iniobj check.discr objmagic dobj_123__BASICBLOCK_DEBUG_FUNCONT",
       ((meltobject_ptr_t) ( /*_.CLASS_REFERENCE__V14*/ meltfptr[13]))->
       meltobj_magic == MELTOBMAG_OBJECT);
  cdat->dobj_123__BASICBLOCK_DEBUG_FUNCONT.meltobj_class =
    (meltobject_ptr_t) ( /*_.CLASS_REFERENCE__V14*/ meltfptr[13]);
  cdat->dobj_123__BASICBLOCK_DEBUG_FUNCONT.obj_hash = 367367980;
  cdat->dobj_123__BASICBLOCK_DEBUG_FUNCONT.obj_len = 1;

  /*inirout drout_124__REGISTER_BASICBLOCK_DEBUG_FUN */
 /*_.VALROUT___V174*/ meltfptr[173] =
    (melt_ptr_t) & cdat->drout_124__REGISTER_BASICBLOCK_DEBUG_FUN;
  cdat->drout_124__REGISTER_BASICBLOCK_DEBUG_FUN.discr =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (DISCR_ROUTINE))));
  strncpy (cdat->drout_124__REGISTER_BASICBLOCK_DEBUG_FUN.routdescr,
	   "REGISTER_BASICBLOCK_DEBUG_FUN @warmelt-debug.melt:246",
	   MELT_ROUTDESCR_LEN - 1);
  cdat->drout_124__REGISTER_BASICBLOCK_DEBUG_FUN.nbval = 2;
  MELT_ROUTINE_SET_ROUTCODE (&cdat->drout_124__REGISTER_BASICBLOCK_DEBUG_FUN,
			     meltrout_6_warmelt_debug_REGISTER_BASICBLOCK_DEBUG_FUN);

  /*iniclos dclo_125__REGISTER_BASICBLOCK_DEBUG_FUN */
 /*_.VALCLO___V175*/ meltfptr[174] =
    (melt_ptr_t) & cdat->dclo_125__REGISTER_BASICBLOCK_DEBUG_FUN;
  cdat->dclo_125__REGISTER_BASICBLOCK_DEBUG_FUN.discr =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (DISCR_CLOSURE))));
  cdat->dclo_125__REGISTER_BASICBLOCK_DEBUG_FUN.nbval = 0;


/*iniobj dsym_126__LOOP_DEBUG_FUNCONT*/
  /*uniqueobj*/ if (! /*_.VALDATA___V176*/ meltfptr[175])
/*_.VALDATA___V176*/ meltfptr[175] =
      (melt_ptr_t) & cdat->dsym_126__LOOP_DEBUG_FUNCONT;
  if (MELT_LIKELY (!melt_prohibit_garbcoll))
    melt_assertmsg ("iniobj check.discr isobj dsym_126__LOOP_DEBUG_FUNCONT",
		    melt_magic_discr ((melt_ptr_t)
				      (((melt_ptr_t)
					(MELT_PREDEF (CLASS_SYMBOL))))) ==
		    MELTOBMAG_OBJECT);
  if (MELT_LIKELY (!melt_prohibit_garbcoll))
    melt_assertmsg
      ("iniobj check.discr objmagic dsym_126__LOOP_DEBUG_FUNCONT",
       ((meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (CLASS_SYMBOL)))))->
       meltobj_magic == MELTOBMAG_OBJECT);
  cdat->dsym_126__LOOP_DEBUG_FUNCONT.meltobj_class =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (CLASS_SYMBOL))));
  cdat->dsym_126__LOOP_DEBUG_FUNCONT.obj_hash = 436769164;
  cdat->dsym_126__LOOP_DEBUG_FUNCONT.obj_len = 3;


/*inistring dstr_127__LOOP_DEBUG_FUNCO*/
 /*_.VALSTR___V177*/ meltfptr[176] =
    (melt_ptr_t) & cdat->dstr_127__LOOP_DEBUG_FUNCO;
  cdat->dstr_127__LOOP_DEBUG_FUNCO.discr =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (DISCR_STRING))));
  /*small inistring */ strncpy (cdat->dstr_127__LOOP_DEBUG_FUNCO.val,
				"LOOP_DEBUG_FUNCONT",
				sizeof (cdat->dstr_127__LOOP_DEBUG_FUNCO.
					val) - 1);
  cdat->dstr_127__LOOP_DEBUG_FUNCO.val[18] = (char) 0;
  cdat->dstr_127__LOOP_DEBUG_FUNCO.slen = 18;

  MELT_LOCATION ("warmelt-debug.melt:258:/ iniobj");

/*iniobj dobj_128__LOOP_DEBUG_FUNCONT*/
/*_.VALDATA___V178*/ meltfptr[177] =
    (melt_ptr_t) & cdat->dobj_128__LOOP_DEBUG_FUNCONT;
  if (MELT_LIKELY (!melt_prohibit_garbcoll))
    melt_assertmsg ("iniobj check.discr isobj dobj_128__LOOP_DEBUG_FUNCONT",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.CLASS_REFERENCE__V14*/
				       meltfptr[13])) == MELTOBMAG_OBJECT);
  if (MELT_LIKELY (!melt_prohibit_garbcoll))
    melt_assertmsg
      ("iniobj check.discr objmagic dobj_128__LOOP_DEBUG_FUNCONT",
       ((meltobject_ptr_t) ( /*_.CLASS_REFERENCE__V14*/ meltfptr[13]))->
       meltobj_magic == MELTOBMAG_OBJECT);
  cdat->dobj_128__LOOP_DEBUG_FUNCONT.meltobj_class =
    (meltobject_ptr_t) ( /*_.CLASS_REFERENCE__V14*/ meltfptr[13]);
  cdat->dobj_128__LOOP_DEBUG_FUNCONT.obj_hash = 590670596;
  cdat->dobj_128__LOOP_DEBUG_FUNCONT.obj_len = 1;

  /*inirout drout_129__REGISTER_LOOP_DEBUG_FUN */
 /*_.VALROUT___V179*/ meltfptr[178] =
    (melt_ptr_t) & cdat->drout_129__REGISTER_LOOP_DEBUG_FUN;
  cdat->drout_129__REGISTER_LOOP_DEBUG_FUN.discr =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (DISCR_ROUTINE))));
  strncpy (cdat->drout_129__REGISTER_LOOP_DEBUG_FUN.routdescr,
	   "REGISTER_LOOP_DEBUG_FUN @warmelt-debug.melt:259",
	   MELT_ROUTDESCR_LEN - 1);
  cdat->drout_129__REGISTER_LOOP_DEBUG_FUN.nbval = 2;
  MELT_ROUTINE_SET_ROUTCODE (&cdat->drout_129__REGISTER_LOOP_DEBUG_FUN,
			     meltrout_7_warmelt_debug_REGISTER_LOOP_DEBUG_FUN);

  /*iniclos dclo_130__REGISTER_LOOP_DEBUG_FUN */
 /*_.VALCLO___V180*/ meltfptr[179] =
    (melt_ptr_t) & cdat->dclo_130__REGISTER_LOOP_DEBUG_FUN;
  cdat->dclo_130__REGISTER_LOOP_DEBUG_FUN.discr =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (DISCR_CLOSURE))));
  cdat->dclo_130__REGISTER_LOOP_DEBUG_FUN.nbval = 0;


/*iniobj dsym_131__DISCR_DEBUG_CLOSURE*/
  /*uniqueobj*/ if (! /*_.VALDATA___V181*/ meltfptr[180])
/*_.VALDATA___V181*/ meltfptr[180] =
      (melt_ptr_t) & cdat->dsym_131__DISCR_DEBUG_CLOSURE;
  if (MELT_LIKELY (!melt_prohibit_garbcoll))
    melt_assertmsg ("iniobj check.discr isobj dsym_131__DISCR_DEBUG_CLOSURE",
		    melt_magic_discr ((melt_ptr_t)
				      (((melt_ptr_t)
					(MELT_PREDEF (CLASS_SYMBOL))))) ==
		    MELTOBMAG_OBJECT);
  if (MELT_LIKELY (!melt_prohibit_garbcoll))
    melt_assertmsg
      ("iniobj check.discr objmagic dsym_131__DISCR_DEBUG_CLOSURE",
       ((meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (CLASS_SYMBOL)))))->
       meltobj_magic == MELTOBMAG_OBJECT);
  cdat->dsym_131__DISCR_DEBUG_CLOSURE.meltobj_class =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (CLASS_SYMBOL))));
  cdat->dsym_131__DISCR_DEBUG_CLOSURE.obj_hash = 726425766;
  cdat->dsym_131__DISCR_DEBUG_CLOSURE.obj_len = 3;


/*inistring dstr_132__DISCR_DEBUG_CLOS*/
 /*_.VALSTR___V182*/ meltfptr[181] =
    (melt_ptr_t) & cdat->dstr_132__DISCR_DEBUG_CLOS;
  cdat->dstr_132__DISCR_DEBUG_CLOS.discr =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (DISCR_STRING))));
  /*small inistring */ strncpy (cdat->dstr_132__DISCR_DEBUG_CLOS.val,
				"DISCR_DEBUG_CLOSURE",
				sizeof (cdat->dstr_132__DISCR_DEBUG_CLOS.
					val) - 1);
  cdat->dstr_132__DISCR_DEBUG_CLOS.val[19] = (char) 0;
  cdat->dstr_132__DISCR_DEBUG_CLOS.slen = 19;


/*iniobj dsym_133__CLASS_DISCRIMINANT*/
  /*uniqueobj*/ if (! /*_.VALDATA___V183*/ meltfptr[182])
/*_.VALDATA___V183*/ meltfptr[182] =
      (melt_ptr_t) & cdat->dsym_133__CLASS_DISCRIMINANT;
  if (MELT_LIKELY (!melt_prohibit_garbcoll))
    melt_assertmsg ("iniobj check.discr isobj dsym_133__CLASS_DISCRIMINANT",
		    melt_magic_discr ((melt_ptr_t)
				      (((melt_ptr_t)
					(MELT_PREDEF (CLASS_SYMBOL))))) ==
		    MELTOBMAG_OBJECT);
  if (MELT_LIKELY (!melt_prohibit_garbcoll))
    melt_assertmsg
      ("iniobj check.discr objmagic dsym_133__CLASS_DISCRIMINANT",
       ((meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (CLASS_SYMBOL)))))->
       meltobj_magic == MELTOBMAG_OBJECT);
  cdat->dsym_133__CLASS_DISCRIMINANT.meltobj_class =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (CLASS_SYMBOL))));
  cdat->dsym_133__CLASS_DISCRIMINANT.obj_hash = 535191995;
  cdat->dsym_133__CLASS_DISCRIMINANT.obj_len = 3;


/*inistring dstr_134__CLASS_DISCRIMINA*/
 /*_.VALSTR___V184*/ meltfptr[183] =
    (melt_ptr_t) & cdat->dstr_134__CLASS_DISCRIMINA;
  cdat->dstr_134__CLASS_DISCRIMINA.discr =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (DISCR_STRING))));
  /*small inistring */ strncpy (cdat->dstr_134__CLASS_DISCRIMINA.val,
				"CLASS_DISCRIMINANT",
				sizeof (cdat->dstr_134__CLASS_DISCRIMINA.
					val) - 1);
  cdat->dstr_134__CLASS_DISCRIMINA.val[18] = (char) 0;
  cdat->dstr_134__CLASS_DISCRIMINA.slen = 18;

  MELT_LOCATION ("warmelt-debug.melt:271:/ iniobj");

/*iniobj dobj_135__DISCR_DEBUG_CLOSURE*/
/*_.VALDATA___V185*/ meltfptr[184] =
    (melt_ptr_t) & cdat->dobj_135__DISCR_DEBUG_CLOSURE;
  if (MELT_LIKELY (!melt_prohibit_garbcoll))
    melt_assertmsg ("iniobj check.discr isobj dobj_135__DISCR_DEBUG_CLOSURE",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.CLASS_DISCRIMINANT__V15*/
				       meltfptr[14])) == MELTOBMAG_OBJECT);
  if (MELT_LIKELY (!melt_prohibit_garbcoll))
    melt_assertmsg
      ("iniobj check.discr objmagic dobj_135__DISCR_DEBUG_CLOSURE",
       ((meltobject_ptr_t) ( /*_.CLASS_DISCRIMINANT__V15*/ meltfptr[14]))->
       meltobj_magic == MELTOBMAG_OBJECT);
  cdat->dobj_135__DISCR_DEBUG_CLOSURE.meltobj_class =
    (meltobject_ptr_t) ( /*_.CLASS_DISCRIMINANT__V15*/ meltfptr[14]);
  cdat->dobj_135__DISCR_DEBUG_CLOSURE.obj_num = MELTOBMAG_CLOSURE;
  cdat->dobj_135__DISCR_DEBUG_CLOSURE.obj_hash = 936092797;
  cdat->dobj_135__DISCR_DEBUG_CLOSURE.obj_len = 5;


/*iniobj dsym_136__DISCR_CLOSURE*/
  /*uniqueobj*/ if (! /*_.VALDATA___V187*/ meltfptr[186])
/*_.VALDATA___V187*/ meltfptr[186] =
      (melt_ptr_t) & cdat->dsym_136__DISCR_CLOSURE;
  if (MELT_LIKELY (!melt_prohibit_garbcoll))
    melt_assertmsg ("iniobj check.discr isobj dsym_136__DISCR_CLOSURE",
		    melt_magic_discr ((melt_ptr_t)
				      (((melt_ptr_t)
					(MELT_PREDEF (CLASS_SYMBOL))))) ==
		    MELTOBMAG_OBJECT);
  if (MELT_LIKELY (!melt_prohibit_garbcoll))
    melt_assertmsg ("iniobj check.discr objmagic dsym_136__DISCR_CLOSURE",
		    ((meltobject_ptr_t)
		     (((melt_ptr_t) (MELT_PREDEF (CLASS_SYMBOL)))))->
		    meltobj_magic == MELTOBMAG_OBJECT);
  cdat->dsym_136__DISCR_CLOSURE.meltobj_class =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (CLASS_SYMBOL))));
  cdat->dsym_136__DISCR_CLOSURE.obj_hash = 99248147;
  cdat->dsym_136__DISCR_CLOSURE.obj_len = 3;


/*inistring dstr_137__DISCR_CLOSURE*/
 /*_.VALSTR___V188*/ meltfptr[187] =
    (melt_ptr_t) & cdat->dstr_137__DISCR_CLOSURE;
  cdat->dstr_137__DISCR_CLOSURE.discr =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (DISCR_STRING))));
  /*small inistring */ strncpy (cdat->dstr_137__DISCR_CLOSURE.val,
				"DISCR_CLOSURE",
				sizeof (cdat->dstr_137__DISCR_CLOSURE.val) -
				1);
  cdat->dstr_137__DISCR_CLOSURE.val[13] = (char) 0;
  cdat->dstr_137__DISCR_CLOSURE.slen = 13;


/*inistring dstr_138__DISCR_DEBUG_CLOS*/
 /*_.VALSTR___V186*/ meltfptr[185] =
    (melt_ptr_t) & cdat->dstr_138__DISCR_DEBUG_CLOS;
  cdat->dstr_138__DISCR_DEBUG_CLOS.discr =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (DISCR_STRING))));
  /*small inistring */ strncpy (cdat->dstr_138__DISCR_DEBUG_CLOS.val,
				"DISCR_DEBUG_CLOSURE",
				sizeof (cdat->dstr_138__DISCR_DEBUG_CLOS.
					val) - 1);
  cdat->dstr_138__DISCR_DEBUG_CLOS.val[19] = (char) 0;
  cdat->dstr_138__DISCR_DEBUG_CLOS.slen = 19;

  /*inirout drout_139__MELT_DEBUG_FUN */
 /*_.VALROUT___V189*/ meltfptr[188] =
    (melt_ptr_t) & cdat->drout_139__MELT_DEBUG_FUN;
  cdat->drout_139__MELT_DEBUG_FUN.discr =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (DISCR_ROUTINE))));
  strncpy (cdat->drout_139__MELT_DEBUG_FUN.routdescr,
	   "MELT_DEBUG_FUN @warmelt-debug.melt:280", MELT_ROUTDESCR_LEN - 1);
  cdat->drout_139__MELT_DEBUG_FUN.nbval = 19;
  MELT_ROUTINE_SET_ROUTCODE (&cdat->drout_139__MELT_DEBUG_FUN,
			     meltrout_8_warmelt_debug_MELT_DEBUG_FUN);

  /*iniclos dclo_140__MELT_DEBUG_FUN */
 /*_.VALCLO___V190*/ meltfptr[189] =
    (melt_ptr_t) & cdat->dclo_140__MELT_DEBUG_FUN;
  cdat->dclo_140__MELT_DEBUG_FUN.discr =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (DISCR_CLOSURE))));
  cdat->dclo_140__MELT_DEBUG_FUN.nbval = 0;


/*iniobj dsym_141__ADD2OUT*/
  /*uniqueobj*/ if (! /*_.VALDATA___V191*/ meltfptr[190])
/*_.VALDATA___V191*/ meltfptr[190] =
      (melt_ptr_t) & cdat->dsym_141__ADD2OUT;
  if (MELT_LIKELY (!melt_prohibit_garbcoll))
    melt_assertmsg ("iniobj check.discr isobj dsym_141__ADD2OUT",
		    melt_magic_discr ((melt_ptr_t)
				      (((melt_ptr_t)
					(MELT_PREDEF (CLASS_SYMBOL))))) ==
		    MELTOBMAG_OBJECT);
  if (MELT_LIKELY (!melt_prohibit_garbcoll))
    melt_assertmsg ("iniobj check.discr objmagic dsym_141__ADD2OUT",
		    ((meltobject_ptr_t)
		     (((melt_ptr_t) (MELT_PREDEF (CLASS_SYMBOL)))))->
		    meltobj_magic == MELTOBMAG_OBJECT);
  cdat->dsym_141__ADD2OUT.meltobj_class =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (CLASS_SYMBOL))));
  cdat->dsym_141__ADD2OUT.obj_hash = 901325062;
  cdat->dsym_141__ADD2OUT.obj_len = 3;


/*inistring dstr_142__ADD2OUT*/
 /*_.VALSTR___V192*/ meltfptr[191] = (melt_ptr_t) & cdat->dstr_142__ADD2OUT;
  cdat->dstr_142__ADD2OUT.discr =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (DISCR_STRING))));
  /*small inistring */ strncpy (cdat->dstr_142__ADD2OUT.val, "ADD2OUT",
				sizeof (cdat->dstr_142__ADD2OUT.val) - 1);
  cdat->dstr_142__ADD2OUT.val[7] = (char) 0;
  cdat->dstr_142__ADD2OUT.slen = 7;


/*iniobj dsym_143__CSTRING_PREFIXED*/
  /*uniqueobj*/ if (! /*_.VALDATA___V193*/ meltfptr[192])
/*_.VALDATA___V193*/ meltfptr[192] =
      (melt_ptr_t) & cdat->dsym_143__CSTRING_PREFIXED;
  if (MELT_LIKELY (!melt_prohibit_garbcoll))
    melt_assertmsg ("iniobj check.discr isobj dsym_143__CSTRING_PREFIXED",
		    melt_magic_discr ((melt_ptr_t)
				      (((melt_ptr_t)
					(MELT_PREDEF (CLASS_SYMBOL))))) ==
		    MELTOBMAG_OBJECT);
  if (MELT_LIKELY (!melt_prohibit_garbcoll))
    melt_assertmsg ("iniobj check.discr objmagic dsym_143__CSTRING_PREFIXED",
		    ((meltobject_ptr_t)
		     (((melt_ptr_t) (MELT_PREDEF (CLASS_SYMBOL)))))->
		    meltobj_magic == MELTOBMAG_OBJECT);
  cdat->dsym_143__CSTRING_PREFIXED.meltobj_class =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (CLASS_SYMBOL))));
  cdat->dsym_143__CSTRING_PREFIXED.obj_hash = 976907042;
  cdat->dsym_143__CSTRING_PREFIXED.obj_len = 3;


/*inistring dstr_144__CSTRING_PREFIXED*/
 /*_.VALSTR___V194*/ meltfptr[193] =
    (melt_ptr_t) & cdat->dstr_144__CSTRING_PREFIXED;
  cdat->dstr_144__CSTRING_PREFIXED.discr =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (DISCR_STRING))));
  /*small inistring */ strncpy (cdat->dstr_144__CSTRING_PREFIXED.val,
				"CSTRING_PREFIXED",
				sizeof (cdat->dstr_144__CSTRING_PREFIXED.
					val) - 1);
  cdat->dstr_144__CSTRING_PREFIXED.val[16] = (char) 0;
  cdat->dstr_144__CSTRING_PREFIXED.slen = 16;


/*iniobj dsym_145__CLASS_NAMED*/
  /*uniqueobj*/ if (! /*_.VALDATA___V195*/ meltfptr[194])
/*_.VALDATA___V195*/ meltfptr[194] =
      (melt_ptr_t) & cdat->dsym_145__CLASS_NAMED;
  if (MELT_LIKELY (!melt_prohibit_garbcoll))
    melt_assertmsg ("iniobj check.discr isobj dsym_145__CLASS_NAMED",
		    melt_magic_discr ((melt_ptr_t)
				      (((melt_ptr_t)
					(MELT_PREDEF (CLASS_SYMBOL))))) ==
		    MELTOBMAG_OBJECT);
  if (MELT_LIKELY (!melt_prohibit_garbcoll))
    melt_assertmsg ("iniobj check.discr objmagic dsym_145__CLASS_NAMED",
		    ((meltobject_ptr_t)
		     (((melt_ptr_t) (MELT_PREDEF (CLASS_SYMBOL)))))->
		    meltobj_magic == MELTOBMAG_OBJECT);
  cdat->dsym_145__CLASS_NAMED.meltobj_class =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (CLASS_SYMBOL))));
  cdat->dsym_145__CLASS_NAMED.obj_hash = 60407004;
  cdat->dsym_145__CLASS_NAMED.obj_len = 3;


/*inistring dstr_146__CLASS_NAMED*/
 /*_.VALSTR___V196*/ meltfptr[195] =
    (melt_ptr_t) & cdat->dstr_146__CLASS_NAMED;
  cdat->dstr_146__CLASS_NAMED.discr =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (DISCR_STRING))));
  /*small inistring */ strncpy (cdat->dstr_146__CLASS_NAMED.val,
				"CLASS_NAMED",
				sizeof (cdat->dstr_146__CLASS_NAMED.val) - 1);
  cdat->dstr_146__CLASS_NAMED.val[11] = (char) 0;
  cdat->dstr_146__CLASS_NAMED.slen = 11;


/*iniobj dsym_147__CLASS_CTYPE*/
  /*uniqueobj*/ if (! /*_.VALDATA___V197*/ meltfptr[196])
/*_.VALDATA___V197*/ meltfptr[196] =
      (melt_ptr_t) & cdat->dsym_147__CLASS_CTYPE;
  if (MELT_LIKELY (!melt_prohibit_garbcoll))
    melt_assertmsg ("iniobj check.discr isobj dsym_147__CLASS_CTYPE",
		    melt_magic_discr ((melt_ptr_t)
				      (((melt_ptr_t)
					(MELT_PREDEF (CLASS_SYMBOL))))) ==
		    MELTOBMAG_OBJECT);
  if (MELT_LIKELY (!melt_prohibit_garbcoll))
    melt_assertmsg ("iniobj check.discr objmagic dsym_147__CLASS_CTYPE",
		    ((meltobject_ptr_t)
		     (((melt_ptr_t) (MELT_PREDEF (CLASS_SYMBOL)))))->
		    meltobj_magic == MELTOBMAG_OBJECT);
  cdat->dsym_147__CLASS_CTYPE.meltobj_class =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (CLASS_SYMBOL))));
  cdat->dsym_147__CLASS_CTYPE.obj_hash = 78610803;
  cdat->dsym_147__CLASS_CTYPE.obj_len = 3;


/*inistring dstr_148__CLASS_CTYPE*/
 /*_.VALSTR___V198*/ meltfptr[197] =
    (melt_ptr_t) & cdat->dstr_148__CLASS_CTYPE;
  cdat->dstr_148__CLASS_CTYPE.discr =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (DISCR_STRING))));
  /*small inistring */ strncpy (cdat->dstr_148__CLASS_CTYPE.val,
				"CLASS_CTYPE",
				sizeof (cdat->dstr_148__CLASS_CTYPE.val) - 1);
  cdat->dstr_148__CLASS_CTYPE.val[11] = (char) 0;
  cdat->dstr_148__CLASS_CTYPE.slen = 11;

  /*inirout drout_149__DBG_OUTOBJECT */
 /*_.VALROUT___V115*/ meltfptr[114] =
    (melt_ptr_t) & cdat->drout_149__DBG_OUTOBJECT;
  cdat->drout_149__DBG_OUTOBJECT.discr =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (DISCR_ROUTINE))));
  strncpy (cdat->drout_149__DBG_OUTOBJECT.routdescr,
	   "DBG_OUTOBJECT @warmelt-debug.melt:454", MELT_ROUTDESCR_LEN - 1);
  cdat->drout_149__DBG_OUTOBJECT.nbval = 4;
  MELT_ROUTINE_SET_ROUTCODE (&cdat->drout_149__DBG_OUTOBJECT,
			     meltrout_9_warmelt_debug_DBG_OUTOBJECT);

  /*iniclos dclo_150__DBG_OUTOBJECT */
 /*_.VALCLO___V114*/ meltfptr[113] =
    (melt_ptr_t) & cdat->dclo_150__DBG_OUTOBJECT;
  cdat->dclo_150__DBG_OUTOBJECT.discr =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (DISCR_CLOSURE))));
  cdat->dclo_150__DBG_OUTOBJECT.nbval = 0;

  /*inirout drout_151__DBG_OUT */
 /*_.VALROUT___V113*/ meltfptr[112] =
    (melt_ptr_t) & cdat->drout_151__DBG_OUT;
  cdat->drout_151__DBG_OUT.discr =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (DISCR_ROUTINE))));
  strncpy (cdat->drout_151__DBG_OUT.routdescr,
	   "DBG_OUT @warmelt-debug.melt:474", MELT_ROUTDESCR_LEN - 1);
  cdat->drout_151__DBG_OUT.nbval = 4;
  MELT_ROUTINE_SET_ROUTCODE (&cdat->drout_151__DBG_OUT,
			     meltrout_10_warmelt_debug_DBG_OUT);

  /*iniclos dclo_152__DBG_OUT */
 /*_.VALCLO___V112*/ meltfptr[111] = (melt_ptr_t) & cdat->dclo_152__DBG_OUT;
  cdat->dclo_152__DBG_OUT.discr =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (DISCR_CLOSURE))));
  cdat->dclo_152__DBG_OUT.nbval = 0;

  /*inirout drout_153__DBGOUT_FIELDS */
 /*_.VALROUT___V199*/ meltfptr[198] =
    (melt_ptr_t) & cdat->drout_153__DBGOUT_FIELDS;
  cdat->drout_153__DBGOUT_FIELDS.discr =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (DISCR_ROUTINE))));
  strncpy (cdat->drout_153__DBGOUT_FIELDS.routdescr,
	   "DBGOUT_FIELDS @warmelt-debug.melt:502", MELT_ROUTDESCR_LEN - 1);
  cdat->drout_153__DBGOUT_FIELDS.nbval = 3;
  MELT_ROUTINE_SET_ROUTCODE (&cdat->drout_153__DBGOUT_FIELDS,
			     meltrout_11_warmelt_debug_DBGOUT_FIELDS);

  /*iniclos dclo_154__DBGOUT_FIELDS */
 /*_.VALCLO___V200*/ meltfptr[199] =
    (melt_ptr_t) & cdat->dclo_154__DBGOUT_FIELDS;
  cdat->dclo_154__DBGOUT_FIELDS.discr =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (DISCR_CLOSURE))));
  cdat->dclo_154__DBGOUT_FIELDS.nbval = 0;

  /*inirout drout_155__DBGOUTAGAIN_FIELDS */
 /*_.VALROUT___V201*/ meltfptr[200] =
    (melt_ptr_t) & cdat->drout_155__DBGOUTAGAIN_FIELDS;
  cdat->drout_155__DBGOUTAGAIN_FIELDS.discr =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (DISCR_ROUTINE))));
  strncpy (cdat->drout_155__DBGOUTAGAIN_FIELDS.routdescr,
	   "DBGOUTAGAIN_FIELDS @warmelt-debug.melt:557",
	   MELT_ROUTDESCR_LEN - 1);
  cdat->drout_155__DBGOUTAGAIN_FIELDS.nbval = 3;
  MELT_ROUTINE_SET_ROUTCODE (&cdat->drout_155__DBGOUTAGAIN_FIELDS,
			     meltrout_12_warmelt_debug_DBGOUTAGAIN_FIELDS);

  /*iniclos dclo_156__DBGOUTAGAIN_FIELDS */
 /*_.VALCLO___V202*/ meltfptr[201] =
    (melt_ptr_t) & cdat->dclo_156__DBGOUTAGAIN_FIELDS;
  cdat->dclo_156__DBGOUTAGAIN_FIELDS.discr =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (DISCR_CLOSURE))));
  cdat->dclo_156__DBGOUTAGAIN_FIELDS.nbval = 0;

  /*inirout drout_157__DBGOUT_NULL_METHOD */
 /*_.VALROUT___V203*/ meltfptr[202] =
    (melt_ptr_t) & cdat->drout_157__DBGOUT_NULL_METHOD;
  cdat->drout_157__DBGOUT_NULL_METHOD.discr =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (DISCR_ROUTINE))));
  strncpy (cdat->drout_157__DBGOUT_NULL_METHOD.routdescr,
	   "DBGOUT_NULL_METHOD @warmelt-debug.melt:601",
	   MELT_ROUTDESCR_LEN - 1);
  cdat->drout_157__DBGOUT_NULL_METHOD.nbval = 0;
  MELT_ROUTINE_SET_ROUTCODE (&cdat->drout_157__DBGOUT_NULL_METHOD,
			     meltrout_13_warmelt_debug_DBGOUT_NULL_METHOD);

  /*iniclos dclo_158__DBGOUT_NULL_METHOD */
 /*_.VALCLO___V204*/ meltfptr[203] =
    (melt_ptr_t) & cdat->dclo_158__DBGOUT_NULL_METHOD;
  cdat->dclo_158__DBGOUT_NULL_METHOD.discr =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (DISCR_CLOSURE))));
  cdat->dclo_158__DBGOUT_NULL_METHOD.nbval = 0;


/*iniobj dsym_159__INSTALL_METHOD*/
  /*uniqueobj*/ if (! /*_.VALDATA___V205*/ meltfptr[204])
/*_.VALDATA___V205*/ meltfptr[204] =
      (melt_ptr_t) & cdat->dsym_159__INSTALL_METHOD;
  if (MELT_LIKELY (!melt_prohibit_garbcoll))
    melt_assertmsg ("iniobj check.discr isobj dsym_159__INSTALL_METHOD",
		    melt_magic_discr ((melt_ptr_t)
				      (((melt_ptr_t)
					(MELT_PREDEF (CLASS_SYMBOL))))) ==
		    MELTOBMAG_OBJECT);
  if (MELT_LIKELY (!melt_prohibit_garbcoll))
    melt_assertmsg ("iniobj check.discr objmagic dsym_159__INSTALL_METHOD",
		    ((meltobject_ptr_t)
		     (((melt_ptr_t) (MELT_PREDEF (CLASS_SYMBOL)))))->
		    meltobj_magic == MELTOBMAG_OBJECT);
  cdat->dsym_159__INSTALL_METHOD.meltobj_class =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (CLASS_SYMBOL))));
  cdat->dsym_159__INSTALL_METHOD.obj_hash = 680699224;
  cdat->dsym_159__INSTALL_METHOD.obj_len = 3;


/*inistring dstr_160__INSTALL_METHOD*/
 /*_.VALSTR___V206*/ meltfptr[205] =
    (melt_ptr_t) & cdat->dstr_160__INSTALL_METHOD;
  cdat->dstr_160__INSTALL_METHOD.discr =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (DISCR_STRING))));
  /*small inistring */ strncpy (cdat->dstr_160__INSTALL_METHOD.val,
				"INSTALL_METHOD",
				sizeof (cdat->dstr_160__INSTALL_METHOD.val) -
				1);
  cdat->dstr_160__INSTALL_METHOD.val[14] = (char) 0;
  cdat->dstr_160__INSTALL_METHOD.slen = 14;


/*iniobj dsym_161__DISCR_NULL_RECEIVER*/
  /*uniqueobj*/ if (! /*_.VALDATA___V207*/ meltfptr[206])
/*_.VALDATA___V207*/ meltfptr[206] =
      (melt_ptr_t) & cdat->dsym_161__DISCR_NULL_RECEIVER;
  if (MELT_LIKELY (!melt_prohibit_garbcoll))
    melt_assertmsg ("iniobj check.discr isobj dsym_161__DISCR_NULL_RECEIVER",
		    melt_magic_discr ((melt_ptr_t)
				      (((melt_ptr_t)
					(MELT_PREDEF (CLASS_SYMBOL))))) ==
		    MELTOBMAG_OBJECT);
  if (MELT_LIKELY (!melt_prohibit_garbcoll))
    melt_assertmsg
      ("iniobj check.discr objmagic dsym_161__DISCR_NULL_RECEIVER",
       ((meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (CLASS_SYMBOL)))))->
       meltobj_magic == MELTOBMAG_OBJECT);
  cdat->dsym_161__DISCR_NULL_RECEIVER.meltobj_class =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (CLASS_SYMBOL))));
  cdat->dsym_161__DISCR_NULL_RECEIVER.obj_hash = 1063609307;
  cdat->dsym_161__DISCR_NULL_RECEIVER.obj_len = 3;


/*inistring dstr_162__DISCR_NULL_RECEI*/
 /*_.VALSTR___V208*/ meltfptr[207] =
    (melt_ptr_t) & cdat->dstr_162__DISCR_NULL_RECEI;
  cdat->dstr_162__DISCR_NULL_RECEI.discr =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (DISCR_STRING))));
  /*small inistring */ strncpy (cdat->dstr_162__DISCR_NULL_RECEI.val,
				"DISCR_NULL_RECEIVER",
				sizeof (cdat->dstr_162__DISCR_NULL_RECEI.
					val) - 1);
  cdat->dstr_162__DISCR_NULL_RECEI.val[19] = (char) 0;
  cdat->dstr_162__DISCR_NULL_RECEI.slen = 19;

  /*inirout drout_163__DBGOUT_STRING_METHOD */
 /*_.VALROUT___V209*/ meltfptr[208] =
    (melt_ptr_t) & cdat->drout_163__DBGOUT_STRING_METHOD;
  cdat->drout_163__DBGOUT_STRING_METHOD.discr =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (DISCR_ROUTINE))));
  strncpy (cdat->drout_163__DBGOUT_STRING_METHOD.routdescr,
	   "DBGOUT_STRING_METHOD @warmelt-debug.melt:609",
	   MELT_ROUTDESCR_LEN - 1);
  cdat->drout_163__DBGOUT_STRING_METHOD.nbval = 2;
  MELT_ROUTINE_SET_ROUTCODE (&cdat->drout_163__DBGOUT_STRING_METHOD,
			     meltrout_14_warmelt_debug_DBGOUT_STRING_METHOD);

  /*iniclos dclo_164__DBGOUT_STRING_METHOD */
 /*_.VALCLO___V210*/ meltfptr[209] =
    (melt_ptr_t) & cdat->dclo_164__DBGOUT_STRING_METHOD;
  cdat->dclo_164__DBGOUT_STRING_METHOD.discr =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (DISCR_CLOSURE))));
  cdat->dclo_164__DBGOUT_STRING_METHOD.nbval = 0;


/*iniobj dsym_165__DISCR_STRING*/
  /*uniqueobj*/ if (! /*_.VALDATA___V211*/ meltfptr[210])
/*_.VALDATA___V211*/ meltfptr[210] =
      (melt_ptr_t) & cdat->dsym_165__DISCR_STRING;
  if (MELT_LIKELY (!melt_prohibit_garbcoll))
    melt_assertmsg ("iniobj check.discr isobj dsym_165__DISCR_STRING",
		    melt_magic_discr ((melt_ptr_t)
				      (((melt_ptr_t)
					(MELT_PREDEF (CLASS_SYMBOL))))) ==
		    MELTOBMAG_OBJECT);
  if (MELT_LIKELY (!melt_prohibit_garbcoll))
    melt_assertmsg ("iniobj check.discr objmagic dsym_165__DISCR_STRING",
		    ((meltobject_ptr_t)
		     (((melt_ptr_t) (MELT_PREDEF (CLASS_SYMBOL)))))->
		    meltobj_magic == MELTOBMAG_OBJECT);
  cdat->dsym_165__DISCR_STRING.meltobj_class =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (CLASS_SYMBOL))));
  cdat->dsym_165__DISCR_STRING.obj_hash = 100436219;
  cdat->dsym_165__DISCR_STRING.obj_len = 3;


/*inistring dstr_166__DISCR_STRING*/
 /*_.VALSTR___V212*/ meltfptr[211] =
    (melt_ptr_t) & cdat->dstr_166__DISCR_STRING;
  cdat->dstr_166__DISCR_STRING.discr =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (DISCR_STRING))));
  /*small inistring */ strncpy (cdat->dstr_166__DISCR_STRING.val,
				"DISCR_STRING",
				sizeof (cdat->dstr_166__DISCR_STRING.val) -
				1);
  cdat->dstr_166__DISCR_STRING.val[12] = (char) 0;
  cdat->dstr_166__DISCR_STRING.slen = 12;

  /*inirout drout_167__DBGOUT_STRBUF_METHOD */
 /*_.VALROUT___V213*/ meltfptr[212] =
    (melt_ptr_t) & cdat->drout_167__DBGOUT_STRBUF_METHOD;
  cdat->drout_167__DBGOUT_STRBUF_METHOD.discr =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (DISCR_ROUTINE))));
  strncpy (cdat->drout_167__DBGOUT_STRBUF_METHOD.routdescr,
	   "DBGOUT_STRBUF_METHOD @warmelt-debug.melt:631",
	   MELT_ROUTDESCR_LEN - 1);
  cdat->drout_167__DBGOUT_STRBUF_METHOD.nbval = 3;
  MELT_ROUTINE_SET_ROUTCODE (&cdat->drout_167__DBGOUT_STRBUF_METHOD,
			     meltrout_15_warmelt_debug_DBGOUT_STRBUF_METHOD);

  /*iniclos dclo_168__DBGOUT_STRBUF_METHOD */
 /*_.VALCLO___V214*/ meltfptr[213] =
    (melt_ptr_t) & cdat->dclo_168__DBGOUT_STRBUF_METHOD;
  cdat->dclo_168__DBGOUT_STRBUF_METHOD.discr =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (DISCR_CLOSURE))));
  cdat->dclo_168__DBGOUT_STRBUF_METHOD.nbval = 0;

  /*inirout drout_169__DBGOUT_INTEGER_METHOD */
 /*_.VALROUT___V215*/ meltfptr[214] =
    (melt_ptr_t) & cdat->drout_169__DBGOUT_INTEGER_METHOD;
  cdat->drout_169__DBGOUT_INTEGER_METHOD.discr =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (DISCR_ROUTINE))));
  strncpy (cdat->drout_169__DBGOUT_INTEGER_METHOD.routdescr,
	   "DBGOUT_INTEGER_METHOD @warmelt-debug.melt:717",
	   MELT_ROUTDESCR_LEN - 1);
  cdat->drout_169__DBGOUT_INTEGER_METHOD.nbval = 4;
  MELT_ROUTINE_SET_ROUTCODE (&cdat->drout_169__DBGOUT_INTEGER_METHOD,
			     meltrout_16_warmelt_debug_DBGOUT_INTEGER_METHOD);

  /*iniclos dclo_170__DBGOUT_INTEGER_METHOD */
 /*_.VALCLO___V216*/ meltfptr[215] =
    (melt_ptr_t) & cdat->dclo_170__DBGOUT_INTEGER_METHOD;
  cdat->dclo_170__DBGOUT_INTEGER_METHOD.discr =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (DISCR_CLOSURE))));
  cdat->dclo_170__DBGOUT_INTEGER_METHOD.nbval = 0;


/*iniobj dsym_171__DISCR_CONSTANT_INTEGER*/
  /*uniqueobj*/ if (! /*_.VALDATA___V217*/ meltfptr[216])
/*_.VALDATA___V217*/ meltfptr[216] =
      (melt_ptr_t) & cdat->dsym_171__DISCR_CONSTANT_INTEGER;
  if (MELT_LIKELY (!melt_prohibit_garbcoll))
    melt_assertmsg
      ("iniobj check.discr isobj dsym_171__DISCR_CONSTANT_INTEGER",
       melt_magic_discr ((melt_ptr_t)
			 (((melt_ptr_t) (MELT_PREDEF (CLASS_SYMBOL))))) ==
       MELTOBMAG_OBJECT);
  if (MELT_LIKELY (!melt_prohibit_garbcoll))
    melt_assertmsg
      ("iniobj check.discr objmagic dsym_171__DISCR_CONSTANT_INTEGER",
       ((meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (CLASS_SYMBOL)))))->
       meltobj_magic == MELTOBMAG_OBJECT);
  cdat->dsym_171__DISCR_CONSTANT_INTEGER.meltobj_class =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (CLASS_SYMBOL))));
  cdat->dsym_171__DISCR_CONSTANT_INTEGER.obj_hash = 1007078494;
  cdat->dsym_171__DISCR_CONSTANT_INTEGER.obj_len = 3;


/*inistring dstr_172__DISCR_CONSTANT_I*/
 /*_.VALSTR___V218*/ meltfptr[217] =
    (melt_ptr_t) & cdat->dstr_172__DISCR_CONSTANT_I;
  cdat->dstr_172__DISCR_CONSTANT_I.discr =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (DISCR_STRING))));
  /*small inistring */ strncpy (cdat->dstr_172__DISCR_CONSTANT_I.val,
				"DISCR_CONSTANT_INTEGER",
				sizeof (cdat->dstr_172__DISCR_CONSTANT_I.
					val) - 1);
  cdat->dstr_172__DISCR_CONSTANT_I.val[22] = (char) 0;
  cdat->dstr_172__DISCR_CONSTANT_I.slen = 22;

  /*inirout drout_173__DBGOUT_MIXINT_METHOD */
 /*_.VALROUT___V219*/ meltfptr[218] =
    (melt_ptr_t) & cdat->drout_173__DBGOUT_MIXINT_METHOD;
  cdat->drout_173__DBGOUT_MIXINT_METHOD.discr =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (DISCR_ROUTINE))));
  strncpy (cdat->drout_173__DBGOUT_MIXINT_METHOD.routdescr,
	   "DBGOUT_MIXINT_METHOD @warmelt-debug.melt:738",
	   MELT_ROUTDESCR_LEN - 1);
  cdat->drout_173__DBGOUT_MIXINT_METHOD.nbval = 4;
  MELT_ROUTINE_SET_ROUTCODE (&cdat->drout_173__DBGOUT_MIXINT_METHOD,
			     meltrout_17_warmelt_debug_DBGOUT_MIXINT_METHOD);

  /*iniclos dclo_174__DBGOUT_MIXINT_METHOD */
 /*_.VALCLO___V220*/ meltfptr[219] =
    (melt_ptr_t) & cdat->dclo_174__DBGOUT_MIXINT_METHOD;
  cdat->dclo_174__DBGOUT_MIXINT_METHOD.discr =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (DISCR_CLOSURE))));
  cdat->dclo_174__DBGOUT_MIXINT_METHOD.nbval = 0;


/*iniobj dsym_175__DISCR_MIXED_INTEGER*/
  /*uniqueobj*/ if (! /*_.VALDATA___V221*/ meltfptr[220])
/*_.VALDATA___V221*/ meltfptr[220] =
      (melt_ptr_t) & cdat->dsym_175__DISCR_MIXED_INTEGER;
  if (MELT_LIKELY (!melt_prohibit_garbcoll))
    melt_assertmsg ("iniobj check.discr isobj dsym_175__DISCR_MIXED_INTEGER",
		    melt_magic_discr ((melt_ptr_t)
				      (((melt_ptr_t)
					(MELT_PREDEF (CLASS_SYMBOL))))) ==
		    MELTOBMAG_OBJECT);
  if (MELT_LIKELY (!melt_prohibit_garbcoll))
    melt_assertmsg
      ("iniobj check.discr objmagic dsym_175__DISCR_MIXED_INTEGER",
       ((meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (CLASS_SYMBOL)))))->
       meltobj_magic == MELTOBMAG_OBJECT);
  cdat->dsym_175__DISCR_MIXED_INTEGER.meltobj_class =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (CLASS_SYMBOL))));
  cdat->dsym_175__DISCR_MIXED_INTEGER.obj_hash = 355248820;
  cdat->dsym_175__DISCR_MIXED_INTEGER.obj_len = 3;


/*inistring dstr_176__DISCR_MIXED_INTE*/
 /*_.VALSTR___V222*/ meltfptr[221] =
    (melt_ptr_t) & cdat->dstr_176__DISCR_MIXED_INTE;
  cdat->dstr_176__DISCR_MIXED_INTE.discr =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (DISCR_STRING))));
  /*small inistring */ strncpy (cdat->dstr_176__DISCR_MIXED_INTE.val,
				"DISCR_MIXED_INTEGER",
				sizeof (cdat->dstr_176__DISCR_MIXED_INTE.
					val) - 1);
  cdat->dstr_176__DISCR_MIXED_INTE.val[19] = (char) 0;
  cdat->dstr_176__DISCR_MIXED_INTE.slen = 19;

  /*inirout drout_177__DBGOUT_MIXLOC_METHOD */
 /*_.VALROUT___V223*/ meltfptr[222] =
    (melt_ptr_t) & cdat->drout_177__DBGOUT_MIXLOC_METHOD;
  cdat->drout_177__DBGOUT_MIXLOC_METHOD.discr =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (DISCR_ROUTINE))));
  strncpy (cdat->drout_177__DBGOUT_MIXLOC_METHOD.routdescr,
	   "DBGOUT_MIXLOC_METHOD @warmelt-debug.melt:768",
	   MELT_ROUTDESCR_LEN - 1);
  cdat->drout_177__DBGOUT_MIXLOC_METHOD.nbval = 3;
  MELT_ROUTINE_SET_ROUTCODE (&cdat->drout_177__DBGOUT_MIXLOC_METHOD,
			     meltrout_18_warmelt_debug_DBGOUT_MIXLOC_METHOD);

  /*iniclos dclo_178__DBGOUT_MIXLOC_METHOD */
 /*_.VALCLO___V224*/ meltfptr[223] =
    (melt_ptr_t) & cdat->dclo_178__DBGOUT_MIXLOC_METHOD;
  cdat->dclo_178__DBGOUT_MIXLOC_METHOD.discr =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (DISCR_CLOSURE))));
  cdat->dclo_178__DBGOUT_MIXLOC_METHOD.nbval = 0;


/*iniobj dsym_179__DISCR_MIXED_LOCATION*/
  /*uniqueobj*/ if (! /*_.VALDATA___V225*/ meltfptr[224])
/*_.VALDATA___V225*/ meltfptr[224] =
      (melt_ptr_t) & cdat->dsym_179__DISCR_MIXED_LOCATION;
  if (MELT_LIKELY (!melt_prohibit_garbcoll))
    melt_assertmsg ("iniobj check.discr isobj dsym_179__DISCR_MIXED_LOCATION",
		    melt_magic_discr ((melt_ptr_t)
				      (((melt_ptr_t)
					(MELT_PREDEF (CLASS_SYMBOL))))) ==
		    MELTOBMAG_OBJECT);
  if (MELT_LIKELY (!melt_prohibit_garbcoll))
    melt_assertmsg
      ("iniobj check.discr objmagic dsym_179__DISCR_MIXED_LOCATION",
       ((meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (CLASS_SYMBOL)))))->
       meltobj_magic == MELTOBMAG_OBJECT);
  cdat->dsym_179__DISCR_MIXED_LOCATION.meltobj_class =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (CLASS_SYMBOL))));
  cdat->dsym_179__DISCR_MIXED_LOCATION.obj_hash = 1019373807;
  cdat->dsym_179__DISCR_MIXED_LOCATION.obj_len = 3;


/*inistring dstr_180__DISCR_MIXED_LOCA*/
 /*_.VALSTR___V226*/ meltfptr[225] =
    (melt_ptr_t) & cdat->dstr_180__DISCR_MIXED_LOCA;
  cdat->dstr_180__DISCR_MIXED_LOCA.discr =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (DISCR_STRING))));
  /*small inistring */ strncpy (cdat->dstr_180__DISCR_MIXED_LOCA.val,
				"DISCR_MIXED_LOCATION",
				sizeof (cdat->dstr_180__DISCR_MIXED_LOCA.
					val) - 1);
  cdat->dstr_180__DISCR_MIXED_LOCA.val[20] = (char) 0;
  cdat->dstr_180__DISCR_MIXED_LOCA.slen = 20;

  /*inirout drout_181__DBGOUT_MIXBIGINT_METHOD */
 /*_.VALROUT___V227*/ meltfptr[226] =
    (melt_ptr_t) & cdat->drout_181__DBGOUT_MIXBIGINT_METHOD;
  cdat->drout_181__DBGOUT_MIXBIGINT_METHOD.discr =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (DISCR_ROUTINE))));
  strncpy (cdat->drout_181__DBGOUT_MIXBIGINT_METHOD.routdescr,
	   "DBGOUT_MIXBIGINT_METHOD @warmelt-debug.melt:800",
	   MELT_ROUTDESCR_LEN - 1);
  cdat->drout_181__DBGOUT_MIXBIGINT_METHOD.nbval = 3;
  MELT_ROUTINE_SET_ROUTCODE (&cdat->drout_181__DBGOUT_MIXBIGINT_METHOD,
			     meltrout_19_warmelt_debug_DBGOUT_MIXBIGINT_METHOD);

  /*iniclos dclo_182__DBGOUT_MIXBIGINT_METHOD */
 /*_.VALCLO___V228*/ meltfptr[227] =
    (melt_ptr_t) & cdat->dclo_182__DBGOUT_MIXBIGINT_METHOD;
  cdat->dclo_182__DBGOUT_MIXBIGINT_METHOD.discr =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (DISCR_CLOSURE))));
  cdat->dclo_182__DBGOUT_MIXBIGINT_METHOD.nbval = 0;


/*iniobj dsym_183__DISCR_MIXED_BIGINT*/
  /*uniqueobj*/ if (! /*_.VALDATA___V229*/ meltfptr[228])
/*_.VALDATA___V229*/ meltfptr[228] =
      (melt_ptr_t) & cdat->dsym_183__DISCR_MIXED_BIGINT;
  if (MELT_LIKELY (!melt_prohibit_garbcoll))
    melt_assertmsg ("iniobj check.discr isobj dsym_183__DISCR_MIXED_BIGINT",
		    melt_magic_discr ((melt_ptr_t)
				      (((melt_ptr_t)
					(MELT_PREDEF (CLASS_SYMBOL))))) ==
		    MELTOBMAG_OBJECT);
  if (MELT_LIKELY (!melt_prohibit_garbcoll))
    melt_assertmsg
      ("iniobj check.discr objmagic dsym_183__DISCR_MIXED_BIGINT",
       ((meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (CLASS_SYMBOL)))))->
       meltobj_magic == MELTOBMAG_OBJECT);
  cdat->dsym_183__DISCR_MIXED_BIGINT.meltobj_class =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (CLASS_SYMBOL))));
  cdat->dsym_183__DISCR_MIXED_BIGINT.obj_hash = 484145131;
  cdat->dsym_183__DISCR_MIXED_BIGINT.obj_len = 3;


/*inistring dstr_184__DISCR_MIXED_BIGI*/
 /*_.VALSTR___V230*/ meltfptr[229] =
    (melt_ptr_t) & cdat->dstr_184__DISCR_MIXED_BIGI;
  cdat->dstr_184__DISCR_MIXED_BIGI.discr =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (DISCR_STRING))));
  /*small inistring */ strncpy (cdat->dstr_184__DISCR_MIXED_BIGI.val,
				"DISCR_MIXED_BIGINT",
				sizeof (cdat->dstr_184__DISCR_MIXED_BIGI.
					val) - 1);
  cdat->dstr_184__DISCR_MIXED_BIGI.val[18] = (char) 0;
  cdat->dstr_184__DISCR_MIXED_BIGI.slen = 18;

  /*inirout drout_185__DBGOUT_MULTIPLE_METHOD */
 /*_.VALROUT___V231*/ meltfptr[230] =
    (melt_ptr_t) & cdat->drout_185__DBGOUT_MULTIPLE_METHOD;
  cdat->drout_185__DBGOUT_MULTIPLE_METHOD.discr =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (DISCR_ROUTINE))));
  strncpy (cdat->drout_185__DBGOUT_MULTIPLE_METHOD.routdescr,
	   "DBGOUT_MULTIPLE_METHOD @warmelt-debug.melt:825",
	   MELT_ROUTDESCR_LEN - 1);
  cdat->drout_185__DBGOUT_MULTIPLE_METHOD.nbval = 3;
  MELT_ROUTINE_SET_ROUTCODE (&cdat->drout_185__DBGOUT_MULTIPLE_METHOD,
			     meltrout_20_warmelt_debug_DBGOUT_MULTIPLE_METHOD);

  /*iniclos dclo_186__DBGOUT_MULTIPLE_METHOD */
 /*_.VALCLO___V232*/ meltfptr[231] =
    (melt_ptr_t) & cdat->dclo_186__DBGOUT_MULTIPLE_METHOD;
  cdat->dclo_186__DBGOUT_MULTIPLE_METHOD.discr =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (DISCR_CLOSURE))));
  cdat->dclo_186__DBGOUT_MULTIPLE_METHOD.nbval = 0;


/*iniobj dsym_187__DISCR_MULTIPLE*/
  /*uniqueobj*/ if (! /*_.VALDATA___V233*/ meltfptr[232])
/*_.VALDATA___V233*/ meltfptr[232] =
      (melt_ptr_t) & cdat->dsym_187__DISCR_MULTIPLE;
  if (MELT_LIKELY (!melt_prohibit_garbcoll))
    melt_assertmsg ("iniobj check.discr isobj dsym_187__DISCR_MULTIPLE",
		    melt_magic_discr ((melt_ptr_t)
				      (((melt_ptr_t)
					(MELT_PREDEF (CLASS_SYMBOL))))) ==
		    MELTOBMAG_OBJECT);
  if (MELT_LIKELY (!melt_prohibit_garbcoll))
    melt_assertmsg ("iniobj check.discr objmagic dsym_187__DISCR_MULTIPLE",
		    ((meltobject_ptr_t)
		     (((melt_ptr_t) (MELT_PREDEF (CLASS_SYMBOL)))))->
		    meltobj_magic == MELTOBMAG_OBJECT);
  cdat->dsym_187__DISCR_MULTIPLE.meltobj_class =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (CLASS_SYMBOL))));
  cdat->dsym_187__DISCR_MULTIPLE.obj_hash = 91864583;
  cdat->dsym_187__DISCR_MULTIPLE.obj_len = 3;


/*inistring dstr_188__DISCR_MULTIPLE*/
 /*_.VALSTR___V234*/ meltfptr[233] =
    (melt_ptr_t) & cdat->dstr_188__DISCR_MULTIPLE;
  cdat->dstr_188__DISCR_MULTIPLE.discr =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (DISCR_STRING))));
  /*small inistring */ strncpy (cdat->dstr_188__DISCR_MULTIPLE.val,
				"DISCR_MULTIPLE",
				sizeof (cdat->dstr_188__DISCR_MULTIPLE.val) -
				1);
  cdat->dstr_188__DISCR_MULTIPLE.val[14] = (char) 0;
  cdat->dstr_188__DISCR_MULTIPLE.slen = 14;

  /*inirout drout_189__DBGOUT_ROUTINE_METHOD */
 /*_.VALROUT___V235*/ meltfptr[234] =
    (melt_ptr_t) & cdat->drout_189__DBGOUT_ROUTINE_METHOD;
  cdat->drout_189__DBGOUT_ROUTINE_METHOD.discr =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (DISCR_ROUTINE))));
  strncpy (cdat->drout_189__DBGOUT_ROUTINE_METHOD.routdescr,
	   "DBGOUT_ROUTINE_METHOD @warmelt-debug.melt:858",
	   MELT_ROUTDESCR_LEN - 1);
  cdat->drout_189__DBGOUT_ROUTINE_METHOD.nbval = 6;
  MELT_ROUTINE_SET_ROUTCODE (&cdat->drout_189__DBGOUT_ROUTINE_METHOD,
			     meltrout_21_warmelt_debug_DBGOUT_ROUTINE_METHOD);

  /*iniclos dclo_190__DBGOUT_ROUTINE_METHOD */
 /*_.VALCLO___V237*/ meltfptr[236] =
    (melt_ptr_t) & cdat->dclo_190__DBGOUT_ROUTINE_METHOD;
  cdat->dclo_190__DBGOUT_ROUTINE_METHOD.discr =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (DISCR_CLOSURE))));
  cdat->dclo_190__DBGOUT_ROUTINE_METHOD.nbval = 0;


/*iniobj dsym_191__DISCR_ROUTINE*/
  /*uniqueobj*/ if (! /*_.VALDATA___V238*/ meltfptr[237])
/*_.VALDATA___V238*/ meltfptr[237] =
      (melt_ptr_t) & cdat->dsym_191__DISCR_ROUTINE;
  if (MELT_LIKELY (!melt_prohibit_garbcoll))
    melt_assertmsg ("iniobj check.discr isobj dsym_191__DISCR_ROUTINE",
		    melt_magic_discr ((melt_ptr_t)
				      (((melt_ptr_t)
					(MELT_PREDEF (CLASS_SYMBOL))))) ==
		    MELTOBMAG_OBJECT);
  if (MELT_LIKELY (!melt_prohibit_garbcoll))
    melt_assertmsg ("iniobj check.discr objmagic dsym_191__DISCR_ROUTINE",
		    ((meltobject_ptr_t)
		     (((melt_ptr_t) (MELT_PREDEF (CLASS_SYMBOL)))))->
		    meltobj_magic == MELTOBMAG_OBJECT);
  cdat->dsym_191__DISCR_ROUTINE.meltobj_class =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (CLASS_SYMBOL))));
  cdat->dsym_191__DISCR_ROUTINE.obj_hash = 30464565;
  cdat->dsym_191__DISCR_ROUTINE.obj_len = 3;


/*inistring dstr_192__DISCR_ROUTINE*/
 /*_.VALSTR___V239*/ meltfptr[238] =
    (melt_ptr_t) & cdat->dstr_192__DISCR_ROUTINE;
  cdat->dstr_192__DISCR_ROUTINE.discr =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (DISCR_STRING))));
  /*small inistring */ strncpy (cdat->dstr_192__DISCR_ROUTINE.val,
				"DISCR_ROUTINE",
				sizeof (cdat->dstr_192__DISCR_ROUTINE.val) -
				1);
  cdat->dstr_192__DISCR_ROUTINE.val[13] = (char) 0;
  cdat->dstr_192__DISCR_ROUTINE.slen = 13;


/*iniobj dsym_193__ROUTINE_EVERY*/
  /*uniqueobj*/ if (! /*_.VALDATA___V240*/ meltfptr[239])
/*_.VALDATA___V240*/ meltfptr[239] =
      (melt_ptr_t) & cdat->dsym_193__ROUTINE_EVERY;
  if (MELT_LIKELY (!melt_prohibit_garbcoll))
    melt_assertmsg ("iniobj check.discr isobj dsym_193__ROUTINE_EVERY",
		    melt_magic_discr ((melt_ptr_t)
				      (((melt_ptr_t)
					(MELT_PREDEF (CLASS_SYMBOL))))) ==
		    MELTOBMAG_OBJECT);
  if (MELT_LIKELY (!melt_prohibit_garbcoll))
    melt_assertmsg ("iniobj check.discr objmagic dsym_193__ROUTINE_EVERY",
		    ((meltobject_ptr_t)
		     (((melt_ptr_t) (MELT_PREDEF (CLASS_SYMBOL)))))->
		    meltobj_magic == MELTOBMAG_OBJECT);
  cdat->dsym_193__ROUTINE_EVERY.meltobj_class =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (CLASS_SYMBOL))));
  cdat->dsym_193__ROUTINE_EVERY.obj_hash = 347090207;
  cdat->dsym_193__ROUTINE_EVERY.obj_len = 3;


/*inistring dstr_194__ROUTINE_EVERY*/
 /*_.VALSTR___V241*/ meltfptr[240] =
    (melt_ptr_t) & cdat->dstr_194__ROUTINE_EVERY;
  cdat->dstr_194__ROUTINE_EVERY.discr =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (DISCR_STRING))));
  /*small inistring */ strncpy (cdat->dstr_194__ROUTINE_EVERY.val,
				"ROUTINE_EVERY",
				sizeof (cdat->dstr_194__ROUTINE_EVERY.val) -
				1);
  cdat->dstr_194__ROUTINE_EVERY.val[13] = (char) 0;
  cdat->dstr_194__ROUTINE_EVERY.slen = 13;

  /*inirout drout_195__LAMBDA_ */
 /*_.VALROUT___V236*/ meltfptr[235] =
    (melt_ptr_t) & cdat->drout_195__LAMBDA_;
  cdat->drout_195__LAMBDA_.discr =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (DISCR_ROUTINE))));
  strncpy (cdat->drout_195__LAMBDA_.routdescr,
	   "LAMBDA_ @warmelt-debug.melt:873", MELT_ROUTDESCR_LEN - 1);
  cdat->drout_195__LAMBDA_.nbval = 1;
  MELT_ROUTINE_SET_ROUTCODE (&cdat->drout_195__LAMBDA_,
			     meltrout_22_warmelt_debug_LAMBDA___1__);

  /*inirout drout_196__DBGOUT_CLOSURE_METHOD */
 /*_.VALROUT___V242*/ meltfptr[241] =
    (melt_ptr_t) & cdat->drout_196__DBGOUT_CLOSURE_METHOD;
  cdat->drout_196__DBGOUT_CLOSURE_METHOD.discr =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (DISCR_ROUTINE))));
  strncpy (cdat->drout_196__DBGOUT_CLOSURE_METHOD.routdescr,
	   "DBGOUT_CLOSURE_METHOD @warmelt-debug.melt:885",
	   MELT_ROUTDESCR_LEN - 1);
  cdat->drout_196__DBGOUT_CLOSURE_METHOD.nbval = 4;
  MELT_ROUTINE_SET_ROUTCODE (&cdat->drout_196__DBGOUT_CLOSURE_METHOD,
			     meltrout_23_warmelt_debug_DBGOUT_CLOSURE_METHOD);

  /*iniclos dclo_197__DBGOUT_CLOSURE_METHOD */
 /*_.VALCLO___V243*/ meltfptr[242] =
    (melt_ptr_t) & cdat->dclo_197__DBGOUT_CLOSURE_METHOD;
  cdat->dclo_197__DBGOUT_CLOSURE_METHOD.discr =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (DISCR_CLOSURE))));
  cdat->dclo_197__DBGOUT_CLOSURE_METHOD.nbval = 0;

  /*inirout drout_198__DBGOUT_LIST_METHOD */
 /*_.VALROUT___V244*/ meltfptr[243] =
    (melt_ptr_t) & cdat->drout_198__DBGOUT_LIST_METHOD;
  cdat->drout_198__DBGOUT_LIST_METHOD.discr =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (DISCR_ROUTINE))));
  strncpy (cdat->drout_198__DBGOUT_LIST_METHOD.routdescr,
	   "DBGOUT_LIST_METHOD @warmelt-debug.melt:930",
	   MELT_ROUTDESCR_LEN - 1);
  cdat->drout_198__DBGOUT_LIST_METHOD.nbval = 4;
  MELT_ROUTINE_SET_ROUTCODE (&cdat->drout_198__DBGOUT_LIST_METHOD,
			     meltrout_24_warmelt_debug_DBGOUT_LIST_METHOD);

  /*iniclos dclo_199__DBGOUT_LIST_METHOD */
 /*_.VALCLO___V245*/ meltfptr[244] =
    (melt_ptr_t) & cdat->dclo_199__DBGOUT_LIST_METHOD;
  cdat->dclo_199__DBGOUT_LIST_METHOD.discr =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (DISCR_CLOSURE))));
  cdat->dclo_199__DBGOUT_LIST_METHOD.nbval = 0;


/*iniobj dsym_200__DISCR_LIST*/
  /*uniqueobj*/ if (! /*_.VALDATA___V246*/ meltfptr[245])
/*_.VALDATA___V246*/ meltfptr[245] =
      (melt_ptr_t) & cdat->dsym_200__DISCR_LIST;
  if (MELT_LIKELY (!melt_prohibit_garbcoll))
    melt_assertmsg ("iniobj check.discr isobj dsym_200__DISCR_LIST",
		    melt_magic_discr ((melt_ptr_t)
				      (((melt_ptr_t)
					(MELT_PREDEF (CLASS_SYMBOL))))) ==
		    MELTOBMAG_OBJECT);
  if (MELT_LIKELY (!melt_prohibit_garbcoll))
    melt_assertmsg ("iniobj check.discr objmagic dsym_200__DISCR_LIST",
		    ((meltobject_ptr_t)
		     (((melt_ptr_t) (MELT_PREDEF (CLASS_SYMBOL)))))->
		    meltobj_magic == MELTOBMAG_OBJECT);
  cdat->dsym_200__DISCR_LIST.meltobj_class =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (CLASS_SYMBOL))));
  cdat->dsym_200__DISCR_LIST.obj_hash = 98407634;
  cdat->dsym_200__DISCR_LIST.obj_len = 3;


/*inistring dstr_201__DISCR_LIST*/
 /*_.VALSTR___V247*/ meltfptr[246] =
    (melt_ptr_t) & cdat->dstr_201__DISCR_LIST;
  cdat->dstr_201__DISCR_LIST.discr =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (DISCR_STRING))));
  /*small inistring */ strncpy (cdat->dstr_201__DISCR_LIST.val, "DISCR_LIST",
				sizeof (cdat->dstr_201__DISCR_LIST.val) - 1);
  cdat->dstr_201__DISCR_LIST.val[10] = (char) 0;
  cdat->dstr_201__DISCR_LIST.slen = 10;

  /*inirout drout_202__DBGOUT_PAIR_METHOD */
 /*_.VALROUT___V248*/ meltfptr[247] =
    (melt_ptr_t) & cdat->drout_202__DBGOUT_PAIR_METHOD;
  cdat->drout_202__DBGOUT_PAIR_METHOD.discr =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (DISCR_ROUTINE))));
  strncpy (cdat->drout_202__DBGOUT_PAIR_METHOD.routdescr,
	   "DBGOUT_PAIR_METHOD @warmelt-debug.melt:973",
	   MELT_ROUTDESCR_LEN - 1);
  cdat->drout_202__DBGOUT_PAIR_METHOD.nbval = 4;
  MELT_ROUTINE_SET_ROUTCODE (&cdat->drout_202__DBGOUT_PAIR_METHOD,
			     meltrout_25_warmelt_debug_DBGOUT_PAIR_METHOD);

  /*iniclos dclo_203__DBGOUT_PAIR_METHOD */
 /*_.VALCLO___V249*/ meltfptr[248] =
    (melt_ptr_t) & cdat->dclo_203__DBGOUT_PAIR_METHOD;
  cdat->dclo_203__DBGOUT_PAIR_METHOD.discr =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (DISCR_CLOSURE))));
  cdat->dclo_203__DBGOUT_PAIR_METHOD.nbval = 0;


/*iniobj dsym_204__DISCR_PAIR*/
  /*uniqueobj*/ if (! /*_.VALDATA___V250*/ meltfptr[249])
/*_.VALDATA___V250*/ meltfptr[249] =
      (melt_ptr_t) & cdat->dsym_204__DISCR_PAIR;
  if (MELT_LIKELY (!melt_prohibit_garbcoll))
    melt_assertmsg ("iniobj check.discr isobj dsym_204__DISCR_PAIR",
		    melt_magic_discr ((melt_ptr_t)
				      (((melt_ptr_t)
					(MELT_PREDEF (CLASS_SYMBOL))))) ==
		    MELTOBMAG_OBJECT);
  if (MELT_LIKELY (!melt_prohibit_garbcoll))
    melt_assertmsg ("iniobj check.discr objmagic dsym_204__DISCR_PAIR",
		    ((meltobject_ptr_t)
		     (((melt_ptr_t) (MELT_PREDEF (CLASS_SYMBOL)))))->
		    meltobj_magic == MELTOBMAG_OBJECT);
  cdat->dsym_204__DISCR_PAIR.meltobj_class =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (CLASS_SYMBOL))));
  cdat->dsym_204__DISCR_PAIR.obj_hash = 69333571;
  cdat->dsym_204__DISCR_PAIR.obj_len = 3;


/*inistring dstr_205__DISCR_PAIR*/
 /*_.VALSTR___V251*/ meltfptr[250] =
    (melt_ptr_t) & cdat->dstr_205__DISCR_PAIR;
  cdat->dstr_205__DISCR_PAIR.discr =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (DISCR_STRING))));
  /*small inistring */ strncpy (cdat->dstr_205__DISCR_PAIR.val, "DISCR_PAIR",
				sizeof (cdat->dstr_205__DISCR_PAIR.val) - 1);
  cdat->dstr_205__DISCR_PAIR.val[10] = (char) 0;
  cdat->dstr_205__DISCR_PAIR.slen = 10;

  /*inirout drout_206__DBGOUT_MAPOBJECT_METHOD */
 /*_.VALROUT___V252*/ meltfptr[251] =
    (melt_ptr_t) & cdat->drout_206__DBGOUT_MAPOBJECT_METHOD;
  cdat->drout_206__DBGOUT_MAPOBJECT_METHOD.discr =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (DISCR_ROUTINE))));
  strncpy (cdat->drout_206__DBGOUT_MAPOBJECT_METHOD.routdescr,
	   "DBGOUT_MAPOBJECT_METHOD @warmelt-debug.melt:998",
	   MELT_ROUTDESCR_LEN - 1);
  cdat->drout_206__DBGOUT_MAPOBJECT_METHOD.nbval = 16;
  MELT_ROUTINE_SET_ROUTCODE (&cdat->drout_206__DBGOUT_MAPOBJECT_METHOD,
			     meltrout_26_warmelt_debug_DBGOUT_MAPOBJECT_METHOD);

  /*iniclos dclo_207__DBGOUT_MAPOBJECT_METHOD */
 /*_.VALCLO___V259*/ meltfptr[258] =
    (melt_ptr_t) & cdat->dclo_207__DBGOUT_MAPOBJECT_METHOD;
  cdat->dclo_207__DBGOUT_MAPOBJECT_METHOD.discr =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (DISCR_CLOSURE))));
  cdat->dclo_207__DBGOUT_MAPOBJECT_METHOD.nbval = 0;


/*iniobj dsym_208__MAPOBJECT_EVERY*/
  /*uniqueobj*/ if (! /*_.VALDATA___V260*/ meltfptr[259])
/*_.VALDATA___V260*/ meltfptr[259] =
      (melt_ptr_t) & cdat->dsym_208__MAPOBJECT_EVERY;
  if (MELT_LIKELY (!melt_prohibit_garbcoll))
    melt_assertmsg ("iniobj check.discr isobj dsym_208__MAPOBJECT_EVERY",
		    melt_magic_discr ((melt_ptr_t)
				      (((melt_ptr_t)
					(MELT_PREDEF (CLASS_SYMBOL))))) ==
		    MELTOBMAG_OBJECT);
  if (MELT_LIKELY (!melt_prohibit_garbcoll))
    melt_assertmsg ("iniobj check.discr objmagic dsym_208__MAPOBJECT_EVERY",
		    ((meltobject_ptr_t)
		     (((melt_ptr_t) (MELT_PREDEF (CLASS_SYMBOL)))))->
		    meltobj_magic == MELTOBMAG_OBJECT);
  cdat->dsym_208__MAPOBJECT_EVERY.meltobj_class =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (CLASS_SYMBOL))));
  cdat->dsym_208__MAPOBJECT_EVERY.obj_hash = 726732435;
  cdat->dsym_208__MAPOBJECT_EVERY.obj_len = 3;


/*inistring dstr_209__MAPOBJECT_EVERY*/
 /*_.VALSTR___V261*/ meltfptr[260] =
    (melt_ptr_t) & cdat->dstr_209__MAPOBJECT_EVERY;
  cdat->dstr_209__MAPOBJECT_EVERY.discr =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (DISCR_STRING))));
  /*small inistring */ strncpy (cdat->dstr_209__MAPOBJECT_EVERY.val,
				"MAPOBJECT_EVERY",
				sizeof (cdat->dstr_209__MAPOBJECT_EVERY.val) -
				1);
  cdat->dstr_209__MAPOBJECT_EVERY.val[15] = (char) 0;
  cdat->dstr_209__MAPOBJECT_EVERY.slen = 15;

  /*inirout drout_210__LAMBDA_ */
 /*_.VALROUT___V253*/ meltfptr[252] =
    (melt_ptr_t) & cdat->drout_210__LAMBDA_;
  cdat->drout_210__LAMBDA_.discr =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (DISCR_ROUTINE))));
  strncpy (cdat->drout_210__LAMBDA_.routdescr,
	   "LAMBDA_ @warmelt-debug.melt:1034", MELT_ROUTDESCR_LEN - 1);
  cdat->drout_210__LAMBDA_.nbval = 1;
  MELT_ROUTINE_SET_ROUTCODE (&cdat->drout_210__LAMBDA_,
			     meltrout_27_warmelt_debug_LAMBDA___2__);

  /*inirout drout_211__LAMBDA_ */
 /*_.VALROUT___V254*/ meltfptr[253] =
    (melt_ptr_t) & cdat->drout_211__LAMBDA_;
  cdat->drout_211__LAMBDA_.discr =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (DISCR_ROUTINE))));
  strncpy (cdat->drout_211__LAMBDA_.routdescr,
	   "LAMBDA_ @warmelt-debug.melt:1047", MELT_ROUTDESCR_LEN - 1);
  cdat->drout_211__LAMBDA_.nbval = 4;
  MELT_ROUTINE_SET_ROUTCODE (&cdat->drout_211__LAMBDA_,
			     meltrout_28_warmelt_debug_LAMBDA___3__);


/*iniobj dsym_212__COMPARE_OBJ_RANKED*/
  /*uniqueobj*/ if (! /*_.VALDATA___V262*/ meltfptr[261])
/*_.VALDATA___V262*/ meltfptr[261] =
      (melt_ptr_t) & cdat->dsym_212__COMPARE_OBJ_RANKED;
  if (MELT_LIKELY (!melt_prohibit_garbcoll))
    melt_assertmsg ("iniobj check.discr isobj dsym_212__COMPARE_OBJ_RANKED",
		    melt_magic_discr ((melt_ptr_t)
				      (((melt_ptr_t)
					(MELT_PREDEF (CLASS_SYMBOL))))) ==
		    MELTOBMAG_OBJECT);
  if (MELT_LIKELY (!melt_prohibit_garbcoll))
    melt_assertmsg
      ("iniobj check.discr objmagic dsym_212__COMPARE_OBJ_RANKED",
       ((meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (CLASS_SYMBOL)))))->
       meltobj_magic == MELTOBMAG_OBJECT);
  cdat->dsym_212__COMPARE_OBJ_RANKED.meltobj_class =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (CLASS_SYMBOL))));
  cdat->dsym_212__COMPARE_OBJ_RANKED.obj_hash = 657124045;
  cdat->dsym_212__COMPARE_OBJ_RANKED.obj_len = 3;


/*inistring dstr_213__COMPARE_OBJ_RANK*/
 /*_.VALSTR___V263*/ meltfptr[262] =
    (melt_ptr_t) & cdat->dstr_213__COMPARE_OBJ_RANK;
  cdat->dstr_213__COMPARE_OBJ_RANK.discr =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (DISCR_STRING))));
  /*small inistring */ strncpy (cdat->dstr_213__COMPARE_OBJ_RANK.val,
				"COMPARE_OBJ_RANKED",
				sizeof (cdat->dstr_213__COMPARE_OBJ_RANK.
					val) - 1);
  cdat->dstr_213__COMPARE_OBJ_RANK.val[18] = (char) 0;
  cdat->dstr_213__COMPARE_OBJ_RANK.slen = 18;

  /*iniboxint dint_214__ */
 /*_.VALBXINT___V255*/ meltfptr[254] = (melt_ptr_t) & cdat->dint_214__;
  cdat->dint_214__.discr =
    (meltobject_ptr_t) (((melt_ptr_t)
			 (MELT_PREDEF (DISCR_CONSTANT_INTEGER))));
  cdat->dint_214__.val = -1;

  /*iniboxint dint_215__ */
 /*_.VALBXINT___V256*/ meltfptr[255] = (melt_ptr_t) & cdat->dint_215__;
  cdat->dint_215__.discr =
    (meltobject_ptr_t) (((melt_ptr_t)
			 (MELT_PREDEF (DISCR_CONSTANT_INTEGER))));
  cdat->dint_215__.val = 0;

  /*iniboxint dint_216__ */
 /*_.VALBXINT___V257*/ meltfptr[256] = (melt_ptr_t) & cdat->dint_216__;
  cdat->dint_216__.discr =
    (meltobject_ptr_t) (((melt_ptr_t)
			 (MELT_PREDEF (DISCR_CONSTANT_INTEGER))));
  cdat->dint_216__.val = 1;


/*iniobj dsym_217__MULTIPLE_EVERY*/
  /*uniqueobj*/ if (! /*_.VALDATA___V264*/ meltfptr[263])
/*_.VALDATA___V264*/ meltfptr[263] =
      (melt_ptr_t) & cdat->dsym_217__MULTIPLE_EVERY;
  if (MELT_LIKELY (!melt_prohibit_garbcoll))
    melt_assertmsg ("iniobj check.discr isobj dsym_217__MULTIPLE_EVERY",
		    melt_magic_discr ((melt_ptr_t)
				      (((melt_ptr_t)
					(MELT_PREDEF (CLASS_SYMBOL))))) ==
		    MELTOBMAG_OBJECT);
  if (MELT_LIKELY (!melt_prohibit_garbcoll))
    melt_assertmsg ("iniobj check.discr objmagic dsym_217__MULTIPLE_EVERY",
		    ((meltobject_ptr_t)
		     (((melt_ptr_t) (MELT_PREDEF (CLASS_SYMBOL)))))->
		    meltobj_magic == MELTOBMAG_OBJECT);
  cdat->dsym_217__MULTIPLE_EVERY.meltobj_class =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (CLASS_SYMBOL))));
  cdat->dsym_217__MULTIPLE_EVERY.obj_hash = 130098745;
  cdat->dsym_217__MULTIPLE_EVERY.obj_len = 3;


/*inistring dstr_218__MULTIPLE_EVERY*/
 /*_.VALSTR___V265*/ meltfptr[264] =
    (melt_ptr_t) & cdat->dstr_218__MULTIPLE_EVERY;
  cdat->dstr_218__MULTIPLE_EVERY.discr =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (DISCR_STRING))));
  /*small inistring */ strncpy (cdat->dstr_218__MULTIPLE_EVERY.val,
				"MULTIPLE_EVERY",
				sizeof (cdat->dstr_218__MULTIPLE_EVERY.val) -
				1);
  cdat->dstr_218__MULTIPLE_EVERY.val[14] = (char) 0;
  cdat->dstr_218__MULTIPLE_EVERY.slen = 14;

  /*inirout drout_219__LAMBDA_ */
 /*_.VALROUT___V258*/ meltfptr[257] =
    (melt_ptr_t) & cdat->drout_219__LAMBDA_;
  cdat->drout_219__LAMBDA_.discr =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (DISCR_ROUTINE))));
  strncpy (cdat->drout_219__LAMBDA_.routdescr,
	   "LAMBDA_ @warmelt-debug.melt:1063", MELT_ROUTDESCR_LEN - 1);
  cdat->drout_219__LAMBDA_.nbval = 2;
  MELT_ROUTINE_SET_ROUTCODE (&cdat->drout_219__LAMBDA_,
			     meltrout_29_warmelt_debug_LAMBDA___4__);

  /*inirout drout_220__DBGOUT_MAPSTRING_METHOD */
 /*_.VALROUT___V266*/ meltfptr[265] =
    (melt_ptr_t) & cdat->drout_220__DBGOUT_MAPSTRING_METHOD;
  cdat->drout_220__DBGOUT_MAPSTRING_METHOD.discr =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (DISCR_ROUTINE))));
  strncpy (cdat->drout_220__DBGOUT_MAPSTRING_METHOD.routdescr,
	   "DBGOUT_MAPSTRING_METHOD @warmelt-debug.melt:1086",
	   MELT_ROUTDESCR_LEN - 1);
  cdat->drout_220__DBGOUT_MAPSTRING_METHOD.nbval = 15;
  MELT_ROUTINE_SET_ROUTCODE (&cdat->drout_220__DBGOUT_MAPSTRING_METHOD,
			     meltrout_30_warmelt_debug_DBGOUT_MAPSTRING_METHOD);

  /*iniclos dclo_221__DBGOUT_MAPSTRING_METHOD */
 /*_.VALCLO___V273*/ meltfptr[272] =
    (melt_ptr_t) & cdat->dclo_221__DBGOUT_MAPSTRING_METHOD;
  cdat->dclo_221__DBGOUT_MAPSTRING_METHOD.discr =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (DISCR_CLOSURE))));
  cdat->dclo_221__DBGOUT_MAPSTRING_METHOD.nbval = 0;


/*iniobj dsym_222__DISCR_MAP_STRINGS*/
  /*uniqueobj*/ if (! /*_.VALDATA___V274*/ meltfptr[273])
/*_.VALDATA___V274*/ meltfptr[273] =
      (melt_ptr_t) & cdat->dsym_222__DISCR_MAP_STRINGS;
  if (MELT_LIKELY (!melt_prohibit_garbcoll))
    melt_assertmsg ("iniobj check.discr isobj dsym_222__DISCR_MAP_STRINGS",
		    melt_magic_discr ((melt_ptr_t)
				      (((melt_ptr_t)
					(MELT_PREDEF (CLASS_SYMBOL))))) ==
		    MELTOBMAG_OBJECT);
  if (MELT_LIKELY (!melt_prohibit_garbcoll))
    melt_assertmsg ("iniobj check.discr objmagic dsym_222__DISCR_MAP_STRINGS",
		    ((meltobject_ptr_t)
		     (((melt_ptr_t) (MELT_PREDEF (CLASS_SYMBOL)))))->
		    meltobj_magic == MELTOBMAG_OBJECT);
  cdat->dsym_222__DISCR_MAP_STRINGS.meltobj_class =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (CLASS_SYMBOL))));
  cdat->dsym_222__DISCR_MAP_STRINGS.obj_hash = 344609662;
  cdat->dsym_222__DISCR_MAP_STRINGS.obj_len = 3;


/*inistring dstr_223__DISCR_MAP_STRING*/
 /*_.VALSTR___V275*/ meltfptr[274] =
    (melt_ptr_t) & cdat->dstr_223__DISCR_MAP_STRING;
  cdat->dstr_223__DISCR_MAP_STRING.discr =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (DISCR_STRING))));
  /*small inistring */ strncpy (cdat->dstr_223__DISCR_MAP_STRING.val,
				"DISCR_MAP_STRINGS",
				sizeof (cdat->dstr_223__DISCR_MAP_STRING.
					val) - 1);
  cdat->dstr_223__DISCR_MAP_STRING.val[17] = (char) 0;
  cdat->dstr_223__DISCR_MAP_STRING.slen = 17;


/*iniobj dsym_224__MAPSTRING_EVERY*/
  /*uniqueobj*/ if (! /*_.VALDATA___V276*/ meltfptr[275])
/*_.VALDATA___V276*/ meltfptr[275] =
      (melt_ptr_t) & cdat->dsym_224__MAPSTRING_EVERY;
  if (MELT_LIKELY (!melt_prohibit_garbcoll))
    melt_assertmsg ("iniobj check.discr isobj dsym_224__MAPSTRING_EVERY",
		    melt_magic_discr ((melt_ptr_t)
				      (((melt_ptr_t)
					(MELT_PREDEF (CLASS_SYMBOL))))) ==
		    MELTOBMAG_OBJECT);
  if (MELT_LIKELY (!melt_prohibit_garbcoll))
    melt_assertmsg ("iniobj check.discr objmagic dsym_224__MAPSTRING_EVERY",
		    ((meltobject_ptr_t)
		     (((melt_ptr_t) (MELT_PREDEF (CLASS_SYMBOL)))))->
		    meltobj_magic == MELTOBMAG_OBJECT);
  cdat->dsym_224__MAPSTRING_EVERY.meltobj_class =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (CLASS_SYMBOL))));
  cdat->dsym_224__MAPSTRING_EVERY.obj_hash = 173778712;
  cdat->dsym_224__MAPSTRING_EVERY.obj_len = 3;


/*inistring dstr_225__MAPSTRING_EVERY*/
 /*_.VALSTR___V277*/ meltfptr[276] =
    (melt_ptr_t) & cdat->dstr_225__MAPSTRING_EVERY;
  cdat->dstr_225__MAPSTRING_EVERY.discr =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (DISCR_STRING))));
  /*small inistring */ strncpy (cdat->dstr_225__MAPSTRING_EVERY.val,
				"MAPSTRING_EVERY",
				sizeof (cdat->dstr_225__MAPSTRING_EVERY.val) -
				1);
  cdat->dstr_225__MAPSTRING_EVERY.val[15] = (char) 0;
  cdat->dstr_225__MAPSTRING_EVERY.slen = 15;

  /*inirout drout_226__LAMBDA_ */
 /*_.VALROUT___V267*/ meltfptr[266] =
    (melt_ptr_t) & cdat->drout_226__LAMBDA_;
  cdat->drout_226__LAMBDA_.discr =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (DISCR_ROUTINE))));
  strncpy (cdat->drout_226__LAMBDA_.routdescr,
	   "LAMBDA_ @warmelt-debug.melt:1122", MELT_ROUTDESCR_LEN - 1);
  cdat->drout_226__LAMBDA_.nbval = 1;
  MELT_ROUTINE_SET_ROUTCODE (&cdat->drout_226__LAMBDA_,
			     meltrout_31_warmelt_debug_LAMBDA___5__);

  /*inirout drout_227__LAMBDA_ */
 /*_.VALROUT___V268*/ meltfptr[267] =
    (melt_ptr_t) & cdat->drout_227__LAMBDA_;
  cdat->drout_227__LAMBDA_.discr =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (DISCR_ROUTINE))));
  strncpy (cdat->drout_227__LAMBDA_.routdescr,
	   "LAMBDA_ @warmelt-debug.melt:1134", MELT_ROUTDESCR_LEN - 1);
  cdat->drout_227__LAMBDA_.nbval = 4;
  MELT_ROUTINE_SET_ROUTCODE (&cdat->drout_227__LAMBDA_,
			     meltrout_32_warmelt_debug_LAMBDA___6__);

  /*iniboxint dint_228__ */
 /*_.VALBXINT___V269*/ meltfptr[268] = (melt_ptr_t) & cdat->dint_228__;
  cdat->dint_228__.discr =
    (meltobject_ptr_t) (((melt_ptr_t)
			 (MELT_PREDEF (DISCR_CONSTANT_INTEGER))));
  cdat->dint_228__.val = -1;

  /*iniboxint dint_229__ */
 /*_.VALBXINT___V270*/ meltfptr[269] = (melt_ptr_t) & cdat->dint_229__;
  cdat->dint_229__.discr =
    (meltobject_ptr_t) (((melt_ptr_t)
			 (MELT_PREDEF (DISCR_CONSTANT_INTEGER))));
  cdat->dint_229__.val = 0;

  /*iniboxint dint_230__ */
 /*_.VALBXINT___V271*/ meltfptr[270] = (melt_ptr_t) & cdat->dint_230__;
  cdat->dint_230__.discr =
    (meltobject_ptr_t) (((melt_ptr_t)
			 (MELT_PREDEF (DISCR_CONSTANT_INTEGER))));
  cdat->dint_230__.val = 1;

  /*inirout drout_231__LAMBDA_ */
 /*_.VALROUT___V272*/ meltfptr[271] =
    (melt_ptr_t) & cdat->drout_231__LAMBDA_;
  cdat->drout_231__LAMBDA_.discr =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (DISCR_ROUTINE))));
  strncpy (cdat->drout_231__LAMBDA_.routdescr,
	   "LAMBDA_ @warmelt-debug.melt:1150", MELT_ROUTDESCR_LEN - 1);
  cdat->drout_231__LAMBDA_.nbval = 1;
  MELT_ROUTINE_SET_ROUTCODE (&cdat->drout_231__LAMBDA_,
			     meltrout_33_warmelt_debug_LAMBDA___7__);

  /*inirout drout_232__DBGOUT_BUCKETLONG_METHOD */
 /*_.VALROUT___V278*/ meltfptr[277] =
    (melt_ptr_t) & cdat->drout_232__DBGOUT_BUCKETLONG_METHOD;
  cdat->drout_232__DBGOUT_BUCKETLONG_METHOD.discr =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (DISCR_ROUTINE))));
  strncpy (cdat->drout_232__DBGOUT_BUCKETLONG_METHOD.routdescr,
	   "DBGOUT_BUCKETLONG_METHOD @warmelt-debug.melt:1170",
	   MELT_ROUTDESCR_LEN - 1);
  cdat->drout_232__DBGOUT_BUCKETLONG_METHOD.nbval = 5;
  MELT_ROUTINE_SET_ROUTCODE (&cdat->drout_232__DBGOUT_BUCKETLONG_METHOD,
			     meltrout_34_warmelt_debug_DBGOUT_BUCKETLONG_METHOD);

  /*iniclos dclo_233__DBGOUT_BUCKETLONG_METHOD */
 /*_.VALCLO___V279*/ meltfptr[278] =
    (melt_ptr_t) & cdat->dclo_233__DBGOUT_BUCKETLONG_METHOD;
  cdat->dclo_233__DBGOUT_BUCKETLONG_METHOD.discr =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (DISCR_CLOSURE))));
  cdat->dclo_233__DBGOUT_BUCKETLONG_METHOD.nbval = 0;


/*iniobj dsym_234__DISCR_BUCKET_LONGS*/
  /*uniqueobj*/ if (! /*_.VALDATA___V280*/ meltfptr[279])
/*_.VALDATA___V280*/ meltfptr[279] =
      (melt_ptr_t) & cdat->dsym_234__DISCR_BUCKET_LONGS;
  if (MELT_LIKELY (!melt_prohibit_garbcoll))
    melt_assertmsg ("iniobj check.discr isobj dsym_234__DISCR_BUCKET_LONGS",
		    melt_magic_discr ((melt_ptr_t)
				      (((melt_ptr_t)
					(MELT_PREDEF (CLASS_SYMBOL))))) ==
		    MELTOBMAG_OBJECT);
  if (MELT_LIKELY (!melt_prohibit_garbcoll))
    melt_assertmsg
      ("iniobj check.discr objmagic dsym_234__DISCR_BUCKET_LONGS",
       ((meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (CLASS_SYMBOL)))))->
       meltobj_magic == MELTOBMAG_OBJECT);
  cdat->dsym_234__DISCR_BUCKET_LONGS.meltobj_class =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (CLASS_SYMBOL))));
  cdat->dsym_234__DISCR_BUCKET_LONGS.obj_hash = 603682282;
  cdat->dsym_234__DISCR_BUCKET_LONGS.obj_len = 3;


/*inistring dstr_235__DISCR_BUCKET_LON*/
 /*_.VALSTR___V281*/ meltfptr[280] =
    (melt_ptr_t) & cdat->dstr_235__DISCR_BUCKET_LON;
  cdat->dstr_235__DISCR_BUCKET_LON.discr =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (DISCR_STRING))));
  /*small inistring */ strncpy (cdat->dstr_235__DISCR_BUCKET_LON.val,
				"DISCR_BUCKET_LONGS",
				sizeof (cdat->dstr_235__DISCR_BUCKET_LON.
					val) - 1);
  cdat->dstr_235__DISCR_BUCKET_LON.val[18] = (char) 0;
  cdat->dstr_235__DISCR_BUCKET_LON.slen = 18;

  /*inirout drout_236__DBGOUT_ANYOBJECT_METHOD */
 /*_.VALROUT___V282*/ meltfptr[281] =
    (melt_ptr_t) & cdat->drout_236__DBGOUT_ANYOBJECT_METHOD;
  cdat->drout_236__DBGOUT_ANYOBJECT_METHOD.discr =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (DISCR_ROUTINE))));
  strncpy (cdat->drout_236__DBGOUT_ANYOBJECT_METHOD.routdescr,
	   "DBGOUT_ANYOBJECT_METHOD @warmelt-debug.melt:1207",
	   MELT_ROUTDESCR_LEN - 1);
  cdat->drout_236__DBGOUT_ANYOBJECT_METHOD.nbval = 3;
  MELT_ROUTINE_SET_ROUTCODE (&cdat->drout_236__DBGOUT_ANYOBJECT_METHOD,
			     meltrout_35_warmelt_debug_DBGOUT_ANYOBJECT_METHOD);

  /*iniclos dclo_237__DBGOUT_ANYOBJECT_METHOD */
 /*_.VALCLO___V283*/ meltfptr[282] =
    (melt_ptr_t) & cdat->dclo_237__DBGOUT_ANYOBJECT_METHOD;
  cdat->dclo_237__DBGOUT_ANYOBJECT_METHOD.discr =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (DISCR_CLOSURE))));
  cdat->dclo_237__DBGOUT_ANYOBJECT_METHOD.nbval = 0;

  /*inirout drout_238__DBGOUT_ANYRECV_METHOD */
 /*_.VALROUT___V284*/ meltfptr[283] =
    (melt_ptr_t) & cdat->drout_238__DBGOUT_ANYRECV_METHOD;
  cdat->drout_238__DBGOUT_ANYRECV_METHOD.discr =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (DISCR_ROUTINE))));
  strncpy (cdat->drout_238__DBGOUT_ANYRECV_METHOD.routdescr,
	   "DBGOUT_ANYRECV_METHOD @warmelt-debug.melt:1235",
	   MELT_ROUTDESCR_LEN - 1);
  cdat->drout_238__DBGOUT_ANYRECV_METHOD.nbval = 2;
  MELT_ROUTINE_SET_ROUTCODE (&cdat->drout_238__DBGOUT_ANYRECV_METHOD,
			     meltrout_36_warmelt_debug_DBGOUT_ANYRECV_METHOD);

  /*iniclos dclo_239__DBGOUT_ANYRECV_METHOD */
 /*_.VALCLO___V285*/ meltfptr[284] =
    (melt_ptr_t) & cdat->dclo_239__DBGOUT_ANYRECV_METHOD;
  cdat->dclo_239__DBGOUT_ANYRECV_METHOD.discr =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (DISCR_CLOSURE))));
  cdat->dclo_239__DBGOUT_ANYRECV_METHOD.nbval = 0;


/*iniobj dsym_240__DISCR_ANY_RECEIVER*/
  /*uniqueobj*/ if (! /*_.VALDATA___V286*/ meltfptr[285])
/*_.VALDATA___V286*/ meltfptr[285] =
      (melt_ptr_t) & cdat->dsym_240__DISCR_ANY_RECEIVER;
  if (MELT_LIKELY (!melt_prohibit_garbcoll))
    melt_assertmsg ("iniobj check.discr isobj dsym_240__DISCR_ANY_RECEIVER",
		    melt_magic_discr ((melt_ptr_t)
				      (((melt_ptr_t)
					(MELT_PREDEF (CLASS_SYMBOL))))) ==
		    MELTOBMAG_OBJECT);
  if (MELT_LIKELY (!melt_prohibit_garbcoll))
    melt_assertmsg
      ("iniobj check.discr objmagic dsym_240__DISCR_ANY_RECEIVER",
       ((meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (CLASS_SYMBOL)))))->
       meltobj_magic == MELTOBMAG_OBJECT);
  cdat->dsym_240__DISCR_ANY_RECEIVER.meltobj_class =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (CLASS_SYMBOL))));
  cdat->dsym_240__DISCR_ANY_RECEIVER.obj_hash = 95947314;
  cdat->dsym_240__DISCR_ANY_RECEIVER.obj_len = 3;


/*inistring dstr_241__DISCR_ANY_RECEIV*/
 /*_.VALSTR___V287*/ meltfptr[286] =
    (melt_ptr_t) & cdat->dstr_241__DISCR_ANY_RECEIV;
  cdat->dstr_241__DISCR_ANY_RECEIV.discr =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (DISCR_STRING))));
  /*small inistring */ strncpy (cdat->dstr_241__DISCR_ANY_RECEIV.val,
				"DISCR_ANY_RECEIVER",
				sizeof (cdat->dstr_241__DISCR_ANY_RECEIV.
					val) - 1);
  cdat->dstr_241__DISCR_ANY_RECEIV.val[18] = (char) 0;
  cdat->dstr_241__DISCR_ANY_RECEIV.slen = 18;

  /*inirout drout_242__DBGOUTAGAIN_ANYOBJECT_METHOD */
 /*_.VALROUT___V288*/ meltfptr[287] =
    (melt_ptr_t) & cdat->drout_242__DBGOUTAGAIN_ANYOBJECT_METHOD;
  cdat->drout_242__DBGOUTAGAIN_ANYOBJECT_METHOD.discr =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (DISCR_ROUTINE))));
  strncpy (cdat->drout_242__DBGOUTAGAIN_ANYOBJECT_METHOD.routdescr,
	   "DBGOUTAGAIN_ANYOBJECT_METHOD @warmelt-debug.melt:1246",
	   MELT_ROUTDESCR_LEN - 1);
  cdat->drout_242__DBGOUTAGAIN_ANYOBJECT_METHOD.nbval = 1;
  MELT_ROUTINE_SET_ROUTCODE (&cdat->drout_242__DBGOUTAGAIN_ANYOBJECT_METHOD,
			     meltrout_37_warmelt_debug_DBGOUTAGAIN_ANYOBJECT_METHOD);

  /*iniclos dclo_243__DBGOUTAGAIN_ANYOBJECT_METHOD */
 /*_.VALCLO___V289*/ meltfptr[288] =
    (melt_ptr_t) & cdat->dclo_243__DBGOUTAGAIN_ANYOBJECT_METHOD;
  cdat->dclo_243__DBGOUTAGAIN_ANYOBJECT_METHOD.discr =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (DISCR_CLOSURE))));
  cdat->dclo_243__DBGOUTAGAIN_ANYOBJECT_METHOD.nbval = 0;

  /*inirout drout_244__DBGOUT_NAMEDOBJECT_METHOD */
 /*_.VALROUT___V290*/ meltfptr[289] =
    (melt_ptr_t) & cdat->drout_244__DBGOUT_NAMEDOBJECT_METHOD;
  cdat->drout_244__DBGOUT_NAMEDOBJECT_METHOD.discr =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (DISCR_ROUTINE))));
  strncpy (cdat->drout_244__DBGOUT_NAMEDOBJECT_METHOD.routdescr,
	   "DBGOUT_NAMEDOBJECT_METHOD @warmelt-debug.melt:1264",
	   MELT_ROUTDESCR_LEN - 1);
  cdat->drout_244__DBGOUT_NAMEDOBJECT_METHOD.nbval = 4;
  MELT_ROUTINE_SET_ROUTCODE (&cdat->drout_244__DBGOUT_NAMEDOBJECT_METHOD,
			     meltrout_38_warmelt_debug_DBGOUT_NAMEDOBJECT_METHOD);

  /*iniclos dclo_245__DBGOUT_NAMEDOBJECT_METHOD */
 /*_.VALCLO___V291*/ meltfptr[290] =
    (melt_ptr_t) & cdat->dclo_245__DBGOUT_NAMEDOBJECT_METHOD;
  cdat->dclo_245__DBGOUT_NAMEDOBJECT_METHOD.discr =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (DISCR_CLOSURE))));
  cdat->dclo_245__DBGOUT_NAMEDOBJECT_METHOD.nbval = 0;


/*iniobj dsym_246__DBGOUT_NAMEDOBJECT_METHOD*/
  /*uniqueobj*/ if (! /*_.VALDATA___V292*/ meltfptr[291])
/*_.VALDATA___V292*/ meltfptr[291] =
      (melt_ptr_t) & cdat->dsym_246__DBGOUT_NAMEDOBJECT_METHOD;
  if (MELT_LIKELY (!melt_prohibit_garbcoll))
    melt_assertmsg
      ("iniobj check.discr isobj dsym_246__DBGOUT_NAMEDOBJECT_METHOD",
       melt_magic_discr ((melt_ptr_t)
			 (((melt_ptr_t) (MELT_PREDEF (CLASS_SYMBOL))))) ==
       MELTOBMAG_OBJECT);
  if (MELT_LIKELY (!melt_prohibit_garbcoll))
    melt_assertmsg
      ("iniobj check.discr objmagic dsym_246__DBGOUT_NAMEDOBJECT_METHOD",
       ((meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (CLASS_SYMBOL)))))->
       meltobj_magic == MELTOBMAG_OBJECT);
  cdat->dsym_246__DBGOUT_NAMEDOBJECT_METHOD.meltobj_class =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (CLASS_SYMBOL))));
  cdat->dsym_246__DBGOUT_NAMEDOBJECT_METHOD.obj_hash = 359355803;
  cdat->dsym_246__DBGOUT_NAMEDOBJECT_METHOD.obj_len = 3;


/*inistring dstr_247__DBGOUT_NAMEDOBJE*/
 /*_.VALSTR___V293*/ meltfptr[292] =
    (melt_ptr_t) & cdat->dstr_247__DBGOUT_NAMEDOBJE;
  cdat->dstr_247__DBGOUT_NAMEDOBJE.discr =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (DISCR_STRING))));
  /*small inistring */ strncpy (cdat->dstr_247__DBGOUT_NAMEDOBJE.val,
				"DBGOUT_NAMEDOBJECT_METHOD",
				sizeof (cdat->dstr_247__DBGOUT_NAMEDOBJE.
					val) - 1);
  cdat->dstr_247__DBGOUT_NAMEDOBJE.val[25] = (char) 0;
  cdat->dstr_247__DBGOUT_NAMEDOBJE.slen = 25;

  /*inirout drout_248__DBGOUTAGAIN_NAMEDOBJECT_METHOD */
 /*_.VALROUT___V294*/ meltfptr[293] =
    (melt_ptr_t) & cdat->drout_248__DBGOUTAGAIN_NAMEDOBJECT_METHOD;
  cdat->drout_248__DBGOUTAGAIN_NAMEDOBJECT_METHOD.discr =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (DISCR_ROUTINE))));
  strncpy (cdat->drout_248__DBGOUTAGAIN_NAMEDOBJECT_METHOD.routdescr,
	   "DBGOUTAGAIN_NAMEDOBJECT_METHOD @warmelt-debug.melt:1301",
	   MELT_ROUTDESCR_LEN - 1);
  cdat->drout_248__DBGOUTAGAIN_NAMEDOBJECT_METHOD.nbval = 1;
  MELT_ROUTINE_SET_ROUTCODE (&cdat->drout_248__DBGOUTAGAIN_NAMEDOBJECT_METHOD,
			     meltrout_39_warmelt_debug_DBGOUTAGAIN_NAMEDOBJECT_METHOD);

  /*iniclos dclo_249__DBGOUTAGAIN_NAMEDOBJECT_METHOD */
 /*_.VALCLO___V295*/ meltfptr[294] =
    (melt_ptr_t) & cdat->dclo_249__DBGOUTAGAIN_NAMEDOBJECT_METHOD;
  cdat->dclo_249__DBGOUTAGAIN_NAMEDOBJECT_METHOD.discr =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (DISCR_CLOSURE))));
  cdat->dclo_249__DBGOUTAGAIN_NAMEDOBJECT_METHOD.nbval = 0;

  /*inirout drout_250__DBGOUTAGAIN_SYMBOL_METHOD */
 /*_.VALROUT___V296*/ meltfptr[295] =
    (melt_ptr_t) & cdat->drout_250__DBGOUTAGAIN_SYMBOL_METHOD;
  cdat->drout_250__DBGOUTAGAIN_SYMBOL_METHOD.discr =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (DISCR_ROUTINE))));
  strncpy (cdat->drout_250__DBGOUTAGAIN_SYMBOL_METHOD.routdescr,
	   "DBGOUTAGAIN_SYMBOL_METHOD @warmelt-debug.melt:1318",
	   MELT_ROUTDESCR_LEN - 1);
  cdat->drout_250__DBGOUTAGAIN_SYMBOL_METHOD.nbval = 2;
  MELT_ROUTINE_SET_ROUTCODE (&cdat->drout_250__DBGOUTAGAIN_SYMBOL_METHOD,
			     meltrout_40_warmelt_debug_DBGOUTAGAIN_SYMBOL_METHOD);

  /*iniclos dclo_251__DBGOUTAGAIN_SYMBOL_METHOD */
 /*_.VALCLO___V297*/ meltfptr[296] =
    (melt_ptr_t) & cdat->dclo_251__DBGOUTAGAIN_SYMBOL_METHOD;
  cdat->dclo_251__DBGOUTAGAIN_SYMBOL_METHOD.discr =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (DISCR_CLOSURE))));
  cdat->dclo_251__DBGOUTAGAIN_SYMBOL_METHOD.nbval = 0;


/*iniobj dsym_252__CLASS_SYMBOL*/
  /*uniqueobj*/ if (! /*_.VALDATA___V298*/ meltfptr[297])
/*_.VALDATA___V298*/ meltfptr[297] =
      (melt_ptr_t) & cdat->dsym_252__CLASS_SYMBOL;
  if (MELT_LIKELY (!melt_prohibit_garbcoll))
    melt_assertmsg ("iniobj check.discr isobj dsym_252__CLASS_SYMBOL",
		    melt_magic_discr ((melt_ptr_t)
				      (((melt_ptr_t)
					(MELT_PREDEF (CLASS_SYMBOL))))) ==
		    MELTOBMAG_OBJECT);
  if (MELT_LIKELY (!melt_prohibit_garbcoll))
    melt_assertmsg ("iniobj check.discr objmagic dsym_252__CLASS_SYMBOL",
		    ((meltobject_ptr_t)
		     (((melt_ptr_t) (MELT_PREDEF (CLASS_SYMBOL)))))->
		    meltobj_magic == MELTOBMAG_OBJECT);
  cdat->dsym_252__CLASS_SYMBOL.meltobj_class =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (CLASS_SYMBOL))));
  cdat->dsym_252__CLASS_SYMBOL.obj_hash = 53524182;
  cdat->dsym_252__CLASS_SYMBOL.obj_len = 3;


/*inistring dstr_253__CLASS_SYMBOL*/
 /*_.VALSTR___V299*/ meltfptr[298] =
    (melt_ptr_t) & cdat->dstr_253__CLASS_SYMBOL;
  cdat->dstr_253__CLASS_SYMBOL.discr =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (DISCR_STRING))));
  /*small inistring */ strncpy (cdat->dstr_253__CLASS_SYMBOL.val,
				"CLASS_SYMBOL",
				sizeof (cdat->dstr_253__CLASS_SYMBOL.val) -
				1);
  cdat->dstr_253__CLASS_SYMBOL.val[12] = (char) 0;
  cdat->dstr_253__CLASS_SYMBOL.slen = 12;

  /*inirout drout_254__DBGOUT_SYMBOL_METHOD */
 /*_.VALROUT___V300*/ meltfptr[299] =
    (melt_ptr_t) & cdat->drout_254__DBGOUT_SYMBOL_METHOD;
  cdat->drout_254__DBGOUT_SYMBOL_METHOD.discr =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (DISCR_ROUTINE))));
  strncpy (cdat->drout_254__DBGOUT_SYMBOL_METHOD.routdescr,
	   "DBGOUT_SYMBOL_METHOD @warmelt-debug.melt:1336",
	   MELT_ROUTDESCR_LEN - 1);
  cdat->drout_254__DBGOUT_SYMBOL_METHOD.nbval = 4;
  MELT_ROUTINE_SET_ROUTCODE (&cdat->drout_254__DBGOUT_SYMBOL_METHOD,
			     meltrout_41_warmelt_debug_DBGOUT_SYMBOL_METHOD);

  /*iniclos dclo_255__DBGOUT_SYMBOL_METHOD */
 /*_.VALCLO___V301*/ meltfptr[300] =
    (melt_ptr_t) & cdat->dclo_255__DBGOUT_SYMBOL_METHOD;
  cdat->dclo_255__DBGOUT_SYMBOL_METHOD.discr =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (DISCR_CLOSURE))));
  cdat->dclo_255__DBGOUT_SYMBOL_METHOD.nbval = 0;

  /*inirout drout_256__DBGOUT_CLASS_METHOD */
 /*_.VALROUT___V302*/ meltfptr[301] =
    (melt_ptr_t) & cdat->drout_256__DBGOUT_CLASS_METHOD;
  cdat->drout_256__DBGOUT_CLASS_METHOD.discr =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (DISCR_ROUTINE))));
  strncpy (cdat->drout_256__DBGOUT_CLASS_METHOD.routdescr,
	   "DBGOUT_CLASS_METHOD @warmelt-debug.melt:1346",
	   MELT_ROUTDESCR_LEN - 1);
  cdat->drout_256__DBGOUT_CLASS_METHOD.nbval = 4;
  MELT_ROUTINE_SET_ROUTCODE (&cdat->drout_256__DBGOUT_CLASS_METHOD,
			     meltrout_42_warmelt_debug_DBGOUT_CLASS_METHOD);

  /*iniclos dclo_257__DBGOUT_CLASS_METHOD */
 /*_.VALCLO___V303*/ meltfptr[302] =
    (melt_ptr_t) & cdat->dclo_257__DBGOUT_CLASS_METHOD;
  cdat->dclo_257__DBGOUT_CLASS_METHOD.discr =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (DISCR_CLOSURE))));
  cdat->dclo_257__DBGOUT_CLASS_METHOD.nbval = 0;


/*iniobj dsym_258__CLASS_CLASS*/
  /*uniqueobj*/ if (! /*_.VALDATA___V304*/ meltfptr[303])
/*_.VALDATA___V304*/ meltfptr[303] =
      (melt_ptr_t) & cdat->dsym_258__CLASS_CLASS;
  if (MELT_LIKELY (!melt_prohibit_garbcoll))
    melt_assertmsg ("iniobj check.discr isobj dsym_258__CLASS_CLASS",
		    melt_magic_discr ((melt_ptr_t)
				      (((melt_ptr_t)
					(MELT_PREDEF (CLASS_SYMBOL))))) ==
		    MELTOBMAG_OBJECT);
  if (MELT_LIKELY (!melt_prohibit_garbcoll))
    melt_assertmsg ("iniobj check.discr objmagic dsym_258__CLASS_CLASS",
		    ((meltobject_ptr_t)
		     (((melt_ptr_t) (MELT_PREDEF (CLASS_SYMBOL)))))->
		    meltobj_magic == MELTOBMAG_OBJECT);
  cdat->dsym_258__CLASS_CLASS.meltobj_class =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (CLASS_SYMBOL))));
  cdat->dsym_258__CLASS_CLASS.obj_hash = 60141292;
  cdat->dsym_258__CLASS_CLASS.obj_len = 3;


/*inistring dstr_259__CLASS_CLASS*/
 /*_.VALSTR___V305*/ meltfptr[304] =
    (melt_ptr_t) & cdat->dstr_259__CLASS_CLASS;
  cdat->dstr_259__CLASS_CLASS.discr =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (DISCR_STRING))));
  /*small inistring */ strncpy (cdat->dstr_259__CLASS_CLASS.val,
				"CLASS_CLASS",
				sizeof (cdat->dstr_259__CLASS_CLASS.val) - 1);
  cdat->dstr_259__CLASS_CLASS.val[11] = (char) 0;
  cdat->dstr_259__CLASS_CLASS.slen = 11;

  /*inirout drout_260__DBGOUTAGAIN_KEYWORD_METHOD */
 /*_.VALROUT___V306*/ meltfptr[305] =
    (melt_ptr_t) & cdat->drout_260__DBGOUTAGAIN_KEYWORD_METHOD;
  cdat->drout_260__DBGOUTAGAIN_KEYWORD_METHOD.discr =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (DISCR_ROUTINE))));
  strncpy (cdat->drout_260__DBGOUTAGAIN_KEYWORD_METHOD.routdescr,
	   "DBGOUTAGAIN_KEYWORD_METHOD @warmelt-debug.melt:1356",
	   MELT_ROUTDESCR_LEN - 1);
  cdat->drout_260__DBGOUTAGAIN_KEYWORD_METHOD.nbval = 2;
  MELT_ROUTINE_SET_ROUTCODE (&cdat->drout_260__DBGOUTAGAIN_KEYWORD_METHOD,
			     meltrout_43_warmelt_debug_DBGOUTAGAIN_KEYWORD_METHOD);

  /*iniclos dclo_261__DBGOUTAGAIN_KEYWORD_METHOD */
 /*_.VALCLO___V307*/ meltfptr[306] =
    (melt_ptr_t) & cdat->dclo_261__DBGOUTAGAIN_KEYWORD_METHOD;
  cdat->dclo_261__DBGOUTAGAIN_KEYWORD_METHOD.discr =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (DISCR_CLOSURE))));
  cdat->dclo_261__DBGOUTAGAIN_KEYWORD_METHOD.nbval = 0;


/*iniobj dsym_262__CLASS_KEYWORD*/
  /*uniqueobj*/ if (! /*_.VALDATA___V308*/ meltfptr[307])
/*_.VALDATA___V308*/ meltfptr[307] =
      (melt_ptr_t) & cdat->dsym_262__CLASS_KEYWORD;
  if (MELT_LIKELY (!melt_prohibit_garbcoll))
    melt_assertmsg ("iniobj check.discr isobj dsym_262__CLASS_KEYWORD",
		    melt_magic_discr ((melt_ptr_t)
				      (((melt_ptr_t)
					(MELT_PREDEF (CLASS_SYMBOL))))) ==
		    MELTOBMAG_OBJECT);
  if (MELT_LIKELY (!melt_prohibit_garbcoll))
    melt_assertmsg ("iniobj check.discr objmagic dsym_262__CLASS_KEYWORD",
		    ((meltobject_ptr_t)
		     (((melt_ptr_t) (MELT_PREDEF (CLASS_SYMBOL)))))->
		    meltobj_magic == MELTOBMAG_OBJECT);
  cdat->dsym_262__CLASS_KEYWORD.meltobj_class =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (CLASS_SYMBOL))));
  cdat->dsym_262__CLASS_KEYWORD.obj_hash = 14992133;
  cdat->dsym_262__CLASS_KEYWORD.obj_len = 3;


/*inistring dstr_263__CLASS_KEYWORD*/
 /*_.VALSTR___V309*/ meltfptr[308] =
    (melt_ptr_t) & cdat->dstr_263__CLASS_KEYWORD;
  cdat->dstr_263__CLASS_KEYWORD.discr =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (DISCR_STRING))));
  /*small inistring */ strncpy (cdat->dstr_263__CLASS_KEYWORD.val,
				"CLASS_KEYWORD",
				sizeof (cdat->dstr_263__CLASS_KEYWORD.val) -
				1);
  cdat->dstr_263__CLASS_KEYWORD.val[13] = (char) 0;
  cdat->dstr_263__CLASS_KEYWORD.slen = 13;

  /*inirout drout_264__DBGOUT_KEYWORD_METHOD */
 /*_.VALROUT___V310*/ meltfptr[309] =
    (melt_ptr_t) & cdat->drout_264__DBGOUT_KEYWORD_METHOD;
  cdat->drout_264__DBGOUT_KEYWORD_METHOD.discr =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (DISCR_ROUTINE))));
  strncpy (cdat->drout_264__DBGOUT_KEYWORD_METHOD.routdescr,
	   "DBGOUT_KEYWORD_METHOD @warmelt-debug.melt:1369",
	   MELT_ROUTDESCR_LEN - 1);
  cdat->drout_264__DBGOUT_KEYWORD_METHOD.nbval = 4;
  MELT_ROUTINE_SET_ROUTCODE (&cdat->drout_264__DBGOUT_KEYWORD_METHOD,
			     meltrout_44_warmelt_debug_DBGOUT_KEYWORD_METHOD);

  /*iniclos dclo_265__DBGOUT_KEYWORD_METHOD */
 /*_.VALCLO___V311*/ meltfptr[310] =
    (melt_ptr_t) & cdat->dclo_265__DBGOUT_KEYWORD_METHOD;
  cdat->dclo_265__DBGOUT_KEYWORD_METHOD.discr =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (DISCR_CLOSURE))));
  cdat->dclo_265__DBGOUT_KEYWORD_METHOD.nbval = 0;

  /*inirout drout_266__DBGOUTAGAIN_CLONED_SYMBOL_METHOD */
 /*_.VALROUT___V312*/ meltfptr[311] =
    (melt_ptr_t) & cdat->drout_266__DBGOUTAGAIN_CLONED_SYMBOL_METHOD;
  cdat->drout_266__DBGOUTAGAIN_CLONED_SYMBOL_METHOD.discr =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (DISCR_ROUTINE))));
  strncpy (cdat->drout_266__DBGOUTAGAIN_CLONED_SYMBOL_METHOD.routdescr,
	   "DBGOUTAGAIN_CLONED_SYMBOL_METHOD @warmelt-debug.melt:1379",
	   MELT_ROUTDESCR_LEN - 1);
  cdat->drout_266__DBGOUTAGAIN_CLONED_SYMBOL_METHOD.nbval = 0;
  MELT_ROUTINE_SET_ROUTCODE (&cdat->
			     drout_266__DBGOUTAGAIN_CLONED_SYMBOL_METHOD,
			     meltrout_45_warmelt_debug_DBGOUTAGAIN_CLONED_SYMBOL_METHOD);

  /*iniclos dclo_267__DBGOUTAGAIN_CLONED_SYMBOL_METHOD */
 /*_.VALCLO___V313*/ meltfptr[312] =
    (melt_ptr_t) & cdat->dclo_267__DBGOUTAGAIN_CLONED_SYMBOL_METHOD;
  cdat->dclo_267__DBGOUTAGAIN_CLONED_SYMBOL_METHOD.discr =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (DISCR_CLOSURE))));
  cdat->dclo_267__DBGOUTAGAIN_CLONED_SYMBOL_METHOD.nbval = 0;


/*iniobj dsym_268__CLASS_CLONED_SYMBOL*/
  /*uniqueobj*/ if (! /*_.VALDATA___V314*/ meltfptr[313])
/*_.VALDATA___V314*/ meltfptr[313] =
      (melt_ptr_t) & cdat->dsym_268__CLASS_CLONED_SYMBOL;
  if (MELT_LIKELY (!melt_prohibit_garbcoll))
    melt_assertmsg ("iniobj check.discr isobj dsym_268__CLASS_CLONED_SYMBOL",
		    melt_magic_discr ((melt_ptr_t)
				      (((melt_ptr_t)
					(MELT_PREDEF (CLASS_SYMBOL))))) ==
		    MELTOBMAG_OBJECT);
  if (MELT_LIKELY (!melt_prohibit_garbcoll))
    melt_assertmsg
      ("iniobj check.discr objmagic dsym_268__CLASS_CLONED_SYMBOL",
       ((meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (CLASS_SYMBOL)))))->
       meltobj_magic == MELTOBMAG_OBJECT);
  cdat->dsym_268__CLASS_CLONED_SYMBOL.meltobj_class =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (CLASS_SYMBOL))));
  cdat->dsym_268__CLASS_CLONED_SYMBOL.obj_hash = 933549728;
  cdat->dsym_268__CLASS_CLONED_SYMBOL.obj_len = 3;


/*inistring dstr_269__CLASS_CLONED_SYM*/
 /*_.VALSTR___V315*/ meltfptr[314] =
    (melt_ptr_t) & cdat->dstr_269__CLASS_CLONED_SYM;
  cdat->dstr_269__CLASS_CLONED_SYM.discr =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (DISCR_STRING))));
  /*small inistring */ strncpy (cdat->dstr_269__CLASS_CLONED_SYM.val,
				"CLASS_CLONED_SYMBOL",
				sizeof (cdat->dstr_269__CLASS_CLONED_SYM.
					val) - 1);
  cdat->dstr_269__CLASS_CLONED_SYM.val[19] = (char) 0;
  cdat->dstr_269__CLASS_CLONED_SYM.slen = 19;

  /*inirout drout_270__DBGOUT_CLONED_SYMBOL_METHOD */
 /*_.VALROUT___V316*/ meltfptr[315] =
    (melt_ptr_t) & cdat->drout_270__DBGOUT_CLONED_SYMBOL_METHOD;
  cdat->drout_270__DBGOUT_CLONED_SYMBOL_METHOD.discr =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (DISCR_ROUTINE))));
  strncpy (cdat->drout_270__DBGOUT_CLONED_SYMBOL_METHOD.routdescr,
	   "DBGOUT_CLONED_SYMBOL_METHOD @warmelt-debug.melt:1394",
	   MELT_ROUTDESCR_LEN - 1);
  cdat->drout_270__DBGOUT_CLONED_SYMBOL_METHOD.nbval = 4;
  MELT_ROUTINE_SET_ROUTCODE (&cdat->drout_270__DBGOUT_CLONED_SYMBOL_METHOD,
			     meltrout_46_warmelt_debug_DBGOUT_CLONED_SYMBOL_METHOD);

  /*iniclos dclo_271__DBGOUT_CLONED_SYMBOL_METHOD */
 /*_.VALCLO___V317*/ meltfptr[316] =
    (melt_ptr_t) & cdat->dclo_271__DBGOUT_CLONED_SYMBOL_METHOD;
  cdat->dclo_271__DBGOUT_CLONED_SYMBOL_METHOD.discr =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (DISCR_CLOSURE))));
  cdat->dclo_271__DBGOUT_CLONED_SYMBOL_METHOD.nbval = 0;

  /*inirout drout_272__DBGOUT_ENVIRONMENT_METHOD */
 /*_.VALROUT___V318*/ meltfptr[317] =
    (melt_ptr_t) & cdat->drout_272__DBGOUT_ENVIRONMENT_METHOD;
  cdat->drout_272__DBGOUT_ENVIRONMENT_METHOD.discr =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (DISCR_ROUTINE))));
  strncpy (cdat->drout_272__DBGOUT_ENVIRONMENT_METHOD.routdescr,
	   "DBGOUT_ENVIRONMENT_METHOD @warmelt-debug.melt:1405",
	   MELT_ROUTDESCR_LEN - 1);
  cdat->drout_272__DBGOUT_ENVIRONMENT_METHOD.nbval = 6;
  MELT_ROUTINE_SET_ROUTCODE (&cdat->drout_272__DBGOUT_ENVIRONMENT_METHOD,
			     meltrout_47_warmelt_debug_DBGOUT_ENVIRONMENT_METHOD);

  /*iniclos dclo_273__DBGOUT_ENVIRONMENT_METHOD */
 /*_.VALCLO___V319*/ meltfptr[318] =
    (melt_ptr_t) & cdat->dclo_273__DBGOUT_ENVIRONMENT_METHOD;
  cdat->dclo_273__DBGOUT_ENVIRONMENT_METHOD.discr =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (DISCR_CLOSURE))));
  cdat->dclo_273__DBGOUT_ENVIRONMENT_METHOD.nbval = 0;


/*iniobj dsym_274__CLASS_ENVIRONMENT*/
  /*uniqueobj*/ if (! /*_.VALDATA___V320*/ meltfptr[319])
/*_.VALDATA___V320*/ meltfptr[319] =
      (melt_ptr_t) & cdat->dsym_274__CLASS_ENVIRONMENT;
  if (MELT_LIKELY (!melt_prohibit_garbcoll))
    melt_assertmsg ("iniobj check.discr isobj dsym_274__CLASS_ENVIRONMENT",
		    melt_magic_discr ((melt_ptr_t)
				      (((melt_ptr_t)
					(MELT_PREDEF (CLASS_SYMBOL))))) ==
		    MELTOBMAG_OBJECT);
  if (MELT_LIKELY (!melt_prohibit_garbcoll))
    melt_assertmsg ("iniobj check.discr objmagic dsym_274__CLASS_ENVIRONMENT",
		    ((meltobject_ptr_t)
		     (((melt_ptr_t) (MELT_PREDEF (CLASS_SYMBOL)))))->
		    meltobj_magic == MELTOBMAG_OBJECT);
  cdat->dsym_274__CLASS_ENVIRONMENT.meltobj_class =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (CLASS_SYMBOL))));
  cdat->dsym_274__CLASS_ENVIRONMENT.obj_hash = 59212821;
  cdat->dsym_274__CLASS_ENVIRONMENT.obj_len = 3;


/*inistring dstr_275__CLASS_ENVIRONMEN*/
 /*_.VALSTR___V321*/ meltfptr[320] =
    (melt_ptr_t) & cdat->dstr_275__CLASS_ENVIRONMEN;
  cdat->dstr_275__CLASS_ENVIRONMEN.discr =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (DISCR_STRING))));
  /*small inistring */ strncpy (cdat->dstr_275__CLASS_ENVIRONMEN.val,
				"CLASS_ENVIRONMENT",
				sizeof (cdat->dstr_275__CLASS_ENVIRONMEN.
					val) - 1);
  cdat->dstr_275__CLASS_ENVIRONMEN.val[17] = (char) 0;
  cdat->dstr_275__CLASS_ENVIRONMEN.slen = 17;


/*iniobj dsym_276__ENV_PREV*/
  /*uniqueobj*/ if (! /*_.VALDATA___V322*/ meltfptr[321])
/*_.VALDATA___V322*/ meltfptr[321] =
      (melt_ptr_t) & cdat->dsym_276__ENV_PREV;
  if (MELT_LIKELY (!melt_prohibit_garbcoll))
    melt_assertmsg ("iniobj check.discr isobj dsym_276__ENV_PREV",
		    melt_magic_discr ((melt_ptr_t)
				      (((melt_ptr_t)
					(MELT_PREDEF (CLASS_SYMBOL))))) ==
		    MELTOBMAG_OBJECT);
  if (MELT_LIKELY (!melt_prohibit_garbcoll))
    melt_assertmsg ("iniobj check.discr objmagic dsym_276__ENV_PREV",
		    ((meltobject_ptr_t)
		     (((melt_ptr_t) (MELT_PREDEF (CLASS_SYMBOL)))))->
		    meltobj_magic == MELTOBMAG_OBJECT);
  cdat->dsym_276__ENV_PREV.meltobj_class =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (CLASS_SYMBOL))));
  cdat->dsym_276__ENV_PREV.obj_hash = 363634370;
  cdat->dsym_276__ENV_PREV.obj_len = 3;


/*inistring dstr_277__ENV_PREV*/
 /*_.VALSTR___V323*/ meltfptr[322] = (melt_ptr_t) & cdat->dstr_277__ENV_PREV;
  cdat->dstr_277__ENV_PREV.discr =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (DISCR_STRING))));
  /*small inistring */ strncpy (cdat->dstr_277__ENV_PREV.val, "ENV_PREV",
				sizeof (cdat->dstr_277__ENV_PREV.val) - 1);
  cdat->dstr_277__ENV_PREV.val[8] = (char) 0;
  cdat->dstr_277__ENV_PREV.slen = 8;

  /*inirout drout_278__DBGOUTAGAIN_CTYPE_METHOD */
 /*_.VALROUT___V324*/ meltfptr[323] =
    (melt_ptr_t) & cdat->drout_278__DBGOUTAGAIN_CTYPE_METHOD;
  cdat->drout_278__DBGOUTAGAIN_CTYPE_METHOD.discr =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (DISCR_ROUTINE))));
  strncpy (cdat->drout_278__DBGOUTAGAIN_CTYPE_METHOD.routdescr,
	   "DBGOUTAGAIN_CTYPE_METHOD @warmelt-debug.melt:1450",
	   MELT_ROUTDESCR_LEN - 1);
  cdat->drout_278__DBGOUTAGAIN_CTYPE_METHOD.nbval = 0;
  MELT_ROUTINE_SET_ROUTCODE (&cdat->drout_278__DBGOUTAGAIN_CTYPE_METHOD,
			     meltrout_48_warmelt_debug_DBGOUTAGAIN_CTYPE_METHOD);

  /*iniclos dclo_279__DBGOUTAGAIN_CTYPE_METHOD */
 /*_.VALCLO___V325*/ meltfptr[324] =
    (melt_ptr_t) & cdat->dclo_279__DBGOUTAGAIN_CTYPE_METHOD;
  cdat->dclo_279__DBGOUTAGAIN_CTYPE_METHOD.discr =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (DISCR_CLOSURE))));
  cdat->dclo_279__DBGOUTAGAIN_CTYPE_METHOD.nbval = 0;

  /*inirout drout_280__DBGOUT_CTYPE_METHOD */
 /*_.VALROUT___V326*/ meltfptr[325] =
    (melt_ptr_t) & cdat->drout_280__DBGOUT_CTYPE_METHOD;
  cdat->drout_280__DBGOUT_CTYPE_METHOD.discr =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (DISCR_ROUTINE))));
  strncpy (cdat->drout_280__DBGOUT_CTYPE_METHOD.routdescr,
	   "DBGOUT_CTYPE_METHOD @warmelt-debug.melt:1462",
	   MELT_ROUTDESCR_LEN - 1);
  cdat->drout_280__DBGOUT_CTYPE_METHOD.nbval = 4;
  MELT_ROUTINE_SET_ROUTCODE (&cdat->drout_280__DBGOUT_CTYPE_METHOD,
			     meltrout_49_warmelt_debug_DBGOUT_CTYPE_METHOD);

  /*iniclos dclo_281__DBGOUT_CTYPE_METHOD */
 /*_.VALCLO___V327*/ meltfptr[326] =
    (melt_ptr_t) & cdat->dclo_281__DBGOUT_CTYPE_METHOD;
  cdat->dclo_281__DBGOUT_CTYPE_METHOD.discr =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (DISCR_CLOSURE))));
  cdat->dclo_281__DBGOUT_CTYPE_METHOD.nbval = 0;

  /*inirout drout_282__DBGOUT_ANYBINDING_METHOD */
 /*_.VALROUT___V328*/ meltfptr[327] =
    (melt_ptr_t) & cdat->drout_282__DBGOUT_ANYBINDING_METHOD;
  cdat->drout_282__DBGOUT_ANYBINDING_METHOD.discr =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (DISCR_ROUTINE))));
  strncpy (cdat->drout_282__DBGOUT_ANYBINDING_METHOD.routdescr,
	   "DBGOUT_ANYBINDING_METHOD @warmelt-debug.melt:1472",
	   MELT_ROUTDESCR_LEN - 1);
  cdat->drout_282__DBGOUT_ANYBINDING_METHOD.nbval = 5;
  MELT_ROUTINE_SET_ROUTCODE (&cdat->drout_282__DBGOUT_ANYBINDING_METHOD,
			     meltrout_50_warmelt_debug_DBGOUT_ANYBINDING_METHOD);

  /*iniclos dclo_283__DBGOUT_ANYBINDING_METHOD */
 /*_.VALCLO___V329*/ meltfptr[328] =
    (melt_ptr_t) & cdat->dclo_283__DBGOUT_ANYBINDING_METHOD;
  cdat->dclo_283__DBGOUT_ANYBINDING_METHOD.discr =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (DISCR_CLOSURE))));
  cdat->dclo_283__DBGOUT_ANYBINDING_METHOD.nbval = 0;


/*iniobj dsym_284__CLASS_ANY_BINDING*/
  /*uniqueobj*/ if (! /*_.VALDATA___V330*/ meltfptr[329])
/*_.VALDATA___V330*/ meltfptr[329] =
      (melt_ptr_t) & cdat->dsym_284__CLASS_ANY_BINDING;
  if (MELT_LIKELY (!melt_prohibit_garbcoll))
    melt_assertmsg ("iniobj check.discr isobj dsym_284__CLASS_ANY_BINDING",
		    melt_magic_discr ((melt_ptr_t)
				      (((melt_ptr_t)
					(MELT_PREDEF (CLASS_SYMBOL))))) ==
		    MELTOBMAG_OBJECT);
  if (MELT_LIKELY (!melt_prohibit_garbcoll))
    melt_assertmsg ("iniobj check.discr objmagic dsym_284__CLASS_ANY_BINDING",
		    ((meltobject_ptr_t)
		     (((melt_ptr_t) (MELT_PREDEF (CLASS_SYMBOL)))))->
		    meltobj_magic == MELTOBMAG_OBJECT);
  cdat->dsym_284__CLASS_ANY_BINDING.meltobj_class =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (CLASS_SYMBOL))));
  cdat->dsym_284__CLASS_ANY_BINDING.obj_hash = 935770;
  cdat->dsym_284__CLASS_ANY_BINDING.obj_len = 3;


/*inistring dstr_285__CLASS_ANY_BINDIN*/
 /*_.VALSTR___V331*/ meltfptr[330] =
    (melt_ptr_t) & cdat->dstr_285__CLASS_ANY_BINDIN;
  cdat->dstr_285__CLASS_ANY_BINDIN.discr =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (DISCR_STRING))));
  /*small inistring */ strncpy (cdat->dstr_285__CLASS_ANY_BINDIN.val,
				"CLASS_ANY_BINDING",
				sizeof (cdat->dstr_285__CLASS_ANY_BINDIN.
					val) - 1);
  cdat->dstr_285__CLASS_ANY_BINDIN.val[17] = (char) 0;
  cdat->dstr_285__CLASS_ANY_BINDIN.slen = 17;

  /*inirout drout_286__DISPLAY_DEBUG_MESSAGE */
 /*_.VALROUT___V332*/ meltfptr[331] =
    (melt_ptr_t) & cdat->drout_286__DISPLAY_DEBUG_MESSAGE;
  cdat->drout_286__DISPLAY_DEBUG_MESSAGE.discr =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (DISCR_ROUTINE))));
  strncpy (cdat->drout_286__DISPLAY_DEBUG_MESSAGE.routdescr,
	   "DISPLAY_DEBUG_MESSAGE @warmelt-debug.melt:1509",
	   MELT_ROUTDESCR_LEN - 1);
  cdat->drout_286__DISPLAY_DEBUG_MESSAGE.nbval = 5;
  MELT_ROUTINE_SET_ROUTCODE (&cdat->drout_286__DISPLAY_DEBUG_MESSAGE,
			     meltrout_51_warmelt_debug_DISPLAY_DEBUG_MESSAGE);

  /*iniclos dclo_287__DISPLAY_DEBUG_MESSAGE */
 /*_.VALCLO___V333*/ meltfptr[332] =
    (melt_ptr_t) & cdat->dclo_287__DISPLAY_DEBUG_MESSAGE;
  cdat->dclo_287__DISPLAY_DEBUG_MESSAGE.discr =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (DISCR_CLOSURE))));
  cdat->dclo_287__DISPLAY_DEBUG_MESSAGE.nbval = 0;


/*iniobj dsym_288__DBG_OUT*/
  /*uniqueobj*/ if (! /*_.VALDATA___V334*/ meltfptr[333])
/*_.VALDATA___V334*/ meltfptr[333] =
      (melt_ptr_t) & cdat->dsym_288__DBG_OUT;
  if (MELT_LIKELY (!melt_prohibit_garbcoll))
    melt_assertmsg ("iniobj check.discr isobj dsym_288__DBG_OUT",
		    melt_magic_discr ((melt_ptr_t)
				      (((melt_ptr_t)
					(MELT_PREDEF (CLASS_SYMBOL))))) ==
		    MELTOBMAG_OBJECT);
  if (MELT_LIKELY (!melt_prohibit_garbcoll))
    melt_assertmsg ("iniobj check.discr objmagic dsym_288__DBG_OUT",
		    ((meltobject_ptr_t)
		     (((melt_ptr_t) (MELT_PREDEF (CLASS_SYMBOL)))))->
		    meltobj_magic == MELTOBMAG_OBJECT);
  cdat->dsym_288__DBG_OUT.meltobj_class =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (CLASS_SYMBOL))));
  cdat->dsym_288__DBG_OUT.obj_hash = 492035674;
  cdat->dsym_288__DBG_OUT.obj_len = 3;


/*inistring dstr_289__DBG_OUT*/
 /*_.VALSTR___V335*/ meltfptr[334] = (melt_ptr_t) & cdat->dstr_289__DBG_OUT;
  cdat->dstr_289__DBG_OUT.discr =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (DISCR_STRING))));
  /*small inistring */ strncpy (cdat->dstr_289__DBG_OUT.val, "DBG_OUT",
				sizeof (cdat->dstr_289__DBG_OUT.val) - 1);
  cdat->dstr_289__DBG_OUT.val[7] = (char) 0;
  cdat->dstr_289__DBG_OUT.slen = 7;


/*iniobj dsym_290__DBG_OUTOBJECT*/
  /*uniqueobj*/ if (! /*_.VALDATA___V336*/ meltfptr[335])
/*_.VALDATA___V336*/ meltfptr[335] =
      (melt_ptr_t) & cdat->dsym_290__DBG_OUTOBJECT;
  if (MELT_LIKELY (!melt_prohibit_garbcoll))
    melt_assertmsg ("iniobj check.discr isobj dsym_290__DBG_OUTOBJECT",
		    melt_magic_discr ((melt_ptr_t)
				      (((melt_ptr_t)
					(MELT_PREDEF (CLASS_SYMBOL))))) ==
		    MELTOBMAG_OBJECT);
  if (MELT_LIKELY (!melt_prohibit_garbcoll))
    melt_assertmsg ("iniobj check.discr objmagic dsym_290__DBG_OUTOBJECT",
		    ((meltobject_ptr_t)
		     (((melt_ptr_t) (MELT_PREDEF (CLASS_SYMBOL)))))->
		    meltobj_magic == MELTOBMAG_OBJECT);
  cdat->dsym_290__DBG_OUTOBJECT.meltobj_class =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (CLASS_SYMBOL))));
  cdat->dsym_290__DBG_OUTOBJECT.obj_hash = 723698673;
  cdat->dsym_290__DBG_OUTOBJECT.obj_len = 3;


/*inistring dstr_291__DBG_OUTOBJECT*/
 /*_.VALSTR___V337*/ meltfptr[336] =
    (melt_ptr_t) & cdat->dstr_291__DBG_OUTOBJECT;
  cdat->dstr_291__DBG_OUTOBJECT.discr =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (DISCR_STRING))));
  /*small inistring */ strncpy (cdat->dstr_291__DBG_OUTOBJECT.val,
				"DBG_OUTOBJECT",
				sizeof (cdat->dstr_291__DBG_OUTOBJECT.val) -
				1);
  cdat->dstr_291__DBG_OUTOBJECT.val[13] = (char) 0;
  cdat->dstr_291__DBG_OUTOBJECT.slen = 13;


/*iniobj dsym_292__DBGOUT_FIELDS*/
  /*uniqueobj*/ if (! /*_.VALDATA___V338*/ meltfptr[337])
/*_.VALDATA___V338*/ meltfptr[337] =
      (melt_ptr_t) & cdat->dsym_292__DBGOUT_FIELDS;
  if (MELT_LIKELY (!melt_prohibit_garbcoll))
    melt_assertmsg ("iniobj check.discr isobj dsym_292__DBGOUT_FIELDS",
		    melt_magic_discr ((melt_ptr_t)
				      (((melt_ptr_t)
					(MELT_PREDEF (CLASS_SYMBOL))))) ==
		    MELTOBMAG_OBJECT);
  if (MELT_LIKELY (!melt_prohibit_garbcoll))
    melt_assertmsg ("iniobj check.discr objmagic dsym_292__DBGOUT_FIELDS",
		    ((meltobject_ptr_t)
		     (((melt_ptr_t) (MELT_PREDEF (CLASS_SYMBOL)))))->
		    meltobj_magic == MELTOBMAG_OBJECT);
  cdat->dsym_292__DBGOUT_FIELDS.meltobj_class =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (CLASS_SYMBOL))));
  cdat->dsym_292__DBGOUT_FIELDS.obj_hash = 795625446;
  cdat->dsym_292__DBGOUT_FIELDS.obj_len = 3;


/*inistring dstr_293__DBGOUT_FIELDS*/
 /*_.VALSTR___V339*/ meltfptr[338] =
    (melt_ptr_t) & cdat->dstr_293__DBGOUT_FIELDS;
  cdat->dstr_293__DBGOUT_FIELDS.discr =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (DISCR_STRING))));
  /*small inistring */ strncpy (cdat->dstr_293__DBGOUT_FIELDS.val,
				"DBGOUT_FIELDS",
				sizeof (cdat->dstr_293__DBGOUT_FIELDS.val) -
				1);
  cdat->dstr_293__DBGOUT_FIELDS.val[13] = (char) 0;
  cdat->dstr_293__DBGOUT_FIELDS.slen = 13;


/*iniobj dsym_294__DBGOUTAGAIN_FIELDS*/
  /*uniqueobj*/ if (! /*_.VALDATA___V340*/ meltfptr[339])
/*_.VALDATA___V340*/ meltfptr[339] =
      (melt_ptr_t) & cdat->dsym_294__DBGOUTAGAIN_FIELDS;
  if (MELT_LIKELY (!melt_prohibit_garbcoll))
    melt_assertmsg ("iniobj check.discr isobj dsym_294__DBGOUTAGAIN_FIELDS",
		    melt_magic_discr ((melt_ptr_t)
				      (((melt_ptr_t)
					(MELT_PREDEF (CLASS_SYMBOL))))) ==
		    MELTOBMAG_OBJECT);
  if (MELT_LIKELY (!melt_prohibit_garbcoll))
    melt_assertmsg
      ("iniobj check.discr objmagic dsym_294__DBGOUTAGAIN_FIELDS",
       ((meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (CLASS_SYMBOL)))))->
       meltobj_magic == MELTOBMAG_OBJECT);
  cdat->dsym_294__DBGOUTAGAIN_FIELDS.meltobj_class =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (CLASS_SYMBOL))));
  cdat->dsym_294__DBGOUTAGAIN_FIELDS.obj_hash = 961773165;
  cdat->dsym_294__DBGOUTAGAIN_FIELDS.obj_len = 3;


/*inistring dstr_295__DBGOUTAGAIN_FIEL*/
 /*_.VALSTR___V341*/ meltfptr[340] =
    (melt_ptr_t) & cdat->dstr_295__DBGOUTAGAIN_FIEL;
  cdat->dstr_295__DBGOUTAGAIN_FIEL.discr =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (DISCR_STRING))));
  /*small inistring */ strncpy (cdat->dstr_295__DBGOUTAGAIN_FIEL.val,
				"DBGOUTAGAIN_FIELDS",
				sizeof (cdat->dstr_295__DBGOUTAGAIN_FIEL.
					val) - 1);
  cdat->dstr_295__DBGOUTAGAIN_FIEL.val[18] = (char) 0;
  cdat->dstr_295__DBGOUTAGAIN_FIEL.slen = 18;


/*iniobj dsym_296__DEBUG_MSG_FUN*/
  /*uniqueobj*/ if (! /*_.VALDATA___V342*/ meltfptr[341])
/*_.VALDATA___V342*/ meltfptr[341] =
      (melt_ptr_t) & cdat->dsym_296__DEBUG_MSG_FUN;
  if (MELT_LIKELY (!melt_prohibit_garbcoll))
    melt_assertmsg ("iniobj check.discr isobj dsym_296__DEBUG_MSG_FUN",
		    melt_magic_discr ((melt_ptr_t)
				      (((melt_ptr_t)
					(MELT_PREDEF (CLASS_SYMBOL))))) ==
		    MELTOBMAG_OBJECT);
  if (MELT_LIKELY (!melt_prohibit_garbcoll))
    melt_assertmsg ("iniobj check.discr objmagic dsym_296__DEBUG_MSG_FUN",
		    ((meltobject_ptr_t)
		     (((melt_ptr_t) (MELT_PREDEF (CLASS_SYMBOL)))))->
		    meltobj_magic == MELTOBMAG_OBJECT);
  cdat->dsym_296__DEBUG_MSG_FUN.meltobj_class =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (CLASS_SYMBOL))));
  cdat->dsym_296__DEBUG_MSG_FUN.obj_hash = 938829072;
  cdat->dsym_296__DEBUG_MSG_FUN.obj_len = 3;


/*inistring dstr_297__DEBUG_MSG_FUN*/
 /*_.VALSTR___V343*/ meltfptr[342] =
    (melt_ptr_t) & cdat->dstr_297__DEBUG_MSG_FUN;
  cdat->dstr_297__DEBUG_MSG_FUN.discr =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (DISCR_STRING))));
  /*small inistring */ strncpy (cdat->dstr_297__DEBUG_MSG_FUN.val,
				"DEBUG_MSG_FUN",
				sizeof (cdat->dstr_297__DEBUG_MSG_FUN.val) -
				1);
  cdat->dstr_297__DEBUG_MSG_FUN.val[13] = (char) 0;
  cdat->dstr_297__DEBUG_MSG_FUN.slen = 13;


/*iniobj dsym_298__DISPLAY_DEBUG_MESSAGE*/
  /*uniqueobj*/ if (! /*_.VALDATA___V344*/ meltfptr[343])
/*_.VALDATA___V344*/ meltfptr[343] =
      (melt_ptr_t) & cdat->dsym_298__DISPLAY_DEBUG_MESSAGE;
  if (MELT_LIKELY (!melt_prohibit_garbcoll))
    melt_assertmsg
      ("iniobj check.discr isobj dsym_298__DISPLAY_DEBUG_MESSAGE",
       melt_magic_discr ((melt_ptr_t)
			 (((melt_ptr_t) (MELT_PREDEF (CLASS_SYMBOL))))) ==
       MELTOBMAG_OBJECT);
  if (MELT_LIKELY (!melt_prohibit_garbcoll))
    melt_assertmsg
      ("iniobj check.discr objmagic dsym_298__DISPLAY_DEBUG_MESSAGE",
       ((meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (CLASS_SYMBOL)))))->
       meltobj_magic == MELTOBMAG_OBJECT);
  cdat->dsym_298__DISPLAY_DEBUG_MESSAGE.meltobj_class =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (CLASS_SYMBOL))));
  cdat->dsym_298__DISPLAY_DEBUG_MESSAGE.obj_hash = 759618783;
  cdat->dsym_298__DISPLAY_DEBUG_MESSAGE.obj_len = 3;


/*inistring dstr_299__DISPLAY_DEBUG_ME*/
 /*_.VALSTR___V345*/ meltfptr[344] =
    (melt_ptr_t) & cdat->dstr_299__DISPLAY_DEBUG_ME;
  cdat->dstr_299__DISPLAY_DEBUG_ME.discr =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (DISCR_STRING))));
  /*small inistring */ strncpy (cdat->dstr_299__DISPLAY_DEBUG_ME.val,
				"DISPLAY_DEBUG_MESSAGE",
				sizeof (cdat->dstr_299__DISPLAY_DEBUG_ME.
					val) - 1);
  cdat->dstr_299__DISPLAY_DEBUG_ME.val[21] = (char) 0;
  cdat->dstr_299__DISPLAY_DEBUG_ME.slen = 21;


/*iniobj dsym_300__MELT_DEBUG_FUN*/
  /*uniqueobj*/ if (! /*_.VALDATA___V346*/ meltfptr[345])
/*_.VALDATA___V346*/ meltfptr[345] =
      (melt_ptr_t) & cdat->dsym_300__MELT_DEBUG_FUN;
  if (MELT_LIKELY (!melt_prohibit_garbcoll))
    melt_assertmsg ("iniobj check.discr isobj dsym_300__MELT_DEBUG_FUN",
		    melt_magic_discr ((melt_ptr_t)
				      (((melt_ptr_t)
					(MELT_PREDEF (CLASS_SYMBOL))))) ==
		    MELTOBMAG_OBJECT);
  if (MELT_LIKELY (!melt_prohibit_garbcoll))
    melt_assertmsg ("iniobj check.discr objmagic dsym_300__MELT_DEBUG_FUN",
		    ((meltobject_ptr_t)
		     (((melt_ptr_t) (MELT_PREDEF (CLASS_SYMBOL)))))->
		    meltobj_magic == MELTOBMAG_OBJECT);
  cdat->dsym_300__MELT_DEBUG_FUN.meltobj_class =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (CLASS_SYMBOL))));
  cdat->dsym_300__MELT_DEBUG_FUN.obj_hash = 972123377;
  cdat->dsym_300__MELT_DEBUG_FUN.obj_len = 3;


/*inistring dstr_301__MELT_DEBUG_FUN*/
 /*_.VALSTR___V347*/ meltfptr[346] =
    (melt_ptr_t) & cdat->dstr_301__MELT_DEBUG_FUN;
  cdat->dstr_301__MELT_DEBUG_FUN.discr =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (DISCR_STRING))));
  /*small inistring */ strncpy (cdat->dstr_301__MELT_DEBUG_FUN.val,
				"MELT_DEBUG_FUN",
				sizeof (cdat->dstr_301__MELT_DEBUG_FUN.val) -
				1);
  cdat->dstr_301__MELT_DEBUG_FUN.val[14] = (char) 0;
  cdat->dstr_301__MELT_DEBUG_FUN.slen = 14;


/*iniobj dsym_302__REGISTER_BASICBLOCK_DEBUG_FUN*/
  /*uniqueobj*/ if (! /*_.VALDATA___V348*/ meltfptr[347])
/*_.VALDATA___V348*/ meltfptr[347] =
      (melt_ptr_t) & cdat->dsym_302__REGISTER_BASICBLOCK_DEBUG_FUN;
  if (MELT_LIKELY (!melt_prohibit_garbcoll))
    melt_assertmsg
      ("iniobj check.discr isobj dsym_302__REGISTER_BASICBLOCK_DEBUG_FUN",
       melt_magic_discr ((melt_ptr_t)
			 (((melt_ptr_t) (MELT_PREDEF (CLASS_SYMBOL))))) ==
       MELTOBMAG_OBJECT);
  if (MELT_LIKELY (!melt_prohibit_garbcoll))
    melt_assertmsg
      ("iniobj check.discr objmagic dsym_302__REGISTER_BASICBLOCK_DEBUG_FUN",
       ((meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (CLASS_SYMBOL)))))->
       meltobj_magic == MELTOBMAG_OBJECT);
  cdat->dsym_302__REGISTER_BASICBLOCK_DEBUG_FUN.meltobj_class =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (CLASS_SYMBOL))));
  cdat->dsym_302__REGISTER_BASICBLOCK_DEBUG_FUN.obj_hash = 475134270;
  cdat->dsym_302__REGISTER_BASICBLOCK_DEBUG_FUN.obj_len = 3;


/*inistring dstr_303__REGISTER_BASICBL*/
 /*_.VALSTR___V349*/ meltfptr[348] =
    (melt_ptr_t) & cdat->dstr_303__REGISTER_BASICBL;
  cdat->dstr_303__REGISTER_BASICBL.discr =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (DISCR_STRING))));
  /*small inistring */ strncpy (cdat->dstr_303__REGISTER_BASICBL.val,
				"REGISTER_BASICBLOCK_DEBUG_FUN",
				sizeof (cdat->dstr_303__REGISTER_BASICBL.
					val) - 1);
  cdat->dstr_303__REGISTER_BASICBL.val[29] = (char) 0;
  cdat->dstr_303__REGISTER_BASICBL.slen = 29;


/*iniobj dsym_304__REGISTER_EDGE_DEBUG_FUN*/
  /*uniqueobj*/ if (! /*_.VALDATA___V350*/ meltfptr[349])
/*_.VALDATA___V350*/ meltfptr[349] =
      (melt_ptr_t) & cdat->dsym_304__REGISTER_EDGE_DEBUG_FUN;
  if (MELT_LIKELY (!melt_prohibit_garbcoll))
    melt_assertmsg
      ("iniobj check.discr isobj dsym_304__REGISTER_EDGE_DEBUG_FUN",
       melt_magic_discr ((melt_ptr_t)
			 (((melt_ptr_t) (MELT_PREDEF (CLASS_SYMBOL))))) ==
       MELTOBMAG_OBJECT);
  if (MELT_LIKELY (!melt_prohibit_garbcoll))
    melt_assertmsg
      ("iniobj check.discr objmagic dsym_304__REGISTER_EDGE_DEBUG_FUN",
       ((meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (CLASS_SYMBOL)))))->
       meltobj_magic == MELTOBMAG_OBJECT);
  cdat->dsym_304__REGISTER_EDGE_DEBUG_FUN.meltobj_class =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (CLASS_SYMBOL))));
  cdat->dsym_304__REGISTER_EDGE_DEBUG_FUN.obj_hash = 739045024;
  cdat->dsym_304__REGISTER_EDGE_DEBUG_FUN.obj_len = 3;


/*inistring dstr_305__REGISTER_EDGE_DE*/
 /*_.VALSTR___V351*/ meltfptr[350] =
    (melt_ptr_t) & cdat->dstr_305__REGISTER_EDGE_DE;
  cdat->dstr_305__REGISTER_EDGE_DE.discr =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (DISCR_STRING))));
  /*small inistring */ strncpy (cdat->dstr_305__REGISTER_EDGE_DE.val,
				"REGISTER_EDGE_DEBUG_FUN",
				sizeof (cdat->dstr_305__REGISTER_EDGE_DE.
					val) - 1);
  cdat->dstr_305__REGISTER_EDGE_DE.val[23] = (char) 0;
  cdat->dstr_305__REGISTER_EDGE_DE.slen = 23;


/*iniobj dsym_306__REGISTER_GIMPLE_DEBUG_FUN*/
  /*uniqueobj*/ if (! /*_.VALDATA___V352*/ meltfptr[351])
/*_.VALDATA___V352*/ meltfptr[351] =
      (melt_ptr_t) & cdat->dsym_306__REGISTER_GIMPLE_DEBUG_FUN;
  if (MELT_LIKELY (!melt_prohibit_garbcoll))
    melt_assertmsg
      ("iniobj check.discr isobj dsym_306__REGISTER_GIMPLE_DEBUG_FUN",
       melt_magic_discr ((melt_ptr_t)
			 (((melt_ptr_t) (MELT_PREDEF (CLASS_SYMBOL))))) ==
       MELTOBMAG_OBJECT);
  if (MELT_LIKELY (!melt_prohibit_garbcoll))
    melt_assertmsg
      ("iniobj check.discr objmagic dsym_306__REGISTER_GIMPLE_DEBUG_FUN",
       ((meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (CLASS_SYMBOL)))))->
       meltobj_magic == MELTOBMAG_OBJECT);
  cdat->dsym_306__REGISTER_GIMPLE_DEBUG_FUN.meltobj_class =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (CLASS_SYMBOL))));
  cdat->dsym_306__REGISTER_GIMPLE_DEBUG_FUN.obj_hash = 158282506;
  cdat->dsym_306__REGISTER_GIMPLE_DEBUG_FUN.obj_len = 3;


/*inistring dstr_307__REGISTER_GIMPLE_*/
 /*_.VALSTR___V353*/ meltfptr[352] =
    (melt_ptr_t) & cdat->dstr_307__REGISTER_GIMPLE_;
  cdat->dstr_307__REGISTER_GIMPLE_.discr =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (DISCR_STRING))));
  /*small inistring */ strncpy (cdat->dstr_307__REGISTER_GIMPLE_.val,
				"REGISTER_GIMPLE_DEBUG_FUN",
				sizeof (cdat->dstr_307__REGISTER_GIMPLE_.
					val) - 1);
  cdat->dstr_307__REGISTER_GIMPLE_.val[25] = (char) 0;
  cdat->dstr_307__REGISTER_GIMPLE_.slen = 25;


/*iniobj dsym_308__REGISTER_GIMPLESEQ_DEBUG_FUN*/
  /*uniqueobj*/ if (! /*_.VALDATA___V354*/ meltfptr[353])
/*_.VALDATA___V354*/ meltfptr[353] =
      (melt_ptr_t) & cdat->dsym_308__REGISTER_GIMPLESEQ_DEBUG_FUN;
  if (MELT_LIKELY (!melt_prohibit_garbcoll))
    melt_assertmsg
      ("iniobj check.discr isobj dsym_308__REGISTER_GIMPLESEQ_DEBUG_FUN",
       melt_magic_discr ((melt_ptr_t)
			 (((melt_ptr_t) (MELT_PREDEF (CLASS_SYMBOL))))) ==
       MELTOBMAG_OBJECT);
  if (MELT_LIKELY (!melt_prohibit_garbcoll))
    melt_assertmsg
      ("iniobj check.discr objmagic dsym_308__REGISTER_GIMPLESEQ_DEBUG_FUN",
       ((meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (CLASS_SYMBOL)))))->
       meltobj_magic == MELTOBMAG_OBJECT);
  cdat->dsym_308__REGISTER_GIMPLESEQ_DEBUG_FUN.meltobj_class =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (CLASS_SYMBOL))));
  cdat->dsym_308__REGISTER_GIMPLESEQ_DEBUG_FUN.obj_hash = 247154639;
  cdat->dsym_308__REGISTER_GIMPLESEQ_DEBUG_FUN.obj_len = 3;


/*inistring dstr_309__REGISTER_GIMPLES*/
 /*_.VALSTR___V355*/ meltfptr[354] =
    (melt_ptr_t) & cdat->dstr_309__REGISTER_GIMPLES;
  cdat->dstr_309__REGISTER_GIMPLES.discr =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (DISCR_STRING))));
  /*small inistring */ strncpy (cdat->dstr_309__REGISTER_GIMPLES.val,
				"REGISTER_GIMPLESEQ_DEBUG_FUN",
				sizeof (cdat->dstr_309__REGISTER_GIMPLES.
					val) - 1);
  cdat->dstr_309__REGISTER_GIMPLES.val[28] = (char) 0;
  cdat->dstr_309__REGISTER_GIMPLES.slen = 28;


/*iniobj dsym_310__REGISTER_LOOP_DEBUG_FUN*/
  /*uniqueobj*/ if (! /*_.VALDATA___V356*/ meltfptr[355])
/*_.VALDATA___V356*/ meltfptr[355] =
      (melt_ptr_t) & cdat->dsym_310__REGISTER_LOOP_DEBUG_FUN;
  if (MELT_LIKELY (!melt_prohibit_garbcoll))
    melt_assertmsg
      ("iniobj check.discr isobj dsym_310__REGISTER_LOOP_DEBUG_FUN",
       melt_magic_discr ((melt_ptr_t)
			 (((melt_ptr_t) (MELT_PREDEF (CLASS_SYMBOL))))) ==
       MELTOBMAG_OBJECT);
  if (MELT_LIKELY (!melt_prohibit_garbcoll))
    melt_assertmsg
      ("iniobj check.discr objmagic dsym_310__REGISTER_LOOP_DEBUG_FUN",
       ((meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (CLASS_SYMBOL)))))->
       meltobj_magic == MELTOBMAG_OBJECT);
  cdat->dsym_310__REGISTER_LOOP_DEBUG_FUN.meltobj_class =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (CLASS_SYMBOL))));
  cdat->dsym_310__REGISTER_LOOP_DEBUG_FUN.obj_hash = 182074459;
  cdat->dsym_310__REGISTER_LOOP_DEBUG_FUN.obj_len = 3;


/*inistring dstr_311__REGISTER_LOOP_DE*/
 /*_.VALSTR___V357*/ meltfptr[356] =
    (melt_ptr_t) & cdat->dstr_311__REGISTER_LOOP_DE;
  cdat->dstr_311__REGISTER_LOOP_DE.discr =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (DISCR_STRING))));
  /*small inistring */ strncpy (cdat->dstr_311__REGISTER_LOOP_DE.val,
				"REGISTER_LOOP_DEBUG_FUN",
				sizeof (cdat->dstr_311__REGISTER_LOOP_DE.
					val) - 1);
  cdat->dstr_311__REGISTER_LOOP_DE.val[23] = (char) 0;
  cdat->dstr_311__REGISTER_LOOP_DE.slen = 23;


/*iniobj dsym_312__REGISTER_TREE_DEBUG_FUN*/
  /*uniqueobj*/ if (! /*_.VALDATA___V358*/ meltfptr[357])
/*_.VALDATA___V358*/ meltfptr[357] =
      (melt_ptr_t) & cdat->dsym_312__REGISTER_TREE_DEBUG_FUN;
  if (MELT_LIKELY (!melt_prohibit_garbcoll))
    melt_assertmsg
      ("iniobj check.discr isobj dsym_312__REGISTER_TREE_DEBUG_FUN",
       melt_magic_discr ((melt_ptr_t)
			 (((melt_ptr_t) (MELT_PREDEF (CLASS_SYMBOL))))) ==
       MELTOBMAG_OBJECT);
  if (MELT_LIKELY (!melt_prohibit_garbcoll))
    melt_assertmsg
      ("iniobj check.discr objmagic dsym_312__REGISTER_TREE_DEBUG_FUN",
       ((meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (CLASS_SYMBOL)))))->
       meltobj_magic == MELTOBMAG_OBJECT);
  cdat->dsym_312__REGISTER_TREE_DEBUG_FUN.meltobj_class =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (CLASS_SYMBOL))));
  cdat->dsym_312__REGISTER_TREE_DEBUG_FUN.obj_hash = 352695421;
  cdat->dsym_312__REGISTER_TREE_DEBUG_FUN.obj_len = 3;


/*inistring dstr_313__REGISTER_TREE_DE*/
 /*_.VALSTR___V359*/ meltfptr[358] =
    (melt_ptr_t) & cdat->dstr_313__REGISTER_TREE_DE;
  cdat->dstr_313__REGISTER_TREE_DE.discr =
    (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (DISCR_STRING))));
  /*small inistring */ strncpy (cdat->dstr_313__REGISTER_TREE_DE.val,
				"REGISTER_TREE_DEBUG_FUN",
				sizeof (cdat->dstr_313__REGISTER_TREE_DE.
					val) - 1);
  cdat->dstr_313__REGISTER_TREE_DE.val[23] = (char) 0;
  cdat->dstr_313__REGISTER_TREE_DE.slen = 23;
  cdat = NULL;
  melt_prohibit_garbcoll = FALSE;

#undef meltfram__
}				/*end initialize_module_meltdata_warmelt_debug */


#if MELT_HAVE_DEBUG
const char meltmodule_warmelt_debug__melt_have_debug_enabled[] =
  "MELT module warmelt-debug have debug enabled";
#else /*!MELT_HAVE_DEBUG */
const char meltmodule_warmelt_debug__melt_have_debug_disabled[] =
  "MELT module warmelt-debug have debug disabled";
#endif /*MELT_HAVE_DEBUG */


void *
melt_start_this_module (void *modargp_)
{

  char meltpredefinited[MELTGLOB__LASTGLOB + 8];

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  initial_frame_st meltfram__;	/*declfrastruct */
#define meltframe meltfram__

  /*melt_start_this_module is initial declstructinit */
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit initial routine melt_start_this_module minihash 2505*/
  meltfram__.mcfr_nbvar = -2505 /*minihash */ ;
  ((struct melt_callframe_st *) &meltfram__)->mcfr_forwmarkrout =
    meltmarking_melt_start_this_module;

  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
/**initial routine prologue**/
  /* set initial frame marking */
  ((struct melt_callframe_st *) &meltfram__)->mcfr_nbvar =
    /*minihash */ -2505;
  ((struct melt_callframe_st *) &meltfram__)->mcfr_forwmarkrout =
    meltmod__warmelt_debug__forward_or_mark_module_start_frame;
 /**COMMENT: get previous environment **/ ;

  /*^compute */
  /*_.PREVENV___V4*/ meltfptr[3] = /*getprevenv */ (melt_ptr_t) modargp_;
  /*^comment */
 /**COMMENT: compute boxloc **/ ;

  /*^cond */
  /*cond */ if ( /*_.CONTENV___V2*/ meltfptr[1] || melt_object_length ((melt_ptr_t) MELT_PREDEF (INITIAL_SYSTEM_DATA)) < MELTFIELD_SYSDATA_CONT_FRESH_ENV)	/*then */
    {
    }
  else
    {				/*^cond.else */

      /*^block */
      /*com.block:compute fresh module environment */
      {

	/*^comment */
   /**COMMENT: start computing boxloc **/ ;
	;
	/*^getslot */
	{
	  melt_ptr_t slot = NULL, obj = NULL;
	  obj =
	    (melt_ptr_t) (((melt_ptr_t) (MELT_PREDEF (INITIAL_SYSTEM_DATA))))
	    /*=obj*/ ;
	  melt_object_get_field (slot, obj, 3, "SYSDATA_CONT_FRESH_ENV");
    /*_.FRESHENV___V3*/ meltfptr[2] = slot;
	};
	;
	/*^compute */

	/*checkfreshenv */ if (( /*_.PREVENV___V4*/ meltfptr[3])
			       &&
			       melt_magic_discr ((melt_ptr_t)
						 ( /*_.FRESHENV___V3*/
						  meltfptr[2])) !=
			       MELTOBMAG_CLOSURE)
	  warning (0,
		   "MELT corruption: bad FRESH_ENV @%p in system data <%s:%d>",
		   (void *) ( /*_.FRESHENV___V3*/ meltfptr[2]), __FILE__,
		   __LINE__);;;
	/*^apply */
	/*apply */
	{
	  /*_.CONTENV___V2*/ meltfptr[1] =
	    melt_apply ((meltclosure_ptr_t)
			( /*_.FRESHENV___V3*/ meltfptr[2]),
			(melt_ptr_t) ( /*_.PREVENV___V4*/ meltfptr[3]), (""),
			(union meltparam_un *) 0, "",
			(union meltparam_un *) 0);
	}
	;
      }				/*com.end block:compute fresh module environment */
      ;
    }

  /*^comment */
 /**COMMENT: get symbols & keywords **/ ;

  /*^getnamedsymbol */
  /*getnamedsym:CLASS_DEBUG_INFORMATION */
  {
    melt_ptr_t sy_CLASS_DEBUG_INFORMATION =
      meltgc_named_symbol ("CLASS_DEBUG_INFORMATION", MELT_GET);
    if (sy_CLASS_DEBUG_INFORMATION
	&& NULL == /*_.VALDATA___V47*/ meltfptr[46])
  /*_.VALDATA___V47*/ meltfptr[46] =
	(melt_ptr_t) sy_CLASS_DEBUG_INFORMATION;
  }

  /*^getnamedsymbol */
  /*getnamedsym:CLASS_ROOT */
  {
    melt_ptr_t sy_CLASS_ROOT = meltgc_named_symbol ("CLASS_ROOT", MELT_GET);
    if (sy_CLASS_ROOT && NULL == /*_.VALDATA___V49*/ meltfptr[48])
  /*_.VALDATA___V49*/ meltfptr[48] = (melt_ptr_t) sy_CLASS_ROOT;
  }

  /*^getnamedsymbol */
  /*getnamedsym:DBGI_OUT */
  {
    melt_ptr_t sy_DBGI_OUT = meltgc_named_symbol ("DBGI_OUT", MELT_GET);
    if (sy_DBGI_OUT && NULL == /*_.VALDATA___V61*/ meltfptr[60])
  /*_.VALDATA___V61*/ meltfptr[60] = (melt_ptr_t) sy_DBGI_OUT;
  }

  /*^getnamedsymbol */
  /*getnamedsym:DBGI_OCCMAP */
  {
    melt_ptr_t sy_DBGI_OCCMAP = meltgc_named_symbol ("DBGI_OCCMAP", MELT_GET);
    if (sy_DBGI_OCCMAP && NULL == /*_.VALDATA___V63*/ meltfptr[62])
  /*_.VALDATA___V63*/ meltfptr[62] = (melt_ptr_t) sy_DBGI_OCCMAP;
  }

  /*^getnamedsymbol */
  /*getnamedsym:DBGI_MAXDEPTH */
  {
    melt_ptr_t sy_DBGI_MAXDEPTH =
      meltgc_named_symbol ("DBGI_MAXDEPTH", MELT_GET);
    if (sy_DBGI_MAXDEPTH && NULL == /*_.VALDATA___V65*/ meltfptr[64])
  /*_.VALDATA___V65*/ meltfptr[64] = (melt_ptr_t) sy_DBGI_MAXDEPTH;
  }

  /*^getnamedsymbol */
  /*getnamedsym:CLASS_DEBUG_OUTPUT_INFORMATION */
  {
    melt_ptr_t sy_CLASS_DEBUG_OUTPUT_INFORMATION =
      meltgc_named_symbol ("CLASS_DEBUG_OUTPUT_INFORMATION", MELT_GET);
    if (sy_CLASS_DEBUG_OUTPUT_INFORMATION
	&& NULL == /*_.VALDATA___V67*/ meltfptr[66])
  /*_.VALDATA___V67*/ meltfptr[66] =
	(melt_ptr_t) sy_CLASS_DEBUG_OUTPUT_INFORMATION;
  }

  /*^getnamedsymbol */
  /*getnamedsym:DEBUG_DEPTH */
  {
    melt_ptr_t sy_DEBUG_DEPTH = meltgc_named_symbol ("DEBUG_DEPTH", MELT_GET);
    if (sy_DEBUG_DEPTH && NULL == /*_.VALDATA___V73*/ meltfptr[72])
  /*_.VALDATA___V73*/ meltfptr[72] = (melt_ptr_t) sy_DEBUG_DEPTH;
  }

  /*^getnamedsymbol */
  /*getnamedsym:MELT_REALLY_NEED_DEBUG */
  {
    melt_ptr_t sy_MELT_REALLY_NEED_DEBUG =
      meltgc_named_symbol ("MELT_REALLY_NEED_DEBUG", MELT_GET);
    if (sy_MELT_REALLY_NEED_DEBUG && NULL == /*_.VALDATA___V80*/ meltfptr[79])
  /*_.VALDATA___V80*/ meltfptr[79] =
	(melt_ptr_t) sy_MELT_REALLY_NEED_DEBUG;
  }

  /*^getnamedsymbol */
  /*getnamedsym:DEPTH */
  {
    melt_ptr_t sy_DEPTH = meltgc_named_symbol ("DEPTH", MELT_GET);
    if (sy_DEPTH && NULL == /*_.VALDATA___V85*/ meltfptr[84])
  /*_.VALDATA___V85*/ meltfptr[84] = (melt_ptr_t) sy_DEPTH;
  }

  /*^getnamedsymbol */
  /*getnamedsym:MELT_HAS_FLAG_DEBUG_SET */
  {
    melt_ptr_t sy_MELT_HAS_FLAG_DEBUG_SET =
      meltgc_named_symbol ("MELT_HAS_FLAG_DEBUG_SET", MELT_GET);
    if (sy_MELT_HAS_FLAG_DEBUG_SET
	&& NULL == /*_.VALDATA___V91*/ meltfptr[90])
  /*_.VALDATA___V91*/ meltfptr[90] =
	(melt_ptr_t) sy_MELT_HAS_FLAG_DEBUG_SET;
  }

  /*^getnamedsymbol */
  /*getnamedsym:MELT_REALLY_NEED_DBGLIM */
  {
    melt_ptr_t sy_MELT_REALLY_NEED_DBGLIM =
      meltgc_named_symbol ("MELT_REALLY_NEED_DBGLIM", MELT_GET);
    if (sy_MELT_REALLY_NEED_DBGLIM
	&& NULL == /*_.VALDATA___V98*/ meltfptr[97])
  /*_.VALDATA___V98*/ meltfptr[97] =
	(melt_ptr_t) sy_MELT_REALLY_NEED_DBGLIM;
  }

  /*^getnamedsymbol */
  /*getnamedsym:LIMIT */
  {
    melt_ptr_t sy_LIMIT = meltgc_named_symbol ("LIMIT", MELT_GET);
    if (sy_LIMIT && NULL == /*_.VALDATA___V104*/ meltfptr[103])
  /*_.VALDATA___V104*/ meltfptr[103] = (melt_ptr_t) sy_LIMIT;
  }

  /*^getnamedsymbol */
  /*getnamedsym:INITIAL_SYSTEM_DATA */
  {
    melt_ptr_t sy_INITIAL_SYSTEM_DATA =
      meltgc_named_symbol ("INITIAL_SYSTEM_DATA", MELT_GET);
    if (sy_INITIAL_SYSTEM_DATA && NULL == /*_.VALDATA___V121*/ meltfptr[120])
  /*_.VALDATA___V121*/ meltfptr[120] =
	(melt_ptr_t) sy_INITIAL_SYSTEM_DATA;
  }

  /*^getnamedsymbol */
  /*getnamedsym:CLASS_SYSTEM_DATA */
  {
    melt_ptr_t sy_CLASS_SYSTEM_DATA =
      meltgc_named_symbol ("CLASS_SYSTEM_DATA", MELT_GET);
    if (sy_CLASS_SYSTEM_DATA && NULL == /*_.VALDATA___V123*/ meltfptr[122])
  /*_.VALDATA___V123*/ meltfptr[122] = (melt_ptr_t) sy_CLASS_SYSTEM_DATA;
  }

  /*^getnamedsymbol */
  /*getnamedsym:DISCR_MAP_OBJECTS */
  {
    melt_ptr_t sy_DISCR_MAP_OBJECTS =
      meltgc_named_symbol ("DISCR_MAP_OBJECTS", MELT_GET);
    if (sy_DISCR_MAP_OBJECTS && NULL == /*_.VALDATA___V125*/ meltfptr[124])
  /*_.VALDATA___V125*/ meltfptr[124] = (melt_ptr_t) sy_DISCR_MAP_OBJECTS;
  }

  /*^getnamedsymbol */
  /*getnamedsym:DISCR_INTEGER */
  {
    melt_ptr_t sy_DISCR_INTEGER =
      meltgc_named_symbol ("DISCR_INTEGER", MELT_GET);
    if (sy_DISCR_INTEGER && NULL == /*_.VALDATA___V127*/ meltfptr[126])
  /*_.VALDATA___V127*/ meltfptr[126] = (melt_ptr_t) sy_DISCR_INTEGER;
  }

  /*^getnamedsymbol */
  /*getnamedsym:DISCR_STRBUF */
  {
    melt_ptr_t sy_DISCR_STRBUF =
      meltgc_named_symbol ("DISCR_STRBUF", MELT_GET);
    if (sy_DISCR_STRBUF && NULL == /*_.VALDATA___V129*/ meltfptr[128])
  /*_.VALDATA___V129*/ meltfptr[128] = (melt_ptr_t) sy_DISCR_STRBUF;
  }

  /*^getnamedsymbol */
  /*getnamedsym:DBG_OUTPUT */
  {
    melt_ptr_t sy_DBG_OUTPUT = meltgc_named_symbol ("DBG_OUTPUT", MELT_GET);
    if (sy_DBG_OUTPUT && NULL == /*_.VALDATA___V131*/ meltfptr[130])
  /*_.VALDATA___V131*/ meltfptr[130] = (melt_ptr_t) sy_DBG_OUTPUT;
  }

  /*^getnamedsymbol */
  /*getnamedsym:CLASS_SELECTOR */
  {
    melt_ptr_t sy_CLASS_SELECTOR =
      meltgc_named_symbol ("CLASS_SELECTOR", MELT_GET);
    if (sy_CLASS_SELECTOR && NULL == /*_.VALDATA___V133*/ meltfptr[132])
  /*_.VALDATA___V133*/ meltfptr[132] = (melt_ptr_t) sy_CLASS_SELECTOR;
  }

  /*^getnamedsymbol */
  /*getnamedsym:RECV */
  {
    melt_ptr_t sy_RECV = meltgc_named_symbol ("RECV", MELT_GET);
    if (sy_RECV && NULL == /*_.VALDATA___V135*/ meltfptr[134])
  /*_.VALDATA___V135*/ meltfptr[134] = (melt_ptr_t) sy_RECV;
  }

  /*^getnamedsymbol */
  /*getnamedsym:DBGINFO */
  {
    melt_ptr_t sy_DBGINFO = meltgc_named_symbol ("DBGINFO", MELT_GET);
    if (sy_DBGINFO && NULL == /*_.VALDATA___V138*/ meltfptr[137])
  /*_.VALDATA___V138*/ meltfptr[137] = (melt_ptr_t) sy_DBGINFO;
  }

  /*^getnamedsymbol */
  /*getnamedsym:DBG_OUTPUTAGAIN */
  {
    melt_ptr_t sy_DBG_OUTPUTAGAIN =
      meltgc_named_symbol ("DBG_OUTPUTAGAIN", MELT_GET);
    if (sy_DBG_OUTPUTAGAIN && NULL == /*_.VALDATA___V143*/ meltfptr[142])
  /*_.VALDATA___V143*/ meltfptr[142] = (melt_ptr_t) sy_DBG_OUTPUTAGAIN;
  }

  /*^getnamedsymbol */
  /*getnamedsym:TREE_DEBUG_FUNCONT */
  {
    melt_ptr_t sy_TREE_DEBUG_FUNCONT =
      meltgc_named_symbol ("TREE_DEBUG_FUNCONT", MELT_GET);
    if (sy_TREE_DEBUG_FUNCONT && NULL == /*_.VALDATA___V149*/ meltfptr[148])
  /*_.VALDATA___V149*/ meltfptr[148] = (melt_ptr_t) sy_TREE_DEBUG_FUNCONT;
  }

  /*^getnamedsymbol */
  /*getnamedsym:CLASS_REFERENCE */
  {
    melt_ptr_t sy_CLASS_REFERENCE =
      meltgc_named_symbol ("CLASS_REFERENCE", MELT_GET);
    if (sy_CLASS_REFERENCE && NULL == /*_.VALDATA___V151*/ meltfptr[150])
  /*_.VALDATA___V151*/ meltfptr[150] = (melt_ptr_t) sy_CLASS_REFERENCE;
  }

  /*^getnamedsymbol */
  /*getnamedsym:GIMPLE_DEBUG_FUNCONT */
  {
    melt_ptr_t sy_GIMPLE_DEBUG_FUNCONT =
      meltgc_named_symbol ("GIMPLE_DEBUG_FUNCONT", MELT_GET);
    if (sy_GIMPLE_DEBUG_FUNCONT && NULL == /*_.VALDATA___V156*/ meltfptr[155])
  /*_.VALDATA___V156*/ meltfptr[155] =
	(melt_ptr_t) sy_GIMPLE_DEBUG_FUNCONT;
  }

  /*^getnamedsymbol */
  /*getnamedsym:GIMPLESEQ_DEBUG_FUNCONT */
  {
    melt_ptr_t sy_GIMPLESEQ_DEBUG_FUNCONT =
      meltgc_named_symbol ("GIMPLESEQ_DEBUG_FUNCONT", MELT_GET);
    if (sy_GIMPLESEQ_DEBUG_FUNCONT
	&& NULL == /*_.VALDATA___V161*/ meltfptr[160])
  /*_.VALDATA___V161*/ meltfptr[160] =
	(melt_ptr_t) sy_GIMPLESEQ_DEBUG_FUNCONT;
  }

  /*^getnamedsymbol */
  /*getnamedsym:EDGE_DEBUG_FUNCONT */
  {
    melt_ptr_t sy_EDGE_DEBUG_FUNCONT =
      meltgc_named_symbol ("EDGE_DEBUG_FUNCONT", MELT_GET);
    if (sy_EDGE_DEBUG_FUNCONT && NULL == /*_.VALDATA___V166*/ meltfptr[165])
  /*_.VALDATA___V166*/ meltfptr[165] = (melt_ptr_t) sy_EDGE_DEBUG_FUNCONT;
  }

  /*^getnamedsymbol */
  /*getnamedsym:BASICBLOCK_DEBUG_FUNCONT */
  {
    melt_ptr_t sy_BASICBLOCK_DEBUG_FUNCONT =
      meltgc_named_symbol ("BASICBLOCK_DEBUG_FUNCONT", MELT_GET);
    if (sy_BASICBLOCK_DEBUG_FUNCONT
	&& NULL == /*_.VALDATA___V171*/ meltfptr[170])
  /*_.VALDATA___V171*/ meltfptr[170] =
	(melt_ptr_t) sy_BASICBLOCK_DEBUG_FUNCONT;
  }

  /*^getnamedsymbol */
  /*getnamedsym:LOOP_DEBUG_FUNCONT */
  {
    melt_ptr_t sy_LOOP_DEBUG_FUNCONT =
      meltgc_named_symbol ("LOOP_DEBUG_FUNCONT", MELT_GET);
    if (sy_LOOP_DEBUG_FUNCONT && NULL == /*_.VALDATA___V176*/ meltfptr[175])
  /*_.VALDATA___V176*/ meltfptr[175] = (melt_ptr_t) sy_LOOP_DEBUG_FUNCONT;
  }

  /*^getnamedsymbol */
  /*getnamedsym:DISCR_DEBUG_CLOSURE */
  {
    melt_ptr_t sy_DISCR_DEBUG_CLOSURE =
      meltgc_named_symbol ("DISCR_DEBUG_CLOSURE", MELT_GET);
    if (sy_DISCR_DEBUG_CLOSURE && NULL == /*_.VALDATA___V181*/ meltfptr[180])
  /*_.VALDATA___V181*/ meltfptr[180] =
	(melt_ptr_t) sy_DISCR_DEBUG_CLOSURE;
  }

  /*^getnamedsymbol */
  /*getnamedsym:CLASS_DISCRIMINANT */
  {
    melt_ptr_t sy_CLASS_DISCRIMINANT =
      meltgc_named_symbol ("CLASS_DISCRIMINANT", MELT_GET);
    if (sy_CLASS_DISCRIMINANT && NULL == /*_.VALDATA___V183*/ meltfptr[182])
  /*_.VALDATA___V183*/ meltfptr[182] = (melt_ptr_t) sy_CLASS_DISCRIMINANT;
  }

  /*^getnamedsymbol */
  /*getnamedsym:DISCR_CLOSURE */
  {
    melt_ptr_t sy_DISCR_CLOSURE =
      meltgc_named_symbol ("DISCR_CLOSURE", MELT_GET);
    if (sy_DISCR_CLOSURE && NULL == /*_.VALDATA___V187*/ meltfptr[186])
  /*_.VALDATA___V187*/ meltfptr[186] = (melt_ptr_t) sy_DISCR_CLOSURE;
  }

  /*^getnamedsymbol */
  /*getnamedsym:ADD2OUT */
  {
    melt_ptr_t sy_ADD2OUT = meltgc_named_symbol ("ADD2OUT", MELT_GET);
    if (sy_ADD2OUT && NULL == /*_.VALDATA___V191*/ meltfptr[190])
  /*_.VALDATA___V191*/ meltfptr[190] = (melt_ptr_t) sy_ADD2OUT;
  }

  /*^getnamedsymbol */
  /*getnamedsym:CSTRING_PREFIXED */
  {
    melt_ptr_t sy_CSTRING_PREFIXED =
      meltgc_named_symbol ("CSTRING_PREFIXED", MELT_GET);
    if (sy_CSTRING_PREFIXED && NULL == /*_.VALDATA___V193*/ meltfptr[192])
  /*_.VALDATA___V193*/ meltfptr[192] = (melt_ptr_t) sy_CSTRING_PREFIXED;
  }

  /*^getnamedsymbol */
  /*getnamedsym:CLASS_NAMED */
  {
    melt_ptr_t sy_CLASS_NAMED = meltgc_named_symbol ("CLASS_NAMED", MELT_GET);
    if (sy_CLASS_NAMED && NULL == /*_.VALDATA___V195*/ meltfptr[194])
  /*_.VALDATA___V195*/ meltfptr[194] = (melt_ptr_t) sy_CLASS_NAMED;
  }

  /*^getnamedsymbol */
  /*getnamedsym:CLASS_CTYPE */
  {
    melt_ptr_t sy_CLASS_CTYPE = meltgc_named_symbol ("CLASS_CTYPE", MELT_GET);
    if (sy_CLASS_CTYPE && NULL == /*_.VALDATA___V197*/ meltfptr[196])
  /*_.VALDATA___V197*/ meltfptr[196] = (melt_ptr_t) sy_CLASS_CTYPE;
  }

  /*^getnamedsymbol */
  /*getnamedsym:INSTALL_METHOD */
  {
    melt_ptr_t sy_INSTALL_METHOD =
      meltgc_named_symbol ("INSTALL_METHOD", MELT_GET);
    if (sy_INSTALL_METHOD && NULL == /*_.VALDATA___V205*/ meltfptr[204])
  /*_.VALDATA___V205*/ meltfptr[204] = (melt_ptr_t) sy_INSTALL_METHOD;
  }

  /*^getnamedsymbol */
  /*getnamedsym:DISCR_NULL_RECEIVER */
  {
    melt_ptr_t sy_DISCR_NULL_RECEIVER =
      meltgc_named_symbol ("DISCR_NULL_RECEIVER", MELT_GET);
    if (sy_DISCR_NULL_RECEIVER && NULL == /*_.VALDATA___V207*/ meltfptr[206])
  /*_.VALDATA___V207*/ meltfptr[206] =
	(melt_ptr_t) sy_DISCR_NULL_RECEIVER;
  }

  /*^getnamedsymbol */
  /*getnamedsym:DISCR_STRING */
  {
    melt_ptr_t sy_DISCR_STRING =
      meltgc_named_symbol ("DISCR_STRING", MELT_GET);
    if (sy_DISCR_STRING && NULL == /*_.VALDATA___V211*/ meltfptr[210])
  /*_.VALDATA___V211*/ meltfptr[210] = (melt_ptr_t) sy_DISCR_STRING;
  }

  /*^getnamedsymbol */
  /*getnamedsym:DISCR_CONSTANT_INTEGER */
  {
    melt_ptr_t sy_DISCR_CONSTANT_INTEGER =
      meltgc_named_symbol ("DISCR_CONSTANT_INTEGER", MELT_GET);
    if (sy_DISCR_CONSTANT_INTEGER
	&& NULL == /*_.VALDATA___V217*/ meltfptr[216])
  /*_.VALDATA___V217*/ meltfptr[216] =
	(melt_ptr_t) sy_DISCR_CONSTANT_INTEGER;
  }

  /*^getnamedsymbol */
  /*getnamedsym:DISCR_MIXED_INTEGER */
  {
    melt_ptr_t sy_DISCR_MIXED_INTEGER =
      meltgc_named_symbol ("DISCR_MIXED_INTEGER", MELT_GET);
    if (sy_DISCR_MIXED_INTEGER && NULL == /*_.VALDATA___V221*/ meltfptr[220])
  /*_.VALDATA___V221*/ meltfptr[220] =
	(melt_ptr_t) sy_DISCR_MIXED_INTEGER;
  }

  /*^getnamedsymbol */
  /*getnamedsym:DISCR_MIXED_LOCATION */
  {
    melt_ptr_t sy_DISCR_MIXED_LOCATION =
      meltgc_named_symbol ("DISCR_MIXED_LOCATION", MELT_GET);
    if (sy_DISCR_MIXED_LOCATION && NULL == /*_.VALDATA___V225*/ meltfptr[224])
  /*_.VALDATA___V225*/ meltfptr[224] =
	(melt_ptr_t) sy_DISCR_MIXED_LOCATION;
  }

  /*^getnamedsymbol */
  /*getnamedsym:DISCR_MIXED_BIGINT */
  {
    melt_ptr_t sy_DISCR_MIXED_BIGINT =
      meltgc_named_symbol ("DISCR_MIXED_BIGINT", MELT_GET);
    if (sy_DISCR_MIXED_BIGINT && NULL == /*_.VALDATA___V229*/ meltfptr[228])
  /*_.VALDATA___V229*/ meltfptr[228] = (melt_ptr_t) sy_DISCR_MIXED_BIGINT;
  }

  /*^getnamedsymbol */
  /*getnamedsym:DISCR_MULTIPLE */
  {
    melt_ptr_t sy_DISCR_MULTIPLE =
      meltgc_named_symbol ("DISCR_MULTIPLE", MELT_GET);
    if (sy_DISCR_MULTIPLE && NULL == /*_.VALDATA___V233*/ meltfptr[232])
  /*_.VALDATA___V233*/ meltfptr[232] = (melt_ptr_t) sy_DISCR_MULTIPLE;
  }

  /*^getnamedsymbol */
  /*getnamedsym:DISCR_ROUTINE */
  {
    melt_ptr_t sy_DISCR_ROUTINE =
      meltgc_named_symbol ("DISCR_ROUTINE", MELT_GET);
    if (sy_DISCR_ROUTINE && NULL == /*_.VALDATA___V238*/ meltfptr[237])
  /*_.VALDATA___V238*/ meltfptr[237] = (melt_ptr_t) sy_DISCR_ROUTINE;
  }

  /*^getnamedsymbol */
  /*getnamedsym:ROUTINE_EVERY */
  {
    melt_ptr_t sy_ROUTINE_EVERY =
      meltgc_named_symbol ("ROUTINE_EVERY", MELT_GET);
    if (sy_ROUTINE_EVERY && NULL == /*_.VALDATA___V240*/ meltfptr[239])
  /*_.VALDATA___V240*/ meltfptr[239] = (melt_ptr_t) sy_ROUTINE_EVERY;
  }

  /*^getnamedsymbol */
  /*getnamedsym:DISCR_LIST */
  {
    melt_ptr_t sy_DISCR_LIST = meltgc_named_symbol ("DISCR_LIST", MELT_GET);
    if (sy_DISCR_LIST && NULL == /*_.VALDATA___V246*/ meltfptr[245])
  /*_.VALDATA___V246*/ meltfptr[245] = (melt_ptr_t) sy_DISCR_LIST;
  }

  /*^getnamedsymbol */
  /*getnamedsym:DISCR_PAIR */
  {
    melt_ptr_t sy_DISCR_PAIR = meltgc_named_symbol ("DISCR_PAIR", MELT_GET);
    if (sy_DISCR_PAIR && NULL == /*_.VALDATA___V250*/ meltfptr[249])
  /*_.VALDATA___V250*/ meltfptr[249] = (melt_ptr_t) sy_DISCR_PAIR;
  }

  /*^getnamedsymbol */
  /*getnamedsym:MAPOBJECT_EVERY */
  {
    melt_ptr_t sy_MAPOBJECT_EVERY =
      meltgc_named_symbol ("MAPOBJECT_EVERY", MELT_GET);
    if (sy_MAPOBJECT_EVERY && NULL == /*_.VALDATA___V260*/ meltfptr[259])
  /*_.VALDATA___V260*/ meltfptr[259] = (melt_ptr_t) sy_MAPOBJECT_EVERY;
  }

  /*^getnamedsymbol */
  /*getnamedsym:COMPARE_OBJ_RANKED */
  {
    melt_ptr_t sy_COMPARE_OBJ_RANKED =
      meltgc_named_symbol ("COMPARE_OBJ_RANKED", MELT_GET);
    if (sy_COMPARE_OBJ_RANKED && NULL == /*_.VALDATA___V262*/ meltfptr[261])
  /*_.VALDATA___V262*/ meltfptr[261] = (melt_ptr_t) sy_COMPARE_OBJ_RANKED;
  }

  /*^getnamedsymbol */
  /*getnamedsym:MULTIPLE_EVERY */
  {
    melt_ptr_t sy_MULTIPLE_EVERY =
      meltgc_named_symbol ("MULTIPLE_EVERY", MELT_GET);
    if (sy_MULTIPLE_EVERY && NULL == /*_.VALDATA___V264*/ meltfptr[263])
  /*_.VALDATA___V264*/ meltfptr[263] = (melt_ptr_t) sy_MULTIPLE_EVERY;
  }

  /*^getnamedsymbol */
  /*getnamedsym:DISCR_MAP_STRINGS */
  {
    melt_ptr_t sy_DISCR_MAP_STRINGS =
      meltgc_named_symbol ("DISCR_MAP_STRINGS", MELT_GET);
    if (sy_DISCR_MAP_STRINGS && NULL == /*_.VALDATA___V274*/ meltfptr[273])
  /*_.VALDATA___V274*/ meltfptr[273] = (melt_ptr_t) sy_DISCR_MAP_STRINGS;
  }

  /*^getnamedsymbol */
  /*getnamedsym:MAPSTRING_EVERY */
  {
    melt_ptr_t sy_MAPSTRING_EVERY =
      meltgc_named_symbol ("MAPSTRING_EVERY", MELT_GET);
    if (sy_MAPSTRING_EVERY && NULL == /*_.VALDATA___V276*/ meltfptr[275])
  /*_.VALDATA___V276*/ meltfptr[275] = (melt_ptr_t) sy_MAPSTRING_EVERY;
  }

  /*^getnamedsymbol */
  /*getnamedsym:DISCR_BUCKET_LONGS */
  {
    melt_ptr_t sy_DISCR_BUCKET_LONGS =
      meltgc_named_symbol ("DISCR_BUCKET_LONGS", MELT_GET);
    if (sy_DISCR_BUCKET_LONGS && NULL == /*_.VALDATA___V280*/ meltfptr[279])
  /*_.VALDATA___V280*/ meltfptr[279] = (melt_ptr_t) sy_DISCR_BUCKET_LONGS;
  }

  /*^getnamedsymbol */
  /*getnamedsym:DISCR_ANY_RECEIVER */
  {
    melt_ptr_t sy_DISCR_ANY_RECEIVER =
      meltgc_named_symbol ("DISCR_ANY_RECEIVER", MELT_GET);
    if (sy_DISCR_ANY_RECEIVER && NULL == /*_.VALDATA___V286*/ meltfptr[285])
  /*_.VALDATA___V286*/ meltfptr[285] = (melt_ptr_t) sy_DISCR_ANY_RECEIVER;
  }

  /*^getnamedsymbol */
  /*getnamedsym:DBGOUT_NAMEDOBJECT_METHOD */
  {
    melt_ptr_t sy_DBGOUT_NAMEDOBJECT_METHOD =
      meltgc_named_symbol ("DBGOUT_NAMEDOBJECT_METHOD", MELT_GET);
    if (sy_DBGOUT_NAMEDOBJECT_METHOD
	&& NULL == /*_.VALDATA___V292*/ meltfptr[291])
  /*_.VALDATA___V292*/ meltfptr[291] =
	(melt_ptr_t) sy_DBGOUT_NAMEDOBJECT_METHOD;
  }

  /*^getnamedsymbol */
  /*getnamedsym:CLASS_SYMBOL */
  {
    melt_ptr_t sy_CLASS_SYMBOL =
      meltgc_named_symbol ("CLASS_SYMBOL", MELT_GET);
    if (sy_CLASS_SYMBOL && NULL == /*_.VALDATA___V298*/ meltfptr[297])
  /*_.VALDATA___V298*/ meltfptr[297] = (melt_ptr_t) sy_CLASS_SYMBOL;
  }

  /*^getnamedsymbol */
  /*getnamedsym:CLASS_CLASS */
  {
    melt_ptr_t sy_CLASS_CLASS = meltgc_named_symbol ("CLASS_CLASS", MELT_GET);
    if (sy_CLASS_CLASS && NULL == /*_.VALDATA___V304*/ meltfptr[303])
  /*_.VALDATA___V304*/ meltfptr[303] = (melt_ptr_t) sy_CLASS_CLASS;
  }

  /*^getnamedsymbol */
  /*getnamedsym:CLASS_KEYWORD */
  {
    melt_ptr_t sy_CLASS_KEYWORD =
      meltgc_named_symbol ("CLASS_KEYWORD", MELT_GET);
    if (sy_CLASS_KEYWORD && NULL == /*_.VALDATA___V308*/ meltfptr[307])
  /*_.VALDATA___V308*/ meltfptr[307] = (melt_ptr_t) sy_CLASS_KEYWORD;
  }

  /*^getnamedsymbol */
  /*getnamedsym:CLASS_CLONED_SYMBOL */
  {
    melt_ptr_t sy_CLASS_CLONED_SYMBOL =
      meltgc_named_symbol ("CLASS_CLONED_SYMBOL", MELT_GET);
    if (sy_CLASS_CLONED_SYMBOL && NULL == /*_.VALDATA___V314*/ meltfptr[313])
  /*_.VALDATA___V314*/ meltfptr[313] =
	(melt_ptr_t) sy_CLASS_CLONED_SYMBOL;
  }

  /*^getnamedsymbol */
  /*getnamedsym:CLASS_ENVIRONMENT */
  {
    melt_ptr_t sy_CLASS_ENVIRONMENT =
      meltgc_named_symbol ("CLASS_ENVIRONMENT", MELT_GET);
    if (sy_CLASS_ENVIRONMENT && NULL == /*_.VALDATA___V320*/ meltfptr[319])
  /*_.VALDATA___V320*/ meltfptr[319] = (melt_ptr_t) sy_CLASS_ENVIRONMENT;
  }

  /*^getnamedsymbol */
  /*getnamedsym:ENV_PREV */
  {
    melt_ptr_t sy_ENV_PREV = meltgc_named_symbol ("ENV_PREV", MELT_GET);
    if (sy_ENV_PREV && NULL == /*_.VALDATA___V322*/ meltfptr[321])
  /*_.VALDATA___V322*/ meltfptr[321] = (melt_ptr_t) sy_ENV_PREV;
  }

  /*^getnamedsymbol */
  /*getnamedsym:CLASS_ANY_BINDING */
  {
    melt_ptr_t sy_CLASS_ANY_BINDING =
      meltgc_named_symbol ("CLASS_ANY_BINDING", MELT_GET);
    if (sy_CLASS_ANY_BINDING && NULL == /*_.VALDATA___V330*/ meltfptr[329])
  /*_.VALDATA___V330*/ meltfptr[329] = (melt_ptr_t) sy_CLASS_ANY_BINDING;
  }

  /*^getnamedsymbol */
  /*getnamedsym:DBG_OUT */
  {
    melt_ptr_t sy_DBG_OUT = meltgc_named_symbol ("DBG_OUT", MELT_GET);
    if (sy_DBG_OUT && NULL == /*_.VALDATA___V334*/ meltfptr[333])
  /*_.VALDATA___V334*/ meltfptr[333] = (melt_ptr_t) sy_DBG_OUT;
  }

  /*^getnamedsymbol */
  /*getnamedsym:DBG_OUTOBJECT */
  {
    melt_ptr_t sy_DBG_OUTOBJECT =
      meltgc_named_symbol ("DBG_OUTOBJECT", MELT_GET);
    if (sy_DBG_OUTOBJECT && NULL == /*_.VALDATA___V336*/ meltfptr[335])
  /*_.VALDATA___V336*/ meltfptr[335] = (melt_ptr_t) sy_DBG_OUTOBJECT;
  }

  /*^getnamedsymbol */
  /*getnamedsym:DBGOUT_FIELDS */
  {
    melt_ptr_t sy_DBGOUT_FIELDS =
      meltgc_named_symbol ("DBGOUT_FIELDS", MELT_GET);
    if (sy_DBGOUT_FIELDS && NULL == /*_.VALDATA___V338*/ meltfptr[337])
  /*_.VALDATA___V338*/ meltfptr[337] = (melt_ptr_t) sy_DBGOUT_FIELDS;
  }

  /*^getnamedsymbol */
  /*getnamedsym:DBGOUTAGAIN_FIELDS */
  {
    melt_ptr_t sy_DBGOUTAGAIN_FIELDS =
      meltgc_named_symbol ("DBGOUTAGAIN_FIELDS", MELT_GET);
    if (sy_DBGOUTAGAIN_FIELDS && NULL == /*_.VALDATA___V340*/ meltfptr[339])
  /*_.VALDATA___V340*/ meltfptr[339] = (melt_ptr_t) sy_DBGOUTAGAIN_FIELDS;
  }

  /*^getnamedsymbol */
  /*getnamedsym:DEBUG_MSG_FUN */
  {
    melt_ptr_t sy_DEBUG_MSG_FUN =
      meltgc_named_symbol ("DEBUG_MSG_FUN", MELT_GET);
    if (sy_DEBUG_MSG_FUN && NULL == /*_.VALDATA___V342*/ meltfptr[341])
  /*_.VALDATA___V342*/ meltfptr[341] = (melt_ptr_t) sy_DEBUG_MSG_FUN;
  }

  /*^getnamedsymbol */
  /*getnamedsym:DISPLAY_DEBUG_MESSAGE */
  {
    melt_ptr_t sy_DISPLAY_DEBUG_MESSAGE =
      meltgc_named_symbol ("DISPLAY_DEBUG_MESSAGE", MELT_GET);
    if (sy_DISPLAY_DEBUG_MESSAGE
	&& NULL == /*_.VALDATA___V344*/ meltfptr[343])
  /*_.VALDATA___V344*/ meltfptr[343] =
	(melt_ptr_t) sy_DISPLAY_DEBUG_MESSAGE;
  }

  /*^getnamedsymbol */
  /*getnamedsym:MELT_DEBUG_FUN */
  {
    melt_ptr_t sy_MELT_DEBUG_FUN =
      meltgc_named_symbol ("MELT_DEBUG_FUN", MELT_GET);
    if (sy_MELT_DEBUG_FUN && NULL == /*_.VALDATA___V346*/ meltfptr[345])
  /*_.VALDATA___V346*/ meltfptr[345] = (melt_ptr_t) sy_MELT_DEBUG_FUN;
  }

  /*^getnamedsymbol */
  /*getnamedsym:REGISTER_BASICBLOCK_DEBUG_FUN */
  {
    melt_ptr_t sy_REGISTER_BASICBLOCK_DEBUG_FUN =
      meltgc_named_symbol ("REGISTER_BASICBLOCK_DEBUG_FUN", MELT_GET);
    if (sy_REGISTER_BASICBLOCK_DEBUG_FUN
	&& NULL == /*_.VALDATA___V348*/ meltfptr[347])
  /*_.VALDATA___V348*/ meltfptr[347] =
	(melt_ptr_t) sy_REGISTER_BASICBLOCK_DEBUG_FUN;
  }

  /*^getnamedsymbol */
  /*getnamedsym:REGISTER_EDGE_DEBUG_FUN */
  {
    melt_ptr_t sy_REGISTER_EDGE_DEBUG_FUN =
      meltgc_named_symbol ("REGISTER_EDGE_DEBUG_FUN", MELT_GET);
    if (sy_REGISTER_EDGE_DEBUG_FUN
	&& NULL == /*_.VALDATA___V350*/ meltfptr[349])
  /*_.VALDATA___V350*/ meltfptr[349] =
	(melt_ptr_t) sy_REGISTER_EDGE_DEBUG_FUN;
  }

  /*^getnamedsymbol */
  /*getnamedsym:REGISTER_GIMPLE_DEBUG_FUN */
  {
    melt_ptr_t sy_REGISTER_GIMPLE_DEBUG_FUN =
      meltgc_named_symbol ("REGISTER_GIMPLE_DEBUG_FUN", MELT_GET);
    if (sy_REGISTER_GIMPLE_DEBUG_FUN
	&& NULL == /*_.VALDATA___V352*/ meltfptr[351])
  /*_.VALDATA___V352*/ meltfptr[351] =
	(melt_ptr_t) sy_REGISTER_GIMPLE_DEBUG_FUN;
  }

  /*^getnamedsymbol */
  /*getnamedsym:REGISTER_GIMPLESEQ_DEBUG_FUN */
  {
    melt_ptr_t sy_REGISTER_GIMPLESEQ_DEBUG_FUN =
      meltgc_named_symbol ("REGISTER_GIMPLESEQ_DEBUG_FUN", MELT_GET);
    if (sy_REGISTER_GIMPLESEQ_DEBUG_FUN
	&& NULL == /*_.VALDATA___V354*/ meltfptr[353])
  /*_.VALDATA___V354*/ meltfptr[353] =
	(melt_ptr_t) sy_REGISTER_GIMPLESEQ_DEBUG_FUN;
  }

  /*^getnamedsymbol */
  /*getnamedsym:REGISTER_LOOP_DEBUG_FUN */
  {
    melt_ptr_t sy_REGISTER_LOOP_DEBUG_FUN =
      meltgc_named_symbol ("REGISTER_LOOP_DEBUG_FUN", MELT_GET);
    if (sy_REGISTER_LOOP_DEBUG_FUN
	&& NULL == /*_.VALDATA___V356*/ meltfptr[355])
  /*_.VALDATA___V356*/ meltfptr[355] =
	(melt_ptr_t) sy_REGISTER_LOOP_DEBUG_FUN;
  }

  /*^getnamedsymbol */
  /*getnamedsym:REGISTER_TREE_DEBUG_FUN */
  {
    melt_ptr_t sy_REGISTER_TREE_DEBUG_FUN =
      meltgc_named_symbol ("REGISTER_TREE_DEBUG_FUN", MELT_GET);
    if (sy_REGISTER_TREE_DEBUG_FUN
	&& NULL == /*_.VALDATA___V358*/ meltfptr[357])
  /*_.VALDATA___V358*/ meltfptr[357] =
	(melt_ptr_t) sy_REGISTER_TREE_DEBUG_FUN;
  }

  /*^comment */
 /**COMMENT: get the value importer **/ ;

  /*^getslot */
  {
    melt_ptr_t slot = NULL, obj = NULL;
    obj =
      (melt_ptr_t) (((melt_ptr_t) (MELT_PREDEF (INITIAL_SYSTEM_DATA))))
      /*=obj*/ ;
    melt_object_get_field (slot, obj, 12, "SYSDATA_VALUE_IMPORTER");
  /*_.VALIMPORT___V6*/ meltfptr[5] = slot;
  };

  /*^comment */
 /**COMMENT: before getting imported values **/ ;

  /*^apply */
  /*apply */
  {
    union meltparam_un argtab[3];
    memset (&argtab, 0, sizeof (argtab));
    /*^apply.arg */
    argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.PREVENV___V4*/ meltfptr[3];
    /*^apply.arg */
    argtab[1].meltbp_cstring = "CLASS_ROOT";
    /*^apply.arg */
    argtab[2].meltbp_cstring = "warmelt-debug";
    /*_.CLASS_ROOT__V7*/ meltfptr[6] =
      melt_apply ((meltclosure_ptr_t) ( /*_.VALIMPORT___V6*/ meltfptr[5]),
		  (melt_ptr_t) ( /*_.VALDATA___V49*/ meltfptr[48]),
		  (MELTBPARSTR_PTR MELTBPARSTR_CSTRING MELTBPARSTR_CSTRING
		   ""), argtab, "", (union meltparam_un *) 0);
  }

  /*^apply */
  /*apply */
  {
    union meltparam_un argtab[3];
    memset (&argtab, 0, sizeof (argtab));
    /*^apply.arg */
    argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.PREVENV___V4*/ meltfptr[3];
    /*^apply.arg */
    argtab[1].meltbp_cstring = "INITIAL_SYSTEM_DATA";
    /*^apply.arg */
    argtab[2].meltbp_cstring = "warmelt-debug";
    /*_.INITIAL_SYSTEM_DATA__V8*/ meltfptr[7] =
      melt_apply ((meltclosure_ptr_t) ( /*_.VALIMPORT___V6*/ meltfptr[5]),
		  (melt_ptr_t) ( /*_.VALDATA___V121*/ meltfptr[120]),
		  (MELTBPARSTR_PTR MELTBPARSTR_CSTRING MELTBPARSTR_CSTRING
		   ""), argtab, "", (union meltparam_un *) 0);
  }

  /*^apply */
  /*apply */
  {
    union meltparam_un argtab[3];
    memset (&argtab, 0, sizeof (argtab));
    /*^apply.arg */
    argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.PREVENV___V4*/ meltfptr[3];
    /*^apply.arg */
    argtab[1].meltbp_cstring = "CLASS_SYSTEM_DATA";
    /*^apply.arg */
    argtab[2].meltbp_cstring = "warmelt-debug";
    /*_.CLASS_SYSTEM_DATA__V9*/ meltfptr[8] =
      melt_apply ((meltclosure_ptr_t) ( /*_.VALIMPORT___V6*/ meltfptr[5]),
		  (melt_ptr_t) ( /*_.VALDATA___V123*/ meltfptr[122]),
		  (MELTBPARSTR_PTR MELTBPARSTR_CSTRING MELTBPARSTR_CSTRING
		   ""), argtab, "", (union meltparam_un *) 0);
  }

  /*^apply */
  /*apply */
  {
    union meltparam_un argtab[3];
    memset (&argtab, 0, sizeof (argtab));
    /*^apply.arg */
    argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.PREVENV___V4*/ meltfptr[3];
    /*^apply.arg */
    argtab[1].meltbp_cstring = "DISCR_MAP_OBJECTS";
    /*^apply.arg */
    argtab[2].meltbp_cstring = "warmelt-debug";
    /*_.DISCR_MAP_OBJECTS__V10*/ meltfptr[9] =
      melt_apply ((meltclosure_ptr_t) ( /*_.VALIMPORT___V6*/ meltfptr[5]),
		  (melt_ptr_t) ( /*_.VALDATA___V125*/ meltfptr[124]),
		  (MELTBPARSTR_PTR MELTBPARSTR_CSTRING MELTBPARSTR_CSTRING
		   ""), argtab, "", (union meltparam_un *) 0);
  }

  /*^apply */
  /*apply */
  {
    union meltparam_un argtab[3];
    memset (&argtab, 0, sizeof (argtab));
    /*^apply.arg */
    argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.PREVENV___V4*/ meltfptr[3];
    /*^apply.arg */
    argtab[1].meltbp_cstring = "DISCR_INTEGER";
    /*^apply.arg */
    argtab[2].meltbp_cstring = "warmelt-debug";
    /*_.DISCR_INTEGER__V11*/ meltfptr[10] =
      melt_apply ((meltclosure_ptr_t) ( /*_.VALIMPORT___V6*/ meltfptr[5]),
		  (melt_ptr_t) ( /*_.VALDATA___V127*/ meltfptr[126]),
		  (MELTBPARSTR_PTR MELTBPARSTR_CSTRING MELTBPARSTR_CSTRING
		   ""), argtab, "", (union meltparam_un *) 0);
  }

  /*^apply */
  /*apply */
  {
    union meltparam_un argtab[3];
    memset (&argtab, 0, sizeof (argtab));
    /*^apply.arg */
    argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.PREVENV___V4*/ meltfptr[3];
    /*^apply.arg */
    argtab[1].meltbp_cstring = "DISCR_STRBUF";
    /*^apply.arg */
    argtab[2].meltbp_cstring = "warmelt-debug";
    /*_.DISCR_STRBUF__V12*/ meltfptr[11] =
      melt_apply ((meltclosure_ptr_t) ( /*_.VALIMPORT___V6*/ meltfptr[5]),
		  (melt_ptr_t) ( /*_.VALDATA___V129*/ meltfptr[128]),
		  (MELTBPARSTR_PTR MELTBPARSTR_CSTRING MELTBPARSTR_CSTRING
		   ""), argtab, "", (union meltparam_un *) 0);
  }

  /*^apply */
  /*apply */
  {
    union meltparam_un argtab[3];
    memset (&argtab, 0, sizeof (argtab));
    /*^apply.arg */
    argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.PREVENV___V4*/ meltfptr[3];
    /*^apply.arg */
    argtab[1].meltbp_cstring = "CLASS_SELECTOR";
    /*^apply.arg */
    argtab[2].meltbp_cstring = "warmelt-debug";
    /*_.CLASS_SELECTOR__V13*/ meltfptr[12] =
      melt_apply ((meltclosure_ptr_t) ( /*_.VALIMPORT___V6*/ meltfptr[5]),
		  (melt_ptr_t) ( /*_.VALDATA___V133*/ meltfptr[132]),
		  (MELTBPARSTR_PTR MELTBPARSTR_CSTRING MELTBPARSTR_CSTRING
		   ""), argtab, "", (union meltparam_un *) 0);
  }

  /*^apply */
  /*apply */
  {
    union meltparam_un argtab[3];
    memset (&argtab, 0, sizeof (argtab));
    /*^apply.arg */
    argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.PREVENV___V4*/ meltfptr[3];
    /*^apply.arg */
    argtab[1].meltbp_cstring = "CLASS_REFERENCE";
    /*^apply.arg */
    argtab[2].meltbp_cstring = "warmelt-debug";
    /*_.CLASS_REFERENCE__V14*/ meltfptr[13] =
      melt_apply ((meltclosure_ptr_t) ( /*_.VALIMPORT___V6*/ meltfptr[5]),
		  (melt_ptr_t) ( /*_.VALDATA___V151*/ meltfptr[150]),
		  (MELTBPARSTR_PTR MELTBPARSTR_CSTRING MELTBPARSTR_CSTRING
		   ""), argtab, "", (union meltparam_un *) 0);
  }

  /*^apply */
  /*apply */
  {
    union meltparam_un argtab[3];
    memset (&argtab, 0, sizeof (argtab));
    /*^apply.arg */
    argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.PREVENV___V4*/ meltfptr[3];
    /*^apply.arg */
    argtab[1].meltbp_cstring = "CLASS_DISCRIMINANT";
    /*^apply.arg */
    argtab[2].meltbp_cstring = "warmelt-debug";
    /*_.CLASS_DISCRIMINANT__V15*/ meltfptr[14] =
      melt_apply ((meltclosure_ptr_t) ( /*_.VALIMPORT___V6*/ meltfptr[5]),
		  (melt_ptr_t) ( /*_.VALDATA___V183*/ meltfptr[182]),
		  (MELTBPARSTR_PTR MELTBPARSTR_CSTRING MELTBPARSTR_CSTRING
		   ""), argtab, "", (union meltparam_un *) 0);
  }

  /*^apply */
  /*apply */
  {
    union meltparam_un argtab[3];
    memset (&argtab, 0, sizeof (argtab));
    /*^apply.arg */
    argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.PREVENV___V4*/ meltfptr[3];
    /*^apply.arg */
    argtab[1].meltbp_cstring = "DISCR_CLOSURE";
    /*^apply.arg */
    argtab[2].meltbp_cstring = "warmelt-debug";
    /*_.DISCR_CLOSURE__V16*/ meltfptr[15] =
      melt_apply ((meltclosure_ptr_t) ( /*_.VALIMPORT___V6*/ meltfptr[5]),
		  (melt_ptr_t) ( /*_.VALDATA___V187*/ meltfptr[186]),
		  (MELTBPARSTR_PTR MELTBPARSTR_CSTRING MELTBPARSTR_CSTRING
		   ""), argtab, "", (union meltparam_un *) 0);
  }

  /*^apply */
  /*apply */
  {
    union meltparam_un argtab[3];
    memset (&argtab, 0, sizeof (argtab));
    /*^apply.arg */
    argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.PREVENV___V4*/ meltfptr[3];
    /*^apply.arg */
    argtab[1].meltbp_cstring = "ADD2OUT";
    /*^apply.arg */
    argtab[2].meltbp_cstring = "warmelt-debug";
    /*_.ADD2OUT__V17*/ meltfptr[16] =
      melt_apply ((meltclosure_ptr_t) ( /*_.VALIMPORT___V6*/ meltfptr[5]),
		  (melt_ptr_t) ( /*_.VALDATA___V191*/ meltfptr[190]),
		  (MELTBPARSTR_PTR MELTBPARSTR_CSTRING MELTBPARSTR_CSTRING
		   ""), argtab, "", (union meltparam_un *) 0);
  }

  /*^apply */
  /*apply */
  {
    union meltparam_un argtab[3];
    memset (&argtab, 0, sizeof (argtab));
    /*^apply.arg */
    argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.PREVENV___V4*/ meltfptr[3];
    /*^apply.arg */
    argtab[1].meltbp_cstring = "CSTRING_PREFIXED";
    /*^apply.arg */
    argtab[2].meltbp_cstring = "warmelt-debug";
    /*_.CSTRING_PREFIXED__V18*/ meltfptr[17] =
      melt_apply ((meltclosure_ptr_t) ( /*_.VALIMPORT___V6*/ meltfptr[5]),
		  (melt_ptr_t) ( /*_.VALDATA___V193*/ meltfptr[192]),
		  (MELTBPARSTR_PTR MELTBPARSTR_CSTRING MELTBPARSTR_CSTRING
		   ""), argtab, "", (union meltparam_un *) 0);
  }

  /*^apply */
  /*apply */
  {
    union meltparam_un argtab[3];
    memset (&argtab, 0, sizeof (argtab));
    /*^apply.arg */
    argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.PREVENV___V4*/ meltfptr[3];
    /*^apply.arg */
    argtab[1].meltbp_cstring = "CLASS_NAMED";
    /*^apply.arg */
    argtab[2].meltbp_cstring = "warmelt-debug";
    /*_.CLASS_NAMED__V19*/ meltfptr[18] =
      melt_apply ((meltclosure_ptr_t) ( /*_.VALIMPORT___V6*/ meltfptr[5]),
		  (melt_ptr_t) ( /*_.VALDATA___V195*/ meltfptr[194]),
		  (MELTBPARSTR_PTR MELTBPARSTR_CSTRING MELTBPARSTR_CSTRING
		   ""), argtab, "", (union meltparam_un *) 0);
  }

  /*^apply */
  /*apply */
  {
    union meltparam_un argtab[3];
    memset (&argtab, 0, sizeof (argtab));
    /*^apply.arg */
    argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.PREVENV___V4*/ meltfptr[3];
    /*^apply.arg */
    argtab[1].meltbp_cstring = "CLASS_CTYPE";
    /*^apply.arg */
    argtab[2].meltbp_cstring = "warmelt-debug";
    /*_.CLASS_CTYPE__V20*/ meltfptr[19] =
      melt_apply ((meltclosure_ptr_t) ( /*_.VALIMPORT___V6*/ meltfptr[5]),
		  (melt_ptr_t) ( /*_.VALDATA___V197*/ meltfptr[196]),
		  (MELTBPARSTR_PTR MELTBPARSTR_CSTRING MELTBPARSTR_CSTRING
		   ""), argtab, "", (union meltparam_un *) 0);
  }

  /*^apply */
  /*apply */
  {
    union meltparam_un argtab[3];
    memset (&argtab, 0, sizeof (argtab));
    /*^apply.arg */
    argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.PREVENV___V4*/ meltfptr[3];
    /*^apply.arg */
    argtab[1].meltbp_cstring = "INSTALL_METHOD";
    /*^apply.arg */
    argtab[2].meltbp_cstring = "warmelt-debug";
    /*_.INSTALL_METHOD__V21*/ meltfptr[20] =
      melt_apply ((meltclosure_ptr_t) ( /*_.VALIMPORT___V6*/ meltfptr[5]),
		  (melt_ptr_t) ( /*_.VALDATA___V205*/ meltfptr[204]),
		  (MELTBPARSTR_PTR MELTBPARSTR_CSTRING MELTBPARSTR_CSTRING
		   ""), argtab, "", (union meltparam_un *) 0);
  }

  /*^apply */
  /*apply */
  {
    union meltparam_un argtab[3];
    memset (&argtab, 0, sizeof (argtab));
    /*^apply.arg */
    argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.PREVENV___V4*/ meltfptr[3];
    /*^apply.arg */
    argtab[1].meltbp_cstring = "DISCR_NULL_RECEIVER";
    /*^apply.arg */
    argtab[2].meltbp_cstring = "warmelt-debug";
    /*_.DISCR_NULL_RECEIVER__V22*/ meltfptr[21] =
      melt_apply ((meltclosure_ptr_t) ( /*_.VALIMPORT___V6*/ meltfptr[5]),
		  (melt_ptr_t) ( /*_.VALDATA___V207*/ meltfptr[206]),
		  (MELTBPARSTR_PTR MELTBPARSTR_CSTRING MELTBPARSTR_CSTRING
		   ""), argtab, "", (union meltparam_un *) 0);
  }

  /*^apply */
  /*apply */
  {
    union meltparam_un argtab[3];
    memset (&argtab, 0, sizeof (argtab));
    /*^apply.arg */
    argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.PREVENV___V4*/ meltfptr[3];
    /*^apply.arg */
    argtab[1].meltbp_cstring = "DISCR_STRING";
    /*^apply.arg */
    argtab[2].meltbp_cstring = "warmelt-debug";
    /*_.DISCR_STRING__V23*/ meltfptr[22] =
      melt_apply ((meltclosure_ptr_t) ( /*_.VALIMPORT___V6*/ meltfptr[5]),
		  (melt_ptr_t) ( /*_.VALDATA___V211*/ meltfptr[210]),
		  (MELTBPARSTR_PTR MELTBPARSTR_CSTRING MELTBPARSTR_CSTRING
		   ""), argtab, "", (union meltparam_un *) 0);
  }

  /*^apply */
  /*apply */
  {
    union meltparam_un argtab[3];
    memset (&argtab, 0, sizeof (argtab));
    /*^apply.arg */
    argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.PREVENV___V4*/ meltfptr[3];
    /*^apply.arg */
    argtab[1].meltbp_cstring = "DISCR_CONSTANT_INTEGER";
    /*^apply.arg */
    argtab[2].meltbp_cstring = "warmelt-debug";
    /*_.DISCR_CONSTANT_INTEGER__V24*/ meltfptr[23] =
      melt_apply ((meltclosure_ptr_t) ( /*_.VALIMPORT___V6*/ meltfptr[5]),
		  (melt_ptr_t) ( /*_.VALDATA___V217*/ meltfptr[216]),
		  (MELTBPARSTR_PTR MELTBPARSTR_CSTRING MELTBPARSTR_CSTRING
		   ""), argtab, "", (union meltparam_un *) 0);
  }

  /*^apply */
  /*apply */
  {
    union meltparam_un argtab[3];
    memset (&argtab, 0, sizeof (argtab));
    /*^apply.arg */
    argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.PREVENV___V4*/ meltfptr[3];
    /*^apply.arg */
    argtab[1].meltbp_cstring = "DISCR_MIXED_INTEGER";
    /*^apply.arg */
    argtab[2].meltbp_cstring = "warmelt-debug";
    /*_.DISCR_MIXED_INTEGER__V25*/ meltfptr[24] =
      melt_apply ((meltclosure_ptr_t) ( /*_.VALIMPORT___V6*/ meltfptr[5]),
		  (melt_ptr_t) ( /*_.VALDATA___V221*/ meltfptr[220]),
		  (MELTBPARSTR_PTR MELTBPARSTR_CSTRING MELTBPARSTR_CSTRING
		   ""), argtab, "", (union meltparam_un *) 0);
  }

  /*^apply */
  /*apply */
  {
    union meltparam_un argtab[3];
    memset (&argtab, 0, sizeof (argtab));
    /*^apply.arg */
    argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.PREVENV___V4*/ meltfptr[3];
    /*^apply.arg */
    argtab[1].meltbp_cstring = "DISCR_MIXED_LOCATION";
    /*^apply.arg */
    argtab[2].meltbp_cstring = "warmelt-debug";
    /*_.DISCR_MIXED_LOCATION__V26*/ meltfptr[25] =
      melt_apply ((meltclosure_ptr_t) ( /*_.VALIMPORT___V6*/ meltfptr[5]),
		  (melt_ptr_t) ( /*_.VALDATA___V225*/ meltfptr[224]),
		  (MELTBPARSTR_PTR MELTBPARSTR_CSTRING MELTBPARSTR_CSTRING
		   ""), argtab, "", (union meltparam_un *) 0);
  }

  /*^apply */
  /*apply */
  {
    union meltparam_un argtab[3];
    memset (&argtab, 0, sizeof (argtab));
    /*^apply.arg */
    argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.PREVENV___V4*/ meltfptr[3];
    /*^apply.arg */
    argtab[1].meltbp_cstring = "DISCR_MIXED_BIGINT";
    /*^apply.arg */
    argtab[2].meltbp_cstring = "warmelt-debug";
    /*_.DISCR_MIXED_BIGINT__V27*/ meltfptr[26] =
      melt_apply ((meltclosure_ptr_t) ( /*_.VALIMPORT___V6*/ meltfptr[5]),
		  (melt_ptr_t) ( /*_.VALDATA___V229*/ meltfptr[228]),
		  (MELTBPARSTR_PTR MELTBPARSTR_CSTRING MELTBPARSTR_CSTRING
		   ""), argtab, "", (union meltparam_un *) 0);
  }

  /*^apply */
  /*apply */
  {
    union meltparam_un argtab[3];
    memset (&argtab, 0, sizeof (argtab));
    /*^apply.arg */
    argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.PREVENV___V4*/ meltfptr[3];
    /*^apply.arg */
    argtab[1].meltbp_cstring = "DISCR_MULTIPLE";
    /*^apply.arg */
    argtab[2].meltbp_cstring = "warmelt-debug";
    /*_.DISCR_MULTIPLE__V28*/ meltfptr[27] =
      melt_apply ((meltclosure_ptr_t) ( /*_.VALIMPORT___V6*/ meltfptr[5]),
		  (melt_ptr_t) ( /*_.VALDATA___V233*/ meltfptr[232]),
		  (MELTBPARSTR_PTR MELTBPARSTR_CSTRING MELTBPARSTR_CSTRING
		   ""), argtab, "", (union meltparam_un *) 0);
  }

  /*^apply */
  /*apply */
  {
    union meltparam_un argtab[3];
    memset (&argtab, 0, sizeof (argtab));
    /*^apply.arg */
    argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.PREVENV___V4*/ meltfptr[3];
    /*^apply.arg */
    argtab[1].meltbp_cstring = "DISCR_ROUTINE";
    /*^apply.arg */
    argtab[2].meltbp_cstring = "warmelt-debug";
    /*_.DISCR_ROUTINE__V29*/ meltfptr[28] =
      melt_apply ((meltclosure_ptr_t) ( /*_.VALIMPORT___V6*/ meltfptr[5]),
		  (melt_ptr_t) ( /*_.VALDATA___V238*/ meltfptr[237]),
		  (MELTBPARSTR_PTR MELTBPARSTR_CSTRING MELTBPARSTR_CSTRING
		   ""), argtab, "", (union meltparam_un *) 0);
  }

  /*^apply */
  /*apply */
  {
    union meltparam_un argtab[3];
    memset (&argtab, 0, sizeof (argtab));
    /*^apply.arg */
    argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.PREVENV___V4*/ meltfptr[3];
    /*^apply.arg */
    argtab[1].meltbp_cstring = "ROUTINE_EVERY";
    /*^apply.arg */
    argtab[2].meltbp_cstring = "warmelt-debug";
    /*_.ROUTINE_EVERY__V30*/ meltfptr[29] =
      melt_apply ((meltclosure_ptr_t) ( /*_.VALIMPORT___V6*/ meltfptr[5]),
		  (melt_ptr_t) ( /*_.VALDATA___V240*/ meltfptr[239]),
		  (MELTBPARSTR_PTR MELTBPARSTR_CSTRING MELTBPARSTR_CSTRING
		   ""), argtab, "", (union meltparam_un *) 0);
  }

  /*^apply */
  /*apply */
  {
    union meltparam_un argtab[3];
    memset (&argtab, 0, sizeof (argtab));
    /*^apply.arg */
    argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.PREVENV___V4*/ meltfptr[3];
    /*^apply.arg */
    argtab[1].meltbp_cstring = "DISCR_LIST";
    /*^apply.arg */
    argtab[2].meltbp_cstring = "warmelt-debug";
    /*_.DISCR_LIST__V31*/ meltfptr[30] =
      melt_apply ((meltclosure_ptr_t) ( /*_.VALIMPORT___V6*/ meltfptr[5]),
		  (melt_ptr_t) ( /*_.VALDATA___V246*/ meltfptr[245]),
		  (MELTBPARSTR_PTR MELTBPARSTR_CSTRING MELTBPARSTR_CSTRING
		   ""), argtab, "", (union meltparam_un *) 0);
  }

  /*^apply */
  /*apply */
  {
    union meltparam_un argtab[3];
    memset (&argtab, 0, sizeof (argtab));
    /*^apply.arg */
    argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.PREVENV___V4*/ meltfptr[3];
    /*^apply.arg */
    argtab[1].meltbp_cstring = "DISCR_PAIR";
    /*^apply.arg */
    argtab[2].meltbp_cstring = "warmelt-debug";
    /*_.DISCR_PAIR__V32*/ meltfptr[31] =
      melt_apply ((meltclosure_ptr_t) ( /*_.VALIMPORT___V6*/ meltfptr[5]),
		  (melt_ptr_t) ( /*_.VALDATA___V250*/ meltfptr[249]),
		  (MELTBPARSTR_PTR MELTBPARSTR_CSTRING MELTBPARSTR_CSTRING
		   ""), argtab, "", (union meltparam_un *) 0);
  }

  /*^apply */
  /*apply */
  {
    union meltparam_un argtab[3];
    memset (&argtab, 0, sizeof (argtab));
    /*^apply.arg */
    argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.PREVENV___V4*/ meltfptr[3];
    /*^apply.arg */
    argtab[1].meltbp_cstring = "MAPOBJECT_EVERY";
    /*^apply.arg */
    argtab[2].meltbp_cstring = "warmelt-debug";
    /*_.MAPOBJECT_EVERY__V33*/ meltfptr[32] =
      melt_apply ((meltclosure_ptr_t) ( /*_.VALIMPORT___V6*/ meltfptr[5]),
		  (melt_ptr_t) ( /*_.VALDATA___V260*/ meltfptr[259]),
		  (MELTBPARSTR_PTR MELTBPARSTR_CSTRING MELTBPARSTR_CSTRING
		   ""), argtab, "", (union meltparam_un *) 0);
  }

  /*^apply */
  /*apply */
  {
    union meltparam_un argtab[3];
    memset (&argtab, 0, sizeof (argtab));
    /*^apply.arg */
    argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.PREVENV___V4*/ meltfptr[3];
    /*^apply.arg */
    argtab[1].meltbp_cstring = "COMPARE_OBJ_RANKED";
    /*^apply.arg */
    argtab[2].meltbp_cstring = "warmelt-debug";
    /*_.COMPARE_OBJ_RANKED__V34*/ meltfptr[33] =
      melt_apply ((meltclosure_ptr_t) ( /*_.VALIMPORT___V6*/ meltfptr[5]),
		  (melt_ptr_t) ( /*_.VALDATA___V262*/ meltfptr[261]),
		  (MELTBPARSTR_PTR MELTBPARSTR_CSTRING MELTBPARSTR_CSTRING
		   ""), argtab, "", (union meltparam_un *) 0);
  }

  /*^apply */
  /*apply */
  {
    union meltparam_un argtab[3];
    memset (&argtab, 0, sizeof (argtab));
    /*^apply.arg */
    argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.PREVENV___V4*/ meltfptr[3];
    /*^apply.arg */
    argtab[1].meltbp_cstring = "MULTIPLE_EVERY";
    /*^apply.arg */
    argtab[2].meltbp_cstring = "warmelt-debug";
    /*_.MULTIPLE_EVERY__V35*/ meltfptr[34] =
      melt_apply ((meltclosure_ptr_t) ( /*_.VALIMPORT___V6*/ meltfptr[5]),
		  (melt_ptr_t) ( /*_.VALDATA___V264*/ meltfptr[263]),
		  (MELTBPARSTR_PTR MELTBPARSTR_CSTRING MELTBPARSTR_CSTRING
		   ""), argtab, "", (union meltparam_un *) 0);
  }

  /*^apply */
  /*apply */
  {
    union meltparam_un argtab[3];
    memset (&argtab, 0, sizeof (argtab));
    /*^apply.arg */
    argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.PREVENV___V4*/ meltfptr[3];
    /*^apply.arg */
    argtab[1].meltbp_cstring = "DISCR_MAP_STRINGS";
    /*^apply.arg */
    argtab[2].meltbp_cstring = "warmelt-debug";
    /*_.DISCR_MAP_STRINGS__V36*/ meltfptr[35] =
      melt_apply ((meltclosure_ptr_t) ( /*_.VALIMPORT___V6*/ meltfptr[5]),
		  (melt_ptr_t) ( /*_.VALDATA___V274*/ meltfptr[273]),
		  (MELTBPARSTR_PTR MELTBPARSTR_CSTRING MELTBPARSTR_CSTRING
		   ""), argtab, "", (union meltparam_un *) 0);
  }

  /*^apply */
  /*apply */
  {
    union meltparam_un argtab[3];
    memset (&argtab, 0, sizeof (argtab));
    /*^apply.arg */
    argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.PREVENV___V4*/ meltfptr[3];
    /*^apply.arg */
    argtab[1].meltbp_cstring = "MAPSTRING_EVERY";
    /*^apply.arg */
    argtab[2].meltbp_cstring = "warmelt-debug";
    /*_.MAPSTRING_EVERY__V37*/ meltfptr[36] =
      melt_apply ((meltclosure_ptr_t) ( /*_.VALIMPORT___V6*/ meltfptr[5]),
		  (melt_ptr_t) ( /*_.VALDATA___V276*/ meltfptr[275]),
		  (MELTBPARSTR_PTR MELTBPARSTR_CSTRING MELTBPARSTR_CSTRING
		   ""), argtab, "", (union meltparam_un *) 0);
  }

  /*^apply */
  /*apply */
  {
    union meltparam_un argtab[3];
    memset (&argtab, 0, sizeof (argtab));
    /*^apply.arg */
    argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.PREVENV___V4*/ meltfptr[3];
    /*^apply.arg */
    argtab[1].meltbp_cstring = "DISCR_BUCKET_LONGS";
    /*^apply.arg */
    argtab[2].meltbp_cstring = "warmelt-debug";
    /*_.DISCR_BUCKET_LONGS__V38*/ meltfptr[37] =
      melt_apply ((meltclosure_ptr_t) ( /*_.VALIMPORT___V6*/ meltfptr[5]),
		  (melt_ptr_t) ( /*_.VALDATA___V280*/ meltfptr[279]),
		  (MELTBPARSTR_PTR MELTBPARSTR_CSTRING MELTBPARSTR_CSTRING
		   ""), argtab, "", (union meltparam_un *) 0);
  }

  /*^apply */
  /*apply */
  {
    union meltparam_un argtab[3];
    memset (&argtab, 0, sizeof (argtab));
    /*^apply.arg */
    argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.PREVENV___V4*/ meltfptr[3];
    /*^apply.arg */
    argtab[1].meltbp_cstring = "DISCR_ANY_RECEIVER";
    /*^apply.arg */
    argtab[2].meltbp_cstring = "warmelt-debug";
    /*_.DISCR_ANY_RECEIVER__V39*/ meltfptr[38] =
      melt_apply ((meltclosure_ptr_t) ( /*_.VALIMPORT___V6*/ meltfptr[5]),
		  (melt_ptr_t) ( /*_.VALDATA___V286*/ meltfptr[285]),
		  (MELTBPARSTR_PTR MELTBPARSTR_CSTRING MELTBPARSTR_CSTRING
		   ""), argtab, "", (union meltparam_un *) 0);
  }

  /*^apply */
  /*apply */
  {
    union meltparam_un argtab[3];
    memset (&argtab, 0, sizeof (argtab));
    /*^apply.arg */
    argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.PREVENV___V4*/ meltfptr[3];
    /*^apply.arg */
    argtab[1].meltbp_cstring = "CLASS_SYMBOL";
    /*^apply.arg */
    argtab[2].meltbp_cstring = "warmelt-debug";
    /*_.CLASS_SYMBOL__V40*/ meltfptr[39] =
      melt_apply ((meltclosure_ptr_t) ( /*_.VALIMPORT___V6*/ meltfptr[5]),
		  (melt_ptr_t) ( /*_.VALDATA___V298*/ meltfptr[297]),
		  (MELTBPARSTR_PTR MELTBPARSTR_CSTRING MELTBPARSTR_CSTRING
		   ""), argtab, "", (union meltparam_un *) 0);
  }

  /*^apply */
  /*apply */
  {
    union meltparam_un argtab[3];
    memset (&argtab, 0, sizeof (argtab));
    /*^apply.arg */
    argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.PREVENV___V4*/ meltfptr[3];
    /*^apply.arg */
    argtab[1].meltbp_cstring = "CLASS_CLASS";
    /*^apply.arg */
    argtab[2].meltbp_cstring = "warmelt-debug";
    /*_.CLASS_CLASS__V41*/ meltfptr[40] =
      melt_apply ((meltclosure_ptr_t) ( /*_.VALIMPORT___V6*/ meltfptr[5]),
		  (melt_ptr_t) ( /*_.VALDATA___V304*/ meltfptr[303]),
		  (MELTBPARSTR_PTR MELTBPARSTR_CSTRING MELTBPARSTR_CSTRING
		   ""), argtab, "", (union meltparam_un *) 0);
  }

  /*^apply */
  /*apply */
  {
    union meltparam_un argtab[3];
    memset (&argtab, 0, sizeof (argtab));
    /*^apply.arg */
    argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.PREVENV___V4*/ meltfptr[3];
    /*^apply.arg */
    argtab[1].meltbp_cstring = "CLASS_KEYWORD";
    /*^apply.arg */
    argtab[2].meltbp_cstring = "warmelt-debug";
    /*_.CLASS_KEYWORD__V42*/ meltfptr[41] =
      melt_apply ((meltclosure_ptr_t) ( /*_.VALIMPORT___V6*/ meltfptr[5]),
		  (melt_ptr_t) ( /*_.VALDATA___V308*/ meltfptr[307]),
		  (MELTBPARSTR_PTR MELTBPARSTR_CSTRING MELTBPARSTR_CSTRING
		   ""), argtab, "", (union meltparam_un *) 0);
  }

  /*^apply */
  /*apply */
  {
    union meltparam_un argtab[3];
    memset (&argtab, 0, sizeof (argtab));
    /*^apply.arg */
    argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.PREVENV___V4*/ meltfptr[3];
    /*^apply.arg */
    argtab[1].meltbp_cstring = "CLASS_CLONED_SYMBOL";
    /*^apply.arg */
    argtab[2].meltbp_cstring = "warmelt-debug";
    /*_.CLASS_CLONED_SYMBOL__V43*/ meltfptr[42] =
      melt_apply ((meltclosure_ptr_t) ( /*_.VALIMPORT___V6*/ meltfptr[5]),
		  (melt_ptr_t) ( /*_.VALDATA___V314*/ meltfptr[313]),
		  (MELTBPARSTR_PTR MELTBPARSTR_CSTRING MELTBPARSTR_CSTRING
		   ""), argtab, "", (union meltparam_un *) 0);
  }

  /*^apply */
  /*apply */
  {
    union meltparam_un argtab[3];
    memset (&argtab, 0, sizeof (argtab));
    /*^apply.arg */
    argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.PREVENV___V4*/ meltfptr[3];
    /*^apply.arg */
    argtab[1].meltbp_cstring = "CLASS_ENVIRONMENT";
    /*^apply.arg */
    argtab[2].meltbp_cstring = "warmelt-debug";
    /*_.CLASS_ENVIRONMENT__V44*/ meltfptr[43] =
      melt_apply ((meltclosure_ptr_t) ( /*_.VALIMPORT___V6*/ meltfptr[5]),
		  (melt_ptr_t) ( /*_.VALDATA___V320*/ meltfptr[319]),
		  (MELTBPARSTR_PTR MELTBPARSTR_CSTRING MELTBPARSTR_CSTRING
		   ""), argtab, "", (union meltparam_un *) 0);
  }

  /*^apply */
  /*apply */
  {
    union meltparam_un argtab[3];
    memset (&argtab, 0, sizeof (argtab));
    /*^apply.arg */
    argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.PREVENV___V4*/ meltfptr[3];
    /*^apply.arg */
    argtab[1].meltbp_cstring = "ENV_PREV";
    /*^apply.arg */
    argtab[2].meltbp_cstring = "warmelt-debug";
    /*_.ENV_PREV__V45*/ meltfptr[44] =
      melt_apply ((meltclosure_ptr_t) ( /*_.VALIMPORT___V6*/ meltfptr[5]),
		  (melt_ptr_t) ( /*_.VALDATA___V322*/ meltfptr[321]),
		  (MELTBPARSTR_PTR MELTBPARSTR_CSTRING MELTBPARSTR_CSTRING
		   ""), argtab, "", (union meltparam_un *) 0);
  }

  /*^apply */
  /*apply */
  {
    union meltparam_un argtab[3];
    memset (&argtab, 0, sizeof (argtab));
    /*^apply.arg */
    argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.PREVENV___V4*/ meltfptr[3];
    /*^apply.arg */
    argtab[1].meltbp_cstring = "CLASS_ANY_BINDING";
    /*^apply.arg */
    argtab[2].meltbp_cstring = "warmelt-debug";
    /*_.CLASS_ANY_BINDING__V46*/ meltfptr[45] =
      melt_apply ((meltclosure_ptr_t) ( /*_.VALIMPORT___V6*/ meltfptr[5]),
		  (melt_ptr_t) ( /*_.VALDATA___V330*/ meltfptr[329]),
		  (MELTBPARSTR_PTR MELTBPARSTR_CSTRING MELTBPARSTR_CSTRING
		   ""), argtab, "", (union meltparam_un *) 0);
  }

  /*^comment */
 /**COMMENT: after getting imported values **/ ;

 /**initial routine cdata initializer**/

  memset (meltpredefinited, 0, sizeof (meltpredefinited));
  initialize_module_meltdata_warmelt_debug (&meltfram__, meltpredefinited);
 /**initial routine body**/

  meltmod__warmelt_debug__initialmeltchunk_0 (&meltfram__, meltpredefinited);
  meltmod__warmelt_debug__initialmeltchunk_1 (&meltfram__, meltpredefinited);
  meltmod__warmelt_debug__initialmeltchunk_2 (&meltfram__, meltpredefinited);
  meltmod__warmelt_debug__initialmeltchunk_3 (&meltfram__, meltpredefinited);
  meltmod__warmelt_debug__initialmeltchunk_4 (&meltfram__, meltpredefinited);
  meltmod__warmelt_debug__initialmeltchunk_5 (&meltfram__, meltpredefinited);
  meltmod__warmelt_debug__initialmeltchunk_6 (&meltfram__, meltpredefinited);;
  goto labend_rout;
labend_rout:;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
/* popped initial frame */
  {				/* clear initial frame & return */
    melt_ptr_t retval = /*_.RETINIT___V1*/ meltfptr[0];
    memset ((void *) &meltfram__, 0, sizeof (meltfram__));
    return retval;
  }
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR

}				/* end  */

void
meltmod__warmelt_debug__initialmeltchunk_0 (struct
					    frame_melt_start_this_module_st
					    *meltmeltframptr__,
					    char meltpredefinited[])
{
#define meltfram__ (*meltmeltframptr__)
#undef meltcallcount
#define meltcallcount 0L
  (void) meltpredefinited;
/**COMMENT: start of oinibody **/ ;

  /*putslot */
  melt_assertmsg ("putslot checkobj",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALDATA___V47*/ meltfptr[46])) ==
		  MELTOBMAG_OBJECT);

  melt_assertmsg ("putslot checkoff",
		  (1 >= 0
		   && 1 <
		   melt_object_length ((melt_ptr_t)
				       ( /*_.VALDATA___V47*/ meltfptr[46]))));
  ((meltobject_ptr_t) ( /*_.VALDATA___V47*/ meltfptr[46]))->obj_vartab[1] =
    (melt_ptr_t) (
  /*_.VALSTR___V48*/ meltfptr[47]);

  /*touch:VALUEDATA_ */
  meltgc_touch ( /*_.VALDATA___V47*/ meltfptr[46]);


  melt_dbgtrace_written_object ( /*_.VALDATA___V47*/ meltfptr[46],
				"new static instance");

  /*putslot */
  melt_assertmsg ("putslot checkobj",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALDATA___V49*/ meltfptr[48])) ==
		  MELTOBMAG_OBJECT);

  melt_assertmsg ("putslot checkoff",
		  (1 >= 0
		   && 1 <
		   melt_object_length ((melt_ptr_t)
				       ( /*_.VALDATA___V49*/ meltfptr[48]))));
  ((meltobject_ptr_t) ( /*_.VALDATA___V49*/ meltfptr[48]))->obj_vartab[1] =
    (melt_ptr_t) (
  /*_.VALSTR___V50*/ meltfptr[49]);

  /*touch:VALUEDATA_ */
  meltgc_touch ( /*_.VALDATA___V49*/ meltfptr[48]);


  melt_dbgtrace_written_object ( /*_.VALDATA___V49*/ meltfptr[48],
				"new static instance");

  MELT_LOCATION ("warmelt-debug.melt:39:/ initchunk");
  /*^putslot */
  /*putslot */
  melt_assertmsg ("putslot checkobj",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALDATA___V51*/ meltfptr[50])) ==
		  MELTOBMAG_OBJECT);

  melt_assertmsg ("putslot checkoff",
		  (1 >= 0
		   && 1 <
		   melt_object_length ((melt_ptr_t)
				       ( /*_.VALDATA___V51*/ meltfptr[50]))));
  ((meltobject_ptr_t) ( /*_.VALDATA___V51*/ meltfptr[50]))->obj_vartab[1] =
    (melt_ptr_t) (
  /*_.VALSTR___V52*/ meltfptr[51]);

  MELT_LOCATION ("warmelt-debug.melt:39:/ initchunk");
  /*^putslot */
  /*putslot */
  melt_assertmsg ("putslot checkobj",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALDATA___V51*/ meltfptr[50])) ==
		  MELTOBMAG_OBJECT);

  melt_assertmsg ("putslot checkoff",
		  (4 >= 0
		   && 4 <
		   melt_object_length ((melt_ptr_t)
				       ( /*_.VALDATA___V51*/ meltfptr[50]))));
  ((meltobject_ptr_t) ( /*_.VALDATA___V51*/ meltfptr[50]))->obj_vartab[4] =
    (melt_ptr_t) (
  /*_.CLASS_ROOT__V7*/ meltfptr[6]);

  /*putupl#7 */
  melt_assertmsg ("putupl #7 checktup",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALTUP___V53*/ meltfptr[52])) ==
		  MELTOBMAG_MULTIPLE);
  melt_assertmsg ("putupl #7 checkoff",
		  (0 >= 0
		   && 0 <
		   melt_multiple_length ((melt_ptr_t)
					 ( /*_.VALTUP___V53*/
					  meltfptr[52]))));
  ((meltmultiple_ptr_t) ( /*_.VALTUP___V53*/ meltfptr[52]))->tabval[0] =
    (melt_ptr_t) ( /*_.CLASS_ROOT__V7*/ meltfptr[6]);

  /*touch:INITUP_ */
  meltgc_touch ( /*_.VALTUP___V53*/ meltfptr[52]);

  MELT_LOCATION ("warmelt-debug.melt:39:/ initchunk");
  /*^putslot */
  /*putslot */
  melt_assertmsg ("putslot checkobj",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALDATA___V51*/ meltfptr[50])) ==
		  MELTOBMAG_OBJECT);

  melt_assertmsg ("putslot checkoff",
		  (5 >= 0
		   && 5 <
		   melt_object_length ((melt_ptr_t)
				       ( /*_.VALDATA___V51*/ meltfptr[50]))));
  ((meltobject_ptr_t) ( /*_.VALDATA___V51*/ meltfptr[50]))->obj_vartab[5] =
    (melt_ptr_t) (
  /*_.VALTUP___V53*/ meltfptr[52]);

  MELT_LOCATION ("warmelt-debug.melt:39:/ initchunk");
  /*^putslot */
  /*putslot */
  melt_assertmsg ("putslot checkobj",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALDATA___V54*/ meltfptr[53])) ==
		  MELTOBMAG_OBJECT);

  melt_assertmsg ("putslot checkoff",
		  (1 >= 0
		   && 1 <
		   melt_object_length ((melt_ptr_t)
				       ( /*_.VALDATA___V54*/ meltfptr[53]))));
  ((meltobject_ptr_t) ( /*_.VALDATA___V54*/ meltfptr[53]))->obj_vartab[1] =
    (melt_ptr_t) (
  /*_.VALSTR___V55*/ meltfptr[54]);

  MELT_LOCATION ("warmelt-debug.melt:39:/ initchunk");
  /*^putslot */
  /*putslot */
  melt_assertmsg ("putslot checkobj",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALDATA___V54*/ meltfptr[53])) ==
		  MELTOBMAG_OBJECT);

  melt_assertmsg ("putslot checkoff",
		  (2 >= 0
		   && 2 <
		   melt_object_length ((melt_ptr_t)
				       ( /*_.VALDATA___V54*/ meltfptr[53]))));
  ((meltobject_ptr_t) ( /*_.VALDATA___V54*/ meltfptr[53]))->obj_vartab[2] =
    (melt_ptr_t) (
  /*_.VALDATA___V51*/ meltfptr[50]);

  MELT_LOCATION ("warmelt-debug.melt:39:/ initchunk");
  /*^touch */
  /*touch:VALUEDATA_ */
  meltgc_touch ( /*_.VALDATA___V54*/ meltfptr[53]);

  MELT_LOCATION ("warmelt-debug.melt:39:/ initchunk");
  /*^touchobj */

  melt_dbgtrace_written_object ( /*_.VALDATA___V54*/ meltfptr[53],
				"new static instance");

  MELT_LOCATION ("warmelt-debug.melt:39:/ initchunk");
  /*^putslot */
  /*putslot */
  melt_assertmsg ("putslot checkobj",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALDATA___V56*/ meltfptr[55])) ==
		  MELTOBMAG_OBJECT);

  melt_assertmsg ("putslot checkoff",
		  (1 >= 0
		   && 1 <
		   melt_object_length ((melt_ptr_t)
				       ( /*_.VALDATA___V56*/ meltfptr[55]))));
  ((meltobject_ptr_t) ( /*_.VALDATA___V56*/ meltfptr[55]))->obj_vartab[1] =
    (melt_ptr_t) (
  /*_.VALSTR___V57*/ meltfptr[56]);

  MELT_LOCATION ("warmelt-debug.melt:39:/ initchunk");
  /*^putslot */
  /*putslot */
  melt_assertmsg ("putslot checkobj",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALDATA___V56*/ meltfptr[55])) ==
		  MELTOBMAG_OBJECT);

  melt_assertmsg ("putslot checkoff",
		  (2 >= 0
		   && 2 <
		   melt_object_length ((melt_ptr_t)
				       ( /*_.VALDATA___V56*/ meltfptr[55]))));
  ((meltobject_ptr_t) ( /*_.VALDATA___V56*/ meltfptr[55]))->obj_vartab[2] =
    (melt_ptr_t) (
  /*_.VALDATA___V51*/ meltfptr[50]);

  MELT_LOCATION ("warmelt-debug.melt:39:/ initchunk");
  /*^touch */
  /*touch:VALUEDATA_ */
  meltgc_touch ( /*_.VALDATA___V56*/ meltfptr[55]);

  MELT_LOCATION ("warmelt-debug.melt:39:/ initchunk");
  /*^touchobj */

  melt_dbgtrace_written_object ( /*_.VALDATA___V56*/ meltfptr[55],
				"new static instance");

  MELT_LOCATION ("warmelt-debug.melt:39:/ initchunk");
  /*^putslot */
  /*putslot */
  melt_assertmsg ("putslot checkobj",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALDATA___V58*/ meltfptr[57])) ==
		  MELTOBMAG_OBJECT);

  melt_assertmsg ("putslot checkoff",
		  (1 >= 0
		   && 1 <
		   melt_object_length ((melt_ptr_t)
				       ( /*_.VALDATA___V58*/ meltfptr[57]))));
  ((meltobject_ptr_t) ( /*_.VALDATA___V58*/ meltfptr[57]))->obj_vartab[1] =
    (melt_ptr_t) (
  /*_.VALSTR___V59*/ meltfptr[58]);

  MELT_LOCATION ("warmelt-debug.melt:39:/ initchunk");
  /*^putslot */
  /*putslot */
  melt_assertmsg ("putslot checkobj",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALDATA___V58*/ meltfptr[57])) ==
		  MELTOBMAG_OBJECT);

  melt_assertmsg ("putslot checkoff",
		  (2 >= 0
		   && 2 <
		   melt_object_length ((melt_ptr_t)
				       ( /*_.VALDATA___V58*/ meltfptr[57]))));
  ((meltobject_ptr_t) ( /*_.VALDATA___V58*/ meltfptr[57]))->obj_vartab[2] =
    (melt_ptr_t) (
  /*_.VALDATA___V51*/ meltfptr[50]);

  MELT_LOCATION ("warmelt-debug.melt:39:/ initchunk");
  /*^touch */
  /*touch:VALUEDATA_ */
  meltgc_touch ( /*_.VALDATA___V58*/ meltfptr[57]);

  MELT_LOCATION ("warmelt-debug.melt:39:/ initchunk");
  /*^touchobj */

  melt_dbgtrace_written_object ( /*_.VALDATA___V58*/ meltfptr[57],
				"new static instance");

  /*putupl#8 */
  melt_assertmsg ("putupl #8 checktup",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALTUP___V60*/ meltfptr[59])) ==
		  MELTOBMAG_MULTIPLE);
  melt_assertmsg ("putupl #8 checkoff",
		  (0 >= 0
		   && 0 <
		   melt_multiple_length ((melt_ptr_t)
					 ( /*_.VALTUP___V60*/
					  meltfptr[59]))));
  ((meltmultiple_ptr_t) ( /*_.VALTUP___V60*/ meltfptr[59]))->tabval[0] =
    (melt_ptr_t) ( /*_.VALDATA___V54*/ meltfptr[53]);

  /*putupl#9 */
  melt_assertmsg ("putupl #9 checktup",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALTUP___V60*/ meltfptr[59])) ==
		  MELTOBMAG_MULTIPLE);
  melt_assertmsg ("putupl #9 checkoff",
		  (1 >= 0
		   && 1 <
		   melt_multiple_length ((melt_ptr_t)
					 ( /*_.VALTUP___V60*/
					  meltfptr[59]))));
  ((meltmultiple_ptr_t) ( /*_.VALTUP___V60*/ meltfptr[59]))->tabval[1] =
    (melt_ptr_t) ( /*_.VALDATA___V56*/ meltfptr[55]);

  /*putupl#10 */
  melt_assertmsg ("putupl #10 checktup",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALTUP___V60*/ meltfptr[59])) ==
		  MELTOBMAG_MULTIPLE);
  melt_assertmsg ("putupl #10 checkoff",
		  (2 >= 0
		   && 2 <
		   melt_multiple_length ((melt_ptr_t)
					 ( /*_.VALTUP___V60*/
					  meltfptr[59]))));
  ((meltmultiple_ptr_t) ( /*_.VALTUP___V60*/ meltfptr[59]))->tabval[2] =
    (melt_ptr_t) ( /*_.VALDATA___V58*/ meltfptr[57]);

  /*touch:INITUP_ */
  meltgc_touch ( /*_.VALTUP___V60*/ meltfptr[59]);

  MELT_LOCATION ("warmelt-debug.melt:39:/ initchunk");
  /*^putslot */
  /*putslot */
  melt_assertmsg ("putslot checkobj",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALDATA___V51*/ meltfptr[50])) ==
		  MELTOBMAG_OBJECT);

  melt_assertmsg ("putslot checkoff",
		  (6 >= 0
		   && 6 <
		   melt_object_length ((melt_ptr_t)
				       ( /*_.VALDATA___V51*/ meltfptr[50]))));
  ((meltobject_ptr_t) ( /*_.VALDATA___V51*/ meltfptr[50]))->obj_vartab[6] =
    (melt_ptr_t) (
  /*_.VALTUP___V60*/ meltfptr[59]);

  MELT_LOCATION ("warmelt-debug.melt:39:/ initchunk");
  /*^touch */
  /*touch:VALUEDATA_ */
  meltgc_touch ( /*_.VALDATA___V51*/ meltfptr[50]);

  MELT_LOCATION ("warmelt-debug.melt:39:/ initchunk");
  /*^touchobj */

  melt_dbgtrace_written_object ( /*_.VALDATA___V51*/ meltfptr[50],
				"new static instance");

  /*putslot */
  melt_assertmsg ("putslot checkobj",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALDATA___V61*/ meltfptr[60])) ==
		  MELTOBMAG_OBJECT);

  melt_assertmsg ("putslot checkoff",
		  (1 >= 0
		   && 1 <
		   melt_object_length ((melt_ptr_t)
				       ( /*_.VALDATA___V61*/ meltfptr[60]))));
  ((meltobject_ptr_t) ( /*_.VALDATA___V61*/ meltfptr[60]))->obj_vartab[1] =
    (melt_ptr_t) (
  /*_.VALSTR___V62*/ meltfptr[61]);

  /*touch:VALUEDATA_ */
  meltgc_touch ( /*_.VALDATA___V61*/ meltfptr[60]);


  melt_dbgtrace_written_object ( /*_.VALDATA___V61*/ meltfptr[60],
				"new static instance");

  /*putslot */
  melt_assertmsg ("putslot checkobj",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALDATA___V63*/ meltfptr[62])) ==
		  MELTOBMAG_OBJECT);

  melt_assertmsg ("putslot checkoff",
		  (1 >= 0
		   && 1 <
		   melt_object_length ((melt_ptr_t)
				       ( /*_.VALDATA___V63*/ meltfptr[62]))));
  ((meltobject_ptr_t) ( /*_.VALDATA___V63*/ meltfptr[62]))->obj_vartab[1] =
    (melt_ptr_t) (
  /*_.VALSTR___V64*/ meltfptr[63]);

  /*touch:VALUEDATA_ */
  meltgc_touch ( /*_.VALDATA___V63*/ meltfptr[62]);


  melt_dbgtrace_written_object ( /*_.VALDATA___V63*/ meltfptr[62],
				"new static instance");

  /*putslot */
  melt_assertmsg ("putslot checkobj",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALDATA___V65*/ meltfptr[64])) ==
		  MELTOBMAG_OBJECT);

  melt_assertmsg ("putslot checkoff",
		  (1 >= 0
		   && 1 <
		   melt_object_length ((melt_ptr_t)
				       ( /*_.VALDATA___V65*/ meltfptr[64]))));
  ((meltobject_ptr_t) ( /*_.VALDATA___V65*/ meltfptr[64]))->obj_vartab[1] =
    (melt_ptr_t) (
  /*_.VALSTR___V66*/ meltfptr[65]);

  /*touch:VALUEDATA_ */
  meltgc_touch ( /*_.VALDATA___V65*/ meltfptr[64]);


  melt_dbgtrace_written_object ( /*_.VALDATA___V65*/ meltfptr[64],
				"new static instance");

  /*putslot */
  melt_assertmsg ("putslot checkobj",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALDATA___V67*/ meltfptr[66])) ==
		  MELTOBMAG_OBJECT);

  melt_assertmsg ("putslot checkoff",
		  (1 >= 0
		   && 1 <
		   melt_object_length ((melt_ptr_t)
				       ( /*_.VALDATA___V67*/ meltfptr[66]))));
  ((meltobject_ptr_t) ( /*_.VALDATA___V67*/ meltfptr[66]))->obj_vartab[1] =
    (melt_ptr_t) (
  /*_.VALSTR___V68*/ meltfptr[67]);

  /*touch:VALUEDATA_ */
  meltgc_touch ( /*_.VALDATA___V67*/ meltfptr[66]);


  melt_dbgtrace_written_object ( /*_.VALDATA___V67*/ meltfptr[66],
				"new static instance");

  MELT_LOCATION ("warmelt-debug.melt:50:/ initchunk");
  /*^putslot */
  /*putslot */
  melt_assertmsg ("putslot checkobj",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALDATA___V69*/ meltfptr[68])) ==
		  MELTOBMAG_OBJECT);

  melt_assertmsg ("putslot checkoff",
		  (1 >= 0
		   && 1 <
		   melt_object_length ((melt_ptr_t)
				       ( /*_.VALDATA___V69*/ meltfptr[68]))));
  ((meltobject_ptr_t) ( /*_.VALDATA___V69*/ meltfptr[68]))->obj_vartab[1] =
    (melt_ptr_t) (
  /*_.VALSTR___V70*/ meltfptr[69]);

  MELT_LOCATION ("warmelt-debug.melt:50:/ initchunk");
  /*^putslot */
  /*putslot */
  melt_assertmsg ("putslot checkobj",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALDATA___V69*/ meltfptr[68])) ==
		  MELTOBMAG_OBJECT);

  melt_assertmsg ("putslot checkoff",
		  (4 >= 0
		   && 4 <
		   melt_object_length ((melt_ptr_t)
				       ( /*_.VALDATA___V69*/ meltfptr[68]))));
  ((meltobject_ptr_t) ( /*_.VALDATA___V69*/ meltfptr[68]))->obj_vartab[4] =
    (melt_ptr_t) (
  /*_.VALDATA___V51*/ meltfptr[50]);

  /*putupl#11 */
  melt_assertmsg ("putupl #11 checktup",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALTUP___V71*/ meltfptr[70])) ==
		  MELTOBMAG_MULTIPLE);
  melt_assertmsg ("putupl #11 checkoff",
		  (0 >= 0
		   && 0 <
		   melt_multiple_length ((melt_ptr_t)
					 ( /*_.VALTUP___V71*/
					  meltfptr[70]))));
  ((meltmultiple_ptr_t) ( /*_.VALTUP___V71*/ meltfptr[70]))->tabval[0] =
    (melt_ptr_t) ( /*_.CLASS_ROOT__V7*/ meltfptr[6]);

  /*putupl#12 */
  melt_assertmsg ("putupl #12 checktup",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALTUP___V71*/ meltfptr[70])) ==
		  MELTOBMAG_MULTIPLE);
  melt_assertmsg ("putupl #12 checkoff",
		  (1 >= 0
		   && 1 <
		   melt_multiple_length ((melt_ptr_t)
					 ( /*_.VALTUP___V71*/
					  meltfptr[70]))));
  ((meltmultiple_ptr_t) ( /*_.VALTUP___V71*/ meltfptr[70]))->tabval[1] =
    (melt_ptr_t) ( /*_.VALDATA___V51*/ meltfptr[50]);

  /*touch:INITUP_ */
  meltgc_touch ( /*_.VALTUP___V71*/ meltfptr[70]);

  MELT_LOCATION ("warmelt-debug.melt:50:/ initchunk");
  /*^putslot */
  /*putslot */
  melt_assertmsg ("putslot checkobj",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALDATA___V69*/ meltfptr[68])) ==
		  MELTOBMAG_OBJECT);

  melt_assertmsg ("putslot checkoff",
		  (5 >= 0
		   && 5 <
		   melt_object_length ((melt_ptr_t)
				       ( /*_.VALDATA___V69*/ meltfptr[68]))));
  ((meltobject_ptr_t) ( /*_.VALDATA___V69*/ meltfptr[68]))->obj_vartab[5] =
    (melt_ptr_t) (
  /*_.VALTUP___V71*/ meltfptr[70]);

  /*putupl#13 */
  melt_assertmsg ("putupl #13 checktup",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALTUP___V72*/ meltfptr[71])) ==
		  MELTOBMAG_MULTIPLE);
  melt_assertmsg ("putupl #13 checkoff",
		  (0 >= 0
		   && 0 <
		   melt_multiple_length ((melt_ptr_t)
					 ( /*_.VALTUP___V72*/
					  meltfptr[71]))));
  ((meltmultiple_ptr_t) ( /*_.VALTUP___V72*/ meltfptr[71]))->tabval[0] =
    (melt_ptr_t) ( /*_.VALDATA___V54*/ meltfptr[53]);

  /*putupl#14 */
  melt_assertmsg ("putupl #14 checktup",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALTUP___V72*/ meltfptr[71])) ==
		  MELTOBMAG_MULTIPLE);
  melt_assertmsg ("putupl #14 checkoff",
		  (1 >= 0
		   && 1 <
		   melt_multiple_length ((melt_ptr_t)
					 ( /*_.VALTUP___V72*/
					  meltfptr[71]))));
  ((meltmultiple_ptr_t) ( /*_.VALTUP___V72*/ meltfptr[71]))->tabval[1] =
    (melt_ptr_t) ( /*_.VALDATA___V56*/ meltfptr[55]);

  /*putupl#15 */
  melt_assertmsg ("putupl #15 checktup",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALTUP___V72*/ meltfptr[71])) ==
		  MELTOBMAG_MULTIPLE);
  melt_assertmsg ("putupl #15 checkoff",
		  (2 >= 0
		   && 2 <
		   melt_multiple_length ((melt_ptr_t)
					 ( /*_.VALTUP___V72*/
					  meltfptr[71]))));
  ((meltmultiple_ptr_t) ( /*_.VALTUP___V72*/ meltfptr[71]))->tabval[2] =
    (melt_ptr_t) ( /*_.VALDATA___V58*/ meltfptr[57]);

  /*touch:INITUP_ */
  meltgc_touch ( /*_.VALTUP___V72*/ meltfptr[71]);

  MELT_LOCATION ("warmelt-debug.melt:50:/ initchunk");
  /*^putslot */
  /*putslot */
  melt_assertmsg ("putslot checkobj",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALDATA___V69*/ meltfptr[68])) ==
		  MELTOBMAG_OBJECT);

  melt_assertmsg ("putslot checkoff",
		  (6 >= 0
		   && 6 <
		   melt_object_length ((melt_ptr_t)
				       ( /*_.VALDATA___V69*/ meltfptr[68]))));
  ((meltobject_ptr_t) ( /*_.VALDATA___V69*/ meltfptr[68]))->obj_vartab[6] =
    (melt_ptr_t) (
  /*_.VALTUP___V72*/ meltfptr[71]);

  MELT_LOCATION ("warmelt-debug.melt:50:/ initchunk");
  /*^touch */
  /*touch:VALUEDATA_ */
  meltgc_touch ( /*_.VALDATA___V69*/ meltfptr[68]);

  MELT_LOCATION ("warmelt-debug.melt:50:/ initchunk");
  /*^touchobj */

  melt_dbgtrace_written_object ( /*_.VALDATA___V69*/ meltfptr[68],
				"new static instance");

  /*putslot */
  melt_assertmsg ("putslot checkobj",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALDATA___V73*/ meltfptr[72])) ==
		  MELTOBMAG_OBJECT);

  melt_assertmsg ("putslot checkoff",
		  (1 >= 0
		   && 1 <
		   melt_object_length ((melt_ptr_t)
				       ( /*_.VALDATA___V73*/ meltfptr[72]))));
  ((meltobject_ptr_t) ( /*_.VALDATA___V73*/ meltfptr[72]))->obj_vartab[1] =
    (melt_ptr_t) (
  /*_.VALSTR___V74*/ meltfptr[73]);

  /*touch:VALUEDATA_ */
  meltgc_touch ( /*_.VALDATA___V73*/ meltfptr[72]);


  melt_dbgtrace_written_object ( /*_.VALDATA___V73*/ meltfptr[72],
				"new static instance");

  MELT_LOCATION ("warmelt-debug.melt:61:/ initchunk");
  /*^putslot */
  /*putslot */
  melt_assertmsg ("putslot checkobj",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALDATA___V75*/ meltfptr[74])) ==
		  MELTOBMAG_OBJECT);

  melt_assertmsg ("putslot checkoff",
		  (1 >= 0
		   && 1 <
		   melt_object_length ((melt_ptr_t)
				       ( /*_.VALDATA___V75*/ meltfptr[74]))));
  ((meltobject_ptr_t) ( /*_.VALDATA___V75*/ meltfptr[74]))->obj_vartab[1] =
    (melt_ptr_t) (
  /*_.VALSTR___V76*/ meltfptr[75]);

  /*touch:INITUP_ */
  meltgc_touch ( /*_.VALTUP___V77*/ meltfptr[76]);

  MELT_LOCATION ("warmelt-debug.melt:61:/ initchunk");
  /*^putslot */
  /*putslot */
  melt_assertmsg ("putslot checkobj",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALDATA___V75*/ meltfptr[74])) ==
		  MELTOBMAG_OBJECT);

  melt_assertmsg ("putslot checkoff",
		  (2 >= 0
		   && 2 <
		   melt_object_length ((melt_ptr_t)
				       ( /*_.VALDATA___V75*/ meltfptr[74]))));
  ((meltobject_ptr_t) ( /*_.VALDATA___V75*/ meltfptr[74]))->obj_vartab[2] =
    (melt_ptr_t) (
  /*_.VALTUP___V77*/ meltfptr[76]);

  MELT_LOCATION ("warmelt-debug.melt:61:/ initchunk");
  /*^putslot */
  /*putslot */
  melt_assertmsg ("putslot checkobj",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALDATA___V75*/ meltfptr[74])) ==
		  MELTOBMAG_OBJECT);

  melt_assertmsg ("putslot checkoff",
		  (3 >= 0
		   && 3 <
		   melt_object_length ((melt_ptr_t)
				       ( /*_.VALDATA___V75*/ meltfptr[74]))));
  ((meltobject_ptr_t) ( /*_.VALDATA___V75*/ meltfptr[74]))->obj_vartab[3] =
    (melt_ptr_t) (
		    ((melt_ptr_t) (MELT_PREDEF (CTYPE_LONG))));

  /*putupl#16 */
  melt_assertmsg ("putupl #16 checktup",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALTUP___V79*/ meltfptr[78])) ==
		  MELTOBMAG_MULTIPLE);
  melt_assertmsg ("putupl #16 checkoff",
		  (0 >= 0
		   && 0 <
		   melt_multiple_length ((melt_ptr_t)
					 ( /*_.VALTUP___V79*/
					  meltfptr[78]))));
  ((meltmultiple_ptr_t) ( /*_.VALTUP___V79*/ meltfptr[78]))->tabval[0] =
    (melt_ptr_t) ( /*_.VALSTR___V78*/ meltfptr[77]);

  /*touch:INITUP_ */
  meltgc_touch ( /*_.VALTUP___V79*/ meltfptr[78]);

  MELT_LOCATION ("warmelt-debug.melt:61:/ initchunk");
  /*^putslot */
  /*putslot */
  melt_assertmsg ("putslot checkobj",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALDATA___V75*/ meltfptr[74])) ==
		  MELTOBMAG_OBJECT);

  melt_assertmsg ("putslot checkoff",
		  (4 >= 0
		   && 4 <
		   melt_object_length ((melt_ptr_t)
				       ( /*_.VALDATA___V75*/ meltfptr[74]))));
  ((meltobject_ptr_t) ( /*_.VALDATA___V75*/ meltfptr[74]))->obj_vartab[4] =
    (melt_ptr_t) (
  /*_.VALTUP___V79*/ meltfptr[78]);

  MELT_LOCATION ("warmelt-debug.melt:61:/ initchunk");
  /*^touch */
  /*touch:VALUEDATA_ */
  meltgc_touch ( /*_.VALDATA___V75*/ meltfptr[74]);

  MELT_LOCATION ("warmelt-debug.melt:61:/ initchunk");
  /*^touchobj */

  melt_dbgtrace_written_object ( /*_.VALDATA___V75*/ meltfptr[74],
				"new static instance");

  /*putslot */
  melt_assertmsg ("putslot checkobj",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALDATA___V80*/ meltfptr[79])) ==
		  MELTOBMAG_OBJECT);

  melt_assertmsg ("putslot checkoff",
		  (1 >= 0
		   && 1 <
		   melt_object_length ((melt_ptr_t)
				       ( /*_.VALDATA___V80*/ meltfptr[79]))));
  ((meltobject_ptr_t) ( /*_.VALDATA___V80*/ meltfptr[79]))->obj_vartab[1] =
    (melt_ptr_t) (
  /*_.VALSTR___V81*/ meltfptr[80]);

  /*touch:VALUEDATA_ */
  meltgc_touch ( /*_.VALDATA___V80*/ meltfptr[79]);


  melt_dbgtrace_written_object ( /*_.VALDATA___V80*/ meltfptr[79],
				"new static instance");

  MELT_LOCATION ("warmelt-debug.melt:69:/ initchunk");
  /*^putslot */
  /*putslot */
  melt_assertmsg ("putslot checkobj",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALDATA___V82*/ meltfptr[81])) ==
		  MELTOBMAG_OBJECT);

  melt_assertmsg ("putslot checkoff",
		  (1 >= 0
		   && 1 <
		   melt_object_length ((melt_ptr_t)
				       ( /*_.VALDATA___V82*/ meltfptr[81]))));
  ((meltobject_ptr_t) ( /*_.VALDATA___V82*/ meltfptr[81]))->obj_vartab[1] =
    (melt_ptr_t) (
  /*_.VALSTR___V83*/ meltfptr[82]);

  /*putslot */
  melt_assertmsg ("putslot checkobj",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALDATA___V85*/ meltfptr[84])) ==
		  MELTOBMAG_OBJECT);

  melt_assertmsg ("putslot checkoff",
		  (1 >= 0
		   && 1 <
		   melt_object_length ((melt_ptr_t)
				       ( /*_.VALDATA___V85*/ meltfptr[84]))));
  ((meltobject_ptr_t) ( /*_.VALDATA___V85*/ meltfptr[84]))->obj_vartab[1] =
    (melt_ptr_t) (
  /*_.VALSTR___V86*/ meltfptr[85]);

  /*touch:VALUEDATA_ */
  meltgc_touch ( /*_.VALDATA___V85*/ meltfptr[84]);


  melt_dbgtrace_written_object ( /*_.VALDATA___V85*/ meltfptr[84],
				"new static instance");

  MELT_LOCATION ("warmelt-debug.melt:69:/ initchunk");
  /*^putslot */
  /*putslot */
  melt_assertmsg ("putslot checkobj",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALDATA___V84*/ meltfptr[83])) ==
		  MELTOBMAG_OBJECT);

  melt_assertmsg ("putslot checkoff",
		  (0 >= 0
		   && 0 <
		   melt_object_length ((melt_ptr_t)
				       ( /*_.VALDATA___V84*/ meltfptr[83]))));
  ((meltobject_ptr_t) ( /*_.VALDATA___V84*/ meltfptr[83]))->obj_vartab[0] =
    (melt_ptr_t) (
  /*_.VALDATA___V85*/ meltfptr[84]);

  MELT_LOCATION ("warmelt-debug.melt:69:/ initchunk");
  /*^putslot */
  /*putslot */
  melt_assertmsg ("putslot checkobj",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALDATA___V84*/ meltfptr[83])) ==
		  MELTOBMAG_OBJECT);

  melt_assertmsg ("putslot checkoff",
		  (1 >= 0
		   && 1 <
		   melt_object_length ((melt_ptr_t)
				       ( /*_.VALDATA___V84*/ meltfptr[83]))));
  ((meltobject_ptr_t) ( /*_.VALDATA___V84*/ meltfptr[83]))->obj_vartab[1] =
    (melt_ptr_t) (
		    ((melt_ptr_t) (MELT_PREDEF (CTYPE_LONG))));

  MELT_LOCATION ("warmelt-debug.melt:69:/ initchunk");
  /*^touch */
  /*touch:VALUEDATA_ */
  meltgc_touch ( /*_.VALDATA___V84*/ meltfptr[83]);

  MELT_LOCATION ("warmelt-debug.melt:69:/ initchunk");
  /*^touchobj */

  melt_dbgtrace_written_object ( /*_.VALDATA___V84*/ meltfptr[83],
				"new static instance");

  /*putupl#17 */
  melt_assertmsg ("putupl #17 checktup",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALTUP___V87*/ meltfptr[86])) ==
		  MELTOBMAG_MULTIPLE);
  melt_assertmsg ("putupl #17 checkoff",
		  (0 >= 0
		   && 0 <
		   melt_multiple_length ((melt_ptr_t)
					 ( /*_.VALTUP___V87*/
					  meltfptr[86]))));
  ((meltmultiple_ptr_t) ( /*_.VALTUP___V87*/ meltfptr[86]))->tabval[0] =
    (melt_ptr_t) ( /*_.VALDATA___V84*/ meltfptr[83]);

  /*touch:INITUP_ */
  meltgc_touch ( /*_.VALTUP___V87*/ meltfptr[86]);

  MELT_LOCATION ("warmelt-debug.melt:69:/ initchunk");
  /*^putslot */
  /*putslot */
  melt_assertmsg ("putslot checkobj",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALDATA___V82*/ meltfptr[81])) ==
		  MELTOBMAG_OBJECT);

  melt_assertmsg ("putslot checkoff",
		  (2 >= 0
		   && 2 <
		   melt_object_length ((melt_ptr_t)
				       ( /*_.VALDATA___V82*/ meltfptr[81]))));
  ((meltobject_ptr_t) ( /*_.VALDATA___V82*/ meltfptr[81]))->obj_vartab[2] =
    (melt_ptr_t) (
  /*_.VALTUP___V87*/ meltfptr[86]);

  MELT_LOCATION ("warmelt-debug.melt:69:/ initchunk");
  /*^putslot */
  /*putslot */
  melt_assertmsg ("putslot checkobj",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALDATA___V82*/ meltfptr[81])) ==
		  MELTOBMAG_OBJECT);

  melt_assertmsg ("putslot checkoff",
		  (3 >= 0
		   && 3 <
		   melt_object_length ((melt_ptr_t)
				       ( /*_.VALDATA___V82*/ meltfptr[81]))));
  ((meltobject_ptr_t) ( /*_.VALDATA___V82*/ meltfptr[81]))->obj_vartab[3] =
    (melt_ptr_t) (
		    ((melt_ptr_t) (MELT_PREDEF (CTYPE_LONG))));

  /*putupl#18 */
  melt_assertmsg ("putupl #18 checktup",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALTUP___V90*/ meltfptr[89])) ==
		  MELTOBMAG_MULTIPLE);
  melt_assertmsg ("putupl #18 checkoff",
		  (0 >= 0
		   && 0 <
		   melt_multiple_length ((melt_ptr_t)
					 ( /*_.VALTUP___V90*/
					  meltfptr[89]))));
  ((meltmultiple_ptr_t) ( /*_.VALTUP___V90*/ meltfptr[89]))->tabval[0] =
    (melt_ptr_t) ( /*_.VALSTR___V88*/ meltfptr[87]);

  /*putupl#19 */
  melt_assertmsg ("putupl #19 checktup",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALTUP___V90*/ meltfptr[89])) ==
		  MELTOBMAG_MULTIPLE);
  melt_assertmsg ("putupl #19 checkoff",
		  (1 >= 0
		   && 1 <
		   melt_multiple_length ((melt_ptr_t)
					 ( /*_.VALTUP___V90*/
					  meltfptr[89]))));
  ((meltmultiple_ptr_t) ( /*_.VALTUP___V90*/ meltfptr[89]))->tabval[1] =
    (melt_ptr_t) ( /*_.VALDATA___V85*/ meltfptr[84]);

  /*putupl#20 */
  melt_assertmsg ("putupl #20 checktup",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALTUP___V90*/ meltfptr[89])) ==
		  MELTOBMAG_MULTIPLE);
  melt_assertmsg ("putupl #20 checkoff",
		  (2 >= 0
		   && 2 <
		   melt_multiple_length ((melt_ptr_t)
					 ( /*_.VALTUP___V90*/
					  meltfptr[89]))));
  ((meltmultiple_ptr_t) ( /*_.VALTUP___V90*/ meltfptr[89]))->tabval[2] =
    (melt_ptr_t) ( /*_.VALSTR___V89*/ meltfptr[88]);

  /*touch:INITUP_ */
  meltgc_touch ( /*_.VALTUP___V90*/ meltfptr[89]);

  MELT_LOCATION ("warmelt-debug.melt:69:/ initchunk");
  /*^putslot */
  /*putslot */
  melt_assertmsg ("putslot checkobj",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALDATA___V82*/ meltfptr[81])) ==
		  MELTOBMAG_OBJECT);

  melt_assertmsg ("putslot checkoff",
		  (4 >= 0
		   && 4 <
		   melt_object_length ((melt_ptr_t)
				       ( /*_.VALDATA___V82*/ meltfptr[81]))));
  ((meltobject_ptr_t) ( /*_.VALDATA___V82*/ meltfptr[81]))->obj_vartab[4] =
    (melt_ptr_t) (
  /*_.VALTUP___V90*/ meltfptr[89]);

  MELT_LOCATION ("warmelt-debug.melt:69:/ initchunk");
  /*^touch */
  /*touch:VALUEDATA_ */
  meltgc_touch ( /*_.VALDATA___V82*/ meltfptr[81]);

  MELT_LOCATION ("warmelt-debug.melt:69:/ initchunk");
  /*^touchobj */

  melt_dbgtrace_written_object ( /*_.VALDATA___V82*/ meltfptr[81],
				"new static instance");

  /*putslot */
  melt_assertmsg ("putslot checkobj",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALDATA___V91*/ meltfptr[90])) ==
		  MELTOBMAG_OBJECT);

  melt_assertmsg ("putslot checkoff",
		  (1 >= 0
		   && 1 <
		   melt_object_length ((melt_ptr_t)
				       ( /*_.VALDATA___V91*/ meltfptr[90]))));
  ((meltobject_ptr_t) ( /*_.VALDATA___V91*/ meltfptr[90]))->obj_vartab[1] =
    (melt_ptr_t) (
  /*_.VALSTR___V92*/ meltfptr[91]);

  /*touch:VALUEDATA_ */
  meltgc_touch ( /*_.VALDATA___V91*/ meltfptr[90]);


  melt_dbgtrace_written_object ( /*_.VALDATA___V91*/ meltfptr[90],
				"new static instance");

  MELT_LOCATION ("warmelt-debug.melt:74:/ initchunk");
  /*^putslot */
  /*putslot */
  melt_assertmsg ("putslot checkobj",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALDATA___V93*/ meltfptr[92])) ==
		  MELTOBMAG_OBJECT);

  melt_assertmsg ("putslot checkoff",
		  (1 >= 0
		   && 1 <
		   melt_object_length ((melt_ptr_t)
				       ( /*_.VALDATA___V93*/ meltfptr[92]))));
  ((meltobject_ptr_t) ( /*_.VALDATA___V93*/ meltfptr[92]))->obj_vartab[1] =
    (melt_ptr_t) (
  /*_.VALSTR___V94*/ meltfptr[93]);

  /*touch:INITUP_ */
  meltgc_touch ( /*_.VALTUP___V95*/ meltfptr[94]);

  MELT_LOCATION ("warmelt-debug.melt:74:/ initchunk");
  /*^putslot */
  /*putslot */
  melt_assertmsg ("putslot checkobj",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALDATA___V93*/ meltfptr[92])) ==
		  MELTOBMAG_OBJECT);

  melt_assertmsg ("putslot checkoff",
		  (2 >= 0
		   && 2 <
		   melt_object_length ((melt_ptr_t)
				       ( /*_.VALDATA___V93*/ meltfptr[92]))));
  ((meltobject_ptr_t) ( /*_.VALDATA___V93*/ meltfptr[92]))->obj_vartab[2] =
    (melt_ptr_t) (
  /*_.VALTUP___V95*/ meltfptr[94]);

  MELT_LOCATION ("warmelt-debug.melt:74:/ initchunk");
  /*^putslot */
  /*putslot */
  melt_assertmsg ("putslot checkobj",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALDATA___V93*/ meltfptr[92])) ==
		  MELTOBMAG_OBJECT);

  melt_assertmsg ("putslot checkoff",
		  (3 >= 0
		   && 3 <
		   melt_object_length ((melt_ptr_t)
				       ( /*_.VALDATA___V93*/ meltfptr[92]))));
  ((meltobject_ptr_t) ( /*_.VALDATA___V93*/ meltfptr[92]))->obj_vartab[3] =
    (melt_ptr_t) (
		    ((melt_ptr_t) (MELT_PREDEF (CTYPE_LONG))));

  /*putupl#21 */
  melt_assertmsg ("putupl #21 checktup",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALTUP___V97*/ meltfptr[96])) ==
		  MELTOBMAG_MULTIPLE);
  melt_assertmsg ("putupl #21 checkoff",
		  (0 >= 0
		   && 0 <
		   melt_multiple_length ((melt_ptr_t)
					 ( /*_.VALTUP___V97*/
					  meltfptr[96]))));
  ((meltmultiple_ptr_t) ( /*_.VALTUP___V97*/ meltfptr[96]))->tabval[0] =
    (melt_ptr_t) ( /*_.VALSTR___V96*/ meltfptr[95]);

  /*touch:INITUP_ */
  meltgc_touch ( /*_.VALTUP___V97*/ meltfptr[96]);

  MELT_LOCATION ("warmelt-debug.melt:74:/ initchunk");
  /*^putslot */
  /*putslot */
  melt_assertmsg ("putslot checkobj",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALDATA___V93*/ meltfptr[92])) ==
		  MELTOBMAG_OBJECT);

  melt_assertmsg ("putslot checkoff",
		  (4 >= 0
		   && 4 <
		   melt_object_length ((melt_ptr_t)
				       ( /*_.VALDATA___V93*/ meltfptr[92]))));
  ((meltobject_ptr_t) ( /*_.VALDATA___V93*/ meltfptr[92]))->obj_vartab[4] =
    (melt_ptr_t) (
  /*_.VALTUP___V97*/ meltfptr[96]);

  MELT_LOCATION ("warmelt-debug.melt:74:/ initchunk");
  /*^touch */
  /*touch:VALUEDATA_ */
  meltgc_touch ( /*_.VALDATA___V93*/ meltfptr[92]);

  MELT_LOCATION ("warmelt-debug.melt:74:/ initchunk");
  /*^touchobj */

  melt_dbgtrace_written_object ( /*_.VALDATA___V93*/ meltfptr[92],
				"new static instance");

  /*putslot */
  melt_assertmsg ("putslot checkobj",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALDATA___V98*/ meltfptr[97])) ==
		  MELTOBMAG_OBJECT);

  melt_assertmsg ("putslot checkoff",
		  (1 >= 0
		   && 1 <
		   melt_object_length ((melt_ptr_t)
				       ( /*_.VALDATA___V98*/ meltfptr[97]))));
  ((meltobject_ptr_t) ( /*_.VALDATA___V98*/ meltfptr[97]))->obj_vartab[1] =
    (melt_ptr_t) (
  /*_.VALSTR___V99*/ meltfptr[98]);

  /*touch:VALUEDATA_ */
  meltgc_touch ( /*_.VALDATA___V98*/ meltfptr[97]);


  melt_dbgtrace_written_object ( /*_.VALDATA___V98*/ meltfptr[97],
				"new static instance");

  MELT_LOCATION ("warmelt-debug.melt:77:/ initchunk");
  /*^putslot */
  /*putslot */
  melt_assertmsg ("putslot checkobj",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALDATA___V100*/ meltfptr[99])) ==
		  MELTOBMAG_OBJECT);

  melt_assertmsg ("putslot checkoff",
		  (1 >= 0
		   && 1 <
		   melt_object_length ((melt_ptr_t)
				       ( /*_.VALDATA___V100*/
					meltfptr[99]))));
  ((meltobject_ptr_t) ( /*_.VALDATA___V100*/ meltfptr[99]))->obj_vartab[1] =
    (melt_ptr_t) (
  /*_.VALSTR___V101*/ meltfptr[100]);

  MELT_LOCATION ("warmelt-debug.melt:77:/ initchunk");
  /*^putslot */
  /*putslot */
  melt_assertmsg ("putslot checkobj",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALDATA___V102*/ meltfptr[101])) ==
		  MELTOBMAG_OBJECT);

  melt_assertmsg ("putslot checkoff",
		  (0 >= 0
		   && 0 <
		   melt_object_length ((melt_ptr_t)
				       ( /*_.VALDATA___V102*/
					meltfptr[101]))));
  ((meltobject_ptr_t) ( /*_.VALDATA___V102*/ meltfptr[101]))->obj_vartab[0] =
    (melt_ptr_t) (
  /*_.VALDATA___V85*/ meltfptr[84]);

  MELT_LOCATION ("warmelt-debug.melt:77:/ initchunk");
  /*^putslot */
  /*putslot */
  melt_assertmsg ("putslot checkobj",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALDATA___V102*/ meltfptr[101])) ==
		  MELTOBMAG_OBJECT);

  melt_assertmsg ("putslot checkoff",
		  (1 >= 0
		   && 1 <
		   melt_object_length ((melt_ptr_t)
				       ( /*_.VALDATA___V102*/
					meltfptr[101]))));
  ((meltobject_ptr_t) ( /*_.VALDATA___V102*/ meltfptr[101]))->obj_vartab[1] =
    (melt_ptr_t) (
		    ((melt_ptr_t) (MELT_PREDEF (CTYPE_LONG))));

  MELT_LOCATION ("warmelt-debug.melt:77:/ initchunk");
  /*^touch */
  /*touch:VALUEDATA_ */
  meltgc_touch ( /*_.VALDATA___V102*/ meltfptr[101]);

  MELT_LOCATION ("warmelt-debug.melt:77:/ initchunk");
  /*^touchobj */

  melt_dbgtrace_written_object ( /*_.VALDATA___V102*/ meltfptr[101],
				"new static instance");

  /*putslot */
  melt_assertmsg ("putslot checkobj",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALDATA___V104*/ meltfptr[103])) ==
		  MELTOBMAG_OBJECT);

  melt_assertmsg ("putslot checkoff",
		  (1 >= 0
		   && 1 <
		   melt_object_length ((melt_ptr_t)
				       ( /*_.VALDATA___V104*/
					meltfptr[103]))));
  ((meltobject_ptr_t) ( /*_.VALDATA___V104*/ meltfptr[103]))->obj_vartab[1] =
    (melt_ptr_t) (
  /*_.VALSTR___V105*/ meltfptr[104]);

  /*touch:VALUEDATA_ */
  meltgc_touch ( /*_.VALDATA___V104*/ meltfptr[103]);


  melt_dbgtrace_written_object ( /*_.VALDATA___V104*/ meltfptr[103],
				"new static instance");

  MELT_LOCATION ("warmelt-debug.melt:77:/ initchunk");
  /*^putslot */
  /*putslot */
  melt_assertmsg ("putslot checkobj",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALDATA___V103*/ meltfptr[102])) ==
		  MELTOBMAG_OBJECT);

  melt_assertmsg ("putslot checkoff",
		  (0 >= 0
		   && 0 <
		   melt_object_length ((melt_ptr_t)
				       ( /*_.VALDATA___V103*/
					meltfptr[102]))));
  ((meltobject_ptr_t) ( /*_.VALDATA___V103*/ meltfptr[102]))->obj_vartab[0] =
    (melt_ptr_t) (
  /*_.VALDATA___V104*/ meltfptr[103]);

  MELT_LOCATION ("warmelt-debug.melt:77:/ initchunk");
  /*^putslot */
  /*putslot */
  melt_assertmsg ("putslot checkobj",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALDATA___V103*/ meltfptr[102])) ==
		  MELTOBMAG_OBJECT);

  melt_assertmsg ("putslot checkoff",
		  (1 >= 0
		   && 1 <
		   melt_object_length ((melt_ptr_t)
				       ( /*_.VALDATA___V103*/
					meltfptr[102]))));
  ((meltobject_ptr_t) ( /*_.VALDATA___V103*/ meltfptr[102]))->obj_vartab[1] =
    (melt_ptr_t) (
		    ((melt_ptr_t) (MELT_PREDEF (CTYPE_LONG))));

  MELT_LOCATION ("warmelt-debug.melt:77:/ initchunk");
  /*^touch */
  /*touch:VALUEDATA_ */
  meltgc_touch ( /*_.VALDATA___V103*/ meltfptr[102]);

  MELT_LOCATION ("warmelt-debug.melt:77:/ initchunk");
  /*^touchobj */

  melt_dbgtrace_written_object ( /*_.VALDATA___V103*/ meltfptr[102],
				"new static instance");

  /*putupl#22 */
  melt_assertmsg ("putupl #22 checktup",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALTUP___V106*/ meltfptr[105])) ==
		  MELTOBMAG_MULTIPLE);
  melt_assertmsg ("putupl #22 checkoff",
		  (0 >= 0
		   && 0 <
		   melt_multiple_length ((melt_ptr_t)
					 ( /*_.VALTUP___V106*/
					  meltfptr[105]))));
  ((meltmultiple_ptr_t) ( /*_.VALTUP___V106*/ meltfptr[105]))->tabval[0] =
    (melt_ptr_t) ( /*_.VALDATA___V102*/ meltfptr[101]);

  /*putupl#23 */
  melt_assertmsg ("putupl #23 checktup",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALTUP___V106*/ meltfptr[105])) ==
		  MELTOBMAG_MULTIPLE);
  melt_assertmsg ("putupl #23 checkoff",
		  (1 >= 0
		   && 1 <
		   melt_multiple_length ((melt_ptr_t)
					 ( /*_.VALTUP___V106*/
					  meltfptr[105]))));
  ((meltmultiple_ptr_t) ( /*_.VALTUP___V106*/ meltfptr[105]))->tabval[1] =
    (melt_ptr_t) ( /*_.VALDATA___V103*/ meltfptr[102]);

  /*touch:INITUP_ */
  meltgc_touch ( /*_.VALTUP___V106*/ meltfptr[105]);

  MELT_LOCATION ("warmelt-debug.melt:77:/ initchunk");
  /*^putslot */
  /*putslot */
  melt_assertmsg ("putslot checkobj",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALDATA___V100*/ meltfptr[99])) ==
		  MELTOBMAG_OBJECT);

  melt_assertmsg ("putslot checkoff",
		  (2 >= 0
		   && 2 <
		   melt_object_length ((melt_ptr_t)
				       ( /*_.VALDATA___V100*/
					meltfptr[99]))));
  ((meltobject_ptr_t) ( /*_.VALDATA___V100*/ meltfptr[99]))->obj_vartab[2] =
    (melt_ptr_t) (
  /*_.VALTUP___V106*/ meltfptr[105]);

  MELT_LOCATION ("warmelt-debug.melt:77:/ initchunk");
  /*^putslot */
  /*putslot */
  melt_assertmsg ("putslot checkobj",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALDATA___V100*/ meltfptr[99])) ==
		  MELTOBMAG_OBJECT);

  melt_assertmsg ("putslot checkoff",
		  (3 >= 0
		   && 3 <
		   melt_object_length ((melt_ptr_t)
				       ( /*_.VALDATA___V100*/
					meltfptr[99]))));
  ((meltobject_ptr_t) ( /*_.VALDATA___V100*/ meltfptr[99]))->obj_vartab[3] =
    (melt_ptr_t) (
		    ((melt_ptr_t) (MELT_PREDEF (CTYPE_LONG))));

  /*putupl#24 */
  melt_assertmsg ("putupl #24 checktup",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALTUP___V110*/ meltfptr[109])) ==
		  MELTOBMAG_MULTIPLE);
  melt_assertmsg ("putupl #24 checkoff",
		  (0 >= 0
		   && 0 <
		   melt_multiple_length ((melt_ptr_t)
					 ( /*_.VALTUP___V110*/
					  meltfptr[109]))));
  ((meltmultiple_ptr_t) ( /*_.VALTUP___V110*/ meltfptr[109]))->tabval[0] =
    (melt_ptr_t) ( /*_.VALSTR___V107*/ meltfptr[106]);

  /*putupl#25 */
  melt_assertmsg ("putupl #25 checktup",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALTUP___V110*/ meltfptr[109])) ==
		  MELTOBMAG_MULTIPLE);
  melt_assertmsg ("putupl #25 checkoff",
		  (1 >= 0
		   && 1 <
		   melt_multiple_length ((melt_ptr_t)
					 ( /*_.VALTUP___V110*/
					  meltfptr[109]))));
  ((meltmultiple_ptr_t) ( /*_.VALTUP___V110*/ meltfptr[109]))->tabval[1] =
    (melt_ptr_t) ( /*_.VALDATA___V85*/ meltfptr[84]);

  /*putupl#26 */
  melt_assertmsg ("putupl #26 checktup",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALTUP___V110*/ meltfptr[109])) ==
		  MELTOBMAG_MULTIPLE);
  melt_assertmsg ("putupl #26 checkoff",
		  (2 >= 0
		   && 2 <
		   melt_multiple_length ((melt_ptr_t)
					 ( /*_.VALTUP___V110*/
					  meltfptr[109]))));
  ((meltmultiple_ptr_t) ( /*_.VALTUP___V110*/ meltfptr[109]))->tabval[2] =
    (melt_ptr_t) ( /*_.VALSTR___V108*/ meltfptr[107]);

  /*putupl#27 */
  melt_assertmsg ("putupl #27 checktup",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALTUP___V110*/ meltfptr[109])) ==
		  MELTOBMAG_MULTIPLE);
  melt_assertmsg ("putupl #27 checkoff",
		  (3 >= 0
		   && 3 <
		   melt_multiple_length ((melt_ptr_t)
					 ( /*_.VALTUP___V110*/
					  meltfptr[109]))));
  ((meltmultiple_ptr_t) ( /*_.VALTUP___V110*/ meltfptr[109]))->tabval[3] =
    (melt_ptr_t) ( /*_.VALDATA___V104*/ meltfptr[103]);

  /*putupl#28 */
  melt_assertmsg ("putupl #28 checktup",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALTUP___V110*/ meltfptr[109])) ==
		  MELTOBMAG_MULTIPLE);
  melt_assertmsg ("putupl #28 checkoff",
		  (4 >= 0
		   && 4 <
		   melt_multiple_length ((melt_ptr_t)
					 ( /*_.VALTUP___V110*/
					  meltfptr[109]))));
  ((meltmultiple_ptr_t) ( /*_.VALTUP___V110*/ meltfptr[109]))->tabval[4] =
    (melt_ptr_t) ( /*_.VALSTR___V109*/ meltfptr[108]);

  /*touch:INITUP_ */
  meltgc_touch ( /*_.VALTUP___V110*/ meltfptr[109]);


#undef meltfram__
}				/*end of meltmod__warmelt_debug__initialmeltchunk_0 */

void
meltmod__warmelt_debug__initialmeltchunk_1 (struct
					    frame_melt_start_this_module_st
					    *meltmeltframptr__,
					    char meltpredefinited[])
{
#define meltfram__ (*meltmeltframptr__)
#undef meltcallcount
#define meltcallcount 0L
  (void) meltpredefinited;
  MELT_LOCATION ("warmelt-debug.melt:77:/ initchunk");
  /*^putslot */
  /*putslot */
  melt_assertmsg ("putslot checkobj",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALDATA___V100*/ meltfptr[99])) ==
		  MELTOBMAG_OBJECT);

  melt_assertmsg ("putslot checkoff",
		  (4 >= 0
		   && 4 <
		   melt_object_length ((melt_ptr_t)
				       ( /*_.VALDATA___V100*/
					meltfptr[99]))));
  ((meltobject_ptr_t) ( /*_.VALDATA___V100*/ meltfptr[99]))->obj_vartab[4] =
    (melt_ptr_t) (
  /*_.VALTUP___V110*/ meltfptr[109]);

  MELT_LOCATION ("warmelt-debug.melt:77:/ initchunk");
  /*^touch */
  /*touch:VALUEDATA_ */
  meltgc_touch ( /*_.VALDATA___V100*/ meltfptr[99]);

  MELT_LOCATION ("warmelt-debug.melt:77:/ initchunk");
  /*^touchobj */

  melt_dbgtrace_written_object ( /*_.VALDATA___V100*/ meltfptr[99],
				"new static instance");

  MELT_LOCATION ("warmelt-debug.melt:84:/ initchunk");
  /*^putroutconst */
  /*putroutconst */
  if (MELT_HAS_INITIAL_ENVIRONMENT)
    melt_assertmsg ("putroutconst checkrout",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.VALROUT___V111*/ meltfptr[110]))
		    == MELTOBMAG_ROUTINE);
  if (MELT_HAS_INITIAL_ENVIRONMENT)
    melt_checkmsg ("putroutconst constnull.drout_65__DEBUG_MSG_FUN#0",
		   NULL != ( /*_.INITIAL_SYSTEM_DATA__V8*/ meltfptr[7]));
  ((meltroutine_ptr_t) /*_.VALROUT___V111*/ meltfptr[110])->tabval[0] =
    (melt_ptr_t) ( /*_.INITIAL_SYSTEM_DATA__V8*/ meltfptr[7]);

  MELT_LOCATION ("warmelt-debug.melt:84:/ initchunk");
  /*^putroutconst */
  /*putroutconst */
  if (MELT_HAS_INITIAL_ENVIRONMENT)
    melt_assertmsg ("putroutconst checkrout",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.VALROUT___V111*/ meltfptr[110]))
		    == MELTOBMAG_ROUTINE);
  if (MELT_HAS_INITIAL_ENVIRONMENT)
    melt_checkmsg ("putroutconst constnull.drout_65__DEBUG_MSG_FUN#1",
		   NULL != ( /*_.CLASS_SYSTEM_DATA__V9*/ meltfptr[8]));
  ((meltroutine_ptr_t) /*_.VALROUT___V111*/ meltfptr[110])->tabval[1] =
    (melt_ptr_t) ( /*_.CLASS_SYSTEM_DATA__V9*/ meltfptr[8]);

  MELT_LOCATION ("warmelt-debug.melt:84:/ initchunk");
  /*^putroutconst */
  /*putroutconst */
  if (MELT_HAS_INITIAL_ENVIRONMENT)
    melt_assertmsg ("putroutconst checkrout",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.VALROUT___V111*/ meltfptr[110]))
		    == MELTOBMAG_ROUTINE);
  if (MELT_HAS_INITIAL_ENVIRONMENT)
    melt_checkmsg ("putroutconst constnull.drout_65__DEBUG_MSG_FUN#2",
		   NULL != ( /*_.DISCR_MAP_OBJECTS__V10*/ meltfptr[9]));
  ((meltroutine_ptr_t) /*_.VALROUT___V111*/ meltfptr[110])->tabval[2] =
    (melt_ptr_t) ( /*_.DISCR_MAP_OBJECTS__V10*/ meltfptr[9]);

  MELT_LOCATION ("warmelt-debug.melt:84:/ initchunk");
  /*^putroutconst */
  /*putroutconst */
  if (MELT_HAS_INITIAL_ENVIRONMENT)
    melt_assertmsg ("putroutconst checkrout",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.VALROUT___V111*/ meltfptr[110]))
		    == MELTOBMAG_ROUTINE);
  if (MELT_HAS_INITIAL_ENVIRONMENT)
    melt_checkmsg ("putroutconst constnull.drout_65__DEBUG_MSG_FUN#3",
		   NULL != ( /*_.DISCR_INTEGER__V11*/ meltfptr[10]));
  ((meltroutine_ptr_t) /*_.VALROUT___V111*/ meltfptr[110])->tabval[3] =
    (melt_ptr_t) ( /*_.DISCR_INTEGER__V11*/ meltfptr[10]);

  MELT_LOCATION ("warmelt-debug.melt:84:/ initchunk");
  /*^putroutconst */
  /*putroutconst */
  if (MELT_HAS_INITIAL_ENVIRONMENT)
    melt_assertmsg ("putroutconst checkrout",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.VALROUT___V111*/ meltfptr[110]))
		    == MELTOBMAG_ROUTINE);
  if (MELT_HAS_INITIAL_ENVIRONMENT)
    melt_checkmsg ("putroutconst constnull.drout_65__DEBUG_MSG_FUN#4",
		   NULL != ( /*_.DISCR_STRBUF__V12*/ meltfptr[11]));
  ((meltroutine_ptr_t) /*_.VALROUT___V111*/ meltfptr[110])->tabval[4] =
    (melt_ptr_t) ( /*_.DISCR_STRBUF__V12*/ meltfptr[11]);

  MELT_LOCATION ("warmelt-debug.melt:84:/ initchunk");
  /*^putroutconstnotnull */
  /*putroutconstnotnull */
  melt_assertmsg ("putroutconstnotnull checkrout",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALROUT___V111*/ meltfptr[110])) ==
		  MELTOBMAG_ROUTINE);
  melt_assertmsg ("putroutconstnotnull notnullconst",
		  NULL != /*_.VALDATA___V51*/ meltfptr[50]);
  ((meltroutine_ptr_t) /*_.VALROUT___V111*/ meltfptr[110])->tabval[5] =
    (melt_ptr_t) ( /*_.VALDATA___V51*/ meltfptr[50]);

  MELT_LOCATION ("warmelt-debug.melt:474:/ initchunk");
  /*^putroutconstnotnull */
  /*putroutconstnotnull */
  melt_assertmsg ("putroutconstnotnull checkrout",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALROUT___V113*/ meltfptr[112])) ==
		  MELTOBMAG_ROUTINE);
  melt_assertmsg ("putroutconstnotnull notnullconst",
		  NULL != /*_.VALDATA___V51*/ meltfptr[50]);
  ((meltroutine_ptr_t) /*_.VALROUT___V113*/ meltfptr[112])->tabval[0] =
    (melt_ptr_t) ( /*_.VALDATA___V51*/ meltfptr[50]);

  MELT_LOCATION ("warmelt-debug.melt:474:/ initchunk");
  /*^putroutconstnotnull */
  /*putroutconstnotnull */
  melt_assertmsg ("putroutconstnotnull checkrout",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALROUT___V113*/ meltfptr[112])) ==
		  MELTOBMAG_ROUTINE);
  melt_assertmsg ("putroutconstnotnull notnullconst",
		  NULL != /*_.VALDATA___V69*/ meltfptr[68]);
  ((meltroutine_ptr_t) /*_.VALROUT___V113*/ meltfptr[112])->tabval[1] =
    (melt_ptr_t) ( /*_.VALDATA___V69*/ meltfptr[68]);

  MELT_LOCATION ("warmelt-debug.melt:454:/ initchunk");
  /*^putroutconstnotnull */
  /*putroutconstnotnull */
  melt_assertmsg ("putroutconstnotnull checkrout",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALROUT___V115*/ meltfptr[114])) ==
		  MELTOBMAG_ROUTINE);
  melt_assertmsg ("putroutconstnotnull notnullconst",
		  NULL != /*_.VALDATA___V51*/ meltfptr[50]);
  ((meltroutine_ptr_t) /*_.VALROUT___V115*/ meltfptr[114])->tabval[0] =
    (melt_ptr_t) ( /*_.VALDATA___V51*/ meltfptr[50]);

  MELT_LOCATION ("warmelt-debug.melt:182:/ initchunk");
  /*^putslot */
  /*putslot */
  melt_assertmsg ("putslot checkobj",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALDATA___V116*/ meltfptr[115])) ==
		  MELTOBMAG_OBJECT);

  melt_assertmsg ("putslot checkoff",
		  (1 >= 0
		   && 1 <
		   melt_object_length ((melt_ptr_t)
				       ( /*_.VALDATA___V116*/
					meltfptr[115]))));
  ((meltobject_ptr_t) ( /*_.VALDATA___V116*/ meltfptr[115]))->obj_vartab[1] =
    (melt_ptr_t) (
  /*_.VALSTR___V117*/ meltfptr[116]);

  MELT_LOCATION ("warmelt-debug.melt:182:/ initchunk");
  /*^touch */
  /*touch:VALUEDATA_ */
  meltgc_touch ( /*_.VALDATA___V116*/ meltfptr[115]);

  MELT_LOCATION ("warmelt-debug.melt:182:/ initchunk");
  /*^touchobj */

  melt_dbgtrace_written_object ( /*_.VALDATA___V116*/ meltfptr[115],
				"new static instance");

  MELT_LOCATION ("warmelt-debug.melt:454:/ initchunk");
  /*^putroutconstnotnull */
  /*putroutconstnotnull */
  melt_assertmsg ("putroutconstnotnull checkrout",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALROUT___V115*/ meltfptr[114])) ==
		  MELTOBMAG_ROUTINE);
  melt_assertmsg ("putroutconstnotnull notnullconst",
		  NULL != /*_.VALDATA___V116*/ meltfptr[115]);
  ((meltroutine_ptr_t) /*_.VALROUT___V115*/ meltfptr[114])->tabval[1] =
    (melt_ptr_t) ( /*_.VALDATA___V116*/ meltfptr[115]);

  MELT_LOCATION ("warmelt-debug.melt:454:/ initchunk");
  /*^putroutconst */
  /*putroutconst */
  if (MELT_HAS_INITIAL_ENVIRONMENT)
    melt_assertmsg ("putroutconst checkrout",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.VALROUT___V115*/ meltfptr[114]))
		    == MELTOBMAG_ROUTINE);
  if (MELT_HAS_INITIAL_ENVIRONMENT)
    melt_checkmsg ("putroutconst constnull.drout_149__DBG_OUTOBJECT#2",
		   NULL != ( /*_.DISCR_INTEGER__V11*/ meltfptr[10]));
  ((meltroutine_ptr_t) /*_.VALROUT___V115*/ meltfptr[114])->tabval[2] =
    (melt_ptr_t) ( /*_.DISCR_INTEGER__V11*/ meltfptr[10]);

  MELT_LOCATION ("warmelt-debug.melt:173:/ initchunk");
  /*^putslot */
  /*putslot */
  melt_assertmsg ("putslot checkobj",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALDATA___V118*/ meltfptr[117])) ==
		  MELTOBMAG_OBJECT);

  melt_assertmsg ("putslot checkoff",
		  (1 >= 0
		   && 1 <
		   melt_object_length ((melt_ptr_t)
				       ( /*_.VALDATA___V118*/
					meltfptr[117]))));
  ((meltobject_ptr_t) ( /*_.VALDATA___V118*/ meltfptr[117]))->obj_vartab[1] =
    (melt_ptr_t) (
  /*_.VALSTR___V119*/ meltfptr[118]);

  MELT_LOCATION ("warmelt-debug.melt:173:/ initchunk");
  /*^touch */
  /*touch:VALUEDATA_ */
  meltgc_touch ( /*_.VALDATA___V118*/ meltfptr[117]);

  MELT_LOCATION ("warmelt-debug.melt:173:/ initchunk");
  /*^touchobj */

  melt_dbgtrace_written_object ( /*_.VALDATA___V118*/ meltfptr[117],
				"new static instance");

  MELT_LOCATION ("warmelt-debug.melt:454:/ initchunk");
  /*^putroutconstnotnull */
  /*putroutconstnotnull */
  melt_assertmsg ("putroutconstnotnull checkrout",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALROUT___V115*/ meltfptr[114])) ==
		  MELTOBMAG_ROUTINE);
  melt_assertmsg ("putroutconstnotnull notnullconst",
		  NULL != /*_.VALDATA___V118*/ meltfptr[117]);
  ((meltroutine_ptr_t) /*_.VALROUT___V115*/ meltfptr[114])->tabval[3] =
    (melt_ptr_t) ( /*_.VALDATA___V118*/ meltfptr[117]);

  MELT_LOCATION ("warmelt-debug.melt:454:/ initchunk");
  /*^touch */
  /*touch:IROUTVAL_ */
  meltgc_touch ( /*_.VALROUT___V115*/ meltfptr[114]);

  /*putclosurout#1 */
  melt_assertmsg ("putclosrout#1 checkclo",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALCLO___V114*/ meltfptr[113])) ==
		  MELTOBMAG_CLOSURE);
  melt_assertmsg ("putclosrout#1 checkrout",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALROUT___V115*/ meltfptr[114])) ==
		  MELTOBMAG_ROUTINE);
  ((meltclosure_ptr_t) /*_.VALCLO___V114*/ meltfptr[113])->rout =
    (meltroutine_ptr_t) ( /*_.VALROUT___V115*/ meltfptr[114]);

  /*touch:DATACLOSURE_ */
  meltgc_touch ( /*_.VALCLO___V114*/ meltfptr[113]);

  MELT_LOCATION ("warmelt-debug.melt:474:/ initchunk");
  /*^putroutconstnotnull */
  /*putroutconstnotnull */
  melt_assertmsg ("putroutconstnotnull checkrout",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALROUT___V113*/ meltfptr[112])) ==
		  MELTOBMAG_ROUTINE);
  melt_assertmsg ("putroutconstnotnull notnullconst",
		  NULL != /*_.VALCLO___V114*/ meltfptr[113]);
  ((meltroutine_ptr_t) /*_.VALROUT___V113*/ meltfptr[112])->tabval[2] =
    (melt_ptr_t) ( /*_.VALCLO___V114*/ meltfptr[113]);

  MELT_LOCATION ("warmelt-debug.melt:474:/ initchunk");
  /*^putroutconstnotnull */
  /*putroutconstnotnull */
  melt_assertmsg ("putroutconstnotnull checkrout",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALROUT___V113*/ meltfptr[112])) ==
		  MELTOBMAG_ROUTINE);
  melt_assertmsg ("putroutconstnotnull notnullconst",
		  NULL != /*_.VALDATA___V118*/ meltfptr[117]);
  ((meltroutine_ptr_t) /*_.VALROUT___V113*/ meltfptr[112])->tabval[3] =
    (melt_ptr_t) ( /*_.VALDATA___V118*/ meltfptr[117]);

  MELT_LOCATION ("warmelt-debug.melt:474:/ initchunk");
  /*^touch */
  /*touch:IROUTVAL_ */
  meltgc_touch ( /*_.VALROUT___V113*/ meltfptr[112]);

  /*putclosurout#2 */
  melt_assertmsg ("putclosrout#2 checkclo",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALCLO___V112*/ meltfptr[111])) ==
		  MELTOBMAG_CLOSURE);
  melt_assertmsg ("putclosrout#2 checkrout",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALROUT___V113*/ meltfptr[112])) ==
		  MELTOBMAG_ROUTINE);
  ((meltclosure_ptr_t) /*_.VALCLO___V112*/ meltfptr[111])->rout =
    (meltroutine_ptr_t) ( /*_.VALROUT___V113*/ meltfptr[112]);

  /*touch:DATACLOSURE_ */
  meltgc_touch ( /*_.VALCLO___V112*/ meltfptr[111]);

  MELT_LOCATION ("warmelt-debug.melt:84:/ initchunk");
  /*^putroutconstnotnull */
  /*putroutconstnotnull */
  melt_assertmsg ("putroutconstnotnull checkrout",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALROUT___V111*/ meltfptr[110])) ==
		  MELTOBMAG_ROUTINE);
  melt_assertmsg ("putroutconstnotnull notnullconst",
		  NULL != /*_.VALCLO___V112*/ meltfptr[111]);
  ((meltroutine_ptr_t) /*_.VALROUT___V111*/ meltfptr[110])->tabval[6] =
    (melt_ptr_t) ( /*_.VALCLO___V112*/ meltfptr[111]);

  MELT_LOCATION ("warmelt-debug.melt:84:/ initchunk");
  /*^touch */
  /*touch:IROUTVAL_ */
  meltgc_touch ( /*_.VALROUT___V111*/ meltfptr[110]);

  /*putclosurout#3 */
  melt_assertmsg ("putclosrout#3 checkclo",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALCLO___V120*/ meltfptr[119])) ==
		  MELTOBMAG_CLOSURE);
  melt_assertmsg ("putclosrout#3 checkrout",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALROUT___V111*/ meltfptr[110])) ==
		  MELTOBMAG_ROUTINE);
  ((meltclosure_ptr_t) /*_.VALCLO___V120*/ meltfptr[119])->rout =
    (meltroutine_ptr_t) ( /*_.VALROUT___V111*/ meltfptr[110]);

  /*touch:DATACLOSURE_ */
  meltgc_touch ( /*_.VALCLO___V120*/ meltfptr[119]);

  /*putslot */
  melt_assertmsg ("putslot checkobj",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALDATA___V121*/ meltfptr[120])) ==
		  MELTOBMAG_OBJECT);

  melt_assertmsg ("putslot checkoff",
		  (1 >= 0
		   && 1 <
		   melt_object_length ((melt_ptr_t)
				       ( /*_.VALDATA___V121*/
					meltfptr[120]))));
  ((meltobject_ptr_t) ( /*_.VALDATA___V121*/ meltfptr[120]))->obj_vartab[1] =
    (melt_ptr_t) (
  /*_.VALSTR___V122*/ meltfptr[121]);

  /*touch:VALUEDATA_ */
  meltgc_touch ( /*_.VALDATA___V121*/ meltfptr[120]);


  melt_dbgtrace_written_object ( /*_.VALDATA___V121*/ meltfptr[120],
				"new static instance");

  /*putslot */
  melt_assertmsg ("putslot checkobj",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALDATA___V123*/ meltfptr[122])) ==
		  MELTOBMAG_OBJECT);

  melt_assertmsg ("putslot checkoff",
		  (1 >= 0
		   && 1 <
		   melt_object_length ((melt_ptr_t)
				       ( /*_.VALDATA___V123*/
					meltfptr[122]))));
  ((meltobject_ptr_t) ( /*_.VALDATA___V123*/ meltfptr[122]))->obj_vartab[1] =
    (melt_ptr_t) (
  /*_.VALSTR___V124*/ meltfptr[123]);

  /*touch:VALUEDATA_ */
  meltgc_touch ( /*_.VALDATA___V123*/ meltfptr[122]);


  melt_dbgtrace_written_object ( /*_.VALDATA___V123*/ meltfptr[122],
				"new static instance");

  /*putslot */
  melt_assertmsg ("putslot checkobj",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALDATA___V125*/ meltfptr[124])) ==
		  MELTOBMAG_OBJECT);

  melt_assertmsg ("putslot checkoff",
		  (1 >= 0
		   && 1 <
		   melt_object_length ((melt_ptr_t)
				       ( /*_.VALDATA___V125*/
					meltfptr[124]))));
  ((meltobject_ptr_t) ( /*_.VALDATA___V125*/ meltfptr[124]))->obj_vartab[1] =
    (melt_ptr_t) (
  /*_.VALSTR___V126*/ meltfptr[125]);

  /*touch:VALUEDATA_ */
  meltgc_touch ( /*_.VALDATA___V125*/ meltfptr[124]);


  melt_dbgtrace_written_object ( /*_.VALDATA___V125*/ meltfptr[124],
				"new static instance");

  /*putslot */
  melt_assertmsg ("putslot checkobj",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALDATA___V127*/ meltfptr[126])) ==
		  MELTOBMAG_OBJECT);

  melt_assertmsg ("putslot checkoff",
		  (1 >= 0
		   && 1 <
		   melt_object_length ((melt_ptr_t)
				       ( /*_.VALDATA___V127*/
					meltfptr[126]))));
  ((meltobject_ptr_t) ( /*_.VALDATA___V127*/ meltfptr[126]))->obj_vartab[1] =
    (melt_ptr_t) (
  /*_.VALSTR___V128*/ meltfptr[127]);

  /*touch:VALUEDATA_ */
  meltgc_touch ( /*_.VALDATA___V127*/ meltfptr[126]);


  melt_dbgtrace_written_object ( /*_.VALDATA___V127*/ meltfptr[126],
				"new static instance");

  /*putslot */
  melt_assertmsg ("putslot checkobj",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALDATA___V129*/ meltfptr[128])) ==
		  MELTOBMAG_OBJECT);

  melt_assertmsg ("putslot checkoff",
		  (1 >= 0
		   && 1 <
		   melt_object_length ((melt_ptr_t)
				       ( /*_.VALDATA___V129*/
					meltfptr[128]))));
  ((meltobject_ptr_t) ( /*_.VALDATA___V129*/ meltfptr[128]))->obj_vartab[1] =
    (melt_ptr_t) (
  /*_.VALSTR___V130*/ meltfptr[129]);

  /*touch:VALUEDATA_ */
  meltgc_touch ( /*_.VALDATA___V129*/ meltfptr[128]);


  melt_dbgtrace_written_object ( /*_.VALDATA___V129*/ meltfptr[128],
				"new static instance");

  /*putslot */
  melt_assertmsg ("putslot checkobj",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALDATA___V131*/ meltfptr[130])) ==
		  MELTOBMAG_OBJECT);

  melt_assertmsg ("putslot checkoff",
		  (1 >= 0
		   && 1 <
		   melt_object_length ((melt_ptr_t)
				       ( /*_.VALDATA___V131*/
					meltfptr[130]))));
  ((meltobject_ptr_t) ( /*_.VALDATA___V131*/ meltfptr[130]))->obj_vartab[1] =
    (melt_ptr_t) (
  /*_.VALSTR___V132*/ meltfptr[131]);

  /*touch:VALUEDATA_ */
  meltgc_touch ( /*_.VALDATA___V131*/ meltfptr[130]);


  melt_dbgtrace_written_object ( /*_.VALDATA___V131*/ meltfptr[130],
				"new static instance");

  /*putslot */
  melt_assertmsg ("putslot checkobj",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALDATA___V133*/ meltfptr[132])) ==
		  MELTOBMAG_OBJECT);

  melt_assertmsg ("putslot checkoff",
		  (1 >= 0
		   && 1 <
		   melt_object_length ((melt_ptr_t)
				       ( /*_.VALDATA___V133*/
					meltfptr[132]))));
  ((meltobject_ptr_t) ( /*_.VALDATA___V133*/ meltfptr[132]))->obj_vartab[1] =
    (melt_ptr_t) (
  /*_.VALSTR___V134*/ meltfptr[133]);

  /*touch:VALUEDATA_ */
  meltgc_touch ( /*_.VALDATA___V133*/ meltfptr[132]);


  melt_dbgtrace_written_object ( /*_.VALDATA___V133*/ meltfptr[132],
				"new static instance");

  /*putslot */
  melt_assertmsg ("putslot checkobj",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALDATA___V135*/ meltfptr[134])) ==
		  MELTOBMAG_OBJECT);

  melt_assertmsg ("putslot checkoff",
		  (1 >= 0
		   && 1 <
		   melt_object_length ((melt_ptr_t)
				       ( /*_.VALDATA___V135*/
					meltfptr[134]))));
  ((meltobject_ptr_t) ( /*_.VALDATA___V135*/ meltfptr[134]))->obj_vartab[1] =
    (melt_ptr_t) (
  /*_.VALSTR___V136*/ meltfptr[135]);

  /*touch:VALUEDATA_ */
  meltgc_touch ( /*_.VALDATA___V135*/ meltfptr[134]);


  melt_dbgtrace_written_object ( /*_.VALDATA___V135*/ meltfptr[134],
				"new static instance");

  MELT_LOCATION ("warmelt-debug.melt:173:/ initchunk");
  /*^putslot */
  /*putslot */
  melt_assertmsg ("putslot checkobj",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALDATA___V137*/ meltfptr[136])) ==
		  MELTOBMAG_OBJECT);

  melt_assertmsg ("putslot checkoff",
		  (0 >= 0
		   && 0 <
		   melt_object_length ((melt_ptr_t)
				       ( /*_.VALDATA___V137*/
					meltfptr[136]))));
  ((meltobject_ptr_t) ( /*_.VALDATA___V137*/ meltfptr[136]))->obj_vartab[0] =
    (melt_ptr_t) (
  /*_.VALDATA___V135*/ meltfptr[134]);

  MELT_LOCATION ("warmelt-debug.melt:173:/ initchunk");
  /*^putslot */
  /*putslot */
  melt_assertmsg ("putslot checkobj",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALDATA___V137*/ meltfptr[136])) ==
		  MELTOBMAG_OBJECT);

  melt_assertmsg ("putslot checkoff",
		  (1 >= 0
		   && 1 <
		   melt_object_length ((melt_ptr_t)
				       ( /*_.VALDATA___V137*/
					meltfptr[136]))));
  ((meltobject_ptr_t) ( /*_.VALDATA___V137*/ meltfptr[136]))->obj_vartab[1] =
    (melt_ptr_t) (
		    ((melt_ptr_t) (MELT_PREDEF (CTYPE_VALUE))));

  MELT_LOCATION ("warmelt-debug.melt:173:/ initchunk");
  /*^touch */
  /*touch:VALUEDATA_ */
  meltgc_touch ( /*_.VALDATA___V137*/ meltfptr[136]);

  MELT_LOCATION ("warmelt-debug.melt:173:/ initchunk");
  /*^touchobj */

  melt_dbgtrace_written_object ( /*_.VALDATA___V137*/ meltfptr[136],
				"new static instance");

  /*putslot */
  melt_assertmsg ("putslot checkobj",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALDATA___V138*/ meltfptr[137])) ==
		  MELTOBMAG_OBJECT);

  melt_assertmsg ("putslot checkoff",
		  (1 >= 0
		   && 1 <
		   melt_object_length ((melt_ptr_t)
				       ( /*_.VALDATA___V138*/
					meltfptr[137]))));
  ((meltobject_ptr_t) ( /*_.VALDATA___V138*/ meltfptr[137]))->obj_vartab[1] =
    (melt_ptr_t) (
  /*_.VALSTR___V139*/ meltfptr[138]);

  /*touch:VALUEDATA_ */
  meltgc_touch ( /*_.VALDATA___V138*/ meltfptr[137]);


  melt_dbgtrace_written_object ( /*_.VALDATA___V138*/ meltfptr[137],
				"new static instance");

  MELT_LOCATION ("warmelt-debug.melt:173:/ initchunk");
  /*^putslot */
  /*putslot */
  melt_assertmsg ("putslot checkobj",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALDATA___V140*/ meltfptr[139])) ==
		  MELTOBMAG_OBJECT);

  melt_assertmsg ("putslot checkoff",
		  (0 >= 0
		   && 0 <
		   melt_object_length ((melt_ptr_t)
				       ( /*_.VALDATA___V140*/
					meltfptr[139]))));
  ((meltobject_ptr_t) ( /*_.VALDATA___V140*/ meltfptr[139]))->obj_vartab[0] =
    (melt_ptr_t) (
  /*_.VALDATA___V138*/ meltfptr[137]);

  MELT_LOCATION ("warmelt-debug.melt:173:/ initchunk");
  /*^putslot */
  /*putslot */
  melt_assertmsg ("putslot checkobj",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALDATA___V140*/ meltfptr[139])) ==
		  MELTOBMAG_OBJECT);

  melt_assertmsg ("putslot checkoff",
		  (1 >= 0
		   && 1 <
		   melt_object_length ((melt_ptr_t)
				       ( /*_.VALDATA___V140*/
					meltfptr[139]))));
  ((meltobject_ptr_t) ( /*_.VALDATA___V140*/ meltfptr[139]))->obj_vartab[1] =
    (melt_ptr_t) (
		    ((melt_ptr_t) (MELT_PREDEF (CTYPE_VALUE))));

  MELT_LOCATION ("warmelt-debug.melt:173:/ initchunk");
  /*^touch */
  /*touch:VALUEDATA_ */
  meltgc_touch ( /*_.VALDATA___V140*/ meltfptr[139]);

  MELT_LOCATION ("warmelt-debug.melt:173:/ initchunk");
  /*^touchobj */

  melt_dbgtrace_written_object ( /*_.VALDATA___V140*/ meltfptr[139],
				"new static instance");

  MELT_LOCATION ("warmelt-debug.melt:173:/ initchunk");
  /*^putslot */
  /*putslot */
  melt_assertmsg ("putslot checkobj",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALDATA___V141*/ meltfptr[140])) ==
		  MELTOBMAG_OBJECT);

  melt_assertmsg ("putslot checkoff",
		  (0 >= 0
		   && 0 <
		   melt_object_length ((melt_ptr_t)
				       ( /*_.VALDATA___V141*/
					meltfptr[140]))));
  ((meltobject_ptr_t) ( /*_.VALDATA___V141*/ meltfptr[140]))->obj_vartab[0] =
    (melt_ptr_t) (
  /*_.VALDATA___V85*/ meltfptr[84]);

  MELT_LOCATION ("warmelt-debug.melt:173:/ initchunk");
  /*^putslot */
  /*putslot */
  melt_assertmsg ("putslot checkobj",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALDATA___V141*/ meltfptr[140])) ==
		  MELTOBMAG_OBJECT);

  melt_assertmsg ("putslot checkoff",
		  (1 >= 0
		   && 1 <
		   melt_object_length ((melt_ptr_t)
				       ( /*_.VALDATA___V141*/
					meltfptr[140]))));
  ((meltobject_ptr_t) ( /*_.VALDATA___V141*/ meltfptr[140]))->obj_vartab[1] =
    (melt_ptr_t) (
		    ((melt_ptr_t) (MELT_PREDEF (CTYPE_LONG))));

  MELT_LOCATION ("warmelt-debug.melt:173:/ initchunk");
  /*^touch */
  /*touch:VALUEDATA_ */
  meltgc_touch ( /*_.VALDATA___V141*/ meltfptr[140]);

  MELT_LOCATION ("warmelt-debug.melt:173:/ initchunk");
  /*^touchobj */

  melt_dbgtrace_written_object ( /*_.VALDATA___V141*/ meltfptr[140],
				"new static instance");

  /*putupl#29 */
  melt_assertmsg ("putupl #29 checktup",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALTUP___V142*/ meltfptr[141])) ==
		  MELTOBMAG_MULTIPLE);
  melt_assertmsg ("putupl #29 checkoff",
		  (0 >= 0
		   && 0 <
		   melt_multiple_length ((melt_ptr_t)
					 ( /*_.VALTUP___V142*/
					  meltfptr[141]))));
  ((meltmultiple_ptr_t) ( /*_.VALTUP___V142*/ meltfptr[141]))->tabval[0] =
    (melt_ptr_t) ( /*_.VALDATA___V137*/ meltfptr[136]);

  /*putupl#30 */
  melt_assertmsg ("putupl #30 checktup",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALTUP___V142*/ meltfptr[141])) ==
		  MELTOBMAG_MULTIPLE);
  melt_assertmsg ("putupl #30 checkoff",
		  (1 >= 0
		   && 1 <
		   melt_multiple_length ((melt_ptr_t)
					 ( /*_.VALTUP___V142*/
					  meltfptr[141]))));
  ((meltmultiple_ptr_t) ( /*_.VALTUP___V142*/ meltfptr[141]))->tabval[1] =
    (melt_ptr_t) ( /*_.VALDATA___V140*/ meltfptr[139]);

  /*putupl#31 */
  melt_assertmsg ("putupl #31 checktup",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALTUP___V142*/ meltfptr[141])) ==
		  MELTOBMAG_MULTIPLE);
  melt_assertmsg ("putupl #31 checkoff",
		  (2 >= 0
		   && 2 <
		   melt_multiple_length ((melt_ptr_t)
					 ( /*_.VALTUP___V142*/
					  meltfptr[141]))));
  ((meltmultiple_ptr_t) ( /*_.VALTUP___V142*/ meltfptr[141]))->tabval[2] =
    (melt_ptr_t) ( /*_.VALDATA___V141*/ meltfptr[140]);

  /*touch:INITUP_ */
  meltgc_touch ( /*_.VALTUP___V142*/ meltfptr[141]);

  /*putslot */
  melt_assertmsg ("putslot checkobj",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALDATA___V143*/ meltfptr[142])) ==
		  MELTOBMAG_OBJECT);

  melt_assertmsg ("putslot checkoff",
		  (1 >= 0
		   && 1 <
		   melt_object_length ((melt_ptr_t)
				       ( /*_.VALDATA___V143*/
					meltfptr[142]))));
  ((meltobject_ptr_t) ( /*_.VALDATA___V143*/ meltfptr[142]))->obj_vartab[1] =
    (melt_ptr_t) (
  /*_.VALSTR___V144*/ meltfptr[143]);

  /*touch:VALUEDATA_ */
  meltgc_touch ( /*_.VALDATA___V143*/ meltfptr[142]);


  melt_dbgtrace_written_object ( /*_.VALDATA___V143*/ meltfptr[142],
				"new static instance");

  MELT_LOCATION ("warmelt-debug.melt:182:/ initchunk");
  /*^putslot */
  /*putslot */
  melt_assertmsg ("putslot checkobj",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALDATA___V145*/ meltfptr[144])) ==
		  MELTOBMAG_OBJECT);

  melt_assertmsg ("putslot checkoff",
		  (0 >= 0
		   && 0 <
		   melt_object_length ((melt_ptr_t)
				       ( /*_.VALDATA___V145*/
					meltfptr[144]))));
  ((meltobject_ptr_t) ( /*_.VALDATA___V145*/ meltfptr[144]))->obj_vartab[0] =
    (melt_ptr_t) (
  /*_.VALDATA___V135*/ meltfptr[134]);

  MELT_LOCATION ("warmelt-debug.melt:182:/ initchunk");
  /*^putslot */
  /*putslot */
  melt_assertmsg ("putslot checkobj",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALDATA___V145*/ meltfptr[144])) ==
		  MELTOBMAG_OBJECT);

  melt_assertmsg ("putslot checkoff",
		  (1 >= 0
		   && 1 <
		   melt_object_length ((melt_ptr_t)
				       ( /*_.VALDATA___V145*/
					meltfptr[144]))));
  ((meltobject_ptr_t) ( /*_.VALDATA___V145*/ meltfptr[144]))->obj_vartab[1] =
    (melt_ptr_t) (
		    ((melt_ptr_t) (MELT_PREDEF (CTYPE_VALUE))));

  MELT_LOCATION ("warmelt-debug.melt:182:/ initchunk");
  /*^touch */
  /*touch:VALUEDATA_ */
  meltgc_touch ( /*_.VALDATA___V145*/ meltfptr[144]);

  MELT_LOCATION ("warmelt-debug.melt:182:/ initchunk");
  /*^touchobj */

  melt_dbgtrace_written_object ( /*_.VALDATA___V145*/ meltfptr[144],
				"new static instance");

  MELT_LOCATION ("warmelt-debug.melt:182:/ initchunk");
  /*^putslot */
  /*putslot */
  melt_assertmsg ("putslot checkobj",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALDATA___V146*/ meltfptr[145])) ==
		  MELTOBMAG_OBJECT);

  melt_assertmsg ("putslot checkoff",
		  (0 >= 0
		   && 0 <
		   melt_object_length ((melt_ptr_t)
				       ( /*_.VALDATA___V146*/
					meltfptr[145]))));
  ((meltobject_ptr_t) ( /*_.VALDATA___V146*/ meltfptr[145]))->obj_vartab[0] =
    (melt_ptr_t) (
  /*_.VALDATA___V138*/ meltfptr[137]);

  MELT_LOCATION ("warmelt-debug.melt:182:/ initchunk");
  /*^putslot */
  /*putslot */
  melt_assertmsg ("putslot checkobj",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALDATA___V146*/ meltfptr[145])) ==
		  MELTOBMAG_OBJECT);

  melt_assertmsg ("putslot checkoff",
		  (1 >= 0
		   && 1 <
		   melt_object_length ((melt_ptr_t)
				       ( /*_.VALDATA___V146*/
					meltfptr[145]))));
  ((meltobject_ptr_t) ( /*_.VALDATA___V146*/ meltfptr[145]))->obj_vartab[1] =
    (melt_ptr_t) (
		    ((melt_ptr_t) (MELT_PREDEF (CTYPE_VALUE))));

  MELT_LOCATION ("warmelt-debug.melt:182:/ initchunk");
  /*^touch */
  /*touch:VALUEDATA_ */
  meltgc_touch ( /*_.VALDATA___V146*/ meltfptr[145]);

  MELT_LOCATION ("warmelt-debug.melt:182:/ initchunk");
  /*^touchobj */

  melt_dbgtrace_written_object ( /*_.VALDATA___V146*/ meltfptr[145],
				"new static instance");

  MELT_LOCATION ("warmelt-debug.melt:182:/ initchunk");
  /*^putslot */
  /*putslot */
  melt_assertmsg ("putslot checkobj",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALDATA___V147*/ meltfptr[146])) ==
		  MELTOBMAG_OBJECT);

  melt_assertmsg ("putslot checkoff",
		  (0 >= 0
		   && 0 <
		   melt_object_length ((melt_ptr_t)
				       ( /*_.VALDATA___V147*/
					meltfptr[146]))));
  ((meltobject_ptr_t) ( /*_.VALDATA___V147*/ meltfptr[146]))->obj_vartab[0] =
    (melt_ptr_t) (
  /*_.VALDATA___V85*/ meltfptr[84]);

  MELT_LOCATION ("warmelt-debug.melt:182:/ initchunk");
  /*^putslot */
  /*putslot */
  melt_assertmsg ("putslot checkobj",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALDATA___V147*/ meltfptr[146])) ==
		  MELTOBMAG_OBJECT);

  melt_assertmsg ("putslot checkoff",
		  (1 >= 0
		   && 1 <
		   melt_object_length ((melt_ptr_t)
				       ( /*_.VALDATA___V147*/
					meltfptr[146]))));
  ((meltobject_ptr_t) ( /*_.VALDATA___V147*/ meltfptr[146]))->obj_vartab[1] =
    (melt_ptr_t) (
		    ((melt_ptr_t) (MELT_PREDEF (CTYPE_LONG))));

  MELT_LOCATION ("warmelt-debug.melt:182:/ initchunk");
  /*^touch */
  /*touch:VALUEDATA_ */
  meltgc_touch ( /*_.VALDATA___V147*/ meltfptr[146]);

  MELT_LOCATION ("warmelt-debug.melt:182:/ initchunk");
  /*^touchobj */

  melt_dbgtrace_written_object ( /*_.VALDATA___V147*/ meltfptr[146],
				"new static instance");

  /*putupl#32 */
  melt_assertmsg ("putupl #32 checktup",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALTUP___V148*/ meltfptr[147])) ==
		  MELTOBMAG_MULTIPLE);
  melt_assertmsg ("putupl #32 checkoff",
		  (0 >= 0
		   && 0 <
		   melt_multiple_length ((melt_ptr_t)
					 ( /*_.VALTUP___V148*/
					  meltfptr[147]))));
  ((meltmultiple_ptr_t) ( /*_.VALTUP___V148*/ meltfptr[147]))->tabval[0] =
    (melt_ptr_t) ( /*_.VALDATA___V145*/ meltfptr[144]);

  /*putupl#33 */
  melt_assertmsg ("putupl #33 checktup",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALTUP___V148*/ meltfptr[147])) ==
		  MELTOBMAG_MULTIPLE);
  melt_assertmsg ("putupl #33 checkoff",
		  (1 >= 0
		   && 1 <
		   melt_multiple_length ((melt_ptr_t)
					 ( /*_.VALTUP___V148*/
					  meltfptr[147]))));
  ((meltmultiple_ptr_t) ( /*_.VALTUP___V148*/ meltfptr[147]))->tabval[1] =
    (melt_ptr_t) ( /*_.VALDATA___V146*/ meltfptr[145]);

  /*putupl#34 */
  melt_assertmsg ("putupl #34 checktup",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALTUP___V148*/ meltfptr[147])) ==
		  MELTOBMAG_MULTIPLE);
  melt_assertmsg ("putupl #34 checkoff",
		  (2 >= 0
		   && 2 <
		   melt_multiple_length ((melt_ptr_t)
					 ( /*_.VALTUP___V148*/
					  meltfptr[147]))));
  ((meltmultiple_ptr_t) ( /*_.VALTUP___V148*/ meltfptr[147]))->tabval[2] =
    (melt_ptr_t) ( /*_.VALDATA___V147*/ meltfptr[146]);

  /*touch:INITUP_ */
  meltgc_touch ( /*_.VALTUP___V148*/ meltfptr[147]);

  /*putslot */
  melt_assertmsg ("putslot checkobj",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALDATA___V149*/ meltfptr[148])) ==
		  MELTOBMAG_OBJECT);

  melt_assertmsg ("putslot checkoff",
		  (1 >= 0
		   && 1 <
		   melt_object_length ((melt_ptr_t)
				       ( /*_.VALDATA___V149*/
					meltfptr[148]))));
  ((meltobject_ptr_t) ( /*_.VALDATA___V149*/ meltfptr[148]))->obj_vartab[1] =
    (melt_ptr_t) (
  /*_.VALSTR___V150*/ meltfptr[149]);

  /*touch:VALUEDATA_ */
  meltgc_touch ( /*_.VALDATA___V149*/ meltfptr[148]);


  melt_dbgtrace_written_object ( /*_.VALDATA___V149*/ meltfptr[148],
				"new static instance");

  /*putslot */
  melt_assertmsg ("putslot checkobj",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALDATA___V151*/ meltfptr[150])) ==
		  MELTOBMAG_OBJECT);

  melt_assertmsg ("putslot checkoff",
		  (1 >= 0
		   && 1 <
		   melt_object_length ((melt_ptr_t)
				       ( /*_.VALDATA___V151*/
					meltfptr[150]))));
  ((meltobject_ptr_t) ( /*_.VALDATA___V151*/ meltfptr[150]))->obj_vartab[1] =
    (melt_ptr_t) (
  /*_.VALSTR___V152*/ meltfptr[151]);

  /*touch:VALUEDATA_ */
  meltgc_touch ( /*_.VALDATA___V151*/ meltfptr[150]);


  melt_dbgtrace_written_object ( /*_.VALDATA___V151*/ meltfptr[150],
				"new static instance");

  MELT_LOCATION ("warmelt-debug.melt:193:/ initchunk");
  /*^touch */
  /*touch:VALUEDATA_ */
  meltgc_touch ( /*_.VALDATA___V153*/ meltfptr[152]);

  MELT_LOCATION ("warmelt-debug.melt:193:/ initchunk");
  /*^touchobj */

  melt_dbgtrace_written_object ( /*_.VALDATA___V153*/ meltfptr[152],
				"new static instance");

  MELT_LOCATION ("warmelt-debug.melt:194:/ initchunk");
  /*^putroutconstnotnull */
  /*putroutconstnotnull */
  melt_assertmsg ("putroutconstnotnull checkrout",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALROUT___V154*/ meltfptr[153])) ==
		  MELTOBMAG_ROUTINE);
  melt_assertmsg ("putroutconstnotnull notnullconst",
		  NULL != /*_.VALDATA___V153*/ meltfptr[152]);
  ((meltroutine_ptr_t) /*_.VALROUT___V154*/ meltfptr[153])->tabval[0] =
    (melt_ptr_t) ( /*_.VALDATA___V153*/ meltfptr[152]);

  MELT_LOCATION ("warmelt-debug.melt:194:/ initchunk");
  /*^putroutconst */
  /*putroutconst */
  if (MELT_HAS_INITIAL_ENVIRONMENT)
    melt_assertmsg ("putroutconst checkrout",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.VALROUT___V154*/ meltfptr[153]))
		    == MELTOBMAG_ROUTINE);
  if (MELT_HAS_INITIAL_ENVIRONMENT)
    melt_checkmsg
      ("putroutconst constnull.drout_104__REGISTER_TREE_DEBUG_FUN#1",
       NULL != ( /*_.CLASS_REFERENCE__V14*/ meltfptr[13]));
  ((meltroutine_ptr_t) /*_.VALROUT___V154*/ meltfptr[153])->tabval[1] =
    (melt_ptr_t) ( /*_.CLASS_REFERENCE__V14*/ meltfptr[13]);

  MELT_LOCATION ("warmelt-debug.melt:194:/ initchunk");
  /*^touch */
  /*touch:IROUTVAL_ */
  meltgc_touch ( /*_.VALROUT___V154*/ meltfptr[153]);

  /*putclosurout#4 */
  melt_assertmsg ("putclosrout#4 checkclo",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALCLO___V155*/ meltfptr[154])) ==
		  MELTOBMAG_CLOSURE);
  melt_assertmsg ("putclosrout#4 checkrout",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALROUT___V154*/ meltfptr[153])) ==
		  MELTOBMAG_ROUTINE);
  ((meltclosure_ptr_t) /*_.VALCLO___V155*/ meltfptr[154])->rout =
    (meltroutine_ptr_t) ( /*_.VALROUT___V154*/ meltfptr[153]);

  /*touch:DATACLOSURE_ */
  meltgc_touch ( /*_.VALCLO___V155*/ meltfptr[154]);

  /*putslot */
  melt_assertmsg ("putslot checkobj",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALDATA___V156*/ meltfptr[155])) ==
		  MELTOBMAG_OBJECT);

  melt_assertmsg ("putslot checkoff",
		  (1 >= 0
		   && 1 <
		   melt_object_length ((melt_ptr_t)
				       ( /*_.VALDATA___V156*/
					meltfptr[155]))));
  ((meltobject_ptr_t) ( /*_.VALDATA___V156*/ meltfptr[155]))->obj_vartab[1] =
    (melt_ptr_t) (
  /*_.VALSTR___V157*/ meltfptr[156]);

  /*touch:VALUEDATA_ */
  meltgc_touch ( /*_.VALDATA___V156*/ meltfptr[155]);


  melt_dbgtrace_written_object ( /*_.VALDATA___V156*/ meltfptr[155],
				"new static instance");

  MELT_LOCATION ("warmelt-debug.melt:206:/ initchunk");
  /*^touch */
  /*touch:VALUEDATA_ */
  meltgc_touch ( /*_.VALDATA___V158*/ meltfptr[157]);

  MELT_LOCATION ("warmelt-debug.melt:206:/ initchunk");
  /*^touchobj */

  melt_dbgtrace_written_object ( /*_.VALDATA___V158*/ meltfptr[157],
				"new static instance");

  MELT_LOCATION ("warmelt-debug.melt:207:/ initchunk");
  /*^putroutconstnotnull */
  /*putroutconstnotnull */
  melt_assertmsg ("putroutconstnotnull checkrout",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALROUT___V159*/ meltfptr[158])) ==
		  MELTOBMAG_ROUTINE);
  melt_assertmsg ("putroutconstnotnull notnullconst",
		  NULL != /*_.VALDATA___V158*/ meltfptr[157]);
  ((meltroutine_ptr_t) /*_.VALROUT___V159*/ meltfptr[158])->tabval[0] =
    (melt_ptr_t) ( /*_.VALDATA___V158*/ meltfptr[157]);

  MELT_LOCATION ("warmelt-debug.melt:207:/ initchunk");
  /*^putroutconst */
  /*putroutconst */
  if (MELT_HAS_INITIAL_ENVIRONMENT)
    melt_assertmsg ("putroutconst checkrout",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.VALROUT___V159*/ meltfptr[158]))
		    == MELTOBMAG_ROUTINE);
  if (MELT_HAS_INITIAL_ENVIRONMENT)
    melt_checkmsg
      ("putroutconst constnull.drout_109__REGISTER_GIMPLE_DEBUG_FUN#1",
       NULL != ( /*_.CLASS_REFERENCE__V14*/ meltfptr[13]));
  ((meltroutine_ptr_t) /*_.VALROUT___V159*/ meltfptr[158])->tabval[1] =
    (melt_ptr_t) ( /*_.CLASS_REFERENCE__V14*/ meltfptr[13]);

  MELT_LOCATION ("warmelt-debug.melt:207:/ initchunk");
  /*^touch */
  /*touch:IROUTVAL_ */
  meltgc_touch ( /*_.VALROUT___V159*/ meltfptr[158]);

  /*putclosurout#5 */
  melt_assertmsg ("putclosrout#5 checkclo",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALCLO___V160*/ meltfptr[159])) ==
		  MELTOBMAG_CLOSURE);
  melt_assertmsg ("putclosrout#5 checkrout",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALROUT___V159*/ meltfptr[158])) ==
		  MELTOBMAG_ROUTINE);
  ((meltclosure_ptr_t) /*_.VALCLO___V160*/ meltfptr[159])->rout =
    (meltroutine_ptr_t) ( /*_.VALROUT___V159*/ meltfptr[158]);

  /*touch:DATACLOSURE_ */
  meltgc_touch ( /*_.VALCLO___V160*/ meltfptr[159]);

  /*putslot */
  melt_assertmsg ("putslot checkobj",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALDATA___V161*/ meltfptr[160])) ==
		  MELTOBMAG_OBJECT);

  melt_assertmsg ("putslot checkoff",
		  (1 >= 0
		   && 1 <
		   melt_object_length ((melt_ptr_t)
				       ( /*_.VALDATA___V161*/
					meltfptr[160]))));
  ((meltobject_ptr_t) ( /*_.VALDATA___V161*/ meltfptr[160]))->obj_vartab[1] =
    (melt_ptr_t) (
  /*_.VALSTR___V162*/ meltfptr[161]);

  /*touch:VALUEDATA_ */
  meltgc_touch ( /*_.VALDATA___V161*/ meltfptr[160]);


  melt_dbgtrace_written_object ( /*_.VALDATA___V161*/ meltfptr[160],
				"new static instance");

  MELT_LOCATION ("warmelt-debug.melt:219:/ initchunk");
  /*^touch */
  /*touch:VALUEDATA_ */
  meltgc_touch ( /*_.VALDATA___V163*/ meltfptr[162]);

  MELT_LOCATION ("warmelt-debug.melt:219:/ initchunk");
  /*^touchobj */

  melt_dbgtrace_written_object ( /*_.VALDATA___V163*/ meltfptr[162],
				"new static instance");

  MELT_LOCATION ("warmelt-debug.melt:220:/ initchunk");
  /*^putroutconstnotnull */
  /*putroutconstnotnull */
  melt_assertmsg ("putroutconstnotnull checkrout",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALROUT___V164*/ meltfptr[163])) ==
		  MELTOBMAG_ROUTINE);
  melt_assertmsg ("putroutconstnotnull notnullconst",
		  NULL != /*_.VALDATA___V163*/ meltfptr[162]);
  ((meltroutine_ptr_t) /*_.VALROUT___V164*/ meltfptr[163])->tabval[0] =
    (melt_ptr_t) ( /*_.VALDATA___V163*/ meltfptr[162]);

  MELT_LOCATION ("warmelt-debug.melt:220:/ initchunk");
  /*^putroutconst */
  /*putroutconst */
  if (MELT_HAS_INITIAL_ENVIRONMENT)
    melt_assertmsg ("putroutconst checkrout",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.VALROUT___V164*/ meltfptr[163]))
		    == MELTOBMAG_ROUTINE);
  if (MELT_HAS_INITIAL_ENVIRONMENT)
    melt_checkmsg
      ("putroutconst constnull.drout_114__REGISTER_GIMPLESEQ_DEBUG_FUN#1",
       NULL != ( /*_.CLASS_REFERENCE__V14*/ meltfptr[13]));
  ((meltroutine_ptr_t) /*_.VALROUT___V164*/ meltfptr[163])->tabval[1] =
    (melt_ptr_t) ( /*_.CLASS_REFERENCE__V14*/ meltfptr[13]);

  MELT_LOCATION ("warmelt-debug.melt:220:/ initchunk");
  /*^touch */
  /*touch:IROUTVAL_ */
  meltgc_touch ( /*_.VALROUT___V164*/ meltfptr[163]);

  /*putclosurout#6 */
  melt_assertmsg ("putclosrout#6 checkclo",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALCLO___V165*/ meltfptr[164])) ==
		  MELTOBMAG_CLOSURE);
  melt_assertmsg ("putclosrout#6 checkrout",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALROUT___V164*/ meltfptr[163])) ==
		  MELTOBMAG_ROUTINE);
  ((meltclosure_ptr_t) /*_.VALCLO___V165*/ meltfptr[164])->rout =
    (meltroutine_ptr_t) ( /*_.VALROUT___V164*/ meltfptr[163]);

  /*touch:DATACLOSURE_ */
  meltgc_touch ( /*_.VALCLO___V165*/ meltfptr[164]);


#undef meltfram__
}				/*end of meltmod__warmelt_debug__initialmeltchunk_1 */

void
meltmod__warmelt_debug__initialmeltchunk_2 (struct
					    frame_melt_start_this_module_st
					    *meltmeltframptr__,
					    char meltpredefinited[])
{
#define meltfram__ (*meltmeltframptr__)
#undef meltcallcount
#define meltcallcount 0L
  (void) meltpredefinited;
/*putslot*/
  melt_assertmsg ("putslot checkobj",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALDATA___V166*/ meltfptr[165])) ==
		  MELTOBMAG_OBJECT);

  melt_assertmsg ("putslot checkoff",
		  (1 >= 0
		   && 1 <
		   melt_object_length ((melt_ptr_t)
				       ( /*_.VALDATA___V166*/
					meltfptr[165]))));
  ((meltobject_ptr_t) ( /*_.VALDATA___V166*/ meltfptr[165]))->obj_vartab[1] =
    (melt_ptr_t) (
  /*_.VALSTR___V167*/ meltfptr[166]);

  /*touch:VALUEDATA_ */
  meltgc_touch ( /*_.VALDATA___V166*/ meltfptr[165]);


  melt_dbgtrace_written_object ( /*_.VALDATA___V166*/ meltfptr[165],
				"new static instance");

  MELT_LOCATION ("warmelt-debug.melt:232:/ initchunk");
  /*^touch */
  /*touch:VALUEDATA_ */
  meltgc_touch ( /*_.VALDATA___V168*/ meltfptr[167]);

  MELT_LOCATION ("warmelt-debug.melt:232:/ initchunk");
  /*^touchobj */

  melt_dbgtrace_written_object ( /*_.VALDATA___V168*/ meltfptr[167],
				"new static instance");

  MELT_LOCATION ("warmelt-debug.melt:233:/ initchunk");
  /*^putroutconstnotnull */
  /*putroutconstnotnull */
  melt_assertmsg ("putroutconstnotnull checkrout",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALROUT___V169*/ meltfptr[168])) ==
		  MELTOBMAG_ROUTINE);
  melt_assertmsg ("putroutconstnotnull notnullconst",
		  NULL != /*_.VALDATA___V168*/ meltfptr[167]);
  ((meltroutine_ptr_t) /*_.VALROUT___V169*/ meltfptr[168])->tabval[0] =
    (melt_ptr_t) ( /*_.VALDATA___V168*/ meltfptr[167]);

  MELT_LOCATION ("warmelt-debug.melt:233:/ initchunk");
  /*^putroutconst */
  /*putroutconst */
  if (MELT_HAS_INITIAL_ENVIRONMENT)
    melt_assertmsg ("putroutconst checkrout",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.VALROUT___V169*/ meltfptr[168]))
		    == MELTOBMAG_ROUTINE);
  if (MELT_HAS_INITIAL_ENVIRONMENT)
    melt_checkmsg
      ("putroutconst constnull.drout_119__REGISTER_EDGE_DEBUG_FUN#1",
       NULL != ( /*_.CLASS_REFERENCE__V14*/ meltfptr[13]));
  ((meltroutine_ptr_t) /*_.VALROUT___V169*/ meltfptr[168])->tabval[1] =
    (melt_ptr_t) ( /*_.CLASS_REFERENCE__V14*/ meltfptr[13]);

  MELT_LOCATION ("warmelt-debug.melt:233:/ initchunk");
  /*^touch */
  /*touch:IROUTVAL_ */
  meltgc_touch ( /*_.VALROUT___V169*/ meltfptr[168]);

  /*putclosurout#7 */
  melt_assertmsg ("putclosrout#7 checkclo",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALCLO___V170*/ meltfptr[169])) ==
		  MELTOBMAG_CLOSURE);
  melt_assertmsg ("putclosrout#7 checkrout",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALROUT___V169*/ meltfptr[168])) ==
		  MELTOBMAG_ROUTINE);
  ((meltclosure_ptr_t) /*_.VALCLO___V170*/ meltfptr[169])->rout =
    (meltroutine_ptr_t) ( /*_.VALROUT___V169*/ meltfptr[168]);

  /*touch:DATACLOSURE_ */
  meltgc_touch ( /*_.VALCLO___V170*/ meltfptr[169]);

  /*putslot */
  melt_assertmsg ("putslot checkobj",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALDATA___V171*/ meltfptr[170])) ==
		  MELTOBMAG_OBJECT);

  melt_assertmsg ("putslot checkoff",
		  (1 >= 0
		   && 1 <
		   melt_object_length ((melt_ptr_t)
				       ( /*_.VALDATA___V171*/
					meltfptr[170]))));
  ((meltobject_ptr_t) ( /*_.VALDATA___V171*/ meltfptr[170]))->obj_vartab[1] =
    (melt_ptr_t) (
  /*_.VALSTR___V172*/ meltfptr[171]);

  /*touch:VALUEDATA_ */
  meltgc_touch ( /*_.VALDATA___V171*/ meltfptr[170]);


  melt_dbgtrace_written_object ( /*_.VALDATA___V171*/ meltfptr[170],
				"new static instance");

  MELT_LOCATION ("warmelt-debug.melt:245:/ initchunk");
  /*^touch */
  /*touch:VALUEDATA_ */
  meltgc_touch ( /*_.VALDATA___V173*/ meltfptr[172]);

  MELT_LOCATION ("warmelt-debug.melt:245:/ initchunk");
  /*^touchobj */

  melt_dbgtrace_written_object ( /*_.VALDATA___V173*/ meltfptr[172],
				"new static instance");

  MELT_LOCATION ("warmelt-debug.melt:246:/ initchunk");
  /*^putroutconstnotnull */
  /*putroutconstnotnull */
  melt_assertmsg ("putroutconstnotnull checkrout",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALROUT___V174*/ meltfptr[173])) ==
		  MELTOBMAG_ROUTINE);
  melt_assertmsg ("putroutconstnotnull notnullconst",
		  NULL != /*_.VALDATA___V173*/ meltfptr[172]);
  ((meltroutine_ptr_t) /*_.VALROUT___V174*/ meltfptr[173])->tabval[0] =
    (melt_ptr_t) ( /*_.VALDATA___V173*/ meltfptr[172]);

  MELT_LOCATION ("warmelt-debug.melt:246:/ initchunk");
  /*^putroutconst */
  /*putroutconst */
  if (MELT_HAS_INITIAL_ENVIRONMENT)
    melt_assertmsg ("putroutconst checkrout",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.VALROUT___V174*/ meltfptr[173]))
		    == MELTOBMAG_ROUTINE);
  if (MELT_HAS_INITIAL_ENVIRONMENT)
    melt_checkmsg
      ("putroutconst constnull.drout_124__REGISTER_BASICBLOCK_DEBUG_FUN#1",
       NULL != ( /*_.CLASS_REFERENCE__V14*/ meltfptr[13]));
  ((meltroutine_ptr_t) /*_.VALROUT___V174*/ meltfptr[173])->tabval[1] =
    (melt_ptr_t) ( /*_.CLASS_REFERENCE__V14*/ meltfptr[13]);

  MELT_LOCATION ("warmelt-debug.melt:246:/ initchunk");
  /*^touch */
  /*touch:IROUTVAL_ */
  meltgc_touch ( /*_.VALROUT___V174*/ meltfptr[173]);

  /*putclosurout#8 */
  melt_assertmsg ("putclosrout#8 checkclo",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALCLO___V175*/ meltfptr[174])) ==
		  MELTOBMAG_CLOSURE);
  melt_assertmsg ("putclosrout#8 checkrout",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALROUT___V174*/ meltfptr[173])) ==
		  MELTOBMAG_ROUTINE);
  ((meltclosure_ptr_t) /*_.VALCLO___V175*/ meltfptr[174])->rout =
    (meltroutine_ptr_t) ( /*_.VALROUT___V174*/ meltfptr[173]);

  /*touch:DATACLOSURE_ */
  meltgc_touch ( /*_.VALCLO___V175*/ meltfptr[174]);

  /*putslot */
  melt_assertmsg ("putslot checkobj",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALDATA___V176*/ meltfptr[175])) ==
		  MELTOBMAG_OBJECT);

  melt_assertmsg ("putslot checkoff",
		  (1 >= 0
		   && 1 <
		   melt_object_length ((melt_ptr_t)
				       ( /*_.VALDATA___V176*/
					meltfptr[175]))));
  ((meltobject_ptr_t) ( /*_.VALDATA___V176*/ meltfptr[175]))->obj_vartab[1] =
    (melt_ptr_t) (
  /*_.VALSTR___V177*/ meltfptr[176]);

  /*touch:VALUEDATA_ */
  meltgc_touch ( /*_.VALDATA___V176*/ meltfptr[175]);


  melt_dbgtrace_written_object ( /*_.VALDATA___V176*/ meltfptr[175],
				"new static instance");

  MELT_LOCATION ("warmelt-debug.melt:258:/ initchunk");
  /*^touch */
  /*touch:VALUEDATA_ */
  meltgc_touch ( /*_.VALDATA___V178*/ meltfptr[177]);

  MELT_LOCATION ("warmelt-debug.melt:258:/ initchunk");
  /*^touchobj */

  melt_dbgtrace_written_object ( /*_.VALDATA___V178*/ meltfptr[177],
				"new static instance");

  MELT_LOCATION ("warmelt-debug.melt:259:/ initchunk");
  /*^putroutconstnotnull */
  /*putroutconstnotnull */
  melt_assertmsg ("putroutconstnotnull checkrout",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALROUT___V179*/ meltfptr[178])) ==
		  MELTOBMAG_ROUTINE);
  melt_assertmsg ("putroutconstnotnull notnullconst",
		  NULL != /*_.VALDATA___V178*/ meltfptr[177]);
  ((meltroutine_ptr_t) /*_.VALROUT___V179*/ meltfptr[178])->tabval[0] =
    (melt_ptr_t) ( /*_.VALDATA___V178*/ meltfptr[177]);

  MELT_LOCATION ("warmelt-debug.melt:259:/ initchunk");
  /*^putroutconst */
  /*putroutconst */
  if (MELT_HAS_INITIAL_ENVIRONMENT)
    melt_assertmsg ("putroutconst checkrout",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.VALROUT___V179*/ meltfptr[178]))
		    == MELTOBMAG_ROUTINE);
  if (MELT_HAS_INITIAL_ENVIRONMENT)
    melt_checkmsg
      ("putroutconst constnull.drout_129__REGISTER_LOOP_DEBUG_FUN#1",
       NULL != ( /*_.CLASS_REFERENCE__V14*/ meltfptr[13]));
  ((meltroutine_ptr_t) /*_.VALROUT___V179*/ meltfptr[178])->tabval[1] =
    (melt_ptr_t) ( /*_.CLASS_REFERENCE__V14*/ meltfptr[13]);

  MELT_LOCATION ("warmelt-debug.melt:259:/ initchunk");
  /*^touch */
  /*touch:IROUTVAL_ */
  meltgc_touch ( /*_.VALROUT___V179*/ meltfptr[178]);

  /*putclosurout#9 */
  melt_assertmsg ("putclosrout#9 checkclo",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALCLO___V180*/ meltfptr[179])) ==
		  MELTOBMAG_CLOSURE);
  melt_assertmsg ("putclosrout#9 checkrout",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALROUT___V179*/ meltfptr[178])) ==
		  MELTOBMAG_ROUTINE);
  ((meltclosure_ptr_t) /*_.VALCLO___V180*/ meltfptr[179])->rout =
    (meltroutine_ptr_t) ( /*_.VALROUT___V179*/ meltfptr[178]);

  /*touch:DATACLOSURE_ */
  meltgc_touch ( /*_.VALCLO___V180*/ meltfptr[179]);

  /*putslot */
  melt_assertmsg ("putslot checkobj",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALDATA___V181*/ meltfptr[180])) ==
		  MELTOBMAG_OBJECT);

  melt_assertmsg ("putslot checkoff",
		  (1 >= 0
		   && 1 <
		   melt_object_length ((melt_ptr_t)
				       ( /*_.VALDATA___V181*/
					meltfptr[180]))));
  ((meltobject_ptr_t) ( /*_.VALDATA___V181*/ meltfptr[180]))->obj_vartab[1] =
    (melt_ptr_t) (
  /*_.VALSTR___V182*/ meltfptr[181]);

  /*touch:VALUEDATA_ */
  meltgc_touch ( /*_.VALDATA___V181*/ meltfptr[180]);


  melt_dbgtrace_written_object ( /*_.VALDATA___V181*/ meltfptr[180],
				"new static instance");

  /*putslot */
  melt_assertmsg ("putslot checkobj",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALDATA___V183*/ meltfptr[182])) ==
		  MELTOBMAG_OBJECT);

  melt_assertmsg ("putslot checkoff",
		  (1 >= 0
		   && 1 <
		   melt_object_length ((melt_ptr_t)
				       ( /*_.VALDATA___V183*/
					meltfptr[182]))));
  ((meltobject_ptr_t) ( /*_.VALDATA___V183*/ meltfptr[182]))->obj_vartab[1] =
    (melt_ptr_t) (
  /*_.VALSTR___V184*/ meltfptr[183]);

  /*touch:VALUEDATA_ */
  meltgc_touch ( /*_.VALDATA___V183*/ meltfptr[182]);


  melt_dbgtrace_written_object ( /*_.VALDATA___V183*/ meltfptr[182],
				"new static instance");

  MELT_LOCATION ("warmelt-debug.melt:271:/ initchunk");
  /*^putslot */
  /*putslot */
  melt_assertmsg ("putslot checkobj",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALDATA___V185*/ meltfptr[184])) ==
		  MELTOBMAG_OBJECT);

  melt_assertmsg ("putslot checkoff",
		  (1 >= 0
		   && 1 <
		   melt_object_length ((melt_ptr_t)
				       ( /*_.VALDATA___V185*/
					meltfptr[184]))));
  ((meltobject_ptr_t) ( /*_.VALDATA___V185*/ meltfptr[184]))->obj_vartab[1] =
    (melt_ptr_t) (
  /*_.VALSTR___V186*/ meltfptr[185]);

  MELT_LOCATION ("warmelt-debug.melt:271:/ initchunk");
  /*^putslot */
  /*putslot */
  melt_assertmsg ("putslot checkobj",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALDATA___V185*/ meltfptr[184])) ==
		  MELTOBMAG_OBJECT);

  melt_assertmsg ("putslot checkoff",
		  (4 >= 0
		   && 4 <
		   melt_object_length ((melt_ptr_t)
				       ( /*_.VALDATA___V185*/
					meltfptr[184]))));
  ((meltobject_ptr_t) ( /*_.VALDATA___V185*/ meltfptr[184]))->obj_vartab[4] =
    (melt_ptr_t) (
  /*_.DISCR_CLOSURE__V16*/ meltfptr[15]);

  MELT_LOCATION ("warmelt-debug.melt:271:/ initchunk");
  /*^touch */
  /*touch:VALUEDATA_ */
  meltgc_touch ( /*_.VALDATA___V185*/ meltfptr[184]);

  MELT_LOCATION ("warmelt-debug.melt:271:/ initchunk");
  /*^touchobj */

  melt_dbgtrace_written_object ( /*_.VALDATA___V185*/ meltfptr[184],
				"new static instance");

  /*putslot */
  melt_assertmsg ("putslot checkobj",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALDATA___V187*/ meltfptr[186])) ==
		  MELTOBMAG_OBJECT);

  melt_assertmsg ("putslot checkoff",
		  (1 >= 0
		   && 1 <
		   melt_object_length ((melt_ptr_t)
				       ( /*_.VALDATA___V187*/
					meltfptr[186]))));
  ((meltobject_ptr_t) ( /*_.VALDATA___V187*/ meltfptr[186]))->obj_vartab[1] =
    (melt_ptr_t) (
  /*_.VALSTR___V188*/ meltfptr[187]);

  /*touch:VALUEDATA_ */
  meltgc_touch ( /*_.VALDATA___V187*/ meltfptr[186]);


  melt_dbgtrace_written_object ( /*_.VALDATA___V187*/ meltfptr[186],
				"new static instance");

  MELT_LOCATION ("warmelt-debug.melt:280:/ initchunk");
  /*^putroutconst */
  /*putroutconst */
  if (MELT_HAS_INITIAL_ENVIRONMENT)
    melt_assertmsg ("putroutconst checkrout",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.VALROUT___V189*/ meltfptr[188]))
		    == MELTOBMAG_ROUTINE);
  if (MELT_HAS_INITIAL_ENVIRONMENT)
    melt_checkmsg ("putroutconst constnull.drout_139__MELT_DEBUG_FUN#0",
		   NULL != ( /*_.INITIAL_SYSTEM_DATA__V8*/ meltfptr[7]));
  ((meltroutine_ptr_t) /*_.VALROUT___V189*/ meltfptr[188])->tabval[0] =
    (melt_ptr_t) ( /*_.INITIAL_SYSTEM_DATA__V8*/ meltfptr[7]);

  MELT_LOCATION ("warmelt-debug.melt:280:/ initchunk");
  /*^putroutconst */
  /*putroutconst */
  if (MELT_HAS_INITIAL_ENVIRONMENT)
    melt_assertmsg ("putroutconst checkrout",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.VALROUT___V189*/ meltfptr[188]))
		    == MELTOBMAG_ROUTINE);
  if (MELT_HAS_INITIAL_ENVIRONMENT)
    melt_checkmsg ("putroutconst constnull.drout_139__MELT_DEBUG_FUN#1",
		   NULL != ( /*_.CLASS_SYSTEM_DATA__V9*/ meltfptr[8]));
  ((meltroutine_ptr_t) /*_.VALROUT___V189*/ meltfptr[188])->tabval[1] =
    (melt_ptr_t) ( /*_.CLASS_SYSTEM_DATA__V9*/ meltfptr[8]);

  MELT_LOCATION ("warmelt-debug.melt:280:/ initchunk");
  /*^putroutconst */
  /*putroutconst */
  if (MELT_HAS_INITIAL_ENVIRONMENT)
    melt_assertmsg ("putroutconst checkrout",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.VALROUT___V189*/ meltfptr[188]))
		    == MELTOBMAG_ROUTINE);
  if (MELT_HAS_INITIAL_ENVIRONMENT)
    melt_checkmsg ("putroutconst constnull.drout_139__MELT_DEBUG_FUN#2",
		   NULL != ( /*_.DISCR_MAP_OBJECTS__V10*/ meltfptr[9]));
  ((meltroutine_ptr_t) /*_.VALROUT___V189*/ meltfptr[188])->tabval[2] =
    (melt_ptr_t) ( /*_.DISCR_MAP_OBJECTS__V10*/ meltfptr[9]);

  MELT_LOCATION ("warmelt-debug.melt:280:/ initchunk");
  /*^putroutconst */
  /*putroutconst */
  if (MELT_HAS_INITIAL_ENVIRONMENT)
    melt_assertmsg ("putroutconst checkrout",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.VALROUT___V189*/ meltfptr[188]))
		    == MELTOBMAG_ROUTINE);
  if (MELT_HAS_INITIAL_ENVIRONMENT)
    melt_checkmsg ("putroutconst constnull.drout_139__MELT_DEBUG_FUN#3",
		   NULL != ( /*_.DISCR_INTEGER__V11*/ meltfptr[10]));
  ((meltroutine_ptr_t) /*_.VALROUT___V189*/ meltfptr[188])->tabval[3] =
    (melt_ptr_t) ( /*_.DISCR_INTEGER__V11*/ meltfptr[10]);

  MELT_LOCATION ("warmelt-debug.melt:280:/ initchunk");
  /*^putroutconst */
  /*putroutconst */
  if (MELT_HAS_INITIAL_ENVIRONMENT)
    melt_assertmsg ("putroutconst checkrout",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.VALROUT___V189*/ meltfptr[188]))
		    == MELTOBMAG_ROUTINE);
  if (MELT_HAS_INITIAL_ENVIRONMENT)
    melt_checkmsg ("putroutconst constnull.drout_139__MELT_DEBUG_FUN#4",
		   NULL != ( /*_.DISCR_STRBUF__V12*/ meltfptr[11]));
  ((meltroutine_ptr_t) /*_.VALROUT___V189*/ meltfptr[188])->tabval[4] =
    (melt_ptr_t) ( /*_.DISCR_STRBUF__V12*/ meltfptr[11]);

  MELT_LOCATION ("warmelt-debug.melt:280:/ initchunk");
  /*^putroutconstnotnull */
  /*putroutconstnotnull */
  melt_assertmsg ("putroutconstnotnull checkrout",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALROUT___V189*/ meltfptr[188])) ==
		  MELTOBMAG_ROUTINE);
  melt_assertmsg ("putroutconstnotnull notnullconst",
		  NULL != /*_.VALDATA___V51*/ meltfptr[50]);
  ((meltroutine_ptr_t) /*_.VALROUT___V189*/ meltfptr[188])->tabval[5] =
    (melt_ptr_t) ( /*_.VALDATA___V51*/ meltfptr[50]);

  MELT_LOCATION ("warmelt-debug.melt:280:/ initchunk");
  /*^putroutconst */
  /*putroutconst */
  if (MELT_HAS_INITIAL_ENVIRONMENT)
    melt_assertmsg ("putroutconst checkrout",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.VALROUT___V189*/ meltfptr[188]))
		    == MELTOBMAG_ROUTINE);
  if (MELT_HAS_INITIAL_ENVIRONMENT)
    melt_checkmsg ("putroutconst constnull.drout_139__MELT_DEBUG_FUN#6",
		   NULL != ( /*_.ADD2OUT__V17*/ meltfptr[16]));
  ((meltroutine_ptr_t) /*_.VALROUT___V189*/ meltfptr[188])->tabval[6] =
    (melt_ptr_t) ( /*_.ADD2OUT__V17*/ meltfptr[16]);

  MELT_LOCATION ("warmelt-debug.melt:280:/ initchunk");
  /*^putroutconst */
  /*putroutconst */
  if (MELT_HAS_INITIAL_ENVIRONMENT)
    melt_assertmsg ("putroutconst checkrout",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.VALROUT___V189*/ meltfptr[188]))
		    == MELTOBMAG_ROUTINE);
  if (MELT_HAS_INITIAL_ENVIRONMENT)
    melt_checkmsg ("putroutconst constnull.drout_139__MELT_DEBUG_FUN#7",
		   NULL != ( /*_.CSTRING_PREFIXED__V18*/ meltfptr[17]));
  ((meltroutine_ptr_t) /*_.VALROUT___V189*/ meltfptr[188])->tabval[7] =
    (melt_ptr_t) ( /*_.CSTRING_PREFIXED__V18*/ meltfptr[17]);

  MELT_LOCATION ("warmelt-debug.melt:280:/ initchunk");
  /*^putroutconstnotnull */
  /*putroutconstnotnull */
  melt_assertmsg ("putroutconstnotnull checkrout",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALROUT___V189*/ meltfptr[188])) ==
		  MELTOBMAG_ROUTINE);
  melt_assertmsg ("putroutconstnotnull notnullconst",
		  NULL != /*_.VALDATA___V185*/ meltfptr[184]);
  ((meltroutine_ptr_t) /*_.VALROUT___V189*/ meltfptr[188])->tabval[8] =
    (melt_ptr_t) ( /*_.VALDATA___V185*/ meltfptr[184]);

  MELT_LOCATION ("warmelt-debug.melt:280:/ initchunk");
  /*^putroutconstnotnull */
  /*putroutconstnotnull */
  melt_assertmsg ("putroutconstnotnull checkrout",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALROUT___V189*/ meltfptr[188])) ==
		  MELTOBMAG_ROUTINE);
  melt_assertmsg ("putroutconstnotnull notnullconst",
		  NULL != /*_.VALCLO___V112*/ meltfptr[111]);
  ((meltroutine_ptr_t) /*_.VALROUT___V189*/ meltfptr[188])->tabval[9] =
    (melt_ptr_t) ( /*_.VALCLO___V112*/ meltfptr[111]);

  MELT_LOCATION ("warmelt-debug.melt:280:/ initchunk");
  /*^putroutconstnotnull */
  /*putroutconstnotnull */
  melt_assertmsg ("putroutconstnotnull checkrout",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALROUT___V189*/ meltfptr[188])) ==
		  MELTOBMAG_ROUTINE);
  melt_assertmsg ("putroutconstnotnull notnullconst",
		  NULL != /*_.VALDATA___V153*/ meltfptr[152]);
  ((meltroutine_ptr_t) /*_.VALROUT___V189*/ meltfptr[188])->tabval[10] =
    (melt_ptr_t) ( /*_.VALDATA___V153*/ meltfptr[152]);

  MELT_LOCATION ("warmelt-debug.melt:280:/ initchunk");
  /*^putroutconst */
  /*putroutconst */
  if (MELT_HAS_INITIAL_ENVIRONMENT)
    melt_assertmsg ("putroutconst checkrout",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.VALROUT___V189*/ meltfptr[188]))
		    == MELTOBMAG_ROUTINE);
  if (MELT_HAS_INITIAL_ENVIRONMENT)
    melt_checkmsg ("putroutconst constnull.drout_139__MELT_DEBUG_FUN#11",
		   NULL != ( /*_.CLASS_REFERENCE__V14*/ meltfptr[13]));
  ((meltroutine_ptr_t) /*_.VALROUT___V189*/ meltfptr[188])->tabval[11] =
    (melt_ptr_t) ( /*_.CLASS_REFERENCE__V14*/ meltfptr[13]);

  MELT_LOCATION ("warmelt-debug.melt:280:/ initchunk");
  /*^putroutconstnotnull */
  /*putroutconstnotnull */
  melt_assertmsg ("putroutconstnotnull checkrout",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALROUT___V189*/ meltfptr[188])) ==
		  MELTOBMAG_ROUTINE);
  melt_assertmsg ("putroutconstnotnull notnullconst",
		  NULL != /*_.VALDATA___V158*/ meltfptr[157]);
  ((meltroutine_ptr_t) /*_.VALROUT___V189*/ meltfptr[188])->tabval[12] =
    (melt_ptr_t) ( /*_.VALDATA___V158*/ meltfptr[157]);

  MELT_LOCATION ("warmelt-debug.melt:280:/ initchunk");
  /*^putroutconstnotnull */
  /*putroutconstnotnull */
  melt_assertmsg ("putroutconstnotnull checkrout",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALROUT___V189*/ meltfptr[188])) ==
		  MELTOBMAG_ROUTINE);
  melt_assertmsg ("putroutconstnotnull notnullconst",
		  NULL != /*_.VALDATA___V163*/ meltfptr[162]);
  ((meltroutine_ptr_t) /*_.VALROUT___V189*/ meltfptr[188])->tabval[13] =
    (melt_ptr_t) ( /*_.VALDATA___V163*/ meltfptr[162]);

  MELT_LOCATION ("warmelt-debug.melt:280:/ initchunk");
  /*^putroutconstnotnull */
  /*putroutconstnotnull */
  melt_assertmsg ("putroutconstnotnull checkrout",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALROUT___V189*/ meltfptr[188])) ==
		  MELTOBMAG_ROUTINE);
  melt_assertmsg ("putroutconstnotnull notnullconst",
		  NULL != /*_.VALDATA___V168*/ meltfptr[167]);
  ((meltroutine_ptr_t) /*_.VALROUT___V189*/ meltfptr[188])->tabval[14] =
    (melt_ptr_t) ( /*_.VALDATA___V168*/ meltfptr[167]);

  MELT_LOCATION ("warmelt-debug.melt:280:/ initchunk");
  /*^putroutconstnotnull */
  /*putroutconstnotnull */
  melt_assertmsg ("putroutconstnotnull checkrout",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALROUT___V189*/ meltfptr[188])) ==
		  MELTOBMAG_ROUTINE);
  melt_assertmsg ("putroutconstnotnull notnullconst",
		  NULL != /*_.VALDATA___V173*/ meltfptr[172]);
  ((meltroutine_ptr_t) /*_.VALROUT___V189*/ meltfptr[188])->tabval[15] =
    (melt_ptr_t) ( /*_.VALDATA___V173*/ meltfptr[172]);

  MELT_LOCATION ("warmelt-debug.melt:280:/ initchunk");
  /*^putroutconstnotnull */
  /*putroutconstnotnull */
  melt_assertmsg ("putroutconstnotnull checkrout",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALROUT___V189*/ meltfptr[188])) ==
		  MELTOBMAG_ROUTINE);
  melt_assertmsg ("putroutconstnotnull notnullconst",
		  NULL != /*_.VALDATA___V178*/ meltfptr[177]);
  ((meltroutine_ptr_t) /*_.VALROUT___V189*/ meltfptr[188])->tabval[16] =
    (melt_ptr_t) ( /*_.VALDATA___V178*/ meltfptr[177]);

  MELT_LOCATION ("warmelt-debug.melt:280:/ initchunk");
  /*^putroutconst */
  /*putroutconst */
  if (MELT_HAS_INITIAL_ENVIRONMENT)
    melt_assertmsg ("putroutconst checkrout",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.VALROUT___V189*/ meltfptr[188]))
		    == MELTOBMAG_ROUTINE);
  if (MELT_HAS_INITIAL_ENVIRONMENT)
    melt_checkmsg ("putroutconst constnull.drout_139__MELT_DEBUG_FUN#17",
		   NULL != ( /*_.CLASS_NAMED__V19*/ meltfptr[18]));
  ((meltroutine_ptr_t) /*_.VALROUT___V189*/ meltfptr[188])->tabval[17] =
    (melt_ptr_t) ( /*_.CLASS_NAMED__V19*/ meltfptr[18]);

  MELT_LOCATION ("warmelt-debug.melt:280:/ initchunk");
  /*^putroutconst */
  /*putroutconst */
  if (MELT_HAS_INITIAL_ENVIRONMENT)
    melt_assertmsg ("putroutconst checkrout",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.VALROUT___V189*/ meltfptr[188]))
		    == MELTOBMAG_ROUTINE);
  if (MELT_HAS_INITIAL_ENVIRONMENT)
    melt_checkmsg ("putroutconst constnull.drout_139__MELT_DEBUG_FUN#18",
		   NULL != ( /*_.CLASS_CTYPE__V20*/ meltfptr[19]));
  ((meltroutine_ptr_t) /*_.VALROUT___V189*/ meltfptr[188])->tabval[18] =
    (melt_ptr_t) ( /*_.CLASS_CTYPE__V20*/ meltfptr[19]);

  MELT_LOCATION ("warmelt-debug.melt:280:/ initchunk");
  /*^touch */
  /*touch:IROUTVAL_ */
  meltgc_touch ( /*_.VALROUT___V189*/ meltfptr[188]);

  /*putclosurout#10 */
  melt_assertmsg ("putclosrout#10 checkclo",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALCLO___V190*/ meltfptr[189])) ==
		  MELTOBMAG_CLOSURE);
  melt_assertmsg ("putclosrout#10 checkrout",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALROUT___V189*/ meltfptr[188])) ==
		  MELTOBMAG_ROUTINE);
  ((meltclosure_ptr_t) /*_.VALCLO___V190*/ meltfptr[189])->rout =
    (meltroutine_ptr_t) ( /*_.VALROUT___V189*/ meltfptr[188]);

  /*touch:DATACLOSURE_ */
  meltgc_touch ( /*_.VALCLO___V190*/ meltfptr[189]);

  /*putslot */
  melt_assertmsg ("putslot checkobj",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALDATA___V191*/ meltfptr[190])) ==
		  MELTOBMAG_OBJECT);

  melt_assertmsg ("putslot checkoff",
		  (1 >= 0
		   && 1 <
		   melt_object_length ((melt_ptr_t)
				       ( /*_.VALDATA___V191*/
					meltfptr[190]))));
  ((meltobject_ptr_t) ( /*_.VALDATA___V191*/ meltfptr[190]))->obj_vartab[1] =
    (melt_ptr_t) (
  /*_.VALSTR___V192*/ meltfptr[191]);

  /*touch:VALUEDATA_ */
  meltgc_touch ( /*_.VALDATA___V191*/ meltfptr[190]);


  melt_dbgtrace_written_object ( /*_.VALDATA___V191*/ meltfptr[190],
				"new static instance");

  /*putslot */
  melt_assertmsg ("putslot checkobj",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALDATA___V193*/ meltfptr[192])) ==
		  MELTOBMAG_OBJECT);

  melt_assertmsg ("putslot checkoff",
		  (1 >= 0
		   && 1 <
		   melt_object_length ((melt_ptr_t)
				       ( /*_.VALDATA___V193*/
					meltfptr[192]))));
  ((meltobject_ptr_t) ( /*_.VALDATA___V193*/ meltfptr[192]))->obj_vartab[1] =
    (melt_ptr_t) (
  /*_.VALSTR___V194*/ meltfptr[193]);

  /*touch:VALUEDATA_ */
  meltgc_touch ( /*_.VALDATA___V193*/ meltfptr[192]);


  melt_dbgtrace_written_object ( /*_.VALDATA___V193*/ meltfptr[192],
				"new static instance");

  /*putslot */
  melt_assertmsg ("putslot checkobj",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALDATA___V195*/ meltfptr[194])) ==
		  MELTOBMAG_OBJECT);

  melt_assertmsg ("putslot checkoff",
		  (1 >= 0
		   && 1 <
		   melt_object_length ((melt_ptr_t)
				       ( /*_.VALDATA___V195*/
					meltfptr[194]))));
  ((meltobject_ptr_t) ( /*_.VALDATA___V195*/ meltfptr[194]))->obj_vartab[1] =
    (melt_ptr_t) (
  /*_.VALSTR___V196*/ meltfptr[195]);

  /*touch:VALUEDATA_ */
  meltgc_touch ( /*_.VALDATA___V195*/ meltfptr[194]);


  melt_dbgtrace_written_object ( /*_.VALDATA___V195*/ meltfptr[194],
				"new static instance");

  /*putslot */
  melt_assertmsg ("putslot checkobj",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALDATA___V197*/ meltfptr[196])) ==
		  MELTOBMAG_OBJECT);

  melt_assertmsg ("putslot checkoff",
		  (1 >= 0
		   && 1 <
		   melt_object_length ((melt_ptr_t)
				       ( /*_.VALDATA___V197*/
					meltfptr[196]))));
  ((meltobject_ptr_t) ( /*_.VALDATA___V197*/ meltfptr[196]))->obj_vartab[1] =
    (melt_ptr_t) (
  /*_.VALSTR___V198*/ meltfptr[197]);

  /*touch:VALUEDATA_ */
  meltgc_touch ( /*_.VALDATA___V197*/ meltfptr[196]);


  melt_dbgtrace_written_object ( /*_.VALDATA___V197*/ meltfptr[196],
				"new static instance");

  MELT_LOCATION ("warmelt-debug.melt:502:/ initchunk");
  /*^putroutconstnotnull */
  /*putroutconstnotnull */
  melt_assertmsg ("putroutconstnotnull checkrout",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALROUT___V199*/ meltfptr[198])) ==
		  MELTOBMAG_ROUTINE);
  melt_assertmsg ("putroutconstnotnull notnullconst",
		  NULL != /*_.VALDATA___V51*/ meltfptr[50]);
  ((meltroutine_ptr_t) /*_.VALROUT___V199*/ meltfptr[198])->tabval[0] =
    (melt_ptr_t) ( /*_.VALDATA___V51*/ meltfptr[50]);

  MELT_LOCATION ("warmelt-debug.melt:502:/ initchunk");
  /*^putroutconstnotnull */
  /*putroutconstnotnull */
  melt_assertmsg ("putroutconstnotnull checkrout",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALROUT___V199*/ meltfptr[198])) ==
		  MELTOBMAG_ROUTINE);
  melt_assertmsg ("putroutconstnotnull notnullconst",
		  NULL != /*_.VALDATA___V69*/ meltfptr[68]);
  ((meltroutine_ptr_t) /*_.VALROUT___V199*/ meltfptr[198])->tabval[1] =
    (melt_ptr_t) ( /*_.VALDATA___V69*/ meltfptr[68]);

  MELT_LOCATION ("warmelt-debug.melt:502:/ initchunk");
  /*^putroutconstnotnull */
  /*putroutconstnotnull */
  melt_assertmsg ("putroutconstnotnull checkrout",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALROUT___V199*/ meltfptr[198])) ==
		  MELTOBMAG_ROUTINE);
  melt_assertmsg ("putroutconstnotnull notnullconst",
		  NULL != /*_.VALCLO___V112*/ meltfptr[111]);
  ((meltroutine_ptr_t) /*_.VALROUT___V199*/ meltfptr[198])->tabval[2] =
    (melt_ptr_t) ( /*_.VALCLO___V112*/ meltfptr[111]);

  MELT_LOCATION ("warmelt-debug.melt:502:/ initchunk");
  /*^touch */
  /*touch:IROUTVAL_ */
  meltgc_touch ( /*_.VALROUT___V199*/ meltfptr[198]);

  /*putclosurout#11 */
  melt_assertmsg ("putclosrout#11 checkclo",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALCLO___V200*/ meltfptr[199])) ==
		  MELTOBMAG_CLOSURE);
  melt_assertmsg ("putclosrout#11 checkrout",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALROUT___V199*/ meltfptr[198])) ==
		  MELTOBMAG_ROUTINE);
  ((meltclosure_ptr_t) /*_.VALCLO___V200*/ meltfptr[199])->rout =
    (meltroutine_ptr_t) ( /*_.VALROUT___V199*/ meltfptr[198]);

  /*touch:DATACLOSURE_ */
  meltgc_touch ( /*_.VALCLO___V200*/ meltfptr[199]);

  MELT_LOCATION ("warmelt-debug.melt:557:/ initchunk");
  /*^putroutconstnotnull */
  /*putroutconstnotnull */
  melt_assertmsg ("putroutconstnotnull checkrout",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALROUT___V201*/ meltfptr[200])) ==
		  MELTOBMAG_ROUTINE);
  melt_assertmsg ("putroutconstnotnull notnullconst",
		  NULL != /*_.VALDATA___V51*/ meltfptr[50]);
  ((meltroutine_ptr_t) /*_.VALROUT___V201*/ meltfptr[200])->tabval[0] =
    (melt_ptr_t) ( /*_.VALDATA___V51*/ meltfptr[50]);

  MELT_LOCATION ("warmelt-debug.melt:557:/ initchunk");
  /*^putroutconstnotnull */
  /*putroutconstnotnull */
  melt_assertmsg ("putroutconstnotnull checkrout",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALROUT___V201*/ meltfptr[200])) ==
		  MELTOBMAG_ROUTINE);
  melt_assertmsg ("putroutconstnotnull notnullconst",
		  NULL != /*_.VALDATA___V69*/ meltfptr[68]);
  ((meltroutine_ptr_t) /*_.VALROUT___V201*/ meltfptr[200])->tabval[1] =
    (melt_ptr_t) ( /*_.VALDATA___V69*/ meltfptr[68]);

  MELT_LOCATION ("warmelt-debug.melt:557:/ initchunk");
  /*^putroutconstnotnull */
  /*putroutconstnotnull */
  melt_assertmsg ("putroutconstnotnull checkrout",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALROUT___V201*/ meltfptr[200])) ==
		  MELTOBMAG_ROUTINE);
  melt_assertmsg ("putroutconstnotnull notnullconst",
		  NULL != /*_.VALDATA___V116*/ meltfptr[115]);
  ((meltroutine_ptr_t) /*_.VALROUT___V201*/ meltfptr[200])->tabval[2] =
    (melt_ptr_t) ( /*_.VALDATA___V116*/ meltfptr[115]);

  MELT_LOCATION ("warmelt-debug.melt:557:/ initchunk");
  /*^touch */
  /*touch:IROUTVAL_ */
  meltgc_touch ( /*_.VALROUT___V201*/ meltfptr[200]);

  /*putclosurout#12 */
  melt_assertmsg ("putclosrout#12 checkclo",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALCLO___V202*/ meltfptr[201])) ==
		  MELTOBMAG_CLOSURE);
  melt_assertmsg ("putclosrout#12 checkrout",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALROUT___V201*/ meltfptr[200])) ==
		  MELTOBMAG_ROUTINE);
  ((meltclosure_ptr_t) /*_.VALCLO___V202*/ meltfptr[201])->rout =
    (meltroutine_ptr_t) ( /*_.VALROUT___V201*/ meltfptr[200]);

  /*touch:DATACLOSURE_ */
  meltgc_touch ( /*_.VALCLO___V202*/ meltfptr[201]);

  /*putclosurout#13 */
  melt_assertmsg ("putclosrout#13 checkclo",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALCLO___V204*/ meltfptr[203])) ==
		  MELTOBMAG_CLOSURE);
  melt_assertmsg ("putclosrout#13 checkrout",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALROUT___V203*/ meltfptr[202])) ==
		  MELTOBMAG_ROUTINE);
  ((meltclosure_ptr_t) /*_.VALCLO___V204*/ meltfptr[203])->rout =
    (meltroutine_ptr_t) ( /*_.VALROUT___V203*/ meltfptr[202]);

  /*touch:DATACLOSURE_ */
  meltgc_touch ( /*_.VALCLO___V204*/ meltfptr[203]);

  /*putslot */
  melt_assertmsg ("putslot checkobj",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALDATA___V205*/ meltfptr[204])) ==
		  MELTOBMAG_OBJECT);

  melt_assertmsg ("putslot checkoff",
		  (1 >= 0
		   && 1 <
		   melt_object_length ((melt_ptr_t)
				       ( /*_.VALDATA___V205*/
					meltfptr[204]))));
  ((meltobject_ptr_t) ( /*_.VALDATA___V205*/ meltfptr[204]))->obj_vartab[1] =
    (melt_ptr_t) (
  /*_.VALSTR___V206*/ meltfptr[205]);

  /*touch:VALUEDATA_ */
  meltgc_touch ( /*_.VALDATA___V205*/ meltfptr[204]);


  melt_dbgtrace_written_object ( /*_.VALDATA___V205*/ meltfptr[204],
				"new static instance");

  /*putslot */
  melt_assertmsg ("putslot checkobj",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALDATA___V207*/ meltfptr[206])) ==
		  MELTOBMAG_OBJECT);

  melt_assertmsg ("putslot checkoff",
		  (1 >= 0
		   && 1 <
		   melt_object_length ((melt_ptr_t)
				       ( /*_.VALDATA___V207*/
					meltfptr[206]))));
  ((meltobject_ptr_t) ( /*_.VALDATA___V207*/ meltfptr[206]))->obj_vartab[1] =
    (melt_ptr_t) (
  /*_.VALSTR___V208*/ meltfptr[207]);

  /*touch:VALUEDATA_ */
  meltgc_touch ( /*_.VALDATA___V207*/ meltfptr[206]);


  melt_dbgtrace_written_object ( /*_.VALDATA___V207*/ meltfptr[206],
				"new static instance");

  MELT_LOCATION ("warmelt-debug.melt:609:/ initchunk");
  /*^putroutconstnotnull */
  /*putroutconstnotnull */
  melt_assertmsg ("putroutconstnotnull checkrout",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALROUT___V209*/ meltfptr[208])) ==
		  MELTOBMAG_ROUTINE);
  melt_assertmsg ("putroutconstnotnull notnullconst",
		  NULL != /*_.VALDATA___V51*/ meltfptr[50]);
  ((meltroutine_ptr_t) /*_.VALROUT___V209*/ meltfptr[208])->tabval[0] =
    (melt_ptr_t) ( /*_.VALDATA___V51*/ meltfptr[50]);

  MELT_LOCATION ("warmelt-debug.melt:609:/ initchunk");
  /*^putroutconst */
  /*putroutconst */
  if (MELT_HAS_INITIAL_ENVIRONMENT)
    melt_assertmsg ("putroutconst checkrout",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.VALROUT___V209*/ meltfptr[208]))
		    == MELTOBMAG_ROUTINE);
  if (MELT_HAS_INITIAL_ENVIRONMENT)
    melt_checkmsg ("putroutconst constnull.drout_163__DBGOUT_STRING_METHOD#1",
		   NULL != ( /*_.DISCR_STRING__V23*/ meltfptr[22]));
  ((meltroutine_ptr_t) /*_.VALROUT___V209*/ meltfptr[208])->tabval[1] =
    (melt_ptr_t) ( /*_.DISCR_STRING__V23*/ meltfptr[22]);

  MELT_LOCATION ("warmelt-debug.melt:609:/ initchunk");
  /*^touch */
  /*touch:IROUTVAL_ */
  meltgc_touch ( /*_.VALROUT___V209*/ meltfptr[208]);

  /*putclosurout#14 */
  melt_assertmsg ("putclosrout#14 checkclo",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALCLO___V210*/ meltfptr[209])) ==
		  MELTOBMAG_CLOSURE);
  melt_assertmsg ("putclosrout#14 checkrout",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALROUT___V209*/ meltfptr[208])) ==
		  MELTOBMAG_ROUTINE);
  ((meltclosure_ptr_t) /*_.VALCLO___V210*/ meltfptr[209])->rout =
    (meltroutine_ptr_t) ( /*_.VALROUT___V209*/ meltfptr[208]);

  /*touch:DATACLOSURE_ */
  meltgc_touch ( /*_.VALCLO___V210*/ meltfptr[209]);

  /*putslot */
  melt_assertmsg ("putslot checkobj",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALDATA___V211*/ meltfptr[210])) ==
		  MELTOBMAG_OBJECT);

  melt_assertmsg ("putslot checkoff",
		  (1 >= 0
		   && 1 <
		   melt_object_length ((melt_ptr_t)
				       ( /*_.VALDATA___V211*/
					meltfptr[210]))));
  ((meltobject_ptr_t) ( /*_.VALDATA___V211*/ meltfptr[210]))->obj_vartab[1] =
    (melt_ptr_t) (
  /*_.VALSTR___V212*/ meltfptr[211]);

  /*touch:VALUEDATA_ */
  meltgc_touch ( /*_.VALDATA___V211*/ meltfptr[210]);


  melt_dbgtrace_written_object ( /*_.VALDATA___V211*/ meltfptr[210],
				"new static instance");

  MELT_LOCATION ("warmelt-debug.melt:631:/ initchunk");
  /*^putroutconstnotnull */
  /*putroutconstnotnull */
  melt_assertmsg ("putroutconstnotnull checkrout",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALROUT___V213*/ meltfptr[212])) ==
		  MELTOBMAG_ROUTINE);
  melt_assertmsg ("putroutconstnotnull notnullconst",
		  NULL != /*_.VALDATA___V51*/ meltfptr[50]);
  ((meltroutine_ptr_t) /*_.VALROUT___V213*/ meltfptr[212])->tabval[0] =
    (melt_ptr_t) ( /*_.VALDATA___V51*/ meltfptr[50]);

  MELT_LOCATION ("warmelt-debug.melt:631:/ initchunk");
  /*^putroutconst */
  /*putroutconst */
  if (MELT_HAS_INITIAL_ENVIRONMENT)
    melt_assertmsg ("putroutconst checkrout",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.VALROUT___V213*/ meltfptr[212]))
		    == MELTOBMAG_ROUTINE);
  if (MELT_HAS_INITIAL_ENVIRONMENT)
    melt_checkmsg ("putroutconst constnull.drout_167__DBGOUT_STRBUF_METHOD#1",
		   NULL != ( /*_.CLASS_NAMED__V19*/ meltfptr[18]));
  ((meltroutine_ptr_t) /*_.VALROUT___V213*/ meltfptr[212])->tabval[1] =
    (melt_ptr_t) ( /*_.CLASS_NAMED__V19*/ meltfptr[18]);

  MELT_LOCATION ("warmelt-debug.melt:631:/ initchunk");
  /*^putroutconst */
  /*putroutconst */
  if (MELT_HAS_INITIAL_ENVIRONMENT)
    melt_assertmsg ("putroutconst checkrout",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.VALROUT___V213*/ meltfptr[212]))
		    == MELTOBMAG_ROUTINE);
  if (MELT_HAS_INITIAL_ENVIRONMENT)
    melt_checkmsg ("putroutconst constnull.drout_167__DBGOUT_STRBUF_METHOD#2",
		   NULL != ( /*_.ADD2OUT__V17*/ meltfptr[16]));
  ((meltroutine_ptr_t) /*_.VALROUT___V213*/ meltfptr[212])->tabval[2] =
    (melt_ptr_t) ( /*_.ADD2OUT__V17*/ meltfptr[16]);

  MELT_LOCATION ("warmelt-debug.melt:631:/ initchunk");
  /*^touch */
  /*touch:IROUTVAL_ */
  meltgc_touch ( /*_.VALROUT___V213*/ meltfptr[212]);

  /*putclosurout#15 */
  melt_assertmsg ("putclosrout#15 checkclo",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALCLO___V214*/ meltfptr[213])) ==
		  MELTOBMAG_CLOSURE);
  melt_assertmsg ("putclosrout#15 checkrout",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALROUT___V213*/ meltfptr[212])) ==
		  MELTOBMAG_ROUTINE);
  ((meltclosure_ptr_t) /*_.VALCLO___V214*/ meltfptr[213])->rout =
    (meltroutine_ptr_t) ( /*_.VALROUT___V213*/ meltfptr[212]);

  /*touch:DATACLOSURE_ */
  meltgc_touch ( /*_.VALCLO___V214*/ meltfptr[213]);

  MELT_LOCATION ("warmelt-debug.melt:717:/ initchunk");
  /*^putroutconstnotnull */
  /*putroutconstnotnull */
  melt_assertmsg ("putroutconstnotnull checkrout",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALROUT___V215*/ meltfptr[214])) ==
		  MELTOBMAG_ROUTINE);
  melt_assertmsg ("putroutconstnotnull notnullconst",
		  NULL != /*_.VALDATA___V51*/ meltfptr[50]);
  ((meltroutine_ptr_t) /*_.VALROUT___V215*/ meltfptr[214])->tabval[0] =
    (melt_ptr_t) ( /*_.VALDATA___V51*/ meltfptr[50]);

  MELT_LOCATION ("warmelt-debug.melt:717:/ initchunk");
  /*^putroutconst */
  /*putroutconst */
  if (MELT_HAS_INITIAL_ENVIRONMENT)
    melt_assertmsg ("putroutconst checkrout",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.VALROUT___V215*/ meltfptr[214]))
		    == MELTOBMAG_ROUTINE);
  if (MELT_HAS_INITIAL_ENVIRONMENT)
    melt_checkmsg
      ("putroutconst constnull.drout_169__DBGOUT_INTEGER_METHOD#1",
       NULL != ( /*_.DISCR_INTEGER__V11*/ meltfptr[10]));
  ((meltroutine_ptr_t) /*_.VALROUT___V215*/ meltfptr[214])->tabval[1] =
    (melt_ptr_t) ( /*_.DISCR_INTEGER__V11*/ meltfptr[10]);

  MELT_LOCATION ("warmelt-debug.melt:717:/ initchunk");
  /*^putroutconst */
  /*putroutconst */
  if (MELT_HAS_INITIAL_ENVIRONMENT)
    melt_assertmsg ("putroutconst checkrout",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.VALROUT___V215*/ meltfptr[214]))
		    == MELTOBMAG_ROUTINE);
  if (MELT_HAS_INITIAL_ENVIRONMENT)
    melt_checkmsg
      ("putroutconst constnull.drout_169__DBGOUT_INTEGER_METHOD#2",
       NULL != ( /*_.DISCR_CONSTANT_INTEGER__V24*/ meltfptr[23]));
  ((meltroutine_ptr_t) /*_.VALROUT___V215*/ meltfptr[214])->tabval[2] =
    (melt_ptr_t) ( /*_.DISCR_CONSTANT_INTEGER__V24*/ meltfptr[23]);

  MELT_LOCATION ("warmelt-debug.melt:717:/ initchunk");
  /*^putroutconst */
  /*putroutconst */
  if (MELT_HAS_INITIAL_ENVIRONMENT)
    melt_assertmsg ("putroutconst checkrout",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.VALROUT___V215*/ meltfptr[214]))
		    == MELTOBMAG_ROUTINE);
  if (MELT_HAS_INITIAL_ENVIRONMENT)
    melt_checkmsg
      ("putroutconst constnull.drout_169__DBGOUT_INTEGER_METHOD#3",
       NULL != ( /*_.ADD2OUT__V17*/ meltfptr[16]));
  ((meltroutine_ptr_t) /*_.VALROUT___V215*/ meltfptr[214])->tabval[3] =
    (melt_ptr_t) ( /*_.ADD2OUT__V17*/ meltfptr[16]);

  MELT_LOCATION ("warmelt-debug.melt:717:/ initchunk");
  /*^touch */
  /*touch:IROUTVAL_ */
  meltgc_touch ( /*_.VALROUT___V215*/ meltfptr[214]);

  /*putclosurout#16 */
  melt_assertmsg ("putclosrout#16 checkclo",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALCLO___V216*/ meltfptr[215])) ==
		  MELTOBMAG_CLOSURE);
  melt_assertmsg ("putclosrout#16 checkrout",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALROUT___V215*/ meltfptr[214])) ==
		  MELTOBMAG_ROUTINE);
  ((meltclosure_ptr_t) /*_.VALCLO___V216*/ meltfptr[215])->rout =
    (meltroutine_ptr_t) ( /*_.VALROUT___V215*/ meltfptr[214]);

  /*touch:DATACLOSURE_ */
  meltgc_touch ( /*_.VALCLO___V216*/ meltfptr[215]);

  /*putslot */
  melt_assertmsg ("putslot checkobj",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALDATA___V217*/ meltfptr[216])) ==
		  MELTOBMAG_OBJECT);

  melt_assertmsg ("putslot checkoff",
		  (1 >= 0
		   && 1 <
		   melt_object_length ((melt_ptr_t)
				       ( /*_.VALDATA___V217*/
					meltfptr[216]))));
  ((meltobject_ptr_t) ( /*_.VALDATA___V217*/ meltfptr[216]))->obj_vartab[1] =
    (melt_ptr_t) (
  /*_.VALSTR___V218*/ meltfptr[217]);

  /*touch:VALUEDATA_ */
  meltgc_touch ( /*_.VALDATA___V217*/ meltfptr[216]);


  melt_dbgtrace_written_object ( /*_.VALDATA___V217*/ meltfptr[216],
				"new static instance");

  MELT_LOCATION ("warmelt-debug.melt:738:/ initchunk");
  /*^putroutconstnotnull */
  /*putroutconstnotnull */
  melt_assertmsg ("putroutconstnotnull checkrout",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALROUT___V219*/ meltfptr[218])) ==
		  MELTOBMAG_ROUTINE);
  melt_assertmsg ("putroutconstnotnull notnullconst",
		  NULL != /*_.VALDATA___V51*/ meltfptr[50]);
  ((meltroutine_ptr_t) /*_.VALROUT___V219*/ meltfptr[218])->tabval[0] =
    (melt_ptr_t) ( /*_.VALDATA___V51*/ meltfptr[50]);

  MELT_LOCATION ("warmelt-debug.melt:738:/ initchunk");
  /*^putroutconst */
  /*putroutconst */
  if (MELT_HAS_INITIAL_ENVIRONMENT)
    melt_assertmsg ("putroutconst checkrout",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.VALROUT___V219*/ meltfptr[218]))
		    == MELTOBMAG_ROUTINE);
  if (MELT_HAS_INITIAL_ENVIRONMENT)
    melt_checkmsg ("putroutconst constnull.drout_173__DBGOUT_MIXINT_METHOD#1",
		   NULL != ( /*_.DISCR_MIXED_INTEGER__V25*/ meltfptr[24]));
  ((meltroutine_ptr_t) /*_.VALROUT___V219*/ meltfptr[218])->tabval[1] =
    (melt_ptr_t) ( /*_.DISCR_MIXED_INTEGER__V25*/ meltfptr[24]);

  MELT_LOCATION ("warmelt-debug.melt:738:/ initchunk");
  /*^putroutconstnotnull */
  /*putroutconstnotnull */
  melt_assertmsg ("putroutconstnotnull checkrout",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALROUT___V219*/ meltfptr[218])) ==
		  MELTOBMAG_ROUTINE);
  melt_assertmsg ("putroutconstnotnull notnullconst",
		  NULL != /*_.VALDATA___V69*/ meltfptr[68]);
  ((meltroutine_ptr_t) /*_.VALROUT___V219*/ meltfptr[218])->tabval[2] =
    (melt_ptr_t) ( /*_.VALDATA___V69*/ meltfptr[68]);

  MELT_LOCATION ("warmelt-debug.melt:738:/ initchunk");
  /*^putroutconstnotnull */
  /*putroutconstnotnull */
  melt_assertmsg ("putroutconstnotnull checkrout",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALROUT___V219*/ meltfptr[218])) ==
		  MELTOBMAG_ROUTINE);
  melt_assertmsg ("putroutconstnotnull notnullconst",
		  NULL != /*_.VALCLO___V112*/ meltfptr[111]);
  ((meltroutine_ptr_t) /*_.VALROUT___V219*/ meltfptr[218])->tabval[3] =
    (melt_ptr_t) ( /*_.VALCLO___V112*/ meltfptr[111]);

  MELT_LOCATION ("warmelt-debug.melt:738:/ initchunk");
  /*^touch */
  /*touch:IROUTVAL_ */
  meltgc_touch ( /*_.VALROUT___V219*/ meltfptr[218]);

  /*putclosurout#17 */
  melt_assertmsg ("putclosrout#17 checkclo",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALCLO___V220*/ meltfptr[219])) ==
		  MELTOBMAG_CLOSURE);
  melt_assertmsg ("putclosrout#17 checkrout",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALROUT___V219*/ meltfptr[218])) ==
		  MELTOBMAG_ROUTINE);
  ((meltclosure_ptr_t) /*_.VALCLO___V220*/ meltfptr[219])->rout =
    (meltroutine_ptr_t) ( /*_.VALROUT___V219*/ meltfptr[218]);

  /*touch:DATACLOSURE_ */
  meltgc_touch ( /*_.VALCLO___V220*/ meltfptr[219]);


#undef meltfram__
}				/*end of meltmod__warmelt_debug__initialmeltchunk_2 */

void
meltmod__warmelt_debug__initialmeltchunk_3 (struct
					    frame_melt_start_this_module_st
					    *meltmeltframptr__,
					    char meltpredefinited[])
{
#define meltfram__ (*meltmeltframptr__)
#undef meltcallcount
#define meltcallcount 0L
  (void) meltpredefinited;
/*putslot*/
  melt_assertmsg ("putslot checkobj",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALDATA___V221*/ meltfptr[220])) ==
		  MELTOBMAG_OBJECT);

  melt_assertmsg ("putslot checkoff",
		  (1 >= 0
		   && 1 <
		   melt_object_length ((melt_ptr_t)
				       ( /*_.VALDATA___V221*/
					meltfptr[220]))));
  ((meltobject_ptr_t) ( /*_.VALDATA___V221*/ meltfptr[220]))->obj_vartab[1] =
    (melt_ptr_t) (
  /*_.VALSTR___V222*/ meltfptr[221]);

  /*touch:VALUEDATA_ */
  meltgc_touch ( /*_.VALDATA___V221*/ meltfptr[220]);


  melt_dbgtrace_written_object ( /*_.VALDATA___V221*/ meltfptr[220],
				"new static instance");

  MELT_LOCATION ("warmelt-debug.melt:768:/ initchunk");
  /*^putroutconstnotnull */
  /*putroutconstnotnull */
  melt_assertmsg ("putroutconstnotnull checkrout",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALROUT___V223*/ meltfptr[222])) ==
		  MELTOBMAG_ROUTINE);
  melt_assertmsg ("putroutconstnotnull notnullconst",
		  NULL != /*_.VALDATA___V51*/ meltfptr[50]);
  ((meltroutine_ptr_t) /*_.VALROUT___V223*/ meltfptr[222])->tabval[0] =
    (melt_ptr_t) ( /*_.VALDATA___V51*/ meltfptr[50]);

  MELT_LOCATION ("warmelt-debug.melt:768:/ initchunk");
  /*^putroutconst */
  /*putroutconst */
  if (MELT_HAS_INITIAL_ENVIRONMENT)
    melt_assertmsg ("putroutconst checkrout",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.VALROUT___V223*/ meltfptr[222]))
		    == MELTOBMAG_ROUTINE);
  if (MELT_HAS_INITIAL_ENVIRONMENT)
    melt_checkmsg ("putroutconst constnull.drout_177__DBGOUT_MIXLOC_METHOD#1",
		   NULL != ( /*_.DISCR_MIXED_LOCATION__V26*/ meltfptr[25]));
  ((meltroutine_ptr_t) /*_.VALROUT___V223*/ meltfptr[222])->tabval[1] =
    (melt_ptr_t) ( /*_.DISCR_MIXED_LOCATION__V26*/ meltfptr[25]);

  MELT_LOCATION ("warmelt-debug.melt:768:/ initchunk");
  /*^putroutconstnotnull */
  /*putroutconstnotnull */
  melt_assertmsg ("putroutconstnotnull checkrout",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALROUT___V223*/ meltfptr[222])) ==
		  MELTOBMAG_ROUTINE);
  melt_assertmsg ("putroutconstnotnull notnullconst",
		  NULL != /*_.VALDATA___V69*/ meltfptr[68]);
  ((meltroutine_ptr_t) /*_.VALROUT___V223*/ meltfptr[222])->tabval[2] =
    (melt_ptr_t) ( /*_.VALDATA___V69*/ meltfptr[68]);

  MELT_LOCATION ("warmelt-debug.melt:768:/ initchunk");
  /*^touch */
  /*touch:IROUTVAL_ */
  meltgc_touch ( /*_.VALROUT___V223*/ meltfptr[222]);

  /*putclosurout#18 */
  melt_assertmsg ("putclosrout#18 checkclo",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALCLO___V224*/ meltfptr[223])) ==
		  MELTOBMAG_CLOSURE);
  melt_assertmsg ("putclosrout#18 checkrout",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALROUT___V223*/ meltfptr[222])) ==
		  MELTOBMAG_ROUTINE);
  ((meltclosure_ptr_t) /*_.VALCLO___V224*/ meltfptr[223])->rout =
    (meltroutine_ptr_t) ( /*_.VALROUT___V223*/ meltfptr[222]);

  /*touch:DATACLOSURE_ */
  meltgc_touch ( /*_.VALCLO___V224*/ meltfptr[223]);

  /*putslot */
  melt_assertmsg ("putslot checkobj",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALDATA___V225*/ meltfptr[224])) ==
		  MELTOBMAG_OBJECT);

  melt_assertmsg ("putslot checkoff",
		  (1 >= 0
		   && 1 <
		   melt_object_length ((melt_ptr_t)
				       ( /*_.VALDATA___V225*/
					meltfptr[224]))));
  ((meltobject_ptr_t) ( /*_.VALDATA___V225*/ meltfptr[224]))->obj_vartab[1] =
    (melt_ptr_t) (
  /*_.VALSTR___V226*/ meltfptr[225]);

  /*touch:VALUEDATA_ */
  meltgc_touch ( /*_.VALDATA___V225*/ meltfptr[224]);


  melt_dbgtrace_written_object ( /*_.VALDATA___V225*/ meltfptr[224],
				"new static instance");

  MELT_LOCATION ("warmelt-debug.melt:800:/ initchunk");
  /*^putroutconstnotnull */
  /*putroutconstnotnull */
  melt_assertmsg ("putroutconstnotnull checkrout",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALROUT___V227*/ meltfptr[226])) ==
		  MELTOBMAG_ROUTINE);
  melt_assertmsg ("putroutconstnotnull notnullconst",
		  NULL != /*_.VALDATA___V51*/ meltfptr[50]);
  ((meltroutine_ptr_t) /*_.VALROUT___V227*/ meltfptr[226])->tabval[0] =
    (melt_ptr_t) ( /*_.VALDATA___V51*/ meltfptr[50]);

  MELT_LOCATION ("warmelt-debug.melt:800:/ initchunk");
  /*^putroutconstnotnull */
  /*putroutconstnotnull */
  melt_assertmsg ("putroutconstnotnull checkrout",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALROUT___V227*/ meltfptr[226])) ==
		  MELTOBMAG_ROUTINE);
  melt_assertmsg ("putroutconstnotnull notnullconst",
		  NULL != /*_.VALDATA___V69*/ meltfptr[68]);
  ((meltroutine_ptr_t) /*_.VALROUT___V227*/ meltfptr[226])->tabval[1] =
    (melt_ptr_t) ( /*_.VALDATA___V69*/ meltfptr[68]);

  MELT_LOCATION ("warmelt-debug.melt:800:/ initchunk");
  /*^putroutconstnotnull */
  /*putroutconstnotnull */
  melt_assertmsg ("putroutconstnotnull checkrout",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALROUT___V227*/ meltfptr[226])) ==
		  MELTOBMAG_ROUTINE);
  melt_assertmsg ("putroutconstnotnull notnullconst",
		  NULL != /*_.VALCLO___V112*/ meltfptr[111]);
  ((meltroutine_ptr_t) /*_.VALROUT___V227*/ meltfptr[226])->tabval[2] =
    (melt_ptr_t) ( /*_.VALCLO___V112*/ meltfptr[111]);

  MELT_LOCATION ("warmelt-debug.melt:800:/ initchunk");
  /*^touch */
  /*touch:IROUTVAL_ */
  meltgc_touch ( /*_.VALROUT___V227*/ meltfptr[226]);

  /*putclosurout#19 */
  melt_assertmsg ("putclosrout#19 checkclo",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALCLO___V228*/ meltfptr[227])) ==
		  MELTOBMAG_CLOSURE);
  melt_assertmsg ("putclosrout#19 checkrout",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALROUT___V227*/ meltfptr[226])) ==
		  MELTOBMAG_ROUTINE);
  ((meltclosure_ptr_t) /*_.VALCLO___V228*/ meltfptr[227])->rout =
    (meltroutine_ptr_t) ( /*_.VALROUT___V227*/ meltfptr[226]);

  /*touch:DATACLOSURE_ */
  meltgc_touch ( /*_.VALCLO___V228*/ meltfptr[227]);

  /*putslot */
  melt_assertmsg ("putslot checkobj",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALDATA___V229*/ meltfptr[228])) ==
		  MELTOBMAG_OBJECT);

  melt_assertmsg ("putslot checkoff",
		  (1 >= 0
		   && 1 <
		   melt_object_length ((melt_ptr_t)
				       ( /*_.VALDATA___V229*/
					meltfptr[228]))));
  ((meltobject_ptr_t) ( /*_.VALDATA___V229*/ meltfptr[228]))->obj_vartab[1] =
    (melt_ptr_t) (
  /*_.VALSTR___V230*/ meltfptr[229]);

  /*touch:VALUEDATA_ */
  meltgc_touch ( /*_.VALDATA___V229*/ meltfptr[228]);


  melt_dbgtrace_written_object ( /*_.VALDATA___V229*/ meltfptr[228],
				"new static instance");

  MELT_LOCATION ("warmelt-debug.melt:825:/ initchunk");
  /*^putroutconstnotnull */
  /*putroutconstnotnull */
  melt_assertmsg ("putroutconstnotnull checkrout",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALROUT___V231*/ meltfptr[230])) ==
		  MELTOBMAG_ROUTINE);
  melt_assertmsg ("putroutconstnotnull notnullconst",
		  NULL != /*_.VALDATA___V51*/ meltfptr[50]);
  ((meltroutine_ptr_t) /*_.VALROUT___V231*/ meltfptr[230])->tabval[0] =
    (melt_ptr_t) ( /*_.VALDATA___V51*/ meltfptr[50]);

  MELT_LOCATION ("warmelt-debug.melt:825:/ initchunk");
  /*^putroutconst */
  /*putroutconst */
  if (MELT_HAS_INITIAL_ENVIRONMENT)
    melt_assertmsg ("putroutconst checkrout",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.VALROUT___V231*/ meltfptr[230]))
		    == MELTOBMAG_ROUTINE);
  if (MELT_HAS_INITIAL_ENVIRONMENT)
    melt_checkmsg
      ("putroutconst constnull.drout_185__DBGOUT_MULTIPLE_METHOD#1",
       NULL != ( /*_.DISCR_MULTIPLE__V28*/ meltfptr[27]));
  ((meltroutine_ptr_t) /*_.VALROUT___V231*/ meltfptr[230])->tabval[1] =
    (melt_ptr_t) ( /*_.DISCR_MULTIPLE__V28*/ meltfptr[27]);

  MELT_LOCATION ("warmelt-debug.melt:825:/ initchunk");
  /*^putroutconstnotnull */
  /*putroutconstnotnull */
  melt_assertmsg ("putroutconstnotnull checkrout",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALROUT___V231*/ meltfptr[230])) ==
		  MELTOBMAG_ROUTINE);
  melt_assertmsg ("putroutconstnotnull notnullconst",
		  NULL != /*_.VALCLO___V112*/ meltfptr[111]);
  ((meltroutine_ptr_t) /*_.VALROUT___V231*/ meltfptr[230])->tabval[2] =
    (melt_ptr_t) ( /*_.VALCLO___V112*/ meltfptr[111]);

  MELT_LOCATION ("warmelt-debug.melt:825:/ initchunk");
  /*^touch */
  /*touch:IROUTVAL_ */
  meltgc_touch ( /*_.VALROUT___V231*/ meltfptr[230]);

  /*putclosurout#20 */
  melt_assertmsg ("putclosrout#20 checkclo",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALCLO___V232*/ meltfptr[231])) ==
		  MELTOBMAG_CLOSURE);
  melt_assertmsg ("putclosrout#20 checkrout",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALROUT___V231*/ meltfptr[230])) ==
		  MELTOBMAG_ROUTINE);
  ((meltclosure_ptr_t) /*_.VALCLO___V232*/ meltfptr[231])->rout =
    (meltroutine_ptr_t) ( /*_.VALROUT___V231*/ meltfptr[230]);

  /*touch:DATACLOSURE_ */
  meltgc_touch ( /*_.VALCLO___V232*/ meltfptr[231]);

  /*putslot */
  melt_assertmsg ("putslot checkobj",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALDATA___V233*/ meltfptr[232])) ==
		  MELTOBMAG_OBJECT);

  melt_assertmsg ("putslot checkoff",
		  (1 >= 0
		   && 1 <
		   melt_object_length ((melt_ptr_t)
				       ( /*_.VALDATA___V233*/
					meltfptr[232]))));
  ((meltobject_ptr_t) ( /*_.VALDATA___V233*/ meltfptr[232]))->obj_vartab[1] =
    (melt_ptr_t) (
  /*_.VALSTR___V234*/ meltfptr[233]);

  /*touch:VALUEDATA_ */
  meltgc_touch ( /*_.VALDATA___V233*/ meltfptr[232]);


  melt_dbgtrace_written_object ( /*_.VALDATA___V233*/ meltfptr[232],
				"new static instance");

  MELT_LOCATION ("warmelt-debug.melt:858:/ initchunk");
  /*^putroutconstnotnull */
  /*putroutconstnotnull */
  melt_assertmsg ("putroutconstnotnull checkrout",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALROUT___V235*/ meltfptr[234])) ==
		  MELTOBMAG_ROUTINE);
  melt_assertmsg ("putroutconstnotnull notnullconst",
		  NULL != /*_.VALDATA___V51*/ meltfptr[50]);
  ((meltroutine_ptr_t) /*_.VALROUT___V235*/ meltfptr[234])->tabval[0] =
    (melt_ptr_t) ( /*_.VALDATA___V51*/ meltfptr[50]);

  MELT_LOCATION ("warmelt-debug.melt:858:/ initchunk");
  /*^putroutconst */
  /*putroutconst */
  if (MELT_HAS_INITIAL_ENVIRONMENT)
    melt_assertmsg ("putroutconst checkrout",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.VALROUT___V235*/ meltfptr[234]))
		    == MELTOBMAG_ROUTINE);
  if (MELT_HAS_INITIAL_ENVIRONMENT)
    melt_checkmsg
      ("putroutconst constnull.drout_189__DBGOUT_ROUTINE_METHOD#1",
       NULL != ( /*_.DISCR_INTEGER__V11*/ meltfptr[10]));
  ((meltroutine_ptr_t) /*_.VALROUT___V235*/ meltfptr[234])->tabval[1] =
    (melt_ptr_t) ( /*_.DISCR_INTEGER__V11*/ meltfptr[10]);

  MELT_LOCATION ("warmelt-debug.melt:858:/ initchunk");
  /*^putroutconst */
  /*putroutconst */
  if (MELT_HAS_INITIAL_ENVIRONMENT)
    melt_assertmsg ("putroutconst checkrout",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.VALROUT___V235*/ meltfptr[234]))
		    == MELTOBMAG_ROUTINE);
  if (MELT_HAS_INITIAL_ENVIRONMENT)
    melt_checkmsg
      ("putroutconst constnull.drout_189__DBGOUT_ROUTINE_METHOD#2",
       NULL != ( /*_.DISCR_ROUTINE__V29*/ meltfptr[28]));
  ((meltroutine_ptr_t) /*_.VALROUT___V235*/ meltfptr[234])->tabval[2] =
    (melt_ptr_t) ( /*_.DISCR_ROUTINE__V29*/ meltfptr[28]);

  MELT_LOCATION ("warmelt-debug.melt:858:/ initchunk");
  /*^putroutconst */
  /*putroutconst */
  if (MELT_HAS_INITIAL_ENVIRONMENT)
    melt_assertmsg ("putroutconst checkrout",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.VALROUT___V235*/ meltfptr[234]))
		    == MELTOBMAG_ROUTINE);
  if (MELT_HAS_INITIAL_ENVIRONMENT)
    melt_checkmsg
      ("putroutconst constnull.drout_189__DBGOUT_ROUTINE_METHOD#3",
       NULL != ( /*_.ROUTINE_EVERY__V30*/ meltfptr[29]));
  ((meltroutine_ptr_t) /*_.VALROUT___V235*/ meltfptr[234])->tabval[3] =
    (melt_ptr_t) ( /*_.ROUTINE_EVERY__V30*/ meltfptr[29]);

  MELT_LOCATION ("warmelt-debug.melt:858:/ initchunk");
  /*^putroutconstnotnull */
  /*putroutconstnotnull */
  melt_assertmsg ("putroutconstnotnull checkrout",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALROUT___V235*/ meltfptr[234])) ==
		  MELTOBMAG_ROUTINE);
  melt_assertmsg ("putroutconstnotnull notnullconst",
		  NULL != /*_.VALCLO___V112*/ meltfptr[111]);
  ((meltroutine_ptr_t) /*_.VALROUT___V235*/ meltfptr[234])->tabval[4] =
    (melt_ptr_t) ( /*_.VALCLO___V112*/ meltfptr[111]);

  MELT_LOCATION ("warmelt-debug.melt:873:/ initchunk");
  /*^putroutconstnotnull */
  /*putroutconstnotnull */
  melt_assertmsg ("putroutconstnotnull checkrout",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALROUT___V236*/ meltfptr[235])) ==
		  MELTOBMAG_ROUTINE);
  melt_assertmsg ("putroutconstnotnull notnullconst",
		  NULL != /*_.VALCLO___V112*/ meltfptr[111]);
  ((meltroutine_ptr_t) /*_.VALROUT___V236*/ meltfptr[235])->tabval[0] =
    (melt_ptr_t) ( /*_.VALCLO___V112*/ meltfptr[111]);

  MELT_LOCATION ("warmelt-debug.melt:873:/ initchunk");
  /*^touch */
  /*touch:IROUTVAL_ */
  meltgc_touch ( /*_.VALROUT___V236*/ meltfptr[235]);

  MELT_LOCATION ("warmelt-debug.melt:858:/ initchunk");
  /*^putroutconst */
  /*putroutconst */
  if (MELT_HAS_INITIAL_ENVIRONMENT)
    melt_assertmsg ("putroutconst checkrout",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.VALROUT___V235*/ meltfptr[234]))
		    == MELTOBMAG_ROUTINE);
  if (MELT_HAS_INITIAL_ENVIRONMENT)
    melt_checkmsg
      ("putroutconst constnull.drout_189__DBGOUT_ROUTINE_METHOD#5",
       NULL != ( /*_.VALROUT___V236*/ meltfptr[235]));
  ((meltroutine_ptr_t) /*_.VALROUT___V235*/ meltfptr[234])->tabval[5] =
    (melt_ptr_t) ( /*_.VALROUT___V236*/ meltfptr[235]);

  MELT_LOCATION ("warmelt-debug.melt:858:/ initchunk");
  /*^touch */
  /*touch:IROUTVAL_ */
  meltgc_touch ( /*_.VALROUT___V235*/ meltfptr[234]);

  /*putclosurout#21 */
  melt_assertmsg ("putclosrout#21 checkclo",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALCLO___V237*/ meltfptr[236])) ==
		  MELTOBMAG_CLOSURE);
  melt_assertmsg ("putclosrout#21 checkrout",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALROUT___V235*/ meltfptr[234])) ==
		  MELTOBMAG_ROUTINE);
  ((meltclosure_ptr_t) /*_.VALCLO___V237*/ meltfptr[236])->rout =
    (meltroutine_ptr_t) ( /*_.VALROUT___V235*/ meltfptr[234]);

  /*touch:DATACLOSURE_ */
  meltgc_touch ( /*_.VALCLO___V237*/ meltfptr[236]);

  /*putslot */
  melt_assertmsg ("putslot checkobj",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALDATA___V238*/ meltfptr[237])) ==
		  MELTOBMAG_OBJECT);

  melt_assertmsg ("putslot checkoff",
		  (1 >= 0
		   && 1 <
		   melt_object_length ((melt_ptr_t)
				       ( /*_.VALDATA___V238*/
					meltfptr[237]))));
  ((meltobject_ptr_t) ( /*_.VALDATA___V238*/ meltfptr[237]))->obj_vartab[1] =
    (melt_ptr_t) (
  /*_.VALSTR___V239*/ meltfptr[238]);

  /*touch:VALUEDATA_ */
  meltgc_touch ( /*_.VALDATA___V238*/ meltfptr[237]);


  melt_dbgtrace_written_object ( /*_.VALDATA___V238*/ meltfptr[237],
				"new static instance");

  /*putslot */
  melt_assertmsg ("putslot checkobj",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALDATA___V240*/ meltfptr[239])) ==
		  MELTOBMAG_OBJECT);

  melt_assertmsg ("putslot checkoff",
		  (1 >= 0
		   && 1 <
		   melt_object_length ((melt_ptr_t)
				       ( /*_.VALDATA___V240*/
					meltfptr[239]))));
  ((meltobject_ptr_t) ( /*_.VALDATA___V240*/ meltfptr[239]))->obj_vartab[1] =
    (melt_ptr_t) (
  /*_.VALSTR___V241*/ meltfptr[240]);

  /*touch:VALUEDATA_ */
  meltgc_touch ( /*_.VALDATA___V240*/ meltfptr[239]);


  melt_dbgtrace_written_object ( /*_.VALDATA___V240*/ meltfptr[239],
				"new static instance");

  MELT_LOCATION ("warmelt-debug.melt:885:/ initchunk");
  /*^putroutconstnotnull */
  /*putroutconstnotnull */
  melt_assertmsg ("putroutconstnotnull checkrout",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALROUT___V242*/ meltfptr[241])) ==
		  MELTOBMAG_ROUTINE);
  melt_assertmsg ("putroutconstnotnull notnullconst",
		  NULL != /*_.VALDATA___V51*/ meltfptr[50]);
  ((meltroutine_ptr_t) /*_.VALROUT___V242*/ meltfptr[241])->tabval[0] =
    (melt_ptr_t) ( /*_.VALDATA___V51*/ meltfptr[50]);

  MELT_LOCATION ("warmelt-debug.melt:885:/ initchunk");
  /*^putroutconst */
  /*putroutconst */
  if (MELT_HAS_INITIAL_ENVIRONMENT)
    melt_assertmsg ("putroutconst checkrout",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.VALROUT___V242*/ meltfptr[241]))
		    == MELTOBMAG_ROUTINE);
  if (MELT_HAS_INITIAL_ENVIRONMENT)
    melt_checkmsg
      ("putroutconst constnull.drout_196__DBGOUT_CLOSURE_METHOD#1",
       NULL != ( /*_.DISCR_CLOSURE__V16*/ meltfptr[15]));
  ((meltroutine_ptr_t) /*_.VALROUT___V242*/ meltfptr[241])->tabval[1] =
    (melt_ptr_t) ( /*_.DISCR_CLOSURE__V16*/ meltfptr[15]);

  MELT_LOCATION ("warmelt-debug.melt:885:/ initchunk");
  /*^putroutconstnotnull */
  /*putroutconstnotnull */
  melt_assertmsg ("putroutconstnotnull checkrout",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALROUT___V242*/ meltfptr[241])) ==
		  MELTOBMAG_ROUTINE);
  melt_assertmsg ("putroutconstnotnull notnullconst",
		  NULL != /*_.VALCLO___V112*/ meltfptr[111]);
  ((meltroutine_ptr_t) /*_.VALROUT___V242*/ meltfptr[241])->tabval[2] =
    (melt_ptr_t) ( /*_.VALCLO___V112*/ meltfptr[111]);

  MELT_LOCATION ("warmelt-debug.melt:885:/ initchunk");
  /*^putroutconstnotnull */
  /*putroutconstnotnull */
  melt_assertmsg ("putroutconstnotnull checkrout",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALROUT___V242*/ meltfptr[241])) ==
		  MELTOBMAG_ROUTINE);
  melt_assertmsg ("putroutconstnotnull notnullconst",
		  NULL != /*_.VALDATA___V69*/ meltfptr[68]);
  ((meltroutine_ptr_t) /*_.VALROUT___V242*/ meltfptr[241])->tabval[3] =
    (melt_ptr_t) ( /*_.VALDATA___V69*/ meltfptr[68]);

  MELT_LOCATION ("warmelt-debug.melt:885:/ initchunk");
  /*^touch */
  /*touch:IROUTVAL_ */
  meltgc_touch ( /*_.VALROUT___V242*/ meltfptr[241]);

  /*putclosurout#22 */
  melt_assertmsg ("putclosrout#22 checkclo",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALCLO___V243*/ meltfptr[242])) ==
		  MELTOBMAG_CLOSURE);
  melt_assertmsg ("putclosrout#22 checkrout",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALROUT___V242*/ meltfptr[241])) ==
		  MELTOBMAG_ROUTINE);
  ((meltclosure_ptr_t) /*_.VALCLO___V243*/ meltfptr[242])->rout =
    (meltroutine_ptr_t) ( /*_.VALROUT___V242*/ meltfptr[241]);

  /*touch:DATACLOSURE_ */
  meltgc_touch ( /*_.VALCLO___V243*/ meltfptr[242]);

  MELT_LOCATION ("warmelt-debug.melt:930:/ initchunk");
  /*^putroutconstnotnull */
  /*putroutconstnotnull */
  melt_assertmsg ("putroutconstnotnull checkrout",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALROUT___V244*/ meltfptr[243])) ==
		  MELTOBMAG_ROUTINE);
  melt_assertmsg ("putroutconstnotnull notnullconst",
		  NULL != /*_.VALDATA___V51*/ meltfptr[50]);
  ((meltroutine_ptr_t) /*_.VALROUT___V244*/ meltfptr[243])->tabval[0] =
    (melt_ptr_t) ( /*_.VALDATA___V51*/ meltfptr[50]);

  MELT_LOCATION ("warmelt-debug.melt:930:/ initchunk");
  /*^putroutconst */
  /*putroutconst */
  if (MELT_HAS_INITIAL_ENVIRONMENT)
    melt_assertmsg ("putroutconst checkrout",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.VALROUT___V244*/ meltfptr[243]))
		    == MELTOBMAG_ROUTINE);
  if (MELT_HAS_INITIAL_ENVIRONMENT)
    melt_checkmsg ("putroutconst constnull.drout_198__DBGOUT_LIST_METHOD#1",
		   NULL != ( /*_.DISCR_LIST__V31*/ meltfptr[30]));
  ((meltroutine_ptr_t) /*_.VALROUT___V244*/ meltfptr[243])->tabval[1] =
    (melt_ptr_t) ( /*_.DISCR_LIST__V31*/ meltfptr[30]);

  MELT_LOCATION ("warmelt-debug.melt:930:/ initchunk");
  /*^putroutconstnotnull */
  /*putroutconstnotnull */
  melt_assertmsg ("putroutconstnotnull checkrout",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALROUT___V244*/ meltfptr[243])) ==
		  MELTOBMAG_ROUTINE);
  melt_assertmsg ("putroutconstnotnull notnullconst",
		  NULL != /*_.VALDATA___V69*/ meltfptr[68]);
  ((meltroutine_ptr_t) /*_.VALROUT___V244*/ meltfptr[243])->tabval[2] =
    (melt_ptr_t) ( /*_.VALDATA___V69*/ meltfptr[68]);

  MELT_LOCATION ("warmelt-debug.melt:930:/ initchunk");
  /*^putroutconstnotnull */
  /*putroutconstnotnull */
  melt_assertmsg ("putroutconstnotnull checkrout",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALROUT___V244*/ meltfptr[243])) ==
		  MELTOBMAG_ROUTINE);
  melt_assertmsg ("putroutconstnotnull notnullconst",
		  NULL != /*_.VALCLO___V112*/ meltfptr[111]);
  ((meltroutine_ptr_t) /*_.VALROUT___V244*/ meltfptr[243])->tabval[3] =
    (melt_ptr_t) ( /*_.VALCLO___V112*/ meltfptr[111]);

  MELT_LOCATION ("warmelt-debug.melt:930:/ initchunk");
  /*^touch */
  /*touch:IROUTVAL_ */
  meltgc_touch ( /*_.VALROUT___V244*/ meltfptr[243]);

  /*putclosurout#23 */
  melt_assertmsg ("putclosrout#23 checkclo",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALCLO___V245*/ meltfptr[244])) ==
		  MELTOBMAG_CLOSURE);
  melt_assertmsg ("putclosrout#23 checkrout",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALROUT___V244*/ meltfptr[243])) ==
		  MELTOBMAG_ROUTINE);
  ((meltclosure_ptr_t) /*_.VALCLO___V245*/ meltfptr[244])->rout =
    (meltroutine_ptr_t) ( /*_.VALROUT___V244*/ meltfptr[243]);

  /*touch:DATACLOSURE_ */
  meltgc_touch ( /*_.VALCLO___V245*/ meltfptr[244]);

  /*putslot */
  melt_assertmsg ("putslot checkobj",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALDATA___V246*/ meltfptr[245])) ==
		  MELTOBMAG_OBJECT);

  melt_assertmsg ("putslot checkoff",
		  (1 >= 0
		   && 1 <
		   melt_object_length ((melt_ptr_t)
				       ( /*_.VALDATA___V246*/
					meltfptr[245]))));
  ((meltobject_ptr_t) ( /*_.VALDATA___V246*/ meltfptr[245]))->obj_vartab[1] =
    (melt_ptr_t) (
  /*_.VALSTR___V247*/ meltfptr[246]);

  /*touch:VALUEDATA_ */
  meltgc_touch ( /*_.VALDATA___V246*/ meltfptr[245]);


  melt_dbgtrace_written_object ( /*_.VALDATA___V246*/ meltfptr[245],
				"new static instance");

  MELT_LOCATION ("warmelt-debug.melt:973:/ initchunk");
  /*^putroutconstnotnull */
  /*putroutconstnotnull */
  melt_assertmsg ("putroutconstnotnull checkrout",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALROUT___V248*/ meltfptr[247])) ==
		  MELTOBMAG_ROUTINE);
  melt_assertmsg ("putroutconstnotnull notnullconst",
		  NULL != /*_.VALDATA___V51*/ meltfptr[50]);
  ((meltroutine_ptr_t) /*_.VALROUT___V248*/ meltfptr[247])->tabval[0] =
    (melt_ptr_t) ( /*_.VALDATA___V51*/ meltfptr[50]);

  MELT_LOCATION ("warmelt-debug.melt:973:/ initchunk");
  /*^putroutconst */
  /*putroutconst */
  if (MELT_HAS_INITIAL_ENVIRONMENT)
    melt_assertmsg ("putroutconst checkrout",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.VALROUT___V248*/ meltfptr[247]))
		    == MELTOBMAG_ROUTINE);
  if (MELT_HAS_INITIAL_ENVIRONMENT)
    melt_checkmsg ("putroutconst constnull.drout_202__DBGOUT_PAIR_METHOD#1",
		   NULL != ( /*_.DISCR_PAIR__V32*/ meltfptr[31]));
  ((meltroutine_ptr_t) /*_.VALROUT___V248*/ meltfptr[247])->tabval[1] =
    (melt_ptr_t) ( /*_.DISCR_PAIR__V32*/ meltfptr[31]);

  MELT_LOCATION ("warmelt-debug.melt:973:/ initchunk");
  /*^putroutconstnotnull */
  /*putroutconstnotnull */
  melt_assertmsg ("putroutconstnotnull checkrout",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALROUT___V248*/ meltfptr[247])) ==
		  MELTOBMAG_ROUTINE);
  melt_assertmsg ("putroutconstnotnull notnullconst",
		  NULL != /*_.VALDATA___V69*/ meltfptr[68]);
  ((meltroutine_ptr_t) /*_.VALROUT___V248*/ meltfptr[247])->tabval[2] =
    (melt_ptr_t) ( /*_.VALDATA___V69*/ meltfptr[68]);

  MELT_LOCATION ("warmelt-debug.melt:973:/ initchunk");
  /*^putroutconstnotnull */
  /*putroutconstnotnull */
  melt_assertmsg ("putroutconstnotnull checkrout",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALROUT___V248*/ meltfptr[247])) ==
		  MELTOBMAG_ROUTINE);
  melt_assertmsg ("putroutconstnotnull notnullconst",
		  NULL != /*_.VALCLO___V112*/ meltfptr[111]);
  ((meltroutine_ptr_t) /*_.VALROUT___V248*/ meltfptr[247])->tabval[3] =
    (melt_ptr_t) ( /*_.VALCLO___V112*/ meltfptr[111]);

  MELT_LOCATION ("warmelt-debug.melt:973:/ initchunk");
  /*^touch */
  /*touch:IROUTVAL_ */
  meltgc_touch ( /*_.VALROUT___V248*/ meltfptr[247]);

  /*putclosurout#24 */
  melt_assertmsg ("putclosrout#24 checkclo",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALCLO___V249*/ meltfptr[248])) ==
		  MELTOBMAG_CLOSURE);
  melt_assertmsg ("putclosrout#24 checkrout",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALROUT___V248*/ meltfptr[247])) ==
		  MELTOBMAG_ROUTINE);
  ((meltclosure_ptr_t) /*_.VALCLO___V249*/ meltfptr[248])->rout =
    (meltroutine_ptr_t) ( /*_.VALROUT___V248*/ meltfptr[247]);

  /*touch:DATACLOSURE_ */
  meltgc_touch ( /*_.VALCLO___V249*/ meltfptr[248]);

  /*putslot */
  melt_assertmsg ("putslot checkobj",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALDATA___V250*/ meltfptr[249])) ==
		  MELTOBMAG_OBJECT);

  melt_assertmsg ("putslot checkoff",
		  (1 >= 0
		   && 1 <
		   melt_object_length ((melt_ptr_t)
				       ( /*_.VALDATA___V250*/
					meltfptr[249]))));
  ((meltobject_ptr_t) ( /*_.VALDATA___V250*/ meltfptr[249]))->obj_vartab[1] =
    (melt_ptr_t) (
  /*_.VALSTR___V251*/ meltfptr[250]);

  /*touch:VALUEDATA_ */
  meltgc_touch ( /*_.VALDATA___V250*/ meltfptr[249]);


  melt_dbgtrace_written_object ( /*_.VALDATA___V250*/ meltfptr[249],
				"new static instance");

  MELT_LOCATION ("warmelt-debug.melt:998:/ initchunk");
  /*^putroutconstnotnull */
  /*putroutconstnotnull */
  melt_assertmsg ("putroutconstnotnull checkrout",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALROUT___V252*/ meltfptr[251])) ==
		  MELTOBMAG_ROUTINE);
  melt_assertmsg ("putroutconstnotnull notnullconst",
		  NULL != /*_.VALDATA___V51*/ meltfptr[50]);
  ((meltroutine_ptr_t) /*_.VALROUT___V252*/ meltfptr[251])->tabval[0] =
    (melt_ptr_t) ( /*_.VALDATA___V51*/ meltfptr[50]);

  MELT_LOCATION ("warmelt-debug.melt:998:/ initchunk");
  /*^putroutconst */
  /*putroutconst */
  if (MELT_HAS_INITIAL_ENVIRONMENT)
    melt_assertmsg ("putroutconst checkrout",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.VALROUT___V252*/ meltfptr[251]))
		    == MELTOBMAG_ROUTINE);
  if (MELT_HAS_INITIAL_ENVIRONMENT)
    melt_checkmsg
      ("putroutconst constnull.drout_206__DBGOUT_MAPOBJECT_METHOD#1",
       NULL != ( /*_.DISCR_MAP_OBJECTS__V10*/ meltfptr[9]));
  ((meltroutine_ptr_t) /*_.VALROUT___V252*/ meltfptr[251])->tabval[1] =
    (melt_ptr_t) ( /*_.DISCR_MAP_OBJECTS__V10*/ meltfptr[9]);

  MELT_LOCATION ("warmelt-debug.melt:998:/ initchunk");
  /*^putroutconstnotnull */
  /*putroutconstnotnull */
  melt_assertmsg ("putroutconstnotnull checkrout",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALROUT___V252*/ meltfptr[251])) ==
		  MELTOBMAG_ROUTINE);
  melt_assertmsg ("putroutconstnotnull notnullconst",
		  NULL != /*_.VALDATA___V69*/ meltfptr[68]);
  ((meltroutine_ptr_t) /*_.VALROUT___V252*/ meltfptr[251])->tabval[2] =
    (melt_ptr_t) ( /*_.VALDATA___V69*/ meltfptr[68]);

  MELT_LOCATION ("warmelt-debug.melt:998:/ initchunk");
  /*^putroutconst */
  /*putroutconst */
  if (MELT_HAS_INITIAL_ENVIRONMENT)
    melt_assertmsg ("putroutconst checkrout",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.VALROUT___V252*/ meltfptr[251]))
		    == MELTOBMAG_ROUTINE);
  if (MELT_HAS_INITIAL_ENVIRONMENT)
    melt_checkmsg
      ("putroutconst constnull.drout_206__DBGOUT_MAPOBJECT_METHOD#3",
       NULL != ( /*_.DISCR_INTEGER__V11*/ meltfptr[10]));
  ((meltroutine_ptr_t) /*_.VALROUT___V252*/ meltfptr[251])->tabval[3] =
    (melt_ptr_t) ( /*_.DISCR_INTEGER__V11*/ meltfptr[10]);

  MELT_LOCATION ("warmelt-debug.melt:998:/ initchunk");
  /*^putroutconst */
  /*putroutconst */
  if (MELT_HAS_INITIAL_ENVIRONMENT)
    melt_assertmsg ("putroutconst checkrout",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.VALROUT___V252*/ meltfptr[251]))
		    == MELTOBMAG_ROUTINE);
  if (MELT_HAS_INITIAL_ENVIRONMENT)
    melt_checkmsg
      ("putroutconst constnull.drout_206__DBGOUT_MAPOBJECT_METHOD#4",
       NULL != ( /*_.DISCR_MULTIPLE__V28*/ meltfptr[27]));
  ((meltroutine_ptr_t) /*_.VALROUT___V252*/ meltfptr[251])->tabval[4] =
    (melt_ptr_t) ( /*_.DISCR_MULTIPLE__V28*/ meltfptr[27]);

  MELT_LOCATION ("warmelt-debug.melt:998:/ initchunk");
  /*^putroutconst */
  /*putroutconst */
  if (MELT_HAS_INITIAL_ENVIRONMENT)
    melt_assertmsg ("putroutconst checkrout",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.VALROUT___V252*/ meltfptr[251]))
		    == MELTOBMAG_ROUTINE);
  if (MELT_HAS_INITIAL_ENVIRONMENT)
    melt_checkmsg
      ("putroutconst constnull.drout_206__DBGOUT_MAPOBJECT_METHOD#5",
       NULL != ( /*_.ADD2OUT__V17*/ meltfptr[16]));
  ((meltroutine_ptr_t) /*_.VALROUT___V252*/ meltfptr[251])->tabval[5] =
    (melt_ptr_t) ( /*_.ADD2OUT__V17*/ meltfptr[16]);

  MELT_LOCATION ("warmelt-debug.melt:998:/ initchunk");
  /*^putroutconstnotnull */
  /*putroutconstnotnull */
  melt_assertmsg ("putroutconstnotnull checkrout",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALROUT___V252*/ meltfptr[251])) ==
		  MELTOBMAG_ROUTINE);
  melt_assertmsg ("putroutconstnotnull notnullconst",
		  NULL != /*_.VALCLO___V112*/ meltfptr[111]);
  ((meltroutine_ptr_t) /*_.VALROUT___V252*/ meltfptr[251])->tabval[6] =
    (melt_ptr_t) ( /*_.VALCLO___V112*/ meltfptr[111]);

  MELT_LOCATION ("warmelt-debug.melt:998:/ initchunk");
  /*^putroutconst */
  /*putroutconst */
  if (MELT_HAS_INITIAL_ENVIRONMENT)
    melt_assertmsg ("putroutconst checkrout",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.VALROUT___V252*/ meltfptr[251]))
		    == MELTOBMAG_ROUTINE);
  if (MELT_HAS_INITIAL_ENVIRONMENT)
    melt_checkmsg
      ("putroutconst constnull.drout_206__DBGOUT_MAPOBJECT_METHOD#7",
       NULL != ( /*_.MAPOBJECT_EVERY__V33*/ meltfptr[32]));
  ((meltroutine_ptr_t) /*_.VALROUT___V252*/ meltfptr[251])->tabval[7] =
    (melt_ptr_t) ( /*_.MAPOBJECT_EVERY__V33*/ meltfptr[32]);

  MELT_LOCATION ("warmelt-debug.melt:998:/ initchunk");
  /*^putroutconst */
  /*putroutconst */
  if (MELT_HAS_INITIAL_ENVIRONMENT)
    melt_assertmsg ("putroutconst checkrout",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.VALROUT___V252*/ meltfptr[251]))
		    == MELTOBMAG_ROUTINE);
  if (MELT_HAS_INITIAL_ENVIRONMENT)
    melt_checkmsg
      ("putroutconst constnull.drout_206__DBGOUT_MAPOBJECT_METHOD#8",
       NULL != ( /*_.DISCR_INTEGER__V11*/ meltfptr[10]));
  ((meltroutine_ptr_t) /*_.VALROUT___V252*/ meltfptr[251])->tabval[8] =
    (melt_ptr_t) ( /*_.DISCR_INTEGER__V11*/ meltfptr[10]);

  MELT_LOCATION ("warmelt-debug.melt:1034:/ initchunk");
  /*^putroutconst */
  /*putroutconst */
  if (MELT_HAS_INITIAL_ENVIRONMENT)
    melt_assertmsg ("putroutconst checkrout",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.VALROUT___V253*/ meltfptr[252]))
		    == MELTOBMAG_ROUTINE);
  if (MELT_HAS_INITIAL_ENVIRONMENT)
    melt_checkmsg ("putroutconst constnull.drout_210__LAMBDA_#0",
		   NULL != ( /*_.DISCR_INTEGER__V11*/ meltfptr[10]));
  ((meltroutine_ptr_t) /*_.VALROUT___V253*/ meltfptr[252])->tabval[0] =
    (melt_ptr_t) ( /*_.DISCR_INTEGER__V11*/ meltfptr[10]);

  MELT_LOCATION ("warmelt-debug.melt:1034:/ initchunk");
  /*^touch */
  /*touch:IROUTVAL_ */
  meltgc_touch ( /*_.VALROUT___V253*/ meltfptr[252]);

  MELT_LOCATION ("warmelt-debug.melt:998:/ initchunk");
  /*^putroutconst */
  /*putroutconst */
  if (MELT_HAS_INITIAL_ENVIRONMENT)
    melt_assertmsg ("putroutconst checkrout",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.VALROUT___V252*/ meltfptr[251]))
		    == MELTOBMAG_ROUTINE);
  if (MELT_HAS_INITIAL_ENVIRONMENT)
    melt_checkmsg
      ("putroutconst constnull.drout_206__DBGOUT_MAPOBJECT_METHOD#9",
       NULL != ( /*_.VALROUT___V253*/ meltfptr[252]));
  ((meltroutine_ptr_t) /*_.VALROUT___V252*/ meltfptr[251])->tabval[9] =
    (melt_ptr_t) ( /*_.VALROUT___V253*/ meltfptr[252]);

  MELT_LOCATION ("warmelt-debug.melt:998:/ initchunk");
  /*^putroutconst */
  /*putroutconst */
  if (MELT_HAS_INITIAL_ENVIRONMENT)
    melt_assertmsg ("putroutconst checkrout",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.VALROUT___V252*/ meltfptr[251]))
		    == MELTOBMAG_ROUTINE);
  if (MELT_HAS_INITIAL_ENVIRONMENT)
    melt_checkmsg
      ("putroutconst constnull.drout_206__DBGOUT_MAPOBJECT_METHOD#10",
       NULL != ( /*_.COMPARE_OBJ_RANKED__V34*/ meltfptr[33]));
  ((meltroutine_ptr_t) /*_.VALROUT___V252*/ meltfptr[251])->tabval[10] =
    (melt_ptr_t) ( /*_.COMPARE_OBJ_RANKED__V34*/ meltfptr[33]);

  MELT_LOCATION ("warmelt-debug.melt:1047:/ initchunk");
  /*^putroutconst */
  /*putroutconst */
  if (MELT_HAS_INITIAL_ENVIRONMENT)
    melt_assertmsg ("putroutconst checkrout",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.VALROUT___V254*/ meltfptr[253]))
		    == MELTOBMAG_ROUTINE);
  if (MELT_HAS_INITIAL_ENVIRONMENT)
    melt_checkmsg ("putroutconst constnull.drout_211__LAMBDA_#0",
		   NULL != ( /*_.COMPARE_OBJ_RANKED__V34*/ meltfptr[33]));
  ((meltroutine_ptr_t) /*_.VALROUT___V254*/ meltfptr[253])->tabval[0] =
    (melt_ptr_t) ( /*_.COMPARE_OBJ_RANKED__V34*/ meltfptr[33]);

  MELT_LOCATION ("warmelt-debug.melt:1047:/ initchunk");
  /*^putroutconst */
  /*putroutconst */
  if (MELT_HAS_INITIAL_ENVIRONMENT)
    melt_assertmsg ("putroutconst checkrout",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.VALROUT___V254*/ meltfptr[253]))
		    == MELTOBMAG_ROUTINE);
  if (MELT_HAS_INITIAL_ENVIRONMENT)
    melt_checkmsg ("putroutconst constnull.drout_211__LAMBDA_#1",
		   NULL != ( /*_.VALBXINT___V255*/ meltfptr[254]));
  ((meltroutine_ptr_t) /*_.VALROUT___V254*/ meltfptr[253])->tabval[1] =
    (melt_ptr_t) ( /*_.VALBXINT___V255*/ meltfptr[254]);

  MELT_LOCATION ("warmelt-debug.melt:1047:/ initchunk");
  /*^putroutconst */
  /*putroutconst */
  if (MELT_HAS_INITIAL_ENVIRONMENT)
    melt_assertmsg ("putroutconst checkrout",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.VALROUT___V254*/ meltfptr[253]))
		    == MELTOBMAG_ROUTINE);
  if (MELT_HAS_INITIAL_ENVIRONMENT)
    melt_checkmsg ("putroutconst constnull.drout_211__LAMBDA_#2",
		   NULL != ( /*_.VALBXINT___V256*/ meltfptr[255]));
  ((meltroutine_ptr_t) /*_.VALROUT___V254*/ meltfptr[253])->tabval[2] =
    (melt_ptr_t) ( /*_.VALBXINT___V256*/ meltfptr[255]);

  MELT_LOCATION ("warmelt-debug.melt:1047:/ initchunk");
  /*^putroutconst */
  /*putroutconst */
  if (MELT_HAS_INITIAL_ENVIRONMENT)
    melt_assertmsg ("putroutconst checkrout",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.VALROUT___V254*/ meltfptr[253]))
		    == MELTOBMAG_ROUTINE);
  if (MELT_HAS_INITIAL_ENVIRONMENT)
    melt_checkmsg ("putroutconst constnull.drout_211__LAMBDA_#3",
		   NULL != ( /*_.VALBXINT___V257*/ meltfptr[256]));
  ((meltroutine_ptr_t) /*_.VALROUT___V254*/ meltfptr[253])->tabval[3] =
    (melt_ptr_t) ( /*_.VALBXINT___V257*/ meltfptr[256]);

  MELT_LOCATION ("warmelt-debug.melt:1047:/ initchunk");
  /*^touch */
  /*touch:IROUTVAL_ */
  meltgc_touch ( /*_.VALROUT___V254*/ meltfptr[253]);

  MELT_LOCATION ("warmelt-debug.melt:998:/ initchunk");
  /*^putroutconst */
  /*putroutconst */
  if (MELT_HAS_INITIAL_ENVIRONMENT)
    melt_assertmsg ("putroutconst checkrout",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.VALROUT___V252*/ meltfptr[251]))
		    == MELTOBMAG_ROUTINE);
  if (MELT_HAS_INITIAL_ENVIRONMENT)
    melt_checkmsg
      ("putroutconst constnull.drout_206__DBGOUT_MAPOBJECT_METHOD#11",
       NULL != ( /*_.VALROUT___V254*/ meltfptr[253]));
  ((meltroutine_ptr_t) /*_.VALROUT___V252*/ meltfptr[251])->tabval[11] =
    (melt_ptr_t) ( /*_.VALROUT___V254*/ meltfptr[253]);

  MELT_LOCATION ("warmelt-debug.melt:998:/ initchunk");
  /*^putroutconst */
  /*putroutconst */
  if (MELT_HAS_INITIAL_ENVIRONMENT)
    melt_assertmsg ("putroutconst checkrout",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.VALROUT___V252*/ meltfptr[251]))
		    == MELTOBMAG_ROUTINE);
  if (MELT_HAS_INITIAL_ENVIRONMENT)
    melt_checkmsg
      ("putroutconst constnull.drout_206__DBGOUT_MAPOBJECT_METHOD#12",
       NULL != ( /*_.MULTIPLE_EVERY__V35*/ meltfptr[34]));
  ((meltroutine_ptr_t) /*_.VALROUT___V252*/ meltfptr[251])->tabval[12] =
    (melt_ptr_t) ( /*_.MULTIPLE_EVERY__V35*/ meltfptr[34]);

  MELT_LOCATION ("warmelt-debug.melt:998:/ initchunk");
  /*^putroutconstnotnull */
  /*putroutconstnotnull */
  melt_assertmsg ("putroutconstnotnull checkrout",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALROUT___V252*/ meltfptr[251])) ==
		  MELTOBMAG_ROUTINE);
  melt_assertmsg ("putroutconstnotnull notnullconst",
		  NULL != /*_.VALCLO___V114*/ meltfptr[113]);
  ((meltroutine_ptr_t) /*_.VALROUT___V252*/ meltfptr[251])->tabval[13] =
    (melt_ptr_t) ( /*_.VALCLO___V114*/ meltfptr[113]);

  MELT_LOCATION ("warmelt-debug.melt:998:/ initchunk");
  /*^putroutconstnotnull */
  /*putroutconstnotnull */
  melt_assertmsg ("putroutconstnotnull checkrout",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALROUT___V252*/ meltfptr[251])) ==
		  MELTOBMAG_ROUTINE);
  melt_assertmsg ("putroutconstnotnull notnullconst",
		  NULL != /*_.VALCLO___V112*/ meltfptr[111]);
  ((meltroutine_ptr_t) /*_.VALROUT___V252*/ meltfptr[251])->tabval[14] =
    (melt_ptr_t) ( /*_.VALCLO___V112*/ meltfptr[111]);

  MELT_LOCATION ("warmelt-debug.melt:1063:/ initchunk");
  /*^putroutconstnotnull */
  /*putroutconstnotnull */
  melt_assertmsg ("putroutconstnotnull checkrout",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALROUT___V258*/ meltfptr[257])) ==
		  MELTOBMAG_ROUTINE);
  melt_assertmsg ("putroutconstnotnull notnullconst",
		  NULL != /*_.VALCLO___V114*/ meltfptr[113]);
  ((meltroutine_ptr_t) /*_.VALROUT___V258*/ meltfptr[257])->tabval[0] =
    (melt_ptr_t) ( /*_.VALCLO___V114*/ meltfptr[113]);

  MELT_LOCATION ("warmelt-debug.melt:1063:/ initchunk");
  /*^putroutconstnotnull */
  /*putroutconstnotnull */
  melt_assertmsg ("putroutconstnotnull checkrout",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALROUT___V258*/ meltfptr[257])) ==
		  MELTOBMAG_ROUTINE);
  melt_assertmsg ("putroutconstnotnull notnullconst",
		  NULL != /*_.VALCLO___V112*/ meltfptr[111]);
  ((meltroutine_ptr_t) /*_.VALROUT___V258*/ meltfptr[257])->tabval[1] =
    (melt_ptr_t) ( /*_.VALCLO___V112*/ meltfptr[111]);

  MELT_LOCATION ("warmelt-debug.melt:1063:/ initchunk");
  /*^touch */
  /*touch:IROUTVAL_ */
  meltgc_touch ( /*_.VALROUT___V258*/ meltfptr[257]);

  MELT_LOCATION ("warmelt-debug.melt:998:/ initchunk");
  /*^putroutconst */
  /*putroutconst */
  if (MELT_HAS_INITIAL_ENVIRONMENT)
    melt_assertmsg ("putroutconst checkrout",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.VALROUT___V252*/ meltfptr[251]))
		    == MELTOBMAG_ROUTINE);
  if (MELT_HAS_INITIAL_ENVIRONMENT)
    melt_checkmsg
      ("putroutconst constnull.drout_206__DBGOUT_MAPOBJECT_METHOD#15",
       NULL != ( /*_.VALROUT___V258*/ meltfptr[257]));
  ((meltroutine_ptr_t) /*_.VALROUT___V252*/ meltfptr[251])->tabval[15] =
    (melt_ptr_t) ( /*_.VALROUT___V258*/ meltfptr[257]);

  MELT_LOCATION ("warmelt-debug.melt:998:/ initchunk");
  /*^touch */
  /*touch:IROUTVAL_ */
  meltgc_touch ( /*_.VALROUT___V252*/ meltfptr[251]);

  /*putclosurout#25 */
  melt_assertmsg ("putclosrout#25 checkclo",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALCLO___V259*/ meltfptr[258])) ==
		  MELTOBMAG_CLOSURE);
  melt_assertmsg ("putclosrout#25 checkrout",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALROUT___V252*/ meltfptr[251])) ==
		  MELTOBMAG_ROUTINE);
  ((meltclosure_ptr_t) /*_.VALCLO___V259*/ meltfptr[258])->rout =
    (meltroutine_ptr_t) ( /*_.VALROUT___V252*/ meltfptr[251]);

  /*touch:DATACLOSURE_ */
  meltgc_touch ( /*_.VALCLO___V259*/ meltfptr[258]);

  /*putslot */
  melt_assertmsg ("putslot checkobj",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALDATA___V260*/ meltfptr[259])) ==
		  MELTOBMAG_OBJECT);

  melt_assertmsg ("putslot checkoff",
		  (1 >= 0
		   && 1 <
		   melt_object_length ((melt_ptr_t)
				       ( /*_.VALDATA___V260*/
					meltfptr[259]))));
  ((meltobject_ptr_t) ( /*_.VALDATA___V260*/ meltfptr[259]))->obj_vartab[1] =
    (melt_ptr_t) (
  /*_.VALSTR___V261*/ meltfptr[260]);

  /*touch:VALUEDATA_ */
  meltgc_touch ( /*_.VALDATA___V260*/ meltfptr[259]);


  melt_dbgtrace_written_object ( /*_.VALDATA___V260*/ meltfptr[259],
				"new static instance");

  /*putslot */
  melt_assertmsg ("putslot checkobj",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALDATA___V262*/ meltfptr[261])) ==
		  MELTOBMAG_OBJECT);

  melt_assertmsg ("putslot checkoff",
		  (1 >= 0
		   && 1 <
		   melt_object_length ((melt_ptr_t)
				       ( /*_.VALDATA___V262*/
					meltfptr[261]))));
  ((meltobject_ptr_t) ( /*_.VALDATA___V262*/ meltfptr[261]))->obj_vartab[1] =
    (melt_ptr_t) (
  /*_.VALSTR___V263*/ meltfptr[262]);

  /*touch:VALUEDATA_ */
  meltgc_touch ( /*_.VALDATA___V262*/ meltfptr[261]);


  melt_dbgtrace_written_object ( /*_.VALDATA___V262*/ meltfptr[261],
				"new static instance");

  /*putslot */
  melt_assertmsg ("putslot checkobj",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALDATA___V264*/ meltfptr[263])) ==
		  MELTOBMAG_OBJECT);

  melt_assertmsg ("putslot checkoff",
		  (1 >= 0
		   && 1 <
		   melt_object_length ((melt_ptr_t)
				       ( /*_.VALDATA___V264*/
					meltfptr[263]))));
  ((meltobject_ptr_t) ( /*_.VALDATA___V264*/ meltfptr[263]))->obj_vartab[1] =
    (melt_ptr_t) (
  /*_.VALSTR___V265*/ meltfptr[264]);

  /*touch:VALUEDATA_ */
  meltgc_touch ( /*_.VALDATA___V264*/ meltfptr[263]);


  melt_dbgtrace_written_object ( /*_.VALDATA___V264*/ meltfptr[263],
				"new static instance");

  MELT_LOCATION ("warmelt-debug.melt:1086:/ initchunk");
  /*^putroutconstnotnull */
  /*putroutconstnotnull */
  melt_assertmsg ("putroutconstnotnull checkrout",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALROUT___V266*/ meltfptr[265])) ==
		  MELTOBMAG_ROUTINE);
  melt_assertmsg ("putroutconstnotnull notnullconst",
		  NULL != /*_.VALDATA___V51*/ meltfptr[50]);
  ((meltroutine_ptr_t) /*_.VALROUT___V266*/ meltfptr[265])->tabval[0] =
    (melt_ptr_t) ( /*_.VALDATA___V51*/ meltfptr[50]);

  MELT_LOCATION ("warmelt-debug.melt:1086:/ initchunk");
  /*^putroutconst */
  /*putroutconst */
  if (MELT_HAS_INITIAL_ENVIRONMENT)
    melt_assertmsg ("putroutconst checkrout",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.VALROUT___V266*/ meltfptr[265]))
		    == MELTOBMAG_ROUTINE);
  if (MELT_HAS_INITIAL_ENVIRONMENT)
    melt_checkmsg
      ("putroutconst constnull.drout_220__DBGOUT_MAPSTRING_METHOD#1",
       NULL != ( /*_.DISCR_INTEGER__V11*/ meltfptr[10]));
  ((meltroutine_ptr_t) /*_.VALROUT___V266*/ meltfptr[265])->tabval[1] =
    (melt_ptr_t) ( /*_.DISCR_INTEGER__V11*/ meltfptr[10]);

  MELT_LOCATION ("warmelt-debug.melt:1086:/ initchunk");
  /*^putroutconst */
  /*putroutconst */
  if (MELT_HAS_INITIAL_ENVIRONMENT)
    melt_assertmsg ("putroutconst checkrout",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.VALROUT___V266*/ meltfptr[265]))
		    == MELTOBMAG_ROUTINE);
  if (MELT_HAS_INITIAL_ENVIRONMENT)
    melt_checkmsg
      ("putroutconst constnull.drout_220__DBGOUT_MAPSTRING_METHOD#2",
       NULL != ( /*_.DISCR_MULTIPLE__V28*/ meltfptr[27]));
  ((meltroutine_ptr_t) /*_.VALROUT___V266*/ meltfptr[265])->tabval[2] =
    (melt_ptr_t) ( /*_.DISCR_MULTIPLE__V28*/ meltfptr[27]);

  MELT_LOCATION ("warmelt-debug.melt:1086:/ initchunk");
  /*^putroutconst */
  /*putroutconst */
  if (MELT_HAS_INITIAL_ENVIRONMENT)
    melt_assertmsg ("putroutconst checkrout",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.VALROUT___V266*/ meltfptr[265]))
		    == MELTOBMAG_ROUTINE);
  if (MELT_HAS_INITIAL_ENVIRONMENT)
    melt_checkmsg
      ("putroutconst constnull.drout_220__DBGOUT_MAPSTRING_METHOD#3",
       NULL != ( /*_.DISCR_MAP_STRINGS__V36*/ meltfptr[35]));
  ((meltroutine_ptr_t) /*_.VALROUT___V266*/ meltfptr[265])->tabval[3] =
    (melt_ptr_t) ( /*_.DISCR_MAP_STRINGS__V36*/ meltfptr[35]);

  MELT_LOCATION ("warmelt-debug.melt:1086:/ initchunk");
  /*^putroutconstnotnull */
  /*putroutconstnotnull */
  melt_assertmsg ("putroutconstnotnull checkrout",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALROUT___V266*/ meltfptr[265])) ==
		  MELTOBMAG_ROUTINE);
  melt_assertmsg ("putroutconstnotnull notnullconst",
		  NULL != /*_.VALDATA___V69*/ meltfptr[68]);
  ((meltroutine_ptr_t) /*_.VALROUT___V266*/ meltfptr[265])->tabval[4] =
    (melt_ptr_t) ( /*_.VALDATA___V69*/ meltfptr[68]);

  MELT_LOCATION ("warmelt-debug.melt:1086:/ initchunk");
  /*^putroutconst */
  /*putroutconst */
  if (MELT_HAS_INITIAL_ENVIRONMENT)
    melt_assertmsg ("putroutconst checkrout",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.VALROUT___V266*/ meltfptr[265]))
		    == MELTOBMAG_ROUTINE);
  if (MELT_HAS_INITIAL_ENVIRONMENT)
    melt_checkmsg
      ("putroutconst constnull.drout_220__DBGOUT_MAPSTRING_METHOD#5",
       NULL != ( /*_.ADD2OUT__V17*/ meltfptr[16]));
  ((meltroutine_ptr_t) /*_.VALROUT___V266*/ meltfptr[265])->tabval[5] =
    (melt_ptr_t) ( /*_.ADD2OUT__V17*/ meltfptr[16]);

  MELT_LOCATION ("warmelt-debug.melt:1086:/ initchunk");
  /*^putroutconstnotnull */
  /*putroutconstnotnull */
  melt_assertmsg ("putroutconstnotnull checkrout",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALROUT___V266*/ meltfptr[265])) ==
		  MELTOBMAG_ROUTINE);
  melt_assertmsg ("putroutconstnotnull notnullconst",
		  NULL != /*_.VALCLO___V112*/ meltfptr[111]);
  ((meltroutine_ptr_t) /*_.VALROUT___V266*/ meltfptr[265])->tabval[6] =
    (melt_ptr_t) ( /*_.VALCLO___V112*/ meltfptr[111]);

  MELT_LOCATION ("warmelt-debug.melt:1086:/ initchunk");
  /*^putroutconst */
  /*putroutconst */
  if (MELT_HAS_INITIAL_ENVIRONMENT)
    melt_assertmsg ("putroutconst checkrout",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.VALROUT___V266*/ meltfptr[265]))
		    == MELTOBMAG_ROUTINE);
  if (MELT_HAS_INITIAL_ENVIRONMENT)
    melt_checkmsg
      ("putroutconst constnull.drout_220__DBGOUT_MAPSTRING_METHOD#7",
       NULL != ( /*_.MAPSTRING_EVERY__V37*/ meltfptr[36]));
  ((meltroutine_ptr_t) /*_.VALROUT___V266*/ meltfptr[265])->tabval[7] =
    (melt_ptr_t) ( /*_.MAPSTRING_EVERY__V37*/ meltfptr[36]);

  MELT_LOCATION ("warmelt-debug.melt:1086:/ initchunk");
  /*^putroutconst */
  /*putroutconst */
  if (MELT_HAS_INITIAL_ENVIRONMENT)
    melt_assertmsg ("putroutconst checkrout",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.VALROUT___V266*/ meltfptr[265]))
		    == MELTOBMAG_ROUTINE);
  if (MELT_HAS_INITIAL_ENVIRONMENT)
    melt_checkmsg
      ("putroutconst constnull.drout_220__DBGOUT_MAPSTRING_METHOD#8",
       NULL != ( /*_.DISCR_INTEGER__V11*/ meltfptr[10]));
  ((meltroutine_ptr_t) /*_.VALROUT___V266*/ meltfptr[265])->tabval[8] =
    (melt_ptr_t) ( /*_.DISCR_INTEGER__V11*/ meltfptr[10]);

  MELT_LOCATION ("warmelt-debug.melt:1122:/ initchunk");
  /*^putroutconst */
  /*putroutconst */
  if (MELT_HAS_INITIAL_ENVIRONMENT)
    melt_assertmsg ("putroutconst checkrout",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.VALROUT___V267*/ meltfptr[266]))
		    == MELTOBMAG_ROUTINE);
  if (MELT_HAS_INITIAL_ENVIRONMENT)
    melt_checkmsg ("putroutconst constnull.drout_226__LAMBDA_#0",
		   NULL != ( /*_.DISCR_INTEGER__V11*/ meltfptr[10]));
  ((meltroutine_ptr_t) /*_.VALROUT___V267*/ meltfptr[266])->tabval[0] =
    (melt_ptr_t) ( /*_.DISCR_INTEGER__V11*/ meltfptr[10]);

  MELT_LOCATION ("warmelt-debug.melt:1122:/ initchunk");
  /*^touch */
  /*touch:IROUTVAL_ */
  meltgc_touch ( /*_.VALROUT___V267*/ meltfptr[266]);

  MELT_LOCATION ("warmelt-debug.melt:1086:/ initchunk");
  /*^putroutconst */
  /*putroutconst */
  if (MELT_HAS_INITIAL_ENVIRONMENT)
    melt_assertmsg ("putroutconst checkrout",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.VALROUT___V266*/ meltfptr[265]))
		    == MELTOBMAG_ROUTINE);
  if (MELT_HAS_INITIAL_ENVIRONMENT)
    melt_checkmsg
      ("putroutconst constnull.drout_220__DBGOUT_MAPSTRING_METHOD#9",
       NULL != ( /*_.VALROUT___V267*/ meltfptr[266]));
  ((meltroutine_ptr_t) /*_.VALROUT___V266*/ meltfptr[265])->tabval[9] =
    (melt_ptr_t) ( /*_.VALROUT___V267*/ meltfptr[266]);

  MELT_LOCATION ("warmelt-debug.melt:1086:/ initchunk");
  /*^putroutconst */
  /*putroutconst */
  if (MELT_HAS_INITIAL_ENVIRONMENT)
    melt_assertmsg ("putroutconst checkrout",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.VALROUT___V266*/ meltfptr[265]))
		    == MELTOBMAG_ROUTINE);
  if (MELT_HAS_INITIAL_ENVIRONMENT)
    melt_checkmsg
      ("putroutconst constnull.drout_220__DBGOUT_MAPSTRING_METHOD#10",
       NULL != ( /*_.COMPARE_OBJ_RANKED__V34*/ meltfptr[33]));
  ((meltroutine_ptr_t) /*_.VALROUT___V266*/ meltfptr[265])->tabval[10] =
    (melt_ptr_t) ( /*_.COMPARE_OBJ_RANKED__V34*/ meltfptr[33]);

  MELT_LOCATION ("warmelt-debug.melt:1134:/ initchunk");
  /*^putroutconst */
  /*putroutconst */
  if (MELT_HAS_INITIAL_ENVIRONMENT)
    melt_assertmsg ("putroutconst checkrout",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.VALROUT___V268*/ meltfptr[267]))
		    == MELTOBMAG_ROUTINE);
  if (MELT_HAS_INITIAL_ENVIRONMENT)
    melt_checkmsg ("putroutconst constnull.drout_227__LAMBDA_#0",
		   NULL != ( /*_.COMPARE_OBJ_RANKED__V34*/ meltfptr[33]));
  ((meltroutine_ptr_t) /*_.VALROUT___V268*/ meltfptr[267])->tabval[0] =
    (melt_ptr_t) ( /*_.COMPARE_OBJ_RANKED__V34*/ meltfptr[33]);

  MELT_LOCATION ("warmelt-debug.melt:1134:/ initchunk");
  /*^putroutconst */
  /*putroutconst */
  if (MELT_HAS_INITIAL_ENVIRONMENT)
    melt_assertmsg ("putroutconst checkrout",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.VALROUT___V268*/ meltfptr[267]))
		    == MELTOBMAG_ROUTINE);
  if (MELT_HAS_INITIAL_ENVIRONMENT)
    melt_checkmsg ("putroutconst constnull.drout_227__LAMBDA_#1",
		   NULL != ( /*_.VALBXINT___V269*/ meltfptr[268]));
  ((meltroutine_ptr_t) /*_.VALROUT___V268*/ meltfptr[267])->tabval[1] =
    (melt_ptr_t) ( /*_.VALBXINT___V269*/ meltfptr[268]);

  MELT_LOCATION ("warmelt-debug.melt:1134:/ initchunk");
  /*^putroutconst */
  /*putroutconst */
  if (MELT_HAS_INITIAL_ENVIRONMENT)
    melt_assertmsg ("putroutconst checkrout",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.VALROUT___V268*/ meltfptr[267]))
		    == MELTOBMAG_ROUTINE);
  if (MELT_HAS_INITIAL_ENVIRONMENT)
    melt_checkmsg ("putroutconst constnull.drout_227__LAMBDA_#2",
		   NULL != ( /*_.VALBXINT___V270*/ meltfptr[269]));
  ((meltroutine_ptr_t) /*_.VALROUT___V268*/ meltfptr[267])->tabval[2] =
    (melt_ptr_t) ( /*_.VALBXINT___V270*/ meltfptr[269]);


#undef meltfram__
}				/*end of meltmod__warmelt_debug__initialmeltchunk_3 */

void
meltmod__warmelt_debug__initialmeltchunk_4 (struct
					    frame_melt_start_this_module_st
					    *meltmeltframptr__,
					    char meltpredefinited[])
{
#define meltfram__ (*meltmeltframptr__)
#undef meltcallcount
#define meltcallcount 0L
  (void) meltpredefinited;
  MELT_LOCATION ("warmelt-debug.melt:1134:/ initchunk");
  /*^putroutconst */
  /*putroutconst */
  if (MELT_HAS_INITIAL_ENVIRONMENT)
    melt_assertmsg ("putroutconst checkrout",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.VALROUT___V268*/ meltfptr[267]))
		    == MELTOBMAG_ROUTINE);
  if (MELT_HAS_INITIAL_ENVIRONMENT)
    melt_checkmsg ("putroutconst constnull.drout_227__LAMBDA_#3",
		   NULL != ( /*_.VALBXINT___V271*/ meltfptr[270]));
  ((meltroutine_ptr_t) /*_.VALROUT___V268*/ meltfptr[267])->tabval[3] =
    (melt_ptr_t) ( /*_.VALBXINT___V271*/ meltfptr[270]);

  MELT_LOCATION ("warmelt-debug.melt:1134:/ initchunk");
  /*^touch */
  /*touch:IROUTVAL_ */
  meltgc_touch ( /*_.VALROUT___V268*/ meltfptr[267]);

  MELT_LOCATION ("warmelt-debug.melt:1086:/ initchunk");
  /*^putroutconst */
  /*putroutconst */
  if (MELT_HAS_INITIAL_ENVIRONMENT)
    melt_assertmsg ("putroutconst checkrout",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.VALROUT___V266*/ meltfptr[265]))
		    == MELTOBMAG_ROUTINE);
  if (MELT_HAS_INITIAL_ENVIRONMENT)
    melt_checkmsg
      ("putroutconst constnull.drout_220__DBGOUT_MAPSTRING_METHOD#11",
       NULL != ( /*_.VALROUT___V268*/ meltfptr[267]));
  ((meltroutine_ptr_t) /*_.VALROUT___V266*/ meltfptr[265])->tabval[11] =
    (melt_ptr_t) ( /*_.VALROUT___V268*/ meltfptr[267]);

  MELT_LOCATION ("warmelt-debug.melt:1086:/ initchunk");
  /*^putroutconst */
  /*putroutconst */
  if (MELT_HAS_INITIAL_ENVIRONMENT)
    melt_assertmsg ("putroutconst checkrout",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.VALROUT___V266*/ meltfptr[265]))
		    == MELTOBMAG_ROUTINE);
  if (MELT_HAS_INITIAL_ENVIRONMENT)
    melt_checkmsg
      ("putroutconst constnull.drout_220__DBGOUT_MAPSTRING_METHOD#12",
       NULL != ( /*_.MULTIPLE_EVERY__V35*/ meltfptr[34]));
  ((meltroutine_ptr_t) /*_.VALROUT___V266*/ meltfptr[265])->tabval[12] =
    (melt_ptr_t) ( /*_.MULTIPLE_EVERY__V35*/ meltfptr[34]);

  MELT_LOCATION ("warmelt-debug.melt:1086:/ initchunk");
  /*^putroutconstnotnull */
  /*putroutconstnotnull */
  melt_assertmsg ("putroutconstnotnull checkrout",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALROUT___V266*/ meltfptr[265])) ==
		  MELTOBMAG_ROUTINE);
  melt_assertmsg ("putroutconstnotnull notnullconst",
		  NULL != /*_.VALCLO___V112*/ meltfptr[111]);
  ((meltroutine_ptr_t) /*_.VALROUT___V266*/ meltfptr[265])->tabval[13] =
    (melt_ptr_t) ( /*_.VALCLO___V112*/ meltfptr[111]);

  MELT_LOCATION ("warmelt-debug.melt:1150:/ initchunk");
  /*^putroutconstnotnull */
  /*putroutconstnotnull */
  melt_assertmsg ("putroutconstnotnull checkrout",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALROUT___V272*/ meltfptr[271])) ==
		  MELTOBMAG_ROUTINE);
  melt_assertmsg ("putroutconstnotnull notnullconst",
		  NULL != /*_.VALCLO___V112*/ meltfptr[111]);
  ((meltroutine_ptr_t) /*_.VALROUT___V272*/ meltfptr[271])->tabval[0] =
    (melt_ptr_t) ( /*_.VALCLO___V112*/ meltfptr[111]);

  MELT_LOCATION ("warmelt-debug.melt:1150:/ initchunk");
  /*^touch */
  /*touch:IROUTVAL_ */
  meltgc_touch ( /*_.VALROUT___V272*/ meltfptr[271]);

  MELT_LOCATION ("warmelt-debug.melt:1086:/ initchunk");
  /*^putroutconst */
  /*putroutconst */
  if (MELT_HAS_INITIAL_ENVIRONMENT)
    melt_assertmsg ("putroutconst checkrout",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.VALROUT___V266*/ meltfptr[265]))
		    == MELTOBMAG_ROUTINE);
  if (MELT_HAS_INITIAL_ENVIRONMENT)
    melt_checkmsg
      ("putroutconst constnull.drout_220__DBGOUT_MAPSTRING_METHOD#14",
       NULL != ( /*_.VALROUT___V272*/ meltfptr[271]));
  ((meltroutine_ptr_t) /*_.VALROUT___V266*/ meltfptr[265])->tabval[14] =
    (melt_ptr_t) ( /*_.VALROUT___V272*/ meltfptr[271]);

  MELT_LOCATION ("warmelt-debug.melt:1086:/ initchunk");
  /*^touch */
  /*touch:IROUTVAL_ */
  meltgc_touch ( /*_.VALROUT___V266*/ meltfptr[265]);

  /*putclosurout#26 */
  melt_assertmsg ("putclosrout#26 checkclo",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALCLO___V273*/ meltfptr[272])) ==
		  MELTOBMAG_CLOSURE);
  melt_assertmsg ("putclosrout#26 checkrout",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALROUT___V266*/ meltfptr[265])) ==
		  MELTOBMAG_ROUTINE);
  ((meltclosure_ptr_t) /*_.VALCLO___V273*/ meltfptr[272])->rout =
    (meltroutine_ptr_t) ( /*_.VALROUT___V266*/ meltfptr[265]);

  /*touch:DATACLOSURE_ */
  meltgc_touch ( /*_.VALCLO___V273*/ meltfptr[272]);

  /*putslot */
  melt_assertmsg ("putslot checkobj",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALDATA___V274*/ meltfptr[273])) ==
		  MELTOBMAG_OBJECT);

  melt_assertmsg ("putslot checkoff",
		  (1 >= 0
		   && 1 <
		   melt_object_length ((melt_ptr_t)
				       ( /*_.VALDATA___V274*/
					meltfptr[273]))));
  ((meltobject_ptr_t) ( /*_.VALDATA___V274*/ meltfptr[273]))->obj_vartab[1] =
    (melt_ptr_t) (
  /*_.VALSTR___V275*/ meltfptr[274]);

  /*touch:VALUEDATA_ */
  meltgc_touch ( /*_.VALDATA___V274*/ meltfptr[273]);


  melt_dbgtrace_written_object ( /*_.VALDATA___V274*/ meltfptr[273],
				"new static instance");

  /*putslot */
  melt_assertmsg ("putslot checkobj",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALDATA___V276*/ meltfptr[275])) ==
		  MELTOBMAG_OBJECT);

  melt_assertmsg ("putslot checkoff",
		  (1 >= 0
		   && 1 <
		   melt_object_length ((melt_ptr_t)
				       ( /*_.VALDATA___V276*/
					meltfptr[275]))));
  ((meltobject_ptr_t) ( /*_.VALDATA___V276*/ meltfptr[275]))->obj_vartab[1] =
    (melt_ptr_t) (
  /*_.VALSTR___V277*/ meltfptr[276]);

  /*touch:VALUEDATA_ */
  meltgc_touch ( /*_.VALDATA___V276*/ meltfptr[275]);


  melt_dbgtrace_written_object ( /*_.VALDATA___V276*/ meltfptr[275],
				"new static instance");

  MELT_LOCATION ("warmelt-debug.melt:1170:/ initchunk");
  /*^putroutconstnotnull */
  /*putroutconstnotnull */
  melt_assertmsg ("putroutconstnotnull checkrout",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALROUT___V278*/ meltfptr[277])) ==
		  MELTOBMAG_ROUTINE);
  melt_assertmsg ("putroutconstnotnull notnullconst",
		  NULL != /*_.VALDATA___V51*/ meltfptr[50]);
  ((meltroutine_ptr_t) /*_.VALROUT___V278*/ meltfptr[277])->tabval[0] =
    (melt_ptr_t) ( /*_.VALDATA___V51*/ meltfptr[50]);

  MELT_LOCATION ("warmelt-debug.melt:1170:/ initchunk");
  /*^putroutconst */
  /*putroutconst */
  if (MELT_HAS_INITIAL_ENVIRONMENT)
    melt_assertmsg ("putroutconst checkrout",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.VALROUT___V278*/ meltfptr[277]))
		    == MELTOBMAG_ROUTINE);
  if (MELT_HAS_INITIAL_ENVIRONMENT)
    melt_checkmsg
      ("putroutconst constnull.drout_232__DBGOUT_BUCKETLONG_METHOD#1",
       NULL != ( /*_.DISCR_INTEGER__V11*/ meltfptr[10]));
  ((meltroutine_ptr_t) /*_.VALROUT___V278*/ meltfptr[277])->tabval[1] =
    (melt_ptr_t) ( /*_.DISCR_INTEGER__V11*/ meltfptr[10]);

  MELT_LOCATION ("warmelt-debug.melt:1170:/ initchunk");
  /*^putroutconst */
  /*putroutconst */
  if (MELT_HAS_INITIAL_ENVIRONMENT)
    melt_assertmsg ("putroutconst checkrout",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.VALROUT___V278*/ meltfptr[277]))
		    == MELTOBMAG_ROUTINE);
  if (MELT_HAS_INITIAL_ENVIRONMENT)
    melt_checkmsg
      ("putroutconst constnull.drout_232__DBGOUT_BUCKETLONG_METHOD#2",
       NULL != ( /*_.ADD2OUT__V17*/ meltfptr[16]));
  ((meltroutine_ptr_t) /*_.VALROUT___V278*/ meltfptr[277])->tabval[2] =
    (melt_ptr_t) ( /*_.ADD2OUT__V17*/ meltfptr[16]);

  MELT_LOCATION ("warmelt-debug.melt:1170:/ initchunk");
  /*^putroutconst */
  /*putroutconst */
  if (MELT_HAS_INITIAL_ENVIRONMENT)
    melt_assertmsg ("putroutconst checkrout",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.VALROUT___V278*/ meltfptr[277]))
		    == MELTOBMAG_ROUTINE);
  if (MELT_HAS_INITIAL_ENVIRONMENT)
    melt_checkmsg
      ("putroutconst constnull.drout_232__DBGOUT_BUCKETLONG_METHOD#3",
       NULL != ( /*_.CLASS_NAMED__V19*/ meltfptr[18]));
  ((meltroutine_ptr_t) /*_.VALROUT___V278*/ meltfptr[277])->tabval[3] =
    (melt_ptr_t) ( /*_.CLASS_NAMED__V19*/ meltfptr[18]);

  MELT_LOCATION ("warmelt-debug.melt:1170:/ initchunk");
  /*^putroutconstnotnull */
  /*putroutconstnotnull */
  melt_assertmsg ("putroutconstnotnull checkrout",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALROUT___V278*/ meltfptr[277])) ==
		  MELTOBMAG_ROUTINE);
  melt_assertmsg ("putroutconstnotnull notnullconst",
		  NULL != /*_.VALCLO___V112*/ meltfptr[111]);
  ((meltroutine_ptr_t) /*_.VALROUT___V278*/ meltfptr[277])->tabval[4] =
    (melt_ptr_t) ( /*_.VALCLO___V112*/ meltfptr[111]);

  MELT_LOCATION ("warmelt-debug.melt:1170:/ initchunk");
  /*^touch */
  /*touch:IROUTVAL_ */
  meltgc_touch ( /*_.VALROUT___V278*/ meltfptr[277]);

  /*putclosurout#27 */
  melt_assertmsg ("putclosrout#27 checkclo",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALCLO___V279*/ meltfptr[278])) ==
		  MELTOBMAG_CLOSURE);
  melt_assertmsg ("putclosrout#27 checkrout",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALROUT___V278*/ meltfptr[277])) ==
		  MELTOBMAG_ROUTINE);
  ((meltclosure_ptr_t) /*_.VALCLO___V279*/ meltfptr[278])->rout =
    (meltroutine_ptr_t) ( /*_.VALROUT___V278*/ meltfptr[277]);

  /*touch:DATACLOSURE_ */
  meltgc_touch ( /*_.VALCLO___V279*/ meltfptr[278]);

  /*putslot */
  melt_assertmsg ("putslot checkobj",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALDATA___V280*/ meltfptr[279])) ==
		  MELTOBMAG_OBJECT);

  melt_assertmsg ("putslot checkoff",
		  (1 >= 0
		   && 1 <
		   melt_object_length ((melt_ptr_t)
				       ( /*_.VALDATA___V280*/
					meltfptr[279]))));
  ((meltobject_ptr_t) ( /*_.VALDATA___V280*/ meltfptr[279]))->obj_vartab[1] =
    (melt_ptr_t) (
  /*_.VALSTR___V281*/ meltfptr[280]);

  /*touch:VALUEDATA_ */
  meltgc_touch ( /*_.VALDATA___V280*/ meltfptr[279]);


  melt_dbgtrace_written_object ( /*_.VALDATA___V280*/ meltfptr[279],
				"new static instance");

  MELT_LOCATION ("warmelt-debug.melt:1207:/ initchunk");
  /*^putroutconstnotnull */
  /*putroutconstnotnull */
  melt_assertmsg ("putroutconstnotnull checkrout",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALROUT___V282*/ meltfptr[281])) ==
		  MELTOBMAG_ROUTINE);
  melt_assertmsg ("putroutconstnotnull notnullconst",
		  NULL != /*_.VALDATA___V51*/ meltfptr[50]);
  ((meltroutine_ptr_t) /*_.VALROUT___V282*/ meltfptr[281])->tabval[0] =
    (melt_ptr_t) ( /*_.VALDATA___V51*/ meltfptr[50]);

  MELT_LOCATION ("warmelt-debug.melt:1207:/ initchunk");
  /*^putroutconstnotnull */
  /*putroutconstnotnull */
  melt_assertmsg ("putroutconstnotnull checkrout",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALROUT___V282*/ meltfptr[281])) ==
		  MELTOBMAG_ROUTINE);
  melt_assertmsg ("putroutconstnotnull notnullconst",
		  NULL != /*_.VALDATA___V69*/ meltfptr[68]);
  ((meltroutine_ptr_t) /*_.VALROUT___V282*/ meltfptr[281])->tabval[1] =
    (melt_ptr_t) ( /*_.VALDATA___V69*/ meltfptr[68]);

  MELT_LOCATION ("warmelt-debug.melt:1207:/ initchunk");
  /*^putroutconstnotnull */
  /*putroutconstnotnull */
  melt_assertmsg ("putroutconstnotnull checkrout",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALROUT___V282*/ meltfptr[281])) ==
		  MELTOBMAG_ROUTINE);
  melt_assertmsg ("putroutconstnotnull notnullconst",
		  NULL != /*_.VALCLO___V200*/ meltfptr[199]);
  ((meltroutine_ptr_t) /*_.VALROUT___V282*/ meltfptr[281])->tabval[2] =
    (melt_ptr_t) ( /*_.VALCLO___V200*/ meltfptr[199]);

  MELT_LOCATION ("warmelt-debug.melt:1207:/ initchunk");
  /*^touch */
  /*touch:IROUTVAL_ */
  meltgc_touch ( /*_.VALROUT___V282*/ meltfptr[281]);

  /*putclosurout#28 */
  melt_assertmsg ("putclosrout#28 checkclo",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALCLO___V283*/ meltfptr[282])) ==
		  MELTOBMAG_CLOSURE);
  melt_assertmsg ("putclosrout#28 checkrout",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALROUT___V282*/ meltfptr[281])) ==
		  MELTOBMAG_ROUTINE);
  ((meltclosure_ptr_t) /*_.VALCLO___V283*/ meltfptr[282])->rout =
    (meltroutine_ptr_t) ( /*_.VALROUT___V282*/ meltfptr[281]);

  /*touch:DATACLOSURE_ */
  meltgc_touch ( /*_.VALCLO___V283*/ meltfptr[282]);

  MELT_LOCATION ("warmelt-debug.melt:1235:/ initchunk");
  /*^putroutconstnotnull */
  /*putroutconstnotnull */
  melt_assertmsg ("putroutconstnotnull checkrout",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALROUT___V284*/ meltfptr[283])) ==
		  MELTOBMAG_ROUTINE);
  melt_assertmsg ("putroutconstnotnull notnullconst",
		  NULL != /*_.VALDATA___V51*/ meltfptr[50]);
  ((meltroutine_ptr_t) /*_.VALROUT___V284*/ meltfptr[283])->tabval[0] =
    (melt_ptr_t) ( /*_.VALDATA___V51*/ meltfptr[50]);

  MELT_LOCATION ("warmelt-debug.melt:1235:/ initchunk");
  /*^putroutconst */
  /*putroutconst */
  if (MELT_HAS_INITIAL_ENVIRONMENT)
    melt_assertmsg ("putroutconst checkrout",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.VALROUT___V284*/ meltfptr[283]))
		    == MELTOBMAG_ROUTINE);
  if (MELT_HAS_INITIAL_ENVIRONMENT)
    melt_checkmsg
      ("putroutconst constnull.drout_238__DBGOUT_ANYRECV_METHOD#1",
       NULL != ( /*_.CLASS_NAMED__V19*/ meltfptr[18]));
  ((meltroutine_ptr_t) /*_.VALROUT___V284*/ meltfptr[283])->tabval[1] =
    (melt_ptr_t) ( /*_.CLASS_NAMED__V19*/ meltfptr[18]);

  MELT_LOCATION ("warmelt-debug.melt:1235:/ initchunk");
  /*^touch */
  /*touch:IROUTVAL_ */
  meltgc_touch ( /*_.VALROUT___V284*/ meltfptr[283]);

  /*putclosurout#29 */
  melt_assertmsg ("putclosrout#29 checkclo",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALCLO___V285*/ meltfptr[284])) ==
		  MELTOBMAG_CLOSURE);
  melt_assertmsg ("putclosrout#29 checkrout",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALROUT___V284*/ meltfptr[283])) ==
		  MELTOBMAG_ROUTINE);
  ((meltclosure_ptr_t) /*_.VALCLO___V285*/ meltfptr[284])->rout =
    (meltroutine_ptr_t) ( /*_.VALROUT___V284*/ meltfptr[283]);

  /*touch:DATACLOSURE_ */
  meltgc_touch ( /*_.VALCLO___V285*/ meltfptr[284]);

  /*putslot */
  melt_assertmsg ("putslot checkobj",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALDATA___V286*/ meltfptr[285])) ==
		  MELTOBMAG_OBJECT);

  melt_assertmsg ("putslot checkoff",
		  (1 >= 0
		   && 1 <
		   melt_object_length ((melt_ptr_t)
				       ( /*_.VALDATA___V286*/
					meltfptr[285]))));
  ((meltobject_ptr_t) ( /*_.VALDATA___V286*/ meltfptr[285]))->obj_vartab[1] =
    (melt_ptr_t) (
  /*_.VALSTR___V287*/ meltfptr[286]);

  /*touch:VALUEDATA_ */
  meltgc_touch ( /*_.VALDATA___V286*/ meltfptr[285]);


  melt_dbgtrace_written_object ( /*_.VALDATA___V286*/ meltfptr[285],
				"new static instance");

  MELT_LOCATION ("warmelt-debug.melt:1246:/ initchunk");
  /*^putroutconstnotnull */
  /*putroutconstnotnull */
  melt_assertmsg ("putroutconstnotnull checkrout",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALROUT___V288*/ meltfptr[287])) ==
		  MELTOBMAG_ROUTINE);
  melt_assertmsg ("putroutconstnotnull notnullconst",
		  NULL != /*_.VALDATA___V51*/ meltfptr[50]);
  ((meltroutine_ptr_t) /*_.VALROUT___V288*/ meltfptr[287])->tabval[0] =
    (melt_ptr_t) ( /*_.VALDATA___V51*/ meltfptr[50]);

  MELT_LOCATION ("warmelt-debug.melt:1246:/ initchunk");
  /*^touch */
  /*touch:IROUTVAL_ */
  meltgc_touch ( /*_.VALROUT___V288*/ meltfptr[287]);

  /*putclosurout#30 */
  melt_assertmsg ("putclosrout#30 checkclo",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALCLO___V289*/ meltfptr[288])) ==
		  MELTOBMAG_CLOSURE);
  melt_assertmsg ("putclosrout#30 checkrout",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALROUT___V288*/ meltfptr[287])) ==
		  MELTOBMAG_ROUTINE);
  ((meltclosure_ptr_t) /*_.VALCLO___V289*/ meltfptr[288])->rout =
    (meltroutine_ptr_t) ( /*_.VALROUT___V288*/ meltfptr[287]);

  /*touch:DATACLOSURE_ */
  meltgc_touch ( /*_.VALCLO___V289*/ meltfptr[288]);

  MELT_LOCATION ("warmelt-debug.melt:1264:/ initchunk");
  /*^putroutconstnotnull */
  /*putroutconstnotnull */
  melt_assertmsg ("putroutconstnotnull checkrout",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALROUT___V290*/ meltfptr[289])) ==
		  MELTOBMAG_ROUTINE);
  melt_assertmsg ("putroutconstnotnull notnullconst",
		  NULL != /*_.VALDATA___V51*/ meltfptr[50]);
  ((meltroutine_ptr_t) /*_.VALROUT___V290*/ meltfptr[289])->tabval[0] =
    (melt_ptr_t) ( /*_.VALDATA___V51*/ meltfptr[50]);

  MELT_LOCATION ("warmelt-debug.melt:1264:/ initchunk");
  /*^putroutconstnotnull */
  /*putroutconstnotnull */
  melt_assertmsg ("putroutconstnotnull checkrout",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALROUT___V290*/ meltfptr[289])) ==
		  MELTOBMAG_ROUTINE);
  melt_assertmsg ("putroutconstnotnull notnullconst",
		  NULL != /*_.VALDATA___V69*/ meltfptr[68]);
  ((meltroutine_ptr_t) /*_.VALROUT___V290*/ meltfptr[289])->tabval[1] =
    (melt_ptr_t) ( /*_.VALDATA___V69*/ meltfptr[68]);

  MELT_LOCATION ("warmelt-debug.melt:1264:/ initchunk");
  /*^putroutconstnotnull */
  /*putroutconstnotnull */
  melt_assertmsg ("putroutconstnotnull checkrout",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALROUT___V290*/ meltfptr[289])) ==
		  MELTOBMAG_ROUTINE);
  melt_assertmsg ("putroutconstnotnull notnullconst",
		  NULL != /*_.VALCLO___V112*/ meltfptr[111]);
  ((meltroutine_ptr_t) /*_.VALROUT___V290*/ meltfptr[289])->tabval[2] =
    (melt_ptr_t) ( /*_.VALCLO___V112*/ meltfptr[111]);

  MELT_LOCATION ("warmelt-debug.melt:1264:/ initchunk");
  /*^putroutconstnotnull */
  /*putroutconstnotnull */
  melt_assertmsg ("putroutconstnotnull checkrout",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALROUT___V290*/ meltfptr[289])) ==
		  MELTOBMAG_ROUTINE);
  melt_assertmsg ("putroutconstnotnull notnullconst",
		  NULL != /*_.VALCLO___V200*/ meltfptr[199]);
  ((meltroutine_ptr_t) /*_.VALROUT___V290*/ meltfptr[289])->tabval[3] =
    (melt_ptr_t) ( /*_.VALCLO___V200*/ meltfptr[199]);

  MELT_LOCATION ("warmelt-debug.melt:1264:/ initchunk");
  /*^touch */
  /*touch:IROUTVAL_ */
  meltgc_touch ( /*_.VALROUT___V290*/ meltfptr[289]);

  /*putclosurout#31 */
  melt_assertmsg ("putclosrout#31 checkclo",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALCLO___V291*/ meltfptr[290])) ==
		  MELTOBMAG_CLOSURE);
  melt_assertmsg ("putclosrout#31 checkrout",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALROUT___V290*/ meltfptr[289])) ==
		  MELTOBMAG_ROUTINE);
  ((meltclosure_ptr_t) /*_.VALCLO___V291*/ meltfptr[290])->rout =
    (meltroutine_ptr_t) ( /*_.VALROUT___V290*/ meltfptr[289]);

  /*touch:DATACLOSURE_ */
  meltgc_touch ( /*_.VALCLO___V291*/ meltfptr[290]);

  /*putslot */
  melt_assertmsg ("putslot checkobj",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALDATA___V292*/ meltfptr[291])) ==
		  MELTOBMAG_OBJECT);

  melt_assertmsg ("putslot checkoff",
		  (1 >= 0
		   && 1 <
		   melt_object_length ((melt_ptr_t)
				       ( /*_.VALDATA___V292*/
					meltfptr[291]))));
  ((meltobject_ptr_t) ( /*_.VALDATA___V292*/ meltfptr[291]))->obj_vartab[1] =
    (melt_ptr_t) (
  /*_.VALSTR___V293*/ meltfptr[292]);

  /*touch:VALUEDATA_ */
  meltgc_touch ( /*_.VALDATA___V292*/ meltfptr[291]);


  melt_dbgtrace_written_object ( /*_.VALDATA___V292*/ meltfptr[291],
				"new static instance");

  MELT_LOCATION ("warmelt-debug.melt:1301:/ initchunk");
  /*^putroutconstnotnull */
  /*putroutconstnotnull */
  melt_assertmsg ("putroutconstnotnull checkrout",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALROUT___V294*/ meltfptr[293])) ==
		  MELTOBMAG_ROUTINE);
  melt_assertmsg ("putroutconstnotnull notnullconst",
		  NULL != /*_.VALDATA___V51*/ meltfptr[50]);
  ((meltroutine_ptr_t) /*_.VALROUT___V294*/ meltfptr[293])->tabval[0] =
    (melt_ptr_t) ( /*_.VALDATA___V51*/ meltfptr[50]);

  MELT_LOCATION ("warmelt-debug.melt:1301:/ initchunk");
  /*^touch */
  /*touch:IROUTVAL_ */
  meltgc_touch ( /*_.VALROUT___V294*/ meltfptr[293]);

  /*putclosurout#32 */
  melt_assertmsg ("putclosrout#32 checkclo",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALCLO___V295*/ meltfptr[294])) ==
		  MELTOBMAG_CLOSURE);
  melt_assertmsg ("putclosrout#32 checkrout",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALROUT___V294*/ meltfptr[293])) ==
		  MELTOBMAG_ROUTINE);
  ((meltclosure_ptr_t) /*_.VALCLO___V295*/ meltfptr[294])->rout =
    (meltroutine_ptr_t) ( /*_.VALROUT___V294*/ meltfptr[293]);

  /*touch:DATACLOSURE_ */
  meltgc_touch ( /*_.VALCLO___V295*/ meltfptr[294]);

  MELT_LOCATION ("warmelt-debug.melt:1318:/ initchunk");
  /*^putroutconstnotnull */
  /*putroutconstnotnull */
  melt_assertmsg ("putroutconstnotnull checkrout",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALROUT___V296*/ meltfptr[295])) ==
		  MELTOBMAG_ROUTINE);
  melt_assertmsg ("putroutconstnotnull notnullconst",
		  NULL != /*_.VALDATA___V51*/ meltfptr[50]);
  ((meltroutine_ptr_t) /*_.VALROUT___V296*/ meltfptr[295])->tabval[0] =
    (melt_ptr_t) ( /*_.VALDATA___V51*/ meltfptr[50]);

  MELT_LOCATION ("warmelt-debug.melt:1318:/ initchunk");
  /*^putroutconst */
  /*putroutconst */
  if (MELT_HAS_INITIAL_ENVIRONMENT)
    melt_assertmsg ("putroutconst checkrout",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.VALROUT___V296*/ meltfptr[295]))
		    == MELTOBMAG_ROUTINE);
  if (MELT_HAS_INITIAL_ENVIRONMENT)
    melt_checkmsg
      ("putroutconst constnull.drout_250__DBGOUTAGAIN_SYMBOL_METHOD#1",
       NULL != ( /*_.CLASS_SYMBOL__V40*/ meltfptr[39]));
  ((meltroutine_ptr_t) /*_.VALROUT___V296*/ meltfptr[295])->tabval[1] =
    (melt_ptr_t) ( /*_.CLASS_SYMBOL__V40*/ meltfptr[39]);

  MELT_LOCATION ("warmelt-debug.melt:1318:/ initchunk");
  /*^touch */
  /*touch:IROUTVAL_ */
  meltgc_touch ( /*_.VALROUT___V296*/ meltfptr[295]);

  /*putclosurout#33 */
  melt_assertmsg ("putclosrout#33 checkclo",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALCLO___V297*/ meltfptr[296])) ==
		  MELTOBMAG_CLOSURE);
  melt_assertmsg ("putclosrout#33 checkrout",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALROUT___V296*/ meltfptr[295])) ==
		  MELTOBMAG_ROUTINE);
  ((meltclosure_ptr_t) /*_.VALCLO___V297*/ meltfptr[296])->rout =
    (meltroutine_ptr_t) ( /*_.VALROUT___V296*/ meltfptr[295]);

  /*touch:DATACLOSURE_ */
  meltgc_touch ( /*_.VALCLO___V297*/ meltfptr[296]);

  /*putslot */
  melt_assertmsg ("putslot checkobj",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALDATA___V298*/ meltfptr[297])) ==
		  MELTOBMAG_OBJECT);

  melt_assertmsg ("putslot checkoff",
		  (1 >= 0
		   && 1 <
		   melt_object_length ((melt_ptr_t)
				       ( /*_.VALDATA___V298*/
					meltfptr[297]))));
  ((meltobject_ptr_t) ( /*_.VALDATA___V298*/ meltfptr[297]))->obj_vartab[1] =
    (melt_ptr_t) (
  /*_.VALSTR___V299*/ meltfptr[298]);

  /*touch:VALUEDATA_ */
  meltgc_touch ( /*_.VALDATA___V298*/ meltfptr[297]);


  melt_dbgtrace_written_object ( /*_.VALDATA___V298*/ meltfptr[297],
				"new static instance");

  MELT_LOCATION ("warmelt-debug.melt:1336:/ initchunk");
  /*^putroutconstnotnull */
  /*putroutconstnotnull */
  melt_assertmsg ("putroutconstnotnull checkrout",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALROUT___V300*/ meltfptr[299])) ==
		  MELTOBMAG_ROUTINE);
  melt_assertmsg ("putroutconstnotnull notnullconst",
		  NULL != /*_.VALDATA___V51*/ meltfptr[50]);
  ((meltroutine_ptr_t) /*_.VALROUT___V300*/ meltfptr[299])->tabval[0] =
    (melt_ptr_t) ( /*_.VALDATA___V51*/ meltfptr[50]);

  MELT_LOCATION ("warmelt-debug.melt:1336:/ initchunk");
  /*^putroutconst */
  /*putroutconst */
  if (MELT_HAS_INITIAL_ENVIRONMENT)
    melt_assertmsg ("putroutconst checkrout",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.VALROUT___V300*/ meltfptr[299]))
		    == MELTOBMAG_ROUTINE);
  if (MELT_HAS_INITIAL_ENVIRONMENT)
    melt_checkmsg ("putroutconst constnull.drout_254__DBGOUT_SYMBOL_METHOD#1",
		   NULL != ( /*_.CLASS_SYMBOL__V40*/ meltfptr[39]));
  ((meltroutine_ptr_t) /*_.VALROUT___V300*/ meltfptr[299])->tabval[1] =
    (melt_ptr_t) ( /*_.CLASS_SYMBOL__V40*/ meltfptr[39]);

  MELT_LOCATION ("warmelt-debug.melt:1336:/ initchunk");
  /*^putroutconstnotnull */
  /*putroutconstnotnull */
  melt_assertmsg ("putroutconstnotnull checkrout",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALROUT___V300*/ meltfptr[299])) ==
		  MELTOBMAG_ROUTINE);
  melt_assertmsg ("putroutconstnotnull notnullconst",
		  NULL != /*_.VALCLO___V291*/ meltfptr[290]);
  ((meltroutine_ptr_t) /*_.VALROUT___V300*/ meltfptr[299])->tabval[2] =
    (melt_ptr_t) ( /*_.VALCLO___V291*/ meltfptr[290]);

  MELT_LOCATION ("warmelt-debug.melt:1336:/ initchunk");
  /*^putroutconstnotnull */
  /*putroutconstnotnull */
  melt_assertmsg ("putroutconstnotnull checkrout",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALROUT___V300*/ meltfptr[299])) ==
		  MELTOBMAG_ROUTINE);
  melt_assertmsg ("putroutconstnotnull notnullconst",
		  NULL != /*_.VALCLO___V297*/ meltfptr[296]);
  ((meltroutine_ptr_t) /*_.VALROUT___V300*/ meltfptr[299])->tabval[3] =
    (melt_ptr_t) ( /*_.VALCLO___V297*/ meltfptr[296]);

  MELT_LOCATION ("warmelt-debug.melt:1336:/ initchunk");
  /*^touch */
  /*touch:IROUTVAL_ */
  meltgc_touch ( /*_.VALROUT___V300*/ meltfptr[299]);

  /*putclosurout#34 */
  melt_assertmsg ("putclosrout#34 checkclo",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALCLO___V301*/ meltfptr[300])) ==
		  MELTOBMAG_CLOSURE);
  melt_assertmsg ("putclosrout#34 checkrout",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALROUT___V300*/ meltfptr[299])) ==
		  MELTOBMAG_ROUTINE);
  ((meltclosure_ptr_t) /*_.VALCLO___V301*/ meltfptr[300])->rout =
    (meltroutine_ptr_t) ( /*_.VALROUT___V300*/ meltfptr[299]);

  /*touch:DATACLOSURE_ */
  meltgc_touch ( /*_.VALCLO___V301*/ meltfptr[300]);

  MELT_LOCATION ("warmelt-debug.melt:1346:/ initchunk");
  /*^putroutconstnotnull */
  /*putroutconstnotnull */
  melt_assertmsg ("putroutconstnotnull checkrout",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALROUT___V302*/ meltfptr[301])) ==
		  MELTOBMAG_ROUTINE);
  melt_assertmsg ("putroutconstnotnull notnullconst",
		  NULL != /*_.VALDATA___V51*/ meltfptr[50]);
  ((meltroutine_ptr_t) /*_.VALROUT___V302*/ meltfptr[301])->tabval[0] =
    (melt_ptr_t) ( /*_.VALDATA___V51*/ meltfptr[50]);

  MELT_LOCATION ("warmelt-debug.melt:1346:/ initchunk");
  /*^putroutconst */
  /*putroutconst */
  if (MELT_HAS_INITIAL_ENVIRONMENT)
    melt_assertmsg ("putroutconst checkrout",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.VALROUT___V302*/ meltfptr[301]))
		    == MELTOBMAG_ROUTINE);
  if (MELT_HAS_INITIAL_ENVIRONMENT)
    melt_checkmsg ("putroutconst constnull.drout_256__DBGOUT_CLASS_METHOD#1",
		   NULL != ( /*_.CLASS_CLASS__V41*/ meltfptr[40]));
  ((meltroutine_ptr_t) /*_.VALROUT___V302*/ meltfptr[301])->tabval[1] =
    (melt_ptr_t) ( /*_.CLASS_CLASS__V41*/ meltfptr[40]);

  MELT_LOCATION ("warmelt-debug.melt:1346:/ initchunk");
  /*^putroutconstnotnull */
  /*putroutconstnotnull */
  melt_assertmsg ("putroutconstnotnull checkrout",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALROUT___V302*/ meltfptr[301])) ==
		  MELTOBMAG_ROUTINE);
  melt_assertmsg ("putroutconstnotnull notnullconst",
		  NULL != /*_.VALCLO___V291*/ meltfptr[290]);
  ((meltroutine_ptr_t) /*_.VALROUT___V302*/ meltfptr[301])->tabval[2] =
    (melt_ptr_t) ( /*_.VALCLO___V291*/ meltfptr[290]);

  MELT_LOCATION ("warmelt-debug.melt:1346:/ initchunk");
  /*^putroutconstnotnull */
  /*putroutconstnotnull */
  melt_assertmsg ("putroutconstnotnull checkrout",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALROUT___V302*/ meltfptr[301])) ==
		  MELTOBMAG_ROUTINE);
  melt_assertmsg ("putroutconstnotnull notnullconst",
		  NULL != /*_.VALCLO___V295*/ meltfptr[294]);
  ((meltroutine_ptr_t) /*_.VALROUT___V302*/ meltfptr[301])->tabval[3] =
    (melt_ptr_t) ( /*_.VALCLO___V295*/ meltfptr[294]);

  MELT_LOCATION ("warmelt-debug.melt:1346:/ initchunk");
  /*^touch */
  /*touch:IROUTVAL_ */
  meltgc_touch ( /*_.VALROUT___V302*/ meltfptr[301]);

  /*putclosurout#35 */
  melt_assertmsg ("putclosrout#35 checkclo",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALCLO___V303*/ meltfptr[302])) ==
		  MELTOBMAG_CLOSURE);
  melt_assertmsg ("putclosrout#35 checkrout",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALROUT___V302*/ meltfptr[301])) ==
		  MELTOBMAG_ROUTINE);
  ((meltclosure_ptr_t) /*_.VALCLO___V303*/ meltfptr[302])->rout =
    (meltroutine_ptr_t) ( /*_.VALROUT___V302*/ meltfptr[301]);

  /*touch:DATACLOSURE_ */
  meltgc_touch ( /*_.VALCLO___V303*/ meltfptr[302]);

  /*putslot */
  melt_assertmsg ("putslot checkobj",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALDATA___V304*/ meltfptr[303])) ==
		  MELTOBMAG_OBJECT);

  melt_assertmsg ("putslot checkoff",
		  (1 >= 0
		   && 1 <
		   melt_object_length ((melt_ptr_t)
				       ( /*_.VALDATA___V304*/
					meltfptr[303]))));
  ((meltobject_ptr_t) ( /*_.VALDATA___V304*/ meltfptr[303]))->obj_vartab[1] =
    (melt_ptr_t) (
  /*_.VALSTR___V305*/ meltfptr[304]);

  /*touch:VALUEDATA_ */
  meltgc_touch ( /*_.VALDATA___V304*/ meltfptr[303]);


  melt_dbgtrace_written_object ( /*_.VALDATA___V304*/ meltfptr[303],
				"new static instance");

  MELT_LOCATION ("warmelt-debug.melt:1356:/ initchunk");
  /*^putroutconstnotnull */
  /*putroutconstnotnull */
  melt_assertmsg ("putroutconstnotnull checkrout",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALROUT___V306*/ meltfptr[305])) ==
		  MELTOBMAG_ROUTINE);
  melt_assertmsg ("putroutconstnotnull notnullconst",
		  NULL != /*_.VALDATA___V51*/ meltfptr[50]);
  ((meltroutine_ptr_t) /*_.VALROUT___V306*/ meltfptr[305])->tabval[0] =
    (melt_ptr_t) ( /*_.VALDATA___V51*/ meltfptr[50]);

  MELT_LOCATION ("warmelt-debug.melt:1356:/ initchunk");
  /*^putroutconst */
  /*putroutconst */
  if (MELT_HAS_INITIAL_ENVIRONMENT)
    melt_assertmsg ("putroutconst checkrout",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.VALROUT___V306*/ meltfptr[305]))
		    == MELTOBMAG_ROUTINE);
  if (MELT_HAS_INITIAL_ENVIRONMENT)
    melt_checkmsg
      ("putroutconst constnull.drout_260__DBGOUTAGAIN_KEYWORD_METHOD#1",
       NULL != ( /*_.CLASS_KEYWORD__V42*/ meltfptr[41]));
  ((meltroutine_ptr_t) /*_.VALROUT___V306*/ meltfptr[305])->tabval[1] =
    (melt_ptr_t) ( /*_.CLASS_KEYWORD__V42*/ meltfptr[41]);

  MELT_LOCATION ("warmelt-debug.melt:1356:/ initchunk");
  /*^touch */
  /*touch:IROUTVAL_ */
  meltgc_touch ( /*_.VALROUT___V306*/ meltfptr[305]);

  /*putclosurout#36 */
  melt_assertmsg ("putclosrout#36 checkclo",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALCLO___V307*/ meltfptr[306])) ==
		  MELTOBMAG_CLOSURE);
  melt_assertmsg ("putclosrout#36 checkrout",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALROUT___V306*/ meltfptr[305])) ==
		  MELTOBMAG_ROUTINE);
  ((meltclosure_ptr_t) /*_.VALCLO___V307*/ meltfptr[306])->rout =
    (meltroutine_ptr_t) ( /*_.VALROUT___V306*/ meltfptr[305]);

  /*touch:DATACLOSURE_ */
  meltgc_touch ( /*_.VALCLO___V307*/ meltfptr[306]);

  /*putslot */
  melt_assertmsg ("putslot checkobj",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALDATA___V308*/ meltfptr[307])) ==
		  MELTOBMAG_OBJECT);

  melt_assertmsg ("putslot checkoff",
		  (1 >= 0
		   && 1 <
		   melt_object_length ((melt_ptr_t)
				       ( /*_.VALDATA___V308*/
					meltfptr[307]))));
  ((meltobject_ptr_t) ( /*_.VALDATA___V308*/ meltfptr[307]))->obj_vartab[1] =
    (melt_ptr_t) (
  /*_.VALSTR___V309*/ meltfptr[308]);

  /*touch:VALUEDATA_ */
  meltgc_touch ( /*_.VALDATA___V308*/ meltfptr[307]);


  melt_dbgtrace_written_object ( /*_.VALDATA___V308*/ meltfptr[307],
				"new static instance");

  MELT_LOCATION ("warmelt-debug.melt:1369:/ initchunk");
  /*^putroutconstnotnull */
  /*putroutconstnotnull */
  melt_assertmsg ("putroutconstnotnull checkrout",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALROUT___V310*/ meltfptr[309])) ==
		  MELTOBMAG_ROUTINE);
  melt_assertmsg ("putroutconstnotnull notnullconst",
		  NULL != /*_.VALDATA___V51*/ meltfptr[50]);
  ((meltroutine_ptr_t) /*_.VALROUT___V310*/ meltfptr[309])->tabval[0] =
    (melt_ptr_t) ( /*_.VALDATA___V51*/ meltfptr[50]);

  MELT_LOCATION ("warmelt-debug.melt:1369:/ initchunk");
  /*^putroutconst */
  /*putroutconst */
  if (MELT_HAS_INITIAL_ENVIRONMENT)
    melt_assertmsg ("putroutconst checkrout",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.VALROUT___V310*/ meltfptr[309]))
		    == MELTOBMAG_ROUTINE);
  if (MELT_HAS_INITIAL_ENVIRONMENT)
    melt_checkmsg
      ("putroutconst constnull.drout_264__DBGOUT_KEYWORD_METHOD#1",
       NULL != ( /*_.CLASS_KEYWORD__V42*/ meltfptr[41]));
  ((meltroutine_ptr_t) /*_.VALROUT___V310*/ meltfptr[309])->tabval[1] =
    (melt_ptr_t) ( /*_.CLASS_KEYWORD__V42*/ meltfptr[41]);

  MELT_LOCATION ("warmelt-debug.melt:1369:/ initchunk");
  /*^putroutconstnotnull */
  /*putroutconstnotnull */
  melt_assertmsg ("putroutconstnotnull checkrout",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALROUT___V310*/ meltfptr[309])) ==
		  MELTOBMAG_ROUTINE);
  melt_assertmsg ("putroutconstnotnull notnullconst",
		  NULL != /*_.VALCLO___V291*/ meltfptr[290]);
  ((meltroutine_ptr_t) /*_.VALROUT___V310*/ meltfptr[309])->tabval[2] =
    (melt_ptr_t) ( /*_.VALCLO___V291*/ meltfptr[290]);

  MELT_LOCATION ("warmelt-debug.melt:1369:/ initchunk");
  /*^putroutconstnotnull */
  /*putroutconstnotnull */
  melt_assertmsg ("putroutconstnotnull checkrout",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALROUT___V310*/ meltfptr[309])) ==
		  MELTOBMAG_ROUTINE);
  melt_assertmsg ("putroutconstnotnull notnullconst",
		  NULL != /*_.VALCLO___V307*/ meltfptr[306]);
  ((meltroutine_ptr_t) /*_.VALROUT___V310*/ meltfptr[309])->tabval[3] =
    (melt_ptr_t) ( /*_.VALCLO___V307*/ meltfptr[306]);

  MELT_LOCATION ("warmelt-debug.melt:1369:/ initchunk");
  /*^touch */
  /*touch:IROUTVAL_ */
  meltgc_touch ( /*_.VALROUT___V310*/ meltfptr[309]);

  /*putclosurout#37 */
  melt_assertmsg ("putclosrout#37 checkclo",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALCLO___V311*/ meltfptr[310])) ==
		  MELTOBMAG_CLOSURE);
  melt_assertmsg ("putclosrout#37 checkrout",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALROUT___V310*/ meltfptr[309])) ==
		  MELTOBMAG_ROUTINE);
  ((meltclosure_ptr_t) /*_.VALCLO___V311*/ meltfptr[310])->rout =
    (meltroutine_ptr_t) ( /*_.VALROUT___V310*/ meltfptr[309]);

  /*touch:DATACLOSURE_ */
  meltgc_touch ( /*_.VALCLO___V311*/ meltfptr[310]);

  /*putclosurout#38 */
  melt_assertmsg ("putclosrout#38 checkclo",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALCLO___V313*/ meltfptr[312])) ==
		  MELTOBMAG_CLOSURE);
  melt_assertmsg ("putclosrout#38 checkrout",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALROUT___V312*/ meltfptr[311])) ==
		  MELTOBMAG_ROUTINE);
  ((meltclosure_ptr_t) /*_.VALCLO___V313*/ meltfptr[312])->rout =
    (meltroutine_ptr_t) ( /*_.VALROUT___V312*/ meltfptr[311]);

  /*touch:DATACLOSURE_ */
  meltgc_touch ( /*_.VALCLO___V313*/ meltfptr[312]);

  /*putslot */
  melt_assertmsg ("putslot checkobj",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALDATA___V314*/ meltfptr[313])) ==
		  MELTOBMAG_OBJECT);

  melt_assertmsg ("putslot checkoff",
		  (1 >= 0
		   && 1 <
		   melt_object_length ((melt_ptr_t)
				       ( /*_.VALDATA___V314*/
					meltfptr[313]))));
  ((meltobject_ptr_t) ( /*_.VALDATA___V314*/ meltfptr[313]))->obj_vartab[1] =
    (melt_ptr_t) (
  /*_.VALSTR___V315*/ meltfptr[314]);

  /*touch:VALUEDATA_ */
  meltgc_touch ( /*_.VALDATA___V314*/ meltfptr[313]);


  melt_dbgtrace_written_object ( /*_.VALDATA___V314*/ meltfptr[313],
				"new static instance");

  MELT_LOCATION ("warmelt-debug.melt:1394:/ initchunk");
  /*^putroutconstnotnull */
  /*putroutconstnotnull */
  melt_assertmsg ("putroutconstnotnull checkrout",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALROUT___V316*/ meltfptr[315])) ==
		  MELTOBMAG_ROUTINE);
  melt_assertmsg ("putroutconstnotnull notnullconst",
		  NULL != /*_.VALDATA___V51*/ meltfptr[50]);
  ((meltroutine_ptr_t) /*_.VALROUT___V316*/ meltfptr[315])->tabval[0] =
    (melt_ptr_t) ( /*_.VALDATA___V51*/ meltfptr[50]);

  MELT_LOCATION ("warmelt-debug.melt:1394:/ initchunk");
  /*^putroutconst */
  /*putroutconst */
  if (MELT_HAS_INITIAL_ENVIRONMENT)
    melt_assertmsg ("putroutconst checkrout",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.VALROUT___V316*/ meltfptr[315]))
		    == MELTOBMAG_ROUTINE);
  if (MELT_HAS_INITIAL_ENVIRONMENT)
    melt_checkmsg
      ("putroutconst constnull.drout_270__DBGOUT_CLONED_SYMBOL_METHOD#1",
       NULL != ( /*_.CLASS_CLONED_SYMBOL__V43*/ meltfptr[42]));
  ((meltroutine_ptr_t) /*_.VALROUT___V316*/ meltfptr[315])->tabval[1] =
    (melt_ptr_t) ( /*_.CLASS_CLONED_SYMBOL__V43*/ meltfptr[42]);

  MELT_LOCATION ("warmelt-debug.melt:1394:/ initchunk");
  /*^putroutconstnotnull */
  /*putroutconstnotnull */
  melt_assertmsg ("putroutconstnotnull checkrout",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALROUT___V316*/ meltfptr[315])) ==
		  MELTOBMAG_ROUTINE);
  melt_assertmsg ("putroutconstnotnull notnullconst",
		  NULL != /*_.VALCLO___V291*/ meltfptr[290]);
  ((meltroutine_ptr_t) /*_.VALROUT___V316*/ meltfptr[315])->tabval[2] =
    (melt_ptr_t) ( /*_.VALCLO___V291*/ meltfptr[290]);

  MELT_LOCATION ("warmelt-debug.melt:1394:/ initchunk");
  /*^putroutconstnotnull */
  /*putroutconstnotnull */
  melt_assertmsg ("putroutconstnotnull checkrout",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALROUT___V316*/ meltfptr[315])) ==
		  MELTOBMAG_ROUTINE);
  melt_assertmsg ("putroutconstnotnull notnullconst",
		  NULL != /*_.VALCLO___V313*/ meltfptr[312]);
  ((meltroutine_ptr_t) /*_.VALROUT___V316*/ meltfptr[315])->tabval[3] =
    (melt_ptr_t) ( /*_.VALCLO___V313*/ meltfptr[312]);

  MELT_LOCATION ("warmelt-debug.melt:1394:/ initchunk");
  /*^touch */
  /*touch:IROUTVAL_ */
  meltgc_touch ( /*_.VALROUT___V316*/ meltfptr[315]);

  /*putclosurout#39 */
  melt_assertmsg ("putclosrout#39 checkclo",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALCLO___V317*/ meltfptr[316])) ==
		  MELTOBMAG_CLOSURE);
  melt_assertmsg ("putclosrout#39 checkrout",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALROUT___V316*/ meltfptr[315])) ==
		  MELTOBMAG_ROUTINE);
  ((meltclosure_ptr_t) /*_.VALCLO___V317*/ meltfptr[316])->rout =
    (meltroutine_ptr_t) ( /*_.VALROUT___V316*/ meltfptr[315]);

  /*touch:DATACLOSURE_ */
  meltgc_touch ( /*_.VALCLO___V317*/ meltfptr[316]);

  MELT_LOCATION ("warmelt-debug.melt:1405:/ initchunk");
  /*^putroutconstnotnull */
  /*putroutconstnotnull */
  melt_assertmsg ("putroutconstnotnull checkrout",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALROUT___V318*/ meltfptr[317])) ==
		  MELTOBMAG_ROUTINE);
  melt_assertmsg ("putroutconstnotnull notnullconst",
		  NULL != /*_.VALDATA___V51*/ meltfptr[50]);
  ((meltroutine_ptr_t) /*_.VALROUT___V318*/ meltfptr[317])->tabval[0] =
    (melt_ptr_t) ( /*_.VALDATA___V51*/ meltfptr[50]);

  MELT_LOCATION ("warmelt-debug.melt:1405:/ initchunk");
  /*^putroutconst */
  /*putroutconst */
  if (MELT_HAS_INITIAL_ENVIRONMENT)
    melt_assertmsg ("putroutconst checkrout",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.VALROUT___V318*/ meltfptr[317]))
		    == MELTOBMAG_ROUTINE);
  if (MELT_HAS_INITIAL_ENVIRONMENT)
    melt_checkmsg
      ("putroutconst constnull.drout_272__DBGOUT_ENVIRONMENT_METHOD#1",
       NULL != ( /*_.CLASS_ENVIRONMENT__V44*/ meltfptr[43]));
  ((meltroutine_ptr_t) /*_.VALROUT___V318*/ meltfptr[317])->tabval[1] =
    (melt_ptr_t) ( /*_.CLASS_ENVIRONMENT__V44*/ meltfptr[43]);

  MELT_LOCATION ("warmelt-debug.melt:1405:/ initchunk");
  /*^putroutconst */
  /*putroutconst */
  if (MELT_HAS_INITIAL_ENVIRONMENT)
    melt_assertmsg ("putroutconst checkrout",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.VALROUT___V318*/ meltfptr[317]))
		    == MELTOBMAG_ROUTINE);
  if (MELT_HAS_INITIAL_ENVIRONMENT)
    melt_checkmsg
      ("putroutconst constnull.drout_272__DBGOUT_ENVIRONMENT_METHOD#2",
       NULL != ( /*_.ENV_PREV__V45*/ meltfptr[44]));
  ((meltroutine_ptr_t) /*_.VALROUT___V318*/ meltfptr[317])->tabval[2] =
    (melt_ptr_t) ( /*_.ENV_PREV__V45*/ meltfptr[44]);

  MELT_LOCATION ("warmelt-debug.melt:1405:/ initchunk");
  /*^putroutconstnotnull */
  /*putroutconstnotnull */
  melt_assertmsg ("putroutconstnotnull checkrout",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALROUT___V318*/ meltfptr[317])) ==
		  MELTOBMAG_ROUTINE);
  melt_assertmsg ("putroutconstnotnull notnullconst",
		  NULL != /*_.VALDATA___V69*/ meltfptr[68]);
  ((meltroutine_ptr_t) /*_.VALROUT___V318*/ meltfptr[317])->tabval[3] =
    (melt_ptr_t) ( /*_.VALDATA___V69*/ meltfptr[68]);

  MELT_LOCATION ("warmelt-debug.melt:1405:/ initchunk");
  /*^putroutconstnotnull */
  /*putroutconstnotnull */
  melt_assertmsg ("putroutconstnotnull checkrout",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALROUT___V318*/ meltfptr[317])) ==
		  MELTOBMAG_ROUTINE);
  melt_assertmsg ("putroutconstnotnull notnullconst",
		  NULL != /*_.VALCLO___V200*/ meltfptr[199]);
  ((meltroutine_ptr_t) /*_.VALROUT___V318*/ meltfptr[317])->tabval[4] =
    (melt_ptr_t) ( /*_.VALCLO___V200*/ meltfptr[199]);

  MELT_LOCATION ("warmelt-debug.melt:1405:/ initchunk");
  /*^putroutconstnotnull */
  /*putroutconstnotnull */
  melt_assertmsg ("putroutconstnotnull checkrout",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALROUT___V318*/ meltfptr[317])) ==
		  MELTOBMAG_ROUTINE);
  melt_assertmsg ("putroutconstnotnull notnullconst",
		  NULL != /*_.VALCLO___V202*/ meltfptr[201]);
  ((meltroutine_ptr_t) /*_.VALROUT___V318*/ meltfptr[317])->tabval[5] =
    (melt_ptr_t) ( /*_.VALCLO___V202*/ meltfptr[201]);

  MELT_LOCATION ("warmelt-debug.melt:1405:/ initchunk");
  /*^touch */
  /*touch:IROUTVAL_ */
  meltgc_touch ( /*_.VALROUT___V318*/ meltfptr[317]);

  /*putclosurout#40 */
  melt_assertmsg ("putclosrout#40 checkclo",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALCLO___V319*/ meltfptr[318])) ==
		  MELTOBMAG_CLOSURE);
  melt_assertmsg ("putclosrout#40 checkrout",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALROUT___V318*/ meltfptr[317])) ==
		  MELTOBMAG_ROUTINE);
  ((meltclosure_ptr_t) /*_.VALCLO___V319*/ meltfptr[318])->rout =
    (meltroutine_ptr_t) ( /*_.VALROUT___V318*/ meltfptr[317]);

  /*touch:DATACLOSURE_ */
  meltgc_touch ( /*_.VALCLO___V319*/ meltfptr[318]);

  /*putslot */
  melt_assertmsg ("putslot checkobj",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALDATA___V320*/ meltfptr[319])) ==
		  MELTOBMAG_OBJECT);

  melt_assertmsg ("putslot checkoff",
		  (1 >= 0
		   && 1 <
		   melt_object_length ((melt_ptr_t)
				       ( /*_.VALDATA___V320*/
					meltfptr[319]))));
  ((meltobject_ptr_t) ( /*_.VALDATA___V320*/ meltfptr[319]))->obj_vartab[1] =
    (melt_ptr_t) (
  /*_.VALSTR___V321*/ meltfptr[320]);

  /*touch:VALUEDATA_ */
  meltgc_touch ( /*_.VALDATA___V320*/ meltfptr[319]);


  melt_dbgtrace_written_object ( /*_.VALDATA___V320*/ meltfptr[319],
				"new static instance");

  /*putslot */
  melt_assertmsg ("putslot checkobj",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALDATA___V322*/ meltfptr[321])) ==
		  MELTOBMAG_OBJECT);

  melt_assertmsg ("putslot checkoff",
		  (1 >= 0
		   && 1 <
		   melt_object_length ((melt_ptr_t)
				       ( /*_.VALDATA___V322*/
					meltfptr[321]))));
  ((meltobject_ptr_t) ( /*_.VALDATA___V322*/ meltfptr[321]))->obj_vartab[1] =
    (melt_ptr_t) (
  /*_.VALSTR___V323*/ meltfptr[322]);

  /*touch:VALUEDATA_ */
  meltgc_touch ( /*_.VALDATA___V322*/ meltfptr[321]);


  melt_dbgtrace_written_object ( /*_.VALDATA___V322*/ meltfptr[321],
				"new static instance");

  /*putclosurout#41 */
  melt_assertmsg ("putclosrout#41 checkclo",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALCLO___V325*/ meltfptr[324])) ==
		  MELTOBMAG_CLOSURE);
  melt_assertmsg ("putclosrout#41 checkrout",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALROUT___V324*/ meltfptr[323])) ==
		  MELTOBMAG_ROUTINE);
  ((meltclosure_ptr_t) /*_.VALCLO___V325*/ meltfptr[324])->rout =
    (meltroutine_ptr_t) ( /*_.VALROUT___V324*/ meltfptr[323]);


#undef meltfram__
}				/*end of meltmod__warmelt_debug__initialmeltchunk_4 */

void
meltmod__warmelt_debug__initialmeltchunk_5 (struct
					    frame_melt_start_this_module_st
					    *meltmeltframptr__,
					    char meltpredefinited[])
{
#define meltfram__ (*meltmeltframptr__)
#undef meltcallcount
#define meltcallcount 0L
  (void) meltpredefinited;
/*touch:DATACLOSURE_*/
  meltgc_touch ( /*_.VALCLO___V325*/ meltfptr[324]);

  MELT_LOCATION ("warmelt-debug.melt:1462:/ initchunk");
  /*^putroutconstnotnull */
  /*putroutconstnotnull */
  melt_assertmsg ("putroutconstnotnull checkrout",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALROUT___V326*/ meltfptr[325])) ==
		  MELTOBMAG_ROUTINE);
  melt_assertmsg ("putroutconstnotnull notnullconst",
		  NULL != /*_.VALDATA___V51*/ meltfptr[50]);
  ((meltroutine_ptr_t) /*_.VALROUT___V326*/ meltfptr[325])->tabval[0] =
    (melt_ptr_t) ( /*_.VALDATA___V51*/ meltfptr[50]);

  MELT_LOCATION ("warmelt-debug.melt:1462:/ initchunk");
  /*^putroutconst */
  /*putroutconst */
  if (MELT_HAS_INITIAL_ENVIRONMENT)
    melt_assertmsg ("putroutconst checkrout",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.VALROUT___V326*/ meltfptr[325]))
		    == MELTOBMAG_ROUTINE);
  if (MELT_HAS_INITIAL_ENVIRONMENT)
    melt_checkmsg ("putroutconst constnull.drout_280__DBGOUT_CTYPE_METHOD#1",
		   NULL != ( /*_.CLASS_CTYPE__V20*/ meltfptr[19]));
  ((meltroutine_ptr_t) /*_.VALROUT___V326*/ meltfptr[325])->tabval[1] =
    (melt_ptr_t) ( /*_.CLASS_CTYPE__V20*/ meltfptr[19]);

  MELT_LOCATION ("warmelt-debug.melt:1462:/ initchunk");
  /*^putroutconstnotnull */
  /*putroutconstnotnull */
  melt_assertmsg ("putroutconstnotnull checkrout",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALROUT___V326*/ meltfptr[325])) ==
		  MELTOBMAG_ROUTINE);
  melt_assertmsg ("putroutconstnotnull notnullconst",
		  NULL != /*_.VALCLO___V291*/ meltfptr[290]);
  ((meltroutine_ptr_t) /*_.VALROUT___V326*/ meltfptr[325])->tabval[2] =
    (melt_ptr_t) ( /*_.VALCLO___V291*/ meltfptr[290]);

  MELT_LOCATION ("warmelt-debug.melt:1462:/ initchunk");
  /*^putroutconstnotnull */
  /*putroutconstnotnull */
  melt_assertmsg ("putroutconstnotnull checkrout",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALROUT___V326*/ meltfptr[325])) ==
		  MELTOBMAG_ROUTINE);
  melt_assertmsg ("putroutconstnotnull notnullconst",
		  NULL != /*_.VALCLO___V325*/ meltfptr[324]);
  ((meltroutine_ptr_t) /*_.VALROUT___V326*/ meltfptr[325])->tabval[3] =
    (melt_ptr_t) ( /*_.VALCLO___V325*/ meltfptr[324]);

  MELT_LOCATION ("warmelt-debug.melt:1462:/ initchunk");
  /*^touch */
  /*touch:IROUTVAL_ */
  meltgc_touch ( /*_.VALROUT___V326*/ meltfptr[325]);

  /*putclosurout#42 */
  melt_assertmsg ("putclosrout#42 checkclo",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALCLO___V327*/ meltfptr[326])) ==
		  MELTOBMAG_CLOSURE);
  melt_assertmsg ("putclosrout#42 checkrout",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALROUT___V326*/ meltfptr[325])) ==
		  MELTOBMAG_ROUTINE);
  ((meltclosure_ptr_t) /*_.VALCLO___V327*/ meltfptr[326])->rout =
    (meltroutine_ptr_t) ( /*_.VALROUT___V326*/ meltfptr[325]);

  /*touch:DATACLOSURE_ */
  meltgc_touch ( /*_.VALCLO___V327*/ meltfptr[326]);

  MELT_LOCATION ("warmelt-debug.melt:1472:/ initchunk");
  /*^putroutconstnotnull */
  /*putroutconstnotnull */
  melt_assertmsg ("putroutconstnotnull checkrout",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALROUT___V328*/ meltfptr[327])) ==
		  MELTOBMAG_ROUTINE);
  melt_assertmsg ("putroutconstnotnull notnullconst",
		  NULL != /*_.VALDATA___V51*/ meltfptr[50]);
  ((meltroutine_ptr_t) /*_.VALROUT___V328*/ meltfptr[327])->tabval[0] =
    (melt_ptr_t) ( /*_.VALDATA___V51*/ meltfptr[50]);

  MELT_LOCATION ("warmelt-debug.melt:1472:/ initchunk");
  /*^putroutconst */
  /*putroutconst */
  if (MELT_HAS_INITIAL_ENVIRONMENT)
    melt_assertmsg ("putroutconst checkrout",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.VALROUT___V328*/ meltfptr[327]))
		    == MELTOBMAG_ROUTINE);
  if (MELT_HAS_INITIAL_ENVIRONMENT)
    melt_checkmsg
      ("putroutconst constnull.drout_282__DBGOUT_ANYBINDING_METHOD#1",
       NULL != ( /*_.CLASS_ANY_BINDING__V46*/ meltfptr[45]));
  ((meltroutine_ptr_t) /*_.VALROUT___V328*/ meltfptr[327])->tabval[1] =
    (melt_ptr_t) ( /*_.CLASS_ANY_BINDING__V46*/ meltfptr[45]);

  MELT_LOCATION ("warmelt-debug.melt:1472:/ initchunk");
  /*^putroutconstnotnull */
  /*putroutconstnotnull */
  melt_assertmsg ("putroutconstnotnull checkrout",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALROUT___V328*/ meltfptr[327])) ==
		  MELTOBMAG_ROUTINE);
  melt_assertmsg ("putroutconstnotnull notnullconst",
		  NULL != /*_.VALDATA___V116*/ meltfptr[115]);
  ((meltroutine_ptr_t) /*_.VALROUT___V328*/ meltfptr[327])->tabval[2] =
    (melt_ptr_t) ( /*_.VALDATA___V116*/ meltfptr[115]);

  MELT_LOCATION ("warmelt-debug.melt:1472:/ initchunk");
  /*^putroutconstnotnull */
  /*putroutconstnotnull */
  melt_assertmsg ("putroutconstnotnull checkrout",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALROUT___V328*/ meltfptr[327])) ==
		  MELTOBMAG_ROUTINE);
  melt_assertmsg ("putroutconstnotnull notnullconst",
		  NULL != /*_.VALDATA___V69*/ meltfptr[68]);
  ((meltroutine_ptr_t) /*_.VALROUT___V328*/ meltfptr[327])->tabval[3] =
    (melt_ptr_t) ( /*_.VALDATA___V69*/ meltfptr[68]);

  MELT_LOCATION ("warmelt-debug.melt:1472:/ initchunk");
  /*^putroutconstnotnull */
  /*putroutconstnotnull */
  melt_assertmsg ("putroutconstnotnull checkrout",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALROUT___V328*/ meltfptr[327])) ==
		  MELTOBMAG_ROUTINE);
  melt_assertmsg ("putroutconstnotnull notnullconst",
		  NULL != /*_.VALCLO___V200*/ meltfptr[199]);
  ((meltroutine_ptr_t) /*_.VALROUT___V328*/ meltfptr[327])->tabval[4] =
    (melt_ptr_t) ( /*_.VALCLO___V200*/ meltfptr[199]);

  MELT_LOCATION ("warmelt-debug.melt:1472:/ initchunk");
  /*^touch */
  /*touch:IROUTVAL_ */
  meltgc_touch ( /*_.VALROUT___V328*/ meltfptr[327]);

  /*putclosurout#43 */
  melt_assertmsg ("putclosrout#43 checkclo",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALCLO___V329*/ meltfptr[328])) ==
		  MELTOBMAG_CLOSURE);
  melt_assertmsg ("putclosrout#43 checkrout",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALROUT___V328*/ meltfptr[327])) ==
		  MELTOBMAG_ROUTINE);
  ((meltclosure_ptr_t) /*_.VALCLO___V329*/ meltfptr[328])->rout =
    (meltroutine_ptr_t) ( /*_.VALROUT___V328*/ meltfptr[327]);

  /*touch:DATACLOSURE_ */
  meltgc_touch ( /*_.VALCLO___V329*/ meltfptr[328]);

  /*putslot */
  melt_assertmsg ("putslot checkobj",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALDATA___V330*/ meltfptr[329])) ==
		  MELTOBMAG_OBJECT);

  melt_assertmsg ("putslot checkoff",
		  (1 >= 0
		   && 1 <
		   melt_object_length ((melt_ptr_t)
				       ( /*_.VALDATA___V330*/
					meltfptr[329]))));
  ((meltobject_ptr_t) ( /*_.VALDATA___V330*/ meltfptr[329]))->obj_vartab[1] =
    (melt_ptr_t) (
  /*_.VALSTR___V331*/ meltfptr[330]);

  /*touch:VALUEDATA_ */
  meltgc_touch ( /*_.VALDATA___V330*/ meltfptr[329]);


  melt_dbgtrace_written_object ( /*_.VALDATA___V330*/ meltfptr[329],
				"new static instance");

  MELT_LOCATION ("warmelt-debug.melt:1509:/ initchunk");
  /*^putroutconst */
  /*putroutconst */
  if (MELT_HAS_INITIAL_ENVIRONMENT)
    melt_assertmsg ("putroutconst checkrout",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.VALROUT___V332*/ meltfptr[331]))
		    == MELTOBMAG_ROUTINE);
  if (MELT_HAS_INITIAL_ENVIRONMENT)
    melt_checkmsg
      ("putroutconst constnull.drout_286__DISPLAY_DEBUG_MESSAGE#0",
       NULL != ( /*_.DISCR_STRBUF__V12*/ meltfptr[11]));
  ((meltroutine_ptr_t) /*_.VALROUT___V332*/ meltfptr[331])->tabval[0] =
    (melt_ptr_t) ( /*_.DISCR_STRBUF__V12*/ meltfptr[11]);

  MELT_LOCATION ("warmelt-debug.melt:1509:/ initchunk");
  /*^putroutconst */
  /*putroutconst */
  if (MELT_HAS_INITIAL_ENVIRONMENT)
    melt_assertmsg ("putroutconst checkrout",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.VALROUT___V332*/ meltfptr[331]))
		    == MELTOBMAG_ROUTINE);
  if (MELT_HAS_INITIAL_ENVIRONMENT)
    melt_checkmsg
      ("putroutconst constnull.drout_286__DISPLAY_DEBUG_MESSAGE#1",
       NULL != ( /*_.DISCR_MAP_OBJECTS__V10*/ meltfptr[9]));
  ((meltroutine_ptr_t) /*_.VALROUT___V332*/ meltfptr[331])->tabval[1] =
    (melt_ptr_t) ( /*_.DISCR_MAP_OBJECTS__V10*/ meltfptr[9]);

  MELT_LOCATION ("warmelt-debug.melt:1509:/ initchunk");
  /*^putroutconst */
  /*putroutconst */
  if (MELT_HAS_INITIAL_ENVIRONMENT)
    melt_assertmsg ("putroutconst checkrout",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.VALROUT___V332*/ meltfptr[331]))
		    == MELTOBMAG_ROUTINE);
  if (MELT_HAS_INITIAL_ENVIRONMENT)
    melt_checkmsg
      ("putroutconst constnull.drout_286__DISPLAY_DEBUG_MESSAGE#2",
       NULL != ( /*_.DISCR_INTEGER__V11*/ meltfptr[10]));
  ((meltroutine_ptr_t) /*_.VALROUT___V332*/ meltfptr[331])->tabval[2] =
    (melt_ptr_t) ( /*_.DISCR_INTEGER__V11*/ meltfptr[10]);

  MELT_LOCATION ("warmelt-debug.melt:1509:/ initchunk");
  /*^putroutconstnotnull */
  /*putroutconstnotnull */
  melt_assertmsg ("putroutconstnotnull checkrout",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALROUT___V332*/ meltfptr[331])) ==
		  MELTOBMAG_ROUTINE);
  melt_assertmsg ("putroutconstnotnull notnullconst",
		  NULL != /*_.VALDATA___V69*/ meltfptr[68]);
  ((meltroutine_ptr_t) /*_.VALROUT___V332*/ meltfptr[331])->tabval[3] =
    (melt_ptr_t) ( /*_.VALDATA___V69*/ meltfptr[68]);

  MELT_LOCATION ("warmelt-debug.melt:1509:/ initchunk");
  /*^putroutconstnotnull */
  /*putroutconstnotnull */
  melt_assertmsg ("putroutconstnotnull checkrout",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALROUT___V332*/ meltfptr[331])) ==
		  MELTOBMAG_ROUTINE);
  melt_assertmsg ("putroutconstnotnull notnullconst",
		  NULL != /*_.VALDATA___V118*/ meltfptr[117]);
  ((meltroutine_ptr_t) /*_.VALROUT___V332*/ meltfptr[331])->tabval[4] =
    (melt_ptr_t) ( /*_.VALDATA___V118*/ meltfptr[117]);

  MELT_LOCATION ("warmelt-debug.melt:1509:/ initchunk");
  /*^touch */
  /*touch:IROUTVAL_ */
  meltgc_touch ( /*_.VALROUT___V332*/ meltfptr[331]);

  /*putclosurout#44 */
  melt_assertmsg ("putclosrout#44 checkclo",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALCLO___V333*/ meltfptr[332])) ==
		  MELTOBMAG_CLOSURE);
  melt_assertmsg ("putclosrout#44 checkrout",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALROUT___V332*/ meltfptr[331])) ==
		  MELTOBMAG_ROUTINE);
  ((meltclosure_ptr_t) /*_.VALCLO___V333*/ meltfptr[332])->rout =
    (meltroutine_ptr_t) ( /*_.VALROUT___V332*/ meltfptr[331]);

  /*touch:DATACLOSURE_ */
  meltgc_touch ( /*_.VALCLO___V333*/ meltfptr[332]);

  /*putslot */
  melt_assertmsg ("putslot checkobj",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALDATA___V334*/ meltfptr[333])) ==
		  MELTOBMAG_OBJECT);

  melt_assertmsg ("putslot checkoff",
		  (1 >= 0
		   && 1 <
		   melt_object_length ((melt_ptr_t)
				       ( /*_.VALDATA___V334*/
					meltfptr[333]))));
  ((meltobject_ptr_t) ( /*_.VALDATA___V334*/ meltfptr[333]))->obj_vartab[1] =
    (melt_ptr_t) (
  /*_.VALSTR___V335*/ meltfptr[334]);

  /*touch:VALUEDATA_ */
  meltgc_touch ( /*_.VALDATA___V334*/ meltfptr[333]);


  melt_dbgtrace_written_object ( /*_.VALDATA___V334*/ meltfptr[333],
				"new static instance");

  /*putslot */
  melt_assertmsg ("putslot checkobj",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALDATA___V336*/ meltfptr[335])) ==
		  MELTOBMAG_OBJECT);

  melt_assertmsg ("putslot checkoff",
		  (1 >= 0
		   && 1 <
		   melt_object_length ((melt_ptr_t)
				       ( /*_.VALDATA___V336*/
					meltfptr[335]))));
  ((meltobject_ptr_t) ( /*_.VALDATA___V336*/ meltfptr[335]))->obj_vartab[1] =
    (melt_ptr_t) (
  /*_.VALSTR___V337*/ meltfptr[336]);

  /*touch:VALUEDATA_ */
  meltgc_touch ( /*_.VALDATA___V336*/ meltfptr[335]);


  melt_dbgtrace_written_object ( /*_.VALDATA___V336*/ meltfptr[335],
				"new static instance");

  /*putslot */
  melt_assertmsg ("putslot checkobj",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALDATA___V338*/ meltfptr[337])) ==
		  MELTOBMAG_OBJECT);

  melt_assertmsg ("putslot checkoff",
		  (1 >= 0
		   && 1 <
		   melt_object_length ((melt_ptr_t)
				       ( /*_.VALDATA___V338*/
					meltfptr[337]))));
  ((meltobject_ptr_t) ( /*_.VALDATA___V338*/ meltfptr[337]))->obj_vartab[1] =
    (melt_ptr_t) (
  /*_.VALSTR___V339*/ meltfptr[338]);

  /*touch:VALUEDATA_ */
  meltgc_touch ( /*_.VALDATA___V338*/ meltfptr[337]);


  melt_dbgtrace_written_object ( /*_.VALDATA___V338*/ meltfptr[337],
				"new static instance");

  /*putslot */
  melt_assertmsg ("putslot checkobj",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALDATA___V340*/ meltfptr[339])) ==
		  MELTOBMAG_OBJECT);

  melt_assertmsg ("putslot checkoff",
		  (1 >= 0
		   && 1 <
		   melt_object_length ((melt_ptr_t)
				       ( /*_.VALDATA___V340*/
					meltfptr[339]))));
  ((meltobject_ptr_t) ( /*_.VALDATA___V340*/ meltfptr[339]))->obj_vartab[1] =
    (melt_ptr_t) (
  /*_.VALSTR___V341*/ meltfptr[340]);

  /*touch:VALUEDATA_ */
  meltgc_touch ( /*_.VALDATA___V340*/ meltfptr[339]);


  melt_dbgtrace_written_object ( /*_.VALDATA___V340*/ meltfptr[339],
				"new static instance");

  /*putslot */
  melt_assertmsg ("putslot checkobj",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALDATA___V342*/ meltfptr[341])) ==
		  MELTOBMAG_OBJECT);

  melt_assertmsg ("putslot checkoff",
		  (1 >= 0
		   && 1 <
		   melt_object_length ((melt_ptr_t)
				       ( /*_.VALDATA___V342*/
					meltfptr[341]))));
  ((meltobject_ptr_t) ( /*_.VALDATA___V342*/ meltfptr[341]))->obj_vartab[1] =
    (melt_ptr_t) (
  /*_.VALSTR___V343*/ meltfptr[342]);

  /*touch:VALUEDATA_ */
  meltgc_touch ( /*_.VALDATA___V342*/ meltfptr[341]);


  melt_dbgtrace_written_object ( /*_.VALDATA___V342*/ meltfptr[341],
				"new static instance");

  /*putslot */
  melt_assertmsg ("putslot checkobj",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALDATA___V344*/ meltfptr[343])) ==
		  MELTOBMAG_OBJECT);

  melt_assertmsg ("putslot checkoff",
		  (1 >= 0
		   && 1 <
		   melt_object_length ((melt_ptr_t)
				       ( /*_.VALDATA___V344*/
					meltfptr[343]))));
  ((meltobject_ptr_t) ( /*_.VALDATA___V344*/ meltfptr[343]))->obj_vartab[1] =
    (melt_ptr_t) (
  /*_.VALSTR___V345*/ meltfptr[344]);

  /*touch:VALUEDATA_ */
  meltgc_touch ( /*_.VALDATA___V344*/ meltfptr[343]);


  melt_dbgtrace_written_object ( /*_.VALDATA___V344*/ meltfptr[343],
				"new static instance");

  /*putslot */
  melt_assertmsg ("putslot checkobj",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALDATA___V346*/ meltfptr[345])) ==
		  MELTOBMAG_OBJECT);

  melt_assertmsg ("putslot checkoff",
		  (1 >= 0
		   && 1 <
		   melt_object_length ((melt_ptr_t)
				       ( /*_.VALDATA___V346*/
					meltfptr[345]))));
  ((meltobject_ptr_t) ( /*_.VALDATA___V346*/ meltfptr[345]))->obj_vartab[1] =
    (melt_ptr_t) (
  /*_.VALSTR___V347*/ meltfptr[346]);

  /*touch:VALUEDATA_ */
  meltgc_touch ( /*_.VALDATA___V346*/ meltfptr[345]);


  melt_dbgtrace_written_object ( /*_.VALDATA___V346*/ meltfptr[345],
				"new static instance");

  /*putslot */
  melt_assertmsg ("putslot checkobj",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALDATA___V348*/ meltfptr[347])) ==
		  MELTOBMAG_OBJECT);

  melt_assertmsg ("putslot checkoff",
		  (1 >= 0
		   && 1 <
		   melt_object_length ((melt_ptr_t)
				       ( /*_.VALDATA___V348*/
					meltfptr[347]))));
  ((meltobject_ptr_t) ( /*_.VALDATA___V348*/ meltfptr[347]))->obj_vartab[1] =
    (melt_ptr_t) (
  /*_.VALSTR___V349*/ meltfptr[348]);

  /*touch:VALUEDATA_ */
  meltgc_touch ( /*_.VALDATA___V348*/ meltfptr[347]);


  melt_dbgtrace_written_object ( /*_.VALDATA___V348*/ meltfptr[347],
				"new static instance");

  /*putslot */
  melt_assertmsg ("putslot checkobj",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALDATA___V350*/ meltfptr[349])) ==
		  MELTOBMAG_OBJECT);

  melt_assertmsg ("putslot checkoff",
		  (1 >= 0
		   && 1 <
		   melt_object_length ((melt_ptr_t)
				       ( /*_.VALDATA___V350*/
					meltfptr[349]))));
  ((meltobject_ptr_t) ( /*_.VALDATA___V350*/ meltfptr[349]))->obj_vartab[1] =
    (melt_ptr_t) (
  /*_.VALSTR___V351*/ meltfptr[350]);

  /*touch:VALUEDATA_ */
  meltgc_touch ( /*_.VALDATA___V350*/ meltfptr[349]);


  melt_dbgtrace_written_object ( /*_.VALDATA___V350*/ meltfptr[349],
				"new static instance");

  /*putslot */
  melt_assertmsg ("putslot checkobj",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALDATA___V352*/ meltfptr[351])) ==
		  MELTOBMAG_OBJECT);

  melt_assertmsg ("putslot checkoff",
		  (1 >= 0
		   && 1 <
		   melt_object_length ((melt_ptr_t)
				       ( /*_.VALDATA___V352*/
					meltfptr[351]))));
  ((meltobject_ptr_t) ( /*_.VALDATA___V352*/ meltfptr[351]))->obj_vartab[1] =
    (melt_ptr_t) (
  /*_.VALSTR___V353*/ meltfptr[352]);

  /*touch:VALUEDATA_ */
  meltgc_touch ( /*_.VALDATA___V352*/ meltfptr[351]);


  melt_dbgtrace_written_object ( /*_.VALDATA___V352*/ meltfptr[351],
				"new static instance");

  /*putslot */
  melt_assertmsg ("putslot checkobj",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALDATA___V354*/ meltfptr[353])) ==
		  MELTOBMAG_OBJECT);

  melt_assertmsg ("putslot checkoff",
		  (1 >= 0
		   && 1 <
		   melt_object_length ((melt_ptr_t)
				       ( /*_.VALDATA___V354*/
					meltfptr[353]))));
  ((meltobject_ptr_t) ( /*_.VALDATA___V354*/ meltfptr[353]))->obj_vartab[1] =
    (melt_ptr_t) (
  /*_.VALSTR___V355*/ meltfptr[354]);

  /*touch:VALUEDATA_ */
  meltgc_touch ( /*_.VALDATA___V354*/ meltfptr[353]);


  melt_dbgtrace_written_object ( /*_.VALDATA___V354*/ meltfptr[353],
				"new static instance");

  /*putslot */
  melt_assertmsg ("putslot checkobj",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALDATA___V356*/ meltfptr[355])) ==
		  MELTOBMAG_OBJECT);

  melt_assertmsg ("putslot checkoff",
		  (1 >= 0
		   && 1 <
		   melt_object_length ((melt_ptr_t)
				       ( /*_.VALDATA___V356*/
					meltfptr[355]))));
  ((meltobject_ptr_t) ( /*_.VALDATA___V356*/ meltfptr[355]))->obj_vartab[1] =
    (melt_ptr_t) (
  /*_.VALSTR___V357*/ meltfptr[356]);

  /*touch:VALUEDATA_ */
  meltgc_touch ( /*_.VALDATA___V356*/ meltfptr[355]);


  melt_dbgtrace_written_object ( /*_.VALDATA___V356*/ meltfptr[355],
				"new static instance");

  /*putslot */
  melt_assertmsg ("putslot checkobj",
		  melt_magic_discr ((melt_ptr_t)
				    ( /*_.VALDATA___V358*/ meltfptr[357])) ==
		  MELTOBMAG_OBJECT);

  melt_assertmsg ("putslot checkoff",
		  (1 >= 0
		   && 1 <
		   melt_object_length ((melt_ptr_t)
				       ( /*_.VALDATA___V358*/
					meltfptr[357]))));
  ((meltobject_ptr_t) ( /*_.VALDATA___V358*/ meltfptr[357]))->obj_vartab[1] =
    (melt_ptr_t) (
  /*_.VALSTR___V359*/ meltfptr[358]);

  /*touch:VALUEDATA_ */
  meltgc_touch ( /*_.VALDATA___V358*/ meltfptr[357]);


  melt_dbgtrace_written_object ( /*_.VALDATA___V358*/ meltfptr[357],
				"new static instance");

 /**COMMENT: before toplevel body **/ ;

  MELT_LOCATION ("warmelt-debug.melt:4:/ initchunk");
  /*^block */
  /*anyblock */
  {

    /*^cond */
    /*cond */ if (
		   /*quasi.cur.mod.env.cont cur.mod.env.cont : at very start */
							/*_.CONTENV___V2*/ meltfptr[1])
							/*then */
      {
	/*^cond.then */
  /*_.OR___V360*/ meltfptr[359] =
	  /*quasi.cur.mod.env.cont cur.mod.env.cont : at very start */
/*_.CONTENV___V2*/ meltfptr[1];;
      }
    else
      {
	MELT_LOCATION ("warmelt-debug.melt:4:/ cond.else");

	/*^block */
	/*anyblock */
	{

	  /*^getslot */
	  {
	    melt_ptr_t slot = NULL, obj = NULL;
	    obj =
	      (melt_ptr_t) (((melt_ptr_t)
			     (MELT_PREDEF (INITIAL_SYSTEM_DATA)))) /*=obj*/ ;
	    melt_object_get_field (slot, obj, 3, "SYSDATA_CONT_FRESH_ENV");
     /*_.SYSDATA_CONT_FRESH_ENV__V361*/ meltfptr[360] = slot;
	  };
	  ;
	  /*^checksignal */
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^apply */
	  /*apply */
	  {
	    union meltparam_un argtab[1];
	    memset (&argtab, 0, sizeof (argtab));
	    /*^apply.arg */
	    argtab[0].meltbp_cstring = "warmelt-debug";
	    /*_.FUN___V362*/ meltfptr[361] =
	      melt_apply ((meltclosure_ptr_t)
			  ( /*_.SYSDATA_CONT_FRESH_ENV__V361*/ meltfptr[360]),
			  (melt_ptr_t) ( /*_.PREVENV___V4*/ meltfptr[3]),
			  (MELTBPARSTR_CSTRING ""), argtab, "",
			  (union meltparam_un *) 0);
	  }
	  ;
	  /*_.OR___V360*/ meltfptr[359] = /*_.FUN___V362*/ meltfptr[361];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-debug.melt:4:/ clear");
	      /*clear *//*_.SYSDATA_CONT_FRESH_ENV__V361*/ meltfptr[360] = 0;
	  /*^clear */
	      /*clear *//*_.FUN___V362*/ meltfptr[361] = 0;
	}
	;
      }
    ;
    /*^block */
    /*com.block:upd.cur.mod.env.cont : at very start */
    {
    }				/*com.end block:upd.cur.mod.env.cont : at very start */
    ;
    /*epilog */

    /*^clear */
	    /*clear *//*_.OR___V360*/ meltfptr[359] = 0;
  }

  MELT_LOCATION ("warmelt-debug.melt:58:/ initchunk");
  /*^block */
  /*anyblock */
  {

    /*^getslot */
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj =
	(melt_ptr_t) (((melt_ptr_t) (MELT_PREDEF (INITIAL_SYSTEM_DATA))))
	/*=obj*/ ;
      melt_object_get_field (slot, obj, 4, "SYSDATA_VALUE_EXPORTER");
   /*_.VALUEXPORTER___V363*/ meltfptr[360] = slot;
    };
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[2];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & /*_.VALDATA___V51*/ meltfptr[50];
      /*^apply.arg */
      argtab[1].meltbp_aptr = (melt_ptr_t *) &
	/*quasi.cur.mod.env.cont norm.exp.val : CLASS_DEBUG_INFORMATION */
/*_.CONTENV___V2*/ meltfptr[1];
      melt_apply ((meltclosure_ptr_t)
		  ( /*_.VALUEXPORTER___V363*/ meltfptr[360]),
		  (melt_ptr_t) ( /*_.VALDATA___V47*/ meltfptr[46]),
		  (MELTBPARSTR_PTR MELTBPARSTR_PTR ""), argtab, "",
		  (union meltparam_un *) 0);
    }
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[2];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & /*_.VALDATA___V54*/ meltfptr[53];
      /*^apply.arg */
      argtab[1].meltbp_aptr = (melt_ptr_t *) &
	/*quasi.cur.mod.env.cont norm.exp.val : DBGI_OUT */
/*_.CONTENV___V2*/ meltfptr[1];
      melt_apply ((meltclosure_ptr_t)
		  ( /*_.VALUEXPORTER___V363*/ meltfptr[360]),
		  (melt_ptr_t) ( /*_.VALDATA___V61*/ meltfptr[60]),
		  (MELTBPARSTR_PTR MELTBPARSTR_PTR ""), argtab, "",
		  (union meltparam_un *) 0);
    }
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[2];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & /*_.VALDATA___V56*/ meltfptr[55];
      /*^apply.arg */
      argtab[1].meltbp_aptr = (melt_ptr_t *) &
	/*quasi.cur.mod.env.cont norm.exp.val : DBGI_OCCMAP */
/*_.CONTENV___V2*/ meltfptr[1];
      melt_apply ((meltclosure_ptr_t)
		  ( /*_.VALUEXPORTER___V363*/ meltfptr[360]),
		  (melt_ptr_t) ( /*_.VALDATA___V63*/ meltfptr[62]),
		  (MELTBPARSTR_PTR MELTBPARSTR_PTR ""), argtab, "",
		  (union meltparam_un *) 0);
    }
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[2];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & /*_.VALDATA___V58*/ meltfptr[57];
      /*^apply.arg */
      argtab[1].meltbp_aptr = (melt_ptr_t *) &
	/*quasi.cur.mod.env.cont norm.exp.val : DBGI_MAXDEPTH */
/*_.CONTENV___V2*/ meltfptr[1];
      melt_apply ((meltclosure_ptr_t)
		  ( /*_.VALUEXPORTER___V363*/ meltfptr[360]),
		  (melt_ptr_t) ( /*_.VALDATA___V65*/ meltfptr[64]),
		  (MELTBPARSTR_PTR MELTBPARSTR_PTR ""), argtab, "",
		  (union meltparam_un *) 0);
    }
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[2];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & /*_.VALDATA___V69*/ meltfptr[68];
      /*^apply.arg */
      argtab[1].meltbp_aptr = (melt_ptr_t *) &
	/*quasi.cur.mod.env.cont norm.exp.val : CLASS_DEBUG_OUTPUT_INFORMATION */
/*_.CONTENV___V2*/ meltfptr[1];
      melt_apply ((meltclosure_ptr_t)
		  ( /*_.VALUEXPORTER___V363*/ meltfptr[360]),
		  (melt_ptr_t) ( /*_.VALDATA___V67*/ meltfptr[66]),
		  (MELTBPARSTR_PTR MELTBPARSTR_PTR ""), argtab, "",
		  (union meltparam_un *) 0);
    }
    ;
    /*epilog */

    /*^clear */
	    /*clear *//*_.VALUEXPORTER___V363*/ meltfptr[360] = 0;
  }

  MELT_LOCATION ("warmelt-debug.melt:605:/ initchunk");
  /*^block */
  /*anyblock */
  {

    /*^checksignal */
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[2];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & /*_.VALDATA___V118*/ meltfptr[117];
      /*^apply.arg */
      argtab[1].meltbp_aptr =
	(melt_ptr_t *) & /*_.VALCLO___V204*/ meltfptr[203];
      /*_.INSTALL_METHOD__V364*/ meltfptr[361] =
	melt_apply ((meltclosure_ptr_t)
		    ( /*_.INSTALL_METHOD__V21*/ meltfptr[20]),
		    (melt_ptr_t) ( /*_.DISCR_NULL_RECEIVER__V22*/
				  meltfptr[21]),
		    (MELTBPARSTR_PTR MELTBPARSTR_PTR ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;
    /*epilog */

    /*^clear */
	    /*clear *//*_.INSTALL_METHOD__V364*/ meltfptr[361] = 0;
  }

  MELT_LOCATION ("warmelt-debug.melt:606:/ initchunk");
  /*^block */
  /*anyblock */
  {

    /*^checksignal */
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[2];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & /*_.VALDATA___V116*/ meltfptr[115];
      /*^apply.arg */
      argtab[1].meltbp_aptr =
	(melt_ptr_t *) & /*_.VALCLO___V204*/ meltfptr[203];
      /*_.INSTALL_METHOD__V365*/ meltfptr[359] =
	melt_apply ((meltclosure_ptr_t)
		    ( /*_.INSTALL_METHOD__V21*/ meltfptr[20]),
		    (melt_ptr_t) ( /*_.DISCR_NULL_RECEIVER__V22*/
				  meltfptr[21]),
		    (MELTBPARSTR_PTR MELTBPARSTR_PTR ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;
    /*epilog */

    /*^clear */
	    /*clear *//*_.INSTALL_METHOD__V365*/ meltfptr[359] = 0;
  }

  MELT_LOCATION ("warmelt-debug.melt:628:/ initchunk");
  /*^block */
  /*anyblock */
  {

    /*^checksignal */
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[2];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & /*_.VALDATA___V118*/ meltfptr[117];
      /*^apply.arg */
      argtab[1].meltbp_aptr =
	(melt_ptr_t *) & /*_.VALCLO___V210*/ meltfptr[209];
      /*_.INSTALL_METHOD__V366*/ meltfptr[360] =
	melt_apply ((meltclosure_ptr_t)
		    ( /*_.INSTALL_METHOD__V21*/ meltfptr[20]),
		    (melt_ptr_t) ( /*_.DISCR_STRING__V23*/ meltfptr[22]),
		    (MELTBPARSTR_PTR MELTBPARSTR_PTR ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;
    /*epilog */

    /*^clear */
	    /*clear *//*_.INSTALL_METHOD__V366*/ meltfptr[360] = 0;
  }

  MELT_LOCATION ("warmelt-debug.melt:711:/ initchunk");
  /*^block */
  /*anyblock */
  {

    /*^checksignal */
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[2];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & /*_.VALDATA___V118*/ meltfptr[117];
      /*^apply.arg */
      argtab[1].meltbp_aptr =
	(melt_ptr_t *) & /*_.VALCLO___V214*/ meltfptr[213];
      /*_.INSTALL_METHOD__V367*/ meltfptr[361] =
	melt_apply ((meltclosure_ptr_t)
		    ( /*_.INSTALL_METHOD__V21*/ meltfptr[20]),
		    (melt_ptr_t) ( /*_.DISCR_STRBUF__V12*/ meltfptr[11]),
		    (MELTBPARSTR_PTR MELTBPARSTR_PTR ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;
    /*epilog */

    /*^clear */
	    /*clear *//*_.INSTALL_METHOD__V367*/ meltfptr[361] = 0;
  }

  MELT_LOCATION ("warmelt-debug.melt:735:/ initchunk");
  /*^block */
  /*anyblock */
  {

    /*^checksignal */
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[2];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & /*_.VALDATA___V118*/ meltfptr[117];
      /*^apply.arg */
      argtab[1].meltbp_aptr =
	(melt_ptr_t *) & /*_.VALCLO___V216*/ meltfptr[215];
      /*_.INSTALL_METHOD__V368*/ meltfptr[359] =
	melt_apply ((meltclosure_ptr_t)
		    ( /*_.INSTALL_METHOD__V21*/ meltfptr[20]),
		    (melt_ptr_t) ( /*_.DISCR_INTEGER__V11*/ meltfptr[10]),
		    (MELTBPARSTR_PTR MELTBPARSTR_PTR ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;
    /*epilog */

    /*^clear */
	    /*clear *//*_.INSTALL_METHOD__V368*/ meltfptr[359] = 0;
  }

  MELT_LOCATION ("warmelt-debug.melt:765:/ initchunk");
  /*^block */
  /*anyblock */
  {

    /*^checksignal */
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[2];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & /*_.VALDATA___V118*/ meltfptr[117];
      /*^apply.arg */
      argtab[1].meltbp_aptr =
	(melt_ptr_t *) & /*_.VALCLO___V220*/ meltfptr[219];
      /*_.INSTALL_METHOD__V369*/ meltfptr[360] =
	melt_apply ((meltclosure_ptr_t)
		    ( /*_.INSTALL_METHOD__V21*/ meltfptr[20]),
		    (melt_ptr_t) ( /*_.DISCR_MIXED_INTEGER__V25*/
				  meltfptr[24]),
		    (MELTBPARSTR_PTR MELTBPARSTR_PTR ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;
    /*epilog */

    /*^clear */
	    /*clear *//*_.INSTALL_METHOD__V369*/ meltfptr[360] = 0;
  }

  MELT_LOCATION ("warmelt-debug.melt:796:/ initchunk");
  /*^block */
  /*anyblock */
  {

    /*^checksignal */
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[2];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & /*_.VALDATA___V118*/ meltfptr[117];
      /*^apply.arg */
      argtab[1].meltbp_aptr =
	(melt_ptr_t *) & /*_.VALCLO___V224*/ meltfptr[223];
      /*_.INSTALL_METHOD__V370*/ meltfptr[361] =
	melt_apply ((meltclosure_ptr_t)
		    ( /*_.INSTALL_METHOD__V21*/ meltfptr[20]),
		    (melt_ptr_t) ( /*_.DISCR_MIXED_LOCATION__V26*/
				  meltfptr[25]),
		    (MELTBPARSTR_PTR MELTBPARSTR_PTR ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;
    /*epilog */

    /*^clear */
	    /*clear *//*_.INSTALL_METHOD__V370*/ meltfptr[361] = 0;
  }

  MELT_LOCATION ("warmelt-debug.melt:822:/ initchunk");
  /*^block */
  /*anyblock */
  {

    /*^checksignal */
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[2];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & /*_.VALDATA___V118*/ meltfptr[117];
      /*^apply.arg */
      argtab[1].meltbp_aptr =
	(melt_ptr_t *) & /*_.VALCLO___V228*/ meltfptr[227];
      /*_.INSTALL_METHOD__V371*/ meltfptr[359] =
	melt_apply ((meltclosure_ptr_t)
		    ( /*_.INSTALL_METHOD__V21*/ meltfptr[20]),
		    (melt_ptr_t) ( /*_.DISCR_MIXED_BIGINT__V27*/
				  meltfptr[26]),
		    (MELTBPARSTR_PTR MELTBPARSTR_PTR ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;
    /*epilog */

    /*^clear */
	    /*clear *//*_.INSTALL_METHOD__V371*/ meltfptr[359] = 0;
  }

  MELT_LOCATION ("warmelt-debug.melt:855:/ initchunk");
  /*^block */
  /*anyblock */
  {

    /*^checksignal */
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[2];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & /*_.VALDATA___V118*/ meltfptr[117];
      /*^apply.arg */
      argtab[1].meltbp_aptr =
	(melt_ptr_t *) & /*_.VALCLO___V232*/ meltfptr[231];
      /*_.INSTALL_METHOD__V372*/ meltfptr[360] =
	melt_apply ((meltclosure_ptr_t)
		    ( /*_.INSTALL_METHOD__V21*/ meltfptr[20]),
		    (melt_ptr_t) ( /*_.DISCR_MULTIPLE__V28*/ meltfptr[27]),
		    (MELTBPARSTR_PTR MELTBPARSTR_PTR ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;
    /*epilog */

    /*^clear */
	    /*clear *//*_.INSTALL_METHOD__V372*/ meltfptr[360] = 0;
  }

  MELT_LOCATION ("warmelt-debug.melt:882:/ initchunk");
  /*^block */
  /*anyblock */
  {

    /*^checksignal */
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[2];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & /*_.VALDATA___V118*/ meltfptr[117];
      /*^apply.arg */
      argtab[1].meltbp_aptr =
	(melt_ptr_t *) & /*_.VALCLO___V237*/ meltfptr[236];
      /*_.INSTALL_METHOD__V373*/ meltfptr[361] =
	melt_apply ((meltclosure_ptr_t)
		    ( /*_.INSTALL_METHOD__V21*/ meltfptr[20]),
		    (melt_ptr_t) ( /*_.DISCR_ROUTINE__V29*/ meltfptr[28]),
		    (MELTBPARSTR_PTR MELTBPARSTR_PTR ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;
    /*epilog */

    /*^clear */
	    /*clear *//*_.INSTALL_METHOD__V373*/ meltfptr[361] = 0;
  }

  MELT_LOCATION ("warmelt-debug.melt:925:/ initchunk");
  /*^block */
  /*anyblock */
  {

    /*^checksignal */
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[2];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & /*_.VALDATA___V118*/ meltfptr[117];
      /*^apply.arg */
      argtab[1].meltbp_aptr =
	(melt_ptr_t *) & /*_.VALCLO___V243*/ meltfptr[242];
      /*_.INSTALL_METHOD__V374*/ meltfptr[359] =
	melt_apply ((meltclosure_ptr_t)
		    ( /*_.INSTALL_METHOD__V21*/ meltfptr[20]),
		    (melt_ptr_t) ( /*_.DISCR_CLOSURE__V16*/ meltfptr[15]),
		    (MELTBPARSTR_PTR MELTBPARSTR_PTR ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;
    /*epilog */

    /*^clear */
	    /*clear *//*_.INSTALL_METHOD__V374*/ meltfptr[359] = 0;
  }

  MELT_LOCATION ("warmelt-debug.melt:970:/ initchunk");
  /*^block */
  /*anyblock */
  {

    /*^checksignal */
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[2];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & /*_.VALDATA___V118*/ meltfptr[117];
      /*^apply.arg */
      argtab[1].meltbp_aptr =
	(melt_ptr_t *) & /*_.VALCLO___V245*/ meltfptr[244];
      /*_.INSTALL_METHOD__V375*/ meltfptr[360] =
	melt_apply ((meltclosure_ptr_t)
		    ( /*_.INSTALL_METHOD__V21*/ meltfptr[20]),
		    (melt_ptr_t) ( /*_.DISCR_LIST__V31*/ meltfptr[30]),
		    (MELTBPARSTR_PTR MELTBPARSTR_PTR ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;
    /*epilog */

    /*^clear */
	    /*clear *//*_.INSTALL_METHOD__V375*/ meltfptr[360] = 0;
  }

  MELT_LOCATION ("warmelt-debug.melt:993:/ initchunk");
  /*^block */
  /*anyblock */
  {

    /*^checksignal */
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[2];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & /*_.VALDATA___V118*/ meltfptr[117];
      /*^apply.arg */
      argtab[1].meltbp_aptr =
	(melt_ptr_t *) & /*_.VALCLO___V249*/ meltfptr[248];
      /*_.INSTALL_METHOD__V376*/ meltfptr[361] =
	melt_apply ((meltclosure_ptr_t)
		    ( /*_.INSTALL_METHOD__V21*/ meltfptr[20]),
		    (melt_ptr_t) ( /*_.DISCR_PAIR__V32*/ meltfptr[31]),
		    (MELTBPARSTR_PTR MELTBPARSTR_PTR ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;
    /*epilog */

    /*^clear */
	    /*clear *//*_.INSTALL_METHOD__V376*/ meltfptr[361] = 0;
  }

  MELT_LOCATION ("warmelt-debug.melt:1082:/ initchunk");
  /*^block */
  /*anyblock */
  {

    /*^checksignal */
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[2];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & /*_.VALDATA___V118*/ meltfptr[117];
      /*^apply.arg */
      argtab[1].meltbp_aptr =
	(melt_ptr_t *) & /*_.VALCLO___V259*/ meltfptr[258];
      /*_.INSTALL_METHOD__V377*/ meltfptr[359] =
	melt_apply ((meltclosure_ptr_t)
		    ( /*_.INSTALL_METHOD__V21*/ meltfptr[20]),
		    (melt_ptr_t) ( /*_.DISCR_MAP_OBJECTS__V10*/ meltfptr[9]),
		    (MELTBPARSTR_PTR MELTBPARSTR_PTR ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;
    /*epilog */

    /*^clear */
	    /*clear *//*_.INSTALL_METHOD__V377*/ meltfptr[359] = 0;
  }

  MELT_LOCATION ("warmelt-debug.melt:1165:/ initchunk");
  /*^block */
  /*anyblock */
  {

    /*^checksignal */
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[2];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & /*_.VALDATA___V118*/ meltfptr[117];
      /*^apply.arg */
      argtab[1].meltbp_aptr =
	(melt_ptr_t *) & /*_.VALCLO___V273*/ meltfptr[272];
      /*_.INSTALL_METHOD__V378*/ meltfptr[360] =
	melt_apply ((meltclosure_ptr_t)
		    ( /*_.INSTALL_METHOD__V21*/ meltfptr[20]),
		    (melt_ptr_t) ( /*_.DISCR_MAP_STRINGS__V36*/ meltfptr[35]),
		    (MELTBPARSTR_PTR MELTBPARSTR_PTR ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;
    /*epilog */

    /*^clear */
	    /*clear *//*_.INSTALL_METHOD__V378*/ meltfptr[360] = 0;
  }

  MELT_LOCATION ("warmelt-debug.melt:1203:/ initchunk");
  /*^block */
  /*anyblock */
  {

    /*^checksignal */
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[2];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & /*_.VALDATA___V118*/ meltfptr[117];
      /*^apply.arg */
      argtab[1].meltbp_aptr =
	(melt_ptr_t *) & /*_.VALCLO___V279*/ meltfptr[278];
      /*_.INSTALL_METHOD__V379*/ meltfptr[361] =
	melt_apply ((meltclosure_ptr_t)
		    ( /*_.INSTALL_METHOD__V21*/ meltfptr[20]),
		    (melt_ptr_t) ( /*_.DISCR_BUCKET_LONGS__V38*/
				  meltfptr[37]),
		    (MELTBPARSTR_PTR MELTBPARSTR_PTR ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;
    /*epilog */

    /*^clear */
	    /*clear *//*_.INSTALL_METHOD__V379*/ meltfptr[361] = 0;
  }

  MELT_LOCATION ("warmelt-debug.melt:1232:/ initchunk");
  /*^block */
  /*anyblock */
  {

    /*^checksignal */
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[2];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & /*_.VALDATA___V118*/ meltfptr[117];
      /*^apply.arg */
      argtab[1].meltbp_aptr =
	(melt_ptr_t *) & /*_.VALCLO___V283*/ meltfptr[282];
      /*_.INSTALL_METHOD__V380*/ meltfptr[359] =
	melt_apply ((meltclosure_ptr_t)
		    ( /*_.INSTALL_METHOD__V21*/ meltfptr[20]),
		    (melt_ptr_t) ( /*_.CLASS_ROOT__V7*/ meltfptr[6]),
		    (MELTBPARSTR_PTR MELTBPARSTR_PTR ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;
    /*epilog */

    /*^clear */
	    /*clear *//*_.INSTALL_METHOD__V380*/ meltfptr[359] = 0;
  }

  MELT_LOCATION ("warmelt-debug.melt:1243:/ initchunk");
  /*^block */
  /*anyblock */
  {

    /*^checksignal */
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[2];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & /*_.VALDATA___V118*/ meltfptr[117];
      /*^apply.arg */
      argtab[1].meltbp_aptr =
	(melt_ptr_t *) & /*_.VALCLO___V285*/ meltfptr[284];
      /*_.INSTALL_METHOD__V381*/ meltfptr[360] =
	melt_apply ((meltclosure_ptr_t)
		    ( /*_.INSTALL_METHOD__V21*/ meltfptr[20]),
		    (melt_ptr_t) ( /*_.DISCR_ANY_RECEIVER__V39*/
				  meltfptr[38]),
		    (MELTBPARSTR_PTR MELTBPARSTR_PTR ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;
    /*epilog */

    /*^clear */
	    /*clear *//*_.INSTALL_METHOD__V381*/ meltfptr[360] = 0;
  }

  MELT_LOCATION ("warmelt-debug.melt:1261:/ initchunk");
  /*^block */
  /*anyblock */
  {

    /*^checksignal */
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[2];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & /*_.VALDATA___V116*/ meltfptr[115];
      /*^apply.arg */
      argtab[1].meltbp_aptr =
	(melt_ptr_t *) & /*_.VALCLO___V289*/ meltfptr[288];
      /*_.INSTALL_METHOD__V382*/ meltfptr[361] =
	melt_apply ((meltclosure_ptr_t)
		    ( /*_.INSTALL_METHOD__V21*/ meltfptr[20]),
		    (melt_ptr_t) ( /*_.CLASS_ROOT__V7*/ meltfptr[6]),
		    (MELTBPARSTR_PTR MELTBPARSTR_PTR ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;
    /*epilog */

    /*^clear */
	    /*clear *//*_.INSTALL_METHOD__V382*/ meltfptr[361] = 0;
  }

  MELT_LOCATION ("warmelt-debug.melt:1297:/ initchunk");
  /*^block */
  /*anyblock */
  {

    /*^checksignal */
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[2];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & /*_.VALDATA___V118*/ meltfptr[117];
      /*^apply.arg */
      argtab[1].meltbp_aptr =
	(melt_ptr_t *) & /*_.VALCLO___V291*/ meltfptr[290];
      /*_.INSTALL_METHOD__V383*/ meltfptr[359] =
	melt_apply ((meltclosure_ptr_t)
		    ( /*_.INSTALL_METHOD__V21*/ meltfptr[20]),
		    (melt_ptr_t) ( /*_.CLASS_NAMED__V19*/ meltfptr[18]),
		    (MELTBPARSTR_PTR MELTBPARSTR_PTR ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;
    /*epilog */

    /*^clear */
	    /*clear *//*_.INSTALL_METHOD__V383*/ meltfptr[359] = 0;
  }

  MELT_LOCATION ("warmelt-debug.melt:1299:/ initchunk");
  /*^block */
  /*anyblock */
  {

    /*^getslot */
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj =
	(melt_ptr_t) (((melt_ptr_t) (MELT_PREDEF (INITIAL_SYSTEM_DATA))))
	/*=obj*/ ;
      melt_object_get_field (slot, obj, 4, "SYSDATA_VALUE_EXPORTER");
   /*_.VALUEXPORTER___V384*/ meltfptr[360] = slot;
    };
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[2];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & /*_.VALCLO___V291*/ meltfptr[290];
      /*^apply.arg */
      argtab[1].meltbp_aptr = (melt_ptr_t *) &
	/*quasi.cur.mod.env.cont norm.exp.val : DBGOUT_NAMEDOBJECT_METHOD */
/*_.CONTENV___V2*/ meltfptr[1];
      melt_apply ((meltclosure_ptr_t)
		  ( /*_.VALUEXPORTER___V384*/ meltfptr[360]),
		  (melt_ptr_t) ( /*_.VALDATA___V292*/ meltfptr[291]),
		  (MELTBPARSTR_PTR MELTBPARSTR_PTR ""), argtab, "",
		  (union meltparam_un *) 0);
    }
    ;
    /*epilog */

    /*^clear */
	    /*clear *//*_.VALUEXPORTER___V384*/ meltfptr[360] = 0;
  }

  MELT_LOCATION ("warmelt-debug.melt:1314:/ initchunk");
  /*^block */
  /*anyblock */
  {

    /*^checksignal */
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[2];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & /*_.VALDATA___V116*/ meltfptr[115];
      /*^apply.arg */
      argtab[1].meltbp_aptr =
	(melt_ptr_t *) & /*_.VALCLO___V295*/ meltfptr[294];
      /*_.INSTALL_METHOD__V385*/ meltfptr[361] =
	melt_apply ((meltclosure_ptr_t)
		    ( /*_.INSTALL_METHOD__V21*/ meltfptr[20]),
		    (melt_ptr_t) ( /*_.CLASS_NAMED__V19*/ meltfptr[18]),
		    (MELTBPARSTR_PTR MELTBPARSTR_PTR ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;
    /*epilog */

    /*^clear */
	    /*clear *//*_.INSTALL_METHOD__V385*/ meltfptr[361] = 0;
  }

  MELT_LOCATION ("warmelt-debug.melt:1334:/ initchunk");
  /*^block */
  /*anyblock */
  {

    /*^checksignal */
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[2];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & /*_.VALDATA___V116*/ meltfptr[115];
      /*^apply.arg */
      argtab[1].meltbp_aptr =
	(melt_ptr_t *) & /*_.VALCLO___V297*/ meltfptr[296];
      /*_.INSTALL_METHOD__V386*/ meltfptr[359] =
	melt_apply ((meltclosure_ptr_t)
		    ( /*_.INSTALL_METHOD__V21*/ meltfptr[20]),
		    (melt_ptr_t) ( /*_.CLASS_SYMBOL__V40*/ meltfptr[39]),
		    (MELTBPARSTR_PTR MELTBPARSTR_PTR ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;
    /*epilog */

    /*^clear */
	    /*clear *//*_.INSTALL_METHOD__V386*/ meltfptr[359] = 0;
  }

  MELT_LOCATION ("warmelt-debug.melt:1342:/ initchunk");
  /*^block */
  /*anyblock */
  {

    /*^checksignal */
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[2];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & /*_.VALDATA___V118*/ meltfptr[117];
      /*^apply.arg */
      argtab[1].meltbp_aptr =
	(melt_ptr_t *) & /*_.VALCLO___V301*/ meltfptr[300];
      /*_.INSTALL_METHOD__V387*/ meltfptr[360] =
	melt_apply ((meltclosure_ptr_t)
		    ( /*_.INSTALL_METHOD__V21*/ meltfptr[20]),
		    (melt_ptr_t) ( /*_.CLASS_SYMBOL__V40*/ meltfptr[39]),
		    (MELTBPARSTR_PTR MELTBPARSTR_PTR ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;
    /*epilog */

    /*^clear */
	    /*clear *//*_.INSTALL_METHOD__V387*/ meltfptr[360] = 0;
  }

  MELT_LOCATION ("warmelt-debug.melt:1352:/ initchunk");
  /*^block */
  /*anyblock */
  {

    /*^checksignal */
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[2];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & /*_.VALDATA___V118*/ meltfptr[117];
      /*^apply.arg */
      argtab[1].meltbp_aptr =
	(melt_ptr_t *) & /*_.VALCLO___V303*/ meltfptr[302];
      /*_.INSTALL_METHOD__V388*/ meltfptr[361] =
	melt_apply ((meltclosure_ptr_t)
		    ( /*_.INSTALL_METHOD__V21*/ meltfptr[20]),
		    (melt_ptr_t) ( /*_.CLASS_CLASS__V41*/ meltfptr[40]),
		    (MELTBPARSTR_PTR MELTBPARSTR_PTR ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;
    /*epilog */

    /*^clear */
	    /*clear *//*_.INSTALL_METHOD__V388*/ meltfptr[361] = 0;
  }

  MELT_LOCATION ("warmelt-debug.melt:1367:/ initchunk");
  /*^block */
  /*anyblock */
  {

    /*^checksignal */
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[2];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & /*_.VALDATA___V116*/ meltfptr[115];
      /*^apply.arg */
      argtab[1].meltbp_aptr =
	(melt_ptr_t *) & /*_.VALCLO___V307*/ meltfptr[306];
      /*_.INSTALL_METHOD__V389*/ meltfptr[359] =
	melt_apply ((meltclosure_ptr_t)
		    ( /*_.INSTALL_METHOD__V21*/ meltfptr[20]),
		    (melt_ptr_t) ( /*_.CLASS_KEYWORD__V42*/ meltfptr[41]),
		    (MELTBPARSTR_PTR MELTBPARSTR_PTR ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;
    /*epilog */

    /*^clear */
	    /*clear *//*_.INSTALL_METHOD__V389*/ meltfptr[359] = 0;
  }

  MELT_LOCATION ("warmelt-debug.melt:1375:/ initchunk");
  /*^block */
  /*anyblock */
  {

    /*^checksignal */
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[2];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & /*_.VALDATA___V118*/ meltfptr[117];
      /*^apply.arg */
      argtab[1].meltbp_aptr =
	(melt_ptr_t *) & /*_.VALCLO___V311*/ meltfptr[310];
      /*_.INSTALL_METHOD__V390*/ meltfptr[360] =
	melt_apply ((meltclosure_ptr_t)
		    ( /*_.INSTALL_METHOD__V21*/ meltfptr[20]),
		    (melt_ptr_t) ( /*_.CLASS_KEYWORD__V42*/ meltfptr[41]),
		    (MELTBPARSTR_PTR MELTBPARSTR_PTR ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;
    /*epilog */

    /*^clear */
	    /*clear *//*_.INSTALL_METHOD__V390*/ meltfptr[360] = 0;
  }

  MELT_LOCATION ("warmelt-debug.melt:1392:/ initchunk");
  /*^block */
  /*anyblock */
  {

    /*^checksignal */
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[2];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & /*_.VALDATA___V116*/ meltfptr[115];
      /*^apply.arg */
      argtab[1].meltbp_aptr =
	(melt_ptr_t *) & /*_.VALCLO___V313*/ meltfptr[312];
      /*_.INSTALL_METHOD__V391*/ meltfptr[361] =
	melt_apply ((meltclosure_ptr_t)
		    ( /*_.INSTALL_METHOD__V21*/ meltfptr[20]),
		    (melt_ptr_t) ( /*_.CLASS_CLONED_SYMBOL__V43*/
				  meltfptr[42]),
		    (MELTBPARSTR_PTR MELTBPARSTR_PTR ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;
    /*epilog */

    /*^clear */
	    /*clear *//*_.INSTALL_METHOD__V391*/ meltfptr[361] = 0;
  }

  MELT_LOCATION ("warmelt-debug.melt:1401:/ initchunk");
  /*^block */
  /*anyblock */
  {

    /*^checksignal */
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[2];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & /*_.VALDATA___V118*/ meltfptr[117];
      /*^apply.arg */
      argtab[1].meltbp_aptr =
	(melt_ptr_t *) & /*_.VALCLO___V317*/ meltfptr[316];
      /*_.INSTALL_METHOD__V392*/ meltfptr[359] =
	melt_apply ((meltclosure_ptr_t)
		    ( /*_.INSTALL_METHOD__V21*/ meltfptr[20]),
		    (melt_ptr_t) ( /*_.CLASS_CLONED_SYMBOL__V43*/
				  meltfptr[42]),
		    (MELTBPARSTR_PTR MELTBPARSTR_PTR ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;
    /*epilog */

    /*^clear */
	    /*clear *//*_.INSTALL_METHOD__V392*/ meltfptr[359] = 0;
  }

  MELT_LOCATION ("warmelt-debug.melt:1446:/ initchunk");
  /*^block */
  /*anyblock */
  {

    /*^checksignal */
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[2];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & /*_.VALDATA___V118*/ meltfptr[117];
      /*^apply.arg */
      argtab[1].meltbp_aptr =
	(melt_ptr_t *) & /*_.VALCLO___V319*/ meltfptr[318];
      /*_.INSTALL_METHOD__V393*/ meltfptr[360] =
	melt_apply ((meltclosure_ptr_t)
		    ( /*_.INSTALL_METHOD__V21*/ meltfptr[20]),
		    (melt_ptr_t) ( /*_.CLASS_ENVIRONMENT__V44*/ meltfptr[43]),
		    (MELTBPARSTR_PTR MELTBPARSTR_PTR ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;
    /*epilog */

    /*^clear */
	    /*clear *//*_.INSTALL_METHOD__V393*/ meltfptr[360] = 0;
  }

  MELT_LOCATION ("warmelt-debug.melt:1459:/ initchunk");
  /*^block */
  /*anyblock */
  {

    /*^checksignal */
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[2];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & /*_.VALDATA___V116*/ meltfptr[115];
      /*^apply.arg */
      argtab[1].meltbp_aptr =
	(melt_ptr_t *) & /*_.VALCLO___V325*/ meltfptr[324];
      /*_.INSTALL_METHOD__V394*/ meltfptr[361] =
	melt_apply ((meltclosure_ptr_t)
		    ( /*_.INSTALL_METHOD__V21*/ meltfptr[20]),
		    (melt_ptr_t) ( /*_.CLASS_CTYPE__V20*/ meltfptr[19]),
		    (MELTBPARSTR_PTR MELTBPARSTR_PTR ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;
    /*epilog */

    /*^clear */
	    /*clear *//*_.INSTALL_METHOD__V394*/ meltfptr[361] = 0;
  }

  MELT_LOCATION ("warmelt-debug.melt:1469:/ initchunk");
  /*^block */
  /*anyblock */
  {

    /*^checksignal */
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[2];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & /*_.VALDATA___V118*/ meltfptr[117];
      /*^apply.arg */
      argtab[1].meltbp_aptr =
	(melt_ptr_t *) & /*_.VALCLO___V327*/ meltfptr[326];
      /*_.INSTALL_METHOD__V395*/ meltfptr[359] =
	melt_apply ((meltclosure_ptr_t)
		    ( /*_.INSTALL_METHOD__V21*/ meltfptr[20]),
		    (melt_ptr_t) ( /*_.CLASS_CTYPE__V20*/ meltfptr[19]),
		    (MELTBPARSTR_PTR MELTBPARSTR_PTR ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;
    /*epilog */

    /*^clear */
	    /*clear *//*_.INSTALL_METHOD__V395*/ meltfptr[359] = 0;
  }

  MELT_LOCATION ("warmelt-debug.melt:1501:/ initchunk");
  /*^block */
  /*anyblock */
  {

    /*^checksignal */
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[2];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & /*_.VALDATA___V118*/ meltfptr[117];
      /*^apply.arg */
      argtab[1].meltbp_aptr =
	(melt_ptr_t *) & /*_.VALCLO___V329*/ meltfptr[328];
      /*_.INSTALL_METHOD__V396*/ meltfptr[360] =
	melt_apply ((meltclosure_ptr_t)
		    ( /*_.INSTALL_METHOD__V21*/ meltfptr[20]),
		    (melt_ptr_t) ( /*_.CLASS_ANY_BINDING__V46*/ meltfptr[45]),
		    (MELTBPARSTR_PTR MELTBPARSTR_PTR ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;
    /*epilog */

    /*^clear */
	    /*clear *//*_.INSTALL_METHOD__V396*/ meltfptr[360] = 0;
  }

  MELT_LOCATION ("warmelt-debug.melt:1502:/ initchunk");
  /*^block */
  /*anyblock */
  {

    /*^checksignal */
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[2];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & /*_.VALDATA___V116*/ meltfptr[115];
      /*^apply.arg */
      argtab[1].meltbp_aptr =
	(melt_ptr_t *) & /*_.VALCLO___V329*/ meltfptr[328];
      /*_.INSTALL_METHOD__V397*/ meltfptr[361] =
	melt_apply ((meltclosure_ptr_t)
		    ( /*_.INSTALL_METHOD__V21*/ meltfptr[20]),
		    (melt_ptr_t) ( /*_.CLASS_ANY_BINDING__V46*/ meltfptr[45]),
		    (MELTBPARSTR_PTR MELTBPARSTR_PTR ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;
    /*epilog */

    /*^clear */
	    /*clear *//*_.INSTALL_METHOD__V397*/ meltfptr[361] = 0;
  }

  MELT_LOCATION ("warmelt-debug.melt:1532:/ initchunk");
  /*^block */
  /*anyblock */
  {

    /*^getslot */
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj =
	(melt_ptr_t) (((melt_ptr_t) (MELT_PREDEF (INITIAL_SYSTEM_DATA))))
	/*=obj*/ ;
      melt_object_get_field (slot, obj, 4, "SYSDATA_VALUE_EXPORTER");
   /*_.VALUEXPORTER___V398*/ meltfptr[359] = slot;
    };
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[2];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & /*_.VALCLO___V112*/ meltfptr[111];
      /*^apply.arg */
      argtab[1].meltbp_aptr = (melt_ptr_t *) &
	/*quasi.cur.mod.env.cont norm.exp.val : DBG_OUT */
/*_.CONTENV___V2*/ meltfptr[1];
      melt_apply ((meltclosure_ptr_t)
		  ( /*_.VALUEXPORTER___V398*/ meltfptr[359]),
		  (melt_ptr_t) ( /*_.VALDATA___V334*/ meltfptr[333]),
		  (MELTBPARSTR_PTR MELTBPARSTR_PTR ""), argtab, "",
		  (union meltparam_un *) 0);
    }
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[2];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & /*_.VALCLO___V114*/ meltfptr[113];
      /*^apply.arg */
      argtab[1].meltbp_aptr = (melt_ptr_t *) &
	/*quasi.cur.mod.env.cont norm.exp.val : DBG_OUTOBJECT */
/*_.CONTENV___V2*/ meltfptr[1];
      melt_apply ((meltclosure_ptr_t)
		  ( /*_.VALUEXPORTER___V398*/ meltfptr[359]),
		  (melt_ptr_t) ( /*_.VALDATA___V336*/ meltfptr[335]),
		  (MELTBPARSTR_PTR MELTBPARSTR_PTR ""), argtab, "",
		  (union meltparam_un *) 0);
    }
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[2];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & /*_.VALDATA___V118*/ meltfptr[117];
      /*^apply.arg */
      argtab[1].meltbp_aptr = (melt_ptr_t *) &
	/*quasi.cur.mod.env.cont norm.exp.val : DBG_OUTPUT */
/*_.CONTENV___V2*/ meltfptr[1];
      melt_apply ((meltclosure_ptr_t)
		  ( /*_.VALUEXPORTER___V398*/ meltfptr[359]),
		  (melt_ptr_t) ( /*_.VALDATA___V131*/ meltfptr[130]),
		  (MELTBPARSTR_PTR MELTBPARSTR_PTR ""), argtab, "",
		  (union meltparam_un *) 0);
    }
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[2];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & /*_.VALDATA___V116*/ meltfptr[115];
      /*^apply.arg */
      argtab[1].meltbp_aptr = (melt_ptr_t *) &
	/*quasi.cur.mod.env.cont norm.exp.val : DBG_OUTPUTAGAIN */
/*_.CONTENV___V2*/ meltfptr[1];
      melt_apply ((meltclosure_ptr_t)
		  ( /*_.VALUEXPORTER___V398*/ meltfptr[359]),
		  (melt_ptr_t) ( /*_.VALDATA___V143*/ meltfptr[142]),
		  (MELTBPARSTR_PTR MELTBPARSTR_PTR ""), argtab, "",
		  (union meltparam_un *) 0);
    }
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[2];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & /*_.VALCLO___V200*/ meltfptr[199];
      /*^apply.arg */
      argtab[1].meltbp_aptr = (melt_ptr_t *) &
	/*quasi.cur.mod.env.cont norm.exp.val : DBGOUT_FIELDS */
/*_.CONTENV___V2*/ meltfptr[1];
      melt_apply ((meltclosure_ptr_t)
		  ( /*_.VALUEXPORTER___V398*/ meltfptr[359]),
		  (melt_ptr_t) ( /*_.VALDATA___V338*/ meltfptr[337]),
		  (MELTBPARSTR_PTR MELTBPARSTR_PTR ""), argtab, "",
		  (union meltparam_un *) 0);
    }
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[2];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & /*_.VALCLO___V202*/ meltfptr[201];
      /*^apply.arg */
      argtab[1].meltbp_aptr = (melt_ptr_t *) &
	/*quasi.cur.mod.env.cont norm.exp.val : DBGOUTAGAIN_FIELDS */
/*_.CONTENV___V2*/ meltfptr[1];
      melt_apply ((meltclosure_ptr_t)
		  ( /*_.VALUEXPORTER___V398*/ meltfptr[359]),
		  (melt_ptr_t) ( /*_.VALDATA___V340*/ meltfptr[339]),
		  (MELTBPARSTR_PTR MELTBPARSTR_PTR ""), argtab, "",
		  (union meltparam_un *) 0);
    }
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[2];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & /*_.VALDATA___V75*/ meltfptr[74];
      /*^apply.arg */
      argtab[1].meltbp_aptr = (melt_ptr_t *) &
	/*quasi.cur.mod.env.cont norm.exp.val : DEBUG_DEPTH */
/*_.CONTENV___V2*/ meltfptr[1];
      melt_apply ((meltclosure_ptr_t)
		  ( /*_.VALUEXPORTER___V398*/ meltfptr[359]),
		  (melt_ptr_t) ( /*_.VALDATA___V73*/ meltfptr[72]),
		  (MELTBPARSTR_PTR MELTBPARSTR_PTR ""), argtab, "",
		  (union meltparam_un *) 0);
    }
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[2];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & /*_.VALCLO___V120*/ meltfptr[119];
      /*^apply.arg */
      argtab[1].meltbp_aptr = (melt_ptr_t *) &
	/*quasi.cur.mod.env.cont norm.exp.val : DEBUG_MSG_FUN */
/*_.CONTENV___V2*/ meltfptr[1];
      melt_apply ((meltclosure_ptr_t)
		  ( /*_.VALUEXPORTER___V398*/ meltfptr[359]),
		  (melt_ptr_t) ( /*_.VALDATA___V342*/ meltfptr[341]),
		  (MELTBPARSTR_PTR MELTBPARSTR_PTR ""), argtab, "",
		  (union meltparam_un *) 0);
    }
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[2];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & /*_.VALDATA___V185*/ meltfptr[184];
      /*^apply.arg */
      argtab[1].meltbp_aptr = (melt_ptr_t *) &
	/*quasi.cur.mod.env.cont norm.exp.val : DISCR_DEBUG_CLOSURE */
/*_.CONTENV___V2*/ meltfptr[1];
      melt_apply ((meltclosure_ptr_t)
		  ( /*_.VALUEXPORTER___V398*/ meltfptr[359]),
		  (melt_ptr_t) ( /*_.VALDATA___V181*/ meltfptr[180]),
		  (MELTBPARSTR_PTR MELTBPARSTR_PTR ""), argtab, "",
		  (union meltparam_un *) 0);
    }
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[2];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & /*_.VALCLO___V333*/ meltfptr[332];
      /*^apply.arg */
      argtab[1].meltbp_aptr = (melt_ptr_t *) &
	/*quasi.cur.mod.env.cont norm.exp.val : DISPLAY_DEBUG_MESSAGE */
/*_.CONTENV___V2*/ meltfptr[1];
      melt_apply ((meltclosure_ptr_t)
		  ( /*_.VALUEXPORTER___V398*/ meltfptr[359]),
		  (melt_ptr_t) ( /*_.VALDATA___V344*/ meltfptr[343]),
		  (MELTBPARSTR_PTR MELTBPARSTR_PTR ""), argtab, "",
		  (union meltparam_un *) 0);
    }
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[2];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & /*_.VALCLO___V190*/ meltfptr[189];
      /*^apply.arg */
      argtab[1].meltbp_aptr = (melt_ptr_t *) &
	/*quasi.cur.mod.env.cont norm.exp.val : MELT_DEBUG_FUN */
/*_.CONTENV___V2*/ meltfptr[1];
      melt_apply ((meltclosure_ptr_t)
		  ( /*_.VALUEXPORTER___V398*/ meltfptr[359]),
		  (melt_ptr_t) ( /*_.VALDATA___V346*/ meltfptr[345]),
		  (MELTBPARSTR_PTR MELTBPARSTR_PTR ""), argtab, "",
		  (union meltparam_un *) 0);
    }
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[2];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & /*_.VALDATA___V93*/ meltfptr[92];
      /*^apply.arg */
      argtab[1].meltbp_aptr = (melt_ptr_t *) &
	/*quasi.cur.mod.env.cont norm.exp.val : MELT_HAS_FLAG_DEBUG_SET */
/*_.CONTENV___V2*/ meltfptr[1];
      melt_apply ((meltclosure_ptr_t)
		  ( /*_.VALUEXPORTER___V398*/ meltfptr[359]),
		  (melt_ptr_t) ( /*_.VALDATA___V91*/ meltfptr[90]),
		  (MELTBPARSTR_PTR MELTBPARSTR_PTR ""), argtab, "",
		  (union meltparam_un *) 0);
    }
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[2];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & /*_.VALDATA___V100*/ meltfptr[99];
      /*^apply.arg */
      argtab[1].meltbp_aptr = (melt_ptr_t *) &
	/*quasi.cur.mod.env.cont norm.exp.val : MELT_REALLY_NEED_DBGLIM */
/*_.CONTENV___V2*/ meltfptr[1];
      melt_apply ((meltclosure_ptr_t)
		  ( /*_.VALUEXPORTER___V398*/ meltfptr[359]),
		  (melt_ptr_t) ( /*_.VALDATA___V98*/ meltfptr[97]),
		  (MELTBPARSTR_PTR MELTBPARSTR_PTR ""), argtab, "",
		  (union meltparam_un *) 0);
    }
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[2];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & /*_.VALDATA___V82*/ meltfptr[81];
      /*^apply.arg */
      argtab[1].meltbp_aptr = (melt_ptr_t *) &
	/*quasi.cur.mod.env.cont norm.exp.val : MELT_REALLY_NEED_DEBUG */
/*_.CONTENV___V2*/ meltfptr[1];
      melt_apply ((meltclosure_ptr_t)
		  ( /*_.VALUEXPORTER___V398*/ meltfptr[359]),
		  (melt_ptr_t) ( /*_.VALDATA___V80*/ meltfptr[79]),
		  (MELTBPARSTR_PTR MELTBPARSTR_PTR ""), argtab, "",
		  (union meltparam_un *) 0);
    }
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[2];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & /*_.VALCLO___V175*/ meltfptr[174];
      /*^apply.arg */
      argtab[1].meltbp_aptr = (melt_ptr_t *) &
	/*quasi.cur.mod.env.cont norm.exp.val : REGISTER_BASICBLOCK_DEBUG_FUN */
/*_.CONTENV___V2*/ meltfptr[1];
      melt_apply ((meltclosure_ptr_t)
		  ( /*_.VALUEXPORTER___V398*/ meltfptr[359]),
		  (melt_ptr_t) ( /*_.VALDATA___V348*/ meltfptr[347]),
		  (MELTBPARSTR_PTR MELTBPARSTR_PTR ""), argtab, "",
		  (union meltparam_un *) 0);
    }
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[2];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & /*_.VALCLO___V170*/ meltfptr[169];
      /*^apply.arg */
      argtab[1].meltbp_aptr = (melt_ptr_t *) &
	/*quasi.cur.mod.env.cont norm.exp.val : REGISTER_EDGE_DEBUG_FUN */
/*_.CONTENV___V2*/ meltfptr[1];
      melt_apply ((meltclosure_ptr_t)
		  ( /*_.VALUEXPORTER___V398*/ meltfptr[359]),
		  (melt_ptr_t) ( /*_.VALDATA___V350*/ meltfptr[349]),
		  (MELTBPARSTR_PTR MELTBPARSTR_PTR ""), argtab, "",
		  (union meltparam_un *) 0);
    }
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[2];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & /*_.VALCLO___V160*/ meltfptr[159];
      /*^apply.arg */
      argtab[1].meltbp_aptr = (melt_ptr_t *) &
	/*quasi.cur.mod.env.cont norm.exp.val : REGISTER_GIMPLE_DEBUG_FUN */
/*_.CONTENV___V2*/ meltfptr[1];
      melt_apply ((meltclosure_ptr_t)
		  ( /*_.VALUEXPORTER___V398*/ meltfptr[359]),
		  (melt_ptr_t) ( /*_.VALDATA___V352*/ meltfptr[351]),
		  (MELTBPARSTR_PTR MELTBPARSTR_PTR ""), argtab, "",
		  (union meltparam_un *) 0);
    }
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[2];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & /*_.VALCLO___V165*/ meltfptr[164];
      /*^apply.arg */
      argtab[1].meltbp_aptr = (melt_ptr_t *) &
	/*quasi.cur.mod.env.cont norm.exp.val : REGISTER_GIMPLESEQ_DEBUG_FUN */
/*_.CONTENV___V2*/ meltfptr[1];
      melt_apply ((meltclosure_ptr_t)
		  ( /*_.VALUEXPORTER___V398*/ meltfptr[359]),
		  (melt_ptr_t) ( /*_.VALDATA___V354*/ meltfptr[353]),
		  (MELTBPARSTR_PTR MELTBPARSTR_PTR ""), argtab, "",
		  (union meltparam_un *) 0);
    }
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[2];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & /*_.VALCLO___V180*/ meltfptr[179];
      /*^apply.arg */
      argtab[1].meltbp_aptr = (melt_ptr_t *) &
	/*quasi.cur.mod.env.cont norm.exp.val : REGISTER_LOOP_DEBUG_FUN */
/*_.CONTENV___V2*/ meltfptr[1];
      melt_apply ((meltclosure_ptr_t)
		  ( /*_.VALUEXPORTER___V398*/ meltfptr[359]),
		  (melt_ptr_t) ( /*_.VALDATA___V356*/ meltfptr[355]),
		  (MELTBPARSTR_PTR MELTBPARSTR_PTR ""), argtab, "",
		  (union meltparam_un *) 0);
    }
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[2];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & /*_.VALCLO___V155*/ meltfptr[154];
      /*^apply.arg */
      argtab[1].meltbp_aptr = (melt_ptr_t *) &
	/*quasi.cur.mod.env.cont norm.exp.val : REGISTER_TREE_DEBUG_FUN */
/*_.CONTENV___V2*/ meltfptr[1];
      melt_apply ((meltclosure_ptr_t)
		  ( /*_.VALUEXPORTER___V398*/ meltfptr[359]),
		  (melt_ptr_t) ( /*_.VALDATA___V358*/ meltfptr[357]),
		  (MELTBPARSTR_PTR MELTBPARSTR_PTR ""), argtab, "",
		  (union meltparam_un *) 0);
    }
    ;
    /*epilog */

    /*^clear */
	    /*clear *//*_.VALUEXPORTER___V398*/ meltfptr[359] = 0;
  }

  MELT_LOCATION ("warmelt-debug.melt:1556:/ initchunk");
  /*^block */
  /*anyblock */
  {

    /*^checksignal */
    MELT_CHECK_SIGNAL ();
    ;
    /*^cond */
    /*cond */ if (
		   /*ifisa */
		   melt_is_instance_of ((melt_ptr_t)
					( /*_.INITIAL_SYSTEM_DATA__V8*/
					 meltfptr[7]),
					(melt_ptr_t) ( /*_.CLASS_SYSTEM_DATA__V9*/ meltfptr[8]))
      )				/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  /*^putslot */
	  /*putslot */
	  melt_assertmsg ("putslot checkobj @SYSDATA_DEBUGMSG",
			  melt_magic_discr ((melt_ptr_t)
					    ( /*_.INITIAL_SYSTEM_DATA__V8*/
					     meltfptr[7])) ==
			  MELTOBMAG_OBJECT);
	  melt_putfield_object (( /*_.INITIAL_SYSTEM_DATA__V8*/ meltfptr[7]),
				(17), ( /*_.VALCLO___V120*/ meltfptr[119]),
				"SYSDATA_DEBUGMSG");
	  ;
	  /*^touch */
	  meltgc_touch ( /*_.INITIAL_SYSTEM_DATA__V8*/ meltfptr[7]);
	  ;
	  /*^touchobj */

	  melt_dbgtrace_written_object ( /*_.INITIAL_SYSTEM_DATA__V8*/
					meltfptr[7], "put-fields");
	  ;
	  /*epilog */
	}
	;
      }				/*noelse */
    ;
    /*epilog */
  }

 /**COMMENT: after toplevel body **/ ;

 /**COMMENT: compute boxloc again **/ ;

  /*cond */ if ( /*_.CONTENV___V2*/ meltfptr[1] || melt_object_length ((melt_ptr_t) MELT_PREDEF (INITIAL_SYSTEM_DATA)) < MELTFIELD_SYSDATA_CONT_FRESH_ENV)	/*then */
    {
    }
  else
    {				/*^cond.else */

      /*^block */
      /*com.block:compute fresh module environment */
      {

	/*^comment */
   /**COMMENT: start computing boxloc **/ ;
	;
	/*^getslot */
	{
	  melt_ptr_t slot = NULL, obj = NULL;
	  obj =
	    (melt_ptr_t) (((melt_ptr_t) (MELT_PREDEF (INITIAL_SYSTEM_DATA))))
	    /*=obj*/ ;
	  melt_object_get_field (slot, obj, 3, "SYSDATA_CONT_FRESH_ENV");
    /*_.FRESHENV___V3*/ meltfptr[2] = slot;
	};
	;
	/*^compute */

	/*checkfreshenv */ if (( /*_.PREVENV___V4*/ meltfptr[3])
			       &&
			       melt_magic_discr ((melt_ptr_t)
						 ( /*_.FRESHENV___V3*/
						  meltfptr[2])) !=
			       MELTOBMAG_CLOSURE)
	  warning (0,
		   "MELT corruption: bad FRESH_ENV @%p in system data <%s:%d>",
		   (void *) ( /*_.FRESHENV___V3*/ meltfptr[2]), __FILE__,
		   __LINE__);;;
	/*^apply */
	/*apply */
	{
	  /*_.CONTENV___V2*/ meltfptr[1] =
	    melt_apply ((meltclosure_ptr_t)
			( /*_.FRESHENV___V3*/ meltfptr[2]),
			(melt_ptr_t) ( /*_.PREVENV___V4*/ meltfptr[3]), (""),
			(union meltparam_un *) 0, "",
			(union meltparam_un *) 0);
	}
	;
      }				/*com.end block:compute fresh module environment */
      ;
    }

 /**COMMENT: intern symbols **/ ;

  /*internsym:CLASS_DEBUG_INFORMATION */
  (void)
    meltgc_intern_symbol ((melt_ptr_t) ( /*_.VALDATA___V47*/ meltfptr[46]));

  /*internsym:CLASS_ROOT */
  (void)
    meltgc_intern_symbol ((melt_ptr_t) ( /*_.VALDATA___V49*/ meltfptr[48]));

  /*internsym:DBGI_OUT */
  (void)
    meltgc_intern_symbol ((melt_ptr_t) ( /*_.VALDATA___V61*/ meltfptr[60]));

  /*internsym:DBGI_OCCMAP */
  (void)
    meltgc_intern_symbol ((melt_ptr_t) ( /*_.VALDATA___V63*/ meltfptr[62]));

  /*internsym:DBGI_MAXDEPTH */
  (void)
    meltgc_intern_symbol ((melt_ptr_t) ( /*_.VALDATA___V65*/ meltfptr[64]));

  /*internsym:CLASS_DEBUG_OUTPUT_INFORMATION */
  (void)
    meltgc_intern_symbol ((melt_ptr_t) ( /*_.VALDATA___V67*/ meltfptr[66]));

  /*internsym:DEBUG_DEPTH */
  (void)
    meltgc_intern_symbol ((melt_ptr_t) ( /*_.VALDATA___V73*/ meltfptr[72]));

  /*internsym:MELT_REALLY_NEED_DEBUG */
  (void)
    meltgc_intern_symbol ((melt_ptr_t) ( /*_.VALDATA___V80*/ meltfptr[79]));

  /*internsym:DEPTH */
  (void)
    meltgc_intern_symbol ((melt_ptr_t) ( /*_.VALDATA___V85*/ meltfptr[84]));

  /*internsym:MELT_HAS_FLAG_DEBUG_SET */
  (void)
    meltgc_intern_symbol ((melt_ptr_t) ( /*_.VALDATA___V91*/ meltfptr[90]));

  /*internsym:MELT_REALLY_NEED_DBGLIM */
  (void)
    meltgc_intern_symbol ((melt_ptr_t) ( /*_.VALDATA___V98*/ meltfptr[97]));

  /*internsym:LIMIT */
  (void)
    meltgc_intern_symbol ((melt_ptr_t) ( /*_.VALDATA___V104*/ meltfptr[103]));

  /*internsym:INITIAL_SYSTEM_DATA */
  (void)
    meltgc_intern_symbol ((melt_ptr_t) ( /*_.VALDATA___V121*/ meltfptr[120]));

  /*internsym:CLASS_SYSTEM_DATA */
  (void)
    meltgc_intern_symbol ((melt_ptr_t) ( /*_.VALDATA___V123*/ meltfptr[122]));

  /*internsym:DISCR_MAP_OBJECTS */
  (void)
    meltgc_intern_symbol ((melt_ptr_t) ( /*_.VALDATA___V125*/ meltfptr[124]));

  /*internsym:DISCR_INTEGER */
  (void)
    meltgc_intern_symbol ((melt_ptr_t) ( /*_.VALDATA___V127*/ meltfptr[126]));

  /*internsym:DISCR_STRBUF */
  (void)
    meltgc_intern_symbol ((melt_ptr_t) ( /*_.VALDATA___V129*/ meltfptr[128]));

  /*internsym:DBG_OUTPUT */
  (void)
    meltgc_intern_symbol ((melt_ptr_t) ( /*_.VALDATA___V131*/ meltfptr[130]));

  /*internsym:CLASS_SELECTOR */
  (void)
    meltgc_intern_symbol ((melt_ptr_t) ( /*_.VALDATA___V133*/ meltfptr[132]));


#undef meltfram__
}				/*end of meltmod__warmelt_debug__initialmeltchunk_5 */

void
meltmod__warmelt_debug__initialmeltchunk_6 (struct
					    frame_melt_start_this_module_st
					    *meltmeltframptr__,
					    char meltpredefinited[])
{
#define meltfram__ (*meltmeltframptr__)
#undef meltcallcount
#define meltcallcount 0L
  (void) meltpredefinited;
/*internsym:RECV*/
  (void)
    meltgc_intern_symbol ((melt_ptr_t) ( /*_.VALDATA___V135*/ meltfptr[134]));

  /*internsym:DBGINFO */
  (void)
    meltgc_intern_symbol ((melt_ptr_t) ( /*_.VALDATA___V138*/ meltfptr[137]));

  /*internsym:DBG_OUTPUTAGAIN */
  (void)
    meltgc_intern_symbol ((melt_ptr_t) ( /*_.VALDATA___V143*/ meltfptr[142]));

  /*internsym:TREE_DEBUG_FUNCONT */
  (void)
    meltgc_intern_symbol ((melt_ptr_t) ( /*_.VALDATA___V149*/ meltfptr[148]));

  /*internsym:CLASS_REFERENCE */
  (void)
    meltgc_intern_symbol ((melt_ptr_t) ( /*_.VALDATA___V151*/ meltfptr[150]));

  /*internsym:GIMPLE_DEBUG_FUNCONT */
  (void)
    meltgc_intern_symbol ((melt_ptr_t) ( /*_.VALDATA___V156*/ meltfptr[155]));

  /*internsym:GIMPLESEQ_DEBUG_FUNCONT */
  (void)
    meltgc_intern_symbol ((melt_ptr_t) ( /*_.VALDATA___V161*/ meltfptr[160]));

  /*internsym:EDGE_DEBUG_FUNCONT */
  (void)
    meltgc_intern_symbol ((melt_ptr_t) ( /*_.VALDATA___V166*/ meltfptr[165]));

  /*internsym:BASICBLOCK_DEBUG_FUNCONT */
  (void)
    meltgc_intern_symbol ((melt_ptr_t) ( /*_.VALDATA___V171*/ meltfptr[170]));

  /*internsym:LOOP_DEBUG_FUNCONT */
  (void)
    meltgc_intern_symbol ((melt_ptr_t) ( /*_.VALDATA___V176*/ meltfptr[175]));

  /*internsym:DISCR_DEBUG_CLOSURE */
  (void)
    meltgc_intern_symbol ((melt_ptr_t) ( /*_.VALDATA___V181*/ meltfptr[180]));

  /*internsym:CLASS_DISCRIMINANT */
  (void)
    meltgc_intern_symbol ((melt_ptr_t) ( /*_.VALDATA___V183*/ meltfptr[182]));

  /*internsym:DISCR_CLOSURE */
  (void)
    meltgc_intern_symbol ((melt_ptr_t) ( /*_.VALDATA___V187*/ meltfptr[186]));

  /*internsym:ADD2OUT */
  (void)
    meltgc_intern_symbol ((melt_ptr_t) ( /*_.VALDATA___V191*/ meltfptr[190]));

  /*internsym:CSTRING_PREFIXED */
  (void)
    meltgc_intern_symbol ((melt_ptr_t) ( /*_.VALDATA___V193*/ meltfptr[192]));

  /*internsym:CLASS_NAMED */
  (void)
    meltgc_intern_symbol ((melt_ptr_t) ( /*_.VALDATA___V195*/ meltfptr[194]));

  /*internsym:CLASS_CTYPE */
  (void)
    meltgc_intern_symbol ((melt_ptr_t) ( /*_.VALDATA___V197*/ meltfptr[196]));

  /*internsym:INSTALL_METHOD */
  (void)
    meltgc_intern_symbol ((melt_ptr_t) ( /*_.VALDATA___V205*/ meltfptr[204]));

  /*internsym:DISCR_NULL_RECEIVER */
  (void)
    meltgc_intern_symbol ((melt_ptr_t) ( /*_.VALDATA___V207*/ meltfptr[206]));

  /*internsym:DISCR_STRING */
  (void)
    meltgc_intern_symbol ((melt_ptr_t) ( /*_.VALDATA___V211*/ meltfptr[210]));

  /*internsym:DISCR_CONSTANT_INTEGER */
  (void)
    meltgc_intern_symbol ((melt_ptr_t) ( /*_.VALDATA___V217*/ meltfptr[216]));

  /*internsym:DISCR_MIXED_INTEGER */
  (void)
    meltgc_intern_symbol ((melt_ptr_t) ( /*_.VALDATA___V221*/ meltfptr[220]));

  /*internsym:DISCR_MIXED_LOCATION */
  (void)
    meltgc_intern_symbol ((melt_ptr_t) ( /*_.VALDATA___V225*/ meltfptr[224]));

  /*internsym:DISCR_MIXED_BIGINT */
  (void)
    meltgc_intern_symbol ((melt_ptr_t) ( /*_.VALDATA___V229*/ meltfptr[228]));

  /*internsym:DISCR_MULTIPLE */
  (void)
    meltgc_intern_symbol ((melt_ptr_t) ( /*_.VALDATA___V233*/ meltfptr[232]));

  /*internsym:DISCR_ROUTINE */
  (void)
    meltgc_intern_symbol ((melt_ptr_t) ( /*_.VALDATA___V238*/ meltfptr[237]));

  /*internsym:ROUTINE_EVERY */
  (void)
    meltgc_intern_symbol ((melt_ptr_t) ( /*_.VALDATA___V240*/ meltfptr[239]));

  /*internsym:DISCR_LIST */
  (void)
    meltgc_intern_symbol ((melt_ptr_t) ( /*_.VALDATA___V246*/ meltfptr[245]));

  /*internsym:DISCR_PAIR */
  (void)
    meltgc_intern_symbol ((melt_ptr_t) ( /*_.VALDATA___V250*/ meltfptr[249]));

  /*internsym:MAPOBJECT_EVERY */
  (void)
    meltgc_intern_symbol ((melt_ptr_t) ( /*_.VALDATA___V260*/ meltfptr[259]));

  /*internsym:COMPARE_OBJ_RANKED */
  (void)
    meltgc_intern_symbol ((melt_ptr_t) ( /*_.VALDATA___V262*/ meltfptr[261]));

  /*internsym:MULTIPLE_EVERY */
  (void)
    meltgc_intern_symbol ((melt_ptr_t) ( /*_.VALDATA___V264*/ meltfptr[263]));

  /*internsym:DISCR_MAP_STRINGS */
  (void)
    meltgc_intern_symbol ((melt_ptr_t) ( /*_.VALDATA___V274*/ meltfptr[273]));

  /*internsym:MAPSTRING_EVERY */
  (void)
    meltgc_intern_symbol ((melt_ptr_t) ( /*_.VALDATA___V276*/ meltfptr[275]));

  /*internsym:DISCR_BUCKET_LONGS */
  (void)
    meltgc_intern_symbol ((melt_ptr_t) ( /*_.VALDATA___V280*/ meltfptr[279]));

  /*internsym:DISCR_ANY_RECEIVER */
  (void)
    meltgc_intern_symbol ((melt_ptr_t) ( /*_.VALDATA___V286*/ meltfptr[285]));

  /*internsym:DBGOUT_NAMEDOBJECT_METHOD */
  (void)
    meltgc_intern_symbol ((melt_ptr_t) ( /*_.VALDATA___V292*/ meltfptr[291]));

  /*internsym:CLASS_SYMBOL */
  (void)
    meltgc_intern_symbol ((melt_ptr_t) ( /*_.VALDATA___V298*/ meltfptr[297]));

  /*internsym:CLASS_CLASS */
  (void)
    meltgc_intern_symbol ((melt_ptr_t) ( /*_.VALDATA___V304*/ meltfptr[303]));

  /*internsym:CLASS_KEYWORD */
  (void)
    meltgc_intern_symbol ((melt_ptr_t) ( /*_.VALDATA___V308*/ meltfptr[307]));

  /*internsym:CLASS_CLONED_SYMBOL */
  (void)
    meltgc_intern_symbol ((melt_ptr_t) ( /*_.VALDATA___V314*/ meltfptr[313]));

  /*internsym:CLASS_ENVIRONMENT */
  (void)
    meltgc_intern_symbol ((melt_ptr_t) ( /*_.VALDATA___V320*/ meltfptr[319]));

  /*internsym:ENV_PREV */
  (void)
    meltgc_intern_symbol ((melt_ptr_t) ( /*_.VALDATA___V322*/ meltfptr[321]));

  /*internsym:CLASS_ANY_BINDING */
  (void)
    meltgc_intern_symbol ((melt_ptr_t) ( /*_.VALDATA___V330*/ meltfptr[329]));

  /*internsym:DBG_OUT */
  (void)
    meltgc_intern_symbol ((melt_ptr_t) ( /*_.VALDATA___V334*/ meltfptr[333]));

  /*internsym:DBG_OUTOBJECT */
  (void)
    meltgc_intern_symbol ((melt_ptr_t) ( /*_.VALDATA___V336*/ meltfptr[335]));

  /*internsym:DBGOUT_FIELDS */
  (void)
    meltgc_intern_symbol ((melt_ptr_t) ( /*_.VALDATA___V338*/ meltfptr[337]));

  /*internsym:DBGOUTAGAIN_FIELDS */
  (void)
    meltgc_intern_symbol ((melt_ptr_t) ( /*_.VALDATA___V340*/ meltfptr[339]));

  /*internsym:DEBUG_MSG_FUN */
  (void)
    meltgc_intern_symbol ((melt_ptr_t) ( /*_.VALDATA___V342*/ meltfptr[341]));

  /*internsym:DISPLAY_DEBUG_MESSAGE */
  (void)
    meltgc_intern_symbol ((melt_ptr_t) ( /*_.VALDATA___V344*/ meltfptr[343]));

  /*internsym:MELT_DEBUG_FUN */
  (void)
    meltgc_intern_symbol ((melt_ptr_t) ( /*_.VALDATA___V346*/ meltfptr[345]));

  /*internsym:REGISTER_BASICBLOCK_DEBUG_FUN */
  (void)
    meltgc_intern_symbol ((melt_ptr_t) ( /*_.VALDATA___V348*/ meltfptr[347]));

  /*internsym:REGISTER_EDGE_DEBUG_FUN */
  (void)
    meltgc_intern_symbol ((melt_ptr_t) ( /*_.VALDATA___V350*/ meltfptr[349]));

  /*internsym:REGISTER_GIMPLE_DEBUG_FUN */
  (void)
    meltgc_intern_symbol ((melt_ptr_t) ( /*_.VALDATA___V352*/ meltfptr[351]));

  /*internsym:REGISTER_GIMPLESEQ_DEBUG_FUN */
  (void)
    meltgc_intern_symbol ((melt_ptr_t) ( /*_.VALDATA___V354*/ meltfptr[353]));

  /*internsym:REGISTER_LOOP_DEBUG_FUN */
  (void)
    meltgc_intern_symbol ((melt_ptr_t) ( /*_.VALDATA___V356*/ meltfptr[355]));

  /*internsym:REGISTER_TREE_DEBUG_FUN */
  (void)
    meltgc_intern_symbol ((melt_ptr_t) ( /*_.VALDATA___V358*/ meltfptr[357]));

 /**COMMENT: set retinit from boxloc **/ ;

 /*_.RETINIT___V1*/ meltfptr[0] =
    /* finalsetretinit */
    melt_reference_value ((melt_ptr_t) ( /*_.CONTENV___V2*/ meltfptr[1]));
 /**COMMENT: end the initproc **/ ;


#undef meltfram__
}				/*end of meltmod__warmelt_debug__initialmeltchunk_6 */

void
meltmod__warmelt_debug__forward_or_mark_module_start_frame (struct
							    melt_callframe_st
							    *fp, int marking)
{
  int ix = 0;
  initial_frame_st *meltframptr_ = (initial_frame_st *) fp;
  melt_assertmsg ("check module frame",
		  meltframptr_->mcfr_nbvar == /*minihash */ -2505);
  if (!marking && melt_is_forwarding)
    {
      dbgprintf
	("forward_or_mark_module_start_frame_warmelt_debug forwarding %d pointers in frame %p",
	 398, (void *) meltframptr_);
      for (ix = 0; ix < 398; ix++)
	MELT_FORWARDED (meltframptr_->mcfr_varptr[ix]);
      return;
    }				/*end forwarding */
  dbgprintf
    ("forward_or_mark_module_start_frame_warmelt_debug marking in frame %p",
     (void *) meltframptr_);
  for (ix = 0; ix < 398; ix++)
    if (meltframptr_->mcfr_varptr[ix])
      gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);

}				/* end meltmod__warmelt_debug__forward_or_mark_module_start_frame */



/* exported 3 field offsets */
MELT_EXTERN const int meltfieldoff__DBGI_MAXDEPTH;
const int meltfieldoff__DBGI_MAXDEPTH = 2;	/* in CLASS_DEBUG_INFORMATION */
MELT_EXTERN const int meltfieldoff__DBGI_OCCMAP;
const int meltfieldoff__DBGI_OCCMAP = 1;	/* in CLASS_DEBUG_INFORMATION */
MELT_EXTERN const int meltfieldoff__DBGI_OUT;
const int meltfieldoff__DBGI_OUT = 0;	/* in CLASS_DEBUG_INFORMATION */

/* exported 2 class lengths */
MELT_EXTERN const int meltclasslen__CLASS_DEBUG_INFORMATION;
const int meltclasslen__CLASS_DEBUG_INFORMATION = 3;
MELT_EXTERN const int meltclasslen__CLASS_DEBUG_OUTPUT_INFORMATION;
const int meltclasslen__CLASS_DEBUG_OUTPUT_INFORMATION = 3;



/**** end of warmelt-debug ****/
