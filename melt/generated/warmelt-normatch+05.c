/* GCC MELT GENERATED FILE warmelt-normatch+05.c - DO NOT EDIT */
/* secondary MELT generated C file of rank #5 */
#include "melt-run.h"


/* used hash from melt-run.h when compiling this file: */
MELT_EXTERN const char meltrun_used_md5_melt_f5[] =
  MELT_RUN_HASHMD5 /* from melt-run.h */ ;


/**** warmelt-normatch+05.c declarations ****/


/***************************************************
***
    Copyright 2008, 2009, 2010, 2011, 2012 Free Software Foundation, Inc.
    Contributed by Basile Starynkevitch <basile@starynkevitch.net>

    This file is part of GCC.

    GCC is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3, or (at your option)
    any later version.

    GCC is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with GCC; see the file COPYING3.  If not see
    <http://www.gnu.org/licenses/>.
***
****************************************************/

/* ordinary MELT module */
#define MELT_HAS_INITIAL_ENVIRONMENT 1	/*usual */

struct melt_callframe_st;	/*defined in melt-runtime.h */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_1_warmelt_normatch_SCANPAT_ANYRECV (meltclosure_ptr_t meltclosp_,
					     melt_ptr_t meltfirstargp_,
					     const melt_argdescr_cell_t
					     meltxargdescr_[],
					     union meltparam_un *meltxargtab_,
					     const melt_argdescr_cell_t
					     meltxresdescr_[],
					     union meltparam_un
					     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_2_warmelt_normatch_SCANPAT_ANYPATTERN (meltclosure_ptr_t meltclosp_,
						melt_ptr_t meltfirstargp_,
						const melt_argdescr_cell_t
						meltxargdescr_[],
						union meltparam_un
						*meltxargtab_,
						const melt_argdescr_cell_t
						meltxresdescr_[],
						union meltparam_un
						*meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_3_warmelt_normatch_SCANPAT_SRCPATVAR (meltclosure_ptr_t meltclosp_,
					       melt_ptr_t meltfirstargp_,
					       const melt_argdescr_cell_t
					       meltxargdescr_[],
					       union meltparam_un
					       *meltxargtab_,
					       const melt_argdescr_cell_t
					       meltxresdescr_[],
					       union meltparam_un
					       *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_4_warmelt_normatch_SCANPAT_SRCPATJOKER (meltclosure_ptr_t meltclosp_,
						 melt_ptr_t meltfirstargp_,
						 const melt_argdescr_cell_t
						 meltxargdescr_[],
						 union meltparam_un
						 *meltxargtab_,
						 const melt_argdescr_cell_t
						 meltxresdescr_[],
						 union meltparam_un
						 *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_5_warmelt_normatch_SCANPAT_SRCPATCONSTANT (meltclosure_ptr_t
						    meltclosp_,
						    melt_ptr_t meltfirstargp_,
						    const melt_argdescr_cell_t
						    meltxargdescr_[],
						    union meltparam_un
						    *meltxargtab_,
						    const melt_argdescr_cell_t
						    meltxresdescr_[],
						    union meltparam_un
						    *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_6_warmelt_normatch_SCANPAT_SRCPATCONSTRUCT (meltclosure_ptr_t
						     meltclosp_,
						     melt_ptr_t
						     meltfirstargp_,
						     const
						     melt_argdescr_cell_t
						     meltxargdescr_[],
						     union meltparam_un
						     *meltxargtab_,
						     const
						     melt_argdescr_cell_t
						     meltxresdescr_[],
						     union meltparam_un
						     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_7_warmelt_normatch_SCANPAT_SRCPATOBJECT (meltclosure_ptr_t
						  meltclosp_,
						  melt_ptr_t meltfirstargp_,
						  const melt_argdescr_cell_t
						  meltxargdescr_[],
						  union meltparam_un
						  *meltxargtab_,
						  const melt_argdescr_cell_t
						  meltxresdescr_[],
						  union meltparam_un
						  *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_8_warmelt_normatch_SCANPAT_SRCPATCOMPOSITE (meltclosure_ptr_t
						     meltclosp_,
						     melt_ptr_t
						     meltfirstargp_,
						     const
						     melt_argdescr_cell_t
						     meltxargdescr_[],
						     union meltparam_un
						     *meltxargtab_,
						     const
						     melt_argdescr_cell_t
						     meltxresdescr_[],
						     union meltparam_un
						     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_9_warmelt_normatch_SCANPAT_SRCPATOR (meltclosure_ptr_t meltclosp_,
					      melt_ptr_t meltfirstargp_,
					      const melt_argdescr_cell_t
					      meltxargdescr_[],
					      union meltparam_un
					      *meltxargtab_,
					      const melt_argdescr_cell_t
					      meltxresdescr_[],
					      union meltparam_un
					      *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_10_warmelt_normatch_LAMBDA___1__ (meltclosure_ptr_t meltclosp_,
					   melt_ptr_t meltfirstargp_,
					   const melt_argdescr_cell_t
					   meltxargdescr_[],
					   union meltparam_un *meltxargtab_,
					   const melt_argdescr_cell_t
					   meltxresdescr_[],
					   union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_11_warmelt_normatch_SCANPAT_SRCPATAND (meltclosure_ptr_t meltclosp_,
						melt_ptr_t meltfirstargp_,
						const melt_argdescr_cell_t
						meltxargdescr_[],
						union meltparam_un
						*meltxargtab_,
						const melt_argdescr_cell_t
						meltxresdescr_[],
						union meltparam_un
						*meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_12_warmelt_normatch_REGISTER_NEW_NORMTESTER (meltclosure_ptr_t
						      meltclosp_,
						      melt_ptr_t
						      meltfirstargp_,
						      const
						      melt_argdescr_cell_t
						      meltxargdescr_[],
						      union meltparam_un
						      *meltxargtab_,
						      const
						      melt_argdescr_cell_t
						      meltxresdescr_[],
						      union meltparam_un
						      *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_13_warmelt_normatch_PUT_TESTER_THEN (meltclosure_ptr_t meltclosp_,
					      melt_ptr_t meltfirstargp_,
					      const melt_argdescr_cell_t
					      meltxargdescr_[],
					      union meltparam_un
					      *meltxargtab_,
					      const melt_argdescr_cell_t
					      meltxresdescr_[],
					      union meltparam_un
					      *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_14_warmelt_normatch_SET_NEW_TESTER_LAST_THEN (meltclosure_ptr_t
						       meltclosp_,
						       melt_ptr_t
						       meltfirstargp_,
						       const
						       melt_argdescr_cell_t
						       meltxargdescr_[],
						       union meltparam_un
						       *meltxargtab_,
						       const
						       melt_argdescr_cell_t
						       meltxresdescr_[],
						       union meltparam_un
						       *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_15_warmelt_normatch_SET_NEW_TESTER_ALL_ELSES (meltclosure_ptr_t
						       meltclosp_,
						       melt_ptr_t
						       meltfirstargp_,
						       const
						       melt_argdescr_cell_t
						       meltxargdescr_[],
						       union meltparam_un
						       *meltxargtab_,
						       const
						       melt_argdescr_cell_t
						       meltxresdescr_[],
						       union meltparam_un
						       *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_16_warmelt_normatch_NORMPAT_ANYRECV (meltclosure_ptr_t meltclosp_,
					      melt_ptr_t meltfirstargp_,
					      const melt_argdescr_cell_t
					      meltxargdescr_[],
					      union meltparam_un
					      *meltxargtab_,
					      const melt_argdescr_cell_t
					      meltxresdescr_[],
					      union meltparam_un
					      *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_17_warmelt_normatch_NORMPAT_ANYPAT (meltclosure_ptr_t meltclosp_,
					     melt_ptr_t meltfirstargp_,
					     const melt_argdescr_cell_t
					     meltxargdescr_[],
					     union meltparam_un *meltxargtab_,
					     const melt_argdescr_cell_t
					     meltxresdescr_[],
					     union meltparam_un
					     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_18_warmelt_normatch_NORMVARPAT_GENREUSETEST (meltclosure_ptr_t
						      meltclosp_,
						      melt_ptr_t
						      meltfirstargp_,
						      const
						      melt_argdescr_cell_t
						      meltxargdescr_[],
						      union meltparam_un
						      *meltxargtab_,
						      const
						      melt_argdescr_cell_t
						      meltxresdescr_[],
						      union meltparam_un
						      *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_19_warmelt_normatch_LAMBDA___2__ (meltclosure_ptr_t meltclosp_,
					   melt_ptr_t meltfirstargp_,
					   const melt_argdescr_cell_t
					   meltxargdescr_[],
					   union meltparam_un *meltxargtab_,
					   const melt_argdescr_cell_t
					   meltxresdescr_[],
					   union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_20_warmelt_normatch_NORMPAT_VARIABLEPAT (meltclosure_ptr_t
						  meltclosp_,
						  melt_ptr_t meltfirstargp_,
						  const melt_argdescr_cell_t
						  meltxargdescr_[],
						  union meltparam_un
						  *meltxargtab_,
						  const melt_argdescr_cell_t
						  meltxresdescr_[],
						  union meltparam_un
						  *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_21_warmelt_normatch_NORMPAT_JOKERPAT (meltclosure_ptr_t meltclosp_,
					       melt_ptr_t meltfirstargp_,
					       const melt_argdescr_cell_t
					       meltxargdescr_[],
					       union meltparam_un
					       *meltxargtab_,
					       const melt_argdescr_cell_t
					       meltxresdescr_[],
					       union meltparam_un
					       *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_22_warmelt_normatch_NORMPAT_INSTANCEPAT (meltclosure_ptr_t
						  meltclosp_,
						  melt_ptr_t meltfirstargp_,
						  const melt_argdescr_cell_t
						  meltxargdescr_[],
						  union meltparam_un
						  *meltxargtab_,
						  const melt_argdescr_cell_t
						  meltxresdescr_[],
						  union meltparam_un
						  *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_23_warmelt_normatch_LAMBDA___3__ (meltclosure_ptr_t meltclosp_,
					   melt_ptr_t meltfirstargp_,
					   const melt_argdescr_cell_t
					   meltxargdescr_[],
					   union meltparam_un *meltxargtab_,
					   const melt_argdescr_cell_t
					   meltxresdescr_[],
					   union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_24_warmelt_normatch_LAMBDA___4__ (meltclosure_ptr_t meltclosp_,
					   melt_ptr_t meltfirstargp_,
					   const melt_argdescr_cell_t
					   meltxargdescr_[],
					   union meltparam_un *meltxargtab_,
					   const melt_argdescr_cell_t
					   meltxresdescr_[],
					   union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_25_warmelt_normatch_LAMBDA___5__ (meltclosure_ptr_t meltclosp_,
					   melt_ptr_t meltfirstargp_,
					   const melt_argdescr_cell_t
					   meltxargdescr_[],
					   union meltparam_un *meltxargtab_,
					   const melt_argdescr_cell_t
					   meltxresdescr_[],
					   union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_26_warmelt_normatch_NORMPAT_TUPLEPAT (meltclosure_ptr_t meltclosp_,
					       melt_ptr_t meltfirstargp_,
					       const melt_argdescr_cell_t
					       meltxargdescr_[],
					       union meltparam_un
					       *meltxargtab_,
					       const melt_argdescr_cell_t
					       meltxresdescr_[],
					       union meltparam_un
					       *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_27_warmelt_normatch_LAMBDA___6__ (meltclosure_ptr_t meltclosp_,
					   melt_ptr_t meltfirstargp_,
					   const melt_argdescr_cell_t
					   meltxargdescr_[],
					   union meltparam_un *meltxargtab_,
					   const melt_argdescr_cell_t
					   meltxresdescr_[],
					   union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_28_warmelt_normatch_LAMBDA___7__ (meltclosure_ptr_t meltclosp_,
					   melt_ptr_t meltfirstargp_,
					   const melt_argdescr_cell_t
					   meltxargdescr_[],
					   union meltparam_un *meltxargtab_,
					   const melt_argdescr_cell_t
					   meltxresdescr_[],
					   union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_29_warmelt_normatch_LAMBDA___8__ (meltclosure_ptr_t meltclosp_,
					   melt_ptr_t meltfirstargp_,
					   const melt_argdescr_cell_t
					   meltxargdescr_[],
					   union meltparam_un *meltxargtab_,
					   const melt_argdescr_cell_t
					   meltxresdescr_[],
					   union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_30_warmelt_normatch_NORMPAT_ANDPAT (meltclosure_ptr_t meltclosp_,
					     melt_ptr_t meltfirstargp_,
					     const melt_argdescr_cell_t
					     meltxargdescr_[],
					     union meltparam_un *meltxargtab_,
					     const melt_argdescr_cell_t
					     meltxresdescr_[],
					     union meltparam_un
					     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_31_warmelt_normatch_LAMBDA___9__ (meltclosure_ptr_t meltclosp_,
					   melt_ptr_t meltfirstargp_,
					   const melt_argdescr_cell_t
					   meltxargdescr_[],
					   union meltparam_un *meltxargtab_,
					   const melt_argdescr_cell_t
					   meltxresdescr_[],
					   union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_32_warmelt_normatch_NORMPAT_ORPAT (meltclosure_ptr_t meltclosp_,
					    melt_ptr_t meltfirstargp_,
					    const melt_argdescr_cell_t
					    meltxargdescr_[],
					    union meltparam_un *meltxargtab_,
					    const melt_argdescr_cell_t
					    meltxresdescr_[],
					    union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_33_warmelt_normatch_LAMBDA___10__ (meltclosure_ptr_t meltclosp_,
					    melt_ptr_t meltfirstargp_,
					    const melt_argdescr_cell_t
					    meltxargdescr_[],
					    union meltparam_un *meltxargtab_,
					    const melt_argdescr_cell_t
					    meltxresdescr_[],
					    union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_34_warmelt_normatch_LAMBDA___11__ (meltclosure_ptr_t meltclosp_,
					    melt_ptr_t meltfirstargp_,
					    const melt_argdescr_cell_t
					    meltxargdescr_[],
					    union meltparam_un *meltxargtab_,
					    const melt_argdescr_cell_t
					    meltxresdescr_[],
					    union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_35_warmelt_normatch_LAMBDA___12__ (meltclosure_ptr_t meltclosp_,
					    melt_ptr_t meltfirstargp_,
					    const melt_argdescr_cell_t
					    meltxargdescr_[],
					    union meltparam_un *meltxargtab_,
					    const melt_argdescr_cell_t
					    meltxresdescr_[],
					    union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_36_warmelt_normatch_NORMPAT_ANYMATCHPAT (meltclosure_ptr_t
						  meltclosp_,
						  melt_ptr_t meltfirstargp_,
						  const melt_argdescr_cell_t
						  meltxargdescr_[],
						  union meltparam_un
						  *meltxargtab_,
						  const melt_argdescr_cell_t
						  meltxresdescr_[],
						  union meltparam_un
						  *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_37_warmelt_normatch_LAMBDA___13__ (meltclosure_ptr_t meltclosp_,
					    melt_ptr_t meltfirstargp_,
					    const melt_argdescr_cell_t
					    meltxargdescr_[],
					    union meltparam_un *meltxargtab_,
					    const melt_argdescr_cell_t
					    meltxresdescr_[],
					    union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_38_warmelt_normatch_LAMBDA___14__ (meltclosure_ptr_t meltclosp_,
					    melt_ptr_t meltfirstargp_,
					    const melt_argdescr_cell_t
					    meltxargdescr_[],
					    union meltparam_un *meltxargtab_,
					    const melt_argdescr_cell_t
					    meltxresdescr_[],
					    union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_39_warmelt_normatch_LAMBDA___15__ (meltclosure_ptr_t meltclosp_,
					    melt_ptr_t meltfirstargp_,
					    const melt_argdescr_cell_t
					    meltxargdescr_[],
					    union meltparam_un *meltxargtab_,
					    const melt_argdescr_cell_t
					    meltxresdescr_[],
					    union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_40_warmelt_normatch_LAMBDA___16__ (meltclosure_ptr_t meltclosp_,
					    melt_ptr_t meltfirstargp_,
					    const melt_argdescr_cell_t
					    meltxargdescr_[],
					    union meltparam_un *meltxargtab_,
					    const melt_argdescr_cell_t
					    meltxresdescr_[],
					    union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_41_warmelt_normatch_NORMPAT_CONSTPAT (meltclosure_ptr_t meltclosp_,
					       melt_ptr_t meltfirstargp_,
					       const melt_argdescr_cell_t
					       meltxargdescr_[],
					       union meltparam_un
					       *meltxargtab_,
					       const melt_argdescr_cell_t
					       meltxresdescr_[],
					       union meltparam_un
					       *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_42_warmelt_normatch_LAMBDA___17__ (meltclosure_ptr_t meltclosp_,
					    melt_ptr_t meltfirstargp_,
					    const melt_argdescr_cell_t
					    meltxargdescr_[],
					    union meltparam_un *meltxargtab_,
					    const melt_argdescr_cell_t
					    meltxresdescr_[],
					    union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_43_warmelt_normatch_MATCH_GRAPHIC_OPTSET (meltclosure_ptr_t
						   meltclosp_,
						   melt_ptr_t meltfirstargp_,
						   const melt_argdescr_cell_t
						   meltxargdescr_[],
						   union meltparam_un
						   *meltxargtab_,
						   const melt_argdescr_cell_t
						   meltxresdescr_[],
						   union meltparam_un
						   *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_44_warmelt_normatch_MG_OUT_NODE_NAME (meltclosure_ptr_t meltclosp_,
					       melt_ptr_t meltfirstargp_,
					       const melt_argdescr_cell_t
					       meltxargdescr_[],
					       union meltparam_un
					       *meltxargtab_,
					       const melt_argdescr_cell_t
					       meltxresdescr_[],
					       union meltparam_un
					       *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_45_warmelt_normatch_MGLABEL_ANY (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_46_warmelt_normatch_MGLABEL_ANY_TEST (meltclosure_ptr_t meltclosp_,
					       melt_ptr_t meltfirstargp_,
					       const melt_argdescr_cell_t
					       meltxargdescr_[],
					       union meltparam_un
					       *meltxargtab_,
					       const melt_argdescr_cell_t
					       meltxresdescr_[],
					       union meltparam_un
					       *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_47_warmelt_normatch_MGLABEL_INSTANCE_TEST (meltclosure_ptr_t
						    meltclosp_,
						    melt_ptr_t meltfirstargp_,
						    const melt_argdescr_cell_t
						    meltxargdescr_[],
						    union meltparam_un
						    *meltxargtab_,
						    const melt_argdescr_cell_t
						    meltxresdescr_[],
						    union meltparam_un
						    *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_48_warmelt_normatch_MGLABEL_SUCCESS_TEST (meltclosure_ptr_t
						   meltclosp_,
						   melt_ptr_t meltfirstargp_,
						   const melt_argdescr_cell_t
						   meltxargdescr_[],
						   union meltparam_un
						   *meltxargtab_,
						   const melt_argdescr_cell_t
						   meltxresdescr_[],
						   union meltparam_un
						   *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_49_warmelt_normatch_MATCHGRAPHIC_TESTS (meltclosure_ptr_t meltclosp_,
						 melt_ptr_t meltfirstargp_,
						 const melt_argdescr_cell_t
						 meltxargdescr_[],
						 union meltparam_un
						 *meltxargtab_,
						 const melt_argdescr_cell_t
						 meltxresdescr_[],
						 union meltparam_un
						 *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_50_warmelt_normatch_MG_DRAW_MATCH_GRAPHVIZ_FILE (meltclosure_ptr_t
							  meltclosp_,
							  melt_ptr_t
							  meltfirstargp_,
							  const
							  melt_argdescr_cell_t
							  meltxargdescr_[],
							  union meltparam_un
							  *meltxargtab_,
							  const
							  melt_argdescr_cell_t
							  meltxresdescr_[],
							  union meltparam_un
							  *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_51_warmelt_normatch_NORMEXP_MATCH (meltclosure_ptr_t meltclosp_,
					    melt_ptr_t meltfirstargp_,
					    const melt_argdescr_cell_t
					    meltxargdescr_[],
					    union meltparam_un *meltxargtab_,
					    const melt_argdescr_cell_t
					    meltxresdescr_[],
					    union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_52_warmelt_normatch_LAMBDA___18__ (meltclosure_ptr_t meltclosp_,
					    melt_ptr_t meltfirstargp_,
					    const melt_argdescr_cell_t
					    meltxargdescr_[],
					    union meltparam_un *meltxargtab_,
					    const melt_argdescr_cell_t
					    meltxresdescr_[],
					    union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_53_warmelt_normatch_NORMBIND_MATCHBIND (meltclosure_ptr_t meltclosp_,
						 melt_ptr_t meltfirstargp_,
						 const melt_argdescr_cell_t
						 meltxargdescr_[],
						 union meltparam_un
						 *meltxargtab_,
						 const melt_argdescr_cell_t
						 meltxresdescr_[],
						 union meltparam_un
						 *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_54_warmelt_normatch_MAKE_MATCH_FLAG (meltclosure_ptr_t meltclosp_,
					      melt_ptr_t meltfirstargp_,
					      const melt_argdescr_cell_t
					      meltxargdescr_[],
					      union meltparam_un
					      *meltxargtab_,
					      const melt_argdescr_cell_t
					      meltxresdescr_[],
					      union meltparam_un
					      *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_55_warmelt_normatch_SCANSUBPAT_ANYRECV (meltclosure_ptr_t meltclosp_,
						 melt_ptr_t meltfirstargp_,
						 const melt_argdescr_cell_t
						 meltxargdescr_[],
						 union meltparam_un
						 *meltxargtab_,
						 const melt_argdescr_cell_t
						 meltxresdescr_[],
						 union meltparam_un
						 *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_56_warmelt_normatch_SCANSTEPDATA_ANYRECV (meltclosure_ptr_t
						   meltclosp_,
						   melt_ptr_t meltfirstargp_,
						   const melt_argdescr_cell_t
						   meltxargdescr_[],
						   union meltparam_un
						   *meltxargtab_,
						   const melt_argdescr_cell_t
						   meltxresdescr_[],
						   union meltparam_un
						   *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_57_warmelt_normatch_SCANSTEPFLAG_ANYRECV (meltclosure_ptr_t
						   meltclosp_,
						   melt_ptr_t meltfirstargp_,
						   const melt_argdescr_cell_t
						   meltxargdescr_[],
						   union meltparam_un
						   *meltxargtab_,
						   const melt_argdescr_cell_t
						   meltxresdescr_[],
						   union meltparam_un
						   *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_58_warmelt_normatch_SCANSUBPAT_NOOP (meltclosure_ptr_t meltclosp_,
					      melt_ptr_t meltfirstargp_,
					      const melt_argdescr_cell_t
					      meltxargdescr_[],
					      union meltparam_un
					      *meltxargtab_,
					      const melt_argdescr_cell_t
					      meltxresdescr_[],
					      union meltparam_un
					      *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_59_warmelt_normatch_SCANSUBPAT_OR (meltclosure_ptr_t meltclosp_,
					    melt_ptr_t meltfirstargp_,
					    const melt_argdescr_cell_t
					    meltxargdescr_[],
					    union meltparam_un *meltxargtab_,
					    const melt_argdescr_cell_t
					    meltxresdescr_[],
					    union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_60_warmelt_normatch_SCANSUBPAT_AND (meltclosure_ptr_t meltclosp_,
					     melt_ptr_t meltfirstargp_,
					     const melt_argdescr_cell_t
					     meltxargdescr_[],
					     union meltparam_un *meltxargtab_,
					     const melt_argdescr_cell_t
					     meltxresdescr_[],
					     union meltparam_un
					     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_61_warmelt_normatch_SCANSUBPAT_CONSTRUCT (meltclosure_ptr_t
						   meltclosp_,
						   melt_ptr_t meltfirstargp_,
						   const melt_argdescr_cell_t
						   meltxargdescr_[],
						   union meltparam_un
						   *meltxargtab_,
						   const melt_argdescr_cell_t
						   meltxresdescr_[],
						   union meltparam_un
						   *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_62_warmelt_normatch_SCANSUBPAT_OBJECT (meltclosure_ptr_t meltclosp_,
						melt_ptr_t meltfirstargp_,
						const melt_argdescr_cell_t
						meltxargdescr_[],
						union meltparam_un
						*meltxargtab_,
						const melt_argdescr_cell_t
						meltxresdescr_[],
						union meltparam_un
						*meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_63_warmelt_normatch_FILL_MATCHCASE (meltclosure_ptr_t meltclosp_,
					     melt_ptr_t meltfirstargp_,
					     const melt_argdescr_cell_t
					     meltxargdescr_[],
					     union meltparam_un *meltxargtab_,
					     const melt_argdescr_cell_t
					     meltxresdescr_[],
					     union meltparam_un
					     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_64_warmelt_normatch_LAMBDA___19__ (meltclosure_ptr_t meltclosp_,
					    melt_ptr_t meltfirstargp_,
					    const melt_argdescr_cell_t
					    meltxargdescr_[],
					    union meltparam_un *meltxargtab_,
					    const melt_argdescr_cell_t
					    meltxresdescr_[],
					    union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_65_warmelt_normatch_PUTTHEN_MATCHANY (meltclosure_ptr_t meltclosp_,
					       melt_ptr_t meltfirstargp_,
					       const melt_argdescr_cell_t
					       meltxargdescr_[],
					       union meltparam_un
					       *meltxargtab_,
					       const melt_argdescr_cell_t
					       meltxresdescr_[],
					       union meltparam_un
					       *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_66_warmelt_normatch_START_STEP (meltclosure_ptr_t meltclosp_,
					 melt_ptr_t meltfirstargp_,
					 const melt_argdescr_cell_t
					 meltxargdescr_[],
					 union meltparam_un *meltxargtab_,
					 const melt_argdescr_cell_t
					 meltxresdescr_[],
					 union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_67_warmelt_normatch_PUTTHEN_MATCHTHEN (meltclosure_ptr_t meltclosp_,
						melt_ptr_t meltfirstargp_,
						const melt_argdescr_cell_t
						meltxargdescr_[],
						union meltparam_un
						*meltxargtab_,
						const melt_argdescr_cell_t
						meltxresdescr_[],
						union meltparam_un
						*meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_68_warmelt_normatch_PUTTHEN_MATCHGROUP (meltclosure_ptr_t meltclosp_,
						 melt_ptr_t meltfirstargp_,
						 const melt_argdescr_cell_t
						 meltxargdescr_[],
						 union meltparam_un
						 *meltxargtab_,
						 const melt_argdescr_cell_t
						 meltxresdescr_[],
						 union meltparam_un
						 *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_69_warmelt_normatch_PUTELSE_MATCHANY (meltclosure_ptr_t meltclosp_,
					       melt_ptr_t meltfirstargp_,
					       const melt_argdescr_cell_t
					       meltxargdescr_[],
					       union meltparam_un
					       *meltxargtab_,
					       const melt_argdescr_cell_t
					       meltxresdescr_[],
					       union meltparam_un
					       *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_70_warmelt_normatch_PUTELSE_MATCHSTEPTHEN (meltclosure_ptr_t
						    meltclosp_,
						    melt_ptr_t meltfirstargp_,
						    const melt_argdescr_cell_t
						    meltxargdescr_[],
						    union meltparam_un
						    *meltxargtab_,
						    const melt_argdescr_cell_t
						    meltxresdescr_[],
						    union meltparam_un
						    *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_71_warmelt_normatch_PUTELSE_MATCHTEST (meltclosure_ptr_t meltclosp_,
						melt_ptr_t meltfirstargp_,
						const melt_argdescr_cell_t
						meltxargdescr_[],
						union meltparam_un
						*meltxargtab_,
						const melt_argdescr_cell_t
						meltxresdescr_[],
						union meltparam_un
						*meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_72_warmelt_normatch_PUTELSE_MATCHGROUP (meltclosure_ptr_t meltclosp_,
						 melt_ptr_t meltfirstargp_,
						 const melt_argdescr_cell_t
						 meltxargdescr_[],
						 union meltparam_un
						 *meltxargtab_,
						 const melt_argdescr_cell_t
						 meltxresdescr_[],
						 union meltparam_un
						 *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_73_warmelt_normatch_SCANSTEPDATA_TESTINS (meltclosure_ptr_t
						   meltclosp_,
						   melt_ptr_t meltfirstargp_,
						   const melt_argdescr_cell_t
						   meltxargdescr_[],
						   union meltparam_un
						   *meltxargtab_,
						   const melt_argdescr_cell_t
						   meltxresdescr_[],
						   union meltparam_un
						   *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_74_warmelt_normatch_SCANSTEPDATA_TESTTUPLE (meltclosure_ptr_t
						     meltclosp_,
						     melt_ptr_t
						     meltfirstargp_,
						     const
						     melt_argdescr_cell_t
						     meltxargdescr_[],
						     union meltparam_un
						     *meltxargtab_,
						     const
						     melt_argdescr_cell_t
						     meltxresdescr_[],
						     union meltparam_un
						     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_75_warmelt_normatch_SCANSTEPDATA_TESTWITHFLAG (meltclosure_ptr_t
							meltclosp_,
							melt_ptr_t
							meltfirstargp_,
							const
							melt_argdescr_cell_t
							meltxargdescr_[],
							union meltparam_un
							*meltxargtab_,
							const
							melt_argdescr_cell_t
							meltxresdescr_[],
							union meltparam_un
							*meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_76_warmelt_normatch_SCANSTEPDATA_TESTMATCHER (meltclosure_ptr_t
						       meltclosp_,
						       melt_ptr_t
						       meltfirstargp_,
						       const
						       melt_argdescr_cell_t
						       meltxargdescr_[],
						       union meltparam_un
						       *meltxargtab_,
						       const
						       melt_argdescr_cell_t
						       meltxresdescr_[],
						       union meltparam_un
						       *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_77_warmelt_normatch_SCANSTEPFLAG_STEPWITHFLAG (meltclosure_ptr_t
							meltclosp_,
							melt_ptr_t
							meltfirstargp_,
							const
							melt_argdescr_cell_t
							meltxargdescr_[],
							union meltparam_un
							*meltxargtab_,
							const
							melt_argdescr_cell_t
							meltxresdescr_[],
							union meltparam_un
							*meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_78_warmelt_normatch_SCANSTEPFLAG_STEPFLAGOPER (meltclosure_ptr_t
							meltclosp_,
							melt_ptr_t
							meltfirstargp_,
							const
							melt_argdescr_cell_t
							meltxargdescr_[],
							union meltparam_un
							*meltxargtab_,
							const
							melt_argdescr_cell_t
							meltxresdescr_[],
							union meltparam_un
							*meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_79_warmelt_normatch_SCANSTEPFLAG_STEPWITHDATA (meltclosure_ptr_t
							meltclosp_,
							melt_ptr_t
							meltfirstargp_,
							const
							melt_argdescr_cell_t
							meltxargdescr_[],
							union meltparam_un
							*meltxargtab_,
							const
							melt_argdescr_cell_t
							meltxresdescr_[],
							union meltparam_un
							*meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_80_warmelt_normatch_TRANSLPAT_ANYRECV (meltclosure_ptr_t meltclosp_,
						melt_ptr_t meltfirstargp_,
						const melt_argdescr_cell_t
						meltxargdescr_[],
						union meltparam_un
						*meltxargtab_,
						const melt_argdescr_cell_t
						meltxresdescr_[],
						union meltparam_un
						*meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_81_warmelt_normatch_TRANSLPAT_JOKERPAT (meltclosure_ptr_t meltclosp_,
						 melt_ptr_t meltfirstargp_,
						 const melt_argdescr_cell_t
						 meltxargdescr_[],
						 union meltparam_un
						 *meltxargtab_,
						 const melt_argdescr_cell_t
						 meltxresdescr_[],
						 union meltparam_un
						 *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_82_warmelt_normatch_TRANSLPAT_CONSTPAT (meltclosure_ptr_t meltclosp_,
						 melt_ptr_t meltfirstargp_,
						 const melt_argdescr_cell_t
						 meltxargdescr_[],
						 union meltparam_un
						 *meltxargtab_,
						 const melt_argdescr_cell_t
						 meltxresdescr_[],
						 union meltparam_un
						 *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_83_warmelt_normatch_TRANSLPAT_LISTPAT (meltclosure_ptr_t meltclosp_,
						melt_ptr_t meltfirstargp_,
						const melt_argdescr_cell_t
						meltxargdescr_[],
						union meltparam_un
						*meltxargtab_,
						const melt_argdescr_cell_t
						meltxresdescr_[],
						union meltparam_un
						*meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_84_warmelt_normatch_TRANSLPAT_TUPLEPAT (meltclosure_ptr_t meltclosp_,
						 melt_ptr_t meltfirstargp_,
						 const melt_argdescr_cell_t
						 meltxargdescr_[],
						 union meltparam_un
						 *meltxargtab_,
						 const melt_argdescr_cell_t
						 meltxresdescr_[],
						 union meltparam_un
						 *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_85_warmelt_normatch_LAMBDA___20__ (meltclosure_ptr_t meltclosp_,
					    melt_ptr_t meltfirstargp_,
					    const melt_argdescr_cell_t
					    meltxargdescr_[],
					    union meltparam_un *meltxargtab_,
					    const melt_argdescr_cell_t
					    meltxresdescr_[],
					    union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_86_warmelt_normatch_TRANSLPAT_INSPAT (meltclosure_ptr_t meltclosp_,
					       melt_ptr_t meltfirstargp_,
					       const melt_argdescr_cell_t
					       meltxargdescr_[],
					       union meltparam_un
					       *meltxargtab_,
					       const melt_argdescr_cell_t
					       meltxresdescr_[],
					       union meltparam_un
					       *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_87_warmelt_normatch_TRANSLPAT_VARPAT (meltclosure_ptr_t meltclosp_,
					       melt_ptr_t meltfirstargp_,
					       const melt_argdescr_cell_t
					       meltxargdescr_[],
					       union meltparam_un
					       *meltxargtab_,
					       const melt_argdescr_cell_t
					       meltxresdescr_[],
					       union meltparam_un
					       *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_88_warmelt_normatch_TRANSLPAT_ANDPAT (meltclosure_ptr_t meltclosp_,
					       melt_ptr_t meltfirstargp_,
					       const melt_argdescr_cell_t
					       meltxargdescr_[],
					       union meltparam_un
					       *meltxargtab_,
					       const melt_argdescr_cell_t
					       meltxresdescr_[],
					       union meltparam_un
					       *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_89_warmelt_normatch_TRANSLPAT_ORPAT (meltclosure_ptr_t meltclosp_,
					      melt_ptr_t meltfirstargp_,
					      const melt_argdescr_cell_t
					      meltxargdescr_[],
					      union meltparam_un
					      *meltxargtab_,
					      const melt_argdescr_cell_t
					      meltxresdescr_[],
					      union meltparam_un
					      *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_90_warmelt_normatch_LAMBDA___21__ (meltclosure_ptr_t meltclosp_,
					    melt_ptr_t meltfirstargp_,
					    const melt_argdescr_cell_t
					    meltxargdescr_[],
					    union meltparam_un *meltxargtab_,
					    const melt_argdescr_cell_t
					    meltxresdescr_[],
					    union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_91_warmelt_normatch_LAMBDA___22__ (meltclosure_ptr_t meltclosp_,
					    melt_ptr_t meltfirstargp_,
					    const melt_argdescr_cell_t
					    meltxargdescr_[],
					    union meltparam_un *meltxargtab_,
					    const melt_argdescr_cell_t
					    meltxresdescr_[],
					    union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_92_warmelt_normatch_TRANSLPAT_PATMAT (meltclosure_ptr_t meltclosp_,
					       melt_ptr_t meltfirstargp_,
					       const melt_argdescr_cell_t
					       meltxargdescr_[],
					       union meltparam_un
					       *meltxargtab_,
					       const melt_argdescr_cell_t
					       meltxresdescr_[],
					       union meltparam_un
					       *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_93_warmelt_normatch_MGALTSTEP_ANY (meltclosure_ptr_t meltclosp_,
					    melt_ptr_t meltfirstargp_,
					    const melt_argdescr_cell_t
					    meltxargdescr_[],
					    union meltparam_un *meltxargtab_,
					    const melt_argdescr_cell_t
					    meltxresdescr_[],
					    union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_94_warmelt_normatch_MGALTSTEP_STEPTESTVAR (meltclosure_ptr_t
						    meltclosp_,
						    melt_ptr_t meltfirstargp_,
						    const melt_argdescr_cell_t
						    meltxargdescr_[],
						    union meltparam_un
						    *meltxargtab_,
						    const melt_argdescr_cell_t
						    meltxresdescr_[],
						    union meltparam_un
						    *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_95_warmelt_normatch_MGALTSTEP_STEPTESTINST (meltclosure_ptr_t
						     meltclosp_,
						     melt_ptr_t
						     meltfirstargp_,
						     const
						     melt_argdescr_cell_t
						     meltxargdescr_[],
						     union meltparam_un
						     *meltxargtab_,
						     const
						     melt_argdescr_cell_t
						     meltxresdescr_[],
						     union meltparam_un
						     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_96_warmelt_normatch_MGALTSTEP_STEPTESTMULT (meltclosure_ptr_t
						     meltclosp_,
						     melt_ptr_t
						     meltfirstargp_,
						     const
						     melt_argdescr_cell_t
						     meltxargdescr_[],
						     union meltparam_un
						     *meltxargtab_,
						     const
						     melt_argdescr_cell_t
						     meltxresdescr_[],
						     union meltparam_un
						     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_97_warmelt_normatch_MGALTSTEP_STEPTESTGROUP (meltclosure_ptr_t
						      meltclosp_,
						      melt_ptr_t
						      meltfirstargp_,
						      const
						      melt_argdescr_cell_t
						      meltxargdescr_[],
						      union meltparam_un
						      *meltxargtab_,
						      const
						      melt_argdescr_cell_t
						      meltxresdescr_[],
						      union meltparam_un
						      *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_98_warmelt_normatch_MGALTSTEP_STEPTESTMATCHER (meltclosure_ptr_t
							meltclosp_,
							melt_ptr_t
							meltfirstargp_,
							const
							melt_argdescr_cell_t
							meltxargdescr_[],
							union meltparam_un
							*meltxargtab_,
							const
							melt_argdescr_cell_t
							meltxresdescr_[],
							union meltparam_un
							*meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_99_warmelt_normatch_MGALTSTEP_STEPSUCCESS (meltclosure_ptr_t
						    meltclosp_,
						    melt_ptr_t meltfirstargp_,
						    const melt_argdescr_cell_t
						    meltxargdescr_[],
						    union meltparam_un
						    *meltxargtab_,
						    const melt_argdescr_cell_t
						    meltxresdescr_[],
						    union meltparam_un
						    *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_100_warmelt_normatch_MGALTSTEP_STEPCLEAR (meltclosure_ptr_t
						   meltclosp_,
						   melt_ptr_t meltfirstargp_,
						   const melt_argdescr_cell_t
						   meltxargdescr_[],
						   union meltparam_un
						   *meltxargtab_,
						   const melt_argdescr_cell_t
						   meltxresdescr_[],
						   union meltparam_un
						   *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_101_warmelt_normatch_MGALTSTEP_STEPFLAGSET (meltclosure_ptr_t
						     meltclosp_,
						     melt_ptr_t
						     meltfirstargp_,
						     const
						     melt_argdescr_cell_t
						     meltxargdescr_[],
						     union meltparam_un
						     *meltxargtab_,
						     const
						     melt_argdescr_cell_t
						     meltxresdescr_[],
						     union meltparam_un
						     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_102_warmelt_normatch_MGALTSTEP_STEPFLAGOPER (meltclosure_ptr_t
						      meltclosp_,
						      melt_ptr_t
						      meltfirstargp_,
						      const
						      melt_argdescr_cell_t
						      meltxargdescr_[],
						      union meltparam_un
						      *meltxargtab_,
						      const
						      melt_argdescr_cell_t
						      meltxresdescr_[],
						      union meltparam_un
						      *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_103_warmelt_normatch_TRANSLATE_MATCHCASE (meltclosure_ptr_t
						   meltclosp_,
						   melt_ptr_t meltfirstargp_,
						   const melt_argdescr_cell_t
						   meltxargdescr_[],
						   union meltparam_un
						   *meltxargtab_,
						   const melt_argdescr_cell_t
						   meltxresdescr_[],
						   union meltparam_un
						   *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_104_warmelt_normatch_LAMBDA___23__ (meltclosure_ptr_t meltclosp_,
					     melt_ptr_t meltfirstargp_,
					     const melt_argdescr_cell_t
					     meltxargdescr_[],
					     union meltparam_un *meltxargtab_,
					     const melt_argdescr_cell_t
					     meltxresdescr_[],
					     union meltparam_un
					     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_105_warmelt_normatch_ALTMATCH_NORMALIZE_FLAG (meltclosure_ptr_t
						       meltclosp_,
						       melt_ptr_t
						       meltfirstargp_,
						       const
						       melt_argdescr_cell_t
						       meltxargdescr_[],
						       union meltparam_un
						       *meltxargtab_,
						       const
						       melt_argdescr_cell_t
						       meltxresdescr_[],
						       union meltparam_un
						       *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_106_warmelt_normatch_MATCH_DATA_UPDATE_DATA_STEPS_INDEX
(meltclosure_ptr_t meltclosp_, melt_ptr_t meltfirstargp_, const melt_argdescr_cell_t meltxargdescr_[],
union meltparam_un *meltxargtab_, const melt_argdescr_cell_t meltxresdescr_[],
union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_107_warmelt_normatch_MATCH_STEP_INDEX (meltclosure_ptr_t meltclosp_,
						melt_ptr_t meltfirstargp_,
						const melt_argdescr_cell_t
						meltxargdescr_[],
						union meltparam_un
						*meltxargtab_,
						const melt_argdescr_cell_t
						meltxresdescr_[],
						union meltparam_un
						*meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_108_warmelt_normatch_COMPLETE_NORMSTEP_IF_LAST (meltclosure_ptr_t
							 meltclosp_,
							 melt_ptr_t
							 meltfirstargp_,
							 const
							 melt_argdescr_cell_t
							 meltxargdescr_[],
							 union meltparam_un
							 *meltxargtab_,
							 const
							 melt_argdescr_cell_t
							 meltxresdescr_[],
							 union meltparam_un
							 *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_109_warmelt_normatch_NORMSTEP_ANYRECV (meltclosure_ptr_t meltclosp_,
						melt_ptr_t meltfirstargp_,
						const melt_argdescr_cell_t
						meltxargdescr_[],
						union meltparam_un
						*meltxargtab_,
						const melt_argdescr_cell_t
						meltxresdescr_[],
						union meltparam_un
						*meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_110_warmelt_normatch_NORMSTEP_MFLAGSET (meltclosure_ptr_t meltclosp_,
						 melt_ptr_t meltfirstargp_,
						 const melt_argdescr_cell_t
						 meltxargdescr_[],
						 union meltparam_un
						 *meltxargtab_,
						 const melt_argdescr_cell_t
						 meltxresdescr_[],
						 union meltparam_un
						 *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_111_warmelt_normatch_NORMSTEP_MFLAGCONJ (meltclosure_ptr_t
						  meltclosp_,
						  melt_ptr_t meltfirstargp_,
						  const melt_argdescr_cell_t
						  meltxargdescr_[],
						  union meltparam_un
						  *meltxargtab_,
						  const melt_argdescr_cell_t
						  meltxresdescr_[],
						  union meltparam_un
						  *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_112_warmelt_normatch_NORMSTEP_MTESTINSTANCE (meltclosure_ptr_t
						      meltclosp_,
						      melt_ptr_t
						      meltfirstargp_,
						      const
						      melt_argdescr_cell_t
						      meltxargdescr_[],
						      union meltparam_un
						      *meltxargtab_,
						      const
						      melt_argdescr_cell_t
						      meltxresdescr_[],
						      union meltparam_un
						      *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_113_warmelt_normatch_NORMSTEP_MTESTMULTIPLE (meltclosure_ptr_t
						      meltclosp_,
						      melt_ptr_t
						      meltfirstargp_,
						      const
						      melt_argdescr_cell_t
						      meltxargdescr_[],
						      union meltparam_un
						      *meltxargtab_,
						      const
						      melt_argdescr_cell_t
						      meltxresdescr_[],
						      union meltparam_un
						      *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_114_warmelt_normatch_NORMSTEP_MTESTVAR (meltclosure_ptr_t meltclosp_,
						 melt_ptr_t meltfirstargp_,
						 const melt_argdescr_cell_t
						 meltxargdescr_[],
						 union meltparam_un
						 *meltxargtab_,
						 const melt_argdescr_cell_t
						 meltxresdescr_[],
						 union meltparam_un
						 *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_115_warmelt_normatch_NORMSTEP_MGROUP (meltclosure_ptr_t meltclosp_,
					       melt_ptr_t meltfirstargp_,
					       const melt_argdescr_cell_t
					       meltxargdescr_[],
					       union meltparam_un
					       *meltxargtab_,
					       const melt_argdescr_cell_t
					       meltxresdescr_[],
					       union meltparam_un
					       *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_116_warmelt_normatch_LAMBDA___24__ (meltclosure_ptr_t meltclosp_,
					     melt_ptr_t meltfirstargp_,
					     const melt_argdescr_cell_t
					     meltxargdescr_[],
					     union meltparam_un *meltxargtab_,
					     const melt_argdescr_cell_t
					     meltxresdescr_[],
					     union meltparam_un
					     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_117_warmelt_normatch_LAMBDA___25__ (meltclosure_ptr_t meltclosp_,
					     melt_ptr_t meltfirstargp_,
					     const melt_argdescr_cell_t
					     meltxargdescr_[],
					     union meltparam_un *meltxargtab_,
					     const melt_argdescr_cell_t
					     meltxresdescr_[],
					     union meltparam_un
					     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_118_warmelt_normatch_LAMBDA___26__ (meltclosure_ptr_t meltclosp_,
					     melt_ptr_t meltfirstargp_,
					     const melt_argdescr_cell_t
					     meltxargdescr_[],
					     union meltparam_un *meltxargtab_,
					     const melt_argdescr_cell_t
					     meltxresdescr_[],
					     union meltparam_un
					     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_119_warmelt_normatch_LAMBDA___27__ (meltclosure_ptr_t meltclosp_,
					     melt_ptr_t meltfirstargp_,
					     const melt_argdescr_cell_t
					     meltxargdescr_[],
					     union meltparam_un *meltxargtab_,
					     const melt_argdescr_cell_t
					     meltxresdescr_[],
					     union meltparam_un
					     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_120_warmelt_normatch_NORMSTEP_MSUCCWHENFLAG (meltclosure_ptr_t
						      meltclosp_,
						      melt_ptr_t
						      meltfirstargp_,
						      const
						      melt_argdescr_cell_t
						      meltxargdescr_[],
						      union meltparam_un
						      *meltxargtab_,
						      const
						      melt_argdescr_cell_t
						      meltxresdescr_[],
						      union meltparam_un
						      *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_121_warmelt_normatch_NORMTESTMATCH_CATCHALL (meltclosure_ptr_t
						      meltclosp_,
						      melt_ptr_t
						      meltfirstargp_,
						      const
						      melt_argdescr_cell_t
						      meltxargdescr_[],
						      union meltparam_un
						      *meltxargtab_,
						      const
						      melt_argdescr_cell_t
						      meltxresdescr_[],
						      union meltparam_un
						      *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_122_warmelt_normatch_NORMFILLMATCH_CATCHALL (meltclosure_ptr_t
						      meltclosp_,
						      melt_ptr_t
						      meltfirstargp_,
						      const
						      melt_argdescr_cell_t
						      meltxargdescr_[],
						      union meltparam_un
						      *meltxargtab_,
						      const
						      melt_argdescr_cell_t
						      meltxresdescr_[],
						      union meltparam_un
						      *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_123_warmelt_normatch_NORMTESTMATCH_CMATCHER (meltclosure_ptr_t
						      meltclosp_,
						      melt_ptr_t
						      meltfirstargp_,
						      const
						      melt_argdescr_cell_t
						      meltxargdescr_[],
						      union meltparam_un
						      *meltxargtab_,
						      const
						      melt_argdescr_cell_t
						      meltxresdescr_[],
						      union meltparam_un
						      *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_124_warmelt_normatch_NORMFILLMATCH_CMATCHER (meltclosure_ptr_t
						      meltclosp_,
						      melt_ptr_t
						      meltfirstargp_,
						      const
						      melt_argdescr_cell_t
						      meltxargdescr_[],
						      union meltparam_un
						      *meltxargtab_,
						      const
						      melt_argdescr_cell_t
						      meltxresdescr_[],
						      union meltparam_un
						      *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_125_warmelt_normatch_NORMSTEP_MTESTMATCHER (meltclosure_ptr_t
						     meltclosp_,
						     melt_ptr_t
						     meltfirstargp_,
						     const
						     melt_argdescr_cell_t
						     meltxargdescr_[],
						     union meltparam_un
						     *meltxargtab_,
						     const
						     melt_argdescr_cell_t
						     meltxresdescr_[],
						     union meltparam_un
						     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_126_warmelt_normatch_ALTMATCH_NORMALIZE_STEP (meltclosure_ptr_t
						       meltclosp_,
						       melt_ptr_t
						       meltfirstargp_,
						       const
						       melt_argdescr_cell_t
						       meltxargdescr_[],
						       union meltparam_un
						       *meltxargtab_,
						       const
						       melt_argdescr_cell_t
						       meltxresdescr_[],
						       union meltparam_un
						       *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_127_warmelt_normatch_ALTMATCH_NORMALIZE_MDATA (meltclosure_ptr_t
							meltclosp_,
							melt_ptr_t
							meltfirstargp_,
							const
							melt_argdescr_cell_t
							meltxargdescr_[],
							union meltparam_un
							*meltxargtab_,
							const
							melt_argdescr_cell_t
							meltxresdescr_[],
							union meltparam_un
							*meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_128_warmelt_normatch_ALTMATCH_HANDLE_NORMALIZED_MDATA
(meltclosure_ptr_t meltclosp_, melt_ptr_t meltfirstargp_, const melt_argdescr_cell_t meltxargdescr_[],
union meltparam_un *meltxargtab_, const melt_argdescr_cell_t meltxresdescr_[],
union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_129_warmelt_normatch_ALTMATCH_MAKE_MATCH_NORMALIZATION_CONTEXT
(meltclosure_ptr_t meltclosp_, melt_ptr_t meltfirstargp_, const melt_argdescr_cell_t meltxargdescr_[],
union meltparam_un *meltxargtab_, const melt_argdescr_cell_t meltxresdescr_[],
union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_130_warmelt_normatch_ALTMATCH_NORMALIZE_MATCH_CONTEXT
(meltclosure_ptr_t meltclosp_, melt_ptr_t meltfirstargp_, const melt_argdescr_cell_t meltxargdescr_[],
union meltparam_un *meltxargtab_, const melt_argdescr_cell_t meltxresdescr_[],
union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_131_warmelt_normatch_LAMBDA___28__ (meltclosure_ptr_t meltclosp_,
					     melt_ptr_t meltfirstargp_,
					     const melt_argdescr_cell_t
					     meltxargdescr_[],
					     union meltparam_un *meltxargtab_,
					     const melt_argdescr_cell_t
					     meltxresdescr_[],
					     union meltparam_un
					     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_132_warmelt_normatch_NORMEXP_ALTMATCH (meltclosure_ptr_t meltclosp_,
						melt_ptr_t meltfirstargp_,
						const melt_argdescr_cell_t
						meltxargdescr_[],
						union meltparam_un
						*meltxargtab_,
						const melt_argdescr_cell_t
						meltxresdescr_[],
						union meltparam_un
						*meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_133_warmelt_normatch_LAMBDA___29__ (meltclosure_ptr_t meltclosp_,
					     melt_ptr_t meltfirstargp_,
					     const melt_argdescr_cell_t
					     meltxargdescr_[],
					     union meltparam_un *meltxargtab_,
					     const melt_argdescr_cell_t
					     meltxresdescr_[],
					     union meltparam_un
					     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_134_warmelt_normatch_ALTERNATE_MATCH_OPTSET (meltclosure_ptr_t
						      meltclosp_,
						      melt_ptr_t
						      meltfirstargp_,
						      const
						      melt_argdescr_cell_t
						      meltxargdescr_[],
						      union meltparam_un
						      *meltxargtab_,
						      const
						      melt_argdescr_cell_t
						      meltxresdescr_[],
						      union meltparam_un
						      *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_135_warmelt_normatch_MG_ALTDRAW_GRAPHVIZ (meltclosure_ptr_t
						   meltclosp_,
						   melt_ptr_t meltfirstargp_,
						   const melt_argdescr_cell_t
						   meltxargdescr_[],
						   union meltparam_un
						   *meltxargtab_,
						   const melt_argdescr_cell_t
						   meltxresdescr_[],
						   union meltparam_un
						   *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_136_warmelt_normatch_LAMBDA___30__ (meltclosure_ptr_t meltclosp_,
					     melt_ptr_t meltfirstargp_,
					     const melt_argdescr_cell_t
					     meltxargdescr_[],
					     union meltparam_un *meltxargtab_,
					     const melt_argdescr_cell_t
					     meltxresdescr_[],
					     union meltparam_un
					     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_137_warmelt_normatch_LAMBDA___31__ (meltclosure_ptr_t meltclosp_,
					     melt_ptr_t meltfirstargp_,
					     const melt_argdescr_cell_t
					     meltxargdescr_[],
					     union meltparam_un *meltxargtab_,
					     const melt_argdescr_cell_t
					     meltxresdescr_[],
					     union meltparam_un
					     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_138_warmelt_normatch_LAMBDA___32__ (meltclosure_ptr_t meltclosp_,
					     melt_ptr_t meltfirstargp_,
					     const melt_argdescr_cell_t
					     meltxargdescr_[],
					     union meltparam_un *meltxargtab_,
					     const melt_argdescr_cell_t
					     meltxresdescr_[],
					     union meltparam_un
					     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_139_warmelt_normatch_LAMBDA___33__ (meltclosure_ptr_t meltclosp_,
					     melt_ptr_t meltfirstargp_,
					     const melt_argdescr_cell_t
					     meltxargdescr_[],
					     union meltparam_un *meltxargtab_,
					     const melt_argdescr_cell_t
					     meltxresdescr_[],
					     union meltparam_un
					     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_140_warmelt_normatch_LAMBDA___34__ (meltclosure_ptr_t meltclosp_,
					     melt_ptr_t meltfirstargp_,
					     const melt_argdescr_cell_t
					     meltxargdescr_[],
					     union meltparam_un *meltxargtab_,
					     const melt_argdescr_cell_t
					     meltxresdescr_[],
					     union meltparam_un
					     *meltxrestab_);



MELT_EXTERN void *melt_start_this_module (void *);


/* define different names when debugging or not */
#if MELT_HAVE_DEBUG
MELT_EXTERN const char meltmodule_warmelt_normatch__melt_have_debug_enabled[];
#define melt_have_debug_string meltmodule_warmelt_normatch__melt_have_debug_enabled
#else /*!MELT_HAVE_DEBUG */
MELT_EXTERN const char
  meltmodule_warmelt_normatch__melt_have_debug_disabled[];
#define melt_have_debug_string meltmodule_warmelt_normatch__melt_have_debug_disabled
#endif /*!MELT_HAVE_DEBUG */


struct frame_melt_start_this_module_st;
void MELT_MODULE_VISIBILITY
meltmod__warmelt_normatch__initialmeltchunk_0 (struct
					       frame_melt_start_this_module_st
					       *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_normatch__initialmeltchunk_1 (struct
					       frame_melt_start_this_module_st
					       *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_normatch__initialmeltchunk_2 (struct
					       frame_melt_start_this_module_st
					       *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_normatch__initialmeltchunk_3 (struct
					       frame_melt_start_this_module_st
					       *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_normatch__initialmeltchunk_4 (struct
					       frame_melt_start_this_module_st
					       *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_normatch__initialmeltchunk_5 (struct
					       frame_melt_start_this_module_st
					       *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_normatch__initialmeltchunk_6 (struct
					       frame_melt_start_this_module_st
					       *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_normatch__initialmeltchunk_7 (struct
					       frame_melt_start_this_module_st
					       *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_normatch__initialmeltchunk_8 (struct
					       frame_melt_start_this_module_st
					       *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_normatch__initialmeltchunk_9 (struct
					       frame_melt_start_this_module_st
					       *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_normatch__initialmeltchunk_10 (struct
						frame_melt_start_this_module_st
						*, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_normatch__initialmeltchunk_11 (struct
						frame_melt_start_this_module_st
						*, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_normatch__initialmeltchunk_12 (struct
						frame_melt_start_this_module_st
						*, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_normatch__initialmeltchunk_13 (struct
						frame_melt_start_this_module_st
						*, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_normatch__initialmeltchunk_14 (struct
						frame_melt_start_this_module_st
						*, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_normatch__initialmeltchunk_15 (struct
						frame_melt_start_this_module_st
						*, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_normatch__initialmeltchunk_16 (struct
						frame_melt_start_this_module_st
						*, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_normatch__initialmeltchunk_17 (struct
						frame_melt_start_this_module_st
						*, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_normatch__initialmeltchunk_18 (struct
						frame_melt_start_this_module_st
						*, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_normatch__initialmeltchunk_19 (struct
						frame_melt_start_this_module_st
						*, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_normatch__initialmeltchunk_20 (struct
						frame_melt_start_this_module_st
						*, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_normatch__initialmeltchunk_21 (struct
						frame_melt_start_this_module_st
						*, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_normatch__initialmeltchunk_22 (struct
						frame_melt_start_this_module_st
						*, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_normatch__initialmeltchunk_23 (struct
						frame_melt_start_this_module_st
						*, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_normatch__initialmeltchunk_24 (struct
						frame_melt_start_this_module_st
						*, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_normatch__initialmeltchunk_25 (struct
						frame_melt_start_this_module_st
						*, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_normatch__initialmeltchunk_26 (struct
						frame_melt_start_this_module_st
						*, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_normatch__initialmeltchunk_27 (struct
						frame_melt_start_this_module_st
						*, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_normatch__initialmeltchunk_28 (struct
						frame_melt_start_this_module_st
						*, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_normatch__initialmeltchunk_29 (struct
						frame_melt_start_this_module_st
						*, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_normatch__initialmeltchunk_30 (struct
						frame_melt_start_this_module_st
						*, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_normatch__initialmeltchunk_31 (struct
						frame_melt_start_this_module_st
						*, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_normatch__initialmeltchunk_32 (struct
						frame_melt_start_this_module_st
						*, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_normatch__initialmeltchunk_33 (struct
						frame_melt_start_this_module_st
						*, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_normatch__forward_or_mark_module_start_frame (struct
							       melt_callframe_st
							       *fp,
							       int marking);
#define meltmarking_melt_start_this_module  meltmod__warmelt_normatch__forward_or_mark_module_start_frame



/**** warmelt-normatch+05.c implementations ****/




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_133_warmelt_normatch_LAMBDA___29__ (meltclosure_ptr_t meltclosp_,
					     melt_ptr_t meltfirstargp_,
					     const melt_argdescr_cell_t
					     meltxargdescr_[],
					     union meltparam_un *meltxargtab_,
					     const melt_argdescr_cell_t
					     meltxresdescr_[],
					     union meltparam_un *meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_133_warmelt_normatch_LAMBDA___29___melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_133_warmelt_normatch_LAMBDA___29___st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 12
    melt_ptr_t mcfr_varptr[12];
#define MELTFRAM_NBVARNUM 5
    long mcfr_varnum[5];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_133_warmelt_normatch_LAMBDA___29__ is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_133_warmelt_normatch_LAMBDA___29___st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 12; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_133_warmelt_normatch_LAMBDA___29__ nbval 12*/
  meltfram__.mcfr_nbvar = 12 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("LAMBDA_", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-normatch.melt:6709:/ getarg");
 /*_.NF1__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.NF2__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.NF2__V3*/ meltfptr[2])) != NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-normatch.melt:6710:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L1*/ meltfnum[0] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.NF1__V2*/ meltfptr[1]),
			     (melt_ptr_t) (( /*!CLASS_NREP_MATCH_FLAG */
					    meltfrout->tabval[0])));;
      MELT_LOCATION ("warmelt-normatch.melt:6710:/ cond");
      /*cond */ if ( /*_#IS_A__L1*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V5*/ meltfptr[4] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-normatch.melt:6710:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check nf1"),
				  ("warmelt-normatch.melt")
				  ? ("warmelt-normatch.melt") : __FILE__,
				  (6710) ? (6710) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V5*/ meltfptr[4] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V4*/ meltfptr[3] = /*_.IFELSE___V5*/ meltfptr[4];;
      /*epilog */

      MELT_LOCATION ("warmelt-normatch.melt:6710:/ clear");
	     /*clear *//*_#IS_A__L1*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V5*/ meltfptr[4] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V4*/ meltfptr[3] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-normatch.melt:6711:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L2*/ meltfnum[0] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.NF2__V3*/ meltfptr[2]),
			     (melt_ptr_t) (( /*!CLASS_NREP_MATCH_FLAG */
					    meltfrout->tabval[0])));;
      MELT_LOCATION ("warmelt-normatch.melt:6711:/ cond");
      /*cond */ if ( /*_#IS_A__L2*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V7*/ meltfptr[6] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-normatch.melt:6711:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check nf2"),
				  ("warmelt-normatch.melt")
				  ? ("warmelt-normatch.melt") : __FILE__,
				  (6711) ? (6711) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V7*/ meltfptr[6] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V6*/ meltfptr[4] = /*_.IFELSE___V7*/ meltfptr[6];;
      /*epilog */

      MELT_LOCATION ("warmelt-normatch.melt:6711:/ clear");
	     /*clear *//*_#IS_A__L2*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V7*/ meltfptr[6] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V6*/ meltfptr[4] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-normatch.melt:6713:/ cond");
    /*cond */ if (
		   /*ifisa */
		   melt_is_instance_of ((melt_ptr_t)
					( /*_.NF1__V2*/ meltfptr[1]),
					(melt_ptr_t) (( /*!CLASS_NREP_MATCH_FLAG */ meltfrout->tabval[0])))
      )				/*then */
      {
	/*^cond.then */
	/*^getslot */
	{
	  melt_ptr_t slot = NULL, obj = NULL;
	  obj = (melt_ptr_t) ( /*_.NF1__V2*/ meltfptr[1]) /*=obj*/ ;
	  melt_object_get_field (slot, obj, 1, "NRPFLA_FLAG");
   /*_.NRPFLA_FLAG__V8*/ meltfptr[6] = slot;
	};
	;
      }
    else
      {				/*^cond.else */

  /*_.NRPFLA_FLAG__V8*/ meltfptr[6] = NULL;;
      }
    ;
    MELT_LOCATION ("warmelt-normatch.melt:6713:/ cond");
    /*cond */ if (
		   /*ifisa */
		   melt_is_instance_of ((melt_ptr_t)
					( /*_.NRPFLA_FLAG__V8*/ meltfptr[6]),
					(melt_ptr_t) (( /*!CLASS_MATCH_FLAG */
						       meltfrout->tabval[2])))
      )				/*then */
      {
	/*^cond.then */
	/*^getslot */
	{
	  melt_ptr_t slot = NULL, obj = NULL;
	  obj = (melt_ptr_t) ( /*_.NRPFLA_FLAG__V8*/ meltfptr[6]) /*=obj*/ ;
	  melt_object_get_field (slot, obj, 3, "MFLAG_RANK");
   /*_.MFLAG_RANK__V9*/ meltfptr[8] = slot;
	};
	;
      }
    else
      {				/*^cond.else */

  /*_.MFLAG_RANK__V9*/ meltfptr[8] = NULL;;
      }
    ;
    /*^compute */
 /*_#GET_INT__L3*/ meltfnum[0] =
      (melt_get_int ((melt_ptr_t) ( /*_.MFLAG_RANK__V9*/ meltfptr[8])));;
    MELT_LOCATION ("warmelt-normatch.melt:6714:/ cond");
    /*cond */ if (
		   /*ifisa */
		   melt_is_instance_of ((melt_ptr_t)
					( /*_.NF2__V3*/ meltfptr[2]),
					(melt_ptr_t) (( /*!CLASS_NREP_MATCH_FLAG */ meltfrout->tabval[0])))
      )				/*then */
      {
	/*^cond.then */
	/*^getslot */
	{
	  melt_ptr_t slot = NULL, obj = NULL;
	  obj = (melt_ptr_t) ( /*_.NF2__V3*/ meltfptr[2]) /*=obj*/ ;
	  melt_object_get_field (slot, obj, 1, "NRPFLA_FLAG");
   /*_.NRPFLA_FLAG__V10*/ meltfptr[9] = slot;
	};
	;
      }
    else
      {				/*^cond.else */

  /*_.NRPFLA_FLAG__V10*/ meltfptr[9] = NULL;;
      }
    ;
    MELT_LOCATION ("warmelt-normatch.melt:6714:/ cond");
    /*cond */ if (
		   /*ifisa */
		   melt_is_instance_of ((melt_ptr_t)
					( /*_.NRPFLA_FLAG__V10*/ meltfptr[9]),
					(melt_ptr_t) (( /*!CLASS_MATCH_FLAG */
						       meltfrout->tabval[2])))
      )				/*then */
      {
	/*^cond.then */
	/*^getslot */
	{
	  melt_ptr_t slot = NULL, obj = NULL;
	  obj = (melt_ptr_t) ( /*_.NRPFLA_FLAG__V10*/ meltfptr[9]) /*=obj*/ ;
	  melt_object_get_field (slot, obj, 3, "MFLAG_RANK");
   /*_.MFLAG_RANK__V11*/ meltfptr[10] = slot;
	};
	;
      }
    else
      {				/*^cond.else */

  /*_.MFLAG_RANK__V11*/ meltfptr[10] = NULL;;
      }
    ;
    /*^compute */
 /*_#GET_INT__L4*/ meltfnum[3] =
      (melt_get_int ((melt_ptr_t) ( /*_.MFLAG_RANK__V11*/ meltfptr[10])));;
    /*^compute */
 /*_#I__L5*/ meltfnum[4] =
      (( /*_#GET_INT__L3*/ meltfnum[0]) - ( /*_#GET_INT__L4*/ meltfnum[3]));;
    MELT_LOCATION ("warmelt-normatch.melt:6712:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] =
      ( /*!konst_1_TRUE */ meltfrout->tabval[1]);;
    MELT_LOCATION ("warmelt-normatch.melt:6712:/ putxtraresult");
    if (!meltxrestab_ || !meltxresdescr_)
      goto labend_rout;
    if (meltxresdescr_[0] != MELTBPAR_LONG)
      goto labend_rout;
    if (meltxrestab_[0].meltbp_longptr)
      *(meltxrestab_[0].meltbp_longptr) = ( /*_#I__L5*/ meltfnum[4]);
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    MELT_LOCATION ("warmelt-normatch.melt:6709:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.RETURN___V12*/ meltfptr[11];;

    {
      MELT_LOCATION ("warmelt-normatch.melt:6709:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V4*/ meltfptr[3] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V6*/ meltfptr[4] = 0;
    /*^clear */
	   /*clear *//*_.NRPFLA_FLAG__V8*/ meltfptr[6] = 0;
    /*^clear */
	   /*clear *//*_.MFLAG_RANK__V9*/ meltfptr[8] = 0;
    /*^clear */
	   /*clear *//*_#GET_INT__L3*/ meltfnum[0] = 0;
    /*^clear */
	   /*clear *//*_.NRPFLA_FLAG__V10*/ meltfptr[9] = 0;
    /*^clear */
	   /*clear *//*_.MFLAG_RANK__V11*/ meltfptr[10] = 0;
    /*^clear */
	   /*clear *//*_#GET_INT__L4*/ meltfnum[3] = 0;
    /*^clear */
	   /*clear *//*_#I__L5*/ meltfnum[4] = 0;
    /*^clear */
	   /*clear *//*_.RETURN___V12*/ meltfptr[11] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("LAMBDA_", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_133_warmelt_normatch_LAMBDA___29___melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_133_warmelt_normatch_LAMBDA___29__ */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_134_warmelt_normatch_ALTERNATE_MATCH_OPTSET (meltclosure_ptr_t
						      meltclosp_,
						      melt_ptr_t
						      meltfirstargp_,
						      const
						      melt_argdescr_cell_t
						      meltxargdescr_[],
						      union meltparam_un *
						      meltxargtab_,
						      const
						      melt_argdescr_cell_t
						      meltxresdescr_[],
						      union meltparam_un *
						      meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_134_warmelt_normatch_ALTERNATE_MATCH_OPTSET_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_134_warmelt_normatch_ALTERNATE_MATCH_OPTSET_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 7
    melt_ptr_t mcfr_varptr[7];
#define MELTFRAM_NBVARNUM 2
    long mcfr_varnum[2];
/*others*/
    const char *loc_CSTRING__o0;
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_134_warmelt_normatch_ALTERNATE_MATCH_OPTSET is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_134_warmelt_normatch_ALTERNATE_MATCH_OPTSET_st
	 *) meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 7; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_134_warmelt_normatch_ALTERNATE_MATCH_OPTSET nbval 7*/
  meltfram__.mcfr_nbvar = 7 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("ALTERNATE_MATCH_OPTSET", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-normatch.melt:6773:/ getarg");
 /*_.OPTSYMB__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_CSTRING)
    goto lab_endgetargs;
 /*_?*/ meltfram__.loc_CSTRING__o0 = meltxargtab_[0].meltbp_cstring;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-normatch.melt:6774:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L1*/ meltfnum[0] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-normatch.melt:6774:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L1*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-normatch.melt:6774:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-normatch.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 6774;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "alternate_match_optset optsymb";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.OPTSYMB__V2*/ meltfptr[1];
	      /*_.MELT_DEBUG_FUN__V5*/ meltfptr[4] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V4*/ meltfptr[3] = /*_.MELT_DEBUG_FUN__V5*/ meltfptr[4];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-normatch.melt:6774:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V5*/ meltfptr[4] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V4*/ meltfptr[3] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-normatch.melt:6774:/ quasiblock");


      /*_.PROGN___V6*/ meltfptr[4] = /*_.IF___V4*/ meltfptr[3];;
      /*^compute */
      /*_.IFCPP___V3*/ meltfptr[2] = /*_.PROGN___V6*/ meltfptr[4];;
      /*epilog */

      MELT_LOCATION ("warmelt-normatch.melt:6774:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L1*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IF___V4*/ meltfptr[3] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V6*/ meltfptr[4] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V3*/ meltfptr[2] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

    {
      MELT_LOCATION ("warmelt-normatch.melt:6775:/ locexp");
      inform (UNKNOWN_LOCATION, "MELT INFORM [#%ld]: %s", melt_dbgcounter,
	      ("enabling alternate matching implementation"));
    }
    ;
    MELT_LOCATION ("warmelt-normatch.melt:6776:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[2];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & ( /*!NORMAL_EXP */ meltfrout->tabval[3]);
      /*^apply.arg */
      argtab[1].meltbp_aptr =
	(melt_ptr_t *) & ( /*!NORMEXP_ALTMATCH */ meltfrout->tabval[4]);
      /*_.INSTALL_METHOD__V7*/ meltfptr[3] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!INSTALL_METHOD */ meltfrout->tabval[1])),
		    (melt_ptr_t) (( /*!CLASS_SOURCE_MATCH */ meltfrout->
				   tabval[2])),
		    (MELTBPARSTR_PTR MELTBPARSTR_PTR ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-normatch.melt:6773:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.INSTALL_METHOD__V7*/ meltfptr[3];;

    {
      MELT_LOCATION ("warmelt-normatch.melt:6773:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V3*/ meltfptr[2] = 0;
    /*^clear */
	   /*clear *//*_.INSTALL_METHOD__V7*/ meltfptr[3] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("ALTERNATE_MATCH_OPTSET", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_134_warmelt_normatch_ALTERNATE_MATCH_OPTSET_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_134_warmelt_normatch_ALTERNATE_MATCH_OPTSET */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_135_warmelt_normatch_MG_ALTDRAW_GRAPHVIZ (meltclosure_ptr_t
						   meltclosp_,
						   melt_ptr_t meltfirstargp_,
						   const melt_argdescr_cell_t
						   meltxargdescr_[],
						   union meltparam_un *
						   meltxargtab_,
						   const melt_argdescr_cell_t
						   meltxresdescr_[],
						   union meltparam_un *
						   meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_135_warmelt_normatch_MG_ALTDRAW_GRAPHVIZ_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_135_warmelt_normatch_MG_ALTDRAW_GRAPHVIZ_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 86
    melt_ptr_t mcfr_varptr[86];
#define MELTFRAM_NBVARNUM 37
    long mcfr_varnum[37];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_135_warmelt_normatch_MG_ALTDRAW_GRAPHVIZ is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_135_warmelt_normatch_MG_ALTDRAW_GRAPHVIZ_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 86; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_135_warmelt_normatch_MG_ALTDRAW_GRAPHVIZ nbval 86*/
  meltfram__.mcfr_nbvar = 86 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("MG_ALTDRAW_GRAPHVIZ", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-normatch.melt:6788:/ getarg");
 /*_.MDATA__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.DOTPREFIX__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.DOTPREFIX__V3*/ meltfptr[2])) !=
	      NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {

    MELT_LOCATION ("warmelt-normatch.melt:6789:/ quasiblock");


 /*_.MAPDATA__V4*/ meltfptr[3] =
      (meltgc_new_mapobjects
       ((meltobject_ptr_t) (( /*!DISCR_MAP_OBJECTS */ meltfrout->tabval[0])),
	(43)));;
    /*^compute */
 /*_.MAPSTEP__V5*/ meltfptr[4] =
      (meltgc_new_mapobjects
       ((meltobject_ptr_t) (( /*!DISCR_MAP_OBJECTS */ meltfrout->tabval[0])),
	(43)));;
    /*^compute */
 /*_.NODEBUF__V6*/ meltfptr[5] =
      (melt_ptr_t)
      meltgc_new_strbuf ((meltobject_ptr_t)
			 (( /*!DISCR_STRBUF */ meltfrout->tabval[1])),
			 (const char *) 0);;
    /*^compute */
 /*_.EDGEBUF__V7*/ meltfptr[6] =
      (melt_ptr_t)
      meltgc_new_strbuf ((meltobject_ptr_t)
			 (( /*!DISCR_STRBUF */ meltfrout->tabval[1])),
			 (const char *) 0);;
    /*^compute */
 /*_.DATACOUNTBOX__V8*/ meltfptr[7] =
      (meltgc_new_int
       ((meltobject_ptr_t) (( /*!DISCR_INTEGER */ meltfrout->tabval[2])),
	(0)));;
    /*^compute */
 /*_#CNT__L1*/ meltfnum[0] = 0;;
    /*^compute */
 /*_#MDATAHASH__L2*/ meltfnum[1] =
      (melt_obj_hash ((melt_ptr_t) ( /*_.MDATA__V2*/ meltfptr[1])));;
    /*^compute */
 /*_.PATHSBUF__V9*/ meltfptr[8] =
      (melt_ptr_t)
      meltgc_new_strbuf ((meltobject_ptr_t)
			 (( /*!DISCR_STRBUF */ meltfrout->tabval[1])),
			 (const char *) 0);;

    {
      MELT_LOCATION ("warmelt-normatch.melt:6801:/ locexp");
      /* mg_draw_match_graphviz_file UNIQCNT__2 */
      static long uniqcounter;
      uniqcounter++;
	     /*_#CNT__L1*/ meltfnum[0] = uniqcounter;
      ;
    }
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-normatch.melt:6806:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_STRBUF__L3*/ meltfnum[2] =
	(melt_magic_discr ((melt_ptr_t) ( /*_.PATHSBUF__V9*/ meltfptr[8])) ==
	 MELTOBMAG_STRBUF);;
      MELT_LOCATION ("warmelt-normatch.melt:6806:/ cond");
      /*cond */ if ( /*_#IS_STRBUF__L3*/ meltfnum[2])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V11*/ meltfptr[10] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-normatch.melt:6806:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check pathsbuf"),
				  ("warmelt-normatch.melt")
				  ? ("warmelt-normatch.melt") : __FILE__,
				  (6806) ? (6806) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V11*/ meltfptr[10] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V10*/ meltfptr[9] = /*_.IFELSE___V11*/ meltfptr[10];;
      /*epilog */

      MELT_LOCATION ("warmelt-normatch.melt:6806:/ clear");
	     /*clear *//*_#IS_STRBUF__L3*/ meltfnum[2] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V11*/ meltfptr[10] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V10*/ meltfptr[9] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

    {
      MELT_LOCATION ("warmelt-normatch.melt:6807:/ locexp");
      /*add2sbuf_string */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.PATHSBUF__V9*/ meltfptr[8]),
			   melt_string_str ((melt_ptr_t)
					    ( /*_.DOTPREFIX__V3*/
					     meltfptr[2])));
    }
    ;

    {
      MELT_LOCATION ("warmelt-normatch.melt:6808:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.PATHSBUF__V9*/ meltfptr[8]),
			   ("+"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-normatch.melt:6809:/ locexp");
      meltgc_add_strbuf_dec ((melt_ptr_t) ( /*_.PATHSBUF__V9*/ meltfptr[8]),
			     ( /*_#CNT__L1*/ meltfnum[0]));
    }
    ;

    {
      MELT_LOCATION ("warmelt-normatch.melt:6810:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.PATHSBUF__V9*/ meltfptr[8]),
			   (".dot"));
    }
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-normatch.melt:6811:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L4*/ meltfnum[2] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-normatch.melt:6811:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L4*/ meltfnum[2])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L5*/ meltfnum[4] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-normatch.melt:6811:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L5*/ meltfnum[4];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-normatch.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 6811;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring =
		"mg_draw_match_graphviz_file pathsbuf";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.PATHSBUF__V9*/ meltfptr[8];
	      /*_.MELT_DEBUG_FUN__V14*/ meltfptr[13] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[3])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V13*/ meltfptr[12] =
	      /*_.MELT_DEBUG_FUN__V14*/ meltfptr[13];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-normatch.melt:6811:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L5*/ meltfnum[4] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V14*/ meltfptr[13] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V13*/ meltfptr[12] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-normatch.melt:6811:/ quasiblock");


      /*_.PROGN___V15*/ meltfptr[13] = /*_.IF___V13*/ meltfptr[12];;
      /*^compute */
      /*_.IFCPP___V12*/ meltfptr[10] = /*_.PROGN___V15*/ meltfptr[13];;
      /*epilog */

      MELT_LOCATION ("warmelt-normatch.melt:6811:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L4*/ meltfnum[2] = 0;
      /*^clear */
	     /*clear *//*_.IF___V13*/ meltfptr[12] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V15*/ meltfptr[13] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V12*/ meltfptr[10] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    /*^compute */
 /*_.STRBUF2STRING__V16*/ meltfptr[12] =
      (meltgc_new_stringdup
       ((meltobject_ptr_t) (( /*!DISCR_STRING */ meltfrout->tabval[4])),
	melt_strbuf_str ((melt_ptr_t) ( /*_.PATHSBUF__V9*/ meltfptr[8]))));;
    MELT_LOCATION ("warmelt-normatch.melt:6798:/ quasiblock");


    /*_.DOTFILENAME__V17*/ meltfptr[13] =
      /*_.STRBUF2STRING__V16*/ meltfptr[12];;
    MELT_LOCATION ("warmelt-normatch.melt:6813:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*^rawallocobj */
    /*rawallocobj */
    {
      melt_ptr_t newobj = 0;
      melt_raw_object_create (newobj,
			      (melt_ptr_t) (( /*!CLASS_MATCH_GRAPHIC */
					     meltfrout->tabval[5])), (6),
			      "CLASS_MATCH_GRAPHIC");
  /*_.INST__V19*/ meltfptr[18] =
	newobj;
    };
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @MCHGX_FILENAME",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V19*/ meltfptr[18])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V19*/ meltfptr[18]), (1),
			  ( /*_.DOTFILENAME__V17*/ meltfptr[13]),
			  "MCHGX_FILENAME");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @MCHGX_NODOUT",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V19*/ meltfptr[18])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V19*/ meltfptr[18]), (2),
			  ( /*_.NODEBUF__V6*/ meltfptr[5]), "MCHGX_NODOUT");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @MCHGX_EDGOUT",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V19*/ meltfptr[18])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V19*/ meltfptr[18]), (3),
			  ( /*_.EDGEBUF__V7*/ meltfptr[6]), "MCHGX_EDGOUT");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @MCHGX_DATANAMEMAP",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V19*/ meltfptr[18])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V19*/ meltfptr[18]), (4),
			  ( /*_.MAPDATA__V4*/ meltfptr[3]),
			  "MCHGX_DATANAMEMAP");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @MCHGX_STEPNAMEMAP",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V19*/ meltfptr[18])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V19*/ meltfptr[18]), (5),
			  ( /*_.MAPSTEP__V5*/ meltfptr[4]),
			  "MCHGX_STEPNAMEMAP");
    ;
    /*^touchobj */

    melt_dbgtrace_written_object ( /*_.INST__V19*/ meltfptr[18],
				  "newly made instance");
    ;
    /*_.MG__V18*/ meltfptr[17] = /*_.INST__V19*/ meltfptr[18];;
    MELT_LOCATION ("warmelt-normatch.melt:6824:/ blockmultialloc");
    /*multiallocblock */
    {
      struct meltletrec_1_st
      {
	struct MELT_CLOSURE_STRUCT (3) rclo_0__SCANDATA;
	struct MELT_CLOSURE_STRUCT (4) rclo_1__SCANSTEP;
	struct MELT_CLOSURE_STRUCT (1) rclo_2__SCANFLAG;
	long meltletrec_1_endgap;
      } *meltletrec_1_ptr = 0;
      meltletrec_1_ptr =
	(struct meltletrec_1_st *)
	meltgc_allocate (sizeof (struct meltletrec_1_st), 0);
      /*^blockmultialloc.initfill */
      /*iniclos rclo_0__SCANDATA */
   /*_.SCANDATA__V21*/ meltfptr[20] =
	(melt_ptr_t) & meltletrec_1_ptr->rclo_0__SCANDATA;
      meltletrec_1_ptr->rclo_0__SCANDATA.discr =
	(meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (DISCR_CLOSURE))));
      meltletrec_1_ptr->rclo_0__SCANDATA.nbval = 3;
      meltletrec_1_ptr->rclo_0__SCANDATA.rout =
	(meltroutine_ptr_t) (( /*!konst_13 */ meltfrout->tabval[13]));

      /*iniclos rclo_1__SCANSTEP */
   /*_.SCANSTEP__V22*/ meltfptr[21] =
	(melt_ptr_t) & meltletrec_1_ptr->rclo_1__SCANSTEP;
      meltletrec_1_ptr->rclo_1__SCANSTEP.discr =
	(meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (DISCR_CLOSURE))));
      meltletrec_1_ptr->rclo_1__SCANSTEP.nbval = 4;
      meltletrec_1_ptr->rclo_1__SCANSTEP.rout =
	(meltroutine_ptr_t) (( /*!konst_25 */ meltfrout->tabval[25]));

      /*iniclos rclo_2__SCANFLAG */
   /*_.SCANFLAG__V23*/ meltfptr[22] =
	(melt_ptr_t) & meltletrec_1_ptr->rclo_2__SCANFLAG;
      meltletrec_1_ptr->rclo_2__SCANFLAG.discr =
	(meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (DISCR_CLOSURE))));
      meltletrec_1_ptr->rclo_2__SCANFLAG.nbval = 1;
      meltletrec_1_ptr->rclo_2__SCANFLAG.rout =
	(meltroutine_ptr_t) (( /*!konst_29 */ meltfrout->tabval[29]));



      MELT_LOCATION ("warmelt-normatch.melt:6826:/ putclosurout");
      /*putclosurout#3 */
      melt_assertmsg ("putclosrout#3 checkclo",
		      melt_magic_discr ((melt_ptr_t)
					( /*_.SCANDATA__V21*/ meltfptr[20]))
		      == MELTOBMAG_CLOSURE);
      melt_assertmsg ("putclosrout#3 checkrout",
		      melt_magic_discr ((melt_ptr_t)
					(( /*!konst_13 */ meltfrout->
					  tabval[13]))) == MELTOBMAG_ROUTINE);
      ((meltclosure_ptr_t) /*_.SCANDATA__V21*/ meltfptr[20])->rout =
	(meltroutine_ptr_t) (( /*!konst_13 */ meltfrout->tabval[13]));
      ;
      /*^putclosedv */
      /*putclosv */
      melt_assertmsg ("putclosv checkclo",
		      melt_magic_discr ((melt_ptr_t)
					( /*_.SCANDATA__V21*/ meltfptr[20]))
		      == MELTOBMAG_CLOSURE);
      melt_assertmsg ("putclosv checkoff", 0 >= 0
		      && 0 <
		      melt_closure_size ((melt_ptr_t)
					 ( /*_.SCANDATA__V21*/
					  meltfptr[20])));
      ((meltclosure_ptr_t) /*_.SCANDATA__V21*/ meltfptr[20])->tabval[0] =
	(melt_ptr_t) ( /*_.MAPDATA__V4*/ meltfptr[3]);
      ;
      /*^putclosedv */
      /*putclosv */
      melt_assertmsg ("putclosv checkclo",
		      melt_magic_discr ((melt_ptr_t)
					( /*_.SCANDATA__V21*/ meltfptr[20]))
		      == MELTOBMAG_CLOSURE);
      melt_assertmsg ("putclosv checkoff", 1 >= 0
		      && 1 <
		      melt_closure_size ((melt_ptr_t)
					 ( /*_.SCANDATA__V21*/
					  meltfptr[20])));
      ((meltclosure_ptr_t) /*_.SCANDATA__V21*/ meltfptr[20])->tabval[1] =
	(melt_ptr_t) ( /*_.DATACOUNTBOX__V8*/ meltfptr[7]);
      ;
      /*^putclosedv */
      /*putclosv */
      melt_assertmsg ("putclosv checkclo",
		      melt_magic_discr ((melt_ptr_t)
					( /*_.SCANDATA__V21*/ meltfptr[20]))
		      == MELTOBMAG_CLOSURE);
      melt_assertmsg ("putclosv checkoff", 2 >= 0
		      && 2 <
		      melt_closure_size ((melt_ptr_t)
					 ( /*_.SCANDATA__V21*/
					  meltfptr[20])));
      ((meltclosure_ptr_t) /*_.SCANDATA__V21*/ meltfptr[20])->tabval[2] =
	(melt_ptr_t) ( /*_.SCANSTEP__V22*/ meltfptr[21]);
      ;
      /*^touch */
      meltgc_touch ( /*_.SCANDATA__V21*/ meltfptr[20]);
      ;
      MELT_LOCATION ("warmelt-normatch.melt:6859:/ putclosurout");
      /*putclosurout#4 */
      melt_assertmsg ("putclosrout#4 checkclo",
		      melt_magic_discr ((melt_ptr_t)
					( /*_.SCANSTEP__V22*/ meltfptr[21]))
		      == MELTOBMAG_CLOSURE);
      melt_assertmsg ("putclosrout#4 checkrout",
		      melt_magic_discr ((melt_ptr_t)
					(( /*!konst_25 */ meltfrout->
					  tabval[25]))) == MELTOBMAG_ROUTINE);
      ((meltclosure_ptr_t) /*_.SCANSTEP__V22*/ meltfptr[21])->rout =
	(meltroutine_ptr_t) (( /*!konst_25 */ meltfrout->tabval[25]));
      ;
      /*^putclosedv */
      /*putclosv */
      melt_assertmsg ("putclosv checkclo",
		      melt_magic_discr ((melt_ptr_t)
					( /*_.SCANSTEP__V22*/ meltfptr[21]))
		      == MELTOBMAG_CLOSURE);
      melt_assertmsg ("putclosv checkoff", 0 >= 0
		      && 0 <
		      melt_closure_size ((melt_ptr_t)
					 ( /*_.SCANSTEP__V22*/
					  meltfptr[21])));
      ((meltclosure_ptr_t) /*_.SCANSTEP__V22*/ meltfptr[21])->tabval[0] =
	(melt_ptr_t) ( /*_.MAPSTEP__V5*/ meltfptr[4]);
      ;
      /*^putclosedv */
      /*putclosv */
      melt_assertmsg ("putclosv checkclo",
		      melt_magic_discr ((melt_ptr_t)
					( /*_.SCANSTEP__V22*/ meltfptr[21]))
		      == MELTOBMAG_CLOSURE);
      melt_assertmsg ("putclosv checkoff", 1 >= 0
		      && 1 <
		      melt_closure_size ((melt_ptr_t)
					 ( /*_.SCANSTEP__V22*/
					  meltfptr[21])));
      ((meltclosure_ptr_t) /*_.SCANSTEP__V22*/ meltfptr[21])->tabval[1] =
	(melt_ptr_t) ( /*_.SCANDATA__V21*/ meltfptr[20]);
      ;
      /*^putclosedv */
      /*putclosv */
      melt_assertmsg ("putclosv checkclo",
		      melt_magic_discr ((melt_ptr_t)
					( /*_.SCANSTEP__V22*/ meltfptr[21]))
		      == MELTOBMAG_CLOSURE);
      melt_assertmsg ("putclosv checkoff", 2 >= 0
		      && 2 <
		      melt_closure_size ((melt_ptr_t)
					 ( /*_.SCANSTEP__V22*/
					  meltfptr[21])));
      ((meltclosure_ptr_t) /*_.SCANSTEP__V22*/ meltfptr[21])->tabval[2] =
	(melt_ptr_t) ( /*_.SCANFLAG__V23*/ meltfptr[22]);
      ;
      /*^putclosedv */
      /*putclosv */
      melt_assertmsg ("putclosv checkclo",
		      melt_magic_discr ((melt_ptr_t)
					( /*_.SCANSTEP__V22*/ meltfptr[21]))
		      == MELTOBMAG_CLOSURE);
      melt_assertmsg ("putclosv checkoff", 3 >= 0
		      && 3 <
		      melt_closure_size ((melt_ptr_t)
					 ( /*_.SCANSTEP__V22*/
					  meltfptr[21])));
      ((meltclosure_ptr_t) /*_.SCANSTEP__V22*/ meltfptr[21])->tabval[3] =
	(melt_ptr_t) ( /*_.SCANSTEP__V22*/ meltfptr[21]);
      ;
      /*^touch */
      meltgc_touch ( /*_.SCANSTEP__V22*/ meltfptr[21]);
      ;
      MELT_LOCATION ("warmelt-normatch.melt:6895:/ putclosurout");
      /*putclosurout#5 */
      melt_assertmsg ("putclosrout#5 checkclo",
		      melt_magic_discr ((melt_ptr_t)
					( /*_.SCANFLAG__V23*/ meltfptr[22]))
		      == MELTOBMAG_CLOSURE);
      melt_assertmsg ("putclosrout#5 checkrout",
		      melt_magic_discr ((melt_ptr_t)
					(( /*!konst_29 */ meltfrout->
					  tabval[29]))) == MELTOBMAG_ROUTINE);
      ((meltclosure_ptr_t) /*_.SCANFLAG__V23*/ meltfptr[22])->rout =
	(meltroutine_ptr_t) (( /*!konst_29 */ meltfrout->tabval[29]));
      ;
      /*^putclosedv */
      /*putclosv */
      melt_assertmsg ("putclosv checkclo",
		      melt_magic_discr ((melt_ptr_t)
					( /*_.SCANFLAG__V23*/ meltfptr[22]))
		      == MELTOBMAG_CLOSURE);
      melt_assertmsg ("putclosv checkoff", 0 >= 0
		      && 0 <
		      melt_closure_size ((melt_ptr_t)
					 ( /*_.SCANFLAG__V23*/
					  meltfptr[22])));
      ((meltclosure_ptr_t) /*_.SCANFLAG__V23*/ meltfptr[22])->tabval[0] =
	(melt_ptr_t) ( /*_.SCANSTEP__V22*/ meltfptr[21]);
      ;
      /*^touch */
      meltgc_touch ( /*_.SCANFLAG__V23*/ meltfptr[22]);
      ;

#if MELT_HAVE_DEBUG
      MELT_LOCATION ("warmelt-normatch.melt:6907:/ cppif.then");
      /*^block */
      /*anyblock */
      {


	{
	  /*^locexp */
	  /*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	  melt_dbgcounter++;
#endif
	  ;
	}
	;
	/*^checksignal */
	MELT_CHECK_SIGNAL ();
	;
    /*_#MELT_NEED_DBG__L6*/ meltfnum[4] =
	  /*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	  ( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	  0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	  ;;
	MELT_LOCATION ("warmelt-normatch.melt:6907:/ cond");
	/*cond */ if ( /*_#MELT_NEED_DBG__L6*/ meltfnum[4])	/*then */
	  {
	    /*^cond.then */
	    /*^block */
	    /*anyblock */
	    {

      /*_#THE_MELTCALLCOUNT__L7*/ meltfnum[2] =
#ifdef meltcallcount
		meltcallcount	/* the_meltcallcount */
#else
		0L
#endif /* meltcallcount the_meltcallcount */
		;;
	      MELT_LOCATION ("warmelt-normatch.melt:6907:/ checksignal");
	      MELT_CHECK_SIGNAL ();
	      ;
	      /*^apply */
	      /*apply */
	      {
		union meltparam_un argtab[7];
		memset (&argtab, 0, sizeof (argtab));
		/*^apply.arg */
		argtab[0].meltbp_long =
		  /*_#THE_MELTCALLCOUNT__L7*/ meltfnum[2];
		/*^apply.arg */
		argtab[1].meltbp_cstring = "warmelt-normatch.melt";
		/*^apply.arg */
		argtab[2].meltbp_long = 6907;
		/*^apply.arg */
		argtab[3].meltbp_cstring =
		  "mg_altdraw_graphviz before scandata mdata=";
		/*^apply.arg */
		argtab[4].meltbp_aptr =
		  (melt_ptr_t *) & /*_.MDATA__V2*/ meltfptr[1];
		/*^apply.arg */
		argtab[5].meltbp_cstring = " scandata=";
		/*^apply.arg */
		argtab[6].meltbp_aptr =
		  (melt_ptr_t *) & /*_.SCANDATA__V21*/ meltfptr[20];
		/*_.MELT_DEBUG_FUN__V26*/ meltfptr[25] =
		  melt_apply ((meltclosure_ptr_t)
			      (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[3])),
			      (melt_ptr_t) (( /*nil */ NULL)),
			      (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			       MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			       MELTBPARSTR_PTR MELTBPARSTR_CSTRING
			       MELTBPARSTR_PTR ""), argtab, "",
			      (union meltparam_un *) 0);
	      }
	      ;
	      /*_.IF___V25*/ meltfptr[24] =
		/*_.MELT_DEBUG_FUN__V26*/ meltfptr[25];;
	      /*epilog */

	      MELT_LOCATION ("warmelt-normatch.melt:6907:/ clear");
		/*clear *//*_#THE_MELTCALLCOUNT__L7*/ meltfnum[2] = 0;
	      /*^clear */
		/*clear *//*_.MELT_DEBUG_FUN__V26*/ meltfptr[25] = 0;
	    }
	    ;
	  }
	else
	  {			/*^cond.else */

     /*_.IF___V25*/ meltfptr[24] = NULL;;
	  }
	;
	MELT_LOCATION ("warmelt-normatch.melt:6907:/ quasiblock");


	/*_.PROGN___V27*/ meltfptr[25] = /*_.IF___V25*/ meltfptr[24];;
	/*^compute */
	/*_.IFCPP___V24*/ meltfptr[23] = /*_.PROGN___V27*/ meltfptr[25];;
	/*epilog */

	MELT_LOCATION ("warmelt-normatch.melt:6907:/ clear");
	      /*clear *//*_#MELT_NEED_DBG__L6*/ meltfnum[4] = 0;
	/*^clear */
	      /*clear *//*_.IF___V25*/ meltfptr[24] = 0;
	/*^clear */
	      /*clear *//*_.PROGN___V27*/ meltfptr[25] = 0;
      }

#else /*MELT_HAVE_DEBUG */
      /*^cppif.else */
      /*_.IFCPP___V24*/ meltfptr[23] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
      ;

#if MELT_HAVE_DEBUG
      MELT_LOCATION ("warmelt-normatch.melt:6908:/ cppif.then");
      /*^block */
      /*anyblock */
      {

	/*^checksignal */
	MELT_CHECK_SIGNAL ();
	;
    /*_#IS_A__L8*/ meltfnum[2] =
	  melt_is_instance_of ((melt_ptr_t) ( /*_.MDATA__V2*/ meltfptr[1]),
			       (melt_ptr_t) (( /*!CLASS_MATCHED_DATA */
					      meltfrout->tabval[30])));;
	MELT_LOCATION ("warmelt-normatch.melt:6908:/ cond");
	/*cond */ if ( /*_#IS_A__L8*/ meltfnum[2])	/*then */
	  {
	    /*^cond.then */
	    /*_.IFELSE___V29*/ meltfptr[25] = ( /*nil */ NULL);;
	  }
	else
	  {
	    MELT_LOCATION ("warmelt-normatch.melt:6908:/ cond.else");

	    /*^block */
	    /*anyblock */
	    {




	      {
		/*^locexp */
		melt_assert_failed (("check mdata"),
				    ("warmelt-normatch.melt")
				    ? ("warmelt-normatch.melt") : __FILE__,
				    (6908) ? (6908) : __LINE__, __FUNCTION__);
		;
	      }
	      ;
		/*clear *//*_.IFELSE___V29*/ meltfptr[25] = 0;
	      /*epilog */
	    }
	    ;
	  }
	;
	/*^compute */
	/*_.IFCPP___V28*/ meltfptr[24] = /*_.IFELSE___V29*/ meltfptr[25];;
	/*epilog */

	MELT_LOCATION ("warmelt-normatch.melt:6908:/ clear");
	      /*clear *//*_#IS_A__L8*/ meltfnum[2] = 0;
	/*^clear */
	      /*clear *//*_.IFELSE___V29*/ meltfptr[25] = 0;
      }

#else /*MELT_HAVE_DEBUG */
      /*^cppif.else */
      /*_.IFCPP___V28*/ meltfptr[24] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
      ;
      MELT_LOCATION ("warmelt-normatch.melt:6909:/ checksignal");
      MELT_CHECK_SIGNAL ();
      ;
      /*^apply */
      /*apply */
      {
	/*_.SCANDATA__V30*/ meltfptr[25] =
	  melt_apply ((meltclosure_ptr_t) ( /*_.SCANDATA__V21*/ meltfptr[20]),
		      (melt_ptr_t) ( /*_.MDATA__V2*/ meltfptr[1]), (""),
		      (union meltparam_un *) 0, "", (union meltparam_un *) 0);
      }
      ;

#if MELT_HAVE_DEBUG
      MELT_LOCATION ("warmelt-normatch.melt:6910:/ cppif.then");
      /*^block */
      /*anyblock */
      {


	{
	  /*^locexp */
	  /*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	  melt_dbgcounter++;
#endif
	  ;
	}
	;
	/*^checksignal */
	MELT_CHECK_SIGNAL ();
	;
    /*_#MELT_NEED_DBG__L9*/ meltfnum[4] =
	  /*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	  ( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	  0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	  ;;
	MELT_LOCATION ("warmelt-normatch.melt:6910:/ cond");
	/*cond */ if ( /*_#MELT_NEED_DBG__L9*/ meltfnum[4])	/*then */
	  {
	    /*^cond.then */
	    /*^block */
	    /*anyblock */
	    {

      /*_#THE_MELTCALLCOUNT__L10*/ meltfnum[2] =
#ifdef meltcallcount
		meltcallcount	/* the_meltcallcount */
#else
		0L
#endif /* meltcallcount the_meltcallcount */
		;;
	      MELT_LOCATION ("warmelt-normatch.melt:6910:/ checksignal");
	      MELT_CHECK_SIGNAL ();
	      ;
	      /*^apply */
	      /*apply */
	      {
		union meltparam_un argtab[9];
		memset (&argtab, 0, sizeof (argtab));
		/*^apply.arg */
		argtab[0].meltbp_long =
		  /*_#THE_MELTCALLCOUNT__L10*/ meltfnum[2];
		/*^apply.arg */
		argtab[1].meltbp_cstring = "warmelt-normatch.melt";
		/*^apply.arg */
		argtab[2].meltbp_long = 6910;
		/*^apply.arg */
		argtab[3].meltbp_cstring =
		  "mg_altdraw_graphviz after scandata mdata=";
		/*^apply.arg */
		argtab[4].meltbp_aptr =
		  (melt_ptr_t *) & /*_.MDATA__V2*/ meltfptr[1];
		/*^apply.arg */
		argtab[5].meltbp_cstring = " mapdata=";
		/*^apply.arg */
		argtab[6].meltbp_aptr =
		  (melt_ptr_t *) & /*_.MAPDATA__V4*/ meltfptr[3];
		/*^apply.arg */
		argtab[7].meltbp_cstring = " mapstep=";
		/*^apply.arg */
		argtab[8].meltbp_aptr =
		  (melt_ptr_t *) & /*_.MAPSTEP__V5*/ meltfptr[4];
		/*_.MELT_DEBUG_FUN__V33*/ meltfptr[32] =
		  melt_apply ((meltclosure_ptr_t)
			      (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[3])),
			      (melt_ptr_t) (( /*nil */ NULL)),
			      (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			       MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			       MELTBPARSTR_PTR MELTBPARSTR_CSTRING
			       MELTBPARSTR_PTR MELTBPARSTR_CSTRING
			       MELTBPARSTR_PTR ""), argtab, "",
			      (union meltparam_un *) 0);
	      }
	      ;
	      /*_.IF___V32*/ meltfptr[31] =
		/*_.MELT_DEBUG_FUN__V33*/ meltfptr[32];;
	      /*epilog */

	      MELT_LOCATION ("warmelt-normatch.melt:6910:/ clear");
		/*clear *//*_#THE_MELTCALLCOUNT__L10*/ meltfnum[2] = 0;
	      /*^clear */
		/*clear *//*_.MELT_DEBUG_FUN__V33*/ meltfptr[32] = 0;
	    }
	    ;
	  }
	else
	  {			/*^cond.else */

     /*_.IF___V32*/ meltfptr[31] = NULL;;
	  }
	;
	MELT_LOCATION ("warmelt-normatch.melt:6910:/ quasiblock");


	/*_.PROGN___V34*/ meltfptr[32] = /*_.IF___V32*/ meltfptr[31];;
	/*^compute */
	/*_.IFCPP___V31*/ meltfptr[30] = /*_.PROGN___V34*/ meltfptr[32];;
	/*epilog */

	MELT_LOCATION ("warmelt-normatch.melt:6910:/ clear");
	      /*clear *//*_#MELT_NEED_DBG__L9*/ meltfnum[4] = 0;
	/*^clear */
	      /*clear *//*_.IF___V32*/ meltfptr[31] = 0;
	/*^clear */
	      /*clear *//*_.PROGN___V34*/ meltfptr[32] = 0;
      }

#else /*MELT_HAVE_DEBUG */
      /*^cppif.else */
      /*_.IFCPP___V31*/ meltfptr[30] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
      ;
      /*^compute */
      /*_.LETREC___V20*/ meltfptr[19] = /*_.IFCPP___V31*/ meltfptr[30];;
      /*epilog */

      MELT_LOCATION ("warmelt-normatch.melt:6824:/ clear");
	    /*clear *//*_.SCANDATA__V21*/ meltfptr[20] = 0;
      /*^clear */
	    /*clear *//*_.SCANSTEP__V22*/ meltfptr[21] = 0;
      /*^clear */
	    /*clear *//*_.SCANFLAG__V23*/ meltfptr[22] = 0;
      /*^clear */
	    /*clear *//*_.SCANDATA__V21*/ meltfptr[20] = 0;
      /*^clear */
	    /*clear *//*_.SCANSTEP__V22*/ meltfptr[21] = 0;
      /*^clear */
	    /*clear *//*_.SCANFLAG__V23*/ meltfptr[22] = 0;
      /*^clear */
	    /*clear *//*_.IFCPP___V24*/ meltfptr[23] = 0;
      /*^clear */
	    /*clear *//*_.IFCPP___V28*/ meltfptr[24] = 0;
      /*^clear */
	    /*clear *//*_.SCANDATA__V30*/ meltfptr[25] = 0;
      /*^clear */
	    /*clear *//*_.IFCPP___V31*/ meltfptr[30] = 0;
    }				/*end multiallocblock */
    ;
    MELT_LOCATION ("warmelt-normatch.melt:6913:/ quasiblock");


 /*_#DATACOUNT__L11*/ meltfnum[2] = 0;;
    /*^compute */
 /*_#STEPCOUNT__L12*/ meltfnum[4] = 0;;
    /*^compute */
 /*_#MAPOBJECT_COUNT__L13*/ meltfnum[12] =
      (melt_count_mapobjects
       ((meltmapobjects_ptr_t) ( /*_.MAPDATA__V4*/ meltfptr[3])));;
    /*^compute */
 /*_.TUPDATA__V35*/ meltfptr[31] =
      (meltgc_new_multiple
       ((meltobject_ptr_t) (( /*!DISCR_MULTIPLE */ meltfrout->tabval[31])),
	( /*_#MAPOBJECT_COUNT__L13*/ meltfnum[12])));;
    /*^compute */
 /*_#MAPOBJECT_COUNT__L14*/ meltfnum[13] =
      (melt_count_mapobjects
       ((meltmapobjects_ptr_t) ( /*_.MAPSTEP__V5*/ meltfptr[4])));;
    /*^compute */
 /*_.TUPSTEP__V36*/ meltfptr[32] =
      (meltgc_new_multiple
       ((meltobject_ptr_t) (( /*!DISCR_MULTIPLE */ meltfrout->tabval[31])),
	( /*_#MAPOBJECT_COUNT__L14*/ meltfnum[13])));;
    /*citerblock FOREACH_IN_MAPOBJECT */
    {
      /* foreach_in_mapobject meltcit1__EACHOBMAP : */ int
	meltcit1__EACHOBMAP_ix = 0, meltcit1__EACHOBMAP_siz = 0;
      for (meltcit1__EACHOBMAP_ix = 0;
	   /* we retrieve in meltcit1__EACHOBMAP_siz the size at each iteration since it could change. */
	   meltcit1__EACHOBMAP_ix >= 0
	   && (meltcit1__EACHOBMAP_siz =
	       melt_size_mapobjects ((meltmapobjects_ptr_t) /*_.MAPDATA__V4*/
				     meltfptr[3])) > 0
	   && meltcit1__EACHOBMAP_ix < meltcit1__EACHOBMAP_siz;
	   meltcit1__EACHOBMAP_ix++)
	{
    /*_.CURDATA__V37*/ meltfptr[20] = NULL;
    /*_.DATANAME__V38*/ meltfptr[21] = NULL;
	  /*_.CURDATA__V37*/ meltfptr[20] =
	    (melt_ptr_t) (((meltmapobjects_ptr_t) /*_.MAPDATA__V4*/
			   meltfptr[3])->entab[meltcit1__EACHOBMAP_ix].e_at);
	  if ( /*_.CURDATA__V37*/ meltfptr[20] == HTAB_DELETED_ENTRY)
	    {							 /*_.CURDATA__V37*/
	      meltfptr[20] = NULL;
	      continue;
	    };
	  if (! /*_.CURDATA__V37*/ meltfptr[20])
	    continue;
	  /*_.DATANAME__V38*/ meltfptr[21] =
	    ((meltmapobjects_ptr_t) /*_.MAPDATA__V4*/ meltfptr[3])->
	    entab[meltcit1__EACHOBMAP_ix].e_va;
	  if (! /*_.DATANAME__V38*/ meltfptr[21])
	    continue;




	  {
	    MELT_LOCATION ("warmelt-normatch.melt:6922:/ locexp");
	    meltgc_multiple_put_nth ((melt_ptr_t)
				     ( /*_.TUPDATA__V35*/ meltfptr[31]),
				     ( /*_#DATACOUNT__L11*/ meltfnum[2]),
				     (melt_ptr_t) ( /*_.CURDATA__V37*/
						   meltfptr[20]));
	  }
	  ;
  /*_#I__L15*/ meltfnum[14] =
	    ((1) + ( /*_#DATACOUNT__L11*/ meltfnum[2]));;
	  MELT_LOCATION ("warmelt-normatch.melt:6923:/ compute");
	  /*_#DATACOUNT__L11*/ meltfnum[2] = /*_#SETQ___L16*/ meltfnum[15] =
	    /*_#I__L15*/ meltfnum[14];;
	  /* foreach_in_mapobject end meltcit1__EACHOBMAP */
    /*_.CURDATA__V37*/ meltfptr[20] = NULL;
    /*_.DATANAME__V38*/ meltfptr[21] = NULL;
	}


      /*citerepilog */

      MELT_LOCATION ("warmelt-normatch.melt:6919:/ clear");
	    /*clear *//*_.CURDATA__V37*/ meltfptr[20] = 0;
      /*^clear */
	    /*clear *//*_.DATANAME__V38*/ meltfptr[21] = 0;
      /*^clear */
	    /*clear *//*_#I__L15*/ meltfnum[14] = 0;
      /*^clear */
	    /*clear *//*_#SETQ___L16*/ meltfnum[15] = 0;
    }				/*endciterblock FOREACH_IN_MAPOBJECT */
    ;
    /*citerblock FOREACH_IN_MAPOBJECT */
    {
      /* foreach_in_mapobject meltcit2__EACHOBMAP : */ int
	meltcit2__EACHOBMAP_ix = 0, meltcit2__EACHOBMAP_siz = 0;
      for (meltcit2__EACHOBMAP_ix = 0;
	   /* we retrieve in meltcit2__EACHOBMAP_siz the size at each iteration since it could change. */
	   meltcit2__EACHOBMAP_ix >= 0
	   && (meltcit2__EACHOBMAP_siz =
	       melt_size_mapobjects ((meltmapobjects_ptr_t) /*_.MAPSTEP__V5*/
				     meltfptr[4])) > 0
	   && meltcit2__EACHOBMAP_ix < meltcit2__EACHOBMAP_siz;
	   meltcit2__EACHOBMAP_ix++)
	{
    /*_.CURSTEP__V39*/ meltfptr[22] = NULL;
    /*_.STEPNAME__V40*/ meltfptr[23] = NULL;
	  /*_.CURSTEP__V39*/ meltfptr[22] =
	    (melt_ptr_t) (((meltmapobjects_ptr_t) /*_.MAPSTEP__V5*/
			   meltfptr[4])->entab[meltcit2__EACHOBMAP_ix].e_at);
	  if ( /*_.CURSTEP__V39*/ meltfptr[22] == HTAB_DELETED_ENTRY)
	    {							 /*_.CURSTEP__V39*/
	      meltfptr[22] = NULL;
	      continue;
	    };
	  if (! /*_.CURSTEP__V39*/ meltfptr[22])
	    continue;
	  /*_.STEPNAME__V40*/ meltfptr[23] =
	    ((meltmapobjects_ptr_t) /*_.MAPSTEP__V5*/ meltfptr[4])->
	    entab[meltcit2__EACHOBMAP_ix].e_va;
	  if (! /*_.STEPNAME__V40*/ meltfptr[23])
	    continue;




	  {
	    MELT_LOCATION ("warmelt-normatch.melt:6928:/ locexp");
	    meltgc_multiple_put_nth ((melt_ptr_t)
				     ( /*_.TUPSTEP__V36*/ meltfptr[32]),
				     ( /*_#STEPCOUNT__L12*/ meltfnum[4]),
				     (melt_ptr_t) ( /*_.CURSTEP__V39*/
						   meltfptr[22]));
	  }
	  ;
  /*_#I__L17*/ meltfnum[16] =
	    ((1) + ( /*_#STEPCOUNT__L12*/ meltfnum[4]));;
	  MELT_LOCATION ("warmelt-normatch.melt:6929:/ compute");
	  /*_#STEPCOUNT__L12*/ meltfnum[4] = /*_#SETQ___L18*/ meltfnum[17] =
	    /*_#I__L17*/ meltfnum[16];;
	  /* foreach_in_mapobject end meltcit2__EACHOBMAP */
    /*_.CURSTEP__V39*/ meltfptr[22] = NULL;
    /*_.STEPNAME__V40*/ meltfptr[23] = NULL;
	}


      /*citerepilog */

      MELT_LOCATION ("warmelt-normatch.melt:6925:/ clear");
	    /*clear *//*_.CURSTEP__V39*/ meltfptr[22] = 0;
      /*^clear */
	    /*clear *//*_.STEPNAME__V40*/ meltfptr[23] = 0;
      /*^clear */
	    /*clear *//*_#I__L17*/ meltfnum[16] = 0;
      /*^clear */
	    /*clear *//*_#SETQ___L18*/ meltfnum[17] = 0;
    }				/*endciterblock FOREACH_IN_MAPOBJECT */
    ;
    MELT_LOCATION ("warmelt-normatch.melt:6931:/ quasiblock");


    MELT_LOCATION ("warmelt-normatch.melt:6935:/ quasiblock");


    /*^newclosure */
		 /*newclosure *//*_.LAMBDA___V42*/ meltfptr[25] =
      (melt_ptr_t)
      meltgc_new_closure ((meltobject_ptr_t)
			  (((melt_ptr_t) (MELT_PREDEF (DISCR_CLOSURE)))),
			  (meltroutine_ptr_t) (( /*!konst_32 */ meltfrout->
						tabval[32])), (1));
    ;
    /*^putclosedv */
    /*putclosv */
    melt_assertmsg ("putclosv checkclo",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.LAMBDA___V42*/ meltfptr[25])) ==
		    MELTOBMAG_CLOSURE);
    melt_assertmsg ("putclosv checkoff", 0 >= 0
		    && 0 <
		    melt_closure_size ((melt_ptr_t)
				       ( /*_.LAMBDA___V42*/ meltfptr[25])));
    ((meltclosure_ptr_t) /*_.LAMBDA___V42*/ meltfptr[25])->tabval[0] =
      (melt_ptr_t) ( /*_.MAPDATA__V4*/ meltfptr[3]);
    ;
    /*_.LAMBDA___V41*/ meltfptr[24] = /*_.LAMBDA___V42*/ meltfptr[25];;
    /*^compute */
 /*_.SORTUPDATA__V43*/ meltfptr[30] =
      meltgc_sort_multiple ((melt_ptr_t) ( /*_.TUPDATA__V35*/ meltfptr[31]),
			    (melt_ptr_t) ( /*_.LAMBDA___V41*/ meltfptr[24]),
			    (melt_ptr_t) (( /*!DISCR_MULTIPLE */ meltfrout->
					   tabval[31])));;
    MELT_LOCATION ("warmelt-normatch.melt:6954:/ quasiblock");


    /*^newclosure */
		 /*newclosure *//*_.LAMBDA___V45*/ meltfptr[44] =
      (melt_ptr_t)
      meltgc_new_closure ((meltobject_ptr_t)
			  (((melt_ptr_t) (MELT_PREDEF (DISCR_CLOSURE)))),
			  (meltroutine_ptr_t) (( /*!konst_33 */ meltfrout->
						tabval[33])), (1));
    ;
    /*^putclosedv */
    /*putclosv */
    melt_assertmsg ("putclosv checkclo",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.LAMBDA___V45*/ meltfptr[44])) ==
		    MELTOBMAG_CLOSURE);
    melt_assertmsg ("putclosv checkoff", 0 >= 0
		    && 0 <
		    melt_closure_size ((melt_ptr_t)
				       ( /*_.LAMBDA___V45*/ meltfptr[44])));
    ((meltclosure_ptr_t) /*_.LAMBDA___V45*/ meltfptr[44])->tabval[0] =
      (melt_ptr_t) ( /*_.MAPSTEP__V5*/ meltfptr[4]);
    ;
    /*_.LAMBDA___V44*/ meltfptr[43] = /*_.LAMBDA___V45*/ meltfptr[44];;
    /*^compute */
 /*_.SORTUPSTEP__V46*/ meltfptr[45] =
      meltgc_sort_multiple ((melt_ptr_t) ( /*_.TUPSTEP__V36*/ meltfptr[32]),
			    (melt_ptr_t) ( /*_.LAMBDA___V44*/ meltfptr[43]),
			    (melt_ptr_t) (( /*!DISCR_MULTIPLE */ meltfrout->
					   tabval[31])));;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-normatch.melt:6972:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L19*/ meltfnum[18] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-normatch.melt:6972:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L19*/ meltfnum[18])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L20*/ meltfnum[19] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-normatch.melt:6972:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long =
		/*_#THE_MELTCALLCOUNT__L20*/ meltfnum[19];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-normatch.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 6972;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "mg_altdraw_graphviz sortupdata";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.SORTUPDATA__V43*/ meltfptr[30];
	      /*_.MELT_DEBUG_FUN__V49*/ meltfptr[48] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[3])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V48*/ meltfptr[47] =
	      /*_.MELT_DEBUG_FUN__V49*/ meltfptr[48];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-normatch.melt:6972:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L20*/ meltfnum[19] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V49*/ meltfptr[48] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V48*/ meltfptr[47] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-normatch.melt:6972:/ quasiblock");


      /*_.PROGN___V50*/ meltfptr[48] = /*_.IF___V48*/ meltfptr[47];;
      /*^compute */
      /*_.IFCPP___V47*/ meltfptr[46] = /*_.PROGN___V50*/ meltfptr[48];;
      /*epilog */

      MELT_LOCATION ("warmelt-normatch.melt:6972:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L19*/ meltfnum[18] = 0;
      /*^clear */
	     /*clear *//*_.IF___V48*/ meltfptr[47] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V50*/ meltfptr[48] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V47*/ meltfptr[46] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    /*citerblock FOREACH_IN_MULTIPLE */
    {
      /* start foreach_in_multiple meltcit3__EACHTUP */
      long meltcit3__EACHTUP_ln =
	melt_multiple_length ((melt_ptr_t) /*_.SORTUPDATA__V43*/
			      meltfptr[30]);
      for ( /*_#DIX__L21*/ meltfnum[19] = 0;
	   ( /*_#DIX__L21*/ meltfnum[19] >= 0)
	   && ( /*_#DIX__L21*/ meltfnum[19] < meltcit3__EACHTUP_ln);
	/*_#DIX__L21*/ meltfnum[19]++)
	{
	  /*_.CURDATA__V51*/ meltfptr[47] =
	    melt_multiple_nth ((melt_ptr_t)
			       ( /*_.SORTUPDATA__V43*/ meltfptr[30]),
			       /*_#DIX__L21*/ meltfnum[19]);




#if MELT_HAVE_DEBUG
	  MELT_LOCATION ("warmelt-normatch.melt:6976:/ cppif.then");
	  /*^block */
	  /*anyblock */
	  {


	    {
	      /*^locexp */
	      /*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	      melt_dbgcounter++;
#endif
	      ;
	    }
	    ;
	    /*^checksignal */
	    MELT_CHECK_SIGNAL ();
	    ;
    /*_#MELT_NEED_DBG__L22*/ meltfnum[18] =
	      /*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	      ( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	      0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	      ;;
	    MELT_LOCATION ("warmelt-normatch.melt:6976:/ cond");
	    /*cond */ if ( /*_#MELT_NEED_DBG__L22*/ meltfnum[18])	/*then */
	      {
		/*^cond.then */
		/*^block */
		/*anyblock */
		{

      /*_#THE_MELTCALLCOUNT__L23*/ meltfnum[22] =
#ifdef meltcallcount
		    meltcallcount	/* the_meltcallcount */
#else
		    0L
#endif /* meltcallcount the_meltcallcount */
		    ;;
		  MELT_LOCATION ("warmelt-normatch.melt:6976:/ checksignal");
		  MELT_CHECK_SIGNAL ();
		  ;
		  /*^apply */
		  /*apply */
		  {
		    union meltparam_un argtab[5];
		    memset (&argtab, 0, sizeof (argtab));
		    /*^apply.arg */
		    argtab[0].meltbp_long =
		      /*_#THE_MELTCALLCOUNT__L23*/ meltfnum[22];
		    /*^apply.arg */
		    argtab[1].meltbp_cstring = "warmelt-normatch.melt";
		    /*^apply.arg */
		    argtab[2].meltbp_long = 6976;
		    /*^apply.arg */
		    argtab[3].meltbp_cstring = "mg_altdraw_graphviz curdata";
		    /*^apply.arg */
		    argtab[4].meltbp_aptr =
		      (melt_ptr_t *) & /*_.CURDATA__V51*/ meltfptr[47];
		    /*_.MELT_DEBUG_FUN__V54*/ meltfptr[53] =
		      melt_apply ((meltclosure_ptr_t)
				  (( /*!MELT_DEBUG_FUN */ meltfrout->
				    tabval[3])),
				  (melt_ptr_t) (( /*nil */ NULL)),
				  (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_PTR ""), argtab, "",
				  (union meltparam_un *) 0);
		  }
		  ;
		  /*_.IF___V53*/ meltfptr[52] =
		    /*_.MELT_DEBUG_FUN__V54*/ meltfptr[53];;
		  /*epilog */

		  MELT_LOCATION ("warmelt-normatch.melt:6976:/ clear");
		/*clear *//*_#THE_MELTCALLCOUNT__L23*/ meltfnum[22] = 0;
		  /*^clear */
		/*clear *//*_.MELT_DEBUG_FUN__V54*/ meltfptr[53] = 0;
		}
		;
	      }
	    else
	      {			/*^cond.else */

     /*_.IF___V53*/ meltfptr[52] = NULL;;
	      }
	    ;
	    MELT_LOCATION ("warmelt-normatch.melt:6976:/ quasiblock");


	    /*_.PROGN___V55*/ meltfptr[53] = /*_.IF___V53*/ meltfptr[52];;
	    /*^compute */
	    /*_.IFCPP___V52*/ meltfptr[48] = /*_.PROGN___V55*/ meltfptr[53];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-normatch.melt:6976:/ clear");
	      /*clear *//*_#MELT_NEED_DBG__L22*/ meltfnum[18] = 0;
	    /*^clear */
	      /*clear *//*_.IF___V53*/ meltfptr[52] = 0;
	    /*^clear */
	      /*clear *//*_.PROGN___V55*/ meltfptr[53] = 0;
	  }

#else /*MELT_HAVE_DEBUG */
	  /*^cppif.else */
	  /*_.IFCPP___V52*/ meltfptr[48] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
	  ;

#if MELT_HAVE_DEBUG
	  MELT_LOCATION ("warmelt-normatch.melt:6977:/ cppif.then");
	  /*^block */
	  /*anyblock */
	  {

	    /*^checksignal */
	    MELT_CHECK_SIGNAL ();
	    ;
    /*_#IS_A__L24*/ meltfnum[22] =
	      melt_is_instance_of ((melt_ptr_t)
				   ( /*_.CURDATA__V51*/ meltfptr[47]),
				   (melt_ptr_t) (( /*!CLASS_MATCHED_DATA */
						  meltfrout->tabval[30])));;
	    MELT_LOCATION ("warmelt-normatch.melt:6977:/ cond");
	    /*cond */ if ( /*_#IS_A__L24*/ meltfnum[22])	/*then */
	      {
		/*^cond.then */
		/*_.IFELSE___V57*/ meltfptr[53] = ( /*nil */ NULL);;
	      }
	    else
	      {
		MELT_LOCATION ("warmelt-normatch.melt:6977:/ cond.else");

		/*^block */
		/*anyblock */
		{




		  {
		    /*^locexp */
		    melt_assert_failed (("mg_altdraw_graphviz check curdata"),
					("warmelt-normatch.melt")
					? ("warmelt-normatch.melt") :
					__FILE__, (6977) ? (6977) : __LINE__,
					__FUNCTION__);
		    ;
		  }
		  ;
		/*clear *//*_.IFELSE___V57*/ meltfptr[53] = 0;
		  /*epilog */
		}
		;
	      }
	    ;
	    /*^compute */
	    /*_.IFCPP___V56*/ meltfptr[52] = /*_.IFELSE___V57*/ meltfptr[53];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-normatch.melt:6977:/ clear");
	      /*clear *//*_#IS_A__L24*/ meltfnum[22] = 0;
	    /*^clear */
	      /*clear *//*_.IFELSE___V57*/ meltfptr[53] = 0;
	  }

#else /*MELT_HAVE_DEBUG */
	  /*^cppif.else */
	  /*_.IFCPP___V56*/ meltfptr[52] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
	  ;
	  MELT_LOCATION ("warmelt-normatch.melt:6978:/ quasiblock");


  /*_.CURDATANAME__V58*/ meltfptr[53] =
	    /*mapobject_get */
	    melt_get_mapobjects ((meltmapobjects_ptr_t)
				 ( /*_.MAPDATA__V4*/ meltfptr[3]),
				 (meltobject_ptr_t) ( /*_.CURDATA__V51*/
						     meltfptr[47]));;
	  MELT_LOCATION ("warmelt-normatch.melt:6980:/ getslot");
	  {
	    melt_ptr_t slot = NULL, obj = NULL;
	    obj = (melt_ptr_t) ( /*_.CURDATA__V51*/ meltfptr[47]) /*=obj*/ ;
	    melt_object_get_field (slot, obj, 2, "MDATA_CTYPE");
   /*_.DATACTYPE__V59*/ meltfptr[58] = slot;
	  };
	  ;
	  MELT_LOCATION ("warmelt-normatch.melt:6981:/ getslot");
	  {
	    melt_ptr_t slot = NULL, obj = NULL;
	    obj = (melt_ptr_t) ( /*_.CURDATA__V51*/ meltfptr[47]) /*=obj*/ ;
	    melt_object_get_field (slot, obj, 1, "MDATA_SYMB");
   /*_.DATASYMB__V60*/ meltfptr[59] = slot;
	  };
	  ;
	  MELT_LOCATION ("warmelt-normatch.melt:6982:/ getslot");
	  {
	    melt_ptr_t slot = NULL, obj = NULL;
	    obj = (melt_ptr_t) ( /*_.CURDATA__V51*/ meltfptr[47]) /*=obj*/ ;
	    melt_object_get_field (slot, obj, 4, "MDATA_STEPS");
   /*_.DATASTEPS__V61*/ meltfptr[60] = slot;
	  };
	  ;

	  {
	    MELT_LOCATION ("warmelt-normatch.melt:6984:/ locexp");
	    meltgc_out_add_indent ((melt_ptr_t)
				   ( /*_.NODEBUF__V6*/ meltfptr[5]), (0), 0);;
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-normatch.melt:6985:/ locexp");
	    meltgc_add_out ((melt_ptr_t) ( /*_.NODEBUF__V6*/ meltfptr[5]),
			    melt_string_str ((melt_ptr_t)
					     ( /*_.CURDATANAME__V58*/
					      meltfptr[53])));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-normatch.melt:6986:/ locexp");
	    meltgc_add_out ((melt_ptr_t) ( /*_.NODEBUF__V6*/ meltfptr[5]),
			    (" [ margin=0, "));
	  }
	  ;
	  MELT_LOCATION ("warmelt-normatch.melt:6987:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
  /*_#__L25*/ meltfnum[18] =
	    (( /*_.CURDATA__V51*/ meltfptr[47]) ==
	     ( /*_.MDATA__V2*/ meltfptr[1]));;
	  MELT_LOCATION ("warmelt-normatch.melt:6987:/ cond");
	  /*cond */ if ( /*_#__L25*/ meltfnum[18])	/*then */
	    {
	      /*^cond.then */
	      /*^block */
	      /*anyblock */
	      {


		{
		  MELT_LOCATION ("warmelt-normatch.melt:6988:/ locexp");
		  meltgc_add_out ((melt_ptr_t)
				  ( /*_.NODEBUF__V6*/ meltfptr[5]),
				  (" style=\"bold,dashed,filled,rounded\", fillcolor=\"lightyellow\", "));
		}
		;
		/*epilog */
	      }
	      ;
	    }
	  else
	    {
	      MELT_LOCATION ("warmelt-normatch.melt:6987:/ cond.else");

	      /*^block */
	      /*anyblock */
	      {


		{
		  MELT_LOCATION ("warmelt-normatch.melt:6989:/ locexp");
		  meltgc_add_out ((melt_ptr_t)
				  ( /*_.NODEBUF__V6*/ meltfptr[5]),
				  (" style=\"dashed,rounded\", "));
		}
		;
		/*epilog */
	      }
	      ;
	    }
	  ;

	  {
	    MELT_LOCATION ("warmelt-normatch.melt:6990:/ locexp");
	    meltgc_add_out ((melt_ptr_t) ( /*_.NODEBUF__V6*/ meltfptr[5]),
			    ("label=<<table border=\"0\" cellborder=\"0\""));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-normatch.melt:6991:/ locexp");
	    meltgc_add_out ((melt_ptr_t) ( /*_.NODEBUF__V6*/ meltfptr[5]),
			    (" cellspacing=\"1\" cellpadding=\"1\">"));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-normatch.melt:6992:/ locexp");
	    meltgc_out_add_indent ((melt_ptr_t)
				   ( /*_.NODEBUF__V6*/ meltfptr[5]), (0), 0);;
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-normatch.melt:6993:/ locexp");
	    meltgc_add_out ((melt_ptr_t) ( /*_.NODEBUF__V6*/ meltfptr[5]),
			    ("<tr><td><font color=\"green\" face=\"Times-Roman Bold\" point-size\
=\"7\">"));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-normatch.melt:6994:/ locexp");
	    meltgc_add_out ((melt_ptr_t) ( /*_.NODEBUF__V6*/ meltfptr[5]),
			    melt_string_str ((melt_ptr_t)
					     ( /*_.CURDATANAME__V58*/
					      meltfptr[53])));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-normatch.melt:6995:/ locexp");
	    meltgc_add_out ((melt_ptr_t) ( /*_.NODEBUF__V6*/ meltfptr[5]),
			    ("</font></td> <td><font face=\"Courier\" point-size=\"6.5\">#"));
	  }
	  ;
  /*_#OBJ_HASH__L26*/ meltfnum[22] =
	    (melt_obj_hash
	     ((melt_ptr_t) ( /*_.CURDATA__V51*/ meltfptr[47])));;

	  {
	    MELT_LOCATION ("warmelt-normatch.melt:6996:/ locexp");
	    meltgc_add_out_hex ((melt_ptr_t) ( /*_.NODEBUF__V6*/ meltfptr[5]),
				( /*_#OBJ_HASH__L26*/ meltfnum[22]));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-normatch.melt:6997:/ locexp");
	    meltgc_add_out ((melt_ptr_t) ( /*_.NODEBUF__V6*/ meltfptr[5]),
			    ("</font></td> </tr>"));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-normatch.melt:6998:/ locexp");
	    meltgc_out_add_indent ((melt_ptr_t)
				   ( /*_.NODEBUF__V6*/ meltfptr[5]), (0), 0);;
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-normatch.melt:6999:/ locexp");
	    meltgc_add_out ((melt_ptr_t) ( /*_.NODEBUF__V6*/ meltfptr[5]),
			    ("<tr><td><font color=\"darkgreen\" face=\"Times-Roman Italic\" point\
-size=\"7\">"));
	  }
	  ;
	  MELT_LOCATION ("warmelt-normatch.melt:7000:/ cond");
	  /*cond */ if (
			 /*ifisa */
			 melt_is_instance_of ((melt_ptr_t)
					      ( /*_.DATASYMB__V60*/
					       meltfptr[59]),
					      (melt_ptr_t) (( /*!CLASS_NAMED */ meltfrout->tabval[34])))
	    )			/*then */
	    {
	      /*^cond.then */
	      /*^getslot */
	      {
		melt_ptr_t slot = NULL, obj = NULL;
		obj =
		  (melt_ptr_t) ( /*_.DATASYMB__V60*/ meltfptr[59]) /*=obj*/ ;
		melt_object_get_field (slot, obj, 1, "NAMED_NAME");
    /*_.NAMED_NAME__V62*/ meltfptr[61] = slot;
	      };
	      ;
	    }
	  else
	    {			/*^cond.else */

   /*_.NAMED_NAME__V62*/ meltfptr[61] = NULL;;
	    }
	  ;

	  {
	    MELT_LOCATION ("warmelt-normatch.melt:7000:/ locexp");
	    meltgc_add_out ((melt_ptr_t) ( /*_.NODEBUF__V6*/ meltfptr[5]),
			    melt_string_str ((melt_ptr_t)
					     ( /*_.NAMED_NAME__V62*/
					      meltfptr[61])));
	  }
	  ;
	  MELT_LOCATION ("warmelt-normatch.melt:7001:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
  /*_#IS_A__L27*/ meltfnum[26] =
	    melt_is_instance_of ((melt_ptr_t)
				 ( /*_.DATASYMB__V60*/ meltfptr[59]),
				 (melt_ptr_t) (( /*!CLASS_CLONED_SYMBOL */
						meltfrout->tabval[35])));;
	  MELT_LOCATION ("warmelt-normatch.melt:7001:/ cond");
	  /*cond */ if ( /*_#IS_A__L27*/ meltfnum[26])	/*then */
	    {
	      /*^cond.then */
	      /*^block */
	      /*anyblock */
	      {

		MELT_LOCATION ("warmelt-normatch.melt:7002:/ quasiblock");


		/*^getslot */
		{
		  melt_ptr_t slot = NULL, obj = NULL;
		  obj =
		    (melt_ptr_t) ( /*_.DATASYMB__V60*/ meltfptr[59]) /*=obj*/
		    ;
		  melt_object_get_field (slot, obj, 3, "CSYM_URANK");
     /*_.CSYM_URANK__V65*/ meltfptr[64] = slot;
		};
		;
    /*_#CLONUM__L28*/ meltfnum[27] =
		  (melt_get_int
		   ((melt_ptr_t) ( /*_.CSYM_URANK__V65*/ meltfptr[64])));;
		MELT_LOCATION ("warmelt-normatch.melt:7004:/ checksignal");
		MELT_CHECK_SIGNAL ();
		;
		/*^apply */
		/*apply */
		{
		  union meltparam_un argtab[3];
		  memset (&argtab, 0, sizeof (argtab));
		  /*^apply.arg */
		  argtab[0].meltbp_cstring = "<font point-size=\"6\">";
		  /*^apply.arg */
		  argtab[1].meltbp_long = /*_#CLONUM__L28*/ meltfnum[27];
		  /*^apply.arg */
		  argtab[2].meltbp_cstring = "</font>";
		  /*_.ADD2OUT__V66*/ meltfptr[65] =
		    melt_apply ((meltclosure_ptr_t)
				(( /*!ADD2OUT */ meltfrout->tabval[36])),
				(melt_ptr_t) ( /*_.NODEBUF__V6*/ meltfptr[5]),
				(MELTBPARSTR_CSTRING MELTBPARSTR_LONG
				 MELTBPARSTR_CSTRING ""), argtab, "",
				(union meltparam_un *) 0);
		}
		;
		/*_.LET___V64*/ meltfptr[63] =
		  /*_.ADD2OUT__V66*/ meltfptr[65];;

		MELT_LOCATION ("warmelt-normatch.melt:7002:/ clear");
	      /*clear *//*_.CSYM_URANK__V65*/ meltfptr[64] = 0;
		/*^clear */
	      /*clear *//*_#CLONUM__L28*/ meltfnum[27] = 0;
		/*^clear */
	      /*clear *//*_.ADD2OUT__V66*/ meltfptr[65] = 0;
		/*_.IF___V63*/ meltfptr[62] = /*_.LET___V64*/ meltfptr[63];;
		/*epilog */

		MELT_LOCATION ("warmelt-normatch.melt:7001:/ clear");
	      /*clear *//*_.LET___V64*/ meltfptr[63] = 0;
	      }
	      ;
	    }
	  else
	    {			/*^cond.else */

   /*_.IF___V63*/ meltfptr[62] = NULL;;
	    }
	  ;

	  {
	    MELT_LOCATION ("warmelt-normatch.melt:7006:/ locexp");
	    meltgc_add_out ((melt_ptr_t) ( /*_.NODEBUF__V6*/ meltfptr[5]),
			    ("</font></td> <td><font color=\"brown\" face=\"Helvetica Oblique\" point\
-size=\"6\">"));
	  }
	  ;
	  MELT_LOCATION ("warmelt-normatch.melt:7007:/ cond");
	  /*cond */ if (
			 /*ifisa */
			 melt_is_instance_of ((melt_ptr_t)
					      ( /*_.DATACTYPE__V59*/
					       meltfptr[58]),
					      (melt_ptr_t) (( /*!CLASS_CTYPE */ meltfrout->tabval[37])))
	    )			/*then */
	    {
	      /*^cond.then */
	      /*^getslot */
	      {
		melt_ptr_t slot = NULL, obj = NULL;
		obj =
		  (melt_ptr_t) ( /*_.DATACTYPE__V59*/ meltfptr[58]) /*=obj*/ ;
		melt_object_get_field (slot, obj, 2, "CTYPE_KEYWORD");
    /*_.CTYPE_KEYWORD__V67*/ meltfptr[64] = slot;
	      };
	      ;
	    }
	  else
	    {			/*^cond.else */

   /*_.CTYPE_KEYWORD__V67*/ meltfptr[64] = NULL;;
	    }
	  ;
	  MELT_LOCATION ("warmelt-normatch.melt:7007:/ cond");
	  /*cond */ if (
			 /*ifisa */
			 melt_is_instance_of ((melt_ptr_t)
					      ( /*_.CTYPE_KEYWORD__V67*/
					       meltfptr[64]),
					      (melt_ptr_t) (( /*!CLASS_NAMED */ meltfrout->tabval[34])))
	    )			/*then */
	    {
	      /*^cond.then */
	      /*^getslot */
	      {
		melt_ptr_t slot = NULL, obj = NULL;
		obj =
		  (melt_ptr_t) ( /*_.CTYPE_KEYWORD__V67*/ meltfptr[64])
		  /*=obj*/ ;
		melt_object_get_field (slot, obj, 1, "NAMED_NAME");
    /*_.NAMED_NAME__V68*/ meltfptr[65] = slot;
	      };
	      ;
	    }
	  else
	    {			/*^cond.else */

   /*_.NAMED_NAME__V68*/ meltfptr[65] = NULL;;
	    }
	  ;

	  {
	    MELT_LOCATION ("warmelt-normatch.melt:7007:/ locexp");
	    meltgc_add_out ((melt_ptr_t) ( /*_.NODEBUF__V6*/ meltfptr[5]),
			    melt_string_str ((melt_ptr_t)
					     ( /*_.NAMED_NAME__V68*/
					      meltfptr[65])));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-normatch.melt:7009:/ locexp");
	    meltgc_add_out ((melt_ptr_t) ( /*_.NODEBUF__V6*/ meltfptr[5]),
			    ("</font></td> </tr>"));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-normatch.melt:7010:/ locexp");
	    meltgc_add_out ((melt_ptr_t) ( /*_.NODEBUF__V6*/ meltfptr[5]),
			    ("</table>> ];"));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-normatch.melt:7011:/ locexp");
	    meltgc_out_add_indent ((melt_ptr_t)
				   ( /*_.NODEBUF__V6*/ meltfptr[5]), (0), 0);;
	  }
	  ;
	  /*citerblock FOREACH_IN_LIST */
	  {
	    /* start foreach_in_list meltcit4__EACHLIST */
	    for ( /*_.PAIRSTEP__V69*/ meltfptr[63] =
		 melt_list_first ((melt_ptr_t) /*_.DATASTEPS__V61*/
				  meltfptr[60]);
		 melt_magic_discr ((melt_ptr_t) /*_.PAIRSTEP__V69*/
				   meltfptr[63]) == MELTOBMAG_PAIR;
		 /*_.PAIRSTEP__V69*/ meltfptr[63] =
		 melt_pair_tail ((melt_ptr_t) /*_.PAIRSTEP__V69*/
				 meltfptr[63]))
	      {
		/*_.CURSTEP__V70*/ meltfptr[69] =
		  melt_pair_head ((melt_ptr_t) /*_.PAIRSTEP__V69*/
				  meltfptr[63]);



		{
		  MELT_LOCATION ("warmelt-normatch.melt:7016:/ locexp");
		  meltgc_out_add_indent ((melt_ptr_t)
					 ( /*_.EDGEBUF__V7*/ meltfptr[6]),
					 (0), 0);;
		}
		;

		{
		  MELT_LOCATION ("warmelt-normatch.melt:7017:/ locexp");
		  meltgc_add_out ((melt_ptr_t)
				  ( /*_.EDGEBUF__V7*/ meltfptr[6]),
				  melt_string_str ((melt_ptr_t)
						   ( /*_.CURDATANAME__V58*/
						    meltfptr[53])));
		}
		;

		{
		  MELT_LOCATION ("warmelt-normatch.melt:7018:/ locexp");
		  meltgc_add_out ((melt_ptr_t)
				  ( /*_.EDGEBUF__V7*/ meltfptr[6]),
				  (" /*data*/ -> /*step*/ "));
		}
		;
   /*_.MAPOBJECT_GET__V71*/ meltfptr[70] =
		  /*mapobject_get */
		  melt_get_mapobjects ((meltmapobjects_ptr_t)
				       ( /*_.MAPSTEP__V5*/ meltfptr[4]),
				       (meltobject_ptr_t) ( /*_.CURSTEP__V70*/
							   meltfptr[69]));;

		{
		  MELT_LOCATION ("warmelt-normatch.melt:7019:/ locexp");
		  meltgc_add_out ((melt_ptr_t)
				  ( /*_.EDGEBUF__V7*/ meltfptr[6]),
				  melt_string_str ((melt_ptr_t)
						   ( /*_.MAPOBJECT_GET__V71*/
						    meltfptr[70])));
		}
		;

		{
		  MELT_LOCATION ("warmelt-normatch.melt:7020:/ locexp");
		  meltgc_add_out ((melt_ptr_t)
				  ( /*_.EDGEBUF__V7*/ meltfptr[6]),
				  (" [ arrowhead=open, color=blueviolet, style=dotted ];"));
		}
		;
	      }			/* end foreach_in_list meltcit4__EACHLIST */
     /*_.PAIRSTEP__V69*/ meltfptr[63] = NULL;
     /*_.CURSTEP__V70*/ meltfptr[69] = NULL;


	    /*citerepilog */

	    MELT_LOCATION ("warmelt-normatch.melt:7013:/ clear");
	     /*clear *//*_.PAIRSTEP__V69*/ meltfptr[63] = 0;
	    /*^clear */
	     /*clear *//*_.CURSTEP__V70*/ meltfptr[69] = 0;
	    /*^clear */
	     /*clear *//*_.MAPOBJECT_GET__V71*/ meltfptr[70] = 0;
	  }			/*endciterblock FOREACH_IN_LIST */
	  ;

	  MELT_LOCATION ("warmelt-normatch.melt:6978:/ clear");
	    /*clear *//*_.CURDATANAME__V58*/ meltfptr[53] = 0;
	  /*^clear */
	    /*clear *//*_.DATACTYPE__V59*/ meltfptr[58] = 0;
	  /*^clear */
	    /*clear *//*_.DATASYMB__V60*/ meltfptr[59] = 0;
	  /*^clear */
	    /*clear *//*_.DATASTEPS__V61*/ meltfptr[60] = 0;
	  /*^clear */
	    /*clear *//*_#__L25*/ meltfnum[18] = 0;
	  /*^clear */
	    /*clear *//*_#OBJ_HASH__L26*/ meltfnum[22] = 0;
	  /*^clear */
	    /*clear *//*_.NAMED_NAME__V62*/ meltfptr[61] = 0;
	  /*^clear */
	    /*clear *//*_#IS_A__L27*/ meltfnum[26] = 0;
	  /*^clear */
	    /*clear *//*_.IF___V63*/ meltfptr[62] = 0;
	  /*^clear */
	    /*clear *//*_.CTYPE_KEYWORD__V67*/ meltfptr[64] = 0;
	  /*^clear */
	    /*clear *//*_.NAMED_NAME__V68*/ meltfptr[65] = 0;
	  if ( /*_#DIX__L21*/ meltfnum[19] < 0)
	    break;
	}			/* end  foreach_in_multiple meltcit3__EACHTUP */

      /*citerepilog */

      MELT_LOCATION ("warmelt-normatch.melt:6973:/ clear");
	    /*clear *//*_.CURDATA__V51*/ meltfptr[47] = 0;
      /*^clear */
	    /*clear *//*_#DIX__L21*/ meltfnum[19] = 0;
      /*^clear */
	    /*clear *//*_.IFCPP___V52*/ meltfptr[48] = 0;
      /*^clear */
	    /*clear *//*_.IFCPP___V56*/ meltfptr[52] = 0;
    }				/*endciterblock FOREACH_IN_MULTIPLE */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-normatch.melt:7025:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L29*/ meltfnum[27] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-normatch.melt:7025:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L29*/ meltfnum[27])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L30*/ meltfnum[18] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-normatch.melt:7025:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long =
		/*_#THE_MELTCALLCOUNT__L30*/ meltfnum[18];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-normatch.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 7025;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "mg_altdraw_graphviz sortupstep";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.SORTUPSTEP__V46*/ meltfptr[45];
	      /*_.MELT_DEBUG_FUN__V74*/ meltfptr[59] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[3])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V73*/ meltfptr[58] =
	      /*_.MELT_DEBUG_FUN__V74*/ meltfptr[59];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-normatch.melt:7025:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L30*/ meltfnum[18] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V74*/ meltfptr[59] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V73*/ meltfptr[58] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-normatch.melt:7025:/ quasiblock");


      /*_.PROGN___V75*/ meltfptr[60] = /*_.IF___V73*/ meltfptr[58];;
      /*^compute */
      /*_.IFCPP___V72*/ meltfptr[53] = /*_.PROGN___V75*/ meltfptr[60];;
      /*epilog */

      MELT_LOCATION ("warmelt-normatch.melt:7025:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L29*/ meltfnum[27] = 0;
      /*^clear */
	     /*clear *//*_.IF___V73*/ meltfptr[58] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V75*/ meltfptr[60] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V72*/ meltfptr[53] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    /*citerblock FOREACH_IN_MULTIPLE */
    {
      /* start foreach_in_multiple meltcit5__EACHTUP */
      long meltcit5__EACHTUP_ln =
	melt_multiple_length ((melt_ptr_t) /*_.SORTUPSTEP__V46*/
			      meltfptr[45]);
      for ( /*_#DIX__L31*/ meltfnum[22] = 0;
	   ( /*_#DIX__L31*/ meltfnum[22] >= 0)
	   && ( /*_#DIX__L31*/ meltfnum[22] < meltcit5__EACHTUP_ln);
	/*_#DIX__L31*/ meltfnum[22]++)
	{
	  /*_.CURSTEP__V76*/ meltfptr[61] =
	    melt_multiple_nth ((melt_ptr_t)
			       ( /*_.SORTUPSTEP__V46*/ meltfptr[45]),
			       /*_#DIX__L31*/ meltfnum[22]);




#if MELT_HAVE_DEBUG
	  MELT_LOCATION ("warmelt-normatch.melt:7029:/ cppif.then");
	  /*^block */
	  /*anyblock */
	  {


	    {
	      /*^locexp */
	      /*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	      melt_dbgcounter++;
#endif
	      ;
	    }
	    ;
	    /*^checksignal */
	    MELT_CHECK_SIGNAL ();
	    ;
    /*_#MELT_NEED_DBG__L32*/ meltfnum[26] =
	      /*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	      ( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	      0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	      ;;
	    MELT_LOCATION ("warmelt-normatch.melt:7029:/ cond");
	    /*cond */ if ( /*_#MELT_NEED_DBG__L32*/ meltfnum[26])	/*then */
	      {
		/*^cond.then */
		/*^block */
		/*anyblock */
		{

      /*_#THE_MELTCALLCOUNT__L33*/ meltfnum[18] =
#ifdef meltcallcount
		    meltcallcount	/* the_meltcallcount */
#else
		    0L
#endif /* meltcallcount the_meltcallcount */
		    ;;
		  MELT_LOCATION ("warmelt-normatch.melt:7029:/ checksignal");
		  MELT_CHECK_SIGNAL ();
		  ;
		  /*^apply */
		  /*apply */
		  {
		    union meltparam_un argtab[5];
		    memset (&argtab, 0, sizeof (argtab));
		    /*^apply.arg */
		    argtab[0].meltbp_long =
		      /*_#THE_MELTCALLCOUNT__L33*/ meltfnum[18];
		    /*^apply.arg */
		    argtab[1].meltbp_cstring = "warmelt-normatch.melt";
		    /*^apply.arg */
		    argtab[2].meltbp_long = 7029;
		    /*^apply.arg */
		    argtab[3].meltbp_cstring = "mg_altdraw_graphviz curstep";
		    /*^apply.arg */
		    argtab[4].meltbp_aptr =
		      (melt_ptr_t *) & /*_.CURSTEP__V76*/ meltfptr[61];
		    /*_.MELT_DEBUG_FUN__V79*/ meltfptr[65] =
		      melt_apply ((meltclosure_ptr_t)
				  (( /*!MELT_DEBUG_FUN */ meltfrout->
				    tabval[3])),
				  (melt_ptr_t) (( /*nil */ NULL)),
				  (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_PTR ""), argtab, "",
				  (union meltparam_un *) 0);
		  }
		  ;
		  /*_.IF___V78*/ meltfptr[64] =
		    /*_.MELT_DEBUG_FUN__V79*/ meltfptr[65];;
		  /*epilog */

		  MELT_LOCATION ("warmelt-normatch.melt:7029:/ clear");
		/*clear *//*_#THE_MELTCALLCOUNT__L33*/ meltfnum[18] = 0;
		  /*^clear */
		/*clear *//*_.MELT_DEBUG_FUN__V79*/ meltfptr[65] = 0;
		}
		;
	      }
	    else
	      {			/*^cond.else */

     /*_.IF___V78*/ meltfptr[64] = NULL;;
	      }
	    ;
	    MELT_LOCATION ("warmelt-normatch.melt:7029:/ quasiblock");


	    /*_.PROGN___V80*/ meltfptr[59] = /*_.IF___V78*/ meltfptr[64];;
	    /*^compute */
	    /*_.IFCPP___V77*/ meltfptr[62] = /*_.PROGN___V80*/ meltfptr[59];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-normatch.melt:7029:/ clear");
	      /*clear *//*_#MELT_NEED_DBG__L32*/ meltfnum[26] = 0;
	    /*^clear */
	      /*clear *//*_.IF___V78*/ meltfptr[64] = 0;
	    /*^clear */
	      /*clear *//*_.PROGN___V80*/ meltfptr[59] = 0;
	  }

#else /*MELT_HAVE_DEBUG */
	  /*^cppif.else */
	  /*_.IFCPP___V77*/ meltfptr[62] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
	  ;

#if MELT_HAVE_DEBUG
	  MELT_LOCATION ("warmelt-normatch.melt:7030:/ cppif.then");
	  /*^block */
	  /*anyblock */
	  {

	    /*^checksignal */
	    MELT_CHECK_SIGNAL ();
	    ;
    /*_#IS_A__L34*/ meltfnum[27] =
	      melt_is_instance_of ((melt_ptr_t)
				   ( /*_.CURSTEP__V76*/ meltfptr[61]),
				   (melt_ptr_t) (( /*!CLASS_MATCH_STEP */
						  meltfrout->tabval[38])));;
	    MELT_LOCATION ("warmelt-normatch.melt:7030:/ cond");
	    /*cond */ if ( /*_#IS_A__L34*/ meltfnum[27])	/*then */
	      {
		/*^cond.then */
		/*_.IFELSE___V82*/ meltfptr[60] = ( /*nil */ NULL);;
	      }
	    else
	      {
		MELT_LOCATION ("warmelt-normatch.melt:7030:/ cond.else");

		/*^block */
		/*anyblock */
		{




		  {
		    /*^locexp */
		    melt_assert_failed (("mg_altdraw_graphviz check curstep"),
					("warmelt-normatch.melt")
					? ("warmelt-normatch.melt") :
					__FILE__, (7030) ? (7030) : __LINE__,
					__FUNCTION__);
		    ;
		  }
		  ;
		/*clear *//*_.IFELSE___V82*/ meltfptr[60] = 0;
		  /*epilog */
		}
		;
	      }
	    ;
	    /*^compute */
	    /*_.IFCPP___V81*/ meltfptr[58] = /*_.IFELSE___V82*/ meltfptr[60];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-normatch.melt:7030:/ clear");
	      /*clear *//*_#IS_A__L34*/ meltfnum[27] = 0;
	    /*^clear */
	      /*clear *//*_.IFELSE___V82*/ meltfptr[60] = 0;
	  }

#else /*MELT_HAVE_DEBUG */
	  /*^cppif.else */
	  /*_.IFCPP___V81*/ meltfptr[58] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
	  ;
	  MELT_LOCATION ("warmelt-normatch.melt:7031:/ quasiblock");


  /*_.CURSTEPNAME__V83*/ meltfptr[65] =
	    /*mapobject_get */
	    melt_get_mapobjects ((meltmapobjects_ptr_t)
				 ( /*_.MAPSTEP__V5*/ meltfptr[4]),
				 (meltobject_ptr_t) ( /*_.CURSTEP__V76*/
						     meltfptr[61]));;

#if MELT_HAVE_DEBUG
	  MELT_LOCATION ("warmelt-normatch.melt:7034:/ cppif.then");
	  /*^block */
	  /*anyblock */
	  {

	    /*^checksignal */
	    MELT_CHECK_SIGNAL ();
	    ;
    /*_#IS_STRING__L35*/ meltfnum[18] =
	      (melt_magic_discr
	       ((melt_ptr_t) ( /*_.CURSTEPNAME__V83*/ meltfptr[65])) ==
	       MELTOBMAG_STRING);;
	    MELT_LOCATION ("warmelt-normatch.melt:7034:/ cond");
	    /*cond */ if ( /*_#IS_STRING__L35*/ meltfnum[18])	/*then */
	      {
		/*^cond.then */
		/*_.IFELSE___V85*/ meltfptr[59] = ( /*nil */ NULL);;
	      }
	    else
	      {
		MELT_LOCATION ("warmelt-normatch.melt:7034:/ cond.else");

		/*^block */
		/*anyblock */
		{




		  {
		    /*^locexp */
		    melt_assert_failed (("mg_altdraw_graphviz check curstepname"), ("warmelt-normatch.melt") ? ("warmelt-normatch.melt") : __FILE__, (7034) ? (7034) : __LINE__, __FUNCTION__);
		    ;
		  }
		  ;
		/*clear *//*_.IFELSE___V85*/ meltfptr[59] = 0;
		  /*epilog */
		}
		;
	      }
	    ;
	    /*^compute */
	    /*_.IFCPP___V84*/ meltfptr[64] = /*_.IFELSE___V85*/ meltfptr[59];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-normatch.melt:7034:/ clear");
	      /*clear *//*_#IS_STRING__L35*/ meltfnum[18] = 0;
	    /*^clear */
	      /*clear *//*_.IFELSE___V85*/ meltfptr[59] = 0;
	  }

#else /*MELT_HAVE_DEBUG */
	  /*^cppif.else */
	  /*_.IFCPP___V84*/ meltfptr[64] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
	  ;

	  {
	    MELT_LOCATION ("warmelt-normatch.melt:7035:/ locexp");
	    meltgc_out_add_indent ((melt_ptr_t)
				   ( /*_.NODEBUF__V6*/ meltfptr[5]), (0), 0);;
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-normatch.melt:7036:/ locexp");
	    meltgc_add_out ((melt_ptr_t) ( /*_.NODEBUF__V6*/ meltfptr[5]),
			    melt_string_str ((melt_ptr_t)
					     ( /*_.CURSTEPNAME__V83*/
					      meltfptr[65])));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-normatch.melt:7037:/ locexp");
	    meltgc_add_out ((melt_ptr_t) ( /*_.NODEBUF__V6*/ meltfptr[5]),
			    (" [ margin=0, "));
	  }
	  ;
	  MELT_LOCATION ("warmelt-normatch.melt:7038:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
  /*_#IS_A__L36*/ meltfnum[26] =
	    melt_is_instance_of ((melt_ptr_t)
				 ( /*_.CURSTEP__V76*/ meltfptr[61]),
				 (melt_ptr_t) (( /*!CLASS_MATCH_STEP_SUCCESS_WHEN_FLAG */ meltfrout->tabval[39])));;
	  MELT_LOCATION ("warmelt-normatch.melt:7038:/ cond");
	  /*cond */ if ( /*_#IS_A__L36*/ meltfnum[26])	/*then */
	    {
	      /*^cond.then */
	      /*^block */
	      /*anyblock */
	      {


		{
		  MELT_LOCATION ("warmelt-normatch.melt:7039:/ locexp");
		  meltgc_add_out ((melt_ptr_t)
				  ( /*_.NODEBUF__V6*/ meltfptr[5]),
				  (" style=\"filled\", bgcolor=\"palegreen\", "));
		}
		;
		/*epilog */
	      }
	      ;
	    }			/*noelse */
	  ;

	  {
	    MELT_LOCATION ("warmelt-normatch.melt:7041:/ locexp");
	    meltgc_add_out ((melt_ptr_t) ( /*_.NODEBUF__V6*/ meltfptr[5]),
			    ("label=<<table border=\"1\" cellborder=\"1\""));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-normatch.melt:7042:/ locexp");
	    meltgc_add_out ((melt_ptr_t) ( /*_.NODEBUF__V6*/ meltfptr[5]),
			    (" cellspacing=\"1\" cellpadding=\"1\">"));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-normatch.melt:7043:/ locexp");
	    meltgc_out_add_indent ((melt_ptr_t)
				   ( /*_.NODEBUF__V6*/ meltfptr[5]), (0), 0);;
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-normatch.melt:7044:/ locexp");
	    meltgc_add_out ((melt_ptr_t) ( /*_.NODEBUF__V6*/ meltfptr[5]),
			    ("<tr><td><font color=\"blue\" face=\"Times-Roman Bold\" point-size=\"7\">"));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-normatch.melt:7045:/ locexp");
	    meltgc_add_out ((melt_ptr_t) ( /*_.NODEBUF__V6*/ meltfptr[5]),
			    melt_string_str ((melt_ptr_t)
					     ( /*_.CURSTEPNAME__V83*/
					      meltfptr[65])));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-normatch.melt:7046:/ locexp");
	    meltgc_add_out ((melt_ptr_t) ( /*_.NODEBUF__V6*/ meltfptr[5]),
			    ("</font></td> <td><font face=\"Courier\" point-size=\"6\">#"));
	  }
	  ;
  /*_#OBJ_HASH__L37*/ meltfnum[27] =
	    (melt_obj_hash
	     ((melt_ptr_t) ( /*_.CURSTEP__V76*/ meltfptr[61])));;

	  {
	    MELT_LOCATION ("warmelt-normatch.melt:7047:/ locexp");
	    meltgc_add_out_hex ((melt_ptr_t) ( /*_.NODEBUF__V6*/ meltfptr[5]),
				( /*_#OBJ_HASH__L37*/ meltfnum[27]));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-normatch.melt:7048:/ locexp");
	    meltgc_add_out ((melt_ptr_t) ( /*_.NODEBUF__V6*/ meltfptr[5]),
			    ("</font></td> </tr>"));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-normatch.melt:7049:/ locexp");
	    meltgc_out_add_indent ((melt_ptr_t)
				   ( /*_.NODEBUF__V6*/ meltfptr[5]), (0), 0);;
	  }
	  ;
	  MELT_LOCATION ("warmelt-normatch.melt:7050:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^msend */
	  /*msend */
	  {
	    union meltparam_un argtab[1];
	    memset (&argtab, 0, sizeof (argtab));
	    /*^ojbmsend.arg */
	    argtab[0].meltbp_aptr =
	      (melt_ptr_t *) & /*_.MG__V18*/ meltfptr[17];
	    /*_.MATCHGRAPHIC_ALTSTEP__V86*/ meltfptr[60] =
	      meltgc_send ((melt_ptr_t) ( /*_.CURSTEP__V76*/ meltfptr[61]),
			   (melt_ptr_t) (( /*!MATCHGRAPHIC_ALTSTEP */
					  meltfrout->tabval[40])),
			   (MELTBPARSTR_PTR ""), argtab, "",
			   (union meltparam_un *) 0);
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-normatch.melt:7051:/ locexp");
	    meltgc_add_out ((melt_ptr_t) ( /*_.NODEBUF__V6*/ meltfptr[5]),
			    ("</table>> ];"));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-normatch.melt:7052:/ locexp");
	    meltgc_out_add_indent ((melt_ptr_t)
				   ( /*_.NODEBUF__V6*/ meltfptr[5]), (0), 0);;
	  }
	  ;

	  MELT_LOCATION ("warmelt-normatch.melt:7031:/ clear");
	    /*clear *//*_.CURSTEPNAME__V83*/ meltfptr[65] = 0;
	  /*^clear */
	    /*clear *//*_.IFCPP___V84*/ meltfptr[64] = 0;
	  /*^clear */
	    /*clear *//*_#IS_A__L36*/ meltfnum[26] = 0;
	  /*^clear */
	    /*clear *//*_#OBJ_HASH__L37*/ meltfnum[27] = 0;
	  /*^clear */
	    /*clear *//*_.MATCHGRAPHIC_ALTSTEP__V86*/ meltfptr[60] = 0;
	  if ( /*_#DIX__L31*/ meltfnum[22] < 0)
	    break;
	}			/* end  foreach_in_multiple meltcit5__EACHTUP */

      /*citerepilog */

      MELT_LOCATION ("warmelt-normatch.melt:7026:/ clear");
	    /*clear *//*_.CURSTEP__V76*/ meltfptr[61] = 0;
      /*^clear */
	    /*clear *//*_#DIX__L31*/ meltfnum[22] = 0;
      /*^clear */
	    /*clear *//*_.IFCPP___V77*/ meltfptr[62] = 0;
      /*^clear */
	    /*clear *//*_.IFCPP___V81*/ meltfptr[58] = 0;
    }				/*endciterblock FOREACH_IN_MULTIPLE */
    ;
    MELT_LOCATION ("warmelt-normatch.melt:7056:/ quasiblock");



    {
      MELT_LOCATION ("warmelt-normatch.melt:7060:/ locexp");
      /* OUTPUTMGFILE__1 start */
      time_t nowt = 0;
      char nowbuf[60];
      FILE *dotfil =
	fopen (melt_string_str
	       ((melt_ptr_t) /*_.DOTFILENAME__V17*/ meltfptr[13]), "w");
      if (!dotfil)
	melt_fatal_error ("failed to open matchdot file %s - %m",
			  melt_string_str ((melt_ptr_t) /*_.DOTFILENAME__V17*/
					   meltfptr[13]));
      fprintf (dotfil, "// melt matchdot file %s\n",
	       melt_string_str ((melt_ptr_t) /*_.DOTFILENAME__V17*/
				meltfptr[13]));
      time (&nowt);
      memset (nowbuf, 0, sizeof (nowbuf));
      strftime (nowbuf, sizeof (nowbuf) - 1,
		"%Y %b %d %Hh%M", localtime (&nowt));
      fprintf (dotfil, "// generated %s\n", nowbuf);
      fprintf (dotfil, "digraph meltaltmatch_%lx {", /*_#MDATAHASH__L2*/
	       meltfnum[1]);
      fprintf (dotfil,
	       " graph [ label=<<font color='firebrick' point-size='9' face='Helvetica Bold'>Melt Alt Match %s ** %s</font>>,",
	       lbasename (melt_string_str
			  ((melt_ptr_t) /*_.DOTFILENAME__V17*/ meltfptr[13])),
	       nowbuf);
      fprintf (dotfil, " pad=\"0.5\", margin=\"0.3\" ];\n");
      fprintf (dotfil, " node [ shape=\"box\", fontsize=\"9\" ];\n");
      melt_putstrbuf (dotfil, (melt_ptr_t) /*_.NODEBUF__V6*/ meltfptr[5]);
      fprintf (dotfil, "\n /// edges\n");
      melt_putstrbuf (dotfil, (melt_ptr_t) /*_.EDGEBUF__V7*/ meltfptr[6]);
      fprintf (dotfil, "\n} // eof %s\n",
	       melt_string_str ((melt_ptr_t) /*_.DOTFILENAME__V17*/
				meltfptr[13]));
      fclose (dotfil);
      /* OUTPUTMGFILE__1 end */
      ;
    }
    ;


    MELT_LOCATION ("warmelt-normatch.melt:6931:/ clear");
	   /*clear *//*_.LAMBDA___V41*/ meltfptr[24] = 0;
    /*^clear */
	   /*clear *//*_.SORTUPDATA__V43*/ meltfptr[30] = 0;
    /*^clear */
	   /*clear *//*_.LAMBDA___V44*/ meltfptr[43] = 0;
    /*^clear */
	   /*clear *//*_.SORTUPSTEP__V46*/ meltfptr[45] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V47*/ meltfptr[46] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V72*/ meltfptr[53] = 0;

    MELT_LOCATION ("warmelt-normatch.melt:6913:/ clear");
	   /*clear *//*_#DATACOUNT__L11*/ meltfnum[2] = 0;
    /*^clear */
	   /*clear *//*_#STEPCOUNT__L12*/ meltfnum[4] = 0;
    /*^clear */
	   /*clear *//*_#MAPOBJECT_COUNT__L13*/ meltfnum[12] = 0;
    /*^clear */
	   /*clear *//*_.TUPDATA__V35*/ meltfptr[31] = 0;
    /*^clear */
	   /*clear *//*_#MAPOBJECT_COUNT__L14*/ meltfnum[13] = 0;
    /*^clear */
	   /*clear *//*_.TUPSTEP__V36*/ meltfptr[32] = 0;

    MELT_LOCATION ("warmelt-normatch.melt:6789:/ clear");
	   /*clear *//*_.MAPDATA__V4*/ meltfptr[3] = 0;
    /*^clear */
	   /*clear *//*_.MAPSTEP__V5*/ meltfptr[4] = 0;
    /*^clear */
	   /*clear *//*_.NODEBUF__V6*/ meltfptr[5] = 0;
    /*^clear */
	   /*clear *//*_.EDGEBUF__V7*/ meltfptr[6] = 0;
    /*^clear */
	   /*clear *//*_.DATACOUNTBOX__V8*/ meltfptr[7] = 0;
    /*^clear */
	   /*clear *//*_#CNT__L1*/ meltfnum[0] = 0;
    /*^clear */
	   /*clear *//*_#MDATAHASH__L2*/ meltfnum[1] = 0;
    /*^clear */
	   /*clear *//*_.PATHSBUF__V9*/ meltfptr[8] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V10*/ meltfptr[9] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V12*/ meltfptr[10] = 0;
    /*^clear */
	   /*clear *//*_.STRBUF2STRING__V16*/ meltfptr[12] = 0;
    /*^clear */
	   /*clear *//*_.DOTFILENAME__V17*/ meltfptr[13] = 0;
    /*^clear */
	   /*clear *//*_.MG__V18*/ meltfptr[17] = 0;
    /*^clear */
	   /*clear *//*_.LETREC___V20*/ meltfptr[19] = 0;
    /*epilog */
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("MG_ALTDRAW_GRAPHVIZ", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_135_warmelt_normatch_MG_ALTDRAW_GRAPHVIZ_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*noretval */ NULL);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_135_warmelt_normatch_MG_ALTDRAW_GRAPHVIZ */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_136_warmelt_normatch_LAMBDA___30__ (meltclosure_ptr_t meltclosp_,
					     melt_ptr_t meltfirstargp_,
					     const melt_argdescr_cell_t
					     meltxargdescr_[],
					     union meltparam_un *
					     meltxargtab_,
					     const melt_argdescr_cell_t
					     meltxresdescr_[],
					     union meltparam_un *
					     meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_136_warmelt_normatch_LAMBDA___30___melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_136_warmelt_normatch_LAMBDA___30___st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 38
    melt_ptr_t mcfr_varptr[38];
#define MELTFRAM_NBVARNUM 11
    long mcfr_varnum[11];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_136_warmelt_normatch_LAMBDA___30__ is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_136_warmelt_normatch_LAMBDA___30___st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 38; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_136_warmelt_normatch_LAMBDA___30__ nbval 38*/
  meltfram__.mcfr_nbvar = 38 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("LAMBDA_", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-normatch.melt:6826:/ getarg");
 /*_.DATA__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-normatch.melt:6827:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L1*/ meltfnum[0] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-normatch.melt:6827:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L1*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-normatch.melt:6827:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-normatch.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 6827;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "mg_altdraw_graphviz/scandata data=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.DATA__V2*/ meltfptr[1];
	      /*_.MELT_DEBUG_FUN__V5*/ meltfptr[4] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V4*/ meltfptr[3] = /*_.MELT_DEBUG_FUN__V5*/ meltfptr[4];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-normatch.melt:6827:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V5*/ meltfptr[4] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V4*/ meltfptr[3] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-normatch.melt:6827:/ quasiblock");


      /*_.PROGN___V6*/ meltfptr[4] = /*_.IF___V4*/ meltfptr[3];;
      /*^compute */
      /*_.IFCPP___V3*/ meltfptr[2] = /*_.PROGN___V6*/ meltfptr[4];;
      /*epilog */

      MELT_LOCATION ("warmelt-normatch.melt:6827:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L1*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IF___V4*/ meltfptr[3] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V6*/ meltfptr[4] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V3*/ meltfptr[2] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-normatch.melt:6828:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L3*/ meltfnum[1] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.DATA__V2*/ meltfptr[1]),
			     (melt_ptr_t) (( /*!CLASS_MATCHED_DATA */
					    meltfrout->tabval[1])));;
      MELT_LOCATION ("warmelt-normatch.melt:6828:/ cond");
      /*cond */ if ( /*_#IS_A__L3*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V8*/ meltfptr[4] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-normatch.melt:6828:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("mg_altdraw_graphviz.scandata check data"),
				  ("warmelt-normatch.melt")
				  ? ("warmelt-normatch.melt") : __FILE__,
				  (6828) ? (6828) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V8*/ meltfptr[4] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V7*/ meltfptr[3] = /*_.IFELSE___V8*/ meltfptr[4];;
      /*epilog */

      MELT_LOCATION ("warmelt-normatch.melt:6828:/ clear");
	     /*clear *//*_#IS_A__L3*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V8*/ meltfptr[4] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V7*/ meltfptr[3] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-normatch.melt:6829:/ quasiblock");


 /*_.MAPOBJECT_GET__V10*/ meltfptr[9] =
      /*mapobject_get */
      melt_get_mapobjects ((meltmapobjects_ptr_t)
			   (( /*~MAPDATA */ meltfclos->tabval[0])),
			   (meltobject_ptr_t) ( /*_.DATA__V2*/ meltfptr[1]));;
    MELT_LOCATION ("warmelt-normatch.melt:6829:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      /*_.OLDNAME__V11*/ meltfptr[10] =
	melt_apply ((meltclosure_ptr_t)
		    ( /*_.MAPOBJECT_GET__V10*/ meltfptr[9]),
		    (melt_ptr_t) (NULL), (""), (union meltparam_un *) 0, "",
		    (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-normatch.melt:6831:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^cond */
    /*cond */ if ( /*_.OLDNAME__V11*/ meltfptr[10])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{


#if MELT_HAVE_DEBUG
	  MELT_LOCATION ("warmelt-normatch.melt:6833:/ cppif.then");
	  /*^block */
	  /*anyblock */
	  {


	    {
	      /*^locexp */
	      /*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	      melt_dbgcounter++;
#endif
	      ;
	    }
	    ;
	    /*^checksignal */
	    MELT_CHECK_SIGNAL ();
	    ;
     /*_#MELT_NEED_DBG__L4*/ meltfnum[0] =
	      /*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	      ( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	      0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	      ;;
	    MELT_LOCATION ("warmelt-normatch.melt:6833:/ cond");
	    /*cond */ if ( /*_#MELT_NEED_DBG__L4*/ meltfnum[0])	/*then */
	      {
		/*^cond.then */
		/*^block */
		/*anyblock */
		{

       /*_#THE_MELTCALLCOUNT__L5*/ meltfnum[1] =
#ifdef meltcallcount
		    meltcallcount	/* the_meltcallcount */
#else
		    0L
#endif /* meltcallcount the_meltcallcount */
		    ;;
		  MELT_LOCATION ("warmelt-normatch.melt:6833:/ checksignal");
		  MELT_CHECK_SIGNAL ();
		  ;
		  /*^apply */
		  /*apply */
		  {
		    union meltparam_un argtab[5];
		    memset (&argtab, 0, sizeof (argtab));
		    /*^apply.arg */
		    argtab[0].meltbp_long =
		      /*_#THE_MELTCALLCOUNT__L5*/ meltfnum[1];
		    /*^apply.arg */
		    argtab[1].meltbp_cstring = "warmelt-normatch.melt";
		    /*^apply.arg */
		    argtab[2].meltbp_long = 6833;
		    /*^apply.arg */
		    argtab[3].meltbp_cstring =
		      "mg_altdraw_graphviz/scandata found oldname";
		    /*^apply.arg */
		    argtab[4].meltbp_aptr =
		      (melt_ptr_t *) & /*_.OLDNAME__V11*/ meltfptr[10];
		    /*_.MELT_DEBUG_FUN__V15*/ meltfptr[14] =
		      melt_apply ((meltclosure_ptr_t)
				  (( /*!MELT_DEBUG_FUN */ meltfrout->
				    tabval[0])),
				  (melt_ptr_t) (( /*nil */ NULL)),
				  (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_PTR ""), argtab, "",
				  (union meltparam_un *) 0);
		  }
		  ;
		  /*_.IF___V14*/ meltfptr[13] =
		    /*_.MELT_DEBUG_FUN__V15*/ meltfptr[14];;
		  /*epilog */

		  MELT_LOCATION ("warmelt-normatch.melt:6833:/ clear");
		 /*clear *//*_#THE_MELTCALLCOUNT__L5*/ meltfnum[1] = 0;
		  /*^clear */
		 /*clear *//*_.MELT_DEBUG_FUN__V15*/ meltfptr[14] = 0;
		}
		;
	      }
	    else
	      {			/*^cond.else */

      /*_.IF___V14*/ meltfptr[13] = NULL;;
	      }
	    ;
	    MELT_LOCATION ("warmelt-normatch.melt:6833:/ quasiblock");


	    /*_.PROGN___V16*/ meltfptr[14] = /*_.IF___V14*/ meltfptr[13];;
	    /*^compute */
	    /*_.IFCPP___V13*/ meltfptr[12] = /*_.PROGN___V16*/ meltfptr[14];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-normatch.melt:6833:/ clear");
	       /*clear *//*_#MELT_NEED_DBG__L4*/ meltfnum[0] = 0;
	    /*^clear */
	       /*clear *//*_.IF___V14*/ meltfptr[13] = 0;
	    /*^clear */
	       /*clear *//*_.PROGN___V16*/ meltfptr[14] = 0;
	  }

#else /*MELT_HAVE_DEBUG */
	  /*^cppif.else */
	  /*_.IFCPP___V13*/ meltfptr[12] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
	  ;
	  MELT_LOCATION ("warmelt-normatch.melt:6834:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^quasiblock */


	  /*_.RETVAL___V1*/ meltfptr[0] = /*_.OLDNAME__V11*/ meltfptr[10];;

	  {
	    MELT_LOCATION ("warmelt-normatch.melt:6834:/ locexp");
	    /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
	    if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	      melt_warn_for_no_expected_secondary_results ();
	    /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
	    ;
	  }
	  ;
	  /*^finalreturn */
	  ;
	  /*finalret */ goto labend_rout;
	  MELT_LOCATION ("warmelt-normatch.melt:6832:/ quasiblock");


	  /*_.PROGN___V18*/ meltfptr[14] = /*_.RETURN___V17*/ meltfptr[13];;
	  /*^compute */
	  /*_.IF___V12*/ meltfptr[11] = /*_.PROGN___V18*/ meltfptr[14];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-normatch.melt:6831:/ clear");
	     /*clear *//*_.IFCPP___V13*/ meltfptr[12] = 0;
	  /*^clear */
	     /*clear *//*_.RETURN___V17*/ meltfptr[13] = 0;
	  /*^clear */
	     /*clear *//*_.PROGN___V18*/ meltfptr[14] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

  /*_.IF___V12*/ meltfptr[11] = NULL;;
      }
    ;
    /*^compute */
    /*_.LET___V9*/ meltfptr[4] = /*_.IF___V12*/ meltfptr[11];;

    MELT_LOCATION ("warmelt-normatch.melt:6829:/ clear");
	   /*clear *//*_.MAPOBJECT_GET__V10*/ meltfptr[9] = 0;
    /*^clear */
	   /*clear *//*_.OLDNAME__V11*/ meltfptr[10] = 0;
    /*^clear */
	   /*clear *//*_.IF___V12*/ meltfptr[11] = 0;
    MELT_LOCATION ("warmelt-normatch.melt:6835:/ quasiblock");


 /*_.NAMBUF__V20*/ meltfptr[13] =
      (melt_ptr_t)
      meltgc_new_strbuf ((meltobject_ptr_t)
			 (( /*!DISCR_STRBUF */ meltfrout->tabval[2])),
			 (const char *) 0);;
    MELT_LOCATION ("warmelt-normatch.melt:6836:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.DATA__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 1, "MDATA_SYMB");
  /*_.MSYMB__V21*/ meltfptr[14] = slot;
    };
    ;
 /*_#GET_INT__L6*/ meltfnum[1] =
      (melt_get_int
       ((melt_ptr_t) (( /*~DATACOUNTBOX */ meltfclos->tabval[1]))));;
    /*^compute */
 /*_#DATACNT__L7*/ meltfnum[0] =
      ((1) + ( /*_#GET_INT__L6*/ meltfnum[1]));;
    MELT_LOCATION ("warmelt-normatch.melt:6838:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.DATA__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 4, "MDATA_STEPS");
  /*_.MSTEPS__V22*/ meltfptr[9] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-normatch.melt:6839:/ cond");
    /*cond */ if (
		   /*ifisa */
		   melt_is_instance_of ((melt_ptr_t)
					( /*_.MSYMB__V21*/ meltfptr[14]),
					(melt_ptr_t) (( /*!CLASS_NAMED */
						       meltfrout->tabval[3])))
      )				/*then */
      {
	/*^cond.then */
	/*^getslot */
	{
	  melt_ptr_t slot = NULL, obj = NULL;
	  obj = (melt_ptr_t) ( /*_.MSYMB__V21*/ meltfptr[14]) /*=obj*/ ;
	  melt_object_get_field (slot, obj, 1, "NAMED_NAME");
   /*_.SYMBNAME__V23*/ meltfptr[10] = slot;
	};
	;
      }
    else
      {				/*^cond.else */

  /*_.SYMBNAME__V23*/ meltfptr[10] = NULL;;
      }
    ;
    MELT_LOCATION ("warmelt-normatch.melt:6841:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[4];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_cstring = "mdata_";
      /*^apply.arg */
      argtab[1].meltbp_long = /*_#DATACNT__L7*/ meltfnum[0];
      /*^apply.arg */
      argtab[2].meltbp_cstring = "_";
      /*^apply.arg */
      argtab[3].meltbp_aptr =
	(melt_ptr_t *) & /*_.SYMBNAME__V23*/ meltfptr[10];
      /*_.ADD2OUT__V24*/ meltfptr[11] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!ADD2OUT */ meltfrout->tabval[4])),
		    (melt_ptr_t) ( /*_.NAMBUF__V20*/ meltfptr[13]),
		    (MELTBPARSTR_CSTRING MELTBPARSTR_LONG MELTBPARSTR_CSTRING
		     MELTBPARSTR_PTR ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-normatch.melt:6842:/ locexp");
      melt_put_int ((melt_ptr_t) (( /*~DATACOUNTBOX */ meltfclos->tabval[1])),
		    ( /*_#DATACNT__L7*/ meltfnum[0]));
    }
    ;
    MELT_LOCATION ("warmelt-normatch.melt:6843:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#IS_A__L8*/ meltfnum[7] =
      melt_is_instance_of ((melt_ptr_t) ( /*_.MSYMB__V21*/ meltfptr[14]),
			   (melt_ptr_t) (( /*!CLASS_CLONED_SYMBOL */
					  meltfrout->tabval[5])));;
    MELT_LOCATION ("warmelt-normatch.melt:6843:/ cond");
    /*cond */ if ( /*_#IS_A__L8*/ meltfnum[7])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-normatch.melt:6844:/ quasiblock");


	  /*^cond */
	  /*cond */ if (
			 /*ifisa */
			 melt_is_instance_of ((melt_ptr_t)
					      ( /*_.MSYMB__V21*/
					       meltfptr[14]),
					      (melt_ptr_t) (( /*!CLASS_CLONED_SYMBOL */ meltfrout->tabval[5])))
	    )			/*then */
	    {
	      /*^cond.then */
	      /*^getslot */
	      {
		melt_ptr_t slot = NULL, obj = NULL;
		obj = (melt_ptr_t) ( /*_.MSYMB__V21*/ meltfptr[14]) /*=obj*/ ;
		melt_object_get_field (slot, obj, 3, "CSYM_URANK");
     /*_.CSYM_URANK__V27*/ meltfptr[26] = slot;
	      };
	      ;
	    }
	  else
	    {			/*^cond.else */

    /*_.CSYM_URANK__V27*/ meltfptr[26] = NULL;;
	    }
	  ;
	  /*^compute */
   /*_#CLONRK__L9*/ meltfnum[8] =
	    (melt_get_int
	     ((melt_ptr_t) ( /*_.CSYM_URANK__V27*/ meltfptr[26])));;
	  MELT_LOCATION ("warmelt-normatch.melt:6846:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^apply */
	  /*apply */
	  {
	    union meltparam_un argtab[2];
	    memset (&argtab, 0, sizeof (argtab));
	    /*^apply.arg */
	    argtab[0].meltbp_cstring = "__";
	    /*^apply.arg */
	    argtab[1].meltbp_long = /*_#CLONRK__L9*/ meltfnum[8];
	    /*_.ADD2OUT__V28*/ meltfptr[27] =
	      melt_apply ((meltclosure_ptr_t)
			  (( /*!ADD2OUT */ meltfrout->tabval[4])),
			  (melt_ptr_t) ( /*_.NAMBUF__V20*/ meltfptr[13]),
			  (MELTBPARSTR_CSTRING MELTBPARSTR_LONG ""), argtab,
			  "", (union meltparam_un *) 0);
	  }
	  ;
	  /*_.LET___V26*/ meltfptr[25] = /*_.ADD2OUT__V28*/ meltfptr[27];;

	  MELT_LOCATION ("warmelt-normatch.melt:6844:/ clear");
	     /*clear *//*_.CSYM_URANK__V27*/ meltfptr[26] = 0;
	  /*^clear */
	     /*clear *//*_#CLONRK__L9*/ meltfnum[8] = 0;
	  /*^clear */
	     /*clear *//*_.ADD2OUT__V28*/ meltfptr[27] = 0;
	  /*_.IF___V25*/ meltfptr[24] = /*_.LET___V26*/ meltfptr[25];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-normatch.melt:6843:/ clear");
	     /*clear *//*_.LET___V26*/ meltfptr[25] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

  /*_.IF___V25*/ meltfptr[24] = NULL;;
      }
    ;
    MELT_LOCATION ("warmelt-normatch.melt:6847:/ quasiblock");


 /*_.NEWNAME__V30*/ meltfptr[27] =
      (meltgc_new_stringdup
       ((meltobject_ptr_t) (( /*!DISCR_STRING */ meltfrout->tabval[6])),
	melt_strbuf_str ((melt_ptr_t) ( /*_.NAMBUF__V20*/ meltfptr[13]))));;

    {
      MELT_LOCATION ("warmelt-normatch.melt:6849:/ locexp");
      meltgc_put_mapobjects ((meltmapobjects_ptr_t)
			     (( /*~MAPDATA */ meltfclos->tabval[0])),
			     (meltobject_ptr_t) ( /*_.DATA__V2*/ meltfptr[1]),
			     (melt_ptr_t) ( /*_.NEWNAME__V30*/ meltfptr[27]));
    }
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-normatch.melt:6850:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L10*/ meltfnum[8] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-normatch.melt:6850:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L10*/ meltfnum[8])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L11*/ meltfnum[10] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-normatch.melt:6850:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[7];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long =
		/*_#THE_MELTCALLCOUNT__L11*/ meltfnum[10];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-normatch.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 6850;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring =
		"mg_altdraw_graphviz/scandata newname=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.NEWNAME__V30*/ meltfptr[27];
	      /*^apply.arg */
	      argtab[5].meltbp_cstring = " updated mapdata=";
	      /*^apply.arg */
	      argtab[6].meltbp_aptr =
		(melt_ptr_t *) & ( /*~MAPDATA */ meltfclos->tabval[0]);
	      /*_.MELT_DEBUG_FUN__V33*/ meltfptr[32] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V32*/ meltfptr[31] =
	      /*_.MELT_DEBUG_FUN__V33*/ meltfptr[32];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-normatch.melt:6850:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L11*/ meltfnum[10] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V33*/ meltfptr[32] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V32*/ meltfptr[31] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-normatch.melt:6850:/ quasiblock");


      /*_.PROGN___V34*/ meltfptr[32] = /*_.IF___V32*/ meltfptr[31];;
      /*^compute */
      /*_.IFCPP___V31*/ meltfptr[25] = /*_.PROGN___V34*/ meltfptr[32];;
      /*epilog */

      MELT_LOCATION ("warmelt-normatch.melt:6850:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L10*/ meltfnum[8] = 0;
      /*^clear */
	     /*clear *//*_.IF___V32*/ meltfptr[31] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V34*/ meltfptr[32] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V31*/ meltfptr[25] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    /*citerblock FOREACH_IN_LIST */
    {
      /* start foreach_in_list meltcit1__EACHLIST */
      for ( /*_.CURPAIR__V35*/ meltfptr[31] =
	   melt_list_first ((melt_ptr_t) /*_.MSTEPS__V22*/ meltfptr[9]);
	   melt_magic_discr ((melt_ptr_t) /*_.CURPAIR__V35*/ meltfptr[31]) ==
	   MELTOBMAG_PAIR;
	   /*_.CURPAIR__V35*/ meltfptr[31] =
	   melt_pair_tail ((melt_ptr_t) /*_.CURPAIR__V35*/ meltfptr[31]))
	{
	  /*_.CURSTEP__V36*/ meltfptr[32] =
	    melt_pair_head ((melt_ptr_t) /*_.CURPAIR__V35*/ meltfptr[31]);


	  MELT_LOCATION ("warmelt-normatch.melt:6854:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^apply */
	  /*apply */
	  {
	    /*_.SCANSTEP__V37*/ meltfptr[36] =
	      melt_apply ((meltclosure_ptr_t)
			  (( /*~SCANSTEP */ meltfclos->tabval[2])),
			  (melt_ptr_t) ( /*_.CURSTEP__V36*/ meltfptr[32]),
			  (""), (union meltparam_un *) 0, "",
			  (union meltparam_un *) 0);
	  }
	  ;
	}			/* end foreach_in_list meltcit1__EACHLIST */
     /*_.CURPAIR__V35*/ meltfptr[31] = NULL;
     /*_.CURSTEP__V36*/ meltfptr[32] = NULL;


      /*citerepilog */

      MELT_LOCATION ("warmelt-normatch.melt:6851:/ clear");
	    /*clear *//*_.CURPAIR__V35*/ meltfptr[31] = 0;
      /*^clear */
	    /*clear *//*_.CURSTEP__V36*/ meltfptr[32] = 0;
      /*^clear */
	    /*clear *//*_.SCANSTEP__V37*/ meltfptr[36] = 0;
    }				/*endciterblock FOREACH_IN_LIST */
    ;
    MELT_LOCATION ("warmelt-normatch.melt:6855:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.NEWNAME__V30*/ meltfptr[27];;

    {
      MELT_LOCATION ("warmelt-normatch.melt:6855:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*_.LET___V29*/ meltfptr[26] = /*_.RETURN___V38*/ meltfptr[37];;

    MELT_LOCATION ("warmelt-normatch.melt:6847:/ clear");
	   /*clear *//*_.NEWNAME__V30*/ meltfptr[27] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V31*/ meltfptr[25] = 0;
    /*^clear */
	   /*clear *//*_.RETURN___V38*/ meltfptr[37] = 0;
    /*_.LET___V19*/ meltfptr[12] = /*_.LET___V29*/ meltfptr[26];;

    MELT_LOCATION ("warmelt-normatch.melt:6835:/ clear");
	   /*clear *//*_.NAMBUF__V20*/ meltfptr[13] = 0;
    /*^clear */
	   /*clear *//*_.MSYMB__V21*/ meltfptr[14] = 0;
    /*^clear */
	   /*clear *//*_#GET_INT__L6*/ meltfnum[1] = 0;
    /*^clear */
	   /*clear *//*_#DATACNT__L7*/ meltfnum[0] = 0;
    /*^clear */
	   /*clear *//*_.MSTEPS__V22*/ meltfptr[9] = 0;
    /*^clear */
	   /*clear *//*_.SYMBNAME__V23*/ meltfptr[10] = 0;
    /*^clear */
	   /*clear *//*_.ADD2OUT__V24*/ meltfptr[11] = 0;
    /*^clear */
	   /*clear *//*_#IS_A__L8*/ meltfnum[7] = 0;
    /*^clear */
	   /*clear *//*_.IF___V25*/ meltfptr[24] = 0;
    /*^clear */
	   /*clear *//*_.LET___V29*/ meltfptr[26] = 0;
    MELT_LOCATION ("warmelt-normatch.melt:6826:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.LET___V19*/ meltfptr[12];;

    {
      MELT_LOCATION ("warmelt-normatch.melt:6826:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V3*/ meltfptr[2] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V7*/ meltfptr[3] = 0;
    /*^clear */
	   /*clear *//*_.LET___V9*/ meltfptr[4] = 0;
    /*^clear */
	   /*clear *//*_.LET___V19*/ meltfptr[12] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("LAMBDA_", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_136_warmelt_normatch_LAMBDA___30___melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_136_warmelt_normatch_LAMBDA___30__ */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_137_warmelt_normatch_LAMBDA___31__ (meltclosure_ptr_t meltclosp_,
					     melt_ptr_t meltfirstargp_,
					     const melt_argdescr_cell_t
					     meltxargdescr_[],
					     union meltparam_un *
					     meltxargtab_,
					     const melt_argdescr_cell_t
					     meltxresdescr_[],
					     union meltparam_un *
					     meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_137_warmelt_normatch_LAMBDA___31___melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_137_warmelt_normatch_LAMBDA___31___st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 37
    melt_ptr_t mcfr_varptr[37];
#define MELTFRAM_NBVARNUM 7
    long mcfr_varnum[7];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_137_warmelt_normatch_LAMBDA___31__ is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_137_warmelt_normatch_LAMBDA___31___st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 37; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_137_warmelt_normatch_LAMBDA___31__ nbval 37*/
  meltfram__.mcfr_nbvar = 37 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("LAMBDA_", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-normatch.melt:6859:/ getarg");
 /*_.STEP__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-normatch.melt:6860:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L1*/ meltfnum[0] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-normatch.melt:6860:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L1*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-normatch.melt:6860:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-normatch.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 6860;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "mg_altdraw_graphviz/scanstep step=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.STEP__V2*/ meltfptr[1];
	      /*_.MELT_DEBUG_FUN__V5*/ meltfptr[4] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V4*/ meltfptr[3] = /*_.MELT_DEBUG_FUN__V5*/ meltfptr[4];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-normatch.melt:6860:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V5*/ meltfptr[4] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V4*/ meltfptr[3] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-normatch.melt:6860:/ quasiblock");


      /*_.PROGN___V6*/ meltfptr[4] = /*_.IF___V4*/ meltfptr[3];;
      /*^compute */
      /*_.IFCPP___V3*/ meltfptr[2] = /*_.PROGN___V6*/ meltfptr[4];;
      /*epilog */

      MELT_LOCATION ("warmelt-normatch.melt:6860:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L1*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IF___V4*/ meltfptr[3] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V6*/ meltfptr[4] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V3*/ meltfptr[2] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-normatch.melt:6861:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L3*/ meltfnum[1] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.STEP__V2*/ meltfptr[1]),
			     (melt_ptr_t) (( /*!CLASS_MATCH_STEP */
					    meltfrout->tabval[1])));;
      MELT_LOCATION ("warmelt-normatch.melt:6861:/ cond");
      /*cond */ if ( /*_#IS_A__L3*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V8*/ meltfptr[4] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-normatch.melt:6861:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("mg_altdraw_graphviz.scanstep check step"),
				  ("warmelt-normatch.melt")
				  ? ("warmelt-normatch.melt") : __FILE__,
				  (6861) ? (6861) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V8*/ meltfptr[4] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V7*/ meltfptr[3] = /*_.IFELSE___V8*/ meltfptr[4];;
      /*epilog */

      MELT_LOCATION ("warmelt-normatch.melt:6861:/ clear");
	     /*clear *//*_#IS_A__L3*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V8*/ meltfptr[4] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V7*/ meltfptr[3] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-normatch.melt:6862:/ quasiblock");


 /*_.OLDNAME__V10*/ meltfptr[9] =
      /*mapobject_get */
      melt_get_mapobjects ((meltmapobjects_ptr_t)
			   (( /*~MAPSTEP */ meltfclos->tabval[0])),
			   (meltobject_ptr_t) ( /*_.STEP__V2*/ meltfptr[1]));;
    MELT_LOCATION ("warmelt-normatch.melt:6864:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^cond */
    /*cond */ if ( /*_.OLDNAME__V10*/ meltfptr[9])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{


#if MELT_HAVE_DEBUG
	  MELT_LOCATION ("warmelt-normatch.melt:6866:/ cppif.then");
	  /*^block */
	  /*anyblock */
	  {


	    {
	      /*^locexp */
	      /*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	      melt_dbgcounter++;
#endif
	      ;
	    }
	    ;
	    /*^checksignal */
	    MELT_CHECK_SIGNAL ();
	    ;
     /*_#MELT_NEED_DBG__L4*/ meltfnum[0] =
	      /*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	      ( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	      0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	      ;;
	    MELT_LOCATION ("warmelt-normatch.melt:6866:/ cond");
	    /*cond */ if ( /*_#MELT_NEED_DBG__L4*/ meltfnum[0])	/*then */
	      {
		/*^cond.then */
		/*^block */
		/*anyblock */
		{

       /*_#THE_MELTCALLCOUNT__L5*/ meltfnum[1] =
#ifdef meltcallcount
		    meltcallcount	/* the_meltcallcount */
#else
		    0L
#endif /* meltcallcount the_meltcallcount */
		    ;;
		  MELT_LOCATION ("warmelt-normatch.melt:6866:/ checksignal");
		  MELT_CHECK_SIGNAL ();
		  ;
		  /*^apply */
		  /*apply */
		  {
		    union meltparam_un argtab[5];
		    memset (&argtab, 0, sizeof (argtab));
		    /*^apply.arg */
		    argtab[0].meltbp_long =
		      /*_#THE_MELTCALLCOUNT__L5*/ meltfnum[1];
		    /*^apply.arg */
		    argtab[1].meltbp_cstring = "warmelt-normatch.melt";
		    /*^apply.arg */
		    argtab[2].meltbp_long = 6866;
		    /*^apply.arg */
		    argtab[3].meltbp_cstring =
		      "mg_altdraw_graphviz.scanstep found oldname";
		    /*^apply.arg */
		    argtab[4].meltbp_aptr =
		      (melt_ptr_t *) & /*_.OLDNAME__V10*/ meltfptr[9];
		    /*_.MELT_DEBUG_FUN__V14*/ meltfptr[13] =
		      melt_apply ((meltclosure_ptr_t)
				  (( /*!MELT_DEBUG_FUN */ meltfrout->
				    tabval[0])),
				  (melt_ptr_t) (( /*nil */ NULL)),
				  (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_PTR ""), argtab, "",
				  (union meltparam_un *) 0);
		  }
		  ;
		  /*_.IF___V13*/ meltfptr[12] =
		    /*_.MELT_DEBUG_FUN__V14*/ meltfptr[13];;
		  /*epilog */

		  MELT_LOCATION ("warmelt-normatch.melt:6866:/ clear");
		 /*clear *//*_#THE_MELTCALLCOUNT__L5*/ meltfnum[1] = 0;
		  /*^clear */
		 /*clear *//*_.MELT_DEBUG_FUN__V14*/ meltfptr[13] = 0;
		}
		;
	      }
	    else
	      {			/*^cond.else */

      /*_.IF___V13*/ meltfptr[12] = NULL;;
	      }
	    ;
	    MELT_LOCATION ("warmelt-normatch.melt:6866:/ quasiblock");


	    /*_.PROGN___V15*/ meltfptr[13] = /*_.IF___V13*/ meltfptr[12];;
	    /*^compute */
	    /*_.IFCPP___V12*/ meltfptr[11] = /*_.PROGN___V15*/ meltfptr[13];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-normatch.melt:6866:/ clear");
	       /*clear *//*_#MELT_NEED_DBG__L4*/ meltfnum[0] = 0;
	    /*^clear */
	       /*clear *//*_.IF___V13*/ meltfptr[12] = 0;
	    /*^clear */
	       /*clear *//*_.PROGN___V15*/ meltfptr[13] = 0;
	  }

#else /*MELT_HAVE_DEBUG */
	  /*^cppif.else */
	  /*_.IFCPP___V12*/ meltfptr[11] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
	  ;
	  MELT_LOCATION ("warmelt-normatch.melt:6867:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^quasiblock */


	  /*_.RETVAL___V1*/ meltfptr[0] = /*_.OLDNAME__V10*/ meltfptr[9];;

	  {
	    MELT_LOCATION ("warmelt-normatch.melt:6867:/ locexp");
	    /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
	    if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	      melt_warn_for_no_expected_secondary_results ();
	    /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
	    ;
	  }
	  ;
	  /*^finalreturn */
	  ;
	  /*finalret */ goto labend_rout;
	  MELT_LOCATION ("warmelt-normatch.melt:6865:/ quasiblock");


	  /*_.PROGN___V17*/ meltfptr[13] = /*_.RETURN___V16*/ meltfptr[12];;
	  /*^compute */
	  /*_.IF___V11*/ meltfptr[10] = /*_.PROGN___V17*/ meltfptr[13];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-normatch.melt:6864:/ clear");
	     /*clear *//*_.IFCPP___V12*/ meltfptr[11] = 0;
	  /*^clear */
	     /*clear *//*_.RETURN___V16*/ meltfptr[12] = 0;
	  /*^clear */
	     /*clear *//*_.PROGN___V17*/ meltfptr[13] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

  /*_.IF___V11*/ meltfptr[10] = NULL;;
      }
    ;
    /*^compute */
    /*_.LET___V9*/ meltfptr[4] = /*_.IF___V11*/ meltfptr[10];;

    MELT_LOCATION ("warmelt-normatch.melt:6862:/ clear");
	   /*clear *//*_.OLDNAME__V10*/ meltfptr[9] = 0;
    /*^clear */
	   /*clear *//*_.IF___V11*/ meltfptr[10] = 0;
    MELT_LOCATION ("warmelt-normatch.melt:6868:/ quasiblock");


 /*_.NAMBUF__V19*/ meltfptr[12] =
      (melt_ptr_t)
      meltgc_new_strbuf ((meltobject_ptr_t)
			 (( /*!DISCR_STRBUF */ meltfrout->tabval[2])),
			 (const char *) 0);;
    /*^compute */
 /*_#MAPCNT__L6*/ meltfnum[1] =
      (melt_count_mapobjects
       ((meltmapobjects_ptr_t) (( /*~MAPSTEP */ meltfclos->tabval[0]))));;
    /*^compute */
 /*_.DISCRIM__V20*/ meltfptr[13] =
      ((melt_ptr_t)
       (melt_discr ((melt_ptr_t) ( /*_.STEP__V2*/ meltfptr[1]))));;
    MELT_LOCATION ("warmelt-normatch.melt:6870:/ cond");
    /*cond */ if (
		   /*ifisa */
		   melt_is_instance_of ((melt_ptr_t)
					( /*_.DISCRIM__V20*/ meltfptr[13]),
					(melt_ptr_t) (( /*!CLASS_NAMED */
						       meltfrout->tabval[3])))
      )				/*then */
      {
	/*^cond.then */
	/*^getslot */
	{
	  melt_ptr_t slot = NULL, obj = NULL;
	  obj = (melt_ptr_t) ( /*_.DISCRIM__V20*/ meltfptr[13]) /*=obj*/ ;
	  melt_object_get_field (slot, obj, 1, "NAMED_NAME");
   /*_.DISNAM__V21*/ meltfptr[9] = slot;
	};
	;
      }
    else
      {				/*^cond.else */

  /*_.DISNAM__V21*/ meltfptr[9] = NULL;;
      }
    ;
    MELT_LOCATION ("warmelt-normatch.melt:6872:/ cond");
    /*cond */ if (
		   /*ifisa */
		   melt_is_instance_of ((melt_ptr_t)
					( /*_.STEP__V2*/ meltfptr[1]),
					(melt_ptr_t) (( /*!CLASS_MATCH_STEP_WITH_DATA */ meltfrout->tabval[4])))
      )				/*then */
      {
	/*^cond.then */
	/*^getslot */
	{
	  melt_ptr_t slot = NULL, obj = NULL;
	  obj = (melt_ptr_t) ( /*_.STEP__V2*/ meltfptr[1]) /*=obj*/ ;
	  melt_object_get_field (slot, obj, 3, "MSTEP_DATA");
   /*_.SDATA__V22*/ meltfptr[10] = slot;
	};
	;
      }
    else
      {				/*^cond.else */

  /*_.SDATA__V22*/ meltfptr[10] = NULL;;
      }
    ;

    {
      MELT_LOCATION ("warmelt-normatch.melt:6875:/ locexp");
      /*CHUNKDISNAM__1 */ meltgc_add_strbuf ((melt_ptr_t) /*_.NAMBUF__V19*/
					     meltfptr[12],
					     melt_string_str ((melt_ptr_t)
							      /*_.DISNAM__V21*/
							      meltfptr[9])
					     + strlen ("CLASS_"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-normatch.melt:6878:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.NAMBUF__V19*/ meltfptr[12]),
			   ("__"));
    }
    ;
 /*_#I__L7*/ meltfnum[0] =
      (( /*_#MAPCNT__L6*/ meltfnum[1]) + (1));;

    {
      MELT_LOCATION ("warmelt-normatch.melt:6879:/ locexp");
      meltgc_add_strbuf_dec ((melt_ptr_t) ( /*_.NAMBUF__V19*/ meltfptr[12]),
			     ( /*_#I__L7*/ meltfnum[0]));
    }
    ;
 /*_.STRBUF2STRING__V23*/ meltfptr[22] =
      (meltgc_new_stringdup
       ((meltobject_ptr_t) (( /*!DISCR_STRING */ meltfrout->tabval[5])),
	melt_strbuf_str ((melt_ptr_t) ( /*_.NAMBUF__V19*/ meltfptr[12]))));;

    {
      MELT_LOCATION ("warmelt-normatch.melt:6880:/ locexp");
      meltgc_put_mapobjects ((meltmapobjects_ptr_t)
			     (( /*~MAPSTEP */ meltfclos->tabval[0])),
			     (meltobject_ptr_t) ( /*_.STEP__V2*/ meltfptr[1]),
			     (melt_ptr_t) ( /*_.STRBUF2STRING__V23*/
					   meltfptr[22]));
    }
    ;
    MELT_LOCATION ("warmelt-normatch.melt:6881:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^cond */
    /*cond */ if ( /*_.SDATA__V22*/ meltfptr[10])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  /*^checksignal */
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^apply */
	  /*apply */
	  {
	    /*_.SCANDATA__V25*/ meltfptr[24] =
	      melt_apply ((meltclosure_ptr_t)
			  (( /*~SCANDATA */ meltfclos->tabval[1])),
			  (melt_ptr_t) ( /*_.SDATA__V22*/ meltfptr[10]), (""),
			  (union meltparam_un *) 0, "",
			  (union meltparam_un *) 0);
	  }
	  ;
	  /*_.IF___V24*/ meltfptr[23] = /*_.SCANDATA__V25*/ meltfptr[24];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-normatch.melt:6881:/ clear");
	     /*clear *//*_.SCANDATA__V25*/ meltfptr[24] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

  /*_.IF___V24*/ meltfptr[23] = NULL;;
      }
    ;
    MELT_LOCATION ("warmelt-normatch.melt:6882:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^msend */
    /*msend */
    {
      union meltparam_un argtab[2];
      memset (&argtab, 0, sizeof (argtab));
      /*^ojbmsend.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & ( /*~SCANDATA */ meltfclos->tabval[1]);
      /*^ojbmsend.arg */
      argtab[1].meltbp_aptr = /*nil */ (melt_ptr_t *) NULL;
      /*_.SCAN_STEP_DATA__V26*/ meltfptr[24] =
	meltgc_send ((melt_ptr_t) ( /*_.STEP__V2*/ meltfptr[1]),
		     (melt_ptr_t) (( /*!SCAN_STEP_DATA */ meltfrout->
				    tabval[6])),
		     (MELTBPARSTR_PTR MELTBPARSTR_PTR ""), argtab, "",
		     (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-normatch.melt:6883:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^msend */
    /*msend */
    {
      union meltparam_un argtab[2];
      memset (&argtab, 0, sizeof (argtab));
      /*^ojbmsend.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & ( /*~SCANFLAG */ meltfclos->tabval[2]);
      /*^ojbmsend.arg */
      argtab[1].meltbp_aptr = /*nil */ (melt_ptr_t *) NULL;
      /*_.SCAN_STEP_FLAG__V27*/ meltfptr[26] =
	meltgc_send ((melt_ptr_t) ( /*_.STEP__V2*/ meltfptr[1]),
		     (melt_ptr_t) (( /*!SCAN_STEP_FLAG */ meltfrout->
				    tabval[7])),
		     (MELTBPARSTR_PTR MELTBPARSTR_PTR ""), argtab, "",
		     (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-normatch.melt:6884:/ quasiblock");


    MELT_LOCATION ("warmelt-normatch.melt:6886:/ cond");
    /*cond */ if (
		   /*ifisa */
		   melt_is_instance_of ((melt_ptr_t)
					( /*_.STEP__V2*/ meltfptr[1]),
					(melt_ptr_t) (( /*!CLASS_MATCH_STEP_THEN */ meltfrout->tabval[8])))
      )				/*then */
      {
	/*^cond.then */
	/*^getslot */
	{
	  melt_ptr_t slot = NULL, obj = NULL;
	  obj = (melt_ptr_t) ( /*_.STEP__V2*/ meltfptr[1]) /*=obj*/ ;
	  melt_object_get_field (slot, obj, 2, "MSTEP_THEN");
   /*_.STHEN__V29*/ meltfptr[28] = slot;
	};
	;
      }
    else
      {				/*^cond.else */

  /*_.STHEN__V29*/ meltfptr[28] = NULL;;
      }
    ;
    MELT_LOCATION ("warmelt-normatch.melt:6887:/ cond");
    /*cond */ if (
		   /*ifisa */
		   melt_is_instance_of ((melt_ptr_t)
					( /*_.STEP__V2*/ meltfptr[1]),
					(melt_ptr_t) (( /*!CLASS_MATCH_STEP_TEST */ meltfrout->tabval[9])))
      )				/*then */
      {
	/*^cond.then */
	/*^getslot */
	{
	  melt_ptr_t slot = NULL, obj = NULL;
	  obj = (melt_ptr_t) ( /*_.STEP__V2*/ meltfptr[1]) /*=obj*/ ;
	  melt_object_get_field (slot, obj, 5, "MSTEP_ELSE");
   /*_.SELSE__V30*/ meltfptr[29] = slot;
	};
	;
      }
    else
      {				/*^cond.else */

  /*_.SELSE__V30*/ meltfptr[29] = NULL;;
      }
    ;
    MELT_LOCATION ("warmelt-normatch.melt:6888:/ cond");
    /*cond */ if (
		   /*ifisa */
		   melt_is_instance_of ((melt_ptr_t)
					( /*_.STEP__V2*/ meltfptr[1]),
					(melt_ptr_t) (( /*!CLASS_MATCH_STEP_WITH_FLAG */ meltfrout->tabval[10])))
      )				/*then */
      {
	/*^cond.then */
	/*^getslot */
	{
	  melt_ptr_t slot = NULL, obj = NULL;
	  obj = (melt_ptr_t) ( /*_.STEP__V2*/ meltfptr[1]) /*=obj*/ ;
	  melt_object_get_field (slot, obj, 3, "MSTEP_FLAG");
   /*_.SFLAG__V31*/ meltfptr[30] = slot;
	};
	;
      }
    else
      {				/*^cond.else */

  /*_.SFLAG__V31*/ meltfptr[30] = NULL;;
      }
    ;
    MELT_LOCATION ("warmelt-normatch.melt:6890:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^cond */
    /*cond */ if ( /*_.STHEN__V29*/ meltfptr[28])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  /*^checksignal */
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^apply */
	  /*apply */
	  {
	    /*_.SCANSTEP__V33*/ meltfptr[32] =
	      melt_apply ((meltclosure_ptr_t)
			  (( /*~SCANSTEP */ meltfclos->tabval[3])),
			  (melt_ptr_t) ( /*_.STHEN__V29*/ meltfptr[28]), (""),
			  (union meltparam_un *) 0, "",
			  (union meltparam_un *) 0);
	  }
	  ;
	  /*_.IF___V32*/ meltfptr[31] = /*_.SCANSTEP__V33*/ meltfptr[32];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-normatch.melt:6890:/ clear");
	     /*clear *//*_.SCANSTEP__V33*/ meltfptr[32] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

  /*_.IF___V32*/ meltfptr[31] = NULL;;
      }
    ;
    MELT_LOCATION ("warmelt-normatch.melt:6891:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^cond */
    /*cond */ if ( /*_.SELSE__V30*/ meltfptr[29])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  /*^checksignal */
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^apply */
	  /*apply */
	  {
	    /*_.SCANSTEP__V35*/ meltfptr[34] =
	      melt_apply ((meltclosure_ptr_t)
			  (( /*~SCANSTEP */ meltfclos->tabval[3])),
			  (melt_ptr_t) ( /*_.SELSE__V30*/ meltfptr[29]), (""),
			  (union meltparam_un *) 0, "",
			  (union meltparam_un *) 0);
	  }
	  ;
	  /*_.IF___V34*/ meltfptr[32] = /*_.SCANSTEP__V35*/ meltfptr[34];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-normatch.melt:6891:/ clear");
	     /*clear *//*_.SCANSTEP__V35*/ meltfptr[34] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

  /*_.IF___V34*/ meltfptr[32] = NULL;;
      }
    ;
    MELT_LOCATION ("warmelt-normatch.melt:6892:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^cond */
    /*cond */ if ( /*_.SFLAG__V31*/ meltfptr[30])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  /*^checksignal */
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^apply */
	  /*apply */
	  {
	    /*_.SCANFLAG__V37*/ meltfptr[36] =
	      melt_apply ((meltclosure_ptr_t)
			  (( /*~SCANFLAG */ meltfclos->tabval[2])),
			  (melt_ptr_t) ( /*_.SFLAG__V31*/ meltfptr[30]), (""),
			  (union meltparam_un *) 0, "",
			  (union meltparam_un *) 0);
	  }
	  ;
	  /*_.IF___V36*/ meltfptr[34] = /*_.SCANFLAG__V37*/ meltfptr[36];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-normatch.melt:6892:/ clear");
	     /*clear *//*_.SCANFLAG__V37*/ meltfptr[36] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

  /*_.IF___V36*/ meltfptr[34] = NULL;;
      }
    ;
    /*^compute */
    /*_.LET___V28*/ meltfptr[27] = /*_.IF___V36*/ meltfptr[34];;

    MELT_LOCATION ("warmelt-normatch.melt:6884:/ clear");
	   /*clear *//*_.STHEN__V29*/ meltfptr[28] = 0;
    /*^clear */
	   /*clear *//*_.SELSE__V30*/ meltfptr[29] = 0;
    /*^clear */
	   /*clear *//*_.SFLAG__V31*/ meltfptr[30] = 0;
    /*^clear */
	   /*clear *//*_.IF___V32*/ meltfptr[31] = 0;
    /*^clear */
	   /*clear *//*_.IF___V34*/ meltfptr[32] = 0;
    /*^clear */
	   /*clear *//*_.IF___V36*/ meltfptr[34] = 0;
    /*_.LET___V18*/ meltfptr[11] = /*_.LET___V28*/ meltfptr[27];;

    MELT_LOCATION ("warmelt-normatch.melt:6868:/ clear");
	   /*clear *//*_.NAMBUF__V19*/ meltfptr[12] = 0;
    /*^clear */
	   /*clear *//*_#MAPCNT__L6*/ meltfnum[1] = 0;
    /*^clear */
	   /*clear *//*_.DISCRIM__V20*/ meltfptr[13] = 0;
    /*^clear */
	   /*clear *//*_.DISNAM__V21*/ meltfptr[9] = 0;
    /*^clear */
	   /*clear *//*_.SDATA__V22*/ meltfptr[10] = 0;
    /*^clear */
	   /*clear *//*_#I__L7*/ meltfnum[0] = 0;
    /*^clear */
	   /*clear *//*_.STRBUF2STRING__V23*/ meltfptr[22] = 0;
    /*^clear */
	   /*clear *//*_.IF___V24*/ meltfptr[23] = 0;
    /*^clear */
	   /*clear *//*_.SCAN_STEP_DATA__V26*/ meltfptr[24] = 0;
    /*^clear */
	   /*clear *//*_.SCAN_STEP_FLAG__V27*/ meltfptr[26] = 0;
    /*^clear */
	   /*clear *//*_.LET___V28*/ meltfptr[27] = 0;
    MELT_LOCATION ("warmelt-normatch.melt:6859:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.LET___V18*/ meltfptr[11];;

    {
      MELT_LOCATION ("warmelt-normatch.melt:6859:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V3*/ meltfptr[2] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V7*/ meltfptr[3] = 0;
    /*^clear */
	   /*clear *//*_.LET___V9*/ meltfptr[4] = 0;
    /*^clear */
	   /*clear *//*_.LET___V18*/ meltfptr[11] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("LAMBDA_", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_137_warmelt_normatch_LAMBDA___31___melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_137_warmelt_normatch_LAMBDA___31__ */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_138_warmelt_normatch_LAMBDA___32__ (meltclosure_ptr_t meltclosp_,
					     melt_ptr_t meltfirstargp_,
					     const melt_argdescr_cell_t
					     meltxargdescr_[],
					     union meltparam_un *
					     meltxargtab_,
					     const melt_argdescr_cell_t
					     meltxresdescr_[],
					     union meltparam_un *
					     meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_138_warmelt_normatch_LAMBDA___32___melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_138_warmelt_normatch_LAMBDA___32___st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 17
    melt_ptr_t mcfr_varptr[17];
#define MELTFRAM_NBVARNUM 5
    long mcfr_varnum[5];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_138_warmelt_normatch_LAMBDA___32__ is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_138_warmelt_normatch_LAMBDA___32___st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 17; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_138_warmelt_normatch_LAMBDA___32__ nbval 17*/
  meltfram__.mcfr_nbvar = 17 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("LAMBDA_", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-normatch.melt:6895:/ getarg");
 /*_.FLAG__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-normatch.melt:6896:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L1*/ meltfnum[0] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-normatch.melt:6896:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L1*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-normatch.melt:6896:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-normatch.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 6896;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "mg_altdraw_graphviz.scanflag flag=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.FLAG__V2*/ meltfptr[1];
	      /*_.MELT_DEBUG_FUN__V5*/ meltfptr[4] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V4*/ meltfptr[3] = /*_.MELT_DEBUG_FUN__V5*/ meltfptr[4];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-normatch.melt:6896:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V5*/ meltfptr[4] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V4*/ meltfptr[3] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-normatch.melt:6896:/ quasiblock");


      /*_.PROGN___V6*/ meltfptr[4] = /*_.IF___V4*/ meltfptr[3];;
      /*^compute */
      /*_.IFCPP___V3*/ meltfptr[2] = /*_.PROGN___V6*/ meltfptr[4];;
      /*epilog */

      MELT_LOCATION ("warmelt-normatch.melt:6896:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L1*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IF___V4*/ meltfptr[3] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V6*/ meltfptr[4] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V3*/ meltfptr[2] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-normatch.melt:6897:/ quasiblock");


    /*^cond */
    /*cond */ if (
		   /*ifisa */
		   melt_is_instance_of ((melt_ptr_t)
					( /*_.FLAG__V2*/ meltfptr[1]),
					(melt_ptr_t) (( /*!CLASS_MATCH_FLAG */
						       meltfrout->tabval[1])))
      )				/*then */
      {
	/*^cond.then */
	/*^getslot */
	{
	  melt_ptr_t slot = NULL, obj = NULL;
	  obj = (melt_ptr_t) ( /*_.FLAG__V2*/ meltfptr[1]) /*=obj*/ ;
	  melt_object_get_field (slot, obj, 5, "MFLAG_SETSTEP");
   /*_.FLSTEP__V8*/ meltfptr[4] = slot;
	};
	;
      }
    else
      {				/*^cond.else */

  /*_.FLSTEP__V8*/ meltfptr[4] = NULL;;
      }
    ;
    MELT_LOCATION ("warmelt-normatch.melt:6899:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^cond */
    /*cond */ if ( /*_.FLSTEP__V8*/ meltfptr[4])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{


#if MELT_HAVE_DEBUG
	  MELT_LOCATION ("warmelt-normatch.melt:6901:/ cppif.then");
	  /*^block */
	  /*anyblock */
	  {


	    {
	      /*^locexp */
	      /*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	      melt_dbgcounter++;
#endif
	      ;
	    }
	    ;
	    /*^checksignal */
	    MELT_CHECK_SIGNAL ();
	    ;
     /*_#MELT_NEED_DBG__L3*/ meltfnum[1] =
	      /*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	      ( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	      0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	      ;;
	    MELT_LOCATION ("warmelt-normatch.melt:6901:/ cond");
	    /*cond */ if ( /*_#MELT_NEED_DBG__L3*/ meltfnum[1])	/*then */
	      {
		/*^cond.then */
		/*^block */
		/*anyblock */
		{

       /*_#THE_MELTCALLCOUNT__L4*/ meltfnum[0] =
#ifdef meltcallcount
		    meltcallcount	/* the_meltcallcount */
#else
		    0L
#endif /* meltcallcount the_meltcallcount */
		    ;;
		  MELT_LOCATION ("warmelt-normatch.melt:6901:/ checksignal");
		  MELT_CHECK_SIGNAL ();
		  ;
		  /*^apply */
		  /*apply */
		  {
		    union meltparam_un argtab[5];
		    memset (&argtab, 0, sizeof (argtab));
		    /*^apply.arg */
		    argtab[0].meltbp_long =
		      /*_#THE_MELTCALLCOUNT__L4*/ meltfnum[0];
		    /*^apply.arg */
		    argtab[1].meltbp_cstring = "warmelt-normatch.melt";
		    /*^apply.arg */
		    argtab[2].meltbp_long = 6901;
		    /*^apply.arg */
		    argtab[3].meltbp_cstring =
		      "mg_altdraw_graphviz.scanflag flstep";
		    /*^apply.arg */
		    argtab[4].meltbp_aptr =
		      (melt_ptr_t *) & /*_.FLSTEP__V8*/ meltfptr[4];
		    /*_.MELT_DEBUG_FUN__V12*/ meltfptr[11] =
		      melt_apply ((meltclosure_ptr_t)
				  (( /*!MELT_DEBUG_FUN */ meltfrout->
				    tabval[0])),
				  (melt_ptr_t) (( /*nil */ NULL)),
				  (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_PTR ""), argtab, "",
				  (union meltparam_un *) 0);
		  }
		  ;
		  /*_.IF___V11*/ meltfptr[10] =
		    /*_.MELT_DEBUG_FUN__V12*/ meltfptr[11];;
		  /*epilog */

		  MELT_LOCATION ("warmelt-normatch.melt:6901:/ clear");
		 /*clear *//*_#THE_MELTCALLCOUNT__L4*/ meltfnum[0] = 0;
		  /*^clear */
		 /*clear *//*_.MELT_DEBUG_FUN__V12*/ meltfptr[11] = 0;
		}
		;
	      }
	    else
	      {			/*^cond.else */

      /*_.IF___V11*/ meltfptr[10] = NULL;;
	      }
	    ;
	    MELT_LOCATION ("warmelt-normatch.melt:6901:/ quasiblock");


	    /*_.PROGN___V13*/ meltfptr[11] = /*_.IF___V11*/ meltfptr[10];;
	    /*^compute */
	    /*_.IFCPP___V10*/ meltfptr[9] = /*_.PROGN___V13*/ meltfptr[11];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-normatch.melt:6901:/ clear");
	       /*clear *//*_#MELT_NEED_DBG__L3*/ meltfnum[1] = 0;
	    /*^clear */
	       /*clear *//*_.IF___V11*/ meltfptr[10] = 0;
	    /*^clear */
	       /*clear *//*_.PROGN___V13*/ meltfptr[11] = 0;
	  }

#else /*MELT_HAVE_DEBUG */
	  /*^cppif.else */
	  /*_.IFCPP___V10*/ meltfptr[9] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
	  ;

#if MELT_HAVE_DEBUG
	  MELT_LOCATION ("warmelt-normatch.melt:6902:/ cppif.then");
	  /*^block */
	  /*anyblock */
	  {

	    /*^checksignal */
	    MELT_CHECK_SIGNAL ();
	    ;
     /*_#IS_A__L5*/ meltfnum[0] =
	      melt_is_instance_of ((melt_ptr_t)
				   ( /*_.FLSTEP__V8*/ meltfptr[4]),
				   (melt_ptr_t) (( /*!CLASS_MATCH_STEP */
						  meltfrout->tabval[2])));;
	    MELT_LOCATION ("warmelt-normatch.melt:6902:/ cond");
	    /*cond */ if ( /*_#IS_A__L5*/ meltfnum[0])	/*then */
	      {
		/*^cond.then */
		/*_.IFELSE___V15*/ meltfptr[11] = ( /*nil */ NULL);;
	      }
	    else
	      {
		MELT_LOCATION ("warmelt-normatch.melt:6902:/ cond.else");

		/*^block */
		/*anyblock */
		{




		  {
		    /*^locexp */
		    melt_assert_failed (("check flstep"),
					("warmelt-normatch.melt")
					? ("warmelt-normatch.melt") :
					__FILE__, (6902) ? (6902) : __LINE__,
					__FUNCTION__);
		    ;
		  }
		  ;
		 /*clear *//*_.IFELSE___V15*/ meltfptr[11] = 0;
		  /*epilog */
		}
		;
	      }
	    ;
	    /*^compute */
	    /*_.IFCPP___V14*/ meltfptr[10] = /*_.IFELSE___V15*/ meltfptr[11];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-normatch.melt:6902:/ clear");
	       /*clear *//*_#IS_A__L5*/ meltfnum[0] = 0;
	    /*^clear */
	       /*clear *//*_.IFELSE___V15*/ meltfptr[11] = 0;
	  }

#else /*MELT_HAVE_DEBUG */
	  /*^cppif.else */
	  /*_.IFCPP___V14*/ meltfptr[10] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
	  ;
	  MELT_LOCATION ("warmelt-normatch.melt:6903:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^apply */
	  /*apply */
	  {
	    /*_.SCANSTEP__V16*/ meltfptr[11] =
	      melt_apply ((meltclosure_ptr_t)
			  (( /*~SCANSTEP */ meltfclos->tabval[0])),
			  (melt_ptr_t) ( /*_.FLSTEP__V8*/ meltfptr[4]), (""),
			  (union meltparam_un *) 0, "",
			  (union meltparam_un *) 0);
	  }
	  ;
	  MELT_LOCATION ("warmelt-normatch.melt:6900:/ quasiblock");


	  /*_.PROGN___V17*/ meltfptr[16] = /*_.SCANSTEP__V16*/ meltfptr[11];;
	  /*^compute */
	  /*_.IF___V9*/ meltfptr[8] = /*_.PROGN___V17*/ meltfptr[16];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-normatch.melt:6899:/ clear");
	     /*clear *//*_.IFCPP___V10*/ meltfptr[9] = 0;
	  /*^clear */
	     /*clear *//*_.IFCPP___V14*/ meltfptr[10] = 0;
	  /*^clear */
	     /*clear *//*_.SCANSTEP__V16*/ meltfptr[11] = 0;
	  /*^clear */
	     /*clear *//*_.PROGN___V17*/ meltfptr[16] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

  /*_.IF___V9*/ meltfptr[8] = NULL;;
      }
    ;
    /*^compute */
    /*_.LET___V7*/ meltfptr[3] = /*_.IF___V9*/ meltfptr[8];;

    MELT_LOCATION ("warmelt-normatch.melt:6897:/ clear");
	   /*clear *//*_.FLSTEP__V8*/ meltfptr[4] = 0;
    /*^clear */
	   /*clear *//*_.IF___V9*/ meltfptr[8] = 0;
    MELT_LOCATION ("warmelt-normatch.melt:6895:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.LET___V7*/ meltfptr[3];;

    {
      MELT_LOCATION ("warmelt-normatch.melt:6895:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V3*/ meltfptr[2] = 0;
    /*^clear */
	   /*clear *//*_.LET___V7*/ meltfptr[3] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("LAMBDA_", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_138_warmelt_normatch_LAMBDA___32___melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_138_warmelt_normatch_LAMBDA___32__ */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_139_warmelt_normatch_LAMBDA___33__ (meltclosure_ptr_t meltclosp_,
					     melt_ptr_t meltfirstargp_,
					     const melt_argdescr_cell_t
					     meltxargdescr_[],
					     union meltparam_un *
					     meltxargtab_,
					     const melt_argdescr_cell_t
					     meltxresdescr_[],
					     union meltparam_un *
					     meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_139_warmelt_normatch_LAMBDA___33___melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_139_warmelt_normatch_LAMBDA___33___st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 9
    melt_ptr_t mcfr_varptr[9];
#define MELTFRAM_NBVARNUM 2
    long mcfr_varnum[2];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_139_warmelt_normatch_LAMBDA___33__ is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_139_warmelt_normatch_LAMBDA___33___st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 9; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_139_warmelt_normatch_LAMBDA___33__ nbval 9*/
  meltfram__.mcfr_nbvar = 9 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("LAMBDA_", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-normatch.melt:6935:/ getarg");
 /*_.D1__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.D2__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.D2__V3*/ meltfptr[2])) != NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {

    MELT_LOCATION ("warmelt-normatch.melt:6936:/ quasiblock");


 /*_.SN1__V5*/ meltfptr[4] =
      /*mapobject_get */
      melt_get_mapobjects ((meltmapobjects_ptr_t)
			   (( /*~MAPDATA */ meltfclos->tabval[0])),
			   (meltobject_ptr_t) ( /*_.D1__V2*/ meltfptr[1]));;
    /*^compute */
 /*_.SN2__V6*/ meltfptr[5] =
      /*mapobject_get */
      melt_get_mapobjects ((meltmapobjects_ptr_t)
			   (( /*~MAPDATA */ meltfclos->tabval[0])),
			   (meltobject_ptr_t) ( /*_.D2__V3*/ meltfptr[2]));;
    MELT_LOCATION ("warmelt-normatch.melt:6941:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#STRING___L1*/ meltfnum[0] =
      melt_string_less ((melt_ptr_t) ( /*_.SN1__V5*/ meltfptr[4]),
			(melt_ptr_t) ( /*_.SN2__V6*/ meltfptr[5]));;
    MELT_LOCATION ("warmelt-normatch.melt:6941:/ cond");
    /*cond */ if ( /*_#STRING___L1*/ meltfnum[0])	/*then */
      {
	/*^cond.then */
	/*_.IFELSE___V7*/ meltfptr[6] =
	  ( /*!konst_0 */ meltfrout->tabval[0]);;
      }
    else
      {
	MELT_LOCATION ("warmelt-normatch.melt:6941:/ cond.else");

	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-normatch.melt:6943:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
   /*_#STRING___L2*/ meltfnum[1] =
	    melt_string_less ((melt_ptr_t) ( /*_.SN2__V6*/ meltfptr[5]),
			      (melt_ptr_t) ( /*_.SN1__V5*/ meltfptr[4]));;
	  MELT_LOCATION ("warmelt-normatch.melt:6943:/ cond");
	  /*cond */ if ( /*_#STRING___L2*/ meltfnum[1])	/*then */
	    {
	      /*^cond.then */
	      /*_.IFELSE___V8*/ meltfptr[7] =
		( /*!konst_1 */ meltfrout->tabval[1]);;
	    }
	  else
	    {
	      MELT_LOCATION ("warmelt-normatch.melt:6943:/ cond.else");

	      /*^block */
	      /*anyblock */
	      {

		MELT_LOCATION ("warmelt-normatch.melt:6945:/ quasiblock");


		/*_.PROGN___V9*/ meltfptr[8] =
		  ( /*!konst_2 */ meltfrout->tabval[2]);;
		/*^compute */
		/*_.IFELSE___V8*/ meltfptr[7] = /*_.PROGN___V9*/ meltfptr[8];;
		/*epilog */

		MELT_LOCATION ("warmelt-normatch.melt:6943:/ clear");
	       /*clear *//*_.PROGN___V9*/ meltfptr[8] = 0;
	      }
	      ;
	    }
	  ;
	  /*_.IFELSE___V7*/ meltfptr[6] = /*_.IFELSE___V8*/ meltfptr[7];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-normatch.melt:6941:/ clear");
	     /*clear *//*_#STRING___L2*/ meltfnum[1] = 0;
	  /*^clear */
	     /*clear *//*_.IFELSE___V8*/ meltfptr[7] = 0;
	}
	;
      }
    ;
    /*_.LET___V4*/ meltfptr[3] = /*_.IFELSE___V7*/ meltfptr[6];;

    MELT_LOCATION ("warmelt-normatch.melt:6936:/ clear");
	   /*clear *//*_.SN1__V5*/ meltfptr[4] = 0;
    /*^clear */
	   /*clear *//*_.SN2__V6*/ meltfptr[5] = 0;
    /*^clear */
	   /*clear *//*_#STRING___L1*/ meltfnum[0] = 0;
    /*^clear */
	   /*clear *//*_.IFELSE___V7*/ meltfptr[6] = 0;
    MELT_LOCATION ("warmelt-normatch.melt:6935:/ quasiblock");


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.LET___V4*/ meltfptr[3];;

    {
      MELT_LOCATION ("warmelt-normatch.melt:6935:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.LET___V4*/ meltfptr[3] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("LAMBDA_", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_139_warmelt_normatch_LAMBDA___33___melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_139_warmelt_normatch_LAMBDA___33__ */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_140_warmelt_normatch_LAMBDA___34__ (meltclosure_ptr_t meltclosp_,
					     melt_ptr_t meltfirstargp_,
					     const melt_argdescr_cell_t
					     meltxargdescr_[],
					     union meltparam_un *
					     meltxargtab_,
					     const melt_argdescr_cell_t
					     meltxresdescr_[],
					     union meltparam_un *
					     meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_140_warmelt_normatch_LAMBDA___34___melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_140_warmelt_normatch_LAMBDA___34___st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 9
    melt_ptr_t mcfr_varptr[9];
#define MELTFRAM_NBVARNUM 2
    long mcfr_varnum[2];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_140_warmelt_normatch_LAMBDA___34__ is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_140_warmelt_normatch_LAMBDA___34___st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 9; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_140_warmelt_normatch_LAMBDA___34__ nbval 9*/
  meltfram__.mcfr_nbvar = 9 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("LAMBDA_", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-normatch.melt:6954:/ getarg");
 /*_.S1__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.S2__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.S2__V3*/ meltfptr[2])) != NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {

    MELT_LOCATION ("warmelt-normatch.melt:6955:/ quasiblock");


 /*_.SN1__V5*/ meltfptr[4] =
      /*mapobject_get */
      melt_get_mapobjects ((meltmapobjects_ptr_t)
			   (( /*~MAPSTEP */ meltfclos->tabval[0])),
			   (meltobject_ptr_t) ( /*_.S1__V2*/ meltfptr[1]));;
    /*^compute */
 /*_.SN2__V6*/ meltfptr[5] =
      /*mapobject_get */
      melt_get_mapobjects ((meltmapobjects_ptr_t)
			   (( /*~MAPSTEP */ meltfclos->tabval[0])),
			   (meltobject_ptr_t) ( /*_.S2__V3*/ meltfptr[2]));;
    MELT_LOCATION ("warmelt-normatch.melt:6960:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#STRING___L1*/ meltfnum[0] =
      melt_string_less ((melt_ptr_t) ( /*_.SN1__V5*/ meltfptr[4]),
			(melt_ptr_t) ( /*_.SN2__V6*/ meltfptr[5]));;
    MELT_LOCATION ("warmelt-normatch.melt:6960:/ cond");
    /*cond */ if ( /*_#STRING___L1*/ meltfnum[0])	/*then */
      {
	/*^cond.then */
	/*_.IFELSE___V7*/ meltfptr[6] =
	  ( /*!konst_0 */ meltfrout->tabval[0]);;
      }
    else
      {
	MELT_LOCATION ("warmelt-normatch.melt:6960:/ cond.else");

	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-normatch.melt:6962:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
   /*_#STRING___L2*/ meltfnum[1] =
	    melt_string_less ((melt_ptr_t) ( /*_.SN2__V6*/ meltfptr[5]),
			      (melt_ptr_t) ( /*_.SN1__V5*/ meltfptr[4]));;
	  MELT_LOCATION ("warmelt-normatch.melt:6962:/ cond");
	  /*cond */ if ( /*_#STRING___L2*/ meltfnum[1])	/*then */
	    {
	      /*^cond.then */
	      /*_.IFELSE___V8*/ meltfptr[7] =
		( /*!konst_1 */ meltfrout->tabval[1]);;
	    }
	  else
	    {
	      MELT_LOCATION ("warmelt-normatch.melt:6962:/ cond.else");

	      /*^block */
	      /*anyblock */
	      {

		MELT_LOCATION ("warmelt-normatch.melt:6964:/ quasiblock");


		/*_.PROGN___V9*/ meltfptr[8] =
		  ( /*!konst_2 */ meltfrout->tabval[2]);;
		/*^compute */
		/*_.IFELSE___V8*/ meltfptr[7] = /*_.PROGN___V9*/ meltfptr[8];;
		/*epilog */

		MELT_LOCATION ("warmelt-normatch.melt:6962:/ clear");
	       /*clear *//*_.PROGN___V9*/ meltfptr[8] = 0;
	      }
	      ;
	    }
	  ;
	  /*_.IFELSE___V7*/ meltfptr[6] = /*_.IFELSE___V8*/ meltfptr[7];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-normatch.melt:6960:/ clear");
	     /*clear *//*_#STRING___L2*/ meltfnum[1] = 0;
	  /*^clear */
	     /*clear *//*_.IFELSE___V8*/ meltfptr[7] = 0;
	}
	;
      }
    ;
    /*_.LET___V4*/ meltfptr[3] = /*_.IFELSE___V7*/ meltfptr[6];;

    MELT_LOCATION ("warmelt-normatch.melt:6955:/ clear");
	   /*clear *//*_.SN1__V5*/ meltfptr[4] = 0;
    /*^clear */
	   /*clear *//*_.SN2__V6*/ meltfptr[5] = 0;
    /*^clear */
	   /*clear *//*_#STRING___L1*/ meltfnum[0] = 0;
    /*^clear */
	   /*clear *//*_.IFELSE___V7*/ meltfptr[6] = 0;
    MELT_LOCATION ("warmelt-normatch.melt:6954:/ quasiblock");


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.LET___V4*/ meltfptr[3];;

    {
      MELT_LOCATION ("warmelt-normatch.melt:6954:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.LET___V4*/ meltfptr[3] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("LAMBDA_", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_140_warmelt_normatch_LAMBDA___34___melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_140_warmelt_normatch_LAMBDA___34__ */



/**** end of warmelt-normatch+05.c ****/
