/* GCC MELT GENERATED FILE warmelt-outobj+01.c - DO NOT EDIT */
/* secondary MELT generated C file of rank #1 */
#include "melt-run.h"


/* used hash from melt-run.h when compiling this file: */
MELT_EXTERN const char meltrun_used_md5_melt_f1[] =
  MELT_RUN_HASHMD5 /* from melt-run.h */ ;


/**** warmelt-outobj+01.c declarations ****/


/***************************************************
***
    Copyright (C) 2008 - 2012 Free Software Foundation, Inc.
    Contributed by Basile Starynkevitch <basile@starynkevitch.net>

    This file is part of GCC.

    GCC is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3, or (at your option)
    any later version.

    GCC is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with GCC; see the file COPYING3.  If not see
    <http://www.gnu.org/licenses/>.
***
****************************************************/

/* ordinary MELT module */
#define MELT_HAS_INITIAL_ENVIRONMENT 1	/*usual */

struct melt_callframe_st;	/*defined in melt-runtime.h */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_1_warmelt_outobj_GET_CODE_BUFFER_LIMIT (meltclosure_ptr_t meltclosp_,
						 melt_ptr_t meltfirstargp_,
						 const melt_argdescr_cell_t
						 meltxargdescr_[],
						 union meltparam_un
						 *meltxargtab_,
						 const melt_argdescr_cell_t
						 meltxresdescr_[],
						 union meltparam_un
						 *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_2_warmelt_outobj_PUT_CODE_BUFFER_LIMIT (meltclosure_ptr_t meltclosp_,
						 melt_ptr_t meltfirstargp_,
						 const melt_argdescr_cell_t
						 meltxargdescr_[],
						 union meltparam_un
						 *meltxargtab_,
						 const melt_argdescr_cell_t
						 meltxresdescr_[],
						 union meltparam_un
						 *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_3_warmelt_outobj_CODE_BUFFER_LIMIT_OPTSET (meltclosure_ptr_t
						    meltclosp_,
						    melt_ptr_t meltfirstargp_,
						    const melt_argdescr_cell_t
						    meltxargdescr_[],
						    union meltparam_un
						    *meltxargtab_,
						    const melt_argdescr_cell_t
						    meltxresdescr_[],
						    union meltparam_un
						    *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_4_warmelt_outobj_OUTDECLINIT_ROOT (meltclosure_ptr_t meltclosp_,
					    melt_ptr_t meltfirstargp_,
					    const melt_argdescr_cell_t
					    meltxargdescr_[],
					    union meltparam_un *meltxargtab_,
					    const melt_argdescr_cell_t
					    meltxresdescr_[],
					    union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_5_warmelt_outobj_OUTPUCOD_OBJINIELEM (meltclosure_ptr_t meltclosp_,
					       melt_ptr_t meltfirstargp_,
					       const melt_argdescr_cell_t
					       meltxargdescr_[],
					       union meltparam_un
					       *meltxargtab_,
					       const melt_argdescr_cell_t
					       meltxresdescr_[],
					       union meltparam_un
					       *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_6_warmelt_outobj_OUTCINITFILL_ROOT (meltclosure_ptr_t meltclosp_,
					     melt_ptr_t meltfirstargp_,
					     const melt_argdescr_cell_t
					     meltxargdescr_[],
					     union meltparam_un *meltxargtab_,
					     const melt_argdescr_cell_t
					     meltxresdescr_[],
					     union meltparam_un
					     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_7_warmelt_outobj_OUTCINITPREDEF_ROOT (meltclosure_ptr_t meltclosp_,
					       melt_ptr_t meltfirstargp_,
					       const melt_argdescr_cell_t
					       meltxargdescr_[],
					       union meltparam_un
					       *meltxargtab_,
					       const melt_argdescr_cell_t
					       meltxresdescr_[],
					       union meltparam_un
					       *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_8_warmelt_outobj_OUTPUT_PREDEF (meltclosure_ptr_t meltclosp_,
					 melt_ptr_t meltfirstargp_,
					 const melt_argdescr_cell_t
					 meltxargdescr_[],
					 union meltparam_un *meltxargtab_,
					 const melt_argdescr_cell_t
					 meltxresdescr_[],
					 union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_9_warmelt_outobj_OUTPUCOD_PREDEF (meltclosure_ptr_t meltclosp_,
					   melt_ptr_t meltfirstargp_,
					   const melt_argdescr_cell_t
					   meltxargdescr_[],
					   union meltparam_un *meltxargtab_,
					   const melt_argdescr_cell_t
					   meltxresdescr_[],
					   union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_10_warmelt_outobj_OUTPUCOD_NIL (meltclosure_ptr_t meltclosp_,
					 melt_ptr_t meltfirstargp_,
					 const melt_argdescr_cell_t
					 meltxargdescr_[],
					 union meltparam_un *meltxargtab_,
					 const melt_argdescr_cell_t
					 meltxresdescr_[],
					 union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_11_warmelt_outobj_OUTPUCOD_OBJEXPANDPUREVAL (meltclosure_ptr_t
						      meltclosp_,
						      melt_ptr_t
						      meltfirstargp_,
						      const
						      melt_argdescr_cell_t
						      meltxargdescr_[],
						      union meltparam_un
						      *meltxargtab_,
						      const
						      melt_argdescr_cell_t
						      meltxresdescr_[],
						      union meltparam_un
						      *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_12_warmelt_outobj_OUTDECLINIT_OBJINITOBJECT (meltclosure_ptr_t
						      meltclosp_,
						      melt_ptr_t
						      meltfirstargp_,
						      const
						      melt_argdescr_cell_t
						      meltxargdescr_[],
						      union meltparam_un
						      *meltxargtab_,
						      const
						      melt_argdescr_cell_t
						      meltxresdescr_[],
						      union meltparam_un
						      *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_13_warmelt_outobj_OUTCINITFILL_OBJINITOBJECT (meltclosure_ptr_t
						       meltclosp_,
						       melt_ptr_t
						       meltfirstargp_,
						       const
						       melt_argdescr_cell_t
						       meltxargdescr_[],
						       union meltparam_un
						       *meltxargtab_,
						       const
						       melt_argdescr_cell_t
						       meltxresdescr_[],
						       union meltparam_un
						       *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_14_warmelt_outobj_OUTCINITPREDEF_OBJINITOBJECT (meltclosure_ptr_t
							 meltclosp_,
							 melt_ptr_t
							 meltfirstargp_,
							 const
							 melt_argdescr_cell_t
							 meltxargdescr_[],
							 union meltparam_un
							 *meltxargtab_,
							 const
							 melt_argdescr_cell_t
							 meltxresdescr_[],
							 union meltparam_un
							 *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_15_warmelt_outobj_OUTDECLINIT_OBJINITMULTIPLE (meltclosure_ptr_t
							meltclosp_,
							melt_ptr_t
							meltfirstargp_,
							const
							melt_argdescr_cell_t
							meltxargdescr_[],
							union meltparam_un
							*meltxargtab_,
							const
							melt_argdescr_cell_t
							meltxresdescr_[],
							union meltparam_un
							*meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_16_warmelt_outobj_OUTCINITFILL_OBJINITMULTIPLE (meltclosure_ptr_t
							 meltclosp_,
							 melt_ptr_t
							 meltfirstargp_,
							 const
							 melt_argdescr_cell_t
							 meltxargdescr_[],
							 union meltparam_un
							 *meltxargtab_,
							 const
							 melt_argdescr_cell_t
							 meltxresdescr_[],
							 union meltparam_un
							 *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_17_warmelt_outobj_OUTDECLINIT_OBJINITCLOSURE (meltclosure_ptr_t
						       meltclosp_,
						       melt_ptr_t
						       meltfirstargp_,
						       const
						       melt_argdescr_cell_t
						       meltxargdescr_[],
						       union meltparam_un
						       *meltxargtab_,
						       const
						       melt_argdescr_cell_t
						       meltxresdescr_[],
						       union meltparam_un
						       *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_18_warmelt_outobj_OUTCINITFILL_OBJINITCLOSURE (meltclosure_ptr_t
							meltclosp_,
							melt_ptr_t
							meltfirstargp_,
							const
							melt_argdescr_cell_t
							meltxargdescr_[],
							union meltparam_un
							*meltxargtab_,
							const
							melt_argdescr_cell_t
							meltxresdescr_[],
							union meltparam_un
							*meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_19_warmelt_outobj_OUTDECLINIT_OBJINITROUTINE (meltclosure_ptr_t
						       meltclosp_,
						       melt_ptr_t
						       meltfirstargp_,
						       const
						       melt_argdescr_cell_t
						       meltxargdescr_[],
						       union meltparam_un
						       *meltxargtab_,
						       const
						       melt_argdescr_cell_t
						       meltxresdescr_[],
						       union meltparam_un
						       *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_20_warmelt_outobj_OUTCINITFILL_OBJINITROUTINE (meltclosure_ptr_t
							meltclosp_,
							melt_ptr_t
							meltfirstargp_,
							const
							melt_argdescr_cell_t
							meltxargdescr_[],
							union meltparam_un
							*meltxargtab_,
							const
							melt_argdescr_cell_t
							meltxresdescr_[],
							union meltparam_un
							*meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_21_warmelt_outobj_OUTDECLINIT_OBJINITSTRING (meltclosure_ptr_t
						      meltclosp_,
						      melt_ptr_t
						      meltfirstargp_,
						      const
						      melt_argdescr_cell_t
						      meltxargdescr_[],
						      union meltparam_un
						      *meltxargtab_,
						      const
						      melt_argdescr_cell_t
						      meltxresdescr_[],
						      union meltparam_un
						      *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_22_warmelt_outobj_OUTCINITFILL_OBJINITSTRING (meltclosure_ptr_t
						       meltclosp_,
						       melt_ptr_t
						       meltfirstargp_,
						       const
						       melt_argdescr_cell_t
						       meltxargdescr_[],
						       union meltparam_un
						       *meltxargtab_,
						       const
						       melt_argdescr_cell_t
						       meltxresdescr_[],
						       union meltparam_un
						       *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_23_warmelt_outobj_OUTDECLINIT_OBJINITBOXEDINTEGER (meltclosure_ptr_t
							    meltclosp_,
							    melt_ptr_t
							    meltfirstargp_,
							    const
							    melt_argdescr_cell_t
							    meltxargdescr_[],
							    union meltparam_un
							    *meltxargtab_,
							    const
							    melt_argdescr_cell_t
							    meltxresdescr_[],
							    union meltparam_un
							    *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_24_warmelt_outobj_OUTCINITFILL_OBJINITBOXEDINTEGER (meltclosure_ptr_t
							     meltclosp_,
							     melt_ptr_t
							     meltfirstargp_,
							     const
							     melt_argdescr_cell_t
							     meltxargdescr_[],
							     union
							     meltparam_un
							     *meltxargtab_,
							     const
							     melt_argdescr_cell_t
							     meltxresdescr_[],
							     union
							     meltparam_un
							     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_25_warmelt_outobj_OUTDECLINIT_OBJINITPAIR (meltclosure_ptr_t
						    meltclosp_,
						    melt_ptr_t meltfirstargp_,
						    const melt_argdescr_cell_t
						    meltxargdescr_[],
						    union meltparam_un
						    *meltxargtab_,
						    const melt_argdescr_cell_t
						    meltxresdescr_[],
						    union meltparam_un
						    *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_26_warmelt_outobj_OUTCINITFILL_OBJINITPAIR (meltclosure_ptr_t
						     meltclosp_,
						     melt_ptr_t
						     meltfirstargp_,
						     const
						     melt_argdescr_cell_t
						     meltxargdescr_[],
						     union meltparam_un
						     *meltxargtab_,
						     const
						     melt_argdescr_cell_t
						     meltxresdescr_[],
						     union meltparam_un
						     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_27_warmelt_outobj_OUTDECLINIT_OBJINITLIST (meltclosure_ptr_t
						    meltclosp_,
						    melt_ptr_t meltfirstargp_,
						    const melt_argdescr_cell_t
						    meltxargdescr_[],
						    union meltparam_un
						    *meltxargtab_,
						    const melt_argdescr_cell_t
						    meltxresdescr_[],
						    union meltparam_un
						    *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_28_warmelt_outobj_OUTCINITFILL_OBJINITLIST (meltclosure_ptr_t
						     meltclosp_,
						     melt_ptr_t
						     meltfirstargp_,
						     const
						     melt_argdescr_cell_t
						     meltxargdescr_[],
						     union meltparam_un
						     *meltxargtab_,
						     const
						     melt_argdescr_cell_t
						     meltxresdescr_[],
						     union meltparam_un
						     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_29_warmelt_outobj_OUTPUCOD_ANYDISCR (meltclosure_ptr_t meltclosp_,
					      melt_ptr_t meltfirstargp_,
					      const melt_argdescr_cell_t
					      meltxargdescr_[],
					      union meltparam_un
					      *meltxargtab_,
					      const melt_argdescr_cell_t
					      meltxresdescr_[],
					      union meltparam_un
					      *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_30_warmelt_outobj_OUTPUCOD_NULL (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_31_warmelt_outobj_OUTPUCOD_CATCHALL_ROOT (meltclosure_ptr_t
						   meltclosp_,
						   melt_ptr_t meltfirstargp_,
						   const melt_argdescr_cell_t
						   meltxargdescr_[],
						   union meltparam_un
						   *meltxargtab_,
						   const melt_argdescr_cell_t
						   meltxresdescr_[],
						   union meltparam_un
						   *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_32_warmelt_outobj_OUTPUT_RAW_LOCATION (meltclosure_ptr_t meltclosp_,
						melt_ptr_t meltfirstargp_,
						const melt_argdescr_cell_t
						meltxargdescr_[],
						union meltparam_un
						*meltxargtab_,
						const melt_argdescr_cell_t
						meltxresdescr_[],
						union meltparam_un
						*meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_33_warmelt_outobj_LINE_AND_FILE_OF_LOCATION (meltclosure_ptr_t
						      meltclosp_,
						      melt_ptr_t
						      meltfirstargp_,
						      const
						      melt_argdescr_cell_t
						      meltxargdescr_[],
						      union meltparam_un
						      *meltxargtab_,
						      const
						      melt_argdescr_cell_t
						      meltxresdescr_[],
						      union meltparam_un
						      *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_34_warmelt_outobj_OUTPUT_LOCATION (meltclosure_ptr_t meltclosp_,
					    melt_ptr_t meltfirstargp_,
					    const melt_argdescr_cell_t
					    meltxargdescr_[],
					    union meltparam_un *meltxargtab_,
					    const melt_argdescr_cell_t
					    meltxresdescr_[],
					    union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_35_warmelt_outobj_OUTPUT_CURFRAME_DECLSTRUCT (meltclosure_ptr_t
						       meltclosp_,
						       melt_ptr_t
						       meltfirstargp_,
						       const
						       melt_argdescr_cell_t
						       meltxargdescr_[],
						       union meltparam_un
						       *meltxargtab_,
						       const
						       melt_argdescr_cell_t
						       meltxresdescr_[],
						       union meltparam_un
						       *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_36_warmelt_outobj_LAMBDA___1__ (meltclosure_ptr_t meltclosp_,
					 melt_ptr_t meltfirstargp_,
					 const melt_argdescr_cell_t
					 meltxargdescr_[],
					 union meltparam_un *meltxargtab_,
					 const melt_argdescr_cell_t
					 meltxresdescr_[],
					 union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_37_warmelt_outobj_OUTPUCOD_MARKER (meltclosure_ptr_t meltclosp_,
					    melt_ptr_t meltfirstargp_,
					    const melt_argdescr_cell_t
					    meltxargdescr_[],
					    union meltparam_un *meltxargtab_,
					    const melt_argdescr_cell_t
					    meltxresdescr_[],
					    union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_38_warmelt_outobj_LAMBDA___2__ (meltclosure_ptr_t meltclosp_,
					 melt_ptr_t meltfirstargp_,
					 const melt_argdescr_cell_t
					 meltxargdescr_[],
					 union meltparam_un *meltxargtab_,
					 const melt_argdescr_cell_t
					 meltxresdescr_[],
					 union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_39_warmelt_outobj_OUTPUT_CURFRAME_DECLSTRUCT_INIT (meltclosure_ptr_t
							    meltclosp_,
							    melt_ptr_t
							    meltfirstargp_,
							    const
							    melt_argdescr_cell_t
							    meltxargdescr_[],
							    union meltparam_un
							    *meltxargtab_,
							    const
							    melt_argdescr_cell_t
							    meltxresdescr_[],
							    union meltparam_un
							    *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_40_warmelt_outobj_OUTPUCOD_PROCROUTINE (meltclosure_ptr_t meltclosp_,
						 melt_ptr_t meltfirstargp_,
						 const melt_argdescr_cell_t
						 meltxargdescr_[],
						 union meltparam_un
						 *meltxargtab_,
						 const melt_argdescr_cell_t
						 meltxresdescr_[],
						 union meltparam_un
						 *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_41_warmelt_outobj_LAMBDA___3__ (meltclosure_ptr_t meltclosp_,
					 melt_ptr_t meltfirstargp_,
					 const melt_argdescr_cell_t
					 meltxargdescr_[],
					 union meltparam_un *meltxargtab_,
					 const melt_argdescr_cell_t
					 meltxresdescr_[],
					 union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_42_warmelt_outobj_OUTPUT_CURFRAME_CDAT_STRUCT (meltclosure_ptr_t
							meltclosp_,
							melt_ptr_t
							meltfirstargp_,
							const
							melt_argdescr_cell_t
							meltxargdescr_[],
							union meltparam_un
							*meltxargtab_,
							const
							melt_argdescr_cell_t
							meltxresdescr_[],
							union meltparam_un
							*meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_43_warmelt_outobj_OUTPUT_CURFRAME_CDAT_FILL (meltclosure_ptr_t
						      meltclosp_,
						      melt_ptr_t
						      meltfirstargp_,
						      const
						      melt_argdescr_cell_t
						      meltxargdescr_[],
						      union meltparam_un
						      *meltxargtab_,
						      const
						      melt_argdescr_cell_t
						      meltxresdescr_[],
						      union meltparam_un
						      *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_44_warmelt_outobj_OUTPUCOD_INITIALMODULEROUTINE (meltclosure_ptr_t
							  meltclosp_,
							  melt_ptr_t
							  meltfirstargp_,
							  const
							  melt_argdescr_cell_t
							  meltxargdescr_[],
							  union meltparam_un
							  *meltxargtab_,
							  const
							  melt_argdescr_cell_t
							  meltxresdescr_[],
							  union meltparam_un
							  *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_45_warmelt_outobj_LAMBDA___4__ (meltclosure_ptr_t meltclosp_,
					 melt_ptr_t meltfirstargp_,
					 const melt_argdescr_cell_t
					 meltxargdescr_[],
					 union meltparam_un *meltxargtab_,
					 const melt_argdescr_cell_t
					 meltxresdescr_[],
					 union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_46_warmelt_outobj_OUTPUCOD_INITIALEXTENSIONROUTINE (meltclosure_ptr_t
							     meltclosp_,
							     melt_ptr_t
							     meltfirstargp_,
							     const
							     melt_argdescr_cell_t
							     meltxargdescr_[],
							     union
							     meltparam_un
							     *meltxargtab_,
							     const
							     melt_argdescr_cell_t
							     meltxresdescr_[],
							     union
							     meltparam_un
							     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_47_warmelt_outobj_LAMBDA___5__ (meltclosure_ptr_t meltclosp_,
					 melt_ptr_t meltfirstargp_,
					 const melt_argdescr_cell_t
					 meltxargdescr_[],
					 union meltparam_un *meltxargtab_,
					 const melt_argdescr_cell_t
					 meltxresdescr_[],
					 union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_48_warmelt_outobj_OUTPUCOD_GETARG (meltclosure_ptr_t meltclosp_,
					    melt_ptr_t meltfirstargp_,
					    const melt_argdescr_cell_t
					    meltxargdescr_[],
					    union meltparam_un *meltxargtab_,
					    const melt_argdescr_cell_t
					    meltxresdescr_[],
					    union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_49_warmelt_outobj_OUTPUCOD_GETARGREST (meltclosure_ptr_t meltclosp_,
						melt_ptr_t meltfirstargp_,
						const melt_argdescr_cell_t
						meltxargdescr_[],
						union meltparam_un
						*meltxargtab_,
						const melt_argdescr_cell_t
						meltxresdescr_[],
						union meltparam_un
						*meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_50_warmelt_outobj_OUTPUCOD_OBJLOCV (meltclosure_ptr_t meltclosp_,
					     melt_ptr_t meltfirstargp_,
					     const melt_argdescr_cell_t
					     meltxargdescr_[],
					     union meltparam_un *meltxargtab_,
					     const melt_argdescr_cell_t
					     meltxresdescr_[],
					     union meltparam_un
					     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_51_warmelt_outobj_OUTPUCOD_OBJCLOCCV (meltclosure_ptr_t meltclosp_,
					       melt_ptr_t meltfirstargp_,
					       const melt_argdescr_cell_t
					       meltxargdescr_[],
					       union meltparam_un
					       *meltxargtab_,
					       const melt_argdescr_cell_t
					       meltxresdescr_[],
					       union meltparam_un
					       *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_52_warmelt_outobj_OUTPUCOD_OBJCONSTV (meltclosure_ptr_t meltclosp_,
					       melt_ptr_t meltfirstargp_,
					       const melt_argdescr_cell_t
					       meltxargdescr_[],
					       union meltparam_un
					       *meltxargtab_,
					       const melt_argdescr_cell_t
					       meltxresdescr_[],
					       union meltparam_un
					       *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_53_warmelt_outobj_OUTPUT_CODE_INSTRUCTIONS_LIST (meltclosure_ptr_t
							  meltclosp_,
							  melt_ptr_t
							  meltfirstargp_,
							  const
							  melt_argdescr_cell_t
							  meltxargdescr_[],
							  union meltparam_un
							  *meltxargtab_,
							  const
							  melt_argdescr_cell_t
							  meltxresdescr_[],
							  union meltparam_un
							  *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_54_warmelt_outobj_OUTPUCOD_OBJCHECKSIGNAL (meltclosure_ptr_t
						    meltclosp_,
						    melt_ptr_t meltfirstargp_,
						    const melt_argdescr_cell_t
						    meltxargdescr_[],
						    union meltparam_un
						    *meltxargtab_,
						    const melt_argdescr_cell_t
						    meltxresdescr_[],
						    union meltparam_un
						    *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_55_warmelt_outobj_OUTPUCOD_OBJANYBLOCK (meltclosure_ptr_t meltclosp_,
						 melt_ptr_t meltfirstargp_,
						 const melt_argdescr_cell_t
						 meltxargdescr_[],
						 union meltparam_un
						 *meltxargtab_,
						 const melt_argdescr_cell_t
						 meltxresdescr_[],
						 union meltparam_un
						 *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_56_warmelt_outobj_OUTPUCOD_OBJMULTIALLOCBLOCK (meltclosure_ptr_t
							meltclosp_,
							melt_ptr_t
							meltfirstargp_,
							const
							melt_argdescr_cell_t
							meltxargdescr_[],
							union meltparam_un
							*meltxargtab_,
							const
							melt_argdescr_cell_t
							meltxresdescr_[],
							union meltparam_un
							*meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_57_warmelt_outobj_OUTPUCOD_OBJCITERBLOCK (meltclosure_ptr_t
						   meltclosp_,
						   melt_ptr_t meltfirstargp_,
						   const melt_argdescr_cell_t
						   meltxargdescr_[],
						   union meltparam_un
						   *meltxargtab_,
						   const melt_argdescr_cell_t
						   meltxresdescr_[],
						   union meltparam_un
						   *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_58_warmelt_outobj_OUTPUCOD_OBJCOMMENTINSTR (meltclosure_ptr_t
						     meltclosp_,
						     melt_ptr_t
						     meltfirstargp_,
						     const
						     melt_argdescr_cell_t
						     meltxargdescr_[],
						     union meltparam_un
						     *meltxargtab_,
						     const
						     melt_argdescr_cell_t
						     meltxresdescr_[],
						     union meltparam_un
						     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_59_warmelt_outobj_OUTPUCOD_OBJCOMMENTEDBLOCK (meltclosure_ptr_t
						       meltclosp_,
						       melt_ptr_t
						       meltfirstargp_,
						       const
						       melt_argdescr_cell_t
						       meltxargdescr_[],
						       union meltparam_un
						       *meltxargtab_,
						       const
						       melt_argdescr_cell_t
						       meltxresdescr_[],
						       union meltparam_un
						       *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_60_warmelt_outobj_OUTPUCOD_OBJLABELINSTR (meltclosure_ptr_t
						   meltclosp_,
						   melt_ptr_t meltfirstargp_,
						   const melt_argdescr_cell_t
						   meltxargdescr_[],
						   union meltparam_un
						   *meltxargtab_,
						   const melt_argdescr_cell_t
						   meltxresdescr_[],
						   union meltparam_un
						   *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_61_warmelt_outobj_OUTPUCOD_OBJGOTOINSTR (meltclosure_ptr_t
						  meltclosp_,
						  melt_ptr_t meltfirstargp_,
						  const melt_argdescr_cell_t
						  meltxargdescr_[],
						  union meltparam_un
						  *meltxargtab_,
						  const melt_argdescr_cell_t
						  meltxresdescr_[],
						  union meltparam_un
						  *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_62_warmelt_outobj_ADD2SBUF_CLONSYM (meltclosure_ptr_t meltclosp_,
					     melt_ptr_t meltfirstargp_,
					     const melt_argdescr_cell_t
					     meltxargdescr_[],
					     union meltparam_un *meltxargtab_,
					     const melt_argdescr_cell_t
					     meltxresdescr_[],
					     union meltparam_un
					     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_63_warmelt_outobj_OUTPUCOD_OBJLOOP (meltclosure_ptr_t meltclosp_,
					     melt_ptr_t meltfirstargp_,
					     const melt_argdescr_cell_t
					     meltxargdescr_[],
					     union meltparam_un *meltxargtab_,
					     const melt_argdescr_cell_t
					     meltxresdescr_[],
					     union meltparam_un
					     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_64_warmelt_outobj_LAMBDA___6__ (meltclosure_ptr_t meltclosp_,
					 melt_ptr_t meltfirstargp_,
					 const melt_argdescr_cell_t
					 meltxargdescr_[],
					 union meltparam_un *meltxargtab_,
					 const melt_argdescr_cell_t
					 meltxresdescr_[],
					 union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_65_warmelt_outobj_LAMBDA___7__ (meltclosure_ptr_t meltclosp_,
					 melt_ptr_t meltfirstargp_,
					 const melt_argdescr_cell_t
					 meltxargdescr_[],
					 union meltparam_un *meltxargtab_,
					 const melt_argdescr_cell_t
					 meltxresdescr_[],
					 union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_66_warmelt_outobj_OUTPUCOD_OBJEXIT (meltclosure_ptr_t meltclosp_,
					     melt_ptr_t meltfirstargp_,
					     const melt_argdescr_cell_t
					     meltxargdescr_[],
					     union meltparam_un *meltxargtab_,
					     const melt_argdescr_cell_t
					     meltxresdescr_[],
					     union meltparam_un
					     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_67_warmelt_outobj_OUTPUCOD_OBJAGAIN (meltclosure_ptr_t meltclosp_,
					      melt_ptr_t meltfirstargp_,
					      const melt_argdescr_cell_t
					      meltxargdescr_[],
					      union meltparam_un
					      *meltxargtab_,
					      const melt_argdescr_cell_t
					      meltxresdescr_[],
					      union meltparam_un
					      *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_68_warmelt_outobj_OUTPUCOD_OBJCOMPUTE (meltclosure_ptr_t meltclosp_,
						melt_ptr_t meltfirstargp_,
						const melt_argdescr_cell_t
						meltxargdescr_[],
						union meltparam_un
						*meltxargtab_,
						const melt_argdescr_cell_t
						meltxresdescr_[],
						union meltparam_un
						*meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_69_warmelt_outobj_LAMBDA___8__ (meltclosure_ptr_t meltclosp_,
					 melt_ptr_t meltfirstargp_,
					 const melt_argdescr_cell_t
					 meltxargdescr_[],
					 union meltparam_un *meltxargtab_,
					 const melt_argdescr_cell_t
					 meltxresdescr_[],
					 union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_70_warmelt_outobj_LAMBDA___9__ (meltclosure_ptr_t meltclosp_,
					 melt_ptr_t meltfirstargp_,
					 const melt_argdescr_cell_t
					 meltxargdescr_[],
					 union meltparam_un *meltxargtab_,
					 const melt_argdescr_cell_t
					 meltxresdescr_[],
					 union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_71_warmelt_outobj_LAMBDA___10__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_72_warmelt_outobj_OUTPUCOD_OBJCOND (meltclosure_ptr_t meltclosp_,
					     melt_ptr_t meltfirstargp_,
					     const melt_argdescr_cell_t
					     meltxargdescr_[],
					     union meltparam_un *meltxargtab_,
					     const melt_argdescr_cell_t
					     meltxresdescr_[],
					     union meltparam_un
					     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_73_warmelt_outobj_OUTPUCOD_OBJCPPIF (meltclosure_ptr_t meltclosp_,
					      melt_ptr_t meltfirstargp_,
					      const melt_argdescr_cell_t
					      meltxargdescr_[],
					      union meltparam_un
					      *meltxargtab_,
					      const melt_argdescr_cell_t
					      meltxresdescr_[],
					      union meltparam_un
					      *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_74_warmelt_outobj_OUTPUCOD_OBJINTERNSYMBOL (meltclosure_ptr_t
						     meltclosp_,
						     melt_ptr_t
						     meltfirstargp_,
						     const
						     melt_argdescr_cell_t
						     meltxargdescr_[],
						     union meltparam_un
						     *meltxargtab_,
						     const
						     melt_argdescr_cell_t
						     meltxresdescr_[],
						     union meltparam_un
						     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_75_warmelt_outobj_OUTPUCOD_OBJINTERNKEYWORD (meltclosure_ptr_t
						      meltclosp_,
						      melt_ptr_t
						      meltfirstargp_,
						      const
						      melt_argdescr_cell_t
						      meltxargdescr_[],
						      union meltparam_un
						      *meltxargtab_,
						      const
						      melt_argdescr_cell_t
						      meltxresdescr_[],
						      union meltparam_un
						      *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_76_warmelt_outobj_OUTPUCOD_OBJGETNAMEDSYMBOL (meltclosure_ptr_t
						       meltclosp_,
						       melt_ptr_t
						       meltfirstargp_,
						       const
						       melt_argdescr_cell_t
						       meltxargdescr_[],
						       union meltparam_un
						       *meltxargtab_,
						       const
						       melt_argdescr_cell_t
						       meltxresdescr_[],
						       union meltparam_un
						       *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_77_warmelt_outobj_OUTPUCOD_OBJGETNAMEDKEYWORD (meltclosure_ptr_t
							meltclosp_,
							melt_ptr_t
							meltfirstargp_,
							const
							melt_argdescr_cell_t
							meltxargdescr_[],
							union meltparam_un
							*meltxargtab_,
							const
							melt_argdescr_cell_t
							meltxresdescr_[],
							union meltparam_un
							*meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_78_warmelt_outobj_OUTPUCOD_OBJAPPLY (meltclosure_ptr_t meltclosp_,
					      melt_ptr_t meltfirstargp_,
					      const melt_argdescr_cell_t
					      meltxargdescr_[],
					      union meltparam_un
					      *meltxargtab_,
					      const melt_argdescr_cell_t
					      meltxresdescr_[],
					      union meltparam_un
					      *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_79_warmelt_outobj_LAMBDA___11__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_80_warmelt_outobj_LAMBDA___12__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_81_warmelt_outobj_OUTPUCOD_OBJMSEND (meltclosure_ptr_t meltclosp_,
					      melt_ptr_t meltfirstargp_,
					      const melt_argdescr_cell_t
					      meltxargdescr_[],
					      union meltparam_un
					      *meltxargtab_,
					      const melt_argdescr_cell_t
					      meltxresdescr_[],
					      union meltparam_un
					      *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_82_warmelt_outobj_LAMBDA___13__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_83_warmelt_outobj_OUTPUCOD_OBJMULTIAPPLY (meltclosure_ptr_t
						   meltclosp_,
						   melt_ptr_t meltfirstargp_,
						   const melt_argdescr_cell_t
						   meltxargdescr_[],
						   union meltparam_un
						   *meltxargtab_,
						   const melt_argdescr_cell_t
						   meltxresdescr_[],
						   union meltparam_un
						   *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_84_warmelt_outobj_LAMBDA___14__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_85_warmelt_outobj_LAMBDA___15__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_86_warmelt_outobj_LAMBDA___16__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_87_warmelt_outobj_LAMBDA___17__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_88_warmelt_outobj_LAMBDA___18__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_89_warmelt_outobj_OUTPUCOD_OBJMULTIMSEND (meltclosure_ptr_t
						   meltclosp_,
						   melt_ptr_t meltfirstargp_,
						   const melt_argdescr_cell_t
						   meltxargdescr_[],
						   union meltparam_un
						   *meltxargtab_,
						   const melt_argdescr_cell_t
						   meltxresdescr_[],
						   union meltparam_un
						   *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_90_warmelt_outobj_LAMBDA___19__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_91_warmelt_outobj_LAMBDA___20__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_92_warmelt_outobj_LAMBDA___21__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_93_warmelt_outobj_LAMBDA___22__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_94_warmelt_outobj_LAMBDA___23__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_95_warmelt_outobj_OUTPUCOD_OBJCLEAR (meltclosure_ptr_t meltclosp_,
					      melt_ptr_t meltfirstargp_,
					      const melt_argdescr_cell_t
					      meltxargdescr_[],
					      union meltparam_un
					      *meltxargtab_,
					      const melt_argdescr_cell_t
					      meltxresdescr_[],
					      union meltparam_un
					      *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_96_warmelt_outobj_OUTPUCOD_OBJRAWALLOCOBJ (meltclosure_ptr_t
						    meltclosp_,
						    melt_ptr_t meltfirstargp_,
						    const melt_argdescr_cell_t
						    meltxargdescr_[],
						    union meltparam_un
						    *meltxargtab_,
						    const melt_argdescr_cell_t
						    meltxresdescr_[],
						    union meltparam_un
						    *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_97_warmelt_outobj_OUTPUCOD_OBJNEWCLOSURE (meltclosure_ptr_t
						   meltclosp_,
						   melt_ptr_t meltfirstargp_,
						   const melt_argdescr_cell_t
						   meltxargdescr_[],
						   union meltparam_un
						   *meltxargtab_,
						   const melt_argdescr_cell_t
						   meltxresdescr_[],
						   union meltparam_un
						   *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_98_warmelt_outobj_LAMBDA___24__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_99_warmelt_outobj_OUTPUCOD_OBJTOUCH (meltclosure_ptr_t meltclosp_,
					      melt_ptr_t meltfirstargp_,
					      const melt_argdescr_cell_t
					      meltxargdescr_[],
					      union meltparam_un
					      *meltxargtab_,
					      const melt_argdescr_cell_t
					      meltxresdescr_[],
					      union meltparam_un
					      *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_100_warmelt_outobj_OUTPUCOD_DBGTRACEWRITEOBJ (meltclosure_ptr_t
						       meltclosp_,
						       melt_ptr_t
						       meltfirstargp_,
						       const
						       melt_argdescr_cell_t
						       meltxargdescr_[],
						       union meltparam_un
						       *meltxargtab_,
						       const
						       melt_argdescr_cell_t
						       meltxresdescr_[],
						       union meltparam_un
						       *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_101_warmelt_outobj_OUTPUCOD_OBJPUTUPLE (meltclosure_ptr_t meltclosp_,
						 melt_ptr_t meltfirstargp_,
						 const melt_argdescr_cell_t
						 meltxargdescr_[],
						 union meltparam_un
						 *meltxargtab_,
						 const melt_argdescr_cell_t
						 meltxresdescr_[],
						 union meltparam_un
						 *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_102_warmelt_outobj_OUTPUCOD_OBJPUTPAIRHEAD (meltclosure_ptr_t
						     meltclosp_,
						     melt_ptr_t
						     meltfirstargp_,
						     const
						     melt_argdescr_cell_t
						     meltxargdescr_[],
						     union meltparam_un
						     *meltxargtab_,
						     const
						     melt_argdescr_cell_t
						     meltxresdescr_[],
						     union meltparam_un
						     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_103_warmelt_outobj_OUTPUCOD_OBJPUTPAIRTAIL (meltclosure_ptr_t
						     meltclosp_,
						     melt_ptr_t
						     meltfirstargp_,
						     const
						     melt_argdescr_cell_t
						     meltxargdescr_[],
						     union meltparam_un
						     *meltxargtab_,
						     const
						     melt_argdescr_cell_t
						     meltxresdescr_[],
						     union meltparam_un
						     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_104_warmelt_outobj_OUTPUCOD_OBJPUTLIST (meltclosure_ptr_t meltclosp_,
						 melt_ptr_t meltfirstargp_,
						 const melt_argdescr_cell_t
						 meltxargdescr_[],
						 union meltparam_un
						 *meltxargtab_,
						 const melt_argdescr_cell_t
						 meltxresdescr_[],
						 union meltparam_un
						 *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_105_warmelt_outobj_OUTPUCOD_OBJGETSLOT (meltclosure_ptr_t meltclosp_,
						 melt_ptr_t meltfirstargp_,
						 const melt_argdescr_cell_t
						 meltxargdescr_[],
						 union meltparam_un
						 *meltxargtab_,
						 const melt_argdescr_cell_t
						 meltxresdescr_[],
						 union meltparam_un
						 *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_106_warmelt_outobj_LAMBDA___25__ (meltclosure_ptr_t meltclosp_,
					   melt_ptr_t meltfirstargp_,
					   const melt_argdescr_cell_t
					   meltxargdescr_[],
					   union meltparam_un *meltxargtab_,
					   const melt_argdescr_cell_t
					   meltxresdescr_[],
					   union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_107_warmelt_outobj_OUTPUCOD_OBJPUTSLOT (meltclosure_ptr_t meltclosp_,
						 melt_ptr_t meltfirstargp_,
						 const melt_argdescr_cell_t
						 meltxargdescr_[],
						 union meltparam_un
						 *meltxargtab_,
						 const melt_argdescr_cell_t
						 meltxresdescr_[],
						 union meltparam_un
						 *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_108_warmelt_outobj_OUTPUCOD_OBJPUTCLOSUROUT (meltclosure_ptr_t
						      meltclosp_,
						      melt_ptr_t
						      meltfirstargp_,
						      const
						      melt_argdescr_cell_t
						      meltxargdescr_[],
						      union meltparam_un
						      *meltxargtab_,
						      const
						      melt_argdescr_cell_t
						      meltxresdescr_[],
						      union meltparam_un
						      *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_109_warmelt_outobj_OUTPUCOD_OBJPUTCLOSEDV (meltclosure_ptr_t
						    meltclosp_,
						    melt_ptr_t meltfirstargp_,
						    const melt_argdescr_cell_t
						    meltxargdescr_[],
						    union meltparam_un
						    *meltxargtab_,
						    const melt_argdescr_cell_t
						    meltxresdescr_[],
						    union meltparam_un
						    *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_110_warmelt_outobj_OUTPUCOD_OBJPUTCLOSEDNOTNULLV (meltclosure_ptr_t
							   meltclosp_,
							   melt_ptr_t
							   meltfirstargp_,
							   const
							   melt_argdescr_cell_t
							   meltxargdescr_[],
							   union meltparam_un
							   *meltxargtab_,
							   const
							   melt_argdescr_cell_t
							   meltxresdescr_[],
							   union meltparam_un
							   *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_111_warmelt_outobj_OUTPUCOD_OBJPUTROUTCONST (meltclosure_ptr_t
						      meltclosp_,
						      melt_ptr_t
						      meltfirstargp_,
						      const
						      melt_argdescr_cell_t
						      meltxargdescr_[],
						      union meltparam_un
						      *meltxargtab_,
						      const
						      melt_argdescr_cell_t
						      meltxresdescr_[],
						      union meltparam_un
						      *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_112_warmelt_outobj_OUTPUCOD_OBJPUTROUTCONSTNOTNULL (meltclosure_ptr_t
							     meltclosp_,
							     melt_ptr_t
							     meltfirstargp_,
							     const
							     melt_argdescr_cell_t
							     meltxargdescr_[],
							     union
							     meltparam_un
							     *meltxargtab_,
							     const
							     melt_argdescr_cell_t
							     meltxresdescr_[],
							     union
							     meltparam_un
							     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_113_warmelt_outobj_OUTPUCOD_OBJPUTXTRARESULT (meltclosure_ptr_t
						       meltclosp_,
						       melt_ptr_t
						       meltfirstargp_,
						       const
						       melt_argdescr_cell_t
						       meltxargdescr_[],
						       union meltparam_un
						       *meltxargtab_,
						       const
						       melt_argdescr_cell_t
						       meltxresdescr_[],
						       union meltparam_un
						       *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_114_warmelt_outobj_OUTPUCOD_OBJEXPV (meltclosure_ptr_t meltclosp_,
					      melt_ptr_t meltfirstargp_,
					      const melt_argdescr_cell_t
					      meltxargdescr_[],
					      union meltparam_un
					      *meltxargtab_,
					      const melt_argdescr_cell_t
					      meltxresdescr_[],
					      union meltparam_un
					      *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_115_warmelt_outobj_OUTPUCOD_OBJLOCATEDEXPV (meltclosure_ptr_t
						     meltclosp_,
						     melt_ptr_t
						     meltfirstargp_,
						     const
						     melt_argdescr_cell_t
						     meltxargdescr_[],
						     union meltparam_un
						     *meltxargtab_,
						     const
						     melt_argdescr_cell_t
						     meltxresdescr_[],
						     union meltparam_un
						     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_116_warmelt_outobj_OUTPUCOD_VERBATIMSTRING (meltclosure_ptr_t
						     meltclosp_,
						     melt_ptr_t
						     meltfirstargp_,
						     const
						     melt_argdescr_cell_t
						     meltxargdescr_[],
						     union meltparam_un
						     *meltxargtab_,
						     const
						     melt_argdescr_cell_t
						     meltxresdescr_[],
						     union meltparam_un
						     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_117_warmelt_outobj_OUTPUCOD_STRING (meltclosure_ptr_t meltclosp_,
					     melt_ptr_t meltfirstargp_,
					     const melt_argdescr_cell_t
					     meltxargdescr_[],
					     union meltparam_un *meltxargtab_,
					     const melt_argdescr_cell_t
					     meltxresdescr_[],
					     union meltparam_un
					     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_118_warmelt_outobj_OUTPUCOD_INTEGER (meltclosure_ptr_t meltclosp_,
					      melt_ptr_t meltfirstargp_,
					      const melt_argdescr_cell_t
					      meltxargdescr_[],
					      union meltparam_un
					      *meltxargtab_,
					      const melt_argdescr_cell_t
					      meltxresdescr_[],
					      union meltparam_un
					      *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_119_warmelt_outobj_OUTPUCOD_FINALRETURN (meltclosure_ptr_t
						  meltclosp_,
						  melt_ptr_t meltfirstargp_,
						  const melt_argdescr_cell_t
						  meltxargdescr_[],
						  union meltparam_un
						  *meltxargtab_,
						  const melt_argdescr_cell_t
						  meltxresdescr_[],
						  union meltparam_un
						  *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_120_warmelt_outobj_SORTED_NAMED_DICT_TUPLE (meltclosure_ptr_t
						     meltclosp_,
						     melt_ptr_t
						     meltfirstargp_,
						     const
						     melt_argdescr_cell_t
						     meltxargdescr_[],
						     union meltparam_un
						     *meltxargtab_,
						     const
						     melt_argdescr_cell_t
						     meltxresdescr_[],
						     union meltparam_un
						     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_121_warmelt_outobj_OUTPUT_EXPORTED_OFFSETS (meltclosure_ptr_t
						     meltclosp_,
						     melt_ptr_t
						     meltfirstargp_,
						     const
						     melt_argdescr_cell_t
						     meltxargdescr_[],
						     union meltparam_un
						     *meltxargtab_,
						     const
						     melt_argdescr_cell_t
						     meltxresdescr_[],
						     union meltparam_un
						     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_122_warmelt_outobj_NTH_SECUNDARY_FILE (meltclosure_ptr_t meltclosp_,
						melt_ptr_t meltfirstargp_,
						const melt_argdescr_cell_t
						meltxargdescr_[],
						union meltparam_un
						*meltxargtab_,
						const melt_argdescr_cell_t
						meltxresdescr_[],
						union meltparam_un
						*meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_123_warmelt_outobj_OUTPUT_MELT_DESCRIPTOR (meltclosure_ptr_t
						    meltclosp_,
						    melt_ptr_t meltfirstargp_,
						    const melt_argdescr_cell_t
						    meltxargdescr_[],
						    union meltparam_un
						    *meltxargtab_,
						    const melt_argdescr_cell_t
						    meltxresdescr_[],
						    union meltparam_un
						    *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_124_warmelt_outobj_SYNTESTGEN_ANY (meltclosure_ptr_t meltclosp_,
					    melt_ptr_t meltfirstargp_,
					    const melt_argdescr_cell_t
					    meltxargdescr_[],
					    union meltparam_un *meltxargtab_,
					    const melt_argdescr_cell_t
					    meltxresdescr_[],
					    union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_125_warmelt_outobj_SUBSTITUTE_FORMALS_FOR_SYNTEST (meltclosure_ptr_t
							    meltclosp_,
							    melt_ptr_t
							    meltfirstargp_,
							    const
							    melt_argdescr_cell_t
							    meltxargdescr_[],
							    union meltparam_un
							    *meltxargtab_,
							    const
							    melt_argdescr_cell_t
							    meltxresdescr_[],
							    union meltparam_un
							    *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_126_warmelt_outobj_EXPAND_TUPLE_FOR_SYNTEST (meltclosure_ptr_t
						      meltclosp_,
						      melt_ptr_t
						      meltfirstargp_,
						      const
						      melt_argdescr_cell_t
						      meltxargdescr_[],
						      union meltparam_un
						      *meltxargtab_,
						      const
						      melt_argdescr_cell_t
						      meltxresdescr_[],
						      union meltparam_un
						      *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_127_warmelt_outobj_SYNTESTGEN_PRIMITIVE (meltclosure_ptr_t
						  meltclosp_,
						  melt_ptr_t meltfirstargp_,
						  const melt_argdescr_cell_t
						  meltxargdescr_[],
						  union meltparam_un
						  *meltxargtab_,
						  const melt_argdescr_cell_t
						  meltxresdescr_[],
						  union meltparam_un
						  *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_128_warmelt_outobj_SYNTESTGEN_CITERATOR (meltclosure_ptr_t
						  meltclosp_,
						  melt_ptr_t meltfirstargp_,
						  const melt_argdescr_cell_t
						  meltxargdescr_[],
						  union meltparam_un
						  *meltxargtab_,
						  const melt_argdescr_cell_t
						  meltxresdescr_[],
						  union meltparam_un
						  *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_129_warmelt_outobj_SYNTESTGEN_CMATCHER (meltclosure_ptr_t meltclosp_,
						 melt_ptr_t meltfirstargp_,
						 const melt_argdescr_cell_t
						 meltxargdescr_[],
						 union meltparam_un
						 *meltxargtab_,
						 const melt_argdescr_cell_t
						 meltxresdescr_[],
						 union meltparam_un
						 *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_130_warmelt_outobj_EMIT_SYNTAX_TESTING_ROUTINE (meltclosure_ptr_t
							 meltclosp_,
							 melt_ptr_t
							 meltfirstargp_,
							 const
							 melt_argdescr_cell_t
							 meltxargdescr_[],
							 union meltparam_un
							 *meltxargtab_,
							 const
							 melt_argdescr_cell_t
							 meltxresdescr_[],
							 union meltparam_un
							 *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_131_warmelt_outobj_NORMADECLB_MACROEXPANDED_LIST (meltclosure_ptr_t
							   meltclosp_,
							   melt_ptr_t
							   meltfirstargp_,
							   const
							   melt_argdescr_cell_t
							   meltxargdescr_[],
							   union meltparam_un
							   *meltxargtab_,
							   const
							   melt_argdescr_cell_t
							   meltxresdescr_[],
							   union meltparam_un
							   *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_132_warmelt_outobj_LAMBDA___26__ (meltclosure_ptr_t meltclosp_,
					   melt_ptr_t meltfirstargp_,
					   const melt_argdescr_cell_t
					   meltxargdescr_[],
					   union meltparam_un *meltxargtab_,
					   const melt_argdescr_cell_t
					   meltxresdescr_[],
					   union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_133_warmelt_outobj_TRANSLATE_MACROEXPANDED_LIST (meltclosure_ptr_t
							  meltclosp_,
							  melt_ptr_t
							  meltfirstargp_,
							  const
							  melt_argdescr_cell_t
							  meltxargdescr_[],
							  union meltparam_un
							  *meltxargtab_,
							  const
							  melt_argdescr_cell_t
							  meltxresdescr_[],
							  union meltparam_un
							  *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_134_warmelt_outobj_LAMBDA___27__ (meltclosure_ptr_t meltclosp_,
					   melt_ptr_t meltfirstargp_,
					   const melt_argdescr_cell_t
					   meltxargdescr_[],
					   union meltparam_un *meltxargtab_,
					   const melt_argdescr_cell_t
					   meltxresdescr_[],
					   union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_135_warmelt_outobj_FATAL_COMPILE_ERROR (meltclosure_ptr_t meltclosp_,
						 melt_ptr_t meltfirstargp_,
						 const melt_argdescr_cell_t
						 meltxargdescr_[],
						 union meltparam_un
						 *meltxargtab_,
						 const melt_argdescr_cell_t
						 meltxresdescr_[],
						 union meltparam_un
						 *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_136_warmelt_outobj_COMPILE_LIST_SEXPR (meltclosure_ptr_t meltclosp_,
						melt_ptr_t meltfirstargp_,
						const melt_argdescr_cell_t
						meltxargdescr_[],
						union meltparam_un
						*meltxargtab_,
						const melt_argdescr_cell_t
						meltxresdescr_[],
						union meltparam_un
						*meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_137_warmelt_outobj_LAMBDA___28__ (meltclosure_ptr_t meltclosp_,
					   melt_ptr_t meltfirstargp_,
					   const melt_argdescr_cell_t
					   meltxargdescr_[],
					   union meltparam_un *meltxargtab_,
					   const melt_argdescr_cell_t
					   meltxresdescr_[],
					   union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_138_warmelt_outobj_AUTOBOX_NORMAL_RETURN (meltclosure_ptr_t
						   meltclosp_,
						   melt_ptr_t meltfirstargp_,
						   const melt_argdescr_cell_t
						   meltxargdescr_[],
						   union meltparam_un
						   *meltxargtab_,
						   const melt_argdescr_cell_t
						   meltxresdescr_[],
						   union meltparam_un
						   *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_139_warmelt_outobj_TRANSLATE_RUN_MELT_EXPRESSIONS (meltclosure_ptr_t
							    meltclosp_,
							    melt_ptr_t
							    meltfirstargp_,
							    const
							    melt_argdescr_cell_t
							    meltxargdescr_[],
							    union meltparam_un
							    *meltxargtab_,
							    const
							    melt_argdescr_cell_t
							    meltxresdescr_[],
							    union meltparam_un
							    *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_140_warmelt_outobj_LAMBDA___29__ (meltclosure_ptr_t meltclosp_,
					   melt_ptr_t meltfirstargp_,
					   const melt_argdescr_cell_t
					   meltxargdescr_[],
					   union meltparam_un *meltxargtab_,
					   const melt_argdescr_cell_t
					   meltxresdescr_[],
					   union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_141_warmelt_outobj_LAMBDA___30__ (meltclosure_ptr_t meltclosp_,
					   melt_ptr_t meltfirstargp_,
					   const melt_argdescr_cell_t
					   meltxargdescr_[],
					   union meltparam_un *meltxargtab_,
					   const melt_argdescr_cell_t
					   meltxresdescr_[],
					   union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_142_warmelt_outobj_LAMBDA___31__ (meltclosure_ptr_t meltclosp_,
					   melt_ptr_t meltfirstargp_,
					   const melt_argdescr_cell_t
					   meltxargdescr_[],
					   union meltparam_un *meltxargtab_,
					   const melt_argdescr_cell_t
					   meltxresdescr_[],
					   union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_143_warmelt_outobj_READ_MELT_EXPRESSIONS (meltclosure_ptr_t
						   meltclosp_,
						   melt_ptr_t meltfirstargp_,
						   const melt_argdescr_cell_t
						   meltxargdescr_[],
						   union meltparam_un
						   *meltxargtab_,
						   const melt_argdescr_cell_t
						   meltxresdescr_[],
						   union meltparam_un
						   *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_144_warmelt_outobj_TRANSLATE_TO_C_MODULE_MELT_SOURCES
(meltclosure_ptr_t meltclosp_, melt_ptr_t meltfirstargp_, const melt_argdescr_cell_t meltxargdescr_[],
union meltparam_un *meltxargtab_, const melt_argdescr_cell_t meltxresdescr_[],
union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_145_warmelt_outobj_GENERATE_GPLV3PLUS_COPYRIGHT_NOTICE_C_COMMENT
(meltclosure_ptr_t meltclosp_, melt_ptr_t meltfirstargp_, const melt_argdescr_cell_t meltxargdescr_[],
union meltparam_un *meltxargtab_, const melt_argdescr_cell_t meltxresdescr_[],
union meltparam_un *meltxrestab_);



MELT_EXTERN void *melt_start_this_module (void *);


/* define different names when debugging or not */
#if MELT_HAVE_DEBUG
MELT_EXTERN const char meltmodule_warmelt_outobj__melt_have_debug_enabled[];
#define melt_have_debug_string meltmodule_warmelt_outobj__melt_have_debug_enabled
#else /*!MELT_HAVE_DEBUG */
MELT_EXTERN const char meltmodule_warmelt_outobj__melt_have_debug_disabled[];
#define melt_have_debug_string meltmodule_warmelt_outobj__melt_have_debug_disabled
#endif /*!MELT_HAVE_DEBUG */


struct frame_melt_start_this_module_st;
void MELT_MODULE_VISIBILITY
meltmod__warmelt_outobj__initialmeltchunk_0 (struct
					     frame_melt_start_this_module_st
					     *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_outobj__initialmeltchunk_1 (struct
					     frame_melt_start_this_module_st
					     *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_outobj__initialmeltchunk_2 (struct
					     frame_melt_start_this_module_st
					     *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_outobj__initialmeltchunk_3 (struct
					     frame_melt_start_this_module_st
					     *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_outobj__initialmeltchunk_4 (struct
					     frame_melt_start_this_module_st
					     *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_outobj__initialmeltchunk_5 (struct
					     frame_melt_start_this_module_st
					     *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_outobj__initialmeltchunk_6 (struct
					     frame_melt_start_this_module_st
					     *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_outobj__initialmeltchunk_7 (struct
					     frame_melt_start_this_module_st
					     *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_outobj__initialmeltchunk_8 (struct
					     frame_melt_start_this_module_st
					     *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_outobj__initialmeltchunk_9 (struct
					     frame_melt_start_this_module_st
					     *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_outobj__initialmeltchunk_10 (struct
					      frame_melt_start_this_module_st
					      *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_outobj__initialmeltchunk_11 (struct
					      frame_melt_start_this_module_st
					      *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_outobj__initialmeltchunk_12 (struct
					      frame_melt_start_this_module_st
					      *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_outobj__initialmeltchunk_13 (struct
					      frame_melt_start_this_module_st
					      *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_outobj__initialmeltchunk_14 (struct
					      frame_melt_start_this_module_st
					      *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_outobj__initialmeltchunk_15 (struct
					      frame_melt_start_this_module_st
					      *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_outobj__initialmeltchunk_16 (struct
					      frame_melt_start_this_module_st
					      *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_outobj__initialmeltchunk_17 (struct
					      frame_melt_start_this_module_st
					      *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_outobj__forward_or_mark_module_start_frame (struct
							     melt_callframe_st
							     *fp,
							     int marking);
#define meltmarking_melt_start_this_module  meltmod__warmelt_outobj__forward_or_mark_module_start_frame



/**** warmelt-outobj+01.c implementations ****/




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_5_warmelt_outobj_OUTPUCOD_OBJINIELEM (meltclosure_ptr_t meltclosp_,
					       melt_ptr_t meltfirstargp_,
					       const melt_argdescr_cell_t
					       meltxargdescr_[],
					       union meltparam_un
					       *meltxargtab_,
					       const melt_argdescr_cell_t
					       meltxresdescr_[],
					       union meltparam_un
					       *meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_5_warmelt_outobj_OUTPUCOD_OBJINIELEM_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_5_warmelt_outobj_OUTPUCOD_OBJINIELEM_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 34
    melt_ptr_t mcfr_varptr[34];
#define MELTFRAM_NBVARNUM 21
    long mcfr_varnum[21];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_5_warmelt_outobj_OUTPUCOD_OBJINIELEM is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_5_warmelt_outobj_OUTPUCOD_OBJINIELEM_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 34; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_5_warmelt_outobj_OUTPUCOD_OBJINIELEM nbval 34*/
  meltfram__.mcfr_nbvar = 34 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("OUTPUCOD_OBJINIELEM", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-outobj.melt:89:/ getarg");
 /*_.OBIELEM__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.DECLBUF__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.DECLBUF__V3*/ meltfptr[2])) !=
	      NULL);


  /*getarg#2 */
  /*^getarg */
  if (meltxargdescr_[1] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.IMPLBUF__V4*/ meltfptr[3] =
    (meltxargtab_[1].meltbp_aptr) ? (*(meltxargtab_[1].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3])) !=
	      NULL);


  /*getarg#3 */
  /*^getarg */
  if (meltxargdescr_[2] != MELTBPAR_LONG)
    goto lab_endgetargs;
 /*_#DEPTH__L1*/ meltfnum[0] = meltxargtab_[2].meltbp_long;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:90:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L2*/ meltfnum[1] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.OBIELEM__V2*/ meltfptr[1]),
			     (melt_ptr_t) (( /*!CLASS_OBJINITELEM */
					    meltfrout->tabval[0])));;
      MELT_LOCATION ("warmelt-outobj.melt:90:/ cond");
      /*cond */ if ( /*_#IS_A__L2*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V6*/ meltfptr[5] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-outobj.melt:90:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check obelem"),
				  ("warmelt-outobj.melt")
				  ? ("warmelt-outobj.melt") : __FILE__,
				  (90) ? (90) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V6*/ meltfptr[5] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V5*/ meltfptr[4] = /*_.IFELSE___V6*/ meltfptr[5];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:90:/ clear");
	     /*clear *//*_#IS_A__L2*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V6*/ meltfptr[5] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V5*/ meltfptr[4] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-outobj.melt:91:/ quasiblock");


    /*^getslot */
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.OBIELEM__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 4, "OIE_LOCVAR");
  /*_.OLOCVAR__V8*/ meltfptr[7] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-outobj.melt:92:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.OBIELEM__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 1, "OIE_CNAME");
  /*_.CNAM__V9*/ meltfptr[8] = slot;
    };
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:93:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_STRING__L3*/ meltfnum[1] =
	(melt_magic_discr ((melt_ptr_t) ( /*_.CNAM__V9*/ meltfptr[8])) ==
	 MELTOBMAG_STRING);;
      MELT_LOCATION ("warmelt-outobj.melt:93:/ cond");
      /*cond */ if ( /*_#IS_STRING__L3*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V11*/ meltfptr[10] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-outobj.melt:93:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check cnam"),
				  ("warmelt-outobj.melt")
				  ? ("warmelt-outobj.melt") : __FILE__,
				  (93) ? (93) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V11*/ meltfptr[10] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V10*/ meltfptr[9] = /*_.IFELSE___V11*/ meltfptr[10];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:93:/ clear");
	     /*clear *//*_#IS_STRING__L3*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V11*/ meltfptr[10] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V10*/ meltfptr[9] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:94:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
      /*^cond */
      /*cond */ if ( /*_.OLOCVAR__V8*/ meltfptr[7])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V13*/ meltfptr[12] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-outobj.melt:94:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check olocvar"),
				  ("warmelt-outobj.melt")
				  ? ("warmelt-outobj.melt") : __FILE__,
				  (94) ? (94) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V13*/ meltfptr[12] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V12*/ meltfptr[10] = /*_.IFELSE___V13*/ meltfptr[12];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:94:/ clear");
	     /*clear *//*_.IFELSE___V13*/ meltfptr[12] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V12*/ meltfptr[10] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-outobj.melt:95:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^msend */
    /*msend */
    {
      union meltparam_un argtab[3];
      memset (&argtab, 0, sizeof (argtab));
      /*^ojbmsend.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.DECLBUF__V3*/ meltfptr[2];
      /*^ojbmsend.arg */
      argtab[1].meltbp_aptr = (melt_ptr_t *) & /*_.IMPLBUF__V4*/ meltfptr[3];
      /*^ojbmsend.arg */
      argtab[2].meltbp_long = /*_#DEPTH__L1*/ meltfnum[0];
      /*_.OUTPUT_C_CODE__V14*/ meltfptr[12] =
	meltgc_send ((melt_ptr_t) ( /*_.OLOCVAR__V8*/ meltfptr[7]),
		     (melt_ptr_t) (( /*!OUTPUT_C_CODE */ meltfrout->
				    tabval[1])),
		     (MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_LONG ""),
		     argtab, "", (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:97:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#STRBUF_USEDLENGTH__L4*/ meltfnum[1] =
      melt_strbuf_usedlength ((melt_ptr_t) ( /*_.DECLBUF__V3*/ meltfptr[2]));;
    MELT_LOCATION ("warmelt-outobj.melt:97:/ cond");
    /*cond */ if (
		   /*ifisa */
		   melt_is_instance_of ((melt_ptr_t)
					(( /*!BUFFER_LIMIT_CONT */ meltfrout->
					  tabval[2])),
					(melt_ptr_t) (( /*!CLASS_REFERENCE */
						       meltfrout->tabval[3])))
      )				/*then */
      {
	/*^cond.then */
	/*^getslot */
	{
	  melt_ptr_t slot = NULL, obj = NULL;
	  obj =
	    (melt_ptr_t) (( /*!BUFFER_LIMIT_CONT */ meltfrout->
			   tabval[2])) /*=obj*/ ;
	  melt_object_get_field (slot, obj, 0, "REFERENCED_VALUE");
   /*_.REFERENCED_VALUE__V15*/ meltfptr[14] = slot;
	};
	;
      }
    else
      {				/*^cond.else */

  /*_.REFERENCED_VALUE__V15*/ meltfptr[14] = NULL;;
      }
    ;
    /*^compute */
 /*_#GET_INT__L5*/ meltfnum[4] =
      (melt_get_int
       ((melt_ptr_t) ( /*_.REFERENCED_VALUE__V15*/ meltfptr[14])));;
    /*^compute */
 /*_#IRAW__L6*/ meltfnum[5] =
      (( /*_#GET_INT__L5*/ meltfnum[4]) / (2));;
    /*^compute */
 /*_#I__L7*/ meltfnum[6] =
      (( /*_#STRBUF_USEDLENGTH__L4*/ meltfnum[1]) >
       ( /*_#IRAW__L6*/ meltfnum[5]));;
    MELT_LOCATION ("warmelt-outobj.melt:97:/ cond");
    /*cond */ if ( /*_#I__L7*/ meltfnum[6])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{


	  {
	    MELT_LOCATION ("warmelt-outobj.melt:98:/ locexp");

#if MELT_HAVE_DEBUG
	    if (melt_need_debug (0))
	      melt_dbgshortbacktrace (("outpucod_objinielem huge declbuf"),
				      (10));
#endif
	    ;
	  }
	  ;

#if MELT_HAVE_DEBUG
	  MELT_LOCATION ("warmelt-outobj.melt:99:/ cppif.then");
	  /*^block */
	  /*anyblock */
	  {


	    {
	      /*^locexp */
	      /*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	      melt_dbgcounter++;
#endif
	      ;
	    }
	    ;
	    /*^checksignal */
	    MELT_CHECK_SIGNAL ();
	    ;
     /*_#MELT_NEED_DBG__L8*/ meltfnum[7] =
	      /*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	      ( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	      0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	      ;;
	    MELT_LOCATION ("warmelt-outobj.melt:99:/ cond");
	    /*cond */ if ( /*_#MELT_NEED_DBG__L8*/ meltfnum[7])	/*then */
	      {
		/*^cond.then */
		/*^block */
		/*anyblock */
		{

       /*_#THE_MELTCALLCOUNT__L9*/ meltfnum[8] =
#ifdef meltcallcount
		    meltcallcount	/* the_meltcallcount */
#else
		    0L
#endif /* meltcallcount the_meltcallcount */
		    ;;
		  MELT_LOCATION ("warmelt-outobj.melt:99:/ checksignal");
		  MELT_CHECK_SIGNAL ();
		  ;
		  /*^apply */
		  /*apply */
		  {
		    union meltparam_un argtab[5];
		    memset (&argtab, 0, sizeof (argtab));
		    /*^apply.arg */
		    argtab[0].meltbp_long =
		      /*_#THE_MELTCALLCOUNT__L9*/ meltfnum[8];
		    /*^apply.arg */
		    argtab[1].meltbp_cstring = "warmelt-outobj.melt";
		    /*^apply.arg */
		    argtab[2].meltbp_long = 99;
		    /*^apply.arg */
		    argtab[3].meltbp_cstring =
		      "outpucod_objinielem huge declbuf=";
		    /*^apply.arg */
		    argtab[4].meltbp_aptr =
		      (melt_ptr_t *) & /*_.DECLBUF__V3*/ meltfptr[2];
		    /*_.MELT_DEBUG_FUN__V19*/ meltfptr[18] =
		      melt_apply ((meltclosure_ptr_t)
				  (( /*!MELT_DEBUG_FUN */ meltfrout->
				    tabval[4])),
				  (melt_ptr_t) (( /*nil */ NULL)),
				  (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_PTR ""), argtab, "",
				  (union meltparam_un *) 0);
		  }
		  ;
		  /*_.IF___V18*/ meltfptr[17] =
		    /*_.MELT_DEBUG_FUN__V19*/ meltfptr[18];;
		  /*epilog */

		  MELT_LOCATION ("warmelt-outobj.melt:99:/ clear");
		 /*clear *//*_#THE_MELTCALLCOUNT__L9*/ meltfnum[8] = 0;
		  /*^clear */
		 /*clear *//*_.MELT_DEBUG_FUN__V19*/ meltfptr[18] = 0;
		}
		;
	      }
	    else
	      {			/*^cond.else */

      /*_.IF___V18*/ meltfptr[17] = NULL;;
	      }
	    ;
	    MELT_LOCATION ("warmelt-outobj.melt:99:/ quasiblock");


	    /*_.PROGN___V20*/ meltfptr[18] = /*_.IF___V18*/ meltfptr[17];;
	    /*^compute */
	    /*_.IFCPP___V17*/ meltfptr[16] = /*_.PROGN___V20*/ meltfptr[18];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-outobj.melt:99:/ clear");
	       /*clear *//*_#MELT_NEED_DBG__L8*/ meltfnum[7] = 0;
	    /*^clear */
	       /*clear *//*_.IF___V18*/ meltfptr[17] = 0;
	    /*^clear */
	       /*clear *//*_.PROGN___V20*/ meltfptr[18] = 0;
	  }

#else /*MELT_HAVE_DEBUG */
	  /*^cppif.else */
	  /*_.IFCPP___V17*/ meltfptr[16] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
	  ;

#if MELT_HAVE_DEBUG
	  MELT_LOCATION ("warmelt-outobj.melt:100:/ cppif.then");
	  /*^block */
	  /*anyblock */
	  {

	    /*^checksignal */
	    MELT_CHECK_SIGNAL ();
	    ;
     /*_#STRBUF_USEDLENGTH__L10*/ meltfnum[8] =
	      melt_strbuf_usedlength ((melt_ptr_t)
				      ( /*_.DECLBUF__V3*/ meltfptr[2]));;
	    MELT_LOCATION ("warmelt-outobj.melt:101:/ cond");
	    /*cond */ if (
			   /*ifisa */
			   melt_is_instance_of ((melt_ptr_t)
						(( /*!BUFFER_LIMIT_CONT */
						  meltfrout->tabval[2])),
						(melt_ptr_t) (( /*!CLASS_REFERENCE */ meltfrout->tabval[3])))
	      )			/*then */
	      {
		/*^cond.then */
		/*^getslot */
		{
		  melt_ptr_t slot = NULL, obj = NULL;
		  obj =
		    (melt_ptr_t) (( /*!BUFFER_LIMIT_CONT */ meltfrout->
				   tabval[2])) /*=obj*/ ;
		  melt_object_get_field (slot, obj, 0, "REFERENCED_VALUE");
       /*_.REFERENCED_VALUE__V22*/ meltfptr[18] = slot;
		};
		;
	      }
	    else
	      {			/*^cond.else */

      /*_.REFERENCED_VALUE__V22*/ meltfptr[18] = NULL;;
	      }
	    ;
	    /*^compute */
     /*_#GET_INT__L11*/ meltfnum[7] =
	      (melt_get_int
	       ((melt_ptr_t) ( /*_.REFERENCED_VALUE__V22*/ meltfptr[18])));;
	    /*^compute */
     /*_#I__L12*/ meltfnum[11] =
	      (( /*_#STRBUF_USEDLENGTH__L10*/ meltfnum[8]) <
	       ( /*_#GET_INT__L11*/ meltfnum[7]));;
	    MELT_LOCATION ("warmelt-outobj.melt:100:/ cond");
	    /*cond */ if ( /*_#I__L12*/ meltfnum[11])	/*then */
	      {
		/*^cond.then */
		/*_.IFELSE___V23*/ meltfptr[22] = ( /*nil */ NULL);;
	      }
	    else
	      {
		MELT_LOCATION ("warmelt-outobj.melt:100:/ cond.else");

		/*^block */
		/*anyblock */
		{




		  {
		    /*^locexp */
		    melt_assert_failed (("check limited declbuf"),
					("warmelt-outobj.melt")
					? ("warmelt-outobj.melt") : __FILE__,
					(100) ? (100) : __LINE__,
					__FUNCTION__);
		    ;
		  }
		  ;
		 /*clear *//*_.IFELSE___V23*/ meltfptr[22] = 0;
		  /*epilog */
		}
		;
	      }
	    ;
	    /*^compute */
	    /*_.IFCPP___V21*/ meltfptr[17] = /*_.IFELSE___V23*/ meltfptr[22];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-outobj.melt:100:/ clear");
	       /*clear *//*_#STRBUF_USEDLENGTH__L10*/ meltfnum[8] = 0;
	    /*^clear */
	       /*clear *//*_.REFERENCED_VALUE__V22*/ meltfptr[18] = 0;
	    /*^clear */
	       /*clear *//*_#GET_INT__L11*/ meltfnum[7] = 0;
	    /*^clear */
	       /*clear *//*_#I__L12*/ meltfnum[11] = 0;
	    /*^clear */
	       /*clear *//*_.IFELSE___V23*/ meltfptr[22] = 0;
	  }

#else /*MELT_HAVE_DEBUG */
	  /*^cppif.else */
	  /*_.IFCPP___V21*/ meltfptr[17] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
	  ;
	  MELT_LOCATION ("warmelt-outobj.melt:97:/ quasiblock");


	  /*_.PROGN___V24*/ meltfptr[18] = /*_.IFCPP___V21*/ meltfptr[17];;
	  /*^compute */
	  /*_.IFELSE___V16*/ meltfptr[15] = /*_.PROGN___V24*/ meltfptr[18];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-outobj.melt:97:/ clear");
	     /*clear *//*_.IFCPP___V17*/ meltfptr[16] = 0;
	  /*^clear */
	     /*clear *//*_.IFCPP___V21*/ meltfptr[17] = 0;
	  /*^clear */
	     /*clear *//*_.PROGN___V24*/ meltfptr[18] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

  /*_.IFELSE___V16*/ meltfptr[15] = NULL;;
      }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:103:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#STRBUF_USEDLENGTH__L13*/ meltfnum[8] =
      melt_strbuf_usedlength ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]));;
    MELT_LOCATION ("warmelt-outobj.melt:103:/ cond");
    /*cond */ if (
		   /*ifisa */
		   melt_is_instance_of ((melt_ptr_t)
					(( /*!BUFFER_LIMIT_CONT */ meltfrout->
					  tabval[2])),
					(melt_ptr_t) (( /*!CLASS_REFERENCE */
						       meltfrout->tabval[3])))
      )				/*then */
      {
	/*^cond.then */
	/*^getslot */
	{
	  melt_ptr_t slot = NULL, obj = NULL;
	  obj =
	    (melt_ptr_t) (( /*!BUFFER_LIMIT_CONT */ meltfrout->
			   tabval[2])) /*=obj*/ ;
	  melt_object_get_field (slot, obj, 0, "REFERENCED_VALUE");
   /*_.REFERENCED_VALUE__V25*/ meltfptr[22] = slot;
	};
	;
      }
    else
      {				/*^cond.else */

  /*_.REFERENCED_VALUE__V25*/ meltfptr[22] = NULL;;
      }
    ;
    /*^compute */
 /*_#GET_INT__L14*/ meltfnum[7] =
      (melt_get_int
       ((melt_ptr_t) ( /*_.REFERENCED_VALUE__V25*/ meltfptr[22])));;
    /*^compute */
 /*_#IRAW__L15*/ meltfnum[11] =
      (( /*_#GET_INT__L14*/ meltfnum[7]) / (2));;
    /*^compute */
 /*_#I__L16*/ meltfnum[15] =
      (( /*_#STRBUF_USEDLENGTH__L13*/ meltfnum[8]) >
       ( /*_#IRAW__L15*/ meltfnum[11]));;
    MELT_LOCATION ("warmelt-outobj.melt:103:/ cond");
    /*cond */ if ( /*_#I__L16*/ meltfnum[15])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{


	  {
	    MELT_LOCATION ("warmelt-outobj.melt:104:/ locexp");

#if MELT_HAVE_DEBUG
	    if (melt_need_debug (0))
	      melt_dbgshortbacktrace (("outpucod_objinielem huge implbuf"),
				      (10));
#endif
	    ;
	  }
	  ;

#if MELT_HAVE_DEBUG
	  MELT_LOCATION ("warmelt-outobj.melt:105:/ cppif.then");
	  /*^block */
	  /*anyblock */
	  {


	    {
	      /*^locexp */
	      /*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	      melt_dbgcounter++;
#endif
	      ;
	    }
	    ;
	    /*^checksignal */
	    MELT_CHECK_SIGNAL ();
	    ;
     /*_#MELT_NEED_DBG__L17*/ meltfnum[16] =
	      /*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	      ( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	      0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	      ;;
	    MELT_LOCATION ("warmelt-outobj.melt:105:/ cond");
	    /*cond */ if ( /*_#MELT_NEED_DBG__L17*/ meltfnum[16])	/*then */
	      {
		/*^cond.then */
		/*^block */
		/*anyblock */
		{

       /*_#THE_MELTCALLCOUNT__L18*/ meltfnum[17] =
#ifdef meltcallcount
		    meltcallcount	/* the_meltcallcount */
#else
		    0L
#endif /* meltcallcount the_meltcallcount */
		    ;;
		  MELT_LOCATION ("warmelt-outobj.melt:105:/ checksignal");
		  MELT_CHECK_SIGNAL ();
		  ;
		  /*^apply */
		  /*apply */
		  {
		    union meltparam_un argtab[5];
		    memset (&argtab, 0, sizeof (argtab));
		    /*^apply.arg */
		    argtab[0].meltbp_long =
		      /*_#THE_MELTCALLCOUNT__L18*/ meltfnum[17];
		    /*^apply.arg */
		    argtab[1].meltbp_cstring = "warmelt-outobj.melt";
		    /*^apply.arg */
		    argtab[2].meltbp_long = 105;
		    /*^apply.arg */
		    argtab[3].meltbp_cstring =
		      "outpucod_objinielem huge implbuf=";
		    /*^apply.arg */
		    argtab[4].meltbp_aptr =
		      (melt_ptr_t *) & /*_.IMPLBUF__V4*/ meltfptr[3];
		    /*_.MELT_DEBUG_FUN__V29*/ meltfptr[28] =
		      melt_apply ((meltclosure_ptr_t)
				  (( /*!MELT_DEBUG_FUN */ meltfrout->
				    tabval[4])),
				  (melt_ptr_t) (( /*nil */ NULL)),
				  (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_PTR ""), argtab, "",
				  (union meltparam_un *) 0);
		  }
		  ;
		  /*_.IF___V28*/ meltfptr[18] =
		    /*_.MELT_DEBUG_FUN__V29*/ meltfptr[28];;
		  /*epilog */

		  MELT_LOCATION ("warmelt-outobj.melt:105:/ clear");
		 /*clear *//*_#THE_MELTCALLCOUNT__L18*/ meltfnum[17] = 0;
		  /*^clear */
		 /*clear *//*_.MELT_DEBUG_FUN__V29*/ meltfptr[28] = 0;
		}
		;
	      }
	    else
	      {			/*^cond.else */

      /*_.IF___V28*/ meltfptr[18] = NULL;;
	      }
	    ;
	    MELT_LOCATION ("warmelt-outobj.melt:105:/ quasiblock");


	    /*_.PROGN___V30*/ meltfptr[28] = /*_.IF___V28*/ meltfptr[18];;
	    /*^compute */
	    /*_.IFCPP___V27*/ meltfptr[17] = /*_.PROGN___V30*/ meltfptr[28];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-outobj.melt:105:/ clear");
	       /*clear *//*_#MELT_NEED_DBG__L17*/ meltfnum[16] = 0;
	    /*^clear */
	       /*clear *//*_.IF___V28*/ meltfptr[18] = 0;
	    /*^clear */
	       /*clear *//*_.PROGN___V30*/ meltfptr[28] = 0;
	  }

#else /*MELT_HAVE_DEBUG */
	  /*^cppif.else */
	  /*_.IFCPP___V27*/ meltfptr[17] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
	  ;

#if MELT_HAVE_DEBUG
	  MELT_LOCATION ("warmelt-outobj.melt:106:/ cppif.then");
	  /*^block */
	  /*anyblock */
	  {

	    /*^checksignal */
	    MELT_CHECK_SIGNAL ();
	    ;
     /*_#STRBUF_USEDLENGTH__L19*/ meltfnum[17] =
	      melt_strbuf_usedlength ((melt_ptr_t)
				      ( /*_.IMPLBUF__V4*/ meltfptr[3]));;
	    MELT_LOCATION ("warmelt-outobj.melt:107:/ cond");
	    /*cond */ if (
			   /*ifisa */
			   melt_is_instance_of ((melt_ptr_t)
						(( /*!BUFFER_LIMIT_CONT */
						  meltfrout->tabval[2])),
						(melt_ptr_t) (( /*!CLASS_REFERENCE */ meltfrout->tabval[3])))
	      )			/*then */
	      {
		/*^cond.then */
		/*^getslot */
		{
		  melt_ptr_t slot = NULL, obj = NULL;
		  obj =
		    (melt_ptr_t) (( /*!BUFFER_LIMIT_CONT */ meltfrout->
				   tabval[2])) /*=obj*/ ;
		  melt_object_get_field (slot, obj, 0, "REFERENCED_VALUE");
       /*_.REFERENCED_VALUE__V32*/ meltfptr[28] = slot;
		};
		;
	      }
	    else
	      {			/*^cond.else */

      /*_.REFERENCED_VALUE__V32*/ meltfptr[28] = NULL;;
	      }
	    ;
	    /*^compute */
     /*_#GET_INT__L20*/ meltfnum[16] =
	      (melt_get_int
	       ((melt_ptr_t) ( /*_.REFERENCED_VALUE__V32*/ meltfptr[28])));;
	    /*^compute */
     /*_#I__L21*/ meltfnum[20] =
	      (( /*_#STRBUF_USEDLENGTH__L19*/ meltfnum[17]) <
	       ( /*_#GET_INT__L20*/ meltfnum[16]));;
	    MELT_LOCATION ("warmelt-outobj.melt:106:/ cond");
	    /*cond */ if ( /*_#I__L21*/ meltfnum[20])	/*then */
	      {
		/*^cond.then */
		/*_.IFELSE___V33*/ meltfptr[32] = ( /*nil */ NULL);;
	      }
	    else
	      {
		MELT_LOCATION ("warmelt-outobj.melt:106:/ cond.else");

		/*^block */
		/*anyblock */
		{




		  {
		    /*^locexp */
		    melt_assert_failed (("check limited implbuf"),
					("warmelt-outobj.melt")
					? ("warmelt-outobj.melt") : __FILE__,
					(106) ? (106) : __LINE__,
					__FUNCTION__);
		    ;
		  }
		  ;
		 /*clear *//*_.IFELSE___V33*/ meltfptr[32] = 0;
		  /*epilog */
		}
		;
	      }
	    ;
	    /*^compute */
	    /*_.IFCPP___V31*/ meltfptr[18] = /*_.IFELSE___V33*/ meltfptr[32];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-outobj.melt:106:/ clear");
	       /*clear *//*_#STRBUF_USEDLENGTH__L19*/ meltfnum[17] = 0;
	    /*^clear */
	       /*clear *//*_.REFERENCED_VALUE__V32*/ meltfptr[28] = 0;
	    /*^clear */
	       /*clear *//*_#GET_INT__L20*/ meltfnum[16] = 0;
	    /*^clear */
	       /*clear *//*_#I__L21*/ meltfnum[20] = 0;
	    /*^clear */
	       /*clear *//*_.IFELSE___V33*/ meltfptr[32] = 0;
	  }

#else /*MELT_HAVE_DEBUG */
	  /*^cppif.else */
	  /*_.IFCPP___V31*/ meltfptr[18] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
	  ;
	  MELT_LOCATION ("warmelt-outobj.melt:103:/ quasiblock");


	  /*_.PROGN___V34*/ meltfptr[28] = /*_.IFCPP___V31*/ meltfptr[18];;
	  /*^compute */
	  /*_.IFELSE___V26*/ meltfptr[16] = /*_.PROGN___V34*/ meltfptr[28];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-outobj.melt:103:/ clear");
	     /*clear *//*_.IFCPP___V27*/ meltfptr[17] = 0;
	  /*^clear */
	     /*clear *//*_.IFCPP___V31*/ meltfptr[18] = 0;
	  /*^clear */
	     /*clear *//*_.PROGN___V34*/ meltfptr[28] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

  /*_.IFELSE___V26*/ meltfptr[16] = NULL;;
      }
    ;
    /*^compute */
    /*_.LET___V7*/ meltfptr[5] = /*_.IFELSE___V26*/ meltfptr[16];;

    MELT_LOCATION ("warmelt-outobj.melt:91:/ clear");
	   /*clear *//*_.OLOCVAR__V8*/ meltfptr[7] = 0;
    /*^clear */
	   /*clear *//*_.CNAM__V9*/ meltfptr[8] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V10*/ meltfptr[9] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V12*/ meltfptr[10] = 0;
    /*^clear */
	   /*clear *//*_.OUTPUT_C_CODE__V14*/ meltfptr[12] = 0;
    /*^clear */
	   /*clear *//*_#STRBUF_USEDLENGTH__L4*/ meltfnum[1] = 0;
    /*^clear */
	   /*clear *//*_.REFERENCED_VALUE__V15*/ meltfptr[14] = 0;
    /*^clear */
	   /*clear *//*_#GET_INT__L5*/ meltfnum[4] = 0;
    /*^clear */
	   /*clear *//*_#IRAW__L6*/ meltfnum[5] = 0;
    /*^clear */
	   /*clear *//*_#I__L7*/ meltfnum[6] = 0;
    /*^clear */
	   /*clear *//*_.IFELSE___V16*/ meltfptr[15] = 0;
    /*^clear */
	   /*clear *//*_#STRBUF_USEDLENGTH__L13*/ meltfnum[8] = 0;
    /*^clear */
	   /*clear *//*_.REFERENCED_VALUE__V25*/ meltfptr[22] = 0;
    /*^clear */
	   /*clear *//*_#GET_INT__L14*/ meltfnum[7] = 0;
    /*^clear */
	   /*clear *//*_#IRAW__L15*/ meltfnum[11] = 0;
    /*^clear */
	   /*clear *//*_#I__L16*/ meltfnum[15] = 0;
    /*^clear */
	   /*clear *//*_.IFELSE___V26*/ meltfptr[16] = 0;
    MELT_LOCATION ("warmelt-outobj.melt:89:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.LET___V7*/ meltfptr[5];;

    {
      MELT_LOCATION ("warmelt-outobj.melt:89:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V5*/ meltfptr[4] = 0;
    /*^clear */
	   /*clear *//*_.LET___V7*/ meltfptr[5] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("OUTPUCOD_OBJINIELEM", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_5_warmelt_outobj_OUTPUCOD_OBJINIELEM_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_5_warmelt_outobj_OUTPUCOD_OBJINIELEM */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_6_warmelt_outobj_OUTCINITFILL_ROOT (meltclosure_ptr_t meltclosp_,
					     melt_ptr_t meltfirstargp_,
					     const melt_argdescr_cell_t
					     meltxargdescr_[],
					     union meltparam_un *
					     meltxargtab_,
					     const melt_argdescr_cell_t
					     meltxresdescr_[],
					     union meltparam_un *
					     meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_6_warmelt_outobj_OUTCINITFILL_ROOT_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_6_warmelt_outobj_OUTCINITFILL_ROOT_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 10
    melt_ptr_t mcfr_varptr[10];
#define MELTFRAM_NBVARNUM 3
    long mcfr_varnum[3];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_6_warmelt_outobj_OUTCINITFILL_ROOT is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_6_warmelt_outobj_OUTCINITFILL_ROOT_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 10; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_6_warmelt_outobj_OUTCINITFILL_ROOT nbval 10*/
  meltfram__.mcfr_nbvar = 10 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("OUTCINITFILL_ROOT", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-outobj.melt:112:/ getarg");
 /*_.RECV__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.IMPLBUF__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.IMPLBUF__V3*/ meltfptr[2])) !=
	      NULL);


  /*getarg#2 */
  /*^getarg */
  if (meltxargdescr_[1] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.PTRSTR__V4*/ meltfptr[3] =
    (meltxargtab_[1].meltbp_aptr) ? (*(meltxargtab_[1].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.PTRSTR__V4*/ meltfptr[3])) !=
	      NULL);


  /*getarg#3 */
  /*^getarg */
  if (meltxargdescr_[2] != MELTBPAR_LONG)
    goto lab_endgetargs;
 /*_#DEPTH__L1*/ meltfnum[0] = meltxargtab_[2].meltbp_long;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:113:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L2*/ meltfnum[1] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-outobj.melt:113:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L2*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L3*/ meltfnum[2] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-outobj.melt:113:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L3*/ meltfnum[2];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-outobj.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 113;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "outcinitfill_root recv=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.RECV__V2*/ meltfptr[1];
	      /*_.MELT_DEBUG_FUN__V7*/ meltfptr[6] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V6*/ meltfptr[5] = /*_.MELT_DEBUG_FUN__V7*/ meltfptr[6];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-outobj.melt:113:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L3*/ meltfnum[2] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V7*/ meltfptr[6] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V6*/ meltfptr[5] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-outobj.melt:113:/ quasiblock");


      /*_.PROGN___V8*/ meltfptr[6] = /*_.IF___V6*/ meltfptr[5];;
      /*^compute */
      /*_.IFCPP___V5*/ meltfptr[4] = /*_.PROGN___V8*/ meltfptr[6];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:113:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L2*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IF___V6*/ meltfptr[5] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V8*/ meltfptr[6] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V5*/ meltfptr[4] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:114:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
      /*^cond */
      /*cond */ if (( /*nil */ NULL))	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V10*/ meltfptr[6] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-outobj.melt:114:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("outcinitfill_root unimplemented catchall"), ("warmelt-outobj.melt") ? ("warmelt-outobj.melt") : __FILE__, (114) ? (114) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V10*/ meltfptr[6] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V9*/ meltfptr[5] = /*_.IFELSE___V10*/ meltfptr[6];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:114:/ clear");
	     /*clear *//*_.IFELSE___V10*/ meltfptr[6] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V9*/ meltfptr[5] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-outobj.melt:112:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.IFCPP___V9*/ meltfptr[5];;

    {
      MELT_LOCATION ("warmelt-outobj.melt:112:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V5*/ meltfptr[4] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V9*/ meltfptr[5] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("OUTCINITFILL_ROOT", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_6_warmelt_outobj_OUTCINITFILL_ROOT_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_6_warmelt_outobj_OUTCINITFILL_ROOT */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_7_warmelt_outobj_OUTCINITPREDEF_ROOT (meltclosure_ptr_t meltclosp_,
					       melt_ptr_t meltfirstargp_,
					       const melt_argdescr_cell_t
					       meltxargdescr_[],
					       union meltparam_un *
					       meltxargtab_,
					       const melt_argdescr_cell_t
					       meltxresdescr_[],
					       union meltparam_un *
					       meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_7_warmelt_outobj_OUTCINITPREDEF_ROOT_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_7_warmelt_outobj_OUTCINITPREDEF_ROOT_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 9
    melt_ptr_t mcfr_varptr[9];
#define MELTFRAM_NBVARNUM 3
    long mcfr_varnum[3];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_7_warmelt_outobj_OUTCINITPREDEF_ROOT is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_7_warmelt_outobj_OUTCINITPREDEF_ROOT_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 9; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_7_warmelt_outobj_OUTCINITPREDEF_ROOT nbval 9*/
  meltfram__.mcfr_nbvar = 9 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("OUTCINITPREDEF_ROOT", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-outobj.melt:120:/ getarg");
 /*_.RECV__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.SBUF__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2])) !=
	      NULL);


  /*getarg#2 */
  /*^getarg */
  if (meltxargdescr_[1] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.PTRSTR__V4*/ meltfptr[3] =
    (meltxargtab_[1].meltbp_aptr) ? (*(meltxargtab_[1].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.PTRSTR__V4*/ meltfptr[3])) !=
	      NULL);


  /*getarg#3 */
  /*^getarg */
  if (meltxargdescr_[2] != MELTBPAR_LONG)
    goto lab_endgetargs;
 /*_#DEPTH__L1*/ meltfnum[0] = meltxargtab_[2].meltbp_long;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:121:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L2*/ meltfnum[1] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-outobj.melt:121:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L2*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L3*/ meltfnum[2] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-outobj.melt:121:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L3*/ meltfnum[2];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-outobj.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 121;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "outcinitfill_root recv=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.RECV__V2*/ meltfptr[1];
	      /*_.MELT_DEBUG_FUN__V7*/ meltfptr[6] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V6*/ meltfptr[5] = /*_.MELT_DEBUG_FUN__V7*/ meltfptr[6];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-outobj.melt:121:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L3*/ meltfnum[2] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V7*/ meltfptr[6] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V6*/ meltfptr[5] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-outobj.melt:121:/ quasiblock");


      /*_.PROGN___V8*/ meltfptr[6] = /*_.IF___V6*/ meltfptr[5];;
      /*^compute */
      /*_.IFCPP___V5*/ meltfptr[4] = /*_.PROGN___V8*/ meltfptr[6];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:121:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L2*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IF___V6*/ meltfptr[5] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V8*/ meltfptr[6] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V5*/ meltfptr[4] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-outobj.melt:122:/ quasiblock");


 /*_.RETVAL___V1*/ meltfptr[0] = NULL;;

    {
      MELT_LOCATION ("warmelt-outobj.melt:122:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    MELT_LOCATION ("warmelt-outobj.melt:120:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.RETURN___V9*/ meltfptr[5];;

    {
      MELT_LOCATION ("warmelt-outobj.melt:120:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V5*/ meltfptr[4] = 0;
    /*^clear */
	   /*clear *//*_.RETURN___V9*/ meltfptr[5] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("OUTCINITPREDEF_ROOT", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_7_warmelt_outobj_OUTCINITPREDEF_ROOT_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_7_warmelt_outobj_OUTCINITPREDEF_ROOT */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_8_warmelt_outobj_OUTPUT_PREDEF (meltclosure_ptr_t meltclosp_,
					 melt_ptr_t meltfirstargp_,
					 const melt_argdescr_cell_t
					 meltxargdescr_[],
					 union meltparam_un * meltxargtab_,
					 const melt_argdescr_cell_t
					 meltxresdescr_[],
					 union meltparam_un * meltxrestab_)
{
  long current_blocklevel_signals_meltrout_8_warmelt_outobj_OUTPUT_PREDEF_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_8_warmelt_outobj_OUTPUT_PREDEF_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 23
    melt_ptr_t mcfr_varptr[23];
#define MELTFRAM_NBVARNUM 15
    long mcfr_varnum[15];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_8_warmelt_outobj_OUTPUT_PREDEF is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_8_warmelt_outobj_OUTPUT_PREDEF_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 23; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_8_warmelt_outobj_OUTPUT_PREDEF nbval 23*/
  meltfram__.mcfr_nbvar = 23 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("OUTPUT_PREDEF", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-outobj.melt:127:/ getarg");
 /*_.OBPR__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.IMPLBUF__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.IMPLBUF__V3*/ meltfptr[2])) !=
	      NULL);


  /*getarg#2 */
  /*^getarg */
  if (meltxargdescr_[1] != MELTBPAR_LONG)
    goto lab_endgetargs;
 /*_#DEPTH__L1*/ meltfnum[0] = meltxargtab_[1].meltbp_long;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {

    MELT_LOCATION ("warmelt-outobj.melt:129:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#IS_INTEGERBOX__L2*/ meltfnum[1] =
      (melt_magic_discr ((melt_ptr_t) ( /*_.OBPR__V2*/ meltfptr[1])) ==
       MELTOBMAG_INT);;
    MELT_LOCATION ("warmelt-outobj.melt:129:/ cond");
    /*cond */ if ( /*_#IS_INTEGERBOX__L2*/ meltfnum[1])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{


	  {
	    MELT_LOCATION ("warmelt-outobj.melt:130:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t)
				 ( /*_.IMPLBUF__V3*/ meltfptr[2]),
				 ("melt_fetch_predefined("));
	  }
	  ;
   /*_#GET_INT__L3*/ meltfnum[2] =
	    (melt_get_int ((melt_ptr_t) ( /*_.OBPR__V2*/ meltfptr[1])));;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:131:/ locexp");
	    meltgc_add_strbuf_dec ((melt_ptr_t)
				   ( /*_.IMPLBUF__V3*/ meltfptr[2]),
				   ( /*_#GET_INT__L3*/ meltfnum[2]));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:132:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t)
				 ( /*_.IMPLBUF__V3*/ meltfptr[2]), (")"));
	  }
	  ;
	  MELT_LOCATION ("warmelt-outobj.melt:129:/ quasiblock");


	  /*epilog */

	  /*^clear */
	     /*clear *//*_#GET_INT__L3*/ meltfnum[2] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-outobj.melt:134:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
   /*_#IS_A__L4*/ meltfnum[2] =
	    melt_is_instance_of ((melt_ptr_t) ( /*_.OBPR__V2*/ meltfptr[1]),
				 (melt_ptr_t) (( /*!CLASS_SYMBOL */
						meltfrout->tabval[0])));;
	  MELT_LOCATION ("warmelt-outobj.melt:134:/ cond");
	  /*cond */ if ( /*_#IS_A__L4*/ meltfnum[2])	/*then */
	    {
	      /*^cond.then */
	      /*^block */
	      /*anyblock */
	      {


		{
		  MELT_LOCATION ("warmelt-outobj.melt:135:/ locexp");
		  /*add2sbuf_strconst */
		    meltgc_add_strbuf ((melt_ptr_t)
				       ( /*_.IMPLBUF__V3*/ meltfptr[2]),
				       ("((melt_ptr_t)(MELT_PREDEF("));
		}
		;
		MELT_LOCATION ("warmelt-outobj.melt:136:/ getslot");
		{
		  melt_ptr_t slot = NULL, obj = NULL;
		  obj = (melt_ptr_t) ( /*_.OBPR__V2*/ meltfptr[1]) /*=obj*/ ;
		  melt_object_get_field (slot, obj, 1, "NAMED_NAME");
      /*_.NAMED_NAME__V6*/ meltfptr[5] = slot;
		};
		;

		{
		  /*^locexp */
		  /*add2sbuf_string */
		    meltgc_add_strbuf ((melt_ptr_t)
				       ( /*_.IMPLBUF__V3*/ meltfptr[2]),
				       melt_string_str ((melt_ptr_t)
							( /*_.NAMED_NAME__V6*/
							 meltfptr[5])));
		}
		;

		{
		  MELT_LOCATION ("warmelt-outobj.melt:137:/ locexp");
		  /*add2sbuf_strconst */
		    meltgc_add_strbuf ((melt_ptr_t)
				       ( /*_.IMPLBUF__V3*/ meltfptr[2]),
				       (")))"));
		}
		;
		MELT_LOCATION ("warmelt-outobj.melt:134:/ quasiblock");


		/*epilog */

		/*^clear */
	       /*clear *//*_.NAMED_NAME__V6*/ meltfptr[5] = 0;
	      }
	      ;
	    }
	  else
	    {			/*^cond.else */

	      /*^block */
	      /*anyblock */
	      {


#if MELT_HAVE_DEBUG
		MELT_LOCATION ("warmelt-outobj.melt:140:/ cppif.then");
		/*^block */
		/*anyblock */
		{


		  {
		    /*^locexp */
		    /*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
		    melt_dbgcounter++;
#endif
		    ;
		  }
		  ;
		  /*^checksignal */
		  MELT_CHECK_SIGNAL ();
		  ;
       /*_#MELT_NEED_DBG__L5*/ meltfnum[4] =
		    /*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
		    ( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
		    0		/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
		    ;;
		  MELT_LOCATION ("warmelt-outobj.melt:140:/ cond");
		  /*cond */ if ( /*_#MELT_NEED_DBG__L5*/ meltfnum[4])	/*then */
		    {
		      /*^cond.then */
		      /*^block */
		      /*anyblock */
		      {

	 /*_#THE_MELTCALLCOUNT__L6*/ meltfnum[5] =
#ifdef meltcallcount
			  meltcallcount	/* the_meltcallcount */
#else
			  0L
#endif /* meltcallcount the_meltcallcount */
			  ;;
			MELT_LOCATION
			  ("warmelt-outobj.melt:140:/ checksignal");
			MELT_CHECK_SIGNAL ();
			;
			/*^apply */
			/*apply */
			{
			  union meltparam_un argtab[6];
			  memset (&argtab, 0, sizeof (argtab));
			  /*^apply.arg */
			  argtab[0].meltbp_long =
			    /*_#THE_MELTCALLCOUNT__L6*/ meltfnum[5];
			  /*^apply.arg */
			  argtab[1].meltbp_cstring = "warmelt-outobj.melt";
			  /*^apply.arg */
			  argtab[2].meltbp_long = 140;
			  /*^apply.arg */
			  argtab[3].meltbp_aptr =
			    (melt_ptr_t *) & ( /*!OBPREDEF */ meltfrout->
					      tabval[2]);
			  /*^apply.arg */
			  argtab[4].meltbp_cstring =
			    " output_predef bad obpredef=";
			  /*^apply.arg */
			  argtab[5].meltbp_aptr =
			    (melt_ptr_t *) & ( /*!OBPREDEF */ meltfrout->
					      tabval[2]);
			  /*_.MELT_DEBUG_FUN__V9*/ meltfptr[8] =
			    melt_apply ((meltclosure_ptr_t)
					(( /*!MELT_DEBUG_FUN */ meltfrout->
					  tabval[1])),
					(melt_ptr_t) (( /*nil */ NULL)),
					(MELTBPARSTR_LONG MELTBPARSTR_CSTRING
					 MELTBPARSTR_LONG MELTBPARSTR_PTR
					 MELTBPARSTR_CSTRING MELTBPARSTR_PTR
					 ""), argtab, "",
					(union meltparam_un *) 0);
			}
			;
			/*_.IF___V8*/ meltfptr[7] =
			  /*_.MELT_DEBUG_FUN__V9*/ meltfptr[8];;
			/*epilog */

			MELT_LOCATION ("warmelt-outobj.melt:140:/ clear");
		   /*clear *//*_#THE_MELTCALLCOUNT__L6*/ meltfnum[5] = 0;
			/*^clear */
		   /*clear *//*_.MELT_DEBUG_FUN__V9*/ meltfptr[8] = 0;
		      }
		      ;
		    }
		  else
		    {		/*^cond.else */

	/*_.IF___V8*/ meltfptr[7] = NULL;;
		    }
		  ;
		  MELT_LOCATION ("warmelt-outobj.melt:140:/ quasiblock");


		  /*_.PROGN___V10*/ meltfptr[8] = /*_.IF___V8*/ meltfptr[7];;
		  /*^compute */
		  /*_.IFCPP___V7*/ meltfptr[5] =
		    /*_.PROGN___V10*/ meltfptr[8];;
		  /*epilog */

		  MELT_LOCATION ("warmelt-outobj.melt:140:/ clear");
		 /*clear *//*_#MELT_NEED_DBG__L5*/ meltfnum[4] = 0;
		  /*^clear */
		 /*clear *//*_.IF___V8*/ meltfptr[7] = 0;
		  /*^clear */
		 /*clear *//*_.PROGN___V10*/ meltfptr[8] = 0;
		}

#else /*MELT_HAVE_DEBUG */
		/*^cppif.else */
		/*_.IFCPP___V7*/ meltfptr[5] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
		;

#if MELT_HAVE_DEBUG
		MELT_LOCATION ("warmelt-outobj.melt:141:/ cppif.then");
		/*^block */
		/*anyblock */
		{

		  /*^checksignal */
		  MELT_CHECK_SIGNAL ();
		  ;
		  /*^cond */
		  /*cond */ if (( /*nil */ NULL))	/*then */
		    {
		      /*^cond.then */
		      /*_.IFELSE___V12*/ meltfptr[8] = ( /*nil */ NULL);;
		    }
		  else
		    {
		      MELT_LOCATION ("warmelt-outobj.melt:141:/ cond.else");

		      /*^block */
		      /*anyblock */
		      {




			{
			  /*^locexp */
			  melt_assert_failed (("invalid obpredef"),
					      ("warmelt-outobj.melt")
					      ? ("warmelt-outobj.melt") :
					      __FILE__,
					      (141) ? (141) : __LINE__,
					      __FUNCTION__);
			  ;
			}
			;
		   /*clear *//*_.IFELSE___V12*/ meltfptr[8] = 0;
			/*epilog */
		      }
		      ;
		    }
		  ;
		  /*^compute */
		  /*_.IFCPP___V11*/ meltfptr[7] =
		    /*_.IFELSE___V12*/ meltfptr[8];;
		  /*epilog */

		  MELT_LOCATION ("warmelt-outobj.melt:141:/ clear");
		 /*clear *//*_.IFELSE___V12*/ meltfptr[8] = 0;
		}

#else /*MELT_HAVE_DEBUG */
		/*^cppif.else */
		/*_.IFCPP___V11*/ meltfptr[7] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
		;
		MELT_LOCATION ("warmelt-outobj.melt:139:/ quasiblock");


		/*_.PROGN___V13*/ meltfptr[8] =
		  /*_.IFCPP___V11*/ meltfptr[7];;
		/*^compute */
		/*_.IFELSE___V5*/ meltfptr[4] =
		  /*_.PROGN___V13*/ meltfptr[8];;
		/*epilog */

		MELT_LOCATION ("warmelt-outobj.melt:134:/ clear");
	       /*clear *//*_.IFCPP___V7*/ meltfptr[5] = 0;
		/*^clear */
	       /*clear *//*_.IFCPP___V11*/ meltfptr[7] = 0;
		/*^clear */
	       /*clear *//*_.PROGN___V13*/ meltfptr[8] = 0;
	      }
	      ;
	    }
	  ;
	  /*_.IFELSE___V4*/ meltfptr[3] = /*_.IFELSE___V5*/ meltfptr[4];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-outobj.melt:129:/ clear");
	     /*clear *//*_#IS_A__L4*/ meltfnum[2] = 0;
	  /*^clear */
	     /*clear *//*_.IFELSE___V5*/ meltfptr[4] = 0;
	}
	;
      }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:145:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#STRBUF_USEDLENGTH__L7*/ meltfnum[5] =
      melt_strbuf_usedlength ((melt_ptr_t) ( /*_.IMPLBUF__V3*/ meltfptr[2]));;
    MELT_LOCATION ("warmelt-outobj.melt:145:/ cond");
    /*cond */ if (
		   /*ifisa */
		   melt_is_instance_of ((melt_ptr_t)
					(( /*!BUFFER_LIMIT_CONT */ meltfrout->
					  tabval[3])),
					(melt_ptr_t) (( /*!CLASS_REFERENCE */
						       meltfrout->tabval[4])))
      )				/*then */
      {
	/*^cond.then */
	/*^getslot */
	{
	  melt_ptr_t slot = NULL, obj = NULL;
	  obj =
	    (melt_ptr_t) (( /*!BUFFER_LIMIT_CONT */ meltfrout->
			   tabval[3])) /*=obj*/ ;
	  melt_object_get_field (slot, obj, 0, "REFERENCED_VALUE");
   /*_.REFERENCED_VALUE__V14*/ meltfptr[5] = slot;
	};
	;
      }
    else
      {				/*^cond.else */

  /*_.REFERENCED_VALUE__V14*/ meltfptr[5] = NULL;;
      }
    ;
    /*^compute */
 /*_#GET_INT__L8*/ meltfnum[4] =
      (melt_get_int
       ((melt_ptr_t) ( /*_.REFERENCED_VALUE__V14*/ meltfptr[5])));;
    /*^compute */
 /*_#IRAW__L9*/ meltfnum[2] =
      (( /*_#GET_INT__L8*/ meltfnum[4]) / (2));;
    /*^compute */
 /*_#I__L10*/ meltfnum[9] =
      (( /*_#STRBUF_USEDLENGTH__L7*/ meltfnum[5]) >
       ( /*_#IRAW__L9*/ meltfnum[2]));;
    MELT_LOCATION ("warmelt-outobj.melt:145:/ cond");
    /*cond */ if ( /*_#I__L10*/ meltfnum[9])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{


	  {
	    MELT_LOCATION ("warmelt-outobj.melt:146:/ locexp");

#if MELT_HAVE_DEBUG
	    if (melt_need_debug (0))
	      melt_dbgshortbacktrace (("output_predef huge implbuf"), (10));
#endif
	    ;
	  }
	  ;

#if MELT_HAVE_DEBUG
	  MELT_LOCATION ("warmelt-outobj.melt:147:/ cppif.then");
	  /*^block */
	  /*anyblock */
	  {


	    {
	      /*^locexp */
	      /*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	      melt_dbgcounter++;
#endif
	      ;
	    }
	    ;
	    /*^checksignal */
	    MELT_CHECK_SIGNAL ();
	    ;
     /*_#MELT_NEED_DBG__L11*/ meltfnum[10] =
	      /*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	      ( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	      0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	      ;;
	    MELT_LOCATION ("warmelt-outobj.melt:147:/ cond");
	    /*cond */ if ( /*_#MELT_NEED_DBG__L11*/ meltfnum[10])	/*then */
	      {
		/*^cond.then */
		/*^block */
		/*anyblock */
		{

       /*_#THE_MELTCALLCOUNT__L12*/ meltfnum[11] =
#ifdef meltcallcount
		    meltcallcount	/* the_meltcallcount */
#else
		    0L
#endif /* meltcallcount the_meltcallcount */
		    ;;
		  MELT_LOCATION ("warmelt-outobj.melt:147:/ checksignal");
		  MELT_CHECK_SIGNAL ();
		  ;
		  /*^apply */
		  /*apply */
		  {
		    union meltparam_un argtab[5];
		    memset (&argtab, 0, sizeof (argtab));
		    /*^apply.arg */
		    argtab[0].meltbp_long =
		      /*_#THE_MELTCALLCOUNT__L12*/ meltfnum[11];
		    /*^apply.arg */
		    argtab[1].meltbp_cstring = "warmelt-outobj.melt";
		    /*^apply.arg */
		    argtab[2].meltbp_long = 147;
		    /*^apply.arg */
		    argtab[3].meltbp_cstring = "output_predef huge implbuf=";
		    /*^apply.arg */
		    argtab[4].meltbp_aptr =
		      (melt_ptr_t *) & /*_.IMPLBUF__V3*/ meltfptr[2];
		    /*_.MELT_DEBUG_FUN__V18*/ meltfptr[17] =
		      melt_apply ((meltclosure_ptr_t)
				  (( /*!MELT_DEBUG_FUN */ meltfrout->
				    tabval[1])),
				  (melt_ptr_t) (( /*nil */ NULL)),
				  (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_PTR ""), argtab, "",
				  (union meltparam_un *) 0);
		  }
		  ;
		  /*_.IF___V17*/ meltfptr[4] =
		    /*_.MELT_DEBUG_FUN__V18*/ meltfptr[17];;
		  /*epilog */

		  MELT_LOCATION ("warmelt-outobj.melt:147:/ clear");
		 /*clear *//*_#THE_MELTCALLCOUNT__L12*/ meltfnum[11] = 0;
		  /*^clear */
		 /*clear *//*_.MELT_DEBUG_FUN__V18*/ meltfptr[17] = 0;
		}
		;
	      }
	    else
	      {			/*^cond.else */

      /*_.IF___V17*/ meltfptr[4] = NULL;;
	      }
	    ;
	    MELT_LOCATION ("warmelt-outobj.melt:147:/ quasiblock");


	    /*_.PROGN___V19*/ meltfptr[17] = /*_.IF___V17*/ meltfptr[4];;
	    /*^compute */
	    /*_.IFCPP___V16*/ meltfptr[8] = /*_.PROGN___V19*/ meltfptr[17];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-outobj.melt:147:/ clear");
	       /*clear *//*_#MELT_NEED_DBG__L11*/ meltfnum[10] = 0;
	    /*^clear */
	       /*clear *//*_.IF___V17*/ meltfptr[4] = 0;
	    /*^clear */
	       /*clear *//*_.PROGN___V19*/ meltfptr[17] = 0;
	  }

#else /*MELT_HAVE_DEBUG */
	  /*^cppif.else */
	  /*_.IFCPP___V16*/ meltfptr[8] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
	  ;

#if MELT_HAVE_DEBUG
	  MELT_LOCATION ("warmelt-outobj.melt:148:/ cppif.then");
	  /*^block */
	  /*anyblock */
	  {

	    /*^checksignal */
	    MELT_CHECK_SIGNAL ();
	    ;
     /*_#STRBUF_USEDLENGTH__L13*/ meltfnum[11] =
	      melt_strbuf_usedlength ((melt_ptr_t)
				      ( /*_.IMPLBUF__V3*/ meltfptr[2]));;
	    MELT_LOCATION ("warmelt-outobj.melt:149:/ cond");
	    /*cond */ if (
			   /*ifisa */
			   melt_is_instance_of ((melt_ptr_t)
						(( /*!BUFFER_LIMIT_CONT */
						  meltfrout->tabval[3])),
						(melt_ptr_t) (( /*!CLASS_REFERENCE */ meltfrout->tabval[4])))
	      )			/*then */
	      {
		/*^cond.then */
		/*^getslot */
		{
		  melt_ptr_t slot = NULL, obj = NULL;
		  obj =
		    (melt_ptr_t) (( /*!BUFFER_LIMIT_CONT */ meltfrout->
				   tabval[3])) /*=obj*/ ;
		  melt_object_get_field (slot, obj, 0, "REFERENCED_VALUE");
       /*_.REFERENCED_VALUE__V21*/ meltfptr[17] = slot;
		};
		;
	      }
	    else
	      {			/*^cond.else */

      /*_.REFERENCED_VALUE__V21*/ meltfptr[17] = NULL;;
	      }
	    ;
	    /*^compute */
     /*_#GET_INT__L14*/ meltfnum[10] =
	      (melt_get_int
	       ((melt_ptr_t) ( /*_.REFERENCED_VALUE__V21*/ meltfptr[17])));;
	    /*^compute */
     /*_#I__L15*/ meltfnum[14] =
	      (( /*_#STRBUF_USEDLENGTH__L13*/ meltfnum[11]) <
	       ( /*_#GET_INT__L14*/ meltfnum[10]));;
	    MELT_LOCATION ("warmelt-outobj.melt:148:/ cond");
	    /*cond */ if ( /*_#I__L15*/ meltfnum[14])	/*then */
	      {
		/*^cond.then */
		/*_.IFELSE___V22*/ meltfptr[21] = ( /*nil */ NULL);;
	      }
	    else
	      {
		MELT_LOCATION ("warmelt-outobj.melt:148:/ cond.else");

		/*^block */
		/*anyblock */
		{




		  {
		    /*^locexp */
		    melt_assert_failed (("check limited implbuf"),
					("warmelt-outobj.melt")
					? ("warmelt-outobj.melt") : __FILE__,
					(148) ? (148) : __LINE__,
					__FUNCTION__);
		    ;
		  }
		  ;
		 /*clear *//*_.IFELSE___V22*/ meltfptr[21] = 0;
		  /*epilog */
		}
		;
	      }
	    ;
	    /*^compute */
	    /*_.IFCPP___V20*/ meltfptr[4] = /*_.IFELSE___V22*/ meltfptr[21];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-outobj.melt:148:/ clear");
	       /*clear *//*_#STRBUF_USEDLENGTH__L13*/ meltfnum[11] = 0;
	    /*^clear */
	       /*clear *//*_.REFERENCED_VALUE__V21*/ meltfptr[17] = 0;
	    /*^clear */
	       /*clear *//*_#GET_INT__L14*/ meltfnum[10] = 0;
	    /*^clear */
	       /*clear *//*_#I__L15*/ meltfnum[14] = 0;
	    /*^clear */
	       /*clear *//*_.IFELSE___V22*/ meltfptr[21] = 0;
	  }

#else /*MELT_HAVE_DEBUG */
	  /*^cppif.else */
	  /*_.IFCPP___V20*/ meltfptr[4] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
	  ;
	  MELT_LOCATION ("warmelt-outobj.melt:145:/ quasiblock");


	  /*_.PROGN___V23*/ meltfptr[17] = /*_.IFCPP___V20*/ meltfptr[4];;
	  /*^compute */
	  /*_.IFELSE___V15*/ meltfptr[7] = /*_.PROGN___V23*/ meltfptr[17];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-outobj.melt:145:/ clear");
	     /*clear *//*_.IFCPP___V16*/ meltfptr[8] = 0;
	  /*^clear */
	     /*clear *//*_.IFCPP___V20*/ meltfptr[4] = 0;
	  /*^clear */
	     /*clear *//*_.PROGN___V23*/ meltfptr[17] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

  /*_.IFELSE___V15*/ meltfptr[7] = NULL;;
      }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:127:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.IFELSE___V15*/ meltfptr[7];;

    {
      MELT_LOCATION ("warmelt-outobj.melt:127:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_#IS_INTEGERBOX__L2*/ meltfnum[1] = 0;
    /*^clear */
	   /*clear *//*_.IFELSE___V4*/ meltfptr[3] = 0;
    /*^clear */
	   /*clear *//*_#STRBUF_USEDLENGTH__L7*/ meltfnum[5] = 0;
    /*^clear */
	   /*clear *//*_.REFERENCED_VALUE__V14*/ meltfptr[5] = 0;
    /*^clear */
	   /*clear *//*_#GET_INT__L8*/ meltfnum[4] = 0;
    /*^clear */
	   /*clear *//*_#IRAW__L9*/ meltfnum[2] = 0;
    /*^clear */
	   /*clear *//*_#I__L10*/ meltfnum[9] = 0;
    /*^clear */
	   /*clear *//*_.IFELSE___V15*/ meltfptr[7] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("OUTPUT_PREDEF", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_8_warmelt_outobj_OUTPUT_PREDEF_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_8_warmelt_outobj_OUTPUT_PREDEF */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_9_warmelt_outobj_OUTPUCOD_PREDEF (meltclosure_ptr_t meltclosp_,
					   melt_ptr_t meltfirstargp_,
					   const melt_argdescr_cell_t
					   meltxargdescr_[],
					   union meltparam_un * meltxargtab_,
					   const melt_argdescr_cell_t
					   meltxresdescr_[],
					   union meltparam_un * meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_9_warmelt_outobj_OUTPUCOD_PREDEF_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_9_warmelt_outobj_OUTPUCOD_PREDEF_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 9
    melt_ptr_t mcfr_varptr[9];
#define MELTFRAM_NBVARNUM 2
    long mcfr_varnum[2];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_9_warmelt_outobj_OUTPUCOD_PREDEF is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_9_warmelt_outobj_OUTPUCOD_PREDEF_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 9; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_9_warmelt_outobj_OUTPUCOD_PREDEF nbval 9*/
  meltfram__.mcfr_nbvar = 9 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("OUTPUCOD_PREDEF", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-outobj.melt:153:/ getarg");
 /*_.OBPRED__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.DECLBUF__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.DECLBUF__V3*/ meltfptr[2])) !=
	      NULL);


  /*getarg#2 */
  /*^getarg */
  if (meltxargdescr_[1] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.IMPLBUF__V4*/ meltfptr[3] =
    (meltxargtab_[1].meltbp_aptr) ? (*(meltxargtab_[1].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3])) !=
	      NULL);


  /*getarg#3 */
  /*^getarg */
  if (meltxargdescr_[2] != MELTBPAR_LONG)
    goto lab_endgetargs;
 /*_#DEPTH__L1*/ meltfnum[0] = meltxargtab_[2].meltbp_long;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:154:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L2*/ meltfnum[1] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.OBPRED__V2*/ meltfptr[1]),
			     (melt_ptr_t) (( /*!CLASS_OBJPREDEF */ meltfrout->
					    tabval[0])));;
      MELT_LOCATION ("warmelt-outobj.melt:154:/ cond");
      /*cond */ if ( /*_#IS_A__L2*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V6*/ meltfptr[5] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-outobj.melt:154:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check obpredef"),
				  ("warmelt-outobj.melt")
				  ? ("warmelt-outobj.melt") : __FILE__,
				  (154) ? (154) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V6*/ meltfptr[5] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V5*/ meltfptr[4] = /*_.IFELSE___V6*/ meltfptr[5];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:154:/ clear");
	     /*clear *//*_#IS_A__L2*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V6*/ meltfptr[5] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V5*/ meltfptr[4] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-outobj.melt:155:/ quasiblock");


    /*^getslot */
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.OBPRED__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 1, "OBPREDEF");
  /*_.OBPR__V8*/ meltfptr[7] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-outobj.melt:156:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[2];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.IMPLBUF__V4*/ meltfptr[3];
      /*^apply.arg */
      argtab[1].meltbp_long = /*_#DEPTH__L1*/ meltfnum[0];
      /*_.OUTPUT_PREDEF__V9*/ meltfptr[8] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!OUTPUT_PREDEF */ meltfrout->tabval[1])),
		    (melt_ptr_t) ( /*_.OBPR__V8*/ meltfptr[7]),
		    (MELTBPARSTR_PTR MELTBPARSTR_LONG ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;
    /*_.LET___V7*/ meltfptr[5] = /*_.OUTPUT_PREDEF__V9*/ meltfptr[8];;

    MELT_LOCATION ("warmelt-outobj.melt:155:/ clear");
	   /*clear *//*_.OBPR__V8*/ meltfptr[7] = 0;
    /*^clear */
	   /*clear *//*_.OUTPUT_PREDEF__V9*/ meltfptr[8] = 0;
    MELT_LOCATION ("warmelt-outobj.melt:153:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.LET___V7*/ meltfptr[5];;

    {
      MELT_LOCATION ("warmelt-outobj.melt:153:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V5*/ meltfptr[4] = 0;
    /*^clear */
	   /*clear *//*_.LET___V7*/ meltfptr[5] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("OUTPUCOD_PREDEF", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_9_warmelt_outobj_OUTPUCOD_PREDEF_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_9_warmelt_outobj_OUTPUCOD_PREDEF */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_10_warmelt_outobj_OUTPUCOD_NIL (meltclosure_ptr_t meltclosp_,
					 melt_ptr_t meltfirstargp_,
					 const melt_argdescr_cell_t
					 meltxargdescr_[],
					 union meltparam_un * meltxargtab_,
					 const melt_argdescr_cell_t
					 meltxresdescr_[],
					 union meltparam_un * meltxrestab_)
{
  long current_blocklevel_signals_meltrout_10_warmelt_outobj_OUTPUCOD_NIL_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_10_warmelt_outobj_OUTPUCOD_NIL_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 6
    melt_ptr_t mcfr_varptr[6];
#define MELTFRAM_NBVARNUM 2
    long mcfr_varnum[2];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_10_warmelt_outobj_OUTPUCOD_NIL is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_10_warmelt_outobj_OUTPUCOD_NIL_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 6; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_10_warmelt_outobj_OUTPUCOD_NIL nbval 6*/
  meltfram__.mcfr_nbvar = 6 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("OUTPUCOD_NIL", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-outobj.melt:160:/ getarg");
 /*_.OBNIL__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.DECLBUF__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.DECLBUF__V3*/ meltfptr[2])) !=
	      NULL);


  /*getarg#2 */
  /*^getarg */
  if (meltxargdescr_[1] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.IMPLBUF__V4*/ meltfptr[3] =
    (meltxargtab_[1].meltbp_aptr) ? (*(meltxargtab_[1].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3])) !=
	      NULL);


  /*getarg#3 */
  /*^getarg */
  if (meltxargdescr_[2] != MELTBPAR_LONG)
    goto lab_endgetargs;
 /*_#DEPTH__L1*/ meltfnum[0] = meltxargtab_[2].meltbp_long;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:161:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L2*/ meltfnum[1] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.OBNIL__V2*/ meltfptr[1]),
			     (melt_ptr_t) (( /*!CLASS_OBJNIL */ meltfrout->
					    tabval[0])));;
      MELT_LOCATION ("warmelt-outobj.melt:161:/ cond");
      /*cond */ if ( /*_#IS_A__L2*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V6*/ meltfptr[5] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-outobj.melt:161:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check obnil"),
				  ("warmelt-outobj.melt")
				  ? ("warmelt-outobj.melt") : __FILE__,
				  (161) ? (161) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V6*/ meltfptr[5] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V5*/ meltfptr[4] = /*_.IFELSE___V6*/ meltfptr[5];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:161:/ clear");
	     /*clear *//*_#IS_A__L2*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V6*/ meltfptr[5] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V5*/ meltfptr[4] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:162:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   ("(/*nil*/NULL)"));
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:160:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V5*/ meltfptr[4] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("OUTPUCOD_NIL", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_10_warmelt_outobj_OUTPUCOD_NIL_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*noretval */ NULL);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_10_warmelt_outobj_OUTPUCOD_NIL */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_11_warmelt_outobj_OUTPUCOD_OBJEXPANDPUREVAL (meltclosure_ptr_t
						      meltclosp_,
						      melt_ptr_t
						      meltfirstargp_,
						      const
						      melt_argdescr_cell_t
						      meltxargdescr_[],
						      union meltparam_un *
						      meltxargtab_,
						      const
						      melt_argdescr_cell_t
						      meltxresdescr_[],
						      union meltparam_un *
						      meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_11_warmelt_outobj_OUTPUCOD_OBJEXPANDPUREVAL_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_11_warmelt_outobj_OUTPUCOD_OBJEXPANDPUREVAL_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 39
    melt_ptr_t mcfr_varptr[39];
#define MELTFRAM_NBVARNUM 15
    long mcfr_varnum[15];
/*others*/
    const char *loc_CSTRING__o0;
    const char *loc_CSTRING__o1;
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_11_warmelt_outobj_OUTPUCOD_OBJEXPANDPUREVAL is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_11_warmelt_outobj_OUTPUCOD_OBJEXPANDPUREVAL_st
	 *) meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 39; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_11_warmelt_outobj_OUTPUCOD_OBJEXPANDPUREVAL nbval 39*/
  meltfram__.mcfr_nbvar = 39 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("OUTPUCOD_OBJEXPANDPUREVAL", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-outobj.melt:166:/ getarg");
 /*_.OBX__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.DECLBUF__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.DECLBUF__V3*/ meltfptr[2])) !=
	      NULL);


  /*getarg#2 */
  /*^getarg */
  if (meltxargdescr_[1] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.IMPLBUF__V4*/ meltfptr[3] =
    (meltxargtab_[1].meltbp_aptr) ? (*(meltxargtab_[1].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3])) !=
	      NULL);


  /*getarg#3 */
  /*^getarg */
  if (meltxargdescr_[2] != MELTBPAR_LONG)
    goto lab_endgetargs;
 /*_#DEPTH__L1*/ meltfnum[0] = meltxargtab_[2].meltbp_long;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:167:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L2*/ meltfnum[1] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-outobj.melt:167:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L2*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L3*/ meltfnum[2] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-outobj.melt:167:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L3*/ meltfnum[2];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-outobj.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 167;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "outpucod_objexpandpureval obx=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.OBX__V2*/ meltfptr[1];
	      /*_.MELT_DEBUG_FUN__V7*/ meltfptr[6] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V6*/ meltfptr[5] = /*_.MELT_DEBUG_FUN__V7*/ meltfptr[6];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-outobj.melt:167:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L3*/ meltfnum[2] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V7*/ meltfptr[6] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V6*/ meltfptr[5] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-outobj.melt:167:/ quasiblock");


      /*_.PROGN___V8*/ meltfptr[6] = /*_.IF___V6*/ meltfptr[5];;
      /*^compute */
      /*_.IFCPP___V5*/ meltfptr[4] = /*_.PROGN___V8*/ meltfptr[6];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:167:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L2*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IF___V6*/ meltfptr[5] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V8*/ meltfptr[6] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V5*/ meltfptr[4] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:168:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L4*/ meltfnum[2] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.OBX__V2*/ meltfptr[1]),
			     (melt_ptr_t) (( /*!CLASS_OBJEXPANDPUREVAL */
					    meltfrout->tabval[1])));;
      MELT_LOCATION ("warmelt-outobj.melt:168:/ cond");
      /*cond */ if ( /*_#IS_A__L4*/ meltfnum[2])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V10*/ meltfptr[6] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-outobj.melt:168:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check obx"),
				  ("warmelt-outobj.melt")
				  ? ("warmelt-outobj.melt") : __FILE__,
				  (168) ? (168) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V10*/ meltfptr[6] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V9*/ meltfptr[5] = /*_.IFELSE___V10*/ meltfptr[6];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:168:/ clear");
	     /*clear *//*_#IS_A__L4*/ meltfnum[2] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V10*/ meltfptr[6] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V9*/ meltfptr[5] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-outobj.melt:169:/ quasiblock");


    /*^cond */
    /*cond */ if (
		   /*ifisa */
		   melt_is_instance_of ((melt_ptr_t)
					( /*_.OBX__V2*/ meltfptr[1]),
					(melt_ptr_t) (( /*!CLASS_OBJEXPANDPUREVAL */ meltfrout->tabval[1])))
      )				/*then */
      {
	/*^cond.then */
	/*^getslot */
	{
	  melt_ptr_t slot = NULL, obj = NULL;
	  obj = (melt_ptr_t) ( /*_.OBX__V2*/ meltfptr[1]) /*=obj*/ ;
	  melt_object_get_field (slot, obj, 1, "OXPURVAL_LOC");
   /*_.OLOC__V12*/ meltfptr[11] = slot;
	};
	;
      }
    else
      {				/*^cond.else */

  /*_.OLOC__V12*/ meltfptr[11] = NULL;;
      }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:170:/ cond");
    /*cond */ if (
		   /*ifisa */
		   melt_is_instance_of ((melt_ptr_t)
					( /*_.OBX__V2*/ meltfptr[1]),
					(melt_ptr_t) (( /*!CLASS_OBJEXPANDPUREVAL */ meltfrout->tabval[1])))
      )				/*then */
      {
	/*^cond.then */
	/*^getslot */
	{
	  melt_ptr_t slot = NULL, obj = NULL;
	  obj = (melt_ptr_t) ( /*_.OBX__V2*/ meltfptr[1]) /*=obj*/ ;
	  melt_object_get_field (slot, obj, 2, "OXPURVAL_COMM");
   /*_.OCOMM__V13*/ meltfptr[12] = slot;
	};
	;
      }
    else
      {				/*^cond.else */

  /*_.OCOMM__V13*/ meltfptr[12] = NULL;;
      }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:171:/ cond");
    /*cond */ if (
		   /*ifisa */
		   melt_is_instance_of ((melt_ptr_t)
					( /*_.OBX__V2*/ meltfptr[1]),
					(melt_ptr_t) (( /*!CLASS_OBJEXPANDPUREVAL */ meltfrout->tabval[1])))
      )				/*then */
      {
	/*^cond.then */
	/*^getslot */
	{
	  melt_ptr_t slot = NULL, obj = NULL;
	  obj = (melt_ptr_t) ( /*_.OBX__V2*/ meltfptr[1]) /*=obj*/ ;
	  melt_object_get_field (slot, obj, 3, "OXPURVAL_CONT");
   /*_.OCONT__V14*/ meltfptr[13] = slot;
	};
	;
      }
    else
      {				/*^cond.else */

  /*_.OCONT__V14*/ meltfptr[13] = NULL;;
      }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:172:/ cond");
    /*cond */ if (
		   /*ifisa */
		   melt_is_instance_of ((melt_ptr_t)
					( /*_.OBX__V2*/ meltfptr[1]),
					(melt_ptr_t) (( /*!CLASS_OBJVALUE */
						       meltfrout->tabval[2])))
      )				/*then */
      {
	/*^cond.then */
	/*^getslot */
	{
	  melt_ptr_t slot = NULL, obj = NULL;
	  obj = (melt_ptr_t) ( /*_.OBX__V2*/ meltfptr[1]) /*=obj*/ ;
	  melt_object_get_field (slot, obj, 0, "OBV_TYPE");
   /*_.OTYP__V15*/ meltfptr[14] = slot;
	};
	;
      }
    else
      {				/*^cond.else */

  /*_.OTYP__V15*/ meltfptr[14] = NULL;;
      }
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:174:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_MULTIPLE_OR_NULL__L5*/ meltfnum[1] =
	(( /*_.OCONT__V14*/ meltfptr[13]) == NULL
	 ||
	 (melt_unsafe_magic_discr
	  ((melt_ptr_t) ( /*_.OCONT__V14*/ meltfptr[13])) ==
	  MELTOBMAG_MULTIPLE));;
      MELT_LOCATION ("warmelt-outobj.melt:174:/ cond");
      /*cond */ if ( /*_#IS_MULTIPLE_OR_NULL__L5*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V17*/ meltfptr[16] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-outobj.melt:174:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check ocont"),
				  ("warmelt-outobj.melt")
				  ? ("warmelt-outobj.melt") : __FILE__,
				  (174) ? (174) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V17*/ meltfptr[16] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V16*/ meltfptr[15] = /*_.IFELSE___V17*/ meltfptr[16];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:174:/ clear");
	     /*clear *//*_#IS_MULTIPLE_OR_NULL__L5*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V17*/ meltfptr[16] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V16*/ meltfptr[15] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:175:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L6*/ meltfnum[2] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.OTYP__V15*/ meltfptr[14]),
			     (melt_ptr_t) (( /*!CLASS_CTYPE */ meltfrout->
					    tabval[3])));;
      MELT_LOCATION ("warmelt-outobj.melt:175:/ cond");
      /*cond */ if ( /*_#IS_A__L6*/ meltfnum[2])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V19*/ meltfptr[18] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-outobj.melt:175:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check otyp"),
				  ("warmelt-outobj.melt")
				  ? ("warmelt-outobj.melt") : __FILE__,
				  (175) ? (175) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V19*/ meltfptr[18] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V18*/ meltfptr[16] = /*_.IFELSE___V19*/ meltfptr[18];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:175:/ clear");
	     /*clear *//*_#IS_A__L6*/ meltfnum[2] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V19*/ meltfptr[18] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V18*/ meltfptr[16] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-outobj.melt:177:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#__L7*/ meltfnum[1] =
      (( /*_.OTYP__V15*/ meltfptr[14]) ==
       (( /*!CTYPE_VOID */ meltfrout->tabval[4])));;
    MELT_LOCATION ("warmelt-outobj.melt:177:/ cond");
    /*cond */ if ( /*_#__L7*/ meltfnum[1])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{


	  {
	    MELT_LOCATION ("warmelt-outobj.melt:178:/ locexp");

#if MELT_HAVE_DEBUG
	    if (melt_need_debug (0))
	      melt_dbgshortbacktrace (("outpucod_objexpandpureval pure void"),
				      (15));
#endif
	    ;
	  }
	  ;
	  MELT_LOCATION ("warmelt-outobj.melt:179:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^cond */
	  /*cond */ if ( /*_.OLOC__V12*/ meltfptr[11])	/*then */
	    {
	      /*^cond.then */
	      /*^block */
	      /*anyblock */
	      {

		MELT_LOCATION ("warmelt-outobj.melt:180:/ quasiblock");


     /*_?*/ meltfram__.loc_CSTRING__o0 =
		  (char *) 0;;

		{
		  MELT_LOCATION ("warmelt-outobj.melt:182:/ locexp");
		  melt_warning_str (0,
				    (melt_ptr_t) ( /*_.OLOC__V12*/
						  meltfptr[11]),
				    ("strange pure value with :void ctype"),
				    (melt_ptr_t) 0);
		}
		;

		{
		  MELT_LOCATION ("warmelt-outobj.melt:185:/ locexp");
		  /* outpucod_objexpandpureval void FILLCOMSTR_CHK__1 */
		  static char buf_FILLCOMSTR_CHK__1[80];
		  const char *comstr_FILLCOMSTR_CHK__1 =
		    melt_string_str ((melt_ptr_t) /*_.OCOMM__V13*/
				     meltfptr[12]);
		  if (comstr_FILLCOMSTR_CHK__1)
		    snprintf (buf_FILLCOMSTR_CHK__1,
			      sizeof (buf_FILLCOMSTR_CHK__1),
			      "xpurvalvoid: %s", comstr_FILLCOMSTR_CHK__1);
		  else
		    snprintf (buf_FILLCOMSTR_CHK__1,
			      sizeof (buf_FILLCOMSTR_CHK__1),
			      "[xpurvalvoid/%lx]",
			      melt_obj_hash ((melt_ptr_t) /*_.OBX__V2*/
					     meltfptr[1]));
	     /*_?*/ meltfram__.loc_CSTRING__o0 = buf_FILLCOMSTR_CHK__1;
		  /* outpucod_objexpandpureval void end FILLCOMSTR_CHK__1 */
		  ;
		}
		;
		MELT_LOCATION ("warmelt-outobj.melt:200:/ checksignal");
		MELT_CHECK_SIGNAL ();
		;
		/*^apply */
		/*apply */
		{
		  union meltparam_un argtab[3];
		  memset (&argtab, 0, sizeof (argtab));
		  /*^apply.arg */
		  argtab[0].meltbp_aptr =
		    (melt_ptr_t *) & /*_.IMPLBUF__V4*/ meltfptr[3];
		  /*^apply.arg */
		  argtab[1].meltbp_long = /*_#DEPTH__L1*/ meltfnum[0];
		  /*^apply.arg */
		  argtab[2].meltbp_cstring =
		    /*_?*/ meltfram__.loc_CSTRING__o0;
		  /*_.OUTPUT_LOCATION__V23*/ meltfptr[22] =
		    melt_apply ((meltclosure_ptr_t)
				(( /*!OUTPUT_LOCATION */ meltfrout->
				  tabval[5])),
				(melt_ptr_t) ( /*_.OLOC__V12*/ meltfptr[11]),
				(MELTBPARSTR_PTR MELTBPARSTR_LONG
				 MELTBPARSTR_CSTRING ""), argtab, "",
				(union meltparam_un *) 0);
		}
		;
		/*_.LET___V22*/ meltfptr[21] =
		  /*_.OUTPUT_LOCATION__V23*/ meltfptr[22];;

		MELT_LOCATION ("warmelt-outobj.melt:180:/ clear");
	       /*clear *//*_?*/ meltfram__.loc_CSTRING__o0 = 0;
		/*^clear */
	       /*clear *//*_.OUTPUT_LOCATION__V23*/ meltfptr[22] = 0;
		/*_.IFELSE___V21*/ meltfptr[20] =
		  /*_.LET___V22*/ meltfptr[21];;
		/*epilog */

		MELT_LOCATION ("warmelt-outobj.melt:179:/ clear");
	       /*clear *//*_.LET___V22*/ meltfptr[21] = 0;
	      }
	      ;
	    }
	  else
	    {			/*^cond.else */

	      /*^block */
	      /*anyblock */
	      {




		{
		  MELT_LOCATION ("warmelt-outobj.melt:201:/ locexp");
		  meltgc_strbuf_add_indent ((melt_ptr_t)
					    ( /*_.IMPLBUF__V4*/ meltfptr[3]),
					    ( /*_#DEPTH__L1*/ meltfnum[0]),
					    0);
		}
		;
	       /*clear *//*_.IFELSE___V21*/ meltfptr[20] = 0;
		/*epilog */
	      }
	      ;
	    }
	  ;
	  MELT_LOCATION ("warmelt-outobj.melt:203:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^checksignal */
	  MELT_CHECK_SIGNAL ();
	  ;
   /*_#NULL__L8*/ meltfnum[2] =
	    (( /*_.OLOC__V12*/ meltfptr[11]) == NULL);;
	  MELT_LOCATION ("warmelt-outobj.melt:203:/ cond");
	  /*cond */ if ( /*_#NULL__L8*/ meltfnum[2])	/*then */
	    {
	      /*^cond.then */
	      /*^block */
	      /*anyblock */
	      {

     /*_#IS_STRING__L10*/ meltfnum[9] =
		  (melt_magic_discr
		   ((melt_ptr_t) ( /*_.OCOMM__V13*/ meltfptr[12])) ==
		   MELTOBMAG_STRING);;
		/*^compute */
		/*_#IF___L9*/ meltfnum[8] = /*_#IS_STRING__L10*/ meltfnum[9];;
		/*epilog */

		MELT_LOCATION ("warmelt-outobj.melt:203:/ clear");
	       /*clear *//*_#IS_STRING__L10*/ meltfnum[9] = 0;
	      }
	      ;
	    }
	  else
	    {			/*^cond.else */

    /*_#IF___L9*/ meltfnum[8] = 0;;
	    }
	  ;
	  MELT_LOCATION ("warmelt-outobj.melt:203:/ cond");
	  /*cond */ if ( /*_#IF___L9*/ meltfnum[8])	/*then */
	    {
	      /*^cond.then */
	      /*^block */
	      /*anyblock */
	      {

		MELT_LOCATION ("warmelt-outobj.melt:204:/ checksignal");
		MELT_CHECK_SIGNAL ();
		;
		/*^apply */
		/*apply */
		{
		  union meltparam_un argtab[1];
		  memset (&argtab, 0, sizeof (argtab));
		  /*^apply.arg */
		  argtab[0].meltbp_cstring = "/*xpurval!";
		  /*_.ADD2OUT__V25*/ meltfptr[21] =
		    melt_apply ((meltclosure_ptr_t)
				(( /*!ADD2OUT */ meltfrout->tabval[6])),
				(melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				(MELTBPARSTR_CSTRING ""), argtab, "",
				(union meltparam_un *) 0);
		}
		;

		{
		  MELT_LOCATION ("warmelt-outobj.melt:205:/ locexp");
		  meltgc_add_strbuf_ccomment ((melt_ptr_t)
					      ( /*_.IMPLBUF__V4*/
					       meltfptr[3]),
					      melt_string_str ((melt_ptr_t)
							       ( /*_.OCOMM__V13*/ meltfptr[12])));
		}
		;
		MELT_LOCATION ("warmelt-outobj.melt:206:/ checksignal");
		MELT_CHECK_SIGNAL ();
		;
		/*^apply */
		/*apply */
		{
		  union meltparam_un argtab[1];
		  memset (&argtab, 0, sizeof (argtab));
		  /*^apply.arg */
		  argtab[0].meltbp_cstring = "*/ ";
		  /*_.ADD2OUT__V26*/ meltfptr[25] =
		    melt_apply ((meltclosure_ptr_t)
				(( /*!ADD2OUT */ meltfrout->tabval[6])),
				(melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				(MELTBPARSTR_CSTRING ""), argtab, "",
				(union meltparam_un *) 0);
		}
		;
		MELT_LOCATION ("warmelt-outobj.melt:203:/ quasiblock");


		/*_.PROGN___V27*/ meltfptr[26] =
		  /*_.ADD2OUT__V26*/ meltfptr[25];;
		/*^compute */
		/*_.IF___V24*/ meltfptr[22] = /*_.PROGN___V27*/ meltfptr[26];;
		/*epilog */

		MELT_LOCATION ("warmelt-outobj.melt:203:/ clear");
	       /*clear *//*_.ADD2OUT__V25*/ meltfptr[21] = 0;
		/*^clear */
	       /*clear *//*_.ADD2OUT__V26*/ meltfptr[25] = 0;
		/*^clear */
	       /*clear *//*_.PROGN___V27*/ meltfptr[26] = 0;
	      }
	      ;
	    }
	  else
	    {			/*^cond.else */

    /*_.IF___V24*/ meltfptr[22] = NULL;;
	    }
	  ;
	  MELT_LOCATION ("warmelt-outobj.melt:208:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^apply */
	  /*apply */
	  {
	    union meltparam_un argtab[1];
	    memset (&argtab, 0, sizeof (argtab));
	    /*^apply.arg */
	    argtab[0].meltbp_cstring = "{";
	    /*_.ADD2OUT__V28*/ meltfptr[21] =
	      melt_apply ((meltclosure_ptr_t)
			  (( /*!ADD2OUT */ meltfrout->tabval[6])),
			  (melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			  (MELTBPARSTR_CSTRING ""), argtab, "",
			  (union meltparam_un *) 0);
	  }
	  ;
	  /*citerblock FOREACH_IN_MULTIPLE */
	  {
	    /* start foreach_in_multiple meltcit1__EACHTUP */
	    long meltcit1__EACHTUP_ln =
	      melt_multiple_length ((melt_ptr_t) /*_.OCONT__V14*/
				    meltfptr[13]);
	    for ( /*_#IX__L11*/ meltfnum[9] = 0;
		 ( /*_#IX__L11*/ meltfnum[9] >= 0)
		 && ( /*_#IX__L11*/ meltfnum[9] < meltcit1__EACHTUP_ln);
	/*_#IX__L11*/ meltfnum[9]++)
	      {
		/*_.COMP__V29*/ meltfptr[25] =
		  melt_multiple_nth ((melt_ptr_t)
				     ( /*_.OCONT__V14*/ meltfptr[13]),
				     /*_#IX__L11*/ meltfnum[9]);



    /*_#I__L12*/ meltfnum[11] =
		  (( /*_#DEPTH__L1*/ meltfnum[0]) + (1));;
		MELT_LOCATION ("warmelt-outobj.melt:212:/ checksignal");
		MELT_CHECK_SIGNAL ();
		;
		/*^msend */
		/*msend */
		{
		  union meltparam_un argtab[3];
		  memset (&argtab, 0, sizeof (argtab));
		  /*^ojbmsend.arg */
		  argtab[0].meltbp_aptr =
		    (melt_ptr_t *) & /*_.DECLBUF__V3*/ meltfptr[2];
		  /*^ojbmsend.arg */
		  argtab[1].meltbp_aptr =
		    (melt_ptr_t *) & /*_.IMPLBUF__V4*/ meltfptr[3];
		  /*^ojbmsend.arg */
		  argtab[2].meltbp_long = /*_#I__L12*/ meltfnum[11];
		  /*_.OUTPUT_C_CODE__V30*/ meltfptr[26] =
		    meltgc_send ((melt_ptr_t) ( /*_.COMP__V29*/ meltfptr[25]),
				 (melt_ptr_t) (( /*!OUTPUT_C_CODE */
						meltfrout->tabval[7])),
				 (MELTBPARSTR_PTR MELTBPARSTR_PTR
				  MELTBPARSTR_LONG ""), argtab, "",
				 (union meltparam_un *) 0);
		}
		;
		if ( /*_#IX__L11*/ meltfnum[9] < 0)
		  break;
	      }			/* end  foreach_in_multiple meltcit1__EACHTUP */

	    /*citerepilog */

	    MELT_LOCATION ("warmelt-outobj.melt:209:/ clear");
	      /*clear *//*_.COMP__V29*/ meltfptr[25] = 0;
	    /*^clear */
	      /*clear *//*_#IX__L11*/ meltfnum[9] = 0;
	    /*^clear */
	      /*clear *//*_#I__L12*/ meltfnum[11] = 0;
	    /*^clear */
	      /*clear *//*_.OUTPUT_C_CODE__V30*/ meltfptr[26] = 0;
	  }			/*endciterblock FOREACH_IN_MULTIPLE */
	  ;
   /*_#I__L13*/ meltfnum[12] =
	    (( /*_#DEPTH__L1*/ meltfnum[0]) + (1));;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:213:/ locexp");
	    meltgc_strbuf_add_indent ((melt_ptr_t)
				      ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				      ( /*_#I__L13*/ meltfnum[12]), 0);
	  }
	  ;
	  MELT_LOCATION ("warmelt-outobj.melt:214:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^apply */
	  /*apply */
	  {
	    union meltparam_un argtab[1];
	    memset (&argtab, 0, sizeof (argtab));
	    /*^apply.arg */
	    argtab[0].meltbp_cstring = "}";
	    /*_.ADD2OUT__V31*/ meltfptr[30] =
	      melt_apply ((meltclosure_ptr_t)
			  (( /*!ADD2OUT */ meltfrout->tabval[6])),
			  (melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			  (MELTBPARSTR_CSTRING ""), argtab, "",
			  (union meltparam_un *) 0);
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:215:/ locexp");
	    meltgc_strbuf_add_indent ((melt_ptr_t)
				      ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				      ( /*_#DEPTH__L1*/ meltfnum[0]), 0);
	  }
	  ;
	  MELT_LOCATION ("warmelt-outobj.melt:177:/ quasiblock");


	  /*epilog */

	  /*^clear */
	     /*clear *//*_.IFELSE___V21*/ meltfptr[20] = 0;
	  /*^clear */
	     /*clear *//*_#NULL__L8*/ meltfnum[2] = 0;
	  /*^clear */
	     /*clear *//*_#IF___L9*/ meltfnum[8] = 0;
	  /*^clear */
	     /*clear *//*_.IF___V24*/ meltfptr[22] = 0;
	  /*^clear */
	     /*clear *//*_.ADD2OUT__V28*/ meltfptr[21] = 0;
	  /*^clear */
	     /*clear *//*_#I__L13*/ meltfnum[12] = 0;
	  /*^clear */
	     /*clear *//*_.ADD2OUT__V31*/ meltfptr[30] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-outobj.melt:219:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^cond */
	  /*cond */ if ( /*_.OLOC__V12*/ meltfptr[11])	/*then */
	    {
	      /*^cond.then */
	      /*^block */
	      /*anyblock */
	      {

		MELT_LOCATION ("warmelt-outobj.melt:220:/ quasiblock");


     /*_?*/ meltfram__.loc_CSTRING__o1 =
		  (char *) 0;;

		{
		  MELT_LOCATION ("warmelt-outobj.melt:224:/ locexp");
		  /* outpucod_objexpandpureval nonvoid FILLCOMSTR_CHK__2 */
		  static char buf_FILLCOMSTR_CHK__2[80];
		  const char *comstr_FILLCOMSTR_CHK__2
		    =
		    melt_string_str ((melt_ptr_t) /*_.OCOMM__V13*/
				     meltfptr[12]);
		  if (comstr_FILLCOMSTR_CHK__2)
		    snprintf (buf_FILLCOMSTR_CHK__2,
			      sizeof (buf_FILLCOMSTR_CHK__2),
			      "xpurval: %s", comstr_FILLCOMSTR_CHK__2);
		  else
		    snprintf (buf_FILLCOMSTR_CHK__2,
			      sizeof (buf_FILLCOMSTR_CHK__2),
			      "[xpurval/%lx]",
			      melt_obj_hash ((melt_ptr_t) /*_.OBX__V2*/
					     meltfptr[1]));
	     /*_?*/ meltfram__.loc_CSTRING__o1 = buf_FILLCOMSTR_CHK__2;
		  /* outpucod_objexpandpureval nonvoid end FILLCOMSTR_CHK__2 */
		  ;
		}
		;
		MELT_LOCATION ("warmelt-outobj.melt:239:/ checksignal");
		MELT_CHECK_SIGNAL ();
		;
		/*^apply */
		/*apply */
		{
		  union meltparam_un argtab[3];
		  memset (&argtab, 0, sizeof (argtab));
		  /*^apply.arg */
		  argtab[0].meltbp_aptr =
		    (melt_ptr_t *) & /*_.IMPLBUF__V4*/ meltfptr[3];
		  /*^apply.arg */
		  argtab[1].meltbp_long = /*_#DEPTH__L1*/ meltfnum[0];
		  /*^apply.arg */
		  argtab[2].meltbp_cstring =
		    /*_?*/ meltfram__.loc_CSTRING__o1;
		  /*_.OUTPUT_RAW_LOCATION__V34*/ meltfptr[21] =
		    melt_apply ((meltclosure_ptr_t)
				(( /*!OUTPUT_RAW_LOCATION */ meltfrout->
				  tabval[8])),
				(melt_ptr_t) ( /*_.OLOC__V12*/ meltfptr[11]),
				(MELTBPARSTR_PTR MELTBPARSTR_LONG
				 MELTBPARSTR_CSTRING ""), argtab, "",
				(union meltparam_un *) 0);
		}
		;
		/*_.LET___V33*/ meltfptr[22] =
		  /*_.OUTPUT_RAW_LOCATION__V34*/ meltfptr[21];;

		MELT_LOCATION ("warmelt-outobj.melt:220:/ clear");
	       /*clear *//*_?*/ meltfram__.loc_CSTRING__o1 = 0;
		/*^clear */
	       /*clear *//*_.OUTPUT_RAW_LOCATION__V34*/ meltfptr[21] = 0;
		/*_.IF___V32*/ meltfptr[20] = /*_.LET___V33*/ meltfptr[22];;
		/*epilog */

		MELT_LOCATION ("warmelt-outobj.melt:219:/ clear");
	       /*clear *//*_.LET___V33*/ meltfptr[22] = 0;
	      }
	      ;
	    }
	  else
	    {			/*^cond.else */

    /*_.IF___V32*/ meltfptr[20] = NULL;;
	    }
	  ;
	  MELT_LOCATION ("warmelt-outobj.melt:241:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^apply */
	  /*apply */
	  {
	    union meltparam_un argtab[1];
	    memset (&argtab, 0, sizeof (argtab));
	    /*^apply.arg */
	    argtab[0].meltbp_cstring = "(";
	    /*_.ADD2OUT__V35*/ meltfptr[30] =
	      melt_apply ((meltclosure_ptr_t)
			  (( /*!ADD2OUT */ meltfrout->tabval[6])),
			  (melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			  (MELTBPARSTR_CSTRING ""), argtab, "",
			  (union meltparam_un *) 0);
	  }
	  ;
	  /*citerblock FOREACH_IN_MULTIPLE */
	  {
	    /* start foreach_in_multiple meltcit2__EACHTUP */
	    long meltcit2__EACHTUP_ln =
	      melt_multiple_length ((melt_ptr_t) /*_.OCONT__V14*/
				    meltfptr[13]);
	    for ( /*_#IX__L14*/ meltfnum[2] = 0;
		 ( /*_#IX__L14*/ meltfnum[2] >= 0)
		 && ( /*_#IX__L14*/ meltfnum[2] < meltcit2__EACHTUP_ln);
	/*_#IX__L14*/ meltfnum[2]++)
	      {
		/*_.COMP__V36*/ meltfptr[21] =
		  melt_multiple_nth ((melt_ptr_t)
				     ( /*_.OCONT__V14*/ meltfptr[13]),
				     /*_#IX__L14*/ meltfnum[2]);



    /*_#I__L15*/ meltfnum[8] =
		  (( /*_#DEPTH__L1*/ meltfnum[0]) + (1));;
		MELT_LOCATION ("warmelt-outobj.melt:245:/ checksignal");
		MELT_CHECK_SIGNAL ();
		;
		/*^msend */
		/*msend */
		{
		  union meltparam_un argtab[3];
		  memset (&argtab, 0, sizeof (argtab));
		  /*^ojbmsend.arg */
		  argtab[0].meltbp_aptr =
		    (melt_ptr_t *) & /*_.DECLBUF__V3*/ meltfptr[2];
		  /*^ojbmsend.arg */
		  argtab[1].meltbp_aptr =
		    (melt_ptr_t *) & /*_.IMPLBUF__V4*/ meltfptr[3];
		  /*^ojbmsend.arg */
		  argtab[2].meltbp_long = /*_#I__L15*/ meltfnum[8];
		  /*_.OUTPUT_C_CODE__V37*/ meltfptr[22] =
		    meltgc_send ((melt_ptr_t) ( /*_.COMP__V36*/ meltfptr[21]),
				 (melt_ptr_t) (( /*!OUTPUT_C_CODE */
						meltfrout->tabval[7])),
				 (MELTBPARSTR_PTR MELTBPARSTR_PTR
				  MELTBPARSTR_LONG ""), argtab, "",
				 (union meltparam_un *) 0);
		}
		;
		if ( /*_#IX__L14*/ meltfnum[2] < 0)
		  break;
	      }			/* end  foreach_in_multiple meltcit2__EACHTUP */

	    /*citerepilog */

	    MELT_LOCATION ("warmelt-outobj.melt:242:/ clear");
	      /*clear *//*_.COMP__V36*/ meltfptr[21] = 0;
	    /*^clear */
	      /*clear *//*_#IX__L14*/ meltfnum[2] = 0;
	    /*^clear */
	      /*clear *//*_#I__L15*/ meltfnum[8] = 0;
	    /*^clear */
	      /*clear *//*_.OUTPUT_C_CODE__V37*/ meltfptr[22] = 0;
	  }			/*endciterblock FOREACH_IN_MULTIPLE */
	  ;
	  MELT_LOCATION ("warmelt-outobj.melt:246:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^apply */
	  /*apply */
	  {
	    union meltparam_un argtab[1];
	    memset (&argtab, 0, sizeof (argtab));
	    /*^apply.arg */
	    argtab[0].meltbp_cstring = ")";
	    /*_.ADD2OUT__V38*/ meltfptr[37] =
	      melt_apply ((meltclosure_ptr_t)
			  (( /*!ADD2OUT */ meltfrout->tabval[6])),
			  (melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			  (MELTBPARSTR_CSTRING ""), argtab, "",
			  (union meltparam_un *) 0);
	  }
	  ;
	  MELT_LOCATION ("warmelt-outobj.melt:217:/ quasiblock");


	  /*_.PROGN___V39*/ meltfptr[38] = /*_.ADD2OUT__V38*/ meltfptr[37];;
	  /*^compute */
	  /*_.IFELSE___V20*/ meltfptr[18] = /*_.PROGN___V39*/ meltfptr[38];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-outobj.melt:177:/ clear");
	     /*clear *//*_.IF___V32*/ meltfptr[20] = 0;
	  /*^clear */
	     /*clear *//*_.ADD2OUT__V35*/ meltfptr[30] = 0;
	  /*^clear */
	     /*clear *//*_.ADD2OUT__V38*/ meltfptr[37] = 0;
	  /*^clear */
	     /*clear *//*_.PROGN___V39*/ meltfptr[38] = 0;
	}
	;
      }
    ;
    /*_.LET___V11*/ meltfptr[6] = /*_.IFELSE___V20*/ meltfptr[18];;

    MELT_LOCATION ("warmelt-outobj.melt:169:/ clear");
	   /*clear *//*_.OLOC__V12*/ meltfptr[11] = 0;
    /*^clear */
	   /*clear *//*_.OCOMM__V13*/ meltfptr[12] = 0;
    /*^clear */
	   /*clear *//*_.OCONT__V14*/ meltfptr[13] = 0;
    /*^clear */
	   /*clear *//*_.OTYP__V15*/ meltfptr[14] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V16*/ meltfptr[15] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V18*/ meltfptr[16] = 0;
    /*^clear */
	   /*clear *//*_#__L7*/ meltfnum[1] = 0;
    /*^clear */
	   /*clear *//*_.IFELSE___V20*/ meltfptr[18] = 0;
    MELT_LOCATION ("warmelt-outobj.melt:166:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.LET___V11*/ meltfptr[6];;

    {
      MELT_LOCATION ("warmelt-outobj.melt:166:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V5*/ meltfptr[4] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V9*/ meltfptr[5] = 0;
    /*^clear */
	   /*clear *//*_.LET___V11*/ meltfptr[6] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("OUTPUCOD_OBJEXPANDPUREVAL", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_11_warmelt_outobj_OUTPUCOD_OBJEXPANDPUREVAL_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_11_warmelt_outobj_OUTPUCOD_OBJEXPANDPUREVAL */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_12_warmelt_outobj_OUTDECLINIT_OBJINITOBJECT (meltclosure_ptr_t
						      meltclosp_,
						      melt_ptr_t
						      meltfirstargp_,
						      const
						      melt_argdescr_cell_t
						      meltxargdescr_[],
						      union meltparam_un *
						      meltxargtab_,
						      const
						      melt_argdescr_cell_t
						      meltxresdescr_[],
						      union meltparam_un *
						      meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_12_warmelt_outobj_OUTDECLINIT_OBJINITOBJECT_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_12_warmelt_outobj_OUTDECLINIT_OBJINITOBJECT_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 4
    melt_ptr_t mcfr_varptr[4];
#define MELTFRAM_NBVARNUM 1
    long mcfr_varnum[1];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_12_warmelt_outobj_OUTDECLINIT_OBJINITOBJECT is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_12_warmelt_outobj_OUTDECLINIT_OBJINITOBJECT_st
	 *) meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 4; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_12_warmelt_outobj_OUTDECLINIT_OBJINITOBJECT nbval 4*/
  meltfram__.mcfr_nbvar = 4 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("OUTDECLINIT_OBJINITOBJECT", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-outobj.melt:251:/ getarg");
 /*_.RECV__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.SBUF__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2])) !=
	      NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


    {
      MELT_LOCATION ("warmelt-outobj.melt:252:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
			   (" struct MELT_OBJECT_STRUCT("));
    }
    ;
 /*_#GET_INT__L1*/ meltfnum[0] =
      (melt_get_int ((melt_ptr_t) ( /*_.RECV__V2*/ meltfptr[1])));;

    {
      MELT_LOCATION ("warmelt-outobj.melt:253:/ locexp");
      meltgc_add_strbuf_dec ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
			     ( /*_#GET_INT__L1*/ meltfnum[0]));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:254:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
			   (") "));
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:255:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.RECV__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 1, "NAMED_NAME");
  /*_.NAMED_NAME__V4*/ meltfptr[3] = slot;
    };
    ;

    {
      /*^locexp */
      /*add2sbuf_string */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
			   melt_string_str ((melt_ptr_t)
					    ( /*_.NAMED_NAME__V4*/
					     meltfptr[3])));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:256:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]), (";"));
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:251:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*epilog */

    /*^clear */
	   /*clear *//*_#GET_INT__L1*/ meltfnum[0] = 0;
    /*^clear */
	   /*clear *//*_.NAMED_NAME__V4*/ meltfptr[3] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("OUTDECLINIT_OBJINITOBJECT", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_12_warmelt_outobj_OUTDECLINIT_OBJINITOBJECT_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*noretval */ NULL);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_12_warmelt_outobj_OUTDECLINIT_OBJINITOBJECT */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_13_warmelt_outobj_OUTCINITFILL_OBJINITOBJECT (meltclosure_ptr_t
						       meltclosp_,
						       melt_ptr_t
						       meltfirstargp_,
						       const
						       melt_argdescr_cell_t
						       meltxargdescr_[],
						       union meltparam_un *
						       meltxargtab_,
						       const
						       melt_argdescr_cell_t
						       meltxresdescr_[],
						       union meltparam_un *
						       meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_13_warmelt_outobj_OUTCINITFILL_OBJINITOBJECT_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_13_warmelt_outobj_OUTCINITFILL_OBJINITOBJECT_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 69
    melt_ptr_t mcfr_varptr[69];
#define MELTFRAM_NBVARNUM 35
    long mcfr_varnum[35];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_13_warmelt_outobj_OUTCINITFILL_OBJINITOBJECT is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_13_warmelt_outobj_OUTCINITFILL_OBJINITOBJECT_st
	 *) meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 69; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_13_warmelt_outobj_OUTCINITFILL_OBJINITOBJECT nbval 69*/
  meltfram__.mcfr_nbvar = 69 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("OUTCINITFILL_OBJINITOBJECT", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-outobj.melt:263:/ getarg");
 /*_.RECV__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.SBUF__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2])) !=
	      NULL);


  /*getarg#2 */
  /*^getarg */
  if (meltxargdescr_[1] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.PTRSTR__V4*/ meltfptr[3] =
    (meltxargtab_[1].meltbp_aptr) ? (*(meltxargtab_[1].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.PTRSTR__V4*/ meltfptr[3])) !=
	      NULL);


  /*getarg#3 */
  /*^getarg */
  if (meltxargdescr_[2] != MELTBPAR_LONG)
    goto lab_endgetargs;
 /*_#DEPTH__L1*/ meltfnum[0] = meltxargtab_[2].meltbp_long;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:264:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L2*/ meltfnum[1] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.RECV__V2*/ meltfptr[1]),
			     (melt_ptr_t) (( /*!CLASS_OBJINITOBJECT */
					    meltfrout->tabval[0])));;
      MELT_LOCATION ("warmelt-outobj.melt:264:/ cond");
      /*cond */ if ( /*_#IS_A__L2*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V6*/ meltfptr[5] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-outobj.melt:264:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("outcinitfill_objinitobject check recv"),
				  ("warmelt-outobj.melt")
				  ? ("warmelt-outobj.melt") : __FILE__,
				  (264) ? (264) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V6*/ meltfptr[5] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V5*/ meltfptr[4] = /*_.IFELSE___V6*/ meltfptr[5];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:264:/ clear");
	     /*clear *//*_#IS_A__L2*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V6*/ meltfptr[5] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V5*/ meltfptr[4] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:265:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L3*/ meltfnum[1] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-outobj.melt:265:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L3*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L4*/ meltfnum[3] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-outobj.melt:265:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[7];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L4*/ meltfnum[3];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-outobj.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 265;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "outcinitfill_objinitobject recv=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.RECV__V2*/ meltfptr[1];
	      /*^apply.arg */
	      argtab[5].meltbp_cstring = " ptrstr=";
	      /*^apply.arg */
	      argtab[6].meltbp_aptr =
		(melt_ptr_t *) & /*_.PTRSTR__V4*/ meltfptr[3];
	      /*_.MELT_DEBUG_FUN__V9*/ meltfptr[8] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[1])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V8*/ meltfptr[7] = /*_.MELT_DEBUG_FUN__V9*/ meltfptr[8];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-outobj.melt:265:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L4*/ meltfnum[3] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V9*/ meltfptr[8] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V8*/ meltfptr[7] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-outobj.melt:265:/ quasiblock");


      /*_.PROGN___V10*/ meltfptr[8] = /*_.IF___V8*/ meltfptr[7];;
      /*^compute */
      /*_.IFCPP___V7*/ meltfptr[5] = /*_.PROGN___V10*/ meltfptr[8];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:265:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L3*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IF___V8*/ meltfptr[7] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V10*/ meltfptr[8] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V7*/ meltfptr[5] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:266:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_STRING__L5*/ meltfnum[3] =
	(melt_magic_discr ((melt_ptr_t) ( /*_.PTRSTR__V4*/ meltfptr[3])) ==
	 MELTOBMAG_STRING);;
      MELT_LOCATION ("warmelt-outobj.melt:266:/ cond");
      /*cond */ if ( /*_#IS_STRING__L5*/ meltfnum[3])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V12*/ meltfptr[8] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-outobj.melt:266:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("outcinitfill_objinitobject check ptrstr"),
				  ("warmelt-outobj.melt")
				  ? ("warmelt-outobj.melt") : __FILE__,
				  (266) ? (266) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V12*/ meltfptr[8] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V11*/ meltfptr[7] = /*_.IFELSE___V12*/ meltfptr[8];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:266:/ clear");
	     /*clear *//*_#IS_STRING__L5*/ meltfnum[3] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V12*/ meltfptr[8] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V11*/ meltfptr[7] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-outobj.melt:267:/ quasiblock");


    /*^getslot */
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.RECV__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 2, "OIE_DATA");
  /*_.ODATA__V14*/ meltfptr[13] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-outobj.melt:268:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.RECV__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 3, "OIE_DISCR");
  /*_.ODISCR__V15*/ meltfptr[14] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-outobj.melt:269:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.RECV__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 1, "OIE_CNAME");
  /*_.ONAME__V16*/ meltfptr[15] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-outobj.melt:270:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.RECV__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 4, "OIE_LOCVAR");
  /*_.OLOCVAR__V17*/ meltfptr[16] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-outobj.melt:271:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.RECV__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 5, "OIO_PREDEF");
  /*_.OIOPREDEF__V18*/ meltfptr[17] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-outobj.melt:272:/ cond");
    /*cond */ if (
		   /*ifisa */
		   melt_is_instance_of ((melt_ptr_t)
					( /*_.RECV__V2*/ meltfptr[1]),
					(melt_ptr_t) (( /*!CLASS_OBJINITOBJECT */ meltfrout->tabval[0])))
      )				/*then */
      {
	/*^cond.then */
	/*^getslot */
	{
	  melt_ptr_t slot = NULL, obj = NULL;
	  obj = (melt_ptr_t) ( /*_.RECV__V2*/ meltfptr[1]) /*=obj*/ ;
	  melt_object_get_field (slot, obj, 6, "OIO_CLASS");
   /*_.OCLASS__V19*/ meltfptr[18] = slot;
	};
	;
      }
    else
      {				/*^cond.else */

  /*_.OCLASS__V19*/ meltfptr[18] = NULL;;
      }
    ;
    /*^compute */
 /*_#DEPTHP1__L6*/ meltfnum[1] =
      (( /*_#DEPTH__L1*/ meltfnum[0]) + (1));;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:275:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L7*/ meltfnum[3] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-outobj.melt:275:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L7*/ meltfnum[3])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L8*/ meltfnum[7] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-outobj.melt:275:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L8*/ meltfnum[7];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-outobj.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 275;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "outcinitfill_objinitobject odata=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.ODATA__V14*/ meltfptr[13];
	      /*_.MELT_DEBUG_FUN__V22*/ meltfptr[21] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[1])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V21*/ meltfptr[20] =
	      /*_.MELT_DEBUG_FUN__V22*/ meltfptr[21];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-outobj.melt:275:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L8*/ meltfnum[7] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V22*/ meltfptr[21] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V21*/ meltfptr[20] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-outobj.melt:275:/ quasiblock");


      /*_.PROGN___V23*/ meltfptr[21] = /*_.IF___V21*/ meltfptr[20];;
      /*^compute */
      /*_.IFCPP___V20*/ meltfptr[19] = /*_.PROGN___V23*/ meltfptr[21];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:275:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L7*/ meltfnum[3] = 0;
      /*^clear */
	     /*clear *//*_.IF___V21*/ meltfptr[20] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V23*/ meltfptr[21] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V20*/ meltfptr[19] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-outobj.melt:276:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^cond */
    /*cond */ if ( /*_.ODATA__V14*/ meltfptr[13])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{


#if MELT_HAVE_DEBUG
	  /*^cppif.then */
	  /*^block */
	  /*anyblock */
	  {

	    /*^checksignal */
	    MELT_CHECK_SIGNAL ();
	    ;
     /*_#IS_A__L9*/ meltfnum[7] =
	      melt_is_instance_of ((melt_ptr_t)
				   ( /*_.ODATA__V14*/ meltfptr[13]),
				   (melt_ptr_t) (( /*!CLASS_NREP_DATAINSTANCE */ meltfrout->tabval[2])));;
	    MELT_LOCATION ("warmelt-outobj.melt:276:/ cond");
	    /*cond */ if ( /*_#IS_A__L9*/ meltfnum[7])	/*then */
	      {
		/*^cond.then */
		/*_.IFELSE___V26*/ meltfptr[25] = ( /*nil */ NULL);;
	      }
	    else
	      {
		MELT_LOCATION ("warmelt-outobj.melt:276:/ cond.else");

		/*^block */
		/*anyblock */
		{




		  {
		    /*^locexp */
		    melt_assert_failed (("check odata"),
					("warmelt-outobj.melt")
					? ("warmelt-outobj.melt") : __FILE__,
					(276) ? (276) : __LINE__,
					__FUNCTION__);
		    ;
		  }
		  ;
		 /*clear *//*_.IFELSE___V26*/ meltfptr[25] = 0;
		  /*epilog */
		}
		;
	      }
	    ;
	    /*^compute */
	    /*_.IFCPP___V25*/ meltfptr[21] = /*_.IFELSE___V26*/ meltfptr[25];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-outobj.melt:276:/ clear");
	       /*clear *//*_#IS_A__L9*/ meltfnum[7] = 0;
	    /*^clear */
	       /*clear *//*_.IFELSE___V26*/ meltfptr[25] = 0;
	  }

#else /*MELT_HAVE_DEBUG */
	  /*^cppif.else */
	  /*_.IFCPP___V25*/ meltfptr[21] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
	  ;
	  /*^compute */
	  /*_.IF___V24*/ meltfptr[20] = /*_.IFCPP___V25*/ meltfptr[21];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-outobj.melt:276:/ clear");
	     /*clear *//*_.IFCPP___V25*/ meltfptr[21] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

  /*_.IF___V24*/ meltfptr[20] = NULL;;
      }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:277:/ quasiblock");


    /*^checksignal */
    MELT_CHECK_SIGNAL ();
    ;
    /*^cond */
    /*cond */ if ( /*_.ODATA__V14*/ meltfptr[13])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  /*^getslot */
	  {
	    melt_ptr_t slot = NULL, obj = NULL;
	    obj = (melt_ptr_t) ( /*_.ODATA__V14*/ meltfptr[13]) /*=obj*/ ;
	    melt_object_get_field (slot, obj, 0, "NREP_LOC");
    /*_.NREP_LOC__V28*/ meltfptr[21] = slot;
	  };
	  ;
	  /*_.ODLOC__V27*/ meltfptr[25] = /*_.NREP_LOC__V28*/ meltfptr[21];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-outobj.melt:277:/ clear");
	     /*clear *//*_.NREP_LOC__V28*/ meltfptr[21] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

  /*_.ODLOC__V27*/ meltfptr[25] = NULL;;
      }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:278:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^cond */
    /*cond */ if ( /*_.ODATA__V14*/ meltfptr[13])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  /*^getslot */
	  {
	    melt_ptr_t slot = NULL, obj = NULL;
	    obj = (melt_ptr_t) ( /*_.ODATA__V14*/ meltfptr[13]) /*=obj*/ ;
	    melt_object_get_field (slot, obj, 7, "NINST_HASH");
    /*_.NINST_HASH__V30*/ meltfptr[29] = slot;
	  };
	  ;
	  /*_.ODHASH__V29*/ meltfptr[21] =
	    /*_.NINST_HASH__V30*/ meltfptr[29];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-outobj.melt:278:/ clear");
	     /*clear *//*_.NINST_HASH__V30*/ meltfptr[29] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

  /*_.ODHASH__V29*/ meltfptr[21] = NULL;;
      }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:279:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^cond */
    /*cond */ if ( /*_.ODATA__V14*/ meltfptr[13])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  /*^getslot */
	  {
	    melt_ptr_t slot = NULL, obj = NULL;
	    obj = (melt_ptr_t) ( /*_.ODATA__V14*/ meltfptr[13]) /*=obj*/ ;
	    melt_object_get_field (slot, obj, 8, "NINST_SLOTS");
    /*_.NINST_SLOTS__V32*/ meltfptr[31] = slot;
	  };
	  ;
	  /*_.ODSLOTS__V31*/ meltfptr[29] =
	    /*_.NINST_SLOTS__V32*/ meltfptr[31];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-outobj.melt:279:/ clear");
	     /*clear *//*_.NINST_SLOTS__V32*/ meltfptr[31] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

  /*_.ODSLOTS__V31*/ meltfptr[29] = NULL;;
      }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:280:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^cond */
    /*cond */ if ( /*_.ODATA__V14*/ meltfptr[13])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  /*^getslot */
	  {
	    melt_ptr_t slot = NULL, obj = NULL;
	    obj = (melt_ptr_t) ( /*_.ODATA__V14*/ meltfptr[13]) /*=obj*/ ;
	    melt_object_get_field (slot, obj, 5, "NINST_OBJNUM");
    /*_.NINST_OBJNUM__V34*/ meltfptr[33] = slot;
	  };
	  ;
	  /*_.ODOBNUM__V33*/ meltfptr[31] =
	    /*_.NINST_OBJNUM__V34*/ meltfptr[33];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-outobj.melt:280:/ clear");
	     /*clear *//*_.NINST_OBJNUM__V34*/ meltfptr[33] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

  /*_.ODOBNUM__V33*/ meltfptr[31] = NULL;;
      }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:282:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#IS_MULTIPLE__L10*/ meltfnum[3] =
      (melt_magic_discr ((melt_ptr_t) ( /*_.ODSLOTS__V31*/ meltfptr[29])) ==
       MELTOBMAG_MULTIPLE);;
    MELT_LOCATION ("warmelt-outobj.melt:282:/ cond");
    /*cond */ if ( /*_#IS_MULTIPLE__L10*/ meltfnum[3])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

   /*_#MULTIPLE_LENGTH__L12*/ meltfnum[11] =
	    (melt_multiple_length
	     ((melt_ptr_t) ( /*_.ODSLOTS__V31*/ meltfptr[29])));;
	  /*^compute */
	  /*_#NBSLOTS__L11*/ meltfnum[7] =
	    /*_#MULTIPLE_LENGTH__L12*/ meltfnum[11];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-outobj.melt:282:/ clear");
	     /*clear *//*_#MULTIPLE_LENGTH__L12*/ meltfnum[11] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-outobj.melt:284:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
   /*_#IS_A__L13*/ meltfnum[11] =
	    melt_is_instance_of ((melt_ptr_t)
				 ( /*_.OCLASS__V19*/ meltfptr[18]),
				 (melt_ptr_t) (( /*!CLASS_CLASS */ meltfrout->
						tabval[3])));;
	  MELT_LOCATION ("warmelt-outobj.melt:284:/ cond");
	  /*cond */ if ( /*_#IS_A__L13*/ meltfnum[11])	/*then */
	    {
	      /*^cond.then */
	      /*^block */
	      /*anyblock */
	      {

		MELT_LOCATION ("warmelt-outobj.melt:285:/ cond");
		/*cond */ if (
			       /*ifisa */
			       melt_is_instance_of ((melt_ptr_t)
						    ( /*_.OCLASS__V19*/
						     meltfptr[18]),
						    (melt_ptr_t) (( /*!CLASS_CLASS */ meltfrout->tabval[3])))
		  )		/*then */
		  {
		    /*^cond.then */
		    /*^getslot */
		    {
		      melt_ptr_t slot = NULL, obj = NULL;
		      obj =
			(melt_ptr_t) ( /*_.OCLASS__V19*/ meltfptr[18])
			/*=obj*/ ;
		      melt_object_get_field (slot, obj, 6, "CLASS_FIELDS");
       /*_.CLASS_FIELDS__V35*/ meltfptr[33] = slot;
		    };
		    ;
		  }
		else
		  {		/*^cond.else */

      /*_.CLASS_FIELDS__V35*/ meltfptr[33] = NULL;;
		  }
		;
		/*^compute */
     /*_#MULTIPLE_LENGTH__L15*/ meltfnum[14] =
		  (melt_multiple_length
		   ((melt_ptr_t) ( /*_.CLASS_FIELDS__V35*/ meltfptr[33])));;
		/*^compute */
		/*_#IFELSE___L14*/ meltfnum[13] =
		  /*_#MULTIPLE_LENGTH__L15*/ meltfnum[14];;
		/*epilog */

		MELT_LOCATION ("warmelt-outobj.melt:284:/ clear");
	       /*clear *//*_.CLASS_FIELDS__V35*/ meltfptr[33] = 0;
		/*^clear */
	       /*clear *//*_#MULTIPLE_LENGTH__L15*/ meltfnum[14] = 0;
	      }
	      ;
	    }
	  else
	    {			/*^cond.else */

	      /*^block */
	      /*anyblock */
	      {


#if MELT_HAVE_DEBUG
		MELT_LOCATION ("warmelt-outobj.melt:287:/ cppif.then");
		/*^block */
		/*anyblock */
		{

		  /*^checksignal */
		  MELT_CHECK_SIGNAL ();
		  ;
		  /*^cond */
		  /*cond */ if (( /*nil */ NULL))	/*then */
		    {
		      /*^cond.then */
		      /*_.IFELSE___V37*/ meltfptr[36] = ( /*nil */ NULL);;
		    }
		  else
		    {
		      MELT_LOCATION ("warmelt-outobj.melt:287:/ cond.else");

		      /*^block */
		      /*anyblock */
		      {




			{
			  /*^locexp */
			  melt_assert_failed (("outcinitfill_objinitobject cannot compute nbslots"), ("warmelt-outobj.melt") ? ("warmelt-outobj.melt") : __FILE__, (287) ? (287) : __LINE__, __FUNCTION__);
			  ;
			}
			;
		   /*clear *//*_.IFELSE___V37*/ meltfptr[36] = 0;
			/*epilog */
		      }
		      ;
		    }
		  ;
		  /*^compute */
		  /*_.IFCPP___V36*/ meltfptr[33] =
		    /*_.IFELSE___V37*/ meltfptr[36];;
		  /*epilog */

		  MELT_LOCATION ("warmelt-outobj.melt:287:/ clear");
		 /*clear *//*_.IFELSE___V37*/ meltfptr[36] = 0;
		}

#else /*MELT_HAVE_DEBUG */
		/*^cppif.else */
		/*_.IFCPP___V36*/ meltfptr[33] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
		;
		MELT_LOCATION ("warmelt-outobj.melt:286:/ quasiblock");


     /*_#PROGN___L16*/ meltfnum[14] = 0;;
		/*^compute */
		/*_#IFELSE___L14*/ meltfnum[13] =
		  /*_#PROGN___L16*/ meltfnum[14];;
		/*epilog */

		MELT_LOCATION ("warmelt-outobj.melt:284:/ clear");
	       /*clear *//*_.IFCPP___V36*/ meltfptr[33] = 0;
		/*^clear */
	       /*clear *//*_#PROGN___L16*/ meltfnum[14] = 0;
	      }
	      ;
	    }
	  ;
	  /*_#NBSLOTS__L11*/ meltfnum[7] = /*_#IFELSE___L14*/ meltfnum[13];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-outobj.melt:282:/ clear");
	     /*clear *//*_#IS_A__L13*/ meltfnum[11] = 0;
	  /*^clear */
	     /*clear *//*_#IFELSE___L14*/ meltfnum[13] = 0;
	}
	;
      }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:290:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^cond */
    /*cond */ if ( /*_.ODLOC__V27*/ meltfptr[25])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  /*^checksignal */
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^apply */
	  /*apply */
	  {
	    union meltparam_un argtab[3];
	    memset (&argtab, 0, sizeof (argtab));
	    /*^apply.arg */
	    argtab[0].meltbp_aptr =
	      (melt_ptr_t *) & /*_.SBUF__V3*/ meltfptr[2];
	    /*^apply.arg */
	    argtab[1].meltbp_long = /*_#DEPTHP1__L6*/ meltfnum[1];
	    /*^apply.arg */
	    argtab[2].meltbp_cstring = "iniobj";
	    /*_.OUTPUT_LOCATION__V39*/ meltfptr[33] =
	      melt_apply ((meltclosure_ptr_t)
			  (( /*!OUTPUT_LOCATION */ meltfrout->tabval[4])),
			  (melt_ptr_t) ( /*_.ODLOC__V27*/ meltfptr[25]),
			  (MELTBPARSTR_PTR MELTBPARSTR_LONG
			   MELTBPARSTR_CSTRING ""), argtab, "",
			  (union meltparam_un *) 0);
	  }
	  ;
	  /*_.IF___V38*/ meltfptr[36] =
	    /*_.OUTPUT_LOCATION__V39*/ meltfptr[33];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-outobj.melt:290:/ clear");
	     /*clear *//*_.OUTPUT_LOCATION__V39*/ meltfptr[33] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

  /*_.IF___V38*/ meltfptr[36] = NULL;;
      }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:291:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
				( /*_#DEPTH__L1*/ meltfnum[0]), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:292:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
			   ("/*iniobj "));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:293:/ locexp");
      /*add2sbuf_string */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
			   melt_string_str ((melt_ptr_t)
					    ( /*_.ONAME__V16*/
					     meltfptr[15])));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:294:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
			   ("*/"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:295:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
				( /*_#DEPTH__L1*/ meltfnum[0]), 0);
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:296:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^cond */
    /*cond */ if ( /*_.OIOPREDEF__V18*/ meltfptr[17])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{


	  {
	    MELT_LOCATION ("warmelt-outobj.melt:298:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
				 ("if ("));
	  }
	  ;
	  MELT_LOCATION ("warmelt-outobj.melt:299:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^apply */
	  /*apply */
	  {
	    union meltparam_un argtab[2];
	    memset (&argtab, 0, sizeof (argtab));
	    /*^apply.arg */
	    argtab[0].meltbp_aptr =
	      (melt_ptr_t *) & /*_.SBUF__V3*/ meltfptr[2];
	    /*^apply.arg */
	    argtab[1].meltbp_long = /*_#DEPTH__L1*/ meltfnum[0];
	    /*_.OUTPUT_PREDEF__V40*/ meltfptr[33] =
	      melt_apply ((meltclosure_ptr_t)
			  (( /*!OUTPUT_PREDEF */ meltfrout->tabval[5])),
			  (melt_ptr_t) ( /*_.OIOPREDEF__V18*/ meltfptr[17]),
			  (MELTBPARSTR_PTR MELTBPARSTR_LONG ""), argtab, "",
			  (union meltparam_un *) 0);
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:300:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
				 (" != (melt_ptr_t)&"));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:301:/ locexp");
	    /*add2sbuf_string */
	      meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
				 melt_string_str ((melt_ptr_t)
						  ( /*_.PTRSTR__V4*/
						   meltfptr[3])));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:302:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
				 ("->"));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:303:/ locexp");
	    /*add2sbuf_string */
	      meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
				 melt_string_str ((melt_ptr_t)
						  ( /*_.ONAME__V16*/
						   meltfptr[15])));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:304:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
				 (") {"));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:305:/ locexp");
	    meltgc_strbuf_add_indent ((melt_ptr_t)
				      ( /*_.SBUF__V3*/ meltfptr[2]),
				      ( /*_#DEPTHP1__L6*/ meltfnum[1]), 0);
	  }
	  ;
	  MELT_LOCATION ("warmelt-outobj.melt:306:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^cond */
	  /*cond */ if ( /*_#NBSLOTS__L11*/ meltfnum[7])	/*then */
	    {
	      /*^cond.then */
	      /*^block */
	      /*anyblock */
	      {


		{
		  MELT_LOCATION ("warmelt-outobj.melt:308:/ locexp");
		  /*add2sbuf_strconst */
		    meltgc_add_strbuf ((melt_ptr_t)
				       ( /*_.SBUF__V3*/ meltfptr[2]),
				       ("melt_assertmsg(\"check.predef length "));
		}
		;
		MELT_LOCATION ("warmelt-outobj.melt:309:/ checksignal");
		MELT_CHECK_SIGNAL ();
		;
		/*^apply */
		/*apply */
		{
		  union meltparam_un argtab[2];
		  memset (&argtab, 0, sizeof (argtab));
		  /*^apply.arg */
		  argtab[0].meltbp_aptr =
		    (melt_ptr_t *) & /*_.SBUF__V3*/ meltfptr[2];
		  /*^apply.arg */
		  argtab[1].meltbp_long = /*_#DEPTHP1__L6*/ meltfnum[1];
		  /*_.OUTPUT_PREDEF__V41*/ meltfptr[40] =
		    melt_apply ((meltclosure_ptr_t)
				(( /*!OUTPUT_PREDEF */ meltfrout->tabval[5])),
				(melt_ptr_t) ( /*_.OIOPREDEF__V18*/
					      meltfptr[17]),
				(MELTBPARSTR_PTR MELTBPARSTR_LONG ""), argtab,
				"", (union meltparam_un *) 0);
		}
		;

		{
		  MELT_LOCATION ("warmelt-outobj.melt:310:/ locexp");
		  /*add2sbuf_strconst */
		    meltgc_add_strbuf ((melt_ptr_t)
				       ( /*_.SBUF__V3*/ meltfptr[2]),
				       ("\", melt_object_length((melt_ptr_t)("));
		}
		;
		MELT_LOCATION ("warmelt-outobj.melt:311:/ checksignal");
		MELT_CHECK_SIGNAL ();
		;
		/*^apply */
		/*apply */
		{
		  union meltparam_un argtab[2];
		  memset (&argtab, 0, sizeof (argtab));
		  /*^apply.arg */
		  argtab[0].meltbp_aptr =
		    (melt_ptr_t *) & /*_.SBUF__V3*/ meltfptr[2];
		  /*^apply.arg */
		  argtab[1].meltbp_long = /*_#DEPTHP1__L6*/ meltfnum[1];
		  /*_.OUTPUT_PREDEF__V42*/ meltfptr[41] =
		    melt_apply ((meltclosure_ptr_t)
				(( /*!OUTPUT_PREDEF */ meltfrout->tabval[5])),
				(melt_ptr_t) ( /*_.OIOPREDEF__V18*/
					      meltfptr[17]),
				(MELTBPARSTR_PTR MELTBPARSTR_LONG ""), argtab,
				"", (union meltparam_un *) 0);
		}
		;

		{
		  MELT_LOCATION ("warmelt-outobj.melt:312:/ locexp");
		  /*add2sbuf_strconst */
		    meltgc_add_strbuf ((melt_ptr_t)
				       ( /*_.SBUF__V3*/ meltfptr[2]),
				       (")) >= "));
		}
		;

		{
		  MELT_LOCATION ("warmelt-outobj.melt:313:/ locexp");
		  meltgc_add_strbuf_dec ((melt_ptr_t)
					 ( /*_.SBUF__V3*/ meltfptr[2]),
					 ( /*_#NBSLOTS__L11*/ meltfnum[7]));
		}
		;

		{
		  MELT_LOCATION ("warmelt-outobj.melt:314:/ locexp");
		  /*add2sbuf_strconst */
		    meltgc_add_strbuf ((melt_ptr_t)
				       ( /*_.SBUF__V3*/ meltfptr[2]), (");"));
		}
		;

		{
		  MELT_LOCATION ("warmelt-outobj.melt:315:/ locexp");
		  meltgc_strbuf_add_indent ((melt_ptr_t)
					    ( /*_.SBUF__V3*/ meltfptr[2]),
					    ( /*_#DEPTHP1__L6*/ meltfnum[1]),
					    0);
		}
		;
		MELT_LOCATION ("warmelt-outobj.melt:307:/ quasiblock");


		/*epilog */

		MELT_LOCATION ("warmelt-outobj.melt:306:/ clear");
	       /*clear *//*_.OUTPUT_PREDEF__V41*/ meltfptr[40] = 0;
		/*^clear */
	       /*clear *//*_.OUTPUT_PREDEF__V42*/ meltfptr[41] = 0;
	      }
	      ;
	    }			/*noelse */
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:316:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
				 ("};"));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:317:/ locexp");
	    meltgc_strbuf_add_indent ((melt_ptr_t)
				      ( /*_.SBUF__V3*/ meltfptr[2]),
				      ( /*_#DEPTH__L1*/ meltfnum[0]), 0);
	  }
	  ;
	  MELT_LOCATION ("warmelt-outobj.melt:318:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^msend */
	  /*msend */
	  {
	    union meltparam_un argtab[3];
	    memset (&argtab, 0, sizeof (argtab));
	    /*^ojbmsend.arg */
	    argtab[0].meltbp_aptr = /*nil */ (melt_ptr_t *) NULL;
	    /*^ojbmsend.arg */
	    argtab[1].meltbp_aptr =
	      (melt_ptr_t *) & /*_.SBUF__V3*/ meltfptr[2];
	    /*^ojbmsend.arg */
	    argtab[2].meltbp_long = /*_#DEPTH__L1*/ meltfnum[0];
	    /*_.OUTPUT_C_CODE__V43*/ meltfptr[40] =
	      meltgc_send ((melt_ptr_t) ( /*_.OLOCVAR__V17*/ meltfptr[16]),
			   (melt_ptr_t) (( /*!OUTPUT_C_CODE */ meltfrout->
					  tabval[6])),
			   (MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_LONG
			    ""), argtab, "", (union meltparam_un *) 0);
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:319:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
				 (" = "));
	  }
	  ;
	  MELT_LOCATION ("warmelt-outobj.melt:320:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^apply */
	  /*apply */
	  {
	    union meltparam_un argtab[2];
	    memset (&argtab, 0, sizeof (argtab));
	    /*^apply.arg */
	    argtab[0].meltbp_aptr =
	      (melt_ptr_t *) & /*_.SBUF__V3*/ meltfptr[2];
	    /*^apply.arg */
	    argtab[1].meltbp_long = /*_#DEPTH__L1*/ meltfnum[0];
	    /*_.OUTPUT_PREDEF__V44*/ meltfptr[41] =
	      melt_apply ((meltclosure_ptr_t)
			  (( /*!OUTPUT_PREDEF */ meltfrout->tabval[5])),
			  (melt_ptr_t) ( /*_.OIOPREDEF__V18*/ meltfptr[17]),
			  (MELTBPARSTR_PTR MELTBPARSTR_LONG ""), argtab, "",
			  (union meltparam_un *) 0);
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:321:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
				 (";"));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:322:/ locexp");
	    meltgc_strbuf_add_indent ((melt_ptr_t)
				      ( /*_.SBUF__V3*/ meltfptr[2]),
				      ( /*_#DEPTH__L1*/ meltfnum[0]), 0);
	  }
	  ;
	  MELT_LOCATION ("warmelt-outobj.melt:297:/ quasiblock");


	  /*epilog */

	  MELT_LOCATION ("warmelt-outobj.melt:296:/ clear");
	     /*clear *//*_.OUTPUT_PREDEF__V40*/ meltfptr[33] = 0;
	  /*^clear */
	     /*clear *//*_.OUTPUT_C_CODE__V43*/ meltfptr[40] = 0;
	  /*^clear */
	     /*clear *//*_.OUTPUT_PREDEF__V44*/ meltfptr[41] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-outobj.melt:327:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
   /*_#IS_A__L17*/ meltfnum[14] =
	    melt_is_instance_of ((melt_ptr_t) ( /*_.RECV__V2*/ meltfptr[1]),
				 (melt_ptr_t) (( /*!CLASS_OBJINITUNIQUEOBJECT */ meltfrout->tabval[7])));;
	  MELT_LOCATION ("warmelt-outobj.melt:327:/ cond");
	  /*cond */ if ( /*_#IS_A__L17*/ meltfnum[14])	/*then */
	    {
	      /*^cond.then */
	      /*^block */
	      /*anyblock */
	      {


		{
		  MELT_LOCATION ("warmelt-outobj.melt:329:/ locexp");
		  /*add2sbuf_strconst */
		    meltgc_add_strbuf ((melt_ptr_t)
				       ( /*_.SBUF__V3*/ meltfptr[2]),
				       ("/*uniqueobj*/ if (!"));
		}
		;
		MELT_LOCATION ("warmelt-outobj.melt:330:/ checksignal");
		MELT_CHECK_SIGNAL ();
		;
		/*^msend */
		/*msend */
		{
		  union meltparam_un argtab[3];
		  memset (&argtab, 0, sizeof (argtab));
		  /*^ojbmsend.arg */
		  argtab[0].meltbp_aptr = /*nil */ (melt_ptr_t *) NULL;
		  /*^ojbmsend.arg */
		  argtab[1].meltbp_aptr =
		    (melt_ptr_t *) & /*_.SBUF__V3*/ meltfptr[2];
		  /*^ojbmsend.arg */
		  argtab[2].meltbp_long = /*_#DEPTH__L1*/ meltfnum[0];
		  /*_.OUTPUT_C_CODE__V45*/ meltfptr[33] =
		    meltgc_send ((melt_ptr_t)
				 ( /*_.OLOCVAR__V17*/ meltfptr[16]),
				 (melt_ptr_t) (( /*!OUTPUT_C_CODE */
						meltfrout->tabval[6])),
				 (MELTBPARSTR_PTR MELTBPARSTR_PTR
				  MELTBPARSTR_LONG ""), argtab, "",
				 (union meltparam_un *) 0);
		}
		;

		{
		  MELT_LOCATION ("warmelt-outobj.melt:331:/ locexp");
		  /*add2sbuf_strconst */
		    meltgc_add_strbuf ((melt_ptr_t)
				       ( /*_.SBUF__V3*/ meltfptr[2]), (") "));
		}
		;

		{
		  MELT_LOCATION ("warmelt-outobj.melt:332:/ locexp");
		  meltgc_strbuf_add_indent ((melt_ptr_t)
					    ( /*_.SBUF__V3*/ meltfptr[2]),
					    ( /*_#DEPTH__L1*/ meltfnum[0]),
					    0);
		}
		;
		MELT_LOCATION ("warmelt-outobj.melt:328:/ quasiblock");


		/*epilog */

		MELT_LOCATION ("warmelt-outobj.melt:327:/ clear");
	       /*clear *//*_.OUTPUT_C_CODE__V45*/ meltfptr[33] = 0;
	      }
	      ;
	    }			/*noelse */
	  ;
   /*_#I__L18*/ meltfnum[11] =
	    (( /*_#DEPTH__L1*/ meltfnum[0]) + (1));;
	  MELT_LOCATION ("warmelt-outobj.melt:334:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^msend */
	  /*msend */
	  {
	    union meltparam_un argtab[3];
	    memset (&argtab, 0, sizeof (argtab));
	    /*^ojbmsend.arg */
	    argtab[0].meltbp_aptr = /*nil */ (melt_ptr_t *) NULL;
	    /*^ojbmsend.arg */
	    argtab[1].meltbp_aptr =
	      (melt_ptr_t *) & /*_.SBUF__V3*/ meltfptr[2];
	    /*^ojbmsend.arg */
	    argtab[2].meltbp_long = /*_#I__L18*/ meltfnum[11];
	    /*_.OUTPUT_C_CODE__V46*/ meltfptr[40] =
	      meltgc_send ((melt_ptr_t) ( /*_.OLOCVAR__V17*/ meltfptr[16]),
			   (melt_ptr_t) (( /*!OUTPUT_C_CODE */ meltfrout->
					  tabval[6])),
			   (MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_LONG
			    ""), argtab, "", (union meltparam_un *) 0);
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:335:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
				 (" = (melt_ptr_t )&"));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:336:/ locexp");
	    /*add2sbuf_string */
	      meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
				 melt_string_str ((melt_ptr_t)
						  ( /*_.PTRSTR__V4*/
						   meltfptr[3])));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:337:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
				 ("->"));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:338:/ locexp");
	    /*add2sbuf_string */
	      meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
				 melt_string_str ((melt_ptr_t)
						  ( /*_.ONAME__V16*/
						   meltfptr[15])));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:339:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
				 (";"));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:340:/ locexp");
	    meltgc_strbuf_add_indent ((melt_ptr_t)
				      ( /*_.SBUF__V3*/ meltfptr[2]),
				      ( /*_#DEPTH__L1*/ meltfnum[0]), 0);
	  }
	  ;
	  MELT_LOCATION ("warmelt-outobj.melt:324:/ quasiblock");


	  /*epilog */

	  MELT_LOCATION ("warmelt-outobj.melt:296:/ clear");
	     /*clear *//*_#IS_A__L17*/ meltfnum[14] = 0;
	  /*^clear */
	     /*clear *//*_#I__L18*/ meltfnum[11] = 0;
	  /*^clear */
	     /*clear *//*_.OUTPUT_C_CODE__V46*/ meltfptr[40] = 0;
	}
	;
      }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:346:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
			   (" if (MELT_LIKELY(!melt_prohibit_garbcoll)) melt_assertmsg(\"iniobj\
 check.discr isobj "));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:347:/ locexp");
      /*add2sbuf_string */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
			   melt_string_str ((melt_ptr_t)
					    ( /*_.ONAME__V16*/
					     meltfptr[15])));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:348:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
			   ("\", melt_magic_discr ((melt_ptr_t) ("));
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:349:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^msend */
    /*msend */
    {
      union meltparam_un argtab[3];
      memset (&argtab, 0, sizeof (argtab));
      /*^ojbmsend.arg */
      argtab[0].meltbp_aptr = /*nil */ (melt_ptr_t *) NULL;
      /*^ojbmsend.arg */
      argtab[1].meltbp_aptr = (melt_ptr_t *) & /*_.SBUF__V3*/ meltfptr[2];
      /*^ojbmsend.arg */
      argtab[2].meltbp_long = /*_#DEPTH__L1*/ meltfnum[0];
      /*_.OUTPUT_C_CODE__V47*/ meltfptr[41] =
	meltgc_send ((melt_ptr_t) ( /*_.ODISCR__V15*/ meltfptr[14]),
		     (melt_ptr_t) (( /*!OUTPUT_C_CODE */ meltfrout->
				    tabval[6])),
		     (MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_LONG ""),
		     argtab, "", (union meltparam_un *) 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:350:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
			   (")) == MELTOBMAG_OBJECT);"));
    }
    ;
 /*_#I__L19*/ meltfnum[13] =
      (( /*_#DEPTH__L1*/ meltfnum[0]) + (1));;

    {
      MELT_LOCATION ("warmelt-outobj.melt:351:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
				( /*_#I__L19*/ meltfnum[13]), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:353:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
			   (" if (MELT_LIKELY(!melt_prohibit_garbcoll)) melt_assertmsg(\"iniobj\
 check.discr objmagic "));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:354:/ locexp");
      /*add2sbuf_string */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
			   melt_string_str ((melt_ptr_t)
					    ( /*_.ONAME__V16*/
					     meltfptr[15])));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:355:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
			   ("\", ((meltobject_ptr_t) ("));
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:356:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^msend */
    /*msend */
    {
      union meltparam_un argtab[3];
      memset (&argtab, 0, sizeof (argtab));
      /*^ojbmsend.arg */
      argtab[0].meltbp_aptr = /*nil */ (melt_ptr_t *) NULL;
      /*^ojbmsend.arg */
      argtab[1].meltbp_aptr = (melt_ptr_t *) & /*_.SBUF__V3*/ meltfptr[2];
      /*^ojbmsend.arg */
      argtab[2].meltbp_long = /*_#DEPTH__L1*/ meltfnum[0];
      /*_.OUTPUT_C_CODE__V48*/ meltfptr[33] =
	meltgc_send ((melt_ptr_t) ( /*_.ODISCR__V15*/ meltfptr[14]),
		     (melt_ptr_t) (( /*!OUTPUT_C_CODE */ meltfrout->
				    tabval[6])),
		     (MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_LONG ""),
		     argtab, "", (union meltparam_un *) 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:357:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
			   ("))->meltobj_magic == MELTOBMAG_OBJECT);"));
    }
    ;
 /*_#I__L20*/ meltfnum[14] =
      (( /*_#DEPTH__L1*/ meltfnum[0]) + (1));;

    {
      MELT_LOCATION ("warmelt-outobj.melt:358:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
				( /*_#I__L20*/ meltfnum[14]), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:360:/ locexp");
      /*add2sbuf_string */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
			   melt_string_str ((melt_ptr_t)
					    ( /*_.PTRSTR__V4*/ meltfptr[3])));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:361:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
			   ("->"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:362:/ locexp");
      /*add2sbuf_string */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
			   melt_string_str ((melt_ptr_t)
					    ( /*_.ONAME__V16*/
					     meltfptr[15])));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:363:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
			   (".meltobj_class = (meltobject_ptr_t)("));
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:364:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^msend */
    /*msend */
    {
      union meltparam_un argtab[3];
      memset (&argtab, 0, sizeof (argtab));
      /*^ojbmsend.arg */
      argtab[0].meltbp_aptr = /*nil */ (melt_ptr_t *) NULL;
      /*^ojbmsend.arg */
      argtab[1].meltbp_aptr = (melt_ptr_t *) & /*_.SBUF__V3*/ meltfptr[2];
      /*^ojbmsend.arg */
      argtab[2].meltbp_long = /*_#DEPTH__L1*/ meltfnum[0];
      /*_.OUTPUT_C_CODE__V49*/ meltfptr[40] =
	meltgc_send ((melt_ptr_t) ( /*_.ODISCR__V15*/ meltfptr[14]),
		     (melt_ptr_t) (( /*!OUTPUT_C_CODE */ meltfrout->
				    tabval[6])),
		     (MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_LONG ""),
		     argtab, "", (union meltparam_un *) 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:365:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
			   (");"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:366:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
				( /*_#DEPTH__L1*/ meltfnum[0]), 0);
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:367:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^cond */
    /*cond */ if ( /*_.ODOBNUM__V33*/ meltfptr[31])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{


	  {
	    MELT_LOCATION ("warmelt-outobj.melt:369:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
				 (" "));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:370:/ locexp");
	    /*add2sbuf_string */
	      meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
				 melt_string_str ((melt_ptr_t)
						  ( /*_.PTRSTR__V4*/
						   meltfptr[3])));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:371:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
				 ("->"));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:372:/ locexp");
	    /*add2sbuf_string */
	      meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
				 melt_string_str ((melt_ptr_t)
						  ( /*_.ONAME__V16*/
						   meltfptr[15])));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:373:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
				 (".obj_num = "));
	  }
	  ;
	  MELT_LOCATION ("warmelt-outobj.melt:374:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
   /*_#IS_INTEGERBOX__L21*/ meltfnum[11] =
	    (melt_magic_discr
	     ((melt_ptr_t) ( /*_.ODOBNUM__V33*/ meltfptr[31])) ==
	     MELTOBMAG_INT);;
	  MELT_LOCATION ("warmelt-outobj.melt:374:/ cond");
	  /*cond */ if ( /*_#IS_INTEGERBOX__L21*/ meltfnum[11])	/*then */
	    {
	      /*^cond.then */
	      /*^block */
	      /*anyblock */
	      {

     /*_#GET_INT__L22*/ meltfnum[21] =
		  (melt_get_int
		   ((melt_ptr_t) ( /*_.ODOBNUM__V33*/ meltfptr[31])));;



		{
		  MELT_LOCATION ("warmelt-outobj.melt:375:/ locexp");
		  meltgc_add_strbuf_dec ((melt_ptr_t)
					 ( /*_.SBUF__V3*/ meltfptr[2]),
					 ( /*_#GET_INT__L22*/ meltfnum[21]));
		}
		;
	       /*clear *//*_.IFELSE___V50*/ meltfptr[49] = 0;
		/*epilog */

		MELT_LOCATION ("warmelt-outobj.melt:374:/ clear");
	       /*clear *//*_#GET_INT__L22*/ meltfnum[21] = 0;
	      }
	      ;
	    }
	  else
	    {			/*^cond.else */

	      /*^block */
	      /*anyblock */
	      {

		MELT_LOCATION ("warmelt-outobj.melt:376:/ checksignal");
		MELT_CHECK_SIGNAL ();
		;
     /*_#IS_A__L23*/ meltfnum[21] =
		  melt_is_instance_of ((melt_ptr_t)
				       ( /*_.ODOBNUM__V33*/ meltfptr[31]),
				       (melt_ptr_t) (( /*!CLASS_SYMBOL */
						      meltfrout->
						      tabval[8])));;
		MELT_LOCATION ("warmelt-outobj.melt:376:/ cond");
		/*cond */ if ( /*_#IS_A__L23*/ meltfnum[21])	/*then */
		  {
		    /*^cond.then */
		    /*^block */
		    /*anyblock */
		    {

		      MELT_LOCATION ("warmelt-outobj.melt:377:/ getslot");
		      {
			melt_ptr_t slot = NULL, obj = NULL;
			obj =
			  (melt_ptr_t) ( /*_.ODOBNUM__V33*/ meltfptr[31])
			  /*=obj*/ ;
			melt_object_get_field (slot, obj, 1, "NAMED_NAME");
	/*_.NAMED_NAME__V52*/ meltfptr[51] = slot;
		      };
		      ;



		      {
			/*^locexp */
			/*add2sbuf_string */
			  meltgc_add_strbuf ((melt_ptr_t)
					     ( /*_.SBUF__V3*/ meltfptr[2]),
					     melt_string_str ((melt_ptr_t)
							      ( /*_.NAMED_NAME__V52*/ meltfptr[51])));
		      }
		      ;
		 /*clear *//*_.IFELSE___V51*/ meltfptr[50] = 0;
		      /*epilog */

		      MELT_LOCATION ("warmelt-outobj.melt:376:/ clear");
		 /*clear *//*_.NAMED_NAME__V52*/ meltfptr[51] = 0;
		    }
		    ;
		  }
		else
		  {		/*^cond.else */

		    /*^block */
		    /*anyblock */
		    {


#if MELT_HAVE_DEBUG
		      MELT_LOCATION ("warmelt-outobj.melt:379:/ cppif.then");
		      /*^block */
		      /*anyblock */
		      {


			{
			  /*^locexp */
			  /*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
			  melt_dbgcounter++;
#endif
			  ;
			}
			;
			/*^checksignal */
			MELT_CHECK_SIGNAL ();
			;
	 /*_#MELT_NEED_DBG__L24*/ meltfnum[23] =
			  /*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
			  ( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
			  0	/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
			  ;;
			MELT_LOCATION ("warmelt-outobj.melt:379:/ cond");
			/*cond */ if ( /*_#MELT_NEED_DBG__L24*/ meltfnum[23])	/*then */
			  {
			    /*^cond.then */
			    /*^block */
			    /*anyblock */
			    {

	   /*_#THE_MELTCALLCOUNT__L25*/ meltfnum[24] =
#ifdef meltcallcount
				meltcallcount	/* the_meltcallcount */
#else
				0L
#endif /* meltcallcount the_meltcallcount */
				;;
			      MELT_LOCATION
				("warmelt-outobj.melt:379:/ checksignal");
			      MELT_CHECK_SIGNAL ();
			      ;
			      /*^apply */
			      /*apply */
			      {
				union meltparam_un argtab[5];
				memset (&argtab, 0, sizeof (argtab));
				/*^apply.arg */
				argtab[0].meltbp_long =
				  /*_#THE_MELTCALLCOUNT__L25*/ meltfnum[24];
				/*^apply.arg */
				argtab[1].meltbp_cstring =
				  "warmelt-outobj.melt";
				/*^apply.arg */
				argtab[2].meltbp_long = 379;
				/*^apply.arg */
				argtab[3].meltbp_cstring =
				  "outcinitfill_objinitobject unexpected odobnum=";
				/*^apply.arg */
				argtab[4].meltbp_aptr =
				  (melt_ptr_t *) & /*_.ODOBNUM__V33*/
				  meltfptr[31];
				/*_.MELT_DEBUG_FUN__V55*/ meltfptr[54] =
				  melt_apply ((meltclosure_ptr_t)
					      (( /*!MELT_DEBUG_FUN */
						meltfrout->tabval[1])),
					      (melt_ptr_t) (( /*nil */ NULL)),
					      (MELTBPARSTR_LONG
					       MELTBPARSTR_CSTRING
					       MELTBPARSTR_LONG
					       MELTBPARSTR_CSTRING
					       MELTBPARSTR_PTR ""), argtab,
					      "", (union meltparam_un *) 0);
			      }
			      ;
			      /*_.IF___V54*/ meltfptr[53] =
				/*_.MELT_DEBUG_FUN__V55*/ meltfptr[54];;
			      /*epilog */

			      MELT_LOCATION
				("warmelt-outobj.melt:379:/ clear");
		     /*clear *//*_#THE_MELTCALLCOUNT__L25*/
				meltfnum[24] = 0;
			      /*^clear */
		     /*clear *//*_.MELT_DEBUG_FUN__V55*/ meltfptr[54]
				= 0;
			    }
			    ;
			  }
			else
			  {	/*^cond.else */

	  /*_.IF___V54*/ meltfptr[53] = NULL;;
			  }
			;
			MELT_LOCATION
			  ("warmelt-outobj.melt:379:/ quasiblock");


			/*_.PROGN___V56*/ meltfptr[54] =
			  /*_.IF___V54*/ meltfptr[53];;
			/*^compute */
			/*_.IFCPP___V53*/ meltfptr[51] =
			  /*_.PROGN___V56*/ meltfptr[54];;
			/*epilog */

			MELT_LOCATION ("warmelt-outobj.melt:379:/ clear");
		   /*clear *//*_#MELT_NEED_DBG__L24*/ meltfnum[23] = 0;
			/*^clear */
		   /*clear *//*_.IF___V54*/ meltfptr[53] = 0;
			/*^clear */
		   /*clear *//*_.PROGN___V56*/ meltfptr[54] = 0;
		      }

#else /*MELT_HAVE_DEBUG */
		      /*^cppif.else */
		      /*_.IFCPP___V53*/ meltfptr[51] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
		      ;

#if MELT_HAVE_DEBUG
		      MELT_LOCATION ("warmelt-outobj.melt:380:/ cppif.then");
		      /*^block */
		      /*anyblock */
		      {

			/*^checksignal */
			MELT_CHECK_SIGNAL ();
			;
			/*^cond */
			/*cond */ if (( /*nil */ NULL))	/*then */
			  {
			    /*^cond.then */
			    /*_.IFELSE___V58*/ meltfptr[54] =
			      ( /*nil */ NULL);;
			  }
			else
			  {
			    MELT_LOCATION
			      ("warmelt-outobj.melt:380:/ cond.else");

			    /*^block */
			    /*anyblock */
			    {




			      {
				/*^locexp */
				melt_assert_failed (("outcinitfill_objinitobject unexpected odobnum"), ("warmelt-outobj.melt") ? ("warmelt-outobj.melt") : __FILE__, (380) ? (380) : __LINE__, __FUNCTION__);
				;
			      }
			      ;
		     /*clear *//*_.IFELSE___V58*/ meltfptr[54] = 0;
			      /*epilog */
			    }
			    ;
			  }
			;
			/*^compute */
			/*_.IFCPP___V57*/ meltfptr[53] =
			  /*_.IFELSE___V58*/ meltfptr[54];;
			/*epilog */

			MELT_LOCATION ("warmelt-outobj.melt:380:/ clear");
		   /*clear *//*_.IFELSE___V58*/ meltfptr[54] = 0;
		      }

#else /*MELT_HAVE_DEBUG */
		      /*^cppif.else */
		      /*_.IFCPP___V57*/ meltfptr[53] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
		      ;
		      MELT_LOCATION ("warmelt-outobj.melt:378:/ quasiblock");


		      /*_.PROGN___V59*/ meltfptr[54] =
			/*_.IFCPP___V57*/ meltfptr[53];;
		      /*^compute */
		      /*_.IFELSE___V51*/ meltfptr[50] =
			/*_.PROGN___V59*/ meltfptr[54];;
		      /*epilog */

		      MELT_LOCATION ("warmelt-outobj.melt:376:/ clear");
		 /*clear *//*_.IFCPP___V53*/ meltfptr[51] = 0;
		      /*^clear */
		 /*clear *//*_.IFCPP___V57*/ meltfptr[53] = 0;
		      /*^clear */
		 /*clear *//*_.PROGN___V59*/ meltfptr[54] = 0;
		    }
		    ;
		  }
		;
		/*_.IFELSE___V50*/ meltfptr[49] =
		  /*_.IFELSE___V51*/ meltfptr[50];;
		/*epilog */

		MELT_LOCATION ("warmelt-outobj.melt:374:/ clear");
	       /*clear *//*_#IS_A__L23*/ meltfnum[21] = 0;
		/*^clear */
	       /*clear *//*_.IFELSE___V51*/ meltfptr[50] = 0;
	      }
	      ;
	    }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:382:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
				 (";"));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:383:/ locexp");
	    meltgc_strbuf_add_indent ((melt_ptr_t)
				      ( /*_.SBUF__V3*/ meltfptr[2]),
				      ( /*_#DEPTH__L1*/ meltfnum[0]), 0);
	  }
	  ;
	  MELT_LOCATION ("warmelt-outobj.melt:368:/ quasiblock");


	  /*epilog */

	  MELT_LOCATION ("warmelt-outobj.melt:367:/ clear");
	     /*clear *//*_#IS_INTEGERBOX__L21*/ meltfnum[11] = 0;
	  /*^clear */
	     /*clear *//*_.IFELSE___V50*/ meltfptr[49] = 0;
	}
	;
      }				/*noelse */
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:385:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]), (" "));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:386:/ locexp");
      /*add2sbuf_string */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
			   melt_string_str ((melt_ptr_t)
					    ( /*_.PTRSTR__V4*/ meltfptr[3])));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:387:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
			   ("->"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:388:/ locexp");
      /*add2sbuf_string */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
			   melt_string_str ((melt_ptr_t)
					    ( /*_.ONAME__V16*/
					     meltfptr[15])));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:389:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
			   (".obj_hash = "));
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:390:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^cond */
    /*cond */ if ( /*_.ODHASH__V29*/ meltfptr[21])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

   /*_#GET_INT__L26*/ meltfnum[24] =
	    (melt_get_int ((melt_ptr_t) ( /*_.ODHASH__V29*/ meltfptr[21])));;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:391:/ locexp");
	    meltgc_add_strbuf_dec ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
				   ( /*_#GET_INT__L26*/ meltfnum[24]));
	  }
	  ;
	  /*epilog */

	  MELT_LOCATION ("warmelt-outobj.melt:390:/ clear");
	     /*clear *//*_#GET_INT__L26*/ meltfnum[24] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

	/*^block */
	/*anyblock */
	{


	  {
	    MELT_LOCATION ("warmelt-outobj.melt:392:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
				 ("melt_nonzerohash ()"));
	  }
	  ;
	  /*epilog */
	}
	;
      }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:393:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]), (";"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:394:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
				( /*_#DEPTH__L1*/ meltfnum[0]), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:395:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]), (" "));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:396:/ locexp");
      /*add2sbuf_string */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
			   melt_string_str ((melt_ptr_t)
					    ( /*_.PTRSTR__V4*/ meltfptr[3])));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:397:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
			   ("->"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:398:/ locexp");
      /*add2sbuf_string */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
			   melt_string_str ((melt_ptr_t)
					    ( /*_.ONAME__V16*/
					     meltfptr[15])));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:399:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
			   (".obj_len = "));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:400:/ locexp");
      meltgc_add_strbuf_dec ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
			     ( /*_#NBSLOTS__L11*/ meltfnum[7]));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:401:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]), (";"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:402:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
				( /*_#DEPTH__L1*/ meltfnum[0]), 0);
    }
    ;

    MELT_LOCATION ("warmelt-outobj.melt:277:/ clear");
	   /*clear *//*_.ODLOC__V27*/ meltfptr[25] = 0;
    /*^clear */
	   /*clear *//*_.ODHASH__V29*/ meltfptr[21] = 0;
    /*^clear */
	   /*clear *//*_.ODSLOTS__V31*/ meltfptr[29] = 0;
    /*^clear */
	   /*clear *//*_.ODOBNUM__V33*/ meltfptr[31] = 0;
    /*^clear */
	   /*clear *//*_#IS_MULTIPLE__L10*/ meltfnum[3] = 0;
    /*^clear */
	   /*clear *//*_#NBSLOTS__L11*/ meltfnum[7] = 0;
    /*^clear */
	   /*clear *//*_.IF___V38*/ meltfptr[36] = 0;
    /*^clear */
	   /*clear *//*_.OUTPUT_C_CODE__V47*/ meltfptr[41] = 0;
    /*^clear */
	   /*clear *//*_#I__L19*/ meltfnum[13] = 0;
    /*^clear */
	   /*clear *//*_.OUTPUT_C_CODE__V48*/ meltfptr[33] = 0;
    /*^clear */
	   /*clear *//*_#I__L20*/ meltfnum[14] = 0;
    /*^clear */
	   /*clear *//*_.OUTPUT_C_CODE__V49*/ meltfptr[40] = 0;
    MELT_LOCATION ("warmelt-outobj.melt:405:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#STRBUF_USEDLENGTH__L27*/ meltfnum[23] =
      melt_strbuf_usedlength ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]));;
    MELT_LOCATION ("warmelt-outobj.melt:405:/ cond");
    /*cond */ if (
		   /*ifisa */
		   melt_is_instance_of ((melt_ptr_t)
					(( /*!BUFFER_LIMIT_CONT */ meltfrout->
					  tabval[9])),
					(melt_ptr_t) (( /*!CLASS_REFERENCE */
						       meltfrout->
						       tabval[10])))
      )				/*then */
      {
	/*^cond.then */
	/*^getslot */
	{
	  melt_ptr_t slot = NULL, obj = NULL;
	  obj =
	    (melt_ptr_t) (( /*!BUFFER_LIMIT_CONT */ meltfrout->
			   tabval[9])) /*=obj*/ ;
	  melt_object_get_field (slot, obj, 0, "REFERENCED_VALUE");
   /*_.REFERENCED_VALUE__V60*/ meltfptr[51] = slot;
	};
	;
      }
    else
      {				/*^cond.else */

  /*_.REFERENCED_VALUE__V60*/ meltfptr[51] = NULL;;
      }
    ;
    /*^compute */
 /*_#GET_INT__L28*/ meltfnum[21] =
      (melt_get_int
       ((melt_ptr_t) ( /*_.REFERENCED_VALUE__V60*/ meltfptr[51])));;
    /*^compute */
 /*_#IRAW__L29*/ meltfnum[11] =
      (( /*_#GET_INT__L28*/ meltfnum[21]) / (2));;
    /*^compute */
 /*_#I__L30*/ meltfnum[24] =
      (( /*_#STRBUF_USEDLENGTH__L27*/ meltfnum[23]) >
       ( /*_#IRAW__L29*/ meltfnum[11]));;
    MELT_LOCATION ("warmelt-outobj.melt:405:/ cond");
    /*cond */ if ( /*_#I__L30*/ meltfnum[24])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{


#if MELT_HAVE_DEBUG
	  MELT_LOCATION ("warmelt-outobj.melt:406:/ cppif.then");
	  /*^block */
	  /*anyblock */
	  {


	    {
	      /*^locexp */
	      /*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	      melt_dbgcounter++;
#endif
	      ;
	    }
	    ;
	    /*^checksignal */
	    MELT_CHECK_SIGNAL ();
	    ;
     /*_#MELT_NEED_DBG__L31*/ meltfnum[3] =
	      /*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	      ( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	      0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	      ;;
	    MELT_LOCATION ("warmelt-outobj.melt:406:/ cond");
	    /*cond */ if ( /*_#MELT_NEED_DBG__L31*/ meltfnum[3])	/*then */
	      {
		/*^cond.then */
		/*^block */
		/*anyblock */
		{

       /*_#THE_MELTCALLCOUNT__L32*/ meltfnum[7] =
#ifdef meltcallcount
		    meltcallcount	/* the_meltcallcount */
#else
		    0L
#endif /* meltcallcount the_meltcallcount */
		    ;;
		  MELT_LOCATION ("warmelt-outobj.melt:406:/ checksignal");
		  MELT_CHECK_SIGNAL ();
		  ;
		  /*^apply */
		  /*apply */
		  {
		    union meltparam_un argtab[5];
		    memset (&argtab, 0, sizeof (argtab));
		    /*^apply.arg */
		    argtab[0].meltbp_long =
		      /*_#THE_MELTCALLCOUNT__L32*/ meltfnum[7];
		    /*^apply.arg */
		    argtab[1].meltbp_cstring = "warmelt-outobj.melt";
		    /*^apply.arg */
		    argtab[2].meltbp_long = 406;
		    /*^apply.arg */
		    argtab[3].meltbp_cstring =
		      "outcinitfill_objinitobject huge sbuf=";
		    /*^apply.arg */
		    argtab[4].meltbp_aptr =
		      (melt_ptr_t *) & /*_.SBUF__V3*/ meltfptr[2];
		    /*_.MELT_DEBUG_FUN__V64*/ meltfptr[49] =
		      melt_apply ((meltclosure_ptr_t)
				  (( /*!MELT_DEBUG_FUN */ meltfrout->
				    tabval[1])),
				  (melt_ptr_t) (( /*nil */ NULL)),
				  (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_PTR ""), argtab, "",
				  (union meltparam_un *) 0);
		  }
		  ;
		  /*_.IF___V63*/ meltfptr[50] =
		    /*_.MELT_DEBUG_FUN__V64*/ meltfptr[49];;
		  /*epilog */

		  MELT_LOCATION ("warmelt-outobj.melt:406:/ clear");
		 /*clear *//*_#THE_MELTCALLCOUNT__L32*/ meltfnum[7] = 0;
		  /*^clear */
		 /*clear *//*_.MELT_DEBUG_FUN__V64*/ meltfptr[49] = 0;
		}
		;
	      }
	    else
	      {			/*^cond.else */

      /*_.IF___V63*/ meltfptr[50] = NULL;;
	      }
	    ;
	    MELT_LOCATION ("warmelt-outobj.melt:406:/ quasiblock");


	    /*_.PROGN___V65*/ meltfptr[25] = /*_.IF___V63*/ meltfptr[50];;
	    /*^compute */
	    /*_.IFCPP___V62*/ meltfptr[54] = /*_.PROGN___V65*/ meltfptr[25];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-outobj.melt:406:/ clear");
	       /*clear *//*_#MELT_NEED_DBG__L31*/ meltfnum[3] = 0;
	    /*^clear */
	       /*clear *//*_.IF___V63*/ meltfptr[50] = 0;
	    /*^clear */
	       /*clear *//*_.PROGN___V65*/ meltfptr[25] = 0;
	  }

#else /*MELT_HAVE_DEBUG */
	  /*^cppif.else */
	  /*_.IFCPP___V62*/ meltfptr[54] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
	  ;

#if MELT_HAVE_DEBUG
	  MELT_LOCATION ("warmelt-outobj.melt:407:/ cppif.then");
	  /*^block */
	  /*anyblock */
	  {

	    /*^checksignal */
	    MELT_CHECK_SIGNAL ();
	    ;
     /*_#STRBUF_USEDLENGTH__L33*/ meltfnum[13] =
	      melt_strbuf_usedlength ((melt_ptr_t)
				      ( /*_.SBUF__V3*/ meltfptr[2]));;
	    MELT_LOCATION ("warmelt-outobj.melt:408:/ cond");
	    /*cond */ if (
			   /*ifisa */
			   melt_is_instance_of ((melt_ptr_t)
						(( /*!BUFFER_LIMIT_CONT */
						  meltfrout->tabval[9])),
						(melt_ptr_t) (( /*!CLASS_REFERENCE */ meltfrout->tabval[10])))
	      )			/*then */
	      {
		/*^cond.then */
		/*^getslot */
		{
		  melt_ptr_t slot = NULL, obj = NULL;
		  obj =
		    (melt_ptr_t) (( /*!BUFFER_LIMIT_CONT */ meltfrout->
				   tabval[9])) /*=obj*/ ;
		  melt_object_get_field (slot, obj, 0, "REFERENCED_VALUE");
       /*_.REFERENCED_VALUE__V67*/ meltfptr[29] = slot;
		};
		;
	      }
	    else
	      {			/*^cond.else */

      /*_.REFERENCED_VALUE__V67*/ meltfptr[29] = NULL;;
	      }
	    ;
	    /*^compute */
     /*_#GET_INT__L34*/ meltfnum[14] =
	      (melt_get_int
	       ((melt_ptr_t) ( /*_.REFERENCED_VALUE__V67*/ meltfptr[29])));;
	    /*^compute */
     /*_#I__L35*/ meltfnum[7] =
	      (( /*_#STRBUF_USEDLENGTH__L33*/ meltfnum[13]) <
	       ( /*_#GET_INT__L34*/ meltfnum[14]));;
	    MELT_LOCATION ("warmelt-outobj.melt:407:/ cond");
	    /*cond */ if ( /*_#I__L35*/ meltfnum[7])	/*then */
	      {
		/*^cond.then */
		/*_.IFELSE___V68*/ meltfptr[31] = ( /*nil */ NULL);;
	      }
	    else
	      {
		MELT_LOCATION ("warmelt-outobj.melt:407:/ cond.else");

		/*^block */
		/*anyblock */
		{




		  {
		    /*^locexp */
		    melt_assert_failed (("check limited sbuf"),
					("warmelt-outobj.melt")
					? ("warmelt-outobj.melt") : __FILE__,
					(407) ? (407) : __LINE__,
					__FUNCTION__);
		    ;
		  }
		  ;
		 /*clear *//*_.IFELSE___V68*/ meltfptr[31] = 0;
		  /*epilog */
		}
		;
	      }
	    ;
	    /*^compute */
	    /*_.IFCPP___V66*/ meltfptr[21] = /*_.IFELSE___V68*/ meltfptr[31];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-outobj.melt:407:/ clear");
	       /*clear *//*_#STRBUF_USEDLENGTH__L33*/ meltfnum[13] = 0;
	    /*^clear */
	       /*clear *//*_.REFERENCED_VALUE__V67*/ meltfptr[29] = 0;
	    /*^clear */
	       /*clear *//*_#GET_INT__L34*/ meltfnum[14] = 0;
	    /*^clear */
	       /*clear *//*_#I__L35*/ meltfnum[7] = 0;
	    /*^clear */
	       /*clear *//*_.IFELSE___V68*/ meltfptr[31] = 0;
	  }

#else /*MELT_HAVE_DEBUG */
	  /*^cppif.else */
	  /*_.IFCPP___V66*/ meltfptr[21] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
	  ;
	  MELT_LOCATION ("warmelt-outobj.melt:405:/ quasiblock");


	  /*_.PROGN___V69*/ meltfptr[36] = /*_.IFCPP___V66*/ meltfptr[21];;
	  /*^compute */
	  /*_.IFELSE___V61*/ meltfptr[53] = /*_.PROGN___V69*/ meltfptr[36];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-outobj.melt:405:/ clear");
	     /*clear *//*_.IFCPP___V62*/ meltfptr[54] = 0;
	  /*^clear */
	     /*clear *//*_.IFCPP___V66*/ meltfptr[21] = 0;
	  /*^clear */
	     /*clear *//*_.PROGN___V69*/ meltfptr[36] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

  /*_.IFELSE___V61*/ meltfptr[53] = NULL;;
      }
    ;
    /*^compute */
    /*_.LET___V13*/ meltfptr[8] = /*_.IFELSE___V61*/ meltfptr[53];;

    MELT_LOCATION ("warmelt-outobj.melt:267:/ clear");
	   /*clear *//*_.ODATA__V14*/ meltfptr[13] = 0;
    /*^clear */
	   /*clear *//*_.ODISCR__V15*/ meltfptr[14] = 0;
    /*^clear */
	   /*clear *//*_.ONAME__V16*/ meltfptr[15] = 0;
    /*^clear */
	   /*clear *//*_.OLOCVAR__V17*/ meltfptr[16] = 0;
    /*^clear */
	   /*clear *//*_.OIOPREDEF__V18*/ meltfptr[17] = 0;
    /*^clear */
	   /*clear *//*_.OCLASS__V19*/ meltfptr[18] = 0;
    /*^clear */
	   /*clear *//*_#DEPTHP1__L6*/ meltfnum[1] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V20*/ meltfptr[19] = 0;
    /*^clear */
	   /*clear *//*_.IF___V24*/ meltfptr[20] = 0;
    /*^clear */
	   /*clear *//*_#STRBUF_USEDLENGTH__L27*/ meltfnum[23] = 0;
    /*^clear */
	   /*clear *//*_.REFERENCED_VALUE__V60*/ meltfptr[51] = 0;
    /*^clear */
	   /*clear *//*_#GET_INT__L28*/ meltfnum[21] = 0;
    /*^clear */
	   /*clear *//*_#IRAW__L29*/ meltfnum[11] = 0;
    /*^clear */
	   /*clear *//*_#I__L30*/ meltfnum[24] = 0;
    /*^clear */
	   /*clear *//*_.IFELSE___V61*/ meltfptr[53] = 0;
    MELT_LOCATION ("warmelt-outobj.melt:263:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.LET___V13*/ meltfptr[8];;

    {
      MELT_LOCATION ("warmelt-outobj.melt:263:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V5*/ meltfptr[4] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V7*/ meltfptr[5] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V11*/ meltfptr[7] = 0;
    /*^clear */
	   /*clear *//*_.LET___V13*/ meltfptr[8] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("OUTCINITFILL_OBJINITOBJECT", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_13_warmelt_outobj_OUTCINITFILL_OBJINITOBJECT_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_13_warmelt_outobj_OUTCINITFILL_OBJINITOBJECT */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_14_warmelt_outobj_OUTCINITPREDEF_OBJINITOBJECT (meltclosure_ptr_t
							 meltclosp_,
							 melt_ptr_t
							 meltfirstargp_,
							 const
							 melt_argdescr_cell_t
							 meltxargdescr_[],
							 union meltparam_un *
							 meltxargtab_,
							 const
							 melt_argdescr_cell_t
							 meltxresdescr_[],
							 union meltparam_un *
							 meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_14_warmelt_outobj_OUTCINITPREDEF_OBJINITOBJECT_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_14_warmelt_outobj_OUTCINITPREDEF_OBJINITOBJECT_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 58
    melt_ptr_t mcfr_varptr[58];
#define MELTFRAM_NBVARNUM 29
    long mcfr_varnum[29];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_14_warmelt_outobj_OUTCINITPREDEF_OBJINITOBJECT is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct
	 frame_meltrout_14_warmelt_outobj_OUTCINITPREDEF_OBJINITOBJECT_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 58; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_14_warmelt_outobj_OUTCINITPREDEF_OBJINITOBJECT nbval 58*/
  meltfram__.mcfr_nbvar = 58 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("OUTCINITPREDEF_OBJINITOBJECT", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-outobj.melt:416:/ getarg");
 /*_.RECV__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.SBUF__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2])) !=
	      NULL);


  /*getarg#2 */
  /*^getarg */
  if (meltxargdescr_[1] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.PTRSTR__V4*/ meltfptr[3] =
    (meltxargtab_[1].meltbp_aptr) ? (*(meltxargtab_[1].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.PTRSTR__V4*/ meltfptr[3])) !=
	      NULL);


  /*getarg#3 */
  /*^getarg */
  if (meltxargdescr_[2] != MELTBPAR_LONG)
    goto lab_endgetargs;
 /*_#DEPTH__L1*/ meltfnum[0] = meltxargtab_[2].meltbp_long;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:417:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L2*/ meltfnum[1] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.RECV__V2*/ meltfptr[1]),
			     (melt_ptr_t) (( /*!CLASS_OBJINITOBJECT */
					    meltfrout->tabval[0])));;
      MELT_LOCATION ("warmelt-outobj.melt:417:/ cond");
      /*cond */ if ( /*_#IS_A__L2*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V6*/ meltfptr[5] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-outobj.melt:417:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("outcinitpredef_objinitobject check recv"),
				  ("warmelt-outobj.melt")
				  ? ("warmelt-outobj.melt") : __FILE__,
				  (417) ? (417) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V6*/ meltfptr[5] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V5*/ meltfptr[4] = /*_.IFELSE___V6*/ meltfptr[5];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:417:/ clear");
	     /*clear *//*_#IS_A__L2*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V6*/ meltfptr[5] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V5*/ meltfptr[4] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:418:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L3*/ meltfnum[1] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-outobj.melt:418:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L3*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L4*/ meltfnum[3] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-outobj.melt:418:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[8];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L4*/ meltfnum[3];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-outobj.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 418;
	      /*^apply.arg */
	      argtab[3].meltbp_aptr =
		(melt_ptr_t *) & /*_.RECV__V2*/ meltfptr[1];
	      /*^apply.arg */
	      argtab[4].meltbp_cstring = "outcinitpredef_objinitobject recv=";
	      /*^apply.arg */
	      argtab[5].meltbp_aptr =
		(melt_ptr_t *) & /*_.RECV__V2*/ meltfptr[1];
	      /*^apply.arg */
	      argtab[6].meltbp_cstring = " ptrstr=";
	      /*^apply.arg */
	      argtab[7].meltbp_aptr =
		(melt_ptr_t *) & /*_.PTRSTR__V4*/ meltfptr[3];
	      /*_.MELT_DEBUG_FUN__V9*/ meltfptr[8] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[1])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_PTR
			     MELTBPARSTR_CSTRING MELTBPARSTR_PTR
			     MELTBPARSTR_CSTRING MELTBPARSTR_PTR ""), argtab,
			    "", (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V8*/ meltfptr[7] = /*_.MELT_DEBUG_FUN__V9*/ meltfptr[8];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-outobj.melt:418:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L4*/ meltfnum[3] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V9*/ meltfptr[8] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V8*/ meltfptr[7] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-outobj.melt:418:/ quasiblock");


      /*_.PROGN___V10*/ meltfptr[8] = /*_.IF___V8*/ meltfptr[7];;
      /*^compute */
      /*_.IFCPP___V7*/ meltfptr[5] = /*_.PROGN___V10*/ meltfptr[8];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:418:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L3*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IF___V8*/ meltfptr[7] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V10*/ meltfptr[8] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V7*/ meltfptr[5] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:419:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_STRBUF__L5*/ meltfnum[3] =
	(melt_magic_discr ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2])) ==
	 MELTOBMAG_STRBUF);;
      MELT_LOCATION ("warmelt-outobj.melt:419:/ cond");
      /*cond */ if ( /*_#IS_STRBUF__L5*/ meltfnum[3])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V12*/ meltfptr[8] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-outobj.melt:419:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("outcinitpredef_objinitobject check sbuf"),
				  ("warmelt-outobj.melt")
				  ? ("warmelt-outobj.melt") : __FILE__,
				  (419) ? (419) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V12*/ meltfptr[8] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V11*/ meltfptr[7] = /*_.IFELSE___V12*/ meltfptr[8];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:419:/ clear");
	     /*clear *//*_#IS_STRBUF__L5*/ meltfnum[3] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V12*/ meltfptr[8] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V11*/ meltfptr[7] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:420:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_STRING__L6*/ meltfnum[1] =
	(melt_magic_discr ((melt_ptr_t) ( /*_.PTRSTR__V4*/ meltfptr[3])) ==
	 MELTOBMAG_STRING);;
      MELT_LOCATION ("warmelt-outobj.melt:420:/ cond");
      /*cond */ if ( /*_#IS_STRING__L6*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V14*/ meltfptr[13] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-outobj.melt:420:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("outcinitpredef_objinitobject check ptrstr"), ("warmelt-outobj.melt") ? ("warmelt-outobj.melt") : __FILE__, (420) ? (420) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V14*/ meltfptr[13] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V13*/ meltfptr[8] = /*_.IFELSE___V14*/ meltfptr[13];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:420:/ clear");
	     /*clear *//*_#IS_STRING__L6*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V14*/ meltfptr[13] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V13*/ meltfptr[8] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-outobj.melt:421:/ quasiblock");


    /*^getslot */
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.RECV__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 2, "OIE_DATA");
  /*_.ODATA__V16*/ meltfptr[15] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-outobj.melt:422:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.RECV__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 3, "OIE_DISCR");
  /*_.ODISCR__V17*/ meltfptr[16] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-outobj.melt:423:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.RECV__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 1, "OIE_CNAME");
  /*_.ONAME__V18*/ meltfptr[17] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-outobj.melt:424:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.RECV__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 4, "OIE_LOCVAR");
  /*_.OLOCVAR__V19*/ meltfptr[18] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-outobj.melt:425:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.RECV__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 5, "OIO_PREDEF");
  /*_.OIOPREDEF__V20*/ meltfptr[19] = slot;
    };
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:427:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L7*/ meltfnum[3] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.ODATA__V16*/ meltfptr[15]),
			     (melt_ptr_t) (( /*!CLASS_NREP_DATAINSTANCE */
					    meltfrout->tabval[2])));;
      MELT_LOCATION ("warmelt-outobj.melt:427:/ cond");
      /*cond */ if ( /*_#IS_A__L7*/ meltfnum[3])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V22*/ meltfptr[21] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-outobj.melt:427:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check odata"),
				  ("warmelt-outobj.melt")
				  ? ("warmelt-outobj.melt") : __FILE__,
				  (427) ? (427) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V22*/ meltfptr[21] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V21*/ meltfptr[20] = /*_.IFELSE___V22*/ meltfptr[21];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:427:/ clear");
	     /*clear *//*_#IS_A__L7*/ meltfnum[3] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V22*/ meltfptr[21] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V21*/ meltfptr[20] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:428:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L8*/ meltfnum[1] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-outobj.melt:428:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L8*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L9*/ meltfnum[3] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-outobj.melt:428:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L9*/ meltfnum[3];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-outobj.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 428;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring =
		"outcinitpredef_objinitobject oiopredef=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.OIOPREDEF__V20*/ meltfptr[19];
	      /*_.MELT_DEBUG_FUN__V25*/ meltfptr[24] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[1])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V24*/ meltfptr[23] =
	      /*_.MELT_DEBUG_FUN__V25*/ meltfptr[24];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-outobj.melt:428:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L9*/ meltfnum[3] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V25*/ meltfptr[24] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V24*/ meltfptr[23] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-outobj.melt:428:/ quasiblock");


      /*_.PROGN___V26*/ meltfptr[24] = /*_.IF___V24*/ meltfptr[23];;
      /*^compute */
      /*_.IFCPP___V23*/ meltfptr[21] = /*_.PROGN___V26*/ meltfptr[24];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:428:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L8*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IF___V24*/ meltfptr[23] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V26*/ meltfptr[24] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V23*/ meltfptr[21] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-outobj.melt:429:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#NULL__L10*/ meltfnum[3] =
      (( /*_.OIOPREDEF__V20*/ meltfptr[19]) == NULL);;
    MELT_LOCATION ("warmelt-outobj.melt:429:/ cond");
    /*cond */ if ( /*_#NULL__L10*/ meltfnum[3])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  /*^checksignal */
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^quasiblock */


	  /*_.RETVAL___V1*/ meltfptr[0] = ( /*nil */ NULL);;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:429:/ locexp");
	    /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
	    if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	      melt_warn_for_no_expected_secondary_results ();
	    /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
	    ;
	  }
	  ;
	  /*^finalreturn */
	  ;
	  /*finalret */ goto labend_rout;
	  /*_.IF___V27*/ meltfptr[23] = /*_.RETURN___V28*/ meltfptr[24];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-outobj.melt:429:/ clear");
	     /*clear *//*_.RETURN___V28*/ meltfptr[24] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

  /*_.IF___V27*/ meltfptr[23] = NULL;;
      }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:430:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#IS_A__L11*/ meltfnum[1] =
      melt_is_instance_of ((melt_ptr_t) ( /*_.OIOPREDEF__V20*/ meltfptr[19]),
			   (melt_ptr_t) (( /*!CLASS_NREP_NIL */ meltfrout->
					  tabval[3])));;
    MELT_LOCATION ("warmelt-outobj.melt:430:/ cond");
    /*cond */ if ( /*_#IS_A__L11*/ meltfnum[1])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  /*^checksignal */
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^quasiblock */


	  /*_.RETVAL___V1*/ meltfptr[0] = ( /*nil */ NULL);;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:430:/ locexp");
	    /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
	    if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	      melt_warn_for_no_expected_secondary_results ();
	    /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
	    ;
	  }
	  ;
	  /*^finalreturn */
	  ;
	  /*finalret */ goto labend_rout;
	  /*_.IF___V29*/ meltfptr[24] = /*_.RETURN___V30*/ meltfptr[29];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-outobj.melt:430:/ clear");
	     /*clear *//*_.RETURN___V30*/ meltfptr[29] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

  /*_.IF___V29*/ meltfptr[24] = NULL;;
      }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:431:/ quasiblock");


    /*^getslot */
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.ODATA__V16*/ meltfptr[15]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 0, "NREP_LOC");
  /*_.ODLOC__V32*/ meltfptr[31] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-outobj.melt:433:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[3];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.SBUF__V3*/ meltfptr[2];
      /*^apply.arg */
      argtab[1].meltbp_long = /*_#DEPTH__L1*/ meltfnum[0];
      /*^apply.arg */
      argtab[2].meltbp_cstring = "inipredef";
      /*_.OUTPUT_LOCATION__V33*/ meltfptr[32] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!OUTPUT_LOCATION */ meltfrout->tabval[4])),
		    (melt_ptr_t) ( /*_.ODLOC__V32*/ meltfptr[31]),
		    (MELTBPARSTR_PTR MELTBPARSTR_LONG MELTBPARSTR_CSTRING ""),
		    argtab, "", (union meltparam_un *) 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:434:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
			   ("/*inipredef "));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:435:/ locexp");
      /*add2sbuf_string */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
			   melt_string_str ((melt_ptr_t)
					    ( /*_.ONAME__V18*/
					     meltfptr[17])));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:436:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
			   ("*/"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:437:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
				( /*_#DEPTH__L1*/ meltfnum[0]), 0);
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:439:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#STRBUF_USEDLENGTH__L12*/ meltfnum[11] =
      melt_strbuf_usedlength ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]));;
    MELT_LOCATION ("warmelt-outobj.melt:439:/ cond");
    /*cond */ if (
		   /*ifisa */
		   melt_is_instance_of ((melt_ptr_t)
					(( /*!BUFFER_LIMIT_CONT */ meltfrout->
					  tabval[5])),
					(melt_ptr_t) (( /*!CLASS_REFERENCE */
						       meltfrout->tabval[6])))
      )				/*then */
      {
	/*^cond.then */
	/*^getslot */
	{
	  melt_ptr_t slot = NULL, obj = NULL;
	  obj =
	    (melt_ptr_t) (( /*!BUFFER_LIMIT_CONT */ meltfrout->
			   tabval[5])) /*=obj*/ ;
	  melt_object_get_field (slot, obj, 0, "REFERENCED_VALUE");
   /*_.REFERENCED_VALUE__V34*/ meltfptr[33] = slot;
	};
	;
      }
    else
      {				/*^cond.else */

  /*_.REFERENCED_VALUE__V34*/ meltfptr[33] = NULL;;
      }
    ;
    /*^compute */
 /*_#GET_INT__L13*/ meltfnum[12] =
      (melt_get_int
       ((melt_ptr_t) ( /*_.REFERENCED_VALUE__V34*/ meltfptr[33])));;
    /*^compute */
 /*_#IRAW__L14*/ meltfnum[13] =
      (( /*_#GET_INT__L13*/ meltfnum[12]) / (2));;
    /*^compute */
 /*_#I__L15*/ meltfnum[14] =
      (( /*_#STRBUF_USEDLENGTH__L12*/ meltfnum[11]) >
       ( /*_#IRAW__L14*/ meltfnum[13]));;
    MELT_LOCATION ("warmelt-outobj.melt:439:/ cond");
    /*cond */ if ( /*_#I__L15*/ meltfnum[14])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{


#if MELT_HAVE_DEBUG
	  MELT_LOCATION ("warmelt-outobj.melt:440:/ cppif.then");
	  /*^block */
	  /*anyblock */
	  {


	    {
	      /*^locexp */
	      /*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	      melt_dbgcounter++;
#endif
	      ;
	    }
	    ;
	    /*^checksignal */
	    MELT_CHECK_SIGNAL ();
	    ;
     /*_#MELT_NEED_DBG__L16*/ meltfnum[15] =
	      /*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	      ( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	      0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	      ;;
	    MELT_LOCATION ("warmelt-outobj.melt:440:/ cond");
	    /*cond */ if ( /*_#MELT_NEED_DBG__L16*/ meltfnum[15])	/*then */
	      {
		/*^cond.then */
		/*^block */
		/*anyblock */
		{

       /*_#THE_MELTCALLCOUNT__L17*/ meltfnum[16] =
#ifdef meltcallcount
		    meltcallcount	/* the_meltcallcount */
#else
		    0L
#endif /* meltcallcount the_meltcallcount */
		    ;;
		  MELT_LOCATION ("warmelt-outobj.melt:440:/ checksignal");
		  MELT_CHECK_SIGNAL ();
		  ;
		  /*^apply */
		  /*apply */
		  {
		    union meltparam_un argtab[5];
		    memset (&argtab, 0, sizeof (argtab));
		    /*^apply.arg */
		    argtab[0].meltbp_long =
		      /*_#THE_MELTCALLCOUNT__L17*/ meltfnum[16];
		    /*^apply.arg */
		    argtab[1].meltbp_cstring = "warmelt-outobj.melt";
		    /*^apply.arg */
		    argtab[2].meltbp_long = 440;
		    /*^apply.arg */
		    argtab[3].meltbp_cstring =
		      "outcinitpredef_objinitobject huge implbuf=";
		    /*^apply.arg */
		    argtab[4].meltbp_aptr =
		      (melt_ptr_t *) & /*_.SBUF__V3*/ meltfptr[2];
		    /*_.MELT_DEBUG_FUN__V38*/ meltfptr[37] =
		      melt_apply ((meltclosure_ptr_t)
				  (( /*!MELT_DEBUG_FUN */ meltfrout->
				    tabval[1])),
				  (melt_ptr_t) (( /*nil */ NULL)),
				  (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_PTR ""), argtab, "",
				  (union meltparam_un *) 0);
		  }
		  ;
		  /*_.IF___V37*/ meltfptr[36] =
		    /*_.MELT_DEBUG_FUN__V38*/ meltfptr[37];;
		  /*epilog */

		  MELT_LOCATION ("warmelt-outobj.melt:440:/ clear");
		 /*clear *//*_#THE_MELTCALLCOUNT__L17*/ meltfnum[16] = 0;
		  /*^clear */
		 /*clear *//*_.MELT_DEBUG_FUN__V38*/ meltfptr[37] = 0;
		}
		;
	      }
	    else
	      {			/*^cond.else */

      /*_.IF___V37*/ meltfptr[36] = NULL;;
	      }
	    ;
	    MELT_LOCATION ("warmelt-outobj.melt:440:/ quasiblock");


	    /*_.PROGN___V39*/ meltfptr[37] = /*_.IF___V37*/ meltfptr[36];;
	    /*^compute */
	    /*_.IFCPP___V36*/ meltfptr[35] = /*_.PROGN___V39*/ meltfptr[37];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-outobj.melt:440:/ clear");
	       /*clear *//*_#MELT_NEED_DBG__L16*/ meltfnum[15] = 0;
	    /*^clear */
	       /*clear *//*_.IF___V37*/ meltfptr[36] = 0;
	    /*^clear */
	       /*clear *//*_.PROGN___V39*/ meltfptr[37] = 0;
	  }

#else /*MELT_HAVE_DEBUG */
	  /*^cppif.else */
	  /*_.IFCPP___V36*/ meltfptr[35] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
	  ;

#if MELT_HAVE_DEBUG
	  MELT_LOCATION ("warmelt-outobj.melt:441:/ cppif.then");
	  /*^block */
	  /*anyblock */
	  {

	    /*^checksignal */
	    MELT_CHECK_SIGNAL ();
	    ;
     /*_#STRBUF_USEDLENGTH__L18*/ meltfnum[16] =
	      melt_strbuf_usedlength ((melt_ptr_t)
				      ( /*_.SBUF__V3*/ meltfptr[2]));;
	    MELT_LOCATION ("warmelt-outobj.melt:442:/ cond");
	    /*cond */ if (
			   /*ifisa */
			   melt_is_instance_of ((melt_ptr_t)
						(( /*!BUFFER_LIMIT_CONT */
						  meltfrout->tabval[5])),
						(melt_ptr_t) (( /*!CLASS_REFERENCE */ meltfrout->tabval[6])))
	      )			/*then */
	      {
		/*^cond.then */
		/*^getslot */
		{
		  melt_ptr_t slot = NULL, obj = NULL;
		  obj =
		    (melt_ptr_t) (( /*!BUFFER_LIMIT_CONT */ meltfrout->
				   tabval[5])) /*=obj*/ ;
		  melt_object_get_field (slot, obj, 0, "REFERENCED_VALUE");
       /*_.REFERENCED_VALUE__V41*/ meltfptr[37] = slot;
		};
		;
	      }
	    else
	      {			/*^cond.else */

      /*_.REFERENCED_VALUE__V41*/ meltfptr[37] = NULL;;
	      }
	    ;
	    /*^compute */
     /*_#GET_INT__L19*/ meltfnum[15] =
	      (melt_get_int
	       ((melt_ptr_t) ( /*_.REFERENCED_VALUE__V41*/ meltfptr[37])));;
	    /*^compute */
     /*_#I__L20*/ meltfnum[19] =
	      (( /*_#STRBUF_USEDLENGTH__L18*/ meltfnum[16]) <
	       ( /*_#GET_INT__L19*/ meltfnum[15]));;
	    MELT_LOCATION ("warmelt-outobj.melt:441:/ cond");
	    /*cond */ if ( /*_#I__L20*/ meltfnum[19])	/*then */
	      {
		/*^cond.then */
		/*_.IFELSE___V42*/ meltfptr[41] = ( /*nil */ NULL);;
	      }
	    else
	      {
		MELT_LOCATION ("warmelt-outobj.melt:441:/ cond.else");

		/*^block */
		/*anyblock */
		{




		  {
		    /*^locexp */
		    melt_assert_failed (("check limited sbuf"),
					("warmelt-outobj.melt")
					? ("warmelt-outobj.melt") : __FILE__,
					(441) ? (441) : __LINE__,
					__FUNCTION__);
		    ;
		  }
		  ;
		 /*clear *//*_.IFELSE___V42*/ meltfptr[41] = 0;
		  /*epilog */
		}
		;
	      }
	    ;
	    /*^compute */
	    /*_.IFCPP___V40*/ meltfptr[36] = /*_.IFELSE___V42*/ meltfptr[41];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-outobj.melt:441:/ clear");
	       /*clear *//*_#STRBUF_USEDLENGTH__L18*/ meltfnum[16] = 0;
	    /*^clear */
	       /*clear *//*_.REFERENCED_VALUE__V41*/ meltfptr[37] = 0;
	    /*^clear */
	       /*clear *//*_#GET_INT__L19*/ meltfnum[15] = 0;
	    /*^clear */
	       /*clear *//*_#I__L20*/ meltfnum[19] = 0;
	    /*^clear */
	       /*clear *//*_.IFELSE___V42*/ meltfptr[41] = 0;
	  }

#else /*MELT_HAVE_DEBUG */
	  /*^cppif.else */
	  /*_.IFCPP___V40*/ meltfptr[36] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
	  ;
	  MELT_LOCATION ("warmelt-outobj.melt:439:/ quasiblock");


	  /*_.PROGN___V43*/ meltfptr[37] = /*_.IFCPP___V40*/ meltfptr[36];;
	  /*^compute */
	  /*_.IFELSE___V35*/ meltfptr[34] = /*_.PROGN___V43*/ meltfptr[37];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-outobj.melt:439:/ clear");
	     /*clear *//*_.IFCPP___V36*/ meltfptr[35] = 0;
	  /*^clear */
	     /*clear *//*_.IFCPP___V40*/ meltfptr[36] = 0;
	  /*^clear */
	     /*clear *//*_.PROGN___V43*/ meltfptr[37] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

  /*_.IFELSE___V35*/ meltfptr[34] = NULL;;
      }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:445:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#IS_A__L21*/ meltfnum[16] =
      melt_is_instance_of ((melt_ptr_t) ( /*_.OIOPREDEF__V20*/ meltfptr[19]),
			   (melt_ptr_t) (( /*!CLASS_SYMBOL */ meltfrout->
					  tabval[7])));;
    MELT_LOCATION ("warmelt-outobj.melt:445:/ cond");
    /*cond */ if ( /*_#IS_A__L21*/ meltfnum[16])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{


	  {
	    MELT_LOCATION ("warmelt-outobj.melt:446:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
				 ("if (!MELT_PREDEF("));
	  }
	  ;
	  MELT_LOCATION ("warmelt-outobj.melt:447:/ getslot");
	  {
	    melt_ptr_t slot = NULL, obj = NULL;
	    obj = (melt_ptr_t) ( /*_.OIOPREDEF__V20*/ meltfptr[19]) /*=obj*/ ;
	    melt_object_get_field (slot, obj, 1, "NAMED_NAME");
    /*_.NAMED_NAME__V45*/ meltfptr[35] = slot;
	  };
	  ;

	  {
	    /*^locexp */
	    /*add2sbuf_string */
	      meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
				 melt_string_str ((melt_ptr_t)
						  ( /*_.NAMED_NAME__V45*/
						   meltfptr[35])));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:448:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
				 (")) MELT_STORE_PREDEF("));
	  }
	  ;
	  MELT_LOCATION ("warmelt-outobj.melt:449:/ getslot");
	  {
	    melt_ptr_t slot = NULL, obj = NULL;
	    obj = (melt_ptr_t) ( /*_.OIOPREDEF__V20*/ meltfptr[19]) /*=obj*/ ;
	    melt_object_get_field (slot, obj, 1, "NAMED_NAME");
    /*_.NAMED_NAME__V46*/ meltfptr[36] = slot;
	  };
	  ;

	  {
	    /*^locexp */
	    /*add2sbuf_string */
	      meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
				 melt_string_str ((melt_ptr_t)
						  ( /*_.NAMED_NAME__V46*/
						   meltfptr[36])));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:450:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
				 (", (melt_ptr_t)&"));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:451:/ locexp");
	    /*add2sbuf_string */
	      meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
				 melt_string_str ((melt_ptr_t)
						  ( /*_.PTRSTR__V4*/
						   meltfptr[3])));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:452:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
				 ("->"));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:453:/ locexp");
	    /*add2sbuf_string */
	      meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
				 melt_string_str ((melt_ptr_t)
						  ( /*_.ONAME__V18*/
						   meltfptr[17])));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:454:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
				 (");"));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:455:/ locexp");
	    meltgc_strbuf_add_indent ((melt_ptr_t)
				      ( /*_.SBUF__V3*/ meltfptr[2]), (1), 0);
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:456:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
				 ("else {"));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:457:/ locexp");
	    meltgc_strbuf_add_indent ((melt_ptr_t)
				      ( /*_.SBUF__V3*/ meltfptr[2]), (2), 0);
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:458:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
				 ("MELTPREDEFIX(meltpredefinited,"));
	  }
	  ;
	  MELT_LOCATION ("warmelt-outobj.melt:459:/ getslot");
	  {
	    melt_ptr_t slot = NULL, obj = NULL;
	    obj = (melt_ptr_t) ( /*_.OIOPREDEF__V20*/ meltfptr[19]) /*=obj*/ ;
	    melt_object_get_field (slot, obj, 1, "NAMED_NAME");
    /*_.NAMED_NAME__V47*/ meltfptr[37] = slot;
	  };
	  ;

	  {
	    /*^locexp */
	    /*add2sbuf_string */
	      meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
				 melt_string_str ((melt_ptr_t)
						  ( /*_.NAMED_NAME__V47*/
						   meltfptr[37])));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:460:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
				 (") = 1;"));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:461:/ locexp");
	    meltgc_strbuf_add_indent ((melt_ptr_t)
				      ( /*_.SBUF__V3*/ meltfptr[2]), (2), 0);
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:462:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
				 ("fnotice(stderr, \"MELT: predefined %s already defined <%s:%d>\\n\", \""));
	  }
	  ;
	  MELT_LOCATION ("warmelt-outobj.melt:463:/ getslot");
	  {
	    melt_ptr_t slot = NULL, obj = NULL;
	    obj = (melt_ptr_t) ( /*_.OIOPREDEF__V20*/ meltfptr[19]) /*=obj*/ ;
	    melt_object_get_field (slot, obj, 1, "NAMED_NAME");
    /*_.NAMED_NAME__V48*/ meltfptr[47] = slot;
	  };
	  ;

	  {
	    /*^locexp */
	    /*add2sbuf_string */
	      meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
				 melt_string_str ((melt_ptr_t)
						  ( /*_.NAMED_NAME__V48*/
						   meltfptr[47])));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:464:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
				 ("\", __FILE__, __LINE__);"));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:465:/ locexp");
	    meltgc_strbuf_add_indent ((melt_ptr_t)
				      ( /*_.SBUF__V3*/ meltfptr[2]), (2), 0);
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:466:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
				 ("};"));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:467:/ locexp");
	    meltgc_strbuf_add_indent ((melt_ptr_t)
				      ( /*_.SBUF__V3*/ meltfptr[2]), (1), 0);
	  }
	  ;
	  MELT_LOCATION ("warmelt-outobj.melt:445:/ quasiblock");


	  /*epilog */

	  /*^clear */
	     /*clear *//*_.NAMED_NAME__V45*/ meltfptr[35] = 0;
	  /*^clear */
	     /*clear *//*_.NAMED_NAME__V46*/ meltfptr[36] = 0;
	  /*^clear */
	     /*clear *//*_.NAMED_NAME__V47*/ meltfptr[37] = 0;
	  /*^clear */
	     /*clear *//*_.NAMED_NAME__V48*/ meltfptr[47] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-outobj.melt:469:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
   /*_#IS_INTEGERBOX__L22*/ meltfnum[15] =
	    (melt_magic_discr
	     ((melt_ptr_t) ( /*_.OIOPREDEF__V20*/ meltfptr[19])) ==
	     MELTOBMAG_INT);;
	  MELT_LOCATION ("warmelt-outobj.melt:469:/ cond");
	  /*cond */ if ( /*_#IS_INTEGERBOX__L22*/ meltfnum[15])	/*then */
	    {
	      /*^cond.then */
	      /*^block */
	      /*anyblock */
	      {


		{
		  MELT_LOCATION ("warmelt-outobj.melt:470:/ locexp");
		  /*add2sbuf_strconst */
		    meltgc_add_strbuf ((melt_ptr_t)
				       ( /*_.SBUF__V3*/ meltfptr[2]),
				       ("if (!melt_fetch_predefined("));
		}
		;
     /*_#GET_INT__L23*/ meltfnum[19] =
		  (melt_get_int
		   ((melt_ptr_t) ( /*_.OIOPREDEF__V20*/ meltfptr[19])));;

		{
		  MELT_LOCATION ("warmelt-outobj.melt:471:/ locexp");
		  meltgc_add_strbuf_dec ((melt_ptr_t)
					 ( /*_.SBUF__V3*/ meltfptr[2]),
					 ( /*_#GET_INT__L23*/ meltfnum[19]));
		}
		;

		{
		  MELT_LOCATION ("warmelt-outobj.melt:472:/ locexp");
		  /*add2sbuf_strconst */
		    meltgc_add_strbuf ((melt_ptr_t)
				       ( /*_.SBUF__V3*/ meltfptr[2]),
				       (")) melt_store_predefined("));
		}
		;
     /*_#GET_INT__L24*/ meltfnum[23] =
		  (melt_get_int
		   ((melt_ptr_t) ( /*_.OIOPREDEF__V20*/ meltfptr[19])));;

		{
		  MELT_LOCATION ("warmelt-outobj.melt:473:/ locexp");
		  meltgc_add_strbuf_dec ((melt_ptr_t)
					 ( /*_.SBUF__V3*/ meltfptr[2]),
					 ( /*_#GET_INT__L24*/ meltfnum[23]));
		}
		;

		{
		  MELT_LOCATION ("warmelt-outobj.melt:474:/ locexp");
		  /*add2sbuf_strconst */
		    meltgc_add_strbuf ((melt_ptr_t)
				       ( /*_.SBUF__V3*/ meltfptr[2]),
				       (", (melt_ptr_t)&"));
		}
		;

		{
		  MELT_LOCATION ("warmelt-outobj.melt:475:/ locexp");
		  /*add2sbuf_string */
		    meltgc_add_strbuf ((melt_ptr_t)
				       ( /*_.SBUF__V3*/ meltfptr[2]),
				       melt_string_str ((melt_ptr_t)
							( /*_.PTRSTR__V4*/
							 meltfptr[3])));
		}
		;

		{
		  MELT_LOCATION ("warmelt-outobj.melt:476:/ locexp");
		  /*add2sbuf_strconst */
		    meltgc_add_strbuf ((melt_ptr_t)
				       ( /*_.SBUF__V3*/ meltfptr[2]), ("->"));
		}
		;

		{
		  MELT_LOCATION ("warmelt-outobj.melt:477:/ locexp");
		  /*add2sbuf_string */
		    meltgc_add_strbuf ((melt_ptr_t)
				       ( /*_.SBUF__V3*/ meltfptr[2]),
				       melt_string_str ((melt_ptr_t)
							( /*_.ONAME__V18*/
							 meltfptr[17])));
		}
		;

		{
		  MELT_LOCATION ("warmelt-outobj.melt:478:/ locexp");
		  /*add2sbuf_strconst */
		    meltgc_add_strbuf ((melt_ptr_t)
				       ( /*_.SBUF__V3*/ meltfptr[2]), (");"));
		}
		;

		{
		  MELT_LOCATION ("warmelt-outobj.melt:479:/ locexp");
		  meltgc_strbuf_add_indent ((melt_ptr_t)
					    ( /*_.SBUF__V3*/ meltfptr[2]),
					    (1), 0);
		}
		;

		{
		  MELT_LOCATION ("warmelt-outobj.melt:480:/ locexp");
		  /*add2sbuf_strconst */
		    meltgc_add_strbuf ((melt_ptr_t)
				       ( /*_.SBUF__V3*/ meltfptr[2]),
				       ("else {"));
		}
		;

		{
		  MELT_LOCATION ("warmelt-outobj.melt:481:/ locexp");
		  meltgc_strbuf_add_indent ((melt_ptr_t)
					    ( /*_.SBUF__V3*/ meltfptr[2]),
					    (2), 0);
		}
		;

		{
		  MELT_LOCATION ("warmelt-outobj.melt:482:/ locexp");
		  /*add2sbuf_strconst */
		    meltgc_add_strbuf ((melt_ptr_t)
				       ( /*_.SBUF__V3*/ meltfptr[2]),
				       ("meltpredefinited["));
		}
		;
     /*_#GET_INT__L25*/ meltfnum[24] =
		  (melt_get_int
		   ((melt_ptr_t) ( /*_.OIOPREDEF__V20*/ meltfptr[19])));;

		{
		  MELT_LOCATION ("warmelt-outobj.melt:483:/ locexp");
		  meltgc_add_strbuf_dec ((melt_ptr_t)
					 ( /*_.SBUF__V3*/ meltfptr[2]),
					 ( /*_#GET_INT__L25*/ meltfnum[24]));
		}
		;

		{
		  MELT_LOCATION ("warmelt-outobj.melt:484:/ locexp");
		  /*add2sbuf_strconst */
		    meltgc_add_strbuf ((melt_ptr_t)
				       ( /*_.SBUF__V3*/ meltfptr[2]),
				       ("] = 1;"));
		}
		;

		{
		  MELT_LOCATION ("warmelt-outobj.melt:485:/ locexp");
		  meltgc_strbuf_add_indent ((melt_ptr_t)
					    ( /*_.SBUF__V3*/ meltfptr[2]),
					    (2), 0);
		}
		;

		{
		  MELT_LOCATION ("warmelt-outobj.melt:486:/ locexp");
		  /*add2sbuf_strconst */
		    meltgc_add_strbuf ((melt_ptr_t)
				       ( /*_.SBUF__V3*/ meltfptr[2]),
				       ("fnotice(\"MELT: predefined #%d already defined <%s:%d>\\n\", "));
		}
		;
     /*_#GET_INT__L26*/ meltfnum[25] =
		  (melt_get_int
		   ((melt_ptr_t) ( /*_.OIOPREDEF__V20*/ meltfptr[19])));;

		{
		  MELT_LOCATION ("warmelt-outobj.melt:487:/ locexp");
		  meltgc_add_strbuf_dec ((melt_ptr_t)
					 ( /*_.SBUF__V3*/ meltfptr[2]),
					 ( /*_#GET_INT__L26*/ meltfnum[25]));
		}
		;

		{
		  MELT_LOCATION ("warmelt-outobj.melt:488:/ locexp");
		  /*add2sbuf_strconst */
		    meltgc_add_strbuf ((melt_ptr_t)
				       ( /*_.SBUF__V3*/ meltfptr[2]),
				       (", __FILE__, __LINE__);"));
		}
		;

		{
		  MELT_LOCATION ("warmelt-outobj.melt:489:/ locexp");
		  meltgc_strbuf_add_indent ((melt_ptr_t)
					    ( /*_.SBUF__V3*/ meltfptr[2]),
					    (2), 0);
		}
		;

		{
		  MELT_LOCATION ("warmelt-outobj.melt:490:/ locexp");
		  /*add2sbuf_strconst */
		    meltgc_add_strbuf ((melt_ptr_t)
				       ( /*_.SBUF__V3*/ meltfptr[2]), ("};"));
		}
		;

		{
		  MELT_LOCATION ("warmelt-outobj.melt:491:/ locexp");
		  meltgc_strbuf_add_indent ((melt_ptr_t)
					    ( /*_.SBUF__V3*/ meltfptr[2]),
					    (1), 0);
		}
		;
		MELT_LOCATION ("warmelt-outobj.melt:469:/ quasiblock");


		/*epilog */

		/*^clear */
	       /*clear *//*_#GET_INT__L23*/ meltfnum[19] = 0;
		/*^clear */
	       /*clear *//*_#GET_INT__L24*/ meltfnum[23] = 0;
		/*^clear */
	       /*clear *//*_#GET_INT__L25*/ meltfnum[24] = 0;
		/*^clear */
	       /*clear *//*_#GET_INT__L26*/ meltfnum[25] = 0;
	      }
	      ;
	    }
	  else
	    {			/*^cond.else */

	      /*^block */
	      /*anyblock */
	      {

		MELT_LOCATION ("warmelt-outobj.melt:494:/ checksignal");
		MELT_CHECK_SIGNAL ();
		;
     /*_#NULL__L27*/ meltfnum[19] =
		  (( /*_.OIOPREDEF__V20*/ meltfptr[19]) == NULL);;
		MELT_LOCATION ("warmelt-outobj.melt:494:/ cond");
		/*cond */ if ( /*_#NULL__L27*/ meltfnum[19])	/*then */
		  {
		    /*^cond.then */
		    /*^block */
		    /*anyblock */
		    {

		      MELT_LOCATION ("warmelt-outobj.melt:495:/ checksignal");
		      MELT_CHECK_SIGNAL ();
		      ;
		      /*^quasiblock */


		      /*_.RETVAL___V1*/ meltfptr[0] = ( /*nil */ NULL);;

		      {
			MELT_LOCATION ("warmelt-outobj.melt:495:/ locexp");
			/*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
			if (meltxresdescr_ && meltxresdescr_[0]
			    && meltxrestab_)
			  melt_warn_for_no_expected_secondary_results ();
			/* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
			;
		      }
		      ;
		      /*^finalreturn */
		      ;
		      /*finalret */ goto labend_rout;
		      /*_.IFELSE___V50*/ meltfptr[36] =
			/*_.RETURN___V51*/ meltfptr[37];;
		      /*epilog */

		      MELT_LOCATION ("warmelt-outobj.melt:494:/ clear");
		 /*clear *//*_.RETURN___V51*/ meltfptr[37] = 0;
		    }
		    ;
		  }
		else
		  {		/*^cond.else */

		    /*^block */
		    /*anyblock */
		    {


#if MELT_HAVE_DEBUG
		      MELT_LOCATION ("warmelt-outobj.melt:497:/ cppif.then");
		      /*^block */
		      /*anyblock */
		      {


			{
			  /*^locexp */
			  /*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
			  melt_dbgcounter++;
#endif
			  ;
			}
			;
			/*^checksignal */
			MELT_CHECK_SIGNAL ();
			;
	 /*_#MELT_NEED_DBG__L28*/ meltfnum[23] =
			  /*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
			  ( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
			  0	/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
			  ;;
			MELT_LOCATION ("warmelt-outobj.melt:497:/ cond");
			/*cond */ if ( /*_#MELT_NEED_DBG__L28*/ meltfnum[23])	/*then */
			  {
			    /*^cond.then */
			    /*^block */
			    /*anyblock */
			    {

	   /*_#THE_MELTCALLCOUNT__L29*/ meltfnum[24] =
#ifdef meltcallcount
				meltcallcount	/* the_meltcallcount */
#else
				0L
#endif /* meltcallcount the_meltcallcount */
				;;
			      MELT_LOCATION
				("warmelt-outobj.melt:497:/ checksignal");
			      MELT_CHECK_SIGNAL ();
			      ;
			      /*^apply */
			      /*apply */
			      {
				union meltparam_un argtab[5];
				memset (&argtab, 0, sizeof (argtab));
				/*^apply.arg */
				argtab[0].meltbp_long =
				  /*_#THE_MELTCALLCOUNT__L29*/ meltfnum[24];
				/*^apply.arg */
				argtab[1].meltbp_cstring =
				  "warmelt-outobj.melt";
				/*^apply.arg */
				argtab[2].meltbp_long = 497;
				/*^apply.arg */
				argtab[3].meltbp_cstring =
				  "outcinitpredef_objinitobject unexpected oiopredef=";
				/*^apply.arg */
				argtab[4].meltbp_aptr =
				  (melt_ptr_t *) & /*_.OIOPREDEF__V20*/
				  meltfptr[19];
				/*_.MELT_DEBUG_FUN__V54*/ meltfptr[53] =
				  melt_apply ((meltclosure_ptr_t)
					      (( /*!MELT_DEBUG_FUN */
						meltfrout->tabval[1])),
					      (melt_ptr_t) (( /*nil */ NULL)),
					      (MELTBPARSTR_LONG
					       MELTBPARSTR_CSTRING
					       MELTBPARSTR_LONG
					       MELTBPARSTR_CSTRING
					       MELTBPARSTR_PTR ""), argtab,
					      "", (union meltparam_un *) 0);
			      }
			      ;
			      /*_.IF___V53*/ meltfptr[37] =
				/*_.MELT_DEBUG_FUN__V54*/ meltfptr[53];;
			      /*epilog */

			      MELT_LOCATION
				("warmelt-outobj.melt:497:/ clear");
		     /*clear *//*_#THE_MELTCALLCOUNT__L29*/
				meltfnum[24] = 0;
			      /*^clear */
		     /*clear *//*_.MELT_DEBUG_FUN__V54*/ meltfptr[53]
				= 0;
			    }
			    ;
			  }
			else
			  {	/*^cond.else */

	  /*_.IF___V53*/ meltfptr[37] = NULL;;
			  }
			;
			MELT_LOCATION
			  ("warmelt-outobj.melt:497:/ quasiblock");


			/*_.PROGN___V55*/ meltfptr[53] =
			  /*_.IF___V53*/ meltfptr[37];;
			/*^compute */
			/*_.IFCPP___V52*/ meltfptr[47] =
			  /*_.PROGN___V55*/ meltfptr[53];;
			/*epilog */

			MELT_LOCATION ("warmelt-outobj.melt:497:/ clear");
		   /*clear *//*_#MELT_NEED_DBG__L28*/ meltfnum[23] = 0;
			/*^clear */
		   /*clear *//*_.IF___V53*/ meltfptr[37] = 0;
			/*^clear */
		   /*clear *//*_.PROGN___V55*/ meltfptr[53] = 0;
		      }

#else /*MELT_HAVE_DEBUG */
		      /*^cppif.else */
		      /*_.IFCPP___V52*/ meltfptr[47] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
		      ;

#if MELT_HAVE_DEBUG
		      MELT_LOCATION ("warmelt-outobj.melt:498:/ cppif.then");
		      /*^block */
		      /*anyblock */
		      {

			/*^checksignal */
			MELT_CHECK_SIGNAL ();
			;
			/*^cond */
			/*cond */ if (( /*nil */ NULL))	/*then */
			  {
			    /*^cond.then */
			    /*_.IFELSE___V57*/ meltfptr[53] =
			      ( /*nil */ NULL);;
			  }
			else
			  {
			    MELT_LOCATION
			      ("warmelt-outobj.melt:498:/ cond.else");

			    /*^block */
			    /*anyblock */
			    {




			      {
				/*^locexp */
				melt_assert_failed (("outcinitpredef_objinitobject unexpected oiopredef"), ("warmelt-outobj.melt") ? ("warmelt-outobj.melt") : __FILE__, (498) ? (498) : __LINE__, __FUNCTION__);
				;
			      }
			      ;
		     /*clear *//*_.IFELSE___V57*/ meltfptr[53] = 0;
			      /*epilog */
			    }
			    ;
			  }
			;
			/*^compute */
			/*_.IFCPP___V56*/ meltfptr[37] =
			  /*_.IFELSE___V57*/ meltfptr[53];;
			/*epilog */

			MELT_LOCATION ("warmelt-outobj.melt:498:/ clear");
		   /*clear *//*_.IFELSE___V57*/ meltfptr[53] = 0;
		      }

#else /*MELT_HAVE_DEBUG */
		      /*^cppif.else */
		      /*_.IFCPP___V56*/ meltfptr[37] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
		      ;
		      MELT_LOCATION ("warmelt-outobj.melt:496:/ quasiblock");


		      /*_.PROGN___V58*/ meltfptr[53] =
			/*_.IFCPP___V56*/ meltfptr[37];;
		      /*^compute */
		      /*_.IFELSE___V50*/ meltfptr[36] =
			/*_.PROGN___V58*/ meltfptr[53];;
		      /*epilog */

		      MELT_LOCATION ("warmelt-outobj.melt:494:/ clear");
		 /*clear *//*_.IFCPP___V52*/ meltfptr[47] = 0;
		      /*^clear */
		 /*clear *//*_.IFCPP___V56*/ meltfptr[37] = 0;
		      /*^clear */
		 /*clear *//*_.PROGN___V58*/ meltfptr[53] = 0;
		    }
		    ;
		  }
		;
		/*_.IFELSE___V49*/ meltfptr[35] =
		  /*_.IFELSE___V50*/ meltfptr[36];;
		/*epilog */

		MELT_LOCATION ("warmelt-outobj.melt:469:/ clear");
	       /*clear *//*_#NULL__L27*/ meltfnum[19] = 0;
		/*^clear */
	       /*clear *//*_.IFELSE___V50*/ meltfptr[36] = 0;
	      }
	      ;
	    }
	  ;
	  /*_.IFELSE___V44*/ meltfptr[41] = /*_.IFELSE___V49*/ meltfptr[35];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-outobj.melt:445:/ clear");
	     /*clear *//*_#IS_INTEGERBOX__L22*/ meltfnum[15] = 0;
	  /*^clear */
	     /*clear *//*_.IFELSE___V49*/ meltfptr[35] = 0;
	}
	;
      }
    ;
    /*_.LET___V31*/ meltfptr[29] = /*_.IFELSE___V44*/ meltfptr[41];;

    MELT_LOCATION ("warmelt-outobj.melt:431:/ clear");
	   /*clear *//*_.ODLOC__V32*/ meltfptr[31] = 0;
    /*^clear */
	   /*clear *//*_.OUTPUT_LOCATION__V33*/ meltfptr[32] = 0;
    /*^clear */
	   /*clear *//*_#STRBUF_USEDLENGTH__L12*/ meltfnum[11] = 0;
    /*^clear */
	   /*clear *//*_.REFERENCED_VALUE__V34*/ meltfptr[33] = 0;
    /*^clear */
	   /*clear *//*_#GET_INT__L13*/ meltfnum[12] = 0;
    /*^clear */
	   /*clear *//*_#IRAW__L14*/ meltfnum[13] = 0;
    /*^clear */
	   /*clear *//*_#I__L15*/ meltfnum[14] = 0;
    /*^clear */
	   /*clear *//*_.IFELSE___V35*/ meltfptr[34] = 0;
    /*^clear */
	   /*clear *//*_#IS_A__L21*/ meltfnum[16] = 0;
    /*^clear */
	   /*clear *//*_.IFELSE___V44*/ meltfptr[41] = 0;
    /*_.LET___V15*/ meltfptr[13] = /*_.LET___V31*/ meltfptr[29];;

    MELT_LOCATION ("warmelt-outobj.melt:421:/ clear");
	   /*clear *//*_.ODATA__V16*/ meltfptr[15] = 0;
    /*^clear */
	   /*clear *//*_.ODISCR__V17*/ meltfptr[16] = 0;
    /*^clear */
	   /*clear *//*_.ONAME__V18*/ meltfptr[17] = 0;
    /*^clear */
	   /*clear *//*_.OLOCVAR__V19*/ meltfptr[18] = 0;
    /*^clear */
	   /*clear *//*_.OIOPREDEF__V20*/ meltfptr[19] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V21*/ meltfptr[20] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V23*/ meltfptr[21] = 0;
    /*^clear */
	   /*clear *//*_#NULL__L10*/ meltfnum[3] = 0;
    /*^clear */
	   /*clear *//*_.IF___V27*/ meltfptr[23] = 0;
    /*^clear */
	   /*clear *//*_#IS_A__L11*/ meltfnum[1] = 0;
    /*^clear */
	   /*clear *//*_.IF___V29*/ meltfptr[24] = 0;
    /*^clear */
	   /*clear *//*_.LET___V31*/ meltfptr[29] = 0;
    MELT_LOCATION ("warmelt-outobj.melt:416:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.LET___V15*/ meltfptr[13];;

    {
      MELT_LOCATION ("warmelt-outobj.melt:416:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V5*/ meltfptr[4] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V7*/ meltfptr[5] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V11*/ meltfptr[7] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V13*/ meltfptr[8] = 0;
    /*^clear */
	   /*clear *//*_.LET___V15*/ meltfptr[13] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("OUTCINITPREDEF_OBJINITOBJECT", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_14_warmelt_outobj_OUTCINITPREDEF_OBJINITOBJECT_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_14_warmelt_outobj_OUTCINITPREDEF_OBJINITOBJECT */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_15_warmelt_outobj_OUTDECLINIT_OBJINITMULTIPLE (meltclosure_ptr_t
							meltclosp_,
							melt_ptr_t
							meltfirstargp_,
							const
							melt_argdescr_cell_t
							meltxargdescr_[],
							union meltparam_un *
							meltxargtab_,
							const
							melt_argdescr_cell_t
							meltxresdescr_[],
							union meltparam_un *
							meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_15_warmelt_outobj_OUTDECLINIT_OBJINITMULTIPLE_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_15_warmelt_outobj_OUTDECLINIT_OBJINITMULTIPLE_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 4
    melt_ptr_t mcfr_varptr[4];
#define MELTFRAM_NBVARNUM 1
    long mcfr_varnum[1];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_15_warmelt_outobj_OUTDECLINIT_OBJINITMULTIPLE is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct
	 frame_meltrout_15_warmelt_outobj_OUTDECLINIT_OBJINITMULTIPLE_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 4; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_15_warmelt_outobj_OUTDECLINIT_OBJINITMULTIPLE nbval 4*/
  meltfram__.mcfr_nbvar = 4 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("OUTDECLINIT_OBJINITMULTIPLE", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-outobj.melt:507:/ getarg");
 /*_.RECV__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.SBUF__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2])) !=
	      NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


    {
      MELT_LOCATION ("warmelt-outobj.melt:508:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
			   (" struct MELT_MULTIPLE_STRUCT("));
    }
    ;
 /*_#GET_INT__L1*/ meltfnum[0] =
      (melt_get_int ((melt_ptr_t) ( /*_.RECV__V2*/ meltfptr[1])));;

    {
      MELT_LOCATION ("warmelt-outobj.melt:509:/ locexp");
      meltgc_add_strbuf_dec ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
			     ( /*_#GET_INT__L1*/ meltfnum[0]));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:510:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
			   (") "));
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:511:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.RECV__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 1, "NAMED_NAME");
  /*_.NAMED_NAME__V4*/ meltfptr[3] = slot;
    };
    ;

    {
      /*^locexp */
      /*add2sbuf_string */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
			   melt_string_str ((melt_ptr_t)
					    ( /*_.NAMED_NAME__V4*/
					     meltfptr[3])));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:512:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]), (";"));
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:507:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*epilog */

    /*^clear */
	   /*clear *//*_#GET_INT__L1*/ meltfnum[0] = 0;
    /*^clear */
	   /*clear *//*_.NAMED_NAME__V4*/ meltfptr[3] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("OUTDECLINIT_OBJINITMULTIPLE", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_15_warmelt_outobj_OUTDECLINIT_OBJINITMULTIPLE_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*noretval */ NULL);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_15_warmelt_outobj_OUTDECLINIT_OBJINITMULTIPLE */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_16_warmelt_outobj_OUTCINITFILL_OBJINITMULTIPLE (meltclosure_ptr_t
							 meltclosp_,
							 melt_ptr_t
							 meltfirstargp_,
							 const
							 melt_argdescr_cell_t
							 meltxargdescr_[],
							 union meltparam_un *
							 meltxargtab_,
							 const
							 melt_argdescr_cell_t
							 meltxresdescr_[],
							 union meltparam_un *
							 meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_16_warmelt_outobj_OUTCINITFILL_OBJINITMULTIPLE_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_16_warmelt_outobj_OUTCINITFILL_OBJINITMULTIPLE_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 28
    melt_ptr_t mcfr_varptr[28];
#define MELTFRAM_NBVARNUM 15
    long mcfr_varnum[15];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_16_warmelt_outobj_OUTCINITFILL_OBJINITMULTIPLE is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct
	 frame_meltrout_16_warmelt_outobj_OUTCINITFILL_OBJINITMULTIPLE_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 28; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_16_warmelt_outobj_OUTCINITFILL_OBJINITMULTIPLE nbval 28*/
  meltfram__.mcfr_nbvar = 28 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("OUTCINITFILL_OBJINITMULTIPLE", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-outobj.melt:517:/ getarg");
 /*_.RECV__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.SBUF__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2])) !=
	      NULL);


  /*getarg#2 */
  /*^getarg */
  if (meltxargdescr_[1] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.PTRSTR__V4*/ meltfptr[3] =
    (meltxargtab_[1].meltbp_aptr) ? (*(meltxargtab_[1].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.PTRSTR__V4*/ meltfptr[3])) !=
	      NULL);


  /*getarg#3 */
  /*^getarg */
  if (meltxargdescr_[2] != MELTBPAR_LONG)
    goto lab_endgetargs;
 /*_#DEPTH__L1*/ meltfnum[0] = meltxargtab_[2].meltbp_long;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:518:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L2*/ meltfnum[1] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.RECV__V2*/ meltfptr[1]),
			     (melt_ptr_t) (( /*!CLASS_OBJINITMULTIPLE */
					    meltfrout->tabval[0])));;
      MELT_LOCATION ("warmelt-outobj.melt:518:/ cond");
      /*cond */ if ( /*_#IS_A__L2*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V6*/ meltfptr[5] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-outobj.melt:518:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("outcinitfill_objinitmultiple check recv"),
				  ("warmelt-outobj.melt")
				  ? ("warmelt-outobj.melt") : __FILE__,
				  (518) ? (518) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V6*/ meltfptr[5] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V5*/ meltfptr[4] = /*_.IFELSE___V6*/ meltfptr[5];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:518:/ clear");
	     /*clear *//*_#IS_A__L2*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V6*/ meltfptr[5] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V5*/ meltfptr[4] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:519:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L3*/ meltfnum[1] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-outobj.melt:519:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L3*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L4*/ meltfnum[3] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-outobj.melt:519:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[7];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L4*/ meltfnum[3];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-outobj.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 519;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "outcinitfill_objinitmultiple recv=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.RECV__V2*/ meltfptr[1];
	      /*^apply.arg */
	      argtab[5].meltbp_cstring = " ptrstr=";
	      /*^apply.arg */
	      argtab[6].meltbp_aptr =
		(melt_ptr_t *) & /*_.PTRSTR__V4*/ meltfptr[3];
	      /*_.MELT_DEBUG_FUN__V9*/ meltfptr[8] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[1])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V8*/ meltfptr[7] = /*_.MELT_DEBUG_FUN__V9*/ meltfptr[8];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-outobj.melt:519:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L4*/ meltfnum[3] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V9*/ meltfptr[8] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V8*/ meltfptr[7] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-outobj.melt:519:/ quasiblock");


      /*_.PROGN___V10*/ meltfptr[8] = /*_.IF___V8*/ meltfptr[7];;
      /*^compute */
      /*_.IFCPP___V7*/ meltfptr[5] = /*_.PROGN___V10*/ meltfptr[8];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:519:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L3*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IF___V8*/ meltfptr[7] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V10*/ meltfptr[8] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V7*/ meltfptr[5] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:520:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_STRING__L5*/ meltfnum[3] =
	(melt_magic_discr ((melt_ptr_t) ( /*_.PTRSTR__V4*/ meltfptr[3])) ==
	 MELTOBMAG_STRING);;
      MELT_LOCATION ("warmelt-outobj.melt:520:/ cond");
      /*cond */ if ( /*_#IS_STRING__L5*/ meltfnum[3])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V12*/ meltfptr[8] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-outobj.melt:520:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("outcinitfill_objinitmultiple check ptrstr"), ("warmelt-outobj.melt") ? ("warmelt-outobj.melt") : __FILE__, (520) ? (520) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V12*/ meltfptr[8] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V11*/ meltfptr[7] = /*_.IFELSE___V12*/ meltfptr[8];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:520:/ clear");
	     /*clear *//*_#IS_STRING__L5*/ meltfnum[3] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V12*/ meltfptr[8] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V11*/ meltfptr[7] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-outobj.melt:521:/ quasiblock");


    /*^getslot */
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.RECV__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 1, "OIE_CNAME");
  /*_.CNAM__V14*/ meltfptr[13] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-outobj.melt:522:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.RECV__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 4, "OIE_LOCVAR");
  /*_.OLOCVAR__V15*/ meltfptr[14] = slot;
    };
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:524:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
			   ("/*inimult "));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:525:/ locexp");
      /*add2sbuf_string */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
			   melt_string_str ((melt_ptr_t)
					    ( /*_.CNAM__V14*/ meltfptr[13])));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:526:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
			   ("*/"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:527:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
				(1), 0);
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:528:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^cond */
    /*cond */ if ( /*_.OLOCVAR__V15*/ meltfptr[14])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-outobj.melt:530:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^msend */
	  /*msend */
	  {
	    union meltparam_un argtab[3];
	    memset (&argtab, 0, sizeof (argtab));
	    /*^ojbmsend.arg */
	    argtab[0].meltbp_aptr = /*nil */ (melt_ptr_t *) NULL;
	    /*^ojbmsend.arg */
	    argtab[1].meltbp_aptr =
	      (melt_ptr_t *) & /*_.SBUF__V3*/ meltfptr[2];
	    /*^ojbmsend.arg */
	    argtab[2].meltbp_long = 1;
	    /*_.OUTPUT_C_CODE__V16*/ meltfptr[15] =
	      meltgc_send ((melt_ptr_t) ( /*_.OLOCVAR__V15*/ meltfptr[14]),
			   (melt_ptr_t) (( /*!OUTPUT_C_CODE */ meltfrout->
					  tabval[2])),
			   (MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_LONG
			    ""), argtab, "", (union meltparam_un *) 0);
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:531:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
				 (" = (melt_ptr_t) &"));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:532:/ locexp");
	    /*add2sbuf_string */
	      meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
				 melt_string_str ((melt_ptr_t)
						  ( /*_.PTRSTR__V4*/
						   meltfptr[3])));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:533:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
				 ("->"));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:534:/ locexp");
	    /*add2sbuf_string */
	      meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
				 melt_string_str ((melt_ptr_t)
						  ( /*_.CNAM__V14*/
						   meltfptr[13])));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:535:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
				 (";"));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:536:/ locexp");
	    meltgc_strbuf_add_indent ((melt_ptr_t)
				      ( /*_.SBUF__V3*/ meltfptr[2]), (1), 0);
	  }
	  ;
	  MELT_LOCATION ("warmelt-outobj.melt:529:/ quasiblock");


	  /*epilog */

	  MELT_LOCATION ("warmelt-outobj.melt:528:/ clear");
	     /*clear *//*_.OUTPUT_C_CODE__V16*/ meltfptr[15] = 0;
	}
	;
      }				/*noelse */
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:538:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]), (" "));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:539:/ locexp");
      /*add2sbuf_string */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
			   melt_string_str ((melt_ptr_t)
					    ( /*_.PTRSTR__V4*/ meltfptr[3])));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:540:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
			   ("->"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:541:/ locexp");
      /*add2sbuf_string */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
			   melt_string_str ((melt_ptr_t)
					    ( /*_.CNAM__V14*/ meltfptr[13])));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:542:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
			   (".discr = (meltobject_ptr_t)("));
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:543:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.RECV__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 3, "OIE_DISCR");
  /*_.OIE_DISCR__V17*/ meltfptr[15] = slot;
    };
    ;
    /*^checksignal */
    MELT_CHECK_SIGNAL ();
    ;
    /*^msend */
    /*msend */
    {
      union meltparam_un argtab[3];
      memset (&argtab, 0, sizeof (argtab));
      /*^ojbmsend.arg */
      argtab[0].meltbp_aptr = /*nil */ (melt_ptr_t *) NULL;
      /*^ojbmsend.arg */
      argtab[1].meltbp_aptr = (melt_ptr_t *) & /*_.SBUF__V3*/ meltfptr[2];
      /*^ojbmsend.arg */
      argtab[2].meltbp_long = 1;
      /*_.OUTPUT_C_CODE__V18*/ meltfptr[17] =
	meltgc_send ((melt_ptr_t) ( /*_.OIE_DISCR__V17*/ meltfptr[15]),
		     (melt_ptr_t) (( /*!OUTPUT_C_CODE */ meltfrout->
				    tabval[2])),
		     (MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_LONG ""),
		     argtab, "", (union meltparam_un *) 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:544:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
			   (");"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:545:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
				(1), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:546:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]), (" "));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:547:/ locexp");
      /*add2sbuf_string */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
			   melt_string_str ((melt_ptr_t)
					    ( /*_.PTRSTR__V4*/ meltfptr[3])));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:548:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
			   ("->"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:549:/ locexp");
      /*add2sbuf_string */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
			   melt_string_str ((melt_ptr_t)
					    ( /*_.CNAM__V14*/ meltfptr[13])));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:550:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
			   (".nbval = "));
    }
    ;
 /*_#GET_INT__L6*/ meltfnum[1] =
      (melt_get_int ((melt_ptr_t) ( /*_.RECV__V2*/ meltfptr[1])));;

    {
      MELT_LOCATION ("warmelt-outobj.melt:551:/ locexp");
      meltgc_add_strbuf_dec ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
			     ( /*_#GET_INT__L6*/ meltfnum[1]));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:552:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]), (";"));
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:554:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#STRBUF_USEDLENGTH__L7*/ meltfnum[3] =
      melt_strbuf_usedlength ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]));;
    MELT_LOCATION ("warmelt-outobj.melt:554:/ cond");
    /*cond */ if (
		   /*ifisa */
		   melt_is_instance_of ((melt_ptr_t)
					(( /*!BUFFER_LIMIT_CONT */ meltfrout->
					  tabval[3])),
					(melt_ptr_t) (( /*!CLASS_REFERENCE */
						       meltfrout->tabval[4])))
      )				/*then */
      {
	/*^cond.then */
	/*^getslot */
	{
	  melt_ptr_t slot = NULL, obj = NULL;
	  obj =
	    (melt_ptr_t) (( /*!BUFFER_LIMIT_CONT */ meltfrout->
			   tabval[3])) /*=obj*/ ;
	  melt_object_get_field (slot, obj, 0, "REFERENCED_VALUE");
   /*_.REFERENCED_VALUE__V19*/ meltfptr[18] = slot;
	};
	;
      }
    else
      {				/*^cond.else */

  /*_.REFERENCED_VALUE__V19*/ meltfptr[18] = NULL;;
      }
    ;
    /*^compute */
 /*_#GET_INT__L8*/ meltfnum[7] =
      (melt_get_int
       ((melt_ptr_t) ( /*_.REFERENCED_VALUE__V19*/ meltfptr[18])));;
    /*^compute */
 /*_#IRAW__L9*/ meltfnum[8] =
      (( /*_#GET_INT__L8*/ meltfnum[7]) / (2));;
    /*^compute */
 /*_#I__L10*/ meltfnum[9] =
      (( /*_#STRBUF_USEDLENGTH__L7*/ meltfnum[3]) >
       ( /*_#IRAW__L9*/ meltfnum[8]));;
    MELT_LOCATION ("warmelt-outobj.melt:554:/ cond");
    /*cond */ if ( /*_#I__L10*/ meltfnum[9])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{


	  {
	    MELT_LOCATION ("warmelt-outobj.melt:555:/ locexp");

#if MELT_HAVE_DEBUG
	    if (melt_need_debug (0))
	      melt_dbgshortbacktrace (("outcinitfill_objinitmultiple huge sbuf"), (15));
#endif
	    ;
	  }
	  ;

#if MELT_HAVE_DEBUG
	  MELT_LOCATION ("warmelt-outobj.melt:556:/ cppif.then");
	  /*^block */
	  /*anyblock */
	  {


	    {
	      /*^locexp */
	      /*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	      melt_dbgcounter++;
#endif
	      ;
	    }
	    ;
	    /*^checksignal */
	    MELT_CHECK_SIGNAL ();
	    ;
     /*_#MELT_NEED_DBG__L11*/ meltfnum[10] =
	      /*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	      ( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	      0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	      ;;
	    MELT_LOCATION ("warmelt-outobj.melt:556:/ cond");
	    /*cond */ if ( /*_#MELT_NEED_DBG__L11*/ meltfnum[10])	/*then */
	      {
		/*^cond.then */
		/*^block */
		/*anyblock */
		{

       /*_#THE_MELTCALLCOUNT__L12*/ meltfnum[11] =
#ifdef meltcallcount
		    meltcallcount	/* the_meltcallcount */
#else
		    0L
#endif /* meltcallcount the_meltcallcount */
		    ;;
		  MELT_LOCATION ("warmelt-outobj.melt:556:/ checksignal");
		  MELT_CHECK_SIGNAL ();
		  ;
		  /*^apply */
		  /*apply */
		  {
		    union meltparam_un argtab[5];
		    memset (&argtab, 0, sizeof (argtab));
		    /*^apply.arg */
		    argtab[0].meltbp_long =
		      /*_#THE_MELTCALLCOUNT__L12*/ meltfnum[11];
		    /*^apply.arg */
		    argtab[1].meltbp_cstring = "warmelt-outobj.melt";
		    /*^apply.arg */
		    argtab[2].meltbp_long = 556;
		    /*^apply.arg */
		    argtab[3].meltbp_cstring =
		      "outcinitfill_objinitmultiple huge sbuf=";
		    /*^apply.arg */
		    argtab[4].meltbp_aptr =
		      (melt_ptr_t *) & /*_.SBUF__V3*/ meltfptr[2];
		    /*_.MELT_DEBUG_FUN__V23*/ meltfptr[22] =
		      melt_apply ((meltclosure_ptr_t)
				  (( /*!MELT_DEBUG_FUN */ meltfrout->
				    tabval[1])),
				  (melt_ptr_t) (( /*nil */ NULL)),
				  (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_PTR ""), argtab, "",
				  (union meltparam_un *) 0);
		  }
		  ;
		  /*_.IF___V22*/ meltfptr[21] =
		    /*_.MELT_DEBUG_FUN__V23*/ meltfptr[22];;
		  /*epilog */

		  MELT_LOCATION ("warmelt-outobj.melt:556:/ clear");
		 /*clear *//*_#THE_MELTCALLCOUNT__L12*/ meltfnum[11] = 0;
		  /*^clear */
		 /*clear *//*_.MELT_DEBUG_FUN__V23*/ meltfptr[22] = 0;
		}
		;
	      }
	    else
	      {			/*^cond.else */

      /*_.IF___V22*/ meltfptr[21] = NULL;;
	      }
	    ;
	    MELT_LOCATION ("warmelt-outobj.melt:556:/ quasiblock");


	    /*_.PROGN___V24*/ meltfptr[22] = /*_.IF___V22*/ meltfptr[21];;
	    /*^compute */
	    /*_.IFCPP___V21*/ meltfptr[20] = /*_.PROGN___V24*/ meltfptr[22];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-outobj.melt:556:/ clear");
	       /*clear *//*_#MELT_NEED_DBG__L11*/ meltfnum[10] = 0;
	    /*^clear */
	       /*clear *//*_.IF___V22*/ meltfptr[21] = 0;
	    /*^clear */
	       /*clear *//*_.PROGN___V24*/ meltfptr[22] = 0;
	  }

#else /*MELT_HAVE_DEBUG */
	  /*^cppif.else */
	  /*_.IFCPP___V21*/ meltfptr[20] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
	  ;

#if MELT_HAVE_DEBUG
	  MELT_LOCATION ("warmelt-outobj.melt:557:/ cppif.then");
	  /*^block */
	  /*anyblock */
	  {

	    /*^checksignal */
	    MELT_CHECK_SIGNAL ();
	    ;
     /*_#STRBUF_USEDLENGTH__L13*/ meltfnum[11] =
	      melt_strbuf_usedlength ((melt_ptr_t)
				      ( /*_.SBUF__V3*/ meltfptr[2]));;
	    MELT_LOCATION ("warmelt-outobj.melt:558:/ cond");
	    /*cond */ if (
			   /*ifisa */
			   melt_is_instance_of ((melt_ptr_t)
						(( /*!BUFFER_LIMIT_CONT */
						  meltfrout->tabval[3])),
						(melt_ptr_t) (( /*!CLASS_REFERENCE */ meltfrout->tabval[4])))
	      )			/*then */
	      {
		/*^cond.then */
		/*^getslot */
		{
		  melt_ptr_t slot = NULL, obj = NULL;
		  obj =
		    (melt_ptr_t) (( /*!BUFFER_LIMIT_CONT */ meltfrout->
				   tabval[3])) /*=obj*/ ;
		  melt_object_get_field (slot, obj, 0, "REFERENCED_VALUE");
       /*_.REFERENCED_VALUE__V26*/ meltfptr[22] = slot;
		};
		;
	      }
	    else
	      {			/*^cond.else */

      /*_.REFERENCED_VALUE__V26*/ meltfptr[22] = NULL;;
	      }
	    ;
	    /*^compute */
     /*_#GET_INT__L14*/ meltfnum[10] =
	      (melt_get_int
	       ((melt_ptr_t) ( /*_.REFERENCED_VALUE__V26*/ meltfptr[22])));;
	    /*^compute */
     /*_#I__L15*/ meltfnum[14] =
	      (( /*_#STRBUF_USEDLENGTH__L13*/ meltfnum[11]) <
	       ( /*_#GET_INT__L14*/ meltfnum[10]));;
	    MELT_LOCATION ("warmelt-outobj.melt:557:/ cond");
	    /*cond */ if ( /*_#I__L15*/ meltfnum[14])	/*then */
	      {
		/*^cond.then */
		/*_.IFELSE___V27*/ meltfptr[26] = ( /*nil */ NULL);;
	      }
	    else
	      {
		MELT_LOCATION ("warmelt-outobj.melt:557:/ cond.else");

		/*^block */
		/*anyblock */
		{




		  {
		    /*^locexp */
		    melt_assert_failed (("check limited sbuf"),
					("warmelt-outobj.melt")
					? ("warmelt-outobj.melt") : __FILE__,
					(557) ? (557) : __LINE__,
					__FUNCTION__);
		    ;
		  }
		  ;
		 /*clear *//*_.IFELSE___V27*/ meltfptr[26] = 0;
		  /*epilog */
		}
		;
	      }
	    ;
	    /*^compute */
	    /*_.IFCPP___V25*/ meltfptr[21] = /*_.IFELSE___V27*/ meltfptr[26];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-outobj.melt:557:/ clear");
	       /*clear *//*_#STRBUF_USEDLENGTH__L13*/ meltfnum[11] = 0;
	    /*^clear */
	       /*clear *//*_.REFERENCED_VALUE__V26*/ meltfptr[22] = 0;
	    /*^clear */
	       /*clear *//*_#GET_INT__L14*/ meltfnum[10] = 0;
	    /*^clear */
	       /*clear *//*_#I__L15*/ meltfnum[14] = 0;
	    /*^clear */
	       /*clear *//*_.IFELSE___V27*/ meltfptr[26] = 0;
	  }

#else /*MELT_HAVE_DEBUG */
	  /*^cppif.else */
	  /*_.IFCPP___V25*/ meltfptr[21] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
	  ;
	  MELT_LOCATION ("warmelt-outobj.melt:554:/ quasiblock");


	  /*_.PROGN___V28*/ meltfptr[22] = /*_.IFCPP___V25*/ meltfptr[21];;
	  /*^compute */
	  /*_.IFELSE___V20*/ meltfptr[19] = /*_.PROGN___V28*/ meltfptr[22];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-outobj.melt:554:/ clear");
	     /*clear *//*_.IFCPP___V21*/ meltfptr[20] = 0;
	  /*^clear */
	     /*clear *//*_.IFCPP___V25*/ meltfptr[21] = 0;
	  /*^clear */
	     /*clear *//*_.PROGN___V28*/ meltfptr[22] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

  /*_.IFELSE___V20*/ meltfptr[19] = NULL;;
      }
    ;
    /*^compute */
    /*_.LET___V13*/ meltfptr[8] = /*_.IFELSE___V20*/ meltfptr[19];;

    MELT_LOCATION ("warmelt-outobj.melt:521:/ clear");
	   /*clear *//*_.CNAM__V14*/ meltfptr[13] = 0;
    /*^clear */
	   /*clear *//*_.OLOCVAR__V15*/ meltfptr[14] = 0;
    /*^clear */
	   /*clear *//*_.OIE_DISCR__V17*/ meltfptr[15] = 0;
    /*^clear */
	   /*clear *//*_.OUTPUT_C_CODE__V18*/ meltfptr[17] = 0;
    /*^clear */
	   /*clear *//*_#GET_INT__L6*/ meltfnum[1] = 0;
    /*^clear */
	   /*clear *//*_#STRBUF_USEDLENGTH__L7*/ meltfnum[3] = 0;
    /*^clear */
	   /*clear *//*_.REFERENCED_VALUE__V19*/ meltfptr[18] = 0;
    /*^clear */
	   /*clear *//*_#GET_INT__L8*/ meltfnum[7] = 0;
    /*^clear */
	   /*clear *//*_#IRAW__L9*/ meltfnum[8] = 0;
    /*^clear */
	   /*clear *//*_#I__L10*/ meltfnum[9] = 0;
    /*^clear */
	   /*clear *//*_.IFELSE___V20*/ meltfptr[19] = 0;
    MELT_LOCATION ("warmelt-outobj.melt:517:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.LET___V13*/ meltfptr[8];;

    {
      MELT_LOCATION ("warmelt-outobj.melt:517:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V5*/ meltfptr[4] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V7*/ meltfptr[5] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V11*/ meltfptr[7] = 0;
    /*^clear */
	   /*clear *//*_.LET___V13*/ meltfptr[8] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("OUTCINITFILL_OBJINITMULTIPLE", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_16_warmelt_outobj_OUTCINITFILL_OBJINITMULTIPLE_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_16_warmelt_outobj_OUTCINITFILL_OBJINITMULTIPLE */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_17_warmelt_outobj_OUTDECLINIT_OBJINITCLOSURE (meltclosure_ptr_t
						       meltclosp_,
						       melt_ptr_t
						       meltfirstargp_,
						       const
						       melt_argdescr_cell_t
						       meltxargdescr_[],
						       union meltparam_un *
						       meltxargtab_,
						       const
						       melt_argdescr_cell_t
						       meltxresdescr_[],
						       union meltparam_un *
						       meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_17_warmelt_outobj_OUTDECLINIT_OBJINITCLOSURE_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_17_warmelt_outobj_OUTDECLINIT_OBJINITCLOSURE_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 4
    melt_ptr_t mcfr_varptr[4];
#define MELTFRAM_NBVARNUM 1
    long mcfr_varnum[1];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_17_warmelt_outobj_OUTDECLINIT_OBJINITCLOSURE is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_17_warmelt_outobj_OUTDECLINIT_OBJINITCLOSURE_st
	 *) meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 4; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_17_warmelt_outobj_OUTDECLINIT_OBJINITCLOSURE nbval 4*/
  meltfram__.mcfr_nbvar = 4 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("OUTDECLINIT_OBJINITCLOSURE", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-outobj.melt:562:/ getarg");
 /*_.RECV__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.SBUF__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2])) !=
	      NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


    {
      MELT_LOCATION ("warmelt-outobj.melt:563:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
			   (" struct MELT_CLOSURE_STRUCT("));
    }
    ;
 /*_#GET_INT__L1*/ meltfnum[0] =
      (melt_get_int ((melt_ptr_t) ( /*_.RECV__V2*/ meltfptr[1])));;

    {
      MELT_LOCATION ("warmelt-outobj.melt:564:/ locexp");
      meltgc_add_strbuf_dec ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
			     ( /*_#GET_INT__L1*/ meltfnum[0]));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:565:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
			   (") "));
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:566:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.RECV__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 1, "OIE_CNAME");
  /*_.OIE_CNAME__V4*/ meltfptr[3] = slot;
    };
    ;

    {
      /*^locexp */
      /*add2sbuf_string */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
			   melt_string_str ((melt_ptr_t)
					    ( /*_.OIE_CNAME__V4*/
					     meltfptr[3])));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:567:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]), (";"));
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:562:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*epilog */

    /*^clear */
	   /*clear *//*_#GET_INT__L1*/ meltfnum[0] = 0;
    /*^clear */
	   /*clear *//*_.OIE_CNAME__V4*/ meltfptr[3] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("OUTDECLINIT_OBJINITCLOSURE", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_17_warmelt_outobj_OUTDECLINIT_OBJINITCLOSURE_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*noretval */ NULL);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_17_warmelt_outobj_OUTDECLINIT_OBJINITCLOSURE */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_18_warmelt_outobj_OUTCINITFILL_OBJINITCLOSURE (meltclosure_ptr_t
							meltclosp_,
							melt_ptr_t
							meltfirstargp_,
							const
							melt_argdescr_cell_t
							meltxargdescr_[],
							union meltparam_un *
							meltxargtab_,
							const
							melt_argdescr_cell_t
							meltxresdescr_[],
							union meltparam_un *
							meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_18_warmelt_outobj_OUTCINITFILL_OBJINITCLOSURE_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_18_warmelt_outobj_OUTCINITFILL_OBJINITCLOSURE_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 30
    melt_ptr_t mcfr_varptr[30];
#define MELTFRAM_NBVARNUM 16
    long mcfr_varnum[16];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_18_warmelt_outobj_OUTCINITFILL_OBJINITCLOSURE is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct
	 frame_meltrout_18_warmelt_outobj_OUTCINITFILL_OBJINITCLOSURE_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 30; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_18_warmelt_outobj_OUTCINITFILL_OBJINITCLOSURE nbval 30*/
  meltfram__.mcfr_nbvar = 30 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("OUTCINITFILL_OBJINITCLOSURE", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-outobj.melt:571:/ getarg");
 /*_.RECV__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.SBUF__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2])) !=
	      NULL);


  /*getarg#2 */
  /*^getarg */
  if (meltxargdescr_[1] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.PTRSTR__V4*/ meltfptr[3] =
    (meltxargtab_[1].meltbp_aptr) ? (*(meltxargtab_[1].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.PTRSTR__V4*/ meltfptr[3])) !=
	      NULL);


  /*getarg#3 */
  /*^getarg */
  if (meltxargdescr_[2] != MELTBPAR_LONG)
    goto lab_endgetargs;
 /*_#DEPTH__L1*/ meltfnum[0] = meltxargtab_[2].meltbp_long;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:572:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L2*/ meltfnum[1] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.RECV__V2*/ meltfptr[1]),
			     (melt_ptr_t) (( /*!CLASS_OBJINITCLOSURE */
					    meltfrout->tabval[0])));;
      MELT_LOCATION ("warmelt-outobj.melt:572:/ cond");
      /*cond */ if ( /*_#IS_A__L2*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V6*/ meltfptr[5] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-outobj.melt:572:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("outcinitfill_objinitclosure check recv"),
				  ("warmelt-outobj.melt")
				  ? ("warmelt-outobj.melt") : __FILE__,
				  (572) ? (572) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V6*/ meltfptr[5] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V5*/ meltfptr[4] = /*_.IFELSE___V6*/ meltfptr[5];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:572:/ clear");
	     /*clear *//*_#IS_A__L2*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V6*/ meltfptr[5] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V5*/ meltfptr[4] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:573:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L3*/ meltfnum[1] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-outobj.melt:573:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L3*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L4*/ meltfnum[3] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-outobj.melt:573:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[7];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L4*/ meltfnum[3];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-outobj.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 573;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "outcinitfill_objinitclosure recv=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.RECV__V2*/ meltfptr[1];
	      /*^apply.arg */
	      argtab[5].meltbp_cstring = " ptrstr=";
	      /*^apply.arg */
	      argtab[6].meltbp_aptr =
		(melt_ptr_t *) & /*_.PTRSTR__V4*/ meltfptr[3];
	      /*_.MELT_DEBUG_FUN__V9*/ meltfptr[8] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[1])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V8*/ meltfptr[7] = /*_.MELT_DEBUG_FUN__V9*/ meltfptr[8];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-outobj.melt:573:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L4*/ meltfnum[3] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V9*/ meltfptr[8] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V8*/ meltfptr[7] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-outobj.melt:573:/ quasiblock");


      /*_.PROGN___V10*/ meltfptr[8] = /*_.IF___V8*/ meltfptr[7];;
      /*^compute */
      /*_.IFCPP___V7*/ meltfptr[5] = /*_.PROGN___V10*/ meltfptr[8];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:573:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L3*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IF___V8*/ meltfptr[7] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V10*/ meltfptr[8] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V7*/ meltfptr[5] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:574:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_STRING__L5*/ meltfnum[3] =
	(melt_magic_discr ((melt_ptr_t) ( /*_.PTRSTR__V4*/ meltfptr[3])) ==
	 MELTOBMAG_STRING);;
      MELT_LOCATION ("warmelt-outobj.melt:574:/ cond");
      /*cond */ if ( /*_#IS_STRING__L5*/ meltfnum[3])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V12*/ meltfptr[8] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-outobj.melt:574:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("outcinitfill_objinitclosure check ptrstr"), ("warmelt-outobj.melt") ? ("warmelt-outobj.melt") : __FILE__, (574) ? (574) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V12*/ meltfptr[8] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V11*/ meltfptr[7] = /*_.IFELSE___V12*/ meltfptr[8];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:574:/ clear");
	     /*clear *//*_#IS_STRING__L5*/ meltfnum[3] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V12*/ meltfptr[8] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V11*/ meltfptr[7] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-outobj.melt:575:/ quasiblock");


    /*^getslot */
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.RECV__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 1, "OIE_CNAME");
  /*_.CNAM__V14*/ meltfptr[13] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-outobj.melt:576:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.RECV__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 4, "OIE_LOCVAR");
  /*_.OLOCVAR__V15*/ meltfptr[14] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-outobj.melt:577:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.RECV__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 5, "OICLO_ROUT");
  /*_.OROUT__V16*/ meltfptr[15] = slot;
    };
    ;
 /*_#DEPTHP1__L6*/ meltfnum[1] =
      ((1) + ( /*_#DEPTH__L1*/ meltfnum[0]));;

    {
      MELT_LOCATION ("warmelt-outobj.melt:580:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
			   ("/*iniclos "));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:581:/ locexp");
      /*add2sbuf_string */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
			   melt_string_str ((melt_ptr_t)
					    ( /*_.CNAM__V14*/ meltfptr[13])));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:582:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
			   ("*/"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:583:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
				( /*_#DEPTHP1__L6*/ meltfnum[1]), 0);
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:584:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^cond */
    /*cond */ if ( /*_.OLOCVAR__V15*/ meltfptr[14])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-outobj.melt:587:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^msend */
	  /*msend */
	  {
	    union meltparam_un argtab[3];
	    memset (&argtab, 0, sizeof (argtab));
	    /*^ojbmsend.arg */
	    argtab[0].meltbp_aptr = /*nil */ (melt_ptr_t *) NULL;
	    /*^ojbmsend.arg */
	    argtab[1].meltbp_aptr =
	      (melt_ptr_t *) & /*_.SBUF__V3*/ meltfptr[2];
	    /*^ojbmsend.arg */
	    argtab[2].meltbp_long = /*_#DEPTHP1__L6*/ meltfnum[1];
	    /*_.OUTPUT_C_CODE__V17*/ meltfptr[16] =
	      meltgc_send ((melt_ptr_t) ( /*_.OLOCVAR__V15*/ meltfptr[14]),
			   (melt_ptr_t) (( /*!OUTPUT_C_CODE */ meltfrout->
					  tabval[2])),
			   (MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_LONG
			    ""), argtab, "", (union meltparam_un *) 0);
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:588:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
				 (" = (melt_ptr_t) &"));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:589:/ locexp");
	    /*add2sbuf_string */
	      meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
				 melt_string_str ((melt_ptr_t)
						  ( /*_.PTRSTR__V4*/
						   meltfptr[3])));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:590:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
				 ("->"));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:591:/ locexp");
	    /*add2sbuf_string */
	      meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
				 melt_string_str ((melt_ptr_t)
						  ( /*_.CNAM__V14*/
						   meltfptr[13])));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:592:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
				 (";"));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:593:/ locexp");
	    meltgc_strbuf_add_indent ((melt_ptr_t)
				      ( /*_.SBUF__V3*/ meltfptr[2]),
				      ( /*_#DEPTHP1__L6*/ meltfnum[1]), 0);
	  }
	  ;
	  MELT_LOCATION ("warmelt-outobj.melt:585:/ quasiblock");


	  /*epilog */

	  MELT_LOCATION ("warmelt-outobj.melt:584:/ clear");
	     /*clear *//*_.OUTPUT_C_CODE__V17*/ meltfptr[16] = 0;
	}
	;
      }				/*noelse */
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:595:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]), (" "));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:596:/ locexp");
      /*add2sbuf_string */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
			   melt_string_str ((melt_ptr_t)
					    ( /*_.PTRSTR__V4*/ meltfptr[3])));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:597:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
			   ("->"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:598:/ locexp");
      /*add2sbuf_string */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
			   melt_string_str ((melt_ptr_t)
					    ( /*_.CNAM__V14*/ meltfptr[13])));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:599:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
			   (".discr = (meltobject_ptr_t)("));
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:600:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.RECV__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 3, "OIE_DISCR");
  /*_.OIE_DISCR__V18*/ meltfptr[16] = slot;
    };
    ;
    /*^checksignal */
    MELT_CHECK_SIGNAL ();
    ;
    /*^msend */
    /*msend */
    {
      union meltparam_un argtab[3];
      memset (&argtab, 0, sizeof (argtab));
      /*^ojbmsend.arg */
      argtab[0].meltbp_aptr = /*nil */ (melt_ptr_t *) NULL;
      /*^ojbmsend.arg */
      argtab[1].meltbp_aptr = (melt_ptr_t *) & /*_.SBUF__V3*/ meltfptr[2];
      /*^ojbmsend.arg */
      argtab[2].meltbp_long = /*_#DEPTHP1__L6*/ meltfnum[1];
      /*_.OUTPUT_C_CODE__V19*/ meltfptr[18] =
	meltgc_send ((melt_ptr_t) ( /*_.OIE_DISCR__V18*/ meltfptr[16]),
		     (melt_ptr_t) (( /*!OUTPUT_C_CODE */ meltfrout->
				    tabval[2])),
		     (MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_LONG ""),
		     argtab, "", (union meltparam_un *) 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:601:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
			   (");"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:602:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
				( /*_#DEPTHP1__L6*/ meltfnum[1]), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:603:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]), (" "));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:604:/ locexp");
      /*add2sbuf_string */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
			   melt_string_str ((melt_ptr_t)
					    ( /*_.PTRSTR__V4*/ meltfptr[3])));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:605:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
			   ("->"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:606:/ locexp");
      /*add2sbuf_string */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
			   melt_string_str ((melt_ptr_t)
					    ( /*_.CNAM__V14*/ meltfptr[13])));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:607:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
			   (".nbval = "));
    }
    ;
 /*_#GET_INT__L7*/ meltfnum[3] =
      (melt_get_int ((melt_ptr_t) ( /*_.RECV__V2*/ meltfptr[1])));;

    {
      MELT_LOCATION ("warmelt-outobj.melt:608:/ locexp");
      meltgc_add_strbuf_dec ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
			     ( /*_#GET_INT__L7*/ meltfnum[3]));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:609:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]), (";"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:610:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
				( /*_#DEPTHP1__L6*/ meltfnum[1]), 0);
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:611:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^cond */
    /*cond */ if ( /*_.OROUT__V16*/ meltfptr[15])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{


	  {
	    MELT_LOCATION ("warmelt-outobj.melt:613:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
				 (" "));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:614:/ locexp");
	    /*add2sbuf_string */
	      meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
				 melt_string_str ((melt_ptr_t)
						  ( /*_.PTRSTR__V4*/
						   meltfptr[3])));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:615:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
				 ("->"));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:616:/ locexp");
	    /*add2sbuf_string */
	      meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
				 melt_string_str ((melt_ptr_t)
						  ( /*_.CNAM__V14*/
						   meltfptr[13])));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:617:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
				 (".rout = (meltroutine_ptr_t) ("));
	  }
	  ;
	  MELT_LOCATION ("warmelt-outobj.melt:618:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^msend */
	  /*msend */
	  {
	    union meltparam_un argtab[3];
	    memset (&argtab, 0, sizeof (argtab));
	    /*^ojbmsend.arg */
	    argtab[0].meltbp_aptr = /*nil */ (melt_ptr_t *) NULL;
	    /*^ojbmsend.arg */
	    argtab[1].meltbp_aptr =
	      (melt_ptr_t *) & /*_.SBUF__V3*/ meltfptr[2];
	    /*^ojbmsend.arg */
	    argtab[2].meltbp_long = /*_#DEPTHP1__L6*/ meltfnum[1];
	    /*_.OUTPUT_C_CODE__V20*/ meltfptr[19] =
	      meltgc_send ((melt_ptr_t) ( /*_.OROUT__V16*/ meltfptr[15]),
			   (melt_ptr_t) (( /*!OUTPUT_C_CODE */ meltfrout->
					  tabval[2])),
			   (MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_LONG
			    ""), argtab, "", (union meltparam_un *) 0);
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:619:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
				 (");"));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:620:/ locexp");
	    meltgc_strbuf_add_indent ((melt_ptr_t)
				      ( /*_.SBUF__V3*/ meltfptr[2]),
				      ( /*_#DEPTHP1__L6*/ meltfnum[1]), 0);
	  }
	  ;
	  MELT_LOCATION ("warmelt-outobj.melt:612:/ quasiblock");


	  /*epilog */

	  MELT_LOCATION ("warmelt-outobj.melt:611:/ clear");
	     /*clear *//*_.OUTPUT_C_CODE__V20*/ meltfptr[19] = 0;
	}
	;
      }				/*noelse */
    ;
    MELT_LOCATION ("warmelt-outobj.melt:623:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#STRBUF_USEDLENGTH__L8*/ meltfnum[7] =
      melt_strbuf_usedlength ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]));;
    MELT_LOCATION ("warmelt-outobj.melt:623:/ cond");
    /*cond */ if (
		   /*ifisa */
		   melt_is_instance_of ((melt_ptr_t)
					(( /*!BUFFER_LIMIT_CONT */ meltfrout->
					  tabval[3])),
					(melt_ptr_t) (( /*!CLASS_REFERENCE */
						       meltfrout->tabval[4])))
      )				/*then */
      {
	/*^cond.then */
	/*^getslot */
	{
	  melt_ptr_t slot = NULL, obj = NULL;
	  obj =
	    (melt_ptr_t) (( /*!BUFFER_LIMIT_CONT */ meltfrout->
			   tabval[3])) /*=obj*/ ;
	  melt_object_get_field (slot, obj, 0, "REFERENCED_VALUE");
   /*_.REFERENCED_VALUE__V21*/ meltfptr[19] = slot;
	};
	;
      }
    else
      {				/*^cond.else */

  /*_.REFERENCED_VALUE__V21*/ meltfptr[19] = NULL;;
      }
    ;
    /*^compute */
 /*_#GET_INT__L9*/ meltfnum[8] =
      (melt_get_int
       ((melt_ptr_t) ( /*_.REFERENCED_VALUE__V21*/ meltfptr[19])));;
    /*^compute */
 /*_#IRAW__L10*/ meltfnum[9] =
      (( /*_#GET_INT__L9*/ meltfnum[8]) / (2));;
    /*^compute */
 /*_#I__L11*/ meltfnum[10] =
      (( /*_#STRBUF_USEDLENGTH__L8*/ meltfnum[7]) >
       ( /*_#IRAW__L10*/ meltfnum[9]));;
    MELT_LOCATION ("warmelt-outobj.melt:623:/ cond");
    /*cond */ if ( /*_#I__L11*/ meltfnum[10])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{


#if MELT_HAVE_DEBUG
	  MELT_LOCATION ("warmelt-outobj.melt:624:/ cppif.then");
	  /*^block */
	  /*anyblock */
	  {


	    {
	      /*^locexp */
	      /*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	      melt_dbgcounter++;
#endif
	      ;
	    }
	    ;
	    /*^checksignal */
	    MELT_CHECK_SIGNAL ();
	    ;
     /*_#MELT_NEED_DBG__L12*/ meltfnum[11] =
	      /*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	      ( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	      0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	      ;;
	    MELT_LOCATION ("warmelt-outobj.melt:624:/ cond");
	    /*cond */ if ( /*_#MELT_NEED_DBG__L12*/ meltfnum[11])	/*then */
	      {
		/*^cond.then */
		/*^block */
		/*anyblock */
		{

       /*_#THE_MELTCALLCOUNT__L13*/ meltfnum[12] =
#ifdef meltcallcount
		    meltcallcount	/* the_meltcallcount */
#else
		    0L
#endif /* meltcallcount the_meltcallcount */
		    ;;
		  MELT_LOCATION ("warmelt-outobj.melt:624:/ checksignal");
		  MELT_CHECK_SIGNAL ();
		  ;
		  /*^apply */
		  /*apply */
		  {
		    union meltparam_un argtab[5];
		    memset (&argtab, 0, sizeof (argtab));
		    /*^apply.arg */
		    argtab[0].meltbp_long =
		      /*_#THE_MELTCALLCOUNT__L13*/ meltfnum[12];
		    /*^apply.arg */
		    argtab[1].meltbp_cstring = "warmelt-outobj.melt";
		    /*^apply.arg */
		    argtab[2].meltbp_long = 624;
		    /*^apply.arg */
		    argtab[3].meltbp_cstring =
		      "outcinitfill_objinitclosure huge sbuf=";
		    /*^apply.arg */
		    argtab[4].meltbp_aptr =
		      (melt_ptr_t *) & /*_.SBUF__V3*/ meltfptr[2];
		    /*_.MELT_DEBUG_FUN__V25*/ meltfptr[24] =
		      melt_apply ((meltclosure_ptr_t)
				  (( /*!MELT_DEBUG_FUN */ meltfrout->
				    tabval[1])),
				  (melt_ptr_t) (( /*nil */ NULL)),
				  (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_PTR ""), argtab, "",
				  (union meltparam_un *) 0);
		  }
		  ;
		  /*_.IF___V24*/ meltfptr[23] =
		    /*_.MELT_DEBUG_FUN__V25*/ meltfptr[24];;
		  /*epilog */

		  MELT_LOCATION ("warmelt-outobj.melt:624:/ clear");
		 /*clear *//*_#THE_MELTCALLCOUNT__L13*/ meltfnum[12] = 0;
		  /*^clear */
		 /*clear *//*_.MELT_DEBUG_FUN__V25*/ meltfptr[24] = 0;
		}
		;
	      }
	    else
	      {			/*^cond.else */

      /*_.IF___V24*/ meltfptr[23] = NULL;;
	      }
	    ;
	    MELT_LOCATION ("warmelt-outobj.melt:624:/ quasiblock");


	    /*_.PROGN___V26*/ meltfptr[24] = /*_.IF___V24*/ meltfptr[23];;
	    /*^compute */
	    /*_.IFCPP___V23*/ meltfptr[22] = /*_.PROGN___V26*/ meltfptr[24];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-outobj.melt:624:/ clear");
	       /*clear *//*_#MELT_NEED_DBG__L12*/ meltfnum[11] = 0;
	    /*^clear */
	       /*clear *//*_.IF___V24*/ meltfptr[23] = 0;
	    /*^clear */
	       /*clear *//*_.PROGN___V26*/ meltfptr[24] = 0;
	  }

#else /*MELT_HAVE_DEBUG */
	  /*^cppif.else */
	  /*_.IFCPP___V23*/ meltfptr[22] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:625:/ locexp");

#if MELT_HAVE_DEBUG
	    if (melt_need_debug (0))
	      melt_dbgshortbacktrace (("outcinitfill_objinitclosure huge sbuf"), (15));
#endif
	    ;
	  }
	  ;

#if MELT_HAVE_DEBUG
	  MELT_LOCATION ("warmelt-outobj.melt:626:/ cppif.then");
	  /*^block */
	  /*anyblock */
	  {

	    /*^checksignal */
	    MELT_CHECK_SIGNAL ();
	    ;
     /*_#STRBUF_USEDLENGTH__L14*/ meltfnum[12] =
	      melt_strbuf_usedlength ((melt_ptr_t)
				      ( /*_.SBUF__V3*/ meltfptr[2]));;
	    MELT_LOCATION ("warmelt-outobj.melt:627:/ cond");
	    /*cond */ if (
			   /*ifisa */
			   melt_is_instance_of ((melt_ptr_t)
						(( /*!BUFFER_LIMIT_CONT */
						  meltfrout->tabval[3])),
						(melt_ptr_t) (( /*!CLASS_REFERENCE */ meltfrout->tabval[4])))
	      )			/*then */
	      {
		/*^cond.then */
		/*^getslot */
		{
		  melt_ptr_t slot = NULL, obj = NULL;
		  obj =
		    (melt_ptr_t) (( /*!BUFFER_LIMIT_CONT */ meltfrout->
				   tabval[3])) /*=obj*/ ;
		  melt_object_get_field (slot, obj, 0, "REFERENCED_VALUE");
       /*_.REFERENCED_VALUE__V28*/ meltfptr[24] = slot;
		};
		;
	      }
	    else
	      {			/*^cond.else */

      /*_.REFERENCED_VALUE__V28*/ meltfptr[24] = NULL;;
	      }
	    ;
	    /*^compute */
     /*_#GET_INT__L15*/ meltfnum[11] =
	      (melt_get_int
	       ((melt_ptr_t) ( /*_.REFERENCED_VALUE__V28*/ meltfptr[24])));;
	    /*^compute */
     /*_#I__L16*/ meltfnum[15] =
	      (( /*_#STRBUF_USEDLENGTH__L14*/ meltfnum[12]) <
	       ( /*_#GET_INT__L15*/ meltfnum[11]));;
	    MELT_LOCATION ("warmelt-outobj.melt:626:/ cond");
	    /*cond */ if ( /*_#I__L16*/ meltfnum[15])	/*then */
	      {
		/*^cond.then */
		/*_.IFELSE___V29*/ meltfptr[28] = ( /*nil */ NULL);;
	      }
	    else
	      {
		MELT_LOCATION ("warmelt-outobj.melt:626:/ cond.else");

		/*^block */
		/*anyblock */
		{




		  {
		    /*^locexp */
		    melt_assert_failed (("check limited sbuf"),
					("warmelt-outobj.melt")
					? ("warmelt-outobj.melt") : __FILE__,
					(626) ? (626) : __LINE__,
					__FUNCTION__);
		    ;
		  }
		  ;
		 /*clear *//*_.IFELSE___V29*/ meltfptr[28] = 0;
		  /*epilog */
		}
		;
	      }
	    ;
	    /*^compute */
	    /*_.IFCPP___V27*/ meltfptr[23] = /*_.IFELSE___V29*/ meltfptr[28];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-outobj.melt:626:/ clear");
	       /*clear *//*_#STRBUF_USEDLENGTH__L14*/ meltfnum[12] = 0;
	    /*^clear */
	       /*clear *//*_.REFERENCED_VALUE__V28*/ meltfptr[24] = 0;
	    /*^clear */
	       /*clear *//*_#GET_INT__L15*/ meltfnum[11] = 0;
	    /*^clear */
	       /*clear *//*_#I__L16*/ meltfnum[15] = 0;
	    /*^clear */
	       /*clear *//*_.IFELSE___V29*/ meltfptr[28] = 0;
	  }

#else /*MELT_HAVE_DEBUG */
	  /*^cppif.else */
	  /*_.IFCPP___V27*/ meltfptr[23] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
	  ;
	  MELT_LOCATION ("warmelt-outobj.melt:623:/ quasiblock");


	  /*_.PROGN___V30*/ meltfptr[24] = /*_.IFCPP___V27*/ meltfptr[23];;
	  /*^compute */
	  /*_.IFELSE___V22*/ meltfptr[21] = /*_.PROGN___V30*/ meltfptr[24];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-outobj.melt:623:/ clear");
	     /*clear *//*_.IFCPP___V23*/ meltfptr[22] = 0;
	  /*^clear */
	     /*clear *//*_.IFCPP___V27*/ meltfptr[23] = 0;
	  /*^clear */
	     /*clear *//*_.PROGN___V30*/ meltfptr[24] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

  /*_.IFELSE___V22*/ meltfptr[21] = NULL;;
      }
    ;
    /*^compute */
    /*_.LET___V13*/ meltfptr[8] = /*_.IFELSE___V22*/ meltfptr[21];;

    MELT_LOCATION ("warmelt-outobj.melt:575:/ clear");
	   /*clear *//*_.CNAM__V14*/ meltfptr[13] = 0;
    /*^clear */
	   /*clear *//*_.OLOCVAR__V15*/ meltfptr[14] = 0;
    /*^clear */
	   /*clear *//*_.OROUT__V16*/ meltfptr[15] = 0;
    /*^clear */
	   /*clear *//*_#DEPTHP1__L6*/ meltfnum[1] = 0;
    /*^clear */
	   /*clear *//*_.OIE_DISCR__V18*/ meltfptr[16] = 0;
    /*^clear */
	   /*clear *//*_.OUTPUT_C_CODE__V19*/ meltfptr[18] = 0;
    /*^clear */
	   /*clear *//*_#GET_INT__L7*/ meltfnum[3] = 0;
    /*^clear */
	   /*clear *//*_#STRBUF_USEDLENGTH__L8*/ meltfnum[7] = 0;
    /*^clear */
	   /*clear *//*_.REFERENCED_VALUE__V21*/ meltfptr[19] = 0;
    /*^clear */
	   /*clear *//*_#GET_INT__L9*/ meltfnum[8] = 0;
    /*^clear */
	   /*clear *//*_#IRAW__L10*/ meltfnum[9] = 0;
    /*^clear */
	   /*clear *//*_#I__L11*/ meltfnum[10] = 0;
    /*^clear */
	   /*clear *//*_.IFELSE___V22*/ meltfptr[21] = 0;
    MELT_LOCATION ("warmelt-outobj.melt:571:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.LET___V13*/ meltfptr[8];;

    {
      MELT_LOCATION ("warmelt-outobj.melt:571:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V5*/ meltfptr[4] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V7*/ meltfptr[5] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V11*/ meltfptr[7] = 0;
    /*^clear */
	   /*clear *//*_.LET___V13*/ meltfptr[8] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("OUTCINITFILL_OBJINITCLOSURE", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_18_warmelt_outobj_OUTCINITFILL_OBJINITCLOSURE_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_18_warmelt_outobj_OUTCINITFILL_OBJINITCLOSURE */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_19_warmelt_outobj_OUTDECLINIT_OBJINITROUTINE (meltclosure_ptr_t
						       meltclosp_,
						       melt_ptr_t
						       meltfirstargp_,
						       const
						       melt_argdescr_cell_t
						       meltxargdescr_[],
						       union meltparam_un *
						       meltxargtab_,
						       const
						       melt_argdescr_cell_t
						       meltxresdescr_[],
						       union meltparam_un *
						       meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_19_warmelt_outobj_OUTDECLINIT_OBJINITROUTINE_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_19_warmelt_outobj_OUTDECLINIT_OBJINITROUTINE_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 4
    melt_ptr_t mcfr_varptr[4];
#define MELTFRAM_NBVARNUM 1
    long mcfr_varnum[1];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_19_warmelt_outobj_OUTDECLINIT_OBJINITROUTINE is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_19_warmelt_outobj_OUTDECLINIT_OBJINITROUTINE_st
	 *) meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 4; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_19_warmelt_outobj_OUTDECLINIT_OBJINITROUTINE nbval 4*/
  meltfram__.mcfr_nbvar = 4 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("OUTDECLINIT_OBJINITROUTINE", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-outobj.melt:632:/ getarg");
 /*_.RECV__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.SBUF__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2])) !=
	      NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


    {
      MELT_LOCATION ("warmelt-outobj.melt:633:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
			   (" struct MELT_ROUTINE_STRUCT("));
    }
    ;
 /*_#GET_INT__L1*/ meltfnum[0] =
      (melt_get_int ((melt_ptr_t) ( /*_.RECV__V2*/ meltfptr[1])));;

    {
      MELT_LOCATION ("warmelt-outobj.melt:634:/ locexp");
      meltgc_add_strbuf_dec ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
			     ( /*_#GET_INT__L1*/ meltfnum[0]));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:635:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
			   (") "));
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:636:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.RECV__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 1, "OIE_CNAME");
  /*_.OIE_CNAME__V4*/ meltfptr[3] = slot;
    };
    ;

    {
      /*^locexp */
      /*add2sbuf_string */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
			   melt_string_str ((melt_ptr_t)
					    ( /*_.OIE_CNAME__V4*/
					     meltfptr[3])));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:637:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]), (";"));
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:632:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*epilog */

    /*^clear */
	   /*clear *//*_#GET_INT__L1*/ meltfnum[0] = 0;
    /*^clear */
	   /*clear *//*_.OIE_CNAME__V4*/ meltfptr[3] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("OUTDECLINIT_OBJINITROUTINE", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_19_warmelt_outobj_OUTDECLINIT_OBJINITROUTINE_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*noretval */ NULL);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_19_warmelt_outobj_OUTDECLINIT_OBJINITROUTINE */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_20_warmelt_outobj_OUTCINITFILL_OBJINITROUTINE (meltclosure_ptr_t
							meltclosp_,
							melt_ptr_t
							meltfirstargp_,
							const
							melt_argdescr_cell_t
							meltxargdescr_[],
							union meltparam_un *
							meltxargtab_,
							const
							melt_argdescr_cell_t
							meltxresdescr_[],
							union meltparam_un *
							meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_20_warmelt_outobj_OUTCINITFILL_OBJINITROUTINE_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_20_warmelt_outobj_OUTCINITFILL_OBJINITROUTINE_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 51
    melt_ptr_t mcfr_varptr[51];
#define MELTFRAM_NBVARNUM 26
    long mcfr_varnum[26];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_20_warmelt_outobj_OUTCINITFILL_OBJINITROUTINE is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct
	 frame_meltrout_20_warmelt_outobj_OUTCINITFILL_OBJINITROUTINE_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 51; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_20_warmelt_outobj_OUTCINITFILL_OBJINITROUTINE nbval 51*/
  meltfram__.mcfr_nbvar = 51 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("OUTCINITFILL_OBJINITROUTINE", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-outobj.melt:642:/ getarg");
 /*_.RECV__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.SBUF__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2])) !=
	      NULL);


  /*getarg#2 */
  /*^getarg */
  if (meltxargdescr_[1] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.PTRSTR__V4*/ meltfptr[3] =
    (meltxargtab_[1].meltbp_aptr) ? (*(meltxargtab_[1].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.PTRSTR__V4*/ meltfptr[3])) !=
	      NULL);


  /*getarg#3 */
  /*^getarg */
  if (meltxargdescr_[2] != MELTBPAR_LONG)
    goto lab_endgetargs;
 /*_#DEPTH__L1*/ meltfnum[0] = meltxargtab_[2].meltbp_long;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:643:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L2*/ meltfnum[1] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.RECV__V2*/ meltfptr[1]),
			     (melt_ptr_t) (( /*!CLASS_OBJINITROUTINE */
					    meltfrout->tabval[0])));;
      MELT_LOCATION ("warmelt-outobj.melt:643:/ cond");
      /*cond */ if ( /*_#IS_A__L2*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V6*/ meltfptr[5] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-outobj.melt:643:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("outcinitfill_objinitroutine check recv"),
				  ("warmelt-outobj.melt")
				  ? ("warmelt-outobj.melt") : __FILE__,
				  (643) ? (643) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V6*/ meltfptr[5] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V5*/ meltfptr[4] = /*_.IFELSE___V6*/ meltfptr[5];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:643:/ clear");
	     /*clear *//*_#IS_A__L2*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V6*/ meltfptr[5] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V5*/ meltfptr[4] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:644:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L3*/ meltfnum[1] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-outobj.melt:644:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L3*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L4*/ meltfnum[3] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-outobj.melt:644:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[8];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L4*/ meltfnum[3];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-outobj.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 644;
	      /*^apply.arg */
	      argtab[3].meltbp_aptr =
		(melt_ptr_t *) & /*_.RECV__V2*/ meltfptr[1];
	      /*^apply.arg */
	      argtab[4].meltbp_cstring = "outcinitfill_objinitroutine recv=";
	      /*^apply.arg */
	      argtab[5].meltbp_aptr =
		(melt_ptr_t *) & /*_.RECV__V2*/ meltfptr[1];
	      /*^apply.arg */
	      argtab[6].meltbp_cstring = " ptrstr=";
	      /*^apply.arg */
	      argtab[7].meltbp_aptr =
		(melt_ptr_t *) & /*_.PTRSTR__V4*/ meltfptr[3];
	      /*_.MELT_DEBUG_FUN__V9*/ meltfptr[8] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[1])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_PTR
			     MELTBPARSTR_CSTRING MELTBPARSTR_PTR
			     MELTBPARSTR_CSTRING MELTBPARSTR_PTR ""), argtab,
			    "", (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V8*/ meltfptr[7] = /*_.MELT_DEBUG_FUN__V9*/ meltfptr[8];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-outobj.melt:644:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L4*/ meltfnum[3] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V9*/ meltfptr[8] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V8*/ meltfptr[7] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-outobj.melt:644:/ quasiblock");


      /*_.PROGN___V10*/ meltfptr[8] = /*_.IF___V8*/ meltfptr[7];;
      /*^compute */
      /*_.IFCPP___V7*/ meltfptr[5] = /*_.PROGN___V10*/ meltfptr[8];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:644:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L3*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IF___V8*/ meltfptr[7] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V10*/ meltfptr[8] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V7*/ meltfptr[5] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:645:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_STRING__L5*/ meltfnum[3] =
	(melt_magic_discr ((melt_ptr_t) ( /*_.PTRSTR__V4*/ meltfptr[3])) ==
	 MELTOBMAG_STRING);;
      MELT_LOCATION ("warmelt-outobj.melt:645:/ cond");
      /*cond */ if ( /*_#IS_STRING__L5*/ meltfnum[3])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V12*/ meltfptr[8] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-outobj.melt:645:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("outcinitfill_objinitroutine check ptrstr"), ("warmelt-outobj.melt") ? ("warmelt-outobj.melt") : __FILE__, (645) ? (645) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V12*/ meltfptr[8] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V11*/ meltfptr[7] = /*_.IFELSE___V12*/ meltfptr[8];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:645:/ clear");
	     /*clear *//*_#IS_STRING__L5*/ meltfnum[3] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V12*/ meltfptr[8] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V11*/ meltfptr[7] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-outobj.melt:646:/ quasiblock");


    /*^getslot */
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.RECV__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 1, "OIE_CNAME");
  /*_.CNAM__V13*/ meltfptr[8] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-outobj.melt:647:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.RECV__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 5, "OIR_PROCROUTINE");
  /*_.IPRO__V14*/ meltfptr[13] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-outobj.melt:648:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.RECV__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 4, "OIE_LOCVAR");
  /*_.OLOCVAR__V15*/ meltfptr[14] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-outobj.melt:649:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.RECV__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 2, "OIE_DATA");
  /*_.NDATR__V16*/ meltfptr[15] = slot;
    };
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:651:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
			   ("/*inirout "));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:652:/ locexp");
      /*add2sbuf_string */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
			   melt_string_str ((melt_ptr_t)
					    ( /*_.CNAM__V13*/ meltfptr[8])));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:653:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
			   ("*/"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:654:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
				(1), 0);
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:655:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^cond */
    /*cond */ if ( /*_.OLOCVAR__V15*/ meltfptr[14])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-outobj.melt:658:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^msend */
	  /*msend */
	  {
	    union meltparam_un argtab[3];
	    memset (&argtab, 0, sizeof (argtab));
	    /*^ojbmsend.arg */
	    argtab[0].meltbp_aptr = /*nil */ (melt_ptr_t *) NULL;
	    /*^ojbmsend.arg */
	    argtab[1].meltbp_aptr =
	      (melt_ptr_t *) & /*_.SBUF__V3*/ meltfptr[2];
	    /*^ojbmsend.arg */
	    argtab[2].meltbp_long = 1;
	    /*_.OUTPUT_C_CODE__V17*/ meltfptr[16] =
	      meltgc_send ((melt_ptr_t) ( /*_.OLOCVAR__V15*/ meltfptr[14]),
			   (melt_ptr_t) (( /*!OUTPUT_C_CODE */ meltfrout->
					  tabval[2])),
			   (MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_LONG
			    ""), argtab, "", (union meltparam_un *) 0);
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:659:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
				 (" = (melt_ptr_t) &"));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:660:/ locexp");
	    /*add2sbuf_string */
	      meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
				 melt_string_str ((melt_ptr_t)
						  ( /*_.PTRSTR__V4*/
						   meltfptr[3])));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:661:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
				 ("->"));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:662:/ locexp");
	    /*add2sbuf_string */
	      meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
				 melt_string_str ((melt_ptr_t)
						  ( /*_.CNAM__V13*/
						   meltfptr[8])));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:663:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
				 (";"));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:664:/ locexp");
	    meltgc_strbuf_add_indent ((melt_ptr_t)
				      ( /*_.SBUF__V3*/ meltfptr[2]), (1), 0);
	  }
	  ;
	  MELT_LOCATION ("warmelt-outobj.melt:656:/ quasiblock");


	  /*epilog */

	  MELT_LOCATION ("warmelt-outobj.melt:655:/ clear");
	     /*clear *//*_.OUTPUT_C_CODE__V17*/ meltfptr[16] = 0;
	}
	;
      }				/*noelse */
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:666:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]), (" "));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:667:/ locexp");
      /*add2sbuf_string */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
			   melt_string_str ((melt_ptr_t)
					    ( /*_.PTRSTR__V4*/ meltfptr[3])));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:668:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
			   ("->"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:669:/ locexp");
      /*add2sbuf_string */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
			   melt_string_str ((melt_ptr_t)
					    ( /*_.CNAM__V13*/ meltfptr[8])));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:670:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
			   (".discr = (meltobject_ptr_t)("));
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:671:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.RECV__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 3, "OIE_DISCR");
  /*_.OIE_DISCR__V18*/ meltfptr[16] = slot;
    };
    ;
    /*^checksignal */
    MELT_CHECK_SIGNAL ();
    ;
    /*^msend */
    /*msend */
    {
      union meltparam_un argtab[3];
      memset (&argtab, 0, sizeof (argtab));
      /*^ojbmsend.arg */
      argtab[0].meltbp_aptr = /*nil */ (melt_ptr_t *) NULL;
      /*^ojbmsend.arg */
      argtab[1].meltbp_aptr = (melt_ptr_t *) & /*_.SBUF__V3*/ meltfptr[2];
      /*^ojbmsend.arg */
      argtab[2].meltbp_long = 1;
      /*_.OUTPUT_C_CODE__V19*/ meltfptr[18] =
	meltgc_send ((melt_ptr_t) ( /*_.OIE_DISCR__V18*/ meltfptr[16]),
		     (melt_ptr_t) (( /*!OUTPUT_C_CODE */ meltfrout->
				    tabval[2])),
		     (MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_LONG ""),
		     argtab, "", (union meltparam_un *) 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:672:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
			   (");"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:673:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
				(1), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:674:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
			   (" strncpy ("));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:675:/ locexp");
      /*add2sbuf_string */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
			   melt_string_str ((melt_ptr_t)
					    ( /*_.PTRSTR__V4*/ meltfptr[3])));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:676:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
			   ("->"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:677:/ locexp");
      /*add2sbuf_string */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
			   melt_string_str ((melt_ptr_t)
					    ( /*_.CNAM__V13*/ meltfptr[8])));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:678:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
			   (".routdescr, \""));
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:679:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#IS_A__L6*/ meltfnum[1] =
      melt_is_instance_of ((melt_ptr_t) ( /*_.NDATR__V16*/ meltfptr[15]),
			   (melt_ptr_t) (( /*!CLASS_NREP_DATAROUTINE */
					  meltfrout->tabval[3])));;
    MELT_LOCATION ("warmelt-outobj.melt:679:/ cond");
    /*cond */ if ( /*_#IS_A__L6*/ meltfnum[1])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-outobj.melt:680:/ quasiblock");


	  /*^getslot */
	  {
	    melt_ptr_t slot = NULL, obj = NULL;
	    obj = (melt_ptr_t) ( /*_.NDATR__V16*/ meltfptr[15]) /*=obj*/ ;
	    melt_object_get_field (slot, obj, 1, "NDATA_NAME");
    /*_.DNAM__V20*/ meltfptr[19] = slot;
	  };
	  ;
	  MELT_LOCATION ("warmelt-outobj.melt:681:/ getslot");
	  {
	    melt_ptr_t slot = NULL, obj = NULL;
	    obj = (melt_ptr_t) ( /*_.NDATR__V16*/ meltfptr[15]) /*=obj*/ ;
	    melt_object_get_field (slot, obj, 5, "NDROU_PROC");
    /*_.DPRO__V21*/ meltfptr[20] = slot;
	  };
	  ;

#if MELT_HAVE_DEBUG
	  MELT_LOCATION ("warmelt-outobj.melt:683:/ cppif.then");
	  /*^block */
	  /*anyblock */
	  {


	    {
	      /*^locexp */
	      /*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	      melt_dbgcounter++;
#endif
	      ;
	    }
	    ;
	    /*^checksignal */
	    MELT_CHECK_SIGNAL ();
	    ;
     /*_#MELT_NEED_DBG__L7*/ meltfnum[3] =
	      /*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	      ( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	      0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	      ;;
	    MELT_LOCATION ("warmelt-outobj.melt:683:/ cond");
	    /*cond */ if ( /*_#MELT_NEED_DBG__L7*/ meltfnum[3])	/*then */
	      {
		/*^cond.then */
		/*^block */
		/*anyblock */
		{

       /*_#THE_MELTCALLCOUNT__L8*/ meltfnum[7] =
#ifdef meltcallcount
		    meltcallcount	/* the_meltcallcount */
#else
		    0L
#endif /* meltcallcount the_meltcallcount */
		    ;;
		  MELT_LOCATION ("warmelt-outobj.melt:683:/ checksignal");
		  MELT_CHECK_SIGNAL ();
		  ;
		  /*^apply */
		  /*apply */
		  {
		    union meltparam_un argtab[8];
		    memset (&argtab, 0, sizeof (argtab));
		    /*^apply.arg */
		    argtab[0].meltbp_long =
		      /*_#THE_MELTCALLCOUNT__L8*/ meltfnum[7];
		    /*^apply.arg */
		    argtab[1].meltbp_cstring = "warmelt-outobj.melt";
		    /*^apply.arg */
		    argtab[2].meltbp_long = 683;
		    /*^apply.arg */
		    argtab[3].meltbp_aptr =
		      (melt_ptr_t *) & /*_.NDATR__V16*/ meltfptr[15];
		    /*^apply.arg */
		    argtab[4].meltbp_cstring =
		      "outcinitfill_objinitroutine ndatr=";
		    /*^apply.arg */
		    argtab[5].meltbp_aptr =
		      (melt_ptr_t *) & /*_.NDATR__V16*/ meltfptr[15];
		    /*^apply.arg */
		    argtab[6].meltbp_cstring = " dpro=";
		    /*^apply.arg */
		    argtab[7].meltbp_aptr =
		      (melt_ptr_t *) & /*_.DPRO__V21*/ meltfptr[20];
		    /*_.MELT_DEBUG_FUN__V24*/ meltfptr[23] =
		      melt_apply ((meltclosure_ptr_t)
				  (( /*!MELT_DEBUG_FUN */ meltfrout->
				    tabval[1])),
				  (melt_ptr_t) (( /*nil */ NULL)),
				  (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_LONG MELTBPARSTR_PTR
				   MELTBPARSTR_CSTRING MELTBPARSTR_PTR
				   MELTBPARSTR_CSTRING MELTBPARSTR_PTR ""),
				  argtab, "", (union meltparam_un *) 0);
		  }
		  ;
		  /*_.IF___V23*/ meltfptr[22] =
		    /*_.MELT_DEBUG_FUN__V24*/ meltfptr[23];;
		  /*epilog */

		  MELT_LOCATION ("warmelt-outobj.melt:683:/ clear");
		 /*clear *//*_#THE_MELTCALLCOUNT__L8*/ meltfnum[7] = 0;
		  /*^clear */
		 /*clear *//*_.MELT_DEBUG_FUN__V24*/ meltfptr[23] = 0;
		}
		;
	      }
	    else
	      {			/*^cond.else */

      /*_.IF___V23*/ meltfptr[22] = NULL;;
	      }
	    ;
	    MELT_LOCATION ("warmelt-outobj.melt:683:/ quasiblock");


	    /*_.PROGN___V25*/ meltfptr[23] = /*_.IF___V23*/ meltfptr[22];;
	    /*^compute */
	    /*_.IFCPP___V22*/ meltfptr[21] = /*_.PROGN___V25*/ meltfptr[23];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-outobj.melt:683:/ clear");
	       /*clear *//*_#MELT_NEED_DBG__L7*/ meltfnum[3] = 0;
	    /*^clear */
	       /*clear *//*_.IF___V23*/ meltfptr[22] = 0;
	    /*^clear */
	       /*clear *//*_.PROGN___V25*/ meltfptr[23] = 0;
	  }

#else /*MELT_HAVE_DEBUG */
	  /*^cppif.else */
	  /*_.IFCPP___V22*/ meltfptr[21] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
	  ;
	  MELT_LOCATION ("warmelt-outobj.melt:684:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
   /*_#IS_A__L9*/ meltfnum[7] =
	    melt_is_instance_of ((melt_ptr_t) ( /*_.DNAM__V20*/ meltfptr[19]),
				 (melt_ptr_t) (( /*!CLASS_NAMED */ meltfrout->
						tabval[4])));;
	  MELT_LOCATION ("warmelt-outobj.melt:684:/ cond");
	  /*cond */ if ( /*_#IS_A__L9*/ meltfnum[7])	/*then */
	    {
	      /*^cond.then */
	      /*^block */
	      /*anyblock */
	      {

		MELT_LOCATION ("warmelt-outobj.melt:685:/ getslot");
		{
		  melt_ptr_t slot = NULL, obj = NULL;
		  obj =
		    (melt_ptr_t) ( /*_.DNAM__V20*/ meltfptr[19]) /*=obj*/ ;
		  melt_object_get_field (slot, obj, 1, "NAMED_NAME");
      /*_.NAMED_NAME__V26*/ meltfptr[22] = slot;
		};
		;

		{
		  /*^locexp */
		  meltgc_add_strbuf_cstr ((melt_ptr_t)
					  ( /*_.SBUF__V3*/ meltfptr[2]),
					  melt_string_str ((melt_ptr_t)
							   ( /*_.NAMED_NAME__V26*/ meltfptr[22])));
		}
		;
		/*epilog */

		MELT_LOCATION ("warmelt-outobj.melt:684:/ clear");
	       /*clear *//*_.NAMED_NAME__V26*/ meltfptr[22] = 0;
	      }
	      ;
	    }			/*noelse */
	  ;
	  MELT_LOCATION ("warmelt-outobj.melt:686:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
   /*_#IS_A__L10*/ meltfnum[3] =
	    melt_is_instance_of ((melt_ptr_t) ( /*_.DPRO__V21*/ meltfptr[20]),
				 (melt_ptr_t) (( /*!CLASS_NREP_ROUTPROC */
						meltfrout->tabval[5])));;
	  MELT_LOCATION ("warmelt-outobj.melt:686:/ cond");
	  /*cond */ if ( /*_#IS_A__L10*/ meltfnum[3])	/*then */
	    {
	      /*^cond.then */
	      /*^block */
	      /*anyblock */
	      {

		MELT_LOCATION ("warmelt-outobj.melt:687:/ quasiblock");


		/*^getslot */
		{
		  melt_ptr_t slot = NULL, obj = NULL;
		  obj =
		    (melt_ptr_t) ( /*_.DPRO__V21*/ meltfptr[20]) /*=obj*/ ;
		  melt_object_get_field (slot, obj, 0, "NREP_LOC");
      /*_.DLOC__V27*/ meltfptr[23] = slot;
		};
		;
     /*_.MIXINT_VAL__V28*/ meltfptr[22] =
		  (melt_val_mixint
		   ((melt_ptr_t) ( /*_.DLOC__V27*/ meltfptr[23])));;
		MELT_LOCATION ("warmelt-outobj.melt:688:/ cond");
		/*cond */ if ( /*_.MIXINT_VAL__V28*/ meltfptr[22])	/*then */
		  {
		    /*^cond.then */
		    /*_.LOCFIL__V29*/ meltfptr[28] =
		      /*_.MIXINT_VAL__V28*/ meltfptr[22];;
		  }
		else
		  {
		    MELT_LOCATION ("warmelt-outobj.melt:688:/ cond.else");

		    /*^block */
		    /*anyblock */
		    {

       /*_.MIXLOC_VAL__V30*/ meltfptr[29] =
			(melt_val_mixloc
			 ((melt_ptr_t) ( /*_.DLOC__V27*/ meltfptr[23])));;
		      /*^compute */
		      /*_.LOCFIL__V29*/ meltfptr[28] =
			/*_.MIXLOC_VAL__V30*/ meltfptr[29];;
		      /*epilog */

		      MELT_LOCATION ("warmelt-outobj.melt:688:/ clear");
		 /*clear *//*_.MIXLOC_VAL__V30*/ meltfptr[29] = 0;
		    }
		    ;
		  }
		;

		{
		  MELT_LOCATION ("warmelt-outobj.melt:690:/ locexp");
		  /*add2sbuf_strconst */
		    meltgc_add_strbuf ((melt_ptr_t)
				       ( /*_.SBUF__V3*/ meltfptr[2]), (" @"));
		}
		;

		{
		  MELT_LOCATION ("warmelt-outobj.melt:691:/ locexp");
		  meltgc_add_strbuf_cstr ((melt_ptr_t)
					  ( /*_.SBUF__V3*/ meltfptr[2]),
					  melt_string_str ((melt_ptr_t)
							   ( /*_.LOCFIL__V29*/
							    meltfptr[28])));
		}
		;

		{
		  MELT_LOCATION ("warmelt-outobj.melt:692:/ locexp");
		  /*add2sbuf_strconst */
		    meltgc_add_strbuf ((melt_ptr_t)
				       ( /*_.SBUF__V3*/ meltfptr[2]), (":"));
		}
		;
     /*_#GET_INT__L11*/ meltfnum[10] =
		  (melt_get_int
		   ((melt_ptr_t) ( /*_.DLOC__V27*/ meltfptr[23])));;

		{
		  MELT_LOCATION ("warmelt-outobj.melt:693:/ locexp");
		  meltgc_add_strbuf_dec ((melt_ptr_t)
					 ( /*_.SBUF__V3*/ meltfptr[2]),
					 ( /*_#GET_INT__L11*/ meltfnum[10]));
		}
		;

		MELT_LOCATION ("warmelt-outobj.melt:687:/ clear");
	       /*clear *//*_.DLOC__V27*/ meltfptr[23] = 0;
		/*^clear */
	       /*clear *//*_.MIXINT_VAL__V28*/ meltfptr[22] = 0;
		/*^clear */
	       /*clear *//*_.LOCFIL__V29*/ meltfptr[28] = 0;
		/*^clear */
	       /*clear *//*_#GET_INT__L11*/ meltfnum[10] = 0;
		/*epilog */
	      }
	      ;
	    }			/*noelse */
	  ;

	  MELT_LOCATION ("warmelt-outobj.melt:680:/ clear");
	     /*clear *//*_.DNAM__V20*/ meltfptr[19] = 0;
	  /*^clear */
	     /*clear *//*_.DPRO__V21*/ meltfptr[20] = 0;
	  /*^clear */
	     /*clear *//*_.IFCPP___V22*/ meltfptr[21] = 0;
	  /*^clear */
	     /*clear *//*_#IS_A__L9*/ meltfnum[7] = 0;
	  /*^clear */
	     /*clear *//*_#IS_A__L10*/ meltfnum[3] = 0;
	  /*epilog */
	}
	;
      }
    else
      {
	MELT_LOCATION ("warmelt-outobj.melt:679:/ cond.else");

	/*^block */
	/*anyblock */
	{


	  {
	    MELT_LOCATION ("warmelt-outobj.melt:697:/ locexp");
	    meltgc_add_strbuf_cstr ((melt_ptr_t)
				    ( /*_.SBUF__V3*/ meltfptr[2]),
				    melt_string_str ((melt_ptr_t)
						     ( /*_.CNAM__V13*/
						      meltfptr[8])));
	  }
	  ;
	  /*epilog */
	}
	;
      }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:698:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
			   ("\",  MELT_ROUTDESCR_LEN - 1);"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:699:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
				(1), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:700:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]), (" "));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:701:/ locexp");
      /*add2sbuf_string */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
			   melt_string_str ((melt_ptr_t)
					    ( /*_.PTRSTR__V4*/ meltfptr[3])));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:702:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
			   ("->"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:703:/ locexp");
      /*add2sbuf_string */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
			   melt_string_str ((melt_ptr_t)
					    ( /*_.CNAM__V13*/ meltfptr[8])));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:704:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
			   (".nbval = "));
    }
    ;
 /*_#GET_INT__L12*/ meltfnum[10] =
      (melt_get_int ((melt_ptr_t) ( /*_.RECV__V2*/ meltfptr[1])));;

    {
      MELT_LOCATION ("warmelt-outobj.melt:705:/ locexp");
      meltgc_add_strbuf_dec ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
			     ( /*_#GET_INT__L12*/ meltfnum[10]));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:706:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]), (";"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:707:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
				(1), 0);
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:708:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^cond */
    /*cond */ if ( /*_.IPRO__V14*/ meltfptr[13])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{


#if MELT_HAVE_DEBUG
	  MELT_LOCATION ("warmelt-outobj.melt:710:/ cppif.then");
	  /*^block */
	  /*anyblock */
	  {


	    {
	      /*^locexp */
	      /*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	      melt_dbgcounter++;
#endif
	      ;
	    }
	    ;
	    /*^checksignal */
	    MELT_CHECK_SIGNAL ();
	    ;
     /*_#MELT_NEED_DBG__L13*/ meltfnum[7] =
	      /*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	      ( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	      0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	      ;;
	    MELT_LOCATION ("warmelt-outobj.melt:710:/ cond");
	    /*cond */ if ( /*_#MELT_NEED_DBG__L13*/ meltfnum[7])	/*then */
	      {
		/*^cond.then */
		/*^block */
		/*anyblock */
		{

       /*_#THE_MELTCALLCOUNT__L14*/ meltfnum[3] =
#ifdef meltcallcount
		    meltcallcount	/* the_meltcallcount */
#else
		    0L
#endif /* meltcallcount the_meltcallcount */
		    ;;
		  MELT_LOCATION ("warmelt-outobj.melt:710:/ checksignal");
		  MELT_CHECK_SIGNAL ();
		  ;
		  /*^apply */
		  /*apply */
		  {
		    union meltparam_un argtab[5];
		    memset (&argtab, 0, sizeof (argtab));
		    /*^apply.arg */
		    argtab[0].meltbp_long =
		      /*_#THE_MELTCALLCOUNT__L14*/ meltfnum[3];
		    /*^apply.arg */
		    argtab[1].meltbp_cstring = "warmelt-outobj.melt";
		    /*^apply.arg */
		    argtab[2].meltbp_long = 710;
		    /*^apply.arg */
		    argtab[3].meltbp_cstring =
		      "outcinitfill_objinitroutine ipro=";
		    /*^apply.arg */
		    argtab[4].meltbp_aptr =
		      (melt_ptr_t *) & /*_.IPRO__V14*/ meltfptr[13];
		    /*_.MELT_DEBUG_FUN__V33*/ meltfptr[22] =
		      melt_apply ((meltclosure_ptr_t)
				  (( /*!MELT_DEBUG_FUN */ meltfrout->
				    tabval[1])),
				  (melt_ptr_t) (( /*nil */ NULL)),
				  (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_PTR ""), argtab, "",
				  (union meltparam_un *) 0);
		  }
		  ;
		  /*_.IF___V32*/ meltfptr[23] =
		    /*_.MELT_DEBUG_FUN__V33*/ meltfptr[22];;
		  /*epilog */

		  MELT_LOCATION ("warmelt-outobj.melt:710:/ clear");
		 /*clear *//*_#THE_MELTCALLCOUNT__L14*/ meltfnum[3] = 0;
		  /*^clear */
		 /*clear *//*_.MELT_DEBUG_FUN__V33*/ meltfptr[22] = 0;
		}
		;
	      }
	    else
	      {			/*^cond.else */

      /*_.IF___V32*/ meltfptr[23] = NULL;;
	      }
	    ;
	    MELT_LOCATION ("warmelt-outobj.melt:710:/ quasiblock");


	    /*_.PROGN___V34*/ meltfptr[28] = /*_.IF___V32*/ meltfptr[23];;
	    /*^compute */
	    /*_.IFCPP___V31*/ meltfptr[29] = /*_.PROGN___V34*/ meltfptr[28];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-outobj.melt:710:/ clear");
	       /*clear *//*_#MELT_NEED_DBG__L13*/ meltfnum[7] = 0;
	    /*^clear */
	       /*clear *//*_.IF___V32*/ meltfptr[23] = 0;
	    /*^clear */
	       /*clear *//*_.PROGN___V34*/ meltfptr[28] = 0;
	  }

#else /*MELT_HAVE_DEBUG */
	  /*^cppif.else */
	  /*_.IFCPP___V31*/ meltfptr[29] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
	  ;

#if MELT_HAVE_DEBUG
	  MELT_LOCATION ("warmelt-outobj.melt:711:/ cppif.then");
	  /*^block */
	  /*anyblock */
	  {

	    /*^checksignal */
	    MELT_CHECK_SIGNAL ();
	    ;
     /*_#IS_A__L15*/ meltfnum[3] =
	      melt_is_instance_of ((melt_ptr_t)
				   ( /*_.IPRO__V14*/ meltfptr[13]),
				   (melt_ptr_t) (( /*!CLASS_NAMED */
						  meltfrout->tabval[4])));;
	    MELT_LOCATION ("warmelt-outobj.melt:711:/ cond");
	    /*cond */ if ( /*_#IS_A__L15*/ meltfnum[3])	/*then */
	      {
		/*^cond.then */
		/*_.IFELSE___V36*/ meltfptr[20] = ( /*nil */ NULL);;
	      }
	    else
	      {
		MELT_LOCATION ("warmelt-outobj.melt:711:/ cond.else");

		/*^block */
		/*anyblock */
		{




		  {
		    /*^locexp */
		    melt_assert_failed (("check ipro"),
					("warmelt-outobj.melt")
					? ("warmelt-outobj.melt") : __FILE__,
					(711) ? (711) : __LINE__,
					__FUNCTION__);
		    ;
		  }
		  ;
		 /*clear *//*_.IFELSE___V36*/ meltfptr[20] = 0;
		  /*epilog */
		}
		;
	      }
	    ;
	    /*^compute */
	    /*_.IFCPP___V35*/ meltfptr[19] = /*_.IFELSE___V36*/ meltfptr[20];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-outobj.melt:711:/ clear");
	       /*clear *//*_#IS_A__L15*/ meltfnum[3] = 0;
	    /*^clear */
	       /*clear *//*_.IFELSE___V36*/ meltfptr[20] = 0;
	  }

#else /*MELT_HAVE_DEBUG */
	  /*^cppif.else */
	  /*_.IFCPP___V35*/ meltfptr[19] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:712:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
				 ("MELT_ROUTINE_SET_ROUTCODE(&"));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:713:/ locexp");
	    /*add2sbuf_string */
	      meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
				 melt_string_str ((melt_ptr_t)
						  ( /*_.PTRSTR__V4*/
						   meltfptr[3])));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:714:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
				 ("->"));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:715:/ locexp");
	    /*add2sbuf_string */
	      meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
				 melt_string_str ((melt_ptr_t)
						  ( /*_.CNAM__V13*/
						   meltfptr[8])));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:716:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
				 (", "));
	  }
	  ;
	  MELT_LOCATION ("warmelt-outobj.melt:717:/ getslot");
	  {
	    melt_ptr_t slot = NULL, obj = NULL;
	    obj = (melt_ptr_t) ( /*_.IPRO__V14*/ meltfptr[13]) /*=obj*/ ;
	    melt_object_get_field (slot, obj, 1, "NAMED_NAME");
    /*_.NAMED_NAME__V37*/ meltfptr[21] = slot;
	  };
	  ;

	  {
	    /*^locexp */
	    /*add2sbuf_string */
	      meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
				 melt_string_str ((melt_ptr_t)
						  ( /*_.NAMED_NAME__V37*/
						   meltfptr[21])));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:718:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
				 (");"));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:719:/ locexp");
	    meltgc_strbuf_add_indent ((melt_ptr_t)
				      ( /*_.SBUF__V3*/ meltfptr[2]), (1), 0);
	  }
	  ;
	  MELT_LOCATION ("warmelt-outobj.melt:709:/ quasiblock");


	  /*epilog */

	  MELT_LOCATION ("warmelt-outobj.melt:708:/ clear");
	     /*clear *//*_.IFCPP___V31*/ meltfptr[29] = 0;
	  /*^clear */
	     /*clear *//*_.IFCPP___V35*/ meltfptr[19] = 0;
	  /*^clear */
	     /*clear *//*_.NAMED_NAME__V37*/ meltfptr[21] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

	/*^block */
	/*anyblock */
	{


#if MELT_HAVE_DEBUG
	  MELT_LOCATION ("warmelt-outobj.melt:722:/ cppif.then");
	  /*^block */
	  /*anyblock */
	  {


	    {
	      /*^locexp */
	      /*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	      melt_dbgcounter++;
#endif
	      ;
	    }
	    ;
	    /*^checksignal */
	    MELT_CHECK_SIGNAL ();
	    ;
     /*_#MELT_NEED_DBG__L16*/ meltfnum[7] =
	      /*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	      ( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	      0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	      ;;
	    MELT_LOCATION ("warmelt-outobj.melt:722:/ cond");
	    /*cond */ if ( /*_#MELT_NEED_DBG__L16*/ meltfnum[7])	/*then */
	      {
		/*^cond.then */
		/*^block */
		/*anyblock */
		{

       /*_#THE_MELTCALLCOUNT__L17*/ meltfnum[3] =
#ifdef meltcallcount
		    meltcallcount	/* the_meltcallcount */
#else
		    0L
#endif /* meltcallcount the_meltcallcount */
		    ;;
		  MELT_LOCATION ("warmelt-outobj.melt:722:/ checksignal");
		  MELT_CHECK_SIGNAL ();
		  ;
		  /*^apply */
		  /*apply */
		  {
		    union meltparam_un argtab[5];
		    memset (&argtab, 0, sizeof (argtab));
		    /*^apply.arg */
		    argtab[0].meltbp_long =
		      /*_#THE_MELTCALLCOUNT__L17*/ meltfnum[3];
		    /*^apply.arg */
		    argtab[1].meltbp_cstring = "warmelt-outobj.melt";
		    /*^apply.arg */
		    argtab[2].meltbp_long = 722;
		    /*^apply.arg */
		    argtab[3].meltbp_cstring =
		      "outcinitfill_objinitroutine (noipro) recv=";
		    /*^apply.arg */
		    argtab[4].meltbp_aptr =
		      (melt_ptr_t *) & /*_.RECV__V2*/ meltfptr[1];
		    /*_.MELT_DEBUG_FUN__V40*/ meltfptr[28] =
		      melt_apply ((meltclosure_ptr_t)
				  (( /*!MELT_DEBUG_FUN */ meltfrout->
				    tabval[1])),
				  (melt_ptr_t) (( /*nil */ NULL)),
				  (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_PTR ""), argtab, "",
				  (union meltparam_un *) 0);
		  }
		  ;
		  /*_.IF___V39*/ meltfptr[23] =
		    /*_.MELT_DEBUG_FUN__V40*/ meltfptr[28];;
		  /*epilog */

		  MELT_LOCATION ("warmelt-outobj.melt:722:/ clear");
		 /*clear *//*_#THE_MELTCALLCOUNT__L17*/ meltfnum[3] = 0;
		  /*^clear */
		 /*clear *//*_.MELT_DEBUG_FUN__V40*/ meltfptr[28] = 0;
		}
		;
	      }
	    else
	      {			/*^cond.else */

      /*_.IF___V39*/ meltfptr[23] = NULL;;
	      }
	    ;
	    MELT_LOCATION ("warmelt-outobj.melt:722:/ quasiblock");


	    /*_.PROGN___V41*/ meltfptr[20] = /*_.IF___V39*/ meltfptr[23];;
	    /*^compute */
	    /*_.IFCPP___V38*/ meltfptr[22] = /*_.PROGN___V41*/ meltfptr[20];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-outobj.melt:722:/ clear");
	       /*clear *//*_#MELT_NEED_DBG__L16*/ meltfnum[7] = 0;
	    /*^clear */
	       /*clear *//*_.IF___V39*/ meltfptr[23] = 0;
	    /*^clear */
	       /*clear *//*_.PROGN___V41*/ meltfptr[20] = 0;
	  }

#else /*MELT_HAVE_DEBUG */
	  /*^cppif.else */
	  /*_.IFCPP___V38*/ meltfptr[22] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:723:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
				 ("#warning no procedure in objinitroutine "));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:724:/ locexp");
	    /*add2sbuf_string */
	      meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
				 melt_string_str ((melt_ptr_t)
						  ( /*_.CNAM__V13*/
						   meltfptr[8])));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:725:/ locexp");
	    meltgc_strbuf_add_indent ((melt_ptr_t)
				      ( /*_.SBUF__V3*/ meltfptr[2]), (1), 0);
	  }
	  ;
	  MELT_LOCATION ("warmelt-outobj.melt:721:/ quasiblock");


	  /*epilog */

	  MELT_LOCATION ("warmelt-outobj.melt:708:/ clear");
	     /*clear *//*_.IFCPP___V38*/ meltfptr[22] = 0;
	}
	;
      }
    ;

    MELT_LOCATION ("warmelt-outobj.melt:646:/ clear");
	   /*clear *//*_.CNAM__V13*/ meltfptr[8] = 0;
    /*^clear */
	   /*clear *//*_.IPRO__V14*/ meltfptr[13] = 0;
    /*^clear */
	   /*clear *//*_.OLOCVAR__V15*/ meltfptr[14] = 0;
    /*^clear */
	   /*clear *//*_.NDATR__V16*/ meltfptr[15] = 0;
    /*^clear */
	   /*clear *//*_.OIE_DISCR__V18*/ meltfptr[16] = 0;
    /*^clear */
	   /*clear *//*_.OUTPUT_C_CODE__V19*/ meltfptr[18] = 0;
    /*^clear */
	   /*clear *//*_#IS_A__L6*/ meltfnum[1] = 0;
    /*^clear */
	   /*clear *//*_#GET_INT__L12*/ meltfnum[10] = 0;
    MELT_LOCATION ("warmelt-outobj.melt:730:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#STRBUF_USEDLENGTH__L18*/ meltfnum[3] =
      melt_strbuf_usedlength ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]));;
    MELT_LOCATION ("warmelt-outobj.melt:730:/ cond");
    /*cond */ if (
		   /*ifisa */
		   melt_is_instance_of ((melt_ptr_t)
					(( /*!BUFFER_LIMIT_CONT */ meltfrout->
					  tabval[6])),
					(melt_ptr_t) (( /*!CLASS_REFERENCE */
						       meltfrout->tabval[7])))
      )				/*then */
      {
	/*^cond.then */
	/*^getslot */
	{
	  melt_ptr_t slot = NULL, obj = NULL;
	  obj =
	    (melt_ptr_t) (( /*!BUFFER_LIMIT_CONT */ meltfrout->
			   tabval[6])) /*=obj*/ ;
	  melt_object_get_field (slot, obj, 0, "REFERENCED_VALUE");
   /*_.REFERENCED_VALUE__V42*/ meltfptr[29] = slot;
	};
	;
      }
    else
      {				/*^cond.else */

  /*_.REFERENCED_VALUE__V42*/ meltfptr[29] = NULL;;
      }
    ;
    /*^compute */
 /*_#GET_INT__L19*/ meltfnum[7] =
      (melt_get_int
       ((melt_ptr_t) ( /*_.REFERENCED_VALUE__V42*/ meltfptr[29])));;
    /*^compute */
 /*_#IRAW__L20*/ meltfnum[1] =
      (( /*_#GET_INT__L19*/ meltfnum[7]) / (2));;
    /*^compute */
 /*_#I__L21*/ meltfnum[10] =
      (( /*_#STRBUF_USEDLENGTH__L18*/ meltfnum[3]) >
       ( /*_#IRAW__L20*/ meltfnum[1]));;
    MELT_LOCATION ("warmelt-outobj.melt:730:/ cond");
    /*cond */ if ( /*_#I__L21*/ meltfnum[10])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{


#if MELT_HAVE_DEBUG
	  MELT_LOCATION ("warmelt-outobj.melt:731:/ cppif.then");
	  /*^block */
	  /*anyblock */
	  {


	    {
	      /*^locexp */
	      /*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	      melt_dbgcounter++;
#endif
	      ;
	    }
	    ;
	    /*^checksignal */
	    MELT_CHECK_SIGNAL ();
	    ;
     /*_#MELT_NEED_DBG__L22*/ meltfnum[21] =
	      /*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	      ( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	      0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	      ;;
	    MELT_LOCATION ("warmelt-outobj.melt:731:/ cond");
	    /*cond */ if ( /*_#MELT_NEED_DBG__L22*/ meltfnum[21])	/*then */
	      {
		/*^cond.then */
		/*^block */
		/*anyblock */
		{

       /*_#THE_MELTCALLCOUNT__L23*/ meltfnum[22] =
#ifdef meltcallcount
		    meltcallcount	/* the_meltcallcount */
#else
		    0L
#endif /* meltcallcount the_meltcallcount */
		    ;;
		  MELT_LOCATION ("warmelt-outobj.melt:731:/ checksignal");
		  MELT_CHECK_SIGNAL ();
		  ;
		  /*^apply */
		  /*apply */
		  {
		    union meltparam_un argtab[5];
		    memset (&argtab, 0, sizeof (argtab));
		    /*^apply.arg */
		    argtab[0].meltbp_long =
		      /*_#THE_MELTCALLCOUNT__L23*/ meltfnum[22];
		    /*^apply.arg */
		    argtab[1].meltbp_cstring = "warmelt-outobj.melt";
		    /*^apply.arg */
		    argtab[2].meltbp_long = 731;
		    /*^apply.arg */
		    argtab[3].meltbp_cstring =
		      "outcinitfill_objinitroutine huge sbuf=";
		    /*^apply.arg */
		    argtab[4].meltbp_aptr =
		      (melt_ptr_t *) & /*_.SBUF__V3*/ meltfptr[2];
		    /*_.MELT_DEBUG_FUN__V46*/ meltfptr[23] =
		      melt_apply ((meltclosure_ptr_t)
				  (( /*!MELT_DEBUG_FUN */ meltfrout->
				    tabval[1])),
				  (melt_ptr_t) (( /*nil */ NULL)),
				  (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_PTR ""), argtab, "",
				  (union meltparam_un *) 0);
		  }
		  ;
		  /*_.IF___V45*/ meltfptr[28] =
		    /*_.MELT_DEBUG_FUN__V46*/ meltfptr[23];;
		  /*epilog */

		  MELT_LOCATION ("warmelt-outobj.melt:731:/ clear");
		 /*clear *//*_#THE_MELTCALLCOUNT__L23*/ meltfnum[22] = 0;
		  /*^clear */
		 /*clear *//*_.MELT_DEBUG_FUN__V46*/ meltfptr[23] = 0;
		}
		;
	      }
	    else
	      {			/*^cond.else */

      /*_.IF___V45*/ meltfptr[28] = NULL;;
	      }
	    ;
	    MELT_LOCATION ("warmelt-outobj.melt:731:/ quasiblock");


	    /*_.PROGN___V47*/ meltfptr[20] = /*_.IF___V45*/ meltfptr[28];;
	    /*^compute */
	    /*_.IFCPP___V44*/ meltfptr[21] = /*_.PROGN___V47*/ meltfptr[20];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-outobj.melt:731:/ clear");
	       /*clear *//*_#MELT_NEED_DBG__L22*/ meltfnum[21] = 0;
	    /*^clear */
	       /*clear *//*_.IF___V45*/ meltfptr[28] = 0;
	    /*^clear */
	       /*clear *//*_.PROGN___V47*/ meltfptr[20] = 0;
	  }

#else /*MELT_HAVE_DEBUG */
	  /*^cppif.else */
	  /*_.IFCPP___V44*/ meltfptr[21] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:732:/ locexp");

#if MELT_HAVE_DEBUG
	    if (melt_need_debug (0))
	      melt_dbgshortbacktrace (("outcinitfill_objinitroutine huge sbuf"), (12));
#endif
	    ;
	  }
	  ;

#if MELT_HAVE_DEBUG
	  MELT_LOCATION ("warmelt-outobj.melt:733:/ cppif.then");
	  /*^block */
	  /*anyblock */
	  {

	    /*^checksignal */
	    MELT_CHECK_SIGNAL ();
	    ;
     /*_#STRBUF_USEDLENGTH__L24*/ meltfnum[22] =
	      melt_strbuf_usedlength ((melt_ptr_t)
				      ( /*_.SBUF__V3*/ meltfptr[2]));;
	    MELT_LOCATION ("warmelt-outobj.melt:734:/ cond");
	    /*cond */ if (
			   /*ifisa */
			   melt_is_instance_of ((melt_ptr_t)
						(( /*!BUFFER_LIMIT_CONT */
						  meltfrout->tabval[6])),
						(melt_ptr_t) (( /*!CLASS_REFERENCE */ meltfrout->tabval[7])))
	      )			/*then */
	      {
		/*^cond.then */
		/*^getslot */
		{
		  melt_ptr_t slot = NULL, obj = NULL;
		  obj =
		    (melt_ptr_t) (( /*!BUFFER_LIMIT_CONT */ meltfrout->
				   tabval[6])) /*=obj*/ ;
		  melt_object_get_field (slot, obj, 0, "REFERENCED_VALUE");
       /*_.REFERENCED_VALUE__V49*/ meltfptr[8] = slot;
		};
		;
	      }
	    else
	      {			/*^cond.else */

      /*_.REFERENCED_VALUE__V49*/ meltfptr[8] = NULL;;
	      }
	    ;
	    /*^compute */
     /*_#GET_INT__L25*/ meltfnum[21] =
	      (melt_get_int
	       ((melt_ptr_t) ( /*_.REFERENCED_VALUE__V49*/ meltfptr[8])));;
	    /*^compute */
     /*_#I__L26*/ meltfnum[25] =
	      (( /*_#STRBUF_USEDLENGTH__L24*/ meltfnum[22]) <
	       ( /*_#GET_INT__L25*/ meltfnum[21]));;
	    MELT_LOCATION ("warmelt-outobj.melt:733:/ cond");
	    /*cond */ if ( /*_#I__L26*/ meltfnum[25])	/*then */
	      {
		/*^cond.then */
		/*_.IFELSE___V50*/ meltfptr[13] = ( /*nil */ NULL);;
	      }
	    else
	      {
		MELT_LOCATION ("warmelt-outobj.melt:733:/ cond.else");

		/*^block */
		/*anyblock */
		{




		  {
		    /*^locexp */
		    melt_assert_failed (("check limited sbuf"),
					("warmelt-outobj.melt")
					? ("warmelt-outobj.melt") : __FILE__,
					(733) ? (733) : __LINE__,
					__FUNCTION__);
		    ;
		  }
		  ;
		 /*clear *//*_.IFELSE___V50*/ meltfptr[13] = 0;
		  /*epilog */
		}
		;
	      }
	    ;
	    /*^compute */
	    /*_.IFCPP___V48*/ meltfptr[22] = /*_.IFELSE___V50*/ meltfptr[13];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-outobj.melt:733:/ clear");
	       /*clear *//*_#STRBUF_USEDLENGTH__L24*/ meltfnum[22] = 0;
	    /*^clear */
	       /*clear *//*_.REFERENCED_VALUE__V49*/ meltfptr[8] = 0;
	    /*^clear */
	       /*clear *//*_#GET_INT__L25*/ meltfnum[21] = 0;
	    /*^clear */
	       /*clear *//*_#I__L26*/ meltfnum[25] = 0;
	    /*^clear */
	       /*clear *//*_.IFELSE___V50*/ meltfptr[13] = 0;
	  }

#else /*MELT_HAVE_DEBUG */
	  /*^cppif.else */
	  /*_.IFCPP___V48*/ meltfptr[22] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
	  ;
	  MELT_LOCATION ("warmelt-outobj.melt:730:/ quasiblock");


	  /*_.PROGN___V51*/ meltfptr[14] = /*_.IFCPP___V48*/ meltfptr[22];;
	  /*^compute */
	  /*_.IFELSE___V43*/ meltfptr[19] = /*_.PROGN___V51*/ meltfptr[14];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-outobj.melt:730:/ clear");
	     /*clear *//*_.IFCPP___V44*/ meltfptr[21] = 0;
	  /*^clear */
	     /*clear *//*_.IFCPP___V48*/ meltfptr[22] = 0;
	  /*^clear */
	     /*clear *//*_.PROGN___V51*/ meltfptr[14] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

  /*_.IFELSE___V43*/ meltfptr[19] = NULL;;
      }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:642:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.IFELSE___V43*/ meltfptr[19];;

    {
      MELT_LOCATION ("warmelt-outobj.melt:642:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V5*/ meltfptr[4] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V7*/ meltfptr[5] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V11*/ meltfptr[7] = 0;
    /*^clear */
	   /*clear *//*_#STRBUF_USEDLENGTH__L18*/ meltfnum[3] = 0;
    /*^clear */
	   /*clear *//*_.REFERENCED_VALUE__V42*/ meltfptr[29] = 0;
    /*^clear */
	   /*clear *//*_#GET_INT__L19*/ meltfnum[7] = 0;
    /*^clear */
	   /*clear *//*_#IRAW__L20*/ meltfnum[1] = 0;
    /*^clear */
	   /*clear *//*_#I__L21*/ meltfnum[10] = 0;
    /*^clear */
	   /*clear *//*_.IFELSE___V43*/ meltfptr[19] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("OUTCINITFILL_OBJINITROUTINE", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_20_warmelt_outobj_OUTCINITFILL_OBJINITROUTINE_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_20_warmelt_outobj_OUTCINITFILL_OBJINITROUTINE */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_21_warmelt_outobj_OUTDECLINIT_OBJINITSTRING (meltclosure_ptr_t
						      meltclosp_,
						      melt_ptr_t
						      meltfirstargp_,
						      const
						      melt_argdescr_cell_t
						      meltxargdescr_[],
						      union meltparam_un *
						      meltxargtab_,
						      const
						      melt_argdescr_cell_t
						      meltxresdescr_[],
						      union meltparam_un *
						      meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_21_warmelt_outobj_OUTDECLINIT_OBJINITSTRING_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_21_warmelt_outobj_OUTDECLINIT_OBJINITSTRING_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 4
    melt_ptr_t mcfr_varptr[4];
#define MELTFRAM_NBVARNUM 1
    long mcfr_varnum[1];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_21_warmelt_outobj_OUTDECLINIT_OBJINITSTRING is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_21_warmelt_outobj_OUTDECLINIT_OBJINITSTRING_st
	 *) meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 4; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_21_warmelt_outobj_OUTDECLINIT_OBJINITSTRING nbval 4*/
  meltfram__.mcfr_nbvar = 4 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("OUTDECLINIT_OBJINITSTRING", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-outobj.melt:740:/ getarg");
 /*_.RECV__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.SBUF__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2])) !=
	      NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


    {
      MELT_LOCATION ("warmelt-outobj.melt:741:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
			   (" struct MELT_STRING_STRUCT("));
    }
    ;
 /*_#GET_INT__L1*/ meltfnum[0] =
      (melt_get_int ((melt_ptr_t) ( /*_.RECV__V2*/ meltfptr[1])));;

    {
      MELT_LOCATION ("warmelt-outobj.melt:742:/ locexp");
      meltgc_add_strbuf_dec ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
			     ( /*_#GET_INT__L1*/ meltfnum[0]));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:743:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
			   (") "));
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:744:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.RECV__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 1, "NAMED_NAME");
  /*_.NAMED_NAME__V4*/ meltfptr[3] = slot;
    };
    ;

    {
      /*^locexp */
      /*add2sbuf_string */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
			   melt_string_str ((melt_ptr_t)
					    ( /*_.NAMED_NAME__V4*/
					     meltfptr[3])));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:745:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]), (";"));
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:740:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*epilog */

    /*^clear */
	   /*clear *//*_#GET_INT__L1*/ meltfnum[0] = 0;
    /*^clear */
	   /*clear *//*_.NAMED_NAME__V4*/ meltfptr[3] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("OUTDECLINIT_OBJINITSTRING", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_21_warmelt_outobj_OUTDECLINIT_OBJINITSTRING_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*noretval */ NULL);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_21_warmelt_outobj_OUTDECLINIT_OBJINITSTRING */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_22_warmelt_outobj_OUTCINITFILL_OBJINITSTRING (meltclosure_ptr_t
						       meltclosp_,
						       melt_ptr_t
						       meltfirstargp_,
						       const
						       melt_argdescr_cell_t
						       meltxargdescr_[],
						       union meltparam_un *
						       meltxargtab_,
						       const
						       melt_argdescr_cell_t
						       meltxresdescr_[],
						       union meltparam_un *
						       meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_22_warmelt_outobj_OUTCINITFILL_OBJINITSTRING_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_22_warmelt_outobj_OUTCINITFILL_OBJINITSTRING_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 57
    melt_ptr_t mcfr_varptr[57];
#define MELTFRAM_NBVARNUM 47
    long mcfr_varnum[47];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_22_warmelt_outobj_OUTCINITFILL_OBJINITSTRING is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_22_warmelt_outobj_OUTCINITFILL_OBJINITSTRING_st
	 *) meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 57; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_22_warmelt_outobj_OUTCINITFILL_OBJINITSTRING nbval 57*/
  meltfram__.mcfr_nbvar = 57 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("OUTCINITFILL_OBJINITSTRING", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-outobj.melt:750:/ getarg");
 /*_.RECV__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.SBUF__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2])) !=
	      NULL);


  /*getarg#2 */
  /*^getarg */
  if (meltxargdescr_[1] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.PTRSTR__V4*/ meltfptr[3] =
    (meltxargtab_[1].meltbp_aptr) ? (*(meltxargtab_[1].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.PTRSTR__V4*/ meltfptr[3])) !=
	      NULL);


  /*getarg#3 */
  /*^getarg */
  if (meltxargdescr_[2] != MELTBPAR_LONG)
    goto lab_endgetargs;
 /*_#DEPTH__L1*/ meltfnum[0] = meltxargtab_[2].meltbp_long;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:751:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L2*/ meltfnum[1] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.RECV__V2*/ meltfptr[1]),
			     (melt_ptr_t) (( /*!CLASS_OBJINITSTRING */
					    meltfrout->tabval[0])));;
      MELT_LOCATION ("warmelt-outobj.melt:751:/ cond");
      /*cond */ if ( /*_#IS_A__L2*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V6*/ meltfptr[5] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-outobj.melt:751:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("outcinitfill_objinitstring check recv"),
				  ("warmelt-outobj.melt")
				  ? ("warmelt-outobj.melt") : __FILE__,
				  (751) ? (751) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V6*/ meltfptr[5] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V5*/ meltfptr[4] = /*_.IFELSE___V6*/ meltfptr[5];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:751:/ clear");
	     /*clear *//*_#IS_A__L2*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V6*/ meltfptr[5] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V5*/ meltfptr[4] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:752:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L3*/ meltfnum[1] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-outobj.melt:752:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L3*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L4*/ meltfnum[3] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-outobj.melt:752:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[7];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L4*/ meltfnum[3];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-outobj.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 752;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "outcinitfill_objinitstring recv=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.RECV__V2*/ meltfptr[1];
	      /*^apply.arg */
	      argtab[5].meltbp_cstring = " ptrstr=";
	      /*^apply.arg */
	      argtab[6].meltbp_aptr =
		(melt_ptr_t *) & /*_.PTRSTR__V4*/ meltfptr[3];
	      /*_.MELT_DEBUG_FUN__V9*/ meltfptr[8] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[1])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V8*/ meltfptr[7] = /*_.MELT_DEBUG_FUN__V9*/ meltfptr[8];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-outobj.melt:752:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L4*/ meltfnum[3] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V9*/ meltfptr[8] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V8*/ meltfptr[7] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-outobj.melt:752:/ quasiblock");


      /*_.PROGN___V10*/ meltfptr[8] = /*_.IF___V8*/ meltfptr[7];;
      /*^compute */
      /*_.IFCPP___V7*/ meltfptr[5] = /*_.PROGN___V10*/ meltfptr[8];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:752:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L3*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IF___V8*/ meltfptr[7] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V10*/ meltfptr[8] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V7*/ meltfptr[5] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:753:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_STRING__L5*/ meltfnum[3] =
	(melt_magic_discr ((melt_ptr_t) ( /*_.PTRSTR__V4*/ meltfptr[3])) ==
	 MELTOBMAG_STRING);;
      MELT_LOCATION ("warmelt-outobj.melt:753:/ cond");
      /*cond */ if ( /*_#IS_STRING__L5*/ meltfnum[3])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V12*/ meltfptr[8] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-outobj.melt:753:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("outcinitfill_objinitstring check ptrstr"),
				  ("warmelt-outobj.melt")
				  ? ("warmelt-outobj.melt") : __FILE__,
				  (753) ? (753) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V12*/ meltfptr[8] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V11*/ meltfptr[7] = /*_.IFELSE___V12*/ meltfptr[8];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:753:/ clear");
	     /*clear *//*_#IS_STRING__L5*/ meltfnum[3] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V12*/ meltfptr[8] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V11*/ meltfptr[7] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-outobj.melt:754:/ quasiblock");


    /*^getslot */
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.RECV__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 1, "OIE_CNAME");
  /*_.CNAM__V13*/ meltfptr[8] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-outobj.melt:755:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.RECV__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 4, "OIE_LOCVAR");
  /*_.OLOCVAR__V14*/ meltfptr[13] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-outobj.melt:756:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.RECV__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 2, "OIE_DATA");
  /*_.STRDATA__V15*/ meltfptr[14] = slot;
    };
    ;
 /*_#DATALEN__L6*/ meltfnum[1] =
      melt_string_length ((melt_ptr_t) ( /*_.STRDATA__V15*/ meltfptr[14]));;

    {
      MELT_LOCATION ("warmelt-outobj.melt:759:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
				( /*_#DEPTH__L1*/ meltfnum[0]), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:760:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
			   ("/*inistring "));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:761:/ locexp");
      /*add2sbuf_string */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
			   melt_string_str ((melt_ptr_t)
					    ( /*_.CNAM__V13*/ meltfptr[8])));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:762:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
			   ("*/"));
    }
    ;
 /*_#I__L7*/ meltfnum[3] =
      (( /*_#DEPTH__L1*/ meltfnum[0]) + (1));;

    {
      MELT_LOCATION ("warmelt-outobj.melt:763:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
				( /*_#I__L7*/ meltfnum[3]), 0);
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:764:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^cond */
    /*cond */ if ( /*_.OLOCVAR__V14*/ meltfptr[13])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-outobj.melt:766:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^msend */
	  /*msend */
	  {
	    union meltparam_un argtab[3];
	    memset (&argtab, 0, sizeof (argtab));
	    /*^ojbmsend.arg */
	    argtab[0].meltbp_aptr = /*nil */ (melt_ptr_t *) NULL;
	    /*^ojbmsend.arg */
	    argtab[1].meltbp_aptr =
	      (melt_ptr_t *) & /*_.SBUF__V3*/ meltfptr[2];
	    /*^ojbmsend.arg */
	    argtab[2].meltbp_long = 1;
	    /*_.OUTPUT_C_CODE__V16*/ meltfptr[15] =
	      meltgc_send ((melt_ptr_t) ( /*_.OLOCVAR__V14*/ meltfptr[13]),
			   (melt_ptr_t) (( /*!OUTPUT_C_CODE */ meltfrout->
					  tabval[2])),
			   (MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_LONG
			    ""), argtab, "", (union meltparam_un *) 0);
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:767:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
				 (" = (melt_ptr_t) &"));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:768:/ locexp");
	    /*add2sbuf_string */
	      meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
				 melt_string_str ((melt_ptr_t)
						  ( /*_.PTRSTR__V4*/
						   meltfptr[3])));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:769:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
				 ("->"));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:770:/ locexp");
	    /*add2sbuf_string */
	      meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
				 melt_string_str ((melt_ptr_t)
						  ( /*_.CNAM__V13*/
						   meltfptr[8])));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:771:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
				 (";"));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:772:/ locexp");
	    meltgc_strbuf_add_indent ((melt_ptr_t)
				      ( /*_.SBUF__V3*/ meltfptr[2]), (1), 0);
	  }
	  ;
	  MELT_LOCATION ("warmelt-outobj.melt:765:/ quasiblock");


	  /*epilog */

	  MELT_LOCATION ("warmelt-outobj.melt:764:/ clear");
	     /*clear *//*_.OUTPUT_C_CODE__V16*/ meltfptr[15] = 0;
	}
	;
      }				/*noelse */
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:774:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]), (" "));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:775:/ locexp");
      /*add2sbuf_string */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
			   melt_string_str ((melt_ptr_t)
					    ( /*_.PTRSTR__V4*/ meltfptr[3])));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:776:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
			   ("->"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:777:/ locexp");
      /*add2sbuf_string */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
			   melt_string_str ((melt_ptr_t)
					    ( /*_.CNAM__V13*/ meltfptr[8])));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:778:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
			   (".discr = (meltobject_ptr_t)("));
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:779:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.RECV__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 3, "OIE_DISCR");
  /*_.OIE_DISCR__V17*/ meltfptr[15] = slot;
    };
    ;
    /*^checksignal */
    MELT_CHECK_SIGNAL ();
    ;
    /*^msend */
    /*msend */
    {
      union meltparam_un argtab[3];
      memset (&argtab, 0, sizeof (argtab));
      /*^ojbmsend.arg */
      argtab[0].meltbp_aptr = /*nil */ (melt_ptr_t *) NULL;
      /*^ojbmsend.arg */
      argtab[1].meltbp_aptr = (melt_ptr_t *) & /*_.SBUF__V3*/ meltfptr[2];
      /*^ojbmsend.arg */
      argtab[2].meltbp_long = 1;
      /*_.OUTPUT_C_CODE__V18*/ meltfptr[17] =
	meltgc_send ((melt_ptr_t) ( /*_.OIE_DISCR__V17*/ meltfptr[15]),
		     (melt_ptr_t) (( /*!OUTPUT_C_CODE */ meltfrout->
				    tabval[2])),
		     (MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_LONG ""),
		     argtab, "", (union meltparam_un *) 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:780:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
			   (");"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:781:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
				(1), 0);
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:783:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#I__L8*/ meltfnum[7] =
      (( /*_#DATALEN__L6*/ meltfnum[1]) < (256));;
    MELT_LOCATION ("warmelt-outobj.melt:783:/ cond");
    /*cond */ if ( /*_#I__L8*/ meltfnum[7])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{


	  {
	    MELT_LOCATION ("warmelt-outobj.melt:785:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
				 ("/*small inistring*/ strncpy("));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:786:/ locexp");
	    /*add2sbuf_string */
	      meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
				 melt_string_str ((melt_ptr_t)
						  ( /*_.PTRSTR__V4*/
						   meltfptr[3])));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:787:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
				 ("->"));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:788:/ locexp");
	    /*add2sbuf_string */
	      meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
				 melt_string_str ((melt_ptr_t)
						  ( /*_.CNAM__V13*/
						   meltfptr[8])));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:789:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
				 (".val, \""));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:790:/ locexp");
	    meltgc_add_strbuf_cstr ((melt_ptr_t)
				    ( /*_.SBUF__V3*/ meltfptr[2]),
				    melt_string_str ((melt_ptr_t)
						     ( /*_.STRDATA__V15*/
						      meltfptr[14])));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:791:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
				 ("\", sizeof ("));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:792:/ locexp");
	    /*add2sbuf_string */
	      meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
				 melt_string_str ((melt_ptr_t)
						  ( /*_.PTRSTR__V4*/
						   meltfptr[3])));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:793:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
				 ("->"));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:794:/ locexp");
	    /*add2sbuf_string */
	      meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
				 melt_string_str ((melt_ptr_t)
						  ( /*_.CNAM__V13*/
						   meltfptr[8])));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:795:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
				 (".val)-1);"));
	  }
	  ;
	  MELT_LOCATION ("warmelt-outobj.melt:784:/ quasiblock");


	  /*epilog */
	}
	;
      }
    else
      {
	MELT_LOCATION ("warmelt-outobj.melt:783:/ cond.else");

	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-outobj.melt:803:/ quasiblock");


   /*_#IX__L9*/ meltfnum[8] = 0;;

#if MELT_HAVE_DEBUG
	  MELT_LOCATION ("warmelt-outobj.melt:805:/ cppif.then");
	  /*^block */
	  /*anyblock */
	  {


	    {
	      /*^locexp */
	      /*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	      melt_dbgcounter++;
#endif
	      ;
	    }
	    ;
	    /*^checksignal */
	    MELT_CHECK_SIGNAL ();
	    ;
     /*_#MELT_NEED_DBG__L10*/ meltfnum[9] =
	      /*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	      ( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	      0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	      ;;
	    MELT_LOCATION ("warmelt-outobj.melt:805:/ cond");
	    /*cond */ if ( /*_#MELT_NEED_DBG__L10*/ meltfnum[9])	/*then */
	      {
		/*^cond.then */
		/*^block */
		/*anyblock */
		{

       /*_#THE_MELTCALLCOUNT__L11*/ meltfnum[10] =
#ifdef meltcallcount
		    meltcallcount	/* the_meltcallcount */
#else
		    0L
#endif /* meltcallcount the_meltcallcount */
		    ;;
		  MELT_LOCATION ("warmelt-outobj.melt:805:/ checksignal");
		  MELT_CHECK_SIGNAL ();
		  ;
		  /*^apply */
		  /*apply */
		  {
		    union meltparam_un argtab[5];
		    memset (&argtab, 0, sizeof (argtab));
		    /*^apply.arg */
		    argtab[0].meltbp_long =
		      /*_#THE_MELTCALLCOUNT__L11*/ meltfnum[10];
		    /*^apply.arg */
		    argtab[1].meltbp_cstring = "warmelt-outobj.melt";
		    /*^apply.arg */
		    argtab[2].meltbp_long = 805;
		    /*^apply.arg */
		    argtab[3].meltbp_cstring =
		      "outcinitfill_objinitstring big datalen=";
		    /*^apply.arg */
		    argtab[4].meltbp_long = /*_#DATALEN__L6*/ meltfnum[1];
		    /*_.MELT_DEBUG_FUN__V23*/ meltfptr[22] =
		      melt_apply ((meltclosure_ptr_t)
				  (( /*!MELT_DEBUG_FUN */ meltfrout->
				    tabval[1])),
				  (melt_ptr_t) (( /*nil */ NULL)),
				  (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_LONG ""), argtab, "",
				  (union meltparam_un *) 0);
		  }
		  ;
		  /*_.IF___V22*/ meltfptr[21] =
		    /*_.MELT_DEBUG_FUN__V23*/ meltfptr[22];;
		  /*epilog */

		  MELT_LOCATION ("warmelt-outobj.melt:805:/ clear");
		 /*clear *//*_#THE_MELTCALLCOUNT__L11*/ meltfnum[10] = 0;
		  /*^clear */
		 /*clear *//*_.MELT_DEBUG_FUN__V23*/ meltfptr[22] = 0;
		}
		;
	      }
	    else
	      {			/*^cond.else */

      /*_.IF___V22*/ meltfptr[21] = NULL;;
	      }
	    ;
	    MELT_LOCATION ("warmelt-outobj.melt:805:/ quasiblock");


	    /*_.PROGN___V24*/ meltfptr[22] = /*_.IF___V22*/ meltfptr[21];;
	    /*^compute */
	    /*_.IFCPP___V21*/ meltfptr[20] = /*_.PROGN___V24*/ meltfptr[22];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-outobj.melt:805:/ clear");
	       /*clear *//*_#MELT_NEED_DBG__L10*/ meltfnum[9] = 0;
	    /*^clear */
	       /*clear *//*_.IF___V22*/ meltfptr[21] = 0;
	    /*^clear */
	       /*clear *//*_.PROGN___V24*/ meltfptr[22] = 0;
	  }

#else /*MELT_HAVE_DEBUG */
	  /*^cppif.else */
	  /*_.IFCPP___V21*/ meltfptr[20] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:806:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
				 ("/*big inistring*/"));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:807:/ locexp");
	    meltgc_strbuf_add_indent ((melt_ptr_t)
				      ( /*_.SBUF__V3*/ meltfptr[2]), (1), 0);
	  }
	  ;
	  MELT_LOCATION ("warmelt-outobj.melt:808:/ loop");
	  /*loop */
	  {
	  labloop_INISTRLOOP_1:;
				/*^loopbody */

	    /*^block */
	    /*anyblock */
	    {

	      /*^checksignal */
	      MELT_CHECK_SIGNAL ();
	      ;

#if MELT_HAVE_DEBUG
	      MELT_LOCATION ("warmelt-outobj.melt:810:/ cppif.then");
	      /*^block */
	      /*anyblock */
	      {


		{
		  /*^locexp */
		  /*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
		  melt_dbgcounter++;
#endif
		  ;
		}
		;
		/*^checksignal */
		MELT_CHECK_SIGNAL ();
		;
       /*_#MELT_NEED_DBG__L12*/ meltfnum[10] =
		  /*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
		  ( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
		  0		/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
		  ;;
		MELT_LOCATION ("warmelt-outobj.melt:810:/ cond");
		/*cond */ if ( /*_#MELT_NEED_DBG__L12*/ meltfnum[10])	/*then */
		  {
		    /*^cond.then */
		    /*^block */
		    /*anyblock */
		    {

	 /*_#THE_MELTCALLCOUNT__L13*/ meltfnum[9] =
#ifdef meltcallcount
			meltcallcount	/* the_meltcallcount */
#else
			0L
#endif /* meltcallcount the_meltcallcount */
			;;
		      /*^compute */
	 /*_#STRBUF_USEDLENGTH__L14*/ meltfnum[13] =
			melt_strbuf_usedlength ((melt_ptr_t)
						( /*_.SBUF__V3*/
						 meltfptr[2]));;
		      MELT_LOCATION ("warmelt-outobj.melt:810:/ checksignal");
		      MELT_CHECK_SIGNAL ();
		      ;
		      /*^apply */
		      /*apply */
		      {
			union meltparam_un argtab[7];
			memset (&argtab, 0, sizeof (argtab));
			/*^apply.arg */
			argtab[0].meltbp_long =
			  /*_#THE_MELTCALLCOUNT__L13*/ meltfnum[9];
			/*^apply.arg */
			argtab[1].meltbp_cstring = "warmelt-outobj.melt";
			/*^apply.arg */
			argtab[2].meltbp_long = 810;
			/*^apply.arg */
			argtab[3].meltbp_cstring =
			  "outcinitfill_objinitstring ix=";
			/*^apply.arg */
			argtab[4].meltbp_long = /*_#IX__L9*/ meltfnum[8];
			/*^apply.arg */
			argtab[5].meltbp_cstring = " sbuflen=";
			/*^apply.arg */
			argtab[6].meltbp_long =
			  /*_#STRBUF_USEDLENGTH__L14*/ meltfnum[13];
			/*_.MELT_DEBUG_FUN__V29*/ meltfptr[28] =
			  melt_apply ((meltclosure_ptr_t)
				      (( /*!MELT_DEBUG_FUN */ meltfrout->
					tabval[1])),
				      (melt_ptr_t) (( /*nil */ NULL)),
				      (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				       MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				       MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				       MELTBPARSTR_LONG ""), argtab, "",
				      (union meltparam_un *) 0);
		      }
		      ;
		      /*_.IF___V28*/ meltfptr[27] =
			/*_.MELT_DEBUG_FUN__V29*/ meltfptr[28];;
		      /*epilog */

		      MELT_LOCATION ("warmelt-outobj.melt:810:/ clear");
		   /*clear *//*_#THE_MELTCALLCOUNT__L13*/ meltfnum[9] = 0;
		      /*^clear */
		   /*clear *//*_#STRBUF_USEDLENGTH__L14*/ meltfnum[13] = 0;
		      /*^clear */
		   /*clear *//*_.MELT_DEBUG_FUN__V29*/ meltfptr[28] = 0;
		    }
		    ;
		  }
		else
		  {		/*^cond.else */

	/*_.IF___V28*/ meltfptr[27] = NULL;;
		  }
		;
		MELT_LOCATION ("warmelt-outobj.melt:810:/ quasiblock");


		/*_.PROGN___V30*/ meltfptr[28] = /*_.IF___V28*/ meltfptr[27];;
		/*^compute */
		/*_.IFCPP___V27*/ meltfptr[26] =
		  /*_.PROGN___V30*/ meltfptr[28];;
		/*epilog */

		MELT_LOCATION ("warmelt-outobj.melt:810:/ clear");
		 /*clear *//*_#MELT_NEED_DBG__L12*/ meltfnum[10] = 0;
		/*^clear */
		 /*clear *//*_.IF___V28*/ meltfptr[27] = 0;
		/*^clear */
		 /*clear *//*_.PROGN___V30*/ meltfptr[28] = 0;
	      }

#else /*MELT_HAVE_DEBUG */
	      /*^cppif.else */
	      /*_.IFCPP___V27*/ meltfptr[26] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
	      ;
	      MELT_LOCATION ("warmelt-outobj.melt:812:/ checksignal");
	      MELT_CHECK_SIGNAL ();
	      ;
     /*_#STRBUF_USEDLENGTH__L15*/ meltfnum[9] =
		melt_strbuf_usedlength ((melt_ptr_t)
					( /*_.SBUF__V3*/ meltfptr[2]));;
	      MELT_LOCATION ("warmelt-outobj.melt:812:/ cond");
	      /*cond */ if (
			     /*ifisa */
			     melt_is_instance_of ((melt_ptr_t)
						  (( /*!BUFFER_LIMIT_CONT */
						    meltfrout->tabval[3])),
						  (melt_ptr_t) (( /*!CLASS_REFERENCE */ meltfrout->tabval[4])))
		)		/*then */
		{
		  /*^cond.then */
		  /*^getslot */
		  {
		    melt_ptr_t slot = NULL, obj = NULL;
		    obj =
		      (melt_ptr_t) (( /*!BUFFER_LIMIT_CONT */ meltfrout->
				     tabval[3])) /*=obj*/ ;
		    melt_object_get_field (slot, obj, 0, "REFERENCED_VALUE");
       /*_.REFERENCED_VALUE__V31*/ meltfptr[27] = slot;
		  };
		  ;
		}
	      else
		{		/*^cond.else */

      /*_.REFERENCED_VALUE__V31*/ meltfptr[27] = NULL;;
		}
	      ;
	      /*^compute */
     /*_#GET_INT__L16*/ meltfnum[13] =
		(melt_get_int
		 ((melt_ptr_t) ( /*_.REFERENCED_VALUE__V31*/ meltfptr[27])));;
	      /*^compute */
     /*_#IRAW__L17*/ meltfnum[10] =
		(( /*_#GET_INT__L16*/ meltfnum[13]) / (2));;
	      /*^compute */
     /*_#I__L18*/ meltfnum[17] =
		(( /*_#STRBUF_USEDLENGTH__L15*/ meltfnum[9]) >
		 ( /*_#IRAW__L17*/ meltfnum[10]));;
	      MELT_LOCATION ("warmelt-outobj.melt:812:/ cond");
	      /*cond */ if ( /*_#I__L18*/ meltfnum[17])	/*then */
		{
		  /*^cond.then */
		  /*^block */
		  /*anyblock */
		  {


		    {
		      MELT_LOCATION ("warmelt-outobj.melt:813:/ locexp");

#if MELT_HAVE_DEBUG
		      if (melt_need_debug (0))
			melt_dbgshortbacktrace (("outcinitfill_objinitstring huge sbuf"), (10));
#endif
		      ;
		    }
		    ;

#if MELT_HAVE_DEBUG
		    MELT_LOCATION ("warmelt-outobj.melt:814:/ cppif.then");
		    /*^block */
		    /*anyblock */
		    {


		      {
			/*^locexp */
			/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
			melt_dbgcounter++;
#endif
			;
		      }
		      ;
		      /*^checksignal */
		      MELT_CHECK_SIGNAL ();
		      ;
	 /*_#MELT_NEED_DBG__L19*/ meltfnum[18] =
			/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
			( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
			0	/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
			;;
		      MELT_LOCATION ("warmelt-outobj.melt:814:/ cond");
		      /*cond */ if ( /*_#MELT_NEED_DBG__L19*/ meltfnum[18])	/*then */
			{
			  /*^cond.then */
			  /*^block */
			  /*anyblock */
			  {

	   /*_#THE_MELTCALLCOUNT__L20*/ meltfnum[19] =
#ifdef meltcallcount
			      meltcallcount	/* the_meltcallcount */
#else
			      0L
#endif /* meltcallcount the_meltcallcount */
			      ;;
			    MELT_LOCATION
			      ("warmelt-outobj.melt:814:/ checksignal");
			    MELT_CHECK_SIGNAL ();
			    ;
			    /*^apply */
			    /*apply */
			    {
			      union meltparam_un argtab[5];
			      memset (&argtab, 0, sizeof (argtab));
			      /*^apply.arg */
			      argtab[0].meltbp_long =
				/*_#THE_MELTCALLCOUNT__L20*/ meltfnum[19];
			      /*^apply.arg */
			      argtab[1].meltbp_cstring =
				"warmelt-outobj.melt";
			      /*^apply.arg */
			      argtab[2].meltbp_long = 814;
			      /*^apply.arg */
			      argtab[3].meltbp_cstring =
				"outcinitfill_objinitstring huge sbuf=";
			      /*^apply.arg */
			      argtab[4].meltbp_aptr =
				(melt_ptr_t *) & /*_.SBUF__V3*/ meltfptr[2];
			      /*_.MELT_DEBUG_FUN__V35*/ meltfptr[34] =
				melt_apply ((meltclosure_ptr_t)
					    (( /*!MELT_DEBUG_FUN */
					      meltfrout->tabval[1])),
					    (melt_ptr_t) (( /*nil */ NULL)),
					    (MELTBPARSTR_LONG
					     MELTBPARSTR_CSTRING
					     MELTBPARSTR_LONG
					     MELTBPARSTR_CSTRING
					     MELTBPARSTR_PTR ""), argtab, "",
					    (union meltparam_un *) 0);
			    }
			    ;
			    /*_.IF___V34*/ meltfptr[33] =
			      /*_.MELT_DEBUG_FUN__V35*/ meltfptr[34];;
			    /*epilog */

			    MELT_LOCATION ("warmelt-outobj.melt:814:/ clear");
		     /*clear *//*_#THE_MELTCALLCOUNT__L20*/
			      meltfnum[19] = 0;
			    /*^clear */
		     /*clear *//*_.MELT_DEBUG_FUN__V35*/ meltfptr[34] =
			      0;
			  }
			  ;
			}
		      else
			{	/*^cond.else */

	  /*_.IF___V34*/ meltfptr[33] = NULL;;
			}
		      ;
		      MELT_LOCATION ("warmelt-outobj.melt:814:/ quasiblock");


		      /*_.PROGN___V36*/ meltfptr[34] =
			/*_.IF___V34*/ meltfptr[33];;
		      /*^compute */
		      /*_.IFCPP___V33*/ meltfptr[32] =
			/*_.PROGN___V36*/ meltfptr[34];;
		      /*epilog */

		      MELT_LOCATION ("warmelt-outobj.melt:814:/ clear");
		   /*clear *//*_#MELT_NEED_DBG__L19*/ meltfnum[18] = 0;
		      /*^clear */
		   /*clear *//*_.IF___V34*/ meltfptr[33] = 0;
		      /*^clear */
		   /*clear *//*_.PROGN___V36*/ meltfptr[34] = 0;
		    }

#else /*MELT_HAVE_DEBUG */
		    /*^cppif.else */
		    /*_.IFCPP___V33*/ meltfptr[32] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
		    ;

#if MELT_HAVE_DEBUG
		    MELT_LOCATION ("warmelt-outobj.melt:815:/ cppif.then");
		    /*^block */
		    /*anyblock */
		    {

		      /*^checksignal */
		      MELT_CHECK_SIGNAL ();
		      ;
	 /*_#STRBUF_USEDLENGTH__L21*/ meltfnum[19] =
			melt_strbuf_usedlength ((melt_ptr_t)
						( /*_.SBUF__V3*/
						 meltfptr[2]));;
		      MELT_LOCATION ("warmelt-outobj.melt:816:/ cond");
		      /*cond */ if (
				     /*ifisa */
				     melt_is_instance_of ((melt_ptr_t)
							  (( /*!BUFFER_LIMIT_CONT */ meltfrout->tabval[3])),
							  (melt_ptr_t) (( /*!CLASS_REFERENCE */ meltfrout->tabval[4])))
			)	/*then */
			{
			  /*^cond.then */
			  /*^getslot */
			  {
			    melt_ptr_t slot = NULL, obj = NULL;
			    obj =
			      (melt_ptr_t) (( /*!BUFFER_LIMIT_CONT */
					     meltfrout->tabval[3])) /*=obj*/ ;
			    melt_object_get_field (slot, obj, 0,
						   "REFERENCED_VALUE");
	   /*_.REFERENCED_VALUE__V38*/ meltfptr[34] = slot;
			  };
			  ;
			}
		      else
			{	/*^cond.else */

	  /*_.REFERENCED_VALUE__V38*/ meltfptr[34] = NULL;;
			}
		      ;
		      /*^compute */
	 /*_#GET_INT__L22*/ meltfnum[18] =
			(melt_get_int
			 ((melt_ptr_t)
			  ( /*_.REFERENCED_VALUE__V38*/ meltfptr[34])));;
		      /*^compute */
	 /*_#I__L23*/ meltfnum[22] =
			(( /*_#STRBUF_USEDLENGTH__L21*/ meltfnum[19]) <
			 ( /*_#GET_INT__L22*/ meltfnum[18]));;
		      MELT_LOCATION ("warmelt-outobj.melt:815:/ cond");
		      /*cond */ if ( /*_#I__L23*/ meltfnum[22])	/*then */
			{
			  /*^cond.then */
			  /*_.IFELSE___V39*/ meltfptr[38] = ( /*nil */ NULL);;
			}
		      else
			{
			  MELT_LOCATION
			    ("warmelt-outobj.melt:815:/ cond.else");

			  /*^block */
			  /*anyblock */
			  {




			    {
			      /*^locexp */
			      melt_assert_failed (("check limited sbuf"),
						  ("warmelt-outobj.melt")
						  ? ("warmelt-outobj.melt") :
						  __FILE__,
						  (815) ? (815) : __LINE__,
						  __FUNCTION__);
			      ;
			    }
			    ;
		     /*clear *//*_.IFELSE___V39*/ meltfptr[38] = 0;
			    /*epilog */
			  }
			  ;
			}
		      ;
		      /*^compute */
		      /*_.IFCPP___V37*/ meltfptr[33] =
			/*_.IFELSE___V39*/ meltfptr[38];;
		      /*epilog */

		      MELT_LOCATION ("warmelt-outobj.melt:815:/ clear");
		   /*clear *//*_#STRBUF_USEDLENGTH__L21*/ meltfnum[19] = 0;
		      /*^clear */
		   /*clear *//*_.REFERENCED_VALUE__V38*/ meltfptr[34] = 0;
		      /*^clear */
		   /*clear *//*_#GET_INT__L22*/ meltfnum[18] = 0;
		      /*^clear */
		   /*clear *//*_#I__L23*/ meltfnum[22] = 0;
		      /*^clear */
		   /*clear *//*_.IFELSE___V39*/ meltfptr[38] = 0;
		    }

#else /*MELT_HAVE_DEBUG */
		    /*^cppif.else */
		    /*_.IFCPP___V37*/ meltfptr[33] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
		    ;
		    MELT_LOCATION ("warmelt-outobj.melt:812:/ quasiblock");


		    /*_.PROGN___V40*/ meltfptr[34] =
		      /*_.IFCPP___V37*/ meltfptr[33];;
		    /*^compute */
		    /*_.IFELSE___V32*/ meltfptr[28] =
		      /*_.PROGN___V40*/ meltfptr[34];;
		    /*epilog */

		    MELT_LOCATION ("warmelt-outobj.melt:812:/ clear");
		 /*clear *//*_.IFCPP___V33*/ meltfptr[32] = 0;
		    /*^clear */
		 /*clear *//*_.IFCPP___V37*/ meltfptr[33] = 0;
		    /*^clear */
		 /*clear *//*_.PROGN___V40*/ meltfptr[34] = 0;
		  }
		  ;
		}
	      else
		{		/*^cond.else */

      /*_.IFELSE___V32*/ meltfptr[28] = NULL;;
		}
	      ;
	      MELT_LOCATION ("warmelt-outobj.melt:820:/ checksignal");
	      MELT_CHECK_SIGNAL ();
	      ;
     /*_#I__L24*/ meltfnum[19] =
		(( /*_#IX__L9*/ meltfnum[8]) >=
		 ( /*_#DATALEN__L6*/ meltfnum[1]));;
	      MELT_LOCATION ("warmelt-outobj.melt:820:/ cond");
	      /*cond */ if ( /*_#I__L24*/ meltfnum[19])	/*then */
		{
		  /*^cond.then */
		  /*^block */
		  /*anyblock */
		  {

		    MELT_LOCATION ("warmelt-outobj.melt:821:/ quasiblock");


		    /*^compute */
		    /*_.INISTRLOOP__V26*/ meltfptr[22] =
		      /*_.IFELSE___V41*/ meltfptr[38] = NULL;;

		    /*^exit */
		    /*exit */
		    {
		      goto labexit_INISTRLOOP_1;
		    }
		    ;
		    /*epilog */
		  }
		  ;
		}
	      else
		{
		  MELT_LOCATION ("warmelt-outobj.melt:820:/ cond.else");

		  /*^block */
		  /*anyblock */
		  {

		    MELT_LOCATION ("warmelt-outobj.melt:823:/ checksignal");
		    MELT_CHECK_SIGNAL ();
		    ;
       /*_#I__L25*/ meltfnum[18] =
		      (( /*_#IX__L9*/ meltfnum[8]) + (72));;
		    /*^compute */
       /*_#I__L26*/ meltfnum[22] =
		      (( /*_#I__L25*/ meltfnum[18]) >
		       ( /*_#DATALEN__L6*/ meltfnum[1]));;
		    MELT_LOCATION ("warmelt-outobj.melt:823:/ cond");
		    /*cond */ if ( /*_#I__L26*/ meltfnum[22])	/*then */
		      {
			/*^cond.then */
			/*^block */
			/*anyblock */
			{


			  {
			    MELT_LOCATION
			      ("warmelt-outobj.melt:824:/ locexp");
			    /*add2sbuf_strconst */
			      meltgc_add_strbuf ((melt_ptr_t)
						 ( /*_.SBUF__V3*/
						  meltfptr[2]),
						 ("/*end big inistring*/ strncpy("));
			  }
			  ;

			  {
			    MELT_LOCATION
			      ("warmelt-outobj.melt:825:/ locexp");
			    /*add2sbuf_string */
			      meltgc_add_strbuf ((melt_ptr_t)
						 ( /*_.SBUF__V3*/
						  meltfptr[2]),
						 melt_string_str ((melt_ptr_t)
								  ( /*_.PTRSTR__V4*/ meltfptr[3])));
			  }
			  ;

			  {
			    MELT_LOCATION
			      ("warmelt-outobj.melt:826:/ locexp");
			    /*add2sbuf_strconst */
			      meltgc_add_strbuf ((melt_ptr_t)
						 ( /*_.SBUF__V3*/
						  meltfptr[2]), ("->"));
			  }
			  ;

			  {
			    MELT_LOCATION
			      ("warmelt-outobj.melt:827:/ locexp");
			    /*add2sbuf_string */
			      meltgc_add_strbuf ((melt_ptr_t)
						 ( /*_.SBUF__V3*/
						  meltfptr[2]),
						 melt_string_str ((melt_ptr_t)
								  ( /*_.CNAM__V13*/ meltfptr[8])));
			  }
			  ;

			  {
			    MELT_LOCATION
			      ("warmelt-outobj.melt:828:/ locexp");
			    /*add2sbuf_strconst */
			      meltgc_add_strbuf ((melt_ptr_t)
						 ( /*_.SBUF__V3*/
						  meltfptr[2]), (".val + "));
			  }
			  ;

			  {
			    MELT_LOCATION
			      ("warmelt-outobj.melt:829:/ locexp");
			    meltgc_add_strbuf_dec ((melt_ptr_t)
						   ( /*_.SBUF__V3*/
						    meltfptr[2]),
						   ( /*_#IX__L9*/
						    meltfnum[8]));
			  }
			  ;

			  {
			    MELT_LOCATION
			      ("warmelt-outobj.melt:830:/ locexp");
			    /*add2sbuf_strconst */
			      meltgc_add_strbuf ((melt_ptr_t)
						 ( /*_.SBUF__V3*/
						  meltfptr[2]), (", \""));
			  }
			  ;
	 /*_#I__L27*/ meltfnum[26] =
			    (( /*_#DATALEN__L6*/ meltfnum[1]) -
			     ( /*_#IX__L9*/ meltfnum[8]));;

			  {
			    MELT_LOCATION
			      ("warmelt-outobj.melt:831:/ locexp");
			    meltgc_add_out_csubstr_len ((melt_ptr_t)
							( /*_.SBUF__V3*/
							 meltfptr[2]),
							melt_string_str ((melt_ptr_t) ( /*_.STRDATA__V15*/ meltfptr[14])),
							( /*_#IX__L9*/
							 meltfnum[8]),
							( /*_#I__L27*/
							 meltfnum[26]));
			  }
			  ;

			  {
			    MELT_LOCATION
			      ("warmelt-outobj.melt:832:/ locexp");
			    /*add2sbuf_strconst */
			      meltgc_add_strbuf ((melt_ptr_t)
						 ( /*_.SBUF__V3*/
						  meltfptr[2]),
						 ("\", sizeof ("));
			  }
			  ;

			  {
			    MELT_LOCATION
			      ("warmelt-outobj.melt:833:/ locexp");
			    /*add2sbuf_string */
			      meltgc_add_strbuf ((melt_ptr_t)
						 ( /*_.SBUF__V3*/
						  meltfptr[2]),
						 melt_string_str ((melt_ptr_t)
								  ( /*_.PTRSTR__V4*/ meltfptr[3])));
			  }
			  ;

			  {
			    MELT_LOCATION
			      ("warmelt-outobj.melt:834:/ locexp");
			    /*add2sbuf_strconst */
			      meltgc_add_strbuf ((melt_ptr_t)
						 ( /*_.SBUF__V3*/
						  meltfptr[2]), ("->"));
			  }
			  ;

			  {
			    MELT_LOCATION
			      ("warmelt-outobj.melt:835:/ locexp");
			    /*add2sbuf_string */
			      meltgc_add_strbuf ((melt_ptr_t)
						 ( /*_.SBUF__V3*/
						  meltfptr[2]),
						 melt_string_str ((melt_ptr_t)
								  ( /*_.CNAM__V13*/ meltfptr[8])));
			  }
			  ;

			  {
			    MELT_LOCATION
			      ("warmelt-outobj.melt:836:/ locexp");
			    /*add2sbuf_strconst */
			      meltgc_add_strbuf ((melt_ptr_t)
						 ( /*_.SBUF__V3*/
						  meltfptr[2]), (".val) - "));
			  }
			  ;
	 /*_#I__L28*/ meltfnum[27] =
			    (( /*_#IX__L9*/ meltfnum[8]) + (1));;

			  {
			    MELT_LOCATION
			      ("warmelt-outobj.melt:837:/ locexp");
			    meltgc_add_strbuf_dec ((melt_ptr_t)
						   ( /*_.SBUF__V3*/
						    meltfptr[2]),
						   ( /*_#I__L28*/
						    meltfnum[27]));
			  }
			  ;

			  {
			    MELT_LOCATION
			      ("warmelt-outobj.melt:838:/ locexp");
			    /*add2sbuf_strconst */
			      meltgc_add_strbuf ((melt_ptr_t)
						 ( /*_.SBUF__V3*/
						  meltfptr[2]), (");"));
			  }
			  ;

			  {
			    MELT_LOCATION
			      ("warmelt-outobj.melt:839:/ locexp");
			    meltgc_strbuf_add_indent ((melt_ptr_t)
						      ( /*_.SBUF__V3*/
						       meltfptr[2]), (1), 0);
			  }
			  ;
			  MELT_LOCATION
			    ("warmelt-outobj.melt:840:/ quasiblock");


			  /*^compute */
	 /*_.INISTRLOOP__V26*/ meltfptr[22] = NULL;;

			  /*^exit */
			  /*exit */
			  {
			    goto labexit_INISTRLOOP_1;
			  }
			  ;
			  MELT_LOCATION
			    ("warmelt-outobj.melt:823:/ quasiblock");


			  /*epilog */

			  /*^clear */
		   /*clear *//*_#I__L27*/ meltfnum[26] = 0;
			  /*^clear */
		   /*clear *//*_#I__L28*/ meltfnum[27] = 0;
			}
			;
		      }
		    else
		      {		/*^cond.else */

			/*^block */
			/*anyblock */
			{

			  MELT_LOCATION
			    ("warmelt-outobj.melt:843:/ checksignal");
			  MELT_CHECK_SIGNAL ();
			  ;
	 /*_#I__L29*/ meltfnum[26] =
			    (( /*_#DATALEN__L6*/ meltfnum[1]) - (256));;
			  /*^compute */
	 /*_#I__L30*/ meltfnum[27] =
			    (( /*_#IX__L9*/ meltfnum[8]) <
			     ( /*_#I__L29*/ meltfnum[26]));;
			  MELT_LOCATION ("warmelt-outobj.melt:843:/ cond");
			  /*cond */ if ( /*_#I__L30*/ meltfnum[27])	/*then */
			    {
			      /*^cond.then */
			      /*^block */
			      /*anyblock */
			      {


				{
				  MELT_LOCATION
				    ("warmelt-outobj.melt:844:/ locexp");
				  /*add2sbuf_strconst */
				    meltgc_add_strbuf ((melt_ptr_t)
						       ( /*_.SBUF__V3*/
							meltfptr[2]),
						       ("/*really big chunk inistring*/"));
				}
				;

				{
				  MELT_LOCATION
				    ("warmelt-outobj.melt:845:/ locexp");
				  meltgc_strbuf_add_indent ((melt_ptr_t)
							    ( /*_.SBUF__V3*/
							     meltfptr[2]),
							    (1), 0);
				}
				;

				{
				  MELT_LOCATION
				    ("warmelt-outobj.melt:846:/ locexp");
				  /*add2sbuf_strconst */
				    meltgc_add_strbuf ((melt_ptr_t)
						       ( /*_.SBUF__V3*/
							meltfptr[2]),
						       ("memcpy ("));
				}
				;

				{
				  MELT_LOCATION
				    ("warmelt-outobj.melt:847:/ locexp");
				  /*add2sbuf_string */
				    meltgc_add_strbuf ((melt_ptr_t)
						       ( /*_.SBUF__V3*/
							meltfptr[2]),
						       melt_string_str ((melt_ptr_t) ( /*_.PTRSTR__V4*/ meltfptr[3])));
				}
				;

				{
				  MELT_LOCATION
				    ("warmelt-outobj.melt:848:/ locexp");
				  /*add2sbuf_strconst */
				    meltgc_add_strbuf ((melt_ptr_t)
						       ( /*_.SBUF__V3*/
							meltfptr[2]), ("->"));
				}
				;

				{
				  MELT_LOCATION
				    ("warmelt-outobj.melt:849:/ locexp");
				  /*add2sbuf_string */
				    meltgc_add_strbuf ((melt_ptr_t)
						       ( /*_.SBUF__V3*/
							meltfptr[2]),
						       melt_string_str ((melt_ptr_t) ( /*_.CNAM__V13*/ meltfptr[8])));
				}
				;

				{
				  MELT_LOCATION
				    ("warmelt-outobj.melt:850:/ locexp");
				  /*add2sbuf_strconst */
				    meltgc_add_strbuf ((melt_ptr_t)
						       ( /*_.SBUF__V3*/
							meltfptr[2]),
						       (".val + "));
				}
				;

				{
				  MELT_LOCATION
				    ("warmelt-outobj.melt:851:/ locexp");
				  meltgc_add_strbuf_dec ((melt_ptr_t)
							 ( /*_.SBUF__V3*/
							  meltfptr[2]),
							 ( /*_#IX__L9*/
							  meltfnum[8]));
				}
				;

				{
				  MELT_LOCATION
				    ("warmelt-outobj.melt:852:/ locexp");
				  /*add2sbuf_strconst */
				    meltgc_add_strbuf ((melt_ptr_t)
						       ( /*_.SBUF__V3*/
							meltfptr[2]), (","));
				}
				;

				{
				  MELT_LOCATION
				    ("warmelt-outobj.melt:854:/ locexp");
				  meltgc_strbuf_add_indent ((melt_ptr_t)
							    ( /*_.SBUF__V3*/
							     meltfptr[2]),
							    (8), 0);
				}
				;

				{
				  MELT_LOCATION
				    ("warmelt-outobj.melt:855:/ locexp");
				  /*add2sbuf_strconst */
				    meltgc_add_strbuf ((melt_ptr_t)
						       ( /*_.SBUF__V3*/
							meltfptr[2]), ("\""));
				}
				;

				{
				  MELT_LOCATION
				    ("warmelt-outobj.melt:856:/ locexp");
				  meltgc_add_out_csubstr_len ((melt_ptr_t)
							      ( /*_.SBUF__V3*/
							       meltfptr[2]),
							      melt_string_str
							      ((melt_ptr_t)
							       ( /*_.STRDATA__V15*/ meltfptr[14])),
							      ( /*_#IX__L9*/
							       meltfnum[8]),
							      (64));
				}
				;

				{
				  MELT_LOCATION
				    ("warmelt-outobj.melt:857:/ locexp");
				  /*add2sbuf_strconst */
				    meltgc_add_strbuf ((melt_ptr_t)
						       ( /*_.SBUF__V3*/
							meltfptr[2]), ("\""));
				}
				;

				{
				  MELT_LOCATION
				    ("warmelt-outobj.melt:858:/ locexp");
				  meltgc_strbuf_add_indent ((melt_ptr_t)
							    ( /*_.SBUF__V3*/
							     meltfptr[2]),
							    (8), 0);
				}
				;

				{
				  MELT_LOCATION
				    ("warmelt-outobj.melt:859:/ locexp");
				  /*add2sbuf_strconst */
				    meltgc_add_strbuf ((melt_ptr_t)
						       ( /*_.SBUF__V3*/
							meltfptr[2]), ("\""));
				}
				;
	   /*_#I__L31*/ meltfnum[30] =
				  ((64) + ( /*_#IX__L9*/ meltfnum[8]));;

				{
				  MELT_LOCATION
				    ("warmelt-outobj.melt:860:/ locexp");
				  meltgc_add_out_csubstr_len ((melt_ptr_t)
							      ( /*_.SBUF__V3*/
							       meltfptr[2]),
							      melt_string_str
							      ((melt_ptr_t)
							       ( /*_.STRDATA__V15*/ meltfptr[14])),
							      ( /*_#I__L31*/
							       meltfnum[30]),
							      (64));
				}
				;

				{
				  MELT_LOCATION
				    ("warmelt-outobj.melt:861:/ locexp");
				  /*add2sbuf_strconst */
				    meltgc_add_strbuf ((melt_ptr_t)
						       ( /*_.SBUF__V3*/
							meltfptr[2]), ("\""));
				}
				;

				{
				  MELT_LOCATION
				    ("warmelt-outobj.melt:862:/ locexp");
				  meltgc_strbuf_add_indent ((melt_ptr_t)
							    ( /*_.SBUF__V3*/
							     meltfptr[2]),
							    (8), 0);
				}
				;

				{
				  MELT_LOCATION
				    ("warmelt-outobj.melt:863:/ locexp");
				  /*add2sbuf_strconst */
				    meltgc_add_strbuf ((melt_ptr_t)
						       ( /*_.SBUF__V3*/
							meltfptr[2]), ("\""));
				}
				;
	   /*_#I__L32*/ meltfnum[31] =
				  ((128) + ( /*_#IX__L9*/ meltfnum[8]));;

				{
				  MELT_LOCATION
				    ("warmelt-outobj.melt:864:/ locexp");
				  meltgc_add_out_csubstr_len ((melt_ptr_t)
							      ( /*_.SBUF__V3*/
							       meltfptr[2]),
							      melt_string_str
							      ((melt_ptr_t)
							       ( /*_.STRDATA__V15*/ meltfptr[14])),
							      ( /*_#I__L32*/
							       meltfnum[31]),
							      (64));
				}
				;

				{
				  MELT_LOCATION
				    ("warmelt-outobj.melt:865:/ locexp");
				  /*add2sbuf_strconst */
				    meltgc_add_strbuf ((melt_ptr_t)
						       ( /*_.SBUF__V3*/
							meltfptr[2]), ("\""));
				}
				;

				{
				  MELT_LOCATION
				    ("warmelt-outobj.melt:866:/ locexp");
				  meltgc_strbuf_add_indent ((melt_ptr_t)
							    ( /*_.SBUF__V3*/
							     meltfptr[2]),
							    (8), 0);
				}
				;

				{
				  MELT_LOCATION
				    ("warmelt-outobj.melt:867:/ locexp");
				  /*add2sbuf_strconst */
				    meltgc_add_strbuf ((melt_ptr_t)
						       ( /*_.SBUF__V3*/
							meltfptr[2]), ("\""));
				}
				;
	   /*_#I__L33*/ meltfnum[32] =
				  ((192) + ( /*_#IX__L9*/ meltfnum[8]));;

				{
				  MELT_LOCATION
				    ("warmelt-outobj.melt:868:/ locexp");
				  meltgc_add_out_csubstr_len ((melt_ptr_t)
							      ( /*_.SBUF__V3*/
							       meltfptr[2]),
							      melt_string_str
							      ((melt_ptr_t)
							       ( /*_.STRDATA__V15*/ meltfptr[14])),
							      ( /*_#I__L33*/
							       meltfnum[32]),
							      (64));
				}
				;

				{
				  MELT_LOCATION
				    ("warmelt-outobj.melt:869:/ locexp");
				  /*add2sbuf_strconst */
				    meltgc_add_strbuf ((melt_ptr_t)
						       ( /*_.SBUF__V3*/
							meltfptr[2]),
						       ("\","));
				}
				;

				{
				  MELT_LOCATION
				    ("warmelt-outobj.melt:870:/ locexp");
				  meltgc_strbuf_add_indent ((melt_ptr_t)
							    ( /*_.SBUF__V3*/
							     meltfptr[2]),
							    (8), 0);
				}
				;

				{
				  MELT_LOCATION
				    ("warmelt-outobj.melt:871:/ locexp");
				  /*add2sbuf_strconst */
				    meltgc_add_strbuf ((melt_ptr_t)
						       ( /*_.SBUF__V3*/
							meltfptr[2]),
						       (" /*big*/ 256);"));
				}
				;

				{
				  MELT_LOCATION
				    ("warmelt-outobj.melt:872:/ locexp");
				  meltgc_strbuf_add_indent ((melt_ptr_t)
							    ( /*_.SBUF__V3*/
							     meltfptr[2]),
							    (1), 0);
				}
				;

				{
				  MELT_LOCATION
				    ("warmelt-outobj.melt:874:/ locexp");
			  /*increment *//*_#IX__L9*/ meltfnum[8] +=
				    256;
				  ;
				}
				;

				{
				  MELT_LOCATION
				    ("warmelt-outobj.melt:875:/ locexp");
				  /*void */ (void) 0;
				}
				;
				MELT_LOCATION
				  ("warmelt-outobj.melt:843:/ quasiblock");


				/*epilog */

				/*^clear */
		     /*clear *//*_#I__L31*/ meltfnum[30] = 0;
				/*^clear */
		     /*clear *//*_#I__L32*/ meltfnum[31] = 0;
				/*^clear */
		     /*clear *//*_#I__L33*/ meltfnum[32] = 0;
			      }
			      ;
			    }
			  else
			    {	/*^cond.else */

			      /*^block */
			      /*anyblock */
			      {

				MELT_LOCATION
				  ("warmelt-outobj.melt:878:/ checksignal");
				MELT_CHECK_SIGNAL ();
				;
	   /*_#I__L34*/ meltfnum[30] =
				  (( /*_#DATALEN__L6*/ meltfnum[1]) - (128));;
				/*^compute */
	   /*_#I__L35*/ meltfnum[31] =
				  (( /*_#IX__L9*/ meltfnum[8]) <
				   ( /*_#I__L34*/ meltfnum[30]));;
				MELT_LOCATION
				  ("warmelt-outobj.melt:878:/ cond");
				/*cond */ if ( /*_#I__L35*/ meltfnum[31])	/*then */
				  {
				    /*^cond.then */
				    /*^block */
				    /*anyblock */
				    {


				      {
					MELT_LOCATION
					  ("warmelt-outobj.melt:879:/ locexp");
					/*add2sbuf_strconst */
					  meltgc_add_strbuf ((melt_ptr_t)
							     ( /*_.SBUF__V3*/
							      meltfptr[2]),
							     ("/*quite big chunk inistring*/"));
				      }
				      ;

				      {
					MELT_LOCATION
					  ("warmelt-outobj.melt:880:/ locexp");
					meltgc_strbuf_add_indent ((melt_ptr_t)
								  ( /*_.SBUF__V3*/ meltfptr[2]), (1), 0);
				      }
				      ;

				      {
					MELT_LOCATION
					  ("warmelt-outobj.melt:881:/ locexp");
					/*add2sbuf_strconst */
					  meltgc_add_strbuf ((melt_ptr_t)
							     ( /*_.SBUF__V3*/
							      meltfptr[2]),
							     ("memcpy ("));
				      }
				      ;

				      {
					MELT_LOCATION
					  ("warmelt-outobj.melt:882:/ locexp");
					/*add2sbuf_string */
					  meltgc_add_strbuf ((melt_ptr_t)
							     ( /*_.SBUF__V3*/
							      meltfptr[2]),
							     melt_string_str ((melt_ptr_t) ( /*_.PTRSTR__V4*/ meltfptr[3])));
				      }
				      ;

				      {
					MELT_LOCATION
					  ("warmelt-outobj.melt:883:/ locexp");
					/*add2sbuf_strconst */
					  meltgc_add_strbuf ((melt_ptr_t)
							     ( /*_.SBUF__V3*/
							      meltfptr[2]),
							     ("->"));
				      }
				      ;

				      {
					MELT_LOCATION
					  ("warmelt-outobj.melt:884:/ locexp");
					/*add2sbuf_string */
					  meltgc_add_strbuf ((melt_ptr_t)
							     ( /*_.SBUF__V3*/
							      meltfptr[2]),
							     melt_string_str ((melt_ptr_t) ( /*_.CNAM__V13*/ meltfptr[8])));
				      }
				      ;

				      {
					MELT_LOCATION
					  ("warmelt-outobj.melt:885:/ locexp");
					/*add2sbuf_strconst */
					  meltgc_add_strbuf ((melt_ptr_t)
							     ( /*_.SBUF__V3*/
							      meltfptr[2]),
							     (".val + "));
				      }
				      ;

				      {
					MELT_LOCATION
					  ("warmelt-outobj.melt:886:/ locexp");
					meltgc_add_strbuf_dec ((melt_ptr_t)
							       ( /*_.SBUF__V3*/ meltfptr[2]), ( /*_#IX__L9*/ meltfnum[8]));
				      }
				      ;

				      {
					MELT_LOCATION
					  ("warmelt-outobj.melt:887:/ locexp");
					/*add2sbuf_strconst */
					  meltgc_add_strbuf ((melt_ptr_t)
							     ( /*_.SBUF__V3*/
							      meltfptr[2]),
							     (","));
				      }
				      ;

				      {
					MELT_LOCATION
					  ("warmelt-outobj.melt:889:/ locexp");
					meltgc_strbuf_add_indent ((melt_ptr_t)
								  ( /*_.SBUF__V3*/ meltfptr[2]), (8), 0);
				      }
				      ;

				      {
					MELT_LOCATION
					  ("warmelt-outobj.melt:890:/ locexp");
					/*add2sbuf_strconst */
					  meltgc_add_strbuf ((melt_ptr_t)
							     ( /*_.SBUF__V3*/
							      meltfptr[2]),
							     ("\""));
				      }
				      ;

				      {
					MELT_LOCATION
					  ("warmelt-outobj.melt:891:/ locexp");
					meltgc_add_out_csubstr_len ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
								    melt_string_str
								    ((melt_ptr_t) ( /*_.STRDATA__V15*/ meltfptr[14])),
								    ( /*_#IX__L9*/ meltfnum[8]), (64));
				      }
				      ;

				      {
					MELT_LOCATION
					  ("warmelt-outobj.melt:892:/ locexp");
					/*add2sbuf_strconst */
					  meltgc_add_strbuf ((melt_ptr_t)
							     ( /*_.SBUF__V3*/
							      meltfptr[2]),
							     ("\""));
				      }
				      ;

				      {
					MELT_LOCATION
					  ("warmelt-outobj.melt:893:/ locexp");
					meltgc_strbuf_add_indent ((melt_ptr_t)
								  ( /*_.SBUF__V3*/ meltfptr[2]), (8), 0);
				      }
				      ;

				      {
					MELT_LOCATION
					  ("warmelt-outobj.melt:894:/ locexp");
					/*add2sbuf_strconst */
					  meltgc_add_strbuf ((melt_ptr_t)
							     ( /*_.SBUF__V3*/
							      meltfptr[2]),
							     ("\""));
				      }
				      ;
	     /*_#I__L36*/ meltfnum[32] =
					((64) + ( /*_#IX__L9*/ meltfnum[8]));;

				      {
					MELT_LOCATION
					  ("warmelt-outobj.melt:895:/ locexp");
					meltgc_add_out_csubstr_len ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
								    melt_string_str
								    ((melt_ptr_t) ( /*_.STRDATA__V15*/ meltfptr[14])),
								    ( /*_#I__L36*/ meltfnum[32]), (64));
				      }
				      ;

				      {
					MELT_LOCATION
					  ("warmelt-outobj.melt:896:/ locexp");
					/*add2sbuf_strconst */
					  meltgc_add_strbuf ((melt_ptr_t)
							     ( /*_.SBUF__V3*/
							      meltfptr[2]),
							     ("\","));
				      }
				      ;

				      {
					MELT_LOCATION
					  ("warmelt-outobj.melt:897:/ locexp");
					meltgc_strbuf_add_indent ((melt_ptr_t)
								  ( /*_.SBUF__V3*/ meltfptr[2]), (8), 0);
				      }
				      ;

				      {
					MELT_LOCATION
					  ("warmelt-outobj.melt:898:/ locexp");
					/*add2sbuf_strconst */
					  meltgc_add_strbuf ((melt_ptr_t)
							     ( /*_.SBUF__V3*/
							      meltfptr[2]),
							     (" /*lessbig*/ 128);"));
				      }
				      ;

				      {
					MELT_LOCATION
					  ("warmelt-outobj.melt:899:/ locexp");
					meltgc_strbuf_add_indent ((melt_ptr_t)
								  ( /*_.SBUF__V3*/ meltfptr[2]), (1), 0);
				      }
				      ;

				      {
					MELT_LOCATION
					  ("warmelt-outobj.melt:900:/ locexp");
			    /*increment *//*_#IX__L9*/ meltfnum[8]
					  += 128;
					;
				      }
				      ;

				      {
					MELT_LOCATION
					  ("warmelt-outobj.melt:902:/ locexp");
					/*void */ (void) 0;
				      }
				      ;
				      MELT_LOCATION
					("warmelt-outobj.melt:878:/ quasiblock");


				      /*epilog */

				      /*^clear */
		       /*clear *//*_#I__L36*/ meltfnum[32] = 0;
				    }
				    ;
				  }
				else
				  {	/*^cond.else */

				    /*^block */
				    /*anyblock */
				    {

				      MELT_LOCATION
					("warmelt-outobj.melt:905:/ checksignal");
				      MELT_CHECK_SIGNAL ();
				      ;
	     /*_#I__L37*/ meltfnum[32] =
					(( /*_#DATALEN__L6*/ meltfnum[1]) -
					 (64));;
				      /*^compute */
	     /*_#I__L38*/ meltfnum[37] =
					(( /*_#IX__L9*/ meltfnum[8]) <
					 ( /*_#I__L37*/ meltfnum[32]));;
				      MELT_LOCATION
					("warmelt-outobj.melt:905:/ cond");
				      /*cond */ if ( /*_#I__L38*/ meltfnum[37])	/*then */
					{
					  /*^cond.then */
					  /*^block */
					  /*anyblock */
					  {


					    {
					      MELT_LOCATION
						("warmelt-outobj.melt:906:/ locexp");
					      /*add2sbuf_strconst */
						meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]), ("/*almost big chunk inistring*/"));
					    }
					    ;

					    {
					      MELT_LOCATION
						("warmelt-outobj.melt:907:/ locexp");
					      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]), (1), 0);
					    }
					    ;

					    {
					      MELT_LOCATION
						("warmelt-outobj.melt:908:/ locexp");
					      /*add2sbuf_strconst */
						meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]), ("memcpy ("));
					    }
					    ;

					    {
					      MELT_LOCATION
						("warmelt-outobj.melt:909:/ locexp");
					      /*add2sbuf_string */
						meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
								   melt_string_str
								   ((melt_ptr_t) ( /*_.PTRSTR__V4*/ meltfptr[3])));
					    }
					    ;

					    {
					      MELT_LOCATION
						("warmelt-outobj.melt:910:/ locexp");
					      /*add2sbuf_strconst */
						meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]), ("->"));
					    }
					    ;

					    {
					      MELT_LOCATION
						("warmelt-outobj.melt:911:/ locexp");
					      /*add2sbuf_string */
						meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
								   melt_string_str
								   ((melt_ptr_t) ( /*_.CNAM__V13*/ meltfptr[8])));
					    }
					    ;

					    {
					      MELT_LOCATION
						("warmelt-outobj.melt:912:/ locexp");
					      /*add2sbuf_strconst */
						meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]), (".val + "));
					    }
					    ;

					    {
					      MELT_LOCATION
						("warmelt-outobj.melt:913:/ locexp");
					      meltgc_add_strbuf_dec ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]), ( /*_#IX__L9*/ meltfnum[8]));
					    }
					    ;

					    {
					      MELT_LOCATION
						("warmelt-outobj.melt:914:/ locexp");
					      /*add2sbuf_strconst */
						meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]), (","));
					    }
					    ;

					    {
					      MELT_LOCATION
						("warmelt-outobj.melt:915:/ locexp");
					      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]), (8), 0);
					    }
					    ;

					    {
					      MELT_LOCATION
						("warmelt-outobj.melt:916:/ locexp");
					      /*add2sbuf_strconst */
						meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]), ("\""));
					    }
					    ;

					    {
					      MELT_LOCATION
						("warmelt-outobj.melt:917:/ locexp");
					      meltgc_add_out_csubstr_len ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
									  melt_string_str
									  ((melt_ptr_t) ( /*_.STRDATA__V15*/ meltfptr[14])),
									  ( /*_#IX__L9*/ meltfnum[8]), (64));
					    }
					    ;

					    {
					      MELT_LOCATION
						("warmelt-outobj.melt:918:/ locexp");
					      /*add2sbuf_strconst */
						meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]), ("\","));
					    }
					    ;

					    {
					      MELT_LOCATION
						("warmelt-outobj.melt:919:/ locexp");
					      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]), (8), 0);
					    }
					    ;

					    {
					      MELT_LOCATION
						("warmelt-outobj.melt:920:/ locexp");
					      /*add2sbuf_strconst */
						meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]), (" /*evenlessbig*/ 64);"));
					    }
					    ;

					    {
					      MELT_LOCATION
						("warmelt-outobj.melt:921:/ locexp");
					      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]), (1), 0);
					    }
					    ;

					    {
					      MELT_LOCATION
						("warmelt-outobj.melt:922:/ locexp");
			      /*increment *//*_#IX__L9*/
						meltfnum[8] += 64;
					      ;
					    }
					    ;

					    {
					      MELT_LOCATION
						("warmelt-outobj.melt:924:/ locexp");
					      /*void */ (void) 0;
					    }
					    ;
					    MELT_LOCATION
					      ("warmelt-outobj.melt:905:/ quasiblock");


					    /*epilog */
					  }
					  ;
					}
				      else
					{	/*^cond.else */

	      /*_.IFELSE___V45*/ meltfptr[44] =
					    NULL;;
					}
				      ;
				      /*^compute */
				      /*_.IFELSE___V44*/ meltfptr[34] =
					/*_.IFELSE___V45*/ meltfptr[44];;
				      /*epilog */

				      MELT_LOCATION
					("warmelt-outobj.melt:878:/ clear");
		       /*clear *//*_#I__L37*/ meltfnum[32] = 0;
				      /*^clear */
		       /*clear *//*_#I__L38*/ meltfnum[37] = 0;
				      /*^clear */
		       /*clear *//*_.IFELSE___V45*/
					meltfptr[44] = 0;
				    }
				    ;
				  }
				;
				/*_.IFELSE___V43*/ meltfptr[33] =
				  /*_.IFELSE___V44*/ meltfptr[34];;
				/*epilog */

				MELT_LOCATION
				  ("warmelt-outobj.melt:843:/ clear");
		     /*clear *//*_#I__L34*/ meltfnum[30] = 0;
				/*^clear */
		     /*clear *//*_#I__L35*/ meltfnum[31] = 0;
				/*^clear */
		     /*clear *//*_.IFELSE___V44*/ meltfptr[34] = 0;
			      }
			      ;
			    }
			  ;
			  /*_.IFELSE___V42*/ meltfptr[32] =
			    /*_.IFELSE___V43*/ meltfptr[33];;
			  /*epilog */

			  MELT_LOCATION ("warmelt-outobj.melt:823:/ clear");
		   /*clear *//*_#I__L29*/ meltfnum[26] = 0;
			  /*^clear */
		   /*clear *//*_#I__L30*/ meltfnum[27] = 0;
			  /*^clear */
		   /*clear *//*_.IFELSE___V43*/ meltfptr[33] = 0;
			}
			;
		      }
		    ;
		    /*_.IFELSE___V41*/ meltfptr[38] =
		      /*_.IFELSE___V42*/ meltfptr[32];;
		    /*epilog */

		    MELT_LOCATION ("warmelt-outobj.melt:820:/ clear");
		 /*clear *//*_#I__L25*/ meltfnum[18] = 0;
		    /*^clear */
		 /*clear *//*_#I__L26*/ meltfnum[22] = 0;
		    /*^clear */
		 /*clear *//*_.IFELSE___V42*/ meltfptr[32] = 0;
		  }
		  ;
		}
	      ;

	      {
		MELT_LOCATION ("warmelt-outobj.melt:927:/ locexp");
		/*void */ (void) 0;
	      }
	      ;
	      MELT_LOCATION ("warmelt-outobj.melt:808:/ checksignal");
	      MELT_CHECK_SIGNAL ();
	      ;
	      /*epilog */

	      /*^clear */
	       /*clear *//*_.IFCPP___V27*/ meltfptr[26] = 0;
	      /*^clear */
	       /*clear *//*_#STRBUF_USEDLENGTH__L15*/ meltfnum[9] = 0;
	      /*^clear */
	       /*clear *//*_.REFERENCED_VALUE__V31*/ meltfptr[27] = 0;
	      /*^clear */
	       /*clear *//*_#GET_INT__L16*/ meltfnum[13] = 0;
	      /*^clear */
	       /*clear *//*_#IRAW__L17*/ meltfnum[10] = 0;
	      /*^clear */
	       /*clear *//*_#I__L18*/ meltfnum[17] = 0;
	      /*^clear */
	       /*clear *//*_.IFELSE___V32*/ meltfptr[28] = 0;
	      /*^clear */
	       /*clear *//*_#I__L24*/ meltfnum[19] = 0;
	      /*^clear */
	       /*clear *//*_.IFELSE___V41*/ meltfptr[38] = 0;
	    }
	    ;
	    ;
	    goto labloop_INISTRLOOP_1;
	  labexit_INISTRLOOP_1:;
				/*^loopepilog */
	    /*loopepilog */
	    /*_.FOREVER___V25*/ meltfptr[21] =
	      /*_.INISTRLOOP__V26*/ meltfptr[22];;
	  }
	  ;
	  MELT_LOCATION ("warmelt-outobj.melt:932:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
   /*_#STRBUF_USEDLENGTH__L39*/ meltfnum[32] =
	    melt_strbuf_usedlength ((melt_ptr_t)
				    ( /*_.SBUF__V3*/ meltfptr[2]));;
	  MELT_LOCATION ("warmelt-outobj.melt:932:/ cond");
	  /*cond */ if (
			 /*ifisa */
			 melt_is_instance_of ((melt_ptr_t)
					      (( /*!BUFFER_LIMIT_CONT */
						meltfrout->tabval[3])),
					      (melt_ptr_t) (( /*!CLASS_REFERENCE */ meltfrout->tabval[4])))
	    )			/*then */
	    {
	      /*^cond.then */
	      /*^getslot */
	      {
		melt_ptr_t slot = NULL, obj = NULL;
		obj =
		  (melt_ptr_t) (( /*!BUFFER_LIMIT_CONT */ meltfrout->
				 tabval[3])) /*=obj*/ ;
		melt_object_get_field (slot, obj, 0, "REFERENCED_VALUE");
     /*_.REFERENCED_VALUE__V46*/ meltfptr[44] = slot;
	      };
	      ;
	    }
	  else
	    {			/*^cond.else */

    /*_.REFERENCED_VALUE__V46*/ meltfptr[44] = NULL;;
	    }
	  ;
	  /*^compute */
   /*_#GET_INT__L40*/ meltfnum[37] =
	    (melt_get_int
	     ((melt_ptr_t) ( /*_.REFERENCED_VALUE__V46*/ meltfptr[44])));;
	  /*^compute */
   /*_#IRAW__L41*/ meltfnum[30] =
	    (( /*_#GET_INT__L40*/ meltfnum[37]) / (2));;
	  /*^compute */
   /*_#I__L42*/ meltfnum[31] =
	    (( /*_#STRBUF_USEDLENGTH__L39*/ meltfnum[32]) >
	     ( /*_#IRAW__L41*/ meltfnum[30]));;
	  MELT_LOCATION ("warmelt-outobj.melt:932:/ cond");
	  /*cond */ if ( /*_#I__L42*/ meltfnum[31])	/*then */
	    {
	      /*^cond.then */
	      /*^block */
	      /*anyblock */
	      {


		{
		  MELT_LOCATION ("warmelt-outobj.melt:933:/ locexp");

#if MELT_HAVE_DEBUG
		  if (melt_need_debug (0))
		    melt_dbgshortbacktrace (("outcinitfill_objinitstring huge sbuf"), (10));
#endif
		  ;
		}
		;

#if MELT_HAVE_DEBUG
		MELT_LOCATION ("warmelt-outobj.melt:934:/ cppif.then");
		/*^block */
		/*anyblock */
		{


		  {
		    /*^locexp */
		    /*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
		    melt_dbgcounter++;
#endif
		    ;
		  }
		  ;
		  /*^checksignal */
		  MELT_CHECK_SIGNAL ();
		  ;
       /*_#MELT_NEED_DBG__L43*/ meltfnum[26] =
		    /*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
		    ( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
		    0		/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
		    ;;
		  MELT_LOCATION ("warmelt-outobj.melt:934:/ cond");
		  /*cond */ if ( /*_#MELT_NEED_DBG__L43*/ meltfnum[26])	/*then */
		    {
		      /*^cond.then */
		      /*^block */
		      /*anyblock */
		      {

	 /*_#THE_MELTCALLCOUNT__L44*/ meltfnum[27] =
#ifdef meltcallcount
			  meltcallcount	/* the_meltcallcount */
#else
			  0L
#endif /* meltcallcount the_meltcallcount */
			  ;;
			MELT_LOCATION
			  ("warmelt-outobj.melt:934:/ checksignal");
			MELT_CHECK_SIGNAL ();
			;
			/*^apply */
			/*apply */
			{
			  union meltparam_un argtab[5];
			  memset (&argtab, 0, sizeof (argtab));
			  /*^apply.arg */
			  argtab[0].meltbp_long =
			    /*_#THE_MELTCALLCOUNT__L44*/ meltfnum[27];
			  /*^apply.arg */
			  argtab[1].meltbp_cstring = "warmelt-outobj.melt";
			  /*^apply.arg */
			  argtab[2].meltbp_long = 934;
			  /*^apply.arg */
			  argtab[3].meltbp_cstring =
			    "outcinitfill_objinitstring huge declbuf=";
			  /*^apply.arg */
			  argtab[4].meltbp_aptr =
			    (melt_ptr_t *) & /*_.SBUF__V3*/ meltfptr[2];
			  /*_.MELT_DEBUG_FUN__V50*/ meltfptr[26] =
			    melt_apply ((meltclosure_ptr_t)
					(( /*!MELT_DEBUG_FUN */ meltfrout->
					  tabval[1])),
					(melt_ptr_t) (( /*nil */ NULL)),
					(MELTBPARSTR_LONG MELTBPARSTR_CSTRING
					 MELTBPARSTR_LONG MELTBPARSTR_CSTRING
					 MELTBPARSTR_PTR ""), argtab, "",
					(union meltparam_un *) 0);
			}
			;
			/*_.IF___V49*/ meltfptr[32] =
			  /*_.MELT_DEBUG_FUN__V50*/ meltfptr[26];;
			/*epilog */

			MELT_LOCATION ("warmelt-outobj.melt:934:/ clear");
		   /*clear *//*_#THE_MELTCALLCOUNT__L44*/ meltfnum[27] =
			  0;
			/*^clear */
		   /*clear *//*_.MELT_DEBUG_FUN__V50*/ meltfptr[26] = 0;
		      }
		      ;
		    }
		  else
		    {		/*^cond.else */

	/*_.IF___V49*/ meltfptr[32] = NULL;;
		    }
		  ;
		  MELT_LOCATION ("warmelt-outobj.melt:934:/ quasiblock");


		  /*_.PROGN___V51*/ meltfptr[27] =
		    /*_.IF___V49*/ meltfptr[32];;
		  /*^compute */
		  /*_.IFCPP___V48*/ meltfptr[33] =
		    /*_.PROGN___V51*/ meltfptr[27];;
		  /*epilog */

		  MELT_LOCATION ("warmelt-outobj.melt:934:/ clear");
		 /*clear *//*_#MELT_NEED_DBG__L43*/ meltfnum[26] = 0;
		  /*^clear */
		 /*clear *//*_.IF___V49*/ meltfptr[32] = 0;
		  /*^clear */
		 /*clear *//*_.PROGN___V51*/ meltfptr[27] = 0;
		}

#else /*MELT_HAVE_DEBUG */
		/*^cppif.else */
		/*_.IFCPP___V48*/ meltfptr[33] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
		;

#if MELT_HAVE_DEBUG
		MELT_LOCATION ("warmelt-outobj.melt:935:/ cppif.then");
		/*^block */
		/*anyblock */
		{

		  /*^checksignal */
		  MELT_CHECK_SIGNAL ();
		  ;
       /*_#STRBUF_USEDLENGTH__L45*/ meltfnum[18] =
		    melt_strbuf_usedlength ((melt_ptr_t)
					    ( /*_.SBUF__V3*/ meltfptr[2]));;
		  MELT_LOCATION ("warmelt-outobj.melt:936:/ cond");
		  /*cond */ if (
				 /*ifisa */
				 melt_is_instance_of ((melt_ptr_t)
						      (( /*!BUFFER_LIMIT_CONT */ meltfrout->tabval[3])),
						      (melt_ptr_t) (( /*!CLASS_REFERENCE */ meltfrout->tabval[4])))
		    )		/*then */
		    {
		      /*^cond.then */
		      /*^getslot */
		      {
			melt_ptr_t slot = NULL, obj = NULL;
			obj =
			  (melt_ptr_t) (( /*!BUFFER_LIMIT_CONT */ meltfrout->
					 tabval[3])) /*=obj*/ ;
			melt_object_get_field (slot, obj, 0,
					       "REFERENCED_VALUE");
	 /*_.REFERENCED_VALUE__V53*/ meltfptr[38] = slot;
		      };
		      ;
		    }
		  else
		    {		/*^cond.else */

	/*_.REFERENCED_VALUE__V53*/ meltfptr[38] = NULL;;
		    }
		  ;
		  /*^compute */
       /*_#GET_INT__L46*/ meltfnum[22] =
		    (melt_get_int
		     ((melt_ptr_t)
		      ( /*_.REFERENCED_VALUE__V53*/ meltfptr[38])));;
		  /*^compute */
       /*_#I__L47*/ meltfnum[9] =
		    (( /*_#STRBUF_USEDLENGTH__L45*/ meltfnum[18]) <
		     ( /*_#GET_INT__L46*/ meltfnum[22]));;
		  MELT_LOCATION ("warmelt-outobj.melt:935:/ cond");
		  /*cond */ if ( /*_#I__L47*/ meltfnum[9])	/*then */
		    {
		      /*^cond.then */
		      /*_.IFELSE___V54*/ meltfptr[26] = ( /*nil */ NULL);;
		    }
		  else
		    {
		      MELT_LOCATION ("warmelt-outobj.melt:935:/ cond.else");

		      /*^block */
		      /*anyblock */
		      {




			{
			  /*^locexp */
			  melt_assert_failed (("check limited sbuf"),
					      ("warmelt-outobj.melt")
					      ? ("warmelt-outobj.melt") :
					      __FILE__,
					      (935) ? (935) : __LINE__,
					      __FUNCTION__);
			  ;
			}
			;
		   /*clear *//*_.IFELSE___V54*/ meltfptr[26] = 0;
			/*epilog */
		      }
		      ;
		    }
		  ;
		  /*^compute */
		  /*_.IFCPP___V52*/ meltfptr[28] =
		    /*_.IFELSE___V54*/ meltfptr[26];;
		  /*epilog */

		  MELT_LOCATION ("warmelt-outobj.melt:935:/ clear");
		 /*clear *//*_#STRBUF_USEDLENGTH__L45*/ meltfnum[18] = 0;
		  /*^clear */
		 /*clear *//*_.REFERENCED_VALUE__V53*/ meltfptr[38] = 0;
		  /*^clear */
		 /*clear *//*_#GET_INT__L46*/ meltfnum[22] = 0;
		  /*^clear */
		 /*clear *//*_#I__L47*/ meltfnum[9] = 0;
		  /*^clear */
		 /*clear *//*_.IFELSE___V54*/ meltfptr[26] = 0;
		}

#else /*MELT_HAVE_DEBUG */
		/*^cppif.else */
		/*_.IFCPP___V52*/ meltfptr[28] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
		;
		MELT_LOCATION ("warmelt-outobj.melt:932:/ quasiblock");


		/*_.PROGN___V55*/ meltfptr[32] =
		  /*_.IFCPP___V52*/ meltfptr[28];;
		/*^compute */
		/*_.IFELSE___V47*/ meltfptr[34] =
		  /*_.PROGN___V55*/ meltfptr[32];;
		/*epilog */

		MELT_LOCATION ("warmelt-outobj.melt:932:/ clear");
	       /*clear *//*_.IFCPP___V48*/ meltfptr[33] = 0;
		/*^clear */
	       /*clear *//*_.IFCPP___V52*/ meltfptr[28] = 0;
		/*^clear */
	       /*clear *//*_.PROGN___V55*/ meltfptr[32] = 0;
	      }
	      ;
	    }
	  else
	    {			/*^cond.else */

    /*_.IFELSE___V47*/ meltfptr[34] = NULL;;
	    }
	  ;
	  /*^compute */
	  /*_.LET___V20*/ meltfptr[19] = /*_.IFELSE___V47*/ meltfptr[34];;

	  MELT_LOCATION ("warmelt-outobj.melt:803:/ clear");
	     /*clear *//*_#IX__L9*/ meltfnum[8] = 0;
	  /*^clear */
	     /*clear *//*_.IFCPP___V21*/ meltfptr[20] = 0;
	  /*^clear */
	     /*clear *//*_.FOREVER___V25*/ meltfptr[21] = 0;
	  /*^clear */
	     /*clear *//*_#STRBUF_USEDLENGTH__L39*/ meltfnum[32] = 0;
	  /*^clear */
	     /*clear *//*_.REFERENCED_VALUE__V46*/ meltfptr[44] = 0;
	  /*^clear */
	     /*clear *//*_#GET_INT__L40*/ meltfnum[37] = 0;
	  /*^clear */
	     /*clear *//*_#IRAW__L41*/ meltfnum[30] = 0;
	  /*^clear */
	     /*clear *//*_#I__L42*/ meltfnum[31] = 0;
	  /*^clear */
	     /*clear *//*_.IFELSE___V47*/ meltfptr[34] = 0;
	  /*_.IFELSE___V19*/ meltfptr[18] = /*_.LET___V20*/ meltfptr[19];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-outobj.melt:783:/ clear");
	     /*clear *//*_.LET___V20*/ meltfptr[19] = 0;
	}
	;
      }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:938:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
				(1), 0);
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:939:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[6];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.PTRSTR__V4*/ meltfptr[3];
      /*^apply.arg */
      argtab[1].meltbp_cstring = "->";
      /*^apply.arg */
      argtab[2].meltbp_aptr = (melt_ptr_t *) & /*_.CNAM__V13*/ meltfptr[8];
      /*^apply.arg */
      argtab[3].meltbp_cstring = ".val[";
      /*^apply.arg */
      argtab[4].meltbp_long = /*_#DATALEN__L6*/ meltfnum[1];
      /*^apply.arg */
      argtab[5].meltbp_cstring = "] = (char)0; ";
      /*_.ADD2OUT__V56*/ meltfptr[27] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!ADD2OUT */ meltfrout->tabval[5])),
		    (melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
		    (MELTBPARSTR_PTR MELTBPARSTR_CSTRING MELTBPARSTR_PTR
		     MELTBPARSTR_CSTRING MELTBPARSTR_LONG MELTBPARSTR_CSTRING
		     ""), argtab, "", (union meltparam_un *) 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:941:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
				(1), 0);
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:942:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[6];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.PTRSTR__V4*/ meltfptr[3];
      /*^apply.arg */
      argtab[1].meltbp_cstring = "->";
      /*^apply.arg */
      argtab[2].meltbp_aptr = (melt_ptr_t *) & /*_.CNAM__V13*/ meltfptr[8];
      /*^apply.arg */
      argtab[3].meltbp_cstring = ".slen = ";
      /*^apply.arg */
      argtab[4].meltbp_long = /*_#DATALEN__L6*/ meltfnum[1];
      /*^apply.arg */
      argtab[5].meltbp_cstring = "; ";
      /*_.ADD2OUT__V57*/ meltfptr[38] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!ADD2OUT */ meltfrout->tabval[5])),
		    (melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
		    (MELTBPARSTR_PTR MELTBPARSTR_CSTRING MELTBPARSTR_PTR
		     MELTBPARSTR_CSTRING MELTBPARSTR_LONG MELTBPARSTR_CSTRING
		     ""), argtab, "", (union meltparam_un *) 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:944:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
				(1), 0);
    }
    ;

    MELT_LOCATION ("warmelt-outobj.melt:754:/ clear");
	   /*clear *//*_.CNAM__V13*/ meltfptr[8] = 0;
    /*^clear */
	   /*clear *//*_.OLOCVAR__V14*/ meltfptr[13] = 0;
    /*^clear */
	   /*clear *//*_.STRDATA__V15*/ meltfptr[14] = 0;
    /*^clear */
	   /*clear *//*_#DATALEN__L6*/ meltfnum[1] = 0;
    /*^clear */
	   /*clear *//*_#I__L7*/ meltfnum[3] = 0;
    /*^clear */
	   /*clear *//*_.OIE_DISCR__V17*/ meltfptr[15] = 0;
    /*^clear */
	   /*clear *//*_.OUTPUT_C_CODE__V18*/ meltfptr[17] = 0;
    /*^clear */
	   /*clear *//*_#I__L8*/ meltfnum[7] = 0;
    /*^clear */
	   /*clear *//*_.IFELSE___V19*/ meltfptr[18] = 0;
    /*^clear */
	   /*clear *//*_.ADD2OUT__V56*/ meltfptr[27] = 0;
    /*^clear */
	   /*clear *//*_.ADD2OUT__V57*/ meltfptr[38] = 0;
    MELT_LOCATION ("warmelt-outobj.melt:750:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V5*/ meltfptr[4] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V7*/ meltfptr[5] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V11*/ meltfptr[7] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("OUTCINITFILL_OBJINITSTRING", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_22_warmelt_outobj_OUTCINITFILL_OBJINITSTRING_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*noretval */ NULL);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_22_warmelt_outobj_OUTCINITFILL_OBJINITSTRING */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_23_warmelt_outobj_OUTDECLINIT_OBJINITBOXEDINTEGER (meltclosure_ptr_t
							    meltclosp_,
							    melt_ptr_t
							    meltfirstargp_,
							    const
							    melt_argdescr_cell_t
							    meltxargdescr_[],
							    union meltparam_un
							    * meltxargtab_,
							    const
							    melt_argdescr_cell_t
							    meltxresdescr_[],
							    union meltparam_un
							    * meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_23_warmelt_outobj_OUTDECLINIT_OBJINITBOXEDINTEGER_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_23_warmelt_outobj_OUTDECLINIT_OBJINITBOXEDINTEGER_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 4
    melt_ptr_t mcfr_varptr[4];
/*no varnum*/
#define MELTFRAM_NBVARNUM /*none*/0
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_23_warmelt_outobj_OUTDECLINIT_OBJINITBOXEDINTEGER is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct
	 frame_meltrout_23_warmelt_outobj_OUTDECLINIT_OBJINITBOXEDINTEGER_st
	 *) meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 4; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_23_warmelt_outobj_OUTDECLINIT_OBJINITBOXEDINTEGER nbval 4*/
  meltfram__.mcfr_nbvar = 4 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("OUTDECLINIT_OBJINITBOXEDINTEGER", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-outobj.melt:950:/ getarg");
 /*_.RECV__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.SBUF__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2])) !=
	      NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


    {
      MELT_LOCATION ("warmelt-outobj.melt:951:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
			   (" struct meltint_st "));
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:952:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.RECV__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 1, "NAMED_NAME");
  /*_.NAMED_NAME__V4*/ meltfptr[3] = slot;
    };
    ;

    {
      /*^locexp */
      /*add2sbuf_string */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
			   melt_string_str ((melt_ptr_t)
					    ( /*_.NAMED_NAME__V4*/
					     meltfptr[3])));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:953:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]), (";"));
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:950:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*epilog */

    /*^clear */
	   /*clear *//*_.NAMED_NAME__V4*/ meltfptr[3] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("OUTDECLINIT_OBJINITBOXEDINTEGER", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_23_warmelt_outobj_OUTDECLINIT_OBJINITBOXEDINTEGER_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*noretval */ NULL);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_23_warmelt_outobj_OUTDECLINIT_OBJINITBOXEDINTEGER */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_24_warmelt_outobj_OUTCINITFILL_OBJINITBOXEDINTEGER (meltclosure_ptr_t
							     meltclosp_,
							     melt_ptr_t
							     meltfirstargp_,
							     const
							     melt_argdescr_cell_t
							     meltxargdescr_[],
							     union
							     meltparam_un *
							     meltxargtab_,
							     const
							     melt_argdescr_cell_t
							     meltxresdescr_[],
							     union
							     meltparam_un *
							     meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_24_warmelt_outobj_OUTCINITFILL_OBJINITBOXEDINTEGER_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_24_warmelt_outobj_OUTCINITFILL_OBJINITBOXEDINTEGER_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 29
    melt_ptr_t mcfr_varptr[29];
#define MELTFRAM_NBVARNUM 16
    long mcfr_varnum[16];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_24_warmelt_outobj_OUTCINITFILL_OBJINITBOXEDINTEGER is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct
	 frame_meltrout_24_warmelt_outobj_OUTCINITFILL_OBJINITBOXEDINTEGER_st
	 *) meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 29; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_24_warmelt_outobj_OUTCINITFILL_OBJINITBOXEDINTEGER nbval 29*/
  meltfram__.mcfr_nbvar = 29 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("OUTCINITFILL_OBJINITBOXEDINTEGER", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-outobj.melt:958:/ getarg");
 /*_.RECV__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.SBUF__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2])) !=
	      NULL);


  /*getarg#2 */
  /*^getarg */
  if (meltxargdescr_[1] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.PTRSTR__V4*/ meltfptr[3] =
    (meltxargtab_[1].meltbp_aptr) ? (*(meltxargtab_[1].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.PTRSTR__V4*/ meltfptr[3])) !=
	      NULL);


  /*getarg#3 */
  /*^getarg */
  if (meltxargdescr_[2] != MELTBPAR_LONG)
    goto lab_endgetargs;
 /*_#DEPTH__L1*/ meltfnum[0] = meltxargtab_[2].meltbp_long;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:959:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L2*/ meltfnum[1] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.RECV__V2*/ meltfptr[1]),
			     (melt_ptr_t) (( /*!CLASS_OBJINITBOXINTEGER */
					    meltfrout->tabval[0])));;
      MELT_LOCATION ("warmelt-outobj.melt:959:/ cond");
      /*cond */ if ( /*_#IS_A__L2*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V6*/ meltfptr[5] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-outobj.melt:959:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("outcinitfill_objinitboxedinteger check recv"), ("warmelt-outobj.melt") ? ("warmelt-outobj.melt") : __FILE__, (959) ? (959) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V6*/ meltfptr[5] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V5*/ meltfptr[4] = /*_.IFELSE___V6*/ meltfptr[5];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:959:/ clear");
	     /*clear *//*_#IS_A__L2*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V6*/ meltfptr[5] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V5*/ meltfptr[4] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:960:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L3*/ meltfnum[1] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-outobj.melt:960:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L3*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L4*/ meltfnum[3] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-outobj.melt:960:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[7];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L4*/ meltfnum[3];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-outobj.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 960;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring =
		"outcinitfill_objinitboxedinteger recv=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.RECV__V2*/ meltfptr[1];
	      /*^apply.arg */
	      argtab[5].meltbp_cstring = " ptrstr=";
	      /*^apply.arg */
	      argtab[6].meltbp_aptr =
		(melt_ptr_t *) & /*_.PTRSTR__V4*/ meltfptr[3];
	      /*_.MELT_DEBUG_FUN__V9*/ meltfptr[8] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[1])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V8*/ meltfptr[7] = /*_.MELT_DEBUG_FUN__V9*/ meltfptr[8];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-outobj.melt:960:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L4*/ meltfnum[3] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V9*/ meltfptr[8] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V8*/ meltfptr[7] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-outobj.melt:960:/ quasiblock");


      /*_.PROGN___V10*/ meltfptr[8] = /*_.IF___V8*/ meltfptr[7];;
      /*^compute */
      /*_.IFCPP___V7*/ meltfptr[5] = /*_.PROGN___V10*/ meltfptr[8];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:960:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L3*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IF___V8*/ meltfptr[7] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V10*/ meltfptr[8] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V7*/ meltfptr[5] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:961:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_STRING__L5*/ meltfnum[3] =
	(melt_magic_discr ((melt_ptr_t) ( /*_.PTRSTR__V4*/ meltfptr[3])) ==
	 MELTOBMAG_STRING);;
      MELT_LOCATION ("warmelt-outobj.melt:961:/ cond");
      /*cond */ if ( /*_#IS_STRING__L5*/ meltfnum[3])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V12*/ meltfptr[8] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-outobj.melt:961:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("outcinitfill_objinitboxedinteger check ptrstr"), ("warmelt-outobj.melt") ? ("warmelt-outobj.melt") : __FILE__, (961) ? (961) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V12*/ meltfptr[8] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V11*/ meltfptr[7] = /*_.IFELSE___V12*/ meltfptr[8];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:961:/ clear");
	     /*clear *//*_#IS_STRING__L5*/ meltfnum[3] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V12*/ meltfptr[8] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V11*/ meltfptr[7] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-outobj.melt:962:/ quasiblock");


    /*^getslot */
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.RECV__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 1, "OIE_CNAME");
  /*_.CNAM__V14*/ meltfptr[13] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-outobj.melt:963:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.RECV__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 4, "OIE_LOCVAR");
  /*_.OLOCVAR__V15*/ meltfptr[14] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-outobj.melt:964:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.RECV__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 2, "OIE_DATA");
  /*_.ODATA__V16*/ meltfptr[15] = slot;
    };
    ;
 /*_#DEPTHP1__L6*/ meltfnum[1] =
      ((1) + ( /*_#DEPTH__L1*/ meltfnum[0]));;

    {
      MELT_LOCATION ("warmelt-outobj.melt:967:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
			   ("/*iniboxint "));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:968:/ locexp");
      /*add2sbuf_string */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
			   melt_string_str ((melt_ptr_t)
					    ( /*_.CNAM__V14*/ meltfptr[13])));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:969:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
			   ("*/"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:970:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
				( /*_#DEPTHP1__L6*/ meltfnum[1]), 0);
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:971:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^cond */
    /*cond */ if ( /*_.OLOCVAR__V15*/ meltfptr[14])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-outobj.melt:973:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^msend */
	  /*msend */
	  {
	    union meltparam_un argtab[3];
	    memset (&argtab, 0, sizeof (argtab));
	    /*^ojbmsend.arg */
	    argtab[0].meltbp_aptr = /*nil */ (melt_ptr_t *) NULL;
	    /*^ojbmsend.arg */
	    argtab[1].meltbp_aptr =
	      (melt_ptr_t *) & /*_.SBUF__V3*/ meltfptr[2];
	    /*^ojbmsend.arg */
	    argtab[2].meltbp_long = /*_#DEPTHP1__L6*/ meltfnum[1];
	    /*_.OUTPUT_C_CODE__V17*/ meltfptr[16] =
	      meltgc_send ((melt_ptr_t) ( /*_.OLOCVAR__V15*/ meltfptr[14]),
			   (melt_ptr_t) (( /*!OUTPUT_C_CODE */ meltfrout->
					  tabval[2])),
			   (MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_LONG
			    ""), argtab, "", (union meltparam_un *) 0);
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:974:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
				 (" = (melt_ptr_t) &"));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:975:/ locexp");
	    /*add2sbuf_string */
	      meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
				 melt_string_str ((melt_ptr_t)
						  ( /*_.PTRSTR__V4*/
						   meltfptr[3])));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:976:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
				 ("->"));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:977:/ locexp");
	    /*add2sbuf_string */
	      meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
				 melt_string_str ((melt_ptr_t)
						  ( /*_.CNAM__V14*/
						   meltfptr[13])));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:978:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
				 (";"));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:979:/ locexp");
	    meltgc_strbuf_add_indent ((melt_ptr_t)
				      ( /*_.SBUF__V3*/ meltfptr[2]),
				      ( /*_#DEPTHP1__L6*/ meltfnum[1]), 0);
	  }
	  ;
	  MELT_LOCATION ("warmelt-outobj.melt:972:/ quasiblock");


	  /*epilog */

	  MELT_LOCATION ("warmelt-outobj.melt:971:/ clear");
	     /*clear *//*_.OUTPUT_C_CODE__V17*/ meltfptr[16] = 0;
	}
	;
      }				/*noelse */
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:981:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]), (" "));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:982:/ locexp");
      /*add2sbuf_string */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
			   melt_string_str ((melt_ptr_t)
					    ( /*_.PTRSTR__V4*/ meltfptr[3])));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:983:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
			   ("->"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:984:/ locexp");
      /*add2sbuf_string */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
			   melt_string_str ((melt_ptr_t)
					    ( /*_.CNAM__V14*/ meltfptr[13])));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:985:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
			   (".discr = (meltobject_ptr_t)("));
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:986:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.RECV__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 3, "OIE_DISCR");
  /*_.OIE_DISCR__V18*/ meltfptr[16] = slot;
    };
    ;
    /*^checksignal */
    MELT_CHECK_SIGNAL ();
    ;
    /*^msend */
    /*msend */
    {
      union meltparam_un argtab[3];
      memset (&argtab, 0, sizeof (argtab));
      /*^ojbmsend.arg */
      argtab[0].meltbp_aptr = /*nil */ (melt_ptr_t *) NULL;
      /*^ojbmsend.arg */
      argtab[1].meltbp_aptr = (melt_ptr_t *) & /*_.SBUF__V3*/ meltfptr[2];
      /*^ojbmsend.arg */
      argtab[2].meltbp_long = /*_#DEPTHP1__L6*/ meltfnum[1];
      /*_.OUTPUT_C_CODE__V19*/ meltfptr[18] =
	meltgc_send ((melt_ptr_t) ( /*_.OIE_DISCR__V18*/ meltfptr[16]),
		     (melt_ptr_t) (( /*!OUTPUT_C_CODE */ meltfrout->
				    tabval[2])),
		     (MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_LONG ""),
		     argtab, "", (union meltparam_un *) 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:987:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
			   (");"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:988:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
				( /*_#DEPTHP1__L6*/ meltfnum[1]), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:989:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]), (" "));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:990:/ locexp");
      /*add2sbuf_string */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
			   melt_string_str ((melt_ptr_t)
					    ( /*_.PTRSTR__V4*/ meltfptr[3])));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:991:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
			   ("->"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:992:/ locexp");
      /*add2sbuf_string */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
			   melt_string_str ((melt_ptr_t)
					    ( /*_.CNAM__V14*/ meltfptr[13])));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:993:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
			   (".val = "));
    }
    ;
 /*_#GET_INT__L7*/ meltfnum[3] =
      (melt_get_int ((melt_ptr_t) ( /*_.ODATA__V16*/ meltfptr[15])));;

    {
      MELT_LOCATION ("warmelt-outobj.melt:994:/ locexp");
      meltgc_add_strbuf_dec ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
			     ( /*_#GET_INT__L7*/ meltfnum[3]));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:995:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]), (";"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:996:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
				( /*_#DEPTHP1__L6*/ meltfnum[1]), 0);
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:998:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#STRBUF_USEDLENGTH__L8*/ meltfnum[7] =
      melt_strbuf_usedlength ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]));;
    MELT_LOCATION ("warmelt-outobj.melt:998:/ cond");
    /*cond */ if (
		   /*ifisa */
		   melt_is_instance_of ((melt_ptr_t)
					(( /*!BUFFER_LIMIT_CONT */ meltfrout->
					  tabval[3])),
					(melt_ptr_t) (( /*!CLASS_REFERENCE */
						       meltfrout->tabval[4])))
      )				/*then */
      {
	/*^cond.then */
	/*^getslot */
	{
	  melt_ptr_t slot = NULL, obj = NULL;
	  obj =
	    (melt_ptr_t) (( /*!BUFFER_LIMIT_CONT */ meltfrout->
			   tabval[3])) /*=obj*/ ;
	  melt_object_get_field (slot, obj, 0, "REFERENCED_VALUE");
   /*_.REFERENCED_VALUE__V20*/ meltfptr[19] = slot;
	};
	;
      }
    else
      {				/*^cond.else */

  /*_.REFERENCED_VALUE__V20*/ meltfptr[19] = NULL;;
      }
    ;
    /*^compute */
 /*_#GET_INT__L9*/ meltfnum[8] =
      (melt_get_int
       ((melt_ptr_t) ( /*_.REFERENCED_VALUE__V20*/ meltfptr[19])));;
    /*^compute */
 /*_#IRAW__L10*/ meltfnum[9] =
      (( /*_#GET_INT__L9*/ meltfnum[8]) / (2));;
    /*^compute */
 /*_#I__L11*/ meltfnum[10] =
      (( /*_#STRBUF_USEDLENGTH__L8*/ meltfnum[7]) >
       ( /*_#IRAW__L10*/ meltfnum[9]));;
    MELT_LOCATION ("warmelt-outobj.melt:998:/ cond");
    /*cond */ if ( /*_#I__L11*/ meltfnum[10])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{


	  {
	    MELT_LOCATION ("warmelt-outobj.melt:999:/ locexp");

#if MELT_HAVE_DEBUG
	    if (melt_need_debug (0))
	      melt_dbgshortbacktrace (("outcinitfill_objinitboxedinteger huge sbuf"), (10));
#endif
	    ;
	  }
	  ;

#if MELT_HAVE_DEBUG
	  MELT_LOCATION ("warmelt-outobj.melt:1000:/ cppif.then");
	  /*^block */
	  /*anyblock */
	  {


	    {
	      /*^locexp */
	      /*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	      melt_dbgcounter++;
#endif
	      ;
	    }
	    ;
	    /*^checksignal */
	    MELT_CHECK_SIGNAL ();
	    ;
     /*_#MELT_NEED_DBG__L12*/ meltfnum[11] =
	      /*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	      ( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	      0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	      ;;
	    MELT_LOCATION ("warmelt-outobj.melt:1000:/ cond");
	    /*cond */ if ( /*_#MELT_NEED_DBG__L12*/ meltfnum[11])	/*then */
	      {
		/*^cond.then */
		/*^block */
		/*anyblock */
		{

       /*_#THE_MELTCALLCOUNT__L13*/ meltfnum[12] =
#ifdef meltcallcount
		    meltcallcount	/* the_meltcallcount */
#else
		    0L
#endif /* meltcallcount the_meltcallcount */
		    ;;
		  MELT_LOCATION ("warmelt-outobj.melt:1000:/ checksignal");
		  MELT_CHECK_SIGNAL ();
		  ;
		  /*^apply */
		  /*apply */
		  {
		    union meltparam_un argtab[5];
		    memset (&argtab, 0, sizeof (argtab));
		    /*^apply.arg */
		    argtab[0].meltbp_long =
		      /*_#THE_MELTCALLCOUNT__L13*/ meltfnum[12];
		    /*^apply.arg */
		    argtab[1].meltbp_cstring = "warmelt-outobj.melt";
		    /*^apply.arg */
		    argtab[2].meltbp_long = 1000;
		    /*^apply.arg */
		    argtab[3].meltbp_cstring =
		      "outcinitfill_objinitboxedinteger huge sbuf=";
		    /*^apply.arg */
		    argtab[4].meltbp_aptr =
		      (melt_ptr_t *) & /*_.SBUF__V3*/ meltfptr[2];
		    /*_.MELT_DEBUG_FUN__V24*/ meltfptr[23] =
		      melt_apply ((meltclosure_ptr_t)
				  (( /*!MELT_DEBUG_FUN */ meltfrout->
				    tabval[1])),
				  (melt_ptr_t) (( /*nil */ NULL)),
				  (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_PTR ""), argtab, "",
				  (union meltparam_un *) 0);
		  }
		  ;
		  /*_.IF___V23*/ meltfptr[22] =
		    /*_.MELT_DEBUG_FUN__V24*/ meltfptr[23];;
		  /*epilog */

		  MELT_LOCATION ("warmelt-outobj.melt:1000:/ clear");
		 /*clear *//*_#THE_MELTCALLCOUNT__L13*/ meltfnum[12] = 0;
		  /*^clear */
		 /*clear *//*_.MELT_DEBUG_FUN__V24*/ meltfptr[23] = 0;
		}
		;
	      }
	    else
	      {			/*^cond.else */

      /*_.IF___V23*/ meltfptr[22] = NULL;;
	      }
	    ;
	    MELT_LOCATION ("warmelt-outobj.melt:1000:/ quasiblock");


	    /*_.PROGN___V25*/ meltfptr[23] = /*_.IF___V23*/ meltfptr[22];;
	    /*^compute */
	    /*_.IFCPP___V22*/ meltfptr[21] = /*_.PROGN___V25*/ meltfptr[23];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-outobj.melt:1000:/ clear");
	       /*clear *//*_#MELT_NEED_DBG__L12*/ meltfnum[11] = 0;
	    /*^clear */
	       /*clear *//*_.IF___V23*/ meltfptr[22] = 0;
	    /*^clear */
	       /*clear *//*_.PROGN___V25*/ meltfptr[23] = 0;
	  }

#else /*MELT_HAVE_DEBUG */
	  /*^cppif.else */
	  /*_.IFCPP___V22*/ meltfptr[21] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
	  ;

#if MELT_HAVE_DEBUG
	  MELT_LOCATION ("warmelt-outobj.melt:1001:/ cppif.then");
	  /*^block */
	  /*anyblock */
	  {

	    /*^checksignal */
	    MELT_CHECK_SIGNAL ();
	    ;
     /*_#STRBUF_USEDLENGTH__L14*/ meltfnum[12] =
	      melt_strbuf_usedlength ((melt_ptr_t)
				      ( /*_.SBUF__V3*/ meltfptr[2]));;
	    MELT_LOCATION ("warmelt-outobj.melt:1002:/ cond");
	    /*cond */ if (
			   /*ifisa */
			   melt_is_instance_of ((melt_ptr_t)
						(( /*!BUFFER_LIMIT_CONT */
						  meltfrout->tabval[3])),
						(melt_ptr_t) (( /*!CLASS_REFERENCE */ meltfrout->tabval[4])))
	      )			/*then */
	      {
		/*^cond.then */
		/*^getslot */
		{
		  melt_ptr_t slot = NULL, obj = NULL;
		  obj =
		    (melt_ptr_t) (( /*!BUFFER_LIMIT_CONT */ meltfrout->
				   tabval[3])) /*=obj*/ ;
		  melt_object_get_field (slot, obj, 0, "REFERENCED_VALUE");
       /*_.REFERENCED_VALUE__V27*/ meltfptr[23] = slot;
		};
		;
	      }
	    else
	      {			/*^cond.else */

      /*_.REFERENCED_VALUE__V27*/ meltfptr[23] = NULL;;
	      }
	    ;
	    /*^compute */
     /*_#GET_INT__L15*/ meltfnum[11] =
	      (melt_get_int
	       ((melt_ptr_t) ( /*_.REFERENCED_VALUE__V27*/ meltfptr[23])));;
	    /*^compute */
     /*_#I__L16*/ meltfnum[15] =
	      (( /*_#STRBUF_USEDLENGTH__L14*/ meltfnum[12]) <
	       ( /*_#GET_INT__L15*/ meltfnum[11]));;
	    MELT_LOCATION ("warmelt-outobj.melt:1001:/ cond");
	    /*cond */ if ( /*_#I__L16*/ meltfnum[15])	/*then */
	      {
		/*^cond.then */
		/*_.IFELSE___V28*/ meltfptr[27] = ( /*nil */ NULL);;
	      }
	    else
	      {
		MELT_LOCATION ("warmelt-outobj.melt:1001:/ cond.else");

		/*^block */
		/*anyblock */
		{




		  {
		    /*^locexp */
		    melt_assert_failed (("check limited sbuf"),
					("warmelt-outobj.melt")
					? ("warmelt-outobj.melt") : __FILE__,
					(1001) ? (1001) : __LINE__,
					__FUNCTION__);
		    ;
		  }
		  ;
		 /*clear *//*_.IFELSE___V28*/ meltfptr[27] = 0;
		  /*epilog */
		}
		;
	      }
	    ;
	    /*^compute */
	    /*_.IFCPP___V26*/ meltfptr[22] = /*_.IFELSE___V28*/ meltfptr[27];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-outobj.melt:1001:/ clear");
	       /*clear *//*_#STRBUF_USEDLENGTH__L14*/ meltfnum[12] = 0;
	    /*^clear */
	       /*clear *//*_.REFERENCED_VALUE__V27*/ meltfptr[23] = 0;
	    /*^clear */
	       /*clear *//*_#GET_INT__L15*/ meltfnum[11] = 0;
	    /*^clear */
	       /*clear *//*_#I__L16*/ meltfnum[15] = 0;
	    /*^clear */
	       /*clear *//*_.IFELSE___V28*/ meltfptr[27] = 0;
	  }

#else /*MELT_HAVE_DEBUG */
	  /*^cppif.else */
	  /*_.IFCPP___V26*/ meltfptr[22] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
	  ;
	  MELT_LOCATION ("warmelt-outobj.melt:998:/ quasiblock");


	  /*_.PROGN___V29*/ meltfptr[23] = /*_.IFCPP___V26*/ meltfptr[22];;
	  /*^compute */
	  /*_.IFELSE___V21*/ meltfptr[20] = /*_.PROGN___V29*/ meltfptr[23];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-outobj.melt:998:/ clear");
	     /*clear *//*_.IFCPP___V22*/ meltfptr[21] = 0;
	  /*^clear */
	     /*clear *//*_.IFCPP___V26*/ meltfptr[22] = 0;
	  /*^clear */
	     /*clear *//*_.PROGN___V29*/ meltfptr[23] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

  /*_.IFELSE___V21*/ meltfptr[20] = NULL;;
      }
    ;
    /*^compute */
    /*_.LET___V13*/ meltfptr[8] = /*_.IFELSE___V21*/ meltfptr[20];;

    MELT_LOCATION ("warmelt-outobj.melt:962:/ clear");
	   /*clear *//*_.CNAM__V14*/ meltfptr[13] = 0;
    /*^clear */
	   /*clear *//*_.OLOCVAR__V15*/ meltfptr[14] = 0;
    /*^clear */
	   /*clear *//*_.ODATA__V16*/ meltfptr[15] = 0;
    /*^clear */
	   /*clear *//*_#DEPTHP1__L6*/ meltfnum[1] = 0;
    /*^clear */
	   /*clear *//*_.OIE_DISCR__V18*/ meltfptr[16] = 0;
    /*^clear */
	   /*clear *//*_.OUTPUT_C_CODE__V19*/ meltfptr[18] = 0;
    /*^clear */
	   /*clear *//*_#GET_INT__L7*/ meltfnum[3] = 0;
    /*^clear */
	   /*clear *//*_#STRBUF_USEDLENGTH__L8*/ meltfnum[7] = 0;
    /*^clear */
	   /*clear *//*_.REFERENCED_VALUE__V20*/ meltfptr[19] = 0;
    /*^clear */
	   /*clear *//*_#GET_INT__L9*/ meltfnum[8] = 0;
    /*^clear */
	   /*clear *//*_#IRAW__L10*/ meltfnum[9] = 0;
    /*^clear */
	   /*clear *//*_#I__L11*/ meltfnum[10] = 0;
    /*^clear */
	   /*clear *//*_.IFELSE___V21*/ meltfptr[20] = 0;
    MELT_LOCATION ("warmelt-outobj.melt:958:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.LET___V13*/ meltfptr[8];;

    {
      MELT_LOCATION ("warmelt-outobj.melt:958:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V5*/ meltfptr[4] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V7*/ meltfptr[5] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V11*/ meltfptr[7] = 0;
    /*^clear */
	   /*clear *//*_.LET___V13*/ meltfptr[8] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("OUTCINITFILL_OBJINITBOXEDINTEGER", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_24_warmelt_outobj_OUTCINITFILL_OBJINITBOXEDINTEGER_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_24_warmelt_outobj_OUTCINITFILL_OBJINITBOXEDINTEGER */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_25_warmelt_outobj_OUTDECLINIT_OBJINITPAIR (meltclosure_ptr_t
						    meltclosp_,
						    melt_ptr_t meltfirstargp_,
						    const melt_argdescr_cell_t
						    meltxargdescr_[],
						    union meltparam_un *
						    meltxargtab_,
						    const melt_argdescr_cell_t
						    meltxresdescr_[],
						    union meltparam_un *
						    meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_25_warmelt_outobj_OUTDECLINIT_OBJINITPAIR_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_25_warmelt_outobj_OUTDECLINIT_OBJINITPAIR_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 4
    melt_ptr_t mcfr_varptr[4];
/*no varnum*/
#define MELTFRAM_NBVARNUM /*none*/0
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_25_warmelt_outobj_OUTDECLINIT_OBJINITPAIR is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_25_warmelt_outobj_OUTDECLINIT_OBJINITPAIR_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 4; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_25_warmelt_outobj_OUTDECLINIT_OBJINITPAIR nbval 4*/
  meltfram__.mcfr_nbvar = 4 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("OUTDECLINIT_OBJINITPAIR", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-outobj.melt:1010:/ getarg");
 /*_.RECV__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.SBUF__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2])) !=
	      NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


    {
      MELT_LOCATION ("warmelt-outobj.melt:1011:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
			   (" struct meltpair_st "));
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:1012:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.RECV__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 1, "NAMED_NAME");
  /*_.NAMED_NAME__V4*/ meltfptr[3] = slot;
    };
    ;

    {
      /*^locexp */
      /*add2sbuf_string */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
			   melt_string_str ((melt_ptr_t)
					    ( /*_.NAMED_NAME__V4*/
					     meltfptr[3])));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1013:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]), (";"));
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:1010:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*epilog */

    /*^clear */
	   /*clear *//*_.NAMED_NAME__V4*/ meltfptr[3] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("OUTDECLINIT_OBJINITPAIR", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_25_warmelt_outobj_OUTDECLINIT_OBJINITPAIR_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*noretval */ NULL);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_25_warmelt_outobj_OUTDECLINIT_OBJINITPAIR */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_26_warmelt_outobj_OUTCINITFILL_OBJINITPAIR (meltclosure_ptr_t
						     meltclosp_,
						     melt_ptr_t
						     meltfirstargp_,
						     const
						     melt_argdescr_cell_t
						     meltxargdescr_[],
						     union meltparam_un *
						     meltxargtab_,
						     const
						     melt_argdescr_cell_t
						     meltxresdescr_[],
						     union meltparam_un *
						     meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_26_warmelt_outobj_OUTCINITFILL_OBJINITPAIR_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_26_warmelt_outobj_OUTCINITFILL_OBJINITPAIR_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 29
    melt_ptr_t mcfr_varptr[29];
#define MELTFRAM_NBVARNUM 15
    long mcfr_varnum[15];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_26_warmelt_outobj_OUTCINITFILL_OBJINITPAIR is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_26_warmelt_outobj_OUTCINITFILL_OBJINITPAIR_st
	 *) meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 29; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_26_warmelt_outobj_OUTCINITFILL_OBJINITPAIR nbval 29*/
  meltfram__.mcfr_nbvar = 29 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("OUTCINITFILL_OBJINITPAIR", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-outobj.melt:1017:/ getarg");
 /*_.RECV__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.SBUF__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2])) !=
	      NULL);


  /*getarg#2 */
  /*^getarg */
  if (meltxargdescr_[1] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.PTRSTR__V4*/ meltfptr[3] =
    (meltxargtab_[1].meltbp_aptr) ? (*(meltxargtab_[1].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.PTRSTR__V4*/ meltfptr[3])) !=
	      NULL);


  /*getarg#3 */
  /*^getarg */
  if (meltxargdescr_[2] != MELTBPAR_LONG)
    goto lab_endgetargs;
 /*_#DEPTH__L1*/ meltfnum[0] = meltxargtab_[2].meltbp_long;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:1018:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L2*/ meltfnum[1] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.RECV__V2*/ meltfptr[1]),
			     (melt_ptr_t) (( /*!CLASS_OBJINITPAIR */
					    meltfrout->tabval[0])));;
      MELT_LOCATION ("warmelt-outobj.melt:1018:/ cond");
      /*cond */ if ( /*_#IS_A__L2*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V6*/ meltfptr[5] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-outobj.melt:1018:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("outcinitfill_objinitpair check recv"),
				  ("warmelt-outobj.melt")
				  ? ("warmelt-outobj.melt") : __FILE__,
				  (1018) ? (1018) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V6*/ meltfptr[5] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V5*/ meltfptr[4] = /*_.IFELSE___V6*/ meltfptr[5];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:1018:/ clear");
	     /*clear *//*_#IS_A__L2*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V6*/ meltfptr[5] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V5*/ meltfptr[4] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:1019:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L3*/ meltfnum[1] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-outobj.melt:1019:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L3*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L4*/ meltfnum[3] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-outobj.melt:1019:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[7];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L4*/ meltfnum[3];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-outobj.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 1019;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "outcinitfill_objinitpair recv=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.RECV__V2*/ meltfptr[1];
	      /*^apply.arg */
	      argtab[5].meltbp_cstring = " ptrstr=";
	      /*^apply.arg */
	      argtab[6].meltbp_aptr =
		(melt_ptr_t *) & /*_.PTRSTR__V4*/ meltfptr[3];
	      /*_.MELT_DEBUG_FUN__V9*/ meltfptr[8] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[1])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V8*/ meltfptr[7] = /*_.MELT_DEBUG_FUN__V9*/ meltfptr[8];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-outobj.melt:1019:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L4*/ meltfnum[3] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V9*/ meltfptr[8] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V8*/ meltfptr[7] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-outobj.melt:1019:/ quasiblock");


      /*_.PROGN___V10*/ meltfptr[8] = /*_.IF___V8*/ meltfptr[7];;
      /*^compute */
      /*_.IFCPP___V7*/ meltfptr[5] = /*_.PROGN___V10*/ meltfptr[8];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:1019:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L3*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IF___V8*/ meltfptr[7] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V10*/ meltfptr[8] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V7*/ meltfptr[5] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:1020:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_STRING__L5*/ meltfnum[3] =
	(melt_magic_discr ((melt_ptr_t) ( /*_.PTRSTR__V4*/ meltfptr[3])) ==
	 MELTOBMAG_STRING);;
      MELT_LOCATION ("warmelt-outobj.melt:1020:/ cond");
      /*cond */ if ( /*_#IS_STRING__L5*/ meltfnum[3])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V12*/ meltfptr[8] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-outobj.melt:1020:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("outcinitfill_objinitpair check ptrstr"),
				  ("warmelt-outobj.melt")
				  ? ("warmelt-outobj.melt") : __FILE__,
				  (1020) ? (1020) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V12*/ meltfptr[8] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V11*/ meltfptr[7] = /*_.IFELSE___V12*/ meltfptr[8];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:1020:/ clear");
	     /*clear *//*_#IS_STRING__L5*/ meltfnum[3] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V12*/ meltfptr[8] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V11*/ meltfptr[7] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-outobj.melt:1021:/ quasiblock");


    /*^getslot */
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.RECV__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 1, "OIE_CNAME");
  /*_.CNAM__V14*/ meltfptr[13] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-outobj.melt:1022:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.RECV__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 4, "OIE_LOCVAR");
  /*_.OLOCVAR__V15*/ meltfptr[14] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-outobj.melt:1023:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.RECV__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 2, "OIE_DATA");
  /*_.ODATA__V16*/ meltfptr[15] = slot;
    };
    ;
 /*_#DEPTHP1__L6*/ meltfnum[1] =
      ((1) + ( /*_#DEPTH__L1*/ meltfnum[0]));;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1026:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
			   ("/*inipair "));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1027:/ locexp");
      /*add2sbuf_string */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
			   melt_string_str ((melt_ptr_t)
					    ( /*_.CNAM__V14*/ meltfptr[13])));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1028:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
			   ("*/"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1029:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
				( /*_#DEPTHP1__L6*/ meltfnum[1]), 0);
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:1030:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^cond */
    /*cond */ if ( /*_.OLOCVAR__V15*/ meltfptr[14])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-outobj.melt:1032:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^msend */
	  /*msend */
	  {
	    union meltparam_un argtab[3];
	    memset (&argtab, 0, sizeof (argtab));
	    /*^ojbmsend.arg */
	    argtab[0].meltbp_aptr = /*nil */ (melt_ptr_t *) NULL;
	    /*^ojbmsend.arg */
	    argtab[1].meltbp_aptr =
	      (melt_ptr_t *) & /*_.SBUF__V3*/ meltfptr[2];
	    /*^ojbmsend.arg */
	    argtab[2].meltbp_long = /*_#DEPTHP1__L6*/ meltfnum[1];
	    /*_.OUTPUT_C_CODE__V17*/ meltfptr[16] =
	      meltgc_send ((melt_ptr_t) ( /*_.OLOCVAR__V15*/ meltfptr[14]),
			   (melt_ptr_t) (( /*!OUTPUT_C_CODE */ meltfrout->
					  tabval[2])),
			   (MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_LONG
			    ""), argtab, "", (union meltparam_un *) 0);
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:1033:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
				 (" = (melt_ptr_t) &"));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:1034:/ locexp");
	    /*add2sbuf_string */
	      meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
				 melt_string_str ((melt_ptr_t)
						  ( /*_.PTRSTR__V4*/
						   meltfptr[3])));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:1035:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
				 ("->"));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:1036:/ locexp");
	    /*add2sbuf_string */
	      meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
				 melt_string_str ((melt_ptr_t)
						  ( /*_.CNAM__V14*/
						   meltfptr[13])));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:1037:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
				 (";"));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:1038:/ locexp");
	    meltgc_strbuf_add_indent ((melt_ptr_t)
				      ( /*_.SBUF__V3*/ meltfptr[2]),
				      ( /*_#DEPTHP1__L6*/ meltfnum[1]), 0);
	  }
	  ;
	  MELT_LOCATION ("warmelt-outobj.melt:1031:/ quasiblock");


	  /*epilog */

	  MELT_LOCATION ("warmelt-outobj.melt:1030:/ clear");
	     /*clear *//*_.OUTPUT_C_CODE__V17*/ meltfptr[16] = 0;
	}
	;
      }				/*noelse */
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1040:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]), (" "));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1041:/ locexp");
      /*add2sbuf_string */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
			   melt_string_str ((melt_ptr_t)
					    ( /*_.PTRSTR__V4*/ meltfptr[3])));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1042:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
			   ("->"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1043:/ locexp");
      /*add2sbuf_string */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
			   melt_string_str ((melt_ptr_t)
					    ( /*_.CNAM__V14*/ meltfptr[13])));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1044:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
			   (".discr = (meltobject_ptr_t)("));
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:1045:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.RECV__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 3, "OIE_DISCR");
  /*_.OIE_DISCR__V18*/ meltfptr[16] = slot;
    };
    ;
    /*^checksignal */
    MELT_CHECK_SIGNAL ();
    ;
    /*^msend */
    /*msend */
    {
      union meltparam_un argtab[3];
      memset (&argtab, 0, sizeof (argtab));
      /*^ojbmsend.arg */
      argtab[0].meltbp_aptr = /*nil */ (melt_ptr_t *) NULL;
      /*^ojbmsend.arg */
      argtab[1].meltbp_aptr = (melt_ptr_t *) & /*_.SBUF__V3*/ meltfptr[2];
      /*^ojbmsend.arg */
      argtab[2].meltbp_long = /*_#DEPTHP1__L6*/ meltfnum[1];
      /*_.OUTPUT_C_CODE__V19*/ meltfptr[18] =
	meltgc_send ((melt_ptr_t) ( /*_.OIE_DISCR__V18*/ meltfptr[16]),
		     (melt_ptr_t) (( /*!OUTPUT_C_CODE */ meltfrout->
				    tabval[2])),
		     (MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_LONG ""),
		     argtab, "", (union meltparam_un *) 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1046:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
			   (");"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1047:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
				( /*_#DEPTH__L1*/ meltfnum[0]), 0);
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:1049:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#STRBUF_USEDLENGTH__L7*/ meltfnum[3] =
      melt_strbuf_usedlength ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]));;
    MELT_LOCATION ("warmelt-outobj.melt:1049:/ cond");
    /*cond */ if (
		   /*ifisa */
		   melt_is_instance_of ((melt_ptr_t)
					(( /*!BUFFER_LIMIT_CONT */ meltfrout->
					  tabval[3])),
					(melt_ptr_t) (( /*!CLASS_REFERENCE */
						       meltfrout->tabval[4])))
      )				/*then */
      {
	/*^cond.then */
	/*^getslot */
	{
	  melt_ptr_t slot = NULL, obj = NULL;
	  obj =
	    (melt_ptr_t) (( /*!BUFFER_LIMIT_CONT */ meltfrout->
			   tabval[3])) /*=obj*/ ;
	  melt_object_get_field (slot, obj, 0, "REFERENCED_VALUE");
   /*_.REFERENCED_VALUE__V20*/ meltfptr[19] = slot;
	};
	;
      }
    else
      {				/*^cond.else */

  /*_.REFERENCED_VALUE__V20*/ meltfptr[19] = NULL;;
      }
    ;
    /*^compute */
 /*_#GET_INT__L8*/ meltfnum[7] =
      (melt_get_int
       ((melt_ptr_t) ( /*_.REFERENCED_VALUE__V20*/ meltfptr[19])));;
    /*^compute */
 /*_#IRAW__L9*/ meltfnum[8] =
      (( /*_#GET_INT__L8*/ meltfnum[7]) / (2));;
    /*^compute */
 /*_#I__L10*/ meltfnum[9] =
      (( /*_#STRBUF_USEDLENGTH__L7*/ meltfnum[3]) >
       ( /*_#IRAW__L9*/ meltfnum[8]));;
    MELT_LOCATION ("warmelt-outobj.melt:1049:/ cond");
    /*cond */ if ( /*_#I__L10*/ meltfnum[9])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{


	  {
	    MELT_LOCATION ("warmelt-outobj.melt:1050:/ locexp");

#if MELT_HAVE_DEBUG
	    if (melt_need_debug (0))
	      melt_dbgshortbacktrace (("outcinitfill_objinitpair huge sbuf"),
				      (10));
#endif
	    ;
	  }
	  ;

#if MELT_HAVE_DEBUG
	  MELT_LOCATION ("warmelt-outobj.melt:1051:/ cppif.then");
	  /*^block */
	  /*anyblock */
	  {


	    {
	      /*^locexp */
	      /*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	      melt_dbgcounter++;
#endif
	      ;
	    }
	    ;
	    /*^checksignal */
	    MELT_CHECK_SIGNAL ();
	    ;
     /*_#MELT_NEED_DBG__L11*/ meltfnum[10] =
	      /*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	      ( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	      0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	      ;;
	    MELT_LOCATION ("warmelt-outobj.melt:1051:/ cond");
	    /*cond */ if ( /*_#MELT_NEED_DBG__L11*/ meltfnum[10])	/*then */
	      {
		/*^cond.then */
		/*^block */
		/*anyblock */
		{

       /*_#THE_MELTCALLCOUNT__L12*/ meltfnum[11] =
#ifdef meltcallcount
		    meltcallcount	/* the_meltcallcount */
#else
		    0L
#endif /* meltcallcount the_meltcallcount */
		    ;;
		  MELT_LOCATION ("warmelt-outobj.melt:1051:/ checksignal");
		  MELT_CHECK_SIGNAL ();
		  ;
		  /*^apply */
		  /*apply */
		  {
		    union meltparam_un argtab[5];
		    memset (&argtab, 0, sizeof (argtab));
		    /*^apply.arg */
		    argtab[0].meltbp_long =
		      /*_#THE_MELTCALLCOUNT__L12*/ meltfnum[11];
		    /*^apply.arg */
		    argtab[1].meltbp_cstring = "warmelt-outobj.melt";
		    /*^apply.arg */
		    argtab[2].meltbp_long = 1051;
		    /*^apply.arg */
		    argtab[3].meltbp_cstring =
		      "outcinitfill_objinitpair huge sbuf=";
		    /*^apply.arg */
		    argtab[4].meltbp_aptr =
		      (melt_ptr_t *) & /*_.SBUF__V3*/ meltfptr[2];
		    /*_.MELT_DEBUG_FUN__V24*/ meltfptr[23] =
		      melt_apply ((meltclosure_ptr_t)
				  (( /*!MELT_DEBUG_FUN */ meltfrout->
				    tabval[1])),
				  (melt_ptr_t) (( /*nil */ NULL)),
				  (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_PTR ""), argtab, "",
				  (union meltparam_un *) 0);
		  }
		  ;
		  /*_.IF___V23*/ meltfptr[22] =
		    /*_.MELT_DEBUG_FUN__V24*/ meltfptr[23];;
		  /*epilog */

		  MELT_LOCATION ("warmelt-outobj.melt:1051:/ clear");
		 /*clear *//*_#THE_MELTCALLCOUNT__L12*/ meltfnum[11] = 0;
		  /*^clear */
		 /*clear *//*_.MELT_DEBUG_FUN__V24*/ meltfptr[23] = 0;
		}
		;
	      }
	    else
	      {			/*^cond.else */

      /*_.IF___V23*/ meltfptr[22] = NULL;;
	      }
	    ;
	    MELT_LOCATION ("warmelt-outobj.melt:1051:/ quasiblock");


	    /*_.PROGN___V25*/ meltfptr[23] = /*_.IF___V23*/ meltfptr[22];;
	    /*^compute */
	    /*_.IFCPP___V22*/ meltfptr[21] = /*_.PROGN___V25*/ meltfptr[23];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-outobj.melt:1051:/ clear");
	       /*clear *//*_#MELT_NEED_DBG__L11*/ meltfnum[10] = 0;
	    /*^clear */
	       /*clear *//*_.IF___V23*/ meltfptr[22] = 0;
	    /*^clear */
	       /*clear *//*_.PROGN___V25*/ meltfptr[23] = 0;
	  }

#else /*MELT_HAVE_DEBUG */
	  /*^cppif.else */
	  /*_.IFCPP___V22*/ meltfptr[21] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
	  ;

#if MELT_HAVE_DEBUG
	  MELT_LOCATION ("warmelt-outobj.melt:1052:/ cppif.then");
	  /*^block */
	  /*anyblock */
	  {

	    /*^checksignal */
	    MELT_CHECK_SIGNAL ();
	    ;
     /*_#STRBUF_USEDLENGTH__L13*/ meltfnum[11] =
	      melt_strbuf_usedlength ((melt_ptr_t)
				      ( /*_.SBUF__V3*/ meltfptr[2]));;
	    MELT_LOCATION ("warmelt-outobj.melt:1053:/ cond");
	    /*cond */ if (
			   /*ifisa */
			   melt_is_instance_of ((melt_ptr_t)
						(( /*!BUFFER_LIMIT_CONT */
						  meltfrout->tabval[3])),
						(melt_ptr_t) (( /*!CLASS_REFERENCE */ meltfrout->tabval[4])))
	      )			/*then */
	      {
		/*^cond.then */
		/*^getslot */
		{
		  melt_ptr_t slot = NULL, obj = NULL;
		  obj =
		    (melt_ptr_t) (( /*!BUFFER_LIMIT_CONT */ meltfrout->
				   tabval[3])) /*=obj*/ ;
		  melt_object_get_field (slot, obj, 0, "REFERENCED_VALUE");
       /*_.REFERENCED_VALUE__V27*/ meltfptr[23] = slot;
		};
		;
	      }
	    else
	      {			/*^cond.else */

      /*_.REFERENCED_VALUE__V27*/ meltfptr[23] = NULL;;
	      }
	    ;
	    /*^compute */
     /*_#GET_INT__L14*/ meltfnum[10] =
	      (melt_get_int
	       ((melt_ptr_t) ( /*_.REFERENCED_VALUE__V27*/ meltfptr[23])));;
	    /*^compute */
     /*_#I__L15*/ meltfnum[14] =
	      (( /*_#STRBUF_USEDLENGTH__L13*/ meltfnum[11]) <
	       ( /*_#GET_INT__L14*/ meltfnum[10]));;
	    MELT_LOCATION ("warmelt-outobj.melt:1052:/ cond");
	    /*cond */ if ( /*_#I__L15*/ meltfnum[14])	/*then */
	      {
		/*^cond.then */
		/*_.IFELSE___V28*/ meltfptr[27] = ( /*nil */ NULL);;
	      }
	    else
	      {
		MELT_LOCATION ("warmelt-outobj.melt:1052:/ cond.else");

		/*^block */
		/*anyblock */
		{




		  {
		    /*^locexp */
		    melt_assert_failed (("check limited sbuf"),
					("warmelt-outobj.melt")
					? ("warmelt-outobj.melt") : __FILE__,
					(1052) ? (1052) : __LINE__,
					__FUNCTION__);
		    ;
		  }
		  ;
		 /*clear *//*_.IFELSE___V28*/ meltfptr[27] = 0;
		  /*epilog */
		}
		;
	      }
	    ;
	    /*^compute */
	    /*_.IFCPP___V26*/ meltfptr[22] = /*_.IFELSE___V28*/ meltfptr[27];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-outobj.melt:1052:/ clear");
	       /*clear *//*_#STRBUF_USEDLENGTH__L13*/ meltfnum[11] = 0;
	    /*^clear */
	       /*clear *//*_.REFERENCED_VALUE__V27*/ meltfptr[23] = 0;
	    /*^clear */
	       /*clear *//*_#GET_INT__L14*/ meltfnum[10] = 0;
	    /*^clear */
	       /*clear *//*_#I__L15*/ meltfnum[14] = 0;
	    /*^clear */
	       /*clear *//*_.IFELSE___V28*/ meltfptr[27] = 0;
	  }

#else /*MELT_HAVE_DEBUG */
	  /*^cppif.else */
	  /*_.IFCPP___V26*/ meltfptr[22] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
	  ;
	  MELT_LOCATION ("warmelt-outobj.melt:1049:/ quasiblock");


	  /*_.PROGN___V29*/ meltfptr[23] = /*_.IFCPP___V26*/ meltfptr[22];;
	  /*^compute */
	  /*_.IFELSE___V21*/ meltfptr[20] = /*_.PROGN___V29*/ meltfptr[23];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-outobj.melt:1049:/ clear");
	     /*clear *//*_.IFCPP___V22*/ meltfptr[21] = 0;
	  /*^clear */
	     /*clear *//*_.IFCPP___V26*/ meltfptr[22] = 0;
	  /*^clear */
	     /*clear *//*_.PROGN___V29*/ meltfptr[23] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

  /*_.IFELSE___V21*/ meltfptr[20] = NULL;;
      }
    ;
    /*^compute */
    /*_.LET___V13*/ meltfptr[8] = /*_.IFELSE___V21*/ meltfptr[20];;

    MELT_LOCATION ("warmelt-outobj.melt:1021:/ clear");
	   /*clear *//*_.CNAM__V14*/ meltfptr[13] = 0;
    /*^clear */
	   /*clear *//*_.OLOCVAR__V15*/ meltfptr[14] = 0;
    /*^clear */
	   /*clear *//*_.ODATA__V16*/ meltfptr[15] = 0;
    /*^clear */
	   /*clear *//*_#DEPTHP1__L6*/ meltfnum[1] = 0;
    /*^clear */
	   /*clear *//*_.OIE_DISCR__V18*/ meltfptr[16] = 0;
    /*^clear */
	   /*clear *//*_.OUTPUT_C_CODE__V19*/ meltfptr[18] = 0;
    /*^clear */
	   /*clear *//*_#STRBUF_USEDLENGTH__L7*/ meltfnum[3] = 0;
    /*^clear */
	   /*clear *//*_.REFERENCED_VALUE__V20*/ meltfptr[19] = 0;
    /*^clear */
	   /*clear *//*_#GET_INT__L8*/ meltfnum[7] = 0;
    /*^clear */
	   /*clear *//*_#IRAW__L9*/ meltfnum[8] = 0;
    /*^clear */
	   /*clear *//*_#I__L10*/ meltfnum[9] = 0;
    /*^clear */
	   /*clear *//*_.IFELSE___V21*/ meltfptr[20] = 0;
    MELT_LOCATION ("warmelt-outobj.melt:1017:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.LET___V13*/ meltfptr[8];;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1017:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V5*/ meltfptr[4] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V7*/ meltfptr[5] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V11*/ meltfptr[7] = 0;
    /*^clear */
	   /*clear *//*_.LET___V13*/ meltfptr[8] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("OUTCINITFILL_OBJINITPAIR", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_26_warmelt_outobj_OUTCINITFILL_OBJINITPAIR_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_26_warmelt_outobj_OUTCINITFILL_OBJINITPAIR */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_27_warmelt_outobj_OUTDECLINIT_OBJINITLIST (meltclosure_ptr_t
						    meltclosp_,
						    melt_ptr_t meltfirstargp_,
						    const melt_argdescr_cell_t
						    meltxargdescr_[],
						    union meltparam_un *
						    meltxargtab_,
						    const melt_argdescr_cell_t
						    meltxresdescr_[],
						    union meltparam_un *
						    meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_27_warmelt_outobj_OUTDECLINIT_OBJINITLIST_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_27_warmelt_outobj_OUTDECLINIT_OBJINITLIST_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 4
    melt_ptr_t mcfr_varptr[4];
/*no varnum*/
#define MELTFRAM_NBVARNUM /*none*/0
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_27_warmelt_outobj_OUTDECLINIT_OBJINITLIST is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_27_warmelt_outobj_OUTDECLINIT_OBJINITLIST_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 4; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_27_warmelt_outobj_OUTDECLINIT_OBJINITLIST nbval 4*/
  meltfram__.mcfr_nbvar = 4 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("OUTDECLINIT_OBJINITLIST", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-outobj.melt:1059:/ getarg");
 /*_.RECV__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.SBUF__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2])) !=
	      NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


    {
      MELT_LOCATION ("warmelt-outobj.melt:1060:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
			   (" struct meltlist_st "));
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:1061:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.RECV__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 1, "NAMED_NAME");
  /*_.NAMED_NAME__V4*/ meltfptr[3] = slot;
    };
    ;

    {
      /*^locexp */
      /*add2sbuf_string */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
			   melt_string_str ((melt_ptr_t)
					    ( /*_.NAMED_NAME__V4*/
					     meltfptr[3])));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1062:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]), (";"));
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:1059:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*epilog */

    /*^clear */
	   /*clear *//*_.NAMED_NAME__V4*/ meltfptr[3] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("OUTDECLINIT_OBJINITLIST", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_27_warmelt_outobj_OUTDECLINIT_OBJINITLIST_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*noretval */ NULL);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_27_warmelt_outobj_OUTDECLINIT_OBJINITLIST */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_28_warmelt_outobj_OUTCINITFILL_OBJINITLIST (meltclosure_ptr_t
						     meltclosp_,
						     melt_ptr_t
						     meltfirstargp_,
						     const
						     melt_argdescr_cell_t
						     meltxargdescr_[],
						     union meltparam_un *
						     meltxargtab_,
						     const
						     melt_argdescr_cell_t
						     meltxresdescr_[],
						     union meltparam_un *
						     meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_28_warmelt_outobj_OUTCINITFILL_OBJINITLIST_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_28_warmelt_outobj_OUTCINITFILL_OBJINITLIST_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 29
    melt_ptr_t mcfr_varptr[29];
#define MELTFRAM_NBVARNUM 15
    long mcfr_varnum[15];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_28_warmelt_outobj_OUTCINITFILL_OBJINITLIST is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_28_warmelt_outobj_OUTCINITFILL_OBJINITLIST_st
	 *) meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 29; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_28_warmelt_outobj_OUTCINITFILL_OBJINITLIST nbval 29*/
  meltfram__.mcfr_nbvar = 29 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("OUTCINITFILL_OBJINITLIST", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-outobj.melt:1066:/ getarg");
 /*_.RECV__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.SBUF__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2])) !=
	      NULL);


  /*getarg#2 */
  /*^getarg */
  if (meltxargdescr_[1] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.PTRSTR__V4*/ meltfptr[3] =
    (meltxargtab_[1].meltbp_aptr) ? (*(meltxargtab_[1].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.PTRSTR__V4*/ meltfptr[3])) !=
	      NULL);


  /*getarg#3 */
  /*^getarg */
  if (meltxargdescr_[2] != MELTBPAR_LONG)
    goto lab_endgetargs;
 /*_#DEPTH__L1*/ meltfnum[0] = meltxargtab_[2].meltbp_long;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:1067:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L2*/ meltfnum[1] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.RECV__V2*/ meltfptr[1]),
			     (melt_ptr_t) (( /*!CLASS_OBJINITLIST */
					    meltfrout->tabval[0])));;
      MELT_LOCATION ("warmelt-outobj.melt:1067:/ cond");
      /*cond */ if ( /*_#IS_A__L2*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V6*/ meltfptr[5] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-outobj.melt:1067:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("outcinitfill_objinitlist check recv"),
				  ("warmelt-outobj.melt")
				  ? ("warmelt-outobj.melt") : __FILE__,
				  (1067) ? (1067) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V6*/ meltfptr[5] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V5*/ meltfptr[4] = /*_.IFELSE___V6*/ meltfptr[5];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:1067:/ clear");
	     /*clear *//*_#IS_A__L2*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V6*/ meltfptr[5] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V5*/ meltfptr[4] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:1068:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L3*/ meltfnum[1] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-outobj.melt:1068:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L3*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L4*/ meltfnum[3] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-outobj.melt:1068:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[7];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L4*/ meltfnum[3];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-outobj.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 1068;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "outcinitfill_objinitlist recv=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.RECV__V2*/ meltfptr[1];
	      /*^apply.arg */
	      argtab[5].meltbp_cstring = " ptrstr=";
	      /*^apply.arg */
	      argtab[6].meltbp_aptr =
		(melt_ptr_t *) & /*_.PTRSTR__V4*/ meltfptr[3];
	      /*_.MELT_DEBUG_FUN__V9*/ meltfptr[8] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[1])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V8*/ meltfptr[7] = /*_.MELT_DEBUG_FUN__V9*/ meltfptr[8];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-outobj.melt:1068:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L4*/ meltfnum[3] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V9*/ meltfptr[8] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V8*/ meltfptr[7] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-outobj.melt:1068:/ quasiblock");


      /*_.PROGN___V10*/ meltfptr[8] = /*_.IF___V8*/ meltfptr[7];;
      /*^compute */
      /*_.IFCPP___V7*/ meltfptr[5] = /*_.PROGN___V10*/ meltfptr[8];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:1068:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L3*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IF___V8*/ meltfptr[7] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V10*/ meltfptr[8] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V7*/ meltfptr[5] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:1069:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_STRING__L5*/ meltfnum[3] =
	(melt_magic_discr ((melt_ptr_t) ( /*_.PTRSTR__V4*/ meltfptr[3])) ==
	 MELTOBMAG_STRING);;
      MELT_LOCATION ("warmelt-outobj.melt:1069:/ cond");
      /*cond */ if ( /*_#IS_STRING__L5*/ meltfnum[3])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V12*/ meltfptr[8] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-outobj.melt:1069:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("outcinitfill_objinitlist check ptrstr"),
				  ("warmelt-outobj.melt")
				  ? ("warmelt-outobj.melt") : __FILE__,
				  (1069) ? (1069) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V12*/ meltfptr[8] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V11*/ meltfptr[7] = /*_.IFELSE___V12*/ meltfptr[8];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:1069:/ clear");
	     /*clear *//*_#IS_STRING__L5*/ meltfnum[3] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V12*/ meltfptr[8] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V11*/ meltfptr[7] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-outobj.melt:1070:/ quasiblock");


    /*^getslot */
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.RECV__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 1, "OIE_CNAME");
  /*_.CNAM__V14*/ meltfptr[13] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-outobj.melt:1071:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.RECV__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 4, "OIE_LOCVAR");
  /*_.OLOCVAR__V15*/ meltfptr[14] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-outobj.melt:1072:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.RECV__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 2, "OIE_DATA");
  /*_.ODATA__V16*/ meltfptr[15] = slot;
    };
    ;
 /*_#DEPTHP1__L6*/ meltfnum[1] =
      ((1) + ( /*_#DEPTH__L1*/ meltfnum[0]));;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1075:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
			   ("/*inilist "));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1076:/ locexp");
      /*add2sbuf_string */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
			   melt_string_str ((melt_ptr_t)
					    ( /*_.CNAM__V14*/ meltfptr[13])));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1077:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
			   ("*/"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1078:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
				( /*_#DEPTHP1__L6*/ meltfnum[1]), 0);
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:1079:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^cond */
    /*cond */ if ( /*_.OLOCVAR__V15*/ meltfptr[14])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-outobj.melt:1081:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^msend */
	  /*msend */
	  {
	    union meltparam_un argtab[3];
	    memset (&argtab, 0, sizeof (argtab));
	    /*^ojbmsend.arg */
	    argtab[0].meltbp_aptr = /*nil */ (melt_ptr_t *) NULL;
	    /*^ojbmsend.arg */
	    argtab[1].meltbp_aptr =
	      (melt_ptr_t *) & /*_.SBUF__V3*/ meltfptr[2];
	    /*^ojbmsend.arg */
	    argtab[2].meltbp_long = /*_#DEPTHP1__L6*/ meltfnum[1];
	    /*_.OUTPUT_C_CODE__V17*/ meltfptr[16] =
	      meltgc_send ((melt_ptr_t) ( /*_.OLOCVAR__V15*/ meltfptr[14]),
			   (melt_ptr_t) (( /*!OUTPUT_C_CODE */ meltfrout->
					  tabval[2])),
			   (MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_LONG
			    ""), argtab, "", (union meltparam_un *) 0);
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:1082:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
				 (" = (melt_ptr_t) &"));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:1083:/ locexp");
	    /*add2sbuf_string */
	      meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
				 melt_string_str ((melt_ptr_t)
						  ( /*_.PTRSTR__V4*/
						   meltfptr[3])));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:1084:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
				 ("->"));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:1085:/ locexp");
	    /*add2sbuf_string */
	      meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
				 melt_string_str ((melt_ptr_t)
						  ( /*_.CNAM__V14*/
						   meltfptr[13])));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:1086:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
				 (";"));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:1087:/ locexp");
	    meltgc_strbuf_add_indent ((melt_ptr_t)
				      ( /*_.SBUF__V3*/ meltfptr[2]),
				      ( /*_#DEPTHP1__L6*/ meltfnum[1]), 0);
	  }
	  ;
	  MELT_LOCATION ("warmelt-outobj.melt:1080:/ quasiblock");


	  /*epilog */

	  MELT_LOCATION ("warmelt-outobj.melt:1079:/ clear");
	     /*clear *//*_.OUTPUT_C_CODE__V17*/ meltfptr[16] = 0;
	}
	;
      }				/*noelse */
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1089:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]), (" "));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1090:/ locexp");
      /*add2sbuf_string */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
			   melt_string_str ((melt_ptr_t)
					    ( /*_.PTRSTR__V4*/ meltfptr[3])));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1091:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
			   ("->"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1092:/ locexp");
      /*add2sbuf_string */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
			   melt_string_str ((melt_ptr_t)
					    ( /*_.CNAM__V14*/ meltfptr[13])));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1093:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
			   (".discr = (meltobject_ptr_t)("));
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:1094:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.RECV__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 3, "OIE_DISCR");
  /*_.OIE_DISCR__V18*/ meltfptr[16] = slot;
    };
    ;
    /*^checksignal */
    MELT_CHECK_SIGNAL ();
    ;
    /*^msend */
    /*msend */
    {
      union meltparam_un argtab[3];
      memset (&argtab, 0, sizeof (argtab));
      /*^ojbmsend.arg */
      argtab[0].meltbp_aptr = /*nil */ (melt_ptr_t *) NULL;
      /*^ojbmsend.arg */
      argtab[1].meltbp_aptr = (melt_ptr_t *) & /*_.SBUF__V3*/ meltfptr[2];
      /*^ojbmsend.arg */
      argtab[2].meltbp_long = /*_#DEPTHP1__L6*/ meltfnum[1];
      /*_.OUTPUT_C_CODE__V19*/ meltfptr[18] =
	meltgc_send ((melt_ptr_t) ( /*_.OIE_DISCR__V18*/ meltfptr[16]),
		     (melt_ptr_t) (( /*!OUTPUT_C_CODE */ meltfrout->
				    tabval[2])),
		     (MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_LONG ""),
		     argtab, "", (union meltparam_un *) 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1095:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
			   (");"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1096:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]),
				( /*_#DEPTH__L1*/ meltfnum[0]), 0);
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:1098:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#STRBUF_USEDLENGTH__L7*/ meltfnum[3] =
      melt_strbuf_usedlength ((melt_ptr_t) ( /*_.SBUF__V3*/ meltfptr[2]));;
    MELT_LOCATION ("warmelt-outobj.melt:1098:/ cond");
    /*cond */ if (
		   /*ifisa */
		   melt_is_instance_of ((melt_ptr_t)
					(( /*!BUFFER_LIMIT_CONT */ meltfrout->
					  tabval[3])),
					(melt_ptr_t) (( /*!CLASS_REFERENCE */
						       meltfrout->tabval[4])))
      )				/*then */
      {
	/*^cond.then */
	/*^getslot */
	{
	  melt_ptr_t slot = NULL, obj = NULL;
	  obj =
	    (melt_ptr_t) (( /*!BUFFER_LIMIT_CONT */ meltfrout->
			   tabval[3])) /*=obj*/ ;
	  melt_object_get_field (slot, obj, 0, "REFERENCED_VALUE");
   /*_.REFERENCED_VALUE__V20*/ meltfptr[19] = slot;
	};
	;
      }
    else
      {				/*^cond.else */

  /*_.REFERENCED_VALUE__V20*/ meltfptr[19] = NULL;;
      }
    ;
    /*^compute */
 /*_#GET_INT__L8*/ meltfnum[7] =
      (melt_get_int
       ((melt_ptr_t) ( /*_.REFERENCED_VALUE__V20*/ meltfptr[19])));;
    /*^compute */
 /*_#IRAW__L9*/ meltfnum[8] =
      (( /*_#GET_INT__L8*/ meltfnum[7]) / (2));;
    /*^compute */
 /*_#I__L10*/ meltfnum[9] =
      (( /*_#STRBUF_USEDLENGTH__L7*/ meltfnum[3]) >
       ( /*_#IRAW__L9*/ meltfnum[8]));;
    MELT_LOCATION ("warmelt-outobj.melt:1098:/ cond");
    /*cond */ if ( /*_#I__L10*/ meltfnum[9])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{


	  {
	    MELT_LOCATION ("warmelt-outobj.melt:1099:/ locexp");

#if MELT_HAVE_DEBUG
	    if (melt_need_debug (0))
	      melt_dbgshortbacktrace (("outcinitfill_objinitlist huge sbuf"),
				      (10));
#endif
	    ;
	  }
	  ;

#if MELT_HAVE_DEBUG
	  MELT_LOCATION ("warmelt-outobj.melt:1100:/ cppif.then");
	  /*^block */
	  /*anyblock */
	  {


	    {
	      /*^locexp */
	      /*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	      melt_dbgcounter++;
#endif
	      ;
	    }
	    ;
	    /*^checksignal */
	    MELT_CHECK_SIGNAL ();
	    ;
     /*_#MELT_NEED_DBG__L11*/ meltfnum[10] =
	      /*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	      ( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	      0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	      ;;
	    MELT_LOCATION ("warmelt-outobj.melt:1100:/ cond");
	    /*cond */ if ( /*_#MELT_NEED_DBG__L11*/ meltfnum[10])	/*then */
	      {
		/*^cond.then */
		/*^block */
		/*anyblock */
		{

       /*_#THE_MELTCALLCOUNT__L12*/ meltfnum[11] =
#ifdef meltcallcount
		    meltcallcount	/* the_meltcallcount */
#else
		    0L
#endif /* meltcallcount the_meltcallcount */
		    ;;
		  MELT_LOCATION ("warmelt-outobj.melt:1100:/ checksignal");
		  MELT_CHECK_SIGNAL ();
		  ;
		  /*^apply */
		  /*apply */
		  {
		    union meltparam_un argtab[5];
		    memset (&argtab, 0, sizeof (argtab));
		    /*^apply.arg */
		    argtab[0].meltbp_long =
		      /*_#THE_MELTCALLCOUNT__L12*/ meltfnum[11];
		    /*^apply.arg */
		    argtab[1].meltbp_cstring = "warmelt-outobj.melt";
		    /*^apply.arg */
		    argtab[2].meltbp_long = 1100;
		    /*^apply.arg */
		    argtab[3].meltbp_cstring =
		      "outcinitfill_objinitlist huge sbuf=";
		    /*^apply.arg */
		    argtab[4].meltbp_aptr =
		      (melt_ptr_t *) & /*_.SBUF__V3*/ meltfptr[2];
		    /*_.MELT_DEBUG_FUN__V24*/ meltfptr[23] =
		      melt_apply ((meltclosure_ptr_t)
				  (( /*!MELT_DEBUG_FUN */ meltfrout->
				    tabval[1])),
				  (melt_ptr_t) (( /*nil */ NULL)),
				  (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_PTR ""), argtab, "",
				  (union meltparam_un *) 0);
		  }
		  ;
		  /*_.IF___V23*/ meltfptr[22] =
		    /*_.MELT_DEBUG_FUN__V24*/ meltfptr[23];;
		  /*epilog */

		  MELT_LOCATION ("warmelt-outobj.melt:1100:/ clear");
		 /*clear *//*_#THE_MELTCALLCOUNT__L12*/ meltfnum[11] = 0;
		  /*^clear */
		 /*clear *//*_.MELT_DEBUG_FUN__V24*/ meltfptr[23] = 0;
		}
		;
	      }
	    else
	      {			/*^cond.else */

      /*_.IF___V23*/ meltfptr[22] = NULL;;
	      }
	    ;
	    MELT_LOCATION ("warmelt-outobj.melt:1100:/ quasiblock");


	    /*_.PROGN___V25*/ meltfptr[23] = /*_.IF___V23*/ meltfptr[22];;
	    /*^compute */
	    /*_.IFCPP___V22*/ meltfptr[21] = /*_.PROGN___V25*/ meltfptr[23];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-outobj.melt:1100:/ clear");
	       /*clear *//*_#MELT_NEED_DBG__L11*/ meltfnum[10] = 0;
	    /*^clear */
	       /*clear *//*_.IF___V23*/ meltfptr[22] = 0;
	    /*^clear */
	       /*clear *//*_.PROGN___V25*/ meltfptr[23] = 0;
	  }

#else /*MELT_HAVE_DEBUG */
	  /*^cppif.else */
	  /*_.IFCPP___V22*/ meltfptr[21] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
	  ;

#if MELT_HAVE_DEBUG
	  MELT_LOCATION ("warmelt-outobj.melt:1101:/ cppif.then");
	  /*^block */
	  /*anyblock */
	  {

	    /*^checksignal */
	    MELT_CHECK_SIGNAL ();
	    ;
     /*_#STRBUF_USEDLENGTH__L13*/ meltfnum[11] =
	      melt_strbuf_usedlength ((melt_ptr_t)
				      ( /*_.SBUF__V3*/ meltfptr[2]));;
	    MELT_LOCATION ("warmelt-outobj.melt:1102:/ cond");
	    /*cond */ if (
			   /*ifisa */
			   melt_is_instance_of ((melt_ptr_t)
						(( /*!BUFFER_LIMIT_CONT */
						  meltfrout->tabval[3])),
						(melt_ptr_t) (( /*!CLASS_REFERENCE */ meltfrout->tabval[4])))
	      )			/*then */
	      {
		/*^cond.then */
		/*^getslot */
		{
		  melt_ptr_t slot = NULL, obj = NULL;
		  obj =
		    (melt_ptr_t) (( /*!BUFFER_LIMIT_CONT */ meltfrout->
				   tabval[3])) /*=obj*/ ;
		  melt_object_get_field (slot, obj, 0, "REFERENCED_VALUE");
       /*_.REFERENCED_VALUE__V27*/ meltfptr[23] = slot;
		};
		;
	      }
	    else
	      {			/*^cond.else */

      /*_.REFERENCED_VALUE__V27*/ meltfptr[23] = NULL;;
	      }
	    ;
	    /*^compute */
     /*_#GET_INT__L14*/ meltfnum[10] =
	      (melt_get_int
	       ((melt_ptr_t) ( /*_.REFERENCED_VALUE__V27*/ meltfptr[23])));;
	    /*^compute */
     /*_#I__L15*/ meltfnum[14] =
	      (( /*_#STRBUF_USEDLENGTH__L13*/ meltfnum[11]) <
	       ( /*_#GET_INT__L14*/ meltfnum[10]));;
	    MELT_LOCATION ("warmelt-outobj.melt:1101:/ cond");
	    /*cond */ if ( /*_#I__L15*/ meltfnum[14])	/*then */
	      {
		/*^cond.then */
		/*_.IFELSE___V28*/ meltfptr[27] = ( /*nil */ NULL);;
	      }
	    else
	      {
		MELT_LOCATION ("warmelt-outobj.melt:1101:/ cond.else");

		/*^block */
		/*anyblock */
		{




		  {
		    /*^locexp */
		    melt_assert_failed (("check limited sbuf"),
					("warmelt-outobj.melt")
					? ("warmelt-outobj.melt") : __FILE__,
					(1101) ? (1101) : __LINE__,
					__FUNCTION__);
		    ;
		  }
		  ;
		 /*clear *//*_.IFELSE___V28*/ meltfptr[27] = 0;
		  /*epilog */
		}
		;
	      }
	    ;
	    /*^compute */
	    /*_.IFCPP___V26*/ meltfptr[22] = /*_.IFELSE___V28*/ meltfptr[27];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-outobj.melt:1101:/ clear");
	       /*clear *//*_#STRBUF_USEDLENGTH__L13*/ meltfnum[11] = 0;
	    /*^clear */
	       /*clear *//*_.REFERENCED_VALUE__V27*/ meltfptr[23] = 0;
	    /*^clear */
	       /*clear *//*_#GET_INT__L14*/ meltfnum[10] = 0;
	    /*^clear */
	       /*clear *//*_#I__L15*/ meltfnum[14] = 0;
	    /*^clear */
	       /*clear *//*_.IFELSE___V28*/ meltfptr[27] = 0;
	  }

#else /*MELT_HAVE_DEBUG */
	  /*^cppif.else */
	  /*_.IFCPP___V26*/ meltfptr[22] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
	  ;
	  MELT_LOCATION ("warmelt-outobj.melt:1098:/ quasiblock");


	  /*_.PROGN___V29*/ meltfptr[23] = /*_.IFCPP___V26*/ meltfptr[22];;
	  /*^compute */
	  /*_.IFELSE___V21*/ meltfptr[20] = /*_.PROGN___V29*/ meltfptr[23];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-outobj.melt:1098:/ clear");
	     /*clear *//*_.IFCPP___V22*/ meltfptr[21] = 0;
	  /*^clear */
	     /*clear *//*_.IFCPP___V26*/ meltfptr[22] = 0;
	  /*^clear */
	     /*clear *//*_.PROGN___V29*/ meltfptr[23] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

  /*_.IFELSE___V21*/ meltfptr[20] = NULL;;
      }
    ;
    /*^compute */
    /*_.LET___V13*/ meltfptr[8] = /*_.IFELSE___V21*/ meltfptr[20];;

    MELT_LOCATION ("warmelt-outobj.melt:1070:/ clear");
	   /*clear *//*_.CNAM__V14*/ meltfptr[13] = 0;
    /*^clear */
	   /*clear *//*_.OLOCVAR__V15*/ meltfptr[14] = 0;
    /*^clear */
	   /*clear *//*_.ODATA__V16*/ meltfptr[15] = 0;
    /*^clear */
	   /*clear *//*_#DEPTHP1__L6*/ meltfnum[1] = 0;
    /*^clear */
	   /*clear *//*_.OIE_DISCR__V18*/ meltfptr[16] = 0;
    /*^clear */
	   /*clear *//*_.OUTPUT_C_CODE__V19*/ meltfptr[18] = 0;
    /*^clear */
	   /*clear *//*_#STRBUF_USEDLENGTH__L7*/ meltfnum[3] = 0;
    /*^clear */
	   /*clear *//*_.REFERENCED_VALUE__V20*/ meltfptr[19] = 0;
    /*^clear */
	   /*clear *//*_#GET_INT__L8*/ meltfnum[7] = 0;
    /*^clear */
	   /*clear *//*_#IRAW__L9*/ meltfnum[8] = 0;
    /*^clear */
	   /*clear *//*_#I__L10*/ meltfnum[9] = 0;
    /*^clear */
	   /*clear *//*_.IFELSE___V21*/ meltfptr[20] = 0;
    MELT_LOCATION ("warmelt-outobj.melt:1066:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.LET___V13*/ meltfptr[8];;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1066:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V5*/ meltfptr[4] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V7*/ meltfptr[5] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V11*/ meltfptr[7] = 0;
    /*^clear */
	   /*clear *//*_.LET___V13*/ meltfptr[8] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("OUTCINITFILL_OBJINITLIST", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_28_warmelt_outobj_OUTCINITFILL_OBJINITLIST_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_28_warmelt_outobj_OUTCINITFILL_OBJINITLIST */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_29_warmelt_outobj_OUTPUCOD_ANYDISCR (meltclosure_ptr_t meltclosp_,
					      melt_ptr_t meltfirstargp_,
					      const melt_argdescr_cell_t
					      meltxargdescr_[],
					      union meltparam_un *
					      meltxargtab_,
					      const melt_argdescr_cell_t
					      meltxresdescr_[],
					      union meltparam_un *
					      meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_29_warmelt_outobj_OUTPUCOD_ANYDISCR_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_29_warmelt_outobj_OUTPUCOD_ANYDISCR_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 17
    melt_ptr_t mcfr_varptr[17];
#define MELTFRAM_NBVARNUM 5
    long mcfr_varnum[5];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_29_warmelt_outobj_OUTPUCOD_ANYDISCR is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_29_warmelt_outobj_OUTPUCOD_ANYDISCR_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 17; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_29_warmelt_outobj_OUTPUCOD_ANYDISCR nbval 17*/
  meltfram__.mcfr_nbvar = 17 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("OUTPUCOD_ANYDISCR", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-outobj.melt:1108:/ getarg");
 /*_.ANY__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.DECLBUF__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.DECLBUF__V3*/ meltfptr[2])) !=
	      NULL);


  /*getarg#2 */
  /*^getarg */
  if (meltxargdescr_[1] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.IMPLBUF__V4*/ meltfptr[3] =
    (meltxargtab_[1].meltbp_aptr) ? (*(meltxargtab_[1].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3])) !=
	      NULL);


  /*getarg#3 */
  /*^getarg */
  if (meltxargdescr_[2] != MELTBPAR_LONG)
    goto lab_endgetargs;
 /*_#DEPTH__L1*/ meltfnum[0] = meltxargtab_[2].meltbp_long;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:1109:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L2*/ meltfnum[1] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-outobj.melt:1109:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L2*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L3*/ meltfnum[2] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-outobj.melt:1109:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L3*/ meltfnum[2];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-outobj.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 1109;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "outpucod_anydiscr any=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.ANY__V2*/ meltfptr[1];
	      /*_.MELT_DEBUG_FUN__V7*/ meltfptr[6] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V6*/ meltfptr[5] = /*_.MELT_DEBUG_FUN__V7*/ meltfptr[6];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-outobj.melt:1109:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L3*/ meltfnum[2] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V7*/ meltfptr[6] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V6*/ meltfptr[5] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-outobj.melt:1109:/ quasiblock");


      /*_.PROGN___V8*/ meltfptr[6] = /*_.IF___V6*/ meltfptr[5];;
      /*^compute */
      /*_.IFCPP___V5*/ meltfptr[4] = /*_.PROGN___V8*/ meltfptr[6];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:1109:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L2*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IF___V6*/ meltfptr[5] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V8*/ meltfptr[6] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V5*/ meltfptr[4] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1110:/ locexp");
      melt_puts (stderr,
		 ("* output_c_code unimplemented receiver discriminator "));
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:1111:/ quasiblock");


 /*_.DISCR__V10*/ meltfptr[6] =
      ((melt_ptr_t)
       (melt_discr ((melt_ptr_t) ( /*_.ANY__V2*/ meltfptr[1]))));;
    MELT_LOCATION ("warmelt-outobj.melt:1112:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.DISCR__V10*/ meltfptr[6]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 1, "NAMED_NAME");
  /*_.NAMED_NAME__V11*/ meltfptr[10] = slot;
    };
    ;

    {
      /*^locexp */
      melt_putstr (stderr,
		   (melt_ptr_t) ( /*_.NAMED_NAME__V11*/ meltfptr[10]));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1113:/ locexp");
      melt_newlineflush (stderr);
    }
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:1114:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L4*/ meltfnum[2] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-outobj.melt:1114:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L4*/ meltfnum[2])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L5*/ meltfnum[1] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-outobj.melt:1114:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L5*/ meltfnum[1];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-outobj.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 1114;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "outpucod_anydiscr discr=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.DISCR__V10*/ meltfptr[6];
	      /*_.MELT_DEBUG_FUN__V14*/ meltfptr[13] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V13*/ meltfptr[12] =
	      /*_.MELT_DEBUG_FUN__V14*/ meltfptr[13];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-outobj.melt:1114:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L5*/ meltfnum[1] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V14*/ meltfptr[13] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V13*/ meltfptr[12] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-outobj.melt:1114:/ quasiblock");


      /*_.PROGN___V15*/ meltfptr[13] = /*_.IF___V13*/ meltfptr[12];;
      /*^compute */
      /*_.IFCPP___V12*/ meltfptr[11] = /*_.PROGN___V15*/ meltfptr[13];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:1114:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L4*/ meltfnum[2] = 0;
      /*^clear */
	     /*clear *//*_.IF___V13*/ meltfptr[12] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V15*/ meltfptr[13] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V12*/ meltfptr[11] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    /*^compute */
    /*_.LET___V9*/ meltfptr[5] = /*_.IFCPP___V12*/ meltfptr[11];;

    MELT_LOCATION ("warmelt-outobj.melt:1111:/ clear");
	   /*clear *//*_.DISCR__V10*/ meltfptr[6] = 0;
    /*^clear */
	   /*clear *//*_.NAMED_NAME__V11*/ meltfptr[10] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V12*/ meltfptr[11] = 0;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:1115:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
      /*^cond */
      /*cond */ if (( /*nil */ NULL))	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V17*/ meltfptr[13] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-outobj.melt:1115:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("@@ outpucod_anydiscr not able to output"),
				  ("warmelt-outobj.melt")
				  ? ("warmelt-outobj.melt") : __FILE__,
				  (1115) ? (1115) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V17*/ meltfptr[13] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V16*/ meltfptr[12] = /*_.IFELSE___V17*/ meltfptr[13];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:1115:/ clear");
	     /*clear *//*_.IFELSE___V17*/ meltfptr[13] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V16*/ meltfptr[12] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-outobj.melt:1108:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.IFCPP___V16*/ meltfptr[12];;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1108:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V5*/ meltfptr[4] = 0;
    /*^clear */
	   /*clear *//*_.LET___V9*/ meltfptr[5] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V16*/ meltfptr[12] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("OUTPUCOD_ANYDISCR", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_29_warmelt_outobj_OUTPUCOD_ANYDISCR_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_29_warmelt_outobj_OUTPUCOD_ANYDISCR */



/**** end of warmelt-outobj+01.c ****/
