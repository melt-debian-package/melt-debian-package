/* GCC MELT GENERATED FILE warmelt-macro+04.c - DO NOT EDIT */
/* secondary MELT generated C file of rank #4 */
#include "melt-run.h"


/* used hash from melt-run.h when compiling this file: */
MELT_EXTERN const char meltrun_used_md5_melt_f4[] =
  MELT_RUN_HASHMD5 /* from melt-run.h */ ;


/**** warmelt-macro+04.c declarations ****/


/***************************************************
***
    Copyright 2008 - 2012 Free Software Foundation, Inc.
    Contributed by Basile Starynkevitch <basile@starynkevitch.net>

    This file is part of GCC.

    GCC is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3, or (at your option)
    any later version.

    GCC is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with GCC; see the file COPYING3.  If not see
    <http://www.gnu.org/licenses/>.
***
****************************************************/

/* ordinary MELT module */
#define MELT_HAS_INITIAL_ENVIRONMENT 1	/*usual */

struct melt_callframe_st;	/*defined in melt-runtime.h */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_1_warmelt_macro_S_EXPR_WEIGHT (meltclosure_ptr_t meltclosp_,
					melt_ptr_t meltfirstargp_,
					const melt_argdescr_cell_t
					meltxargdescr_[],
					union meltparam_un *meltxargtab_,
					const melt_argdescr_cell_t
					meltxresdescr_[],
					union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_2_warmelt_macro_LAMBDA___1__ (meltclosure_ptr_t meltclosp_,
				       melt_ptr_t meltfirstargp_,
				       const melt_argdescr_cell_t
				       meltxargdescr_[],
				       union meltparam_un *meltxargtab_,
				       const melt_argdescr_cell_t
				       meltxresdescr_[],
				       union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_3_warmelt_macro_EXPAND_RESTLIST_AS_LIST (meltclosure_ptr_t
						  meltclosp_,
						  melt_ptr_t meltfirstargp_,
						  const melt_argdescr_cell_t
						  meltxargdescr_[],
						  union meltparam_un
						  *meltxargtab_,
						  const melt_argdescr_cell_t
						  meltxresdescr_[],
						  union meltparam_un
						  *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_4_warmelt_macro_EXPAND_RESTLIST_AS_TUPLE (meltclosure_ptr_t
						   meltclosp_,
						   melt_ptr_t meltfirstargp_,
						   const melt_argdescr_cell_t
						   meltxargdescr_[],
						   union meltparam_un
						   *meltxargtab_,
						   const melt_argdescr_cell_t
						   meltxresdescr_[],
						   union meltparam_un
						   *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_5_warmelt_macro_EXPAND_PAIRLIST_AS_LIST (meltclosure_ptr_t
						  meltclosp_,
						  melt_ptr_t meltfirstargp_,
						  const melt_argdescr_cell_t
						  meltxargdescr_[],
						  union meltparam_un
						  *meltxargtab_,
						  const melt_argdescr_cell_t
						  meltxresdescr_[],
						  union meltparam_un
						  *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_6_warmelt_macro_EXPAND_PAIRLIST_AS_TUPLE (meltclosure_ptr_t
						   meltclosp_,
						   melt_ptr_t meltfirstargp_,
						   const melt_argdescr_cell_t
						   meltxargdescr_[],
						   union meltparam_un
						   *meltxargtab_,
						   const melt_argdescr_cell_t
						   meltxresdescr_[],
						   union meltparam_un
						   *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_7_warmelt_macro_REGISTER_GENERATOR_DEVICE (meltclosure_ptr_t
						    meltclosp_,
						    melt_ptr_t meltfirstargp_,
						    const melt_argdescr_cell_t
						    meltxargdescr_[],
						    union meltparam_un
						    *meltxargtab_,
						    const melt_argdescr_cell_t
						    meltxresdescr_[],
						    union meltparam_un
						    *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_8_warmelt_macro_EXPAND_APPLY (meltclosure_ptr_t meltclosp_,
				       melt_ptr_t meltfirstargp_,
				       const melt_argdescr_cell_t
				       meltxargdescr_[],
				       union meltparam_un *meltxargtab_,
				       const melt_argdescr_cell_t
				       meltxresdescr_[],
				       union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_9_warmelt_macro_EXPAND_MSEND (meltclosure_ptr_t meltclosp_,
				       melt_ptr_t meltfirstargp_,
				       const melt_argdescr_cell_t
				       meltxargdescr_[],
				       union meltparam_un *meltxargtab_,
				       const melt_argdescr_cell_t
				       meltxresdescr_[],
				       union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_10_warmelt_macro_EXPAND_FIELDEXPR (meltclosure_ptr_t meltclosp_,
					    melt_ptr_t meltfirstargp_,
					    const melt_argdescr_cell_t
					    meltxargdescr_[],
					    union meltparam_un *meltxargtab_,
					    const melt_argdescr_cell_t
					    meltxresdescr_[],
					    union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_11_warmelt_macro_EXPAND_CITERATION (meltclosure_ptr_t meltclosp_,
					     melt_ptr_t meltfirstargp_,
					     const melt_argdescr_cell_t
					     meltxargdescr_[],
					     union meltparam_un *meltxargtab_,
					     const melt_argdescr_cell_t
					     meltxresdescr_[],
					     union meltparam_un
					     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_12_warmelt_macro_LAMBDA___2__ (meltclosure_ptr_t meltclosp_,
					melt_ptr_t meltfirstargp_,
					const melt_argdescr_cell_t
					meltxargdescr_[],
					union meltparam_un *meltxargtab_,
					const melt_argdescr_cell_t
					meltxresdescr_[],
					union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_13_warmelt_macro_EXPAND_CMATCHEXPR (meltclosure_ptr_t meltclosp_,
					     melt_ptr_t meltfirstargp_,
					     const melt_argdescr_cell_t
					     meltxargdescr_[],
					     union meltparam_un *meltxargtab_,
					     const melt_argdescr_cell_t
					     meltxresdescr_[],
					     union meltparam_un
					     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_14_warmelt_macro_EXPAND_FUNMATCHEXPR (meltclosure_ptr_t meltclosp_,
					       melt_ptr_t meltfirstargp_,
					       const melt_argdescr_cell_t
					       meltxargdescr_[],
					       union meltparam_un
					       *meltxargtab_,
					       const melt_argdescr_cell_t
					       meltxresdescr_[],
					       union meltparam_un
					       *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_15_warmelt_macro_EXPAND_KEYWORDFUN (meltclosure_ptr_t meltclosp_,
					     melt_ptr_t meltfirstargp_,
					     const melt_argdescr_cell_t
					     meltxargdescr_[],
					     union meltparam_un *meltxargtab_,
					     const melt_argdescr_cell_t
					     meltxresdescr_[],
					     union meltparam_un
					     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_16_warmelt_macro_MACROEXPAND_1 (meltclosure_ptr_t meltclosp_,
					 melt_ptr_t meltfirstargp_,
					 const melt_argdescr_cell_t
					 meltxargdescr_[],
					 union meltparam_un *meltxargtab_,
					 const melt_argdescr_cell_t
					 meltxresdescr_[],
					 union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_17_warmelt_macro_LAMBDA___3__ (meltclosure_ptr_t meltclosp_,
					melt_ptr_t meltfirstargp_,
					const melt_argdescr_cell_t
					meltxargdescr_[],
					union meltparam_un *meltxargtab_,
					const melt_argdescr_cell_t
					meltxresdescr_[],
					union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_18_warmelt_macro_EXPAND_PRIMITIVE (meltclosure_ptr_t meltclosp_,
					    melt_ptr_t meltfirstargp_,
					    const melt_argdescr_cell_t
					    meltxargdescr_[],
					    union meltparam_un *meltxargtab_,
					    const melt_argdescr_cell_t
					    meltxresdescr_[],
					    union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_19_warmelt_macro_PATTERNEXPAND_PAIRLIST_AS_TUPLE (meltclosure_ptr_t
							   meltclosp_,
							   melt_ptr_t
							   meltfirstargp_,
							   const
							   melt_argdescr_cell_t
							   meltxargdescr_[],
							   union meltparam_un
							   *meltxargtab_,
							   const
							   melt_argdescr_cell_t
							   meltxresdescr_[],
							   union meltparam_un
							   *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_20_warmelt_macro_LAMBDA___4__ (meltclosure_ptr_t meltclosp_,
					melt_ptr_t meltfirstargp_,
					const melt_argdescr_cell_t
					meltxargdescr_[],
					union meltparam_un *meltxargtab_,
					const melt_argdescr_cell_t
					meltxresdescr_[],
					union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_21_warmelt_macro_PATMACEXPAND_FOR_MATCHER (meltclosure_ptr_t
						    meltclosp_,
						    melt_ptr_t meltfirstargp_,
						    const melt_argdescr_cell_t
						    meltxargdescr_[],
						    union meltparam_un
						    *meltxargtab_,
						    const melt_argdescr_cell_t
						    meltxresdescr_[],
						    union meltparam_un
						    *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_22_warmelt_macro_LAMBDA___5__ (meltclosure_ptr_t meltclosp_,
					melt_ptr_t meltfirstargp_,
					const melt_argdescr_cell_t
					meltxargdescr_[],
					union meltparam_un *meltxargtab_,
					const melt_argdescr_cell_t
					meltxresdescr_[],
					union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_23_warmelt_macro_LAMBDA___6__ (meltclosure_ptr_t meltclosp_,
					melt_ptr_t meltfirstargp_,
					const melt_argdescr_cell_t
					meltxargdescr_[],
					union meltparam_un *meltxargtab_,
					const melt_argdescr_cell_t
					meltxresdescr_[],
					union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_24_warmelt_macro_PATTERN_WEIGHT_TUPLE (meltclosure_ptr_t meltclosp_,
						melt_ptr_t meltfirstargp_,
						const melt_argdescr_cell_t
						meltxargdescr_[],
						union meltparam_un
						*meltxargtab_,
						const melt_argdescr_cell_t
						meltxresdescr_[],
						union meltparam_un
						*meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_25_warmelt_macro_LAMBDA___7__ (meltclosure_ptr_t meltclosp_,
					melt_ptr_t meltfirstargp_,
					const melt_argdescr_cell_t
					meltxargdescr_[],
					union meltparam_un *meltxargtab_,
					const melt_argdescr_cell_t
					meltxresdescr_[],
					union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_26_warmelt_macro_PATTERNEXPAND_EXPR (meltclosure_ptr_t meltclosp_,
					      melt_ptr_t meltfirstargp_,
					      const melt_argdescr_cell_t
					      meltxargdescr_[],
					      union meltparam_un
					      *meltxargtab_,
					      const melt_argdescr_cell_t
					      meltxresdescr_[],
					      union meltparam_un
					      *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_27_warmelt_macro_PATTERNEXPAND_1 (meltclosure_ptr_t meltclosp_,
					   melt_ptr_t meltfirstargp_,
					   const melt_argdescr_cell_t
					   meltxargdescr_[],
					   union meltparam_un *meltxargtab_,
					   const melt_argdescr_cell_t
					   meltxresdescr_[],
					   union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_28_warmelt_macro_MACROEXPAND_TOPLEVEL_LIST (meltclosure_ptr_t
						     meltclosp_,
						     melt_ptr_t
						     meltfirstargp_,
						     const
						     melt_argdescr_cell_t
						     meltxargdescr_[],
						     union meltparam_un
						     *meltxargtab_,
						     const
						     melt_argdescr_cell_t
						     meltxresdescr_[],
						     union meltparam_un
						     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_29_warmelt_macro_LAMBDA_ARG_BINDINGS (meltclosure_ptr_t meltclosp_,
					       melt_ptr_t meltfirstargp_,
					       const melt_argdescr_cell_t
					       meltxargdescr_[],
					       union meltparam_un
					       *meltxargtab_,
					       const melt_argdescr_cell_t
					       meltxresdescr_[],
					       union meltparam_un
					       *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_30_warmelt_macro_INSTALL_INITIAL_MACRO (meltclosure_ptr_t meltclosp_,
						 melt_ptr_t meltfirstargp_,
						 const melt_argdescr_cell_t
						 meltxargdescr_[],
						 union meltparam_un
						 *meltxargtab_,
						 const melt_argdescr_cell_t
						 meltxresdescr_[],
						 union meltparam_un
						 *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_31_warmelt_macro_INSTALL_INITIAL_PATMACRO (meltclosure_ptr_t
						    meltclosp_,
						    melt_ptr_t meltfirstargp_,
						    const melt_argdescr_cell_t
						    meltxargdescr_[],
						    union meltparam_un
						    *meltxargtab_,
						    const melt_argdescr_cell_t
						    meltxresdescr_[],
						    union meltparam_un
						    *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_32_warmelt_macro_WARN_IF_REDEFINED (meltclosure_ptr_t meltclosp_,
					     melt_ptr_t meltfirstargp_,
					     const melt_argdescr_cell_t
					     meltxargdescr_[],
					     union meltparam_un *meltxargtab_,
					     const melt_argdescr_cell_t
					     meltxresdescr_[],
					     union meltparam_un
					     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_33_warmelt_macro_FLATTEN_FOR_C_CODE_EXPANSION (meltclosure_ptr_t
							meltclosp_,
							melt_ptr_t
							meltfirstargp_,
							const
							melt_argdescr_cell_t
							meltxargdescr_[],
							union meltparam_un
							*meltxargtab_,
							const
							melt_argdescr_cell_t
							meltxresdescr_[],
							union meltparam_un
							*meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_34_warmelt_macro_PARSE_PAIRLIST_C_CODE_EXPANSION (meltclosure_ptr_t
							   meltclosp_,
							   melt_ptr_t
							   meltfirstargp_,
							   const
							   melt_argdescr_cell_t
							   meltxargdescr_[],
							   union meltparam_un
							   *meltxargtab_,
							   const
							   melt_argdescr_cell_t
							   meltxresdescr_[],
							   union meltparam_un
							   *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_35_warmelt_macro_CHECK_C_EXPANSION (meltclosure_ptr_t meltclosp_,
					     melt_ptr_t meltfirstargp_,
					     const melt_argdescr_cell_t
					     meltxargdescr_[],
					     union meltparam_un *meltxargtab_,
					     const melt_argdescr_cell_t
					     meltxresdescr_[],
					     union meltparam_un
					     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_36_warmelt_macro_MEXPAND_DEFPRIMITIVE (meltclosure_ptr_t meltclosp_,
						melt_ptr_t meltfirstargp_,
						const melt_argdescr_cell_t
						meltxargdescr_[],
						union meltparam_un
						*meltxargtab_,
						const melt_argdescr_cell_t
						meltxresdescr_[],
						union meltparam_un
						*meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_37_warmelt_macro_MEXPAND_DEFCITERATOR (meltclosure_ptr_t meltclosp_,
						melt_ptr_t meltfirstargp_,
						const melt_argdescr_cell_t
						meltxargdescr_[],
						union meltparam_un
						*meltxargtab_,
						const melt_argdescr_cell_t
						meltxresdescr_[],
						union meltparam_un
						*meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_38_warmelt_macro_MEXPAND_DEFCMATCHER (meltclosure_ptr_t meltclosp_,
					       melt_ptr_t meltfirstargp_,
					       const melt_argdescr_cell_t
					       meltxargdescr_[],
					       union meltparam_un
					       *meltxargtab_,
					       const melt_argdescr_cell_t
					       meltxresdescr_[],
					       union meltparam_un
					       *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_39_warmelt_macro_LAMBDA___8__ (meltclosure_ptr_t meltclosp_,
					melt_ptr_t meltfirstargp_,
					const melt_argdescr_cell_t
					meltxargdescr_[],
					union meltparam_un *meltxargtab_,
					const melt_argdescr_cell_t
					meltxresdescr_[],
					union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_40_warmelt_macro_MEXPAND_DEFUNMATCHER (meltclosure_ptr_t meltclosp_,
						melt_ptr_t meltfirstargp_,
						const melt_argdescr_cell_t
						meltxargdescr_[],
						union meltparam_un
						*meltxargtab_,
						const melt_argdescr_cell_t
						meltxresdescr_[],
						union meltparam_un
						*meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_41_warmelt_macro_LAMBDA___9__ (meltclosure_ptr_t meltclosp_,
					melt_ptr_t meltfirstargp_,
					const melt_argdescr_cell_t
					meltxargdescr_[],
					union meltparam_un *meltxargtab_,
					const melt_argdescr_cell_t
					meltxresdescr_[],
					union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_42_warmelt_macro_MEXPAND_DEFUN (meltclosure_ptr_t meltclosp_,
					 melt_ptr_t meltfirstargp_,
					 const melt_argdescr_cell_t
					 meltxargdescr_[],
					 union meltparam_un *meltxargtab_,
					 const melt_argdescr_cell_t
					 meltxresdescr_[],
					 union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_43_warmelt_macro_LAMBDA___10__ (meltclosure_ptr_t meltclosp_,
					 melt_ptr_t meltfirstargp_,
					 const melt_argdescr_cell_t
					 meltxargdescr_[],
					 union meltparam_un *meltxargtab_,
					 const melt_argdescr_cell_t
					 meltxresdescr_[],
					 union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_44_warmelt_macro_MEXPAND_DEFINE (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_45_warmelt_macro_LAMBDA___11__ (meltclosure_ptr_t meltclosp_,
					 melt_ptr_t meltfirstargp_,
					 const melt_argdescr_cell_t
					 meltxargdescr_[],
					 union meltparam_un *meltxargtab_,
					 const melt_argdescr_cell_t
					 meltxresdescr_[],
					 union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_46_warmelt_macro_SCAN_DEFCLASS (meltclosure_ptr_t meltclosp_,
					 melt_ptr_t meltfirstargp_,
					 const melt_argdescr_cell_t
					 meltxargdescr_[],
					 union meltparam_un *meltxargtab_,
					 const melt_argdescr_cell_t
					 meltxresdescr_[],
					 union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_47_warmelt_macro_MEXPAND_DEFCLASS (meltclosure_ptr_t meltclosp_,
					    melt_ptr_t meltfirstargp_,
					    const melt_argdescr_cell_t
					    meltxargdescr_[],
					    union meltparam_un *meltxargtab_,
					    const melt_argdescr_cell_t
					    meltxresdescr_[],
					    union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_48_warmelt_macro_LAMBDA___12__ (meltclosure_ptr_t meltclosp_,
					 melt_ptr_t meltfirstargp_,
					 const melt_argdescr_cell_t
					 meltxargdescr_[],
					 union meltparam_un *meltxargtab_,
					 const melt_argdescr_cell_t
					 meltxresdescr_[],
					 union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_49_warmelt_macro_LAMBDA___13__ (meltclosure_ptr_t meltclosp_,
					 melt_ptr_t meltfirstargp_,
					 const melt_argdescr_cell_t
					 meltxargdescr_[],
					 union meltparam_un *meltxargtab_,
					 const melt_argdescr_cell_t
					 meltxresdescr_[],
					 union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_50_warmelt_macro_PARSE_FIELD_ASSIGNMENT (meltclosure_ptr_t
						  meltclosp_,
						  melt_ptr_t meltfirstargp_,
						  const melt_argdescr_cell_t
						  meltxargdescr_[],
						  union meltparam_un
						  *meltxargtab_,
						  const melt_argdescr_cell_t
						  meltxresdescr_[],
						  union meltparam_un
						  *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_51_warmelt_macro_LAMBDA___14__ (meltclosure_ptr_t meltclosp_,
					 melt_ptr_t meltfirstargp_,
					 const melt_argdescr_cell_t
					 meltxargdescr_[],
					 union meltparam_un *meltxargtab_,
					 const melt_argdescr_cell_t
					 meltxresdescr_[],
					 union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_52_warmelt_macro_MEXPAND_DEFINSTANCE (meltclosure_ptr_t meltclosp_,
					       melt_ptr_t meltfirstargp_,
					       const melt_argdescr_cell_t
					       meltxargdescr_[],
					       union meltparam_un
					       *meltxargtab_,
					       const melt_argdescr_cell_t
					       meltxresdescr_[],
					       union meltparam_un
					       *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_53_warmelt_macro_MEXPAND_DEFSELECTOR (meltclosure_ptr_t meltclosp_,
					       melt_ptr_t meltfirstargp_,
					       const melt_argdescr_cell_t
					       meltxargdescr_[],
					       union meltparam_un
					       *meltxargtab_,
					       const melt_argdescr_cell_t
					       meltxresdescr_[],
					       union meltparam_un
					       *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_54_warmelt_macro_MEXPAND_INSTANCE (meltclosure_ptr_t meltclosp_,
					    melt_ptr_t meltfirstargp_,
					    const melt_argdescr_cell_t
					    meltxargdescr_[],
					    union meltparam_un *meltxargtab_,
					    const melt_argdescr_cell_t
					    meltxresdescr_[],
					    union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_55_warmelt_macro_MEXPAND_LOAD (meltclosure_ptr_t meltclosp_,
					melt_ptr_t meltfirstargp_,
					const melt_argdescr_cell_t
					meltxargdescr_[],
					union meltparam_un *meltxargtab_,
					const melt_argdescr_cell_t
					meltxresdescr_[],
					union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_56_warmelt_macro_PARSE_FIELD_PATTERN (meltclosure_ptr_t meltclosp_,
					       melt_ptr_t meltfirstargp_,
					       const melt_argdescr_cell_t
					       meltxargdescr_[],
					       union meltparam_un
					       *meltxargtab_,
					       const melt_argdescr_cell_t
					       meltxresdescr_[],
					       union meltparam_un
					       *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_57_warmelt_macro_LAMBDA___15__ (meltclosure_ptr_t meltclosp_,
					 melt_ptr_t meltfirstargp_,
					 const melt_argdescr_cell_t
					 meltxargdescr_[],
					 union meltparam_un *meltxargtab_,
					 const melt_argdescr_cell_t
					 meltxresdescr_[],
					 union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_58_warmelt_macro_PATEXPAND_INSTANCE (meltclosure_ptr_t meltclosp_,
					      melt_ptr_t meltfirstargp_,
					      const melt_argdescr_cell_t
					      meltxargdescr_[],
					      union meltparam_un
					      *meltxargtab_,
					      const melt_argdescr_cell_t
					      meltxresdescr_[],
					      union meltparam_un
					      *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_59_warmelt_macro_LAMBDA___16__ (meltclosure_ptr_t meltclosp_,
					 melt_ptr_t meltfirstargp_,
					 const melt_argdescr_cell_t
					 meltxargdescr_[],
					 union meltparam_un *meltxargtab_,
					 const melt_argdescr_cell_t
					 meltxresdescr_[],
					 union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_60_warmelt_macro_PATEXPAND_OBJECT (meltclosure_ptr_t meltclosp_,
					    melt_ptr_t meltfirstargp_,
					    const melt_argdescr_cell_t
					    meltxargdescr_[],
					    union meltparam_un *meltxargtab_,
					    const melt_argdescr_cell_t
					    meltxresdescr_[],
					    union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_61_warmelt_macro_LAMBDA___17__ (meltclosure_ptr_t meltclosp_,
					 melt_ptr_t meltfirstargp_,
					 const melt_argdescr_cell_t
					 meltxargdescr_[],
					 union meltparam_un *meltxargtab_,
					 const melt_argdescr_cell_t
					 meltxresdescr_[],
					 union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_62_warmelt_macro_MEXPAND_OBJECT (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_63_warmelt_macro_MEXPAND_CODE_CHUNK (meltclosure_ptr_t meltclosp_,
					      melt_ptr_t meltfirstargp_,
					      const melt_argdescr_cell_t
					      meltxargdescr_[],
					      union meltparam_un
					      *meltxargtab_,
					      const melt_argdescr_cell_t
					      meltxresdescr_[],
					      union meltparam_un
					      *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_64_warmelt_macro_MEXPAND_UNSAFE_PUT_FIELDS (meltclosure_ptr_t
						     meltclosp_,
						     melt_ptr_t
						     meltfirstargp_,
						     const
						     melt_argdescr_cell_t
						     meltxargdescr_[],
						     union meltparam_un
						     *meltxargtab_,
						     const
						     melt_argdescr_cell_t
						     meltxresdescr_[],
						     union meltparam_un
						     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_65_warmelt_macro_MEXPAND_PUT_FIELDS (meltclosure_ptr_t meltclosp_,
					      melt_ptr_t meltfirstargp_,
					      const melt_argdescr_cell_t
					      meltxargdescr_[],
					      union meltparam_un
					      *meltxargtab_,
					      const melt_argdescr_cell_t
					      meltxresdescr_[],
					      union meltparam_un
					      *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_66_warmelt_macro_MEXPAND_UNSAFE_GET_FIELD (meltclosure_ptr_t
						    meltclosp_,
						    melt_ptr_t meltfirstargp_,
						    const melt_argdescr_cell_t
						    meltxargdescr_[],
						    union meltparam_un
						    *meltxargtab_,
						    const melt_argdescr_cell_t
						    meltxresdescr_[],
						    union meltparam_un
						    *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_67_warmelt_macro_MEXPAND_GET_FIELD (meltclosure_ptr_t meltclosp_,
					     melt_ptr_t meltfirstargp_,
					     const melt_argdescr_cell_t
					     meltxargdescr_[],
					     union meltparam_un *meltxargtab_,
					     const melt_argdescr_cell_t
					     meltxresdescr_[],
					     union meltparam_un
					     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_68_warmelt_macro_PAIRLIST_TO_PROGN (meltclosure_ptr_t meltclosp_,
					     melt_ptr_t meltfirstargp_,
					     const melt_argdescr_cell_t
					     meltxargdescr_[],
					     union meltparam_un *meltxargtab_,
					     const melt_argdescr_cell_t
					     meltxresdescr_[],
					     union meltparam_un
					     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_69_warmelt_macro_LAMBDA___18__ (meltclosure_ptr_t meltclosp_,
					 melt_ptr_t meltfirstargp_,
					 const melt_argdescr_cell_t
					 meltxargdescr_[],
					 union meltparam_un *meltxargtab_,
					 const melt_argdescr_cell_t
					 meltxresdescr_[],
					 union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_70_warmelt_macro_MEXPAND_SETQ (meltclosure_ptr_t meltclosp_,
					melt_ptr_t meltfirstargp_,
					const melt_argdescr_cell_t
					meltxargdescr_[],
					union meltparam_un *meltxargtab_,
					const melt_argdescr_cell_t
					meltxresdescr_[],
					union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_71_warmelt_macro_MEXPAND_IF (meltclosure_ptr_t meltclosp_,
				      melt_ptr_t meltfirstargp_,
				      const melt_argdescr_cell_t
				      meltxargdescr_[],
				      union meltparam_un *meltxargtab_,
				      const melt_argdescr_cell_t
				      meltxresdescr_[],
				      union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_72_warmelt_macro_MEXPAND_WHEN (meltclosure_ptr_t meltclosp_,
					melt_ptr_t meltfirstargp_,
					const melt_argdescr_cell_t
					meltxargdescr_[],
					union meltparam_un *meltxargtab_,
					const melt_argdescr_cell_t
					meltxresdescr_[],
					union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_73_warmelt_macro_MEXPAND_UNLESS (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_74_warmelt_macro_MEXPAND_CPPIF (meltclosure_ptr_t meltclosp_,
					 melt_ptr_t meltfirstargp_,
					 const melt_argdescr_cell_t
					 meltxargdescr_[],
					 union meltparam_un *meltxargtab_,
					 const melt_argdescr_cell_t
					 meltxresdescr_[],
					 union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_75_warmelt_macro_FILTERGCCVERSION (meltclosure_ptr_t meltclosp_,
					    melt_ptr_t meltfirstargp_,
					    const melt_argdescr_cell_t
					    meltxargdescr_[],
					    union meltparam_un *meltxargtab_,
					    const melt_argdescr_cell_t
					    meltxresdescr_[],
					    union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_76_warmelt_macro_MEXPAND_GCCIF (meltclosure_ptr_t meltclosp_,
					 melt_ptr_t meltfirstargp_,
					 const melt_argdescr_cell_t
					 meltxargdescr_[],
					 union meltparam_un *meltxargtab_,
					 const melt_argdescr_cell_t
					 meltxresdescr_[],
					 union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_77_warmelt_macro_MEXPAND_COND (meltclosure_ptr_t meltclosp_,
					melt_ptr_t meltfirstargp_,
					const melt_argdescr_cell_t
					meltxargdescr_[],
					union meltparam_un *meltxargtab_,
					const melt_argdescr_cell_t
					meltxresdescr_[],
					union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_78_warmelt_macro_LAMBDA___19__ (meltclosure_ptr_t meltclosp_,
					 melt_ptr_t meltfirstargp_,
					 const melt_argdescr_cell_t
					 meltxargdescr_[],
					 union meltparam_un *meltxargtab_,
					 const melt_argdescr_cell_t
					 meltxresdescr_[],
					 union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_79_warmelt_macro_MEXPAND_AND (meltclosure_ptr_t meltclosp_,
				       melt_ptr_t meltfirstargp_,
				       const melt_argdescr_cell_t
				       meltxargdescr_[],
				       union meltparam_un *meltxargtab_,
				       const melt_argdescr_cell_t
				       meltxresdescr_[],
				       union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_80_warmelt_macro_LAMBDA___20__ (meltclosure_ptr_t meltclosp_,
					 melt_ptr_t meltfirstargp_,
					 const melt_argdescr_cell_t
					 meltxargdescr_[],
					 union meltparam_un *meltxargtab_,
					 const melt_argdescr_cell_t
					 meltxresdescr_[],
					 union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_81_warmelt_macro_PATEXPAND_AS (meltclosure_ptr_t meltclosp_,
					melt_ptr_t meltfirstargp_,
					const melt_argdescr_cell_t
					meltxargdescr_[],
					union meltparam_un *meltxargtab_,
					const melt_argdescr_cell_t
					meltxresdescr_[],
					union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_82_warmelt_macro_MEXPAND_AS (meltclosure_ptr_t meltclosp_,
				      melt_ptr_t meltfirstargp_,
				      const melt_argdescr_cell_t
				      meltxargdescr_[],
				      union meltparam_un *meltxargtab_,
				      const melt_argdescr_cell_t
				      meltxresdescr_[],
				      union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_83_warmelt_macro_PATEXPAND_WHEN (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_84_warmelt_macro_MEXPAND_WHEN (meltclosure_ptr_t meltclosp_,
					melt_ptr_t meltfirstargp_,
					const melt_argdescr_cell_t
					meltxargdescr_[],
					union meltparam_un *meltxargtab_,
					const melt_argdescr_cell_t
					meltxresdescr_[],
					union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_85_warmelt_macro_PATEXPAND_AND (meltclosure_ptr_t meltclosp_,
					 melt_ptr_t meltfirstargp_,
					 const melt_argdescr_cell_t
					 meltxargdescr_[],
					 union meltparam_un *meltxargtab_,
					 const melt_argdescr_cell_t
					 meltxresdescr_[],
					 union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_86_warmelt_macro_MEXPAND_OR (meltclosure_ptr_t meltclosp_,
				      melt_ptr_t meltfirstargp_,
				      const melt_argdescr_cell_t
				      meltxargdescr_[],
				      union meltparam_un *meltxargtab_,
				      const melt_argdescr_cell_t
				      meltxresdescr_[],
				      union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_87_warmelt_macro_LAMBDA___21__ (meltclosure_ptr_t meltclosp_,
					 melt_ptr_t meltfirstargp_,
					 const melt_argdescr_cell_t
					 meltxargdescr_[],
					 union meltparam_un *meltxargtab_,
					 const melt_argdescr_cell_t
					 meltxresdescr_[],
					 union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_88_warmelt_macro_PATEXPAND_OR (meltclosure_ptr_t meltclosp_,
					melt_ptr_t meltfirstargp_,
					const melt_argdescr_cell_t
					meltxargdescr_[],
					union meltparam_un *meltxargtab_,
					const melt_argdescr_cell_t
					meltxresdescr_[],
					union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_89_warmelt_macro_MEXPAND_REFERENCE (meltclosure_ptr_t meltclosp_,
					     melt_ptr_t meltfirstargp_,
					     const melt_argdescr_cell_t
					     meltxargdescr_[],
					     union meltparam_un *meltxargtab_,
					     const melt_argdescr_cell_t
					     meltxresdescr_[],
					     union meltparam_un
					     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_90_warmelt_macro_PATEXPAND_REFERENCE (meltclosure_ptr_t meltclosp_,
					       melt_ptr_t meltfirstargp_,
					       const melt_argdescr_cell_t
					       meltxargdescr_[],
					       union meltparam_un
					       *meltxargtab_,
					       const melt_argdescr_cell_t
					       meltxresdescr_[],
					       union meltparam_un
					       *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_91_warmelt_macro_MEXPANDOBSOLETE_CONTAINER (meltclosure_ptr_t
						     meltclosp_,
						     melt_ptr_t
						     meltfirstargp_,
						     const
						     melt_argdescr_cell_t
						     meltxargdescr_[],
						     union meltparam_un
						     *meltxargtab_,
						     const
						     melt_argdescr_cell_t
						     meltxresdescr_[],
						     union meltparam_un
						     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_92_warmelt_macro_PATEXPANDOBSOLETE_CONTAINER (meltclosure_ptr_t
						       meltclosp_,
						       melt_ptr_t
						       meltfirstargp_,
						       const
						       melt_argdescr_cell_t
						       meltxargdescr_[],
						       union meltparam_un
						       *meltxargtab_,
						       const
						       melt_argdescr_cell_t
						       meltxresdescr_[],
						       union meltparam_un
						       *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_93_warmelt_macro_MEXPAND_DEREF (meltclosure_ptr_t meltclosp_,
					 melt_ptr_t meltfirstargp_,
					 const melt_argdescr_cell_t
					 meltxargdescr_[],
					 union meltparam_un *meltxargtab_,
					 const melt_argdescr_cell_t
					 meltxresdescr_[],
					 union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_94_warmelt_macro_MEXPANDOBSOLETE_CONTENT (meltclosure_ptr_t
						   meltclosp_,
						   melt_ptr_t meltfirstargp_,
						   const melt_argdescr_cell_t
						   meltxargdescr_[],
						   union meltparam_un
						   *meltxargtab_,
						   const melt_argdescr_cell_t
						   meltxresdescr_[],
						   union meltparam_un
						   *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_95_warmelt_macro_MEXPAND_SET_REF (meltclosure_ptr_t meltclosp_,
					   melt_ptr_t meltfirstargp_,
					   const melt_argdescr_cell_t
					   meltxargdescr_[],
					   union meltparam_un *meltxargtab_,
					   const melt_argdescr_cell_t
					   meltxresdescr_[],
					   union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_96_warmelt_macro_MEXPAND_TUPLE (meltclosure_ptr_t meltclosp_,
					 melt_ptr_t meltfirstargp_,
					 const melt_argdescr_cell_t
					 meltxargdescr_[],
					 union meltparam_un *meltxargtab_,
					 const melt_argdescr_cell_t
					 meltxresdescr_[],
					 union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_97_warmelt_macro_PATEXPAND_TUPLE (meltclosure_ptr_t meltclosp_,
					   melt_ptr_t meltfirstargp_,
					   const melt_argdescr_cell_t
					   meltxargdescr_[],
					   union meltparam_un *meltxargtab_,
					   const melt_argdescr_cell_t
					   meltxresdescr_[],
					   union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_98_warmelt_macro_MEXPAND_LIST (meltclosure_ptr_t meltclosp_,
					melt_ptr_t meltfirstargp_,
					const melt_argdescr_cell_t
					meltxargdescr_[],
					union meltparam_un *meltxargtab_,
					const melt_argdescr_cell_t
					meltxresdescr_[],
					union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_99_warmelt_macro_PATEXPAND_LIST (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_100_warmelt_macro_MEXPAND_MATCH (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_101_warmelt_macro_LAMBDA___22__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_102_warmelt_macro_MEXPAND_LETBINDING (meltclosure_ptr_t meltclosp_,
					       melt_ptr_t meltfirstargp_,
					       const melt_argdescr_cell_t
					       meltxargdescr_[],
					       union meltparam_un
					       *meltxargtab_,
					       const melt_argdescr_cell_t
					       meltxresdescr_[],
					       union meltparam_un
					       *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_103_warmelt_macro_MEXPAND_LET (meltclosure_ptr_t meltclosp_,
					melt_ptr_t meltfirstargp_,
					const melt_argdescr_cell_t
					meltxargdescr_[],
					union meltparam_un *meltxargtab_,
					const melt_argdescr_cell_t
					meltxresdescr_[],
					union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_104_warmelt_macro_LAMBDA___23__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_105_warmelt_macro_LAMBDA___24__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_106_warmelt_macro_LAMBDA___25__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_107_warmelt_macro_YES_RECURSIVELY_CONSTRUCTIBLE (meltclosure_ptr_t
							  meltclosp_,
							  melt_ptr_t
							  meltfirstargp_,
							  const
							  melt_argdescr_cell_t
							  meltxargdescr_[],
							  union meltparam_un
							  *meltxargtab_,
							  const
							  melt_argdescr_cell_t
							  meltxresdescr_[],
							  union meltparam_un
							  *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_108_warmelt_macro_MEXPAND_LETREC (meltclosure_ptr_t meltclosp_,
					   melt_ptr_t meltfirstargp_,
					   const melt_argdescr_cell_t
					   meltxargdescr_[],
					   union meltparam_un *meltxargtab_,
					   const melt_argdescr_cell_t
					   meltxresdescr_[],
					   union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_109_warmelt_macro_LAMBDA___26__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_110_warmelt_macro_LAMBDA___27__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_111_warmelt_macro_MEXPAND_LAMBDA (meltclosure_ptr_t meltclosp_,
					   melt_ptr_t meltfirstargp_,
					   const melt_argdescr_cell_t
					   meltxargdescr_[],
					   union meltparam_un *meltxargtab_,
					   const melt_argdescr_cell_t
					   meltxresdescr_[],
					   union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_112_warmelt_macro_LAMBDA___28__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_113_warmelt_macro_MEXPAND_VARIADIC (meltclosure_ptr_t meltclosp_,
					     melt_ptr_t meltfirstargp_,
					     const melt_argdescr_cell_t
					     meltxargdescr_[],
					     union meltparam_un *meltxargtab_,
					     const melt_argdescr_cell_t
					     meltxresdescr_[],
					     union meltparam_un
					     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_114_warmelt_macro_LAMBDA___29__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_115_warmelt_macro_LAMBDA___30__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_116_warmelt_macro_LAMBDA___31__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_117_warmelt_macro_LAMBDA___32__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_118_warmelt_macro_MEXPAND_MULTICALL (meltclosure_ptr_t meltclosp_,
					      melt_ptr_t meltfirstargp_,
					      const melt_argdescr_cell_t
					      meltxargdescr_[],
					      union meltparam_un
					      *meltxargtab_,
					      const melt_argdescr_cell_t
					      meltxresdescr_[],
					      union meltparam_un
					      *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_119_warmelt_macro_LAMBDA___33__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_120_warmelt_macro_LAMBDA___34__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_121_warmelt_macro_MEXPAND_QUOTE (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_122_warmelt_macro_MEXPAND_COMMENT (meltclosure_ptr_t meltclosp_,
					    melt_ptr_t meltfirstargp_,
					    const melt_argdescr_cell_t
					    meltxargdescr_[],
					    union meltparam_un *meltxargtab_,
					    const melt_argdescr_cell_t
					    meltxresdescr_[],
					    union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_123_warmelt_macro_MEXPAND_CHEADER (meltclosure_ptr_t meltclosp_,
					    melt_ptr_t meltfirstargp_,
					    const melt_argdescr_cell_t
					    meltxargdescr_[],
					    union meltparam_un *meltxargtab_,
					    const melt_argdescr_cell_t
					    meltxresdescr_[],
					    union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_124_warmelt_macro_MEXPAND_USE_PACKAGE_FROM_PKG_CONFIG
(meltclosure_ptr_t meltclosp_, melt_ptr_t meltfirstargp_, const melt_argdescr_cell_t meltxargdescr_[],
union meltparam_un *meltxargtab_, const melt_argdescr_cell_t meltxresdescr_[],
union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_125_warmelt_macro_PAIRLIST_TO_RETURN (meltclosure_ptr_t meltclosp_,
					       melt_ptr_t meltfirstargp_,
					       const melt_argdescr_cell_t
					       meltxargdescr_[],
					       union meltparam_un
					       *meltxargtab_,
					       const melt_argdescr_cell_t
					       meltxresdescr_[],
					       union meltparam_un
					       *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_126_warmelt_macro_LAMBDA___35__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_127_warmelt_macro_MEXPAND_PROGN (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_128_warmelt_macro_MEXPAND_RETURN (meltclosure_ptr_t meltclosp_,
					   melt_ptr_t meltfirstargp_,
					   const melt_argdescr_cell_t
					   meltxargdescr_[],
					   union meltparam_un *meltxargtab_,
					   const melt_argdescr_cell_t
					   meltxresdescr_[],
					   union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_129_warmelt_macro_MEXPAND_FOREVER (meltclosure_ptr_t meltclosp_,
					    melt_ptr_t meltfirstargp_,
					    const melt_argdescr_cell_t
					    meltxargdescr_[],
					    union meltparam_un *meltxargtab_,
					    const melt_argdescr_cell_t
					    meltxresdescr_[],
					    union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_130_warmelt_macro_LAMBDA___36__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_131_warmelt_macro_MEXPAND_EXIT (meltclosure_ptr_t meltclosp_,
					 melt_ptr_t meltfirstargp_,
					 const melt_argdescr_cell_t
					 meltxargdescr_[],
					 union meltparam_un *meltxargtab_,
					 const melt_argdescr_cell_t
					 meltxresdescr_[],
					 union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_132_warmelt_macro_LAMBDA___37__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_133_warmelt_macro_MEXPAND_AGAIN (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_134_warmelt_macro_MEXPAND_COMPILE_WARNING (meltclosure_ptr_t
						    meltclosp_,
						    melt_ptr_t meltfirstargp_,
						    const melt_argdescr_cell_t
						    meltxargdescr_[],
						    union meltparam_un
						    *meltxargtab_,
						    const melt_argdescr_cell_t
						    meltxresdescr_[],
						    union meltparam_un
						    *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_135_warmelt_macro_MEXPAND_ASSERT_MSG (meltclosure_ptr_t meltclosp_,
					       melt_ptr_t meltfirstargp_,
					       const melt_argdescr_cell_t
					       meltxargdescr_[],
					       union meltparam_un
					       *meltxargtab_,
					       const melt_argdescr_cell_t
					       meltxresdescr_[],
					       union meltparam_un
					       *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_136_warmelt_macro_MEXPAND_DEBUG_MSG (meltclosure_ptr_t meltclosp_,
					      melt_ptr_t meltfirstargp_,
					      const melt_argdescr_cell_t
					      meltxargdescr_[],
					      union meltparam_un
					      *meltxargtab_,
					      const melt_argdescr_cell_t
					      meltxresdescr_[],
					      union meltparam_un
					      *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_137_warmelt_macro_MEXPAND_DEBUG (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_138_warmelt_macro_MEXPAND_EXPORT_VALUES (meltclosure_ptr_t
						  meltclosp_,
						  melt_ptr_t meltfirstargp_,
						  const melt_argdescr_cell_t
						  meltxargdescr_[],
						  union meltparam_un
						  *meltxargtab_,
						  const melt_argdescr_cell_t
						  meltxresdescr_[],
						  union meltparam_un
						  *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_139_warmelt_macro_LAMBDA___38__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_140_warmelt_macro_MEXPAND_EXPORT_MACRO (meltclosure_ptr_t meltclosp_,
						 melt_ptr_t meltfirstargp_,
						 const melt_argdescr_cell_t
						 meltxargdescr_[],
						 union meltparam_un
						 *meltxargtab_,
						 const melt_argdescr_cell_t
						 meltxresdescr_[],
						 union meltparam_un
						 *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_141_warmelt_macro_MEXPAND_EXPORT_PATMACRO (meltclosure_ptr_t
						    meltclosp_,
						    melt_ptr_t meltfirstargp_,
						    const melt_argdescr_cell_t
						    meltxargdescr_[],
						    union meltparam_un
						    *meltxargtab_,
						    const melt_argdescr_cell_t
						    meltxresdescr_[],
						    union meltparam_un
						    *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_142_warmelt_macro_MEXPAND_EXPORT_CLASS (meltclosure_ptr_t meltclosp_,
						 melt_ptr_t meltfirstargp_,
						 const melt_argdescr_cell_t
						 meltxargdescr_[],
						 union meltparam_un
						 *meltxargtab_,
						 const melt_argdescr_cell_t
						 meltxresdescr_[],
						 union meltparam_un
						 *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_143_warmelt_macro_LAMBDA___39__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_144_warmelt_macro_MEXPAND_EXPORT_SYNONYM (meltclosure_ptr_t
						   meltclosp_,
						   melt_ptr_t meltfirstargp_,
						   const melt_argdescr_cell_t
						   meltxargdescr_[],
						   union meltparam_un
						   *meltxargtab_,
						   const melt_argdescr_cell_t
						   meltxresdescr_[],
						   union meltparam_un
						   *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_145_warmelt_macro_MEXPAND_CURRENT_MODULE_ENVIRONMENT_CONTAINER
(meltclosure_ptr_t meltclosp_, melt_ptr_t meltfirstargp_, const melt_argdescr_cell_t meltxargdescr_[],
union meltparam_un *meltxargtab_, const melt_argdescr_cell_t meltxresdescr_[],
union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_146_warmelt_macro_MEXPAND_PARENT_MODULE_ENVIRONMENT
(meltclosure_ptr_t meltclosp_, melt_ptr_t meltfirstargp_, const melt_argdescr_cell_t meltxargdescr_[],
union meltparam_un *meltxargtab_, const melt_argdescr_cell_t meltxresdescr_[],
union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_147_warmelt_macro_MEXPAND_UPDATE_CURRENT_MODULE_ENVIRONMENT_CONTAINER
(meltclosure_ptr_t meltclosp_, melt_ptr_t meltfirstargp_, const melt_argdescr_cell_t meltxargdescr_[],
union meltparam_un *meltxargtab_, const melt_argdescr_cell_t meltxresdescr_[],
union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_148_warmelt_macro_MEXPAND_FETCH_PREDEFINED (meltclosure_ptr_t
						     meltclosp_,
						     melt_ptr_t
						     meltfirstargp_,
						     const
						     melt_argdescr_cell_t
						     meltxargdescr_[],
						     union meltparam_un
						     *meltxargtab_,
						     const
						     melt_argdescr_cell_t
						     meltxresdescr_[],
						     union meltparam_un
						     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_149_warmelt_macro_MEXPAND_STORE_PREDEFINED (meltclosure_ptr_t
						     meltclosp_,
						     melt_ptr_t
						     meltfirstargp_,
						     const
						     melt_argdescr_cell_t
						     meltxargdescr_[],
						     union meltparam_un
						     *meltxargtab_,
						     const
						     melt_argdescr_cell_t
						     meltxresdescr_[],
						     union meltparam_un
						     *meltxrestab_);



MELT_EXTERN void *melt_start_this_module (void *);


/* define different names when debugging or not */
#if MELT_HAVE_DEBUG
MELT_EXTERN const char meltmodule_warmelt_macro__melt_have_debug_enabled[];
#define melt_have_debug_string meltmodule_warmelt_macro__melt_have_debug_enabled
#else /*!MELT_HAVE_DEBUG */
MELT_EXTERN const char meltmodule_warmelt_macro__melt_have_debug_disabled[];
#define melt_have_debug_string meltmodule_warmelt_macro__melt_have_debug_disabled
#endif /*!MELT_HAVE_DEBUG */


struct frame_melt_start_this_module_st;
void MELT_MODULE_VISIBILITY meltmod__warmelt_macro__initialmeltchunk_0 (struct
									frame_melt_start_this_module_st
									*,
									char
									*);
void MELT_MODULE_VISIBILITY meltmod__warmelt_macro__initialmeltchunk_1 (struct
									frame_melt_start_this_module_st
									*,
									char
									*);
void MELT_MODULE_VISIBILITY meltmod__warmelt_macro__initialmeltchunk_2 (struct
									frame_melt_start_this_module_st
									*,
									char
									*);
void MELT_MODULE_VISIBILITY meltmod__warmelt_macro__initialmeltchunk_3 (struct
									frame_melt_start_this_module_st
									*,
									char
									*);
void MELT_MODULE_VISIBILITY meltmod__warmelt_macro__initialmeltchunk_4 (struct
									frame_melt_start_this_module_st
									*,
									char
									*);
void MELT_MODULE_VISIBILITY meltmod__warmelt_macro__initialmeltchunk_5 (struct
									frame_melt_start_this_module_st
									*,
									char
									*);
void MELT_MODULE_VISIBILITY meltmod__warmelt_macro__initialmeltchunk_6 (struct
									frame_melt_start_this_module_st
									*,
									char
									*);
void MELT_MODULE_VISIBILITY meltmod__warmelt_macro__initialmeltchunk_7 (struct
									frame_melt_start_this_module_st
									*,
									char
									*);
void MELT_MODULE_VISIBILITY meltmod__warmelt_macro__initialmeltchunk_8 (struct
									frame_melt_start_this_module_st
									*,
									char
									*);
void MELT_MODULE_VISIBILITY meltmod__warmelt_macro__initialmeltchunk_9 (struct
									frame_melt_start_this_module_st
									*,
									char
									*);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_macro__initialmeltchunk_10 (struct
					     frame_melt_start_this_module_st
					     *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_macro__initialmeltchunk_11 (struct
					     frame_melt_start_this_module_st
					     *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_macro__initialmeltchunk_12 (struct
					     frame_melt_start_this_module_st
					     *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_macro__initialmeltchunk_13 (struct
					     frame_melt_start_this_module_st
					     *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_macro__initialmeltchunk_14 (struct
					     frame_melt_start_this_module_st
					     *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_macro__initialmeltchunk_15 (struct
					     frame_melt_start_this_module_st
					     *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_macro__initialmeltchunk_16 (struct
					     frame_melt_start_this_module_st
					     *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_macro__initialmeltchunk_17 (struct
					     frame_melt_start_this_module_st
					     *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_macro__initialmeltchunk_18 (struct
					     frame_melt_start_this_module_st
					     *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_macro__initialmeltchunk_19 (struct
					     frame_melt_start_this_module_st
					     *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_macro__initialmeltchunk_20 (struct
					     frame_melt_start_this_module_st
					     *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_macro__initialmeltchunk_21 (struct
					     frame_melt_start_this_module_st
					     *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_macro__initialmeltchunk_22 (struct
					     frame_melt_start_this_module_st
					     *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_macro__initialmeltchunk_23 (struct
					     frame_melt_start_this_module_st
					     *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_macro__initialmeltchunk_24 (struct
					     frame_melt_start_this_module_st
					     *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_macro__initialmeltchunk_25 (struct
					     frame_melt_start_this_module_st
					     *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_macro__initialmeltchunk_26 (struct
					     frame_melt_start_this_module_st
					     *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_macro__initialmeltchunk_27 (struct
					     frame_melt_start_this_module_st
					     *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_macro__initialmeltchunk_28 (struct
					     frame_melt_start_this_module_st
					     *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_macro__initialmeltchunk_29 (struct
					     frame_melt_start_this_module_st
					     *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_macro__initialmeltchunk_30 (struct
					     frame_melt_start_this_module_st
					     *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_macro__initialmeltchunk_31 (struct
					     frame_melt_start_this_module_st
					     *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_macro__initialmeltchunk_32 (struct
					     frame_melt_start_this_module_st
					     *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_macro__initialmeltchunk_33 (struct
					     frame_melt_start_this_module_st
					     *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_macro__initialmeltchunk_34 (struct
					     frame_melt_start_this_module_st
					     *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_macro__initialmeltchunk_35 (struct
					     frame_melt_start_this_module_st
					     *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_macro__initialmeltchunk_36 (struct
					     frame_melt_start_this_module_st
					     *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_macro__initialmeltchunk_37 (struct
					     frame_melt_start_this_module_st
					     *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_macro__initialmeltchunk_38 (struct
					     frame_melt_start_this_module_st
					     *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_macro__initialmeltchunk_39 (struct
					     frame_melt_start_this_module_st
					     *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_macro__forward_or_mark_module_start_frame (struct
							    melt_callframe_st
							    *fp, int marking);
#define meltmarking_melt_start_this_module  meltmod__warmelt_macro__forward_or_mark_module_start_frame



/**** warmelt-macro+04.c implementations ****/




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_101_warmelt_macro_LAMBDA___22__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_101_warmelt_macro_LAMBDA___22___melt =
    melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_101_warmelt_macro_LAMBDA___22___st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 2
    melt_ptr_t mcfr_varptr[2];
#define MELTFRAM_NBVARNUM 1
    long mcfr_varnum[1];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_101_warmelt_macro_LAMBDA___22__ is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_101_warmelt_macro_LAMBDA___22___st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 2; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_101_warmelt_macro_LAMBDA___22__ nbval 2*/
  meltfram__.mcfr_nbvar = 2 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("LAMBDA_", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-macro.melt:5141:/ getarg");
 /*_.C__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {

    MELT_LOCATION ("warmelt-macro.melt:5142:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#IS_NOT_A__L1*/ meltfnum[0] =
      !melt_is_instance_of ((melt_ptr_t) ( /*_.C__V2*/ meltfptr[1]),
			    (melt_ptr_t) (( /*!CLASS_SEXPR */ meltfrout->
					   tabval[0])));;
    MELT_LOCATION ("warmelt-macro.melt:5142:/ cond");
    /*cond */ if ( /*_#IS_NOT_A__L1*/ meltfnum[0])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{


	  {
	    MELT_LOCATION ("warmelt-macro.melt:5143:/ locexp");
	    /* error_plain */
	      melt_error_str ((melt_ptr_t)
			      (( /*~LOC */ meltfclos->tabval[0])),
			      ("MATCH with non-sexpr"), (melt_ptr_t) 0);
	  }
	  ;
	  /*epilog */
	}
	;
      }				/*noelse */
    ;
    MELT_LOCATION ("warmelt-macro.melt:5141:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.C__V2*/ meltfptr[1];;

    {
      MELT_LOCATION ("warmelt-macro.melt:5141:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_#IS_NOT_A__L1*/ meltfnum[0] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("LAMBDA_", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_101_warmelt_macro_LAMBDA___22___melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_101_warmelt_macro_LAMBDA___22__ */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_102_warmelt_macro_MEXPAND_LETBINDING (meltclosure_ptr_t meltclosp_,
					       melt_ptr_t meltfirstargp_,
					       const melt_argdescr_cell_t
					       meltxargdescr_[],
					       union meltparam_un *
					       meltxargtab_,
					       const melt_argdescr_cell_t
					       meltxresdescr_[],
					       union meltparam_un *
					       meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_102_warmelt_macro_MEXPAND_LETBINDING_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_102_warmelt_macro_MEXPAND_LETBINDING_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 71
    melt_ptr_t mcfr_varptr[71];
#define MELTFRAM_NBVARNUM 16
    long mcfr_varnum[16];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_102_warmelt_macro_MEXPAND_LETBINDING is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_102_warmelt_macro_MEXPAND_LETBINDING_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 71; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_102_warmelt_macro_MEXPAND_LETBINDING nbval 71*/
  meltfram__.mcfr_nbvar = 71 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("MEXPAND_LETBINDING", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-macro.melt:5219:/ getarg");
 /*_.SEXPR__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.ENV__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.ENV__V3*/ meltfptr[2])) != NULL);


  /*getarg#2 */
  /*^getarg */
  if (meltxargdescr_[1] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.MEXPANDER__V4*/ meltfptr[3] =
    (meltxargtab_[1].meltbp_aptr) ? (*(meltxargtab_[1].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.MEXPANDER__V4*/ meltfptr[3])) !=
	      NULL);


  /*getarg#3 */
  /*^getarg */
  if (meltxargdescr_[2] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.MODCTX__V5*/ meltfptr[4] =
    (meltxargtab_[2].meltbp_aptr) ? (*(meltxargtab_[2].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.MODCTX__V5*/ meltfptr[4])) !=
	      NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-macro.melt:5220:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L1*/ meltfnum[0] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-macro.melt:5220:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L1*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-macro.melt:5220:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-macro.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 5220;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "mexpand_letbinding sexpr";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.SEXPR__V2*/ meltfptr[1];
	      /*_.MELT_DEBUG_FUN__V8*/ meltfptr[7] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V7*/ meltfptr[6] = /*_.MELT_DEBUG_FUN__V8*/ meltfptr[7];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-macro.melt:5220:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V8*/ meltfptr[7] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V7*/ meltfptr[6] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-macro.melt:5220:/ quasiblock");


      /*_.PROGN___V9*/ meltfptr[7] = /*_.IF___V7*/ meltfptr[6];;
      /*^compute */
      /*_.IFCPP___V6*/ meltfptr[5] = /*_.PROGN___V9*/ meltfptr[7];;
      /*epilog */

      MELT_LOCATION ("warmelt-macro.melt:5220:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L1*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IF___V7*/ meltfptr[6] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V9*/ meltfptr[7] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V6*/ meltfptr[5] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-macro.melt:5221:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L3*/ meltfnum[1] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.SEXPR__V2*/ meltfptr[1]),
			     (melt_ptr_t) (( /*!CLASS_SEXPR */ meltfrout->
					    tabval[1])));;
      MELT_LOCATION ("warmelt-macro.melt:5221:/ cond");
      /*cond */ if ( /*_#IS_A__L3*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V11*/ meltfptr[7] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-macro.melt:5221:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check sexpr"),
				  ("warmelt-macro.melt")
				  ? ("warmelt-macro.melt") : __FILE__,
				  (5221) ? (5221) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V11*/ meltfptr[7] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V10*/ meltfptr[6] = /*_.IFELSE___V11*/ meltfptr[7];;
      /*epilog */

      MELT_LOCATION ("warmelt-macro.melt:5221:/ clear");
	     /*clear *//*_#IS_A__L3*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V11*/ meltfptr[7] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V10*/ meltfptr[6] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-macro.melt:5222:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L4*/ meltfnum[0] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.ENV__V3*/ meltfptr[2]),
			     (melt_ptr_t) (( /*!CLASS_ENVIRONMENT */
					    meltfrout->tabval[2])));;
      MELT_LOCATION ("warmelt-macro.melt:5222:/ cond");
      /*cond */ if ( /*_#IS_A__L4*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V13*/ meltfptr[12] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-macro.melt:5222:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check env"),
				  ("warmelt-macro.melt")
				  ? ("warmelt-macro.melt") : __FILE__,
				  (5222) ? (5222) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V13*/ meltfptr[12] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V12*/ meltfptr[7] = /*_.IFELSE___V13*/ meltfptr[12];;
      /*epilog */

      MELT_LOCATION ("warmelt-macro.melt:5222:/ clear");
	     /*clear *//*_#IS_A__L4*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V13*/ meltfptr[12] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V12*/ meltfptr[7] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-macro.melt:5223:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#NULL__L5*/ meltfnum[1] =
      (( /*_.MEXPANDER__V4*/ meltfptr[3]) == NULL);;
    MELT_LOCATION ("warmelt-macro.melt:5223:/ cond");
    /*cond */ if ( /*_#NULL__L5*/ meltfnum[1])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  /*^compute */
	  /*_.MEXPANDER__V4*/ meltfptr[3] = /*_.SETQ___V15*/ meltfptr[14] =
	    ( /*!MACROEXPAND_1 */ meltfrout->tabval[3]);;
	  /*_.IF___V14*/ meltfptr[12] = /*_.SETQ___V15*/ meltfptr[14];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-macro.melt:5223:/ clear");
	     /*clear *//*_.SETQ___V15*/ meltfptr[14] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

  /*_.IF___V14*/ meltfptr[12] = NULL;;
      }
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-macro.melt:5224:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_OBJECT__L6*/ meltfnum[0] =
	(melt_magic_discr ((melt_ptr_t) ( /*_.MODCTX__V5*/ meltfptr[4])) ==
	 MELTOBMAG_OBJECT);;
      MELT_LOCATION ("warmelt-macro.melt:5224:/ cond");
      /*cond */ if ( /*_#IS_OBJECT__L6*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V17*/ meltfptr[16] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-macro.melt:5224:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check modctx"),
				  ("warmelt-macro.melt")
				  ? ("warmelt-macro.melt") : __FILE__,
				  (5224) ? (5224) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V17*/ meltfptr[16] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V16*/ meltfptr[14] = /*_.IFELSE___V17*/ meltfptr[16];;
      /*epilog */

      MELT_LOCATION ("warmelt-macro.melt:5224:/ clear");
	     /*clear *//*_#IS_OBJECT__L6*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V17*/ meltfptr[16] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V16*/ meltfptr[14] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-macro.melt:5225:/ quasiblock");


    MELT_LOCATION ("warmelt-macro.melt:5226:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.SEXPR__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 2, "SEXP_CONTENTS");
  /*_.CONT__V19*/ meltfptr[18] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-macro.melt:5227:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.SEXPR__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 1, "LOCA_LOCATION");
  /*_.LOC__V20*/ meltfptr[19] = slot;
    };
    ;
    /*_.CTYP__V21*/ meltfptr[20] = ( /*!CTYPE_VALUE */ meltfrout->tabval[4]);;
    /*^compute */
    /*_.VAR__V22*/ meltfptr[21] = ( /*nil */ NULL);;
    /*^compute */
    /*_.EXPR__V23*/ meltfptr[22] = ( /*nil */ NULL);;
    /*^compute */
 /*_.CURPAIR__V24*/ meltfptr[23] =
      (melt_list_first ((melt_ptr_t) ( /*_.CONT__V19*/ meltfptr[18])));;
    /*^compute */
    /*_.CURARG__V25*/ meltfptr[24] = ( /*nil */ NULL);;
    /*^compute */
 /*_.PAIR_HEAD__V26*/ meltfptr[25] =
      (melt_pair_head ((melt_ptr_t) ( /*_.CURPAIR__V24*/ meltfptr[23])));;
    MELT_LOCATION ("warmelt-macro.melt:5234:/ compute");
    /*_.CURARG__V25*/ meltfptr[24] = /*_.SETQ___V27*/ meltfptr[26] =
      /*_.PAIR_HEAD__V26*/ meltfptr[25];;
    MELT_LOCATION ("warmelt-macro.melt:5235:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#IS_A__L7*/ meltfnum[0] =
      melt_is_instance_of ((melt_ptr_t) ( /*_.CURARG__V25*/ meltfptr[24]),
			   (melt_ptr_t) (( /*!CLASS_KEYWORD */ meltfrout->
					  tabval[5])));;
    MELT_LOCATION ("warmelt-macro.melt:5235:/ cond");
    /*cond */ if ( /*_#IS_A__L7*/ meltfnum[0])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-macro.melt:5236:/ quasiblock");


	  /*^getslot */
	  {
	    melt_ptr_t slot = NULL, obj = NULL;
	    obj = (melt_ptr_t) ( /*_.CURARG__V25*/ meltfptr[24]) /*=obj*/ ;
	    melt_object_get_field (slot, obj, 2, "SYMB_DATA");
    /*_.CTY__V30*/ meltfptr[29] = slot;
	  };
	  ;
	  MELT_LOCATION ("warmelt-macro.melt:5237:/ getslot");
	  {
	    melt_ptr_t slot = NULL, obj = NULL;
	    obj = (melt_ptr_t) ( /*_.CURARG__V25*/ meltfptr[24]) /*=obj*/ ;
	    melt_object_get_field (slot, obj, 1, "NAMED_NAME");
    /*_.TYNAM__V31*/ meltfptr[30] = slot;
	  };
	  ;
	  MELT_LOCATION ("warmelt-macro.melt:5240:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
   /*_#IS_NOT_A__L8*/ meltfnum[7] =
	    !melt_is_instance_of ((melt_ptr_t) ( /*_.CTY__V30*/ meltfptr[29]),
				  (melt_ptr_t) (( /*!CLASS_CTYPE */
						 meltfrout->tabval[6])));;
	  MELT_LOCATION ("warmelt-macro.melt:5240:/ cond");
	  /*cond */ if ( /*_#IS_NOT_A__L8*/ meltfnum[7])	/*then */
	    {
	      /*^cond.then */
	      /*^block */
	      /*anyblock */
	      {




		{
		  MELT_LOCATION ("warmelt-macro.melt:5241:/ locexp");
		  melt_error_str ((melt_ptr_t) ( /*_.LOC__V20*/ meltfptr[19]),
				  ("letbinding with invalid type keyword"),
				  (melt_ptr_t) ( /*_.TYNAM__V31*/
						meltfptr[30]));
		}
		;
	       /*clear *//*_.IFELSE___V32*/ meltfptr[31] = 0;
		/*epilog */
	      }
	      ;
	    }
	  else
	    {
	      MELT_LOCATION ("warmelt-macro.melt:5240:/ cond.else");

	      /*^block */
	      /*anyblock */
	      {

		MELT_LOCATION ("warmelt-macro.melt:5242:/ checksignal");
		MELT_CHECK_SIGNAL ();
		;
		/*^getslot */
		{
		  melt_ptr_t slot = NULL, obj = NULL;
		  obj = (melt_ptr_t) ( /*_.CTY__V30*/ meltfptr[29]) /*=obj*/ ;
		  melt_object_get_field (slot, obj, 2, "CTYPE_KEYWORD");
      /*_.CTYPE_KEYWORD__V33*/ meltfptr[32] = slot;
		};
		;
     /*_#__L9*/ meltfnum[8] =
		  (( /*_.CTYPE_KEYWORD__V33*/ meltfptr[32]) ==
		   ( /*_.CURARG__V25*/ meltfptr[24]));;
		MELT_LOCATION ("warmelt-macro.melt:5242:/ cond");
		/*cond */ if ( /*_#__L9*/ meltfnum[8])	/*then */
		  {
		    /*^cond.then */
		    /*^block */
		    /*anyblock */
		    {

		      MELT_LOCATION ("warmelt-macro.melt:5243:/ compute");
		      /*_.CTYP__V21*/ meltfptr[20] =
			/*_.SETQ___V35*/ meltfptr[34] =
			/*_.CTY__V30*/ meltfptr[29];;
		      /*_.IFELSE___V34*/ meltfptr[33] =
			/*_.SETQ___V35*/ meltfptr[34];;
		      /*epilog */

		      MELT_LOCATION ("warmelt-macro.melt:5242:/ clear");
		 /*clear *//*_.SETQ___V35*/ meltfptr[34] = 0;
		    }
		    ;
		  }
		else
		  {		/*^cond.else */

		    /*^block */
		    /*anyblock */
		    {

		      MELT_LOCATION ("warmelt-macro.melt:5244:/ checksignal");
		      MELT_CHECK_SIGNAL ();
		      ;
		      /*^getslot */
		      {
			melt_ptr_t slot = NULL, obj = NULL;
			obj =
			  (melt_ptr_t) ( /*_.CTY__V30*/ meltfptr[29]) /*=obj*/
			  ;
			melt_object_get_field (slot, obj, 10,
					       "CTYPE_ALTKEYWORD");
	/*_.CTYPE_ALTKEYWORD__V36*/ meltfptr[34] = slot;
		      };
		      ;
       /*_#__L10*/ meltfnum[9] =
			(( /*_.CTYPE_ALTKEYWORD__V36*/ meltfptr[34]) ==
			 ( /*_.CURARG__V25*/ meltfptr[24]));;
		      MELT_LOCATION ("warmelt-macro.melt:5244:/ cond");
		      /*cond */ if ( /*_#__L10*/ meltfnum[9])	/*then */
			{
			  /*^cond.then */
			  /*^block */
			  /*anyblock */
			  {

			    MELT_LOCATION
			      ("warmelt-macro.melt:5245:/ compute");
			    /*_.CTYP__V21*/ meltfptr[20] =
			      /*_.SETQ___V37*/ meltfptr[36] =
			      /*_.CTY__V30*/ meltfptr[29];;

			    {
			      MELT_LOCATION
				("warmelt-macro.melt:5246:/ locexp");
			      melt_warning_str (0,
						(melt_ptr_t) ( /*_.LOC__V20*/
							      meltfptr[19]),
						("obsolete alternate ctype keyword in let binding"),
						(melt_ptr_t) ( /*_.TYNAM__V31*/ meltfptr[30]));
			    }
			    ;
			    MELT_LOCATION ("warmelt-macro.melt:5248:/ cond");
			    /*cond */ if (
					   /*ifisa */
					   melt_is_instance_of ((melt_ptr_t)
								( /*_.CTYP__V21*/ meltfptr[20]),
								(melt_ptr_t) (( /*!CLASS_CTYPE */ meltfrout->tabval[6])))
			      )	/*then */
			      {
				/*^cond.then */
				/*^getslot */
				{
				  melt_ptr_t slot = NULL, obj = NULL;
				  obj =
				    (melt_ptr_t) ( /*_.CTYP__V21*/
						  meltfptr[20]) /*=obj*/ ;
				  melt_object_get_field (slot, obj, 2,
							 "CTYPE_KEYWORD");
	   /*_.CTYPE_KEYWORD__V38*/ meltfptr[37] =
				    slot;
				};
				;
			      }
			    else
			      {	/*^cond.else */

	  /*_.CTYPE_KEYWORD__V38*/ meltfptr[37] = NULL;;
			      }
			    ;
			    MELT_LOCATION ("warmelt-macro.melt:5248:/ cond");
			    /*cond */ if (
					   /*ifisa */
					   melt_is_instance_of ((melt_ptr_t)
								( /*_.CTYPE_KEYWORD__V38*/ meltfptr[37]),
								(melt_ptr_t) (( /*!CLASS_NAMED */ meltfrout->tabval[7])))
			      )	/*then */
			      {
				/*^cond.then */
				/*^getslot */
				{
				  melt_ptr_t slot = NULL, obj = NULL;
				  obj =
				    (melt_ptr_t) ( /*_.CTYPE_KEYWORD__V38*/
						  meltfptr[37]) /*=obj*/ ;
				  melt_object_get_field (slot, obj, 1,
							 "NAMED_NAME");
	   /*_.NAMED_NAME__V39*/ meltfptr[38] = slot;
				};
				;
			      }
			    else
			      {	/*^cond.else */

	  /*_.NAMED_NAME__V39*/ meltfptr[38] = NULL;;
			      }
			    ;

			    {
			      MELT_LOCATION
				("warmelt-macro.melt:5248:/ locexp");
			      melt_inform_str ((melt_ptr_t)
					       ( /*_.LOC__V20*/ meltfptr[19]),
					       ("prefered ctype keyword"),
					       (melt_ptr_t) ( /*_.NAMED_NAME__V39*/ meltfptr[38]));
			    }
			    ;
			    MELT_LOCATION
			      ("warmelt-macro.melt:5244:/ quasiblock");


			    /*epilog */

			    /*^clear */
		   /*clear *//*_.SETQ___V37*/ meltfptr[36] = 0;
			    /*^clear */
		   /*clear *//*_.CTYPE_KEYWORD__V38*/ meltfptr[37] =
			      0;
			    /*^clear */
		   /*clear *//*_.NAMED_NAME__V39*/ meltfptr[38] = 0;
			  }
			  ;
			}
		      else
			{	/*^cond.else */

			  /*^block */
			  /*anyblock */
			  {


			    {
			      MELT_LOCATION
				("warmelt-macro.melt:5251:/ locexp");
			      melt_error_str ((melt_ptr_t)
					      ( /*_.LOC__V20*/ meltfptr[19]),
					      ("letbinding with invalid type keyword"),
					      (melt_ptr_t) ( /*_.TYNAM__V31*/
							    meltfptr[30]));
			    }
			    ;
			    MELT_LOCATION
			      ("warmelt-macro.melt:5250:/ quasiblock");


			    /*epilog */
			  }
			  ;
			}
		      ;
		      /*epilog */

		      MELT_LOCATION ("warmelt-macro.melt:5242:/ clear");
		 /*clear *//*_.CTYPE_ALTKEYWORD__V36*/ meltfptr[34] = 0;
		      /*^clear */
		 /*clear *//*_#__L10*/ meltfnum[9] = 0;
		    }
		    ;
		  }
		;
		/*_.IFELSE___V32*/ meltfptr[31] =
		  /*_.IFELSE___V34*/ meltfptr[33];;
		/*epilog */

		MELT_LOCATION ("warmelt-macro.melt:5240:/ clear");
	       /*clear *//*_.CTYPE_KEYWORD__V33*/ meltfptr[32] = 0;
		/*^clear */
	       /*clear *//*_#__L9*/ meltfnum[8] = 0;
		/*^clear */
	       /*clear *//*_.IFELSE___V34*/ meltfptr[33] = 0;
	      }
	      ;
	    }
	  ;
   /*_.PAIR_TAIL__V40*/ meltfptr[36] =
	    (melt_pair_tail
	     ((melt_ptr_t) ( /*_.CURPAIR__V24*/ meltfptr[23])));;
	  MELT_LOCATION ("warmelt-macro.melt:5253:/ compute");
	  /*_.CURPAIR__V24*/ meltfptr[23] = /*_.SETQ___V41*/ meltfptr[37] =
	    /*_.PAIR_TAIL__V40*/ meltfptr[36];;
   /*_.PAIR_HEAD__V42*/ meltfptr[38] =
	    (melt_pair_head
	     ((melt_ptr_t) ( /*_.CURPAIR__V24*/ meltfptr[23])));;
	  MELT_LOCATION ("warmelt-macro.melt:5254:/ compute");
	  /*_.CURARG__V25*/ meltfptr[24] = /*_.SETQ___V43*/ meltfptr[34] =
	    /*_.PAIR_HEAD__V42*/ meltfptr[38];;
	  /*_.LET___V29*/ meltfptr[28] = /*_.SETQ___V43*/ meltfptr[34];;

	  MELT_LOCATION ("warmelt-macro.melt:5236:/ clear");
	     /*clear *//*_.CTY__V30*/ meltfptr[29] = 0;
	  /*^clear */
	     /*clear *//*_.TYNAM__V31*/ meltfptr[30] = 0;
	  /*^clear */
	     /*clear *//*_#IS_NOT_A__L8*/ meltfnum[7] = 0;
	  /*^clear */
	     /*clear *//*_.IFELSE___V32*/ meltfptr[31] = 0;
	  /*^clear */
	     /*clear *//*_.PAIR_TAIL__V40*/ meltfptr[36] = 0;
	  /*^clear */
	     /*clear *//*_.SETQ___V41*/ meltfptr[37] = 0;
	  /*^clear */
	     /*clear *//*_.PAIR_HEAD__V42*/ meltfptr[38] = 0;
	  /*^clear */
	     /*clear *//*_.SETQ___V43*/ meltfptr[34] = 0;
	  /*_.IF___V28*/ meltfptr[27] = /*_.LET___V29*/ meltfptr[28];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-macro.melt:5235:/ clear");
	     /*clear *//*_.LET___V29*/ meltfptr[28] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

  /*_.IF___V28*/ meltfptr[27] = NULL;;
      }
    ;
    MELT_LOCATION ("warmelt-macro.melt:5256:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#IS_A__L11*/ meltfnum[9] =
      melt_is_instance_of ((melt_ptr_t) ( /*_.CURARG__V25*/ meltfptr[24]),
			   (melt_ptr_t) (( /*!CLASS_KEYWORD */ meltfrout->
					  tabval[5])));;
    MELT_LOCATION ("warmelt-macro.melt:5256:/ cond");
    /*cond */ if ( /*_#IS_A__L11*/ meltfnum[9])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-macro.melt:5258:/ getslot");
	  {
	    melt_ptr_t slot = NULL, obj = NULL;
	    obj = (melt_ptr_t) ( /*_.CURARG__V25*/ meltfptr[24]) /*=obj*/ ;
	    melt_object_get_field (slot, obj, 1, "NAMED_NAME");
    /*_.NAMED_NAME__V45*/ meltfptr[33] = slot;
	  };
	  ;



	  {
	    MELT_LOCATION ("warmelt-macro.melt:5257:/ locexp");
	    melt_error_str ((melt_ptr_t) ( /*_.LOC__V20*/ meltfptr[19]),
			    ("letbinding cannot bind keyword"),
			    (melt_ptr_t) ( /*_.NAMED_NAME__V45*/
					  meltfptr[33]));
	  }
	  ;
	     /*clear *//*_.IFELSE___V44*/ meltfptr[32] = 0;
	  /*epilog */

	  MELT_LOCATION ("warmelt-macro.melt:5256:/ clear");
	     /*clear *//*_.NAMED_NAME__V45*/ meltfptr[33] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-macro.melt:5259:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
   /*_#IS_A__L12*/ meltfnum[8] =
	    melt_is_instance_of ((melt_ptr_t)
				 ( /*_.CURARG__V25*/ meltfptr[24]),
				 (melt_ptr_t) (( /*!CLASS_SYMBOL */
						meltfrout->tabval[8])));;
	  MELT_LOCATION ("warmelt-macro.melt:5259:/ cond");
	  /*cond */ if ( /*_#IS_A__L12*/ meltfnum[8])	/*then */
	    {
	      /*^cond.then */
	      /*^block */
	      /*anyblock */
	      {

		MELT_LOCATION ("warmelt-macro.melt:5260:/ compute");
		/*_.VAR__V22*/ meltfptr[21] = /*_.SETQ___V47*/ meltfptr[30] =
		  /*_.CURARG__V25*/ meltfptr[24];;
     /*_.PAIR_TAIL__V48*/ meltfptr[31] =
		  (melt_pair_tail
		   ((melt_ptr_t) ( /*_.CURPAIR__V24*/ meltfptr[23])));;
		MELT_LOCATION ("warmelt-macro.melt:5261:/ compute");
		/*_.CURPAIR__V24*/ meltfptr[23] =
		  /*_.SETQ___V49*/ meltfptr[36] =
		  /*_.PAIR_TAIL__V48*/ meltfptr[31];;
     /*_.PAIR_HEAD__V50*/ meltfptr[37] =
		  (melt_pair_head
		   ((melt_ptr_t) ( /*_.CURPAIR__V24*/ meltfptr[23])));;
		MELT_LOCATION ("warmelt-macro.melt:5262:/ compute");
		/*_.CURARG__V25*/ meltfptr[24] =
		  /*_.SETQ___V51*/ meltfptr[38] =
		  /*_.PAIR_HEAD__V50*/ meltfptr[37];;
		MELT_LOCATION ("warmelt-macro.melt:5259:/ quasiblock");


		/*_.PROGN___V52*/ meltfptr[34] =
		  /*_.SETQ___V51*/ meltfptr[38];;
		/*^compute */
		/*_.IFELSE___V46*/ meltfptr[29] =
		  /*_.PROGN___V52*/ meltfptr[34];;
		/*epilog */

		MELT_LOCATION ("warmelt-macro.melt:5259:/ clear");
	       /*clear *//*_.SETQ___V47*/ meltfptr[30] = 0;
		/*^clear */
	       /*clear *//*_.PAIR_TAIL__V48*/ meltfptr[31] = 0;
		/*^clear */
	       /*clear *//*_.SETQ___V49*/ meltfptr[36] = 0;
		/*^clear */
	       /*clear *//*_.PAIR_HEAD__V50*/ meltfptr[37] = 0;
		/*^clear */
	       /*clear *//*_.SETQ___V51*/ meltfptr[38] = 0;
		/*^clear */
	       /*clear *//*_.PROGN___V52*/ meltfptr[34] = 0;
	      }
	      ;
	    }
	  else
	    {			/*^cond.else */

    /*_.IFELSE___V46*/ meltfptr[29] = NULL;;
	    }
	  ;
	  /*^compute */
	  /*_.IFELSE___V44*/ meltfptr[32] = /*_.IFELSE___V46*/ meltfptr[29];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-macro.melt:5256:/ clear");
	     /*clear *//*_#IS_A__L12*/ meltfnum[8] = 0;
	  /*^clear */
	     /*clear *//*_.IFELSE___V46*/ meltfptr[29] = 0;
	}
	;
      }
    ;
    MELT_LOCATION ("warmelt-macro.melt:5264:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#NULL__L13*/ meltfnum[7] =
      (( /*_.VAR__V22*/ meltfptr[21]) == NULL);;
    MELT_LOCATION ("warmelt-macro.melt:5264:/ cond");
    /*cond */ if ( /*_#NULL__L13*/ meltfnum[7])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{


	  {
	    MELT_LOCATION ("warmelt-macro.melt:5265:/ locexp");
	    /* error_plain */
	      melt_error_str ((melt_ptr_t) ( /*_.LOC__V20*/ meltfptr[19]),
			      ("missing variable in letbinding"),
			      (melt_ptr_t) 0);
	  }
	  ;
	  /*epilog */
	}
	;
      }				/*noelse */
    ;
    MELT_LOCATION ("warmelt-macro.melt:5266:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^cond */
    /*cond */ if ( /*_.CURARG__V25*/ meltfptr[24])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-macro.melt:5268:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^apply */
	  /*apply */
	  {
	    union meltparam_un argtab[3];
	    memset (&argtab, 0, sizeof (argtab));
	    /*^apply.arg */
	    argtab[0].meltbp_aptr =
	      (melt_ptr_t *) & /*_.ENV__V3*/ meltfptr[2];
	    /*^apply.arg */
	    argtab[1].meltbp_aptr =
	      (melt_ptr_t *) & /*_.MEXPANDER__V4*/ meltfptr[3];
	    /*^apply.arg */
	    argtab[2].meltbp_aptr =
	      (melt_ptr_t *) & /*_.MODCTX__V5*/ meltfptr[4];
	    /*_.MEXPANDER__V53*/ meltfptr[28] =
	      melt_apply ((meltclosure_ptr_t)
			  ( /*_.MEXPANDER__V4*/ meltfptr[3]),
			  (melt_ptr_t) ( /*_.CURARG__V25*/ meltfptr[24]),
			  (MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_PTR
			   ""), argtab, "", (union meltparam_un *) 0);
	  }
	  ;
	  /*^compute */
	  /*_.EXPR__V23*/ meltfptr[22] = /*_.SETQ___V54*/ meltfptr[33] =
	    /*_.MEXPANDER__V53*/ meltfptr[28];;
   /*_.PAIR_TAIL__V55*/ meltfptr[30] =
	    (melt_pair_tail
	     ((melt_ptr_t) ( /*_.CURPAIR__V24*/ meltfptr[23])));;
	  MELT_LOCATION ("warmelt-macro.melt:5269:/ compute");
	  /*_.CURPAIR__V24*/ meltfptr[23] = /*_.SETQ___V56*/ meltfptr[31] =
	    /*_.PAIR_TAIL__V55*/ meltfptr[30];;
   /*_.PAIR_HEAD__V57*/ meltfptr[36] =
	    (melt_pair_head
	     ((melt_ptr_t) ( /*_.CURPAIR__V24*/ meltfptr[23])));;
	  MELT_LOCATION ("warmelt-macro.melt:5270:/ compute");
	  /*_.CURARG__V25*/ meltfptr[24] = /*_.SETQ___V58*/ meltfptr[37] =
	    /*_.PAIR_HEAD__V57*/ meltfptr[36];;
	  MELT_LOCATION ("warmelt-macro.melt:5271:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^cond */
	  /*cond */ if ( /*_.CURARG__V25*/ meltfptr[24])	/*then */
	    {
	      /*^cond.then */
	      /*^block */
	      /*anyblock */
	      {


		{
		  /*^locexp */
		  /* error_plain */
		    melt_error_str ((melt_ptr_t)
				    ( /*_.LOC__V20*/ meltfptr[19]),
				    ("too long letbinding"), (melt_ptr_t) 0);
		}
		;
		/*epilog */
	      }
	      ;
	    }			/*noelse */
	  ;
	  MELT_LOCATION ("warmelt-macro.melt:5267:/ quasiblock");


	  /*epilog */

	  MELT_LOCATION ("warmelt-macro.melt:5266:/ clear");
	     /*clear *//*_.MEXPANDER__V53*/ meltfptr[28] = 0;
	  /*^clear */
	     /*clear *//*_.SETQ___V54*/ meltfptr[33] = 0;
	  /*^clear */
	     /*clear *//*_.PAIR_TAIL__V55*/ meltfptr[30] = 0;
	  /*^clear */
	     /*clear *//*_.SETQ___V56*/ meltfptr[31] = 0;
	  /*^clear */
	     /*clear *//*_.PAIR_HEAD__V57*/ meltfptr[36] = 0;
	  /*^clear */
	     /*clear *//*_.SETQ___V58*/ meltfptr[37] = 0;
	}
	;
      }				/*noelse */
    ;
    MELT_LOCATION ("warmelt-macro.melt:5273:/ quasiblock");


    /*^checksignal */
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.VAR__V22*/ meltfptr[21];
      /*_.PREVBIND__V60*/ meltfptr[34] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!FIND_ENV */ meltfrout->tabval[9])),
		    (melt_ptr_t) ( /*_.ENV__V3*/ meltfptr[2]),
		    (MELTBPARSTR_PTR ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-macro.melt:5275:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#NULL__L14*/ meltfnum[8] =
      (( /*_.PREVBIND__V60*/ meltfptr[34]) == NULL);;
    MELT_LOCATION ("warmelt-macro.melt:5275:/ cond");
    /*cond */ if ( /*_#NULL__L14*/ meltfnum[8])	/*then */
      {
	/*^cond.then */
	/*_.IFELSE___V61*/ meltfptr[29] = ( /*nil */ NULL);;
      }
    else
      {
	MELT_LOCATION ("warmelt-macro.melt:5275:/ cond.else");

	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-macro.melt:5276:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
   /*_#IS_A__L15*/ meltfnum[14] =
	    melt_is_instance_of ((melt_ptr_t)
				 ( /*_.PREVBIND__V60*/ meltfptr[34]),
				 (melt_ptr_t) (( /*!CLASS_LET_BINDING */
						meltfrout->tabval[10])));;
	  MELT_LOCATION ("warmelt-macro.melt:5276:/ cond");
	  /*cond */ if ( /*_#IS_A__L15*/ meltfnum[14])	/*then */
	    {
	      /*^cond.then */
	      /*^block */
	      /*anyblock */
	      {

		MELT_LOCATION ("warmelt-macro.melt:5277:/ quasiblock");


		/*^cond */
		/*cond */ if (
			       /*ifisa */
			       melt_is_instance_of ((melt_ptr_t)
						    ( /*_.PREVBIND__V60*/
						     meltfptr[34]),
						    (melt_ptr_t) (( /*!CLASS_LET_BINDING */ meltfrout->tabval[10])))
		  )		/*then */
		  {
		    /*^cond.then */
		    /*^getslot */
		    {
		      melt_ptr_t slot = NULL, obj = NULL;
		      obj =
			(melt_ptr_t) ( /*_.PREVBIND__V60*/ meltfptr[34])
			/*=obj*/ ;
		      melt_object_get_field (slot, obj, 3, "LETBIND_LOC");
       /*_.PREVLOC__V63*/ meltfptr[33] = slot;
		    };
		    ;
		  }
		else
		  {		/*^cond.else */

      /*_.PREVLOC__V63*/ meltfptr[33] = NULL;;
		  }
		;
		MELT_LOCATION ("warmelt-macro.melt:5280:/ cond");
		/*cond */ if (
			       /*ifisa */
			       melt_is_instance_of ((melt_ptr_t)
						    ( /*_.VAR__V22*/
						     meltfptr[21]),
						    (melt_ptr_t) (( /*!CLASS_NAMED */ meltfrout->tabval[7])))
		  )		/*then */
		  {
		    /*^cond.then */
		    /*^getslot */
		    {
		      melt_ptr_t slot = NULL, obj = NULL;
		      obj =
			(melt_ptr_t) ( /*_.VAR__V22*/ meltfptr[21]) /*=obj*/ ;
		      melt_object_get_field (slot, obj, 1, "NAMED_NAME");
       /*_.NAMED_NAME__V64*/ meltfptr[30] = slot;
		    };
		    ;
		  }
		else
		  {		/*^cond.else */

      /*_.NAMED_NAME__V64*/ meltfptr[30] = NULL;;
		  }
		;

		{
		  MELT_LOCATION ("warmelt-macro.melt:5278:/ locexp");
		  melt_warning_str (0,
				    (melt_ptr_t) ( /*_.LOC__V20*/
						  meltfptr[19]),
				    ("local let binding hides upper one"),
				    (melt_ptr_t) ( /*_.NAMED_NAME__V64*/
						  meltfptr[30]));
		}
		;
		MELT_LOCATION ("warmelt-macro.melt:5281:/ checksignal");
		MELT_CHECK_SIGNAL ();
		;
		/*^cond */
		/*cond */ if ( /*_.PREVLOC__V63*/ meltfptr[33])	/*then */
		  {
		    /*^cond.then */
		    /*^block */
		    /*anyblock */
		    {

		      MELT_LOCATION ("warmelt-macro.melt:5284:/ cond");
		      /*cond */ if (
				     /*ifisa */
				     melt_is_instance_of ((melt_ptr_t)
							  ( /*_.VAR__V22*/
							   meltfptr[21]),
							  (melt_ptr_t) (( /*!CLASS_NAMED */ meltfrout->tabval[7])))
			)	/*then */
			{
			  /*^cond.then */
			  /*^getslot */
			  {
			    melt_ptr_t slot = NULL, obj = NULL;
			    obj =
			      (melt_ptr_t) ( /*_.VAR__V22*/ meltfptr[21])
			      /*=obj*/ ;
			    melt_object_get_field (slot, obj, 1,
						   "NAMED_NAME");
	 /*_.NAMED_NAME__V65*/ meltfptr[31] = slot;
			  };
			  ;
			}
		      else
			{	/*^cond.else */

	/*_.NAMED_NAME__V65*/ meltfptr[31] = NULL;;
			}
		      ;



		      {
			MELT_LOCATION ("warmelt-macro.melt:5282:/ locexp");
			melt_warning_str (0,
					  (melt_ptr_t) ( /*_.PREVLOC__V63*/
							meltfptr[33]),
					  ("here is the hidden binding"),
					  (melt_ptr_t) ( /*_.NAMED_NAME__V65*/
							meltfptr[31]));
		      }
		      ;
		 /*clear *//*_.IFELSE___V62*/ meltfptr[28] = 0;
		      /*epilog */

		      MELT_LOCATION ("warmelt-macro.melt:5281:/ clear");
		 /*clear *//*_.NAMED_NAME__V65*/ meltfptr[31] = 0;
		    }
		    ;
		  }
		else
		  {		/*^cond.else */

      /*_.IFELSE___V62*/ meltfptr[28] = NULL;;
		  }
		;

		MELT_LOCATION ("warmelt-macro.melt:5277:/ clear");
	       /*clear *//*_.PREVLOC__V63*/ meltfptr[33] = 0;
		/*^clear */
	       /*clear *//*_.NAMED_NAME__V64*/ meltfptr[30] = 0;
		/*epilog */
	      }
	      ;
	    }
	  else
	    {
	      MELT_LOCATION ("warmelt-macro.melt:5276:/ cond.else");

	      /*^block */
	      /*anyblock */
	      {

		MELT_LOCATION ("warmelt-macro.melt:5287:/ checksignal");
		MELT_CHECK_SIGNAL ();
		;
     /*_#IS_A__L16*/ meltfnum[15] =
		  melt_is_instance_of ((melt_ptr_t)
				       ( /*_.PREVBIND__V60*/ meltfptr[34]),
				       (melt_ptr_t) (( /*!CLASS_FIXED_BINDING */ meltfrout->tabval[11])));;
		MELT_LOCATION ("warmelt-macro.melt:5287:/ cond");
		/*cond */ if ( /*_#IS_A__L16*/ meltfnum[15])	/*then */
		  {
		    /*^cond.then */
		    /*^block */
		    /*anyblock */
		    {

		      MELT_LOCATION ("warmelt-macro.melt:5290:/ cond");
		      /*cond */ if (
				     /*ifisa */
				     melt_is_instance_of ((melt_ptr_t)
							  ( /*_.VAR__V22*/
							   meltfptr[21]),
							  (melt_ptr_t) (( /*!CLASS_NAMED */ meltfrout->tabval[7])))
			)	/*then */
			{
			  /*^cond.then */
			  /*^getslot */
			  {
			    melt_ptr_t slot = NULL, obj = NULL;
			    obj =
			      (melt_ptr_t) ( /*_.VAR__V22*/ meltfptr[21])
			      /*=obj*/ ;
			    melt_object_get_field (slot, obj, 1,
						   "NAMED_NAME");
	 /*_.NAMED_NAME__V67*/ meltfptr[37] = slot;
			  };
			  ;
			}
		      else
			{	/*^cond.else */

	/*_.NAMED_NAME__V67*/ meltfptr[37] = NULL;;
			}
		      ;



		      {
			MELT_LOCATION ("warmelt-macro.melt:5288:/ locexp");
			melt_warning_str (0,
					  (melt_ptr_t) ( /*_.LOC__V20*/
							meltfptr[19]),
					  ("local let binding hides definition"),
					  (melt_ptr_t) ( /*_.NAMED_NAME__V67*/
							meltfptr[37]));
		      }
		      ;
		 /*clear *//*_.IFELSE___V66*/ meltfptr[36] = 0;
		      /*epilog */

		      MELT_LOCATION ("warmelt-macro.melt:5287:/ clear");
		 /*clear *//*_.NAMED_NAME__V67*/ meltfptr[37] = 0;
		    }
		    ;
		  }
		else
		  {		/*^cond.else */

      /*_.IFELSE___V66*/ meltfptr[36] = NULL;;
		  }
		;
		/*^compute */
		/*_.IFELSE___V62*/ meltfptr[28] =
		  /*_.IFELSE___V66*/ meltfptr[36];;
		/*epilog */

		MELT_LOCATION ("warmelt-macro.melt:5276:/ clear");
	       /*clear *//*_#IS_A__L16*/ meltfnum[15] = 0;
		/*^clear */
	       /*clear *//*_.IFELSE___V66*/ meltfptr[36] = 0;
	      }
	      ;
	    }
	  ;
	  /*_.IFELSE___V61*/ meltfptr[29] = /*_.IFELSE___V62*/ meltfptr[28];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-macro.melt:5275:/ clear");
	     /*clear *//*_#IS_A__L15*/ meltfnum[14] = 0;
	  /*^clear */
	     /*clear *//*_.IFELSE___V62*/ meltfptr[28] = 0;
	}
	;
      }
    ;
    /*_.LET___V59*/ meltfptr[38] = /*_.IFELSE___V61*/ meltfptr[29];;

    MELT_LOCATION ("warmelt-macro.melt:5273:/ clear");
	   /*clear *//*_.PREVBIND__V60*/ meltfptr[34] = 0;
    /*^clear */
	   /*clear *//*_#NULL__L14*/ meltfnum[8] = 0;
    /*^clear */
	   /*clear *//*_.IFELSE___V61*/ meltfptr[29] = 0;
    MELT_LOCATION ("warmelt-macro.melt:5293:/ quasiblock");


    MELT_LOCATION ("warmelt-macro.melt:5294:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*^rawallocobj */
    /*rawallocobj */
    {
      melt_ptr_t newobj = 0;
      melt_raw_object_create (newobj,
			      (melt_ptr_t) (( /*!CLASS_SOURCE_LET_BINDING */
					     meltfrout->tabval[12])), (5),
			      "CLASS_SOURCE_LET_BINDING");
  /*_.INST__V70*/ meltfptr[30] =
	newobj;
    };
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @LOCA_LOCATION",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V70*/ meltfptr[30])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V70*/ meltfptr[30]), (1),
			  ( /*_.LOC__V20*/ meltfptr[19]), "LOCA_LOCATION");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @SLETB_TYPE",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V70*/ meltfptr[30])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V70*/ meltfptr[30]), (2),
			  ( /*_.CTYP__V21*/ meltfptr[20]), "SLETB_TYPE");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @SLETB_BINDER",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V70*/ meltfptr[30])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V70*/ meltfptr[30]), (3),
			  ( /*_.VAR__V22*/ meltfptr[21]), "SLETB_BINDER");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @SLETB_EXPR",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V70*/ meltfptr[30])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V70*/ meltfptr[30]), (4),
			  ( /*_.EXPR__V23*/ meltfptr[22]), "SLETB_EXPR");
    ;
    /*^touchobj */

    melt_dbgtrace_written_object ( /*_.INST__V70*/ meltfptr[30],
				  "newly made instance");
    ;
    /*_.LETB__V69*/ meltfptr[33] = /*_.INST__V70*/ meltfptr[30];;
    MELT_LOCATION ("warmelt-macro.melt:5299:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.LETB__V69*/ meltfptr[33];;

    {
      MELT_LOCATION ("warmelt-macro.melt:5299:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*_.LET___V68*/ meltfptr[31] = /*_.RETURN___V71*/ meltfptr[37];;

    MELT_LOCATION ("warmelt-macro.melt:5293:/ clear");
	   /*clear *//*_.LETB__V69*/ meltfptr[33] = 0;
    /*^clear */
	   /*clear *//*_.RETURN___V71*/ meltfptr[37] = 0;
    /*_.LET___V18*/ meltfptr[16] = /*_.LET___V68*/ meltfptr[31];;

    MELT_LOCATION ("warmelt-macro.melt:5225:/ clear");
	   /*clear *//*_.CONT__V19*/ meltfptr[18] = 0;
    /*^clear */
	   /*clear *//*_.LOC__V20*/ meltfptr[19] = 0;
    /*^clear */
	   /*clear *//*_.CTYP__V21*/ meltfptr[20] = 0;
    /*^clear */
	   /*clear *//*_.VAR__V22*/ meltfptr[21] = 0;
    /*^clear */
	   /*clear *//*_.EXPR__V23*/ meltfptr[22] = 0;
    /*^clear */
	   /*clear *//*_.CURPAIR__V24*/ meltfptr[23] = 0;
    /*^clear */
	   /*clear *//*_.CURARG__V25*/ meltfptr[24] = 0;
    /*^clear */
	   /*clear *//*_.PAIR_HEAD__V26*/ meltfptr[25] = 0;
    /*^clear */
	   /*clear *//*_.SETQ___V27*/ meltfptr[26] = 0;
    /*^clear */
	   /*clear *//*_#IS_A__L7*/ meltfnum[0] = 0;
    /*^clear */
	   /*clear *//*_.IF___V28*/ meltfptr[27] = 0;
    /*^clear */
	   /*clear *//*_#IS_A__L11*/ meltfnum[9] = 0;
    /*^clear */
	   /*clear *//*_.IFELSE___V44*/ meltfptr[32] = 0;
    /*^clear */
	   /*clear *//*_#NULL__L13*/ meltfnum[7] = 0;
    /*^clear */
	   /*clear *//*_.LET___V59*/ meltfptr[38] = 0;
    /*^clear */
	   /*clear *//*_.LET___V68*/ meltfptr[31] = 0;
    MELT_LOCATION ("warmelt-macro.melt:5219:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.LET___V18*/ meltfptr[16];;

    {
      MELT_LOCATION ("warmelt-macro.melt:5219:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V6*/ meltfptr[5] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V10*/ meltfptr[6] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V12*/ meltfptr[7] = 0;
    /*^clear */
	   /*clear *//*_#NULL__L5*/ meltfnum[1] = 0;
    /*^clear */
	   /*clear *//*_.IF___V14*/ meltfptr[12] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V16*/ meltfptr[14] = 0;
    /*^clear */
	   /*clear *//*_.LET___V18*/ meltfptr[16] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("MEXPAND_LETBINDING", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_102_warmelt_macro_MEXPAND_LETBINDING_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_102_warmelt_macro_MEXPAND_LETBINDING */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_103_warmelt_macro_MEXPAND_LET (meltclosure_ptr_t meltclosp_,
					melt_ptr_t meltfirstargp_,
					const melt_argdescr_cell_t
					meltxargdescr_[],
					union meltparam_un * meltxargtab_,
					const melt_argdescr_cell_t
					meltxresdescr_[],
					union meltparam_un * meltxrestab_)
{
  long current_blocklevel_signals_meltrout_103_warmelt_macro_MEXPAND_LET_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_103_warmelt_macro_MEXPAND_LET_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 46
    melt_ptr_t mcfr_varptr[46];
#define MELTFRAM_NBVARNUM 7
    long mcfr_varnum[7];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_103_warmelt_macro_MEXPAND_LET is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_103_warmelt_macro_MEXPAND_LET_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 46; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_103_warmelt_macro_MEXPAND_LET nbval 46*/
  meltfram__.mcfr_nbvar = 46 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("MEXPAND_LET", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-macro.melt:5303:/ getarg");
 /*_.SEXPR__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.ENV__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.ENV__V3*/ meltfptr[2])) != NULL);


  /*getarg#2 */
  /*^getarg */
  if (meltxargdescr_[1] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.MEXPANDER__V4*/ meltfptr[3] =
    (meltxargtab_[1].meltbp_aptr) ? (*(meltxargtab_[1].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.MEXPANDER__V4*/ meltfptr[3])) !=
	      NULL);


  /*getarg#3 */
  /*^getarg */
  if (meltxargdescr_[2] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.MODCTX__V5*/ meltfptr[4] =
    (meltxargtab_[2].meltbp_aptr) ? (*(meltxargtab_[2].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.MODCTX__V5*/ meltfptr[4])) !=
	      NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-macro.melt:5304:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L1*/ meltfnum[0] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-macro.melt:5304:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L1*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-macro.melt:5304:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-macro.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 5304;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "mexpand_let sexpr=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.SEXPR__V2*/ meltfptr[1];
	      /*_.MELT_DEBUG_FUN__V8*/ meltfptr[7] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V7*/ meltfptr[6] = /*_.MELT_DEBUG_FUN__V8*/ meltfptr[7];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-macro.melt:5304:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V8*/ meltfptr[7] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V7*/ meltfptr[6] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-macro.melt:5304:/ quasiblock");


      /*_.PROGN___V9*/ meltfptr[7] = /*_.IF___V7*/ meltfptr[6];;
      /*^compute */
      /*_.IFCPP___V6*/ meltfptr[5] = /*_.PROGN___V9*/ meltfptr[7];;
      /*epilog */

      MELT_LOCATION ("warmelt-macro.melt:5304:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L1*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IF___V7*/ meltfptr[6] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V9*/ meltfptr[7] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V6*/ meltfptr[5] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-macro.melt:5305:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L3*/ meltfnum[1] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.SEXPR__V2*/ meltfptr[1]),
			     (melt_ptr_t) (( /*!CLASS_SEXPR */ meltfrout->
					    tabval[1])));;
      MELT_LOCATION ("warmelt-macro.melt:5305:/ cond");
      /*cond */ if ( /*_#IS_A__L3*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V11*/ meltfptr[7] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-macro.melt:5305:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check sexpr"),
				  ("warmelt-macro.melt")
				  ? ("warmelt-macro.melt") : __FILE__,
				  (5305) ? (5305) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V11*/ meltfptr[7] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V10*/ meltfptr[6] = /*_.IFELSE___V11*/ meltfptr[7];;
      /*epilog */

      MELT_LOCATION ("warmelt-macro.melt:5305:/ clear");
	     /*clear *//*_#IS_A__L3*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V11*/ meltfptr[7] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V10*/ meltfptr[6] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-macro.melt:5306:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L4*/ meltfnum[0] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.ENV__V3*/ meltfptr[2]),
			     (melt_ptr_t) (( /*!CLASS_ENVIRONMENT */
					    meltfrout->tabval[2])));;
      MELT_LOCATION ("warmelt-macro.melt:5306:/ cond");
      /*cond */ if ( /*_#IS_A__L4*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V13*/ meltfptr[12] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-macro.melt:5306:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check env"),
				  ("warmelt-macro.melt")
				  ? ("warmelt-macro.melt") : __FILE__,
				  (5306) ? (5306) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V13*/ meltfptr[12] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V12*/ meltfptr[7] = /*_.IFELSE___V13*/ meltfptr[12];;
      /*epilog */

      MELT_LOCATION ("warmelt-macro.melt:5306:/ clear");
	     /*clear *//*_#IS_A__L4*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V13*/ meltfptr[12] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V12*/ meltfptr[7] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-macro.melt:5307:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#NULL__L5*/ meltfnum[1] =
      (( /*_.MEXPANDER__V4*/ meltfptr[3]) == NULL);;
    MELT_LOCATION ("warmelt-macro.melt:5307:/ cond");
    /*cond */ if ( /*_#NULL__L5*/ meltfnum[1])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  /*^compute */
	  /*_.MEXPANDER__V4*/ meltfptr[3] = /*_.SETQ___V15*/ meltfptr[14] =
	    ( /*!MACROEXPAND_1 */ meltfrout->tabval[3]);;
	  /*_.IF___V14*/ meltfptr[12] = /*_.SETQ___V15*/ meltfptr[14];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-macro.melt:5307:/ clear");
	     /*clear *//*_.SETQ___V15*/ meltfptr[14] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

  /*_.IF___V14*/ meltfptr[12] = NULL;;
      }
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-macro.melt:5308:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_OBJECT__L6*/ meltfnum[0] =
	(melt_magic_discr ((melt_ptr_t) ( /*_.MODCTX__V5*/ meltfptr[4])) ==
	 MELTOBMAG_OBJECT);;
      MELT_LOCATION ("warmelt-macro.melt:5308:/ cond");
      /*cond */ if ( /*_#IS_OBJECT__L6*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V17*/ meltfptr[16] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-macro.melt:5308:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check modctx"),
				  ("warmelt-macro.melt")
				  ? ("warmelt-macro.melt") : __FILE__,
				  (5308) ? (5308) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V17*/ meltfptr[16] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V16*/ meltfptr[14] = /*_.IFELSE___V17*/ meltfptr[16];;
      /*epilog */

      MELT_LOCATION ("warmelt-macro.melt:5308:/ clear");
	     /*clear *//*_#IS_OBJECT__L6*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V17*/ meltfptr[16] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V16*/ meltfptr[14] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-macro.melt:5309:/ quasiblock");


    MELT_LOCATION ("warmelt-macro.melt:5310:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.SEXPR__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 2, "SEXP_CONTENTS");
  /*_.CONT__V19*/ meltfptr[18] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-macro.melt:5311:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.SEXPR__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 1, "LOCA_LOCATION");
  /*_.LOC__V20*/ meltfptr[19] = slot;
    };
    ;
 /*_.LIST_FIRST__V21*/ meltfptr[20] =
      (melt_list_first ((melt_ptr_t) ( /*_.CONT__V19*/ meltfptr[18])));;
    /*^compute */
 /*_.SECPAIR__V22*/ meltfptr[21] =
      (melt_pair_tail ((melt_ptr_t) ( /*_.LIST_FIRST__V21*/ meltfptr[20])));;
    /*^compute */
 /*_.RESTPAIR__V23*/ meltfptr[22] =
      (melt_pair_tail ((melt_ptr_t) ( /*_.SECPAIR__V22*/ meltfptr[21])));;
    /*^compute */
 /*_.BINDEXPR__V24*/ meltfptr[23] =
      (melt_pair_head ((melt_ptr_t) ( /*_.SECPAIR__V22*/ meltfptr[21])));;
    MELT_LOCATION ("warmelt-macro.melt:5315:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      /*_.NEWENV__V25*/ meltfptr[24] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!FRESH_ENV */ meltfrout->tabval[4])),
		    (melt_ptr_t) ( /*_.ENV__V3*/ meltfptr[2]), (""),
		    (union meltparam_un *) 0, "", (union meltparam_un *) 0);
    }
    ;
    /*_.BINDTUP__V26*/ meltfptr[25] = ( /*nil */ NULL);;
    /*^compute */
    /*_.BODYTUP__V27*/ meltfptr[26] = ( /*nil */ NULL);;
    MELT_LOCATION ("warmelt-macro.melt:5319:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^cond */
    /*cond */ if ( /*_.BINDEXPR__V24*/ meltfptr[23])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-macro.melt:5320:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
   /*_#IS_A__L7*/ meltfnum[0] =
	    melt_is_instance_of ((melt_ptr_t)
				 ( /*_.BINDEXPR__V24*/ meltfptr[23]),
				 (melt_ptr_t) (( /*!CLASS_SEXPR */ meltfrout->
						tabval[1])));;
	  MELT_LOCATION ("warmelt-macro.melt:5320:/ cond");
	  /*cond */ if ( /*_#IS_A__L7*/ meltfnum[0])	/*then */
	    {
	      /*^cond.then */
	      /*^block */
	      /*anyblock */
	      {

		MELT_LOCATION ("warmelt-macro.melt:5323:/ getslot");
		{
		  melt_ptr_t slot = NULL, obj = NULL;
		  obj =
		    (melt_ptr_t) ( /*_.BINDEXPR__V24*/ meltfptr[23]) /*=obj*/
		    ;
		  melt_object_get_field (slot, obj, 2, "SEXP_CONTENTS");
      /*_.SEXP_CONTENTS__V30*/ meltfptr[29] = slot;
		};
		;
     /*_.LIST_FIRST__V31*/ meltfptr[30] =
		  (melt_list_first
		   ((melt_ptr_t) ( /*_.SEXP_CONTENTS__V30*/ meltfptr[29])));;
		MELT_LOCATION ("warmelt-macro.melt:5325:/ quasiblock");


		/*^newclosure */
		     /*newclosure *//*_.LAMBDA___V33*/ meltfptr[32] =
		  (melt_ptr_t)
		  meltgc_new_closure ((meltobject_ptr_t)
				      (((melt_ptr_t)
					(MELT_PREDEF (DISCR_CLOSURE)))),
				      (meltroutine_ptr_t) (( /*!konst_9 */
							    meltfrout->
							    tabval[9])), (4));
		;
		/*^putclosedv */
		/*putclosv */
		melt_assertmsg ("putclosv checkclo",
				melt_magic_discr ((melt_ptr_t)
						  ( /*_.LAMBDA___V33*/
						   meltfptr[32])) ==
				MELTOBMAG_CLOSURE);
		melt_assertmsg ("putclosv checkoff", 0 >= 0
				&& 0 <
				melt_closure_size ((melt_ptr_t)
						   ( /*_.LAMBDA___V33*/
						    meltfptr[32])));
		((meltclosure_ptr_t) /*_.LAMBDA___V33*/ meltfptr[32])->
		  tabval[0] = (melt_ptr_t) ( /*_.LOC__V20*/ meltfptr[19]);
		;
		/*^putclosedv */
		/*putclosv */
		melt_assertmsg ("putclosv checkclo",
				melt_magic_discr ((melt_ptr_t)
						  ( /*_.LAMBDA___V33*/
						   meltfptr[32])) ==
				MELTOBMAG_CLOSURE);
		melt_assertmsg ("putclosv checkoff", 1 >= 0
				&& 1 <
				melt_closure_size ((melt_ptr_t)
						   ( /*_.LAMBDA___V33*/
						    meltfptr[32])));
		((meltclosure_ptr_t) /*_.LAMBDA___V33*/ meltfptr[32])->
		  tabval[1] = (melt_ptr_t) ( /*_.ENV__V3*/ meltfptr[2]);
		;
		/*^putclosedv */
		/*putclosv */
		melt_assertmsg ("putclosv checkclo",
				melt_magic_discr ((melt_ptr_t)
						  ( /*_.LAMBDA___V33*/
						   meltfptr[32])) ==
				MELTOBMAG_CLOSURE);
		melt_assertmsg ("putclosv checkoff", 2 >= 0
				&& 2 <
				melt_closure_size ((melt_ptr_t)
						   ( /*_.LAMBDA___V33*/
						    meltfptr[32])));
		((meltclosure_ptr_t) /*_.LAMBDA___V33*/ meltfptr[32])->
		  tabval[2] = (melt_ptr_t) ( /*_.MEXPANDER__V4*/ meltfptr[3]);
		;
		/*^putclosedv */
		/*putclosv */
		melt_assertmsg ("putclosv checkclo",
				melt_magic_discr ((melt_ptr_t)
						  ( /*_.LAMBDA___V33*/
						   meltfptr[32])) ==
				MELTOBMAG_CLOSURE);
		melt_assertmsg ("putclosv checkoff", 3 >= 0
				&& 3 <
				melt_closure_size ((melt_ptr_t)
						   ( /*_.LAMBDA___V33*/
						    meltfptr[32])));
		((meltclosure_ptr_t) /*_.LAMBDA___V33*/ meltfptr[32])->
		  tabval[3] = (melt_ptr_t) ( /*_.MODCTX__V5*/ meltfptr[4]);
		;
		/*_.LAMBDA___V32*/ meltfptr[31] =
		  /*_.LAMBDA___V33*/ meltfptr[32];;
		MELT_LOCATION ("warmelt-macro.melt:5322:/ checksignal");
		MELT_CHECK_SIGNAL ();
		;
		/*^apply */
		/*apply */
		{
		  union meltparam_un argtab[2];
		  memset (&argtab, 0, sizeof (argtab));
		  /*^apply.arg */
		  argtab[0].meltbp_aptr =
		    (melt_ptr_t *) & ( /*!DISCR_MULTIPLE */ meltfrout->
				      tabval[6]);
		  /*^apply.arg */
		  argtab[1].meltbp_aptr =
		    (melt_ptr_t *) & /*_.LAMBDA___V32*/ meltfptr[31];
		  /*_.PAIRLIST_TO_MULTIPLE__V34*/ meltfptr[33] =
		    melt_apply ((meltclosure_ptr_t)
				(( /*!PAIRLIST_TO_MULTIPLE */ meltfrout->
				  tabval[5])),
				(melt_ptr_t) ( /*_.LIST_FIRST__V31*/
					      meltfptr[30]),
				(MELTBPARSTR_PTR MELTBPARSTR_PTR ""), argtab,
				"", (union meltparam_un *) 0);
		}
		;
		MELT_LOCATION ("warmelt-macro.melt:5321:/ compute");
		/*_.BINDTUP__V26*/ meltfptr[25] =
		  /*_.SETQ___V35*/ meltfptr[34] =
		  /*_.PAIRLIST_TO_MULTIPLE__V34*/ meltfptr[33];;
		/*_.IFELSE___V29*/ meltfptr[28] =
		  /*_.SETQ___V35*/ meltfptr[34];;
		/*epilog */

		MELT_LOCATION ("warmelt-macro.melt:5320:/ clear");
	       /*clear *//*_.SEXP_CONTENTS__V30*/ meltfptr[29] = 0;
		/*^clear */
	       /*clear *//*_.LIST_FIRST__V31*/ meltfptr[30] = 0;
		/*^clear */
	       /*clear *//*_.LAMBDA___V32*/ meltfptr[31] = 0;
		/*^clear */
	       /*clear *//*_.PAIRLIST_TO_MULTIPLE__V34*/ meltfptr[33] = 0;
		/*^clear */
	       /*clear *//*_.SETQ___V35*/ meltfptr[34] = 0;
	      }
	      ;
	    }
	  else
	    {			/*^cond.else */

	      /*^block */
	      /*anyblock */
	      {




		{
		  MELT_LOCATION ("warmelt-macro.melt:5329:/ locexp");
		  /* error_plain */
		    melt_error_str ((melt_ptr_t)
				    ( /*_.LOC__V20*/ meltfptr[19]),
				    ("missing letbinding-s in LET"),
				    (melt_ptr_t) 0);
		}
		;
	       /*clear *//*_.IFELSE___V29*/ meltfptr[28] = 0;
		/*epilog */
	      }
	      ;
	    }
	  ;
	  /*^compute */
	  /*_.IF___V28*/ meltfptr[27] = /*_.IFELSE___V29*/ meltfptr[28];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-macro.melt:5319:/ clear");
	     /*clear *//*_#IS_A__L7*/ meltfnum[0] = 0;
	  /*^clear */
	     /*clear *//*_.IFELSE___V29*/ meltfptr[28] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

  /*_.IF___V28*/ meltfptr[27] = NULL;;
      }
    ;
    MELT_LOCATION ("warmelt-macro.melt:5333:/ quasiblock");


    /*^newclosure */
		 /*newclosure *//*_.LAMBDA___V37*/ meltfptr[30] =
      (melt_ptr_t)
      meltgc_new_closure ((meltobject_ptr_t)
			  (((melt_ptr_t) (MELT_PREDEF (DISCR_CLOSURE)))),
			  (meltroutine_ptr_t) (( /*!konst_14 */ meltfrout->
						tabval[14])), (1));
    ;
    /*^putclosedv */
    /*putclosv */
    melt_assertmsg ("putclosv checkclo",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.LAMBDA___V37*/ meltfptr[30])) ==
		    MELTOBMAG_CLOSURE);
    melt_assertmsg ("putclosv checkoff", 0 >= 0
		    && 0 <
		    melt_closure_size ((melt_ptr_t)
				       ( /*_.LAMBDA___V37*/ meltfptr[30])));
    ((meltclosure_ptr_t) /*_.LAMBDA___V37*/ meltfptr[30])->tabval[0] =
      (melt_ptr_t) ( /*_.NEWENV__V25*/ meltfptr[24]);
    ;
    /*_.LAMBDA___V36*/ meltfptr[29] = /*_.LAMBDA___V37*/ meltfptr[30];;
    MELT_LOCATION ("warmelt-macro.melt:5331:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & /*_.LAMBDA___V36*/ meltfptr[29];
      /*_.MULTIPLE_EVERY__V38*/ meltfptr[31] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!MULTIPLE_EVERY */ meltfrout->tabval[10])),
		    (melt_ptr_t) ( /*_.BINDTUP__V26*/ meltfptr[25]),
		    (MELTBPARSTR_PTR ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-macro.melt:5346:/ quasiblock");


    /*^newclosure */
		 /*newclosure *//*_.LAMBDA___V40*/ meltfptr[34] =
      (melt_ptr_t)
      meltgc_new_closure ((meltobject_ptr_t)
			  (((melt_ptr_t) (MELT_PREDEF (DISCR_CLOSURE)))),
			  (meltroutine_ptr_t) (( /*!konst_15 */ meltfrout->
						tabval[15])), (3));
    ;
    /*^putclosedv */
    /*putclosv */
    melt_assertmsg ("putclosv checkclo",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.LAMBDA___V40*/ meltfptr[34])) ==
		    MELTOBMAG_CLOSURE);
    melt_assertmsg ("putclosv checkoff", 0 >= 0
		    && 0 <
		    melt_closure_size ((melt_ptr_t)
				       ( /*_.LAMBDA___V40*/ meltfptr[34])));
    ((meltclosure_ptr_t) /*_.LAMBDA___V40*/ meltfptr[34])->tabval[0] =
      (melt_ptr_t) ( /*_.MEXPANDER__V4*/ meltfptr[3]);
    ;
    /*^putclosedv */
    /*putclosv */
    melt_assertmsg ("putclosv checkclo",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.LAMBDA___V40*/ meltfptr[34])) ==
		    MELTOBMAG_CLOSURE);
    melt_assertmsg ("putclosv checkoff", 1 >= 0
		    && 1 <
		    melt_closure_size ((melt_ptr_t)
				       ( /*_.LAMBDA___V40*/ meltfptr[34])));
    ((meltclosure_ptr_t) /*_.LAMBDA___V40*/ meltfptr[34])->tabval[1] =
      (melt_ptr_t) ( /*_.NEWENV__V25*/ meltfptr[24]);
    ;
    /*^putclosedv */
    /*putclosv */
    melt_assertmsg ("putclosv checkclo",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.LAMBDA___V40*/ meltfptr[34])) ==
		    MELTOBMAG_CLOSURE);
    melt_assertmsg ("putclosv checkoff", 2 >= 0
		    && 2 <
		    melt_closure_size ((melt_ptr_t)
				       ( /*_.LAMBDA___V40*/ meltfptr[34])));
    ((meltclosure_ptr_t) /*_.LAMBDA___V40*/ meltfptr[34])->tabval[2] =
      (melt_ptr_t) ( /*_.MODCTX__V5*/ meltfptr[4]);
    ;
    /*_.LAMBDA___V39*/ meltfptr[33] = /*_.LAMBDA___V40*/ meltfptr[34];;
    MELT_LOCATION ("warmelt-macro.melt:5344:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[2];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & ( /*!DISCR_MULTIPLE */ meltfrout->tabval[6]);
      /*^apply.arg */
      argtab[1].meltbp_aptr =
	(melt_ptr_t *) & /*_.LAMBDA___V39*/ meltfptr[33];
      /*_.PAIRLIST_TO_MULTIPLE__V41*/ meltfptr[28] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!PAIRLIST_TO_MULTIPLE */ meltfrout->tabval[5])),
		    (melt_ptr_t) ( /*_.RESTPAIR__V23*/ meltfptr[22]),
		    (MELTBPARSTR_PTR MELTBPARSTR_PTR ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;
    /*^compute */
    /*_.BODYTUP__V27*/ meltfptr[26] = /*_.SETQ___V42*/ meltfptr[41] =
      /*_.PAIRLIST_TO_MULTIPLE__V41*/ meltfptr[28];;
    MELT_LOCATION ("warmelt-macro.melt:5347:/ quasiblock");


    MELT_LOCATION ("warmelt-macro.melt:5348:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*^rawallocobj */
    /*rawallocobj */
    {
      melt_ptr_t newobj = 0;
      melt_raw_object_create (newobj,
			      (melt_ptr_t) (( /*!CLASS_SOURCE_LET */
					     meltfrout->tabval[16])), (4),
			      "CLASS_SOURCE_LET");
  /*_.INST__V45*/ meltfptr[44] =
	newobj;
    };
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @LOCA_LOCATION",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V45*/ meltfptr[44])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V45*/ meltfptr[44]), (1),
			  ( /*_.LOC__V20*/ meltfptr[19]), "LOCA_LOCATION");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @SLET_BINDINGS",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V45*/ meltfptr[44])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V45*/ meltfptr[44]), (2),
			  ( /*_.BINDTUP__V26*/ meltfptr[25]),
			  "SLET_BINDINGS");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @SLET_BODY",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V45*/ meltfptr[44])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V45*/ meltfptr[44]), (3),
			  ( /*_.BODYTUP__V27*/ meltfptr[26]), "SLET_BODY");
    ;
    /*^touchobj */

    melt_dbgtrace_written_object ( /*_.INST__V45*/ meltfptr[44],
				  "newly made instance");
    ;
    /*_.LETR__V44*/ meltfptr[43] = /*_.INST__V45*/ meltfptr[44];;
    MELT_LOCATION ("warmelt-macro.melt:5352:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.LETR__V44*/ meltfptr[43];;

    {
      MELT_LOCATION ("warmelt-macro.melt:5352:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*_.LET___V43*/ meltfptr[42] = /*_.RETURN___V46*/ meltfptr[45];;

    MELT_LOCATION ("warmelt-macro.melt:5347:/ clear");
	   /*clear *//*_.LETR__V44*/ meltfptr[43] = 0;
    /*^clear */
	   /*clear *//*_.RETURN___V46*/ meltfptr[45] = 0;
    /*_.LET___V18*/ meltfptr[16] = /*_.LET___V43*/ meltfptr[42];;

    MELT_LOCATION ("warmelt-macro.melt:5309:/ clear");
	   /*clear *//*_.CONT__V19*/ meltfptr[18] = 0;
    /*^clear */
	   /*clear *//*_.LOC__V20*/ meltfptr[19] = 0;
    /*^clear */
	   /*clear *//*_.LIST_FIRST__V21*/ meltfptr[20] = 0;
    /*^clear */
	   /*clear *//*_.SECPAIR__V22*/ meltfptr[21] = 0;
    /*^clear */
	   /*clear *//*_.RESTPAIR__V23*/ meltfptr[22] = 0;
    /*^clear */
	   /*clear *//*_.BINDEXPR__V24*/ meltfptr[23] = 0;
    /*^clear */
	   /*clear *//*_.NEWENV__V25*/ meltfptr[24] = 0;
    /*^clear */
	   /*clear *//*_.BINDTUP__V26*/ meltfptr[25] = 0;
    /*^clear */
	   /*clear *//*_.BODYTUP__V27*/ meltfptr[26] = 0;
    /*^clear */
	   /*clear *//*_.IF___V28*/ meltfptr[27] = 0;
    /*^clear */
	   /*clear *//*_.LAMBDA___V36*/ meltfptr[29] = 0;
    /*^clear */
	   /*clear *//*_.MULTIPLE_EVERY__V38*/ meltfptr[31] = 0;
    /*^clear */
	   /*clear *//*_.LAMBDA___V39*/ meltfptr[33] = 0;
    /*^clear */
	   /*clear *//*_.PAIRLIST_TO_MULTIPLE__V41*/ meltfptr[28] = 0;
    /*^clear */
	   /*clear *//*_.SETQ___V42*/ meltfptr[41] = 0;
    /*^clear */
	   /*clear *//*_.LET___V43*/ meltfptr[42] = 0;
    MELT_LOCATION ("warmelt-macro.melt:5303:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.LET___V18*/ meltfptr[16];;

    {
      MELT_LOCATION ("warmelt-macro.melt:5303:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V6*/ meltfptr[5] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V10*/ meltfptr[6] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V12*/ meltfptr[7] = 0;
    /*^clear */
	   /*clear *//*_#NULL__L5*/ meltfnum[1] = 0;
    /*^clear */
	   /*clear *//*_.IF___V14*/ meltfptr[12] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V16*/ meltfptr[14] = 0;
    /*^clear */
	   /*clear *//*_.LET___V18*/ meltfptr[16] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("MEXPAND_LET", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_103_warmelt_macro_MEXPAND_LET_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_103_warmelt_macro_MEXPAND_LET */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_104_warmelt_macro_LAMBDA___23__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un * meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un * meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_104_warmelt_macro_LAMBDA___23___melt =
    melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_104_warmelt_macro_LAMBDA___23___st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 3
    melt_ptr_t mcfr_varptr[3];
#define MELTFRAM_NBVARNUM 1
    long mcfr_varnum[1];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_104_warmelt_macro_LAMBDA___23__ is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_104_warmelt_macro_LAMBDA___23___st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 3; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_104_warmelt_macro_LAMBDA___23__ nbval 3*/
  meltfram__.mcfr_nbvar = 3 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("LAMBDA_", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-macro.melt:5325:/ getarg");
 /*_.B__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {

    MELT_LOCATION ("warmelt-macro.melt:5326:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#IS_NOT_A__L1*/ meltfnum[0] =
      !melt_is_instance_of ((melt_ptr_t) ( /*_.B__V2*/ meltfptr[1]),
			    (melt_ptr_t) (( /*!CLASS_SEXPR */ meltfrout->
					   tabval[0])));;
    MELT_LOCATION ("warmelt-macro.melt:5326:/ cond");
    /*cond */ if ( /*_#IS_NOT_A__L1*/ meltfnum[0])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{


	  {
	    MELT_LOCATION ("warmelt-macro.melt:5327:/ locexp");
	    /* error_plain */
	      melt_error_str ((melt_ptr_t)
			      (( /*~LOC */ meltfclos->tabval[0])),
			      ("sexpr expected in LET binding"),
			      (melt_ptr_t) 0);
	  }
	  ;
	  /*epilog */
	}
	;
      }				/*noelse */
    ;
    MELT_LOCATION ("warmelt-macro.melt:5328:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[3];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & ( /*~ENV */ meltfclos->tabval[1]);
      /*^apply.arg */
      argtab[1].meltbp_aptr =
	(melt_ptr_t *) & ( /*~MEXPANDER */ meltfclos->tabval[2]);
      /*^apply.arg */
      argtab[2].meltbp_aptr =
	(melt_ptr_t *) & ( /*~MODCTX */ meltfclos->tabval[3]);
      /*_.MEXPAND_LETBINDING__V3*/ meltfptr[2] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!MEXPAND_LETBINDING */ meltfrout->tabval[1])),
		    (melt_ptr_t) ( /*_.B__V2*/ meltfptr[1]),
		    (MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_PTR ""),
		    argtab, "", (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-macro.melt:5325:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.MEXPAND_LETBINDING__V3*/ meltfptr[2];;

    {
      MELT_LOCATION ("warmelt-macro.melt:5325:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_#IS_NOT_A__L1*/ meltfnum[0] = 0;
    /*^clear */
	   /*clear *//*_.MEXPAND_LETBINDING__V3*/ meltfptr[2] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("LAMBDA_", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_104_warmelt_macro_LAMBDA___23___melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_104_warmelt_macro_LAMBDA___23__ */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_105_warmelt_macro_LAMBDA___24__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un * meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un * meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_105_warmelt_macro_LAMBDA___24___melt =
    melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_105_warmelt_macro_LAMBDA___24___st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 13
    melt_ptr_t mcfr_varptr[13];
#define MELTFRAM_NBVARNUM 3
    long mcfr_varnum[3];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_105_warmelt_macro_LAMBDA___24__ is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_105_warmelt_macro_LAMBDA___24___st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 13; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_105_warmelt_macro_LAMBDA___24__ nbval 13*/
  meltfram__.mcfr_nbvar = 13 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("LAMBDA_", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-macro.melt:5333:/ getarg");
 /*_.SLB__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-macro.melt:5334:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L1*/ meltfnum[0] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.SLB__V2*/ meltfptr[1]),
			     (melt_ptr_t) (( /*!CLASS_SOURCE_LET_BINDING */
					    meltfrout->tabval[0])));;
      MELT_LOCATION ("warmelt-macro.melt:5334:/ cond");
      /*cond */ if ( /*_#IS_A__L1*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V4*/ meltfptr[3] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-macro.melt:5334:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("mexp.let. check slb"),
				  ("warmelt-macro.melt")
				  ? ("warmelt-macro.melt") : __FILE__,
				  (5334) ? (5334) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V4*/ meltfptr[3] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V3*/ meltfptr[2] = /*_.IFELSE___V4*/ meltfptr[3];;
      /*epilog */

      MELT_LOCATION ("warmelt-macro.melt:5334:/ clear");
	     /*clear *//*_#IS_A__L1*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V4*/ meltfptr[3] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V3*/ meltfptr[2] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-macro.melt:5335:/ quasiblock");


    MELT_LOCATION ("warmelt-macro.melt:5336:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.SLB__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 4, "SLETB_EXPR");
  /*_.SX__V6*/ meltfptr[5] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-macro.melt:5337:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    MELT_LOCATION ("warmelt-macro.melt:5338:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.SLB__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 3, "SLETB_BINDER");
  /*_.SLETB_BINDER__V7*/ meltfptr[6] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-macro.melt:5339:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.SLB__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 2, "SLETB_TYPE");
  /*_.SLETB_TYPE__V8*/ meltfptr[7] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-macro.melt:5337:/ quasiblock");


    /*^rawallocobj */
    /*rawallocobj */
    {
      melt_ptr_t newobj = 0;
      melt_raw_object_create (newobj,
			      (melt_ptr_t) (( /*!CLASS_LET_BINDING */
					     meltfrout->tabval[1])), (4),
			      "CLASS_LET_BINDING");
  /*_.INST__V10*/ meltfptr[9] =
	newobj;
    };
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @BINDER",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V10*/ meltfptr[9])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V10*/ meltfptr[9]), (0),
			  ( /*_.SLETB_BINDER__V7*/ meltfptr[6]), "BINDER");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @LETBIND_TYPE",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V10*/ meltfptr[9])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V10*/ meltfptr[9]), (1),
			  ( /*_.SLETB_TYPE__V8*/ meltfptr[7]),
			  "LETBIND_TYPE");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @LETBIND_EXPR",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V10*/ meltfptr[9])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V10*/ meltfptr[9]), (2),
			  ( /*_.SX__V6*/ meltfptr[5]), "LETBIND_EXPR");
    ;
    /*^touchobj */

    melt_dbgtrace_written_object ( /*_.INST__V10*/ meltfptr[9],
				  "newly made instance");
    ;
    /*_.LB__V9*/ meltfptr[8] = /*_.INST__V10*/ meltfptr[9];;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-macro.melt:5341:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_LIST__L2*/ meltfnum[0] =
	(melt_magic_discr ((melt_ptr_t) ( /*_.LB__V9*/ meltfptr[8])) ==
	 MELTOBMAG_LIST);;
      /*^compute */
   /*_#NOT__L3*/ meltfnum[2] =
	(!( /*_#IS_LIST__L2*/ meltfnum[0]));;
      MELT_LOCATION ("warmelt-macro.melt:5341:/ cond");
      /*cond */ if ( /*_#NOT__L3*/ meltfnum[2])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V12*/ meltfptr[11] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-macro.melt:5341:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("mexp.let not list lb"),
				  ("warmelt-macro.melt")
				  ? ("warmelt-macro.melt") : __FILE__,
				  (5341) ? (5341) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V12*/ meltfptr[11] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V11*/ meltfptr[10] = /*_.IFELSE___V12*/ meltfptr[11];;
      /*epilog */

      MELT_LOCATION ("warmelt-macro.melt:5341:/ clear");
	     /*clear *//*_#IS_LIST__L2*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_#NOT__L3*/ meltfnum[2] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V12*/ meltfptr[11] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V11*/ meltfptr[10] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-macro.melt:5342:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.LB__V9*/ meltfptr[8];
      /*_.PUT_ENV__V13*/ meltfptr[11] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!PUT_ENV */ meltfrout->tabval[2])),
		    (melt_ptr_t) (( /*~NEWENV */ meltfclos->tabval[0])),
		    (MELTBPARSTR_PTR ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;
    /*_.LET___V5*/ meltfptr[3] = /*_.PUT_ENV__V13*/ meltfptr[11];;

    MELT_LOCATION ("warmelt-macro.melt:5335:/ clear");
	   /*clear *//*_.SX__V6*/ meltfptr[5] = 0;
    /*^clear */
	   /*clear *//*_.SLETB_BINDER__V7*/ meltfptr[6] = 0;
    /*^clear */
	   /*clear *//*_.SLETB_TYPE__V8*/ meltfptr[7] = 0;
    /*^clear */
	   /*clear *//*_.LB__V9*/ meltfptr[8] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V11*/ meltfptr[10] = 0;
    /*^clear */
	   /*clear *//*_.PUT_ENV__V13*/ meltfptr[11] = 0;
    MELT_LOCATION ("warmelt-macro.melt:5333:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.LET___V5*/ meltfptr[3];;

    {
      MELT_LOCATION ("warmelt-macro.melt:5333:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V3*/ meltfptr[2] = 0;
    /*^clear */
	   /*clear *//*_.LET___V5*/ meltfptr[3] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("LAMBDA_", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_105_warmelt_macro_LAMBDA___24___melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_105_warmelt_macro_LAMBDA___24__ */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_106_warmelt_macro_LAMBDA___25__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un * meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un * meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_106_warmelt_macro_LAMBDA___25___melt =
    melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_106_warmelt_macro_LAMBDA___25___st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 3
    melt_ptr_t mcfr_varptr[3];
/*no varnum*/
#define MELTFRAM_NBVARNUM /*none*/0
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_106_warmelt_macro_LAMBDA___25__ is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_106_warmelt_macro_LAMBDA___25___st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 3; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_106_warmelt_macro_LAMBDA___25__ nbval 3*/
  meltfram__.mcfr_nbvar = 3 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("LAMBDA_", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-macro.melt:5346:/ getarg");
 /*_.E__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {

    /*^checksignal */
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[3];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & ( /*~NEWENV */ meltfclos->tabval[1]);
      /*^apply.arg */
      argtab[1].meltbp_aptr =
	(melt_ptr_t *) & ( /*~MEXPANDER */ meltfclos->tabval[0]);
      /*^apply.arg */
      argtab[2].meltbp_aptr =
	(melt_ptr_t *) & ( /*~MODCTX */ meltfclos->tabval[2]);
      /*_.MEXPANDER__V3*/ meltfptr[2] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*~MEXPANDER */ meltfclos->tabval[0])),
		    (melt_ptr_t) ( /*_.E__V2*/ meltfptr[1]),
		    (MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_PTR ""),
		    argtab, "", (union meltparam_un *) 0);
    }
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.MEXPANDER__V3*/ meltfptr[2];;

    {
      MELT_LOCATION ("warmelt-macro.melt:5346:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.MEXPANDER__V3*/ meltfptr[2] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("LAMBDA_", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_106_warmelt_macro_LAMBDA___25___melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_106_warmelt_macro_LAMBDA___25__ */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_107_warmelt_macro_YES_RECURSIVELY_CONSTRUCTIBLE (meltclosure_ptr_t
							  meltclosp_,
							  melt_ptr_t
							  meltfirstargp_,
							  const
							  melt_argdescr_cell_t
							  meltxargdescr_[],
							  union meltparam_un *
							  meltxargtab_,
							  const
							  melt_argdescr_cell_t
							  meltxresdescr_[],
							  union meltparam_un *
							  meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_107_warmelt_macro_YES_RECURSIVELY_CONSTRUCTIBLE_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_107_warmelt_macro_YES_RECURSIVELY_CONSTRUCTIBLE_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 7
    melt_ptr_t mcfr_varptr[7];
#define MELTFRAM_NBVARNUM 2
    long mcfr_varnum[2];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_107_warmelt_macro_YES_RECURSIVELY_CONSTRUCTIBLE is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct
	 frame_meltrout_107_warmelt_macro_YES_RECURSIVELY_CONSTRUCTIBLE_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 7; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_107_warmelt_macro_YES_RECURSIVELY_CONSTRUCTIBLE nbval 7*/
  meltfram__.mcfr_nbvar = 7 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("YES_RECURSIVELY_CONSTRUCTIBLE", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-macro.melt:5370:/ getarg");
 /*_.RECV__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-macro.melt:5371:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L1*/ meltfnum[0] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-macro.melt:5371:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L1*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-macro.melt:5371:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-macro.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 5371;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "yes_recursively_constructible recv";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.RECV__V2*/ meltfptr[1];
	      /*_.MELT_DEBUG_FUN__V5*/ meltfptr[4] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V4*/ meltfptr[3] = /*_.MELT_DEBUG_FUN__V5*/ meltfptr[4];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-macro.melt:5371:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V5*/ meltfptr[4] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V4*/ meltfptr[3] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-macro.melt:5371:/ quasiblock");


      /*_.PROGN___V6*/ meltfptr[4] = /*_.IF___V4*/ meltfptr[3];;
      /*^compute */
      /*_.IFCPP___V3*/ meltfptr[2] = /*_.PROGN___V6*/ meltfptr[4];;
      /*epilog */

      MELT_LOCATION ("warmelt-macro.melt:5371:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L1*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IF___V4*/ meltfptr[3] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V6*/ meltfptr[4] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V3*/ meltfptr[2] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-macro.melt:5372:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.RECV__V2*/ meltfptr[1];;

    {
      MELT_LOCATION ("warmelt-macro.melt:5372:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    MELT_LOCATION ("warmelt-macro.melt:5370:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.RETURN___V7*/ meltfptr[3];;

    {
      MELT_LOCATION ("warmelt-macro.melt:5370:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V3*/ meltfptr[2] = 0;
    /*^clear */
	   /*clear *//*_.RETURN___V7*/ meltfptr[3] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("YES_RECURSIVELY_CONSTRUCTIBLE", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_107_warmelt_macro_YES_RECURSIVELY_CONSTRUCTIBLE_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_107_warmelt_macro_YES_RECURSIVELY_CONSTRUCTIBLE */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_108_warmelt_macro_MEXPAND_LETREC (meltclosure_ptr_t meltclosp_,
					   melt_ptr_t meltfirstargp_,
					   const melt_argdescr_cell_t
					   meltxargdescr_[],
					   union meltparam_un * meltxargtab_,
					   const melt_argdescr_cell_t
					   meltxresdescr_[],
					   union meltparam_un * meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_108_warmelt_macro_MEXPAND_LETREC_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_108_warmelt_macro_MEXPAND_LETREC_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 126
    melt_ptr_t mcfr_varptr[126];
#define MELTFRAM_NBVARNUM 37
    long mcfr_varnum[37];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_108_warmelt_macro_MEXPAND_LETREC is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_108_warmelt_macro_MEXPAND_LETREC_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 126; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_108_warmelt_macro_MEXPAND_LETREC nbval 126*/
  meltfram__.mcfr_nbvar = 126 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("MEXPAND_LETREC", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-macro.melt:5383:/ getarg");
 /*_.SEXPR__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.ENV__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.ENV__V3*/ meltfptr[2])) != NULL);


  /*getarg#2 */
  /*^getarg */
  if (meltxargdescr_[1] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.MEXPANDER__V4*/ meltfptr[3] =
    (meltxargtab_[1].meltbp_aptr) ? (*(meltxargtab_[1].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.MEXPANDER__V4*/ meltfptr[3])) !=
	      NULL);


  /*getarg#3 */
  /*^getarg */
  if (meltxargdescr_[2] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.MODCTX__V5*/ meltfptr[4] =
    (meltxargtab_[2].meltbp_aptr) ? (*(meltxargtab_[2].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.MODCTX__V5*/ meltfptr[4])) !=
	      NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-macro.melt:5384:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L1*/ meltfnum[0] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-macro.melt:5384:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L1*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-macro.melt:5384:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-macro.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 5384;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "mexpand_letrec sexpr";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.SEXPR__V2*/ meltfptr[1];
	      /*_.MELT_DEBUG_FUN__V8*/ meltfptr[7] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V7*/ meltfptr[6] = /*_.MELT_DEBUG_FUN__V8*/ meltfptr[7];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-macro.melt:5384:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V8*/ meltfptr[7] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V7*/ meltfptr[6] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-macro.melt:5384:/ quasiblock");


      /*_.PROGN___V9*/ meltfptr[7] = /*_.IF___V7*/ meltfptr[6];;
      /*^compute */
      /*_.IFCPP___V6*/ meltfptr[5] = /*_.PROGN___V9*/ meltfptr[7];;
      /*epilog */

      MELT_LOCATION ("warmelt-macro.melt:5384:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L1*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IF___V7*/ meltfptr[6] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V9*/ meltfptr[7] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V6*/ meltfptr[5] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-macro.melt:5385:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L3*/ meltfnum[1] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.SEXPR__V2*/ meltfptr[1]),
			     (melt_ptr_t) (( /*!CLASS_SEXPR */ meltfrout->
					    tabval[1])));;
      MELT_LOCATION ("warmelt-macro.melt:5385:/ cond");
      /*cond */ if ( /*_#IS_A__L3*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V11*/ meltfptr[7] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-macro.melt:5385:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check sexpr"),
				  ("warmelt-macro.melt")
				  ? ("warmelt-macro.melt") : __FILE__,
				  (5385) ? (5385) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V11*/ meltfptr[7] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V10*/ meltfptr[6] = /*_.IFELSE___V11*/ meltfptr[7];;
      /*epilog */

      MELT_LOCATION ("warmelt-macro.melt:5385:/ clear");
	     /*clear *//*_#IS_A__L3*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V11*/ meltfptr[7] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V10*/ meltfptr[6] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-macro.melt:5386:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L4*/ meltfnum[0] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.ENV__V3*/ meltfptr[2]),
			     (melt_ptr_t) (( /*!CLASS_ENVIRONMENT */
					    meltfrout->tabval[2])));;
      MELT_LOCATION ("warmelt-macro.melt:5386:/ cond");
      /*cond */ if ( /*_#IS_A__L4*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V13*/ meltfptr[12] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-macro.melt:5386:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check env"),
				  ("warmelt-macro.melt")
				  ? ("warmelt-macro.melt") : __FILE__,
				  (5386) ? (5386) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V13*/ meltfptr[12] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V12*/ meltfptr[7] = /*_.IFELSE___V13*/ meltfptr[12];;
      /*epilog */

      MELT_LOCATION ("warmelt-macro.melt:5386:/ clear");
	     /*clear *//*_#IS_A__L4*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V13*/ meltfptr[12] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V12*/ meltfptr[7] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-macro.melt:5387:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_CLOSURE__L5*/ meltfnum[1] =
	(melt_magic_discr ((melt_ptr_t) ( /*_.MEXPANDER__V4*/ meltfptr[3])) ==
	 MELTOBMAG_CLOSURE);;
      MELT_LOCATION ("warmelt-macro.melt:5387:/ cond");
      /*cond */ if ( /*_#IS_CLOSURE__L5*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V15*/ meltfptr[14] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-macro.melt:5387:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check mexpander"),
				  ("warmelt-macro.melt")
				  ? ("warmelt-macro.melt") : __FILE__,
				  (5387) ? (5387) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V15*/ meltfptr[14] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V14*/ meltfptr[12] = /*_.IFELSE___V15*/ meltfptr[14];;
      /*epilog */

      MELT_LOCATION ("warmelt-macro.melt:5387:/ clear");
	     /*clear *//*_#IS_CLOSURE__L5*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V15*/ meltfptr[14] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V14*/ meltfptr[12] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-macro.melt:5388:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_OBJECT__L6*/ meltfnum[0] =
	(melt_magic_discr ((melt_ptr_t) ( /*_.MODCTX__V5*/ meltfptr[4])) ==
	 MELTOBMAG_OBJECT);;
      MELT_LOCATION ("warmelt-macro.melt:5388:/ cond");
      /*cond */ if ( /*_#IS_OBJECT__L6*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V17*/ meltfptr[16] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-macro.melt:5388:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check modctx"),
				  ("warmelt-macro.melt")
				  ? ("warmelt-macro.melt") : __FILE__,
				  (5388) ? (5388) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V17*/ meltfptr[16] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V16*/ meltfptr[14] = /*_.IFELSE___V17*/ meltfptr[16];;
      /*epilog */

      MELT_LOCATION ("warmelt-macro.melt:5388:/ clear");
	     /*clear *//*_#IS_OBJECT__L6*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V17*/ meltfptr[16] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V16*/ meltfptr[14] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-macro.melt:5389:/ quasiblock");


    MELT_LOCATION ("warmelt-macro.melt:5390:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.SEXPR__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 2, "SEXP_CONTENTS");
  /*_.CONT__V19*/ meltfptr[18] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-macro.melt:5391:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.SEXPR__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 1, "LOCA_LOCATION");
  /*_.LOC__V20*/ meltfptr[19] = slot;
    };
    ;
 /*_.LIST_FIRST__V21*/ meltfptr[20] =
      (melt_list_first ((melt_ptr_t) ( /*_.CONT__V19*/ meltfptr[18])));;
    /*^compute */
 /*_.SECPAIR__V22*/ meltfptr[21] =
      (melt_pair_tail ((melt_ptr_t) ( /*_.LIST_FIRST__V21*/ meltfptr[20])));;
    /*^compute */
 /*_.RESTPAIR__V23*/ meltfptr[22] =
      (melt_pair_tail ((melt_ptr_t) ( /*_.SECPAIR__V22*/ meltfptr[21])));;
    /*^compute */
 /*_.BINDEXPR__V24*/ meltfptr[23] =
      (melt_pair_head ((melt_ptr_t) ( /*_.SECPAIR__V22*/ meltfptr[21])));;
    MELT_LOCATION ("warmelt-macro.melt:5395:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      /*_.NEWENV__V25*/ meltfptr[24] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!FRESH_ENV */ meltfrout->tabval[3])),
		    (melt_ptr_t) ( /*_.ENV__V3*/ meltfptr[2]), (""),
		    (union meltparam_un *) 0, "", (union meltparam_un *) 0);
    }
    ;
 /*_#NBIND__L7*/ meltfnum[1] = 0;;
    /*^compute */
    /*_.BINDTUP__V26*/ meltfptr[25] = ( /*nil */ NULL);;
    /*^compute */
    /*_.SRCBINDTUP__V27*/ meltfptr[26] = ( /*nil */ NULL);;
    /*^compute */
    /*_.VARTUP__V28*/ meltfptr[27] = ( /*nil */ NULL);;
    /*^compute */
    /*_.EXPRTUP__V29*/ meltfptr[28] = ( /*nil */ NULL);;
    /*^compute */
    /*_.BODYTUP__V30*/ meltfptr[29] = ( /*nil */ NULL);;
    MELT_LOCATION ("warmelt-macro.melt:5404:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^cond */
    /*cond */ if ( /*_.BINDEXPR__V24*/ meltfptr[23])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-macro.melt:5405:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
   /*_#IS_A__L8*/ meltfnum[0] =
	    melt_is_instance_of ((melt_ptr_t)
				 ( /*_.BINDEXPR__V24*/ meltfptr[23]),
				 (melt_ptr_t) (( /*!CLASS_SEXPR */ meltfrout->
						tabval[1])));;
	  MELT_LOCATION ("warmelt-macro.melt:5405:/ cond");
	  /*cond */ if ( /*_#IS_A__L8*/ meltfnum[0])	/*then */
	    {
	      /*^cond.then */
	      /*^block */
	      /*anyblock */
	      {

		MELT_LOCATION ("warmelt-macro.melt:5406:/ quasiblock");


		MELT_LOCATION ("warmelt-macro.melt:5408:/ getslot");
		{
		  melt_ptr_t slot = NULL, obj = NULL;
		  obj =
		    (melt_ptr_t) ( /*_.BINDEXPR__V24*/ meltfptr[23]) /*=obj*/
		    ;
		  melt_object_get_field (slot, obj, 2, "SEXP_CONTENTS");
      /*_.SEXP_CONTENTS__V34*/ meltfptr[33] = slot;
		};
		;
     /*_.LIST_FIRST__V35*/ meltfptr[34] =
		  (melt_list_first
		   ((melt_ptr_t) ( /*_.SEXP_CONTENTS__V34*/ meltfptr[33])));;
		MELT_LOCATION ("warmelt-macro.melt:5410:/ quasiblock");


		/*^newclosure */
		     /*newclosure *//*_.LAMBDA___V37*/ meltfptr[36] =
		  (melt_ptr_t)
		  meltgc_new_closure ((meltobject_ptr_t)
				      (((melt_ptr_t)
					(MELT_PREDEF (DISCR_CLOSURE)))),
				      (meltroutine_ptr_t) (( /*!konst_7 */
							    meltfrout->
							    tabval[7])), (1));
		;
		/*^putclosedv */
		/*putclosv */
		melt_assertmsg ("putclosv checkclo",
				melt_magic_discr ((melt_ptr_t)
						  ( /*_.LAMBDA___V37*/
						   meltfptr[36])) ==
				MELTOBMAG_CLOSURE);
		melt_assertmsg ("putclosv checkoff", 0 >= 0
				&& 0 <
				melt_closure_size ((melt_ptr_t)
						   ( /*_.LAMBDA___V37*/
						    meltfptr[36])));
		((meltclosure_ptr_t) /*_.LAMBDA___V37*/ meltfptr[36])->
		  tabval[0] = (melt_ptr_t) ( /*_.LOC__V20*/ meltfptr[19]);
		;
		/*_.LAMBDA___V36*/ meltfptr[35] =
		  /*_.LAMBDA___V37*/ meltfptr[36];;
		MELT_LOCATION ("warmelt-macro.melt:5407:/ checksignal");
		MELT_CHECK_SIGNAL ();
		;
		/*^apply */
		/*apply */
		{
		  union meltparam_un argtab[2];
		  memset (&argtab, 0, sizeof (argtab));
		  /*^apply.arg */
		  argtab[0].meltbp_aptr =
		    (melt_ptr_t *) & ( /*!DISCR_MULTIPLE */ meltfrout->
				      tabval[5]);
		  /*^apply.arg */
		  argtab[1].meltbp_aptr =
		    (melt_ptr_t *) & /*_.LAMBDA___V36*/ meltfptr[35];
		  /*_.RECBINDTUP__V38*/ meltfptr[37] =
		    melt_apply ((meltclosure_ptr_t)
				(( /*!PAIRLIST_TO_MULTIPLE */ meltfrout->
				  tabval[4])),
				(melt_ptr_t) ( /*_.LIST_FIRST__V35*/
					      meltfptr[34]),
				(MELTBPARSTR_PTR MELTBPARSTR_PTR ""), argtab,
				"", (union meltparam_un *) 0);
		}
		;
     /*_#NBRECBIND__L9*/ meltfnum[8] =
		  (melt_multiple_length
		   ((melt_ptr_t) ( /*_.RECBINDTUP__V38*/ meltfptr[37])));;
		/*^compute */
     /*_.RECSEXPRTUP__V39*/ meltfptr[38] =
		  (meltgc_new_multiple
		   ((meltobject_ptr_t)
		    (( /*!DISCR_MULTIPLE */ meltfrout->tabval[5])),
		    ( /*_#NBRECBIND__L9*/ meltfnum[8])));;
		MELT_LOCATION ("warmelt-macro.melt:5417:/ compute");
		/*_#NBIND__L7*/ meltfnum[1] = /*_#SETQ___L10*/ meltfnum[9] =
		  /*_#NBRECBIND__L9*/ meltfnum[8];;
     /*_.MAKE_MULTIPLE__V40*/ meltfptr[39] =
		  (meltgc_new_multiple
		   ((meltobject_ptr_t)
		    (( /*!DISCR_MULTIPLE */ meltfrout->tabval[5])),
		    ( /*_#NBIND__L7*/ meltfnum[1])));;
		MELT_LOCATION ("warmelt-macro.melt:5418:/ compute");
		/*_.BINDTUP__V26*/ meltfptr[25] =
		  /*_.SETQ___V41*/ meltfptr[40] =
		  /*_.MAKE_MULTIPLE__V40*/ meltfptr[39];;
     /*_.MAKE_MULTIPLE__V42*/ meltfptr[41] =
		  (meltgc_new_multiple
		   ((meltobject_ptr_t)
		    (( /*!DISCR_MULTIPLE */ meltfrout->tabval[5])),
		    ( /*_#NBIND__L7*/ meltfnum[1])));;
		MELT_LOCATION ("warmelt-macro.melt:5419:/ compute");
		/*_.SRCBINDTUP__V27*/ meltfptr[26] =
		  /*_.SETQ___V43*/ meltfptr[42] =
		  /*_.MAKE_MULTIPLE__V42*/ meltfptr[41];;
     /*_.MAKE_MULTIPLE__V44*/ meltfptr[43] =
		  (meltgc_new_multiple
		   ((meltobject_ptr_t)
		    (( /*!DISCR_MULTIPLE */ meltfrout->tabval[5])),
		    ( /*_#NBIND__L7*/ meltfnum[1])));;
		MELT_LOCATION ("warmelt-macro.melt:5420:/ compute");
		/*_.VARTUP__V28*/ meltfptr[27] =
		  /*_.SETQ___V45*/ meltfptr[44] =
		  /*_.MAKE_MULTIPLE__V44*/ meltfptr[43];;
     /*_.MAKE_MULTIPLE__V46*/ meltfptr[45] =
		  (meltgc_new_multiple
		   ((meltobject_ptr_t)
		    (( /*!DISCR_MULTIPLE */ meltfrout->tabval[5])),
		    ( /*_#NBIND__L7*/ meltfnum[1])));;
		MELT_LOCATION ("warmelt-macro.melt:5421:/ compute");
		/*_.EXPRTUP__V29*/ meltfptr[28] =
		  /*_.SETQ___V47*/ meltfptr[46] =
		  /*_.MAKE_MULTIPLE__V46*/ meltfptr[45];;
		/*citerblock FOREACH_IN_MULTIPLE */
		{
		  /* start foreach_in_multiple meltcit1__EACHTUP */
		  long meltcit1__EACHTUP_ln =
		    melt_multiple_length ((melt_ptr_t) /*_.RECBINDTUP__V38*/
					  meltfptr[37]);
		  for ( /*_#BINDIX__L11*/ meltfnum[10] = 0;
		       ( /*_#BINDIX__L11*/ meltfnum[10] >= 0)
		       && ( /*_#BINDIX__L11*/ meltfnum[10] <
			   meltcit1__EACHTUP_ln);
	/*_#BINDIX__L11*/ meltfnum[10]++)
		    {
		      /*_.CURBINDEXPR__V48*/ meltfptr[47] =
			melt_multiple_nth ((melt_ptr_t)
					   ( /*_.RECBINDTUP__V38*/
					    meltfptr[37]), /*_#BINDIX__L11*/
					   meltfnum[10]);




#if MELT_HAVE_DEBUG
		      MELT_LOCATION ("warmelt-macro.melt:5426:/ cppif.then");
		      /*^block */
		      /*anyblock */
		      {


			{
			  /*^locexp */
			  /*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
			  melt_dbgcounter++;
#endif
			  ;
			}
			;
			/*^checksignal */
			MELT_CHECK_SIGNAL ();
			;
	/*_#MELT_NEED_DBG__L12*/ meltfnum[11] =
			  /*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
			  ( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
			  0	/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
			  ;;
			MELT_LOCATION ("warmelt-macro.melt:5426:/ cond");
			/*cond */ if ( /*_#MELT_NEED_DBG__L12*/ meltfnum[11])	/*then */
			  {
			    /*^cond.then */
			    /*^block */
			    /*anyblock */
			    {

	  /*_#THE_MELTCALLCOUNT__L13*/ meltfnum[12] =
#ifdef meltcallcount
				meltcallcount	/* the_meltcallcount */
#else
				0L
#endif /* meltcallcount the_meltcallcount */
				;;
			      MELT_LOCATION
				("warmelt-macro.melt:5426:/ checksignal");
			      MELT_CHECK_SIGNAL ();
			      ;
			      /*^apply */
			      /*apply */
			      {
				union meltparam_un argtab[5];
				memset (&argtab, 0, sizeof (argtab));
				/*^apply.arg */
				argtab[0].meltbp_long =
				  /*_#THE_MELTCALLCOUNT__L13*/ meltfnum[12];
				/*^apply.arg */
				argtab[1].meltbp_cstring =
				  "warmelt-macro.melt";
				/*^apply.arg */
				argtab[2].meltbp_long = 5426;
				/*^apply.arg */
				argtab[3].meltbp_cstring =
				  "mexpand_letrec firstloop curbindexpr";
				/*^apply.arg */
				argtab[4].meltbp_aptr =
				  (melt_ptr_t *) & /*_.CURBINDEXPR__V48*/
				  meltfptr[47];
				/*_.MELT_DEBUG_FUN__V51*/ meltfptr[50] =
				  melt_apply ((meltclosure_ptr_t)
					      (( /*!MELT_DEBUG_FUN */
						meltfrout->tabval[0])),
					      (melt_ptr_t) (( /*nil */ NULL)),
					      (MELTBPARSTR_LONG
					       MELTBPARSTR_CSTRING
					       MELTBPARSTR_LONG
					       MELTBPARSTR_CSTRING
					       MELTBPARSTR_PTR ""), argtab,
					      "", (union meltparam_un *) 0);
			      }
			      ;
			      /*_.IF___V50*/ meltfptr[49] =
				/*_.MELT_DEBUG_FUN__V51*/ meltfptr[50];;
			      /*epilog */

			      MELT_LOCATION
				("warmelt-macro.melt:5426:/ clear");
		    /*clear *//*_#THE_MELTCALLCOUNT__L13*/
				meltfnum[12] = 0;
			      /*^clear */
		    /*clear *//*_.MELT_DEBUG_FUN__V51*/ meltfptr[50]
				= 0;
			    }
			    ;
			  }
			else
			  {	/*^cond.else */

	 /*_.IF___V50*/ meltfptr[49] = NULL;;
			  }
			;
			MELT_LOCATION
			  ("warmelt-macro.melt:5426:/ quasiblock");


			/*_.PROGN___V52*/ meltfptr[50] =
			  /*_.IF___V50*/ meltfptr[49];;
			/*^compute */
			/*_.IFCPP___V49*/ meltfptr[48] =
			  /*_.PROGN___V52*/ meltfptr[50];;
			/*epilog */

			MELT_LOCATION ("warmelt-macro.melt:5426:/ clear");
		  /*clear *//*_#MELT_NEED_DBG__L12*/ meltfnum[11] = 0;
			/*^clear */
		  /*clear *//*_.IF___V50*/ meltfptr[49] = 0;
			/*^clear */
		  /*clear *//*_.PROGN___V52*/ meltfptr[50] = 0;
		      }

#else /*MELT_HAVE_DEBUG */
		      /*^cppif.else */
		      /*_.IFCPP___V49*/ meltfptr[48] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
		      ;
		      MELT_LOCATION ("warmelt-macro.melt:5427:/ checksignal");
		      MELT_CHECK_SIGNAL ();
		      ;
      /*_#IS_NOT_A__L14*/ meltfnum[12] =
			!melt_is_instance_of ((melt_ptr_t)
					      ( /*_.CURBINDEXPR__V48*/
					       meltfptr[47]),
					      (melt_ptr_t) (( /*!CLASS_SEXPR */ meltfrout->tabval[1])));;
		      MELT_LOCATION ("warmelt-macro.melt:5427:/ cond");
		      /*cond */ if ( /*_#IS_NOT_A__L14*/ meltfnum[12])	/*then */
			{
			  /*^cond.then */
			  /*^block */
			  /*anyblock */
			  {

			    MELT_LOCATION
			      ("warmelt-macro.melt:5429:/ quasiblock");


	/*_.RETVAL___V1*/ meltfptr[0] = NULL;;

			    {
			      MELT_LOCATION
				("warmelt-macro.melt:5429:/ locexp");
			      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
			      if (meltxresdescr_ && meltxresdescr_[0]
				  && meltxrestab_)
				melt_warn_for_no_expected_secondary_results
				  ();
			      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
			      ;
			    }
			    ;
			    /*^finalreturn */
			    ;
			    /*finalret */ goto labend_rout;
			    /*_.IF___V53*/ meltfptr[49] =
			      /*_.RETURN___V54*/ meltfptr[50];;
			    /*epilog */

			    MELT_LOCATION ("warmelt-macro.melt:5427:/ clear");
		  /*clear *//*_.RETURN___V54*/ meltfptr[50] = 0;
			  }
			  ;
			}
		      else
			{	/*^cond.else */

       /*_.IF___V53*/ meltfptr[49] = NULL;;
			}
		      ;
		      MELT_LOCATION ("warmelt-macro.melt:5430:/ quasiblock");


		      /*^cond */
		      /*cond */ if (
				     /*ifisa */
				     melt_is_instance_of ((melt_ptr_t)
							  ( /*_.CURBINDEXPR__V48*/ meltfptr[47]),
							  (melt_ptr_t) (( /*!CLASS_SEXPR */ meltfrout->tabval[1])))
			)	/*then */
			{
			  /*^cond.then */
			  /*^getslot */
			  {
			    melt_ptr_t slot = NULL, obj = NULL;
			    obj =
			      (melt_ptr_t) ( /*_.CURBINDEXPR__V48*/
					    meltfptr[47]) /*=obj*/ ;
			    melt_object_get_field (slot, obj, 2,
						   "SEXP_CONTENTS");
	/*_.CURCONT__V55*/ meltfptr[50] = slot;
			  };
			  ;
			}
		      else
			{	/*^cond.else */

       /*_.CURCONT__V55*/ meltfptr[50] = NULL;;
			}
		      ;
		      MELT_LOCATION ("warmelt-macro.melt:5431:/ cond");
		      /*cond */ if (
				     /*ifisa */
				     melt_is_instance_of ((melt_ptr_t)
							  ( /*_.CURBINDEXPR__V48*/ meltfptr[47]),
							  (melt_ptr_t) (( /*!CLASS_LOCATED */ meltfrout->tabval[8])))
			)	/*then */
			{
			  /*^cond.then */
			  /*^getslot */
			  {
			    melt_ptr_t slot = NULL, obj = NULL;
			    obj =
			      (melt_ptr_t) ( /*_.CURBINDEXPR__V48*/
					    meltfptr[47]) /*=obj*/ ;
			    melt_object_get_field (slot, obj, 1,
						   "LOCA_LOCATION");
	/*_.CURLOC__V56*/ meltfptr[55] = slot;
			  };
			  ;
			}
		      else
			{	/*^cond.else */

       /*_.CURLOC__V56*/ meltfptr[55] = NULL;;
			}
		      ;
		      /*^compute */
      /*_.CURPAIR__V57*/ meltfptr[56] =
			(melt_list_first
			 ((melt_ptr_t) ( /*_.CURCONT__V55*/ meltfptr[50])));;
		      /*^compute */
      /*_.CURCOMP__V58*/ meltfptr[57] =
			(melt_pair_head
			 ((melt_ptr_t) ( /*_.CURPAIR__V57*/ meltfptr[56])));;
		      /*^compute */
		      /*_.CURSYMB__V59*/ meltfptr[58] = ( /*nil */ NULL);;
		      /*^compute */
		      /*_.CURSEXPR__V60*/ meltfptr[59] = ( /*nil */ NULL);;
		      MELT_LOCATION ("warmelt-macro.melt:5437:/ checksignal");
		      MELT_CHECK_SIGNAL ();
		      ;
      /*_#IS_A__L15*/ meltfnum[11] =
			melt_is_instance_of ((melt_ptr_t)
					     ( /*_.CURCOMP__V58*/
					      meltfptr[57]),
					     (melt_ptr_t) (( /*!CLASS_KEYWORD */ meltfrout->tabval[9])));;
		      MELT_LOCATION ("warmelt-macro.melt:5437:/ cond");
		      /*cond */ if ( /*_#IS_A__L15*/ meltfnum[11])	/*then */
			{
			  /*^cond.then */
			  /*^block */
			  /*anyblock */
			  {




			    {
			      MELT_LOCATION
				("warmelt-macro.melt:5438:/ locexp");
			      /* error_plain */
				melt_error_str ((melt_ptr_t)
						( /*_.CURLOC__V56*/
						 meltfptr[55]),
						("keyword invalid in LETREC binding"),
						(melt_ptr_t) 0);
			    }
			    ;
		  /*clear *//*_.IFELSE___V61*/ meltfptr[60] = 0;
			    /*epilog */
			  }
			  ;
			}
		      else
			{
			  MELT_LOCATION
			    ("warmelt-macro.melt:5437:/ cond.else");

			  /*^block */
			  /*anyblock */
			  {

			    MELT_LOCATION
			      ("warmelt-macro.melt:5440:/ checksignal");
			    MELT_CHECK_SIGNAL ();
			    ;
	/*_#IS_A__L16*/ meltfnum[15] =
			      melt_is_instance_of ((melt_ptr_t)
						   ( /*_.CURCOMP__V58*/
						    meltfptr[57]),
						   (melt_ptr_t) (( /*!CLASS_SYMBOL */ meltfrout->tabval[10])));;
			    MELT_LOCATION ("warmelt-macro.melt:5440:/ cond");
			    /*cond */ if ( /*_#IS_A__L16*/ meltfnum[15])	/*then */
			      {
				/*^cond.then */
				/*^block */
				/*anyblock */
				{

				  MELT_LOCATION
				    ("warmelt-macro.melt:5441:/ compute");
				  /*_.CURSYMB__V59*/ meltfptr[58] =
				    /*_.SETQ___V63*/ meltfptr[62] =
				    /*_.CURCOMP__V58*/ meltfptr[57];;
				  /*_.IFELSE___V62*/ meltfptr[61] =
				    /*_.SETQ___V63*/ meltfptr[62];;
				  /*epilog */

				  MELT_LOCATION
				    ("warmelt-macro.melt:5440:/ clear");
		    /*clear *//*_.SETQ___V63*/ meltfptr[62] = 0;
				}
				;
			      }
			    else
			      {	/*^cond.else */

				/*^block */
				/*anyblock */
				{


				  {
				    MELT_LOCATION
				      ("warmelt-macro.melt:5444:/ locexp");
				    /* error_plain */
				      melt_error_str ((melt_ptr_t)
						      ( /*_.CURLOC__V56*/
						       meltfptr[55]),
						      ("invalid LETREC binding - expecting (<symbol> <constructive-expr>)"),
						      (melt_ptr_t) 0);
				  }
				  ;
				  MELT_LOCATION
				    ("warmelt-macro.melt:5443:/ quasiblock");


				  /*epilog */
				}
				;
			      }
			    ;
			    /*_.IFELSE___V61*/ meltfptr[60] =
			      /*_.IFELSE___V62*/ meltfptr[61];;
			    /*epilog */

			    MELT_LOCATION ("warmelt-macro.melt:5437:/ clear");
		  /*clear *//*_#IS_A__L16*/ meltfnum[15] = 0;
			    /*^clear */
		  /*clear *//*_.IFELSE___V62*/ meltfptr[61] = 0;
			  }
			  ;
			}
		      ;
      /*_.PAIR_TAIL__V64*/ meltfptr[62] =
			(melt_pair_tail
			 ((melt_ptr_t) ( /*_.CURPAIR__V57*/ meltfptr[56])));;
		      MELT_LOCATION ("warmelt-macro.melt:5446:/ compute");
		      /*_.CURPAIR__V57*/ meltfptr[56] =
			/*_.SETQ___V65*/ meltfptr[61] =
			/*_.PAIR_TAIL__V64*/ meltfptr[62];;
      /*_.PAIR_HEAD__V66*/ meltfptr[65] =
			(melt_pair_head
			 ((melt_ptr_t) ( /*_.CURPAIR__V57*/ meltfptr[56])));;
		      MELT_LOCATION ("warmelt-macro.melt:5447:/ compute");
		      /*_.CURCOMP__V58*/ meltfptr[57] =
			/*_.SETQ___V67*/ meltfptr[66] =
			/*_.PAIR_HEAD__V66*/ meltfptr[65];;
		      MELT_LOCATION ("warmelt-macro.melt:5448:/ checksignal");
		      MELT_CHECK_SIGNAL ();
		      ;
      /*_#IS_A__L17*/ meltfnum[15] =
			melt_is_instance_of ((melt_ptr_t)
					     ( /*_.CURCOMP__V58*/
					      meltfptr[57]),
					     (melt_ptr_t) (( /*!CLASS_SEXPR */
							    meltfrout->
							    tabval[1])));;
		      MELT_LOCATION ("warmelt-macro.melt:5448:/ cond");
		      /*cond */ if ( /*_#IS_A__L17*/ meltfnum[15])	/*then */
			{
			  /*^cond.then */
			  /*^block */
			  /*anyblock */
			  {

			    MELT_LOCATION
			      ("warmelt-macro.melt:5449:/ checksignal");
			    MELT_CHECK_SIGNAL ();
			    ;
	/*_#IS_A__L18*/ meltfnum[17] =
			      melt_is_instance_of ((melt_ptr_t)
						   ( /*_.CURSYMB__V59*/
						    meltfptr[58]),
						   (melt_ptr_t) (( /*!CLASS_SYMBOL */ meltfrout->tabval[10])));;
			    MELT_LOCATION ("warmelt-macro.melt:5449:/ cond");
			    /*cond */ if ( /*_#IS_A__L18*/ meltfnum[17])	/*then */
			      {
				/*^cond.then */
				/*^block */
				/*anyblock */
				{

				  MELT_LOCATION
				    ("warmelt-macro.melt:5451:/ compute");
				  /*_.CURSEXPR__V60*/ meltfptr[59] =
				    /*_.SETQ___V68*/ meltfptr[67] =
				    /*_.CURCOMP__V58*/ meltfptr[57];;

				  {
				    MELT_LOCATION
				      ("warmelt-macro.melt:5452:/ locexp");
				    meltgc_multiple_put_nth ((melt_ptr_t)
							     ( /*_.RECSEXPRTUP__V39*/ meltfptr[38]), ( /*_#BINDIX__L11*/ meltfnum[10]), (melt_ptr_t) ( /*_.CURSEXPR__V60*/ meltfptr[59]));
				  }
				  ;

				  {
				    MELT_LOCATION
				      ("warmelt-macro.melt:5453:/ locexp");
				    meltgc_multiple_put_nth ((melt_ptr_t)
							     ( /*_.VARTUP__V28*/ meltfptr[27]), ( /*_#BINDIX__L11*/ meltfnum[10]), (melt_ptr_t) ( /*_.CURSYMB__V59*/ meltfptr[58]));
				  }
				  ;
				  MELT_LOCATION
				    ("warmelt-macro.melt:5450:/ quasiblock");


				  /*epilog */

				  MELT_LOCATION
				    ("warmelt-macro.melt:5449:/ clear");
		    /*clear *//*_.SETQ___V68*/ meltfptr[67] = 0;
				}
				;
			      }	/*noelse */
			    ;
			    /*epilog */

			    MELT_LOCATION ("warmelt-macro.melt:5448:/ clear");
		  /*clear *//*_#IS_A__L18*/ meltfnum[17] = 0;
			  }
			  ;
			}
		      else
			{	/*^cond.else */

			  /*^block */
			  /*anyblock */
			  {


			    {
			      MELT_LOCATION
				("warmelt-macro.melt:5455:/ locexp");
			      /* error_plain */
				melt_error_str ((melt_ptr_t)
						( /*_.CURLOC__V56*/
						 meltfptr[55]),
						("invalid LETREC binding - missing constructive expression"),
						(melt_ptr_t) 0);
			    }
			    ;
			    /*epilog */
			  }
			  ;
			}
		      ;
		      MELT_LOCATION ("warmelt-macro.melt:5456:/ checksignal");
		      MELT_CHECK_SIGNAL ();
		      ;
      /*_.PAIR_TAIL__V69*/ meltfptr[67] =
			(melt_pair_tail
			 ((melt_ptr_t) ( /*_.CURPAIR__V57*/ meltfptr[56])));;
		      MELT_LOCATION ("warmelt-macro.melt:5456:/ cond");
		      /*cond */ if ( /*_.PAIR_TAIL__V69*/ meltfptr[67])	/*then */
			{
			  /*^cond.then */
			  /*^block */
			  /*anyblock */
			  {


			    {
			      MELT_LOCATION
				("warmelt-macro.melt:5457:/ locexp");
			      /* error_plain */
				melt_error_str ((melt_ptr_t)
						( /*_.CURLOC__V56*/
						 meltfptr[55]),
						("invalid LETREC binding - more than two components"),
						(melt_ptr_t) 0);
			    }
			    ;
			    /*epilog */
			  }
			  ;
			}	/*noelse */
		      ;

		      MELT_LOCATION ("warmelt-macro.melt:5430:/ clear");
		/*clear *//*_.CURCONT__V55*/ meltfptr[50] = 0;
		      /*^clear */
		/*clear *//*_.CURLOC__V56*/ meltfptr[55] = 0;
		      /*^clear */
		/*clear *//*_.CURPAIR__V57*/ meltfptr[56] = 0;
		      /*^clear */
		/*clear *//*_.CURCOMP__V58*/ meltfptr[57] = 0;
		      /*^clear */
		/*clear *//*_.CURSYMB__V59*/ meltfptr[58] = 0;
		      /*^clear */
		/*clear *//*_.CURSEXPR__V60*/ meltfptr[59] = 0;
		      /*^clear */
		/*clear *//*_#IS_A__L15*/ meltfnum[11] = 0;
		      /*^clear */
		/*clear *//*_.IFELSE___V61*/ meltfptr[60] = 0;
		      /*^clear */
		/*clear *//*_.PAIR_TAIL__V64*/ meltfptr[62] = 0;
		      /*^clear */
		/*clear *//*_.SETQ___V65*/ meltfptr[61] = 0;
		      /*^clear */
		/*clear *//*_.PAIR_HEAD__V66*/ meltfptr[65] = 0;
		      /*^clear */
		/*clear *//*_.SETQ___V67*/ meltfptr[66] = 0;
		      /*^clear */
		/*clear *//*_#IS_A__L17*/ meltfnum[15] = 0;
		      /*^clear */
		/*clear *//*_.PAIR_TAIL__V69*/ meltfptr[67] = 0;
		      if ( /*_#BINDIX__L11*/ meltfnum[10] < 0)
			break;
		    }		/* end  foreach_in_multiple meltcit1__EACHTUP */

		  /*citerepilog */

		  MELT_LOCATION ("warmelt-macro.melt:5423:/ clear");
		/*clear *//*_.CURBINDEXPR__V48*/ meltfptr[47] = 0;
		  /*^clear */
		/*clear *//*_#BINDIX__L11*/ meltfnum[10] = 0;
		  /*^clear */
		/*clear *//*_.IFCPP___V49*/ meltfptr[48] = 0;
		  /*^clear */
		/*clear *//*_#IS_NOT_A__L14*/ meltfnum[12] = 0;
		  /*^clear */
		/*clear *//*_.IF___V53*/ meltfptr[49] = 0;
		}		/*endciterblock FOREACH_IN_MULTIPLE */
		;

#if MELT_HAVE_DEBUG
		MELT_LOCATION ("warmelt-macro.melt:5459:/ cppif.then");
		/*^block */
		/*anyblock */
		{


		  {
		    /*^locexp */
		    /*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
		    melt_dbgcounter++;
#endif
		    ;
		  }
		  ;
		  /*^checksignal */
		  MELT_CHECK_SIGNAL ();
		  ;
       /*_#MELT_NEED_DBG__L19*/ meltfnum[17] =
		    /*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
		    ( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
		    0		/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
		    ;;
		  MELT_LOCATION ("warmelt-macro.melt:5459:/ cond");
		  /*cond */ if ( /*_#MELT_NEED_DBG__L19*/ meltfnum[17])	/*then */
		    {
		      /*^cond.then */
		      /*^block */
		      /*anyblock */
		      {

	 /*_#THE_MELTCALLCOUNT__L20*/ meltfnum[11] =
#ifdef meltcallcount
			  meltcallcount	/* the_meltcallcount */
#else
			  0L
#endif /* meltcallcount the_meltcallcount */
			  ;;
			MELT_LOCATION
			  ("warmelt-macro.melt:5459:/ checksignal");
			MELT_CHECK_SIGNAL ();
			;
			/*^apply */
			/*apply */
			{
			  union meltparam_un argtab[5];
			  memset (&argtab, 0, sizeof (argtab));
			  /*^apply.arg */
			  argtab[0].meltbp_long =
			    /*_#THE_MELTCALLCOUNT__L20*/ meltfnum[11];
			  /*^apply.arg */
			  argtab[1].meltbp_cstring = "warmelt-macro.melt";
			  /*^apply.arg */
			  argtab[2].meltbp_long = 5459;
			  /*^apply.arg */
			  argtab[3].meltbp_cstring =
			    "mexpand_letrec recsexprtup after firstloop";
			  /*^apply.arg */
			  argtab[4].meltbp_aptr =
			    (melt_ptr_t *) & /*_.RECSEXPRTUP__V39*/
			    meltfptr[38];
			  /*_.MELT_DEBUG_FUN__V72*/ meltfptr[56] =
			    melt_apply ((meltclosure_ptr_t)
					(( /*!MELT_DEBUG_FUN */ meltfrout->
					  tabval[0])),
					(melt_ptr_t) (( /*nil */ NULL)),
					(MELTBPARSTR_LONG MELTBPARSTR_CSTRING
					 MELTBPARSTR_LONG MELTBPARSTR_CSTRING
					 MELTBPARSTR_PTR ""), argtab, "",
					(union meltparam_un *) 0);
			}
			;
			/*_.IF___V71*/ meltfptr[55] =
			  /*_.MELT_DEBUG_FUN__V72*/ meltfptr[56];;
			/*epilog */

			MELT_LOCATION ("warmelt-macro.melt:5459:/ clear");
		   /*clear *//*_#THE_MELTCALLCOUNT__L20*/ meltfnum[11] =
			  0;
			/*^clear */
		   /*clear *//*_.MELT_DEBUG_FUN__V72*/ meltfptr[56] = 0;
		      }
		      ;
		    }
		  else
		    {		/*^cond.else */

	/*_.IF___V71*/ meltfptr[55] = NULL;;
		    }
		  ;
		  MELT_LOCATION ("warmelt-macro.melt:5459:/ quasiblock");


		  /*_.PROGN___V73*/ meltfptr[57] =
		    /*_.IF___V71*/ meltfptr[55];;
		  /*^compute */
		  /*_.IFCPP___V70*/ meltfptr[50] =
		    /*_.PROGN___V73*/ meltfptr[57];;
		  /*epilog */

		  MELT_LOCATION ("warmelt-macro.melt:5459:/ clear");
		 /*clear *//*_#MELT_NEED_DBG__L19*/ meltfnum[17] = 0;
		  /*^clear */
		 /*clear *//*_.IF___V71*/ meltfptr[55] = 0;
		  /*^clear */
		 /*clear *//*_.PROGN___V73*/ meltfptr[57] = 0;
		}

#else /*MELT_HAVE_DEBUG */
		/*^cppif.else */
		/*_.IFCPP___V70*/ meltfptr[50] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
		;

#if MELT_HAVE_DEBUG
		MELT_LOCATION ("warmelt-macro.melt:5460:/ cppif.then");
		/*^block */
		/*anyblock */
		{


		  {
		    /*^locexp */
		    /*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
		    melt_dbgcounter++;
#endif
		    ;
		  }
		  ;
		  /*^checksignal */
		  MELT_CHECK_SIGNAL ();
		  ;
       /*_#MELT_NEED_DBG__L21*/ meltfnum[15] =
		    /*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
		    ( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
		    0		/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
		    ;;
		  MELT_LOCATION ("warmelt-macro.melt:5460:/ cond");
		  /*cond */ if ( /*_#MELT_NEED_DBG__L21*/ meltfnum[15])	/*then */
		    {
		      /*^cond.then */
		      /*^block */
		      /*anyblock */
		      {

	 /*_#THE_MELTCALLCOUNT__L22*/ meltfnum[11] =
#ifdef meltcallcount
			  meltcallcount	/* the_meltcallcount */
#else
			  0L
#endif /* meltcallcount the_meltcallcount */
			  ;;
			MELT_LOCATION
			  ("warmelt-macro.melt:5460:/ checksignal");
			MELT_CHECK_SIGNAL ();
			;
			/*^apply */
			/*apply */
			{
			  union meltparam_un argtab[5];
			  memset (&argtab, 0, sizeof (argtab));
			  /*^apply.arg */
			  argtab[0].meltbp_long =
			    /*_#THE_MELTCALLCOUNT__L22*/ meltfnum[11];
			  /*^apply.arg */
			  argtab[1].meltbp_cstring = "warmelt-macro.melt";
			  /*^apply.arg */
			  argtab[2].meltbp_long = 5460;
			  /*^apply.arg */
			  argtab[3].meltbp_cstring =
			    "mexpand_letrec vartup after firstloop";
			  /*^apply.arg */
			  argtab[4].meltbp_aptr =
			    (melt_ptr_t *) & /*_.VARTUP__V28*/ meltfptr[27];
			  /*_.MELT_DEBUG_FUN__V76*/ meltfptr[60] =
			    melt_apply ((meltclosure_ptr_t)
					(( /*!MELT_DEBUG_FUN */ meltfrout->
					  tabval[0])),
					(melt_ptr_t) (( /*nil */ NULL)),
					(MELTBPARSTR_LONG MELTBPARSTR_CSTRING
					 MELTBPARSTR_LONG MELTBPARSTR_CSTRING
					 MELTBPARSTR_PTR ""), argtab, "",
					(union meltparam_un *) 0);
			}
			;
			/*_.IF___V75*/ meltfptr[59] =
			  /*_.MELT_DEBUG_FUN__V76*/ meltfptr[60];;
			/*epilog */

			MELT_LOCATION ("warmelt-macro.melt:5460:/ clear");
		   /*clear *//*_#THE_MELTCALLCOUNT__L22*/ meltfnum[11] =
			  0;
			/*^clear */
		   /*clear *//*_.MELT_DEBUG_FUN__V76*/ meltfptr[60] = 0;
		      }
		      ;
		    }
		  else
		    {		/*^cond.else */

	/*_.IF___V75*/ meltfptr[59] = NULL;;
		    }
		  ;
		  MELT_LOCATION ("warmelt-macro.melt:5460:/ quasiblock");


		  /*_.PROGN___V77*/ meltfptr[62] =
		    /*_.IF___V75*/ meltfptr[59];;
		  /*^compute */
		  /*_.IFCPP___V74*/ meltfptr[58] =
		    /*_.PROGN___V77*/ meltfptr[62];;
		  /*epilog */

		  MELT_LOCATION ("warmelt-macro.melt:5460:/ clear");
		 /*clear *//*_#MELT_NEED_DBG__L21*/ meltfnum[15] = 0;
		  /*^clear */
		 /*clear *//*_.IF___V75*/ meltfptr[59] = 0;
		  /*^clear */
		 /*clear *//*_.PROGN___V77*/ meltfptr[62] = 0;
		}

#else /*MELT_HAVE_DEBUG */
		/*^cppif.else */
		/*_.IFCPP___V74*/ meltfptr[58] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
		;
		MELT_LOCATION ("warmelt-macro.melt:5463:/ quasiblock");


		/*^cond */
		/*cond */ if (
			       /*ifisa */
			       melt_is_instance_of ((melt_ptr_t)
						    ( /*_.NEWENV__V25*/
						     meltfptr[24]),
						    (melt_ptr_t) (( /*!CLASS_ENVIRONMENT */ meltfrout->tabval[2])))
		  )		/*then */
		  {
		    /*^cond.then */
		    /*^getslot */
		    {
		      melt_ptr_t slot = NULL, obj = NULL;
		      obj =
			(melt_ptr_t) ( /*_.NEWENV__V25*/ meltfptr[24])
			/*=obj*/ ;
		      melt_object_get_field (slot, obj, 0, "ENV_BIND");
       /*_.ENVMAP__V78*/ meltfptr[61] = slot;
		    };
		    ;
		  }
		else
		  {		/*^cond.else */

      /*_.ENVMAP__V78*/ meltfptr[61] = NULL;;
		  }
		;
		/*citerblock FOREACH_IN_MULTIPLE */
		{
		  /* start foreach_in_multiple meltcit2__EACHTUP */
		  long meltcit2__EACHTUP_ln =
		    melt_multiple_length ((melt_ptr_t) /*_.VARTUP__V28*/
					  meltfptr[27]);
		  for ( /*_#VARIX__L23*/ meltfnum[17] = 0;
		       ( /*_#VARIX__L23*/ meltfnum[17] >= 0)
		       && ( /*_#VARIX__L23*/ meltfnum[17] <
			   meltcit2__EACHTUP_ln);
	/*_#VARIX__L23*/ meltfnum[17]++)
		    {
		      /*_.CURVAR__V79*/ meltfptr[65] =
			melt_multiple_nth ((melt_ptr_t)
					   ( /*_.VARTUP__V28*/ meltfptr[27]),
					   /*_#VARIX__L23*/ meltfnum[17]);




#if MELT_HAVE_DEBUG
		      MELT_LOCATION ("warmelt-macro.melt:5468:/ cppif.then");
		      /*^block */
		      /*anyblock */
		      {


			{
			  /*^locexp */
			  /*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
			  melt_dbgcounter++;
#endif
			  ;
			}
			;
			/*^checksignal */
			MELT_CHECK_SIGNAL ();
			;
	/*_#MELT_NEED_DBG__L24*/ meltfnum[11] =
			  /*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
			  ( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
			  0	/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
			  ;;
			MELT_LOCATION ("warmelt-macro.melt:5468:/ cond");
			/*cond */ if ( /*_#MELT_NEED_DBG__L24*/ meltfnum[11])	/*then */
			  {
			    /*^cond.then */
			    /*^block */
			    /*anyblock */
			    {

	  /*_#THE_MELTCALLCOUNT__L25*/ meltfnum[15] =
#ifdef meltcallcount
				meltcallcount	/* the_meltcallcount */
#else
				0L
#endif /* meltcallcount the_meltcallcount */
				;;
			      MELT_LOCATION
				("warmelt-macro.melt:5468:/ checksignal");
			      MELT_CHECK_SIGNAL ();
			      ;
			      /*^apply */
			      /*apply */
			      {
				union meltparam_un argtab[5];
				memset (&argtab, 0, sizeof (argtab));
				/*^apply.arg */
				argtab[0].meltbp_long =
				  /*_#THE_MELTCALLCOUNT__L25*/ meltfnum[15];
				/*^apply.arg */
				argtab[1].meltbp_cstring =
				  "warmelt-macro.melt";
				/*^apply.arg */
				argtab[2].meltbp_long = 5468;
				/*^apply.arg */
				argtab[3].meltbp_cstring =
				  "mexpand_letrec second loop curvar";
				/*^apply.arg */
				argtab[4].meltbp_aptr =
				  (melt_ptr_t *) & /*_.CURVAR__V79*/
				  meltfptr[65];
				/*_.MELT_DEBUG_FUN__V82*/ meltfptr[56] =
				  melt_apply ((meltclosure_ptr_t)
					      (( /*!MELT_DEBUG_FUN */
						meltfrout->tabval[0])),
					      (melt_ptr_t) (( /*nil */ NULL)),
					      (MELTBPARSTR_LONG
					       MELTBPARSTR_CSTRING
					       MELTBPARSTR_LONG
					       MELTBPARSTR_CSTRING
					       MELTBPARSTR_PTR ""), argtab,
					      "", (union meltparam_un *) 0);
			      }
			      ;
			      /*_.IF___V81*/ meltfptr[67] =
				/*_.MELT_DEBUG_FUN__V82*/ meltfptr[56];;
			      /*epilog */

			      MELT_LOCATION
				("warmelt-macro.melt:5468:/ clear");
		    /*clear *//*_#THE_MELTCALLCOUNT__L25*/
				meltfnum[15] = 0;
			      /*^clear */
		    /*clear *//*_.MELT_DEBUG_FUN__V82*/ meltfptr[56]
				= 0;
			    }
			    ;
			  }
			else
			  {	/*^cond.else */

	 /*_.IF___V81*/ meltfptr[67] = NULL;;
			  }
			;
			MELT_LOCATION
			  ("warmelt-macro.melt:5468:/ quasiblock");


			/*_.PROGN___V83*/ meltfptr[55] =
			  /*_.IF___V81*/ meltfptr[67];;
			/*^compute */
			/*_.IFCPP___V80*/ meltfptr[66] =
			  /*_.PROGN___V83*/ meltfptr[55];;
			/*epilog */

			MELT_LOCATION ("warmelt-macro.melt:5468:/ clear");
		  /*clear *//*_#MELT_NEED_DBG__L24*/ meltfnum[11] = 0;
			/*^clear */
		  /*clear *//*_.IF___V81*/ meltfptr[67] = 0;
			/*^clear */
		  /*clear *//*_.PROGN___V83*/ meltfptr[55] = 0;
		      }

#else /*MELT_HAVE_DEBUG */
		      /*^cppif.else */
		      /*_.IFCPP___V80*/ meltfptr[66] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
		      ;
		      MELT_LOCATION ("warmelt-macro.melt:5469:/ checksignal");
		      MELT_CHECK_SIGNAL ();
		      ;
      /*_.MAPOBJECT_GET__V84*/ meltfptr[57] =
			/*mapobject_get */
			melt_get_mapobjects ((meltmapobjects_ptr_t)
					     ( /*_.ENVMAP__V78*/
					      meltfptr[61]),
					     (meltobject_ptr_t) ( /*_.CURVAR__V79*/ meltfptr[65]));;
		      MELT_LOCATION ("warmelt-macro.melt:5469:/ cond");
		      /*cond */ if ( /*_.MAPOBJECT_GET__V84*/ meltfptr[57])	/*then */
			{
			  /*^cond.then */
			  /*^block */
			  /*anyblock */
			  {

			    MELT_LOCATION ("warmelt-macro.melt:5471:/ cond");
			    /*cond */ if (
					   /*ifisa */
					   melt_is_instance_of ((melt_ptr_t)
								( /*_.CURVAR__V79*/ meltfptr[65]),
								(melt_ptr_t) (( /*!CLASS_NAMED */ meltfrout->tabval[11])))
			      )	/*then */
			      {
				/*^cond.then */
				/*^getslot */
				{
				  melt_ptr_t slot = NULL, obj = NULL;
				  obj =
				    (melt_ptr_t) ( /*_.CURVAR__V79*/
						  meltfptr[65]) /*=obj*/ ;
				  melt_object_get_field (slot, obj, 1,
							 "NAMED_NAME");
	  /*_.NAMED_NAME__V85*/ meltfptr[60] = slot;
				};
				;
			      }
			    else
			      {	/*^cond.else */

	 /*_.NAMED_NAME__V85*/ meltfptr[60] = NULL;;
			      }
			    ;

			    {
			      MELT_LOCATION
				("warmelt-macro.melt:5470:/ locexp");
			      melt_error_str ((melt_ptr_t)
					      ( /*_.LOC__V20*/ meltfptr[19]),
					      ("repeated variable in LETREC binding"),
					      (melt_ptr_t) ( /*_.NAMED_NAME__V85*/ meltfptr[60]));
			    }
			    ;
			    /*epilog */

			    MELT_LOCATION ("warmelt-macro.melt:5469:/ clear");
		  /*clear *//*_.NAMED_NAME__V85*/ meltfptr[60] = 0;
			  }
			  ;
			}	/*noelse */
		      ;
		      MELT_LOCATION ("warmelt-macro.melt:5473:/ quasiblock");


		      /*^checksignal */
		      MELT_CHECK_SIGNAL ();
		      ;
		      /*^quasiblock */


		      /*^rawallocobj */
		      /*rawallocobj */
		      {
			melt_ptr_t newobj = 0;
			melt_raw_object_create (newobj,
						(melt_ptr_t) (( /*!CLASS_LETREC_BINDING */ meltfrout->tabval[12])), (4), "CLASS_LETREC_BINDING");
       /*_.INST__V87*/ meltfptr[62] =
			  newobj;
		      };
		      ;
		      /*^putslot */
		      /*putslot */
		      melt_assertmsg ("putslot checkobj @BINDER",
				      melt_magic_discr ((melt_ptr_t)
							( /*_.INST__V87*/
							 meltfptr[62])) ==
				      MELTOBMAG_OBJECT);
		      melt_putfield_object (( /*_.INST__V87*/ meltfptr[62]),
					    (0),
					    ( /*_.CURVAR__V79*/ meltfptr[65]),
					    "BINDER");
		      ;
		      /*^putslot */
		      /*putslot */
		      melt_assertmsg ("putslot checkobj @LETBIND_TYPE",
				      melt_magic_discr ((melt_ptr_t)
							( /*_.INST__V87*/
							 meltfptr[62])) ==
				      MELTOBMAG_OBJECT);
		      melt_putfield_object (( /*_.INST__V87*/ meltfptr[62]),
					    (1),
					    (( /*!CTYPE_VALUE */ meltfrout->
					      tabval[13])), "LETBIND_TYPE");
		      ;
		      /*^putslot */
		      /*putslot */
		      melt_assertmsg ("putslot checkobj @LETBIND_EXPR",
				      melt_magic_discr ((melt_ptr_t)
							( /*_.INST__V87*/
							 meltfptr[62])) ==
				      MELTOBMAG_OBJECT);
		      melt_putfield_object (( /*_.INST__V87*/ meltfptr[62]),
					    (2), (( /*nil */ NULL)),
					    "LETBIND_EXPR");
		      ;
		      /*^touchobj */

		      melt_dbgtrace_written_object ( /*_.INST__V87*/
						    meltfptr[62],
						    "newly made instance");
		      ;
		      /*_.CURBIND__V86*/ meltfptr[59] =
			/*_.INST__V87*/ meltfptr[62];;
		      MELT_LOCATION ("warmelt-macro.melt:5479:/ checksignal");
		      MELT_CHECK_SIGNAL ();
		      ;
		      /*^apply */
		      /*apply */
		      {
			union meltparam_un argtab[1];
			memset (&argtab, 0, sizeof (argtab));
			/*^apply.arg */
			argtab[0].meltbp_aptr =
			  (melt_ptr_t *) & /*_.CURBIND__V86*/ meltfptr[59];
			/*_.PUT_ENV__V88*/ meltfptr[56] =
			  melt_apply ((meltclosure_ptr_t)
				      (( /*!PUT_ENV */ meltfrout->
					tabval[14])),
				      (melt_ptr_t) ( /*_.NEWENV__V25*/
						    meltfptr[24]),
				      (MELTBPARSTR_PTR ""), argtab, "",
				      (union meltparam_un *) 0);
		      }
		      ;

		      {
			MELT_LOCATION ("warmelt-macro.melt:5480:/ locexp");
			meltgc_multiple_put_nth ((melt_ptr_t)
						 ( /*_.BINDTUP__V26*/
						  meltfptr[25]),
						 ( /*_#VARIX__L23*/
						  meltfnum[17]),
						 (melt_ptr_t) ( /*_.CURBIND__V86*/ meltfptr[59]));
		      }
		      ;

		      MELT_LOCATION ("warmelt-macro.melt:5473:/ clear");
		/*clear *//*_.CURBIND__V86*/ meltfptr[59] = 0;
		      /*^clear */
		/*clear *//*_.PUT_ENV__V88*/ meltfptr[56] = 0;
		      if ( /*_#VARIX__L23*/ meltfnum[17] < 0)
			break;
		    }		/* end  foreach_in_multiple meltcit2__EACHTUP */

		  /*citerepilog */

		  MELT_LOCATION ("warmelt-macro.melt:5465:/ clear");
		/*clear *//*_.CURVAR__V79*/ meltfptr[65] = 0;
		  /*^clear */
		/*clear *//*_#VARIX__L23*/ meltfnum[17] = 0;
		  /*^clear */
		/*clear *//*_.IFCPP___V80*/ meltfptr[66] = 0;
		  /*^clear */
		/*clear *//*_.MAPOBJECT_GET__V84*/ meltfptr[57] = 0;
		}		/*endciterblock FOREACH_IN_MULTIPLE */
		;

		MELT_LOCATION ("warmelt-macro.melt:5463:/ clear");
	       /*clear *//*_.ENVMAP__V78*/ meltfptr[61] = 0;

#if MELT_HAVE_DEBUG
		MELT_LOCATION ("warmelt-macro.melt:5482:/ cppif.then");
		/*^block */
		/*anyblock */
		{


		  {
		    /*^locexp */
		    /*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
		    melt_dbgcounter++;
#endif
		    ;
		  }
		  ;
		  /*^checksignal */
		  MELT_CHECK_SIGNAL ();
		  ;
       /*_#MELT_NEED_DBG__L26*/ meltfnum[15] =
		    /*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
		    ( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
		    0		/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
		    ;;
		  MELT_LOCATION ("warmelt-macro.melt:5482:/ cond");
		  /*cond */ if ( /*_#MELT_NEED_DBG__L26*/ meltfnum[15])	/*then */
		    {
		      /*^cond.then */
		      /*^block */
		      /*anyblock */
		      {

	 /*_#THE_MELTCALLCOUNT__L27*/ meltfnum[11] =
#ifdef meltcallcount
			  meltcallcount	/* the_meltcallcount */
#else
			  0L
#endif /* meltcallcount the_meltcallcount */
			  ;;
			MELT_LOCATION
			  ("warmelt-macro.melt:5482:/ checksignal");
			MELT_CHECK_SIGNAL ();
			;
			/*^apply */
			/*apply */
			{
			  union meltparam_un argtab[5];
			  memset (&argtab, 0, sizeof (argtab));
			  /*^apply.arg */
			  argtab[0].meltbp_long =
			    /*_#THE_MELTCALLCOUNT__L27*/ meltfnum[11];
			  /*^apply.arg */
			  argtab[1].meltbp_cstring = "warmelt-macro.melt";
			  /*^apply.arg */
			  argtab[2].meltbp_long = 5482;
			  /*^apply.arg */
			  argtab[3].meltbp_cstring =
			    "mexpand_letrec bindtup after secondloop";
			  /*^apply.arg */
			  argtab[4].meltbp_aptr =
			    (melt_ptr_t *) & /*_.BINDTUP__V26*/ meltfptr[25];
			  /*_.MELT_DEBUG_FUN__V91*/ meltfptr[60] =
			    melt_apply ((meltclosure_ptr_t)
					(( /*!MELT_DEBUG_FUN */ meltfrout->
					  tabval[0])),
					(melt_ptr_t) (( /*nil */ NULL)),
					(MELTBPARSTR_LONG MELTBPARSTR_CSTRING
					 MELTBPARSTR_LONG MELTBPARSTR_CSTRING
					 MELTBPARSTR_PTR ""), argtab, "",
					(union meltparam_un *) 0);
			}
			;
			/*_.IF___V90*/ meltfptr[55] =
			  /*_.MELT_DEBUG_FUN__V91*/ meltfptr[60];;
			/*epilog */

			MELT_LOCATION ("warmelt-macro.melt:5482:/ clear");
		   /*clear *//*_#THE_MELTCALLCOUNT__L27*/ meltfnum[11] =
			  0;
			/*^clear */
		   /*clear *//*_.MELT_DEBUG_FUN__V91*/ meltfptr[60] = 0;
		      }
		      ;
		    }
		  else
		    {		/*^cond.else */

	/*_.IF___V90*/ meltfptr[55] = NULL;;
		    }
		  ;
		  MELT_LOCATION ("warmelt-macro.melt:5482:/ quasiblock");


		  /*_.PROGN___V92*/ meltfptr[59] =
		    /*_.IF___V90*/ meltfptr[55];;
		  /*^compute */
		  /*_.IFCPP___V89*/ meltfptr[67] =
		    /*_.PROGN___V92*/ meltfptr[59];;
		  /*epilog */

		  MELT_LOCATION ("warmelt-macro.melt:5482:/ clear");
		 /*clear *//*_#MELT_NEED_DBG__L26*/ meltfnum[15] = 0;
		  /*^clear */
		 /*clear *//*_.IF___V90*/ meltfptr[55] = 0;
		  /*^clear */
		 /*clear *//*_.PROGN___V92*/ meltfptr[59] = 0;
		}

#else /*MELT_HAVE_DEBUG */
		/*^cppif.else */
		/*_.IFCPP___V89*/ meltfptr[67] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
		;
		/*citerblock FOREACH_IN_MULTIPLE */
		{
		  /* start foreach_in_multiple meltcit3__EACHTUP */
		  long meltcit3__EACHTUP_ln =
		    melt_multiple_length ((melt_ptr_t) /*_.RECSEXPRTUP__V39*/
					  meltfptr[38]);
		  for ( /*_#EXPIX__L28*/ meltfnum[11] = 0;
		       ( /*_#EXPIX__L28*/ meltfnum[11] >= 0)
		       && ( /*_#EXPIX__L28*/ meltfnum[11] <
			   meltcit3__EACHTUP_ln);
	/*_#EXPIX__L28*/ meltfnum[11]++)
		    {
		      /*_.CURSEXPR__V93*/ meltfptr[56] =
			melt_multiple_nth ((melt_ptr_t)
					   ( /*_.RECSEXPRTUP__V39*/
					    meltfptr[38]), /*_#EXPIX__L28*/
					   meltfnum[11]);



		      MELT_LOCATION ("warmelt-macro.melt:5488:/ quasiblock");


		      MELT_LOCATION ("warmelt-macro.melt:5489:/ cond");
		      /*cond */ if (
				     /*ifisa */
				     melt_is_instance_of ((melt_ptr_t)
							  ( /*_.CURSEXPR__V93*/ meltfptr[56]),
							  (melt_ptr_t) (( /*!CLASS_LOCATED */ meltfrout->tabval[8])))
			)	/*then */
			{
			  /*^cond.then */
			  /*^getslot */
			  {
			    melt_ptr_t slot = NULL, obj = NULL;
			    obj =
			      (melt_ptr_t) ( /*_.CURSEXPR__V93*/ meltfptr[56])
			      /*=obj*/ ;
			    melt_object_get_field (slot, obj, 1,
						   "LOCA_LOCATION");
	/*_.LOCA_LOCATION__V94*/ meltfptr[61] = slot;
			  };
			  ;
			}
		      else
			{	/*^cond.else */

       /*_.LOCA_LOCATION__V94*/ meltfptr[61] = NULL;;
			}
		      ;
		      MELT_LOCATION ("warmelt-macro.melt:5489:/ cond");
		      /*cond */ if ( /*_.LOCA_LOCATION__V94*/ meltfptr[61])	/*then */
			{
			  /*^cond.then */
			  /*_.CURLOC__V95*/ meltfptr[60] =
			    /*_.LOCA_LOCATION__V94*/ meltfptr[61];;
			}
		      else
			{
			  MELT_LOCATION
			    ("warmelt-macro.melt:5489:/ cond.else");

			  /*_.CURLOC__V95*/ meltfptr[60] =
			    /*_.LOC__V20*/ meltfptr[19];;
			}
		      ;
		      MELT_LOCATION ("warmelt-macro.melt:5490:/ checksignal");
		      MELT_CHECK_SIGNAL ();
		      ;
		      /*^apply */
		      /*apply */
		      {
			union meltparam_un argtab[3];
			memset (&argtab, 0, sizeof (argtab));
			/*^apply.arg */
			argtab[0].meltbp_aptr =
			  (melt_ptr_t *) & /*_.NEWENV__V25*/ meltfptr[24];
			/*^apply.arg */
			argtab[1].meltbp_aptr =
			  (melt_ptr_t *) & /*_.MEXPANDER__V4*/ meltfptr[3];
			/*^apply.arg */
			argtab[2].meltbp_aptr =
			  (melt_ptr_t *) & /*_.MODCTX__V5*/ meltfptr[4];
			/*_.CUREXP__V96*/ meltfptr[55] =
			  melt_apply ((meltclosure_ptr_t)
				      ( /*_.MEXPANDER__V4*/ meltfptr[3]),
				      (melt_ptr_t) ( /*_.CURSEXPR__V93*/
						    meltfptr[56]),
				      (MELTBPARSTR_PTR MELTBPARSTR_PTR
				       MELTBPARSTR_PTR ""), argtab, "",
				      (union meltparam_un *) 0);
		      }
		      ;
      /*_.CURBIND__V97*/ meltfptr[59] =
			(melt_multiple_nth
			 ((melt_ptr_t) ( /*_.BINDTUP__V26*/ meltfptr[25]),
			  ( /*_#EXPIX__L28*/ meltfnum[11])));;
		      MELT_LOCATION ("warmelt-macro.melt:5493:/ checksignal");
		      MELT_CHECK_SIGNAL ();
		      ;
		      /*^checksignal */
		      MELT_CHECK_SIGNAL ();
		      ;
		      /*^msend */
		      /*msend */
		      {
			/*_.IS_RECURSIVELY_CONSTRUCTIBLE__V98*/ meltfptr[97] =
			  meltgc_send ((melt_ptr_t)
				       ( /*_.CUREXP__V96*/ meltfptr[55]),
				       (melt_ptr_t) (( /*!IS_RECURSIVELY_CONSTRUCTIBLE */ meltfrout->tabval[15])), (""), (union meltparam_un *) 0, "", (union meltparam_un *) 0);
		      }
		      ;
      /*_#NULL__L29*/ meltfnum[15] =
			(( /*_.IS_RECURSIVELY_CONSTRUCTIBLE__V98*/
			  meltfptr[97]) == NULL);;
		      MELT_LOCATION ("warmelt-macro.melt:5493:/ cond");
		      /*cond */ if ( /*_#NULL__L29*/ meltfnum[15])	/*then */
			{
			  /*^cond.then */
			  /*^block */
			  /*anyblock */
			  {


			    {
			      MELT_LOCATION
				("warmelt-macro.melt:5495:/ locexp");
			      /* error_plain */
				melt_error_str ((melt_ptr_t)
						( /*_.CURLOC__V95*/
						 meltfptr[60]),
						("invalid expression in LETREC binding [not recursively constructible]"),
						(melt_ptr_t) 0);
			    }
			    ;
			    MELT_LOCATION
			      ("warmelt-macro.melt:5496:/ quasiblock");


	/*_.RETVAL___V1*/ meltfptr[0] = NULL;;

			    {
			      MELT_LOCATION
				("warmelt-macro.melt:5496:/ locexp");
			      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
			      if (meltxresdescr_ && meltxresdescr_[0]
				  && meltxrestab_)
				melt_warn_for_no_expected_secondary_results
				  ();
			      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
			      ;
			    }
			    ;
			    /*^finalreturn */
			    ;
			    /*finalret */ goto labend_rout;
			    MELT_LOCATION
			      ("warmelt-macro.melt:5494:/ quasiblock");


			    /*_.PROGN___V101*/ meltfptr[100] =
			      /*_.RETURN___V100*/ meltfptr[99];;
			    /*^compute */
			    /*_.IF___V99*/ meltfptr[98] =
			      /*_.PROGN___V101*/ meltfptr[100];;
			    /*epilog */

			    MELT_LOCATION ("warmelt-macro.melt:5493:/ clear");
		  /*clear *//*_.RETURN___V100*/ meltfptr[99] = 0;
			    /*^clear */
		  /*clear *//*_.PROGN___V101*/ meltfptr[100] = 0;
			  }
			  ;
			}
		      else
			{	/*^cond.else */

       /*_.IF___V99*/ meltfptr[98] = NULL;;
			}
		      ;
		      MELT_LOCATION ("warmelt-macro.melt:5497:/ checksignal");
		      MELT_CHECK_SIGNAL ();
		      ;
		      /*^cond */
		      /*cond */ if (
				     /*ifisa */
				     melt_is_instance_of ((melt_ptr_t)
							  ( /*_.CURBIND__V97*/
							   meltfptr[59]),
							  (melt_ptr_t) (( /*!CLASS_LET_BINDING */ meltfrout->tabval[16])))
			)	/*then */
			{
			  /*^cond.then */
			  /*^block */
			  /*anyblock */
			  {

			    /*^putslot */
			    /*putslot */
			    melt_assertmsg ("putslot checkobj @LETBIND_EXPR",
					    melt_magic_discr ((melt_ptr_t)
							      ( /*_.CURBIND__V97*/ meltfptr[59])) == MELTOBMAG_OBJECT);
			    melt_putfield_object (( /*_.CURBIND__V97*/
						   meltfptr[59]), (2),
						  ( /*_.CUREXP__V96*/
						   meltfptr[55]),
						  "LETBIND_EXPR");
			    ;
			    /*^touch */
			    meltgc_touch ( /*_.CURBIND__V97*/ meltfptr[59]);
			    ;
			    /*^touchobj */

			    melt_dbgtrace_written_object ( /*_.CURBIND__V97*/
							  meltfptr[59],
							  "put-fields");
			    ;
			    /*epilog */
			  }
			  ;
			}	/*noelse */
		      ;

		      {
			MELT_LOCATION ("warmelt-macro.melt:5498:/ locexp");
			meltgc_multiple_put_nth ((melt_ptr_t)
						 ( /*_.EXPRTUP__V29*/
						  meltfptr[28]),
						 ( /*_#EXPIX__L28*/
						  meltfnum[11]),
						 (melt_ptr_t) ( /*_.CUREXP__V96*/ meltfptr[55]));
		      }
		      ;
		      MELT_LOCATION ("warmelt-macro.melt:5499:/ quasiblock");


		      MELT_LOCATION ("warmelt-macro.melt:5500:/ checksignal");
		      MELT_CHECK_SIGNAL ();
		      ;
		      MELT_LOCATION ("warmelt-macro.melt:5501:/ cond");
		      /*cond */ if (
				     /*ifisa */
				     melt_is_instance_of ((melt_ptr_t)
							  ( /*_.CUREXP__V96*/
							   meltfptr[55]),
							  (melt_ptr_t) (( /*!CLASS_LOCATED */ meltfrout->tabval[8])))
			)	/*then */
			{
			  /*^cond.then */
			  /*^getslot */
			  {
			    melt_ptr_t slot = NULL, obj = NULL;
			    obj =
			      (melt_ptr_t) ( /*_.CUREXP__V96*/ meltfptr[55])
			      /*=obj*/ ;
			    melt_object_get_field (slot, obj, 1,
						   "LOCA_LOCATION");
	/*_.LOCA_LOCATION__V102*/ meltfptr[99] = slot;
			  };
			  ;
			}
		      else
			{	/*^cond.else */

       /*_.LOCA_LOCATION__V102*/ meltfptr[99] = NULL;;
			}
		      ;
		      MELT_LOCATION ("warmelt-macro.melt:5501:/ cond");
		      /*cond */ if ( /*_.LOCA_LOCATION__V102*/ meltfptr[99])	/*then */
			{
			  /*^cond.then */
			  /*_.OR___V103*/ meltfptr[100] =
			    /*_.LOCA_LOCATION__V102*/ meltfptr[99];;
			}
		      else
			{
			  MELT_LOCATION
			    ("warmelt-macro.melt:5501:/ cond.else");

			  /*_.OR___V103*/ meltfptr[100] =
			    /*_.LOC__V20*/ meltfptr[19];;
			}
		      ;
		      /*^compute */
      /*_.MULTIPLE_NTH__V104*/ meltfptr[103] =
			(melt_multiple_nth
			 ((melt_ptr_t) ( /*_.VARTUP__V28*/ meltfptr[27]),
			  ( /*_#EXPIX__L28*/ meltfnum[11])));;
		      MELT_LOCATION ("warmelt-macro.melt:5500:/ quasiblock");


		      /*^rawallocobj */
		      /*rawallocobj */
		      {
			melt_ptr_t newobj = 0;
			melt_raw_object_create (newobj,
						(melt_ptr_t) (( /*!CLASS_SOURCE_LETREC_BINDING */ meltfrout->tabval[17])), (5), "CLASS_SOURCE_LETREC_BINDING");
       /*_.INST__V106*/ meltfptr[105] =
			  newobj;
		      };
		      ;
		      /*^putslot */
		      /*putslot */
		      melt_assertmsg ("putslot checkobj @LOCA_LOCATION",
				      melt_magic_discr ((melt_ptr_t)
							( /*_.INST__V106*/
							 meltfptr[105])) ==
				      MELTOBMAG_OBJECT);
		      melt_putfield_object (( /*_.INST__V106*/ meltfptr[105]),
					    (1),
					    ( /*_.OR___V103*/ meltfptr[100]),
					    "LOCA_LOCATION");
		      ;
		      /*^putslot */
		      /*putslot */
		      melt_assertmsg ("putslot checkobj @SLETB_TYPE",
				      melt_magic_discr ((melt_ptr_t)
							( /*_.INST__V106*/
							 meltfptr[105])) ==
				      MELTOBMAG_OBJECT);
		      melt_putfield_object (( /*_.INST__V106*/ meltfptr[105]),
					    (2),
					    (( /*!CTYPE_VALUE */ meltfrout->
					      tabval[13])), "SLETB_TYPE");
		      ;
		      /*^putslot */
		      /*putslot */
		      melt_assertmsg ("putslot checkobj @SLETB_BINDER",
				      melt_magic_discr ((melt_ptr_t)
							( /*_.INST__V106*/
							 meltfptr[105])) ==
				      MELTOBMAG_OBJECT);
		      melt_putfield_object (( /*_.INST__V106*/ meltfptr[105]),
					    (3),
					    ( /*_.MULTIPLE_NTH__V104*/
					     meltfptr[103]), "SLETB_BINDER");
		      ;
		      /*^putslot */
		      /*putslot */
		      melt_assertmsg ("putslot checkobj @SLETB_EXPR",
				      melt_magic_discr ((melt_ptr_t)
							( /*_.INST__V106*/
							 meltfptr[105])) ==
				      MELTOBMAG_OBJECT);
		      melt_putfield_object (( /*_.INST__V106*/ meltfptr[105]),
					    (4),
					    ( /*_.CUREXP__V96*/ meltfptr[55]),
					    "SLETB_EXPR");
		      ;
		      /*^touchobj */

		      melt_dbgtrace_written_object ( /*_.INST__V106*/
						    meltfptr[105],
						    "newly made instance");
		      ;
		      /*_.SBIND__V105*/ meltfptr[104] =
			/*_.INST__V106*/ meltfptr[105];;

		      {
			MELT_LOCATION ("warmelt-macro.melt:5507:/ locexp");
			meltgc_multiple_put_nth ((melt_ptr_t)
						 ( /*_.SRCBINDTUP__V27*/
						  meltfptr[26]),
						 ( /*_#EXPIX__L28*/
						  meltfnum[11]),
						 (melt_ptr_t) ( /*_.SBIND__V105*/ meltfptr[104]));
		      }
		      ;

		      MELT_LOCATION ("warmelt-macro.melt:5499:/ clear");
		/*clear *//*_.LOCA_LOCATION__V102*/ meltfptr[99] = 0;
		      /*^clear */
		/*clear *//*_.OR___V103*/ meltfptr[100] = 0;
		      /*^clear */
		/*clear *//*_.MULTIPLE_NTH__V104*/ meltfptr[103] = 0;
		      /*^clear */
		/*clear *//*_.SBIND__V105*/ meltfptr[104] = 0;

		      MELT_LOCATION ("warmelt-macro.melt:5488:/ clear");
		/*clear *//*_.LOCA_LOCATION__V94*/ meltfptr[61] = 0;
		      /*^clear */
		/*clear *//*_.CURLOC__V95*/ meltfptr[60] = 0;
		      /*^clear */
		/*clear *//*_.CUREXP__V96*/ meltfptr[55] = 0;
		      /*^clear */
		/*clear *//*_.CURBIND__V97*/ meltfptr[59] = 0;
		      /*^clear */
		/*clear *//*_.IS_RECURSIVELY_CONSTRUCTIBLE__V98*/
			meltfptr[97] = 0;
		      /*^clear */
		/*clear *//*_#NULL__L29*/ meltfnum[15] = 0;
		      /*^clear */
		/*clear *//*_.IF___V99*/ meltfptr[98] = 0;
		      if ( /*_#EXPIX__L28*/ meltfnum[11] < 0)
			break;
		    }		/* end  foreach_in_multiple meltcit3__EACHTUP */

		  /*citerepilog */

		  MELT_LOCATION ("warmelt-macro.melt:5485:/ clear");
		/*clear *//*_.CURSEXPR__V93*/ meltfptr[56] = 0;
		  /*^clear */
		/*clear *//*_#EXPIX__L28*/ meltfnum[11] = 0;
		}		/*endciterblock FOREACH_IN_MULTIPLE */
		;

#if MELT_HAVE_DEBUG
		MELT_LOCATION ("warmelt-macro.melt:5509:/ cppif.then");
		/*^block */
		/*anyblock */
		{


		  {
		    /*^locexp */
		    /*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
		    melt_dbgcounter++;
#endif
		    ;
		  }
		  ;
		  /*^checksignal */
		  MELT_CHECK_SIGNAL ();
		  ;
       /*_#MELT_NEED_DBG__L30*/ meltfnum[15] =
		    /*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
		    ( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
		    0		/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
		    ;;
		  MELT_LOCATION ("warmelt-macro.melt:5509:/ cond");
		  /*cond */ if ( /*_#MELT_NEED_DBG__L30*/ meltfnum[15])	/*then */
		    {
		      /*^cond.then */
		      /*^block */
		      /*anyblock */
		      {

	 /*_#THE_MELTCALLCOUNT__L31*/ meltfnum[30] =
#ifdef meltcallcount
			  meltcallcount	/* the_meltcallcount */
#else
			  0L
#endif /* meltcallcount the_meltcallcount */
			  ;;
			MELT_LOCATION
			  ("warmelt-macro.melt:5509:/ checksignal");
			MELT_CHECK_SIGNAL ();
			;
			/*^apply */
			/*apply */
			{
			  union meltparam_un argtab[5];
			  memset (&argtab, 0, sizeof (argtab));
			  /*^apply.arg */
			  argtab[0].meltbp_long =
			    /*_#THE_MELTCALLCOUNT__L31*/ meltfnum[30];
			  /*^apply.arg */
			  argtab[1].meltbp_cstring = "warmelt-macro.melt";
			  /*^apply.arg */
			  argtab[2].meltbp_long = 5509;
			  /*^apply.arg */
			  argtab[3].meltbp_cstring =
			    "mexpand_letrec exprtup after thirdloop";
			  /*^apply.arg */
			  argtab[4].meltbp_aptr =
			    (melt_ptr_t *) & /*_.EXPRTUP__V29*/ meltfptr[28];
			  /*_.MELT_DEBUG_FUN__V109*/ meltfptr[103] =
			    melt_apply ((meltclosure_ptr_t)
					(( /*!MELT_DEBUG_FUN */ meltfrout->
					  tabval[0])),
					(melt_ptr_t) (( /*nil */ NULL)),
					(MELTBPARSTR_LONG MELTBPARSTR_CSTRING
					 MELTBPARSTR_LONG MELTBPARSTR_CSTRING
					 MELTBPARSTR_PTR ""), argtab, "",
					(union meltparam_un *) 0);
			}
			;
			/*_.IF___V108*/ meltfptr[100] =
			  /*_.MELT_DEBUG_FUN__V109*/ meltfptr[103];;
			/*epilog */

			MELT_LOCATION ("warmelt-macro.melt:5509:/ clear");
		   /*clear *//*_#THE_MELTCALLCOUNT__L31*/ meltfnum[30] =
			  0;
			/*^clear */
		   /*clear *//*_.MELT_DEBUG_FUN__V109*/ meltfptr[103] =
			  0;
		      }
		      ;
		    }
		  else
		    {		/*^cond.else */

	/*_.IF___V108*/ meltfptr[100] = NULL;;
		    }
		  ;
		  MELT_LOCATION ("warmelt-macro.melt:5509:/ quasiblock");


		  /*_.PROGN___V110*/ meltfptr[104] =
		    /*_.IF___V108*/ meltfptr[100];;
		  /*^compute */
		  /*_.IFCPP___V107*/ meltfptr[99] =
		    /*_.PROGN___V110*/ meltfptr[104];;
		  /*epilog */

		  MELT_LOCATION ("warmelt-macro.melt:5509:/ clear");
		 /*clear *//*_#MELT_NEED_DBG__L30*/ meltfnum[15] = 0;
		  /*^clear */
		 /*clear *//*_.IF___V108*/ meltfptr[100] = 0;
		  /*^clear */
		 /*clear *//*_.PROGN___V110*/ meltfptr[104] = 0;
		}

#else /*MELT_HAVE_DEBUG */
		/*^cppif.else */
		/*_.IFCPP___V107*/ meltfptr[99] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
		;
		/*^compute */
		/*_.LET___V33*/ meltfptr[32] =
		  /*_.IFCPP___V107*/ meltfptr[99];;

		MELT_LOCATION ("warmelt-macro.melt:5406:/ clear");
	       /*clear *//*_.SEXP_CONTENTS__V34*/ meltfptr[33] = 0;
		/*^clear */
	       /*clear *//*_.LIST_FIRST__V35*/ meltfptr[34] = 0;
		/*^clear */
	       /*clear *//*_.LAMBDA___V36*/ meltfptr[35] = 0;
		/*^clear */
	       /*clear *//*_.RECBINDTUP__V38*/ meltfptr[37] = 0;
		/*^clear */
	       /*clear *//*_#NBRECBIND__L9*/ meltfnum[8] = 0;
		/*^clear */
	       /*clear *//*_.RECSEXPRTUP__V39*/ meltfptr[38] = 0;
		/*^clear */
	       /*clear *//*_#SETQ___L10*/ meltfnum[9] = 0;
		/*^clear */
	       /*clear *//*_.MAKE_MULTIPLE__V40*/ meltfptr[39] = 0;
		/*^clear */
	       /*clear *//*_.SETQ___V41*/ meltfptr[40] = 0;
		/*^clear */
	       /*clear *//*_.MAKE_MULTIPLE__V42*/ meltfptr[41] = 0;
		/*^clear */
	       /*clear *//*_.SETQ___V43*/ meltfptr[42] = 0;
		/*^clear */
	       /*clear *//*_.MAKE_MULTIPLE__V44*/ meltfptr[43] = 0;
		/*^clear */
	       /*clear *//*_.SETQ___V45*/ meltfptr[44] = 0;
		/*^clear */
	       /*clear *//*_.MAKE_MULTIPLE__V46*/ meltfptr[45] = 0;
		/*^clear */
	       /*clear *//*_.SETQ___V47*/ meltfptr[46] = 0;
		/*^clear */
	       /*clear *//*_.IFCPP___V70*/ meltfptr[50] = 0;
		/*^clear */
	       /*clear *//*_.IFCPP___V74*/ meltfptr[58] = 0;
		/*^clear */
	       /*clear *//*_.IFCPP___V89*/ meltfptr[67] = 0;
		/*^clear */
	       /*clear *//*_.IFCPP___V107*/ meltfptr[99] = 0;
		/*_.IFELSE___V32*/ meltfptr[31] =
		  /*_.LET___V33*/ meltfptr[32];;
		/*epilog */

		MELT_LOCATION ("warmelt-macro.melt:5405:/ clear");
	       /*clear *//*_.LET___V33*/ meltfptr[32] = 0;
	      }
	      ;
	    }
	  else
	    {			/*^cond.else */

	      /*^block */
	      /*anyblock */
	      {




		{
		  MELT_LOCATION ("warmelt-macro.melt:5511:/ locexp");
		  /* error_plain */
		    melt_error_str ((melt_ptr_t)
				    ( /*_.LOC__V20*/ meltfptr[19]),
				    ("missing letbinding-s in LETREC"),
				    (melt_ptr_t) 0);
		}
		;
	       /*clear *//*_.IFELSE___V32*/ meltfptr[31] = 0;
		/*epilog */
	      }
	      ;
	    }
	  ;
	  /*^compute */
	  /*_.IF___V31*/ meltfptr[30] = /*_.IFELSE___V32*/ meltfptr[31];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-macro.melt:5404:/ clear");
	     /*clear *//*_#IS_A__L8*/ meltfnum[0] = 0;
	  /*^clear */
	     /*clear *//*_.IFELSE___V32*/ meltfptr[31] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

  /*_.IF___V31*/ meltfptr[30] = NULL;;
      }
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-macro.melt:5513:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L32*/ meltfnum[30] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-macro.melt:5513:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L32*/ meltfnum[30])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L33*/ meltfnum[15] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-macro.melt:5513:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long =
		/*_#THE_MELTCALLCOUNT__L33*/ meltfnum[15];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-macro.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 5513;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "mexpand_letrec srcbindtup";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.SRCBINDTUP__V27*/ meltfptr[26];
	      /*_.MELT_DEBUG_FUN__V113*/ meltfptr[55] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V112*/ meltfptr[60] =
	      /*_.MELT_DEBUG_FUN__V113*/ meltfptr[55];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-macro.melt:5513:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L33*/ meltfnum[15] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V113*/ meltfptr[55] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V112*/ meltfptr[60] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-macro.melt:5513:/ quasiblock");


      /*_.PROGN___V114*/ meltfptr[59] = /*_.IF___V112*/ meltfptr[60];;
      /*^compute */
      /*_.IFCPP___V111*/ meltfptr[61] = /*_.PROGN___V114*/ meltfptr[59];;
      /*epilog */

      MELT_LOCATION ("warmelt-macro.melt:5513:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L32*/ meltfnum[30] = 0;
      /*^clear */
	     /*clear *//*_.IF___V112*/ meltfptr[60] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V114*/ meltfptr[59] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V111*/ meltfptr[61] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-macro.melt:5516:/ quasiblock");


    /*^newclosure */
		 /*newclosure *//*_.LAMBDA___V116*/ meltfptr[98] =
      (melt_ptr_t)
      meltgc_new_closure ((meltobject_ptr_t)
			  (((melt_ptr_t) (MELT_PREDEF (DISCR_CLOSURE)))),
			  (meltroutine_ptr_t) (( /*!konst_18 */ meltfrout->
						tabval[18])), (3));
    ;
    /*^putclosedv */
    /*putclosv */
    melt_assertmsg ("putclosv checkclo",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.LAMBDA___V116*/ meltfptr[98])) ==
		    MELTOBMAG_CLOSURE);
    melt_assertmsg ("putclosv checkoff", 0 >= 0
		    && 0 <
		    melt_closure_size ((melt_ptr_t)
				       ( /*_.LAMBDA___V116*/ meltfptr[98])));
    ((meltclosure_ptr_t) /*_.LAMBDA___V116*/ meltfptr[98])->tabval[0] =
      (melt_ptr_t) ( /*_.MEXPANDER__V4*/ meltfptr[3]);
    ;
    /*^putclosedv */
    /*putclosv */
    melt_assertmsg ("putclosv checkclo",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.LAMBDA___V116*/ meltfptr[98])) ==
		    MELTOBMAG_CLOSURE);
    melt_assertmsg ("putclosv checkoff", 1 >= 0
		    && 1 <
		    melt_closure_size ((melt_ptr_t)
				       ( /*_.LAMBDA___V116*/ meltfptr[98])));
    ((meltclosure_ptr_t) /*_.LAMBDA___V116*/ meltfptr[98])->tabval[1] =
      (melt_ptr_t) ( /*_.NEWENV__V25*/ meltfptr[24]);
    ;
    /*^putclosedv */
    /*putclosv */
    melt_assertmsg ("putclosv checkclo",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.LAMBDA___V116*/ meltfptr[98])) ==
		    MELTOBMAG_CLOSURE);
    melt_assertmsg ("putclosv checkoff", 2 >= 0
		    && 2 <
		    melt_closure_size ((melt_ptr_t)
				       ( /*_.LAMBDA___V116*/ meltfptr[98])));
    ((meltclosure_ptr_t) /*_.LAMBDA___V116*/ meltfptr[98])->tabval[2] =
      (melt_ptr_t) ( /*_.MODCTX__V5*/ meltfptr[4]);
    ;
    /*_.LAMBDA___V115*/ meltfptr[97] = /*_.LAMBDA___V116*/ meltfptr[98];;
    MELT_LOCATION ("warmelt-macro.melt:5514:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[2];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & ( /*!DISCR_MULTIPLE */ meltfrout->tabval[5]);
      /*^apply.arg */
      argtab[1].meltbp_aptr =
	(melt_ptr_t *) & /*_.LAMBDA___V115*/ meltfptr[97];
      /*_.PAIRLIST_TO_MULTIPLE__V117*/ meltfptr[103] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!PAIRLIST_TO_MULTIPLE */ meltfrout->tabval[4])),
		    (melt_ptr_t) ( /*_.RESTPAIR__V23*/ meltfptr[22]),
		    (MELTBPARSTR_PTR MELTBPARSTR_PTR ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;
    /*^compute */
    /*_.BODYTUP__V30*/ meltfptr[29] = /*_.SETQ___V118*/ meltfptr[100] =
      /*_.PAIRLIST_TO_MULTIPLE__V117*/ meltfptr[103];;
    MELT_LOCATION ("warmelt-macro.melt:5517:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#MULTIPLE_LENGTH__L34*/ meltfnum[8] =
      (melt_multiple_length
       ((melt_ptr_t) ( /*_.BODYTUP__V30*/ meltfptr[29])));;
    /*^compute */
 /*_#I__L35*/ meltfnum[9] =
      (( /*_#MULTIPLE_LENGTH__L34*/ meltfnum[8]) <= (0));;
    MELT_LOCATION ("warmelt-macro.melt:5517:/ cond");
    /*cond */ if ( /*_#I__L35*/ meltfnum[9])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{


	  {
	    MELT_LOCATION ("warmelt-macro.melt:5518:/ locexp");
	    /* error_plain */
	      melt_error_str ((melt_ptr_t) ( /*_.LOC__V20*/ meltfptr[19]),
			      ("emptyy body in LETREC"), (melt_ptr_t) 0);
	  }
	  ;
	  /*epilog */
	}
	;
      }				/*noelse */
    ;
    MELT_LOCATION ("warmelt-macro.melt:5519:/ quasiblock");


    MELT_LOCATION ("warmelt-macro.melt:5520:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*^rawallocobj */
    /*rawallocobj */
    {
      melt_ptr_t newobj = 0;
      melt_raw_object_create (newobj,
			      (melt_ptr_t) (( /*!CLASS_SOURCE_LETREC */
					     meltfrout->tabval[19])), (4),
			      "CLASS_SOURCE_LETREC");
  /*_.INST__V121*/ meltfptr[34] =
	newobj;
    };
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @LOCA_LOCATION",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V121*/ meltfptr[34])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V121*/ meltfptr[34]), (1),
			  ( /*_.LOC__V20*/ meltfptr[19]), "LOCA_LOCATION");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @SLET_BINDINGS",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V121*/ meltfptr[34])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V121*/ meltfptr[34]), (2),
			  ( /*_.SRCBINDTUP__V27*/ meltfptr[26]),
			  "SLET_BINDINGS");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @SLET_BODY",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V121*/ meltfptr[34])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V121*/ meltfptr[34]), (3),
			  ( /*_.BODYTUP__V30*/ meltfptr[29]), "SLET_BODY");
    ;
    /*^touchobj */

    melt_dbgtrace_written_object ( /*_.INST__V121*/ meltfptr[34],
				  "newly made instance");
    ;
    /*_.LETR__V120*/ meltfptr[33] = /*_.INST__V121*/ meltfptr[34];;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-macro.melt:5524:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L36*/ meltfnum[0] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-macro.melt:5524:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L36*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L37*/ meltfnum[15] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-macro.melt:5524:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long =
		/*_#THE_MELTCALLCOUNT__L37*/ meltfnum[15];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-macro.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 5524;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "mexpand_letrec result";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.LETR__V120*/ meltfptr[33];
	      /*_.MELT_DEBUG_FUN__V124*/ meltfptr[38] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V123*/ meltfptr[37] =
	      /*_.MELT_DEBUG_FUN__V124*/ meltfptr[38];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-macro.melt:5524:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L37*/ meltfnum[15] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V124*/ meltfptr[38] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V123*/ meltfptr[37] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-macro.melt:5524:/ quasiblock");


      /*_.PROGN___V125*/ meltfptr[39] = /*_.IF___V123*/ meltfptr[37];;
      /*^compute */
      /*_.IFCPP___V122*/ meltfptr[35] = /*_.PROGN___V125*/ meltfptr[39];;
      /*epilog */

      MELT_LOCATION ("warmelt-macro.melt:5524:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L36*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IF___V123*/ meltfptr[37] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V125*/ meltfptr[39] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V122*/ meltfptr[35] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-macro.melt:5525:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.LETR__V120*/ meltfptr[33];;

    {
      MELT_LOCATION ("warmelt-macro.melt:5525:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*_.LET___V119*/ meltfptr[104] = /*_.RETURN___V126*/ meltfptr[40];;

    MELT_LOCATION ("warmelt-macro.melt:5519:/ clear");
	   /*clear *//*_.LETR__V120*/ meltfptr[33] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V122*/ meltfptr[35] = 0;
    /*^clear */
	   /*clear *//*_.RETURN___V126*/ meltfptr[40] = 0;
    /*_.LET___V18*/ meltfptr[16] = /*_.LET___V119*/ meltfptr[104];;

    MELT_LOCATION ("warmelt-macro.melt:5389:/ clear");
	   /*clear *//*_.CONT__V19*/ meltfptr[18] = 0;
    /*^clear */
	   /*clear *//*_.LOC__V20*/ meltfptr[19] = 0;
    /*^clear */
	   /*clear *//*_.LIST_FIRST__V21*/ meltfptr[20] = 0;
    /*^clear */
	   /*clear *//*_.SECPAIR__V22*/ meltfptr[21] = 0;
    /*^clear */
	   /*clear *//*_.RESTPAIR__V23*/ meltfptr[22] = 0;
    /*^clear */
	   /*clear *//*_.BINDEXPR__V24*/ meltfptr[23] = 0;
    /*^clear */
	   /*clear *//*_.NEWENV__V25*/ meltfptr[24] = 0;
    /*^clear */
	   /*clear *//*_#NBIND__L7*/ meltfnum[1] = 0;
    /*^clear */
	   /*clear *//*_.BINDTUP__V26*/ meltfptr[25] = 0;
    /*^clear */
	   /*clear *//*_.SRCBINDTUP__V27*/ meltfptr[26] = 0;
    /*^clear */
	   /*clear *//*_.VARTUP__V28*/ meltfptr[27] = 0;
    /*^clear */
	   /*clear *//*_.EXPRTUP__V29*/ meltfptr[28] = 0;
    /*^clear */
	   /*clear *//*_.BODYTUP__V30*/ meltfptr[29] = 0;
    /*^clear */
	   /*clear *//*_.IF___V31*/ meltfptr[30] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V111*/ meltfptr[61] = 0;
    /*^clear */
	   /*clear *//*_.LAMBDA___V115*/ meltfptr[97] = 0;
    /*^clear */
	   /*clear *//*_.PAIRLIST_TO_MULTIPLE__V117*/ meltfptr[103] = 0;
    /*^clear */
	   /*clear *//*_.SETQ___V118*/ meltfptr[100] = 0;
    /*^clear */
	   /*clear *//*_#MULTIPLE_LENGTH__L34*/ meltfnum[8] = 0;
    /*^clear */
	   /*clear *//*_#I__L35*/ meltfnum[9] = 0;
    /*^clear */
	   /*clear *//*_.LET___V119*/ meltfptr[104] = 0;
    MELT_LOCATION ("warmelt-macro.melt:5383:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.LET___V18*/ meltfptr[16];;

    {
      MELT_LOCATION ("warmelt-macro.melt:5383:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V6*/ meltfptr[5] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V10*/ meltfptr[6] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V12*/ meltfptr[7] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V14*/ meltfptr[12] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V16*/ meltfptr[14] = 0;
    /*^clear */
	   /*clear *//*_.LET___V18*/ meltfptr[16] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("MEXPAND_LETREC", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_108_warmelt_macro_MEXPAND_LETREC_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_108_warmelt_macro_MEXPAND_LETREC */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_109_warmelt_macro_LAMBDA___26__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un * meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un * meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_109_warmelt_macro_LAMBDA___26___melt =
    melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_109_warmelt_macro_LAMBDA___26___st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 2
    melt_ptr_t mcfr_varptr[2];
#define MELTFRAM_NBVARNUM 1
    long mcfr_varnum[1];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_109_warmelt_macro_LAMBDA___26__ is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_109_warmelt_macro_LAMBDA___26___st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 2; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_109_warmelt_macro_LAMBDA___26__ nbval 2*/
  meltfram__.mcfr_nbvar = 2 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("LAMBDA_", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-macro.melt:5410:/ getarg");
 /*_.BX__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {

    MELT_LOCATION ("warmelt-macro.melt:5411:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#IS_NOT_A__L1*/ meltfnum[0] =
      !melt_is_instance_of ((melt_ptr_t) ( /*_.BX__V2*/ meltfptr[1]),
			    (melt_ptr_t) (( /*!CLASS_SEXPR */ meltfrout->
					   tabval[0])));;
    MELT_LOCATION ("warmelt-macro.melt:5411:/ cond");
    /*cond */ if ( /*_#IS_NOT_A__L1*/ meltfnum[0])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{


	  {
	    MELT_LOCATION ("warmelt-macro.melt:5412:/ locexp");
	    /* error_plain */
	      melt_error_str ((melt_ptr_t)
			      (( /*~LOC */ meltfclos->tabval[0])),
			      ("sexpr expected in LETREC binding"),
			      (melt_ptr_t) 0);
	  }
	  ;
	  /*epilog */
	}
	;
      }				/*noelse */
    ;
    MELT_LOCATION ("warmelt-macro.melt:5410:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.BX__V2*/ meltfptr[1];;

    {
      MELT_LOCATION ("warmelt-macro.melt:5410:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_#IS_NOT_A__L1*/ meltfnum[0] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("LAMBDA_", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_109_warmelt_macro_LAMBDA___26___melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_109_warmelt_macro_LAMBDA___26__ */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_110_warmelt_macro_LAMBDA___27__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un * meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un * meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_110_warmelt_macro_LAMBDA___27___melt =
    melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_110_warmelt_macro_LAMBDA___27___st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 3
    melt_ptr_t mcfr_varptr[3];
/*no varnum*/
#define MELTFRAM_NBVARNUM /*none*/0
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_110_warmelt_macro_LAMBDA___27__ is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_110_warmelt_macro_LAMBDA___27___st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 3; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_110_warmelt_macro_LAMBDA___27__ nbval 3*/
  meltfram__.mcfr_nbvar = 3 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("LAMBDA_", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-macro.melt:5516:/ getarg");
 /*_.E__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {

    /*^checksignal */
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[3];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & ( /*~NEWENV */ meltfclos->tabval[1]);
      /*^apply.arg */
      argtab[1].meltbp_aptr =
	(melt_ptr_t *) & ( /*~MEXPANDER */ meltfclos->tabval[0]);
      /*^apply.arg */
      argtab[2].meltbp_aptr =
	(melt_ptr_t *) & ( /*~MODCTX */ meltfclos->tabval[2]);
      /*_.MEXPANDER__V3*/ meltfptr[2] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*~MEXPANDER */ meltfclos->tabval[0])),
		    (melt_ptr_t) ( /*_.E__V2*/ meltfptr[1]),
		    (MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_PTR ""),
		    argtab, "", (union meltparam_un *) 0);
    }
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.MEXPANDER__V3*/ meltfptr[2];;

    {
      MELT_LOCATION ("warmelt-macro.melt:5516:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.MEXPANDER__V3*/ meltfptr[2] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("LAMBDA_", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_110_warmelt_macro_LAMBDA___27___melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_110_warmelt_macro_LAMBDA___27__ */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_111_warmelt_macro_MEXPAND_LAMBDA (meltclosure_ptr_t meltclosp_,
					   melt_ptr_t meltfirstargp_,
					   const melt_argdescr_cell_t
					   meltxargdescr_[],
					   union meltparam_un * meltxargtab_,
					   const melt_argdescr_cell_t
					   meltxresdescr_[],
					   union meltparam_un * meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_111_warmelt_macro_MEXPAND_LAMBDA_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_111_warmelt_macro_MEXPAND_LAMBDA_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 35
    melt_ptr_t mcfr_varptr[35];
#define MELTFRAM_NBVARNUM 9
    long mcfr_varnum[9];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_111_warmelt_macro_MEXPAND_LAMBDA is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_111_warmelt_macro_MEXPAND_LAMBDA_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 35; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_111_warmelt_macro_MEXPAND_LAMBDA nbval 35*/
  meltfram__.mcfr_nbvar = 35 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("MEXPAND_LAMBDA", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-macro.melt:5538:/ getarg");
 /*_.SEXPR__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.ENV__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.ENV__V3*/ meltfptr[2])) != NULL);


  /*getarg#2 */
  /*^getarg */
  if (meltxargdescr_[1] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.MEXPANDER__V4*/ meltfptr[3] =
    (meltxargtab_[1].meltbp_aptr) ? (*(meltxargtab_[1].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.MEXPANDER__V4*/ meltfptr[3])) !=
	      NULL);


  /*getarg#3 */
  /*^getarg */
  if (meltxargdescr_[2] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.MODCTX__V5*/ meltfptr[4] =
    (meltxargtab_[2].meltbp_aptr) ? (*(meltxargtab_[2].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.MODCTX__V5*/ meltfptr[4])) !=
	      NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-macro.melt:5539:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L1*/ meltfnum[0] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.SEXPR__V2*/ meltfptr[1]),
			     (melt_ptr_t) (( /*!CLASS_SEXPR */ meltfrout->
					    tabval[0])));;
      MELT_LOCATION ("warmelt-macro.melt:5539:/ cond");
      /*cond */ if ( /*_#IS_A__L1*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V7*/ meltfptr[6] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-macro.melt:5539:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check sexpr"),
				  ("warmelt-macro.melt")
				  ? ("warmelt-macro.melt") : __FILE__,
				  (5539) ? (5539) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V7*/ meltfptr[6] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V6*/ meltfptr[5] = /*_.IFELSE___V7*/ meltfptr[6];;
      /*epilog */

      MELT_LOCATION ("warmelt-macro.melt:5539:/ clear");
	     /*clear *//*_#IS_A__L1*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V7*/ meltfptr[6] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V6*/ meltfptr[5] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-macro.melt:5540:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L2*/ meltfnum[0] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.ENV__V3*/ meltfptr[2]),
			     (melt_ptr_t) (( /*!CLASS_ENVIRONMENT */
					    meltfrout->tabval[1])));;
      MELT_LOCATION ("warmelt-macro.melt:5540:/ cond");
      /*cond */ if ( /*_#IS_A__L2*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V9*/ meltfptr[8] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-macro.melt:5540:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check env"),
				  ("warmelt-macro.melt")
				  ? ("warmelt-macro.melt") : __FILE__,
				  (5540) ? (5540) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V9*/ meltfptr[8] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V8*/ meltfptr[6] = /*_.IFELSE___V9*/ meltfptr[8];;
      /*epilog */

      MELT_LOCATION ("warmelt-macro.melt:5540:/ clear");
	     /*clear *//*_#IS_A__L2*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V9*/ meltfptr[8] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V8*/ meltfptr[6] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-macro.melt:5541:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_OBJECT__L3*/ meltfnum[0] =
	(melt_magic_discr ((melt_ptr_t) ( /*_.MODCTX__V5*/ meltfptr[4])) ==
	 MELTOBMAG_OBJECT);;
      MELT_LOCATION ("warmelt-macro.melt:5541:/ cond");
      /*cond */ if ( /*_#IS_OBJECT__L3*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V11*/ meltfptr[10] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-macro.melt:5541:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check modctx"),
				  ("warmelt-macro.melt")
				  ? ("warmelt-macro.melt") : __FILE__,
				  (5541) ? (5541) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V11*/ meltfptr[10] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V10*/ meltfptr[8] = /*_.IFELSE___V11*/ meltfptr[10];;
      /*epilog */

      MELT_LOCATION ("warmelt-macro.melt:5541:/ clear");
	     /*clear *//*_#IS_OBJECT__L3*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V11*/ meltfptr[10] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V10*/ meltfptr[8] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-macro.melt:5542:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_CLOSURE__L4*/ meltfnum[0] =
	(melt_magic_discr ((melt_ptr_t) ( /*_.MEXPANDER__V4*/ meltfptr[3])) ==
	 MELTOBMAG_CLOSURE);;
      MELT_LOCATION ("warmelt-macro.melt:5542:/ cond");
      /*cond */ if ( /*_#IS_CLOSURE__L4*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V13*/ meltfptr[12] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-macro.melt:5542:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check mexpander"),
				  ("warmelt-macro.melt")
				  ? ("warmelt-macro.melt") : __FILE__,
				  (5542) ? (5542) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V13*/ meltfptr[12] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V12*/ meltfptr[10] = /*_.IFELSE___V13*/ meltfptr[12];;
      /*epilog */

      MELT_LOCATION ("warmelt-macro.melt:5542:/ clear");
	     /*clear *//*_#IS_CLOSURE__L4*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V13*/ meltfptr[12] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V12*/ meltfptr[10] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-macro.melt:5543:/ quasiblock");


    /*^getslot */
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.SEXPR__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 2, "SEXP_CONTENTS");
  /*_.CONT__V15*/ meltfptr[14] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-macro.melt:5544:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.SEXPR__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 1, "LOCA_LOCATION");
  /*_.LOC__V16*/ meltfptr[15] = slot;
    };
    ;
 /*_.LIST_FIRST__V17*/ meltfptr[16] =
      (melt_list_first ((melt_ptr_t) ( /*_.CONT__V15*/ meltfptr[14])));;
    /*^compute */
 /*_.CURPAIR__V18*/ meltfptr[17] =
      (melt_pair_tail ((melt_ptr_t) ( /*_.LIST_FIRST__V17*/ meltfptr[16])));;
    MELT_LOCATION ("warmelt-macro.melt:5546:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      /*_.NEWENV__V19*/ meltfptr[18] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!FRESH_ENV */ meltfrout->tabval[2])),
		    (melt_ptr_t) ( /*_.ENV__V3*/ meltfptr[2]), (""),
		    (union meltparam_un *) 0, "", (union meltparam_un *) 0);
    }
    ;
 /*_.FORMALS__V20*/ meltfptr[19] =
      (melt_pair_head ((melt_ptr_t) ( /*_.CURPAIR__V18*/ meltfptr[17])));;
    MELT_LOCATION ("warmelt-macro.melt:5550:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^checksignal */
    MELT_CHECK_SIGNAL ();
    ;
 /*_#NOTNULL__L5*/ meltfnum[0] =
      (( /*_.FORMALS__V20*/ meltfptr[19]) != NULL);;
    MELT_LOCATION ("warmelt-macro.melt:5550:/ cond");
    /*cond */ if ( /*_#NOTNULL__L5*/ meltfnum[0])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

   /*_#IS_NOT_A__L7*/ meltfnum[6] =
	    !melt_is_instance_of ((melt_ptr_t)
				  ( /*_.FORMALS__V20*/ meltfptr[19]),
				  (melt_ptr_t) (( /*!CLASS_SEXPR */
						 meltfrout->tabval[0])));;
	  /*^compute */
	  /*_#IF___L6*/ meltfnum[5] = /*_#IS_NOT_A__L7*/ meltfnum[6];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-macro.melt:5550:/ clear");
	     /*clear *//*_#IS_NOT_A__L7*/ meltfnum[6] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

  /*_#IF___L6*/ meltfnum[5] = 0;;
      }
    ;
    MELT_LOCATION ("warmelt-macro.melt:5550:/ cond");
    /*cond */ if ( /*_#IF___L6*/ meltfnum[5])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{


	  {
	    MELT_LOCATION ("warmelt-macro.melt:5551:/ locexp");
	    /* error_plain */
	      melt_error_str ((melt_ptr_t) ( /*_.LOC__V16*/ meltfptr[15]),
			      ("missing formal argument list in (LAMBDA (arglist...) body...)"),
			      (melt_ptr_t) 0);
	  }
	  ;
	  /*epilog */
	}
	;
      }				/*noelse */
    ;
    MELT_LOCATION ("warmelt-macro.melt:5552:/ quasiblock");


    /*^checksignal */
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.SEXPR__V2*/ meltfptr[1];
      /*_.ARGTUP__V22*/ meltfptr[21] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!LAMBDA_ARG_BINDINGS */ meltfrout->tabval[3])),
		    (melt_ptr_t) ( /*_.FORMALS__V20*/ meltfptr[19]),
		    (MELTBPARSTR_PTR ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;
 /*_.PAIR_TAIL__V23*/ meltfptr[22] =
      (melt_pair_tail ((melt_ptr_t) ( /*_.CURPAIR__V18*/ meltfptr[17])));;
    MELT_LOCATION ("warmelt-macro.melt:5553:/ compute");
    /*_.CURPAIR__V18*/ meltfptr[17] = /*_.SETQ___V24*/ meltfptr[23] =
      /*_.PAIR_TAIL__V23*/ meltfptr[22];;
    /*citerblock FOREACH_IN_MULTIPLE */
    {
      /* start foreach_in_multiple meltcit1__EACHTUP */
      long meltcit1__EACHTUP_ln =
	melt_multiple_length ((melt_ptr_t) /*_.ARGTUP__V22*/ meltfptr[21]);
      for ( /*_#IX__L8*/ meltfnum[6] = 0;
	   ( /*_#IX__L8*/ meltfnum[6] >= 0)
	   && ( /*_#IX__L8*/ meltfnum[6] < meltcit1__EACHTUP_ln);
	/*_#IX__L8*/ meltfnum[6]++)
	{
	  /*_.LB__V25*/ meltfptr[24] =
	    melt_multiple_nth ((melt_ptr_t) ( /*_.ARGTUP__V22*/ meltfptr[21]),
			       /*_#IX__L8*/ meltfnum[6]);




#if MELT_HAVE_DEBUG
	  MELT_LOCATION ("warmelt-macro.melt:5557:/ cppif.then");
	  /*^block */
	  /*anyblock */
	  {

	    /*^checksignal */
	    MELT_CHECK_SIGNAL ();
	    ;
    /*_#IS_A__L9*/ meltfnum[8] =
	      melt_is_instance_of ((melt_ptr_t) ( /*_.LB__V25*/ meltfptr[24]),
				   (melt_ptr_t) (( /*!CLASS_FORMAL_BINDING */
						  meltfrout->tabval[4])));;
	    MELT_LOCATION ("warmelt-macro.melt:5557:/ cond");
	    /*cond */ if ( /*_#IS_A__L9*/ meltfnum[8])	/*then */
	      {
		/*^cond.then */
		/*_.IFELSE___V27*/ meltfptr[26] = ( /*nil */ NULL);;
	      }
	    else
	      {
		MELT_LOCATION ("warmelt-macro.melt:5557:/ cond.else");

		/*^block */
		/*anyblock */
		{




		  {
		    /*^locexp */
		    melt_assert_failed (("check lb"),
					("warmelt-macro.melt")
					? ("warmelt-macro.melt") : __FILE__,
					(5557) ? (5557) : __LINE__,
					__FUNCTION__);
		    ;
		  }
		  ;
		/*clear *//*_.IFELSE___V27*/ meltfptr[26] = 0;
		  /*epilog */
		}
		;
	      }
	    ;
	    /*^compute */
	    /*_.IFCPP___V26*/ meltfptr[25] = /*_.IFELSE___V27*/ meltfptr[26];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-macro.melt:5557:/ clear");
	      /*clear *//*_#IS_A__L9*/ meltfnum[8] = 0;
	    /*^clear */
	      /*clear *//*_.IFELSE___V27*/ meltfptr[26] = 0;
	  }

#else /*MELT_HAVE_DEBUG */
	  /*^cppif.else */
	  /*_.IFCPP___V26*/ meltfptr[25] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
	  ;
	  MELT_LOCATION ("warmelt-macro.melt:5558:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^apply */
	  /*apply */
	  {
	    union meltparam_un argtab[1];
	    memset (&argtab, 0, sizeof (argtab));
	    /*^apply.arg */
	    argtab[0].meltbp_aptr =
	      (melt_ptr_t *) & /*_.LB__V25*/ meltfptr[24];
	    /*_.PUT_ENV__V28*/ meltfptr[26] =
	      melt_apply ((meltclosure_ptr_t)
			  (( /*!PUT_ENV */ meltfrout->tabval[5])),
			  (melt_ptr_t) ( /*_.NEWENV__V19*/ meltfptr[18]),
			  (MELTBPARSTR_PTR ""), argtab, "",
			  (union meltparam_un *) 0);
	  }
	  ;
	  if ( /*_#IX__L8*/ meltfnum[6] < 0)
	    break;
	}			/* end  foreach_in_multiple meltcit1__EACHTUP */

      /*citerepilog */

      MELT_LOCATION ("warmelt-macro.melt:5554:/ clear");
	    /*clear *//*_.LB__V25*/ meltfptr[24] = 0;
      /*^clear */
	    /*clear *//*_#IX__L8*/ meltfnum[6] = 0;
      /*^clear */
	    /*clear *//*_.IFCPP___V26*/ meltfptr[25] = 0;
      /*^clear */
	    /*clear *//*_.PUT_ENV__V28*/ meltfptr[26] = 0;
    }				/*endciterblock FOREACH_IN_MULTIPLE */
    ;
    MELT_LOCATION ("warmelt-macro.melt:5559:/ quasiblock");


    MELT_LOCATION ("warmelt-macro.melt:5561:/ quasiblock");


    /*^newclosure */
		 /*newclosure *//*_.LAMBDA___V31*/ meltfptr[30] =
      (melt_ptr_t)
      meltgc_new_closure ((meltobject_ptr_t)
			  (((melt_ptr_t) (MELT_PREDEF (DISCR_CLOSURE)))),
			  (meltroutine_ptr_t) (( /*!konst_8 */ meltfrout->
						tabval[8])), (3));
    ;
    /*^putclosedv */
    /*putclosv */
    melt_assertmsg ("putclosv checkclo",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.LAMBDA___V31*/ meltfptr[30])) ==
		    MELTOBMAG_CLOSURE);
    melt_assertmsg ("putclosv checkoff", 0 >= 0
		    && 0 <
		    melt_closure_size ((melt_ptr_t)
				       ( /*_.LAMBDA___V31*/ meltfptr[30])));
    ((meltclosure_ptr_t) /*_.LAMBDA___V31*/ meltfptr[30])->tabval[0] =
      (melt_ptr_t) ( /*_.MEXPANDER__V4*/ meltfptr[3]);
    ;
    /*^putclosedv */
    /*putclosv */
    melt_assertmsg ("putclosv checkclo",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.LAMBDA___V31*/ meltfptr[30])) ==
		    MELTOBMAG_CLOSURE);
    melt_assertmsg ("putclosv checkoff", 1 >= 0
		    && 1 <
		    melt_closure_size ((melt_ptr_t)
				       ( /*_.LAMBDA___V31*/ meltfptr[30])));
    ((meltclosure_ptr_t) /*_.LAMBDA___V31*/ meltfptr[30])->tabval[1] =
      (melt_ptr_t) ( /*_.NEWENV__V19*/ meltfptr[18]);
    ;
    /*^putclosedv */
    /*putclosv */
    melt_assertmsg ("putclosv checkclo",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.LAMBDA___V31*/ meltfptr[30])) ==
		    MELTOBMAG_CLOSURE);
    melt_assertmsg ("putclosv checkoff", 2 >= 0
		    && 2 <
		    melt_closure_size ((melt_ptr_t)
				       ( /*_.LAMBDA___V31*/ meltfptr[30])));
    ((meltclosure_ptr_t) /*_.LAMBDA___V31*/ meltfptr[30])->tabval[2] =
      (melt_ptr_t) ( /*_.MODCTX__V5*/ meltfptr[4]);
    ;
    /*_.LAMBDA___V30*/ meltfptr[29] = /*_.LAMBDA___V31*/ meltfptr[30];;
    MELT_LOCATION ("warmelt-macro.melt:5559:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[2];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & ( /*!DISCR_MULTIPLE */ meltfrout->tabval[7]);
      /*^apply.arg */
      argtab[1].meltbp_aptr =
	(melt_ptr_t *) & /*_.LAMBDA___V30*/ meltfptr[29];
      /*_.BODYTUP__V32*/ meltfptr[31] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!PAIRLIST_TO_MULTIPLE */ meltfrout->tabval[6])),
		    (melt_ptr_t) ( /*_.CURPAIR__V18*/ meltfptr[17]),
		    (MELTBPARSTR_PTR MELTBPARSTR_PTR ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-macro.melt:5563:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*^rawallocobj */
    /*rawallocobj */
    {
      melt_ptr_t newobj = 0;
      melt_raw_object_create (newobj,
			      (melt_ptr_t) (( /*!CLASS_SOURCE_LAMBDA */
					     meltfrout->tabval[9])), (4),
			      "CLASS_SOURCE_LAMBDA");
  /*_.INST__V34*/ meltfptr[33] =
	newobj;
    };
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @LOCA_LOCATION",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V34*/ meltfptr[33])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V34*/ meltfptr[33]), (1),
			  ( /*_.LOC__V16*/ meltfptr[15]), "LOCA_LOCATION");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @SLAM_ARGBIND",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V34*/ meltfptr[33])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V34*/ meltfptr[33]), (2),
			  ( /*_.ARGTUP__V22*/ meltfptr[21]), "SLAM_ARGBIND");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @SLAM_BODY",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V34*/ meltfptr[33])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V34*/ meltfptr[33]), (3),
			  ( /*_.BODYTUP__V32*/ meltfptr[31]), "SLAM_BODY");
    ;
    /*^touchobj */

    melt_dbgtrace_written_object ( /*_.INST__V34*/ meltfptr[33],
				  "newly made instance");
    ;
    /*_.LAMBR__V33*/ meltfptr[32] = /*_.INST__V34*/ meltfptr[33];;
    MELT_LOCATION ("warmelt-macro.melt:5568:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.LAMBR__V33*/ meltfptr[32];;

    {
      MELT_LOCATION ("warmelt-macro.melt:5568:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*_.LET___V29*/ meltfptr[28] = /*_.RETURN___V35*/ meltfptr[34];;

    MELT_LOCATION ("warmelt-macro.melt:5559:/ clear");
	   /*clear *//*_.LAMBDA___V30*/ meltfptr[29] = 0;
    /*^clear */
	   /*clear *//*_.BODYTUP__V32*/ meltfptr[31] = 0;
    /*^clear */
	   /*clear *//*_.LAMBR__V33*/ meltfptr[32] = 0;
    /*^clear */
	   /*clear *//*_.RETURN___V35*/ meltfptr[34] = 0;
    /*_.LET___V21*/ meltfptr[20] = /*_.LET___V29*/ meltfptr[28];;

    MELT_LOCATION ("warmelt-macro.melt:5552:/ clear");
	   /*clear *//*_.ARGTUP__V22*/ meltfptr[21] = 0;
    /*^clear */
	   /*clear *//*_.PAIR_TAIL__V23*/ meltfptr[22] = 0;
    /*^clear */
	   /*clear *//*_.SETQ___V24*/ meltfptr[23] = 0;
    /*^clear */
	   /*clear *//*_.LET___V29*/ meltfptr[28] = 0;
    /*_.LET___V14*/ meltfptr[12] = /*_.LET___V21*/ meltfptr[20];;

    MELT_LOCATION ("warmelt-macro.melt:5543:/ clear");
	   /*clear *//*_.CONT__V15*/ meltfptr[14] = 0;
    /*^clear */
	   /*clear *//*_.LOC__V16*/ meltfptr[15] = 0;
    /*^clear */
	   /*clear *//*_.LIST_FIRST__V17*/ meltfptr[16] = 0;
    /*^clear */
	   /*clear *//*_.CURPAIR__V18*/ meltfptr[17] = 0;
    /*^clear */
	   /*clear *//*_.NEWENV__V19*/ meltfptr[18] = 0;
    /*^clear */
	   /*clear *//*_.FORMALS__V20*/ meltfptr[19] = 0;
    /*^clear */
	   /*clear *//*_#NOTNULL__L5*/ meltfnum[0] = 0;
    /*^clear */
	   /*clear *//*_#IF___L6*/ meltfnum[5] = 0;
    /*^clear */
	   /*clear *//*_.LET___V21*/ meltfptr[20] = 0;
    MELT_LOCATION ("warmelt-macro.melt:5538:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.LET___V14*/ meltfptr[12];;

    {
      MELT_LOCATION ("warmelt-macro.melt:5538:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V6*/ meltfptr[5] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V8*/ meltfptr[6] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V10*/ meltfptr[8] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V12*/ meltfptr[10] = 0;
    /*^clear */
	   /*clear *//*_.LET___V14*/ meltfptr[12] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("MEXPAND_LAMBDA", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_111_warmelt_macro_MEXPAND_LAMBDA_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_111_warmelt_macro_MEXPAND_LAMBDA */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_112_warmelt_macro_LAMBDA___28__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un * meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un * meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_112_warmelt_macro_LAMBDA___28___melt =
    melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_112_warmelt_macro_LAMBDA___28___st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 3
    melt_ptr_t mcfr_varptr[3];
/*no varnum*/
#define MELTFRAM_NBVARNUM /*none*/0
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_112_warmelt_macro_LAMBDA___28__ is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_112_warmelt_macro_LAMBDA___28___st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 3; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_112_warmelt_macro_LAMBDA___28__ nbval 3*/
  meltfram__.mcfr_nbvar = 3 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("LAMBDA_", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-macro.melt:5561:/ getarg");
 /*_.E__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {

    /*^checksignal */
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[3];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & ( /*~NEWENV */ meltfclos->tabval[1]);
      /*^apply.arg */
      argtab[1].meltbp_aptr =
	(melt_ptr_t *) & ( /*~MEXPANDER */ meltfclos->tabval[0]);
      /*^apply.arg */
      argtab[2].meltbp_aptr =
	(melt_ptr_t *) & ( /*~MODCTX */ meltfclos->tabval[2]);
      /*_.MEXPANDER__V3*/ meltfptr[2] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*~MEXPANDER */ meltfclos->tabval[0])),
		    (melt_ptr_t) ( /*_.E__V2*/ meltfptr[1]),
		    (MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_PTR ""),
		    argtab, "", (union meltparam_un *) 0);
    }
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.MEXPANDER__V3*/ meltfptr[2];;

    {
      MELT_LOCATION ("warmelt-macro.melt:5561:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.MEXPANDER__V3*/ meltfptr[2] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("LAMBDA_", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_112_warmelt_macro_LAMBDA___28___melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_112_warmelt_macro_LAMBDA___28__ */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_113_warmelt_macro_MEXPAND_VARIADIC (meltclosure_ptr_t meltclosp_,
					     melt_ptr_t meltfirstargp_,
					     const melt_argdescr_cell_t
					     meltxargdescr_[],
					     union meltparam_un *
					     meltxargtab_,
					     const melt_argdescr_cell_t
					     meltxresdescr_[],
					     union meltparam_un *
					     meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_113_warmelt_macro_MEXPAND_VARIADIC_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_113_warmelt_macro_MEXPAND_VARIADIC_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 116
    melt_ptr_t mcfr_varptr[116];
#define MELTFRAM_NBVARNUM 35
    long mcfr_varnum[35];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_113_warmelt_macro_MEXPAND_VARIADIC is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_113_warmelt_macro_MEXPAND_VARIADIC_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 116; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_113_warmelt_macro_MEXPAND_VARIADIC nbval 116*/
  meltfram__.mcfr_nbvar = 116 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("MEXPAND_VARIADIC", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-macro.melt:5589:/ getarg");
 /*_.SEXPR__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.ENV__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.ENV__V3*/ meltfptr[2])) != NULL);


  /*getarg#2 */
  /*^getarg */
  if (meltxargdescr_[1] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.MEXPANDER__V4*/ meltfptr[3] =
    (meltxargtab_[1].meltbp_aptr) ? (*(meltxargtab_[1].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.MEXPANDER__V4*/ meltfptr[3])) !=
	      NULL);


  /*getarg#3 */
  /*^getarg */
  if (meltxargdescr_[2] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.MODCTX__V5*/ meltfptr[4] =
    (meltxargtab_[2].meltbp_aptr) ? (*(meltxargtab_[2].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.MODCTX__V5*/ meltfptr[4])) !=
	      NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-macro.melt:5590:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L1*/ meltfnum[0] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.SEXPR__V2*/ meltfptr[1]),
			     (melt_ptr_t) (( /*!CLASS_SEXPR */ meltfrout->
					    tabval[0])));;
      MELT_LOCATION ("warmelt-macro.melt:5590:/ cond");
      /*cond */ if ( /*_#IS_A__L1*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V7*/ meltfptr[6] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-macro.melt:5590:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check sexpr"),
				  ("warmelt-macro.melt")
				  ? ("warmelt-macro.melt") : __FILE__,
				  (5590) ? (5590) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V7*/ meltfptr[6] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V6*/ meltfptr[5] = /*_.IFELSE___V7*/ meltfptr[6];;
      /*epilog */

      MELT_LOCATION ("warmelt-macro.melt:5590:/ clear");
	     /*clear *//*_#IS_A__L1*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V7*/ meltfptr[6] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V6*/ meltfptr[5] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-macro.melt:5591:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L2*/ meltfnum[0] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.ENV__V3*/ meltfptr[2]),
			     (melt_ptr_t) (( /*!CLASS_ENVIRONMENT */
					    meltfrout->tabval[1])));;
      MELT_LOCATION ("warmelt-macro.melt:5591:/ cond");
      /*cond */ if ( /*_#IS_A__L2*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V9*/ meltfptr[8] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-macro.melt:5591:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check env"),
				  ("warmelt-macro.melt")
				  ? ("warmelt-macro.melt") : __FILE__,
				  (5591) ? (5591) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V9*/ meltfptr[8] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V8*/ meltfptr[6] = /*_.IFELSE___V9*/ meltfptr[8];;
      /*epilog */

      MELT_LOCATION ("warmelt-macro.melt:5591:/ clear");
	     /*clear *//*_#IS_A__L2*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V9*/ meltfptr[8] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V8*/ meltfptr[6] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-macro.melt:5592:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_OBJECT__L3*/ meltfnum[0] =
	(melt_magic_discr ((melt_ptr_t) ( /*_.MODCTX__V5*/ meltfptr[4])) ==
	 MELTOBMAG_OBJECT);;
      MELT_LOCATION ("warmelt-macro.melt:5592:/ cond");
      /*cond */ if ( /*_#IS_OBJECT__L3*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V11*/ meltfptr[10] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-macro.melt:5592:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check modctx"),
				  ("warmelt-macro.melt")
				  ? ("warmelt-macro.melt") : __FILE__,
				  (5592) ? (5592) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V11*/ meltfptr[10] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V10*/ meltfptr[8] = /*_.IFELSE___V11*/ meltfptr[10];;
      /*epilog */

      MELT_LOCATION ("warmelt-macro.melt:5592:/ clear");
	     /*clear *//*_#IS_OBJECT__L3*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V11*/ meltfptr[10] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V10*/ meltfptr[8] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-macro.melt:5593:/ quasiblock");


    /*^getslot */
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.SEXPR__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 2, "SEXP_CONTENTS");
  /*_.CONT__V13*/ meltfptr[12] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-macro.melt:5594:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.SEXPR__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 1, "LOCA_LOCATION");
  /*_.LOC__V14*/ meltfptr[13] = slot;
    };
    ;
 /*_.LIST_FIRST__V15*/ meltfptr[14] =
      (melt_list_first ((melt_ptr_t) ( /*_.CONT__V13*/ meltfptr[12])));;
    /*^compute */
 /*_.FIRSTPAIR__V16*/ meltfptr[15] =
      (melt_pair_tail ((melt_ptr_t) ( /*_.LIST_FIRST__V15*/ meltfptr[14])));;
    MELT_LOCATION ("warmelt-macro.melt:5596:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*^rawallocobj */
    /*rawallocobj */
    {
      melt_ptr_t newobj = 0;
      melt_raw_object_create (newobj,
			      (melt_ptr_t) (( /*!CLASS_REFERENCE */
					     meltfrout->tabval[2])), (1),
			      "CLASS_REFERENCE");
  /*_.INST__V18*/ meltfptr[17] =
	newobj;
    };
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @REFERENCED_VALUE",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V18*/ meltfptr[17])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V18*/ meltfptr[17]), (0),
			  (( /*nil */ NULL)), "REFERENCED_VALUE");
    ;
    /*^touchobj */

    melt_dbgtrace_written_object ( /*_.INST__V18*/ meltfptr[17],
				  "newly made instance");
    ;
    /*_.RESCONT__V17*/ meltfptr[16] = /*_.INST__V18*/ meltfptr[17];;
    /*^compute */
 /*_#LIST_LENGTH__L4*/ meltfnum[0] =
      (melt_list_length ((melt_ptr_t) ( /*_.CONT__V13*/ meltfptr[12])));;
    /*^compute */
 /*_#I__L5*/ meltfnum[4] =
      ((3) * ( /*_#LIST_LENGTH__L4*/ meltfnum[0]));;
    /*^compute */
 /*_#I__L6*/ meltfnum[5] =
      ((7) + ( /*_#I__L5*/ meltfnum[4]));;
    /*^compute */
 /*_.VARBINDMAP__V19*/ meltfptr[18] =
      (meltgc_new_mapobjects
       ((meltobject_ptr_t) (( /*!DISCR_MAP_OBJECTS */ meltfrout->tabval[3])),
	( /*_#I__L6*/ meltfnum[5])));;
    MELT_LOCATION ("warmelt-macro.melt:5598:/ quasiblock");


    /*^newclosure */
		 /*newclosure *//*_.LAMBDA___V21*/ meltfptr[20] =
      (melt_ptr_t)
      meltgc_new_closure ((meltobject_ptr_t)
			  (((melt_ptr_t) (MELT_PREDEF (DISCR_CLOSURE)))),
			  (meltroutine_ptr_t) (( /*!konst_6 */ meltfrout->
						tabval[6])), (1));
    ;
    /*^putclosedv */
    /*putclosv */
    melt_assertmsg ("putclosv checkclo",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.LAMBDA___V21*/ meltfptr[20])) ==
		    MELTOBMAG_CLOSURE);
    melt_assertmsg ("putclosv checkoff", 0 >= 0
		    && 0 <
		    melt_closure_size ((melt_ptr_t)
				       ( /*_.LAMBDA___V21*/ meltfptr[20])));
    ((meltclosure_ptr_t) /*_.LAMBDA___V21*/ meltfptr[20])->tabval[0] =
      (melt_ptr_t) ( /*_.RESCONT__V17*/ meltfptr[16]);
    ;
    /*_.HOOKFUN__V20*/ meltfptr[19] = /*_.LAMBDA___V21*/ meltfptr[20];;
    /*citerblock FOREACH_PAIR */
    {
      /* start foreach_pair meltcit1__EACHPAIR */
      for ( /*_.CURPAIRCASE__V22*/ meltfptr[21] =
	   /*_.FIRSTPAIR__V16*/ meltfptr[15];
	   melt_magic_discr ((melt_ptr_t) /*_.CURPAIRCASE__V22*/ meltfptr[21])
	   == MELTOBMAG_PAIR;
	   /*_.CURPAIRCASE__V22*/ meltfptr[21] =
	   melt_pair_tail ((melt_ptr_t) /*_.CURPAIRCASE__V22*/ meltfptr[21]))
	{
	  /*_.CURCASE__V23*/ meltfptr[22] =
	    melt_pair_head ((melt_ptr_t) /*_.CURPAIRCASE__V22*/ meltfptr[21]);




#if MELT_HAVE_DEBUG
	  MELT_LOCATION ("warmelt-macro.melt:5605:/ cppif.then");
	  /*^block */
	  /*anyblock */
	  {


	    {
	      /*^locexp */
	      /*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	      melt_dbgcounter++;
#endif
	      ;
	    }
	    ;
	    /*^checksignal */
	    MELT_CHECK_SIGNAL ();
	    ;
    /*_#MELT_NEED_DBG__L7*/ meltfnum[6] =
	      /*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	      ( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	      0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	      ;;
	    MELT_LOCATION ("warmelt-macro.melt:5605:/ cond");
	    /*cond */ if ( /*_#MELT_NEED_DBG__L7*/ meltfnum[6])	/*then */
	      {
		/*^cond.then */
		/*^block */
		/*anyblock */
		{

      /*_#THE_MELTCALLCOUNT__L8*/ meltfnum[7] =
#ifdef meltcallcount
		    meltcallcount	/* the_meltcallcount */
#else
		    0L
#endif /* meltcallcount the_meltcallcount */
		    ;;
		  MELT_LOCATION ("warmelt-macro.melt:5605:/ checksignal");
		  MELT_CHECK_SIGNAL ();
		  ;
		  /*^apply */
		  /*apply */
		  {
		    union meltparam_un argtab[5];
		    memset (&argtab, 0, sizeof (argtab));
		    /*^apply.arg */
		    argtab[0].meltbp_long =
		      /*_#THE_MELTCALLCOUNT__L8*/ meltfnum[7];
		    /*^apply.arg */
		    argtab[1].meltbp_cstring = "warmelt-macro.melt";
		    /*^apply.arg */
		    argtab[2].meltbp_long = 5605;
		    /*^apply.arg */
		    argtab[3].meltbp_cstring = "mexpand_variadic curcase";
		    /*^apply.arg */
		    argtab[4].meltbp_aptr =
		      (melt_ptr_t *) & /*_.CURCASE__V23*/ meltfptr[22];
		    /*_.MELT_DEBUG_FUN__V26*/ meltfptr[25] =
		      melt_apply ((meltclosure_ptr_t)
				  (( /*!MELT_DEBUG_FUN */ meltfrout->
				    tabval[7])),
				  (melt_ptr_t) (( /*nil */ NULL)),
				  (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_PTR ""), argtab, "",
				  (union meltparam_un *) 0);
		  }
		  ;
		  /*_.IF___V25*/ meltfptr[24] =
		    /*_.MELT_DEBUG_FUN__V26*/ meltfptr[25];;
		  /*epilog */

		  MELT_LOCATION ("warmelt-macro.melt:5605:/ clear");
		/*clear *//*_#THE_MELTCALLCOUNT__L8*/ meltfnum[7] = 0;
		  /*^clear */
		/*clear *//*_.MELT_DEBUG_FUN__V26*/ meltfptr[25] = 0;
		}
		;
	      }
	    else
	      {			/*^cond.else */

     /*_.IF___V25*/ meltfptr[24] = NULL;;
	      }
	    ;
	    MELT_LOCATION ("warmelt-macro.melt:5605:/ quasiblock");


	    /*_.PROGN___V27*/ meltfptr[25] = /*_.IF___V25*/ meltfptr[24];;
	    /*^compute */
	    /*_.IFCPP___V24*/ meltfptr[23] = /*_.PROGN___V27*/ meltfptr[25];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-macro.melt:5605:/ clear");
	      /*clear *//*_#MELT_NEED_DBG__L7*/ meltfnum[6] = 0;
	    /*^clear */
	      /*clear *//*_.IF___V25*/ meltfptr[24] = 0;
	    /*^clear */
	      /*clear *//*_.PROGN___V27*/ meltfptr[25] = 0;
	  }

#else /*MELT_HAVE_DEBUG */
	  /*^cppif.else */
	  /*_.IFCPP___V24*/ meltfptr[23] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
	  ;
	  MELT_LOCATION ("warmelt-macro.melt:5606:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
  /*_#IS_NOT_A__L9*/ meltfnum[7] =
	    !melt_is_instance_of ((melt_ptr_t)
				  ( /*_.CURCASE__V23*/ meltfptr[22]),
				  (melt_ptr_t) (( /*!CLASS_SEXPR */
						 meltfrout->tabval[0])));;
	  MELT_LOCATION ("warmelt-macro.melt:5606:/ cond");
	  /*cond */ if ( /*_#IS_NOT_A__L9*/ meltfnum[7])	/*then */
	    {
	      /*^cond.then */
	      /*^block */
	      /*anyblock */
	      {


		{
		  MELT_LOCATION ("warmelt-macro.melt:5608:/ locexp");
		  /* error_plain */ melt_error_str ((melt_ptr_t) ( /*_.LOC__V14*/ meltfptr[13]), ("(VARIADIC variadic-case...) expects a list, so cannot have non-list\
 components."), (melt_ptr_t) 0);
		}
		;
		MELT_LOCATION ("warmelt-macro.melt:5609:/ quasiblock");


    /*_.RETVAL___V1*/ meltfptr[0] = NULL;;

		{
		  MELT_LOCATION ("warmelt-macro.melt:5609:/ locexp");
		  /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
		  if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
		    melt_warn_for_no_expected_secondary_results ();
		  /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
		  ;
		}
		;
		/*^finalreturn */
		;
		/*finalret */ goto labend_rout;
		MELT_LOCATION ("warmelt-macro.melt:5607:/ quasiblock");


		/*_.PROGN___V30*/ meltfptr[29] =
		  /*_.RETURN___V29*/ meltfptr[25];;
		/*^compute */
		/*_.IF___V28*/ meltfptr[24] = /*_.PROGN___V30*/ meltfptr[29];;
		/*epilog */

		MELT_LOCATION ("warmelt-macro.melt:5606:/ clear");
	      /*clear *//*_.RETURN___V29*/ meltfptr[25] = 0;
		/*^clear */
	      /*clear *//*_.PROGN___V30*/ meltfptr[29] = 0;
	      }
	      ;
	    }
	  else
	    {			/*^cond.else */

   /*_.IF___V28*/ meltfptr[24] = NULL;;
	    }
	  ;
	  MELT_LOCATION ("warmelt-macro.melt:5610:/ quasiblock");


	  MELT_LOCATION ("warmelt-macro.melt:5611:/ getslot");
	  {
	    melt_ptr_t slot = NULL, obj = NULL;
	    obj = (melt_ptr_t) ( /*_.CURCASE__V23*/ meltfptr[22]) /*=obj*/ ;
	    melt_object_get_field (slot, obj, 1, "LOCA_LOCATION");
   /*_.CURCASELOC__V32*/ meltfptr[29] = slot;
	  };
	  ;
	  MELT_LOCATION ("warmelt-macro.melt:5612:/ getslot");
	  {
	    melt_ptr_t slot = NULL, obj = NULL;
	    obj = (melt_ptr_t) ( /*_.CURCASE__V23*/ meltfptr[22]) /*=obj*/ ;
	    melt_object_get_field (slot, obj, 2, "SEXP_CONTENTS");
   /*_.CURCASECONT__V33*/ meltfptr[32] = slot;
	  };
	  ;
  /*_.CURCASEPAIR__V34*/ meltfptr[33] =
	    (melt_list_first
	     ((melt_ptr_t) ( /*_.CURCASECONT__V33*/ meltfptr[32])));;
	  /*^compute */
  /*_.CASEFIRST__V35*/ meltfptr[34] =
	    (melt_pair_head
	     ((melt_ptr_t) ( /*_.CURCASEPAIR__V34*/ meltfptr[33])));;
	  /*^compute */
  /*_.CASEREST__V36*/ meltfptr[35] =
	    (melt_pair_tail
	     ((melt_ptr_t) ( /*_.CURCASEPAIR__V34*/ meltfptr[33])));;

#if MELT_HAVE_DEBUG
	  MELT_LOCATION ("warmelt-macro.melt:5617:/ cppif.then");
	  /*^block */
	  /*anyblock */
	  {


	    {
	      /*^locexp */
	      /*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	      melt_dbgcounter++;
#endif
	      ;
	    }
	    ;
	    /*^checksignal */
	    MELT_CHECK_SIGNAL ();
	    ;
    /*_#MELT_NEED_DBG__L10*/ meltfnum[6] =
	      /*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	      ( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	      0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	      ;;
	    MELT_LOCATION ("warmelt-macro.melt:5617:/ cond");
	    /*cond */ if ( /*_#MELT_NEED_DBG__L10*/ meltfnum[6])	/*then */
	      {
		/*^cond.then */
		/*^block */
		/*anyblock */
		{

      /*_#THE_MELTCALLCOUNT__L11*/ meltfnum[10] =
#ifdef meltcallcount
		    meltcallcount	/* the_meltcallcount */
#else
		    0L
#endif /* meltcallcount the_meltcallcount */
		    ;;
		  MELT_LOCATION ("warmelt-macro.melt:5617:/ checksignal");
		  MELT_CHECK_SIGNAL ();
		  ;
		  /*^apply */
		  /*apply */
		  {
		    union meltparam_un argtab[7];
		    memset (&argtab, 0, sizeof (argtab));
		    /*^apply.arg */
		    argtab[0].meltbp_long =
		      /*_#THE_MELTCALLCOUNT__L11*/ meltfnum[10];
		    /*^apply.arg */
		    argtab[1].meltbp_cstring = "warmelt-macro.melt";
		    /*^apply.arg */
		    argtab[2].meltbp_long = 5617;
		    /*^apply.arg */
		    argtab[3].meltbp_cstring = "mexpand_variadic casefirst=";
		    /*^apply.arg */
		    argtab[4].meltbp_aptr =
		      (melt_ptr_t *) & /*_.CASEFIRST__V35*/ meltfptr[34];
		    /*^apply.arg */
		    argtab[5].meltbp_cstring = " curcase=";
		    /*^apply.arg */
		    argtab[6].meltbp_aptr =
		      (melt_ptr_t *) & /*_.CURCASE__V23*/ meltfptr[22];
		    /*_.MELT_DEBUG_FUN__V39*/ meltfptr[38] =
		      melt_apply ((meltclosure_ptr_t)
				  (( /*!MELT_DEBUG_FUN */ meltfrout->
				    tabval[7])),
				  (melt_ptr_t) (( /*nil */ NULL)),
				  (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_PTR MELTBPARSTR_CSTRING
				   MELTBPARSTR_PTR ""), argtab, "",
				  (union meltparam_un *) 0);
		  }
		  ;
		  /*_.IF___V38*/ meltfptr[37] =
		    /*_.MELT_DEBUG_FUN__V39*/ meltfptr[38];;
		  /*epilog */

		  MELT_LOCATION ("warmelt-macro.melt:5617:/ clear");
		/*clear *//*_#THE_MELTCALLCOUNT__L11*/ meltfnum[10] = 0;
		  /*^clear */
		/*clear *//*_.MELT_DEBUG_FUN__V39*/ meltfptr[38] = 0;
		}
		;
	      }
	    else
	      {			/*^cond.else */

     /*_.IF___V38*/ meltfptr[37] = NULL;;
	      }
	    ;
	    MELT_LOCATION ("warmelt-macro.melt:5617:/ quasiblock");


	    /*_.PROGN___V40*/ meltfptr[38] = /*_.IF___V38*/ meltfptr[37];;
	    /*^compute */
	    /*_.IFCPP___V37*/ meltfptr[36] = /*_.PROGN___V40*/ meltfptr[38];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-macro.melt:5617:/ clear");
	      /*clear *//*_#MELT_NEED_DBG__L10*/ meltfnum[6] = 0;
	    /*^clear */
	      /*clear *//*_.IF___V38*/ meltfptr[37] = 0;
	    /*^clear */
	      /*clear *//*_.PROGN___V40*/ meltfptr[38] = 0;
	  }

#else /*MELT_HAVE_DEBUG */
	  /*^cppif.else */
	  /*_.IFCPP___V37*/ meltfptr[36] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
	  ;
	  MELT_LOCATION ("warmelt-macro.melt:5619:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
  /*_#__L12*/ meltfnum[10] =
	    (( /*_.CASEFIRST__V35*/ meltfptr[34]) ==
	     (( /*!konst_8_ELSE */ meltfrout->tabval[8])));;
	  MELT_LOCATION ("warmelt-macro.melt:5619:/ cond");
	  /*cond */ if ( /*_#__L12*/ meltfnum[10])	/*then */
	    {
	      /*^cond.then */
	      /*^block */
	      /*anyblock */
	      {

		MELT_LOCATION ("warmelt-macro.melt:5620:/ checksignal");
		MELT_CHECK_SIGNAL ();
		;
    /*_.PAIR_TAIL__V42*/ meltfptr[38] =
		  (melt_pair_tail
		   ((melt_ptr_t) ( /*_.CURPAIRCASE__V22*/ meltfptr[21])));;
		MELT_LOCATION ("warmelt-macro.melt:5620:/ cond");
		/*cond */ if ( /*_.PAIR_TAIL__V42*/ meltfptr[38])	/*then */
		  {
		    /*^cond.then */
		    /*^block */
		    /*anyblock */
		    {


		      {
			MELT_LOCATION ("warmelt-macro.melt:5622:/ locexp");
			/* error_plain */
			  melt_error_str ((melt_ptr_t)
					  ( /*_.CURCASELOC__V32*/
					   meltfptr[29]),
					  ("case (:ELSE ...) should be last in (VARIADIC ...)"),
					  (melt_ptr_t) 0);
		      }
		      ;

		      {
			MELT_LOCATION ("warmelt-macro.melt:5623:/ locexp");
			melt_warning_str (0,
					  (melt_ptr_t) ( /*_.LOC__V14*/
							meltfptr[13]),
					  ("This (VARIADIC ...) should have (:ELSE ...) case at last"),
					  (melt_ptr_t) 0);
		      }
		      ;
		      MELT_LOCATION ("warmelt-macro.melt:5624:/ quasiblock");


      /*_.RETVAL___V1*/ meltfptr[0] = NULL;;

		      {
			MELT_LOCATION ("warmelt-macro.melt:5624:/ locexp");
			/*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
			if (meltxresdescr_ && meltxresdescr_[0]
			    && meltxrestab_)
			  melt_warn_for_no_expected_secondary_results ();
			/* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
			;
		      }
		      ;
		      /*^finalreturn */
		      ;
		      /*finalret */ goto labend_rout;
		      MELT_LOCATION ("warmelt-macro.melt:5621:/ quasiblock");


		      /*_.PROGN___V45*/ meltfptr[44] =
			/*_.RETURN___V44*/ meltfptr[43];;
		      /*^compute */
		      /*_.IF___V43*/ meltfptr[42] =
			/*_.PROGN___V45*/ meltfptr[44];;
		      /*epilog */

		      MELT_LOCATION ("warmelt-macro.melt:5620:/ clear");
		/*clear *//*_.RETURN___V44*/ meltfptr[43] = 0;
		      /*^clear */
		/*clear *//*_.PROGN___V45*/ meltfptr[44] = 0;
		    }
		    ;
		  }
		else
		  {		/*^cond.else */

     /*_.IF___V43*/ meltfptr[42] = NULL;;
		  }
		;
		MELT_LOCATION ("warmelt-macro.melt:5625:/ quasiblock");


		MELT_LOCATION ("warmelt-macro.melt:5628:/ quasiblock");


		/*^newclosure */
		    /*newclosure *//*_.LAMBDA___V48*/ meltfptr[47] =
		  (melt_ptr_t)
		  meltgc_new_closure ((meltobject_ptr_t)
				      (((melt_ptr_t)
					(MELT_PREDEF (DISCR_CLOSURE)))),
				      (meltroutine_ptr_t) (( /*!konst_11 */
							    meltfrout->
							    tabval[11])),
				      (3));
		;
		/*^putclosedv */
		/*putclosv */
		melt_assertmsg ("putclosv checkclo",
				melt_magic_discr ((melt_ptr_t)
						  ( /*_.LAMBDA___V48*/
						   meltfptr[47])) ==
				MELTOBMAG_CLOSURE);
		melt_assertmsg ("putclosv checkoff", 0 >= 0
				&& 0 <
				melt_closure_size ((melt_ptr_t)
						   ( /*_.LAMBDA___V48*/
						    meltfptr[47])));
		((meltclosure_ptr_t) /*_.LAMBDA___V48*/ meltfptr[47])->
		  tabval[0] = (melt_ptr_t) ( /*_.MEXPANDER__V4*/ meltfptr[3]);
		;
		/*^putclosedv */
		/*putclosv */
		melt_assertmsg ("putclosv checkclo",
				melt_magic_discr ((melt_ptr_t)
						  ( /*_.LAMBDA___V48*/
						   meltfptr[47])) ==
				MELTOBMAG_CLOSURE);
		melt_assertmsg ("putclosv checkoff", 1 >= 0
				&& 1 <
				melt_closure_size ((melt_ptr_t)
						   ( /*_.LAMBDA___V48*/
						    meltfptr[47])));
		((meltclosure_ptr_t) /*_.LAMBDA___V48*/ meltfptr[47])->
		  tabval[1] = (melt_ptr_t) ( /*_.ENV__V3*/ meltfptr[2]);
		;
		/*^putclosedv */
		/*putclosv */
		melt_assertmsg ("putclosv checkclo",
				melt_magic_discr ((melt_ptr_t)
						  ( /*_.LAMBDA___V48*/
						   meltfptr[47])) ==
				MELTOBMAG_CLOSURE);
		melt_assertmsg ("putclosv checkoff", 2 >= 0
				&& 2 <
				melt_closure_size ((melt_ptr_t)
						   ( /*_.LAMBDA___V48*/
						    meltfptr[47])));
		((meltclosure_ptr_t) /*_.LAMBDA___V48*/ meltfptr[47])->
		  tabval[2] = (melt_ptr_t) ( /*_.MODCTX__V5*/ meltfptr[4]);
		;
		/*_.LAMBDA___V47*/ meltfptr[44] =
		  /*_.LAMBDA___V48*/ meltfptr[47];;
		MELT_LOCATION ("warmelt-macro.melt:5625:/ checksignal");
		MELT_CHECK_SIGNAL ();
		;
		/*^apply */
		/*apply */
		{
		  union meltparam_un argtab[2];
		  memset (&argtab, 0, sizeof (argtab));
		  /*^apply.arg */
		  argtab[0].meltbp_aptr =
		    (melt_ptr_t *) & ( /*!DISCR_MULTIPLE */ meltfrout->
				      tabval[10]);
		  /*^apply.arg */
		  argtab[1].meltbp_aptr =
		    (melt_ptr_t *) & /*_.LAMBDA___V47*/ meltfptr[44];
		  /*_.BODYTUP__V49*/ meltfptr[48] =
		    melt_apply ((meltclosure_ptr_t)
				(( /*!PAIRLIST_TO_MULTIPLE */ meltfrout->
				  tabval[9])),
				(melt_ptr_t) ( /*_.CASEREST__V36*/
					      meltfptr[35]),
				(MELTBPARSTR_PTR MELTBPARSTR_PTR ""), argtab,
				"", (union meltparam_un *) 0);
		}
		;

#if MELT_HAVE_DEBUG
		MELT_LOCATION ("warmelt-macro.melt:5630:/ cppif.then");
		/*^block */
		/*anyblock */
		{


		  {
		    /*^locexp */
		    /*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
		    melt_dbgcounter++;
#endif
		    ;
		  }
		  ;
		  /*^checksignal */
		  MELT_CHECK_SIGNAL ();
		  ;
      /*_#MELT_NEED_DBG__L13*/ meltfnum[6] =
		    /*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
		    ( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
		    0		/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
		    ;;
		  MELT_LOCATION ("warmelt-macro.melt:5630:/ cond");
		  /*cond */ if ( /*_#MELT_NEED_DBG__L13*/ meltfnum[6])	/*then */
		    {
		      /*^cond.then */
		      /*^block */
		      /*anyblock */
		      {

	/*_#THE_MELTCALLCOUNT__L14*/ meltfnum[13] =
#ifdef meltcallcount
			  meltcallcount	/* the_meltcallcount */
#else
			  0L
#endif /* meltcallcount the_meltcallcount */
			  ;;
			MELT_LOCATION
			  ("warmelt-macro.melt:5630:/ checksignal");
			MELT_CHECK_SIGNAL ();
			;
			/*^apply */
			/*apply */
			{
			  union meltparam_un argtab[5];
			  memset (&argtab, 0, sizeof (argtab));
			  /*^apply.arg */
			  argtab[0].meltbp_long =
			    /*_#THE_MELTCALLCOUNT__L14*/ meltfnum[13];
			  /*^apply.arg */
			  argtab[1].meltbp_cstring = "warmelt-macro.melt";
			  /*^apply.arg */
			  argtab[2].meltbp_long = 5630;
			  /*^apply.arg */
			  argtab[3].meltbp_cstring =
			    "mexpand_variadic else bodytup before hookfun";
			  /*^apply.arg */
			  argtab[4].meltbp_aptr =
			    (melt_ptr_t *) & /*_.BODYTUP__V49*/ meltfptr[48];
			  /*_.MELT_DEBUG_FUN__V52*/ meltfptr[51] =
			    melt_apply ((meltclosure_ptr_t)
					(( /*!MELT_DEBUG_FUN */ meltfrout->
					  tabval[7])),
					(melt_ptr_t) (( /*nil */ NULL)),
					(MELTBPARSTR_LONG MELTBPARSTR_CSTRING
					 MELTBPARSTR_LONG MELTBPARSTR_CSTRING
					 MELTBPARSTR_PTR ""), argtab, "",
					(union meltparam_un *) 0);
			}
			;
			/*_.IF___V51*/ meltfptr[50] =
			  /*_.MELT_DEBUG_FUN__V52*/ meltfptr[51];;
			/*epilog */

			MELT_LOCATION ("warmelt-macro.melt:5630:/ clear");
		  /*clear *//*_#THE_MELTCALLCOUNT__L14*/ meltfnum[13] =
			  0;
			/*^clear */
		  /*clear *//*_.MELT_DEBUG_FUN__V52*/ meltfptr[51] = 0;
		      }
		      ;
		    }
		  else
		    {		/*^cond.else */

       /*_.IF___V51*/ meltfptr[50] = NULL;;
		    }
		  ;
		  MELT_LOCATION ("warmelt-macro.melt:5630:/ quasiblock");


		  /*_.PROGN___V53*/ meltfptr[51] =
		    /*_.IF___V51*/ meltfptr[50];;
		  /*^compute */
		  /*_.IFCPP___V50*/ meltfptr[49] =
		    /*_.PROGN___V53*/ meltfptr[51];;
		  /*epilog */

		  MELT_LOCATION ("warmelt-macro.melt:5630:/ clear");
		/*clear *//*_#MELT_NEED_DBG__L13*/ meltfnum[6] = 0;
		  /*^clear */
		/*clear *//*_.IF___V51*/ meltfptr[50] = 0;
		  /*^clear */
		/*clear *//*_.PROGN___V53*/ meltfptr[51] = 0;
		}

#else /*MELT_HAVE_DEBUG */
		/*^cppif.else */
		/*_.IFCPP___V50*/ meltfptr[49] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
		;

#if MELT_HAVE_DEBUG
		MELT_LOCATION ("warmelt-macro.melt:5631:/ cppif.then");
		/*^block */
		/*anyblock */
		{


		  {
		    /*^locexp */
		    /*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
		    melt_dbgcounter++;
#endif
		    ;
		  }
		  ;
		  /*^checksignal */
		  MELT_CHECK_SIGNAL ();
		  ;
      /*_#MELT_NEED_DBG__L15*/ meltfnum[13] =
		    /*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
		    ( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
		    0		/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
		    ;;
		  MELT_LOCATION ("warmelt-macro.melt:5631:/ cond");
		  /*cond */ if ( /*_#MELT_NEED_DBG__L15*/ meltfnum[13])	/*then */
		    {
		      /*^cond.then */
		      /*^block */
		      /*anyblock */
		      {

	/*_#THE_MELTCALLCOUNT__L16*/ meltfnum[6] =
#ifdef meltcallcount
			  meltcallcount	/* the_meltcallcount */
#else
			  0L
#endif /* meltcallcount the_meltcallcount */
			  ;;
			MELT_LOCATION
			  ("warmelt-macro.melt:5631:/ checksignal");
			MELT_CHECK_SIGNAL ();
			;
			/*^apply */
			/*apply */
			{
			  union meltparam_un argtab[5];
			  memset (&argtab, 0, sizeof (argtab));
			  /*^apply.arg */
			  argtab[0].meltbp_long =
			    /*_#THE_MELTCALLCOUNT__L16*/ meltfnum[6];
			  /*^apply.arg */
			  argtab[1].meltbp_cstring = "warmelt-macro.melt";
			  /*^apply.arg */
			  argtab[2].meltbp_long = 5631;
			  /*^apply.arg */
			  argtab[3].meltbp_cstring =
			    "mexpand_variadic hookfun before";
			  /*^apply.arg */
			  argtab[4].meltbp_aptr =
			    (melt_ptr_t *) & /*_.HOOKFUN__V20*/ meltfptr[19];
			  /*_.MELT_DEBUG_FUN__V56*/ meltfptr[55] =
			    melt_apply ((meltclosure_ptr_t)
					(( /*!MELT_DEBUG_FUN */ meltfrout->
					  tabval[7])),
					(melt_ptr_t) (( /*nil */ NULL)),
					(MELTBPARSTR_LONG MELTBPARSTR_CSTRING
					 MELTBPARSTR_LONG MELTBPARSTR_CSTRING
					 MELTBPARSTR_PTR ""), argtab, "",
					(union meltparam_un *) 0);
			}
			;
			/*_.IF___V55*/ meltfptr[51] =
			  /*_.MELT_DEBUG_FUN__V56*/ meltfptr[55];;
			/*epilog */

			MELT_LOCATION ("warmelt-macro.melt:5631:/ clear");
		  /*clear *//*_#THE_MELTCALLCOUNT__L16*/ meltfnum[6] =
			  0;
			/*^clear */
		  /*clear *//*_.MELT_DEBUG_FUN__V56*/ meltfptr[55] = 0;
		      }
		      ;
		    }
		  else
		    {		/*^cond.else */

       /*_.IF___V55*/ meltfptr[51] = NULL;;
		    }
		  ;
		  MELT_LOCATION ("warmelt-macro.melt:5631:/ quasiblock");


		  /*_.PROGN___V57*/ meltfptr[55] =
		    /*_.IF___V55*/ meltfptr[51];;
		  /*^compute */
		  /*_.IFCPP___V54*/ meltfptr[50] =
		    /*_.PROGN___V57*/ meltfptr[55];;
		  /*epilog */

		  MELT_LOCATION ("warmelt-macro.melt:5631:/ clear");
		/*clear *//*_#MELT_NEED_DBG__L15*/ meltfnum[13] = 0;
		  /*^clear */
		/*clear *//*_.IF___V55*/ meltfptr[51] = 0;
		  /*^clear */
		/*clear *//*_.PROGN___V57*/ meltfptr[55] = 0;
		}

#else /*MELT_HAVE_DEBUG */
		/*^cppif.else */
		/*_.IFCPP___V54*/ meltfptr[50] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
		;

		{
		  MELT_LOCATION ("warmelt-macro.melt:5632:/ locexp");

#if MELT_HAVE_DEBUG
		  if (melt_need_debug (0))
		    melt_dbgshortbacktrace (("mexpand_variadic before calling hookfun for else"), (15));
#endif
		  ;
		}
		;
		MELT_LOCATION ("warmelt-macro.melt:5633:/ checksignal");
		MELT_CHECK_SIGNAL ();
		;
		/*^apply */
		/*apply */
		{
		  /*_.HOOKFUN__V58*/ meltfptr[51] =
		    melt_apply ((meltclosure_ptr_t)
				( /*_.HOOKFUN__V20*/ meltfptr[19]),
				(melt_ptr_t) ( /*_.BODYTUP__V49*/
					      meltfptr[48]), (""),
				(union meltparam_un *) 0, "",
				(union meltparam_un *) 0);
		}
		;
		/*_.LET___V46*/ meltfptr[43] =
		  /*_.HOOKFUN__V58*/ meltfptr[51];;

		MELT_LOCATION ("warmelt-macro.melt:5625:/ clear");
	      /*clear *//*_.LAMBDA___V47*/ meltfptr[44] = 0;
		/*^clear */
	      /*clear *//*_.BODYTUP__V49*/ meltfptr[48] = 0;
		/*^clear */
	      /*clear *//*_.IFCPP___V50*/ meltfptr[49] = 0;
		/*^clear */
	      /*clear *//*_.IFCPP___V54*/ meltfptr[50] = 0;
		/*^clear */
	      /*clear *//*_.HOOKFUN__V58*/ meltfptr[51] = 0;
		MELT_LOCATION ("warmelt-macro.melt:5619:/ quasiblock");


		/*_.PROGN___V59*/ meltfptr[55] =
		  /*_.LET___V46*/ meltfptr[43];;
		/*^compute */
		/*_.IFELSE___V41*/ meltfptr[37] =
		  /*_.PROGN___V59*/ meltfptr[55];;
		/*epilog */

		MELT_LOCATION ("warmelt-macro.melt:5619:/ clear");
	      /*clear *//*_.PAIR_TAIL__V42*/ meltfptr[38] = 0;
		/*^clear */
	      /*clear *//*_.IF___V43*/ meltfptr[42] = 0;
		/*^clear */
	      /*clear *//*_.LET___V46*/ meltfptr[43] = 0;
		/*^clear */
	      /*clear *//*_.PROGN___V59*/ meltfptr[55] = 0;
	      }
	      ;
	    }
	  else
	    {			/*^cond.else */

	      /*^block */
	      /*anyblock */
	      {

		MELT_LOCATION ("warmelt-macro.melt:5636:/ checksignal");
		MELT_CHECK_SIGNAL ();
		;
    /*_#NULL__L17*/ meltfnum[6] =
		  (( /*_.CASEFIRST__V35*/ meltfptr[34]) == NULL);;
		MELT_LOCATION ("warmelt-macro.melt:5636:/ cond");
		/*cond */ if ( /*_#NULL__L17*/ meltfnum[6])	/*then */
		  {
		    /*^cond.then */
		    /*_#OR___L18*/ meltfnum[13] =
		      /*_#NULL__L17*/ meltfnum[6];;
		  }
		else
		  {
		    MELT_LOCATION ("warmelt-macro.melt:5636:/ cond.else");

		    /*^block */
		    /*anyblock */
		    {

      /*_#IS_A__L19*/ meltfnum[18] =
			melt_is_instance_of ((melt_ptr_t)
					     ( /*_.CASEFIRST__V35*/
					      meltfptr[34]),
					     (melt_ptr_t) (( /*!CLASS_SEXPR */
							    meltfrout->
							    tabval[0])));;
		      /*^compute */
		      /*_#OR___L18*/ meltfnum[13] =
			/*_#IS_A__L19*/ meltfnum[18];;
		      /*epilog */

		      MELT_LOCATION ("warmelt-macro.melt:5636:/ clear");
		/*clear *//*_#IS_A__L19*/ meltfnum[18] = 0;
		    }
		    ;
		  }
		;
		/*^cond */
		/*cond */ if ( /*_#OR___L18*/ meltfnum[13])	/*then */
		  {
		    /*^cond.then */
		    /*^block */
		    /*anyblock */
		    {


#if MELT_HAVE_DEBUG
		      MELT_LOCATION ("warmelt-macro.melt:5638:/ cppif.then");
		      /*^block */
		      /*anyblock */
		      {


			{
			  /*^locexp */
			  /*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
			  melt_dbgcounter++;
#endif
			  ;
			}
			;
			/*^checksignal */
			MELT_CHECK_SIGNAL ();
			;
	/*_#MELT_NEED_DBG__L20*/ meltfnum[18] =
			  /*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
			  ( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
			  0	/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
			  ;;
			MELT_LOCATION ("warmelt-macro.melt:5638:/ cond");
			/*cond */ if ( /*_#MELT_NEED_DBG__L20*/ meltfnum[18])	/*then */
			  {
			    /*^cond.then */
			    /*^block */
			    /*anyblock */
			    {

	  /*_#THE_MELTCALLCOUNT__L21*/ meltfnum[20] =
#ifdef meltcallcount
				meltcallcount	/* the_meltcallcount */
#else
				0L
#endif /* meltcallcount the_meltcallcount */
				;;
			      MELT_LOCATION
				("warmelt-macro.melt:5638:/ checksignal");
			      MELT_CHECK_SIGNAL ();
			      ;
			      /*^apply */
			      /*apply */
			      {
				union meltparam_un argtab[5];
				memset (&argtab, 0, sizeof (argtab));
				/*^apply.arg */
				argtab[0].meltbp_long =
				  /*_#THE_MELTCALLCOUNT__L21*/ meltfnum[20];
				/*^apply.arg */
				argtab[1].meltbp_cstring =
				  "warmelt-macro.melt";
				/*^apply.arg */
				argtab[2].meltbp_long = 5638;
				/*^apply.arg */
				argtab[3].meltbp_cstring =
				  "mexpand_variadic casefirst=";
				/*^apply.arg */
				argtab[4].meltbp_aptr =
				  (melt_ptr_t *) & /*_.CASEFIRST__V35*/
				  meltfptr[34];
				/*_.MELT_DEBUG_FUN__V63*/ meltfptr[50] =
				  melt_apply ((meltclosure_ptr_t)
					      (( /*!MELT_DEBUG_FUN */
						meltfrout->tabval[7])),
					      (melt_ptr_t) (( /*nil */ NULL)),
					      (MELTBPARSTR_LONG
					       MELTBPARSTR_CSTRING
					       MELTBPARSTR_LONG
					       MELTBPARSTR_CSTRING
					       MELTBPARSTR_PTR ""), argtab,
					      "", (union meltparam_un *) 0);
			      }
			      ;
			      /*_.IF___V62*/ meltfptr[49] =
				/*_.MELT_DEBUG_FUN__V63*/ meltfptr[50];;
			      /*epilog */

			      MELT_LOCATION
				("warmelt-macro.melt:5638:/ clear");
		    /*clear *//*_#THE_MELTCALLCOUNT__L21*/
				meltfnum[20] = 0;
			      /*^clear */
		    /*clear *//*_.MELT_DEBUG_FUN__V63*/ meltfptr[50]
				= 0;
			    }
			    ;
			  }
			else
			  {	/*^cond.else */

	 /*_.IF___V62*/ meltfptr[49] = NULL;;
			  }
			;
			MELT_LOCATION
			  ("warmelt-macro.melt:5638:/ quasiblock");


			/*_.PROGN___V64*/ meltfptr[51] =
			  /*_.IF___V62*/ meltfptr[49];;
			/*^compute */
			/*_.IFCPP___V61*/ meltfptr[48] =
			  /*_.PROGN___V64*/ meltfptr[51];;
			/*epilog */

			MELT_LOCATION ("warmelt-macro.melt:5638:/ clear");
		  /*clear *//*_#MELT_NEED_DBG__L20*/ meltfnum[18] = 0;
			/*^clear */
		  /*clear *//*_.IF___V62*/ meltfptr[49] = 0;
			/*^clear */
		  /*clear *//*_.PROGN___V64*/ meltfptr[51] = 0;
		      }

#else /*MELT_HAVE_DEBUG */
		      /*^cppif.else */
		      /*_.IFCPP___V61*/ meltfptr[48] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
		      ;
		      MELT_LOCATION ("warmelt-macro.melt:5639:/ quasiblock");


		      /*^checksignal */
		      MELT_CHECK_SIGNAL ();
		      ;
		      /*^apply */
		      /*apply */
		      {
			union meltparam_un argtab[1];
			memset (&argtab, 0, sizeof (argtab));
			/*^apply.arg */
			argtab[0].meltbp_aptr =
			  (melt_ptr_t *) & ( /*!konst_13_TRUE */ meltfrout->
					    tabval[13]);
			/*_.ARGS__V66*/ meltfptr[42] =
			  melt_apply ((meltclosure_ptr_t)
				      (( /*!LAMBDA_ARG_BINDINGS */ meltfrout->
					tabval[12])),
				      (melt_ptr_t) ( /*_.CASEFIRST__V35*/
						    meltfptr[34]),
				      (MELTBPARSTR_PTR ""), argtab, "",
				      (union meltparam_un *) 0);
		      }
		      ;
		      MELT_LOCATION ("warmelt-macro.melt:5640:/ checksignal");
		      MELT_CHECK_SIGNAL ();
		      ;
		      /*^apply */
		      /*apply */
		      {
			/*_.NEWENV__V67*/ meltfptr[43] =
			  melt_apply ((meltclosure_ptr_t)
				      (( /*!FRESH_ENV */ meltfrout->
					tabval[14])),
				      (melt_ptr_t) ( /*_.ENV__V3*/
						    meltfptr[2]), (""),
				      (union meltparam_un *) 0, "",
				      (union meltparam_un *) 0);
		      }
		      ;

#if MELT_HAVE_DEBUG
		      MELT_LOCATION ("warmelt-macro.melt:5642:/ cppif.then");
		      /*^block */
		      /*anyblock */
		      {


			{
			  /*^locexp */
			  /*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
			  melt_dbgcounter++;
#endif
			  ;
			}
			;
			/*^checksignal */
			MELT_CHECK_SIGNAL ();
			;
	/*_#MELT_NEED_DBG__L22*/ meltfnum[20] =
			  /*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
			  ( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
			  0	/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
			  ;;
			MELT_LOCATION ("warmelt-macro.melt:5642:/ cond");
			/*cond */ if ( /*_#MELT_NEED_DBG__L22*/ meltfnum[20])	/*then */
			  {
			    /*^cond.then */
			    /*^block */
			    /*anyblock */
			    {

	  /*_#THE_MELTCALLCOUNT__L23*/ meltfnum[18] =
#ifdef meltcallcount
				meltcallcount	/* the_meltcallcount */
#else
				0L
#endif /* meltcallcount the_meltcallcount */
				;;
			      MELT_LOCATION
				("warmelt-macro.melt:5642:/ checksignal");
			      MELT_CHECK_SIGNAL ();
			      ;
			      /*^apply */
			      /*apply */
			      {
				union meltparam_un argtab[5];
				memset (&argtab, 0, sizeof (argtab));
				/*^apply.arg */
				argtab[0].meltbp_long =
				  /*_#THE_MELTCALLCOUNT__L23*/ meltfnum[18];
				/*^apply.arg */
				argtab[1].meltbp_cstring =
				  "warmelt-macro.melt";
				/*^apply.arg */
				argtab[2].meltbp_long = 5642;
				/*^apply.arg */
				argtab[3].meltbp_cstring =
				  "mexpand_variadic args";
				/*^apply.arg */
				argtab[4].meltbp_aptr =
				  (melt_ptr_t *) & /*_.ARGS__V66*/
				  meltfptr[42];
				/*_.MELT_DEBUG_FUN__V70*/ meltfptr[49] =
				  melt_apply ((meltclosure_ptr_t)
					      (( /*!MELT_DEBUG_FUN */
						meltfrout->tabval[7])),
					      (melt_ptr_t) (( /*nil */ NULL)),
					      (MELTBPARSTR_LONG
					       MELTBPARSTR_CSTRING
					       MELTBPARSTR_LONG
					       MELTBPARSTR_CSTRING
					       MELTBPARSTR_PTR ""), argtab,
					      "", (union meltparam_un *) 0);
			      }
			      ;
			      /*_.IF___V69*/ meltfptr[50] =
				/*_.MELT_DEBUG_FUN__V70*/ meltfptr[49];;
			      /*epilog */

			      MELT_LOCATION
				("warmelt-macro.melt:5642:/ clear");
		    /*clear *//*_#THE_MELTCALLCOUNT__L23*/
				meltfnum[18] = 0;
			      /*^clear */
		    /*clear *//*_.MELT_DEBUG_FUN__V70*/ meltfptr[49]
				= 0;
			    }
			    ;
			  }
			else
			  {	/*^cond.else */

	 /*_.IF___V69*/ meltfptr[50] = NULL;;
			  }
			;
			MELT_LOCATION
			  ("warmelt-macro.melt:5642:/ quasiblock");


			/*_.PROGN___V71*/ meltfptr[51] =
			  /*_.IF___V69*/ meltfptr[50];;
			/*^compute */
			/*_.IFCPP___V68*/ meltfptr[55] =
			  /*_.PROGN___V71*/ meltfptr[51];;
			/*epilog */

			MELT_LOCATION ("warmelt-macro.melt:5642:/ clear");
		  /*clear *//*_#MELT_NEED_DBG__L22*/ meltfnum[20] = 0;
			/*^clear */
		  /*clear *//*_.IF___V69*/ meltfptr[50] = 0;
			/*^clear */
		  /*clear *//*_.PROGN___V71*/ meltfptr[51] = 0;
		      }

#else /*MELT_HAVE_DEBUG */
		      /*^cppif.else */
		      /*_.IFCPP___V68*/ meltfptr[55] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
		      ;
		      /*citerblock FOREACH_IN_MULTIPLE */
		      {
			/* start foreach_in_multiple meltcit2__EACHTUP */
			long meltcit2__EACHTUP_ln =
			  melt_multiple_length ((melt_ptr_t) /*_.ARGS__V66*/
						meltfptr[42]);
			for ( /*_#FIX__L24*/ meltfnum[18] = 0;
			     ( /*_#FIX__L24*/ meltfnum[18] >= 0)
			     && ( /*_#FIX__L24*/ meltfnum[18] <
				 meltcit2__EACHTUP_ln);
	/*_#FIX__L24*/ meltfnum[18]++)
			  {
			    /*_.FBI__V72*/ meltfptr[49] =
			      melt_multiple_nth ((melt_ptr_t)
						 ( /*_.ARGS__V66*/
						  meltfptr[42]),
						 /*_#FIX__L24*/ meltfnum[18]);




#if MELT_HAVE_DEBUG
			    MELT_LOCATION
			      ("warmelt-macro.melt:5646:/ cppif.then");
			    /*^block */
			    /*anyblock */
			    {

			      /*^checksignal */
			      MELT_CHECK_SIGNAL ();
			      ;
	 /*_#IS_A__L25*/ meltfnum[20] =
				melt_is_instance_of ((melt_ptr_t)
						     ( /*_.FBI__V72*/
						      meltfptr[49]),
						     (melt_ptr_t) (( /*!CLASS_FORMAL_BINDING */ meltfrout->tabval[15])));;
			      MELT_LOCATION
				("warmelt-macro.melt:5646:/ cond");
			      /*cond */ if ( /*_#IS_A__L25*/ meltfnum[20])	/*then */
				{
				  /*^cond.then */
				  /*_.IFELSE___V74*/ meltfptr[51] =
				    ( /*nil */ NULL);;
				}
			      else
				{
				  MELT_LOCATION
				    ("warmelt-macro.melt:5646:/ cond.else");

				  /*^block */
				  /*anyblock */
				  {




				    {
				      /*^locexp */
				      melt_assert_failed (("check fbi"),
							  ("warmelt-macro.melt")
							  ?
							  ("warmelt-macro.melt")
							  : __FILE__,
							  (5646) ? (5646) :
							  __LINE__,
							  __FUNCTION__);
				      ;
				    }
				    ;
		     /*clear *//*_.IFELSE___V74*/ meltfptr[51]
				      = 0;
				    /*epilog */
				  }
				  ;
				}
			      ;
			      /*^compute */
			      /*_.IFCPP___V73*/ meltfptr[50] =
				/*_.IFELSE___V74*/ meltfptr[51];;
			      /*epilog */

			      MELT_LOCATION
				("warmelt-macro.melt:5646:/ clear");
		   /*clear *//*_#IS_A__L25*/ meltfnum[20] = 0;
			      /*^clear */
		   /*clear *//*_.IFELSE___V74*/ meltfptr[51] = 0;
			    }

#else /*MELT_HAVE_DEBUG */
			    /*^cppif.else */
			    /*_.IFCPP___V73*/ meltfptr[50] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
			    ;
			    MELT_LOCATION
			      ("warmelt-macro.melt:5647:/ quasiblock");


			    /*^cond */
			    /*cond */ if (
					   /*ifisa */
					   melt_is_instance_of ((melt_ptr_t)
								( /*_.FBI__V72*/ meltfptr[49]),
								(melt_ptr_t) (( /*!CLASS_ANY_BINDING */ meltfrout->tabval[16])))
			      )	/*then */
			      {
				/*^cond.then */
				/*^getslot */
				{
				  melt_ptr_t slot = NULL, obj = NULL;
				  obj =
				    (melt_ptr_t) ( /*_.FBI__V72*/
						  meltfptr[49]) /*=obj*/ ;
				  melt_object_get_field (slot, obj, 0,
							 "BINDER");
	 /*_.FBISYMB__V76*/ meltfptr[75] = slot;
				};
				;
			      }
			    else
			      {	/*^cond.else */

	/*_.FBISYMB__V76*/ meltfptr[75] = NULL;;
			      }
			    ;
			    MELT_LOCATION
			      ("warmelt-macro.melt:5649:/ checksignal");
			    MELT_CHECK_SIGNAL ();
			    ;
			    /*^apply */
			    /*apply */
			    {
			      union meltparam_un argtab[2];
			      memset (&argtab, 0, sizeof (argtab));
			      /*^apply.arg */
			      argtab[0].meltbp_aptr =
				(melt_ptr_t *) & /*_.NEWENV__V67*/
				meltfptr[43];
			      /*^apply.arg */
			      argtab[1].meltbp_aptr =
				(melt_ptr_t *) & /*_.LOC__V14*/ meltfptr[13];
			      /*_.WARN_IF_REDEFINED__V77*/ meltfptr[76] =
				melt_apply ((meltclosure_ptr_t)
					    (( /*!WARN_IF_REDEFINED */
					      meltfrout->tabval[17])),
					    (melt_ptr_t) ( /*_.FBISYMB__V76*/
							  meltfptr[75]),
					    (MELTBPARSTR_PTR MELTBPARSTR_PTR
					     ""), argtab, "",
					    (union meltparam_un *) 0);
			    }
			    ;
			    MELT_LOCATION
			      ("warmelt-macro.melt:5650:/ checksignal");
			    MELT_CHECK_SIGNAL ();
			    ;
       /*_.MAPOBJECT_GET__V78*/ meltfptr[77] =
			      /*mapobject_get */
			      melt_get_mapobjects ((meltmapobjects_ptr_t)
						   ( /*_.VARBINDMAP__V19*/
						    meltfptr[18]),
						   (meltobject_ptr_t) ( /*_.FBISYMB__V76*/ meltfptr[75]));;
			    MELT_LOCATION ("warmelt-macro.melt:5650:/ cond");
			    /*cond */ if ( /*_.MAPOBJECT_GET__V78*/ meltfptr[77])	/*then */
			      {
				/*^cond.then */
				/*^block */
				/*anyblock */
				{

				  MELT_LOCATION
				    ("warmelt-macro.melt:5652:/ cond");
				  /*cond */ if (
						 /*ifisa */
						 melt_is_instance_of ((melt_ptr_t) ( /*_.FBISYMB__V76*/ meltfptr[75]),
								      (melt_ptr_t) (( /*!CLASS_NAMED */ meltfrout->tabval[18])))
				    )	/*then */
				    {
				      /*^cond.then */
				      /*^getslot */
				      {
					melt_ptr_t slot = NULL, obj = NULL;
					obj =
					  (melt_ptr_t) ( /*_.FBISYMB__V76*/
							meltfptr[75]) /*=obj*/
					  ;
					melt_object_get_field (slot, obj, 1,
							       "NAMED_NAME");
	   /*_.NAMED_NAME__V79*/ meltfptr[78] =
					  slot;
				      };
				      ;
				    }
				  else
				    {	/*^cond.else */

	  /*_.NAMED_NAME__V79*/ meltfptr[78] =
					NULL;;
				    }
				  ;

				  {
				    MELT_LOCATION
				      ("warmelt-macro.melt:5652:/ locexp");
				    melt_error_str ((melt_ptr_t)
						    ( /*_.CURCASELOC__V32*/
						     meltfptr[29]),
						    ("formals should all be distinct in (VARIADIC ...) "),
						    (melt_ptr_t) ( /*_.NAMED_NAME__V79*/ meltfptr[78]));
				  }
				  ;
				  /*epilog */

				  MELT_LOCATION
				    ("warmelt-macro.melt:5650:/ clear");
		   /*clear *//*_.NAMED_NAME__V79*/ meltfptr[78]
				    = 0;
				}
				;
			      }	/*noelse */
			    ;

			    {
			      MELT_LOCATION
				("warmelt-macro.melt:5654:/ locexp");
			      meltgc_put_mapobjects ((meltmapobjects_ptr_t)
						     ( /*_.VARBINDMAP__V19*/
						      meltfptr[18]),
						     (meltobject_ptr_t) ( /*_.FBISYMB__V76*/ meltfptr[75]),
						     (melt_ptr_t) ( /*_.FBI__V72*/ meltfptr[49]));
			    }
			    ;
			    MELT_LOCATION
			      ("warmelt-macro.melt:5655:/ checksignal");
			    MELT_CHECK_SIGNAL ();
			    ;
			    /*^apply */
			    /*apply */
			    {
			      union meltparam_un argtab[1];
			      memset (&argtab, 0, sizeof (argtab));
			      /*^apply.arg */
			      argtab[0].meltbp_aptr =
				(melt_ptr_t *) & /*_.FBI__V72*/ meltfptr[49];
			      /*_.PUT_ENV__V80*/ meltfptr[78] =
				melt_apply ((meltclosure_ptr_t)
					    (( /*!PUT_ENV */ meltfrout->
					      tabval[19])),
					    (melt_ptr_t) ( /*_.NEWENV__V67*/
							  meltfptr[43]),
					    (MELTBPARSTR_PTR ""), argtab, "",
					    (union meltparam_un *) 0);
			    }
			    ;
			    /*_.LET___V75*/ meltfptr[51] =
			      /*_.PUT_ENV__V80*/ meltfptr[78];;

			    MELT_LOCATION ("warmelt-macro.melt:5647:/ clear");
		 /*clear *//*_.FBISYMB__V76*/ meltfptr[75] = 0;
			    /*^clear */
		 /*clear *//*_.WARN_IF_REDEFINED__V77*/
			      meltfptr[76] = 0;
			    /*^clear */
		 /*clear *//*_.MAPOBJECT_GET__V78*/ meltfptr[77] =
			      0;
			    /*^clear */
		 /*clear *//*_.PUT_ENV__V80*/ meltfptr[78] = 0;
			    if ( /*_#FIX__L24*/ meltfnum[18] < 0)
			      break;
			  }	/* end  foreach_in_multiple meltcit2__EACHTUP */

			/*citerepilog */

			MELT_LOCATION ("warmelt-macro.melt:5643:/ clear");
		 /*clear *//*_.FBI__V72*/ meltfptr[49] = 0;
			/*^clear */
		 /*clear *//*_#FIX__L24*/ meltfnum[18] = 0;
			/*^clear */
		 /*clear *//*_.IFCPP___V73*/ meltfptr[50] = 0;
			/*^clear */
		 /*clear *//*_.LET___V75*/ meltfptr[51] = 0;
		      }		/*endciterblock FOREACH_IN_MULTIPLE */
		      ;
		      MELT_LOCATION ("warmelt-macro.melt:5657:/ quasiblock");


		      MELT_LOCATION ("warmelt-macro.melt:5659:/ quasiblock");


		      /*^newclosure */
		      /*newclosure *//*_.LAMBDA___V83*/ meltfptr[77] =
			(melt_ptr_t)
			meltgc_new_closure ((meltobject_ptr_t)
					    (((melt_ptr_t)
					      (MELT_PREDEF (DISCR_CLOSURE)))),
					    (meltroutine_ptr_t) (( /*!konst_20 */ meltfrout->tabval[20])), (3));
		      ;
		      /*^putclosedv */
		      /*putclosv */
		      melt_assertmsg ("putclosv checkclo",
				      melt_magic_discr ((melt_ptr_t)
							( /*_.LAMBDA___V83*/
							 meltfptr[77])) ==
				      MELTOBMAG_CLOSURE);
		      melt_assertmsg ("putclosv checkoff", 0 >= 0
				      && 0 <
				      melt_closure_size ((melt_ptr_t)
							 ( /*_.LAMBDA___V83*/
							  meltfptr[77])));
		      ((meltclosure_ptr_t) /*_.LAMBDA___V83*/ meltfptr[77])->
			tabval[0] =
			(melt_ptr_t) ( /*_.MEXPANDER__V4*/ meltfptr[3]);
		      ;
		      /*^putclosedv */
		      /*putclosv */
		      melt_assertmsg ("putclosv checkclo",
				      melt_magic_discr ((melt_ptr_t)
							( /*_.LAMBDA___V83*/
							 meltfptr[77])) ==
				      MELTOBMAG_CLOSURE);
		      melt_assertmsg ("putclosv checkoff", 1 >= 0
				      && 1 <
				      melt_closure_size ((melt_ptr_t)
							 ( /*_.LAMBDA___V83*/
							  meltfptr[77])));
		      ((meltclosure_ptr_t) /*_.LAMBDA___V83*/ meltfptr[77])->
			tabval[1] =
			(melt_ptr_t) ( /*_.NEWENV__V67*/ meltfptr[43]);
		      ;
		      /*^putclosedv */
		      /*putclosv */
		      melt_assertmsg ("putclosv checkclo",
				      melt_magic_discr ((melt_ptr_t)
							( /*_.LAMBDA___V83*/
							 meltfptr[77])) ==
				      MELTOBMAG_CLOSURE);
		      melt_assertmsg ("putclosv checkoff", 2 >= 0
				      && 2 <
				      melt_closure_size ((melt_ptr_t)
							 ( /*_.LAMBDA___V83*/
							  meltfptr[77])));
		      ((meltclosure_ptr_t) /*_.LAMBDA___V83*/ meltfptr[77])->
			tabval[2] =
			(melt_ptr_t) ( /*_.MODCTX__V5*/ meltfptr[4]);
		      ;
		      /*_.LAMBDA___V82*/ meltfptr[76] =
			/*_.LAMBDA___V83*/ meltfptr[77];;
		      MELT_LOCATION ("warmelt-macro.melt:5657:/ checksignal");
		      MELT_CHECK_SIGNAL ();
		      ;
		      /*^apply */
		      /*apply */
		      {
			union meltparam_un argtab[2];
			memset (&argtab, 0, sizeof (argtab));
			/*^apply.arg */
			argtab[0].meltbp_aptr =
			  (melt_ptr_t *) & ( /*!DISCR_MULTIPLE */ meltfrout->
					    tabval[10]);
			/*^apply.arg */
			argtab[1].meltbp_aptr =
			  (melt_ptr_t *) & /*_.LAMBDA___V82*/ meltfptr[76];
			/*_.BODYTUP__V84*/ meltfptr[78] =
			  melt_apply ((meltclosure_ptr_t)
				      (( /*!PAIRLIST_TO_MULTIPLE */
					meltfrout->tabval[9])),
				      (melt_ptr_t) ( /*_.CASEREST__V36*/
						    meltfptr[35]),
				      (MELTBPARSTR_PTR MELTBPARSTR_PTR ""),
				      argtab, "", (union meltparam_un *) 0);
		      }
		      ;
		      MELT_LOCATION ("warmelt-macro.melt:5660:/ checksignal");
		      MELT_CHECK_SIGNAL ();
		      ;
		      /*^quasiblock */


		      /*^rawallocobj */
		      /*rawallocobj */
		      {
			melt_ptr_t newobj = 0;
			melt_raw_object_create (newobj,
						(melt_ptr_t) (( /*!CLASS_SOURCE_IFVARIADIC */ meltfrout->tabval[21])), (5), "CLASS_SOURCE_IFVARIADIC");
       /*_.INST__V86*/ meltfptr[85] =
			  newobj;
		      };
		      ;
		      /*^putslot */
		      /*putslot */
		      melt_assertmsg ("putslot checkobj @LOCA_LOCATION",
				      melt_magic_discr ((melt_ptr_t)
							( /*_.INST__V86*/
							 meltfptr[85])) ==
				      MELTOBMAG_OBJECT);
		      melt_putfield_object (( /*_.INST__V86*/ meltfptr[85]),
					    (1),
					    ( /*_.LOC__V14*/ meltfptr[13]),
					    "LOCA_LOCATION");
		      ;
		      /*^putslot */
		      /*putslot */
		      melt_assertmsg ("putslot checkobj @SIFVARIADIC_ARGBIND",
				      melt_magic_discr ((melt_ptr_t)
							( /*_.INST__V86*/
							 meltfptr[85])) ==
				      MELTOBMAG_OBJECT);
		      melt_putfield_object (( /*_.INST__V86*/ meltfptr[85]),
					    (2),
					    ( /*_.ARGS__V66*/ meltfptr[42]),
					    "SIFVARIADIC_ARGBIND");
		      ;
		      /*^putslot */
		      /*putslot */
		      melt_assertmsg ("putslot checkobj @SIFVARIADIC_THEN",
				      melt_magic_discr ((melt_ptr_t)
							( /*_.INST__V86*/
							 meltfptr[85])) ==
				      MELTOBMAG_OBJECT);
		      melt_putfield_object (( /*_.INST__V86*/ meltfptr[85]),
					    (3),
					    ( /*_.BODYTUP__V84*/
					     meltfptr[78]),
					    "SIFVARIADIC_THEN");
		      ;
		      /*^putslot */
		      /*putslot */
		      melt_assertmsg ("putslot checkobj @SIFVARIADIC_ELSE",
				      melt_magic_discr ((melt_ptr_t)
							( /*_.INST__V86*/
							 meltfptr[85])) ==
				      MELTOBMAG_OBJECT);
		      melt_putfield_object (( /*_.INST__V86*/ meltfptr[85]),
					    (4), (( /*nil */ NULL)),
					    "SIFVARIADIC_ELSE");
		      ;
		      /*^touchobj */

		      melt_dbgtrace_written_object ( /*_.INST__V86*/
						    meltfptr[85],
						    "newly made instance");
		      ;
		      /*_.SIFVARIADIC__V85*/ meltfptr[84] =
			/*_.INST__V86*/ meltfptr[85];;

#if MELT_HAVE_DEBUG
		      MELT_LOCATION ("warmelt-macro.melt:5666:/ cppif.then");
		      /*^block */
		      /*anyblock */
		      {


			{
			  /*^locexp */
			  /*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
			  melt_dbgcounter++;
#endif
			  ;
			}
			;
			/*^checksignal */
			MELT_CHECK_SIGNAL ();
			;
	/*_#MELT_NEED_DBG__L26*/ meltfnum[20] =
			  /*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
			  ( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
			  0	/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
			  ;;
			MELT_LOCATION ("warmelt-macro.melt:5666:/ cond");
			/*cond */ if ( /*_#MELT_NEED_DBG__L26*/ meltfnum[20])	/*then */
			  {
			    /*^cond.then */
			    /*^block */
			    /*anyblock */
			    {

	  /*_#THE_MELTCALLCOUNT__L27*/ meltfnum[26] =
#ifdef meltcallcount
				meltcallcount	/* the_meltcallcount */
#else
				0L
#endif /* meltcallcount the_meltcallcount */
				;;
			      MELT_LOCATION
				("warmelt-macro.melt:5666:/ checksignal");
			      MELT_CHECK_SIGNAL ();
			      ;
			      /*^apply */
			      /*apply */
			      {
				union meltparam_un argtab[5];
				memset (&argtab, 0, sizeof (argtab));
				/*^apply.arg */
				argtab[0].meltbp_long =
				  /*_#THE_MELTCALLCOUNT__L27*/ meltfnum[26];
				/*^apply.arg */
				argtab[1].meltbp_cstring =
				  "warmelt-macro.melt";
				/*^apply.arg */
				argtab[2].meltbp_long = 5666;
				/*^apply.arg */
				argtab[3].meltbp_cstring =
				  "mexpand_variadic bodytup";
				/*^apply.arg */
				argtab[4].meltbp_aptr =
				  (melt_ptr_t *) & /*_.BODYTUP__V84*/
				  meltfptr[78];
				/*_.MELT_DEBUG_FUN__V89*/ meltfptr[88] =
				  melt_apply ((meltclosure_ptr_t)
					      (( /*!MELT_DEBUG_FUN */
						meltfrout->tabval[7])),
					      (melt_ptr_t) (( /*nil */ NULL)),
					      (MELTBPARSTR_LONG
					       MELTBPARSTR_CSTRING
					       MELTBPARSTR_LONG
					       MELTBPARSTR_CSTRING
					       MELTBPARSTR_PTR ""), argtab,
					      "", (union meltparam_un *) 0);
			      }
			      ;
			      /*_.IF___V88*/ meltfptr[87] =
				/*_.MELT_DEBUG_FUN__V89*/ meltfptr[88];;
			      /*epilog */

			      MELT_LOCATION
				("warmelt-macro.melt:5666:/ clear");
		    /*clear *//*_#THE_MELTCALLCOUNT__L27*/
				meltfnum[26] = 0;
			      /*^clear */
		    /*clear *//*_.MELT_DEBUG_FUN__V89*/ meltfptr[88]
				= 0;
			    }
			    ;
			  }
			else
			  {	/*^cond.else */

	 /*_.IF___V88*/ meltfptr[87] = NULL;;
			  }
			;
			MELT_LOCATION
			  ("warmelt-macro.melt:5666:/ quasiblock");


			/*_.PROGN___V90*/ meltfptr[88] =
			  /*_.IF___V88*/ meltfptr[87];;
			/*^compute */
			/*_.IFCPP___V87*/ meltfptr[86] =
			  /*_.PROGN___V90*/ meltfptr[88];;
			/*epilog */

			MELT_LOCATION ("warmelt-macro.melt:5666:/ clear");
		  /*clear *//*_#MELT_NEED_DBG__L26*/ meltfnum[20] = 0;
			/*^clear */
		  /*clear *//*_.IF___V88*/ meltfptr[87] = 0;
			/*^clear */
		  /*clear *//*_.PROGN___V90*/ meltfptr[88] = 0;
		      }

#else /*MELT_HAVE_DEBUG */
		      /*^cppif.else */
		      /*_.IFCPP___V87*/ meltfptr[86] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
		      ;

		      {
			MELT_LOCATION ("warmelt-macro.melt:5667:/ locexp");

#if MELT_HAVE_DEBUG
			if (melt_need_debug (0))
			  melt_dbgshortbacktrace (("mexpand_variadic before calling hookfun for casefirst"), (15));
#endif
			;
		      }
		      ;
		      MELT_LOCATION ("warmelt-macro.melt:5668:/ checksignal");
		      MELT_CHECK_SIGNAL ();
		      ;
		      /*^apply */
		      /*apply */
		      {
			/*_.HOOKFUN__V91*/ meltfptr[87] =
			  melt_apply ((meltclosure_ptr_t)
				      ( /*_.HOOKFUN__V20*/ meltfptr[19]),
				      (melt_ptr_t) ( /*_.SIFVARIADIC__V85*/
						    meltfptr[84]), (""),
				      (union meltparam_un *) 0, "",
				      (union meltparam_un *) 0);
		      }
		      ;
		      MELT_LOCATION ("warmelt-macro.melt:5670:/ quasiblock");


		      /*^newclosure */
		      /*newclosure *//*_.LAMBDA___V93*/ meltfptr[92] =
			(melt_ptr_t)
			meltgc_new_closure ((meltobject_ptr_t)
					    (((melt_ptr_t)
					      (MELT_PREDEF (DISCR_CLOSURE)))),
					    (meltroutine_ptr_t) (( /*!konst_24 */ meltfrout->tabval[24])), (1));
		      ;
		      /*^putclosedv */
		      /*putclosv */
		      melt_assertmsg ("putclosv checkclo",
				      melt_magic_discr ((melt_ptr_t)
							( /*_.LAMBDA___V93*/
							 meltfptr[92])) ==
				      MELTOBMAG_CLOSURE);
		      melt_assertmsg ("putclosv checkoff", 0 >= 0
				      && 0 <
				      melt_closure_size ((melt_ptr_t)
							 ( /*_.LAMBDA___V93*/
							  meltfptr[92])));
		      ((meltclosure_ptr_t) /*_.LAMBDA___V93*/ meltfptr[92])->
			tabval[0] =
			(melt_ptr_t) ( /*_.SIFVARIADIC__V85*/ meltfptr[84]);
		      ;
		      /*_.LAMBDA___V92*/ meltfptr[88] =
			/*_.LAMBDA___V93*/ meltfptr[92];;
		      MELT_LOCATION ("warmelt-macro.melt:5669:/ compute");
		      /*_.HOOKFUN__V20*/ meltfptr[19] =
			/*_.SETQ___V94*/ meltfptr[93] =
			/*_.LAMBDA___V92*/ meltfptr[88];;
		      /*_.LET___V81*/ meltfptr[75] =
			/*_.SETQ___V94*/ meltfptr[93];;

		      MELT_LOCATION ("warmelt-macro.melt:5657:/ clear");
		/*clear *//*_.LAMBDA___V82*/ meltfptr[76] = 0;
		      /*^clear */
		/*clear *//*_.BODYTUP__V84*/ meltfptr[78] = 0;
		      /*^clear */
		/*clear *//*_.SIFVARIADIC__V85*/ meltfptr[84] = 0;
		      /*^clear */
		/*clear *//*_.IFCPP___V87*/ meltfptr[86] = 0;
		      /*^clear */
		/*clear *//*_.HOOKFUN__V91*/ meltfptr[87] = 0;
		      /*^clear */
		/*clear *//*_.LAMBDA___V92*/ meltfptr[88] = 0;
		      /*^clear */
		/*clear *//*_.SETQ___V94*/ meltfptr[93] = 0;
		      /*_.LET___V65*/ meltfptr[38] =
			/*_.LET___V81*/ meltfptr[75];;

		      MELT_LOCATION ("warmelt-macro.melt:5639:/ clear");
		/*clear *//*_.ARGS__V66*/ meltfptr[42] = 0;
		      /*^clear */
		/*clear *//*_.NEWENV__V67*/ meltfptr[43] = 0;
		      /*^clear */
		/*clear *//*_.IFCPP___V68*/ meltfptr[55] = 0;
		      /*^clear */
		/*clear *//*_.LET___V81*/ meltfptr[75] = 0;
		      MELT_LOCATION ("warmelt-macro.melt:5636:/ quasiblock");


		      /*_.PROGN___V95*/ meltfptr[76] =
			/*_.LET___V65*/ meltfptr[38];;
		      /*^compute */
		      /*_.IFELSE___V60*/ meltfptr[44] =
			/*_.PROGN___V95*/ meltfptr[76];;
		      /*epilog */

		      MELT_LOCATION ("warmelt-macro.melt:5636:/ clear");
		/*clear *//*_.IFCPP___V61*/ meltfptr[48] = 0;
		      /*^clear */
		/*clear *//*_.LET___V65*/ meltfptr[38] = 0;
		      /*^clear */
		/*clear *//*_.PROGN___V95*/ meltfptr[76] = 0;
		    }
		    ;
		  }
		else
		  {		/*^cond.else */

		    /*^block */
		    /*anyblock */
		    {


#if MELT_HAVE_DEBUG
		      MELT_LOCATION ("warmelt-macro.melt:5680:/ cppif.then");
		      /*^block */
		      /*anyblock */
		      {


			{
			  /*^locexp */
			  /*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
			  melt_dbgcounter++;
#endif
			  ;
			}
			;
			/*^checksignal */
			MELT_CHECK_SIGNAL ();
			;
	/*_#MELT_NEED_DBG__L28*/ meltfnum[26] =
			  /*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
			  ( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
			  0	/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
			  ;;
			MELT_LOCATION ("warmelt-macro.melt:5680:/ cond");
			/*cond */ if ( /*_#MELT_NEED_DBG__L28*/ meltfnum[26])	/*then */
			  {
			    /*^cond.then */
			    /*^block */
			    /*anyblock */
			    {

	  /*_#THE_MELTCALLCOUNT__L29*/ meltfnum[20] =
#ifdef meltcallcount
				meltcallcount	/* the_meltcallcount */
#else
				0L
#endif /* meltcallcount the_meltcallcount */
				;;
			      MELT_LOCATION
				("warmelt-macro.melt:5680:/ checksignal");
			      MELT_CHECK_SIGNAL ();
			      ;
			      /*^apply */
			      /*apply */
			      {
				union meltparam_un argtab[5];
				memset (&argtab, 0, sizeof (argtab));
				/*^apply.arg */
				argtab[0].meltbp_long =
				  /*_#THE_MELTCALLCOUNT__L29*/ meltfnum[20];
				/*^apply.arg */
				argtab[1].meltbp_cstring =
				  "warmelt-macro.melt";
				/*^apply.arg */
				argtab[2].meltbp_long = 5680;
				/*^apply.arg */
				argtab[3].meltbp_cstring =
				  "mexpand_variadic invalid casefirst";
				/*^apply.arg */
				argtab[4].meltbp_aptr =
				  (melt_ptr_t *) & /*_.CASEFIRST__V35*/
				  meltfptr[34];
				/*_.MELT_DEBUG_FUN__V98*/ meltfptr[86] =
				  melt_apply ((meltclosure_ptr_t)
					      (( /*!MELT_DEBUG_FUN */
						meltfrout->tabval[7])),
					      (melt_ptr_t) (( /*nil */ NULL)),
					      (MELTBPARSTR_LONG
					       MELTBPARSTR_CSTRING
					       MELTBPARSTR_LONG
					       MELTBPARSTR_CSTRING
					       MELTBPARSTR_PTR ""), argtab,
					      "", (union meltparam_un *) 0);
			      }
			      ;
			      /*_.IF___V97*/ meltfptr[84] =
				/*_.MELT_DEBUG_FUN__V98*/ meltfptr[86];;
			      /*epilog */

			      MELT_LOCATION
				("warmelt-macro.melt:5680:/ clear");
		    /*clear *//*_#THE_MELTCALLCOUNT__L29*/
				meltfnum[20] = 0;
			      /*^clear */
		    /*clear *//*_.MELT_DEBUG_FUN__V98*/ meltfptr[86]
				= 0;
			    }
			    ;
			  }
			else
			  {	/*^cond.else */

	 /*_.IF___V97*/ meltfptr[84] = NULL;;
			  }
			;
			MELT_LOCATION
			  ("warmelt-macro.melt:5680:/ quasiblock");


			/*_.PROGN___V99*/ meltfptr[87] =
			  /*_.IF___V97*/ meltfptr[84];;
			/*^compute */
			/*_.IFCPP___V96*/ meltfptr[78] =
			  /*_.PROGN___V99*/ meltfptr[87];;
			/*epilog */

			MELT_LOCATION ("warmelt-macro.melt:5680:/ clear");
		  /*clear *//*_#MELT_NEED_DBG__L28*/ meltfnum[26] = 0;
			/*^clear */
		  /*clear *//*_.IF___V97*/ meltfptr[84] = 0;
			/*^clear */
		  /*clear *//*_.PROGN___V99*/ meltfptr[87] = 0;
		      }

#else /*MELT_HAVE_DEBUG */
		      /*^cppif.else */
		      /*_.IFCPP___V96*/ meltfptr[78] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
		      ;

		      {
			MELT_LOCATION ("warmelt-macro.melt:5681:/ locexp");
			/* error_plain */
			  melt_error_str ((melt_ptr_t)
					  ( /*_.CURCASELOC__V32*/
					   meltfptr[29]),
					  ("invalid case in (VARIADIC ...), should start with formals"),
					  (melt_ptr_t) 0);
		      }
		      ;
		      MELT_LOCATION ("warmelt-macro.melt:5682:/ quasiblock");


      /*_.RETVAL___V1*/ meltfptr[0] = NULL;;

		      {
			MELT_LOCATION ("warmelt-macro.melt:5682:/ locexp");
			/*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
			if (meltxresdescr_ && meltxresdescr_[0]
			    && meltxrestab_)
			  melt_warn_for_no_expected_secondary_results ();
			/* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
			;
		      }
		      ;
		      /*^finalreturn */
		      ;
		      /*finalret */ goto labend_rout;
		      MELT_LOCATION ("warmelt-macro.melt:5679:/ quasiblock");


		      /*_.PROGN___V101*/ meltfptr[93] =
			/*_.RETURN___V100*/ meltfptr[88];;
		      /*^compute */
		      /*_.IFELSE___V60*/ meltfptr[44] =
			/*_.PROGN___V101*/ meltfptr[93];;
		      /*epilog */

		      MELT_LOCATION ("warmelt-macro.melt:5636:/ clear");
		/*clear *//*_.IFCPP___V96*/ meltfptr[78] = 0;
		      /*^clear */
		/*clear *//*_.RETURN___V100*/ meltfptr[88] = 0;
		      /*^clear */
		/*clear *//*_.PROGN___V101*/ meltfptr[93] = 0;
		    }
		    ;
		  }
		;
		/*_.IFELSE___V41*/ meltfptr[37] =
		  /*_.IFELSE___V60*/ meltfptr[44];;
		/*epilog */

		MELT_LOCATION ("warmelt-macro.melt:5619:/ clear");
	      /*clear *//*_#NULL__L17*/ meltfnum[6] = 0;
		/*^clear */
	      /*clear *//*_#OR___L18*/ meltfnum[13] = 0;
		/*^clear */
	      /*clear *//*_.IFELSE___V60*/ meltfptr[44] = 0;
	      }
	      ;
	    }
	  ;
	  /*_.LET___V31*/ meltfptr[25] = /*_.IFELSE___V41*/ meltfptr[37];;

	  MELT_LOCATION ("warmelt-macro.melt:5610:/ clear");
	    /*clear *//*_.CURCASELOC__V32*/ meltfptr[29] = 0;
	  /*^clear */
	    /*clear *//*_.CURCASECONT__V33*/ meltfptr[32] = 0;
	  /*^clear */
	    /*clear *//*_.CURCASEPAIR__V34*/ meltfptr[33] = 0;
	  /*^clear */
	    /*clear *//*_.CASEFIRST__V35*/ meltfptr[34] = 0;
	  /*^clear */
	    /*clear *//*_.CASEREST__V36*/ meltfptr[35] = 0;
	  /*^clear */
	    /*clear *//*_.IFCPP___V37*/ meltfptr[36] = 0;
	  /*^clear */
	    /*clear *//*_#__L12*/ meltfnum[10] = 0;
	  /*^clear */
	    /*clear *//*_.IFELSE___V41*/ meltfptr[37] = 0;

#if MELT_HAVE_DEBUG
	  MELT_LOCATION ("warmelt-macro.melt:5686:/ cppif.then");
	  /*^block */
	  /*anyblock */
	  {


	    {
	      /*^locexp */
	      /*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	      melt_dbgcounter++;
#endif
	      ;
	    }
	    ;
	    /*^checksignal */
	    MELT_CHECK_SIGNAL ();
	    ;
    /*_#MELT_NEED_DBG__L30*/ meltfnum[20] =
	      /*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	      ( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	      0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	      ;;
	    MELT_LOCATION ("warmelt-macro.melt:5686:/ cond");
	    /*cond */ if ( /*_#MELT_NEED_DBG__L30*/ meltfnum[20])	/*then */
	      {
		/*^cond.then */
		/*^block */
		/*anyblock */
		{

      /*_#THE_MELTCALLCOUNT__L31*/ meltfnum[26] =
#ifdef meltcallcount
		    meltcallcount	/* the_meltcallcount */
#else
		    0L
#endif /* meltcallcount the_meltcallcount */
		    ;;
		  MELT_LOCATION ("warmelt-macro.melt:5686:/ checksignal");
		  MELT_CHECK_SIGNAL ();
		  ;
		  /*^apply */
		  /*apply */
		  {
		    union meltparam_un argtab[5];
		    memset (&argtab, 0, sizeof (argtab));
		    /*^apply.arg */
		    argtab[0].meltbp_long =
		      /*_#THE_MELTCALLCOUNT__L31*/ meltfnum[26];
		    /*^apply.arg */
		    argtab[1].meltbp_cstring = "warmelt-macro.melt";
		    /*^apply.arg */
		    argtab[2].meltbp_long = 5686;
		    /*^apply.arg */
		    argtab[3].meltbp_cstring =
		      "mexpand_variadic done curcase";
		    /*^apply.arg */
		    argtab[4].meltbp_aptr =
		      (melt_ptr_t *) & /*_.CURCASE__V23*/ meltfptr[22];
		    /*_.MELT_DEBUG_FUN__V104*/ meltfptr[55] =
		      melt_apply ((meltclosure_ptr_t)
				  (( /*!MELT_DEBUG_FUN */ meltfrout->
				    tabval[7])),
				  (melt_ptr_t) (( /*nil */ NULL)),
				  (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_PTR ""), argtab, "",
				  (union meltparam_un *) 0);
		  }
		  ;
		  /*_.IF___V103*/ meltfptr[43] =
		    /*_.MELT_DEBUG_FUN__V104*/ meltfptr[55];;
		  /*epilog */

		  MELT_LOCATION ("warmelt-macro.melt:5686:/ clear");
		/*clear *//*_#THE_MELTCALLCOUNT__L31*/ meltfnum[26] = 0;
		  /*^clear */
		/*clear *//*_.MELT_DEBUG_FUN__V104*/ meltfptr[55] = 0;
		}
		;
	      }
	    else
	      {			/*^cond.else */

     /*_.IF___V103*/ meltfptr[43] = NULL;;
	      }
	    ;
	    MELT_LOCATION ("warmelt-macro.melt:5686:/ quasiblock");


	    /*_.PROGN___V105*/ meltfptr[75] = /*_.IF___V103*/ meltfptr[43];;
	    /*^compute */
	    /*_.IFCPP___V102*/ meltfptr[42] =
	      /*_.PROGN___V105*/ meltfptr[75];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-macro.melt:5686:/ clear");
	      /*clear *//*_#MELT_NEED_DBG__L30*/ meltfnum[20] = 0;
	    /*^clear */
	      /*clear *//*_.IF___V103*/ meltfptr[43] = 0;
	    /*^clear */
	      /*clear *//*_.PROGN___V105*/ meltfptr[75] = 0;
	  }

#else /*MELT_HAVE_DEBUG */
	  /*^cppif.else */
	  /*_.IFCPP___V102*/ meltfptr[42] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
	  ;
	}			/* ending  foreach_pair meltcit1__EACHPAIR */
   /*_.CURPAIRCASE__V22*/ meltfptr[21] = NULL;
   /*_.CURCASE__V23*/ meltfptr[22] = NULL;


      /*citerepilog */

      MELT_LOCATION ("warmelt-macro.melt:5602:/ clear");
	    /*clear *//*_.CURPAIRCASE__V22*/ meltfptr[21] = 0;
      /*^clear */
	    /*clear *//*_.CURCASE__V23*/ meltfptr[22] = 0;
      /*^clear */
	    /*clear *//*_.IFCPP___V24*/ meltfptr[23] = 0;
      /*^clear */
	    /*clear *//*_#IS_NOT_A__L9*/ meltfnum[7] = 0;
      /*^clear */
	    /*clear *//*_.IF___V28*/ meltfptr[24] = 0;
      /*^clear */
	    /*clear *//*_.LET___V31*/ meltfptr[25] = 0;
      /*^clear */
	    /*clear *//*_.IFCPP___V102*/ meltfptr[42] = 0;
    }				/*endciterblock FOREACH_PAIR */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-macro.melt:5688:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L32*/ meltfnum[6] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-macro.melt:5688:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L32*/ meltfnum[6])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L33*/ meltfnum[13] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-macro.melt:5688:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long =
		/*_#THE_MELTCALLCOUNT__L33*/ meltfnum[13];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-macro.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 5688;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "mexpand_variadic rescont";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.RESCONT__V17*/ meltfptr[16];
	      /*_.MELT_DEBUG_FUN__V108*/ meltfptr[76] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[7])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V107*/ meltfptr[38] =
	      /*_.MELT_DEBUG_FUN__V108*/ meltfptr[76];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-macro.melt:5688:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L33*/ meltfnum[13] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V108*/ meltfptr[76] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V107*/ meltfptr[38] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-macro.melt:5688:/ quasiblock");


      /*_.PROGN___V109*/ meltfptr[86] = /*_.IF___V107*/ meltfptr[38];;
      /*^compute */
      /*_.IFCPP___V106*/ meltfptr[48] = /*_.PROGN___V109*/ meltfptr[86];;
      /*epilog */

      MELT_LOCATION ("warmelt-macro.melt:5688:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L32*/ meltfnum[6] = 0;
      /*^clear */
	     /*clear *//*_.IF___V107*/ meltfptr[38] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V109*/ meltfptr[86] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V106*/ meltfptr[48] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-macro.melt:5689:/ quasiblock");


    /*^cond */
    /*cond */ if (
		   /*ifisa */
		   melt_is_instance_of ((melt_ptr_t)
					( /*_.RESCONT__V17*/ meltfptr[16]),
					(melt_ptr_t) (( /*!CLASS_REFERENCE */
						       meltfrout->tabval[2])))
      )				/*then */
      {
	/*^cond.then */
	/*^getslot */
	{
	  melt_ptr_t slot = NULL, obj = NULL;
	  obj = (melt_ptr_t) ( /*_.RESCONT__V17*/ meltfptr[16]) /*=obj*/ ;
	  melt_object_get_field (slot, obj, 0, "REFERENCED_VALUE");
   /*_.RES__V111*/ meltfptr[87] = slot;
	};
	;
      }
    else
      {				/*^cond.else */

  /*_.RES__V111*/ meltfptr[87] = NULL;;
      }
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-macro.melt:5691:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L34*/ meltfnum[10] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-macro.melt:5691:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L34*/ meltfnum[10])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L35*/ meltfnum[26] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-macro.melt:5691:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long =
		/*_#THE_MELTCALLCOUNT__L35*/ meltfnum[26];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-macro.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 5691;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "mexpand_variadic result";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.RES__V111*/ meltfptr[87];
	      /*_.MELT_DEBUG_FUN__V114*/ meltfptr[93] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[7])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V113*/ meltfptr[88] =
	      /*_.MELT_DEBUG_FUN__V114*/ meltfptr[93];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-macro.melt:5691:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L35*/ meltfnum[26] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V114*/ meltfptr[93] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V113*/ meltfptr[88] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-macro.melt:5691:/ quasiblock");


      /*_.PROGN___V115*/ meltfptr[44] = /*_.IF___V113*/ meltfptr[88];;
      /*^compute */
      /*_.IFCPP___V112*/ meltfptr[78] = /*_.PROGN___V115*/ meltfptr[44];;
      /*epilog */

      MELT_LOCATION ("warmelt-macro.melt:5691:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L34*/ meltfnum[10] = 0;
      /*^clear */
	     /*clear *//*_.IF___V113*/ meltfptr[88] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V115*/ meltfptr[44] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V112*/ meltfptr[78] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-macro.melt:5692:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.RES__V111*/ meltfptr[87];;

    {
      MELT_LOCATION ("warmelt-macro.melt:5692:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*_.LET___V110*/ meltfptr[84] = /*_.RETURN___V116*/ meltfptr[29];;

    MELT_LOCATION ("warmelt-macro.melt:5689:/ clear");
	   /*clear *//*_.RES__V111*/ meltfptr[87] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V112*/ meltfptr[78] = 0;
    /*^clear */
	   /*clear *//*_.RETURN___V116*/ meltfptr[29] = 0;
    /*_.LET___V12*/ meltfptr[10] = /*_.LET___V110*/ meltfptr[84];;

    MELT_LOCATION ("warmelt-macro.melt:5593:/ clear");
	   /*clear *//*_.CONT__V13*/ meltfptr[12] = 0;
    /*^clear */
	   /*clear *//*_.LOC__V14*/ meltfptr[13] = 0;
    /*^clear */
	   /*clear *//*_.LIST_FIRST__V15*/ meltfptr[14] = 0;
    /*^clear */
	   /*clear *//*_.FIRSTPAIR__V16*/ meltfptr[15] = 0;
    /*^clear */
	   /*clear *//*_.RESCONT__V17*/ meltfptr[16] = 0;
    /*^clear */
	   /*clear *//*_#LIST_LENGTH__L4*/ meltfnum[0] = 0;
    /*^clear */
	   /*clear *//*_#I__L5*/ meltfnum[4] = 0;
    /*^clear */
	   /*clear *//*_#I__L6*/ meltfnum[5] = 0;
    /*^clear */
	   /*clear *//*_.VARBINDMAP__V19*/ meltfptr[18] = 0;
    /*^clear */
	   /*clear *//*_.HOOKFUN__V20*/ meltfptr[19] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V106*/ meltfptr[48] = 0;
    /*^clear */
	   /*clear *//*_.LET___V110*/ meltfptr[84] = 0;
    MELT_LOCATION ("warmelt-macro.melt:5589:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.LET___V12*/ meltfptr[10];;

    {
      MELT_LOCATION ("warmelt-macro.melt:5589:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V6*/ meltfptr[5] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V8*/ meltfptr[6] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V10*/ meltfptr[8] = 0;
    /*^clear */
	   /*clear *//*_.LET___V12*/ meltfptr[10] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("MEXPAND_VARIADIC", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_113_warmelt_macro_MEXPAND_VARIADIC_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_113_warmelt_macro_MEXPAND_VARIADIC */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_114_warmelt_macro_LAMBDA___29__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un * meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un * meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_114_warmelt_macro_LAMBDA___29___melt =
    melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_114_warmelt_macro_LAMBDA___29___st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 6
    melt_ptr_t mcfr_varptr[6];
#define MELTFRAM_NBVARNUM 2
    long mcfr_varnum[2];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_114_warmelt_macro_LAMBDA___29__ is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_114_warmelt_macro_LAMBDA___29___st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 6; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_114_warmelt_macro_LAMBDA___29__ nbval 6*/
  meltfram__.mcfr_nbvar = 6 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("LAMBDA_", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-macro.melt:5598:/ getarg");
 /*_.X__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-macro.melt:5599:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L1*/ meltfnum[0] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-macro.melt:5599:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L1*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-macro.melt:5599:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-macro.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 5599;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring =
		"mexpand_variadic/hookfun rescont set to x";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.X__V2*/ meltfptr[1];
	      /*_.MELT_DEBUG_FUN__V5*/ meltfptr[4] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V4*/ meltfptr[3] = /*_.MELT_DEBUG_FUN__V5*/ meltfptr[4];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-macro.melt:5599:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V5*/ meltfptr[4] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V4*/ meltfptr[3] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-macro.melt:5599:/ quasiblock");


      /*_.PROGN___V6*/ meltfptr[4] = /*_.IF___V4*/ meltfptr[3];;
      /*^compute */
      /*_.IFCPP___V3*/ meltfptr[2] = /*_.PROGN___V6*/ meltfptr[4];;
      /*epilog */

      MELT_LOCATION ("warmelt-macro.melt:5599:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L1*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IF___V4*/ meltfptr[3] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V6*/ meltfptr[4] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V3*/ meltfptr[2] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-macro.melt:5600:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^cond */
    /*cond */ if (
		   /*ifisa */
		   melt_is_instance_of ((melt_ptr_t)
					(( /*~RESCONT */ meltfclos->
					  tabval[0])),
					(melt_ptr_t) (( /*!CLASS_REFERENCE */
						       meltfrout->tabval[1])))
      )				/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  /*^putslot */
	  /*putslot */
	  melt_assertmsg ("putslot checkobj @REFERENCED_VALUE",
			  melt_magic_discr ((melt_ptr_t)
					    (( /*~RESCONT */ meltfclos->
					      tabval[0]))) ==
			  MELTOBMAG_OBJECT);
	  melt_putfield_object ((( /*~RESCONT */ meltfclos->tabval[0])), (0),
				( /*_.X__V2*/ meltfptr[1]),
				"REFERENCED_VALUE");
	  ;
	  /*^touch */
	  meltgc_touch (( /*~RESCONT */ meltfclos->tabval[0]));
	  ;
	  /*^touchobj */

	  melt_dbgtrace_written_object (( /*~RESCONT */ meltfclos->tabval[0]),
					"put-fields");
	  ;
	  /*epilog */
	}
	;
      }				/*noelse */
    ;
    MELT_LOCATION ("warmelt-macro.melt:5598:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V3*/ meltfptr[2] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("LAMBDA_", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_114_warmelt_macro_LAMBDA___29___melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*noretval */ NULL);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_114_warmelt_macro_LAMBDA___29__ */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_115_warmelt_macro_LAMBDA___30__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un * meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un * meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_115_warmelt_macro_LAMBDA___30___melt =
    melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_115_warmelt_macro_LAMBDA___30___st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 3
    melt_ptr_t mcfr_varptr[3];
/*no varnum*/
#define MELTFRAM_NBVARNUM /*none*/0
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_115_warmelt_macro_LAMBDA___30__ is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_115_warmelt_macro_LAMBDA___30___st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 3; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_115_warmelt_macro_LAMBDA___30__ nbval 3*/
  meltfram__.mcfr_nbvar = 3 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("LAMBDA_", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-macro.melt:5628:/ getarg");
 /*_.E__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {

    /*^checksignal */
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[3];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & ( /*~ENV */ meltfclos->tabval[1]);
      /*^apply.arg */
      argtab[1].meltbp_aptr =
	(melt_ptr_t *) & ( /*~MEXPANDER */ meltfclos->tabval[0]);
      /*^apply.arg */
      argtab[2].meltbp_aptr =
	(melt_ptr_t *) & ( /*~MODCTX */ meltfclos->tabval[2]);
      /*_.MEXPANDER__V3*/ meltfptr[2] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*~MEXPANDER */ meltfclos->tabval[0])),
		    (melt_ptr_t) ( /*_.E__V2*/ meltfptr[1]),
		    (MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_PTR ""),
		    argtab, "", (union meltparam_un *) 0);
    }
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.MEXPANDER__V3*/ meltfptr[2];;

    {
      MELT_LOCATION ("warmelt-macro.melt:5628:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.MEXPANDER__V3*/ meltfptr[2] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("LAMBDA_", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_115_warmelt_macro_LAMBDA___30___melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_115_warmelt_macro_LAMBDA___30__ */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_116_warmelt_macro_LAMBDA___31__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un * meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un * meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_116_warmelt_macro_LAMBDA___31___melt =
    melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_116_warmelt_macro_LAMBDA___31___st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 3
    melt_ptr_t mcfr_varptr[3];
/*no varnum*/
#define MELTFRAM_NBVARNUM /*none*/0
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_116_warmelt_macro_LAMBDA___31__ is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_116_warmelt_macro_LAMBDA___31___st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 3; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_116_warmelt_macro_LAMBDA___31__ nbval 3*/
  meltfram__.mcfr_nbvar = 3 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("LAMBDA_", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-macro.melt:5659:/ getarg");
 /*_.E__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {

    /*^checksignal */
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[3];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & ( /*~NEWENV */ meltfclos->tabval[1]);
      /*^apply.arg */
      argtab[1].meltbp_aptr =
	(melt_ptr_t *) & ( /*~MEXPANDER */ meltfclos->tabval[0]);
      /*^apply.arg */
      argtab[2].meltbp_aptr =
	(melt_ptr_t *) & ( /*~MODCTX */ meltfclos->tabval[2]);
      /*_.MEXPANDER__V3*/ meltfptr[2] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*~MEXPANDER */ meltfclos->tabval[0])),
		    (melt_ptr_t) ( /*_.E__V2*/ meltfptr[1]),
		    (MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_PTR ""),
		    argtab, "", (union meltparam_un *) 0);
    }
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.MEXPANDER__V3*/ meltfptr[2];;

    {
      MELT_LOCATION ("warmelt-macro.melt:5659:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.MEXPANDER__V3*/ meltfptr[2] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("LAMBDA_", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_116_warmelt_macro_LAMBDA___31___melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_116_warmelt_macro_LAMBDA___31__ */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_117_warmelt_macro_LAMBDA___32__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un * meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un * meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_117_warmelt_macro_LAMBDA___32___melt =
    melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_117_warmelt_macro_LAMBDA___32___st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 13
    melt_ptr_t mcfr_varptr[13];
#define MELTFRAM_NBVARNUM 5
    long mcfr_varnum[5];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_117_warmelt_macro_LAMBDA___32__ is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_117_warmelt_macro_LAMBDA___32___st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 13; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_117_warmelt_macro_LAMBDA___32__ nbval 13*/
  meltfram__.mcfr_nbvar = 13 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("LAMBDA_", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-macro.melt:5670:/ getarg");
 /*_.XTUP__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-macro.melt:5671:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L1*/ meltfnum[0] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-macro.melt:5671:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L1*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-macro.melt:5671:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-macro.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 5671;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "mexpand_variadic/hookfun xtup";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.XTUP__V2*/ meltfptr[1];
	      /*_.MELT_DEBUG_FUN__V5*/ meltfptr[4] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V4*/ meltfptr[3] = /*_.MELT_DEBUG_FUN__V5*/ meltfptr[4];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-macro.melt:5671:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V5*/ meltfptr[4] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V4*/ meltfptr[3] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-macro.melt:5671:/ quasiblock");


      /*_.PROGN___V6*/ meltfptr[4] = /*_.IF___V4*/ meltfptr[3];;
      /*^compute */
      /*_.IFCPP___V3*/ meltfptr[2] = /*_.PROGN___V6*/ meltfptr[4];;
      /*epilog */

      MELT_LOCATION ("warmelt-macro.melt:5671:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L1*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IF___V4*/ meltfptr[3] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V6*/ meltfptr[4] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V3*/ meltfptr[2] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-macro.melt:5672:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    MELT_LOCATION ("warmelt-macro.melt:5673:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#IS_MULTIPLE_OR_NULL__L3*/ meltfnum[1] =
      (( /*_.XTUP__V2*/ meltfptr[1]) == NULL
       ||
       (melt_unsafe_magic_discr ((melt_ptr_t) ( /*_.XTUP__V2*/ meltfptr[1]))
	== MELTOBMAG_MULTIPLE));;
    MELT_LOCATION ("warmelt-macro.melt:5673:/ cond");
    /*cond */ if ( /*_#IS_MULTIPLE_OR_NULL__L3*/ meltfnum[1])	/*then */
      {
	/*^cond.then */
	/*_.IFELSE___V7*/ meltfptr[3] = /*_.XTUP__V2*/ meltfptr[1];;
      }
    else
      {
	MELT_LOCATION ("warmelt-macro.melt:5673:/ cond.else");

	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-macro.melt:5675:/ blockmultialloc");
	  /*multiallocblock */
	  {
	    struct meltletrec_1_st
	    {
	      struct MELT_MULTIPLE_STRUCT (1) rtup_0__TUPLREC__x8;
	      long meltletrec_1_endgap;
	    } *meltletrec_1_ptr = 0;
	    meltletrec_1_ptr =
	      (struct meltletrec_1_st *)
	      meltgc_allocate (sizeof (struct meltletrec_1_st), 0);
	    /*^blockmultialloc.initfill */
	    /*inimult rtup_0__TUPLREC__x8 */
 /*_.TUPLREC___V9*/ meltfptr[8] =
	      (melt_ptr_t) & meltletrec_1_ptr->rtup_0__TUPLREC__x8;
	    meltletrec_1_ptr->rtup_0__TUPLREC__x8.discr =
	      (meltobject_ptr_t) (((melt_ptr_t)
				   (MELT_PREDEF (DISCR_MULTIPLE))));
	    meltletrec_1_ptr->rtup_0__TUPLREC__x8.nbval = 1;


	    /*^putuple */
	    /*putupl#11 */
	    melt_assertmsg ("putupl [:5675] #11 checktup",
			    melt_magic_discr ((melt_ptr_t)
					      ( /*_.TUPLREC___V9*/
					       meltfptr[8])) ==
			    MELTOBMAG_MULTIPLE);
	    melt_assertmsg ("putupl [:5675] #11 checkoff",
			    (0 >= 0
			     && 0 <
			     melt_multiple_length ((melt_ptr_t)
						   ( /*_.TUPLREC___V9*/
						    meltfptr[8]))));
	    ((meltmultiple_ptr_t) ( /*_.TUPLREC___V9*/ meltfptr[8]))->
	      tabval[0] = (melt_ptr_t) ( /*_.XTUP__V2*/ meltfptr[1]);
	    ;
	    /*^touch */
	    meltgc_touch ( /*_.TUPLREC___V9*/ meltfptr[8]);
	    ;
	    /*_.TUPLE___V8*/ meltfptr[4] = /*_.TUPLREC___V9*/ meltfptr[8];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-macro.melt:5675:/ clear");
	      /*clear *//*_.TUPLREC___V9*/ meltfptr[8] = 0;
	    /*^clear */
	      /*clear *//*_.TUPLREC___V9*/ meltfptr[8] = 0;
	  }			/*end multiallocblock */
	  ;
	  /*_.IFELSE___V7*/ meltfptr[3] = /*_.TUPLE___V8*/ meltfptr[4];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-macro.melt:5673:/ clear");
	     /*clear *//*_.TUPLE___V8*/ meltfptr[4] = 0;
	}
	;
      }
    ;
    MELT_LOCATION ("warmelt-macro.melt:5672:/ cond");
    /*cond */ if (
		   /*ifisa */
		   melt_is_instance_of ((melt_ptr_t)
					(( /*~SIFVARIADIC */ meltfclos->
					  tabval[0])),
					(melt_ptr_t) (( /*!CLASS_SOURCE_IFVARIADIC */ meltfrout->tabval[1])))
      )				/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  /*^putslot */
	  /*putslot */
	  melt_assertmsg ("putslot checkobj @SIFVARIADIC_ELSE",
			  melt_magic_discr ((melt_ptr_t)
					    (( /*~SIFVARIADIC */ meltfclos->
					      tabval[0]))) ==
			  MELTOBMAG_OBJECT);
	  melt_putfield_object ((( /*~SIFVARIADIC */ meltfclos->tabval[0])),
				(4), ( /*_.IFELSE___V7*/ meltfptr[3]),
				"SIFVARIADIC_ELSE");
	  ;
	  /*^touch */
	  meltgc_touch (( /*~SIFVARIADIC */ meltfclos->tabval[0]));
	  ;
	  /*^touchobj */

	  melt_dbgtrace_written_object (( /*~SIFVARIADIC */ meltfclos->
					 tabval[0]), "put-fields");
	  ;
	  /*epilog */
	}
	;
      }				/*noelse */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-macro.melt:5676:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L4*/ meltfnum[0] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-macro.melt:5676:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L4*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L5*/ meltfnum[4] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-macro.melt:5676:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L5*/ meltfnum[4];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-macro.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 5676;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring =
		"mexpand_variadic/hookfun updated sifvariadic";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & ( /*~SIFVARIADIC */ meltfclos->tabval[0]);
	      /*_.MELT_DEBUG_FUN__V12*/ meltfptr[11] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V11*/ meltfptr[4] =
	      /*_.MELT_DEBUG_FUN__V12*/ meltfptr[11];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-macro.melt:5676:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L5*/ meltfnum[4] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V12*/ meltfptr[11] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V11*/ meltfptr[4] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-macro.melt:5676:/ quasiblock");


      /*_.PROGN___V13*/ meltfptr[11] = /*_.IF___V11*/ meltfptr[4];;
      /*^compute */
      /*_.IFCPP___V10*/ meltfptr[8] = /*_.PROGN___V13*/ meltfptr[11];;
      /*epilog */

      MELT_LOCATION ("warmelt-macro.melt:5676:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L4*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IF___V11*/ meltfptr[4] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V13*/ meltfptr[11] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V10*/ meltfptr[8] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-macro.melt:5670:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.IFCPP___V10*/ meltfptr[8];;

    {
      MELT_LOCATION ("warmelt-macro.melt:5670:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V3*/ meltfptr[2] = 0;
    /*^clear */
	   /*clear *//*_#IS_MULTIPLE_OR_NULL__L3*/ meltfnum[1] = 0;
    /*^clear */
	   /*clear *//*_.IFELSE___V7*/ meltfptr[3] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V10*/ meltfptr[8] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("LAMBDA_", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_117_warmelt_macro_LAMBDA___32___melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_117_warmelt_macro_LAMBDA___32__ */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_118_warmelt_macro_MEXPAND_MULTICALL (meltclosure_ptr_t meltclosp_,
					      melt_ptr_t meltfirstargp_,
					      const melt_argdescr_cell_t
					      meltxargdescr_[],
					      union meltparam_un *
					      meltxargtab_,
					      const melt_argdescr_cell_t
					      meltxresdescr_[],
					      union meltparam_un *
					      meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_118_warmelt_macro_MEXPAND_MULTICALL_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_118_warmelt_macro_MEXPAND_MULTICALL_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 50
    melt_ptr_t mcfr_varptr[50];
#define MELTFRAM_NBVARNUM 12
    long mcfr_varnum[12];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_118_warmelt_macro_MEXPAND_MULTICALL is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_118_warmelt_macro_MEXPAND_MULTICALL_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 50; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_118_warmelt_macro_MEXPAND_MULTICALL nbval 50*/
  meltfram__.mcfr_nbvar = 50 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("MEXPAND_MULTICALL", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-macro.melt:5712:/ getarg");
 /*_.SEXPR__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.ENV__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.ENV__V3*/ meltfptr[2])) != NULL);


  /*getarg#2 */
  /*^getarg */
  if (meltxargdescr_[1] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.MEXPANDER__V4*/ meltfptr[3] =
    (meltxargtab_[1].meltbp_aptr) ? (*(meltxargtab_[1].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.MEXPANDER__V4*/ meltfptr[3])) !=
	      NULL);


  /*getarg#3 */
  /*^getarg */
  if (meltxargdescr_[2] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.MODCTX__V5*/ meltfptr[4] =
    (meltxargtab_[2].meltbp_aptr) ? (*(meltxargtab_[2].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.MODCTX__V5*/ meltfptr[4])) !=
	      NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-macro.melt:5713:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L1*/ meltfnum[0] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.SEXPR__V2*/ meltfptr[1]),
			     (melt_ptr_t) (( /*!CLASS_SEXPR */ meltfrout->
					    tabval[0])));;
      MELT_LOCATION ("warmelt-macro.melt:5713:/ cond");
      /*cond */ if ( /*_#IS_A__L1*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V7*/ meltfptr[6] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-macro.melt:5713:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check sexpr"),
				  ("warmelt-macro.melt")
				  ? ("warmelt-macro.melt") : __FILE__,
				  (5713) ? (5713) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V7*/ meltfptr[6] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V6*/ meltfptr[5] = /*_.IFELSE___V7*/ meltfptr[6];;
      /*epilog */

      MELT_LOCATION ("warmelt-macro.melt:5713:/ clear");
	     /*clear *//*_#IS_A__L1*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V7*/ meltfptr[6] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V6*/ meltfptr[5] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-macro.melt:5714:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L2*/ meltfnum[0] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.ENV__V3*/ meltfptr[2]),
			     (melt_ptr_t) (( /*!CLASS_ENVIRONMENT */
					    meltfrout->tabval[1])));;
      MELT_LOCATION ("warmelt-macro.melt:5714:/ cond");
      /*cond */ if ( /*_#IS_A__L2*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V9*/ meltfptr[8] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-macro.melt:5714:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check env"),
				  ("warmelt-macro.melt")
				  ? ("warmelt-macro.melt") : __FILE__,
				  (5714) ? (5714) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V9*/ meltfptr[8] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V8*/ meltfptr[6] = /*_.IFELSE___V9*/ meltfptr[8];;
      /*epilog */

      MELT_LOCATION ("warmelt-macro.melt:5714:/ clear");
	     /*clear *//*_#IS_A__L2*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V9*/ meltfptr[8] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V8*/ meltfptr[6] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-macro.melt:5715:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_CLOSURE__L3*/ meltfnum[0] =
	(melt_magic_discr ((melt_ptr_t) ( /*_.MEXPANDER__V4*/ meltfptr[3])) ==
	 MELTOBMAG_CLOSURE);;
      MELT_LOCATION ("warmelt-macro.melt:5715:/ cond");
      /*cond */ if ( /*_#IS_CLOSURE__L3*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V11*/ meltfptr[10] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-macro.melt:5715:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check mexpander"),
				  ("warmelt-macro.melt")
				  ? ("warmelt-macro.melt") : __FILE__,
				  (5715) ? (5715) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V11*/ meltfptr[10] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V10*/ meltfptr[8] = /*_.IFELSE___V11*/ meltfptr[10];;
      /*epilog */

      MELT_LOCATION ("warmelt-macro.melt:5715:/ clear");
	     /*clear *//*_#IS_CLOSURE__L3*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V11*/ meltfptr[10] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V10*/ meltfptr[8] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-macro.melt:5716:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_OBJECT__L4*/ meltfnum[0] =
	(melt_magic_discr ((melt_ptr_t) ( /*_.MODCTX__V5*/ meltfptr[4])) ==
	 MELTOBMAG_OBJECT);;
      MELT_LOCATION ("warmelt-macro.melt:5716:/ cond");
      /*cond */ if ( /*_#IS_OBJECT__L4*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V13*/ meltfptr[12] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-macro.melt:5716:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check modctx"),
				  ("warmelt-macro.melt")
				  ? ("warmelt-macro.melt") : __FILE__,
				  (5716) ? (5716) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V13*/ meltfptr[12] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V12*/ meltfptr[10] = /*_.IFELSE___V13*/ meltfptr[12];;
      /*epilog */

      MELT_LOCATION ("warmelt-macro.melt:5716:/ clear");
	     /*clear *//*_#IS_OBJECT__L4*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V13*/ meltfptr[12] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V12*/ meltfptr[10] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-macro.melt:5717:/ quasiblock");


    /*^getslot */
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.SEXPR__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 2, "SEXP_CONTENTS");
  /*_.CONT__V15*/ meltfptr[14] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-macro.melt:5718:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.SEXPR__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 1, "LOCA_LOCATION");
  /*_.LOC__V16*/ meltfptr[15] = slot;
    };
    ;
 /*_.LIST_FIRST__V17*/ meltfptr[16] =
      (melt_list_first ((melt_ptr_t) ( /*_.CONT__V15*/ meltfptr[14])));;
    /*^compute */
 /*_.CURPAIR__V18*/ meltfptr[17] =
      (melt_pair_tail ((melt_ptr_t) ( /*_.LIST_FIRST__V17*/ meltfptr[16])));;
    MELT_LOCATION ("warmelt-macro.melt:5720:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      /*_.NEWENV__V19*/ meltfptr[18] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!FRESH_ENV */ meltfrout->tabval[2])),
		    (melt_ptr_t) ( /*_.ENV__V3*/ meltfptr[2]), (""),
		    (union meltparam_un *) 0, "", (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-macro.melt:5723:/ quasiblock");


 /*_.PAIR_HEAD__V21*/ meltfptr[20] =
      (melt_pair_head ((melt_ptr_t) ( /*_.CURPAIR__V18*/ meltfptr[17])));;
    MELT_LOCATION ("warmelt-macro.melt:5723:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.SEXPR__V2*/ meltfptr[1];
      /*_.RESTUP__V22*/ meltfptr[21] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!LAMBDA_ARG_BINDINGS */ meltfrout->tabval[3])),
		    (melt_ptr_t) ( /*_.PAIR_HEAD__V21*/ meltfptr[20]),
		    (MELTBPARSTR_PTR ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-macro.melt:5725:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#IS_A__L5*/ meltfnum[0] =
      melt_is_instance_of ((melt_ptr_t) ( /*_.RESTUP__V22*/ meltfptr[21]),
			   (melt_ptr_t) (( /*!DISCR_VARIADIC_FORMAL_SEQUENCE */ meltfrout->tabval[4])));;
    MELT_LOCATION ("warmelt-macro.melt:5725:/ cond");
    /*cond */ if ( /*_#IS_A__L5*/ meltfnum[0])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{


	  {
	    MELT_LOCATION ("warmelt-macro.melt:5727:/ locexp");
	    /* error_plain */
	      melt_error_str ((melt_ptr_t) ( /*_.LOC__V16*/ meltfptr[15]),
			      ("MULTICALL cannot have variadic result"),
			      (melt_ptr_t) 0);
	  }
	  ;
	  MELT_LOCATION ("warmelt-macro.melt:5728:/ quasiblock");


   /*_.RETVAL___V1*/ meltfptr[0] = NULL;;

	  {
	    MELT_LOCATION ("warmelt-macro.melt:5728:/ locexp");
	    /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
	    if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	      melt_warn_for_no_expected_secondary_results ();
	    /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
	    ;
	  }
	  ;
	  /*^finalreturn */
	  ;
	  /*finalret */ goto labend_rout;
	  MELT_LOCATION ("warmelt-macro.melt:5726:/ quasiblock");


	  /*_.PROGN___V25*/ meltfptr[24] = /*_.RETURN___V24*/ meltfptr[23];;
	  /*^compute */
	  /*_.IF___V23*/ meltfptr[22] = /*_.PROGN___V25*/ meltfptr[24];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-macro.melt:5725:/ clear");
	     /*clear *//*_.RETURN___V24*/ meltfptr[23] = 0;
	  /*^clear */
	     /*clear *//*_.PROGN___V25*/ meltfptr[24] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

  /*_.IF___V23*/ meltfptr[22] = NULL;;
      }
    ;
    /*^compute */
 /*_.PAIR_TAIL__V26*/ meltfptr[23] =
      (melt_pair_tail ((melt_ptr_t) ( /*_.CURPAIR__V18*/ meltfptr[17])));;
    MELT_LOCATION ("warmelt-macro.melt:5729:/ compute");
    /*_.CURPAIR__V18*/ meltfptr[17] = /*_.SETQ___V27*/ meltfptr[24] =
      /*_.PAIR_TAIL__V26*/ meltfptr[23];;
    MELT_LOCATION ("warmelt-macro.melt:5730:/ quasiblock");


 /*_.CURCALLEXP__V29*/ meltfptr[28] =
      (melt_pair_head ((melt_ptr_t) ( /*_.CURPAIR__V18*/ meltfptr[17])));;
    MELT_LOCATION ("warmelt-macro.melt:5731:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#IS_A__L6*/ meltfnum[5] =
      melt_is_instance_of ((melt_ptr_t) ( /*_.CURCALLEXP__V29*/ meltfptr[28]),
			   (melt_ptr_t) (( /*!CLASS_SEXPR */ meltfrout->
					  tabval[0])));;
    /*^compute */
 /*_#NOT__L7*/ meltfnum[6] =
      (!( /*_#IS_A__L6*/ meltfnum[5]));;
    MELT_LOCATION ("warmelt-macro.melt:5731:/ cond");
    /*cond */ if ( /*_#NOT__L7*/ meltfnum[6])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{


	  {
	    MELT_LOCATION ("warmelt-macro.melt:5732:/ locexp");
	    /* error_plain */
	      melt_error_str ((melt_ptr_t) ( /*_.LOC__V16*/ meltfptr[15]),
			      ("missing called expression in MULTICALL"),
			      (melt_ptr_t) 0);
	  }
	  ;
	  /*epilog */
	}
	;
      }				/*noelse */
    ;
 /*_.PAIR_TAIL__V30*/ meltfptr[29] =
      (melt_pair_tail ((melt_ptr_t) ( /*_.CURPAIR__V18*/ meltfptr[17])));;
    MELT_LOCATION ("warmelt-macro.melt:5733:/ compute");
    /*_.CURPAIR__V18*/ meltfptr[17] = /*_.SETQ___V31*/ meltfptr[30] =
      /*_.PAIR_TAIL__V30*/ meltfptr[29];;
    MELT_LOCATION ("warmelt-macro.melt:5734:/ quasiblock");


    /*^checksignal */
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[3];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.ENV__V3*/ meltfptr[2];
      /*^apply.arg */
      argtab[1].meltbp_aptr =
	(melt_ptr_t *) & /*_.MEXPANDER__V4*/ meltfptr[3];
      /*^apply.arg */
      argtab[2].meltbp_aptr = (melt_ptr_t *) & /*_.MODCTX__V5*/ meltfptr[4];
      /*_.CURCALL__V33*/ meltfptr[32] =
	melt_apply ((meltclosure_ptr_t) ( /*_.MEXPANDER__V4*/ meltfptr[3]),
		    (melt_ptr_t) ( /*_.CURCALLEXP__V29*/ meltfptr[28]),
		    (MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_PTR ""),
		    argtab, "", (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-macro.melt:5737:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#IS_A__L8*/ meltfnum[7] =
      melt_is_instance_of ((melt_ptr_t) ( /*_.CURCALL__V33*/ meltfptr[32]),
			   (melt_ptr_t) (( /*!CLASS_SOURCE_APPLY */
					  meltfrout->tabval[6])));;
    MELT_LOCATION ("warmelt-macro.melt:5737:/ cond");
    /*cond */ if ( /*_#IS_A__L8*/ meltfnum[7])	/*then */
      {
	/*^cond.then */
	/*_#OR___L9*/ meltfnum[8] = /*_#IS_A__L8*/ meltfnum[7];;
      }
    else
      {
	MELT_LOCATION ("warmelt-macro.melt:5737:/ cond.else");

	/*^block */
	/*anyblock */
	{

   /*_#IS_A__L10*/ meltfnum[9] =
	    melt_is_instance_of ((melt_ptr_t)
				 ( /*_.CURCALL__V33*/ meltfptr[32]),
				 (melt_ptr_t) (( /*!CLASS_SOURCE_MSEND */
						meltfrout->tabval[5])));;
	  /*^compute */
	  /*_#OR___L9*/ meltfnum[8] = /*_#IS_A__L10*/ meltfnum[9];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-macro.melt:5737:/ clear");
	     /*clear *//*_#IS_A__L10*/ meltfnum[9] = 0;
	}
	;
      }
    ;
    /*^cond */
    /*cond */ if ( /*_#OR___L9*/ meltfnum[8])	/*then */
      {
	/*^cond.then */
	/*_.IFELSE___V34*/ meltfptr[33] = ( /*nil */ NULL);;
      }
    else
      {
	MELT_LOCATION ("warmelt-macro.melt:5737:/ cond.else");

	/*^block */
	/*anyblock */
	{


#if MELT_HAVE_DEBUG
	  MELT_LOCATION ("warmelt-macro.melt:5741:/ cppif.then");
	  /*^block */
	  /*anyblock */
	  {


	    {
	      /*^locexp */
	      /*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	      melt_dbgcounter++;
#endif
	      ;
	    }
	    ;
	    /*^checksignal */
	    MELT_CHECK_SIGNAL ();
	    ;
     /*_#MELT_NEED_DBG__L11*/ meltfnum[9] =
	      /*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	      ( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	      0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	      ;;
	    MELT_LOCATION ("warmelt-macro.melt:5741:/ cond");
	    /*cond */ if ( /*_#MELT_NEED_DBG__L11*/ meltfnum[9])	/*then */
	      {
		/*^cond.then */
		/*^block */
		/*anyblock */
		{

       /*_#THE_MELTCALLCOUNT__L12*/ meltfnum[11] =
#ifdef meltcallcount
		    meltcallcount	/* the_meltcallcount */
#else
		    0L
#endif /* meltcallcount the_meltcallcount */
		    ;;
		  MELT_LOCATION ("warmelt-macro.melt:5741:/ checksignal");
		  MELT_CHECK_SIGNAL ();
		  ;
		  /*^apply */
		  /*apply */
		  {
		    union meltparam_un argtab[5];
		    memset (&argtab, 0, sizeof (argtab));
		    /*^apply.arg */
		    argtab[0].meltbp_long =
		      /*_#THE_MELTCALLCOUNT__L12*/ meltfnum[11];
		    /*^apply.arg */
		    argtab[1].meltbp_cstring = "warmelt-macro.melt";
		    /*^apply.arg */
		    argtab[2].meltbp_long = 5741;
		    /*^apply.arg */
		    argtab[3].meltbp_cstring =
		      "mexpand_multicall bad curcall";
		    /*^apply.arg */
		    argtab[4].meltbp_aptr =
		      (melt_ptr_t *) & /*_.CURCALL__V33*/ meltfptr[32];
		    /*_.MELT_DEBUG_FUN__V37*/ meltfptr[36] =
		      melt_apply ((meltclosure_ptr_t)
				  (( /*!MELT_DEBUG_FUN */ meltfrout->
				    tabval[7])),
				  (melt_ptr_t) (( /*nil */ NULL)),
				  (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_PTR ""), argtab, "",
				  (union meltparam_un *) 0);
		  }
		  ;
		  /*_.IF___V36*/ meltfptr[35] =
		    /*_.MELT_DEBUG_FUN__V37*/ meltfptr[36];;
		  /*epilog */

		  MELT_LOCATION ("warmelt-macro.melt:5741:/ clear");
		 /*clear *//*_#THE_MELTCALLCOUNT__L12*/ meltfnum[11] = 0;
		  /*^clear */
		 /*clear *//*_.MELT_DEBUG_FUN__V37*/ meltfptr[36] = 0;
		}
		;
	      }
	    else
	      {			/*^cond.else */

      /*_.IF___V36*/ meltfptr[35] = NULL;;
	      }
	    ;
	    MELT_LOCATION ("warmelt-macro.melt:5741:/ quasiblock");


	    /*_.PROGN___V38*/ meltfptr[36] = /*_.IF___V36*/ meltfptr[35];;
	    /*^compute */
	    /*_.IFCPP___V35*/ meltfptr[34] = /*_.PROGN___V38*/ meltfptr[36];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-macro.melt:5741:/ clear");
	       /*clear *//*_#MELT_NEED_DBG__L11*/ meltfnum[9] = 0;
	    /*^clear */
	       /*clear *//*_.IF___V36*/ meltfptr[35] = 0;
	    /*^clear */
	       /*clear *//*_.PROGN___V38*/ meltfptr[36] = 0;
	  }

#else /*MELT_HAVE_DEBUG */
	  /*^cppif.else */
	  /*_.IFCPP___V35*/ meltfptr[34] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
	  ;

	  {
	    MELT_LOCATION ("warmelt-macro.melt:5742:/ locexp");
	    /* error_plain */ melt_error_str ((melt_ptr_t) ( /*_.LOC__V16*/ meltfptr[15]), ("called expression in MULTICALL is invalid, expecting application or\
 send"), (melt_ptr_t) 0);
	  }
	  ;
	  MELT_LOCATION ("warmelt-macro.melt:5743:/ quasiblock");


   /*_.RETVAL___V1*/ meltfptr[0] = NULL;;

	  {
	    MELT_LOCATION ("warmelt-macro.melt:5743:/ locexp");
	    /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
	    if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	      melt_warn_for_no_expected_secondary_results ();
	    /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
	    ;
	  }
	  ;
	  /*^finalreturn */
	  ;
	  /*finalret */ goto labend_rout;
	  MELT_LOCATION ("warmelt-macro.melt:5740:/ quasiblock");


	  /*_.PROGN___V40*/ meltfptr[36] = /*_.RETURN___V39*/ meltfptr[35];;
	  /*^compute */
	  /*_.IFELSE___V34*/ meltfptr[33] = /*_.PROGN___V40*/ meltfptr[36];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-macro.melt:5737:/ clear");
	     /*clear *//*_.IFCPP___V35*/ meltfptr[34] = 0;
	  /*^clear */
	     /*clear *//*_.RETURN___V39*/ meltfptr[35] = 0;
	  /*^clear */
	     /*clear *//*_.PROGN___V40*/ meltfptr[36] = 0;
	}
	;
      }
    ;
    MELT_LOCATION ("warmelt-macro.melt:5745:/ quasiblock");


    /*^newclosure */
		 /*newclosure *//*_.LAMBDA___V42*/ meltfptr[35] =
      (melt_ptr_t)
      meltgc_new_closure ((meltobject_ptr_t)
			  (((melt_ptr_t) (MELT_PREDEF (DISCR_CLOSURE)))),
			  (meltroutine_ptr_t) (( /*!konst_10 */ meltfrout->
						tabval[10])), (1));
    ;
    /*^putclosedv */
    /*putclosv */
    melt_assertmsg ("putclosv checkclo",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.LAMBDA___V42*/ meltfptr[35])) ==
		    MELTOBMAG_CLOSURE);
    melt_assertmsg ("putclosv checkoff", 0 >= 0
		    && 0 <
		    melt_closure_size ((melt_ptr_t)
				       ( /*_.LAMBDA___V42*/ meltfptr[35])));
    ((meltclosure_ptr_t) /*_.LAMBDA___V42*/ meltfptr[35])->tabval[0] =
      (melt_ptr_t) ( /*_.NEWENV__V19*/ meltfptr[18]);
    ;
    /*_.LAMBDA___V41*/ meltfptr[34] = /*_.LAMBDA___V42*/ meltfptr[35];;
    MELT_LOCATION ("warmelt-macro.melt:5744:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & /*_.LAMBDA___V41*/ meltfptr[34];
      /*_.MULTIPLE_EVERY__V43*/ meltfptr[36] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!MULTIPLE_EVERY */ meltfrout->tabval[8])),
		    (melt_ptr_t) ( /*_.RESTUP__V22*/ meltfptr[21]),
		    (MELTBPARSTR_PTR ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-macro.melt:5747:/ quasiblock");


    MELT_LOCATION ("warmelt-macro.melt:5749:/ quasiblock");


    /*^newclosure */
		 /*newclosure *//*_.LAMBDA___V46*/ meltfptr[45] =
      (melt_ptr_t)
      meltgc_new_closure ((meltobject_ptr_t)
			  (((melt_ptr_t) (MELT_PREDEF (DISCR_CLOSURE)))),
			  (meltroutine_ptr_t) (( /*!konst_13 */ meltfrout->
						tabval[13])), (3));
    ;
    /*^putclosedv */
    /*putclosv */
    melt_assertmsg ("putclosv checkclo",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.LAMBDA___V46*/ meltfptr[45])) ==
		    MELTOBMAG_CLOSURE);
    melt_assertmsg ("putclosv checkoff", 0 >= 0
		    && 0 <
		    melt_closure_size ((melt_ptr_t)
				       ( /*_.LAMBDA___V46*/ meltfptr[45])));
    ((meltclosure_ptr_t) /*_.LAMBDA___V46*/ meltfptr[45])->tabval[0] =
      (melt_ptr_t) ( /*_.MEXPANDER__V4*/ meltfptr[3]);
    ;
    /*^putclosedv */
    /*putclosv */
    melt_assertmsg ("putclosv checkclo",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.LAMBDA___V46*/ meltfptr[45])) ==
		    MELTOBMAG_CLOSURE);
    melt_assertmsg ("putclosv checkoff", 1 >= 0
		    && 1 <
		    melt_closure_size ((melt_ptr_t)
				       ( /*_.LAMBDA___V46*/ meltfptr[45])));
    ((meltclosure_ptr_t) /*_.LAMBDA___V46*/ meltfptr[45])->tabval[1] =
      (melt_ptr_t) ( /*_.NEWENV__V19*/ meltfptr[18]);
    ;
    /*^putclosedv */
    /*putclosv */
    melt_assertmsg ("putclosv checkclo",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.LAMBDA___V46*/ meltfptr[45])) ==
		    MELTOBMAG_CLOSURE);
    melt_assertmsg ("putclosv checkoff", 2 >= 0
		    && 2 <
		    melt_closure_size ((melt_ptr_t)
				       ( /*_.LAMBDA___V46*/ meltfptr[45])));
    ((meltclosure_ptr_t) /*_.LAMBDA___V46*/ meltfptr[45])->tabval[2] =
      (melt_ptr_t) ( /*_.MODCTX__V5*/ meltfptr[4]);
    ;
    /*_.LAMBDA___V45*/ meltfptr[44] = /*_.LAMBDA___V46*/ meltfptr[45];;
    MELT_LOCATION ("warmelt-macro.melt:5747:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[2];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & ( /*!DISCR_MULTIPLE */ meltfrout->tabval[12]);
      /*^apply.arg */
      argtab[1].meltbp_aptr =
	(melt_ptr_t *) & /*_.LAMBDA___V45*/ meltfptr[44];
      /*_.BODYTUP__V47*/ meltfptr[46] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!PAIRLIST_TO_MULTIPLE */ meltfrout->tabval[11])),
		    (melt_ptr_t) ( /*_.CURPAIR__V18*/ meltfptr[17]),
		    (MELTBPARSTR_PTR MELTBPARSTR_PTR ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-macro.melt:5751:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*^rawallocobj */
    /*rawallocobj */
    {
      melt_ptr_t newobj = 0;
      melt_raw_object_create (newobj,
			      (melt_ptr_t) (( /*!CLASS_SOURCE_MULTICALL */
					     meltfrout->tabval[14])), (5),
			      "CLASS_SOURCE_MULTICALL");
  /*_.INST__V49*/ meltfptr[48] =
	newobj;
    };
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @LOCA_LOCATION",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V49*/ meltfptr[48])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V49*/ meltfptr[48]), (1),
			  ( /*_.LOC__V16*/ meltfptr[15]), "LOCA_LOCATION");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @SMULC_RESBIND",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V49*/ meltfptr[48])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V49*/ meltfptr[48]), (2),
			  ( /*_.RESTUP__V22*/ meltfptr[21]), "SMULC_RESBIND");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @SMULC_CALL",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V49*/ meltfptr[48])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V49*/ meltfptr[48]), (3),
			  ( /*_.CURCALL__V33*/ meltfptr[32]), "SMULC_CALL");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @SMULC_BODY",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V49*/ meltfptr[48])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V49*/ meltfptr[48]), (4),
			  ( /*_.BODYTUP__V47*/ meltfptr[46]), "SMULC_BODY");
    ;
    /*^touchobj */

    melt_dbgtrace_written_object ( /*_.INST__V49*/ meltfptr[48],
				  "newly made instance");
    ;
    /*_.MULCR__V48*/ meltfptr[47] = /*_.INST__V49*/ meltfptr[48];;
    MELT_LOCATION ("warmelt-macro.melt:5758:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.MULCR__V48*/ meltfptr[47];;

    {
      MELT_LOCATION ("warmelt-macro.melt:5758:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*_.LET___V44*/ meltfptr[43] = /*_.RETURN___V50*/ meltfptr[49];;

    MELT_LOCATION ("warmelt-macro.melt:5747:/ clear");
	   /*clear *//*_.LAMBDA___V45*/ meltfptr[44] = 0;
    /*^clear */
	   /*clear *//*_.BODYTUP__V47*/ meltfptr[46] = 0;
    /*^clear */
	   /*clear *//*_.MULCR__V48*/ meltfptr[47] = 0;
    /*^clear */
	   /*clear *//*_.RETURN___V50*/ meltfptr[49] = 0;
    /*_.LET___V32*/ meltfptr[31] = /*_.LET___V44*/ meltfptr[43];;

    MELT_LOCATION ("warmelt-macro.melt:5734:/ clear");
	   /*clear *//*_.CURCALL__V33*/ meltfptr[32] = 0;
    /*^clear */
	   /*clear *//*_#IS_A__L8*/ meltfnum[7] = 0;
    /*^clear */
	   /*clear *//*_#OR___L9*/ meltfnum[8] = 0;
    /*^clear */
	   /*clear *//*_.IFELSE___V34*/ meltfptr[33] = 0;
    /*^clear */
	   /*clear *//*_.LAMBDA___V41*/ meltfptr[34] = 0;
    /*^clear */
	   /*clear *//*_.MULTIPLE_EVERY__V43*/ meltfptr[36] = 0;
    /*^clear */
	   /*clear *//*_.LET___V44*/ meltfptr[43] = 0;
    /*_.LET___V28*/ meltfptr[27] = /*_.LET___V32*/ meltfptr[31];;

    MELT_LOCATION ("warmelt-macro.melt:5730:/ clear");
	   /*clear *//*_.CURCALLEXP__V29*/ meltfptr[28] = 0;
    /*^clear */
	   /*clear *//*_#IS_A__L6*/ meltfnum[5] = 0;
    /*^clear */
	   /*clear *//*_#NOT__L7*/ meltfnum[6] = 0;
    /*^clear */
	   /*clear *//*_.PAIR_TAIL__V30*/ meltfptr[29] = 0;
    /*^clear */
	   /*clear *//*_.SETQ___V31*/ meltfptr[30] = 0;
    /*^clear */
	   /*clear *//*_.LET___V32*/ meltfptr[31] = 0;
    /*_.LET___V20*/ meltfptr[19] = /*_.LET___V28*/ meltfptr[27];;

    MELT_LOCATION ("warmelt-macro.melt:5723:/ clear");
	   /*clear *//*_.PAIR_HEAD__V21*/ meltfptr[20] = 0;
    /*^clear */
	   /*clear *//*_.RESTUP__V22*/ meltfptr[21] = 0;
    /*^clear */
	   /*clear *//*_#IS_A__L5*/ meltfnum[0] = 0;
    /*^clear */
	   /*clear *//*_.IF___V23*/ meltfptr[22] = 0;
    /*^clear */
	   /*clear *//*_.PAIR_TAIL__V26*/ meltfptr[23] = 0;
    /*^clear */
	   /*clear *//*_.SETQ___V27*/ meltfptr[24] = 0;
    /*^clear */
	   /*clear *//*_.LET___V28*/ meltfptr[27] = 0;
    /*_.LET___V14*/ meltfptr[12] = /*_.LET___V20*/ meltfptr[19];;

    MELT_LOCATION ("warmelt-macro.melt:5717:/ clear");
	   /*clear *//*_.CONT__V15*/ meltfptr[14] = 0;
    /*^clear */
	   /*clear *//*_.LOC__V16*/ meltfptr[15] = 0;
    /*^clear */
	   /*clear *//*_.LIST_FIRST__V17*/ meltfptr[16] = 0;
    /*^clear */
	   /*clear *//*_.CURPAIR__V18*/ meltfptr[17] = 0;
    /*^clear */
	   /*clear *//*_.NEWENV__V19*/ meltfptr[18] = 0;
    /*^clear */
	   /*clear *//*_.LET___V20*/ meltfptr[19] = 0;
    MELT_LOCATION ("warmelt-macro.melt:5712:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.LET___V14*/ meltfptr[12];;

    {
      MELT_LOCATION ("warmelt-macro.melt:5712:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V6*/ meltfptr[5] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V8*/ meltfptr[6] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V10*/ meltfptr[8] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V12*/ meltfptr[10] = 0;
    /*^clear */
	   /*clear *//*_.LET___V14*/ meltfptr[12] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("MEXPAND_MULTICALL", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_118_warmelt_macro_MEXPAND_MULTICALL_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_118_warmelt_macro_MEXPAND_MULTICALL */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_119_warmelt_macro_LAMBDA___33__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un * meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un * meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_119_warmelt_macro_LAMBDA___33___melt =
    melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_119_warmelt_macro_LAMBDA___33___st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 3
    melt_ptr_t mcfr_varptr[3];
/*no varnum*/
#define MELTFRAM_NBVARNUM /*none*/0
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_119_warmelt_macro_LAMBDA___33__ is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_119_warmelt_macro_LAMBDA___33___st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 3; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_119_warmelt_macro_LAMBDA___33__ nbval 3*/
  meltfram__.mcfr_nbvar = 3 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("LAMBDA_", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-macro.melt:5745:/ getarg");
 /*_.LB__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {

    /*^checksignal */
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.LB__V2*/ meltfptr[1];
      /*_.PUT_ENV__V3*/ meltfptr[2] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!PUT_ENV */ meltfrout->tabval[0])),
		    (melt_ptr_t) (( /*~NEWENV */ meltfclos->tabval[0])),
		    (MELTBPARSTR_PTR ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.PUT_ENV__V3*/ meltfptr[2];;

    {
      MELT_LOCATION ("warmelt-macro.melt:5745:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.PUT_ENV__V3*/ meltfptr[2] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("LAMBDA_", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_119_warmelt_macro_LAMBDA___33___melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_119_warmelt_macro_LAMBDA___33__ */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_120_warmelt_macro_LAMBDA___34__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un * meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un * meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_120_warmelt_macro_LAMBDA___34___melt =
    melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_120_warmelt_macro_LAMBDA___34___st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 3
    melt_ptr_t mcfr_varptr[3];
/*no varnum*/
#define MELTFRAM_NBVARNUM /*none*/0
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_120_warmelt_macro_LAMBDA___34__ is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_120_warmelt_macro_LAMBDA___34___st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 3; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_120_warmelt_macro_LAMBDA___34__ nbval 3*/
  meltfram__.mcfr_nbvar = 3 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("LAMBDA_", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-macro.melt:5749:/ getarg");
 /*_.E__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {

    /*^checksignal */
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[3];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & ( /*~NEWENV */ meltfclos->tabval[1]);
      /*^apply.arg */
      argtab[1].meltbp_aptr =
	(melt_ptr_t *) & ( /*~MEXPANDER */ meltfclos->tabval[0]);
      /*^apply.arg */
      argtab[2].meltbp_aptr =
	(melt_ptr_t *) & ( /*~MODCTX */ meltfclos->tabval[2]);
      /*_.MEXPANDER__V3*/ meltfptr[2] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*~MEXPANDER */ meltfclos->tabval[0])),
		    (melt_ptr_t) ( /*_.E__V2*/ meltfptr[1]),
		    (MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_PTR ""),
		    argtab, "", (union meltparam_un *) 0);
    }
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.MEXPANDER__V3*/ meltfptr[2];;

    {
      MELT_LOCATION ("warmelt-macro.melt:5749:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.MEXPANDER__V3*/ meltfptr[2] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("LAMBDA_", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_120_warmelt_macro_LAMBDA___34___melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_120_warmelt_macro_LAMBDA___34__ */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_121_warmelt_macro_MEXPAND_QUOTE (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un * meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un * meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_121_warmelt_macro_MEXPAND_QUOTE_melt =
    melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_121_warmelt_macro_MEXPAND_QUOTE_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 27
    melt_ptr_t mcfr_varptr[27];
#define MELTFRAM_NBVARNUM 7
    long mcfr_varnum[7];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_121_warmelt_macro_MEXPAND_QUOTE is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_121_warmelt_macro_MEXPAND_QUOTE_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 27; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_121_warmelt_macro_MEXPAND_QUOTE nbval 27*/
  meltfram__.mcfr_nbvar = 27 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("MEXPAND_QUOTE", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-macro.melt:5773:/ getarg");
 /*_.SEXPR__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.ENV__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.ENV__V3*/ meltfptr[2])) != NULL);


  /*getarg#2 */
  /*^getarg */
  if (meltxargdescr_[1] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.MEXPANDER__V4*/ meltfptr[3] =
    (meltxargtab_[1].meltbp_aptr) ? (*(meltxargtab_[1].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.MEXPANDER__V4*/ meltfptr[3])) !=
	      NULL);


  /*getarg#3 */
  /*^getarg */
  if (meltxargdescr_[2] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.MODCTX__V5*/ meltfptr[4] =
    (meltxargtab_[2].meltbp_aptr) ? (*(meltxargtab_[2].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.MODCTX__V5*/ meltfptr[4])) !=
	      NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-macro.melt:5774:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L1*/ meltfnum[0] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.SEXPR__V2*/ meltfptr[1]),
			     (melt_ptr_t) (( /*!CLASS_SEXPR */ meltfrout->
					    tabval[0])));;
      MELT_LOCATION ("warmelt-macro.melt:5774:/ cond");
      /*cond */ if ( /*_#IS_A__L1*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V7*/ meltfptr[6] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-macro.melt:5774:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check sexpr"),
				  ("warmelt-macro.melt")
				  ? ("warmelt-macro.melt") : __FILE__,
				  (5774) ? (5774) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V7*/ meltfptr[6] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V6*/ meltfptr[5] = /*_.IFELSE___V7*/ meltfptr[6];;
      /*epilog */

      MELT_LOCATION ("warmelt-macro.melt:5774:/ clear");
	     /*clear *//*_#IS_A__L1*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V7*/ meltfptr[6] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V6*/ meltfptr[5] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-macro.melt:5775:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L2*/ meltfnum[0] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.ENV__V3*/ meltfptr[2]),
			     (melt_ptr_t) (( /*!CLASS_ENVIRONMENT */
					    meltfrout->tabval[1])));;
      MELT_LOCATION ("warmelt-macro.melt:5775:/ cond");
      /*cond */ if ( /*_#IS_A__L2*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V9*/ meltfptr[8] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-macro.melt:5775:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check env"),
				  ("warmelt-macro.melt")
				  ? ("warmelt-macro.melt") : __FILE__,
				  (5775) ? (5775) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V9*/ meltfptr[8] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V8*/ meltfptr[6] = /*_.IFELSE___V9*/ meltfptr[8];;
      /*epilog */

      MELT_LOCATION ("warmelt-macro.melt:5775:/ clear");
	     /*clear *//*_#IS_A__L2*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V9*/ meltfptr[8] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V8*/ meltfptr[6] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-macro.melt:5776:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_OBJECT__L3*/ meltfnum[0] =
	(melt_magic_discr ((melt_ptr_t) ( /*_.MODCTX__V5*/ meltfptr[4])) ==
	 MELTOBMAG_OBJECT);;
      MELT_LOCATION ("warmelt-macro.melt:5776:/ cond");
      /*cond */ if ( /*_#IS_OBJECT__L3*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V11*/ meltfptr[10] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-macro.melt:5776:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check modctx"),
				  ("warmelt-macro.melt")
				  ? ("warmelt-macro.melt") : __FILE__,
				  (5776) ? (5776) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V11*/ meltfptr[10] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V10*/ meltfptr[8] = /*_.IFELSE___V11*/ meltfptr[10];;
      /*epilog */

      MELT_LOCATION ("warmelt-macro.melt:5776:/ clear");
	     /*clear *//*_#IS_OBJECT__L3*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V11*/ meltfptr[10] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V10*/ meltfptr[8] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-macro.melt:5777:/ quasiblock");


    /*^getslot */
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.SEXPR__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 2, "SEXP_CONTENTS");
  /*_.CONT__V13*/ meltfptr[12] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-macro.melt:5778:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.SEXPR__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 1, "LOCA_LOCATION");
  /*_.LOC__V14*/ meltfptr[13] = slot;
    };
    ;
 /*_.LIST_FIRST__V15*/ meltfptr[14] =
      (melt_list_first ((melt_ptr_t) ( /*_.CONT__V13*/ meltfptr[12])));;
    /*^compute */
 /*_.CURPAIR__V16*/ meltfptr[15] =
      (melt_pair_tail ((melt_ptr_t) ( /*_.LIST_FIRST__V15*/ meltfptr[14])));;
    /*^compute */
 /*_.QUOTED__V17*/ meltfptr[16] =
      (melt_pair_head ((melt_ptr_t) ( /*_.CURPAIR__V16*/ meltfptr[15])));;
    MELT_LOCATION ("warmelt-macro.melt:5782:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_.PAIR_TAIL__V18*/ meltfptr[17] =
      (melt_pair_tail ((melt_ptr_t) ( /*_.CURPAIR__V16*/ meltfptr[15])));;
    MELT_LOCATION ("warmelt-macro.melt:5782:/ cond");
    /*cond */ if ( /*_.PAIR_TAIL__V18*/ meltfptr[17])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{


	  {
	    MELT_LOCATION ("warmelt-macro.melt:5783:/ locexp");
	    /* error_plain */
	      melt_error_str ((melt_ptr_t) ( /*_.LOC__V14*/ meltfptr[13]),
			      ("QUOTE should have only one argument"),
			      (melt_ptr_t) 0);
	  }
	  ;
	  /*epilog */
	}
	;
      }				/*noelse */
    ;
    MELT_LOCATION ("warmelt-macro.melt:5784:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#IS_A__L4*/ meltfnum[0] =
      melt_is_instance_of ((melt_ptr_t) ( /*_.QUOTED__V17*/ meltfptr[16]),
			   (melt_ptr_t) (( /*!CLASS_SYMBOL */ meltfrout->
					  tabval[2])));;
    MELT_LOCATION ("warmelt-macro.melt:5784:/ cond");
    /*cond */ if ( /*_#IS_A__L4*/ meltfnum[0])	/*then */
      {
	/*^cond.then */
	/*_.IFELSE___V19*/ meltfptr[18] = ( /*nil */ NULL);;
      }
    else
      {
	MELT_LOCATION ("warmelt-macro.melt:5784:/ cond.else");

	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-macro.melt:5787:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
   /*_#IS_STRING__L5*/ meltfnum[4] =
	    (melt_magic_discr ((melt_ptr_t) ( /*_.QUOTED__V17*/ meltfptr[16]))
	     == MELTOBMAG_STRING);;
	  MELT_LOCATION ("warmelt-macro.melt:5787:/ cond");
	  /*cond */ if ( /*_#IS_STRING__L5*/ meltfnum[4])	/*then */
	    {
	      /*^cond.then */
	      /*_.IFELSE___V20*/ meltfptr[19] = ( /*nil */ NULL);;
	    }
	  else
	    {
	      MELT_LOCATION ("warmelt-macro.melt:5787:/ cond.else");

	      /*^block */
	      /*anyblock */
	      {

		MELT_LOCATION ("warmelt-macro.melt:5790:/ checksignal");
		MELT_CHECK_SIGNAL ();
		;
     /*_#IS_INTEGERBOX__L6*/ meltfnum[5] =
		  (melt_magic_discr
		   ((melt_ptr_t) ( /*_.QUOTED__V17*/ meltfptr[16])) ==
		   MELTOBMAG_INT);;
		MELT_LOCATION ("warmelt-macro.melt:5790:/ cond");
		/*cond */ if ( /*_#IS_INTEGERBOX__L6*/ meltfnum[5])	/*then */
		  {
		    /*^cond.then */
		    /*_.IFELSE___V21*/ meltfptr[20] = ( /*nil */ NULL);;
		  }
		else
		  {
		    MELT_LOCATION ("warmelt-macro.melt:5790:/ cond.else");

		    /*^block */
		    /*anyblock */
		    {


		      {
			MELT_LOCATION ("warmelt-macro.melt:5794:/ locexp");
			/* error_plain */
			  melt_error_str ((melt_ptr_t)
					  ( /*_.LOC__V14*/ meltfptr[13]),
					  ("QUOTE should have a symbol, string, or integer argument"),
					  (melt_ptr_t) 0);
		      }
		      ;
		      MELT_LOCATION ("warmelt-macro.melt:5793:/ quasiblock");


		      /*epilog */
		    }
		    ;
		  }
		;
		/*_.IFELSE___V20*/ meltfptr[19] =
		  /*_.IFELSE___V21*/ meltfptr[20];;
		/*epilog */

		MELT_LOCATION ("warmelt-macro.melt:5787:/ clear");
	       /*clear *//*_#IS_INTEGERBOX__L6*/ meltfnum[5] = 0;
		/*^clear */
	       /*clear *//*_.IFELSE___V21*/ meltfptr[20] = 0;
	      }
	      ;
	    }
	  ;
	  /*_.IFELSE___V19*/ meltfptr[18] = /*_.IFELSE___V20*/ meltfptr[19];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-macro.melt:5784:/ clear");
	     /*clear *//*_#IS_STRING__L5*/ meltfnum[4] = 0;
	  /*^clear */
	     /*clear *//*_.IFELSE___V20*/ meltfptr[19] = 0;
	}
	;
      }
    ;
    MELT_LOCATION ("warmelt-macro.melt:5796:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#IS_A__L7*/ meltfnum[5] =
      melt_is_instance_of ((melt_ptr_t) ( /*_.QUOTED__V17*/ meltfptr[16]),
			   (melt_ptr_t) (( /*!CLASS_KEYWORD */ meltfrout->
					  tabval[3])));;
    MELT_LOCATION ("warmelt-macro.melt:5796:/ cond");
    /*cond */ if ( /*_#IS_A__L7*/ meltfnum[5])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-macro.melt:5797:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^quasiblock */


	  /*_.RETVAL___V1*/ meltfptr[0] = /*_.QUOTED__V17*/ meltfptr[16];;

	  {
	    MELT_LOCATION ("warmelt-macro.melt:5797:/ locexp");
	    /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
	    if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	      melt_warn_for_no_expected_secondary_results ();
	    /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
	    ;
	  }
	  ;
	  /*^finalreturn */
	  ;
	  /*finalret */ goto labend_rout;
	  /*_.IF___V22*/ meltfptr[20] = /*_.RETURN___V23*/ meltfptr[19];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-macro.melt:5796:/ clear");
	     /*clear *//*_.RETURN___V23*/ meltfptr[19] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

  /*_.IF___V22*/ meltfptr[20] = NULL;;
      }
    ;
    MELT_LOCATION ("warmelt-macro.melt:5798:/ quasiblock");


    /*^checksignal */
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*^rawallocobj */
    /*rawallocobj */
    {
      melt_ptr_t newobj = 0;
      melt_raw_object_create (newobj,
			      (melt_ptr_t) (( /*!CLASS_SOURCE_QUOTE */
					     meltfrout->tabval[4])), (3),
			      "CLASS_SOURCE_QUOTE");
  /*_.INST__V26*/ meltfptr[25] =
	newobj;
    };
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @LOCA_LOCATION",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V26*/ meltfptr[25])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V26*/ meltfptr[25]), (1),
			  ( /*_.LOC__V14*/ meltfptr[13]), "LOCA_LOCATION");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @SQUOTED",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V26*/ meltfptr[25])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V26*/ meltfptr[25]), (2),
			  ( /*_.QUOTED__V17*/ meltfptr[16]), "SQUOTED");
    ;
    /*^touchobj */

    melt_dbgtrace_written_object ( /*_.INST__V26*/ meltfptr[25],
				  "newly made instance");
    ;
    /*_.SQU__V25*/ meltfptr[24] = /*_.INST__V26*/ meltfptr[25];;
    MELT_LOCATION ("warmelt-macro.melt:5801:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.SQU__V25*/ meltfptr[24];;

    {
      MELT_LOCATION ("warmelt-macro.melt:5801:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*_.LET___V24*/ meltfptr[19] = /*_.RETURN___V27*/ meltfptr[26];;

    MELT_LOCATION ("warmelt-macro.melt:5798:/ clear");
	   /*clear *//*_.SQU__V25*/ meltfptr[24] = 0;
    /*^clear */
	   /*clear *//*_.RETURN___V27*/ meltfptr[26] = 0;
    /*_.LET___V12*/ meltfptr[10] = /*_.LET___V24*/ meltfptr[19];;

    MELT_LOCATION ("warmelt-macro.melt:5777:/ clear");
	   /*clear *//*_.CONT__V13*/ meltfptr[12] = 0;
    /*^clear */
	   /*clear *//*_.LOC__V14*/ meltfptr[13] = 0;
    /*^clear */
	   /*clear *//*_.LIST_FIRST__V15*/ meltfptr[14] = 0;
    /*^clear */
	   /*clear *//*_.CURPAIR__V16*/ meltfptr[15] = 0;
    /*^clear */
	   /*clear *//*_.QUOTED__V17*/ meltfptr[16] = 0;
    /*^clear */
	   /*clear *//*_.PAIR_TAIL__V18*/ meltfptr[17] = 0;
    /*^clear */
	   /*clear *//*_#IS_A__L4*/ meltfnum[0] = 0;
    /*^clear */
	   /*clear *//*_.IFELSE___V19*/ meltfptr[18] = 0;
    /*^clear */
	   /*clear *//*_#IS_A__L7*/ meltfnum[5] = 0;
    /*^clear */
	   /*clear *//*_.IF___V22*/ meltfptr[20] = 0;
    /*^clear */
	   /*clear *//*_.LET___V24*/ meltfptr[19] = 0;
    MELT_LOCATION ("warmelt-macro.melt:5773:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.LET___V12*/ meltfptr[10];;

    {
      MELT_LOCATION ("warmelt-macro.melt:5773:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V6*/ meltfptr[5] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V8*/ meltfptr[6] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V10*/ meltfptr[8] = 0;
    /*^clear */
	   /*clear *//*_.LET___V12*/ meltfptr[10] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("MEXPAND_QUOTE", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_121_warmelt_macro_MEXPAND_QUOTE_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_121_warmelt_macro_MEXPAND_QUOTE */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_122_warmelt_macro_MEXPAND_COMMENT (meltclosure_ptr_t meltclosp_,
					    melt_ptr_t meltfirstargp_,
					    const melt_argdescr_cell_t
					    meltxargdescr_[],
					    union meltparam_un * meltxargtab_,
					    const melt_argdescr_cell_t
					    meltxresdescr_[],
					    union meltparam_un * meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_122_warmelt_macro_MEXPAND_COMMENT_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_122_warmelt_macro_MEXPAND_COMMENT_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 25
    melt_ptr_t mcfr_varptr[25];
#define MELTFRAM_NBVARNUM 5
    long mcfr_varnum[5];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_122_warmelt_macro_MEXPAND_COMMENT is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_122_warmelt_macro_MEXPAND_COMMENT_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 25; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_122_warmelt_macro_MEXPAND_COMMENT nbval 25*/
  meltfram__.mcfr_nbvar = 25 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("MEXPAND_COMMENT", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-macro.melt:5812:/ getarg");
 /*_.SEXPR__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.ENV__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.ENV__V3*/ meltfptr[2])) != NULL);


  /*getarg#2 */
  /*^getarg */
  if (meltxargdescr_[1] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.MEXPANDER__V4*/ meltfptr[3] =
    (meltxargtab_[1].meltbp_aptr) ? (*(meltxargtab_[1].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.MEXPANDER__V4*/ meltfptr[3])) !=
	      NULL);


  /*getarg#3 */
  /*^getarg */
  if (meltxargdescr_[2] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.MODCTX__V5*/ meltfptr[4] =
    (meltxargtab_[2].meltbp_aptr) ? (*(meltxargtab_[2].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.MODCTX__V5*/ meltfptr[4])) !=
	      NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-macro.melt:5813:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L1*/ meltfnum[0] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.SEXPR__V2*/ meltfptr[1]),
			     (melt_ptr_t) (( /*!CLASS_SEXPR */ meltfrout->
					    tabval[0])));;
      MELT_LOCATION ("warmelt-macro.melt:5813:/ cond");
      /*cond */ if ( /*_#IS_A__L1*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V7*/ meltfptr[6] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-macro.melt:5813:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check sexpr"),
				  ("warmelt-macro.melt")
				  ? ("warmelt-macro.melt") : __FILE__,
				  (5813) ? (5813) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V7*/ meltfptr[6] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V6*/ meltfptr[5] = /*_.IFELSE___V7*/ meltfptr[6];;
      /*epilog */

      MELT_LOCATION ("warmelt-macro.melt:5813:/ clear");
	     /*clear *//*_#IS_A__L1*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V7*/ meltfptr[6] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V6*/ meltfptr[5] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-macro.melt:5814:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L2*/ meltfnum[0] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.ENV__V3*/ meltfptr[2]),
			     (melt_ptr_t) (( /*!CLASS_ENVIRONMENT */
					    meltfrout->tabval[1])));;
      MELT_LOCATION ("warmelt-macro.melt:5814:/ cond");
      /*cond */ if ( /*_#IS_A__L2*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V9*/ meltfptr[8] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-macro.melt:5814:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check env"),
				  ("warmelt-macro.melt")
				  ? ("warmelt-macro.melt") : __FILE__,
				  (5814) ? (5814) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V9*/ meltfptr[8] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V8*/ meltfptr[6] = /*_.IFELSE___V9*/ meltfptr[8];;
      /*epilog */

      MELT_LOCATION ("warmelt-macro.melt:5814:/ clear");
	     /*clear *//*_#IS_A__L2*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V9*/ meltfptr[8] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V8*/ meltfptr[6] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-macro.melt:5815:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_OBJECT__L3*/ meltfnum[0] =
	(melt_magic_discr ((melt_ptr_t) ( /*_.MODCTX__V5*/ meltfptr[4])) ==
	 MELTOBMAG_OBJECT);;
      MELT_LOCATION ("warmelt-macro.melt:5815:/ cond");
      /*cond */ if ( /*_#IS_OBJECT__L3*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V11*/ meltfptr[10] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-macro.melt:5815:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check modctx"),
				  ("warmelt-macro.melt")
				  ? ("warmelt-macro.melt") : __FILE__,
				  (5815) ? (5815) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V11*/ meltfptr[10] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V10*/ meltfptr[8] = /*_.IFELSE___V11*/ meltfptr[10];;
      /*epilog */

      MELT_LOCATION ("warmelt-macro.melt:5815:/ clear");
	     /*clear *//*_#IS_OBJECT__L3*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V11*/ meltfptr[10] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V10*/ meltfptr[8] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-macro.melt:5816:/ quasiblock");


    /*^getslot */
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.SEXPR__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 2, "SEXP_CONTENTS");
  /*_.CONT__V13*/ meltfptr[12] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-macro.melt:5817:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.SEXPR__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 1, "LOCA_LOCATION");
  /*_.LOC__V14*/ meltfptr[13] = slot;
    };
    ;
 /*_.LIST_FIRST__V15*/ meltfptr[14] =
      (melt_list_first ((melt_ptr_t) ( /*_.CONT__V13*/ meltfptr[12])));;
    /*^compute */
 /*_.CURPAIR__V16*/ meltfptr[15] =
      (melt_pair_tail ((melt_ptr_t) ( /*_.LIST_FIRST__V15*/ meltfptr[14])));;
    /*^compute */
 /*_.COMSTR__V17*/ meltfptr[16] =
      (melt_pair_head ((melt_ptr_t) ( /*_.CURPAIR__V16*/ meltfptr[15])));;
    MELT_LOCATION ("warmelt-macro.melt:5821:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_.PAIR_TAIL__V18*/ meltfptr[17] =
      (melt_pair_tail ((melt_ptr_t) ( /*_.CURPAIR__V16*/ meltfptr[15])));;
    MELT_LOCATION ("warmelt-macro.melt:5821:/ cond");
    /*cond */ if ( /*_.PAIR_TAIL__V18*/ meltfptr[17])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{


	  {
	    MELT_LOCATION ("warmelt-macro.melt:5822:/ locexp");
	    melt_warning_str (0, (melt_ptr_t) ( /*_.LOC__V14*/ meltfptr[13]),
			      ("COMMENT should have only one string argument"),
			      (melt_ptr_t) 0);
	  }
	  ;
	  /*epilog */
	}
	;
      }				/*noelse */
    ;
    MELT_LOCATION ("warmelt-macro.melt:5823:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#IS_STRING__L4*/ meltfnum[0] =
      (melt_magic_discr ((melt_ptr_t) ( /*_.COMSTR__V17*/ meltfptr[16])) ==
       MELTOBMAG_STRING);;
    /*^compute */
 /*_#NOT__L5*/ meltfnum[4] =
      (!( /*_#IS_STRING__L4*/ meltfnum[0]));;
    MELT_LOCATION ("warmelt-macro.melt:5823:/ cond");
    /*cond */ if ( /*_#NOT__L5*/ meltfnum[4])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{


	  {
	    MELT_LOCATION ("warmelt-macro.melt:5825:/ locexp");
	    melt_warning_str (0, (melt_ptr_t) ( /*_.LOC__V14*/ meltfptr[13]),
			      ("COMMENT without string is ignored"),
			      (melt_ptr_t) 0);
	  }
	  ;
	  MELT_LOCATION ("warmelt-macro.melt:5826:/ quasiblock");


   /*_.RETVAL___V1*/ meltfptr[0] = NULL;;

	  {
	    MELT_LOCATION ("warmelt-macro.melt:5826:/ locexp");
	    /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
	    if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	      melt_warn_for_no_expected_secondary_results ();
	    /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
	    ;
	  }
	  ;
	  /*^finalreturn */
	  ;
	  /*finalret */ goto labend_rout;
	  MELT_LOCATION ("warmelt-macro.melt:5824:/ quasiblock");


	  /*_.PROGN___V21*/ meltfptr[20] = /*_.RETURN___V20*/ meltfptr[19];;
	  /*^compute */
	  /*_.IF___V19*/ meltfptr[18] = /*_.PROGN___V21*/ meltfptr[20];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-macro.melt:5823:/ clear");
	     /*clear *//*_.RETURN___V20*/ meltfptr[19] = 0;
	  /*^clear */
	     /*clear *//*_.PROGN___V21*/ meltfptr[20] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

  /*_.IF___V19*/ meltfptr[18] = NULL;;
      }
    ;
    MELT_LOCATION ("warmelt-macro.melt:5829:/ quasiblock");


    /*^checksignal */
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*^rawallocobj */
    /*rawallocobj */
    {
      melt_ptr_t newobj = 0;
      melt_raw_object_create (newobj,
			      (melt_ptr_t) (( /*!CLASS_SOURCE_COMMENT */
					     meltfrout->tabval[2])), (3),
			      "CLASS_SOURCE_COMMENT");
  /*_.INST__V24*/ meltfptr[23] =
	newobj;
    };
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @LOCA_LOCATION",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V24*/ meltfptr[23])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V24*/ meltfptr[23]), (1),
			  ( /*_.LOC__V14*/ meltfptr[13]), "LOCA_LOCATION");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @SCOMM_STR",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V24*/ meltfptr[23])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V24*/ meltfptr[23]), (2),
			  ( /*_.COMSTR__V17*/ meltfptr[16]), "SCOMM_STR");
    ;
    /*^touchobj */

    melt_dbgtrace_written_object ( /*_.INST__V24*/ meltfptr[23],
				  "newly made instance");
    ;
    /*_.SCOM__V23*/ meltfptr[20] = /*_.INST__V24*/ meltfptr[23];;
    MELT_LOCATION ("warmelt-macro.melt:5832:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.SCOM__V23*/ meltfptr[20];;

    {
      MELT_LOCATION ("warmelt-macro.melt:5832:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*_.LET___V22*/ meltfptr[19] = /*_.RETURN___V25*/ meltfptr[24];;

    MELT_LOCATION ("warmelt-macro.melt:5829:/ clear");
	   /*clear *//*_.SCOM__V23*/ meltfptr[20] = 0;
    /*^clear */
	   /*clear *//*_.RETURN___V25*/ meltfptr[24] = 0;
    /*_.LET___V12*/ meltfptr[10] = /*_.LET___V22*/ meltfptr[19];;

    MELT_LOCATION ("warmelt-macro.melt:5816:/ clear");
	   /*clear *//*_.CONT__V13*/ meltfptr[12] = 0;
    /*^clear */
	   /*clear *//*_.LOC__V14*/ meltfptr[13] = 0;
    /*^clear */
	   /*clear *//*_.LIST_FIRST__V15*/ meltfptr[14] = 0;
    /*^clear */
	   /*clear *//*_.CURPAIR__V16*/ meltfptr[15] = 0;
    /*^clear */
	   /*clear *//*_.COMSTR__V17*/ meltfptr[16] = 0;
    /*^clear */
	   /*clear *//*_.PAIR_TAIL__V18*/ meltfptr[17] = 0;
    /*^clear */
	   /*clear *//*_#IS_STRING__L4*/ meltfnum[0] = 0;
    /*^clear */
	   /*clear *//*_#NOT__L5*/ meltfnum[4] = 0;
    /*^clear */
	   /*clear *//*_.IF___V19*/ meltfptr[18] = 0;
    /*^clear */
	   /*clear *//*_.LET___V22*/ meltfptr[19] = 0;
    MELT_LOCATION ("warmelt-macro.melt:5812:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.LET___V12*/ meltfptr[10];;

    {
      MELT_LOCATION ("warmelt-macro.melt:5812:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V6*/ meltfptr[5] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V8*/ meltfptr[6] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V10*/ meltfptr[8] = 0;
    /*^clear */
	   /*clear *//*_.LET___V12*/ meltfptr[10] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("MEXPAND_COMMENT", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_122_warmelt_macro_MEXPAND_COMMENT_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_122_warmelt_macro_MEXPAND_COMMENT */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_123_warmelt_macro_MEXPAND_CHEADER (meltclosure_ptr_t meltclosp_,
					    melt_ptr_t meltfirstargp_,
					    const melt_argdescr_cell_t
					    meltxargdescr_[],
					    union meltparam_un * meltxargtab_,
					    const melt_argdescr_cell_t
					    meltxresdescr_[],
					    union meltparam_un * meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_123_warmelt_macro_MEXPAND_CHEADER_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_123_warmelt_macro_MEXPAND_CHEADER_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 49
    melt_ptr_t mcfr_varptr[49];
#define MELTFRAM_NBVARNUM 11
    long mcfr_varnum[11];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_123_warmelt_macro_MEXPAND_CHEADER is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_123_warmelt_macro_MEXPAND_CHEADER_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 49; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_123_warmelt_macro_MEXPAND_CHEADER nbval 49*/
  meltfram__.mcfr_nbvar = 49 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("MEXPAND_CHEADER", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-macro.melt:5842:/ getarg");
 /*_.SEXPR__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.ENV__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.ENV__V3*/ meltfptr[2])) != NULL);


  /*getarg#2 */
  /*^getarg */
  if (meltxargdescr_[1] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.MEXPANDER__V4*/ meltfptr[3] =
    (meltxargtab_[1].meltbp_aptr) ? (*(meltxargtab_[1].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.MEXPANDER__V4*/ meltfptr[3])) !=
	      NULL);


  /*getarg#3 */
  /*^getarg */
  if (meltxargdescr_[2] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.MODCTX__V5*/ meltfptr[4] =
    (meltxargtab_[2].meltbp_aptr) ? (*(meltxargtab_[2].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.MODCTX__V5*/ meltfptr[4])) !=
	      NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-macro.melt:5843:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L1*/ meltfnum[0] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-macro.melt:5843:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L1*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-macro.melt:5843:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-macro.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 5843;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "mexpand_cheader sexpr=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.SEXPR__V2*/ meltfptr[1];
	      /*_.MELT_DEBUG_FUN__V8*/ meltfptr[7] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V7*/ meltfptr[6] = /*_.MELT_DEBUG_FUN__V8*/ meltfptr[7];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-macro.melt:5843:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V8*/ meltfptr[7] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V7*/ meltfptr[6] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-macro.melt:5843:/ quasiblock");


      /*_.PROGN___V9*/ meltfptr[7] = /*_.IF___V7*/ meltfptr[6];;
      /*^compute */
      /*_.IFCPP___V6*/ meltfptr[5] = /*_.PROGN___V9*/ meltfptr[7];;
      /*epilog */

      MELT_LOCATION ("warmelt-macro.melt:5843:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L1*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IF___V7*/ meltfptr[6] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V9*/ meltfptr[7] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V6*/ meltfptr[5] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-macro.melt:5844:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L3*/ meltfnum[1] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.SEXPR__V2*/ meltfptr[1]),
			     (melt_ptr_t) (( /*!CLASS_SEXPR */ meltfrout->
					    tabval[1])));;
      MELT_LOCATION ("warmelt-macro.melt:5844:/ cond");
      /*cond */ if ( /*_#IS_A__L3*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V11*/ meltfptr[7] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-macro.melt:5844:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check sexpr"),
				  ("warmelt-macro.melt")
				  ? ("warmelt-macro.melt") : __FILE__,
				  (5844) ? (5844) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V11*/ meltfptr[7] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V10*/ meltfptr[6] = /*_.IFELSE___V11*/ meltfptr[7];;
      /*epilog */

      MELT_LOCATION ("warmelt-macro.melt:5844:/ clear");
	     /*clear *//*_#IS_A__L3*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V11*/ meltfptr[7] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V10*/ meltfptr[6] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-macro.melt:5845:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L4*/ meltfnum[0] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.ENV__V3*/ meltfptr[2]),
			     (melt_ptr_t) (( /*!CLASS_ENVIRONMENT */
					    meltfrout->tabval[2])));;
      MELT_LOCATION ("warmelt-macro.melt:5845:/ cond");
      /*cond */ if ( /*_#IS_A__L4*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V13*/ meltfptr[12] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-macro.melt:5845:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check env"),
				  ("warmelt-macro.melt")
				  ? ("warmelt-macro.melt") : __FILE__,
				  (5845) ? (5845) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V13*/ meltfptr[12] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V12*/ meltfptr[7] = /*_.IFELSE___V13*/ meltfptr[12];;
      /*epilog */

      MELT_LOCATION ("warmelt-macro.melt:5845:/ clear");
	     /*clear *//*_#IS_A__L4*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V13*/ meltfptr[12] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V12*/ meltfptr[7] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-macro.melt:5846:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_OBJECT__L5*/ meltfnum[1] =
	(melt_magic_discr ((melt_ptr_t) ( /*_.MODCTX__V5*/ meltfptr[4])) ==
	 MELTOBMAG_OBJECT);;
      MELT_LOCATION ("warmelt-macro.melt:5846:/ cond");
      /*cond */ if ( /*_#IS_OBJECT__L5*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V15*/ meltfptr[14] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-macro.melt:5846:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check modctx"),
				  ("warmelt-macro.melt")
				  ? ("warmelt-macro.melt") : __FILE__,
				  (5846) ? (5846) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V15*/ meltfptr[14] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V14*/ meltfptr[12] = /*_.IFELSE___V15*/ meltfptr[14];;
      /*epilog */

      MELT_LOCATION ("warmelt-macro.melt:5846:/ clear");
	     /*clear *//*_#IS_OBJECT__L5*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V15*/ meltfptr[14] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V14*/ meltfptr[12] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-macro.melt:5847:/ quasiblock");


    /*^getslot */
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.SEXPR__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 2, "SEXP_CONTENTS");
  /*_.CONT__V17*/ meltfptr[16] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-macro.melt:5848:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.SEXPR__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 1, "LOCA_LOCATION");
  /*_.LOC__V18*/ meltfptr[17] = slot;
    };
    ;
 /*_.LIST_FIRST__V19*/ meltfptr[18] =
      (melt_list_first ((melt_ptr_t) ( /*_.CONT__V17*/ meltfptr[16])));;
    /*^compute */
 /*_.CURPAIR__V20*/ meltfptr[19] =
      (melt_pair_tail ((melt_ptr_t) ( /*_.LIST_FIRST__V19*/ meltfptr[18])));;
    /*^compute */
 /*_.CHEAD__V21*/ meltfptr[20] =
      (melt_pair_head ((melt_ptr_t) ( /*_.CURPAIR__V20*/ meltfptr[19])));;
    MELT_LOCATION ("warmelt-macro.melt:5852:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_.PAIR_TAIL__V22*/ meltfptr[21] =
      (melt_pair_tail ((melt_ptr_t) ( /*_.CURPAIR__V20*/ meltfptr[19])));;
    MELT_LOCATION ("warmelt-macro.melt:5852:/ cond");
    /*cond */ if ( /*_.PAIR_TAIL__V22*/ meltfptr[21])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{


	  {
	    MELT_LOCATION ("warmelt-macro.melt:5853:/ locexp");
	    melt_warning_str (0, (melt_ptr_t) ( /*_.LOC__V18*/ meltfptr[17]),
			      ("CHEADER should have only one argument"),
			      (melt_ptr_t) 0);
	  }
	  ;
	  /*epilog */
	}
	;
      }				/*noelse */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-macro.melt:5854:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L6*/ meltfnum[0] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-macro.melt:5854:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L6*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L7*/ meltfnum[1] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-macro.melt:5854:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L7*/ meltfnum[1];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-macro.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 5854;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "mexpand_cheader chead=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.CHEAD__V21*/ meltfptr[20];
	      /*_.MELT_DEBUG_FUN__V25*/ meltfptr[24] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V24*/ meltfptr[23] =
	      /*_.MELT_DEBUG_FUN__V25*/ meltfptr[24];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-macro.melt:5854:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L7*/ meltfnum[1] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V25*/ meltfptr[24] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V24*/ meltfptr[23] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-macro.melt:5854:/ quasiblock");


      /*_.PROGN___V26*/ meltfptr[24] = /*_.IF___V24*/ meltfptr[23];;
      /*^compute */
      /*_.IFCPP___V23*/ meltfptr[22] = /*_.PROGN___V26*/ meltfptr[24];;
      /*epilog */

      MELT_LOCATION ("warmelt-macro.melt:5854:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L6*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IF___V24*/ meltfptr[23] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V26*/ meltfptr[24] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V23*/ meltfptr[22] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-macro.melt:5856:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#IS_STRING__L8*/ meltfnum[1] =
      (melt_magic_discr ((melt_ptr_t) ( /*_.CHEAD__V21*/ meltfptr[20])) ==
       MELTOBMAG_STRING);;
    MELT_LOCATION ("warmelt-macro.melt:5856:/ cond");
    /*cond */ if ( /*_#IS_STRING__L8*/ meltfnum[1])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{




	  {
	    MELT_LOCATION ("warmelt-macro.melt:5857:/ locexp");
	    /*void */ (void) 0;
	  }
	  ;
	     /*clear *//*_.IFELSE___V27*/ meltfptr[23] = 0;
	  /*epilog */
	}
	;
      }
    else
      {
	MELT_LOCATION ("warmelt-macro.melt:5856:/ cond.else");

	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-macro.melt:5859:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
   /*_#IS_A__L9*/ meltfnum[0] =
	    melt_is_instance_of ((melt_ptr_t)
				 ( /*_.CHEAD__V21*/ meltfptr[20]),
				 (melt_ptr_t) (( /*!CLASS_SEXPR_MACROSTRING */
						meltfrout->tabval[3])));;
	  MELT_LOCATION ("warmelt-macro.melt:5859:/ cond");
	  /*cond */ if ( /*_#IS_A__L9*/ meltfnum[0])	/*then */
	    {
	      /*^cond.then */
	      /*^block */
	      /*anyblock */
	      {

		MELT_LOCATION ("warmelt-macro.melt:5860:/ quasiblock");


     /*_.SBUF__V30*/ meltfptr[29] =
		  (melt_ptr_t)
		  meltgc_new_strbuf ((meltobject_ptr_t)
				     (( /*!DISCR_STRBUF */ meltfrout->
				       tabval[4])), (const char *) 0);;
		MELT_LOCATION ("warmelt-macro.melt:5861:/ cond");
		/*cond */ if (
			       /*ifisa */
			       melt_is_instance_of ((melt_ptr_t)
						    ( /*_.CHEAD__V21*/
						     meltfptr[20]),
						    (melt_ptr_t) (( /*!CLASS_SEXPR */ meltfrout->tabval[1])))
		  )		/*then */
		  {
		    /*^cond.then */
		    /*^getslot */
		    {
		      melt_ptr_t slot = NULL, obj = NULL;
		      obj =
			(melt_ptr_t) ( /*_.CHEAD__V21*/ meltfptr[20]) /*=obj*/
			;
		      melt_object_get_field (slot, obj, 2, "SEXP_CONTENTS");
       /*_.SCONT__V31*/ meltfptr[30] = slot;
		    };
		    ;
		  }
		else
		  {		/*^cond.else */

      /*_.SCONT__V31*/ meltfptr[30] = NULL;;
		  }
		;
		MELT_LOCATION ("warmelt-macro.melt:5862:/ cond");
		/*cond */ if (
			       /*ifisa */
			       melt_is_instance_of ((melt_ptr_t)
						    ( /*_.CHEAD__V21*/
						     meltfptr[20]),
						    (melt_ptr_t) (( /*!CLASS_LOCATED */ meltfrout->tabval[5])))
		  )		/*then */
		  {
		    /*^cond.then */
		    /*^getslot */
		    {
		      melt_ptr_t slot = NULL, obj = NULL;
		      obj =
			(melt_ptr_t) ( /*_.CHEAD__V21*/ meltfptr[20]) /*=obj*/
			;
		      melt_object_get_field (slot, obj, 1, "LOCA_LOCATION");
       /*_.SLOC__V32*/ meltfptr[31] = slot;
		    };
		    ;
		  }
		else
		  {		/*^cond.else */

      /*_.SLOC__V32*/ meltfptr[31] = NULL;;
		  }
		;
		MELT_LOCATION ("warmelt-macro.melt:5864:/ checksignal");
		MELT_CHECK_SIGNAL ();
		;
		/*^cond */
		/*cond */ if ( /*_.SLOC__V32*/ meltfptr[31])	/*then */
		  {
		    /*^cond.then */
		    /*^block */
		    /*anyblock */
		    {

		      /*^compute */
		      /*_.LOC__V18*/ meltfptr[17] =
			/*_.SETQ___V34*/ meltfptr[33] =
			/*_.SLOC__V32*/ meltfptr[31];;
		      /*_.IF___V33*/ meltfptr[32] =
			/*_.SETQ___V34*/ meltfptr[33];;
		      /*epilog */

		      MELT_LOCATION ("warmelt-macro.melt:5864:/ clear");
		 /*clear *//*_.SETQ___V34*/ meltfptr[33] = 0;
		    }
		    ;
		  }
		else
		  {		/*^cond.else */

      /*_.IF___V33*/ meltfptr[32] = NULL;;
		  }
		;
		/*citerblock FOREACH_IN_LIST */
		{
		  /* start foreach_in_list meltcit1__EACHLIST */
		  for ( /*_.CURPAIR__V35*/ meltfptr[33] =
		       melt_list_first ((melt_ptr_t) /*_.SCONT__V31*/
					meltfptr[30]);
		       melt_magic_discr ((melt_ptr_t) /*_.CURPAIR__V35*/
					 meltfptr[33]) == MELTOBMAG_PAIR;
		       /*_.CURPAIR__V35*/ meltfptr[33] =
		       melt_pair_tail ((melt_ptr_t) /*_.CURPAIR__V35*/
				       meltfptr[33]))
		    {
		      /*_.CURARG__V36*/ meltfptr[35] =
			melt_pair_head ((melt_ptr_t) /*_.CURPAIR__V35*/
					meltfptr[33]);


		      MELT_LOCATION ("warmelt-macro.melt:5868:/ checksignal");
		      MELT_CHECK_SIGNAL ();
		      ;
		      /*^apply */
		      /*apply */
		      {
			union meltparam_un argtab[1];
			memset (&argtab, 0, sizeof (argtab));
			/*^apply.arg */
			argtab[0].meltbp_aptr =
			  (melt_ptr_t *) & /*_.CURARG__V36*/ meltfptr[35];
			/*_.ADD2OUT__V37*/ meltfptr[36] =
			  melt_apply ((meltclosure_ptr_t)
				      (( /*!ADD2OUT */ meltfrout->tabval[6])),
				      (melt_ptr_t) ( /*_.SBUF__V30*/
						    meltfptr[29]),
				      (MELTBPARSTR_PTR ""), argtab, "",
				      (union meltparam_un *) 0);
		      }
		      ;
		    }		/* end foreach_in_list meltcit1__EACHLIST */
     /*_.CURPAIR__V35*/ meltfptr[33] = NULL;
     /*_.CURARG__V36*/ meltfptr[35] = NULL;


		  /*citerepilog */

		  MELT_LOCATION ("warmelt-macro.melt:5865:/ clear");
		/*clear *//*_.CURPAIR__V35*/ meltfptr[33] = 0;
		  /*^clear */
		/*clear *//*_.CURARG__V36*/ meltfptr[35] = 0;
		  /*^clear */
		/*clear *//*_.ADD2OUT__V37*/ meltfptr[36] = 0;
		}		/*endciterblock FOREACH_IN_LIST */
		;
     /*_.STRBUF2STRING__V38*/ meltfptr[37] =
		  (meltgc_new_stringdup
		   ((meltobject_ptr_t)
		    (( /*!DISCR_VERBATIM_STRING */ meltfrout->tabval[7])),
		    melt_strbuf_str ((melt_ptr_t)
				     ( /*_.SBUF__V30*/ meltfptr[29]))));;
		MELT_LOCATION ("warmelt-macro.melt:5870:/ compute");
		/*_.CHEAD__V21*/ meltfptr[20] =
		  /*_.SETQ___V39*/ meltfptr[38] =
		  /*_.STRBUF2STRING__V38*/ meltfptr[37];;
		/*_.LET___V29*/ meltfptr[28] = /*_.SETQ___V39*/ meltfptr[38];;

		MELT_LOCATION ("warmelt-macro.melt:5860:/ clear");
	       /*clear *//*_.SBUF__V30*/ meltfptr[29] = 0;
		/*^clear */
	       /*clear *//*_.SCONT__V31*/ meltfptr[30] = 0;
		/*^clear */
	       /*clear *//*_.SLOC__V32*/ meltfptr[31] = 0;
		/*^clear */
	       /*clear *//*_.IF___V33*/ meltfptr[32] = 0;
		/*^clear */
	       /*clear *//*_.STRBUF2STRING__V38*/ meltfptr[37] = 0;
		/*^clear */
	       /*clear *//*_.SETQ___V39*/ meltfptr[38] = 0;
		/*_.IFELSE___V28*/ meltfptr[24] =
		  /*_.LET___V29*/ meltfptr[28];;
		/*epilog */

		MELT_LOCATION ("warmelt-macro.melt:5859:/ clear");
	       /*clear *//*_.LET___V29*/ meltfptr[28] = 0;
	      }
	      ;
	    }
	  else
	    {			/*^cond.else */

	      /*^block */
	      /*anyblock */
	      {


		{
		  MELT_LOCATION ("warmelt-macro.melt:5873:/ locexp");
		  /* error_plain */
		    melt_error_str ((melt_ptr_t)
				    ( /*_.LOC__V18*/ meltfptr[17]),
				    ("CHEADER without string or macrostring"),
				    (melt_ptr_t) 0);
		}
		;
		MELT_LOCATION ("warmelt-macro.melt:5874:/ quasiblock");


     /*_.RETVAL___V1*/ meltfptr[0] = NULL;;

		{
		  MELT_LOCATION ("warmelt-macro.melt:5874:/ locexp");
		  /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
		  if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
		    melt_warn_for_no_expected_secondary_results ();
		  /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
		  ;
		}
		;
		/*^finalreturn */
		;
		/*finalret */ goto labend_rout;
		MELT_LOCATION ("warmelt-macro.melt:5872:/ quasiblock");


		/*_.PROGN___V41*/ meltfptr[30] =
		  /*_.RETURN___V40*/ meltfptr[29];;
		/*^compute */
		/*_.IFELSE___V28*/ meltfptr[24] =
		  /*_.PROGN___V41*/ meltfptr[30];;
		/*epilog */

		MELT_LOCATION ("warmelt-macro.melt:5859:/ clear");
	       /*clear *//*_.RETURN___V40*/ meltfptr[29] = 0;
		/*^clear */
	       /*clear *//*_.PROGN___V41*/ meltfptr[30] = 0;
	      }
	      ;
	    }
	  ;
	  /*_.IFELSE___V27*/ meltfptr[23] = /*_.IFELSE___V28*/ meltfptr[24];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-macro.melt:5856:/ clear");
	     /*clear *//*_#IS_A__L9*/ meltfnum[0] = 0;
	  /*^clear */
	     /*clear *//*_.IFELSE___V28*/ meltfptr[24] = 0;
	}
	;
      }
    ;
    MELT_LOCATION ("warmelt-macro.melt:5877:/ quasiblock");


    /*^checksignal */
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*^rawallocobj */
    /*rawallocobj */
    {
      melt_ptr_t newobj = 0;
      melt_raw_object_create (newobj,
			      (melt_ptr_t) (( /*!CLASS_SOURCE_CHEADER */
					     meltfrout->tabval[8])), (3),
			      "CLASS_SOURCE_CHEADER");
  /*_.INST__V44*/ meltfptr[37] =
	newobj;
    };
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @LOCA_LOCATION",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V44*/ meltfptr[37])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V44*/ meltfptr[37]), (1),
			  ( /*_.LOC__V18*/ meltfptr[17]), "LOCA_LOCATION");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @SCHEADER_CODESTRING",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V44*/ meltfptr[37])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V44*/ meltfptr[37]), (2),
			  ( /*_.CHEAD__V21*/ meltfptr[20]),
			  "SCHEADER_CODESTRING");
    ;
    /*^touchobj */

    melt_dbgtrace_written_object ( /*_.INST__V44*/ meltfptr[37],
				  "newly made instance");
    ;
    /*_.SCH__V43*/ meltfptr[32] = /*_.INST__V44*/ meltfptr[37];;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-macro.melt:5880:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L10*/ meltfnum[0] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-macro.melt:5880:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L10*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L11*/ meltfnum[10] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-macro.melt:5880:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long =
		/*_#THE_MELTCALLCOUNT__L11*/ meltfnum[10];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-macro.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 5880;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "mexpand_cheader gives sch=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.SCH__V43*/ meltfptr[32];
	      /*_.MELT_DEBUG_FUN__V47*/ meltfptr[29] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V46*/ meltfptr[28] =
	      /*_.MELT_DEBUG_FUN__V47*/ meltfptr[29];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-macro.melt:5880:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L11*/ meltfnum[10] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V47*/ meltfptr[29] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V46*/ meltfptr[28] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-macro.melt:5880:/ quasiblock");


      /*_.PROGN___V48*/ meltfptr[30] = /*_.IF___V46*/ meltfptr[28];;
      /*^compute */
      /*_.IFCPP___V45*/ meltfptr[38] = /*_.PROGN___V48*/ meltfptr[30];;
      /*epilog */

      MELT_LOCATION ("warmelt-macro.melt:5880:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L10*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IF___V46*/ meltfptr[28] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V48*/ meltfptr[30] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V45*/ meltfptr[38] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-macro.melt:5881:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.SCH__V43*/ meltfptr[32];;

    {
      MELT_LOCATION ("warmelt-macro.melt:5881:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*_.LET___V42*/ meltfptr[31] = /*_.RETURN___V49*/ meltfptr[24];;

    MELT_LOCATION ("warmelt-macro.melt:5877:/ clear");
	   /*clear *//*_.SCH__V43*/ meltfptr[32] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V45*/ meltfptr[38] = 0;
    /*^clear */
	   /*clear *//*_.RETURN___V49*/ meltfptr[24] = 0;
    /*_.LET___V16*/ meltfptr[14] = /*_.LET___V42*/ meltfptr[31];;

    MELT_LOCATION ("warmelt-macro.melt:5847:/ clear");
	   /*clear *//*_.CONT__V17*/ meltfptr[16] = 0;
    /*^clear */
	   /*clear *//*_.LOC__V18*/ meltfptr[17] = 0;
    /*^clear */
	   /*clear *//*_.LIST_FIRST__V19*/ meltfptr[18] = 0;
    /*^clear */
	   /*clear *//*_.CURPAIR__V20*/ meltfptr[19] = 0;
    /*^clear */
	   /*clear *//*_.CHEAD__V21*/ meltfptr[20] = 0;
    /*^clear */
	   /*clear *//*_.PAIR_TAIL__V22*/ meltfptr[21] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V23*/ meltfptr[22] = 0;
    /*^clear */
	   /*clear *//*_#IS_STRING__L8*/ meltfnum[1] = 0;
    /*^clear */
	   /*clear *//*_.IFELSE___V27*/ meltfptr[23] = 0;
    /*^clear */
	   /*clear *//*_.LET___V42*/ meltfptr[31] = 0;
    MELT_LOCATION ("warmelt-macro.melt:5842:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.LET___V16*/ meltfptr[14];;

    {
      MELT_LOCATION ("warmelt-macro.melt:5842:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V6*/ meltfptr[5] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V10*/ meltfptr[6] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V12*/ meltfptr[7] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V14*/ meltfptr[12] = 0;
    /*^clear */
	   /*clear *//*_.LET___V16*/ meltfptr[14] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("MEXPAND_CHEADER", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_123_warmelt_macro_MEXPAND_CHEADER_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_123_warmelt_macro_MEXPAND_CHEADER */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_124_warmelt_macro_MEXPAND_USE_PACKAGE_FROM_PKG_CONFIG
  (meltclosure_ptr_t meltclosp_, melt_ptr_t meltfirstargp_,
   const melt_argdescr_cell_t meltxargdescr_[],
   union meltparam_un * meltxargtab_,
   const melt_argdescr_cell_t meltxresdescr_[],
   union meltparam_un * meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_124_warmelt_macro_MEXPAND_USE_PACKAGE_FROM_PKG_CONFIG_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_124_warmelt_macro_MEXPAND_USE_PACKAGE_FROM_PKG_CONFIG_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 55
    melt_ptr_t mcfr_varptr[55];
#define MELTFRAM_NBVARNUM 17
    long mcfr_varnum[17];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_124_warmelt_macro_MEXPAND_USE_PACKAGE_FROM_PKG_CONFIG is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct
	 frame_meltrout_124_warmelt_macro_MEXPAND_USE_PACKAGE_FROM_PKG_CONFIG_st
	 *) meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 55; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_124_warmelt_macro_MEXPAND_USE_PACKAGE_FROM_PKG_CONFIG nbval 55*/
  meltfram__.mcfr_nbvar = 55 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("MEXPAND_USE_PACKAGE_FROM_PKG_CONFIG", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-macro.melt:5891:/ getarg");
 /*_.SEXPR__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.ENV__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.ENV__V3*/ meltfptr[2])) != NULL);


  /*getarg#2 */
  /*^getarg */
  if (meltxargdescr_[1] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.MEXPANDER__V4*/ meltfptr[3] =
    (meltxargtab_[1].meltbp_aptr) ? (*(meltxargtab_[1].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.MEXPANDER__V4*/ meltfptr[3])) !=
	      NULL);


  /*getarg#3 */
  /*^getarg */
  if (meltxargdescr_[2] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.MODCTX__V5*/ meltfptr[4] =
    (meltxargtab_[2].meltbp_aptr) ? (*(meltxargtab_[2].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.MODCTX__V5*/ meltfptr[4])) !=
	      NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-macro.melt:5892:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L1*/ meltfnum[0] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-macro.melt:5892:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L1*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-macro.melt:5892:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-macro.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 5892;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring =
		"mexpand_use_package_from_pkg_config sexpr=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.SEXPR__V2*/ meltfptr[1];
	      /*_.MELT_DEBUG_FUN__V8*/ meltfptr[7] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V7*/ meltfptr[6] = /*_.MELT_DEBUG_FUN__V8*/ meltfptr[7];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-macro.melt:5892:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V8*/ meltfptr[7] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V7*/ meltfptr[6] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-macro.melt:5892:/ quasiblock");


      /*_.PROGN___V9*/ meltfptr[7] = /*_.IF___V7*/ meltfptr[6];;
      /*^compute */
      /*_.IFCPP___V6*/ meltfptr[5] = /*_.PROGN___V9*/ meltfptr[7];;
      /*epilog */

      MELT_LOCATION ("warmelt-macro.melt:5892:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L1*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IF___V7*/ meltfptr[6] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V9*/ meltfptr[7] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V6*/ meltfptr[5] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-macro.melt:5893:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L3*/ meltfnum[1] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.SEXPR__V2*/ meltfptr[1]),
			     (melt_ptr_t) (( /*!CLASS_SEXPR */ meltfrout->
					    tabval[1])));;
      MELT_LOCATION ("warmelt-macro.melt:5893:/ cond");
      /*cond */ if ( /*_#IS_A__L3*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V11*/ meltfptr[7] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-macro.melt:5893:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check sexpr"),
				  ("warmelt-macro.melt")
				  ? ("warmelt-macro.melt") : __FILE__,
				  (5893) ? (5893) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V11*/ meltfptr[7] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V10*/ meltfptr[6] = /*_.IFELSE___V11*/ meltfptr[7];;
      /*epilog */

      MELT_LOCATION ("warmelt-macro.melt:5893:/ clear");
	     /*clear *//*_#IS_A__L3*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V11*/ meltfptr[7] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V10*/ meltfptr[6] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-macro.melt:5894:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L4*/ meltfnum[0] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.ENV__V3*/ meltfptr[2]),
			     (melt_ptr_t) (( /*!CLASS_ENVIRONMENT */
					    meltfrout->tabval[2])));;
      MELT_LOCATION ("warmelt-macro.melt:5894:/ cond");
      /*cond */ if ( /*_#IS_A__L4*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V13*/ meltfptr[12] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-macro.melt:5894:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check env"),
				  ("warmelt-macro.melt")
				  ? ("warmelt-macro.melt") : __FILE__,
				  (5894) ? (5894) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V13*/ meltfptr[12] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V12*/ meltfptr[7] = /*_.IFELSE___V13*/ meltfptr[12];;
      /*epilog */

      MELT_LOCATION ("warmelt-macro.melt:5894:/ clear");
	     /*clear *//*_#IS_A__L4*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V13*/ meltfptr[12] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V12*/ meltfptr[7] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-macro.melt:5895:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_OBJECT__L5*/ meltfnum[1] =
	(melt_magic_discr ((melt_ptr_t) ( /*_.MODCTX__V5*/ meltfptr[4])) ==
	 MELTOBMAG_OBJECT);;
      MELT_LOCATION ("warmelt-macro.melt:5895:/ cond");
      /*cond */ if ( /*_#IS_OBJECT__L5*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V15*/ meltfptr[14] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-macro.melt:5895:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check modctx"),
				  ("warmelt-macro.melt")
				  ? ("warmelt-macro.melt") : __FILE__,
				  (5895) ? (5895) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V15*/ meltfptr[14] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V14*/ meltfptr[12] = /*_.IFELSE___V15*/ meltfptr[14];;
      /*epilog */

      MELT_LOCATION ("warmelt-macro.melt:5895:/ clear");
	     /*clear *//*_#IS_OBJECT__L5*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V15*/ meltfptr[14] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V14*/ meltfptr[12] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-macro.melt:5896:/ quasiblock");


    /*^getslot */
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.SEXPR__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 2, "SEXP_CONTENTS");
  /*_.CONT__V17*/ meltfptr[16] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-macro.melt:5897:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.SEXPR__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 1, "LOCA_LOCATION");
  /*_.LOC__V18*/ meltfptr[17] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-macro.melt:5898:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[3];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.ENV__V3*/ meltfptr[2];
      /*^apply.arg */
      argtab[1].meltbp_aptr =
	(melt_ptr_t *) & /*_.MEXPANDER__V4*/ meltfptr[3];
      /*^apply.arg */
      argtab[2].meltbp_aptr = (melt_ptr_t *) & /*_.MODCTX__V5*/ meltfptr[4];
      /*_.XARGTUP__V19*/ meltfptr[18] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!EXPAND_RESTLIST_AS_TUPLE */ meltfrout->tabval[3])),
		    (melt_ptr_t) ( /*_.CONT__V17*/ meltfptr[16]),
		    (MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_PTR ""),
		    argtab, "", (union meltparam_un *) 0);
    }
    ;
 /*_.CMDBUF__V20*/ meltfptr[19] =
      (melt_ptr_t)
      meltgc_new_strbuf ((meltobject_ptr_t)
			 (( /*!DISCR_STRBUF */ meltfrout->tabval[4])),
			 (const char *) 0);;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-macro.melt:5901:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L6*/ meltfnum[0] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-macro.melt:5901:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L6*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L7*/ meltfnum[1] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-macro.melt:5901:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L7*/ meltfnum[1];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-macro.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 5901;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring =
		"mexpand_use_package_from_pkg_config xargtup=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.XARGTUP__V19*/ meltfptr[18];
	      /*_.MELT_DEBUG_FUN__V23*/ meltfptr[22] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V22*/ meltfptr[21] =
	      /*_.MELT_DEBUG_FUN__V23*/ meltfptr[22];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-macro.melt:5901:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L7*/ meltfnum[1] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V23*/ meltfptr[22] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V22*/ meltfptr[21] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-macro.melt:5901:/ quasiblock");


      /*_.PROGN___V24*/ meltfptr[22] = /*_.IF___V22*/ meltfptr[21];;
      /*^compute */
      /*_.IFCPP___V21*/ meltfptr[20] = /*_.PROGN___V24*/ meltfptr[22];;
      /*epilog */

      MELT_LOCATION ("warmelt-macro.melt:5901:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L6*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IF___V22*/ meltfptr[21] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V24*/ meltfptr[22] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V21*/ meltfptr[20] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-macro.melt:5902:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_cstring = "pkg-config --exists";
      /*_.ADD2OUT__V25*/ meltfptr[21] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!ADD2OUT */ meltfrout->tabval[5])),
		    (melt_ptr_t) ( /*_.CMDBUF__V20*/ meltfptr[19]),
		    (MELTBPARSTR_CSTRING ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;
    /*citerblock FOREACH_IN_MULTIPLE */
    {
      /* start foreach_in_multiple meltcit1__EACHTUP */
      long meltcit1__EACHTUP_ln =
	melt_multiple_length ((melt_ptr_t) /*_.XARGTUP__V19*/ meltfptr[18]);
      for ( /*_#PKGIX__L8*/ meltfnum[1] = 0;
	   ( /*_#PKGIX__L8*/ meltfnum[1] >= 0)
	   && ( /*_#PKGIX__L8*/ meltfnum[1] < meltcit1__EACHTUP_ln);
	/*_#PKGIX__L8*/ meltfnum[1]++)
	{
	  /*_.CURPKGNAME__V26*/ meltfptr[22] =
	    melt_multiple_nth ((melt_ptr_t)
			       ( /*_.XARGTUP__V19*/ meltfptr[18]),
			       /*_#PKGIX__L8*/ meltfnum[1]);



	  MELT_LOCATION ("warmelt-macro.melt:5906:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
  /*_#IS_STRING__L9*/ meltfnum[0] =
	    (melt_magic_discr
	     ((melt_ptr_t) ( /*_.CURPKGNAME__V26*/ meltfptr[22])) ==
	     MELTOBMAG_STRING);;
	  MELT_LOCATION ("warmelt-macro.melt:5906:/ cond");
	  /*cond */ if ( /*_#IS_STRING__L9*/ meltfnum[0])	/*then */
	    {
	      /*^cond.then */
	      /*_.IFELSE___V27*/ meltfptr[26] = ( /*nil */ NULL);;
	    }
	  else
	    {
	      MELT_LOCATION ("warmelt-macro.melt:5906:/ cond.else");

	      /*^block */
	      /*anyblock */
	      {


		{
		  MELT_LOCATION ("warmelt-macro.melt:5908:/ locexp");
		  /* error_plain */ melt_error_str ((melt_ptr_t) ( /*_.LOC__V18*/ meltfptr[17]), ("invalid argument to (USE_PACKAGE_FROM_PKG_CONFIG <pkg-name>...), expecting\
 string"), (melt_ptr_t) 0);
		}
		;
		MELT_LOCATION ("warmelt-macro.melt:5909:/ quasiblock");


    /*_.RETVAL___V1*/ meltfptr[0] = NULL;;

		{
		  MELT_LOCATION ("warmelt-macro.melt:5909:/ locexp");
		  /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
		  if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
		    melt_warn_for_no_expected_secondary_results ();
		  /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
		  ;
		}
		;
		/*^finalreturn */
		;
		/*finalret */ goto labend_rout;
		MELT_LOCATION ("warmelt-macro.melt:5906:/ quasiblock");


		/*_.PROGN___V29*/ meltfptr[28] =
		  /*_.RETURN___V28*/ meltfptr[27];;
		/*^compute */
		/*_.IFELSE___V27*/ meltfptr[26] =
		  /*_.PROGN___V29*/ meltfptr[28];;
		/*epilog */

		MELT_LOCATION ("warmelt-macro.melt:5906:/ clear");
	      /*clear *//*_.RETURN___V28*/ meltfptr[27] = 0;
		/*^clear */
	      /*clear *//*_.PROGN___V29*/ meltfptr[28] = 0;
	      }
	      ;
	    }
	  ;
	  MELT_LOCATION ("warmelt-macro.melt:5911:/ quasiblock");


  /*_#GOODPKGNAME__L10*/ meltfnum[9] = 1;;

	  {
	    MELT_LOCATION ("warmelt-macro.melt:5913:/ locexp");
	    /* mexpand_use_package_from_pkg_config CHECKPKGNAME_CHK__1 */
	    {
	      const char *pkgs_CHECKPKGNAME_CHK__1
		=
		melt_string_str ((melt_ptr_t) /*_.CURPKGNAME__V26*/
				 meltfptr[22]);
	      if (!pkgs_CHECKPKGNAME_CHK__1)
		      /*_#GOODPKGNAME__L10*/ meltfnum[9] = 0;
	      else
		{
		  const char *pc = NULL;
		  for (pc = pkgs_CHECKPKGNAME_CHK__1;
		       *pc && /*_#GOODPKGNAME__L10*/ meltfnum[9];
		       pc++)
		    {
		      if (ISALNUM (*pc) || *pc == '+' || *pc == '-'
			  || *pc == '_' || *pc == '.' || *pc == '@')
			continue;
		      else  /*_#GOODPKGNAME__L10*/
			meltfnum[9] = 0L;
		    }
		};
	    } /* end mexpand_use_package_from_pkg_config CHECKPKGNAME_CHK__1 */ ;
	  }
	  ;
	  MELT_LOCATION ("warmelt-macro.melt:5930:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^cond */
	  /*cond */ if ( /*_#GOODPKGNAME__L10*/ meltfnum[9])	/*then */
	    {
	      /*^cond.then */
	      /*_.IFELSE___V31*/ meltfptr[28] = ( /*nil */ NULL);;
	    }
	  else
	    {
	      MELT_LOCATION ("warmelt-macro.melt:5930:/ cond.else");

	      /*^block */
	      /*anyblock */
	      {


		{
		  MELT_LOCATION ("warmelt-macro.melt:5931:/ locexp");
		  melt_error_str ((melt_ptr_t) ( /*_.LOC__V18*/ meltfptr[17]),
				  ("invalid package name for (USE_PACKAGE_FROM_PKG_CONFIG ...)"),
				  (melt_ptr_t) ( /*_.CURPKGNAME__V26*/
						meltfptr[22]));
		}
		;
		MELT_LOCATION ("warmelt-macro.melt:5932:/ quasiblock");


    /*_.RETVAL___V1*/ meltfptr[0] = NULL;;

		{
		  MELT_LOCATION ("warmelt-macro.melt:5932:/ locexp");
		  /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
		  if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
		    melt_warn_for_no_expected_secondary_results ();
		  /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
		  ;
		}
		;
		/*^finalreturn */
		;
		/*finalret */ goto labend_rout;
		MELT_LOCATION ("warmelt-macro.melt:5930:/ quasiblock");


		/*_.PROGN___V33*/ meltfptr[32] =
		  /*_.RETURN___V32*/ meltfptr[31];;
		/*^compute */
		/*_.IFELSE___V31*/ meltfptr[28] =
		  /*_.PROGN___V33*/ meltfptr[32];;
		/*epilog */

		MELT_LOCATION ("warmelt-macro.melt:5930:/ clear");
	      /*clear *//*_.RETURN___V32*/ meltfptr[31] = 0;
		/*^clear */
	      /*clear *//*_.PROGN___V33*/ meltfptr[32] = 0;
	      }
	      ;
	    }
	  ;
	  MELT_LOCATION ("warmelt-macro.melt:5933:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^apply */
	  /*apply */
	  {
	    union meltparam_un argtab[2];
	    memset (&argtab, 0, sizeof (argtab));
	    /*^apply.arg */
	    argtab[0].meltbp_cstring = " ";
	    /*^apply.arg */
	    argtab[1].meltbp_aptr =
	      (melt_ptr_t *) & /*_.CURPKGNAME__V26*/ meltfptr[22];
	    /*_.ADD2OUT__V34*/ meltfptr[31] =
	      melt_apply ((meltclosure_ptr_t)
			  (( /*!ADD2OUT */ meltfrout->tabval[5])),
			  (melt_ptr_t) ( /*_.CMDBUF__V20*/ meltfptr[19]),
			  (MELTBPARSTR_CSTRING MELTBPARSTR_PTR ""), argtab,
			  "", (union meltparam_un *) 0);
	  }
	  ;
	  /*_.LET___V30*/ meltfptr[27] = /*_.ADD2OUT__V34*/ meltfptr[31];;

	  MELT_LOCATION ("warmelt-macro.melt:5911:/ clear");
	    /*clear *//*_#GOODPKGNAME__L10*/ meltfnum[9] = 0;
	  /*^clear */
	    /*clear *//*_.IFELSE___V31*/ meltfptr[28] = 0;
	  /*^clear */
	    /*clear *//*_.ADD2OUT__V34*/ meltfptr[31] = 0;
	  if ( /*_#PKGIX__L8*/ meltfnum[1] < 0)
	    break;
	}			/* end  foreach_in_multiple meltcit1__EACHTUP */

      /*citerepilog */

      MELT_LOCATION ("warmelt-macro.melt:5903:/ clear");
	    /*clear *//*_.CURPKGNAME__V26*/ meltfptr[22] = 0;
      /*^clear */
	    /*clear *//*_#PKGIX__L8*/ meltfnum[1] = 0;
      /*^clear */
	    /*clear *//*_#IS_STRING__L9*/ meltfnum[0] = 0;
      /*^clear */
	    /*clear *//*_.IFELSE___V27*/ meltfptr[26] = 0;
      /*^clear */
	    /*clear *//*_.LET___V30*/ meltfptr[27] = 0;
    }				/*endciterblock FOREACH_IN_MULTIPLE */
    ;
    MELT_LOCATION ("warmelt-macro.melt:5937:/ quasiblock");


 /*_.CMDSTR__V36*/ meltfptr[28] =
      (meltgc_new_stringdup
       ((meltobject_ptr_t) (( /*!DISCR_STRING */ meltfrout->tabval[6])),
	melt_strbuf_str ((melt_ptr_t) ( /*_.CMDBUF__V20*/ meltfptr[19]))));;
    /*^compute */
 /*_#FAILCMD__L11*/ meltfnum[9] = 0;;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-macro.melt:5940:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L12*/ meltfnum[11] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-macro.melt:5940:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L12*/ meltfnum[11])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L13*/ meltfnum[12] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-macro.melt:5940:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long =
		/*_#THE_MELTCALLCOUNT__L13*/ meltfnum[12];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-macro.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 5940;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring =
		"mexpand_use_package_from_pkg_config cmdstr=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.CMDSTR__V36*/ meltfptr[28];
	      /*_.MELT_DEBUG_FUN__V39*/ meltfptr[38] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V38*/ meltfptr[37] =
	      /*_.MELT_DEBUG_FUN__V39*/ meltfptr[38];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-macro.melt:5940:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L13*/ meltfnum[12] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V39*/ meltfptr[38] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V38*/ meltfptr[37] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-macro.melt:5940:/ quasiblock");


      /*_.PROGN___V40*/ meltfptr[38] = /*_.IF___V38*/ meltfptr[37];;
      /*^compute */
      /*_.IFCPP___V37*/ meltfptr[31] = /*_.PROGN___V40*/ meltfptr[38];;
      /*epilog */

      MELT_LOCATION ("warmelt-macro.melt:5940:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L12*/ meltfnum[11] = 0;
      /*^clear */
	     /*clear *//*_.IF___V38*/ meltfptr[37] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V40*/ meltfptr[38] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V37*/ meltfptr[31] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

    {
      MELT_LOCATION ("warmelt-macro.melt:5941:/ locexp");
      /* mexpand_use_package_from_pkg_config TESTPKGCONFIG_CHK__1 */
      {
	const char *cmd_TESTPKGCONFIG_CHK__1 =
	  melt_string_str ((melt_ptr_t) /*_.CMDSTR__V36*/ meltfptr[28]);
	fflush (NULL);
		  /*_#FAILCMD__L11*/ meltfnum[9] = (cmd_TESTPKGCONFIG_CHK__1
						    ?
						    system
						    (cmd_TESTPKGCONFIG_CHK__1)
						    : 1000);
      }				/* end mexpand_use_package_from_pkg_config TESTPKGCONFIG_CHK__1 */
      ;
    }
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-macro.melt:5950:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L14*/ meltfnum[12] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-macro.melt:5950:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L14*/ meltfnum[12])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L15*/ meltfnum[11] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-macro.melt:5950:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long =
		/*_#THE_MELTCALLCOUNT__L15*/ meltfnum[11];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-macro.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 5950;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring =
		"mexpand_use_package_from_pkg_config failcmd=";
	      /*^apply.arg */
	      argtab[4].meltbp_long = /*_#FAILCMD__L11*/ meltfnum[9];
	      /*_.MELT_DEBUG_FUN__V43*/ meltfptr[42] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V42*/ meltfptr[38] =
	      /*_.MELT_DEBUG_FUN__V43*/ meltfptr[42];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-macro.melt:5950:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L15*/ meltfnum[11] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V43*/ meltfptr[42] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V42*/ meltfptr[38] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-macro.melt:5950:/ quasiblock");


      /*_.PROGN___V44*/ meltfptr[42] = /*_.IF___V42*/ meltfptr[38];;
      /*^compute */
      /*_.IFCPP___V41*/ meltfptr[37] = /*_.PROGN___V44*/ meltfptr[42];;
      /*epilog */

      MELT_LOCATION ("warmelt-macro.melt:5950:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L14*/ meltfnum[12] = 0;
      /*^clear */
	     /*clear *//*_.IF___V42*/ meltfptr[38] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V44*/ meltfptr[42] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V41*/ meltfptr[37] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-macro.melt:5951:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^cond */
    /*cond */ if ( /*_#FAILCMD__L11*/ meltfnum[9])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{


	  {
	    MELT_LOCATION ("warmelt-macro.melt:5952:/ locexp");
	    melt_error_str ((melt_ptr_t) ( /*_.LOC__V18*/ meltfptr[17]),
			    ("unexistent package names for USE_PACKAGE_FROM_PKG_CONFIG "),
			    (melt_ptr_t) ( /*_.CMDSTR__V36*/ meltfptr[28]));
	  }
	  ;
	  MELT_LOCATION ("warmelt-macro.melt:5953:/ quasiblock");


   /*_.RETVAL___V1*/ meltfptr[0] = NULL;;

	  {
	    MELT_LOCATION ("warmelt-macro.melt:5953:/ locexp");
	    /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
	    if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	      melt_warn_for_no_expected_secondary_results ();
	    /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
	    ;
	  }
	  ;
	  /*^finalreturn */
	  ;
	  /*finalret */ goto labend_rout;
	  MELT_LOCATION ("warmelt-macro.melt:5951:/ quasiblock");


	  /*_.PROGN___V47*/ meltfptr[46] = /*_.RETURN___V46*/ meltfptr[42];;
	  /*^compute */
	  /*_.IF___V45*/ meltfptr[38] = /*_.PROGN___V47*/ meltfptr[46];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-macro.melt:5951:/ clear");
	     /*clear *//*_.RETURN___V46*/ meltfptr[42] = 0;
	  /*^clear */
	     /*clear *//*_.PROGN___V47*/ meltfptr[46] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

  /*_.IF___V45*/ meltfptr[38] = NULL;;
      }
    ;
    /*^compute */
    /*_.LET___V35*/ meltfptr[32] = /*_.IF___V45*/ meltfptr[38];;

    MELT_LOCATION ("warmelt-macro.melt:5937:/ clear");
	   /*clear *//*_.CMDSTR__V36*/ meltfptr[28] = 0;
    /*^clear */
	   /*clear *//*_#FAILCMD__L11*/ meltfnum[9] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V37*/ meltfptr[31] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V41*/ meltfptr[37] = 0;
    /*^clear */
	   /*clear *//*_.IF___V45*/ meltfptr[38] = 0;
    MELT_LOCATION ("warmelt-macro.melt:5956:/ quasiblock");


    /*^checksignal */
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*^rawallocobj */
    /*rawallocobj */
    {
      melt_ptr_t newobj = 0;
      melt_raw_object_create (newobj,
			      (melt_ptr_t) (( /*!CLASS_SOURCE_USE_PACKAGE_FROM_PKG_CONFIG */ meltfrout->tabval[7])), (3), "CLASS_SOURCE_USE_PACKAGE_FROM_PKG_CONFIG");
  /*_.INST__V50*/ meltfptr[28] =
	newobj;
    };
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @LOCA_LOCATION",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V50*/ meltfptr[28])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V50*/ meltfptr[28]), (1),
			  ( /*_.LOC__V18*/ meltfptr[17]), "LOCA_LOCATION");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @SUSEPACKAGE_PKGTUPLE",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V50*/ meltfptr[28])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V50*/ meltfptr[28]), (2),
			  ( /*_.XARGTUP__V19*/ meltfptr[18]),
			  "SUSEPACKAGE_PKGTUPLE");
    ;
    /*^touchobj */

    melt_dbgtrace_written_object ( /*_.INST__V50*/ meltfptr[28],
				  "newly made instance");
    ;
    /*_.SUP__V49*/ meltfptr[46] = /*_.INST__V50*/ meltfptr[28];;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-macro.melt:5959:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L16*/ meltfnum[11] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-macro.melt:5959:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L16*/ meltfnum[11])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L17*/ meltfnum[12] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-macro.melt:5959:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long =
		/*_#THE_MELTCALLCOUNT__L17*/ meltfnum[12];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-macro.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 5959;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring =
		"mexpand_use_package_from_pkg_config gives sup=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.SUP__V49*/ meltfptr[46];
	      /*_.MELT_DEBUG_FUN__V53*/ meltfptr[38] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V52*/ meltfptr[37] =
	      /*_.MELT_DEBUG_FUN__V53*/ meltfptr[38];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-macro.melt:5959:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L17*/ meltfnum[12] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V53*/ meltfptr[38] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V52*/ meltfptr[37] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-macro.melt:5959:/ quasiblock");


      /*_.PROGN___V54*/ meltfptr[38] = /*_.IF___V52*/ meltfptr[37];;
      /*^compute */
      /*_.IFCPP___V51*/ meltfptr[31] = /*_.PROGN___V54*/ meltfptr[38];;
      /*epilog */

      MELT_LOCATION ("warmelt-macro.melt:5959:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L16*/ meltfnum[11] = 0;
      /*^clear */
	     /*clear *//*_.IF___V52*/ meltfptr[37] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V54*/ meltfptr[38] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V51*/ meltfptr[31] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-macro.melt:5960:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.SUP__V49*/ meltfptr[46];;

    {
      MELT_LOCATION ("warmelt-macro.melt:5960:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*_.LET___V48*/ meltfptr[42] = /*_.RETURN___V55*/ meltfptr[37];;

    MELT_LOCATION ("warmelt-macro.melt:5956:/ clear");
	   /*clear *//*_.SUP__V49*/ meltfptr[46] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V51*/ meltfptr[31] = 0;
    /*^clear */
	   /*clear *//*_.RETURN___V55*/ meltfptr[37] = 0;
    /*_.LET___V16*/ meltfptr[14] = /*_.LET___V48*/ meltfptr[42];;

    MELT_LOCATION ("warmelt-macro.melt:5896:/ clear");
	   /*clear *//*_.CONT__V17*/ meltfptr[16] = 0;
    /*^clear */
	   /*clear *//*_.LOC__V18*/ meltfptr[17] = 0;
    /*^clear */
	   /*clear *//*_.XARGTUP__V19*/ meltfptr[18] = 0;
    /*^clear */
	   /*clear *//*_.CMDBUF__V20*/ meltfptr[19] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V21*/ meltfptr[20] = 0;
    /*^clear */
	   /*clear *//*_.ADD2OUT__V25*/ meltfptr[21] = 0;
    /*^clear */
	   /*clear *//*_.LET___V35*/ meltfptr[32] = 0;
    /*^clear */
	   /*clear *//*_.LET___V48*/ meltfptr[42] = 0;
    MELT_LOCATION ("warmelt-macro.melt:5891:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.LET___V16*/ meltfptr[14];;

    {
      MELT_LOCATION ("warmelt-macro.melt:5891:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V6*/ meltfptr[5] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V10*/ meltfptr[6] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V12*/ meltfptr[7] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V14*/ meltfptr[12] = 0;
    /*^clear */
	   /*clear *//*_.LET___V16*/ meltfptr[14] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("MEXPAND_USE_PACKAGE_FROM_PKG_CONFIG", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_124_warmelt_macro_MEXPAND_USE_PACKAGE_FROM_PKG_CONFIG_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_124_warmelt_macro_MEXPAND_USE_PACKAGE_FROM_PKG_CONFIG */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_125_warmelt_macro_PAIRLIST_TO_RETURN (meltclosure_ptr_t meltclosp_,
					       melt_ptr_t meltfirstargp_,
					       const melt_argdescr_cell_t
					       meltxargdescr_[],
					       union meltparam_un *
					       meltxargtab_,
					       const melt_argdescr_cell_t
					       meltxresdescr_[],
					       union meltparam_un *
					       meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_125_warmelt_macro_PAIRLIST_TO_RETURN_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_125_warmelt_macro_PAIRLIST_TO_RETURN_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 18
    melt_ptr_t mcfr_varptr[18];
#define MELTFRAM_NBVARNUM 3
    long mcfr_varnum[3];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_125_warmelt_macro_PAIRLIST_TO_RETURN is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_125_warmelt_macro_PAIRLIST_TO_RETURN_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 18; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_125_warmelt_macro_PAIRLIST_TO_RETURN nbval 18*/
  meltfram__.mcfr_nbvar = 18 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("PAIRLIST_TO_RETURN", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-macro.melt:5981:/ getarg");
 /*_.PAIR__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.LOC__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.LOC__V3*/ meltfptr[2])) != NULL);


  /*getarg#2 */
  /*^getarg */
  if (meltxargdescr_[1] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.ENV__V4*/ meltfptr[3] =
    (meltxargtab_[1].meltbp_aptr) ? (*(meltxargtab_[1].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.ENV__V4*/ meltfptr[3])) != NULL);


  /*getarg#3 */
  /*^getarg */
  if (meltxargdescr_[2] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.MEXPANDER__V5*/ meltfptr[4] =
    (meltxargtab_[2].meltbp_aptr) ? (*(meltxargtab_[2].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.MEXPANDER__V5*/ meltfptr[4])) !=
	      NULL);


  /*getarg#4 */
  /*^getarg */
  if (meltxargdescr_[3] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.MODCTX__V6*/ meltfptr[5] =
    (meltxargtab_[3].meltbp_aptr) ? (*(meltxargtab_[3].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.MODCTX__V6*/ meltfptr[5])) !=
	      NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-macro.melt:5982:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L1*/ meltfnum[0] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.ENV__V4*/ meltfptr[3]),
			     (melt_ptr_t) (( /*!CLASS_ENVIRONMENT */
					    meltfrout->tabval[0])));;
      MELT_LOCATION ("warmelt-macro.melt:5982:/ cond");
      /*cond */ if ( /*_#IS_A__L1*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V8*/ meltfptr[7] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-macro.melt:5982:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check env"),
				  ("warmelt-macro.melt")
				  ? ("warmelt-macro.melt") : __FILE__,
				  (5982) ? (5982) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V8*/ meltfptr[7] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V7*/ meltfptr[6] = /*_.IFELSE___V8*/ meltfptr[7];;
      /*epilog */

      MELT_LOCATION ("warmelt-macro.melt:5982:/ clear");
	     /*clear *//*_#IS_A__L1*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V8*/ meltfptr[7] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V7*/ meltfptr[6] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-macro.melt:5983:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_CLOSURE__L2*/ meltfnum[0] =
	(melt_magic_discr ((melt_ptr_t) ( /*_.MEXPANDER__V5*/ meltfptr[4])) ==
	 MELTOBMAG_CLOSURE);;
      MELT_LOCATION ("warmelt-macro.melt:5983:/ cond");
      /*cond */ if ( /*_#IS_CLOSURE__L2*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V10*/ meltfptr[9] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-macro.melt:5983:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check mexpander"),
				  ("warmelt-macro.melt")
				  ? ("warmelt-macro.melt") : __FILE__,
				  (5983) ? (5983) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V10*/ meltfptr[9] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V9*/ meltfptr[7] = /*_.IFELSE___V10*/ meltfptr[9];;
      /*epilog */

      MELT_LOCATION ("warmelt-macro.melt:5983:/ clear");
	     /*clear *//*_#IS_CLOSURE__L2*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V10*/ meltfptr[9] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V9*/ meltfptr[7] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-macro.melt:5984:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_OBJECT__L3*/ meltfnum[0] =
	(melt_magic_discr ((melt_ptr_t) ( /*_.MODCTX__V6*/ meltfptr[5])) ==
	 MELTOBMAG_OBJECT);;
      MELT_LOCATION ("warmelt-macro.melt:5984:/ cond");
      /*cond */ if ( /*_#IS_OBJECT__L3*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V12*/ meltfptr[11] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-macro.melt:5984:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check modctx"),
				  ("warmelt-macro.melt")
				  ? ("warmelt-macro.melt") : __FILE__,
				  (5984) ? (5984) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V12*/ meltfptr[11] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V11*/ meltfptr[9] = /*_.IFELSE___V12*/ meltfptr[11];;
      /*epilog */

      MELT_LOCATION ("warmelt-macro.melt:5984:/ clear");
	     /*clear *//*_#IS_OBJECT__L3*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V12*/ meltfptr[11] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V11*/ meltfptr[9] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-macro.melt:5985:/ quasiblock");


    MELT_LOCATION ("warmelt-macro.melt:5989:/ quasiblock");


    /*^newclosure */
		 /*newclosure *//*_.LAMBDA___V15*/ meltfptr[14] =
      (melt_ptr_t)
      meltgc_new_closure ((meltobject_ptr_t)
			  (((melt_ptr_t) (MELT_PREDEF (DISCR_CLOSURE)))),
			  (meltroutine_ptr_t) (( /*!konst_3 */ meltfrout->
						tabval[3])), (3));
    ;
    /*^putclosedv */
    /*putclosv */
    melt_assertmsg ("putclosv checkclo",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.LAMBDA___V15*/ meltfptr[14])) ==
		    MELTOBMAG_CLOSURE);
    melt_assertmsg ("putclosv checkoff", 0 >= 0
		    && 0 <
		    melt_closure_size ((melt_ptr_t)
				       ( /*_.LAMBDA___V15*/ meltfptr[14])));
    ((meltclosure_ptr_t) /*_.LAMBDA___V15*/ meltfptr[14])->tabval[0] =
      (melt_ptr_t) ( /*_.MEXPANDER__V5*/ meltfptr[4]);
    ;
    /*^putclosedv */
    /*putclosv */
    melt_assertmsg ("putclosv checkclo",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.LAMBDA___V15*/ meltfptr[14])) ==
		    MELTOBMAG_CLOSURE);
    melt_assertmsg ("putclosv checkoff", 1 >= 0
		    && 1 <
		    melt_closure_size ((melt_ptr_t)
				       ( /*_.LAMBDA___V15*/ meltfptr[14])));
    ((meltclosure_ptr_t) /*_.LAMBDA___V15*/ meltfptr[14])->tabval[1] =
      (melt_ptr_t) ( /*_.ENV__V4*/ meltfptr[3]);
    ;
    /*^putclosedv */
    /*putclosv */
    melt_assertmsg ("putclosv checkclo",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.LAMBDA___V15*/ meltfptr[14])) ==
		    MELTOBMAG_CLOSURE);
    melt_assertmsg ("putclosv checkoff", 2 >= 0
		    && 2 <
		    melt_closure_size ((melt_ptr_t)
				       ( /*_.LAMBDA___V15*/ meltfptr[14])));
    ((meltclosure_ptr_t) /*_.LAMBDA___V15*/ meltfptr[14])->tabval[2] =
      (melt_ptr_t) ( /*_.MODCTX__V6*/ meltfptr[5]);
    ;
    /*_.LAMBDA___V14*/ meltfptr[13] = /*_.LAMBDA___V15*/ meltfptr[14];;
    MELT_LOCATION ("warmelt-macro.melt:5986:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[2];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & ( /*!DISCR_MULTIPLE */ meltfrout->tabval[2]);
      /*^apply.arg */
      argtab[1].meltbp_aptr =
	(melt_ptr_t *) & /*_.LAMBDA___V14*/ meltfptr[13];
      /*_.BODYTUP__V16*/ meltfptr[15] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!PAIRLIST_TO_MULTIPLE */ meltfrout->tabval[1])),
		    (melt_ptr_t) ( /*_.PAIR__V2*/ meltfptr[1]),
		    (MELTBPARSTR_PTR MELTBPARSTR_PTR ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-macro.melt:5990:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*^rawallocobj */
    /*rawallocobj */
    {
      melt_ptr_t newobj = 0;
      melt_raw_object_create (newobj,
			      (melt_ptr_t) (( /*!CLASS_SOURCE_RETURN */
					     meltfrout->tabval[4])), (3),
			      "CLASS_SOURCE_RETURN");
  /*_.INST__V18*/ meltfptr[17] =
	newobj;
    };
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @LOCA_LOCATION",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V18*/ meltfptr[17])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V18*/ meltfptr[17]), (1),
			  ( /*_.LOC__V3*/ meltfptr[2]), "LOCA_LOCATION");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @SARGOP_ARGS",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V18*/ meltfptr[17])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V18*/ meltfptr[17]), (2),
			  ( /*_.BODYTUP__V16*/ meltfptr[15]), "SARGOP_ARGS");
    ;
    /*^touchobj */

    melt_dbgtrace_written_object ( /*_.INST__V18*/ meltfptr[17],
				  "newly made instance");
    ;
    /*_.INST___V17*/ meltfptr[16] = /*_.INST__V18*/ meltfptr[17];;
    /*^compute */
    /*_.LET___V13*/ meltfptr[11] = /*_.INST___V17*/ meltfptr[16];;

    MELT_LOCATION ("warmelt-macro.melt:5985:/ clear");
	   /*clear *//*_.LAMBDA___V14*/ meltfptr[13] = 0;
    /*^clear */
	   /*clear *//*_.BODYTUP__V16*/ meltfptr[15] = 0;
    /*^clear */
	   /*clear *//*_.INST___V17*/ meltfptr[16] = 0;
    MELT_LOCATION ("warmelt-macro.melt:5981:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.LET___V13*/ meltfptr[11];;

    {
      MELT_LOCATION ("warmelt-macro.melt:5981:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V7*/ meltfptr[6] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V9*/ meltfptr[7] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V11*/ meltfptr[9] = 0;
    /*^clear */
	   /*clear *//*_.LET___V13*/ meltfptr[11] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("PAIRLIST_TO_RETURN", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_125_warmelt_macro_PAIRLIST_TO_RETURN_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_125_warmelt_macro_PAIRLIST_TO_RETURN */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_126_warmelt_macro_LAMBDA___35__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un * meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un * meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_126_warmelt_macro_LAMBDA___35___melt =
    melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_126_warmelt_macro_LAMBDA___35___st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 3
    melt_ptr_t mcfr_varptr[3];
/*no varnum*/
#define MELTFRAM_NBVARNUM /*none*/0
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_126_warmelt_macro_LAMBDA___35__ is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_126_warmelt_macro_LAMBDA___35___st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 3; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_126_warmelt_macro_LAMBDA___35__ nbval 3*/
  meltfram__.mcfr_nbvar = 3 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("LAMBDA_", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-macro.melt:5989:/ getarg");
 /*_.E__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {

    /*^checksignal */
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[3];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & ( /*~ENV */ meltfclos->tabval[1]);
      /*^apply.arg */
      argtab[1].meltbp_aptr =
	(melt_ptr_t *) & ( /*~MEXPANDER */ meltfclos->tabval[0]);
      /*^apply.arg */
      argtab[2].meltbp_aptr =
	(melt_ptr_t *) & ( /*~MODCTX */ meltfclos->tabval[2]);
      /*_.MEXPANDER__V3*/ meltfptr[2] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*~MEXPANDER */ meltfclos->tabval[0])),
		    (melt_ptr_t) ( /*_.E__V2*/ meltfptr[1]),
		    (MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_PTR ""),
		    argtab, "", (union meltparam_un *) 0);
    }
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.MEXPANDER__V3*/ meltfptr[2];;

    {
      MELT_LOCATION ("warmelt-macro.melt:5989:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.MEXPANDER__V3*/ meltfptr[2] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("LAMBDA_", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_126_warmelt_macro_LAMBDA___35___melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_126_warmelt_macro_LAMBDA___35__ */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_127_warmelt_macro_MEXPAND_PROGN (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un * meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un * meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_127_warmelt_macro_MEXPAND_PROGN_melt =
    melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_127_warmelt_macro_MEXPAND_PROGN_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 18
    melt_ptr_t mcfr_varptr[18];
#define MELTFRAM_NBVARNUM 3
    long mcfr_varnum[3];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_127_warmelt_macro_MEXPAND_PROGN is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_127_warmelt_macro_MEXPAND_PROGN_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 18; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_127_warmelt_macro_MEXPAND_PROGN nbval 18*/
  meltfram__.mcfr_nbvar = 18 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("MEXPAND_PROGN", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-macro.melt:5997:/ getarg");
 /*_.SEXPR__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.ENV__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.ENV__V3*/ meltfptr[2])) != NULL);


  /*getarg#2 */
  /*^getarg */
  if (meltxargdescr_[1] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.MEXPANDER__V4*/ meltfptr[3] =
    (meltxargtab_[1].meltbp_aptr) ? (*(meltxargtab_[1].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.MEXPANDER__V4*/ meltfptr[3])) !=
	      NULL);


  /*getarg#3 */
  /*^getarg */
  if (meltxargdescr_[2] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.MODCTX__V5*/ meltfptr[4] =
    (meltxargtab_[2].meltbp_aptr) ? (*(meltxargtab_[2].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.MODCTX__V5*/ meltfptr[4])) !=
	      NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-macro.melt:5998:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L1*/ meltfnum[0] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.SEXPR__V2*/ meltfptr[1]),
			     (melt_ptr_t) (( /*!CLASS_SEXPR */ meltfrout->
					    tabval[0])));;
      MELT_LOCATION ("warmelt-macro.melt:5998:/ cond");
      /*cond */ if ( /*_#IS_A__L1*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V7*/ meltfptr[6] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-macro.melt:5998:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check sexpr"),
				  ("warmelt-macro.melt")
				  ? ("warmelt-macro.melt") : __FILE__,
				  (5998) ? (5998) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V7*/ meltfptr[6] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V6*/ meltfptr[5] = /*_.IFELSE___V7*/ meltfptr[6];;
      /*epilog */

      MELT_LOCATION ("warmelt-macro.melt:5998:/ clear");
	     /*clear *//*_#IS_A__L1*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V7*/ meltfptr[6] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V6*/ meltfptr[5] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-macro.melt:5999:/ quasiblock");


    /*^getslot */
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.SEXPR__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 1, "LOCA_LOCATION");
  /*_.SLOC__V9*/ meltfptr[8] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-macro.melt:6001:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.SEXPR__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 2, "SEXP_CONTENTS");
  /*_.SEXP_CONTENTS__V10*/ meltfptr[9] = slot;
    };
    ;
 /*_.LIST_FIRST__V11*/ meltfptr[10] =
      (melt_list_first
       ((melt_ptr_t) ( /*_.SEXP_CONTENTS__V10*/ meltfptr[9])));;
    /*^compute */
 /*_.PAIRS__V12*/ meltfptr[11] =
      (melt_pair_tail ((melt_ptr_t) ( /*_.LIST_FIRST__V11*/ meltfptr[10])));;
    MELT_LOCATION ("warmelt-macro.melt:6003:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#IS_PAIR__L2*/ meltfnum[0] =
      (melt_magic_discr ((melt_ptr_t) ( /*_.PAIRS__V12*/ meltfptr[11])) ==
       MELTOBMAG_PAIR);;
    /*^compute */
 /*_#NOT__L3*/ meltfnum[2] =
      (!( /*_#IS_PAIR__L2*/ meltfnum[0]));;
    MELT_LOCATION ("warmelt-macro.melt:6003:/ cond");
    /*cond */ if ( /*_#NOT__L3*/ meltfnum[2])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{


	  {
	    MELT_LOCATION ("warmelt-macro.melt:6005:/ locexp");
	    /* error_plain */
	      melt_error_str ((melt_ptr_t) ( /*_.SLOC__V9*/ meltfptr[8]),
			      ("empty PROGN"), (melt_ptr_t) 0);
	  }
	  ;
	  MELT_LOCATION ("warmelt-macro.melt:6006:/ quasiblock");


   /*_.RETVAL___V1*/ meltfptr[0] = NULL;;

	  {
	    MELT_LOCATION ("warmelt-macro.melt:6006:/ locexp");
	    /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
	    if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	      melt_warn_for_no_expected_secondary_results ();
	    /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
	    ;
	  }
	  ;
	  /*^finalreturn */
	  ;
	  /*finalret */ goto labend_rout;
	  MELT_LOCATION ("warmelt-macro.melt:6004:/ quasiblock");


	  /*_.PROGN___V15*/ meltfptr[14] = /*_.RETURN___V14*/ meltfptr[13];;
	  /*^compute */
	  /*_.IF___V13*/ meltfptr[12] = /*_.PROGN___V15*/ meltfptr[14];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-macro.melt:6003:/ clear");
	     /*clear *//*_.RETURN___V14*/ meltfptr[13] = 0;
	  /*^clear */
	     /*clear *//*_.PROGN___V15*/ meltfptr[14] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

  /*_.IF___V13*/ meltfptr[12] = NULL;;
      }
    ;
    MELT_LOCATION ("warmelt-macro.melt:6007:/ quasiblock");


    MELT_LOCATION ("warmelt-macro.melt:6008:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[4];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.SLOC__V9*/ meltfptr[8];
      /*^apply.arg */
      argtab[1].meltbp_aptr = (melt_ptr_t *) & /*_.ENV__V3*/ meltfptr[2];
      /*^apply.arg */
      argtab[2].meltbp_aptr =
	(melt_ptr_t *) & /*_.MEXPANDER__V4*/ meltfptr[3];
      /*^apply.arg */
      argtab[3].meltbp_aptr = (melt_ptr_t *) & /*_.MODCTX__V5*/ meltfptr[4];
      /*_.PROGR__V17*/ meltfptr[14] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!PAIRLIST_TO_PROGN */ meltfrout->tabval[1])),
		    (melt_ptr_t) ( /*_.PAIRS__V12*/ meltfptr[11]),
		    (MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_PTR
		     MELTBPARSTR_PTR ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-macro.melt:6013:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.PROGR__V17*/ meltfptr[14];;

    {
      MELT_LOCATION ("warmelt-macro.melt:6013:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*_.LET___V16*/ meltfptr[13] = /*_.RETURN___V18*/ meltfptr[17];;

    MELT_LOCATION ("warmelt-macro.melt:6007:/ clear");
	   /*clear *//*_.PROGR__V17*/ meltfptr[14] = 0;
    /*^clear */
	   /*clear *//*_.RETURN___V18*/ meltfptr[17] = 0;
    /*_.LET___V8*/ meltfptr[6] = /*_.LET___V16*/ meltfptr[13];;

    MELT_LOCATION ("warmelt-macro.melt:5999:/ clear");
	   /*clear *//*_.SLOC__V9*/ meltfptr[8] = 0;
    /*^clear */
	   /*clear *//*_.SEXP_CONTENTS__V10*/ meltfptr[9] = 0;
    /*^clear */
	   /*clear *//*_.LIST_FIRST__V11*/ meltfptr[10] = 0;
    /*^clear */
	   /*clear *//*_.PAIRS__V12*/ meltfptr[11] = 0;
    /*^clear */
	   /*clear *//*_#IS_PAIR__L2*/ meltfnum[0] = 0;
    /*^clear */
	   /*clear *//*_#NOT__L3*/ meltfnum[2] = 0;
    /*^clear */
	   /*clear *//*_.IF___V13*/ meltfptr[12] = 0;
    /*^clear */
	   /*clear *//*_.LET___V16*/ meltfptr[13] = 0;
    MELT_LOCATION ("warmelt-macro.melt:5997:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.LET___V8*/ meltfptr[6];;

    {
      MELT_LOCATION ("warmelt-macro.melt:5997:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V6*/ meltfptr[5] = 0;
    /*^clear */
	   /*clear *//*_.LET___V8*/ meltfptr[6] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("MEXPAND_PROGN", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_127_warmelt_macro_MEXPAND_PROGN_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_127_warmelt_macro_MEXPAND_PROGN */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_128_warmelt_macro_MEXPAND_RETURN (meltclosure_ptr_t meltclosp_,
					   melt_ptr_t meltfirstargp_,
					   const melt_argdescr_cell_t
					   meltxargdescr_[],
					   union meltparam_un * meltxargtab_,
					   const melt_argdescr_cell_t
					   meltxresdescr_[],
					   union meltparam_un * meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_128_warmelt_macro_MEXPAND_RETURN_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_128_warmelt_macro_MEXPAND_RETURN_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 22
    melt_ptr_t mcfr_varptr[22];
#define MELTFRAM_NBVARNUM 5
    long mcfr_varnum[5];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_128_warmelt_macro_MEXPAND_RETURN is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_128_warmelt_macro_MEXPAND_RETURN_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 22; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_128_warmelt_macro_MEXPAND_RETURN nbval 22*/
  meltfram__.mcfr_nbvar = 22 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("MEXPAND_RETURN", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-macro.melt:6022:/ getarg");
 /*_.SEXPR__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.ENV__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.ENV__V3*/ meltfptr[2])) != NULL);


  /*getarg#2 */
  /*^getarg */
  if (meltxargdescr_[1] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.MEXPANDER__V4*/ meltfptr[3] =
    (meltxargtab_[1].meltbp_aptr) ? (*(meltxargtab_[1].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.MEXPANDER__V4*/ meltfptr[3])) !=
	      NULL);


  /*getarg#3 */
  /*^getarg */
  if (meltxargdescr_[2] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.MODCTX__V5*/ meltfptr[4] =
    (meltxargtab_[2].meltbp_aptr) ? (*(meltxargtab_[2].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.MODCTX__V5*/ meltfptr[4])) !=
	      NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-macro.melt:6023:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L1*/ meltfnum[0] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-macro.melt:6023:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L1*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-macro.melt:6023:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-macro.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 6023;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "mexpand_return sexpr=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.SEXPR__V2*/ meltfptr[1];
	      /*_.MELT_DEBUG_FUN__V8*/ meltfptr[7] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V7*/ meltfptr[6] = /*_.MELT_DEBUG_FUN__V8*/ meltfptr[7];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-macro.melt:6023:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V8*/ meltfptr[7] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V7*/ meltfptr[6] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-macro.melt:6023:/ quasiblock");


      /*_.PROGN___V9*/ meltfptr[7] = /*_.IF___V7*/ meltfptr[6];;
      /*^compute */
      /*_.IFCPP___V6*/ meltfptr[5] = /*_.PROGN___V9*/ meltfptr[7];;
      /*epilog */

      MELT_LOCATION ("warmelt-macro.melt:6023:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L1*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IF___V7*/ meltfptr[6] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V9*/ meltfptr[7] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V6*/ meltfptr[5] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-macro.melt:6024:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L3*/ meltfnum[1] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.SEXPR__V2*/ meltfptr[1]),
			     (melt_ptr_t) (( /*!CLASS_SEXPR */ meltfrout->
					    tabval[1])));;
      MELT_LOCATION ("warmelt-macro.melt:6024:/ cond");
      /*cond */ if ( /*_#IS_A__L3*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V11*/ meltfptr[7] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-macro.melt:6024:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check sexpr"),
				  ("warmelt-macro.melt")
				  ? ("warmelt-macro.melt") : __FILE__,
				  (6024) ? (6024) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V11*/ meltfptr[7] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V10*/ meltfptr[6] = /*_.IFELSE___V11*/ meltfptr[7];;
      /*epilog */

      MELT_LOCATION ("warmelt-macro.melt:6024:/ clear");
	     /*clear *//*_#IS_A__L3*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V11*/ meltfptr[7] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V10*/ meltfptr[6] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-macro.melt:6025:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_CLOSURE__L4*/ meltfnum[0] =
	(melt_magic_discr ((melt_ptr_t) ( /*_.MEXPANDER__V4*/ meltfptr[3])) ==
	 MELTOBMAG_CLOSURE);;
      MELT_LOCATION ("warmelt-macro.melt:6025:/ cond");
      /*cond */ if ( /*_#IS_CLOSURE__L4*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V13*/ meltfptr[12] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-macro.melt:6025:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check mexpander"),
				  ("warmelt-macro.melt")
				  ? ("warmelt-macro.melt") : __FILE__,
				  (6025) ? (6025) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V13*/ meltfptr[12] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V12*/ meltfptr[7] = /*_.IFELSE___V13*/ meltfptr[12];;
      /*epilog */

      MELT_LOCATION ("warmelt-macro.melt:6025:/ clear");
	     /*clear *//*_#IS_CLOSURE__L4*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V13*/ meltfptr[12] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V12*/ meltfptr[7] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-macro.melt:6026:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_OBJECT__L5*/ meltfnum[1] =
	(melt_magic_discr ((melt_ptr_t) ( /*_.MODCTX__V5*/ meltfptr[4])) ==
	 MELTOBMAG_OBJECT);;
      MELT_LOCATION ("warmelt-macro.melt:6026:/ cond");
      /*cond */ if ( /*_#IS_OBJECT__L5*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V15*/ meltfptr[14] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-macro.melt:6026:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check modctx"),
				  ("warmelt-macro.melt")
				  ? ("warmelt-macro.melt") : __FILE__,
				  (6026) ? (6026) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V15*/ meltfptr[14] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V14*/ meltfptr[12] = /*_.IFELSE___V15*/ meltfptr[14];;
      /*epilog */

      MELT_LOCATION ("warmelt-macro.melt:6026:/ clear");
	     /*clear *//*_#IS_OBJECT__L5*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V15*/ meltfptr[14] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V14*/ meltfptr[12] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-macro.melt:6027:/ quasiblock");


    MELT_LOCATION ("warmelt-macro.melt:6028:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.SEXPR__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 2, "SEXP_CONTENTS");
  /*_.SEXP_CONTENTS__V17*/ meltfptr[16] = slot;
    };
    ;
 /*_.LIST_FIRST__V18*/ meltfptr[17] =
      (melt_list_first
       ((melt_ptr_t) ( /*_.SEXP_CONTENTS__V17*/ meltfptr[16])));;
    /*^compute */
 /*_.PAIR_TAIL__V19*/ meltfptr[18] =
      (melt_pair_tail ((melt_ptr_t) ( /*_.LIST_FIRST__V18*/ meltfptr[17])));;
    MELT_LOCATION ("warmelt-macro.melt:6029:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.SEXPR__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 1, "LOCA_LOCATION");
  /*_.LOCA_LOCATION__V20*/ meltfptr[19] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-macro.melt:6028:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[4];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & /*_.LOCA_LOCATION__V20*/ meltfptr[19];
      /*^apply.arg */
      argtab[1].meltbp_aptr = (melt_ptr_t *) & /*_.ENV__V3*/ meltfptr[2];
      /*^apply.arg */
      argtab[2].meltbp_aptr =
	(melt_ptr_t *) & /*_.MEXPANDER__V4*/ meltfptr[3];
      /*^apply.arg */
      argtab[3].meltbp_aptr = (melt_ptr_t *) & /*_.MODCTX__V5*/ meltfptr[4];
      /*_.RETR__V21*/ meltfptr[20] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!PAIRLIST_TO_RETURN */ meltfrout->tabval[2])),
		    (melt_ptr_t) ( /*_.PAIR_TAIL__V19*/ meltfptr[18]),
		    (MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_PTR
		     MELTBPARSTR_PTR ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-macro.melt:6034:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.RETR__V21*/ meltfptr[20];;

    {
      MELT_LOCATION ("warmelt-macro.melt:6034:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*_.LET___V16*/ meltfptr[14] = /*_.RETURN___V22*/ meltfptr[21];;

    MELT_LOCATION ("warmelt-macro.melt:6027:/ clear");
	   /*clear *//*_.SEXP_CONTENTS__V17*/ meltfptr[16] = 0;
    /*^clear */
	   /*clear *//*_.LIST_FIRST__V18*/ meltfptr[17] = 0;
    /*^clear */
	   /*clear *//*_.PAIR_TAIL__V19*/ meltfptr[18] = 0;
    /*^clear */
	   /*clear *//*_.LOCA_LOCATION__V20*/ meltfptr[19] = 0;
    /*^clear */
	   /*clear *//*_.RETR__V21*/ meltfptr[20] = 0;
    /*^clear */
	   /*clear *//*_.RETURN___V22*/ meltfptr[21] = 0;
    MELT_LOCATION ("warmelt-macro.melt:6022:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.LET___V16*/ meltfptr[14];;

    {
      MELT_LOCATION ("warmelt-macro.melt:6022:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V6*/ meltfptr[5] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V10*/ meltfptr[6] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V12*/ meltfptr[7] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V14*/ meltfptr[12] = 0;
    /*^clear */
	   /*clear *//*_.LET___V16*/ meltfptr[14] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("MEXPAND_RETURN", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_128_warmelt_macro_MEXPAND_RETURN_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_128_warmelt_macro_MEXPAND_RETURN */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_129_warmelt_macro_MEXPAND_FOREVER (meltclosure_ptr_t meltclosp_,
					    melt_ptr_t meltfirstargp_,
					    const melt_argdescr_cell_t
					    meltxargdescr_[],
					    union meltparam_un * meltxargtab_,
					    const melt_argdescr_cell_t
					    meltxresdescr_[],
					    union meltparam_un * meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_129_warmelt_macro_MEXPAND_FOREVER_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_129_warmelt_macro_MEXPAND_FOREVER_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 39
    melt_ptr_t mcfr_varptr[39];
#define MELTFRAM_NBVARNUM 6
    long mcfr_varnum[6];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_129_warmelt_macro_MEXPAND_FOREVER is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_129_warmelt_macro_MEXPAND_FOREVER_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 39; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_129_warmelt_macro_MEXPAND_FOREVER nbval 39*/
  meltfram__.mcfr_nbvar = 39 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("MEXPAND_FOREVER", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-macro.melt:6045:/ getarg");
 /*_.SEXPR__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.ENV__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.ENV__V3*/ meltfptr[2])) != NULL);


  /*getarg#2 */
  /*^getarg */
  if (meltxargdescr_[1] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.MEXPANDER__V4*/ meltfptr[3] =
    (meltxargtab_[1].meltbp_aptr) ? (*(meltxargtab_[1].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.MEXPANDER__V4*/ meltfptr[3])) !=
	      NULL);


  /*getarg#3 */
  /*^getarg */
  if (meltxargdescr_[2] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.MODCTX__V5*/ meltfptr[4] =
    (meltxargtab_[2].meltbp_aptr) ? (*(meltxargtab_[2].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.MODCTX__V5*/ meltfptr[4])) !=
	      NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-macro.melt:6046:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L1*/ meltfnum[0] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-macro.melt:6046:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L1*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-macro.melt:6046:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-macro.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 6046;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "mexpand_forever sexpr=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.SEXPR__V2*/ meltfptr[1];
	      /*_.MELT_DEBUG_FUN__V8*/ meltfptr[7] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V7*/ meltfptr[6] = /*_.MELT_DEBUG_FUN__V8*/ meltfptr[7];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-macro.melt:6046:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V8*/ meltfptr[7] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V7*/ meltfptr[6] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-macro.melt:6046:/ quasiblock");


      /*_.PROGN___V9*/ meltfptr[7] = /*_.IF___V7*/ meltfptr[6];;
      /*^compute */
      /*_.IFCPP___V6*/ meltfptr[5] = /*_.PROGN___V9*/ meltfptr[7];;
      /*epilog */

      MELT_LOCATION ("warmelt-macro.melt:6046:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L1*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IF___V7*/ meltfptr[6] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V9*/ meltfptr[7] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V6*/ meltfptr[5] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-macro.melt:6047:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L3*/ meltfnum[1] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.SEXPR__V2*/ meltfptr[1]),
			     (melt_ptr_t) (( /*!CLASS_SEXPR */ meltfrout->
					    tabval[1])));;
      MELT_LOCATION ("warmelt-macro.melt:6047:/ cond");
      /*cond */ if ( /*_#IS_A__L3*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V11*/ meltfptr[7] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-macro.melt:6047:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check sexpr"),
				  ("warmelt-macro.melt")
				  ? ("warmelt-macro.melt") : __FILE__,
				  (6047) ? (6047) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V11*/ meltfptr[7] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V10*/ meltfptr[6] = /*_.IFELSE___V11*/ meltfptr[7];;
      /*epilog */

      MELT_LOCATION ("warmelt-macro.melt:6047:/ clear");
	     /*clear *//*_#IS_A__L3*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V11*/ meltfptr[7] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V10*/ meltfptr[6] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-macro.melt:6048:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_CLOSURE__L4*/ meltfnum[0] =
	(melt_magic_discr ((melt_ptr_t) ( /*_.MEXPANDER__V4*/ meltfptr[3])) ==
	 MELTOBMAG_CLOSURE);;
      MELT_LOCATION ("warmelt-macro.melt:6048:/ cond");
      /*cond */ if ( /*_#IS_CLOSURE__L4*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V13*/ meltfptr[12] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-macro.melt:6048:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check mexpander"),
				  ("warmelt-macro.melt")
				  ? ("warmelt-macro.melt") : __FILE__,
				  (6048) ? (6048) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V13*/ meltfptr[12] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V12*/ meltfptr[7] = /*_.IFELSE___V13*/ meltfptr[12];;
      /*epilog */

      MELT_LOCATION ("warmelt-macro.melt:6048:/ clear");
	     /*clear *//*_#IS_CLOSURE__L4*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V13*/ meltfptr[12] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V12*/ meltfptr[7] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-macro.melt:6049:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_OBJECT__L5*/ meltfnum[1] =
	(melt_magic_discr ((melt_ptr_t) ( /*_.MODCTX__V5*/ meltfptr[4])) ==
	 MELTOBMAG_OBJECT);;
      MELT_LOCATION ("warmelt-macro.melt:6049:/ cond");
      /*cond */ if ( /*_#IS_OBJECT__L5*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V15*/ meltfptr[14] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-macro.melt:6049:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check modctx"),
				  ("warmelt-macro.melt")
				  ? ("warmelt-macro.melt") : __FILE__,
				  (6049) ? (6049) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V15*/ meltfptr[14] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V14*/ meltfptr[12] = /*_.IFELSE___V15*/ meltfptr[14];;
      /*epilog */

      MELT_LOCATION ("warmelt-macro.melt:6049:/ clear");
	     /*clear *//*_#IS_OBJECT__L5*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V15*/ meltfptr[14] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V14*/ meltfptr[12] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-macro.melt:6050:/ quasiblock");


    /*^getslot */
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.SEXPR__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 2, "SEXP_CONTENTS");
  /*_.CONT__V17*/ meltfptr[16] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-macro.melt:6051:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.SEXPR__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 1, "LOCA_LOCATION");
  /*_.LOC__V18*/ meltfptr[17] = slot;
    };
    ;
 /*_.LIST_FIRST__V19*/ meltfptr[18] =
      (melt_list_first ((melt_ptr_t) ( /*_.CONT__V17*/ meltfptr[16])));;
    /*^compute */
 /*_.CURPAIR__V20*/ meltfptr[19] =
      (melt_pair_tail ((melt_ptr_t) ( /*_.LIST_FIRST__V19*/ meltfptr[18])));;
    /*^compute */
 /*_.SLABNAM__V21*/ meltfptr[20] =
      (melt_pair_head ((melt_ptr_t) ( /*_.CURPAIR__V20*/ meltfptr[19])));;
    MELT_LOCATION ("warmelt-macro.melt:6054:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[3];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.ENV__V3*/ meltfptr[2];
      /*^apply.arg */
      argtab[1].meltbp_aptr =
	(melt_ptr_t *) & /*_.MEXPANDER__V4*/ meltfptr[3];
      /*^apply.arg */
      argtab[2].meltbp_aptr = (melt_ptr_t *) & /*_.MODCTX__V5*/ meltfptr[4];
      /*_.XLABNAM__V22*/ meltfptr[21] =
	melt_apply ((meltclosure_ptr_t) ( /*_.MEXPANDER__V4*/ meltfptr[3]),
		    (melt_ptr_t) ( /*_.SLABNAM__V21*/ meltfptr[20]),
		    (MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_PTR ""),
		    argtab, "", (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-macro.melt:6055:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      /*_.NEWENV__V23*/ meltfptr[22] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!FRESH_ENV */ meltfrout->tabval[2])),
		    (melt_ptr_t) ( /*_.ENV__V3*/ meltfptr[2]), (""),
		    (union meltparam_un *) 0, "", (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-macro.melt:6057:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#IS_NOT_A__L6*/ meltfnum[0] =
      !melt_is_instance_of ((melt_ptr_t) ( /*_.XLABNAM__V22*/ meltfptr[21]),
			    (melt_ptr_t) (( /*!CLASS_SYMBOL */ meltfrout->
					   tabval[3])));;
    MELT_LOCATION ("warmelt-macro.melt:6057:/ cond");
    /*cond */ if ( /*_#IS_NOT_A__L6*/ meltfnum[0])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{


	  {
	    MELT_LOCATION ("warmelt-macro.melt:6059:/ locexp");
	    /* error_plain */
	      melt_error_str ((melt_ptr_t) ( /*_.LOC__V18*/ meltfptr[17]),
			      ("missing label in FOREVER"), (melt_ptr_t) 0);
	  }
	  ;
	  MELT_LOCATION ("warmelt-macro.melt:6060:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^quasiblock */


	  /*_.RETVAL___V1*/ meltfptr[0] = ( /*nil */ NULL);;

	  {
	    MELT_LOCATION ("warmelt-macro.melt:6060:/ locexp");
	    /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
	    if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	      melt_warn_for_no_expected_secondary_results ();
	    /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
	    ;
	  }
	  ;
	  /*^finalreturn */
	  ;
	  /*finalret */ goto labend_rout;
	  MELT_LOCATION ("warmelt-macro.melt:6058:/ quasiblock");


	  /*_.PROGN___V26*/ meltfptr[25] = /*_.RETURN___V25*/ meltfptr[24];;
	  /*^compute */
	  /*_.IF___V24*/ meltfptr[23] = /*_.PROGN___V26*/ meltfptr[25];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-macro.melt:6057:/ clear");
	     /*clear *//*_.RETURN___V25*/ meltfptr[24] = 0;
	  /*^clear */
	     /*clear *//*_.PROGN___V26*/ meltfptr[25] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

  /*_.IF___V24*/ meltfptr[23] = NULL;;
      }
    ;
    /*^compute */
 /*_.PAIR_TAIL__V27*/ meltfptr[24] =
      (melt_pair_tail ((melt_ptr_t) ( /*_.CURPAIR__V20*/ meltfptr[19])));;
    MELT_LOCATION ("warmelt-macro.melt:6061:/ compute");
    /*_.CURPAIR__V20*/ meltfptr[19] = /*_.SETQ___V28*/ meltfptr[25] =
      /*_.PAIR_TAIL__V27*/ meltfptr[24];;
    MELT_LOCATION ("warmelt-macro.melt:6062:/ quasiblock");


    /*^checksignal */
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*^rawallocobj */
    /*rawallocobj */
    {
      melt_ptr_t newobj = 0;
      melt_raw_object_create (newobj,
			      (melt_ptr_t) (( /*!CLASS_LABEL_BINDING */
					     meltfrout->tabval[4])), (4),
			      "CLASS_LABEL_BINDING");
  /*_.INST__V31*/ meltfptr[30] =
	newobj;
    };
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @BINDER",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V31*/ meltfptr[30])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V31*/ meltfptr[30]), (0),
			  ( /*_.XLABNAM__V22*/ meltfptr[21]), "BINDER");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @LABIND_LOC",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V31*/ meltfptr[30])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V31*/ meltfptr[30]), (1),
			  ( /*_.LOC__V18*/ meltfptr[17]), "LABIND_LOC");
    ;
    /*^touchobj */

    melt_dbgtrace_written_object ( /*_.INST__V31*/ meltfptr[30],
				  "newly made instance");
    ;
    /*_.LABIND__V30*/ meltfptr[29] = /*_.INST__V31*/ meltfptr[30];;
    MELT_LOCATION ("warmelt-macro.melt:6065:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.LABIND__V30*/ meltfptr[29];
      /*_.PUT_ENV__V32*/ meltfptr[31] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!PUT_ENV */ meltfrout->tabval[5])),
		    (melt_ptr_t) ( /*_.NEWENV__V23*/ meltfptr[22]),
		    (MELTBPARSTR_PTR ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-macro.melt:6066:/ quasiblock");


    MELT_LOCATION ("warmelt-macro.melt:6069:/ quasiblock");


    /*^newclosure */
		 /*newclosure *//*_.LAMBDA___V35*/ meltfptr[34] =
      (melt_ptr_t)
      meltgc_new_closure ((meltobject_ptr_t)
			  (((melt_ptr_t) (MELT_PREDEF (DISCR_CLOSURE)))),
			  (meltroutine_ptr_t) (( /*!konst_8 */ meltfrout->
						tabval[8])), (3));
    ;
    /*^putclosedv */
    /*putclosv */
    melt_assertmsg ("putclosv checkclo",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.LAMBDA___V35*/ meltfptr[34])) ==
		    MELTOBMAG_CLOSURE);
    melt_assertmsg ("putclosv checkoff", 0 >= 0
		    && 0 <
		    melt_closure_size ((melt_ptr_t)
				       ( /*_.LAMBDA___V35*/ meltfptr[34])));
    ((meltclosure_ptr_t) /*_.LAMBDA___V35*/ meltfptr[34])->tabval[0] =
      (melt_ptr_t) ( /*_.MEXPANDER__V4*/ meltfptr[3]);
    ;
    /*^putclosedv */
    /*putclosv */
    melt_assertmsg ("putclosv checkclo",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.LAMBDA___V35*/ meltfptr[34])) ==
		    MELTOBMAG_CLOSURE);
    melt_assertmsg ("putclosv checkoff", 1 >= 0
		    && 1 <
		    melt_closure_size ((melt_ptr_t)
				       ( /*_.LAMBDA___V35*/ meltfptr[34])));
    ((meltclosure_ptr_t) /*_.LAMBDA___V35*/ meltfptr[34])->tabval[1] =
      (melt_ptr_t) ( /*_.NEWENV__V23*/ meltfptr[22]);
    ;
    /*^putclosedv */
    /*putclosv */
    melt_assertmsg ("putclosv checkclo",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.LAMBDA___V35*/ meltfptr[34])) ==
		    MELTOBMAG_CLOSURE);
    melt_assertmsg ("putclosv checkoff", 2 >= 0
		    && 2 <
		    melt_closure_size ((melt_ptr_t)
				       ( /*_.LAMBDA___V35*/ meltfptr[34])));
    ((meltclosure_ptr_t) /*_.LAMBDA___V35*/ meltfptr[34])->tabval[2] =
      (melt_ptr_t) ( /*_.MODCTX__V5*/ meltfptr[4]);
    ;
    /*_.LAMBDA___V34*/ meltfptr[33] = /*_.LAMBDA___V35*/ meltfptr[34];;
    MELT_LOCATION ("warmelt-macro.melt:6066:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[2];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & ( /*!DISCR_MULTIPLE */ meltfrout->tabval[7]);
      /*^apply.arg */
      argtab[1].meltbp_aptr =
	(melt_ptr_t *) & /*_.LAMBDA___V34*/ meltfptr[33];
      /*_.BODYTUP__V36*/ meltfptr[35] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!PAIRLIST_TO_MULTIPLE */ meltfrout->tabval[6])),
		    (melt_ptr_t) ( /*_.CURPAIR__V20*/ meltfptr[19]),
		    (MELTBPARSTR_PTR MELTBPARSTR_PTR ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-macro.melt:6071:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*^rawallocobj */
    /*rawallocobj */
    {
      melt_ptr_t newobj = 0;
      melt_raw_object_create (newobj,
			      (melt_ptr_t) (( /*!CLASS_SOURCE_FOREVER */
					     meltfrout->tabval[9])), (4),
			      "CLASS_SOURCE_FOREVER");
  /*_.INST__V38*/ meltfptr[37] =
	newobj;
    };
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @LOCA_LOCATION",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V38*/ meltfptr[37])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V38*/ meltfptr[37]), (1),
			  ( /*_.LOC__V18*/ meltfptr[17]), "LOCA_LOCATION");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @SLABEL_BIND",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V38*/ meltfptr[37])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V38*/ meltfptr[37]), (2),
			  ( /*_.LABIND__V30*/ meltfptr[29]), "SLABEL_BIND");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @SFRV_BODY",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V38*/ meltfptr[37])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V38*/ meltfptr[37]), (3),
			  ( /*_.BODYTUP__V36*/ meltfptr[35]), "SFRV_BODY");
    ;
    /*^touchobj */

    melt_dbgtrace_written_object ( /*_.INST__V38*/ meltfptr[37],
				  "newly made instance");
    ;
    /*_.FORR__V37*/ meltfptr[36] = /*_.INST__V38*/ meltfptr[37];;
    MELT_LOCATION ("warmelt-macro.melt:6076:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.FORR__V37*/ meltfptr[36];;

    {
      MELT_LOCATION ("warmelt-macro.melt:6076:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*_.LET___V33*/ meltfptr[32] = /*_.RETURN___V39*/ meltfptr[38];;

    MELT_LOCATION ("warmelt-macro.melt:6066:/ clear");
	   /*clear *//*_.LAMBDA___V34*/ meltfptr[33] = 0;
    /*^clear */
	   /*clear *//*_.BODYTUP__V36*/ meltfptr[35] = 0;
    /*^clear */
	   /*clear *//*_.FORR__V37*/ meltfptr[36] = 0;
    /*^clear */
	   /*clear *//*_.RETURN___V39*/ meltfptr[38] = 0;
    /*_.LET___V29*/ meltfptr[28] = /*_.LET___V33*/ meltfptr[32];;

    MELT_LOCATION ("warmelt-macro.melt:6062:/ clear");
	   /*clear *//*_.LABIND__V30*/ meltfptr[29] = 0;
    /*^clear */
	   /*clear *//*_.PUT_ENV__V32*/ meltfptr[31] = 0;
    /*^clear */
	   /*clear *//*_.LET___V33*/ meltfptr[32] = 0;
    /*_.LET___V16*/ meltfptr[14] = /*_.LET___V29*/ meltfptr[28];;

    MELT_LOCATION ("warmelt-macro.melt:6050:/ clear");
	   /*clear *//*_.CONT__V17*/ meltfptr[16] = 0;
    /*^clear */
	   /*clear *//*_.LOC__V18*/ meltfptr[17] = 0;
    /*^clear */
	   /*clear *//*_.LIST_FIRST__V19*/ meltfptr[18] = 0;
    /*^clear */
	   /*clear *//*_.CURPAIR__V20*/ meltfptr[19] = 0;
    /*^clear */
	   /*clear *//*_.SLABNAM__V21*/ meltfptr[20] = 0;
    /*^clear */
	   /*clear *//*_.XLABNAM__V22*/ meltfptr[21] = 0;
    /*^clear */
	   /*clear *//*_.NEWENV__V23*/ meltfptr[22] = 0;
    /*^clear */
	   /*clear *//*_#IS_NOT_A__L6*/ meltfnum[0] = 0;
    /*^clear */
	   /*clear *//*_.IF___V24*/ meltfptr[23] = 0;
    /*^clear */
	   /*clear *//*_.PAIR_TAIL__V27*/ meltfptr[24] = 0;
    /*^clear */
	   /*clear *//*_.SETQ___V28*/ meltfptr[25] = 0;
    /*^clear */
	   /*clear *//*_.LET___V29*/ meltfptr[28] = 0;
    MELT_LOCATION ("warmelt-macro.melt:6045:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.LET___V16*/ meltfptr[14];;

    {
      MELT_LOCATION ("warmelt-macro.melt:6045:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V6*/ meltfptr[5] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V10*/ meltfptr[6] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V12*/ meltfptr[7] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V14*/ meltfptr[12] = 0;
    /*^clear */
	   /*clear *//*_.LET___V16*/ meltfptr[14] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("MEXPAND_FOREVER", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_129_warmelt_macro_MEXPAND_FOREVER_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_129_warmelt_macro_MEXPAND_FOREVER */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_130_warmelt_macro_LAMBDA___36__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un * meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un * meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_130_warmelt_macro_LAMBDA___36___melt =
    melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_130_warmelt_macro_LAMBDA___36___st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 3
    melt_ptr_t mcfr_varptr[3];
/*no varnum*/
#define MELTFRAM_NBVARNUM /*none*/0
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_130_warmelt_macro_LAMBDA___36__ is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_130_warmelt_macro_LAMBDA___36___st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 3; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_130_warmelt_macro_LAMBDA___36__ nbval 3*/
  meltfram__.mcfr_nbvar = 3 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("LAMBDA_", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-macro.melt:6069:/ getarg");
 /*_.E__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {

    /*^checksignal */
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[3];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & ( /*~NEWENV */ meltfclos->tabval[1]);
      /*^apply.arg */
      argtab[1].meltbp_aptr =
	(melt_ptr_t *) & ( /*~MEXPANDER */ meltfclos->tabval[0]);
      /*^apply.arg */
      argtab[2].meltbp_aptr =
	(melt_ptr_t *) & ( /*~MODCTX */ meltfclos->tabval[2]);
      /*_.MEXPANDER__V3*/ meltfptr[2] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*~MEXPANDER */ meltfclos->tabval[0])),
		    (melt_ptr_t) ( /*_.E__V2*/ meltfptr[1]),
		    (MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_PTR ""),
		    argtab, "", (union meltparam_un *) 0);
    }
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.MEXPANDER__V3*/ meltfptr[2];;

    {
      MELT_LOCATION ("warmelt-macro.melt:6069:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.MEXPANDER__V3*/ meltfptr[2] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("LAMBDA_", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_130_warmelt_macro_LAMBDA___36___melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_130_warmelt_macro_LAMBDA___36__ */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_131_warmelt_macro_MEXPAND_EXIT (meltclosure_ptr_t meltclosp_,
					 melt_ptr_t meltfirstargp_,
					 const melt_argdescr_cell_t
					 meltxargdescr_[],
					 union meltparam_un * meltxargtab_,
					 const melt_argdescr_cell_t
					 meltxresdescr_[],
					 union meltparam_un * meltxrestab_)
{
  long current_blocklevel_signals_meltrout_131_warmelt_macro_MEXPAND_EXIT_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_131_warmelt_macro_MEXPAND_EXIT_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 41
    melt_ptr_t mcfr_varptr[41];
#define MELTFRAM_NBVARNUM 7
    long mcfr_varnum[7];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_131_warmelt_macro_MEXPAND_EXIT is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_131_warmelt_macro_MEXPAND_EXIT_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 41; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_131_warmelt_macro_MEXPAND_EXIT nbval 41*/
  meltfram__.mcfr_nbvar = 41 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("MEXPAND_EXIT", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-macro.melt:6088:/ getarg");
 /*_.SEXPR__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.ENV__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.ENV__V3*/ meltfptr[2])) != NULL);


  /*getarg#2 */
  /*^getarg */
  if (meltxargdescr_[1] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.MEXPANDER__V4*/ meltfptr[3] =
    (meltxargtab_[1].meltbp_aptr) ? (*(meltxargtab_[1].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.MEXPANDER__V4*/ meltfptr[3])) !=
	      NULL);


  /*getarg#3 */
  /*^getarg */
  if (meltxargdescr_[2] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.MODCTX__V5*/ meltfptr[4] =
    (meltxargtab_[2].meltbp_aptr) ? (*(meltxargtab_[2].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.MODCTX__V5*/ meltfptr[4])) !=
	      NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-macro.melt:6089:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L1*/ meltfnum[0] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-macro.melt:6089:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L1*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-macro.melt:6089:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-macro.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 6089;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "mexpand_exit sexpr=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.SEXPR__V2*/ meltfptr[1];
	      /*_.MELT_DEBUG_FUN__V8*/ meltfptr[7] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V7*/ meltfptr[6] = /*_.MELT_DEBUG_FUN__V8*/ meltfptr[7];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-macro.melt:6089:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V8*/ meltfptr[7] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V7*/ meltfptr[6] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-macro.melt:6089:/ quasiblock");


      /*_.PROGN___V9*/ meltfptr[7] = /*_.IF___V7*/ meltfptr[6];;
      /*^compute */
      /*_.IFCPP___V6*/ meltfptr[5] = /*_.PROGN___V9*/ meltfptr[7];;
      /*epilog */

      MELT_LOCATION ("warmelt-macro.melt:6089:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L1*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IF___V7*/ meltfptr[6] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V9*/ meltfptr[7] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V6*/ meltfptr[5] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-macro.melt:6090:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L3*/ meltfnum[1] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.SEXPR__V2*/ meltfptr[1]),
			     (melt_ptr_t) (( /*!CLASS_SEXPR */ meltfrout->
					    tabval[1])));;
      MELT_LOCATION ("warmelt-macro.melt:6090:/ cond");
      /*cond */ if ( /*_#IS_A__L3*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V11*/ meltfptr[7] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-macro.melt:6090:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check sexpr"),
				  ("warmelt-macro.melt")
				  ? ("warmelt-macro.melt") : __FILE__,
				  (6090) ? (6090) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V11*/ meltfptr[7] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V10*/ meltfptr[6] = /*_.IFELSE___V11*/ meltfptr[7];;
      /*epilog */

      MELT_LOCATION ("warmelt-macro.melt:6090:/ clear");
	     /*clear *//*_#IS_A__L3*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V11*/ meltfptr[7] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V10*/ meltfptr[6] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-macro.melt:6091:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_OBJECT__L4*/ meltfnum[0] =
	(melt_magic_discr ((melt_ptr_t) ( /*_.MODCTX__V5*/ meltfptr[4])) ==
	 MELTOBMAG_OBJECT);;
      MELT_LOCATION ("warmelt-macro.melt:6091:/ cond");
      /*cond */ if ( /*_#IS_OBJECT__L4*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V13*/ meltfptr[12] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-macro.melt:6091:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check modctx"),
				  ("warmelt-macro.melt")
				  ? ("warmelt-macro.melt") : __FILE__,
				  (6091) ? (6091) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V13*/ meltfptr[12] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V12*/ meltfptr[7] = /*_.IFELSE___V13*/ meltfptr[12];;
      /*epilog */

      MELT_LOCATION ("warmelt-macro.melt:6091:/ clear");
	     /*clear *//*_#IS_OBJECT__L4*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V13*/ meltfptr[12] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V12*/ meltfptr[7] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-macro.melt:6092:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_CLOSURE__L5*/ meltfnum[1] =
	(melt_magic_discr ((melt_ptr_t) ( /*_.MEXPANDER__V4*/ meltfptr[3])) ==
	 MELTOBMAG_CLOSURE);;
      MELT_LOCATION ("warmelt-macro.melt:6092:/ cond");
      /*cond */ if ( /*_#IS_CLOSURE__L5*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V15*/ meltfptr[14] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-macro.melt:6092:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check mexpander"),
				  ("warmelt-macro.melt")
				  ? ("warmelt-macro.melt") : __FILE__,
				  (6092) ? (6092) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V15*/ meltfptr[14] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V14*/ meltfptr[12] = /*_.IFELSE___V15*/ meltfptr[14];;
      /*epilog */

      MELT_LOCATION ("warmelt-macro.melt:6092:/ clear");
	     /*clear *//*_#IS_CLOSURE__L5*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V15*/ meltfptr[14] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V14*/ meltfptr[12] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-macro.melt:6093:/ quasiblock");


    /*^getslot */
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.SEXPR__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 2, "SEXP_CONTENTS");
  /*_.CONT__V17*/ meltfptr[16] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-macro.melt:6094:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.SEXPR__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 1, "LOCA_LOCATION");
  /*_.LOC__V18*/ meltfptr[17] = slot;
    };
    ;
 /*_.LIST_FIRST__V19*/ meltfptr[18] =
      (melt_list_first ((melt_ptr_t) ( /*_.CONT__V17*/ meltfptr[16])));;
    /*^compute */
 /*_.CURPAIR__V20*/ meltfptr[19] =
      (melt_pair_tail ((melt_ptr_t) ( /*_.LIST_FIRST__V19*/ meltfptr[18])));;
    /*^compute */
 /*_.SLABNAM__V21*/ meltfptr[20] =
      (melt_pair_head ((melt_ptr_t) ( /*_.CURPAIR__V20*/ meltfptr[19])));;
    MELT_LOCATION ("warmelt-macro.melt:6097:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[3];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.ENV__V3*/ meltfptr[2];
      /*^apply.arg */
      argtab[1].meltbp_aptr =
	(melt_ptr_t *) & /*_.MEXPANDER__V4*/ meltfptr[3];
      /*^apply.arg */
      argtab[2].meltbp_aptr = (melt_ptr_t *) & /*_.MODCTX__V5*/ meltfptr[4];
      /*_.XLABNAM__V22*/ meltfptr[21] =
	melt_apply ((meltclosure_ptr_t) ( /*_.MEXPANDER__V4*/ meltfptr[3]),
		    (melt_ptr_t) ( /*_.SLABNAM__V21*/ meltfptr[20]),
		    (MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_PTR ""),
		    argtab, "", (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-macro.melt:6098:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      /*_.NEWENV__V23*/ meltfptr[22] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!FRESH_ENV */ meltfrout->tabval[2])),
		    (melt_ptr_t) ( /*_.ENV__V3*/ meltfptr[2]), (""),
		    (union meltparam_un *) 0, "", (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-macro.melt:6100:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#IS_NOT_A__L6*/ meltfnum[0] =
      !melt_is_instance_of ((melt_ptr_t) ( /*_.XLABNAM__V22*/ meltfptr[21]),
			    (melt_ptr_t) (( /*!CLASS_SYMBOL */ meltfrout->
					   tabval[3])));;
    MELT_LOCATION ("warmelt-macro.melt:6100:/ cond");
    /*cond */ if ( /*_#IS_NOT_A__L6*/ meltfnum[0])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{


	  {
	    MELT_LOCATION ("warmelt-macro.melt:6102:/ locexp");
	    /* error_plain */
	      melt_error_str ((melt_ptr_t) ( /*_.LOC__V18*/ meltfptr[17]),
			      ("missing label in EXIT"), (melt_ptr_t) 0);
	  }
	  ;
	  MELT_LOCATION ("warmelt-macro.melt:6103:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^quasiblock */


	  /*_.RETVAL___V1*/ meltfptr[0] = ( /*nil */ NULL);;

	  {
	    MELT_LOCATION ("warmelt-macro.melt:6103:/ locexp");
	    /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
	    if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	      melt_warn_for_no_expected_secondary_results ();
	    /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
	    ;
	  }
	  ;
	  /*^finalreturn */
	  ;
	  /*finalret */ goto labend_rout;
	  MELT_LOCATION ("warmelt-macro.melt:6101:/ quasiblock");


	  /*_.PROGN___V26*/ meltfptr[25] = /*_.RETURN___V25*/ meltfptr[24];;
	  /*^compute */
	  /*_.IF___V24*/ meltfptr[23] = /*_.PROGN___V26*/ meltfptr[25];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-macro.melt:6100:/ clear");
	     /*clear *//*_.RETURN___V25*/ meltfptr[24] = 0;
	  /*^clear */
	     /*clear *//*_.PROGN___V26*/ meltfptr[25] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

  /*_.IF___V24*/ meltfptr[23] = NULL;;
      }
    ;
    /*^compute */
 /*_.PAIR_TAIL__V27*/ meltfptr[24] =
      (melt_pair_tail ((melt_ptr_t) ( /*_.CURPAIR__V20*/ meltfptr[19])));;
    MELT_LOCATION ("warmelt-macro.melt:6104:/ compute");
    /*_.CURPAIR__V20*/ meltfptr[19] = /*_.SETQ___V28*/ meltfptr[25] =
      /*_.PAIR_TAIL__V27*/ meltfptr[24];;
    MELT_LOCATION ("warmelt-macro.melt:6105:/ quasiblock");


    /*^checksignal */
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & /*_.XLABNAM__V22*/ meltfptr[21];
      /*_.LABIND__V30*/ meltfptr[29] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!FIND_ENV */ meltfrout->tabval[4])),
		    (melt_ptr_t) ( /*_.ENV__V3*/ meltfptr[2]),
		    (MELTBPARSTR_PTR ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-macro.melt:6106:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#IS_NOT_A__L7*/ meltfnum[1] =
      !melt_is_instance_of ((melt_ptr_t) ( /*_.LABIND__V30*/ meltfptr[29]),
			    (melt_ptr_t) (( /*!CLASS_LABEL_BINDING */
					   meltfrout->tabval[5])));;
    MELT_LOCATION ("warmelt-macro.melt:6106:/ cond");
    /*cond */ if ( /*_#IS_NOT_A__L7*/ meltfnum[1])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-macro.melt:6109:/ getslot");
	  {
	    melt_ptr_t slot = NULL, obj = NULL;
	    obj = (melt_ptr_t) ( /*_.XLABNAM__V22*/ meltfptr[21]) /*=obj*/ ;
	    melt_object_get_field (slot, obj, 1, "NAMED_NAME");
    /*_.NAMED_NAME__V32*/ meltfptr[31] = slot;
	  };
	  ;

	  {
	    MELT_LOCATION ("warmelt-macro.melt:6108:/ locexp");
	    melt_error_str ((melt_ptr_t) ( /*_.LOC__V18*/ meltfptr[17]),
			    ("bad label in EXIT"),
			    (melt_ptr_t) ( /*_.NAMED_NAME__V32*/
					  meltfptr[31]));
	  }
	  ;
	  MELT_LOCATION ("warmelt-macro.melt:6110:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^quasiblock */


	  /*_.RETVAL___V1*/ meltfptr[0] = ( /*nil */ NULL);;

	  {
	    MELT_LOCATION ("warmelt-macro.melt:6110:/ locexp");
	    /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
	    if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	      melt_warn_for_no_expected_secondary_results ();
	    /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
	    ;
	  }
	  ;
	  /*^finalreturn */
	  ;
	  /*finalret */ goto labend_rout;
	  MELT_LOCATION ("warmelt-macro.melt:6107:/ quasiblock");


	  /*_.PROGN___V34*/ meltfptr[33] = /*_.RETURN___V33*/ meltfptr[32];;
	  /*^compute */
	  /*_.IF___V31*/ meltfptr[30] = /*_.PROGN___V34*/ meltfptr[33];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-macro.melt:6106:/ clear");
	     /*clear *//*_.NAMED_NAME__V32*/ meltfptr[31] = 0;
	  /*^clear */
	     /*clear *//*_.RETURN___V33*/ meltfptr[32] = 0;
	  /*^clear */
	     /*clear *//*_.PROGN___V34*/ meltfptr[33] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

  /*_.IF___V31*/ meltfptr[30] = NULL;;
      }
    ;
    MELT_LOCATION ("warmelt-macro.melt:6111:/ quasiblock");


    MELT_LOCATION ("warmelt-macro.melt:6114:/ quasiblock");


    /*^newclosure */
		 /*newclosure *//*_.LAMBDA___V37*/ meltfptr[33] =
      (melt_ptr_t)
      meltgc_new_closure ((meltobject_ptr_t)
			  (((melt_ptr_t) (MELT_PREDEF (DISCR_CLOSURE)))),
			  (meltroutine_ptr_t) (( /*!konst_8 */ meltfrout->
						tabval[8])), (3));
    ;
    /*^putclosedv */
    /*putclosv */
    melt_assertmsg ("putclosv checkclo",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.LAMBDA___V37*/ meltfptr[33])) ==
		    MELTOBMAG_CLOSURE);
    melt_assertmsg ("putclosv checkoff", 0 >= 0
		    && 0 <
		    melt_closure_size ((melt_ptr_t)
				       ( /*_.LAMBDA___V37*/ meltfptr[33])));
    ((meltclosure_ptr_t) /*_.LAMBDA___V37*/ meltfptr[33])->tabval[0] =
      (melt_ptr_t) ( /*_.MEXPANDER__V4*/ meltfptr[3]);
    ;
    /*^putclosedv */
    /*putclosv */
    melt_assertmsg ("putclosv checkclo",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.LAMBDA___V37*/ meltfptr[33])) ==
		    MELTOBMAG_CLOSURE);
    melt_assertmsg ("putclosv checkoff", 1 >= 0
		    && 1 <
		    melt_closure_size ((melt_ptr_t)
				       ( /*_.LAMBDA___V37*/ meltfptr[33])));
    ((meltclosure_ptr_t) /*_.LAMBDA___V37*/ meltfptr[33])->tabval[1] =
      (melt_ptr_t) ( /*_.NEWENV__V23*/ meltfptr[22]);
    ;
    /*^putclosedv */
    /*putclosv */
    melt_assertmsg ("putclosv checkclo",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.LAMBDA___V37*/ meltfptr[33])) ==
		    MELTOBMAG_CLOSURE);
    melt_assertmsg ("putclosv checkoff", 2 >= 0
		    && 2 <
		    melt_closure_size ((melt_ptr_t)
				       ( /*_.LAMBDA___V37*/ meltfptr[33])));
    ((meltclosure_ptr_t) /*_.LAMBDA___V37*/ meltfptr[33])->tabval[2] =
      (melt_ptr_t) ( /*_.MODCTX__V5*/ meltfptr[4]);
    ;
    /*_.LAMBDA___V36*/ meltfptr[32] = /*_.LAMBDA___V37*/ meltfptr[33];;
    MELT_LOCATION ("warmelt-macro.melt:6111:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[2];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & ( /*!DISCR_MULTIPLE */ meltfrout->tabval[7]);
      /*^apply.arg */
      argtab[1].meltbp_aptr =
	(melt_ptr_t *) & /*_.LAMBDA___V36*/ meltfptr[32];
      /*_.BODYTUP__V38*/ meltfptr[37] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!PAIRLIST_TO_MULTIPLE */ meltfrout->tabval[6])),
		    (melt_ptr_t) ( /*_.CURPAIR__V20*/ meltfptr[19]),
		    (MELTBPARSTR_PTR MELTBPARSTR_PTR ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-macro.melt:6116:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*^rawallocobj */
    /*rawallocobj */
    {
      melt_ptr_t newobj = 0;
      melt_raw_object_create (newobj,
			      (melt_ptr_t) (( /*!CLASS_SOURCE_EXIT */
					     meltfrout->tabval[9])), (4),
			      "CLASS_SOURCE_EXIT");
  /*_.INST__V40*/ meltfptr[39] =
	newobj;
    };
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @LOCA_LOCATION",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V40*/ meltfptr[39])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V40*/ meltfptr[39]), (1),
			  ( /*_.LOC__V18*/ meltfptr[17]), "LOCA_LOCATION");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @SLABEL_BIND",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V40*/ meltfptr[39])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V40*/ meltfptr[39]), (2),
			  ( /*_.LABIND__V30*/ meltfptr[29]), "SLABEL_BIND");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @SEXI_BODY",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V40*/ meltfptr[39])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V40*/ meltfptr[39]), (3),
			  ( /*_.BODYTUP__V38*/ meltfptr[37]), "SEXI_BODY");
    ;
    /*^touchobj */

    melt_dbgtrace_written_object ( /*_.INST__V40*/ meltfptr[39],
				  "newly made instance");
    ;
    /*_.EXR__V39*/ meltfptr[38] = /*_.INST__V40*/ meltfptr[39];;
    MELT_LOCATION ("warmelt-macro.melt:6121:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.EXR__V39*/ meltfptr[38];;

    {
      MELT_LOCATION ("warmelt-macro.melt:6121:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*_.LET___V35*/ meltfptr[31] = /*_.RETURN___V41*/ meltfptr[40];;

    MELT_LOCATION ("warmelt-macro.melt:6111:/ clear");
	   /*clear *//*_.LAMBDA___V36*/ meltfptr[32] = 0;
    /*^clear */
	   /*clear *//*_.BODYTUP__V38*/ meltfptr[37] = 0;
    /*^clear */
	   /*clear *//*_.EXR__V39*/ meltfptr[38] = 0;
    /*^clear */
	   /*clear *//*_.RETURN___V41*/ meltfptr[40] = 0;
    /*_.LET___V29*/ meltfptr[28] = /*_.LET___V35*/ meltfptr[31];;

    MELT_LOCATION ("warmelt-macro.melt:6105:/ clear");
	   /*clear *//*_.LABIND__V30*/ meltfptr[29] = 0;
    /*^clear */
	   /*clear *//*_#IS_NOT_A__L7*/ meltfnum[1] = 0;
    /*^clear */
	   /*clear *//*_.IF___V31*/ meltfptr[30] = 0;
    /*^clear */
	   /*clear *//*_.LET___V35*/ meltfptr[31] = 0;
    /*_.LET___V16*/ meltfptr[14] = /*_.LET___V29*/ meltfptr[28];;

    MELT_LOCATION ("warmelt-macro.melt:6093:/ clear");
	   /*clear *//*_.CONT__V17*/ meltfptr[16] = 0;
    /*^clear */
	   /*clear *//*_.LOC__V18*/ meltfptr[17] = 0;
    /*^clear */
	   /*clear *//*_.LIST_FIRST__V19*/ meltfptr[18] = 0;
    /*^clear */
	   /*clear *//*_.CURPAIR__V20*/ meltfptr[19] = 0;
    /*^clear */
	   /*clear *//*_.SLABNAM__V21*/ meltfptr[20] = 0;
    /*^clear */
	   /*clear *//*_.XLABNAM__V22*/ meltfptr[21] = 0;
    /*^clear */
	   /*clear *//*_.NEWENV__V23*/ meltfptr[22] = 0;
    /*^clear */
	   /*clear *//*_#IS_NOT_A__L6*/ meltfnum[0] = 0;
    /*^clear */
	   /*clear *//*_.IF___V24*/ meltfptr[23] = 0;
    /*^clear */
	   /*clear *//*_.PAIR_TAIL__V27*/ meltfptr[24] = 0;
    /*^clear */
	   /*clear *//*_.SETQ___V28*/ meltfptr[25] = 0;
    /*^clear */
	   /*clear *//*_.LET___V29*/ meltfptr[28] = 0;
    MELT_LOCATION ("warmelt-macro.melt:6088:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.LET___V16*/ meltfptr[14];;

    {
      MELT_LOCATION ("warmelt-macro.melt:6088:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V6*/ meltfptr[5] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V10*/ meltfptr[6] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V12*/ meltfptr[7] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V14*/ meltfptr[12] = 0;
    /*^clear */
	   /*clear *//*_.LET___V16*/ meltfptr[14] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("MEXPAND_EXIT", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_131_warmelt_macro_MEXPAND_EXIT_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_131_warmelt_macro_MEXPAND_EXIT */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_132_warmelt_macro_LAMBDA___37__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un * meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un * meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_132_warmelt_macro_LAMBDA___37___melt =
    melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_132_warmelt_macro_LAMBDA___37___st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 3
    melt_ptr_t mcfr_varptr[3];
/*no varnum*/
#define MELTFRAM_NBVARNUM /*none*/0
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_132_warmelt_macro_LAMBDA___37__ is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_132_warmelt_macro_LAMBDA___37___st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 3; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_132_warmelt_macro_LAMBDA___37__ nbval 3*/
  meltfram__.mcfr_nbvar = 3 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("LAMBDA_", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-macro.melt:6114:/ getarg");
 /*_.E__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {

    /*^checksignal */
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[3];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & ( /*~NEWENV */ meltfclos->tabval[1]);
      /*^apply.arg */
      argtab[1].meltbp_aptr =
	(melt_ptr_t *) & ( /*~MEXPANDER */ meltfclos->tabval[0]);
      /*^apply.arg */
      argtab[2].meltbp_aptr =
	(melt_ptr_t *) & ( /*~MODCTX */ meltfclos->tabval[2]);
      /*_.MEXPANDER__V3*/ meltfptr[2] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*~MEXPANDER */ meltfclos->tabval[0])),
		    (melt_ptr_t) ( /*_.E__V2*/ meltfptr[1]),
		    (MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_PTR ""),
		    argtab, "", (union meltparam_un *) 0);
    }
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.MEXPANDER__V3*/ meltfptr[2];;

    {
      MELT_LOCATION ("warmelt-macro.melt:6114:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.MEXPANDER__V3*/ meltfptr[2] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("LAMBDA_", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_132_warmelt_macro_LAMBDA___37___melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_132_warmelt_macro_LAMBDA___37__ */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_133_warmelt_macro_MEXPAND_AGAIN (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un * meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un * meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_133_warmelt_macro_MEXPAND_AGAIN_melt =
    melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_133_warmelt_macro_MEXPAND_AGAIN_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 44
    melt_ptr_t mcfr_varptr[44];
#define MELTFRAM_NBVARNUM 9
    long mcfr_varnum[9];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_133_warmelt_macro_MEXPAND_AGAIN is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_133_warmelt_macro_MEXPAND_AGAIN_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 44; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_133_warmelt_macro_MEXPAND_AGAIN nbval 44*/
  meltfram__.mcfr_nbvar = 44 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("MEXPAND_AGAIN", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-macro.melt:6131:/ getarg");
 /*_.SEXPR__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.ENV__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.ENV__V3*/ meltfptr[2])) != NULL);


  /*getarg#2 */
  /*^getarg */
  if (meltxargdescr_[1] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.MEXPANDER__V4*/ meltfptr[3] =
    (meltxargtab_[1].meltbp_aptr) ? (*(meltxargtab_[1].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.MEXPANDER__V4*/ meltfptr[3])) !=
	      NULL);


  /*getarg#3 */
  /*^getarg */
  if (meltxargdescr_[2] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.MODCTX__V5*/ meltfptr[4] =
    (meltxargtab_[2].meltbp_aptr) ? (*(meltxargtab_[2].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.MODCTX__V5*/ meltfptr[4])) !=
	      NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-macro.melt:6132:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L1*/ meltfnum[0] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-macro.melt:6132:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L1*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-macro.melt:6132:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-macro.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 6132;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "mexpand_again sexpr";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.SEXPR__V2*/ meltfptr[1];
	      /*_.MELT_DEBUG_FUN__V8*/ meltfptr[7] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V7*/ meltfptr[6] = /*_.MELT_DEBUG_FUN__V8*/ meltfptr[7];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-macro.melt:6132:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V8*/ meltfptr[7] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V7*/ meltfptr[6] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-macro.melt:6132:/ quasiblock");


      /*_.PROGN___V9*/ meltfptr[7] = /*_.IF___V7*/ meltfptr[6];;
      /*^compute */
      /*_.IFCPP___V6*/ meltfptr[5] = /*_.PROGN___V9*/ meltfptr[7];;
      /*epilog */

      MELT_LOCATION ("warmelt-macro.melt:6132:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L1*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IF___V7*/ meltfptr[6] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V9*/ meltfptr[7] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V6*/ meltfptr[5] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-macro.melt:6133:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L3*/ meltfnum[1] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.SEXPR__V2*/ meltfptr[1]),
			     (melt_ptr_t) (( /*!CLASS_SEXPR */ meltfrout->
					    tabval[1])));;
      MELT_LOCATION ("warmelt-macro.melt:6133:/ cond");
      /*cond */ if ( /*_#IS_A__L3*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V11*/ meltfptr[7] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-macro.melt:6133:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check sexpr"),
				  ("warmelt-macro.melt")
				  ? ("warmelt-macro.melt") : __FILE__,
				  (6133) ? (6133) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V11*/ meltfptr[7] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V10*/ meltfptr[6] = /*_.IFELSE___V11*/ meltfptr[7];;
      /*epilog */

      MELT_LOCATION ("warmelt-macro.melt:6133:/ clear");
	     /*clear *//*_#IS_A__L3*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V11*/ meltfptr[7] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V10*/ meltfptr[6] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-macro.melt:6134:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_CLOSURE__L4*/ meltfnum[0] =
	(melt_magic_discr ((melt_ptr_t) ( /*_.MEXPANDER__V4*/ meltfptr[3])) ==
	 MELTOBMAG_CLOSURE);;
      MELT_LOCATION ("warmelt-macro.melt:6134:/ cond");
      /*cond */ if ( /*_#IS_CLOSURE__L4*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V13*/ meltfptr[12] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-macro.melt:6134:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check mexpander"),
				  ("warmelt-macro.melt")
				  ? ("warmelt-macro.melt") : __FILE__,
				  (6134) ? (6134) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V13*/ meltfptr[12] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V12*/ meltfptr[7] = /*_.IFELSE___V13*/ meltfptr[12];;
      /*epilog */

      MELT_LOCATION ("warmelt-macro.melt:6134:/ clear");
	     /*clear *//*_#IS_CLOSURE__L4*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V13*/ meltfptr[12] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V12*/ meltfptr[7] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-macro.melt:6135:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_OBJECT__L5*/ meltfnum[1] =
	(melt_magic_discr ((melt_ptr_t) ( /*_.MODCTX__V5*/ meltfptr[4])) ==
	 MELTOBMAG_OBJECT);;
      MELT_LOCATION ("warmelt-macro.melt:6135:/ cond");
      /*cond */ if ( /*_#IS_OBJECT__L5*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V15*/ meltfptr[14] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-macro.melt:6135:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check modctx"),
				  ("warmelt-macro.melt")
				  ? ("warmelt-macro.melt") : __FILE__,
				  (6135) ? (6135) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V15*/ meltfptr[14] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V14*/ meltfptr[12] = /*_.IFELSE___V15*/ meltfptr[14];;
      /*epilog */

      MELT_LOCATION ("warmelt-macro.melt:6135:/ clear");
	     /*clear *//*_#IS_OBJECT__L5*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V15*/ meltfptr[14] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V14*/ meltfptr[12] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-macro.melt:6136:/ quasiblock");


    /*^getslot */
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.SEXPR__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 2, "SEXP_CONTENTS");
  /*_.CONT__V17*/ meltfptr[16] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-macro.melt:6137:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.SEXPR__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 1, "LOCA_LOCATION");
  /*_.LOC__V18*/ meltfptr[17] = slot;
    };
    ;
 /*_.LIST_FIRST__V19*/ meltfptr[18] =
      (melt_list_first ((melt_ptr_t) ( /*_.CONT__V17*/ meltfptr[16])));;
    /*^compute */
 /*_.CURPAIR__V20*/ meltfptr[19] =
      (melt_pair_tail ((melt_ptr_t) ( /*_.LIST_FIRST__V19*/ meltfptr[18])));;
    /*^compute */
 /*_.SLABNAM__V21*/ meltfptr[20] =
      (melt_pair_head ((melt_ptr_t) ( /*_.CURPAIR__V20*/ meltfptr[19])));;
    MELT_LOCATION ("warmelt-macro.melt:6140:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[3];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.ENV__V3*/ meltfptr[2];
      /*^apply.arg */
      argtab[1].meltbp_aptr =
	(melt_ptr_t *) & /*_.MEXPANDER__V4*/ meltfptr[3];
      /*^apply.arg */
      argtab[2].meltbp_aptr = (melt_ptr_t *) & /*_.MODCTX__V5*/ meltfptr[4];
      /*_.XLABNAM__V22*/ meltfptr[21] =
	melt_apply ((meltclosure_ptr_t) ( /*_.MEXPANDER__V4*/ meltfptr[3]),
		    (melt_ptr_t) ( /*_.SLABNAM__V21*/ meltfptr[20]),
		    (MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_PTR ""),
		    argtab, "", (union meltparam_un *) 0);
    }
    ;
 /*_.PAIR_TAIL__V23*/ meltfptr[22] =
      (melt_pair_tail ((melt_ptr_t) ( /*_.CURPAIR__V20*/ meltfptr[19])));;
    MELT_LOCATION ("warmelt-macro.melt:6142:/ compute");
    /*_.CURPAIR__V20*/ meltfptr[19] = /*_.SETQ___V24*/ meltfptr[23] =
      /*_.PAIR_TAIL__V23*/ meltfptr[22];;
    MELT_LOCATION ("warmelt-macro.melt:6143:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#IS_NOT_A__L6*/ meltfnum[0] =
      !melt_is_instance_of ((melt_ptr_t) ( /*_.XLABNAM__V22*/ meltfptr[21]),
			    (melt_ptr_t) (( /*!CLASS_SYMBOL */ meltfrout->
					   tabval[2])));;
    MELT_LOCATION ("warmelt-macro.melt:6143:/ cond");
    /*cond */ if ( /*_#IS_NOT_A__L6*/ meltfnum[0])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{


	  {
	    MELT_LOCATION ("warmelt-macro.melt:6145:/ locexp");
	    /* error_plain */
	      melt_error_str ((melt_ptr_t) ( /*_.LOC__V18*/ meltfptr[17]),
			      ("bad label in (AGAIN <label>)"),
			      (melt_ptr_t) 0);
	  }
	  ;
	  MELT_LOCATION ("warmelt-macro.melt:6146:/ quasiblock");


   /*_.RETVAL___V1*/ meltfptr[0] = NULL;;

	  {
	    MELT_LOCATION ("warmelt-macro.melt:6146:/ locexp");
	    /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
	    if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	      melt_warn_for_no_expected_secondary_results ();
	    /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
	    ;
	  }
	  ;
	  /*^finalreturn */
	  ;
	  /*finalret */ goto labend_rout;
	  MELT_LOCATION ("warmelt-macro.melt:6144:/ quasiblock");


	  /*_.PROGN___V27*/ meltfptr[26] = /*_.RETURN___V26*/ meltfptr[25];;
	  /*^compute */
	  /*_.IF___V25*/ meltfptr[24] = /*_.PROGN___V27*/ meltfptr[26];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-macro.melt:6143:/ clear");
	     /*clear *//*_.RETURN___V26*/ meltfptr[25] = 0;
	  /*^clear */
	     /*clear *//*_.PROGN___V27*/ meltfptr[26] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

  /*_.IF___V25*/ meltfptr[24] = NULL;;
      }
    ;
    MELT_LOCATION ("warmelt-macro.melt:6147:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^cond */
    /*cond */ if ( /*_.CURPAIR__V20*/ meltfptr[19])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{


	  {
	    MELT_LOCATION ("warmelt-macro.melt:6149:/ locexp");
	    /* error_plain */
	      melt_error_str ((melt_ptr_t) ( /*_.LOC__V18*/ meltfptr[17]),
			      ("extra operands to (AGAIN <label>)"),
			      (melt_ptr_t) 0);
	  }
	  ;
	  MELT_LOCATION ("warmelt-macro.melt:6150:/ quasiblock");


   /*_.RETVAL___V1*/ meltfptr[0] = NULL;;

	  {
	    MELT_LOCATION ("warmelt-macro.melt:6150:/ locexp");
	    /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
	    if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	      melt_warn_for_no_expected_secondary_results ();
	    /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
	    ;
	  }
	  ;
	  /*^finalreturn */
	  ;
	  /*finalret */ goto labend_rout;
	  MELT_LOCATION ("warmelt-macro.melt:6148:/ quasiblock");


	  /*_.PROGN___V30*/ meltfptr[29] = /*_.RETURN___V29*/ meltfptr[26];;
	  /*^compute */
	  /*_.IF___V28*/ meltfptr[25] = /*_.PROGN___V30*/ meltfptr[29];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-macro.melt:6147:/ clear");
	     /*clear *//*_.RETURN___V29*/ meltfptr[26] = 0;
	  /*^clear */
	     /*clear *//*_.PROGN___V30*/ meltfptr[29] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

  /*_.IF___V28*/ meltfptr[25] = NULL;;
      }
    ;
    MELT_LOCATION ("warmelt-macro.melt:6151:/ quasiblock");


    /*^checksignal */
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & /*_.XLABNAM__V22*/ meltfptr[21];
      /*_.LABIND__V32*/ meltfptr[29] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!FIND_ENV */ meltfrout->tabval[3])),
		    (melt_ptr_t) ( /*_.ENV__V3*/ meltfptr[2]),
		    (MELTBPARSTR_PTR ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-macro.melt:6152:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#IS_NOT_A__L7*/ meltfnum[1] =
      !melt_is_instance_of ((melt_ptr_t) ( /*_.LABIND__V32*/ meltfptr[29]),
			    (melt_ptr_t) (( /*!CLASS_LABEL_BINDING */
					   meltfrout->tabval[4])));;
    MELT_LOCATION ("warmelt-macro.melt:6152:/ cond");
    /*cond */ if ( /*_#IS_NOT_A__L7*/ meltfnum[1])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-macro.melt:6155:/ getslot");
	  {
	    melt_ptr_t slot = NULL, obj = NULL;
	    obj = (melt_ptr_t) ( /*_.XLABNAM__V22*/ meltfptr[21]) /*=obj*/ ;
	    melt_object_get_field (slot, obj, 1, "NAMED_NAME");
    /*_.NAMED_NAME__V34*/ meltfptr[33] = slot;
	  };
	  ;

	  {
	    MELT_LOCATION ("warmelt-macro.melt:6154:/ locexp");
	    melt_error_str ((melt_ptr_t) ( /*_.LOC__V18*/ meltfptr[17]),
			    ("bad label in AGAIN"),
			    (melt_ptr_t) ( /*_.NAMED_NAME__V34*/
					  meltfptr[33]));
	  }
	  ;
	  MELT_LOCATION ("warmelt-macro.melt:6156:/ quasiblock");


   /*_.RETVAL___V1*/ meltfptr[0] = NULL;;

	  {
	    MELT_LOCATION ("warmelt-macro.melt:6156:/ locexp");
	    /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
	    if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	      melt_warn_for_no_expected_secondary_results ();
	    /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
	    ;
	  }
	  ;
	  /*^finalreturn */
	  ;
	  /*finalret */ goto labend_rout;
	  MELT_LOCATION ("warmelt-macro.melt:6153:/ quasiblock");


	  /*_.PROGN___V36*/ meltfptr[35] = /*_.RETURN___V35*/ meltfptr[34];;
	  /*^compute */
	  /*_.IF___V33*/ meltfptr[32] = /*_.PROGN___V36*/ meltfptr[35];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-macro.melt:6152:/ clear");
	     /*clear *//*_.NAMED_NAME__V34*/ meltfptr[33] = 0;
	  /*^clear */
	     /*clear *//*_.RETURN___V35*/ meltfptr[34] = 0;
	  /*^clear */
	     /*clear *//*_.PROGN___V36*/ meltfptr[35] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

  /*_.IF___V33*/ meltfptr[32] = NULL;;
      }
    ;
    MELT_LOCATION ("warmelt-macro.melt:6157:/ quasiblock");


    /*^checksignal */
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*^rawallocobj */
    /*rawallocobj */
    {
      melt_ptr_t newobj = 0;
      melt_raw_object_create (newobj,
			      (melt_ptr_t) (( /*!CLASS_SOURCE_AGAIN */
					     meltfrout->tabval[5])), (3),
			      "CLASS_SOURCE_AGAIN");
  /*_.INST__V39*/ meltfptr[35] =
	newobj;
    };
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @LOCA_LOCATION",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V39*/ meltfptr[35])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V39*/ meltfptr[35]), (1),
			  ( /*_.LOC__V18*/ meltfptr[17]), "LOCA_LOCATION");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @SLABEL_BIND",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V39*/ meltfptr[35])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V39*/ meltfptr[35]), (2),
			  ( /*_.LABIND__V32*/ meltfptr[29]), "SLABEL_BIND");
    ;
    /*^touchobj */

    melt_dbgtrace_written_object ( /*_.INST__V39*/ meltfptr[35],
				  "newly made instance");
    ;
    /*_.MAGAIN__V38*/ meltfptr[34] = /*_.INST__V39*/ meltfptr[35];;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-macro.melt:6162:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L8*/ meltfnum[7] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-macro.melt:6162:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L8*/ meltfnum[7])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L9*/ meltfnum[8] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-macro.melt:6162:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L9*/ meltfnum[8];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-macro.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 6162;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "mexpand_again result";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.MAGAIN__V38*/ meltfptr[34];
	      /*_.MELT_DEBUG_FUN__V42*/ meltfptr[41] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V41*/ meltfptr[40] =
	      /*_.MELT_DEBUG_FUN__V42*/ meltfptr[41];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-macro.melt:6162:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L9*/ meltfnum[8] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V42*/ meltfptr[41] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V41*/ meltfptr[40] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-macro.melt:6162:/ quasiblock");


      /*_.PROGN___V43*/ meltfptr[41] = /*_.IF___V41*/ meltfptr[40];;
      /*^compute */
      /*_.IFCPP___V40*/ meltfptr[39] = /*_.PROGN___V43*/ meltfptr[41];;
      /*epilog */

      MELT_LOCATION ("warmelt-macro.melt:6162:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L8*/ meltfnum[7] = 0;
      /*^clear */
	     /*clear *//*_.IF___V41*/ meltfptr[40] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V43*/ meltfptr[41] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V40*/ meltfptr[39] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-macro.melt:6163:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.MAGAIN__V38*/ meltfptr[34];;

    {
      MELT_LOCATION ("warmelt-macro.melt:6163:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*_.LET___V37*/ meltfptr[33] = /*_.RETURN___V44*/ meltfptr[40];;

    MELT_LOCATION ("warmelt-macro.melt:6157:/ clear");
	   /*clear *//*_.MAGAIN__V38*/ meltfptr[34] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V40*/ meltfptr[39] = 0;
    /*^clear */
	   /*clear *//*_.RETURN___V44*/ meltfptr[40] = 0;
    /*_.LET___V31*/ meltfptr[26] = /*_.LET___V37*/ meltfptr[33];;

    MELT_LOCATION ("warmelt-macro.melt:6151:/ clear");
	   /*clear *//*_.LABIND__V32*/ meltfptr[29] = 0;
    /*^clear */
	   /*clear *//*_#IS_NOT_A__L7*/ meltfnum[1] = 0;
    /*^clear */
	   /*clear *//*_.IF___V33*/ meltfptr[32] = 0;
    /*^clear */
	   /*clear *//*_.LET___V37*/ meltfptr[33] = 0;
    /*_.LET___V16*/ meltfptr[14] = /*_.LET___V31*/ meltfptr[26];;

    MELT_LOCATION ("warmelt-macro.melt:6136:/ clear");
	   /*clear *//*_.CONT__V17*/ meltfptr[16] = 0;
    /*^clear */
	   /*clear *//*_.LOC__V18*/ meltfptr[17] = 0;
    /*^clear */
	   /*clear *//*_.LIST_FIRST__V19*/ meltfptr[18] = 0;
    /*^clear */
	   /*clear *//*_.CURPAIR__V20*/ meltfptr[19] = 0;
    /*^clear */
	   /*clear *//*_.SLABNAM__V21*/ meltfptr[20] = 0;
    /*^clear */
	   /*clear *//*_.XLABNAM__V22*/ meltfptr[21] = 0;
    /*^clear */
	   /*clear *//*_.PAIR_TAIL__V23*/ meltfptr[22] = 0;
    /*^clear */
	   /*clear *//*_.SETQ___V24*/ meltfptr[23] = 0;
    /*^clear */
	   /*clear *//*_#IS_NOT_A__L6*/ meltfnum[0] = 0;
    /*^clear */
	   /*clear *//*_.IF___V25*/ meltfptr[24] = 0;
    /*^clear */
	   /*clear *//*_.IF___V28*/ meltfptr[25] = 0;
    /*^clear */
	   /*clear *//*_.LET___V31*/ meltfptr[26] = 0;
    MELT_LOCATION ("warmelt-macro.melt:6131:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.LET___V16*/ meltfptr[14];;

    {
      MELT_LOCATION ("warmelt-macro.melt:6131:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V6*/ meltfptr[5] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V10*/ meltfptr[6] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V12*/ meltfptr[7] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V14*/ meltfptr[12] = 0;
    /*^clear */
	   /*clear *//*_.LET___V16*/ meltfptr[14] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("MEXPAND_AGAIN", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_133_warmelt_macro_MEXPAND_AGAIN_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_133_warmelt_macro_MEXPAND_AGAIN */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_134_warmelt_macro_MEXPAND_COMPILE_WARNING (meltclosure_ptr_t
						    meltclosp_,
						    melt_ptr_t meltfirstargp_,
						    const melt_argdescr_cell_t
						    meltxargdescr_[],
						    union meltparam_un *
						    meltxargtab_,
						    const melt_argdescr_cell_t
						    meltxresdescr_[],
						    union meltparam_un *
						    meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_134_warmelt_macro_MEXPAND_COMPILE_WARNING_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_134_warmelt_macro_MEXPAND_COMPILE_WARNING_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 40
    melt_ptr_t mcfr_varptr[40];
#define MELTFRAM_NBVARNUM 10
    long mcfr_varnum[10];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_134_warmelt_macro_MEXPAND_COMPILE_WARNING is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_134_warmelt_macro_MEXPAND_COMPILE_WARNING_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 40; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_134_warmelt_macro_MEXPAND_COMPILE_WARNING nbval 40*/
  meltfram__.mcfr_nbvar = 40 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("MEXPAND_COMPILE_WARNING", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-macro.melt:6175:/ getarg");
 /*_.SEXPR__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.ENV__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.ENV__V3*/ meltfptr[2])) != NULL);


  /*getarg#2 */
  /*^getarg */
  if (meltxargdescr_[1] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.MEXPANDER__V4*/ meltfptr[3] =
    (meltxargtab_[1].meltbp_aptr) ? (*(meltxargtab_[1].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.MEXPANDER__V4*/ meltfptr[3])) !=
	      NULL);


  /*getarg#3 */
  /*^getarg */
  if (meltxargdescr_[2] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.MODCTX__V5*/ meltfptr[4] =
    (meltxargtab_[2].meltbp_aptr) ? (*(meltxargtab_[2].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.MODCTX__V5*/ meltfptr[4])) !=
	      NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-macro.melt:6176:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L1*/ meltfnum[0] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-macro.melt:6176:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L1*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-macro.melt:6176:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-macro.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 6176;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "mexpand_compile_warning sexpr";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.SEXPR__V2*/ meltfptr[1];
	      /*_.MELT_DEBUG_FUN__V8*/ meltfptr[7] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V7*/ meltfptr[6] = /*_.MELT_DEBUG_FUN__V8*/ meltfptr[7];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-macro.melt:6176:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V8*/ meltfptr[7] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V7*/ meltfptr[6] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-macro.melt:6176:/ quasiblock");


      /*_.PROGN___V9*/ meltfptr[7] = /*_.IF___V7*/ meltfptr[6];;
      /*^compute */
      /*_.IFCPP___V6*/ meltfptr[5] = /*_.PROGN___V9*/ meltfptr[7];;
      /*epilog */

      MELT_LOCATION ("warmelt-macro.melt:6176:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L1*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IF___V7*/ meltfptr[6] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V9*/ meltfptr[7] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V6*/ meltfptr[5] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-macro.melt:6177:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L3*/ meltfnum[1] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.SEXPR__V2*/ meltfptr[1]),
			     (melt_ptr_t) (( /*!CLASS_SEXPR */ meltfrout->
					    tabval[1])));;
      MELT_LOCATION ("warmelt-macro.melt:6177:/ cond");
      /*cond */ if ( /*_#IS_A__L3*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V11*/ meltfptr[7] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-macro.melt:6177:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check sexpr"),
				  ("warmelt-macro.melt")
				  ? ("warmelt-macro.melt") : __FILE__,
				  (6177) ? (6177) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V11*/ meltfptr[7] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V10*/ meltfptr[6] = /*_.IFELSE___V11*/ meltfptr[7];;
      /*epilog */

      MELT_LOCATION ("warmelt-macro.melt:6177:/ clear");
	     /*clear *//*_#IS_A__L3*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V11*/ meltfptr[7] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V10*/ meltfptr[6] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-macro.melt:6178:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_OBJECT__L4*/ meltfnum[0] =
	(melt_magic_discr ((melt_ptr_t) ( /*_.MODCTX__V5*/ meltfptr[4])) ==
	 MELTOBMAG_OBJECT);;
      MELT_LOCATION ("warmelt-macro.melt:6178:/ cond");
      /*cond */ if ( /*_#IS_OBJECT__L4*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V13*/ meltfptr[12] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-macro.melt:6178:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check modctx"),
				  ("warmelt-macro.melt")
				  ? ("warmelt-macro.melt") : __FILE__,
				  (6178) ? (6178) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V13*/ meltfptr[12] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V12*/ meltfptr[7] = /*_.IFELSE___V13*/ meltfptr[12];;
      /*epilog */

      MELT_LOCATION ("warmelt-macro.melt:6178:/ clear");
	     /*clear *//*_#IS_OBJECT__L4*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V13*/ meltfptr[12] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V12*/ meltfptr[7] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-macro.melt:6179:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#NULL__L5*/ meltfnum[1] =
      (( /*_.MEXPANDER__V4*/ meltfptr[3]) == NULL);;
    MELT_LOCATION ("warmelt-macro.melt:6179:/ cond");
    /*cond */ if ( /*_#NULL__L5*/ meltfnum[1])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  /*^compute */
	  /*_.MEXPANDER__V4*/ meltfptr[3] = /*_.SETQ___V15*/ meltfptr[14] =
	    ( /*!MACROEXPAND_1 */ meltfrout->tabval[2]);;
	  /*_.IF___V14*/ meltfptr[12] = /*_.SETQ___V15*/ meltfptr[14];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-macro.melt:6179:/ clear");
	     /*clear *//*_.SETQ___V15*/ meltfptr[14] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

  /*_.IF___V14*/ meltfptr[12] = NULL;;
      }
    ;
    MELT_LOCATION ("warmelt-macro.melt:6180:/ quasiblock");


    /*^getslot */
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.SEXPR__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 2, "SEXP_CONTENTS");
  /*_.CONT__V17*/ meltfptr[16] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-macro.melt:6181:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.SEXPR__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 1, "LOCA_LOCATION");
  /*_.LOC__V18*/ meltfptr[17] = slot;
    };
    ;
 /*_.LIST_FIRST__V19*/ meltfptr[18] =
      (melt_list_first ((melt_ptr_t) ( /*_.CONT__V17*/ meltfptr[16])));;
    /*^compute */
 /*_.CURPAIR__V20*/ meltfptr[19] =
      (melt_pair_tail ((melt_ptr_t) ( /*_.LIST_FIRST__V19*/ meltfptr[18])));;
    /*^compute */
 /*_.SMSG__V21*/ meltfptr[20] =
      (melt_pair_head ((melt_ptr_t) ( /*_.CURPAIR__V20*/ meltfptr[19])));;
    MELT_LOCATION ("warmelt-macro.melt:6184:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[3];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.ENV__V3*/ meltfptr[2];
      /*^apply.arg */
      argtab[1].meltbp_aptr =
	(melt_ptr_t *) & /*_.MEXPANDER__V4*/ meltfptr[3];
      /*^apply.arg */
      argtab[2].meltbp_aptr = (melt_ptr_t *) & /*_.MODCTX__V5*/ meltfptr[4];
      /*_.XMSG__V22*/ meltfptr[21] =
	melt_apply ((meltclosure_ptr_t) ( /*_.MEXPANDER__V4*/ meltfptr[3]),
		    (melt_ptr_t) ( /*_.SMSG__V21*/ meltfptr[20]),
		    (MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_PTR ""),
		    argtab, "", (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-macro.melt:6186:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#IS_STRING__L6*/ meltfnum[0] =
      (melt_magic_discr ((melt_ptr_t) ( /*_.XMSG__V22*/ meltfptr[21])) ==
       MELTOBMAG_STRING);;
    /*^compute */
 /*_#NOT__L7*/ meltfnum[6] =
      (!( /*_#IS_STRING__L6*/ meltfnum[0]));;
    MELT_LOCATION ("warmelt-macro.melt:6186:/ cond");
    /*cond */ if ( /*_#NOT__L7*/ meltfnum[6])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{


	  {
	    MELT_LOCATION ("warmelt-macro.melt:6188:/ locexp");
	    /* error_plain */
	      melt_error_str ((melt_ptr_t) ( /*_.LOC__V18*/ meltfptr[17]),
			      ("missing message string in (COMPILE_WARNING <msg> <exp>)"),
			      (melt_ptr_t) 0);
	  }
	  ;
	  MELT_LOCATION ("warmelt-macro.melt:6189:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^quasiblock */


	  /*_.RETVAL___V1*/ meltfptr[0] = ( /*nil */ NULL);;

	  {
	    MELT_LOCATION ("warmelt-macro.melt:6189:/ locexp");
	    /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
	    if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	      melt_warn_for_no_expected_secondary_results ();
	    /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
	    ;
	  }
	  ;
	  /*^finalreturn */
	  ;
	  /*finalret */ goto labend_rout;
	  MELT_LOCATION ("warmelt-macro.melt:6187:/ quasiblock");


	  /*_.PROGN___V25*/ meltfptr[24] = /*_.RETURN___V24*/ meltfptr[23];;
	  /*^compute */
	  /*_.IF___V23*/ meltfptr[22] = /*_.PROGN___V25*/ meltfptr[24];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-macro.melt:6186:/ clear");
	     /*clear *//*_.RETURN___V24*/ meltfptr[23] = 0;
	  /*^clear */
	     /*clear *//*_.PROGN___V25*/ meltfptr[24] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

  /*_.IF___V23*/ meltfptr[22] = NULL;;
      }
    ;
    /*^compute */
 /*_.PAIR_TAIL__V26*/ meltfptr[23] =
      (melt_pair_tail ((melt_ptr_t) ( /*_.CURPAIR__V20*/ meltfptr[19])));;
    MELT_LOCATION ("warmelt-macro.melt:6190:/ compute");
    /*_.CURPAIR__V20*/ meltfptr[19] = /*_.SETQ___V27*/ meltfptr[24] =
      /*_.PAIR_TAIL__V26*/ meltfptr[23];;
    MELT_LOCATION ("warmelt-macro.melt:6191:/ quasiblock");


 /*_.SEXP__V29*/ meltfptr[28] =
      (melt_pair_head ((melt_ptr_t) ( /*_.CURPAIR__V20*/ meltfptr[19])));;
    MELT_LOCATION ("warmelt-macro.melt:6192:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[3];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.ENV__V3*/ meltfptr[2];
      /*^apply.arg */
      argtab[1].meltbp_aptr =
	(melt_ptr_t *) & /*_.MEXPANDER__V4*/ meltfptr[3];
      /*^apply.arg */
      argtab[2].meltbp_aptr = (melt_ptr_t *) & /*_.MODCTX__V5*/ meltfptr[4];
      /*_.XEXP__V30*/ meltfptr[29] =
	melt_apply ((meltclosure_ptr_t) ( /*_.MEXPANDER__V4*/ meltfptr[3]),
		    (melt_ptr_t) ( /*_.SEXP__V29*/ meltfptr[28]),
		    (MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_PTR ""),
		    argtab, "", (union meltparam_un *) 0);
    }
    ;
 /*_.PAIR_TAIL__V31*/ meltfptr[30] =
      (melt_pair_tail ((melt_ptr_t) ( /*_.CURPAIR__V20*/ meltfptr[19])));;
    MELT_LOCATION ("warmelt-macro.melt:6194:/ compute");
    /*_.CURPAIR__V20*/ meltfptr[19] = /*_.SETQ___V32*/ meltfptr[31] =
      /*_.PAIR_TAIL__V31*/ meltfptr[30];;
    MELT_LOCATION ("warmelt-macro.melt:6195:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#NOTNULL__L8*/ meltfnum[7] =
      (( /*_.CURPAIR__V20*/ meltfptr[19]) != NULL);;
    MELT_LOCATION ("warmelt-macro.melt:6195:/ cond");
    /*cond */ if ( /*_#NOTNULL__L8*/ meltfnum[7])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{


	  {
	    MELT_LOCATION ("warmelt-macro.melt:6196:/ locexp");
	    /* error_plain */
	      melt_error_str ((melt_ptr_t) ( /*_.LOC__V18*/ meltfptr[17]),
			      ("too many arguments in (COMPILE_WARNING <msg> [<exp>])"),
			      (melt_ptr_t) 0);
	  }
	  ;
	  /*epilog */
	}
	;
      }				/*noelse */
    ;
    MELT_LOCATION ("warmelt-macro.melt:6197:/ quasiblock");


    /*^checksignal */
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*^rawallocobj */
    /*rawallocobj */
    {
      melt_ptr_t newobj = 0;
      melt_raw_object_create (newobj,
			      (melt_ptr_t) (( /*!CLASS_SOURCE_COMPILEWARNING */ meltfrout->tabval[3])), (4), "CLASS_SOURCE_COMPILEWARNING");
  /*_.INST__V35*/ meltfptr[34] =
	newobj;
    };
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @LOCA_LOCATION",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V35*/ meltfptr[34])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V35*/ meltfptr[34]), (1),
			  ( /*_.LOC__V18*/ meltfptr[17]), "LOCA_LOCATION");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @SCWARN_MSG",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V35*/ meltfptr[34])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V35*/ meltfptr[34]), (2),
			  ( /*_.XMSG__V22*/ meltfptr[21]), "SCWARN_MSG");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @SCWARN_EXPR",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V35*/ meltfptr[34])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V35*/ meltfptr[34]), (3),
			  ( /*_.XEXP__V30*/ meltfptr[29]), "SCWARN_EXPR");
    ;
    /*^touchobj */

    melt_dbgtrace_written_object ( /*_.INST__V35*/ meltfptr[34],
				  "newly made instance");
    ;
    /*_.RES__V34*/ meltfptr[33] = /*_.INST__V35*/ meltfptr[34];;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-macro.melt:6203:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L9*/ meltfnum[8] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-macro.melt:6203:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L9*/ meltfnum[8])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L10*/ meltfnum[9] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-macro.melt:6203:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long =
		/*_#THE_MELTCALLCOUNT__L10*/ meltfnum[9];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-macro.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 6203;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "mexpand_compile_warning result";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.RES__V34*/ meltfptr[33];
	      /*_.MELT_DEBUG_FUN__V38*/ meltfptr[37] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V37*/ meltfptr[36] =
	      /*_.MELT_DEBUG_FUN__V38*/ meltfptr[37];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-macro.melt:6203:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L10*/ meltfnum[9] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V38*/ meltfptr[37] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V37*/ meltfptr[36] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-macro.melt:6203:/ quasiblock");


      /*_.PROGN___V39*/ meltfptr[37] = /*_.IF___V37*/ meltfptr[36];;
      /*^compute */
      /*_.IFCPP___V36*/ meltfptr[35] = /*_.PROGN___V39*/ meltfptr[37];;
      /*epilog */

      MELT_LOCATION ("warmelt-macro.melt:6203:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L9*/ meltfnum[8] = 0;
      /*^clear */
	     /*clear *//*_.IF___V37*/ meltfptr[36] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V39*/ meltfptr[37] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V36*/ meltfptr[35] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-macro.melt:6204:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.RES__V34*/ meltfptr[33];;

    {
      MELT_LOCATION ("warmelt-macro.melt:6204:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*_.LET___V33*/ meltfptr[32] = /*_.RETURN___V40*/ meltfptr[36];;

    MELT_LOCATION ("warmelt-macro.melt:6197:/ clear");
	   /*clear *//*_.RES__V34*/ meltfptr[33] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V36*/ meltfptr[35] = 0;
    /*^clear */
	   /*clear *//*_.RETURN___V40*/ meltfptr[36] = 0;
    /*_.LET___V28*/ meltfptr[27] = /*_.LET___V33*/ meltfptr[32];;

    MELT_LOCATION ("warmelt-macro.melt:6191:/ clear");
	   /*clear *//*_.SEXP__V29*/ meltfptr[28] = 0;
    /*^clear */
	   /*clear *//*_.XEXP__V30*/ meltfptr[29] = 0;
    /*^clear */
	   /*clear *//*_.PAIR_TAIL__V31*/ meltfptr[30] = 0;
    /*^clear */
	   /*clear *//*_.SETQ___V32*/ meltfptr[31] = 0;
    /*^clear */
	   /*clear *//*_#NOTNULL__L8*/ meltfnum[7] = 0;
    /*^clear */
	   /*clear *//*_.LET___V33*/ meltfptr[32] = 0;
    /*_.LET___V16*/ meltfptr[14] = /*_.LET___V28*/ meltfptr[27];;

    MELT_LOCATION ("warmelt-macro.melt:6180:/ clear");
	   /*clear *//*_.CONT__V17*/ meltfptr[16] = 0;
    /*^clear */
	   /*clear *//*_.LOC__V18*/ meltfptr[17] = 0;
    /*^clear */
	   /*clear *//*_.LIST_FIRST__V19*/ meltfptr[18] = 0;
    /*^clear */
	   /*clear *//*_.CURPAIR__V20*/ meltfptr[19] = 0;
    /*^clear */
	   /*clear *//*_.SMSG__V21*/ meltfptr[20] = 0;
    /*^clear */
	   /*clear *//*_.XMSG__V22*/ meltfptr[21] = 0;
    /*^clear */
	   /*clear *//*_#IS_STRING__L6*/ meltfnum[0] = 0;
    /*^clear */
	   /*clear *//*_#NOT__L7*/ meltfnum[6] = 0;
    /*^clear */
	   /*clear *//*_.IF___V23*/ meltfptr[22] = 0;
    /*^clear */
	   /*clear *//*_.PAIR_TAIL__V26*/ meltfptr[23] = 0;
    /*^clear */
	   /*clear *//*_.SETQ___V27*/ meltfptr[24] = 0;
    /*^clear */
	   /*clear *//*_.LET___V28*/ meltfptr[27] = 0;
    MELT_LOCATION ("warmelt-macro.melt:6175:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.LET___V16*/ meltfptr[14];;

    {
      MELT_LOCATION ("warmelt-macro.melt:6175:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V6*/ meltfptr[5] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V10*/ meltfptr[6] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V12*/ meltfptr[7] = 0;
    /*^clear */
	   /*clear *//*_#NULL__L5*/ meltfnum[1] = 0;
    /*^clear */
	   /*clear *//*_.IF___V14*/ meltfptr[12] = 0;
    /*^clear */
	   /*clear *//*_.LET___V16*/ meltfptr[14] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("MEXPAND_COMPILE_WARNING", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_134_warmelt_macro_MEXPAND_COMPILE_WARNING_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_134_warmelt_macro_MEXPAND_COMPILE_WARNING */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_135_warmelt_macro_MEXPAND_ASSERT_MSG (meltclosure_ptr_t meltclosp_,
					       melt_ptr_t meltfirstargp_,
					       const melt_argdescr_cell_t
					       meltxargdescr_[],
					       union meltparam_un *
					       meltxargtab_,
					       const melt_argdescr_cell_t
					       meltxresdescr_[],
					       union meltparam_un *
					       meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_135_warmelt_macro_MEXPAND_ASSERT_MSG_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_135_warmelt_macro_MEXPAND_ASSERT_MSG_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 77
    melt_ptr_t mcfr_varptr[77];
#define MELTFRAM_NBVARNUM 23
    long mcfr_varnum[23];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_135_warmelt_macro_MEXPAND_ASSERT_MSG is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_135_warmelt_macro_MEXPAND_ASSERT_MSG_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 77; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_135_warmelt_macro_MEXPAND_ASSERT_MSG nbval 77*/
  meltfram__.mcfr_nbvar = 77 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("MEXPAND_ASSERT_MSG", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-macro.melt:6218:/ getarg");
 /*_.SEXPR__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.ENV__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.ENV__V3*/ meltfptr[2])) != NULL);


  /*getarg#2 */
  /*^getarg */
  if (meltxargdescr_[1] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.MEXPANDER__V4*/ meltfptr[3] =
    (meltxargtab_[1].meltbp_aptr) ? (*(meltxargtab_[1].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.MEXPANDER__V4*/ meltfptr[3])) !=
	      NULL);


  /*getarg#3 */
  /*^getarg */
  if (meltxargdescr_[2] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.MODCTX__V5*/ meltfptr[4] =
    (meltxargtab_[2].meltbp_aptr) ? (*(meltxargtab_[2].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.MODCTX__V5*/ meltfptr[4])) !=
	      NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-macro.melt:6219:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L1*/ meltfnum[0] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-macro.melt:6219:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L1*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-macro.melt:6219:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-macro.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 6219;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "mexpand_assert_msg sexpr=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.SEXPR__V2*/ meltfptr[1];
	      /*_.MELT_DEBUG_FUN__V8*/ meltfptr[7] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V7*/ meltfptr[6] = /*_.MELT_DEBUG_FUN__V8*/ meltfptr[7];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-macro.melt:6219:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V8*/ meltfptr[7] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V7*/ meltfptr[6] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-macro.melt:6219:/ quasiblock");


      /*_.PROGN___V9*/ meltfptr[7] = /*_.IF___V7*/ meltfptr[6];;
      /*^compute */
      /*_.IFCPP___V6*/ meltfptr[5] = /*_.PROGN___V9*/ meltfptr[7];;
      /*epilog */

      MELT_LOCATION ("warmelt-macro.melt:6219:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L1*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IF___V7*/ meltfptr[6] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V9*/ meltfptr[7] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V6*/ meltfptr[5] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-macro.melt:6220:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L3*/ meltfnum[1] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.SEXPR__V2*/ meltfptr[1]),
			     (melt_ptr_t) (( /*!CLASS_SEXPR */ meltfrout->
					    tabval[1])));;
      MELT_LOCATION ("warmelt-macro.melt:6220:/ cond");
      /*cond */ if ( /*_#IS_A__L3*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V11*/ meltfptr[7] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-macro.melt:6220:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check sexpr"),
				  ("warmelt-macro.melt")
				  ? ("warmelt-macro.melt") : __FILE__,
				  (6220) ? (6220) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V11*/ meltfptr[7] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V10*/ meltfptr[6] = /*_.IFELSE___V11*/ meltfptr[7];;
      /*epilog */

      MELT_LOCATION ("warmelt-macro.melt:6220:/ clear");
	     /*clear *//*_#IS_A__L3*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V11*/ meltfptr[7] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V10*/ meltfptr[6] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-macro.melt:6221:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L4*/ meltfnum[0] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.ENV__V3*/ meltfptr[2]),
			     (melt_ptr_t) (( /*!CLASS_ENVIRONMENT */
					    meltfrout->tabval[2])));;
      MELT_LOCATION ("warmelt-macro.melt:6221:/ cond");
      /*cond */ if ( /*_#IS_A__L4*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V13*/ meltfptr[12] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-macro.melt:6221:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check env"),
				  ("warmelt-macro.melt")
				  ? ("warmelt-macro.melt") : __FILE__,
				  (6221) ? (6221) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V13*/ meltfptr[12] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V12*/ meltfptr[7] = /*_.IFELSE___V13*/ meltfptr[12];;
      /*epilog */

      MELT_LOCATION ("warmelt-macro.melt:6221:/ clear");
	     /*clear *//*_#IS_A__L4*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V13*/ meltfptr[12] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V12*/ meltfptr[7] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-macro.melt:6222:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#NULL__L5*/ meltfnum[1] =
      (( /*_.MEXPANDER__V4*/ meltfptr[3]) == NULL);;
    MELT_LOCATION ("warmelt-macro.melt:6222:/ cond");
    /*cond */ if ( /*_#NULL__L5*/ meltfnum[1])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  /*^compute */
	  /*_.MEXPANDER__V4*/ meltfptr[3] = /*_.SETQ___V15*/ meltfptr[14] =
	    ( /*!MACROEXPAND_1 */ meltfrout->tabval[3]);;
	  /*_.IF___V14*/ meltfptr[12] = /*_.SETQ___V15*/ meltfptr[14];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-macro.melt:6222:/ clear");
	     /*clear *//*_.SETQ___V15*/ meltfptr[14] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

  /*_.IF___V14*/ meltfptr[12] = NULL;;
      }
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-macro.melt:6223:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_OBJECT__L6*/ meltfnum[0] =
	(melt_magic_discr ((melt_ptr_t) ( /*_.MODCTX__V5*/ meltfptr[4])) ==
	 MELTOBMAG_OBJECT);;
      MELT_LOCATION ("warmelt-macro.melt:6223:/ cond");
      /*cond */ if ( /*_#IS_OBJECT__L6*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V17*/ meltfptr[16] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-macro.melt:6223:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check modctx"),
				  ("warmelt-macro.melt")
				  ? ("warmelt-macro.melt") : __FILE__,
				  (6223) ? (6223) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V17*/ meltfptr[16] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V16*/ meltfptr[14] = /*_.IFELSE___V17*/ meltfptr[16];;
      /*epilog */

      MELT_LOCATION ("warmelt-macro.melt:6223:/ clear");
	     /*clear *//*_#IS_OBJECT__L6*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V17*/ meltfptr[16] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V16*/ meltfptr[14] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-macro.melt:6224:/ quasiblock");


    MELT_LOCATION ("warmelt-macro.melt:6225:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.SEXPR__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 2, "SEXP_CONTENTS");
  /*_.CONT__V19*/ meltfptr[18] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-macro.melt:6226:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.SEXPR__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 1, "LOCA_LOCATION");
  /*_.LOC__V20*/ meltfptr[19] = slot;
    };
    ;
 /*_.LIST_FIRST__V21*/ meltfptr[20] =
      (melt_list_first ((melt_ptr_t) ( /*_.CONT__V19*/ meltfptr[18])));;
    /*^compute */
 /*_.CURPAIR__V22*/ meltfptr[21] =
      (melt_pair_tail ((melt_ptr_t) ( /*_.LIST_FIRST__V21*/ meltfptr[20])));;
    /*^compute */
 /*_.PAIR_HEAD__V23*/ meltfptr[22] =
      (melt_pair_head ((melt_ptr_t) ( /*_.CURPAIR__V22*/ meltfptr[21])));;
    MELT_LOCATION ("warmelt-macro.melt:6228:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[3];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.ENV__V3*/ meltfptr[2];
      /*^apply.arg */
      argtab[1].meltbp_aptr =
	(melt_ptr_t *) & /*_.MEXPANDER__V4*/ meltfptr[3];
      /*^apply.arg */
      argtab[2].meltbp_aptr = (melt_ptr_t *) & /*_.MODCTX__V5*/ meltfptr[4];
      /*_.XMSG__V24*/ meltfptr[23] =
	melt_apply ((meltclosure_ptr_t) ( /*_.MEXPANDER__V4*/ meltfptr[3]),
		    (melt_ptr_t) ( /*_.PAIR_HEAD__V23*/ meltfptr[22]),
		    (MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_PTR ""),
		    argtab, "", (union meltparam_un *) 0);
    }
    ;
    /*_.XTEST__V25*/ meltfptr[24] = ( /*nil */ NULL);;
    /*^compute */
    /*_.ASSFAIL_SYMB__V26*/ meltfptr[25] =
      ( /*!konst_4_ASSERT_FAILED */ meltfrout->tabval[4]);;
    MELT_LOCATION ("warmelt-macro.melt:6231:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & /*_.ASSFAIL_SYMB__V26*/ meltfptr[25];
      /*_.ASSFAIL_BINDING__V27*/ meltfptr[26] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!FIND_ENV */ meltfrout->tabval[5])),
		    (melt_ptr_t) ( /*_.ENV__V3*/ meltfptr[2]),
		    (MELTBPARSTR_PTR ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-macro.melt:6233:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#IS_STRING__L7*/ meltfnum[0] =
      (melt_magic_discr ((melt_ptr_t) ( /*_.XMSG__V24*/ meltfptr[23])) ==
       MELTOBMAG_STRING);;
    /*^compute */
 /*_#NOT__L8*/ meltfnum[7] =
      (!( /*_#IS_STRING__L7*/ meltfnum[0]));;
    MELT_LOCATION ("warmelt-macro.melt:6233:/ cond");
    /*cond */ if ( /*_#NOT__L8*/ meltfnum[7])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{


	  {
	    MELT_LOCATION ("warmelt-macro.melt:6234:/ locexp");
	    /* error_plain */
	      melt_error_str ((melt_ptr_t) ( /*_.LOC__V20*/ meltfptr[19]),
			      ("non string message in (ASSERT_MSG <msg> <test>)"),
			      (melt_ptr_t) 0);
	  }
	  ;
	  /*epilog */
	}
	;
      }				/*noelse */
    ;
 /*_.PAIR_TAIL__V28*/ meltfptr[27] =
      (melt_pair_tail ((melt_ptr_t) ( /*_.CURPAIR__V22*/ meltfptr[21])));;
    MELT_LOCATION ("warmelt-macro.melt:6235:/ compute");
    /*_.CURPAIR__V22*/ meltfptr[21] = /*_.SETQ___V29*/ meltfptr[28] =
      /*_.PAIR_TAIL__V28*/ meltfptr[27];;
    MELT_LOCATION ("warmelt-macro.melt:6236:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#IS_PAIR__L9*/ meltfnum[8] =
      (melt_magic_discr ((melt_ptr_t) ( /*_.CURPAIR__V22*/ meltfptr[21])) ==
       MELTOBMAG_PAIR);;
    MELT_LOCATION ("warmelt-macro.melt:6236:/ cond");
    /*cond */ if ( /*_#IS_PAIR__L9*/ meltfnum[8])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-macro.melt:6237:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
   /*_.PAIR_TAIL__V31*/ meltfptr[30] =
	    (melt_pair_tail
	     ((melt_ptr_t) ( /*_.CURPAIR__V22*/ meltfptr[21])));;
	  MELT_LOCATION ("warmelt-macro.melt:6237:/ cond");
	  /*cond */ if ( /*_.PAIR_TAIL__V31*/ meltfptr[30])	/*then */
	    {
	      /*^cond.then */
	      /*^block */
	      /*anyblock */
	      {


		{
		  MELT_LOCATION ("warmelt-macro.melt:6238:/ locexp");
		  /* error_plain */
		    melt_error_str ((melt_ptr_t)
				    ( /*_.LOC__V20*/ meltfptr[19]),
				    ("extra arg for (ASSERT_MSG <msg> <test>)"),
				    (melt_ptr_t) 0);
		}
		;
		/*epilog */
	      }
	      ;
	    }			/*noelse */
	  ;
   /*_.PAIR_HEAD__V32*/ meltfptr[31] =
	    (melt_pair_head
	     ((melt_ptr_t) ( /*_.CURPAIR__V22*/ meltfptr[21])));;
	  MELT_LOCATION ("warmelt-macro.melt:6239:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^apply */
	  /*apply */
	  {
	    union meltparam_un argtab[3];
	    memset (&argtab, 0, sizeof (argtab));
	    /*^apply.arg */
	    argtab[0].meltbp_aptr =
	      (melt_ptr_t *) & /*_.ENV__V3*/ meltfptr[2];
	    /*^apply.arg */
	    argtab[1].meltbp_aptr =
	      (melt_ptr_t *) & /*_.MEXPANDER__V4*/ meltfptr[3];
	    /*^apply.arg */
	    argtab[2].meltbp_aptr =
	      (melt_ptr_t *) & /*_.MODCTX__V5*/ meltfptr[4];
	    /*_.MEXPANDER__V33*/ meltfptr[32] =
	      melt_apply ((meltclosure_ptr_t)
			  ( /*_.MEXPANDER__V4*/ meltfptr[3]),
			  (melt_ptr_t) ( /*_.PAIR_HEAD__V32*/ meltfptr[31]),
			  (MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_PTR
			   ""), argtab, "", (union meltparam_un *) 0);
	  }
	  ;
	  /*^compute */
	  /*_.XTEST__V25*/ meltfptr[24] = /*_.SETQ___V34*/ meltfptr[33] =
	    /*_.MEXPANDER__V33*/ meltfptr[32];;
	  MELT_LOCATION ("warmelt-macro.melt:6236:/ quasiblock");


	  /*_.PROGN___V35*/ meltfptr[34] = /*_.SETQ___V34*/ meltfptr[33];;
	  /*^compute */
	  /*_.IFELSE___V30*/ meltfptr[29] = /*_.PROGN___V35*/ meltfptr[34];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-macro.melt:6236:/ clear");
	     /*clear *//*_.PAIR_TAIL__V31*/ meltfptr[30] = 0;
	  /*^clear */
	     /*clear *//*_.PAIR_HEAD__V32*/ meltfptr[31] = 0;
	  /*^clear */
	     /*clear *//*_.MEXPANDER__V33*/ meltfptr[32] = 0;
	  /*^clear */
	     /*clear *//*_.SETQ___V34*/ meltfptr[33] = 0;
	  /*^clear */
	     /*clear *//*_.PROGN___V35*/ meltfptr[34] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-macro.melt:6241:/ compute");
	  /*_.XTEST__V25*/ meltfptr[24] = /*_.SETQ___V36*/ meltfptr[30] =
	    ( /*nil */ NULL);;

	  {
	    MELT_LOCATION ("warmelt-macro.melt:6242:/ locexp");
	    melt_warning_str (0, (melt_ptr_t) ( /*_.LOC__V20*/ meltfptr[19]),
			      ("empty ASSERT_MSG"),
			      (melt_ptr_t) ( /*_.XMSG__V24*/ meltfptr[23]));
	  }
	  ;
	  MELT_LOCATION ("warmelt-macro.melt:6240:/ quasiblock");


	  /*epilog */

	  MELT_LOCATION ("warmelt-macro.melt:6236:/ clear");
	     /*clear *//*_.SETQ___V36*/ meltfptr[30] = 0;
	}
	;
      }
    ;
    MELT_LOCATION ("warmelt-macro.melt:6244:/ quasiblock");


    MELT_LOCATION ("warmelt-macro.melt:6247:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#NULL__L10*/ meltfnum[9] =
      (( /*_.ASSFAIL_BINDING__V27*/ meltfptr[26]) == NULL);;
    MELT_LOCATION ("warmelt-macro.melt:6247:/ cond");
    /*cond */ if ( /*_#NULL__L10*/ meltfnum[9])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{


	  {
	    MELT_LOCATION ("warmelt-macro.melt:6248:/ locexp");
	    /* error_plain */
	      melt_error_str ((melt_ptr_t) ( /*_.LOC__V20*/ meltfptr[19]),
			      ("unbound ASSERT_FAILED in (ASSERT_MSG <msg> <test>)"),
			      (melt_ptr_t) 0);
	  }
	  ;

#if MELT_HAVE_DEBUG
	  MELT_LOCATION ("warmelt-macro.melt:6249:/ cppif.then");
	  /*^block */
	  /*anyblock */
	  {


	    {
	      /*^locexp */
	      /*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	      melt_dbgcounter++;
#endif
	      ;
	    }
	    ;
	    /*^checksignal */
	    MELT_CHECK_SIGNAL ();
	    ;
     /*_#MELT_NEED_DBG__L11*/ meltfnum[10] =
	      /*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	      ( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	      0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	      ;;
	    MELT_LOCATION ("warmelt-macro.melt:6249:/ cond");
	    /*cond */ if ( /*_#MELT_NEED_DBG__L11*/ meltfnum[10])	/*then */
	      {
		/*^cond.then */
		/*^block */
		/*anyblock */
		{

       /*_#THE_MELTCALLCOUNT__L12*/ meltfnum[11] =
#ifdef meltcallcount
		    meltcallcount	/* the_meltcallcount */
#else
		    0L
#endif /* meltcallcount the_meltcallcount */
		    ;;
		  /*^compute */
       /*_.DISCRIM__V41*/ meltfptr[30] =
		    ((melt_ptr_t)
		     (melt_discr
		      ((melt_ptr_t) ( /*_.ENV__V3*/ meltfptr[2]))));;
		  MELT_LOCATION ("warmelt-macro.melt:6251:/ checksignal");
		  MELT_CHECK_SIGNAL ();
		  ;
		  /*^apply */
		  /*apply */
		  {
		    union meltparam_un argtab[1];
		    memset (&argtab, 0, sizeof (argtab));
		    /*^apply.arg */
		    argtab[0].meltbp_aptr =
		      (melt_ptr_t *) & /*_.ASSFAIL_SYMB__V26*/ meltfptr[25];
		    /*_.FIND_ENV_DEBUG__V42*/ meltfptr[41] =
		      melt_apply ((meltclosure_ptr_t)
				  (( /*!FIND_ENV_DEBUG */ meltfrout->
				    tabval[6])),
				  (melt_ptr_t) ( /*_.ENV__V3*/ meltfptr[2]),
				  (MELTBPARSTR_PTR ""), argtab, "",
				  (union meltparam_un *) 0);
		  }
		  ;
		  MELT_LOCATION ("warmelt-macro.melt:6249:/ checksignal");
		  MELT_CHECK_SIGNAL ();
		  ;
		  /*^apply */
		  /*apply */
		  {
		    union meltparam_un argtab[11];
		    memset (&argtab, 0, sizeof (argtab));
		    /*^apply.arg */
		    argtab[0].meltbp_long =
		      /*_#THE_MELTCALLCOUNT__L12*/ meltfnum[11];
		    /*^apply.arg */
		    argtab[1].meltbp_cstring = "warmelt-macro.melt";
		    /*^apply.arg */
		    argtab[2].meltbp_long = 6249;
		    /*^apply.arg */
		    argtab[3].meltbp_cstring =
		      "mexpand_assert_msg  without assert_failed env=";
		    /*^apply.arg */
		    argtab[4].meltbp_aptr =
		      (melt_ptr_t *) & /*_.ENV__V3*/ meltfptr[2];
		    /*^apply.arg */
		    argtab[5].meltbp_cstring = " env\'s class";
		    /*^apply.arg */
		    argtab[6].meltbp_aptr =
		      (melt_ptr_t *) & /*_.DISCRIM__V41*/ meltfptr[30];
		    /*^apply.arg */
		    argtab[7].meltbp_cstring = "find_env giving:";
		    /*^apply.arg */
		    argtab[8].meltbp_aptr =
		      (melt_ptr_t *) & /*_.FIND_ENV_DEBUG__V42*/ meltfptr[41];
		    /*^apply.arg */
		    argtab[9].meltbp_cstring = "assfail_symb=";
		    /*^apply.arg */
		    argtab[10].meltbp_aptr =
		      (melt_ptr_t *) & /*_.ASSFAIL_SYMB__V26*/ meltfptr[25];
		    /*_.MELT_DEBUG_FUN__V43*/ meltfptr[42] =
		      melt_apply ((meltclosure_ptr_t)
				  (( /*!MELT_DEBUG_FUN */ meltfrout->
				    tabval[0])),
				  (melt_ptr_t) (( /*nil */ NULL)),
				  (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_PTR MELTBPARSTR_CSTRING
				   MELTBPARSTR_PTR MELTBPARSTR_CSTRING
				   MELTBPARSTR_PTR MELTBPARSTR_CSTRING
				   MELTBPARSTR_PTR ""), argtab, "",
				  (union meltparam_un *) 0);
		  }
		  ;
		  /*_.IF___V40*/ meltfptr[34] =
		    /*_.MELT_DEBUG_FUN__V43*/ meltfptr[42];;
		  /*epilog */

		  MELT_LOCATION ("warmelt-macro.melt:6249:/ clear");
		 /*clear *//*_#THE_MELTCALLCOUNT__L12*/ meltfnum[11] = 0;
		  /*^clear */
		 /*clear *//*_.DISCRIM__V41*/ meltfptr[30] = 0;
		  /*^clear */
		 /*clear *//*_.FIND_ENV_DEBUG__V42*/ meltfptr[41] = 0;
		  /*^clear */
		 /*clear *//*_.MELT_DEBUG_FUN__V43*/ meltfptr[42] = 0;
		}
		;
	      }
	    else
	      {			/*^cond.else */

      /*_.IF___V40*/ meltfptr[34] = NULL;;
	      }
	    ;
	    MELT_LOCATION ("warmelt-macro.melt:6249:/ quasiblock");


	    /*_.PROGN___V44*/ meltfptr[30] = /*_.IF___V40*/ meltfptr[34];;
	    /*^compute */
	    /*_.IFCPP___V39*/ meltfptr[33] = /*_.PROGN___V44*/ meltfptr[30];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-macro.melt:6249:/ clear");
	       /*clear *//*_#MELT_NEED_DBG__L11*/ meltfnum[10] = 0;
	    /*^clear */
	       /*clear *//*_.IF___V40*/ meltfptr[34] = 0;
	    /*^clear */
	       /*clear *//*_.PROGN___V44*/ meltfptr[30] = 0;
	  }

#else /*MELT_HAVE_DEBUG */
	  /*^cppif.else */
	  /*_.IFCPP___V39*/ meltfptr[33] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
	  ;

#if MELT_HAVE_DEBUG
	  MELT_LOCATION ("warmelt-macro.melt:6253:/ cppif.then");
	  /*^block */
	  /*anyblock */
	  {

	    /*^checksignal */
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^cond */
	    /*cond */ if (( /*nil */ NULL))	/*then */
	      {
		/*^cond.then */
		/*_.IFELSE___V46*/ meltfptr[42] = ( /*nil */ NULL);;
	      }
	    else
	      {
		MELT_LOCATION ("warmelt-macro.melt:6253:/ cond.else");

		/*^block */
		/*anyblock */
		{




		  {
		    /*^locexp */
		    melt_assert_failed (("assfail_binding nul! @@"),
					("warmelt-macro.melt")
					? ("warmelt-macro.melt") : __FILE__,
					(6253) ? (6253) : __LINE__,
					__FUNCTION__);
		    ;
		  }
		  ;
		 /*clear *//*_.IFELSE___V46*/ meltfptr[42] = 0;
		  /*epilog */
		}
		;
	      }
	    ;
	    /*^compute */
	    /*_.IFCPP___V45*/ meltfptr[41] = /*_.IFELSE___V46*/ meltfptr[42];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-macro.melt:6253:/ clear");
	       /*clear *//*_.IFELSE___V46*/ meltfptr[42] = 0;
	  }

#else /*MELT_HAVE_DEBUG */
	  /*^cppif.else */
	  /*_.IFCPP___V45*/ meltfptr[41] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
	  ;
	  MELT_LOCATION ("warmelt-macro.melt:6254:/ quasiblock");


   /*_.RETVAL___V1*/ meltfptr[0] = NULL;;

	  {
	    MELT_LOCATION ("warmelt-macro.melt:6254:/ locexp");
	    /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
	    if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	      melt_warn_for_no_expected_secondary_results ();
	    /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
	    ;
	  }
	  ;
	  /*^finalreturn */
	  ;
	  /*finalret */ goto labend_rout;
	  MELT_LOCATION ("warmelt-macro.melt:6247:/ quasiblock");


	  /*_.PROGN___V48*/ meltfptr[30] = /*_.RETURN___V47*/ meltfptr[34];;
	  /*^compute */
	  /*_.AFPRIM__V38*/ meltfptr[32] = /*_.PROGN___V48*/ meltfptr[30];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-macro.melt:6247:/ clear");
	     /*clear *//*_.IFCPP___V39*/ meltfptr[33] = 0;
	  /*^clear */
	     /*clear *//*_.IFCPP___V45*/ meltfptr[41] = 0;
	  /*^clear */
	     /*clear *//*_.RETURN___V47*/ meltfptr[34] = 0;
	  /*^clear */
	     /*clear *//*_.PROGN___V48*/ meltfptr[30] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-macro.melt:6255:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
   /*_#IS_A__L13*/ meltfnum[11] =
	    melt_is_instance_of ((melt_ptr_t)
				 ( /*_.ASSFAIL_BINDING__V27*/ meltfptr[26]),
				 (melt_ptr_t) (( /*!CLASS_PRIMITIVE_BINDING */
						meltfrout->tabval[7])));;
	  MELT_LOCATION ("warmelt-macro.melt:6255:/ cond");
	  /*cond */ if ( /*_#IS_A__L13*/ meltfnum[11])	/*then */
	    {
	      /*^cond.then */
	      /*^block */
	      /*anyblock */
	      {

		MELT_LOCATION ("warmelt-macro.melt:6256:/ getslot");
		{
		  melt_ptr_t slot = NULL, obj = NULL;
		  obj =
		    (melt_ptr_t) ( /*_.ASSFAIL_BINDING__V27*/ meltfptr[26])
		    /*=obj*/ ;
		  melt_object_get_field (slot, obj, 3, "PBIND_PRIMITIVE");
      /*_.PBIND_PRIMITIVE__V50*/ meltfptr[33] = slot;
		};
		;
		/*_.IFELSE___V49*/ meltfptr[42] =
		  /*_.PBIND_PRIMITIVE__V50*/ meltfptr[33];;
		/*epilog */

		MELT_LOCATION ("warmelt-macro.melt:6255:/ clear");
	       /*clear *//*_.PBIND_PRIMITIVE__V50*/ meltfptr[33] = 0;
	      }
	      ;
	    }
	  else
	    {			/*^cond.else */

	      /*^block */
	      /*anyblock */
	      {

		MELT_LOCATION ("warmelt-macro.melt:6257:/ checksignal");
		MELT_CHECK_SIGNAL ();
		;
		/*^checksignal */
		MELT_CHECK_SIGNAL ();
		;
     /*_#IS_A__L14*/ meltfnum[10] =
		  melt_is_instance_of ((melt_ptr_t)
				       ( /*_.ASSFAIL_BINDING__V27*/
					meltfptr[26]),
				       (melt_ptr_t) (( /*!CLASS_VALUE_BINDING */ meltfrout->tabval[8])));;
		MELT_LOCATION ("warmelt-macro.melt:6257:/ cond");
		/*cond */ if ( /*_#IS_A__L14*/ meltfnum[10])	/*then */
		  {
		    /*^cond.then */
		    /*^block */
		    /*anyblock */
		    {

		      MELT_LOCATION ("warmelt-macro.melt:6258:/ getslot");
		      {
			melt_ptr_t slot = NULL, obj = NULL;
			obj =
			  (melt_ptr_t) ( /*_.ASSFAIL_BINDING__V27*/
					meltfptr[26]) /*=obj*/ ;
			melt_object_get_field (slot, obj, 1, "VBIND_VALUE");
	/*_.VBIND_VALUE__V51*/ meltfptr[41] = slot;
		      };
		      ;
       /*_#IS_A__L16*/ meltfnum[15] =
			melt_is_instance_of ((melt_ptr_t)
					     ( /*_.VBIND_VALUE__V51*/
					      meltfptr[41]),
					     (melt_ptr_t) (( /*!CLASS_PRIMITIVE */ meltfrout->tabval[9])));;
		      /*^compute */
		      /*_#IF___L15*/ meltfnum[14] =
			/*_#IS_A__L16*/ meltfnum[15];;
		      /*epilog */

		      MELT_LOCATION ("warmelt-macro.melt:6257:/ clear");
		 /*clear *//*_.VBIND_VALUE__V51*/ meltfptr[41] = 0;
		      /*^clear */
		 /*clear *//*_#IS_A__L16*/ meltfnum[15] = 0;
		    }
		    ;
		  }
		else
		  {		/*^cond.else */

      /*_#IF___L15*/ meltfnum[14] = 0;;
		  }
		;
		MELT_LOCATION ("warmelt-macro.melt:6257:/ cond");
		/*cond */ if ( /*_#IF___L15*/ meltfnum[14])	/*then */
		  {
		    /*^cond.then */
		    /*^block */
		    /*anyblock */
		    {

		      MELT_LOCATION ("warmelt-macro.melt:6260:/ getslot");
		      {
			melt_ptr_t slot = NULL, obj = NULL;
			obj =
			  (melt_ptr_t) ( /*_.ASSFAIL_BINDING__V27*/
					meltfptr[26]) /*=obj*/ ;
			melt_object_get_field (slot, obj, 1, "VBIND_VALUE");
	/*_.VBIND_VALUE__V53*/ meltfptr[30] = slot;
		      };
		      ;
		      /*_.IFELSE___V52*/ meltfptr[34] =
			/*_.VBIND_VALUE__V53*/ meltfptr[30];;
		      /*epilog */

		      MELT_LOCATION ("warmelt-macro.melt:6257:/ clear");
		 /*clear *//*_.VBIND_VALUE__V53*/ meltfptr[30] = 0;
		    }
		    ;
		  }
		else
		  {		/*^cond.else */

		    /*^block */
		    /*anyblock */
		    {


#if MELT_HAVE_DEBUG
		      MELT_LOCATION ("warmelt-macro.melt:6262:/ cppif.then");
		      /*^block */
		      /*anyblock */
		      {


			{
			  /*^locexp */
			  /*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
			  melt_dbgcounter++;
#endif
			  ;
			}
			;
			/*^checksignal */
			MELT_CHECK_SIGNAL ();
			;
	 /*_#MELT_NEED_DBG__L17*/ meltfnum[15] =
			  /*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
			  ( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
			  0	/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
			  ;;
			MELT_LOCATION ("warmelt-macro.melt:6262:/ cond");
			/*cond */ if ( /*_#MELT_NEED_DBG__L17*/ meltfnum[15])	/*then */
			  {
			    /*^cond.then */
			    /*^block */
			    /*anyblock */
			    {

	   /*_#THE_MELTCALLCOUNT__L18*/ meltfnum[17] =
#ifdef meltcallcount
				meltcallcount	/* the_meltcallcount */
#else
				0L
#endif /* meltcallcount the_meltcallcount */
				;;
			      MELT_LOCATION
				("warmelt-macro.melt:6262:/ checksignal");
			      MELT_CHECK_SIGNAL ();
			      ;
			      /*^apply */
			      /*apply */
			      {
				union meltparam_un argtab[5];
				memset (&argtab, 0, sizeof (argtab));
				/*^apply.arg */
				argtab[0].meltbp_long =
				  /*_#THE_MELTCALLCOUNT__L18*/ meltfnum[17];
				/*^apply.arg */
				argtab[1].meltbp_cstring =
				  "warmelt-macro.melt";
				/*^apply.arg */
				argtab[2].meltbp_long = 6262;
				/*^apply.arg */
				argtab[3].meltbp_cstring =
				  "mexpand_assert_msg bad assfail_binding=";
				/*^apply.arg */
				argtab[4].meltbp_aptr =
				  (melt_ptr_t *) & /*_.ASSFAIL_BINDING__V27*/
				  meltfptr[26];
				/*_.MELT_DEBUG_FUN__V56*/ meltfptr[30] =
				  melt_apply ((meltclosure_ptr_t)
					      (( /*!MELT_DEBUG_FUN */
						meltfrout->tabval[0])),
					      (melt_ptr_t) (( /*nil */ NULL)),
					      (MELTBPARSTR_LONG
					       MELTBPARSTR_CSTRING
					       MELTBPARSTR_LONG
					       MELTBPARSTR_CSTRING
					       MELTBPARSTR_PTR ""), argtab,
					      "", (union meltparam_un *) 0);
			      }
			      ;
			      /*_.IF___V55*/ meltfptr[41] =
				/*_.MELT_DEBUG_FUN__V56*/ meltfptr[30];;
			      /*epilog */

			      MELT_LOCATION
				("warmelt-macro.melt:6262:/ clear");
		     /*clear *//*_#THE_MELTCALLCOUNT__L18*/
				meltfnum[17] = 0;
			      /*^clear */
		     /*clear *//*_.MELT_DEBUG_FUN__V56*/ meltfptr[30]
				= 0;
			    }
			    ;
			  }
			else
			  {	/*^cond.else */

	  /*_.IF___V55*/ meltfptr[41] = NULL;;
			  }
			;
			MELT_LOCATION
			  ("warmelt-macro.melt:6262:/ quasiblock");


			/*_.PROGN___V57*/ meltfptr[30] =
			  /*_.IF___V55*/ meltfptr[41];;
			/*^compute */
			/*_.IFCPP___V54*/ meltfptr[33] =
			  /*_.PROGN___V57*/ meltfptr[30];;
			/*epilog */

			MELT_LOCATION ("warmelt-macro.melt:6262:/ clear");
		   /*clear *//*_#MELT_NEED_DBG__L17*/ meltfnum[15] = 0;
			/*^clear */
		   /*clear *//*_.IF___V55*/ meltfptr[41] = 0;
			/*^clear */
		   /*clear *//*_.PROGN___V57*/ meltfptr[30] = 0;
		      }

#else /*MELT_HAVE_DEBUG */
		      /*^cppif.else */
		      /*_.IFCPP___V54*/ meltfptr[33] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
		      ;

		      {
			MELT_LOCATION ("warmelt-macro.melt:6263:/ locexp");
			/* error_plain */
			  melt_error_str ((melt_ptr_t)
					  ( /*_.LOC__V20*/ meltfptr[19]),
					  ("ASSERT_FAILED not bound to a primitive in  (ASSERT_MSG <msg> <test>)"),
					  (melt_ptr_t) 0);
		      }
		      ;
		      MELT_LOCATION ("warmelt-macro.melt:6264:/ quasiblock");


       /*_.RETVAL___V1*/ meltfptr[0] = NULL;;

		      {
			MELT_LOCATION ("warmelt-macro.melt:6264:/ locexp");
			/*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
			if (meltxresdescr_ && meltxresdescr_[0]
			    && meltxrestab_)
			  melt_warn_for_no_expected_secondary_results ();
			/* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
			;
		      }
		      ;
		      /*^finalreturn */
		      ;
		      /*finalret */ goto labend_rout;
		      MELT_LOCATION ("warmelt-macro.melt:6261:/ quasiblock");


		      /*_.PROGN___V59*/ meltfptr[30] =
			/*_.RETURN___V58*/ meltfptr[41];;
		      /*^compute */
		      /*_.IFELSE___V52*/ meltfptr[34] =
			/*_.PROGN___V59*/ meltfptr[30];;
		      /*epilog */

		      MELT_LOCATION ("warmelt-macro.melt:6257:/ clear");
		 /*clear *//*_.IFCPP___V54*/ meltfptr[33] = 0;
		      /*^clear */
		 /*clear *//*_.RETURN___V58*/ meltfptr[41] = 0;
		      /*^clear */
		 /*clear *//*_.PROGN___V59*/ meltfptr[30] = 0;
		    }
		    ;
		  }
		;
		/*_.IFELSE___V49*/ meltfptr[42] =
		  /*_.IFELSE___V52*/ meltfptr[34];;
		/*epilog */

		MELT_LOCATION ("warmelt-macro.melt:6255:/ clear");
	       /*clear *//*_#IS_A__L14*/ meltfnum[10] = 0;
		/*^clear */
	       /*clear *//*_#IF___L15*/ meltfnum[14] = 0;
		/*^clear */
	       /*clear *//*_.IFELSE___V52*/ meltfptr[34] = 0;
	      }
	      ;
	    }
	  ;
	  /*_.AFPRIM__V38*/ meltfptr[32] = /*_.IFELSE___V49*/ meltfptr[42];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-macro.melt:6247:/ clear");
	     /*clear *//*_#IS_A__L13*/ meltfnum[11] = 0;
	  /*^clear */
	     /*clear *//*_.IFELSE___V49*/ meltfptr[42] = 0;
	}
	;
      }
    ;
    MELT_LOCATION ("warmelt-macro.melt:6267:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#IS_MIXINT__L19*/ meltfnum[17] =
      (melt_magic_discr ((melt_ptr_t) ( /*_.LOC__V20*/ meltfptr[19])) ==
       MELTOBMAG_MIXINT);;
    MELT_LOCATION ("warmelt-macro.melt:6267:/ cond");
    /*cond */ if ( /*_#IS_MIXINT__L19*/ meltfnum[17])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

   /*_.MIXINT_VAL__V61*/ meltfptr[41] =
	    (melt_val_mixint ((melt_ptr_t) ( /*_.LOC__V20*/ meltfptr[19])));;
	  /*^compute */
	  /*_.FILNAM__V60*/ meltfptr[33] =
	    /*_.MIXINT_VAL__V61*/ meltfptr[41];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-macro.melt:6267:/ clear");
	     /*clear *//*_.MIXINT_VAL__V61*/ meltfptr[41] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-macro.melt:6268:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
   /*_#IS_MIXLOC__L20*/ meltfnum[15] =
	    (melt_magic_discr ((melt_ptr_t) ( /*_.LOC__V20*/ meltfptr[19])) ==
	     MELTOBMAG_MIXLOC);;
	  MELT_LOCATION ("warmelt-macro.melt:6268:/ cond");
	  /*cond */ if ( /*_#IS_MIXLOC__L20*/ meltfnum[15])	/*then */
	    {
	      /*^cond.then */
	      /*^block */
	      /*anyblock */
	      {

     /*_.MIXLOC_VAL__V63*/ meltfptr[34] =
		  (melt_val_mixloc
		   ((melt_ptr_t) ( /*_.LOC__V20*/ meltfptr[19])));;
		/*^compute */
		/*_.IFELSE___V62*/ meltfptr[30] =
		  /*_.MIXLOC_VAL__V63*/ meltfptr[34];;
		/*epilog */

		MELT_LOCATION ("warmelt-macro.melt:6268:/ clear");
	       /*clear *//*_.MIXLOC_VAL__V63*/ meltfptr[34] = 0;
	      }
	      ;
	    }
	  else
	    {			/*^cond.else */

    /*_.IFELSE___V62*/ meltfptr[30] = NULL;;
	    }
	  ;
	  /*^compute */
	  /*_.FILNAM__V60*/ meltfptr[33] = /*_.IFELSE___V62*/ meltfptr[30];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-macro.melt:6267:/ clear");
	     /*clear *//*_#IS_MIXLOC__L20*/ meltfnum[15] = 0;
	  /*^clear */
	     /*clear *//*_.IFELSE___V62*/ meltfptr[30] = 0;
	}
	;
      }
    ;
    MELT_LOCATION ("warmelt-macro.melt:6269:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#GET_INT__L21*/ meltfnum[10] =
      (melt_get_int ((melt_ptr_t) ( /*_.LOC__V20*/ meltfptr[19])));;
    /*^compute */
 /*_.MAKE_INTEGERBOX__V64*/ meltfptr[42] =
      (meltgc_new_int
       ((meltobject_ptr_t) (( /*!DISCR_INTEGER */ meltfrout->tabval[11])),
	( /*_#GET_INT__L21*/ meltfnum[10])));;
    MELT_LOCATION ("warmelt-macro.melt:6273:/ blockmultialloc");
    /*multiallocblock */
    {
      struct meltletrec_1_st
      {
	struct MELT_MULTIPLE_STRUCT (3) rtup_0__TUPLREC__x9;
	long meltletrec_1_endgap;
      } *meltletrec_1_ptr = 0;
      meltletrec_1_ptr =
	(struct meltletrec_1_st *)
	meltgc_allocate (sizeof (struct meltletrec_1_st), 0);
      /*^blockmultialloc.initfill */
      /*inimult rtup_0__TUPLREC__x9 */
 /*_.TUPLREC___V66*/ meltfptr[34] =
	(melt_ptr_t) & meltletrec_1_ptr->rtup_0__TUPLREC__x9;
      meltletrec_1_ptr->rtup_0__TUPLREC__x9.discr =
	(meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (DISCR_MULTIPLE))));
      meltletrec_1_ptr->rtup_0__TUPLREC__x9.nbval = 3;


      /*^putuple */
      /*putupl#12 */
      melt_assertmsg ("putupl [:6273] #12 checktup",
		      melt_magic_discr ((melt_ptr_t)
					( /*_.TUPLREC___V66*/ meltfptr[34]))
		      == MELTOBMAG_MULTIPLE);
      melt_assertmsg ("putupl [:6273] #12 checkoff",
		      (0 >= 0
		       && 0 <
		       melt_multiple_length ((melt_ptr_t)
					     ( /*_.TUPLREC___V66*/
					      meltfptr[34]))));
      ((meltmultiple_ptr_t) ( /*_.TUPLREC___V66*/ meltfptr[34]))->tabval[0] =
	(melt_ptr_t) ( /*_.XMSG__V24*/ meltfptr[23]);
      ;
      /*^putuple */
      /*putupl#13 */
      melt_assertmsg ("putupl [:6273] #13 checktup",
		      melt_magic_discr ((melt_ptr_t)
					( /*_.TUPLREC___V66*/ meltfptr[34]))
		      == MELTOBMAG_MULTIPLE);
      melt_assertmsg ("putupl [:6273] #13 checkoff",
		      (1 >= 0
		       && 1 <
		       melt_multiple_length ((melt_ptr_t)
					     ( /*_.TUPLREC___V66*/
					      meltfptr[34]))));
      ((meltmultiple_ptr_t) ( /*_.TUPLREC___V66*/ meltfptr[34]))->tabval[1] =
	(melt_ptr_t) ( /*_.FILNAM__V60*/ meltfptr[33]);
      ;
      /*^putuple */
      /*putupl#14 */
      melt_assertmsg ("putupl [:6273] #14 checktup",
		      melt_magic_discr ((melt_ptr_t)
					( /*_.TUPLREC___V66*/ meltfptr[34]))
		      == MELTOBMAG_MULTIPLE);
      melt_assertmsg ("putupl [:6273] #14 checkoff",
		      (2 >= 0
		       && 2 <
		       melt_multiple_length ((melt_ptr_t)
					     ( /*_.TUPLREC___V66*/
					      meltfptr[34]))));
      ((meltmultiple_ptr_t) ( /*_.TUPLREC___V66*/ meltfptr[34]))->tabval[2] =
	(melt_ptr_t) ( /*_.MAKE_INTEGERBOX__V64*/ meltfptr[42]);
      ;
      /*^touch */
      meltgc_touch ( /*_.TUPLREC___V66*/ meltfptr[34]);
      ;
      /*_.TUPLE___V65*/ meltfptr[41] = /*_.TUPLREC___V66*/ meltfptr[34];;
      /*epilog */

      MELT_LOCATION ("warmelt-macro.melt:6273:/ clear");
	    /*clear *//*_.TUPLREC___V66*/ meltfptr[34] = 0;
      /*^clear */
	    /*clear *//*_.TUPLREC___V66*/ meltfptr[34] = 0;
    }				/*end multiallocblock */
    ;
    MELT_LOCATION ("warmelt-macro.melt:6269:/ quasiblock");


    /*^rawallocobj */
    /*rawallocobj */
    {
      melt_ptr_t newobj = 0;
      melt_raw_object_create (newobj,
			      (melt_ptr_t) (( /*!CLASS_SOURCE_PRIMITIVE */
					     meltfrout->tabval[10])), (4),
			      "CLASS_SOURCE_PRIMITIVE");
  /*_.INST__V68*/ meltfptr[34] =
	newobj;
    };
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @LOCA_LOCATION",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V68*/ meltfptr[34])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V68*/ meltfptr[34]), (1),
			  ( /*_.LOC__V20*/ meltfptr[19]), "LOCA_LOCATION");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @SPRIM_OPER",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V68*/ meltfptr[34])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V68*/ meltfptr[34]), (3),
			  ( /*_.AFPRIM__V38*/ meltfptr[32]), "SPRIM_OPER");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @SARGOP_ARGS",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V68*/ meltfptr[34])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V68*/ meltfptr[34]), (2),
			  ( /*_.TUPLE___V65*/ meltfptr[41]), "SARGOP_ARGS");
    ;
    /*^touchobj */

    melt_dbgtrace_written_object ( /*_.INST__V68*/ meltfptr[34],
				  "newly made instance");
    ;
    /*_.APRIM__V67*/ meltfptr[30] = /*_.INST__V68*/ meltfptr[34];;
    MELT_LOCATION ("warmelt-macro.melt:6277:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*^rawallocobj */
    /*rawallocobj */
    {
      melt_ptr_t newobj = 0;
      melt_raw_object_create (newobj,
			      (melt_ptr_t) (( /*!CLASS_SOURCE_IFELSE */
					     meltfrout->tabval[12])), (5),
			      "CLASS_SOURCE_IFELSE");
  /*_.INST__V70*/ meltfptr[69] =
	newobj;
    };
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @LOCA_LOCATION",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V70*/ meltfptr[69])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V70*/ meltfptr[69]), (1),
			  ( /*_.LOC__V20*/ meltfptr[19]), "LOCA_LOCATION");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @SIF_TEST",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V70*/ meltfptr[69])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V70*/ meltfptr[69]), (2),
			  ( /*_.XTEST__V25*/ meltfptr[24]), "SIF_TEST");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @SIF_THEN",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V70*/ meltfptr[69])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V70*/ meltfptr[69]), (3),
			  (( /*nil */ NULL)), "SIF_THEN");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @SIF_ELSE",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V70*/ meltfptr[69])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V70*/ meltfptr[69]), (4),
			  ( /*_.APRIM__V67*/ meltfptr[30]), "SIF_ELSE");
    ;
    /*^touchobj */

    melt_dbgtrace_written_object ( /*_.INST__V70*/ meltfptr[69],
				  "newly made instance");
    ;
    /*_.ATEST__V69*/ meltfptr[68] = /*_.INST__V70*/ meltfptr[69];;
    MELT_LOCATION ("warmelt-macro.melt:6283:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*^rawallocobj */
    /*rawallocobj */
    {
      melt_ptr_t newobj = 0;
      melt_raw_object_create (newobj,
			      (melt_ptr_t) (( /*!CLASS_SOURCE_CPPIF */
					     meltfrout->tabval[13])), (5),
			      "CLASS_SOURCE_CPPIF");
  /*_.INST__V72*/ meltfptr[71] =
	newobj;
    };
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @LOCA_LOCATION",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V72*/ meltfptr[71])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V72*/ meltfptr[71]), (1),
			  ( /*_.LOC__V20*/ meltfptr[19]), "LOCA_LOCATION");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @SIFP_COND",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V72*/ meltfptr[71])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V72*/ meltfptr[71]), (2),
			  (( /*!konst_14_MELT_HAVE_DEBUG */ meltfrout->
			    tabval[14])), "SIFP_COND");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @SIFP_THEN",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V72*/ meltfptr[71])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V72*/ meltfptr[71]), (3),
			  ( /*_.ATEST__V69*/ meltfptr[68]), "SIFP_THEN");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @SIFP_ELSE",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V72*/ meltfptr[71])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V72*/ meltfptr[71]), (4),
			  (( /*nil */ NULL)), "SIFP_ELSE");
    ;
    /*^touchobj */

    melt_dbgtrace_written_object ( /*_.INST__V72*/ meltfptr[71],
				  "newly made instance");
    ;
    /*_.ACPPIF__V71*/ meltfptr[70] = /*_.INST__V72*/ meltfptr[71];;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-macro.melt:6290:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L22*/ meltfnum[14] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-macro.melt:6290:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L22*/ meltfnum[14])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L23*/ meltfnum[11] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-macro.melt:6290:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long =
		/*_#THE_MELTCALLCOUNT__L23*/ meltfnum[11];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-macro.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 6290;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "mexpand_assert_msg result acppif";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.ACPPIF__V71*/ meltfptr[70];
	      /*_.MELT_DEBUG_FUN__V75*/ meltfptr[74] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V74*/ meltfptr[73] =
	      /*_.MELT_DEBUG_FUN__V75*/ meltfptr[74];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-macro.melt:6290:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L23*/ meltfnum[11] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V75*/ meltfptr[74] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V74*/ meltfptr[73] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-macro.melt:6290:/ quasiblock");


      /*_.PROGN___V76*/ meltfptr[74] = /*_.IF___V74*/ meltfptr[73];;
      /*^compute */
      /*_.IFCPP___V73*/ meltfptr[72] = /*_.PROGN___V76*/ meltfptr[74];;
      /*epilog */

      MELT_LOCATION ("warmelt-macro.melt:6290:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L22*/ meltfnum[14] = 0;
      /*^clear */
	     /*clear *//*_.IF___V74*/ meltfptr[73] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V76*/ meltfptr[74] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V73*/ meltfptr[72] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-macro.melt:6291:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.ACPPIF__V71*/ meltfptr[70];;

    {
      MELT_LOCATION ("warmelt-macro.melt:6291:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*_.LET___V37*/ meltfptr[31] = /*_.RETURN___V77*/ meltfptr[73];;

    MELT_LOCATION ("warmelt-macro.melt:6244:/ clear");
	   /*clear *//*_#NULL__L10*/ meltfnum[9] = 0;
    /*^clear */
	   /*clear *//*_.AFPRIM__V38*/ meltfptr[32] = 0;
    /*^clear */
	   /*clear *//*_#IS_MIXINT__L19*/ meltfnum[17] = 0;
    /*^clear */
	   /*clear *//*_.FILNAM__V60*/ meltfptr[33] = 0;
    /*^clear */
	   /*clear *//*_#GET_INT__L21*/ meltfnum[10] = 0;
    /*^clear */
	   /*clear *//*_.MAKE_INTEGERBOX__V64*/ meltfptr[42] = 0;
    /*^clear */
	   /*clear *//*_.TUPLE___V65*/ meltfptr[41] = 0;
    /*^clear */
	   /*clear *//*_.APRIM__V67*/ meltfptr[30] = 0;
    /*^clear */
	   /*clear *//*_.ATEST__V69*/ meltfptr[68] = 0;
    /*^clear */
	   /*clear *//*_.ACPPIF__V71*/ meltfptr[70] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V73*/ meltfptr[72] = 0;
    /*^clear */
	   /*clear *//*_.RETURN___V77*/ meltfptr[73] = 0;
    /*_.LET___V18*/ meltfptr[16] = /*_.LET___V37*/ meltfptr[31];;

    MELT_LOCATION ("warmelt-macro.melt:6224:/ clear");
	   /*clear *//*_.CONT__V19*/ meltfptr[18] = 0;
    /*^clear */
	   /*clear *//*_.LOC__V20*/ meltfptr[19] = 0;
    /*^clear */
	   /*clear *//*_.LIST_FIRST__V21*/ meltfptr[20] = 0;
    /*^clear */
	   /*clear *//*_.CURPAIR__V22*/ meltfptr[21] = 0;
    /*^clear */
	   /*clear *//*_.PAIR_HEAD__V23*/ meltfptr[22] = 0;
    /*^clear */
	   /*clear *//*_.XMSG__V24*/ meltfptr[23] = 0;
    /*^clear */
	   /*clear *//*_.XTEST__V25*/ meltfptr[24] = 0;
    /*^clear */
	   /*clear *//*_.ASSFAIL_SYMB__V26*/ meltfptr[25] = 0;
    /*^clear */
	   /*clear *//*_.ASSFAIL_BINDING__V27*/ meltfptr[26] = 0;
    /*^clear */
	   /*clear *//*_#IS_STRING__L7*/ meltfnum[0] = 0;
    /*^clear */
	   /*clear *//*_#NOT__L8*/ meltfnum[7] = 0;
    /*^clear */
	   /*clear *//*_.PAIR_TAIL__V28*/ meltfptr[27] = 0;
    /*^clear */
	   /*clear *//*_.SETQ___V29*/ meltfptr[28] = 0;
    /*^clear */
	   /*clear *//*_#IS_PAIR__L9*/ meltfnum[8] = 0;
    /*^clear */
	   /*clear *//*_.IFELSE___V30*/ meltfptr[29] = 0;
    /*^clear */
	   /*clear *//*_.LET___V37*/ meltfptr[31] = 0;
    MELT_LOCATION ("warmelt-macro.melt:6218:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.LET___V18*/ meltfptr[16];;

    {
      MELT_LOCATION ("warmelt-macro.melt:6218:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V6*/ meltfptr[5] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V10*/ meltfptr[6] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V12*/ meltfptr[7] = 0;
    /*^clear */
	   /*clear *//*_#NULL__L5*/ meltfnum[1] = 0;
    /*^clear */
	   /*clear *//*_.IF___V14*/ meltfptr[12] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V16*/ meltfptr[14] = 0;
    /*^clear */
	   /*clear *//*_.LET___V18*/ meltfptr[16] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("MEXPAND_ASSERT_MSG", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_135_warmelt_macro_MEXPAND_ASSERT_MSG_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_135_warmelt_macro_MEXPAND_ASSERT_MSG */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_136_warmelt_macro_MEXPAND_DEBUG_MSG (meltclosure_ptr_t meltclosp_,
					      melt_ptr_t meltfirstargp_,
					      const melt_argdescr_cell_t
					      meltxargdescr_[],
					      union meltparam_un *
					      meltxargtab_,
					      const melt_argdescr_cell_t
					      meltxresdescr_[],
					      union meltparam_un *
					      meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_136_warmelt_macro_MEXPAND_DEBUG_MSG_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_136_warmelt_macro_MEXPAND_DEBUG_MSG_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 64
    melt_ptr_t mcfr_varptr[64];
#define MELTFRAM_NBVARNUM 17
    long mcfr_varnum[17];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_136_warmelt_macro_MEXPAND_DEBUG_MSG is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_136_warmelt_macro_MEXPAND_DEBUG_MSG_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 64; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_136_warmelt_macro_MEXPAND_DEBUG_MSG nbval 64*/
  meltfram__.mcfr_nbvar = 64 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("MEXPAND_DEBUG_MSG", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-macro.melt:6307:/ getarg");
 /*_.SEXPR__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.ENV__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.ENV__V3*/ meltfptr[2])) != NULL);


  /*getarg#2 */
  /*^getarg */
  if (meltxargdescr_[1] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.MEXPANDER__V4*/ meltfptr[3] =
    (meltxargtab_[1].meltbp_aptr) ? (*(meltxargtab_[1].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.MEXPANDER__V4*/ meltfptr[3])) !=
	      NULL);


  /*getarg#3 */
  /*^getarg */
  if (meltxargdescr_[2] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.MODCTX__V5*/ meltfptr[4] =
    (meltxargtab_[2].meltbp_aptr) ? (*(meltxargtab_[2].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.MODCTX__V5*/ meltfptr[4])) !=
	      NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-macro.melt:6308:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L1*/ meltfnum[0] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-macro.melt:6308:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L1*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-macro.melt:6308:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-macro.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 6308;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "mexpand_debug_msg sexpr";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.SEXPR__V2*/ meltfptr[1];
	      /*_.MELT_DEBUG_FUN__V8*/ meltfptr[7] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V7*/ meltfptr[6] = /*_.MELT_DEBUG_FUN__V8*/ meltfptr[7];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-macro.melt:6308:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V8*/ meltfptr[7] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V7*/ meltfptr[6] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-macro.melt:6308:/ quasiblock");


      /*_.PROGN___V9*/ meltfptr[7] = /*_.IF___V7*/ meltfptr[6];;
      /*^compute */
      /*_.IFCPP___V6*/ meltfptr[5] = /*_.PROGN___V9*/ meltfptr[7];;
      /*epilog */

      MELT_LOCATION ("warmelt-macro.melt:6308:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L1*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IF___V7*/ meltfptr[6] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V9*/ meltfptr[7] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V6*/ meltfptr[5] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-macro.melt:6309:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L3*/ meltfnum[1] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.SEXPR__V2*/ meltfptr[1]),
			     (melt_ptr_t) (( /*!CLASS_SEXPR */ meltfrout->
					    tabval[1])));;
      MELT_LOCATION ("warmelt-macro.melt:6309:/ cond");
      /*cond */ if ( /*_#IS_A__L3*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V11*/ meltfptr[7] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-macro.melt:6309:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check sexpr"),
				  ("warmelt-macro.melt")
				  ? ("warmelt-macro.melt") : __FILE__,
				  (6309) ? (6309) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V11*/ meltfptr[7] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V10*/ meltfptr[6] = /*_.IFELSE___V11*/ meltfptr[7];;
      /*epilog */

      MELT_LOCATION ("warmelt-macro.melt:6309:/ clear");
	     /*clear *//*_#IS_A__L3*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V11*/ meltfptr[7] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V10*/ meltfptr[6] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-macro.melt:6310:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L4*/ meltfnum[0] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.ENV__V3*/ meltfptr[2]),
			     (melt_ptr_t) (( /*!CLASS_ENVIRONMENT */
					    meltfrout->tabval[2])));;
      MELT_LOCATION ("warmelt-macro.melt:6310:/ cond");
      /*cond */ if ( /*_#IS_A__L4*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V13*/ meltfptr[12] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-macro.melt:6310:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check env"),
				  ("warmelt-macro.melt")
				  ? ("warmelt-macro.melt") : __FILE__,
				  (6310) ? (6310) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V13*/ meltfptr[12] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V12*/ meltfptr[7] = /*_.IFELSE___V13*/ meltfptr[12];;
      /*epilog */

      MELT_LOCATION ("warmelt-macro.melt:6310:/ clear");
	     /*clear *//*_#IS_A__L4*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V13*/ meltfptr[12] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V12*/ meltfptr[7] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-macro.melt:6311:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_OBJECT__L5*/ meltfnum[1] =
	(melt_magic_discr ((melt_ptr_t) ( /*_.MODCTX__V5*/ meltfptr[4])) ==
	 MELTOBMAG_OBJECT);;
      MELT_LOCATION ("warmelt-macro.melt:6311:/ cond");
      /*cond */ if ( /*_#IS_OBJECT__L5*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V15*/ meltfptr[14] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-macro.melt:6311:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check modctx"),
				  ("warmelt-macro.melt")
				  ? ("warmelt-macro.melt") : __FILE__,
				  (6311) ? (6311) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V15*/ meltfptr[14] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V14*/ meltfptr[12] = /*_.IFELSE___V15*/ meltfptr[14];;
      /*epilog */

      MELT_LOCATION ("warmelt-macro.melt:6311:/ clear");
	     /*clear *//*_#IS_OBJECT__L5*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V15*/ meltfptr[14] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V14*/ meltfptr[12] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-macro.melt:6312:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_CLOSURE__L6*/ meltfnum[0] =
	(melt_magic_discr ((melt_ptr_t) ( /*_.MEXPANDER__V4*/ meltfptr[3])) ==
	 MELTOBMAG_CLOSURE);;
      MELT_LOCATION ("warmelt-macro.melt:6312:/ cond");
      /*cond */ if ( /*_#IS_CLOSURE__L6*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V17*/ meltfptr[16] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-macro.melt:6312:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check mexpander"),
				  ("warmelt-macro.melt")
				  ? ("warmelt-macro.melt") : __FILE__,
				  (6312) ? (6312) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V17*/ meltfptr[16] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V16*/ meltfptr[14] = /*_.IFELSE___V17*/ meltfptr[16];;
      /*epilog */

      MELT_LOCATION ("warmelt-macro.melt:6312:/ clear");
	     /*clear *//*_#IS_CLOSURE__L6*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V17*/ meltfptr[16] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V16*/ meltfptr[14] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-macro.melt:6313:/ quasiblock");


    MELT_LOCATION ("warmelt-macro.melt:6314:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.SEXPR__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 2, "SEXP_CONTENTS");
  /*_.CONT__V19*/ meltfptr[18] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-macro.melt:6315:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.SEXPR__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 1, "LOCA_LOCATION");
  /*_.LOC__V20*/ meltfptr[19] = slot;
    };
    ;
 /*_.LIST_FIRST__V21*/ meltfptr[20] =
      (melt_list_first ((melt_ptr_t) ( /*_.CONT__V19*/ meltfptr[18])));;
    /*^compute */
 /*_.CURPAIR__V22*/ meltfptr[21] =
      (melt_pair_tail ((melt_ptr_t) ( /*_.LIST_FIRST__V21*/ meltfptr[20])));;
    /*^compute */
 /*_.PAIR_HEAD__V23*/ meltfptr[22] =
      (melt_pair_head ((melt_ptr_t) ( /*_.CURPAIR__V22*/ meltfptr[21])));;
    MELT_LOCATION ("warmelt-macro.melt:6317:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[3];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.ENV__V3*/ meltfptr[2];
      /*^apply.arg */
      argtab[1].meltbp_aptr =
	(melt_ptr_t *) & /*_.MEXPANDER__V4*/ meltfptr[3];
      /*^apply.arg */
      argtab[2].meltbp_aptr = (melt_ptr_t *) & /*_.MODCTX__V5*/ meltfptr[4];
      /*_.XVAL__V24*/ meltfptr[23] =
	melt_apply ((meltclosure_ptr_t) ( /*_.MEXPANDER__V4*/ meltfptr[3]),
		    (melt_ptr_t) ( /*_.PAIR_HEAD__V23*/ meltfptr[22]),
		    (MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_PTR ""),
		    argtab, "", (union meltparam_un *) 0);
    }
    ;
    /*_.XMSG__V25*/ meltfptr[24] = ( /*nil */ NULL);;
    /*^compute */
    /*_.XCOUNT__V26*/ meltfptr[25] = ( /*nil */ NULL);;

    {
      MELT_LOCATION ("warmelt-macro.melt:6321:/ locexp");
      melt_warning_str (0, (melt_ptr_t) ( /*_.LOC__V20*/ meltfptr[19]),
			("(DEBUG_MSG ....) is obsolete, use (DEBUG ...) variadically"),
			(melt_ptr_t) 0);
    }
    ;
 /*_.PAIR_TAIL__V27*/ meltfptr[26] =
      (melt_pair_tail ((melt_ptr_t) ( /*_.CURPAIR__V22*/ meltfptr[21])));;
    MELT_LOCATION ("warmelt-macro.melt:6322:/ compute");
    /*_.CURPAIR__V22*/ meltfptr[21] = /*_.SETQ___V28*/ meltfptr[27] =
      /*_.PAIR_TAIL__V27*/ meltfptr[26];;
    MELT_LOCATION ("warmelt-macro.melt:6323:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#IS_PAIR__L7*/ meltfnum[1] =
      (melt_magic_discr ((melt_ptr_t) ( /*_.CURPAIR__V22*/ meltfptr[21])) ==
       MELTOBMAG_PAIR);;
    /*^compute */
 /*_#NOT__L8*/ meltfnum[0] =
      (!( /*_#IS_PAIR__L7*/ meltfnum[1]));;
    MELT_LOCATION ("warmelt-macro.melt:6323:/ cond");
    /*cond */ if ( /*_#NOT__L8*/ meltfnum[0])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{


	  {
	    MELT_LOCATION ("warmelt-macro.melt:6324:/ locexp");
	    /* error_plain */
	      melt_error_str ((melt_ptr_t) ( /*_.LOC__V20*/ meltfptr[19]),
			      ("missing message in DEBUG_MSG"),
			      (melt_ptr_t) 0);
	  }
	  ;
	  /*epilog */
	}
	;
      }				/*noelse */
    ;
 /*_.PAIR_HEAD__V29*/ meltfptr[28] =
      (melt_pair_head ((melt_ptr_t) ( /*_.CURPAIR__V22*/ meltfptr[21])));;
    MELT_LOCATION ("warmelt-macro.melt:6325:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[3];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.ENV__V3*/ meltfptr[2];
      /*^apply.arg */
      argtab[1].meltbp_aptr =
	(melt_ptr_t *) & /*_.MEXPANDER__V4*/ meltfptr[3];
      /*^apply.arg */
      argtab[2].meltbp_aptr = (melt_ptr_t *) & /*_.MODCTX__V5*/ meltfptr[4];
      /*_.MEXPANDER__V30*/ meltfptr[29] =
	melt_apply ((meltclosure_ptr_t) ( /*_.MEXPANDER__V4*/ meltfptr[3]),
		    (melt_ptr_t) ( /*_.PAIR_HEAD__V29*/ meltfptr[28]),
		    (MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_PTR ""),
		    argtab, "", (union meltparam_un *) 0);
    }
    ;
    /*^compute */
    /*_.XMSG__V25*/ meltfptr[24] = /*_.SETQ___V31*/ meltfptr[30] =
      /*_.MEXPANDER__V30*/ meltfptr[29];;
 /*_.PAIR_TAIL__V32*/ meltfptr[31] =
      (melt_pair_tail ((melt_ptr_t) ( /*_.CURPAIR__V22*/ meltfptr[21])));;
    MELT_LOCATION ("warmelt-macro.melt:6326:/ compute");
    /*_.CURPAIR__V22*/ meltfptr[21] = /*_.SETQ___V33*/ meltfptr[32] =
      /*_.PAIR_TAIL__V32*/ meltfptr[31];;
    MELT_LOCATION ("warmelt-macro.melt:6327:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#IS_PAIR__L9*/ meltfnum[8] =
      (melt_magic_discr ((melt_ptr_t) ( /*_.CURPAIR__V22*/ meltfptr[21])) ==
       MELTOBMAG_PAIR);;
    MELT_LOCATION ("warmelt-macro.melt:6327:/ cond");
    /*cond */ if ( /*_#IS_PAIR__L9*/ meltfnum[8])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

   /*_.PAIR_HEAD__V35*/ meltfptr[34] =
	    (melt_pair_head
	     ((melt_ptr_t) ( /*_.CURPAIR__V22*/ meltfptr[21])));;
	  MELT_LOCATION ("warmelt-macro.melt:6329:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^apply */
	  /*apply */
	  {
	    union meltparam_un argtab[3];
	    memset (&argtab, 0, sizeof (argtab));
	    /*^apply.arg */
	    argtab[0].meltbp_aptr =
	      (melt_ptr_t *) & /*_.ENV__V3*/ meltfptr[2];
	    /*^apply.arg */
	    argtab[1].meltbp_aptr =
	      (melt_ptr_t *) & /*_.MEXPANDER__V4*/ meltfptr[3];
	    /*^apply.arg */
	    argtab[2].meltbp_aptr =
	      (melt_ptr_t *) & /*_.MODCTX__V5*/ meltfptr[4];
	    /*_.MEXPANDER__V36*/ meltfptr[35] =
	      melt_apply ((meltclosure_ptr_t)
			  ( /*_.MEXPANDER__V4*/ meltfptr[3]),
			  (melt_ptr_t) ( /*_.PAIR_HEAD__V35*/ meltfptr[34]),
			  (MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_PTR
			   ""), argtab, "", (union meltparam_un *) 0);
	  }
	  ;
	  /*^compute */
	  /*_.XCOUNT__V26*/ meltfptr[25] = /*_.SETQ___V37*/ meltfptr[36] =
	    /*_.MEXPANDER__V36*/ meltfptr[35];;
   /*_.PAIR_TAIL__V38*/ meltfptr[37] =
	    (melt_pair_tail
	     ((melt_ptr_t) ( /*_.CURPAIR__V22*/ meltfptr[21])));;
	  MELT_LOCATION ("warmelt-macro.melt:6330:/ compute");
	  /*_.CURPAIR__V22*/ meltfptr[21] = /*_.SETQ___V39*/ meltfptr[38] =
	    /*_.PAIR_TAIL__V38*/ meltfptr[37];;
	  MELT_LOCATION ("warmelt-macro.melt:6328:/ quasiblock");


	  /*_.PROGN___V40*/ meltfptr[39] = /*_.SETQ___V39*/ meltfptr[38];;
	  /*^compute */
	  /*_.IF___V34*/ meltfptr[33] = /*_.PROGN___V40*/ meltfptr[39];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-macro.melt:6327:/ clear");
	     /*clear *//*_.PAIR_HEAD__V35*/ meltfptr[34] = 0;
	  /*^clear */
	     /*clear *//*_.MEXPANDER__V36*/ meltfptr[35] = 0;
	  /*^clear */
	     /*clear *//*_.SETQ___V37*/ meltfptr[36] = 0;
	  /*^clear */
	     /*clear *//*_.PAIR_TAIL__V38*/ meltfptr[37] = 0;
	  /*^clear */
	     /*clear *//*_.SETQ___V39*/ meltfptr[38] = 0;
	  /*^clear */
	     /*clear *//*_.PROGN___V40*/ meltfptr[39] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

  /*_.IF___V34*/ meltfptr[33] = NULL;;
      }
    ;
    MELT_LOCATION ("warmelt-macro.melt:6331:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^cond */
    /*cond */ if ( /*_.CURPAIR__V22*/ meltfptr[21])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{


	  {
	    MELT_LOCATION ("warmelt-macro.melt:6332:/ locexp");
	    /* error_plain */
	      melt_error_str ((melt_ptr_t) ( /*_.LOC__V20*/ meltfptr[19]),
			      ("too many arguments to DEBUG_MSG"),
			      (melt_ptr_t) 0);
	  }
	  ;
	  /*epilog */
	}
	;
      }				/*noelse */
    ;
    MELT_LOCATION ("warmelt-macro.melt:6333:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#IS_STRING__L10*/ meltfnum[9] =
      (melt_magic_discr ((melt_ptr_t) ( /*_.XMSG__V25*/ meltfptr[24])) ==
       MELTOBMAG_STRING);;
    /*^compute */
 /*_#NOT__L11*/ meltfnum[10] =
      (!( /*_#IS_STRING__L10*/ meltfnum[9]));;
    MELT_LOCATION ("warmelt-macro.melt:6333:/ cond");
    /*cond */ if ( /*_#NOT__L11*/ meltfnum[10])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{


	  {
	    MELT_LOCATION ("warmelt-macro.melt:6334:/ locexp");
	    /* error_plain */
	      melt_error_str ((melt_ptr_t) ( /*_.LOC__V20*/ meltfptr[19]),
			      ("message argument should be string in DEBUG_MSG"),
			      (melt_ptr_t) 0);
	  }
	  ;
	  /*epilog */
	}
	;
      }				/*noelse */
    ;
    MELT_LOCATION ("warmelt-macro.melt:6335:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#NULL__L12*/ meltfnum[11] =
      (( /*_.XCOUNT__V26*/ meltfptr[25]) == NULL);;
    MELT_LOCATION ("warmelt-macro.melt:6335:/ cond");
    /*cond */ if ( /*_#NULL__L12*/ meltfnum[11])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-macro.melt:6336:/ quasiblock");


   /*_.CONLIS__V43*/ meltfptr[36] =
	    (meltgc_new_list
	     ((meltobject_ptr_t)
	      (( /*!DISCR_LIST */ meltfrout->tabval[3]))));;

	  {
	    MELT_LOCATION ("warmelt-macro.melt:6337:/ locexp");
	    meltgc_append_list ((melt_ptr_t)
				( /*_.CONLIS__V43*/ meltfptr[36]),
				(melt_ptr_t) (( /*!konst_4_THE_MELTCALLCOUNT */ meltfrout->tabval[4])));
	  }
	  ;
	  MELT_LOCATION ("warmelt-macro.melt:6338:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^quasiblock */


	  /*^rawallocobj */
	  /*rawallocobj */
	  {
	    melt_ptr_t newobj = 0;
	    melt_raw_object_create (newobj,
				    (melt_ptr_t) (( /*!CLASS_SEXPR */
						   meltfrout->tabval[1])),
				    (3), "CLASS_SEXPR");
    /*_.INST__V45*/ meltfptr[38] =
	      newobj;
	  };
	  ;
	  /*^putslot */
	  /*putslot */
	  melt_assertmsg ("putslot checkobj @SEXP_CONTENTS",
			  melt_magic_discr ((melt_ptr_t)
					    ( /*_.INST__V45*/ meltfptr[38]))
			  == MELTOBMAG_OBJECT);
	  melt_putfield_object (( /*_.INST__V45*/ meltfptr[38]), (2),
				( /*_.CONLIS__V43*/ meltfptr[36]),
				"SEXP_CONTENTS");
	  ;
	  /*^putslot */
	  /*putslot */
	  melt_assertmsg ("putslot checkobj @LOCA_LOCATION",
			  melt_magic_discr ((melt_ptr_t)
					    ( /*_.INST__V45*/ meltfptr[38]))
			  == MELTOBMAG_OBJECT);
	  melt_putfield_object (( /*_.INST__V45*/ meltfptr[38]), (1),
				( /*_.LOC__V20*/ meltfptr[19]),
				"LOCA_LOCATION");
	  ;
	  /*^touchobj */

	  melt_dbgtrace_written_object ( /*_.INST__V45*/ meltfptr[38],
					"newly made instance");
	  ;
	  /*_.INST___V44*/ meltfptr[37] = /*_.INST__V45*/ meltfptr[38];;
	  MELT_LOCATION ("warmelt-macro.melt:6338:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^apply */
	  /*apply */
	  {
	    union meltparam_un argtab[2];
	    memset (&argtab, 0, sizeof (argtab));
	    /*^apply.arg */
	    argtab[0].meltbp_aptr =
	      (melt_ptr_t *) & /*_.ENV__V3*/ meltfptr[2];
	    /*^apply.arg */
	    argtab[1].meltbp_aptr =
	      (melt_ptr_t *) & /*_.MEXPANDER__V4*/ meltfptr[3];
	    /*_.MACROEXPAND_1__V46*/ meltfptr[39] =
	      melt_apply ((meltclosure_ptr_t)
			  (( /*!MACROEXPAND_1 */ meltfrout->tabval[5])),
			  (melt_ptr_t) ( /*_.INST___V44*/ meltfptr[37]),
			  (MELTBPARSTR_PTR MELTBPARSTR_PTR ""), argtab, "",
			  (union meltparam_un *) 0);
	  }
	  ;
	  /*^compute */
	  /*_.XCOUNT__V26*/ meltfptr[25] = /*_.SETQ___V47*/ meltfptr[46] =
	    /*_.MACROEXPAND_1__V46*/ meltfptr[39];;
	  /*_.LET___V42*/ meltfptr[35] = /*_.SETQ___V47*/ meltfptr[46];;

	  MELT_LOCATION ("warmelt-macro.melt:6336:/ clear");
	     /*clear *//*_.CONLIS__V43*/ meltfptr[36] = 0;
	  /*^clear */
	     /*clear *//*_.INST___V44*/ meltfptr[37] = 0;
	  /*^clear */
	     /*clear *//*_.MACROEXPAND_1__V46*/ meltfptr[39] = 0;
	  /*^clear */
	     /*clear *//*_.SETQ___V47*/ meltfptr[46] = 0;
	  /*_.IF___V41*/ meltfptr[34] = /*_.LET___V42*/ meltfptr[35];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-macro.melt:6335:/ clear");
	     /*clear *//*_.LET___V42*/ meltfptr[35] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

  /*_.IF___V41*/ meltfptr[34] = NULL;;
      }
    ;
    MELT_LOCATION ("warmelt-macro.melt:6342:/ quasiblock");


    MELT_LOCATION ("warmelt-macro.melt:6344:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#IS_MIXINT__L13*/ meltfnum[12] =
      (melt_magic_discr ((melt_ptr_t) ( /*_.LOC__V20*/ meltfptr[19])) ==
       MELTOBMAG_MIXINT);;
    MELT_LOCATION ("warmelt-macro.melt:6344:/ cond");
    /*cond */ if ( /*_#IS_MIXINT__L13*/ meltfnum[12])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

   /*_.MIXINT_VAL__V50*/ meltfptr[39] =
	    (melt_val_mixint ((melt_ptr_t) ( /*_.LOC__V20*/ meltfptr[19])));;
	  /*^compute */
	  /*_.DFILNAM__V49*/ meltfptr[37] =
	    /*_.MIXINT_VAL__V50*/ meltfptr[39];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-macro.melt:6344:/ clear");
	     /*clear *//*_.MIXINT_VAL__V50*/ meltfptr[39] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-macro.melt:6345:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
   /*_#IS_MIXLOC__L14*/ meltfnum[13] =
	    (melt_magic_discr ((melt_ptr_t) ( /*_.LOC__V20*/ meltfptr[19])) ==
	     MELTOBMAG_MIXLOC);;
	  MELT_LOCATION ("warmelt-macro.melt:6345:/ cond");
	  /*cond */ if ( /*_#IS_MIXLOC__L14*/ meltfnum[13])	/*then */
	    {
	      /*^cond.then */
	      /*^block */
	      /*anyblock */
	      {

     /*_.MIXLOC_VAL__V52*/ meltfptr[35] =
		  (melt_val_mixloc
		   ((melt_ptr_t) ( /*_.LOC__V20*/ meltfptr[19])));;
		/*^compute */
		/*_.IFELSE___V51*/ meltfptr[46] =
		  /*_.MIXLOC_VAL__V52*/ meltfptr[35];;
		/*epilog */

		MELT_LOCATION ("warmelt-macro.melt:6345:/ clear");
	       /*clear *//*_.MIXLOC_VAL__V52*/ meltfptr[35] = 0;
	      }
	      ;
	    }
	  else
	    {			/*^cond.else */

    /*_.IFELSE___V51*/ meltfptr[46] = NULL;;
	    }
	  ;
	  /*^compute */
	  /*_.DFILNAM__V49*/ meltfptr[37] = /*_.IFELSE___V51*/ meltfptr[46];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-macro.melt:6344:/ clear");
	     /*clear *//*_#IS_MIXLOC__L14*/ meltfnum[13] = 0;
	  /*^clear */
	     /*clear *//*_.IFELSE___V51*/ meltfptr[46] = 0;
	}
	;
      }
    ;
    MELT_LOCATION ("warmelt-macro.melt:6346:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#GET_INT__L15*/ meltfnum[13] =
      (melt_get_int ((melt_ptr_t) ( /*_.LOC__V20*/ meltfptr[19])));;
    /*^compute */
 /*_.MAKE_INTEGERBOX__V53*/ meltfptr[39] =
      (meltgc_new_int
       ((meltobject_ptr_t) (( /*!DISCR_INTEGER */ meltfrout->tabval[8])),
	( /*_#GET_INT__L15*/ meltfnum[13])));;
    MELT_LOCATION ("warmelt-macro.melt:6350:/ blockmultialloc");
    /*multiallocblock */
    {
      struct meltletrec_1_st
      {
	struct MELT_MULTIPLE_STRUCT (5) rtup_0__TUPLREC__x10;
	long meltletrec_1_endgap;
      } *meltletrec_1_ptr = 0;
      meltletrec_1_ptr =
	(struct meltletrec_1_st *)
	meltgc_allocate (sizeof (struct meltletrec_1_st), 0);
      /*^blockmultialloc.initfill */
      /*inimult rtup_0__TUPLREC__x10 */
 /*_.TUPLREC___V55*/ meltfptr[46] =
	(melt_ptr_t) & meltletrec_1_ptr->rtup_0__TUPLREC__x10;
      meltletrec_1_ptr->rtup_0__TUPLREC__x10.discr =
	(meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (DISCR_MULTIPLE))));
      meltletrec_1_ptr->rtup_0__TUPLREC__x10.nbval = 5;


      /*^putuple */
      /*putupl#15 */
      melt_assertmsg ("putupl [:6350] #15 checktup",
		      melt_magic_discr ((melt_ptr_t)
					( /*_.TUPLREC___V55*/ meltfptr[46]))
		      == MELTOBMAG_MULTIPLE);
      melt_assertmsg ("putupl [:6350] #15 checkoff",
		      (0 >= 0
		       && 0 <
		       melt_multiple_length ((melt_ptr_t)
					     ( /*_.TUPLREC___V55*/
					      meltfptr[46]))));
      ((meltmultiple_ptr_t) ( /*_.TUPLREC___V55*/ meltfptr[46]))->tabval[0] =
	(melt_ptr_t) ( /*_.XVAL__V24*/ meltfptr[23]);
      ;
      /*^putuple */
      /*putupl#16 */
      melt_assertmsg ("putupl [:6350] #16 checktup",
		      melt_magic_discr ((melt_ptr_t)
					( /*_.TUPLREC___V55*/ meltfptr[46]))
		      == MELTOBMAG_MULTIPLE);
      melt_assertmsg ("putupl [:6350] #16 checkoff",
		      (1 >= 0
		       && 1 <
		       melt_multiple_length ((melt_ptr_t)
					     ( /*_.TUPLREC___V55*/
					      meltfptr[46]))));
      ((meltmultiple_ptr_t) ( /*_.TUPLREC___V55*/ meltfptr[46]))->tabval[1] =
	(melt_ptr_t) ( /*_.XMSG__V25*/ meltfptr[24]);
      ;
      /*^putuple */
      /*putupl#17 */
      melt_assertmsg ("putupl [:6350] #17 checktup",
		      melt_magic_discr ((melt_ptr_t)
					( /*_.TUPLREC___V55*/ meltfptr[46]))
		      == MELTOBMAG_MULTIPLE);
      melt_assertmsg ("putupl [:6350] #17 checkoff",
		      (2 >= 0
		       && 2 <
		       melt_multiple_length ((melt_ptr_t)
					     ( /*_.TUPLREC___V55*/
					      meltfptr[46]))));
      ((meltmultiple_ptr_t) ( /*_.TUPLREC___V55*/ meltfptr[46]))->tabval[2] =
	(melt_ptr_t) ( /*_.XCOUNT__V26*/ meltfptr[25]);
      ;
      /*^putuple */
      /*putupl#18 */
      melt_assertmsg ("putupl [:6350] #18 checktup",
		      melt_magic_discr ((melt_ptr_t)
					( /*_.TUPLREC___V55*/ meltfptr[46]))
		      == MELTOBMAG_MULTIPLE);
      melt_assertmsg ("putupl [:6350] #18 checkoff",
		      (3 >= 0
		       && 3 <
		       melt_multiple_length ((melt_ptr_t)
					     ( /*_.TUPLREC___V55*/
					      meltfptr[46]))));
      ((meltmultiple_ptr_t) ( /*_.TUPLREC___V55*/ meltfptr[46]))->tabval[3] =
	(melt_ptr_t) ( /*_.DFILNAM__V49*/ meltfptr[37]);
      ;
      /*^putuple */
      /*putupl#19 */
      melt_assertmsg ("putupl [:6350] #19 checktup",
		      melt_magic_discr ((melt_ptr_t)
					( /*_.TUPLREC___V55*/ meltfptr[46]))
		      == MELTOBMAG_MULTIPLE);
      melt_assertmsg ("putupl [:6350] #19 checkoff",
		      (4 >= 0
		       && 4 <
		       melt_multiple_length ((melt_ptr_t)
					     ( /*_.TUPLREC___V55*/
					      meltfptr[46]))));
      ((meltmultiple_ptr_t) ( /*_.TUPLREC___V55*/ meltfptr[46]))->tabval[4] =
	(melt_ptr_t) ( /*_.MAKE_INTEGERBOX__V53*/ meltfptr[39]);
      ;
      /*^touch */
      meltgc_touch ( /*_.TUPLREC___V55*/ meltfptr[46]);
      ;
      /*_.TUPLE___V54*/ meltfptr[35] = /*_.TUPLREC___V55*/ meltfptr[46];;
      /*epilog */

      MELT_LOCATION ("warmelt-macro.melt:6350:/ clear");
	    /*clear *//*_.TUPLREC___V55*/ meltfptr[46] = 0;
      /*^clear */
	    /*clear *//*_.TUPLREC___V55*/ meltfptr[46] = 0;
    }				/*end multiallocblock */
    ;
    MELT_LOCATION ("warmelt-macro.melt:6346:/ quasiblock");


    /*^rawallocobj */
    /*rawallocobj */
    {
      melt_ptr_t newobj = 0;
      melt_raw_object_create (newobj,
			      (melt_ptr_t) (( /*!CLASS_SOURCE_APPLY */
					     meltfrout->tabval[6])), (4),
			      "CLASS_SOURCE_APPLY");
  /*_.INST__V57*/ meltfptr[56] =
	newobj;
    };
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @LOCA_LOCATION",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V57*/ meltfptr[56])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V57*/ meltfptr[56]), (1),
			  ( /*_.LOC__V20*/ meltfptr[19]), "LOCA_LOCATION");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @SAPP_FUN",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V57*/ meltfptr[56])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V57*/ meltfptr[56]), (3),
			  (( /*!konst_7_DEBUG_MSG_FUN */ meltfrout->
			    tabval[7])), "SAPP_FUN");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @SARGOP_ARGS",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V57*/ meltfptr[56])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V57*/ meltfptr[56]), (2),
			  ( /*_.TUPLE___V54*/ meltfptr[35]), "SARGOP_ARGS");
    ;
    /*^touchobj */

    melt_dbgtrace_written_object ( /*_.INST__V57*/ meltfptr[56],
				  "newly made instance");
    ;
    /*_.DCALL__V56*/ meltfptr[46] = /*_.INST__V57*/ meltfptr[56];;
    MELT_LOCATION ("warmelt-macro.melt:6356:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*^rawallocobj */
    /*rawallocobj */
    {
      melt_ptr_t newobj = 0;
      melt_raw_object_create (newobj,
			      (melt_ptr_t) (( /*!CLASS_SOURCE_CPPIF */
					     meltfrout->tabval[9])), (5),
			      "CLASS_SOURCE_CPPIF");
  /*_.INST__V59*/ meltfptr[58] =
	newobj;
    };
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @LOCA_LOCATION",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V59*/ meltfptr[58])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V59*/ meltfptr[58]), (1),
			  ( /*_.LOC__V20*/ meltfptr[19]), "LOCA_LOCATION");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @SIFP_COND",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V59*/ meltfptr[58])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V59*/ meltfptr[58]), (2),
			  (( /*!konst_10_MELT_HAVE_DEBUG */ meltfrout->
			    tabval[10])), "SIFP_COND");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @SIFP_THEN",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V59*/ meltfptr[58])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V59*/ meltfptr[58]), (3),
			  ( /*_.DCALL__V56*/ meltfptr[46]), "SIFP_THEN");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @SIFP_ELSE",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V59*/ meltfptr[58])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V59*/ meltfptr[58]), (4),
			  (( /*nil */ NULL)), "SIFP_ELSE");
    ;
    /*^touchobj */

    melt_dbgtrace_written_object ( /*_.INST__V59*/ meltfptr[58],
				  "newly made instance");
    ;
    /*_.DCPPIF__V58*/ meltfptr[57] = /*_.INST__V59*/ meltfptr[58];;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-macro.melt:6363:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L16*/ meltfnum[15] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-macro.melt:6363:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L16*/ meltfnum[15])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L17*/ meltfnum[16] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-macro.melt:6363:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long =
		/*_#THE_MELTCALLCOUNT__L17*/ meltfnum[16];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-macro.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 6363;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "mexpand_debug_msg result dcppif";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.DCPPIF__V58*/ meltfptr[57];
	      /*_.MELT_DEBUG_FUN__V62*/ meltfptr[61] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V61*/ meltfptr[60] =
	      /*_.MELT_DEBUG_FUN__V62*/ meltfptr[61];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-macro.melt:6363:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L17*/ meltfnum[16] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V62*/ meltfptr[61] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V61*/ meltfptr[60] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-macro.melt:6363:/ quasiblock");


      /*_.PROGN___V63*/ meltfptr[61] = /*_.IF___V61*/ meltfptr[60];;
      /*^compute */
      /*_.IFCPP___V60*/ meltfptr[59] = /*_.PROGN___V63*/ meltfptr[61];;
      /*epilog */

      MELT_LOCATION ("warmelt-macro.melt:6363:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L16*/ meltfnum[15] = 0;
      /*^clear */
	     /*clear *//*_.IF___V61*/ meltfptr[60] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V63*/ meltfptr[61] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V60*/ meltfptr[59] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-macro.melt:6364:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.DCPPIF__V58*/ meltfptr[57];;

    {
      MELT_LOCATION ("warmelt-macro.melt:6364:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*_.LET___V48*/ meltfptr[36] = /*_.RETURN___V64*/ meltfptr[60];;

    MELT_LOCATION ("warmelt-macro.melt:6342:/ clear");
	   /*clear *//*_#IS_MIXINT__L13*/ meltfnum[12] = 0;
    /*^clear */
	   /*clear *//*_.DFILNAM__V49*/ meltfptr[37] = 0;
    /*^clear */
	   /*clear *//*_#GET_INT__L15*/ meltfnum[13] = 0;
    /*^clear */
	   /*clear *//*_.MAKE_INTEGERBOX__V53*/ meltfptr[39] = 0;
    /*^clear */
	   /*clear *//*_.TUPLE___V54*/ meltfptr[35] = 0;
    /*^clear */
	   /*clear *//*_.DCALL__V56*/ meltfptr[46] = 0;
    /*^clear */
	   /*clear *//*_.DCPPIF__V58*/ meltfptr[57] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V60*/ meltfptr[59] = 0;
    /*^clear */
	   /*clear *//*_.RETURN___V64*/ meltfptr[60] = 0;
    /*_.LET___V18*/ meltfptr[16] = /*_.LET___V48*/ meltfptr[36];;

    MELT_LOCATION ("warmelt-macro.melt:6313:/ clear");
	   /*clear *//*_.CONT__V19*/ meltfptr[18] = 0;
    /*^clear */
	   /*clear *//*_.LOC__V20*/ meltfptr[19] = 0;
    /*^clear */
	   /*clear *//*_.LIST_FIRST__V21*/ meltfptr[20] = 0;
    /*^clear */
	   /*clear *//*_.CURPAIR__V22*/ meltfptr[21] = 0;
    /*^clear */
	   /*clear *//*_.PAIR_HEAD__V23*/ meltfptr[22] = 0;
    /*^clear */
	   /*clear *//*_.XVAL__V24*/ meltfptr[23] = 0;
    /*^clear */
	   /*clear *//*_.XMSG__V25*/ meltfptr[24] = 0;
    /*^clear */
	   /*clear *//*_.XCOUNT__V26*/ meltfptr[25] = 0;
    /*^clear */
	   /*clear *//*_.PAIR_TAIL__V27*/ meltfptr[26] = 0;
    /*^clear */
	   /*clear *//*_.SETQ___V28*/ meltfptr[27] = 0;
    /*^clear */
	   /*clear *//*_#IS_PAIR__L7*/ meltfnum[1] = 0;
    /*^clear */
	   /*clear *//*_#NOT__L8*/ meltfnum[0] = 0;
    /*^clear */
	   /*clear *//*_.PAIR_HEAD__V29*/ meltfptr[28] = 0;
    /*^clear */
	   /*clear *//*_.MEXPANDER__V30*/ meltfptr[29] = 0;
    /*^clear */
	   /*clear *//*_.SETQ___V31*/ meltfptr[30] = 0;
    /*^clear */
	   /*clear *//*_.PAIR_TAIL__V32*/ meltfptr[31] = 0;
    /*^clear */
	   /*clear *//*_.SETQ___V33*/ meltfptr[32] = 0;
    /*^clear */
	   /*clear *//*_#IS_PAIR__L9*/ meltfnum[8] = 0;
    /*^clear */
	   /*clear *//*_.IF___V34*/ meltfptr[33] = 0;
    /*^clear */
	   /*clear *//*_#IS_STRING__L10*/ meltfnum[9] = 0;
    /*^clear */
	   /*clear *//*_#NOT__L11*/ meltfnum[10] = 0;
    /*^clear */
	   /*clear *//*_#NULL__L12*/ meltfnum[11] = 0;
    /*^clear */
	   /*clear *//*_.IF___V41*/ meltfptr[34] = 0;
    /*^clear */
	   /*clear *//*_.LET___V48*/ meltfptr[36] = 0;
    MELT_LOCATION ("warmelt-macro.melt:6307:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.LET___V18*/ meltfptr[16];;

    {
      MELT_LOCATION ("warmelt-macro.melt:6307:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V6*/ meltfptr[5] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V10*/ meltfptr[6] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V12*/ meltfptr[7] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V14*/ meltfptr[12] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V16*/ meltfptr[14] = 0;
    /*^clear */
	   /*clear *//*_.LET___V18*/ meltfptr[16] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("MEXPAND_DEBUG_MSG", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_136_warmelt_macro_MEXPAND_DEBUG_MSG_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_136_warmelt_macro_MEXPAND_DEBUG_MSG */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_137_warmelt_macro_MEXPAND_DEBUG (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un * meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un * meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_137_warmelt_macro_MEXPAND_DEBUG_melt =
    melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_137_warmelt_macro_MEXPAND_DEBUG_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 94
    melt_ptr_t mcfr_varptr[94];
#define MELTFRAM_NBVARNUM 30
    long mcfr_varnum[30];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_137_warmelt_macro_MEXPAND_DEBUG is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_137_warmelt_macro_MEXPAND_DEBUG_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 94; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_137_warmelt_macro_MEXPAND_DEBUG nbval 94*/
  meltfram__.mcfr_nbvar = 94 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("MEXPAND_DEBUG", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-macro.melt:6380:/ getarg");
 /*_.SEXPR__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.ENV__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.ENV__V3*/ meltfptr[2])) != NULL);


  /*getarg#2 */
  /*^getarg */
  if (meltxargdescr_[1] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.MEXPANDER__V4*/ meltfptr[3] =
    (meltxargtab_[1].meltbp_aptr) ? (*(meltxargtab_[1].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.MEXPANDER__V4*/ meltfptr[3])) !=
	      NULL);


  /*getarg#3 */
  /*^getarg */
  if (meltxargdescr_[2] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.MODCTX__V5*/ meltfptr[4] =
    (meltxargtab_[2].meltbp_aptr) ? (*(meltxargtab_[2].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.MODCTX__V5*/ meltfptr[4])) !=
	      NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-macro.melt:6381:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L1*/ meltfnum[0] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-macro.melt:6381:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L1*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-macro.melt:6381:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-macro.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 6381;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "mexpand_debug sexpr";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.SEXPR__V2*/ meltfptr[1];
	      /*_.MELT_DEBUG_FUN__V8*/ meltfptr[7] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V7*/ meltfptr[6] = /*_.MELT_DEBUG_FUN__V8*/ meltfptr[7];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-macro.melt:6381:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V8*/ meltfptr[7] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V7*/ meltfptr[6] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-macro.melt:6381:/ quasiblock");


      /*_.PROGN___V9*/ meltfptr[7] = /*_.IF___V7*/ meltfptr[6];;
      /*^compute */
      /*_.IFCPP___V6*/ meltfptr[5] = /*_.PROGN___V9*/ meltfptr[7];;
      /*epilog */

      MELT_LOCATION ("warmelt-macro.melt:6381:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L1*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IF___V7*/ meltfptr[6] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V9*/ meltfptr[7] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V6*/ meltfptr[5] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-macro.melt:6382:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L3*/ meltfnum[1] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.SEXPR__V2*/ meltfptr[1]),
			     (melt_ptr_t) (( /*!CLASS_SEXPR */ meltfrout->
					    tabval[1])));;
      MELT_LOCATION ("warmelt-macro.melt:6382:/ cond");
      /*cond */ if ( /*_#IS_A__L3*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V11*/ meltfptr[7] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-macro.melt:6382:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check sexpr"),
				  ("warmelt-macro.melt")
				  ? ("warmelt-macro.melt") : __FILE__,
				  (6382) ? (6382) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V11*/ meltfptr[7] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V10*/ meltfptr[6] = /*_.IFELSE___V11*/ meltfptr[7];;
      /*epilog */

      MELT_LOCATION ("warmelt-macro.melt:6382:/ clear");
	     /*clear *//*_#IS_A__L3*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V11*/ meltfptr[7] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V10*/ meltfptr[6] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-macro.melt:6383:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L4*/ meltfnum[0] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.ENV__V3*/ meltfptr[2]),
			     (melt_ptr_t) (( /*!CLASS_ENVIRONMENT */
					    meltfrout->tabval[2])));;
      MELT_LOCATION ("warmelt-macro.melt:6383:/ cond");
      /*cond */ if ( /*_#IS_A__L4*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V13*/ meltfptr[12] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-macro.melt:6383:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check env"),
				  ("warmelt-macro.melt")
				  ? ("warmelt-macro.melt") : __FILE__,
				  (6383) ? (6383) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V13*/ meltfptr[12] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V12*/ meltfptr[7] = /*_.IFELSE___V13*/ meltfptr[12];;
      /*epilog */

      MELT_LOCATION ("warmelt-macro.melt:6383:/ clear");
	     /*clear *//*_#IS_A__L4*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V13*/ meltfptr[12] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V12*/ meltfptr[7] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-macro.melt:6384:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_OBJECT__L5*/ meltfnum[1] =
	(melt_magic_discr ((melt_ptr_t) ( /*_.MODCTX__V5*/ meltfptr[4])) ==
	 MELTOBMAG_OBJECT);;
      MELT_LOCATION ("warmelt-macro.melt:6384:/ cond");
      /*cond */ if ( /*_#IS_OBJECT__L5*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V15*/ meltfptr[14] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-macro.melt:6384:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check modctx"),
				  ("warmelt-macro.melt")
				  ? ("warmelt-macro.melt") : __FILE__,
				  (6384) ? (6384) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V15*/ meltfptr[14] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V14*/ meltfptr[12] = /*_.IFELSE___V15*/ meltfptr[14];;
      /*epilog */

      MELT_LOCATION ("warmelt-macro.melt:6384:/ clear");
	     /*clear *//*_#IS_OBJECT__L5*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V15*/ meltfptr[14] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V14*/ meltfptr[12] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-macro.melt:6385:/ quasiblock");


    MELT_LOCATION ("warmelt-macro.melt:6386:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.SEXPR__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 2, "SEXP_CONTENTS");
  /*_.CONT__V17*/ meltfptr[16] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-macro.melt:6387:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.SEXPR__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 1, "LOCA_LOCATION");
  /*_.LOC__V18*/ meltfptr[17] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-macro.melt:6389:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#IS_MIXINT__L6*/ meltfnum[0] =
      (melt_magic_discr ((melt_ptr_t) ( /*_.LOC__V18*/ meltfptr[17])) ==
       MELTOBMAG_MIXINT);;
    MELT_LOCATION ("warmelt-macro.melt:6389:/ cond");
    /*cond */ if ( /*_#IS_MIXINT__L6*/ meltfnum[0])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

   /*_.MIXINT_VAL__V20*/ meltfptr[19] =
	    (melt_val_mixint ((melt_ptr_t) ( /*_.LOC__V18*/ meltfptr[17])));;
	  /*^compute */
	  /*_.DFILNAM__V19*/ meltfptr[18] =
	    /*_.MIXINT_VAL__V20*/ meltfptr[19];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-macro.melt:6389:/ clear");
	     /*clear *//*_.MIXINT_VAL__V20*/ meltfptr[19] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-macro.melt:6390:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
   /*_#IS_MIXLOC__L7*/ meltfnum[1] =
	    (melt_magic_discr ((melt_ptr_t) ( /*_.LOC__V18*/ meltfptr[17])) ==
	     MELTOBMAG_MIXLOC);;
	  MELT_LOCATION ("warmelt-macro.melt:6390:/ cond");
	  /*cond */ if ( /*_#IS_MIXLOC__L7*/ meltfnum[1])	/*then */
	    {
	      /*^cond.then */
	      /*^block */
	      /*anyblock */
	      {

     /*_.MIXLOC_VAL__V22*/ meltfptr[21] =
		  (melt_val_mixloc
		   ((melt_ptr_t) ( /*_.LOC__V18*/ meltfptr[17])));;
		/*^compute */
		/*_.IFELSE___V21*/ meltfptr[19] =
		  /*_.MIXLOC_VAL__V22*/ meltfptr[21];;
		/*epilog */

		MELT_LOCATION ("warmelt-macro.melt:6390:/ clear");
	       /*clear *//*_.MIXLOC_VAL__V22*/ meltfptr[21] = 0;
	      }
	      ;
	    }
	  else
	    {			/*^cond.else */

    /*_.IFELSE___V21*/ meltfptr[19] = NULL;;
	    }
	  ;
	  /*^compute */
	  /*_.DFILNAM__V19*/ meltfptr[18] = /*_.IFELSE___V21*/ meltfptr[19];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-macro.melt:6389:/ clear");
	     /*clear *//*_#IS_MIXLOC__L7*/ meltfnum[1] = 0;
	  /*^clear */
	     /*clear *//*_.IFELSE___V21*/ meltfptr[19] = 0;
	}
	;
      }
    ;
 /*_#DLINE__L8*/ meltfnum[1] =
      (melt_get_int ((melt_ptr_t) ( /*_.LOC__V18*/ meltfptr[17])));;
    MELT_LOCATION ("warmelt-macro.melt:6392:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & ( /*!konst_4_MELT_DEBUG_FUN */ meltfrout->tabval[4]);
      /*_.DEBUGFUNBIND__V23*/ meltfptr[21] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!FIND_ENV */ meltfrout->tabval[3])),
		    (melt_ptr_t) ( /*_.ENV__V3*/ meltfptr[2]),
		    (MELTBPARSTR_PTR ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-macro.melt:6393:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & ( /*!konst_5_THE_MELTCALLCOUNT */ meltfrout->
			  tabval[5]);
      /*_.THECOUNTBIND__V24*/ meltfptr[19] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!FIND_ENV */ meltfrout->tabval[3])),
		    (melt_ptr_t) ( /*_.ENV__V3*/ meltfptr[2]),
		    (MELTBPARSTR_PTR ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-macro.melt:6394:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & ( /*!konst_6_MELT_NEED_DBG */ meltfrout->tabval[6]);
      /*_.THENEEDDBGBIND__V25*/ meltfptr[24] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!FIND_ENV */ meltfrout->tabval[3])),
		    (melt_ptr_t) ( /*_.ENV__V3*/ meltfptr[2]),
		    (MELTBPARSTR_PTR ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-macro.melt:6395:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & ( /*!konst_7_MELT_INCREMENT_DBGCOUNTER */ meltfrout->
			  tabval[7]);
      /*_.THEINCRDBGBIND__V26*/ meltfptr[25] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!FIND_ENV */ meltfrout->tabval[3])),
		    (melt_ptr_t) ( /*_.ENV__V3*/ meltfptr[2]),
		    (MELTBPARSTR_PTR ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;
 /*_.LIST_FIRST__V27*/ meltfptr[26] =
      (melt_list_first ((melt_ptr_t) ( /*_.CONT__V17*/ meltfptr[16])));;
    /*^compute */
 /*_.CURPAIR__V28*/ meltfptr[27] =
      (melt_pair_tail ((melt_ptr_t) ( /*_.LIST_FIRST__V27*/ meltfptr[26])));;
    MELT_LOCATION ("warmelt-macro.melt:6397:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[3];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.ENV__V3*/ meltfptr[2];
      /*^apply.arg */
      argtab[1].meltbp_aptr =
	(melt_ptr_t *) & /*_.MEXPANDER__V4*/ meltfptr[3];
      /*^apply.arg */
      argtab[2].meltbp_aptr = (melt_ptr_t *) & /*_.MODCTX__V5*/ meltfptr[4];
      /*_.XARGLIST__V29*/ meltfptr[28] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!EXPAND_PAIRLIST_AS_LIST */ meltfrout->tabval[8])),
		    (melt_ptr_t) ( /*_.CURPAIR__V28*/ meltfptr[27]),
		    (MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_PTR ""),
		    argtab, "", (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-macro.melt:6398:/ blockmultialloc");
    /*multiallocblock */
    {
      struct meltletrec_1_st
      {
	struct meltpair_st rpair_0__PAIROFLIST_x1;
	struct meltlist_st rlist_1__LIST_;
	long meltletrec_1_endgap;
      } *meltletrec_1_ptr = 0;
      meltletrec_1_ptr =
	(struct meltletrec_1_st *)
	meltgc_allocate (sizeof (struct meltletrec_1_st), 0);
      /*^blockmultialloc.initfill */
      /*inipair rpair_0__PAIROFLIST_x1 */
   /*_.PAIROFLIST__V31*/ meltfptr[30] =
	(melt_ptr_t) & meltletrec_1_ptr->rpair_0__PAIROFLIST_x1;
      meltletrec_1_ptr->rpair_0__PAIROFLIST_x1.discr =
	(meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (DISCR_PAIR))));

      /*inilist rlist_1__LIST_ */
   /*_.LIST___V32*/ meltfptr[31] =
	(melt_ptr_t) & meltletrec_1_ptr->rlist_1__LIST_;
      meltletrec_1_ptr->rlist_1__LIST_.discr =
	(meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (DISCR_LIST))));



      /*^putpairhead */
      /*putpairhead */
      melt_assertmsg ("putpairhead /1 checkpair",
		      melt_magic_discr ((melt_ptr_t)
					( /*_.PAIROFLIST__V31*/ meltfptr[30]))
		      == MELTOBMAG_PAIR);
      ((meltpair_ptr_t) ( /*_.PAIROFLIST__V31*/ meltfptr[30]))->hd =
	(melt_ptr_t) (( /*nil */ NULL));
      ;
      /*^touch */
      meltgc_touch ( /*_.PAIROFLIST__V31*/ meltfptr[30]);
      ;
      /*^putlist */
      /*putlist */
      melt_assertmsg ("putlist checklist",
		      melt_magic_discr ((melt_ptr_t)
					( /*_.LIST___V32*/ meltfptr[31])) ==
		      MELTOBMAG_LIST);
      ((meltlist_ptr_t) ( /*_.LIST___V32*/ meltfptr[31]))->first =
	(meltpair_ptr_t) ( /*_.PAIROFLIST__V31*/ meltfptr[30]);
      ((meltlist_ptr_t) ( /*_.LIST___V32*/ meltfptr[31]))->last =
	(meltpair_ptr_t) ( /*_.PAIROFLIST__V31*/ meltfptr[30]);
      ;
      /*^touch */
      meltgc_touch ( /*_.LIST___V32*/ meltfptr[31]);
      ;
      /*_.DBGARGLIST__V30*/ meltfptr[29] = /*_.LIST___V32*/ meltfptr[31];;
      /*epilog */

      MELT_LOCATION ("warmelt-macro.melt:6398:/ clear");
	    /*clear *//*_.PAIROFLIST__V31*/ meltfptr[30] = 0;
      /*^clear */
	    /*clear *//*_.LIST___V32*/ meltfptr[31] = 0;
      /*^clear */
	    /*clear *//*_.PAIROFLIST__V31*/ meltfptr[30] = 0;
      /*^clear */
	    /*clear *//*_.LIST___V32*/ meltfptr[31] = 0;
    }				/*end multiallocblock */
    ;
    MELT_LOCATION ("warmelt-macro.melt:6401:/ quasiblock");


    /*^checksignal */
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      /*_.SEXPW__V33*/ meltfptr[30] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!S_EXPR_WEIGHT */ meltfrout->tabval[9])),
		    (melt_ptr_t) ( /*_.SEXPR__V2*/ meltfptr[1]), (""),
		    (union meltparam_un *) 0, "", (union meltparam_un *) 0);
    }
    ;
 /*_#NBSEPW__L9*/ meltfnum[8] =
      (melt_get_int ((melt_ptr_t) ( /*_.SEXPW__V33*/ meltfptr[30])));;
    MELT_LOCATION ("warmelt-macro.melt:6404:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#I__L10*/ meltfnum[9] =
      (( /*_#NBSEPW__L9*/ meltfnum[8]) > (26));;
    MELT_LOCATION ("warmelt-macro.melt:6404:/ cond");
    /*cond */ if ( /*_#I__L10*/ meltfnum[9])	/*then */
      {
	/*^cond.then */
	/*_#OR___L11*/ meltfnum[10] = /*_#I__L10*/ meltfnum[9];;
      }
    else
      {
	MELT_LOCATION ("warmelt-macro.melt:6404:/ cond.else");

	/*^block */
	/*anyblock */
	{

   /*_#NULL__L12*/ meltfnum[11] =
	    (( /*_.SEXPW__V33*/ meltfptr[30]) == NULL);;
	  /*^compute */
	  /*_#OR___L11*/ meltfnum[10] = /*_#NULL__L12*/ meltfnum[11];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-macro.melt:6404:/ clear");
	     /*clear *//*_#NULL__L12*/ meltfnum[11] = 0;
	}
	;
      }
    ;
    /*^cond */
    /*cond */ if ( /*_#OR___L11*/ meltfnum[10])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-macro.melt:6409:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^apply */
	  /*apply */
	  {
	    union meltparam_un argtab[2];
	    memset (&argtab, 0, sizeof (argtab));
	    /*^apply.arg */
	    argtab[0].meltbp_cstring = "weight:";
	    /*^apply.arg */
	    argtab[1].meltbp_aptr =
	      (melt_ptr_t *) & /*_.SEXPW__V33*/ meltfptr[30];
	    /*_.STRING4OUT__V34*/ meltfptr[31] =
	      melt_apply ((meltclosure_ptr_t)
			  (( /*!STRING4OUT */ meltfrout->tabval[10])),
			  (melt_ptr_t) (( /*!DISCR_STRING */ meltfrout->
					 tabval[11])),
			  (MELTBPARSTR_CSTRING MELTBPARSTR_PTR ""), argtab,
			  "", (union meltparam_un *) 0);
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-macro.melt:6407:/ locexp");
	    melt_warning_str (0, (melt_ptr_t) ( /*_.LOC__V18*/ meltfptr[17]),
			      ("suspicious very heavy (DEBUG ....)"),
			      (melt_ptr_t) ( /*_.STRING4OUT__V34*/
					    meltfptr[31]));
	  }
	  ;
	  /*epilog */

	  MELT_LOCATION ("warmelt-macro.melt:6404:/ clear");
	     /*clear *//*_.STRING4OUT__V34*/ meltfptr[31] = 0;
	}
	;
      }				/*noelse */
    ;

    MELT_LOCATION ("warmelt-macro.melt:6401:/ clear");
	   /*clear *//*_.SEXPW__V33*/ meltfptr[30] = 0;
    /*^clear */
	   /*clear *//*_#NBSEPW__L9*/ meltfnum[8] = 0;
    /*^clear */
	   /*clear *//*_#I__L10*/ meltfnum[9] = 0;
    /*^clear */
	   /*clear *//*_#OR___L11*/ meltfnum[10] = 0;
    MELT_LOCATION ("warmelt-macro.melt:6411:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#IS_STRING__L13*/ meltfnum[11] =
      (melt_magic_discr ((melt_ptr_t) ( /*_.DFILNAM__V19*/ meltfptr[18])) ==
       MELTOBMAG_STRING);;
    /*^compute */
 /*_#NOT__L14*/ meltfnum[8] =
      (!( /*_#IS_STRING__L13*/ meltfnum[11]));;
    MELT_LOCATION ("warmelt-macro.melt:6411:/ cond");
    /*cond */ if ( /*_#NOT__L14*/ meltfnum[8])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{


	  {
	    MELT_LOCATION ("warmelt-macro.melt:6413:/ locexp");
	    /* error_plain */
	      melt_error_str ((melt_ptr_t) ( /*_.LOC__V18*/ meltfptr[17]),
			      ("(DEBUG ...) used without file location"),
			      (melt_ptr_t) 0);
	  }
	  ;
	  MELT_LOCATION ("warmelt-macro.melt:6414:/ quasiblock");


   /*_.RETVAL___V1*/ meltfptr[0] = NULL;;

	  {
	    MELT_LOCATION ("warmelt-macro.melt:6414:/ locexp");
	    /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
	    if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	      melt_warn_for_no_expected_secondary_results ();
	    /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
	    ;
	  }
	  ;
	  /*^finalreturn */
	  ;
	  /*finalret */ goto labend_rout;
	  MELT_LOCATION ("warmelt-macro.melt:6412:/ quasiblock");


	  /*_.PROGN___V37*/ meltfptr[36] = /*_.RETURN___V36*/ meltfptr[30];;
	  /*^compute */
	  /*_.IF___V35*/ meltfptr[31] = /*_.PROGN___V37*/ meltfptr[36];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-macro.melt:6411:/ clear");
	     /*clear *//*_.RETURN___V36*/ meltfptr[30] = 0;
	  /*^clear */
	     /*clear *//*_.PROGN___V37*/ meltfptr[36] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

  /*_.IF___V35*/ meltfptr[31] = NULL;;
      }
    ;
    MELT_LOCATION ("warmelt-macro.melt:6415:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#IS_A__L15*/ meltfnum[9] =
      melt_is_instance_of ((melt_ptr_t)
			   ( /*_.DEBUGFUNBIND__V23*/ meltfptr[21]),
			   (melt_ptr_t) (( /*!CLASS_FUNCTION_BINDING */
					  meltfrout->tabval[13])));;
    MELT_LOCATION ("warmelt-macro.melt:6415:/ cond");
    /*cond */ if ( /*_#IS_A__L15*/ meltfnum[9])	/*then */
      {
	/*^cond.then */
	/*_#OR___L16*/ meltfnum[10] = /*_#IS_A__L15*/ meltfnum[9];;
      }
    else
      {
	MELT_LOCATION ("warmelt-macro.melt:6415:/ cond.else");

	/*^block */
	/*anyblock */
	{

   /*_#IS_A__L17*/ meltfnum[16] =
	    melt_is_instance_of ((melt_ptr_t)
				 ( /*_.DEBUGFUNBIND__V23*/ meltfptr[21]),
				 (melt_ptr_t) (( /*!CLASS_VALUE_BINDING */
						meltfrout->tabval[12])));;
	  /*^compute */
	  /*_#OR___L16*/ meltfnum[10] = /*_#IS_A__L17*/ meltfnum[16];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-macro.melt:6415:/ clear");
	     /*clear *//*_#IS_A__L17*/ meltfnum[16] = 0;
	}
	;
      }
    ;
 /*_#NOT__L18*/ meltfnum[16] =
      (!( /*_#OR___L16*/ meltfnum[10]));;
    MELT_LOCATION ("warmelt-macro.melt:6415:/ cond");
    /*cond */ if ( /*_#NOT__L18*/ meltfnum[16])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{


	  {
	    MELT_LOCATION ("warmelt-macro.melt:6418:/ locexp");
	    /* error_plain */
	      melt_error_str ((melt_ptr_t) ( /*_.LOC__V18*/ meltfptr[17]),
			      ("(DEBUG ...) used in context with bad MELT_DEBUG_FUN"),
			      (melt_ptr_t) 0);
	  }
	  ;
	  MELT_LOCATION ("warmelt-macro.melt:6419:/ quasiblock");


   /*_.RETVAL___V1*/ meltfptr[0] = NULL;;

	  {
	    MELT_LOCATION ("warmelt-macro.melt:6419:/ locexp");
	    /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
	    if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	      melt_warn_for_no_expected_secondary_results ();
	    /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
	    ;
	  }
	  ;
	  /*^finalreturn */
	  ;
	  /*finalret */ goto labend_rout;
	  MELT_LOCATION ("warmelt-macro.melt:6417:/ quasiblock");


	  /*_.PROGN___V40*/ meltfptr[39] = /*_.RETURN___V39*/ meltfptr[36];;
	  /*^compute */
	  /*_.IF___V38*/ meltfptr[30] = /*_.PROGN___V40*/ meltfptr[39];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-macro.melt:6415:/ clear");
	     /*clear *//*_.RETURN___V39*/ meltfptr[36] = 0;
	  /*^clear */
	     /*clear *//*_.PROGN___V40*/ meltfptr[39] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

  /*_.IF___V38*/ meltfptr[30] = NULL;;
      }
    ;
    MELT_LOCATION ("warmelt-macro.melt:6420:/ quasiblock");


    MELT_LOCATION ("warmelt-macro.melt:6423:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#IS_A__L19*/ meltfnum[18] =
      melt_is_instance_of ((melt_ptr_t)
			   ( /*_.THECOUNTBIND__V24*/ meltfptr[19]),
			   (melt_ptr_t) (( /*!CLASS_PRIMITIVE_BINDING */
					  meltfrout->tabval[14])));;
    MELT_LOCATION ("warmelt-macro.melt:6423:/ cond");
    /*cond */ if ( /*_#IS_A__L19*/ meltfnum[18])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-macro.melt:6424:/ cond");
	  /*cond */ if (
			 /*ifisa */
			 melt_is_instance_of ((melt_ptr_t)
					      ( /*_.THECOUNTBIND__V24*/
					       meltfptr[19]),
					      (melt_ptr_t) (( /*!CLASS_PRIMITIVE_BINDING */ meltfrout->tabval[14])))
	    )			/*then */
	    {
	      /*^cond.then */
	      /*^getslot */
	      {
		melt_ptr_t slot = NULL, obj = NULL;
		obj =
		  (melt_ptr_t) ( /*_.THECOUNTBIND__V24*/ meltfptr[19])
		  /*=obj*/ ;
		melt_object_get_field (slot, obj, 3, "PBIND_PRIMITIVE");
     /*_.PBIND_PRIMITIVE__V42*/ meltfptr[39] = slot;
	      };
	      ;
	    }
	  else
	    {			/*^cond.else */

    /*_.PBIND_PRIMITIVE__V42*/ meltfptr[39] = NULL;;
	    }
	  ;
	  /*^compute */
	  /*_.THECOUNTPRIM__V41*/ meltfptr[36] =
	    /*_.PBIND_PRIMITIVE__V42*/ meltfptr[39];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-macro.melt:6423:/ clear");
	     /*clear *//*_.PBIND_PRIMITIVE__V42*/ meltfptr[39] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-macro.melt:6425:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
   /*_#IS_A__L20*/ meltfnum[19] =
	    melt_is_instance_of ((melt_ptr_t)
				 ( /*_.THECOUNTBIND__V24*/ meltfptr[19]),
				 (melt_ptr_t) (( /*!CLASS_VALUE_BINDING */
						meltfrout->tabval[12])));;
	  MELT_LOCATION ("warmelt-macro.melt:6425:/ cond");
	  /*cond */ if ( /*_#IS_A__L20*/ meltfnum[19])	/*then */
	    {
	      /*^cond.then */
	      /*^block */
	      /*anyblock */
	      {

		MELT_LOCATION ("warmelt-macro.melt:6426:/ cond");
		/*cond */ if (
			       /*ifisa */
			       melt_is_instance_of ((melt_ptr_t)
						    ( /*_.THECOUNTBIND__V24*/
						     meltfptr[19]),
						    (melt_ptr_t) (( /*!CLASS_VALUE_BINDING */ meltfrout->tabval[12])))
		  )		/*then */
		  {
		    /*^cond.then */
		    /*^getslot */
		    {
		      melt_ptr_t slot = NULL, obj = NULL;
		      obj =
			(melt_ptr_t) ( /*_.THECOUNTBIND__V24*/ meltfptr[19])
			/*=obj*/ ;
		      melt_object_get_field (slot, obj, 1, "VBIND_VALUE");
       /*_.VBIND_VALUE__V44*/ meltfptr[43] = slot;
		    };
		    ;
		  }
		else
		  {		/*^cond.else */

      /*_.VBIND_VALUE__V44*/ meltfptr[43] = NULL;;
		  }
		;
		/*^compute */
		/*_.IFELSE___V43*/ meltfptr[39] =
		  /*_.VBIND_VALUE__V44*/ meltfptr[43];;
		/*epilog */

		MELT_LOCATION ("warmelt-macro.melt:6425:/ clear");
	       /*clear *//*_.VBIND_VALUE__V44*/ meltfptr[43] = 0;
	      }
	      ;
	    }
	  else
	    {			/*^cond.else */

	      /*^block */
	      /*anyblock */
	      {


		{
		  MELT_LOCATION ("warmelt-macro.melt:6428:/ locexp");
		  /* error_plain */
		    melt_error_str ((melt_ptr_t)
				    ( /*_.LOC__V18*/ meltfptr[17]),
				    ("(DEBUG ...) used in context with bad THE_MELTCALLCOUNT"),
				    (melt_ptr_t) 0);
		}
		;
		MELT_LOCATION ("warmelt-macro.melt:6429:/ quasiblock");


     /*_.RETVAL___V1*/ meltfptr[0] = NULL;;

		{
		  MELT_LOCATION ("warmelt-macro.melt:6429:/ locexp");
		  /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
		  if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
		    melt_warn_for_no_expected_secondary_results ();
		  /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
		  ;
		}
		;
		/*^finalreturn */
		;
		/*finalret */ goto labend_rout;
		MELT_LOCATION ("warmelt-macro.melt:6427:/ quasiblock");


		/*_.PROGN___V46*/ meltfptr[45] =
		  /*_.RETURN___V45*/ meltfptr[43];;
		/*^compute */
		/*_.IFELSE___V43*/ meltfptr[39] =
		  /*_.PROGN___V46*/ meltfptr[45];;
		/*epilog */

		MELT_LOCATION ("warmelt-macro.melt:6425:/ clear");
	       /*clear *//*_.RETURN___V45*/ meltfptr[43] = 0;
		/*^clear */
	       /*clear *//*_.PROGN___V46*/ meltfptr[45] = 0;
	      }
	      ;
	    }
	  ;
	  /*_.THECOUNTPRIM__V41*/ meltfptr[36] =
	    /*_.IFELSE___V43*/ meltfptr[39];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-macro.melt:6423:/ clear");
	     /*clear *//*_#IS_A__L20*/ meltfnum[19] = 0;
	  /*^clear */
	     /*clear *//*_.IFELSE___V43*/ meltfptr[39] = 0;
	}
	;
      }
    ;
    MELT_LOCATION ("warmelt-macro.melt:6430:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    MELT_LOCATION ("warmelt-macro.melt:6433:/ blockmultialloc");
    /*multiallocblock */
    {
      struct meltletrec_2_st
      {
	struct MELT_MULTIPLE_STRUCT (0) rtup_0__TUPLREC__x11;
	long meltletrec_2_endgap;
      } *meltletrec_2_ptr = 0;
      meltletrec_2_ptr =
	(struct meltletrec_2_st *)
	meltgc_allocate (sizeof (struct meltletrec_2_st), 0);
      /*^blockmultialloc.initfill */
      /*inimult rtup_0__TUPLREC__x11 */
 /*_.TUPLREC___V48*/ meltfptr[45] =
	(melt_ptr_t) & meltletrec_2_ptr->rtup_0__TUPLREC__x11;
      meltletrec_2_ptr->rtup_0__TUPLREC__x11.discr =
	(meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (DISCR_MULTIPLE))));
      meltletrec_2_ptr->rtup_0__TUPLREC__x11.nbval = 0;


      /*^touch */
      meltgc_touch ( /*_.TUPLREC___V48*/ meltfptr[45]);
      ;
      /*_.TUPLE___V47*/ meltfptr[43] = /*_.TUPLREC___V48*/ meltfptr[45];;
      /*epilog */

      MELT_LOCATION ("warmelt-macro.melt:6433:/ clear");
	    /*clear *//*_.TUPLREC___V48*/ meltfptr[45] = 0;
      /*^clear */
	    /*clear *//*_.TUPLREC___V48*/ meltfptr[45] = 0;
    }				/*end multiallocblock */
    ;
    MELT_LOCATION ("warmelt-macro.melt:6430:/ quasiblock");


    /*^rawallocobj */
    /*rawallocobj */
    {
      melt_ptr_t newobj = 0;
      melt_raw_object_create (newobj,
			      (melt_ptr_t) (( /*!CLASS_SOURCE_PRIMITIVE */
					     meltfrout->tabval[15])), (4),
			      "CLASS_SOURCE_PRIMITIVE");
  /*_.INST__V50*/ meltfptr[45] =
	newobj;
    };
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @LOCA_LOCATION",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V50*/ meltfptr[45])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V50*/ meltfptr[45]), (1),
			  ( /*_.LOC__V18*/ meltfptr[17]), "LOCA_LOCATION");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @SPRIM_OPER",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V50*/ meltfptr[45])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V50*/ meltfptr[45]), (3),
			  ( /*_.THECOUNTPRIM__V41*/ meltfptr[36]),
			  "SPRIM_OPER");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @SARGOP_ARGS",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V50*/ meltfptr[45])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V50*/ meltfptr[45]), (2),
			  ( /*_.TUPLE___V47*/ meltfptr[43]), "SARGOP_ARGS");
    ;
    /*^touchobj */

    melt_dbgtrace_written_object ( /*_.INST__V50*/ meltfptr[45],
				  "newly made instance");
    ;
    /*_.DCOUNT__V49*/ meltfptr[39] = /*_.INST__V50*/ meltfptr[45];;

    {
      MELT_LOCATION ("warmelt-macro.melt:6435:/ locexp");
      meltgc_append_list ((melt_ptr_t) ( /*_.DBGARGLIST__V30*/ meltfptr[29]),
			  (melt_ptr_t) ( /*_.DCOUNT__V49*/ meltfptr[39]));
    }
    ;

    MELT_LOCATION ("warmelt-macro.melt:6420:/ clear");
	   /*clear *//*_#IS_A__L19*/ meltfnum[18] = 0;
    /*^clear */
	   /*clear *//*_.THECOUNTPRIM__V41*/ meltfptr[36] = 0;
    /*^clear */
	   /*clear *//*_.TUPLE___V47*/ meltfptr[43] = 0;
    /*^clear */
	   /*clear *//*_.DCOUNT__V49*/ meltfptr[39] = 0;

    {
      MELT_LOCATION ("warmelt-macro.melt:6437:/ locexp");
      meltgc_append_list ((melt_ptr_t) ( /*_.DBGARGLIST__V30*/ meltfptr[29]),
			  (melt_ptr_t) ( /*_.DFILNAM__V19*/ meltfptr[18]));
    }
    ;
 /*_.MAKE_INTEGERBOX__V51*/ meltfptr[36] =
      (meltgc_new_int
       ((meltobject_ptr_t)
	(( /*!DISCR_CONSTANT_INTEGER */ meltfrout->tabval[16])),
	( /*_#DLINE__L8*/ meltfnum[1])));;

    {
      MELT_LOCATION ("warmelt-macro.melt:6438:/ locexp");
      meltgc_append_list ((melt_ptr_t) ( /*_.DBGARGLIST__V30*/ meltfptr[29]),
			  (melt_ptr_t) ( /*_.MAKE_INTEGERBOX__V51*/
					meltfptr[36]));
    }
    ;
    MELT_LOCATION ("warmelt-macro.melt:6439:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & /*_.XARGLIST__V29*/ meltfptr[28];
      /*_.LIST_APPEND2LIST__V52*/ meltfptr[43] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!LIST_APPEND2LIST */ meltfrout->tabval[17])),
		    (melt_ptr_t) ( /*_.DBGARGLIST__V30*/ meltfptr[29]),
		    (MELTBPARSTR_PTR ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-macro.melt:6440:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L21*/ meltfnum[19] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-macro.melt:6440:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L21*/ meltfnum[19])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L22*/ meltfnum[18] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-macro.melt:6440:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long =
		/*_#THE_MELTCALLCOUNT__L22*/ meltfnum[18];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-macro.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 6440;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "mexpand_debug dbgarglist";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.DBGARGLIST__V30*/ meltfptr[29];
	      /*_.MELT_DEBUG_FUN__V55*/ meltfptr[54] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V54*/ meltfptr[53] =
	      /*_.MELT_DEBUG_FUN__V55*/ meltfptr[54];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-macro.melt:6440:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L22*/ meltfnum[18] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V55*/ meltfptr[54] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V54*/ meltfptr[53] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-macro.melt:6440:/ quasiblock");


      /*_.PROGN___V56*/ meltfptr[54] = /*_.IF___V54*/ meltfptr[53];;
      /*^compute */
      /*_.IFCPP___V53*/ meltfptr[39] = /*_.PROGN___V56*/ meltfptr[54];;
      /*epilog */

      MELT_LOCATION ("warmelt-macro.melt:6440:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L21*/ meltfnum[19] = 0;
      /*^clear */
	     /*clear *//*_.IF___V54*/ meltfptr[53] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V56*/ meltfptr[54] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V53*/ meltfptr[39] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-macro.melt:6441:/ quasiblock");


 /*_#NBDBGARG__L23*/ meltfnum[18] =
      (melt_list_length
       ((melt_ptr_t) ( /*_.DBGARGLIST__V30*/ meltfptr[29])));;
    MELT_LOCATION ("warmelt-macro.melt:6445:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#IS_A__L24*/ meltfnum[19] =
      melt_is_instance_of ((melt_ptr_t)
			   ( /*_.THENEEDDBGBIND__V25*/ meltfptr[24]),
			   (melt_ptr_t) (( /*!CLASS_PRIMITIVE_BINDING */
					  meltfrout->tabval[14])));;
    MELT_LOCATION ("warmelt-macro.melt:6445:/ cond");
    /*cond */ if ( /*_#IS_A__L24*/ meltfnum[19])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-macro.melt:6446:/ cond");
	  /*cond */ if (
			 /*ifisa */
			 melt_is_instance_of ((melt_ptr_t)
					      ( /*_.THENEEDDBGBIND__V25*/
					       meltfptr[24]),
					      (melt_ptr_t) (( /*!CLASS_PRIMITIVE_BINDING */ meltfrout->tabval[14])))
	    )			/*then */
	    {
	      /*^cond.then */
	      /*^getslot */
	      {
		melt_ptr_t slot = NULL, obj = NULL;
		obj =
		  (melt_ptr_t) ( /*_.THENEEDDBGBIND__V25*/ meltfptr[24])
		  /*=obj*/ ;
		melt_object_get_field (slot, obj, 3, "PBIND_PRIMITIVE");
     /*_.PBIND_PRIMITIVE__V59*/ meltfptr[58] = slot;
	      };
	      ;
	    }
	  else
	    {			/*^cond.else */

    /*_.PBIND_PRIMITIVE__V59*/ meltfptr[58] = NULL;;
	    }
	  ;
	  /*^compute */
	  /*_.THENEEDDBG__V58*/ meltfptr[54] =
	    /*_.PBIND_PRIMITIVE__V59*/ meltfptr[58];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-macro.melt:6445:/ clear");
	     /*clear *//*_.PBIND_PRIMITIVE__V59*/ meltfptr[58] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-macro.melt:6447:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
   /*_#IS_A__L25*/ meltfnum[24] =
	    melt_is_instance_of ((melt_ptr_t)
				 ( /*_.THENEEDDBGBIND__V25*/ meltfptr[24]),
				 (melt_ptr_t) (( /*!CLASS_VALUE_BINDING */
						meltfrout->tabval[12])));;
	  MELT_LOCATION ("warmelt-macro.melt:6447:/ cond");
	  /*cond */ if ( /*_#IS_A__L25*/ meltfnum[24])	/*then */
	    {
	      /*^cond.then */
	      /*^block */
	      /*anyblock */
	      {

		MELT_LOCATION ("warmelt-macro.melt:6448:/ cond");
		/*cond */ if (
			       /*ifisa */
			       melt_is_instance_of ((melt_ptr_t)
						    ( /*_.THENEEDDBGBIND__V25*/ meltfptr[24]),
						    (melt_ptr_t) (( /*!CLASS_VALUE_BINDING */ meltfrout->tabval[12])))
		  )		/*then */
		  {
		    /*^cond.then */
		    /*^getslot */
		    {
		      melt_ptr_t slot = NULL, obj = NULL;
		      obj =
			(melt_ptr_t) ( /*_.THENEEDDBGBIND__V25*/ meltfptr[24])
			/*=obj*/ ;
		      melt_object_get_field (slot, obj, 1, "VBIND_VALUE");
       /*_.VBIND_VALUE__V61*/ meltfptr[60] = slot;
		    };
		    ;
		  }
		else
		  {		/*^cond.else */

      /*_.VBIND_VALUE__V61*/ meltfptr[60] = NULL;;
		  }
		;
		/*^compute */
		/*_.IFELSE___V60*/ meltfptr[58] =
		  /*_.VBIND_VALUE__V61*/ meltfptr[60];;
		/*epilog */

		MELT_LOCATION ("warmelt-macro.melt:6447:/ clear");
	       /*clear *//*_.VBIND_VALUE__V61*/ meltfptr[60] = 0;
	      }
	      ;
	    }
	  else
	    {			/*^cond.else */

	      /*^block */
	      /*anyblock */
	      {


		{
		  MELT_LOCATION ("warmelt-macro.melt:6450:/ locexp");
		  /* error_plain */
		    melt_error_str ((melt_ptr_t)
				    ( /*_.LOC__V18*/ meltfptr[17]),
				    ("(DEBUG ...) used in context with bad MELT_NEED_DBG"),
				    (melt_ptr_t) 0);
		}
		;
		MELT_LOCATION ("warmelt-macro.melt:6451:/ quasiblock");


     /*_.RETVAL___V1*/ meltfptr[0] = NULL;;

		{
		  MELT_LOCATION ("warmelt-macro.melt:6451:/ locexp");
		  /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
		  if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
		    melt_warn_for_no_expected_secondary_results ();
		  /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
		  ;
		}
		;
		/*^finalreturn */
		;
		/*finalret */ goto labend_rout;
		MELT_LOCATION ("warmelt-macro.melt:6449:/ quasiblock");


		/*_.PROGN___V63*/ meltfptr[62] =
		  /*_.RETURN___V62*/ meltfptr[60];;
		/*^compute */
		/*_.IFELSE___V60*/ meltfptr[58] =
		  /*_.PROGN___V63*/ meltfptr[62];;
		/*epilog */

		MELT_LOCATION ("warmelt-macro.melt:6447:/ clear");
	       /*clear *//*_.RETURN___V62*/ meltfptr[60] = 0;
		/*^clear */
	       /*clear *//*_.PROGN___V63*/ meltfptr[62] = 0;
	      }
	      ;
	    }
	  ;
	  /*_.THENEEDDBG__V58*/ meltfptr[54] =
	    /*_.IFELSE___V60*/ meltfptr[58];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-macro.melt:6445:/ clear");
	     /*clear *//*_#IS_A__L25*/ meltfnum[24] = 0;
	  /*^clear */
	     /*clear *//*_.IFELSE___V60*/ meltfptr[58] = 0;
	}
	;
      }
    ;
    MELT_LOCATION ("warmelt-macro.melt:6452:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    MELT_LOCATION ("warmelt-macro.melt:6455:/ blockmultialloc");
    /*multiallocblock */
    {
      struct meltletrec_3_st
      {
	struct MELT_MULTIPLE_STRUCT (1) rtup_0__TUPLREC__x12;
	long meltletrec_3_endgap;
      } *meltletrec_3_ptr = 0;
      meltletrec_3_ptr =
	(struct meltletrec_3_st *)
	meltgc_allocate (sizeof (struct meltletrec_3_st), 0);
      /*^blockmultialloc.initfill */
      /*inimult rtup_0__TUPLREC__x12 */
 /*_.TUPLREC___V65*/ meltfptr[62] =
	(melt_ptr_t) & meltletrec_3_ptr->rtup_0__TUPLREC__x12;
      meltletrec_3_ptr->rtup_0__TUPLREC__x12.discr =
	(meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (DISCR_MULTIPLE))));
      meltletrec_3_ptr->rtup_0__TUPLREC__x12.nbval = 1;


      /*^putuple */
      /*putupl#20 */
      melt_assertmsg ("putupl [:6455] #20 checktup",
		      melt_magic_discr ((melt_ptr_t)
					( /*_.TUPLREC___V65*/ meltfptr[62]))
		      == MELTOBMAG_MULTIPLE);
      melt_assertmsg ("putupl [:6455] #20 checkoff",
		      (0 >= 0
		       && 0 <
		       melt_multiple_length ((melt_ptr_t)
					     ( /*_.TUPLREC___V65*/
					      meltfptr[62]))));
      ((meltmultiple_ptr_t) ( /*_.TUPLREC___V65*/ meltfptr[62]))->tabval[0] =
	(melt_ptr_t) (( /*!konst_18 */ meltfrout->tabval[18]));
      ;
      /*^touch */
      meltgc_touch ( /*_.TUPLREC___V65*/ meltfptr[62]);
      ;
      /*_.TUPLE___V64*/ meltfptr[60] = /*_.TUPLREC___V65*/ meltfptr[62];;
      /*epilog */

      MELT_LOCATION ("warmelt-macro.melt:6455:/ clear");
	    /*clear *//*_.TUPLREC___V65*/ meltfptr[62] = 0;
      /*^clear */
	    /*clear *//*_.TUPLREC___V65*/ meltfptr[62] = 0;
    }				/*end multiallocblock */
    ;
    MELT_LOCATION ("warmelt-macro.melt:6452:/ quasiblock");


    /*^rawallocobj */
    /*rawallocobj */
    {
      melt_ptr_t newobj = 0;
      melt_raw_object_create (newobj,
			      (melt_ptr_t) (( /*!CLASS_SOURCE_PRIMITIVE */
					     meltfrout->tabval[15])), (4),
			      "CLASS_SOURCE_PRIMITIVE");
  /*_.INST__V67*/ meltfptr[62] =
	newobj;
    };
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @LOCA_LOCATION",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V67*/ meltfptr[62])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V67*/ meltfptr[62]), (1),
			  ( /*_.LOC__V18*/ meltfptr[17]), "LOCA_LOCATION");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @SPRIM_OPER",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V67*/ meltfptr[62])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V67*/ meltfptr[62]), (3),
			  ( /*_.THENEEDDBG__V58*/ meltfptr[54]),
			  "SPRIM_OPER");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @SARGOP_ARGS",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V67*/ meltfptr[62])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V67*/ meltfptr[62]), (2),
			  ( /*_.TUPLE___V64*/ meltfptr[60]), "SARGOP_ARGS");
    ;
    /*^touchobj */

    melt_dbgtrace_written_object ( /*_.INST__V67*/ meltfptr[62],
				  "newly made instance");
    ;
    /*_.DNEEDBG__V66*/ meltfptr[58] = /*_.INST__V67*/ meltfptr[62];;
    MELT_LOCATION ("warmelt-macro.melt:6456:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    MELT_LOCATION ("warmelt-macro.melt:6460:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & ( /*!DISCR_MULTIPLE */ meltfrout->tabval[22]);
      /*_.LIST_TO_MULTIPLE__V68*/ meltfptr[67] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!LIST_TO_MULTIPLE */ meltfrout->tabval[21])),
		    (melt_ptr_t) ( /*_.DBGARGLIST__V30*/ meltfptr[29]),
		    (MELTBPARSTR_PTR ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-macro.melt:6456:/ quasiblock");


    /*^rawallocobj */
    /*rawallocobj */
    {
      melt_ptr_t newobj = 0;
      melt_raw_object_create (newobj,
			      (melt_ptr_t) (( /*!CLASS_SOURCE_APPLY */
					     meltfrout->tabval[19])), (4),
			      "CLASS_SOURCE_APPLY");
  /*_.INST__V70*/ meltfptr[69] =
	newobj;
    };
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @LOCA_LOCATION",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V70*/ meltfptr[69])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V70*/ meltfptr[69]), (1),
			  ( /*_.LOC__V18*/ meltfptr[17]), "LOCA_LOCATION");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @SAPP_FUN",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V70*/ meltfptr[69])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V70*/ meltfptr[69]), (3),
			  (( /*!konst_4_MELT_DEBUG_FUN */ meltfrout->
			    tabval[4])), "SAPP_FUN");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @SARGOP_ARGS",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V70*/ meltfptr[69])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V70*/ meltfptr[69]), (2),
			  ( /*_.LIST_TO_MULTIPLE__V68*/ meltfptr[67]),
			  "SARGOP_ARGS");
    ;
    /*^touchobj */

    melt_dbgtrace_written_object ( /*_.INST__V70*/ meltfptr[69],
				  "newly made instance");
    ;
    /*_.DCALL__V69*/ meltfptr[68] = /*_.INST__V70*/ meltfptr[69];;
    MELT_LOCATION ("warmelt-macro.melt:6463:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#IS_A__L26*/ meltfnum[24] =
      melt_is_instance_of ((melt_ptr_t)
			   ( /*_.THEINCRDBGBIND__V26*/ meltfptr[25]),
			   (melt_ptr_t) (( /*!CLASS_PRIMITIVE_BINDING */
					  meltfrout->tabval[14])));;
    MELT_LOCATION ("warmelt-macro.melt:6463:/ cond");
    /*cond */ if ( /*_#IS_A__L26*/ meltfnum[24])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-macro.melt:6464:/ cond");
	  /*cond */ if (
			 /*ifisa */
			 melt_is_instance_of ((melt_ptr_t)
					      ( /*_.THEINCRDBGBIND__V26*/
					       meltfptr[25]),
					      (melt_ptr_t) (( /*!CLASS_PRIMITIVE_BINDING */ meltfrout->tabval[14])))
	    )			/*then */
	    {
	      /*^cond.then */
	      /*^getslot */
	      {
		melt_ptr_t slot = NULL, obj = NULL;
		obj =
		  (melt_ptr_t) ( /*_.THEINCRDBGBIND__V26*/ meltfptr[25])
		  /*=obj*/ ;
		melt_object_get_field (slot, obj, 3, "PBIND_PRIMITIVE");
     /*_.PBIND_PRIMITIVE__V72*/ meltfptr[71] = slot;
	      };
	      ;
	    }
	  else
	    {			/*^cond.else */

    /*_.PBIND_PRIMITIVE__V72*/ meltfptr[71] = NULL;;
	    }
	  ;
	  /*^compute */
	  /*_.THEINCRDBG__V71*/ meltfptr[70] =
	    /*_.PBIND_PRIMITIVE__V72*/ meltfptr[71];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-macro.melt:6463:/ clear");
	     /*clear *//*_.PBIND_PRIMITIVE__V72*/ meltfptr[71] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-macro.melt:6465:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
   /*_#IS_A__L27*/ meltfnum[26] =
	    melt_is_instance_of ((melt_ptr_t)
				 ( /*_.THEINCRDBGBIND__V26*/ meltfptr[25]),
				 (melt_ptr_t) (( /*!CLASS_VALUE_BINDING */
						meltfrout->tabval[12])));;
	  MELT_LOCATION ("warmelt-macro.melt:6465:/ cond");
	  /*cond */ if ( /*_#IS_A__L27*/ meltfnum[26])	/*then */
	    {
	      /*^cond.then */
	      /*^block */
	      /*anyblock */
	      {

		MELT_LOCATION ("warmelt-macro.melt:6466:/ cond");
		/*cond */ if (
			       /*ifisa */
			       melt_is_instance_of ((melt_ptr_t)
						    ( /*_.THEINCRDBGBIND__V26*/ meltfptr[25]),
						    (melt_ptr_t) (( /*!CLASS_VALUE_BINDING */ meltfrout->tabval[12])))
		  )		/*then */
		  {
		    /*^cond.then */
		    /*^getslot */
		    {
		      melt_ptr_t slot = NULL, obj = NULL;
		      obj =
			(melt_ptr_t) ( /*_.THEINCRDBGBIND__V26*/ meltfptr[25])
			/*=obj*/ ;
		      melt_object_get_field (slot, obj, 1, "VBIND_VALUE");
       /*_.VBIND_VALUE__V74*/ meltfptr[73] = slot;
		    };
		    ;
		  }
		else
		  {		/*^cond.else */

      /*_.VBIND_VALUE__V74*/ meltfptr[73] = NULL;;
		  }
		;
		/*^compute */
		/*_.IFELSE___V73*/ meltfptr[71] =
		  /*_.VBIND_VALUE__V74*/ meltfptr[73];;
		/*epilog */

		MELT_LOCATION ("warmelt-macro.melt:6465:/ clear");
	       /*clear *//*_.VBIND_VALUE__V74*/ meltfptr[73] = 0;
	      }
	      ;
	    }
	  else
	    {			/*^cond.else */

	      /*^block */
	      /*anyblock */
	      {


		{
		  MELT_LOCATION ("warmelt-macro.melt:6468:/ locexp");
		  /* error_plain */
		    melt_error_str ((melt_ptr_t)
				    ( /*_.LOC__V18*/ meltfptr[17]),
				    ("(DEBUG ...) used in context with bad MELT_INCREMENT_DBGCOUNTER"),
				    (melt_ptr_t) 0);
		}
		;
		MELT_LOCATION ("warmelt-macro.melt:6471:/ quasiblock");


     /*_.RETVAL___V1*/ meltfptr[0] = NULL;;

		{
		  MELT_LOCATION ("warmelt-macro.melt:6471:/ locexp");
		  /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
		  if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
		    melt_warn_for_no_expected_secondary_results ();
		  /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
		  ;
		}
		;
		/*^finalreturn */
		;
		/*finalret */ goto labend_rout;
		MELT_LOCATION ("warmelt-macro.melt:6467:/ quasiblock");


		/*_.PROGN___V76*/ meltfptr[75] =
		  /*_.RETURN___V75*/ meltfptr[73];;
		/*^compute */
		/*_.IFELSE___V73*/ meltfptr[71] =
		  /*_.PROGN___V76*/ meltfptr[75];;
		/*epilog */

		MELT_LOCATION ("warmelt-macro.melt:6465:/ clear");
	       /*clear *//*_.RETURN___V75*/ meltfptr[73] = 0;
		/*^clear */
	       /*clear *//*_.PROGN___V76*/ meltfptr[75] = 0;
	      }
	      ;
	    }
	  ;
	  /*_.THEINCRDBG__V71*/ meltfptr[70] =
	    /*_.IFELSE___V73*/ meltfptr[71];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-macro.melt:6463:/ clear");
	     /*clear *//*_#IS_A__L27*/ meltfnum[26] = 0;
	  /*^clear */
	     /*clear *//*_.IFELSE___V73*/ meltfptr[71] = 0;
	}
	;
      }
    ;
    MELT_LOCATION ("warmelt-macro.melt:6472:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    MELT_LOCATION ("warmelt-macro.melt:6476:/ blockmultialloc");
    /*multiallocblock */
    {
      struct meltletrec_4_st
      {
	struct MELT_MULTIPLE_STRUCT (0) rtup_0__TUPLREC__x13;
	long meltletrec_4_endgap;
      } *meltletrec_4_ptr = 0;
      meltletrec_4_ptr =
	(struct meltletrec_4_st *)
	meltgc_allocate (sizeof (struct meltletrec_4_st), 0);
      /*^blockmultialloc.initfill */
      /*inimult rtup_0__TUPLREC__x13 */
 /*_.TUPLREC___V78*/ meltfptr[75] =
	(melt_ptr_t) & meltletrec_4_ptr->rtup_0__TUPLREC__x13;
      meltletrec_4_ptr->rtup_0__TUPLREC__x13.discr =
	(meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (DISCR_MULTIPLE))));
      meltletrec_4_ptr->rtup_0__TUPLREC__x13.nbval = 0;


      /*^touch */
      meltgc_touch ( /*_.TUPLREC___V78*/ meltfptr[75]);
      ;
      /*_.TUPLE___V77*/ meltfptr[73] = /*_.TUPLREC___V78*/ meltfptr[75];;
      /*epilog */

      MELT_LOCATION ("warmelt-macro.melt:6476:/ clear");
	    /*clear *//*_.TUPLREC___V78*/ meltfptr[75] = 0;
      /*^clear */
	    /*clear *//*_.TUPLREC___V78*/ meltfptr[75] = 0;
    }				/*end multiallocblock */
    ;
    MELT_LOCATION ("warmelt-macro.melt:6472:/ quasiblock");


    /*^rawallocobj */
    /*rawallocobj */
    {
      melt_ptr_t newobj = 0;
      melt_raw_object_create (newobj,
			      (melt_ptr_t) (( /*!CLASS_SOURCE_PRIMITIVE */
					     meltfrout->tabval[15])), (4),
			      "CLASS_SOURCE_PRIMITIVE");
  /*_.INST__V80*/ meltfptr[75] =
	newobj;
    };
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @LOCA_LOCATION",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V80*/ meltfptr[75])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V80*/ meltfptr[75]), (1),
			  ( /*_.LOC__V18*/ meltfptr[17]), "LOCA_LOCATION");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @SPRIM_OPER",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V80*/ meltfptr[75])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V80*/ meltfptr[75]), (3),
			  ( /*_.THEINCRDBG__V71*/ meltfptr[70]),
			  "SPRIM_OPER");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @SARGOP_ARGS",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V80*/ meltfptr[75])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V80*/ meltfptr[75]), (2),
			  ( /*_.TUPLE___V77*/ meltfptr[73]), "SARGOP_ARGS");
    ;
    /*^touchobj */

    melt_dbgtrace_written_object ( /*_.INST__V80*/ meltfptr[75],
				  "newly made instance");
    ;
    /*_.DINCRDBG__V79*/ meltfptr[71] = /*_.INST__V80*/ meltfptr[75];;
    MELT_LOCATION ("warmelt-macro.melt:6477:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*^rawallocobj */
    /*rawallocobj */
    {
      melt_ptr_t newobj = 0;
      melt_raw_object_create (newobj,
			      (melt_ptr_t) (( /*!CLASS_SOURCE_IF */
					     meltfrout->tabval[23])), (4),
			      "CLASS_SOURCE_IF");
  /*_.INST__V82*/ meltfptr[81] =
	newobj;
    };
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @LOCA_LOCATION",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V82*/ meltfptr[81])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V82*/ meltfptr[81]), (1),
			  ( /*_.LOC__V18*/ meltfptr[17]), "LOCA_LOCATION");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @SIF_TEST",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V82*/ meltfptr[81])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V82*/ meltfptr[81]), (2),
			  ( /*_.DNEEDBG__V66*/ meltfptr[58]), "SIF_TEST");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @SIF_THEN",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V82*/ meltfptr[81])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V82*/ meltfptr[81]), (3),
			  ( /*_.DCALL__V69*/ meltfptr[68]), "SIF_THEN");
    ;
    /*^touchobj */

    melt_dbgtrace_written_object ( /*_.INST__V82*/ meltfptr[81],
				  "newly made instance");
    ;
    /*_.DIFDBG__V81*/ meltfptr[80] = /*_.INST__V82*/ meltfptr[81];;
    MELT_LOCATION ("warmelt-macro.melt:6482:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    MELT_LOCATION ("warmelt-macro.melt:6485:/ blockmultialloc");
    /*multiallocblock */
    {
      struct meltletrec_5_st
      {
	struct MELT_MULTIPLE_STRUCT (2) rtup_0__TUPLREC__x14;
	long meltletrec_5_endgap;
      } *meltletrec_5_ptr = 0;
      meltletrec_5_ptr =
	(struct meltletrec_5_st *)
	meltgc_allocate (sizeof (struct meltletrec_5_st), 0);
      /*^blockmultialloc.initfill */
      /*inimult rtup_0__TUPLREC__x14 */
 /*_.TUPLREC___V84*/ meltfptr[83] =
	(melt_ptr_t) & meltletrec_5_ptr->rtup_0__TUPLREC__x14;
      meltletrec_5_ptr->rtup_0__TUPLREC__x14.discr =
	(meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (DISCR_MULTIPLE))));
      meltletrec_5_ptr->rtup_0__TUPLREC__x14.nbval = 2;


      /*^putuple */
      /*putupl#21 */
      melt_assertmsg ("putupl [:6485] #21 checktup",
		      melt_magic_discr ((melt_ptr_t)
					( /*_.TUPLREC___V84*/ meltfptr[83]))
		      == MELTOBMAG_MULTIPLE);
      melt_assertmsg ("putupl [:6485] #21 checkoff",
		      (0 >= 0
		       && 0 <
		       melt_multiple_length ((melt_ptr_t)
					     ( /*_.TUPLREC___V84*/
					      meltfptr[83]))));
      ((meltmultiple_ptr_t) ( /*_.TUPLREC___V84*/ meltfptr[83]))->tabval[0] =
	(melt_ptr_t) ( /*_.DINCRDBG__V79*/ meltfptr[71]);
      ;
      /*^putuple */
      /*putupl#22 */
      melt_assertmsg ("putupl [:6485] #22 checktup",
		      melt_magic_discr ((melt_ptr_t)
					( /*_.TUPLREC___V84*/ meltfptr[83]))
		      == MELTOBMAG_MULTIPLE);
      melt_assertmsg ("putupl [:6485] #22 checkoff",
		      (1 >= 0
		       && 1 <
		       melt_multiple_length ((melt_ptr_t)
					     ( /*_.TUPLREC___V84*/
					      meltfptr[83]))));
      ((meltmultiple_ptr_t) ( /*_.TUPLREC___V84*/ meltfptr[83]))->tabval[1] =
	(melt_ptr_t) ( /*_.DIFDBG__V81*/ meltfptr[80]);
      ;
      /*^touch */
      meltgc_touch ( /*_.TUPLREC___V84*/ meltfptr[83]);
      ;
      /*_.TUPLE___V83*/ meltfptr[82] = /*_.TUPLREC___V84*/ meltfptr[83];;
      /*epilog */

      MELT_LOCATION ("warmelt-macro.melt:6485:/ clear");
	    /*clear *//*_.TUPLREC___V84*/ meltfptr[83] = 0;
      /*^clear */
	    /*clear *//*_.TUPLREC___V84*/ meltfptr[83] = 0;
    }				/*end multiallocblock */
    ;
    MELT_LOCATION ("warmelt-macro.melt:6482:/ quasiblock");


    /*^rawallocobj */
    /*rawallocobj */
    {
      melt_ptr_t newobj = 0;
      melt_raw_object_create (newobj,
			      (melt_ptr_t) (( /*!CLASS_SOURCE_PROGN */
					     meltfrout->tabval[24])), (3),
			      "CLASS_SOURCE_PROGN");
  /*_.INST__V86*/ meltfptr[85] =
	newobj;
    };
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @LOCA_LOCATION",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V86*/ meltfptr[85])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V86*/ meltfptr[85]), (1),
			  ( /*_.LOC__V18*/ meltfptr[17]), "LOCA_LOCATION");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @SPROGN_BODY",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V86*/ meltfptr[85])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V86*/ meltfptr[85]), (2),
			  ( /*_.TUPLE___V83*/ meltfptr[82]), "SPROGN_BODY");
    ;
    /*^touchobj */

    melt_dbgtrace_written_object ( /*_.INST__V86*/ meltfptr[85],
				  "newly made instance");
    ;
    /*_.DPROGN__V85*/ meltfptr[83] = /*_.INST__V86*/ meltfptr[85];;
    MELT_LOCATION ("warmelt-macro.melt:6487:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*^rawallocobj */
    /*rawallocobj */
    {
      melt_ptr_t newobj = 0;
      melt_raw_object_create (newobj,
			      (melt_ptr_t) (( /*!CLASS_SOURCE_CPPIF */
					     meltfrout->tabval[25])), (5),
			      "CLASS_SOURCE_CPPIF");
  /*_.INST__V88*/ meltfptr[87] =
	newobj;
    };
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @LOCA_LOCATION",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V88*/ meltfptr[87])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V88*/ meltfptr[87]), (1),
			  ( /*_.LOC__V18*/ meltfptr[17]), "LOCA_LOCATION");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @SIFP_COND",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V88*/ meltfptr[87])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V88*/ meltfptr[87]), (2),
			  (( /*!konst_26_MELT_HAVE_DEBUG */ meltfrout->
			    tabval[26])), "SIFP_COND");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @SIFP_THEN",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V88*/ meltfptr[87])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V88*/ meltfptr[87]), (3),
			  ( /*_.DPROGN__V85*/ meltfptr[83]), "SIFP_THEN");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @SIFP_ELSE",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V88*/ meltfptr[87])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V88*/ meltfptr[87]), (4),
			  (( /*nil */ NULL)), "SIFP_ELSE");
    ;
    /*^touchobj */

    melt_dbgtrace_written_object ( /*_.INST__V88*/ meltfptr[87],
				  "newly made instance");
    ;
    /*_.DCPPIF__V87*/ meltfptr[86] = /*_.INST__V88*/ meltfptr[87];;
    MELT_LOCATION ("warmelt-macro.melt:6495:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#I__L28*/ meltfnum[26] =
      (( /*_#NBDBGARG__L23*/ meltfnum[18]) >= (15));;
    MELT_LOCATION ("warmelt-macro.melt:6495:/ cond");
    /*cond */ if ( /*_#I__L28*/ meltfnum[26])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-macro.melt:6496:/ quasiblock");


	  /*^checksignal */
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^apply */
	  /*apply */
	  {
	    union meltparam_un argtab[1];
	    memset (&argtab, 0, sizeof (argtab));
	    /*^apply.arg */
	    argtab[0].meltbp_long = /*_#NBDBGARG__L23*/ meltfnum[18];
	    /*_.STRNBARG__V89*/ meltfptr[88] =
	      melt_apply ((meltclosure_ptr_t)
			  (( /*!STRING4OUT */ meltfrout->tabval[10])),
			  (melt_ptr_t) (( /*!DISCR_STRING */ meltfrout->
					 tabval[11])), (MELTBPARSTR_LONG ""),
			  argtab, "", (union meltparam_un *) 0);
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-macro.melt:6498:/ locexp");
	    melt_inform_str ((melt_ptr_t) ( /*_.LOC__V18*/ meltfptr[17]),
			     ("quite long (DEBUG ...), perhaps consider splitting it"),
			     (melt_ptr_t) ( /*_.STRNBARG__V89*/
					   meltfptr[88]));
	  }
	  ;

	  MELT_LOCATION ("warmelt-macro.melt:6496:/ clear");
	     /*clear *//*_.STRNBARG__V89*/ meltfptr[88] = 0;
	  /*epilog */
	}
	;
      }				/*noelse */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-macro.melt:6501:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L29*/ meltfnum[28] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-macro.melt:6501:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L29*/ meltfnum[28])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L30*/ meltfnum[29] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-macro.melt:6501:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[7];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long =
		/*_#THE_MELTCALLCOUNT__L30*/ meltfnum[29];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-macro.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 6501;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "mexpand_debug dcall=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.DCALL__V69*/ meltfptr[68];
	      /*^apply.arg */
	      argtab[5].meltbp_cstring = " return dcppif=";
	      /*^apply.arg */
	      argtab[6].meltbp_aptr =
		(melt_ptr_t *) & /*_.DCPPIF__V87*/ meltfptr[86];
	      /*_.MELT_DEBUG_FUN__V92*/ meltfptr[91] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V91*/ meltfptr[90] =
	      /*_.MELT_DEBUG_FUN__V92*/ meltfptr[91];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-macro.melt:6501:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L30*/ meltfnum[29] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V92*/ meltfptr[91] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V91*/ meltfptr[90] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-macro.melt:6501:/ quasiblock");


      /*_.PROGN___V93*/ meltfptr[91] = /*_.IF___V91*/ meltfptr[90];;
      /*^compute */
      /*_.IFCPP___V90*/ meltfptr[88] = /*_.PROGN___V93*/ meltfptr[91];;
      /*epilog */

      MELT_LOCATION ("warmelt-macro.melt:6501:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L29*/ meltfnum[28] = 0;
      /*^clear */
	     /*clear *//*_.IF___V91*/ meltfptr[90] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V93*/ meltfptr[91] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V90*/ meltfptr[88] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-macro.melt:6502:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.DCPPIF__V87*/ meltfptr[86];;

    {
      MELT_LOCATION ("warmelt-macro.melt:6502:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*_.LET___V57*/ meltfptr[53] = /*_.RETURN___V94*/ meltfptr[90];;

    MELT_LOCATION ("warmelt-macro.melt:6441:/ clear");
	   /*clear *//*_#NBDBGARG__L23*/ meltfnum[18] = 0;
    /*^clear */
	   /*clear *//*_#IS_A__L24*/ meltfnum[19] = 0;
    /*^clear */
	   /*clear *//*_.THENEEDDBG__V58*/ meltfptr[54] = 0;
    /*^clear */
	   /*clear *//*_.TUPLE___V64*/ meltfptr[60] = 0;
    /*^clear */
	   /*clear *//*_.DNEEDBG__V66*/ meltfptr[58] = 0;
    /*^clear */
	   /*clear *//*_.LIST_TO_MULTIPLE__V68*/ meltfptr[67] = 0;
    /*^clear */
	   /*clear *//*_.DCALL__V69*/ meltfptr[68] = 0;
    /*^clear */
	   /*clear *//*_#IS_A__L26*/ meltfnum[24] = 0;
    /*^clear */
	   /*clear *//*_.THEINCRDBG__V71*/ meltfptr[70] = 0;
    /*^clear */
	   /*clear *//*_.TUPLE___V77*/ meltfptr[73] = 0;
    /*^clear */
	   /*clear *//*_.DINCRDBG__V79*/ meltfptr[71] = 0;
    /*^clear */
	   /*clear *//*_.DIFDBG__V81*/ meltfptr[80] = 0;
    /*^clear */
	   /*clear *//*_.TUPLE___V83*/ meltfptr[82] = 0;
    /*^clear */
	   /*clear *//*_.DPROGN__V85*/ meltfptr[83] = 0;
    /*^clear */
	   /*clear *//*_.DCPPIF__V87*/ meltfptr[86] = 0;
    /*^clear */
	   /*clear *//*_#I__L28*/ meltfnum[26] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V90*/ meltfptr[88] = 0;
    /*^clear */
	   /*clear *//*_.RETURN___V94*/ meltfptr[90] = 0;
    /*_.LET___V16*/ meltfptr[14] = /*_.LET___V57*/ meltfptr[53];;

    MELT_LOCATION ("warmelt-macro.melt:6385:/ clear");
	   /*clear *//*_.CONT__V17*/ meltfptr[16] = 0;
    /*^clear */
	   /*clear *//*_.LOC__V18*/ meltfptr[17] = 0;
    /*^clear */
	   /*clear *//*_#IS_MIXINT__L6*/ meltfnum[0] = 0;
    /*^clear */
	   /*clear *//*_.DFILNAM__V19*/ meltfptr[18] = 0;
    /*^clear */
	   /*clear *//*_#DLINE__L8*/ meltfnum[1] = 0;
    /*^clear */
	   /*clear *//*_.DEBUGFUNBIND__V23*/ meltfptr[21] = 0;
    /*^clear */
	   /*clear *//*_.THECOUNTBIND__V24*/ meltfptr[19] = 0;
    /*^clear */
	   /*clear *//*_.THENEEDDBGBIND__V25*/ meltfptr[24] = 0;
    /*^clear */
	   /*clear *//*_.THEINCRDBGBIND__V26*/ meltfptr[25] = 0;
    /*^clear */
	   /*clear *//*_.LIST_FIRST__V27*/ meltfptr[26] = 0;
    /*^clear */
	   /*clear *//*_.CURPAIR__V28*/ meltfptr[27] = 0;
    /*^clear */
	   /*clear *//*_.XARGLIST__V29*/ meltfptr[28] = 0;
    /*^clear */
	   /*clear *//*_.DBGARGLIST__V30*/ meltfptr[29] = 0;
    /*^clear */
	   /*clear *//*_#IS_STRING__L13*/ meltfnum[11] = 0;
    /*^clear */
	   /*clear *//*_#NOT__L14*/ meltfnum[8] = 0;
    /*^clear */
	   /*clear *//*_.IF___V35*/ meltfptr[31] = 0;
    /*^clear */
	   /*clear *//*_#IS_A__L15*/ meltfnum[9] = 0;
    /*^clear */
	   /*clear *//*_#OR___L16*/ meltfnum[10] = 0;
    /*^clear */
	   /*clear *//*_#NOT__L18*/ meltfnum[16] = 0;
    /*^clear */
	   /*clear *//*_.IF___V38*/ meltfptr[30] = 0;
    /*^clear */
	   /*clear *//*_.MAKE_INTEGERBOX__V51*/ meltfptr[36] = 0;
    /*^clear */
	   /*clear *//*_.LIST_APPEND2LIST__V52*/ meltfptr[43] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V53*/ meltfptr[39] = 0;
    /*^clear */
	   /*clear *//*_.LET___V57*/ meltfptr[53] = 0;
    MELT_LOCATION ("warmelt-macro.melt:6380:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.LET___V16*/ meltfptr[14];;

    {
      MELT_LOCATION ("warmelt-macro.melt:6380:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V6*/ meltfptr[5] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V10*/ meltfptr[6] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V12*/ meltfptr[7] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V14*/ meltfptr[12] = 0;
    /*^clear */
	   /*clear *//*_.LET___V16*/ meltfptr[14] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("MEXPAND_DEBUG", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_137_warmelt_macro_MEXPAND_DEBUG_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_137_warmelt_macro_MEXPAND_DEBUG */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_138_warmelt_macro_MEXPAND_EXPORT_VALUES (meltclosure_ptr_t
						  meltclosp_,
						  melt_ptr_t meltfirstargp_,
						  const melt_argdescr_cell_t
						  meltxargdescr_[],
						  union meltparam_un *
						  meltxargtab_,
						  const melt_argdescr_cell_t
						  meltxresdescr_[],
						  union meltparam_un *
						  meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_138_warmelt_macro_MEXPAND_EXPORT_VALUES_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_138_warmelt_macro_MEXPAND_EXPORT_VALUES_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 32
    melt_ptr_t mcfr_varptr[32];
#define MELTFRAM_NBVARNUM 8
    long mcfr_varnum[8];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_138_warmelt_macro_MEXPAND_EXPORT_VALUES is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_138_warmelt_macro_MEXPAND_EXPORT_VALUES_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 32; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_138_warmelt_macro_MEXPAND_EXPORT_VALUES nbval 32*/
  meltfram__.mcfr_nbvar = 32 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("MEXPAND_EXPORT_VALUES", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-macro.melt:6520:/ getarg");
 /*_.SEXPR__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.ENV__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.ENV__V3*/ meltfptr[2])) != NULL);


  /*getarg#2 */
  /*^getarg */
  if (meltxargdescr_[1] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.MEXPANDER__V4*/ meltfptr[3] =
    (meltxargtab_[1].meltbp_aptr) ? (*(meltxargtab_[1].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.MEXPANDER__V4*/ meltfptr[3])) !=
	      NULL);


  /*getarg#3 */
  /*^getarg */
  if (meltxargdescr_[2] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.MODCTX__V5*/ meltfptr[4] =
    (meltxargtab_[2].meltbp_aptr) ? (*(meltxargtab_[2].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.MODCTX__V5*/ meltfptr[4])) !=
	      NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-macro.melt:6521:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L1*/ meltfnum[0] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-macro.melt:6521:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L1*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-macro.melt:6521:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-macro.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 6521;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "mexpand_export_values sexpr";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.SEXPR__V2*/ meltfptr[1];
	      /*_.MELT_DEBUG_FUN__V8*/ meltfptr[7] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V7*/ meltfptr[6] = /*_.MELT_DEBUG_FUN__V8*/ meltfptr[7];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-macro.melt:6521:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V8*/ meltfptr[7] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V7*/ meltfptr[6] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-macro.melt:6521:/ quasiblock");


      /*_.PROGN___V9*/ meltfptr[7] = /*_.IF___V7*/ meltfptr[6];;
      /*^compute */
      /*_.IFCPP___V6*/ meltfptr[5] = /*_.PROGN___V9*/ meltfptr[7];;
      /*epilog */

      MELT_LOCATION ("warmelt-macro.melt:6521:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L1*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IF___V7*/ meltfptr[6] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V9*/ meltfptr[7] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V6*/ meltfptr[5] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-macro.melt:6522:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L3*/ meltfnum[1] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.SEXPR__V2*/ meltfptr[1]),
			     (melt_ptr_t) (( /*!CLASS_SEXPR */ meltfrout->
					    tabval[1])));;
      MELT_LOCATION ("warmelt-macro.melt:6522:/ cond");
      /*cond */ if ( /*_#IS_A__L3*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V11*/ meltfptr[7] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-macro.melt:6522:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check sexpr"),
				  ("warmelt-macro.melt")
				  ? ("warmelt-macro.melt") : __FILE__,
				  (6522) ? (6522) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V11*/ meltfptr[7] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V10*/ meltfptr[6] = /*_.IFELSE___V11*/ meltfptr[7];;
      /*epilog */

      MELT_LOCATION ("warmelt-macro.melt:6522:/ clear");
	     /*clear *//*_#IS_A__L3*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V11*/ meltfptr[7] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V10*/ meltfptr[6] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-macro.melt:6523:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L4*/ meltfnum[0] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.ENV__V3*/ meltfptr[2]),
			     (melt_ptr_t) (( /*!CLASS_ENVIRONMENT */
					    meltfrout->tabval[2])));;
      MELT_LOCATION ("warmelt-macro.melt:6523:/ cond");
      /*cond */ if ( /*_#IS_A__L4*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V13*/ meltfptr[12] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-macro.melt:6523:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check env"),
				  ("warmelt-macro.melt")
				  ? ("warmelt-macro.melt") : __FILE__,
				  (6523) ? (6523) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V13*/ meltfptr[12] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V12*/ meltfptr[7] = /*_.IFELSE___V13*/ meltfptr[12];;
      /*epilog */

      MELT_LOCATION ("warmelt-macro.melt:6523:/ clear");
	     /*clear *//*_#IS_A__L4*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V13*/ meltfptr[12] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V12*/ meltfptr[7] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-macro.melt:6524:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_CLOSURE__L5*/ meltfnum[1] =
	(melt_magic_discr ((melt_ptr_t) ( /*_.MEXPANDER__V4*/ meltfptr[3])) ==
	 MELTOBMAG_CLOSURE);;
      MELT_LOCATION ("warmelt-macro.melt:6524:/ cond");
      /*cond */ if ( /*_#IS_CLOSURE__L5*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V15*/ meltfptr[14] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-macro.melt:6524:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check mexpander"),
				  ("warmelt-macro.melt")
				  ? ("warmelt-macro.melt") : __FILE__,
				  (6524) ? (6524) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V15*/ meltfptr[14] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V14*/ meltfptr[12] = /*_.IFELSE___V15*/ meltfptr[14];;
      /*epilog */

      MELT_LOCATION ("warmelt-macro.melt:6524:/ clear");
	     /*clear *//*_#IS_CLOSURE__L5*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V15*/ meltfptr[14] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V14*/ meltfptr[12] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-macro.melt:6525:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_OBJECT__L6*/ meltfnum[0] =
	(melt_magic_discr ((melt_ptr_t) ( /*_.MODCTX__V5*/ meltfptr[4])) ==
	 MELTOBMAG_OBJECT);;
      MELT_LOCATION ("warmelt-macro.melt:6525:/ cond");
      /*cond */ if ( /*_#IS_OBJECT__L6*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V17*/ meltfptr[16] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-macro.melt:6525:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check modctx"),
				  ("warmelt-macro.melt")
				  ? ("warmelt-macro.melt") : __FILE__,
				  (6525) ? (6525) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V17*/ meltfptr[16] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V16*/ meltfptr[14] = /*_.IFELSE___V17*/ meltfptr[16];;
      /*epilog */

      MELT_LOCATION ("warmelt-macro.melt:6525:/ clear");
	     /*clear *//*_#IS_OBJECT__L6*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V17*/ meltfptr[16] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V16*/ meltfptr[14] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-macro.melt:6526:/ quasiblock");


    MELT_LOCATION ("warmelt-macro.melt:6527:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.SEXPR__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 2, "SEXP_CONTENTS");
  /*_.CONT__V19*/ meltfptr[18] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-macro.melt:6528:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.SEXPR__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 1, "LOCA_LOCATION");
  /*_.LOC__V20*/ meltfptr[19] = slot;
    };
    ;
 /*_.LIST_FIRST__V21*/ meltfptr[20] =
      (melt_list_first ((melt_ptr_t) ( /*_.CONT__V19*/ meltfptr[18])));;
    /*^compute */
 /*_.CURPAIR__V22*/ meltfptr[21] =
      (melt_pair_tail ((melt_ptr_t) ( /*_.LIST_FIRST__V21*/ meltfptr[20])));;
    MELT_LOCATION ("warmelt-macro.melt:6532:/ quasiblock");


    /*^newclosure */
		 /*newclosure *//*_.LAMBDA___V24*/ meltfptr[23] =
      (melt_ptr_t)
      meltgc_new_closure ((meltobject_ptr_t)
			  (((melt_ptr_t) (MELT_PREDEF (DISCR_CLOSURE)))),
			  (meltroutine_ptr_t) (( /*!konst_7 */ meltfrout->
						tabval[7])), (4));
    ;
    /*^putclosedv */
    /*putclosv */
    melt_assertmsg ("putclosv checkclo",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.LAMBDA___V24*/ meltfptr[23])) ==
		    MELTOBMAG_CLOSURE);
    melt_assertmsg ("putclosv checkoff", 0 >= 0
		    && 0 <
		    melt_closure_size ((melt_ptr_t)
				       ( /*_.LAMBDA___V24*/ meltfptr[23])));
    ((meltclosure_ptr_t) /*_.LAMBDA___V24*/ meltfptr[23])->tabval[0] =
      (melt_ptr_t) ( /*_.MEXPANDER__V4*/ meltfptr[3]);
    ;
    /*^putclosedv */
    /*putclosv */
    melt_assertmsg ("putclosv checkclo",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.LAMBDA___V24*/ meltfptr[23])) ==
		    MELTOBMAG_CLOSURE);
    melt_assertmsg ("putclosv checkoff", 1 >= 0
		    && 1 <
		    melt_closure_size ((melt_ptr_t)
				       ( /*_.LAMBDA___V24*/ meltfptr[23])));
    ((meltclosure_ptr_t) /*_.LAMBDA___V24*/ meltfptr[23])->tabval[1] =
      (melt_ptr_t) ( /*_.ENV__V3*/ meltfptr[2]);
    ;
    /*^putclosedv */
    /*putclosv */
    melt_assertmsg ("putclosv checkclo",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.LAMBDA___V24*/ meltfptr[23])) ==
		    MELTOBMAG_CLOSURE);
    melt_assertmsg ("putclosv checkoff", 2 >= 0
		    && 2 <
		    melt_closure_size ((melt_ptr_t)
				       ( /*_.LAMBDA___V24*/ meltfptr[23])));
    ((meltclosure_ptr_t) /*_.LAMBDA___V24*/ meltfptr[23])->tabval[2] =
      (melt_ptr_t) ( /*_.MODCTX__V5*/ meltfptr[4]);
    ;
    /*^putclosedv */
    /*putclosv */
    melt_assertmsg ("putclosv checkclo",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.LAMBDA___V24*/ meltfptr[23])) ==
		    MELTOBMAG_CLOSURE);
    melt_assertmsg ("putclosv checkoff", 3 >= 0
		    && 3 <
		    melt_closure_size ((melt_ptr_t)
				       ( /*_.LAMBDA___V24*/ meltfptr[23])));
    ((meltclosure_ptr_t) /*_.LAMBDA___V24*/ meltfptr[23])->tabval[3] =
      (melt_ptr_t) ( /*_.LOC__V20*/ meltfptr[19]);
    ;
    /*_.LAMBDA___V23*/ meltfptr[22] = /*_.LAMBDA___V24*/ meltfptr[23];;
    MELT_LOCATION ("warmelt-macro.melt:6530:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[2];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & ( /*!DISCR_MULTIPLE */ meltfrout->tabval[4]);
      /*^apply.arg */
      argtab[1].meltbp_aptr =
	(melt_ptr_t *) & /*_.LAMBDA___V23*/ meltfptr[22];
      /*_.SYMBTUP__V25*/ meltfptr[24] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!PAIRLIST_TO_MULTIPLE */ meltfrout->tabval[3])),
		    (melt_ptr_t) ( /*_.CURPAIR__V22*/ meltfptr[21]),
		    (MELTBPARSTR_PTR MELTBPARSTR_PTR ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-macro.melt:6540:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*^rawallocobj */
    /*rawallocobj */
    {
      melt_ptr_t newobj = 0;
      melt_raw_object_create (newobj,
			      (melt_ptr_t) (( /*!CLASS_SOURCE_EXPORT_VALUES */
					     meltfrout->tabval[8])), (3),
			      "CLASS_SOURCE_EXPORT_VALUES");
  /*_.INST__V27*/ meltfptr[26] =
	newobj;
    };
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @LOCA_LOCATION",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V27*/ meltfptr[26])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V27*/ meltfptr[26]), (1),
			  ( /*_.LOC__V20*/ meltfptr[19]), "LOCA_LOCATION");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @SEXPORT_NAMES",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V27*/ meltfptr[26])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V27*/ meltfptr[26]), (2),
			  ( /*_.SYMBTUP__V25*/ meltfptr[24]),
			  "SEXPORT_NAMES");
    ;
    /*^touchobj */

    melt_dbgtrace_written_object ( /*_.INST__V27*/ meltfptr[26],
				  "newly made instance");
    ;
    /*_.RES__V26*/ meltfptr[25] = /*_.INST__V27*/ meltfptr[26];;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-macro.melt:6544:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L7*/ meltfnum[1] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-macro.melt:6544:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L7*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L8*/ meltfnum[0] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-macro.melt:6544:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L8*/ meltfnum[0];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-macro.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 6544;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "mexpand_export_values result res";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.RES__V26*/ meltfptr[25];
	      /*_.MELT_DEBUG_FUN__V30*/ meltfptr[29] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V29*/ meltfptr[28] =
	      /*_.MELT_DEBUG_FUN__V30*/ meltfptr[29];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-macro.melt:6544:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L8*/ meltfnum[0] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V30*/ meltfptr[29] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V29*/ meltfptr[28] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-macro.melt:6544:/ quasiblock");


      /*_.PROGN___V31*/ meltfptr[29] = /*_.IF___V29*/ meltfptr[28];;
      /*^compute */
      /*_.IFCPP___V28*/ meltfptr[27] = /*_.PROGN___V31*/ meltfptr[29];;
      /*epilog */

      MELT_LOCATION ("warmelt-macro.melt:6544:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L7*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IF___V29*/ meltfptr[28] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V31*/ meltfptr[29] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V28*/ meltfptr[27] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-macro.melt:6545:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.RES__V26*/ meltfptr[25];;

    {
      MELT_LOCATION ("warmelt-macro.melt:6545:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*_.LET___V18*/ meltfptr[16] = /*_.RETURN___V32*/ meltfptr[28];;

    MELT_LOCATION ("warmelt-macro.melt:6526:/ clear");
	   /*clear *//*_.CONT__V19*/ meltfptr[18] = 0;
    /*^clear */
	   /*clear *//*_.LOC__V20*/ meltfptr[19] = 0;
    /*^clear */
	   /*clear *//*_.LIST_FIRST__V21*/ meltfptr[20] = 0;
    /*^clear */
	   /*clear *//*_.CURPAIR__V22*/ meltfptr[21] = 0;
    /*^clear */
	   /*clear *//*_.LAMBDA___V23*/ meltfptr[22] = 0;
    /*^clear */
	   /*clear *//*_.SYMBTUP__V25*/ meltfptr[24] = 0;
    /*^clear */
	   /*clear *//*_.RES__V26*/ meltfptr[25] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V28*/ meltfptr[27] = 0;
    /*^clear */
	   /*clear *//*_.RETURN___V32*/ meltfptr[28] = 0;
    MELT_LOCATION ("warmelt-macro.melt:6520:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.LET___V18*/ meltfptr[16];;

    {
      MELT_LOCATION ("warmelt-macro.melt:6520:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V6*/ meltfptr[5] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V10*/ meltfptr[6] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V12*/ meltfptr[7] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V14*/ meltfptr[12] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V16*/ meltfptr[14] = 0;
    /*^clear */
	   /*clear *//*_.LET___V18*/ meltfptr[16] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("MEXPAND_EXPORT_VALUES", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_138_warmelt_macro_MEXPAND_EXPORT_VALUES_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_138_warmelt_macro_MEXPAND_EXPORT_VALUES */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_139_warmelt_macro_LAMBDA___38__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un * meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un * meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_139_warmelt_macro_LAMBDA___38___melt =
    melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_139_warmelt_macro_LAMBDA___38___st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 8
    melt_ptr_t mcfr_varptr[8];
#define MELTFRAM_NBVARNUM 3
    long mcfr_varnum[3];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_139_warmelt_macro_LAMBDA___38__ is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_139_warmelt_macro_LAMBDA___38___st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 8; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_139_warmelt_macro_LAMBDA___38__ nbval 8*/
  meltfram__.mcfr_nbvar = 8 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("LAMBDA_", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-macro.melt:6532:/ getarg");
 /*_.S__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {

    MELT_LOCATION ("warmelt-macro.melt:6533:/ quasiblock");


    /*^checksignal */
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[3];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & ( /*~ENV */ meltfclos->tabval[1]);
      /*^apply.arg */
      argtab[1].meltbp_aptr =
	(melt_ptr_t *) & ( /*~MEXPANDER */ meltfclos->tabval[0]);
      /*^apply.arg */
      argtab[2].meltbp_aptr =
	(melt_ptr_t *) & ( /*~MODCTX */ meltfclos->tabval[2]);
      /*_.SYM__V4*/ meltfptr[3] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*~MEXPANDER */ meltfclos->tabval[0])),
		    (melt_ptr_t) ( /*_.S__V2*/ meltfptr[1]),
		    (MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_PTR ""),
		    argtab, "", (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-macro.melt:6534:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#IS_NOT_A__L1*/ meltfnum[0] =
      !melt_is_instance_of ((melt_ptr_t) ( /*_.SYM__V4*/ meltfptr[3]),
			    (melt_ptr_t) (( /*!CLASS_SYMBOL */ meltfrout->
					   tabval[0])));;
    MELT_LOCATION ("warmelt-macro.melt:6534:/ cond");
    /*cond */ if ( /*_#IS_NOT_A__L1*/ meltfnum[0])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{


#if MELT_HAVE_DEBUG
	  MELT_LOCATION ("warmelt-macro.melt:6536:/ cppif.then");
	  /*^block */
	  /*anyblock */
	  {


	    {
	      /*^locexp */
	      /*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	      melt_dbgcounter++;
#endif
	      ;
	    }
	    ;
	    /*^checksignal */
	    MELT_CHECK_SIGNAL ();
	    ;
     /*_#MELT_NEED_DBG__L2*/ meltfnum[1] =
	      /*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	      ( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	      0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	      ;;
	    MELT_LOCATION ("warmelt-macro.melt:6536:/ cond");
	    /*cond */ if ( /*_#MELT_NEED_DBG__L2*/ meltfnum[1])	/*then */
	      {
		/*^cond.then */
		/*^block */
		/*anyblock */
		{

       /*_#THE_MELTCALLCOUNT__L3*/ meltfnum[2] =
#ifdef meltcallcount
		    meltcallcount	/* the_meltcallcount */
#else
		    0L
#endif /* meltcallcount the_meltcallcount */
		    ;;
		  MELT_LOCATION ("warmelt-macro.melt:6536:/ checksignal");
		  MELT_CHECK_SIGNAL ();
		  ;
		  /*^apply */
		  /*apply */
		  {
		    union meltparam_un argtab[5];
		    memset (&argtab, 0, sizeof (argtab));
		    /*^apply.arg */
		    argtab[0].meltbp_long =
		      /*_#THE_MELTCALLCOUNT__L3*/ meltfnum[2];
		    /*^apply.arg */
		    argtab[1].meltbp_cstring = "warmelt-macro.melt";
		    /*^apply.arg */
		    argtab[2].meltbp_long = 6536;
		    /*^apply.arg */
		    argtab[3].meltbp_cstring =
		      "mexpand_export_values bad sym";
		    /*^apply.arg */
		    argtab[4].meltbp_aptr =
		      (melt_ptr_t *) & /*_.SYM__V4*/ meltfptr[3];
		    /*_.MELT_DEBUG_FUN__V7*/ meltfptr[6] =
		      melt_apply ((meltclosure_ptr_t)
				  (( /*!MELT_DEBUG_FUN */ meltfrout->
				    tabval[1])),
				  (melt_ptr_t) (( /*nil */ NULL)),
				  (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_PTR ""), argtab, "",
				  (union meltparam_un *) 0);
		  }
		  ;
		  /*_.IF___V6*/ meltfptr[5] =
		    /*_.MELT_DEBUG_FUN__V7*/ meltfptr[6];;
		  /*epilog */

		  MELT_LOCATION ("warmelt-macro.melt:6536:/ clear");
		 /*clear *//*_#THE_MELTCALLCOUNT__L3*/ meltfnum[2] = 0;
		  /*^clear */
		 /*clear *//*_.MELT_DEBUG_FUN__V7*/ meltfptr[6] = 0;
		}
		;
	      }
	    else
	      {			/*^cond.else */

      /*_.IF___V6*/ meltfptr[5] = NULL;;
	      }
	    ;
	    MELT_LOCATION ("warmelt-macro.melt:6536:/ quasiblock");


	    /*_.PROGN___V8*/ meltfptr[6] = /*_.IF___V6*/ meltfptr[5];;
	    /*^compute */
	    /*_.IFCPP___V5*/ meltfptr[4] = /*_.PROGN___V8*/ meltfptr[6];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-macro.melt:6536:/ clear");
	       /*clear *//*_#MELT_NEED_DBG__L2*/ meltfnum[1] = 0;
	    /*^clear */
	       /*clear *//*_.IF___V6*/ meltfptr[5] = 0;
	    /*^clear */
	       /*clear *//*_.PROGN___V8*/ meltfptr[6] = 0;
	  }

#else /*MELT_HAVE_DEBUG */
	  /*^cppif.else */
	  /*_.IFCPP___V5*/ meltfptr[4] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
	  ;

	  {
	    MELT_LOCATION ("warmelt-macro.melt:6537:/ locexp");
	    /* error_plain */
	      melt_error_str ((melt_ptr_t)
			      (( /*~LOC */ meltfclos->tabval[3])),
			      ("(EXPORT_VALUES <sym>...) expecting symbol"),
			      (melt_ptr_t) 0);
	  }
	  ;
	  MELT_LOCATION ("warmelt-macro.melt:6535:/ quasiblock");


	  /*epilog */

	  MELT_LOCATION ("warmelt-macro.melt:6534:/ clear");
	     /*clear *//*_.IFCPP___V5*/ meltfptr[4] = 0;
	}
	;
      }				/*noelse */
    ;
    /*_.LET___V3*/ meltfptr[2] = /*_.SYM__V4*/ meltfptr[3];;

    MELT_LOCATION ("warmelt-macro.melt:6533:/ clear");
	   /*clear *//*_.SYM__V4*/ meltfptr[3] = 0;
    /*^clear */
	   /*clear *//*_#IS_NOT_A__L1*/ meltfnum[0] = 0;
    MELT_LOCATION ("warmelt-macro.melt:6532:/ quasiblock");


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.LET___V3*/ meltfptr[2];;

    {
      MELT_LOCATION ("warmelt-macro.melt:6532:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.LET___V3*/ meltfptr[2] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("LAMBDA_", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_139_warmelt_macro_LAMBDA___38___melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_139_warmelt_macro_LAMBDA___38__ */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_140_warmelt_macro_MEXPAND_EXPORT_MACRO (meltclosure_ptr_t meltclosp_,
						 melt_ptr_t meltfirstargp_,
						 const melt_argdescr_cell_t
						 meltxargdescr_[],
						 union meltparam_un *
						 meltxargtab_,
						 const melt_argdescr_cell_t
						 meltxresdescr_[],
						 union meltparam_un *
						 meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_140_warmelt_macro_MEXPAND_EXPORT_MACRO_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_140_warmelt_macro_MEXPAND_EXPORT_MACRO_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 47
    melt_ptr_t mcfr_varptr[47];
#define MELTFRAM_NBVARNUM 11
    long mcfr_varnum[11];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_140_warmelt_macro_MEXPAND_EXPORT_MACRO is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_140_warmelt_macro_MEXPAND_EXPORT_MACRO_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 47; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_140_warmelt_macro_MEXPAND_EXPORT_MACRO nbval 47*/
  meltfram__.mcfr_nbvar = 47 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("MEXPAND_EXPORT_MACRO", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-macro.melt:6558:/ getarg");
 /*_.SEXPR__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.ENV__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.ENV__V3*/ meltfptr[2])) != NULL);


  /*getarg#2 */
  /*^getarg */
  if (meltxargdescr_[1] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.MEXPANDER__V4*/ meltfptr[3] =
    (meltxargtab_[1].meltbp_aptr) ? (*(meltxargtab_[1].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.MEXPANDER__V4*/ meltfptr[3])) !=
	      NULL);


  /*getarg#3 */
  /*^getarg */
  if (meltxargdescr_[2] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.MODCTX__V5*/ meltfptr[4] =
    (meltxargtab_[2].meltbp_aptr) ? (*(meltxargtab_[2].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.MODCTX__V5*/ meltfptr[4])) !=
	      NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-macro.melt:6559:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L1*/ meltfnum[0] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-macro.melt:6559:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L1*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-macro.melt:6559:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-macro.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 6559;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "mexpand_export_macro sexpr";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.SEXPR__V2*/ meltfptr[1];
	      /*_.MELT_DEBUG_FUN__V8*/ meltfptr[7] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V7*/ meltfptr[6] = /*_.MELT_DEBUG_FUN__V8*/ meltfptr[7];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-macro.melt:6559:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V8*/ meltfptr[7] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V7*/ meltfptr[6] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-macro.melt:6559:/ quasiblock");


      /*_.PROGN___V9*/ meltfptr[7] = /*_.IF___V7*/ meltfptr[6];;
      /*^compute */
      /*_.IFCPP___V6*/ meltfptr[5] = /*_.PROGN___V9*/ meltfptr[7];;
      /*epilog */

      MELT_LOCATION ("warmelt-macro.melt:6559:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L1*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IF___V7*/ meltfptr[6] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V9*/ meltfptr[7] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V6*/ meltfptr[5] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-macro.melt:6560:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L3*/ meltfnum[1] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.SEXPR__V2*/ meltfptr[1]),
			     (melt_ptr_t) (( /*!CLASS_SEXPR */ meltfrout->
					    tabval[1])));;
      MELT_LOCATION ("warmelt-macro.melt:6560:/ cond");
      /*cond */ if ( /*_#IS_A__L3*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V11*/ meltfptr[7] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-macro.melt:6560:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check sexpr"),
				  ("warmelt-macro.melt")
				  ? ("warmelt-macro.melt") : __FILE__,
				  (6560) ? (6560) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V11*/ meltfptr[7] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V10*/ meltfptr[6] = /*_.IFELSE___V11*/ meltfptr[7];;
      /*epilog */

      MELT_LOCATION ("warmelt-macro.melt:6560:/ clear");
	     /*clear *//*_#IS_A__L3*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V11*/ meltfptr[7] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V10*/ meltfptr[6] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-macro.melt:6561:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L4*/ meltfnum[0] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.ENV__V3*/ meltfptr[2]),
			     (melt_ptr_t) (( /*!CLASS_ENVIRONMENT */
					    meltfrout->tabval[2])));;
      MELT_LOCATION ("warmelt-macro.melt:6561:/ cond");
      /*cond */ if ( /*_#IS_A__L4*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V13*/ meltfptr[12] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-macro.melt:6561:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check env"),
				  ("warmelt-macro.melt")
				  ? ("warmelt-macro.melt") : __FILE__,
				  (6561) ? (6561) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V13*/ meltfptr[12] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V12*/ meltfptr[7] = /*_.IFELSE___V13*/ meltfptr[12];;
      /*epilog */

      MELT_LOCATION ("warmelt-macro.melt:6561:/ clear");
	     /*clear *//*_#IS_A__L4*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V13*/ meltfptr[12] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V12*/ meltfptr[7] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-macro.melt:6562:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_CLOSURE__L5*/ meltfnum[1] =
	(melt_magic_discr ((melt_ptr_t) ( /*_.MEXPANDER__V4*/ meltfptr[3])) ==
	 MELTOBMAG_CLOSURE);;
      MELT_LOCATION ("warmelt-macro.melt:6562:/ cond");
      /*cond */ if ( /*_#IS_CLOSURE__L5*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V15*/ meltfptr[14] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-macro.melt:6562:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check mexpander"),
				  ("warmelt-macro.melt")
				  ? ("warmelt-macro.melt") : __FILE__,
				  (6562) ? (6562) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V15*/ meltfptr[14] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V14*/ meltfptr[12] = /*_.IFELSE___V15*/ meltfptr[14];;
      /*epilog */

      MELT_LOCATION ("warmelt-macro.melt:6562:/ clear");
	     /*clear *//*_#IS_CLOSURE__L5*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V15*/ meltfptr[14] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V14*/ meltfptr[12] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-macro.melt:6563:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_OBJECT__L6*/ meltfnum[0] =
	(melt_magic_discr ((melt_ptr_t) ( /*_.MODCTX__V5*/ meltfptr[4])) ==
	 MELTOBMAG_OBJECT);;
      MELT_LOCATION ("warmelt-macro.melt:6563:/ cond");
      /*cond */ if ( /*_#IS_OBJECT__L6*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V17*/ meltfptr[16] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-macro.melt:6563:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check modctx"),
				  ("warmelt-macro.melt")
				  ? ("warmelt-macro.melt") : __FILE__,
				  (6563) ? (6563) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V17*/ meltfptr[16] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V16*/ meltfptr[14] = /*_.IFELSE___V17*/ meltfptr[16];;
      /*epilog */

      MELT_LOCATION ("warmelt-macro.melt:6563:/ clear");
	     /*clear *//*_#IS_OBJECT__L6*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V17*/ meltfptr[16] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V16*/ meltfptr[14] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-macro.melt:6564:/ quasiblock");


    MELT_LOCATION ("warmelt-macro.melt:6565:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.SEXPR__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 2, "SEXP_CONTENTS");
  /*_.CONT__V19*/ meltfptr[18] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-macro.melt:6566:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.SEXPR__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 1, "LOCA_LOCATION");
  /*_.LOC__V20*/ meltfptr[19] = slot;
    };
    ;
 /*_.LIST_FIRST__V21*/ meltfptr[20] =
      (melt_list_first ((melt_ptr_t) ( /*_.CONT__V19*/ meltfptr[18])));;
    /*^compute */
 /*_.CURPAIR__V22*/ meltfptr[21] =
      (melt_pair_tail ((melt_ptr_t) ( /*_.LIST_FIRST__V21*/ meltfptr[20])));;
    /*^compute */
 /*_.PAIR_HEAD__V23*/ meltfptr[22] =
      (melt_pair_head ((melt_ptr_t) ( /*_.CURPAIR__V22*/ meltfptr[21])));;
    MELT_LOCATION ("warmelt-macro.melt:6568:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[3];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.ENV__V3*/ meltfptr[2];
      /*^apply.arg */
      argtab[1].meltbp_aptr =
	(melt_ptr_t *) & /*_.MEXPANDER__V4*/ meltfptr[3];
      /*^apply.arg */
      argtab[2].meltbp_aptr = (melt_ptr_t *) & /*_.MODCTX__V5*/ meltfptr[4];
      /*_.SYMB__V24*/ meltfptr[23] =
	melt_apply ((meltclosure_ptr_t) ( /*_.MEXPANDER__V4*/ meltfptr[3]),
		    (melt_ptr_t) ( /*_.PAIR_HEAD__V23*/ meltfptr[22]),
		    (MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_PTR ""),
		    argtab, "", (union meltparam_un *) 0);
    }
    ;
    /*_.DOC__V25*/ meltfptr[24] = ( /*nil */ NULL);;
    MELT_LOCATION ("warmelt-macro.melt:6571:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#IS_NOT_A__L7*/ meltfnum[1] =
      !melt_is_instance_of ((melt_ptr_t) ( /*_.SYMB__V24*/ meltfptr[23]),
			    (melt_ptr_t) (( /*!CLASS_SYMBOL */ meltfrout->
					   tabval[3])));;
    MELT_LOCATION ("warmelt-macro.melt:6571:/ cond");
    /*cond */ if ( /*_#IS_NOT_A__L7*/ meltfnum[1])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{


	  {
	    MELT_LOCATION ("warmelt-macro.melt:6572:/ locexp");
	    /* error_plain */
	      melt_error_str ((melt_ptr_t) ( /*_.LOC__V20*/ meltfptr[19]),
			      ("(EXPORT_MACRO <sym> <expander>) expecting symbol"),
			      (melt_ptr_t) 0);
	  }
	  ;
	  /*epilog */
	}
	;
      }				/*noelse */
    ;
 /*_.PAIR_TAIL__V26*/ meltfptr[25] =
      (melt_pair_tail ((melt_ptr_t) ( /*_.CURPAIR__V22*/ meltfptr[21])));;
    MELT_LOCATION ("warmelt-macro.melt:6573:/ compute");
    /*_.CURPAIR__V22*/ meltfptr[21] = /*_.SETQ___V27*/ meltfptr[26] =
      /*_.PAIR_TAIL__V26*/ meltfptr[25];;
    MELT_LOCATION ("warmelt-macro.melt:6574:/ quasiblock");


 /*_.PAIR_HEAD__V29*/ meltfptr[28] =
      (melt_pair_head ((melt_ptr_t) ( /*_.CURPAIR__V22*/ meltfptr[21])));;
    MELT_LOCATION ("warmelt-macro.melt:6574:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[3];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.ENV__V3*/ meltfptr[2];
      /*^apply.arg */
      argtab[1].meltbp_aptr =
	(melt_ptr_t *) & /*_.MEXPANDER__V4*/ meltfptr[3];
      /*^apply.arg */
      argtab[2].meltbp_aptr = (melt_ptr_t *) & /*_.MODCTX__V5*/ meltfptr[4];
      /*_.EXPV__V30*/ meltfptr[29] =
	melt_apply ((meltclosure_ptr_t) ( /*_.MEXPANDER__V4*/ meltfptr[3]),
		    (melt_ptr_t) ( /*_.PAIR_HEAD__V29*/ meltfptr[28]),
		    (MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_PTR ""),
		    argtab, "", (union meltparam_un *) 0);
    }
    ;
 /*_.PAIR_TAIL__V31*/ meltfptr[30] =
      (melt_pair_tail ((melt_ptr_t) ( /*_.CURPAIR__V22*/ meltfptr[21])));;
    MELT_LOCATION ("warmelt-macro.melt:6576:/ compute");
    /*_.CURPAIR__V22*/ meltfptr[21] = /*_.SETQ___V32*/ meltfptr[31] =
      /*_.PAIR_TAIL__V31*/ meltfptr[30];;
    MELT_LOCATION ("warmelt-macro.melt:6577:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_.PAIR_HEAD__V33*/ meltfptr[32] =
      (melt_pair_head ((melt_ptr_t) ( /*_.CURPAIR__V22*/ meltfptr[21])));;
    /*^compute */
 /*_#__L8*/ meltfnum[0] =
      (( /*_.PAIR_HEAD__V33*/ meltfptr[32]) ==
       (( /*!konst_4_DOC */ meltfrout->tabval[4])));;
    MELT_LOCATION ("warmelt-macro.melt:6577:/ cond");
    /*cond */ if ( /*_#__L8*/ meltfnum[0])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

   /*_.PAIR_TAIL__V35*/ meltfptr[34] =
	    (melt_pair_tail
	     ((melt_ptr_t) ( /*_.CURPAIR__V22*/ meltfptr[21])));;
	  MELT_LOCATION ("warmelt-macro.melt:6579:/ compute");
	  /*_.CURPAIR__V22*/ meltfptr[21] = /*_.SETQ___V36*/ meltfptr[35] =
	    /*_.PAIR_TAIL__V35*/ meltfptr[34];;
   /*_.PAIR_HEAD__V37*/ meltfptr[36] =
	    (melt_pair_head
	     ((melt_ptr_t) ( /*_.CURPAIR__V22*/ meltfptr[21])));;
	  MELT_LOCATION ("warmelt-macro.melt:6580:/ compute");
	  /*_.DOC__V25*/ meltfptr[24] = /*_.SETQ___V38*/ meltfptr[37] =
	    /*_.PAIR_HEAD__V37*/ meltfptr[36];;
	  MELT_LOCATION ("warmelt-macro.melt:6578:/ quasiblock");


	  /*_.PROGN___V39*/ meltfptr[38] = /*_.SETQ___V38*/ meltfptr[37];;
	  /*^compute */
	  /*_.IF___V34*/ meltfptr[33] = /*_.PROGN___V39*/ meltfptr[38];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-macro.melt:6577:/ clear");
	     /*clear *//*_.PAIR_TAIL__V35*/ meltfptr[34] = 0;
	  /*^clear */
	     /*clear *//*_.SETQ___V36*/ meltfptr[35] = 0;
	  /*^clear */
	     /*clear *//*_.PAIR_HEAD__V37*/ meltfptr[36] = 0;
	  /*^clear */
	     /*clear *//*_.SETQ___V38*/ meltfptr[37] = 0;
	  /*^clear */
	     /*clear *//*_.PROGN___V39*/ meltfptr[38] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

  /*_.IF___V34*/ meltfptr[33] = NULL;;
      }
    ;
    MELT_LOCATION ("warmelt-macro.melt:6581:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#NULL__L9*/ meltfnum[8] =
      (( /*_.EXPV__V30*/ meltfptr[29]) == NULL);;
    MELT_LOCATION ("warmelt-macro.melt:6581:/ cond");
    /*cond */ if ( /*_#NULL__L9*/ meltfnum[8])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{


	  {
	    MELT_LOCATION ("warmelt-macro.melt:6582:/ locexp");
	    /* error_plain */
	      melt_error_str ((melt_ptr_t) ( /*_.LOC__V20*/ meltfptr[19]),
			      ("(EXPORT_MACRO <sym> <expander> [:doc <docum>]) expecting expander"),
			      (melt_ptr_t) 0);
	  }
	  ;
	  /*epilog */
	}
	;
      }				/*noelse */
    ;
    MELT_LOCATION ("warmelt-macro.melt:6583:/ quasiblock");


    /*^checksignal */
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*^rawallocobj */
    /*rawallocobj */
    {
      melt_ptr_t newobj = 0;
      melt_raw_object_create (newobj,
			      (melt_ptr_t) (( /*!CLASS_SOURCE_EXPORT_MACRO */
					     meltfrout->tabval[5])), (5),
			      "CLASS_SOURCE_EXPORT_MACRO");
  /*_.INST__V42*/ meltfptr[36] =
	newobj;
    };
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @LOCA_LOCATION",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V42*/ meltfptr[36])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V42*/ meltfptr[36]), (1),
			  ( /*_.LOC__V20*/ meltfptr[19]), "LOCA_LOCATION");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @SEXPMAC_MNAME",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V42*/ meltfptr[36])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V42*/ meltfptr[36]), (2),
			  ( /*_.SYMB__V24*/ meltfptr[23]), "SEXPMAC_MNAME");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @SEXPMAC_MVAL",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V42*/ meltfptr[36])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V42*/ meltfptr[36]), (3),
			  ( /*_.EXPV__V30*/ meltfptr[29]), "SEXPMAC_MVAL");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @SEXPMAC_DOC",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V42*/ meltfptr[36])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V42*/ meltfptr[36]), (4),
			  ( /*_.DOC__V25*/ meltfptr[24]), "SEXPMAC_DOC");
    ;
    /*^touchobj */

    melt_dbgtrace_written_object ( /*_.INST__V42*/ meltfptr[36],
				  "newly made instance");
    ;
    /*_.RES__V41*/ meltfptr[35] = /*_.INST__V42*/ meltfptr[36];;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-macro.melt:6590:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L10*/ meltfnum[9] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-macro.melt:6590:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L10*/ meltfnum[9])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L11*/ meltfnum[10] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-macro.melt:6590:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long =
		/*_#THE_MELTCALLCOUNT__L11*/ meltfnum[10];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-macro.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 6590;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "mexpand_export_macro result res";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.RES__V41*/ meltfptr[35];
	      /*_.MELT_DEBUG_FUN__V45*/ meltfptr[44] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V44*/ meltfptr[38] =
	      /*_.MELT_DEBUG_FUN__V45*/ meltfptr[44];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-macro.melt:6590:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L11*/ meltfnum[10] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V45*/ meltfptr[44] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V44*/ meltfptr[38] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-macro.melt:6590:/ quasiblock");


      /*_.PROGN___V46*/ meltfptr[44] = /*_.IF___V44*/ meltfptr[38];;
      /*^compute */
      /*_.IFCPP___V43*/ meltfptr[37] = /*_.PROGN___V46*/ meltfptr[44];;
      /*epilog */

      MELT_LOCATION ("warmelt-macro.melt:6590:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L10*/ meltfnum[9] = 0;
      /*^clear */
	     /*clear *//*_.IF___V44*/ meltfptr[38] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V46*/ meltfptr[44] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V43*/ meltfptr[37] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-macro.melt:6591:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.RES__V41*/ meltfptr[35];;

    {
      MELT_LOCATION ("warmelt-macro.melt:6591:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*_.LET___V40*/ meltfptr[34] = /*_.RETURN___V47*/ meltfptr[38];;

    MELT_LOCATION ("warmelt-macro.melt:6583:/ clear");
	   /*clear *//*_.RES__V41*/ meltfptr[35] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V43*/ meltfptr[37] = 0;
    /*^clear */
	   /*clear *//*_.RETURN___V47*/ meltfptr[38] = 0;
    /*_.LET___V28*/ meltfptr[27] = /*_.LET___V40*/ meltfptr[34];;

    MELT_LOCATION ("warmelt-macro.melt:6574:/ clear");
	   /*clear *//*_.PAIR_HEAD__V29*/ meltfptr[28] = 0;
    /*^clear */
	   /*clear *//*_.EXPV__V30*/ meltfptr[29] = 0;
    /*^clear */
	   /*clear *//*_.PAIR_TAIL__V31*/ meltfptr[30] = 0;
    /*^clear */
	   /*clear *//*_.SETQ___V32*/ meltfptr[31] = 0;
    /*^clear */
	   /*clear *//*_.PAIR_HEAD__V33*/ meltfptr[32] = 0;
    /*^clear */
	   /*clear *//*_#__L8*/ meltfnum[0] = 0;
    /*^clear */
	   /*clear *//*_.IF___V34*/ meltfptr[33] = 0;
    /*^clear */
	   /*clear *//*_#NULL__L9*/ meltfnum[8] = 0;
    /*^clear */
	   /*clear *//*_.LET___V40*/ meltfptr[34] = 0;
    /*_.LET___V18*/ meltfptr[16] = /*_.LET___V28*/ meltfptr[27];;

    MELT_LOCATION ("warmelt-macro.melt:6564:/ clear");
	   /*clear *//*_.CONT__V19*/ meltfptr[18] = 0;
    /*^clear */
	   /*clear *//*_.LOC__V20*/ meltfptr[19] = 0;
    /*^clear */
	   /*clear *//*_.LIST_FIRST__V21*/ meltfptr[20] = 0;
    /*^clear */
	   /*clear *//*_.CURPAIR__V22*/ meltfptr[21] = 0;
    /*^clear */
	   /*clear *//*_.PAIR_HEAD__V23*/ meltfptr[22] = 0;
    /*^clear */
	   /*clear *//*_.SYMB__V24*/ meltfptr[23] = 0;
    /*^clear */
	   /*clear *//*_.DOC__V25*/ meltfptr[24] = 0;
    /*^clear */
	   /*clear *//*_#IS_NOT_A__L7*/ meltfnum[1] = 0;
    /*^clear */
	   /*clear *//*_.PAIR_TAIL__V26*/ meltfptr[25] = 0;
    /*^clear */
	   /*clear *//*_.SETQ___V27*/ meltfptr[26] = 0;
    /*^clear */
	   /*clear *//*_.LET___V28*/ meltfptr[27] = 0;
    MELT_LOCATION ("warmelt-macro.melt:6558:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.LET___V18*/ meltfptr[16];;

    {
      MELT_LOCATION ("warmelt-macro.melt:6558:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V6*/ meltfptr[5] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V10*/ meltfptr[6] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V12*/ meltfptr[7] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V14*/ meltfptr[12] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V16*/ meltfptr[14] = 0;
    /*^clear */
	   /*clear *//*_.LET___V18*/ meltfptr[16] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("MEXPAND_EXPORT_MACRO", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_140_warmelt_macro_MEXPAND_EXPORT_MACRO_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_140_warmelt_macro_MEXPAND_EXPORT_MACRO */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_141_warmelt_macro_MEXPAND_EXPORT_PATMACRO (meltclosure_ptr_t
						    meltclosp_,
						    melt_ptr_t meltfirstargp_,
						    const melt_argdescr_cell_t
						    meltxargdescr_[],
						    union meltparam_un *
						    meltxargtab_,
						    const melt_argdescr_cell_t
						    meltxresdescr_[],
						    union meltparam_un *
						    meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_141_warmelt_macro_MEXPAND_EXPORT_PATMACRO_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_141_warmelt_macro_MEXPAND_EXPORT_PATMACRO_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 54
    melt_ptr_t mcfr_varptr[54];
#define MELTFRAM_NBVARNUM 12
    long mcfr_varnum[12];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_141_warmelt_macro_MEXPAND_EXPORT_PATMACRO is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_141_warmelt_macro_MEXPAND_EXPORT_PATMACRO_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 54; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_141_warmelt_macro_MEXPAND_EXPORT_PATMACRO nbval 54*/
  meltfram__.mcfr_nbvar = 54 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("MEXPAND_EXPORT_PATMACRO", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-macro.melt:6602:/ getarg");
 /*_.SEXPR__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.ENV__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.ENV__V3*/ meltfptr[2])) != NULL);


  /*getarg#2 */
  /*^getarg */
  if (meltxargdescr_[1] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.MEXPANDER__V4*/ meltfptr[3] =
    (meltxargtab_[1].meltbp_aptr) ? (*(meltxargtab_[1].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.MEXPANDER__V4*/ meltfptr[3])) !=
	      NULL);


  /*getarg#3 */
  /*^getarg */
  if (meltxargdescr_[2] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.MODCTX__V5*/ meltfptr[4] =
    (meltxargtab_[2].meltbp_aptr) ? (*(meltxargtab_[2].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.MODCTX__V5*/ meltfptr[4])) !=
	      NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-macro.melt:6603:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L1*/ meltfnum[0] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-macro.melt:6603:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L1*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-macro.melt:6603:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-macro.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 6603;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "mexpand_export_patmacro sexpr";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.SEXPR__V2*/ meltfptr[1];
	      /*_.MELT_DEBUG_FUN__V8*/ meltfptr[7] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V7*/ meltfptr[6] = /*_.MELT_DEBUG_FUN__V8*/ meltfptr[7];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-macro.melt:6603:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V8*/ meltfptr[7] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V7*/ meltfptr[6] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-macro.melt:6603:/ quasiblock");


      /*_.PROGN___V9*/ meltfptr[7] = /*_.IF___V7*/ meltfptr[6];;
      /*^compute */
      /*_.IFCPP___V6*/ meltfptr[5] = /*_.PROGN___V9*/ meltfptr[7];;
      /*epilog */

      MELT_LOCATION ("warmelt-macro.melt:6603:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L1*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IF___V7*/ meltfptr[6] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V9*/ meltfptr[7] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V6*/ meltfptr[5] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-macro.melt:6604:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L3*/ meltfnum[1] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.SEXPR__V2*/ meltfptr[1]),
			     (melt_ptr_t) (( /*!CLASS_SEXPR */ meltfrout->
					    tabval[1])));;
      MELT_LOCATION ("warmelt-macro.melt:6604:/ cond");
      /*cond */ if ( /*_#IS_A__L3*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V11*/ meltfptr[7] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-macro.melt:6604:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check sexpr"),
				  ("warmelt-macro.melt")
				  ? ("warmelt-macro.melt") : __FILE__,
				  (6604) ? (6604) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V11*/ meltfptr[7] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V10*/ meltfptr[6] = /*_.IFELSE___V11*/ meltfptr[7];;
      /*epilog */

      MELT_LOCATION ("warmelt-macro.melt:6604:/ clear");
	     /*clear *//*_#IS_A__L3*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V11*/ meltfptr[7] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V10*/ meltfptr[6] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-macro.melt:6605:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L4*/ meltfnum[0] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.ENV__V3*/ meltfptr[2]),
			     (melt_ptr_t) (( /*!CLASS_ENVIRONMENT */
					    meltfrout->tabval[2])));;
      MELT_LOCATION ("warmelt-macro.melt:6605:/ cond");
      /*cond */ if ( /*_#IS_A__L4*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V13*/ meltfptr[12] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-macro.melt:6605:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check env"),
				  ("warmelt-macro.melt")
				  ? ("warmelt-macro.melt") : __FILE__,
				  (6605) ? (6605) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V13*/ meltfptr[12] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V12*/ meltfptr[7] = /*_.IFELSE___V13*/ meltfptr[12];;
      /*epilog */

      MELT_LOCATION ("warmelt-macro.melt:6605:/ clear");
	     /*clear *//*_#IS_A__L4*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V13*/ meltfptr[12] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V12*/ meltfptr[7] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-macro.melt:6606:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_CLOSURE__L5*/ meltfnum[1] =
	(melt_magic_discr ((melt_ptr_t) ( /*_.MEXPANDER__V4*/ meltfptr[3])) ==
	 MELTOBMAG_CLOSURE);;
      MELT_LOCATION ("warmelt-macro.melt:6606:/ cond");
      /*cond */ if ( /*_#IS_CLOSURE__L5*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V15*/ meltfptr[14] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-macro.melt:6606:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check mexpander"),
				  ("warmelt-macro.melt")
				  ? ("warmelt-macro.melt") : __FILE__,
				  (6606) ? (6606) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V15*/ meltfptr[14] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V14*/ meltfptr[12] = /*_.IFELSE___V15*/ meltfptr[14];;
      /*epilog */

      MELT_LOCATION ("warmelt-macro.melt:6606:/ clear");
	     /*clear *//*_#IS_CLOSURE__L5*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V15*/ meltfptr[14] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V14*/ meltfptr[12] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-macro.melt:6607:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_OBJECT__L6*/ meltfnum[0] =
	(melt_magic_discr ((melt_ptr_t) ( /*_.MODCTX__V5*/ meltfptr[4])) ==
	 MELTOBMAG_OBJECT);;
      MELT_LOCATION ("warmelt-macro.melt:6607:/ cond");
      /*cond */ if ( /*_#IS_OBJECT__L6*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V17*/ meltfptr[16] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-macro.melt:6607:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check modctx"),
				  ("warmelt-macro.melt")
				  ? ("warmelt-macro.melt") : __FILE__,
				  (6607) ? (6607) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V17*/ meltfptr[16] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V16*/ meltfptr[14] = /*_.IFELSE___V17*/ meltfptr[16];;
      /*epilog */

      MELT_LOCATION ("warmelt-macro.melt:6607:/ clear");
	     /*clear *//*_#IS_OBJECT__L6*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V17*/ meltfptr[16] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V16*/ meltfptr[14] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-macro.melt:6608:/ quasiblock");


    MELT_LOCATION ("warmelt-macro.melt:6609:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.SEXPR__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 2, "SEXP_CONTENTS");
  /*_.CONT__V19*/ meltfptr[18] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-macro.melt:6610:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.SEXPR__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 1, "LOCA_LOCATION");
  /*_.LOC__V20*/ meltfptr[19] = slot;
    };
    ;
 /*_.LIST_FIRST__V21*/ meltfptr[20] =
      (melt_list_first ((melt_ptr_t) ( /*_.CONT__V19*/ meltfptr[18])));;
    /*^compute */
 /*_.CURPAIR__V22*/ meltfptr[21] =
      (melt_pair_tail ((melt_ptr_t) ( /*_.LIST_FIRST__V21*/ meltfptr[20])));;
    /*^compute */
 /*_.PAIR_HEAD__V23*/ meltfptr[22] =
      (melt_pair_head ((melt_ptr_t) ( /*_.CURPAIR__V22*/ meltfptr[21])));;
    MELT_LOCATION ("warmelt-macro.melt:6612:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[3];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.ENV__V3*/ meltfptr[2];
      /*^apply.arg */
      argtab[1].meltbp_aptr =
	(melt_ptr_t *) & /*_.MEXPANDER__V4*/ meltfptr[3];
      /*^apply.arg */
      argtab[2].meltbp_aptr = (melt_ptr_t *) & /*_.MODCTX__V5*/ meltfptr[4];
      /*_.SYMB__V24*/ meltfptr[23] =
	melt_apply ((meltclosure_ptr_t) ( /*_.MEXPANDER__V4*/ meltfptr[3]),
		    (melt_ptr_t) ( /*_.PAIR_HEAD__V23*/ meltfptr[22]),
		    (MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_PTR ""),
		    argtab, "", (union meltparam_un *) 0);
    }
    ;
    /*_.PATEXPV__V25*/ meltfptr[24] = ( /*nil */ NULL);;
    /*^compute */
    /*_.MACEXPV__V26*/ meltfptr[25] = ( /*nil */ NULL);;
    /*^compute */
    /*_.DOC__V27*/ meltfptr[26] = ( /*nil */ NULL);;
    MELT_LOCATION ("warmelt-macro.melt:6617:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#IS_NOT_A__L7*/ meltfnum[1] =
      !melt_is_instance_of ((melt_ptr_t) ( /*_.SYMB__V24*/ meltfptr[23]),
			    (melt_ptr_t) (( /*!CLASS_SYMBOL */ meltfrout->
					   tabval[3])));;
    MELT_LOCATION ("warmelt-macro.melt:6617:/ cond");
    /*cond */ if ( /*_#IS_NOT_A__L7*/ meltfnum[1])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{


	  {
	    MELT_LOCATION ("warmelt-macro.melt:6618:/ locexp");
	    /* error_plain */ melt_error_str ((melt_ptr_t) ( /*_.LOC__V20*/ meltfptr[19]), ("(EXPORT_PATMACRO <sym> <patexpander> <macexpander> [:doc <docum>])\
 expecting symbol"), (melt_ptr_t) 0);
	  }
	  ;
	  /*epilog */
	}
	;
      }				/*noelse */
    ;
 /*_.PAIR_TAIL__V28*/ meltfptr[27] =
      (melt_pair_tail ((melt_ptr_t) ( /*_.CURPAIR__V22*/ meltfptr[21])));;
    MELT_LOCATION ("warmelt-macro.melt:6619:/ compute");
    /*_.CURPAIR__V22*/ meltfptr[21] = /*_.SETQ___V29*/ meltfptr[28] =
      /*_.PAIR_TAIL__V28*/ meltfptr[27];;
 /*_.PAIR_HEAD__V30*/ meltfptr[29] =
      (melt_pair_head ((melt_ptr_t) ( /*_.CURPAIR__V22*/ meltfptr[21])));;
    MELT_LOCATION ("warmelt-macro.melt:6620:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[3];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.ENV__V3*/ meltfptr[2];
      /*^apply.arg */
      argtab[1].meltbp_aptr =
	(melt_ptr_t *) & /*_.MEXPANDER__V4*/ meltfptr[3];
      /*^apply.arg */
      argtab[2].meltbp_aptr = (melt_ptr_t *) & /*_.MODCTX__V5*/ meltfptr[4];
      /*_.MEXPANDER__V31*/ meltfptr[30] =
	melt_apply ((meltclosure_ptr_t) ( /*_.MEXPANDER__V4*/ meltfptr[3]),
		    (melt_ptr_t) ( /*_.PAIR_HEAD__V30*/ meltfptr[29]),
		    (MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_PTR ""),
		    argtab, "", (union meltparam_un *) 0);
    }
    ;
    /*^compute */
    /*_.PATEXPV__V25*/ meltfptr[24] = /*_.SETQ___V32*/ meltfptr[31] =
      /*_.MEXPANDER__V31*/ meltfptr[30];;
    MELT_LOCATION ("warmelt-macro.melt:6621:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#NULL__L8*/ meltfnum[0] =
      (( /*_.PATEXPV__V25*/ meltfptr[24]) == NULL);;
    MELT_LOCATION ("warmelt-macro.melt:6621:/ cond");
    /*cond */ if ( /*_#NULL__L8*/ meltfnum[0])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{


	  {
	    MELT_LOCATION ("warmelt-macro.melt:6622:/ locexp");
	    /* error_plain */ melt_error_str ((melt_ptr_t) ( /*_.LOC__V20*/ meltfptr[19]), ("(EXPORT_PATMACRO <sym> <patexpander> <macexpander> [:doc <docum>])\
 expecting patexpander"), (melt_ptr_t) 0);
	  }
	  ;
	  /*epilog */
	}
	;
      }				/*noelse */
    ;
 /*_.PAIR_TAIL__V33*/ meltfptr[32] =
      (melt_pair_tail ((melt_ptr_t) ( /*_.CURPAIR__V22*/ meltfptr[21])));;
    MELT_LOCATION ("warmelt-macro.melt:6623:/ compute");
    /*_.CURPAIR__V22*/ meltfptr[21] = /*_.SETQ___V34*/ meltfptr[33] =
      /*_.PAIR_TAIL__V33*/ meltfptr[32];;
 /*_.PAIR_HEAD__V35*/ meltfptr[34] =
      (melt_pair_head ((melt_ptr_t) ( /*_.CURPAIR__V22*/ meltfptr[21])));;
    MELT_LOCATION ("warmelt-macro.melt:6624:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[3];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.ENV__V3*/ meltfptr[2];
      /*^apply.arg */
      argtab[1].meltbp_aptr =
	(melt_ptr_t *) & /*_.MEXPANDER__V4*/ meltfptr[3];
      /*^apply.arg */
      argtab[2].meltbp_aptr = (melt_ptr_t *) & /*_.MODCTX__V5*/ meltfptr[4];
      /*_.MEXPANDER__V36*/ meltfptr[35] =
	melt_apply ((meltclosure_ptr_t) ( /*_.MEXPANDER__V4*/ meltfptr[3]),
		    (melt_ptr_t) ( /*_.PAIR_HEAD__V35*/ meltfptr[34]),
		    (MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_PTR ""),
		    argtab, "", (union meltparam_un *) 0);
    }
    ;
    /*^compute */
    /*_.MACEXPV__V26*/ meltfptr[25] = /*_.SETQ___V37*/ meltfptr[36] =
      /*_.MEXPANDER__V36*/ meltfptr[35];;
    MELT_LOCATION ("warmelt-macro.melt:6625:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#NULL__L9*/ meltfnum[8] =
      (( /*_.MACEXPV__V26*/ meltfptr[25]) == NULL);;
    MELT_LOCATION ("warmelt-macro.melt:6625:/ cond");
    /*cond */ if ( /*_#NULL__L9*/ meltfnum[8])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{


	  {
	    MELT_LOCATION ("warmelt-macro.melt:6626:/ locexp");
	    /* error_plain */ melt_error_str ((melt_ptr_t) ( /*_.LOC__V20*/ meltfptr[19]), ("(EXPORT_PATMACRO <sym> <patexpander> <macexpander> [:doc <docum>])\
 expecting macexpander"), (melt_ptr_t) 0);
	  }
	  ;
	  /*epilog */
	}
	;
      }				/*noelse */
    ;
 /*_.PAIR_TAIL__V38*/ meltfptr[37] =
      (melt_pair_tail ((melt_ptr_t) ( /*_.CURPAIR__V22*/ meltfptr[21])));;
    MELT_LOCATION ("warmelt-macro.melt:6627:/ compute");
    /*_.CURPAIR__V22*/ meltfptr[21] = /*_.SETQ___V39*/ meltfptr[38] =
      /*_.PAIR_TAIL__V38*/ meltfptr[37];;
    MELT_LOCATION ("warmelt-macro.melt:6628:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_.PAIR_HEAD__V40*/ meltfptr[39] =
      (melt_pair_head ((melt_ptr_t) ( /*_.CURPAIR__V22*/ meltfptr[21])));;
    /*^compute */
 /*_#__L10*/ meltfnum[9] =
      (( /*_.PAIR_HEAD__V40*/ meltfptr[39]) ==
       (( /*!konst_4_DOC */ meltfrout->tabval[4])));;
    MELT_LOCATION ("warmelt-macro.melt:6628:/ cond");
    /*cond */ if ( /*_#__L10*/ meltfnum[9])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

   /*_.PAIR_TAIL__V42*/ meltfptr[41] =
	    (melt_pair_tail
	     ((melt_ptr_t) ( /*_.CURPAIR__V22*/ meltfptr[21])));;
	  MELT_LOCATION ("warmelt-macro.melt:6630:/ compute");
	  /*_.CURPAIR__V22*/ meltfptr[21] = /*_.SETQ___V43*/ meltfptr[42] =
	    /*_.PAIR_TAIL__V42*/ meltfptr[41];;
   /*_.PAIR_HEAD__V44*/ meltfptr[43] =
	    (melt_pair_head
	     ((melt_ptr_t) ( /*_.CURPAIR__V22*/ meltfptr[21])));;
	  MELT_LOCATION ("warmelt-macro.melt:6631:/ compute");
	  /*_.DOC__V27*/ meltfptr[26] = /*_.SETQ___V45*/ meltfptr[44] =
	    /*_.PAIR_HEAD__V44*/ meltfptr[43];;
	  MELT_LOCATION ("warmelt-macro.melt:6629:/ quasiblock");


	  /*_.PROGN___V46*/ meltfptr[45] = /*_.SETQ___V45*/ meltfptr[44];;
	  /*^compute */
	  /*_.IF___V41*/ meltfptr[40] = /*_.PROGN___V46*/ meltfptr[45];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-macro.melt:6628:/ clear");
	     /*clear *//*_.PAIR_TAIL__V42*/ meltfptr[41] = 0;
	  /*^clear */
	     /*clear *//*_.SETQ___V43*/ meltfptr[42] = 0;
	  /*^clear */
	     /*clear *//*_.PAIR_HEAD__V44*/ meltfptr[43] = 0;
	  /*^clear */
	     /*clear *//*_.SETQ___V45*/ meltfptr[44] = 0;
	  /*^clear */
	     /*clear *//*_.PROGN___V46*/ meltfptr[45] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

  /*_.IF___V41*/ meltfptr[40] = NULL;;
      }
    ;
    MELT_LOCATION ("warmelt-macro.melt:6634:/ quasiblock");


    /*^checksignal */
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*^rawallocobj */
    /*rawallocobj */
    {
      melt_ptr_t newobj = 0;
      melt_raw_object_create (newobj,
			      (melt_ptr_t) (( /*!CLASS_SOURCE_EXPORT_PATMACRO */ meltfrout->tabval[5])), (6), "CLASS_SOURCE_EXPORT_PATMACRO");
  /*_.INST__V49*/ meltfptr[43] =
	newobj;
    };
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @LOCA_LOCATION",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V49*/ meltfptr[43])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V49*/ meltfptr[43]), (1),
			  ( /*_.LOC__V20*/ meltfptr[19]), "LOCA_LOCATION");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @SEXPMAC_MNAME",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V49*/ meltfptr[43])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V49*/ meltfptr[43]), (2),
			  ( /*_.SYMB__V24*/ meltfptr[23]), "SEXPMAC_MNAME");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @SEXPPAT_PVAL",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V49*/ meltfptr[43])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V49*/ meltfptr[43]), (5),
			  ( /*_.PATEXPV__V25*/ meltfptr[24]), "SEXPPAT_PVAL");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @SEXPMAC_MVAL",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V49*/ meltfptr[43])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V49*/ meltfptr[43]), (3),
			  ( /*_.MACEXPV__V26*/ meltfptr[25]), "SEXPMAC_MVAL");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @SEXPMAC_DOC",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V49*/ meltfptr[43])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V49*/ meltfptr[43]), (4),
			  ( /*_.DOC__V27*/ meltfptr[26]), "SEXPMAC_DOC");
    ;
    /*^touchobj */

    melt_dbgtrace_written_object ( /*_.INST__V49*/ meltfptr[43],
				  "newly made instance");
    ;
    /*_.RES__V48*/ meltfptr[42] = /*_.INST__V49*/ meltfptr[43];;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-macro.melt:6642:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L11*/ meltfnum[10] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-macro.melt:6642:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L11*/ meltfnum[10])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L12*/ meltfnum[11] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-macro.melt:6642:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long =
		/*_#THE_MELTCALLCOUNT__L12*/ meltfnum[11];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-macro.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 6642;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "mexpand_export_patmacro result res";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.RES__V48*/ meltfptr[42];
	      /*_.MELT_DEBUG_FUN__V52*/ meltfptr[51] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V51*/ meltfptr[45] =
	      /*_.MELT_DEBUG_FUN__V52*/ meltfptr[51];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-macro.melt:6642:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L12*/ meltfnum[11] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V52*/ meltfptr[51] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V51*/ meltfptr[45] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-macro.melt:6642:/ quasiblock");


      /*_.PROGN___V53*/ meltfptr[51] = /*_.IF___V51*/ meltfptr[45];;
      /*^compute */
      /*_.IFCPP___V50*/ meltfptr[44] = /*_.PROGN___V53*/ meltfptr[51];;
      /*epilog */

      MELT_LOCATION ("warmelt-macro.melt:6642:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L11*/ meltfnum[10] = 0;
      /*^clear */
	     /*clear *//*_.IF___V51*/ meltfptr[45] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V53*/ meltfptr[51] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V50*/ meltfptr[44] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-macro.melt:6643:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.RES__V48*/ meltfptr[42];;

    {
      MELT_LOCATION ("warmelt-macro.melt:6643:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*_.LET___V47*/ meltfptr[41] = /*_.RETURN___V54*/ meltfptr[45];;

    MELT_LOCATION ("warmelt-macro.melt:6634:/ clear");
	   /*clear *//*_.RES__V48*/ meltfptr[42] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V50*/ meltfptr[44] = 0;
    /*^clear */
	   /*clear *//*_.RETURN___V54*/ meltfptr[45] = 0;
    /*_.LET___V18*/ meltfptr[16] = /*_.LET___V47*/ meltfptr[41];;

    MELT_LOCATION ("warmelt-macro.melt:6608:/ clear");
	   /*clear *//*_.CONT__V19*/ meltfptr[18] = 0;
    /*^clear */
	   /*clear *//*_.LOC__V20*/ meltfptr[19] = 0;
    /*^clear */
	   /*clear *//*_.LIST_FIRST__V21*/ meltfptr[20] = 0;
    /*^clear */
	   /*clear *//*_.CURPAIR__V22*/ meltfptr[21] = 0;
    /*^clear */
	   /*clear *//*_.PAIR_HEAD__V23*/ meltfptr[22] = 0;
    /*^clear */
	   /*clear *//*_.SYMB__V24*/ meltfptr[23] = 0;
    /*^clear */
	   /*clear *//*_.PATEXPV__V25*/ meltfptr[24] = 0;
    /*^clear */
	   /*clear *//*_.MACEXPV__V26*/ meltfptr[25] = 0;
    /*^clear */
	   /*clear *//*_.DOC__V27*/ meltfptr[26] = 0;
    /*^clear */
	   /*clear *//*_#IS_NOT_A__L7*/ meltfnum[1] = 0;
    /*^clear */
	   /*clear *//*_.PAIR_TAIL__V28*/ meltfptr[27] = 0;
    /*^clear */
	   /*clear *//*_.SETQ___V29*/ meltfptr[28] = 0;
    /*^clear */
	   /*clear *//*_.PAIR_HEAD__V30*/ meltfptr[29] = 0;
    /*^clear */
	   /*clear *//*_.MEXPANDER__V31*/ meltfptr[30] = 0;
    /*^clear */
	   /*clear *//*_.SETQ___V32*/ meltfptr[31] = 0;
    /*^clear */
	   /*clear *//*_#NULL__L8*/ meltfnum[0] = 0;
    /*^clear */
	   /*clear *//*_.PAIR_TAIL__V33*/ meltfptr[32] = 0;
    /*^clear */
	   /*clear *//*_.SETQ___V34*/ meltfptr[33] = 0;
    /*^clear */
	   /*clear *//*_.PAIR_HEAD__V35*/ meltfptr[34] = 0;
    /*^clear */
	   /*clear *//*_.MEXPANDER__V36*/ meltfptr[35] = 0;
    /*^clear */
	   /*clear *//*_.SETQ___V37*/ meltfptr[36] = 0;
    /*^clear */
	   /*clear *//*_#NULL__L9*/ meltfnum[8] = 0;
    /*^clear */
	   /*clear *//*_.PAIR_TAIL__V38*/ meltfptr[37] = 0;
    /*^clear */
	   /*clear *//*_.SETQ___V39*/ meltfptr[38] = 0;
    /*^clear */
	   /*clear *//*_.PAIR_HEAD__V40*/ meltfptr[39] = 0;
    /*^clear */
	   /*clear *//*_#__L10*/ meltfnum[9] = 0;
    /*^clear */
	   /*clear *//*_.IF___V41*/ meltfptr[40] = 0;
    /*^clear */
	   /*clear *//*_.LET___V47*/ meltfptr[41] = 0;
    MELT_LOCATION ("warmelt-macro.melt:6602:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.LET___V18*/ meltfptr[16];;

    {
      MELT_LOCATION ("warmelt-macro.melt:6602:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V6*/ meltfptr[5] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V10*/ meltfptr[6] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V12*/ meltfptr[7] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V14*/ meltfptr[12] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V16*/ meltfptr[14] = 0;
    /*^clear */
	   /*clear *//*_.LET___V18*/ meltfptr[16] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("MEXPAND_EXPORT_PATMACRO", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_141_warmelt_macro_MEXPAND_EXPORT_PATMACRO_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_141_warmelt_macro_MEXPAND_EXPORT_PATMACRO */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_142_warmelt_macro_MEXPAND_EXPORT_CLASS (meltclosure_ptr_t meltclosp_,
						 melt_ptr_t meltfirstargp_,
						 const melt_argdescr_cell_t
						 meltxargdescr_[],
						 union meltparam_un *
						 meltxargtab_,
						 const melt_argdescr_cell_t
						 meltxresdescr_[],
						 union meltparam_un *
						 meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_142_warmelt_macro_MEXPAND_EXPORT_CLASS_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_142_warmelt_macro_MEXPAND_EXPORT_CLASS_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 32
    melt_ptr_t mcfr_varptr[32];
#define MELTFRAM_NBVARNUM 8
    long mcfr_varnum[8];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_142_warmelt_macro_MEXPAND_EXPORT_CLASS is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_142_warmelt_macro_MEXPAND_EXPORT_CLASS_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 32; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_142_warmelt_macro_MEXPAND_EXPORT_CLASS nbval 32*/
  meltfram__.mcfr_nbvar = 32 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("MEXPAND_EXPORT_CLASS", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-macro.melt:6652:/ getarg");
 /*_.SEXPR__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.ENV__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.ENV__V3*/ meltfptr[2])) != NULL);


  /*getarg#2 */
  /*^getarg */
  if (meltxargdescr_[1] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.MEXPANDER__V4*/ meltfptr[3] =
    (meltxargtab_[1].meltbp_aptr) ? (*(meltxargtab_[1].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.MEXPANDER__V4*/ meltfptr[3])) !=
	      NULL);


  /*getarg#3 */
  /*^getarg */
  if (meltxargdescr_[2] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.MODCTX__V5*/ meltfptr[4] =
    (meltxargtab_[2].meltbp_aptr) ? (*(meltxargtab_[2].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.MODCTX__V5*/ meltfptr[4])) !=
	      NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-macro.melt:6653:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L1*/ meltfnum[0] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.SEXPR__V2*/ meltfptr[1]),
			     (melt_ptr_t) (( /*!CLASS_SEXPR */ meltfrout->
					    tabval[0])));;
      MELT_LOCATION ("warmelt-macro.melt:6653:/ cond");
      /*cond */ if ( /*_#IS_A__L1*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V7*/ meltfptr[6] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-macro.melt:6653:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check sexpr"),
				  ("warmelt-macro.melt")
				  ? ("warmelt-macro.melt") : __FILE__,
				  (6653) ? (6653) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V7*/ meltfptr[6] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V6*/ meltfptr[5] = /*_.IFELSE___V7*/ meltfptr[6];;
      /*epilog */

      MELT_LOCATION ("warmelt-macro.melt:6653:/ clear");
	     /*clear *//*_#IS_A__L1*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V7*/ meltfptr[6] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V6*/ meltfptr[5] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-macro.melt:6654:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L2*/ meltfnum[0] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.ENV__V3*/ meltfptr[2]),
			     (melt_ptr_t) (( /*!CLASS_ENVIRONMENT */
					    meltfrout->tabval[1])));;
      MELT_LOCATION ("warmelt-macro.melt:6654:/ cond");
      /*cond */ if ( /*_#IS_A__L2*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V9*/ meltfptr[8] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-macro.melt:6654:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check env"),
				  ("warmelt-macro.melt")
				  ? ("warmelt-macro.melt") : __FILE__,
				  (6654) ? (6654) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V9*/ meltfptr[8] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V8*/ meltfptr[6] = /*_.IFELSE___V9*/ meltfptr[8];;
      /*epilog */

      MELT_LOCATION ("warmelt-macro.melt:6654:/ clear");
	     /*clear *//*_#IS_A__L2*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V9*/ meltfptr[8] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V8*/ meltfptr[6] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-macro.melt:6655:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#NULL__L3*/ meltfnum[0] =
      (( /*_.MEXPANDER__V4*/ meltfptr[3]) == NULL);;
    MELT_LOCATION ("warmelt-macro.melt:6655:/ cond");
    /*cond */ if ( /*_#NULL__L3*/ meltfnum[0])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  /*^compute */
	  /*_.MEXPANDER__V4*/ meltfptr[3] = /*_.SETQ___V11*/ meltfptr[10] =
	    ( /*!MACROEXPAND_1 */ meltfrout->tabval[2]);;
	  /*_.IF___V10*/ meltfptr[8] = /*_.SETQ___V11*/ meltfptr[10];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-macro.melt:6655:/ clear");
	     /*clear *//*_.SETQ___V11*/ meltfptr[10] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

  /*_.IF___V10*/ meltfptr[8] = NULL;;
      }
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-macro.melt:6656:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_OBJECT__L4*/ meltfnum[3] =
	(melt_magic_discr ((melt_ptr_t) ( /*_.MODCTX__V5*/ meltfptr[4])) ==
	 MELTOBMAG_OBJECT);;
      MELT_LOCATION ("warmelt-macro.melt:6656:/ cond");
      /*cond */ if ( /*_#IS_OBJECT__L4*/ meltfnum[3])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V13*/ meltfptr[12] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-macro.melt:6656:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check modctx"),
				  ("warmelt-macro.melt")
				  ? ("warmelt-macro.melt") : __FILE__,
				  (6656) ? (6656) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V13*/ meltfptr[12] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V12*/ meltfptr[10] = /*_.IFELSE___V13*/ meltfptr[12];;
      /*epilog */

      MELT_LOCATION ("warmelt-macro.melt:6656:/ clear");
	     /*clear *//*_#IS_OBJECT__L4*/ meltfnum[3] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V13*/ meltfptr[12] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V12*/ meltfptr[10] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-macro.melt:6657:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L5*/ meltfnum[3] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-macro.melt:6657:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L5*/ meltfnum[3])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L6*/ meltfnum[5] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-macro.melt:6657:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L6*/ meltfnum[5];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-macro.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 6657;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "mexpand_export_class sexpr";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.SEXPR__V2*/ meltfptr[1];
	      /*_.MELT_DEBUG_FUN__V16*/ meltfptr[15] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[3])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V15*/ meltfptr[14] =
	      /*_.MELT_DEBUG_FUN__V16*/ meltfptr[15];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-macro.melt:6657:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L6*/ meltfnum[5] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V16*/ meltfptr[15] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V15*/ meltfptr[14] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-macro.melt:6657:/ quasiblock");


      /*_.PROGN___V17*/ meltfptr[15] = /*_.IF___V15*/ meltfptr[14];;
      /*^compute */
      /*_.IFCPP___V14*/ meltfptr[12] = /*_.PROGN___V17*/ meltfptr[15];;
      /*epilog */

      MELT_LOCATION ("warmelt-macro.melt:6657:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L5*/ meltfnum[3] = 0;
      /*^clear */
	     /*clear *//*_.IF___V15*/ meltfptr[14] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V17*/ meltfptr[15] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V14*/ meltfptr[12] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-macro.melt:6658:/ quasiblock");


    MELT_LOCATION ("warmelt-macro.melt:6659:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.SEXPR__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 2, "SEXP_CONTENTS");
  /*_.CONT__V19*/ meltfptr[15] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-macro.melt:6660:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.SEXPR__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 1, "LOCA_LOCATION");
  /*_.LOC__V20*/ meltfptr[19] = slot;
    };
    ;
 /*_.LIST_FIRST__V21*/ meltfptr[20] =
      (melt_list_first ((melt_ptr_t) ( /*_.CONT__V19*/ meltfptr[15])));;
    /*^compute */
 /*_.CURPAIR__V22*/ meltfptr[21] =
      (melt_pair_tail ((melt_ptr_t) ( /*_.LIST_FIRST__V21*/ meltfptr[20])));;
    MELT_LOCATION ("warmelt-macro.melt:6664:/ quasiblock");


    /*^newclosure */
		 /*newclosure *//*_.LAMBDA___V24*/ meltfptr[23] =
      (melt_ptr_t)
      meltgc_new_closure ((meltobject_ptr_t)
			  (((melt_ptr_t) (MELT_PREDEF (DISCR_CLOSURE)))),
			  (meltroutine_ptr_t) (( /*!konst_7 */ meltfrout->
						tabval[7])), (4));
    ;
    /*^putclosedv */
    /*putclosv */
    melt_assertmsg ("putclosv checkclo",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.LAMBDA___V24*/ meltfptr[23])) ==
		    MELTOBMAG_CLOSURE);
    melt_assertmsg ("putclosv checkoff", 0 >= 0
		    && 0 <
		    melt_closure_size ((melt_ptr_t)
				       ( /*_.LAMBDA___V24*/ meltfptr[23])));
    ((meltclosure_ptr_t) /*_.LAMBDA___V24*/ meltfptr[23])->tabval[0] =
      (melt_ptr_t) ( /*_.MEXPANDER__V4*/ meltfptr[3]);
    ;
    /*^putclosedv */
    /*putclosv */
    melt_assertmsg ("putclosv checkclo",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.LAMBDA___V24*/ meltfptr[23])) ==
		    MELTOBMAG_CLOSURE);
    melt_assertmsg ("putclosv checkoff", 1 >= 0
		    && 1 <
		    melt_closure_size ((melt_ptr_t)
				       ( /*_.LAMBDA___V24*/ meltfptr[23])));
    ((meltclosure_ptr_t) /*_.LAMBDA___V24*/ meltfptr[23])->tabval[1] =
      (melt_ptr_t) ( /*_.ENV__V3*/ meltfptr[2]);
    ;
    /*^putclosedv */
    /*putclosv */
    melt_assertmsg ("putclosv checkclo",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.LAMBDA___V24*/ meltfptr[23])) ==
		    MELTOBMAG_CLOSURE);
    melt_assertmsg ("putclosv checkoff", 2 >= 0
		    && 2 <
		    melt_closure_size ((melt_ptr_t)
				       ( /*_.LAMBDA___V24*/ meltfptr[23])));
    ((meltclosure_ptr_t) /*_.LAMBDA___V24*/ meltfptr[23])->tabval[2] =
      (melt_ptr_t) ( /*_.MODCTX__V5*/ meltfptr[4]);
    ;
    /*^putclosedv */
    /*putclosv */
    melt_assertmsg ("putclosv checkclo",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.LAMBDA___V24*/ meltfptr[23])) ==
		    MELTOBMAG_CLOSURE);
    melt_assertmsg ("putclosv checkoff", 3 >= 0
		    && 3 <
		    melt_closure_size ((melt_ptr_t)
				       ( /*_.LAMBDA___V24*/ meltfptr[23])));
    ((meltclosure_ptr_t) /*_.LAMBDA___V24*/ meltfptr[23])->tabval[3] =
      (melt_ptr_t) ( /*_.LOC__V20*/ meltfptr[19]);
    ;
    /*_.LAMBDA___V23*/ meltfptr[22] = /*_.LAMBDA___V24*/ meltfptr[23];;
    MELT_LOCATION ("warmelt-macro.melt:6662:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[2];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & ( /*!DISCR_MULTIPLE */ meltfrout->tabval[5]);
      /*^apply.arg */
      argtab[1].meltbp_aptr =
	(melt_ptr_t *) & /*_.LAMBDA___V23*/ meltfptr[22];
      /*_.SYMBTUP__V25*/ meltfptr[24] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!PAIRLIST_TO_MULTIPLE */ meltfrout->tabval[4])),
		    (melt_ptr_t) ( /*_.CURPAIR__V22*/ meltfptr[21]),
		    (MELTBPARSTR_PTR MELTBPARSTR_PTR ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-macro.melt:6670:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*^rawallocobj */
    /*rawallocobj */
    {
      melt_ptr_t newobj = 0;
      melt_raw_object_create (newobj,
			      (melt_ptr_t) (( /*!CLASS_SOURCE_EXPORT_CLASS */
					     meltfrout->tabval[8])), (3),
			      "CLASS_SOURCE_EXPORT_CLASS");
  /*_.INST__V27*/ meltfptr[26] =
	newobj;
    };
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @LOCA_LOCATION",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V27*/ meltfptr[26])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V27*/ meltfptr[26]), (1),
			  ( /*_.LOC__V20*/ meltfptr[19]), "LOCA_LOCATION");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @SEXPORT_NAMES",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V27*/ meltfptr[26])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V27*/ meltfptr[26]), (2),
			  ( /*_.SYMBTUP__V25*/ meltfptr[24]),
			  "SEXPORT_NAMES");
    ;
    /*^touchobj */

    melt_dbgtrace_written_object ( /*_.INST__V27*/ meltfptr[26],
				  "newly made instance");
    ;
    /*_.RES__V26*/ meltfptr[25] = /*_.INST__V27*/ meltfptr[26];;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-macro.melt:6674:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L7*/ meltfnum[5] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-macro.melt:6674:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L7*/ meltfnum[5])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L8*/ meltfnum[3] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-macro.melt:6674:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L8*/ meltfnum[3];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-macro.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 6674;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "mexpand_export_class result res";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.RES__V26*/ meltfptr[25];
	      /*_.MELT_DEBUG_FUN__V30*/ meltfptr[29] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[3])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V29*/ meltfptr[28] =
	      /*_.MELT_DEBUG_FUN__V30*/ meltfptr[29];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-macro.melt:6674:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L8*/ meltfnum[3] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V30*/ meltfptr[29] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V29*/ meltfptr[28] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-macro.melt:6674:/ quasiblock");


      /*_.PROGN___V31*/ meltfptr[29] = /*_.IF___V29*/ meltfptr[28];;
      /*^compute */
      /*_.IFCPP___V28*/ meltfptr[27] = /*_.PROGN___V31*/ meltfptr[29];;
      /*epilog */

      MELT_LOCATION ("warmelt-macro.melt:6674:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L7*/ meltfnum[5] = 0;
      /*^clear */
	     /*clear *//*_.IF___V29*/ meltfptr[28] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V31*/ meltfptr[29] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V28*/ meltfptr[27] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-macro.melt:6675:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.RES__V26*/ meltfptr[25];;

    {
      MELT_LOCATION ("warmelt-macro.melt:6675:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*_.LET___V18*/ meltfptr[14] = /*_.RETURN___V32*/ meltfptr[28];;

    MELT_LOCATION ("warmelt-macro.melt:6658:/ clear");
	   /*clear *//*_.CONT__V19*/ meltfptr[15] = 0;
    /*^clear */
	   /*clear *//*_.LOC__V20*/ meltfptr[19] = 0;
    /*^clear */
	   /*clear *//*_.LIST_FIRST__V21*/ meltfptr[20] = 0;
    /*^clear */
	   /*clear *//*_.CURPAIR__V22*/ meltfptr[21] = 0;
    /*^clear */
	   /*clear *//*_.LAMBDA___V23*/ meltfptr[22] = 0;
    /*^clear */
	   /*clear *//*_.SYMBTUP__V25*/ meltfptr[24] = 0;
    /*^clear */
	   /*clear *//*_.RES__V26*/ meltfptr[25] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V28*/ meltfptr[27] = 0;
    /*^clear */
	   /*clear *//*_.RETURN___V32*/ meltfptr[28] = 0;
    MELT_LOCATION ("warmelt-macro.melt:6652:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.LET___V18*/ meltfptr[14];;

    {
      MELT_LOCATION ("warmelt-macro.melt:6652:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V6*/ meltfptr[5] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V8*/ meltfptr[6] = 0;
    /*^clear */
	   /*clear *//*_#NULL__L3*/ meltfnum[0] = 0;
    /*^clear */
	   /*clear *//*_.IF___V10*/ meltfptr[8] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V12*/ meltfptr[10] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V14*/ meltfptr[12] = 0;
    /*^clear */
	   /*clear *//*_.LET___V18*/ meltfptr[14] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("MEXPAND_EXPORT_CLASS", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_142_warmelt_macro_MEXPAND_EXPORT_CLASS_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_142_warmelt_macro_MEXPAND_EXPORT_CLASS */



/**** end of warmelt-macro+04.c ****/
