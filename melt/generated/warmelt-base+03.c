/* GCC MELT GENERATED FILE warmelt-base+03.c - DO NOT EDIT */
/* secondary MELT generated C file of rank #3 */
#include "melt-run.h"


/* used hash from melt-run.h when compiling this file: */
MELT_EXTERN const char meltrun_used_md5_melt_f3[] =
  MELT_RUN_HASHMD5 /* from melt-run.h */ ;


/**** warmelt-base+03.c declarations ****/


/***************************************************
***
    Copyright 2008 - 2012 Free Software Foundation, Inc.
    Contributed by Basile Starynkevitch <basile@starynkevitch.net>
               and Pierre Vittet  <piervit@pvittet.com>

    This file is part of GCC.

    GCC is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3, or (at your option)
    any later version.

    GCC is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with GCC; see the file COPYING3.  If not see
    <http://www.gnu.org/licenses/>.
***
****************************************************/

/* ordinary MELT module */
#define MELT_HAS_INITIAL_ENVIRONMENT 1	/*usual */

struct melt_callframe_st;	/*defined in melt-runtime.h */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_1_warmelt_base_IV (meltclosure_ptr_t meltclosp_,
			    melt_ptr_t meltfirstargp_,
			    const melt_argdescr_cell_t meltxargdescr_[],
			    union meltparam_un *meltxargtab_,
			    const melt_argdescr_cell_t meltxresdescr_[],
			    union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_2_warmelt_base_IV (meltclosure_ptr_t meltclosp_,
			    melt_ptr_t meltfirstargp_,
			    const melt_argdescr_cell_t meltxargdescr_[],
			    union meltparam_un *meltxargtab_,
			    const melt_argdescr_cell_t meltxresdescr_[],
			    union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_3_warmelt_base_IV (meltclosure_ptr_t meltclosp_,
			    melt_ptr_t meltfirstargp_,
			    const melt_argdescr_cell_t meltxargdescr_[],
			    union meltparam_un *meltxargtab_,
			    const melt_argdescr_cell_t meltxresdescr_[],
			    union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_4_warmelt_base_IV (meltclosure_ptr_t meltclosp_,
			    melt_ptr_t meltfirstargp_,
			    const melt_argdescr_cell_t meltxargdescr_[],
			    union meltparam_un *meltxargtab_,
			    const melt_argdescr_cell_t meltxresdescr_[],
			    union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_5_warmelt_base_IV (meltclosure_ptr_t meltclosp_,
			    melt_ptr_t meltfirstargp_,
			    const melt_argdescr_cell_t meltxargdescr_[],
			    union meltparam_un *meltxargtab_,
			    const melt_argdescr_cell_t meltxresdescr_[],
			    union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_6_warmelt_base_COMPARE_NAMED_ALPHA (meltclosure_ptr_t meltclosp_,
					     melt_ptr_t meltfirstargp_,
					     const melt_argdescr_cell_t
					     meltxargdescr_[],
					     union meltparam_un *meltxargtab_,
					     const melt_argdescr_cell_t
					     meltxresdescr_[],
					     union meltparam_un
					     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_7_warmelt_base_ADD2OUT (meltclosure_ptr_t meltclosp_,
				 melt_ptr_t meltfirstargp_,
				 const melt_argdescr_cell_t meltxargdescr_[],
				 union meltparam_un *meltxargtab_,
				 const melt_argdescr_cell_t meltxresdescr_[],
				 union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_8_warmelt_base_STRING4OUT (meltclosure_ptr_t meltclosp_,
				    melt_ptr_t meltfirstargp_,
				    const melt_argdescr_cell_t
				    meltxargdescr_[],
				    union meltparam_un *meltxargtab_,
				    const melt_argdescr_cell_t
				    meltxresdescr_[],
				    union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_9_warmelt_base_ADD2OUT4NULL (meltclosure_ptr_t meltclosp_,
				      melt_ptr_t meltfirstargp_,
				      const melt_argdescr_cell_t
				      meltxargdescr_[],
				      union meltparam_un *meltxargtab_,
				      const melt_argdescr_cell_t
				      meltxresdescr_[],
				      union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_10_warmelt_base_ADD2OUT4INTEGER (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_11_warmelt_base_ADD2OUT4STRING (meltclosure_ptr_t meltclosp_,
					 melt_ptr_t meltfirstargp_,
					 const melt_argdescr_cell_t
					 meltxargdescr_[],
					 union meltparam_un *meltxargtab_,
					 const melt_argdescr_cell_t
					 meltxresdescr_[],
					 union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_12_warmelt_base_ADD2OUT4STRBUF (meltclosure_ptr_t meltclosp_,
					 melt_ptr_t meltfirstargp_,
					 const melt_argdescr_cell_t
					 meltxargdescr_[],
					 union meltparam_un *meltxargtab_,
					 const melt_argdescr_cell_t
					 meltxresdescr_[],
					 union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_13_warmelt_base_ADD2OUT4CLONEDSYMB (meltclosure_ptr_t meltclosp_,
					     melt_ptr_t meltfirstargp_,
					     const melt_argdescr_cell_t
					     meltxargdescr_[],
					     union meltparam_un *meltxargtab_,
					     const melt_argdescr_cell_t
					     meltxresdescr_[],
					     union meltparam_un
					     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_14_warmelt_base_ADD2OUT4NAMED (meltclosure_ptr_t meltclosp_,
					melt_ptr_t meltfirstargp_,
					const melt_argdescr_cell_t
					meltxargdescr_[],
					union meltparam_un *meltxargtab_,
					const melt_argdescr_cell_t
					meltxresdescr_[],
					union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_15_warmelt_base_ADD2OUT4ROOTOBJECT (meltclosure_ptr_t meltclosp_,
					     melt_ptr_t meltfirstargp_,
					     const melt_argdescr_cell_t
					     meltxargdescr_[],
					     union meltparam_un *meltxargtab_,
					     const melt_argdescr_cell_t
					     meltxresdescr_[],
					     union meltparam_un
					     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_16_warmelt_base_ADD2OUT4ANY (meltclosure_ptr_t meltclosp_,
				      melt_ptr_t meltfirstargp_,
				      const melt_argdescr_cell_t
				      meltxargdescr_[],
				      union meltparam_un *meltxargtab_,
				      const melt_argdescr_cell_t
				      meltxresdescr_[],
				      union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_17_warmelt_base_MAPSTRING_EVERY (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_18_warmelt_base_MAPSTRING_ITERATE_TEST (meltclosure_ptr_t meltclosp_,
						 melt_ptr_t meltfirstargp_,
						 const melt_argdescr_cell_t
						 meltxargdescr_[],
						 union meltparam_un
						 *meltxargtab_,
						 const melt_argdescr_cell_t
						 meltxresdescr_[],
						 union meltparam_un
						 *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_19_warmelt_base_MULTIPLE_EVERY (meltclosure_ptr_t meltclosp_,
					 melt_ptr_t meltfirstargp_,
					 const melt_argdescr_cell_t
					 meltxargdescr_[],
					 union meltparam_un *meltxargtab_,
					 const melt_argdescr_cell_t
					 meltxresdescr_[],
					 union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_20_warmelt_base_MULTIPLE_BACKWARD_EVERY (meltclosure_ptr_t
						  meltclosp_,
						  melt_ptr_t meltfirstargp_,
						  const melt_argdescr_cell_t
						  meltxargdescr_[],
						  union meltparam_un
						  *meltxargtab_,
						  const melt_argdescr_cell_t
						  meltxresdescr_[],
						  union meltparam_un
						  *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_21_warmelt_base_MULTIPLE_EVERY_BOTH (meltclosure_ptr_t meltclosp_,
					      melt_ptr_t meltfirstargp_,
					      const melt_argdescr_cell_t
					      meltxargdescr_[],
					      union meltparam_un
					      *meltxargtab_,
					      const melt_argdescr_cell_t
					      meltxresdescr_[],
					      union meltparam_un
					      *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_22_warmelt_base_RUN_ALL_PASS_EXECUTION_HOOKS (meltclosure_ptr_t
						       meltclosp_,
						       melt_ptr_t
						       meltfirstargp_,
						       const
						       melt_argdescr_cell_t
						       meltxargdescr_[],
						       union meltparam_un
						       *meltxargtab_,
						       const
						       melt_argdescr_cell_t
						       meltxresdescr_[],
						       union meltparam_un
						       *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_23_warmelt_base_REGISTER_PASS_EXECUTION_HOOK (meltclosure_ptr_t
						       meltclosp_,
						       melt_ptr_t
						       meltfirstargp_,
						       const
						       melt_argdescr_cell_t
						       meltxargdescr_[],
						       union meltparam_un
						       *meltxargtab_,
						       const
						       melt_argdescr_cell_t
						       meltxresdescr_[],
						       union meltparam_un
						       *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_24_warmelt_base_LAMBDA___1__ (meltclosure_ptr_t meltclosp_,
				       melt_ptr_t meltfirstargp_,
				       const melt_argdescr_cell_t
				       meltxargdescr_[],
				       union meltparam_un *meltxargtab_,
				       const melt_argdescr_cell_t
				       meltxresdescr_[],
				       union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_25_warmelt_base_UNREGISTER_PASS_EXECUTION_HOOK (meltclosure_ptr_t
							 meltclosp_,
							 melt_ptr_t
							 meltfirstargp_,
							 const
							 melt_argdescr_cell_t
							 meltxargdescr_[],
							 union meltparam_un
							 *meltxargtab_,
							 const
							 melt_argdescr_cell_t
							 meltxresdescr_[],
							 union meltparam_un
							 *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_26_warmelt_base_LAMBDA___2__ (meltclosure_ptr_t meltclosp_,
				       melt_ptr_t meltfirstargp_,
				       const melt_argdescr_cell_t
				       meltxargdescr_[],
				       union meltparam_un *meltxargtab_,
				       const melt_argdescr_cell_t
				       meltxresdescr_[],
				       union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_27_warmelt_base_REGISTER_PRAGMA_HANDLER (meltclosure_ptr_t
						  meltclosp_,
						  melt_ptr_t meltfirstargp_,
						  const melt_argdescr_cell_t
						  meltxargdescr_[],
						  union meltparam_un
						  *meltxargtab_,
						  const melt_argdescr_cell_t
						  meltxresdescr_[],
						  union meltparam_un
						  *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_28_warmelt_base_REGISTER_PRE_GENERICIZE_HOOK_FIRST (meltclosure_ptr_t
							     meltclosp_,
							     melt_ptr_t
							     meltfirstargp_,
							     const
							     melt_argdescr_cell_t
							     meltxargdescr_[],
							     union
							     meltparam_un
							     *meltxargtab_,
							     const
							     melt_argdescr_cell_t
							     meltxresdescr_[],
							     union
							     meltparam_un
							     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_29_warmelt_base_REGISTER_PRE_GENERICIZE_HOOK_LAST (meltclosure_ptr_t
							    meltclosp_,
							    melt_ptr_t
							    meltfirstargp_,
							    const
							    melt_argdescr_cell_t
							    meltxargdescr_[],
							    union meltparam_un
							    *meltxargtab_,
							    const
							    melt_argdescr_cell_t
							    meltxresdescr_[],
							    union meltparam_un
							    *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_30_warmelt_base_PRE_GENERICIZE_HOOK_RUNNER (meltclosure_ptr_t
						     meltclosp_,
						     melt_ptr_t
						     meltfirstargp_,
						     const
						     melt_argdescr_cell_t
						     meltxargdescr_[],
						     union meltparam_un
						     *meltxargtab_,
						     const
						     melt_argdescr_cell_t
						     meltxresdescr_[],
						     union meltparam_un
						     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_31_warmelt_base_MELT_FINISH_TYPE_RUNNER (meltclosure_ptr_t
						  meltclosp_,
						  melt_ptr_t meltfirstargp_,
						  const melt_argdescr_cell_t
						  meltxargdescr_[],
						  union meltparam_un
						  *meltxargtab_,
						  const melt_argdescr_cell_t
						  meltxresdescr_[],
						  union meltparam_un
						  *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_32_warmelt_base_REGISTER_FINISH_TYPE_HOOK_FIRST (meltclosure_ptr_t
							  meltclosp_,
							  melt_ptr_t
							  meltfirstargp_,
							  const
							  melt_argdescr_cell_t
							  meltxargdescr_[],
							  union meltparam_un
							  *meltxargtab_,
							  const
							  melt_argdescr_cell_t
							  meltxresdescr_[],
							  union meltparam_un
							  *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_33_warmelt_base_REGISTER_FINISH_TYPE_HOOK_LAST (meltclosure_ptr_t
							 meltclosp_,
							 melt_ptr_t
							 meltfirstargp_,
							 const
							 melt_argdescr_cell_t
							 meltxargdescr_[],
							 union meltparam_un
							 *meltxargtab_,
							 const
							 melt_argdescr_cell_t
							 meltxresdescr_[],
							 union meltparam_un
							 *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_34_warmelt_base_MELT_FINISH_DECL_RUNNER (meltclosure_ptr_t
						  meltclosp_,
						  melt_ptr_t meltfirstargp_,
						  const melt_argdescr_cell_t
						  meltxargdescr_[],
						  union meltparam_un
						  *meltxargtab_,
						  const melt_argdescr_cell_t
						  meltxresdescr_[],
						  union meltparam_un
						  *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_35_warmelt_base_REGISTER_FINISH_DECL_HOOK_FIRST (meltclosure_ptr_t
							  meltclosp_,
							  melt_ptr_t
							  meltfirstargp_,
							  const
							  melt_argdescr_cell_t
							  meltxargdescr_[],
							  union meltparam_un
							  *meltxargtab_,
							  const
							  melt_argdescr_cell_t
							  meltxresdescr_[],
							  union meltparam_un
							  *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_36_warmelt_base_REGISTER_FINISH_DECL_HOOK_LAST (meltclosure_ptr_t
							 meltclosp_,
							 melt_ptr_t
							 meltfirstargp_,
							 const
							 melt_argdescr_cell_t
							 meltxargdescr_[],
							 union meltparam_un
							 *meltxargtab_,
							 const
							 melt_argdescr_cell_t
							 meltxresdescr_[],
							 union meltparam_un
							 *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_37_warmelt_base_MELT_ALL_PASSES_START_RUNNER (meltclosure_ptr_t
						       meltclosp_,
						       melt_ptr_t
						       meltfirstargp_,
						       const
						       melt_argdescr_cell_t
						       meltxargdescr_[],
						       union meltparam_un
						       *meltxargtab_,
						       const
						       melt_argdescr_cell_t
						       meltxresdescr_[],
						       union meltparam_un
						       *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_38_warmelt_base_REGISTER_ALL_PASSES_START_HOOK_FIRST
(meltclosure_ptr_t meltclosp_, melt_ptr_t meltfirstargp_, const melt_argdescr_cell_t meltxargdescr_[],
union meltparam_un *meltxargtab_, const melt_argdescr_cell_t meltxresdescr_[],
union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_39_warmelt_base_REGISTER_ALL_PASSES_START_HOOK_LAST
(meltclosure_ptr_t meltclosp_, melt_ptr_t meltfirstargp_, const melt_argdescr_cell_t meltxargdescr_[],
union meltparam_un *meltxargtab_, const melt_argdescr_cell_t meltxresdescr_[],
union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_40_warmelt_base_MELT_ALL_PASSES_END_RUNNER (meltclosure_ptr_t
						     meltclosp_,
						     melt_ptr_t
						     meltfirstargp_,
						     const
						     melt_argdescr_cell_t
						     meltxargdescr_[],
						     union meltparam_un
						     *meltxargtab_,
						     const
						     melt_argdescr_cell_t
						     meltxresdescr_[],
						     union meltparam_un
						     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_41_warmelt_base_REGISTER_ALL_PASSES_END_HOOK_FIRST (meltclosure_ptr_t
							     meltclosp_,
							     melt_ptr_t
							     meltfirstargp_,
							     const
							     melt_argdescr_cell_t
							     meltxargdescr_[],
							     union
							     meltparam_un
							     *meltxargtab_,
							     const
							     melt_argdescr_cell_t
							     meltxresdescr_[],
							     union
							     meltparam_un
							     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_42_warmelt_base_REGISTER_ALL_PASSES_END_HOOK_LAST (meltclosure_ptr_t
							    meltclosp_,
							    melt_ptr_t
							    meltfirstargp_,
							    const
							    melt_argdescr_cell_t
							    meltxargdescr_[],
							    union meltparam_un
							    *meltxargtab_,
							    const
							    melt_argdescr_cell_t
							    meltxresdescr_[],
							    union meltparam_un
							    *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_43_warmelt_base_MELT_ALL_IPA_PASSES_START_RUNNER (meltclosure_ptr_t
							   meltclosp_,
							   melt_ptr_t
							   meltfirstargp_,
							   const
							   melt_argdescr_cell_t
							   meltxargdescr_[],
							   union meltparam_un
							   *meltxargtab_,
							   const
							   melt_argdescr_cell_t
							   meltxresdescr_[],
							   union meltparam_un
							   *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_44_warmelt_base_REGISTER_ALL_IPA_PASSES_START_HOOK_FIRST
(meltclosure_ptr_t meltclosp_, melt_ptr_t meltfirstargp_, const melt_argdescr_cell_t meltxargdescr_[],
union meltparam_un *meltxargtab_, const melt_argdescr_cell_t meltxresdescr_[],
union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_45_warmelt_base_REGISTER_ALL_IPA_PASSES_START_HOOK_LAST
(meltclosure_ptr_t meltclosp_, melt_ptr_t meltfirstargp_, const melt_argdescr_cell_t meltxargdescr_[],
union meltparam_un *meltxargtab_, const melt_argdescr_cell_t meltxresdescr_[],
union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_46_warmelt_base_MELT_ALL_IPA_PASSES_END_RUNNER (meltclosure_ptr_t
							 meltclosp_,
							 melt_ptr_t
							 meltfirstargp_,
							 const
							 melt_argdescr_cell_t
							 meltxargdescr_[],
							 union meltparam_un
							 *meltxargtab_,
							 const
							 melt_argdescr_cell_t
							 meltxresdescr_[],
							 union meltparam_un
							 *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_47_warmelt_base_REGISTER_ALL_IPA_PASSES_END_HOOK_FIRST
(meltclosure_ptr_t meltclosp_, melt_ptr_t meltfirstargp_, const melt_argdescr_cell_t meltxargdescr_[],
union meltparam_un *meltxargtab_, const melt_argdescr_cell_t meltxresdescr_[],
union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_48_warmelt_base_REGISTER_ALL_IPA_PASSES_END_HOOK_LAST
(meltclosure_ptr_t meltclosp_, melt_ptr_t meltfirstargp_, const melt_argdescr_cell_t meltxargdescr_[],
union meltparam_un *meltxargtab_, const melt_argdescr_cell_t meltxresdescr_[],
union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_49_warmelt_base_MELT_EARLY_GIMPLE_PASSES_START_RUNNER
(meltclosure_ptr_t meltclosp_, melt_ptr_t meltfirstargp_, const melt_argdescr_cell_t meltxargdescr_[],
union meltparam_un *meltxargtab_, const melt_argdescr_cell_t meltxresdescr_[],
union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_50_warmelt_base_REGISTER_EARLY_GIMPLE_PASSES_START_HOOK_FIRST
(meltclosure_ptr_t meltclosp_, melt_ptr_t meltfirstargp_, const melt_argdescr_cell_t meltxargdescr_[],
union meltparam_un *meltxargtab_, const melt_argdescr_cell_t meltxresdescr_[],
union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_51_warmelt_base_REGISTER_EARLY_GIMPLE_PASSES_START_HOOK_LAST
(meltclosure_ptr_t meltclosp_, melt_ptr_t meltfirstargp_, const melt_argdescr_cell_t meltxargdescr_[],
union meltparam_un *meltxargtab_, const melt_argdescr_cell_t meltxresdescr_[],
union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_52_warmelt_base_MELT_EARLY_GIMPLE_PASSES_END_RUNNER
(meltclosure_ptr_t meltclosp_, melt_ptr_t meltfirstargp_, const melt_argdescr_cell_t meltxargdescr_[],
union meltparam_un *meltxargtab_, const melt_argdescr_cell_t meltxresdescr_[],
union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_53_warmelt_base_REGISTER_EARLY_GIMPLE_PASSES_END_HOOK_FIRST
(meltclosure_ptr_t meltclosp_, melt_ptr_t meltfirstargp_, const melt_argdescr_cell_t meltxargdescr_[],
union meltparam_un *meltxargtab_, const melt_argdescr_cell_t meltxresdescr_[],
union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_54_warmelt_base_REGISTER_EARLY_GIMPLE_PASSES_END_HOOK_LAST
(meltclosure_ptr_t meltclosp_, melt_ptr_t meltfirstargp_, const melt_argdescr_cell_t meltxargdescr_[],
union meltparam_un *meltxargtab_, const melt_argdescr_cell_t meltxresdescr_[],
union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_55_warmelt_base_SET_REFERENCE (meltclosure_ptr_t meltclosp_,
					melt_ptr_t meltfirstargp_,
					const melt_argdescr_cell_t
					meltxargdescr_[],
					union meltparam_un *meltxargtab_,
					const melt_argdescr_cell_t
					meltxresdescr_[],
					union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_56_warmelt_base_INSTALL_VALUE_DESCRIPTOR (meltclosure_ptr_t
						   meltclosp_,
						   melt_ptr_t meltfirstargp_,
						   const melt_argdescr_cell_t
						   meltxargdescr_[],
						   union meltparam_un
						   *meltxargtab_,
						   const melt_argdescr_cell_t
						   meltxresdescr_[],
						   union meltparam_un
						   *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_57_warmelt_base_RETRIEVE_VALUE_DESCRIPTOR_LIST (meltclosure_ptr_t
							 meltclosp_,
							 melt_ptr_t
							 meltfirstargp_,
							 const
							 melt_argdescr_cell_t
							 meltxargdescr_[],
							 union meltparam_un
							 *meltxargtab_,
							 const
							 melt_argdescr_cell_t
							 meltxresdescr_[],
							 union meltparam_un
							 *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_58_warmelt_base_LIST_REMOVE_LAST_ELEMENT (meltclosure_ptr_t
						   meltclosp_,
						   melt_ptr_t meltfirstargp_,
						   const melt_argdescr_cell_t
						   meltxargdescr_[],
						   union meltparam_un
						   *meltxargtab_,
						   const melt_argdescr_cell_t
						   meltxresdescr_[],
						   union meltparam_un
						   *meltxrestab_);



MELT_EXTERN void *melt_start_this_module (void *);


/* define different names when debugging or not */
#if MELT_HAVE_DEBUG
MELT_EXTERN const char meltmodule_warmelt_base__melt_have_debug_enabled[];
#define melt_have_debug_string meltmodule_warmelt_base__melt_have_debug_enabled
#else /*!MELT_HAVE_DEBUG */
MELT_EXTERN const char meltmodule_warmelt_base__melt_have_debug_disabled[];
#define melt_have_debug_string meltmodule_warmelt_base__melt_have_debug_disabled
#endif /*!MELT_HAVE_DEBUG */


struct frame_melt_start_this_module_st;
void MELT_MODULE_VISIBILITY meltmod__warmelt_base__initialmeltchunk_0 (struct
								       frame_melt_start_this_module_st
								       *,
								       char
								       *);
void MELT_MODULE_VISIBILITY meltmod__warmelt_base__initialmeltchunk_1 (struct
								       frame_melt_start_this_module_st
								       *,
								       char
								       *);
void MELT_MODULE_VISIBILITY meltmod__warmelt_base__initialmeltchunk_2 (struct
								       frame_melt_start_this_module_st
								       *,
								       char
								       *);
void MELT_MODULE_VISIBILITY meltmod__warmelt_base__initialmeltchunk_3 (struct
								       frame_melt_start_this_module_st
								       *,
								       char
								       *);
void MELT_MODULE_VISIBILITY meltmod__warmelt_base__initialmeltchunk_4 (struct
								       frame_melt_start_this_module_st
								       *,
								       char
								       *);
void MELT_MODULE_VISIBILITY meltmod__warmelt_base__initialmeltchunk_5 (struct
								       frame_melt_start_this_module_st
								       *,
								       char
								       *);
void MELT_MODULE_VISIBILITY meltmod__warmelt_base__initialmeltchunk_6 (struct
								       frame_melt_start_this_module_st
								       *,
								       char
								       *);
void MELT_MODULE_VISIBILITY meltmod__warmelt_base__initialmeltchunk_7 (struct
								       frame_melt_start_this_module_st
								       *,
								       char
								       *);
void MELT_MODULE_VISIBILITY meltmod__warmelt_base__initialmeltchunk_8 (struct
								       frame_melt_start_this_module_st
								       *,
								       char
								       *);
void MELT_MODULE_VISIBILITY meltmod__warmelt_base__initialmeltchunk_9 (struct
								       frame_melt_start_this_module_st
								       *,
								       char
								       *);
void MELT_MODULE_VISIBILITY meltmod__warmelt_base__initialmeltchunk_10 (struct
									frame_melt_start_this_module_st
									*,
									char
									*);
void MELT_MODULE_VISIBILITY meltmod__warmelt_base__initialmeltchunk_11 (struct
									frame_melt_start_this_module_st
									*,
									char
									*);
void MELT_MODULE_VISIBILITY meltmod__warmelt_base__initialmeltchunk_12 (struct
									frame_melt_start_this_module_st
									*,
									char
									*);
void MELT_MODULE_VISIBILITY meltmod__warmelt_base__initialmeltchunk_13 (struct
									frame_melt_start_this_module_st
									*,
									char
									*);
void MELT_MODULE_VISIBILITY meltmod__warmelt_base__initialmeltchunk_14 (struct
									frame_melt_start_this_module_st
									*,
									char
									*);
void MELT_MODULE_VISIBILITY meltmod__warmelt_base__initialmeltchunk_15 (struct
									frame_melt_start_this_module_st
									*,
									char
									*);
void MELT_MODULE_VISIBILITY meltmod__warmelt_base__initialmeltchunk_16 (struct
									frame_melt_start_this_module_st
									*,
									char
									*);
void MELT_MODULE_VISIBILITY meltmod__warmelt_base__initialmeltchunk_17 (struct
									frame_melt_start_this_module_st
									*,
									char
									*);
void MELT_MODULE_VISIBILITY meltmod__warmelt_base__initialmeltchunk_18 (struct
									frame_melt_start_this_module_st
									*,
									char
									*);
void MELT_MODULE_VISIBILITY meltmod__warmelt_base__initialmeltchunk_19 (struct
									frame_melt_start_this_module_st
									*,
									char
									*);
void MELT_MODULE_VISIBILITY meltmod__warmelt_base__initialmeltchunk_20 (struct
									frame_melt_start_this_module_st
									*,
									char
									*);
void MELT_MODULE_VISIBILITY meltmod__warmelt_base__initialmeltchunk_21 (struct
									frame_melt_start_this_module_st
									*,
									char
									*);
void MELT_MODULE_VISIBILITY meltmod__warmelt_base__initialmeltchunk_22 (struct
									frame_melt_start_this_module_st
									*,
									char
									*);
void MELT_MODULE_VISIBILITY meltmod__warmelt_base__initialmeltchunk_23 (struct
									frame_melt_start_this_module_st
									*,
									char
									*);
void MELT_MODULE_VISIBILITY meltmod__warmelt_base__initialmeltchunk_24 (struct
									frame_melt_start_this_module_st
									*,
									char
									*);
void MELT_MODULE_VISIBILITY meltmod__warmelt_base__initialmeltchunk_25 (struct
									frame_melt_start_this_module_st
									*,
									char
									*);
void MELT_MODULE_VISIBILITY meltmod__warmelt_base__initialmeltchunk_26 (struct
									frame_melt_start_this_module_st
									*,
									char
									*);
void MELT_MODULE_VISIBILITY meltmod__warmelt_base__initialmeltchunk_27 (struct
									frame_melt_start_this_module_st
									*,
									char
									*);
void MELT_MODULE_VISIBILITY meltmod__warmelt_base__initialmeltchunk_28 (struct
									frame_melt_start_this_module_st
									*,
									char
									*);
void MELT_MODULE_VISIBILITY meltmod__warmelt_base__initialmeltchunk_29 (struct
									frame_melt_start_this_module_st
									*,
									char
									*);
void MELT_MODULE_VISIBILITY meltmod__warmelt_base__initialmeltchunk_30 (struct
									frame_melt_start_this_module_st
									*,
									char
									*);
void MELT_MODULE_VISIBILITY meltmod__warmelt_base__initialmeltchunk_31 (struct
									frame_melt_start_this_module_st
									*,
									char
									*);
void MELT_MODULE_VISIBILITY meltmod__warmelt_base__initialmeltchunk_32 (struct
									frame_melt_start_this_module_st
									*,
									char
									*);
void MELT_MODULE_VISIBILITY meltmod__warmelt_base__initialmeltchunk_33 (struct
									frame_melt_start_this_module_st
									*,
									char
									*);
void MELT_MODULE_VISIBILITY meltmod__warmelt_base__initialmeltchunk_34 (struct
									frame_melt_start_this_module_st
									*,
									char
									*);
void MELT_MODULE_VISIBILITY meltmod__warmelt_base__initialmeltchunk_35 (struct
									frame_melt_start_this_module_st
									*,
									char
									*);
void MELT_MODULE_VISIBILITY meltmod__warmelt_base__initialmeltchunk_36 (struct
									frame_melt_start_this_module_st
									*,
									char
									*);
void MELT_MODULE_VISIBILITY meltmod__warmelt_base__initialmeltchunk_37 (struct
									frame_melt_start_this_module_st
									*,
									char
									*);
void MELT_MODULE_VISIBILITY meltmod__warmelt_base__initialmeltchunk_38 (struct
									frame_melt_start_this_module_st
									*,
									char
									*);
void MELT_MODULE_VISIBILITY meltmod__warmelt_base__initialmeltchunk_39 (struct
									frame_melt_start_this_module_st
									*,
									char
									*);
void MELT_MODULE_VISIBILITY meltmod__warmelt_base__initialmeltchunk_40 (struct
									frame_melt_start_this_module_st
									*,
									char
									*);
void MELT_MODULE_VISIBILITY meltmod__warmelt_base__initialmeltchunk_41 (struct
									frame_melt_start_this_module_st
									*,
									char
									*);
void MELT_MODULE_VISIBILITY meltmod__warmelt_base__initialmeltchunk_42 (struct
									frame_melt_start_this_module_st
									*,
									char
									*);
void MELT_MODULE_VISIBILITY meltmod__warmelt_base__initialmeltchunk_43 (struct
									frame_melt_start_this_module_st
									*,
									char
									*);
void MELT_MODULE_VISIBILITY meltmod__warmelt_base__initialmeltchunk_44 (struct
									frame_melt_start_this_module_st
									*,
									char
									*);
void MELT_MODULE_VISIBILITY meltmod__warmelt_base__initialmeltchunk_45 (struct
									frame_melt_start_this_module_st
									*,
									char
									*);
void MELT_MODULE_VISIBILITY meltmod__warmelt_base__initialmeltchunk_46 (struct
									frame_melt_start_this_module_st
									*,
									char
									*);
void MELT_MODULE_VISIBILITY meltmod__warmelt_base__initialmeltchunk_47 (struct
									frame_melt_start_this_module_st
									*,
									char
									*);
void MELT_MODULE_VISIBILITY meltmod__warmelt_base__initialmeltchunk_48 (struct
									frame_melt_start_this_module_st
									*,
									char
									*);
void MELT_MODULE_VISIBILITY meltmod__warmelt_base__initialmeltchunk_49 (struct
									frame_melt_start_this_module_st
									*,
									char
									*);
void MELT_MODULE_VISIBILITY meltmod__warmelt_base__initialmeltchunk_50 (struct
									frame_melt_start_this_module_st
									*,
									char
									*);
void MELT_MODULE_VISIBILITY meltmod__warmelt_base__initialmeltchunk_51 (struct
									frame_melt_start_this_module_st
									*,
									char
									*);
void MELT_MODULE_VISIBILITY meltmod__warmelt_base__initialmeltchunk_52 (struct
									frame_melt_start_this_module_st
									*,
									char
									*);
void MELT_MODULE_VISIBILITY meltmod__warmelt_base__initialmeltchunk_53 (struct
									frame_melt_start_this_module_st
									*,
									char
									*);
void MELT_MODULE_VISIBILITY meltmod__warmelt_base__initialmeltchunk_54 (struct
									frame_melt_start_this_module_st
									*,
									char
									*);
void MELT_MODULE_VISIBILITY meltmod__warmelt_base__initialmeltchunk_55 (struct
									frame_melt_start_this_module_st
									*,
									char
									*);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_base__forward_or_mark_module_start_frame (struct
							   melt_callframe_st
							   *fp, int marking);
#define meltmarking_melt_start_this_module  meltmod__warmelt_base__forward_or_mark_module_start_frame



/**** warmelt-base+03.c implementations ****/




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_57_warmelt_base_RETRIEVE_VALUE_DESCRIPTOR_LIST (meltclosure_ptr_t
							 meltclosp_,
							 melt_ptr_t
							 meltfirstargp_,
							 const
							 melt_argdescr_cell_t
							 meltxargdescr_[],
							 union meltparam_un
							 *meltxargtab_,
							 const
							 melt_argdescr_cell_t
							 meltxresdescr_[],
							 union meltparam_un
							 *meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_57_warmelt_base_RETRIEVE_VALUE_DESCRIPTOR_LIST_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_57_warmelt_base_RETRIEVE_VALUE_DESCRIPTOR_LIST_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 2
    melt_ptr_t mcfr_varptr[2];
/*no varnum*/
#define MELTFRAM_NBVARNUM /*none*/0
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_57_warmelt_base_RETRIEVE_VALUE_DESCRIPTOR_LIST is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct
	 frame_meltrout_57_warmelt_base_RETRIEVE_VALUE_DESCRIPTOR_LIST_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 2; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_57_warmelt_base_RETRIEVE_VALUE_DESCRIPTOR_LIST nbval 2*/
  meltfram__.mcfr_nbvar = 2 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("RETRIEVE_VALUE_DESCRIPTOR_LIST", meltcallcount);
/*getargs*/
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
  MELT_LOCATION ("warmelt-base.melt:2591:/ block");
/*anyblock*/
  {

    MELT_LOCATION ("warmelt-base.melt:2592:/ cond");
    /*cond */ if (
		   /*ifisa */
		   melt_is_instance_of ((melt_ptr_t)
					(( /*!VALUE_DESCRIPTOR_LIST_REFERENCE */ meltfrout->tabval[0])),
					(melt_ptr_t) (( /*!CLASS_REFERENCE */
						       meltfrout->tabval[1])))
      )				/*then */
      {
	/*^cond.then */
	/*^getslot */
	{
	  melt_ptr_t slot = NULL, obj = NULL;
	  obj =
	    (melt_ptr_t) (( /*!VALUE_DESCRIPTOR_LIST_REFERENCE */ meltfrout->
			   tabval[0])) /*=obj*/ ;
	  melt_object_get_field (slot, obj, 0, "REFERENCED_VALUE");
   /*_.REFERENCED_VALUE__V2*/ meltfptr[1] = slot;
	};
	;
      }
    else
      {				/*^cond.else */

  /*_.REFERENCED_VALUE__V2*/ meltfptr[1] = NULL;;
      }
    ;
    MELT_LOCATION ("warmelt-base.melt:2591:/ quasiblock");


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.REFERENCED_VALUE__V2*/ meltfptr[1];;

    {
      MELT_LOCATION ("warmelt-base.melt:2591:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.REFERENCED_VALUE__V2*/ meltfptr[1] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("RETRIEVE_VALUE_DESCRIPTOR_LIST", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_57_warmelt_base_RETRIEVE_VALUE_DESCRIPTOR_LIST_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_57_warmelt_base_RETRIEVE_VALUE_DESCRIPTOR_LIST */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_58_warmelt_base_LIST_REMOVE_LAST_ELEMENT (meltclosure_ptr_t
						   meltclosp_,
						   melt_ptr_t meltfirstargp_,
						   const melt_argdescr_cell_t
						   meltxargdescr_[],
						   union meltparam_un *
						   meltxargtab_,
						   const melt_argdescr_cell_t
						   meltxresdescr_[],
						   union meltparam_un *
						   meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_58_warmelt_base_LIST_REMOVE_LAST_ELEMENT_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_58_warmelt_base_LIST_REMOVE_LAST_ELEMENT_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 18
    melt_ptr_t mcfr_varptr[18];
#define MELTFRAM_NBVARNUM 6
    long mcfr_varnum[6];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_58_warmelt_base_LIST_REMOVE_LAST_ELEMENT is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_58_warmelt_base_LIST_REMOVE_LAST_ELEMENT_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 18; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_58_warmelt_base_LIST_REMOVE_LAST_ELEMENT nbval 18*/
  meltfram__.mcfr_nbvar = 18 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("LIST_REMOVE_LAST_ELEMENT", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-base.melt:3586:/ getarg");
 /*_.LIS__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {

    MELT_LOCATION ("warmelt-base.melt:3590:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#IS_LIST__L1*/ meltfnum[0] =
      (melt_magic_discr ((melt_ptr_t) ( /*_.LIS__V2*/ meltfptr[1])) ==
       MELTOBMAG_LIST);;
    /*^compute */
 /*_#NOT__L2*/ meltfnum[1] =
      (!( /*_#IS_LIST__L1*/ meltfnum[0]));;
    MELT_LOCATION ("warmelt-base.melt:3590:/ cond");
    /*cond */ if ( /*_#NOT__L2*/ meltfnum[1])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-base.melt:3591:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^quasiblock */


	  /*_.RETVAL___V1*/ meltfptr[0] = ( /*nil */ NULL);;

	  {
	    MELT_LOCATION ("warmelt-base.melt:3591:/ locexp");
	    /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
	    if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	      melt_warn_for_no_expected_secondary_results ();
	    /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
	    ;
	  }
	  ;
	  /*^finalreturn */
	  ;
	  /*finalret */ goto labend_rout;
	  /*_.IF___V3*/ meltfptr[2] = /*_.RETURN___V4*/ meltfptr[3];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-base.melt:3590:/ clear");
	     /*clear *//*_.RETURN___V4*/ meltfptr[3] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

  /*_.IF___V3*/ meltfptr[2] = NULL;;
      }
    ;
    MELT_LOCATION ("warmelt-base.melt:3592:/ quasiblock");


 /*_.LASTPAIR__V5*/ meltfptr[3] =
      (melt_list_last ((melt_ptr_t) ( /*_.LIS__V2*/ meltfptr[1])));;
    /*^compute */
    /*_.PREVPAIR__V6*/ meltfptr[5] = ( /*nil */ NULL);;
    MELT_LOCATION ("warmelt-base.melt:3595:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^cond */
    /*cond */ if ( /*_.LASTPAIR__V5*/ meltfptr[3])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{


#if MELT_HAVE_DEBUG
	  /*^cppif.then */
	  /*^block */
	  /*anyblock */
	  {

	    /*^checksignal */
	    MELT_CHECK_SIGNAL ();
	    ;
     /*_#IS_PAIR__L3*/ meltfnum[2] =
	      (melt_magic_discr
	       ((melt_ptr_t) ( /*_.LASTPAIR__V5*/ meltfptr[3])) ==
	       MELTOBMAG_PAIR);;
	    MELT_LOCATION ("warmelt-base.melt:3595:/ cond");
	    /*cond */ if ( /*_#IS_PAIR__L3*/ meltfnum[2])	/*then */
	      {
		/*^cond.then */
		/*_.IFELSE___V9*/ meltfptr[8] = ( /*nil */ NULL);;
	      }
	    else
	      {
		MELT_LOCATION ("warmelt-base.melt:3595:/ cond.else");

		/*^block */
		/*anyblock */
		{




		  {
		    /*^locexp */
		    melt_assert_failed (("check lastpair"),
					("warmelt-base.melt")
					? ("warmelt-base.melt") : __FILE__,
					(3595) ? (3595) : __LINE__,
					__FUNCTION__);
		    ;
		  }
		  ;
		 /*clear *//*_.IFELSE___V9*/ meltfptr[8] = 0;
		  /*epilog */
		}
		;
	      }
	    ;
	    /*^compute */
	    /*_.IFCPP___V8*/ meltfptr[7] = /*_.IFELSE___V9*/ meltfptr[8];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-base.melt:3595:/ clear");
	       /*clear *//*_#IS_PAIR__L3*/ meltfnum[2] = 0;
	    /*^clear */
	       /*clear *//*_.IFELSE___V9*/ meltfptr[8] = 0;
	  }

#else /*MELT_HAVE_DEBUG */
	  /*^cppif.else */
	  /*_.IFCPP___V8*/ meltfptr[7] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
	  ;
	  /*^compute */
	  /*_.IF___V7*/ meltfptr[6] = /*_.IFCPP___V8*/ meltfptr[7];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-base.melt:3595:/ clear");
	     /*clear *//*_.IFCPP___V8*/ meltfptr[7] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

  /*_.IF___V7*/ meltfptr[6] = NULL;;
      }
    ;
    /*citerblock FOREACH_IN_LIST */
    {
      /* start foreach_in_list meltcit1__EACHLIST */
      for ( /*_.CURPAIR__V10*/ meltfptr[8] =
	   melt_list_first ((melt_ptr_t) /*_.LIS__V2*/ meltfptr[1]);
	   melt_magic_discr ((melt_ptr_t) /*_.CURPAIR__V10*/ meltfptr[8]) ==
	   MELTOBMAG_PAIR;
	   /*_.CURPAIR__V10*/ meltfptr[8] =
	   melt_pair_tail ((melt_ptr_t) /*_.CURPAIR__V10*/ meltfptr[8]))
	{
	  /*_.CURELEM__V11*/ meltfptr[7] =
	    melt_pair_head ((melt_ptr_t) /*_.CURPAIR__V10*/ meltfptr[8]);



#if MELT_HAVE_DEBUG
	  MELT_LOCATION ("warmelt-base.melt:3599:/ cppif.then");
	  /*^block */
	  /*anyblock */
	  {

	    /*^checksignal */
	    MELT_CHECK_SIGNAL ();
	    ;
    /*_#IS_PAIR__L4*/ meltfnum[2] =
	      (melt_magic_discr
	       ((melt_ptr_t) ( /*_.CURPAIR__V10*/ meltfptr[8])) ==
	       MELTOBMAG_PAIR);;
	    MELT_LOCATION ("warmelt-base.melt:3599:/ cond");
	    /*cond */ if ( /*_#IS_PAIR__L4*/ meltfnum[2])	/*then */
	      {
		/*^cond.then */
		/*_.IFELSE___V13*/ meltfptr[12] = ( /*nil */ NULL);;
	      }
	    else
	      {
		MELT_LOCATION ("warmelt-base.melt:3599:/ cond.else");

		/*^block */
		/*anyblock */
		{




		  {
		    /*^locexp */
		    melt_assert_failed (("check curpair"),
					("warmelt-base.melt")
					? ("warmelt-base.melt") : __FILE__,
					(3599) ? (3599) : __LINE__,
					__FUNCTION__);
		    ;
		  }
		  ;
		/*clear *//*_.IFELSE___V13*/ meltfptr[12] = 0;
		  /*epilog */
		}
		;
	      }
	    ;
	    /*^compute */
	    /*_.IFCPP___V12*/ meltfptr[11] = /*_.IFELSE___V13*/ meltfptr[12];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-base.melt:3599:/ clear");
	      /*clear *//*_#IS_PAIR__L4*/ meltfnum[2] = 0;
	    /*^clear */
	      /*clear *//*_.IFELSE___V13*/ meltfptr[12] = 0;
	  }

#else /*MELT_HAVE_DEBUG */
	  /*^cppif.else */
	  /*_.IFCPP___V12*/ meltfptr[11] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
	  ;
	  MELT_LOCATION ("warmelt-base.melt:3600:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
  /*_#__L5*/ meltfnum[2] =
	    (( /*_.CURPAIR__V10*/ meltfptr[8]) ==
	     ( /*_.LASTPAIR__V5*/ meltfptr[3]));;
	  MELT_LOCATION ("warmelt-base.melt:3600:/ cond");
	  /*cond */ if ( /*_#__L5*/ meltfnum[2])	/*then */
	    {
	      /*^cond.then */
	      /*^block */
	      /*anyblock */
	      {

		MELT_LOCATION ("warmelt-base.melt:3601:/ compute");
		/*_.CURPAIR__V10*/ meltfptr[8] =
		  /*_.SETQ___V15*/ meltfptr[14] = ( /*nil */ NULL);;
		MELT_LOCATION ("warmelt-base.melt:3602:/ checksignal");
		MELT_CHECK_SIGNAL ();
		;
    /*_#IS_PAIR__L6*/ meltfnum[5] =
		  (melt_magic_discr
		   ((melt_ptr_t) ( /*_.PREVPAIR__V6*/ meltfptr[5])) ==
		   MELTOBMAG_PAIR);;
		MELT_LOCATION ("warmelt-base.melt:3602:/ cond");
		/*cond */ if ( /*_#IS_PAIR__L6*/ meltfnum[5])	/*then */
		  {
		    /*^cond.then */
		    /*^block */
		    /*anyblock */
		    {


		      {
			MELT_LOCATION ("warmelt-base.melt:3604:/ locexp");
			/* list_remove_last_element UPDATEPAIR_CHK__1 */
			((struct meltpair_st *) /*_.PREVPAIR__V6*/
			 meltfptr[5])->tl = NULL;
			((struct meltlist_st *) /*_.LIS__V2*/ meltfptr[1])->last
			  = (struct meltpair_st *) /*_.PREVPAIR__V6*/
			  meltfptr[5];
			meltgc_touch_dest ( /*_.LIS__V2*/ meltfptr[1],
					   /*_.PREVPAIR__V6*/ meltfptr[5]);
			;
		      }
		      ;
		      /*epilog */
		    }
		    ;
		  }
		else
		  {
		    MELT_LOCATION ("warmelt-base.melt:3602:/ cond.else");

		    /*^block */
		    /*anyblock */
		    {


		      {
			MELT_LOCATION ("warmelt-base.melt:3611:/ locexp");
			/* list_remove_last_element EMPTYLIST_CHK__1 */
			((struct meltlist_st *) /*_.LIS__V2*/ meltfptr[1])->
			  first = NULL;
			((struct meltlist_st *) /*_.LIS__V2*/ meltfptr[1])->
			  last = NULL;
			meltgc_touch ( /*_.LIS__V2*/ meltfptr[1]);
			;
		      }
		      ;
		      /*epilog */
		    }
		    ;
		  }
		;
		MELT_LOCATION ("warmelt-base.melt:3616:/ checksignal");
		MELT_CHECK_SIGNAL ();
		;
		/*^quasiblock */


		/*_.RETVAL___V1*/ meltfptr[0] =
		  /*_.CURELEM__V11*/ meltfptr[7];;

		{
		  MELT_LOCATION ("warmelt-base.melt:3616:/ locexp");
		  /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
		  if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
		    melt_warn_for_no_expected_secondary_results ();
		  /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
		  ;
		}
		;
		/*^finalreturn */
		;
		/*finalret */ goto labend_rout;
		MELT_LOCATION ("warmelt-base.melt:3600:/ quasiblock");


		/*_.PROGN___V17*/ meltfptr[16] =
		  /*_.RETURN___V16*/ meltfptr[15];;
		/*^compute */
		/*_.IF___V14*/ meltfptr[12] = /*_.PROGN___V17*/ meltfptr[16];;
		/*epilog */

		MELT_LOCATION ("warmelt-base.melt:3600:/ clear");
	      /*clear *//*_.SETQ___V15*/ meltfptr[14] = 0;
		/*^clear */
	      /*clear *//*_#IS_PAIR__L6*/ meltfnum[5] = 0;
		/*^clear */
	      /*clear *//*_.RETURN___V16*/ meltfptr[15] = 0;
		/*^clear */
	      /*clear *//*_.PROGN___V17*/ meltfptr[16] = 0;
	      }
	      ;
	    }
	  else
	    {			/*^cond.else */

   /*_.IF___V14*/ meltfptr[12] = NULL;;
	    }
	  ;
	  MELT_LOCATION ("warmelt-base.melt:3617:/ compute");
	  /*_.PREVPAIR__V6*/ meltfptr[5] = /*_.SETQ___V18*/ meltfptr[14] =
	    /*_.CURPAIR__V10*/ meltfptr[8];;
	}			/* end foreach_in_list meltcit1__EACHLIST */
     /*_.CURPAIR__V10*/ meltfptr[8] = NULL;
     /*_.CURELEM__V11*/ meltfptr[7] = NULL;


      /*citerepilog */

      MELT_LOCATION ("warmelt-base.melt:3596:/ clear");
	    /*clear *//*_.CURPAIR__V10*/ meltfptr[8] = 0;
      /*^clear */
	    /*clear *//*_.CURELEM__V11*/ meltfptr[7] = 0;
      /*^clear */
	    /*clear *//*_.IFCPP___V12*/ meltfptr[11] = 0;
      /*^clear */
	    /*clear *//*_#__L5*/ meltfnum[2] = 0;
      /*^clear */
	    /*clear *//*_.IF___V14*/ meltfptr[12] = 0;
      /*^clear */
	    /*clear *//*_.SETQ___V18*/ meltfptr[14] = 0;
    }				/*endciterblock FOREACH_IN_LIST */
    ;

    MELT_LOCATION ("warmelt-base.melt:3592:/ clear");
	   /*clear *//*_.LASTPAIR__V5*/ meltfptr[3] = 0;
    /*^clear */
	   /*clear *//*_.PREVPAIR__V6*/ meltfptr[5] = 0;
    /*^clear */
	   /*clear *//*_.IF___V7*/ meltfptr[6] = 0;
    MELT_LOCATION ("warmelt-base.melt:3586:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*epilog */

    /*^clear */
	   /*clear *//*_#IS_LIST__L1*/ meltfnum[0] = 0;
    /*^clear */
	   /*clear *//*_#NOT__L2*/ meltfnum[1] = 0;
    /*^clear */
	   /*clear *//*_.IF___V3*/ meltfptr[2] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("LIST_REMOVE_LAST_ELEMENT", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_58_warmelt_base_LIST_REMOVE_LAST_ELEMENT_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_58_warmelt_base_LIST_REMOVE_LAST_ELEMENT */



/**** end of warmelt-base+03.c ****/
