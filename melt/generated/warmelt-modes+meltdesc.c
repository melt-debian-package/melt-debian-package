/** GENERATED MELT DESCRIPTOR FILE meltbuild-sources/warmelt-modes+meltdesc.c 
** NEVER EDIT OR MOVE THIS, IT IS GENERATED & PARSED! **/
/* These identifiers are generated in warmelt-outobj.melt 
 & handled in melt-runtime.c carefully. */

	     #ifdef __cplusplus
	     /* explicitly declare as extern "C" our dlsym-ed symbols */
	     extern "C" const char melt_versionmeltstr[]	    ;
	     extern "C" const char melt_genversionstr[]		    ;
	     extern "C" const char melt_modulename[]		    ;
	     extern "C" const char melt_modulerealpath[]	    ;
	     extern "C" const char melt_prepromd5meltrun[]	    ;
	     extern "C" const char melt_primaryhexmd5[]		    ;
	     extern "C" const char* const melt_secondaryhexmd5tab[] ;
	     extern "C" const int melt_lastsecfileindex		    ;
	     extern "C" const char melt_cumulated_hexmd5[]	    ;

	     extern "C" {
	     #endif /*__cplusplus */
	     
/* version of the GCC compiler & MELT runtime generating this */
const char melt_genversionstr[]="4.8.0 20121010 (experimental) [melt-branch revision 192289] MELT_0\
.9.7-rc4"

	     #ifdef __cplusplus
	     " (in C++)"
	     #else
	     " (in C)"
	     #endif
					;
	     
const char melt_versionmeltstr[]="0.9.7-rc4 [melt-branch_revision_192289]";

/* source name & real path of the module */
/*MELTMODULENAME meltbuild-sources/warmelt-modes */
const char melt_modulename[]="warmelt-modes";
const char melt_modulerealpath[]="/usr/local/libexec/gcc-melt/gcc/x86_64-unknown-linux-gnu/4.8.0/melt\
-modules/0.9.7-rc4/warmelt-modes";

/* hash of preprocessed melt-run.h generating this */
const char melt_prepromd5meltrun[]="f66a30d9bc2c10de00b1532482ad91ed";
/* hexmd5checksum of primary C file */
const char melt_primaryhexmd5[]="1a837d7c434ceb60e34771370cefda5d";

/* hexmd5checksum of secondary C files */
const char* const melt_secondaryhexmd5tab[]={
 /*nosecfile*/ (const char*)0,
 /*sechexmd5checksum meltbuild-sources/warmelt-modes+01.c #1 */ "32950b2723bbb7b78e95b9321e54d491",
 /*sechexmd5checksum meltbuild-sources/warmelt-modes+02.c #2 */ "433e1c5db23bfb9ce94d068c2cb4c9e2",
 /*sechexmd5checksum meltbuild-sources/warmelt-modes+03.c #3 */ "34a5ed324b524499cdfe7284cba71ac5",
 (const char*)0 };

/* last index of secondary files */
const int melt_lastsecfileindex=3;

/* cumulated checksum of primary & secondary files */
const char melt_cumulated_hexmd5[]="008af99c13714a7f594164a8fdb3c371" ;

/* include the timestamp file */
#define meltmod_warmelt_modes_mds__008af99c13714a7f594164a8fdb3c371 1
#include "warmelt-modes+melttime.h"
	 

		 #ifdef __cplusplus
		 }	  /* end extern C descriptor */
		 #endif /*__cplusplus */
		 
/* end of melt descriptor file */
