/* GCC MELT GENERATED FILE warmelt-base+01.c - DO NOT EDIT */
/* secondary MELT generated C file of rank #1 */
#include "melt-run.h"


/* used hash from melt-run.h when compiling this file: */
MELT_EXTERN const char meltrun_used_md5_melt_f1[] =
  MELT_RUN_HASHMD5 /* from melt-run.h */ ;


/**** warmelt-base+01.c declarations ****/


/***************************************************
***
    Copyright 2008 - 2012 Free Software Foundation, Inc.
    Contributed by Basile Starynkevitch <basile@starynkevitch.net>
               and Pierre Vittet  <piervit@pvittet.com>

    This file is part of GCC.

    GCC is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3, or (at your option)
    any later version.

    GCC is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with GCC; see the file COPYING3.  If not see
    <http://www.gnu.org/licenses/>.
***
****************************************************/

/* ordinary MELT module */
#define MELT_HAS_INITIAL_ENVIRONMENT 1	/*usual */

struct melt_callframe_st;	/*defined in melt-runtime.h */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_1_warmelt_base_IV (meltclosure_ptr_t meltclosp_,
			    melt_ptr_t meltfirstargp_,
			    const melt_argdescr_cell_t meltxargdescr_[],
			    union meltparam_un *meltxargtab_,
			    const melt_argdescr_cell_t meltxresdescr_[],
			    union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_2_warmelt_base_IV (meltclosure_ptr_t meltclosp_,
			    melt_ptr_t meltfirstargp_,
			    const melt_argdescr_cell_t meltxargdescr_[],
			    union meltparam_un *meltxargtab_,
			    const melt_argdescr_cell_t meltxresdescr_[],
			    union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_3_warmelt_base_IV (meltclosure_ptr_t meltclosp_,
			    melt_ptr_t meltfirstargp_,
			    const melt_argdescr_cell_t meltxargdescr_[],
			    union meltparam_un *meltxargtab_,
			    const melt_argdescr_cell_t meltxresdescr_[],
			    union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_4_warmelt_base_IV (meltclosure_ptr_t meltclosp_,
			    melt_ptr_t meltfirstargp_,
			    const melt_argdescr_cell_t meltxargdescr_[],
			    union meltparam_un *meltxargtab_,
			    const melt_argdescr_cell_t meltxresdescr_[],
			    union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_5_warmelt_base_IV (meltclosure_ptr_t meltclosp_,
			    melt_ptr_t meltfirstargp_,
			    const melt_argdescr_cell_t meltxargdescr_[],
			    union meltparam_un *meltxargtab_,
			    const melt_argdescr_cell_t meltxresdescr_[],
			    union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_6_warmelt_base_COMPARE_NAMED_ALPHA (meltclosure_ptr_t meltclosp_,
					     melt_ptr_t meltfirstargp_,
					     const melt_argdescr_cell_t
					     meltxargdescr_[],
					     union meltparam_un *meltxargtab_,
					     const melt_argdescr_cell_t
					     meltxresdescr_[],
					     union meltparam_un
					     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_7_warmelt_base_ADD2OUT (meltclosure_ptr_t meltclosp_,
				 melt_ptr_t meltfirstargp_,
				 const melt_argdescr_cell_t meltxargdescr_[],
				 union meltparam_un *meltxargtab_,
				 const melt_argdescr_cell_t meltxresdescr_[],
				 union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_8_warmelt_base_STRING4OUT (meltclosure_ptr_t meltclosp_,
				    melt_ptr_t meltfirstargp_,
				    const melt_argdescr_cell_t
				    meltxargdescr_[],
				    union meltparam_un *meltxargtab_,
				    const melt_argdescr_cell_t
				    meltxresdescr_[],
				    union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_9_warmelt_base_ADD2OUT4NULL (meltclosure_ptr_t meltclosp_,
				      melt_ptr_t meltfirstargp_,
				      const melt_argdescr_cell_t
				      meltxargdescr_[],
				      union meltparam_un *meltxargtab_,
				      const melt_argdescr_cell_t
				      meltxresdescr_[],
				      union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_10_warmelt_base_ADD2OUT4INTEGER (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_11_warmelt_base_ADD2OUT4STRING (meltclosure_ptr_t meltclosp_,
					 melt_ptr_t meltfirstargp_,
					 const melt_argdescr_cell_t
					 meltxargdescr_[],
					 union meltparam_un *meltxargtab_,
					 const melt_argdescr_cell_t
					 meltxresdescr_[],
					 union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_12_warmelt_base_ADD2OUT4STRBUF (meltclosure_ptr_t meltclosp_,
					 melt_ptr_t meltfirstargp_,
					 const melt_argdescr_cell_t
					 meltxargdescr_[],
					 union meltparam_un *meltxargtab_,
					 const melt_argdescr_cell_t
					 meltxresdescr_[],
					 union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_13_warmelt_base_ADD2OUT4CLONEDSYMB (meltclosure_ptr_t meltclosp_,
					     melt_ptr_t meltfirstargp_,
					     const melt_argdescr_cell_t
					     meltxargdescr_[],
					     union meltparam_un *meltxargtab_,
					     const melt_argdescr_cell_t
					     meltxresdescr_[],
					     union meltparam_un
					     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_14_warmelt_base_ADD2OUT4NAMED (meltclosure_ptr_t meltclosp_,
					melt_ptr_t meltfirstargp_,
					const melt_argdescr_cell_t
					meltxargdescr_[],
					union meltparam_un *meltxargtab_,
					const melt_argdescr_cell_t
					meltxresdescr_[],
					union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_15_warmelt_base_ADD2OUT4ROOTOBJECT (meltclosure_ptr_t meltclosp_,
					     melt_ptr_t meltfirstargp_,
					     const melt_argdescr_cell_t
					     meltxargdescr_[],
					     union meltparam_un *meltxargtab_,
					     const melt_argdescr_cell_t
					     meltxresdescr_[],
					     union meltparam_un
					     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_16_warmelt_base_ADD2OUT4ANY (meltclosure_ptr_t meltclosp_,
				      melt_ptr_t meltfirstargp_,
				      const melt_argdescr_cell_t
				      meltxargdescr_[],
				      union meltparam_un *meltxargtab_,
				      const melt_argdescr_cell_t
				      meltxresdescr_[],
				      union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_17_warmelt_base_MAPSTRING_EVERY (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_18_warmelt_base_MAPSTRING_ITERATE_TEST (meltclosure_ptr_t meltclosp_,
						 melt_ptr_t meltfirstargp_,
						 const melt_argdescr_cell_t
						 meltxargdescr_[],
						 union meltparam_un
						 *meltxargtab_,
						 const melt_argdescr_cell_t
						 meltxresdescr_[],
						 union meltparam_un
						 *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_19_warmelt_base_MULTIPLE_EVERY (meltclosure_ptr_t meltclosp_,
					 melt_ptr_t meltfirstargp_,
					 const melt_argdescr_cell_t
					 meltxargdescr_[],
					 union meltparam_un *meltxargtab_,
					 const melt_argdescr_cell_t
					 meltxresdescr_[],
					 union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_20_warmelt_base_MULTIPLE_BACKWARD_EVERY (meltclosure_ptr_t
						  meltclosp_,
						  melt_ptr_t meltfirstargp_,
						  const melt_argdescr_cell_t
						  meltxargdescr_[],
						  union meltparam_un
						  *meltxargtab_,
						  const melt_argdescr_cell_t
						  meltxresdescr_[],
						  union meltparam_un
						  *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_21_warmelt_base_MULTIPLE_EVERY_BOTH (meltclosure_ptr_t meltclosp_,
					      melt_ptr_t meltfirstargp_,
					      const melt_argdescr_cell_t
					      meltxargdescr_[],
					      union meltparam_un
					      *meltxargtab_,
					      const melt_argdescr_cell_t
					      meltxresdescr_[],
					      union meltparam_un
					      *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_22_warmelt_base_RUN_ALL_PASS_EXECUTION_HOOKS (meltclosure_ptr_t
						       meltclosp_,
						       melt_ptr_t
						       meltfirstargp_,
						       const
						       melt_argdescr_cell_t
						       meltxargdescr_[],
						       union meltparam_un
						       *meltxargtab_,
						       const
						       melt_argdescr_cell_t
						       meltxresdescr_[],
						       union meltparam_un
						       *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_23_warmelt_base_REGISTER_PASS_EXECUTION_HOOK (meltclosure_ptr_t
						       meltclosp_,
						       melt_ptr_t
						       meltfirstargp_,
						       const
						       melt_argdescr_cell_t
						       meltxargdescr_[],
						       union meltparam_un
						       *meltxargtab_,
						       const
						       melt_argdescr_cell_t
						       meltxresdescr_[],
						       union meltparam_un
						       *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_24_warmelt_base_LAMBDA___1__ (meltclosure_ptr_t meltclosp_,
				       melt_ptr_t meltfirstargp_,
				       const melt_argdescr_cell_t
				       meltxargdescr_[],
				       union meltparam_un *meltxargtab_,
				       const melt_argdescr_cell_t
				       meltxresdescr_[],
				       union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_25_warmelt_base_UNREGISTER_PASS_EXECUTION_HOOK (meltclosure_ptr_t
							 meltclosp_,
							 melt_ptr_t
							 meltfirstargp_,
							 const
							 melt_argdescr_cell_t
							 meltxargdescr_[],
							 union meltparam_un
							 *meltxargtab_,
							 const
							 melt_argdescr_cell_t
							 meltxresdescr_[],
							 union meltparam_un
							 *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_26_warmelt_base_LAMBDA___2__ (meltclosure_ptr_t meltclosp_,
				       melt_ptr_t meltfirstargp_,
				       const melt_argdescr_cell_t
				       meltxargdescr_[],
				       union meltparam_un *meltxargtab_,
				       const melt_argdescr_cell_t
				       meltxresdescr_[],
				       union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_27_warmelt_base_REGISTER_PRAGMA_HANDLER (meltclosure_ptr_t
						  meltclosp_,
						  melt_ptr_t meltfirstargp_,
						  const melt_argdescr_cell_t
						  meltxargdescr_[],
						  union meltparam_un
						  *meltxargtab_,
						  const melt_argdescr_cell_t
						  meltxresdescr_[],
						  union meltparam_un
						  *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_28_warmelt_base_REGISTER_PRE_GENERICIZE_HOOK_FIRST (meltclosure_ptr_t
							     meltclosp_,
							     melt_ptr_t
							     meltfirstargp_,
							     const
							     melt_argdescr_cell_t
							     meltxargdescr_[],
							     union
							     meltparam_un
							     *meltxargtab_,
							     const
							     melt_argdescr_cell_t
							     meltxresdescr_[],
							     union
							     meltparam_un
							     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_29_warmelt_base_REGISTER_PRE_GENERICIZE_HOOK_LAST (meltclosure_ptr_t
							    meltclosp_,
							    melt_ptr_t
							    meltfirstargp_,
							    const
							    melt_argdescr_cell_t
							    meltxargdescr_[],
							    union meltparam_un
							    *meltxargtab_,
							    const
							    melt_argdescr_cell_t
							    meltxresdescr_[],
							    union meltparam_un
							    *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_30_warmelt_base_PRE_GENERICIZE_HOOK_RUNNER (meltclosure_ptr_t
						     meltclosp_,
						     melt_ptr_t
						     meltfirstargp_,
						     const
						     melt_argdescr_cell_t
						     meltxargdescr_[],
						     union meltparam_un
						     *meltxargtab_,
						     const
						     melt_argdescr_cell_t
						     meltxresdescr_[],
						     union meltparam_un
						     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_31_warmelt_base_MELT_FINISH_TYPE_RUNNER (meltclosure_ptr_t
						  meltclosp_,
						  melt_ptr_t meltfirstargp_,
						  const melt_argdescr_cell_t
						  meltxargdescr_[],
						  union meltparam_un
						  *meltxargtab_,
						  const melt_argdescr_cell_t
						  meltxresdescr_[],
						  union meltparam_un
						  *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_32_warmelt_base_REGISTER_FINISH_TYPE_HOOK_FIRST (meltclosure_ptr_t
							  meltclosp_,
							  melt_ptr_t
							  meltfirstargp_,
							  const
							  melt_argdescr_cell_t
							  meltxargdescr_[],
							  union meltparam_un
							  *meltxargtab_,
							  const
							  melt_argdescr_cell_t
							  meltxresdescr_[],
							  union meltparam_un
							  *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_33_warmelt_base_REGISTER_FINISH_TYPE_HOOK_LAST (meltclosure_ptr_t
							 meltclosp_,
							 melt_ptr_t
							 meltfirstargp_,
							 const
							 melt_argdescr_cell_t
							 meltxargdescr_[],
							 union meltparam_un
							 *meltxargtab_,
							 const
							 melt_argdescr_cell_t
							 meltxresdescr_[],
							 union meltparam_un
							 *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_34_warmelt_base_MELT_FINISH_DECL_RUNNER (meltclosure_ptr_t
						  meltclosp_,
						  melt_ptr_t meltfirstargp_,
						  const melt_argdescr_cell_t
						  meltxargdescr_[],
						  union meltparam_un
						  *meltxargtab_,
						  const melt_argdescr_cell_t
						  meltxresdescr_[],
						  union meltparam_un
						  *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_35_warmelt_base_REGISTER_FINISH_DECL_HOOK_FIRST (meltclosure_ptr_t
							  meltclosp_,
							  melt_ptr_t
							  meltfirstargp_,
							  const
							  melt_argdescr_cell_t
							  meltxargdescr_[],
							  union meltparam_un
							  *meltxargtab_,
							  const
							  melt_argdescr_cell_t
							  meltxresdescr_[],
							  union meltparam_un
							  *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_36_warmelt_base_REGISTER_FINISH_DECL_HOOK_LAST (meltclosure_ptr_t
							 meltclosp_,
							 melt_ptr_t
							 meltfirstargp_,
							 const
							 melt_argdescr_cell_t
							 meltxargdescr_[],
							 union meltparam_un
							 *meltxargtab_,
							 const
							 melt_argdescr_cell_t
							 meltxresdescr_[],
							 union meltparam_un
							 *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_37_warmelt_base_MELT_ALL_PASSES_START_RUNNER (meltclosure_ptr_t
						       meltclosp_,
						       melt_ptr_t
						       meltfirstargp_,
						       const
						       melt_argdescr_cell_t
						       meltxargdescr_[],
						       union meltparam_un
						       *meltxargtab_,
						       const
						       melt_argdescr_cell_t
						       meltxresdescr_[],
						       union meltparam_un
						       *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_38_warmelt_base_REGISTER_ALL_PASSES_START_HOOK_FIRST
(meltclosure_ptr_t meltclosp_, melt_ptr_t meltfirstargp_, const melt_argdescr_cell_t meltxargdescr_[],
union meltparam_un *meltxargtab_, const melt_argdescr_cell_t meltxresdescr_[],
union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_39_warmelt_base_REGISTER_ALL_PASSES_START_HOOK_LAST
(meltclosure_ptr_t meltclosp_, melt_ptr_t meltfirstargp_, const melt_argdescr_cell_t meltxargdescr_[],
union meltparam_un *meltxargtab_, const melt_argdescr_cell_t meltxresdescr_[],
union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_40_warmelt_base_MELT_ALL_PASSES_END_RUNNER (meltclosure_ptr_t
						     meltclosp_,
						     melt_ptr_t
						     meltfirstargp_,
						     const
						     melt_argdescr_cell_t
						     meltxargdescr_[],
						     union meltparam_un
						     *meltxargtab_,
						     const
						     melt_argdescr_cell_t
						     meltxresdescr_[],
						     union meltparam_un
						     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_41_warmelt_base_REGISTER_ALL_PASSES_END_HOOK_FIRST (meltclosure_ptr_t
							     meltclosp_,
							     melt_ptr_t
							     meltfirstargp_,
							     const
							     melt_argdescr_cell_t
							     meltxargdescr_[],
							     union
							     meltparam_un
							     *meltxargtab_,
							     const
							     melt_argdescr_cell_t
							     meltxresdescr_[],
							     union
							     meltparam_un
							     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_42_warmelt_base_REGISTER_ALL_PASSES_END_HOOK_LAST (meltclosure_ptr_t
							    meltclosp_,
							    melt_ptr_t
							    meltfirstargp_,
							    const
							    melt_argdescr_cell_t
							    meltxargdescr_[],
							    union meltparam_un
							    *meltxargtab_,
							    const
							    melt_argdescr_cell_t
							    meltxresdescr_[],
							    union meltparam_un
							    *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_43_warmelt_base_MELT_ALL_IPA_PASSES_START_RUNNER (meltclosure_ptr_t
							   meltclosp_,
							   melt_ptr_t
							   meltfirstargp_,
							   const
							   melt_argdescr_cell_t
							   meltxargdescr_[],
							   union meltparam_un
							   *meltxargtab_,
							   const
							   melt_argdescr_cell_t
							   meltxresdescr_[],
							   union meltparam_un
							   *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_44_warmelt_base_REGISTER_ALL_IPA_PASSES_START_HOOK_FIRST
(meltclosure_ptr_t meltclosp_, melt_ptr_t meltfirstargp_, const melt_argdescr_cell_t meltxargdescr_[],
union meltparam_un *meltxargtab_, const melt_argdescr_cell_t meltxresdescr_[],
union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_45_warmelt_base_REGISTER_ALL_IPA_PASSES_START_HOOK_LAST
(meltclosure_ptr_t meltclosp_, melt_ptr_t meltfirstargp_, const melt_argdescr_cell_t meltxargdescr_[],
union meltparam_un *meltxargtab_, const melt_argdescr_cell_t meltxresdescr_[],
union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_46_warmelt_base_MELT_ALL_IPA_PASSES_END_RUNNER (meltclosure_ptr_t
							 meltclosp_,
							 melt_ptr_t
							 meltfirstargp_,
							 const
							 melt_argdescr_cell_t
							 meltxargdescr_[],
							 union meltparam_un
							 *meltxargtab_,
							 const
							 melt_argdescr_cell_t
							 meltxresdescr_[],
							 union meltparam_un
							 *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_47_warmelt_base_REGISTER_ALL_IPA_PASSES_END_HOOK_FIRST
(meltclosure_ptr_t meltclosp_, melt_ptr_t meltfirstargp_, const melt_argdescr_cell_t meltxargdescr_[],
union meltparam_un *meltxargtab_, const melt_argdescr_cell_t meltxresdescr_[],
union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_48_warmelt_base_REGISTER_ALL_IPA_PASSES_END_HOOK_LAST
(meltclosure_ptr_t meltclosp_, melt_ptr_t meltfirstargp_, const melt_argdescr_cell_t meltxargdescr_[],
union meltparam_un *meltxargtab_, const melt_argdescr_cell_t meltxresdescr_[],
union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_49_warmelt_base_MELT_EARLY_GIMPLE_PASSES_START_RUNNER
(meltclosure_ptr_t meltclosp_, melt_ptr_t meltfirstargp_, const melt_argdescr_cell_t meltxargdescr_[],
union meltparam_un *meltxargtab_, const melt_argdescr_cell_t meltxresdescr_[],
union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_50_warmelt_base_REGISTER_EARLY_GIMPLE_PASSES_START_HOOK_FIRST
(meltclosure_ptr_t meltclosp_, melt_ptr_t meltfirstargp_, const melt_argdescr_cell_t meltxargdescr_[],
union meltparam_un *meltxargtab_, const melt_argdescr_cell_t meltxresdescr_[],
union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_51_warmelt_base_REGISTER_EARLY_GIMPLE_PASSES_START_HOOK_LAST
(meltclosure_ptr_t meltclosp_, melt_ptr_t meltfirstargp_, const melt_argdescr_cell_t meltxargdescr_[],
union meltparam_un *meltxargtab_, const melt_argdescr_cell_t meltxresdescr_[],
union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_52_warmelt_base_MELT_EARLY_GIMPLE_PASSES_END_RUNNER
(meltclosure_ptr_t meltclosp_, melt_ptr_t meltfirstargp_, const melt_argdescr_cell_t meltxargdescr_[],
union meltparam_un *meltxargtab_, const melt_argdescr_cell_t meltxresdescr_[],
union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_53_warmelt_base_REGISTER_EARLY_GIMPLE_PASSES_END_HOOK_FIRST
(meltclosure_ptr_t meltclosp_, melt_ptr_t meltfirstargp_, const melt_argdescr_cell_t meltxargdescr_[],
union meltparam_un *meltxargtab_, const melt_argdescr_cell_t meltxresdescr_[],
union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_54_warmelt_base_REGISTER_EARLY_GIMPLE_PASSES_END_HOOK_LAST
(meltclosure_ptr_t meltclosp_, melt_ptr_t meltfirstargp_, const melt_argdescr_cell_t meltxargdescr_[],
union meltparam_un *meltxargtab_, const melt_argdescr_cell_t meltxresdescr_[],
union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_55_warmelt_base_SET_REFERENCE (meltclosure_ptr_t meltclosp_,
					melt_ptr_t meltfirstargp_,
					const melt_argdescr_cell_t
					meltxargdescr_[],
					union meltparam_un *meltxargtab_,
					const melt_argdescr_cell_t
					meltxresdescr_[],
					union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_56_warmelt_base_INSTALL_VALUE_DESCRIPTOR (meltclosure_ptr_t
						   meltclosp_,
						   melt_ptr_t meltfirstargp_,
						   const melt_argdescr_cell_t
						   meltxargdescr_[],
						   union meltparam_un
						   *meltxargtab_,
						   const melt_argdescr_cell_t
						   meltxresdescr_[],
						   union meltparam_un
						   *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_57_warmelt_base_RETRIEVE_VALUE_DESCRIPTOR_LIST (meltclosure_ptr_t
							 meltclosp_,
							 melt_ptr_t
							 meltfirstargp_,
							 const
							 melt_argdescr_cell_t
							 meltxargdescr_[],
							 union meltparam_un
							 *meltxargtab_,
							 const
							 melt_argdescr_cell_t
							 meltxresdescr_[],
							 union meltparam_un
							 *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_58_warmelt_base_LIST_REMOVE_LAST_ELEMENT (meltclosure_ptr_t
						   meltclosp_,
						   melt_ptr_t meltfirstargp_,
						   const melt_argdescr_cell_t
						   meltxargdescr_[],
						   union meltparam_un
						   *meltxargtab_,
						   const melt_argdescr_cell_t
						   meltxresdescr_[],
						   union meltparam_un
						   *meltxrestab_);



MELT_EXTERN void *melt_start_this_module (void *);


/* define different names when debugging or not */
#if MELT_HAVE_DEBUG
MELT_EXTERN const char meltmodule_warmelt_base__melt_have_debug_enabled[];
#define melt_have_debug_string meltmodule_warmelt_base__melt_have_debug_enabled
#else /*!MELT_HAVE_DEBUG */
MELT_EXTERN const char meltmodule_warmelt_base__melt_have_debug_disabled[];
#define melt_have_debug_string meltmodule_warmelt_base__melt_have_debug_disabled
#endif /*!MELT_HAVE_DEBUG */


struct frame_melt_start_this_module_st;
void MELT_MODULE_VISIBILITY meltmod__warmelt_base__initialmeltchunk_0 (struct
								       frame_melt_start_this_module_st
								       *,
								       char
								       *);
void MELT_MODULE_VISIBILITY meltmod__warmelt_base__initialmeltchunk_1 (struct
								       frame_melt_start_this_module_st
								       *,
								       char
								       *);
void MELT_MODULE_VISIBILITY meltmod__warmelt_base__initialmeltchunk_2 (struct
								       frame_melt_start_this_module_st
								       *,
								       char
								       *);
void MELT_MODULE_VISIBILITY meltmod__warmelt_base__initialmeltchunk_3 (struct
								       frame_melt_start_this_module_st
								       *,
								       char
								       *);
void MELT_MODULE_VISIBILITY meltmod__warmelt_base__initialmeltchunk_4 (struct
								       frame_melt_start_this_module_st
								       *,
								       char
								       *);
void MELT_MODULE_VISIBILITY meltmod__warmelt_base__initialmeltchunk_5 (struct
								       frame_melt_start_this_module_st
								       *,
								       char
								       *);
void MELT_MODULE_VISIBILITY meltmod__warmelt_base__initialmeltchunk_6 (struct
								       frame_melt_start_this_module_st
								       *,
								       char
								       *);
void MELT_MODULE_VISIBILITY meltmod__warmelt_base__initialmeltchunk_7 (struct
								       frame_melt_start_this_module_st
								       *,
								       char
								       *);
void MELT_MODULE_VISIBILITY meltmod__warmelt_base__initialmeltchunk_8 (struct
								       frame_melt_start_this_module_st
								       *,
								       char
								       *);
void MELT_MODULE_VISIBILITY meltmod__warmelt_base__initialmeltchunk_9 (struct
								       frame_melt_start_this_module_st
								       *,
								       char
								       *);
void MELT_MODULE_VISIBILITY meltmod__warmelt_base__initialmeltchunk_10 (struct
									frame_melt_start_this_module_st
									*,
									char
									*);
void MELT_MODULE_VISIBILITY meltmod__warmelt_base__initialmeltchunk_11 (struct
									frame_melt_start_this_module_st
									*,
									char
									*);
void MELT_MODULE_VISIBILITY meltmod__warmelt_base__initialmeltchunk_12 (struct
									frame_melt_start_this_module_st
									*,
									char
									*);
void MELT_MODULE_VISIBILITY meltmod__warmelt_base__initialmeltchunk_13 (struct
									frame_melt_start_this_module_st
									*,
									char
									*);
void MELT_MODULE_VISIBILITY meltmod__warmelt_base__initialmeltchunk_14 (struct
									frame_melt_start_this_module_st
									*,
									char
									*);
void MELT_MODULE_VISIBILITY meltmod__warmelt_base__initialmeltchunk_15 (struct
									frame_melt_start_this_module_st
									*,
									char
									*);
void MELT_MODULE_VISIBILITY meltmod__warmelt_base__initialmeltchunk_16 (struct
									frame_melt_start_this_module_st
									*,
									char
									*);
void MELT_MODULE_VISIBILITY meltmod__warmelt_base__initialmeltchunk_17 (struct
									frame_melt_start_this_module_st
									*,
									char
									*);
void MELT_MODULE_VISIBILITY meltmod__warmelt_base__initialmeltchunk_18 (struct
									frame_melt_start_this_module_st
									*,
									char
									*);
void MELT_MODULE_VISIBILITY meltmod__warmelt_base__initialmeltchunk_19 (struct
									frame_melt_start_this_module_st
									*,
									char
									*);
void MELT_MODULE_VISIBILITY meltmod__warmelt_base__initialmeltchunk_20 (struct
									frame_melt_start_this_module_st
									*,
									char
									*);
void MELT_MODULE_VISIBILITY meltmod__warmelt_base__initialmeltchunk_21 (struct
									frame_melt_start_this_module_st
									*,
									char
									*);
void MELT_MODULE_VISIBILITY meltmod__warmelt_base__initialmeltchunk_22 (struct
									frame_melt_start_this_module_st
									*,
									char
									*);
void MELT_MODULE_VISIBILITY meltmod__warmelt_base__initialmeltchunk_23 (struct
									frame_melt_start_this_module_st
									*,
									char
									*);
void MELT_MODULE_VISIBILITY meltmod__warmelt_base__initialmeltchunk_24 (struct
									frame_melt_start_this_module_st
									*,
									char
									*);
void MELT_MODULE_VISIBILITY meltmod__warmelt_base__initialmeltchunk_25 (struct
									frame_melt_start_this_module_st
									*,
									char
									*);
void MELT_MODULE_VISIBILITY meltmod__warmelt_base__initialmeltchunk_26 (struct
									frame_melt_start_this_module_st
									*,
									char
									*);
void MELT_MODULE_VISIBILITY meltmod__warmelt_base__initialmeltchunk_27 (struct
									frame_melt_start_this_module_st
									*,
									char
									*);
void MELT_MODULE_VISIBILITY meltmod__warmelt_base__initialmeltchunk_28 (struct
									frame_melt_start_this_module_st
									*,
									char
									*);
void MELT_MODULE_VISIBILITY meltmod__warmelt_base__initialmeltchunk_29 (struct
									frame_melt_start_this_module_st
									*,
									char
									*);
void MELT_MODULE_VISIBILITY meltmod__warmelt_base__initialmeltchunk_30 (struct
									frame_melt_start_this_module_st
									*,
									char
									*);
void MELT_MODULE_VISIBILITY meltmod__warmelt_base__initialmeltchunk_31 (struct
									frame_melt_start_this_module_st
									*,
									char
									*);
void MELT_MODULE_VISIBILITY meltmod__warmelt_base__initialmeltchunk_32 (struct
									frame_melt_start_this_module_st
									*,
									char
									*);
void MELT_MODULE_VISIBILITY meltmod__warmelt_base__initialmeltchunk_33 (struct
									frame_melt_start_this_module_st
									*,
									char
									*);
void MELT_MODULE_VISIBILITY meltmod__warmelt_base__initialmeltchunk_34 (struct
									frame_melt_start_this_module_st
									*,
									char
									*);
void MELT_MODULE_VISIBILITY meltmod__warmelt_base__initialmeltchunk_35 (struct
									frame_melt_start_this_module_st
									*,
									char
									*);
void MELT_MODULE_VISIBILITY meltmod__warmelt_base__initialmeltchunk_36 (struct
									frame_melt_start_this_module_st
									*,
									char
									*);
void MELT_MODULE_VISIBILITY meltmod__warmelt_base__initialmeltchunk_37 (struct
									frame_melt_start_this_module_st
									*,
									char
									*);
void MELT_MODULE_VISIBILITY meltmod__warmelt_base__initialmeltchunk_38 (struct
									frame_melt_start_this_module_st
									*,
									char
									*);
void MELT_MODULE_VISIBILITY meltmod__warmelt_base__initialmeltchunk_39 (struct
									frame_melt_start_this_module_st
									*,
									char
									*);
void MELT_MODULE_VISIBILITY meltmod__warmelt_base__initialmeltchunk_40 (struct
									frame_melt_start_this_module_st
									*,
									char
									*);
void MELT_MODULE_VISIBILITY meltmod__warmelt_base__initialmeltchunk_41 (struct
									frame_melt_start_this_module_st
									*,
									char
									*);
void MELT_MODULE_VISIBILITY meltmod__warmelt_base__initialmeltchunk_42 (struct
									frame_melt_start_this_module_st
									*,
									char
									*);
void MELT_MODULE_VISIBILITY meltmod__warmelt_base__initialmeltchunk_43 (struct
									frame_melt_start_this_module_st
									*,
									char
									*);
void MELT_MODULE_VISIBILITY meltmod__warmelt_base__initialmeltchunk_44 (struct
									frame_melt_start_this_module_st
									*,
									char
									*);
void MELT_MODULE_VISIBILITY meltmod__warmelt_base__initialmeltchunk_45 (struct
									frame_melt_start_this_module_st
									*,
									char
									*);
void MELT_MODULE_VISIBILITY meltmod__warmelt_base__initialmeltchunk_46 (struct
									frame_melt_start_this_module_st
									*,
									char
									*);
void MELT_MODULE_VISIBILITY meltmod__warmelt_base__initialmeltchunk_47 (struct
									frame_melt_start_this_module_st
									*,
									char
									*);
void MELT_MODULE_VISIBILITY meltmod__warmelt_base__initialmeltchunk_48 (struct
									frame_melt_start_this_module_st
									*,
									char
									*);
void MELT_MODULE_VISIBILITY meltmod__warmelt_base__initialmeltchunk_49 (struct
									frame_melt_start_this_module_st
									*,
									char
									*);
void MELT_MODULE_VISIBILITY meltmod__warmelt_base__initialmeltchunk_50 (struct
									frame_melt_start_this_module_st
									*,
									char
									*);
void MELT_MODULE_VISIBILITY meltmod__warmelt_base__initialmeltchunk_51 (struct
									frame_melt_start_this_module_st
									*,
									char
									*);
void MELT_MODULE_VISIBILITY meltmod__warmelt_base__initialmeltchunk_52 (struct
									frame_melt_start_this_module_st
									*,
									char
									*);
void MELT_MODULE_VISIBILITY meltmod__warmelt_base__initialmeltchunk_53 (struct
									frame_melt_start_this_module_st
									*,
									char
									*);
void MELT_MODULE_VISIBILITY meltmod__warmelt_base__initialmeltchunk_54 (struct
									frame_melt_start_this_module_st
									*,
									char
									*);
void MELT_MODULE_VISIBILITY meltmod__warmelt_base__initialmeltchunk_55 (struct
									frame_melt_start_this_module_st
									*,
									char
									*);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_base__forward_or_mark_module_start_frame (struct
							   melt_callframe_st
							   *fp, int marking);
#define meltmarking_melt_start_this_module  meltmod__warmelt_base__forward_or_mark_module_start_frame



/**** warmelt-base+01.c implementations ****/




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_5_warmelt_base_IV (meltclosure_ptr_t meltclosp_,
			    melt_ptr_t meltfirstargp_,
			    const melt_argdescr_cell_t meltxargdescr_[],
			    union meltparam_un *meltxargtab_,
			    const melt_argdescr_cell_t meltxresdescr_[],
			    union meltparam_un *meltxrestab_)
{
  long current_blocklevel_signals_meltrout_5_warmelt_base_IV_melt =
    melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_5_warmelt_base_IV_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 8
    melt_ptr_t mcfr_varptr[8];
#define MELTFRAM_NBVARNUM 5
    long mcfr_varnum[5];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_5_warmelt_base_IV is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_5_warmelt_base_IV_st *) meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 8; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_5_warmelt_base_IV nbval 8*/
  meltfram__.mcfr_nbvar = 8 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("%IV", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-base.melt:220:/ getarg");
 /*_.A__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.B__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.B__V3*/ meltfptr[2])) != NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {

    MELT_LOCATION ("warmelt-base.melt:223:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#IS_INTEGERBOX__L1*/ meltfnum[0] =
      (melt_magic_discr ((melt_ptr_t) ( /*_.A__V2*/ meltfptr[1])) ==
       MELTOBMAG_INT);;
    MELT_LOCATION ("warmelt-base.melt:223:/ cond");
    /*cond */ if ( /*_#IS_INTEGERBOX__L1*/ meltfnum[0])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-base.melt:224:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
   /*_#IS_INTEGERBOX__L2*/ meltfnum[1] =
	    (melt_magic_discr ((melt_ptr_t) ( /*_.B__V3*/ meltfptr[2])) ==
	     MELTOBMAG_INT);;
	  MELT_LOCATION ("warmelt-base.melt:224:/ cond");
	  /*cond */ if ( /*_#IS_INTEGERBOX__L2*/ meltfnum[1])	/*then */
	    {
	      /*^cond.then */
	      /*^block */
	      /*anyblock */
	      {

		MELT_LOCATION ("warmelt-base.melt:225:/ quasiblock");


     /*_#IB__L3*/ meltfnum[2] =
		  (melt_get_int ((melt_ptr_t) ( /*_.B__V3*/ meltfptr[2])));;
		MELT_LOCATION ("warmelt-base.melt:226:/ checksignal");
		MELT_CHECK_SIGNAL ();
		;
		/*^cond */
		/*cond */ if ( /*_#IB__L3*/ meltfnum[2])	/*then */
		  {
		    /*^cond.then */
		    /*^block */
		    /*anyblock */
		    {

       /*_#GET_INT__L4*/ meltfnum[3] =
			(melt_get_int
			 ((melt_ptr_t) ( /*_.A__V2*/ meltfptr[1])));;
		      /*^compute */
       /*_#I__L5*/ meltfnum[4] =
			(melt_imod
			 (( /*_#GET_INT__L4*/ meltfnum[3]),
			  ( /*_#IB__L3*/ meltfnum[2])));;
		      /*^compute */
       /*_.MAKE_INTEGERBOX__V8*/ meltfptr[7] =
			(meltgc_new_int
			 ((meltobject_ptr_t)
			  (( /*!DISCR_CONSTANT_INTEGER */ meltfrout->
			    tabval[0])), ( /*_#I__L5*/ meltfnum[4])));;
		      /*^compute */
		      /*_.IF___V7*/ meltfptr[6] =
			/*_.MAKE_INTEGERBOX__V8*/ meltfptr[7];;
		      /*epilog */

		      MELT_LOCATION ("warmelt-base.melt:226:/ clear");
		 /*clear *//*_#GET_INT__L4*/ meltfnum[3] = 0;
		      /*^clear */
		 /*clear *//*_#I__L5*/ meltfnum[4] = 0;
		      /*^clear */
		 /*clear *//*_.MAKE_INTEGERBOX__V8*/ meltfptr[7] = 0;
		    }
		    ;
		  }
		else
		  {		/*^cond.else */

      /*_.IF___V7*/ meltfptr[6] = NULL;;
		  }
		;
		/*^compute */
		/*_.LET___V6*/ meltfptr[5] = /*_.IF___V7*/ meltfptr[6];;

		MELT_LOCATION ("warmelt-base.melt:225:/ clear");
	       /*clear *//*_#IB__L3*/ meltfnum[2] = 0;
		/*^clear */
	       /*clear *//*_.IF___V7*/ meltfptr[6] = 0;
		/*_.IF___V5*/ meltfptr[4] = /*_.LET___V6*/ meltfptr[5];;
		/*epilog */

		MELT_LOCATION ("warmelt-base.melt:224:/ clear");
	       /*clear *//*_.LET___V6*/ meltfptr[5] = 0;
	      }
	      ;
	    }
	  else
	    {			/*^cond.else */

    /*_.IF___V5*/ meltfptr[4] = NULL;;
	    }
	  ;
	  /*^compute */
	  /*_.IF___V4*/ meltfptr[3] = /*_.IF___V5*/ meltfptr[4];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-base.melt:223:/ clear");
	     /*clear *//*_#IS_INTEGERBOX__L2*/ meltfnum[1] = 0;
	  /*^clear */
	     /*clear *//*_.IF___V5*/ meltfptr[4] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

  /*_.IF___V4*/ meltfptr[3] = NULL;;
      }
    ;
    MELT_LOCATION ("warmelt-base.melt:220:/ quasiblock");


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.IF___V4*/ meltfptr[3];;

    {
      MELT_LOCATION ("warmelt-base.melt:220:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_#IS_INTEGERBOX__L1*/ meltfnum[0] = 0;
    /*^clear */
	   /*clear *//*_.IF___V4*/ meltfptr[3] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("%IV", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_5_warmelt_base_IV_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_5_warmelt_base_IV */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_6_warmelt_base_COMPARE_NAMED_ALPHA (meltclosure_ptr_t meltclosp_,
					     melt_ptr_t meltfirstargp_,
					     const melt_argdescr_cell_t
					     meltxargdescr_[],
					     union meltparam_un *
					     meltxargtab_,
					     const melt_argdescr_cell_t
					     meltxresdescr_[],
					     union meltparam_un *
					     meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_6_warmelt_base_COMPARE_NAMED_ALPHA_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_6_warmelt_base_COMPARE_NAMED_ALPHA_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 19
    melt_ptr_t mcfr_varptr[19];
#define MELTFRAM_NBVARNUM 5
    long mcfr_varnum[5];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_6_warmelt_base_COMPARE_NAMED_ALPHA is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_6_warmelt_base_COMPARE_NAMED_ALPHA_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 19; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_6_warmelt_base_COMPARE_NAMED_ALPHA nbval 19*/
  meltfram__.mcfr_nbvar = 19 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("COMPARE_NAMED_ALPHA", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-base.melt:990:/ getarg");
 /*_.N1__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.N2__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.N2__V3*/ meltfptr[2])) != NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {

    MELT_LOCATION ("warmelt-base.melt:994:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#__L1*/ meltfnum[0] =
      (( /*_.N1__V2*/ meltfptr[1]) == ( /*_.N2__V3*/ meltfptr[2]));;
    MELT_LOCATION ("warmelt-base.melt:994:/ cond");
    /*cond */ if ( /*_#__L1*/ meltfnum[0])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-base.melt:995:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^quasiblock */


	  /*_.RETVAL___V1*/ meltfptr[0] =
	    ( /*!konst_0 */ meltfrout->tabval[0]);;
	  MELT_LOCATION ("warmelt-base.melt:995:/ putxtraresult");
	  if (!meltxrestab_ || !meltxresdescr_)
	    goto labend_rout;
	  if (meltxresdescr_[0] != MELTBPAR_PTR)
	    goto labend_rout;
	  if (meltxrestab_[0].meltbp_aptr)
	    *(meltxrestab_[0].meltbp_aptr) = (melt_ptr_t) (( /*nil */ NULL));
	  ;
	  /*^finalreturn */
	  ;
	  /*finalret */ goto labend_rout;
	  /*_.IFELSE___V4*/ meltfptr[3] = /*_.RETURN___V5*/ meltfptr[4];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-base.melt:994:/ clear");
	     /*clear *//*_.RETURN___V5*/ meltfptr[4] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-base.melt:996:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
   /*_#IS_NOT_A__L2*/ meltfnum[1] =
	    !melt_is_instance_of ((melt_ptr_t) ( /*_.N1__V2*/ meltfptr[1]),
				  (melt_ptr_t) (( /*!CLASS_NAMED */
						 meltfrout->tabval[1])));;
	  MELT_LOCATION ("warmelt-base.melt:996:/ cond");
	  /*cond */ if ( /*_#IS_NOT_A__L2*/ meltfnum[1])	/*then */
	    {
	      /*^cond.then */
	      /*^block */
	      /*anyblock */
	      {

		MELT_LOCATION ("warmelt-base.melt:997:/ checksignal");
		MELT_CHECK_SIGNAL ();
		;
		/*^quasiblock */


		/*_.RETVAL___V1*/ meltfptr[0] =
		  ( /*!konst_2 */ meltfrout->tabval[2]);;
		MELT_LOCATION ("warmelt-base.melt:997:/ putxtraresult");
		if (!meltxrestab_ || !meltxresdescr_)
		  goto labend_rout;
		if (meltxresdescr_[0] != MELTBPAR_PTR)
		  goto labend_rout;
		if (meltxrestab_[0].meltbp_aptr)
		  *(meltxrestab_[0].meltbp_aptr) =
		    (melt_ptr_t) (( /*nil */ NULL));
		;
		/*^finalreturn */
		;
		/*finalret */ goto labend_rout;
		/*_.IFELSE___V6*/ meltfptr[4] =
		  /*_.RETURN___V7*/ meltfptr[6];;
		/*epilog */

		MELT_LOCATION ("warmelt-base.melt:996:/ clear");
	       /*clear *//*_.RETURN___V7*/ meltfptr[6] = 0;
	      }
	      ;
	    }
	  else
	    {			/*^cond.else */

	      /*^block */
	      /*anyblock */
	      {

		MELT_LOCATION ("warmelt-base.melt:998:/ checksignal");
		MELT_CHECK_SIGNAL ();
		;
     /*_#IS_NOT_A__L3*/ meltfnum[2] =
		  !melt_is_instance_of ((melt_ptr_t)
					( /*_.N2__V3*/ meltfptr[2]),
					(melt_ptr_t) (( /*!CLASS_NAMED */
						       meltfrout->
						       tabval[1])));;
		MELT_LOCATION ("warmelt-base.melt:998:/ cond");
		/*cond */ if ( /*_#IS_NOT_A__L3*/ meltfnum[2])	/*then */
		  {
		    /*^cond.then */
		    /*^block */
		    /*anyblock */
		    {

		      MELT_LOCATION ("warmelt-base.melt:999:/ checksignal");
		      MELT_CHECK_SIGNAL ();
		      ;
		      /*^quasiblock */


		      /*_.RETVAL___V1*/ meltfptr[0] =
			( /*!konst_3 */ meltfrout->tabval[3]);;
		      MELT_LOCATION ("warmelt-base.melt:999:/ putxtraresult");
		      if (!meltxrestab_ || !meltxresdescr_)
			goto labend_rout;
		      if (meltxresdescr_[0] != MELTBPAR_PTR)
			goto labend_rout;
		      if (meltxrestab_[0].meltbp_aptr)
			*(meltxrestab_[0].meltbp_aptr) =
			  (melt_ptr_t) (( /*nil */ NULL));
		      ;
		      /*^finalreturn */
		      ;
		      /*finalret */ goto labend_rout;
		      /*_.IFELSE___V8*/ meltfptr[6] =
			/*_.RETURN___V9*/ meltfptr[8];;
		      /*epilog */

		      MELT_LOCATION ("warmelt-base.melt:998:/ clear");
		 /*clear *//*_.RETURN___V9*/ meltfptr[8] = 0;
		    }
		    ;
		  }
		else
		  {		/*^cond.else */

		    /*^block */
		    /*anyblock */
		    {

		      MELT_LOCATION ("warmelt-base.melt:1001:/ quasiblock");


		      /*^getslot */
		      {
			melt_ptr_t slot = NULL, obj = NULL;
			obj =
			  (melt_ptr_t) ( /*_.N1__V2*/ meltfptr[1]) /*=obj*/ ;
			melt_object_get_field (slot, obj, 1, "NAMED_NAME");
	/*_.SN1__V11*/ meltfptr[10] = slot;
		      };
		      ;
		      MELT_LOCATION ("warmelt-base.melt:1002:/ getslot");
		      {
			melt_ptr_t slot = NULL, obj = NULL;
			obj =
			  (melt_ptr_t) ( /*_.N2__V3*/ meltfptr[2]) /*=obj*/ ;
			melt_object_get_field (slot, obj, 1, "NAMED_NAME");
	/*_.SN2__V12*/ meltfptr[11] = slot;
		      };
		      ;
		      MELT_LOCATION ("warmelt-base.melt:1004:/ checksignal");
		      MELT_CHECK_SIGNAL ();
		      ;
       /*_#STRING___L4*/ meltfnum[3] =
			melt_string_less ((melt_ptr_t)
					  ( /*_.SN1__V11*/ meltfptr[10]),
					  (melt_ptr_t) ( /*_.SN2__V12*/
							meltfptr[11]));;
		      MELT_LOCATION ("warmelt-base.melt:1004:/ cond");
		      /*cond */ if ( /*_#STRING___L4*/ meltfnum[3])	/*then */
			{
			  /*^cond.then */
			  /*^block */
			  /*anyblock */
			  {

			    MELT_LOCATION
			      ("warmelt-base.melt:1005:/ checksignal");
			    MELT_CHECK_SIGNAL ();
			    ;
			    /*^quasiblock */


			    /*_.RETVAL___V1*/ meltfptr[0] =
			      ( /*!konst_4 */ meltfrout->tabval[4]);;
			    MELT_LOCATION
			      ("warmelt-base.melt:1005:/ putxtraresult");
			    if (!meltxrestab_ || !meltxresdescr_)
			      goto labend_rout;
			    if (meltxresdescr_[0] != MELTBPAR_PTR)
			      goto labend_rout;
			    if (meltxrestab_[0].meltbp_aptr)
			      *(meltxrestab_[0].meltbp_aptr) =
				(melt_ptr_t) (( /*nil */ NULL));
			    ;
			    /*^finalreturn */
			    ;
			    /*finalret */ goto labend_rout;
			    /*_.IFELSE___V13*/ meltfptr[12] =
			      /*_.RETURN___V14*/ meltfptr[13];;
			    /*epilog */

			    MELT_LOCATION ("warmelt-base.melt:1004:/ clear");
		   /*clear *//*_.RETURN___V14*/ meltfptr[13] = 0;
			  }
			  ;
			}
		      else
			{	/*^cond.else */

			  /*^block */
			  /*anyblock */
			  {

			    MELT_LOCATION
			      ("warmelt-base.melt:1006:/ checksignal");
			    MELT_CHECK_SIGNAL ();
			    ;
	 /*_#STRING___L5*/ meltfnum[4] =
			      melt_string_less ((melt_ptr_t)
						( /*_.SN2__V12*/
						 meltfptr[11]),
						(melt_ptr_t) ( /*_.SN1__V11*/
							      meltfptr[10]));;
			    MELT_LOCATION ("warmelt-base.melt:1006:/ cond");
			    /*cond */ if ( /*_#STRING___L5*/ meltfnum[4])	/*then */
			      {
				/*^cond.then */
				/*^block */
				/*anyblock */
				{

				  MELT_LOCATION
				    ("warmelt-base.melt:1007:/ checksignal");
				  MELT_CHECK_SIGNAL ();
				  ;
				  /*^quasiblock */


				  /*_.RETVAL___V1*/ meltfptr[0] =
				    ( /*!konst_5 */ meltfrout->tabval[5]);;
				  MELT_LOCATION
				    ("warmelt-base.melt:1007:/ putxtraresult");
				  if (!meltxrestab_ || !meltxresdescr_)
				    goto labend_rout;
				  if (meltxresdescr_[0] != MELTBPAR_PTR)
				    goto labend_rout;
				  if (meltxrestab_[0].meltbp_aptr)
				    *(meltxrestab_[0].meltbp_aptr) =
				      (melt_ptr_t) (( /*nil */ NULL));
				  ;
				  /*^finalreturn */
				  ;
				  /*finalret */ goto labend_rout;
				  /*_.IFELSE___V15*/ meltfptr[13] =
				    /*_.RETURN___V16*/ meltfptr[15];;
				  /*epilog */

				  MELT_LOCATION
				    ("warmelt-base.melt:1006:/ clear");
		     /*clear *//*_.RETURN___V16*/ meltfptr[15] =
				    0;
				}
				;
			      }
			    else
			      {	/*^cond.else */

				/*^block */
				/*anyblock */
				{

				  MELT_LOCATION
				    ("warmelt-base.melt:1009:/ checksignal");
				  MELT_CHECK_SIGNAL ();
				  ;
				  /*^quasiblock */


				  /*_.RETVAL___V1*/ meltfptr[0] =
				    ( /*!konst_6 */ meltfrout->tabval[6]);;
				  MELT_LOCATION
				    ("warmelt-base.melt:1009:/ putxtraresult");
				  if (!meltxrestab_ || !meltxresdescr_)
				    goto labend_rout;
				  if (meltxresdescr_[0] != MELTBPAR_PTR)
				    goto labend_rout;
				  if (meltxrestab_[0].meltbp_aptr)
				    *(meltxrestab_[0].meltbp_aptr) =
				      (melt_ptr_t) (( /*nil */ NULL));
				  ;
				  /*^finalreturn */
				  ;
				  /*finalret */ goto labend_rout;
				  MELT_LOCATION
				    ("warmelt-base.melt:1008:/ quasiblock");


				  /*_.PROGN___V18*/ meltfptr[17] =
				    /*_.RETURN___V17*/ meltfptr[15];;
				  /*^compute */
				  /*_.IFELSE___V15*/ meltfptr[13] =
				    /*_.PROGN___V18*/ meltfptr[17];;
				  /*epilog */

				  MELT_LOCATION
				    ("warmelt-base.melt:1006:/ clear");
		     /*clear *//*_.RETURN___V17*/ meltfptr[15] =
				    0;
				  /*^clear */
		     /*clear *//*_.PROGN___V18*/ meltfptr[17] =
				    0;
				}
				;
			      }
			    ;
			    /*_.IFELSE___V13*/ meltfptr[12] =
			      /*_.IFELSE___V15*/ meltfptr[13];;
			    /*epilog */

			    MELT_LOCATION ("warmelt-base.melt:1004:/ clear");
		   /*clear *//*_#STRING___L5*/ meltfnum[4] = 0;
			    /*^clear */
		   /*clear *//*_.IFELSE___V15*/ meltfptr[13] = 0;
			  }
			  ;
			}
		      ;
		      /*_.LET___V10*/ meltfptr[8] =
			/*_.IFELSE___V13*/ meltfptr[12];;

		      MELT_LOCATION ("warmelt-base.melt:1001:/ clear");
		 /*clear *//*_.SN1__V11*/ meltfptr[10] = 0;
		      /*^clear */
		 /*clear *//*_.SN2__V12*/ meltfptr[11] = 0;
		      /*^clear */
		 /*clear *//*_#STRING___L4*/ meltfnum[3] = 0;
		      /*^clear */
		 /*clear *//*_.IFELSE___V13*/ meltfptr[12] = 0;
		      MELT_LOCATION ("warmelt-base.melt:1000:/ quasiblock");


		      /*_.PROGN___V19*/ meltfptr[15] =
			/*_.LET___V10*/ meltfptr[8];;
		      /*^compute */
		      /*_.IFELSE___V8*/ meltfptr[6] =
			/*_.PROGN___V19*/ meltfptr[15];;
		      /*epilog */

		      MELT_LOCATION ("warmelt-base.melt:998:/ clear");
		 /*clear *//*_.LET___V10*/ meltfptr[8] = 0;
		      /*^clear */
		 /*clear *//*_.PROGN___V19*/ meltfptr[15] = 0;
		    }
		    ;
		  }
		;
		/*_.IFELSE___V6*/ meltfptr[4] =
		  /*_.IFELSE___V8*/ meltfptr[6];;
		/*epilog */

		MELT_LOCATION ("warmelt-base.melt:996:/ clear");
	       /*clear *//*_#IS_NOT_A__L3*/ meltfnum[2] = 0;
		/*^clear */
	       /*clear *//*_.IFELSE___V8*/ meltfptr[6] = 0;
	      }
	      ;
	    }
	  ;
	  /*_.IFELSE___V4*/ meltfptr[3] = /*_.IFELSE___V6*/ meltfptr[4];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-base.melt:994:/ clear");
	     /*clear *//*_#IS_NOT_A__L2*/ meltfnum[1] = 0;
	  /*^clear */
	     /*clear *//*_.IFELSE___V6*/ meltfptr[4] = 0;
	}
	;
      }
    ;
    MELT_LOCATION ("warmelt-base.melt:990:/ quasiblock");


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.IFELSE___V4*/ meltfptr[3];;

    {
      MELT_LOCATION ("warmelt-base.melt:990:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_#__L1*/ meltfnum[0] = 0;
    /*^clear */
	   /*clear *//*_.IFELSE___V4*/ meltfptr[3] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("COMPARE_NAMED_ALPHA", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_6_warmelt_base_COMPARE_NAMED_ALPHA_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_6_warmelt_base_COMPARE_NAMED_ALPHA */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_7_warmelt_base_ADD2OUT (meltclosure_ptr_t meltclosp_,
				 melt_ptr_t meltfirstargp_,
				 const melt_argdescr_cell_t meltxargdescr_[],
				 union meltparam_un * meltxargtab_,
				 const melt_argdescr_cell_t meltxresdescr_[],
				 union meltparam_un * meltxrestab_)
{
  /*variadic */ int variad_ADD2OUT_ix = 0, variad_ADD2OUT_len =
    melt_argdescr_length (meltxargdescr_);
#define melt_variadic_length  (0+variad_ADD2OUT_len)
#define melt_variadic_index variad_ADD2OUT_ix

  long current_blocklevel_signals_meltrout_7_warmelt_base_ADD2OUT_melt =
    melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_7_warmelt_base_ADD2OUT_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 29
    melt_ptr_t mcfr_varptr[29];
#define MELTFRAM_NBVARNUM 5
    long mcfr_varnum[5];
/*others*/
    const char *loc_CSTRING__o0;
    tree loc_TREE__o1;
    gimple loc_GIMPLE__o2;
    gimple_seq loc_GIMPLE_SEQ__o3;
    edge loc_EDGE__o4;
    loop_p loc_LOOP__o5;
    const char *loc_CSTRING__o6;
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_7_warmelt_base_ADD2OUT is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_7_warmelt_base_ADD2OUT_st *) meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 29; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      if (meltframptr_->loc_TREE__o1)
	gt_ggc_mx_tree_node (meltframptr_->loc_TREE__o1);
      if (meltframptr_->loc_GIMPLE__o2)
	gt_ggc_mx_gimple_statement_d (meltframptr_->loc_GIMPLE__o2);
      if (meltframptr_->loc_GIMPLE_SEQ__o3)
	gt_ggc_mx_gimple_seq_d (meltframptr_->loc_GIMPLE_SEQ__o3);
      if (meltframptr_->loc_EDGE__o4)
	gt_ggc_mx_edge_def (meltframptr_->loc_EDGE__o4);
      if (meltframptr_->loc_LOOP__o5)
	gt_ggc_mx_loop (meltframptr_->loc_LOOP__o5);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_7_warmelt_base_ADD2OUT nbval 29*/
  meltfram__.mcfr_nbvar = 29 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("ADD2OUT", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-base.melt:1305:/ getarg");
 /*_.OUT__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {

    MELT_LOCATION ("warmelt-base.melt:1309:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#IS_OUT__L1*/ meltfnum[0] =
      (melt_is_out ((melt_ptr_t) /*_.OUT__V2*/ meltfptr[1]));;
    /*^compute */
 /*_#NOT__L2*/ meltfnum[1] =
      (!( /*_#IS_OUT__L1*/ meltfnum[0]));;
    MELT_LOCATION ("warmelt-base.melt:1309:/ cond");
    /*cond */ if ( /*_#NOT__L2*/ meltfnum[1])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-base.melt:1310:/ quasiblock");


   /*_.RETVAL___V1*/ meltfptr[0] = NULL;;

	  {
	    MELT_LOCATION ("warmelt-base.melt:1310:/ locexp");
	    /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
	    if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	      melt_warn_for_no_expected_secondary_results ();
	    /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
	    ;
	  }
	  ;
	  /*^finalreturn */
	  ;
	  /*finalret */ goto labend_rout;
	  /*_.IF___V3*/ meltfptr[2] = /*_.RETURN___V4*/ meltfptr[3];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-base.melt:1309:/ clear");
	     /*clear *//*_.RETURN___V4*/ meltfptr[3] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

  /*_.IF___V3*/ meltfptr[2] = NULL;;
      }
    ;
    MELT_LOCATION ("warmelt-base.melt:1311:/ loop");
    /*loop */
    {
    labloop_ARGLOOP_1:;	/*^loopbody */

      /*^block */
      /*anyblock */
      {

	/*^checksignal */
	MELT_CHECK_SIGNAL ();
	;
	MELT_LOCATION ("warmelt-base.melt:1313:/ cond");
	/*cond */ if ( /*ifvariadic nomore */ variad_ADD2OUT_ix == variad_ADD2OUT_len)	/*then */
	  {
	    /*^cond.then */
	    /*^block */
	    /*anyblock */
	    {

	      /*^compute */

	      /*consume variadic  ! */ variad_ADD2OUT_ix += 0;;
	      MELT_LOCATION ("warmelt-base.melt:1315:/ quasiblock");


	      /*^compute */
     /*_.ARGLOOP__V6*/ meltfptr[5] = NULL;;

	      /*^exit */
	      /*exit */
	      {
		goto labexit_ARGLOOP_1;
	      }
	      ;
	      /*epilog */
	    }
	    ;
	  }
	else
	  {
	    MELT_LOCATION ("warmelt-base.melt:1313:/ cond.else");

	    /*^block */
	    /*anyblock */
	    {

	      /*^cond */
	      /*cond */ if ( /*ifvariadic arg#1 */ variad_ADD2OUT_ix >= 0 && variad_ADD2OUT_ix + 1 <= variad_ADD2OUT_len && meltxargdescr_[variad_ADD2OUT_ix] == MELTBPAR_PTR)	/*then */
		{
		  /*^cond.then */
		  /*^block */
		  /*anyblock */
		  {

		    /*^compute */
       /*_.V__V7*/ meltfptr[6] =
		      /*variadic argument value */
		      ((meltxargtab_[variad_ADD2OUT_ix + 0].
			meltbp_aptr) ? (*(meltxargtab_[variad_ADD2OUT_ix + 0].
					  meltbp_aptr)) : NULL);;
		    /*^compute */

		    /*consume variadic Value ! */ variad_ADD2OUT_ix += 1;;
		    MELT_LOCATION ("warmelt-base.melt:1317:/ checksignal");
		    MELT_CHECK_SIGNAL ();
		    ;
       /*_#IS_CLOSURE__L3*/ meltfnum[2] =
		      (melt_magic_discr
		       ((melt_ptr_t) ( /*_.V__V7*/ meltfptr[6])) ==
		       MELTOBMAG_CLOSURE);;
		    MELT_LOCATION ("warmelt-base.melt:1317:/ cond");
		    /*cond */ if ( /*_#IS_CLOSURE__L3*/ meltfnum[2])	/*then */
		      {
			/*^cond.then */
			/*^block */
			/*anyblock */
			{

			  MELT_LOCATION ("warmelt-base.melt:1318:/ cond");
			  /*cond */ if ( /*ifvariadic arg#1 */ variad_ADD2OUT_ix >= 0 && variad_ADD2OUT_ix + 1 <= variad_ADD2OUT_len && meltxargdescr_[variad_ADD2OUT_ix] == MELTBPAR_PTR)	/*then */
			    {
			      /*^cond.then */
			      /*^block */
			      /*anyblock */
			      {

				/*^compute */
	   /*_.VV__V9*/ meltfptr[8] =
				  /*variadic argument value */
				  ((meltxargtab_[variad_ADD2OUT_ix + 0].
				    meltbp_aptr)
				   ? (*
				      (meltxargtab_[variad_ADD2OUT_ix + 0].
				       meltbp_aptr)) : NULL);;
				/*^compute */

				/*consume variadic Value ! */
				  variad_ADD2OUT_ix += 1;;
				MELT_LOCATION
				  ("warmelt-base.melt:1320:/ checksignal");
				MELT_CHECK_SIGNAL ();
				;
				/*^apply */
				/*apply */
				{
				  union meltparam_un argtab[1];
				  memset (&argtab, 0, sizeof (argtab));
				  /*^apply.arg */
				  argtab[0].meltbp_aptr =
				    (melt_ptr_t *) & /*_.VV__V9*/ meltfptr[8];
				  /*_.V__V10*/ meltfptr[9] =
				    melt_apply ((meltclosure_ptr_t)
						( /*_.V__V7*/ meltfptr[6]),
						(melt_ptr_t) ( /*_.OUT__V2*/
							      meltfptr[1]),
						(MELTBPARSTR_PTR ""), argtab,
						"", (union meltparam_un *) 0);
				}
				;
				/*_.IFELSE___V8*/ meltfptr[7] =
				  /*_.V__V10*/ meltfptr[9];;
				/*epilog */

				MELT_LOCATION
				  ("warmelt-base.melt:1318:/ clear");
		     /*clear *//*_.VV__V9*/ meltfptr[8] = 0;
				/*^clear */
		     /*clear *//*_.V__V10*/ meltfptr[9] = 0;
			      }
			      ;
			    }
			  else
			    {	/*^cond.else */

			      /*^block */
			      /*anyblock */
			      {

				/*^cond */
				/*cond */ if ( /*ifvariadic arg#1 */ variad_ADD2OUT_ix >= 0 && variad_ADD2OUT_ix + 1 <= variad_ADD2OUT_len && meltxargdescr_[variad_ADD2OUT_ix] == MELTBPAR_LONG)	/*then */
				  {
				    /*^cond.then */
				    /*^block */
				    /*anyblock */
				    {

				      /*^compute */
	     /*_#LL__L4*/ meltfnum[3] =
					/*variadic argument stuff */
					meltxargtab_[variad_ADD2OUT_ix +
						     0].meltbp_long;;
				      /*^compute */

				      /*consume variadic LONG ! */
					variad_ADD2OUT_ix += 1;;
				      MELT_LOCATION
					("warmelt-base.melt:1322:/ checksignal");
				      MELT_CHECK_SIGNAL ();
				      ;
				      /*^apply */
				      /*apply */
				      {
					union meltparam_un argtab[1];
					memset (&argtab, 0, sizeof (argtab));
					/*^apply.arg */
					argtab[0].meltbp_long =
					  /*_#LL__L4*/ meltfnum[3];
					/*_.V__V11*/ meltfptr[8] =
					  melt_apply ((meltclosure_ptr_t)
						      ( /*_.V__V7*/
						       meltfptr[6]),
						      (melt_ptr_t) ( /*_.OUT__V2*/ meltfptr[1]), (MELTBPARSTR_LONG ""), argtab, "", (union meltparam_un *) 0);
				      }
				      ;
				      /*_.IFELSE___V8*/ meltfptr[7] =
					/*_.V__V11*/ meltfptr[8];;
				      /*epilog */

				      MELT_LOCATION
					("warmelt-base.melt:1318:/ clear");
		       /*clear *//*_#LL__L4*/ meltfnum[3] = 0;
				      /*^clear */
		       /*clear *//*_.V__V11*/ meltfptr[8] = 0;
				    }
				    ;
				  }
				else
				  {	/*^cond.else */

				    /*^block */
				    /*anyblock */
				    {

				      /*^cond */
				      /*cond */ if ( /*ifvariadic arg#1 */ variad_ADD2OUT_ix >= 0 && variad_ADD2OUT_ix + 1 <= variad_ADD2OUT_len && meltxargdescr_[variad_ADD2OUT_ix] == MELTBPAR_CSTRING)	/*then */
					{
					  /*^cond.then */
					  /*^block */
					  /*anyblock */
					  {

					    /*^compute */
	       /*_?*/ meltfram__.
					      loc_CSTRING__o0 =
					      /*variadic argument stuff */
					      meltxargtab_[variad_ADD2OUT_ix +
							   0].meltbp_cstring;;
					    /*^compute */

					    /*consume variadic CSTRING ! */
					      variad_ADD2OUT_ix += 1;;
					    MELT_LOCATION
					      ("warmelt-base.melt:1324:/ checksignal");
					    MELT_CHECK_SIGNAL ();
					    ;
					    /*^apply */
					    /*apply */
					    {
					      union meltparam_un argtab[1];
					      memset (&argtab, 0,
						      sizeof (argtab));
					      /*^apply.arg */
					      argtab[0].meltbp_cstring =
						/*_?*/
						meltfram__.loc_CSTRING__o0;
					      /*_.V__V12*/ meltfptr[9] =
						melt_apply ((meltclosure_ptr_t) ( /*_.V__V7*/ meltfptr[6]), (melt_ptr_t) ( /*_.OUT__V2*/ meltfptr[1]), (MELTBPARSTR_CSTRING ""), argtab, "", (union meltparam_un *) 0);
					    }
					    ;
					    /*_.IFELSE___V8*/ meltfptr[7] =
					      /*_.V__V12*/ meltfptr[9];;
					    /*epilog */

					    MELT_LOCATION
					      ("warmelt-base.melt:1318:/ clear");
			 /*clear *//*_?*/ meltfram__.
					      loc_CSTRING__o0 = 0;
					    /*^clear */
			 /*clear *//*_.V__V12*/ meltfptr[9]
					      = 0;
					  }
					  ;
					}
				      else
					{	/*^cond.else */

					  /*^block */
					  /*anyblock */
					  {

					    /*^cond */
					    /*cond */ if ( /*ifvariadic arg#1 */ variad_ADD2OUT_ix >= 0 && variad_ADD2OUT_ix + 1 <= variad_ADD2OUT_len && meltxargdescr_[variad_ADD2OUT_ix] == MELTBPAR_TREE)	/*then */
					      {
						/*^cond.then */
						/*^block */
						/*anyblock */
						{

						  /*^compute */
		 /*_?*/ meltfram__.
						    loc_TREE__o1 =
						    /*variadic argument stuff */
						    meltxargtab_
						    [variad_ADD2OUT_ix +
						     0].meltbp_tree;;
						  /*^compute */

						  /*consume variadic TREE ! */
						    variad_ADD2OUT_ix += 1;;
						  MELT_LOCATION
						    ("warmelt-base.melt:1326:/ checksignal");
						  MELT_CHECK_SIGNAL ();
						  ;
						  /*^apply */
						  /*apply */
						  {
						    union meltparam_un
						      argtab[1];
						    memset (&argtab, 0,
							    sizeof (argtab));
						    /*^apply.arg */
						    argtab[0].meltbp_tree =
						      /*_?*/
						      meltfram__.loc_TREE__o1;
						    /*_.V__V13*/ meltfptr[8] =
						      melt_apply ((meltclosure_ptr_t) ( /*_.V__V7*/ meltfptr[6]), (melt_ptr_t) ( /*_.OUT__V2*/ meltfptr[1]), (MELTBPARSTR_TREE ""), argtab, "", (union meltparam_un *) 0);
						  }
						  ;
						  /*_.IFELSE___V8*/
						    meltfptr[7] =
						    /*_.V__V13*/ meltfptr[8];;
						  /*epilog */

						  MELT_LOCATION
						    ("warmelt-base.melt:1318:/ clear");
			   /*clear *//*_?*/ meltfram__.
						    loc_TREE__o1 = 0;
						  /*^clear */
			   /*clear *//*_.V__V13*/
						    meltfptr[8] = 0;
						}
						;
					      }
					    else
					      {	/*^cond.else */

						/*^block */
						/*anyblock */
						{

						  /*^cond */
						  /*cond */ if ( /*ifvariadic arg#1 */ variad_ADD2OUT_ix >= 0 && variad_ADD2OUT_ix + 1 <= variad_ADD2OUT_len && meltxargdescr_[variad_ADD2OUT_ix] == MELTBPAR_GIMPLE)	/*then */
						    {
						      /*^cond.then */
						      /*^block */
						      /*anyblock */
						      {

							/*^compute */
		   /*_?*/ meltfram__.
							  loc_GIMPLE__o2 =
							  /*variadic argument stuff */
							  meltxargtab_
							  [variad_ADD2OUT_ix +
							   0].meltbp_gimple;;
							/*^compute */

							/*consume variadic GIMPLE ! */
							  variad_ADD2OUT_ix +=
							  1;;
							MELT_LOCATION
							  ("warmelt-base.melt:1328:/ checksignal");
							MELT_CHECK_SIGNAL ();
							;
							/*^apply */
							/*apply */
							{
							  union meltparam_un
							    argtab[1];
							  memset (&argtab, 0,
								  sizeof
								  (argtab));
							  /*^apply.arg */
							  argtab[0].
							    meltbp_gimple =
							    /*_?*/
							    meltfram__.
							    loc_GIMPLE__o2;
							  /*_.V__V14*/
							    meltfptr[9] =
							    melt_apply ((meltclosure_ptr_t) ( /*_.V__V7*/ meltfptr[6]), (melt_ptr_t) ( /*_.OUT__V2*/ meltfptr[1]), (MELTBPARSTR_GIMPLE ""), argtab, "", (union meltparam_un *) 0);
							}
							;
							/*_.IFELSE___V8*/
							  meltfptr[7] =
							  /*_.V__V14*/
							  meltfptr[9];;
							/*epilog */

							MELT_LOCATION
							  ("warmelt-base.melt:1318:/ clear");
			     /*clear *//*_?*/
							  meltfram__.
							  loc_GIMPLE__o2 = 0;
							/*^clear */
			     /*clear *//*_.V__V14*/
							  meltfptr[9] = 0;
						      }
						      ;
						    }
						  else
						    {	/*^cond.else */

						      /*^block */
						      /*anyblock */
						      {

							/*^cond */
							/*cond */ if ( /*ifvariadic arg#1 */ variad_ADD2OUT_ix >= 0 && variad_ADD2OUT_ix + 1 <= variad_ADD2OUT_len && meltxargdescr_[variad_ADD2OUT_ix] == MELTBPAR_GIMPLESEQ)	/*then */
							  {
							    /*^cond.then */
							    /*^block */
							    /*anyblock */
							    {

							      /*^compute */
		     /*_?*/
								meltfram__.
								loc_GIMPLE_SEQ__o3
								=
								/*variadic argument stuff */
								meltxargtab_
								[variad_ADD2OUT_ix
								 +
								 0].
								meltbp_gimpleseq;;
							      /*^compute */

							      /*consume variadic GIMPLE_SEQ ! */
								variad_ADD2OUT_ix
								+= 1;;
							      MELT_LOCATION
								("warmelt-base.melt:1330:/ checksignal");
							      MELT_CHECK_SIGNAL
								();
							      ;
							      /*^apply */
							      /*apply */
							      {
								union
								  meltparam_un
								  argtab[1];
								memset
								  (&argtab, 0,
								   sizeof
								   (argtab));
								/*^apply.arg */
								argtab[0].
								  meltbp_gimpleseq
								  =
								  /*_?*/
								  meltfram__.
								  loc_GIMPLE_SEQ__o3;
								/*_.V__V15*/
								  meltfptr[8]
								  =
								  melt_apply ((meltclosure_ptr_t) ( /*_.V__V7*/ meltfptr[6]), (melt_ptr_t) ( /*_.OUT__V2*/ meltfptr[1]), (MELTBPARSTR_GIMPLESEQ ""), argtab, "", (union meltparam_un *) 0);
							      }
							      ;
							      /*_.IFELSE___V8*/
								meltfptr[7] =
								/*_.V__V15*/
								meltfptr[8];;
							      /*epilog */

							      MELT_LOCATION
								("warmelt-base.melt:1318:/ clear");
			       /*clear *//*_?*/
								meltfram__.
								loc_GIMPLE_SEQ__o3
								= 0;
							      /*^clear */
			       /*clear *//*_.V__V15*/
								meltfptr
								[8] = 0;
							    }
							    ;
							  }
							else
							  {	/*^cond.else */

							    /*^block */
							    /*anyblock */
							    {

							      /*^cond */
							      /*cond */ if ( /*ifvariadic arg#1 */ variad_ADD2OUT_ix >= 0 && variad_ADD2OUT_ix + 1 <= variad_ADD2OUT_len && meltxargdescr_[variad_ADD2OUT_ix] == MELTBPAR_EDGE)	/*then */
								{
								  /*^cond.then */
								  /*^block */
								  /*anyblock */
								  {

								    /*^compute */
		       /*_?*/
								      meltfram__.
								      loc_EDGE__o4
								      =
								      /*variadic argument stuff */
								      meltxargtab_
								      [variad_ADD2OUT_ix
								       +
								       0].
								      meltbp_edge;;
								    /*^compute */

								    /*consume variadic EDGE ! */
								      variad_ADD2OUT_ix
								      += 1;;
								    MELT_LOCATION
								      ("warmelt-base.melt:1332:/ checksignal");
								    MELT_CHECK_SIGNAL
								      ();
								    ;
								    /*^apply */
								    /*apply */
								    {
								      union
									meltparam_un
									argtab
									[1];
								      memset
									(&argtab,
									 0,
									 sizeof
									 (argtab));
								      /*^apply.arg */
								      argtab
									[0].
									meltbp_edge
									=
									/*_?*/
									meltfram__.
									loc_EDGE__o4;
								      /*_.V__V16*/
									meltfptr
									[9] =
									melt_apply
									((meltclosure_ptr_t) ( /*_.V__V7*/ meltfptr[6]), (melt_ptr_t) ( /*_.OUT__V2*/ meltfptr[1]), (MELTBPARSTR_EDGE ""), argtab, "", (union meltparam_un *) 0);
								    }
								    ;
								    /*_.IFELSE___V8*/
								      meltfptr
								      [7] =
								      /*_.V__V16*/
								      meltfptr
								      [9];;
								    /*epilog */

								    MELT_LOCATION
								      ("warmelt-base.melt:1318:/ clear");
				 /*clear *//*_?*/
								      meltfram__.
								      loc_EDGE__o4
								      = 0;
								    /*^clear */
				 /*clear *//*_.V__V16*/
								      meltfptr
								      [9] = 0;
								  }
								  ;
								}
							      else
								{	/*^cond.else */

								  /*^block */
								  /*anyblock */
								  {

								    /*^cond */
								    /*cond */ if ( /*ifvariadic arg#1 */ variad_ADD2OUT_ix >= 0 && variad_ADD2OUT_ix + 1 <= variad_ADD2OUT_len && meltxargdescr_[variad_ADD2OUT_ix] == MELTBPAR_LOOP)	/*then */
								      {
									/*^cond.then */
									/*^block */
									/*anyblock */
									{

									  /*^compute */
			 /*_?*/
									    meltfram__.
									    loc_LOOP__o5
									    =
									    /*variadic argument stuff */
									    meltxargtab_
									    [variad_ADD2OUT_ix
									     +
									     0].
									    meltbp_loop;;
									  /*^compute */

									  /*consume variadic LOOP ! */
									    variad_ADD2OUT_ix
									    +=
									    1;;
									  MELT_LOCATION
									    ("warmelt-base.melt:1334:/ checksignal");
									  MELT_CHECK_SIGNAL
									    ();
									  ;
									  /*^apply */
									  /*apply */
									  {
									    union
									      meltparam_un
									      argtab
									      [1];
									    memset
									      (&argtab,
									       0,
									       sizeof
									       (argtab));
									    /*^apply.arg */
									    argtab
									      [0].
									      meltbp_loop
									      =
									      /*_?*/
									      meltfram__.
									      loc_LOOP__o5;
									    /*_.V__V17*/
									      meltfptr
									      [8]
									      =
									      melt_apply
									      ((meltclosure_ptr_t) ( /*_.V__V7*/ meltfptr[6]), (melt_ptr_t) ( /*_.OUT__V2*/ meltfptr[1]), (MELTBPARSTR_LOOP ""), argtab, "", (union meltparam_un *) 0);
									  }
									  ;
									  /*_.IFELSE___V8*/
									    meltfptr
									    [7]
									    =
									    /*_.V__V17*/
									    meltfptr
									    [8];;
									  /*epilog */

									  MELT_LOCATION
									    ("warmelt-base.melt:1318:/ clear");
				   /*clear *//*_?*/
									    meltfram__.
									    loc_LOOP__o5
									    =
									    0;
									  /*^clear */
				   /*clear *//*_.V__V17*/
									    meltfptr
									    [8]
									    =
									    0;
									}
									;
								      }
								    else
								      {	/*^cond.else */

									/*^block */
									/*anyblock */
									{

									  MELT_LOCATION
									    ("warmelt-base.melt:1336:/ quasiblock");


			 /*_.VCTY__V19*/
									    meltfptr
									    [8]
									    =
									    /*variadic_type_code */
#ifdef melt_variadic_index
									    (((melt_variadic_index + 0) >= 0 && (melt_variadic_index + 0) < melt_variadic_length) ? melt_code_to_ctype (meltxargdescr_[melt_variadic_index + 0] & MELT_ARGDESCR_MAX) : NULL)
#else
									    NULL	/* no variadic_ctype outside of variadic functions */
#endif /*melt_variadic_index */
									    ;;
									  MELT_LOCATION
									    ("warmelt-base.melt:1339:/ cond");
									  /*cond */ if (
											 /*ifisa */
											 melt_is_instance_of
											 ((melt_ptr_t) ( /*_.VCTY__V19*/ meltfptr[8]),
											  (melt_ptr_t) (( /*!CLASS_NAMED */ meltfrout->tabval[0])))
									    )	/*then */
									    {
									      /*^cond.then */
									      /*^getslot */
									      {
										melt_ptr_t
										  slot
										  =
										  NULL,
										  obj
										  =
										  NULL;
										obj
										  =
										  (melt_ptr_t)
										  ( /*_.VCTY__V19*/ meltfptr[8]) /*=obj*/ ;
										melt_object_get_field
										  (slot,
										   obj,
										   1,
										   "NAMED_NAME");
			   /*_.NAMED_NAME__V20*/
										  meltfptr
										  [19]
										  =
										  slot;
									      };
									      ;
									    }
									  else
									    {	/*^cond.else */

			  /*_.NAMED_NAME__V20*/
										meltfptr
										[19]
										=
										NULL;;
									    }
									  ;

									  {
									    MELT_LOCATION
									      ("warmelt-base.melt:1338:/ locexp");
									    error
									      ("MELT ERROR MSG [#%ld]::: %s - %s",
									       melt_dbgcounter,
									       ("ADD2OUT with manipulator for unsupported ctype"),
									       melt_string_str
									       ((melt_ptr_t) ( /*_.NAMED_NAME__V20*/ meltfptr[19])));
									  }
									  ;

#if MELT_HAVE_DEBUG
									  MELT_LOCATION
									    ("warmelt-base.melt:1340:/ cppif.then");
									  /*^block */
									  /*anyblock */
									  {

									    /*^checksignal */
									    MELT_CHECK_SIGNAL
									      ();
									    ;
									    /*^cond */
									    /*cond */ if (( /*nil */ NULL))	/*then */
									      {
										/*^cond.then */
										/*_.IFELSE___V22*/
										  meltfptr
										  [21]
										  =
										  ( /*nil */ NULL);;
									      }
									    else
									      {
										MELT_LOCATION
										  ("warmelt-base.melt:1340:/ cond.else");

										/*^block */
										/*anyblock */
										{




										  {
										    /*^locexp */
										    melt_assert_failed
										      (("invalid variadic argument after closure to ADD2OUT"), ("warmelt-base.melt") ? ("warmelt-base.melt") : __FILE__, (1340) ? (1340) : __LINE__, __FUNCTION__);
										    ;
										  }
										  ;
				       /*clear *//*_.IFELSE___V22*/
										    meltfptr
										    [21]
										    =
										    0;
										  /*epilog */
										}
										;
									      }
									    ;
									    /*^compute */
									    /*_.IFCPP___V21*/
									      meltfptr
									      [20]
									      =
									      /*_.IFELSE___V22*/
									      meltfptr
									      [21];;
									    /*epilog */

									    MELT_LOCATION
									      ("warmelt-base.melt:1340:/ clear");
				     /*clear *//*_.IFELSE___V22*/
									      meltfptr
									      [21]
									      =
									      0;
									  }

#else /*MELT_HAVE_DEBUG */
									  /*^cppif.else */
									  /*_.IFCPP___V21*/
									    meltfptr
									    [20]
									    =
									    ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
									  ;
									  MELT_LOCATION
									    ("warmelt-base.melt:1341:/ quasiblock");


			 /*_.RETVAL___V1*/
									    meltfptr
									    [0]
									    =
									    NULL;;

									  {
									    MELT_LOCATION
									      ("warmelt-base.melt:1341:/ locexp");
									    /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
									    if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
									      melt_warn_for_no_expected_secondary_results
										();
									    /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
									    ;
									  }
									  ;
									  /*^finalreturn */
									  ;
									  /*finalret */
									    goto
									    labend_rout;
									  /*_.LET___V18*/
									    meltfptr
									    [9]
									    =
									    /*_.RETURN___V23*/
									    meltfptr
									    [21];;

									  MELT_LOCATION
									    ("warmelt-base.melt:1336:/ clear");
				   /*clear *//*_.VCTY__V19*/
									    meltfptr
									    [8]
									    =
									    0;
									  /*^clear */
				   /*clear *//*_.NAMED_NAME__V20*/
									    meltfptr
									    [19]
									    =
									    0;
									  /*^clear */
				   /*clear *//*_.IFCPP___V21*/
									    meltfptr
									    [20]
									    =
									    0;
									  /*^clear */
				   /*clear *//*_.RETURN___V23*/
									    meltfptr
									    [21]
									    =
									    0;
									  /*_.IFELSE___V8*/
									    meltfptr
									    [7]
									    =
									    /*_.LET___V18*/
									    meltfptr
									    [9];;
									  /*epilog */

									  MELT_LOCATION
									    ("warmelt-base.melt:1318:/ clear");
				   /*clear *//*_.LET___V18*/
									    meltfptr
									    [9]
									    =
									    0;
									}
									;
								      }
								    ;
								    /*epilog */
								  }
								  ;
								}
							      ;
							      /*epilog */
							    }
							    ;
							  }
							;
							/*epilog */
						      }
						      ;
						    }
						  ;
						  /*epilog */
						}
						;
					      }
					    ;
					    /*epilog */
					  }
					  ;
					}
				      ;
				      /*epilog */
				    }
				    ;
				  }
				;
				/*epilog */
			      }
			      ;
			    }
			  ;
			  /*epilog */
			}
			;
		      }
		    else
		      {
			MELT_LOCATION ("warmelt-base.melt:1317:/ cond.else");

			/*^block */
			/*anyblock */
			{

			  MELT_LOCATION
			    ("warmelt-base.melt:1343:/ checksignal");
			  MELT_CHECK_SIGNAL ();
			  ;
			  /*^msend */
			  /*msend */
			  {
			    union meltparam_un argtab[1];
			    memset (&argtab, 0, sizeof (argtab));
			    /*^ojbmsend.arg */
			    argtab[0].meltbp_aptr =
			      (melt_ptr_t *) & /*_.OUT__V2*/ meltfptr[1];
			    /*_.ADD_TO_OUT__V24*/ meltfptr[8] =
			      meltgc_send ((melt_ptr_t)
					   ( /*_.V__V7*/ meltfptr[6]),
					   (melt_ptr_t) (( /*!ADD_TO_OUT */
							  meltfrout->
							  tabval[1])),
					   (MELTBPARSTR_PTR ""), argtab, "",
					   (union meltparam_un *) 0);
			  }
			  ;
			  /*_.IFELSE___V8*/ meltfptr[7] =
			    /*_.ADD_TO_OUT__V24*/ meltfptr[8];;
			  /*epilog */

			  MELT_LOCATION ("warmelt-base.melt:1317:/ clear");
		   /*clear *//*_.ADD_TO_OUT__V24*/ meltfptr[8] = 0;
			}
			;
		      }
		    ;
		    /*epilog */

		    MELT_LOCATION ("warmelt-base.melt:1313:/ clear");
		 /*clear *//*_.V__V7*/ meltfptr[6] = 0;
		    /*^clear */
		 /*clear *//*_#IS_CLOSURE__L3*/ meltfnum[2] = 0;
		    /*^clear */
		 /*clear *//*_.IFELSE___V8*/ meltfptr[7] = 0;
		  }
		  ;
		}
	      else
		{		/*^cond.else */

		  /*^block */
		  /*anyblock */
		  {

		    /*^cond */
		    /*cond */ if ( /*ifvariadic arg#1 */ variad_ADD2OUT_ix >= 0 && variad_ADD2OUT_ix + 1 <= variad_ADD2OUT_len && meltxargdescr_[variad_ADD2OUT_ix] == MELTBPAR_LONG)	/*then */
		      {
			/*^cond.then */
			/*^block */
			/*anyblock */
			{

			  /*^compute */
	 /*_#N__L5*/ meltfnum[3] =
			    /*variadic argument stuff */
			    meltxargtab_[variad_ADD2OUT_ix + 0].meltbp_long;;
			  /*^compute */

			  /*consume variadic LONG ! */ variad_ADD2OUT_ix +=
			    1;;

			  {
			    MELT_LOCATION ("warmelt-base.melt:1345:/ locexp");
			    meltgc_add_out_dec ((melt_ptr_t)
						( /*_.OUT__V2*/ meltfptr[1]),
						( /*_#N__L5*/ meltfnum[3]));
			  }
			  ;
			  /*epilog */

			  MELT_LOCATION ("warmelt-base.melt:1313:/ clear");
		   /*clear *//*_#N__L5*/ meltfnum[3] = 0;
			}
			;
		      }
		    else
		      {		/*^cond.else */

			/*^block */
			/*anyblock */
			{

			  /*^cond */
			  /*cond */ if ( /*ifvariadic arg#1 */ variad_ADD2OUT_ix >= 0 && variad_ADD2OUT_ix + 1 <= variad_ADD2OUT_len && meltxargdescr_[variad_ADD2OUT_ix] == MELTBPAR_CSTRING)	/*then */
			    {
			      /*^cond.then */
			      /*^block */
			      /*anyblock */
			      {

				/*^compute */
	   /*_?*/ meltfram__.loc_CSTRING__o6 =
				  /*variadic argument stuff */
				  meltxargtab_[variad_ADD2OUT_ix +
					       0].meltbp_cstring;;
				/*^compute */

				/*consume variadic CSTRING ! */
				  variad_ADD2OUT_ix += 1;;

				{
				  MELT_LOCATION
				    ("warmelt-base.melt:1347:/ locexp");
				  meltgc_add_out ((melt_ptr_t)
						  ( /*_.OUT__V2*/
						   meltfptr[1]),
						  ( /*_?*/ meltfram__.
						   loc_CSTRING__o6));
				}
				;
				/*epilog */

				MELT_LOCATION
				  ("warmelt-base.melt:1313:/ clear");
		     /*clear *//*_?*/ meltfram__.loc_CSTRING__o6 =
				  0;
			      }
			      ;
			    }
			  else
			    {	/*^cond.else */

			      /*^block */
			      /*anyblock */
			      {

				MELT_LOCATION
				  ("warmelt-base.melt:1349:/ quasiblock");


	   /*_.VCTY__V25*/ meltfptr[19] =
				  /*variadic_type_code */
#ifdef melt_variadic_index
				  (((melt_variadic_index + 0) >= 0
				    && (melt_variadic_index + 0) <
				    melt_variadic_length) ?
				   melt_code_to_ctype (meltxargdescr_
						       [melt_variadic_index +
							0] &
						       MELT_ARGDESCR_MAX) :
				   NULL)
#else
				  NULL	/* no variadic_ctype outside of variadic functions */
#endif /*melt_variadic_index */
				  ;;
				MELT_LOCATION
				  ("warmelt-base.melt:1352:/ cond");
				/*cond */ if (
					       /*ifisa */
					       melt_is_instance_of ((melt_ptr_t) ( /*_.VCTY__V25*/ meltfptr[19]),
								    (melt_ptr_t) (( /*!CLASS_NAMED */ meltfrout->tabval[0])))
				  )	/*then */
				  {
				    /*^cond.then */
				    /*^getslot */
				    {
				      melt_ptr_t slot = NULL, obj = NULL;
				      obj =
					(melt_ptr_t) ( /*_.VCTY__V25*/
						      meltfptr[19]) /*=obj*/ ;
				      melt_object_get_field (slot, obj, 1,
							     "NAMED_NAME");
	     /*_.NAMED_NAME__V26*/ meltfptr[20] =
					slot;
				    };
				    ;
				  }
				else
				  {	/*^cond.else */

	    /*_.NAMED_NAME__V26*/ meltfptr[20] =
				      NULL;;
				  }
				;

				{
				  MELT_LOCATION
				    ("warmelt-base.melt:1351:/ locexp");
				  error ("MELT ERROR MSG [#%ld]::: %s - %s",
					 melt_dbgcounter,
					 ("ADD2OUT for unsupported ctype; use a manipulator like OUTPUT_TREE "),
					 melt_string_str ((melt_ptr_t)
							  ( /*_.NAMED_NAME__V26*/ meltfptr[20])));
				}
				;

				MELT_LOCATION
				  ("warmelt-base.melt:1349:/ clear");
		     /*clear *//*_.VCTY__V25*/ meltfptr[19] = 0;
				/*^clear */
		     /*clear *//*_.NAMED_NAME__V26*/ meltfptr[20] =
				  0;

#if MELT_HAVE_DEBUG
				MELT_LOCATION
				  ("warmelt-base.melt:1354:/ cppif.then");
				/*^block */
				/*anyblock */
				{

				  /*^checksignal */
				  MELT_CHECK_SIGNAL ();
				  ;
				  /*^cond */
				  /*cond */ if (( /*nil */ NULL))	/*then */
				    {
				      /*^cond.then */
				      /*_.IFELSE___V28*/ meltfptr[9] =
					( /*nil */ NULL);;
				    }
				  else
				    {
				      MELT_LOCATION
					("warmelt-base.melt:1354:/ cond.else");

				      /*^block */
				      /*anyblock */
				      {




					{
					  /*^locexp */
					  melt_assert_failed (("invalid variadic argument to ADD2OUT"), ("warmelt-base.melt") ? ("warmelt-base.melt") : __FILE__, (1354) ? (1354) : __LINE__, __FUNCTION__);
					  ;
					}
					;
			 /*clear *//*_.IFELSE___V28*/
					  meltfptr[9] = 0;
					/*epilog */
				      }
				      ;
				    }
				  ;
				  /*^compute */
				  /*_.IFCPP___V27*/ meltfptr[21] =
				    /*_.IFELSE___V28*/ meltfptr[9];;
				  /*epilog */

				  MELT_LOCATION
				    ("warmelt-base.melt:1354:/ clear");
		       /*clear *//*_.IFELSE___V28*/ meltfptr[9] =
				    0;
				}

#else /*MELT_HAVE_DEBUG */
				/*^cppif.else */
				/*_.IFCPP___V27*/ meltfptr[21] =
				  ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
				;
				MELT_LOCATION
				  ("warmelt-base.melt:1313:/ checksignal");
				MELT_CHECK_SIGNAL ();
				;
				/*epilog */

				/*^clear */
		     /*clear *//*_.IFCPP___V27*/ meltfptr[21] = 0;
			      }
			      ;
			    }
			  ;
			  /*epilog */
			}
			;
		      }
		    ;
		    /*epilog */
		  }
		  ;
		}
	      ;
	      /*epilog */
	    }
	    ;
	  }
	;
	/*epilog */
      }
      ;
      ;
      goto labloop_ARGLOOP_1;
    labexit_ARGLOOP_1:;
      MELT_LOCATION ("warmelt-base.melt:1311:/ loopepilog");
      /*loopepilog */
      /*_.FOREVER___V5*/ meltfptr[3] = /*_.ARGLOOP__V6*/ meltfptr[5];;
    }
    ;
    MELT_LOCATION ("warmelt-base.melt:1356:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.OUT__V2*/ meltfptr[1];;

    {
      MELT_LOCATION ("warmelt-base.melt:1356:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    MELT_LOCATION ("warmelt-base.melt:1305:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.RETURN___V29*/ meltfptr[8];;

    {
      MELT_LOCATION ("warmelt-base.melt:1305:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_#IS_OUT__L1*/ meltfnum[0] = 0;
    /*^clear */
	   /*clear *//*_#NOT__L2*/ meltfnum[1] = 0;
    /*^clear */
	   /*clear *//*_.IF___V3*/ meltfptr[2] = 0;
    /*^clear */
	   /*clear *//*_.FOREVER___V5*/ meltfptr[3] = 0;
    /*^clear */
	   /*clear *//*_.RETURN___V29*/ meltfptr[8] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("ADD2OUT", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_7_warmelt_base_ADD2OUT_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef melt_variadic_length
#undef melt_variadic_index

#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_7_warmelt_base_ADD2OUT */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_8_warmelt_base_STRING4OUT (meltclosure_ptr_t meltclosp_,
				    melt_ptr_t meltfirstargp_,
				    const melt_argdescr_cell_t
				    meltxargdescr_[],
				    union meltparam_un * meltxargtab_,
				    const melt_argdescr_cell_t
				    meltxresdescr_[],
				    union meltparam_un * meltxrestab_)
{
  /*variadic */ int variad_STRING4OUT_ix = 0, variad_STRING4OUT_len =
    melt_argdescr_length (meltxargdescr_);
#define melt_variadic_length  (0+variad_STRING4OUT_len)
#define melt_variadic_index variad_STRING4OUT_ix

  long current_blocklevel_signals_meltrout_8_warmelt_base_STRING4OUT_melt =
    melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_8_warmelt_base_STRING4OUT_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 41
    melt_ptr_t mcfr_varptr[41];
#define MELTFRAM_NBVARNUM 5
    long mcfr_varnum[5];
/*others*/
    const char *loc_CSTRING__o0;
    tree loc_TREE__o1;
    gimple loc_GIMPLE__o2;
    gimple_seq loc_GIMPLE_SEQ__o3;
    edge loc_EDGE__o4;
    loop_p loc_LOOP__o5;
    const char *loc_CSTRING__o6;
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_8_warmelt_base_STRING4OUT is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_8_warmelt_base_STRING4OUT_st *) meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 41; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      if (meltframptr_->loc_TREE__o1)
	gt_ggc_mx_tree_node (meltframptr_->loc_TREE__o1);
      if (meltframptr_->loc_GIMPLE__o2)
	gt_ggc_mx_gimple_statement_d (meltframptr_->loc_GIMPLE__o2);
      if (meltframptr_->loc_GIMPLE_SEQ__o3)
	gt_ggc_mx_gimple_seq_d (meltframptr_->loc_GIMPLE_SEQ__o3);
      if (meltframptr_->loc_EDGE__o4)
	gt_ggc_mx_edge_def (meltframptr_->loc_EDGE__o4);
      if (meltframptr_->loc_LOOP__o5)
	gt_ggc_mx_loop (meltframptr_->loc_LOOP__o5);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_8_warmelt_base_STRING4OUT nbval 41*/
  meltfram__.mcfr_nbvar = 41 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("STRING4OUT", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-base.melt:1360:/ getarg");
 /*_.DIS__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {

    MELT_LOCATION ("warmelt-base.melt:1363:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^cond */
    /*cond */ if ( /*_.DIS__V2*/ meltfptr[1])	/*then */
      {
	/*^cond.then */
	/*_.IFELSE___V3*/ meltfptr[2] = ( /*nil */ NULL);;
      }
    else
      {
	MELT_LOCATION ("warmelt-base.melt:1363:/ cond.else");

	/*^block */
	/*anyblock */
	{

	  /*^compute */
	  /*_.DIS__V2*/ meltfptr[1] = /*_.SETQ___V4*/ meltfptr[3] =
	    ( /*!DISCR_STRING */ meltfrout->tabval[0]);;
	  /*^quasiblock */


	  /*_.PROGN___V5*/ meltfptr[4] = /*_.SETQ___V4*/ meltfptr[3];;
	  /*^compute */
	  /*_.IFELSE___V3*/ meltfptr[2] = /*_.PROGN___V5*/ meltfptr[4];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-base.melt:1363:/ clear");
	     /*clear *//*_.SETQ___V4*/ meltfptr[3] = 0;
	  /*^clear */
	     /*clear *//*_.PROGN___V5*/ meltfptr[4] = 0;
	}
	;
      }
    ;
    MELT_LOCATION ("warmelt-base.melt:1364:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#IS_A__L1*/ meltfnum[0] =
      melt_is_instance_of ((melt_ptr_t) ( /*_.DIS__V2*/ meltfptr[1]),
			   (melt_ptr_t) (( /*!CLASS_DISCRIMINANT */
					  meltfrout->tabval[1])));;
    MELT_LOCATION ("warmelt-base.melt:1364:/ cond");
    /*cond */ if ( /*_#IS_A__L1*/ meltfnum[0])	/*then */
      {
	/*^cond.then */
	/*_.IFELSE___V6*/ meltfptr[3] = ( /*nil */ NULL);;
      }
    else
      {
	MELT_LOCATION ("warmelt-base.melt:1364:/ cond.else");

	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-base.melt:1365:/ quasiblock");


   /*_.RETVAL___V1*/ meltfptr[0] = NULL;;

	  {
	    MELT_LOCATION ("warmelt-base.melt:1365:/ locexp");
	    /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
	    if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	      melt_warn_for_no_expected_secondary_results ();
	    /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
	    ;
	  }
	  ;
	  /*^finalreturn */
	  ;
	  /*finalret */ goto labend_rout;
	  MELT_LOCATION ("warmelt-base.melt:1364:/ quasiblock");


	  /*_.PROGN___V8*/ meltfptr[7] = /*_.RETURN___V7*/ meltfptr[4];;
	  /*^compute */
	  /*_.IFELSE___V6*/ meltfptr[3] = /*_.PROGN___V8*/ meltfptr[7];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-base.melt:1364:/ clear");
	     /*clear *//*_.RETURN___V7*/ meltfptr[4] = 0;
	  /*^clear */
	     /*clear *//*_.PROGN___V8*/ meltfptr[7] = 0;
	}
	;
      }
    ;
    MELT_LOCATION ("warmelt-base.melt:1366:/ quasiblock");


 /*_#GOODMAG__L2*/ meltfnum[1] = 0;;

    {
      MELT_LOCATION ("warmelt-base.melt:1368:/ locexp");
				   /* string4out GETMAGIDISCHK__1 *//*_#GOODMAG__L2*/ meltfnum[1] =
	((meltobject_ptr_t) /*_.DIS__V2*/ meltfptr[1])->meltobj_magic ==
	MELTOBMAG_STRING;;
    }
    ;
    MELT_LOCATION ("warmelt-base.melt:1371:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^cond */
    /*cond */ if ( /*_#GOODMAG__L2*/ meltfnum[1])	/*then */
      {
	/*^cond.then */
	/*_.IFELSE___V10*/ meltfptr[7] = ( /*nil */ NULL);;
      }
    else
      {
	MELT_LOCATION ("warmelt-base.melt:1371:/ cond.else");

	/*^block */
	/*anyblock */
	{

	  /*^quasiblock */


   /*_.RETVAL___V1*/ meltfptr[0] = NULL;;

	  {
	    MELT_LOCATION ("warmelt-base.melt:1371:/ locexp");
	    /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
	    if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	      melt_warn_for_no_expected_secondary_results ();
	    /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
	    ;
	  }
	  ;
	  /*^finalreturn */
	  ;
	  /*finalret */ goto labend_rout;
	  /*^quasiblock */


	  /*_.PROGN___V12*/ meltfptr[11] = /*_.RETURN___V11*/ meltfptr[10];;
	  /*^compute */
	  /*_.IFELSE___V10*/ meltfptr[7] = /*_.PROGN___V12*/ meltfptr[11];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-base.melt:1371:/ clear");
	     /*clear *//*_.RETURN___V11*/ meltfptr[10] = 0;
	  /*^clear */
	     /*clear *//*_.PROGN___V12*/ meltfptr[11] = 0;
	}
	;
      }
    ;
    /*_.LET___V9*/ meltfptr[4] = /*_.IFELSE___V10*/ meltfptr[7];;

    MELT_LOCATION ("warmelt-base.melt:1366:/ clear");
	   /*clear *//*_#GOODMAG__L2*/ meltfnum[1] = 0;
    /*^clear */
	   /*clear *//*_.IFELSE___V10*/ meltfptr[7] = 0;
    MELT_LOCATION ("warmelt-base.melt:1372:/ quasiblock");


 /*_.OUT__V14*/ meltfptr[11] =
      (melt_ptr_t)
      meltgc_new_strbuf ((meltobject_ptr_t)
			 (( /*!DISCR_STRBUF */ meltfrout->tabval[2])),
			 (const char *) 0);;
    MELT_LOCATION ("warmelt-base.melt:1374:/ loop");
    /*loop */
    {
    labloop_ARGLOOP_2:;	/*^loopbody */

      /*^block */
      /*anyblock */
      {

	/*^checksignal */
	MELT_CHECK_SIGNAL ();
	;
	MELT_LOCATION ("warmelt-base.melt:1376:/ cond");
	/*cond */ if ( /*ifvariadic nomore */ variad_STRING4OUT_ix == variad_STRING4OUT_len)	/*then */
	  {
	    /*^cond.then */
	    /*^block */
	    /*anyblock */
	    {

	      /*^compute */

	      /*consume variadic  ! */ variad_STRING4OUT_ix += 0;;
	      MELT_LOCATION ("warmelt-base.melt:1378:/ quasiblock");


	      /*^compute */
     /*_.ARGLOOP__V16*/ meltfptr[15] = NULL;;

	      /*^exit */
	      /*exit */
	      {
		goto labexit_ARGLOOP_2;
	      }
	      ;
	      /*epilog */
	    }
	    ;
	  }
	else
	  {
	    MELT_LOCATION ("warmelt-base.melt:1376:/ cond.else");

	    /*^block */
	    /*anyblock */
	    {

	      /*^cond */
	      /*cond */ if ( /*ifvariadic arg#1 */ variad_STRING4OUT_ix >= 0 && variad_STRING4OUT_ix + 1 <= variad_STRING4OUT_len && meltxargdescr_[variad_STRING4OUT_ix] == MELTBPAR_PTR)	/*then */
		{
		  /*^cond.then */
		  /*^block */
		  /*anyblock */
		  {

		    /*^compute */
       /*_.V__V17*/ meltfptr[16] =
		      /*variadic argument value */
		      ((meltxargtab_[variad_STRING4OUT_ix + 0].
			meltbp_aptr)
		       ? (*
			  (meltxargtab_[variad_STRING4OUT_ix + 0].
			   meltbp_aptr)) : NULL);;
		    /*^compute */

		    /*consume variadic Value ! */ variad_STRING4OUT_ix += 1;;
		    MELT_LOCATION ("warmelt-base.melt:1380:/ checksignal");
		    MELT_CHECK_SIGNAL ();
		    ;
       /*_#IS_CLOSURE__L3*/ meltfnum[1] =
		      (melt_magic_discr
		       ((melt_ptr_t) ( /*_.V__V17*/ meltfptr[16])) ==
		       MELTOBMAG_CLOSURE);;
		    MELT_LOCATION ("warmelt-base.melt:1380:/ cond");
		    /*cond */ if ( /*_#IS_CLOSURE__L3*/ meltfnum[1])	/*then */
		      {
			/*^cond.then */
			/*^block */
			/*anyblock */
			{

			  MELT_LOCATION ("warmelt-base.melt:1381:/ cond");
			  /*cond */ if ( /*ifvariadic arg#1 */ variad_STRING4OUT_ix >= 0 && variad_STRING4OUT_ix + 1 <= variad_STRING4OUT_len && meltxargdescr_[variad_STRING4OUT_ix] == MELTBPAR_PTR)	/*then */
			    {
			      /*^cond.then */
			      /*^block */
			      /*anyblock */
			      {

				/*^compute */
	   /*_.VV__V19*/ meltfptr[18] =
				  /*variadic argument value */
				  ((meltxargtab_[variad_STRING4OUT_ix + 0].
				    meltbp_aptr)
				   ? (*
				      (meltxargtab_[variad_STRING4OUT_ix + 0].
				       meltbp_aptr)) : NULL);;
				/*^compute */

				/*consume variadic Value ! */
				  variad_STRING4OUT_ix += 1;;
				MELT_LOCATION
				  ("warmelt-base.melt:1383:/ checksignal");
				MELT_CHECK_SIGNAL ();
				;
				/*^apply */
				/*apply */
				{
				  union meltparam_un argtab[1];
				  memset (&argtab, 0, sizeof (argtab));
				  /*^apply.arg */
				  argtab[0].meltbp_aptr =
				    (melt_ptr_t *) & /*_.VV__V19*/
				    meltfptr[18];
				  /*_.V__V20*/ meltfptr[19] =
				    melt_apply ((meltclosure_ptr_t)
						( /*_.V__V17*/ meltfptr[16]),
						(melt_ptr_t) ( /*_.OUT__V14*/
							      meltfptr[11]),
						(MELTBPARSTR_PTR ""), argtab,
						"", (union meltparam_un *) 0);
				}
				;
				/*_.IFELSE___V18*/ meltfptr[17] =
				  /*_.V__V20*/ meltfptr[19];;
				/*epilog */

				MELT_LOCATION
				  ("warmelt-base.melt:1381:/ clear");
		     /*clear *//*_.VV__V19*/ meltfptr[18] = 0;
				/*^clear */
		     /*clear *//*_.V__V20*/ meltfptr[19] = 0;
			      }
			      ;
			    }
			  else
			    {	/*^cond.else */

			      /*^block */
			      /*anyblock */
			      {

				/*^cond */
				/*cond */ if ( /*ifvariadic arg#1 */ variad_STRING4OUT_ix >= 0 && variad_STRING4OUT_ix + 1 <= variad_STRING4OUT_len && meltxargdescr_[variad_STRING4OUT_ix] == MELTBPAR_LONG)	/*then */
				  {
				    /*^cond.then */
				    /*^block */
				    /*anyblock */
				    {

				      /*^compute */
	     /*_#LL__L4*/ meltfnum[3] =
					/*variadic argument stuff */
					meltxargtab_[variad_STRING4OUT_ix +
						     0].meltbp_long;;
				      /*^compute */

				      /*consume variadic LONG ! */
					variad_STRING4OUT_ix += 1;;
				      MELT_LOCATION
					("warmelt-base.melt:1385:/ checksignal");
				      MELT_CHECK_SIGNAL ();
				      ;
				      /*^apply */
				      /*apply */
				      {
					union meltparam_un argtab[1];
					memset (&argtab, 0, sizeof (argtab));
					/*^apply.arg */
					argtab[0].meltbp_long =
					  /*_#LL__L4*/ meltfnum[3];
					/*_.V__V21*/ meltfptr[18] =
					  melt_apply ((meltclosure_ptr_t)
						      ( /*_.V__V17*/
						       meltfptr[16]),
						      (melt_ptr_t) ( /*_.OUT__V14*/ meltfptr[11]), (MELTBPARSTR_LONG ""), argtab, "", (union meltparam_un *) 0);
				      }
				      ;
				      /*_.IFELSE___V18*/ meltfptr[17] =
					/*_.V__V21*/ meltfptr[18];;
				      /*epilog */

				      MELT_LOCATION
					("warmelt-base.melt:1381:/ clear");
		       /*clear *//*_#LL__L4*/ meltfnum[3] = 0;
				      /*^clear */
		       /*clear *//*_.V__V21*/ meltfptr[18] = 0;
				    }
				    ;
				  }
				else
				  {	/*^cond.else */

				    /*^block */
				    /*anyblock */
				    {

				      /*^cond */
				      /*cond */ if ( /*ifvariadic arg#1 */ variad_STRING4OUT_ix >= 0 && variad_STRING4OUT_ix + 1 <= variad_STRING4OUT_len && meltxargdescr_[variad_STRING4OUT_ix] == MELTBPAR_CSTRING)	/*then */
					{
					  /*^cond.then */
					  /*^block */
					  /*anyblock */
					  {

					    /*^compute */
	       /*_?*/ meltfram__.
					      loc_CSTRING__o0 =
					      /*variadic argument stuff */
					      meltxargtab_
					      [variad_STRING4OUT_ix +
					       0].meltbp_cstring;;
					    /*^compute */

					    /*consume variadic CSTRING ! */
					      variad_STRING4OUT_ix += 1;;
					    MELT_LOCATION
					      ("warmelt-base.melt:1387:/ checksignal");
					    MELT_CHECK_SIGNAL ();
					    ;
					    /*^apply */
					    /*apply */
					    {
					      union meltparam_un argtab[1];
					      memset (&argtab, 0,
						      sizeof (argtab));
					      /*^apply.arg */
					      argtab[0].meltbp_cstring =
						/*_?*/
						meltfram__.loc_CSTRING__o0;
					      /*_.V__V22*/ meltfptr[19] =
						melt_apply ((meltclosure_ptr_t) ( /*_.V__V17*/ meltfptr[16]), (melt_ptr_t) ( /*_.OUT__V14*/ meltfptr[11]), (MELTBPARSTR_CSTRING ""), argtab, "", (union meltparam_un *) 0);
					    }
					    ;
					    /*_.IFELSE___V18*/ meltfptr[17] =
					      /*_.V__V22*/ meltfptr[19];;
					    /*epilog */

					    MELT_LOCATION
					      ("warmelt-base.melt:1381:/ clear");
			 /*clear *//*_?*/ meltfram__.
					      loc_CSTRING__o0 = 0;
					    /*^clear */
			 /*clear *//*_.V__V22*/
					      meltfptr[19] = 0;
					  }
					  ;
					}
				      else
					{	/*^cond.else */

					  /*^block */
					  /*anyblock */
					  {

					    /*^cond */
					    /*cond */ if ( /*ifvariadic arg#1 */ variad_STRING4OUT_ix >= 0 && variad_STRING4OUT_ix + 1 <= variad_STRING4OUT_len && meltxargdescr_[variad_STRING4OUT_ix] == MELTBPAR_TREE)	/*then */
					      {
						/*^cond.then */
						/*^block */
						/*anyblock */
						{

						  /*^compute */
		 /*_?*/ meltfram__.
						    loc_TREE__o1 =
						    /*variadic argument stuff */
						    meltxargtab_
						    [variad_STRING4OUT_ix +
						     0].meltbp_tree;;
						  /*^compute */

						  /*consume variadic TREE ! */
						    variad_STRING4OUT_ix +=
						    1;;
						  MELT_LOCATION
						    ("warmelt-base.melt:1389:/ checksignal");
						  MELT_CHECK_SIGNAL ();
						  ;
						  /*^apply */
						  /*apply */
						  {
						    union meltparam_un
						      argtab[1];
						    memset (&argtab, 0,
							    sizeof (argtab));
						    /*^apply.arg */
						    argtab[0].meltbp_tree =
						      /*_?*/
						      meltfram__.loc_TREE__o1;
						    /*_.V__V23*/ meltfptr[18]
						      =
						      melt_apply ((meltclosure_ptr_t) ( /*_.V__V17*/ meltfptr[16]), (melt_ptr_t) ( /*_.OUT__V14*/ meltfptr[11]), (MELTBPARSTR_TREE ""), argtab, "", (union meltparam_un *) 0);
						  }
						  ;
						  /*_.IFELSE___V18*/
						    meltfptr[17] =
						    /*_.V__V23*/
						    meltfptr[18];;
						  /*epilog */

						  MELT_LOCATION
						    ("warmelt-base.melt:1381:/ clear");
			   /*clear *//*_?*/ meltfram__.
						    loc_TREE__o1 = 0;
						  /*^clear */
			   /*clear *//*_.V__V23*/
						    meltfptr[18] = 0;
						}
						;
					      }
					    else
					      {	/*^cond.else */

						/*^block */
						/*anyblock */
						{

						  /*^cond */
						  /*cond */ if ( /*ifvariadic arg#1 */ variad_STRING4OUT_ix >= 0 && variad_STRING4OUT_ix + 1 <= variad_STRING4OUT_len && meltxargdescr_[variad_STRING4OUT_ix] == MELTBPAR_GIMPLE)	/*then */
						    {
						      /*^cond.then */
						      /*^block */
						      /*anyblock */
						      {

							/*^compute */
		   /*_?*/ meltfram__.
							  loc_GIMPLE__o2 =
							  /*variadic argument stuff */
							  meltxargtab_
							  [variad_STRING4OUT_ix
							   +
							   0].meltbp_gimple;;
							/*^compute */

							/*consume variadic GIMPLE ! */
							  variad_STRING4OUT_ix
							  += 1;;
							MELT_LOCATION
							  ("warmelt-base.melt:1391:/ checksignal");
							MELT_CHECK_SIGNAL ();
							;
							/*^apply */
							/*apply */
							{
							  union meltparam_un
							    argtab[1];
							  memset (&argtab, 0,
								  sizeof
								  (argtab));
							  /*^apply.arg */
							  argtab[0].
							    meltbp_gimple =
							    /*_?*/
							    meltfram__.
							    loc_GIMPLE__o2;
							  /*_.V__V24*/
							    meltfptr[19] =
							    melt_apply ((meltclosure_ptr_t) ( /*_.V__V17*/ meltfptr[16]), (melt_ptr_t) ( /*_.OUT__V14*/ meltfptr[11]), (MELTBPARSTR_GIMPLE ""), argtab, "", (union meltparam_un *) 0);
							}
							;
							/*_.IFELSE___V18*/
							  meltfptr[17] =
							  /*_.V__V24*/
							  meltfptr[19];;
							/*epilog */

							MELT_LOCATION
							  ("warmelt-base.melt:1381:/ clear");
			     /*clear *//*_?*/
							  meltfram__.
							  loc_GIMPLE__o2 = 0;
							/*^clear */
			     /*clear *//*_.V__V24*/
							  meltfptr[19] = 0;
						      }
						      ;
						    }
						  else
						    {	/*^cond.else */

						      /*^block */
						      /*anyblock */
						      {

							/*^cond */
							/*cond */ if ( /*ifvariadic arg#1 */ variad_STRING4OUT_ix >= 0 && variad_STRING4OUT_ix + 1 <= variad_STRING4OUT_len && meltxargdescr_[variad_STRING4OUT_ix] == MELTBPAR_GIMPLESEQ)	/*then */
							  {
							    /*^cond.then */
							    /*^block */
							    /*anyblock */
							    {

							      /*^compute */
		     /*_?*/
								meltfram__.
								loc_GIMPLE_SEQ__o3
								=
								/*variadic argument stuff */
								meltxargtab_
								[variad_STRING4OUT_ix
								 +
								 0].
								meltbp_gimpleseq;;
							      /*^compute */

							      /*consume variadic GIMPLE_SEQ ! */
								variad_STRING4OUT_ix
								+= 1;;
							      MELT_LOCATION
								("warmelt-base.melt:1393:/ checksignal");
							      MELT_CHECK_SIGNAL
								();
							      ;
							      /*^apply */
							      /*apply */
							      {
								union
								  meltparam_un
								  argtab[1];
								memset
								  (&argtab, 0,
								   sizeof
								   (argtab));
								/*^apply.arg */
								argtab[0].
								  meltbp_gimpleseq
								  =
								  /*_?*/
								  meltfram__.
								  loc_GIMPLE_SEQ__o3;
								/*_.V__V25*/
								  meltfptr[18]
								  =
								  melt_apply ((meltclosure_ptr_t) ( /*_.V__V17*/ meltfptr[16]), (melt_ptr_t) ( /*_.OUT__V14*/ meltfptr[11]), (MELTBPARSTR_GIMPLESEQ ""), argtab, "", (union meltparam_un *) 0);
							      }
							      ;
							      /*_.IFELSE___V18*/
								meltfptr[17] =
								/*_.V__V25*/
								meltfptr[18];;
							      /*epilog */

							      MELT_LOCATION
								("warmelt-base.melt:1381:/ clear");
			       /*clear *//*_?*/
								meltfram__.
								loc_GIMPLE_SEQ__o3
								= 0;
							      /*^clear */
			       /*clear *//*_.V__V25*/
								meltfptr
								[18] = 0;
							    }
							    ;
							  }
							else
							  {	/*^cond.else */

							    /*^block */
							    /*anyblock */
							    {

							      /*^cond */
							      /*cond */ if ( /*ifvariadic arg#1 */ variad_STRING4OUT_ix >= 0 && variad_STRING4OUT_ix + 1 <= variad_STRING4OUT_len && meltxargdescr_[variad_STRING4OUT_ix] == MELTBPAR_EDGE)	/*then */
								{
								  /*^cond.then */
								  /*^block */
								  /*anyblock */
								  {

								    /*^compute */
		       /*_?*/
								      meltfram__.
								      loc_EDGE__o4
								      =
								      /*variadic argument stuff */
								      meltxargtab_
								      [variad_STRING4OUT_ix
								       +
								       0].
								      meltbp_edge;;
								    /*^compute */

								    /*consume variadic EDGE ! */
								      variad_STRING4OUT_ix
								      += 1;;
								    MELT_LOCATION
								      ("warmelt-base.melt:1395:/ checksignal");
								    MELT_CHECK_SIGNAL
								      ();
								    ;
								    /*^apply */
								    /*apply */
								    {
								      union
									meltparam_un
									argtab
									[1];
								      memset
									(&argtab,
									 0,
									 sizeof
									 (argtab));
								      /*^apply.arg */
								      argtab
									[0].
									meltbp_edge
									=
									/*_?*/
									meltfram__.
									loc_EDGE__o4;
								      /*_.V__V26*/
									meltfptr
									[19] =
									melt_apply
									((meltclosure_ptr_t) ( /*_.V__V17*/ meltfptr[16]), (melt_ptr_t) ( /*_.OUT__V14*/ meltfptr[11]), (MELTBPARSTR_EDGE ""), argtab, "", (union meltparam_un *) 0);
								    }
								    ;
								    /*_.IFELSE___V18*/
								      meltfptr
								      [17] =
								      /*_.V__V26*/
								      meltfptr
								      [19];;
								    /*epilog */

								    MELT_LOCATION
								      ("warmelt-base.melt:1381:/ clear");
				 /*clear *//*_?*/
								      meltfram__.
								      loc_EDGE__o4
								      = 0;
								    /*^clear */
				 /*clear *//*_.V__V26*/
								      meltfptr
								      [19]
								      = 0;
								  }
								  ;
								}
							      else
								{	/*^cond.else */

								  /*^block */
								  /*anyblock */
								  {

								    /*^cond */
								    /*cond */ if ( /*ifvariadic arg#1 */ variad_STRING4OUT_ix >= 0 && variad_STRING4OUT_ix + 1 <= variad_STRING4OUT_len && meltxargdescr_[variad_STRING4OUT_ix] == MELTBPAR_LOOP)	/*then */
								      {
									/*^cond.then */
									/*^block */
									/*anyblock */
									{

									  /*^compute */
			 /*_?*/
									    meltfram__.
									    loc_LOOP__o5
									    =
									    /*variadic argument stuff */
									    meltxargtab_
									    [variad_STRING4OUT_ix
									     +
									     0].
									    meltbp_loop;;
									  /*^compute */

									  /*consume variadic LOOP ! */
									    variad_STRING4OUT_ix
									    +=
									    1;;
									  MELT_LOCATION
									    ("warmelt-base.melt:1397:/ checksignal");
									  MELT_CHECK_SIGNAL
									    ();
									  ;
									  /*^apply */
									  /*apply */
									  {
									    union
									      meltparam_un
									      argtab
									      [1];
									    memset
									      (&argtab,
									       0,
									       sizeof
									       (argtab));
									    /*^apply.arg */
									    argtab
									      [0].
									      meltbp_loop
									      =
									      /*_?*/
									      meltfram__.
									      loc_LOOP__o5;
									    /*_.V__V27*/
									      meltfptr
									      [18]
									      =
									      melt_apply
									      ((meltclosure_ptr_t) ( /*_.V__V17*/ meltfptr[16]), (melt_ptr_t) ( /*_.OUT__V14*/ meltfptr[11]), (MELTBPARSTR_LOOP ""), argtab, "", (union meltparam_un *) 0);
									  }
									  ;
									  /*_.IFELSE___V18*/
									    meltfptr
									    [17]
									    =
									    /*_.V__V27*/
									    meltfptr
									    [18];;
									  /*epilog */

									  MELT_LOCATION
									    ("warmelt-base.melt:1381:/ clear");
				   /*clear *//*_?*/
									    meltfram__.
									    loc_LOOP__o5
									    =
									    0;
									  /*^clear */
				   /*clear *//*_.V__V27*/
									    meltfptr
									    [18]
									    =
									    0;
									}
									;
								      }
								    else
								      {	/*^cond.else */

									/*^block */
									/*anyblock */
									{

									  MELT_LOCATION
									    ("warmelt-base.melt:1399:/ quasiblock");


			 /*_.VCTY__V29*/
									    meltfptr
									    [18]
									    =
									    /*variadic_type_code */
#ifdef melt_variadic_index
									    (((melt_variadic_index + 0) >= 0 && (melt_variadic_index + 0) < melt_variadic_length) ? melt_code_to_ctype (meltxargdescr_[melt_variadic_index + 0] & MELT_ARGDESCR_MAX) : NULL)
#else
									    NULL	/* no variadic_ctype outside of variadic functions */
#endif /*melt_variadic_index */
									    ;;
									  MELT_LOCATION
									    ("warmelt-base.melt:1402:/ cond");
									  /*cond */ if (
											 /*ifisa */
											 melt_is_instance_of
											 ((melt_ptr_t) ( /*_.VCTY__V29*/ meltfptr[18]),
											  (melt_ptr_t) (( /*!CLASS_NAMED */ meltfrout->tabval[3])))
									    )	/*then */
									    {
									      /*^cond.then */
									      /*^getslot */
									      {
										melt_ptr_t
										  slot
										  =
										  NULL,
										  obj
										  =
										  NULL;
										obj
										  =
										  (melt_ptr_t)
										  ( /*_.VCTY__V29*/ meltfptr[18]) /*=obj*/ ;
										melt_object_get_field
										  (slot,
										   obj,
										   1,
										   "NAMED_NAME");
			   /*_.NAMED_NAME__V30*/
										  meltfptr
										  [29]
										  =
										  slot;
									      };
									      ;
									    }
									  else
									    {	/*^cond.else */

			  /*_.NAMED_NAME__V30*/
										meltfptr
										[29]
										=
										NULL;;
									    }
									  ;

									  {
									    MELT_LOCATION
									      ("warmelt-base.melt:1401:/ locexp");
									    error
									      ("MELT ERROR MSG [#%ld]::: %s - %s",
									       melt_dbgcounter,
									       ("STRING4OUT with manipulator for unsupported ctype"),
									       melt_string_str
									       ((melt_ptr_t) ( /*_.NAMED_NAME__V30*/ meltfptr[29])));
									  }
									  ;

#if MELT_HAVE_DEBUG
									  MELT_LOCATION
									    ("warmelt-base.melt:1403:/ cppif.then");
									  /*^block */
									  /*anyblock */
									  {

									    /*^checksignal */
									    MELT_CHECK_SIGNAL
									      ();
									    ;
									    /*^cond */
									    /*cond */ if (( /*nil */ NULL))	/*then */
									      {
										/*^cond.then */
										/*_.IFELSE___V32*/
										  meltfptr
										  [31]
										  =
										  ( /*nil */ NULL);;
									      }
									    else
									      {
										MELT_LOCATION
										  ("warmelt-base.melt:1403:/ cond.else");

										/*^block */
										/*anyblock */
										{




										  {
										    /*^locexp */
										    melt_assert_failed
										      (("invalid variadic argument after closure to STRING4OUT"), ("warmelt-base.melt") ? ("warmelt-base.melt") : __FILE__, (1403) ? (1403) : __LINE__, __FUNCTION__);
										    ;
										  }
										  ;
				       /*clear *//*_.IFELSE___V32*/
										    meltfptr
										    [31]
										    =
										    0;
										  /*epilog */
										}
										;
									      }
									    ;
									    /*^compute */
									    /*_.IFCPP___V31*/
									      meltfptr
									      [30]
									      =
									      /*_.IFELSE___V32*/
									      meltfptr
									      [31];;
									    /*epilog */

									    MELT_LOCATION
									      ("warmelt-base.melt:1403:/ clear");
				     /*clear *//*_.IFELSE___V32*/
									      meltfptr
									      [31]
									      =
									      0;
									  }

#else /*MELT_HAVE_DEBUG */
									  /*^cppif.else */
									  /*_.IFCPP___V31*/
									    meltfptr
									    [30]
									    =
									    ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
									  ;
									  MELT_LOCATION
									    ("warmelt-base.melt:1404:/ quasiblock");


			 /*_.RETVAL___V1*/
									    meltfptr
									    [0]
									    =
									    NULL;;

									  {
									    MELT_LOCATION
									      ("warmelt-base.melt:1404:/ locexp");
									    /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
									    if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
									      melt_warn_for_no_expected_secondary_results
										();
									    /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
									    ;
									  }
									  ;
									  /*^finalreturn */
									  ;
									  /*finalret */
									    goto
									    labend_rout;
									  /*_.LET___V28*/
									    meltfptr
									    [19]
									    =
									    /*_.RETURN___V33*/
									    meltfptr
									    [31];;

									  MELT_LOCATION
									    ("warmelt-base.melt:1399:/ clear");
				   /*clear *//*_.VCTY__V29*/
									    meltfptr
									    [18]
									    =
									    0;
									  /*^clear */
				   /*clear *//*_.NAMED_NAME__V30*/
									    meltfptr
									    [29]
									    =
									    0;
									  /*^clear */
				   /*clear *//*_.IFCPP___V31*/
									    meltfptr
									    [30]
									    =
									    0;
									  /*^clear */
				   /*clear *//*_.RETURN___V33*/
									    meltfptr
									    [31]
									    =
									    0;
									  /*_.IFELSE___V18*/
									    meltfptr
									    [17]
									    =
									    /*_.LET___V28*/
									    meltfptr
									    [19];;
									  /*epilog */

									  MELT_LOCATION
									    ("warmelt-base.melt:1381:/ clear");
				   /*clear *//*_.LET___V28*/
									    meltfptr
									    [19]
									    =
									    0;
									}
									;
								      }
								    ;
								    /*epilog */
								  }
								  ;
								}
							      ;
							      /*epilog */
							    }
							    ;
							  }
							;
							/*epilog */
						      }
						      ;
						    }
						  ;
						  /*epilog */
						}
						;
					      }
					    ;
					    /*epilog */
					  }
					  ;
					}
				      ;
				      /*epilog */
				    }
				    ;
				  }
				;
				/*epilog */
			      }
			      ;
			    }
			  ;
			  /*epilog */
			}
			;
		      }
		    else
		      {
			MELT_LOCATION ("warmelt-base.melt:1380:/ cond.else");

			/*^block */
			/*anyblock */
			{

			  MELT_LOCATION
			    ("warmelt-base.melt:1406:/ checksignal");
			  MELT_CHECK_SIGNAL ();
			  ;
			  /*^msend */
			  /*msend */
			  {
			    union meltparam_un argtab[1];
			    memset (&argtab, 0, sizeof (argtab));
			    /*^ojbmsend.arg */
			    argtab[0].meltbp_aptr =
			      (melt_ptr_t *) & /*_.OUT__V14*/ meltfptr[11];
			    /*_.ADD_TO_OUT__V34*/ meltfptr[18] =
			      meltgc_send ((melt_ptr_t)
					   ( /*_.V__V17*/ meltfptr[16]),
					   (melt_ptr_t) (( /*!ADD_TO_OUT */
							  meltfrout->
							  tabval[4])),
					   (MELTBPARSTR_PTR ""), argtab, "",
					   (union meltparam_un *) 0);
			  }
			  ;
			  /*_.IFELSE___V18*/ meltfptr[17] =
			    /*_.ADD_TO_OUT__V34*/ meltfptr[18];;
			  /*epilog */

			  MELT_LOCATION ("warmelt-base.melt:1380:/ clear");
		   /*clear *//*_.ADD_TO_OUT__V34*/ meltfptr[18] = 0;
			}
			;
		      }
		    ;
		    /*epilog */

		    MELT_LOCATION ("warmelt-base.melt:1376:/ clear");
		 /*clear *//*_.V__V17*/ meltfptr[16] = 0;
		    /*^clear */
		 /*clear *//*_#IS_CLOSURE__L3*/ meltfnum[1] = 0;
		    /*^clear */
		 /*clear *//*_.IFELSE___V18*/ meltfptr[17] = 0;
		  }
		  ;
		}
	      else
		{		/*^cond.else */

		  /*^block */
		  /*anyblock */
		  {

		    /*^cond */
		    /*cond */ if ( /*ifvariadic arg#1 */ variad_STRING4OUT_ix >= 0 && variad_STRING4OUT_ix + 1 <= variad_STRING4OUT_len && meltxargdescr_[variad_STRING4OUT_ix] == MELTBPAR_LONG)	/*then */
		      {
			/*^cond.then */
			/*^block */
			/*anyblock */
			{

			  /*^compute */
	 /*_#N__L5*/ meltfnum[3] =
			    /*variadic argument stuff */
			    meltxargtab_[variad_STRING4OUT_ix +
					 0].meltbp_long;;
			  /*^compute */

			  /*consume variadic LONG ! */ variad_STRING4OUT_ix +=
			    1;;

			  {
			    MELT_LOCATION ("warmelt-base.melt:1408:/ locexp");
			    meltgc_add_out_dec ((melt_ptr_t)
						( /*_.OUT__V14*/
						 meltfptr[11]),
						( /*_#N__L5*/ meltfnum[3]));
			  }
			  ;
			  /*epilog */

			  MELT_LOCATION ("warmelt-base.melt:1376:/ clear");
		   /*clear *//*_#N__L5*/ meltfnum[3] = 0;
			}
			;
		      }
		    else
		      {		/*^cond.else */

			/*^block */
			/*anyblock */
			{

			  /*^cond */
			  /*cond */ if ( /*ifvariadic arg#1 */ variad_STRING4OUT_ix >= 0 && variad_STRING4OUT_ix + 1 <= variad_STRING4OUT_len && meltxargdescr_[variad_STRING4OUT_ix] == MELTBPAR_CSTRING)	/*then */
			    {
			      /*^cond.then */
			      /*^block */
			      /*anyblock */
			      {

				/*^compute */
	   /*_?*/ meltfram__.loc_CSTRING__o6 =
				  /*variadic argument stuff */
				  meltxargtab_[variad_STRING4OUT_ix +
					       0].meltbp_cstring;;
				/*^compute */

				/*consume variadic CSTRING ! */
				  variad_STRING4OUT_ix += 1;;

				{
				  MELT_LOCATION
				    ("warmelt-base.melt:1410:/ locexp");
				  meltgc_add_out ((melt_ptr_t)
						  ( /*_.OUT__V14*/
						   meltfptr[11]),
						  ( /*_?*/ meltfram__.
						   loc_CSTRING__o6));
				}
				;
				/*epilog */

				MELT_LOCATION
				  ("warmelt-base.melt:1376:/ clear");
		     /*clear *//*_?*/ meltfram__.loc_CSTRING__o6 =
				  0;
			      }
			      ;
			    }
			  else
			    {	/*^cond.else */

			      /*^block */
			      /*anyblock */
			      {

				MELT_LOCATION
				  ("warmelt-base.melt:1412:/ quasiblock");


	   /*_.VCTY__V35*/ meltfptr[29] =
				  /*variadic_type_code */
#ifdef melt_variadic_index
				  (((melt_variadic_index + 0) >= 0
				    && (melt_variadic_index + 0) <
				    melt_variadic_length) ?
				   melt_code_to_ctype (meltxargdescr_
						       [melt_variadic_index +
							0] &
						       MELT_ARGDESCR_MAX) :
				   NULL)
#else
				  NULL	/* no variadic_ctype outside of variadic functions */
#endif /*melt_variadic_index */
				  ;;
				MELT_LOCATION
				  ("warmelt-base.melt:1415:/ cond");
				/*cond */ if (
					       /*ifisa */
					       melt_is_instance_of ((melt_ptr_t) ( /*_.VCTY__V35*/ meltfptr[29]),
								    (melt_ptr_t) (( /*!CLASS_NAMED */ meltfrout->tabval[3])))
				  )	/*then */
				  {
				    /*^cond.then */
				    /*^getslot */
				    {
				      melt_ptr_t slot = NULL, obj = NULL;
				      obj =
					(melt_ptr_t) ( /*_.VCTY__V35*/
						      meltfptr[29]) /*=obj*/ ;
				      melt_object_get_field (slot, obj, 1,
							     "NAMED_NAME");
	     /*_.NAMED_NAME__V36*/ meltfptr[30] =
					slot;
				    };
				    ;
				  }
				else
				  {	/*^cond.else */

	    /*_.NAMED_NAME__V36*/ meltfptr[30] =
				      NULL;;
				  }
				;

				{
				  MELT_LOCATION
				    ("warmelt-base.melt:1414:/ locexp");
				  error ("MELT ERROR MSG [#%ld]::: %s - %s",
					 melt_dbgcounter,
					 ("STRING4OUT for unsupported ctype; use a manipulator like OUTPUT_TREE "),
					 melt_string_str ((melt_ptr_t)
							  ( /*_.NAMED_NAME__V36*/ meltfptr[30])));
				}
				;

				MELT_LOCATION
				  ("warmelt-base.melt:1412:/ clear");
		     /*clear *//*_.VCTY__V35*/ meltfptr[29] = 0;
				/*^clear */
		     /*clear *//*_.NAMED_NAME__V36*/ meltfptr[30] =
				  0;

#if MELT_HAVE_DEBUG
				MELT_LOCATION
				  ("warmelt-base.melt:1417:/ cppif.then");
				/*^block */
				/*anyblock */
				{

				  /*^checksignal */
				  MELT_CHECK_SIGNAL ();
				  ;
				  /*^cond */
				  /*cond */ if (( /*nil */ NULL))	/*then */
				    {
				      /*^cond.then */
				      /*_.IFELSE___V38*/ meltfptr[19] =
					( /*nil */ NULL);;
				    }
				  else
				    {
				      MELT_LOCATION
					("warmelt-base.melt:1417:/ cond.else");

				      /*^block */
				      /*anyblock */
				      {




					{
					  /*^locexp */
					  melt_assert_failed (("invalid variadic argument to STRING4OUT"), ("warmelt-base.melt") ? ("warmelt-base.melt") : __FILE__, (1417) ? (1417) : __LINE__, __FUNCTION__);
					  ;
					}
					;
			 /*clear *//*_.IFELSE___V38*/
					  meltfptr[19] = 0;
					/*epilog */
				      }
				      ;
				    }
				  ;
				  /*^compute */
				  /*_.IFCPP___V37*/ meltfptr[31] =
				    /*_.IFELSE___V38*/ meltfptr[19];;
				  /*epilog */

				  MELT_LOCATION
				    ("warmelt-base.melt:1417:/ clear");
		       /*clear *//*_.IFELSE___V38*/ meltfptr[19] =
				    0;
				}

#else /*MELT_HAVE_DEBUG */
				/*^cppif.else */
				/*_.IFCPP___V37*/ meltfptr[31] =
				  ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
				;
				MELT_LOCATION
				  ("warmelt-base.melt:1376:/ checksignal");
				MELT_CHECK_SIGNAL ();
				;
				/*epilog */

				/*^clear */
		     /*clear *//*_.IFCPP___V37*/ meltfptr[31] = 0;
			      }
			      ;
			    }
			  ;
			  /*epilog */
			}
			;
		      }
		    ;
		    /*epilog */
		  }
		  ;
		}
	      ;
	      /*epilog */
	    }
	    ;
	  }
	;
	/*epilog */
      }
      ;
      ;
      goto labloop_ARGLOOP_2;
    labexit_ARGLOOP_2:;
      MELT_LOCATION ("warmelt-base.melt:1374:/ loopepilog");
      /*loopepilog */
      /*_.FOREVER___V15*/ meltfptr[7] = /*_.ARGLOOP__V16*/ meltfptr[15];;
    }
    ;
    MELT_LOCATION ("warmelt-base.melt:1419:/ quasiblock");


 /*_.STR__V40*/ meltfptr[16] =
      (meltgc_new_stringdup
       ((meltobject_ptr_t) ( /*_.DIS__V2*/ meltfptr[1]),
	melt_strbuf_str ((melt_ptr_t) ( /*_.OUT__V14*/ meltfptr[11]))));;
    MELT_LOCATION ("warmelt-base.melt:1421:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.STR__V40*/ meltfptr[16];;

    {
      MELT_LOCATION ("warmelt-base.melt:1421:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*_.LET___V39*/ meltfptr[18] = /*_.RETURN___V41*/ meltfptr[17];;

    MELT_LOCATION ("warmelt-base.melt:1419:/ clear");
	   /*clear *//*_.STR__V40*/ meltfptr[16] = 0;
    /*^clear */
	   /*clear *//*_.RETURN___V41*/ meltfptr[17] = 0;
    /*_.LET___V13*/ meltfptr[10] = /*_.LET___V39*/ meltfptr[18];;

    MELT_LOCATION ("warmelt-base.melt:1372:/ clear");
	   /*clear *//*_.OUT__V14*/ meltfptr[11] = 0;
    /*^clear */
	   /*clear *//*_.FOREVER___V15*/ meltfptr[7] = 0;
    /*^clear */
	   /*clear *//*_.LET___V39*/ meltfptr[18] = 0;
    MELT_LOCATION ("warmelt-base.melt:1360:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.LET___V13*/ meltfptr[10];;

    {
      MELT_LOCATION ("warmelt-base.melt:1360:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFELSE___V3*/ meltfptr[2] = 0;
    /*^clear */
	   /*clear *//*_#IS_A__L1*/ meltfnum[0] = 0;
    /*^clear */
	   /*clear *//*_.IFELSE___V6*/ meltfptr[3] = 0;
    /*^clear */
	   /*clear *//*_.LET___V9*/ meltfptr[4] = 0;
    /*^clear */
	   /*clear *//*_.LET___V13*/ meltfptr[10] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("STRING4OUT", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_8_warmelt_base_STRING4OUT_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef melt_variadic_length
#undef melt_variadic_index

#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_8_warmelt_base_STRING4OUT */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_9_warmelt_base_ADD2OUT4NULL (meltclosure_ptr_t meltclosp_,
				      melt_ptr_t meltfirstargp_,
				      const melt_argdescr_cell_t
				      meltxargdescr_[],
				      union meltparam_un * meltxargtab_,
				      const melt_argdescr_cell_t
				      meltxresdescr_[],
				      union meltparam_un * meltxrestab_)
{
  long current_blocklevel_signals_meltrout_9_warmelt_base_ADD2OUT4NULL_melt =
    melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_9_warmelt_base_ADD2OUT4NULL_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 5
    melt_ptr_t mcfr_varptr[5];
#define MELTFRAM_NBVARNUM 1
    long mcfr_varnum[1];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_9_warmelt_base_ADD2OUT4NULL is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_9_warmelt_base_ADD2OUT4NULL_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 5; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_9_warmelt_base_ADD2OUT4NULL nbval 5*/
  meltfram__.mcfr_nbvar = 5 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("ADD2OUT4NULL", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-base.melt:1427:/ getarg");
 /*_.V__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.OUT__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.OUT__V3*/ meltfptr[2])) != NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-base.melt:1428:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#NULL__L1*/ meltfnum[0] =
	(( /*_.V__V2*/ meltfptr[1]) == NULL);;
      MELT_LOCATION ("warmelt-base.melt:1428:/ cond");
      /*cond */ if ( /*_#NULL__L1*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V5*/ meltfptr[4] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-base.melt:1428:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check null v"),
				  ("warmelt-base.melt")
				  ? ("warmelt-base.melt") : __FILE__,
				  (1428) ? (1428) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V5*/ meltfptr[4] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V4*/ meltfptr[3] = /*_.IFELSE___V5*/ meltfptr[4];;
      /*epilog */

      MELT_LOCATION ("warmelt-base.melt:1428:/ clear");
	     /*clear *//*_#NULL__L1*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V5*/ meltfptr[4] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V4*/ meltfptr[3] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

    {
      MELT_LOCATION ("warmelt-base.melt:1429:/ locexp");
      meltgc_add_out ((melt_ptr_t) ( /*_.OUT__V3*/ meltfptr[2]), ("*nil*"));
    }
    ;
    MELT_LOCATION ("warmelt-base.melt:1427:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V4*/ meltfptr[3] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("ADD2OUT4NULL", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_9_warmelt_base_ADD2OUT4NULL_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*noretval */ NULL);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_9_warmelt_base_ADD2OUT4NULL */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_10_warmelt_base_ADD2OUT4INTEGER (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un * meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un * meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_10_warmelt_base_ADD2OUT4INTEGER_melt =
    melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_10_warmelt_base_ADD2OUT4INTEGER_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 5
    melt_ptr_t mcfr_varptr[5];
#define MELTFRAM_NBVARNUM 2
    long mcfr_varnum[2];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_10_warmelt_base_ADD2OUT4INTEGER is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_10_warmelt_base_ADD2OUT4INTEGER_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 5; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_10_warmelt_base_ADD2OUT4INTEGER nbval 5*/
  meltfram__.mcfr_nbvar = 5 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("ADD2OUT4INTEGER", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-base.melt:1432:/ getarg");
 /*_.VN__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.OUT__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.OUT__V3*/ meltfptr[2])) != NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-base.melt:1433:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_INTEGERBOX__L1*/ meltfnum[0] =
	(melt_magic_discr ((melt_ptr_t) ( /*_.VN__V2*/ meltfptr[1])) ==
	 MELTOBMAG_INT);;
      MELT_LOCATION ("warmelt-base.melt:1433:/ cond");
      /*cond */ if ( /*_#IS_INTEGERBOX__L1*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V5*/ meltfptr[4] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-base.melt:1433:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check boxed number vn"),
				  ("warmelt-base.melt")
				  ? ("warmelt-base.melt") : __FILE__,
				  (1433) ? (1433) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V5*/ meltfptr[4] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V4*/ meltfptr[3] = /*_.IFELSE___V5*/ meltfptr[4];;
      /*epilog */

      MELT_LOCATION ("warmelt-base.melt:1433:/ clear");
	     /*clear *//*_#IS_INTEGERBOX__L1*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V5*/ meltfptr[4] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V4*/ meltfptr[3] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    /*^compute */
 /*_#GET_INT__L2*/ meltfnum[0] =
      (melt_get_int ((melt_ptr_t) ( /*_.VN__V2*/ meltfptr[1])));;

    {
      MELT_LOCATION ("warmelt-base.melt:1434:/ locexp");
      meltgc_add_out_dec ((melt_ptr_t) ( /*_.OUT__V3*/ meltfptr[2]),
			  ( /*_#GET_INT__L2*/ meltfnum[0]));
    }
    ;
    MELT_LOCATION ("warmelt-base.melt:1432:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V4*/ meltfptr[3] = 0;
    /*^clear */
	   /*clear *//*_#GET_INT__L2*/ meltfnum[0] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("ADD2OUT4INTEGER", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_10_warmelt_base_ADD2OUT4INTEGER_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*noretval */ NULL);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_10_warmelt_base_ADD2OUT4INTEGER */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_11_warmelt_base_ADD2OUT4STRING (meltclosure_ptr_t meltclosp_,
					 melt_ptr_t meltfirstargp_,
					 const melt_argdescr_cell_t
					 meltxargdescr_[],
					 union meltparam_un * meltxargtab_,
					 const melt_argdescr_cell_t
					 meltxresdescr_[],
					 union meltparam_un * meltxrestab_)
{
  long current_blocklevel_signals_meltrout_11_warmelt_base_ADD2OUT4STRING_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_11_warmelt_base_ADD2OUT4STRING_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 5
    melt_ptr_t mcfr_varptr[5];
#define MELTFRAM_NBVARNUM 1
    long mcfr_varnum[1];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_11_warmelt_base_ADD2OUT4STRING is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_11_warmelt_base_ADD2OUT4STRING_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 5; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_11_warmelt_base_ADD2OUT4STRING nbval 5*/
  meltfram__.mcfr_nbvar = 5 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("ADD2OUT4STRING", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-base.melt:1437:/ getarg");
 /*_.VS__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.OUT__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.OUT__V3*/ meltfptr[2])) != NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-base.melt:1438:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_STRING__L1*/ meltfnum[0] =
	(melt_magic_discr ((melt_ptr_t) ( /*_.VS__V2*/ meltfptr[1])) ==
	 MELTOBMAG_STRING);;
      MELT_LOCATION ("warmelt-base.melt:1438:/ cond");
      /*cond */ if ( /*_#IS_STRING__L1*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V5*/ meltfptr[4] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-base.melt:1438:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check boxed string vs"),
				  ("warmelt-base.melt")
				  ? ("warmelt-base.melt") : __FILE__,
				  (1438) ? (1438) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V5*/ meltfptr[4] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V4*/ meltfptr[3] = /*_.IFELSE___V5*/ meltfptr[4];;
      /*epilog */

      MELT_LOCATION ("warmelt-base.melt:1438:/ clear");
	     /*clear *//*_#IS_STRING__L1*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V5*/ meltfptr[4] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V4*/ meltfptr[3] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

    {
      MELT_LOCATION ("warmelt-base.melt:1439:/ locexp");
      meltgc_add_out ((melt_ptr_t) ( /*_.OUT__V3*/ meltfptr[2]),
		      melt_string_str ((melt_ptr_t)
				       ( /*_.VS__V2*/ meltfptr[1])));
    }
    ;
    MELT_LOCATION ("warmelt-base.melt:1437:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V4*/ meltfptr[3] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("ADD2OUT4STRING", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_11_warmelt_base_ADD2OUT4STRING_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*noretval */ NULL);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_11_warmelt_base_ADD2OUT4STRING */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_12_warmelt_base_ADD2OUT4STRBUF (meltclosure_ptr_t meltclosp_,
					 melt_ptr_t meltfirstargp_,
					 const melt_argdescr_cell_t
					 meltxargdescr_[],
					 union meltparam_un * meltxargtab_,
					 const melt_argdescr_cell_t
					 meltxresdescr_[],
					 union meltparam_un * meltxrestab_)
{
  long current_blocklevel_signals_meltrout_12_warmelt_base_ADD2OUT4STRBUF_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_12_warmelt_base_ADD2OUT4STRBUF_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 5
    melt_ptr_t mcfr_varptr[5];
#define MELTFRAM_NBVARNUM 1
    long mcfr_varnum[1];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_12_warmelt_base_ADD2OUT4STRBUF is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_12_warmelt_base_ADD2OUT4STRBUF_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 5; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_12_warmelt_base_ADD2OUT4STRBUF nbval 5*/
  meltfram__.mcfr_nbvar = 5 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("ADD2OUT4STRBUF", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-base.melt:1442:/ getarg");
 /*_.VSBUF__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.OUT__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.OUT__V3*/ meltfptr[2])) != NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-base.melt:1443:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_STRBUF__L1*/ meltfnum[0] =
	(melt_magic_discr ((melt_ptr_t) ( /*_.VSBUF__V2*/ meltfptr[1])) ==
	 MELTOBMAG_STRBUF);;
      MELT_LOCATION ("warmelt-base.melt:1443:/ cond");
      /*cond */ if ( /*_#IS_STRBUF__L1*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V5*/ meltfptr[4] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-base.melt:1443:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check strbuf vsbuf"),
				  ("warmelt-base.melt")
				  ? ("warmelt-base.melt") : __FILE__,
				  (1443) ? (1443) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V5*/ meltfptr[4] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V4*/ meltfptr[3] = /*_.IFELSE___V5*/ meltfptr[4];;
      /*epilog */

      MELT_LOCATION ("warmelt-base.melt:1443:/ clear");
	     /*clear *//*_#IS_STRBUF__L1*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V5*/ meltfptr[4] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V4*/ meltfptr[3] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

    {
      MELT_LOCATION ("warmelt-base.melt:1444:/ locexp");
      meltgc_add_out ((melt_ptr_t) ( /*_.OUT__V3*/ meltfptr[2]),
		      melt_strbuf_str ((melt_ptr_t) /*_.VSBUF__V2*/
				       meltfptr[1]));
    }
    ;
    MELT_LOCATION ("warmelt-base.melt:1442:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V4*/ meltfptr[3] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("ADD2OUT4STRBUF", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_12_warmelt_base_ADD2OUT4STRBUF_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*noretval */ NULL);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_12_warmelt_base_ADD2OUT4STRBUF */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_13_warmelt_base_ADD2OUT4CLONEDSYMB (meltclosure_ptr_t meltclosp_,
					     melt_ptr_t meltfirstargp_,
					     const melt_argdescr_cell_t
					     meltxargdescr_[],
					     union meltparam_un *
					     meltxargtab_,
					     const melt_argdescr_cell_t
					     meltxresdescr_[],
					     union meltparam_un *
					     meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_13_warmelt_base_ADD2OUT4CLONEDSYMB_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_13_warmelt_base_ADD2OUT4CLONEDSYMB_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 10
    melt_ptr_t mcfr_varptr[10];
#define MELTFRAM_NBVARNUM 3
    long mcfr_varnum[3];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_13_warmelt_base_ADD2OUT4CLONEDSYMB is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_13_warmelt_base_ADD2OUT4CLONEDSYMB_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 10; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_13_warmelt_base_ADD2OUT4CLONEDSYMB nbval 10*/
  meltfram__.mcfr_nbvar = 10 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("ADD2OUT4CLONEDSYMB", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-base.melt:1447:/ getarg");
 /*_.VC__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.OUT__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.OUT__V3*/ meltfptr[2])) != NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {

    MELT_LOCATION ("warmelt-base.melt:1448:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^block */
    /*anyblock */
    {

      /*^objgoto */
      /*objgoto */ goto mtch1_0;
      ;

    /*objlabel */ mtch1_0:;
      MELT_LOCATION ("warmelt-base.melt:1449:/ objlabel");
      ;
      /*^clear */
	    /*clear *//*_.NAMED_NAME__V4*/ meltfptr[3] = 0;
      /*^clear */
	    /*clear *//*_.CSYM_URANK__V5*/ meltfptr[4] = 0;
      /*^cond */
      /*cond */ if (
		     /*normtesterinst */
	(melt_is_instance_of ((melt_ptr_t) ( /*_.VC__V2*/ meltfptr[1]),
			      (melt_ptr_t) (( /*!CLASS_CLONED_SYMBOL */ meltfrout->tabval[0])))))	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

	    /*^getslot */
	    {
	      melt_ptr_t slot = NULL, obj = NULL;
	      obj = (melt_ptr_t) ( /*_.VC__V2*/ meltfptr[1]) /*=obj*/ ;
	      melt_object_get_field (slot, obj, 1, "NAMED_NAME");
     /*_.NAMED_NAME__V4*/ meltfptr[3] = slot;
	    };
	    ;
	    /*^getslot */
	    {
	      melt_ptr_t slot = NULL, obj = NULL;
	      obj = (melt_ptr_t) ( /*_.VC__V2*/ meltfptr[1]) /*=obj*/ ;
	      melt_object_get_field (slot, obj, 3, "CSYM_URANK");
     /*_.CSYM_URANK__V5*/ meltfptr[4] = slot;
	    };
	    ;
	    MELT_LOCATION ("warmelt-base.melt:1451:/ objgoto");
	    /*objgoto */ goto mtch1_1;
	    ;
	  }
	  ;
	}
      else
	{
	  MELT_LOCATION ("warmelt-base.melt:1449:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {

	    MELT_LOCATION ("warmelt-base.melt:1456:/ objgoto");
	    /*objgoto */ goto mtch1_3;
	    ;
	  }
	  ;
	}
      ;

    /*objlabel */ mtch1_1:;
      MELT_LOCATION ("warmelt-base.melt:1451:/ objlabel");
      ;
      /*^clear */
	    /*clear *//*_#ICT__L1*/ meltfnum[0] = 0;
      /*^cond */
      /*cond */ if (
					 /* integerbox_of IBOXOF_mtch1__1 ? *//*_.CSYM_URANK__V5*/
		     meltfptr[4]
		     && melt_magic_discr ((melt_ptr_t) /*_.CSYM_URANK__V5*/ meltfptr[4]) == MELTOBMAG_INT)	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

	    /*^clear */
	      /*clear *//*_#ICT__L1*/ meltfnum[0] = 0;

	    {
	      /*^locexp */
	      /* integerbox_of IBOXOF_mtch1__1 ! *//*_#ICT__L1*/ meltfnum[0] =
		((struct meltint_st *) /*_.CSYM_URANK__V5*/ meltfptr[4])->
		val;;
	    }
	    ;
	    MELT_LOCATION ("warmelt-base.melt:1449:/ objgoto");
	    /*objgoto */ goto mtch1_2;
	    ;
	  }
	  ;
	}
      else
	{
	  MELT_LOCATION ("warmelt-base.melt:1451:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {

	    MELT_LOCATION ("warmelt-base.melt:1456:/ objgoto");
	    /*objgoto */ goto mtch1_3;
	    ;
	  }
	  ;
	}
      ;

    /*objlabel */ mtch1_2:;
      MELT_LOCATION ("warmelt-base.melt:1449:/ objlabel");
      ;
      /*^quasiblock */


      /*_.NVARNAM__V6*/ meltfptr[4] = /*_.NAMED_NAME__V4*/ meltfptr[3];;
      /*^compute */
      /*_#NVARURANK__L2*/ meltfnum[1] = /*_#ICT__L1*/ meltfnum[0];;

#if MELT_HAVE_DEBUG
      MELT_LOCATION ("warmelt-base.melt:1452:/ cppif.then");
      /*^block */
      /*anyblock */
      {

	/*^checksignal */
	MELT_CHECK_SIGNAL ();
	;
    /*_#IS_STRING__L3*/ meltfnum[2] =
	  (melt_magic_discr ((melt_ptr_t) ( /*_.NVARNAM__V6*/ meltfptr[4])) ==
	   MELTOBMAG_STRING);;
	MELT_LOCATION ("warmelt-base.melt:1452:/ cond");
	/*cond */ if ( /*_#IS_STRING__L3*/ meltfnum[2])	/*then */
	  {
	    /*^cond.then */
	    /*_.IFELSE___V8*/ meltfptr[7] = ( /*nil */ NULL);;
	  }
	else
	  {
	    MELT_LOCATION ("warmelt-base.melt:1452:/ cond.else");

	    /*^block */
	    /*anyblock */
	    {




	      {
		/*^locexp */
		melt_assert_failed (("check nvarnam"),
				    ("warmelt-base.melt")
				    ? ("warmelt-base.melt") : __FILE__,
				    (1452) ? (1452) : __LINE__, __FUNCTION__);
		;
	      }
	      ;
		/*clear *//*_.IFELSE___V8*/ meltfptr[7] = 0;
	      /*epilog */
	    }
	    ;
	  }
	;
	/*^compute */
	/*_.IFCPP___V7*/ meltfptr[6] = /*_.IFELSE___V8*/ meltfptr[7];;
	/*epilog */

	MELT_LOCATION ("warmelt-base.melt:1452:/ clear");
	      /*clear *//*_#IS_STRING__L3*/ meltfnum[2] = 0;
	/*^clear */
	      /*clear *//*_.IFELSE___V8*/ meltfptr[7] = 0;
      }

#else /*MELT_HAVE_DEBUG */
      /*^cppif.else */
      /*_.IFCPP___V7*/ meltfptr[6] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
      ;

      {
	MELT_LOCATION ("warmelt-base.melt:1453:/ locexp");
	meltgc_add_out_cident ((melt_ptr_t) ( /*_.OUT__V3*/ meltfptr[2]),
			       melt_string_str ((melt_ptr_t)
						( /*_.NVARNAM__V6*/
						 meltfptr[4])));
      }
      ;

      {
	MELT_LOCATION ("warmelt-base.melt:1454:/ locexp");
	meltgc_add_out ((melt_ptr_t) ( /*_.OUT__V3*/ meltfptr[2]), ("__"));
      }
      ;

      {
	MELT_LOCATION ("warmelt-base.melt:1455:/ locexp");
	meltgc_add_out_dec ((melt_ptr_t) ( /*_.OUT__V3*/ meltfptr[2]),
			    ( /*_#NVARURANK__L2*/ meltfnum[1]));
      }
      ;
      MELT_LOCATION ("warmelt-base.melt:1449:/ checksignal");
      MELT_CHECK_SIGNAL ();
      ;

      /*^clear */
	    /*clear *//*_.NVARNAM__V6*/ meltfptr[4] = 0;
      /*^clear */
	    /*clear *//*_#NVARURANK__L2*/ meltfnum[1] = 0;
      /*^clear */
	    /*clear *//*_.IFCPP___V7*/ meltfptr[6] = 0;
      /*^objgoto */
      /*objgoto */ goto mtch1__end /*endmatch */ ;
      ;

    /*objlabel */ mtch1_3:;
      MELT_LOCATION ("warmelt-base.melt:1456:/ objlabel");
      ;
      /*^quasiblock */



#if MELT_HAVE_DEBUG
      MELT_LOCATION ("warmelt-base.melt:1457:/ cppif.then");
      /*^block */
      /*anyblock */
      {

	/*^checksignal */
	MELT_CHECK_SIGNAL ();
	;
	/*^cond */
	/*cond */ if (( /*nil */ NULL))	/*then */
	  {
	    /*^cond.then */
	    /*_.IFELSE___V10*/ meltfptr[4] = ( /*nil */ NULL);;
	  }
	else
	  {
	    MELT_LOCATION ("warmelt-base.melt:1457:/ cond.else");

	    /*^block */
	    /*anyblock */
	    {




	      {
		/*^locexp */
		melt_assert_failed (("bad cloned symbol"),
				    ("warmelt-base.melt")
				    ? ("warmelt-base.melt") : __FILE__,
				    (1457) ? (1457) : __LINE__, __FUNCTION__);
		;
	      }
	      ;
		/*clear *//*_.IFELSE___V10*/ meltfptr[4] = 0;
	      /*epilog */
	    }
	    ;
	  }
	;
	/*^compute */
	/*_.IFCPP___V9*/ meltfptr[7] = /*_.IFELSE___V10*/ meltfptr[4];;
	/*epilog */

	MELT_LOCATION ("warmelt-base.melt:1457:/ clear");
	      /*clear *//*_.IFELSE___V10*/ meltfptr[4] = 0;
      }

#else /*MELT_HAVE_DEBUG */
      /*^cppif.else */
      /*_.IFCPP___V9*/ meltfptr[7] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
      ;

      MELT_LOCATION ("warmelt-base.melt:1456:/ clear");
	    /*clear *//*_.IFCPP___V9*/ meltfptr[7] = 0;
      /*^objgoto */
      /*objgoto */ goto mtch1__end /*endmatch */ ;
      ;

    /*objlabel */ mtch1__end:;
      MELT_LOCATION ("warmelt-base.melt:1448:/ objlabel");
      ;
    }
    ;
    /*epilog */
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("ADD2OUT4CLONEDSYMB", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_13_warmelt_base_ADD2OUT4CLONEDSYMB_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*noretval */ NULL);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_13_warmelt_base_ADD2OUT4CLONEDSYMB */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_14_warmelt_base_ADD2OUT4NAMED (meltclosure_ptr_t meltclosp_,
					melt_ptr_t meltfirstargp_,
					const melt_argdescr_cell_t
					meltxargdescr_[],
					union meltparam_un * meltxargtab_,
					const melt_argdescr_cell_t
					meltxresdescr_[],
					union meltparam_un * meltxrestab_)
{
  long current_blocklevel_signals_meltrout_14_warmelt_base_ADD2OUT4NAMED_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_14_warmelt_base_ADD2OUT4NAMED_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 4
    melt_ptr_t mcfr_varptr[4];
/*no varnum*/
#define MELTFRAM_NBVARNUM /*none*/0
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_14_warmelt_base_ADD2OUT4NAMED is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_14_warmelt_base_ADD2OUT4NAMED_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 4; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_14_warmelt_base_ADD2OUT4NAMED nbval 4*/
  meltfram__.mcfr_nbvar = 4 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("ADD2OUT4NAMED", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-base.melt:1460:/ getarg");
 /*_.VN__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.OUT__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.OUT__V3*/ meltfptr[2])) != NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {

    MELT_LOCATION ("warmelt-base.melt:1461:/ cond");
    /*cond */ if (
		   /*ifisa */
		   melt_is_instance_of ((melt_ptr_t)
					( /*_.VN__V2*/ meltfptr[1]),
					(melt_ptr_t) (( /*!CLASS_NAMED */
						       meltfrout->tabval[0])))
      )				/*then */
      {
	/*^cond.then */
	/*^getslot */
	{
	  melt_ptr_t slot = NULL, obj = NULL;
	  obj = (melt_ptr_t) ( /*_.VN__V2*/ meltfptr[1]) /*=obj*/ ;
	  melt_object_get_field (slot, obj, 1, "NAMED_NAME");
   /*_.NAMED_NAME__V4*/ meltfptr[3] = slot;
	};
	;
      }
    else
      {				/*^cond.else */

  /*_.NAMED_NAME__V4*/ meltfptr[3] = NULL;;
      }
    ;

    {
      MELT_LOCATION ("warmelt-base.melt:1461:/ locexp");
      meltgc_add_out_cident ((melt_ptr_t) ( /*_.OUT__V3*/ meltfptr[2]),
			     melt_string_str ((melt_ptr_t)
					      ( /*_.NAMED_NAME__V4*/
					       meltfptr[3])));
    }
    ;
    /*epilog */

    MELT_LOCATION ("warmelt-base.melt:1460:/ clear");
	   /*clear *//*_.NAMED_NAME__V4*/ meltfptr[3] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("ADD2OUT4NAMED", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_14_warmelt_base_ADD2OUT4NAMED_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*noretval */ NULL);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_14_warmelt_base_ADD2OUT4NAMED */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_15_warmelt_base_ADD2OUT4ROOTOBJECT (meltclosure_ptr_t meltclosp_,
					     melt_ptr_t meltfirstargp_,
					     const melt_argdescr_cell_t
					     meltxargdescr_[],
					     union meltparam_un *
					     meltxargtab_,
					     const melt_argdescr_cell_t
					     meltxresdescr_[],
					     union meltparam_un *
					     meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_15_warmelt_base_ADD2OUT4ROOTOBJECT_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_15_warmelt_base_ADD2OUT4ROOTOBJECT_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 5
    melt_ptr_t mcfr_varptr[5];
#define MELTFRAM_NBVARNUM 1
    long mcfr_varnum[1];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_15_warmelt_base_ADD2OUT4ROOTOBJECT is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_15_warmelt_base_ADD2OUT4ROOTOBJECT_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 5; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_15_warmelt_base_ADD2OUT4ROOTOBJECT nbval 5*/
  meltfram__.mcfr_nbvar = 5 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("ADD2OUT4ROOTOBJECT", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-base.melt:1464:/ getarg");
 /*_.VO__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.OUT__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.OUT__V3*/ meltfptr[2])) != NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


    {
      MELT_LOCATION ("warmelt-base.melt:1465:/ locexp");
      meltgc_add_out ((melt_ptr_t) ( /*_.OUT__V3*/ meltfptr[2]), ("*"));
    }
    ;
 /*_.DISCRIM__V4*/ meltfptr[3] =
      ((melt_ptr_t) (melt_discr ((melt_ptr_t) ( /*_.VO__V2*/ meltfptr[1]))));;
    MELT_LOCATION ("warmelt-base.melt:1466:/ cond");
    /*cond */ if (
		   /*ifisa */
		   melt_is_instance_of ((melt_ptr_t)
					( /*_.DISCRIM__V4*/ meltfptr[3]),
					(melt_ptr_t) (( /*!CLASS_NAMED */
						       meltfrout->tabval[0])))
      )				/*then */
      {
	/*^cond.then */
	/*^getslot */
	{
	  melt_ptr_t slot = NULL, obj = NULL;
	  obj = (melt_ptr_t) ( /*_.DISCRIM__V4*/ meltfptr[3]) /*=obj*/ ;
	  melt_object_get_field (slot, obj, 1, "NAMED_NAME");
   /*_.NAMED_NAME__V5*/ meltfptr[4] = slot;
	};
	;
      }
    else
      {				/*^cond.else */

  /*_.NAMED_NAME__V5*/ meltfptr[4] = NULL;;
      }
    ;

    {
      MELT_LOCATION ("warmelt-base.melt:1466:/ locexp");
      meltgc_add_out ((melt_ptr_t) ( /*_.OUT__V3*/ meltfptr[2]),
		      melt_string_str ((melt_ptr_t)
				       ( /*_.NAMED_NAME__V5*/ meltfptr[4])));
    }
    ;

    {
      MELT_LOCATION ("warmelt-base.melt:1467:/ locexp");
      meltgc_add_out ((melt_ptr_t) ( /*_.OUT__V3*/ meltfptr[2]), ("/"));
    }
    ;
 /*_#OBJ_HASH__L1*/ meltfnum[0] =
      (melt_obj_hash ((melt_ptr_t) ( /*_.VO__V2*/ meltfptr[1])));;

    {
      MELT_LOCATION ("warmelt-base.melt:1468:/ locexp");
      meltgc_add_out_hex ((melt_ptr_t) ( /*_.OUT__V3*/ meltfptr[2]),
			  ( /*_#OBJ_HASH__L1*/ meltfnum[0]));
    }
    ;

    {
      MELT_LOCATION ("warmelt-base.melt:1469:/ locexp");
      meltgc_add_out ((melt_ptr_t) ( /*_.OUT__V3*/ meltfptr[2]), ("."));
    }
    ;
    MELT_LOCATION ("warmelt-base.melt:1464:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*epilog */

    /*^clear */
	   /*clear *//*_.DISCRIM__V4*/ meltfptr[3] = 0;
    /*^clear */
	   /*clear *//*_.NAMED_NAME__V5*/ meltfptr[4] = 0;
    /*^clear */
	   /*clear *//*_#OBJ_HASH__L1*/ meltfnum[0] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("ADD2OUT4ROOTOBJECT", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_15_warmelt_base_ADD2OUT4ROOTOBJECT_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*noretval */ NULL);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_15_warmelt_base_ADD2OUT4ROOTOBJECT */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_16_warmelt_base_ADD2OUT4ANY (meltclosure_ptr_t meltclosp_,
				      melt_ptr_t meltfirstargp_,
				      const melt_argdescr_cell_t
				      meltxargdescr_[],
				      union meltparam_un * meltxargtab_,
				      const melt_argdescr_cell_t
				      meltxresdescr_[],
				      union meltparam_un * meltxrestab_)
{
  long current_blocklevel_signals_meltrout_16_warmelt_base_ADD2OUT4ANY_melt =
    melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_16_warmelt_base_ADD2OUT4ANY_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 5
    melt_ptr_t mcfr_varptr[5];
/*no varnum*/
#define MELTFRAM_NBVARNUM /*none*/0
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_16_warmelt_base_ADD2OUT4ANY is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_16_warmelt_base_ADD2OUT4ANY_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 5; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_16_warmelt_base_ADD2OUT4ANY nbval 5*/
  meltfram__.mcfr_nbvar = 5 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("ADD2OUT4ANY", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-base.melt:1472:/ getarg");
 /*_.VA__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.OUT__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.OUT__V3*/ meltfptr[2])) != NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


    {
      MELT_LOCATION ("warmelt-base.melt:1473:/ locexp");
      meltgc_add_out ((melt_ptr_t) ( /*_.OUT__V3*/ meltfptr[2]), ("*"));
    }
    ;
 /*_.DISCRIM__V4*/ meltfptr[3] =
      ((melt_ptr_t) (melt_discr ((melt_ptr_t) ( /*_.VA__V2*/ meltfptr[1]))));;
    MELT_LOCATION ("warmelt-base.melt:1474:/ cond");
    /*cond */ if (
		   /*ifisa */
		   melt_is_instance_of ((melt_ptr_t)
					( /*_.DISCRIM__V4*/ meltfptr[3]),
					(melt_ptr_t) (( /*!CLASS_NAMED */
						       meltfrout->tabval[0])))
      )				/*then */
      {
	/*^cond.then */
	/*^getslot */
	{
	  melt_ptr_t slot = NULL, obj = NULL;
	  obj = (melt_ptr_t) ( /*_.DISCRIM__V4*/ meltfptr[3]) /*=obj*/ ;
	  melt_object_get_field (slot, obj, 1, "NAMED_NAME");
   /*_.NAMED_NAME__V5*/ meltfptr[4] = slot;
	};
	;
      }
    else
      {				/*^cond.else */

  /*_.NAMED_NAME__V5*/ meltfptr[4] = NULL;;
      }
    ;

    {
      MELT_LOCATION ("warmelt-base.melt:1474:/ locexp");
      meltgc_add_out ((melt_ptr_t) ( /*_.OUT__V3*/ meltfptr[2]),
		      melt_string_str ((melt_ptr_t)
				       ( /*_.NAMED_NAME__V5*/ meltfptr[4])));
    }
    ;
    MELT_LOCATION ("warmelt-base.melt:1472:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*epilog */

    /*^clear */
	   /*clear *//*_.DISCRIM__V4*/ meltfptr[3] = 0;
    /*^clear */
	   /*clear *//*_.NAMED_NAME__V5*/ meltfptr[4] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("ADD2OUT4ANY", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_16_warmelt_base_ADD2OUT4ANY_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*noretval */ NULL);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_16_warmelt_base_ADD2OUT4ANY */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_17_warmelt_base_MAPSTRING_EVERY (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un * meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un * meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_17_warmelt_base_MAPSTRING_EVERY_melt =
    melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_17_warmelt_base_MAPSTRING_EVERY_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 6
    melt_ptr_t mcfr_varptr[6];
#define MELTFRAM_NBVARNUM 2
    long mcfr_varnum[2];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_17_warmelt_base_MAPSTRING_EVERY is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_17_warmelt_base_MAPSTRING_EVERY_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 6; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_17_warmelt_base_MAPSTRING_EVERY nbval 6*/
  meltfram__.mcfr_nbvar = 6 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("MAPSTRING_EVERY", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-base.melt:1516:/ getarg");
 /*_.MAP__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.F__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.F__V3*/ meltfptr[2])) != NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {

    MELT_LOCATION ("warmelt-base.melt:1520:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#IS_MAPSTRING__L1*/ meltfnum[0] =
      (melt_magic_discr ((melt_ptr_t) ( /*_.MAP__V2*/ meltfptr[1])) ==
       MELTOBMAG_MAPSTRINGS);;
    MELT_LOCATION ("warmelt-base.melt:1520:/ cond");
    /*cond */ if ( /*_#IS_MAPSTRING__L1*/ meltfnum[0])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-base.melt:1521:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
   /*_#IS_CLOSURE__L2*/ meltfnum[1] =
	    (melt_magic_discr ((melt_ptr_t) ( /*_.F__V3*/ meltfptr[2])) ==
	     MELTOBMAG_CLOSURE);;
	  MELT_LOCATION ("warmelt-base.melt:1521:/ cond");
	  /*cond */ if ( /*_#IS_CLOSURE__L2*/ meltfnum[1])	/*then */
	    {
	      /*^cond.then */
	      /*^block */
	      /*anyblock */
	      {

		/*citerblock FOREACH_IN_MAPSTRING */
		{
		  /*foreach_in_mapstring meltcit1__EACHSTRMAP : */ int
		    meltcit1__EACHSTRMAP_ix = 0, meltcit1__EACHSTRMAP_siz = 0;
		  for (meltcit1__EACHSTRMAP_ix = 0;
		       /* we retrieve in meltcit1__EACHSTRMAP_siz the size at each iteration since it could change. */
		       meltcit1__EACHSTRMAP_ix >= 0
		       && (meltcit1__EACHSTRMAP_siz =
			   melt_size_mapstrings ((struct meltmapstrings_st *)
						 /*_.MAP__V2*/ meltfptr[1])) >
		       0
		       && meltcit1__EACHSTRMAP_ix < meltcit1__EACHSTRMAP_siz;
		       meltcit1__EACHSTRMAP_ix++)
		    {
		      const char *meltcit1__EACHSTRMAP_str = NULL;
		      const char *meltcit1__EACHSTRMAP_nam = NULL;
    /*_.CURAT__V4*/ meltfptr[3] = NULL;
    /*_.CURVAL__V5*/ meltfptr[4] = NULL;
		      meltcit1__EACHSTRMAP_str =
			((struct meltmapstrings_st *) /*_.MAP__V2*/
			 meltfptr[1])->entab[meltcit1__EACHSTRMAP_ix].e_at;
		      if (!meltcit1__EACHSTRMAP_str
			  || meltcit1__EACHSTRMAP_str == HTAB_DELETED_ENTRY)
			continue;	/*foreach_in_mapstring meltcit1__EACHSTRMAP inside before */
		      /*_.CURVAL__V5*/ meltfptr[4] =
			((struct meltmapstrings_st *) /*_.MAP__V2*/
			 meltfptr[1])->entab[meltcit1__EACHSTRMAP_ix].e_va;
		      if (! /*_.CURVAL__V5*/ meltfptr[4])
			continue;
		      if (melt_is_instance_of
			  ((melt_ptr_t) /*_.CURVAL__V5*/ meltfptr[4],
			   (melt_ptr_t) MELT_PREDEF (CLASS_NAMED))
			  && ( /*_.CURAT__V4*/ meltfptr[3] =
			      melt_object_nth_field ((melt_ptr_t)
						     /*_.CURVAL__V5*/
						     meltfptr[4],
						     MELTFIELD_NAMED_NAME)) !=
			  NULL
			  && (meltcit1__EACHSTRMAP_nam =
			      melt_string_str ((melt_ptr_t) /*_.CURAT__V4*/
					       meltfptr[3])) != (char *) 0
			  && !strcmp (meltcit1__EACHSTRMAP_nam,
				      meltcit1__EACHSTRMAP_str))
			/*_.CURAT__V4*/ meltfptr[3] =
			  /*_.CURAT__V4*/ meltfptr[3];
		      else
			{
      /*_.CURAT__V4*/ meltfptr[3] = NULL;
      /*_.CURAT__V4*/ meltfptr[3] =
			    meltgc_new_stringdup ((meltobject_ptr_t)
						  MELT_PREDEF (DISCR_STRING),
						  meltcit1__EACHSTRMAP_str);
			}
		      meltcit1__EACHSTRMAP_str = (const char *) 0;
		      meltcit1__EACHSTRMAP_nam = (const char *) 0;



		      MELT_LOCATION ("warmelt-base.melt:1525:/ checksignal");
		      MELT_CHECK_SIGNAL ();
		      ;
		      /*^apply */
		      /*apply */
		      {
			union meltparam_un argtab[1];
			memset (&argtab, 0, sizeof (argtab));
			/*^apply.arg */
			argtab[0].meltbp_aptr =
			  (melt_ptr_t *) & /*_.CURVAL__V5*/ meltfptr[4];
			/*_.F__V6*/ meltfptr[5] =
			  melt_apply ((meltclosure_ptr_t)
				      ( /*_.F__V3*/ meltfptr[2]),
				      (melt_ptr_t) ( /*_.CURAT__V4*/
						    meltfptr[3]),
				      (MELTBPARSTR_PTR ""), argtab, "",
				      (union meltparam_un *) 0);
		      }
		      ;
		      /* end foreach_in_mapstring meltcit1__EACHSTRMAP */
    /*_.CURAT__V4*/ meltfptr[3] = NULL;
    /*_.CURVAL__V5*/ meltfptr[4] = NULL;
		    }

		  /*citerepilog */

		  MELT_LOCATION ("warmelt-base.melt:1522:/ clear");
		/*clear *//*_.CURAT__V4*/ meltfptr[3] = 0;
		  /*^clear */
		/*clear *//*_.CURVAL__V5*/ meltfptr[4] = 0;
		  /*^clear */
		/*clear *//*_.F__V6*/ meltfptr[5] = 0;
		}		/*endciterblock FOREACH_IN_MAPSTRING */
		;
		/*epilog */
	      }
	      ;
	    }			/*noelse */
	  ;
	  /*epilog */

	  MELT_LOCATION ("warmelt-base.melt:1520:/ clear");
	     /*clear *//*_#IS_CLOSURE__L2*/ meltfnum[1] = 0;
	}
	;
      }				/*noelse */
    ;
    /*epilog */

    MELT_LOCATION ("warmelt-base.melt:1516:/ clear");
	   /*clear *//*_#IS_MAPSTRING__L1*/ meltfnum[0] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("MAPSTRING_EVERY", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_17_warmelt_base_MAPSTRING_EVERY_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*noretval */ NULL);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_17_warmelt_base_MAPSTRING_EVERY */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_18_warmelt_base_MAPSTRING_ITERATE_TEST (meltclosure_ptr_t meltclosp_,
						 melt_ptr_t meltfirstargp_,
						 const melt_argdescr_cell_t
						 meltxargdescr_[],
						 union meltparam_un *
						 meltxargtab_,
						 const melt_argdescr_cell_t
						 meltxresdescr_[],
						 union meltparam_un *
						 meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_18_warmelt_base_MAPSTRING_ITERATE_TEST_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_18_warmelt_base_MAPSTRING_ITERATE_TEST_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 10
    melt_ptr_t mcfr_varptr[10];
#define MELTFRAM_NBVARNUM 3
    long mcfr_varnum[3];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_18_warmelt_base_MAPSTRING_ITERATE_TEST is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_18_warmelt_base_MAPSTRING_ITERATE_TEST_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 10; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_18_warmelt_base_MAPSTRING_ITERATE_TEST nbval 10*/
  meltfram__.mcfr_nbvar = 10 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("MAPSTRING_ITERATE_TEST", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-base.melt:1529:/ getarg");
 /*_.MAP__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.F__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.F__V3*/ meltfptr[2])) != NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {

    MELT_LOCATION ("warmelt-base.melt:1534:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#IS_MAPSTRING__L1*/ meltfnum[0] =
      (melt_magic_discr ((melt_ptr_t) ( /*_.MAP__V2*/ meltfptr[1])) ==
       MELTOBMAG_MAPSTRINGS);;
    MELT_LOCATION ("warmelt-base.melt:1534:/ cond");
    /*cond */ if ( /*_#IS_MAPSTRING__L1*/ meltfnum[0])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-base.melt:1535:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
   /*_#IS_CLOSURE__L2*/ meltfnum[1] =
	    (melt_magic_discr ((melt_ptr_t) ( /*_.F__V3*/ meltfptr[2])) ==
	     MELTOBMAG_CLOSURE);;
	  MELT_LOCATION ("warmelt-base.melt:1535:/ cond");
	  /*cond */ if ( /*_#IS_CLOSURE__L2*/ meltfnum[1])	/*then */
	    {
	      /*^cond.then */
	      /*^block */
	      /*anyblock */
	      {

		/*citerblock FOREACH_IN_MAPSTRING */
		{
		  /*foreach_in_mapstring meltcit1__EACHSTRMAP : */ int
		    meltcit1__EACHSTRMAP_ix = 0, meltcit1__EACHSTRMAP_siz = 0;
		  for (meltcit1__EACHSTRMAP_ix = 0;
		       /* we retrieve in meltcit1__EACHSTRMAP_siz the size at each iteration since it could change. */
		       meltcit1__EACHSTRMAP_ix >= 0
		       && (meltcit1__EACHSTRMAP_siz =
			   melt_size_mapstrings ((struct meltmapstrings_st *)
						 /*_.MAP__V2*/ meltfptr[1])) >
		       0
		       && meltcit1__EACHSTRMAP_ix < meltcit1__EACHSTRMAP_siz;
		       meltcit1__EACHSTRMAP_ix++)
		    {
		      const char *meltcit1__EACHSTRMAP_str = NULL;
		      const char *meltcit1__EACHSTRMAP_nam = NULL;
    /*_.CURAT__V4*/ meltfptr[3] = NULL;
    /*_.CURVAL__V5*/ meltfptr[4] = NULL;
		      meltcit1__EACHSTRMAP_str =
			((struct meltmapstrings_st *) /*_.MAP__V2*/
			 meltfptr[1])->entab[meltcit1__EACHSTRMAP_ix].e_at;
		      if (!meltcit1__EACHSTRMAP_str
			  || meltcit1__EACHSTRMAP_str == HTAB_DELETED_ENTRY)
			continue;	/*foreach_in_mapstring meltcit1__EACHSTRMAP inside before */
		      /*_.CURVAL__V5*/ meltfptr[4] =
			((struct meltmapstrings_st *) /*_.MAP__V2*/
			 meltfptr[1])->entab[meltcit1__EACHSTRMAP_ix].e_va;
		      if (! /*_.CURVAL__V5*/ meltfptr[4])
			continue;
		      if (melt_is_instance_of
			  ((melt_ptr_t) /*_.CURVAL__V5*/ meltfptr[4],
			   (melt_ptr_t) MELT_PREDEF (CLASS_NAMED))
			  && ( /*_.CURAT__V4*/ meltfptr[3] =
			      melt_object_nth_field ((melt_ptr_t)
						     /*_.CURVAL__V5*/
						     meltfptr[4],
						     MELTFIELD_NAMED_NAME)) !=
			  NULL
			  && (meltcit1__EACHSTRMAP_nam =
			      melt_string_str ((melt_ptr_t) /*_.CURAT__V4*/
					       meltfptr[3])) != (char *) 0
			  && !strcmp (meltcit1__EACHSTRMAP_nam,
				      meltcit1__EACHSTRMAP_str))
			/*_.CURAT__V4*/ meltfptr[3] =
			  /*_.CURAT__V4*/ meltfptr[3];
		      else
			{
      /*_.CURAT__V4*/ meltfptr[3] = NULL;
      /*_.CURAT__V4*/ meltfptr[3] =
			    meltgc_new_stringdup ((meltobject_ptr_t)
						  MELT_PREDEF (DISCR_STRING),
						  meltcit1__EACHSTRMAP_str);
			}
		      meltcit1__EACHSTRMAP_str = (const char *) 0;
		      meltcit1__EACHSTRMAP_nam = (const char *) 0;



		      MELT_LOCATION ("warmelt-base.melt:1541:/ checksignal");
		      MELT_CHECK_SIGNAL ();
		      ;
		      MELT_LOCATION ("warmelt-base.melt:1539:/ quasiblock");


		      /*^multiapply */
		      /*multiapply 2args, 1x.res */
		      {
			union meltparam_un argtab[1];

			union meltparam_un restab[1];
			memset (&restab, 0, sizeof (restab));
			memset (&argtab, 0, sizeof (argtab));
			/*^multiapply.arg */
			argtab[0].meltbp_aptr =
			  (melt_ptr_t *) & /*_.CURVAL__V5*/ meltfptr[4];
			/*^multiapply.xres */
			restab[0].meltbp_aptr =
			  (melt_ptr_t *) & /*_.OTHER__V8*/ meltfptr[7];
			/*^multiapply.appl */
			/*_.TEST__V7*/ meltfptr[6] =
			  melt_apply ((meltclosure_ptr_t)
				      ( /*_.F__V3*/ meltfptr[2]),
				      (melt_ptr_t) ( /*_.CURAT__V4*/
						    meltfptr[3]),
				      (MELTBPARSTR_PTR ""), argtab,
				      (MELTBPARSTR_PTR ""), restab);
		      }
		      ;
		      /*^quasiblock */


		      MELT_LOCATION ("warmelt-base.melt:1542:/ checksignal");
		      MELT_CHECK_SIGNAL ();
		      ;
      /*_#NULL__L3*/ meltfnum[2] =
			(( /*_.TEST__V7*/ meltfptr[6]) == NULL);;
		      MELT_LOCATION ("warmelt-base.melt:1542:/ cond");
		      /*cond */ if ( /*_#NULL__L3*/ meltfnum[2])	/*then */
			{
			  /*^cond.then */
			  /*^block */
			  /*anyblock */
			  {

			    MELT_LOCATION
			      ("warmelt-base.melt:1543:/ checksignal");
			    MELT_CHECK_SIGNAL ();
			    ;
			    /*^quasiblock */


			    /*_.RETVAL___V1*/ meltfptr[0] =
			      /*_.CURAT__V4*/ meltfptr[3];;
			    MELT_LOCATION
			      ("warmelt-base.melt:1543:/ putxtraresult");
			    if (!meltxrestab_ || !meltxresdescr_)
			      goto labend_rout;
			    if (meltxresdescr_[0] != MELTBPAR_PTR)
			      goto labend_rout;
			    if (meltxrestab_[0].meltbp_aptr)
			      *(meltxrestab_[0].meltbp_aptr) =
				(melt_ptr_t) ( /*_.CURVAL__V5*/ meltfptr[4]);
			    ;
			    /*^putxtraresult */
			    if (!meltxrestab_ || !meltxresdescr_)
			      goto labend_rout;
			    if (meltxresdescr_[1] != MELTBPAR_PTR)
			      goto labend_rout;
			    if (meltxrestab_[1].meltbp_aptr)
			      *(meltxrestab_[1].meltbp_aptr) =
				(melt_ptr_t) ( /*_.OTHER__V8*/ meltfptr[7]);
			    ;
			    /*^finalreturn */
			    ;
			    /*finalret */ goto labend_rout;
			    /*_.IF___V9*/ meltfptr[8] =
			      /*_.RETURN___V10*/ meltfptr[9];;
			    /*epilog */

			    MELT_LOCATION ("warmelt-base.melt:1542:/ clear");
		  /*clear *//*_.RETURN___V10*/ meltfptr[9] = 0;
			  }
			  ;
			}
		      else
			{	/*^cond.else */

       /*_.IF___V9*/ meltfptr[8] = NULL;;
			}
		      ;
		      /*^compute */
		      /*_.MULTI___V6*/ meltfptr[5] =
			/*_.IF___V9*/ meltfptr[8];;

		      MELT_LOCATION ("warmelt-base.melt:1539:/ clear");
		/*clear *//*_#NULL__L3*/ meltfnum[2] = 0;
		      /*^clear */
		/*clear *//*_.IF___V9*/ meltfptr[8] = 0;

		      /*^clear */
		/*clear *//*_.OTHER__V8*/ meltfptr[7] = 0;
		      /* end foreach_in_mapstring meltcit1__EACHSTRMAP */
    /*_.CURAT__V4*/ meltfptr[3] = NULL;
    /*_.CURVAL__V5*/ meltfptr[4] = NULL;
		    }

		  /*citerepilog */

		  MELT_LOCATION ("warmelt-base.melt:1536:/ clear");
		/*clear *//*_.CURAT__V4*/ meltfptr[3] = 0;
		  /*^clear */
		/*clear *//*_.CURVAL__V5*/ meltfptr[4] = 0;
		  /*^clear */
		/*clear *//*_.MULTI___V6*/ meltfptr[5] = 0;
		}		/*endciterblock FOREACH_IN_MAPSTRING */
		;
		/*epilog */
	      }
	      ;
	    }			/*noelse */
	  ;
	  /*epilog */

	  MELT_LOCATION ("warmelt-base.melt:1534:/ clear");
	     /*clear *//*_#IS_CLOSURE__L2*/ meltfnum[1] = 0;
	}
	;
      }				/*noelse */
    ;
    /*epilog */

    MELT_LOCATION ("warmelt-base.melt:1529:/ clear");
	   /*clear *//*_#IS_MAPSTRING__L1*/ meltfnum[0] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("MAPSTRING_ITERATE_TEST", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_18_warmelt_base_MAPSTRING_ITERATE_TEST_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_18_warmelt_base_MAPSTRING_ITERATE_TEST */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_19_warmelt_base_MULTIPLE_EVERY (meltclosure_ptr_t meltclosp_,
					 melt_ptr_t meltfirstargp_,
					 const melt_argdescr_cell_t
					 meltxargdescr_[],
					 union meltparam_un * meltxargtab_,
					 const melt_argdescr_cell_t
					 meltxresdescr_[],
					 union meltparam_un * meltxrestab_)
{
  long current_blocklevel_signals_meltrout_19_warmelt_base_MULTIPLE_EVERY_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_19_warmelt_base_MULTIPLE_EVERY_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 5
    melt_ptr_t mcfr_varptr[5];
#define MELTFRAM_NBVARNUM 3
    long mcfr_varnum[3];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_19_warmelt_base_MULTIPLE_EVERY is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_19_warmelt_base_MULTIPLE_EVERY_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 5; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_19_warmelt_base_MULTIPLE_EVERY nbval 5*/
  meltfram__.mcfr_nbvar = 5 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("MULTIPLE_EVERY", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-base.melt:1578:/ getarg");
 /*_.TUP__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.F__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.F__V3*/ meltfptr[2])) != NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {

    MELT_LOCATION ("warmelt-base.melt:1581:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#IS_MULTIPLE__L1*/ meltfnum[0] =
      (melt_magic_discr ((melt_ptr_t) ( /*_.TUP__V2*/ meltfptr[1])) ==
       MELTOBMAG_MULTIPLE);;
    MELT_LOCATION ("warmelt-base.melt:1581:/ cond");
    /*cond */ if ( /*_#IS_MULTIPLE__L1*/ meltfnum[0])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-base.melt:1582:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
   /*_#IS_CLOSURE__L2*/ meltfnum[1] =
	    (melt_magic_discr ((melt_ptr_t) ( /*_.F__V3*/ meltfptr[2])) ==
	     MELTOBMAG_CLOSURE);;
	  MELT_LOCATION ("warmelt-base.melt:1582:/ cond");
	  /*cond */ if ( /*_#IS_CLOSURE__L2*/ meltfnum[1])	/*then */
	    {
	      /*^cond.then */
	      /*^block */
	      /*anyblock */
	      {

		/*citerblock FOREACH_IN_MULTIPLE */
		{
		  /* start foreach_in_multiple meltcit1__EACHTUP */
		  long meltcit1__EACHTUP_ln =
		    melt_multiple_length ((melt_ptr_t) /*_.TUP__V2*/
					  meltfptr[1]);
		  for ( /*_#IX__L3*/ meltfnum[2] = 0;
		       ( /*_#IX__L3*/ meltfnum[2] >= 0)
		       && ( /*_#IX__L3*/ meltfnum[2] < meltcit1__EACHTUP_ln);
	/*_#IX__L3*/ meltfnum[2]++)
		    {
		      /*_.COMP__V4*/ meltfptr[3] =
			melt_multiple_nth ((melt_ptr_t)
					   ( /*_.TUP__V2*/ meltfptr[1]),
					   /*_#IX__L3*/ meltfnum[2]);



		      MELT_LOCATION ("warmelt-base.melt:1586:/ checksignal");
		      MELT_CHECK_SIGNAL ();
		      ;
		      /*^apply */
		      /*apply */
		      {
			union meltparam_un argtab[1];
			memset (&argtab, 0, sizeof (argtab));
			/*^apply.arg */
			argtab[0].meltbp_long = /*_#IX__L3*/ meltfnum[2];
			/*_.F__V5*/ meltfptr[4] =
			  melt_apply ((meltclosure_ptr_t)
				      ( /*_.F__V3*/ meltfptr[2]),
				      (melt_ptr_t) ( /*_.COMP__V4*/
						    meltfptr[3]),
				      (MELTBPARSTR_LONG ""), argtab, "",
				      (union meltparam_un *) 0);
		      }
		      ;
		      if ( /*_#IX__L3*/ meltfnum[2] < 0)
			break;
		    }		/* end  foreach_in_multiple meltcit1__EACHTUP */

		  /*citerepilog */

		  MELT_LOCATION ("warmelt-base.melt:1583:/ clear");
		/*clear *//*_.COMP__V4*/ meltfptr[3] = 0;
		  /*^clear */
		/*clear *//*_#IX__L3*/ meltfnum[2] = 0;
		  /*^clear */
		/*clear *//*_.F__V5*/ meltfptr[4] = 0;
		}		/*endciterblock FOREACH_IN_MULTIPLE */
		;
		/*epilog */
	      }
	      ;
	    }			/*noelse */
	  ;
	  /*epilog */

	  MELT_LOCATION ("warmelt-base.melt:1581:/ clear");
	     /*clear *//*_#IS_CLOSURE__L2*/ meltfnum[1] = 0;
	}
	;
      }				/*noelse */
    ;
    /*epilog */

    MELT_LOCATION ("warmelt-base.melt:1578:/ clear");
	   /*clear *//*_#IS_MULTIPLE__L1*/ meltfnum[0] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("MULTIPLE_EVERY", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_19_warmelt_base_MULTIPLE_EVERY_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*noretval */ NULL);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_19_warmelt_base_MULTIPLE_EVERY */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_20_warmelt_base_MULTIPLE_BACKWARD_EVERY (meltclosure_ptr_t
						  meltclosp_,
						  melt_ptr_t meltfirstargp_,
						  const melt_argdescr_cell_t
						  meltxargdescr_[],
						  union meltparam_un *
						  meltxargtab_,
						  const melt_argdescr_cell_t
						  meltxresdescr_[],
						  union meltparam_un *
						  meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_20_warmelt_base_MULTIPLE_BACKWARD_EVERY_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_20_warmelt_base_MULTIPLE_BACKWARD_EVERY_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 5
    melt_ptr_t mcfr_varptr[5];
#define MELTFRAM_NBVARNUM 3
    long mcfr_varnum[3];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_20_warmelt_base_MULTIPLE_BACKWARD_EVERY is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_20_warmelt_base_MULTIPLE_BACKWARD_EVERY_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 5; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_20_warmelt_base_MULTIPLE_BACKWARD_EVERY nbval 5*/
  meltfram__.mcfr_nbvar = 5 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("MULTIPLE_BACKWARD_EVERY", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-base.melt:1589:/ getarg");
 /*_.TUP__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.F__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.F__V3*/ meltfptr[2])) != NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {

    MELT_LOCATION ("warmelt-base.melt:1592:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#IS_MULTIPLE__L1*/ meltfnum[0] =
      (melt_magic_discr ((melt_ptr_t) ( /*_.TUP__V2*/ meltfptr[1])) ==
       MELTOBMAG_MULTIPLE);;
    MELT_LOCATION ("warmelt-base.melt:1592:/ cond");
    /*cond */ if ( /*_#IS_MULTIPLE__L1*/ meltfnum[0])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-base.melt:1593:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
   /*_#IS_CLOSURE__L2*/ meltfnum[1] =
	    (melt_magic_discr ((melt_ptr_t) ( /*_.F__V3*/ meltfptr[2])) ==
	     MELTOBMAG_CLOSURE);;
	  MELT_LOCATION ("warmelt-base.melt:1593:/ cond");
	  /*cond */ if ( /*_#IS_CLOSURE__L2*/ meltfnum[1])	/*then */
	    {
	      /*^cond.then */
	      /*^block */
	      /*anyblock */
	      {

		/*citerblock FOREACH_IN_MULTIPLE_BACKWARD */
		{
		  /* start foreach_in_multiple_backward meltcit1__EACHTUPBACK */
		  long meltcit1__EACHTUPBACK_ln =
		    melt_multiple_length ((melt_ptr_t) /*_.TUP__V2*/
					  meltfptr[1]);
		  long meltcit1__EACHTUPBACK_ix = 0;
		  for (meltcit1__EACHTUPBACK_ix =
		       meltcit1__EACHTUPBACK_ln - 1;
		       meltcit1__EACHTUPBACK_ix >= 0;
		       meltcit1__EACHTUPBACK_ix--)
		    {
		      /*_.COMP__V4*/ meltfptr[3] =
			melt_multiple_nth ((melt_ptr_t)
					   ( /*_.TUP__V2*/ meltfptr[1]),
					   meltcit1__EACHTUPBACK_ix);
   /*_#IX__L3*/ meltfnum[2] = meltcit1__EACHTUPBACK_ix;


		      MELT_LOCATION ("warmelt-base.melt:1597:/ checksignal");
		      MELT_CHECK_SIGNAL ();
		      ;
		      /*^apply */
		      /*apply */
		      {
			union meltparam_un argtab[1];
			memset (&argtab, 0, sizeof (argtab));
			/*^apply.arg */
			argtab[0].meltbp_long = /*_#IX__L3*/ meltfnum[2];
			/*_.F__V5*/ meltfptr[4] =
			  melt_apply ((meltclosure_ptr_t)
				      ( /*_.F__V3*/ meltfptr[2]),
				      (melt_ptr_t) ( /*_.COMP__V4*/
						    meltfptr[3]),
				      (MELTBPARSTR_LONG ""), argtab, "",
				      (union meltparam_un *) 0);
		      }
		      ;
		    }		/* end  foreach_in_multiple_backward meltcit1__EACHTUPBACK */

		  /*citerepilog */

		  MELT_LOCATION ("warmelt-base.melt:1594:/ clear");
		/*clear *//*_.COMP__V4*/ meltfptr[3] = 0;
		  /*^clear */
		/*clear *//*_#IX__L3*/ meltfnum[2] = 0;
		  /*^clear */
		/*clear *//*_.F__V5*/ meltfptr[4] = 0;
		}		/*endciterblock FOREACH_IN_MULTIPLE_BACKWARD */
		;
		/*epilog */
	      }
	      ;
	    }			/*noelse */
	  ;
	  /*epilog */

	  MELT_LOCATION ("warmelt-base.melt:1592:/ clear");
	     /*clear *//*_#IS_CLOSURE__L2*/ meltfnum[1] = 0;
	}
	;
      }				/*noelse */
    ;
    /*epilog */

    MELT_LOCATION ("warmelt-base.melt:1589:/ clear");
	   /*clear *//*_#IS_MULTIPLE__L1*/ meltfnum[0] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("MULTIPLE_BACKWARD_EVERY", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_20_warmelt_base_MULTIPLE_BACKWARD_EVERY_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*noretval */ NULL);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_20_warmelt_base_MULTIPLE_BACKWARD_EVERY */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_21_warmelt_base_MULTIPLE_EVERY_BOTH (meltclosure_ptr_t meltclosp_,
					      melt_ptr_t meltfirstargp_,
					      const melt_argdescr_cell_t
					      meltxargdescr_[],
					      union meltparam_un *
					      meltxargtab_,
					      const melt_argdescr_cell_t
					      meltxresdescr_[],
					      union meltparam_un *
					      meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_21_warmelt_base_MULTIPLE_EVERY_BOTH_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_21_warmelt_base_MULTIPLE_EVERY_BOTH_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 13
    melt_ptr_t mcfr_varptr[13];
#define MELTFRAM_NBVARNUM 10
    long mcfr_varnum[10];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_21_warmelt_base_MULTIPLE_EVERY_BOTH is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_21_warmelt_base_MULTIPLE_EVERY_BOTH_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 13; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_21_warmelt_base_MULTIPLE_EVERY_BOTH nbval 13*/
  meltfram__.mcfr_nbvar = 13 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("MULTIPLE_EVERY_BOTH", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-base.melt:1601:/ getarg");
 /*_.TUP1__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.TUP2__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.TUP2__V3*/ meltfptr[2])) !=
	      NULL);


  /*getarg#2 */
  /*^getarg */
  if (meltxargdescr_[1] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.F__V4*/ meltfptr[3] =
    (meltxargtab_[1].meltbp_aptr) ? (*(meltxargtab_[1].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.F__V4*/ meltfptr[3])) != NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {

    MELT_LOCATION ("warmelt-base.melt:1604:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#IS_MULTIPLE__L1*/ meltfnum[0] =
      (melt_magic_discr ((melt_ptr_t) ( /*_.TUP1__V2*/ meltfptr[1])) ==
       MELTOBMAG_MULTIPLE);;
    MELT_LOCATION ("warmelt-base.melt:1604:/ cond");
    /*cond */ if ( /*_#IS_MULTIPLE__L1*/ meltfnum[0])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-base.melt:1605:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
   /*_#IS_MULTIPLE__L2*/ meltfnum[1] =
	    (melt_magic_discr ((melt_ptr_t) ( /*_.TUP2__V3*/ meltfptr[2])) ==
	     MELTOBMAG_MULTIPLE);;
	  MELT_LOCATION ("warmelt-base.melt:1605:/ cond");
	  /*cond */ if ( /*_#IS_MULTIPLE__L2*/ meltfnum[1])	/*then */
	    {
	      /*^cond.then */
	      /*^block */
	      /*anyblock */
	      {

		MELT_LOCATION ("warmelt-base.melt:1606:/ checksignal");
		MELT_CHECK_SIGNAL ();
		;
     /*_#IS_CLOSURE__L3*/ meltfnum[2] =
		  (melt_magic_discr ((melt_ptr_t) ( /*_.F__V4*/ meltfptr[3]))
		   == MELTOBMAG_CLOSURE);;
		MELT_LOCATION ("warmelt-base.melt:1606:/ cond");
		/*cond */ if ( /*_#IS_CLOSURE__L3*/ meltfnum[2])	/*then */
		  {
		    /*^cond.then */
		    /*^block */
		    /*anyblock */
		    {

		      MELT_LOCATION ("warmelt-base.melt:1607:/ quasiblock");


       /*_#LN1__L4*/ meltfnum[3] =
			(melt_multiple_length
			 ((melt_ptr_t) ( /*_.TUP1__V2*/ meltfptr[1])));;
		      /*^compute */
       /*_#LN2__L5*/ meltfnum[4] =
			(melt_multiple_length
			 ((melt_ptr_t) ( /*_.TUP2__V3*/ meltfptr[2])));;
		      /*^compute */
       /*_#IX__L6*/ meltfnum[5] = 0;;
		      MELT_LOCATION ("warmelt-base.melt:1610:/ loop");
		      /*loop */
		      {
		      labloop_TUPLOOP_1:;
				/*^loopbody */

			/*^block */
			/*anyblock */
			{

			  /*^checksignal */
			  MELT_CHECK_SIGNAL ();
			  ;
			  MELT_LOCATION
			    ("warmelt-base.melt:1611:/ checksignal");
			  MELT_CHECK_SIGNAL ();
			  ;
	 /*_#I__L7*/ meltfnum[6] =
			    (( /*_#IX__L6*/ meltfnum[5]) >=
			     ( /*_#LN1__L4*/ meltfnum[3]));;
			  MELT_LOCATION ("warmelt-base.melt:1611:/ cond");
			  /*cond */ if ( /*_#I__L7*/ meltfnum[6])	/*then */
			    {
			      /*^cond.then */
			      /*^block */
			      /*anyblock */
			      {

				/*^quasiblock */


				/*^compute */
	   /*_.TUPLOOP__V10*/ meltfptr[9] = NULL;;

				/*^exit */
				/*exit */
				{
				  goto labexit_TUPLOOP_1;
				}
				;
				/*epilog */
			      }
			      ;
			    }	/*noelse */
			  ;
			  MELT_LOCATION
			    ("warmelt-base.melt:1612:/ checksignal");
			  MELT_CHECK_SIGNAL ();
			  ;
	 /*_#I__L8*/ meltfnum[7] =
			    (( /*_#IX__L6*/ meltfnum[5]) >=
			     ( /*_#LN2__L5*/ meltfnum[4]));;
			  MELT_LOCATION ("warmelt-base.melt:1612:/ cond");
			  /*cond */ if ( /*_#I__L8*/ meltfnum[7])	/*then */
			    {
			      /*^cond.then */
			      /*^block */
			      /*anyblock */
			      {

				/*^quasiblock */


				/*^compute */
	   /*_.TUPLOOP__V10*/ meltfptr[9] = NULL;;

				/*^exit */
				/*exit */
				{
				  goto labexit_TUPLOOP_1;
				}
				;
				/*epilog */
			      }
			      ;
			    }	/*noelse */
			  ;
	 /*_.MULTIPLE_NTH__V11*/ meltfptr[10] =
			    (melt_multiple_nth
			     ((melt_ptr_t) ( /*_.TUP1__V2*/ meltfptr[1]),
			      ( /*_#IX__L6*/ meltfnum[5])));;
			  /*^compute */
	 /*_.MULTIPLE_NTH__V12*/ meltfptr[11] =
			    (melt_multiple_nth
			     ((melt_ptr_t) ( /*_.TUP2__V3*/ meltfptr[2]),
			      ( /*_#IX__L6*/ meltfnum[5])));;
			  MELT_LOCATION
			    ("warmelt-base.melt:1613:/ checksignal");
			  MELT_CHECK_SIGNAL ();
			  ;
			  /*^apply */
			  /*apply */
			  {
			    union meltparam_un argtab[2];
			    memset (&argtab, 0, sizeof (argtab));
			    /*^apply.arg */
			    argtab[0].meltbp_aptr =
			      (melt_ptr_t *) & /*_.MULTIPLE_NTH__V12*/
			      meltfptr[11];
			    /*^apply.arg */
			    argtab[1].meltbp_long = /*_#IX__L6*/ meltfnum[5];
			    /*_.F__V13*/ meltfptr[12] =
			      melt_apply ((meltclosure_ptr_t)
					  ( /*_.F__V4*/ meltfptr[3]),
					  (melt_ptr_t) ( /*_.MULTIPLE_NTH__V11*/ meltfptr[10]), (MELTBPARSTR_PTR MELTBPARSTR_LONG ""), argtab, "", (union meltparam_un *) 0);
			  }
			  ;
	 /*_#I__L9*/ meltfnum[8] =
			    (( /*_#IX__L6*/ meltfnum[5]) + (1));;
			  MELT_LOCATION ("warmelt-base.melt:1614:/ compute");
			  /*_#IX__L6*/ meltfnum[5] =
			    /*_#SETQ___L10*/ meltfnum[9] =
			    /*_#I__L9*/ meltfnum[8];;
			  MELT_LOCATION
			    ("warmelt-base.melt:1610:/ checksignal");
			  MELT_CHECK_SIGNAL ();
			  ;
			  /*epilog */

			  /*^clear */
		   /*clear *//*_#I__L7*/ meltfnum[6] = 0;
			  /*^clear */
		   /*clear *//*_#I__L8*/ meltfnum[7] = 0;
			  /*^clear */
		   /*clear *//*_.MULTIPLE_NTH__V11*/ meltfptr[10] = 0;
			  /*^clear */
		   /*clear *//*_.MULTIPLE_NTH__V12*/ meltfptr[11] = 0;
			  /*^clear */
		   /*clear *//*_.F__V13*/ meltfptr[12] = 0;
			  /*^clear */
		   /*clear *//*_#I__L9*/ meltfnum[8] = 0;
			  /*^clear */
		   /*clear *//*_#SETQ___L10*/ meltfnum[9] = 0;
			}
			;
			;
			goto labloop_TUPLOOP_1;
		      labexit_TUPLOOP_1:;
				/*^loopepilog */
			/*loopepilog */
			/*_.FOREVER___V9*/ meltfptr[8] =
			  /*_.TUPLOOP__V10*/ meltfptr[9];;
		      }
		      ;
		      /*^compute */
		      /*_.LET___V8*/ meltfptr[7] =
			/*_.FOREVER___V9*/ meltfptr[8];;

		      MELT_LOCATION ("warmelt-base.melt:1607:/ clear");
		 /*clear *//*_#LN1__L4*/ meltfnum[3] = 0;
		      /*^clear */
		 /*clear *//*_#LN2__L5*/ meltfnum[4] = 0;
		      /*^clear */
		 /*clear *//*_#IX__L6*/ meltfnum[5] = 0;
		      /*^clear */
		 /*clear *//*_.FOREVER___V9*/ meltfptr[8] = 0;
		      /*_.IF___V7*/ meltfptr[6] = /*_.LET___V8*/ meltfptr[7];;
		      /*epilog */

		      MELT_LOCATION ("warmelt-base.melt:1606:/ clear");
		 /*clear *//*_.LET___V8*/ meltfptr[7] = 0;
		    }
		    ;
		  }
		else
		  {		/*^cond.else */

      /*_.IF___V7*/ meltfptr[6] = NULL;;
		  }
		;
		/*^compute */
		/*_.IF___V6*/ meltfptr[5] = /*_.IF___V7*/ meltfptr[6];;
		/*epilog */

		MELT_LOCATION ("warmelt-base.melt:1605:/ clear");
	       /*clear *//*_#IS_CLOSURE__L3*/ meltfnum[2] = 0;
		/*^clear */
	       /*clear *//*_.IF___V7*/ meltfptr[6] = 0;
	      }
	      ;
	    }
	  else
	    {			/*^cond.else */

    /*_.IF___V6*/ meltfptr[5] = NULL;;
	    }
	  ;
	  /*^compute */
	  /*_.IF___V5*/ meltfptr[4] = /*_.IF___V6*/ meltfptr[5];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-base.melt:1604:/ clear");
	     /*clear *//*_#IS_MULTIPLE__L2*/ meltfnum[1] = 0;
	  /*^clear */
	     /*clear *//*_.IF___V6*/ meltfptr[5] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

  /*_.IF___V5*/ meltfptr[4] = NULL;;
      }
    ;
    MELT_LOCATION ("warmelt-base.melt:1601:/ quasiblock");


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.IF___V5*/ meltfptr[4];;

    {
      MELT_LOCATION ("warmelt-base.melt:1601:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_#IS_MULTIPLE__L1*/ meltfnum[0] = 0;
    /*^clear */
	   /*clear *//*_.IF___V5*/ meltfptr[4] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("MULTIPLE_EVERY_BOTH", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_21_warmelt_base_MULTIPLE_EVERY_BOTH_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_21_warmelt_base_MULTIPLE_EVERY_BOTH */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_22_warmelt_base_RUN_ALL_PASS_EXECUTION_HOOKS (meltclosure_ptr_t
						       meltclosp_,
						       melt_ptr_t
						       meltfirstargp_,
						       const
						       melt_argdescr_cell_t
						       meltxargdescr_[],
						       union meltparam_un *
						       meltxargtab_,
						       const
						       melt_argdescr_cell_t
						       meltxresdescr_[],
						       union meltparam_un *
						       meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_22_warmelt_base_RUN_ALL_PASS_EXECUTION_HOOKS_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_22_warmelt_base_RUN_ALL_PASS_EXECUTION_HOOKS_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 7
    melt_ptr_t mcfr_varptr[7];
#define MELTFRAM_NBVARNUM 1
    long mcfr_varnum[1];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_22_warmelt_base_RUN_ALL_PASS_EXECUTION_HOOKS is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_22_warmelt_base_RUN_ALL_PASS_EXECUTION_HOOKS_st
	 *) meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 7; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_22_warmelt_base_RUN_ALL_PASS_EXECUTION_HOOKS nbval 7*/
  meltfram__.mcfr_nbvar = 7 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("RUN_ALL_PASS_EXECUTION_HOOKS", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-base.melt:1640:/ getarg");
 /*_.HOOKLIST__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.PASSNAME__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.PASSNAME__V3*/ meltfptr[2])) !=
	      NULL);


  /*getarg#2 */
  /*^getarg */
  if (meltxargdescr_[1] != MELTBPAR_LONG)
    goto lab_endgetargs;
 /*_#PASSNUM__L1*/ meltfnum[0] = meltxargtab_[1].meltbp_long;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {

    /*citerblock FOREACH_IN_LIST */
    {
      /* start foreach_in_list meltcit1__EACHLIST */
      for ( /*_.CURPAIR__V4*/ meltfptr[3] =
	   melt_list_first ((melt_ptr_t) /*_.HOOKLIST__V2*/ meltfptr[1]);
	   melt_magic_discr ((melt_ptr_t) /*_.CURPAIR__V4*/ meltfptr[3]) ==
	   MELTOBMAG_PAIR;
	   /*_.CURPAIR__V4*/ meltfptr[3] =
	   melt_pair_tail ((melt_ptr_t) /*_.CURPAIR__V4*/ meltfptr[3]))
	{
	  /*_.CURHOOK__V5*/ meltfptr[4] =
	    melt_pair_head ((melt_ptr_t) /*_.CURPAIR__V4*/ meltfptr[3]);


	  MELT_LOCATION ("warmelt-base.melt:1645:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^apply */
	  /*apply */
	  {
	    union meltparam_un argtab[1];
	    memset (&argtab, 0, sizeof (argtab));
	    /*^apply.arg */
	    argtab[0].meltbp_long = /*_#PASSNUM__L1*/ meltfnum[0];
	    /*_.CURHOOK__V6*/ meltfptr[5] =
	      melt_apply ((meltclosure_ptr_t)
			  ( /*_.CURHOOK__V5*/ meltfptr[4]),
			  (melt_ptr_t) ( /*_.PASSNAME__V3*/ meltfptr[2]),
			  (MELTBPARSTR_LONG ""), argtab, "",
			  (union meltparam_un *) 0);
	  }
	  ;
	}			/* end foreach_in_list meltcit1__EACHLIST */
     /*_.CURPAIR__V4*/ meltfptr[3] = NULL;
     /*_.CURHOOK__V5*/ meltfptr[4] = NULL;


      /*citerepilog */

      MELT_LOCATION ("warmelt-base.melt:1642:/ clear");
	    /*clear *//*_.CURPAIR__V4*/ meltfptr[3] = 0;
      /*^clear */
	    /*clear *//*_.CURHOOK__V5*/ meltfptr[4] = 0;
      /*^clear */
	    /*clear *//*_.CURHOOK__V6*/ meltfptr[5] = 0;
    }				/*endciterblock FOREACH_IN_LIST */
    ;
    MELT_LOCATION ("warmelt-base.melt:1647:/ quasiblock");


 /*_.RETVAL___V1*/ meltfptr[0] = NULL;;

    {
      MELT_LOCATION ("warmelt-base.melt:1647:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    MELT_LOCATION ("warmelt-base.melt:1640:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.RETURN___V7*/ meltfptr[6];;

    {
      MELT_LOCATION ("warmelt-base.melt:1640:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.RETURN___V7*/ meltfptr[6] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("RUN_ALL_PASS_EXECUTION_HOOKS", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_22_warmelt_base_RUN_ALL_PASS_EXECUTION_HOOKS_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_22_warmelt_base_RUN_ALL_PASS_EXECUTION_HOOKS */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_23_warmelt_base_REGISTER_PASS_EXECUTION_HOOK (meltclosure_ptr_t
						       meltclosp_,
						       melt_ptr_t
						       meltfirstargp_,
						       const
						       melt_argdescr_cell_t
						       meltxargdescr_[],
						       union meltparam_un *
						       meltxargtab_,
						       const
						       melt_argdescr_cell_t
						       meltxresdescr_[],
						       union meltparam_un *
						       meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_23_warmelt_base_REGISTER_PASS_EXECUTION_HOOK_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_23_warmelt_base_REGISTER_PASS_EXECUTION_HOOK_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 17
    melt_ptr_t mcfr_varptr[17];
#define MELTFRAM_NBVARNUM 4
    long mcfr_varnum[4];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_23_warmelt_base_REGISTER_PASS_EXECUTION_HOOK is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_23_warmelt_base_REGISTER_PASS_EXECUTION_HOOK_st
	 *) meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 17; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_23_warmelt_base_REGISTER_PASS_EXECUTION_HOOK nbval 17*/
  meltfram__.mcfr_nbvar = 17 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("REGISTER_PASS_EXECUTION_HOOK", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-base.melt:1651:/ getarg");
 /*_.FUN__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {

    MELT_LOCATION ("warmelt-base.melt:1657:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#IS_CLOSURE__L1*/ meltfnum[0] =
      (melt_magic_discr ((melt_ptr_t) ( /*_.FUN__V2*/ meltfptr[1])) ==
       MELTOBMAG_CLOSURE);;
    MELT_LOCATION ("warmelt-base.melt:1657:/ cond");
    /*cond */ if ( /*_#IS_CLOSURE__L1*/ meltfnum[0])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-base.melt:1658:/ quasiblock");


	  /*^getslot */
	  {
	    melt_ptr_t slot = NULL, obj = NULL;
	    obj =
	      (melt_ptr_t) (( /*!PASS_EXECUTION_REFERENCE */ meltfrout->
			     tabval[0])) /*=obj*/ ;
	    melt_object_get_field (slot, obj, 0, "REFERENCED_VALUE");
    /*_.HOOKLIST__V5*/ meltfptr[4] = slot;
	  };
	  ;
	  MELT_LOCATION ("warmelt-base.melt:1661:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
   /*_#NULL__L2*/ meltfnum[1] =
	    (( /*_.HOOKLIST__V5*/ meltfptr[4]) == NULL);;
	  MELT_LOCATION ("warmelt-base.melt:1661:/ cond");
	  /*cond */ if ( /*_#NULL__L2*/ meltfnum[1])	/*then */
	    {
	      /*^cond.then */
	      /*^block */
	      /*anyblock */
	      {

		MELT_LOCATION ("warmelt-base.melt:1663:/ blockmultialloc");
		/*multiallocblock */
		{
		  struct meltletrec_1_st
		  {
		    struct meltlist_st rlist_0__LIST_;
		    long meltletrec_1_endgap;
		  } *meltletrec_1_ptr = 0;
		  meltletrec_1_ptr =
		    (struct meltletrec_1_st *)
		    meltgc_allocate (sizeof (struct meltletrec_1_st), 0);
		  /*^blockmultialloc.initfill */
		  /*inilist rlist_0__LIST_ */
       /*_.LIST___V7*/ meltfptr[6] =
		    (melt_ptr_t) & meltletrec_1_ptr->rlist_0__LIST_;
		  meltletrec_1_ptr->rlist_0__LIST_.discr =
		    (meltobject_ptr_t) (((melt_ptr_t)
					 (MELT_PREDEF (DISCR_LIST))));



		  /*_.LIST___V6*/ meltfptr[5] = /*_.LIST___V7*/ meltfptr[6];;
		  /*epilog */

		  MELT_LOCATION ("warmelt-base.melt:1663:/ clear");
		/*clear *//*_.LIST___V7*/ meltfptr[6] = 0;
		  /*^clear */
		/*clear *//*_.LIST___V7*/ meltfptr[6] = 0;
		}		/*end multiallocblock */
		;
		/*^compute */
		/*_.HOOKLIST__V5*/ meltfptr[4] = /*_.SETQ___V8*/ meltfptr[6] =
		  /*_.LIST___V6*/ meltfptr[5];;
		MELT_LOCATION ("warmelt-base.melt:1664:/ checksignal");
		MELT_CHECK_SIGNAL ();
		;
		/*^cond */
		/*cond */ if (
			       /*ifisa */
			       melt_is_instance_of ((melt_ptr_t)
						    (( /*!PASS_EXECUTION_REFERENCE */ meltfrout->tabval[0])),
						    (melt_ptr_t) (( /*!CLASS_REFERENCE */ meltfrout->tabval[1])))
		  )		/*then */
		  {
		    /*^cond.then */
		    /*^block */
		    /*anyblock */
		    {

		      /*^putslot */
		      /*putslot */
		      melt_assertmsg ("putslot checkobj @REFERENCED_VALUE",
				      melt_magic_discr ((melt_ptr_t)
							(( /*!PASS_EXECUTION_REFERENCE */ meltfrout->tabval[0]))) == MELTOBMAG_OBJECT);
		      melt_putfield_object ((( /*!PASS_EXECUTION_REFERENCE */
					      meltfrout->tabval[0])), (0),
					    ( /*_.HOOKLIST__V5*/ meltfptr[4]),
					    "REFERENCED_VALUE");
		      ;
		      /*^touch */
		      meltgc_touch (( /*!PASS_EXECUTION_REFERENCE */
				     meltfrout->tabval[0]));
		      ;
		      /*^touchobj */

		      melt_dbgtrace_written_object (( /*!PASS_EXECUTION_REFERENCE */ meltfrout->tabval[0]), "put-fields");
		      ;
		      /*epilog */
		    }
		    ;
		  }		/*noelse */
		;
		MELT_LOCATION ("warmelt-base.melt:1662:/ quasiblock");


		/*epilog */

		MELT_LOCATION ("warmelt-base.melt:1661:/ clear");
	       /*clear *//*_.LIST___V6*/ meltfptr[5] = 0;
		/*^clear */
	       /*clear *//*_.SETQ___V8*/ meltfptr[6] = 0;
	      }
	      ;
	    }			/*noelse */
	  ;

#if MELT_HAVE_DEBUG
	  MELT_LOCATION ("warmelt-base.melt:1665:/ cppif.then");
	  /*^block */
	  /*anyblock */
	  {

	    /*^checksignal */
	    MELT_CHECK_SIGNAL ();
	    ;
     /*_#IS_LIST__L3*/ meltfnum[2] =
	      (melt_magic_discr
	       ((melt_ptr_t) ( /*_.HOOKLIST__V5*/ meltfptr[4])) ==
	       MELTOBMAG_LIST);;
	    MELT_LOCATION ("warmelt-base.melt:1665:/ cond");
	    /*cond */ if ( /*_#IS_LIST__L3*/ meltfnum[2])	/*then */
	      {
		/*^cond.then */
		/*_.IFELSE___V10*/ meltfptr[6] = ( /*nil */ NULL);;
	      }
	    else
	      {
		MELT_LOCATION ("warmelt-base.melt:1665:/ cond.else");

		/*^block */
		/*anyblock */
		{




		  {
		    /*^locexp */
		    melt_assert_failed (("check hooklist"),
					("warmelt-base.melt")
					? ("warmelt-base.melt") : __FILE__,
					(1665) ? (1665) : __LINE__,
					__FUNCTION__);
		    ;
		  }
		  ;
		 /*clear *//*_.IFELSE___V10*/ meltfptr[6] = 0;
		  /*epilog */
		}
		;
	      }
	    ;
	    /*^compute */
	    /*_.IFCPP___V9*/ meltfptr[5] = /*_.IFELSE___V10*/ meltfptr[6];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-base.melt:1665:/ clear");
	       /*clear *//*_#IS_LIST__L3*/ meltfnum[2] = 0;
	    /*^clear */
	       /*clear *//*_.IFELSE___V10*/ meltfptr[6] = 0;
	  }

#else /*MELT_HAVE_DEBUG */
	  /*^cppif.else */
	  /*_.IFCPP___V9*/ meltfptr[5] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
	  ;
	  /*citerblock FOREACH_IN_LIST */
	  {
	    /* start foreach_in_list meltcit1__EACHLIST */
	    for ( /*_.CURPAIR__V11*/ meltfptr[6] =
		 melt_list_first ((melt_ptr_t) /*_.HOOKLIST__V5*/
				  meltfptr[4]);
		 melt_magic_discr ((melt_ptr_t) /*_.CURPAIR__V11*/
				   meltfptr[6]) == MELTOBMAG_PAIR;
		 /*_.CURPAIR__V11*/ meltfptr[6] =
		 melt_pair_tail ((melt_ptr_t) /*_.CURPAIR__V11*/ meltfptr[6]))
	      {
		/*_.CURHOOK__V12*/ meltfptr[11] =
		  melt_pair_head ((melt_ptr_t) /*_.CURPAIR__V11*/
				  meltfptr[6]);


		MELT_LOCATION ("warmelt-base.melt:1670:/ checksignal");
		MELT_CHECK_SIGNAL ();
		;
    /*_#__L4*/ meltfnum[2] =
		  (( /*_.CURHOOK__V12*/ meltfptr[11]) ==
		   ( /*_.FUN__V2*/ meltfptr[1]));;
		MELT_LOCATION ("warmelt-base.melt:1670:/ cond");
		/*cond */ if ( /*_#__L4*/ meltfnum[2])	/*then */
		  {
		    /*^cond.then */
		    /*^block */
		    /*anyblock */
		    {

		      MELT_LOCATION ("warmelt-base.melt:1671:/ quasiblock");


      /*_.RETVAL___V1*/ meltfptr[0] = NULL;;

		      {
			MELT_LOCATION ("warmelt-base.melt:1671:/ locexp");
			/*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
			if (meltxresdescr_ && meltxresdescr_[0]
			    && meltxrestab_)
			  melt_warn_for_no_expected_secondary_results ();
			/* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
			;
		      }
		      ;
		      /*^finalreturn */
		      ;
		      /*finalret */ goto labend_rout;
		      /*_.IF___V13*/ meltfptr[12] =
			/*_.RETURN___V14*/ meltfptr[13];;
		      /*epilog */

		      MELT_LOCATION ("warmelt-base.melt:1670:/ clear");
		/*clear *//*_.RETURN___V14*/ meltfptr[13] = 0;
		    }
		    ;
		  }
		else
		  {		/*^cond.else */

     /*_.IF___V13*/ meltfptr[12] = NULL;;
		  }
		;
	      }			/* end foreach_in_list meltcit1__EACHLIST */
     /*_.CURPAIR__V11*/ meltfptr[6] = NULL;
     /*_.CURHOOK__V12*/ meltfptr[11] = NULL;


	    /*citerepilog */

	    MELT_LOCATION ("warmelt-base.melt:1667:/ clear");
	      /*clear *//*_.CURPAIR__V11*/ meltfptr[6] = 0;
	    /*^clear */
	      /*clear *//*_.CURHOOK__V12*/ meltfptr[11] = 0;
	    /*^clear */
	      /*clear *//*_#__L4*/ meltfnum[2] = 0;
	    /*^clear */
	      /*clear *//*_.IF___V13*/ meltfptr[12] = 0;
	  }			/*endciterblock FOREACH_IN_LIST */
	  ;

	  {
	    MELT_LOCATION ("warmelt-base.melt:1673:/ locexp");
	    meltgc_append_list ((melt_ptr_t)
				( /*_.HOOKLIST__V5*/ meltfptr[4]),
				(melt_ptr_t) ( /*_.FUN__V2*/ meltfptr[1]));
	  }
	  ;
	  MELT_LOCATION ("warmelt-base.melt:1674:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  MELT_LOCATION ("warmelt-base.melt:1676:/ quasiblock");


	  /*^newclosure */
		   /*newclosure *//*_.LAMBDA___V16*/ meltfptr[15] =
	    (melt_ptr_t)
	    meltgc_new_closure ((meltobject_ptr_t)
				(((melt_ptr_t)
				  (MELT_PREDEF (DISCR_CLOSURE)))),
				(meltroutine_ptr_t) (( /*!konst_4 */
						      meltfrout->tabval[4])),
				(1));
	  ;
	  /*^putclosedv */
	  /*putclosv */
	  melt_assertmsg ("putclosv checkclo",
			  melt_magic_discr ((melt_ptr_t)
					    ( /*_.LAMBDA___V16*/
					     meltfptr[15])) ==
			  MELTOBMAG_CLOSURE);
	  melt_assertmsg ("putclosv checkoff", 0 >= 0
			  && 0 <
			  melt_closure_size ((melt_ptr_t)
					     ( /*_.LAMBDA___V16*/
					      meltfptr[15])));
	  ((meltclosure_ptr_t) /*_.LAMBDA___V16*/ meltfptr[15])->tabval[0] =
	    (melt_ptr_t) ( /*_.HOOKLIST__V5*/ meltfptr[4]);
	  ;
	  /*_.LAMBDA___V15*/ meltfptr[13] = /*_.LAMBDA___V16*/ meltfptr[15];;
	  MELT_LOCATION ("warmelt-base.melt:1674:/ cond");
	  /*cond */ if (
			 /*ifisa */
			 melt_is_instance_of ((melt_ptr_t)
					      (( /*!INITIAL_SYSTEM_DATA */
						meltfrout->tabval[2])),
					      (melt_ptr_t) (( /*!CLASS_SYSTEM_DATA */ meltfrout->tabval[5])))
	    )			/*then */
	    {
	      /*^cond.then */
	      /*^block */
	      /*anyblock */
	      {

		/*^putslot */
		/*putslot */
		melt_assertmsg ("putslot checkobj @SYSDATA_PASSEXEC_HOOK",
				melt_magic_discr ((melt_ptr_t)
						  (( /*!INITIAL_SYSTEM_DATA */
						    meltfrout->tabval[2]))) ==
				MELTOBMAG_OBJECT);
		melt_putfield_object ((( /*!INITIAL_SYSTEM_DATA */ meltfrout->
					tabval[2])), (25),
				      ( /*_.LAMBDA___V15*/ meltfptr[13]),
				      "SYSDATA_PASSEXEC_HOOK");
		;
		/*^touch */
		meltgc_touch (( /*!INITIAL_SYSTEM_DATA */ meltfrout->
			       tabval[2]));
		;
		/*^touchobj */

		melt_dbgtrace_written_object (( /*!INITIAL_SYSTEM_DATA */
					       meltfrout->tabval[2]),
					      "put-fields");
		;
		/*epilog */
	      }
	      ;
	    }			/*noelse */
	  ;
	  MELT_LOCATION ("warmelt-base.melt:1678:/ quasiblock");


   /*_.RETVAL___V1*/ meltfptr[0] = NULL;;

	  {
	    MELT_LOCATION ("warmelt-base.melt:1678:/ locexp");
	    /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
	    if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	      melt_warn_for_no_expected_secondary_results ();
	    /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
	    ;
	  }
	  ;
	  /*^finalreturn */
	  ;
	  /*finalret */ goto labend_rout;
	  /*_.LET___V4*/ meltfptr[3] = /*_.RETURN___V17*/ meltfptr[16];;

	  MELT_LOCATION ("warmelt-base.melt:1658:/ clear");
	     /*clear *//*_.HOOKLIST__V5*/ meltfptr[4] = 0;
	  /*^clear */
	     /*clear *//*_#NULL__L2*/ meltfnum[1] = 0;
	  /*^clear */
	     /*clear *//*_.IFCPP___V9*/ meltfptr[5] = 0;
	  /*^clear */
	     /*clear *//*_.LAMBDA___V15*/ meltfptr[13] = 0;
	  /*^clear */
	     /*clear *//*_.RETURN___V17*/ meltfptr[16] = 0;
	  /*_.IF___V3*/ meltfptr[2] = /*_.LET___V4*/ meltfptr[3];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-base.melt:1657:/ clear");
	     /*clear *//*_.LET___V4*/ meltfptr[3] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

  /*_.IF___V3*/ meltfptr[2] = NULL;;
      }
    ;
    MELT_LOCATION ("warmelt-base.melt:1651:/ quasiblock");


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.IF___V3*/ meltfptr[2];;

    {
      MELT_LOCATION ("warmelt-base.melt:1651:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_#IS_CLOSURE__L1*/ meltfnum[0] = 0;
    /*^clear */
	   /*clear *//*_.IF___V3*/ meltfptr[2] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("REGISTER_PASS_EXECUTION_HOOK", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_23_warmelt_base_REGISTER_PASS_EXECUTION_HOOK_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_23_warmelt_base_REGISTER_PASS_EXECUTION_HOOK */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_24_warmelt_base_LAMBDA___1__ (meltclosure_ptr_t meltclosp_,
				       melt_ptr_t meltfirstargp_,
				       const melt_argdescr_cell_t
				       meltxargdescr_[],
				       union meltparam_un * meltxargtab_,
				       const melt_argdescr_cell_t
				       meltxresdescr_[],
				       union meltparam_un * meltxrestab_)
{
  long current_blocklevel_signals_meltrout_24_warmelt_base_LAMBDA___1___melt =
    melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_24_warmelt_base_LAMBDA___1___st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 3
    melt_ptr_t mcfr_varptr[3];
#define MELTFRAM_NBVARNUM 1
    long mcfr_varnum[1];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_24_warmelt_base_LAMBDA___1__ is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_24_warmelt_base_LAMBDA___1___st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 3; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_24_warmelt_base_LAMBDA___1__ nbval 3*/
  meltfram__.mcfr_nbvar = 3 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("LAMBDA_", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-base.melt:1676:/ getarg");
 /*_.PASSNAME__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_LONG)
    goto lab_endgetargs;
 /*_#PASSNUM__L1*/ meltfnum[0] = meltxargtab_[0].meltbp_long;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {

    MELT_LOCATION ("warmelt-base.melt:1677:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[2];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.PASSNAME__V2*/ meltfptr[1];
      /*^apply.arg */
      argtab[1].meltbp_long = /*_#PASSNUM__L1*/ meltfnum[0];
      /*_.RUN_ALL_PASS_EXECUTION_HOOKS__V3*/ meltfptr[2] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!RUN_ALL_PASS_EXECUTION_HOOKS */ meltfrout->
		      tabval[0])),
		    (melt_ptr_t) (( /*~HOOKLIST */ meltfclos->tabval[0])),
		    (MELTBPARSTR_PTR MELTBPARSTR_LONG ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-base.melt:1676:/ quasiblock");


    /*_.RETVAL___V1*/ meltfptr[0] =
      /*_.RUN_ALL_PASS_EXECUTION_HOOKS__V3*/ meltfptr[2];;

    {
      MELT_LOCATION ("warmelt-base.melt:1676:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.RUN_ALL_PASS_EXECUTION_HOOKS__V3*/ meltfptr[2] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("LAMBDA_", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_24_warmelt_base_LAMBDA___1___melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_24_warmelt_base_LAMBDA___1__ */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_25_warmelt_base_UNREGISTER_PASS_EXECUTION_HOOK (meltclosure_ptr_t
							 meltclosp_,
							 melt_ptr_t
							 meltfirstargp_,
							 const
							 melt_argdescr_cell_t
							 meltxargdescr_[],
							 union meltparam_un *
							 meltxargtab_,
							 const
							 melt_argdescr_cell_t
							 meltxresdescr_[],
							 union meltparam_un *
							 meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_25_warmelt_base_UNREGISTER_PASS_EXECUTION_HOOK_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_25_warmelt_base_UNREGISTER_PASS_EXECUTION_HOOK_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 9
    melt_ptr_t mcfr_varptr[9];
#define MELTFRAM_NBVARNUM 3
    long mcfr_varnum[3];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_25_warmelt_base_UNREGISTER_PASS_EXECUTION_HOOK is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct
	 frame_meltrout_25_warmelt_base_UNREGISTER_PASS_EXECUTION_HOOK_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 9; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_25_warmelt_base_UNREGISTER_PASS_EXECUTION_HOOK nbval 9*/
  meltfram__.mcfr_nbvar = 9 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("UNREGISTER_PASS_EXECUTION_HOOK", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-base.melt:1682:/ getarg");
 /*_.FUN__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {

    MELT_LOCATION ("warmelt-base.melt:1685:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#IS_CLOSURE__L1*/ meltfnum[0] =
      (melt_magic_discr ((melt_ptr_t) ( /*_.FUN__V2*/ meltfptr[1])) ==
       MELTOBMAG_CLOSURE);;
    MELT_LOCATION ("warmelt-base.melt:1685:/ cond");
    /*cond */ if ( /*_#IS_CLOSURE__L1*/ meltfnum[0])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-base.melt:1686:/ quasiblock");


	  /*^getslot */
	  {
	    melt_ptr_t slot = NULL, obj = NULL;
	    obj =
	      (melt_ptr_t) (( /*!PASS_EXECUTION_REFERENCE */ meltfrout->
			     tabval[0])) /*=obj*/ ;
	    melt_object_get_field (slot, obj, 0, "REFERENCED_VALUE");
    /*_.OLDHOOKLIST__V3*/ meltfptr[2] = slot;
	  };
	  ;
	  MELT_LOCATION ("warmelt-base.melt:1688:/ blockmultialloc");
	  /*multiallocblock */
	  {
	    struct meltletrec_1_st
	    {
	      struct meltlist_st rlist_0__LIST_;
	      long meltletrec_1_endgap;
	    } *meltletrec_1_ptr = 0;
	    meltletrec_1_ptr =
	      (struct meltletrec_1_st *)
	      meltgc_allocate (sizeof (struct meltletrec_1_st), 0);
	    /*^blockmultialloc.initfill */
	    /*inilist rlist_0__LIST_ */
     /*_.LIST___V5*/ meltfptr[4] =
	      (melt_ptr_t) & meltletrec_1_ptr->rlist_0__LIST_;
	    meltletrec_1_ptr->rlist_0__LIST_.discr =
	      (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (DISCR_LIST))));



	    /*_.NEWHOOKLIST__V4*/ meltfptr[3] = /*_.LIST___V5*/ meltfptr[4];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-base.melt:1688:/ clear");
	      /*clear *//*_.LIST___V5*/ meltfptr[4] = 0;
	    /*^clear */
	      /*clear *//*_.LIST___V5*/ meltfptr[4] = 0;
	  }			/*end multiallocblock */
	  ;
	  /*citerblock FOREACH_IN_LIST */
	  {
	    /* start foreach_in_list meltcit1__EACHLIST */
	    for ( /*_.CURPAIR__V6*/ meltfptr[4] =
		 melt_list_first ((melt_ptr_t) /*_.OLDHOOKLIST__V3*/
				  meltfptr[2]);
		 melt_magic_discr ((melt_ptr_t) /*_.CURPAIR__V6*/ meltfptr[4])
		 == MELTOBMAG_PAIR;
		 /*_.CURPAIR__V6*/ meltfptr[4] =
		 melt_pair_tail ((melt_ptr_t) /*_.CURPAIR__V6*/ meltfptr[4]))
	      {
		/*_.CURHOOK__V7*/ meltfptr[6] =
		  melt_pair_head ((melt_ptr_t) /*_.CURPAIR__V6*/ meltfptr[4]);


		MELT_LOCATION ("warmelt-base.melt:1693:/ checksignal");
		MELT_CHECK_SIGNAL ();
		;
    /*_#__L2*/ meltfnum[1] =
		  (( /*_.CURHOOK__V7*/ meltfptr[6]) !=
		   ( /*_.FUN__V2*/ meltfptr[1]));;
		MELT_LOCATION ("warmelt-base.melt:1693:/ cond");
		/*cond */ if ( /*_#__L2*/ meltfnum[1])	/*then */
		  {
		    /*^cond.then */
		    /*^block */
		    /*anyblock */
		    {


		      {
			MELT_LOCATION ("warmelt-base.melt:1694:/ locexp");
			meltgc_append_list ((melt_ptr_t)
					    ( /*_.NEWHOOKLIST__V4*/
					     meltfptr[3]),
					    (melt_ptr_t) ( /*_.CURHOOK__V7*/
							  meltfptr[6]));
		      }
		      ;
		      /*epilog */
		    }
		    ;
		  }		/*noelse */
		;
	      }			/* end foreach_in_list meltcit1__EACHLIST */
     /*_.CURPAIR__V6*/ meltfptr[4] = NULL;
     /*_.CURHOOK__V7*/ meltfptr[6] = NULL;


	    /*citerepilog */

	    MELT_LOCATION ("warmelt-base.melt:1690:/ clear");
	      /*clear *//*_.CURPAIR__V6*/ meltfptr[4] = 0;
	    /*^clear */
	      /*clear *//*_.CURHOOK__V7*/ meltfptr[6] = 0;
	    /*^clear */
	      /*clear *//*_#__L2*/ meltfnum[1] = 0;
	  }			/*endciterblock FOREACH_IN_LIST */
	  ;
	  MELT_LOCATION ("warmelt-base.melt:1696:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
   /*_#IS_NON_EMPTY_LIST__L3*/ meltfnum[2] =
	    (melt_magic_discr
	     ((melt_ptr_t) ( /*_.NEWHOOKLIST__V4*/ meltfptr[3])) ==
	     MELTOBMAG_LIST
	     && NULL !=
	     melt_list_first ((melt_ptr_t)
			      ( /*_.NEWHOOKLIST__V4*/ meltfptr[3])));;
	  MELT_LOCATION ("warmelt-base.melt:1696:/ cond");
	  /*cond */ if ( /*_#IS_NON_EMPTY_LIST__L3*/ meltfnum[2])	/*then */
	    {
	      /*^cond.then */
	      /*^block */
	      /*anyblock */
	      {

		MELT_LOCATION ("warmelt-base.melt:1697:/ checksignal");
		MELT_CHECK_SIGNAL ();
		;
		/*^cond */
		/*cond */ if (
			       /*ifisa */
			       melt_is_instance_of ((melt_ptr_t)
						    (( /*!INITIAL_SYSTEM_DATA */ meltfrout->tabval[1])),
						    (melt_ptr_t) (( /*!CLASS_SYSTEM_DATA */ meltfrout->tabval[2])))
		  )		/*then */
		  {
		    /*^cond.then */
		    /*^block */
		    /*anyblock */
		    {

		      /*^putslot */
		      /*putslot */
		      melt_assertmsg
			("putslot checkobj @SYSDATA_PASSEXEC_HOOK",
			 melt_magic_discr ((melt_ptr_t)
					   (( /*!INITIAL_SYSTEM_DATA */
					     meltfrout->tabval[1]))) ==
			 MELTOBMAG_OBJECT);
		      melt_putfield_object ((( /*!INITIAL_SYSTEM_DATA */
					      meltfrout->tabval[1])), (25),
					    (( /*nil */ NULL)),
					    "SYSDATA_PASSEXEC_HOOK");
		      ;
		      /*^touch */
		      meltgc_touch (( /*!INITIAL_SYSTEM_DATA */ meltfrout->
				     tabval[1]));
		      ;
		      /*^touchobj */

		      melt_dbgtrace_written_object (( /*!INITIAL_SYSTEM_DATA */ meltfrout->tabval[1]), "put-fields");
		      ;
		      /*epilog */
		    }
		    ;
		  }		/*noelse */
		;
		/*epilog */
	      }
	      ;
	    }
	  else
	    {
	      MELT_LOCATION ("warmelt-base.melt:1696:/ cond.else");

	      /*^block */
	      /*anyblock */
	      {

		MELT_LOCATION ("warmelt-base.melt:1699:/ checksignal");
		MELT_CHECK_SIGNAL ();
		;
		MELT_LOCATION ("warmelt-base.melt:1701:/ quasiblock");


		/*^newclosure */
		     /*newclosure *//*_.LAMBDA___V9*/ meltfptr[8] =
		  (melt_ptr_t)
		  meltgc_new_closure ((meltobject_ptr_t)
				      (((melt_ptr_t)
					(MELT_PREDEF (DISCR_CLOSURE)))),
				      (meltroutine_ptr_t) (( /*!konst_4 */
							    meltfrout->
							    tabval[4])), (1));
		;
		/*^putclosedv */
		/*putclosv */
		melt_assertmsg ("putclosv checkclo",
				melt_magic_discr ((melt_ptr_t)
						  ( /*_.LAMBDA___V9*/
						   meltfptr[8])) ==
				MELTOBMAG_CLOSURE);
		melt_assertmsg ("putclosv checkoff", 0 >= 0
				&& 0 <
				melt_closure_size ((melt_ptr_t)
						   ( /*_.LAMBDA___V9*/
						    meltfptr[8])));
		((meltclosure_ptr_t) /*_.LAMBDA___V9*/ meltfptr[8])->
		  tabval[0] =
		  (melt_ptr_t) ( /*_.NEWHOOKLIST__V4*/ meltfptr[3]);
		;
		/*_.LAMBDA___V8*/ meltfptr[7] =
		  /*_.LAMBDA___V9*/ meltfptr[8];;
		MELT_LOCATION ("warmelt-base.melt:1699:/ cond");
		/*cond */ if (
			       /*ifisa */
			       melt_is_instance_of ((melt_ptr_t)
						    (( /*!INITIAL_SYSTEM_DATA */ meltfrout->tabval[1])),
						    (melt_ptr_t) (( /*!CLASS_SYSTEM_DATA */ meltfrout->tabval[2])))
		  )		/*then */
		  {
		    /*^cond.then */
		    /*^block */
		    /*anyblock */
		    {

		      /*^putslot */
		      /*putslot */
		      melt_assertmsg
			("putslot checkobj @SYSDATA_PASSEXEC_HOOK",
			 melt_magic_discr ((melt_ptr_t)
					   (( /*!INITIAL_SYSTEM_DATA */
					     meltfrout->tabval[1]))) ==
			 MELTOBMAG_OBJECT);
		      melt_putfield_object ((( /*!INITIAL_SYSTEM_DATA */
					      meltfrout->tabval[1])), (25),
					    ( /*_.LAMBDA___V8*/ meltfptr[7]),
					    "SYSDATA_PASSEXEC_HOOK");
		      ;
		      /*^touch */
		      meltgc_touch (( /*!INITIAL_SYSTEM_DATA */ meltfrout->
				     tabval[1]));
		      ;
		      /*^touchobj */

		      melt_dbgtrace_written_object (( /*!INITIAL_SYSTEM_DATA */ meltfrout->tabval[1]), "put-fields");
		      ;
		      /*epilog */
		    }
		    ;
		  }		/*noelse */
		;
		/*epilog */

		MELT_LOCATION ("warmelt-base.melt:1696:/ clear");
	       /*clear *//*_.LAMBDA___V8*/ meltfptr[7] = 0;
	      }
	      ;
	    }
	  ;

	  MELT_LOCATION ("warmelt-base.melt:1686:/ clear");
	     /*clear *//*_.OLDHOOKLIST__V3*/ meltfptr[2] = 0;
	  /*^clear */
	     /*clear *//*_.NEWHOOKLIST__V4*/ meltfptr[3] = 0;
	  /*^clear */
	     /*clear *//*_#IS_NON_EMPTY_LIST__L3*/ meltfnum[2] = 0;
	  /*epilog */
	}
	;
      }				/*noelse */
    ;
    /*epilog */

    MELT_LOCATION ("warmelt-base.melt:1682:/ clear");
	   /*clear *//*_#IS_CLOSURE__L1*/ meltfnum[0] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("UNREGISTER_PASS_EXECUTION_HOOK", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_25_warmelt_base_UNREGISTER_PASS_EXECUTION_HOOK_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*noretval */ NULL);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_25_warmelt_base_UNREGISTER_PASS_EXECUTION_HOOK */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_26_warmelt_base_LAMBDA___2__ (meltclosure_ptr_t meltclosp_,
				       melt_ptr_t meltfirstargp_,
				       const melt_argdescr_cell_t
				       meltxargdescr_[],
				       union meltparam_un * meltxargtab_,
				       const melt_argdescr_cell_t
				       meltxresdescr_[],
				       union meltparam_un * meltxrestab_)
{
  long current_blocklevel_signals_meltrout_26_warmelt_base_LAMBDA___2___melt =
    melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_26_warmelt_base_LAMBDA___2___st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 3
    melt_ptr_t mcfr_varptr[3];
#define MELTFRAM_NBVARNUM 1
    long mcfr_varnum[1];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_26_warmelt_base_LAMBDA___2__ is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_26_warmelt_base_LAMBDA___2___st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 3; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_26_warmelt_base_LAMBDA___2__ nbval 3*/
  meltfram__.mcfr_nbvar = 3 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("LAMBDA_", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-base.melt:1701:/ getarg");
 /*_.PASSNAME__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_LONG)
    goto lab_endgetargs;
 /*_#PASSNUM__L1*/ meltfnum[0] = meltxargtab_[0].meltbp_long;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {

    MELT_LOCATION ("warmelt-base.melt:1702:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[2];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.PASSNAME__V2*/ meltfptr[1];
      /*^apply.arg */
      argtab[1].meltbp_long = /*_#PASSNUM__L1*/ meltfnum[0];
      /*_.RUN_ALL_PASS_EXECUTION_HOOKS__V3*/ meltfptr[2] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!RUN_ALL_PASS_EXECUTION_HOOKS */ meltfrout->
		      tabval[0])),
		    (melt_ptr_t) (( /*~NEWHOOKLIST */ meltfclos->tabval[0])),
		    (MELTBPARSTR_PTR MELTBPARSTR_LONG ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-base.melt:1701:/ quasiblock");


    /*_.RETVAL___V1*/ meltfptr[0] =
      /*_.RUN_ALL_PASS_EXECUTION_HOOKS__V3*/ meltfptr[2];;

    {
      MELT_LOCATION ("warmelt-base.melt:1701:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.RUN_ALL_PASS_EXECUTION_HOOKS__V3*/ meltfptr[2] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("LAMBDA_", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_26_warmelt_base_LAMBDA___2___melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_26_warmelt_base_LAMBDA___2__ */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_27_warmelt_base_REGISTER_PRAGMA_HANDLER (meltclosure_ptr_t
						  meltclosp_,
						  melt_ptr_t meltfirstargp_,
						  const melt_argdescr_cell_t
						  meltxargdescr_[],
						  union meltparam_un *
						  meltxargtab_,
						  const melt_argdescr_cell_t
						  meltxresdescr_[],
						  union meltparam_un *
						  meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_27_warmelt_base_REGISTER_PRAGMA_HANDLER_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_27_warmelt_base_REGISTER_PRAGMA_HANDLER_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 12
    melt_ptr_t mcfr_varptr[12];
#define MELTFRAM_NBVARNUM 14
    long mcfr_varnum[14];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_27_warmelt_base_REGISTER_PRAGMA_HANDLER is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_27_warmelt_base_REGISTER_PRAGMA_HANDLER_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 12; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_27_warmelt_base_REGISTER_PRAGMA_HANDLER nbval 12*/
  meltfram__.mcfr_nbvar = 12 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("REGISTER_PRAGMA_HANDLER", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-base.melt:1706:/ getarg");
 /*_.LSTHANDLER__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-base.melt:1711:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_LIST__L1*/ meltfnum[0] =
	(melt_magic_discr ((melt_ptr_t) ( /*_.LSTHANDLER__V2*/ meltfptr[1]))
	 == MELTOBMAG_LIST);;
      MELT_LOCATION ("warmelt-base.melt:1711:/ cond");
      /*cond */ if ( /*_#IS_LIST__L1*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V4*/ meltfptr[3] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-base.melt:1711:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("register_pragma_handler takes a list as argument."), ("warmelt-base.melt") ? ("warmelt-base.melt") : __FILE__, (1711) ? (1711) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V4*/ meltfptr[3] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V3*/ meltfptr[2] = /*_.IFELSE___V4*/ meltfptr[3];;
      /*epilog */

      MELT_LOCATION ("warmelt-base.melt:1711:/ clear");
	     /*clear *//*_#IS_LIST__L1*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V4*/ meltfptr[3] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V3*/ meltfptr[2] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-base.melt:1713:/ quasiblock");


    /*^cond */
    /*cond */ if (
		   /*ifisa */
		   melt_is_instance_of ((melt_ptr_t)
					(( /*!INITIAL_SYSTEM_DATA */
					  meltfrout->tabval[0])),
					(melt_ptr_t) (( /*!CLASS_SYSTEM_DATA */ meltfrout->tabval[1])))
      )				/*then */
      {
	/*^cond.then */
	/*^getslot */
	{
	  melt_ptr_t slot = NULL, obj = NULL;
	  obj =
	    (melt_ptr_t) (( /*!INITIAL_SYSTEM_DATA */ meltfrout->
			   tabval[0])) /*=obj*/ ;
	  melt_object_get_field (slot, obj, 27, "SYSDATA_MELTPRAGMAS");
   /*_.OLDTUPLE__V5*/ meltfptr[3] = slot;
	};
	;
      }
    else
      {				/*^cond.else */

  /*_.OLDTUPLE__V5*/ meltfptr[3] = NULL;;
      }
    ;
    /*^compute */
 /*_#OLDSIZE__L2*/ meltfnum[0] = 0;;
    MELT_LOCATION ("warmelt-base.melt:1715:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^cond */
    /*cond */ if (( /*!NOTNULL */ meltfrout->tabval[2]))	/*then */
      {
	/*^cond.then */
	/*_.IF___V6*/ meltfptr[5] = /*_.OLDTUPLE__V5*/ meltfptr[3];;
      }
    else
      {
	MELT_LOCATION ("warmelt-base.melt:1715:/ cond.else");

  /*_.IF___V6*/ meltfptr[5] = NULL;;
      }
    ;
    /*^compute */
 /*_#MULTIPLE_LENGTH__L3*/ meltfnum[2] =
      (melt_multiple_length
       ((melt_ptr_t) ( /*_.OLDTUPLE__V5*/ meltfptr[3])));;
    MELT_LOCATION ("warmelt-base.melt:1716:/ compute");
    /*_#OLDSIZE__L2*/ meltfnum[0] = /*_#SETQ___L4*/ meltfnum[3] =
      /*_#MULTIPLE_LENGTH__L3*/ meltfnum[2];;
    MELT_LOCATION ("warmelt-base.melt:1717:/ quasiblock");


 /*_#MULTIPLE_LENGTH__L5*/ meltfnum[4] =
      (melt_multiple_length
       ((melt_ptr_t) ( /*_.OLDTUPLE__V5*/ meltfptr[3])));;
    /*^compute */
 /*_#LIST_LENGTH__L6*/ meltfnum[5] =
      (melt_list_length ((melt_ptr_t) ( /*_.LSTHANDLER__V2*/ meltfptr[1])));;
    /*^compute */
 /*_#NEWSIZE__L7*/ meltfnum[6] =
      (( /*_#MULTIPLE_LENGTH__L5*/ meltfnum[4]) +
       ( /*_#LIST_LENGTH__L6*/ meltfnum[5]));;
    /*^compute */
 /*_.NEWTUPLE__V7*/ meltfptr[6] =
      (meltgc_new_multiple
       ((meltobject_ptr_t) (( /*!DISCR_MULTIPLE */ meltfrout->tabval[3])),
	( /*_#NEWSIZE__L7*/ meltfnum[6])));;
    /*^compute */
 /*_#I__L8*/ meltfnum[7] = 0;;
    /*citerblock FOREACH_IN_MULTIPLE */
    {
      /* start foreach_in_multiple meltcit1__EACHTUP */
      long meltcit1__EACHTUP_ln =
	melt_multiple_length ((melt_ptr_t) /*_.OLDTUPLE__V5*/ meltfptr[3]);
      for ( /*_#IUNUSED__L9*/ meltfnum[8] = 0;
	   ( /*_#IUNUSED__L9*/ meltfnum[8] >= 0)
	   && ( /*_#IUNUSED__L9*/ meltfnum[8] < meltcit1__EACHTUP_ln);
	/*_#IUNUSED__L9*/ meltfnum[8]++)
	{
	  /*_.CURHANDER__V8*/ meltfptr[7] =
	    melt_multiple_nth ((melt_ptr_t) ( /*_.OLDTUPLE__V5*/ meltfptr[3]),
			       /*_#IUNUSED__L9*/ meltfnum[8]);




	  {
	    MELT_LOCATION ("warmelt-base.melt:1725:/ locexp");
	    meltgc_multiple_put_nth ((melt_ptr_t)
				     ( /*_.NEWTUPLE__V7*/ meltfptr[6]),
				     ( /*_#I__L8*/ meltfnum[7]),
				     (melt_ptr_t) ( /*_.CURHANDER__V8*/
						   meltfptr[7]));
	  }
	  ;
  /*_#I__L10*/ meltfnum[9] =
	    (( /*_#I__L8*/ meltfnum[7]) + (1));;
	  MELT_LOCATION ("warmelt-base.melt:1726:/ compute");
	  /*_#I__L8*/ meltfnum[7] = /*_#SETQ___L11*/ meltfnum[10] =
	    /*_#I__L10*/ meltfnum[9];;
	  if ( /*_#IUNUSED__L9*/ meltfnum[8] < 0)
	    break;
	}			/* end  foreach_in_multiple meltcit1__EACHTUP */

      /*citerepilog */

      MELT_LOCATION ("warmelt-base.melt:1722:/ clear");
	    /*clear *//*_.CURHANDER__V8*/ meltfptr[7] = 0;
      /*^clear */
	    /*clear *//*_#IUNUSED__L9*/ meltfnum[8] = 0;
      /*^clear */
	    /*clear *//*_#I__L10*/ meltfnum[9] = 0;
      /*^clear */
	    /*clear *//*_#SETQ___L11*/ meltfnum[10] = 0;
    }				/*endciterblock FOREACH_IN_MULTIPLE */
    ;
    /*citerblock FOREACH_IN_LIST */
    {
      /* start foreach_in_list meltcit2__EACHLIST */
      for ( /*_.CURPAIR__V9*/ meltfptr[8] =
	   melt_list_first ((melt_ptr_t) /*_.LSTHANDLER__V2*/ meltfptr[1]);
	   melt_magic_discr ((melt_ptr_t) /*_.CURPAIR__V9*/ meltfptr[8]) ==
	   MELTOBMAG_PAIR;
	   /*_.CURPAIR__V9*/ meltfptr[8] =
	   melt_pair_tail ((melt_ptr_t) /*_.CURPAIR__V9*/ meltfptr[8]))
	{
	  /*_.CURHANDLER__V10*/ meltfptr[9] =
	    melt_pair_head ((melt_ptr_t) /*_.CURPAIR__V9*/ meltfptr[8]);



#if MELT_HAVE_DEBUG
	  MELT_LOCATION ("warmelt-base.melt:1732:/ cppif.then");
	  /*^block */
	  /*anyblock */
	  {

	    /*^checksignal */
	    MELT_CHECK_SIGNAL ();
	    ;
    /*_#IS_A__L12*/ meltfnum[11] =
	      melt_is_instance_of ((melt_ptr_t)
				   ( /*_.CURHANDLER__V10*/ meltfptr[9]),
				   (melt_ptr_t) (( /*!CLASS_GCC_PRAGMA */
						  meltfrout->tabval[4])));;
	    MELT_LOCATION ("warmelt-base.melt:1732:/ cond");
	    /*cond */ if ( /*_#IS_A__L12*/ meltfnum[11])	/*then */
	      {
		/*^cond.then */
		/*_.IFELSE___V12*/ meltfptr[11] = ( /*nil */ NULL);;
	      }
	    else
	      {
		MELT_LOCATION ("warmelt-base.melt:1732:/ cond.else");

		/*^block */
		/*anyblock */
		{




		  {
		    /*^locexp */
		    melt_assert_failed (("register_pragma_handler must be a list of class_gcc_pragma."), ("warmelt-base.melt") ? ("warmelt-base.melt") : __FILE__, (1732) ? (1732) : __LINE__, __FUNCTION__);
		    ;
		  }
		  ;
		/*clear *//*_.IFELSE___V12*/ meltfptr[11] = 0;
		  /*epilog */
		}
		;
	      }
	    ;
	    /*^compute */
	    /*_.IFCPP___V11*/ meltfptr[10] = /*_.IFELSE___V12*/ meltfptr[11];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-base.melt:1732:/ clear");
	      /*clear *//*_#IS_A__L12*/ meltfnum[11] = 0;
	    /*^clear */
	      /*clear *//*_.IFELSE___V12*/ meltfptr[11] = 0;
	  }

#else /*MELT_HAVE_DEBUG */
	  /*^cppif.else */
	  /*_.IFCPP___V11*/ meltfptr[10] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
	  ;

	  {
	    MELT_LOCATION ("warmelt-base.melt:1734:/ locexp");
	    meltgc_multiple_put_nth ((melt_ptr_t)
				     ( /*_.NEWTUPLE__V7*/ meltfptr[6]),
				     ( /*_#I__L8*/ meltfnum[7]),
				     (melt_ptr_t) ( /*_.CURHANDLER__V10*/
						   meltfptr[9]));
	  }
	  ;
  /*_#I__L13*/ meltfnum[11] =
	    (( /*_#I__L8*/ meltfnum[7]) + (1));;
	  MELT_LOCATION ("warmelt-base.melt:1735:/ compute");
	  /*_#I__L8*/ meltfnum[7] = /*_#SETQ___L14*/ meltfnum[13] =
	    /*_#I__L13*/ meltfnum[11];;
	}			/* end foreach_in_list meltcit2__EACHLIST */
     /*_.CURPAIR__V9*/ meltfptr[8] = NULL;
     /*_.CURHANDLER__V10*/ meltfptr[9] = NULL;


      /*citerepilog */

      MELT_LOCATION ("warmelt-base.melt:1729:/ clear");
	    /*clear *//*_.CURPAIR__V9*/ meltfptr[8] = 0;
      /*^clear */
	    /*clear *//*_.CURHANDLER__V10*/ meltfptr[9] = 0;
      /*^clear */
	    /*clear *//*_.IFCPP___V11*/ meltfptr[10] = 0;
      /*^clear */
	    /*clear *//*_#I__L13*/ meltfnum[11] = 0;
      /*^clear */
	    /*clear *//*_#SETQ___L14*/ meltfnum[13] = 0;
    }				/*endciterblock FOREACH_IN_LIST */
    ;
    MELT_LOCATION ("warmelt-base.melt:1737:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^cond */
    /*cond */ if (
		   /*ifisa */
		   melt_is_instance_of ((melt_ptr_t)
					(( /*!INITIAL_SYSTEM_DATA */
					  meltfrout->tabval[0])),
					(melt_ptr_t) (( /*!CLASS_SYSTEM_DATA */ meltfrout->tabval[1])))
      )				/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  /*^putslot */
	  /*putslot */
	  melt_assertmsg ("putslot checkobj @SYSDATA_MELTPRAGMAS",
			  melt_magic_discr ((melt_ptr_t)
					    (( /*!INITIAL_SYSTEM_DATA */
					      meltfrout->tabval[0]))) ==
			  MELTOBMAG_OBJECT);
	  melt_putfield_object ((( /*!INITIAL_SYSTEM_DATA */ meltfrout->
				  tabval[0])), (27),
				( /*_.NEWTUPLE__V7*/ meltfptr[6]),
				"SYSDATA_MELTPRAGMAS");
	  ;
	  /*^touch */
	  meltgc_touch (( /*!INITIAL_SYSTEM_DATA */ meltfrout->tabval[0]));
	  ;
	  /*^touchobj */

	  melt_dbgtrace_written_object (( /*!INITIAL_SYSTEM_DATA */
					 meltfrout->tabval[0]), "put-fields");
	  ;
	  /*epilog */
	}
	;
      }				/*noelse */
    ;

    MELT_LOCATION ("warmelt-base.melt:1717:/ clear");
	   /*clear *//*_#MULTIPLE_LENGTH__L5*/ meltfnum[4] = 0;
    /*^clear */
	   /*clear *//*_#LIST_LENGTH__L6*/ meltfnum[5] = 0;
    /*^clear */
	   /*clear *//*_#NEWSIZE__L7*/ meltfnum[6] = 0;
    /*^clear */
	   /*clear *//*_.NEWTUPLE__V7*/ meltfptr[6] = 0;
    /*^clear */
	   /*clear *//*_#I__L8*/ meltfnum[7] = 0;

    MELT_LOCATION ("warmelt-base.melt:1713:/ clear");
	   /*clear *//*_.OLDTUPLE__V5*/ meltfptr[3] = 0;
    /*^clear */
	   /*clear *//*_#OLDSIZE__L2*/ meltfnum[0] = 0;
    /*^clear */
	   /*clear *//*_.IF___V6*/ meltfptr[5] = 0;
    /*^clear */
	   /*clear *//*_#MULTIPLE_LENGTH__L3*/ meltfnum[2] = 0;
    /*^clear */
	   /*clear *//*_#SETQ___L4*/ meltfnum[3] = 0;
    MELT_LOCATION ("warmelt-base.melt:1706:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V3*/ meltfptr[2] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("REGISTER_PRAGMA_HANDLER", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_27_warmelt_base_REGISTER_PRAGMA_HANDLER_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*noretval */ NULL);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_27_warmelt_base_REGISTER_PRAGMA_HANDLER */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_28_warmelt_base_REGISTER_PRE_GENERICIZE_HOOK_FIRST (meltclosure_ptr_t
							     meltclosp_,
							     melt_ptr_t
							     meltfirstargp_,
							     const
							     melt_argdescr_cell_t
							     meltxargdescr_[],
							     union
							     meltparam_un *
							     meltxargtab_,
							     const
							     melt_argdescr_cell_t
							     meltxresdescr_[],
							     union
							     meltparam_un *
							     meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_28_warmelt_base_REGISTER_PRE_GENERICIZE_HOOK_FIRST_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_28_warmelt_base_REGISTER_PRE_GENERICIZE_HOOK_FIRST_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 6
    melt_ptr_t mcfr_varptr[6];
#define MELTFRAM_NBVARNUM 2
    long mcfr_varnum[2];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_28_warmelt_base_REGISTER_PRE_GENERICIZE_HOOK_FIRST is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct
	 frame_meltrout_28_warmelt_base_REGISTER_PRE_GENERICIZE_HOOK_FIRST_st
	 *) meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 6; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_28_warmelt_base_REGISTER_PRE_GENERICIZE_HOOK_FIRST nbval 6*/
  meltfram__.mcfr_nbvar = 6 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("REGISTER_PRE_GENERICIZE_HOOK_FIRST", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-base.melt:1748:/ getarg");
 /*_.FUN__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {

    MELT_LOCATION ("warmelt-base.melt:1752:/ quasiblock");


    /*^cond */
    /*cond */ if (
		   /*ifisa */
		   melt_is_instance_of ((melt_ptr_t)
					(( /*!PREGENERICIZE_DELAYED_QUEUE */
					  meltfrout->tabval[0])),
					(melt_ptr_t) (( /*!CLASS_DELAYED_QUEUE */ meltfrout->tabval[1])))
      )				/*then */
      {
	/*^cond.then */
	/*^getslot */
	{
	  melt_ptr_t slot = NULL, obj = NULL;
	  obj =
	    (melt_ptr_t) (( /*!PREGENERICIZE_DELAYED_QUEUE */ meltfrout->
			   tabval[0])) /*=obj*/ ;
	  melt_object_get_field (slot, obj, 2, "DELQU_FIRST");
   /*_.FIRSTLIST__V3*/ meltfptr[2] = slot;
	};
	;
      }
    else
      {				/*^cond.else */

  /*_.FIRSTLIST__V3*/ meltfptr[2] = NULL;;
      }
    ;
    MELT_LOCATION ("warmelt-base.melt:1754:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#NULL__L1*/ meltfnum[0] =
      (( /*_.FIRSTLIST__V3*/ meltfptr[2]) == NULL);;
    MELT_LOCATION ("warmelt-base.melt:1754:/ cond");
    /*cond */ if ( /*_#NULL__L1*/ meltfnum[0])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

   /*_.MAKE_LIST__V4*/ meltfptr[3] =
	    (meltgc_new_list
	     ((meltobject_ptr_t)
	      (( /*!DISCR_LIST */ meltfrout->tabval[2]))));;
	  MELT_LOCATION ("warmelt-base.melt:1756:/ compute");
	  /*_.FIRSTLIST__V3*/ meltfptr[2] = /*_.SETQ___V5*/ meltfptr[4] =
	    /*_.MAKE_LIST__V4*/ meltfptr[3];;
	  MELT_LOCATION ("warmelt-base.melt:1757:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
   /*_.MAKE_LIST__V6*/ meltfptr[5] =
	    (meltgc_new_list
	     ((meltobject_ptr_t)
	      (( /*!DISCR_LIST */ meltfrout->tabval[2]))));;
	  MELT_LOCATION ("warmelt-base.melt:1757:/ cond");
	  /*cond */ if (
			 /*ifisa */
			 melt_is_instance_of ((melt_ptr_t)
					      (( /*!PREGENERICIZE_DELAYED_QUEUE */ meltfrout->tabval[0])),
					      (melt_ptr_t) (( /*!CLASS_DELAYED_QUEUE */ meltfrout->tabval[1])))
	    )			/*then */
	    {
	      /*^cond.then */
	      /*^block */
	      /*anyblock */
	      {

		/*^putslot */
		/*putslot */
		melt_assertmsg ("putslot checkobj @DELQU_FIRST",
				melt_magic_discr ((melt_ptr_t)
						  (( /*!PREGENERICIZE_DELAYED_QUEUE */ meltfrout->tabval[0]))) == MELTOBMAG_OBJECT);
		melt_putfield_object ((( /*!PREGENERICIZE_DELAYED_QUEUE */
					meltfrout->tabval[0])), (2),
				      ( /*_.FIRSTLIST__V3*/ meltfptr[2]),
				      "DELQU_FIRST");
		;
		/*^putslot */
		/*putslot */
		melt_assertmsg ("putslot checkobj @DELQU_LAST",
				melt_magic_discr ((melt_ptr_t)
						  (( /*!PREGENERICIZE_DELAYED_QUEUE */ meltfrout->tabval[0]))) == MELTOBMAG_OBJECT);
		melt_putfield_object ((( /*!PREGENERICIZE_DELAYED_QUEUE */
					meltfrout->tabval[0])), (3),
				      ( /*_.MAKE_LIST__V6*/ meltfptr[5]),
				      "DELQU_LAST");
		;
		/*^touch */
		meltgc_touch (( /*!PREGENERICIZE_DELAYED_QUEUE */ meltfrout->
			       tabval[0]));
		;
		/*^touchobj */

		melt_dbgtrace_written_object (( /*!PREGENERICIZE_DELAYED_QUEUE */ meltfrout->tabval[0]), "put-fields");
		;
		/*epilog */
	      }
	      ;
	    }			/*noelse */
	  ;
	  MELT_LOCATION ("warmelt-base.melt:1761:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^cond */
	  /*cond */ if (
			 /*ifisa */
			 melt_is_instance_of ((melt_ptr_t)
					      (( /*!INITIAL_SYSTEM_DATA */
						meltfrout->tabval[3])),
					      (melt_ptr_t) (( /*!CLASS_SYSTEM_DATA */ meltfrout->tabval[5])))
	    )			/*then */
	    {
	      /*^cond.then */
	      /*^block */
	      /*anyblock */
	      {

		/*^putslot */
		/*putslot */
		melt_assertmsg ("putslot checkobj @SYSDATA_PRE_GENERICIZE",
				melt_magic_discr ((melt_ptr_t)
						  (( /*!INITIAL_SYSTEM_DATA */
						    meltfrout->tabval[3]))) ==
				MELTOBMAG_OBJECT);
		melt_putfield_object ((( /*!INITIAL_SYSTEM_DATA */ meltfrout->
					tabval[3])), (21),
				      (( /*!PRE_GENERICIZE_HOOK_RUNNER */
					meltfrout->tabval[4])),
				      "SYSDATA_PRE_GENERICIZE");
		;
		/*^touch */
		meltgc_touch (( /*!INITIAL_SYSTEM_DATA */ meltfrout->
			       tabval[3]));
		;
		/*^touchobj */

		melt_dbgtrace_written_object (( /*!INITIAL_SYSTEM_DATA */
					       meltfrout->tabval[3]),
					      "put-fields");
		;
		/*epilog */
	      }
	      ;
	    }			/*noelse */
	  ;
	  MELT_LOCATION ("warmelt-base.melt:1755:/ quasiblock");


	  /*epilog */

	  MELT_LOCATION ("warmelt-base.melt:1754:/ clear");
	     /*clear *//*_.MAKE_LIST__V4*/ meltfptr[3] = 0;
	  /*^clear */
	     /*clear *//*_.SETQ___V5*/ meltfptr[4] = 0;
	  /*^clear */
	     /*clear *//*_.MAKE_LIST__V6*/ meltfptr[5] = 0;
	}
	;
      }				/*noelse */
    ;
    MELT_LOCATION ("warmelt-base.melt:1764:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#IS_CLOSURE__L2*/ meltfnum[1] =
      (melt_magic_discr ((melt_ptr_t) ( /*_.FUN__V2*/ meltfptr[1])) ==
       MELTOBMAG_CLOSURE);;
    MELT_LOCATION ("warmelt-base.melt:1764:/ cond");
    /*cond */ if ( /*_#IS_CLOSURE__L2*/ meltfnum[1])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{


	  {
	    MELT_LOCATION ("warmelt-base.melt:1765:/ locexp");
	    meltgc_append_list ((melt_ptr_t)
				( /*_.FIRSTLIST__V3*/ meltfptr[2]),
				(melt_ptr_t) ( /*_.FUN__V2*/ meltfptr[1]));
	  }
	  ;
	  /*epilog */
	}
	;
      }
    else
      {
	MELT_LOCATION ("warmelt-base.melt:1764:/ cond.else");

	/*^block */
	/*anyblock */
	{


	  {
	    MELT_LOCATION ("warmelt-base.melt:1766:/ locexp");
	    error ("MELT ERROR MSG [#%ld]::: %s", melt_dbgcounter,
		   ("Bad hook passed to register_pre_genericize_hook"));
	  }
	  ;
	  /*epilog */
	}
	;
      }
    ;

    MELT_LOCATION ("warmelt-base.melt:1752:/ clear");
	   /*clear *//*_.FIRSTLIST__V3*/ meltfptr[2] = 0;
    /*^clear */
	   /*clear *//*_#NULL__L1*/ meltfnum[0] = 0;
    /*^clear */
	   /*clear *//*_#IS_CLOSURE__L2*/ meltfnum[1] = 0;
    /*epilog */
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("REGISTER_PRE_GENERICIZE_HOOK_FIRST", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_28_warmelt_base_REGISTER_PRE_GENERICIZE_HOOK_FIRST_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*noretval */ NULL);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_28_warmelt_base_REGISTER_PRE_GENERICIZE_HOOK_FIRST */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_29_warmelt_base_REGISTER_PRE_GENERICIZE_HOOK_LAST (meltclosure_ptr_t
							    meltclosp_,
							    melt_ptr_t
							    meltfirstargp_,
							    const
							    melt_argdescr_cell_t
							    meltxargdescr_[],
							    union meltparam_un
							    * meltxargtab_,
							    const
							    melt_argdescr_cell_t
							    meltxresdescr_[],
							    union meltparam_un
							    * meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_29_warmelt_base_REGISTER_PRE_GENERICIZE_HOOK_LAST_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_29_warmelt_base_REGISTER_PRE_GENERICIZE_HOOK_LAST_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 6
    melt_ptr_t mcfr_varptr[6];
#define MELTFRAM_NBVARNUM 2
    long mcfr_varnum[2];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_29_warmelt_base_REGISTER_PRE_GENERICIZE_HOOK_LAST is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct
	 frame_meltrout_29_warmelt_base_REGISTER_PRE_GENERICIZE_HOOK_LAST_st
	 *) meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 6; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_29_warmelt_base_REGISTER_PRE_GENERICIZE_HOOK_LAST nbval 6*/
  meltfram__.mcfr_nbvar = 6 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("REGISTER_PRE_GENERICIZE_HOOK_LAST", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-base.melt:1771:/ getarg");
 /*_.FUN__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {

    MELT_LOCATION ("warmelt-base.melt:1775:/ quasiblock");


    /*^cond */
    /*cond */ if (
		   /*ifisa */
		   melt_is_instance_of ((melt_ptr_t)
					(( /*!PREGENERICIZE_DELAYED_QUEUE */
					  meltfrout->tabval[0])),
					(melt_ptr_t) (( /*!CLASS_DELAYED_QUEUE */ meltfrout->tabval[1])))
      )				/*then */
      {
	/*^cond.then */
	/*^getslot */
	{
	  melt_ptr_t slot = NULL, obj = NULL;
	  obj =
	    (melt_ptr_t) (( /*!PREGENERICIZE_DELAYED_QUEUE */ meltfrout->
			   tabval[0])) /*=obj*/ ;
	  melt_object_get_field (slot, obj, 2, "DELQU_FIRST");
   /*_.LASTLIST__V3*/ meltfptr[2] = slot;
	};
	;
      }
    else
      {				/*^cond.else */

  /*_.LASTLIST__V3*/ meltfptr[2] = NULL;;
      }
    ;
    MELT_LOCATION ("warmelt-base.melt:1777:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#NULL__L1*/ meltfnum[0] =
      (( /*_.LASTLIST__V3*/ meltfptr[2]) == NULL);;
    MELT_LOCATION ("warmelt-base.melt:1777:/ cond");
    /*cond */ if ( /*_#NULL__L1*/ meltfnum[0])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

   /*_.MAKE_LIST__V4*/ meltfptr[3] =
	    (meltgc_new_list
	     ((meltobject_ptr_t)
	      (( /*!DISCR_LIST */ meltfrout->tabval[2]))));;
	  MELT_LOCATION ("warmelt-base.melt:1779:/ compute");
	  /*_.LASTLIST__V3*/ meltfptr[2] = /*_.SETQ___V5*/ meltfptr[4] =
	    /*_.MAKE_LIST__V4*/ meltfptr[3];;
	  MELT_LOCATION ("warmelt-base.melt:1780:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
   /*_.MAKE_LIST__V6*/ meltfptr[5] =
	    (meltgc_new_list
	     ((meltobject_ptr_t)
	      (( /*!DISCR_LIST */ meltfrout->tabval[2]))));;
	  MELT_LOCATION ("warmelt-base.melt:1780:/ cond");
	  /*cond */ if (
			 /*ifisa */
			 melt_is_instance_of ((melt_ptr_t)
					      (( /*!PREGENERICIZE_DELAYED_QUEUE */ meltfrout->tabval[0])),
					      (melt_ptr_t) (( /*!CLASS_DELAYED_QUEUE */ meltfrout->tabval[1])))
	    )			/*then */
	    {
	      /*^cond.then */
	      /*^block */
	      /*anyblock */
	      {

		/*^putslot */
		/*putslot */
		melt_assertmsg ("putslot checkobj @DELQU_FIRST",
				melt_magic_discr ((melt_ptr_t)
						  (( /*!PREGENERICIZE_DELAYED_QUEUE */ meltfrout->tabval[0]))) == MELTOBMAG_OBJECT);
		melt_putfield_object ((( /*!PREGENERICIZE_DELAYED_QUEUE */
					meltfrout->tabval[0])), (2),
				      ( /*_.LASTLIST__V3*/ meltfptr[2]),
				      "DELQU_FIRST");
		;
		/*^putslot */
		/*putslot */
		melt_assertmsg ("putslot checkobj @DELQU_LAST",
				melt_magic_discr ((melt_ptr_t)
						  (( /*!PREGENERICIZE_DELAYED_QUEUE */ meltfrout->tabval[0]))) == MELTOBMAG_OBJECT);
		melt_putfield_object ((( /*!PREGENERICIZE_DELAYED_QUEUE */
					meltfrout->tabval[0])), (3),
				      ( /*_.MAKE_LIST__V6*/ meltfptr[5]),
				      "DELQU_LAST");
		;
		/*^touch */
		meltgc_touch (( /*!PREGENERICIZE_DELAYED_QUEUE */ meltfrout->
			       tabval[0]));
		;
		/*^touchobj */

		melt_dbgtrace_written_object (( /*!PREGENERICIZE_DELAYED_QUEUE */ meltfrout->tabval[0]), "put-fields");
		;
		/*epilog */
	      }
	      ;
	    }			/*noelse */
	  ;
	  MELT_LOCATION ("warmelt-base.melt:1784:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^cond */
	  /*cond */ if (
			 /*ifisa */
			 melt_is_instance_of ((melt_ptr_t)
					      (( /*!INITIAL_SYSTEM_DATA */
						meltfrout->tabval[3])),
					      (melt_ptr_t) (( /*!CLASS_SYSTEM_DATA */ meltfrout->tabval[5])))
	    )			/*then */
	    {
	      /*^cond.then */
	      /*^block */
	      /*anyblock */
	      {

		/*^putslot */
		/*putslot */
		melt_assertmsg ("putslot checkobj @SYSDATA_PRE_GENERICIZE",
				melt_magic_discr ((melt_ptr_t)
						  (( /*!INITIAL_SYSTEM_DATA */
						    meltfrout->tabval[3]))) ==
				MELTOBMAG_OBJECT);
		melt_putfield_object ((( /*!INITIAL_SYSTEM_DATA */ meltfrout->
					tabval[3])), (21),
				      (( /*!PRE_GENERICIZE_HOOK_RUNNER */
					meltfrout->tabval[4])),
				      "SYSDATA_PRE_GENERICIZE");
		;
		/*^touch */
		meltgc_touch (( /*!INITIAL_SYSTEM_DATA */ meltfrout->
			       tabval[3]));
		;
		/*^touchobj */

		melt_dbgtrace_written_object (( /*!INITIAL_SYSTEM_DATA */
					       meltfrout->tabval[3]),
					      "put-fields");
		;
		/*epilog */
	      }
	      ;
	    }			/*noelse */
	  ;
	  MELT_LOCATION ("warmelt-base.melt:1778:/ quasiblock");


	  /*epilog */

	  MELT_LOCATION ("warmelt-base.melt:1777:/ clear");
	     /*clear *//*_.MAKE_LIST__V4*/ meltfptr[3] = 0;
	  /*^clear */
	     /*clear *//*_.SETQ___V5*/ meltfptr[4] = 0;
	  /*^clear */
	     /*clear *//*_.MAKE_LIST__V6*/ meltfptr[5] = 0;
	}
	;
      }				/*noelse */
    ;
    MELT_LOCATION ("warmelt-base.melt:1787:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#IS_CLOSURE__L2*/ meltfnum[1] =
      (melt_magic_discr ((melt_ptr_t) ( /*_.FUN__V2*/ meltfptr[1])) ==
       MELTOBMAG_CLOSURE);;
    MELT_LOCATION ("warmelt-base.melt:1787:/ cond");
    /*cond */ if ( /*_#IS_CLOSURE__L2*/ meltfnum[1])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{


	  {
	    MELT_LOCATION ("warmelt-base.melt:1788:/ locexp");
	    meltgc_append_list ((melt_ptr_t)
				( /*_.LASTLIST__V3*/ meltfptr[2]),
				(melt_ptr_t) ( /*_.FUN__V2*/ meltfptr[1]));
	  }
	  ;
	  /*epilog */
	}
	;
      }
    else
      {
	MELT_LOCATION ("warmelt-base.melt:1787:/ cond.else");

	/*^block */
	/*anyblock */
	{


	  {
	    MELT_LOCATION ("warmelt-base.melt:1789:/ locexp");
	    error ("MELT ERROR MSG [#%ld]::: %s", melt_dbgcounter,
		   ("Bad hook passed to register_pre_genericize_hook"));
	  }
	  ;
	  /*epilog */
	}
	;
      }
    ;

    MELT_LOCATION ("warmelt-base.melt:1775:/ clear");
	   /*clear *//*_.LASTLIST__V3*/ meltfptr[2] = 0;
    /*^clear */
	   /*clear *//*_#NULL__L1*/ meltfnum[0] = 0;
    /*^clear */
	   /*clear *//*_#IS_CLOSURE__L2*/ meltfnum[1] = 0;
    /*epilog */
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("REGISTER_PRE_GENERICIZE_HOOK_LAST", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_29_warmelt_base_REGISTER_PRE_GENERICIZE_HOOK_LAST_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*noretval */ NULL);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_29_warmelt_base_REGISTER_PRE_GENERICIZE_HOOK_LAST */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_30_warmelt_base_PRE_GENERICIZE_HOOK_RUNNER (meltclosure_ptr_t
						     meltclosp_,
						     melt_ptr_t
						     meltfirstargp_,
						     const
						     melt_argdescr_cell_t
						     meltxargdescr_[],
						     union meltparam_un *
						     meltxargtab_,
						     const
						     melt_argdescr_cell_t
						     meltxresdescr_[],
						     union meltparam_un *
						     meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_30_warmelt_base_PRE_GENERICIZE_HOOK_RUNNER_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_30_warmelt_base_PRE_GENERICIZE_HOOK_RUNNER_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 16
    melt_ptr_t mcfr_varptr[16];
#define MELTFRAM_NBVARNUM 1
    long mcfr_varnum[1];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_30_warmelt_base_PRE_GENERICIZE_HOOK_RUNNER is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_30_warmelt_base_PRE_GENERICIZE_HOOK_RUNNER_st
	 *) meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 16; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_30_warmelt_base_PRE_GENERICIZE_HOOK_RUNNER nbval 16*/
  meltfram__.mcfr_nbvar = 16 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("PRE_GENERICIZE_HOOK_RUNNER", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-base.melt:1795:/ getarg");
 /*_.FNDECLV__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {

    MELT_LOCATION ("warmelt-base.melt:1800:/ quasiblock");


    /*^cond */
    /*cond */ if (
		   /*ifisa */
		   melt_is_instance_of ((melt_ptr_t)
					(( /*!PREGENERICIZE_DELAYED_QUEUE */
					  meltfrout->tabval[0])),
					(melt_ptr_t) (( /*!CLASS_DELAYED_QUEUE */ meltfrout->tabval[1])))
      )				/*then */
      {
	/*^cond.then */
	/*^getslot */
	{
	  melt_ptr_t slot = NULL, obj = NULL;
	  obj =
	    (melt_ptr_t) (( /*!PREGENERICIZE_DELAYED_QUEUE */ meltfrout->
			   tabval[0])) /*=obj*/ ;
	  melt_object_get_field (slot, obj, 2, "DELQU_FIRST");
   /*_.FIRSTLIST__V3*/ meltfptr[2] = slot;
	};
	;
      }
    else
      {				/*^cond.else */

  /*_.FIRSTLIST__V3*/ meltfptr[2] = NULL;;
      }
    ;
    MELT_LOCATION ("warmelt-base.melt:1801:/ cond");
    /*cond */ if (
		   /*ifisa */
		   melt_is_instance_of ((melt_ptr_t)
					(( /*!PREGENERICIZE_DELAYED_QUEUE */
					  meltfrout->tabval[0])),
					(melt_ptr_t) (( /*!CLASS_DELAYED_QUEUE */ meltfrout->tabval[1])))
      )				/*then */
      {
	/*^cond.then */
	/*^getslot */
	{
	  melt_ptr_t slot = NULL, obj = NULL;
	  obj =
	    (melt_ptr_t) (( /*!PREGENERICIZE_DELAYED_QUEUE */ meltfrout->
			   tabval[0])) /*=obj*/ ;
	  melt_object_get_field (slot, obj, 3, "DELQU_LAST");
   /*_.LASTLIST__V4*/ meltfptr[3] = slot;
	};
	;
      }
    else
      {				/*^cond.else */

  /*_.LASTLIST__V4*/ meltfptr[3] = NULL;;
      }
    ;
    /*^compute */
 /*_.REVLASTLIST__V5*/ meltfptr[4] =
      (meltgc_new_list
       ((meltobject_ptr_t) (( /*!DISCR_LIST */ meltfrout->tabval[2]))));;
    /*^compute */
    /*_.RES__V6*/ meltfptr[5] = ( /*nil */ NULL);;
    /*citerblock FOREACH_IN_LIST */
    {
      /* start foreach_in_list meltcit1__EACHLIST */
      for ( /*_.FIRSTPAIR__V7*/ meltfptr[6] =
	   melt_list_first ((melt_ptr_t) /*_.FIRSTLIST__V3*/ meltfptr[2]);
	   melt_magic_discr ((melt_ptr_t) /*_.FIRSTPAIR__V7*/ meltfptr[6]) ==
	   MELTOBMAG_PAIR;
	   /*_.FIRSTPAIR__V7*/ meltfptr[6] =
	   melt_pair_tail ((melt_ptr_t) /*_.FIRSTPAIR__V7*/ meltfptr[6]))
	{
	  /*_.FIRSTPROC__V8*/ meltfptr[7] =
	    melt_pair_head ((melt_ptr_t) /*_.FIRSTPAIR__V7*/ meltfptr[6]);


	  MELT_LOCATION ("warmelt-base.melt:1809:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^apply */
	  /*apply */
	  {
	    union meltparam_un argtab[1];
	    memset (&argtab, 0, sizeof (argtab));
	    /*^apply.arg */
	    argtab[0].meltbp_aptr =
	      (melt_ptr_t *) & /*_.RES__V6*/ meltfptr[5];
	    /*_.FIRSTPROC__V9*/ meltfptr[8] =
	      melt_apply ((meltclosure_ptr_t)
			  ( /*_.FIRSTPROC__V8*/ meltfptr[7]),
			  (melt_ptr_t) ( /*_.FNDECLV__V2*/ meltfptr[1]),
			  (MELTBPARSTR_PTR ""), argtab, "",
			  (union meltparam_un *) 0);
	  }
	  ;
	  /*^compute */
	  /*_.RES__V6*/ meltfptr[5] = /*_.SETQ___V10*/ meltfptr[9] =
	    /*_.FIRSTPROC__V9*/ meltfptr[8];;
	}			/* end foreach_in_list meltcit1__EACHLIST */
     /*_.FIRSTPAIR__V7*/ meltfptr[6] = NULL;
     /*_.FIRSTPROC__V8*/ meltfptr[7] = NULL;


      /*citerepilog */

      MELT_LOCATION ("warmelt-base.melt:1806:/ clear");
	    /*clear *//*_.FIRSTPAIR__V7*/ meltfptr[6] = 0;
      /*^clear */
	    /*clear *//*_.FIRSTPROC__V8*/ meltfptr[7] = 0;
      /*^clear */
	    /*clear *//*_.FIRSTPROC__V9*/ meltfptr[8] = 0;
      /*^clear */
	    /*clear *//*_.SETQ___V10*/ meltfptr[9] = 0;
    }				/*endciterblock FOREACH_IN_LIST */
    ;
    /*citerblock FOREACH_IN_LIST */
    {
      /* start foreach_in_list meltcit2__EACHLIST */
      for ( /*_.LASTPAIR__V11*/ meltfptr[10] =
	   melt_list_first ((melt_ptr_t) /*_.LASTLIST__V4*/ meltfptr[3]);
	   melt_magic_discr ((melt_ptr_t) /*_.LASTPAIR__V11*/ meltfptr[10]) ==
	   MELTOBMAG_PAIR;
	   /*_.LASTPAIR__V11*/ meltfptr[10] =
	   melt_pair_tail ((melt_ptr_t) /*_.LASTPAIR__V11*/ meltfptr[10]))
	{
	  /*_.LASTPROC__V12*/ meltfptr[11] =
	    melt_pair_head ((melt_ptr_t) /*_.LASTPAIR__V11*/ meltfptr[10]);


	  MELT_LOCATION ("warmelt-base.melt:1815:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
  /*_#IS_CLOSURE__L1*/ meltfnum[0] =
	    (melt_magic_discr
	     ((melt_ptr_t) ( /*_.LASTPROC__V12*/ meltfptr[11])) ==
	     MELTOBMAG_CLOSURE);;
	  MELT_LOCATION ("warmelt-base.melt:1815:/ cond");
	  /*cond */ if ( /*_#IS_CLOSURE__L1*/ meltfnum[0])	/*then */
	    {
	      /*^cond.then */
	      /*^block */
	      /*anyblock */
	      {


		{
		  /*^locexp */
		  meltgc_prepend_list ((melt_ptr_t)
				       ( /*_.REVLASTLIST__V5*/ meltfptr[4]),
				       (melt_ptr_t) ( /*_.LASTPROC__V12*/
						     meltfptr[11]));
		}
		;
		/*epilog */
	      }
	      ;
	    }			/*noelse */
	  ;
	}			/* end foreach_in_list meltcit2__EACHLIST */
     /*_.LASTPAIR__V11*/ meltfptr[10] = NULL;
     /*_.LASTPROC__V12*/ meltfptr[11] = NULL;


      /*citerepilog */

      MELT_LOCATION ("warmelt-base.melt:1812:/ clear");
	    /*clear *//*_.LASTPAIR__V11*/ meltfptr[10] = 0;
      /*^clear */
	    /*clear *//*_.LASTPROC__V12*/ meltfptr[11] = 0;
      /*^clear */
	    /*clear *//*_#IS_CLOSURE__L1*/ meltfnum[0] = 0;
    }				/*endciterblock FOREACH_IN_LIST */
    ;
    /*citerblock FOREACH_IN_LIST */
    {
      /* start foreach_in_list meltcit3__EACHLIST */
      for ( /*_.REVLASTPAIR__V13*/ meltfptr[12] =
	   melt_list_first ((melt_ptr_t) /*_.REVLASTLIST__V5*/ meltfptr[4]);
	   melt_magic_discr ((melt_ptr_t) /*_.REVLASTPAIR__V13*/ meltfptr[12])
	   == MELTOBMAG_PAIR;
	   /*_.REVLASTPAIR__V13*/ meltfptr[12] =
	   melt_pair_tail ((melt_ptr_t) /*_.REVLASTPAIR__V13*/ meltfptr[12]))
	{
	  /*_.REVLASTPROC__V14*/ meltfptr[13] =
	    melt_pair_head ((melt_ptr_t) /*_.REVLASTPAIR__V13*/ meltfptr[12]);


	  MELT_LOCATION ("warmelt-base.melt:1820:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^apply */
	  /*apply */
	  {
	    union meltparam_un argtab[1];
	    memset (&argtab, 0, sizeof (argtab));
	    /*^apply.arg */
	    argtab[0].meltbp_aptr =
	      (melt_ptr_t *) & /*_.RES__V6*/ meltfptr[5];
	    /*_.REVLASTPROC__V15*/ meltfptr[14] =
	      melt_apply ((meltclosure_ptr_t)
			  ( /*_.REVLASTPROC__V14*/ meltfptr[13]),
			  (melt_ptr_t) ( /*_.FNDECLV__V2*/ meltfptr[1]),
			  (MELTBPARSTR_PTR ""), argtab, "",
			  (union meltparam_un *) 0);
	  }
	  ;
	  /*^compute */
	  /*_.RES__V6*/ meltfptr[5] = /*_.SETQ___V16*/ meltfptr[15] =
	    /*_.REVLASTPROC__V15*/ meltfptr[14];;
	}			/* end foreach_in_list meltcit3__EACHLIST */
     /*_.REVLASTPAIR__V13*/ meltfptr[12] = NULL;
     /*_.REVLASTPROC__V14*/ meltfptr[13] = NULL;


      /*citerepilog */

      MELT_LOCATION ("warmelt-base.melt:1817:/ clear");
	    /*clear *//*_.REVLASTPAIR__V13*/ meltfptr[12] = 0;
      /*^clear */
	    /*clear *//*_.REVLASTPROC__V14*/ meltfptr[13] = 0;
      /*^clear */
	    /*clear *//*_.REVLASTPROC__V15*/ meltfptr[14] = 0;
      /*^clear */
	    /*clear *//*_.SETQ___V16*/ meltfptr[15] = 0;
    }				/*endciterblock FOREACH_IN_LIST */
    ;

    MELT_LOCATION ("warmelt-base.melt:1800:/ clear");
	   /*clear *//*_.FIRSTLIST__V3*/ meltfptr[2] = 0;
    /*^clear */
	   /*clear *//*_.LASTLIST__V4*/ meltfptr[3] = 0;
    /*^clear */
	   /*clear *//*_.REVLASTLIST__V5*/ meltfptr[4] = 0;
    /*^clear */
	   /*clear *//*_.RES__V6*/ meltfptr[5] = 0;
    /*epilog */
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("PRE_GENERICIZE_HOOK_RUNNER", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_30_warmelt_base_PRE_GENERICIZE_HOOK_RUNNER_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*noretval */ NULL);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_30_warmelt_base_PRE_GENERICIZE_HOOK_RUNNER */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_31_warmelt_base_MELT_FINISH_TYPE_RUNNER (meltclosure_ptr_t
						  meltclosp_,
						  melt_ptr_t meltfirstargp_,
						  const melt_argdescr_cell_t
						  meltxargdescr_[],
						  union meltparam_un *
						  meltxargtab_,
						  const melt_argdescr_cell_t
						  meltxresdescr_[],
						  union meltparam_un *
						  meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_31_warmelt_base_MELT_FINISH_TYPE_RUNNER_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_31_warmelt_base_MELT_FINISH_TYPE_RUNNER_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 16
    melt_ptr_t mcfr_varptr[16];
#define MELTFRAM_NBVARNUM 1
    long mcfr_varnum[1];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_31_warmelt_base_MELT_FINISH_TYPE_RUNNER is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_31_warmelt_base_MELT_FINISH_TYPE_RUNNER_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 16; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_31_warmelt_base_MELT_FINISH_TYPE_RUNNER nbval 16*/
  meltfram__.mcfr_nbvar = 16 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("MELT_FINISH_TYPE_RUNNER", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-base.melt:1840:/ getarg");
 /*_.BOXTREEV__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {

    MELT_LOCATION ("warmelt-base.melt:1841:/ quasiblock");


    /*^cond */
    /*cond */ if (
		   /*ifisa */
		   melt_is_instance_of ((melt_ptr_t)
					(( /*!FINISHTYPE_DELAYED_QUEUE */
					  meltfrout->tabval[0])),
					(melt_ptr_t) (( /*!CLASS_DELAYED_QUEUE */ meltfrout->tabval[1])))
      )				/*then */
      {
	/*^cond.then */
	/*^getslot */
	{
	  melt_ptr_t slot = NULL, obj = NULL;
	  obj =
	    (melt_ptr_t) (( /*!FINISHTYPE_DELAYED_QUEUE */ meltfrout->
			   tabval[0])) /*=obj*/ ;
	  melt_object_get_field (slot, obj, 2, "DELQU_FIRST");
   /*_.FIRSTLIST__V3*/ meltfptr[2] = slot;
	};
	;
      }
    else
      {				/*^cond.else */

  /*_.FIRSTLIST__V3*/ meltfptr[2] = NULL;;
      }
    ;
    MELT_LOCATION ("warmelt-base.melt:1842:/ cond");
    /*cond */ if (
		   /*ifisa */
		   melt_is_instance_of ((melt_ptr_t)
					(( /*!FINISHTYPE_DELAYED_QUEUE */
					  meltfrout->tabval[0])),
					(melt_ptr_t) (( /*!CLASS_DELAYED_QUEUE */ meltfrout->tabval[1])))
      )				/*then */
      {
	/*^cond.then */
	/*^getslot */
	{
	  melt_ptr_t slot = NULL, obj = NULL;
	  obj =
	    (melt_ptr_t) (( /*!FINISHTYPE_DELAYED_QUEUE */ meltfrout->
			   tabval[0])) /*=obj*/ ;
	  melt_object_get_field (slot, obj, 3, "DELQU_LAST");
   /*_.LASTLIST__V4*/ meltfptr[3] = slot;
	};
	;
      }
    else
      {				/*^cond.else */

  /*_.LASTLIST__V4*/ meltfptr[3] = NULL;;
      }
    ;
    /*^compute */
 /*_.REVLASTLIST__V5*/ meltfptr[4] =
      (meltgc_new_list
       ((meltobject_ptr_t) (( /*!DISCR_LIST */ meltfrout->tabval[2]))));;
    /*^compute */
    /*_.RES__V6*/ meltfptr[5] = ( /*nil */ NULL);;
    /*citerblock FOREACH_IN_LIST */
    {
      /* start foreach_in_list meltcit1__EACHLIST */
      for ( /*_.FIRSTPAIR__V7*/ meltfptr[6] =
	   melt_list_first ((melt_ptr_t) /*_.FIRSTLIST__V3*/ meltfptr[2]);
	   melt_magic_discr ((melt_ptr_t) /*_.FIRSTPAIR__V7*/ meltfptr[6]) ==
	   MELTOBMAG_PAIR;
	   /*_.FIRSTPAIR__V7*/ meltfptr[6] =
	   melt_pair_tail ((melt_ptr_t) /*_.FIRSTPAIR__V7*/ meltfptr[6]))
	{
	  /*_.FIRSTPROC__V8*/ meltfptr[7] =
	    melt_pair_head ((melt_ptr_t) /*_.FIRSTPAIR__V7*/ meltfptr[6]);


	  MELT_LOCATION ("warmelt-base.melt:1850:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^apply */
	  /*apply */
	  {
	    union meltparam_un argtab[1];
	    memset (&argtab, 0, sizeof (argtab));
	    /*^apply.arg */
	    argtab[0].meltbp_aptr =
	      (melt_ptr_t *) & /*_.RES__V6*/ meltfptr[5];
	    /*_.FIRSTPROC__V9*/ meltfptr[8] =
	      melt_apply ((meltclosure_ptr_t)
			  ( /*_.FIRSTPROC__V8*/ meltfptr[7]),
			  (melt_ptr_t) ( /*_.BOXTREEV__V2*/ meltfptr[1]),
			  (MELTBPARSTR_PTR ""), argtab, "",
			  (union meltparam_un *) 0);
	  }
	  ;
	  /*^compute */
	  /*_.RES__V6*/ meltfptr[5] = /*_.SETQ___V10*/ meltfptr[9] =
	    /*_.FIRSTPROC__V9*/ meltfptr[8];;
	}			/* end foreach_in_list meltcit1__EACHLIST */
     /*_.FIRSTPAIR__V7*/ meltfptr[6] = NULL;
     /*_.FIRSTPROC__V8*/ meltfptr[7] = NULL;


      /*citerepilog */

      MELT_LOCATION ("warmelt-base.melt:1847:/ clear");
	    /*clear *//*_.FIRSTPAIR__V7*/ meltfptr[6] = 0;
      /*^clear */
	    /*clear *//*_.FIRSTPROC__V8*/ meltfptr[7] = 0;
      /*^clear */
	    /*clear *//*_.FIRSTPROC__V9*/ meltfptr[8] = 0;
      /*^clear */
	    /*clear *//*_.SETQ___V10*/ meltfptr[9] = 0;
    }				/*endciterblock FOREACH_IN_LIST */
    ;
    /*citerblock FOREACH_IN_LIST */
    {
      /* start foreach_in_list meltcit2__EACHLIST */
      for ( /*_.LASTPAIR__V11*/ meltfptr[10] =
	   melt_list_first ((melt_ptr_t) /*_.LASTLIST__V4*/ meltfptr[3]);
	   melt_magic_discr ((melt_ptr_t) /*_.LASTPAIR__V11*/ meltfptr[10]) ==
	   MELTOBMAG_PAIR;
	   /*_.LASTPAIR__V11*/ meltfptr[10] =
	   melt_pair_tail ((melt_ptr_t) /*_.LASTPAIR__V11*/ meltfptr[10]))
	{
	  /*_.LASTPROC__V12*/ meltfptr[11] =
	    melt_pair_head ((melt_ptr_t) /*_.LASTPAIR__V11*/ meltfptr[10]);


	  MELT_LOCATION ("warmelt-base.melt:1856:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
  /*_#IS_CLOSURE__L1*/ meltfnum[0] =
	    (melt_magic_discr
	     ((melt_ptr_t) ( /*_.LASTPROC__V12*/ meltfptr[11])) ==
	     MELTOBMAG_CLOSURE);;
	  MELT_LOCATION ("warmelt-base.melt:1856:/ cond");
	  /*cond */ if ( /*_#IS_CLOSURE__L1*/ meltfnum[0])	/*then */
	    {
	      /*^cond.then */
	      /*^block */
	      /*anyblock */
	      {


		{
		  /*^locexp */
		  meltgc_prepend_list ((melt_ptr_t)
				       ( /*_.REVLASTLIST__V5*/ meltfptr[4]),
				       (melt_ptr_t) ( /*_.LASTPROC__V12*/
						     meltfptr[11]));
		}
		;
		/*epilog */
	      }
	      ;
	    }			/*noelse */
	  ;
	}			/* end foreach_in_list meltcit2__EACHLIST */
     /*_.LASTPAIR__V11*/ meltfptr[10] = NULL;
     /*_.LASTPROC__V12*/ meltfptr[11] = NULL;


      /*citerepilog */

      MELT_LOCATION ("warmelt-base.melt:1853:/ clear");
	    /*clear *//*_.LASTPAIR__V11*/ meltfptr[10] = 0;
      /*^clear */
	    /*clear *//*_.LASTPROC__V12*/ meltfptr[11] = 0;
      /*^clear */
	    /*clear *//*_#IS_CLOSURE__L1*/ meltfnum[0] = 0;
    }				/*endciterblock FOREACH_IN_LIST */
    ;
    /*citerblock FOREACH_IN_LIST */
    {
      /* start foreach_in_list meltcit3__EACHLIST */
      for ( /*_.REVLASTPAIR__V13*/ meltfptr[12] =
	   melt_list_first ((melt_ptr_t) /*_.REVLASTLIST__V5*/ meltfptr[4]);
	   melt_magic_discr ((melt_ptr_t) /*_.REVLASTPAIR__V13*/ meltfptr[12])
	   == MELTOBMAG_PAIR;
	   /*_.REVLASTPAIR__V13*/ meltfptr[12] =
	   melt_pair_tail ((melt_ptr_t) /*_.REVLASTPAIR__V13*/ meltfptr[12]))
	{
	  /*_.REVLASTPROC__V14*/ meltfptr[13] =
	    melt_pair_head ((melt_ptr_t) /*_.REVLASTPAIR__V13*/ meltfptr[12]);


	  MELT_LOCATION ("warmelt-base.melt:1861:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^apply */
	  /*apply */
	  {
	    union meltparam_un argtab[1];
	    memset (&argtab, 0, sizeof (argtab));
	    /*^apply.arg */
	    argtab[0].meltbp_aptr =
	      (melt_ptr_t *) & /*_.RES__V6*/ meltfptr[5];
	    /*_.REVLASTPROC__V15*/ meltfptr[14] =
	      melt_apply ((meltclosure_ptr_t)
			  ( /*_.REVLASTPROC__V14*/ meltfptr[13]),
			  (melt_ptr_t) ( /*_.BOXTREEV__V2*/ meltfptr[1]),
			  (MELTBPARSTR_PTR ""), argtab, "",
			  (union meltparam_un *) 0);
	  }
	  ;
	  /*^compute */
	  /*_.RES__V6*/ meltfptr[5] = /*_.SETQ___V16*/ meltfptr[15] =
	    /*_.REVLASTPROC__V15*/ meltfptr[14];;
	}			/* end foreach_in_list meltcit3__EACHLIST */
     /*_.REVLASTPAIR__V13*/ meltfptr[12] = NULL;
     /*_.REVLASTPROC__V14*/ meltfptr[13] = NULL;


      /*citerepilog */

      MELT_LOCATION ("warmelt-base.melt:1858:/ clear");
	    /*clear *//*_.REVLASTPAIR__V13*/ meltfptr[12] = 0;
      /*^clear */
	    /*clear *//*_.REVLASTPROC__V14*/ meltfptr[13] = 0;
      /*^clear */
	    /*clear *//*_.REVLASTPROC__V15*/ meltfptr[14] = 0;
      /*^clear */
	    /*clear *//*_.SETQ___V16*/ meltfptr[15] = 0;
    }				/*endciterblock FOREACH_IN_LIST */
    ;

    MELT_LOCATION ("warmelt-base.melt:1841:/ clear");
	   /*clear *//*_.FIRSTLIST__V3*/ meltfptr[2] = 0;
    /*^clear */
	   /*clear *//*_.LASTLIST__V4*/ meltfptr[3] = 0;
    /*^clear */
	   /*clear *//*_.REVLASTLIST__V5*/ meltfptr[4] = 0;
    /*^clear */
	   /*clear *//*_.RES__V6*/ meltfptr[5] = 0;
    /*epilog */
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("MELT_FINISH_TYPE_RUNNER", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_31_warmelt_base_MELT_FINISH_TYPE_RUNNER_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*noretval */ NULL);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_31_warmelt_base_MELT_FINISH_TYPE_RUNNER */



/**** end of warmelt-base+01.c ****/
