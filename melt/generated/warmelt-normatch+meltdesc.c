/** GENERATED MELT DESCRIPTOR FILE meltbuild-sources/warmelt-normatch+meltdesc.c 
** NEVER EDIT OR MOVE THIS, IT IS GENERATED & PARSED! **/
/* These identifiers are generated in warmelt-outobj.melt 
 & handled in melt-runtime.c carefully. */

	     #ifdef __cplusplus
	     /* explicitly declare as extern "C" our dlsym-ed symbols */
	     extern "C" const char melt_versionmeltstr[]	    ;
	     extern "C" const char melt_genversionstr[]		    ;
	     extern "C" const char melt_modulename[]		    ;
	     extern "C" const char melt_modulerealpath[]	    ;
	     extern "C" const char melt_prepromd5meltrun[]	    ;
	     extern "C" const char melt_primaryhexmd5[]		    ;
	     extern "C" const char* const melt_secondaryhexmd5tab[] ;
	     extern "C" const int melt_lastsecfileindex		    ;
	     extern "C" const char melt_cumulated_hexmd5[]	    ;

	     extern "C" {
	     #endif /*__cplusplus */
	     
/* version of the GCC compiler & MELT runtime generating this */
const char melt_genversionstr[]="4.8.0 20121010 (experimental) [melt-branch revision 192289] MELT_0\
.9.7-rc4"

	     #ifdef __cplusplus
	     " (in C++)"
	     #else
	     " (in C)"
	     #endif
					;
	     
const char melt_versionmeltstr[]="0.9.7-rc4 [melt-branch_revision_192289]";

/* source name & real path of the module */
/*MELTMODULENAME meltbuild-sources/warmelt-normatch */
const char melt_modulename[]="warmelt-normatch";
const char melt_modulerealpath[]="/usr/local/libexec/gcc-melt/gcc/x86_64-unknown-linux-gnu/4.8.0/melt\
-modules/0.9.7-rc4/warmelt-normatch";

/* hash of preprocessed melt-run.h generating this */
const char melt_prepromd5meltrun[]="f66a30d9bc2c10de00b1532482ad91ed";
/* hexmd5checksum of primary C file */
const char melt_primaryhexmd5[]="b98a0614b98803a42a6bdb946d553853";

/* hexmd5checksum of secondary C files */
const char* const melt_secondaryhexmd5tab[]={
 /*nosecfile*/ (const char*)0,
 /*sechexmd5checksum meltbuild-sources/warmelt-normatch+01.c #1 */ "5397842c29b16413d482160bf30ad091",
 /*sechexmd5checksum meltbuild-sources/warmelt-normatch+02.c #2 */ "bdf454c4511759a888f91a760b21ad08",
 /*sechexmd5checksum meltbuild-sources/warmelt-normatch+03.c #3 */ "0c49ae6341a92cc0f77ba0bb10520fa3",
 /*sechexmd5checksum meltbuild-sources/warmelt-normatch+04.c #4 */ "5a34f890bd804c2e5efe726f93354527",
 /*sechexmd5checksum meltbuild-sources/warmelt-normatch+05.c #5 */ "5a7cea3b6c244283b824f86473a8bc20",
 /*nosecfile*/ (const char*)0,
 (const char*)0 };

/* last index of secondary files */
const int melt_lastsecfileindex=5;

/* cumulated checksum of primary & secondary files */
const char melt_cumulated_hexmd5[]="6b443a9000be0748a7c8bf6f4c87aaf3" ;

/* include the timestamp file */
#define meltmod_warmelt_normatch_mds__6b443a9000be0748a7c8bf6f4c87aaf3 1
#include "warmelt-normatch+melttime.h"
	 

		 #ifdef __cplusplus
		 }	  /* end extern C descriptor */
		 #endif /*__cplusplus */
		 
/* end of melt descriptor file */
