/* GCC MELT GENERATED FILE warmelt-normal+01.c - DO NOT EDIT */
/* secondary MELT generated C file of rank #1 */
#include "melt-run.h"


/* used hash from melt-run.h when compiling this file: */
MELT_EXTERN const char meltrun_used_md5_melt_f1[] =
  MELT_RUN_HASHMD5 /* from melt-run.h */ ;


/**** warmelt-normal+01.c declarations ****/


/***************************************************
***
    Copyright 2008, 2009, 2010, 2011, 2012 Free Software Foundation, Inc.
    Contributed by Basile Starynkevitch <basile@starynkevitch.net>

    This file is part of GCC.

    GCC is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3, or (at your option)
    any later version.

    GCC is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with GCC; see the file COPYING3.  If not see
    <http://www.gnu.org/licenses/>.
***
****************************************************/

/* ordinary MELT module */
#define MELT_HAS_INITIAL_ENVIRONMENT 1	/*usual */

struct melt_callframe_st;	/*defined in melt-runtime.h */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_1_warmelt_normal_ADD_NCTX_DATA (meltclosure_ptr_t meltclosp_,
					 melt_ptr_t meltfirstargp_,
					 const melt_argdescr_cell_t
					 meltxargdescr_[],
					 union meltparam_un *meltxargtab_,
					 const melt_argdescr_cell_t
					 meltxresdescr_[],
					 union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_2_warmelt_normal_FILL_INITIAL_PREDEFMAP (meltclosure_ptr_t
						  meltclosp_,
						  melt_ptr_t meltfirstargp_,
						  const melt_argdescr_cell_t
						  meltxargdescr_[],
						  union meltparam_un
						  *meltxargtab_,
						  const melt_argdescr_cell_t
						  meltxresdescr_[],
						  union meltparam_un
						  *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_3_warmelt_normal_REGISTER_LITERAL_VALUE (meltclosure_ptr_t
						  meltclosp_,
						  melt_ptr_t meltfirstargp_,
						  const melt_argdescr_cell_t
						  meltxargdescr_[],
						  union meltparam_un
						  *meltxargtab_,
						  const melt_argdescr_cell_t
						  meltxresdescr_[],
						  union meltparam_un
						  *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_4_warmelt_normal_CREATE_NORMCONTEXT (meltclosure_ptr_t meltclosp_,
					      melt_ptr_t meltfirstargp_,
					      const melt_argdescr_cell_t
					      meltxargdescr_[],
					      union meltparam_un
					      *meltxargtab_,
					      const melt_argdescr_cell_t
					      meltxresdescr_[],
					      union meltparam_un
					      *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_5_warmelt_normal_CREATE_NORMAL_EXTENDING_CONTEXT (meltclosure_ptr_t
							   meltclosp_,
							   melt_ptr_t
							   meltfirstargp_,
							   const
							   melt_argdescr_cell_t
							   meltxargdescr_[],
							   union meltparam_un
							   *meltxargtab_,
							   const
							   melt_argdescr_cell_t
							   meltxresdescr_[],
							   union meltparam_un
							   *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_6_warmelt_normal_NORMEXP_IDENTICAL (meltclosure_ptr_t meltclosp_,
					     melt_ptr_t meltfirstargp_,
					     const melt_argdescr_cell_t
					     meltxargdescr_[],
					     union meltparam_un *meltxargtab_,
					     const melt_argdescr_cell_t
					     meltxresdescr_[],
					     union meltparam_un
					     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_7_warmelt_normal_NORMEXP_NULL (meltclosure_ptr_t meltclosp_,
					melt_ptr_t meltfirstargp_,
					const melt_argdescr_cell_t
					meltxargdescr_[],
					union meltparam_un *meltxargtab_,
					const melt_argdescr_cell_t
					meltxresdescr_[],
					union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_8_warmelt_normal_NORMEXP_ANY_OBJECT (meltclosure_ptr_t meltclosp_,
					      melt_ptr_t meltfirstargp_,
					      const melt_argdescr_cell_t
					      meltxargdescr_[],
					      union meltparam_un
					      *meltxargtab_,
					      const melt_argdescr_cell_t
					      meltxresdescr_[],
					      union meltparam_un
					      *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_9_warmelt_normal_NORMEXP_ANY_VALUE (meltclosure_ptr_t meltclosp_,
					     melt_ptr_t meltfirstargp_,
					     const melt_argdescr_cell_t
					     meltxargdescr_[],
					     union meltparam_un *meltxargtab_,
					     const melt_argdescr_cell_t
					     meltxresdescr_[],
					     union meltparam_un
					     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_10_warmelt_normal_NORMEXP_SRC_CATCHALL (meltclosure_ptr_t meltclosp_,
						 melt_ptr_t meltfirstargp_,
						 const melt_argdescr_cell_t
						 meltxargdescr_[],
						 union meltparam_un
						 *meltxargtab_,
						 const melt_argdescr_cell_t
						 meltxresdescr_[],
						 union meltparam_un
						 *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_11_warmelt_normal_NORMEXP_LAZYMACROEXP (meltclosure_ptr_t meltclosp_,
						 melt_ptr_t meltfirstargp_,
						 const melt_argdescr_cell_t
						 meltxargdescr_[],
						 union meltparam_un
						 *meltxargtab_,
						 const melt_argdescr_cell_t
						 meltxresdescr_[],
						 union meltparam_un
						 *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_12_warmelt_normal_GECTYP_ANYRECV (meltclosure_ptr_t meltclosp_,
					   melt_ptr_t meltfirstargp_,
					   const melt_argdescr_cell_t
					   meltxargdescr_[],
					   union meltparam_un *meltxargtab_,
					   const melt_argdescr_cell_t
					   meltxresdescr_[],
					   union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_13_warmelt_normal_GECTYP_ROOT (meltclosure_ptr_t meltclosp_,
					melt_ptr_t meltfirstargp_,
					const melt_argdescr_cell_t
					meltxargdescr_[],
					union meltparam_un *meltxargtab_,
					const melt_argdescr_cell_t
					meltxresdescr_[],
					union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_14_warmelt_normal_GECTYP_INTEGER (meltclosure_ptr_t meltclosp_,
					   melt_ptr_t meltfirstargp_,
					   const melt_argdescr_cell_t
					   meltxargdescr_[],
					   union meltparam_un *meltxargtab_,
					   const melt_argdescr_cell_t
					   meltxresdescr_[],
					   union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_15_warmelt_normal_GECTYP_STRING (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_16_warmelt_normal_NORMALIZE_TUPLE (meltclosure_ptr_t meltclosp_,
					    melt_ptr_t meltfirstargp_,
					    const melt_argdescr_cell_t
					    meltxargdescr_[],
					    union meltparam_un *meltxargtab_,
					    const melt_argdescr_cell_t
					    meltxresdescr_[],
					    union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_17_warmelt_normal_LAMBDA___1__ (meltclosure_ptr_t meltclosp_,
					 melt_ptr_t meltfirstargp_,
					 const melt_argdescr_cell_t
					 meltxargdescr_[],
					 union meltparam_un *meltxargtab_,
					 const melt_argdescr_cell_t
					 meltxresdescr_[],
					 union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_18_warmelt_normal_LAMBDA___2__ (meltclosure_ptr_t meltclosp_,
					 melt_ptr_t meltfirstargp_,
					 const melt_argdescr_cell_t
					 meltxargdescr_[],
					 union meltparam_un *meltxargtab_,
					 const melt_argdescr_cell_t
					 meltxresdescr_[],
					 union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_19_warmelt_normal_WRAP_NORMAL_LET1 (meltclosure_ptr_t meltclosp_,
					     melt_ptr_t meltfirstargp_,
					     const melt_argdescr_cell_t
					     meltxargdescr_[],
					     union meltparam_un *meltxargtab_,
					     const melt_argdescr_cell_t
					     meltxresdescr_[],
					     union meltparam_un
					     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_20_warmelt_normal_LAMBDA___3__ (meltclosure_ptr_t meltclosp_,
					 melt_ptr_t meltfirstargp_,
					 const melt_argdescr_cell_t
					 meltxargdescr_[],
					 union meltparam_un *meltxargtab_,
					 const melt_argdescr_cell_t
					 meltxresdescr_[],
					 union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_21_warmelt_normal_WRAP_NORMAL_LETSEQ (meltclosure_ptr_t meltclosp_,
					       melt_ptr_t meltfirstargp_,
					       const melt_argdescr_cell_t
					       meltxargdescr_[],
					       union meltparam_un
					       *meltxargtab_,
					       const melt_argdescr_cell_t
					       meltxresdescr_[],
					       union meltparam_un
					       *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_22_warmelt_normal_LAMBDA___4__ (meltclosure_ptr_t meltclosp_,
					 melt_ptr_t meltfirstargp_,
					 const melt_argdescr_cell_t
					 meltxargdescr_[],
					 union meltparam_un *meltxargtab_,
					 const melt_argdescr_cell_t
					 meltxresdescr_[],
					 union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_23_warmelt_normal_CHECK_CTYPE_NARGS (meltclosure_ptr_t meltclosp_,
					      melt_ptr_t meltfirstargp_,
					      const melt_argdescr_cell_t
					      meltxargdescr_[],
					      union meltparam_un
					      *meltxargtab_,
					      const melt_argdescr_cell_t
					      meltxresdescr_[],
					      union meltparam_un
					      *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_24_warmelt_normal_LAMBDA___5__ (meltclosure_ptr_t meltclosp_,
					 melt_ptr_t meltfirstargp_,
					 const melt_argdescr_cell_t
					 meltxargdescr_[],
					 union meltparam_un *meltxargtab_,
					 const melt_argdescr_cell_t
					 meltxresdescr_[],
					 union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_25_warmelt_normal_NORMBIND_FAILANY (meltclosure_ptr_t meltclosp_,
					     melt_ptr_t meltfirstargp_,
					     const melt_argdescr_cell_t
					     meltxargdescr_[],
					     union meltparam_un *meltxargtab_,
					     const melt_argdescr_cell_t
					     meltxresdescr_[],
					     union meltparam_un
					     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_26_warmelt_normal_NORMBIND_ANYBIND (meltclosure_ptr_t meltclosp_,
					     melt_ptr_t meltfirstargp_,
					     const melt_argdescr_cell_t
					     meltxargdescr_[],
					     union meltparam_un *meltxargtab_,
					     const melt_argdescr_cell_t
					     meltxresdescr_[],
					     union meltparam_un
					     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_27_warmelt_normal_NORMBIND_FORMALBIND (meltclosure_ptr_t meltclosp_,
						melt_ptr_t meltfirstargp_,
						const melt_argdescr_cell_t
						meltxargdescr_[],
						union meltparam_un
						*meltxargtab_,
						const melt_argdescr_cell_t
						meltxresdescr_[],
						union meltparam_un
						*meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_28_warmelt_normal_NORMBIND_LETBIND (meltclosure_ptr_t meltclosp_,
					     melt_ptr_t meltfirstargp_,
					     const melt_argdescr_cell_t
					     meltxargdescr_[],
					     union meltparam_un *meltxargtab_,
					     const melt_argdescr_cell_t
					     meltxresdescr_[],
					     union meltparam_un
					     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_29_warmelt_normal_NORMBIND_FIXBIND (meltclosure_ptr_t meltclosp_,
					     melt_ptr_t meltfirstargp_,
					     const melt_argdescr_cell_t
					     meltxargdescr_[],
					     union meltparam_un *meltxargtab_,
					     const melt_argdescr_cell_t
					     meltxresdescr_[],
					     union meltparam_un
					     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_30_warmelt_normal_NORMBIND_DEFINEDVALBIND (meltclosure_ptr_t
						    meltclosp_,
						    melt_ptr_t meltfirstargp_,
						    const melt_argdescr_cell_t
						    meltxargdescr_[],
						    union meltparam_un
						    *meltxargtab_,
						    const melt_argdescr_cell_t
						    meltxresdescr_[],
						    union meltparam_un
						    *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_31_warmelt_normal_NORMBIND_CONSTRUCTBIND (meltclosure_ptr_t
						   meltclosp_,
						   melt_ptr_t meltfirstargp_,
						   const melt_argdescr_cell_t
						   meltxargdescr_[],
						   union meltparam_un
						   *meltxargtab_,
						   const melt_argdescr_cell_t
						   meltxresdescr_[],
						   union meltparam_un
						   *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_32_warmelt_normal_NORMEXP_SYMBOL (meltclosure_ptr_t meltclosp_,
					   melt_ptr_t meltfirstargp_,
					   const melt_argdescr_cell_t
					   meltxargdescr_[],
					   union meltparam_un *meltxargtab_,
					   const melt_argdescr_cell_t
					   meltxresdescr_[],
					   union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_33_warmelt_normal_LAMBDA___6__ (meltclosure_ptr_t meltclosp_,
					 melt_ptr_t meltfirstargp_,
					 const melt_argdescr_cell_t
					 meltxargdescr_[],
					 union meltparam_un *meltxargtab_,
					 const melt_argdescr_cell_t
					 meltxresdescr_[],
					 union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_34_warmelt_normal_LAMBDA___7__ (meltclosure_ptr_t meltclosp_,
					 melt_ptr_t meltfirstargp_,
					 const melt_argdescr_cell_t
					 meltxargdescr_[],
					 union meltparam_un *meltxargtab_,
					 const melt_argdescr_cell_t
					 meltxresdescr_[],
					 union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_35_warmelt_normal_LAMBDA___8__ (meltclosure_ptr_t meltclosp_,
					 melt_ptr_t meltfirstargp_,
					 const melt_argdescr_cell_t
					 meltxargdescr_[],
					 union meltparam_un *meltxargtab_,
					 const melt_argdescr_cell_t
					 meltxresdescr_[],
					 union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_36_warmelt_normal_LAMBDA___9__ (meltclosure_ptr_t meltclosp_,
					 melt_ptr_t meltfirstargp_,
					 const melt_argdescr_cell_t
					 meltxargdescr_[],
					 union meltparam_un *meltxargtab_,
					 const melt_argdescr_cell_t
					 meltxresdescr_[],
					 union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_37_warmelt_normal_LAMBDA___10__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_38_warmelt_normal_LAMBDA___11__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_39_warmelt_normal_GECTYP_SYMOCC (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_40_warmelt_normal_NORMEXP_CLASS (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_41_warmelt_normal_NORMEXP_PRIMITIVE (meltclosure_ptr_t meltclosp_,
					      melt_ptr_t meltfirstargp_,
					      const melt_argdescr_cell_t
					      meltxargdescr_[],
					      union meltparam_un
					      *meltxargtab_,
					      const melt_argdescr_cell_t
					      meltxresdescr_[],
					      union meltparam_un
					      *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_42_warmelt_normal_LAMBDA___12__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_43_warmelt_normal_LAMBDA___13__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_44_warmelt_normal_NORMEXP_CODE_CHUNK (meltclosure_ptr_t meltclosp_,
					       melt_ptr_t meltfirstargp_,
					       const melt_argdescr_cell_t
					       meltxargdescr_[],
					       union meltparam_un
					       *meltxargtab_,
					       const melt_argdescr_cell_t
					       meltxresdescr_[],
					       union meltparam_un
					       *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_45_warmelt_normal_LAMBDA___14__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_46_warmelt_normal_NORMEXP_CMATCHEXPR (meltclosure_ptr_t meltclosp_,
					       melt_ptr_t meltfirstargp_,
					       const melt_argdescr_cell_t
					       meltxargdescr_[],
					       union meltparam_un
					       *meltxargtab_,
					       const melt_argdescr_cell_t
					       meltxresdescr_[],
					       union meltparam_un
					       *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_47_warmelt_normal_LAMBDA___15__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_48_warmelt_normal_LAMBDA___16__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_49_warmelt_normal_NORMEXP_FUNMATCHEXPR (meltclosure_ptr_t meltclosp_,
						 melt_ptr_t meltfirstargp_,
						 const melt_argdescr_cell_t
						 meltxargdescr_[],
						 union meltparam_un
						 *meltxargtab_,
						 const melt_argdescr_cell_t
						 meltxresdescr_[],
						 union meltparam_un
						 *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_50_warmelt_normal_NORMEXP_APPLY (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_51_warmelt_normal_NORMEXP_MSEND (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_52_warmelt_normal_NORMEXP_RETURN (meltclosure_ptr_t meltclosp_,
					   melt_ptr_t meltfirstargp_,
					   const melt_argdescr_cell_t
					   meltxargdescr_[],
					   union meltparam_un *meltxargtab_,
					   const melt_argdescr_cell_t
					   meltxresdescr_[],
					   union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_53_warmelt_normal_NORMEXP_IF (meltclosure_ptr_t meltclosp_,
				       melt_ptr_t meltfirstargp_,
				       const melt_argdescr_cell_t
				       meltxargdescr_[],
				       union meltparam_un *meltxargtab_,
				       const melt_argdescr_cell_t
				       meltxresdescr_[],
				       union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_54_warmelt_normal_LAMBDA___17__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_55_warmelt_normal_NORMEXP_IFELSE (meltclosure_ptr_t meltclosp_,
					   melt_ptr_t meltfirstargp_,
					   const melt_argdescr_cell_t
					   meltxargdescr_[],
					   union meltparam_un *meltxargtab_,
					   const melt_argdescr_cell_t
					   meltxresdescr_[],
					   union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_56_warmelt_normal_LAMBDA___18__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_57_warmelt_normal_LAMBDA___19__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_58_warmelt_normal_NORMEXP_CPPIF (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_59_warmelt_normal_LAMBDA___20__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_60_warmelt_normal_LAMBDA___21__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_61_warmelt_normal_NORMEXP_OR (meltclosure_ptr_t meltclosp_,
				       melt_ptr_t meltfirstargp_,
				       const melt_argdescr_cell_t
				       meltxargdescr_[],
				       union meltparam_un *meltxargtab_,
				       const melt_argdescr_cell_t
				       meltxresdescr_[],
				       union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_62_warmelt_normal_LAMBDA___22__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_63_warmelt_normal_LAMBDA___23__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_64_warmelt_normal_NORMEXP_PROGN (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_65_warmelt_normal_LAMBDA___24__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_66_warmelt_normal_NORMEXP_LET (meltclosure_ptr_t meltclosp_,
					melt_ptr_t meltfirstargp_,
					const melt_argdescr_cell_t
					meltxargdescr_[],
					union meltparam_un *meltxargtab_,
					const melt_argdescr_cell_t
					meltxresdescr_[],
					union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_67_warmelt_normal_LAMBDA___25__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_68_warmelt_normal_LAMBDA___26__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_69_warmelt_normal_LAMBDA___27__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_70_warmelt_normal_NORMEXP_UNSAFE_GET_FIELD (meltclosure_ptr_t
						     meltclosp_,
						     melt_ptr_t
						     meltfirstargp_,
						     const
						     melt_argdescr_cell_t
						     meltxargdescr_[],
						     union meltparam_un
						     *meltxargtab_,
						     const
						     melt_argdescr_cell_t
						     meltxresdescr_[],
						     union meltparam_un
						     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_71_warmelt_normal_NORMEXP_GET_FIELD (meltclosure_ptr_t meltclosp_,
					      melt_ptr_t meltfirstargp_,
					      const melt_argdescr_cell_t
					      meltxargdescr_[],
					      union meltparam_un
					      *meltxargtab_,
					      const melt_argdescr_cell_t
					      meltxresdescr_[],
					      union meltparam_un
					      *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_72_warmelt_normal_NORMEXP_UNSAFE_PUT_FIELDS (meltclosure_ptr_t
						      meltclosp_,
						      melt_ptr_t
						      meltfirstargp_,
						      const
						      melt_argdescr_cell_t
						      meltxargdescr_[],
						      union meltparam_un
						      *meltxargtab_,
						      const
						      melt_argdescr_cell_t
						      meltxresdescr_[],
						      union meltparam_un
						      *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_73_warmelt_normal_LAMBDA___28__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_74_warmelt_normal_LAMBDA___29__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_75_warmelt_normal_NORMEXP_PUT_FIELDS (meltclosure_ptr_t meltclosp_,
					       melt_ptr_t meltfirstargp_,
					       const melt_argdescr_cell_t
					       meltxargdescr_[],
					       union meltparam_un
					       *meltxargtab_,
					       const melt_argdescr_cell_t
					       meltxresdescr_[],
					       union meltparam_un
					       *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_76_warmelt_normal_NORMEXP_SETQ (meltclosure_ptr_t meltclosp_,
					 melt_ptr_t meltfirstargp_,
					 const melt_argdescr_cell_t
					 meltxargdescr_[],
					 union meltparam_un *meltxargtab_,
					 const melt_argdescr_cell_t
					 meltxresdescr_[],
					 union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_77_warmelt_normal_LAMBDA___30__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_78_warmelt_normal_NORMEXP_INSTANCE (meltclosure_ptr_t meltclosp_,
					     melt_ptr_t meltfirstargp_,
					     const melt_argdescr_cell_t
					     meltxargdescr_[],
					     union meltparam_un *meltxargtab_,
					     const melt_argdescr_cell_t
					     meltxresdescr_[],
					     union meltparam_un
					     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_79_warmelt_normal_LAMBDA___31__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_80_warmelt_normal_LAMBDA___32__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_81_warmelt_normal_NORMEXP_FOREVER (meltclosure_ptr_t meltclosp_,
					    melt_ptr_t meltfirstargp_,
					    const melt_argdescr_cell_t
					    meltxargdescr_[],
					    union meltparam_un *meltxargtab_,
					    const melt_argdescr_cell_t
					    meltxresdescr_[],
					    union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_82_warmelt_normal_NORMEXP_EXIT (meltclosure_ptr_t meltclosp_,
					 melt_ptr_t meltfirstargp_,
					 const melt_argdescr_cell_t
					 meltxargdescr_[],
					 union meltparam_un *meltxargtab_,
					 const melt_argdescr_cell_t
					 meltxresdescr_[],
					 union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_83_warmelt_normal_NORMEXP_AGAIN (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_84_warmelt_normal_NORMEXP_IFVARIADIC (meltclosure_ptr_t meltclosp_,
					       melt_ptr_t meltfirstargp_,
					       const melt_argdescr_cell_t
					       meltxargdescr_[],
					       union meltparam_un
					       *meltxargtab_,
					       const melt_argdescr_cell_t
					       meltxresdescr_[],
					       union meltparam_un
					       *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_85_warmelt_normal_NORMEXP_COMPILEWARNING (meltclosure_ptr_t
						   meltclosp_,
						   melt_ptr_t meltfirstargp_,
						   const melt_argdescr_cell_t
						   meltxargdescr_[],
						   union meltparam_un
						   *meltxargtab_,
						   const melt_argdescr_cell_t
						   meltxresdescr_[],
						   union meltparam_un
						   *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_86_warmelt_normal_LAMBDA___33__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_87_warmelt_normal_REPLACE_LAST_BY_RETURN (meltclosure_ptr_t
						   meltclosp_,
						   melt_ptr_t meltfirstargp_,
						   const melt_argdescr_cell_t
						   meltxargdescr_[],
						   union meltparam_un
						   *meltxargtab_,
						   const melt_argdescr_cell_t
						   meltxresdescr_[],
						   union meltparam_un
						   *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_88_warmelt_normal_LAMBDA___34__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_89_warmelt_normal_NORMEXP_DEFUN (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_90_warmelt_normal_LAMBDA___35__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_91_warmelt_normal_LAMBDA___36__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_92_warmelt_normal_NORMALIZE_LAMBDA (meltclosure_ptr_t meltclosp_,
					     melt_ptr_t meltfirstargp_,
					     const melt_argdescr_cell_t
					     meltxargdescr_[],
					     union meltparam_un *meltxargtab_,
					     const melt_argdescr_cell_t
					     meltxresdescr_[],
					     union meltparam_un
					     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_93_warmelt_normal_LAMBDA___37__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_94_warmelt_normal_NORMEXP_LAMBDA (meltclosure_ptr_t meltclosp_,
					   melt_ptr_t meltfirstargp_,
					   const melt_argdescr_cell_t
					   meltxargdescr_[],
					   union meltparam_un *meltxargtab_,
					   const melt_argdescr_cell_t
					   meltxresdescr_[],
					   union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_95_warmelt_normal_NORMEXP_MULTICALL (meltclosure_ptr_t meltclosp_,
					      melt_ptr_t meltfirstargp_,
					      const melt_argdescr_cell_t
					      meltxargdescr_[],
					      union meltparam_un
					      *meltxargtab_,
					      const melt_argdescr_cell_t
					      meltxresdescr_[],
					      union meltparam_un
					      *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_96_warmelt_normal_LAMBDA___38__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_97_warmelt_normal_LAMBDA___39__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_98_warmelt_normal_LAMBDA___40__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_99_warmelt_normal_NORMEXP_TUPLE (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_100_warmelt_normal_NORMEXP_LIST (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_101_warmelt_normal_LAMBDA___41__ (meltclosure_ptr_t meltclosp_,
					   melt_ptr_t meltfirstargp_,
					   const melt_argdescr_cell_t
					   meltxargdescr_[],
					   union meltparam_un *meltxargtab_,
					   const melt_argdescr_cell_t
					   meltxresdescr_[],
					   union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_102_warmelt_normal_BADMETH_PREPARE_CONSTRUCTOR_BINDING
(meltclosure_ptr_t meltclosp_, melt_ptr_t meltfirstargp_, const melt_argdescr_cell_t meltxargdescr_[],
union meltparam_un *meltxargtab_, const melt_argdescr_cell_t meltxresdescr_[],
union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_103_warmelt_normal_BADMETH_NORMAL_LETREC_CONSTRUCTIVE
(meltclosure_ptr_t meltclosp_, melt_ptr_t meltfirstargp_, const melt_argdescr_cell_t meltxargdescr_[],
union meltparam_un *meltxargtab_, const melt_argdescr_cell_t meltxresdescr_[],
union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_104_warmelt_normal_PREPCONS_LAMBDA (meltclosure_ptr_t meltclosp_,
					     melt_ptr_t meltfirstargp_,
					     const melt_argdescr_cell_t
					     meltxargdescr_[],
					     union meltparam_un *meltxargtab_,
					     const melt_argdescr_cell_t
					     meltxresdescr_[],
					     union meltparam_un
					     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_105_warmelt_normal_NORMLETREC_LAMBDA (meltclosure_ptr_t meltclosp_,
					       melt_ptr_t meltfirstargp_,
					       const melt_argdescr_cell_t
					       meltxargdescr_[],
					       union meltparam_un
					       *meltxargtab_,
					       const melt_argdescr_cell_t
					       meltxresdescr_[],
					       union meltparam_un
					       *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_106_warmelt_normal_PREPCONS_TUPLE (meltclosure_ptr_t meltclosp_,
					    melt_ptr_t meltfirstargp_,
					    const melt_argdescr_cell_t
					    meltxargdescr_[],
					    union meltparam_un *meltxargtab_,
					    const melt_argdescr_cell_t
					    meltxresdescr_[],
					    union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_107_warmelt_normal_NORMLETREC_TUPLE (meltclosure_ptr_t meltclosp_,
					      melt_ptr_t meltfirstargp_,
					      const melt_argdescr_cell_t
					      meltxargdescr_[],
					      union meltparam_un
					      *meltxargtab_,
					      const melt_argdescr_cell_t
					      meltxresdescr_[],
					      union meltparam_un
					      *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_108_warmelt_normal_PREPCONS_LIST (meltclosure_ptr_t meltclosp_,
					   melt_ptr_t meltfirstargp_,
					   const melt_argdescr_cell_t
					   meltxargdescr_[],
					   union meltparam_un *meltxargtab_,
					   const melt_argdescr_cell_t
					   meltxresdescr_[],
					   union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_109_warmelt_normal_NRECLIST_FIND_LOCSYM (meltclosure_ptr_t
						  meltclosp_,
						  melt_ptr_t meltfirstargp_,
						  const melt_argdescr_cell_t
						  meltxargdescr_[],
						  union meltparam_un
						  *meltxargtab_,
						  const melt_argdescr_cell_t
						  meltxresdescr_[],
						  union meltparam_un
						  *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_110_warmelt_normal_NORMLETREC_LIST (meltclosure_ptr_t meltclosp_,
					     melt_ptr_t meltfirstargp_,
					     const melt_argdescr_cell_t
					     meltxargdescr_[],
					     union meltparam_un *meltxargtab_,
					     const melt_argdescr_cell_t
					     meltxresdescr_[],
					     union meltparam_un
					     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_111_warmelt_normal_PREPCONS_INSTANCE (meltclosure_ptr_t meltclosp_,
					       melt_ptr_t meltfirstargp_,
					       const melt_argdescr_cell_t
					       meltxargdescr_[],
					       union meltparam_un
					       *meltxargtab_,
					       const melt_argdescr_cell_t
					       meltxresdescr_[],
					       union meltparam_un
					       *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_112_warmelt_normal_NORMLETREC_INSTANCE (meltclosure_ptr_t meltclosp_,
						 melt_ptr_t meltfirstargp_,
						 const melt_argdescr_cell_t
						 meltxargdescr_[],
						 union meltparam_un
						 *meltxargtab_,
						 const melt_argdescr_cell_t
						 meltxresdescr_[],
						 union meltparam_un
						 *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_113_warmelt_normal_LAMBDA___42__ (meltclosure_ptr_t meltclosp_,
					   melt_ptr_t meltfirstargp_,
					   const melt_argdescr_cell_t
					   meltxargdescr_[],
					   union meltparam_un *meltxargtab_,
					   const melt_argdescr_cell_t
					   meltxresdescr_[],
					   union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_114_warmelt_normal_NORMEXP_LETREC (meltclosure_ptr_t meltclosp_,
					    melt_ptr_t meltfirstargp_,
					    const melt_argdescr_cell_t
					    meltxargdescr_[],
					    union meltparam_un *meltxargtab_,
					    const melt_argdescr_cell_t
					    meltxresdescr_[],
					    union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_115_warmelt_normal_NORMAL_PREDEF (meltclosure_ptr_t meltclosp_,
					   melt_ptr_t meltfirstargp_,
					   const melt_argdescr_cell_t
					   meltxargdescr_[],
					   union meltparam_un *meltxargtab_,
					   const melt_argdescr_cell_t
					   meltxresdescr_[],
					   union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_116_warmelt_normal_NORMAL_SYMBOL_DATA (meltclosure_ptr_t meltclosp_,
						melt_ptr_t meltfirstargp_,
						const melt_argdescr_cell_t
						meltxargdescr_[],
						union meltparam_un
						*meltxargtab_,
						const melt_argdescr_cell_t
						meltxresdescr_[],
						union meltparam_un
						*meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_117_warmelt_normal_NORMAL_KEYWORD_DATA (meltclosure_ptr_t meltclosp_,
						 melt_ptr_t meltfirstargp_,
						 const melt_argdescr_cell_t
						 meltxargdescr_[],
						 union meltparam_un
						 *meltxargtab_,
						 const melt_argdescr_cell_t
						 meltxresdescr_[],
						 union meltparam_un
						 *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_118_warmelt_normal_CREATE_DATA_SLOTS (meltclosure_ptr_t meltclosp_,
					       melt_ptr_t meltfirstargp_,
					       const melt_argdescr_cell_t
					       meltxargdescr_[],
					       union meltparam_un
					       *meltxargtab_,
					       const melt_argdescr_cell_t
					       meltxresdescr_[],
					       union meltparam_un
					       *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_119_warmelt_normal_FILL_DATA_SLOT (meltclosure_ptr_t meltclosp_,
					    melt_ptr_t meltfirstargp_,
					    const melt_argdescr_cell_t
					    meltxargdescr_[],
					    union meltparam_un *meltxargtab_,
					    const melt_argdescr_cell_t
					    meltxresdescr_[],
					    union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_120_warmelt_normal_NORMEXP_QUOTE (meltclosure_ptr_t meltclosp_,
					   melt_ptr_t meltfirstargp_,
					   const melt_argdescr_cell_t
					   meltxargdescr_[],
					   union meltparam_un *meltxargtab_,
					   const melt_argdescr_cell_t
					   meltxresdescr_[],
					   union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_121_warmelt_normal_NORMEXP_COMMENT (meltclosure_ptr_t meltclosp_,
					     melt_ptr_t meltfirstargp_,
					     const melt_argdescr_cell_t
					     meltxargdescr_[],
					     union meltparam_un *meltxargtab_,
					     const melt_argdescr_cell_t
					     meltxresdescr_[],
					     union meltparam_un
					     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_122_warmelt_normal_NORMEXP_KEYWORD (meltclosure_ptr_t meltclosp_,
					     melt_ptr_t meltfirstargp_,
					     const melt_argdescr_cell_t
					     meltxargdescr_[],
					     union meltparam_un *meltxargtab_,
					     const melt_argdescr_cell_t
					     meltxresdescr_[],
					     union meltparam_un
					     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_123_warmelt_normal_FILL_NORMAL_FORMALBIND (meltclosure_ptr_t
						    meltclosp_,
						    melt_ptr_t meltfirstargp_,
						    const melt_argdescr_cell_t
						    meltxargdescr_[],
						    union meltparam_un
						    *meltxargtab_,
						    const melt_argdescr_cell_t
						    meltxresdescr_[],
						    union meltparam_un
						    *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_124_warmelt_normal_FILL_NORMAL_FORMALS (meltclosure_ptr_t meltclosp_,
						 melt_ptr_t meltfirstargp_,
						 const melt_argdescr_cell_t
						 meltxargdescr_[],
						 union meltparam_un
						 *meltxargtab_,
						 const melt_argdescr_cell_t
						 meltxresdescr_[],
						 union meltparam_un
						 *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_125_warmelt_normal_FILL_NORMAL_EXPANSION (meltclosure_ptr_t
						   meltclosp_,
						   melt_ptr_t meltfirstargp_,
						   const melt_argdescr_cell_t
						   meltxargdescr_[],
						   union meltparam_un
						   *meltxargtab_,
						   const melt_argdescr_cell_t
						   meltxresdescr_[],
						   union meltparam_un
						   *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_126_warmelt_normal_LAMBDA___43__ (meltclosure_ptr_t meltclosp_,
					   melt_ptr_t meltfirstargp_,
					   const melt_argdescr_cell_t
					   meltxargdescr_[],
					   union meltparam_un *meltxargtab_,
					   const melt_argdescr_cell_t
					   meltxresdescr_[],
					   union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_127_warmelt_normal_NORMEXP_DEFPRIMITIVE (meltclosure_ptr_t
						  meltclosp_,
						  melt_ptr_t meltfirstargp_,
						  const melt_argdescr_cell_t
						  meltxargdescr_[],
						  union meltparam_un
						  *meltxargtab_,
						  const melt_argdescr_cell_t
						  meltxresdescr_[],
						  union meltparam_un
						  *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_128_warmelt_normal_NORMEXP_DEFCITERATOR (meltclosure_ptr_t
						  meltclosp_,
						  melt_ptr_t meltfirstargp_,
						  const melt_argdescr_cell_t
						  meltxargdescr_[],
						  union meltparam_un
						  *meltxargtab_,
						  const melt_argdescr_cell_t
						  meltxresdescr_[],
						  union meltparam_un
						  *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_129_warmelt_normal_NORMEXP_CITERATION (meltclosure_ptr_t meltclosp_,
						melt_ptr_t meltfirstargp_,
						const melt_argdescr_cell_t
						meltxargdescr_[],
						union meltparam_un
						*meltxargtab_,
						const melt_argdescr_cell_t
						meltxresdescr_[],
						union meltparam_un
						*meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_130_warmelt_normal_LAMBDA___44__ (meltclosure_ptr_t meltclosp_,
					   melt_ptr_t meltfirstargp_,
					   const melt_argdescr_cell_t
					   meltxargdescr_[],
					   union meltparam_un *meltxargtab_,
					   const melt_argdescr_cell_t
					   meltxresdescr_[],
					   union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_131_warmelt_normal_LAMBDA___45__ (meltclosure_ptr_t meltclosp_,
					   melt_ptr_t meltfirstargp_,
					   const melt_argdescr_cell_t
					   meltxargdescr_[],
					   union meltparam_un *meltxargtab_,
					   const melt_argdescr_cell_t
					   meltxresdescr_[],
					   union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_132_warmelt_normal_LAMBDA___46__ (meltclosure_ptr_t meltclosp_,
					   melt_ptr_t meltfirstargp_,
					   const melt_argdescr_cell_t
					   meltxargdescr_[],
					   union meltparam_un *meltxargtab_,
					   const melt_argdescr_cell_t
					   meltxresdescr_[],
					   union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_133_warmelt_normal_LAMBDA___47__ (meltclosure_ptr_t meltclosp_,
					   melt_ptr_t meltfirstargp_,
					   const melt_argdescr_cell_t
					   meltxargdescr_[],
					   union meltparam_un *meltxargtab_,
					   const melt_argdescr_cell_t
					   meltxresdescr_[],
					   union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_134_warmelt_normal_LAMBDA___48__ (meltclosure_ptr_t meltclosp_,
					   melt_ptr_t meltfirstargp_,
					   const melt_argdescr_cell_t
					   meltxargdescr_[],
					   union meltparam_un *meltxargtab_,
					   const melt_argdescr_cell_t
					   meltxresdescr_[],
					   union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_135_warmelt_normal_LAMBDA___49__ (meltclosure_ptr_t meltclosp_,
					   melt_ptr_t meltfirstargp_,
					   const melt_argdescr_cell_t
					   meltxargdescr_[],
					   union meltparam_un *meltxargtab_,
					   const melt_argdescr_cell_t
					   meltxresdescr_[],
					   union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_136_warmelt_normal_LAMBDA___50__ (meltclosure_ptr_t meltclosp_,
					   melt_ptr_t meltfirstargp_,
					   const melt_argdescr_cell_t
					   meltxargdescr_[],
					   union meltparam_un *meltxargtab_,
					   const melt_argdescr_cell_t
					   meltxresdescr_[],
					   union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_137_warmelt_normal_NORMEXP_DEFCMATCHER (meltclosure_ptr_t meltclosp_,
						 melt_ptr_t meltfirstargp_,
						 const melt_argdescr_cell_t
						 meltxargdescr_[],
						 union meltparam_un
						 *meltxargtab_,
						 const melt_argdescr_cell_t
						 meltxresdescr_[],
						 union meltparam_un
						 *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_138_warmelt_normal_NORMEXP_DEFUNMATCHER (meltclosure_ptr_t
						  meltclosp_,
						  melt_ptr_t meltfirstargp_,
						  const melt_argdescr_cell_t
						  meltxargdescr_[],
						  union meltparam_un
						  *meltxargtab_,
						  const melt_argdescr_cell_t
						  meltxresdescr_[],
						  union meltparam_un
						  *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_139_warmelt_normal_NORMEXP_DEFCLASS (meltclosure_ptr_t meltclosp_,
					      melt_ptr_t meltfirstargp_,
					      const melt_argdescr_cell_t
					      meltxargdescr_[],
					      union meltparam_un
					      *meltxargtab_,
					      const melt_argdescr_cell_t
					      meltxresdescr_[],
					      union meltparam_un
					      *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_140_warmelt_normal_NORMEXP_DEFINSTANCE (meltclosure_ptr_t meltclosp_,
						 melt_ptr_t meltfirstargp_,
						 const melt_argdescr_cell_t
						 meltxargdescr_[],
						 union meltparam_un
						 *meltxargtab_,
						 const melt_argdescr_cell_t
						 meltxresdescr_[],
						 union meltparam_un
						 *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_141_warmelt_normal_LAMBDA___51__ (meltclosure_ptr_t meltclosp_,
					   melt_ptr_t meltfirstargp_,
					   const melt_argdescr_cell_t
					   meltxargdescr_[],
					   union meltparam_un *meltxargtab_,
					   const melt_argdescr_cell_t
					   meltxresdescr_[],
					   union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_142_warmelt_normal_NORMEXP_DEFINE (meltclosure_ptr_t meltclosp_,
					    melt_ptr_t meltfirstargp_,
					    const melt_argdescr_cell_t
					    meltxargdescr_[],
					    union meltparam_un *meltxargtab_,
					    const melt_argdescr_cell_t
					    meltxresdescr_[],
					    union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_143_warmelt_normal_LAMBDA___52__ (meltclosure_ptr_t meltclosp_,
					   melt_ptr_t meltfirstargp_,
					   const melt_argdescr_cell_t
					   meltxargdescr_[],
					   union meltparam_un *meltxargtab_,
					   const melt_argdescr_cell_t
					   meltxresdescr_[],
					   union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_144_warmelt_normal_NORMEXP_DEFSELECTOR (meltclosure_ptr_t meltclosp_,
						 melt_ptr_t meltfirstargp_,
						 const melt_argdescr_cell_t
						 meltxargdescr_[],
						 union meltparam_un
						 *meltxargtab_,
						 const melt_argdescr_cell_t
						 meltxresdescr_[],
						 union meltparam_un
						 *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_145_warmelt_normal_LAMBDA___53__ (meltclosure_ptr_t meltclosp_,
					   melt_ptr_t meltfirstargp_,
					   const melt_argdescr_cell_t
					   meltxargdescr_[],
					   union meltparam_un *meltxargtab_,
					   const melt_argdescr_cell_t
					   meltxresdescr_[],
					   union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_146_warmelt_normal_NORMAL_VALUE_EXPORTER (meltclosure_ptr_t
						   meltclosp_,
						   melt_ptr_t meltfirstargp_,
						   const melt_argdescr_cell_t
						   meltxargdescr_[],
						   union meltparam_un
						   *meltxargtab_,
						   const melt_argdescr_cell_t
						   meltxresdescr_[],
						   union meltparam_un
						   *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_147_warmelt_normal_NORMAL_EXPORTED_VALUE (meltclosure_ptr_t
						   meltclosp_,
						   melt_ptr_t meltfirstargp_,
						   const melt_argdescr_cell_t
						   meltxargdescr_[],
						   union meltparam_un
						   *meltxargtab_,
						   const melt_argdescr_cell_t
						   meltxresdescr_[],
						   union meltparam_un
						   *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_148_warmelt_normal_NORMEXP_EXPORT_VALUES (meltclosure_ptr_t
						   meltclosp_,
						   melt_ptr_t meltfirstargp_,
						   const melt_argdescr_cell_t
						   meltxargdescr_[],
						   union meltparam_un
						   *meltxargtab_,
						   const melt_argdescr_cell_t
						   meltxresdescr_[],
						   union meltparam_un
						   *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_149_warmelt_normal_NORMEXP_EXPORT_SYNONYM (meltclosure_ptr_t
						    meltclosp_,
						    melt_ptr_t meltfirstargp_,
						    const melt_argdescr_cell_t
						    meltxargdescr_[],
						    union meltparam_un
						    *meltxargtab_,
						    const melt_argdescr_cell_t
						    meltxresdescr_[],
						    union meltparam_un
						    *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_150_warmelt_normal_NORMEXP_EXPORT_CLASS (meltclosure_ptr_t
						  meltclosp_,
						  melt_ptr_t meltfirstargp_,
						  const melt_argdescr_cell_t
						  meltxargdescr_[],
						  union meltparam_un
						  *meltxargtab_,
						  const melt_argdescr_cell_t
						  meltxresdescr_[],
						  union meltparam_un
						  *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_151_warmelt_normal_LAMBDA___54__ (meltclosure_ptr_t meltclosp_,
					   melt_ptr_t meltfirstargp_,
					   const melt_argdescr_cell_t
					   meltxargdescr_[],
					   union meltparam_un *meltxargtab_,
					   const melt_argdescr_cell_t
					   meltxresdescr_[],
					   union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_152_warmelt_normal_LAMBDA___55__ (meltclosure_ptr_t meltclosp_,
					   melt_ptr_t meltfirstargp_,
					   const melt_argdescr_cell_t
					   meltxargdescr_[],
					   union meltparam_un *meltxargtab_,
					   const melt_argdescr_cell_t
					   meltxresdescr_[],
					   union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_153_warmelt_normal_NORMAL_MACRO_EXPORTER (meltclosure_ptr_t
						   meltclosp_,
						   melt_ptr_t meltfirstargp_,
						   const melt_argdescr_cell_t
						   meltxargdescr_[],
						   union meltparam_un
						   *meltxargtab_,
						   const melt_argdescr_cell_t
						   meltxresdescr_[],
						   union meltparam_un
						   *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_154_warmelt_normal_NORMAL_PATMACRO_EXPORTER (meltclosure_ptr_t
						      meltclosp_,
						      melt_ptr_t
						      meltfirstargp_,
						      const
						      melt_argdescr_cell_t
						      meltxargdescr_[],
						      union meltparam_un
						      *meltxargtab_,
						      const
						      melt_argdescr_cell_t
						      meltxresdescr_[],
						      union meltparam_un
						      *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_155_warmelt_normal_NORMAL_EXPORTED_MACRO (meltclosure_ptr_t
						   meltclosp_,
						   melt_ptr_t meltfirstargp_,
						   const melt_argdescr_cell_t
						   meltxargdescr_[],
						   union meltparam_un
						   *meltxargtab_,
						   const melt_argdescr_cell_t
						   meltxresdescr_[],
						   union meltparam_un
						   *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_156_warmelt_normal_NORMEXP_EXPORT_MACRO (meltclosure_ptr_t
						  meltclosp_,
						  melt_ptr_t meltfirstargp_,
						  const melt_argdescr_cell_t
						  meltxargdescr_[],
						  union meltparam_un
						  *meltxargtab_,
						  const melt_argdescr_cell_t
						  meltxresdescr_[],
						  union meltparam_un
						  *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_157_warmelt_normal_NORMAL_EXPORTED_PATMACRO (meltclosure_ptr_t
						      meltclosp_,
						      melt_ptr_t
						      meltfirstargp_,
						      const
						      melt_argdescr_cell_t
						      meltxargdescr_[],
						      union meltparam_un
						      *meltxargtab_,
						      const
						      melt_argdescr_cell_t
						      meltxresdescr_[],
						      union meltparam_un
						      *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_158_warmelt_normal_NORMEXP_EXPORT_PATMACRO (meltclosure_ptr_t
						     meltclosp_,
						     melt_ptr_t
						     meltfirstargp_,
						     const
						     melt_argdescr_cell_t
						     meltxargdescr_[],
						     union meltparam_un
						     *meltxargtab_,
						     const
						     melt_argdescr_cell_t
						     meltxresdescr_[],
						     union meltparam_un
						     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_159_warmelt_normal_NORMEXP_CURRENT_MODULE_ENVIRONMENT_CONTAINER
(meltclosure_ptr_t meltclosp_, melt_ptr_t meltfirstargp_, const melt_argdescr_cell_t meltxargdescr_[],
union meltparam_un *meltxargtab_, const melt_argdescr_cell_t meltxresdescr_[],
union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_160_warmelt_normal_NORMEXP_PARENT_MODULE_ENVIRONMENT
(meltclosure_ptr_t meltclosp_, melt_ptr_t meltfirstargp_, const melt_argdescr_cell_t meltxargdescr_[],
union meltparam_un *meltxargtab_, const melt_argdescr_cell_t meltxresdescr_[],
union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_161_warmelt_normal_NORMEXP_UPDATE_CURRENT_MODULE_ENVIRONMENT_CONTAINER
(meltclosure_ptr_t meltclosp_, melt_ptr_t meltfirstargp_, const melt_argdescr_cell_t meltxargdescr_[],
union meltparam_un *meltxargtab_, const melt_argdescr_cell_t meltxresdescr_[],
union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_162_warmelt_normal_NORMEXP_FETCH_PREDEFINED (meltclosure_ptr_t
						      meltclosp_,
						      melt_ptr_t
						      meltfirstargp_,
						      const
						      melt_argdescr_cell_t
						      meltxargdescr_[],
						      union meltparam_un
						      *meltxargtab_,
						      const
						      melt_argdescr_cell_t
						      meltxresdescr_[],
						      union meltparam_un
						      *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_163_warmelt_normal_LAMBDA___56__ (meltclosure_ptr_t meltclosp_,
					   melt_ptr_t meltfirstargp_,
					   const melt_argdescr_cell_t
					   meltxargdescr_[],
					   union meltparam_un *meltxargtab_,
					   const melt_argdescr_cell_t
					   meltxresdescr_[],
					   union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_164_warmelt_normal_NORMEXP_STORE_PREDEFINED (meltclosure_ptr_t
						      meltclosp_,
						      melt_ptr_t
						      meltfirstargp_,
						      const
						      melt_argdescr_cell_t
						      meltxargdescr_[],
						      union meltparam_un
						      *meltxargtab_,
						      const
						      melt_argdescr_cell_t
						      meltxresdescr_[],
						      union meltparam_un
						      *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_165_warmelt_normal_LAMBDA___57__ (meltclosure_ptr_t meltclosp_,
					   melt_ptr_t meltfirstargp_,
					   const melt_argdescr_cell_t
					   meltxargdescr_[],
					   union meltparam_un *meltxargtab_,
					   const melt_argdescr_cell_t
					   meltxresdescr_[],
					   union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_166_warmelt_normal_NORMEXP_CHEADER (meltclosure_ptr_t meltclosp_,
					     melt_ptr_t meltfirstargp_,
					     const melt_argdescr_cell_t
					     meltxargdescr_[],
					     union meltparam_un *meltxargtab_,
					     const melt_argdescr_cell_t
					     meltxresdescr_[],
					     union meltparam_un
					     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_167_warmelt_normal_NORMEXP_USE_PACKAGE_FROM_PKG_CONFIG
(meltclosure_ptr_t meltclosp_, melt_ptr_t meltfirstargp_, const melt_argdescr_cell_t meltxargdescr_[],
union meltparam_un *meltxargtab_, const melt_argdescr_cell_t meltxresdescr_[],
union meltparam_un *meltxrestab_);



MELT_EXTERN void *melt_start_this_module (void *);


/* define different names when debugging or not */
#if MELT_HAVE_DEBUG
MELT_EXTERN const char meltmodule_warmelt_normal__melt_have_debug_enabled[];
#define melt_have_debug_string meltmodule_warmelt_normal__melt_have_debug_enabled
#else /*!MELT_HAVE_DEBUG */
MELT_EXTERN const char meltmodule_warmelt_normal__melt_have_debug_disabled[];
#define melt_have_debug_string meltmodule_warmelt_normal__melt_have_debug_disabled
#endif /*!MELT_HAVE_DEBUG */


struct frame_melt_start_this_module_st;
void MELT_MODULE_VISIBILITY
meltmod__warmelt_normal__initialmeltchunk_0 (struct
					     frame_melt_start_this_module_st
					     *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_normal__initialmeltchunk_1 (struct
					     frame_melt_start_this_module_st
					     *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_normal__initialmeltchunk_2 (struct
					     frame_melt_start_this_module_st
					     *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_normal__initialmeltchunk_3 (struct
					     frame_melt_start_this_module_st
					     *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_normal__initialmeltchunk_4 (struct
					     frame_melt_start_this_module_st
					     *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_normal__initialmeltchunk_5 (struct
					     frame_melt_start_this_module_st
					     *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_normal__initialmeltchunk_6 (struct
					     frame_melt_start_this_module_st
					     *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_normal__initialmeltchunk_7 (struct
					     frame_melt_start_this_module_st
					     *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_normal__initialmeltchunk_8 (struct
					     frame_melt_start_this_module_st
					     *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_normal__initialmeltchunk_9 (struct
					     frame_melt_start_this_module_st
					     *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_normal__initialmeltchunk_10 (struct
					      frame_melt_start_this_module_st
					      *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_normal__initialmeltchunk_11 (struct
					      frame_melt_start_this_module_st
					      *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_normal__initialmeltchunk_12 (struct
					      frame_melt_start_this_module_st
					      *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_normal__initialmeltchunk_13 (struct
					      frame_melt_start_this_module_st
					      *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_normal__initialmeltchunk_14 (struct
					      frame_melt_start_this_module_st
					      *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_normal__initialmeltchunk_15 (struct
					      frame_melt_start_this_module_st
					      *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_normal__initialmeltchunk_16 (struct
					      frame_melt_start_this_module_st
					      *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_normal__initialmeltchunk_17 (struct
					      frame_melt_start_this_module_st
					      *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_normal__initialmeltchunk_18 (struct
					      frame_melt_start_this_module_st
					      *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_normal__initialmeltchunk_19 (struct
					      frame_melt_start_this_module_st
					      *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_normal__initialmeltchunk_20 (struct
					      frame_melt_start_this_module_st
					      *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_normal__initialmeltchunk_21 (struct
					      frame_melt_start_this_module_st
					      *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_normal__initialmeltchunk_22 (struct
					      frame_melt_start_this_module_st
					      *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_normal__initialmeltchunk_23 (struct
					      frame_melt_start_this_module_st
					      *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_normal__initialmeltchunk_24 (struct
					      frame_melt_start_this_module_st
					      *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_normal__initialmeltchunk_25 (struct
					      frame_melt_start_this_module_st
					      *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_normal__initialmeltchunk_26 (struct
					      frame_melt_start_this_module_st
					      *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_normal__initialmeltchunk_27 (struct
					      frame_melt_start_this_module_st
					      *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_normal__initialmeltchunk_28 (struct
					      frame_melt_start_this_module_st
					      *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_normal__initialmeltchunk_29 (struct
					      frame_melt_start_this_module_st
					      *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_normal__initialmeltchunk_30 (struct
					      frame_melt_start_this_module_st
					      *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_normal__initialmeltchunk_31 (struct
					      frame_melt_start_this_module_st
					      *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_normal__initialmeltchunk_32 (struct
					      frame_melt_start_this_module_st
					      *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_normal__initialmeltchunk_33 (struct
					      frame_melt_start_this_module_st
					      *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_normal__initialmeltchunk_34 (struct
					      frame_melt_start_this_module_st
					      *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_normal__initialmeltchunk_35 (struct
					      frame_melt_start_this_module_st
					      *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_normal__initialmeltchunk_36 (struct
					      frame_melt_start_this_module_st
					      *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_normal__initialmeltchunk_37 (struct
					      frame_melt_start_this_module_st
					      *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_normal__initialmeltchunk_38 (struct
					      frame_melt_start_this_module_st
					      *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_normal__initialmeltchunk_39 (struct
					      frame_melt_start_this_module_st
					      *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_normal__initialmeltchunk_40 (struct
					      frame_melt_start_this_module_st
					      *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_normal__initialmeltchunk_41 (struct
					      frame_melt_start_this_module_st
					      *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_normal__initialmeltchunk_42 (struct
					      frame_melt_start_this_module_st
					      *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_normal__initialmeltchunk_43 (struct
					      frame_melt_start_this_module_st
					      *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_normal__initialmeltchunk_44 (struct
					      frame_melt_start_this_module_st
					      *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_normal__initialmeltchunk_45 (struct
					      frame_melt_start_this_module_st
					      *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_normal__initialmeltchunk_46 (struct
					      frame_melt_start_this_module_st
					      *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_normal__initialmeltchunk_47 (struct
					      frame_melt_start_this_module_st
					      *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_normal__initialmeltchunk_48 (struct
					      frame_melt_start_this_module_st
					      *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_normal__initialmeltchunk_49 (struct
					      frame_melt_start_this_module_st
					      *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_normal__initialmeltchunk_50 (struct
					      frame_melt_start_this_module_st
					      *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_normal__forward_or_mark_module_start_frame (struct
							     melt_callframe_st
							     *fp,
							     int marking);
#define meltmarking_melt_start_this_module  meltmod__warmelt_normal__forward_or_mark_module_start_frame



/**** warmelt-normal+01.c implementations ****/




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_5_warmelt_normal_CREATE_NORMAL_EXTENDING_CONTEXT (meltclosure_ptr_t
							   meltclosp_,
							   melt_ptr_t
							   meltfirstargp_,
							   const
							   melt_argdescr_cell_t
							   meltxargdescr_[],
							   union meltparam_un
							   *meltxargtab_,
							   const
							   melt_argdescr_cell_t
							   meltxresdescr_[],
							   union meltparam_un
							   *meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_5_warmelt_normal_CREATE_NORMAL_EXTENDING_CONTEXT_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_5_warmelt_normal_CREATE_NORMAL_EXTENDING_CONTEXT_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 43
    melt_ptr_t mcfr_varptr[43];
#define MELTFRAM_NBVARNUM 14
    long mcfr_varnum[14];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_5_warmelt_normal_CREATE_NORMAL_EXTENDING_CONTEXT is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct
	 frame_meltrout_5_warmelt_normal_CREATE_NORMAL_EXTENDING_CONTEXT_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 43; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_5_warmelt_normal_CREATE_NORMAL_EXTENDING_CONTEXT nbval 43*/
  meltfram__.mcfr_nbvar = 43 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("CREATE_NORMAL_EXTENDING_CONTEXT", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-normal.melt:980:/ getarg");
 /*_.MODCTX__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.MODENV__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.MODENV__V3*/ meltfptr[2])) !=
	      NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-normal.melt:983:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L1*/ meltfnum[0] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-normal.melt:983:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L1*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-normal.melt:983:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[8];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-normal.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 983;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring =
		"create_normal_extending_context start modctx=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.MODCTX__V2*/ meltfptr[1];
	      /*^apply.arg */
	      argtab[5].meltbp_cstring = "\n* modenv=";
	      /*^apply.arg */
	      argtab[6].meltbp_aptr =
		(melt_ptr_t *) & /*_.MODENV__V3*/ meltfptr[2];
	      /*^apply.arg */
	      argtab[7].meltbp_cstring = "\n";
	      /*_.MELT_DEBUG_FUN__V6*/ meltfptr[5] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR MELTBPARSTR_CSTRING ""), argtab,
			    "", (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V5*/ meltfptr[4] = /*_.MELT_DEBUG_FUN__V6*/ meltfptr[5];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-normal.melt:983:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V6*/ meltfptr[5] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V5*/ meltfptr[4] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-normal.melt:983:/ quasiblock");


      /*_.PROGN___V7*/ meltfptr[5] = /*_.IF___V5*/ meltfptr[4];;
      /*^compute */
      /*_.IFCPP___V4*/ meltfptr[3] = /*_.PROGN___V7*/ meltfptr[5];;
      /*epilog */

      MELT_LOCATION ("warmelt-normal.melt:983:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L1*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IF___V5*/ meltfptr[4] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V7*/ meltfptr[5] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V4*/ meltfptr[3] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-normal.melt:985:/ quasiblock");


 /*_#MAXPREDEFIX__L3*/ meltfnum[1] =
      BGLOB__LASTGLOB;;
    /*^compute */
 /*_#IX__L4*/ meltfnum[0] = 1;;
    /*^compute */
 /*_#I__L5*/ meltfnum[4] =
      ((2) * ( /*_#MAXPREDEFIX__L3*/ meltfnum[1]));;
    /*^compute */
 /*_#I__L6*/ meltfnum[5] =
      ((11) + ( /*_#I__L5*/ meltfnum[4]));;
    /*^compute */
 /*_.PREDEFMAP__V9*/ meltfptr[5] =
      (meltgc_new_mapobjects
       ((meltobject_ptr_t) (( /*!DISCR_MAP_OBJECTS */ meltfrout->tabval[1])),
	( /*_#I__L6*/ meltfnum[5])));;
    /*^compute */
 /*_.VALMAP__V10*/ meltfptr[9] =
      (meltgc_new_mapobjects
       ((meltobject_ptr_t) (( /*!DISCR_MAP_OBJECTS */ meltfrout->tabval[1])),
	(91)));;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-normal.melt:990:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L7*/ meltfnum[6] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.MODCTX__V2*/ meltfptr[1]),
			     (melt_ptr_t) (( /*!CLASS_MODULE_CONTEXT */
					    meltfrout->tabval[2])));;
      MELT_LOCATION ("warmelt-normal.melt:990:/ cond");
      /*cond */ if ( /*_#IS_A__L7*/ meltfnum[6])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V12*/ meltfptr[11] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-normal.melt:990:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check modctx"),
				  ("warmelt-normal.melt")
				  ? ("warmelt-normal.melt") : __FILE__,
				  (990) ? (990) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V12*/ meltfptr[11] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V11*/ meltfptr[10] = /*_.IFELSE___V12*/ meltfptr[11];;
      /*epilog */

      MELT_LOCATION ("warmelt-normal.melt:990:/ clear");
	     /*clear *//*_#IS_A__L7*/ meltfnum[6] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V12*/ meltfptr[11] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V11*/ meltfptr[10] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-normal.melt:991:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L8*/ meltfnum[6] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.MODENV__V3*/ meltfptr[2]),
			     (melt_ptr_t) (( /*!CLASS_ENVIRONMENT */
					    meltfrout->tabval[3])));;
      MELT_LOCATION ("warmelt-normal.melt:991:/ cond");
      /*cond */ if ( /*_#IS_A__L8*/ meltfnum[6])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V14*/ meltfptr[13] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-normal.melt:991:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check modenv"),
				  ("warmelt-normal.melt")
				  ? ("warmelt-normal.melt") : __FILE__,
				  (991) ? (991) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V14*/ meltfptr[13] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V13*/ meltfptr[11] = /*_.IFELSE___V14*/ meltfptr[13];;
      /*epilog */

      MELT_LOCATION ("warmelt-normal.melt:991:/ clear");
	     /*clear *//*_#IS_A__L8*/ meltfnum[6] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V14*/ meltfptr[13] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V13*/ meltfptr[11] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-normal.melt:992:/ loop");
    /*loop */
    {
    labloop_PREDEFLOOP_2:;	/*^loopbody */

      /*^block */
      /*anyblock */
      {

	/*^checksignal */
	MELT_CHECK_SIGNAL ();
	;
	MELT_LOCATION ("warmelt-normal.melt:993:/ checksignal");
	MELT_CHECK_SIGNAL ();
	;
   /*_#I__L9*/ meltfnum[6] =
	  (( /*_#IX__L4*/ meltfnum[0]) >=
	   ( /*_#MAXPREDEFIX__L3*/ meltfnum[1]));;
	MELT_LOCATION ("warmelt-normal.melt:993:/ cond");
	/*cond */ if ( /*_#I__L9*/ meltfnum[6])	/*then */
	  {
	    /*^cond.then */
	    /*^block */
	    /*anyblock */
	    {

	      /*^quasiblock */


	      /*^compute */
     /*_.PREDEFLOOP__V16*/ meltfptr[15] = NULL;;

	      /*^exit */
	      /*exit */
	      {
		goto labexit_PREDEFLOOP_2;
	      }
	      ;
	      /*epilog */
	    }
	    ;
	  }			/*noelse */
	;
	MELT_LOCATION ("warmelt-normal.melt:994:/ quasiblock");


   /*_.CURPREDEF__V17*/ meltfptr[16] =
	  /*get_globpredef */
	  ((melt_ptr_t) melt_globpredef ( /*_#IX__L4*/ meltfnum[0]));;
	MELT_LOCATION ("warmelt-normal.melt:995:/ checksignal");
	MELT_CHECK_SIGNAL ();
	;
   /*_#IS_OBJECT__L10*/ meltfnum[9] =
	  (melt_magic_discr
	   ((melt_ptr_t) ( /*_.CURPREDEF__V17*/ meltfptr[16])) ==
	   MELTOBMAG_OBJECT);;
	MELT_LOCATION ("warmelt-normal.melt:995:/ cond");
	/*cond */ if ( /*_#IS_OBJECT__L10*/ meltfnum[9])	/*then */
	  {
	    /*^cond.then */
	    /*^block */
	    /*anyblock */
	    {

     /*_.MAKE_INTEGERBOX__V18*/ meltfptr[17] =
		(meltgc_new_int
		 ((meltobject_ptr_t)
		  (( /*!DISCR_INTEGER */ meltfrout->tabval[4])),
		  ( /*_#IX__L4*/ meltfnum[0])));;

	      {
		MELT_LOCATION ("warmelt-normal.melt:996:/ locexp");
		meltgc_put_mapobjects ((meltmapobjects_ptr_t)
				       ( /*_.PREDEFMAP__V9*/ meltfptr[5]),
				       (meltobject_ptr_t) ( /*_.CURPREDEF__V17*/ meltfptr[16]),
				       (melt_ptr_t) ( /*_.MAKE_INTEGERBOX__V18*/ meltfptr[17]));
	      }
	      ;
	      /*epilog */

	      MELT_LOCATION ("warmelt-normal.melt:995:/ clear");
	       /*clear *//*_.MAKE_INTEGERBOX__V18*/ meltfptr[17] = 0;
	    }
	    ;
	  }			/*noelse */
	;

	MELT_LOCATION ("warmelt-normal.melt:994:/ clear");
	     /*clear *//*_.CURPREDEF__V17*/ meltfptr[16] = 0;
	/*^clear */
	     /*clear *//*_#IS_OBJECT__L10*/ meltfnum[9] = 0;
   /*_#I__L11*/ meltfnum[9] =
	  (( /*_#IX__L4*/ meltfnum[0]) + (1));;
	MELT_LOCATION ("warmelt-normal.melt:998:/ compute");
	/*_#IX__L4*/ meltfnum[0] = /*_#SETQ___L12*/ meltfnum[11] =
	  /*_#I__L11*/ meltfnum[9];;
	MELT_LOCATION ("warmelt-normal.melt:992:/ checksignal");
	MELT_CHECK_SIGNAL ();
	;
	/*epilog */

	/*^clear */
	     /*clear *//*_#I__L9*/ meltfnum[6] = 0;
	/*^clear */
	     /*clear *//*_#I__L11*/ meltfnum[9] = 0;
	/*^clear */
	     /*clear *//*_#SETQ___L12*/ meltfnum[11] = 0;
      }
      ;
      ;
      goto labloop_PREDEFLOOP_2;
    labexit_PREDEFLOOP_2:;	/*^loopepilog */
      /*loopepilog */
      /*_.FOREVER___V15*/ meltfptr[13] = /*_.PREDEFLOOP__V16*/ meltfptr[15];;
    }
    ;
    MELT_LOCATION ("warmelt-normal.melt:999:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      /*_.FILL_INITIAL_PREDEFMAP__V19*/ meltfptr[17] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!FILL_INITIAL_PREDEFMAP */ meltfrout->tabval[5])),
		    (melt_ptr_t) ( /*_.PREDEFMAP__V9*/ meltfptr[5]), (""),
		    (union meltparam_un *) 0, "", (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-normal.melt:1000:/ quasiblock");


    /*^checksignal */
    MELT_CHECK_SIGNAL ();
    ;
 /*_.MAKE_LIST__V21*/ meltfptr[20] =
      (meltgc_new_list
       ((meltobject_ptr_t) (( /*!DISCR_LIST */ meltfrout->tabval[7]))));;
    /*^compute */
 /*_.MAKE_LIST__V22*/ meltfptr[21] =
      (meltgc_new_list
       ((meltobject_ptr_t) (( /*!DISCR_LIST */ meltfrout->tabval[7]))));;
    MELT_LOCATION ("warmelt-normal.melt:1000:/ quasiblock");


    /*^rawallocobj */
    /*rawallocobj */
    {
      melt_ptr_t newobj = 0;
      melt_raw_object_create (newobj,
			      (melt_ptr_t) (( /*!CLASS_NREP_INITEXTENDPROC */
					     meltfrout->tabval[6])), (5),
			      "CLASS_NREP_INITEXTENDPROC");
  /*_.INST__V24*/ meltfptr[23] =
	newobj;
    };
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @NINIT_TOPL",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V24*/ meltfptr[23])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V24*/ meltfptr[23]), (2),
			  ( /*_.MAKE_LIST__V21*/ meltfptr[20]), "NINIT_TOPL");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @NINIT_DEFBINDS",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V24*/ meltfptr[23])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V24*/ meltfptr[23]), (3),
			  ( /*_.MAKE_LIST__V22*/ meltfptr[21]),
			  "NINIT_DEFBINDS");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @NREP_LOC",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V24*/ meltfptr[23])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V24*/ meltfptr[23]), (0),
			  (( /*nil */ NULL)), "NREP_LOC");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @NINITEXTEND_MODENV",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V24*/ meltfptr[23])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V24*/ meltfptr[23]), (4),
			  ( /*_.MODENV__V3*/ meltfptr[2]),
			  "NINITEXTEND_MODENV");
    ;
    /*^touchobj */

    melt_dbgtrace_written_object ( /*_.INST__V24*/ meltfptr[23],
				  "newly made instance");
    ;
    /*_.INIPRO__V23*/ meltfptr[22] = /*_.INST__V24*/ meltfptr[23];;
    MELT_LOCATION ("warmelt-normal.melt:1006:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_.MAKE_LIST__V25*/ meltfptr[24] =
      (meltgc_new_list
       ((meltobject_ptr_t) (( /*!DISCR_LIST */ meltfrout->tabval[7]))));;
    /*^compute */
 /*_.MAKE_LIST__V26*/ meltfptr[25] =
      (meltgc_new_list
       ((meltobject_ptr_t) (( /*!DISCR_LIST */ meltfrout->tabval[7]))));;
    /*^compute */
 /*_.MAKE_LIST__V27*/ meltfptr[26] =
      (meltgc_new_list
       ((meltobject_ptr_t) (( /*!DISCR_LIST */ meltfrout->tabval[7]))));;
    /*^compute */
 /*_.MAKE_MAPSTRING__V28*/ meltfptr[27] =
      (meltgc_new_mapstrings
       ((meltobject_ptr_t) (( /*!DISCR_MAP_STRINGS */ meltfrout->tabval[9])),
	(30)));;
    /*^compute */
 /*_.MAKE_MAPSTRING__V29*/ meltfptr[28] =
      (meltgc_new_mapstrings
       ((meltobject_ptr_t) (( /*!DISCR_MAP_STRINGS */ meltfrout->tabval[9])),
	(20)));;
    /*^compute */
 /*_.MAKE_MAPOBJECT__V30*/ meltfptr[29] =
      (meltgc_new_mapobjects
       ((meltobject_ptr_t) (( /*!DISCR_MAP_OBJECTS */ meltfrout->tabval[1])),
	(20)));;
    /*^compute */
 /*_.MAKE_MAPOBJECT__V31*/ meltfptr[30] =
      (meltgc_new_mapobjects
       ((meltobject_ptr_t) (( /*!DISCR_MAP_OBJECTS */ meltfrout->tabval[1])),
	(10)));;
    MELT_LOCATION ("warmelt-normal.melt:1021:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*^rawallocobj */
    /*rawallocobj */
    {
      melt_ptr_t newobj = 0;
      melt_raw_object_create (newobj,
			      (melt_ptr_t) (( /*!CLASS_NREP_QUASIDATA_CURRENT_MODULE_ENVIRONMENT_CONTAINER */ meltfrout->tabval[10])), (1), "CLASS_NREP_QUASIDATA_CURRENT_MODULE_ENVIRONMENT_CONTAINER");
  /*_.INST__V33*/ meltfptr[32] =
	newobj;
    };
    ;
    /*^touchobj */

    melt_dbgtrace_written_object ( /*_.INST__V33*/ meltfptr[32],
				  "newly made instance");
    ;
    /*_.INST___V32*/ meltfptr[31] = /*_.INST__V33*/ meltfptr[32];;
    MELT_LOCATION ("warmelt-normal.melt:1023:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*^rawallocobj */
    /*rawallocobj */
    {
      melt_ptr_t newobj = 0;
      melt_raw_object_create (newobj,
			      (melt_ptr_t) (( /*!CLASS_NREP_QUASIDATA_PARENT_MODULE_ENVIRONMENT */ meltfrout->tabval[11])), (1), "CLASS_NREP_QUASIDATA_PARENT_MODULE_ENVIRONMENT");
  /*_.INST__V35*/ meltfptr[34] =
	newobj;
    };
    ;
    /*^touchobj */

    melt_dbgtrace_written_object ( /*_.INST__V35*/ meltfptr[34],
				  "newly made instance");
    ;
    /*_.INST___V34*/ meltfptr[33] = /*_.INST__V35*/ meltfptr[34];;
    /*^compute */
 /*_.MAKE_LIST__V36*/ meltfptr[35] =
      (meltgc_new_list
       ((meltobject_ptr_t) (( /*!DISCR_LIST */ meltfrout->tabval[7]))));;
    MELT_LOCATION ("warmelt-normal.melt:1006:/ quasiblock");


    /*^rawallocobj */
    /*rawallocobj */
    {
      melt_ptr_t newobj = 0;
      melt_raw_object_create (newobj,
			      (melt_ptr_t) (( /*!CLASS_NORMALIZATION_CONTEXT */ meltfrout->tabval[8])), (15), "CLASS_NORMALIZATION_CONTEXT");
  /*_.INST__V38*/ meltfptr[37] =
	newobj;
    };
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @NCTX_INITPROC",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V38*/ meltfptr[37])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V38*/ meltfptr[37]), (0),
			  ( /*_.INIPRO__V23*/ meltfptr[22]), "NCTX_INITPROC");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @NCTX_PROCLIST",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V38*/ meltfptr[37])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V38*/ meltfptr[37]), (1),
			  ( /*_.MAKE_LIST__V25*/ meltfptr[24]),
			  "NCTX_PROCLIST");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @NCTX_DATALIST",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V38*/ meltfptr[37])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V38*/ meltfptr[37]), (2),
			  ( /*_.MAKE_LIST__V26*/ meltfptr[25]),
			  "NCTX_DATALIST");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @NCTX_VALUELIST",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V38*/ meltfptr[37])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V38*/ meltfptr[37]), (3),
			  ( /*_.MAKE_LIST__V27*/ meltfptr[26]),
			  "NCTX_VALUELIST");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @NCTX_SYMBMAP",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V38*/ meltfptr[37])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V38*/ meltfptr[37]), (4),
			  ( /*_.MAKE_MAPSTRING__V28*/ meltfptr[27]),
			  "NCTX_SYMBMAP");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @NCTX_KEYWMAP",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V38*/ meltfptr[37])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V38*/ meltfptr[37]), (5),
			  ( /*_.MAKE_MAPSTRING__V29*/ meltfptr[28]),
			  "NCTX_KEYWMAP");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @NCTX_PREDEFMAP",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V38*/ meltfptr[37])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V38*/ meltfptr[37]), (7),
			  ( /*_.PREDEFMAP__V9*/ meltfptr[5]),
			  "NCTX_PREDEFMAP");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @NCTX_VALMAP",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V38*/ meltfptr[37])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V38*/ meltfptr[37]), (8),
			  ( /*_.VALMAP__V10*/ meltfptr[9]), "NCTX_VALMAP");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @NCTX_VALBINDMAP",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V38*/ meltfptr[37])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V38*/ meltfptr[37]), (9),
			  ( /*_.MAKE_MAPOBJECT__V30*/ meltfptr[29]),
			  "NCTX_VALBINDMAP");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @NCTX_SYMBCACHEMAP",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V38*/ meltfptr[37])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V38*/ meltfptr[37]), (6),
			  ( /*_.MAKE_MAPOBJECT__V31*/ meltfptr[30]),
			  "NCTX_SYMBCACHEMAP");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @NCTX_CURPROC",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V38*/ meltfptr[37])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V38*/ meltfptr[37]), (10),
			  ( /*_.INIPRO__V23*/ meltfptr[22]), "NCTX_CURPROC");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @NCTX_MODULCONTEXT",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V38*/ meltfptr[37])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V38*/ meltfptr[37]), (11),
			  ( /*_.MODCTX__V2*/ meltfptr[1]),
			  "NCTX_MODULCONTEXT");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @NCTX_QDATCURMODENVBOX",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V38*/ meltfptr[37])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V38*/ meltfptr[37]), (12),
			  ( /*_.INST___V32*/ meltfptr[31]),
			  "NCTX_QDATCURMODENVBOX");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @NCTX_QDATPARMODENV",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V38*/ meltfptr[37])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V38*/ meltfptr[37]), (13),
			  ( /*_.INST___V34*/ meltfptr[33]),
			  "NCTX_QDATPARMODENV");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @NCTX_PROCURMODENVLIST",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V38*/ meltfptr[37])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V38*/ meltfptr[37]), (14),
			  ( /*_.MAKE_LIST__V36*/ meltfptr[35]),
			  "NCTX_PROCURMODENVLIST");
    ;
    /*^touchobj */

    melt_dbgtrace_written_object ( /*_.INST__V38*/ meltfptr[37],
				  "newly made instance");
    ;
    /*_.NCX__V37*/ meltfptr[36] = /*_.INST__V38*/ meltfptr[37];;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-normal.melt:1028:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L13*/ meltfnum[6] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-normal.melt:1028:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L13*/ meltfnum[6])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L14*/ meltfnum[9] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-normal.melt:1028:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long =
		/*_#THE_MELTCALLCOUNT__L14*/ meltfnum[9];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-normal.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 1028;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring =
		"create_normal_extending_context return ncx";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.NCX__V37*/ meltfptr[36];
	      /*_.MELT_DEBUG_FUN__V41*/ meltfptr[40] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V40*/ meltfptr[39] =
	      /*_.MELT_DEBUG_FUN__V41*/ meltfptr[40];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-normal.melt:1028:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L14*/ meltfnum[9] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V41*/ meltfptr[40] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V40*/ meltfptr[39] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-normal.melt:1028:/ quasiblock");


      /*_.PROGN___V42*/ meltfptr[40] = /*_.IF___V40*/ meltfptr[39];;
      /*^compute */
      /*_.IFCPP___V39*/ meltfptr[38] = /*_.PROGN___V42*/ meltfptr[40];;
      /*epilog */

      MELT_LOCATION ("warmelt-normal.melt:1028:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L13*/ meltfnum[6] = 0;
      /*^clear */
	     /*clear *//*_.IF___V40*/ meltfptr[39] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V42*/ meltfptr[40] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V39*/ meltfptr[38] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-normal.melt:1029:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.NCX__V37*/ meltfptr[36];;

    {
      MELT_LOCATION ("warmelt-normal.melt:1029:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*_.LET___V20*/ meltfptr[16] = /*_.RETURN___V43*/ meltfptr[39];;

    MELT_LOCATION ("warmelt-normal.melt:1000:/ clear");
	   /*clear *//*_.MAKE_LIST__V21*/ meltfptr[20] = 0;
    /*^clear */
	   /*clear *//*_.MAKE_LIST__V22*/ meltfptr[21] = 0;
    /*^clear */
	   /*clear *//*_.INIPRO__V23*/ meltfptr[22] = 0;
    /*^clear */
	   /*clear *//*_.MAKE_LIST__V25*/ meltfptr[24] = 0;
    /*^clear */
	   /*clear *//*_.MAKE_LIST__V26*/ meltfptr[25] = 0;
    /*^clear */
	   /*clear *//*_.MAKE_LIST__V27*/ meltfptr[26] = 0;
    /*^clear */
	   /*clear *//*_.MAKE_MAPSTRING__V28*/ meltfptr[27] = 0;
    /*^clear */
	   /*clear *//*_.MAKE_MAPSTRING__V29*/ meltfptr[28] = 0;
    /*^clear */
	   /*clear *//*_.MAKE_MAPOBJECT__V30*/ meltfptr[29] = 0;
    /*^clear */
	   /*clear *//*_.MAKE_MAPOBJECT__V31*/ meltfptr[30] = 0;
    /*^clear */
	   /*clear *//*_.INST___V32*/ meltfptr[31] = 0;
    /*^clear */
	   /*clear *//*_.INST___V34*/ meltfptr[33] = 0;
    /*^clear */
	   /*clear *//*_.MAKE_LIST__V36*/ meltfptr[35] = 0;
    /*^clear */
	   /*clear *//*_.NCX__V37*/ meltfptr[36] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V39*/ meltfptr[38] = 0;
    /*^clear */
	   /*clear *//*_.RETURN___V43*/ meltfptr[39] = 0;
    /*_.LET___V8*/ meltfptr[4] = /*_.LET___V20*/ meltfptr[16];;

    MELT_LOCATION ("warmelt-normal.melt:985:/ clear");
	   /*clear *//*_#MAXPREDEFIX__L3*/ meltfnum[1] = 0;
    /*^clear */
	   /*clear *//*_#IX__L4*/ meltfnum[0] = 0;
    /*^clear */
	   /*clear *//*_#I__L5*/ meltfnum[4] = 0;
    /*^clear */
	   /*clear *//*_#I__L6*/ meltfnum[5] = 0;
    /*^clear */
	   /*clear *//*_.PREDEFMAP__V9*/ meltfptr[5] = 0;
    /*^clear */
	   /*clear *//*_.VALMAP__V10*/ meltfptr[9] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V11*/ meltfptr[10] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V13*/ meltfptr[11] = 0;
    /*^clear */
	   /*clear *//*_.FOREVER___V15*/ meltfptr[13] = 0;
    /*^clear */
	   /*clear *//*_.FILL_INITIAL_PREDEFMAP__V19*/ meltfptr[17] = 0;
    /*^clear */
	   /*clear *//*_.LET___V20*/ meltfptr[16] = 0;
    MELT_LOCATION ("warmelt-normal.melt:980:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.LET___V8*/ meltfptr[4];;

    {
      MELT_LOCATION ("warmelt-normal.melt:980:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V4*/ meltfptr[3] = 0;
    /*^clear */
	   /*clear *//*_.LET___V8*/ meltfptr[4] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("CREATE_NORMAL_EXTENDING_CONTEXT", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_5_warmelt_normal_CREATE_NORMAL_EXTENDING_CONTEXT_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_5_warmelt_normal_CREATE_NORMAL_EXTENDING_CONTEXT */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_6_warmelt_normal_NORMEXP_IDENTICAL (meltclosure_ptr_t meltclosp_,
					     melt_ptr_t meltfirstargp_,
					     const melt_argdescr_cell_t
					     meltxargdescr_[],
					     union meltparam_un *
					     meltxargtab_,
					     const melt_argdescr_cell_t
					     meltxresdescr_[],
					     union meltparam_un *
					     meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_6_warmelt_normal_NORMEXP_IDENTICAL_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_6_warmelt_normal_NORMEXP_IDENTICAL_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 14
    melt_ptr_t mcfr_varptr[14];
#define MELTFRAM_NBVARNUM 4
    long mcfr_varnum[4];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_6_warmelt_normal_NORMEXP_IDENTICAL is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_6_warmelt_normal_NORMEXP_IDENTICAL_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 14; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_6_warmelt_normal_NORMEXP_IDENTICAL nbval 14*/
  meltfram__.mcfr_nbvar = 14 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("NORMEXP_IDENTICAL", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-normal.melt:1058:/ getarg");
 /*_.RECV__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.ENV__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.ENV__V3*/ meltfptr[2])) != NULL);


  /*getarg#2 */
  /*^getarg */
  if (meltxargdescr_[1] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.NCX__V4*/ meltfptr[3] =
    (meltxargtab_[1].meltbp_aptr) ? (*(meltxargtab_[1].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.NCX__V4*/ meltfptr[3])) != NULL);


  /*getarg#3 */
  /*^getarg */
  if (meltxargdescr_[2] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.PSLOC__V5*/ meltfptr[4] =
    (meltxargtab_[2].meltbp_aptr) ? (*(meltxargtab_[2].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.PSLOC__V5*/ meltfptr[4])) !=
	      NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-normal.melt:1059:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L1*/ meltfnum[0] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.ENV__V3*/ meltfptr[2]),
			     (melt_ptr_t) (( /*!CLASS_ENVIRONMENT */
					    meltfrout->tabval[0])));;
      MELT_LOCATION ("warmelt-normal.melt:1059:/ cond");
      /*cond */ if ( /*_#IS_A__L1*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V7*/ meltfptr[6] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-normal.melt:1059:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check env"),
				  ("warmelt-normal.melt")
				  ? ("warmelt-normal.melt") : __FILE__,
				  (1059) ? (1059) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V7*/ meltfptr[6] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V6*/ meltfptr[5] = /*_.IFELSE___V7*/ meltfptr[6];;
      /*epilog */

      MELT_LOCATION ("warmelt-normal.melt:1059:/ clear");
	     /*clear *//*_#IS_A__L1*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V7*/ meltfptr[6] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V6*/ meltfptr[5] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-normal.melt:1060:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L2*/ meltfnum[0] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.NCX__V4*/ meltfptr[3]),
			     (melt_ptr_t) (( /*!CLASS_NORMALIZATION_CONTEXT */
					    meltfrout->tabval[1])));;
      MELT_LOCATION ("warmelt-normal.melt:1060:/ cond");
      /*cond */ if ( /*_#IS_A__L2*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V9*/ meltfptr[8] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-normal.melt:1060:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check nctxt"),
				  ("warmelt-normal.melt")
				  ? ("warmelt-normal.melt") : __FILE__,
				  (1060) ? (1060) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V9*/ meltfptr[8] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V8*/ meltfptr[6] = /*_.IFELSE___V9*/ meltfptr[8];;
      /*epilog */

      MELT_LOCATION ("warmelt-normal.melt:1060:/ clear");
	     /*clear *//*_#IS_A__L2*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V9*/ meltfptr[8] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V8*/ meltfptr[6] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-normal.melt:1062:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L3*/ meltfnum[0] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-normal.melt:1062:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L3*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L4*/ meltfnum[3] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-normal.melt:1062:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L4*/ meltfnum[3];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-normal.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 1062;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "normexp_identical recv";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.RECV__V2*/ meltfptr[1];
	      /*_.MELT_DEBUG_FUN__V12*/ meltfptr[11] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[2])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V11*/ meltfptr[10] =
	      /*_.MELT_DEBUG_FUN__V12*/ meltfptr[11];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-normal.melt:1062:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L4*/ meltfnum[3] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V12*/ meltfptr[11] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V11*/ meltfptr[10] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-normal.melt:1062:/ quasiblock");


      /*_.PROGN___V13*/ meltfptr[11] = /*_.IF___V11*/ meltfptr[10];;
      /*^compute */
      /*_.IFCPP___V10*/ meltfptr[8] = /*_.PROGN___V13*/ meltfptr[11];;
      /*epilog */

      MELT_LOCATION ("warmelt-normal.melt:1062:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L3*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IF___V11*/ meltfptr[10] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V13*/ meltfptr[11] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V10*/ meltfptr[8] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-normal.melt:1063:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.RECV__V2*/ meltfptr[1];;
    MELT_LOCATION ("warmelt-normal.melt:1063:/ putxtraresult");
    if (!meltxrestab_ || !meltxresdescr_)
      goto labend_rout;
    if (meltxresdescr_[0] != MELTBPAR_PTR)
      goto labend_rout;
    if (meltxrestab_[0].meltbp_aptr)
      *(meltxrestab_[0].meltbp_aptr) = (melt_ptr_t) (( /*nil */ NULL));
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    MELT_LOCATION ("warmelt-normal.melt:1058:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.RETURN___V14*/ meltfptr[10];;

    {
      MELT_LOCATION ("warmelt-normal.melt:1058:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V6*/ meltfptr[5] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V8*/ meltfptr[6] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V10*/ meltfptr[8] = 0;
    /*^clear */
	   /*clear *//*_.RETURN___V14*/ meltfptr[10] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("NORMEXP_IDENTICAL", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_6_warmelt_normal_NORMEXP_IDENTICAL_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_6_warmelt_normal_NORMEXP_IDENTICAL */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_7_warmelt_normal_NORMEXP_NULL (meltclosure_ptr_t meltclosp_,
					melt_ptr_t meltfirstargp_,
					const melt_argdescr_cell_t
					meltxargdescr_[],
					union meltparam_un * meltxargtab_,
					const melt_argdescr_cell_t
					meltxresdescr_[],
					union meltparam_un * meltxrestab_)
{
  long current_blocklevel_signals_meltrout_7_warmelt_normal_NORMEXP_NULL_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_7_warmelt_normal_NORMEXP_NULL_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 17
    melt_ptr_t mcfr_varptr[17];
#define MELTFRAM_NBVARNUM 4
    long mcfr_varnum[4];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_7_warmelt_normal_NORMEXP_NULL is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_7_warmelt_normal_NORMEXP_NULL_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 17; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_7_warmelt_normal_NORMEXP_NULL nbval 17*/
  meltfram__.mcfr_nbvar = 17 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("NORMEXP_NULL", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-normal.melt:1068:/ getarg");
 /*_.RECV__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.ENV__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.ENV__V3*/ meltfptr[2])) != NULL);


  /*getarg#2 */
  /*^getarg */
  if (meltxargdescr_[1] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.NCX__V4*/ meltfptr[3] =
    (meltxargtab_[1].meltbp_aptr) ? (*(meltxargtab_[1].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.NCX__V4*/ meltfptr[3])) != NULL);


  /*getarg#3 */
  /*^getarg */
  if (meltxargdescr_[2] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.PSLOC__V5*/ meltfptr[4] =
    (meltxargtab_[2].meltbp_aptr) ? (*(meltxargtab_[2].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.PSLOC__V5*/ meltfptr[4])) !=
	      NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-normal.melt:1069:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L1*/ meltfnum[0] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.ENV__V3*/ meltfptr[2]),
			     (melt_ptr_t) (( /*!CLASS_ENVIRONMENT */
					    meltfrout->tabval[0])));;
      MELT_LOCATION ("warmelt-normal.melt:1069:/ cond");
      /*cond */ if ( /*_#IS_A__L1*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V7*/ meltfptr[6] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-normal.melt:1069:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check env"),
				  ("warmelt-normal.melt")
				  ? ("warmelt-normal.melt") : __FILE__,
				  (1069) ? (1069) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V7*/ meltfptr[6] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V6*/ meltfptr[5] = /*_.IFELSE___V7*/ meltfptr[6];;
      /*epilog */

      MELT_LOCATION ("warmelt-normal.melt:1069:/ clear");
	     /*clear *//*_#IS_A__L1*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V7*/ meltfptr[6] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V6*/ meltfptr[5] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-normal.melt:1070:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L2*/ meltfnum[0] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.NCX__V4*/ meltfptr[3]),
			     (melt_ptr_t) (( /*!CLASS_NORMALIZATION_CONTEXT */
					    meltfrout->tabval[1])));;
      MELT_LOCATION ("warmelt-normal.melt:1070:/ cond");
      /*cond */ if ( /*_#IS_A__L2*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V9*/ meltfptr[8] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-normal.melt:1070:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check nctxt"),
				  ("warmelt-normal.melt")
				  ? ("warmelt-normal.melt") : __FILE__,
				  (1070) ? (1070) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V9*/ meltfptr[8] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V8*/ meltfptr[6] = /*_.IFELSE___V9*/ meltfptr[8];;
      /*epilog */

      MELT_LOCATION ("warmelt-normal.melt:1070:/ clear");
	     /*clear *//*_#IS_A__L2*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V9*/ meltfptr[8] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V8*/ meltfptr[6] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-normal.melt:1071:/ quasiblock");


    /*^checksignal */
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*^rawallocobj */
    /*rawallocobj */
    {
      melt_ptr_t newobj = 0;
      melt_raw_object_create (newobj,
			      (melt_ptr_t) (( /*!CLASS_NREP_NIL */ meltfrout->
					     tabval[2])), (1),
			      "CLASS_NREP_NIL");
  /*_.INST__V12*/ meltfptr[11] =
	newobj;
    };
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @NREP_LOC",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V12*/ meltfptr[11])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V12*/ meltfptr[11]), (0),
			  ( /*_.PSLOC__V5*/ meltfptr[4]), "NREP_LOC");
    ;
    /*^touchobj */

    melt_dbgtrace_written_object ( /*_.INST__V12*/ meltfptr[11],
				  "newly made instance");
    ;
    /*_.NORMNULL__V11*/ meltfptr[10] = /*_.INST__V12*/ meltfptr[11];;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-normal.melt:1072:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L3*/ meltfnum[0] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-normal.melt:1072:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L3*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L4*/ meltfnum[3] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-normal.melt:1072:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L4*/ meltfnum[3];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-normal.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 1072;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "normexp_null normnull";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.NORMNULL__V11*/ meltfptr[10];
	      /*_.MELT_DEBUG_FUN__V15*/ meltfptr[14] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[3])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V14*/ meltfptr[13] =
	      /*_.MELT_DEBUG_FUN__V15*/ meltfptr[14];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-normal.melt:1072:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L4*/ meltfnum[3] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V15*/ meltfptr[14] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V14*/ meltfptr[13] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-normal.melt:1072:/ quasiblock");


      /*_.PROGN___V16*/ meltfptr[14] = /*_.IF___V14*/ meltfptr[13];;
      /*^compute */
      /*_.IFCPP___V13*/ meltfptr[12] = /*_.PROGN___V16*/ meltfptr[14];;
      /*epilog */

      MELT_LOCATION ("warmelt-normal.melt:1072:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L3*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IF___V14*/ meltfptr[13] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V16*/ meltfptr[14] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V13*/ meltfptr[12] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-normal.melt:1073:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.NORMNULL__V11*/ meltfptr[10];;
    MELT_LOCATION ("warmelt-normal.melt:1073:/ putxtraresult");
    if (!meltxrestab_ || !meltxresdescr_)
      goto labend_rout;
    if (meltxresdescr_[0] != MELTBPAR_PTR)
      goto labend_rout;
    if (meltxrestab_[0].meltbp_aptr)
      *(meltxrestab_[0].meltbp_aptr) = (melt_ptr_t) (( /*nil */ NULL));
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*_.LET___V10*/ meltfptr[8] = /*_.RETURN___V17*/ meltfptr[13];;

    MELT_LOCATION ("warmelt-normal.melt:1071:/ clear");
	   /*clear *//*_.NORMNULL__V11*/ meltfptr[10] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V13*/ meltfptr[12] = 0;
    /*^clear */
	   /*clear *//*_.RETURN___V17*/ meltfptr[13] = 0;
    MELT_LOCATION ("warmelt-normal.melt:1068:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.LET___V10*/ meltfptr[8];;

    {
      MELT_LOCATION ("warmelt-normal.melt:1068:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V6*/ meltfptr[5] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V8*/ meltfptr[6] = 0;
    /*^clear */
	   /*clear *//*_.LET___V10*/ meltfptr[8] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("NORMEXP_NULL", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_7_warmelt_normal_NORMEXP_NULL_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_7_warmelt_normal_NORMEXP_NULL */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_8_warmelt_normal_NORMEXP_ANY_OBJECT (meltclosure_ptr_t meltclosp_,
					      melt_ptr_t meltfirstargp_,
					      const melt_argdescr_cell_t
					      meltxargdescr_[],
					      union meltparam_un *
					      meltxargtab_,
					      const melt_argdescr_cell_t
					      meltxresdescr_[],
					      union meltparam_un *
					      meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_8_warmelt_normal_NORMEXP_ANY_OBJECT_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_8_warmelt_normal_NORMEXP_ANY_OBJECT_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 15
    melt_ptr_t mcfr_varptr[15];
#define MELTFRAM_NBVARNUM 3
    long mcfr_varnum[3];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_8_warmelt_normal_NORMEXP_ANY_OBJECT is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_8_warmelt_normal_NORMEXP_ANY_OBJECT_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 15; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_8_warmelt_normal_NORMEXP_ANY_OBJECT nbval 15*/
  meltfram__.mcfr_nbvar = 15 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("NORMEXP_ANY_OBJECT", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-normal.melt:1083:/ getarg");
 /*_.RECV__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.ENV__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.ENV__V3*/ meltfptr[2])) != NULL);


  /*getarg#2 */
  /*^getarg */
  if (meltxargdescr_[1] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.NCX__V4*/ meltfptr[3] =
    (meltxargtab_[1].meltbp_aptr) ? (*(meltxargtab_[1].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.NCX__V4*/ meltfptr[3])) != NULL);


  /*getarg#3 */
  /*^getarg */
  if (meltxargdescr_[2] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.PSLOC__V5*/ meltfptr[4] =
    (meltxargtab_[2].meltbp_aptr) ? (*(meltxargtab_[2].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.PSLOC__V5*/ meltfptr[4])) !=
	      NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-normal.melt:1084:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L1*/ meltfnum[0] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.ENV__V3*/ meltfptr[2]),
			     (melt_ptr_t) (( /*!CLASS_ENVIRONMENT */
					    meltfrout->tabval[0])));;
      MELT_LOCATION ("warmelt-normal.melt:1084:/ cond");
      /*cond */ if ( /*_#IS_A__L1*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V7*/ meltfptr[6] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-normal.melt:1084:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check env"),
				  ("warmelt-normal.melt")
				  ? ("warmelt-normal.melt") : __FILE__,
				  (1084) ? (1084) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V7*/ meltfptr[6] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V6*/ meltfptr[5] = /*_.IFELSE___V7*/ meltfptr[6];;
      /*epilog */

      MELT_LOCATION ("warmelt-normal.melt:1084:/ clear");
	     /*clear *//*_#IS_A__L1*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V7*/ meltfptr[6] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V6*/ meltfptr[5] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-normal.melt:1085:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L2*/ meltfnum[0] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.NCX__V4*/ meltfptr[3]),
			     (melt_ptr_t) (( /*!CLASS_NORMALIZATION_CONTEXT */
					    meltfrout->tabval[1])));;
      MELT_LOCATION ("warmelt-normal.melt:1085:/ cond");
      /*cond */ if ( /*_#IS_A__L2*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V9*/ meltfptr[8] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-normal.melt:1085:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check nctxt"),
				  ("warmelt-normal.melt")
				  ? ("warmelt-normal.melt") : __FILE__,
				  (1085) ? (1085) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V9*/ meltfptr[8] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V8*/ meltfptr[6] = /*_.IFELSE___V9*/ meltfptr[8];;
      /*epilog */

      MELT_LOCATION ("warmelt-normal.melt:1085:/ clear");
	     /*clear *//*_#IS_A__L2*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V9*/ meltfptr[8] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V8*/ meltfptr[6] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-normal.melt:1090:/ quasiblock");


 /*_.CLA__V11*/ meltfptr[10] =
      ((melt_ptr_t)
       (melt_discr ((melt_ptr_t) ( /*_.RECV__V2*/ meltfptr[1]))));;
    MELT_LOCATION ("warmelt-normal.melt:1091:/ cond");
    /*cond */ if (
		   /*ifisa */
		   melt_is_instance_of ((melt_ptr_t)
					( /*_.CLA__V11*/ meltfptr[10]),
					(melt_ptr_t) (( /*!CLASS_NAMED */
						       meltfrout->tabval[2])))
      )				/*then */
      {
	/*^cond.then */
	/*^getslot */
	{
	  melt_ptr_t slot = NULL, obj = NULL;
	  obj = (melt_ptr_t) ( /*_.CLA__V11*/ meltfptr[10]) /*=obj*/ ;
	  melt_object_get_field (slot, obj, 1, "NAMED_NAME");
   /*_.CLANAME__V12*/ meltfptr[11] = slot;
	};
	;
      }
    else
      {				/*^cond.else */

  /*_.CLANAME__V12*/ meltfptr[11] = NULL;;
      }
    ;
    MELT_LOCATION ("warmelt-normal.melt:1092:/ cond");
    /*cond */ if (
		   /*ifisa */
		   melt_is_instance_of ((melt_ptr_t)
					( /*_.RECV__V2*/ meltfptr[1]),
					(melt_ptr_t) (( /*!CLASS_NAMED */
						       meltfrout->tabval[2])))
      )				/*then */
      {
	/*^cond.then */
	/*^getslot */
	{
	  melt_ptr_t slot = NULL, obj = NULL;
	  obj = (melt_ptr_t) ( /*_.RECV__V2*/ meltfptr[1]) /*=obj*/ ;
	  melt_object_get_field (slot, obj, 1, "NAMED_NAME");
   /*_.RECNAME__V13*/ meltfptr[12] = slot;
	};
	;
      }
    else
      {				/*^cond.else */

  /*_.RECNAME__V13*/ meltfptr[12] = NULL;;
      }
    ;
    MELT_LOCATION ("warmelt-normal.melt:1094:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#IS_STRING__L3*/ meltfnum[0] =
      (melt_magic_discr ((melt_ptr_t) ( /*_.RECNAME__V13*/ meltfptr[12])) ==
       MELTOBMAG_STRING);;
    MELT_LOCATION ("warmelt-normal.melt:1094:/ cond");
    /*cond */ if ( /*_#IS_STRING__L3*/ meltfnum[0])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{


	  {
	    MELT_LOCATION ("warmelt-normal.melt:1095:/ locexp");
	    melt_error_str ((melt_ptr_t) ( /*_.PSLOC__V5*/ meltfptr[4]),
			    ("unimplemented normalization for literal object named "),
			    (melt_ptr_t) ( /*_.RECNAME__V13*/ meltfptr[12]));
	  }
	  ;
	  /*epilog */
	}
	;
      }				/*noelse */
    ;

    {
      MELT_LOCATION ("warmelt-normal.melt:1097:/ locexp");
      melt_error_str ((melt_ptr_t) ( /*_.PSLOC__V5*/ meltfptr[4]),
		      ("unimplemented normalization for literal object of "),
		      (melt_ptr_t) ( /*_.CLANAME__V12*/ meltfptr[11]));
    }
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-normal.melt:1099:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
      /*^cond */
      /*cond */ if (( /*nil */ NULL))	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V15*/ meltfptr[14] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-normal.melt:1099:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("@$@ unimplemented normexp_any_object"),
				  ("warmelt-normal.melt")
				  ? ("warmelt-normal.melt") : __FILE__,
				  (1099) ? (1099) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V15*/ meltfptr[14] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V14*/ meltfptr[13] = /*_.IFELSE___V15*/ meltfptr[14];;
      /*epilog */

      MELT_LOCATION ("warmelt-normal.melt:1099:/ clear");
	     /*clear *//*_.IFELSE___V15*/ meltfptr[14] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V14*/ meltfptr[13] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    /*^compute */
    /*_.LET___V10*/ meltfptr[8] = /*_.IFCPP___V14*/ meltfptr[13];;

    MELT_LOCATION ("warmelt-normal.melt:1090:/ clear");
	   /*clear *//*_.CLA__V11*/ meltfptr[10] = 0;
    /*^clear */
	   /*clear *//*_.CLANAME__V12*/ meltfptr[11] = 0;
    /*^clear */
	   /*clear *//*_.RECNAME__V13*/ meltfptr[12] = 0;
    /*^clear */
	   /*clear *//*_#IS_STRING__L3*/ meltfnum[0] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V14*/ meltfptr[13] = 0;
    MELT_LOCATION ("warmelt-normal.melt:1083:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.LET___V10*/ meltfptr[8];;

    {
      MELT_LOCATION ("warmelt-normal.melt:1083:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V6*/ meltfptr[5] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V8*/ meltfptr[6] = 0;
    /*^clear */
	   /*clear *//*_.LET___V10*/ meltfptr[8] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("NORMEXP_ANY_OBJECT", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_8_warmelt_normal_NORMEXP_ANY_OBJECT_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_8_warmelt_normal_NORMEXP_ANY_OBJECT */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_9_warmelt_normal_NORMEXP_ANY_VALUE (meltclosure_ptr_t meltclosp_,
					     melt_ptr_t meltfirstargp_,
					     const melt_argdescr_cell_t
					     meltxargdescr_[],
					     union meltparam_un *
					     meltxargtab_,
					     const melt_argdescr_cell_t
					     meltxresdescr_[],
					     union meltparam_un *
					     meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_9_warmelt_normal_NORMEXP_ANY_VALUE_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_9_warmelt_normal_NORMEXP_ANY_VALUE_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 14
    melt_ptr_t mcfr_varptr[14];
#define MELTFRAM_NBVARNUM 2
    long mcfr_varnum[2];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_9_warmelt_normal_NORMEXP_ANY_VALUE is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_9_warmelt_normal_NORMEXP_ANY_VALUE_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 14; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_9_warmelt_normal_NORMEXP_ANY_VALUE nbval 14*/
  meltfram__.mcfr_nbvar = 14 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("NORMEXP_ANY_VALUE", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-normal.melt:1102:/ getarg");
 /*_.RECV__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.ENV__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.ENV__V3*/ meltfptr[2])) != NULL);


  /*getarg#2 */
  /*^getarg */
  if (meltxargdescr_[1] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.NCX__V4*/ meltfptr[3] =
    (meltxargtab_[1].meltbp_aptr) ? (*(meltxargtab_[1].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.NCX__V4*/ meltfptr[3])) != NULL);


  /*getarg#3 */
  /*^getarg */
  if (meltxargdescr_[2] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.PSLOC__V5*/ meltfptr[4] =
    (meltxargtab_[2].meltbp_aptr) ? (*(meltxargtab_[2].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.PSLOC__V5*/ meltfptr[4])) !=
	      NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-normal.melt:1103:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L1*/ meltfnum[0] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.ENV__V3*/ meltfptr[2]),
			     (melt_ptr_t) (( /*!CLASS_ENVIRONMENT */
					    meltfrout->tabval[0])));;
      MELT_LOCATION ("warmelt-normal.melt:1103:/ cond");
      /*cond */ if ( /*_#IS_A__L1*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V7*/ meltfptr[6] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-normal.melt:1103:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check env"),
				  ("warmelt-normal.melt")
				  ? ("warmelt-normal.melt") : __FILE__,
				  (1103) ? (1103) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V7*/ meltfptr[6] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V6*/ meltfptr[5] = /*_.IFELSE___V7*/ meltfptr[6];;
      /*epilog */

      MELT_LOCATION ("warmelt-normal.melt:1103:/ clear");
	     /*clear *//*_#IS_A__L1*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V7*/ meltfptr[6] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V6*/ meltfptr[5] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-normal.melt:1104:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L2*/ meltfnum[0] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.NCX__V4*/ meltfptr[3]),
			     (melt_ptr_t) (( /*!CLASS_NORMALIZATION_CONTEXT */
					    meltfrout->tabval[1])));;
      MELT_LOCATION ("warmelt-normal.melt:1104:/ cond");
      /*cond */ if ( /*_#IS_A__L2*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V9*/ meltfptr[8] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-normal.melt:1104:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check nctxt"),
				  ("warmelt-normal.melt")
				  ? ("warmelt-normal.melt") : __FILE__,
				  (1104) ? (1104) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V9*/ meltfptr[8] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V8*/ meltfptr[6] = /*_.IFELSE___V9*/ meltfptr[8];;
      /*epilog */

      MELT_LOCATION ("warmelt-normal.melt:1104:/ clear");
	     /*clear *//*_#IS_A__L2*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V9*/ meltfptr[8] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V8*/ meltfptr[6] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-normal.melt:1109:/ quasiblock");


 /*_.CLA__V11*/ meltfptr[10] =
      ((melt_ptr_t)
       (melt_discr ((melt_ptr_t) ( /*_.RECV__V2*/ meltfptr[1]))));;
    MELT_LOCATION ("warmelt-normal.melt:1110:/ cond");
    /*cond */ if (
		   /*ifisa */
		   melt_is_instance_of ((melt_ptr_t)
					( /*_.CLA__V11*/ meltfptr[10]),
					(melt_ptr_t) (( /*!CLASS_NAMED */
						       meltfrout->tabval[2])))
      )				/*then */
      {
	/*^cond.then */
	/*^getslot */
	{
	  melt_ptr_t slot = NULL, obj = NULL;
	  obj = (melt_ptr_t) ( /*_.CLA__V11*/ meltfptr[10]) /*=obj*/ ;
	  melt_object_get_field (slot, obj, 1, "NAMED_NAME");
   /*_.CLANAME__V12*/ meltfptr[11] = slot;
	};
	;
      }
    else
      {				/*^cond.else */

  /*_.CLANAME__V12*/ meltfptr[11] = NULL;;
      }
    ;

    {
      MELT_LOCATION ("warmelt-normal.melt:1112:/ locexp");
      melt_error_str ((melt_ptr_t) ( /*_.PSLOC__V5*/ meltfptr[4]),
		      ("unimplemented normalization for literal value of "),
		      (melt_ptr_t) ( /*_.CLANAME__V12*/ meltfptr[11]));
    }
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-normal.melt:1114:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
      /*^cond */
      /*cond */ if (( /*nil */ NULL))	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V14*/ meltfptr[13] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-normal.melt:1114:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("@$@ unimplemented normexp_any_value"),
				  ("warmelt-normal.melt")
				  ? ("warmelt-normal.melt") : __FILE__,
				  (1114) ? (1114) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V14*/ meltfptr[13] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V13*/ meltfptr[12] = /*_.IFELSE___V14*/ meltfptr[13];;
      /*epilog */

      MELT_LOCATION ("warmelt-normal.melt:1114:/ clear");
	     /*clear *//*_.IFELSE___V14*/ meltfptr[13] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V13*/ meltfptr[12] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    /*^compute */
    /*_.LET___V10*/ meltfptr[8] = /*_.IFCPP___V13*/ meltfptr[12];;

    MELT_LOCATION ("warmelt-normal.melt:1109:/ clear");
	   /*clear *//*_.CLA__V11*/ meltfptr[10] = 0;
    /*^clear */
	   /*clear *//*_.CLANAME__V12*/ meltfptr[11] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V13*/ meltfptr[12] = 0;
    MELT_LOCATION ("warmelt-normal.melt:1102:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.LET___V10*/ meltfptr[8];;

    {
      MELT_LOCATION ("warmelt-normal.melt:1102:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V6*/ meltfptr[5] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V8*/ meltfptr[6] = 0;
    /*^clear */
	   /*clear *//*_.LET___V10*/ meltfptr[8] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("NORMEXP_ANY_VALUE", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_9_warmelt_normal_NORMEXP_ANY_VALUE_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_9_warmelt_normal_NORMEXP_ANY_VALUE */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_10_warmelt_normal_NORMEXP_SRC_CATCHALL (meltclosure_ptr_t meltclosp_,
						 melt_ptr_t meltfirstargp_,
						 const melt_argdescr_cell_t
						 meltxargdescr_[],
						 union meltparam_un *
						 meltxargtab_,
						 const melt_argdescr_cell_t
						 meltxresdescr_[],
						 union meltparam_un *
						 meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_10_warmelt_normal_NORMEXP_SRC_CATCHALL_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_10_warmelt_normal_NORMEXP_SRC_CATCHALL_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 19
    melt_ptr_t mcfr_varptr[19];
#define MELTFRAM_NBVARNUM 4
    long mcfr_varnum[4];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_10_warmelt_normal_NORMEXP_SRC_CATCHALL is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_10_warmelt_normal_NORMEXP_SRC_CATCHALL_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 19; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_10_warmelt_normal_NORMEXP_SRC_CATCHALL nbval 19*/
  meltfram__.mcfr_nbvar = 19 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("NORMEXP_SRC_CATCHALL", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-normal.melt:1118:/ getarg");
 /*_.RECV__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.ENV__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.ENV__V3*/ meltfptr[2])) != NULL);


  /*getarg#2 */
  /*^getarg */
  if (meltxargdescr_[1] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.NCX__V4*/ meltfptr[3] =
    (meltxargtab_[1].meltbp_aptr) ? (*(meltxargtab_[1].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.NCX__V4*/ meltfptr[3])) != NULL);


  /*getarg#3 */
  /*^getarg */
  if (meltxargdescr_[2] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.PSLOC__V5*/ meltfptr[4] =
    (meltxargtab_[2].meltbp_aptr) ? (*(meltxargtab_[2].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.PSLOC__V5*/ meltfptr[4])) !=
	      NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-normal.melt:1119:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L1*/ meltfnum[0] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-normal.melt:1119:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L1*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-normal.melt:1119:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-normal.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 1119;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "normexp_src_catchall recv=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.RECV__V2*/ meltfptr[1];
	      /*_.MELT_DEBUG_FUN__V8*/ meltfptr[7] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V7*/ meltfptr[6] = /*_.MELT_DEBUG_FUN__V8*/ meltfptr[7];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-normal.melt:1119:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V8*/ meltfptr[7] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V7*/ meltfptr[6] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-normal.melt:1119:/ quasiblock");


      /*_.PROGN___V9*/ meltfptr[7] = /*_.IF___V7*/ meltfptr[6];;
      /*^compute */
      /*_.IFCPP___V6*/ meltfptr[5] = /*_.PROGN___V9*/ meltfptr[7];;
      /*epilog */

      MELT_LOCATION ("warmelt-normal.melt:1119:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L1*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IF___V7*/ meltfptr[6] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V9*/ meltfptr[7] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V6*/ meltfptr[5] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-normal.melt:1120:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L3*/ meltfnum[1] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.ENV__V3*/ meltfptr[2]),
			     (melt_ptr_t) (( /*!CLASS_ENVIRONMENT */
					    meltfrout->tabval[1])));;
      MELT_LOCATION ("warmelt-normal.melt:1120:/ cond");
      /*cond */ if ( /*_#IS_A__L3*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V11*/ meltfptr[7] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-normal.melt:1120:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check env"),
				  ("warmelt-normal.melt")
				  ? ("warmelt-normal.melt") : __FILE__,
				  (1120) ? (1120) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V11*/ meltfptr[7] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V10*/ meltfptr[6] = /*_.IFELSE___V11*/ meltfptr[7];;
      /*epilog */

      MELT_LOCATION ("warmelt-normal.melt:1120:/ clear");
	     /*clear *//*_#IS_A__L3*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V11*/ meltfptr[7] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V10*/ meltfptr[6] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-normal.melt:1121:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L4*/ meltfnum[0] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.NCX__V4*/ meltfptr[3]),
			     (melt_ptr_t) (( /*!CLASS_NORMALIZATION_CONTEXT */
					    meltfrout->tabval[2])));;
      MELT_LOCATION ("warmelt-normal.melt:1121:/ cond");
      /*cond */ if ( /*_#IS_A__L4*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V13*/ meltfptr[12] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-normal.melt:1121:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check nctxt"),
				  ("warmelt-normal.melt")
				  ? ("warmelt-normal.melt") : __FILE__,
				  (1121) ? (1121) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V13*/ meltfptr[12] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V12*/ meltfptr[7] = /*_.IFELSE___V13*/ meltfptr[12];;
      /*epilog */

      MELT_LOCATION ("warmelt-normal.melt:1121:/ clear");
	     /*clear *//*_#IS_A__L4*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V13*/ meltfptr[12] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V12*/ meltfptr[7] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-normal.melt:1122:/ quasiblock");


 /*_.MYCLASS__V15*/ meltfptr[14] =
      ((melt_ptr_t)
       (melt_discr ((melt_ptr_t) ( /*_.RECV__V2*/ meltfptr[1]))));;
    MELT_LOCATION ("warmelt-normal.melt:1123:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.MYCLASS__V15*/ meltfptr[14]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 1, "NAMED_NAME");
  /*_.MYCLASSNAME__V16*/ meltfptr[15] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-normal.melt:1124:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.RECV__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 1, "LOCA_LOCATION");
  /*_.LOCA_LOCATION__V17*/ meltfptr[16] = slot;
    };
    ;

    {
      /*^locexp */
      melt_error_str ((melt_ptr_t) ( /*_.LOCA_LOCATION__V17*/ meltfptr[16]),
		      ("unimplemented normalization for "),
		      (melt_ptr_t) ( /*_.MYCLASSNAME__V16*/ meltfptr[15]));
    }
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-normal.melt:1126:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
      /*^cond */
      /*cond */ if (( /*nil */ NULL))	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V19*/ meltfptr[18] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-normal.melt:1126:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("normexp_src_catchall unimplemented normexp for src"), ("warmelt-normal.melt") ? ("warmelt-normal.melt") : __FILE__, (1126) ? (1126) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V19*/ meltfptr[18] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V18*/ meltfptr[17] = /*_.IFELSE___V19*/ meltfptr[18];;
      /*epilog */

      MELT_LOCATION ("warmelt-normal.melt:1126:/ clear");
	     /*clear *//*_.IFELSE___V19*/ meltfptr[18] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V18*/ meltfptr[17] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    /*^compute */
    /*_.LET___V14*/ meltfptr[12] = /*_.IFCPP___V18*/ meltfptr[17];;

    MELT_LOCATION ("warmelt-normal.melt:1122:/ clear");
	   /*clear *//*_.MYCLASS__V15*/ meltfptr[14] = 0;
    /*^clear */
	   /*clear *//*_.MYCLASSNAME__V16*/ meltfptr[15] = 0;
    /*^clear */
	   /*clear *//*_.LOCA_LOCATION__V17*/ meltfptr[16] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V18*/ meltfptr[17] = 0;
    MELT_LOCATION ("warmelt-normal.melt:1118:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.LET___V14*/ meltfptr[12];;

    {
      MELT_LOCATION ("warmelt-normal.melt:1118:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V6*/ meltfptr[5] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V10*/ meltfptr[6] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V12*/ meltfptr[7] = 0;
    /*^clear */
	   /*clear *//*_.LET___V14*/ meltfptr[12] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("NORMEXP_SRC_CATCHALL", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_10_warmelt_normal_NORMEXP_SRC_CATCHALL_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_10_warmelt_normal_NORMEXP_SRC_CATCHALL */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_11_warmelt_normal_NORMEXP_LAZYMACROEXP (meltclosure_ptr_t meltclosp_,
						 melt_ptr_t meltfirstargp_,
						 const melt_argdescr_cell_t
						 meltxargdescr_[],
						 union meltparam_un *
						 meltxargtab_,
						 const melt_argdescr_cell_t
						 meltxresdescr_[],
						 union meltparam_un *
						 meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_11_warmelt_normal_NORMEXP_LAZYMACROEXP_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_11_warmelt_normal_NORMEXP_LAZYMACROEXP_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 32
    melt_ptr_t mcfr_varptr[32];
#define MELTFRAM_NBVARNUM 8
    long mcfr_varnum[8];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_11_warmelt_normal_NORMEXP_LAZYMACROEXP is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_11_warmelt_normal_NORMEXP_LAZYMACROEXP_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 32; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_11_warmelt_normal_NORMEXP_LAZYMACROEXP nbval 32*/
  meltfram__.mcfr_nbvar = 32 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("NORMEXP_LAZYMACROEXP", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-normal.melt:1132:/ getarg");
 /*_.RECV__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.ENV__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.ENV__V3*/ meltfptr[2])) != NULL);


  /*getarg#2 */
  /*^getarg */
  if (meltxargdescr_[1] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.NCX__V4*/ meltfptr[3] =
    (meltxargtab_[1].meltbp_aptr) ? (*(meltxargtab_[1].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.NCX__V4*/ meltfptr[3])) != NULL);


  /*getarg#3 */
  /*^getarg */
  if (meltxargdescr_[2] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.PSLOC__V5*/ meltfptr[4] =
    (meltxargtab_[2].meltbp_aptr) ? (*(meltxargtab_[2].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.PSLOC__V5*/ meltfptr[4])) !=
	      NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-normal.melt:1133:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L1*/ meltfnum[0] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.ENV__V3*/ meltfptr[2]),
			     (melt_ptr_t) (( /*!CLASS_ENVIRONMENT */
					    meltfrout->tabval[0])));;
      MELT_LOCATION ("warmelt-normal.melt:1133:/ cond");
      /*cond */ if ( /*_#IS_A__L1*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V7*/ meltfptr[6] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-normal.melt:1133:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check env"),
				  ("warmelt-normal.melt")
				  ? ("warmelt-normal.melt") : __FILE__,
				  (1133) ? (1133) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V7*/ meltfptr[6] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V6*/ meltfptr[5] = /*_.IFELSE___V7*/ meltfptr[6];;
      /*epilog */

      MELT_LOCATION ("warmelt-normal.melt:1133:/ clear");
	     /*clear *//*_#IS_A__L1*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V7*/ meltfptr[6] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V6*/ meltfptr[5] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-normal.melt:1134:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L2*/ meltfnum[0] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.NCX__V4*/ meltfptr[3]),
			     (melt_ptr_t) (( /*!CLASS_NORMALIZATION_CONTEXT */
					    meltfrout->tabval[1])));;
      MELT_LOCATION ("warmelt-normal.melt:1134:/ cond");
      /*cond */ if ( /*_#IS_A__L2*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V9*/ meltfptr[8] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-normal.melt:1134:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check nctxt"),
				  ("warmelt-normal.melt")
				  ? ("warmelt-normal.melt") : __FILE__,
				  (1134) ? (1134) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V9*/ meltfptr[8] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V8*/ meltfptr[6] = /*_.IFELSE___V9*/ meltfptr[8];;
      /*epilog */

      MELT_LOCATION ("warmelt-normal.melt:1134:/ clear");
	     /*clear *//*_#IS_A__L2*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V9*/ meltfptr[8] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V8*/ meltfptr[6] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-normal.melt:1135:/ quasiblock");


    MELT_LOCATION ("warmelt-normal.melt:1136:/ cond");
    /*cond */ if (
		   /*ifisa */
		   melt_is_instance_of ((melt_ptr_t)
					( /*_.RECV__V2*/ meltfptr[1]),
					(melt_ptr_t) (( /*!CLASS_LOCATED */
						       meltfrout->tabval[2])))
      )				/*then */
      {
	/*^cond.then */
	/*^getslot */
	{
	  melt_ptr_t slot = NULL, obj = NULL;
	  obj = (melt_ptr_t) ( /*_.RECV__V2*/ meltfptr[1]) /*=obj*/ ;
	  melt_object_get_field (slot, obj, 1, "LOCA_LOCATION");
   /*_.SLOC__V11*/ meltfptr[10] = slot;
	};
	;
      }
    else
      {				/*^cond.else */

  /*_.SLOC__V11*/ meltfptr[10] = NULL;;
      }
    ;
    MELT_LOCATION ("warmelt-normal.melt:1137:/ cond");
    /*cond */ if (
		   /*ifisa */
		   melt_is_instance_of ((melt_ptr_t)
					( /*_.RECV__V2*/ meltfptr[1]),
					(melt_ptr_t) (( /*!CLASS_SOURCE_LAZY_MACRO_EXPANSION */ meltfrout->tabval[3])))
      )				/*then */
      {
	/*^cond.then */
	/*^getslot */
	{
	  melt_ptr_t slot = NULL, obj = NULL;
	  obj = (melt_ptr_t) ( /*_.RECV__V2*/ meltfptr[1]) /*=obj*/ ;
	  melt_object_get_field (slot, obj, 2, "SLAZYMACRO_FUN");
   /*_.LAZYMACFUN__V12*/ meltfptr[11] = slot;
	};
	;
      }
    else
      {				/*^cond.else */

  /*_.LAZYMACFUN__V12*/ meltfptr[11] = NULL;;
      }
    ;
    MELT_LOCATION ("warmelt-normal.melt:1138:/ cond");
    /*cond */ if (
		   /*ifisa */
		   melt_is_instance_of ((melt_ptr_t)
					( /*_.RECV__V2*/ meltfptr[1]),
					(melt_ptr_t) (( /*!CLASS_SOURCE_LAZY_MACRO_EXPANSION */ meltfrout->tabval[3])))
      )				/*then */
      {
	/*^cond.then */
	/*^getslot */
	{
	  melt_ptr_t slot = NULL, obj = NULL;
	  obj = (melt_ptr_t) ( /*_.RECV__V2*/ meltfptr[1]) /*=obj*/ ;
	  melt_object_get_field (slot, obj, 3, "SLAZYMACRO_OPER");
   /*_.LAZYMACOPER__V13*/ meltfptr[12] = slot;
	};
	;
      }
    else
      {				/*^cond.else */

  /*_.LAZYMACOPER__V13*/ meltfptr[12] = NULL;;
      }
    ;
    MELT_LOCATION ("warmelt-normal.melt:1142:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    MELT_LOCATION ("warmelt-normal.melt:1140:/ quasiblock");


    /*^multiapply */
    /*multiapply 0args, 1x.res */
    {

      union meltparam_un restab[1];
      memset (&restab, 0, sizeof (restab));
      /*^multiapply.xres */
      restab[0].meltbp_aptr =
	(melt_ptr_t *) & /*_.MRESEXP__V16*/ meltfptr[15];
      /*^multiapply.appl */
      /*_.MEXP__V15*/ meltfptr[14] =
	melt_apply ((meltclosure_ptr_t) ( /*_.LAZYMACFUN__V12*/ meltfptr[11]),
		    (melt_ptr_t) (NULL), (""), (union meltparam_un *) 0,
		    (MELTBPARSTR_PTR ""), restab);
    }
    ;
    /*^quasiblock */



#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-normal.melt:1143:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L3*/ meltfnum[0] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-normal.melt:1143:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L3*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L4*/ meltfnum[3] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-normal.melt:1143:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L4*/ meltfnum[3];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-normal.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 1143;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "normexp_lazymacroexp mexp";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.MEXP__V15*/ meltfptr[14];
	      /*_.MELT_DEBUG_FUN__V19*/ meltfptr[18] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[4])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V18*/ meltfptr[17] =
	      /*_.MELT_DEBUG_FUN__V19*/ meltfptr[18];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-normal.melt:1143:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L4*/ meltfnum[3] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V19*/ meltfptr[18] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V18*/ meltfptr[17] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-normal.melt:1143:/ quasiblock");


      /*_.PROGN___V20*/ meltfptr[18] = /*_.IF___V18*/ meltfptr[17];;
      /*^compute */
      /*_.IFCPP___V17*/ meltfptr[16] = /*_.PROGN___V20*/ meltfptr[18];;
      /*epilog */

      MELT_LOCATION ("warmelt-normal.melt:1143:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L3*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IF___V18*/ meltfptr[17] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V20*/ meltfptr[18] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V17*/ meltfptr[16] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-normal.melt:1144:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^cond */
    /*cond */ if ( /*_.MRESEXP__V16*/ meltfptr[15])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{


	  {
	    MELT_LOCATION ("warmelt-normal.melt:1147:/ locexp");
	    /* error_plain */
	      melt_error_str ((melt_ptr_t) ( /*_.SLOC__V11*/ meltfptr[10]),
			      ("delayed lazy macro expansion cannot macro expand multiply"),
			      (melt_ptr_t) 0);
	  }
	  ;
	  /*epilog */
	}
	;
      }				/*noelse */
    ;
    MELT_LOCATION ("warmelt-normal.melt:1148:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#IS_A__L5*/ meltfnum[3] =
      melt_is_instance_of ((melt_ptr_t) ( /*_.MEXP__V15*/ meltfptr[14]),
			   (melt_ptr_t) (( /*!CLASS_SOURCE_LAZY_MACRO_EXPANSION */ meltfrout->tabval[3])));;
    MELT_LOCATION ("warmelt-normal.melt:1148:/ cond");
    /*cond */ if ( /*_#IS_A__L5*/ meltfnum[3])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-normal.melt:1152:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
   /*_#IS_A__L6*/ meltfnum[0] =
	    melt_is_instance_of ((melt_ptr_t)
				 ( /*_.LAZYMACOPER__V13*/ meltfptr[12]),
				 (melt_ptr_t) (( /*!CLASS_SYMBOL */
						meltfrout->tabval[5])));;
	  MELT_LOCATION ("warmelt-normal.melt:1152:/ cond");
	  /*cond */ if ( /*_#IS_A__L6*/ meltfnum[0])	/*then */
	    {
	      /*^cond.then */
	      /*^block */
	      /*anyblock */
	      {

		MELT_LOCATION ("warmelt-normal.melt:1154:/ cond");
		/*cond */ if (
			       /*ifisa */
			       melt_is_instance_of ((melt_ptr_t)
						    ( /*_.LAZYMACOPER__V13*/
						     meltfptr[12]),
						    (melt_ptr_t) (( /*!CLASS_NAMED */ meltfrout->tabval[6])))
		  )		/*then */
		  {
		    /*^cond.then */
		    /*^getslot */
		    {
		      melt_ptr_t slot = NULL, obj = NULL;
		      obj =
			(melt_ptr_t) ( /*_.LAZYMACOPER__V13*/ meltfptr[12])
			/*=obj*/ ;
		      melt_object_get_field (slot, obj, 1, "NAMED_NAME");
       /*_.NAMED_NAME__V22*/ meltfptr[18] = slot;
		    };
		    ;
		  }
		else
		  {		/*^cond.else */

      /*_.NAMED_NAME__V22*/ meltfptr[18] = NULL;;
		  }
		;

		{
		  MELT_LOCATION ("warmelt-normal.melt:1153:/ locexp");
		  melt_error_str ((melt_ptr_t)
				  ( /*_.SLOC__V11*/ meltfptr[10]),
				  ("undefined operator; unknown name"),
				  (melt_ptr_t) ( /*_.NAMED_NAME__V22*/
						meltfptr[18]));
		}
		;
		/*epilog */

		MELT_LOCATION ("warmelt-normal.melt:1152:/ clear");
	       /*clear *//*_.NAMED_NAME__V22*/ meltfptr[18] = 0;
	      }
	      ;
	    }
	  else
	    {			/*^cond.else */

	      /*^block */
	      /*anyblock */
	      {


		{
		  MELT_LOCATION ("warmelt-normal.melt:1155:/ locexp");
		  /* error_plain */
		    melt_error_str ((melt_ptr_t)
				    ( /*_.SLOC__V11*/ meltfptr[10]),
				    ("undefined macro; delayed lazy macro expansion too lazy"),
				    (melt_ptr_t) 0);
		}
		;
		/*epilog */
	      }
	      ;
	    }
	  ;
	  MELT_LOCATION ("warmelt-normal.melt:1156:/ quasiblock");


   /*_.RETVAL___V1*/ meltfptr[0] = NULL;;

	  {
	    MELT_LOCATION ("warmelt-normal.melt:1156:/ locexp");
	    /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
	    if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	      melt_warn_for_no_expected_secondary_results ();
	    /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
	    ;
	  }
	  ;
	  /*^finalreturn */
	  ;
	  /*finalret */ goto labend_rout;
	  MELT_LOCATION ("warmelt-normal.melt:1151:/ quasiblock");


	  /*_.PROGN___V24*/ meltfptr[23] = /*_.RETURN___V23*/ meltfptr[18];;
	  /*^compute */
	  /*_.IF___V21*/ meltfptr[17] = /*_.PROGN___V24*/ meltfptr[23];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-normal.melt:1148:/ clear");
	     /*clear *//*_#IS_A__L6*/ meltfnum[0] = 0;
	  /*^clear */
	     /*clear *//*_.RETURN___V23*/ meltfptr[18] = 0;
	  /*^clear */
	     /*clear *//*_.PROGN___V24*/ meltfptr[23] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

  /*_.IF___V21*/ meltfptr[17] = NULL;;
      }
    ;
    MELT_LOCATION ("warmelt-normal.melt:1160:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    MELT_LOCATION ("warmelt-normal.melt:1158:/ quasiblock");


    /*^multimsend */
    /*multimsend */
    {
      union meltparam_un argtab[3];
      union meltparam_un restab[1];
      memset (&argtab, 0, sizeof (argtab));
      memset (&restab, 0, sizeof (restab));
      /*^multimsend.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.ENV__V3*/ meltfptr[2];	/*^multimsend.arg */
      argtab[1].meltbp_aptr = (melt_ptr_t *) & /*_.NCX__V4*/ meltfptr[3];	/*^multimsend.arg */
      argtab[2].meltbp_aptr = (melt_ptr_t *) & /*_.PSLOC__V5*/ meltfptr[4];
      /*^multimsend.xres */
      restab[0].meltbp_aptr = (melt_ptr_t *) & /*_.NBIND__V27*/ meltfptr[26];	/*^multimsend.send */
      /*_.NREP__V26*/ meltfptr[23] =
	meltgc_send ((melt_ptr_t) ( /*_.MEXP__V15*/ meltfptr[14]),
		     ((melt_ptr_t)
		      (( /*!NORMAL_EXP */ meltfrout->tabval[7]))),
		     (MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_PTR ""),
		     argtab, (MELTBPARSTR_PTR ""), restab);
    }
    ;
    /*^quasiblock */



#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-normal.melt:1161:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L7*/ meltfnum[0] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-normal.melt:1161:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L7*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L8*/ meltfnum[7] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-normal.melt:1161:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[7];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L8*/ meltfnum[7];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-normal.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 1161;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "normexp_lazymacroexp nrep=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.NREP__V26*/ meltfptr[23];
	      /*^apply.arg */
	      argtab[5].meltbp_cstring = " nbind=";
	      /*^apply.arg */
	      argtab[6].meltbp_aptr =
		(melt_ptr_t *) & /*_.NBIND__V27*/ meltfptr[26];
	      /*_.MELT_DEBUG_FUN__V30*/ meltfptr[29] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[4])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V29*/ meltfptr[28] =
	      /*_.MELT_DEBUG_FUN__V30*/ meltfptr[29];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-normal.melt:1161:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L8*/ meltfnum[7] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V30*/ meltfptr[29] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V29*/ meltfptr[28] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-normal.melt:1161:/ quasiblock");


      /*_.PROGN___V31*/ meltfptr[29] = /*_.IF___V29*/ meltfptr[28];;
      /*^compute */
      /*_.IFCPP___V28*/ meltfptr[27] = /*_.PROGN___V31*/ meltfptr[29];;
      /*epilog */

      MELT_LOCATION ("warmelt-normal.melt:1161:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L7*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IF___V29*/ meltfptr[28] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V31*/ meltfptr[29] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V28*/ meltfptr[27] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-normal.melt:1162:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.NREP__V26*/ meltfptr[23];;
    MELT_LOCATION ("warmelt-normal.melt:1162:/ putxtraresult");
    if (!meltxrestab_ || !meltxresdescr_)
      goto labend_rout;
    if (meltxresdescr_[0] != MELTBPAR_PTR)
      goto labend_rout;
    if (meltxrestab_[0].meltbp_aptr)
      *(meltxrestab_[0].meltbp_aptr) =
	(melt_ptr_t) ( /*_.NBIND__V27*/ meltfptr[26]);
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    MELT_LOCATION ("warmelt-normal.melt:1158:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*_.MULTI___V25*/ meltfptr[18] = /*_.RETURN___V32*/ meltfptr[28];;

    MELT_LOCATION ("warmelt-normal.melt:1158:/ clear");
	   /*clear *//*_.IFCPP___V28*/ meltfptr[27] = 0;
    /*^clear */
	   /*clear *//*_.RETURN___V32*/ meltfptr[28] = 0;

    /*^clear */
	   /*clear *//*_.NBIND__V27*/ meltfptr[26] = 0;
    MELT_LOCATION ("warmelt-normal.melt:1140:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*_.MULTI___V14*/ meltfptr[13] = /*_.MULTI___V25*/ meltfptr[18];;

    MELT_LOCATION ("warmelt-normal.melt:1140:/ clear");
	   /*clear *//*_.IFCPP___V17*/ meltfptr[16] = 0;
    /*^clear */
	   /*clear *//*_#IS_A__L5*/ meltfnum[3] = 0;
    /*^clear */
	   /*clear *//*_.IF___V21*/ meltfptr[17] = 0;
    /*^clear */
	   /*clear *//*_.MULTI___V25*/ meltfptr[18] = 0;

    /*^clear */
	   /*clear *//*_.MRESEXP__V16*/ meltfptr[15] = 0;
    /*_.LET___V10*/ meltfptr[8] = /*_.MULTI___V14*/ meltfptr[13];;

    MELT_LOCATION ("warmelt-normal.melt:1135:/ clear");
	   /*clear *//*_.SLOC__V11*/ meltfptr[10] = 0;
    /*^clear */
	   /*clear *//*_.LAZYMACFUN__V12*/ meltfptr[11] = 0;
    /*^clear */
	   /*clear *//*_.LAZYMACOPER__V13*/ meltfptr[12] = 0;
    /*^clear */
	   /*clear *//*_.MULTI___V14*/ meltfptr[13] = 0;
    MELT_LOCATION ("warmelt-normal.melt:1132:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.LET___V10*/ meltfptr[8];;

    {
      MELT_LOCATION ("warmelt-normal.melt:1132:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V6*/ meltfptr[5] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V8*/ meltfptr[6] = 0;
    /*^clear */
	   /*clear *//*_.LET___V10*/ meltfptr[8] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("NORMEXP_LAZYMACROEXP", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_11_warmelt_normal_NORMEXP_LAZYMACROEXP_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_11_warmelt_normal_NORMEXP_LAZYMACROEXP */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_12_warmelt_normal_GECTYP_ANYRECV (meltclosure_ptr_t meltclosp_,
					   melt_ptr_t meltfirstargp_,
					   const melt_argdescr_cell_t
					   meltxargdescr_[],
					   union meltparam_un * meltxargtab_,
					   const melt_argdescr_cell_t
					   meltxresdescr_[],
					   union meltparam_un * meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_12_warmelt_normal_GECTYP_ANYRECV_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_12_warmelt_normal_GECTYP_ANYRECV_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 3
    melt_ptr_t mcfr_varptr[3];
/*no varnum*/
#define MELTFRAM_NBVARNUM /*none*/0
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_12_warmelt_normal_GECTYP_ANYRECV is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_12_warmelt_normal_GECTYP_ANYRECV_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 3; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_12_warmelt_normal_GECTYP_ANYRECV nbval 3*/
  meltfram__.mcfr_nbvar = 3 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("GECTYP_ANYRECV", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-normal.melt:1184:/ getarg");
 /*_.RECV__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.ENV__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.ENV__V3*/ meltfptr[2])) != NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {

    /*_.RETVAL___V1*/ meltfptr[0] =
      ( /*!CTYPE_VALUE */ meltfrout->tabval[0]);;

    {
      MELT_LOCATION ("warmelt-normal.melt:1184:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("GECTYP_ANYRECV", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_12_warmelt_normal_GECTYP_ANYRECV_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_12_warmelt_normal_GECTYP_ANYRECV */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_13_warmelt_normal_GECTYP_ROOT (meltclosure_ptr_t meltclosp_,
					melt_ptr_t meltfirstargp_,
					const melt_argdescr_cell_t
					meltxargdescr_[],
					union meltparam_un * meltxargtab_,
					const melt_argdescr_cell_t
					meltxresdescr_[],
					union meltparam_un * meltxrestab_)
{
  long current_blocklevel_signals_meltrout_13_warmelt_normal_GECTYP_ROOT_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_13_warmelt_normal_GECTYP_ROOT_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 3
    melt_ptr_t mcfr_varptr[3];
/*no varnum*/
#define MELTFRAM_NBVARNUM /*none*/0
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_13_warmelt_normal_GECTYP_ROOT is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_13_warmelt_normal_GECTYP_ROOT_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 3; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_13_warmelt_normal_GECTYP_ROOT nbval 3*/
  meltfram__.mcfr_nbvar = 3 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("GECTYP_ROOT", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-normal.melt:1187:/ getarg");
 /*_.RECV__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.ENV__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.ENV__V3*/ meltfptr[2])) != NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {

    /*_.RETVAL___V1*/ meltfptr[0] =
      ( /*!CTYPE_VALUE */ meltfrout->tabval[0]);;

    {
      MELT_LOCATION ("warmelt-normal.melt:1187:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("GECTYP_ROOT", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_13_warmelt_normal_GECTYP_ROOT_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_13_warmelt_normal_GECTYP_ROOT */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_14_warmelt_normal_GECTYP_INTEGER (meltclosure_ptr_t meltclosp_,
					   melt_ptr_t meltfirstargp_,
					   const melt_argdescr_cell_t
					   meltxargdescr_[],
					   union meltparam_un * meltxargtab_,
					   const melt_argdescr_cell_t
					   meltxresdescr_[],
					   union meltparam_un * meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_14_warmelt_normal_GECTYP_INTEGER_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_14_warmelt_normal_GECTYP_INTEGER_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 7
    melt_ptr_t mcfr_varptr[7];
#define MELTFRAM_NBVARNUM 2
    long mcfr_varnum[2];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_14_warmelt_normal_GECTYP_INTEGER is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_14_warmelt_normal_GECTYP_INTEGER_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 7; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_14_warmelt_normal_GECTYP_INTEGER nbval 7*/
  meltfram__.mcfr_nbvar = 7 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("GECTYP_INTEGER", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-normal.melt:1191:/ getarg");
 /*_.RECV__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.ENV__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.ENV__V3*/ meltfptr[2])) != NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-normal.melt:1192:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L1*/ meltfnum[0] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-normal.melt:1192:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L1*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-normal.melt:1192:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-normal.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 1192;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "gectyp_integer recv";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.RECV__V2*/ meltfptr[1];
	      /*_.MELT_DEBUG_FUN__V6*/ meltfptr[5] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V5*/ meltfptr[4] = /*_.MELT_DEBUG_FUN__V6*/ meltfptr[5];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-normal.melt:1192:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V6*/ meltfptr[5] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V5*/ meltfptr[4] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-normal.melt:1192:/ quasiblock");


      /*_.PROGN___V7*/ meltfptr[5] = /*_.IF___V5*/ meltfptr[4];;
      /*^compute */
      /*_.IFCPP___V4*/ meltfptr[3] = /*_.PROGN___V7*/ meltfptr[5];;
      /*epilog */

      MELT_LOCATION ("warmelt-normal.melt:1192:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L1*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IF___V5*/ meltfptr[4] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V7*/ meltfptr[5] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V4*/ meltfptr[3] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-normal.melt:1191:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = ( /*!CTYPE_LONG */ meltfrout->tabval[1]);;

    {
      MELT_LOCATION ("warmelt-normal.melt:1191:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V4*/ meltfptr[3] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("GECTYP_INTEGER", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_14_warmelt_normal_GECTYP_INTEGER_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_14_warmelt_normal_GECTYP_INTEGER */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_15_warmelt_normal_GECTYP_STRING (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un * meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un * meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_15_warmelt_normal_GECTYP_STRING_melt =
    melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_15_warmelt_normal_GECTYP_STRING_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 3
    melt_ptr_t mcfr_varptr[3];
/*no varnum*/
#define MELTFRAM_NBVARNUM /*none*/0
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_15_warmelt_normal_GECTYP_STRING is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_15_warmelt_normal_GECTYP_STRING_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 3; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_15_warmelt_normal_GECTYP_STRING nbval 3*/
  meltfram__.mcfr_nbvar = 3 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("GECTYP_STRING", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-normal.melt:1197:/ getarg");
 /*_.RECV__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.ENV__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.ENV__V3*/ meltfptr[2])) != NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {

    /*_.RETVAL___V1*/ meltfptr[0] =
      ( /*!CTYPE_CSTRING */ meltfrout->tabval[0]);;

    {
      MELT_LOCATION ("warmelt-normal.melt:1197:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("GECTYP_STRING", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_15_warmelt_normal_GECTYP_STRING_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_15_warmelt_normal_GECTYP_STRING */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_16_warmelt_normal_NORMALIZE_TUPLE (meltclosure_ptr_t meltclosp_,
					    melt_ptr_t meltfirstargp_,
					    const melt_argdescr_cell_t
					    meltxargdescr_[],
					    union meltparam_un * meltxargtab_,
					    const melt_argdescr_cell_t
					    meltxresdescr_[],
					    union meltparam_un * meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_16_warmelt_normal_NORMALIZE_TUPLE_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_16_warmelt_normal_NORMALIZE_TUPLE_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 30
    melt_ptr_t mcfr_varptr[30];
#define MELTFRAM_NBVARNUM 10
    long mcfr_varnum[10];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_16_warmelt_normal_NORMALIZE_TUPLE is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_16_warmelt_normal_NORMALIZE_TUPLE_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 30; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_16_warmelt_normal_NORMALIZE_TUPLE nbval 30*/
  meltfram__.mcfr_nbvar = 30 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("NORMALIZE_TUPLE", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-normal.melt:1202:/ getarg");
 /*_.TUP__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.ENV__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.ENV__V3*/ meltfptr[2])) != NULL);


  /*getarg#2 */
  /*^getarg */
  if (meltxargdescr_[1] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.NCX__V4*/ meltfptr[3] =
    (meltxargtab_[1].meltbp_aptr) ? (*(meltxargtab_[1].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.NCX__V4*/ meltfptr[3])) != NULL);


  /*getarg#3 */
  /*^getarg */
  if (meltxargdescr_[2] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.PSLOC__V5*/ meltfptr[4] =
    (meltxargtab_[2].meltbp_aptr) ? (*(meltxargtab_[2].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.PSLOC__V5*/ meltfptr[4])) !=
	      NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-normal.melt:1203:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L1*/ meltfnum[0] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-normal.melt:1203:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L1*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-normal.melt:1203:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[7];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-normal.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 1203;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "normalize_tuple tup=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.TUP__V2*/ meltfptr[1];
	      /*^apply.arg */
	      argtab[5].meltbp_cstring = " psloc=";
	      /*^apply.arg */
	      argtab[6].meltbp_aptr =
		(melt_ptr_t *) & /*_.PSLOC__V5*/ meltfptr[4];
	      /*_.MELT_DEBUG_FUN__V8*/ meltfptr[7] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V7*/ meltfptr[6] = /*_.MELT_DEBUG_FUN__V8*/ meltfptr[7];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-normal.melt:1203:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V8*/ meltfptr[7] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V7*/ meltfptr[6] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-normal.melt:1203:/ quasiblock");


      /*_.PROGN___V9*/ meltfptr[7] = /*_.IF___V7*/ meltfptr[6];;
      /*^compute */
      /*_.IFCPP___V6*/ meltfptr[5] = /*_.PROGN___V9*/ meltfptr[7];;
      /*epilog */

      MELT_LOCATION ("warmelt-normal.melt:1203:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L1*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IF___V7*/ meltfptr[6] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V9*/ meltfptr[7] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V6*/ meltfptr[5] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

    {
      MELT_LOCATION ("warmelt-normal.melt:1204:/ locexp");

#if MELT_HAVE_DEBUG
      if (melt_need_debug (0))
	melt_dbgshortbacktrace (("normalize_tuple"), (8));
#endif
      ;
    }
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-normal.melt:1205:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L3*/ meltfnum[1] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.ENV__V3*/ meltfptr[2]),
			     (melt_ptr_t) (( /*!CLASS_ENVIRONMENT */
					    meltfrout->tabval[1])));;
      MELT_LOCATION ("warmelt-normal.melt:1205:/ cond");
      /*cond */ if ( /*_#IS_A__L3*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V11*/ meltfptr[7] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-normal.melt:1205:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check env"),
				  ("warmelt-normal.melt")
				  ? ("warmelt-normal.melt") : __FILE__,
				  (1205) ? (1205) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V11*/ meltfptr[7] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V10*/ meltfptr[6] = /*_.IFELSE___V11*/ meltfptr[7];;
      /*epilog */

      MELT_LOCATION ("warmelt-normal.melt:1205:/ clear");
	     /*clear *//*_#IS_A__L3*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V11*/ meltfptr[7] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V10*/ meltfptr[6] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-normal.melt:1206:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L4*/ meltfnum[0] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.NCX__V4*/ meltfptr[3]),
			     (melt_ptr_t) (( /*!CLASS_NORMALIZATION_CONTEXT */
					    meltfrout->tabval[2])));;
      MELT_LOCATION ("warmelt-normal.melt:1206:/ cond");
      /*cond */ if ( /*_#IS_A__L4*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V13*/ meltfptr[12] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-normal.melt:1206:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check nctxt"),
				  ("warmelt-normal.melt")
				  ? ("warmelt-normal.melt") : __FILE__,
				  (1206) ? (1206) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V13*/ meltfptr[12] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V12*/ meltfptr[7] = /*_.IFELSE___V13*/ meltfptr[12];;
      /*epilog */

      MELT_LOCATION ("warmelt-normal.melt:1206:/ clear");
	     /*clear *//*_#IS_A__L4*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V13*/ meltfptr[12] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V12*/ meltfptr[7] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-normal.melt:1207:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#NULL__L5*/ meltfnum[1] =
      (( /*_.TUP__V2*/ meltfptr[1]) == NULL);;
    MELT_LOCATION ("warmelt-normal.melt:1207:/ cond");
    /*cond */ if ( /*_#NULL__L5*/ meltfnum[1])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  /*^checksignal */
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^quasiblock */


	  /*_.RETVAL___V1*/ meltfptr[0] = ( /*nil */ NULL);;
	  MELT_LOCATION ("warmelt-normal.melt:1207:/ putxtraresult");
	  if (!meltxrestab_ || !meltxresdescr_)
	    goto labend_rout;
	  if (meltxresdescr_[0] != MELTBPAR_PTR)
	    goto labend_rout;
	  if (meltxrestab_[0].meltbp_aptr)
	    *(meltxrestab_[0].meltbp_aptr) = (melt_ptr_t) (( /*nil */ NULL));
	  ;
	  /*^finalreturn */
	  ;
	  /*finalret */ goto labend_rout;
	  /*_.IF___V14*/ meltfptr[12] = /*_.RETURN___V15*/ meltfptr[14];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-normal.melt:1207:/ clear");
	     /*clear *//*_.RETURN___V15*/ meltfptr[14] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

  /*_.IF___V14*/ meltfptr[12] = NULL;;
      }
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-normal.melt:1208:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_MULTIPLE__L6*/ meltfnum[0] =
	(melt_magic_discr ((melt_ptr_t) ( /*_.TUP__V2*/ meltfptr[1])) ==
	 MELTOBMAG_MULTIPLE);;
      MELT_LOCATION ("warmelt-normal.melt:1208:/ cond");
      /*cond */ if ( /*_#IS_MULTIPLE__L6*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V17*/ meltfptr[16] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-normal.melt:1208:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check tup"),
				  ("warmelt-normal.melt")
				  ? ("warmelt-normal.melt") : __FILE__,
				  (1208) ? (1208) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V17*/ meltfptr[16] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V16*/ meltfptr[14] = /*_.IFELSE___V17*/ meltfptr[16];;
      /*epilog */

      MELT_LOCATION ("warmelt-normal.melt:1208:/ clear");
	     /*clear *//*_#IS_MULTIPLE__L6*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V17*/ meltfptr[16] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V16*/ meltfptr[14] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-normal.melt:1209:/ quasiblock");


 /*_.BINDLIST__V19*/ meltfptr[18] =
      (meltgc_new_list
       ((meltobject_ptr_t) (( /*!DISCR_LIST */ meltfrout->tabval[3]))));;
    MELT_LOCATION ("warmelt-normal.melt:1212:/ quasiblock");


    /*^newclosure */
		 /*newclosure *//*_.LAMBDA___V21*/ meltfptr[20] =
      (melt_ptr_t)
      meltgc_new_closure ((meltobject_ptr_t)
			  (((melt_ptr_t) (MELT_PREDEF (DISCR_CLOSURE)))),
			  (meltroutine_ptr_t) (( /*!konst_10 */ meltfrout->
						tabval[10])), (4));
    ;
    /*^putclosedv */
    /*putclosv */
    melt_assertmsg ("putclosv checkclo",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.LAMBDA___V21*/ meltfptr[20])) ==
		    MELTOBMAG_CLOSURE);
    melt_assertmsg ("putclosv checkoff", 0 >= 0
		    && 0 <
		    melt_closure_size ((melt_ptr_t)
				       ( /*_.LAMBDA___V21*/ meltfptr[20])));
    ((meltclosure_ptr_t) /*_.LAMBDA___V21*/ meltfptr[20])->tabval[0] =
      (melt_ptr_t) ( /*_.ENV__V3*/ meltfptr[2]);
    ;
    /*^putclosedv */
    /*putclosv */
    melt_assertmsg ("putclosv checkclo",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.LAMBDA___V21*/ meltfptr[20])) ==
		    MELTOBMAG_CLOSURE);
    melt_assertmsg ("putclosv checkoff", 1 >= 0
		    && 1 <
		    melt_closure_size ((melt_ptr_t)
				       ( /*_.LAMBDA___V21*/ meltfptr[20])));
    ((meltclosure_ptr_t) /*_.LAMBDA___V21*/ meltfptr[20])->tabval[1] =
      (melt_ptr_t) ( /*_.NCX__V4*/ meltfptr[3]);
    ;
    /*^putclosedv */
    /*putclosv */
    melt_assertmsg ("putclosv checkclo",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.LAMBDA___V21*/ meltfptr[20])) ==
		    MELTOBMAG_CLOSURE);
    melt_assertmsg ("putclosv checkoff", 2 >= 0
		    && 2 <
		    melt_closure_size ((melt_ptr_t)
				       ( /*_.LAMBDA___V21*/ meltfptr[20])));
    ((meltclosure_ptr_t) /*_.LAMBDA___V21*/ meltfptr[20])->tabval[2] =
      (melt_ptr_t) ( /*_.PSLOC__V5*/ meltfptr[4]);
    ;
    /*^putclosedv */
    /*putclosv */
    melt_assertmsg ("putclosv checkclo",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.LAMBDA___V21*/ meltfptr[20])) ==
		    MELTOBMAG_CLOSURE);
    melt_assertmsg ("putclosv checkoff", 3 >= 0
		    && 3 <
		    melt_closure_size ((melt_ptr_t)
				       ( /*_.LAMBDA___V21*/ meltfptr[20])));
    ((meltclosure_ptr_t) /*_.LAMBDA___V21*/ meltfptr[20])->tabval[3] =
      (melt_ptr_t) ( /*_.BINDLIST__V19*/ meltfptr[18]);
    ;
    /*_.LAMBDA___V20*/ meltfptr[19] = /*_.LAMBDA___V21*/ meltfptr[20];;
    MELT_LOCATION ("warmelt-normal.melt:1210:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & /*_.LAMBDA___V20*/ meltfptr[19];
      /*_.RES__V22*/ meltfptr[21] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!MULTIPLE_MAP */ meltfrout->tabval[4])),
		    (melt_ptr_t) ( /*_.TUP__V2*/ meltfptr[1]),
		    (MELTBPARSTR_PTR ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-normal.melt:1232:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_.LIST_FIRST__V23*/ meltfptr[22] =
      (melt_list_first ((melt_ptr_t) ( /*_.BINDLIST__V19*/ meltfptr[18])));;
    /*^compute */
 /*_#IS_PAIR__L7*/ meltfnum[0] =
      (melt_magic_discr ((melt_ptr_t) ( /*_.LIST_FIRST__V23*/ meltfptr[22]))
       == MELTOBMAG_PAIR);;
    /*^compute */
 /*_#NOT__L8*/ meltfnum[7] =
      (!( /*_#IS_PAIR__L7*/ meltfnum[0]));;
    MELT_LOCATION ("warmelt-normal.melt:1232:/ cond");
    /*cond */ if ( /*_#NOT__L8*/ meltfnum[7])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-normal.melt:1233:/ compute");
	  /*_.BINDLIST__V19*/ meltfptr[18] = /*_.SETQ___V25*/ meltfptr[24] =
	    ( /*nil */ NULL);;
	  /*_.IF___V24*/ meltfptr[23] = /*_.SETQ___V25*/ meltfptr[24];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-normal.melt:1232:/ clear");
	     /*clear *//*_.SETQ___V25*/ meltfptr[24] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

  /*_.IF___V24*/ meltfptr[23] = NULL;;
      }
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-normal.melt:1234:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L9*/ meltfnum[8] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-normal.melt:1234:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L9*/ meltfnum[8])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L10*/ meltfnum[9] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-normal.melt:1234:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[7];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long =
		/*_#THE_MELTCALLCOUNT__L10*/ meltfnum[9];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-normal.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 1234;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "normalize_tuple res=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.RES__V22*/ meltfptr[21];
	      /*^apply.arg */
	      argtab[5].meltbp_cstring = " bindlist=";
	      /*^apply.arg */
	      argtab[6].meltbp_aptr =
		(melt_ptr_t *) & /*_.BINDLIST__V19*/ meltfptr[18];
	      /*_.MELT_DEBUG_FUN__V28*/ meltfptr[27] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V27*/ meltfptr[26] =
	      /*_.MELT_DEBUG_FUN__V28*/ meltfptr[27];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-normal.melt:1234:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L10*/ meltfnum[9] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V28*/ meltfptr[27] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V27*/ meltfptr[26] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-normal.melt:1234:/ quasiblock");


      /*_.PROGN___V29*/ meltfptr[27] = /*_.IF___V27*/ meltfptr[26];;
      /*^compute */
      /*_.IFCPP___V26*/ meltfptr[24] = /*_.PROGN___V29*/ meltfptr[27];;
      /*epilog */

      MELT_LOCATION ("warmelt-normal.melt:1234:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L9*/ meltfnum[8] = 0;
      /*^clear */
	     /*clear *//*_.IF___V27*/ meltfptr[26] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V29*/ meltfptr[27] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V26*/ meltfptr[24] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-normal.melt:1235:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.RES__V22*/ meltfptr[21];;
    MELT_LOCATION ("warmelt-normal.melt:1235:/ putxtraresult");
    if (!meltxrestab_ || !meltxresdescr_)
      goto labend_rout;
    if (meltxresdescr_[0] != MELTBPAR_PTR)
      goto labend_rout;
    if (meltxrestab_[0].meltbp_aptr)
      *(meltxrestab_[0].meltbp_aptr) =
	(melt_ptr_t) ( /*_.BINDLIST__V19*/ meltfptr[18]);
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*_.LET___V18*/ meltfptr[16] = /*_.RETURN___V30*/ meltfptr[26];;

    MELT_LOCATION ("warmelt-normal.melt:1209:/ clear");
	   /*clear *//*_.BINDLIST__V19*/ meltfptr[18] = 0;
    /*^clear */
	   /*clear *//*_.LAMBDA___V20*/ meltfptr[19] = 0;
    /*^clear */
	   /*clear *//*_.RES__V22*/ meltfptr[21] = 0;
    /*^clear */
	   /*clear *//*_.LIST_FIRST__V23*/ meltfptr[22] = 0;
    /*^clear */
	   /*clear *//*_#IS_PAIR__L7*/ meltfnum[0] = 0;
    /*^clear */
	   /*clear *//*_#NOT__L8*/ meltfnum[7] = 0;
    /*^clear */
	   /*clear *//*_.IF___V24*/ meltfptr[23] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V26*/ meltfptr[24] = 0;
    /*^clear */
	   /*clear *//*_.RETURN___V30*/ meltfptr[26] = 0;
    MELT_LOCATION ("warmelt-normal.melt:1202:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.LET___V18*/ meltfptr[16];;

    {
      MELT_LOCATION ("warmelt-normal.melt:1202:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V6*/ meltfptr[5] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V10*/ meltfptr[6] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V12*/ meltfptr[7] = 0;
    /*^clear */
	   /*clear *//*_#NULL__L5*/ meltfnum[1] = 0;
    /*^clear */
	   /*clear *//*_.IF___V14*/ meltfptr[12] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V16*/ meltfptr[14] = 0;
    /*^clear */
	   /*clear *//*_.LET___V18*/ meltfptr[16] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("NORMALIZE_TUPLE", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_16_warmelt_normal_NORMALIZE_TUPLE_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_16_warmelt_normal_NORMALIZE_TUPLE */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_17_warmelt_normal_LAMBDA___1__ (meltclosure_ptr_t meltclosp_,
					 melt_ptr_t meltfirstargp_,
					 const melt_argdescr_cell_t
					 meltxargdescr_[],
					 union meltparam_un * meltxargtab_,
					 const melt_argdescr_cell_t
					 meltxresdescr_[],
					 union meltparam_un * meltxrestab_)
{
  long current_blocklevel_signals_meltrout_17_warmelt_normal_LAMBDA___1___melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_17_warmelt_normal_LAMBDA___1___st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 21
    melt_ptr_t mcfr_varptr[21];
#define MELTFRAM_NBVARNUM 8
    long mcfr_varnum[8];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_17_warmelt_normal_LAMBDA___1__ is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_17_warmelt_normal_LAMBDA___1___st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 21; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_17_warmelt_normal_LAMBDA___1__ nbval 21*/
  meltfram__.mcfr_nbvar = 21 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("LAMBDA_", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-normal.melt:1212:/ getarg");
 /*_.COMP__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_LONG)
    goto lab_endgetargs;
 /*_#IX__L1*/ meltfnum[0] = meltxargtab_[0].meltbp_long;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-normal.melt:1213:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L2*/ meltfnum[1] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-normal.melt:1213:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L2*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L3*/ meltfnum[2] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-normal.melt:1213:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[7];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L3*/ meltfnum[2];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-normal.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 1213;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "normalize_tuple comp=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.COMP__V2*/ meltfptr[1];
	      /*^apply.arg */
	      argtab[5].meltbp_cstring = " ix=";
	      /*^apply.arg */
	      argtab[6].meltbp_long = /*_#IX__L1*/ meltfnum[0];
	      /*_.MELT_DEBUG_FUN__V5*/ meltfptr[4] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V4*/ meltfptr[3] = /*_.MELT_DEBUG_FUN__V5*/ meltfptr[4];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-normal.melt:1213:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L3*/ meltfnum[2] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V5*/ meltfptr[4] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V4*/ meltfptr[3] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-normal.melt:1213:/ quasiblock");


      /*_.PROGN___V6*/ meltfptr[4] = /*_.IF___V4*/ meltfptr[3];;
      /*^compute */
      /*_.IFCPP___V3*/ meltfptr[2] = /*_.PROGN___V6*/ meltfptr[4];;
      /*epilog */

      MELT_LOCATION ("warmelt-normal.melt:1213:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L2*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IF___V4*/ meltfptr[3] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V6*/ meltfptr[4] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V3*/ meltfptr[2] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-normal.melt:1216:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    MELT_LOCATION ("warmelt-normal.melt:1214:/ quasiblock");


    /*^multimsend */
    /*multimsend */
    {
      union meltparam_un argtab[3];
      union meltparam_un restab[1];
      memset (&argtab, 0, sizeof (argtab));
      memset (&restab, 0, sizeof (restab));
      /*^multimsend.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & ( /*~ENV */ meltfclos->tabval[0]);	/*^multimsend.arg */
      argtab[1].meltbp_aptr = (melt_ptr_t *) & ( /*~NCX */ meltfclos->tabval[1]);	/*^multimsend.arg */
      argtab[2].meltbp_aptr =
	(melt_ptr_t *) & ( /*~PSLOC */ meltfclos->tabval[2]);
      /*^multimsend.xres */
      restab[0].meltbp_aptr = (melt_ptr_t *) & /*_.NBINDS__V9*/ meltfptr[8];	/*^multimsend.send */
      /*_.NORCOMP__V8*/ meltfptr[4] =
	meltgc_send ((melt_ptr_t) ( /*_.COMP__V2*/ meltfptr[1]),
		     ((melt_ptr_t)
		      (( /*!NORMAL_EXP */ meltfrout->tabval[1]))),
		     (MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_PTR ""),
		     argtab, (MELTBPARSTR_PTR ""), restab);
    }
    ;
    /*^quasiblock */



#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-normal.melt:1217:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L4*/ meltfnum[2] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-normal.melt:1217:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L4*/ meltfnum[2])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L5*/ meltfnum[1] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-normal.melt:1217:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[11];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L5*/ meltfnum[1];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-normal.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 1217;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "normalize_tuple norcomp=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.NORCOMP__V8*/ meltfptr[4];
	      /*^apply.arg */
	      argtab[5].meltbp_cstring = " nbinds=";
	      /*^apply.arg */
	      argtab[6].meltbp_aptr =
		(melt_ptr_t *) & /*_.NBINDS__V9*/ meltfptr[8];
	      /*^apply.arg */
	      argtab[7].meltbp_cstring = " for comp=";
	      /*^apply.arg */
	      argtab[8].meltbp_aptr =
		(melt_ptr_t *) & /*_.COMP__V2*/ meltfptr[1];
	      /*^apply.arg */
	      argtab[9].meltbp_cstring = " ix=";
	      /*^apply.arg */
	      argtab[10].meltbp_long = /*_#IX__L1*/ meltfnum[0];
	      /*_.MELT_DEBUG_FUN__V12*/ meltfptr[11] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V11*/ meltfptr[10] =
	      /*_.MELT_DEBUG_FUN__V12*/ meltfptr[11];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-normal.melt:1217:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L5*/ meltfnum[1] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V12*/ meltfptr[11] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V11*/ meltfptr[10] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-normal.melt:1217:/ quasiblock");


      /*_.PROGN___V13*/ meltfptr[11] = /*_.IF___V11*/ meltfptr[10];;
      /*^compute */
      /*_.IFCPP___V10*/ meltfptr[9] = /*_.PROGN___V13*/ meltfptr[11];;
      /*epilog */

      MELT_LOCATION ("warmelt-normal.melt:1217:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L4*/ meltfnum[2] = 0;
      /*^clear */
	     /*clear *//*_.IF___V11*/ meltfptr[10] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V13*/ meltfptr[11] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V10*/ meltfptr[9] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-normal.melt:1218:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_LIST_OR_NULL__L6*/ meltfnum[1] =
	(( /*_.NBINDS__V9*/ meltfptr[8]) == NULL
	 ||
	 (melt_unsafe_magic_discr
	  ((melt_ptr_t) ( /*_.NBINDS__V9*/ meltfptr[8])) == MELTOBMAG_LIST));;
      MELT_LOCATION ("warmelt-normal.melt:1218:/ cond");
      /*cond */ if ( /*_#IS_LIST_OR_NULL__L6*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V15*/ meltfptr[11] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-normal.melt:1218:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check nbinds"),
				  ("warmelt-normal.melt")
				  ? ("warmelt-normal.melt") : __FILE__,
				  (1218) ? (1218) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V15*/ meltfptr[11] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V14*/ meltfptr[10] = /*_.IFELSE___V15*/ meltfptr[11];;
      /*epilog */

      MELT_LOCATION ("warmelt-normal.melt:1218:/ clear");
	     /*clear *//*_#IS_LIST_OR_NULL__L6*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V15*/ meltfptr[11] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V14*/ meltfptr[10] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-normal.melt:1219:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_NOT_A__L7*/ meltfnum[2] =
	!melt_is_instance_of ((melt_ptr_t) ( /*_.NORCOMP__V8*/ meltfptr[4]),
			      (melt_ptr_t) (( /*!CLASS_NREP_EXPRESSION */
					     meltfrout->tabval[2])));;
      MELT_LOCATION ("warmelt-normal.melt:1219:/ cond");
      /*cond */ if ( /*_#IS_NOT_A__L7*/ meltfnum[2])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V17*/ meltfptr[16] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-normal.melt:1219:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check norcomp not nrep_expr"),
				  ("warmelt-normal.melt")
				  ? ("warmelt-normal.melt") : __FILE__,
				  (1219) ? (1219) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V17*/ meltfptr[16] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V16*/ meltfptr[11] = /*_.IFELSE___V17*/ meltfptr[16];;
      /*epilog */

      MELT_LOCATION ("warmelt-normal.melt:1219:/ clear");
	     /*clear *//*_#IS_NOT_A__L7*/ meltfnum[2] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V17*/ meltfptr[16] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V16*/ meltfptr[11] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-normal.melt:1220:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#IS_LIST__L8*/ meltfnum[1] =
      (melt_magic_discr ((melt_ptr_t) ( /*_.NBINDS__V9*/ meltfptr[8])) ==
       MELTOBMAG_LIST);;
    MELT_LOCATION ("warmelt-normal.melt:1220:/ cond");
    /*cond */ if ( /*_#IS_LIST__L8*/ meltfnum[1])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-normal.melt:1223:/ quasiblock");


	  /*^newclosure */
		   /*newclosure *//*_.LAMBDA___V20*/ meltfptr[19] =
	    (melt_ptr_t)
	    meltgc_new_closure ((meltobject_ptr_t)
				(((melt_ptr_t)
				  (MELT_PREDEF (DISCR_CLOSURE)))),
				(meltroutine_ptr_t) (( /*!konst_5 */
						      meltfrout->tabval[5])),
				(1));
	  ;
	  /*^putclosedv */
	  /*putclosv */
	  melt_assertmsg ("putclosv checkclo",
			  melt_magic_discr ((melt_ptr_t)
					    ( /*_.LAMBDA___V20*/
					     meltfptr[19])) ==
			  MELTOBMAG_CLOSURE);
	  melt_assertmsg ("putclosv checkoff", 0 >= 0
			  && 0 <
			  melt_closure_size ((melt_ptr_t)
					     ( /*_.LAMBDA___V20*/
					      meltfptr[19])));
	  ((meltclosure_ptr_t) /*_.LAMBDA___V20*/ meltfptr[19])->tabval[0] =
	    (melt_ptr_t) (( /*~BINDLIST */ meltfclos->tabval[3]));
	  ;
	  /*_.LAMBDA___V19*/ meltfptr[18] = /*_.LAMBDA___V20*/ meltfptr[19];;
	  MELT_LOCATION ("warmelt-normal.melt:1221:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^apply */
	  /*apply */
	  {
	    union meltparam_un argtab[1];
	    memset (&argtab, 0, sizeof (argtab));
	    /*^apply.arg */
	    argtab[0].meltbp_aptr =
	      (melt_ptr_t *) & /*_.LAMBDA___V19*/ meltfptr[18];
	    /*_.LIST_EVERY__V21*/ meltfptr[20] =
	      melt_apply ((meltclosure_ptr_t)
			  (( /*!LIST_EVERY */ meltfrout->tabval[3])),
			  (melt_ptr_t) ( /*_.NBINDS__V9*/ meltfptr[8]),
			  (MELTBPARSTR_PTR ""), argtab, "",
			  (union meltparam_un *) 0);
	  }
	  ;
	  /*_.IF___V18*/ meltfptr[16] = /*_.LIST_EVERY__V21*/ meltfptr[20];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-normal.melt:1220:/ clear");
	     /*clear *//*_.LAMBDA___V19*/ meltfptr[18] = 0;
	  /*^clear */
	     /*clear *//*_.LIST_EVERY__V21*/ meltfptr[20] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

  /*_.IF___V18*/ meltfptr[16] = NULL;;
      }
    ;
    MELT_LOCATION ("warmelt-normal.melt:1214:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*_.MULTI___V7*/ meltfptr[3] = /*_.NORCOMP__V8*/ meltfptr[4];;

    MELT_LOCATION ("warmelt-normal.melt:1214:/ clear");
	   /*clear *//*_.IFCPP___V10*/ meltfptr[9] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V14*/ meltfptr[10] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V16*/ meltfptr[11] = 0;
    /*^clear */
	   /*clear *//*_#IS_LIST__L8*/ meltfnum[1] = 0;
    /*^clear */
	   /*clear *//*_.IF___V18*/ meltfptr[16] = 0;

    /*^clear */
	   /*clear *//*_.NBINDS__V9*/ meltfptr[8] = 0;
    MELT_LOCATION ("warmelt-normal.melt:1212:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.MULTI___V7*/ meltfptr[3];;

    {
      MELT_LOCATION ("warmelt-normal.melt:1212:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V3*/ meltfptr[2] = 0;
    /*^clear */
	   /*clear *//*_.MULTI___V7*/ meltfptr[3] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("LAMBDA_", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_17_warmelt_normal_LAMBDA___1___melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_17_warmelt_normal_LAMBDA___1__ */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_18_warmelt_normal_LAMBDA___2__ (meltclosure_ptr_t meltclosp_,
					 melt_ptr_t meltfirstargp_,
					 const melt_argdescr_cell_t
					 meltxargdescr_[],
					 union meltparam_un * meltxargtab_,
					 const melt_argdescr_cell_t
					 meltxresdescr_[],
					 union meltparam_un * meltxrestab_)
{
  long current_blocklevel_signals_meltrout_18_warmelt_normal_LAMBDA___2___melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_18_warmelt_normal_LAMBDA___2___st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 6
    melt_ptr_t mcfr_varptr[6];
#define MELTFRAM_NBVARNUM 2
    long mcfr_varnum[2];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_18_warmelt_normal_LAMBDA___2__ is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_18_warmelt_normal_LAMBDA___2___st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 6; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_18_warmelt_normal_LAMBDA___2__ nbval 6*/
  meltfram__.mcfr_nbvar = 6 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("LAMBDA_", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-normal.melt:1223:/ getarg");
 /*_.BND__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-normal.melt:1224:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L1*/ meltfnum[0] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.BND__V2*/ meltfptr[1]),
			     (melt_ptr_t) (( /*!CLASS_ANY_BINDING */
					    meltfrout->tabval[0])));;
      MELT_LOCATION ("warmelt-normal.melt:1224:/ cond");
      /*cond */ if ( /*_#IS_A__L1*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V4*/ meltfptr[3] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-normal.melt:1224:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check bnd"),
				  ("warmelt-normal.melt")
				  ? ("warmelt-normal.melt") : __FILE__,
				  (1224) ? (1224) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V4*/ meltfptr[3] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V3*/ meltfptr[2] = /*_.IFELSE___V4*/ meltfptr[3];;
      /*epilog */

      MELT_LOCATION ("warmelt-normal.melt:1224:/ clear");
	     /*clear *//*_#IS_A__L1*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V4*/ meltfptr[3] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V3*/ meltfptr[2] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-normal.melt:1225:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_LIST__L2*/ meltfnum[0] =
	(melt_magic_discr
	 ((melt_ptr_t) (( /*~BINDLIST */ meltfclos->tabval[0]))) ==
	 MELTOBMAG_LIST);;
      MELT_LOCATION ("warmelt-normal.melt:1225:/ cond");
      /*cond */ if ( /*_#IS_LIST__L2*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V6*/ meltfptr[5] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-normal.melt:1225:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check bindlist"),
				  ("warmelt-normal.melt")
				  ? ("warmelt-normal.melt") : __FILE__,
				  (1225) ? (1225) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V6*/ meltfptr[5] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V5*/ meltfptr[3] = /*_.IFELSE___V6*/ meltfptr[5];;
      /*epilog */

      MELT_LOCATION ("warmelt-normal.melt:1225:/ clear");
	     /*clear *//*_#IS_LIST__L2*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V6*/ meltfptr[5] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V5*/ meltfptr[3] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

    {
      MELT_LOCATION ("warmelt-normal.melt:1226:/ locexp");
      meltgc_append_list ((melt_ptr_t)
			  (( /*~BINDLIST */ meltfclos->tabval[0])),
			  (melt_ptr_t) ( /*_.BND__V2*/ meltfptr[1]));
    }
    ;
    MELT_LOCATION ("warmelt-normal.melt:1223:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V3*/ meltfptr[2] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V5*/ meltfptr[3] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("LAMBDA_", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_18_warmelt_normal_LAMBDA___2___melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*noretval */ NULL);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_18_warmelt_normal_LAMBDA___2__ */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_19_warmelt_normal_WRAP_NORMAL_LET1 (meltclosure_ptr_t meltclosp_,
					     melt_ptr_t meltfirstargp_,
					     const melt_argdescr_cell_t
					     meltxargdescr_[],
					     union meltparam_un *
					     meltxargtab_,
					     const melt_argdescr_cell_t
					     meltxresdescr_[],
					     union meltparam_un *
					     meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_19_warmelt_normal_WRAP_NORMAL_LET1_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_19_warmelt_normal_WRAP_NORMAL_LET1_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 17
    melt_ptr_t mcfr_varptr[17];
#define MELTFRAM_NBVARNUM 4
    long mcfr_varnum[4];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_19_warmelt_normal_WRAP_NORMAL_LET1 is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_19_warmelt_normal_WRAP_NORMAL_LET1_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 17; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_19_warmelt_normal_WRAP_NORMAL_LET1 nbval 17*/
  meltfram__.mcfr_nbvar = 17 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("WRAP_NORMAL_LET1", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-normal.melt:1240:/ getarg");
 /*_.NEXP__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.BINDLIST__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.BINDLIST__V3*/ meltfptr[2])) !=
	      NULL);


  /*getarg#2 */
  /*^getarg */
  if (meltxargdescr_[1] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.LOC__V4*/ meltfptr[3] =
    (meltxargtab_[1].meltbp_aptr) ? (*(meltxargtab_[1].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.LOC__V4*/ meltfptr[3])) != NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-normal.melt:1241:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_LIST_OR_NULL__L1*/ meltfnum[0] =
	(( /*_.BINDLIST__V3*/ meltfptr[2]) == NULL
	 ||
	 (melt_unsafe_magic_discr
	  ((melt_ptr_t) ( /*_.BINDLIST__V3*/ meltfptr[2])) ==
	  MELTOBMAG_LIST));;
      MELT_LOCATION ("warmelt-normal.melt:1241:/ cond");
      /*cond */ if ( /*_#IS_LIST_OR_NULL__L1*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V6*/ meltfptr[5] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-normal.melt:1241:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check bindlist"),
				  ("warmelt-normal.melt")
				  ? ("warmelt-normal.melt") : __FILE__,
				  (1241) ? (1241) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V6*/ meltfptr[5] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V5*/ meltfptr[4] = /*_.IFELSE___V6*/ meltfptr[5];;
      /*epilog */

      MELT_LOCATION ("warmelt-normal.melt:1241:/ clear");
	     /*clear *//*_#IS_LIST_OR_NULL__L1*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V6*/ meltfptr[5] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V5*/ meltfptr[4] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-normal.melt:1244:/ quasiblock");


    /*^newclosure */
		 /*newclosure *//*_.LAMBDA___V8*/ meltfptr[7] =
      (melt_ptr_t)
      meltgc_new_closure ((meltobject_ptr_t)
			  (((melt_ptr_t) (MELT_PREDEF (DISCR_CLOSURE)))),
			  (meltroutine_ptr_t) (( /*!konst_3 */ meltfrout->
						tabval[3])), (2));
    ;
    /*^putclosedv */
    /*putclosv */
    melt_assertmsg ("putclosv checkclo",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.LAMBDA___V8*/ meltfptr[7])) ==
		    MELTOBMAG_CLOSURE);
    melt_assertmsg ("putclosv checkoff", 0 >= 0
		    && 0 <
		    melt_closure_size ((melt_ptr_t)
				       ( /*_.LAMBDA___V8*/ meltfptr[7])));
    ((meltclosure_ptr_t) /*_.LAMBDA___V8*/ meltfptr[7])->tabval[0] =
      (melt_ptr_t) ( /*_.NEXP__V2*/ meltfptr[1]);
    ;
    /*^putclosedv */
    /*putclosv */
    melt_assertmsg ("putclosv checkclo",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.LAMBDA___V8*/ meltfptr[7])) ==
		    MELTOBMAG_CLOSURE);
    melt_assertmsg ("putclosv checkoff", 1 >= 0
		    && 1 <
		    melt_closure_size ((melt_ptr_t)
				       ( /*_.LAMBDA___V8*/ meltfptr[7])));
    ((meltclosure_ptr_t) /*_.LAMBDA___V8*/ meltfptr[7])->tabval[1] =
      (melt_ptr_t) ( /*_.BINDLIST__V3*/ meltfptr[2]);
    ;
    /*_.LAMBDA___V7*/ meltfptr[5] = /*_.LAMBDA___V8*/ meltfptr[7];;
    MELT_LOCATION ("warmelt-normal.melt:1242:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.LAMBDA___V7*/ meltfptr[5];
      /*_.LIST_EVERY__V9*/ meltfptr[8] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!LIST_EVERY */ meltfrout->tabval[0])),
		    (melt_ptr_t) ( /*_.BINDLIST__V3*/ meltfptr[2]),
		    (MELTBPARSTR_PTR ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-normal.melt:1250:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    MELT_LOCATION ("warmelt-normal.melt:1251:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#IS_LIST__L2*/ meltfnum[0] =
      (melt_magic_discr ((melt_ptr_t) ( /*_.BINDLIST__V3*/ meltfptr[2])) ==
       MELTOBMAG_LIST);;
    MELT_LOCATION ("warmelt-normal.melt:1251:/ cond");
    /*cond */ if ( /*_#IS_LIST__L2*/ meltfnum[0])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

   /*_.LIST_FIRST__V10*/ meltfptr[9] =
	    (melt_list_first
	     ((melt_ptr_t) ( /*_.BINDLIST__V3*/ meltfptr[2])));;
	  /*^compute */
   /*_#IS_PAIR__L4*/ meltfnum[3] =
	    (melt_magic_discr
	     ((melt_ptr_t) ( /*_.LIST_FIRST__V10*/ meltfptr[9])) ==
	     MELTOBMAG_PAIR);;
	  /*^compute */
	  /*_#IF___L3*/ meltfnum[2] = /*_#IS_PAIR__L4*/ meltfnum[3];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-normal.melt:1251:/ clear");
	     /*clear *//*_.LIST_FIRST__V10*/ meltfptr[9] = 0;
	  /*^clear */
	     /*clear *//*_#IS_PAIR__L4*/ meltfnum[3] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

  /*_#IF___L3*/ meltfnum[2] = 0;;
      }
    ;
    MELT_LOCATION ("warmelt-normal.melt:1250:/ cond");
    /*cond */ if ( /*_#IF___L3*/ meltfnum[2])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-normal.melt:1253:/ quasiblock");


	  MELT_LOCATION ("warmelt-normal.melt:1254:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  MELT_LOCATION ("warmelt-normal.melt:1256:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^apply */
	  /*apply */
	  {
	    /*_.LIST_TO_MULTIPLE__V13*/ meltfptr[12] =
	      melt_apply ((meltclosure_ptr_t)
			  (( /*!LIST_TO_MULTIPLE */ meltfrout->tabval[5])),
			  (melt_ptr_t) ( /*_.BINDLIST__V3*/ meltfptr[2]),
			  (""), (union meltparam_un *) 0, "",
			  (union meltparam_un *) 0);
	  }
	  ;
	  MELT_LOCATION ("warmelt-normal.melt:1257:/ blockmultialloc");
	  /*multiallocblock */
	  {
	    struct meltletrec_1_st
	    {
	      struct MELT_MULTIPLE_STRUCT (1) rtup_0__TUPLREC__x1;
	      long meltletrec_1_endgap;
	    } *meltletrec_1_ptr = 0;
	    meltletrec_1_ptr =
	      (struct meltletrec_1_st *)
	      meltgc_allocate (sizeof (struct meltletrec_1_st), 0);
	    /*^blockmultialloc.initfill */
	    /*inimult rtup_0__TUPLREC__x1 */
 /*_.TUPLREC___V15*/ meltfptr[14] =
	      (melt_ptr_t) & meltletrec_1_ptr->rtup_0__TUPLREC__x1;
	    meltletrec_1_ptr->rtup_0__TUPLREC__x1.discr =
	      (meltobject_ptr_t) (((melt_ptr_t)
				   (MELT_PREDEF (DISCR_MULTIPLE))));
	    meltletrec_1_ptr->rtup_0__TUPLREC__x1.nbval = 1;


	    /*^putuple */
	    /*putupl#1 */
	    melt_assertmsg ("putupl [:1257] #1 checktup",
			    melt_magic_discr ((melt_ptr_t)
					      ( /*_.TUPLREC___V15*/
					       meltfptr[14])) ==
			    MELTOBMAG_MULTIPLE);
	    melt_assertmsg ("putupl [:1257] #1 checkoff",
			    (0 >= 0
			     && 0 <
			     melt_multiple_length ((melt_ptr_t)
						   ( /*_.TUPLREC___V15*/
						    meltfptr[14]))));
	    ((meltmultiple_ptr_t) ( /*_.TUPLREC___V15*/ meltfptr[14]))->
	      tabval[0] = (melt_ptr_t) ( /*_.NEXP__V2*/ meltfptr[1]);
	    ;
	    /*^touch */
	    meltgc_touch ( /*_.TUPLREC___V15*/ meltfptr[14]);
	    ;
	    /*_.TUPLE___V14*/ meltfptr[13] =
	      /*_.TUPLREC___V15*/ meltfptr[14];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-normal.melt:1257:/ clear");
	      /*clear *//*_.TUPLREC___V15*/ meltfptr[14] = 0;
	    /*^clear */
	      /*clear *//*_.TUPLREC___V15*/ meltfptr[14] = 0;
	  }			/*end multiallocblock */
	  ;
	  MELT_LOCATION ("warmelt-normal.melt:1254:/ quasiblock");


	  /*^rawallocobj */
	  /*rawallocobj */
	  {
	    melt_ptr_t newobj = 0;
	    melt_raw_object_create (newobj,
				    (melt_ptr_t) (( /*!CLASS_NREP_LET */
						   meltfrout->tabval[4])),
				    (3), "CLASS_NREP_LET");
    /*_.INST__V17*/ meltfptr[16] =
	      newobj;
	  };
	  ;
	  /*^putslot */
	  /*putslot */
	  melt_assertmsg ("putslot checkobj @NREP_LOC",
			  melt_magic_discr ((melt_ptr_t)
					    ( /*_.INST__V17*/ meltfptr[16]))
			  == MELTOBMAG_OBJECT);
	  melt_putfield_object (( /*_.INST__V17*/ meltfptr[16]), (0),
				( /*_.LOC__V4*/ meltfptr[3]), "NREP_LOC");
	  ;
	  /*^putslot */
	  /*putslot */
	  melt_assertmsg ("putslot checkobj @NLET_BINDINGS",
			  melt_magic_discr ((melt_ptr_t)
					    ( /*_.INST__V17*/ meltfptr[16]))
			  == MELTOBMAG_OBJECT);
	  melt_putfield_object (( /*_.INST__V17*/ meltfptr[16]), (1),
				( /*_.LIST_TO_MULTIPLE__V13*/ meltfptr[12]),
				"NLET_BINDINGS");
	  ;
	  /*^putslot */
	  /*putslot */
	  melt_assertmsg ("putslot checkobj @NLET_BODY",
			  melt_magic_discr ((melt_ptr_t)
					    ( /*_.INST__V17*/ meltfptr[16]))
			  == MELTOBMAG_OBJECT);
	  melt_putfield_object (( /*_.INST__V17*/ meltfptr[16]), (2),
				( /*_.TUPLE___V14*/ meltfptr[13]),
				"NLET_BODY");
	  ;
	  /*^touchobj */

	  melt_dbgtrace_written_object ( /*_.INST__V17*/ meltfptr[16],
					"newly made instance");
	  ;
	  /*_.WNLET__V16*/ meltfptr[14] = /*_.INST__V17*/ meltfptr[16];;
	  /*^compute */
	  /*_.LET___V12*/ meltfptr[11] = /*_.WNLET__V16*/ meltfptr[14];;

	  MELT_LOCATION ("warmelt-normal.melt:1253:/ clear");
	     /*clear *//*_.LIST_TO_MULTIPLE__V13*/ meltfptr[12] = 0;
	  /*^clear */
	     /*clear *//*_.TUPLE___V14*/ meltfptr[13] = 0;
	  /*^clear */
	     /*clear *//*_.WNLET__V16*/ meltfptr[14] = 0;
	  /*_.IFELSE___V11*/ meltfptr[9] = /*_.LET___V12*/ meltfptr[11];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-normal.melt:1250:/ clear");
	     /*clear *//*_.LET___V12*/ meltfptr[11] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

	/*_.IFELSE___V11*/ meltfptr[9] = /*_.NEXP__V2*/ meltfptr[1];;
      }
    ;
    MELT_LOCATION ("warmelt-normal.melt:1240:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.IFELSE___V11*/ meltfptr[9];;

    {
      MELT_LOCATION ("warmelt-normal.melt:1240:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V5*/ meltfptr[4] = 0;
    /*^clear */
	   /*clear *//*_.LAMBDA___V7*/ meltfptr[5] = 0;
    /*^clear */
	   /*clear *//*_.LIST_EVERY__V9*/ meltfptr[8] = 0;
    /*^clear */
	   /*clear *//*_#IS_LIST__L2*/ meltfnum[0] = 0;
    /*^clear */
	   /*clear *//*_#IF___L3*/ meltfnum[2] = 0;
    /*^clear */
	   /*clear *//*_.IFELSE___V11*/ meltfptr[9] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("WRAP_NORMAL_LET1", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_19_warmelt_normal_WRAP_NORMAL_LET1_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_19_warmelt_normal_WRAP_NORMAL_LET1 */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_20_warmelt_normal_LAMBDA___3__ (meltclosure_ptr_t meltclosp_,
					 melt_ptr_t meltfirstargp_,
					 const melt_argdescr_cell_t
					 meltxargdescr_[],
					 union meltparam_un * meltxargtab_,
					 const melt_argdescr_cell_t
					 meltxresdescr_[],
					 union meltparam_un * meltxrestab_)
{
  long current_blocklevel_signals_meltrout_20_warmelt_normal_LAMBDA___3___melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_20_warmelt_normal_LAMBDA___3___st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 9
    melt_ptr_t mcfr_varptr[9];
#define MELTFRAM_NBVARNUM 5
    long mcfr_varnum[5];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_20_warmelt_normal_LAMBDA___3__ is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_20_warmelt_normal_LAMBDA___3___st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 9; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_20_warmelt_normal_LAMBDA___3__ nbval 9*/
  meltfram__.mcfr_nbvar = 9 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("LAMBDA_", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-normal.melt:1244:/ getarg");
 /*_.CBIND__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {

    MELT_LOCATION ("warmelt-normal.melt:1245:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#IS_A__L1*/ meltfnum[0] =
      melt_is_instance_of ((melt_ptr_t) ( /*_.CBIND__V2*/ meltfptr[1]),
			   (melt_ptr_t) (( /*!CLASS_NORMAL_LET_BINDING */
					  meltfrout->tabval[0])));;
    /*^compute */
 /*_#NOT__L2*/ meltfnum[1] =
      (!( /*_#IS_A__L1*/ meltfnum[0]));;
    MELT_LOCATION ("warmelt-normal.melt:1245:/ cond");
    /*cond */ if ( /*_#NOT__L2*/ meltfnum[1])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{


#if MELT_HAVE_DEBUG
	  MELT_LOCATION ("warmelt-normal.melt:1246:/ cppif.then");
	  /*^block */
	  /*anyblock */
	  {


	    {
	      /*^locexp */
	      /*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	      melt_dbgcounter++;
#endif
	      ;
	    }
	    ;
	    /*^checksignal */
	    MELT_CHECK_SIGNAL ();
	    ;
     /*_#MELT_NEED_DBG__L3*/ meltfnum[2] =
	      /*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	      ( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	      0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	      ;;
	    MELT_LOCATION ("warmelt-normal.melt:1246:/ cond");
	    /*cond */ if ( /*_#MELT_NEED_DBG__L3*/ meltfnum[2])	/*then */
	      {
		/*^cond.then */
		/*^block */
		/*anyblock */
		{

       /*_#THE_MELTCALLCOUNT__L4*/ meltfnum[3] =
#ifdef meltcallcount
		    meltcallcount	/* the_meltcallcount */
#else
		    0L
#endif /* meltcallcount the_meltcallcount */
		    ;;
		  MELT_LOCATION ("warmelt-normal.melt:1246:/ checksignal");
		  MELT_CHECK_SIGNAL ();
		  ;
		  /*^apply */
		  /*apply */
		  {
		    union meltparam_un argtab[9];
		    memset (&argtab, 0, sizeof (argtab));
		    /*^apply.arg */
		    argtab[0].meltbp_long =
		      /*_#THE_MELTCALLCOUNT__L4*/ meltfnum[3];
		    /*^apply.arg */
		    argtab[1].meltbp_cstring = "warmelt-normal.melt";
		    /*^apply.arg */
		    argtab[2].meltbp_long = 1246;
		    /*^apply.arg */
		    argtab[3].meltbp_cstring = "wrap_normal_let1 nexp=";
		    /*^apply.arg */
		    argtab[4].meltbp_aptr =
		      (melt_ptr_t *) & ( /*~NEXP */ meltfclos->tabval[0]);
		    /*^apply.arg */
		    argtab[5].meltbp_cstring = " bindlist=";
		    /*^apply.arg */
		    argtab[6].meltbp_aptr =
		      (melt_ptr_t *) & ( /*~BINDLIST */ meltfclos->tabval[1]);
		    /*^apply.arg */
		    argtab[7].meltbp_cstring = " cbind";
		    /*^apply.arg */
		    argtab[8].meltbp_aptr =
		      (melt_ptr_t *) & /*_.CBIND__V2*/ meltfptr[1];
		    /*_.MELT_DEBUG_FUN__V6*/ meltfptr[5] =
		      melt_apply ((meltclosure_ptr_t)
				  (( /*!MELT_DEBUG_FUN */ meltfrout->
				    tabval[1])),
				  (melt_ptr_t) (( /*nil */ NULL)),
				  (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_PTR MELTBPARSTR_CSTRING
				   MELTBPARSTR_PTR MELTBPARSTR_CSTRING
				   MELTBPARSTR_PTR ""), argtab, "",
				  (union meltparam_un *) 0);
		  }
		  ;
		  /*_.IF___V5*/ meltfptr[4] =
		    /*_.MELT_DEBUG_FUN__V6*/ meltfptr[5];;
		  /*epilog */

		  MELT_LOCATION ("warmelt-normal.melt:1246:/ clear");
		 /*clear *//*_#THE_MELTCALLCOUNT__L4*/ meltfnum[3] = 0;
		  /*^clear */
		 /*clear *//*_.MELT_DEBUG_FUN__V6*/ meltfptr[5] = 0;
		}
		;
	      }
	    else
	      {			/*^cond.else */

      /*_.IF___V5*/ meltfptr[4] = NULL;;
	      }
	    ;
	    MELT_LOCATION ("warmelt-normal.melt:1246:/ quasiblock");


	    /*_.PROGN___V7*/ meltfptr[5] = /*_.IF___V5*/ meltfptr[4];;
	    /*^compute */
	    /*_.IFCPP___V4*/ meltfptr[3] = /*_.PROGN___V7*/ meltfptr[5];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-normal.melt:1246:/ clear");
	       /*clear *//*_#MELT_NEED_DBG__L3*/ meltfnum[2] = 0;
	    /*^clear */
	       /*clear *//*_.IF___V5*/ meltfptr[4] = 0;
	    /*^clear */
	       /*clear *//*_.PROGN___V7*/ meltfptr[5] = 0;
	  }

#else /*MELT_HAVE_DEBUG */
	  /*^cppif.else */
	  /*_.IFCPP___V4*/ meltfptr[3] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
	  ;
	  /*^compute */
	  /*_.IF___V3*/ meltfptr[2] = /*_.IFCPP___V4*/ meltfptr[3];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-normal.melt:1245:/ clear");
	     /*clear *//*_.IFCPP___V4*/ meltfptr[3] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

  /*_.IF___V3*/ meltfptr[2] = NULL;;
      }
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-normal.melt:1249:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L5*/ meltfnum[3] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.CBIND__V2*/ meltfptr[1]),
			     (melt_ptr_t) (( /*!CLASS_NORMAL_LET_BINDING */
					    meltfrout->tabval[0])));;
      MELT_LOCATION ("warmelt-normal.melt:1249:/ cond");
      /*cond */ if ( /*_#IS_A__L5*/ meltfnum[3])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V9*/ meltfptr[5] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-normal.melt:1249:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check cbind wrapnormlet1"),
				  ("warmelt-normal.melt")
				  ? ("warmelt-normal.melt") : __FILE__,
				  (1249) ? (1249) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V9*/ meltfptr[5] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V8*/ meltfptr[4] = /*_.IFELSE___V9*/ meltfptr[5];;
      /*epilog */

      MELT_LOCATION ("warmelt-normal.melt:1249:/ clear");
	     /*clear *//*_#IS_A__L5*/ meltfnum[3] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V9*/ meltfptr[5] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V8*/ meltfptr[4] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-normal.melt:1244:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.IFCPP___V8*/ meltfptr[4];;

    {
      MELT_LOCATION ("warmelt-normal.melt:1244:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_#IS_A__L1*/ meltfnum[0] = 0;
    /*^clear */
	   /*clear *//*_#NOT__L2*/ meltfnum[1] = 0;
    /*^clear */
	   /*clear *//*_.IF___V3*/ meltfptr[2] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V8*/ meltfptr[4] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("LAMBDA_", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_20_warmelt_normal_LAMBDA___3___melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_20_warmelt_normal_LAMBDA___3__ */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_21_warmelt_normal_WRAP_NORMAL_LETSEQ (meltclosure_ptr_t meltclosp_,
					       melt_ptr_t meltfirstargp_,
					       const melt_argdescr_cell_t
					       meltxargdescr_[],
					       union meltparam_un *
					       meltxargtab_,
					       const melt_argdescr_cell_t
					       meltxresdescr_[],
					       union meltparam_un *
					       meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_21_warmelt_normal_WRAP_NORMAL_LETSEQ_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_21_warmelt_normal_WRAP_NORMAL_LETSEQ_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 53
    melt_ptr_t mcfr_varptr[53];
#define MELTFRAM_NBVARNUM 22
    long mcfr_varnum[22];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_21_warmelt_normal_WRAP_NORMAL_LETSEQ is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_21_warmelt_normal_WRAP_NORMAL_LETSEQ_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 53; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_21_warmelt_normal_WRAP_NORMAL_LETSEQ nbval 53*/
  meltfram__.mcfr_nbvar = 53 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("WRAP_NORMAL_LETSEQ", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-normal.melt:1265:/ getarg");
 /*_.TUPNEXP__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.BINDLIST__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.BINDLIST__V3*/ meltfptr[2])) !=
	      NULL);


  /*getarg#2 */
  /*^getarg */
  if (meltxargdescr_[1] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.LOC__V4*/ meltfptr[3] =
    (meltxargtab_[1].meltbp_aptr) ? (*(meltxargtab_[1].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.LOC__V4*/ meltfptr[3])) != NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-normal.melt:1266:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L1*/ meltfnum[0] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-normal.melt:1266:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L1*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-normal.melt:1266:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[9];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-normal.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 1266;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "wrap_normal_letseq tupnexp=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.TUPNEXP__V2*/ meltfptr[1];
	      /*^apply.arg */
	      argtab[5].meltbp_cstring = " bindlist=";
	      /*^apply.arg */
	      argtab[6].meltbp_aptr =
		(melt_ptr_t *) & /*_.BINDLIST__V3*/ meltfptr[2];
	      /*^apply.arg */
	      argtab[7].meltbp_cstring = " loc=";
	      /*^apply.arg */
	      argtab[8].meltbp_aptr =
		(melt_ptr_t *) & /*_.LOC__V4*/ meltfptr[3];
	      /*_.MELT_DEBUG_FUN__V7*/ meltfptr[6] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V6*/ meltfptr[5] = /*_.MELT_DEBUG_FUN__V7*/ meltfptr[6];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-normal.melt:1266:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V7*/ meltfptr[6] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V6*/ meltfptr[5] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-normal.melt:1266:/ quasiblock");


      /*_.PROGN___V8*/ meltfptr[6] = /*_.IF___V6*/ meltfptr[5];;
      /*^compute */
      /*_.IFCPP___V5*/ meltfptr[4] = /*_.PROGN___V8*/ meltfptr[6];;
      /*epilog */

      MELT_LOCATION ("warmelt-normal.melt:1266:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L1*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IF___V6*/ meltfptr[5] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V8*/ meltfptr[6] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V5*/ meltfptr[4] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

    {
      MELT_LOCATION ("warmelt-normal.melt:1267:/ locexp");

#if MELT_HAVE_DEBUG
      if (melt_need_debug (0))
	melt_dbgshortbacktrace (("wrap_normal_letseq"), (6));
#endif
      ;
    }
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-normal.melt:1268:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_MULTIPLE_OR_NULL__L3*/ meltfnum[1] =
	(( /*_.TUPNEXP__V2*/ meltfptr[1]) == NULL
	 ||
	 (melt_unsafe_magic_discr
	  ((melt_ptr_t) ( /*_.TUPNEXP__V2*/ meltfptr[1])) ==
	  MELTOBMAG_MULTIPLE));;
      MELT_LOCATION ("warmelt-normal.melt:1268:/ cond");
      /*cond */ if ( /*_#IS_MULTIPLE_OR_NULL__L3*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V10*/ meltfptr[6] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-normal.melt:1268:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check tupnexp"),
				  ("warmelt-normal.melt")
				  ? ("warmelt-normal.melt") : __FILE__,
				  (1268) ? (1268) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V10*/ meltfptr[6] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V9*/ meltfptr[5] = /*_.IFELSE___V10*/ meltfptr[6];;
      /*epilog */

      MELT_LOCATION ("warmelt-normal.melt:1268:/ clear");
	     /*clear *//*_#IS_MULTIPLE_OR_NULL__L3*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V10*/ meltfptr[6] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V9*/ meltfptr[5] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-normal.melt:1269:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_LIST_OR_NULL__L4*/ meltfnum[0] =
	(( /*_.BINDLIST__V3*/ meltfptr[2]) == NULL
	 ||
	 (melt_unsafe_magic_discr
	  ((melt_ptr_t) ( /*_.BINDLIST__V3*/ meltfptr[2])) ==
	  MELTOBMAG_LIST));;
      MELT_LOCATION ("warmelt-normal.melt:1269:/ cond");
      /*cond */ if ( /*_#IS_LIST_OR_NULL__L4*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V12*/ meltfptr[11] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-normal.melt:1269:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check bindlist"),
				  ("warmelt-normal.melt")
				  ? ("warmelt-normal.melt") : __FILE__,
				  (1269) ? (1269) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V12*/ meltfptr[11] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V11*/ meltfptr[6] = /*_.IFELSE___V12*/ meltfptr[11];;
      /*epilog */

      MELT_LOCATION ("warmelt-normal.melt:1269:/ clear");
	     /*clear *//*_#IS_LIST_OR_NULL__L4*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V12*/ meltfptr[11] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V11*/ meltfptr[6] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-normal.melt:1270:/ quasiblock");


 /*_#NBNEXP__L5*/ meltfnum[1] =
      (melt_multiple_length ((melt_ptr_t) ( /*_.TUPNEXP__V2*/ meltfptr[1])));;
    MELT_LOCATION ("warmelt-normal.melt:1273:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#IS_MULTIPLE__L6*/ meltfnum[0] =
      (melt_magic_discr ((melt_ptr_t) ( /*_.TUPNEXP__V2*/ meltfptr[1])) ==
       MELTOBMAG_MULTIPLE);;
    /*^compute */
 /*_#NOT__L7*/ meltfnum[6] =
      (!( /*_#IS_MULTIPLE__L6*/ meltfnum[0]));;
    MELT_LOCATION ("warmelt-normal.melt:1273:/ cond");
    /*cond */ if ( /*_#NOT__L7*/ meltfnum[6])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-normal.melt:1274:/ quasiblock");


	  /*^checksignal */
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^apply */
	  /*apply */
	  {
	    union meltparam_un argtab[2];
	    memset (&argtab, 0, sizeof (argtab));
	    /*^apply.arg */
	    argtab[0].meltbp_aptr =
	      (melt_ptr_t *) & /*_.BINDLIST__V3*/ meltfptr[2];
	    /*^apply.arg */
	    argtab[1].meltbp_aptr =
	      (melt_ptr_t *) & /*_.LOC__V4*/ meltfptr[3];
	    /*_.WNLETN__V16*/ meltfptr[15] =
	      melt_apply ((meltclosure_ptr_t)
			  (( /*!WRAP_NORMAL_LET1 */ meltfrout->tabval[1])),
			  (melt_ptr_t) ( /*_.TUPNEXP__V2*/ meltfptr[1]),
			  (MELTBPARSTR_PTR MELTBPARSTR_PTR ""), argtab, "",
			  (union meltparam_un *) 0);
	  }
	  ;

#if MELT_HAVE_DEBUG
	  MELT_LOCATION ("warmelt-normal.melt:1276:/ cppif.then");
	  /*^block */
	  /*anyblock */
	  {


	    {
	      /*^locexp */
	      /*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	      melt_dbgcounter++;
#endif
	      ;
	    }
	    ;
	    /*^checksignal */
	    MELT_CHECK_SIGNAL ();
	    ;
     /*_#MELT_NEED_DBG__L8*/ meltfnum[7] =
	      /*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	      ( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	      0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	      ;;
	    MELT_LOCATION ("warmelt-normal.melt:1276:/ cond");
	    /*cond */ if ( /*_#MELT_NEED_DBG__L8*/ meltfnum[7])	/*then */
	      {
		/*^cond.then */
		/*^block */
		/*anyblock */
		{

       /*_#THE_MELTCALLCOUNT__L9*/ meltfnum[8] =
#ifdef meltcallcount
		    meltcallcount	/* the_meltcallcount */
#else
		    0L
#endif /* meltcallcount the_meltcallcount */
		    ;;
		  MELT_LOCATION ("warmelt-normal.melt:1276:/ checksignal");
		  MELT_CHECK_SIGNAL ();
		  ;
		  /*^apply */
		  /*apply */
		  {
		    union meltparam_un argtab[7];
		    memset (&argtab, 0, sizeof (argtab));
		    /*^apply.arg */
		    argtab[0].meltbp_long =
		      /*_#THE_MELTCALLCOUNT__L9*/ meltfnum[8];
		    /*^apply.arg */
		    argtab[1].meltbp_cstring = "warmelt-normal.melt";
		    /*^apply.arg */
		    argtab[2].meltbp_long = 1276;
		    /*^apply.arg */
		    argtab[3].meltbp_cstring =
		      "wrap_normal_letseq non-tuple tupnexp=";
		    /*^apply.arg */
		    argtab[4].meltbp_aptr =
		      (melt_ptr_t *) & /*_.TUPNEXP__V2*/ meltfptr[1];
		    /*^apply.arg */
		    argtab[5].meltbp_cstring = "\n return wnletn=";
		    /*^apply.arg */
		    argtab[6].meltbp_aptr =
		      (melt_ptr_t *) & /*_.WNLETN__V16*/ meltfptr[15];
		    /*_.MELT_DEBUG_FUN__V19*/ meltfptr[18] =
		      melt_apply ((meltclosure_ptr_t)
				  (( /*!MELT_DEBUG_FUN */ meltfrout->
				    tabval[0])),
				  (melt_ptr_t) (( /*nil */ NULL)),
				  (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_PTR MELTBPARSTR_CSTRING
				   MELTBPARSTR_PTR ""), argtab, "",
				  (union meltparam_un *) 0);
		  }
		  ;
		  /*_.IF___V18*/ meltfptr[17] =
		    /*_.MELT_DEBUG_FUN__V19*/ meltfptr[18];;
		  /*epilog */

		  MELT_LOCATION ("warmelt-normal.melt:1276:/ clear");
		 /*clear *//*_#THE_MELTCALLCOUNT__L9*/ meltfnum[8] = 0;
		  /*^clear */
		 /*clear *//*_.MELT_DEBUG_FUN__V19*/ meltfptr[18] = 0;
		}
		;
	      }
	    else
	      {			/*^cond.else */

      /*_.IF___V18*/ meltfptr[17] = NULL;;
	      }
	    ;
	    MELT_LOCATION ("warmelt-normal.melt:1276:/ quasiblock");


	    /*_.PROGN___V20*/ meltfptr[18] = /*_.IF___V18*/ meltfptr[17];;
	    /*^compute */
	    /*_.IFCPP___V17*/ meltfptr[16] = /*_.PROGN___V20*/ meltfptr[18];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-normal.melt:1276:/ clear");
	       /*clear *//*_#MELT_NEED_DBG__L8*/ meltfnum[7] = 0;
	    /*^clear */
	       /*clear *//*_.IF___V18*/ meltfptr[17] = 0;
	    /*^clear */
	       /*clear *//*_.PROGN___V20*/ meltfptr[18] = 0;
	  }

#else /*MELT_HAVE_DEBUG */
	  /*^cppif.else */
	  /*_.IFCPP___V17*/ meltfptr[16] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
	  ;
	  MELT_LOCATION ("warmelt-normal.melt:1278:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^quasiblock */


	  /*_.RETVAL___V1*/ meltfptr[0] = /*_.WNLETN__V16*/ meltfptr[15];;

	  {
	    MELT_LOCATION ("warmelt-normal.melt:1278:/ locexp");
	    /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
	    if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	      melt_warn_for_no_expected_secondary_results ();
	    /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
	    ;
	  }
	  ;
	  /*^finalreturn */
	  ;
	  /*finalret */ goto labend_rout;
	  /*_.LET___V15*/ meltfptr[14] = /*_.RETURN___V21*/ meltfptr[17];;

	  MELT_LOCATION ("warmelt-normal.melt:1274:/ clear");
	     /*clear *//*_.WNLETN__V16*/ meltfptr[15] = 0;
	  /*^clear */
	     /*clear *//*_.IFCPP___V17*/ meltfptr[16] = 0;
	  /*^clear */
	     /*clear *//*_.RETURN___V21*/ meltfptr[17] = 0;
	  /*_.IFELSE___V14*/ meltfptr[13] = /*_.LET___V15*/ meltfptr[14];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-normal.melt:1273:/ clear");
	     /*clear *//*_.LET___V15*/ meltfptr[14] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-normal.melt:1279:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
   /*_#I__L10*/ meltfnum[8] =
	    (( /*_#NBNEXP__L5*/ meltfnum[1]) == (0));;
	  MELT_LOCATION ("warmelt-normal.melt:1279:/ cond");
	  /*cond */ if ( /*_#I__L10*/ meltfnum[8])	/*then */
	    {
	      /*^cond.then */
	      /*^block */
	      /*anyblock */
	      {


#if MELT_HAVE_DEBUG
		MELT_LOCATION ("warmelt-normal.melt:1280:/ cppif.then");
		/*^block */
		/*anyblock */
		{

		  /*^checksignal */
		  MELT_CHECK_SIGNAL ();
		  ;
       /*_#I__L11*/ meltfnum[7] =
		    (( /*_#NBNEXP__L5*/ meltfnum[1]) > (0));;
		  MELT_LOCATION ("warmelt-normal.melt:1280:/ cond");
		  /*cond */ if ( /*_#I__L11*/ meltfnum[7])	/*then */
		    {
		      /*^cond.then */
		      /*_.IFELSE___V24*/ meltfptr[16] = ( /*nil */ NULL);;
		    }
		  else
		    {
		      MELT_LOCATION ("warmelt-normal.melt:1280:/ cond.else");

		      /*^block */
		      /*anyblock */
		      {




			{
			  /*^locexp */
			  melt_assert_failed (("check nbnexp"),
					      ("warmelt-normal.melt")
					      ? ("warmelt-normal.melt") :
					      __FILE__,
					      (1280) ? (1280) : __LINE__,
					      __FUNCTION__);
			  ;
			}
			;
		   /*clear *//*_.IFELSE___V24*/ meltfptr[16] = 0;
			/*epilog */
		      }
		      ;
		    }
		  ;
		  /*^compute */
		  /*_.IFCPP___V23*/ meltfptr[15] =
		    /*_.IFELSE___V24*/ meltfptr[16];;
		  /*epilog */

		  MELT_LOCATION ("warmelt-normal.melt:1280:/ clear");
		 /*clear *//*_#I__L11*/ meltfnum[7] = 0;
		  /*^clear */
		 /*clear *//*_.IFELSE___V24*/ meltfptr[16] = 0;
		}

#else /*MELT_HAVE_DEBUG */
		/*^cppif.else */
		/*_.IFCPP___V23*/ meltfptr[15] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
		;
		/*^compute */
		/*_.IFELSE___V22*/ meltfptr[18] =
		  /*_.IFCPP___V23*/ meltfptr[15];;
		/*epilog */

		MELT_LOCATION ("warmelt-normal.melt:1279:/ clear");
	       /*clear *//*_.IFCPP___V23*/ meltfptr[15] = 0;
	      }
	      ;
	    }
	  else
	    {			/*^cond.else */

	      /*^block */
	      /*anyblock */
	      {

		MELT_LOCATION ("warmelt-normal.melt:1281:/ checksignal");
		MELT_CHECK_SIGNAL ();
		;
     /*_#I__L12*/ meltfnum[7] =
		  (( /*_#NBNEXP__L5*/ meltfnum[1]) == (1));;
		MELT_LOCATION ("warmelt-normal.melt:1281:/ cond");
		/*cond */ if ( /*_#I__L12*/ meltfnum[7])	/*then */
		  {
		    /*^cond.then */
		    /*^block */
		    /*anyblock */
		    {

		      MELT_LOCATION ("warmelt-normal.melt:1282:/ quasiblock");


       /*_.SUBNEXP__V27*/ meltfptr[16] =
			(melt_multiple_nth
			 ((melt_ptr_t) ( /*_.TUPNEXP__V2*/ meltfptr[1]),
			  (0)));;
		      MELT_LOCATION
			("warmelt-normal.melt:1283:/ checksignal");
		      MELT_CHECK_SIGNAL ();
		      ;
		      /*^apply */
		      /*apply */
		      {
			union meltparam_un argtab[2];
			memset (&argtab, 0, sizeof (argtab));
			/*^apply.arg */
			argtab[0].meltbp_aptr =
			  (melt_ptr_t *) & /*_.BINDLIST__V3*/ meltfptr[2];
			/*^apply.arg */
			argtab[1].meltbp_aptr =
			  (melt_ptr_t *) & /*_.LOC__V4*/ meltfptr[3];
			/*_.WNLET1__V28*/ meltfptr[15] =
			  melt_apply ((meltclosure_ptr_t)
				      (( /*!WRAP_NORMAL_LET1 */ meltfrout->
					tabval[1])),
				      (melt_ptr_t) ( /*_.SUBNEXP__V27*/
						    meltfptr[16]),
				      (MELTBPARSTR_PTR MELTBPARSTR_PTR ""),
				      argtab, "", (union meltparam_un *) 0);
		      }
		      ;

#if MELT_HAVE_DEBUG
		      MELT_LOCATION ("warmelt-normal.melt:1286:/ cppif.then");
		      /*^block */
		      /*anyblock */
		      {


			{
			  /*^locexp */
			  /*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
			  melt_dbgcounter++;
#endif
			  ;
			}
			;
			/*^checksignal */
			MELT_CHECK_SIGNAL ();
			;
	 /*_#MELT_NEED_DBG__L13*/ meltfnum[12] =
			  /*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
			  ( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
			  0	/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
			  ;;
			MELT_LOCATION ("warmelt-normal.melt:1286:/ cond");
			/*cond */ if ( /*_#MELT_NEED_DBG__L13*/ meltfnum[12])	/*then */
			  {
			    /*^cond.then */
			    /*^block */
			    /*anyblock */
			    {

	   /*_#THE_MELTCALLCOUNT__L14*/ meltfnum[13] =
#ifdef meltcallcount
				meltcallcount	/* the_meltcallcount */
#else
				0L
#endif /* meltcallcount the_meltcallcount */
				;;
			      MELT_LOCATION
				("warmelt-normal.melt:1286:/ checksignal");
			      MELT_CHECK_SIGNAL ();
			      ;
			      /*^apply */
			      /*apply */
			      {
				union meltparam_un argtab[5];
				memset (&argtab, 0, sizeof (argtab));
				/*^apply.arg */
				argtab[0].meltbp_long =
				  /*_#THE_MELTCALLCOUNT__L14*/ meltfnum[13];
				/*^apply.arg */
				argtab[1].meltbp_cstring =
				  "warmelt-normal.melt";
				/*^apply.arg */
				argtab[2].meltbp_long = 1286;
				/*^apply.arg */
				argtab[3].meltbp_cstring =
				  "wrap_normal_letseq return wnlet1=";
				/*^apply.arg */
				argtab[4].meltbp_aptr =
				  (melt_ptr_t *) & /*_.WNLET1__V28*/
				  meltfptr[15];
				/*_.MELT_DEBUG_FUN__V31*/ meltfptr[30] =
				  melt_apply ((meltclosure_ptr_t)
					      (( /*!MELT_DEBUG_FUN */
						meltfrout->tabval[0])),
					      (melt_ptr_t) (( /*nil */ NULL)),
					      (MELTBPARSTR_LONG
					       MELTBPARSTR_CSTRING
					       MELTBPARSTR_LONG
					       MELTBPARSTR_CSTRING
					       MELTBPARSTR_PTR ""), argtab,
					      "", (union meltparam_un *) 0);
			      }
			      ;
			      /*_.IF___V30*/ meltfptr[29] =
				/*_.MELT_DEBUG_FUN__V31*/ meltfptr[30];;
			      /*epilog */

			      MELT_LOCATION
				("warmelt-normal.melt:1286:/ clear");
		     /*clear *//*_#THE_MELTCALLCOUNT__L14*/
				meltfnum[13] = 0;
			      /*^clear */
		     /*clear *//*_.MELT_DEBUG_FUN__V31*/ meltfptr[30]
				= 0;
			    }
			    ;
			  }
			else
			  {	/*^cond.else */

	  /*_.IF___V30*/ meltfptr[29] = NULL;;
			  }
			;
			MELT_LOCATION
			  ("warmelt-normal.melt:1286:/ quasiblock");


			/*_.PROGN___V32*/ meltfptr[30] =
			  /*_.IF___V30*/ meltfptr[29];;
			/*^compute */
			/*_.IFCPP___V29*/ meltfptr[28] =
			  /*_.PROGN___V32*/ meltfptr[30];;
			/*epilog */

			MELT_LOCATION ("warmelt-normal.melt:1286:/ clear");
		   /*clear *//*_#MELT_NEED_DBG__L13*/ meltfnum[12] = 0;
			/*^clear */
		   /*clear *//*_.IF___V30*/ meltfptr[29] = 0;
			/*^clear */
		   /*clear *//*_.PROGN___V32*/ meltfptr[30] = 0;
		      }

#else /*MELT_HAVE_DEBUG */
		      /*^cppif.else */
		      /*_.IFCPP___V29*/ meltfptr[28] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
		      ;
		      MELT_LOCATION
			("warmelt-normal.melt:1287:/ checksignal");
		      MELT_CHECK_SIGNAL ();
		      ;
		      /*^quasiblock */


		      /*_.RETVAL___V1*/ meltfptr[0] =
			/*_.WNLET1__V28*/ meltfptr[15];;

		      {
			MELT_LOCATION ("warmelt-normal.melt:1287:/ locexp");
			/*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
			if (meltxresdescr_ && meltxresdescr_[0]
			    && meltxrestab_)
			  melt_warn_for_no_expected_secondary_results ();
			/* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
			;
		      }
		      ;
		      /*^finalreturn */
		      ;
		      /*finalret */ goto labend_rout;
		      /*_.LET___V26*/ meltfptr[14] =
			/*_.RETURN___V33*/ meltfptr[29];;

		      MELT_LOCATION ("warmelt-normal.melt:1282:/ clear");
		 /*clear *//*_.SUBNEXP__V27*/ meltfptr[16] = 0;
		      /*^clear */
		 /*clear *//*_.WNLET1__V28*/ meltfptr[15] = 0;
		      /*^clear */
		 /*clear *//*_.IFCPP___V29*/ meltfptr[28] = 0;
		      /*^clear */
		 /*clear *//*_.RETURN___V33*/ meltfptr[29] = 0;
		      /*_.IFELSE___V25*/ meltfptr[17] =
			/*_.LET___V26*/ meltfptr[14];;
		      /*epilog */

		      MELT_LOCATION ("warmelt-normal.melt:1281:/ clear");
		 /*clear *//*_.LET___V26*/ meltfptr[14] = 0;
		    }
		    ;
		  }
		else
		  {		/*^cond.else */

		    /*^block */
		    /*anyblock */
		    {

		      MELT_LOCATION ("warmelt-normal.melt:1290:/ quasiblock");


		      MELT_LOCATION
			("warmelt-normal.melt:1291:/ checksignal");
		      MELT_CHECK_SIGNAL ();
		      ;
		      /*^quasiblock */


		      /*^rawallocobj */
		      /*rawallocobj */
		      {
			melt_ptr_t newobj = 0;
			melt_raw_object_create (newobj,
						(melt_ptr_t) (( /*!CLASS_NREP_CHECKSIGNAL */ meltfrout->tabval[2])), (1), "CLASS_NREP_CHECKSIGNAL");
	/*_.INST__V36*/ meltfptr[15] =
			  newobj;
		      };
		      ;
		      /*^putslot */
		      /*putslot */
		      melt_assertmsg ("putslot checkobj @NREP_LOC",
				      melt_magic_discr ((melt_ptr_t)
							( /*_.INST__V36*/
							 meltfptr[15])) ==
				      MELTOBMAG_OBJECT);
		      melt_putfield_object (( /*_.INST__V36*/ meltfptr[15]),
					    (0), ( /*_.LOC__V4*/ meltfptr[3]),
					    "NREP_LOC");
		      ;
		      /*^touchobj */

		      melt_dbgtrace_written_object ( /*_.INST__V36*/
						    meltfptr[15],
						    "newly made instance");
		      ;
		      /*_.NCHECKINT__V35*/ meltfptr[16] =
			/*_.INST__V36*/ meltfptr[15];;
		      /*^compute */
       /*_#I__L15*/ meltfnum[13] =
			(( /*_#NBNEXP__L5*/ meltfnum[1]) + (1));;
		      /*^compute */
       /*_.GROWNTUP__V37*/ meltfptr[28] =
			(meltgc_new_multiple
			 ((meltobject_ptr_t)
			  (( /*!DISCR_MULTIPLE */ meltfrout->tabval[3])),
			  ( /*_#I__L15*/ meltfnum[13])));;

		      {
			MELT_LOCATION ("warmelt-normal.melt:1295:/ locexp");
			meltgc_multiple_put_nth ((melt_ptr_t)
						 ( /*_.GROWNTUP__V37*/
						  meltfptr[28]), (0),
						 (melt_ptr_t) ( /*_.NCHECKINT__V35*/ meltfptr[16]));
		      }
		      ;
		      /*citerblock FOREACH_IN_MULTIPLE */
		      {
			/* start foreach_in_multiple meltcit1__EACHTUP */
			long meltcit1__EACHTUP_ln =
			  melt_multiple_length ((melt_ptr_t) /*_.TUPNEXP__V2*/
						meltfptr[1]);
			for ( /*_#NIX__L16*/ meltfnum[12] = 0;
			     ( /*_#NIX__L16*/ meltfnum[12] >= 0)
			     && ( /*_#NIX__L16*/ meltfnum[12] <
				 meltcit1__EACHTUP_ln);
	/*_#NIX__L16*/ meltfnum[12]++)
			  {
			    /*_.CURNEXP__V38*/ meltfptr[29] =
			      melt_multiple_nth ((melt_ptr_t)
						 ( /*_.TUPNEXP__V2*/
						  meltfptr[1]), /*_#NIX__L16*/
						 meltfnum[12]);




#if MELT_HAVE_DEBUG
			    MELT_LOCATION
			      ("warmelt-normal.melt:1299:/ cppif.then");
			    /*^block */
			    /*anyblock */
			    {

			      /*^checksignal */
			      MELT_CHECK_SIGNAL ();
			      ;
	  /*_#IS_NOT_OBJECT__L17*/ meltfnum[16] =
				(melt_magic_discr
				 ((melt_ptr_t)
				  ( /*_.CURNEXP__V38*/ meltfptr[29])) !=
				 MELTOBMAG_OBJECT);;
			      MELT_LOCATION
				("warmelt-normal.melt:1299:/ cond");
			      /*cond */ if ( /*_#IS_NOT_OBJECT__L17*/ meltfnum[16])	/*then */
				{
				  /*^cond.then */
				  /*_#OR___L18*/ meltfnum[17] =
				    /*_#IS_NOT_OBJECT__L17*/ meltfnum[16];;
				}
			      else
				{
				  MELT_LOCATION
				    ("warmelt-normal.melt:1299:/ cond.else");

				  /*^block */
				  /*anyblock */
				  {

	    /*_#IS_A__L19*/ meltfnum[18] =
				      melt_is_instance_of ((melt_ptr_t)
							   ( /*_.CURNEXP__V38*/ meltfptr[29]), (melt_ptr_t) (( /*!CLASS_NREP */ meltfrout->tabval[4])));;
				    /*^compute */
				    /*_#OR___L18*/ meltfnum[17] =
				      /*_#IS_A__L19*/ meltfnum[18];;
				    /*epilog */

				    MELT_LOCATION
				      ("warmelt-normal.melt:1299:/ clear");
		      /*clear *//*_#IS_A__L19*/ meltfnum[18] =
				      0;
				  }
				  ;
				}
			      ;
			      /*^cond */
			      /*cond */ if ( /*_#OR___L18*/ meltfnum[17])	/*then */
				{
				  /*^cond.then */
				  /*_.IFELSE___V40*/ meltfptr[39] =
				    ( /*nil */ NULL);;
				}
			      else
				{
				  MELT_LOCATION
				    ("warmelt-normal.melt:1299:/ cond.else");

				  /*^block */
				  /*anyblock */
				  {




				    {
				      /*^locexp */
				      melt_assert_failed (("check curnexp"),
							  ("warmelt-normal.melt")
							  ?
							  ("warmelt-normal.melt")
							  : __FILE__,
							  (1299) ? (1299) :
							  __LINE__,
							  __FUNCTION__);
				      ;
				    }
				    ;
		      /*clear *//*_.IFELSE___V40*/ meltfptr[39]
				      = 0;
				    /*epilog */
				  }
				  ;
				}
			      ;
			      /*^compute */
			      /*_.IFCPP___V39*/ meltfptr[14] =
				/*_.IFELSE___V40*/ meltfptr[39];;
			      /*epilog */

			      MELT_LOCATION
				("warmelt-normal.melt:1299:/ clear");
		    /*clear *//*_#IS_NOT_OBJECT__L17*/ meltfnum[16]
				= 0;
			      /*^clear */
		    /*clear *//*_#OR___L18*/ meltfnum[17] = 0;
			      /*^clear */
		    /*clear *//*_.IFELSE___V40*/ meltfptr[39] = 0;
			    }

#else /*MELT_HAVE_DEBUG */
			    /*^cppif.else */
			    /*_.IFCPP___V39*/ meltfptr[14] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
			    ;
			    /*^compute */
	/*_#I__L20*/ meltfnum[18] =
			      (( /*_#NIX__L16*/ meltfnum[12]) + (1));;

			    {
			      MELT_LOCATION
				("warmelt-normal.melt:1300:/ locexp");
			      meltgc_multiple_put_nth ((melt_ptr_t)
						       ( /*_.GROWNTUP__V37*/
							meltfptr[28]),
						       ( /*_#I__L20*/
							meltfnum[18]),
						       (melt_ptr_t) ( /*_.CURNEXP__V38*/ meltfptr[29]));
			    }
			    ;
			    if ( /*_#NIX__L16*/ meltfnum[12] < 0)
			      break;
			  }	/* end  foreach_in_multiple meltcit1__EACHTUP */

			/*citerepilog */

			MELT_LOCATION ("warmelt-normal.melt:1296:/ clear");
		  /*clear *//*_.CURNEXP__V38*/ meltfptr[29] = 0;
			/*^clear */
		  /*clear *//*_#NIX__L16*/ meltfnum[12] = 0;
			/*^clear */
		  /*clear *//*_.IFCPP___V39*/ meltfptr[14] = 0;
			/*^clear */
		  /*clear *//*_#I__L20*/ meltfnum[18] = 0;
		      }		/*endciterblock FOREACH_IN_MULTIPLE */
		      ;
		      MELT_LOCATION ("warmelt-normal.melt:1303:/ quasiblock");


		      /*^newclosure */
		       /*newclosure *//*_.LAMBDA___V42*/ meltfptr[41] =
			(melt_ptr_t)
			meltgc_new_closure ((meltobject_ptr_t)
					    (((melt_ptr_t)
					      (MELT_PREDEF (DISCR_CLOSURE)))),
					    (meltroutine_ptr_t) (( /*!konst_8 */ meltfrout->tabval[8])), (2));
		      ;
		      /*^putclosedv */
		      /*putclosv */
		      melt_assertmsg ("putclosv checkclo",
				      melt_magic_discr ((melt_ptr_t)
							( /*_.LAMBDA___V42*/
							 meltfptr[41])) ==
				      MELTOBMAG_CLOSURE);
		      melt_assertmsg ("putclosv checkoff", 0 >= 0
				      && 0 <
				      melt_closure_size ((melt_ptr_t)
							 ( /*_.LAMBDA___V42*/
							  meltfptr[41])));
		      ((meltclosure_ptr_t) /*_.LAMBDA___V42*/ meltfptr[41])->
			tabval[0] =
			(melt_ptr_t) ( /*_.TUPNEXP__V2*/ meltfptr[1]);
		      ;
		      /*^putclosedv */
		      /*putclosv */
		      melt_assertmsg ("putclosv checkclo",
				      melt_magic_discr ((melt_ptr_t)
							( /*_.LAMBDA___V42*/
							 meltfptr[41])) ==
				      MELTOBMAG_CLOSURE);
		      melt_assertmsg ("putclosv checkoff", 1 >= 0
				      && 1 <
				      melt_closure_size ((melt_ptr_t)
							 ( /*_.LAMBDA___V42*/
							  meltfptr[41])));
		      ((meltclosure_ptr_t) /*_.LAMBDA___V42*/ meltfptr[41])->
			tabval[1] =
			(melt_ptr_t) ( /*_.BINDLIST__V3*/ meltfptr[2]);
		      ;
		      /*_.LAMBDA___V41*/ meltfptr[39] =
			/*_.LAMBDA___V42*/ meltfptr[41];;
		      MELT_LOCATION
			("warmelt-normal.melt:1301:/ checksignal");
		      MELT_CHECK_SIGNAL ();
		      ;
		      /*^apply */
		      /*apply */
		      {
			union meltparam_un argtab[1];
			memset (&argtab, 0, sizeof (argtab));
			/*^apply.arg */
			argtab[0].meltbp_aptr =
			  (melt_ptr_t *) & /*_.LAMBDA___V41*/ meltfptr[39];
			/*_.LIST_EVERY__V43*/ meltfptr[42] =
			  melt_apply ((meltclosure_ptr_t)
				      (( /*!LIST_EVERY */ meltfrout->
					tabval[5])),
				      (melt_ptr_t) ( /*_.BINDLIST__V3*/
						    meltfptr[2]),
				      (MELTBPARSTR_PTR ""), argtab, "",
				      (union meltparam_un *) 0);
		      }
		      ;
		      MELT_LOCATION ("warmelt-normal.melt:1310:/ quasiblock");


		      MELT_LOCATION
			("warmelt-normal.melt:1311:/ checksignal");
		      MELT_CHECK_SIGNAL ();
		      ;
		      MELT_LOCATION
			("warmelt-normal.melt:1313:/ checksignal");
		      MELT_CHECK_SIGNAL ();
		      ;
		      /*^apply */
		      /*apply */
		      {
			/*_.LIST_TO_MULTIPLE__V45*/ meltfptr[44] =
			  melt_apply ((meltclosure_ptr_t)
				      (( /*!LIST_TO_MULTIPLE */ meltfrout->
					tabval[10])),
				      (melt_ptr_t) ( /*_.BINDLIST__V3*/
						    meltfptr[2]), (""),
				      (union meltparam_un *) 0, "",
				      (union meltparam_un *) 0);
		      }
		      ;
		      MELT_LOCATION ("warmelt-normal.melt:1311:/ quasiblock");


		      /*^rawallocobj */
		      /*rawallocobj */
		      {
			melt_ptr_t newobj = 0;
			melt_raw_object_create (newobj,
						(melt_ptr_t) (( /*!CLASS_NREP_LET */ meltfrout->tabval[9])), (3), "CLASS_NREP_LET");
	/*_.INST__V47*/ meltfptr[46] =
			  newobj;
		      };
		      ;
		      /*^putslot */
		      /*putslot */
		      melt_assertmsg ("putslot checkobj @NREP_LOC",
				      melt_magic_discr ((melt_ptr_t)
							( /*_.INST__V47*/
							 meltfptr[46])) ==
				      MELTOBMAG_OBJECT);
		      melt_putfield_object (( /*_.INST__V47*/ meltfptr[46]),
					    (0), ( /*_.LOC__V4*/ meltfptr[3]),
					    "NREP_LOC");
		      ;
		      /*^putslot */
		      /*putslot */
		      melt_assertmsg ("putslot checkobj @NLET_BINDINGS",
				      melt_magic_discr ((melt_ptr_t)
							( /*_.INST__V47*/
							 meltfptr[46])) ==
				      MELTOBMAG_OBJECT);
		      melt_putfield_object (( /*_.INST__V47*/ meltfptr[46]),
					    (1),
					    ( /*_.LIST_TO_MULTIPLE__V45*/
					     meltfptr[44]), "NLET_BINDINGS");
		      ;
		      /*^putslot */
		      /*putslot */
		      melt_assertmsg ("putslot checkobj @NLET_BODY",
				      melt_magic_discr ((melt_ptr_t)
							( /*_.INST__V47*/
							 meltfptr[46])) ==
				      MELTOBMAG_OBJECT);
		      melt_putfield_object (( /*_.INST__V47*/ meltfptr[46]),
					    (2),
					    ( /*_.GROWNTUP__V37*/
					     meltfptr[28]), "NLET_BODY");
		      ;
		      /*^touchobj */

		      melt_dbgtrace_written_object ( /*_.INST__V47*/
						    meltfptr[46],
						    "newly made instance");
		      ;
		      /*_.WNLET__V46*/ meltfptr[45] =
			/*_.INST__V47*/ meltfptr[46];;

#if MELT_HAVE_DEBUG
		      MELT_LOCATION ("warmelt-normal.melt:1316:/ cppif.then");
		      /*^block */
		      /*anyblock */
		      {


			{
			  /*^locexp */
			  /*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
			  melt_dbgcounter++;
#endif
			  ;
			}
			;
			/*^checksignal */
			MELT_CHECK_SIGNAL ();
			;
	 /*_#MELT_NEED_DBG__L21*/ meltfnum[16] =
			  /*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
			  ( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
			  0	/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
			  ;;
			MELT_LOCATION ("warmelt-normal.melt:1316:/ cond");
			/*cond */ if ( /*_#MELT_NEED_DBG__L21*/ meltfnum[16])	/*then */
			  {
			    /*^cond.then */
			    /*^block */
			    /*anyblock */
			    {

	   /*_#THE_MELTCALLCOUNT__L22*/ meltfnum[17] =
#ifdef meltcallcount
				meltcallcount	/* the_meltcallcount */
#else
				0L
#endif /* meltcallcount the_meltcallcount */
				;;
			      MELT_LOCATION
				("warmelt-normal.melt:1316:/ checksignal");
			      MELT_CHECK_SIGNAL ();
			      ;
			      /*^apply */
			      /*apply */
			      {
				union meltparam_un argtab[5];
				memset (&argtab, 0, sizeof (argtab));
				/*^apply.arg */
				argtab[0].meltbp_long =
				  /*_#THE_MELTCALLCOUNT__L22*/ meltfnum[17];
				/*^apply.arg */
				argtab[1].meltbp_cstring =
				  "warmelt-normal.melt";
				/*^apply.arg */
				argtab[2].meltbp_long = 1316;
				/*^apply.arg */
				argtab[3].meltbp_cstring =
				  "wrap_normal_letseq return wnlet=";
				/*^apply.arg */
				argtab[4].meltbp_aptr =
				  (melt_ptr_t *) & /*_.WNLET__V46*/
				  meltfptr[45];
				/*_.MELT_DEBUG_FUN__V50*/ meltfptr[49] =
				  melt_apply ((meltclosure_ptr_t)
					      (( /*!MELT_DEBUG_FUN */
						meltfrout->tabval[0])),
					      (melt_ptr_t) (( /*nil */ NULL)),
					      (MELTBPARSTR_LONG
					       MELTBPARSTR_CSTRING
					       MELTBPARSTR_LONG
					       MELTBPARSTR_CSTRING
					       MELTBPARSTR_PTR ""), argtab,
					      "", (union meltparam_un *) 0);
			      }
			      ;
			      /*_.IF___V49*/ meltfptr[48] =
				/*_.MELT_DEBUG_FUN__V50*/ meltfptr[49];;
			      /*epilog */

			      MELT_LOCATION
				("warmelt-normal.melt:1316:/ clear");
		     /*clear *//*_#THE_MELTCALLCOUNT__L22*/
				meltfnum[17] = 0;
			      /*^clear */
		     /*clear *//*_.MELT_DEBUG_FUN__V50*/ meltfptr[49]
				= 0;
			    }
			    ;
			  }
			else
			  {	/*^cond.else */

	  /*_.IF___V49*/ meltfptr[48] = NULL;;
			  }
			;
			MELT_LOCATION
			  ("warmelt-normal.melt:1316:/ quasiblock");


			/*_.PROGN___V51*/ meltfptr[49] =
			  /*_.IF___V49*/ meltfptr[48];;
			/*^compute */
			/*_.IFCPP___V48*/ meltfptr[47] =
			  /*_.PROGN___V51*/ meltfptr[49];;
			/*epilog */

			MELT_LOCATION ("warmelt-normal.melt:1316:/ clear");
		   /*clear *//*_#MELT_NEED_DBG__L21*/ meltfnum[16] = 0;
			/*^clear */
		   /*clear *//*_.IF___V49*/ meltfptr[48] = 0;
			/*^clear */
		   /*clear *//*_.PROGN___V51*/ meltfptr[49] = 0;
		      }

#else /*MELT_HAVE_DEBUG */
		      /*^cppif.else */
		      /*_.IFCPP___V48*/ meltfptr[47] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
		      ;
		      MELT_LOCATION
			("warmelt-normal.melt:1317:/ checksignal");
		      MELT_CHECK_SIGNAL ();
		      ;
		      /*^quasiblock */


		      /*_.RETVAL___V1*/ meltfptr[0] =
			/*_.WNLET__V46*/ meltfptr[45];;

		      {
			MELT_LOCATION ("warmelt-normal.melt:1317:/ locexp");
			/*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
			if (meltxresdescr_ && meltxresdescr_[0]
			    && meltxrestab_)
			  melt_warn_for_no_expected_secondary_results ();
			/* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
			;
		      }
		      ;
		      /*^finalreturn */
		      ;
		      /*finalret */ goto labend_rout;
		      /*_.LET___V44*/ meltfptr[43] =
			/*_.RETURN___V52*/ meltfptr[48];;

		      MELT_LOCATION ("warmelt-normal.melt:1310:/ clear");
		 /*clear *//*_.LIST_TO_MULTIPLE__V45*/ meltfptr[44] = 0;
		      /*^clear */
		 /*clear *//*_.WNLET__V46*/ meltfptr[45] = 0;
		      /*^clear */
		 /*clear *//*_.IFCPP___V48*/ meltfptr[47] = 0;
		      /*^clear */
		 /*clear *//*_.RETURN___V52*/ meltfptr[48] = 0;
		      /*_.LET___V34*/ meltfptr[30] =
			/*_.LET___V44*/ meltfptr[43];;

		      MELT_LOCATION ("warmelt-normal.melt:1290:/ clear");
		 /*clear *//*_.NCHECKINT__V35*/ meltfptr[16] = 0;
		      /*^clear */
		 /*clear *//*_#I__L15*/ meltfnum[13] = 0;
		      /*^clear */
		 /*clear *//*_.GROWNTUP__V37*/ meltfptr[28] = 0;
		      /*^clear */
		 /*clear *//*_.LAMBDA___V41*/ meltfptr[39] = 0;
		      /*^clear */
		 /*clear *//*_.LIST_EVERY__V43*/ meltfptr[42] = 0;
		      /*^clear */
		 /*clear *//*_.LET___V44*/ meltfptr[43] = 0;
		      MELT_LOCATION ("warmelt-normal.melt:1289:/ quasiblock");


		      /*_.PROGN___V53*/ meltfptr[49] =
			/*_.LET___V34*/ meltfptr[30];;
		      /*^compute */
		      /*_.IFELSE___V25*/ meltfptr[17] =
			/*_.PROGN___V53*/ meltfptr[49];;
		      /*epilog */

		      MELT_LOCATION ("warmelt-normal.melt:1281:/ clear");
		 /*clear *//*_.LET___V34*/ meltfptr[30] = 0;
		      /*^clear */
		 /*clear *//*_.PROGN___V53*/ meltfptr[49] = 0;
		    }
		    ;
		  }
		;
		/*_.IFELSE___V22*/ meltfptr[18] =
		  /*_.IFELSE___V25*/ meltfptr[17];;
		/*epilog */

		MELT_LOCATION ("warmelt-normal.melt:1279:/ clear");
	       /*clear *//*_#I__L12*/ meltfnum[7] = 0;
		/*^clear */
	       /*clear *//*_.IFELSE___V25*/ meltfptr[17] = 0;
	      }
	      ;
	    }
	  ;
	  /*_.IFELSE___V14*/ meltfptr[13] = /*_.IFELSE___V22*/ meltfptr[18];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-normal.melt:1273:/ clear");
	     /*clear *//*_#I__L10*/ meltfnum[8] = 0;
	  /*^clear */
	     /*clear *//*_.IFELSE___V22*/ meltfptr[18] = 0;
	}
	;
      }
    ;
    /*_.LET___V13*/ meltfptr[11] = /*_.IFELSE___V14*/ meltfptr[13];;

    MELT_LOCATION ("warmelt-normal.melt:1270:/ clear");
	   /*clear *//*_#NBNEXP__L5*/ meltfnum[1] = 0;
    /*^clear */
	   /*clear *//*_#IS_MULTIPLE__L6*/ meltfnum[0] = 0;
    /*^clear */
	   /*clear *//*_#NOT__L7*/ meltfnum[6] = 0;
    /*^clear */
	   /*clear *//*_.IFELSE___V14*/ meltfptr[13] = 0;
    MELT_LOCATION ("warmelt-normal.melt:1265:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.LET___V13*/ meltfptr[11];;

    {
      MELT_LOCATION ("warmelt-normal.melt:1265:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V5*/ meltfptr[4] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V9*/ meltfptr[5] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V11*/ meltfptr[6] = 0;
    /*^clear */
	   /*clear *//*_.LET___V13*/ meltfptr[11] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("WRAP_NORMAL_LETSEQ", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_21_warmelt_normal_WRAP_NORMAL_LETSEQ_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_21_warmelt_normal_WRAP_NORMAL_LETSEQ */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_22_warmelt_normal_LAMBDA___4__ (meltclosure_ptr_t meltclosp_,
					 melt_ptr_t meltfirstargp_,
					 const melt_argdescr_cell_t
					 meltxargdescr_[],
					 union meltparam_un * meltxargtab_,
					 const melt_argdescr_cell_t
					 meltxresdescr_[],
					 union meltparam_un * meltxrestab_)
{
  long current_blocklevel_signals_meltrout_22_warmelt_normal_LAMBDA___4___melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_22_warmelt_normal_LAMBDA___4___st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 9
    melt_ptr_t mcfr_varptr[9];
#define MELTFRAM_NBVARNUM 5
    long mcfr_varnum[5];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_22_warmelt_normal_LAMBDA___4__ is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_22_warmelt_normal_LAMBDA___4___st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 9; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_22_warmelt_normal_LAMBDA___4__ nbval 9*/
  meltfram__.mcfr_nbvar = 9 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("LAMBDA_", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-normal.melt:1303:/ getarg");
 /*_.CBIND__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {

    MELT_LOCATION ("warmelt-normal.melt:1304:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#IS_A__L1*/ meltfnum[0] =
      melt_is_instance_of ((melt_ptr_t) ( /*_.CBIND__V2*/ meltfptr[1]),
			   (melt_ptr_t) (( /*!CLASS_NORMAL_LET_BINDING */
					  meltfrout->tabval[0])));;
    /*^compute */
 /*_#NOT__L2*/ meltfnum[1] =
      (!( /*_#IS_A__L1*/ meltfnum[0]));;
    MELT_LOCATION ("warmelt-normal.melt:1304:/ cond");
    /*cond */ if ( /*_#NOT__L2*/ meltfnum[1])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{


#if MELT_HAVE_DEBUG
	  MELT_LOCATION ("warmelt-normal.melt:1305:/ cppif.then");
	  /*^block */
	  /*anyblock */
	  {


	    {
	      /*^locexp */
	      /*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	      melt_dbgcounter++;
#endif
	      ;
	    }
	    ;
	    /*^checksignal */
	    MELT_CHECK_SIGNAL ();
	    ;
     /*_#MELT_NEED_DBG__L3*/ meltfnum[2] =
	      /*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	      ( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	      0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	      ;;
	    MELT_LOCATION ("warmelt-normal.melt:1305:/ cond");
	    /*cond */ if ( /*_#MELT_NEED_DBG__L3*/ meltfnum[2])	/*then */
	      {
		/*^cond.then */
		/*^block */
		/*anyblock */
		{

       /*_#THE_MELTCALLCOUNT__L4*/ meltfnum[3] =
#ifdef meltcallcount
		    meltcallcount	/* the_meltcallcount */
#else
		    0L
#endif /* meltcallcount the_meltcallcount */
		    ;;
		  MELT_LOCATION ("warmelt-normal.melt:1305:/ checksignal");
		  MELT_CHECK_SIGNAL ();
		  ;
		  /*^apply */
		  /*apply */
		  {
		    union meltparam_un argtab[9];
		    memset (&argtab, 0, sizeof (argtab));
		    /*^apply.arg */
		    argtab[0].meltbp_long =
		      /*_#THE_MELTCALLCOUNT__L4*/ meltfnum[3];
		    /*^apply.arg */
		    argtab[1].meltbp_cstring = "warmelt-normal.melt";
		    /*^apply.arg */
		    argtab[2].meltbp_long = 1305;
		    /*^apply.arg */
		    argtab[3].meltbp_cstring = "wrap_normal_letseq tuplexp=";
		    /*^apply.arg */
		    argtab[4].meltbp_aptr =
		      (melt_ptr_t *) & ( /*~TUPNEXP */ meltfclos->tabval[0]);
		    /*^apply.arg */
		    argtab[5].meltbp_cstring = " bindlist=";
		    /*^apply.arg */
		    argtab[6].meltbp_aptr =
		      (melt_ptr_t *) & ( /*~BINDLIST */ meltfclos->tabval[1]);
		    /*^apply.arg */
		    argtab[7].meltbp_cstring = " cbind=";
		    /*^apply.arg */
		    argtab[8].meltbp_aptr =
		      (melt_ptr_t *) & /*_.CBIND__V2*/ meltfptr[1];
		    /*_.MELT_DEBUG_FUN__V6*/ meltfptr[5] =
		      melt_apply ((meltclosure_ptr_t)
				  (( /*!MELT_DEBUG_FUN */ meltfrout->
				    tabval[1])),
				  (melt_ptr_t) (( /*nil */ NULL)),
				  (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_PTR MELTBPARSTR_CSTRING
				   MELTBPARSTR_PTR MELTBPARSTR_CSTRING
				   MELTBPARSTR_PTR ""), argtab, "",
				  (union meltparam_un *) 0);
		  }
		  ;
		  /*_.IF___V5*/ meltfptr[4] =
		    /*_.MELT_DEBUG_FUN__V6*/ meltfptr[5];;
		  /*epilog */

		  MELT_LOCATION ("warmelt-normal.melt:1305:/ clear");
		 /*clear *//*_#THE_MELTCALLCOUNT__L4*/ meltfnum[3] = 0;
		  /*^clear */
		 /*clear *//*_.MELT_DEBUG_FUN__V6*/ meltfptr[5] = 0;
		}
		;
	      }
	    else
	      {			/*^cond.else */

      /*_.IF___V5*/ meltfptr[4] = NULL;;
	      }
	    ;
	    MELT_LOCATION ("warmelt-normal.melt:1305:/ quasiblock");


	    /*_.PROGN___V7*/ meltfptr[5] = /*_.IF___V5*/ meltfptr[4];;
	    /*^compute */
	    /*_.IFCPP___V4*/ meltfptr[3] = /*_.PROGN___V7*/ meltfptr[5];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-normal.melt:1305:/ clear");
	       /*clear *//*_#MELT_NEED_DBG__L3*/ meltfnum[2] = 0;
	    /*^clear */
	       /*clear *//*_.IF___V5*/ meltfptr[4] = 0;
	    /*^clear */
	       /*clear *//*_.PROGN___V7*/ meltfptr[5] = 0;
	  }

#else /*MELT_HAVE_DEBUG */
	  /*^cppif.else */
	  /*_.IFCPP___V4*/ meltfptr[3] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
	  ;
	  /*^compute */
	  /*_.IF___V3*/ meltfptr[2] = /*_.IFCPP___V4*/ meltfptr[3];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-normal.melt:1304:/ clear");
	     /*clear *//*_.IFCPP___V4*/ meltfptr[3] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

  /*_.IF___V3*/ meltfptr[2] = NULL;;
      }
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-normal.melt:1308:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L5*/ meltfnum[3] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.CBIND__V2*/ meltfptr[1]),
			     (melt_ptr_t) (( /*!CLASS_NORMAL_LET_BINDING */
					    meltfrout->tabval[0])));;
      MELT_LOCATION ("warmelt-normal.melt:1308:/ cond");
      /*cond */ if ( /*_#IS_A__L5*/ meltfnum[3])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V9*/ meltfptr[5] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-normal.melt:1308:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check cbind wrapnormletseq"),
				  ("warmelt-normal.melt")
				  ? ("warmelt-normal.melt") : __FILE__,
				  (1308) ? (1308) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V9*/ meltfptr[5] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V8*/ meltfptr[4] = /*_.IFELSE___V9*/ meltfptr[5];;
      /*epilog */

      MELT_LOCATION ("warmelt-normal.melt:1308:/ clear");
	     /*clear *//*_#IS_A__L5*/ meltfnum[3] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V9*/ meltfptr[5] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V8*/ meltfptr[4] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-normal.melt:1303:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.IFCPP___V8*/ meltfptr[4];;

    {
      MELT_LOCATION ("warmelt-normal.melt:1303:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_#IS_A__L1*/ meltfnum[0] = 0;
    /*^clear */
	   /*clear *//*_#NOT__L2*/ meltfnum[1] = 0;
    /*^clear */
	   /*clear *//*_.IF___V3*/ meltfptr[2] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V8*/ meltfptr[4] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("LAMBDA_", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_22_warmelt_normal_LAMBDA___4___melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_22_warmelt_normal_LAMBDA___4__ */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_23_warmelt_normal_CHECK_CTYPE_NARGS (meltclosure_ptr_t meltclosp_,
					      melt_ptr_t meltfirstargp_,
					      const melt_argdescr_cell_t
					      meltxargdescr_[],
					      union meltparam_un *
					      meltxargtab_,
					      const melt_argdescr_cell_t
					      meltxresdescr_[],
					      union meltparam_un *
					      meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_23_warmelt_normal_CHECK_CTYPE_NARGS_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_23_warmelt_normal_CHECK_CTYPE_NARGS_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 7
    melt_ptr_t mcfr_varptr[7];
/*no varnum*/
#define MELTFRAM_NBVARNUM /*none*/0
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_23_warmelt_normal_CHECK_CTYPE_NARGS is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_23_warmelt_normal_CHECK_CTYPE_NARGS_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 7; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_23_warmelt_normal_CHECK_CTYPE_NARGS nbval 7*/
  meltfram__.mcfr_nbvar = 7 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("CHECK_CTYPE_NARGS", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-normal.melt:1322:/ getarg");
 /*_.NARGS__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.ENV__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.ENV__V3*/ meltfptr[2])) != NULL);


  /*getarg#2 */
  /*^getarg */
  if (meltxargdescr_[1] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.SLOC__V4*/ meltfptr[3] =
    (meltxargtab_[1].meltbp_aptr) ? (*(meltxargtab_[1].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.SLOC__V4*/ meltfptr[3])) !=
	      NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {

    MELT_LOCATION ("warmelt-normal.melt:1325:/ quasiblock");


    /*^newclosure */
		 /*newclosure *//*_.LAMBDA___V6*/ meltfptr[5] =
      (melt_ptr_t)
      meltgc_new_closure ((meltobject_ptr_t)
			  (((melt_ptr_t) (MELT_PREDEF (DISCR_CLOSURE)))),
			  (meltroutine_ptr_t) (( /*!konst_3 */ meltfrout->
						tabval[3])), (2));
    ;
    /*^putclosedv */
    /*putclosv */
    melt_assertmsg ("putclosv checkclo",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.LAMBDA___V6*/ meltfptr[5])) ==
		    MELTOBMAG_CLOSURE);
    melt_assertmsg ("putclosv checkoff", 0 >= 0
		    && 0 <
		    melt_closure_size ((melt_ptr_t)
				       ( /*_.LAMBDA___V6*/ meltfptr[5])));
    ((meltclosure_ptr_t) /*_.LAMBDA___V6*/ meltfptr[5])->tabval[0] =
      (melt_ptr_t) ( /*_.ENV__V3*/ meltfptr[2]);
    ;
    /*^putclosedv */
    /*putclosv */
    melt_assertmsg ("putclosv checkclo",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.LAMBDA___V6*/ meltfptr[5])) ==
		    MELTOBMAG_CLOSURE);
    melt_assertmsg ("putclosv checkoff", 1 >= 0
		    && 1 <
		    melt_closure_size ((melt_ptr_t)
				       ( /*_.LAMBDA___V6*/ meltfptr[5])));
    ((meltclosure_ptr_t) /*_.LAMBDA___V6*/ meltfptr[5])->tabval[1] =
      (melt_ptr_t) ( /*_.SLOC__V4*/ meltfptr[3]);
    ;
    /*_.LAMBDA___V5*/ meltfptr[4] = /*_.LAMBDA___V6*/ meltfptr[5];;
    MELT_LOCATION ("warmelt-normal.melt:1323:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.LAMBDA___V5*/ meltfptr[4];
      /*_.MULTIPLE_EVERY__V7*/ meltfptr[6] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!MULTIPLE_EVERY */ meltfrout->tabval[0])),
		    (melt_ptr_t) ( /*_.NARGS__V2*/ meltfptr[1]),
		    (MELTBPARSTR_PTR ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-normal.melt:1322:/ quasiblock");


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.MULTIPLE_EVERY__V7*/ meltfptr[6];;

    {
      MELT_LOCATION ("warmelt-normal.melt:1322:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.LAMBDA___V5*/ meltfptr[4] = 0;
    /*^clear */
	   /*clear *//*_.MULTIPLE_EVERY__V7*/ meltfptr[6] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("CHECK_CTYPE_NARGS", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_23_warmelt_normal_CHECK_CTYPE_NARGS_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_23_warmelt_normal_CHECK_CTYPE_NARGS */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_24_warmelt_normal_LAMBDA___5__ (meltclosure_ptr_t meltclosp_,
					 melt_ptr_t meltfirstargp_,
					 const melt_argdescr_cell_t
					 meltxargdescr_[],
					 union meltparam_un * meltxargtab_,
					 const melt_argdescr_cell_t
					 meltxresdescr_[],
					 union meltparam_un * meltxrestab_)
{
  long current_blocklevel_signals_meltrout_24_warmelt_normal_LAMBDA___5___melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_24_warmelt_normal_LAMBDA___5___st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 7
    melt_ptr_t mcfr_varptr[7];
#define MELTFRAM_NBVARNUM 4
    long mcfr_varnum[4];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_24_warmelt_normal_LAMBDA___5__ is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_24_warmelt_normal_LAMBDA___5___st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 7; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_24_warmelt_normal_LAMBDA___5__ nbval 7*/
  meltfram__.mcfr_nbvar = 7 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("LAMBDA_", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-normal.melt:1325:/ getarg");
 /*_.CNARG__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_LONG)
    goto lab_endgetargs;
 /*_#IX__L1*/ meltfnum[0] = meltxargtab_[0].meltbp_long;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {

    MELT_LOCATION ("warmelt-normal.melt:1326:/ quasiblock");


    /*^checksignal */
    MELT_CHECK_SIGNAL ();
    ;
    /*^msend */
    /*msend */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^ojbmsend.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & ( /*~ENV */ meltfclos->tabval[0]);
      /*_.CTYP__V3*/ meltfptr[2] =
	meltgc_send ((melt_ptr_t) ( /*_.CNARG__V2*/ meltfptr[1]),
		     (melt_ptr_t) (( /*!GET_CTYPE */ meltfrout->tabval[0])),
		     (MELTBPARSTR_PTR ""), argtab, "",
		     (union meltparam_un *) 0);
    }
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-normal.melt:1327:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L2*/ meltfnum[1] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.CTYP__V3*/ meltfptr[2]),
			     (melt_ptr_t) (( /*!CLASS_CTYPE */ meltfrout->
					    tabval[1])));;
      MELT_LOCATION ("warmelt-normal.melt:1327:/ cond");
      /*cond */ if ( /*_#IS_A__L2*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V5*/ meltfptr[4] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-normal.melt:1327:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check_ctype_nargs ctyp"),
				  ("warmelt-normal.melt")
				  ? ("warmelt-normal.melt") : __FILE__,
				  (1327) ? (1327) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V5*/ meltfptr[4] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V4*/ meltfptr[3] = /*_.IFELSE___V5*/ meltfptr[4];;
      /*epilog */

      MELT_LOCATION ("warmelt-normal.melt:1327:/ clear");
	     /*clear *//*_#IS_A__L2*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V5*/ meltfptr[4] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V4*/ meltfptr[3] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-normal.melt:1328:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^getslot */
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.CTYP__V3*/ meltfptr[2]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 5, "CTYPE_PARSTRING");
  /*_.CTYPE_PARSTRING__V6*/ meltfptr[4] = slot;
    };
    ;
 /*_#IS_STRING__L3*/ meltfnum[1] =
      (melt_magic_discr
       ((melt_ptr_t) ( /*_.CTYPE_PARSTRING__V6*/ meltfptr[4])) ==
       MELTOBMAG_STRING);;
    /*^compute */
 /*_#NOT__L4*/ meltfnum[3] =
      (!( /*_#IS_STRING__L3*/ meltfnum[1]));;
    MELT_LOCATION ("warmelt-normal.melt:1328:/ cond");
    /*cond */ if ( /*_#NOT__L4*/ meltfnum[3])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-normal.melt:1329:/ getslot");
	  {
	    melt_ptr_t slot = NULL, obj = NULL;
	    obj = (melt_ptr_t) ( /*_.CTYP__V3*/ meltfptr[2]) /*=obj*/ ;
	    melt_object_get_field (slot, obj, 1, "NAMED_NAME");
    /*_.NAMED_NAME__V7*/ meltfptr[6] = slot;
	  };
	  ;

	  {
	    /*^locexp */
	    melt_error_str ((melt_ptr_t) (( /*~SLOC */ meltfclos->tabval[1])),
			    ("argument has invalid type"),
			    (melt_ptr_t) ( /*_.NAMED_NAME__V7*/ meltfptr[6]));
	  }
	  ;
	  /*epilog */

	  MELT_LOCATION ("warmelt-normal.melt:1328:/ clear");
	     /*clear *//*_.NAMED_NAME__V7*/ meltfptr[6] = 0;
	}
	;
      }				/*noelse */
    ;

    MELT_LOCATION ("warmelt-normal.melt:1326:/ clear");
	   /*clear *//*_.CTYP__V3*/ meltfptr[2] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V4*/ meltfptr[3] = 0;
    /*^clear */
	   /*clear *//*_.CTYPE_PARSTRING__V6*/ meltfptr[4] = 0;
    /*^clear */
	   /*clear *//*_#IS_STRING__L3*/ meltfnum[1] = 0;
    /*^clear */
	   /*clear *//*_#NOT__L4*/ meltfnum[3] = 0;
    /*epilog */
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("LAMBDA_", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_24_warmelt_normal_LAMBDA___5___melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*noretval */ NULL);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_24_warmelt_normal_LAMBDA___5__ */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_25_warmelt_normal_NORMBIND_FAILANY (meltclosure_ptr_t meltclosp_,
					     melt_ptr_t meltfirstargp_,
					     const melt_argdescr_cell_t
					     meltxargdescr_[],
					     union meltparam_un *
					     meltxargtab_,
					     const melt_argdescr_cell_t
					     meltxresdescr_[],
					     union meltparam_un *
					     meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_25_warmelt_normal_NORMBIND_FAILANY_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_25_warmelt_normal_NORMBIND_FAILANY_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 19
    melt_ptr_t mcfr_varptr[19];
#define MELTFRAM_NBVARNUM 4
    long mcfr_varnum[4];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_25_warmelt_normal_NORMBIND_FAILANY is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_25_warmelt_normal_NORMBIND_FAILANY_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 19; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_25_warmelt_normal_NORMBIND_FAILANY nbval 19*/
  meltfram__.mcfr_nbvar = 19 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("NORMBIND_FAILANY", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-normal.melt:1340:/ getarg");
 /*_.RECV__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.ENV__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.ENV__V3*/ meltfptr[2])) != NULL);


  /*getarg#2 */
  /*^getarg */
  if (meltxargdescr_[1] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.NCX__V4*/ meltfptr[3] =
    (meltxargtab_[1].meltbp_aptr) ? (*(meltxargtab_[1].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.NCX__V4*/ meltfptr[3])) != NULL);


  /*getarg#3 */
  /*^getarg */
  if (meltxargdescr_[2] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.PROCS__V5*/ meltfptr[4] =
    (meltxargtab_[2].meltbp_aptr) ? (*(meltxargtab_[2].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.PROCS__V5*/ meltfptr[4])) !=
	      NULL);


  /*getarg#4 */
  /*^getarg */
  if (meltxargdescr_[3] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.PSLOC__V6*/ meltfptr[5] =
    (meltxargtab_[3].meltbp_aptr) ? (*(meltxargtab_[3].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.PSLOC__V6*/ meltfptr[5])) !=
	      NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-normal.melt:1341:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L1*/ meltfnum[0] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-normal.melt:1341:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L1*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-normal.melt:1341:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-normal.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 1341;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "normbind_failany recv";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.RECV__V2*/ meltfptr[1];
	      /*_.MELT_DEBUG_FUN__V9*/ meltfptr[8] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V8*/ meltfptr[7] = /*_.MELT_DEBUG_FUN__V9*/ meltfptr[8];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-normal.melt:1341:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V9*/ meltfptr[8] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V8*/ meltfptr[7] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-normal.melt:1341:/ quasiblock");


      /*_.PROGN___V10*/ meltfptr[8] = /*_.IF___V8*/ meltfptr[7];;
      /*^compute */
      /*_.IFCPP___V7*/ meltfptr[6] = /*_.PROGN___V10*/ meltfptr[8];;
      /*epilog */

      MELT_LOCATION ("warmelt-normal.melt:1341:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L1*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IF___V8*/ meltfptr[7] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V10*/ meltfptr[8] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V7*/ meltfptr[6] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-normal.melt:1342:/ quasiblock");


 /*_.DIS__V12*/ meltfptr[8] =
      ((melt_ptr_t)
       (melt_discr ((melt_ptr_t) ( /*_.RECV__V2*/ meltfptr[1]))));;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-normal.melt:1344:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L3*/ meltfnum[1] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-normal.melt:1344:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L3*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L4*/ meltfnum[0] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-normal.melt:1344:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L4*/ meltfnum[0];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-normal.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 1344;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "normbind_failany dis";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.DIS__V12*/ meltfptr[8];
	      /*_.MELT_DEBUG_FUN__V15*/ meltfptr[14] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V14*/ meltfptr[13] =
	      /*_.MELT_DEBUG_FUN__V15*/ meltfptr[14];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-normal.melt:1344:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L4*/ meltfnum[0] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V15*/ meltfptr[14] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V14*/ meltfptr[13] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-normal.melt:1344:/ quasiblock");


      /*_.PROGN___V16*/ meltfptr[14] = /*_.IF___V14*/ meltfptr[13];;
      /*^compute */
      /*_.IFCPP___V13*/ meltfptr[12] = /*_.PROGN___V16*/ meltfptr[14];;
      /*epilog */

      MELT_LOCATION ("warmelt-normal.melt:1344:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L3*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IF___V14*/ meltfptr[13] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V16*/ meltfptr[14] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V13*/ meltfptr[12] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-normal.melt:1345:/ cond");
    /*cond */ if (
		   /*ifisa */
		   melt_is_instance_of ((melt_ptr_t)
					( /*_.DIS__V12*/ meltfptr[8]),
					(melt_ptr_t) (( /*!CLASS_NAMED */
						       meltfrout->tabval[1])))
      )				/*then */
      {
	/*^cond.then */
	/*^getslot */
	{
	  melt_ptr_t slot = NULL, obj = NULL;
	  obj = (melt_ptr_t) ( /*_.DIS__V12*/ meltfptr[8]) /*=obj*/ ;
	  melt_object_get_field (slot, obj, 1, "NAMED_NAME");
   /*_.NAMED_NAME__V17*/ meltfptr[13] = slot;
	};
	;
      }
    else
      {				/*^cond.else */

  /*_.NAMED_NAME__V17*/ meltfptr[13] = NULL;;
      }
    ;

    {
      MELT_LOCATION ("warmelt-normal.melt:1345:/ locexp");
      melt_error_str ((melt_ptr_t) ( /*_.PSLOC__V6*/ meltfptr[5]),
		      ("unexpected binding normalization of class"),
		      (melt_ptr_t) ( /*_.NAMED_NAME__V17*/ meltfptr[13]));
    }
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-normal.melt:1346:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
      /*^cond */
      /*cond */ if (( /*nil */ NULL))	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V19*/ meltfptr[18] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-normal.melt:1346:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("@$@unexpected normalize binding"),
				  ("warmelt-normal.melt")
				  ? ("warmelt-normal.melt") : __FILE__,
				  (1346) ? (1346) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V19*/ meltfptr[18] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V18*/ meltfptr[14] = /*_.IFELSE___V19*/ meltfptr[18];;
      /*epilog */

      MELT_LOCATION ("warmelt-normal.melt:1346:/ clear");
	     /*clear *//*_.IFELSE___V19*/ meltfptr[18] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V18*/ meltfptr[14] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    /*^compute */
    /*_.LET___V11*/ meltfptr[7] = /*_.IFCPP___V18*/ meltfptr[14];;

    MELT_LOCATION ("warmelt-normal.melt:1342:/ clear");
	   /*clear *//*_.DIS__V12*/ meltfptr[8] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V13*/ meltfptr[12] = 0;
    /*^clear */
	   /*clear *//*_.NAMED_NAME__V17*/ meltfptr[13] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V18*/ meltfptr[14] = 0;
    MELT_LOCATION ("warmelt-normal.melt:1340:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.LET___V11*/ meltfptr[7];;

    {
      MELT_LOCATION ("warmelt-normal.melt:1340:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V7*/ meltfptr[6] = 0;
    /*^clear */
	   /*clear *//*_.LET___V11*/ meltfptr[7] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("NORMBIND_FAILANY", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_25_warmelt_normal_NORMBIND_FAILANY_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_25_warmelt_normal_NORMBIND_FAILANY */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_26_warmelt_normal_NORMBIND_ANYBIND (meltclosure_ptr_t meltclosp_,
					     melt_ptr_t meltfirstargp_,
					     const melt_argdescr_cell_t
					     meltxargdescr_[],
					     union meltparam_un *
					     meltxargtab_,
					     const melt_argdescr_cell_t
					     meltxresdescr_[],
					     union meltparam_un *
					     meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_26_warmelt_normal_NORMBIND_ANYBIND_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_26_warmelt_normal_NORMBIND_ANYBIND_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 48
    melt_ptr_t mcfr_varptr[48];
#define MELTFRAM_NBVARNUM 12
    long mcfr_varnum[12];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_26_warmelt_normal_NORMBIND_ANYBIND is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_26_warmelt_normal_NORMBIND_ANYBIND_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 48; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_26_warmelt_normal_NORMBIND_ANYBIND nbval 48*/
  meltfram__.mcfr_nbvar = 48 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("NORMBIND_ANYBIND", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-normal.melt:1350:/ getarg");
 /*_.BIND__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.ENV__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.ENV__V3*/ meltfptr[2])) != NULL);


  /*getarg#2 */
  /*^getarg */
  if (meltxargdescr_[1] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.NCX__V4*/ meltfptr[3] =
    (meltxargtab_[1].meltbp_aptr) ? (*(meltxargtab_[1].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.NCX__V4*/ meltfptr[3])) != NULL);


  /*getarg#3 */
  /*^getarg */
  if (meltxargdescr_[2] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.PROCS__V5*/ meltfptr[4] =
    (meltxargtab_[2].meltbp_aptr) ? (*(meltxargtab_[2].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.PROCS__V5*/ meltfptr[4])) !=
	      NULL);


  /*getarg#4 */
  /*^getarg */
  if (meltxargdescr_[3] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.PSLOC__V6*/ meltfptr[5] =
    (meltxargtab_[3].meltbp_aptr) ? (*(meltxargtab_[3].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.PSLOC__V6*/ meltfptr[5])) !=
	      NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-normal.melt:1351:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L1*/ meltfnum[0] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-normal.melt:1351:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L1*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-normal.melt:1351:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-normal.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 1351;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "normbind_anybind bind=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.BIND__V2*/ meltfptr[1];
	      /*_.MELT_DEBUG_FUN__V9*/ meltfptr[8] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V8*/ meltfptr[7] = /*_.MELT_DEBUG_FUN__V9*/ meltfptr[8];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-normal.melt:1351:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V9*/ meltfptr[8] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V8*/ meltfptr[7] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-normal.melt:1351:/ quasiblock");


      /*_.PROGN___V10*/ meltfptr[8] = /*_.IF___V8*/ meltfptr[7];;
      /*^compute */
      /*_.IFCPP___V7*/ meltfptr[6] = /*_.PROGN___V10*/ meltfptr[8];;
      /*epilog */

      MELT_LOCATION ("warmelt-normal.melt:1351:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L1*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IF___V8*/ meltfptr[7] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V10*/ meltfptr[8] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V7*/ meltfptr[6] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-normal.melt:1352:/ quasiblock");


 /*_.DIS__V12*/ meltfptr[8] =
      ((melt_ptr_t)
       (melt_discr ((melt_ptr_t) ( /*_.BIND__V2*/ meltfptr[1]))));;
    MELT_LOCATION ("warmelt-normal.melt:1353:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.BIND__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 0, "BINDER");
  /*_.SYMB__V13*/ meltfptr[12] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-normal.melt:1354:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.NCX__V4*/ meltfptr[3]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 6, "NCTX_SYMBCACHEMAP");
  /*_.SYCMAP__V14*/ meltfptr[13] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-normal.melt:1357:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.SYMB__V13*/ meltfptr[12]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 1, "NAMED_NAME");
  /*_.NAMED_NAME__V15*/ meltfptr[14] = slot;
    };
    ;

    {
      MELT_LOCATION ("warmelt-normal.melt:1356:/ locexp");
      melt_warning_str (0, (melt_ptr_t) ( /*_.PSLOC__V6*/ meltfptr[5]),
			("bizarre?? constant reference to"),
			(melt_ptr_t) ( /*_.NAMED_NAME__V15*/ meltfptr[14]));
    }
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-normal.melt:1358:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L3*/ meltfnum[1] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-normal.melt:1358:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L3*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L4*/ meltfnum[0] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-normal.melt:1358:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L4*/ meltfnum[0];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-normal.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 1358;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "normbind_anybind dis";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.DIS__V12*/ meltfptr[8];
	      /*_.MELT_DEBUG_FUN__V18*/ meltfptr[17] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V17*/ meltfptr[16] =
	      /*_.MELT_DEBUG_FUN__V18*/ meltfptr[17];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-normal.melt:1358:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L4*/ meltfnum[0] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V18*/ meltfptr[17] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V17*/ meltfptr[16] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-normal.melt:1358:/ quasiblock");


      /*_.PROGN___V19*/ meltfptr[17] = /*_.IF___V17*/ meltfptr[16];;
      /*^compute */
      /*_.IFCPP___V16*/ meltfptr[15] = /*_.PROGN___V19*/ meltfptr[17];;
      /*epilog */

      MELT_LOCATION ("warmelt-normal.melt:1358:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L3*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IF___V17*/ meltfptr[16] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V19*/ meltfptr[17] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V16*/ meltfptr[15] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-normal.melt:1359:/ cond");
    /*cond */ if (
		   /*ifisa */
		   melt_is_instance_of ((melt_ptr_t)
					( /*_.DIS__V12*/ meltfptr[8]),
					(melt_ptr_t) (( /*!CLASS_NAMED */
						       meltfrout->tabval[1])))
      )				/*then */
      {
	/*^cond.then */
	/*^getslot */
	{
	  melt_ptr_t slot = NULL, obj = NULL;
	  obj = (melt_ptr_t) ( /*_.DIS__V12*/ meltfptr[8]) /*=obj*/ ;
	  melt_object_get_field (slot, obj, 1, "NAMED_NAME");
   /*_.NAMED_NAME__V20*/ meltfptr[16] = slot;
	};
	;
      }
    else
      {				/*^cond.else */

  /*_.NAMED_NAME__V20*/ meltfptr[16] = NULL;;
      }
    ;

    {
      MELT_LOCATION ("warmelt-normal.melt:1359:/ locexp");
      melt_warning_str (0, (melt_ptr_t) ( /*_.PSLOC__V6*/ meltfptr[5]),
			("bizarre binding normalization of binding of "),
			(melt_ptr_t) ( /*_.NAMED_NAME__V20*/ meltfptr[16]));
    }
    ;
    MELT_LOCATION ("warmelt-normal.melt:1360:/ quasiblock");


    MELT_LOCATION ("warmelt-normal.melt:1361:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*^rawallocobj */
    /*rawallocobj */
    {
      melt_ptr_t newobj = 0;
      melt_raw_object_create (newobj,
			      (melt_ptr_t) (( /*!CLASS_NREP_CONSTOCC */
					     meltfrout->tabval[2])), (5),
			      "CLASS_NREP_CONSTOCC");
  /*_.INST__V23*/ meltfptr[22] =
	newobj;
    };
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @NREP_LOC",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V23*/ meltfptr[22])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V23*/ meltfptr[22]), (0),
			  ( /*_.PSLOC__V6*/ meltfptr[5]), "NREP_LOC");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @NOCC_CTYP",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V23*/ meltfptr[22])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V23*/ meltfptr[22]), (2),
			  (( /*!CTYPE_VALUE */ meltfrout->tabval[3])),
			  "NOCC_CTYP");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @NOCC_SYMB",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V23*/ meltfptr[22])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V23*/ meltfptr[22]), (1),
			  ( /*_.SYMB__V13*/ meltfptr[12]), "NOCC_SYMB");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @NOCC_BIND",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V23*/ meltfptr[22])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V23*/ meltfptr[22]), (3),
			  ( /*_.BIND__V2*/ meltfptr[1]), "NOCC_BIND");
    ;
    /*^touchobj */

    melt_dbgtrace_written_object ( /*_.INST__V23*/ meltfptr[22],
				  "newly made instance");
    ;
    /*_.KOCC__V22*/ meltfptr[21] = /*_.INST__V23*/ meltfptr[22];;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-normal.melt:1366:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L5*/ meltfnum[0] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-normal.melt:1366:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L5*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L6*/ meltfnum[1] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-normal.melt:1366:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L6*/ meltfnum[1];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-normal.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 1366;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "normbind_anybind kocc=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.KOCC__V22*/ meltfptr[21];
	      /*_.MELT_DEBUG_FUN__V26*/ meltfptr[25] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V25*/ meltfptr[24] =
	      /*_.MELT_DEBUG_FUN__V26*/ meltfptr[25];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-normal.melt:1366:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L6*/ meltfnum[1] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V26*/ meltfptr[25] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V25*/ meltfptr[24] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-normal.melt:1366:/ quasiblock");


      /*_.PROGN___V27*/ meltfptr[25] = /*_.IF___V25*/ meltfptr[24];;
      /*^compute */
      /*_.IFCPP___V24*/ meltfptr[23] = /*_.PROGN___V27*/ meltfptr[25];;
      /*epilog */

      MELT_LOCATION ("warmelt-normal.melt:1366:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L5*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IF___V25*/ meltfptr[24] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V27*/ meltfptr[25] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V24*/ meltfptr[23] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

    {
      MELT_LOCATION ("warmelt-normal.melt:1368:/ locexp");
      meltgc_put_mapobjects ((meltmapobjects_ptr_t)
			     ( /*_.SYCMAP__V14*/ meltfptr[13]),
			     (meltobject_ptr_t) ( /*_.SYMB__V13*/
						 meltfptr[12]),
			     (melt_ptr_t) ( /*_.KOCC__V22*/ meltfptr[21]));
    }
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-normal.melt:1369:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L7*/ meltfnum[1] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-normal.melt:1369:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L7*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L8*/ meltfnum[0] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-normal.melt:1369:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L8*/ meltfnum[0];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-normal.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 1369;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "normbind_anybind updated sycmap=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.SYCMAP__V14*/ meltfptr[13];
	      /*_.MELT_DEBUG_FUN__V30*/ meltfptr[29] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V29*/ meltfptr[25] =
	      /*_.MELT_DEBUG_FUN__V30*/ meltfptr[29];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-normal.melt:1369:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L8*/ meltfnum[0] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V30*/ meltfptr[29] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V29*/ meltfptr[25] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-normal.melt:1369:/ quasiblock");


      /*_.PROGN___V31*/ meltfptr[29] = /*_.IF___V29*/ meltfptr[25];;
      /*^compute */
      /*_.IFCPP___V28*/ meltfptr[24] = /*_.PROGN___V31*/ meltfptr[29];;
      /*epilog */

      MELT_LOCATION ("warmelt-normal.melt:1369:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L7*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IF___V29*/ meltfptr[25] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V31*/ meltfptr[29] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V28*/ meltfptr[24] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    /*citerblock FOREACH_IN_LIST */
    {
      /* start foreach_in_list meltcit1__EACHLIST */
      for ( /*_.CURPAIR__V32*/ meltfptr[25] =
	   melt_list_first ((melt_ptr_t) /*_.PROCS__V5*/ meltfptr[4]);
	   melt_magic_discr ((melt_ptr_t) /*_.CURPAIR__V32*/ meltfptr[25]) ==
	   MELTOBMAG_PAIR;
	   /*_.CURPAIR__V32*/ meltfptr[25] =
	   melt_pair_tail ((melt_ptr_t) /*_.CURPAIR__V32*/ meltfptr[25]))
	{
	  /*_.CURPROC__V33*/ meltfptr[29] =
	    melt_pair_head ((melt_ptr_t) /*_.CURPAIR__V32*/ meltfptr[25]);



#if MELT_HAVE_DEBUG
	  MELT_LOCATION ("warmelt-normal.melt:1374:/ cppif.then");
	  /*^block */
	  /*anyblock */
	  {


	    {
	      /*^locexp */
	      /*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	      melt_dbgcounter++;
#endif
	      ;
	    }
	    ;
	    /*^checksignal */
	    MELT_CHECK_SIGNAL ();
	    ;
    /*_#MELT_NEED_DBG__L9*/ meltfnum[0] =
	      /*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	      ( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	      0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	      ;;
	    MELT_LOCATION ("warmelt-normal.melt:1374:/ cond");
	    /*cond */ if ( /*_#MELT_NEED_DBG__L9*/ meltfnum[0])	/*then */
	      {
		/*^cond.then */
		/*^block */
		/*anyblock */
		{

      /*_#THE_MELTCALLCOUNT__L10*/ meltfnum[1] =
#ifdef meltcallcount
		    meltcallcount	/* the_meltcallcount */
#else
		    0L
#endif /* meltcallcount the_meltcallcount */
		    ;;
		  MELT_LOCATION ("warmelt-normal.melt:1374:/ checksignal");
		  MELT_CHECK_SIGNAL ();
		  ;
		  /*^apply */
		  /*apply */
		  {
		    union meltparam_un argtab[5];
		    memset (&argtab, 0, sizeof (argtab));
		    /*^apply.arg */
		    argtab[0].meltbp_long =
		      /*_#THE_MELTCALLCOUNT__L10*/ meltfnum[1];
		    /*^apply.arg */
		    argtab[1].meltbp_cstring = "warmelt-normal.melt";
		    /*^apply.arg */
		    argtab[2].meltbp_long = 1374;
		    /*^apply.arg */
		    argtab[3].meltbp_cstring =
		      "normbind_anybind const curproc";
		    /*^apply.arg */
		    argtab[4].meltbp_aptr =
		      (melt_ptr_t *) & /*_.CURPROC__V33*/ meltfptr[29];
		    /*_.MELT_DEBUG_FUN__V36*/ meltfptr[35] =
		      melt_apply ((meltclosure_ptr_t)
				  (( /*!MELT_DEBUG_FUN */ meltfrout->
				    tabval[0])),
				  (melt_ptr_t) (( /*nil */ NULL)),
				  (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_PTR ""), argtab, "",
				  (union meltparam_un *) 0);
		  }
		  ;
		  /*_.IF___V35*/ meltfptr[34] =
		    /*_.MELT_DEBUG_FUN__V36*/ meltfptr[35];;
		  /*epilog */

		  MELT_LOCATION ("warmelt-normal.melt:1374:/ clear");
		/*clear *//*_#THE_MELTCALLCOUNT__L10*/ meltfnum[1] = 0;
		  /*^clear */
		/*clear *//*_.MELT_DEBUG_FUN__V36*/ meltfptr[35] = 0;
		}
		;
	      }
	    else
	      {			/*^cond.else */

     /*_.IF___V35*/ meltfptr[34] = NULL;;
	      }
	    ;
	    MELT_LOCATION ("warmelt-normal.melt:1374:/ quasiblock");


	    /*_.PROGN___V37*/ meltfptr[35] = /*_.IF___V35*/ meltfptr[34];;
	    /*^compute */
	    /*_.IFCPP___V34*/ meltfptr[33] = /*_.PROGN___V37*/ meltfptr[35];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-normal.melt:1374:/ clear");
	      /*clear *//*_#MELT_NEED_DBG__L9*/ meltfnum[0] = 0;
	    /*^clear */
	      /*clear *//*_.IF___V35*/ meltfptr[34] = 0;
	    /*^clear */
	      /*clear *//*_.PROGN___V37*/ meltfptr[35] = 0;
	  }

#else /*MELT_HAVE_DEBUG */
	  /*^cppif.else */
	  /*_.IFCPP___V34*/ meltfptr[33] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
	  ;

#if MELT_HAVE_DEBUG
	  MELT_LOCATION ("warmelt-normal.melt:1375:/ cppif.then");
	  /*^block */
	  /*anyblock */
	  {

	    /*^checksignal */
	    MELT_CHECK_SIGNAL ();
	    ;
    /*_#IS_A__L11*/ meltfnum[1] =
	      melt_is_instance_of ((melt_ptr_t)
				   ( /*_.CURPROC__V33*/ meltfptr[29]),
				   (melt_ptr_t) (( /*!CLASS_NREP_ANYPROC */
						  meltfrout->tabval[4])));;
	    MELT_LOCATION ("warmelt-normal.melt:1375:/ cond");
	    /*cond */ if ( /*_#IS_A__L11*/ meltfnum[1])	/*then */
	      {
		/*^cond.then */
		/*_.IFELSE___V39*/ meltfptr[35] = ( /*nil */ NULL);;
	      }
	    else
	      {
		MELT_LOCATION ("warmelt-normal.melt:1375:/ cond.else");

		/*^block */
		/*anyblock */
		{




		  {
		    /*^locexp */
		    melt_assert_failed (("check curproc"),
					("warmelt-normal.melt")
					? ("warmelt-normal.melt") : __FILE__,
					(1375) ? (1375) : __LINE__,
					__FUNCTION__);
		    ;
		  }
		  ;
		/*clear *//*_.IFELSE___V39*/ meltfptr[35] = 0;
		  /*epilog */
		}
		;
	      }
	    ;
	    /*^compute */
	    /*_.IFCPP___V38*/ meltfptr[34] = /*_.IFELSE___V39*/ meltfptr[35];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-normal.melt:1375:/ clear");
	      /*clear *//*_#IS_A__L11*/ meltfnum[1] = 0;
	    /*^clear */
	      /*clear *//*_.IFELSE___V39*/ meltfptr[35] = 0;
	  }

#else /*MELT_HAVE_DEBUG */
	  /*^cppif.else */
	  /*_.IFCPP___V38*/ meltfptr[34] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
	  ;
	  MELT_LOCATION ("warmelt-normal.melt:1376:/ quasiblock");


	  /*^checksignal */
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^quasiblock */


	  /*^rawallocobj */
	  /*rawallocobj */
	  {
	    melt_ptr_t newobj = 0;
	    melt_raw_object_create (newobj,
				    (melt_ptr_t) (( /*!CLASS_REFERENCE */
						   meltfrout->tabval[5])),
				    (1), "CLASS_REFERENCE");
   /*_.INST__V41*/ meltfptr[40] =
	      newobj;
	  };
	  ;
	  /*^putslot */
	  /*putslot */
	  melt_assertmsg ("putslot checkobj @REFERENCED_VALUE",
			  melt_magic_discr ((melt_ptr_t)
					    ( /*_.INST__V41*/ meltfptr[40]))
			  == MELTOBMAG_OBJECT);
	  melt_putfield_object (( /*_.INST__V41*/ meltfptr[40]), (0),
				( /*_.KOCC__V22*/ meltfptr[21]),
				"REFERENCED_VALUE");
	  ;
	  /*^touchobj */

	  melt_dbgtrace_written_object ( /*_.INST__V41*/ meltfptr[40],
					"newly made instance");
	  ;
	  /*_.CLCONT__V40*/ meltfptr[35] = /*_.INST__V41*/ meltfptr[40];;
	  MELT_LOCATION ("warmelt-normal.melt:1377:/ getslot");
	  {
	    melt_ptr_t slot = NULL, obj = NULL;
	    obj = (melt_ptr_t) ( /*_.CURPROC__V33*/ meltfptr[29]) /*=obj*/ ;
	    melt_object_get_field (slot, obj, 5, "NRPRO_CONST");
   /*_.CONSTPROC__V42*/ meltfptr[41] = slot;
	  };
	  ;
	  /*citerblock FOREACH_IN_LIST */
	  {
	    /* start foreach_in_list meltcit2__EACHLIST */
	    for ( /*_.CURPAIRCL__V43*/ meltfptr[42] =
		 melt_list_first ((melt_ptr_t) /*_.CONSTPROC__V42*/
				  meltfptr[41]);
		 melt_magic_discr ((melt_ptr_t) /*_.CURPAIRCL__V43*/
				   meltfptr[42]) == MELTOBMAG_PAIR;
		 /*_.CURPAIRCL__V43*/ meltfptr[42] =
		 melt_pair_tail ((melt_ptr_t) /*_.CURPAIRCL__V43*/
				 meltfptr[42]))
	      {
		/*_.CURCL__V44*/ meltfptr[43] =
		  melt_pair_head ((melt_ptr_t) /*_.CURPAIRCL__V43*/
				  meltfptr[42]);


		MELT_LOCATION ("warmelt-normal.melt:1381:/ checksignal");
		MELT_CHECK_SIGNAL ();
		;
   /*_#__L12*/ meltfnum[0] =
		  (( /*_.CURCL__V44*/ meltfptr[43]) ==
		   ( /*_.KOCC__V22*/ meltfptr[21]));;
		MELT_LOCATION ("warmelt-normal.melt:1381:/ cond");
		/*cond */ if ( /*_#__L12*/ meltfnum[0])	/*then */
		  {
		    /*^cond.then */
		    /*^block */
		    /*anyblock */
		    {

		      MELT_LOCATION
			("warmelt-normal.melt:1382:/ checksignal");
		      MELT_CHECK_SIGNAL ();
		      ;
		      /*^cond */
		      /*cond */ if (
				     /*ifisa */
				     melt_is_instance_of ((melt_ptr_t)
							  ( /*_.CLCONT__V40*/
							   meltfptr[35]),
							  (melt_ptr_t) (( /*!CLASS_REFERENCE */ meltfrout->tabval[5])))
			)	/*then */
			{
			  /*^cond.then */
			  /*^block */
			  /*anyblock */
			  {

			    /*^putslot */
			    /*putslot */
			    melt_assertmsg
			      ("putslot checkobj @REFERENCED_VALUE",
			       melt_magic_discr ((melt_ptr_t)
						 ( /*_.CLCONT__V40*/
						  meltfptr[35])) ==
			       MELTOBMAG_OBJECT);
			    melt_putfield_object (( /*_.CLCONT__V40*/
						   meltfptr[35]), (0),
						  (( /*nil */ NULL)),
						  "REFERENCED_VALUE");
			    ;
			    /*^touch */
			    meltgc_touch ( /*_.CLCONT__V40*/ meltfptr[35]);
			    ;
			    /*^touchobj */

			    melt_dbgtrace_written_object ( /*_.CLCONT__V40*/
							  meltfptr[35],
							  "put-fields");
			    ;
			    /*epilog */
			  }
			  ;
			}	/*noelse */
		      ;
		      /*^quasiblock */


		      /*_.PROGN___V46*/ meltfptr[45] = ( /*nil */ NULL);;
		      /*^compute */
		      /*_.IF___V45*/ meltfptr[44] =
			/*_.PROGN___V46*/ meltfptr[45];;
		      /*epilog */

		      MELT_LOCATION ("warmelt-normal.melt:1381:/ clear");
	       /*clear *//*_.PROGN___V46*/ meltfptr[45] = 0;
		    }
		    ;
		  }
		else
		  {		/*^cond.else */

    /*_.IF___V45*/ meltfptr[44] = NULL;;
		  }
		;
	      }			/* end foreach_in_list meltcit2__EACHLIST */
     /*_.CURPAIRCL__V43*/ meltfptr[42] = NULL;
     /*_.CURCL__V44*/ meltfptr[43] = NULL;


	    /*citerepilog */

	    MELT_LOCATION ("warmelt-normal.melt:1378:/ clear");
	     /*clear *//*_.CURPAIRCL__V43*/ meltfptr[42] = 0;
	    /*^clear */
	     /*clear *//*_.CURCL__V44*/ meltfptr[43] = 0;
	    /*^clear */
	     /*clear *//*_#__L12*/ meltfnum[0] = 0;
	    /*^clear */
	     /*clear *//*_.IF___V45*/ meltfptr[44] = 0;
	  }			/*endciterblock FOREACH_IN_LIST */
	  ;
	  MELT_LOCATION ("warmelt-normal.melt:1383:/ quasiblock");


	  /*^cond */
	  /*cond */ if (
			 /*ifisa */
			 melt_is_instance_of ((melt_ptr_t)
					      ( /*_.CLCONT__V40*/
					       meltfptr[35]),
					      (melt_ptr_t) (( /*!CLASS_REFERENCE */ meltfrout->tabval[5])))
	    )			/*then */
	    {
	      /*^cond.then */
	      /*^getslot */
	      {
		melt_ptr_t slot = NULL, obj = NULL;
		obj =
		  (melt_ptr_t) ( /*_.CLCONT__V40*/ meltfptr[35]) /*=obj*/ ;
		melt_object_get_field (slot, obj, 0, "REFERENCED_VALUE");
    /*_.NEWCL__V47*/ meltfptr[45] = slot;
	      };
	      ;
	    }
	  else
	    {			/*^cond.else */

   /*_.NEWCL__V47*/ meltfptr[45] = NULL;;
	    }
	  ;
	  MELT_LOCATION ("warmelt-normal.melt:1384:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^cond */
	  /*cond */ if ( /*_.NEWCL__V47*/ meltfptr[45])	/*then */
	    {
	      /*^cond.then */
	      /*^block */
	      /*anyblock */
	      {


		{
		  /*^locexp */
		  meltgc_append_list ((melt_ptr_t)
				      ( /*_.CONSTPROC__V42*/ meltfptr[41]),
				      (melt_ptr_t) ( /*_.NEWCL__V47*/
						    meltfptr[45]));
		}
		;
		/*epilog */
	      }
	      ;
	    }			/*noelse */
	  ;

	  MELT_LOCATION ("warmelt-normal.melt:1383:/ clear");
	    /*clear *//*_.NEWCL__V47*/ meltfptr[45] = 0;

	  MELT_LOCATION ("warmelt-normal.melt:1376:/ clear");
	    /*clear *//*_.CLCONT__V40*/ meltfptr[35] = 0;
	  /*^clear */
	    /*clear *//*_.CONSTPROC__V42*/ meltfptr[41] = 0;
	}			/* end foreach_in_list meltcit1__EACHLIST */
     /*_.CURPAIR__V32*/ meltfptr[25] = NULL;
     /*_.CURPROC__V33*/ meltfptr[29] = NULL;


      /*citerepilog */

      MELT_LOCATION ("warmelt-normal.melt:1371:/ clear");
	    /*clear *//*_.CURPAIR__V32*/ meltfptr[25] = 0;
      /*^clear */
	    /*clear *//*_.CURPROC__V33*/ meltfptr[29] = 0;
      /*^clear */
	    /*clear *//*_.IFCPP___V34*/ meltfptr[33] = 0;
      /*^clear */
	    /*clear *//*_.IFCPP___V38*/ meltfptr[34] = 0;
    }				/*endciterblock FOREACH_IN_LIST */
    ;
    MELT_LOCATION ("warmelt-normal.melt:1386:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.KOCC__V22*/ meltfptr[21];;

    {
      MELT_LOCATION ("warmelt-normal.melt:1386:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*_.LET___V21*/ meltfptr[17] = /*_.RETURN___V48*/ meltfptr[45];;

    MELT_LOCATION ("warmelt-normal.melt:1360:/ clear");
	   /*clear *//*_.KOCC__V22*/ meltfptr[21] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V24*/ meltfptr[23] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V28*/ meltfptr[24] = 0;
    /*^clear */
	   /*clear *//*_.RETURN___V48*/ meltfptr[45] = 0;
    /*_.LET___V11*/ meltfptr[7] = /*_.LET___V21*/ meltfptr[17];;

    MELT_LOCATION ("warmelt-normal.melt:1352:/ clear");
	   /*clear *//*_.DIS__V12*/ meltfptr[8] = 0;
    /*^clear */
	   /*clear *//*_.SYMB__V13*/ meltfptr[12] = 0;
    /*^clear */
	   /*clear *//*_.SYCMAP__V14*/ meltfptr[13] = 0;
    /*^clear */
	   /*clear *//*_.NAMED_NAME__V15*/ meltfptr[14] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V16*/ meltfptr[15] = 0;
    /*^clear */
	   /*clear *//*_.NAMED_NAME__V20*/ meltfptr[16] = 0;
    /*^clear */
	   /*clear *//*_.LET___V21*/ meltfptr[17] = 0;
    MELT_LOCATION ("warmelt-normal.melt:1350:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.LET___V11*/ meltfptr[7];;

    {
      MELT_LOCATION ("warmelt-normal.melt:1350:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V7*/ meltfptr[6] = 0;
    /*^clear */
	   /*clear *//*_.LET___V11*/ meltfptr[7] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("NORMBIND_ANYBIND", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_26_warmelt_normal_NORMBIND_ANYBIND_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_26_warmelt_normal_NORMBIND_ANYBIND */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_27_warmelt_normal_NORMBIND_FORMALBIND (meltclosure_ptr_t meltclosp_,
						melt_ptr_t meltfirstargp_,
						const melt_argdescr_cell_t
						meltxargdescr_[],
						union meltparam_un *
						meltxargtab_,
						const melt_argdescr_cell_t
						meltxresdescr_[],
						union meltparam_un *
						meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_27_warmelt_normal_NORMBIND_FORMALBIND_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_27_warmelt_normal_NORMBIND_FORMALBIND_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 22
    melt_ptr_t mcfr_varptr[22];
#define MELTFRAM_NBVARNUM 5
    long mcfr_varnum[5];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_27_warmelt_normal_NORMBIND_FORMALBIND is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_27_warmelt_normal_NORMBIND_FORMALBIND_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 22; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_27_warmelt_normal_NORMBIND_FORMALBIND nbval 22*/
  meltfram__.mcfr_nbvar = 22 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("NORMBIND_FORMALBIND", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-normal.melt:1392:/ getarg");
 /*_.BIND__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.ENV__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.ENV__V3*/ meltfptr[2])) != NULL);


  /*getarg#2 */
  /*^getarg */
  if (meltxargdescr_[1] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.NCX__V4*/ meltfptr[3] =
    (meltxargtab_[1].meltbp_aptr) ? (*(meltxargtab_[1].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.NCX__V4*/ meltfptr[3])) != NULL);


  /*getarg#3 */
  /*^getarg */
  if (meltxargdescr_[2] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.PROCS__V5*/ meltfptr[4] =
    (meltxargtab_[2].meltbp_aptr) ? (*(meltxargtab_[2].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.PROCS__V5*/ meltfptr[4])) !=
	      NULL);


  /*getarg#4 */
  /*^getarg */
  if (meltxargdescr_[3] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.PSLOC__V6*/ meltfptr[5] =
    (meltxargtab_[3].meltbp_aptr) ? (*(meltxargtab_[3].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.PSLOC__V6*/ meltfptr[5])) !=
	      NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-normal.melt:1393:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L1*/ meltfnum[0] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.BIND__V2*/ meltfptr[1]),
			     (melt_ptr_t) (( /*!CLASS_FORMAL_BINDING */
					    meltfrout->tabval[0])));;
      MELT_LOCATION ("warmelt-normal.melt:1393:/ cond");
      /*cond */ if ( /*_#IS_A__L1*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V8*/ meltfptr[7] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-normal.melt:1393:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check bind"),
				  ("warmelt-normal.melt")
				  ? ("warmelt-normal.melt") : __FILE__,
				  (1393) ? (1393) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V8*/ meltfptr[7] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V7*/ meltfptr[6] = /*_.IFELSE___V8*/ meltfptr[7];;
      /*epilog */

      MELT_LOCATION ("warmelt-normal.melt:1393:/ clear");
	     /*clear *//*_#IS_A__L1*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V8*/ meltfptr[7] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V7*/ meltfptr[6] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-normal.melt:1394:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L2*/ meltfnum[0] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.ENV__V3*/ meltfptr[2]),
			     (melt_ptr_t) (( /*!CLASS_ENVIRONMENT */
					    meltfrout->tabval[1])));;
      MELT_LOCATION ("warmelt-normal.melt:1394:/ cond");
      /*cond */ if ( /*_#IS_A__L2*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V10*/ meltfptr[9] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-normal.melt:1394:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check env"),
				  ("warmelt-normal.melt")
				  ? ("warmelt-normal.melt") : __FILE__,
				  (1394) ? (1394) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V10*/ meltfptr[9] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V9*/ meltfptr[7] = /*_.IFELSE___V10*/ meltfptr[9];;
      /*epilog */

      MELT_LOCATION ("warmelt-normal.melt:1394:/ clear");
	     /*clear *//*_#IS_A__L2*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V10*/ meltfptr[9] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V9*/ meltfptr[7] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-normal.melt:1395:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L3*/ meltfnum[0] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.NCX__V4*/ meltfptr[3]),
			     (melt_ptr_t) (( /*!CLASS_NORMALIZATION_CONTEXT */
					    meltfrout->tabval[2])));;
      MELT_LOCATION ("warmelt-normal.melt:1395:/ cond");
      /*cond */ if ( /*_#IS_A__L3*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V12*/ meltfptr[11] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-normal.melt:1395:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check ncx"),
				  ("warmelt-normal.melt")
				  ? ("warmelt-normal.melt") : __FILE__,
				  (1395) ? (1395) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V12*/ meltfptr[11] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V11*/ meltfptr[9] = /*_.IFELSE___V12*/ meltfptr[11];;
      /*epilog */

      MELT_LOCATION ("warmelt-normal.melt:1395:/ clear");
	     /*clear *//*_#IS_A__L3*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V12*/ meltfptr[11] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V11*/ meltfptr[9] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-normal.melt:1396:/ quasiblock");


    /*^getslot */
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.NCX__V4*/ meltfptr[3]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 6, "NCTX_SYMBCACHEMAP");
  /*_.SYCMAP__V14*/ meltfptr[13] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-normal.melt:1397:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.BIND__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 0, "BINDER");
  /*_.SYMB__V15*/ meltfptr[14] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-normal.melt:1399:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    MELT_LOCATION ("warmelt-normal.melt:1401:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.BIND__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 1, "FBIND_TYPE");
  /*_.FBIND_TYPE__V16*/ meltfptr[15] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-normal.melt:1399:/ quasiblock");


    /*^rawallocobj */
    /*rawallocobj */
    {
      melt_ptr_t newobj = 0;
      melt_raw_object_create (newobj,
			      (melt_ptr_t) (( /*!CLASS_NREP_LOCSYMOCC */
					     meltfrout->tabval[3])), (4),
			      "CLASS_NREP_LOCSYMOCC");
  /*_.INST__V18*/ meltfptr[17] =
	newobj;
    };
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @NREP_LOC",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V18*/ meltfptr[17])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V18*/ meltfptr[17]), (0),
			  ( /*_.PSLOC__V6*/ meltfptr[5]), "NREP_LOC");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @NOCC_CTYP",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V18*/ meltfptr[17])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V18*/ meltfptr[17]), (2),
			  ( /*_.FBIND_TYPE__V16*/ meltfptr[15]), "NOCC_CTYP");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @NOCC_SYMB",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V18*/ meltfptr[17])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V18*/ meltfptr[17]), (1),
			  ( /*_.SYMB__V15*/ meltfptr[14]), "NOCC_SYMB");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @NOCC_BIND",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V18*/ meltfptr[17])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V18*/ meltfptr[17]), (3),
			  ( /*_.BIND__V2*/ meltfptr[1]), "NOCC_BIND");
    ;
    /*^touchobj */

    melt_dbgtrace_written_object ( /*_.INST__V18*/ meltfptr[17],
				  "newly made instance");
    ;
    /*_.SYOCC__V17*/ meltfptr[16] = /*_.INST__V18*/ meltfptr[17];;

    {
      MELT_LOCATION ("warmelt-normal.melt:1406:/ locexp");
      meltgc_put_mapobjects ((meltmapobjects_ptr_t)
			     ( /*_.SYCMAP__V14*/ meltfptr[13]),
			     (meltobject_ptr_t) ( /*_.SYMB__V15*/
						 meltfptr[14]),
			     (melt_ptr_t) ( /*_.SYOCC__V17*/ meltfptr[16]));
    }
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-normal.melt:1407:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L4*/ meltfnum[0] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-normal.melt:1407:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L4*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L5*/ meltfnum[4] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-normal.melt:1407:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[7];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L5*/ meltfnum[4];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-normal.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 1407;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring =
		"normbind_formalbind updated sycmap=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.SYCMAP__V14*/ meltfptr[13];
	      /*^apply.arg */
	      argtab[5].meltbp_cstring = " syocc=";
	      /*^apply.arg */
	      argtab[6].meltbp_aptr =
		(melt_ptr_t *) & /*_.SYOCC__V17*/ meltfptr[16];
	      /*_.MELT_DEBUG_FUN__V21*/ meltfptr[20] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[4])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V20*/ meltfptr[19] =
	      /*_.MELT_DEBUG_FUN__V21*/ meltfptr[20];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-normal.melt:1407:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L5*/ meltfnum[4] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V21*/ meltfptr[20] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V20*/ meltfptr[19] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-normal.melt:1407:/ quasiblock");


      /*_.PROGN___V22*/ meltfptr[20] = /*_.IF___V20*/ meltfptr[19];;
      /*^compute */
      /*_.IFCPP___V19*/ meltfptr[18] = /*_.PROGN___V22*/ meltfptr[20];;
      /*epilog */

      MELT_LOCATION ("warmelt-normal.melt:1407:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L4*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IF___V20*/ meltfptr[19] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V22*/ meltfptr[20] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V19*/ meltfptr[18] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    /*^compute */
    /*_.LET___V13*/ meltfptr[11] = /*_.SYOCC__V17*/ meltfptr[16];;

    MELT_LOCATION ("warmelt-normal.melt:1396:/ clear");
	   /*clear *//*_.SYCMAP__V14*/ meltfptr[13] = 0;
    /*^clear */
	   /*clear *//*_.SYMB__V15*/ meltfptr[14] = 0;
    /*^clear */
	   /*clear *//*_.FBIND_TYPE__V16*/ meltfptr[15] = 0;
    /*^clear */
	   /*clear *//*_.SYOCC__V17*/ meltfptr[16] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V19*/ meltfptr[18] = 0;
    MELT_LOCATION ("warmelt-normal.melt:1392:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.LET___V13*/ meltfptr[11];;

    {
      MELT_LOCATION ("warmelt-normal.melt:1392:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V7*/ meltfptr[6] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V9*/ meltfptr[7] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V11*/ meltfptr[9] = 0;
    /*^clear */
	   /*clear *//*_.LET___V13*/ meltfptr[11] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("NORMBIND_FORMALBIND", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_27_warmelt_normal_NORMBIND_FORMALBIND_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_27_warmelt_normal_NORMBIND_FORMALBIND */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_28_warmelt_normal_NORMBIND_LETBIND (meltclosure_ptr_t meltclosp_,
					     melt_ptr_t meltfirstargp_,
					     const melt_argdescr_cell_t
					     meltxargdescr_[],
					     union meltparam_un *
					     meltxargtab_,
					     const melt_argdescr_cell_t
					     meltxresdescr_[],
					     union meltparam_un *
					     meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_28_warmelt_normal_NORMBIND_LETBIND_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_28_warmelt_normal_NORMBIND_LETBIND_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 22
    melt_ptr_t mcfr_varptr[22];
#define MELTFRAM_NBVARNUM 5
    long mcfr_varnum[5];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_28_warmelt_normal_NORMBIND_LETBIND is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_28_warmelt_normal_NORMBIND_LETBIND_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 22; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_28_warmelt_normal_NORMBIND_LETBIND nbval 22*/
  meltfram__.mcfr_nbvar = 22 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("NORMBIND_LETBIND", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-normal.melt:1415:/ getarg");
 /*_.BIND__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.ENV__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.ENV__V3*/ meltfptr[2])) != NULL);


  /*getarg#2 */
  /*^getarg */
  if (meltxargdescr_[1] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.NCX__V4*/ meltfptr[3] =
    (meltxargtab_[1].meltbp_aptr) ? (*(meltxargtab_[1].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.NCX__V4*/ meltfptr[3])) != NULL);


  /*getarg#3 */
  /*^getarg */
  if (meltxargdescr_[2] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.PROCS__V5*/ meltfptr[4] =
    (meltxargtab_[2].meltbp_aptr) ? (*(meltxargtab_[2].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.PROCS__V5*/ meltfptr[4])) !=
	      NULL);


  /*getarg#4 */
  /*^getarg */
  if (meltxargdescr_[3] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.PSLOC__V6*/ meltfptr[5] =
    (meltxargtab_[3].meltbp_aptr) ? (*(meltxargtab_[3].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.PSLOC__V6*/ meltfptr[5])) !=
	      NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-normal.melt:1416:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L1*/ meltfnum[0] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.BIND__V2*/ meltfptr[1]),
			     (melt_ptr_t) (( /*!CLASS_LET_BINDING */
					    meltfrout->tabval[0])));;
      MELT_LOCATION ("warmelt-normal.melt:1416:/ cond");
      /*cond */ if ( /*_#IS_A__L1*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V8*/ meltfptr[7] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-normal.melt:1416:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check bind"),
				  ("warmelt-normal.melt")
				  ? ("warmelt-normal.melt") : __FILE__,
				  (1416) ? (1416) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V8*/ meltfptr[7] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V7*/ meltfptr[6] = /*_.IFELSE___V8*/ meltfptr[7];;
      /*epilog */

      MELT_LOCATION ("warmelt-normal.melt:1416:/ clear");
	     /*clear *//*_#IS_A__L1*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V8*/ meltfptr[7] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V7*/ meltfptr[6] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-normal.melt:1417:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L2*/ meltfnum[0] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.ENV__V3*/ meltfptr[2]),
			     (melt_ptr_t) (( /*!CLASS_ENVIRONMENT */
					    meltfrout->tabval[1])));;
      MELT_LOCATION ("warmelt-normal.melt:1417:/ cond");
      /*cond */ if ( /*_#IS_A__L2*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V10*/ meltfptr[9] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-normal.melt:1417:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check env"),
				  ("warmelt-normal.melt")
				  ? ("warmelt-normal.melt") : __FILE__,
				  (1417) ? (1417) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V10*/ meltfptr[9] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V9*/ meltfptr[7] = /*_.IFELSE___V10*/ meltfptr[9];;
      /*epilog */

      MELT_LOCATION ("warmelt-normal.melt:1417:/ clear");
	     /*clear *//*_#IS_A__L2*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V10*/ meltfptr[9] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V9*/ meltfptr[7] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-normal.melt:1418:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L3*/ meltfnum[0] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.NCX__V4*/ meltfptr[3]),
			     (melt_ptr_t) (( /*!CLASS_NORMALIZATION_CONTEXT */
					    meltfrout->tabval[2])));;
      MELT_LOCATION ("warmelt-normal.melt:1418:/ cond");
      /*cond */ if ( /*_#IS_A__L3*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V12*/ meltfptr[11] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-normal.melt:1418:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check ncx"),
				  ("warmelt-normal.melt")
				  ? ("warmelt-normal.melt") : __FILE__,
				  (1418) ? (1418) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V12*/ meltfptr[11] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V11*/ meltfptr[9] = /*_.IFELSE___V12*/ meltfptr[11];;
      /*epilog */

      MELT_LOCATION ("warmelt-normal.melt:1418:/ clear");
	     /*clear *//*_#IS_A__L3*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V12*/ meltfptr[11] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V11*/ meltfptr[9] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-normal.melt:1419:/ quasiblock");


    /*^getslot */
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.NCX__V4*/ meltfptr[3]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 6, "NCTX_SYMBCACHEMAP");
  /*_.SYCMAP__V14*/ meltfptr[13] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-normal.melt:1420:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.BIND__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 0, "BINDER");
  /*_.SYMB__V15*/ meltfptr[14] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-normal.melt:1422:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    MELT_LOCATION ("warmelt-normal.melt:1424:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.BIND__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 1, "FBIND_TYPE");
  /*_.FBIND_TYPE__V16*/ meltfptr[15] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-normal.melt:1422:/ quasiblock");


    /*^rawallocobj */
    /*rawallocobj */
    {
      melt_ptr_t newobj = 0;
      melt_raw_object_create (newobj,
			      (melt_ptr_t) (( /*!CLASS_NREP_LOCSYMOCC */
					     meltfrout->tabval[3])), (4),
			      "CLASS_NREP_LOCSYMOCC");
  /*_.INST__V18*/ meltfptr[17] =
	newobj;
    };
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @NREP_LOC",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V18*/ meltfptr[17])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V18*/ meltfptr[17]), (0),
			  ( /*_.PSLOC__V6*/ meltfptr[5]), "NREP_LOC");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @NOCC_CTYP",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V18*/ meltfptr[17])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V18*/ meltfptr[17]), (2),
			  ( /*_.FBIND_TYPE__V16*/ meltfptr[15]), "NOCC_CTYP");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @NOCC_SYMB",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V18*/ meltfptr[17])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V18*/ meltfptr[17]), (1),
			  ( /*_.SYMB__V15*/ meltfptr[14]), "NOCC_SYMB");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @NOCC_BIND",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V18*/ meltfptr[17])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V18*/ meltfptr[17]), (3),
			  ( /*_.BIND__V2*/ meltfptr[1]), "NOCC_BIND");
    ;
    /*^touchobj */

    melt_dbgtrace_written_object ( /*_.INST__V18*/ meltfptr[17],
				  "newly made instance");
    ;
    /*_.SYOCC__V17*/ meltfptr[16] = /*_.INST__V18*/ meltfptr[17];;

    {
      MELT_LOCATION ("warmelt-normal.melt:1429:/ locexp");
      meltgc_put_mapobjects ((meltmapobjects_ptr_t)
			     ( /*_.SYCMAP__V14*/ meltfptr[13]),
			     (meltobject_ptr_t) ( /*_.SYMB__V15*/
						 meltfptr[14]),
			     (melt_ptr_t) ( /*_.SYOCC__V17*/ meltfptr[16]));
    }
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-normal.melt:1430:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L4*/ meltfnum[0] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-normal.melt:1430:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L4*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L5*/ meltfnum[4] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-normal.melt:1430:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[7];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L5*/ meltfnum[4];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-normal.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 1430;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "normbind_letbind updated sycmap=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.SYCMAP__V14*/ meltfptr[13];
	      /*^apply.arg */
	      argtab[5].meltbp_cstring = " syocc=";
	      /*^apply.arg */
	      argtab[6].meltbp_aptr =
		(melt_ptr_t *) & /*_.SYOCC__V17*/ meltfptr[16];
	      /*_.MELT_DEBUG_FUN__V21*/ meltfptr[20] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[4])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V20*/ meltfptr[19] =
	      /*_.MELT_DEBUG_FUN__V21*/ meltfptr[20];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-normal.melt:1430:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L5*/ meltfnum[4] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V21*/ meltfptr[20] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V20*/ meltfptr[19] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-normal.melt:1430:/ quasiblock");


      /*_.PROGN___V22*/ meltfptr[20] = /*_.IF___V20*/ meltfptr[19];;
      /*^compute */
      /*_.IFCPP___V19*/ meltfptr[18] = /*_.PROGN___V22*/ meltfptr[20];;
      /*epilog */

      MELT_LOCATION ("warmelt-normal.melt:1430:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L4*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IF___V20*/ meltfptr[19] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V22*/ meltfptr[20] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V19*/ meltfptr[18] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    /*^compute */
    /*_.LET___V13*/ meltfptr[11] = /*_.SYOCC__V17*/ meltfptr[16];;

    MELT_LOCATION ("warmelt-normal.melt:1419:/ clear");
	   /*clear *//*_.SYCMAP__V14*/ meltfptr[13] = 0;
    /*^clear */
	   /*clear *//*_.SYMB__V15*/ meltfptr[14] = 0;
    /*^clear */
	   /*clear *//*_.FBIND_TYPE__V16*/ meltfptr[15] = 0;
    /*^clear */
	   /*clear *//*_.SYOCC__V17*/ meltfptr[16] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V19*/ meltfptr[18] = 0;
    MELT_LOCATION ("warmelt-normal.melt:1415:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.LET___V13*/ meltfptr[11];;

    {
      MELT_LOCATION ("warmelt-normal.melt:1415:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V7*/ meltfptr[6] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V9*/ meltfptr[7] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V11*/ meltfptr[9] = 0;
    /*^clear */
	   /*clear *//*_.LET___V13*/ meltfptr[11] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("NORMBIND_LETBIND", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_28_warmelt_normal_NORMBIND_LETBIND_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_28_warmelt_normal_NORMBIND_LETBIND */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_29_warmelt_normal_NORMBIND_FIXBIND (meltclosure_ptr_t meltclosp_,
					     melt_ptr_t meltfirstargp_,
					     const melt_argdescr_cell_t
					     meltxargdescr_[],
					     union meltparam_un *
					     meltxargtab_,
					     const melt_argdescr_cell_t
					     meltxresdescr_[],
					     union meltparam_un *
					     meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_29_warmelt_normal_NORMBIND_FIXBIND_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_29_warmelt_normal_NORMBIND_FIXBIND_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 36
    melt_ptr_t mcfr_varptr[36];
#define MELTFRAM_NBVARNUM 14
    long mcfr_varnum[14];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_29_warmelt_normal_NORMBIND_FIXBIND is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_29_warmelt_normal_NORMBIND_FIXBIND_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 36; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_29_warmelt_normal_NORMBIND_FIXBIND nbval 36*/
  meltfram__.mcfr_nbvar = 36 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("NORMBIND_FIXBIND", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-normal.melt:1437:/ getarg");
 /*_.BIND__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.ENV__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.ENV__V3*/ meltfptr[2])) != NULL);


  /*getarg#2 */
  /*^getarg */
  if (meltxargdescr_[1] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.NCX__V4*/ meltfptr[3] =
    (meltxargtab_[1].meltbp_aptr) ? (*(meltxargtab_[1].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.NCX__V4*/ meltfptr[3])) != NULL);


  /*getarg#3 */
  /*^getarg */
  if (meltxargdescr_[2] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.PROCS__V5*/ meltfptr[4] =
    (meltxargtab_[2].meltbp_aptr) ? (*(meltxargtab_[2].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.PROCS__V5*/ meltfptr[4])) !=
	      NULL);


  /*getarg#4 */
  /*^getarg */
  if (meltxargdescr_[3] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.PSLOC__V6*/ meltfptr[5] =
    (meltxargtab_[3].meltbp_aptr) ? (*(meltxargtab_[3].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.PSLOC__V6*/ meltfptr[5])) !=
	      NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {

    MELT_LOCATION ("warmelt-normal.melt:1438:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#IS_NOT_A__L1*/ meltfnum[0] =
      !melt_is_instance_of ((melt_ptr_t) ( /*_.BIND__V2*/ meltfptr[1]),
			    (melt_ptr_t) (( /*!CLASS_FIXED_BINDING */
					   meltfrout->tabval[0])));;
    MELT_LOCATION ("warmelt-normal.melt:1438:/ cond");
    /*cond */ if ( /*_#IS_NOT_A__L1*/ meltfnum[0])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{


#if MELT_HAVE_DEBUG
	  MELT_LOCATION ("warmelt-normal.melt:1439:/ cppif.then");
	  /*^block */
	  /*anyblock */
	  {


	    {
	      /*^locexp */
	      /*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	      melt_dbgcounter++;
#endif
	      ;
	    }
	    ;
	    /*^checksignal */
	    MELT_CHECK_SIGNAL ();
	    ;
     /*_#MELT_NEED_DBG__L2*/ meltfnum[1] =
	      /*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	      ( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	      0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	      ;;
	    MELT_LOCATION ("warmelt-normal.melt:1439:/ cond");
	    /*cond */ if ( /*_#MELT_NEED_DBG__L2*/ meltfnum[1])	/*then */
	      {
		/*^cond.then */
		/*^block */
		/*anyblock */
		{

       /*_#THE_MELTCALLCOUNT__L3*/ meltfnum[2] =
#ifdef meltcallcount
		    meltcallcount	/* the_meltcallcount */
#else
		    0L
#endif /* meltcallcount the_meltcallcount */
		    ;;
		  MELT_LOCATION ("warmelt-normal.melt:1439:/ checksignal");
		  MELT_CHECK_SIGNAL ();
		  ;
		  /*^apply */
		  /*apply */
		  {
		    union meltparam_un argtab[5];
		    memset (&argtab, 0, sizeof (argtab));
		    /*^apply.arg */
		    argtab[0].meltbp_long =
		      /*_#THE_MELTCALLCOUNT__L3*/ meltfnum[2];
		    /*^apply.arg */
		    argtab[1].meltbp_cstring = "warmelt-normal.melt";
		    /*^apply.arg */
		    argtab[2].meltbp_long = 1439;
		    /*^apply.arg */
		    argtab[3].meltbp_cstring = "normbind_fixbind bad bind";
		    /*^apply.arg */
		    argtab[4].meltbp_aptr =
		      (melt_ptr_t *) & /*_.BIND__V2*/ meltfptr[1];
		    /*_.MELT_DEBUG_FUN__V10*/ meltfptr[9] =
		      melt_apply ((meltclosure_ptr_t)
				  (( /*!MELT_DEBUG_FUN */ meltfrout->
				    tabval[1])),
				  (melt_ptr_t) (( /*nil */ NULL)),
				  (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_PTR ""), argtab, "",
				  (union meltparam_un *) 0);
		  }
		  ;
		  /*_.IF___V9*/ meltfptr[8] =
		    /*_.MELT_DEBUG_FUN__V10*/ meltfptr[9];;
		  /*epilog */

		  MELT_LOCATION ("warmelt-normal.melt:1439:/ clear");
		 /*clear *//*_#THE_MELTCALLCOUNT__L3*/ meltfnum[2] = 0;
		  /*^clear */
		 /*clear *//*_.MELT_DEBUG_FUN__V10*/ meltfptr[9] = 0;
		}
		;
	      }
	    else
	      {			/*^cond.else */

      /*_.IF___V9*/ meltfptr[8] = NULL;;
	      }
	    ;
	    MELT_LOCATION ("warmelt-normal.melt:1439:/ quasiblock");


	    /*_.PROGN___V11*/ meltfptr[9] = /*_.IF___V9*/ meltfptr[8];;
	    /*^compute */
	    /*_.IFCPP___V8*/ meltfptr[7] = /*_.PROGN___V11*/ meltfptr[9];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-normal.melt:1439:/ clear");
	       /*clear *//*_#MELT_NEED_DBG__L2*/ meltfnum[1] = 0;
	    /*^clear */
	       /*clear *//*_.IF___V9*/ meltfptr[8] = 0;
	    /*^clear */
	       /*clear *//*_.PROGN___V11*/ meltfptr[9] = 0;
	  }

#else /*MELT_HAVE_DEBUG */
	  /*^cppif.else */
	  /*_.IFCPP___V8*/ meltfptr[7] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
	  ;
	  /*^compute */
	  /*_.IF___V7*/ meltfptr[6] = /*_.IFCPP___V8*/ meltfptr[7];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-normal.melt:1438:/ clear");
	     /*clear *//*_.IFCPP___V8*/ meltfptr[7] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

  /*_.IF___V7*/ meltfptr[6] = NULL;;
      }
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-normal.melt:1440:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L4*/ meltfnum[2] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.BIND__V2*/ meltfptr[1]),
			     (melt_ptr_t) (( /*!CLASS_FIXED_BINDING */
					    meltfrout->tabval[0])));;
      MELT_LOCATION ("warmelt-normal.melt:1440:/ cond");
      /*cond */ if ( /*_#IS_A__L4*/ meltfnum[2])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V13*/ meltfptr[9] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-normal.melt:1440:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check bind"),
				  ("warmelt-normal.melt")
				  ? ("warmelt-normal.melt") : __FILE__,
				  (1440) ? (1440) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V13*/ meltfptr[9] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V12*/ meltfptr[8] = /*_.IFELSE___V13*/ meltfptr[9];;
      /*epilog */

      MELT_LOCATION ("warmelt-normal.melt:1440:/ clear");
	     /*clear *//*_#IS_A__L4*/ meltfnum[2] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V13*/ meltfptr[9] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V12*/ meltfptr[8] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-normal.melt:1441:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L5*/ meltfnum[1] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.ENV__V3*/ meltfptr[2]),
			     (melt_ptr_t) (( /*!CLASS_ENVIRONMENT */
					    meltfrout->tabval[2])));;
      MELT_LOCATION ("warmelt-normal.melt:1441:/ cond");
      /*cond */ if ( /*_#IS_A__L5*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V15*/ meltfptr[9] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-normal.melt:1441:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check env"),
				  ("warmelt-normal.melt")
				  ? ("warmelt-normal.melt") : __FILE__,
				  (1441) ? (1441) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V15*/ meltfptr[9] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V14*/ meltfptr[7] = /*_.IFELSE___V15*/ meltfptr[9];;
      /*epilog */

      MELT_LOCATION ("warmelt-normal.melt:1441:/ clear");
	     /*clear *//*_#IS_A__L5*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V15*/ meltfptr[9] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V14*/ meltfptr[7] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-normal.melt:1442:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L6*/ meltfnum[2] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.NCX__V4*/ meltfptr[3]),
			     (melt_ptr_t) (( /*!CLASS_NORMALIZATION_CONTEXT */
					    meltfrout->tabval[3])));;
      MELT_LOCATION ("warmelt-normal.melt:1442:/ cond");
      /*cond */ if ( /*_#IS_A__L6*/ meltfnum[2])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V17*/ meltfptr[16] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-normal.melt:1442:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check ncx"),
				  ("warmelt-normal.melt")
				  ? ("warmelt-normal.melt") : __FILE__,
				  (1442) ? (1442) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V17*/ meltfptr[16] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V16*/ meltfptr[9] = /*_.IFELSE___V17*/ meltfptr[16];;
      /*epilog */

      MELT_LOCATION ("warmelt-normal.melt:1442:/ clear");
	     /*clear *//*_#IS_A__L6*/ meltfnum[2] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V17*/ meltfptr[16] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V16*/ meltfptr[9] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-normal.melt:1443:/ quasiblock");


    /*^getslot */
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.NCX__V4*/ meltfptr[3]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 6, "NCTX_SYMBCACHEMAP");
  /*_.SYCMAP__V19*/ meltfptr[18] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-normal.melt:1444:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.BIND__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 0, "BINDER");
  /*_.SYMB__V20*/ meltfptr[19] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-normal.melt:1445:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.BIND__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 1, "FIXBIND_DATA");
  /*_.FIXDAT__V21*/ meltfptr[20] = slot;
    };
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-normal.melt:1447:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L7*/ meltfnum[1] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-normal.melt:1447:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L7*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L8*/ meltfnum[2] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-normal.melt:1447:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L8*/ meltfnum[2];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-normal.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 1447;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "normbind_fixbind fixdat";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.FIXDAT__V21*/ meltfptr[20];
	      /*_.MELT_DEBUG_FUN__V24*/ meltfptr[23] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[1])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V23*/ meltfptr[22] =
	      /*_.MELT_DEBUG_FUN__V24*/ meltfptr[23];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-normal.melt:1447:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L8*/ meltfnum[2] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V24*/ meltfptr[23] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V23*/ meltfptr[22] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-normal.melt:1447:/ quasiblock");


      /*_.PROGN___V25*/ meltfptr[23] = /*_.IF___V23*/ meltfptr[22];;
      /*^compute */
      /*_.IFCPP___V22*/ meltfptr[21] = /*_.PROGN___V25*/ meltfptr[23];;
      /*epilog */

      MELT_LOCATION ("warmelt-normal.melt:1447:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L7*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IF___V23*/ meltfptr[22] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V25*/ meltfptr[23] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V22*/ meltfptr[21] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-normal.melt:1448:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#NULL__L9*/ meltfnum[2] =
      (( /*_.FIXDAT__V21*/ meltfptr[20]) == NULL);;
    MELT_LOCATION ("warmelt-normal.melt:1448:/ cond");
    /*cond */ if ( /*_#NULL__L9*/ meltfnum[2])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{


#if MELT_HAVE_DEBUG
	  MELT_LOCATION ("warmelt-normal.melt:1450:/ cppif.then");
	  /*^block */
	  /*anyblock */
	  {


	    {
	      /*^locexp */
	      /*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	      melt_dbgcounter++;
#endif
	      ;
	    }
	    ;
	    /*^checksignal */
	    MELT_CHECK_SIGNAL ();
	    ;
     /*_#MELT_NEED_DBG__L10*/ meltfnum[1] =
	      /*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	      ( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	      0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	      ;;
	    MELT_LOCATION ("warmelt-normal.melt:1450:/ cond");
	    /*cond */ if ( /*_#MELT_NEED_DBG__L10*/ meltfnum[1])	/*then */
	      {
		/*^cond.then */
		/*^block */
		/*anyblock */
		{

       /*_#THE_MELTCALLCOUNT__L11*/ meltfnum[10] =
#ifdef meltcallcount
		    meltcallcount	/* the_meltcallcount */
#else
		    0L
#endif /* meltcallcount the_meltcallcount */
		    ;;
		  MELT_LOCATION ("warmelt-normal.melt:1450:/ checksignal");
		  MELT_CHECK_SIGNAL ();
		  ;
		  /*^apply */
		  /*apply */
		  {
		    union meltparam_un argtab[5];
		    memset (&argtab, 0, sizeof (argtab));
		    /*^apply.arg */
		    argtab[0].meltbp_long =
		      /*_#THE_MELTCALLCOUNT__L11*/ meltfnum[10];
		    /*^apply.arg */
		    argtab[1].meltbp_cstring = "warmelt-normal.melt";
		    /*^apply.arg */
		    argtab[2].meltbp_long = 1450;
		    /*^apply.arg */
		    argtab[3].meltbp_cstring =
		      "normbind_fixbind strange bind";
		    /*^apply.arg */
		    argtab[4].meltbp_aptr =
		      (melt_ptr_t *) & /*_.BIND__V2*/ meltfptr[1];
		    /*_.MELT_DEBUG_FUN__V28*/ meltfptr[27] =
		      melt_apply ((meltclosure_ptr_t)
				  (( /*!MELT_DEBUG_FUN */ meltfrout->
				    tabval[1])),
				  (melt_ptr_t) (( /*nil */ NULL)),
				  (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_PTR ""), argtab, "",
				  (union meltparam_un *) 0);
		  }
		  ;
		  /*_.IF___V27*/ meltfptr[23] =
		    /*_.MELT_DEBUG_FUN__V28*/ meltfptr[27];;
		  /*epilog */

		  MELT_LOCATION ("warmelt-normal.melt:1450:/ clear");
		 /*clear *//*_#THE_MELTCALLCOUNT__L11*/ meltfnum[10] = 0;
		  /*^clear */
		 /*clear *//*_.MELT_DEBUG_FUN__V28*/ meltfptr[27] = 0;
		}
		;
	      }
	    else
	      {			/*^cond.else */

      /*_.IF___V27*/ meltfptr[23] = NULL;;
	      }
	    ;
	    MELT_LOCATION ("warmelt-normal.melt:1450:/ quasiblock");


	    /*_.PROGN___V29*/ meltfptr[27] = /*_.IF___V27*/ meltfptr[23];;
	    /*^compute */
	    /*_.IFCPP___V26*/ meltfptr[22] = /*_.PROGN___V29*/ meltfptr[27];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-normal.melt:1450:/ clear");
	       /*clear *//*_#MELT_NEED_DBG__L10*/ meltfnum[1] = 0;
	    /*^clear */
	       /*clear *//*_.IF___V27*/ meltfptr[23] = 0;
	    /*^clear */
	       /*clear *//*_.PROGN___V29*/ meltfptr[27] = 0;
	  }

#else /*MELT_HAVE_DEBUG */
	  /*^cppif.else */
	  /*_.IFCPP___V26*/ meltfptr[22] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
	  ;
	  MELT_LOCATION ("warmelt-normal.melt:1452:/ getslot");
	  {
	    melt_ptr_t slot = NULL, obj = NULL;
	    obj = (melt_ptr_t) ( /*_.SYMB__V20*/ meltfptr[19]) /*=obj*/ ;
	    melt_object_get_field (slot, obj, 1, "NAMED_NAME");
    /*_.NAMED_NAME__V30*/ meltfptr[23] = slot;
	  };
	  ;

	  {
	    MELT_LOCATION ("warmelt-normal.melt:1451:/ locexp");
	    melt_error_str ((melt_ptr_t) ( /*_.PSLOC__V6*/ meltfptr[5]),
			    ("unresolved forward fixed reference to"),
			    (melt_ptr_t) ( /*_.NAMED_NAME__V30*/
					  meltfptr[23]));
	  }
	  ;
	  MELT_LOCATION ("warmelt-normal.melt:1449:/ quasiblock");


	  /*epilog */

	  MELT_LOCATION ("warmelt-normal.melt:1448:/ clear");
	     /*clear *//*_.IFCPP___V26*/ meltfptr[22] = 0;
	  /*^clear */
	     /*clear *//*_.NAMED_NAME__V30*/ meltfptr[23] = 0;
	}
	;
      }				/*noelse */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-normal.melt:1455:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L12*/ meltfnum[10] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.FIXDAT__V21*/ meltfptr[20]),
			     (melt_ptr_t) (( /*!CLASS_NREP_BOUND_DATA */
					    meltfrout->tabval[4])));;
      MELT_LOCATION ("warmelt-normal.melt:1455:/ cond");
      /*cond */ if ( /*_#IS_A__L12*/ meltfnum[10])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V32*/ meltfptr[22] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-normal.melt:1455:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("normbind_fixbind check fixdat"),
				  ("warmelt-normal.melt")
				  ? ("warmelt-normal.melt") : __FILE__,
				  (1455) ? (1455) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V32*/ meltfptr[22] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V31*/ meltfptr[27] = /*_.IFELSE___V32*/ meltfptr[22];;
      /*epilog */

      MELT_LOCATION ("warmelt-normal.melt:1455:/ clear");
	     /*clear *//*_#IS_A__L12*/ meltfnum[10] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V32*/ meltfptr[22] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V31*/ meltfptr[27] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

    {
      MELT_LOCATION ("warmelt-normal.melt:1457:/ locexp");
      meltgc_put_mapobjects ((meltmapobjects_ptr_t)
			     ( /*_.SYCMAP__V19*/ meltfptr[18]),
			     (meltobject_ptr_t) ( /*_.SYMB__V20*/
						 meltfptr[19]),
			     (melt_ptr_t) ( /*_.FIXDAT__V21*/ meltfptr[20]));
    }
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-normal.melt:1458:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L13*/ meltfnum[1] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-normal.melt:1458:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L13*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L14*/ meltfnum[10] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-normal.melt:1458:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[9];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long =
		/*_#THE_MELTCALLCOUNT__L14*/ meltfnum[10];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-normal.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 1458;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "normbind_fixbind updated sycmap=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.SYCMAP__V19*/ meltfptr[18];
	      /*^apply.arg */
	      argtab[5].meltbp_cstring = " symb=";
	      /*^apply.arg */
	      argtab[6].meltbp_aptr =
		(melt_ptr_t *) & /*_.SYMB__V20*/ meltfptr[19];
	      /*^apply.arg */
	      argtab[7].meltbp_cstring = " fixdat=";
	      /*^apply.arg */
	      argtab[8].meltbp_aptr =
		(melt_ptr_t *) & /*_.FIXDAT__V21*/ meltfptr[20];
	      /*_.MELT_DEBUG_FUN__V35*/ meltfptr[34] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[1])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V34*/ meltfptr[22] =
	      /*_.MELT_DEBUG_FUN__V35*/ meltfptr[34];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-normal.melt:1458:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L14*/ meltfnum[10] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V35*/ meltfptr[34] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V34*/ meltfptr[22] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-normal.melt:1458:/ quasiblock");


      /*_.PROGN___V36*/ meltfptr[34] = /*_.IF___V34*/ meltfptr[22];;
      /*^compute */
      /*_.IFCPP___V33*/ meltfptr[23] = /*_.PROGN___V36*/ meltfptr[34];;
      /*epilog */

      MELT_LOCATION ("warmelt-normal.melt:1458:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L13*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IF___V34*/ meltfptr[22] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V36*/ meltfptr[34] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V33*/ meltfptr[23] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    /*^compute */
    /*_.LET___V18*/ meltfptr[16] = /*_.FIXDAT__V21*/ meltfptr[20];;

    MELT_LOCATION ("warmelt-normal.melt:1443:/ clear");
	   /*clear *//*_.SYCMAP__V19*/ meltfptr[18] = 0;
    /*^clear */
	   /*clear *//*_.SYMB__V20*/ meltfptr[19] = 0;
    /*^clear */
	   /*clear *//*_.FIXDAT__V21*/ meltfptr[20] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V22*/ meltfptr[21] = 0;
    /*^clear */
	   /*clear *//*_#NULL__L9*/ meltfnum[2] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V31*/ meltfptr[27] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V33*/ meltfptr[23] = 0;
    MELT_LOCATION ("warmelt-normal.melt:1437:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.LET___V18*/ meltfptr[16];;

    {
      MELT_LOCATION ("warmelt-normal.melt:1437:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_#IS_NOT_A__L1*/ meltfnum[0] = 0;
    /*^clear */
	   /*clear *//*_.IF___V7*/ meltfptr[6] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V12*/ meltfptr[8] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V14*/ meltfptr[7] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V16*/ meltfptr[9] = 0;
    /*^clear */
	   /*clear *//*_.LET___V18*/ meltfptr[16] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("NORMBIND_FIXBIND", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_29_warmelt_normal_NORMBIND_FIXBIND_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_29_warmelt_normal_NORMBIND_FIXBIND */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_30_warmelt_normal_NORMBIND_DEFINEDVALBIND (meltclosure_ptr_t
						    meltclosp_,
						    melt_ptr_t meltfirstargp_,
						    const melt_argdescr_cell_t
						    meltxargdescr_[],
						    union meltparam_un *
						    meltxargtab_,
						    const melt_argdescr_cell_t
						    meltxresdescr_[],
						    union meltparam_un *
						    meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_30_warmelt_normal_NORMBIND_DEFINEDVALBIND_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_30_warmelt_normal_NORMBIND_DEFINEDVALBIND_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 34
    melt_ptr_t mcfr_varptr[34];
#define MELTFRAM_NBVARNUM 10
    long mcfr_varnum[10];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_30_warmelt_normal_NORMBIND_DEFINEDVALBIND is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_30_warmelt_normal_NORMBIND_DEFINEDVALBIND_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 34; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_30_warmelt_normal_NORMBIND_DEFINEDVALBIND nbval 34*/
  meltfram__.mcfr_nbvar = 34 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("NORMBIND_DEFINEDVALBIND", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-normal.melt:1464:/ getarg");
 /*_.BIND__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.ENV__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.ENV__V3*/ meltfptr[2])) != NULL);


  /*getarg#2 */
  /*^getarg */
  if (meltxargdescr_[1] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.NCX__V4*/ meltfptr[3] =
    (meltxargtab_[1].meltbp_aptr) ? (*(meltxargtab_[1].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.NCX__V4*/ meltfptr[3])) != NULL);


  /*getarg#3 */
  /*^getarg */
  if (meltxargdescr_[2] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.PROCS__V5*/ meltfptr[4] =
    (meltxargtab_[2].meltbp_aptr) ? (*(meltxargtab_[2].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.PROCS__V5*/ meltfptr[4])) !=
	      NULL);


  /*getarg#4 */
  /*^getarg */
  if (meltxargdescr_[3] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.PSLOC__V6*/ meltfptr[5] =
    (meltxargtab_[3].meltbp_aptr) ? (*(meltxargtab_[3].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.PSLOC__V6*/ meltfptr[5])) !=
	      NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-normal.melt:1465:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L1*/ meltfnum[0] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-normal.melt:1465:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L1*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-normal.melt:1465:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[7];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-normal.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 1465;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "normbind_definedvalbind bind=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.BIND__V2*/ meltfptr[1];
	      /*^apply.arg */
	      argtab[5].meltbp_cstring = " psloc=";
	      /*^apply.arg */
	      argtab[6].meltbp_aptr =
		(melt_ptr_t *) & /*_.PSLOC__V6*/ meltfptr[5];
	      /*_.MELT_DEBUG_FUN__V9*/ meltfptr[8] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V8*/ meltfptr[7] = /*_.MELT_DEBUG_FUN__V9*/ meltfptr[8];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-normal.melt:1465:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V9*/ meltfptr[8] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V8*/ meltfptr[7] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-normal.melt:1465:/ quasiblock");


      /*_.PROGN___V10*/ meltfptr[8] = /*_.IF___V8*/ meltfptr[7];;
      /*^compute */
      /*_.IFCPP___V7*/ meltfptr[6] = /*_.PROGN___V10*/ meltfptr[8];;
      /*epilog */

      MELT_LOCATION ("warmelt-normal.melt:1465:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L1*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IF___V8*/ meltfptr[7] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V10*/ meltfptr[8] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V7*/ meltfptr[6] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-normal.melt:1466:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L3*/ meltfnum[1] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.BIND__V2*/ meltfptr[1]),
			     (melt_ptr_t) (( /*!CLASS_DEFINED_VALUE_BINDING */
					    meltfrout->tabval[1])));;
      MELT_LOCATION ("warmelt-normal.melt:1466:/ cond");
      /*cond */ if ( /*_#IS_A__L3*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V12*/ meltfptr[8] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-normal.melt:1466:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check bind"),
				  ("warmelt-normal.melt")
				  ? ("warmelt-normal.melt") : __FILE__,
				  (1466) ? (1466) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V12*/ meltfptr[8] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V11*/ meltfptr[7] = /*_.IFELSE___V12*/ meltfptr[8];;
      /*epilog */

      MELT_LOCATION ("warmelt-normal.melt:1466:/ clear");
	     /*clear *//*_#IS_A__L3*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V12*/ meltfptr[8] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V11*/ meltfptr[7] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-normal.melt:1467:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L4*/ meltfnum[0] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.ENV__V3*/ meltfptr[2]),
			     (melt_ptr_t) (( /*!CLASS_ENVIRONMENT */
					    meltfrout->tabval[2])));;
      MELT_LOCATION ("warmelt-normal.melt:1467:/ cond");
      /*cond */ if ( /*_#IS_A__L4*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V14*/ meltfptr[13] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-normal.melt:1467:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check env"),
				  ("warmelt-normal.melt")
				  ? ("warmelt-normal.melt") : __FILE__,
				  (1467) ? (1467) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V14*/ meltfptr[13] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V13*/ meltfptr[8] = /*_.IFELSE___V14*/ meltfptr[13];;
      /*epilog */

      MELT_LOCATION ("warmelt-normal.melt:1467:/ clear");
	     /*clear *//*_#IS_A__L4*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V14*/ meltfptr[13] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V13*/ meltfptr[8] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-normal.melt:1468:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L5*/ meltfnum[1] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.NCX__V4*/ meltfptr[3]),
			     (melt_ptr_t) (( /*!CLASS_NORMALIZATION_CONTEXT */
					    meltfrout->tabval[3])));;
      MELT_LOCATION ("warmelt-normal.melt:1468:/ cond");
      /*cond */ if ( /*_#IS_A__L5*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V16*/ meltfptr[15] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-normal.melt:1468:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check ncx"),
				  ("warmelt-normal.melt")
				  ? ("warmelt-normal.melt") : __FILE__,
				  (1468) ? (1468) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V16*/ meltfptr[15] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V15*/ meltfptr[13] = /*_.IFELSE___V16*/ meltfptr[15];;
      /*epilog */

      MELT_LOCATION ("warmelt-normal.melt:1468:/ clear");
	     /*clear *//*_#IS_A__L5*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V16*/ meltfptr[15] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V15*/ meltfptr[13] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-normal.melt:1469:/ quasiblock");


    /*^getslot */
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.NCX__V4*/ meltfptr[3]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 6, "NCTX_SYMBCACHEMAP");
  /*_.SYCMAP__V18*/ meltfptr[17] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-normal.melt:1470:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.BIND__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 0, "BINDER");
  /*_.SYMB__V19*/ meltfptr[18] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-normal.melt:1471:/ cond");
    /*cond */ if (
		   /*ifisa */
		   melt_is_instance_of ((melt_ptr_t)
					( /*_.NCX__V4*/ meltfptr[3]),
					(melt_ptr_t) (( /*!CLASS_NORMALIZATION_CONTEXT */ meltfrout->tabval[3])))
      )				/*then */
      {
	/*^cond.then */
	/*^getslot */
	{
	  melt_ptr_t slot = NULL, obj = NULL;
	  obj = (melt_ptr_t) ( /*_.NCX__V4*/ meltfptr[3]) /*=obj*/ ;
	  melt_object_get_field (slot, obj, 10, "NCTX_CURPROC");
   /*_.CURPROC__V20*/ meltfptr[19] = slot;
	};
	;
      }
    else
      {				/*^cond.else */

  /*_.CURPROC__V20*/ meltfptr[19] = NULL;;
      }
    ;
    MELT_LOCATION ("warmelt-normal.melt:1472:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*^rawallocobj */
    /*rawallocobj */
    {
      melt_ptr_t newobj = 0;
      melt_raw_object_create (newobj,
			      (melt_ptr_t) (( /*!CLASS_NREP_DEFINED_CONSTANT */ meltfrout->tabval[4])), (5), "CLASS_NREP_DEFINED_CONSTANT");
  /*_.INST__V22*/ meltfptr[21] =
	newobj;
    };
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @NREP_LOC",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V22*/ meltfptr[21])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V22*/ meltfptr[21]), (0),
			  ( /*_.PSLOC__V6*/ meltfptr[5]), "NREP_LOC");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @NCONST_SVAL",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V22*/ meltfptr[21])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V22*/ meltfptr[21]), (1),
			  ( /*_.SYMB__V19*/ meltfptr[18]), "NCONST_SVAL");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @NCONST_DATA",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V22*/ meltfptr[21])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V22*/ meltfptr[21]), (3),
			  (( /*nil */ NULL)), "NCONST_DATA");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @NCONST_PROC",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V22*/ meltfptr[21])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V22*/ meltfptr[21]), (2),
			  ( /*_.CURPROC__V20*/ meltfptr[19]), "NCONST_PROC");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @NCONST_DEFBIND",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V22*/ meltfptr[21])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V22*/ meltfptr[21]), (4),
			  ( /*_.BIND__V2*/ meltfptr[1]), "NCONST_DEFBIND");
    ;
    /*^touchobj */

    melt_dbgtrace_written_object ( /*_.INST__V22*/ meltfptr[21],
				  "newly made instance");
    ;
    /*_.SYCONST__V21*/ meltfptr[20] = /*_.INST__V22*/ meltfptr[21];;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-normal.melt:1486:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L6*/ meltfnum[0] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-normal.melt:1486:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L6*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L7*/ meltfnum[1] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-normal.melt:1486:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[9];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L7*/ meltfnum[1];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-normal.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 1486;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "normbind_definedvalbind ncx=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.NCX__V4*/ meltfptr[3];
	      /*^apply.arg */
	      argtab[5].meltbp_cstring = "\n env=";
	      /*^apply.arg */
	      argtab[6].meltbp_aptr =
		(melt_ptr_t *) & /*_.ENV__V3*/ meltfptr[2];
	      /*^apply.arg */
	      argtab[7].meltbp_cstring = " procs=";
	      /*^apply.arg */
	      argtab[8].meltbp_aptr =
		(melt_ptr_t *) & /*_.PROCS__V5*/ meltfptr[4];
	      /*_.MELT_DEBUG_FUN__V25*/ meltfptr[24] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V24*/ meltfptr[23] =
	      /*_.MELT_DEBUG_FUN__V25*/ meltfptr[24];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-normal.melt:1486:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L7*/ meltfnum[1] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V25*/ meltfptr[24] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V24*/ meltfptr[23] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-normal.melt:1486:/ quasiblock");


      /*_.PROGN___V26*/ meltfptr[24] = /*_.IF___V24*/ meltfptr[23];;
      /*^compute */
      /*_.IFCPP___V23*/ meltfptr[22] = /*_.PROGN___V26*/ meltfptr[24];;
      /*epilog */

      MELT_LOCATION ("warmelt-normal.melt:1486:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L6*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IF___V24*/ meltfptr[23] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V26*/ meltfptr[24] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V23*/ meltfptr[22] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

    {
      MELT_LOCATION ("warmelt-normal.melt:1489:/ locexp");
      meltgc_put_mapobjects ((meltmapobjects_ptr_t)
			     ( /*_.SYCMAP__V18*/ meltfptr[17]),
			     (meltobject_ptr_t) ( /*_.SYMB__V19*/
						 meltfptr[18]),
			     (melt_ptr_t) ( /*_.SYCONST__V21*/ meltfptr[20]));
    }
    ;
    MELT_LOCATION ("warmelt-normal.melt:1490:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^cond */
    /*cond */ if (
		   /*ifisa */
		   melt_is_instance_of ((melt_ptr_t)
					( /*_.BIND__V2*/ meltfptr[1]),
					(melt_ptr_t) (( /*!CLASS_FIXED_BINDING */ meltfrout->tabval[5])))
      )				/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  /*^putslot */
	  /*putslot */
	  melt_assertmsg ("putslot checkobj @FIXBIND_DATA",
			  melt_magic_discr ((melt_ptr_t)
					    ( /*_.BIND__V2*/ meltfptr[1])) ==
			  MELTOBMAG_OBJECT);
	  melt_putfield_object (( /*_.BIND__V2*/ meltfptr[1]), (1),
				( /*_.SYCONST__V21*/ meltfptr[20]),
				"FIXBIND_DATA");
	  ;
	  /*^touch */
	  meltgc_touch ( /*_.BIND__V2*/ meltfptr[1]);
	  ;
	  /*^touchobj */

	  melt_dbgtrace_written_object ( /*_.BIND__V2*/ meltfptr[1],
					"put-fields");
	  ;
	  /*epilog */
	}
	;
      }				/*noelse */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-normal.melt:1491:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L8*/ meltfnum[1] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-normal.melt:1491:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L8*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L9*/ meltfnum[0] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-normal.melt:1491:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[9];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L9*/ meltfnum[0];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-normal.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 1491;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring =
		"normbind_definedvalbind updated sycmap=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.SYCMAP__V18*/ meltfptr[17];
	      /*^apply.arg */
	      argtab[5].meltbp_cstring = " syconst=";
	      /*^apply.arg */
	      argtab[6].meltbp_aptr =
		(melt_ptr_t *) & /*_.SYCONST__V21*/ meltfptr[20];
	      /*^apply.arg */
	      argtab[7].meltbp_cstring = " bind=";
	      /*^apply.arg */
	      argtab[8].meltbp_aptr =
		(melt_ptr_t *) & /*_.BIND__V2*/ meltfptr[1];
	      /*_.MELT_DEBUG_FUN__V29*/ meltfptr[28] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V28*/ meltfptr[24] =
	      /*_.MELT_DEBUG_FUN__V29*/ meltfptr[28];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-normal.melt:1491:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L9*/ meltfnum[0] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V29*/ meltfptr[28] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V28*/ meltfptr[24] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-normal.melt:1491:/ quasiblock");


      /*_.PROGN___V30*/ meltfptr[28] = /*_.IF___V28*/ meltfptr[24];;
      /*^compute */
      /*_.IFCPP___V27*/ meltfptr[23] = /*_.PROGN___V30*/ meltfptr[28];;
      /*epilog */

      MELT_LOCATION ("warmelt-normal.melt:1491:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L8*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IF___V28*/ meltfptr[24] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V30*/ meltfptr[28] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V27*/ meltfptr[23] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-normal.melt:1492:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#IS_NON_EMPTY_LIST__L10*/ meltfnum[0] =
      (melt_magic_discr ((melt_ptr_t) ( /*_.PROCS__V5*/ meltfptr[4])) ==
       MELTOBMAG_LIST
       && NULL !=
       melt_list_first ((melt_ptr_t) ( /*_.PROCS__V5*/ meltfptr[4])));;
    MELT_LOCATION ("warmelt-normal.melt:1492:/ cond");
    /*cond */ if ( /*_#IS_NON_EMPTY_LIST__L10*/ meltfnum[0])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{


#if MELT_HAVE_DEBUG
	  MELT_LOCATION ("warmelt-normal.melt:1493:/ cppif.then");
	  /*^block */
	  /*anyblock */
	  {

	    /*^checksignal */
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^cond */
	    /*cond */ if (( /*nil */ NULL))	/*then */
	      {
		/*^cond.then */
		/*_.IFELSE___V33*/ meltfptr[32] = ( /*nil */ NULL);;
	      }
	    else
	      {
		MELT_LOCATION ("warmelt-normal.melt:1493:/ cond.else");

		/*^block */
		/*anyblock */
		{




		  {
		    /*^locexp */
		    melt_assert_failed (("normbind_definedvalbind check no procs"), ("warmelt-normal.melt") ? ("warmelt-normal.melt") : __FILE__, (1493) ? (1493) : __LINE__, __FUNCTION__);
		    ;
		  }
		  ;
		 /*clear *//*_.IFELSE___V33*/ meltfptr[32] = 0;
		  /*epilog */
		}
		;
	      }
	    ;
	    /*^compute */
	    /*_.IFCPP___V32*/ meltfptr[28] = /*_.IFELSE___V33*/ meltfptr[32];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-normal.melt:1493:/ clear");
	       /*clear *//*_.IFELSE___V33*/ meltfptr[32] = 0;
	  }

#else /*MELT_HAVE_DEBUG */
	  /*^cppif.else */
	  /*_.IFCPP___V32*/ meltfptr[28] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
	  ;
	  /*^compute */
	  /*_.IF___V31*/ meltfptr[24] = /*_.IFCPP___V32*/ meltfptr[28];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-normal.melt:1492:/ clear");
	     /*clear *//*_.IFCPP___V32*/ meltfptr[28] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

  /*_.IF___V31*/ meltfptr[24] = NULL;;
      }
    ;
    MELT_LOCATION ("warmelt-normal.melt:1494:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.SYCONST__V21*/ meltfptr[20];;

    {
      MELT_LOCATION ("warmelt-normal.melt:1494:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*_.LET___V17*/ meltfptr[15] = /*_.RETURN___V34*/ meltfptr[32];;

    MELT_LOCATION ("warmelt-normal.melt:1469:/ clear");
	   /*clear *//*_.SYCMAP__V18*/ meltfptr[17] = 0;
    /*^clear */
	   /*clear *//*_.SYMB__V19*/ meltfptr[18] = 0;
    /*^clear */
	   /*clear *//*_.CURPROC__V20*/ meltfptr[19] = 0;
    /*^clear */
	   /*clear *//*_.SYCONST__V21*/ meltfptr[20] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V23*/ meltfptr[22] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V27*/ meltfptr[23] = 0;
    /*^clear */
	   /*clear *//*_#IS_NON_EMPTY_LIST__L10*/ meltfnum[0] = 0;
    /*^clear */
	   /*clear *//*_.IF___V31*/ meltfptr[24] = 0;
    /*^clear */
	   /*clear *//*_.RETURN___V34*/ meltfptr[32] = 0;
    MELT_LOCATION ("warmelt-normal.melt:1464:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.LET___V17*/ meltfptr[15];;

    {
      MELT_LOCATION ("warmelt-normal.melt:1464:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V7*/ meltfptr[6] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V11*/ meltfptr[7] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V13*/ meltfptr[8] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V15*/ meltfptr[13] = 0;
    /*^clear */
	   /*clear *//*_.LET___V17*/ meltfptr[15] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("NORMBIND_DEFINEDVALBIND", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_30_warmelt_normal_NORMBIND_DEFINEDVALBIND_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_30_warmelt_normal_NORMBIND_DEFINEDVALBIND */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_31_warmelt_normal_NORMBIND_CONSTRUCTBIND (meltclosure_ptr_t
						   meltclosp_,
						   melt_ptr_t meltfirstargp_,
						   const melt_argdescr_cell_t
						   meltxargdescr_[],
						   union meltparam_un *
						   meltxargtab_,
						   const melt_argdescr_cell_t
						   meltxresdescr_[],
						   union meltparam_un *
						   meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_31_warmelt_normal_NORMBIND_CONSTRUCTBIND_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_31_warmelt_normal_NORMBIND_CONSTRUCTBIND_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 45
    melt_ptr_t mcfr_varptr[45];
#define MELTFRAM_NBVARNUM 16
    long mcfr_varnum[16];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_31_warmelt_normal_NORMBIND_CONSTRUCTBIND is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_31_warmelt_normal_NORMBIND_CONSTRUCTBIND_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 45; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_31_warmelt_normal_NORMBIND_CONSTRUCTBIND nbval 45*/
  meltfram__.mcfr_nbvar = 45 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("NORMBIND_CONSTRUCTBIND", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-normal.melt:1499:/ getarg");
 /*_.BIND__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.ENV__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.ENV__V3*/ meltfptr[2])) != NULL);


  /*getarg#2 */
  /*^getarg */
  if (meltxargdescr_[1] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.NCX__V4*/ meltfptr[3] =
    (meltxargtab_[1].meltbp_aptr) ? (*(meltxargtab_[1].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.NCX__V4*/ meltfptr[3])) != NULL);


  /*getarg#3 */
  /*^getarg */
  if (meltxargdescr_[2] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.PROCS__V5*/ meltfptr[4] =
    (meltxargtab_[2].meltbp_aptr) ? (*(meltxargtab_[2].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.PROCS__V5*/ meltfptr[4])) !=
	      NULL);


  /*getarg#4 */
  /*^getarg */
  if (meltxargdescr_[3] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.PSLOC__V6*/ meltfptr[5] =
    (meltxargtab_[3].meltbp_aptr) ? (*(meltxargtab_[3].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.PSLOC__V6*/ meltfptr[5])) !=
	      NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-normal.melt:1500:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L1*/ meltfnum[0] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.BIND__V2*/ meltfptr[1]),
			     (melt_ptr_t) (( /*!CLASS_NORMAL_CONSTRUCTOR_BINDING */ meltfrout->tabval[0])));;
      MELT_LOCATION ("warmelt-normal.melt:1500:/ cond");
      /*cond */ if ( /*_#IS_A__L1*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V8*/ meltfptr[7] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-normal.melt:1500:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check bind"),
				  ("warmelt-normal.melt")
				  ? ("warmelt-normal.melt") : __FILE__,
				  (1500) ? (1500) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V8*/ meltfptr[7] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V7*/ meltfptr[6] = /*_.IFELSE___V8*/ meltfptr[7];;
      /*epilog */

      MELT_LOCATION ("warmelt-normal.melt:1500:/ clear");
	     /*clear *//*_#IS_A__L1*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V8*/ meltfptr[7] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V7*/ meltfptr[6] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-normal.melt:1501:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L2*/ meltfnum[0] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.ENV__V3*/ meltfptr[2]),
			     (melt_ptr_t) (( /*!CLASS_ENVIRONMENT */
					    meltfrout->tabval[1])));;
      MELT_LOCATION ("warmelt-normal.melt:1501:/ cond");
      /*cond */ if ( /*_#IS_A__L2*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V10*/ meltfptr[9] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-normal.melt:1501:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check env"),
				  ("warmelt-normal.melt")
				  ? ("warmelt-normal.melt") : __FILE__,
				  (1501) ? (1501) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V10*/ meltfptr[9] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V9*/ meltfptr[7] = /*_.IFELSE___V10*/ meltfptr[9];;
      /*epilog */

      MELT_LOCATION ("warmelt-normal.melt:1501:/ clear");
	     /*clear *//*_#IS_A__L2*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V10*/ meltfptr[9] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V9*/ meltfptr[7] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-normal.melt:1502:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L3*/ meltfnum[0] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.NCX__V4*/ meltfptr[3]),
			     (melt_ptr_t) (( /*!CLASS_NORMALIZATION_CONTEXT */
					    meltfrout->tabval[2])));;
      MELT_LOCATION ("warmelt-normal.melt:1502:/ cond");
      /*cond */ if ( /*_#IS_A__L3*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V12*/ meltfptr[11] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-normal.melt:1502:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check ncx"),
				  ("warmelt-normal.melt")
				  ? ("warmelt-normal.melt") : __FILE__,
				  (1502) ? (1502) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V12*/ meltfptr[11] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V11*/ meltfptr[9] = /*_.IFELSE___V12*/ meltfptr[11];;
      /*epilog */

      MELT_LOCATION ("warmelt-normal.melt:1502:/ clear");
	     /*clear *//*_#IS_A__L3*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V12*/ meltfptr[11] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V11*/ meltfptr[9] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-normal.melt:1503:/ quasiblock");


    /*^getslot */
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.NCX__V4*/ meltfptr[3]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 6, "NCTX_SYMBCACHEMAP");
  /*_.SYCMAP__V14*/ meltfptr[13] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-normal.melt:1504:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.BIND__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 0, "BINDER");
  /*_.SYMB__V15*/ meltfptr[14] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-normal.melt:1505:/ cond");
    /*cond */ if (
		   /*ifisa */
		   melt_is_instance_of ((melt_ptr_t)
					( /*_.BIND__V2*/ meltfptr[1]),
					(melt_ptr_t) (( /*!CLASS_NORMAL_CONSTRUCTOR_BINDING */ meltfrout->tabval[0])))
      )				/*then */
      {
	/*^cond.then */
	/*^getslot */
	{
	  melt_ptr_t slot = NULL, obj = NULL;
	  obj = (melt_ptr_t) ( /*_.BIND__V2*/ meltfptr[1]) /*=obj*/ ;
	  melt_object_get_field (slot, obj, 3, "NCONSB_NLETREC");
   /*_.NLETREC__V16*/ meltfptr[15] = slot;
	};
	;
      }
    else
      {				/*^cond.else */

  /*_.NLETREC__V16*/ meltfptr[15] = NULL;;
      }
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-normal.melt:1507:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L4*/ meltfnum[0] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-normal.melt:1507:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L4*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L5*/ meltfnum[4] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-normal.melt:1507:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L5*/ meltfnum[4];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-normal.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 1507;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "normbind_constructbind nletrec";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.NLETREC__V16*/ meltfptr[15];
	      /*_.MELT_DEBUG_FUN__V19*/ meltfptr[18] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[3])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V18*/ meltfptr[17] =
	      /*_.MELT_DEBUG_FUN__V19*/ meltfptr[18];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-normal.melt:1507:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L5*/ meltfnum[4] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V19*/ meltfptr[18] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V18*/ meltfptr[17] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-normal.melt:1507:/ quasiblock");


      /*_.PROGN___V20*/ meltfptr[18] = /*_.IF___V18*/ meltfptr[17];;
      /*^compute */
      /*_.IFCPP___V17*/ meltfptr[16] = /*_.PROGN___V20*/ meltfptr[18];;
      /*epilog */

      MELT_LOCATION ("warmelt-normal.melt:1507:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L4*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IF___V18*/ meltfptr[17] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V20*/ meltfptr[18] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V17*/ meltfptr[16] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-normal.melt:1508:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L6*/ meltfnum[4] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.NLETREC__V16*/ meltfptr[15]),
			     (melt_ptr_t) (( /*!CLASS_NREP_LETREC */
					    meltfrout->tabval[4])));;
      MELT_LOCATION ("warmelt-normal.melt:1508:/ cond");
      /*cond */ if ( /*_#IS_A__L6*/ meltfnum[4])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V22*/ meltfptr[18] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-normal.melt:1508:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("normexp_symbol check nletrec"),
				  ("warmelt-normal.melt")
				  ? ("warmelt-normal.melt") : __FILE__,
				  (1508) ? (1508) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V22*/ meltfptr[18] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V21*/ meltfptr[17] = /*_.IFELSE___V22*/ meltfptr[18];;
      /*epilog */

      MELT_LOCATION ("warmelt-normal.melt:1508:/ clear");
	     /*clear *//*_#IS_A__L6*/ meltfnum[4] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V22*/ meltfptr[18] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V21*/ meltfptr[17] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-normal.melt:1509:/ quasiblock");


    /*^cond */
    /*cond */ if (
		   /*ifisa */
		   melt_is_instance_of ((melt_ptr_t)
					( /*_.NLETREC__V16*/ meltfptr[15]),
					(melt_ptr_t) (( /*!CLASS_NREP_LETREC */ meltfrout->tabval[4])))
      )				/*then */
      {
	/*^cond.then */
	/*^getslot */
	{
	  melt_ptr_t slot = NULL, obj = NULL;
	  obj = (melt_ptr_t) ( /*_.NLETREC__V16*/ meltfptr[15]) /*=obj*/ ;
	  melt_object_get_field (slot, obj, 5, "NLETREC_LOCSYMS");
   /*_.NLOCSYMS__V24*/ meltfptr[23] = slot;
	};
	;
      }
    else
      {				/*^cond.else */

  /*_.NLOCSYMS__V24*/ meltfptr[23] = NULL;;
      }
    ;
    /*^compute */
    /*_.OURLOCSYM__V25*/ meltfptr[24] = ( /*nil */ NULL);;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-normal.melt:1512:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L7*/ meltfnum[0] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-normal.melt:1512:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L7*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L8*/ meltfnum[4] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-normal.melt:1512:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L8*/ meltfnum[4];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-normal.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 1512;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "normbind_constructbind nlocsyms";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.NLOCSYMS__V24*/ meltfptr[23];
	      /*_.MELT_DEBUG_FUN__V28*/ meltfptr[27] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[3])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V27*/ meltfptr[26] =
	      /*_.MELT_DEBUG_FUN__V28*/ meltfptr[27];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-normal.melt:1512:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L8*/ meltfnum[4] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V28*/ meltfptr[27] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V27*/ meltfptr[26] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-normal.melt:1512:/ quasiblock");


      /*_.PROGN___V29*/ meltfptr[27] = /*_.IF___V27*/ meltfptr[26];;
      /*^compute */
      /*_.IFCPP___V26*/ meltfptr[25] = /*_.PROGN___V29*/ meltfptr[27];;
      /*epilog */

      MELT_LOCATION ("warmelt-normal.melt:1512:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L7*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IF___V27*/ meltfptr[26] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V29*/ meltfptr[27] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V26*/ meltfptr[25] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    /*citerblock FOREACH_IN_MULTIPLE */
    {
      /* start foreach_in_multiple meltcit1__EACHTUP */
      long meltcit1__EACHTUP_ln =
	melt_multiple_length ((melt_ptr_t) /*_.NLOCSYMS__V24*/ meltfptr[23]);
      for ( /*_#SYIX__L9*/ meltfnum[4] = 0;
	   ( /*_#SYIX__L9*/ meltfnum[4] >= 0)
	   && ( /*_#SYIX__L9*/ meltfnum[4] < meltcit1__EACHTUP_ln);
	/*_#SYIX__L9*/ meltfnum[4]++)
	{
	  /*_.CURLOCSYM__V30*/ meltfptr[26] =
	    melt_multiple_nth ((melt_ptr_t)
			       ( /*_.NLOCSYMS__V24*/ meltfptr[23]),
			       /*_#SYIX__L9*/ meltfnum[4]);



	  MELT_LOCATION ("warmelt-normal.melt:1517:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^cond */
	  /*cond */ if (
			 /*ifisa */
			 melt_is_instance_of ((melt_ptr_t)
					      ( /*_.CURLOCSYM__V30*/
					       meltfptr[26]),
					      (melt_ptr_t) (( /*!CLASS_NREP_SYMOCC */ meltfrout->tabval[5])))
	    )			/*then */
	    {
	      /*^cond.then */
	      /*^getslot */
	      {
		melt_ptr_t slot = NULL, obj = NULL;
		obj =
		  (melt_ptr_t) ( /*_.CURLOCSYM__V30*/ meltfptr[26]) /*=obj*/ ;
		melt_object_get_field (slot, obj, 3, "NOCC_BIND");
    /*_.NOCC_BIND__V31*/ meltfptr[27] = slot;
	      };
	      ;
	    }
	  else
	    {			/*^cond.else */

   /*_.NOCC_BIND__V31*/ meltfptr[27] = NULL;;
	    }
	  ;
	  /*^compute */
  /*_#__L10*/ meltfnum[0] =
	    (( /*_.NOCC_BIND__V31*/ meltfptr[27]) ==
	     ( /*_.BIND__V2*/ meltfptr[1]));;
	  MELT_LOCATION ("warmelt-normal.melt:1517:/ cond");
	  /*cond */ if ( /*_#__L10*/ meltfnum[0])	/*then */
	    {
	      /*^cond.then */
	      /*^block */
	      /*anyblock */
	      {

		MELT_LOCATION ("warmelt-normal.melt:1519:/ compute");
		/*_#SYIX__L9*/ meltfnum[4] = /*_#SETQ___L11*/ meltfnum[10] =
		  -99;;
		MELT_LOCATION ("warmelt-normal.melt:1520:/ compute");
		/*_.OURLOCSYM__V25*/ meltfptr[24] =
		  /*_.SETQ___V33*/ meltfptr[32] =
		  /*_.CURLOCSYM__V30*/ meltfptr[26];;
		MELT_LOCATION ("warmelt-normal.melt:1518:/ quasiblock");


		/*_.PROGN___V34*/ meltfptr[33] =
		  /*_.SETQ___V33*/ meltfptr[32];;
		/*^compute */
		/*_.IF___V32*/ meltfptr[31] = /*_.PROGN___V34*/ meltfptr[33];;
		/*epilog */

		MELT_LOCATION ("warmelt-normal.melt:1517:/ clear");
	      /*clear *//*_#SETQ___L11*/ meltfnum[10] = 0;
		/*^clear */
	      /*clear *//*_.SETQ___V33*/ meltfptr[32] = 0;
		/*^clear */
	      /*clear *//*_.PROGN___V34*/ meltfptr[33] = 0;
	      }
	      ;
	    }
	  else
	    {			/*^cond.else */

   /*_.IF___V32*/ meltfptr[31] = NULL;;
	    }
	  ;
	  if ( /*_#SYIX__L9*/ meltfnum[4] < 0)
	    break;
	}			/* end  foreach_in_multiple meltcit1__EACHTUP */

      /*citerepilog */

      MELT_LOCATION ("warmelt-normal.melt:1514:/ clear");
	    /*clear *//*_.CURLOCSYM__V30*/ meltfptr[26] = 0;
      /*^clear */
	    /*clear *//*_#SYIX__L9*/ meltfnum[4] = 0;
      /*^clear */
	    /*clear *//*_.NOCC_BIND__V31*/ meltfptr[27] = 0;
      /*^clear */
	    /*clear *//*_#__L10*/ meltfnum[0] = 0;
      /*^clear */
	    /*clear *//*_.IF___V32*/ meltfptr[31] = 0;
    }				/*endciterblock FOREACH_IN_MULTIPLE */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-normal.melt:1522:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L12*/ meltfnum[10] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-normal.melt:1522:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L12*/ meltfnum[10])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L13*/ meltfnum[12] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-normal.melt:1522:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long =
		/*_#THE_MELTCALLCOUNT__L13*/ meltfnum[12];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-normal.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 1522;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "normbind_constructbind ourlocsym";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.OURLOCSYM__V25*/ meltfptr[24];
	      /*_.MELT_DEBUG_FUN__V37*/ meltfptr[36] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[3])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V36*/ meltfptr[33] =
	      /*_.MELT_DEBUG_FUN__V37*/ meltfptr[36];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-normal.melt:1522:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L13*/ meltfnum[12] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V37*/ meltfptr[36] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V36*/ meltfptr[33] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-normal.melt:1522:/ quasiblock");


      /*_.PROGN___V38*/ meltfptr[36] = /*_.IF___V36*/ meltfptr[33];;
      /*^compute */
      /*_.IFCPP___V35*/ meltfptr[32] = /*_.PROGN___V38*/ meltfptr[36];;
      /*epilog */

      MELT_LOCATION ("warmelt-normal.melt:1522:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L12*/ meltfnum[10] = 0;
      /*^clear */
	     /*clear *//*_.IF___V36*/ meltfptr[33] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V38*/ meltfptr[36] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V35*/ meltfptr[32] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-normal.melt:1523:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L14*/ meltfnum[12] =
	melt_is_instance_of ((melt_ptr_t)
			     ( /*_.OURLOCSYM__V25*/ meltfptr[24]),
			     (melt_ptr_t) (( /*!CLASS_NREP_LOCSYMOCC */
					    meltfrout->tabval[6])));;
      MELT_LOCATION ("warmelt-normal.melt:1523:/ cond");
      /*cond */ if ( /*_#IS_A__L14*/ meltfnum[12])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V40*/ meltfptr[36] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-normal.melt:1523:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("normbind_constructbind should have ourlocsym"), ("warmelt-normal.melt") ? ("warmelt-normal.melt") : __FILE__, (1523) ? (1523) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V40*/ meltfptr[36] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V39*/ meltfptr[33] = /*_.IFELSE___V40*/ meltfptr[36];;
      /*epilog */

      MELT_LOCATION ("warmelt-normal.melt:1523:/ clear");
	     /*clear *//*_#IS_A__L14*/ meltfnum[12] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V40*/ meltfptr[36] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V39*/ meltfptr[33] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

    {
      MELT_LOCATION ("warmelt-normal.melt:1526:/ locexp");
      meltgc_put_mapobjects ((meltmapobjects_ptr_t)
			     ( /*_.SYCMAP__V14*/ meltfptr[13]),
			     (meltobject_ptr_t) ( /*_.SYMB__V15*/
						 meltfptr[14]),
			     (melt_ptr_t) ( /*_.OURLOCSYM__V25*/
					   meltfptr[24]));
    }
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-normal.melt:1527:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L15*/ meltfnum[10] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-normal.melt:1527:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L15*/ meltfnum[10])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L16*/ meltfnum[12] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-normal.melt:1527:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[9];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long =
		/*_#THE_MELTCALLCOUNT__L16*/ meltfnum[12];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-normal.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 1527;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring =
		"normbind_constructbind updated sycmap=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.SYCMAP__V14*/ meltfptr[13];
	      /*^apply.arg */
	      argtab[5].meltbp_cstring = " symb=";
	      /*^apply.arg */
	      argtab[6].meltbp_aptr =
		(melt_ptr_t *) & /*_.SYMB__V15*/ meltfptr[14];
	      /*^apply.arg */
	      argtab[7].meltbp_cstring = " ourlocsym=";
	      /*^apply.arg */
	      argtab[8].meltbp_aptr =
		(melt_ptr_t *) & /*_.OURLOCSYM__V25*/ meltfptr[24];
	      /*_.MELT_DEBUG_FUN__V43*/ meltfptr[42] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[3])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V42*/ meltfptr[41] =
	      /*_.MELT_DEBUG_FUN__V43*/ meltfptr[42];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-normal.melt:1527:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L16*/ meltfnum[12] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V43*/ meltfptr[42] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V42*/ meltfptr[41] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-normal.melt:1527:/ quasiblock");


      /*_.PROGN___V44*/ meltfptr[42] = /*_.IF___V42*/ meltfptr[41];;
      /*^compute */
      /*_.IFCPP___V41*/ meltfptr[36] = /*_.PROGN___V44*/ meltfptr[42];;
      /*epilog */

      MELT_LOCATION ("warmelt-normal.melt:1527:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L15*/ meltfnum[10] = 0;
      /*^clear */
	     /*clear *//*_.IF___V42*/ meltfptr[41] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V44*/ meltfptr[42] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V41*/ meltfptr[36] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-normal.melt:1528:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.OURLOCSYM__V25*/ meltfptr[24];;

    {
      MELT_LOCATION ("warmelt-normal.melt:1528:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*_.LET___V23*/ meltfptr[18] = /*_.RETURN___V45*/ meltfptr[41];;

    MELT_LOCATION ("warmelt-normal.melt:1509:/ clear");
	   /*clear *//*_.NLOCSYMS__V24*/ meltfptr[23] = 0;
    /*^clear */
	   /*clear *//*_.OURLOCSYM__V25*/ meltfptr[24] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V26*/ meltfptr[25] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V35*/ meltfptr[32] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V39*/ meltfptr[33] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V41*/ meltfptr[36] = 0;
    /*^clear */
	   /*clear *//*_.RETURN___V45*/ meltfptr[41] = 0;
    /*_.LET___V13*/ meltfptr[11] = /*_.LET___V23*/ meltfptr[18];;

    MELT_LOCATION ("warmelt-normal.melt:1503:/ clear");
	   /*clear *//*_.SYCMAP__V14*/ meltfptr[13] = 0;
    /*^clear */
	   /*clear *//*_.SYMB__V15*/ meltfptr[14] = 0;
    /*^clear */
	   /*clear *//*_.NLETREC__V16*/ meltfptr[15] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V17*/ meltfptr[16] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V21*/ meltfptr[17] = 0;
    /*^clear */
	   /*clear *//*_.LET___V23*/ meltfptr[18] = 0;
    MELT_LOCATION ("warmelt-normal.melt:1499:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.LET___V13*/ meltfptr[11];;

    {
      MELT_LOCATION ("warmelt-normal.melt:1499:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V7*/ meltfptr[6] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V9*/ meltfptr[7] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V11*/ meltfptr[9] = 0;
    /*^clear */
	   /*clear *//*_.LET___V13*/ meltfptr[11] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("NORMBIND_CONSTRUCTBIND", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_31_warmelt_normal_NORMBIND_CONSTRUCTBIND_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_31_warmelt_normal_NORMBIND_CONSTRUCTBIND */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_32_warmelt_normal_NORMEXP_SYMBOL (meltclosure_ptr_t meltclosp_,
					   melt_ptr_t meltfirstargp_,
					   const melt_argdescr_cell_t
					   meltxargdescr_[],
					   union meltparam_un * meltxargtab_,
					   const melt_argdescr_cell_t
					   meltxresdescr_[],
					   union meltparam_un * meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_32_warmelt_normal_NORMEXP_SYMBOL_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_32_warmelt_normal_NORMEXP_SYMBOL_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 130
    melt_ptr_t mcfr_varptr[130];
#define MELTFRAM_NBVARNUM 42
    long mcfr_varnum[42];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_32_warmelt_normal_NORMEXP_SYMBOL is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_32_warmelt_normal_NORMEXP_SYMBOL_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 130; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_32_warmelt_normal_NORMEXP_SYMBOL nbval 130*/
  meltfram__.mcfr_nbvar = 130 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("NORMEXP_SYMBOL", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-normal.melt:1540:/ getarg");
 /*_.RECV__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.ENV__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.ENV__V3*/ meltfptr[2])) != NULL);


  /*getarg#2 */
  /*^getarg */
  if (meltxargdescr_[1] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.NCX__V4*/ meltfptr[3] =
    (meltxargtab_[1].meltbp_aptr) ? (*(meltxargtab_[1].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.NCX__V4*/ meltfptr[3])) != NULL);


  /*getarg#3 */
  /*^getarg */
  if (meltxargdescr_[2] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.PSLOC__V5*/ meltfptr[4] =
    (meltxargtab_[2].meltbp_aptr) ? (*(meltxargtab_[2].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.PSLOC__V5*/ meltfptr[4])) !=
	      NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-normal.melt:1541:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L1*/ meltfnum[0] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-normal.melt:1541:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L1*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-normal.melt:1541:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[7];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-normal.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 1541;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "normexp_symbol recv=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.RECV__V2*/ meltfptr[1];
	      /*^apply.arg */
	      argtab[5].meltbp_cstring = " psloc=";
	      /*^apply.arg */
	      argtab[6].meltbp_aptr =
		(melt_ptr_t *) & /*_.PSLOC__V5*/ meltfptr[4];
	      /*_.MELT_DEBUG_FUN__V8*/ meltfptr[7] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V7*/ meltfptr[6] = /*_.MELT_DEBUG_FUN__V8*/ meltfptr[7];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-normal.melt:1541:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V8*/ meltfptr[7] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V7*/ meltfptr[6] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-normal.melt:1541:/ quasiblock");


      /*_.PROGN___V9*/ meltfptr[7] = /*_.IF___V7*/ meltfptr[6];;
      /*^compute */
      /*_.IFCPP___V6*/ meltfptr[5] = /*_.PROGN___V9*/ meltfptr[7];;
      /*epilog */

      MELT_LOCATION ("warmelt-normal.melt:1541:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L1*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IF___V7*/ meltfptr[6] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V9*/ meltfptr[7] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V6*/ meltfptr[5] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-normal.melt:1542:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L3*/ meltfnum[1] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.NCX__V4*/ meltfptr[3]),
			     (melt_ptr_t) (( /*!CLASS_NORMALIZATION_CONTEXT */
					    meltfrout->tabval[1])));;
      MELT_LOCATION ("warmelt-normal.melt:1542:/ cond");
      /*cond */ if ( /*_#IS_A__L3*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V11*/ meltfptr[7] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-normal.melt:1542:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check ncx"),
				  ("warmelt-normal.melt")
				  ? ("warmelt-normal.melt") : __FILE__,
				  (1542) ? (1542) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V11*/ meltfptr[7] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V10*/ meltfptr[6] = /*_.IFELSE___V11*/ meltfptr[7];;
      /*epilog */

      MELT_LOCATION ("warmelt-normal.melt:1542:/ clear");
	     /*clear *//*_#IS_A__L3*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V11*/ meltfptr[7] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V10*/ meltfptr[6] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-normal.melt:1545:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    MELT_LOCATION ("warmelt-normal.melt:1543:/ quasiblock");


    /*^multiapply */
    /*multiapply 2args, 1x.res */
    {
      union meltparam_un argtab[1];

      union meltparam_un restab[1];
      memset (&restab, 0, sizeof (restab));
      memset (&argtab, 0, sizeof (argtab));
      /*^multiapply.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.RECV__V2*/ meltfptr[1];
      /*^multiapply.xres */
      restab[0].meltbp_aptr = (melt_ptr_t *) & /*_.PROCS__V14*/ meltfptr[13];
      /*^multiapply.appl */
      /*_.BIND__V13*/ meltfptr[12] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!FIND_ENCLOSING_ENV */ meltfrout->tabval[2])),
		    (melt_ptr_t) ( /*_.ENV__V3*/ meltfptr[2]),
		    (MELTBPARSTR_PTR ""), argtab, (MELTBPARSTR_PTR ""),
		    restab);
    }
    ;
    /*^quasiblock */



#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-normal.melt:1546:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L4*/ meltfnum[0] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-normal.melt:1546:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L4*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L5*/ meltfnum[1] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-normal.melt:1546:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[7];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L5*/ meltfnum[1];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-normal.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 1546;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring =
		"normexp_symbol after find_enclosing_env bind=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.BIND__V13*/ meltfptr[12];
	      /*^apply.arg */
	      argtab[5].meltbp_cstring = " procs=";
	      /*^apply.arg */
	      argtab[6].meltbp_aptr =
		(melt_ptr_t *) & /*_.PROCS__V14*/ meltfptr[13];
	      /*_.MELT_DEBUG_FUN__V17*/ meltfptr[16] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V16*/ meltfptr[15] =
	      /*_.MELT_DEBUG_FUN__V17*/ meltfptr[16];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-normal.melt:1546:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L5*/ meltfnum[1] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V17*/ meltfptr[16] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V16*/ meltfptr[15] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-normal.melt:1546:/ quasiblock");


      /*_.PROGN___V18*/ meltfptr[16] = /*_.IF___V16*/ meltfptr[15];;
      /*^compute */
      /*_.IFCPP___V15*/ meltfptr[14] = /*_.PROGN___V18*/ meltfptr[16];;
      /*epilog */

      MELT_LOCATION ("warmelt-normal.melt:1546:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L4*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IF___V16*/ meltfptr[15] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V18*/ meltfptr[16] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V15*/ meltfptr[14] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-normal.melt:1547:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L6*/ meltfnum[1] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.RECV__V2*/ meltfptr[1]),
			     (melt_ptr_t) (( /*!CLASS_SYMBOL */ meltfrout->
					    tabval[3])));;
      MELT_LOCATION ("warmelt-normal.melt:1547:/ cond");
      /*cond */ if ( /*_#IS_A__L6*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V20*/ meltfptr[16] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-normal.melt:1547:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("normexp_symbol check recv"),
				  ("warmelt-normal.melt")
				  ? ("warmelt-normal.melt") : __FILE__,
				  (1547) ? (1547) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V20*/ meltfptr[16] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V19*/ meltfptr[15] = /*_.IFELSE___V20*/ meltfptr[16];;
      /*epilog */

      MELT_LOCATION ("warmelt-normal.melt:1547:/ clear");
	     /*clear *//*_#IS_A__L6*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V20*/ meltfptr[16] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V19*/ meltfptr[15] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-normal.melt:1548:/ quasiblock");


    /*^getslot */
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.NCX__V4*/ meltfptr[3]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 11, "NCTX_MODULCONTEXT");
  /*_.MODCTX__V22*/ meltfptr[21] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-normal.melt:1549:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.NCX__V4*/ meltfptr[3]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 9, "NCTX_VALBINDMAP");
  /*_.VALBINDMAP__V23*/ meltfptr[22] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-normal.melt:1550:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.NCX__V4*/ meltfptr[3]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 3, "NCTX_VALUELIST");
  /*_.VALUELIST__V24*/ meltfptr[23] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-normal.melt:1552:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#NULL__L7*/ meltfnum[0] =
      (( /*_.PSLOC__V5*/ meltfptr[4]) == NULL);;
    MELT_LOCATION ("warmelt-normal.melt:1552:/ cond");
    /*cond */ if ( /*_#NULL__L7*/ meltfnum[0])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{


	  {
	    MELT_LOCATION ("warmelt-normal.melt:1553:/ locexp");

#if MELT_HAVE_DEBUG
	    if (melt_need_debug (0))
	      melt_dbgshortbacktrace (("normex_symbol null psloc"), (10));
#endif
	    ;
	  }
	  ;
	  /*epilog */
	}
	;
      }				/*noelse */
    ;
    MELT_LOCATION ("warmelt-normal.melt:1555:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#NULL__L8*/ meltfnum[1] =
      (( /*_.BIND__V13*/ meltfptr[12]) == NULL);;
    MELT_LOCATION ("warmelt-normal.melt:1555:/ cond");
    /*cond */ if ( /*_#NULL__L8*/ meltfnum[1])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-normal.melt:1558:/ getslot");
	  {
	    melt_ptr_t slot = NULL, obj = NULL;
	    obj = (melt_ptr_t) ( /*_.RECV__V2*/ meltfptr[1]) /*=obj*/ ;
	    melt_object_get_field (slot, obj, 1, "NAMED_NAME");
    /*_.NAMED_NAME__V26*/ meltfptr[25] = slot;
	  };
	  ;

	  {
	    MELT_LOCATION ("warmelt-normal.melt:1557:/ locexp");
	    melt_error_str ((melt_ptr_t) ( /*_.PSLOC__V5*/ meltfptr[4]),
			    ("unknown name; symbol is not bound"),
			    (melt_ptr_t) ( /*_.NAMED_NAME__V26*/
					  meltfptr[25]));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-normal.melt:1559:/ locexp");

#if MELT_HAVE_DEBUG
	    if (melt_need_debug (0))
	      melt_dbgshortbacktrace (("normex_symbol null bind"), (15));
#endif
	    ;
	  }
	  ;
	  MELT_LOCATION ("warmelt-normal.melt:1560:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^quasiblock */


	  /*_.RETVAL___V1*/ meltfptr[0] = ( /*nil */ NULL);;
	  MELT_LOCATION ("warmelt-normal.melt:1560:/ putxtraresult");
	  if (!meltxrestab_ || !meltxresdescr_)
	    goto labend_rout;
	  if (meltxresdescr_[0] != MELTBPAR_PTR)
	    goto labend_rout;
	  if (meltxrestab_[0].meltbp_aptr)
	    *(meltxrestab_[0].meltbp_aptr) = (melt_ptr_t) (( /*nil */ NULL));
	  ;
	  /*^finalreturn */
	  ;
	  /*finalret */ goto labend_rout;
	  MELT_LOCATION ("warmelt-normal.melt:1556:/ quasiblock");


	  /*_.PROGN___V28*/ meltfptr[27] = /*_.RETURN___V27*/ meltfptr[26];;
	  /*^compute */
	  /*_.IF___V25*/ meltfptr[24] = /*_.PROGN___V28*/ meltfptr[27];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-normal.melt:1555:/ clear");
	     /*clear *//*_.NAMED_NAME__V26*/ meltfptr[25] = 0;
	  /*^clear */
	     /*clear *//*_.RETURN___V27*/ meltfptr[26] = 0;
	  /*^clear */
	     /*clear *//*_.PROGN___V28*/ meltfptr[27] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

  /*_.IF___V25*/ meltfptr[24] = NULL;;
      }
    ;
    MELT_LOCATION ("warmelt-normal.melt:1561:/ quasiblock");


    /*^getslot */
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.NCX__V4*/ meltfptr[3]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 6, "NCTX_SYMBCACHEMAP");
  /*_.SYCMAP__V30*/ meltfptr[26] = slot;
    };
    ;
 /*_.SYCA__V31*/ meltfptr[27] =
      /*mapobject_get */
      melt_get_mapobjects ((meltmapobjects_ptr_t)
			   ( /*_.SYCMAP__V30*/ meltfptr[26]),
			   (meltobject_ptr_t) ( /*_.RECV__V2*/ meltfptr[1]));;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-normal.melt:1563:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_MAPOBJECT__L9*/ meltfnum[8] =
	/*is_mapobject: */
	(melt_magic_discr ((melt_ptr_t) ( /*_.SYCMAP__V30*/ meltfptr[26])) ==
	 MELTOBMAG_MAPOBJECTS);;
      MELT_LOCATION ("warmelt-normal.melt:1563:/ cond");
      /*cond */ if ( /*_#IS_MAPOBJECT__L9*/ meltfnum[8])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V33*/ meltfptr[32] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-normal.melt:1563:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check sycmap"),
				  ("warmelt-normal.melt")
				  ? ("warmelt-normal.melt") : __FILE__,
				  (1563) ? (1563) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V33*/ meltfptr[32] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V32*/ meltfptr[31] = /*_.IFELSE___V33*/ meltfptr[32];;
      /*epilog */

      MELT_LOCATION ("warmelt-normal.melt:1563:/ clear");
	     /*clear *//*_#IS_MAPOBJECT__L9*/ meltfnum[8] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V33*/ meltfptr[32] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V32*/ meltfptr[31] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-normal.melt:1564:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L10*/ meltfnum[8] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-normal.melt:1564:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L10*/ meltfnum[8])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L11*/ meltfnum[10] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-normal.melt:1564:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[9];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long =
		/*_#THE_MELTCALLCOUNT__L11*/ meltfnum[10];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-normal.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 1564;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "normex_symbol syca=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.SYCA__V31*/ meltfptr[27];
	      /*^apply.arg */
	      argtab[5].meltbp_cstring = " for recv=";
	      /*^apply.arg */
	      argtab[6].meltbp_aptr =
		(melt_ptr_t *) & /*_.RECV__V2*/ meltfptr[1];
	      /*^apply.arg */
	      argtab[7].meltbp_cstring = " bind=";
	      /*^apply.arg */
	      argtab[8].meltbp_aptr =
		(melt_ptr_t *) & /*_.BIND__V13*/ meltfptr[12];
	      /*_.MELT_DEBUG_FUN__V36*/ meltfptr[35] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V35*/ meltfptr[34] =
	      /*_.MELT_DEBUG_FUN__V36*/ meltfptr[35];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-normal.melt:1564:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L11*/ meltfnum[10] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V36*/ meltfptr[35] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V35*/ meltfptr[34] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-normal.melt:1564:/ quasiblock");


      /*_.PROGN___V37*/ meltfptr[35] = /*_.IF___V35*/ meltfptr[34];;
      /*^compute */
      /*_.IFCPP___V34*/ meltfptr[32] = /*_.PROGN___V37*/ meltfptr[35];;
      /*epilog */

      MELT_LOCATION ("warmelt-normal.melt:1564:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L10*/ meltfnum[8] = 0;
      /*^clear */
	     /*clear *//*_.IF___V35*/ meltfptr[34] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V37*/ meltfptr[35] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V34*/ meltfptr[32] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-normal.melt:1567:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^cond */
    /*cond */ if ( /*_.SYCA__V31*/ meltfptr[27])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-normal.melt:1568:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^quasiblock */


	  /*_.RETVAL___V1*/ meltfptr[0] = /*_.SYCA__V31*/ meltfptr[27];;
	  MELT_LOCATION ("warmelt-normal.melt:1568:/ putxtraresult");
	  if (!meltxrestab_ || !meltxresdescr_)
	    goto labend_rout;
	  if (meltxresdescr_[0] != MELTBPAR_PTR)
	    goto labend_rout;
	  if (meltxrestab_[0].meltbp_aptr)
	    *(meltxrestab_[0].meltbp_aptr) = (melt_ptr_t) (( /*nil */ NULL));
	  ;
	  /*^finalreturn */
	  ;
	  /*finalret */ goto labend_rout;
	  /*_.IFELSE___V38*/ meltfptr[34] = /*_.RETURN___V39*/ meltfptr[35];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-normal.melt:1567:/ clear");
	     /*clear *//*_.RETURN___V39*/ meltfptr[35] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-normal.melt:1571:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
   /*_#IS_A__L12*/ meltfnum[10] =
	    melt_is_instance_of ((melt_ptr_t) ( /*_.BIND__V13*/ meltfptr[12]),
				 (melt_ptr_t) (( /*!CLASS_VALUE_BINDING */
						meltfrout->tabval[4])));;
	  MELT_LOCATION ("warmelt-normal.melt:1571:/ cond");
	  /*cond */ if ( /*_#IS_A__L12*/ meltfnum[10])	/*then */
	    {
	      /*^cond.then */
	      /*^block */
	      /*anyblock */
	      {

		MELT_LOCATION ("warmelt-normal.melt:1572:/ quasiblock");


     /*_.BVAR__V42*/ meltfptr[41] =
		  /*mapobject_get */
		  melt_get_mapobjects ((meltmapobjects_ptr_t)
				       ( /*_.VALBINDMAP__V23*/ meltfptr[22]),
				       (meltobject_ptr_t) ( /*_.BIND__V13*/
							   meltfptr[12]));;
		MELT_LOCATION ("warmelt-normal.melt:1573:/ cond");
		/*cond */ if (
			       /*ifisa */
			       melt_is_instance_of ((melt_ptr_t)
						    ( /*_.BIND__V13*/
						     meltfptr[12]),
						    (melt_ptr_t) (( /*!CLASS_VALUE_BINDING */ meltfrout->tabval[4])))
		  )		/*then */
		  {
		    /*^cond.then */
		    /*^getslot */
		    {
		      melt_ptr_t slot = NULL, obj = NULL;
		      obj =
			(melt_ptr_t) ( /*_.BIND__V13*/ meltfptr[12]) /*=obj*/
			;
		      melt_object_get_field (slot, obj, 1, "VBIND_VALUE");
       /*_.VAL__V43*/ meltfptr[42] = slot;
		    };
		    ;
		  }
		else
		  {		/*^cond.else */

      /*_.VAL__V43*/ meltfptr[42] = NULL;;
		  }
		;

#if MELT_HAVE_DEBUG
		MELT_LOCATION ("warmelt-normal.melt:1575:/ cppif.then");
		/*^block */
		/*anyblock */
		{


		  {
		    /*^locexp */
		    /*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
		    melt_dbgcounter++;
#endif
		    ;
		  }
		  ;
		  /*^checksignal */
		  MELT_CHECK_SIGNAL ();
		  ;
       /*_#MELT_NEED_DBG__L13*/ meltfnum[8] =
		    /*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
		    ( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
		    0		/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
		    ;;
		  MELT_LOCATION ("warmelt-normal.melt:1575:/ cond");
		  /*cond */ if ( /*_#MELT_NEED_DBG__L13*/ meltfnum[8])	/*then */
		    {
		      /*^cond.then */
		      /*^block */
		      /*anyblock */
		      {

	 /*_#THE_MELTCALLCOUNT__L14*/ meltfnum[13] =
#ifdef meltcallcount
			  meltcallcount	/* the_meltcallcount */
#else
			  0L
#endif /* meltcallcount the_meltcallcount */
			  ;;
			MELT_LOCATION
			  ("warmelt-normal.melt:1575:/ checksignal");
			MELT_CHECK_SIGNAL ();
			;
			/*^apply */
			/*apply */
			{
			  union meltparam_un argtab[7];
			  memset (&argtab, 0, sizeof (argtab));
			  /*^apply.arg */
			  argtab[0].meltbp_long =
			    /*_#THE_MELTCALLCOUNT__L14*/ meltfnum[13];
			  /*^apply.arg */
			  argtab[1].meltbp_cstring = "warmelt-normal.melt";
			  /*^apply.arg */
			  argtab[2].meltbp_long = 1575;
			  /*^apply.arg */
			  argtab[3].meltbp_cstring =
			    "normexp_symbol value bind=";
			  /*^apply.arg */
			  argtab[4].meltbp_aptr =
			    (melt_ptr_t *) & /*_.BIND__V13*/ meltfptr[12];
			  /*^apply.arg */
			  argtab[5].meltbp_cstring = " procs=";
			  /*^apply.arg */
			  argtab[6].meltbp_aptr =
			    (melt_ptr_t *) & /*_.PROCS__V14*/ meltfptr[13];
			  /*_.MELT_DEBUG_FUN__V46*/ meltfptr[45] =
			    melt_apply ((meltclosure_ptr_t)
					(( /*!MELT_DEBUG_FUN */ meltfrout->
					  tabval[0])),
					(melt_ptr_t) (( /*nil */ NULL)),
					(MELTBPARSTR_LONG MELTBPARSTR_CSTRING
					 MELTBPARSTR_LONG MELTBPARSTR_CSTRING
					 MELTBPARSTR_PTR MELTBPARSTR_CSTRING
					 MELTBPARSTR_PTR ""), argtab, "",
					(union meltparam_un *) 0);
			}
			;
			/*_.IF___V45*/ meltfptr[44] =
			  /*_.MELT_DEBUG_FUN__V46*/ meltfptr[45];;
			/*epilog */

			MELT_LOCATION ("warmelt-normal.melt:1575:/ clear");
		   /*clear *//*_#THE_MELTCALLCOUNT__L14*/ meltfnum[13] =
			  0;
			/*^clear */
		   /*clear *//*_.MELT_DEBUG_FUN__V46*/ meltfptr[45] = 0;
		      }
		      ;
		    }
		  else
		    {		/*^cond.else */

	/*_.IF___V45*/ meltfptr[44] = NULL;;
		    }
		  ;
		  MELT_LOCATION ("warmelt-normal.melt:1575:/ quasiblock");


		  /*_.PROGN___V47*/ meltfptr[45] =
		    /*_.IF___V45*/ meltfptr[44];;
		  /*^compute */
		  /*_.IFCPP___V44*/ meltfptr[43] =
		    /*_.PROGN___V47*/ meltfptr[45];;
		  /*epilog */

		  MELT_LOCATION ("warmelt-normal.melt:1575:/ clear");
		 /*clear *//*_#MELT_NEED_DBG__L13*/ meltfnum[8] = 0;
		  /*^clear */
		 /*clear *//*_.IF___V45*/ meltfptr[44] = 0;
		  /*^clear */
		 /*clear *//*_.PROGN___V47*/ meltfptr[45] = 0;
		}

#else /*MELT_HAVE_DEBUG */
		/*^cppif.else */
		/*_.IFCPP___V44*/ meltfptr[43] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
		;
		MELT_LOCATION ("warmelt-normal.melt:1576:/ checksignal");
		MELT_CHECK_SIGNAL ();
		;
     /*_#NULL__L15*/ meltfnum[13] =
		  (( /*_.BVAR__V42*/ meltfptr[41]) == NULL);;
		MELT_LOCATION ("warmelt-normal.melt:1576:/ cond");
		/*cond */ if ( /*_#NULL__L15*/ meltfnum[13])	/*then */
		  {
		    /*^cond.then */
		    /*^block */
		    /*anyblock */
		    {

		      MELT_LOCATION ("warmelt-normal.melt:1577:/ quasiblock");


		      MELT_LOCATION
			("warmelt-normal.melt:1578:/ checksignal");
		      MELT_CHECK_SIGNAL ();
		      ;
       /*_#IS_A__L16*/ meltfnum[8] =
			melt_is_instance_of ((melt_ptr_t)
					     ( /*_.MODCTX__V22*/
					      meltfptr[21]),
					     (melt_ptr_t) (( /*!CLASS_RUNNING_EXTENSION_MODULE_CONTEXT */ meltfrout->tabval[5])));;
		      MELT_LOCATION ("warmelt-normal.melt:1578:/ cond");
		      /*cond */ if ( /*_#IS_A__L16*/ meltfnum[8])	/*then */
			{
			  /*^cond.then */
			  /*^block */
			  /*anyblock */
			  {

			    MELT_LOCATION
			      ("warmelt-normal.melt:1579:/ quasiblock");


			    MELT_LOCATION
			      ("warmelt-normal.melt:1580:/ checksignal");
			    MELT_CHECK_SIGNAL ();
			    ;
			    /*^apply */
			    /*apply */
			    {
			      union meltparam_un argtab[1];
			      memset (&argtab, 0, sizeof (argtab));
			      /*^apply.arg */
			      argtab[0].meltbp_aptr =
				(melt_ptr_t *) & /*_.MODCTX__V22*/
				meltfptr[21];
			      /*_.REGLITVAL__V50*/ meltfptr[49] =
				melt_apply ((meltclosure_ptr_t)
					    (( /*!REGISTER_LITERAL_VALUE */
					      meltfrout->tabval[6])),
					    (melt_ptr_t) ( /*_.VAL__V43*/
							  meltfptr[42]),
					    (MELTBPARSTR_PTR ""), argtab, "",
					    (union meltparam_un *) 0);
			    }
			    ;
			    MELT_LOCATION
			      ("warmelt-normal.melt:1581:/ checksignal");
			    MELT_CHECK_SIGNAL ();
			    ;
			    /*^quasiblock */


			    /*^rawallocobj */
			    /*rawallocobj */
			    {
			      melt_ptr_t newobj = 0;
			      melt_raw_object_create (newobj,
						      (melt_ptr_t) (( /*!CLASS_NREP_LITERALNAMEDVALUE */ meltfrout->tabval[7])), (3), "CLASS_NREP_LITERALNAMEDVALUE");
	  /*_.INST__V52*/ meltfptr[51] =
				newobj;
			    };
			    ;
			    /*^putslot */
			    /*putslot */
			    melt_assertmsg
			      ("putslot checkobj @NLITVAL_REGVAL",
			       melt_magic_discr ((melt_ptr_t)
						 ( /*_.INST__V52*/
						  meltfptr[51])) ==
			       MELTOBMAG_OBJECT);
			    melt_putfield_object (( /*_.INST__V52*/
						   meltfptr[51]), (1),
						  ( /*_.REGLITVAL__V50*/
						   meltfptr[49]),
						  "NLITVAL_REGVAL");
			    ;
			    /*^putslot */
			    /*putslot */
			    melt_assertmsg
			      ("putslot checkobj @NLITVAL_SYMBOL",
			       melt_magic_discr ((melt_ptr_t)
						 ( /*_.INST__V52*/
						  meltfptr[51])) ==
			       MELTOBMAG_OBJECT);
			    melt_putfield_object (( /*_.INST__V52*/
						   meltfptr[51]), (2),
						  ( /*_.RECV__V2*/
						   meltfptr[1]),
						  "NLITVAL_SYMBOL");
			    ;
			    /*^touchobj */

			    melt_dbgtrace_written_object ( /*_.INST__V52*/
							  meltfptr[51],
							  "newly made instance");
			    ;
			    /*_.NLITVAL__V51*/ meltfptr[50] =
			      /*_.INST__V52*/ meltfptr[51];;

#if MELT_HAVE_DEBUG
			    MELT_LOCATION
			      ("warmelt-normal.melt:1585:/ cppif.then");
			    /*^block */
			    /*anyblock */
			    {


			      {
				/*^locexp */
				/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
				melt_dbgcounter++;
#endif
				;
			      }
			      ;
			      /*^checksignal */
			      MELT_CHECK_SIGNAL ();
			      ;
	   /*_#MELT_NEED_DBG__L17*/ meltfnum[16] =
				/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
				( /*melt_need_dbg */
				 melt_need_debug ((int) 0))
#else
				0	/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
				;;
			      MELT_LOCATION
				("warmelt-normal.melt:1585:/ cond");
			      /*cond */ if ( /*_#MELT_NEED_DBG__L17*/ meltfnum[16])	/*then */
				{
				  /*^cond.then */
				  /*^block */
				  /*anyblock */
				  {

	     /*_#THE_MELTCALLCOUNT__L18*/ meltfnum[17]
				      =
#ifdef meltcallcount
				      meltcallcount	/* the_meltcallcount */
#else
				      0L
#endif /* meltcallcount the_meltcallcount */
				      ;;
				    MELT_LOCATION
				      ("warmelt-normal.melt:1585:/ checksignal");
				    MELT_CHECK_SIGNAL ();
				    ;
				    /*^apply */
				    /*apply */
				    {
				      union meltparam_un argtab[5];
				      memset (&argtab, 0, sizeof (argtab));
				      /*^apply.arg */
				      argtab[0].meltbp_long =
					/*_#THE_MELTCALLCOUNT__L18*/
					meltfnum[17];
				      /*^apply.arg */
				      argtab[1].meltbp_cstring =
					"warmelt-normal.melt";
				      /*^apply.arg */
				      argtab[2].meltbp_long = 1585;
				      /*^apply.arg */
				      argtab[3].meltbp_cstring =
					"normexp_symbol nlitval=";
				      /*^apply.arg */
				      argtab[4].meltbp_aptr =
					(melt_ptr_t *) & /*_.NLITVAL__V51*/
					meltfptr[50];
				      /*_.MELT_DEBUG_FUN__V55*/ meltfptr[54] =
					melt_apply ((meltclosure_ptr_t)
						    (( /*!MELT_DEBUG_FUN */
						      meltfrout->tabval[0])),
						    (melt_ptr_t) (( /*nil */
								   NULL)),
						    (MELTBPARSTR_LONG
						     MELTBPARSTR_CSTRING
						     MELTBPARSTR_LONG
						     MELTBPARSTR_CSTRING
						     MELTBPARSTR_PTR ""),
						    argtab, "",
						    (union meltparam_un *) 0);
				    }
				    ;
				    /*_.IF___V54*/ meltfptr[53] =
				      /*_.MELT_DEBUG_FUN__V55*/ meltfptr[54];;
				    /*epilog */

				    MELT_LOCATION
				      ("warmelt-normal.melt:1585:/ clear");
		       /*clear *//*_#THE_MELTCALLCOUNT__L18*/
				      meltfnum[17] = 0;
				    /*^clear */
		       /*clear *//*_.MELT_DEBUG_FUN__V55*/
				      meltfptr[54] = 0;
				  }
				  ;
				}
			      else
				{	/*^cond.else */

	    /*_.IF___V54*/ meltfptr[53] = NULL;;
				}
			      ;
			      MELT_LOCATION
				("warmelt-normal.melt:1585:/ quasiblock");


			      /*_.PROGN___V56*/ meltfptr[54] =
				/*_.IF___V54*/ meltfptr[53];;
			      /*^compute */
			      /*_.IFCPP___V53*/ meltfptr[52] =
				/*_.PROGN___V56*/ meltfptr[54];;
			      /*epilog */

			      MELT_LOCATION
				("warmelt-normal.melt:1585:/ clear");
		     /*clear *//*_#MELT_NEED_DBG__L17*/ meltfnum[16]
				= 0;
			      /*^clear */
		     /*clear *//*_.IF___V54*/ meltfptr[53] = 0;
			      /*^clear */
		     /*clear *//*_.PROGN___V56*/ meltfptr[54] = 0;
			    }

#else /*MELT_HAVE_DEBUG */
			    /*^cppif.else */
			    /*_.IFCPP___V53*/ meltfptr[52] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
			    ;
			    /*^compute */
			    /*_.LET___V49*/ meltfptr[45] =
			      /*_.NLITVAL__V51*/ meltfptr[50];;

			    MELT_LOCATION
			      ("warmelt-normal.melt:1579:/ clear");
		   /*clear *//*_.REGLITVAL__V50*/ meltfptr[49] = 0;
			    /*^clear */
		   /*clear *//*_.NLITVAL__V51*/ meltfptr[50] = 0;
			    /*^clear */
		   /*clear *//*_.IFCPP___V53*/ meltfptr[52] = 0;
			    /*_.NEWBVAR__V48*/ meltfptr[44] =
			      /*_.LET___V49*/ meltfptr[45];;
			    /*epilog */

			    MELT_LOCATION
			      ("warmelt-normal.melt:1578:/ clear");
		   /*clear *//*_.LET___V49*/ meltfptr[45] = 0;
			  }
			  ;
			}
		      else
			{	/*^cond.else */

			  /*^block */
			  /*anyblock */
			  {

			    MELT_LOCATION
			      ("warmelt-normal.melt:1590:/ checksignal");
			    MELT_CHECK_SIGNAL ();
			    ;
			    MELT_LOCATION
			      ("warmelt-normal.melt:1593:/ checksignal");
			    MELT_CHECK_SIGNAL ();
			    ;
			    /*^apply */
			    /*apply */
			    {
			      union meltparam_un argtab[2];
			      memset (&argtab, 0, sizeof (argtab));
			      /*^apply.arg */
			      argtab[0].meltbp_aptr =
				(melt_ptr_t *) & /*_.NCX__V4*/ meltfptr[3];
			      /*^apply.arg */
			      argtab[1].meltbp_aptr =
				(melt_ptr_t *) & /*_.PSLOC__V5*/ meltfptr[4];
			      /*_.NORMAL_SYMBOL_DATA__V57*/ meltfptr[53] =
				melt_apply ((meltclosure_ptr_t)
					    (( /*!NORMAL_SYMBOL_DATA */
					      meltfrout->tabval[9])),
					    (melt_ptr_t) ( /*_.RECV__V2*/
							  meltfptr[1]),
					    (MELTBPARSTR_PTR MELTBPARSTR_PTR
					     ""), argtab, "",
					    (union meltparam_un *) 0);
			    }
			    ;
			    MELT_LOCATION
			      ("warmelt-normal.melt:1590:/ quasiblock");


			    /*^rawallocobj */
			    /*rawallocobj */
			    {
			      melt_ptr_t newobj = 0;
			      melt_raw_object_create (newobj,
						      (melt_ptr_t) (( /*!CLASS_NREP_IMPORTEDVAL */ meltfrout->tabval[8])), (3), "CLASS_NREP_IMPORTEDVAL");
	  /*_.INST__V59*/ meltfptr[49] =
				newobj;
			    };
			    ;
			    /*^putslot */
			    /*putslot */
			    melt_assertmsg ("putslot checkobj @NREP_LOC",
					    melt_magic_discr ((melt_ptr_t)
							      ( /*_.INST__V59*/ meltfptr[49])) == MELTOBMAG_OBJECT);
			    melt_putfield_object (( /*_.INST__V59*/
						   meltfptr[49]), (0),
						  ( /*_.PSLOC__V5*/
						   meltfptr[4]), "NREP_LOC");
			    ;
			    /*^putslot */
			    /*putslot */
			    melt_assertmsg ("putslot checkobj @NIMPORT_SYMB",
					    melt_magic_discr ((melt_ptr_t)
							      ( /*_.INST__V59*/ meltfptr[49])) == MELTOBMAG_OBJECT);
			    melt_putfield_object (( /*_.INST__V59*/
						   meltfptr[49]), (1),
						  ( /*_.RECV__V2*/
						   meltfptr[1]),
						  "NIMPORT_SYMB");
			    ;
			    /*^putslot */
			    /*putslot */
			    melt_assertmsg
			      ("putslot checkobj @NIMPORT_SYDATA",
			       melt_magic_discr ((melt_ptr_t)
						 ( /*_.INST__V59*/
						  meltfptr[49])) ==
			       MELTOBMAG_OBJECT);
			    melt_putfield_object (( /*_.INST__V59*/
						   meltfptr[49]), (2),
						  ( /*_.NORMAL_SYMBOL_DATA__V57*/ meltfptr[53]), "NIMPORT_SYDATA");
			    ;
			    /*^touchobj */

			    melt_dbgtrace_written_object ( /*_.INST__V59*/
							  meltfptr[49],
							  "newly made instance");
			    ;
			    /*_.INST___V58*/ meltfptr[54] =
			      /*_.INST__V59*/ meltfptr[49];;
			    MELT_LOCATION
			      ("warmelt-normal.melt:1589:/ quasiblock");


			    /*_.PROGN___V60*/ meltfptr[50] =
			      /*_.INST___V58*/ meltfptr[54];;
			    /*^compute */
			    /*_.NEWBVAR__V48*/ meltfptr[44] =
			      /*_.PROGN___V60*/ meltfptr[50];;
			    /*epilog */

			    MELT_LOCATION
			      ("warmelt-normal.melt:1578:/ clear");
		   /*clear *//*_.NORMAL_SYMBOL_DATA__V57*/
			      meltfptr[53] = 0;
			    /*^clear */
		   /*clear *//*_.INST___V58*/ meltfptr[54] = 0;
			    /*^clear */
		   /*clear *//*_.PROGN___V60*/ meltfptr[50] = 0;
			  }
			  ;
			}
		      ;

		      {
			MELT_LOCATION ("warmelt-normal.melt:1595:/ locexp");
			meltgc_put_mapobjects ((meltmapobjects_ptr_t)
					       ( /*_.VALBINDMAP__V23*/
						meltfptr[22]),
					       (meltobject_ptr_t) ( /*_.BIND__V13*/ meltfptr[12]),
					       (melt_ptr_t) ( /*_.NEWBVAR__V48*/ meltfptr[44]));
		      }
		      ;
		      MELT_LOCATION ("warmelt-normal.melt:1596:/ compute");
		      /*_.BVAR__V42*/ meltfptr[41] =
			/*_.SETQ___V61*/ meltfptr[52] =
			/*_.NEWBVAR__V48*/ meltfptr[44];;

		      {
			MELT_LOCATION ("warmelt-normal.melt:1597:/ locexp");
			meltgc_append_list ((melt_ptr_t)
					    ( /*_.VALUELIST__V24*/
					     meltfptr[23]),
					    (melt_ptr_t) ( /*_.NEWBVAR__V48*/
							  meltfptr[44]));
		      }
		      ;

		      MELT_LOCATION ("warmelt-normal.melt:1577:/ clear");
		 /*clear *//*_#IS_A__L16*/ meltfnum[8] = 0;
		      /*^clear */
		 /*clear *//*_.NEWBVAR__V48*/ meltfptr[44] = 0;
		      /*^clear */
		 /*clear *//*_.SETQ___V61*/ meltfptr[52] = 0;
		      /*epilog */
		    }
		    ;
		  }		/*noelse */
		;
		MELT_LOCATION ("warmelt-normal.melt:1600:/ checksignal");
		MELT_CHECK_SIGNAL ();
		;
		/*^checksignal */
		MELT_CHECK_SIGNAL ();
		;
     /*_#IS_LIST__L19*/ meltfnum[17] =
		  (melt_magic_discr
		   ((melt_ptr_t) ( /*_.PROCS__V14*/ meltfptr[13])) ==
		   MELTOBMAG_LIST);;
		MELT_LOCATION ("warmelt-normal.melt:1600:/ cond");
		/*cond */ if ( /*_#IS_LIST__L19*/ meltfnum[17])	/*then */
		  {
		    /*^cond.then */
		    /*^block */
		    /*anyblock */
		    {

       /*_.LIST_FIRST__V62*/ meltfptr[45] =
			(melt_list_first
			 ((melt_ptr_t) ( /*_.PROCS__V14*/ meltfptr[13])));;
		      /*^compute */
       /*_#IS_PAIR__L21*/ meltfnum[8] =
			(melt_magic_discr
			 ((melt_ptr_t) ( /*_.LIST_FIRST__V62*/ meltfptr[45]))
			 == MELTOBMAG_PAIR);;
		      /*^compute */
		      /*_#IF___L20*/ meltfnum[16] =
			/*_#IS_PAIR__L21*/ meltfnum[8];;
		      /*epilog */

		      MELT_LOCATION ("warmelt-normal.melt:1600:/ clear");
		 /*clear *//*_.LIST_FIRST__V62*/ meltfptr[45] = 0;
		      /*^clear */
		 /*clear *//*_#IS_PAIR__L21*/ meltfnum[8] = 0;
		    }
		    ;
		  }
		else
		  {		/*^cond.else */

      /*_#IF___L20*/ meltfnum[16] = 0;;
		  }
		;
		MELT_LOCATION ("warmelt-normal.melt:1600:/ cond");
		/*cond */ if ( /*_#IF___L20*/ meltfnum[16])	/*then */
		  {
		    /*^cond.then */
		    /*^block */
		    /*anyblock */
		    {

		      MELT_LOCATION ("warmelt-normal.melt:1602:/ quasiblock");


		      MELT_LOCATION
			("warmelt-normal.melt:1603:/ checksignal");
		      MELT_CHECK_SIGNAL ();
		      ;
		      /*^quasiblock */


		      /*^rawallocobj */
		      /*rawallocobj */
		      {
			melt_ptr_t newobj = 0;
			melt_raw_object_create (newobj,
						(melt_ptr_t) (( /*!CLASS_NREP_CONSTOCC */ meltfrout->tabval[10])), (5), "CLASS_NREP_CONSTOCC");
	/*_.INST__V66*/ meltfptr[44] =
			  newobj;
		      };
		      ;
		      /*^putslot */
		      /*putslot */
		      melt_assertmsg ("putslot checkobj @NREP_LOC",
				      melt_magic_discr ((melt_ptr_t)
							( /*_.INST__V66*/
							 meltfptr[44])) ==
				      MELTOBMAG_OBJECT);
		      melt_putfield_object (( /*_.INST__V66*/ meltfptr[44]),
					    (0),
					    ( /*_.PSLOC__V5*/ meltfptr[4]),
					    "NREP_LOC");
		      ;
		      /*^putslot */
		      /*putslot */
		      melt_assertmsg ("putslot checkobj @NOCC_SYMB",
				      melt_magic_discr ((melt_ptr_t)
							( /*_.INST__V66*/
							 meltfptr[44])) ==
				      MELTOBMAG_OBJECT);
		      melt_putfield_object (( /*_.INST__V66*/ meltfptr[44]),
					    (1),
					    ( /*_.RECV__V2*/ meltfptr[1]),
					    "NOCC_SYMB");
		      ;
		      /*^putslot */
		      /*putslot */
		      melt_assertmsg ("putslot checkobj @NOCC_BIND",
				      melt_magic_discr ((melt_ptr_t)
							( /*_.INST__V66*/
							 meltfptr[44])) ==
				      MELTOBMAG_OBJECT);
		      melt_putfield_object (( /*_.INST__V66*/ meltfptr[44]),
					    (3),
					    ( /*_.BIND__V13*/ meltfptr[12]),
					    "NOCC_BIND");
		      ;
		      /*^putslot */
		      /*putslot */
		      melt_assertmsg ("putslot checkobj @NOCC_CTYP",
				      melt_magic_discr ((melt_ptr_t)
							( /*_.INST__V66*/
							 meltfptr[44])) ==
				      MELTOBMAG_OBJECT);
		      melt_putfield_object (( /*_.INST__V66*/ meltfptr[44]),
					    (2),
					    (( /*!CTYPE_VALUE */ meltfrout->
					      tabval[11])), "NOCC_CTYP");
		      ;
		      /*^putslot */
		      /*putslot */
		      melt_assertmsg ("putslot checkobj @NCLOC_PROCS",
				      melt_magic_discr ((melt_ptr_t)
							( /*_.INST__V66*/
							 meltfptr[44])) ==
				      MELTOBMAG_OBJECT);
		      melt_putfield_object (( /*_.INST__V66*/ meltfptr[44]),
					    (4),
					    ( /*_.PROCS__V14*/ meltfptr[13]),
					    "NCLOC_PROCS");
		      ;
		      /*^touchobj */

		      melt_dbgtrace_written_object ( /*_.INST__V66*/
						    meltfptr[44],
						    "newly made instance");
		      ;
		      /*_.FXOCC__V65*/ meltfptr[50] =
			/*_.INST__V66*/ meltfptr[44];;

		      {
			MELT_LOCATION ("warmelt-normal.melt:1611:/ locexp");
			meltgc_put_mapobjects ((meltmapobjects_ptr_t)
					       ( /*_.SYCMAP__V30*/
						meltfptr[26]),
					       (meltobject_ptr_t) ( /*_.RECV__V2*/ meltfptr[1]),
					       (melt_ptr_t) ( /*_.FXOCC__V65*/
							     meltfptr[50]));
		      }
		      ;

#if MELT_HAVE_DEBUG
		      MELT_LOCATION ("warmelt-normal.melt:1612:/ cppif.then");
		      /*^block */
		      /*anyblock */
		      {


			{
			  /*^locexp */
			  /*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
			  melt_dbgcounter++;
#endif
			  ;
			}
			;
			/*^checksignal */
			MELT_CHECK_SIGNAL ();
			;
	 /*_#MELT_NEED_DBG__L22*/ meltfnum[8] =
			  /*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
			  ( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
			  0	/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
			  ;;
			MELT_LOCATION ("warmelt-normal.melt:1612:/ cond");
			/*cond */ if ( /*_#MELT_NEED_DBG__L22*/ meltfnum[8])	/*then */
			  {
			    /*^cond.then */
			    /*^block */
			    /*anyblock */
			    {

	   /*_#THE_MELTCALLCOUNT__L23*/ meltfnum[22] =
#ifdef meltcallcount
				meltcallcount	/* the_meltcallcount */
#else
				0L
#endif /* meltcallcount the_meltcallcount */
				;;
			      MELT_LOCATION
				("warmelt-normal.melt:1612:/ checksignal");
			      MELT_CHECK_SIGNAL ();
			      ;
			      /*^apply */
			      /*apply */
			      {
				union meltparam_un argtab[7];
				memset (&argtab, 0, sizeof (argtab));
				/*^apply.arg */
				argtab[0].meltbp_long =
				  /*_#THE_MELTCALLCOUNT__L23*/ meltfnum[22];
				/*^apply.arg */
				argtab[1].meltbp_cstring =
				  "warmelt-normal.melt";
				/*^apply.arg */
				argtab[2].meltbp_long = 1612;
				/*^apply.arg */
				argtab[3].meltbp_cstring =
				  "normexp_symbol const value fxocc";
				/*^apply.arg */
				argtab[4].meltbp_aptr =
				  (melt_ptr_t *) & /*_.FXOCC__V65*/
				  meltfptr[50];
				/*^apply.arg */
				argtab[5].meltbp_cstring = " updated sycmap=";
				/*^apply.arg */
				argtab[6].meltbp_aptr =
				  (melt_ptr_t *) & /*_.SYCMAP__V30*/
				  meltfptr[26];
				/*_.MELT_DEBUG_FUN__V69*/ meltfptr[68] =
				  melt_apply ((meltclosure_ptr_t)
					      (( /*!MELT_DEBUG_FUN */
						meltfrout->tabval[0])),
					      (melt_ptr_t) (( /*nil */ NULL)),
					      (MELTBPARSTR_LONG
					       MELTBPARSTR_CSTRING
					       MELTBPARSTR_LONG
					       MELTBPARSTR_CSTRING
					       MELTBPARSTR_PTR
					       MELTBPARSTR_CSTRING
					       MELTBPARSTR_PTR ""), argtab,
					      "", (union meltparam_un *) 0);
			      }
			      ;
			      /*_.IF___V68*/ meltfptr[45] =
				/*_.MELT_DEBUG_FUN__V69*/ meltfptr[68];;
			      /*epilog */

			      MELT_LOCATION
				("warmelt-normal.melt:1612:/ clear");
		     /*clear *//*_#THE_MELTCALLCOUNT__L23*/
				meltfnum[22] = 0;
			      /*^clear */
		     /*clear *//*_.MELT_DEBUG_FUN__V69*/ meltfptr[68]
				= 0;
			    }
			    ;
			  }
			else
			  {	/*^cond.else */

	  /*_.IF___V68*/ meltfptr[45] = NULL;;
			  }
			;
			MELT_LOCATION
			  ("warmelt-normal.melt:1612:/ quasiblock");


			/*_.PROGN___V70*/ meltfptr[68] =
			  /*_.IF___V68*/ meltfptr[45];;
			/*^compute */
			/*_.IFCPP___V67*/ meltfptr[52] =
			  /*_.PROGN___V70*/ meltfptr[68];;
			/*epilog */

			MELT_LOCATION ("warmelt-normal.melt:1612:/ clear");
		   /*clear *//*_#MELT_NEED_DBG__L22*/ meltfnum[8] = 0;
			/*^clear */
		   /*clear *//*_.IF___V68*/ meltfptr[45] = 0;
			/*^clear */
		   /*clear *//*_.PROGN___V70*/ meltfptr[68] = 0;
		      }

#else /*MELT_HAVE_DEBUG */
		      /*^cppif.else */
		      /*_.IFCPP___V67*/ meltfptr[52] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
		      ;
		      MELT_LOCATION ("warmelt-normal.melt:1616:/ quasiblock");


		      /*^newclosure */
		       /*newclosure *//*_.LAMBDA___V72*/ meltfptr[68] =
			(melt_ptr_t)
			meltgc_new_closure ((meltobject_ptr_t)
					    (((melt_ptr_t)
					      (MELT_PREDEF (DISCR_CLOSURE)))),
					    (meltroutine_ptr_t) (( /*!konst_17 */ meltfrout->tabval[17])), (1));
		      ;
		      /*^putclosedv */
		      /*putclosv */
		      melt_assertmsg ("putclosv checkclo",
				      melt_magic_discr ((melt_ptr_t)
							( /*_.LAMBDA___V72*/
							 meltfptr[68])) ==
				      MELTOBMAG_CLOSURE);
		      melt_assertmsg ("putclosv checkoff", 0 >= 0
				      && 0 <
				      melt_closure_size ((melt_ptr_t)
							 ( /*_.LAMBDA___V72*/
							  meltfptr[68])));
		      ((meltclosure_ptr_t) /*_.LAMBDA___V72*/ meltfptr[68])->
			tabval[0] =
			(melt_ptr_t) ( /*_.FXOCC__V65*/ meltfptr[50]);
		      ;
		      /*_.LAMBDA___V71*/ meltfptr[45] =
			/*_.LAMBDA___V72*/ meltfptr[68];;
		      MELT_LOCATION
			("warmelt-normal.melt:1614:/ checksignal");
		      MELT_CHECK_SIGNAL ();
		      ;
		      /*^apply */
		      /*apply */
		      {
			union meltparam_un argtab[1];
			memset (&argtab, 0, sizeof (argtab));
			/*^apply.arg */
			argtab[0].meltbp_aptr =
			  (melt_ptr_t *) & /*_.LAMBDA___V71*/ meltfptr[45];
			/*_.LIST_EVERY__V73*/ meltfptr[72] =
			  melt_apply ((meltclosure_ptr_t)
				      (( /*!LIST_EVERY */ meltfrout->
					tabval[12])),
				      (melt_ptr_t) ( /*_.PROCS__V14*/
						    meltfptr[13]),
				      (MELTBPARSTR_PTR ""), argtab, "",
				      (union meltparam_un *) 0);
		      }
		      ;
		      MELT_LOCATION
			("warmelt-normal.melt:1628:/ checksignal");
		      MELT_CHECK_SIGNAL ();
		      ;
		      /*^quasiblock */


		      /*_.RETVAL___V1*/ meltfptr[0] =
			/*_.FXOCC__V65*/ meltfptr[50];;
		      MELT_LOCATION
			("warmelt-normal.melt:1628:/ putxtraresult");
		      if (!meltxrestab_ || !meltxresdescr_)
			goto labend_rout;
		      if (meltxresdescr_[0] != MELTBPAR_PTR)
			goto labend_rout;
		      if (meltxrestab_[0].meltbp_aptr)
			*(meltxrestab_[0].meltbp_aptr) =
			  (melt_ptr_t) (( /*nil */ NULL));
		      ;
		      /*^finalreturn */
		      ;
		      /*finalret */ goto labend_rout;
		      /*_.LET___V64*/ meltfptr[54] =
			/*_.RETURN___V74*/ meltfptr[73];;

		      MELT_LOCATION ("warmelt-normal.melt:1602:/ clear");
		 /*clear *//*_.FXOCC__V65*/ meltfptr[50] = 0;
		      /*^clear */
		 /*clear *//*_.IFCPP___V67*/ meltfptr[52] = 0;
		      /*^clear */
		 /*clear *//*_.LAMBDA___V71*/ meltfptr[45] = 0;
		      /*^clear */
		 /*clear *//*_.LIST_EVERY__V73*/ meltfptr[72] = 0;
		      /*^clear */
		 /*clear *//*_.RETURN___V74*/ meltfptr[73] = 0;
		      /*_.IFELSE___V63*/ meltfptr[53] =
			/*_.LET___V64*/ meltfptr[54];;
		      /*epilog */

		      MELT_LOCATION ("warmelt-normal.melt:1600:/ clear");
		 /*clear *//*_.LET___V64*/ meltfptr[54] = 0;
		    }
		    ;
		  }
		else
		  {		/*^cond.else */

		    /*^block */
		    /*anyblock */
		    {


		      {
			MELT_LOCATION ("warmelt-normal.melt:1633:/ locexp");
			meltgc_put_mapobjects ((meltmapobjects_ptr_t)
					       ( /*_.SYCMAP__V30*/
						meltfptr[26]),
					       (meltobject_ptr_t) ( /*_.RECV__V2*/ meltfptr[1]),
					       (melt_ptr_t) ( /*_.BVAR__V42*/
							     meltfptr[41]));
		      }
		      ;

#if MELT_HAVE_DEBUG
		      MELT_LOCATION ("warmelt-normal.melt:1634:/ cppif.then");
		      /*^block */
		      /*anyblock */
		      {


			{
			  /*^locexp */
			  /*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
			  melt_dbgcounter++;
#endif
			  ;
			}
			;
			/*^checksignal */
			MELT_CHECK_SIGNAL ();
			;
	 /*_#MELT_NEED_DBG__L24*/ meltfnum[22] =
			  /*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
			  ( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
			  0	/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
			  ;;
			MELT_LOCATION ("warmelt-normal.melt:1634:/ cond");
			/*cond */ if ( /*_#MELT_NEED_DBG__L24*/ meltfnum[22])	/*then */
			  {
			    /*^cond.then */
			    /*^block */
			    /*anyblock */
			    {

	   /*_#THE_MELTCALLCOUNT__L25*/ meltfnum[8] =
#ifdef meltcallcount
				meltcallcount	/* the_meltcallcount */
#else
				0L
#endif /* meltcallcount the_meltcallcount */
				;;
			      MELT_LOCATION
				("warmelt-normal.melt:1634:/ checksignal");
			      MELT_CHECK_SIGNAL ();
			      ;
			      /*^apply */
			      /*apply */
			      {
				union meltparam_un argtab[7];
				memset (&argtab, 0, sizeof (argtab));
				/*^apply.arg */
				argtab[0].meltbp_long =
				  /*_#THE_MELTCALLCOUNT__L25*/ meltfnum[8];
				/*^apply.arg */
				argtab[1].meltbp_cstring =
				  "warmelt-normal.melt";
				/*^apply.arg */
				argtab[2].meltbp_long = 1634;
				/*^apply.arg */
				argtab[3].meltbp_cstring =
				  "normexp_symbol local value bvar=";
				/*^apply.arg */
				argtab[4].meltbp_aptr =
				  (melt_ptr_t *) & /*_.BVAR__V42*/
				  meltfptr[41];
				/*^apply.arg */
				argtab[5].meltbp_cstring = " updated sycmap=";
				/*^apply.arg */
				argtab[6].meltbp_aptr =
				  (melt_ptr_t *) & /*_.SYCMAP__V30*/
				  meltfptr[26];
				/*_.MELT_DEBUG_FUN__V77*/ meltfptr[45] =
				  melt_apply ((meltclosure_ptr_t)
					      (( /*!MELT_DEBUG_FUN */
						meltfrout->tabval[0])),
					      (melt_ptr_t) (( /*nil */ NULL)),
					      (MELTBPARSTR_LONG
					       MELTBPARSTR_CSTRING
					       MELTBPARSTR_LONG
					       MELTBPARSTR_CSTRING
					       MELTBPARSTR_PTR
					       MELTBPARSTR_CSTRING
					       MELTBPARSTR_PTR ""), argtab,
					      "", (union meltparam_un *) 0);
			      }
			      ;
			      /*_.IF___V76*/ meltfptr[52] =
				/*_.MELT_DEBUG_FUN__V77*/ meltfptr[45];;
			      /*epilog */

			      MELT_LOCATION
				("warmelt-normal.melt:1634:/ clear");
		     /*clear *//*_#THE_MELTCALLCOUNT__L25*/
				meltfnum[8] = 0;
			      /*^clear */
		     /*clear *//*_.MELT_DEBUG_FUN__V77*/ meltfptr[45]
				= 0;
			    }
			    ;
			  }
			else
			  {	/*^cond.else */

	  /*_.IF___V76*/ meltfptr[52] = NULL;;
			  }
			;
			MELT_LOCATION
			  ("warmelt-normal.melt:1634:/ quasiblock");


			/*_.PROGN___V78*/ meltfptr[72] =
			  /*_.IF___V76*/ meltfptr[52];;
			/*^compute */
			/*_.IFCPP___V75*/ meltfptr[50] =
			  /*_.PROGN___V78*/ meltfptr[72];;
			/*epilog */

			MELT_LOCATION ("warmelt-normal.melt:1634:/ clear");
		   /*clear *//*_#MELT_NEED_DBG__L24*/ meltfnum[22] = 0;
			/*^clear */
		   /*clear *//*_.IF___V76*/ meltfptr[52] = 0;
			/*^clear */
		   /*clear *//*_.PROGN___V78*/ meltfptr[72] = 0;
		      }

#else /*MELT_HAVE_DEBUG */
		      /*^cppif.else */
		      /*_.IFCPP___V75*/ meltfptr[50] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
		      ;
		      MELT_LOCATION
			("warmelt-normal.melt:1635:/ checksignal");
		      MELT_CHECK_SIGNAL ();
		      ;
		      /*^quasiblock */


		      /*_.RETVAL___V1*/ meltfptr[0] =
			/*_.BVAR__V42*/ meltfptr[41];;
		      MELT_LOCATION
			("warmelt-normal.melt:1635:/ putxtraresult");
		      if (!meltxrestab_ || !meltxresdescr_)
			goto labend_rout;
		      if (meltxresdescr_[0] != MELTBPAR_PTR)
			goto labend_rout;
		      if (meltxrestab_[0].meltbp_aptr)
			*(meltxrestab_[0].meltbp_aptr) =
			  (melt_ptr_t) (( /*nil */ NULL));
		      ;
		      /*^finalreturn */
		      ;
		      /*finalret */ goto labend_rout;
		      MELT_LOCATION ("warmelt-normal.melt:1631:/ quasiblock");


		      /*_.PROGN___V80*/ meltfptr[54] =
			/*_.RETURN___V79*/ meltfptr[73];;
		      /*^compute */
		      /*_.IFELSE___V63*/ meltfptr[53] =
			/*_.PROGN___V80*/ meltfptr[54];;
		      /*epilog */

		      MELT_LOCATION ("warmelt-normal.melt:1600:/ clear");
		 /*clear *//*_.IFCPP___V75*/ meltfptr[50] = 0;
		      /*^clear */
		 /*clear *//*_.RETURN___V79*/ meltfptr[73] = 0;
		      /*^clear */
		 /*clear *//*_.PROGN___V80*/ meltfptr[54] = 0;
		    }
		    ;
		  }
		;
		/*_.LET___V41*/ meltfptr[40] =
		  /*_.IFELSE___V63*/ meltfptr[53];;

		MELT_LOCATION ("warmelt-normal.melt:1572:/ clear");
	       /*clear *//*_.BVAR__V42*/ meltfptr[41] = 0;
		/*^clear */
	       /*clear *//*_.VAL__V43*/ meltfptr[42] = 0;
		/*^clear */
	       /*clear *//*_.IFCPP___V44*/ meltfptr[43] = 0;
		/*^clear */
	       /*clear *//*_#NULL__L15*/ meltfnum[13] = 0;
		/*^clear */
	       /*clear *//*_#IS_LIST__L19*/ meltfnum[17] = 0;
		/*^clear */
	       /*clear *//*_#IF___L20*/ meltfnum[16] = 0;
		/*^clear */
	       /*clear *//*_.IFELSE___V63*/ meltfptr[53] = 0;
		/*_.IFELSE___V40*/ meltfptr[35] =
		  /*_.LET___V41*/ meltfptr[40];;
		/*epilog */

		MELT_LOCATION ("warmelt-normal.melt:1571:/ clear");
	       /*clear *//*_.LET___V41*/ meltfptr[40] = 0;
	      }
	      ;
	    }
	  else
	    {			/*^cond.else */

	      /*^block */
	      /*anyblock */
	      {

		MELT_LOCATION ("warmelt-normal.melt:1640:/ checksignal");
		MELT_CHECK_SIGNAL ();
		;
		/*^checksignal */
		MELT_CHECK_SIGNAL ();
		;
     /*_#IS_LIST__L26*/ meltfnum[8] =
		  (melt_magic_discr
		   ((melt_ptr_t) ( /*_.PROCS__V14*/ meltfptr[13])) ==
		   MELTOBMAG_LIST);;
		MELT_LOCATION ("warmelt-normal.melt:1640:/ cond");
		/*cond */ if ( /*_#IS_LIST__L26*/ meltfnum[8])	/*then */
		  {
		    /*^cond.then */
		    /*^block */
		    /*anyblock */
		    {

       /*_.LIST_FIRST__V81*/ meltfptr[45] =
			(melt_list_first
			 ((melt_ptr_t) ( /*_.PROCS__V14*/ meltfptr[13])));;
		      /*^compute */
       /*_#IS_PAIR__L28*/ meltfnum[13] =
			(melt_magic_discr
			 ((melt_ptr_t) ( /*_.LIST_FIRST__V81*/ meltfptr[45]))
			 == MELTOBMAG_PAIR);;
		      /*^compute */
		      /*_#IF___L27*/ meltfnum[22] =
			/*_#IS_PAIR__L28*/ meltfnum[13];;
		      /*epilog */

		      MELT_LOCATION ("warmelt-normal.melt:1640:/ clear");
		 /*clear *//*_.LIST_FIRST__V81*/ meltfptr[45] = 0;
		      /*^clear */
		 /*clear *//*_#IS_PAIR__L28*/ meltfnum[13] = 0;
		    }
		    ;
		  }
		else
		  {		/*^cond.else */

      /*_#IF___L27*/ meltfnum[22] = 0;;
		  }
		;
		MELT_LOCATION ("warmelt-normal.melt:1640:/ cond");
		/*cond */ if ( /*_#IF___L27*/ meltfnum[22])	/*then */
		  {
		    /*^cond.then */
		    /*^block */
		    /*anyblock */
		    {


#if MELT_HAVE_DEBUG
		      MELT_LOCATION ("warmelt-normal.melt:1642:/ cppif.then");
		      /*^block */
		      /*anyblock */
		      {


			{
			  /*^locexp */
			  /*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
			  melt_dbgcounter++;
#endif
			  ;
			}
			;
			/*^checksignal */
			MELT_CHECK_SIGNAL ();
			;
	 /*_#MELT_NEED_DBG__L29*/ meltfnum[17] =
			  /*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
			  ( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
			  0	/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
			  ;;
			MELT_LOCATION ("warmelt-normal.melt:1642:/ cond");
			/*cond */ if ( /*_#MELT_NEED_DBG__L29*/ meltfnum[17])	/*then */
			  {
			    /*^cond.then */
			    /*^block */
			    /*anyblock */
			    {

	   /*_#THE_MELTCALLCOUNT__L30*/ meltfnum[16] =
#ifdef meltcallcount
				meltcallcount	/* the_meltcallcount */
#else
				0L
#endif /* meltcallcount the_meltcallcount */
				;;
			      MELT_LOCATION
				("warmelt-normal.melt:1642:/ checksignal");
			      MELT_CHECK_SIGNAL ();
			      ;
			      /*^apply */
			      /*apply */
			      {
				union meltparam_un argtab[7];
				memset (&argtab, 0, sizeof (argtab));
				/*^apply.arg */
				argtab[0].meltbp_long =
				  /*_#THE_MELTCALLCOUNT__L30*/ meltfnum[16];
				/*^apply.arg */
				argtab[1].meltbp_cstring =
				  "warmelt-normal.melt";
				/*^apply.arg */
				argtab[2].meltbp_long = 1642;
				/*^apply.arg */
				argtab[3].meltbp_cstring =
				  "normexp_symbol closed procs=";
				/*^apply.arg */
				argtab[4].meltbp_aptr =
				  (melt_ptr_t *) & /*_.PROCS__V14*/
				  meltfptr[13];
				/*^apply.arg */
				argtab[5].meltbp_cstring = " bind=";
				/*^apply.arg */
				argtab[6].meltbp_aptr =
				  (melt_ptr_t *) & /*_.BIND__V13*/
				  meltfptr[12];
				/*_.MELT_DEBUG_FUN__V85*/ meltfptr[73] =
				  melt_apply ((meltclosure_ptr_t)
					      (( /*!MELT_DEBUG_FUN */
						meltfrout->tabval[0])),
					      (melt_ptr_t) (( /*nil */ NULL)),
					      (MELTBPARSTR_LONG
					       MELTBPARSTR_CSTRING
					       MELTBPARSTR_LONG
					       MELTBPARSTR_CSTRING
					       MELTBPARSTR_PTR
					       MELTBPARSTR_CSTRING
					       MELTBPARSTR_PTR ""), argtab,
					      "", (union meltparam_un *) 0);
			      }
			      ;
			      /*_.IF___V84*/ meltfptr[50] =
				/*_.MELT_DEBUG_FUN__V85*/ meltfptr[73];;
			      /*epilog */

			      MELT_LOCATION
				("warmelt-normal.melt:1642:/ clear");
		     /*clear *//*_#THE_MELTCALLCOUNT__L30*/
				meltfnum[16] = 0;
			      /*^clear */
		     /*clear *//*_.MELT_DEBUG_FUN__V85*/ meltfptr[73]
				= 0;
			    }
			    ;
			  }
			else
			  {	/*^cond.else */

	  /*_.IF___V84*/ meltfptr[50] = NULL;;
			  }
			;
			MELT_LOCATION
			  ("warmelt-normal.melt:1642:/ quasiblock");


			/*_.PROGN___V86*/ meltfptr[54] =
			  /*_.IF___V84*/ meltfptr[50];;
			/*^compute */
			/*_.IFCPP___V83*/ meltfptr[72] =
			  /*_.PROGN___V86*/ meltfptr[54];;
			/*epilog */

			MELT_LOCATION ("warmelt-normal.melt:1642:/ clear");
		   /*clear *//*_#MELT_NEED_DBG__L29*/ meltfnum[17] = 0;
			/*^clear */
		   /*clear *//*_.IF___V84*/ meltfptr[50] = 0;
			/*^clear */
		   /*clear *//*_.PROGN___V86*/ meltfptr[54] = 0;
		      }

#else /*MELT_HAVE_DEBUG */
		      /*^cppif.else */
		      /*_.IFCPP___V83*/ meltfptr[72] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
		      ;
		      MELT_LOCATION ("warmelt-normal.melt:1644:/ quasiblock");


		      MELT_LOCATION
			("warmelt-normal.melt:1645:/ checksignal");
		      MELT_CHECK_SIGNAL ();
		      ;
       /*_#IS_A__L31*/ meltfnum[13] =
			melt_is_instance_of ((melt_ptr_t)
					     ( /*_.BIND__V13*/ meltfptr[12]),
					     (melt_ptr_t) (( /*!CLASS_FORMAL_BINDING */ meltfrout->tabval[18])));;
		      MELT_LOCATION ("warmelt-normal.melt:1645:/ cond");
		      /*cond */ if ( /*_#IS_A__L31*/ meltfnum[13])	/*then */
			{
			  /*^cond.then */
			  /*^block */
			  /*anyblock */
			  {

			    MELT_LOCATION
			      ("warmelt-normal.melt:1646:/ getslot");
			    {
			      melt_ptr_t slot = NULL, obj = NULL;
			      obj =
				(melt_ptr_t) ( /*_.BIND__V13*/ meltfptr[12])
				/*=obj*/ ;
			      melt_object_get_field (slot, obj, 1,
						     "FBIND_TYPE");
	  /*_.FBIND_TYPE__V89*/ meltfptr[43] = slot;
			    };
			    ;
			    /*_.BTY__V88*/ meltfptr[42] =
			      /*_.FBIND_TYPE__V89*/ meltfptr[43];;
			    /*epilog */

			    MELT_LOCATION
			      ("warmelt-normal.melt:1645:/ clear");
		   /*clear *//*_.FBIND_TYPE__V89*/ meltfptr[43] = 0;
			  }
			  ;
			}
		      else
			{	/*^cond.else */

			  /*^block */
			  /*anyblock */
			  {

			    MELT_LOCATION
			      ("warmelt-normal.melt:1647:/ checksignal");
			    MELT_CHECK_SIGNAL ();
			    ;
	 /*_#IS_A__L32*/ meltfnum[16] =
			      melt_is_instance_of ((melt_ptr_t)
						   ( /*_.BIND__V13*/
						    meltfptr[12]),
						   (melt_ptr_t) (( /*!CLASS_LET_BINDING */ meltfrout->tabval[19])));;
			    MELT_LOCATION ("warmelt-normal.melt:1647:/ cond");
			    /*cond */ if ( /*_#IS_A__L32*/ meltfnum[16])	/*then */
			      {
				/*^cond.then */
				/*^block */
				/*anyblock */
				{

				  MELT_LOCATION
				    ("warmelt-normal.melt:1648:/ getslot");
				  {
				    melt_ptr_t slot = NULL, obj = NULL;
				    obj =
				      (melt_ptr_t) ( /*_.BIND__V13*/
						    meltfptr[12]) /*=obj*/ ;
				    melt_object_get_field (slot, obj, 1,
							   "LETBIND_TYPE");
	    /*_.LETBIND_TYPE__V91*/ meltfptr[40] =
				      slot;
				  };
				  ;
				  /*_.IFELSE___V90*/ meltfptr[53] =
				    /*_.LETBIND_TYPE__V91*/ meltfptr[40];;
				  /*epilog */

				  MELT_LOCATION
				    ("warmelt-normal.melt:1647:/ clear");
		     /*clear *//*_.LETBIND_TYPE__V91*/
				    meltfptr[40] = 0;
				}
				;
			      }
			    else
			      {	/*^cond.else */

				/*^block */
				/*anyblock */
				{

				  MELT_LOCATION
				    ("warmelt-normal.melt:1649:/ quasiblock");


				  /*_.PROGN___V92*/ meltfptr[45] =
				    ( /*nil */ NULL);;
				  /*^compute */
				  /*_.IFELSE___V90*/ meltfptr[53] =
				    /*_.PROGN___V92*/ meltfptr[45];;
				  /*epilog */

				  MELT_LOCATION
				    ("warmelt-normal.melt:1647:/ clear");
		     /*clear *//*_.PROGN___V92*/ meltfptr[45] =
				    0;
				}
				;
			      }
			    ;
			    /*_.BTY__V88*/ meltfptr[42] =
			      /*_.IFELSE___V90*/ meltfptr[53];;
			    /*epilog */

			    MELT_LOCATION
			      ("warmelt-normal.melt:1645:/ clear");
		   /*clear *//*_#IS_A__L32*/ meltfnum[16] = 0;
			    /*^clear */
		   /*clear *//*_.IFELSE___V90*/ meltfptr[53] = 0;
			  }
			  ;
			}
		      ;
		      MELT_LOCATION
			("warmelt-normal.melt:1651:/ checksignal");
		      MELT_CHECK_SIGNAL ();
		      ;
		      /*^cond */
		      /*cond */ if ( /*_.BTY__V88*/ meltfptr[42])	/*then */
			{
			  /*^cond.then */
			  /*^block */
			  /*anyblock */
			  {

			    MELT_LOCATION
			      ("warmelt-normal.melt:1652:/ checksignal");
			    MELT_CHECK_SIGNAL ();
			    ;
	 /*_#__L33*/ meltfnum[17] =
			      (( /*_.BTY__V88*/ meltfptr[42]) !=
			       (( /*!CTYPE_VALUE */ meltfrout->tabval[11])));;
			    MELT_LOCATION ("warmelt-normal.melt:1652:/ cond");
			    /*cond */ if ( /*_#__L33*/ meltfnum[17])	/*then */
			      {
				/*^cond.then */
				/*^block */
				/*anyblock */
				{

				  MELT_LOCATION
				    ("warmelt-normal.melt:1655:/ getslot");
				  {
				    melt_ptr_t slot = NULL, obj = NULL;
				    obj =
				      (melt_ptr_t) ( /*_.RECV__V2*/
						    meltfptr[1]) /*=obj*/ ;
				    melt_object_get_field (slot, obj, 1,
							   "NAMED_NAME");
	    /*_.NAMED_NAME__V93*/ meltfptr[73] = slot;
				  };
				  ;

				  {
				    MELT_LOCATION
				      ("warmelt-normal.melt:1653:/ locexp");
				    melt_error_str ((melt_ptr_t)
						    ( /*_.PSLOC__V5*/
						     meltfptr[4]),
						    ("closed variable has non value type (boxing required)"),
						    (melt_ptr_t) ( /*_.NAMED_NAME__V93*/ meltfptr[73]));
				  }
				  ;
				  /*epilog */

				  MELT_LOCATION
				    ("warmelt-normal.melt:1652:/ clear");
		     /*clear *//*_.NAMED_NAME__V93*/ meltfptr[73]
				    = 0;
				}
				;
			      }	/*noelse */
			    ;
			    /*epilog */

			    MELT_LOCATION
			      ("warmelt-normal.melt:1651:/ clear");
		   /*clear *//*_#__L33*/ meltfnum[17] = 0;
			  }
			  ;
			}	/*noelse */
		      ;
		      MELT_LOCATION ("warmelt-normal.melt:1657:/ compute");
		      /*_.BTY__V88*/ meltfptr[42] =
			/*_.SETQ___V94*/ meltfptr[50] =
			( /*!CTYPE_VALUE */ meltfrout->tabval[11]);;
		      MELT_LOCATION
			("warmelt-normal.melt:1658:/ checksignal");
		      MELT_CHECK_SIGNAL ();
		      ;
       /*_#IS_A__L34*/ meltfnum[16] =
			melt_is_instance_of ((melt_ptr_t)
					     ( /*_.BIND__V13*/ meltfptr[12]),
					     (melt_ptr_t) (( /*!CLASS_FIXED_BINDING */ meltfrout->tabval[20])));;
		      MELT_LOCATION ("warmelt-normal.melt:1658:/ cond");
		      /*cond */ if ( /*_#IS_A__L34*/ meltfnum[16])	/*then */
			{
			  /*^cond.then */
			  /*^block */
			  /*anyblock */
			  {

			    MELT_LOCATION
			      ("warmelt-normal.melt:1659:/ quasiblock");


			    MELT_LOCATION
			      ("warmelt-normal.melt:1660:/ checksignal");
			    MELT_CHECK_SIGNAL ();
			    ;
			    /*^quasiblock */


			    /*^rawallocobj */
			    /*rawallocobj */
			    {
			      melt_ptr_t newobj = 0;
			      melt_raw_object_create (newobj,
						      (melt_ptr_t) (( /*!CLASS_NREP_CONSTOCC */ meltfrout->tabval[10])), (5), "CLASS_NREP_CONSTOCC");
	  /*_.INST__V98*/ meltfptr[45] =
				newobj;
			    };
			    ;
			    /*^putslot */
			    /*putslot */
			    melt_assertmsg ("putslot checkobj @NREP_LOC",
					    melt_magic_discr ((melt_ptr_t)
							      ( /*_.INST__V98*/ meltfptr[45])) == MELTOBMAG_OBJECT);
			    melt_putfield_object (( /*_.INST__V98*/
						   meltfptr[45]), (0),
						  ( /*_.PSLOC__V5*/
						   meltfptr[4]), "NREP_LOC");
			    ;
			    /*^putslot */
			    /*putslot */
			    melt_assertmsg ("putslot checkobj @NOCC_SYMB",
					    melt_magic_discr ((melt_ptr_t)
							      ( /*_.INST__V98*/ meltfptr[45])) == MELTOBMAG_OBJECT);
			    melt_putfield_object (( /*_.INST__V98*/
						   meltfptr[45]), (1),
						  ( /*_.RECV__V2*/
						   meltfptr[1]), "NOCC_SYMB");
			    ;
			    /*^putslot */
			    /*putslot */
			    melt_assertmsg ("putslot checkobj @NOCC_BIND",
					    melt_magic_discr ((melt_ptr_t)
							      ( /*_.INST__V98*/ meltfptr[45])) == MELTOBMAG_OBJECT);
			    melt_putfield_object (( /*_.INST__V98*/
						   meltfptr[45]), (3),
						  ( /*_.BIND__V13*/
						   meltfptr[12]),
						  "NOCC_BIND");
			    ;
			    /*^putslot */
			    /*putslot */
			    melt_assertmsg ("putslot checkobj @NOCC_CTYP",
					    melt_magic_discr ((melt_ptr_t)
							      ( /*_.INST__V98*/ meltfptr[45])) == MELTOBMAG_OBJECT);
			    melt_putfield_object (( /*_.INST__V98*/
						   meltfptr[45]), (2),
						  (( /*!CTYPE_VALUE */
						    meltfrout->tabval[11])),
						  "NOCC_CTYP");
			    ;
			    /*^putslot */
			    /*putslot */
			    melt_assertmsg ("putslot checkobj @NCLOC_PROCS",
					    melt_magic_discr ((melt_ptr_t)
							      ( /*_.INST__V98*/ meltfptr[45])) == MELTOBMAG_OBJECT);
			    melt_putfield_object (( /*_.INST__V98*/
						   meltfptr[45]), (4),
						  ( /*_.PROCS__V14*/
						   meltfptr[13]),
						  "NCLOC_PROCS");
			    ;
			    /*^touchobj */

			    melt_dbgtrace_written_object ( /*_.INST__V98*/
							  meltfptr[45],
							  "newly made instance");
			    ;
			    /*_.FXOCC__V97*/ meltfptr[40] =
			      /*_.INST__V98*/ meltfptr[45];;

			    {
			      MELT_LOCATION
				("warmelt-normal.melt:1667:/ locexp");
			      meltgc_put_mapobjects ((meltmapobjects_ptr_t)
						     ( /*_.SYCMAP__V30*/
						      meltfptr[26]),
						     (meltobject_ptr_t) ( /*_.RECV__V2*/ meltfptr[1]),
						     (melt_ptr_t) ( /*_.FXOCC__V97*/ meltfptr[40]));
			    }
			    ;

#if MELT_HAVE_DEBUG
			    MELT_LOCATION
			      ("warmelt-normal.melt:1668:/ cppif.then");
			    /*^block */
			    /*anyblock */
			    {


			      {
				/*^locexp */
				/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
				melt_dbgcounter++;
#endif
				;
			      }
			      ;
			      /*^checksignal */
			      MELT_CHECK_SIGNAL ();
			      ;
	   /*_#MELT_NEED_DBG__L35*/ meltfnum[17] =
				/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
				( /*melt_need_dbg */
				 melt_need_debug ((int) 0))
#else
				0	/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
				;;
			      MELT_LOCATION
				("warmelt-normal.melt:1668:/ cond");
			      /*cond */ if ( /*_#MELT_NEED_DBG__L35*/ meltfnum[17])	/*then */
				{
				  /*^cond.then */
				  /*^block */
				  /*anyblock */
				  {

	     /*_#THE_MELTCALLCOUNT__L36*/ meltfnum[35]
				      =
#ifdef meltcallcount
				      meltcallcount	/* the_meltcallcount */
#else
				      0L
#endif /* meltcallcount the_meltcallcount */
				      ;;
				    MELT_LOCATION
				      ("warmelt-normal.melt:1668:/ checksignal");
				    MELT_CHECK_SIGNAL ();
				    ;
				    /*^apply */
				    /*apply */
				    {
				      union meltparam_un argtab[7];
				      memset (&argtab, 0, sizeof (argtab));
				      /*^apply.arg */
				      argtab[0].meltbp_long =
					/*_#THE_MELTCALLCOUNT__L36*/
					meltfnum[35];
				      /*^apply.arg */
				      argtab[1].meltbp_cstring =
					"warmelt-normal.melt";
				      /*^apply.arg */
				      argtab[2].meltbp_long = 1668;
				      /*^apply.arg */
				      argtab[3].meltbp_cstring =
					"normexp_symbol fxocc";
				      /*^apply.arg */
				      argtab[4].meltbp_aptr =
					(melt_ptr_t *) & /*_.FXOCC__V97*/
					meltfptr[40];
				      /*^apply.arg */
				      argtab[5].meltbp_cstring =
					" updated sycmap=";
				      /*^apply.arg */
				      argtab[6].meltbp_aptr =
					(melt_ptr_t *) & /*_.SYCMAP__V30*/
					meltfptr[26];
				      /*_.MELT_DEBUG_FUN__V101*/ meltfptr[100]
					=
					melt_apply ((meltclosure_ptr_t)
						    (( /*!MELT_DEBUG_FUN */
						      meltfrout->tabval[0])),
						    (melt_ptr_t) (( /*nil */
								   NULL)),
						    (MELTBPARSTR_LONG
						     MELTBPARSTR_CSTRING
						     MELTBPARSTR_LONG
						     MELTBPARSTR_CSTRING
						     MELTBPARSTR_PTR
						     MELTBPARSTR_CSTRING
						     MELTBPARSTR_PTR ""),
						    argtab, "",
						    (union meltparam_un *) 0);
				    }
				    ;
				    /*_.IF___V100*/ meltfptr[73] =
				      /*_.MELT_DEBUG_FUN__V101*/
				      meltfptr[100];;
				    /*epilog */

				    MELT_LOCATION
				      ("warmelt-normal.melt:1668:/ clear");
		       /*clear *//*_#THE_MELTCALLCOUNT__L36*/
				      meltfnum[35] = 0;
				    /*^clear */
		       /*clear *//*_.MELT_DEBUG_FUN__V101*/
				      meltfptr[100] = 0;
				  }
				  ;
				}
			      else
				{	/*^cond.else */

	    /*_.IF___V100*/ meltfptr[73] = NULL;;
				}
			      ;
			      MELT_LOCATION
				("warmelt-normal.melt:1668:/ quasiblock");


			      /*_.PROGN___V102*/ meltfptr[100] =
				/*_.IF___V100*/ meltfptr[73];;
			      /*^compute */
			      /*_.IFCPP___V99*/ meltfptr[53] =
				/*_.PROGN___V102*/ meltfptr[100];;
			      /*epilog */

			      MELT_LOCATION
				("warmelt-normal.melt:1668:/ clear");
		     /*clear *//*_#MELT_NEED_DBG__L35*/ meltfnum[17]
				= 0;
			      /*^clear */
		     /*clear *//*_.IF___V100*/ meltfptr[73] = 0;
			      /*^clear */
		     /*clear *//*_.PROGN___V102*/ meltfptr[100] = 0;
			    }

#else /*MELT_HAVE_DEBUG */
			    /*^cppif.else */
			    /*_.IFCPP___V99*/ meltfptr[53] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
			    ;
			    MELT_LOCATION
			      ("warmelt-normal.melt:1672:/ quasiblock");


			    /*^newclosure */
			 /*newclosure *//*_.LAMBDA___V104*/ meltfptr[100] =
			      (melt_ptr_t)
			      meltgc_new_closure ((meltobject_ptr_t)
						  (((melt_ptr_t)
						    (MELT_PREDEF
						     (DISCR_CLOSURE)))),
						  (meltroutine_ptr_t) (( /*!konst_25 */ meltfrout->tabval[25])), (1));
			    ;
			    /*^putclosedv */
			    /*putclosv */
			    melt_assertmsg ("putclosv checkclo",
					    melt_magic_discr ((melt_ptr_t)
							      ( /*_.LAMBDA___V104*/ meltfptr[100])) == MELTOBMAG_CLOSURE);
			    melt_assertmsg ("putclosv checkoff", 0 >= 0
					    && 0 <
					    melt_closure_size ((melt_ptr_t)
							       ( /*_.LAMBDA___V104*/ meltfptr[100])));
			    ((meltclosure_ptr_t) /*_.LAMBDA___V104*/
			     meltfptr[100])->tabval[0] =
(melt_ptr_t) ( /*_.FXOCC__V97*/ meltfptr[40]);
			    ;
			    /*_.LAMBDA___V103*/ meltfptr[73] =
			      /*_.LAMBDA___V104*/ meltfptr[100];;
			    MELT_LOCATION
			      ("warmelt-normal.melt:1670:/ checksignal");
			    MELT_CHECK_SIGNAL ();
			    ;
			    /*^apply */
			    /*apply */
			    {
			      union meltparam_un argtab[1];
			      memset (&argtab, 0, sizeof (argtab));
			      /*^apply.arg */
			      argtab[0].meltbp_aptr =
				(melt_ptr_t *) & /*_.LAMBDA___V103*/
				meltfptr[73];
			      /*_.LIST_EVERY__V105*/ meltfptr[104] =
				melt_apply ((meltclosure_ptr_t)
					    (( /*!LIST_EVERY */ meltfrout->
					      tabval[12])),
					    (melt_ptr_t) ( /*_.PROCS__V14*/
							  meltfptr[13]),
					    (MELTBPARSTR_PTR ""), argtab, "",
					    (union meltparam_un *) 0);
			    }
			    ;
			    MELT_LOCATION
			      ("warmelt-normal.melt:1685:/ checksignal");
			    MELT_CHECK_SIGNAL ();
			    ;
			    /*^quasiblock */


			    /*_.RETVAL___V1*/ meltfptr[0] =
			      /*_.FXOCC__V97*/ meltfptr[40];;
			    MELT_LOCATION
			      ("warmelt-normal.melt:1685:/ putxtraresult");
			    if (!meltxrestab_ || !meltxresdescr_)
			      goto labend_rout;
			    if (meltxresdescr_[0] != MELTBPAR_PTR)
			      goto labend_rout;
			    if (meltxrestab_[0].meltbp_aptr)
			      *(meltxrestab_[0].meltbp_aptr) =
				(melt_ptr_t) (( /*nil */ NULL));
			    ;
			    /*^finalreturn */
			    ;
			    /*finalret */ goto labend_rout;
			    /*_.LET___V96*/ meltfptr[43] =
			      /*_.RETURN___V106*/ meltfptr[105];;

			    MELT_LOCATION
			      ("warmelt-normal.melt:1659:/ clear");
		   /*clear *//*_.FXOCC__V97*/ meltfptr[40] = 0;
			    /*^clear */
		   /*clear *//*_.IFCPP___V99*/ meltfptr[53] = 0;
			    /*^clear */
		   /*clear *//*_.LAMBDA___V103*/ meltfptr[73] = 0;
			    /*^clear */
		   /*clear *//*_.LIST_EVERY__V105*/ meltfptr[104] =
			      0;
			    /*^clear */
		   /*clear *//*_.RETURN___V106*/ meltfptr[105] = 0;
			    /*_.IFELSE___V95*/ meltfptr[54] =
			      /*_.LET___V96*/ meltfptr[43];;
			    /*epilog */

			    MELT_LOCATION
			      ("warmelt-normal.melt:1658:/ clear");
		   /*clear *//*_.LET___V96*/ meltfptr[43] = 0;
			  }
			  ;
			}
		      else
			{	/*^cond.else */

			  /*^block */
			  /*anyblock */
			  {

			    MELT_LOCATION
			      ("warmelt-normal.melt:1687:/ quasiblock");


			    MELT_LOCATION
			      ("warmelt-normal.melt:1688:/ checksignal");
			    MELT_CHECK_SIGNAL ();
			    ;
			    /*^quasiblock */


			    /*^rawallocobj */
			    /*rawallocobj */
			    {
			      melt_ptr_t newobj = 0;
			      melt_raw_object_create (newobj,
						      (melt_ptr_t) (( /*!CLASS_NREP_CLOSEDOCC */ meltfrout->tabval[26])), (5), "CLASS_NREP_CLOSEDOCC");
	  /*_.INST__V109*/ meltfptr[73] =
				newobj;
			    };
			    ;
			    /*^putslot */
			    /*putslot */
			    melt_assertmsg ("putslot checkobj @NREP_LOC",
					    melt_magic_discr ((melt_ptr_t)
							      ( /*_.INST__V109*/ meltfptr[73])) == MELTOBMAG_OBJECT);
			    melt_putfield_object (( /*_.INST__V109*/
						   meltfptr[73]), (0),
						  ( /*_.PSLOC__V5*/
						   meltfptr[4]), "NREP_LOC");
			    ;
			    /*^putslot */
			    /*putslot */
			    melt_assertmsg ("putslot checkobj @NOCC_SYMB",
					    melt_magic_discr ((melt_ptr_t)
							      ( /*_.INST__V109*/ meltfptr[73])) == MELTOBMAG_OBJECT);
			    melt_putfield_object (( /*_.INST__V109*/
						   meltfptr[73]), (1),
						  ( /*_.RECV__V2*/
						   meltfptr[1]), "NOCC_SYMB");
			    ;
			    /*^putslot */
			    /*putslot */
			    melt_assertmsg ("putslot checkobj @NOCC_CTYP",
					    melt_magic_discr ((melt_ptr_t)
							      ( /*_.INST__V109*/ meltfptr[73])) == MELTOBMAG_OBJECT);
			    melt_putfield_object (( /*_.INST__V109*/
						   meltfptr[73]), (2),
						  (( /*!CTYPE_VALUE */
						    meltfrout->tabval[11])),
						  "NOCC_CTYP");
			    ;
			    /*^putslot */
			    /*putslot */
			    melt_assertmsg ("putslot checkobj @NOCC_BIND",
					    melt_magic_discr ((melt_ptr_t)
							      ( /*_.INST__V109*/ meltfptr[73])) == MELTOBMAG_OBJECT);
			    melt_putfield_object (( /*_.INST__V109*/
						   meltfptr[73]), (3),
						  ( /*_.BIND__V13*/
						   meltfptr[12]),
						  "NOCC_BIND");
			    ;
			    /*^putslot */
			    /*putslot */
			    melt_assertmsg ("putslot checkobj @NCLOC_PROCS",
					    melt_magic_discr ((melt_ptr_t)
							      ( /*_.INST__V109*/ meltfptr[73])) == MELTOBMAG_OBJECT);
			    melt_putfield_object (( /*_.INST__V109*/
						   meltfptr[73]), (4),
						  ( /*_.PROCS__V14*/
						   meltfptr[13]),
						  "NCLOC_PROCS");
			    ;
			    /*^touchobj */

			    melt_dbgtrace_written_object ( /*_.INST__V109*/
							  meltfptr[73],
							  "newly made instance");
			    ;
			    /*_.CLOCC__V108*/ meltfptr[53] =
			      /*_.INST__V109*/ meltfptr[73];;

			    {
			      MELT_LOCATION
				("warmelt-normal.melt:1696:/ locexp");
			      meltgc_put_mapobjects ((meltmapobjects_ptr_t)
						     ( /*_.SYCMAP__V30*/
						      meltfptr[26]),
						     (meltobject_ptr_t) ( /*_.RECV__V2*/ meltfptr[1]),
						     (melt_ptr_t) ( /*_.CLOCC__V108*/ meltfptr[53]));
			    }
			    ;

#if MELT_HAVE_DEBUG
			    MELT_LOCATION
			      ("warmelt-normal.melt:1697:/ cppif.then");
			    /*^block */
			    /*anyblock */
			    {


			      {
				/*^locexp */
				/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
				melt_dbgcounter++;
#endif
				;
			      }
			      ;
			      /*^checksignal */
			      MELT_CHECK_SIGNAL ();
			      ;
	   /*_#MELT_NEED_DBG__L37*/ meltfnum[35] =
				/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
				( /*melt_need_dbg */
				 melt_need_debug ((int) 0))
#else
				0	/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
				;;
			      MELT_LOCATION
				("warmelt-normal.melt:1697:/ cond");
			      /*cond */ if ( /*_#MELT_NEED_DBG__L37*/ meltfnum[35])	/*then */
				{
				  /*^cond.then */
				  /*^block */
				  /*anyblock */
				  {

	     /*_#THE_MELTCALLCOUNT__L38*/ meltfnum[17]
				      =
#ifdef meltcallcount
				      meltcallcount	/* the_meltcallcount */
#else
				      0L
#endif /* meltcallcount the_meltcallcount */
				      ;;
				    MELT_LOCATION
				      ("warmelt-normal.melt:1697:/ checksignal");
				    MELT_CHECK_SIGNAL ();
				    ;
				    /*^apply */
				    /*apply */
				    {
				      union meltparam_un argtab[7];
				      memset (&argtab, 0, sizeof (argtab));
				      /*^apply.arg */
				      argtab[0].meltbp_long =
					/*_#THE_MELTCALLCOUNT__L38*/
					meltfnum[17];
				      /*^apply.arg */
				      argtab[1].meltbp_cstring =
					"warmelt-normal.melt";
				      /*^apply.arg */
				      argtab[2].meltbp_long = 1697;
				      /*^apply.arg */
				      argtab[3].meltbp_cstring =
					"normexp_symbol updated sycmap=";
				      /*^apply.arg */
				      argtab[4].meltbp_aptr =
					(melt_ptr_t *) & /*_.SYCMAP__V30*/
					meltfptr[26];
				      /*^apply.arg */
				      argtab[5].meltbp_cstring = " clocc=";
				      /*^apply.arg */
				      argtab[6].meltbp_aptr =
					(melt_ptr_t *) & /*_.CLOCC__V108*/
					meltfptr[53];
				      /*_.MELT_DEBUG_FUN__V112*/ meltfptr[43]
					=
					melt_apply ((meltclosure_ptr_t)
						    (( /*!MELT_DEBUG_FUN */
						      meltfrout->tabval[0])),
						    (melt_ptr_t) (( /*nil */
								   NULL)),
						    (MELTBPARSTR_LONG
						     MELTBPARSTR_CSTRING
						     MELTBPARSTR_LONG
						     MELTBPARSTR_CSTRING
						     MELTBPARSTR_PTR
						     MELTBPARSTR_CSTRING
						     MELTBPARSTR_PTR ""),
						    argtab, "",
						    (union meltparam_un *) 0);
				    }
				    ;
				    /*_.IF___V111*/ meltfptr[105] =
				      /*_.MELT_DEBUG_FUN__V112*/
				      meltfptr[43];;
				    /*epilog */

				    MELT_LOCATION
				      ("warmelt-normal.melt:1697:/ clear");
		       /*clear *//*_#THE_MELTCALLCOUNT__L38*/
				      meltfnum[17] = 0;
				    /*^clear */
		       /*clear *//*_.MELT_DEBUG_FUN__V112*/
				      meltfptr[43] = 0;
				  }
				  ;
				}
			      else
				{	/*^cond.else */

	    /*_.IF___V111*/ meltfptr[105] = NULL;;
				}
			      ;
			      MELT_LOCATION
				("warmelt-normal.melt:1697:/ quasiblock");


			      /*_.PROGN___V113*/ meltfptr[43] =
				/*_.IF___V111*/ meltfptr[105];;
			      /*^compute */
			      /*_.IFCPP___V110*/ meltfptr[104] =
				/*_.PROGN___V113*/ meltfptr[43];;
			      /*epilog */

			      MELT_LOCATION
				("warmelt-normal.melt:1697:/ clear");
		     /*clear *//*_#MELT_NEED_DBG__L37*/ meltfnum[35]
				= 0;
			      /*^clear */
		     /*clear *//*_.IF___V111*/ meltfptr[105] = 0;
			      /*^clear */
		     /*clear *//*_.PROGN___V113*/ meltfptr[43] = 0;
			    }

#else /*MELT_HAVE_DEBUG */
			    /*^cppif.else */
			    /*_.IFCPP___V110*/ meltfptr[104] =
			      ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
			    ;
			    MELT_LOCATION
			      ("warmelt-normal.melt:1701:/ quasiblock");


			    /*^newclosure */
			 /*newclosure *//*_.LAMBDA___V115*/ meltfptr[43] =
			      (melt_ptr_t)
			      meltgc_new_closure ((meltobject_ptr_t)
						  (((melt_ptr_t)
						    (MELT_PREDEF
						     (DISCR_CLOSURE)))),
						  (meltroutine_ptr_t) (( /*!konst_31 */ meltfrout->tabval[31])), (2));
			    ;
			    /*^putclosedv */
			    /*putclosv */
			    melt_assertmsg ("putclosv checkclo",
					    melt_magic_discr ((melt_ptr_t)
							      ( /*_.LAMBDA___V115*/ meltfptr[43])) == MELTOBMAG_CLOSURE);
			    melt_assertmsg ("putclosv checkoff", 0 >= 0
					    && 0 <
					    melt_closure_size ((melt_ptr_t)
							       ( /*_.LAMBDA___V115*/ meltfptr[43])));
			    ((meltclosure_ptr_t) /*_.LAMBDA___V115*/
			     meltfptr[43])->tabval[0] =
(melt_ptr_t) ( /*_.CLOCC__V108*/ meltfptr[53]);
			    ;
			    /*^putclosedv */
			    /*putclosv */
			    melt_assertmsg ("putclosv checkclo",
					    melt_magic_discr ((melt_ptr_t)
							      ( /*_.LAMBDA___V115*/ meltfptr[43])) == MELTOBMAG_CLOSURE);
			    melt_assertmsg ("putclosv checkoff", 1 >= 0
					    && 1 <
					    melt_closure_size ((melt_ptr_t)
							       ( /*_.LAMBDA___V115*/ meltfptr[43])));
			    ((meltclosure_ptr_t) /*_.LAMBDA___V115*/
			     meltfptr[43])->tabval[1] =
(melt_ptr_t) ( /*_.BIND__V13*/ meltfptr[12]);
			    ;
			    /*_.LAMBDA___V114*/ meltfptr[105] =
			      /*_.LAMBDA___V115*/ meltfptr[43];;
			    MELT_LOCATION
			      ("warmelt-normal.melt:1699:/ checksignal");
			    MELT_CHECK_SIGNAL ();
			    ;
			    /*^apply */
			    /*apply */
			    {
			      union meltparam_un argtab[1];
			      memset (&argtab, 0, sizeof (argtab));
			      /*^apply.arg */
			      argtab[0].meltbp_aptr =
				(melt_ptr_t *) & /*_.LAMBDA___V114*/
				meltfptr[105];
			      /*_.LIST_EVERY__V116*/ meltfptr[115] =
				melt_apply ((meltclosure_ptr_t)
					    (( /*!LIST_EVERY */ meltfrout->
					      tabval[12])),
					    (melt_ptr_t) ( /*_.PROCS__V14*/
							  meltfptr[13]),
					    (MELTBPARSTR_PTR ""), argtab, "",
					    (union meltparam_un *) 0);
			    }
			    ;
			    MELT_LOCATION
			      ("warmelt-normal.melt:1712:/ checksignal");
			    MELT_CHECK_SIGNAL ();
			    ;
			    /*^quasiblock */


			    /*_.RETVAL___V1*/ meltfptr[0] =
			      /*_.CLOCC__V108*/ meltfptr[53];;
			    MELT_LOCATION
			      ("warmelt-normal.melt:1712:/ putxtraresult");
			    if (!meltxrestab_ || !meltxresdescr_)
			      goto labend_rout;
			    if (meltxresdescr_[0] != MELTBPAR_PTR)
			      goto labend_rout;
			    if (meltxrestab_[0].meltbp_aptr)
			      *(meltxrestab_[0].meltbp_aptr) =
				(melt_ptr_t) (( /*nil */ NULL));
			    ;
			    /*^finalreturn */
			    ;
			    /*finalret */ goto labend_rout;
			    /*_.LET___V107*/ meltfptr[40] =
			      /*_.RETURN___V117*/ meltfptr[116];;

			    MELT_LOCATION
			      ("warmelt-normal.melt:1687:/ clear");
		   /*clear *//*_.CLOCC__V108*/ meltfptr[53] = 0;
			    /*^clear */
		   /*clear *//*_.IFCPP___V110*/ meltfptr[104] = 0;
			    /*^clear */
		   /*clear *//*_.LAMBDA___V114*/ meltfptr[105] = 0;
			    /*^clear */
		   /*clear *//*_.LIST_EVERY__V116*/ meltfptr[115] =
			      0;
			    /*^clear */
		   /*clear *//*_.RETURN___V117*/ meltfptr[116] = 0;
			    /*_.IFELSE___V95*/ meltfptr[54] =
			      /*_.LET___V107*/ meltfptr[40];;
			    /*epilog */

			    MELT_LOCATION
			      ("warmelt-normal.melt:1658:/ clear");
		   /*clear *//*_.LET___V107*/ meltfptr[40] = 0;
			  }
			  ;
			}
		      ;
		      /*_.LET___V87*/ meltfptr[41] =
			/*_.IFELSE___V95*/ meltfptr[54];;

		      MELT_LOCATION ("warmelt-normal.melt:1644:/ clear");
		 /*clear *//*_#IS_A__L31*/ meltfnum[13] = 0;
		      /*^clear */
		 /*clear *//*_.BTY__V88*/ meltfptr[42] = 0;
		      /*^clear */
		 /*clear *//*_.SETQ___V94*/ meltfptr[50] = 0;
		      /*^clear */
		 /*clear *//*_#IS_A__L34*/ meltfnum[16] = 0;
		      /*^clear */
		 /*clear *//*_.IFELSE___V95*/ meltfptr[54] = 0;
		      MELT_LOCATION ("warmelt-normal.melt:1640:/ quasiblock");


		      /*_.PROGN___V118*/ meltfptr[53] =
			/*_.LET___V87*/ meltfptr[41];;
		      /*^compute */
		      /*_.IFELSE___V82*/ meltfptr[52] =
			/*_.PROGN___V118*/ meltfptr[53];;
		      /*epilog */

		      MELT_LOCATION ("warmelt-normal.melt:1640:/ clear");
		 /*clear *//*_.IFCPP___V83*/ meltfptr[72] = 0;
		      /*^clear */
		 /*clear *//*_.LET___V87*/ meltfptr[41] = 0;
		      /*^clear */
		 /*clear *//*_.PROGN___V118*/ meltfptr[53] = 0;
		    }
		    ;
		  }
		else
		  {		/*^cond.else */

		    /*^block */
		    /*anyblock */
		    {


#if MELT_HAVE_DEBUG
		      MELT_LOCATION ("warmelt-normal.melt:1717:/ cppif.then");
		      /*^block */
		      /*anyblock */
		      {


			{
			  /*^locexp */
			  /*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
			  melt_dbgcounter++;
#endif
			  ;
			}
			;
			/*^checksignal */
			MELT_CHECK_SIGNAL ();
			;
	 /*_#MELT_NEED_DBG__L39*/ meltfnum[17] =
			  /*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
			  ( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
			  0	/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
			  ;;
			MELT_LOCATION ("warmelt-normal.melt:1717:/ cond");
			/*cond */ if ( /*_#MELT_NEED_DBG__L39*/ meltfnum[17])	/*then */
			  {
			    /*^cond.then */
			    /*^block */
			    /*anyblock */
			    {

	   /*_#THE_MELTCALLCOUNT__L40*/ meltfnum[35] =
#ifdef meltcallcount
				meltcallcount	/* the_meltcallcount */
#else
				0L
#endif /* meltcallcount the_meltcallcount */
				;;
			      MELT_LOCATION
				("warmelt-normal.melt:1717:/ checksignal");
			      MELT_CHECK_SIGNAL ();
			      ;
			      /*^apply */
			      /*apply */
			      {
				union meltparam_un argtab[9];
				memset (&argtab, 0, sizeof (argtab));
				/*^apply.arg */
				argtab[0].meltbp_long =
				  /*_#THE_MELTCALLCOUNT__L40*/ meltfnum[35];
				/*^apply.arg */
				argtab[1].meltbp_cstring =
				  "warmelt-normal.melt";
				/*^apply.arg */
				argtab[2].meltbp_long = 1717;
				/*^apply.arg */
				argtab[3].meltbp_cstring =
				  "normexp_symbol before normalize_binding bind=";
				/*^apply.arg */
				argtab[4].meltbp_aptr =
				  (melt_ptr_t *) & /*_.BIND__V13*/
				  meltfptr[12];
				/*^apply.arg */
				argtab[5].meltbp_cstring = " for recv=";
				/*^apply.arg */
				argtab[6].meltbp_aptr =
				  (melt_ptr_t *) & /*_.RECV__V2*/ meltfptr[1];
				/*^apply.arg */
				argtab[7].meltbp_cstring = " psloc=";
				/*^apply.arg */
				argtab[8].meltbp_aptr =
				  (melt_ptr_t *) & /*_.PSLOC__V5*/
				  meltfptr[4];
				/*_.MELT_DEBUG_FUN__V121*/ meltfptr[115] =
				  melt_apply ((meltclosure_ptr_t)
					      (( /*!MELT_DEBUG_FUN */
						meltfrout->tabval[0])),
					      (melt_ptr_t) (( /*nil */ NULL)),
					      (MELTBPARSTR_LONG
					       MELTBPARSTR_CSTRING
					       MELTBPARSTR_LONG
					       MELTBPARSTR_CSTRING
					       MELTBPARSTR_PTR
					       MELTBPARSTR_CSTRING
					       MELTBPARSTR_PTR
					       MELTBPARSTR_CSTRING
					       MELTBPARSTR_PTR ""), argtab,
					      "", (union meltparam_un *) 0);
			      }
			      ;
			      /*_.IF___V120*/ meltfptr[105] =
				/*_.MELT_DEBUG_FUN__V121*/ meltfptr[115];;
			      /*epilog */

			      MELT_LOCATION
				("warmelt-normal.melt:1717:/ clear");
		     /*clear *//*_#THE_MELTCALLCOUNT__L40*/
				meltfnum[35] = 0;
			      /*^clear */
		     /*clear *//*_.MELT_DEBUG_FUN__V121*/
				meltfptr[115] = 0;
			    }
			    ;
			  }
			else
			  {	/*^cond.else */

	  /*_.IF___V120*/ meltfptr[105] = NULL;;
			  }
			;
			MELT_LOCATION
			  ("warmelt-normal.melt:1717:/ quasiblock");


			/*_.PROGN___V122*/ meltfptr[116] =
			  /*_.IF___V120*/ meltfptr[105];;
			/*^compute */
			/*_.IFCPP___V119*/ meltfptr[104] =
			  /*_.PROGN___V122*/ meltfptr[116];;
			/*epilog */

			MELT_LOCATION ("warmelt-normal.melt:1717:/ clear");
		   /*clear *//*_#MELT_NEED_DBG__L39*/ meltfnum[17] = 0;
			/*^clear */
		   /*clear *//*_.IF___V120*/ meltfptr[105] = 0;
			/*^clear */
		   /*clear *//*_.PROGN___V122*/ meltfptr[116] = 0;
		      }

#else /*MELT_HAVE_DEBUG */
		      /*^cppif.else */
		      /*_.IFCPP___V119*/ meltfptr[104] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
		      ;
		      MELT_LOCATION ("warmelt-normal.melt:1718:/ quasiblock");


		      MELT_LOCATION
			("warmelt-normal.melt:1719:/ checksignal");
		      MELT_CHECK_SIGNAL ();
		      ;
		      /*^msend */
		      /*msend */
		      {
			union meltparam_un argtab[4];
			memset (&argtab, 0, sizeof (argtab));
			/*^ojbmsend.arg */
			argtab[0].meltbp_aptr =
			  (melt_ptr_t *) & /*_.ENV__V3*/ meltfptr[2];
			/*^ojbmsend.arg */
			argtab[1].meltbp_aptr =
			  (melt_ptr_t *) & /*_.NCX__V4*/ meltfptr[3];
			/*^ojbmsend.arg */
			argtab[2].meltbp_aptr =
			  (melt_ptr_t *) & /*_.PROCS__V14*/ meltfptr[13];
			/*^ojbmsend.arg */
			argtab[3].meltbp_aptr =
			  (melt_ptr_t *) & /*_.PSLOC__V5*/ meltfptr[4];
			/*_.RESNORMBIND__V124*/ meltfptr[42] =
			  meltgc_send ((melt_ptr_t)
				       ( /*_.BIND__V13*/ meltfptr[12]),
				       (melt_ptr_t) (( /*!NORMALIZE_BINDING */
						      meltfrout->tabval[32])),
				       (MELTBPARSTR_PTR MELTBPARSTR_PTR
					MELTBPARSTR_PTR MELTBPARSTR_PTR ""),
				       argtab, "", (union meltparam_un *) 0);
		      }
		      ;

#if MELT_HAVE_DEBUG
		      MELT_LOCATION ("warmelt-normal.melt:1721:/ cppif.then");
		      /*^block */
		      /*anyblock */
		      {


			{
			  /*^locexp */
			  /*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
			  melt_dbgcounter++;
#endif
			  ;
			}
			;
			/*^checksignal */
			MELT_CHECK_SIGNAL ();
			;
	 /*_#MELT_NEED_DBG__L41*/ meltfnum[13] =
			  /*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
			  ( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
			  0	/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
			  ;;
			MELT_LOCATION ("warmelt-normal.melt:1721:/ cond");
			/*cond */ if ( /*_#MELT_NEED_DBG__L41*/ meltfnum[13])	/*then */
			  {
			    /*^cond.then */
			    /*^block */
			    /*anyblock */
			    {

	   /*_#THE_MELTCALLCOUNT__L42*/ meltfnum[16] =
#ifdef meltcallcount
				meltcallcount	/* the_meltcallcount */
#else
				0L
#endif /* meltcallcount the_meltcallcount */
				;;
			      MELT_LOCATION
				("warmelt-normal.melt:1721:/ checksignal");
			      MELT_CHECK_SIGNAL ();
			      ;
			      /*^apply */
			      /*apply */
			      {
				union meltparam_un argtab[11];
				memset (&argtab, 0, sizeof (argtab));
				/*^apply.arg */
				argtab[0].meltbp_long =
				  /*_#THE_MELTCALLCOUNT__L42*/ meltfnum[16];
				/*^apply.arg */
				argtab[1].meltbp_cstring =
				  "warmelt-normal.melt";
				/*^apply.arg */
				argtab[2].meltbp_long = 1721;
				/*^apply.arg */
				argtab[3].meltbp_cstring =
				  "normexp_symbol after normalize_binding resnormbind=";
				/*^apply.arg */
				argtab[4].meltbp_aptr =
				  (melt_ptr_t *) & /*_.RESNORMBIND__V124*/
				  meltfptr[42];
				/*^apply.arg */
				argtab[5].meltbp_cstring = " for bind=";
				/*^apply.arg */
				argtab[6].meltbp_aptr =
				  (melt_ptr_t *) & /*_.BIND__V13*/
				  meltfptr[12];
				/*^apply.arg */
				argtab[7].meltbp_cstring = " recv=";
				/*^apply.arg */
				argtab[8].meltbp_aptr =
				  (melt_ptr_t *) & /*_.RECV__V2*/ meltfptr[1];
				/*^apply.arg */
				argtab[9].meltbp_cstring = " psloc=";
				/*^apply.arg */
				argtab[10].meltbp_aptr =
				  (melt_ptr_t *) & /*_.PSLOC__V5*/
				  meltfptr[4];
				/*_.MELT_DEBUG_FUN__V127*/ meltfptr[72] =
				  melt_apply ((meltclosure_ptr_t)
					      (( /*!MELT_DEBUG_FUN */
						meltfrout->tabval[0])),
					      (melt_ptr_t) (( /*nil */ NULL)),
					      (MELTBPARSTR_LONG
					       MELTBPARSTR_CSTRING
					       MELTBPARSTR_LONG
					       MELTBPARSTR_CSTRING
					       MELTBPARSTR_PTR
					       MELTBPARSTR_CSTRING
					       MELTBPARSTR_PTR
					       MELTBPARSTR_CSTRING
					       MELTBPARSTR_PTR
					       MELTBPARSTR_CSTRING
					       MELTBPARSTR_PTR ""), argtab,
					      "", (union meltparam_un *) 0);
			      }
			      ;
			      /*_.IF___V126*/ meltfptr[54] =
				/*_.MELT_DEBUG_FUN__V127*/ meltfptr[72];;
			      /*epilog */

			      MELT_LOCATION
				("warmelt-normal.melt:1721:/ clear");
		     /*clear *//*_#THE_MELTCALLCOUNT__L42*/
				meltfnum[16] = 0;
			      /*^clear */
		     /*clear *//*_.MELT_DEBUG_FUN__V127*/
				meltfptr[72] = 0;
			    }
			    ;
			  }
			else
			  {	/*^cond.else */

	  /*_.IF___V126*/ meltfptr[54] = NULL;;
			  }
			;
			MELT_LOCATION
			  ("warmelt-normal.melt:1721:/ quasiblock");


			/*_.PROGN___V128*/ meltfptr[41] =
			  /*_.IF___V126*/ meltfptr[54];;
			/*^compute */
			/*_.IFCPP___V125*/ meltfptr[50] =
			  /*_.PROGN___V128*/ meltfptr[41];;
			/*epilog */

			MELT_LOCATION ("warmelt-normal.melt:1721:/ clear");
		   /*clear *//*_#MELT_NEED_DBG__L41*/ meltfnum[13] = 0;
			/*^clear */
		   /*clear *//*_.IF___V126*/ meltfptr[54] = 0;
			/*^clear */
		   /*clear *//*_.PROGN___V128*/ meltfptr[41] = 0;
		      }

#else /*MELT_HAVE_DEBUG */
		      /*^cppif.else */
		      /*_.IFCPP___V125*/ meltfptr[50] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
		      ;
		      MELT_LOCATION
			("warmelt-normal.melt:1723:/ checksignal");
		      MELT_CHECK_SIGNAL ();
		      ;
		      /*^quasiblock */


		      /*_.RETVAL___V1*/ meltfptr[0] =
			/*_.RESNORMBIND__V124*/ meltfptr[42];;
		      MELT_LOCATION
			("warmelt-normal.melt:1723:/ putxtraresult");
		      if (!meltxrestab_ || !meltxresdescr_)
			goto labend_rout;
		      if (meltxresdescr_[0] != MELTBPAR_PTR)
			goto labend_rout;
		      if (meltxrestab_[0].meltbp_aptr)
			*(meltxrestab_[0].meltbp_aptr) =
			  (melt_ptr_t) (( /*nil */ NULL));
		      ;
		      /*^finalreturn */
		      ;
		      /*finalret */ goto labend_rout;
		      /*_.LET___V123*/ meltfptr[40] =
			/*_.RETURN___V129*/ meltfptr[53];;

		      MELT_LOCATION ("warmelt-normal.melt:1718:/ clear");
		 /*clear *//*_.RESNORMBIND__V124*/ meltfptr[42] = 0;
		      /*^clear */
		 /*clear *//*_.IFCPP___V125*/ meltfptr[50] = 0;
		      /*^clear */
		 /*clear *//*_.RETURN___V129*/ meltfptr[53] = 0;
		      MELT_LOCATION ("warmelt-normal.melt:1716:/ quasiblock");


		      /*_.PROGN___V130*/ meltfptr[115] =
			/*_.LET___V123*/ meltfptr[40];;
		      /*^compute */
		      /*_.IFELSE___V82*/ meltfptr[52] =
			/*_.PROGN___V130*/ meltfptr[115];;
		      /*epilog */

		      MELT_LOCATION ("warmelt-normal.melt:1640:/ clear");
		 /*clear *//*_.IFCPP___V119*/ meltfptr[104] = 0;
		      /*^clear */
		 /*clear *//*_.LET___V123*/ meltfptr[40] = 0;
		      /*^clear */
		 /*clear *//*_.PROGN___V130*/ meltfptr[115] = 0;
		    }
		    ;
		  }
		;
		/*_.IFELSE___V40*/ meltfptr[35] =
		  /*_.IFELSE___V82*/ meltfptr[52];;
		/*epilog */

		MELT_LOCATION ("warmelt-normal.melt:1571:/ clear");
	       /*clear *//*_#IS_LIST__L26*/ meltfnum[8] = 0;
		/*^clear */
	       /*clear *//*_#IF___L27*/ meltfnum[22] = 0;
		/*^clear */
	       /*clear *//*_.IFELSE___V82*/ meltfptr[52] = 0;
	      }
	      ;
	    }
	  ;
	  /*_.IFELSE___V38*/ meltfptr[34] = /*_.IFELSE___V40*/ meltfptr[35];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-normal.melt:1567:/ clear");
	     /*clear *//*_#IS_A__L12*/ meltfnum[10] = 0;
	  /*^clear */
	     /*clear *//*_.IFELSE___V40*/ meltfptr[35] = 0;
	}
	;
      }
    ;
    /*_.LET___V29*/ meltfptr[25] = /*_.IFELSE___V38*/ meltfptr[34];;

    MELT_LOCATION ("warmelt-normal.melt:1561:/ clear");
	   /*clear *//*_.SYCMAP__V30*/ meltfptr[26] = 0;
    /*^clear */
	   /*clear *//*_.SYCA__V31*/ meltfptr[27] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V32*/ meltfptr[31] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V34*/ meltfptr[32] = 0;
    /*^clear */
	   /*clear *//*_.IFELSE___V38*/ meltfptr[34] = 0;
    /*_.LET___V21*/ meltfptr[16] = /*_.LET___V29*/ meltfptr[25];;

    MELT_LOCATION ("warmelt-normal.melt:1548:/ clear");
	   /*clear *//*_.MODCTX__V22*/ meltfptr[21] = 0;
    /*^clear */
	   /*clear *//*_.VALBINDMAP__V23*/ meltfptr[22] = 0;
    /*^clear */
	   /*clear *//*_.VALUELIST__V24*/ meltfptr[23] = 0;
    /*^clear */
	   /*clear *//*_#NULL__L7*/ meltfnum[0] = 0;
    /*^clear */
	   /*clear *//*_#NULL__L8*/ meltfnum[1] = 0;
    /*^clear */
	   /*clear *//*_.IF___V25*/ meltfptr[24] = 0;
    /*^clear */
	   /*clear *//*_.LET___V29*/ meltfptr[25] = 0;
    MELT_LOCATION ("warmelt-normal.melt:1543:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*_.MULTI___V12*/ meltfptr[7] = /*_.LET___V21*/ meltfptr[16];;

    MELT_LOCATION ("warmelt-normal.melt:1543:/ clear");
	   /*clear *//*_.IFCPP___V15*/ meltfptr[14] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V19*/ meltfptr[15] = 0;
    /*^clear */
	   /*clear *//*_.LET___V21*/ meltfptr[16] = 0;

    /*^clear */
	   /*clear *//*_.PROCS__V14*/ meltfptr[13] = 0;
    MELT_LOCATION ("warmelt-normal.melt:1540:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.MULTI___V12*/ meltfptr[7];;

    {
      MELT_LOCATION ("warmelt-normal.melt:1540:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V6*/ meltfptr[5] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V10*/ meltfptr[6] = 0;
    /*^clear */
	   /*clear *//*_.MULTI___V12*/ meltfptr[7] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("NORMEXP_SYMBOL", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_32_warmelt_normal_NORMEXP_SYMBOL_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_32_warmelt_normal_NORMEXP_SYMBOL */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_33_warmelt_normal_LAMBDA___6__ (meltclosure_ptr_t meltclosp_,
					 melt_ptr_t meltfirstargp_,
					 const melt_argdescr_cell_t
					 meltxargdescr_[],
					 union meltparam_un * meltxargtab_,
					 const melt_argdescr_cell_t
					 meltxresdescr_[],
					 union meltparam_un * meltxrestab_)
{
  long current_blocklevel_signals_meltrout_33_warmelt_normal_LAMBDA___6___melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_33_warmelt_normal_LAMBDA___6___st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 11
    melt_ptr_t mcfr_varptr[11];
#define MELTFRAM_NBVARNUM 1
    long mcfr_varnum[1];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_33_warmelt_normal_LAMBDA___6__ is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_33_warmelt_normal_LAMBDA___6___st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 11; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_33_warmelt_normal_LAMBDA___6__ nbval 11*/
  meltfram__.mcfr_nbvar = 11 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("LAMBDA_", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-normal.melt:1616:/ getarg");
 /*_.PR__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-normal.melt:1617:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L1*/ meltfnum[0] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.PR__V2*/ meltfptr[1]),
			     (melt_ptr_t) (( /*!CLASS_NREP_ANYPROC */
					    meltfrout->tabval[0])));;
      MELT_LOCATION ("warmelt-normal.melt:1617:/ cond");
      /*cond */ if ( /*_#IS_A__L1*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V4*/ meltfptr[3] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-normal.melt:1617:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check pr"),
				  ("warmelt-normal.melt")
				  ? ("warmelt-normal.melt") : __FILE__,
				  (1617) ? (1617) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V4*/ meltfptr[3] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V3*/ meltfptr[2] = /*_.IFELSE___V4*/ meltfptr[3];;
      /*epilog */

      MELT_LOCATION ("warmelt-normal.melt:1617:/ clear");
	     /*clear *//*_#IS_A__L1*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V4*/ meltfptr[3] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V3*/ meltfptr[2] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-normal.melt:1618:/ quasiblock");


    MELT_LOCATION ("warmelt-normal.melt:1619:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*^rawallocobj */
    /*rawallocobj */
    {
      melt_ptr_t newobj = 0;
      melt_raw_object_create (newobj,
			      (melt_ptr_t) (( /*!CLASS_REFERENCE */
					     meltfrout->tabval[1])), (1),
			      "CLASS_REFERENCE");
  /*_.INST__V6*/ meltfptr[5] =
	newobj;
    };
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @REFERENCED_VALUE",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V6*/ meltfptr[5])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V6*/ meltfptr[5]), (0),
			  (( /*~FXOCC */ meltfclos->tabval[0])),
			  "REFERENCED_VALUE");
    ;
    /*^touchobj */

    melt_dbgtrace_written_object ( /*_.INST__V6*/ meltfptr[5],
				  "newly made instance");
    ;
    /*_.CLCONT__V5*/ meltfptr[3] = /*_.INST__V6*/ meltfptr[5];;
    MELT_LOCATION ("warmelt-normal.melt:1620:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.PR__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 5, "NRPRO_CONST");
  /*_.CNSTPROC__V7*/ meltfptr[6] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-normal.melt:1623:/ quasiblock");


    /*^newclosure */
		 /*newclosure *//*_.LAMBDA___V9*/ meltfptr[8] =
      (melt_ptr_t)
      meltgc_new_closure ((meltobject_ptr_t)
			  (((melt_ptr_t) (MELT_PREDEF (DISCR_CLOSURE)))),
			  (meltroutine_ptr_t) (( /*!konst_4 */ meltfrout->
						tabval[4])), (2));
    ;
    /*^putclosedv */
    /*putclosv */
    melt_assertmsg ("putclosv checkclo",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.LAMBDA___V9*/ meltfptr[8])) ==
		    MELTOBMAG_CLOSURE);
    melt_assertmsg ("putclosv checkoff", 0 >= 0
		    && 0 <
		    melt_closure_size ((melt_ptr_t)
				       ( /*_.LAMBDA___V9*/ meltfptr[8])));
    ((meltclosure_ptr_t) /*_.LAMBDA___V9*/ meltfptr[8])->tabval[0] =
      (melt_ptr_t) (( /*~FXOCC */ meltfclos->tabval[0]));
    ;
    /*^putclosedv */
    /*putclosv */
    melt_assertmsg ("putclosv checkclo",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.LAMBDA___V9*/ meltfptr[8])) ==
		    MELTOBMAG_CLOSURE);
    melt_assertmsg ("putclosv checkoff", 1 >= 0
		    && 1 <
		    melt_closure_size ((melt_ptr_t)
				       ( /*_.LAMBDA___V9*/ meltfptr[8])));
    ((meltclosure_ptr_t) /*_.LAMBDA___V9*/ meltfptr[8])->tabval[1] =
      (melt_ptr_t) ( /*_.CLCONT__V5*/ meltfptr[3]);
    ;
    /*_.LAMBDA___V8*/ meltfptr[7] = /*_.LAMBDA___V9*/ meltfptr[8];;
    MELT_LOCATION ("warmelt-normal.melt:1621:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.LAMBDA___V8*/ meltfptr[7];
      /*_.LIST_EVERY__V10*/ meltfptr[9] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!LIST_EVERY */ meltfrout->tabval[2])),
		    (melt_ptr_t) ( /*_.CNSTPROC__V7*/ meltfptr[6]),
		    (MELTBPARSTR_PTR ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-normal.melt:1625:/ quasiblock");


    /*^cond */
    /*cond */ if (
		   /*ifisa */
		   melt_is_instance_of ((melt_ptr_t)
					( /*_.CLCONT__V5*/ meltfptr[3]),
					(melt_ptr_t) (( /*!CLASS_REFERENCE */
						       meltfrout->tabval[1])))
      )				/*then */
      {
	/*^cond.then */
	/*^getslot */
	{
	  melt_ptr_t slot = NULL, obj = NULL;
	  obj = (melt_ptr_t) ( /*_.CLCONT__V5*/ meltfptr[3]) /*=obj*/ ;
	  melt_object_get_field (slot, obj, 0, "REFERENCED_VALUE");
   /*_.NEWCL__V11*/ meltfptr[10] = slot;
	};
	;
      }
    else
      {				/*^cond.else */

  /*_.NEWCL__V11*/ meltfptr[10] = NULL;;
      }
    ;
    MELT_LOCATION ("warmelt-normal.melt:1626:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^cond */
    /*cond */ if ( /*_.NEWCL__V11*/ meltfptr[10])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{


	  {
	    /*^locexp */
	    meltgc_append_list ((melt_ptr_t)
				( /*_.CNSTPROC__V7*/ meltfptr[6]),
				(melt_ptr_t) ( /*_.NEWCL__V11*/
					      meltfptr[10]));
	  }
	  ;
	  /*epilog */
	}
	;
      }				/*noelse */
    ;

    MELT_LOCATION ("warmelt-normal.melt:1625:/ clear");
	   /*clear *//*_.NEWCL__V11*/ meltfptr[10] = 0;

    MELT_LOCATION ("warmelt-normal.melt:1618:/ clear");
	   /*clear *//*_.CLCONT__V5*/ meltfptr[3] = 0;
    /*^clear */
	   /*clear *//*_.CNSTPROC__V7*/ meltfptr[6] = 0;
    /*^clear */
	   /*clear *//*_.LAMBDA___V8*/ meltfptr[7] = 0;
    /*^clear */
	   /*clear *//*_.LIST_EVERY__V10*/ meltfptr[9] = 0;
    MELT_LOCATION ("warmelt-normal.melt:1616:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V3*/ meltfptr[2] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("LAMBDA_", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_33_warmelt_normal_LAMBDA___6___melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*noretval */ NULL);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_33_warmelt_normal_LAMBDA___6__ */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_34_warmelt_normal_LAMBDA___7__ (meltclosure_ptr_t meltclosp_,
					 melt_ptr_t meltfirstargp_,
					 const melt_argdescr_cell_t
					 meltxargdescr_[],
					 union meltparam_un * meltxargtab_,
					 const melt_argdescr_cell_t
					 meltxresdescr_[],
					 union meltparam_un * meltxrestab_)
{
  long current_blocklevel_signals_meltrout_34_warmelt_normal_LAMBDA___7___melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_34_warmelt_normal_LAMBDA___7___st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 4
    melt_ptr_t mcfr_varptr[4];
#define MELTFRAM_NBVARNUM 1
    long mcfr_varnum[1];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_34_warmelt_normal_LAMBDA___7__ is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_34_warmelt_normal_LAMBDA___7___st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 4; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_34_warmelt_normal_LAMBDA___7__ nbval 4*/
  meltfram__.mcfr_nbvar = 4 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("LAMBDA_", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-normal.melt:1623:/ getarg");
 /*_.CX__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {

    /*^checksignal */
    MELT_CHECK_SIGNAL ();
    ;
 /*_#__L1*/ meltfnum[0] =
      (( /*_.CX__V2*/ meltfptr[1]) == (( /*~FXOCC */ meltfclos->tabval[0])));;
    MELT_LOCATION ("warmelt-normal.melt:1623:/ cond");
    /*cond */ if ( /*_#__L1*/ meltfnum[0])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-normal.melt:1624:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^cond */
	  /*cond */ if (
			 /*ifisa */
			 melt_is_instance_of ((melt_ptr_t)
					      (( /*~CLCONT */ meltfclos->
						tabval[1])),
					      (melt_ptr_t) (( /*!CLASS_REFERENCE */ meltfrout->tabval[0])))
	    )			/*then */
	    {
	      /*^cond.then */
	      /*^block */
	      /*anyblock */
	      {

		/*^putslot */
		/*putslot */
		melt_assertmsg ("putslot checkobj @REFERENCED_VALUE",
				melt_magic_discr ((melt_ptr_t)
						  (( /*~CLCONT */ meltfclos->
						    tabval[1]))) ==
				MELTOBMAG_OBJECT);
		melt_putfield_object ((( /*~CLCONT */ meltfclos->tabval[1])),
				      (0), (( /*nil */ NULL)),
				      "REFERENCED_VALUE");
		;
		/*^touch */
		meltgc_touch (( /*~CLCONT */ meltfclos->tabval[1]));
		;
		/*^touchobj */

		melt_dbgtrace_written_object (( /*~CLCONT */ meltfclos->
					       tabval[1]), "put-fields");
		;
		/*epilog */
	      }
	      ;
	    }			/*noelse */
	  ;
	  /*^quasiblock */


	  /*_.PROGN___V4*/ meltfptr[3] = ( /*nil */ NULL);;
	  /*^compute */
	  /*_.IF___V3*/ meltfptr[2] = /*_.PROGN___V4*/ meltfptr[3];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-normal.melt:1623:/ clear");
	     /*clear *//*_.PROGN___V4*/ meltfptr[3] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

  /*_.IF___V3*/ meltfptr[2] = NULL;;
      }
    ;
    MELT_LOCATION ("warmelt-normal.melt:1623:/ quasiblock");


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.IF___V3*/ meltfptr[2];;

    {
      MELT_LOCATION ("warmelt-normal.melt:1623:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_#__L1*/ meltfnum[0] = 0;
    /*^clear */
	   /*clear *//*_.IF___V3*/ meltfptr[2] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("LAMBDA_", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_34_warmelt_normal_LAMBDA___7___melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_34_warmelt_normal_LAMBDA___7__ */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_35_warmelt_normal_LAMBDA___8__ (meltclosure_ptr_t meltclosp_,
					 melt_ptr_t meltfirstargp_,
					 const melt_argdescr_cell_t
					 meltxargdescr_[],
					 union meltparam_un * meltxargtab_,
					 const melt_argdescr_cell_t
					 meltxresdescr_[],
					 union meltparam_un * meltxrestab_)
{
  long current_blocklevel_signals_meltrout_35_warmelt_normal_LAMBDA___8___melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_35_warmelt_normal_LAMBDA___8___st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 11
    melt_ptr_t mcfr_varptr[11];
#define MELTFRAM_NBVARNUM 1
    long mcfr_varnum[1];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_35_warmelt_normal_LAMBDA___8__ is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_35_warmelt_normal_LAMBDA___8___st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 11; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_35_warmelt_normal_LAMBDA___8__ nbval 11*/
  meltfram__.mcfr_nbvar = 11 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("LAMBDA_", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-normal.melt:1672:/ getarg");
 /*_.PR__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-normal.melt:1673:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L1*/ meltfnum[0] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.PR__V2*/ meltfptr[1]),
			     (melt_ptr_t) (( /*!CLASS_NREP_ANYPROC */
					    meltfrout->tabval[0])));;
      MELT_LOCATION ("warmelt-normal.melt:1673:/ cond");
      /*cond */ if ( /*_#IS_A__L1*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V4*/ meltfptr[3] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-normal.melt:1673:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check pr"),
				  ("warmelt-normal.melt")
				  ? ("warmelt-normal.melt") : __FILE__,
				  (1673) ? (1673) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V4*/ meltfptr[3] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V3*/ meltfptr[2] = /*_.IFELSE___V4*/ meltfptr[3];;
      /*epilog */

      MELT_LOCATION ("warmelt-normal.melt:1673:/ clear");
	     /*clear *//*_#IS_A__L1*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V4*/ meltfptr[3] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V3*/ meltfptr[2] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-normal.melt:1674:/ quasiblock");


    MELT_LOCATION ("warmelt-normal.melt:1675:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*^rawallocobj */
    /*rawallocobj */
    {
      melt_ptr_t newobj = 0;
      melt_raw_object_create (newobj,
			      (melt_ptr_t) (( /*!CLASS_REFERENCE */
					     meltfrout->tabval[1])), (1),
			      "CLASS_REFERENCE");
  /*_.INST__V6*/ meltfptr[5] =
	newobj;
    };
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @REFERENCED_VALUE",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V6*/ meltfptr[5])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V6*/ meltfptr[5]), (0),
			  (( /*~FXOCC */ meltfclos->tabval[0])),
			  "REFERENCED_VALUE");
    ;
    /*^touchobj */

    melt_dbgtrace_written_object ( /*_.INST__V6*/ meltfptr[5],
				  "newly made instance");
    ;
    /*_.CLCONT__V5*/ meltfptr[3] = /*_.INST__V6*/ meltfptr[5];;
    MELT_LOCATION ("warmelt-normal.melt:1676:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.PR__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 5, "NRPRO_CONST");
  /*_.CNSTPROC__V7*/ meltfptr[6] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-normal.melt:1679:/ quasiblock");


    /*^newclosure */
		 /*newclosure *//*_.LAMBDA___V9*/ meltfptr[8] =
      (melt_ptr_t)
      meltgc_new_closure ((meltobject_ptr_t)
			  (((melt_ptr_t) (MELT_PREDEF (DISCR_CLOSURE)))),
			  (meltroutine_ptr_t) (( /*!konst_4 */ meltfrout->
						tabval[4])), (2));
    ;
    /*^putclosedv */
    /*putclosv */
    melt_assertmsg ("putclosv checkclo",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.LAMBDA___V9*/ meltfptr[8])) ==
		    MELTOBMAG_CLOSURE);
    melt_assertmsg ("putclosv checkoff", 0 >= 0
		    && 0 <
		    melt_closure_size ((melt_ptr_t)
				       ( /*_.LAMBDA___V9*/ meltfptr[8])));
    ((meltclosure_ptr_t) /*_.LAMBDA___V9*/ meltfptr[8])->tabval[0] =
      (melt_ptr_t) (( /*~FXOCC */ meltfclos->tabval[0]));
    ;
    /*^putclosedv */
    /*putclosv */
    melt_assertmsg ("putclosv checkclo",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.LAMBDA___V9*/ meltfptr[8])) ==
		    MELTOBMAG_CLOSURE);
    melt_assertmsg ("putclosv checkoff", 1 >= 0
		    && 1 <
		    melt_closure_size ((melt_ptr_t)
				       ( /*_.LAMBDA___V9*/ meltfptr[8])));
    ((meltclosure_ptr_t) /*_.LAMBDA___V9*/ meltfptr[8])->tabval[1] =
      (melt_ptr_t) ( /*_.CLCONT__V5*/ meltfptr[3]);
    ;
    /*_.LAMBDA___V8*/ meltfptr[7] = /*_.LAMBDA___V9*/ meltfptr[8];;
    MELT_LOCATION ("warmelt-normal.melt:1677:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.LAMBDA___V8*/ meltfptr[7];
      /*_.LIST_EVERY__V10*/ meltfptr[9] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!LIST_EVERY */ meltfrout->tabval[2])),
		    (melt_ptr_t) ( /*_.CNSTPROC__V7*/ meltfptr[6]),
		    (MELTBPARSTR_PTR ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-normal.melt:1682:/ quasiblock");


    /*^cond */
    /*cond */ if (
		   /*ifisa */
		   melt_is_instance_of ((melt_ptr_t)
					( /*_.CLCONT__V5*/ meltfptr[3]),
					(melt_ptr_t) (( /*!CLASS_REFERENCE */
						       meltfrout->tabval[1])))
      )				/*then */
      {
	/*^cond.then */
	/*^getslot */
	{
	  melt_ptr_t slot = NULL, obj = NULL;
	  obj = (melt_ptr_t) ( /*_.CLCONT__V5*/ meltfptr[3]) /*=obj*/ ;
	  melt_object_get_field (slot, obj, 0, "REFERENCED_VALUE");
   /*_.NEWCL__V11*/ meltfptr[10] = slot;
	};
	;
      }
    else
      {				/*^cond.else */

  /*_.NEWCL__V11*/ meltfptr[10] = NULL;;
      }
    ;
    MELT_LOCATION ("warmelt-normal.melt:1683:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^cond */
    /*cond */ if ( /*_.NEWCL__V11*/ meltfptr[10])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{


	  {
	    /*^locexp */
	    meltgc_append_list ((melt_ptr_t)
				( /*_.CNSTPROC__V7*/ meltfptr[6]),
				(melt_ptr_t) ( /*_.NEWCL__V11*/
					      meltfptr[10]));
	  }
	  ;
	  /*epilog */
	}
	;
      }				/*noelse */
    ;

    MELT_LOCATION ("warmelt-normal.melt:1682:/ clear");
	   /*clear *//*_.NEWCL__V11*/ meltfptr[10] = 0;

    MELT_LOCATION ("warmelt-normal.melt:1674:/ clear");
	   /*clear *//*_.CLCONT__V5*/ meltfptr[3] = 0;
    /*^clear */
	   /*clear *//*_.CNSTPROC__V7*/ meltfptr[6] = 0;
    /*^clear */
	   /*clear *//*_.LAMBDA___V8*/ meltfptr[7] = 0;
    /*^clear */
	   /*clear *//*_.LIST_EVERY__V10*/ meltfptr[9] = 0;
    MELT_LOCATION ("warmelt-normal.melt:1672:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V3*/ meltfptr[2] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("LAMBDA_", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_35_warmelt_normal_LAMBDA___8___melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*noretval */ NULL);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_35_warmelt_normal_LAMBDA___8__ */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_36_warmelt_normal_LAMBDA___9__ (meltclosure_ptr_t meltclosp_,
					 melt_ptr_t meltfirstargp_,
					 const melt_argdescr_cell_t
					 meltxargdescr_[],
					 union meltparam_un * meltxargtab_,
					 const melt_argdescr_cell_t
					 meltxresdescr_[],
					 union meltparam_un * meltxrestab_)
{
  long current_blocklevel_signals_meltrout_36_warmelt_normal_LAMBDA___9___melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_36_warmelt_normal_LAMBDA___9___st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 4
    melt_ptr_t mcfr_varptr[4];
#define MELTFRAM_NBVARNUM 1
    long mcfr_varnum[1];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_36_warmelt_normal_LAMBDA___9__ is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_36_warmelt_normal_LAMBDA___9___st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 4; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_36_warmelt_normal_LAMBDA___9__ nbval 4*/
  meltfram__.mcfr_nbvar = 4 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("LAMBDA_", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-normal.melt:1679:/ getarg");
 /*_.CX__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {

    /*^checksignal */
    MELT_CHECK_SIGNAL ();
    ;
 /*_#__L1*/ meltfnum[0] =
      (( /*_.CX__V2*/ meltfptr[1]) == (( /*~FXOCC */ meltfclos->tabval[0])));;
    MELT_LOCATION ("warmelt-normal.melt:1679:/ cond");
    /*cond */ if ( /*_#__L1*/ meltfnum[0])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-normal.melt:1681:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^cond */
	  /*cond */ if (
			 /*ifisa */
			 melt_is_instance_of ((melt_ptr_t)
					      (( /*~CLCONT */ meltfclos->
						tabval[1])),
					      (melt_ptr_t) (( /*!CLASS_REFERENCE */ meltfrout->tabval[0])))
	    )			/*then */
	    {
	      /*^cond.then */
	      /*^block */
	      /*anyblock */
	      {

		/*^putslot */
		/*putslot */
		melt_assertmsg ("putslot checkobj @REFERENCED_VALUE",
				melt_magic_discr ((melt_ptr_t)
						  (( /*~CLCONT */ meltfclos->
						    tabval[1]))) ==
				MELTOBMAG_OBJECT);
		melt_putfield_object ((( /*~CLCONT */ meltfclos->tabval[1])),
				      (0), (( /*nil */ NULL)),
				      "REFERENCED_VALUE");
		;
		/*^touch */
		meltgc_touch (( /*~CLCONT */ meltfclos->tabval[1]));
		;
		/*^touchobj */

		melt_dbgtrace_written_object (( /*~CLCONT */ meltfclos->
					       tabval[1]), "put-fields");
		;
		/*epilog */
	      }
	      ;
	    }			/*noelse */
	  ;
	  MELT_LOCATION ("warmelt-normal.melt:1680:/ quasiblock");


	  /*_.PROGN___V4*/ meltfptr[3] = ( /*nil */ NULL);;
	  /*^compute */
	  /*_.IF___V3*/ meltfptr[2] = /*_.PROGN___V4*/ meltfptr[3];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-normal.melt:1679:/ clear");
	     /*clear *//*_.PROGN___V4*/ meltfptr[3] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

  /*_.IF___V3*/ meltfptr[2] = NULL;;
      }
    ;
    MELT_LOCATION ("warmelt-normal.melt:1679:/ quasiblock");


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.IF___V3*/ meltfptr[2];;

    {
      MELT_LOCATION ("warmelt-normal.melt:1679:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_#__L1*/ meltfnum[0] = 0;
    /*^clear */
	   /*clear *//*_.IF___V3*/ meltfptr[2] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("LAMBDA_", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_36_warmelt_normal_LAMBDA___9___melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_36_warmelt_normal_LAMBDA___9__ */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_37_warmelt_normal_LAMBDA___10__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un * meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un * meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_37_warmelt_normal_LAMBDA___10___melt =
    melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_37_warmelt_normal_LAMBDA___10___st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 11
    melt_ptr_t mcfr_varptr[11];
#define MELTFRAM_NBVARNUM 1
    long mcfr_varnum[1];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_37_warmelt_normal_LAMBDA___10__ is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_37_warmelt_normal_LAMBDA___10___st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 11; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_37_warmelt_normal_LAMBDA___10__ nbval 11*/
  meltfram__.mcfr_nbvar = 11 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("LAMBDA_", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-normal.melt:1701:/ getarg");
 /*_.PR__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-normal.melt:1702:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L1*/ meltfnum[0] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.PR__V2*/ meltfptr[1]),
			     (melt_ptr_t) (( /*!CLASS_NREP_ANYPROC */
					    meltfrout->tabval[0])));;
      MELT_LOCATION ("warmelt-normal.melt:1702:/ cond");
      /*cond */ if ( /*_#IS_A__L1*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V4*/ meltfptr[3] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-normal.melt:1702:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check pr"),
				  ("warmelt-normal.melt")
				  ? ("warmelt-normal.melt") : __FILE__,
				  (1702) ? (1702) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V4*/ meltfptr[3] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V3*/ meltfptr[2] = /*_.IFELSE___V4*/ meltfptr[3];;
      /*epilog */

      MELT_LOCATION ("warmelt-normal.melt:1702:/ clear");
	     /*clear *//*_#IS_A__L1*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V4*/ meltfptr[3] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V3*/ meltfptr[2] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-normal.melt:1703:/ quasiblock");


    /*^checksignal */
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*^rawallocobj */
    /*rawallocobj */
    {
      melt_ptr_t newobj = 0;
      melt_raw_object_create (newobj,
			      (melt_ptr_t) (( /*!CLASS_REFERENCE */
					     meltfrout->tabval[1])), (1),
			      "CLASS_REFERENCE");
  /*_.INST__V6*/ meltfptr[5] =
	newobj;
    };
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @REFERENCED_VALUE",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V6*/ meltfptr[5])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V6*/ meltfptr[5]), (0),
			  (( /*~CLOCC */ meltfclos->tabval[0])),
			  "REFERENCED_VALUE");
    ;
    /*^touchobj */

    melt_dbgtrace_written_object ( /*_.INST__V6*/ meltfptr[5],
				  "newly made instance");
    ;
    /*_.CLCONT__V5*/ meltfptr[3] = /*_.INST__V6*/ meltfptr[5];;
    MELT_LOCATION ("warmelt-normal.melt:1704:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.PR__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 4, "NRPRO_CLOSEDB");
  /*_.CLOBINDL__V7*/ meltfptr[6] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-normal.melt:1707:/ quasiblock");


    /*^newclosure */
		 /*newclosure *//*_.LAMBDA___V9*/ meltfptr[8] =
      (melt_ptr_t)
      meltgc_new_closure ((meltobject_ptr_t)
			  (((melt_ptr_t) (MELT_PREDEF (DISCR_CLOSURE)))),
			  (meltroutine_ptr_t) (( /*!konst_4 */ meltfrout->
						tabval[4])), (2));
    ;
    /*^putclosedv */
    /*putclosv */
    melt_assertmsg ("putclosv checkclo",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.LAMBDA___V9*/ meltfptr[8])) ==
		    MELTOBMAG_CLOSURE);
    melt_assertmsg ("putclosv checkoff", 0 >= 0
		    && 0 <
		    melt_closure_size ((melt_ptr_t)
				       ( /*_.LAMBDA___V9*/ meltfptr[8])));
    ((meltclosure_ptr_t) /*_.LAMBDA___V9*/ meltfptr[8])->tabval[0] =
      (melt_ptr_t) (( /*~BIND */ meltfclos->tabval[1]));
    ;
    /*^putclosedv */
    /*putclosv */
    melt_assertmsg ("putclosv checkclo",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.LAMBDA___V9*/ meltfptr[8])) ==
		    MELTOBMAG_CLOSURE);
    melt_assertmsg ("putclosv checkoff", 1 >= 0
		    && 1 <
		    melt_closure_size ((melt_ptr_t)
				       ( /*_.LAMBDA___V9*/ meltfptr[8])));
    ((meltclosure_ptr_t) /*_.LAMBDA___V9*/ meltfptr[8])->tabval[1] =
      (melt_ptr_t) ( /*_.CLCONT__V5*/ meltfptr[3]);
    ;
    /*_.LAMBDA___V8*/ meltfptr[7] = /*_.LAMBDA___V9*/ meltfptr[8];;
    MELT_LOCATION ("warmelt-normal.melt:1705:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.LAMBDA___V8*/ meltfptr[7];
      /*_.LIST_EVERY__V10*/ meltfptr[9] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!LIST_EVERY */ meltfrout->tabval[2])),
		    (melt_ptr_t) ( /*_.CLOBINDL__V7*/ meltfptr[6]),
		    (MELTBPARSTR_PTR ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-normal.melt:1709:/ quasiblock");


    /*^cond */
    /*cond */ if (
		   /*ifisa */
		   melt_is_instance_of ((melt_ptr_t)
					( /*_.CLCONT__V5*/ meltfptr[3]),
					(melt_ptr_t) (( /*!CLASS_REFERENCE */
						       meltfrout->tabval[1])))
      )				/*then */
      {
	/*^cond.then */
	/*^getslot */
	{
	  melt_ptr_t slot = NULL, obj = NULL;
	  obj = (melt_ptr_t) ( /*_.CLCONT__V5*/ meltfptr[3]) /*=obj*/ ;
	  melt_object_get_field (slot, obj, 0, "REFERENCED_VALUE");
   /*_.NEWCL__V11*/ meltfptr[10] = slot;
	};
	;
      }
    else
      {				/*^cond.else */

  /*_.NEWCL__V11*/ meltfptr[10] = NULL;;
      }
    ;
    MELT_LOCATION ("warmelt-normal.melt:1710:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^cond */
    /*cond */ if ( /*_.NEWCL__V11*/ meltfptr[10])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{


	  {
	    /*^locexp */
	    meltgc_append_list ((melt_ptr_t)
				( /*_.CLOBINDL__V7*/ meltfptr[6]),
				(melt_ptr_t) (( /*~BIND */ meltfclos->
					       tabval[1])));
	  }
	  ;
	  /*epilog */
	}
	;
      }				/*noelse */
    ;

    MELT_LOCATION ("warmelt-normal.melt:1709:/ clear");
	   /*clear *//*_.NEWCL__V11*/ meltfptr[10] = 0;

    MELT_LOCATION ("warmelt-normal.melt:1703:/ clear");
	   /*clear *//*_.CLCONT__V5*/ meltfptr[3] = 0;
    /*^clear */
	   /*clear *//*_.CLOBINDL__V7*/ meltfptr[6] = 0;
    /*^clear */
	   /*clear *//*_.LAMBDA___V8*/ meltfptr[7] = 0;
    /*^clear */
	   /*clear *//*_.LIST_EVERY__V10*/ meltfptr[9] = 0;
    MELT_LOCATION ("warmelt-normal.melt:1701:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V3*/ meltfptr[2] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("LAMBDA_", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_37_warmelt_normal_LAMBDA___10___melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*noretval */ NULL);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_37_warmelt_normal_LAMBDA___10__ */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_38_warmelt_normal_LAMBDA___11__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un * meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un * meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_38_warmelt_normal_LAMBDA___11___melt =
    melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_38_warmelt_normal_LAMBDA___11___st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 4
    melt_ptr_t mcfr_varptr[4];
#define MELTFRAM_NBVARNUM 1
    long mcfr_varnum[1];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_38_warmelt_normal_LAMBDA___11__ is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_38_warmelt_normal_LAMBDA___11___st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 4; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_38_warmelt_normal_LAMBDA___11__ nbval 4*/
  meltfram__.mcfr_nbvar = 4 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("LAMBDA_", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-normal.melt:1707:/ getarg");
 /*_.CLBND__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {

    /*^checksignal */
    MELT_CHECK_SIGNAL ();
    ;
 /*_#__L1*/ meltfnum[0] =
      (( /*_.CLBND__V2*/ meltfptr[1]) ==
       (( /*~BIND */ meltfclos->tabval[0])));;
    MELT_LOCATION ("warmelt-normal.melt:1707:/ cond");
    /*cond */ if ( /*_#__L1*/ meltfnum[0])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-normal.melt:1708:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^cond */
	  /*cond */ if (
			 /*ifisa */
			 melt_is_instance_of ((melt_ptr_t)
					      (( /*~CLCONT */ meltfclos->
						tabval[1])),
					      (melt_ptr_t) (( /*!CLASS_REFERENCE */ meltfrout->tabval[0])))
	    )			/*then */
	    {
	      /*^cond.then */
	      /*^block */
	      /*anyblock */
	      {

		/*^putslot */
		/*putslot */
		melt_assertmsg ("putslot checkobj @REFERENCED_VALUE",
				melt_magic_discr ((melt_ptr_t)
						  (( /*~CLCONT */ meltfclos->
						    tabval[1]))) ==
				MELTOBMAG_OBJECT);
		melt_putfield_object ((( /*~CLCONT */ meltfclos->tabval[1])),
				      (0), (( /*nil */ NULL)),
				      "REFERENCED_VALUE");
		;
		/*^touch */
		meltgc_touch (( /*~CLCONT */ meltfclos->tabval[1]));
		;
		/*^touchobj */

		melt_dbgtrace_written_object (( /*~CLCONT */ meltfclos->
					       tabval[1]), "put-fields");
		;
		/*epilog */
	      }
	      ;
	    }			/*noelse */
	  ;
	  /*^quasiblock */


	  /*_.PROGN___V4*/ meltfptr[3] = ( /*nil */ NULL);;
	  /*^compute */
	  /*_.IF___V3*/ meltfptr[2] = /*_.PROGN___V4*/ meltfptr[3];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-normal.melt:1707:/ clear");
	     /*clear *//*_.PROGN___V4*/ meltfptr[3] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

  /*_.IF___V3*/ meltfptr[2] = NULL;;
      }
    ;
    MELT_LOCATION ("warmelt-normal.melt:1707:/ quasiblock");


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.IF___V3*/ meltfptr[2];;

    {
      MELT_LOCATION ("warmelt-normal.melt:1707:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_#__L1*/ meltfnum[0] = 0;
    /*^clear */
	   /*clear *//*_.IF___V3*/ meltfptr[2] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("LAMBDA_", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_38_warmelt_normal_LAMBDA___11___melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_38_warmelt_normal_LAMBDA___11__ */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_39_warmelt_normal_GECTYP_SYMOCC (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un * meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un * meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_39_warmelt_normal_GECTYP_SYMOCC_melt =
    melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_39_warmelt_normal_GECTYP_SYMOCC_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 6
    melt_ptr_t mcfr_varptr[6];
#define MELTFRAM_NBVARNUM 1
    long mcfr_varnum[1];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_39_warmelt_normal_GECTYP_SYMOCC is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_39_warmelt_normal_GECTYP_SYMOCC_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 6; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_39_warmelt_normal_GECTYP_SYMOCC nbval 6*/
  meltfram__.mcfr_nbvar = 6 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("GECTYP_SYMOCC", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-normal.melt:1730:/ getarg");
 /*_.RECV__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.ENV__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.ENV__V3*/ meltfptr[2])) != NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-normal.melt:1731:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L1*/ meltfnum[0] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.RECV__V2*/ meltfptr[1]),
			     (melt_ptr_t) (( /*!CLASS_NREP_SYMOCC */
					    meltfrout->tabval[0])));;
      MELT_LOCATION ("warmelt-normal.melt:1731:/ cond");
      /*cond */ if ( /*_#IS_A__L1*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V5*/ meltfptr[4] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-normal.melt:1731:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check recv"),
				  ("warmelt-normal.melt")
				  ? ("warmelt-normal.melt") : __FILE__,
				  (1731) ? (1731) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V5*/ meltfptr[4] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V4*/ meltfptr[3] = /*_.IFELSE___V5*/ meltfptr[4];;
      /*epilog */

      MELT_LOCATION ("warmelt-normal.melt:1731:/ clear");
	     /*clear *//*_#IS_A__L1*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V5*/ meltfptr[4] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V4*/ meltfptr[3] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-normal.melt:1732:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.RECV__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 2, "NOCC_CTYP");
  /*_.NOCC_CTYP__V6*/ meltfptr[4] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-normal.melt:1730:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.NOCC_CTYP__V6*/ meltfptr[4];;

    {
      MELT_LOCATION ("warmelt-normal.melt:1730:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V4*/ meltfptr[3] = 0;
    /*^clear */
	   /*clear *//*_.NOCC_CTYP__V6*/ meltfptr[4] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("GECTYP_SYMOCC", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_39_warmelt_normal_GECTYP_SYMOCC_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_39_warmelt_normal_GECTYP_SYMOCC */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_40_warmelt_normal_NORMEXP_CLASS (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un * meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un * meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_40_warmelt_normal_NORMEXP_CLASS_melt =
    melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_40_warmelt_normal_NORMEXP_CLASS_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 54
    melt_ptr_t mcfr_varptr[54];
#define MELTFRAM_NBVARNUM 20
    long mcfr_varnum[20];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_40_warmelt_normal_NORMEXP_CLASS is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_40_warmelt_normal_NORMEXP_CLASS_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 54; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_40_warmelt_normal_NORMEXP_CLASS nbval 54*/
  meltfram__.mcfr_nbvar = 54 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("NORMEXP_CLASS", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-normal.melt:1740:/ getarg");
 /*_.RECV__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.ENV__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.ENV__V3*/ meltfptr[2])) != NULL);


  /*getarg#2 */
  /*^getarg */
  if (meltxargdescr_[1] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.NCX__V4*/ meltfptr[3] =
    (meltxargtab_[1].meltbp_aptr) ? (*(meltxargtab_[1].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.NCX__V4*/ meltfptr[3])) != NULL);


  /*getarg#3 */
  /*^getarg */
  if (meltxargdescr_[2] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.PSLOC__V5*/ meltfptr[4] =
    (meltxargtab_[2].meltbp_aptr) ? (*(meltxargtab_[2].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.PSLOC__V5*/ meltfptr[4])) !=
	      NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-normal.melt:1741:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L1*/ meltfnum[0] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-normal.melt:1741:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L1*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-normal.melt:1741:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-normal.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 1741;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "normexp_class recv";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.RECV__V2*/ meltfptr[1];
	      /*_.MELT_DEBUG_FUN__V8*/ meltfptr[7] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V7*/ meltfptr[6] = /*_.MELT_DEBUG_FUN__V8*/ meltfptr[7];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-normal.melt:1741:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V8*/ meltfptr[7] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V7*/ meltfptr[6] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-normal.melt:1741:/ quasiblock");


      /*_.PROGN___V9*/ meltfptr[7] = /*_.IF___V7*/ meltfptr[6];;
      /*^compute */
      /*_.IFCPP___V6*/ meltfptr[5] = /*_.PROGN___V9*/ meltfptr[7];;
      /*epilog */

      MELT_LOCATION ("warmelt-normal.melt:1741:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L1*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IF___V7*/ meltfptr[6] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V9*/ meltfptr[7] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V6*/ meltfptr[5] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-normal.melt:1742:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L3*/ meltfnum[1] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.RECV__V2*/ meltfptr[1]),
			     (melt_ptr_t) (( /*!CLASS_CLASS */ meltfrout->
					    tabval[1])));;
      MELT_LOCATION ("warmelt-normal.melt:1742:/ cond");
      /*cond */ if ( /*_#IS_A__L3*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V11*/ meltfptr[7] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-normal.melt:1742:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check recv"),
				  ("warmelt-normal.melt")
				  ? ("warmelt-normal.melt") : __FILE__,
				  (1742) ? (1742) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V11*/ meltfptr[7] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V10*/ meltfptr[6] = /*_.IFELSE___V11*/ meltfptr[7];;
      /*epilog */

      MELT_LOCATION ("warmelt-normal.melt:1742:/ clear");
	     /*clear *//*_#IS_A__L3*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V11*/ meltfptr[7] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V10*/ meltfptr[6] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-normal.melt:1743:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L4*/ meltfnum[0] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.ENV__V3*/ meltfptr[2]),
			     (melt_ptr_t) (( /*!CLASS_ENVIRONMENT */
					    meltfrout->tabval[2])));;
      MELT_LOCATION ("warmelt-normal.melt:1743:/ cond");
      /*cond */ if ( /*_#IS_A__L4*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V13*/ meltfptr[12] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-normal.melt:1743:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check env"),
				  ("warmelt-normal.melt")
				  ? ("warmelt-normal.melt") : __FILE__,
				  (1743) ? (1743) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V13*/ meltfptr[12] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V12*/ meltfptr[7] = /*_.IFELSE___V13*/ meltfptr[12];;
      /*epilog */

      MELT_LOCATION ("warmelt-normal.melt:1743:/ clear");
	     /*clear *//*_#IS_A__L4*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V13*/ meltfptr[12] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V12*/ meltfptr[7] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-normal.melt:1744:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L5*/ meltfnum[1] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.NCX__V4*/ meltfptr[3]),
			     (melt_ptr_t) (( /*!CLASS_NORMALIZATION_CONTEXT */
					    meltfrout->tabval[3])));;
      MELT_LOCATION ("warmelt-normal.melt:1744:/ cond");
      /*cond */ if ( /*_#IS_A__L5*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V15*/ meltfptr[14] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-normal.melt:1744:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check nctxt"),
				  ("warmelt-normal.melt")
				  ? ("warmelt-normal.melt") : __FILE__,
				  (1744) ? (1744) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V15*/ meltfptr[14] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V14*/ meltfptr[12] = /*_.IFELSE___V15*/ meltfptr[14];;
      /*epilog */

      MELT_LOCATION ("warmelt-normal.melt:1744:/ clear");
	     /*clear *//*_#IS_A__L5*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V15*/ meltfptr[14] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V14*/ meltfptr[12] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-normal.melt:1745:/ quasiblock");


    /*^getslot */
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.RECV__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 1, "NAMED_NAME");
  /*_.NAMED_NAME__V17*/ meltfptr[16] = slot;
    };
    ;
 /*_.CLASYMB__V18*/ meltfptr[17] =
      meltgc_named_symbol (melt_string_str
			   ((melt_ptr_t)
			    ( /*_.NAMED_NAME__V17*/ meltfptr[16])),
			   MELT_GET);;
    MELT_LOCATION ("warmelt-normal.melt:1746:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & /*_.CLASYMB__V18*/ meltfptr[17];
      /*_.CLABIND__V19*/ meltfptr[18] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!FIND_ENV */ meltfrout->tabval[4])),
		    (melt_ptr_t) ( /*_.ENV__V3*/ meltfptr[2]),
		    (MELTBPARSTR_PTR ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-normal.melt:1748:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L6*/ meltfnum[0] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-normal.melt:1748:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L6*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L7*/ meltfnum[1] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-normal.melt:1748:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L7*/ meltfnum[1];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-normal.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 1748;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "normexp_class clabind";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.CLABIND__V19*/ meltfptr[18];
	      /*_.MELT_DEBUG_FUN__V22*/ meltfptr[21] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V21*/ meltfptr[20] =
	      /*_.MELT_DEBUG_FUN__V22*/ meltfptr[21];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-normal.melt:1748:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L7*/ meltfnum[1] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V22*/ meltfptr[21] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V21*/ meltfptr[20] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-normal.melt:1748:/ quasiblock");


      /*_.PROGN___V23*/ meltfptr[21] = /*_.IF___V21*/ meltfptr[20];;
      /*^compute */
      /*_.IFCPP___V20*/ meltfptr[19] = /*_.PROGN___V23*/ meltfptr[21];;
      /*epilog */

      MELT_LOCATION ("warmelt-normal.melt:1748:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L6*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IF___V21*/ meltfptr[20] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V23*/ meltfptr[21] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V20*/ meltfptr[19] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-normal.melt:1749:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#IS_A__L8*/ meltfnum[1] =
      melt_is_instance_of ((melt_ptr_t) ( /*_.CLABIND__V19*/ meltfptr[18]),
			   (melt_ptr_t) (( /*!CLASS_CLASS_BINDING */
					  meltfrout->tabval[5])));;
    MELT_LOCATION ("warmelt-normal.melt:1749:/ cond");
    /*cond */ if ( /*_#IS_A__L8*/ meltfnum[1])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-normal.melt:1750:/ quasiblock");


	  /*^checksignal */
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^apply */
	  /*apply */
	  {
	    union meltparam_un argtab[3];
	    memset (&argtab, 0, sizeof (argtab));
	    /*^apply.arg */
	    argtab[0].meltbp_aptr =
	      (melt_ptr_t *) & /*_.ENV__V3*/ meltfptr[2];
	    /*^apply.arg */
	    argtab[1].meltbp_aptr =
	      (melt_ptr_t *) & /*_.NCX__V4*/ meltfptr[3];
	    /*^apply.arg */
	    argtab[2].meltbp_aptr =
	      (melt_ptr_t *) & /*_.PSLOC__V5*/ meltfptr[4];
	    /*_.NORMCLA__V26*/ meltfptr[25] =
	      melt_apply ((meltclosure_ptr_t)
			  (( /*!NORMEXP_SYMBOL */ meltfrout->tabval[6])),
			  (melt_ptr_t) ( /*_.CLASYMB__V18*/ meltfptr[17]),
			  (MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_PTR
			   ""), argtab, "", (union meltparam_un *) 0);
	  }
	  ;

#if MELT_HAVE_DEBUG
	  MELT_LOCATION ("warmelt-normal.melt:1751:/ cppif.then");
	  /*^block */
	  /*anyblock */
	  {


	    {
	      /*^locexp */
	      /*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	      melt_dbgcounter++;
#endif
	      ;
	    }
	    ;
	    /*^checksignal */
	    MELT_CHECK_SIGNAL ();
	    ;
     /*_#MELT_NEED_DBG__L9*/ meltfnum[0] =
	      /*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	      ( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	      0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	      ;;
	    MELT_LOCATION ("warmelt-normal.melt:1751:/ cond");
	    /*cond */ if ( /*_#MELT_NEED_DBG__L9*/ meltfnum[0])	/*then */
	      {
		/*^cond.then */
		/*^block */
		/*anyblock */
		{

       /*_#THE_MELTCALLCOUNT__L10*/ meltfnum[9] =
#ifdef meltcallcount
		    meltcallcount	/* the_meltcallcount */
#else
		    0L
#endif /* meltcallcount the_meltcallcount */
		    ;;
		  MELT_LOCATION ("warmelt-normal.melt:1751:/ checksignal");
		  MELT_CHECK_SIGNAL ();
		  ;
		  /*^apply */
		  /*apply */
		  {
		    union meltparam_un argtab[5];
		    memset (&argtab, 0, sizeof (argtab));
		    /*^apply.arg */
		    argtab[0].meltbp_long =
		      /*_#THE_MELTCALLCOUNT__L10*/ meltfnum[9];
		    /*^apply.arg */
		    argtab[1].meltbp_cstring = "warmelt-normal.melt";
		    /*^apply.arg */
		    argtab[2].meltbp_long = 1751;
		    /*^apply.arg */
		    argtab[3].meltbp_cstring =
		      "normexp_class normcla class data inst";
		    /*^apply.arg */
		    argtab[4].meltbp_aptr =
		      (melt_ptr_t *) & /*_.NORMCLA__V26*/ meltfptr[25];
		    /*_.MELT_DEBUG_FUN__V29*/ meltfptr[28] =
		      melt_apply ((meltclosure_ptr_t)
				  (( /*!MELT_DEBUG_FUN */ meltfrout->
				    tabval[0])),
				  (melt_ptr_t) (( /*nil */ NULL)),
				  (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_PTR ""), argtab, "",
				  (union meltparam_un *) 0);
		  }
		  ;
		  /*_.IF___V28*/ meltfptr[27] =
		    /*_.MELT_DEBUG_FUN__V29*/ meltfptr[28];;
		  /*epilog */

		  MELT_LOCATION ("warmelt-normal.melt:1751:/ clear");
		 /*clear *//*_#THE_MELTCALLCOUNT__L10*/ meltfnum[9] = 0;
		  /*^clear */
		 /*clear *//*_.MELT_DEBUG_FUN__V29*/ meltfptr[28] = 0;
		}
		;
	      }
	    else
	      {			/*^cond.else */

      /*_.IF___V28*/ meltfptr[27] = NULL;;
	      }
	    ;
	    MELT_LOCATION ("warmelt-normal.melt:1751:/ quasiblock");


	    /*_.PROGN___V30*/ meltfptr[28] = /*_.IF___V28*/ meltfptr[27];;
	    /*^compute */
	    /*_.IFCPP___V27*/ meltfptr[26] = /*_.PROGN___V30*/ meltfptr[28];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-normal.melt:1751:/ clear");
	       /*clear *//*_#MELT_NEED_DBG__L9*/ meltfnum[0] = 0;
	    /*^clear */
	       /*clear *//*_.IF___V28*/ meltfptr[27] = 0;
	    /*^clear */
	       /*clear *//*_.PROGN___V30*/ meltfptr[28] = 0;
	  }

#else /*MELT_HAVE_DEBUG */
	  /*^cppif.else */
	  /*_.IFCPP___V27*/ meltfptr[26] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
	  ;

#if MELT_HAVE_DEBUG
	  MELT_LOCATION ("warmelt-normal.melt:1752:/ cppif.then");
	  /*^block */
	  /*anyblock */
	  {

	    /*^checksignal */
	    MELT_CHECK_SIGNAL ();
	    ;
     /*_#IS_A__L11*/ meltfnum[9] =
	      melt_is_instance_of ((melt_ptr_t)
				   ( /*_.NORMCLA__V26*/ meltfptr[25]),
				   (melt_ptr_t) (( /*!CLASS_NREP_DATAINSTANCE */ meltfrout->tabval[8])));;
	    MELT_LOCATION ("warmelt-normal.melt:1753:/ cond");
	    /*cond */ if ( /*_#IS_A__L11*/ meltfnum[9])	/*then */
	      {
		/*^cond.then */
		/*_#OR___L12*/ meltfnum[0] = /*_#IS_A__L11*/ meltfnum[9];;
	      }
	    else
	      {
		MELT_LOCATION ("warmelt-normal.melt:1753:/ cond.else");

		/*^block */
		/*anyblock */
		{

       /*_#IS_A__L13*/ meltfnum[12] =
		    melt_is_instance_of ((melt_ptr_t)
					 ( /*_.NORMCLA__V26*/ meltfptr[25]),
					 (melt_ptr_t) (( /*!CLASS_NREP_CONSTOCC */ meltfrout->tabval[7])));;
		  /*^compute */
		  /*_#OR___L12*/ meltfnum[0] = /*_#IS_A__L13*/ meltfnum[12];;
		  /*epilog */

		  MELT_LOCATION ("warmelt-normal.melt:1753:/ clear");
		 /*clear *//*_#IS_A__L13*/ meltfnum[12] = 0;
		}
		;
	      }
	    ;
	    MELT_LOCATION ("warmelt-normal.melt:1752:/ cond");
	    /*cond */ if ( /*_#OR___L12*/ meltfnum[0])	/*then */
	      {
		/*^cond.then */
		/*_.IFELSE___V32*/ meltfptr[28] = ( /*nil */ NULL);;
	      }
	    else
	      {
		MELT_LOCATION ("warmelt-normal.melt:1752:/ cond.else");

		/*^block */
		/*anyblock */
		{




		  {
		    /*^locexp */
		    melt_assert_failed (("check normcla"),
					("warmelt-normal.melt")
					? ("warmelt-normal.melt") : __FILE__,
					(1752) ? (1752) : __LINE__,
					__FUNCTION__);
		    ;
		  }
		  ;
		 /*clear *//*_.IFELSE___V32*/ meltfptr[28] = 0;
		  /*epilog */
		}
		;
	      }
	    ;
	    /*^compute */
	    /*_.IFCPP___V31*/ meltfptr[27] = /*_.IFELSE___V32*/ meltfptr[28];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-normal.melt:1752:/ clear");
	       /*clear *//*_#IS_A__L11*/ meltfnum[9] = 0;
	    /*^clear */
	       /*clear *//*_#OR___L12*/ meltfnum[0] = 0;
	    /*^clear */
	       /*clear *//*_.IFELSE___V32*/ meltfptr[28] = 0;
	  }

#else /*MELT_HAVE_DEBUG */
	  /*^cppif.else */
	  /*_.IFCPP___V31*/ meltfptr[27] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
	  ;
	  MELT_LOCATION ("warmelt-normal.melt:1757:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^quasiblock */


	  /*_.RETVAL___V1*/ meltfptr[0] = /*_.NORMCLA__V26*/ meltfptr[25];;

	  {
	    MELT_LOCATION ("warmelt-normal.melt:1757:/ locexp");
	    /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
	    if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	      melt_warn_for_no_expected_secondary_results ();
	    /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
	    ;
	  }
	  ;
	  /*^finalreturn */
	  ;
	  /*finalret */ goto labend_rout;
	  /*_.LET___V25*/ meltfptr[21] = /*_.RETURN___V33*/ meltfptr[28];;

	  MELT_LOCATION ("warmelt-normal.melt:1750:/ clear");
	     /*clear *//*_.NORMCLA__V26*/ meltfptr[25] = 0;
	  /*^clear */
	     /*clear *//*_.IFCPP___V27*/ meltfptr[26] = 0;
	  /*^clear */
	     /*clear *//*_.IFCPP___V31*/ meltfptr[27] = 0;
	  /*^clear */
	     /*clear *//*_.RETURN___V33*/ meltfptr[28] = 0;
	  /*_.IFELSE___V24*/ meltfptr[20] = /*_.LET___V25*/ meltfptr[21];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-normal.melt:1749:/ clear");
	     /*clear *//*_.LET___V25*/ meltfptr[21] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-normal.melt:1759:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
   /*_#IS_A__L14*/ meltfnum[12] =
	    melt_is_instance_of ((melt_ptr_t)
				 ( /*_.CLABIND__V19*/ meltfptr[18]),
				 (melt_ptr_t) (( /*!CLASS_VALUE_BINDING */
						meltfrout->tabval[9])));;
	  MELT_LOCATION ("warmelt-normal.melt:1759:/ cond");
	  /*cond */ if ( /*_#IS_A__L14*/ meltfnum[12])	/*then */
	    {
	      /*^cond.then */
	      /*^block */
	      /*anyblock */
	      {


#if MELT_HAVE_DEBUG
		MELT_LOCATION ("warmelt-normal.melt:1760:/ cppif.then");
		/*^block */
		/*anyblock */
		{

		  /*^checksignal */
		  MELT_CHECK_SIGNAL ();
		  ;
		  /*^getslot */
		  {
		    melt_ptr_t slot = NULL, obj = NULL;
		    obj =
		      (melt_ptr_t) ( /*_.CLABIND__V19*/ meltfptr[18]) /*=obj*/
		      ;
		    melt_object_get_field (slot, obj, 1, "VBIND_VALUE");
	/*_.VBIND_VALUE__V36*/ meltfptr[27] = slot;
		  };
		  ;
       /*_#__L15*/ meltfnum[9] =
		    (( /*_.RECV__V2*/ meltfptr[1]) ==
		     ( /*_.VBIND_VALUE__V36*/ meltfptr[27]));;
		  MELT_LOCATION ("warmelt-normal.melt:1760:/ cond");
		  /*cond */ if ( /*_#__L15*/ meltfnum[9])	/*then */
		    {
		      /*^cond.then */
		      /*_.IFELSE___V37*/ meltfptr[28] = ( /*nil */ NULL);;
		    }
		  else
		    {
		      MELT_LOCATION ("warmelt-normal.melt:1760:/ cond.else");

		      /*^block */
		      /*anyblock */
		      {




			{
			  /*^locexp */
			  melt_assert_failed (("check clabind value"),
					      ("warmelt-normal.melt")
					      ? ("warmelt-normal.melt") :
					      __FILE__,
					      (1760) ? (1760) : __LINE__,
					      __FUNCTION__);
			  ;
			}
			;
		   /*clear *//*_.IFELSE___V37*/ meltfptr[28] = 0;
			/*epilog */
		      }
		      ;
		    }
		  ;
		  /*^compute */
		  /*_.IFCPP___V35*/ meltfptr[26] =
		    /*_.IFELSE___V37*/ meltfptr[28];;
		  /*epilog */

		  MELT_LOCATION ("warmelt-normal.melt:1760:/ clear");
		 /*clear *//*_.VBIND_VALUE__V36*/ meltfptr[27] = 0;
		  /*^clear */
		 /*clear *//*_#__L15*/ meltfnum[9] = 0;
		  /*^clear */
		 /*clear *//*_.IFELSE___V37*/ meltfptr[28] = 0;
		}

#else /*MELT_HAVE_DEBUG */
		/*^cppif.else */
		/*_.IFCPP___V35*/ meltfptr[26] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
		;
		MELT_LOCATION ("warmelt-normal.melt:1761:/ quasiblock");


		/*^checksignal */
		MELT_CHECK_SIGNAL ();
		;
		/*^apply */
		/*apply */
		{
		  union meltparam_un argtab[3];
		  memset (&argtab, 0, sizeof (argtab));
		  /*^apply.arg */
		  argtab[0].meltbp_aptr =
		    (melt_ptr_t *) & /*_.ENV__V3*/ meltfptr[2];
		  /*^apply.arg */
		  argtab[1].meltbp_aptr =
		    (melt_ptr_t *) & /*_.NCX__V4*/ meltfptr[3];
		  /*^apply.arg */
		  argtab[2].meltbp_aptr =
		    (melt_ptr_t *) & /*_.PSLOC__V5*/ meltfptr[4];
		  /*_.NORMCLA__V39*/ meltfptr[27] =
		    melt_apply ((meltclosure_ptr_t)
				(( /*!NORMEXP_SYMBOL */ meltfrout->
				  tabval[6])),
				(melt_ptr_t) ( /*_.CLASYMB__V18*/
					      meltfptr[17]),
				(MELTBPARSTR_PTR MELTBPARSTR_PTR
				 MELTBPARSTR_PTR ""), argtab, "",
				(union meltparam_un *) 0);
		}
		;

#if MELT_HAVE_DEBUG
		MELT_LOCATION ("warmelt-normal.melt:1762:/ cppif.then");
		/*^block */
		/*anyblock */
		{


		  {
		    /*^locexp */
		    /*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
		    melt_dbgcounter++;
#endif
		    ;
		  }
		  ;
		  /*^checksignal */
		  MELT_CHECK_SIGNAL ();
		  ;
       /*_#MELT_NEED_DBG__L16*/ meltfnum[0] =
		    /*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
		    ( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
		    0		/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
		    ;;
		  MELT_LOCATION ("warmelt-normal.melt:1762:/ cond");
		  /*cond */ if ( /*_#MELT_NEED_DBG__L16*/ meltfnum[0])	/*then */
		    {
		      /*^cond.then */
		      /*^block */
		      /*anyblock */
		      {

	 /*_#THE_MELTCALLCOUNT__L17*/ meltfnum[9] =
#ifdef meltcallcount
			  meltcallcount	/* the_meltcallcount */
#else
			  0L
#endif /* meltcallcount the_meltcallcount */
			  ;;
			MELT_LOCATION
			  ("warmelt-normal.melt:1762:/ checksignal");
			MELT_CHECK_SIGNAL ();
			;
			/*^apply */
			/*apply */
			{
			  union meltparam_un argtab[5];
			  memset (&argtab, 0, sizeof (argtab));
			  /*^apply.arg */
			  argtab[0].meltbp_long =
			    /*_#THE_MELTCALLCOUNT__L17*/ meltfnum[9];
			  /*^apply.arg */
			  argtab[1].meltbp_cstring = "warmelt-normal.melt";
			  /*^apply.arg */
			  argtab[2].meltbp_long = 1762;
			  /*^apply.arg */
			  argtab[3].meltbp_cstring =
			    "normexp_class normcla class value";
			  /*^apply.arg */
			  argtab[4].meltbp_aptr =
			    (melt_ptr_t *) & /*_.NORMCLA__V39*/ meltfptr[27];
			  /*_.MELT_DEBUG_FUN__V42*/ meltfptr[41] =
			    melt_apply ((meltclosure_ptr_t)
					(( /*!MELT_DEBUG_FUN */ meltfrout->
					  tabval[0])),
					(melt_ptr_t) (( /*nil */ NULL)),
					(MELTBPARSTR_LONG MELTBPARSTR_CSTRING
					 MELTBPARSTR_LONG MELTBPARSTR_CSTRING
					 MELTBPARSTR_PTR ""), argtab, "",
					(union meltparam_un *) 0);
			}
			;
			/*_.IF___V41*/ meltfptr[40] =
			  /*_.MELT_DEBUG_FUN__V42*/ meltfptr[41];;
			/*epilog */

			MELT_LOCATION ("warmelt-normal.melt:1762:/ clear");
		   /*clear *//*_#THE_MELTCALLCOUNT__L17*/ meltfnum[9] =
			  0;
			/*^clear */
		   /*clear *//*_.MELT_DEBUG_FUN__V42*/ meltfptr[41] = 0;
		      }
		      ;
		    }
		  else
		    {		/*^cond.else */

	/*_.IF___V41*/ meltfptr[40] = NULL;;
		    }
		  ;
		  MELT_LOCATION ("warmelt-normal.melt:1762:/ quasiblock");


		  /*_.PROGN___V43*/ meltfptr[41] =
		    /*_.IF___V41*/ meltfptr[40];;
		  /*^compute */
		  /*_.IFCPP___V40*/ meltfptr[28] =
		    /*_.PROGN___V43*/ meltfptr[41];;
		  /*epilog */

		  MELT_LOCATION ("warmelt-normal.melt:1762:/ clear");
		 /*clear *//*_#MELT_NEED_DBG__L16*/ meltfnum[0] = 0;
		  /*^clear */
		 /*clear *//*_.IF___V41*/ meltfptr[40] = 0;
		  /*^clear */
		 /*clear *//*_.PROGN___V43*/ meltfptr[41] = 0;
		}

#else /*MELT_HAVE_DEBUG */
		/*^cppif.else */
		/*_.IFCPP___V40*/ meltfptr[28] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
		;

#if MELT_HAVE_DEBUG
		MELT_LOCATION ("warmelt-normal.melt:1764:/ cppif.then");
		/*^block */
		/*anyblock */
		{

		  /*^checksignal */
		  MELT_CHECK_SIGNAL ();
		  ;
       /*_#IS_A__L18*/ meltfnum[9] =
		    melt_is_instance_of ((melt_ptr_t)
					 ( /*_.NORMCLA__V39*/ meltfptr[27]),
					 (melt_ptr_t) (( /*!CLASS_NREP */
							meltfrout->
							tabval[10])));;
		  MELT_LOCATION ("warmelt-normal.melt:1764:/ cond");
		  /*cond */ if ( /*_#IS_A__L18*/ meltfnum[9])	/*then */
		    {
		      /*^cond.then */
		      /*_.IFELSE___V45*/ meltfptr[41] = ( /*nil */ NULL);;
		    }
		  else
		    {
		      MELT_LOCATION ("warmelt-normal.melt:1764:/ cond.else");

		      /*^block */
		      /*anyblock */
		      {




			{
			  /*^locexp */
			  melt_assert_failed (("check normcla"),
					      ("warmelt-normal.melt")
					      ? ("warmelt-normal.melt") :
					      __FILE__,
					      (1764) ? (1764) : __LINE__,
					      __FUNCTION__);
			  ;
			}
			;
		   /*clear *//*_.IFELSE___V45*/ meltfptr[41] = 0;
			/*epilog */
		      }
		      ;
		    }
		  ;
		  /*^compute */
		  /*_.IFCPP___V44*/ meltfptr[40] =
		    /*_.IFELSE___V45*/ meltfptr[41];;
		  /*epilog */

		  MELT_LOCATION ("warmelt-normal.melt:1764:/ clear");
		 /*clear *//*_#IS_A__L18*/ meltfnum[9] = 0;
		  /*^clear */
		 /*clear *//*_.IFELSE___V45*/ meltfptr[41] = 0;
		}

#else /*MELT_HAVE_DEBUG */
		/*^cppif.else */
		/*_.IFCPP___V44*/ meltfptr[40] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
		;
		MELT_LOCATION ("warmelt-normal.melt:1765:/ checksignal");
		MELT_CHECK_SIGNAL ();
		;
		/*^quasiblock */


		/*_.RETVAL___V1*/ meltfptr[0] =
		  /*_.NORMCLA__V39*/ meltfptr[27];;

		{
		  MELT_LOCATION ("warmelt-normal.melt:1765:/ locexp");
		  /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
		  if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
		    melt_warn_for_no_expected_secondary_results ();
		  /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
		  ;
		}
		;
		/*^finalreturn */
		;
		/*finalret */ goto labend_rout;
		/*_.LET___V38*/ meltfptr[21] =
		  /*_.RETURN___V46*/ meltfptr[41];;

		MELT_LOCATION ("warmelt-normal.melt:1761:/ clear");
	       /*clear *//*_.NORMCLA__V39*/ meltfptr[27] = 0;
		/*^clear */
	       /*clear *//*_.IFCPP___V40*/ meltfptr[28] = 0;
		/*^clear */
	       /*clear *//*_.IFCPP___V44*/ meltfptr[40] = 0;
		/*^clear */
	       /*clear *//*_.RETURN___V46*/ meltfptr[41] = 0;
		MELT_LOCATION ("warmelt-normal.melt:1759:/ quasiblock");


		/*_.PROGN___V47*/ meltfptr[27] =
		  /*_.LET___V38*/ meltfptr[21];;
		/*^compute */
		/*_.IFELSE___V34*/ meltfptr[25] =
		  /*_.PROGN___V47*/ meltfptr[27];;
		/*epilog */

		MELT_LOCATION ("warmelt-normal.melt:1759:/ clear");
	       /*clear *//*_.IFCPP___V35*/ meltfptr[26] = 0;
		/*^clear */
	       /*clear *//*_.LET___V38*/ meltfptr[21] = 0;
		/*^clear */
	       /*clear *//*_.PROGN___V47*/ meltfptr[27] = 0;
	      }
	      ;
	    }
	  else
	    {			/*^cond.else */

	      /*^block */
	      /*anyblock */
	      {

		MELT_LOCATION ("warmelt-normal.melt:1773:/ getslot");
		{
		  melt_ptr_t slot = NULL, obj = NULL;
		  obj = (melt_ptr_t) ( /*_.RECV__V2*/ meltfptr[1]) /*=obj*/ ;
		  melt_object_get_field (slot, obj, 1, "NAMED_NAME");
      /*_.NAMED_NAME__V48*/ meltfptr[28] = slot;
		};
		;

		{
		  MELT_LOCATION ("warmelt-normal.melt:1772:/ locexp");
		  melt_error_str ((melt_ptr_t) ( /*_.PSLOC__V5*/ meltfptr[4]),
				  ("class incorrectly bound, perhaps locally rebound"),
				  (melt_ptr_t) ( /*_.NAMED_NAME__V48*/
						meltfptr[28]));
		}
		;

#if MELT_HAVE_DEBUG
		MELT_LOCATION ("warmelt-normal.melt:1774:/ cppif.then");
		/*^block */
		/*anyblock */
		{


		  {
		    /*^locexp */
		    /*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
		    melt_dbgcounter++;
#endif
		    ;
		  }
		  ;
		  /*^checksignal */
		  MELT_CHECK_SIGNAL ();
		  ;
       /*_#MELT_NEED_DBG__L19*/ meltfnum[0] =
		    /*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
		    ( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
		    0		/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
		    ;;
		  MELT_LOCATION ("warmelt-normal.melt:1774:/ cond");
		  /*cond */ if ( /*_#MELT_NEED_DBG__L19*/ meltfnum[0])	/*then */
		    {
		      /*^cond.then */
		      /*^block */
		      /*anyblock */
		      {

	 /*_#THE_MELTCALLCOUNT__L20*/ meltfnum[9] =
#ifdef meltcallcount
			  meltcallcount	/* the_meltcallcount */
#else
			  0L
#endif /* meltcallcount the_meltcallcount */
			  ;;
			MELT_LOCATION
			  ("warmelt-normal.melt:1774:/ checksignal");
			MELT_CHECK_SIGNAL ();
			;
			/*^apply */
			/*apply */
			{
			  union meltparam_un argtab[4];
			  memset (&argtab, 0, sizeof (argtab));
			  /*^apply.arg */
			  argtab[0].meltbp_long =
			    /*_#THE_MELTCALLCOUNT__L20*/ meltfnum[9];
			  /*^apply.arg */
			  argtab[1].meltbp_cstring = "warmelt-normal.melt";
			  /*^apply.arg */
			  argtab[2].meltbp_long = 1774;
			  /*^apply.arg */
			  argtab[3].meltbp_cstring = "normexp_class failed";
			  /*_.MELT_DEBUG_FUN__V51*/ meltfptr[26] =
			    melt_apply ((meltclosure_ptr_t)
					(( /*!MELT_DEBUG_FUN */ meltfrout->
					  tabval[0])),
					(melt_ptr_t) (( /*nil */ NULL)),
					(MELTBPARSTR_LONG MELTBPARSTR_CSTRING
					 MELTBPARSTR_LONG MELTBPARSTR_CSTRING
					 ""), argtab, "",
					(union meltparam_un *) 0);
			}
			;
			/*_.IF___V50*/ meltfptr[41] =
			  /*_.MELT_DEBUG_FUN__V51*/ meltfptr[26];;
			/*epilog */

			MELT_LOCATION ("warmelt-normal.melt:1774:/ clear");
		   /*clear *//*_#THE_MELTCALLCOUNT__L20*/ meltfnum[9] =
			  0;
			/*^clear */
		   /*clear *//*_.MELT_DEBUG_FUN__V51*/ meltfptr[26] = 0;
		      }
		      ;
		    }
		  else
		    {		/*^cond.else */

	/*_.IF___V50*/ meltfptr[41] = NULL;;
		    }
		  ;
		  MELT_LOCATION ("warmelt-normal.melt:1774:/ quasiblock");


		  /*_.PROGN___V52*/ meltfptr[21] =
		    /*_.IF___V50*/ meltfptr[41];;
		  /*^compute */
		  /*_.IFCPP___V49*/ meltfptr[40] =
		    /*_.PROGN___V52*/ meltfptr[21];;
		  /*epilog */

		  MELT_LOCATION ("warmelt-normal.melt:1774:/ clear");
		 /*clear *//*_#MELT_NEED_DBG__L19*/ meltfnum[0] = 0;
		  /*^clear */
		 /*clear *//*_.IF___V50*/ meltfptr[41] = 0;
		  /*^clear */
		 /*clear *//*_.PROGN___V52*/ meltfptr[21] = 0;
		}

#else /*MELT_HAVE_DEBUG */
		/*^cppif.else */
		/*_.IFCPP___V49*/ meltfptr[40] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
		;
		MELT_LOCATION ("warmelt-normal.melt:1775:/ quasiblock");


     /*_.RETVAL___V1*/ meltfptr[0] = NULL;;

		{
		  MELT_LOCATION ("warmelt-normal.melt:1775:/ locexp");
		  /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
		  if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
		    melt_warn_for_no_expected_secondary_results ();
		  /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
		  ;
		}
		;
		/*^finalreturn */
		;
		/*finalret */ goto labend_rout;
		MELT_LOCATION ("warmelt-normal.melt:1767:/ quasiblock");


		/*_.PROGN___V54*/ meltfptr[26] =
		  /*_.RETURN___V53*/ meltfptr[27];;
		/*^compute */
		/*_.IFELSE___V34*/ meltfptr[25] =
		  /*_.PROGN___V54*/ meltfptr[26];;
		/*epilog */

		MELT_LOCATION ("warmelt-normal.melt:1759:/ clear");
	       /*clear *//*_.NAMED_NAME__V48*/ meltfptr[28] = 0;
		/*^clear */
	       /*clear *//*_.IFCPP___V49*/ meltfptr[40] = 0;
		/*^clear */
	       /*clear *//*_.RETURN___V53*/ meltfptr[27] = 0;
		/*^clear */
	       /*clear *//*_.PROGN___V54*/ meltfptr[26] = 0;
	      }
	      ;
	    }
	  ;
	  /*_.IFELSE___V24*/ meltfptr[20] = /*_.IFELSE___V34*/ meltfptr[25];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-normal.melt:1749:/ clear");
	     /*clear *//*_#IS_A__L14*/ meltfnum[12] = 0;
	  /*^clear */
	     /*clear *//*_.IFELSE___V34*/ meltfptr[25] = 0;
	}
	;
      }
    ;
    /*_.LET___V16*/ meltfptr[14] = /*_.IFELSE___V24*/ meltfptr[20];;

    MELT_LOCATION ("warmelt-normal.melt:1745:/ clear");
	   /*clear *//*_.NAMED_NAME__V17*/ meltfptr[16] = 0;
    /*^clear */
	   /*clear *//*_.CLASYMB__V18*/ meltfptr[17] = 0;
    /*^clear */
	   /*clear *//*_.CLABIND__V19*/ meltfptr[18] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V20*/ meltfptr[19] = 0;
    /*^clear */
	   /*clear *//*_#IS_A__L8*/ meltfnum[1] = 0;
    /*^clear */
	   /*clear *//*_.IFELSE___V24*/ meltfptr[20] = 0;
    MELT_LOCATION ("warmelt-normal.melt:1740:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.LET___V16*/ meltfptr[14];;

    {
      MELT_LOCATION ("warmelt-normal.melt:1740:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V6*/ meltfptr[5] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V10*/ meltfptr[6] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V12*/ meltfptr[7] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V14*/ meltfptr[12] = 0;
    /*^clear */
	   /*clear *//*_.LET___V16*/ meltfptr[14] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("NORMEXP_CLASS", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_40_warmelt_normal_NORMEXP_CLASS_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_40_warmelt_normal_NORMEXP_CLASS */



/**** end of warmelt-normal+01.c ****/
