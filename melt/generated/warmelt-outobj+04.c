/* GCC MELT GENERATED FILE warmelt-outobj+04.c - DO NOT EDIT */
/* secondary MELT generated C file of rank #4 */
#include "melt-run.h"


/* used hash from melt-run.h when compiling this file: */
MELT_EXTERN const char meltrun_used_md5_melt_f4[] =
  MELT_RUN_HASHMD5 /* from melt-run.h */ ;


/**** warmelt-outobj+04.c declarations ****/


/***************************************************
***
    Copyright (C) 2008 - 2012 Free Software Foundation, Inc.
    Contributed by Basile Starynkevitch <basile@starynkevitch.net>

    This file is part of GCC.

    GCC is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3, or (at your option)
    any later version.

    GCC is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with GCC; see the file COPYING3.  If not see
    <http://www.gnu.org/licenses/>.
***
****************************************************/

/* ordinary MELT module */
#define MELT_HAS_INITIAL_ENVIRONMENT 1	/*usual */

struct melt_callframe_st;	/*defined in melt-runtime.h */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_1_warmelt_outobj_GET_CODE_BUFFER_LIMIT (meltclosure_ptr_t meltclosp_,
						 melt_ptr_t meltfirstargp_,
						 const melt_argdescr_cell_t
						 meltxargdescr_[],
						 union meltparam_un
						 *meltxargtab_,
						 const melt_argdescr_cell_t
						 meltxresdescr_[],
						 union meltparam_un
						 *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_2_warmelt_outobj_PUT_CODE_BUFFER_LIMIT (meltclosure_ptr_t meltclosp_,
						 melt_ptr_t meltfirstargp_,
						 const melt_argdescr_cell_t
						 meltxargdescr_[],
						 union meltparam_un
						 *meltxargtab_,
						 const melt_argdescr_cell_t
						 meltxresdescr_[],
						 union meltparam_un
						 *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_3_warmelt_outobj_CODE_BUFFER_LIMIT_OPTSET (meltclosure_ptr_t
						    meltclosp_,
						    melt_ptr_t meltfirstargp_,
						    const melt_argdescr_cell_t
						    meltxargdescr_[],
						    union meltparam_un
						    *meltxargtab_,
						    const melt_argdescr_cell_t
						    meltxresdescr_[],
						    union meltparam_un
						    *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_4_warmelt_outobj_OUTDECLINIT_ROOT (meltclosure_ptr_t meltclosp_,
					    melt_ptr_t meltfirstargp_,
					    const melt_argdescr_cell_t
					    meltxargdescr_[],
					    union meltparam_un *meltxargtab_,
					    const melt_argdescr_cell_t
					    meltxresdescr_[],
					    union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_5_warmelt_outobj_OUTPUCOD_OBJINIELEM (meltclosure_ptr_t meltclosp_,
					       melt_ptr_t meltfirstargp_,
					       const melt_argdescr_cell_t
					       meltxargdescr_[],
					       union meltparam_un
					       *meltxargtab_,
					       const melt_argdescr_cell_t
					       meltxresdescr_[],
					       union meltparam_un
					       *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_6_warmelt_outobj_OUTCINITFILL_ROOT (meltclosure_ptr_t meltclosp_,
					     melt_ptr_t meltfirstargp_,
					     const melt_argdescr_cell_t
					     meltxargdescr_[],
					     union meltparam_un *meltxargtab_,
					     const melt_argdescr_cell_t
					     meltxresdescr_[],
					     union meltparam_un
					     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_7_warmelt_outobj_OUTCINITPREDEF_ROOT (meltclosure_ptr_t meltclosp_,
					       melt_ptr_t meltfirstargp_,
					       const melt_argdescr_cell_t
					       meltxargdescr_[],
					       union meltparam_un
					       *meltxargtab_,
					       const melt_argdescr_cell_t
					       meltxresdescr_[],
					       union meltparam_un
					       *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_8_warmelt_outobj_OUTPUT_PREDEF (meltclosure_ptr_t meltclosp_,
					 melt_ptr_t meltfirstargp_,
					 const melt_argdescr_cell_t
					 meltxargdescr_[],
					 union meltparam_un *meltxargtab_,
					 const melt_argdescr_cell_t
					 meltxresdescr_[],
					 union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_9_warmelt_outobj_OUTPUCOD_PREDEF (meltclosure_ptr_t meltclosp_,
					   melt_ptr_t meltfirstargp_,
					   const melt_argdescr_cell_t
					   meltxargdescr_[],
					   union meltparam_un *meltxargtab_,
					   const melt_argdescr_cell_t
					   meltxresdescr_[],
					   union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_10_warmelt_outobj_OUTPUCOD_NIL (meltclosure_ptr_t meltclosp_,
					 melt_ptr_t meltfirstargp_,
					 const melt_argdescr_cell_t
					 meltxargdescr_[],
					 union meltparam_un *meltxargtab_,
					 const melt_argdescr_cell_t
					 meltxresdescr_[],
					 union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_11_warmelt_outobj_OUTPUCOD_OBJEXPANDPUREVAL (meltclosure_ptr_t
						      meltclosp_,
						      melt_ptr_t
						      meltfirstargp_,
						      const
						      melt_argdescr_cell_t
						      meltxargdescr_[],
						      union meltparam_un
						      *meltxargtab_,
						      const
						      melt_argdescr_cell_t
						      meltxresdescr_[],
						      union meltparam_un
						      *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_12_warmelt_outobj_OUTDECLINIT_OBJINITOBJECT (meltclosure_ptr_t
						      meltclosp_,
						      melt_ptr_t
						      meltfirstargp_,
						      const
						      melt_argdescr_cell_t
						      meltxargdescr_[],
						      union meltparam_un
						      *meltxargtab_,
						      const
						      melt_argdescr_cell_t
						      meltxresdescr_[],
						      union meltparam_un
						      *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_13_warmelt_outobj_OUTCINITFILL_OBJINITOBJECT (meltclosure_ptr_t
						       meltclosp_,
						       melt_ptr_t
						       meltfirstargp_,
						       const
						       melt_argdescr_cell_t
						       meltxargdescr_[],
						       union meltparam_un
						       *meltxargtab_,
						       const
						       melt_argdescr_cell_t
						       meltxresdescr_[],
						       union meltparam_un
						       *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_14_warmelt_outobj_OUTCINITPREDEF_OBJINITOBJECT (meltclosure_ptr_t
							 meltclosp_,
							 melt_ptr_t
							 meltfirstargp_,
							 const
							 melt_argdescr_cell_t
							 meltxargdescr_[],
							 union meltparam_un
							 *meltxargtab_,
							 const
							 melt_argdescr_cell_t
							 meltxresdescr_[],
							 union meltparam_un
							 *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_15_warmelt_outobj_OUTDECLINIT_OBJINITMULTIPLE (meltclosure_ptr_t
							meltclosp_,
							melt_ptr_t
							meltfirstargp_,
							const
							melt_argdescr_cell_t
							meltxargdescr_[],
							union meltparam_un
							*meltxargtab_,
							const
							melt_argdescr_cell_t
							meltxresdescr_[],
							union meltparam_un
							*meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_16_warmelt_outobj_OUTCINITFILL_OBJINITMULTIPLE (meltclosure_ptr_t
							 meltclosp_,
							 melt_ptr_t
							 meltfirstargp_,
							 const
							 melt_argdescr_cell_t
							 meltxargdescr_[],
							 union meltparam_un
							 *meltxargtab_,
							 const
							 melt_argdescr_cell_t
							 meltxresdescr_[],
							 union meltparam_un
							 *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_17_warmelt_outobj_OUTDECLINIT_OBJINITCLOSURE (meltclosure_ptr_t
						       meltclosp_,
						       melt_ptr_t
						       meltfirstargp_,
						       const
						       melt_argdescr_cell_t
						       meltxargdescr_[],
						       union meltparam_un
						       *meltxargtab_,
						       const
						       melt_argdescr_cell_t
						       meltxresdescr_[],
						       union meltparam_un
						       *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_18_warmelt_outobj_OUTCINITFILL_OBJINITCLOSURE (meltclosure_ptr_t
							meltclosp_,
							melt_ptr_t
							meltfirstargp_,
							const
							melt_argdescr_cell_t
							meltxargdescr_[],
							union meltparam_un
							*meltxargtab_,
							const
							melt_argdescr_cell_t
							meltxresdescr_[],
							union meltparam_un
							*meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_19_warmelt_outobj_OUTDECLINIT_OBJINITROUTINE (meltclosure_ptr_t
						       meltclosp_,
						       melt_ptr_t
						       meltfirstargp_,
						       const
						       melt_argdescr_cell_t
						       meltxargdescr_[],
						       union meltparam_un
						       *meltxargtab_,
						       const
						       melt_argdescr_cell_t
						       meltxresdescr_[],
						       union meltparam_un
						       *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_20_warmelt_outobj_OUTCINITFILL_OBJINITROUTINE (meltclosure_ptr_t
							meltclosp_,
							melt_ptr_t
							meltfirstargp_,
							const
							melt_argdescr_cell_t
							meltxargdescr_[],
							union meltparam_un
							*meltxargtab_,
							const
							melt_argdescr_cell_t
							meltxresdescr_[],
							union meltparam_un
							*meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_21_warmelt_outobj_OUTDECLINIT_OBJINITSTRING (meltclosure_ptr_t
						      meltclosp_,
						      melt_ptr_t
						      meltfirstargp_,
						      const
						      melt_argdescr_cell_t
						      meltxargdescr_[],
						      union meltparam_un
						      *meltxargtab_,
						      const
						      melt_argdescr_cell_t
						      meltxresdescr_[],
						      union meltparam_un
						      *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_22_warmelt_outobj_OUTCINITFILL_OBJINITSTRING (meltclosure_ptr_t
						       meltclosp_,
						       melt_ptr_t
						       meltfirstargp_,
						       const
						       melt_argdescr_cell_t
						       meltxargdescr_[],
						       union meltparam_un
						       *meltxargtab_,
						       const
						       melt_argdescr_cell_t
						       meltxresdescr_[],
						       union meltparam_un
						       *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_23_warmelt_outobj_OUTDECLINIT_OBJINITBOXEDINTEGER (meltclosure_ptr_t
							    meltclosp_,
							    melt_ptr_t
							    meltfirstargp_,
							    const
							    melt_argdescr_cell_t
							    meltxargdescr_[],
							    union meltparam_un
							    *meltxargtab_,
							    const
							    melt_argdescr_cell_t
							    meltxresdescr_[],
							    union meltparam_un
							    *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_24_warmelt_outobj_OUTCINITFILL_OBJINITBOXEDINTEGER (meltclosure_ptr_t
							     meltclosp_,
							     melt_ptr_t
							     meltfirstargp_,
							     const
							     melt_argdescr_cell_t
							     meltxargdescr_[],
							     union
							     meltparam_un
							     *meltxargtab_,
							     const
							     melt_argdescr_cell_t
							     meltxresdescr_[],
							     union
							     meltparam_un
							     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_25_warmelt_outobj_OUTDECLINIT_OBJINITPAIR (meltclosure_ptr_t
						    meltclosp_,
						    melt_ptr_t meltfirstargp_,
						    const melt_argdescr_cell_t
						    meltxargdescr_[],
						    union meltparam_un
						    *meltxargtab_,
						    const melt_argdescr_cell_t
						    meltxresdescr_[],
						    union meltparam_un
						    *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_26_warmelt_outobj_OUTCINITFILL_OBJINITPAIR (meltclosure_ptr_t
						     meltclosp_,
						     melt_ptr_t
						     meltfirstargp_,
						     const
						     melt_argdescr_cell_t
						     meltxargdescr_[],
						     union meltparam_un
						     *meltxargtab_,
						     const
						     melt_argdescr_cell_t
						     meltxresdescr_[],
						     union meltparam_un
						     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_27_warmelt_outobj_OUTDECLINIT_OBJINITLIST (meltclosure_ptr_t
						    meltclosp_,
						    melt_ptr_t meltfirstargp_,
						    const melt_argdescr_cell_t
						    meltxargdescr_[],
						    union meltparam_un
						    *meltxargtab_,
						    const melt_argdescr_cell_t
						    meltxresdescr_[],
						    union meltparam_un
						    *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_28_warmelt_outobj_OUTCINITFILL_OBJINITLIST (meltclosure_ptr_t
						     meltclosp_,
						     melt_ptr_t
						     meltfirstargp_,
						     const
						     melt_argdescr_cell_t
						     meltxargdescr_[],
						     union meltparam_un
						     *meltxargtab_,
						     const
						     melt_argdescr_cell_t
						     meltxresdescr_[],
						     union meltparam_un
						     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_29_warmelt_outobj_OUTPUCOD_ANYDISCR (meltclosure_ptr_t meltclosp_,
					      melt_ptr_t meltfirstargp_,
					      const melt_argdescr_cell_t
					      meltxargdescr_[],
					      union meltparam_un
					      *meltxargtab_,
					      const melt_argdescr_cell_t
					      meltxresdescr_[],
					      union meltparam_un
					      *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_30_warmelt_outobj_OUTPUCOD_NULL (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_31_warmelt_outobj_OUTPUCOD_CATCHALL_ROOT (meltclosure_ptr_t
						   meltclosp_,
						   melt_ptr_t meltfirstargp_,
						   const melt_argdescr_cell_t
						   meltxargdescr_[],
						   union meltparam_un
						   *meltxargtab_,
						   const melt_argdescr_cell_t
						   meltxresdescr_[],
						   union meltparam_un
						   *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_32_warmelt_outobj_OUTPUT_RAW_LOCATION (meltclosure_ptr_t meltclosp_,
						melt_ptr_t meltfirstargp_,
						const melt_argdescr_cell_t
						meltxargdescr_[],
						union meltparam_un
						*meltxargtab_,
						const melt_argdescr_cell_t
						meltxresdescr_[],
						union meltparam_un
						*meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_33_warmelt_outobj_LINE_AND_FILE_OF_LOCATION (meltclosure_ptr_t
						      meltclosp_,
						      melt_ptr_t
						      meltfirstargp_,
						      const
						      melt_argdescr_cell_t
						      meltxargdescr_[],
						      union meltparam_un
						      *meltxargtab_,
						      const
						      melt_argdescr_cell_t
						      meltxresdescr_[],
						      union meltparam_un
						      *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_34_warmelt_outobj_OUTPUT_LOCATION (meltclosure_ptr_t meltclosp_,
					    melt_ptr_t meltfirstargp_,
					    const melt_argdescr_cell_t
					    meltxargdescr_[],
					    union meltparam_un *meltxargtab_,
					    const melt_argdescr_cell_t
					    meltxresdescr_[],
					    union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_35_warmelt_outobj_OUTPUT_CURFRAME_DECLSTRUCT (meltclosure_ptr_t
						       meltclosp_,
						       melt_ptr_t
						       meltfirstargp_,
						       const
						       melt_argdescr_cell_t
						       meltxargdescr_[],
						       union meltparam_un
						       *meltxargtab_,
						       const
						       melt_argdescr_cell_t
						       meltxresdescr_[],
						       union meltparam_un
						       *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_36_warmelt_outobj_LAMBDA___1__ (meltclosure_ptr_t meltclosp_,
					 melt_ptr_t meltfirstargp_,
					 const melt_argdescr_cell_t
					 meltxargdescr_[],
					 union meltparam_un *meltxargtab_,
					 const melt_argdescr_cell_t
					 meltxresdescr_[],
					 union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_37_warmelt_outobj_OUTPUCOD_MARKER (meltclosure_ptr_t meltclosp_,
					    melt_ptr_t meltfirstargp_,
					    const melt_argdescr_cell_t
					    meltxargdescr_[],
					    union meltparam_un *meltxargtab_,
					    const melt_argdescr_cell_t
					    meltxresdescr_[],
					    union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_38_warmelt_outobj_LAMBDA___2__ (meltclosure_ptr_t meltclosp_,
					 melt_ptr_t meltfirstargp_,
					 const melt_argdescr_cell_t
					 meltxargdescr_[],
					 union meltparam_un *meltxargtab_,
					 const melt_argdescr_cell_t
					 meltxresdescr_[],
					 union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_39_warmelt_outobj_OUTPUT_CURFRAME_DECLSTRUCT_INIT (meltclosure_ptr_t
							    meltclosp_,
							    melt_ptr_t
							    meltfirstargp_,
							    const
							    melt_argdescr_cell_t
							    meltxargdescr_[],
							    union meltparam_un
							    *meltxargtab_,
							    const
							    melt_argdescr_cell_t
							    meltxresdescr_[],
							    union meltparam_un
							    *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_40_warmelt_outobj_OUTPUCOD_PROCROUTINE (meltclosure_ptr_t meltclosp_,
						 melt_ptr_t meltfirstargp_,
						 const melt_argdescr_cell_t
						 meltxargdescr_[],
						 union meltparam_un
						 *meltxargtab_,
						 const melt_argdescr_cell_t
						 meltxresdescr_[],
						 union meltparam_un
						 *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_41_warmelt_outobj_LAMBDA___3__ (meltclosure_ptr_t meltclosp_,
					 melt_ptr_t meltfirstargp_,
					 const melt_argdescr_cell_t
					 meltxargdescr_[],
					 union meltparam_un *meltxargtab_,
					 const melt_argdescr_cell_t
					 meltxresdescr_[],
					 union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_42_warmelt_outobj_OUTPUT_CURFRAME_CDAT_STRUCT (meltclosure_ptr_t
							meltclosp_,
							melt_ptr_t
							meltfirstargp_,
							const
							melt_argdescr_cell_t
							meltxargdescr_[],
							union meltparam_un
							*meltxargtab_,
							const
							melt_argdescr_cell_t
							meltxresdescr_[],
							union meltparam_un
							*meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_43_warmelt_outobj_OUTPUT_CURFRAME_CDAT_FILL (meltclosure_ptr_t
						      meltclosp_,
						      melt_ptr_t
						      meltfirstargp_,
						      const
						      melt_argdescr_cell_t
						      meltxargdescr_[],
						      union meltparam_un
						      *meltxargtab_,
						      const
						      melt_argdescr_cell_t
						      meltxresdescr_[],
						      union meltparam_un
						      *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_44_warmelt_outobj_OUTPUCOD_INITIALMODULEROUTINE (meltclosure_ptr_t
							  meltclosp_,
							  melt_ptr_t
							  meltfirstargp_,
							  const
							  melt_argdescr_cell_t
							  meltxargdescr_[],
							  union meltparam_un
							  *meltxargtab_,
							  const
							  melt_argdescr_cell_t
							  meltxresdescr_[],
							  union meltparam_un
							  *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_45_warmelt_outobj_LAMBDA___4__ (meltclosure_ptr_t meltclosp_,
					 melt_ptr_t meltfirstargp_,
					 const melt_argdescr_cell_t
					 meltxargdescr_[],
					 union meltparam_un *meltxargtab_,
					 const melt_argdescr_cell_t
					 meltxresdescr_[],
					 union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_46_warmelt_outobj_OUTPUCOD_INITIALEXTENSIONROUTINE (meltclosure_ptr_t
							     meltclosp_,
							     melt_ptr_t
							     meltfirstargp_,
							     const
							     melt_argdescr_cell_t
							     meltxargdescr_[],
							     union
							     meltparam_un
							     *meltxargtab_,
							     const
							     melt_argdescr_cell_t
							     meltxresdescr_[],
							     union
							     meltparam_un
							     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_47_warmelt_outobj_LAMBDA___5__ (meltclosure_ptr_t meltclosp_,
					 melt_ptr_t meltfirstargp_,
					 const melt_argdescr_cell_t
					 meltxargdescr_[],
					 union meltparam_un *meltxargtab_,
					 const melt_argdescr_cell_t
					 meltxresdescr_[],
					 union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_48_warmelt_outobj_OUTPUCOD_GETARG (meltclosure_ptr_t meltclosp_,
					    melt_ptr_t meltfirstargp_,
					    const melt_argdescr_cell_t
					    meltxargdescr_[],
					    union meltparam_un *meltxargtab_,
					    const melt_argdescr_cell_t
					    meltxresdescr_[],
					    union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_49_warmelt_outobj_OUTPUCOD_GETARGREST (meltclosure_ptr_t meltclosp_,
						melt_ptr_t meltfirstargp_,
						const melt_argdescr_cell_t
						meltxargdescr_[],
						union meltparam_un
						*meltxargtab_,
						const melt_argdescr_cell_t
						meltxresdescr_[],
						union meltparam_un
						*meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_50_warmelt_outobj_OUTPUCOD_OBJLOCV (meltclosure_ptr_t meltclosp_,
					     melt_ptr_t meltfirstargp_,
					     const melt_argdescr_cell_t
					     meltxargdescr_[],
					     union meltparam_un *meltxargtab_,
					     const melt_argdescr_cell_t
					     meltxresdescr_[],
					     union meltparam_un
					     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_51_warmelt_outobj_OUTPUCOD_OBJCLOCCV (meltclosure_ptr_t meltclosp_,
					       melt_ptr_t meltfirstargp_,
					       const melt_argdescr_cell_t
					       meltxargdescr_[],
					       union meltparam_un
					       *meltxargtab_,
					       const melt_argdescr_cell_t
					       meltxresdescr_[],
					       union meltparam_un
					       *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_52_warmelt_outobj_OUTPUCOD_OBJCONSTV (meltclosure_ptr_t meltclosp_,
					       melt_ptr_t meltfirstargp_,
					       const melt_argdescr_cell_t
					       meltxargdescr_[],
					       union meltparam_un
					       *meltxargtab_,
					       const melt_argdescr_cell_t
					       meltxresdescr_[],
					       union meltparam_un
					       *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_53_warmelt_outobj_OUTPUT_CODE_INSTRUCTIONS_LIST (meltclosure_ptr_t
							  meltclosp_,
							  melt_ptr_t
							  meltfirstargp_,
							  const
							  melt_argdescr_cell_t
							  meltxargdescr_[],
							  union meltparam_un
							  *meltxargtab_,
							  const
							  melt_argdescr_cell_t
							  meltxresdescr_[],
							  union meltparam_un
							  *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_54_warmelt_outobj_OUTPUCOD_OBJCHECKSIGNAL (meltclosure_ptr_t
						    meltclosp_,
						    melt_ptr_t meltfirstargp_,
						    const melt_argdescr_cell_t
						    meltxargdescr_[],
						    union meltparam_un
						    *meltxargtab_,
						    const melt_argdescr_cell_t
						    meltxresdescr_[],
						    union meltparam_un
						    *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_55_warmelt_outobj_OUTPUCOD_OBJANYBLOCK (meltclosure_ptr_t meltclosp_,
						 melt_ptr_t meltfirstargp_,
						 const melt_argdescr_cell_t
						 meltxargdescr_[],
						 union meltparam_un
						 *meltxargtab_,
						 const melt_argdescr_cell_t
						 meltxresdescr_[],
						 union meltparam_un
						 *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_56_warmelt_outobj_OUTPUCOD_OBJMULTIALLOCBLOCK (meltclosure_ptr_t
							meltclosp_,
							melt_ptr_t
							meltfirstargp_,
							const
							melt_argdescr_cell_t
							meltxargdescr_[],
							union meltparam_un
							*meltxargtab_,
							const
							melt_argdescr_cell_t
							meltxresdescr_[],
							union meltparam_un
							*meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_57_warmelt_outobj_OUTPUCOD_OBJCITERBLOCK (meltclosure_ptr_t
						   meltclosp_,
						   melt_ptr_t meltfirstargp_,
						   const melt_argdescr_cell_t
						   meltxargdescr_[],
						   union meltparam_un
						   *meltxargtab_,
						   const melt_argdescr_cell_t
						   meltxresdescr_[],
						   union meltparam_un
						   *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_58_warmelt_outobj_OUTPUCOD_OBJCOMMENTINSTR (meltclosure_ptr_t
						     meltclosp_,
						     melt_ptr_t
						     meltfirstargp_,
						     const
						     melt_argdescr_cell_t
						     meltxargdescr_[],
						     union meltparam_un
						     *meltxargtab_,
						     const
						     melt_argdescr_cell_t
						     meltxresdescr_[],
						     union meltparam_un
						     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_59_warmelt_outobj_OUTPUCOD_OBJCOMMENTEDBLOCK (meltclosure_ptr_t
						       meltclosp_,
						       melt_ptr_t
						       meltfirstargp_,
						       const
						       melt_argdescr_cell_t
						       meltxargdescr_[],
						       union meltparam_un
						       *meltxargtab_,
						       const
						       melt_argdescr_cell_t
						       meltxresdescr_[],
						       union meltparam_un
						       *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_60_warmelt_outobj_OUTPUCOD_OBJLABELINSTR (meltclosure_ptr_t
						   meltclosp_,
						   melt_ptr_t meltfirstargp_,
						   const melt_argdescr_cell_t
						   meltxargdescr_[],
						   union meltparam_un
						   *meltxargtab_,
						   const melt_argdescr_cell_t
						   meltxresdescr_[],
						   union meltparam_un
						   *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_61_warmelt_outobj_OUTPUCOD_OBJGOTOINSTR (meltclosure_ptr_t
						  meltclosp_,
						  melt_ptr_t meltfirstargp_,
						  const melt_argdescr_cell_t
						  meltxargdescr_[],
						  union meltparam_un
						  *meltxargtab_,
						  const melt_argdescr_cell_t
						  meltxresdescr_[],
						  union meltparam_un
						  *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_62_warmelt_outobj_ADD2SBUF_CLONSYM (meltclosure_ptr_t meltclosp_,
					     melt_ptr_t meltfirstargp_,
					     const melt_argdescr_cell_t
					     meltxargdescr_[],
					     union meltparam_un *meltxargtab_,
					     const melt_argdescr_cell_t
					     meltxresdescr_[],
					     union meltparam_un
					     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_63_warmelt_outobj_OUTPUCOD_OBJLOOP (meltclosure_ptr_t meltclosp_,
					     melt_ptr_t meltfirstargp_,
					     const melt_argdescr_cell_t
					     meltxargdescr_[],
					     union meltparam_un *meltxargtab_,
					     const melt_argdescr_cell_t
					     meltxresdescr_[],
					     union meltparam_un
					     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_64_warmelt_outobj_LAMBDA___6__ (meltclosure_ptr_t meltclosp_,
					 melt_ptr_t meltfirstargp_,
					 const melt_argdescr_cell_t
					 meltxargdescr_[],
					 union meltparam_un *meltxargtab_,
					 const melt_argdescr_cell_t
					 meltxresdescr_[],
					 union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_65_warmelt_outobj_LAMBDA___7__ (meltclosure_ptr_t meltclosp_,
					 melt_ptr_t meltfirstargp_,
					 const melt_argdescr_cell_t
					 meltxargdescr_[],
					 union meltparam_un *meltxargtab_,
					 const melt_argdescr_cell_t
					 meltxresdescr_[],
					 union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_66_warmelt_outobj_OUTPUCOD_OBJEXIT (meltclosure_ptr_t meltclosp_,
					     melt_ptr_t meltfirstargp_,
					     const melt_argdescr_cell_t
					     meltxargdescr_[],
					     union meltparam_un *meltxargtab_,
					     const melt_argdescr_cell_t
					     meltxresdescr_[],
					     union meltparam_un
					     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_67_warmelt_outobj_OUTPUCOD_OBJAGAIN (meltclosure_ptr_t meltclosp_,
					      melt_ptr_t meltfirstargp_,
					      const melt_argdescr_cell_t
					      meltxargdescr_[],
					      union meltparam_un
					      *meltxargtab_,
					      const melt_argdescr_cell_t
					      meltxresdescr_[],
					      union meltparam_un
					      *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_68_warmelt_outobj_OUTPUCOD_OBJCOMPUTE (meltclosure_ptr_t meltclosp_,
						melt_ptr_t meltfirstargp_,
						const melt_argdescr_cell_t
						meltxargdescr_[],
						union meltparam_un
						*meltxargtab_,
						const melt_argdescr_cell_t
						meltxresdescr_[],
						union meltparam_un
						*meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_69_warmelt_outobj_LAMBDA___8__ (meltclosure_ptr_t meltclosp_,
					 melt_ptr_t meltfirstargp_,
					 const melt_argdescr_cell_t
					 meltxargdescr_[],
					 union meltparam_un *meltxargtab_,
					 const melt_argdescr_cell_t
					 meltxresdescr_[],
					 union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_70_warmelt_outobj_LAMBDA___9__ (meltclosure_ptr_t meltclosp_,
					 melt_ptr_t meltfirstargp_,
					 const melt_argdescr_cell_t
					 meltxargdescr_[],
					 union meltparam_un *meltxargtab_,
					 const melt_argdescr_cell_t
					 meltxresdescr_[],
					 union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_71_warmelt_outobj_LAMBDA___10__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_72_warmelt_outobj_OUTPUCOD_OBJCOND (meltclosure_ptr_t meltclosp_,
					     melt_ptr_t meltfirstargp_,
					     const melt_argdescr_cell_t
					     meltxargdescr_[],
					     union meltparam_un *meltxargtab_,
					     const melt_argdescr_cell_t
					     meltxresdescr_[],
					     union meltparam_un
					     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_73_warmelt_outobj_OUTPUCOD_OBJCPPIF (meltclosure_ptr_t meltclosp_,
					      melt_ptr_t meltfirstargp_,
					      const melt_argdescr_cell_t
					      meltxargdescr_[],
					      union meltparam_un
					      *meltxargtab_,
					      const melt_argdescr_cell_t
					      meltxresdescr_[],
					      union meltparam_un
					      *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_74_warmelt_outobj_OUTPUCOD_OBJINTERNSYMBOL (meltclosure_ptr_t
						     meltclosp_,
						     melt_ptr_t
						     meltfirstargp_,
						     const
						     melt_argdescr_cell_t
						     meltxargdescr_[],
						     union meltparam_un
						     *meltxargtab_,
						     const
						     melt_argdescr_cell_t
						     meltxresdescr_[],
						     union meltparam_un
						     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_75_warmelt_outobj_OUTPUCOD_OBJINTERNKEYWORD (meltclosure_ptr_t
						      meltclosp_,
						      melt_ptr_t
						      meltfirstargp_,
						      const
						      melt_argdescr_cell_t
						      meltxargdescr_[],
						      union meltparam_un
						      *meltxargtab_,
						      const
						      melt_argdescr_cell_t
						      meltxresdescr_[],
						      union meltparam_un
						      *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_76_warmelt_outobj_OUTPUCOD_OBJGETNAMEDSYMBOL (meltclosure_ptr_t
						       meltclosp_,
						       melt_ptr_t
						       meltfirstargp_,
						       const
						       melt_argdescr_cell_t
						       meltxargdescr_[],
						       union meltparam_un
						       *meltxargtab_,
						       const
						       melt_argdescr_cell_t
						       meltxresdescr_[],
						       union meltparam_un
						       *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_77_warmelt_outobj_OUTPUCOD_OBJGETNAMEDKEYWORD (meltclosure_ptr_t
							meltclosp_,
							melt_ptr_t
							meltfirstargp_,
							const
							melt_argdescr_cell_t
							meltxargdescr_[],
							union meltparam_un
							*meltxargtab_,
							const
							melt_argdescr_cell_t
							meltxresdescr_[],
							union meltparam_un
							*meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_78_warmelt_outobj_OUTPUCOD_OBJAPPLY (meltclosure_ptr_t meltclosp_,
					      melt_ptr_t meltfirstargp_,
					      const melt_argdescr_cell_t
					      meltxargdescr_[],
					      union meltparam_un
					      *meltxargtab_,
					      const melt_argdescr_cell_t
					      meltxresdescr_[],
					      union meltparam_un
					      *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_79_warmelt_outobj_LAMBDA___11__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_80_warmelt_outobj_LAMBDA___12__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_81_warmelt_outobj_OUTPUCOD_OBJMSEND (meltclosure_ptr_t meltclosp_,
					      melt_ptr_t meltfirstargp_,
					      const melt_argdescr_cell_t
					      meltxargdescr_[],
					      union meltparam_un
					      *meltxargtab_,
					      const melt_argdescr_cell_t
					      meltxresdescr_[],
					      union meltparam_un
					      *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_82_warmelt_outobj_LAMBDA___13__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_83_warmelt_outobj_OUTPUCOD_OBJMULTIAPPLY (meltclosure_ptr_t
						   meltclosp_,
						   melt_ptr_t meltfirstargp_,
						   const melt_argdescr_cell_t
						   meltxargdescr_[],
						   union meltparam_un
						   *meltxargtab_,
						   const melt_argdescr_cell_t
						   meltxresdescr_[],
						   union meltparam_un
						   *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_84_warmelt_outobj_LAMBDA___14__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_85_warmelt_outobj_LAMBDA___15__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_86_warmelt_outobj_LAMBDA___16__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_87_warmelt_outobj_LAMBDA___17__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_88_warmelt_outobj_LAMBDA___18__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_89_warmelt_outobj_OUTPUCOD_OBJMULTIMSEND (meltclosure_ptr_t
						   meltclosp_,
						   melt_ptr_t meltfirstargp_,
						   const melt_argdescr_cell_t
						   meltxargdescr_[],
						   union meltparam_un
						   *meltxargtab_,
						   const melt_argdescr_cell_t
						   meltxresdescr_[],
						   union meltparam_un
						   *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_90_warmelt_outobj_LAMBDA___19__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_91_warmelt_outobj_LAMBDA___20__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_92_warmelt_outobj_LAMBDA___21__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_93_warmelt_outobj_LAMBDA___22__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_94_warmelt_outobj_LAMBDA___23__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_95_warmelt_outobj_OUTPUCOD_OBJCLEAR (meltclosure_ptr_t meltclosp_,
					      melt_ptr_t meltfirstargp_,
					      const melt_argdescr_cell_t
					      meltxargdescr_[],
					      union meltparam_un
					      *meltxargtab_,
					      const melt_argdescr_cell_t
					      meltxresdescr_[],
					      union meltparam_un
					      *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_96_warmelt_outobj_OUTPUCOD_OBJRAWALLOCOBJ (meltclosure_ptr_t
						    meltclosp_,
						    melt_ptr_t meltfirstargp_,
						    const melt_argdescr_cell_t
						    meltxargdescr_[],
						    union meltparam_un
						    *meltxargtab_,
						    const melt_argdescr_cell_t
						    meltxresdescr_[],
						    union meltparam_un
						    *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_97_warmelt_outobj_OUTPUCOD_OBJNEWCLOSURE (meltclosure_ptr_t
						   meltclosp_,
						   melt_ptr_t meltfirstargp_,
						   const melt_argdescr_cell_t
						   meltxargdescr_[],
						   union meltparam_un
						   *meltxargtab_,
						   const melt_argdescr_cell_t
						   meltxresdescr_[],
						   union meltparam_un
						   *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_98_warmelt_outobj_LAMBDA___24__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_99_warmelt_outobj_OUTPUCOD_OBJTOUCH (meltclosure_ptr_t meltclosp_,
					      melt_ptr_t meltfirstargp_,
					      const melt_argdescr_cell_t
					      meltxargdescr_[],
					      union meltparam_un
					      *meltxargtab_,
					      const melt_argdescr_cell_t
					      meltxresdescr_[],
					      union meltparam_un
					      *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_100_warmelt_outobj_OUTPUCOD_DBGTRACEWRITEOBJ (meltclosure_ptr_t
						       meltclosp_,
						       melt_ptr_t
						       meltfirstargp_,
						       const
						       melt_argdescr_cell_t
						       meltxargdescr_[],
						       union meltparam_un
						       *meltxargtab_,
						       const
						       melt_argdescr_cell_t
						       meltxresdescr_[],
						       union meltparam_un
						       *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_101_warmelt_outobj_OUTPUCOD_OBJPUTUPLE (meltclosure_ptr_t meltclosp_,
						 melt_ptr_t meltfirstargp_,
						 const melt_argdescr_cell_t
						 meltxargdescr_[],
						 union meltparam_un
						 *meltxargtab_,
						 const melt_argdescr_cell_t
						 meltxresdescr_[],
						 union meltparam_un
						 *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_102_warmelt_outobj_OUTPUCOD_OBJPUTPAIRHEAD (meltclosure_ptr_t
						     meltclosp_,
						     melt_ptr_t
						     meltfirstargp_,
						     const
						     melt_argdescr_cell_t
						     meltxargdescr_[],
						     union meltparam_un
						     *meltxargtab_,
						     const
						     melt_argdescr_cell_t
						     meltxresdescr_[],
						     union meltparam_un
						     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_103_warmelt_outobj_OUTPUCOD_OBJPUTPAIRTAIL (meltclosure_ptr_t
						     meltclosp_,
						     melt_ptr_t
						     meltfirstargp_,
						     const
						     melt_argdescr_cell_t
						     meltxargdescr_[],
						     union meltparam_un
						     *meltxargtab_,
						     const
						     melt_argdescr_cell_t
						     meltxresdescr_[],
						     union meltparam_un
						     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_104_warmelt_outobj_OUTPUCOD_OBJPUTLIST (meltclosure_ptr_t meltclosp_,
						 melt_ptr_t meltfirstargp_,
						 const melt_argdescr_cell_t
						 meltxargdescr_[],
						 union meltparam_un
						 *meltxargtab_,
						 const melt_argdescr_cell_t
						 meltxresdescr_[],
						 union meltparam_un
						 *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_105_warmelt_outobj_OUTPUCOD_OBJGETSLOT (meltclosure_ptr_t meltclosp_,
						 melt_ptr_t meltfirstargp_,
						 const melt_argdescr_cell_t
						 meltxargdescr_[],
						 union meltparam_un
						 *meltxargtab_,
						 const melt_argdescr_cell_t
						 meltxresdescr_[],
						 union meltparam_un
						 *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_106_warmelt_outobj_LAMBDA___25__ (meltclosure_ptr_t meltclosp_,
					   melt_ptr_t meltfirstargp_,
					   const melt_argdescr_cell_t
					   meltxargdescr_[],
					   union meltparam_un *meltxargtab_,
					   const melt_argdescr_cell_t
					   meltxresdescr_[],
					   union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_107_warmelt_outobj_OUTPUCOD_OBJPUTSLOT (meltclosure_ptr_t meltclosp_,
						 melt_ptr_t meltfirstargp_,
						 const melt_argdescr_cell_t
						 meltxargdescr_[],
						 union meltparam_un
						 *meltxargtab_,
						 const melt_argdescr_cell_t
						 meltxresdescr_[],
						 union meltparam_un
						 *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_108_warmelt_outobj_OUTPUCOD_OBJPUTCLOSUROUT (meltclosure_ptr_t
						      meltclosp_,
						      melt_ptr_t
						      meltfirstargp_,
						      const
						      melt_argdescr_cell_t
						      meltxargdescr_[],
						      union meltparam_un
						      *meltxargtab_,
						      const
						      melt_argdescr_cell_t
						      meltxresdescr_[],
						      union meltparam_un
						      *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_109_warmelt_outobj_OUTPUCOD_OBJPUTCLOSEDV (meltclosure_ptr_t
						    meltclosp_,
						    melt_ptr_t meltfirstargp_,
						    const melt_argdescr_cell_t
						    meltxargdescr_[],
						    union meltparam_un
						    *meltxargtab_,
						    const melt_argdescr_cell_t
						    meltxresdescr_[],
						    union meltparam_un
						    *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_110_warmelt_outobj_OUTPUCOD_OBJPUTCLOSEDNOTNULLV (meltclosure_ptr_t
							   meltclosp_,
							   melt_ptr_t
							   meltfirstargp_,
							   const
							   melt_argdescr_cell_t
							   meltxargdescr_[],
							   union meltparam_un
							   *meltxargtab_,
							   const
							   melt_argdescr_cell_t
							   meltxresdescr_[],
							   union meltparam_un
							   *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_111_warmelt_outobj_OUTPUCOD_OBJPUTROUTCONST (meltclosure_ptr_t
						      meltclosp_,
						      melt_ptr_t
						      meltfirstargp_,
						      const
						      melt_argdescr_cell_t
						      meltxargdescr_[],
						      union meltparam_un
						      *meltxargtab_,
						      const
						      melt_argdescr_cell_t
						      meltxresdescr_[],
						      union meltparam_un
						      *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_112_warmelt_outobj_OUTPUCOD_OBJPUTROUTCONSTNOTNULL (meltclosure_ptr_t
							     meltclosp_,
							     melt_ptr_t
							     meltfirstargp_,
							     const
							     melt_argdescr_cell_t
							     meltxargdescr_[],
							     union
							     meltparam_un
							     *meltxargtab_,
							     const
							     melt_argdescr_cell_t
							     meltxresdescr_[],
							     union
							     meltparam_un
							     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_113_warmelt_outobj_OUTPUCOD_OBJPUTXTRARESULT (meltclosure_ptr_t
						       meltclosp_,
						       melt_ptr_t
						       meltfirstargp_,
						       const
						       melt_argdescr_cell_t
						       meltxargdescr_[],
						       union meltparam_un
						       *meltxargtab_,
						       const
						       melt_argdescr_cell_t
						       meltxresdescr_[],
						       union meltparam_un
						       *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_114_warmelt_outobj_OUTPUCOD_OBJEXPV (meltclosure_ptr_t meltclosp_,
					      melt_ptr_t meltfirstargp_,
					      const melt_argdescr_cell_t
					      meltxargdescr_[],
					      union meltparam_un
					      *meltxargtab_,
					      const melt_argdescr_cell_t
					      meltxresdescr_[],
					      union meltparam_un
					      *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_115_warmelt_outobj_OUTPUCOD_OBJLOCATEDEXPV (meltclosure_ptr_t
						     meltclosp_,
						     melt_ptr_t
						     meltfirstargp_,
						     const
						     melt_argdescr_cell_t
						     meltxargdescr_[],
						     union meltparam_un
						     *meltxargtab_,
						     const
						     melt_argdescr_cell_t
						     meltxresdescr_[],
						     union meltparam_un
						     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_116_warmelt_outobj_OUTPUCOD_VERBATIMSTRING (meltclosure_ptr_t
						     meltclosp_,
						     melt_ptr_t
						     meltfirstargp_,
						     const
						     melt_argdescr_cell_t
						     meltxargdescr_[],
						     union meltparam_un
						     *meltxargtab_,
						     const
						     melt_argdescr_cell_t
						     meltxresdescr_[],
						     union meltparam_un
						     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_117_warmelt_outobj_OUTPUCOD_STRING (meltclosure_ptr_t meltclosp_,
					     melt_ptr_t meltfirstargp_,
					     const melt_argdescr_cell_t
					     meltxargdescr_[],
					     union meltparam_un *meltxargtab_,
					     const melt_argdescr_cell_t
					     meltxresdescr_[],
					     union meltparam_un
					     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_118_warmelt_outobj_OUTPUCOD_INTEGER (meltclosure_ptr_t meltclosp_,
					      melt_ptr_t meltfirstargp_,
					      const melt_argdescr_cell_t
					      meltxargdescr_[],
					      union meltparam_un
					      *meltxargtab_,
					      const melt_argdescr_cell_t
					      meltxresdescr_[],
					      union meltparam_un
					      *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_119_warmelt_outobj_OUTPUCOD_FINALRETURN (meltclosure_ptr_t
						  meltclosp_,
						  melt_ptr_t meltfirstargp_,
						  const melt_argdescr_cell_t
						  meltxargdescr_[],
						  union meltparam_un
						  *meltxargtab_,
						  const melt_argdescr_cell_t
						  meltxresdescr_[],
						  union meltparam_un
						  *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_120_warmelt_outobj_SORTED_NAMED_DICT_TUPLE (meltclosure_ptr_t
						     meltclosp_,
						     melt_ptr_t
						     meltfirstargp_,
						     const
						     melt_argdescr_cell_t
						     meltxargdescr_[],
						     union meltparam_un
						     *meltxargtab_,
						     const
						     melt_argdescr_cell_t
						     meltxresdescr_[],
						     union meltparam_un
						     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_121_warmelt_outobj_OUTPUT_EXPORTED_OFFSETS (meltclosure_ptr_t
						     meltclosp_,
						     melt_ptr_t
						     meltfirstargp_,
						     const
						     melt_argdescr_cell_t
						     meltxargdescr_[],
						     union meltparam_un
						     *meltxargtab_,
						     const
						     melt_argdescr_cell_t
						     meltxresdescr_[],
						     union meltparam_un
						     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_122_warmelt_outobj_NTH_SECUNDARY_FILE (meltclosure_ptr_t meltclosp_,
						melt_ptr_t meltfirstargp_,
						const melt_argdescr_cell_t
						meltxargdescr_[],
						union meltparam_un
						*meltxargtab_,
						const melt_argdescr_cell_t
						meltxresdescr_[],
						union meltparam_un
						*meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_123_warmelt_outobj_OUTPUT_MELT_DESCRIPTOR (meltclosure_ptr_t
						    meltclosp_,
						    melt_ptr_t meltfirstargp_,
						    const melt_argdescr_cell_t
						    meltxargdescr_[],
						    union meltparam_un
						    *meltxargtab_,
						    const melt_argdescr_cell_t
						    meltxresdescr_[],
						    union meltparam_un
						    *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_124_warmelt_outobj_SYNTESTGEN_ANY (meltclosure_ptr_t meltclosp_,
					    melt_ptr_t meltfirstargp_,
					    const melt_argdescr_cell_t
					    meltxargdescr_[],
					    union meltparam_un *meltxargtab_,
					    const melt_argdescr_cell_t
					    meltxresdescr_[],
					    union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_125_warmelt_outobj_SUBSTITUTE_FORMALS_FOR_SYNTEST (meltclosure_ptr_t
							    meltclosp_,
							    melt_ptr_t
							    meltfirstargp_,
							    const
							    melt_argdescr_cell_t
							    meltxargdescr_[],
							    union meltparam_un
							    *meltxargtab_,
							    const
							    melt_argdescr_cell_t
							    meltxresdescr_[],
							    union meltparam_un
							    *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_126_warmelt_outobj_EXPAND_TUPLE_FOR_SYNTEST (meltclosure_ptr_t
						      meltclosp_,
						      melt_ptr_t
						      meltfirstargp_,
						      const
						      melt_argdescr_cell_t
						      meltxargdescr_[],
						      union meltparam_un
						      *meltxargtab_,
						      const
						      melt_argdescr_cell_t
						      meltxresdescr_[],
						      union meltparam_un
						      *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_127_warmelt_outobj_SYNTESTGEN_PRIMITIVE (meltclosure_ptr_t
						  meltclosp_,
						  melt_ptr_t meltfirstargp_,
						  const melt_argdescr_cell_t
						  meltxargdescr_[],
						  union meltparam_un
						  *meltxargtab_,
						  const melt_argdescr_cell_t
						  meltxresdescr_[],
						  union meltparam_un
						  *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_128_warmelt_outobj_SYNTESTGEN_CITERATOR (meltclosure_ptr_t
						  meltclosp_,
						  melt_ptr_t meltfirstargp_,
						  const melt_argdescr_cell_t
						  meltxargdescr_[],
						  union meltparam_un
						  *meltxargtab_,
						  const melt_argdescr_cell_t
						  meltxresdescr_[],
						  union meltparam_un
						  *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_129_warmelt_outobj_SYNTESTGEN_CMATCHER (meltclosure_ptr_t meltclosp_,
						 melt_ptr_t meltfirstargp_,
						 const melt_argdescr_cell_t
						 meltxargdescr_[],
						 union meltparam_un
						 *meltxargtab_,
						 const melt_argdescr_cell_t
						 meltxresdescr_[],
						 union meltparam_un
						 *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_130_warmelt_outobj_EMIT_SYNTAX_TESTING_ROUTINE (meltclosure_ptr_t
							 meltclosp_,
							 melt_ptr_t
							 meltfirstargp_,
							 const
							 melt_argdescr_cell_t
							 meltxargdescr_[],
							 union meltparam_un
							 *meltxargtab_,
							 const
							 melt_argdescr_cell_t
							 meltxresdescr_[],
							 union meltparam_un
							 *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_131_warmelt_outobj_NORMADECLB_MACROEXPANDED_LIST (meltclosure_ptr_t
							   meltclosp_,
							   melt_ptr_t
							   meltfirstargp_,
							   const
							   melt_argdescr_cell_t
							   meltxargdescr_[],
							   union meltparam_un
							   *meltxargtab_,
							   const
							   melt_argdescr_cell_t
							   meltxresdescr_[],
							   union meltparam_un
							   *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_132_warmelt_outobj_LAMBDA___26__ (meltclosure_ptr_t meltclosp_,
					   melt_ptr_t meltfirstargp_,
					   const melt_argdescr_cell_t
					   meltxargdescr_[],
					   union meltparam_un *meltxargtab_,
					   const melt_argdescr_cell_t
					   meltxresdescr_[],
					   union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_133_warmelt_outobj_TRANSLATE_MACROEXPANDED_LIST (meltclosure_ptr_t
							  meltclosp_,
							  melt_ptr_t
							  meltfirstargp_,
							  const
							  melt_argdescr_cell_t
							  meltxargdescr_[],
							  union meltparam_un
							  *meltxargtab_,
							  const
							  melt_argdescr_cell_t
							  meltxresdescr_[],
							  union meltparam_un
							  *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_134_warmelt_outobj_LAMBDA___27__ (meltclosure_ptr_t meltclosp_,
					   melt_ptr_t meltfirstargp_,
					   const melt_argdescr_cell_t
					   meltxargdescr_[],
					   union meltparam_un *meltxargtab_,
					   const melt_argdescr_cell_t
					   meltxresdescr_[],
					   union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_135_warmelt_outobj_FATAL_COMPILE_ERROR (meltclosure_ptr_t meltclosp_,
						 melt_ptr_t meltfirstargp_,
						 const melt_argdescr_cell_t
						 meltxargdescr_[],
						 union meltparam_un
						 *meltxargtab_,
						 const melt_argdescr_cell_t
						 meltxresdescr_[],
						 union meltparam_un
						 *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_136_warmelt_outobj_COMPILE_LIST_SEXPR (meltclosure_ptr_t meltclosp_,
						melt_ptr_t meltfirstargp_,
						const melt_argdescr_cell_t
						meltxargdescr_[],
						union meltparam_un
						*meltxargtab_,
						const melt_argdescr_cell_t
						meltxresdescr_[],
						union meltparam_un
						*meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_137_warmelt_outobj_LAMBDA___28__ (meltclosure_ptr_t meltclosp_,
					   melt_ptr_t meltfirstargp_,
					   const melt_argdescr_cell_t
					   meltxargdescr_[],
					   union meltparam_un *meltxargtab_,
					   const melt_argdescr_cell_t
					   meltxresdescr_[],
					   union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_138_warmelt_outobj_AUTOBOX_NORMAL_RETURN (meltclosure_ptr_t
						   meltclosp_,
						   melt_ptr_t meltfirstargp_,
						   const melt_argdescr_cell_t
						   meltxargdescr_[],
						   union meltparam_un
						   *meltxargtab_,
						   const melt_argdescr_cell_t
						   meltxresdescr_[],
						   union meltparam_un
						   *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_139_warmelt_outobj_TRANSLATE_RUN_MELT_EXPRESSIONS (meltclosure_ptr_t
							    meltclosp_,
							    melt_ptr_t
							    meltfirstargp_,
							    const
							    melt_argdescr_cell_t
							    meltxargdescr_[],
							    union meltparam_un
							    *meltxargtab_,
							    const
							    melt_argdescr_cell_t
							    meltxresdescr_[],
							    union meltparam_un
							    *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_140_warmelt_outobj_LAMBDA___29__ (meltclosure_ptr_t meltclosp_,
					   melt_ptr_t meltfirstargp_,
					   const melt_argdescr_cell_t
					   meltxargdescr_[],
					   union meltparam_un *meltxargtab_,
					   const melt_argdescr_cell_t
					   meltxresdescr_[],
					   union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_141_warmelt_outobj_LAMBDA___30__ (meltclosure_ptr_t meltclosp_,
					   melt_ptr_t meltfirstargp_,
					   const melt_argdescr_cell_t
					   meltxargdescr_[],
					   union meltparam_un *meltxargtab_,
					   const melt_argdescr_cell_t
					   meltxresdescr_[],
					   union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_142_warmelt_outobj_LAMBDA___31__ (meltclosure_ptr_t meltclosp_,
					   melt_ptr_t meltfirstargp_,
					   const melt_argdescr_cell_t
					   meltxargdescr_[],
					   union meltparam_un *meltxargtab_,
					   const melt_argdescr_cell_t
					   meltxresdescr_[],
					   union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_143_warmelt_outobj_READ_MELT_EXPRESSIONS (meltclosure_ptr_t
						   meltclosp_,
						   melt_ptr_t meltfirstargp_,
						   const melt_argdescr_cell_t
						   meltxargdescr_[],
						   union meltparam_un
						   *meltxargtab_,
						   const melt_argdescr_cell_t
						   meltxresdescr_[],
						   union meltparam_un
						   *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_144_warmelt_outobj_TRANSLATE_TO_C_MODULE_MELT_SOURCES
(meltclosure_ptr_t meltclosp_, melt_ptr_t meltfirstargp_, const melt_argdescr_cell_t meltxargdescr_[],
union meltparam_un *meltxargtab_, const melt_argdescr_cell_t meltxresdescr_[],
union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_145_warmelt_outobj_GENERATE_GPLV3PLUS_COPYRIGHT_NOTICE_C_COMMENT
(meltclosure_ptr_t meltclosp_, melt_ptr_t meltfirstargp_, const melt_argdescr_cell_t meltxargdescr_[],
union meltparam_un *meltxargtab_, const melt_argdescr_cell_t meltxresdescr_[],
union meltparam_un *meltxrestab_);



MELT_EXTERN void *melt_start_this_module (void *);


/* define different names when debugging or not */
#if MELT_HAVE_DEBUG
MELT_EXTERN const char meltmodule_warmelt_outobj__melt_have_debug_enabled[];
#define melt_have_debug_string meltmodule_warmelt_outobj__melt_have_debug_enabled
#else /*!MELT_HAVE_DEBUG */
MELT_EXTERN const char meltmodule_warmelt_outobj__melt_have_debug_disabled[];
#define melt_have_debug_string meltmodule_warmelt_outobj__melt_have_debug_disabled
#endif /*!MELT_HAVE_DEBUG */


struct frame_melt_start_this_module_st;
void MELT_MODULE_VISIBILITY
meltmod__warmelt_outobj__initialmeltchunk_0 (struct
					     frame_melt_start_this_module_st
					     *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_outobj__initialmeltchunk_1 (struct
					     frame_melt_start_this_module_st
					     *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_outobj__initialmeltchunk_2 (struct
					     frame_melt_start_this_module_st
					     *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_outobj__initialmeltchunk_3 (struct
					     frame_melt_start_this_module_st
					     *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_outobj__initialmeltchunk_4 (struct
					     frame_melt_start_this_module_st
					     *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_outobj__initialmeltchunk_5 (struct
					     frame_melt_start_this_module_st
					     *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_outobj__initialmeltchunk_6 (struct
					     frame_melt_start_this_module_st
					     *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_outobj__initialmeltchunk_7 (struct
					     frame_melt_start_this_module_st
					     *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_outobj__initialmeltchunk_8 (struct
					     frame_melt_start_this_module_st
					     *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_outobj__initialmeltchunk_9 (struct
					     frame_melt_start_this_module_st
					     *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_outobj__initialmeltchunk_10 (struct
					      frame_melt_start_this_module_st
					      *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_outobj__initialmeltchunk_11 (struct
					      frame_melt_start_this_module_st
					      *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_outobj__initialmeltchunk_12 (struct
					      frame_melt_start_this_module_st
					      *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_outobj__initialmeltchunk_13 (struct
					      frame_melt_start_this_module_st
					      *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_outobj__initialmeltchunk_14 (struct
					      frame_melt_start_this_module_st
					      *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_outobj__initialmeltchunk_15 (struct
					      frame_melt_start_this_module_st
					      *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_outobj__initialmeltchunk_16 (struct
					      frame_melt_start_this_module_st
					      *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_outobj__initialmeltchunk_17 (struct
					      frame_melt_start_this_module_st
					      *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_outobj__forward_or_mark_module_start_frame (struct
							     melt_callframe_st
							     *fp,
							     int marking);
#define meltmarking_melt_start_this_module  meltmod__warmelt_outobj__forward_or_mark_module_start_frame



/**** warmelt-outobj+04.c implementations ****/




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_104_warmelt_outobj_OUTPUCOD_OBJPUTLIST (meltclosure_ptr_t meltclosp_,
						 melt_ptr_t meltfirstargp_,
						 const melt_argdescr_cell_t
						 meltxargdescr_[],
						 union meltparam_un
						 *meltxargtab_,
						 const melt_argdescr_cell_t
						 meltxresdescr_[],
						 union meltparam_un
						 *meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_104_warmelt_outobj_OUTPUCOD_OBJPUTLIST_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_104_warmelt_outobj_OUTPUCOD_OBJPUTLIST_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 18
    melt_ptr_t mcfr_varptr[18];
#define MELTFRAM_NBVARNUM 4
    long mcfr_varnum[4];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_104_warmelt_outobj_OUTPUCOD_OBJPUTLIST is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_104_warmelt_outobj_OUTPUCOD_OBJPUTLIST_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 18; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_104_warmelt_outobj_OUTPUCOD_OBJPUTLIST nbval 18*/
  meltfram__.mcfr_nbvar = 18 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("OUTPUCOD_OBJPUTLIST", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-outobj.melt:4014:/ getarg");
 /*_.OPUT__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.DECLBUF__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.DECLBUF__V3*/ meltfptr[2])) !=
	      NULL);


  /*getarg#2 */
  /*^getarg */
  if (meltxargdescr_[1] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.IMPLBUF__V4*/ meltfptr[3] =
    (meltxargtab_[1].meltbp_aptr) ? (*(meltxargtab_[1].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3])) !=
	      NULL);


  /*getarg#3 */
  /*^getarg */
  if (meltxargdescr_[2] != MELTBPAR_LONG)
    goto lab_endgetargs;
 /*_#DEPTH__L1*/ meltfnum[0] = meltxargtab_[2].meltbp_long;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {

    MELT_LOCATION ("warmelt-outobj.melt:4015:/ quasiblock");


    /*^getslot */
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.OPUT__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 0, "OBI_LOC");
  /*_.OLOC__V6*/ meltfptr[5] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-outobj.melt:4016:/ cond");
    /*cond */ if (
		   /*ifisa */
		   melt_is_instance_of ((melt_ptr_t)
					( /*_.OPUT__V2*/ meltfptr[1]),
					(melt_ptr_t) (( /*!CLASS_OBJPUTLIST */
						       meltfrout->tabval[0])))
      )				/*then */
      {
	/*^cond.then */
	/*^getslot */
	{
	  melt_ptr_t slot = NULL, obj = NULL;
	  obj = (melt_ptr_t) ( /*_.OPUT__V2*/ meltfptr[1]) /*=obj*/ ;
	  melt_object_get_field (slot, obj, 1, "OPUTL_LIST");
   /*_.OLIST__V7*/ meltfptr[6] = slot;
	};
	;
      }
    else
      {				/*^cond.else */

  /*_.OLIST__V7*/ meltfptr[6] = NULL;;
      }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:4017:/ cond");
    /*cond */ if (
		   /*ifisa */
		   melt_is_instance_of ((melt_ptr_t)
					( /*_.OPUT__V2*/ meltfptr[1]),
					(melt_ptr_t) (( /*!CLASS_OBJPUTLIST */
						       meltfrout->tabval[0])))
      )				/*then */
      {
	/*^cond.then */
	/*^getslot */
	{
	  melt_ptr_t slot = NULL, obj = NULL;
	  obj = (melt_ptr_t) ( /*_.OPUT__V2*/ meltfptr[1]) /*=obj*/ ;
	  melt_object_get_field (slot, obj, 2, "OPUTL_FIRST");
   /*_.OFIRST__V8*/ meltfptr[7] = slot;
	};
	;
      }
    else
      {				/*^cond.else */

  /*_.OFIRST__V8*/ meltfptr[7] = NULL;;
      }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:4018:/ cond");
    /*cond */ if (
		   /*ifisa */
		   melt_is_instance_of ((melt_ptr_t)
					( /*_.OPUT__V2*/ meltfptr[1]),
					(melt_ptr_t) (( /*!CLASS_OBJPUTLIST */
						       meltfrout->tabval[0])))
      )				/*then */
      {
	/*^cond.then */
	/*^getslot */
	{
	  melt_ptr_t slot = NULL, obj = NULL;
	  obj = (melt_ptr_t) ( /*_.OPUT__V2*/ meltfptr[1]) /*=obj*/ ;
	  melt_object_get_field (slot, obj, 3, "OPUTL_LAST");
   /*_.OLAST__V9*/ meltfptr[8] = slot;
	};
	;
      }
    else
      {				/*^cond.else */

  /*_.OLAST__V9*/ meltfptr[8] = NULL;;
      }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:4020:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[3];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.IMPLBUF__V4*/ meltfptr[3];
      /*^apply.arg */
      argtab[1].meltbp_long = /*_#DEPTH__L1*/ meltfnum[0];
      /*^apply.arg */
      argtab[2].meltbp_cstring = "putlist";
      /*_.OUTPUT_LOCATION__V10*/ meltfptr[9] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!OUTPUT_LOCATION */ meltfrout->tabval[1])),
		    (melt_ptr_t) ( /*_.OLOC__V6*/ meltfptr[5]),
		    (MELTBPARSTR_PTR MELTBPARSTR_LONG MELTBPARSTR_CSTRING ""),
		    argtab, "", (union meltparam_un *) 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:4021:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   ("/*putlist*/"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:4022:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				( /*_#DEPTH__L1*/ meltfnum[0]), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:4023:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   ("melt_assertmsg(\"putlist checklist\", melt_magic_discr((melt_ptr_t)("));
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:4024:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^msend */
    /*msend */
    {
      union meltparam_un argtab[3];
      memset (&argtab, 0, sizeof (argtab));
      /*^ojbmsend.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.DECLBUF__V3*/ meltfptr[2];
      /*^ojbmsend.arg */
      argtab[1].meltbp_aptr = (melt_ptr_t *) & /*_.IMPLBUF__V4*/ meltfptr[3];
      /*^ojbmsend.arg */
      argtab[2].meltbp_long = /*_#DEPTH__L1*/ meltfnum[0];
      /*_.OUTPUT_C_CODE__V11*/ meltfptr[10] =
	meltgc_send ((melt_ptr_t) ( /*_.OLIST__V7*/ meltfptr[6]),
		     (melt_ptr_t) (( /*!OUTPUT_C_CODE */ meltfrout->
				    tabval[2])),
		     (MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_LONG ""),
		     argtab, "", (union meltparam_un *) 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:4025:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   ("))== MELTOBMAG_LIST);"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:4026:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				( /*_#DEPTH__L1*/ meltfnum[0]), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:4027:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   ("((meltlist_ptr_t)("));
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:4028:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^msend */
    /*msend */
    {
      union meltparam_un argtab[3];
      memset (&argtab, 0, sizeof (argtab));
      /*^ojbmsend.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.DECLBUF__V3*/ meltfptr[2];
      /*^ojbmsend.arg */
      argtab[1].meltbp_aptr = (melt_ptr_t *) & /*_.IMPLBUF__V4*/ meltfptr[3];
      /*^ojbmsend.arg */
      argtab[2].meltbp_long = /*_#DEPTH__L1*/ meltfnum[0];
      /*_.OUTPUT_C_CODE__V12*/ meltfptr[11] =
	meltgc_send ((melt_ptr_t) ( /*_.OLIST__V7*/ meltfptr[6]),
		     (melt_ptr_t) (( /*!OUTPUT_C_CODE */ meltfrout->
				    tabval[2])),
		     (MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_LONG ""),
		     argtab, "", (union meltparam_un *) 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:4029:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   ("))->first = (meltpair_ptr_t) ("));
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:4030:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^msend */
    /*msend */
    {
      union meltparam_un argtab[3];
      memset (&argtab, 0, sizeof (argtab));
      /*^ojbmsend.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.DECLBUF__V3*/ meltfptr[2];
      /*^ojbmsend.arg */
      argtab[1].meltbp_aptr = (melt_ptr_t *) & /*_.IMPLBUF__V4*/ meltfptr[3];
      /*^ojbmsend.arg */
      argtab[2].meltbp_long = /*_#DEPTH__L1*/ meltfnum[0];
      /*_.OUTPUT_C_CODE__V13*/ meltfptr[12] =
	meltgc_send ((melt_ptr_t) ( /*_.OFIRST__V8*/ meltfptr[7]),
		     (melt_ptr_t) (( /*!OUTPUT_C_CODE */ meltfrout->
				    tabval[2])),
		     (MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_LONG ""),
		     argtab, "", (union meltparam_un *) 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:4031:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   (");"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:4032:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				( /*_#DEPTH__L1*/ meltfnum[0]), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:4033:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   ("((meltlist_ptr_t)("));
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:4034:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^msend */
    /*msend */
    {
      union meltparam_un argtab[3];
      memset (&argtab, 0, sizeof (argtab));
      /*^ojbmsend.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.DECLBUF__V3*/ meltfptr[2];
      /*^ojbmsend.arg */
      argtab[1].meltbp_aptr = (melt_ptr_t *) & /*_.IMPLBUF__V4*/ meltfptr[3];
      /*^ojbmsend.arg */
      argtab[2].meltbp_long = /*_#DEPTH__L1*/ meltfnum[0];
      /*_.OUTPUT_C_CODE__V14*/ meltfptr[13] =
	meltgc_send ((melt_ptr_t) ( /*_.OLIST__V7*/ meltfptr[6]),
		     (melt_ptr_t) (( /*!OUTPUT_C_CODE */ meltfrout->
				    tabval[2])),
		     (MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_LONG ""),
		     argtab, "", (union meltparam_un *) 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:4035:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   ("))->last = (meltpair_ptr_t) ("));
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:4036:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^msend */
    /*msend */
    {
      union meltparam_un argtab[3];
      memset (&argtab, 0, sizeof (argtab));
      /*^ojbmsend.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.DECLBUF__V3*/ meltfptr[2];
      /*^ojbmsend.arg */
      argtab[1].meltbp_aptr = (melt_ptr_t *) & /*_.IMPLBUF__V4*/ meltfptr[3];
      /*^ojbmsend.arg */
      argtab[2].meltbp_long = /*_#DEPTH__L1*/ meltfnum[0];
      /*_.OUTPUT_C_CODE__V15*/ meltfptr[14] =
	meltgc_send ((melt_ptr_t) ( /*_.OLAST__V9*/ meltfptr[8]),
		     (melt_ptr_t) (( /*!OUTPUT_C_CODE */ meltfrout->
				    tabval[2])),
		     (MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_LONG ""),
		     argtab, "", (union meltparam_un *) 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:4037:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   (");"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:4038:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				( /*_#DEPTH__L1*/ meltfnum[0]), 0);
    }
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:4039:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#STRBUF_USEDLENGTH__L2*/ meltfnum[1] =
	melt_strbuf_usedlength ((melt_ptr_t)
				( /*_.IMPLBUF__V4*/ meltfptr[3]));;
      MELT_LOCATION ("warmelt-outobj.melt:4040:/ cond");
      /*cond */ if (
		     /*ifisa */
		     melt_is_instance_of ((melt_ptr_t)
					  (( /*!BUFFER_LIMIT_CONT */
					    meltfrout->tabval[3])),
					  (melt_ptr_t) (( /*!CLASS_REFERENCE */ meltfrout->tabval[4])))
	)			/*then */
	{
	  /*^cond.then */
	  /*^getslot */
	  {
	    melt_ptr_t slot = NULL, obj = NULL;
	    obj =
	      (melt_ptr_t) (( /*!BUFFER_LIMIT_CONT */ meltfrout->
			     tabval[3])) /*=obj*/ ;
	    melt_object_get_field (slot, obj, 0, "REFERENCED_VALUE");
     /*_.REFERENCED_VALUE__V17*/ meltfptr[16] = slot;
	  };
	  ;
	}
      else
	{			/*^cond.else */

    /*_.REFERENCED_VALUE__V17*/ meltfptr[16] = NULL;;
	}
      ;
      /*^compute */
   /*_#GET_INT__L3*/ meltfnum[2] =
	(melt_get_int
	 ((melt_ptr_t) ( /*_.REFERENCED_VALUE__V17*/ meltfptr[16])));;
      /*^compute */
   /*_#I__L4*/ meltfnum[3] =
	(( /*_#STRBUF_USEDLENGTH__L2*/ meltfnum[1]) <
	 ( /*_#GET_INT__L3*/ meltfnum[2]));;
      MELT_LOCATION ("warmelt-outobj.melt:4039:/ cond");
      /*cond */ if ( /*_#I__L4*/ meltfnum[3])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V18*/ meltfptr[17] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-outobj.melt:4039:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check limited implbuf"),
				  ("warmelt-outobj.melt")
				  ? ("warmelt-outobj.melt") : __FILE__,
				  (4039) ? (4039) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V18*/ meltfptr[17] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V16*/ meltfptr[15] = /*_.IFELSE___V18*/ meltfptr[17];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:4039:/ clear");
	     /*clear *//*_#STRBUF_USEDLENGTH__L2*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.REFERENCED_VALUE__V17*/ meltfptr[16] = 0;
      /*^clear */
	     /*clear *//*_#GET_INT__L3*/ meltfnum[2] = 0;
      /*^clear */
	     /*clear *//*_#I__L4*/ meltfnum[3] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V18*/ meltfptr[17] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V16*/ meltfptr[15] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    /*^compute */
    /*_.LET___V5*/ meltfptr[4] = /*_.IFCPP___V16*/ meltfptr[15];;

    MELT_LOCATION ("warmelt-outobj.melt:4015:/ clear");
	   /*clear *//*_.OLOC__V6*/ meltfptr[5] = 0;
    /*^clear */
	   /*clear *//*_.OLIST__V7*/ meltfptr[6] = 0;
    /*^clear */
	   /*clear *//*_.OFIRST__V8*/ meltfptr[7] = 0;
    /*^clear */
	   /*clear *//*_.OLAST__V9*/ meltfptr[8] = 0;
    /*^clear */
	   /*clear *//*_.OUTPUT_LOCATION__V10*/ meltfptr[9] = 0;
    /*^clear */
	   /*clear *//*_.OUTPUT_C_CODE__V11*/ meltfptr[10] = 0;
    /*^clear */
	   /*clear *//*_.OUTPUT_C_CODE__V12*/ meltfptr[11] = 0;
    /*^clear */
	   /*clear *//*_.OUTPUT_C_CODE__V13*/ meltfptr[12] = 0;
    /*^clear */
	   /*clear *//*_.OUTPUT_C_CODE__V14*/ meltfptr[13] = 0;
    /*^clear */
	   /*clear *//*_.OUTPUT_C_CODE__V15*/ meltfptr[14] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V16*/ meltfptr[15] = 0;
    MELT_LOCATION ("warmelt-outobj.melt:4014:/ quasiblock");


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.LET___V5*/ meltfptr[4];;

    {
      MELT_LOCATION ("warmelt-outobj.melt:4014:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.LET___V5*/ meltfptr[4] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("OUTPUCOD_OBJPUTLIST", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_104_warmelt_outobj_OUTPUCOD_OBJPUTLIST_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_104_warmelt_outobj_OUTPUCOD_OBJPUTLIST */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_105_warmelt_outobj_OUTPUCOD_OBJGETSLOT (meltclosure_ptr_t meltclosp_,
						 melt_ptr_t meltfirstargp_,
						 const melt_argdescr_cell_t
						 meltxargdescr_[],
						 union meltparam_un *
						 meltxargtab_,
						 const melt_argdescr_cell_t
						 meltxresdescr_[],
						 union meltparam_un *
						 meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_105_warmelt_outobj_OUTPUCOD_OBJGETSLOT_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_105_warmelt_outobj_OUTPUCOD_OBJGETSLOT_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 23
    melt_ptr_t mcfr_varptr[23];
#define MELTFRAM_NBVARNUM 11
    long mcfr_varnum[11];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_105_warmelt_outobj_OUTPUCOD_OBJGETSLOT is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_105_warmelt_outobj_OUTPUCOD_OBJGETSLOT_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 23; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_105_warmelt_outobj_OUTPUCOD_OBJGETSLOT nbval 23*/
  meltfram__.mcfr_nbvar = 23 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("OUTPUCOD_OBJGETSLOT", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-outobj.melt:4045:/ getarg");
 /*_.OGSL__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.DECLBUF__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.DECLBUF__V3*/ meltfptr[2])) !=
	      NULL);


  /*getarg#2 */
  /*^getarg */
  if (meltxargdescr_[1] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.IMPLBUF__V4*/ meltfptr[3] =
    (meltxargtab_[1].meltbp_aptr) ? (*(meltxargtab_[1].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3])) !=
	      NULL);


  /*getarg#3 */
  /*^getarg */
  if (meltxargdescr_[2] != MELTBPAR_LONG)
    goto lab_endgetargs;
 /*_#DEPTH__L1*/ meltfnum[0] = meltxargtab_[2].meltbp_long;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:4046:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L2*/ meltfnum[1] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.OGSL__V2*/ meltfptr[1]),
			     (melt_ptr_t) (( /*!CLASS_OBJGETSLOT */
					    meltfrout->tabval[0])));;
      MELT_LOCATION ("warmelt-outobj.melt:4046:/ cond");
      /*cond */ if ( /*_#IS_A__L2*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V6*/ meltfptr[5] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-outobj.melt:4046:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check ogsl"),
				  ("warmelt-outobj.melt")
				  ? ("warmelt-outobj.melt") : __FILE__,
				  (4046) ? (4046) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V6*/ meltfptr[5] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V5*/ meltfptr[4] = /*_.IFELSE___V6*/ meltfptr[5];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:4046:/ clear");
	     /*clear *//*_#IS_A__L2*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V6*/ meltfptr[5] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V5*/ meltfptr[4] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-outobj.melt:4047:/ quasiblock");


    /*^getslot */
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.OGSL__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 0, "OBI_LOC");
  /*_.OLOC__V8*/ meltfptr[7] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-outobj.melt:4048:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.OGSL__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 1, "OBDI_DESTLIST");
  /*_.DESTLIST__V9*/ meltfptr[8] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-outobj.melt:4049:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.OGSL__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 2, "OGETSL_OBJ");
  /*_.OOBJ__V10*/ meltfptr[9] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-outobj.melt:4050:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.OGSL__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 3, "OGETSL_FIELD");
  /*_.OFIELD__V11*/ meltfptr[10] = slot;
    };
    ;
 /*_#I__L3*/ meltfnum[1] =
      (( /*_#DEPTH__L1*/ meltfnum[0]) + (1));;
    /*^compute */
 /*_.BOXDEPTHP1__V12*/ meltfptr[11] =
      (meltgc_new_int
       ((meltobject_ptr_t) (( /*!DISCR_INTEGER */ meltfrout->tabval[1])),
	( /*_#I__L3*/ meltfnum[1])));;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:4053:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L4*/ meltfnum[3] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.OFIELD__V11*/ meltfptr[10]),
			     (melt_ptr_t) (( /*!CLASS_FIELD */ meltfrout->
					    tabval[2])));;
      MELT_LOCATION ("warmelt-outobj.melt:4053:/ cond");
      /*cond */ if ( /*_#IS_A__L4*/ meltfnum[3])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V14*/ meltfptr[13] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-outobj.melt:4053:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check ofield"),
				  ("warmelt-outobj.melt")
				  ? ("warmelt-outobj.melt") : __FILE__,
				  (4053) ? (4053) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V14*/ meltfptr[13] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V13*/ meltfptr[12] = /*_.IFELSE___V14*/ meltfptr[13];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:4053:/ clear");
	     /*clear *//*_#IS_A__L4*/ meltfnum[3] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V14*/ meltfptr[13] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V13*/ meltfptr[12] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-outobj.melt:4054:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[3];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.IMPLBUF__V4*/ meltfptr[3];
      /*^apply.arg */
      argtab[1].meltbp_long = /*_#DEPTH__L1*/ meltfnum[0];
      /*^apply.arg */
      argtab[2].meltbp_cstring = "getslot";
      /*_.OUTPUT_LOCATION__V15*/ meltfptr[13] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!OUTPUT_LOCATION */ meltfrout->tabval[3])),
		    (melt_ptr_t) ( /*_.OLOC__V8*/ meltfptr[7]),
		    (MELTBPARSTR_PTR MELTBPARSTR_LONG MELTBPARSTR_CSTRING ""),
		    argtab, "", (union meltparam_un *) 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:4055:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   ("{ melt_ptr_t slot=NULL, obj=NULL;"));
    }
    ;
 /*_#I__L5*/ meltfnum[3] =
      ((1) + ( /*_#DEPTH__L1*/ meltfnum[0]));;

    {
      MELT_LOCATION ("warmelt-outobj.melt:4056:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				( /*_#I__L5*/ meltfnum[3]), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:4057:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   ("obj = (melt_ptr_t)("));
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:4058:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^msend */
    /*msend */
    {
      union meltparam_un argtab[3];
      memset (&argtab, 0, sizeof (argtab));
      /*^ojbmsend.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.DECLBUF__V3*/ meltfptr[2];
      /*^ojbmsend.arg */
      argtab[1].meltbp_aptr = (melt_ptr_t *) & /*_.IMPLBUF__V4*/ meltfptr[3];
      /*^ojbmsend.arg */
      argtab[2].meltbp_long = /*_#DEPTH__L1*/ meltfnum[0];
      /*_.OUTPUT_C_CODE__V16*/ meltfptr[15] =
	meltgc_send ((melt_ptr_t) ( /*_.OOBJ__V10*/ meltfptr[9]),
		     (melt_ptr_t) (( /*!OUTPUT_C_CODE */ meltfrout->
				    tabval[4])),
		     (MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_LONG ""),
		     argtab, "", (union meltparam_un *) 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:4059:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   (") /*=obj*/;"));
    }
    ;
 /*_#I__L6*/ meltfnum[5] =
      ((1) + ( /*_#DEPTH__L1*/ meltfnum[0]));;

    {
      MELT_LOCATION ("warmelt-outobj.melt:4060:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				( /*_#I__L6*/ meltfnum[5]), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:4061:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   ("melt_object_get_field(slot,obj, "));
    }
    ;
 /*_#GET_INT__L7*/ meltfnum[6] =
      (melt_get_int ((melt_ptr_t) ( /*_.OFIELD__V11*/ meltfptr[10])));;

    {
      MELT_LOCATION ("warmelt-outobj.melt:4062:/ locexp");
      meltgc_add_strbuf_dec ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			     ( /*_#GET_INT__L7*/ meltfnum[6]));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:4063:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   (", \""));
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:4064:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.OFIELD__V11*/ meltfptr[10]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 1, "NAMED_NAME");
  /*_.NAMED_NAME__V17*/ meltfptr[16] = slot;
    };
    ;

    {
      /*^locexp */
      /*add2sbuf_string */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   melt_string_str ((melt_ptr_t)
					    ( /*_.NAMED_NAME__V17*/
					     meltfptr[16])));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:4065:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   ("\");"));
    }
    ;
 /*_#I__L8*/ meltfnum[7] =
      ((1) + ( /*_#DEPTH__L1*/ meltfnum[0]));;

    {
      MELT_LOCATION ("warmelt-outobj.melt:4066:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				( /*_#I__L8*/ meltfnum[7]), 0);
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:4069:/ quasiblock");


    /*^newclosure */
		 /*newclosure *//*_.LAMBDA___V19*/ meltfptr[18] =
      (melt_ptr_t)
      meltgc_new_closure ((meltobject_ptr_t)
			  (((melt_ptr_t) (MELT_PREDEF (DISCR_CLOSURE)))),
			  (meltroutine_ptr_t) (( /*!konst_9 */ meltfrout->
						tabval[9])), (3));
    ;
    /*^putclosedv */
    /*putclosv */
    melt_assertmsg ("putclosv checkclo",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.LAMBDA___V19*/ meltfptr[18])) ==
		    MELTOBMAG_CLOSURE);
    melt_assertmsg ("putclosv checkoff", 0 >= 0
		    && 0 <
		    melt_closure_size ((melt_ptr_t)
				       ( /*_.LAMBDA___V19*/ meltfptr[18])));
    ((meltclosure_ptr_t) /*_.LAMBDA___V19*/ meltfptr[18])->tabval[0] =
      (melt_ptr_t) ( /*_.DECLBUF__V3*/ meltfptr[2]);
    ;
    /*^putclosedv */
    /*putclosv */
    melt_assertmsg ("putclosv checkclo",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.LAMBDA___V19*/ meltfptr[18])) ==
		    MELTOBMAG_CLOSURE);
    melt_assertmsg ("putclosv checkoff", 1 >= 0
		    && 1 <
		    melt_closure_size ((melt_ptr_t)
				       ( /*_.LAMBDA___V19*/ meltfptr[18])));
    ((meltclosure_ptr_t) /*_.LAMBDA___V19*/ meltfptr[18])->tabval[1] =
      (melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]);
    ;
    /*^putclosedv */
    /*putclosv */
    melt_assertmsg ("putclosv checkclo",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.LAMBDA___V19*/ meltfptr[18])) ==
		    MELTOBMAG_CLOSURE);
    melt_assertmsg ("putclosv checkoff", 2 >= 0
		    && 2 <
		    melt_closure_size ((melt_ptr_t)
				       ( /*_.LAMBDA___V19*/ meltfptr[18])));
    ((meltclosure_ptr_t) /*_.LAMBDA___V19*/ meltfptr[18])->tabval[2] =
      (melt_ptr_t) ( /*_.BOXDEPTHP1__V12*/ meltfptr[11]);
    ;
    /*_.LAMBDA___V18*/ meltfptr[17] = /*_.LAMBDA___V19*/ meltfptr[18];;
    MELT_LOCATION ("warmelt-outobj.melt:4067:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & /*_.LAMBDA___V18*/ meltfptr[17];
      /*_.LIST_EVERY__V20*/ meltfptr[19] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!LIST_EVERY */ meltfrout->tabval[5])),
		    (melt_ptr_t) ( /*_.DESTLIST__V9*/ meltfptr[8]),
		    (MELTBPARSTR_PTR ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:4074:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   ("slot; };"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:4075:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				( /*_#DEPTH__L1*/ meltfnum[0]), 0);
    }
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:4076:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#STRBUF_USEDLENGTH__L9*/ meltfnum[8] =
	melt_strbuf_usedlength ((melt_ptr_t)
				( /*_.IMPLBUF__V4*/ meltfptr[3]));;
      MELT_LOCATION ("warmelt-outobj.melt:4077:/ cond");
      /*cond */ if (
		     /*ifisa */
		     melt_is_instance_of ((melt_ptr_t)
					  (( /*!BUFFER_LIMIT_CONT */
					    meltfrout->tabval[10])),
					  (melt_ptr_t) (( /*!CLASS_REFERENCE */ meltfrout->tabval[11])))
	)			/*then */
	{
	  /*^cond.then */
	  /*^getslot */
	  {
	    melt_ptr_t slot = NULL, obj = NULL;
	    obj =
	      (melt_ptr_t) (( /*!BUFFER_LIMIT_CONT */ meltfrout->
			     tabval[10])) /*=obj*/ ;
	    melt_object_get_field (slot, obj, 0, "REFERENCED_VALUE");
     /*_.REFERENCED_VALUE__V22*/ meltfptr[21] = slot;
	  };
	  ;
	}
      else
	{			/*^cond.else */

    /*_.REFERENCED_VALUE__V22*/ meltfptr[21] = NULL;;
	}
      ;
      /*^compute */
   /*_#GET_INT__L10*/ meltfnum[9] =
	(melt_get_int
	 ((melt_ptr_t) ( /*_.REFERENCED_VALUE__V22*/ meltfptr[21])));;
      /*^compute */
   /*_#I__L11*/ meltfnum[10] =
	(( /*_#STRBUF_USEDLENGTH__L9*/ meltfnum[8]) <
	 ( /*_#GET_INT__L10*/ meltfnum[9]));;
      MELT_LOCATION ("warmelt-outobj.melt:4076:/ cond");
      /*cond */ if ( /*_#I__L11*/ meltfnum[10])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V23*/ meltfptr[22] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-outobj.melt:4076:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check limited implbuf"),
				  ("warmelt-outobj.melt")
				  ? ("warmelt-outobj.melt") : __FILE__,
				  (4076) ? (4076) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V23*/ meltfptr[22] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V21*/ meltfptr[20] = /*_.IFELSE___V23*/ meltfptr[22];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:4076:/ clear");
	     /*clear *//*_#STRBUF_USEDLENGTH__L9*/ meltfnum[8] = 0;
      /*^clear */
	     /*clear *//*_.REFERENCED_VALUE__V22*/ meltfptr[21] = 0;
      /*^clear */
	     /*clear *//*_#GET_INT__L10*/ meltfnum[9] = 0;
      /*^clear */
	     /*clear *//*_#I__L11*/ meltfnum[10] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V23*/ meltfptr[22] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V21*/ meltfptr[20] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    /*^compute */
    /*_.LET___V7*/ meltfptr[5] = /*_.IFCPP___V21*/ meltfptr[20];;

    MELT_LOCATION ("warmelt-outobj.melt:4047:/ clear");
	   /*clear *//*_.OLOC__V8*/ meltfptr[7] = 0;
    /*^clear */
	   /*clear *//*_.DESTLIST__V9*/ meltfptr[8] = 0;
    /*^clear */
	   /*clear *//*_.OOBJ__V10*/ meltfptr[9] = 0;
    /*^clear */
	   /*clear *//*_.OFIELD__V11*/ meltfptr[10] = 0;
    /*^clear */
	   /*clear *//*_#I__L3*/ meltfnum[1] = 0;
    /*^clear */
	   /*clear *//*_.BOXDEPTHP1__V12*/ meltfptr[11] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V13*/ meltfptr[12] = 0;
    /*^clear */
	   /*clear *//*_.OUTPUT_LOCATION__V15*/ meltfptr[13] = 0;
    /*^clear */
	   /*clear *//*_#I__L5*/ meltfnum[3] = 0;
    /*^clear */
	   /*clear *//*_.OUTPUT_C_CODE__V16*/ meltfptr[15] = 0;
    /*^clear */
	   /*clear *//*_#I__L6*/ meltfnum[5] = 0;
    /*^clear */
	   /*clear *//*_#GET_INT__L7*/ meltfnum[6] = 0;
    /*^clear */
	   /*clear *//*_.NAMED_NAME__V17*/ meltfptr[16] = 0;
    /*^clear */
	   /*clear *//*_#I__L8*/ meltfnum[7] = 0;
    /*^clear */
	   /*clear *//*_.LAMBDA___V18*/ meltfptr[17] = 0;
    /*^clear */
	   /*clear *//*_.LIST_EVERY__V20*/ meltfptr[19] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V21*/ meltfptr[20] = 0;
    MELT_LOCATION ("warmelt-outobj.melt:4045:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.LET___V7*/ meltfptr[5];;

    {
      MELT_LOCATION ("warmelt-outobj.melt:4045:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V5*/ meltfptr[4] = 0;
    /*^clear */
	   /*clear *//*_.LET___V7*/ meltfptr[5] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("OUTPUCOD_OBJGETSLOT", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_105_warmelt_outobj_OUTPUCOD_OBJGETSLOT_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_105_warmelt_outobj_OUTPUCOD_OBJGETSLOT */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_106_warmelt_outobj_LAMBDA___25__ (meltclosure_ptr_t meltclosp_,
					   melt_ptr_t meltfirstargp_,
					   const melt_argdescr_cell_t
					   meltxargdescr_[],
					   union meltparam_un * meltxargtab_,
					   const melt_argdescr_cell_t
					   meltxresdescr_[],
					   union meltparam_un * meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_106_warmelt_outobj_LAMBDA___25___melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_106_warmelt_outobj_LAMBDA___25___st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 6
    melt_ptr_t mcfr_varptr[6];
#define MELTFRAM_NBVARNUM 4
    long mcfr_varnum[4];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_106_warmelt_outobj_LAMBDA___25__ is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_106_warmelt_outobj_LAMBDA___25___st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 6; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_106_warmelt_outobj_LAMBDA___25__ nbval 6*/
  meltfram__.mcfr_nbvar = 6 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("LAMBDA_", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-outobj.melt:4069:/ getarg");
 /*_.DST__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {

 /*_#GET_INT__L1*/ meltfnum[0] =
      (melt_get_int
       ((melt_ptr_t) (( /*~BOXDEPTHP1 */ meltfclos->tabval[2]))));;
    MELT_LOCATION ("warmelt-outobj.melt:4070:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^msend */
    /*msend */
    {
      union meltparam_un argtab[3];
      memset (&argtab, 0, sizeof (argtab));
      /*^ojbmsend.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & ( /*~DECLBUF */ meltfclos->tabval[0]);
      /*^ojbmsend.arg */
      argtab[1].meltbp_aptr =
	(melt_ptr_t *) & ( /*~IMPLBUF */ meltfclos->tabval[1]);
      /*^ojbmsend.arg */
      argtab[2].meltbp_long = /*_#GET_INT__L1*/ meltfnum[0];
      /*_.OUTPUT_C_CODE__V3*/ meltfptr[2] =
	meltgc_send ((melt_ptr_t) ( /*_.DST__V2*/ meltfptr[1]),
		     (melt_ptr_t) (( /*!OUTPUT_C_CODE */ meltfrout->
				    tabval[0])),
		     (MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_LONG ""),
		     argtab, "", (union meltparam_un *) 0);
    }
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:4071:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#STRBUF_USEDLENGTH__L2*/ meltfnum[1] =
	melt_strbuf_usedlength ((melt_ptr_t)
				(( /*~IMPLBUF */ meltfclos->tabval[1])));;
      MELT_LOCATION ("warmelt-outobj.melt:4072:/ cond");
      /*cond */ if (
		     /*ifisa */
		     melt_is_instance_of ((melt_ptr_t)
					  (( /*!BUFFER_LIMIT_CONT */
					    meltfrout->tabval[1])),
					  (melt_ptr_t) (( /*!CLASS_REFERENCE */ meltfrout->tabval[2])))
	)			/*then */
	{
	  /*^cond.then */
	  /*^getslot */
	  {
	    melt_ptr_t slot = NULL, obj = NULL;
	    obj =
	      (melt_ptr_t) (( /*!BUFFER_LIMIT_CONT */ meltfrout->
			     tabval[1])) /*=obj*/ ;
	    melt_object_get_field (slot, obj, 0, "REFERENCED_VALUE");
     /*_.REFERENCED_VALUE__V5*/ meltfptr[4] = slot;
	  };
	  ;
	}
      else
	{			/*^cond.else */

    /*_.REFERENCED_VALUE__V5*/ meltfptr[4] = NULL;;
	}
      ;
      /*^compute */
   /*_#GET_INT__L3*/ meltfnum[2] =
	(melt_get_int
	 ((melt_ptr_t) ( /*_.REFERENCED_VALUE__V5*/ meltfptr[4])));;
      /*^compute */
   /*_#I__L4*/ meltfnum[3] =
	(( /*_#STRBUF_USEDLENGTH__L2*/ meltfnum[1]) <
	 ( /*_#GET_INT__L3*/ meltfnum[2]));;
      MELT_LOCATION ("warmelt-outobj.melt:4071:/ cond");
      /*cond */ if ( /*_#I__L4*/ meltfnum[3])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V6*/ meltfptr[5] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-outobj.melt:4071:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check limited implbuf"),
				  ("warmelt-outobj.melt")
				  ? ("warmelt-outobj.melt") : __FILE__,
				  (4071) ? (4071) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V6*/ meltfptr[5] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V4*/ meltfptr[3] = /*_.IFELSE___V6*/ meltfptr[5];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:4071:/ clear");
	     /*clear *//*_#STRBUF_USEDLENGTH__L2*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.REFERENCED_VALUE__V5*/ meltfptr[4] = 0;
      /*^clear */
	     /*clear *//*_#GET_INT__L3*/ meltfnum[2] = 0;
      /*^clear */
	     /*clear *//*_#I__L4*/ meltfnum[3] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V6*/ meltfptr[5] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V4*/ meltfptr[3] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:4073:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t)
			   (( /*~IMPLBUF */ meltfclos->tabval[1])), (" = "));
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:4069:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*epilog */

    /*^clear */
	   /*clear *//*_#GET_INT__L1*/ meltfnum[0] = 0;
    /*^clear */
	   /*clear *//*_.OUTPUT_C_CODE__V3*/ meltfptr[2] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V4*/ meltfptr[3] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("LAMBDA_", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_106_warmelt_outobj_LAMBDA___25___melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*noretval */ NULL);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_106_warmelt_outobj_LAMBDA___25__ */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_107_warmelt_outobj_OUTPUCOD_OBJPUTSLOT (meltclosure_ptr_t meltclosp_,
						 melt_ptr_t meltfirstargp_,
						 const melt_argdescr_cell_t
						 meltxargdescr_[],
						 union meltparam_un *
						 meltxargtab_,
						 const melt_argdescr_cell_t
						 meltxresdescr_[],
						 union meltparam_un *
						 meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_107_warmelt_outobj_OUTPUCOD_OBJPUTSLOT_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_107_warmelt_outobj_OUTPUCOD_OBJPUTSLOT_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 33
    melt_ptr_t mcfr_varptr[33];
#define MELTFRAM_NBVARNUM 15
    long mcfr_varnum[15];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_107_warmelt_outobj_OUTPUCOD_OBJPUTSLOT is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_107_warmelt_outobj_OUTPUCOD_OBJPUTSLOT_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 33; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_107_warmelt_outobj_OUTPUCOD_OBJPUTSLOT nbval 33*/
  meltfram__.mcfr_nbvar = 33 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("OUTPUCOD_OBJPUTSLOT", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-outobj.melt:4084:/ getarg");
 /*_.OPSLO__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.DECLBUF__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.DECLBUF__V3*/ meltfptr[2])) !=
	      NULL);


  /*getarg#2 */
  /*^getarg */
  if (meltxargdescr_[1] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.IMPLBUF__V4*/ meltfptr[3] =
    (meltxargtab_[1].meltbp_aptr) ? (*(meltxargtab_[1].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3])) !=
	      NULL);


  /*getarg#3 */
  /*^getarg */
  if (meltxargdescr_[2] != MELTBPAR_LONG)
    goto lab_endgetargs;
 /*_#DEPTH__L1*/ meltfnum[0] = meltxargtab_[2].meltbp_long;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:4085:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L2*/ meltfnum[1] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.OPSLO__V2*/ meltfptr[1]),
			     (melt_ptr_t) (( /*!CLASS_OBJPUTSLOT */
					    meltfrout->tabval[0])));;
      MELT_LOCATION ("warmelt-outobj.melt:4085:/ cond");
      /*cond */ if ( /*_#IS_A__L2*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V6*/ meltfptr[5] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-outobj.melt:4085:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check opslo"),
				  ("warmelt-outobj.melt")
				  ? ("warmelt-outobj.melt") : __FILE__,
				  (4085) ? (4085) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V6*/ meltfptr[5] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V5*/ meltfptr[4] = /*_.IFELSE___V6*/ meltfptr[5];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:4085:/ clear");
	     /*clear *//*_#IS_A__L2*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V6*/ meltfptr[5] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V5*/ meltfptr[4] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-outobj.melt:4086:/ quasiblock");


    /*^getslot */
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.OPSLO__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 0, "OBI_LOC");
  /*_.ILOC__V8*/ meltfptr[7] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-outobj.melt:4087:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.OPSLO__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 1, "OSLOT_ODATA");
  /*_.ODATA__V9*/ meltfptr[8] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-outobj.melt:4088:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.OPSLO__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 2, "OSLOT_OFFSET");
  /*_.OOFF__V10*/ meltfptr[9] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-outobj.melt:4089:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.OPSLO__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 3, "OSLOT_FIELD");
  /*_.OFIELD__V11*/ meltfptr[10] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-outobj.melt:4090:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.OPSLO__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 4, "OSLOT_VALUE");
  /*_.OVAL__V12*/ meltfptr[11] = slot;
    };
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:4092:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L3*/ meltfnum[1] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.OVAL__V12*/ meltfptr[11]),
			     (melt_ptr_t) (( /*!CLASS_NREP */ meltfrout->
					    tabval[1])));;
      /*^compute */
   /*_#NOT__L4*/ meltfnum[3] =
	(!( /*_#IS_A__L3*/ meltfnum[1]));;
      MELT_LOCATION ("warmelt-outobj.melt:4092:/ cond");
      /*cond */ if ( /*_#NOT__L4*/ meltfnum[3])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V14*/ meltfptr[13] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-outobj.melt:4092:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("outpucod_objputslot check oval not nrep"),
				  ("warmelt-outobj.melt")
				  ? ("warmelt-outobj.melt") : __FILE__,
				  (4092) ? (4092) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V14*/ meltfptr[13] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V13*/ meltfptr[12] = /*_.IFELSE___V14*/ meltfptr[13];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:4092:/ clear");
	     /*clear *//*_#IS_A__L3*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_#NOT__L4*/ meltfnum[3] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V14*/ meltfptr[13] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V13*/ meltfptr[12] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-outobj.melt:4093:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[3];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.IMPLBUF__V4*/ meltfptr[3];
      /*^apply.arg */
      argtab[1].meltbp_long = /*_#DEPTH__L1*/ meltfnum[0];
      /*^apply.arg */
      argtab[2].meltbp_cstring = "putslot";
      /*_.OUTPUT_LOCATION__V15*/ meltfptr[13] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!OUTPUT_LOCATION */ meltfrout->tabval[2])),
		    (melt_ptr_t) ( /*_.ILOC__V8*/ meltfptr[7]),
		    (MELTBPARSTR_PTR MELTBPARSTR_LONG MELTBPARSTR_CSTRING ""),
		    argtab, "", (union meltparam_un *) 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:4094:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   ("/*putslot*/"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:4095:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				( /*_#DEPTH__L1*/ meltfnum[0]), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:4096:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   ("melt_assertmsg(\"putslot checkobj"));
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:4097:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#IS_A__L5*/ meltfnum[1] =
      melt_is_instance_of ((melt_ptr_t) ( /*_.ODATA__V9*/ meltfptr[8]),
			   (melt_ptr_t) (( /*!CLASS_NAMED */ meltfrout->
					  tabval[3])));;
    MELT_LOCATION ("warmelt-outobj.melt:4097:/ cond");
    /*cond */ if ( /*_#IS_A__L5*/ meltfnum[1])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{


	  {
	    MELT_LOCATION ("warmelt-outobj.melt:4099:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t)
				 ( /*_.IMPLBUF__V4*/ meltfptr[3]), (" "));
	  }
	  ;
	  MELT_LOCATION ("warmelt-outobj.melt:4100:/ getslot");
	  {
	    melt_ptr_t slot = NULL, obj = NULL;
	    obj = (melt_ptr_t) ( /*_.ODATA__V9*/ meltfptr[8]) /*=obj*/ ;
	    melt_object_get_field (slot, obj, 1, "NAMED_NAME");
    /*_.NAMED_NAME__V16*/ meltfptr[15] = slot;
	  };
	  ;

	  {
	    /*^locexp */
	    /*add2sbuf_string */
	      meltgc_add_strbuf ((melt_ptr_t)
				 ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				 melt_string_str ((melt_ptr_t)
						  ( /*_.NAMED_NAME__V16*/
						   meltfptr[15])));
	  }
	  ;
	  MELT_LOCATION ("warmelt-outobj.melt:4098:/ quasiblock");


	  /*epilog */

	  MELT_LOCATION ("warmelt-outobj.melt:4097:/ clear");
	     /*clear *//*_.NAMED_NAME__V16*/ meltfptr[15] = 0;
	}
	;
      }				/*noelse */
    ;
    MELT_LOCATION ("warmelt-outobj.melt:4101:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#IS_A__L6*/ meltfnum[3] =
      melt_is_instance_of ((melt_ptr_t) ( /*_.OFIELD__V11*/ meltfptr[10]),
			   (melt_ptr_t) (( /*!CLASS_NAMED */ meltfrout->
					  tabval[3])));;
    MELT_LOCATION ("warmelt-outobj.melt:4101:/ cond");
    /*cond */ if ( /*_#IS_A__L6*/ meltfnum[3])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{


	  {
	    MELT_LOCATION ("warmelt-outobj.melt:4103:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t)
				 ( /*_.IMPLBUF__V4*/ meltfptr[3]), (" @"));
	  }
	  ;
	  MELT_LOCATION ("warmelt-outobj.melt:4104:/ getslot");
	  {
	    melt_ptr_t slot = NULL, obj = NULL;
	    obj = (melt_ptr_t) ( /*_.OFIELD__V11*/ meltfptr[10]) /*=obj*/ ;
	    melt_object_get_field (slot, obj, 1, "NAMED_NAME");
    /*_.NAMED_NAME__V17*/ meltfptr[15] = slot;
	  };
	  ;

	  {
	    /*^locexp */
	    /*add2sbuf_string */
	      meltgc_add_strbuf ((melt_ptr_t)
				 ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				 melt_string_str ((melt_ptr_t)
						  ( /*_.NAMED_NAME__V17*/
						   meltfptr[15])));
	  }
	  ;
	  MELT_LOCATION ("warmelt-outobj.melt:4102:/ quasiblock");


	  /*epilog */

	  MELT_LOCATION ("warmelt-outobj.melt:4101:/ clear");
	     /*clear *//*_.NAMED_NAME__V17*/ meltfptr[15] = 0;
	}
	;
      }				/*noelse */
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:4105:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   ("\", melt_magic_discr((melt_ptr_t)("));
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:4106:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^msend */
    /*msend */
    {
      union meltparam_un argtab[3];
      memset (&argtab, 0, sizeof (argtab));
      /*^ojbmsend.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.DECLBUF__V3*/ meltfptr[2];
      /*^ojbmsend.arg */
      argtab[1].meltbp_aptr = (melt_ptr_t *) & /*_.IMPLBUF__V4*/ meltfptr[3];
      /*^ojbmsend.arg */
      argtab[2].meltbp_long = /*_#DEPTH__L1*/ meltfnum[0];
      /*_.OUTPUT_C_CODE__V18*/ meltfptr[15] =
	meltgc_send ((melt_ptr_t) ( /*_.ODATA__V9*/ meltfptr[8]),
		     (melt_ptr_t) (( /*!OUTPUT_C_CODE */ meltfrout->
				    tabval[4])),
		     (MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_LONG ""),
		     argtab, "", (union meltparam_un *) 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:4107:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   (")) == MELTOBMAG_OBJECT);"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:4108:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				( /*_#DEPTH__L1*/ meltfnum[0]), 0);
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:4109:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#IS_A__L7*/ meltfnum[6] =
      melt_is_instance_of ((melt_ptr_t) ( /*_.OFIELD__V11*/ meltfptr[10]),
			   (melt_ptr_t) (( /*!CLASS_FIELD */ meltfrout->
					  tabval[5])));;
    MELT_LOCATION ("warmelt-outobj.melt:4109:/ cond");
    /*cond */ if ( /*_#IS_A__L7*/ meltfnum[6])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{


	  {
	    MELT_LOCATION ("warmelt-outobj.melt:4111:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t)
				 ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				 ("melt_putfield_object(("));
	  }
	  ;
	  MELT_LOCATION ("warmelt-outobj.melt:4112:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^msend */
	  /*msend */
	  {
	    union meltparam_un argtab[3];
	    memset (&argtab, 0, sizeof (argtab));
	    /*^ojbmsend.arg */
	    argtab[0].meltbp_aptr =
	      (melt_ptr_t *) & /*_.DECLBUF__V3*/ meltfptr[2];
	    /*^ojbmsend.arg */
	    argtab[1].meltbp_aptr =
	      (melt_ptr_t *) & /*_.IMPLBUF__V4*/ meltfptr[3];
	    /*^ojbmsend.arg */
	    argtab[2].meltbp_long = /*_#DEPTH__L1*/ meltfnum[0];
	    /*_.OUTPUT_C_CODE__V19*/ meltfptr[18] =
	      meltgc_send ((melt_ptr_t) ( /*_.ODATA__V9*/ meltfptr[8]),
			   (melt_ptr_t) (( /*!OUTPUT_C_CODE */ meltfrout->
					  tabval[4])),
			   (MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_LONG
			    ""), argtab, "", (union meltparam_un *) 0);
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:4113:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t)
				 ( /*_.IMPLBUF__V4*/ meltfptr[3]), ("), ("));
	  }
	  ;
	  MELT_LOCATION ("warmelt-outobj.melt:4114:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^msend */
	  /*msend */
	  {
	    union meltparam_un argtab[3];
	    memset (&argtab, 0, sizeof (argtab));
	    /*^ojbmsend.arg */
	    argtab[0].meltbp_aptr =
	      (melt_ptr_t *) & /*_.DECLBUF__V3*/ meltfptr[2];
	    /*^ojbmsend.arg */
	    argtab[1].meltbp_aptr =
	      (melt_ptr_t *) & /*_.IMPLBUF__V4*/ meltfptr[3];
	    /*^ojbmsend.arg */
	    argtab[2].meltbp_long = /*_#DEPTH__L1*/ meltfnum[0];
	    /*_.OUTPUT_C_CODE__V20*/ meltfptr[19] =
	      meltgc_send ((melt_ptr_t) ( /*_.OOFF__V10*/ meltfptr[9]),
			   (melt_ptr_t) (( /*!OUTPUT_C_CODE */ meltfrout->
					  tabval[4])),
			   (MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_LONG
			    ""), argtab, "", (union meltparam_un *) 0);
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:4115:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t)
				 ( /*_.IMPLBUF__V4*/ meltfptr[3]), ("), ("));
	  }
	  ;
   /*_#I__L8*/ meltfnum[7] =
	    ((1) + ( /*_#DEPTH__L1*/ meltfnum[0]));;
	  MELT_LOCATION ("warmelt-outobj.melt:4116:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^msend */
	  /*msend */
	  {
	    union meltparam_un argtab[3];
	    memset (&argtab, 0, sizeof (argtab));
	    /*^ojbmsend.arg */
	    argtab[0].meltbp_aptr =
	      (melt_ptr_t *) & /*_.DECLBUF__V3*/ meltfptr[2];
	    /*^ojbmsend.arg */
	    argtab[1].meltbp_aptr =
	      (melt_ptr_t *) & /*_.IMPLBUF__V4*/ meltfptr[3];
	    /*^ojbmsend.arg */
	    argtab[2].meltbp_long = /*_#I__L8*/ meltfnum[7];
	    /*_.OUTPUT_C_CODE__V21*/ meltfptr[20] =
	      meltgc_send ((melt_ptr_t) ( /*_.OVAL__V12*/ meltfptr[11]),
			   (melt_ptr_t) (( /*!OUTPUT_C_CODE */ meltfrout->
					  tabval[4])),
			   (MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_LONG
			    ""), argtab, "", (union meltparam_un *) 0);
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:4117:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t)
				 ( /*_.IMPLBUF__V4*/ meltfptr[3]), ("), \""));
	  }
	  ;
	  MELT_LOCATION ("warmelt-outobj.melt:4118:/ getslot");
	  {
	    melt_ptr_t slot = NULL, obj = NULL;
	    obj = (melt_ptr_t) ( /*_.OFIELD__V11*/ meltfptr[10]) /*=obj*/ ;
	    melt_object_get_field (slot, obj, 1, "NAMED_NAME");
    /*_.NAMED_NAME__V22*/ meltfptr[21] = slot;
	  };
	  ;

	  {
	    /*^locexp */
	    meltgc_add_strbuf_cident ((melt_ptr_t)
				      ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				      melt_string_str ((melt_ptr_t)
						       ( /*_.NAMED_NAME__V22*/
							meltfptr[21])));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:4119:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t)
				 ( /*_.IMPLBUF__V4*/ meltfptr[3]), ("\");"));
	  }
	  ;
	  MELT_LOCATION ("warmelt-outobj.melt:4110:/ quasiblock");


	  /*epilog */

	  MELT_LOCATION ("warmelt-outobj.melt:4109:/ clear");
	     /*clear *//*_.OUTPUT_C_CODE__V19*/ meltfptr[18] = 0;
	  /*^clear */
	     /*clear *//*_.OUTPUT_C_CODE__V20*/ meltfptr[19] = 0;
	  /*^clear */
	     /*clear *//*_#I__L8*/ meltfnum[7] = 0;
	  /*^clear */
	     /*clear *//*_.OUTPUT_C_CODE__V21*/ meltfptr[20] = 0;
	  /*^clear */
	     /*clear *//*_.NAMED_NAME__V22*/ meltfptr[21] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

	/*^block */
	/*anyblock */
	{


	  {
	    MELT_LOCATION ("warmelt-outobj.melt:4123:/ locexp");
	    meltgc_strbuf_add_indent ((melt_ptr_t)
				      ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				      ( /*_#DEPTH__L1*/ meltfnum[0]), 0);
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:4124:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t)
				 ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				 ("melt_assertmsg(\"putslot checkoff"));
	  }
	  ;
	  MELT_LOCATION ("warmelt-outobj.melt:4125:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
   /*_#IS_A__L9*/ meltfnum[7] =
	    melt_is_instance_of ((melt_ptr_t) ( /*_.ODATA__V9*/ meltfptr[8]),
				 (melt_ptr_t) (( /*!CLASS_NAMED */ meltfrout->
						tabval[3])));;
	  MELT_LOCATION ("warmelt-outobj.melt:4125:/ cond");
	  /*cond */ if ( /*_#IS_A__L9*/ meltfnum[7])	/*then */
	    {
	      /*^cond.then */
	      /*^block */
	      /*anyblock */
	      {


		{
		  MELT_LOCATION ("warmelt-outobj.melt:4127:/ locexp");
		  /*add2sbuf_strconst */
		    meltgc_add_strbuf ((melt_ptr_t)
				       ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				       (" "));
		}
		;
		MELT_LOCATION ("warmelt-outobj.melt:4128:/ getslot");
		{
		  melt_ptr_t slot = NULL, obj = NULL;
		  obj = (melt_ptr_t) ( /*_.ODATA__V9*/ meltfptr[8]) /*=obj*/ ;
		  melt_object_get_field (slot, obj, 1, "NAMED_NAME");
      /*_.NAMED_NAME__V23*/ meltfptr[18] = slot;
		};
		;

		{
		  /*^locexp */
		  /*add2sbuf_string */
		    meltgc_add_strbuf ((melt_ptr_t)
				       ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				       melt_string_str ((melt_ptr_t)
							( /*_.NAMED_NAME__V23*/ meltfptr[18])));
		}
		;
		MELT_LOCATION ("warmelt-outobj.melt:4126:/ quasiblock");


		/*epilog */

		MELT_LOCATION ("warmelt-outobj.melt:4125:/ clear");
	       /*clear *//*_.NAMED_NAME__V23*/ meltfptr[18] = 0;
	      }
	      ;
	    }			/*noelse */
	  ;
	  MELT_LOCATION ("warmelt-outobj.melt:4129:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
   /*_#IS_A__L10*/ meltfnum[9] =
	    melt_is_instance_of ((melt_ptr_t)
				 ( /*_.OFIELD__V11*/ meltfptr[10]),
				 (melt_ptr_t) (( /*!CLASS_NAMED */ meltfrout->
						tabval[3])));;
	  MELT_LOCATION ("warmelt-outobj.melt:4129:/ cond");
	  /*cond */ if ( /*_#IS_A__L10*/ meltfnum[9])	/*then */
	    {
	      /*^cond.then */
	      /*^block */
	      /*anyblock */
	      {


		{
		  MELT_LOCATION ("warmelt-outobj.melt:4131:/ locexp");
		  /*add2sbuf_strconst */
		    meltgc_add_strbuf ((melt_ptr_t)
				       ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				       (" @"));
		}
		;
		MELT_LOCATION ("warmelt-outobj.melt:4132:/ getslot");
		{
		  melt_ptr_t slot = NULL, obj = NULL;
		  obj =
		    (melt_ptr_t) ( /*_.OFIELD__V11*/ meltfptr[10]) /*=obj*/ ;
		  melt_object_get_field (slot, obj, 1, "NAMED_NAME");
      /*_.NAMED_NAME__V24*/ meltfptr[19] = slot;
		};
		;

		{
		  /*^locexp */
		  /*add2sbuf_string */
		    meltgc_add_strbuf ((melt_ptr_t)
				       ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				       melt_string_str ((melt_ptr_t)
							( /*_.NAMED_NAME__V24*/ meltfptr[19])));
		}
		;
		MELT_LOCATION ("warmelt-outobj.melt:4130:/ quasiblock");


		/*epilog */

		MELT_LOCATION ("warmelt-outobj.melt:4129:/ clear");
	       /*clear *//*_.NAMED_NAME__V24*/ meltfptr[19] = 0;
	      }
	      ;
	    }			/*noelse */
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:4133:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t)
				 ( /*_.IMPLBUF__V4*/ meltfptr[3]), ("\", ("));
	  }
	  ;
	  MELT_LOCATION ("warmelt-outobj.melt:4134:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^msend */
	  /*msend */
	  {
	    union meltparam_un argtab[3];
	    memset (&argtab, 0, sizeof (argtab));
	    /*^ojbmsend.arg */
	    argtab[0].meltbp_aptr =
	      (melt_ptr_t *) & /*_.DECLBUF__V3*/ meltfptr[2];
	    /*^ojbmsend.arg */
	    argtab[1].meltbp_aptr =
	      (melt_ptr_t *) & /*_.IMPLBUF__V4*/ meltfptr[3];
	    /*^ojbmsend.arg */
	    argtab[2].meltbp_long = /*_#DEPTH__L1*/ meltfnum[0];
	    /*_.OUTPUT_C_CODE__V25*/ meltfptr[20] =
	      meltgc_send ((melt_ptr_t) ( /*_.OOFF__V10*/ meltfptr[9]),
			   (melt_ptr_t) (( /*!OUTPUT_C_CODE */ meltfrout->
					  tabval[4])),
			   (MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_LONG
			    ""), argtab, "", (union meltparam_un *) 0);
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:4135:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t)
				 ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				 (">=0 && "));
	  }
	  ;
	  MELT_LOCATION ("warmelt-outobj.melt:4136:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^msend */
	  /*msend */
	  {
	    union meltparam_un argtab[3];
	    memset (&argtab, 0, sizeof (argtab));
	    /*^ojbmsend.arg */
	    argtab[0].meltbp_aptr =
	      (melt_ptr_t *) & /*_.DECLBUF__V3*/ meltfptr[2];
	    /*^ojbmsend.arg */
	    argtab[1].meltbp_aptr =
	      (melt_ptr_t *) & /*_.IMPLBUF__V4*/ meltfptr[3];
	    /*^ojbmsend.arg */
	    argtab[2].meltbp_long = /*_#DEPTH__L1*/ meltfnum[0];
	    /*_.OUTPUT_C_CODE__V26*/ meltfptr[21] =
	      meltgc_send ((melt_ptr_t) ( /*_.OOFF__V10*/ meltfptr[9]),
			   (melt_ptr_t) (( /*!OUTPUT_C_CODE */ meltfrout->
					  tabval[4])),
			   (MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_LONG
			    ""), argtab, "", (union meltparam_un *) 0);
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:4137:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t)
				 ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				 ("< melt_object_length((melt_ptr_t)("));
	  }
	  ;
	  MELT_LOCATION ("warmelt-outobj.melt:4138:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^msend */
	  /*msend */
	  {
	    union meltparam_un argtab[3];
	    memset (&argtab, 0, sizeof (argtab));
	    /*^ojbmsend.arg */
	    argtab[0].meltbp_aptr =
	      (melt_ptr_t *) & /*_.DECLBUF__V3*/ meltfptr[2];
	    /*^ojbmsend.arg */
	    argtab[1].meltbp_aptr =
	      (melt_ptr_t *) & /*_.IMPLBUF__V4*/ meltfptr[3];
	    /*^ojbmsend.arg */
	    argtab[2].meltbp_long = /*_#DEPTH__L1*/ meltfnum[0];
	    /*_.OUTPUT_C_CODE__V27*/ meltfptr[18] =
	      meltgc_send ((melt_ptr_t) ( /*_.ODATA__V9*/ meltfptr[8]),
			   (melt_ptr_t) (( /*!OUTPUT_C_CODE */ meltfrout->
					  tabval[4])),
			   (MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_LONG
			    ""), argtab, "", (union meltparam_un *) 0);
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:4139:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t)
				 ( /*_.IMPLBUF__V4*/ meltfptr[3]), ("))));"));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:4140:/ locexp");
	    meltgc_strbuf_add_indent ((melt_ptr_t)
				      ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				      ( /*_#DEPTH__L1*/ meltfnum[0]), 0);
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:4141:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t)
				 ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				 ("((meltobject_ptr_t)("));
	  }
	  ;
	  MELT_LOCATION ("warmelt-outobj.melt:4142:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^msend */
	  /*msend */
	  {
	    union meltparam_un argtab[3];
	    memset (&argtab, 0, sizeof (argtab));
	    /*^ojbmsend.arg */
	    argtab[0].meltbp_aptr =
	      (melt_ptr_t *) & /*_.DECLBUF__V3*/ meltfptr[2];
	    /*^ojbmsend.arg */
	    argtab[1].meltbp_aptr =
	      (melt_ptr_t *) & /*_.IMPLBUF__V4*/ meltfptr[3];
	    /*^ojbmsend.arg */
	    argtab[2].meltbp_long = /*_#DEPTH__L1*/ meltfnum[0];
	    /*_.OUTPUT_C_CODE__V28*/ meltfptr[19] =
	      meltgc_send ((melt_ptr_t) ( /*_.ODATA__V9*/ meltfptr[8]),
			   (melt_ptr_t) (( /*!OUTPUT_C_CODE */ meltfrout->
					  tabval[4])),
			   (MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_LONG
			    ""), argtab, "", (union meltparam_un *) 0);
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:4143:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t)
				 ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				 ("))->obj_vartab["));
	  }
	  ;
	  MELT_LOCATION ("warmelt-outobj.melt:4144:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^msend */
	  /*msend */
	  {
	    union meltparam_un argtab[3];
	    memset (&argtab, 0, sizeof (argtab));
	    /*^ojbmsend.arg */
	    argtab[0].meltbp_aptr =
	      (melt_ptr_t *) & /*_.DECLBUF__V3*/ meltfptr[2];
	    /*^ojbmsend.arg */
	    argtab[1].meltbp_aptr =
	      (melt_ptr_t *) & /*_.IMPLBUF__V4*/ meltfptr[3];
	    /*^ojbmsend.arg */
	    argtab[2].meltbp_long = /*_#DEPTH__L1*/ meltfnum[0];
	    /*_.OUTPUT_C_CODE__V29*/ meltfptr[28] =
	      meltgc_send ((melt_ptr_t) ( /*_.OOFF__V10*/ meltfptr[9]),
			   (melt_ptr_t) (( /*!OUTPUT_C_CODE */ meltfrout->
					  tabval[4])),
			   (MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_LONG
			    ""), argtab, "", (union meltparam_un *) 0);
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:4145:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t)
				 ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				 ("] = (melt_ptr_t)("));
	  }
	  ;
   /*_#I__L11*/ meltfnum[10] =
	    ((1) + ( /*_#DEPTH__L1*/ meltfnum[0]));;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:4146:/ locexp");
	    meltgc_strbuf_add_indent ((melt_ptr_t)
				      ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				      ( /*_#I__L11*/ meltfnum[10]), 0);
	  }
	  ;
   /*_#I__L12*/ meltfnum[11] =
	    ((1) + ( /*_#DEPTH__L1*/ meltfnum[0]));;
	  MELT_LOCATION ("warmelt-outobj.melt:4147:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^msend */
	  /*msend */
	  {
	    union meltparam_un argtab[3];
	    memset (&argtab, 0, sizeof (argtab));
	    /*^ojbmsend.arg */
	    argtab[0].meltbp_aptr =
	      (melt_ptr_t *) & /*_.DECLBUF__V3*/ meltfptr[2];
	    /*^ojbmsend.arg */
	    argtab[1].meltbp_aptr =
	      (melt_ptr_t *) & /*_.IMPLBUF__V4*/ meltfptr[3];
	    /*^ojbmsend.arg */
	    argtab[2].meltbp_long = /*_#I__L12*/ meltfnum[11];
	    /*_.OUTPUT_C_CODE__V30*/ meltfptr[29] =
	      meltgc_send ((melt_ptr_t) ( /*_.OVAL__V12*/ meltfptr[11]),
			   (melt_ptr_t) (( /*!OUTPUT_C_CODE */ meltfrout->
					  tabval[4])),
			   (MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_LONG
			    ""), argtab, "", (union meltparam_un *) 0);
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:4148:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t)
				 ( /*_.IMPLBUF__V4*/ meltfptr[3]), (");"));
	  }
	  ;
	  MELT_LOCATION ("warmelt-outobj.melt:4121:/ quasiblock");


	  /*epilog */

	  MELT_LOCATION ("warmelt-outobj.melt:4109:/ clear");
	     /*clear *//*_#IS_A__L9*/ meltfnum[7] = 0;
	  /*^clear */
	     /*clear *//*_#IS_A__L10*/ meltfnum[9] = 0;
	  /*^clear */
	     /*clear *//*_.OUTPUT_C_CODE__V25*/ meltfptr[20] = 0;
	  /*^clear */
	     /*clear *//*_.OUTPUT_C_CODE__V26*/ meltfptr[21] = 0;
	  /*^clear */
	     /*clear *//*_.OUTPUT_C_CODE__V27*/ meltfptr[18] = 0;
	  /*^clear */
	     /*clear *//*_.OUTPUT_C_CODE__V28*/ meltfptr[19] = 0;
	  /*^clear */
	     /*clear *//*_.OUTPUT_C_CODE__V29*/ meltfptr[28] = 0;
	  /*^clear */
	     /*clear *//*_#I__L11*/ meltfnum[10] = 0;
	  /*^clear */
	     /*clear *//*_#I__L12*/ meltfnum[11] = 0;
	  /*^clear */
	     /*clear *//*_.OUTPUT_C_CODE__V30*/ meltfptr[29] = 0;
	}
	;
      }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:4151:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				( /*_#DEPTH__L1*/ meltfnum[0]), 0);
    }
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:4152:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#STRBUF_USEDLENGTH__L13*/ meltfnum[7] =
	melt_strbuf_usedlength ((melt_ptr_t)
				( /*_.IMPLBUF__V4*/ meltfptr[3]));;
      MELT_LOCATION ("warmelt-outobj.melt:4153:/ cond");
      /*cond */ if (
		     /*ifisa */
		     melt_is_instance_of ((melt_ptr_t)
					  (( /*!BUFFER_LIMIT_CONT */
					    meltfrout->tabval[6])),
					  (melt_ptr_t) (( /*!CLASS_REFERENCE */ meltfrout->tabval[7])))
	)			/*then */
	{
	  /*^cond.then */
	  /*^getslot */
	  {
	    melt_ptr_t slot = NULL, obj = NULL;
	    obj =
	      (melt_ptr_t) (( /*!BUFFER_LIMIT_CONT */ meltfrout->
			     tabval[6])) /*=obj*/ ;
	    melt_object_get_field (slot, obj, 0, "REFERENCED_VALUE");
     /*_.REFERENCED_VALUE__V32*/ meltfptr[21] = slot;
	  };
	  ;
	}
      else
	{			/*^cond.else */

    /*_.REFERENCED_VALUE__V32*/ meltfptr[21] = NULL;;
	}
      ;
      /*^compute */
   /*_#GET_INT__L14*/ meltfnum[9] =
	(melt_get_int
	 ((melt_ptr_t) ( /*_.REFERENCED_VALUE__V32*/ meltfptr[21])));;
      /*^compute */
   /*_#I__L15*/ meltfnum[10] =
	(( /*_#STRBUF_USEDLENGTH__L13*/ meltfnum[7]) <
	 ( /*_#GET_INT__L14*/ meltfnum[9]));;
      MELT_LOCATION ("warmelt-outobj.melt:4152:/ cond");
      /*cond */ if ( /*_#I__L15*/ meltfnum[10])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V33*/ meltfptr[18] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-outobj.melt:4152:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check limited implbuf"),
				  ("warmelt-outobj.melt")
				  ? ("warmelt-outobj.melt") : __FILE__,
				  (4152) ? (4152) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V33*/ meltfptr[18] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V31*/ meltfptr[20] = /*_.IFELSE___V33*/ meltfptr[18];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:4152:/ clear");
	     /*clear *//*_#STRBUF_USEDLENGTH__L13*/ meltfnum[7] = 0;
      /*^clear */
	     /*clear *//*_.REFERENCED_VALUE__V32*/ meltfptr[21] = 0;
      /*^clear */
	     /*clear *//*_#GET_INT__L14*/ meltfnum[9] = 0;
      /*^clear */
	     /*clear *//*_#I__L15*/ meltfnum[10] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V33*/ meltfptr[18] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V31*/ meltfptr[20] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    /*^compute */
    /*_.LET___V7*/ meltfptr[5] = /*_.IFCPP___V31*/ meltfptr[20];;

    MELT_LOCATION ("warmelt-outobj.melt:4086:/ clear");
	   /*clear *//*_.ILOC__V8*/ meltfptr[7] = 0;
    /*^clear */
	   /*clear *//*_.ODATA__V9*/ meltfptr[8] = 0;
    /*^clear */
	   /*clear *//*_.OOFF__V10*/ meltfptr[9] = 0;
    /*^clear */
	   /*clear *//*_.OFIELD__V11*/ meltfptr[10] = 0;
    /*^clear */
	   /*clear *//*_.OVAL__V12*/ meltfptr[11] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V13*/ meltfptr[12] = 0;
    /*^clear */
	   /*clear *//*_.OUTPUT_LOCATION__V15*/ meltfptr[13] = 0;
    /*^clear */
	   /*clear *//*_#IS_A__L5*/ meltfnum[1] = 0;
    /*^clear */
	   /*clear *//*_#IS_A__L6*/ meltfnum[3] = 0;
    /*^clear */
	   /*clear *//*_.OUTPUT_C_CODE__V18*/ meltfptr[15] = 0;
    /*^clear */
	   /*clear *//*_#IS_A__L7*/ meltfnum[6] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V31*/ meltfptr[20] = 0;
    MELT_LOCATION ("warmelt-outobj.melt:4084:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.LET___V7*/ meltfptr[5];;

    {
      MELT_LOCATION ("warmelt-outobj.melt:4084:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V5*/ meltfptr[4] = 0;
    /*^clear */
	   /*clear *//*_.LET___V7*/ meltfptr[5] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("OUTPUCOD_OBJPUTSLOT", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_107_warmelt_outobj_OUTPUCOD_OBJPUTSLOT_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_107_warmelt_outobj_OUTPUCOD_OBJPUTSLOT */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_108_warmelt_outobj_OUTPUCOD_OBJPUTCLOSUROUT (meltclosure_ptr_t
						      meltclosp_,
						      melt_ptr_t
						      meltfirstargp_,
						      const
						      melt_argdescr_cell_t
						      meltxargdescr_[],
						      union meltparam_un *
						      meltxargtab_,
						      const
						      melt_argdescr_cell_t
						      meltxresdescr_[],
						      union meltparam_un *
						      meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_108_warmelt_outobj_OUTPUCOD_OBJPUTCLOSUROUT_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_108_warmelt_outobj_OUTPUCOD_OBJPUTCLOSUROUT_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 18
    melt_ptr_t mcfr_varptr[18];
#define MELTFRAM_NBVARNUM 6
    long mcfr_varnum[6];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_108_warmelt_outobj_OUTPUCOD_OBJPUTCLOSUROUT is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_108_warmelt_outobj_OUTPUCOD_OBJPUTCLOSUROUT_st
	 *) meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 18; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_108_warmelt_outobj_OUTPUCOD_OBJPUTCLOSUROUT nbval 18*/
  meltfram__.mcfr_nbvar = 18 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("OUTPUCOD_OBJPUTCLOSUROUT", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-outobj.melt:4159:/ getarg");
 /*_.OPCLOR__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.DECLBUF__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.DECLBUF__V3*/ meltfptr[2])) !=
	      NULL);


  /*getarg#2 */
  /*^getarg */
  if (meltxargdescr_[1] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.IMPLBUF__V4*/ meltfptr[3] =
    (meltxargtab_[1].meltbp_aptr) ? (*(meltxargtab_[1].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3])) !=
	      NULL);


  /*getarg#3 */
  /*^getarg */
  if (meltxargdescr_[2] != MELTBPAR_LONG)
    goto lab_endgetargs;
 /*_#DEPTH__L1*/ meltfnum[0] = meltxargtab_[2].meltbp_long;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:4160:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L2*/ meltfnum[1] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.OPCLOR__V2*/ meltfptr[1]),
			     (melt_ptr_t) (( /*!CLASS_OBJPUTCLOSUROUT */
					    meltfrout->tabval[0])));;
      MELT_LOCATION ("warmelt-outobj.melt:4160:/ cond");
      /*cond */ if ( /*_#IS_A__L2*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V6*/ meltfptr[5] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-outobj.melt:4160:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check opclor"),
				  ("warmelt-outobj.melt")
				  ? ("warmelt-outobj.melt") : __FILE__,
				  (4160) ? (4160) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V6*/ meltfptr[5] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V5*/ meltfptr[4] = /*_.IFELSE___V6*/ meltfptr[5];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:4160:/ clear");
	     /*clear *//*_#IS_A__L2*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V6*/ meltfptr[5] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V5*/ meltfptr[4] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-outobj.melt:4161:/ quasiblock");


    /*^getslot */
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.OPCLOR__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 0, "OBI_LOC");
  /*_.OLOC__V8*/ meltfptr[7] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-outobj.melt:4162:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.OPCLOR__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 1, "OPCLOR_CLOS");
  /*_.OCLOS__V9*/ meltfptr[8] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-outobj.melt:4163:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.OPCLOR__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 2, "OPCLOR_ROUT");
  /*_.OROUT__V10*/ meltfptr[9] = slot;
    };
    ;
 /*_#CNT__L3*/ meltfnum[1] = 0;;

    {
      MELT_LOCATION ("warmelt-outobj.melt:4168:/ locexp");
      /* GETCNTCHK__1 in outpucod_objputclosurout */
      {
	static long GETCNTCHK__1_cnt;
	GETCNTCHK__1_cnt++;
     /*_#CNT__L3*/ meltfnum[1] = GETCNTCHK__1_cnt;
      };
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:4173:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[3];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.IMPLBUF__V4*/ meltfptr[3];
      /*^apply.arg */
      argtab[1].meltbp_long = /*_#DEPTH__L1*/ meltfnum[0];
      /*^apply.arg */
      argtab[2].meltbp_cstring = "putclosurout";
      /*_.OUTPUT_LOCATION__V11*/ meltfptr[10] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!OUTPUT_LOCATION */ meltfrout->tabval[1])),
		    (melt_ptr_t) ( /*_.OLOC__V8*/ meltfptr[7]),
		    (MELTBPARSTR_PTR MELTBPARSTR_LONG MELTBPARSTR_CSTRING ""),
		    argtab, "", (union meltparam_un *) 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:4174:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   ("/*putclosurout#"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:4175:/ locexp");
      meltgc_add_strbuf_dec ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			     ( /*_#CNT__L3*/ meltfnum[1]));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:4176:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   ("*/"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:4177:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				( /*_#DEPTH__L1*/ meltfnum[0]), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:4178:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   ("melt_assertmsg(\"putclosrout#"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:4179:/ locexp");
      meltgc_add_strbuf_dec ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			     ( /*_#CNT__L3*/ meltfnum[1]));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:4180:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   (" checkclo\", melt_magic_discr((melt_ptr_t)("));
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:4181:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^msend */
    /*msend */
    {
      union meltparam_un argtab[3];
      memset (&argtab, 0, sizeof (argtab));
      /*^ojbmsend.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.DECLBUF__V3*/ meltfptr[2];
      /*^ojbmsend.arg */
      argtab[1].meltbp_aptr = (melt_ptr_t *) & /*_.IMPLBUF__V4*/ meltfptr[3];
      /*^ojbmsend.arg */
      argtab[2].meltbp_long = /*_#DEPTH__L1*/ meltfnum[0];
      /*_.OUTPUT_C_CODE__V12*/ meltfptr[11] =
	meltgc_send ((melt_ptr_t) ( /*_.OCLOS__V9*/ meltfptr[8]),
		     (melt_ptr_t) (( /*!OUTPUT_C_CODE */ meltfrout->
				    tabval[2])),
		     (MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_LONG ""),
		     argtab, "", (union meltparam_un *) 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:4182:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   (")) == MELTOBMAG_CLOSURE);"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:4183:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				( /*_#DEPTH__L1*/ meltfnum[0]), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:4184:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   ("melt_assertmsg(\"putclosrout#"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:4185:/ locexp");
      meltgc_add_strbuf_dec ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			     ( /*_#CNT__L3*/ meltfnum[1]));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:4186:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   (" checkrout\", melt_magic_discr((melt_ptr_t)("));
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:4187:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^msend */
    /*msend */
    {
      union meltparam_un argtab[3];
      memset (&argtab, 0, sizeof (argtab));
      /*^ojbmsend.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.DECLBUF__V3*/ meltfptr[2];
      /*^ojbmsend.arg */
      argtab[1].meltbp_aptr = (melt_ptr_t *) & /*_.IMPLBUF__V4*/ meltfptr[3];
      /*^ojbmsend.arg */
      argtab[2].meltbp_long = /*_#DEPTH__L1*/ meltfnum[0];
      /*_.OUTPUT_C_CODE__V13*/ meltfptr[12] =
	meltgc_send ((melt_ptr_t) ( /*_.OROUT__V10*/ meltfptr[9]),
		     (melt_ptr_t) (( /*!OUTPUT_C_CODE */ meltfrout->
				    tabval[2])),
		     (MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_LONG ""),
		     argtab, "", (union meltparam_un *) 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:4188:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   (")) == MELTOBMAG_ROUTINE);"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:4189:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				( /*_#DEPTH__L1*/ meltfnum[0]), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:4190:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   ("((meltclosure_ptr_t)"));
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:4191:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^msend */
    /*msend */
    {
      union meltparam_un argtab[3];
      memset (&argtab, 0, sizeof (argtab));
      /*^ojbmsend.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.DECLBUF__V3*/ meltfptr[2];
      /*^ojbmsend.arg */
      argtab[1].meltbp_aptr = (melt_ptr_t *) & /*_.IMPLBUF__V4*/ meltfptr[3];
      /*^ojbmsend.arg */
      argtab[2].meltbp_long = /*_#DEPTH__L1*/ meltfnum[0];
      /*_.OUTPUT_C_CODE__V14*/ meltfptr[13] =
	meltgc_send ((melt_ptr_t) ( /*_.OCLOS__V9*/ meltfptr[8]),
		     (melt_ptr_t) (( /*!OUTPUT_C_CODE */ meltfrout->
				    tabval[2])),
		     (MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_LONG ""),
		     argtab, "", (union meltparam_un *) 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:4192:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   (")->rout = (meltroutine_ptr_t) ("));
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:4193:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^msend */
    /*msend */
    {
      union meltparam_un argtab[3];
      memset (&argtab, 0, sizeof (argtab));
      /*^ojbmsend.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.DECLBUF__V3*/ meltfptr[2];
      /*^ojbmsend.arg */
      argtab[1].meltbp_aptr = (melt_ptr_t *) & /*_.IMPLBUF__V4*/ meltfptr[3];
      /*^ojbmsend.arg */
      argtab[2].meltbp_long = /*_#DEPTH__L1*/ meltfnum[0];
      /*_.OUTPUT_C_CODE__V15*/ meltfptr[14] =
	meltgc_send ((melt_ptr_t) ( /*_.OROUT__V10*/ meltfptr[9]),
		     (melt_ptr_t) (( /*!OUTPUT_C_CODE */ meltfrout->
				    tabval[2])),
		     (MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_LONG ""),
		     argtab, "", (union meltparam_un *) 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:4194:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   (");"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:4195:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				( /*_#DEPTH__L1*/ meltfnum[0]), 0);
    }
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:4196:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#STRBUF_USEDLENGTH__L4*/ meltfnum[3] =
	melt_strbuf_usedlength ((melt_ptr_t)
				( /*_.IMPLBUF__V4*/ meltfptr[3]));;
      MELT_LOCATION ("warmelt-outobj.melt:4197:/ cond");
      /*cond */ if (
		     /*ifisa */
		     melt_is_instance_of ((melt_ptr_t)
					  (( /*!BUFFER_LIMIT_CONT */
					    meltfrout->tabval[3])),
					  (melt_ptr_t) (( /*!CLASS_REFERENCE */ meltfrout->tabval[4])))
	)			/*then */
	{
	  /*^cond.then */
	  /*^getslot */
	  {
	    melt_ptr_t slot = NULL, obj = NULL;
	    obj =
	      (melt_ptr_t) (( /*!BUFFER_LIMIT_CONT */ meltfrout->
			     tabval[3])) /*=obj*/ ;
	    melt_object_get_field (slot, obj, 0, "REFERENCED_VALUE");
     /*_.REFERENCED_VALUE__V17*/ meltfptr[16] = slot;
	  };
	  ;
	}
      else
	{			/*^cond.else */

    /*_.REFERENCED_VALUE__V17*/ meltfptr[16] = NULL;;
	}
      ;
      /*^compute */
   /*_#GET_INT__L5*/ meltfnum[4] =
	(melt_get_int
	 ((melt_ptr_t) ( /*_.REFERENCED_VALUE__V17*/ meltfptr[16])));;
      /*^compute */
   /*_#I__L6*/ meltfnum[5] =
	(( /*_#STRBUF_USEDLENGTH__L4*/ meltfnum[3]) <
	 ( /*_#GET_INT__L5*/ meltfnum[4]));;
      MELT_LOCATION ("warmelt-outobj.melt:4196:/ cond");
      /*cond */ if ( /*_#I__L6*/ meltfnum[5])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V18*/ meltfptr[17] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-outobj.melt:4196:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check limited implbuf"),
				  ("warmelt-outobj.melt")
				  ? ("warmelt-outobj.melt") : __FILE__,
				  (4196) ? (4196) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V18*/ meltfptr[17] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V16*/ meltfptr[15] = /*_.IFELSE___V18*/ meltfptr[17];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:4196:/ clear");
	     /*clear *//*_#STRBUF_USEDLENGTH__L4*/ meltfnum[3] = 0;
      /*^clear */
	     /*clear *//*_.REFERENCED_VALUE__V17*/ meltfptr[16] = 0;
      /*^clear */
	     /*clear *//*_#GET_INT__L5*/ meltfnum[4] = 0;
      /*^clear */
	     /*clear *//*_#I__L6*/ meltfnum[5] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V18*/ meltfptr[17] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V16*/ meltfptr[15] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    /*^compute */
    /*_.LET___V7*/ meltfptr[5] = /*_.IFCPP___V16*/ meltfptr[15];;

    MELT_LOCATION ("warmelt-outobj.melt:4161:/ clear");
	   /*clear *//*_.OLOC__V8*/ meltfptr[7] = 0;
    /*^clear */
	   /*clear *//*_.OCLOS__V9*/ meltfptr[8] = 0;
    /*^clear */
	   /*clear *//*_.OROUT__V10*/ meltfptr[9] = 0;
    /*^clear */
	   /*clear *//*_#CNT__L3*/ meltfnum[1] = 0;
    /*^clear */
	   /*clear *//*_.OUTPUT_LOCATION__V11*/ meltfptr[10] = 0;
    /*^clear */
	   /*clear *//*_.OUTPUT_C_CODE__V12*/ meltfptr[11] = 0;
    /*^clear */
	   /*clear *//*_.OUTPUT_C_CODE__V13*/ meltfptr[12] = 0;
    /*^clear */
	   /*clear *//*_.OUTPUT_C_CODE__V14*/ meltfptr[13] = 0;
    /*^clear */
	   /*clear *//*_.OUTPUT_C_CODE__V15*/ meltfptr[14] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V16*/ meltfptr[15] = 0;
    MELT_LOCATION ("warmelt-outobj.melt:4159:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.LET___V7*/ meltfptr[5];;

    {
      MELT_LOCATION ("warmelt-outobj.melt:4159:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V5*/ meltfptr[4] = 0;
    /*^clear */
	   /*clear *//*_.LET___V7*/ meltfptr[5] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("OUTPUCOD_OBJPUTCLOSUROUT", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_108_warmelt_outobj_OUTPUCOD_OBJPUTCLOSUROUT_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_108_warmelt_outobj_OUTPUCOD_OBJPUTCLOSUROUT */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_109_warmelt_outobj_OUTPUCOD_OBJPUTCLOSEDV (meltclosure_ptr_t
						    meltclosp_,
						    melt_ptr_t meltfirstargp_,
						    const melt_argdescr_cell_t
						    meltxargdescr_[],
						    union meltparam_un *
						    meltxargtab_,
						    const melt_argdescr_cell_t
						    meltxresdescr_[],
						    union meltparam_un *
						    meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_109_warmelt_outobj_OUTPUCOD_OBJPUTCLOSEDV_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_109_warmelt_outobj_OUTPUCOD_OBJPUTCLOSEDV_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 22
    melt_ptr_t mcfr_varptr[22];
#define MELTFRAM_NBVARNUM 5
    long mcfr_varnum[5];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_109_warmelt_outobj_OUTPUCOD_OBJPUTCLOSEDV is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_109_warmelt_outobj_OUTPUCOD_OBJPUTCLOSEDV_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 22; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_109_warmelt_outobj_OUTPUCOD_OBJPUTCLOSEDV nbval 22*/
  meltfram__.mcfr_nbvar = 22 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("OUTPUCOD_OBJPUTCLOSEDV", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-outobj.melt:4203:/ getarg");
 /*_.OPCLOV__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.DECLBUF__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.DECLBUF__V3*/ meltfptr[2])) !=
	      NULL);


  /*getarg#2 */
  /*^getarg */
  if (meltxargdescr_[1] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.IMPLBUF__V4*/ meltfptr[3] =
    (meltxargtab_[1].meltbp_aptr) ? (*(meltxargtab_[1].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3])) !=
	      NULL);


  /*getarg#3 */
  /*^getarg */
  if (meltxargdescr_[2] != MELTBPAR_LONG)
    goto lab_endgetargs;
 /*_#DEPTH__L1*/ meltfnum[0] = meltxargtab_[2].meltbp_long;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:4204:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L2*/ meltfnum[1] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.OPCLOV__V2*/ meltfptr[1]),
			     (melt_ptr_t) (( /*!CLASS_OBJPUTCLOSEDV */
					    meltfrout->tabval[0])));;
      MELT_LOCATION ("warmelt-outobj.melt:4204:/ cond");
      /*cond */ if ( /*_#IS_A__L2*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V6*/ meltfptr[5] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-outobj.melt:4204:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check opclor"),
				  ("warmelt-outobj.melt")
				  ? ("warmelt-outobj.melt") : __FILE__,
				  (4204) ? (4204) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V6*/ meltfptr[5] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V5*/ meltfptr[4] = /*_.IFELSE___V6*/ meltfptr[5];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:4204:/ clear");
	     /*clear *//*_#IS_A__L2*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V6*/ meltfptr[5] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V5*/ meltfptr[4] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-outobj.melt:4205:/ quasiblock");


    /*^getslot */
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.OPCLOV__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 0, "OBI_LOC");
  /*_.OLOC__V8*/ meltfptr[7] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-outobj.melt:4206:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.OPCLOV__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 1, "OPCLOV_CLOS");
  /*_.OCLOS__V9*/ meltfptr[8] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-outobj.melt:4207:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.OPCLOV__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 2, "OPCLOV_OFF");
  /*_.OOFF__V10*/ meltfptr[9] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-outobj.melt:4208:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.OPCLOV__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 3, "OPCLOV_CVAL");
  /*_.OCVAL__V11*/ meltfptr[10] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-outobj.melt:4209:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[3];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.IMPLBUF__V4*/ meltfptr[3];
      /*^apply.arg */
      argtab[1].meltbp_long = /*_#DEPTH__L1*/ meltfnum[0];
      /*^apply.arg */
      argtab[2].meltbp_cstring = "putclosedv";
      /*_.OUTPUT_LOCATION__V12*/ meltfptr[11] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!OUTPUT_LOCATION */ meltfrout->tabval[1])),
		    (melt_ptr_t) ( /*_.OLOC__V8*/ meltfptr[7]),
		    (MELTBPARSTR_PTR MELTBPARSTR_LONG MELTBPARSTR_CSTRING ""),
		    argtab, "", (union meltparam_un *) 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:4210:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   ("/*putclosv*/"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:4211:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				( /*_#DEPTH__L1*/ meltfnum[0]), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:4212:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   ("melt_assertmsg(\"putclosv checkclo\", melt_magic_discr((melt_ptr_t)("));
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:4213:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^msend */
    /*msend */
    {
      union meltparam_un argtab[3];
      memset (&argtab, 0, sizeof (argtab));
      /*^ojbmsend.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.DECLBUF__V3*/ meltfptr[2];
      /*^ojbmsend.arg */
      argtab[1].meltbp_aptr = (melt_ptr_t *) & /*_.IMPLBUF__V4*/ meltfptr[3];
      /*^ojbmsend.arg */
      argtab[2].meltbp_long = /*_#DEPTH__L1*/ meltfnum[0];
      /*_.OUTPUT_C_CODE__V13*/ meltfptr[12] =
	meltgc_send ((melt_ptr_t) ( /*_.OCLOS__V9*/ meltfptr[8]),
		     (melt_ptr_t) (( /*!OUTPUT_C_CODE */ meltfrout->
				    tabval[2])),
		     (MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_LONG ""),
		     argtab, "", (union meltparam_un *) 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:4214:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   (")) == MELTOBMAG_CLOSURE);"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:4215:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				( /*_#DEPTH__L1*/ meltfnum[0]), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:4216:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   ("melt_assertmsg(\"putclosv checkoff\", "));
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:4217:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^msend */
    /*msend */
    {
      union meltparam_un argtab[3];
      memset (&argtab, 0, sizeof (argtab));
      /*^ojbmsend.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.DECLBUF__V3*/ meltfptr[2];
      /*^ojbmsend.arg */
      argtab[1].meltbp_aptr = (melt_ptr_t *) & /*_.IMPLBUF__V4*/ meltfptr[3];
      /*^ojbmsend.arg */
      argtab[2].meltbp_long = /*_#DEPTH__L1*/ meltfnum[0];
      /*_.OUTPUT_C_CODE__V14*/ meltfptr[13] =
	meltgc_send ((melt_ptr_t) ( /*_.OOFF__V10*/ meltfptr[9]),
		     (melt_ptr_t) (( /*!OUTPUT_C_CODE */ meltfrout->
				    tabval[2])),
		     (MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_LONG ""),
		     argtab, "", (union meltparam_un *) 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:4218:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   (">= 0 && "));
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:4219:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^msend */
    /*msend */
    {
      union meltparam_un argtab[3];
      memset (&argtab, 0, sizeof (argtab));
      /*^ojbmsend.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.DECLBUF__V3*/ meltfptr[2];
      /*^ojbmsend.arg */
      argtab[1].meltbp_aptr = (melt_ptr_t *) & /*_.IMPLBUF__V4*/ meltfptr[3];
      /*^ojbmsend.arg */
      argtab[2].meltbp_long = /*_#DEPTH__L1*/ meltfnum[0];
      /*_.OUTPUT_C_CODE__V15*/ meltfptr[14] =
	meltgc_send ((melt_ptr_t) ( /*_.OOFF__V10*/ meltfptr[9]),
		     (melt_ptr_t) (( /*!OUTPUT_C_CODE */ meltfrout->
				    tabval[2])),
		     (MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_LONG ""),
		     argtab, "", (union meltparam_un *) 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:4220:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   ("< melt_closure_size((melt_ptr_t) ("));
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:4221:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^msend */
    /*msend */
    {
      union meltparam_un argtab[3];
      memset (&argtab, 0, sizeof (argtab));
      /*^ojbmsend.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.DECLBUF__V3*/ meltfptr[2];
      /*^ojbmsend.arg */
      argtab[1].meltbp_aptr = (melt_ptr_t *) & /*_.IMPLBUF__V4*/ meltfptr[3];
      /*^ojbmsend.arg */
      argtab[2].meltbp_long = /*_#DEPTH__L1*/ meltfnum[0];
      /*_.OUTPUT_C_CODE__V16*/ meltfptr[15] =
	meltgc_send ((melt_ptr_t) ( /*_.OCLOS__V9*/ meltfptr[8]),
		     (melt_ptr_t) (( /*!OUTPUT_C_CODE */ meltfrout->
				    tabval[2])),
		     (MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_LONG ""),
		     argtab, "", (union meltparam_un *) 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:4222:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   (")));"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:4223:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				( /*_#DEPTH__L1*/ meltfnum[0]), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:4224:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   ("((meltclosure_ptr_t)"));
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:4225:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^msend */
    /*msend */
    {
      union meltparam_un argtab[3];
      memset (&argtab, 0, sizeof (argtab));
      /*^ojbmsend.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.DECLBUF__V3*/ meltfptr[2];
      /*^ojbmsend.arg */
      argtab[1].meltbp_aptr = (melt_ptr_t *) & /*_.IMPLBUF__V4*/ meltfptr[3];
      /*^ojbmsend.arg */
      argtab[2].meltbp_long = /*_#DEPTH__L1*/ meltfnum[0];
      /*_.OUTPUT_C_CODE__V17*/ meltfptr[16] =
	meltgc_send ((melt_ptr_t) ( /*_.OCLOS__V9*/ meltfptr[8]),
		     (melt_ptr_t) (( /*!OUTPUT_C_CODE */ meltfrout->
				    tabval[2])),
		     (MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_LONG ""),
		     argtab, "", (union meltparam_un *) 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:4226:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   (")->tabval["));
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:4227:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^msend */
    /*msend */
    {
      union meltparam_un argtab[3];
      memset (&argtab, 0, sizeof (argtab));
      /*^ojbmsend.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.DECLBUF__V3*/ meltfptr[2];
      /*^ojbmsend.arg */
      argtab[1].meltbp_aptr = (melt_ptr_t *) & /*_.IMPLBUF__V4*/ meltfptr[3];
      /*^ojbmsend.arg */
      argtab[2].meltbp_long = /*_#DEPTH__L1*/ meltfnum[0];
      /*_.OUTPUT_C_CODE__V18*/ meltfptr[17] =
	meltgc_send ((melt_ptr_t) ( /*_.OOFF__V10*/ meltfptr[9]),
		     (melt_ptr_t) (( /*!OUTPUT_C_CODE */ meltfrout->
				    tabval[2])),
		     (MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_LONG ""),
		     argtab, "", (union meltparam_un *) 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:4228:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   ("] = (melt_ptr_t)("));
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:4229:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^msend */
    /*msend */
    {
      union meltparam_un argtab[3];
      memset (&argtab, 0, sizeof (argtab));
      /*^ojbmsend.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.DECLBUF__V3*/ meltfptr[2];
      /*^ojbmsend.arg */
      argtab[1].meltbp_aptr = (melt_ptr_t *) & /*_.IMPLBUF__V4*/ meltfptr[3];
      /*^ojbmsend.arg */
      argtab[2].meltbp_long = /*_#DEPTH__L1*/ meltfnum[0];
      /*_.OUTPUT_C_CODE__V19*/ meltfptr[18] =
	meltgc_send ((melt_ptr_t) ( /*_.OCVAL__V11*/ meltfptr[10]),
		     (melt_ptr_t) (( /*!OUTPUT_C_CODE */ meltfrout->
				    tabval[2])),
		     (MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_LONG ""),
		     argtab, "", (union meltparam_un *) 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:4230:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   (");"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:4231:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				( /*_#DEPTH__L1*/ meltfnum[0]), 0);
    }
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:4232:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#STRBUF_USEDLENGTH__L3*/ meltfnum[1] =
	melt_strbuf_usedlength ((melt_ptr_t)
				( /*_.IMPLBUF__V4*/ meltfptr[3]));;
      MELT_LOCATION ("warmelt-outobj.melt:4233:/ cond");
      /*cond */ if (
		     /*ifisa */
		     melt_is_instance_of ((melt_ptr_t)
					  (( /*!BUFFER_LIMIT_CONT */
					    meltfrout->tabval[3])),
					  (melt_ptr_t) (( /*!CLASS_REFERENCE */ meltfrout->tabval[4])))
	)			/*then */
	{
	  /*^cond.then */
	  /*^getslot */
	  {
	    melt_ptr_t slot = NULL, obj = NULL;
	    obj =
	      (melt_ptr_t) (( /*!BUFFER_LIMIT_CONT */ meltfrout->
			     tabval[3])) /*=obj*/ ;
	    melt_object_get_field (slot, obj, 0, "REFERENCED_VALUE");
     /*_.REFERENCED_VALUE__V21*/ meltfptr[20] = slot;
	  };
	  ;
	}
      else
	{			/*^cond.else */

    /*_.REFERENCED_VALUE__V21*/ meltfptr[20] = NULL;;
	}
      ;
      /*^compute */
   /*_#GET_INT__L4*/ meltfnum[3] =
	(melt_get_int
	 ((melt_ptr_t) ( /*_.REFERENCED_VALUE__V21*/ meltfptr[20])));;
      /*^compute */
   /*_#I__L5*/ meltfnum[4] =
	(( /*_#STRBUF_USEDLENGTH__L3*/ meltfnum[1]) <
	 ( /*_#GET_INT__L4*/ meltfnum[3]));;
      MELT_LOCATION ("warmelt-outobj.melt:4232:/ cond");
      /*cond */ if ( /*_#I__L5*/ meltfnum[4])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V22*/ meltfptr[21] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-outobj.melt:4232:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check limited implbuf"),
				  ("warmelt-outobj.melt")
				  ? ("warmelt-outobj.melt") : __FILE__,
				  (4232) ? (4232) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V22*/ meltfptr[21] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V20*/ meltfptr[19] = /*_.IFELSE___V22*/ meltfptr[21];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:4232:/ clear");
	     /*clear *//*_#STRBUF_USEDLENGTH__L3*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.REFERENCED_VALUE__V21*/ meltfptr[20] = 0;
      /*^clear */
	     /*clear *//*_#GET_INT__L4*/ meltfnum[3] = 0;
      /*^clear */
	     /*clear *//*_#I__L5*/ meltfnum[4] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V22*/ meltfptr[21] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V20*/ meltfptr[19] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    /*^compute */
    /*_.LET___V7*/ meltfptr[5] = /*_.IFCPP___V20*/ meltfptr[19];;

    MELT_LOCATION ("warmelt-outobj.melt:4205:/ clear");
	   /*clear *//*_.OLOC__V8*/ meltfptr[7] = 0;
    /*^clear */
	   /*clear *//*_.OCLOS__V9*/ meltfptr[8] = 0;
    /*^clear */
	   /*clear *//*_.OOFF__V10*/ meltfptr[9] = 0;
    /*^clear */
	   /*clear *//*_.OCVAL__V11*/ meltfptr[10] = 0;
    /*^clear */
	   /*clear *//*_.OUTPUT_LOCATION__V12*/ meltfptr[11] = 0;
    /*^clear */
	   /*clear *//*_.OUTPUT_C_CODE__V13*/ meltfptr[12] = 0;
    /*^clear */
	   /*clear *//*_.OUTPUT_C_CODE__V14*/ meltfptr[13] = 0;
    /*^clear */
	   /*clear *//*_.OUTPUT_C_CODE__V15*/ meltfptr[14] = 0;
    /*^clear */
	   /*clear *//*_.OUTPUT_C_CODE__V16*/ meltfptr[15] = 0;
    /*^clear */
	   /*clear *//*_.OUTPUT_C_CODE__V17*/ meltfptr[16] = 0;
    /*^clear */
	   /*clear *//*_.OUTPUT_C_CODE__V18*/ meltfptr[17] = 0;
    /*^clear */
	   /*clear *//*_.OUTPUT_C_CODE__V19*/ meltfptr[18] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V20*/ meltfptr[19] = 0;
    MELT_LOCATION ("warmelt-outobj.melt:4203:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.LET___V7*/ meltfptr[5];;

    {
      MELT_LOCATION ("warmelt-outobj.melt:4203:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V5*/ meltfptr[4] = 0;
    /*^clear */
	   /*clear *//*_.LET___V7*/ meltfptr[5] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("OUTPUCOD_OBJPUTCLOSEDV", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_109_warmelt_outobj_OUTPUCOD_OBJPUTCLOSEDV_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_109_warmelt_outobj_OUTPUCOD_OBJPUTCLOSEDV */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_110_warmelt_outobj_OUTPUCOD_OBJPUTCLOSEDNOTNULLV (meltclosure_ptr_t
							   meltclosp_,
							   melt_ptr_t
							   meltfirstargp_,
							   const
							   melt_argdescr_cell_t
							   meltxargdescr_[],
							   union meltparam_un
							   * meltxargtab_,
							   const
							   melt_argdescr_cell_t
							   meltxresdescr_[],
							   union meltparam_un
							   * meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_110_warmelt_outobj_OUTPUCOD_OBJPUTCLOSEDNOTNULLV_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_110_warmelt_outobj_OUTPUCOD_OBJPUTCLOSEDNOTNULLV_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 23
    melt_ptr_t mcfr_varptr[23];
#define MELTFRAM_NBVARNUM 5
    long mcfr_varnum[5];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_110_warmelt_outobj_OUTPUCOD_OBJPUTCLOSEDNOTNULLV is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct
	 frame_meltrout_110_warmelt_outobj_OUTPUCOD_OBJPUTCLOSEDNOTNULLV_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 23; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_110_warmelt_outobj_OUTPUCOD_OBJPUTCLOSEDNOTNULLV nbval 23*/
  meltfram__.mcfr_nbvar = 23 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("OUTPUCOD_OBJPUTCLOSEDNOTNULLV", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-outobj.melt:4239:/ getarg");
 /*_.OPCLOV__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.DECLBUF__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.DECLBUF__V3*/ meltfptr[2])) !=
	      NULL);


  /*getarg#2 */
  /*^getarg */
  if (meltxargdescr_[1] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.IMPLBUF__V4*/ meltfptr[3] =
    (meltxargtab_[1].meltbp_aptr) ? (*(meltxargtab_[1].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3])) !=
	      NULL);


  /*getarg#3 */
  /*^getarg */
  if (meltxargdescr_[2] != MELTBPAR_LONG)
    goto lab_endgetargs;
 /*_#DEPTH__L1*/ meltfnum[0] = meltxargtab_[2].meltbp_long;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:4240:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L2*/ meltfnum[1] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.OPCLOV__V2*/ meltfptr[1]),
			     (melt_ptr_t) (( /*!CLASS_OBJPUTCLOSEDNOTNULLV */
					    meltfrout->tabval[0])));;
      MELT_LOCATION ("warmelt-outobj.melt:4240:/ cond");
      /*cond */ if ( /*_#IS_A__L2*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V6*/ meltfptr[5] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-outobj.melt:4240:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check opclor"),
				  ("warmelt-outobj.melt")
				  ? ("warmelt-outobj.melt") : __FILE__,
				  (4240) ? (4240) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V6*/ meltfptr[5] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V5*/ meltfptr[4] = /*_.IFELSE___V6*/ meltfptr[5];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:4240:/ clear");
	     /*clear *//*_#IS_A__L2*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V6*/ meltfptr[5] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V5*/ meltfptr[4] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-outobj.melt:4241:/ quasiblock");


    /*^getslot */
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.OPCLOV__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 0, "OBI_LOC");
  /*_.OLOC__V8*/ meltfptr[7] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-outobj.melt:4242:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.OPCLOV__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 1, "OPCLOV_CLOS");
  /*_.OCLOS__V9*/ meltfptr[8] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-outobj.melt:4243:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.OPCLOV__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 2, "OPCLOV_OFF");
  /*_.OOFF__V10*/ meltfptr[9] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-outobj.melt:4244:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.OPCLOV__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 3, "OPCLOV_CVAL");
  /*_.OCVAL__V11*/ meltfptr[10] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-outobj.melt:4245:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[3];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.IMPLBUF__V4*/ meltfptr[3];
      /*^apply.arg */
      argtab[1].meltbp_long = /*_#DEPTH__L1*/ meltfnum[0];
      /*^apply.arg */
      argtab[2].meltbp_cstring = "putclosednotnullv";
      /*_.OUTPUT_LOCATION__V12*/ meltfptr[11] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!OUTPUT_LOCATION */ meltfrout->tabval[1])),
		    (melt_ptr_t) ( /*_.OLOC__V8*/ meltfptr[7]),
		    (MELTBPARSTR_PTR MELTBPARSTR_LONG MELTBPARSTR_CSTRING ""),
		    argtab, "", (union meltparam_un *) 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:4246:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   ("/*putclosvnotnull*/"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:4247:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				( /*_#DEPTH__L1*/ meltfnum[0]), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:4248:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   ("melt_assertmsg(\"putclosvnotnull checkclo\", melt_magic_discr((melt_ptr_t)("));
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:4249:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^msend */
    /*msend */
    {
      union meltparam_un argtab[3];
      memset (&argtab, 0, sizeof (argtab));
      /*^ojbmsend.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.DECLBUF__V3*/ meltfptr[2];
      /*^ojbmsend.arg */
      argtab[1].meltbp_aptr = (melt_ptr_t *) & /*_.IMPLBUF__V4*/ meltfptr[3];
      /*^ojbmsend.arg */
      argtab[2].meltbp_long = /*_#DEPTH__L1*/ meltfnum[0];
      /*_.OUTPUT_C_CODE__V13*/ meltfptr[12] =
	meltgc_send ((melt_ptr_t) ( /*_.OCLOS__V9*/ meltfptr[8]),
		     (melt_ptr_t) (( /*!OUTPUT_C_CODE */ meltfrout->
				    tabval[2])),
		     (MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_LONG ""),
		     argtab, "", (union meltparam_un *) 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:4250:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   (")) == MELTOBMAG_CLOSURE);"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:4251:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				( /*_#DEPTH__L1*/ meltfnum[0]), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:4252:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   ("melt_assertmsg(\"putclosvnotnull checknotnullval\", NULL != "));
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:4253:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^msend */
    /*msend */
    {
      union meltparam_un argtab[3];
      memset (&argtab, 0, sizeof (argtab));
      /*^ojbmsend.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.DECLBUF__V3*/ meltfptr[2];
      /*^ojbmsend.arg */
      argtab[1].meltbp_aptr = (melt_ptr_t *) & /*_.IMPLBUF__V4*/ meltfptr[3];
      /*^ojbmsend.arg */
      argtab[2].meltbp_long = /*_#DEPTH__L1*/ meltfnum[0];
      /*_.OUTPUT_C_CODE__V14*/ meltfptr[13] =
	meltgc_send ((melt_ptr_t) ( /*_.OCVAL__V11*/ meltfptr[10]),
		     (melt_ptr_t) (( /*!OUTPUT_C_CODE */ meltfrout->
				    tabval[2])),
		     (MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_LONG ""),
		     argtab, "", (union meltparam_un *) 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:4254:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   (");"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:4255:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				( /*_#DEPTH__L1*/ meltfnum[0]), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:4256:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   ("melt_assertmsg(\"putclosvnotnull checkoff\", "));
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:4257:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^msend */
    /*msend */
    {
      union meltparam_un argtab[3];
      memset (&argtab, 0, sizeof (argtab));
      /*^ojbmsend.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.DECLBUF__V3*/ meltfptr[2];
      /*^ojbmsend.arg */
      argtab[1].meltbp_aptr = (melt_ptr_t *) & /*_.IMPLBUF__V4*/ meltfptr[3];
      /*^ojbmsend.arg */
      argtab[2].meltbp_long = /*_#DEPTH__L1*/ meltfnum[0];
      /*_.OUTPUT_C_CODE__V15*/ meltfptr[14] =
	meltgc_send ((melt_ptr_t) ( /*_.OOFF__V10*/ meltfptr[9]),
		     (melt_ptr_t) (( /*!OUTPUT_C_CODE */ meltfrout->
				    tabval[2])),
		     (MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_LONG ""),
		     argtab, "", (union meltparam_un *) 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:4258:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   (">= 0 && "));
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:4259:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^msend */
    /*msend */
    {
      union meltparam_un argtab[3];
      memset (&argtab, 0, sizeof (argtab));
      /*^ojbmsend.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.DECLBUF__V3*/ meltfptr[2];
      /*^ojbmsend.arg */
      argtab[1].meltbp_aptr = (melt_ptr_t *) & /*_.IMPLBUF__V4*/ meltfptr[3];
      /*^ojbmsend.arg */
      argtab[2].meltbp_long = /*_#DEPTH__L1*/ meltfnum[0];
      /*_.OUTPUT_C_CODE__V16*/ meltfptr[15] =
	meltgc_send ((melt_ptr_t) ( /*_.OOFF__V10*/ meltfptr[9]),
		     (melt_ptr_t) (( /*!OUTPUT_C_CODE */ meltfrout->
				    tabval[2])),
		     (MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_LONG ""),
		     argtab, "", (union meltparam_un *) 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:4260:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   ("< melt_closure_size((melt_ptr_t) ("));
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:4261:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^msend */
    /*msend */
    {
      union meltparam_un argtab[3];
      memset (&argtab, 0, sizeof (argtab));
      /*^ojbmsend.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.DECLBUF__V3*/ meltfptr[2];
      /*^ojbmsend.arg */
      argtab[1].meltbp_aptr = (melt_ptr_t *) & /*_.IMPLBUF__V4*/ meltfptr[3];
      /*^ojbmsend.arg */
      argtab[2].meltbp_long = /*_#DEPTH__L1*/ meltfnum[0];
      /*_.OUTPUT_C_CODE__V17*/ meltfptr[16] =
	meltgc_send ((melt_ptr_t) ( /*_.OCLOS__V9*/ meltfptr[8]),
		     (melt_ptr_t) (( /*!OUTPUT_C_CODE */ meltfrout->
				    tabval[2])),
		     (MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_LONG ""),
		     argtab, "", (union meltparam_un *) 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:4262:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   (")));"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:4263:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				( /*_#DEPTH__L1*/ meltfnum[0]), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:4264:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   ("((meltclosure_ptr_t)"));
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:4265:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^msend */
    /*msend */
    {
      union meltparam_un argtab[3];
      memset (&argtab, 0, sizeof (argtab));
      /*^ojbmsend.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.DECLBUF__V3*/ meltfptr[2];
      /*^ojbmsend.arg */
      argtab[1].meltbp_aptr = (melt_ptr_t *) & /*_.IMPLBUF__V4*/ meltfptr[3];
      /*^ojbmsend.arg */
      argtab[2].meltbp_long = /*_#DEPTH__L1*/ meltfnum[0];
      /*_.OUTPUT_C_CODE__V18*/ meltfptr[17] =
	meltgc_send ((melt_ptr_t) ( /*_.OCLOS__V9*/ meltfptr[8]),
		     (melt_ptr_t) (( /*!OUTPUT_C_CODE */ meltfrout->
				    tabval[2])),
		     (MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_LONG ""),
		     argtab, "", (union meltparam_un *) 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:4266:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   (")->tabval["));
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:4267:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^msend */
    /*msend */
    {
      union meltparam_un argtab[3];
      memset (&argtab, 0, sizeof (argtab));
      /*^ojbmsend.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.DECLBUF__V3*/ meltfptr[2];
      /*^ojbmsend.arg */
      argtab[1].meltbp_aptr = (melt_ptr_t *) & /*_.IMPLBUF__V4*/ meltfptr[3];
      /*^ojbmsend.arg */
      argtab[2].meltbp_long = /*_#DEPTH__L1*/ meltfnum[0];
      /*_.OUTPUT_C_CODE__V19*/ meltfptr[18] =
	meltgc_send ((melt_ptr_t) ( /*_.OOFF__V10*/ meltfptr[9]),
		     (melt_ptr_t) (( /*!OUTPUT_C_CODE */ meltfrout->
				    tabval[2])),
		     (MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_LONG ""),
		     argtab, "", (union meltparam_un *) 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:4268:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   ("] = (melt_ptr_t)("));
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:4269:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^msend */
    /*msend */
    {
      union meltparam_un argtab[3];
      memset (&argtab, 0, sizeof (argtab));
      /*^ojbmsend.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.DECLBUF__V3*/ meltfptr[2];
      /*^ojbmsend.arg */
      argtab[1].meltbp_aptr = (melt_ptr_t *) & /*_.IMPLBUF__V4*/ meltfptr[3];
      /*^ojbmsend.arg */
      argtab[2].meltbp_long = /*_#DEPTH__L1*/ meltfnum[0];
      /*_.OUTPUT_C_CODE__V20*/ meltfptr[19] =
	meltgc_send ((melt_ptr_t) ( /*_.OCVAL__V11*/ meltfptr[10]),
		     (melt_ptr_t) (( /*!OUTPUT_C_CODE */ meltfrout->
				    tabval[2])),
		     (MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_LONG ""),
		     argtab, "", (union meltparam_un *) 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:4270:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   (");"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:4271:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				( /*_#DEPTH__L1*/ meltfnum[0]), 0);
    }
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:4272:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#STRBUF_USEDLENGTH__L3*/ meltfnum[1] =
	melt_strbuf_usedlength ((melt_ptr_t)
				( /*_.IMPLBUF__V4*/ meltfptr[3]));;
      MELT_LOCATION ("warmelt-outobj.melt:4273:/ cond");
      /*cond */ if (
		     /*ifisa */
		     melt_is_instance_of ((melt_ptr_t)
					  (( /*!BUFFER_LIMIT_CONT */
					    meltfrout->tabval[3])),
					  (melt_ptr_t) (( /*!CLASS_REFERENCE */ meltfrout->tabval[4])))
	)			/*then */
	{
	  /*^cond.then */
	  /*^getslot */
	  {
	    melt_ptr_t slot = NULL, obj = NULL;
	    obj =
	      (melt_ptr_t) (( /*!BUFFER_LIMIT_CONT */ meltfrout->
			     tabval[3])) /*=obj*/ ;
	    melt_object_get_field (slot, obj, 0, "REFERENCED_VALUE");
     /*_.REFERENCED_VALUE__V22*/ meltfptr[21] = slot;
	  };
	  ;
	}
      else
	{			/*^cond.else */

    /*_.REFERENCED_VALUE__V22*/ meltfptr[21] = NULL;;
	}
      ;
      /*^compute */
   /*_#GET_INT__L4*/ meltfnum[3] =
	(melt_get_int
	 ((melt_ptr_t) ( /*_.REFERENCED_VALUE__V22*/ meltfptr[21])));;
      /*^compute */
   /*_#I__L5*/ meltfnum[4] =
	(( /*_#STRBUF_USEDLENGTH__L3*/ meltfnum[1]) <
	 ( /*_#GET_INT__L4*/ meltfnum[3]));;
      MELT_LOCATION ("warmelt-outobj.melt:4272:/ cond");
      /*cond */ if ( /*_#I__L5*/ meltfnum[4])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V23*/ meltfptr[22] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-outobj.melt:4272:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check limited implbuf"),
				  ("warmelt-outobj.melt")
				  ? ("warmelt-outobj.melt") : __FILE__,
				  (4272) ? (4272) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V23*/ meltfptr[22] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V21*/ meltfptr[20] = /*_.IFELSE___V23*/ meltfptr[22];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:4272:/ clear");
	     /*clear *//*_#STRBUF_USEDLENGTH__L3*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.REFERENCED_VALUE__V22*/ meltfptr[21] = 0;
      /*^clear */
	     /*clear *//*_#GET_INT__L4*/ meltfnum[3] = 0;
      /*^clear */
	     /*clear *//*_#I__L5*/ meltfnum[4] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V23*/ meltfptr[22] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V21*/ meltfptr[20] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    /*^compute */
    /*_.LET___V7*/ meltfptr[5] = /*_.IFCPP___V21*/ meltfptr[20];;

    MELT_LOCATION ("warmelt-outobj.melt:4241:/ clear");
	   /*clear *//*_.OLOC__V8*/ meltfptr[7] = 0;
    /*^clear */
	   /*clear *//*_.OCLOS__V9*/ meltfptr[8] = 0;
    /*^clear */
	   /*clear *//*_.OOFF__V10*/ meltfptr[9] = 0;
    /*^clear */
	   /*clear *//*_.OCVAL__V11*/ meltfptr[10] = 0;
    /*^clear */
	   /*clear *//*_.OUTPUT_LOCATION__V12*/ meltfptr[11] = 0;
    /*^clear */
	   /*clear *//*_.OUTPUT_C_CODE__V13*/ meltfptr[12] = 0;
    /*^clear */
	   /*clear *//*_.OUTPUT_C_CODE__V14*/ meltfptr[13] = 0;
    /*^clear */
	   /*clear *//*_.OUTPUT_C_CODE__V15*/ meltfptr[14] = 0;
    /*^clear */
	   /*clear *//*_.OUTPUT_C_CODE__V16*/ meltfptr[15] = 0;
    /*^clear */
	   /*clear *//*_.OUTPUT_C_CODE__V17*/ meltfptr[16] = 0;
    /*^clear */
	   /*clear *//*_.OUTPUT_C_CODE__V18*/ meltfptr[17] = 0;
    /*^clear */
	   /*clear *//*_.OUTPUT_C_CODE__V19*/ meltfptr[18] = 0;
    /*^clear */
	   /*clear *//*_.OUTPUT_C_CODE__V20*/ meltfptr[19] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V21*/ meltfptr[20] = 0;
    MELT_LOCATION ("warmelt-outobj.melt:4239:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.LET___V7*/ meltfptr[5];;

    {
      MELT_LOCATION ("warmelt-outobj.melt:4239:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V5*/ meltfptr[4] = 0;
    /*^clear */
	   /*clear *//*_.LET___V7*/ meltfptr[5] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("OUTPUCOD_OBJPUTCLOSEDNOTNULLV", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_110_warmelt_outobj_OUTPUCOD_OBJPUTCLOSEDNOTNULLV_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_110_warmelt_outobj_OUTPUCOD_OBJPUTCLOSEDNOTNULLV */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_111_warmelt_outobj_OUTPUCOD_OBJPUTROUTCONST (meltclosure_ptr_t
						      meltclosp_,
						      melt_ptr_t
						      meltfirstargp_,
						      const
						      melt_argdescr_cell_t
						      meltxargdescr_[],
						      union meltparam_un *
						      meltxargtab_,
						      const
						      melt_argdescr_cell_t
						      meltxresdescr_[],
						      union meltparam_un *
						      meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_111_warmelt_outobj_OUTPUCOD_OBJPUTROUTCONST_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_111_warmelt_outobj_OUTPUCOD_OBJPUTROUTCONST_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 23
    melt_ptr_t mcfr_varptr[23];
#define MELTFRAM_NBVARNUM 7
    long mcfr_varnum[7];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_111_warmelt_outobj_OUTPUCOD_OBJPUTROUTCONST is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_111_warmelt_outobj_OUTPUCOD_OBJPUTROUTCONST_st
	 *) meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 23; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_111_warmelt_outobj_OUTPUCOD_OBJPUTROUTCONST nbval 23*/
  meltfram__.mcfr_nbvar = 23 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("OUTPUCOD_OBJPUTROUTCONST", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-outobj.melt:4279:/ getarg");
 /*_.OPRCONST__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.DECLBUF__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.DECLBUF__V3*/ meltfptr[2])) !=
	      NULL);


  /*getarg#2 */
  /*^getarg */
  if (meltxargdescr_[1] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.IMPLBUF__V4*/ meltfptr[3] =
    (meltxargtab_[1].meltbp_aptr) ? (*(meltxargtab_[1].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3])) !=
	      NULL);


  /*getarg#3 */
  /*^getarg */
  if (meltxargdescr_[2] != MELTBPAR_LONG)
    goto lab_endgetargs;
 /*_#DEPTH__L1*/ meltfnum[0] = meltxargtab_[2].meltbp_long;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:4280:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L2*/ meltfnum[1] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.OPRCONST__V2*/ meltfptr[1]),
			     (melt_ptr_t) (( /*!CLASS_OBJPUTROUTCONST */
					    meltfrout->tabval[0])));;
      MELT_LOCATION ("warmelt-outobj.melt:4280:/ cond");
      /*cond */ if ( /*_#IS_A__L2*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V6*/ meltfptr[5] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-outobj.melt:4280:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check oprconst"),
				  ("warmelt-outobj.melt")
				  ? ("warmelt-outobj.melt") : __FILE__,
				  (4280) ? (4280) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V6*/ meltfptr[5] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V5*/ meltfptr[4] = /*_.IFELSE___V6*/ meltfptr[5];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:4280:/ clear");
	     /*clear *//*_#IS_A__L2*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V6*/ meltfptr[5] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V5*/ meltfptr[4] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-outobj.melt:4281:/ quasiblock");


    /*^getslot */
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.OPRCONST__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 0, "OBI_LOC");
  /*_.OLOC__V8*/ meltfptr[7] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-outobj.melt:4282:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.OPRCONST__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 1, "OPRCONST_ROUT");
  /*_.OROUT__V9*/ meltfptr[8] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-outobj.melt:4283:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#IS_A__L3*/ meltfnum[1] =
      melt_is_instance_of ((melt_ptr_t) ( /*_.OROUT__V9*/ meltfptr[8]),
			   (melt_ptr_t) (( /*!CLASS_OBJINITROUTINE */
					  meltfrout->tabval[1])));;
    MELT_LOCATION ("warmelt-outobj.melt:4283:/ cond");
    /*cond */ if ( /*_#IS_A__L3*/ meltfnum[1])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  /*^getslot */
	  {
	    melt_ptr_t slot = NULL, obj = NULL;
	    obj = (melt_ptr_t) ( /*_.OROUT__V9*/ meltfptr[8]) /*=obj*/ ;
	    melt_object_get_field (slot, obj, 1, "OIE_CNAME");
    /*_.OIE_CNAME__V11*/ meltfptr[10] = slot;
	  };
	  ;
	  /*_.OROUTNAM__V10*/ meltfptr[9] =
	    /*_.OIE_CNAME__V11*/ meltfptr[10];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-outobj.melt:4283:/ clear");
	     /*clear *//*_.OIE_CNAME__V11*/ meltfptr[10] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

  /*_.OROUTNAM__V10*/ meltfptr[9] = NULL;;
      }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:4284:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.OPRCONST__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 2, "OPRCONST_OFF");
  /*_.OOFF__V12*/ meltfptr[10] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-outobj.melt:4285:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.OPRCONST__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 3, "OPRCONST_CVAL");
  /*_.OCVAL__V13*/ meltfptr[12] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-outobj.melt:4286:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[3];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.IMPLBUF__V4*/ meltfptr[3];
      /*^apply.arg */
      argtab[1].meltbp_long = /*_#DEPTH__L1*/ meltfnum[0];
      /*^apply.arg */
      argtab[2].meltbp_cstring = "putroutconst";
      /*_.OUTPUT_LOCATION__V14*/ meltfptr[13] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!OUTPUT_LOCATION */ meltfrout->tabval[2])),
		    (melt_ptr_t) ( /*_.OLOC__V8*/ meltfptr[7]),
		    (MELTBPARSTR_PTR MELTBPARSTR_LONG MELTBPARSTR_CSTRING ""),
		    argtab, "", (union meltparam_un *) 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:4287:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   ("/*putroutconst*/"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:4288:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				( /*_#DEPTH__L1*/ meltfnum[0]), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:4289:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   ("if (MELT_HAS_INITIAL_ENVIRONMENT) melt_assertmsg(\"putroutconst checkrout\
\", melt_magic_discr((melt_ptr_t)("));
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:4290:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^msend */
    /*msend */
    {
      union meltparam_un argtab[3];
      memset (&argtab, 0, sizeof (argtab));
      /*^ojbmsend.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.DECLBUF__V3*/ meltfptr[2];
      /*^ojbmsend.arg */
      argtab[1].meltbp_aptr = (melt_ptr_t *) & /*_.IMPLBUF__V4*/ meltfptr[3];
      /*^ojbmsend.arg */
      argtab[2].meltbp_long = /*_#DEPTH__L1*/ meltfnum[0];
      /*_.OUTPUT_C_CODE__V15*/ meltfptr[14] =
	meltgc_send ((melt_ptr_t) ( /*_.OROUT__V9*/ meltfptr[8]),
		     (melt_ptr_t) (( /*!OUTPUT_C_CODE */ meltfrout->
				    tabval[3])),
		     (MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_LONG ""),
		     argtab, "", (union meltparam_un *) 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:4291:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   (")) == MELTOBMAG_ROUTINE);"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:4292:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				( /*_#DEPTH__L1*/ meltfnum[0]), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:4294:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   ("if (MELT_HAS_INITIAL_ENVIRONMENT) melt_checkmsg(\"putroutconst constnull."));
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:4295:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#IS_STRING__L4*/ meltfnum[3] =
      (melt_magic_discr ((melt_ptr_t) ( /*_.OROUTNAM__V10*/ meltfptr[9])) ==
       MELTOBMAG_STRING);;
    MELT_LOCATION ("warmelt-outobj.melt:4295:/ cond");
    /*cond */ if ( /*_#IS_STRING__L4*/ meltfnum[3])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{


	  {
	    /*^locexp */
	    /*add2sbuf_string */
	      meltgc_add_strbuf ((melt_ptr_t)
				 ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				 melt_string_str ((melt_ptr_t)
						  ( /*_.OROUTNAM__V10*/
						   meltfptr[9])));
	  }
	  ;
	  /*epilog */
	}
	;
      }				/*noelse */
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:4296:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   ("#"));
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:4297:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^msend */
    /*msend */
    {
      union meltparam_un argtab[3];
      memset (&argtab, 0, sizeof (argtab));
      /*^ojbmsend.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.DECLBUF__V3*/ meltfptr[2];
      /*^ojbmsend.arg */
      argtab[1].meltbp_aptr = (melt_ptr_t *) & /*_.IMPLBUF__V4*/ meltfptr[3];
      /*^ojbmsend.arg */
      argtab[2].meltbp_long = /*_#DEPTH__L1*/ meltfnum[0];
      /*_.OUTPUT_C_CODE__V16*/ meltfptr[15] =
	meltgc_send ((melt_ptr_t) ( /*_.OOFF__V12*/ meltfptr[10]),
		     (melt_ptr_t) (( /*!OUTPUT_C_CODE */ meltfrout->
				    tabval[3])),
		     (MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_LONG ""),
		     argtab, "", (union meltparam_un *) 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:4298:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   ("\", NULL != ("));
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:4299:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^msend */
    /*msend */
    {
      union meltparam_un argtab[3];
      memset (&argtab, 0, sizeof (argtab));
      /*^ojbmsend.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.DECLBUF__V3*/ meltfptr[2];
      /*^ojbmsend.arg */
      argtab[1].meltbp_aptr = (melt_ptr_t *) & /*_.IMPLBUF__V4*/ meltfptr[3];
      /*^ojbmsend.arg */
      argtab[2].meltbp_long = /*_#DEPTH__L1*/ meltfnum[0];
      /*_.OUTPUT_C_CODE__V17*/ meltfptr[16] =
	meltgc_send ((melt_ptr_t) ( /*_.OCVAL__V13*/ meltfptr[12]),
		     (melt_ptr_t) (( /*!OUTPUT_C_CODE */ meltfrout->
				    tabval[3])),
		     (MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_LONG ""),
		     argtab, "", (union meltparam_un *) 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:4300:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   ("));"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:4301:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				( /*_#DEPTH__L1*/ meltfnum[0]), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:4303:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   ("((meltroutine_ptr_t)"));
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:4304:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^msend */
    /*msend */
    {
      union meltparam_un argtab[3];
      memset (&argtab, 0, sizeof (argtab));
      /*^ojbmsend.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.DECLBUF__V3*/ meltfptr[2];
      /*^ojbmsend.arg */
      argtab[1].meltbp_aptr = (melt_ptr_t *) & /*_.IMPLBUF__V4*/ meltfptr[3];
      /*^ojbmsend.arg */
      argtab[2].meltbp_long = /*_#DEPTH__L1*/ meltfnum[0];
      /*_.OUTPUT_C_CODE__V18*/ meltfptr[17] =
	meltgc_send ((melt_ptr_t) ( /*_.OROUT__V9*/ meltfptr[8]),
		     (melt_ptr_t) (( /*!OUTPUT_C_CODE */ meltfrout->
				    tabval[3])),
		     (MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_LONG ""),
		     argtab, "", (union meltparam_un *) 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:4305:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   (")->tabval["));
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:4306:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^msend */
    /*msend */
    {
      union meltparam_un argtab[3];
      memset (&argtab, 0, sizeof (argtab));
      /*^ojbmsend.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.DECLBUF__V3*/ meltfptr[2];
      /*^ojbmsend.arg */
      argtab[1].meltbp_aptr = (melt_ptr_t *) & /*_.IMPLBUF__V4*/ meltfptr[3];
      /*^ojbmsend.arg */
      argtab[2].meltbp_long = /*_#DEPTH__L1*/ meltfnum[0];
      /*_.OUTPUT_C_CODE__V19*/ meltfptr[18] =
	meltgc_send ((melt_ptr_t) ( /*_.OOFF__V12*/ meltfptr[10]),
		     (melt_ptr_t) (( /*!OUTPUT_C_CODE */ meltfrout->
				    tabval[3])),
		     (MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_LONG ""),
		     argtab, "", (union meltparam_un *) 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:4307:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   ("] = (melt_ptr_t)("));
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:4308:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^msend */
    /*msend */
    {
      union meltparam_un argtab[3];
      memset (&argtab, 0, sizeof (argtab));
      /*^ojbmsend.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.DECLBUF__V3*/ meltfptr[2];
      /*^ojbmsend.arg */
      argtab[1].meltbp_aptr = (melt_ptr_t *) & /*_.IMPLBUF__V4*/ meltfptr[3];
      /*^ojbmsend.arg */
      argtab[2].meltbp_long = /*_#DEPTH__L1*/ meltfnum[0];
      /*_.OUTPUT_C_CODE__V20*/ meltfptr[19] =
	meltgc_send ((melt_ptr_t) ( /*_.OCVAL__V13*/ meltfptr[12]),
		     (melt_ptr_t) (( /*!OUTPUT_C_CODE */ meltfrout->
				    tabval[3])),
		     (MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_LONG ""),
		     argtab, "", (union meltparam_un *) 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:4309:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   (");"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:4310:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				( /*_#DEPTH__L1*/ meltfnum[0]), 0);
    }
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:4311:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#STRBUF_USEDLENGTH__L5*/ meltfnum[4] =
	melt_strbuf_usedlength ((melt_ptr_t)
				( /*_.IMPLBUF__V4*/ meltfptr[3]));;
      MELT_LOCATION ("warmelt-outobj.melt:4312:/ cond");
      /*cond */ if (
		     /*ifisa */
		     melt_is_instance_of ((melt_ptr_t)
					  (( /*!BUFFER_LIMIT_CONT */
					    meltfrout->tabval[4])),
					  (melt_ptr_t) (( /*!CLASS_REFERENCE */ meltfrout->tabval[5])))
	)			/*then */
	{
	  /*^cond.then */
	  /*^getslot */
	  {
	    melt_ptr_t slot = NULL, obj = NULL;
	    obj =
	      (melt_ptr_t) (( /*!BUFFER_LIMIT_CONT */ meltfrout->
			     tabval[4])) /*=obj*/ ;
	    melt_object_get_field (slot, obj, 0, "REFERENCED_VALUE");
     /*_.REFERENCED_VALUE__V22*/ meltfptr[21] = slot;
	  };
	  ;
	}
      else
	{			/*^cond.else */

    /*_.REFERENCED_VALUE__V22*/ meltfptr[21] = NULL;;
	}
      ;
      /*^compute */
   /*_#GET_INT__L6*/ meltfnum[5] =
	(melt_get_int
	 ((melt_ptr_t) ( /*_.REFERENCED_VALUE__V22*/ meltfptr[21])));;
      /*^compute */
   /*_#I__L7*/ meltfnum[6] =
	(( /*_#STRBUF_USEDLENGTH__L5*/ meltfnum[4]) <
	 ( /*_#GET_INT__L6*/ meltfnum[5]));;
      MELT_LOCATION ("warmelt-outobj.melt:4311:/ cond");
      /*cond */ if ( /*_#I__L7*/ meltfnum[6])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V23*/ meltfptr[22] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-outobj.melt:4311:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check limited implbuf"),
				  ("warmelt-outobj.melt")
				  ? ("warmelt-outobj.melt") : __FILE__,
				  (4311) ? (4311) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V23*/ meltfptr[22] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V21*/ meltfptr[20] = /*_.IFELSE___V23*/ meltfptr[22];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:4311:/ clear");
	     /*clear *//*_#STRBUF_USEDLENGTH__L5*/ meltfnum[4] = 0;
      /*^clear */
	     /*clear *//*_.REFERENCED_VALUE__V22*/ meltfptr[21] = 0;
      /*^clear */
	     /*clear *//*_#GET_INT__L6*/ meltfnum[5] = 0;
      /*^clear */
	     /*clear *//*_#I__L7*/ meltfnum[6] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V23*/ meltfptr[22] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V21*/ meltfptr[20] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    /*^compute */
    /*_.LET___V7*/ meltfptr[5] = /*_.IFCPP___V21*/ meltfptr[20];;

    MELT_LOCATION ("warmelt-outobj.melt:4281:/ clear");
	   /*clear *//*_.OLOC__V8*/ meltfptr[7] = 0;
    /*^clear */
	   /*clear *//*_.OROUT__V9*/ meltfptr[8] = 0;
    /*^clear */
	   /*clear *//*_#IS_A__L3*/ meltfnum[1] = 0;
    /*^clear */
	   /*clear *//*_.OROUTNAM__V10*/ meltfptr[9] = 0;
    /*^clear */
	   /*clear *//*_.OOFF__V12*/ meltfptr[10] = 0;
    /*^clear */
	   /*clear *//*_.OCVAL__V13*/ meltfptr[12] = 0;
    /*^clear */
	   /*clear *//*_.OUTPUT_LOCATION__V14*/ meltfptr[13] = 0;
    /*^clear */
	   /*clear *//*_.OUTPUT_C_CODE__V15*/ meltfptr[14] = 0;
    /*^clear */
	   /*clear *//*_#IS_STRING__L4*/ meltfnum[3] = 0;
    /*^clear */
	   /*clear *//*_.OUTPUT_C_CODE__V16*/ meltfptr[15] = 0;
    /*^clear */
	   /*clear *//*_.OUTPUT_C_CODE__V17*/ meltfptr[16] = 0;
    /*^clear */
	   /*clear *//*_.OUTPUT_C_CODE__V18*/ meltfptr[17] = 0;
    /*^clear */
	   /*clear *//*_.OUTPUT_C_CODE__V19*/ meltfptr[18] = 0;
    /*^clear */
	   /*clear *//*_.OUTPUT_C_CODE__V20*/ meltfptr[19] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V21*/ meltfptr[20] = 0;
    MELT_LOCATION ("warmelt-outobj.melt:4279:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.LET___V7*/ meltfptr[5];;

    {
      MELT_LOCATION ("warmelt-outobj.melt:4279:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V5*/ meltfptr[4] = 0;
    /*^clear */
	   /*clear *//*_.LET___V7*/ meltfptr[5] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("OUTPUCOD_OBJPUTROUTCONST", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_111_warmelt_outobj_OUTPUCOD_OBJPUTROUTCONST_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_111_warmelt_outobj_OUTPUCOD_OBJPUTROUTCONST */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_112_warmelt_outobj_OUTPUCOD_OBJPUTROUTCONSTNOTNULL (meltclosure_ptr_t
							     meltclosp_,
							     melt_ptr_t
							     meltfirstargp_,
							     const
							     melt_argdescr_cell_t
							     meltxargdescr_[],
							     union
							     meltparam_un *
							     meltxargtab_,
							     const
							     melt_argdescr_cell_t
							     meltxresdescr_[],
							     union
							     meltparam_un *
							     meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_112_warmelt_outobj_OUTPUCOD_OBJPUTROUTCONSTNOTNULL_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_112_warmelt_outobj_OUTPUCOD_OBJPUTROUTCONSTNOTNULL_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 22
    melt_ptr_t mcfr_varptr[22];
#define MELTFRAM_NBVARNUM 6
    long mcfr_varnum[6];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_112_warmelt_outobj_OUTPUCOD_OBJPUTROUTCONSTNOTNULL is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct
	 frame_meltrout_112_warmelt_outobj_OUTPUCOD_OBJPUTROUTCONSTNOTNULL_st
	 *) meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 22; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_112_warmelt_outobj_OUTPUCOD_OBJPUTROUTCONSTNOTNULL nbval 22*/
  meltfram__.mcfr_nbvar = 22 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("OUTPUCOD_OBJPUTROUTCONSTNOTNULL", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-outobj.melt:4318:/ getarg");
 /*_.OPRCONST__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.DECLBUF__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.DECLBUF__V3*/ meltfptr[2])) !=
	      NULL);


  /*getarg#2 */
  /*^getarg */
  if (meltxargdescr_[1] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.IMPLBUF__V4*/ meltfptr[3] =
    (meltxargtab_[1].meltbp_aptr) ? (*(meltxargtab_[1].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3])) !=
	      NULL);


  /*getarg#3 */
  /*^getarg */
  if (meltxargdescr_[2] != MELTBPAR_LONG)
    goto lab_endgetargs;
 /*_#DEPTH__L1*/ meltfnum[0] = meltxargtab_[2].meltbp_long;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:4319:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L2*/ meltfnum[1] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.OPRCONST__V2*/ meltfptr[1]),
			     (melt_ptr_t) (( /*!CLASS_OBJPUTROUTCONSTNOTNULL */ meltfrout->tabval[0])));;
      MELT_LOCATION ("warmelt-outobj.melt:4319:/ cond");
      /*cond */ if ( /*_#IS_A__L2*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V6*/ meltfptr[5] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-outobj.melt:4319:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check oprconst"),
				  ("warmelt-outobj.melt")
				  ? ("warmelt-outobj.melt") : __FILE__,
				  (4319) ? (4319) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V6*/ meltfptr[5] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V5*/ meltfptr[4] = /*_.IFELSE___V6*/ meltfptr[5];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:4319:/ clear");
	     /*clear *//*_#IS_A__L2*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V6*/ meltfptr[5] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V5*/ meltfptr[4] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-outobj.melt:4320:/ quasiblock");


    /*^getslot */
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.OPRCONST__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 0, "OBI_LOC");
  /*_.OLOC__V8*/ meltfptr[7] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-outobj.melt:4321:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.OPRCONST__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 1, "OPRCONST_ROUT");
  /*_.OROUT__V9*/ meltfptr[8] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-outobj.melt:4322:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.OPRCONST__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 2, "OPRCONST_OFF");
  /*_.OOFF__V10*/ meltfptr[9] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-outobj.melt:4323:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.OPRCONST__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 3, "OPRCONST_CVAL");
  /*_.OCVAL__V11*/ meltfptr[10] = slot;
    };
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:4324:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#NOTNULL__L3*/ meltfnum[1] =
	(( /*_.OCVAL__V11*/ meltfptr[10]) != NULL);;
      MELT_LOCATION ("warmelt-outobj.melt:4324:/ cond");
      /*cond */ if ( /*_#NOTNULL__L3*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V13*/ meltfptr[12] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-outobj.melt:4324:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check notnull ocval"),
				  ("warmelt-outobj.melt")
				  ? ("warmelt-outobj.melt") : __FILE__,
				  (4324) ? (4324) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V13*/ meltfptr[12] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V12*/ meltfptr[11] = /*_.IFELSE___V13*/ meltfptr[12];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:4324:/ clear");
	     /*clear *//*_#NOTNULL__L3*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V13*/ meltfptr[12] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V12*/ meltfptr[11] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-outobj.melt:4325:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[3];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.IMPLBUF__V4*/ meltfptr[3];
      /*^apply.arg */
      argtab[1].meltbp_long = /*_#DEPTH__L1*/ meltfnum[0];
      /*^apply.arg */
      argtab[2].meltbp_cstring = "putroutconstnotnull";
      /*_.OUTPUT_LOCATION__V14*/ meltfptr[12] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!OUTPUT_LOCATION */ meltfrout->tabval[1])),
		    (melt_ptr_t) ( /*_.OLOC__V8*/ meltfptr[7]),
		    (MELTBPARSTR_PTR MELTBPARSTR_LONG MELTBPARSTR_CSTRING ""),
		    argtab, "", (union meltparam_un *) 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:4326:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   ("/*putroutconstnotnull*/"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:4327:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				( /*_#DEPTH__L1*/ meltfnum[0]), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:4328:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   ("melt_assertmsg(\"putroutconstnotnull checkrout\", melt_magic_discr\
((melt_ptr_t)("));
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:4329:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^msend */
    /*msend */
    {
      union meltparam_un argtab[3];
      memset (&argtab, 0, sizeof (argtab));
      /*^ojbmsend.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.DECLBUF__V3*/ meltfptr[2];
      /*^ojbmsend.arg */
      argtab[1].meltbp_aptr = (melt_ptr_t *) & /*_.IMPLBUF__V4*/ meltfptr[3];
      /*^ojbmsend.arg */
      argtab[2].meltbp_long = /*_#DEPTH__L1*/ meltfnum[0];
      /*_.OUTPUT_C_CODE__V15*/ meltfptr[14] =
	meltgc_send ((melt_ptr_t) ( /*_.OROUT__V9*/ meltfptr[8]),
		     (melt_ptr_t) (( /*!OUTPUT_C_CODE */ meltfrout->
				    tabval[2])),
		     (MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_LONG ""),
		     argtab, "", (union meltparam_un *) 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:4330:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   (")) == MELTOBMAG_ROUTINE);"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:4331:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				( /*_#DEPTH__L1*/ meltfnum[0]), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:4332:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   ("melt_assertmsg(\"putroutconstnotnull notnullconst\", NULL != "));
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:4333:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^msend */
    /*msend */
    {
      union meltparam_un argtab[3];
      memset (&argtab, 0, sizeof (argtab));
      /*^ojbmsend.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.DECLBUF__V3*/ meltfptr[2];
      /*^ojbmsend.arg */
      argtab[1].meltbp_aptr = (melt_ptr_t *) & /*_.IMPLBUF__V4*/ meltfptr[3];
      /*^ojbmsend.arg */
      argtab[2].meltbp_long = /*_#DEPTH__L1*/ meltfnum[0];
      /*_.OUTPUT_C_CODE__V16*/ meltfptr[15] =
	meltgc_send ((melt_ptr_t) ( /*_.OCVAL__V11*/ meltfptr[10]),
		     (melt_ptr_t) (( /*!OUTPUT_C_CODE */ meltfrout->
				    tabval[2])),
		     (MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_LONG ""),
		     argtab, "", (union meltparam_un *) 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:4334:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   (");"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:4335:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				( /*_#DEPTH__L1*/ meltfnum[0]), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:4336:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   ("((meltroutine_ptr_t)"));
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:4337:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^msend */
    /*msend */
    {
      union meltparam_un argtab[3];
      memset (&argtab, 0, sizeof (argtab));
      /*^ojbmsend.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.DECLBUF__V3*/ meltfptr[2];
      /*^ojbmsend.arg */
      argtab[1].meltbp_aptr = (melt_ptr_t *) & /*_.IMPLBUF__V4*/ meltfptr[3];
      /*^ojbmsend.arg */
      argtab[2].meltbp_long = /*_#DEPTH__L1*/ meltfnum[0];
      /*_.OUTPUT_C_CODE__V17*/ meltfptr[16] =
	meltgc_send ((melt_ptr_t) ( /*_.OROUT__V9*/ meltfptr[8]),
		     (melt_ptr_t) (( /*!OUTPUT_C_CODE */ meltfrout->
				    tabval[2])),
		     (MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_LONG ""),
		     argtab, "", (union meltparam_un *) 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:4338:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   (")->tabval["));
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:4339:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^msend */
    /*msend */
    {
      union meltparam_un argtab[3];
      memset (&argtab, 0, sizeof (argtab));
      /*^ojbmsend.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.DECLBUF__V3*/ meltfptr[2];
      /*^ojbmsend.arg */
      argtab[1].meltbp_aptr = (melt_ptr_t *) & /*_.IMPLBUF__V4*/ meltfptr[3];
      /*^ojbmsend.arg */
      argtab[2].meltbp_long = /*_#DEPTH__L1*/ meltfnum[0];
      /*_.OUTPUT_C_CODE__V18*/ meltfptr[17] =
	meltgc_send ((melt_ptr_t) ( /*_.OOFF__V10*/ meltfptr[9]),
		     (melt_ptr_t) (( /*!OUTPUT_C_CODE */ meltfrout->
				    tabval[2])),
		     (MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_LONG ""),
		     argtab, "", (union meltparam_un *) 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:4340:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   ("] = (melt_ptr_t)("));
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:4341:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^msend */
    /*msend */
    {
      union meltparam_un argtab[3];
      memset (&argtab, 0, sizeof (argtab));
      /*^ojbmsend.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.DECLBUF__V3*/ meltfptr[2];
      /*^ojbmsend.arg */
      argtab[1].meltbp_aptr = (melt_ptr_t *) & /*_.IMPLBUF__V4*/ meltfptr[3];
      /*^ojbmsend.arg */
      argtab[2].meltbp_long = /*_#DEPTH__L1*/ meltfnum[0];
      /*_.OUTPUT_C_CODE__V19*/ meltfptr[18] =
	meltgc_send ((melt_ptr_t) ( /*_.OCVAL__V11*/ meltfptr[10]),
		     (melt_ptr_t) (( /*!OUTPUT_C_CODE */ meltfrout->
				    tabval[2])),
		     (MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_LONG ""),
		     argtab, "", (union meltparam_un *) 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:4342:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   (");"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:4343:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				( /*_#DEPTH__L1*/ meltfnum[0]), 0);
    }
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:4344:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#STRBUF_USEDLENGTH__L4*/ meltfnum[1] =
	melt_strbuf_usedlength ((melt_ptr_t)
				( /*_.IMPLBUF__V4*/ meltfptr[3]));;
      MELT_LOCATION ("warmelt-outobj.melt:4345:/ cond");
      /*cond */ if (
		     /*ifisa */
		     melt_is_instance_of ((melt_ptr_t)
					  (( /*!BUFFER_LIMIT_CONT */
					    meltfrout->tabval[3])),
					  (melt_ptr_t) (( /*!CLASS_REFERENCE */ meltfrout->tabval[4])))
	)			/*then */
	{
	  /*^cond.then */
	  /*^getslot */
	  {
	    melt_ptr_t slot = NULL, obj = NULL;
	    obj =
	      (melt_ptr_t) (( /*!BUFFER_LIMIT_CONT */ meltfrout->
			     tabval[3])) /*=obj*/ ;
	    melt_object_get_field (slot, obj, 0, "REFERENCED_VALUE");
     /*_.REFERENCED_VALUE__V21*/ meltfptr[20] = slot;
	  };
	  ;
	}
      else
	{			/*^cond.else */

    /*_.REFERENCED_VALUE__V21*/ meltfptr[20] = NULL;;
	}
      ;
      /*^compute */
   /*_#GET_INT__L5*/ meltfnum[4] =
	(melt_get_int
	 ((melt_ptr_t) ( /*_.REFERENCED_VALUE__V21*/ meltfptr[20])));;
      /*^compute */
   /*_#I__L6*/ meltfnum[5] =
	(( /*_#STRBUF_USEDLENGTH__L4*/ meltfnum[1]) <
	 ( /*_#GET_INT__L5*/ meltfnum[4]));;
      MELT_LOCATION ("warmelt-outobj.melt:4344:/ cond");
      /*cond */ if ( /*_#I__L6*/ meltfnum[5])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V22*/ meltfptr[21] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-outobj.melt:4344:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check limited implbuf"),
				  ("warmelt-outobj.melt")
				  ? ("warmelt-outobj.melt") : __FILE__,
				  (4344) ? (4344) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V22*/ meltfptr[21] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V20*/ meltfptr[19] = /*_.IFELSE___V22*/ meltfptr[21];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:4344:/ clear");
	     /*clear *//*_#STRBUF_USEDLENGTH__L4*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.REFERENCED_VALUE__V21*/ meltfptr[20] = 0;
      /*^clear */
	     /*clear *//*_#GET_INT__L5*/ meltfnum[4] = 0;
      /*^clear */
	     /*clear *//*_#I__L6*/ meltfnum[5] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V22*/ meltfptr[21] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V20*/ meltfptr[19] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    /*^compute */
    /*_.LET___V7*/ meltfptr[5] = /*_.IFCPP___V20*/ meltfptr[19];;

    MELT_LOCATION ("warmelt-outobj.melt:4320:/ clear");
	   /*clear *//*_.OLOC__V8*/ meltfptr[7] = 0;
    /*^clear */
	   /*clear *//*_.OROUT__V9*/ meltfptr[8] = 0;
    /*^clear */
	   /*clear *//*_.OOFF__V10*/ meltfptr[9] = 0;
    /*^clear */
	   /*clear *//*_.OCVAL__V11*/ meltfptr[10] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V12*/ meltfptr[11] = 0;
    /*^clear */
	   /*clear *//*_.OUTPUT_LOCATION__V14*/ meltfptr[12] = 0;
    /*^clear */
	   /*clear *//*_.OUTPUT_C_CODE__V15*/ meltfptr[14] = 0;
    /*^clear */
	   /*clear *//*_.OUTPUT_C_CODE__V16*/ meltfptr[15] = 0;
    /*^clear */
	   /*clear *//*_.OUTPUT_C_CODE__V17*/ meltfptr[16] = 0;
    /*^clear */
	   /*clear *//*_.OUTPUT_C_CODE__V18*/ meltfptr[17] = 0;
    /*^clear */
	   /*clear *//*_.OUTPUT_C_CODE__V19*/ meltfptr[18] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V20*/ meltfptr[19] = 0;
    MELT_LOCATION ("warmelt-outobj.melt:4318:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.LET___V7*/ meltfptr[5];;

    {
      MELT_LOCATION ("warmelt-outobj.melt:4318:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V5*/ meltfptr[4] = 0;
    /*^clear */
	   /*clear *//*_.LET___V7*/ meltfptr[5] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("OUTPUCOD_OBJPUTROUTCONSTNOTNULL", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_112_warmelt_outobj_OUTPUCOD_OBJPUTROUTCONSTNOTNULL_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_112_warmelt_outobj_OUTPUCOD_OBJPUTROUTCONSTNOTNULL */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_113_warmelt_outobj_OUTPUCOD_OBJPUTXTRARESULT (meltclosure_ptr_t
						       meltclosp_,
						       melt_ptr_t
						       meltfirstargp_,
						       const
						       melt_argdescr_cell_t
						       meltxargdescr_[],
						       union meltparam_un *
						       meltxargtab_,
						       const
						       melt_argdescr_cell_t
						       meltxresdescr_[],
						       union meltparam_un *
						       meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_113_warmelt_outobj_OUTPUCOD_OBJPUTXTRARESULT_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_113_warmelt_outobj_OUTPUCOD_OBJPUTXTRARESULT_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 30
    melt_ptr_t mcfr_varptr[30];
#define MELTFRAM_NBVARNUM 14
    long mcfr_varnum[14];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_113_warmelt_outobj_OUTPUCOD_OBJPUTXTRARESULT is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_113_warmelt_outobj_OUTPUCOD_OBJPUTXTRARESULT_st
	 *) meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 30; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_113_warmelt_outobj_OUTPUCOD_OBJPUTXTRARESULT nbval 30*/
  meltfram__.mcfr_nbvar = 30 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("OUTPUCOD_OBJPUTXTRARESULT", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-outobj.melt:4353:/ getarg");
 /*_.OPUTX__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.DECLBUF__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.DECLBUF__V3*/ meltfptr[2])) !=
	      NULL);


  /*getarg#2 */
  /*^getarg */
  if (meltxargdescr_[1] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.IMPLBUF__V4*/ meltfptr[3] =
    (meltxargtab_[1].meltbp_aptr) ? (*(meltxargtab_[1].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3])) !=
	      NULL);


  /*getarg#3 */
  /*^getarg */
  if (meltxargdescr_[2] != MELTBPAR_LONG)
    goto lab_endgetargs;
 /*_#DEPTH__L1*/ meltfnum[0] = meltxargtab_[2].meltbp_long;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:4354:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L2*/ meltfnum[1] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.OPUTX__V2*/ meltfptr[1]),
			     (melt_ptr_t) (( /*!CLASS_OBJPUTXTRARESULT */
					    meltfrout->tabval[0])));;
      MELT_LOCATION ("warmelt-outobj.melt:4354:/ cond");
      /*cond */ if ( /*_#IS_A__L2*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V6*/ meltfptr[5] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-outobj.melt:4354:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check oputx"),
				  ("warmelt-outobj.melt")
				  ? ("warmelt-outobj.melt") : __FILE__,
				  (4354) ? (4354) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V6*/ meltfptr[5] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V5*/ meltfptr[4] = /*_.IFELSE___V6*/ meltfptr[5];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:4354:/ clear");
	     /*clear *//*_#IS_A__L2*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V6*/ meltfptr[5] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V5*/ meltfptr[4] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-outobj.melt:4355:/ quasiblock");


    /*^getslot */
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.OPUTX__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 0, "OBI_LOC");
  /*_.OLOC__V8*/ meltfptr[7] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-outobj.melt:4356:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.OPUTX__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 1, "OBXRES_RANK");
  /*_.ORANK__V9*/ meltfptr[8] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-outobj.melt:4357:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.OPUTX__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 2, "OBXRES_OBLOC");
  /*_.OVLOC__V10*/ meltfptr[9] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-outobj.melt:4358:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^msend */
    /*msend */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^ojbmsend.arg */
      argtab[0].meltbp_aptr = /*nil */ (melt_ptr_t *) NULL;
      /*_.OCTYP__V11*/ meltfptr[10] =
	meltgc_send ((melt_ptr_t) ( /*_.OVLOC__V10*/ meltfptr[9]),
		     (melt_ptr_t) (( /*!GET_CTYPE */ meltfrout->tabval[1])),
		     (MELTBPARSTR_PTR ""), argtab, "",
		     (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:4360:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[3];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.IMPLBUF__V4*/ meltfptr[3];
      /*^apply.arg */
      argtab[1].meltbp_long = /*_#DEPTH__L1*/ meltfnum[0];
      /*^apply.arg */
      argtab[2].meltbp_cstring = "putxtraresult";
      /*_.OUTPUT_LOCATION__V12*/ meltfptr[11] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!OUTPUT_LOCATION */ meltfrout->tabval[2])),
		    (melt_ptr_t) ( /*_.OLOC__V8*/ meltfptr[7]),
		    (MELTBPARSTR_PTR MELTBPARSTR_LONG MELTBPARSTR_CSTRING ""),
		    argtab, "", (union meltparam_un *) 0);
    }
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:4361:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L3*/ meltfnum[1] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.OCTYP__V11*/ meltfptr[10]),
			     (melt_ptr_t) (( /*!CLASS_CTYPE */ meltfrout->
					    tabval[3])));;
      MELT_LOCATION ("warmelt-outobj.melt:4361:/ cond");
      /*cond */ if ( /*_#IS_A__L3*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V14*/ meltfptr[13] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-outobj.melt:4361:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check octyp"),
				  ("warmelt-outobj.melt")
				  ? ("warmelt-outobj.melt") : __FILE__,
				  (4361) ? (4361) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V14*/ meltfptr[13] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V13*/ meltfptr[12] = /*_.IFELSE___V14*/ meltfptr[13];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:4361:/ clear");
	     /*clear *//*_#IS_A__L3*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V14*/ meltfptr[13] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V13*/ meltfptr[12] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:4362:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_INTEGERBOX__L4*/ meltfnum[1] =
	(melt_magic_discr ((melt_ptr_t) ( /*_.ORANK__V9*/ meltfptr[8])) ==
	 MELTOBMAG_INT);;
      MELT_LOCATION ("warmelt-outobj.melt:4362:/ cond");
      /*cond */ if ( /*_#IS_INTEGERBOX__L4*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V16*/ meltfptr[15] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-outobj.melt:4362:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check orank"),
				  ("warmelt-outobj.melt")
				  ? ("warmelt-outobj.melt") : __FILE__,
				  (4362) ? (4362) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V16*/ meltfptr[15] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V15*/ meltfptr[13] = /*_.IFELSE___V16*/ meltfptr[15];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:4362:/ clear");
	     /*clear *//*_#IS_INTEGERBOX__L4*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V16*/ meltfptr[15] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V15*/ meltfptr[13] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:4363:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   ("if (!meltxrestab_ || !meltxresdescr_) goto labend_rout;"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:4364:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				( /*_#DEPTH__L1*/ meltfnum[0]), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:4365:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   ("if (meltxresdescr_["));
    }
    ;
 /*_#GET_INT__L5*/ meltfnum[1] =
      (melt_get_int ((melt_ptr_t) ( /*_.ORANK__V9*/ meltfptr[8])));;

    {
      MELT_LOCATION ("warmelt-outobj.melt:4366:/ locexp");
      meltgc_add_strbuf_dec ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			     ( /*_#GET_INT__L5*/ meltfnum[1]));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:4367:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   ("] != "));
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:4368:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.OCTYP__V11*/ meltfptr[10]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 4, "CTYPE_PARCHAR");
  /*_.CTYPE_PARCHAR__V17*/ meltfptr[15] = slot;
    };
    ;

    {
      /*^locexp */
      /*add2sbuf_string */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   melt_string_str ((melt_ptr_t)
					    ( /*_.CTYPE_PARCHAR__V17*/
					     meltfptr[15])));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:4369:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   (") goto labend_rout;"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:4370:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				( /*_#DEPTH__L1*/ meltfnum[0]), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:4371:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   ("if (meltxrestab_["));
    }
    ;
 /*_#GET_INT__L6*/ meltfnum[5] =
      (melt_get_int ((melt_ptr_t) ( /*_.ORANK__V9*/ meltfptr[8])));;

    {
      MELT_LOCATION ("warmelt-outobj.melt:4372:/ locexp");
      meltgc_add_strbuf_dec ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			     ( /*_#GET_INT__L6*/ meltfnum[5]));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:4373:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   ("]."));
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:4374:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^cond */
    /*cond */ if (
		   /*ifisa */
		   melt_is_instance_of ((melt_ptr_t)
					( /*_.OCTYP__V11*/ meltfptr[10]),
					(melt_ptr_t) (( /*!CLASS_CTYPE */
						       meltfrout->tabval[3])))
      )				/*then */
      {
	/*^cond.then */
	/*^getslot */
	{
	  melt_ptr_t slot = NULL, obj = NULL;
	  obj = (melt_ptr_t) ( /*_.OCTYP__V11*/ meltfptr[10]) /*=obj*/ ;
	  melt_object_get_field (slot, obj, 7, "CTYPE_RESFIELD");
   /*_.CTYPE_RESFIELD__V18*/ meltfptr[17] = slot;
	};
	;
      }
    else
      {				/*^cond.else */

  /*_.CTYPE_RESFIELD__V18*/ meltfptr[17] = NULL;;
      }
    ;
    /*^compute */
 /*_#IS_STRING__L7*/ meltfnum[6] =
      (melt_magic_discr
       ((melt_ptr_t) ( /*_.CTYPE_RESFIELD__V18*/ meltfptr[17])) ==
       MELTOBMAG_STRING);;
    MELT_LOCATION ("warmelt-outobj.melt:4374:/ cond");
    /*cond */ if ( /*_#IS_STRING__L7*/ meltfnum[6])	/*then */
      {
	/*^cond.then */
	/*_.IFELSE___V19*/ meltfptr[18] = ( /*nil */ NULL);;
      }
    else
      {
	MELT_LOCATION ("warmelt-outobj.melt:4374:/ cond.else");

	/*^block */
	/*anyblock */
	{


#if MELT_HAVE_DEBUG
	  MELT_LOCATION ("warmelt-outobj.melt:4375:/ cppif.then");
	  /*^block */
	  /*anyblock */
	  {


	    {
	      /*^locexp */
	      /*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	      melt_dbgcounter++;
#endif
	      ;
	    }
	    ;
	    /*^checksignal */
	    MELT_CHECK_SIGNAL ();
	    ;
     /*_#MELT_NEED_DBG__L8*/ meltfnum[7] =
	      /*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	      ( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	      0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	      ;;
	    MELT_LOCATION ("warmelt-outobj.melt:4375:/ cond");
	    /*cond */ if ( /*_#MELT_NEED_DBG__L8*/ meltfnum[7])	/*then */
	      {
		/*^cond.then */
		/*^block */
		/*anyblock */
		{

       /*_#THE_MELTCALLCOUNT__L9*/ meltfnum[8] =
#ifdef meltcallcount
		    meltcallcount	/* the_meltcallcount */
#else
		    0L
#endif /* meltcallcount the_meltcallcount */
		    ;;
		  MELT_LOCATION ("warmelt-outobj.melt:4375:/ checksignal");
		  MELT_CHECK_SIGNAL ();
		  ;
		  /*^apply */
		  /*apply */
		  {
		    union meltparam_un argtab[7];
		    memset (&argtab, 0, sizeof (argtab));
		    /*^apply.arg */
		    argtab[0].meltbp_long =
		      /*_#THE_MELTCALLCOUNT__L9*/ meltfnum[8];
		    /*^apply.arg */
		    argtab[1].meltbp_cstring = "warmelt-outobj.melt";
		    /*^apply.arg */
		    argtab[2].meltbp_long = 4375;
		    /*^apply.arg */
		    argtab[3].meltbp_cstring =
		      "outpucod_objputxtraresult bad octyp";
		    /*^apply.arg */
		    argtab[4].meltbp_aptr =
		      (melt_ptr_t *) & /*_.OCTYP__V11*/ meltfptr[10];
		    /*^apply.arg */
		    argtab[5].meltbp_cstring = " oputx=";
		    /*^apply.arg */
		    argtab[6].meltbp_aptr =
		      (melt_ptr_t *) & /*_.OPUTX__V2*/ meltfptr[1];
		    /*_.MELT_DEBUG_FUN__V22*/ meltfptr[21] =
		      melt_apply ((meltclosure_ptr_t)
				  (( /*!MELT_DEBUG_FUN */ meltfrout->
				    tabval[4])),
				  (melt_ptr_t) (( /*nil */ NULL)),
				  (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_PTR MELTBPARSTR_CSTRING
				   MELTBPARSTR_PTR ""), argtab, "",
				  (union meltparam_un *) 0);
		  }
		  ;
		  /*_.IF___V21*/ meltfptr[20] =
		    /*_.MELT_DEBUG_FUN__V22*/ meltfptr[21];;
		  /*epilog */

		  MELT_LOCATION ("warmelt-outobj.melt:4375:/ clear");
		 /*clear *//*_#THE_MELTCALLCOUNT__L9*/ meltfnum[8] = 0;
		  /*^clear */
		 /*clear *//*_.MELT_DEBUG_FUN__V22*/ meltfptr[21] = 0;
		}
		;
	      }
	    else
	      {			/*^cond.else */

      /*_.IF___V21*/ meltfptr[20] = NULL;;
	      }
	    ;
	    MELT_LOCATION ("warmelt-outobj.melt:4375:/ quasiblock");


	    /*_.PROGN___V23*/ meltfptr[21] = /*_.IF___V21*/ meltfptr[20];;
	    /*^compute */
	    /*_.IFCPP___V20*/ meltfptr[19] = /*_.PROGN___V23*/ meltfptr[21];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-outobj.melt:4375:/ clear");
	       /*clear *//*_#MELT_NEED_DBG__L8*/ meltfnum[7] = 0;
	    /*^clear */
	       /*clear *//*_.IF___V21*/ meltfptr[20] = 0;
	    /*^clear */
	       /*clear *//*_.PROGN___V23*/ meltfptr[21] = 0;
	  }

#else /*MELT_HAVE_DEBUG */
	  /*^cppif.else */
	  /*_.IFCPP___V20*/ meltfptr[19] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
	  ;
	  MELT_LOCATION ("warmelt-outobj.melt:4376:/ cond");
	  /*cond */ if (
			 /*ifisa */
			 melt_is_instance_of ((melt_ptr_t)
					      ( /*_.OCTYP__V11*/
					       meltfptr[10]),
					      (melt_ptr_t) (( /*!CLASS_NAMED */ meltfrout->tabval[5])))
	    )			/*then */
	    {
	      /*^cond.then */
	      /*^getslot */
	      {
		melt_ptr_t slot = NULL, obj = NULL;
		obj = (melt_ptr_t) ( /*_.OCTYP__V11*/ meltfptr[10]) /*=obj*/ ;
		melt_object_get_field (slot, obj, 1, "NAMED_NAME");
     /*_.NAMED_NAME__V24*/ meltfptr[20] = slot;
	      };
	      ;
	    }
	  else
	    {			/*^cond.else */

    /*_.NAMED_NAME__V24*/ meltfptr[20] = NULL;;
	    }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:4376:/ locexp");
	    melt_error_str ((melt_ptr_t) ( /*_.OLOC__V8*/ meltfptr[7]),
			    ("impossible secondary result type"),
			    (melt_ptr_t) ( /*_.NAMED_NAME__V24*/
					  meltfptr[20]));
	  }
	  ;
	  MELT_LOCATION ("warmelt-outobj.melt:4374:/ quasiblock");


	  /*epilog */

	  /*^clear */
	     /*clear *//*_.IFCPP___V20*/ meltfptr[19] = 0;
	  /*^clear */
	     /*clear *//*_.NAMED_NAME__V24*/ meltfptr[20] = 0;
	}
	;
      }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:4378:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.OCTYP__V11*/ meltfptr[10]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 7, "CTYPE_RESFIELD");
  /*_.CTYPE_RESFIELD__V25*/ meltfptr[21] = slot;
    };
    ;

    {
      /*^locexp */
      /*add2sbuf_string */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   melt_string_str ((melt_ptr_t)
					    ( /*_.CTYPE_RESFIELD__V25*/
					     meltfptr[21])));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:4379:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   (") *(meltxrestab_["));
    }
    ;
 /*_#GET_INT__L10*/ meltfnum[8] =
      (melt_get_int ((melt_ptr_t) ( /*_.ORANK__V9*/ meltfptr[8])));;

    {
      MELT_LOCATION ("warmelt-outobj.melt:4380:/ locexp");
      meltgc_add_strbuf_dec ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			     ( /*_#GET_INT__L10*/ meltfnum[8]));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:4381:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   ("]."));
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:4382:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.OCTYP__V11*/ meltfptr[10]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 7, "CTYPE_RESFIELD");
  /*_.CTYPE_RESFIELD__V26*/ meltfptr[19] = slot;
    };
    ;

    {
      /*^locexp */
      /*add2sbuf_string */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   melt_string_str ((melt_ptr_t)
					    ( /*_.CTYPE_RESFIELD__V26*/
					     meltfptr[19])));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:4383:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   (") = ("));
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:4384:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#__L11*/ meltfnum[7] =
      (( /*_.OCTYP__V11*/ meltfptr[10]) ==
       (( /*!CTYPE_VALUE */ meltfrout->tabval[6])));;
    MELT_LOCATION ("warmelt-outobj.melt:4384:/ cond");
    /*cond */ if ( /*_#__L11*/ meltfnum[7])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{


	  {
	    /*^locexp */
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t)
				 ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				 ("melt_ptr_t) ("));
	  }
	  ;
	  /*epilog */
	}
	;
      }				/*noelse */
    ;
    MELT_LOCATION ("warmelt-outobj.melt:4385:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^msend */
    /*msend */
    {
      union meltparam_un argtab[3];
      memset (&argtab, 0, sizeof (argtab));
      /*^ojbmsend.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.DECLBUF__V3*/ meltfptr[2];
      /*^ojbmsend.arg */
      argtab[1].meltbp_aptr = (melt_ptr_t *) & /*_.IMPLBUF__V4*/ meltfptr[3];
      /*^ojbmsend.arg */
      argtab[2].meltbp_long = /*_#DEPTH__L1*/ meltfnum[0];
      /*_.OUTPUT_C_CODE__V27*/ meltfptr[20] =
	meltgc_send ((melt_ptr_t) ( /*_.OVLOC__V10*/ meltfptr[9]),
		     (melt_ptr_t) (( /*!OUTPUT_C_CODE */ meltfrout->
				    tabval[7])),
		     (MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_LONG ""),
		     argtab, "", (union meltparam_un *) 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:4386:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   (");"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:4387:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				( /*_#DEPTH__L1*/ meltfnum[0]), 0);
    }
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:4388:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#STRBUF_USEDLENGTH__L12*/ meltfnum[11] =
	melt_strbuf_usedlength ((melt_ptr_t)
				( /*_.IMPLBUF__V4*/ meltfptr[3]));;
      MELT_LOCATION ("warmelt-outobj.melt:4389:/ cond");
      /*cond */ if (
		     /*ifisa */
		     melt_is_instance_of ((melt_ptr_t)
					  (( /*!BUFFER_LIMIT_CONT */
					    meltfrout->tabval[8])),
					  (melt_ptr_t) (( /*!CLASS_REFERENCE */ meltfrout->tabval[9])))
	)			/*then */
	{
	  /*^cond.then */
	  /*^getslot */
	  {
	    melt_ptr_t slot = NULL, obj = NULL;
	    obj =
	      (melt_ptr_t) (( /*!BUFFER_LIMIT_CONT */ meltfrout->
			     tabval[8])) /*=obj*/ ;
	    melt_object_get_field (slot, obj, 0, "REFERENCED_VALUE");
     /*_.REFERENCED_VALUE__V29*/ meltfptr[28] = slot;
	  };
	  ;
	}
      else
	{			/*^cond.else */

    /*_.REFERENCED_VALUE__V29*/ meltfptr[28] = NULL;;
	}
      ;
      /*^compute */
   /*_#GET_INT__L13*/ meltfnum[12] =
	(melt_get_int
	 ((melt_ptr_t) ( /*_.REFERENCED_VALUE__V29*/ meltfptr[28])));;
      /*^compute */
   /*_#I__L14*/ meltfnum[13] =
	(( /*_#STRBUF_USEDLENGTH__L12*/ meltfnum[11]) <
	 ( /*_#GET_INT__L13*/ meltfnum[12]));;
      MELT_LOCATION ("warmelt-outobj.melt:4388:/ cond");
      /*cond */ if ( /*_#I__L14*/ meltfnum[13])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V30*/ meltfptr[29] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-outobj.melt:4388:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check limited implbuf"),
				  ("warmelt-outobj.melt")
				  ? ("warmelt-outobj.melt") : __FILE__,
				  (4388) ? (4388) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V30*/ meltfptr[29] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V28*/ meltfptr[27] = /*_.IFELSE___V30*/ meltfptr[29];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:4388:/ clear");
	     /*clear *//*_#STRBUF_USEDLENGTH__L12*/ meltfnum[11] = 0;
      /*^clear */
	     /*clear *//*_.REFERENCED_VALUE__V29*/ meltfptr[28] = 0;
      /*^clear */
	     /*clear *//*_#GET_INT__L13*/ meltfnum[12] = 0;
      /*^clear */
	     /*clear *//*_#I__L14*/ meltfnum[13] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V30*/ meltfptr[29] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V28*/ meltfptr[27] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    /*^compute */
    /*_.LET___V7*/ meltfptr[5] = /*_.IFCPP___V28*/ meltfptr[27];;

    MELT_LOCATION ("warmelt-outobj.melt:4355:/ clear");
	   /*clear *//*_.OLOC__V8*/ meltfptr[7] = 0;
    /*^clear */
	   /*clear *//*_.ORANK__V9*/ meltfptr[8] = 0;
    /*^clear */
	   /*clear *//*_.OVLOC__V10*/ meltfptr[9] = 0;
    /*^clear */
	   /*clear *//*_.OCTYP__V11*/ meltfptr[10] = 0;
    /*^clear */
	   /*clear *//*_.OUTPUT_LOCATION__V12*/ meltfptr[11] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V13*/ meltfptr[12] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V15*/ meltfptr[13] = 0;
    /*^clear */
	   /*clear *//*_#GET_INT__L5*/ meltfnum[1] = 0;
    /*^clear */
	   /*clear *//*_.CTYPE_PARCHAR__V17*/ meltfptr[15] = 0;
    /*^clear */
	   /*clear *//*_#GET_INT__L6*/ meltfnum[5] = 0;
    /*^clear */
	   /*clear *//*_.CTYPE_RESFIELD__V18*/ meltfptr[17] = 0;
    /*^clear */
	   /*clear *//*_#IS_STRING__L7*/ meltfnum[6] = 0;
    /*^clear */
	   /*clear *//*_.IFELSE___V19*/ meltfptr[18] = 0;
    /*^clear */
	   /*clear *//*_.CTYPE_RESFIELD__V25*/ meltfptr[21] = 0;
    /*^clear */
	   /*clear *//*_#GET_INT__L10*/ meltfnum[8] = 0;
    /*^clear */
	   /*clear *//*_.CTYPE_RESFIELD__V26*/ meltfptr[19] = 0;
    /*^clear */
	   /*clear *//*_#__L11*/ meltfnum[7] = 0;
    /*^clear */
	   /*clear *//*_.OUTPUT_C_CODE__V27*/ meltfptr[20] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V28*/ meltfptr[27] = 0;
    MELT_LOCATION ("warmelt-outobj.melt:4353:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.LET___V7*/ meltfptr[5];;

    {
      MELT_LOCATION ("warmelt-outobj.melt:4353:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V5*/ meltfptr[4] = 0;
    /*^clear */
	   /*clear *//*_.LET___V7*/ meltfptr[5] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("OUTPUCOD_OBJPUTXTRARESULT", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_113_warmelt_outobj_OUTPUCOD_OBJPUTXTRARESULT_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_113_warmelt_outobj_OUTPUCOD_OBJPUTXTRARESULT */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_114_warmelt_outobj_OUTPUCOD_OBJEXPV (meltclosure_ptr_t meltclosp_,
					      melt_ptr_t meltfirstargp_,
					      const melt_argdescr_cell_t
					      meltxargdescr_[],
					      union meltparam_un *
					      meltxargtab_,
					      const melt_argdescr_cell_t
					      meltxresdescr_[],
					      union meltparam_un *
					      meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_114_warmelt_outobj_OUTPUCOD_OBJEXPV_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_114_warmelt_outobj_OUTPUCOD_OBJEXPV_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 15
    melt_ptr_t mcfr_varptr[15];
#define MELTFRAM_NBVARNUM 9
    long mcfr_varnum[9];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_114_warmelt_outobj_OUTPUCOD_OBJEXPV is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_114_warmelt_outobj_OUTPUCOD_OBJEXPV_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 15; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_114_warmelt_outobj_OUTPUCOD_OBJEXPV nbval 15*/
  meltfram__.mcfr_nbvar = 15 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("OUTPUCOD_OBJEXPV", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-outobj.melt:4394:/ getarg");
 /*_.OEXP__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.DECLBUF__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.DECLBUF__V3*/ meltfptr[2])) !=
	      NULL);


  /*getarg#2 */
  /*^getarg */
  if (meltxargdescr_[1] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.IMPLBUF__V4*/ meltfptr[3] =
    (meltxargtab_[1].meltbp_aptr) ? (*(meltxargtab_[1].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3])) !=
	      NULL);


  /*getarg#3 */
  /*^getarg */
  if (meltxargdescr_[2] != MELTBPAR_LONG)
    goto lab_endgetargs;
 /*_#DEPTH__L1*/ meltfnum[0] = meltxargtab_[2].meltbp_long;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:4395:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L2*/ meltfnum[1] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.OEXP__V2*/ meltfptr[1]),
			     (melt_ptr_t) (( /*!CLASS_OBJEXPV */ meltfrout->
					    tabval[0])));;
      MELT_LOCATION ("warmelt-outobj.melt:4395:/ cond");
      /*cond */ if ( /*_#IS_A__L2*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V6*/ meltfptr[5] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-outobj.melt:4395:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check oexp"),
				  ("warmelt-outobj.melt")
				  ? ("warmelt-outobj.melt") : __FILE__,
				  (4395) ? (4395) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V6*/ meltfptr[5] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V5*/ meltfptr[4] = /*_.IFELSE___V6*/ meltfptr[5];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:4395:/ clear");
	     /*clear *//*_#IS_A__L2*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V6*/ meltfptr[5] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V5*/ meltfptr[4] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-outobj.melt:4396:/ quasiblock");


    /*^getslot */
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.OEXP__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 1, "OBX_CONT");
  /*_.CONT__V7*/ meltfptr[5] = slot;
    };
    ;
 /*_#I__L3*/ meltfnum[1] =
      (( /*_#DEPTH__L1*/ meltfnum[0]) + (1));;
    /*^compute */
 /*_.BOXDEPTHP1__V8*/ meltfptr[7] =
      (meltgc_new_int
       ((meltobject_ptr_t) (( /*!DISCR_INTEGER */ meltfrout->tabval[1])),
	( /*_#I__L3*/ meltfnum[1])));;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:4399:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_MULTIPLE__L4*/ meltfnum[3] =
	(melt_magic_discr ((melt_ptr_t) ( /*_.CONT__V7*/ meltfptr[5])) ==
	 MELTOBMAG_MULTIPLE);;
      MELT_LOCATION ("warmelt-outobj.melt:4399:/ cond");
      /*cond */ if ( /*_#IS_MULTIPLE__L4*/ meltfnum[3])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V10*/ meltfptr[9] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-outobj.melt:4399:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check cont"),
				  ("warmelt-outobj.melt")
				  ? ("warmelt-outobj.melt") : __FILE__,
				  (4399) ? (4399) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V10*/ meltfptr[9] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V9*/ meltfptr[8] = /*_.IFELSE___V10*/ meltfptr[9];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:4399:/ clear");
	     /*clear *//*_#IS_MULTIPLE__L4*/ meltfnum[3] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V10*/ meltfptr[9] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V9*/ meltfptr[8] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    /*citerblock FOREACH_IN_MULTIPLE */
    {
      /* start foreach_in_multiple meltcit1__EACHTUP */
      long meltcit1__EACHTUP_ln =
	melt_multiple_length ((melt_ptr_t) /*_.CONT__V7*/ meltfptr[5]);
      for ( /*_#IX__L5*/ meltfnum[3] = 0;
	   ( /*_#IX__L5*/ meltfnum[3] >= 0)
	   && ( /*_#IX__L5*/ meltfnum[3] < meltcit1__EACHTUP_ln);
	/*_#IX__L5*/ meltfnum[3]++)
	{
	  /*_.COMP__V11*/ meltfptr[9] =
	    melt_multiple_nth ((melt_ptr_t) ( /*_.CONT__V7*/ meltfptr[5]),
			       /*_#IX__L5*/ meltfnum[3]);



  /*_#GET_INT__L6*/ meltfnum[5] =
	    (melt_get_int
	     ((melt_ptr_t) ( /*_.BOXDEPTHP1__V8*/ meltfptr[7])));;
	  MELT_LOCATION ("warmelt-outobj.melt:4403:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^msend */
	  /*msend */
	  {
	    union meltparam_un argtab[3];
	    memset (&argtab, 0, sizeof (argtab));
	    /*^ojbmsend.arg */
	    argtab[0].meltbp_aptr =
	      (melt_ptr_t *) & /*_.DECLBUF__V3*/ meltfptr[2];
	    /*^ojbmsend.arg */
	    argtab[1].meltbp_aptr =
	      (melt_ptr_t *) & /*_.IMPLBUF__V4*/ meltfptr[3];
	    /*^ojbmsend.arg */
	    argtab[2].meltbp_long = /*_#GET_INT__L6*/ meltfnum[5];
	    /*_.OUTPUT_C_CODE__V12*/ meltfptr[11] =
	      meltgc_send ((melt_ptr_t) ( /*_.COMP__V11*/ meltfptr[9]),
			   (melt_ptr_t) (( /*!OUTPUT_C_CODE */ meltfrout->
					  tabval[2])),
			   (MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_LONG
			    ""), argtab, "", (union meltparam_un *) 0);
	  }
	  ;

#if MELT_HAVE_DEBUG
	  MELT_LOCATION ("warmelt-outobj.melt:4404:/ cppif.then");
	  /*^block */
	  /*anyblock */
	  {

	    /*^checksignal */
	    MELT_CHECK_SIGNAL ();
	    ;
    /*_#STRBUF_USEDLENGTH__L7*/ meltfnum[6] =
	      melt_strbuf_usedlength ((melt_ptr_t)
				      ( /*_.IMPLBUF__V4*/ meltfptr[3]));;
	    MELT_LOCATION ("warmelt-outobj.melt:4405:/ cond");
	    /*cond */ if (
			   /*ifisa */
			   melt_is_instance_of ((melt_ptr_t)
						(( /*!BUFFER_LIMIT_CONT */
						  meltfrout->tabval[3])),
						(melt_ptr_t) (( /*!CLASS_REFERENCE */ meltfrout->tabval[4])))
	      )			/*then */
	      {
		/*^cond.then */
		/*^getslot */
		{
		  melt_ptr_t slot = NULL, obj = NULL;
		  obj =
		    (melt_ptr_t) (( /*!BUFFER_LIMIT_CONT */ meltfrout->
				   tabval[3])) /*=obj*/ ;
		  melt_object_get_field (slot, obj, 0, "REFERENCED_VALUE");
      /*_.REFERENCED_VALUE__V14*/ meltfptr[13] = slot;
		};
		;
	      }
	    else
	      {			/*^cond.else */

     /*_.REFERENCED_VALUE__V14*/ meltfptr[13] = NULL;;
	      }
	    ;
	    /*^compute */
    /*_#GET_INT__L8*/ meltfnum[7] =
	      (melt_get_int
	       ((melt_ptr_t) ( /*_.REFERENCED_VALUE__V14*/ meltfptr[13])));;
	    /*^compute */
    /*_#I__L9*/ meltfnum[8] =
	      (( /*_#STRBUF_USEDLENGTH__L7*/ meltfnum[6]) <
	       ( /*_#GET_INT__L8*/ meltfnum[7]));;
	    MELT_LOCATION ("warmelt-outobj.melt:4404:/ cond");
	    /*cond */ if ( /*_#I__L9*/ meltfnum[8])	/*then */
	      {
		/*^cond.then */
		/*_.IFELSE___V15*/ meltfptr[14] = ( /*nil */ NULL);;
	      }
	    else
	      {
		MELT_LOCATION ("warmelt-outobj.melt:4404:/ cond.else");

		/*^block */
		/*anyblock */
		{




		  {
		    /*^locexp */
		    melt_assert_failed (("check limited implbuf"),
					("warmelt-outobj.melt")
					? ("warmelt-outobj.melt") : __FILE__,
					(4404) ? (4404) : __LINE__,
					__FUNCTION__);
		    ;
		  }
		  ;
		/*clear *//*_.IFELSE___V15*/ meltfptr[14] = 0;
		  /*epilog */
		}
		;
	      }
	    ;
	    /*^compute */
	    /*_.IFCPP___V13*/ meltfptr[12] = /*_.IFELSE___V15*/ meltfptr[14];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-outobj.melt:4404:/ clear");
	      /*clear *//*_#STRBUF_USEDLENGTH__L7*/ meltfnum[6] = 0;
	    /*^clear */
	      /*clear *//*_.REFERENCED_VALUE__V14*/ meltfptr[13] = 0;
	    /*^clear */
	      /*clear *//*_#GET_INT__L8*/ meltfnum[7] = 0;
	    /*^clear */
	      /*clear *//*_#I__L9*/ meltfnum[8] = 0;
	    /*^clear */
	      /*clear *//*_.IFELSE___V15*/ meltfptr[14] = 0;
	  }

#else /*MELT_HAVE_DEBUG */
	  /*^cppif.else */
	  /*_.IFCPP___V13*/ meltfptr[12] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
	  ;
	  if ( /*_#IX__L5*/ meltfnum[3] < 0)
	    break;
	}			/* end  foreach_in_multiple meltcit1__EACHTUP */

      /*citerepilog */

      MELT_LOCATION ("warmelt-outobj.melt:4400:/ clear");
	    /*clear *//*_.COMP__V11*/ meltfptr[9] = 0;
      /*^clear */
	    /*clear *//*_#IX__L5*/ meltfnum[3] = 0;
      /*^clear */
	    /*clear *//*_#GET_INT__L6*/ meltfnum[5] = 0;
      /*^clear */
	    /*clear *//*_.OUTPUT_C_CODE__V12*/ meltfptr[11] = 0;
      /*^clear */
	    /*clear *//*_.IFCPP___V13*/ meltfptr[12] = 0;
    }				/*endciterblock FOREACH_IN_MULTIPLE */
    ;

    MELT_LOCATION ("warmelt-outobj.melt:4396:/ clear");
	   /*clear *//*_.CONT__V7*/ meltfptr[5] = 0;
    /*^clear */
	   /*clear *//*_#I__L3*/ meltfnum[1] = 0;
    /*^clear */
	   /*clear *//*_.BOXDEPTHP1__V8*/ meltfptr[7] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V9*/ meltfptr[8] = 0;
    MELT_LOCATION ("warmelt-outobj.melt:4394:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V5*/ meltfptr[4] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("OUTPUCOD_OBJEXPV", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_114_warmelt_outobj_OUTPUCOD_OBJEXPV_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*noretval */ NULL);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_114_warmelt_outobj_OUTPUCOD_OBJEXPV */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_115_warmelt_outobj_OUTPUCOD_OBJLOCATEDEXPV (meltclosure_ptr_t
						     meltclosp_,
						     melt_ptr_t
						     meltfirstargp_,
						     const
						     melt_argdescr_cell_t
						     meltxargdescr_[],
						     union meltparam_un *
						     meltxargtab_,
						     const
						     melt_argdescr_cell_t
						     meltxresdescr_[],
						     union meltparam_un *
						     meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_115_warmelt_outobj_OUTPUCOD_OBJLOCATEDEXPV_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_115_warmelt_outobj_OUTPUCOD_OBJLOCATEDEXPV_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 25
    melt_ptr_t mcfr_varptr[25];
#define MELTFRAM_NBVARNUM 14
    long mcfr_varnum[14];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_115_warmelt_outobj_OUTPUCOD_OBJLOCATEDEXPV is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_115_warmelt_outobj_OUTPUCOD_OBJLOCATEDEXPV_st
	 *) meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 25; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_115_warmelt_outobj_OUTPUCOD_OBJLOCATEDEXPV nbval 25*/
  meltfram__.mcfr_nbvar = 25 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("OUTPUCOD_OBJLOCATEDEXPV", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-outobj.melt:4410:/ getarg");
 /*_.OEXP__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.DECLBUF__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.DECLBUF__V3*/ meltfptr[2])) !=
	      NULL);


  /*getarg#2 */
  /*^getarg */
  if (meltxargdescr_[1] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.IMPLBUF__V4*/ meltfptr[3] =
    (meltxargtab_[1].meltbp_aptr) ? (*(meltxargtab_[1].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3])) !=
	      NULL);


  /*getarg#3 */
  /*^getarg */
  if (meltxargdescr_[2] != MELTBPAR_LONG)
    goto lab_endgetargs;
 /*_#DEPTH__L1*/ meltfnum[0] = meltxargtab_[2].meltbp_long;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:4411:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L2*/ meltfnum[1] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.OEXP__V2*/ meltfptr[1]),
			     (melt_ptr_t) (( /*!CLASS_OBJLOCATEDEXPV */
					    meltfrout->tabval[0])));;
      MELT_LOCATION ("warmelt-outobj.melt:4411:/ cond");
      /*cond */ if ( /*_#IS_A__L2*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V6*/ meltfptr[5] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-outobj.melt:4411:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check oexp"),
				  ("warmelt-outobj.melt")
				  ? ("warmelt-outobj.melt") : __FILE__,
				  (4411) ? (4411) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V6*/ meltfptr[5] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V5*/ meltfptr[4] = /*_.IFELSE___V6*/ meltfptr[5];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:4411:/ clear");
	     /*clear *//*_#IS_A__L2*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V6*/ meltfptr[5] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V5*/ meltfptr[4] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-outobj.melt:4412:/ quasiblock");


    /*^getslot */
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.OEXP__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 1, "OBX_CONT");
  /*_.CONT__V7*/ meltfptr[5] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-outobj.melt:4413:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.OEXP__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 2, "OBCX_LOC");
  /*_.OLOC__V8*/ meltfptr[7] = slot;
    };
    ;
 /*_#I__L3*/ meltfnum[1] =
      (( /*_#DEPTH__L1*/ meltfnum[0]) + (1));;
    /*^compute */
 /*_.BOXDEPTHP1__V9*/ meltfptr[8] =
      (meltgc_new_int
       ((meltobject_ptr_t) (( /*!DISCR_INTEGER */ meltfrout->tabval[1])),
	( /*_#I__L3*/ meltfnum[1])));;
    MELT_LOCATION ("warmelt-outobj.melt:4415:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.OEXP__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 0, "OBV_TYPE");
  /*_.OTYP__V10*/ meltfptr[9] = slot;
    };
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:4418:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_MULTIPLE_OR_NULL__L4*/ meltfnum[3] =
	(( /*_.CONT__V7*/ meltfptr[5]) == NULL
	 ||
	 (melt_unsafe_magic_discr ((melt_ptr_t) ( /*_.CONT__V7*/ meltfptr[5]))
	  == MELTOBMAG_MULTIPLE));;
      MELT_LOCATION ("warmelt-outobj.melt:4418:/ cond");
      /*cond */ if ( /*_#IS_MULTIPLE_OR_NULL__L4*/ meltfnum[3])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V12*/ meltfptr[11] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-outobj.melt:4418:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check cont"),
				  ("warmelt-outobj.melt")
				  ? ("warmelt-outobj.melt") : __FILE__,
				  (4418) ? (4418) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V12*/ meltfptr[11] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V11*/ meltfptr[10] = /*_.IFELSE___V12*/ meltfptr[11];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:4418:/ clear");
	     /*clear *//*_#IS_MULTIPLE_OR_NULL__L4*/ meltfnum[3] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V12*/ meltfptr[11] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V11*/ meltfptr[10] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-outobj.melt:4419:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#__L5*/ meltfnum[3] =
      (( /*_.OTYP__V10*/ meltfptr[9]) ==
       (( /*!CTYPE_VOID */ meltfrout->tabval[2])));;
    MELT_LOCATION ("warmelt-outobj.melt:4419:/ cond");
    /*cond */ if ( /*_#__L5*/ meltfnum[3])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{


	  {
	    MELT_LOCATION ("warmelt-outobj.melt:4421:/ locexp");
	    meltgc_strbuf_add_indent ((melt_ptr_t)
				      ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				      ( /*_#DEPTH__L1*/ meltfnum[0]), 0);
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:4422:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t)
				 ( /*_.IMPLBUF__V4*/ meltfptr[3]), ("{"));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:4423:/ locexp");
	    meltgc_strbuf_add_indent ((melt_ptr_t)
				      ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				      ( /*_#DEPTH__L1*/ meltfnum[0]), 0);
	  }
	  ;
	  MELT_LOCATION ("warmelt-outobj.melt:4424:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^apply */
	  /*apply */
	  {
	    union meltparam_un argtab[3];
	    memset (&argtab, 0, sizeof (argtab));
	    /*^apply.arg */
	    argtab[0].meltbp_aptr =
	      (melt_ptr_t *) & /*_.IMPLBUF__V4*/ meltfptr[3];
	    /*^apply.arg */
	    argtab[1].meltbp_long = /*_#DEPTH__L1*/ meltfnum[0];
	    /*^apply.arg */
	    argtab[2].meltbp_cstring = "locexp";
	    /*_.OUTPUT_LOCATION__V14*/ meltfptr[13] =
	      melt_apply ((meltclosure_ptr_t)
			  (( /*!OUTPUT_LOCATION */ meltfrout->tabval[3])),
			  (melt_ptr_t) ( /*_.OLOC__V8*/ meltfptr[7]),
			  (MELTBPARSTR_PTR MELTBPARSTR_LONG
			   MELTBPARSTR_CSTRING ""), argtab, "",
			  (union meltparam_un *) 0);
	  }
	  ;
	  MELT_LOCATION ("warmelt-outobj.melt:4420:/ quasiblock");


	  /*_.PROGN___V15*/ meltfptr[14] =
	    /*_.OUTPUT_LOCATION__V14*/ meltfptr[13];;
	  /*^compute */
	  /*_.IFELSE___V13*/ meltfptr[11] = /*_.PROGN___V15*/ meltfptr[14];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-outobj.melt:4419:/ clear");
	     /*clear *//*_.OUTPUT_LOCATION__V14*/ meltfptr[13] = 0;
	  /*^clear */
	     /*clear *//*_.PROGN___V15*/ meltfptr[14] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-outobj.melt:4426:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^cond */
	  /*cond */ if ( /*_.OLOC__V8*/ meltfptr[7])	/*then */
	    {
	      /*^cond.then */
	      /*^block */
	      /*anyblock */
	      {

		MELT_LOCATION ("warmelt-outobj.melt:4427:/ checksignal");
		MELT_CHECK_SIGNAL ();
		;
		/*^apply */
		/*apply */
		{
		  union meltparam_un argtab[3];
		  memset (&argtab, 0, sizeof (argtab));
		  /*^apply.arg */
		  argtab[0].meltbp_aptr =
		    (melt_ptr_t *) & /*_.IMPLBUF__V4*/ meltfptr[3];
		  /*^apply.arg */
		  argtab[1].meltbp_long = /*_#DEPTH__L1*/ meltfnum[0];
		  /*^apply.arg */
		  argtab[2].meltbp_cstring = "expr";
		  /*_.OUTPUT_RAW_LOCATION__V17*/ meltfptr[14] =
		    melt_apply ((meltclosure_ptr_t)
				(( /*!OUTPUT_RAW_LOCATION */ meltfrout->
				  tabval[4])),
				(melt_ptr_t) ( /*_.OLOC__V8*/ meltfptr[7]),
				(MELTBPARSTR_PTR MELTBPARSTR_LONG
				 MELTBPARSTR_CSTRING ""), argtab, "",
				(union meltparam_un *) 0);
		}
		;
		/*_.IF___V16*/ meltfptr[13] =
		  /*_.OUTPUT_RAW_LOCATION__V17*/ meltfptr[14];;
		/*epilog */

		MELT_LOCATION ("warmelt-outobj.melt:4426:/ clear");
	       /*clear *//*_.OUTPUT_RAW_LOCATION__V17*/ meltfptr[14] = 0;
	      }
	      ;
	    }
	  else
	    {			/*^cond.else */

    /*_.IF___V16*/ meltfptr[13] = NULL;;
	    }
	  ;
	  /*^compute */
	  /*_.IFELSE___V13*/ meltfptr[11] = /*_.IF___V16*/ meltfptr[13];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-outobj.melt:4419:/ clear");
	     /*clear *//*_.IF___V16*/ meltfptr[13] = 0;
	}
	;
      }
    ;
    /*citerblock FOREACH_IN_MULTIPLE */
    {
      /* start foreach_in_multiple meltcit1__EACHTUP */
      long meltcit1__EACHTUP_ln =
	melt_multiple_length ((melt_ptr_t) /*_.CONT__V7*/ meltfptr[5]);
      for ( /*_#IX__L6*/ meltfnum[5] = 0;
	   ( /*_#IX__L6*/ meltfnum[5] >= 0)
	   && ( /*_#IX__L6*/ meltfnum[5] < meltcit1__EACHTUP_ln);
	/*_#IX__L6*/ meltfnum[5]++)
	{
	  /*_.COMP__V18*/ meltfptr[14] =
	    melt_multiple_nth ((melt_ptr_t) ( /*_.CONT__V7*/ meltfptr[5]),
			       /*_#IX__L6*/ meltfnum[5]);



  /*_#GET_INT__L7*/ meltfnum[6] =
	    (melt_get_int
	     ((melt_ptr_t) ( /*_.BOXDEPTHP1__V9*/ meltfptr[8])));;
	  MELT_LOCATION ("warmelt-outobj.melt:4434:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^msend */
	  /*msend */
	  {
	    union meltparam_un argtab[3];
	    memset (&argtab, 0, sizeof (argtab));
	    /*^ojbmsend.arg */
	    argtab[0].meltbp_aptr =
	      (melt_ptr_t *) & /*_.DECLBUF__V3*/ meltfptr[2];
	    /*^ojbmsend.arg */
	    argtab[1].meltbp_aptr =
	      (melt_ptr_t *) & /*_.IMPLBUF__V4*/ meltfptr[3];
	    /*^ojbmsend.arg */
	    argtab[2].meltbp_long = /*_#GET_INT__L7*/ meltfnum[6];
	    /*_.OUTPUT_C_CODE__V19*/ meltfptr[13] =
	      meltgc_send ((melt_ptr_t) ( /*_.COMP__V18*/ meltfptr[14]),
			   (melt_ptr_t) (( /*!OUTPUT_C_CODE */ meltfrout->
					  tabval[5])),
			   (MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_LONG
			    ""), argtab, "", (union meltparam_un *) 0);
	  }
	  ;

#if MELT_HAVE_DEBUG
	  MELT_LOCATION ("warmelt-outobj.melt:4435:/ cppif.then");
	  /*^block */
	  /*anyblock */
	  {

	    /*^checksignal */
	    MELT_CHECK_SIGNAL ();
	    ;
    /*_#STRBUF_USEDLENGTH__L8*/ meltfnum[7] =
	      melt_strbuf_usedlength ((melt_ptr_t)
				      ( /*_.IMPLBUF__V4*/ meltfptr[3]));;
	    MELT_LOCATION ("warmelt-outobj.melt:4436:/ cond");
	    /*cond */ if (
			   /*ifisa */
			   melt_is_instance_of ((melt_ptr_t)
						(( /*!BUFFER_LIMIT_CONT */
						  meltfrout->tabval[6])),
						(melt_ptr_t) (( /*!CLASS_REFERENCE */ meltfrout->tabval[7])))
	      )			/*then */
	      {
		/*^cond.then */
		/*^getslot */
		{
		  melt_ptr_t slot = NULL, obj = NULL;
		  obj =
		    (melt_ptr_t) (( /*!BUFFER_LIMIT_CONT */ meltfrout->
				   tabval[6])) /*=obj*/ ;
		  melt_object_get_field (slot, obj, 0, "REFERENCED_VALUE");
      /*_.REFERENCED_VALUE__V21*/ meltfptr[20] = slot;
		};
		;
	      }
	    else
	      {			/*^cond.else */

     /*_.REFERENCED_VALUE__V21*/ meltfptr[20] = NULL;;
	      }
	    ;
	    /*^compute */
    /*_#GET_INT__L9*/ meltfnum[8] =
	      (melt_get_int
	       ((melt_ptr_t) ( /*_.REFERENCED_VALUE__V21*/ meltfptr[20])));;
	    /*^compute */
    /*_#I__L10*/ meltfnum[9] =
	      (( /*_#STRBUF_USEDLENGTH__L8*/ meltfnum[7]) <
	       ( /*_#GET_INT__L9*/ meltfnum[8]));;
	    MELT_LOCATION ("warmelt-outobj.melt:4435:/ cond");
	    /*cond */ if ( /*_#I__L10*/ meltfnum[9])	/*then */
	      {
		/*^cond.then */
		/*_.IFELSE___V22*/ meltfptr[21] = ( /*nil */ NULL);;
	      }
	    else
	      {
		MELT_LOCATION ("warmelt-outobj.melt:4435:/ cond.else");

		/*^block */
		/*anyblock */
		{




		  {
		    /*^locexp */
		    melt_assert_failed (("check limited implbuf"),
					("warmelt-outobj.melt")
					? ("warmelt-outobj.melt") : __FILE__,
					(4435) ? (4435) : __LINE__,
					__FUNCTION__);
		    ;
		  }
		  ;
		/*clear *//*_.IFELSE___V22*/ meltfptr[21] = 0;
		  /*epilog */
		}
		;
	      }
	    ;
	    /*^compute */
	    /*_.IFCPP___V20*/ meltfptr[19] = /*_.IFELSE___V22*/ meltfptr[21];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-outobj.melt:4435:/ clear");
	      /*clear *//*_#STRBUF_USEDLENGTH__L8*/ meltfnum[7] = 0;
	    /*^clear */
	      /*clear *//*_.REFERENCED_VALUE__V21*/ meltfptr[20] = 0;
	    /*^clear */
	      /*clear *//*_#GET_INT__L9*/ meltfnum[8] = 0;
	    /*^clear */
	      /*clear *//*_#I__L10*/ meltfnum[9] = 0;
	    /*^clear */
	      /*clear *//*_.IFELSE___V22*/ meltfptr[21] = 0;
	  }

#else /*MELT_HAVE_DEBUG */
	  /*^cppif.else */
	  /*_.IFCPP___V20*/ meltfptr[19] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
	  ;
	  if ( /*_#IX__L6*/ meltfnum[5] < 0)
	    break;
	}			/* end  foreach_in_multiple meltcit1__EACHTUP */

      /*citerepilog */

      MELT_LOCATION ("warmelt-outobj.melt:4431:/ clear");
	    /*clear *//*_.COMP__V18*/ meltfptr[14] = 0;
      /*^clear */
	    /*clear *//*_#IX__L6*/ meltfnum[5] = 0;
      /*^clear */
	    /*clear *//*_#GET_INT__L7*/ meltfnum[6] = 0;
      /*^clear */
	    /*clear *//*_.OUTPUT_C_CODE__V19*/ meltfptr[13] = 0;
      /*^clear */
	    /*clear *//*_.IFCPP___V20*/ meltfptr[19] = 0;
    }				/*endciterblock FOREACH_IN_MULTIPLE */
    ;
    MELT_LOCATION ("warmelt-outobj.melt:4438:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#__L11*/ meltfnum[7] =
      (( /*_.OTYP__V10*/ meltfptr[9]) ==
       (( /*!CTYPE_VOID */ meltfrout->tabval[2])));;
    MELT_LOCATION ("warmelt-outobj.melt:4438:/ cond");
    /*cond */ if ( /*_#__L11*/ meltfnum[7])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{


	  {
	    MELT_LOCATION ("warmelt-outobj.melt:4440:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t)
				 ( /*_.IMPLBUF__V4*/ meltfptr[3]), (";}"));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:4441:/ locexp");
	    meltgc_strbuf_add_indent ((melt_ptr_t)
				      ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				      ( /*_#DEPTH__L1*/ meltfnum[0]), 0);
	  }
	  ;
	  MELT_LOCATION ("warmelt-outobj.melt:4439:/ quasiblock");


	  /*epilog */
	}
	;
      }				/*noelse */
    ;

    MELT_LOCATION ("warmelt-outobj.melt:4412:/ clear");
	   /*clear *//*_.CONT__V7*/ meltfptr[5] = 0;
    /*^clear */
	   /*clear *//*_.OLOC__V8*/ meltfptr[7] = 0;
    /*^clear */
	   /*clear *//*_#I__L3*/ meltfnum[1] = 0;
    /*^clear */
	   /*clear *//*_.BOXDEPTHP1__V9*/ meltfptr[8] = 0;
    /*^clear */
	   /*clear *//*_.OTYP__V10*/ meltfptr[9] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V11*/ meltfptr[10] = 0;
    /*^clear */
	   /*clear *//*_#__L5*/ meltfnum[3] = 0;
    /*^clear */
	   /*clear *//*_.IFELSE___V13*/ meltfptr[11] = 0;
    /*^clear */
	   /*clear *//*_#__L11*/ meltfnum[7] = 0;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:4443:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#STRBUF_USEDLENGTH__L12*/ meltfnum[8] =
	melt_strbuf_usedlength ((melt_ptr_t)
				( /*_.IMPLBUF__V4*/ meltfptr[3]));;
      MELT_LOCATION ("warmelt-outobj.melt:4444:/ cond");
      /*cond */ if (
		     /*ifisa */
		     melt_is_instance_of ((melt_ptr_t)
					  (( /*!BUFFER_LIMIT_CONT */
					    meltfrout->tabval[6])),
					  (melt_ptr_t) (( /*!CLASS_REFERENCE */ meltfrout->tabval[7])))
	)			/*then */
	{
	  /*^cond.then */
	  /*^getslot */
	  {
	    melt_ptr_t slot = NULL, obj = NULL;
	    obj =
	      (melt_ptr_t) (( /*!BUFFER_LIMIT_CONT */ meltfrout->
			     tabval[6])) /*=obj*/ ;
	    melt_object_get_field (slot, obj, 0, "REFERENCED_VALUE");
     /*_.REFERENCED_VALUE__V24*/ meltfptr[21] = slot;
	  };
	  ;
	}
      else
	{			/*^cond.else */

    /*_.REFERENCED_VALUE__V24*/ meltfptr[21] = NULL;;
	}
      ;
      /*^compute */
   /*_#GET_INT__L13*/ meltfnum[9] =
	(melt_get_int
	 ((melt_ptr_t) ( /*_.REFERENCED_VALUE__V24*/ meltfptr[21])));;
      /*^compute */
   /*_#I__L14*/ meltfnum[1] =
	(( /*_#STRBUF_USEDLENGTH__L12*/ meltfnum[8]) <
	 ( /*_#GET_INT__L13*/ meltfnum[9]));;
      MELT_LOCATION ("warmelt-outobj.melt:4443:/ cond");
      /*cond */ if ( /*_#I__L14*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V25*/ meltfptr[5] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-outobj.melt:4443:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check limited implbuf"),
				  ("warmelt-outobj.melt")
				  ? ("warmelt-outobj.melt") : __FILE__,
				  (4443) ? (4443) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V25*/ meltfptr[5] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V23*/ meltfptr[20] = /*_.IFELSE___V25*/ meltfptr[5];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:4443:/ clear");
	     /*clear *//*_#STRBUF_USEDLENGTH__L12*/ meltfnum[8] = 0;
      /*^clear */
	     /*clear *//*_.REFERENCED_VALUE__V24*/ meltfptr[21] = 0;
      /*^clear */
	     /*clear *//*_#GET_INT__L13*/ meltfnum[9] = 0;
      /*^clear */
	     /*clear *//*_#I__L14*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V25*/ meltfptr[5] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V23*/ meltfptr[20] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-outobj.melt:4410:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.IFCPP___V23*/ meltfptr[20];;

    {
      MELT_LOCATION ("warmelt-outobj.melt:4410:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V5*/ meltfptr[4] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V23*/ meltfptr[20] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("OUTPUCOD_OBJLOCATEDEXPV", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_115_warmelt_outobj_OUTPUCOD_OBJLOCATEDEXPV_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_115_warmelt_outobj_OUTPUCOD_OBJLOCATEDEXPV */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_116_warmelt_outobj_OUTPUCOD_VERBATIMSTRING (meltclosure_ptr_t
						     meltclosp_,
						     melt_ptr_t
						     meltfirstargp_,
						     const
						     melt_argdescr_cell_t
						     meltxargdescr_[],
						     union meltparam_un *
						     meltxargtab_,
						     const
						     melt_argdescr_cell_t
						     meltxresdescr_[],
						     union meltparam_un *
						     meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_116_warmelt_outobj_OUTPUCOD_VERBATIMSTRING_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_116_warmelt_outobj_OUTPUCOD_VERBATIMSTRING_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 10
    melt_ptr_t mcfr_varptr[10];
#define MELTFRAM_NBVARNUM 5
    long mcfr_varnum[5];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_116_warmelt_outobj_OUTPUCOD_VERBATIMSTRING is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_116_warmelt_outobj_OUTPUCOD_VERBATIMSTRING_st
	 *) meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 10; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_116_warmelt_outobj_OUTPUCOD_VERBATIMSTRING nbval 10*/
  meltfram__.mcfr_nbvar = 10 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("OUTPUCOD_VERBATIMSTRING", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-outobj.melt:4449:/ getarg");
 /*_.VSTR__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.DECLBUF__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.DECLBUF__V3*/ meltfptr[2])) !=
	      NULL);


  /*getarg#2 */
  /*^getarg */
  if (meltxargdescr_[1] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.IMPLBUF__V4*/ meltfptr[3] =
    (meltxargtab_[1].meltbp_aptr) ? (*(meltxargtab_[1].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3])) !=
	      NULL);


  /*getarg#3 */
  /*^getarg */
  if (meltxargdescr_[2] != MELTBPAR_LONG)
    goto lab_endgetargs;
 /*_#DEPTH__L1*/ meltfnum[0] = meltxargtab_[2].meltbp_long;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:4450:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_.DISCRIM__V6*/ meltfptr[5] =
	((melt_ptr_t)
	 (melt_discr ((melt_ptr_t) ( /*_.VSTR__V2*/ meltfptr[1]))));;
      /*^compute */
   /*_#__L2*/ meltfnum[1] =
	(( /*_.DISCRIM__V6*/ meltfptr[5]) ==
	 (( /*!DISCR_VERBATIM_STRING */ meltfrout->tabval[0])));;
      MELT_LOCATION ("warmelt-outobj.melt:4450:/ cond");
      /*cond */ if ( /*_#__L2*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V7*/ meltfptr[6] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-outobj.melt:4450:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check vstr"),
				  ("warmelt-outobj.melt")
				  ? ("warmelt-outobj.melt") : __FILE__,
				  (4450) ? (4450) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V7*/ meltfptr[6] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V5*/ meltfptr[4] = /*_.IFELSE___V7*/ meltfptr[6];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:4450:/ clear");
	     /*clear *//*_.DISCRIM__V6*/ meltfptr[5] = 0;
      /*^clear */
	     /*clear *//*_#__L2*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V7*/ meltfptr[6] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V5*/ meltfptr[4] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:4451:/ locexp");
      /*add2sbuf_string */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   melt_string_str ((melt_ptr_t)
					    ( /*_.VSTR__V2*/ meltfptr[1])));
    }
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:4452:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#STRBUF_USEDLENGTH__L3*/ meltfnum[1] =
	melt_strbuf_usedlength ((melt_ptr_t)
				( /*_.IMPLBUF__V4*/ meltfptr[3]));;
      MELT_LOCATION ("warmelt-outobj.melt:4453:/ cond");
      /*cond */ if (
		     /*ifisa */
		     melt_is_instance_of ((melt_ptr_t)
					  (( /*!BUFFER_LIMIT_CONT */
					    meltfrout->tabval[1])),
					  (melt_ptr_t) (( /*!CLASS_REFERENCE */ meltfrout->tabval[2])))
	)			/*then */
	{
	  /*^cond.then */
	  /*^getslot */
	  {
	    melt_ptr_t slot = NULL, obj = NULL;
	    obj =
	      (melt_ptr_t) (( /*!BUFFER_LIMIT_CONT */ meltfrout->
			     tabval[1])) /*=obj*/ ;
	    melt_object_get_field (slot, obj, 0, "REFERENCED_VALUE");
     /*_.REFERENCED_VALUE__V9*/ meltfptr[6] = slot;
	  };
	  ;
	}
      else
	{			/*^cond.else */

    /*_.REFERENCED_VALUE__V9*/ meltfptr[6] = NULL;;
	}
      ;
      /*^compute */
   /*_#GET_INT__L4*/ meltfnum[3] =
	(melt_get_int
	 ((melt_ptr_t) ( /*_.REFERENCED_VALUE__V9*/ meltfptr[6])));;
      /*^compute */
   /*_#I__L5*/ meltfnum[4] =
	(( /*_#STRBUF_USEDLENGTH__L3*/ meltfnum[1]) <
	 ( /*_#GET_INT__L4*/ meltfnum[3]));;
      MELT_LOCATION ("warmelt-outobj.melt:4452:/ cond");
      /*cond */ if ( /*_#I__L5*/ meltfnum[4])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V10*/ meltfptr[9] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-outobj.melt:4452:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check limited implbuf"),
				  ("warmelt-outobj.melt")
				  ? ("warmelt-outobj.melt") : __FILE__,
				  (4452) ? (4452) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V10*/ meltfptr[9] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V8*/ meltfptr[5] = /*_.IFELSE___V10*/ meltfptr[9];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:4452:/ clear");
	     /*clear *//*_#STRBUF_USEDLENGTH__L3*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.REFERENCED_VALUE__V9*/ meltfptr[6] = 0;
      /*^clear */
	     /*clear *//*_#GET_INT__L4*/ meltfnum[3] = 0;
      /*^clear */
	     /*clear *//*_#I__L5*/ meltfnum[4] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V10*/ meltfptr[9] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V8*/ meltfptr[5] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-outobj.melt:4449:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.IFCPP___V8*/ meltfptr[5];;

    {
      MELT_LOCATION ("warmelt-outobj.melt:4449:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V5*/ meltfptr[4] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V8*/ meltfptr[5] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("OUTPUCOD_VERBATIMSTRING", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_116_warmelt_outobj_OUTPUCOD_VERBATIMSTRING_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_116_warmelt_outobj_OUTPUCOD_VERBATIMSTRING */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_117_warmelt_outobj_OUTPUCOD_STRING (meltclosure_ptr_t meltclosp_,
					     melt_ptr_t meltfirstargp_,
					     const melt_argdescr_cell_t
					     meltxargdescr_[],
					     union meltparam_un *
					     meltxargtab_,
					     const melt_argdescr_cell_t
					     meltxresdescr_[],
					     union meltparam_un *
					     meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_117_warmelt_outobj_OUTPUCOD_STRING_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_117_warmelt_outobj_OUTPUCOD_STRING_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 10
    melt_ptr_t mcfr_varptr[10];
#define MELTFRAM_NBVARNUM 5
    long mcfr_varnum[5];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_117_warmelt_outobj_OUTPUCOD_STRING is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_117_warmelt_outobj_OUTPUCOD_STRING_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 10; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_117_warmelt_outobj_OUTPUCOD_STRING nbval 10*/
  meltfram__.mcfr_nbvar = 10 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("OUTPUCOD_STRING", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-outobj.melt:4462:/ getarg");
 /*_.VSTR__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.DECLBUF__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.DECLBUF__V3*/ meltfptr[2])) !=
	      NULL);


  /*getarg#2 */
  /*^getarg */
  if (meltxargdescr_[1] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.IMPLBUF__V4*/ meltfptr[3] =
    (meltxargtab_[1].meltbp_aptr) ? (*(meltxargtab_[1].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3])) !=
	      NULL);


  /*getarg#3 */
  /*^getarg */
  if (meltxargdescr_[2] != MELTBPAR_LONG)
    goto lab_endgetargs;
 /*_#DEPTH__L1*/ meltfnum[0] = meltxargtab_[2].meltbp_long;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:4463:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_.DISCRIM__V6*/ meltfptr[5] =
	((melt_ptr_t)
	 (melt_discr ((melt_ptr_t) ( /*_.VSTR__V2*/ meltfptr[1]))));;
      /*^compute */
   /*_#__L2*/ meltfnum[1] =
	(( /*_.DISCRIM__V6*/ meltfptr[5]) ==
	 (( /*!DISCR_STRING */ meltfrout->tabval[0])));;
      MELT_LOCATION ("warmelt-outobj.melt:4463:/ cond");
      /*cond */ if ( /*_#__L2*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V7*/ meltfptr[6] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-outobj.melt:4463:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check vstr"),
				  ("warmelt-outobj.melt")
				  ? ("warmelt-outobj.melt") : __FILE__,
				  (4463) ? (4463) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V7*/ meltfptr[6] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V5*/ meltfptr[4] = /*_.IFELSE___V7*/ meltfptr[6];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:4463:/ clear");
	     /*clear *//*_.DISCRIM__V6*/ meltfptr[5] = 0;
      /*^clear */
	     /*clear *//*_#__L2*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V7*/ meltfptr[6] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V5*/ meltfptr[4] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:4464:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   (" \""));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:4465:/ locexp");
      meltgc_add_strbuf_cstr ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			      melt_string_str ((melt_ptr_t)
					       ( /*_.VSTR__V2*/
						meltfptr[1])));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:4466:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   ("\""));
    }
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:4467:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#STRBUF_USEDLENGTH__L3*/ meltfnum[1] =
	melt_strbuf_usedlength ((melt_ptr_t)
				( /*_.IMPLBUF__V4*/ meltfptr[3]));;
      MELT_LOCATION ("warmelt-outobj.melt:4468:/ cond");
      /*cond */ if (
		     /*ifisa */
		     melt_is_instance_of ((melt_ptr_t)
					  (( /*!BUFFER_LIMIT_CONT */
					    meltfrout->tabval[1])),
					  (melt_ptr_t) (( /*!CLASS_REFERENCE */ meltfrout->tabval[2])))
	)			/*then */
	{
	  /*^cond.then */
	  /*^getslot */
	  {
	    melt_ptr_t slot = NULL, obj = NULL;
	    obj =
	      (melt_ptr_t) (( /*!BUFFER_LIMIT_CONT */ meltfrout->
			     tabval[1])) /*=obj*/ ;
	    melt_object_get_field (slot, obj, 0, "REFERENCED_VALUE");
     /*_.REFERENCED_VALUE__V9*/ meltfptr[6] = slot;
	  };
	  ;
	}
      else
	{			/*^cond.else */

    /*_.REFERENCED_VALUE__V9*/ meltfptr[6] = NULL;;
	}
      ;
      /*^compute */
   /*_#GET_INT__L4*/ meltfnum[3] =
	(melt_get_int
	 ((melt_ptr_t) ( /*_.REFERENCED_VALUE__V9*/ meltfptr[6])));;
      /*^compute */
   /*_#I__L5*/ meltfnum[4] =
	(( /*_#STRBUF_USEDLENGTH__L3*/ meltfnum[1]) <
	 ( /*_#GET_INT__L4*/ meltfnum[3]));;
      MELT_LOCATION ("warmelt-outobj.melt:4467:/ cond");
      /*cond */ if ( /*_#I__L5*/ meltfnum[4])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V10*/ meltfptr[9] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-outobj.melt:4467:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check limited implbuf"),
				  ("warmelt-outobj.melt")
				  ? ("warmelt-outobj.melt") : __FILE__,
				  (4467) ? (4467) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V10*/ meltfptr[9] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V8*/ meltfptr[5] = /*_.IFELSE___V10*/ meltfptr[9];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:4467:/ clear");
	     /*clear *//*_#STRBUF_USEDLENGTH__L3*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.REFERENCED_VALUE__V9*/ meltfptr[6] = 0;
      /*^clear */
	     /*clear *//*_#GET_INT__L4*/ meltfnum[3] = 0;
      /*^clear */
	     /*clear *//*_#I__L5*/ meltfnum[4] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V10*/ meltfptr[9] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V8*/ meltfptr[5] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-outobj.melt:4462:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.IFCPP___V8*/ meltfptr[5];;

    {
      MELT_LOCATION ("warmelt-outobj.melt:4462:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V5*/ meltfptr[4] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V8*/ meltfptr[5] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("OUTPUCOD_STRING", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_117_warmelt_outobj_OUTPUCOD_STRING_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_117_warmelt_outobj_OUTPUCOD_STRING */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_118_warmelt_outobj_OUTPUCOD_INTEGER (meltclosure_ptr_t meltclosp_,
					      melt_ptr_t meltfirstargp_,
					      const melt_argdescr_cell_t
					      meltxargdescr_[],
					      union meltparam_un *
					      meltxargtab_,
					      const melt_argdescr_cell_t
					      meltxresdescr_[],
					      union meltparam_un *
					      meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_118_warmelt_outobj_OUTPUCOD_INTEGER_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_118_warmelt_outobj_OUTPUCOD_INTEGER_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 6
    melt_ptr_t mcfr_varptr[6];
#define MELTFRAM_NBVARNUM 3
    long mcfr_varnum[3];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_118_warmelt_outobj_OUTPUCOD_INTEGER is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_118_warmelt_outobj_OUTPUCOD_INTEGER_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 6; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_118_warmelt_outobj_OUTPUCOD_INTEGER nbval 6*/
  meltfram__.mcfr_nbvar = 6 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("OUTPUCOD_INTEGER", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-outobj.melt:4475:/ getarg");
 /*_.VINT__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.DECLBUF__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.DECLBUF__V3*/ meltfptr[2])) !=
	      NULL);


  /*getarg#2 */
  /*^getarg */
  if (meltxargdescr_[1] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.IMPLBUF__V4*/ meltfptr[3] =
    (meltxargtab_[1].meltbp_aptr) ? (*(meltxargtab_[1].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3])) !=
	      NULL);


  /*getarg#3 */
  /*^getarg */
  if (meltxargdescr_[2] != MELTBPAR_LONG)
    goto lab_endgetargs;
 /*_#DEPTH__L1*/ meltfnum[0] = meltxargtab_[2].meltbp_long;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:4476:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_INTEGERBOX__L2*/ meltfnum[1] =
	(melt_magic_discr ((melt_ptr_t) ( /*_.VINT__V2*/ meltfptr[1])) ==
	 MELTOBMAG_INT);;
      MELT_LOCATION ("warmelt-outobj.melt:4476:/ cond");
      /*cond */ if ( /*_#IS_INTEGERBOX__L2*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V6*/ meltfptr[5] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-outobj.melt:4476:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check vint"),
				  ("warmelt-outobj.melt")
				  ? ("warmelt-outobj.melt") : __FILE__,
				  (4476) ? (4476) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V6*/ meltfptr[5] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V5*/ meltfptr[4] = /*_.IFELSE___V6*/ meltfptr[5];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:4476:/ clear");
	     /*clear *//*_#IS_INTEGERBOX__L2*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V6*/ meltfptr[5] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V5*/ meltfptr[4] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    /*^compute */
 /*_#GET_INT__L3*/ meltfnum[1] =
      (melt_get_int ((melt_ptr_t) ( /*_.VINT__V2*/ meltfptr[1])));;

    {
      MELT_LOCATION ("warmelt-outobj.melt:4477:/ locexp");
      meltgc_add_strbuf_dec ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			     ( /*_#GET_INT__L3*/ meltfnum[1]));
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:4475:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V5*/ meltfptr[4] = 0;
    /*^clear */
	   /*clear *//*_#GET_INT__L3*/ meltfnum[1] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("OUTPUCOD_INTEGER", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_118_warmelt_outobj_OUTPUCOD_INTEGER_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*noretval */ NULL);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_118_warmelt_outobj_OUTPUCOD_INTEGER */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_119_warmelt_outobj_OUTPUCOD_FINALRETURN (meltclosure_ptr_t
						  meltclosp_,
						  melt_ptr_t meltfirstargp_,
						  const melt_argdescr_cell_t
						  meltxargdescr_[],
						  union meltparam_un *
						  meltxargtab_,
						  const melt_argdescr_cell_t
						  meltxresdescr_[],
						  union meltparam_un *
						  meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_119_warmelt_outobj_OUTPUCOD_FINALRETURN_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_119_warmelt_outobj_OUTPUCOD_FINALRETURN_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 11
    melt_ptr_t mcfr_varptr[11];
#define MELTFRAM_NBVARNUM 5
    long mcfr_varnum[5];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_119_warmelt_outobj_OUTPUCOD_FINALRETURN is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_119_warmelt_outobj_OUTPUCOD_FINALRETURN_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 11; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_119_warmelt_outobj_OUTPUCOD_FINALRETURN nbval 11*/
  meltfram__.mcfr_nbvar = 11 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("OUTPUCOD_FINALRETURN", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-outobj.melt:4482:/ getarg");
 /*_.FRET__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.DECLBUF__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.DECLBUF__V3*/ meltfptr[2])) !=
	      NULL);


  /*getarg#2 */
  /*^getarg */
  if (meltxargdescr_[1] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.IMPLBUF__V4*/ meltfptr[3] =
    (meltxargtab_[1].meltbp_aptr) ? (*(meltxargtab_[1].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3])) !=
	      NULL);


  /*getarg#3 */
  /*^getarg */
  if (meltxargdescr_[2] != MELTBPAR_LONG)
    goto lab_endgetargs;
 /*_#DEPTH__L1*/ meltfnum[0] = meltxargtab_[2].meltbp_long;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:4483:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L2*/ meltfnum[1] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.FRET__V2*/ meltfptr[1]),
			     (melt_ptr_t) (( /*!CLASS_OBJFINALRETURN */
					    meltfrout->tabval[0])));;
      MELT_LOCATION ("warmelt-outobj.melt:4483:/ cond");
      /*cond */ if ( /*_#IS_A__L2*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V6*/ meltfptr[5] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-outobj.melt:4483:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check fret"),
				  ("warmelt-outobj.melt")
				  ? ("warmelt-outobj.melt") : __FILE__,
				  (4483) ? (4483) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V6*/ meltfptr[5] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V5*/ meltfptr[4] = /*_.IFELSE___V6*/ meltfptr[5];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:4483:/ clear");
	     /*clear *//*_#IS_A__L2*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V6*/ meltfptr[5] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V5*/ meltfptr[4] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-outobj.melt:4484:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.FRET__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 0, "OBI_LOC");
  /*_.OBI_LOC__V7*/ meltfptr[5] = slot;
    };
    ;
    /*^checksignal */
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[3];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.IMPLBUF__V4*/ meltfptr[3];
      /*^apply.arg */
      argtab[1].meltbp_long = /*_#DEPTH__L1*/ meltfnum[0];
      /*^apply.arg */
      argtab[2].meltbp_cstring = "finalreturn";
      /*_.OUTPUT_LOCATION__V8*/ meltfptr[7] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!OUTPUT_LOCATION */ meltfrout->tabval[1])),
		    (melt_ptr_t) ( /*_.OBI_LOC__V7*/ meltfptr[5]),
		    (MELTBPARSTR_PTR MELTBPARSTR_LONG MELTBPARSTR_CSTRING ""),
		    argtab, "", (union meltparam_un *) 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:4485:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   (";"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:4486:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				( /*_#DEPTH__L1*/ meltfnum[0]), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:4487:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   ("/*finalret*/ goto labend_rout "));
    }
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:4488:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#STRBUF_USEDLENGTH__L3*/ meltfnum[1] =
	melt_strbuf_usedlength ((melt_ptr_t)
				( /*_.IMPLBUF__V4*/ meltfptr[3]));;
      MELT_LOCATION ("warmelt-outobj.melt:4489:/ cond");
      /*cond */ if (
		     /*ifisa */
		     melt_is_instance_of ((melt_ptr_t)
					  (( /*!BUFFER_LIMIT_CONT */
					    meltfrout->tabval[2])),
					  (melt_ptr_t) (( /*!CLASS_REFERENCE */ meltfrout->tabval[3])))
	)			/*then */
	{
	  /*^cond.then */
	  /*^getslot */
	  {
	    melt_ptr_t slot = NULL, obj = NULL;
	    obj =
	      (melt_ptr_t) (( /*!BUFFER_LIMIT_CONT */ meltfrout->
			     tabval[2])) /*=obj*/ ;
	    melt_object_get_field (slot, obj, 0, "REFERENCED_VALUE");
     /*_.REFERENCED_VALUE__V10*/ meltfptr[9] = slot;
	  };
	  ;
	}
      else
	{			/*^cond.else */

    /*_.REFERENCED_VALUE__V10*/ meltfptr[9] = NULL;;
	}
      ;
      /*^compute */
   /*_#GET_INT__L4*/ meltfnum[3] =
	(melt_get_int
	 ((melt_ptr_t) ( /*_.REFERENCED_VALUE__V10*/ meltfptr[9])));;
      /*^compute */
   /*_#I__L5*/ meltfnum[4] =
	(( /*_#STRBUF_USEDLENGTH__L3*/ meltfnum[1]) <
	 ( /*_#GET_INT__L4*/ meltfnum[3]));;
      MELT_LOCATION ("warmelt-outobj.melt:4488:/ cond");
      /*cond */ if ( /*_#I__L5*/ meltfnum[4])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V11*/ meltfptr[10] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-outobj.melt:4488:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check limited implbuf"),
				  ("warmelt-outobj.melt")
				  ? ("warmelt-outobj.melt") : __FILE__,
				  (4488) ? (4488) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V11*/ meltfptr[10] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V9*/ meltfptr[8] = /*_.IFELSE___V11*/ meltfptr[10];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:4488:/ clear");
	     /*clear *//*_#STRBUF_USEDLENGTH__L3*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.REFERENCED_VALUE__V10*/ meltfptr[9] = 0;
      /*^clear */
	     /*clear *//*_#GET_INT__L4*/ meltfnum[3] = 0;
      /*^clear */
	     /*clear *//*_#I__L5*/ meltfnum[4] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V11*/ meltfptr[10] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V9*/ meltfptr[8] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-outobj.melt:4482:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.IFCPP___V9*/ meltfptr[8];;

    {
      MELT_LOCATION ("warmelt-outobj.melt:4482:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V5*/ meltfptr[4] = 0;
    /*^clear */
	   /*clear *//*_.OBI_LOC__V7*/ meltfptr[5] = 0;
    /*^clear */
	   /*clear *//*_.OUTPUT_LOCATION__V8*/ meltfptr[7] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V9*/ meltfptr[8] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("OUTPUCOD_FINALRETURN", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_119_warmelt_outobj_OUTPUCOD_FINALRETURN_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_119_warmelt_outobj_OUTPUCOD_FINALRETURN */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_120_warmelt_outobj_SORTED_NAMED_DICT_TUPLE (meltclosure_ptr_t
						     meltclosp_,
						     melt_ptr_t
						     meltfirstargp_,
						     const
						     melt_argdescr_cell_t
						     meltxargdescr_[],
						     union meltparam_un *
						     meltxargtab_,
						     const
						     melt_argdescr_cell_t
						     meltxresdescr_[],
						     union meltparam_un *
						     meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_120_warmelt_outobj_SORTED_NAMED_DICT_TUPLE_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_120_warmelt_outobj_SORTED_NAMED_DICT_TUPLE_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 13
    melt_ptr_t mcfr_varptr[13];
#define MELTFRAM_NBVARNUM 4
    long mcfr_varnum[4];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_120_warmelt_outobj_SORTED_NAMED_DICT_TUPLE is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_120_warmelt_outobj_SORTED_NAMED_DICT_TUPLE_st
	 *) meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 13; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_120_warmelt_outobj_SORTED_NAMED_DICT_TUPLE nbval 13*/
  meltfram__.mcfr_nbvar = 13 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("SORTED_NAMED_DICT_TUPLE", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-outobj.melt:4495:/ getarg");
 /*_.DIC__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {

    MELT_LOCATION ("warmelt-outobj.melt:4496:/ quasiblock");


 /*_#DICNT__L1*/ meltfnum[0] =
      (melt_count_mapstrings
       ((struct meltmapstrings_st *) ( /*_.DIC__V2*/ meltfptr[1])));;
    /*^compute */
 /*_.ENTLIST__V4*/ meltfptr[3] =
      (meltgc_new_list
       ((meltobject_ptr_t) (( /*!DISCR_LIST */ meltfrout->tabval[0]))));;
    /*citerblock FOREACH_IN_MAPSTRING */
    {
      /*foreach_in_mapstring meltcit1__EACHSTRMAP : */ int
	meltcit1__EACHSTRMAP_ix = 0, meltcit1__EACHSTRMAP_siz = 0;
      for (meltcit1__EACHSTRMAP_ix = 0;
	   /* we retrieve in meltcit1__EACHSTRMAP_siz the size at each iteration since it could change. */
	   meltcit1__EACHSTRMAP_ix >= 0
	   && (meltcit1__EACHSTRMAP_siz =
	       melt_size_mapstrings ((struct meltmapstrings_st *)
				     /*_.DIC__V2*/ meltfptr[1])) > 0
	   && meltcit1__EACHSTRMAP_ix < meltcit1__EACHSTRMAP_siz;
	   meltcit1__EACHSTRMAP_ix++)
	{
	  const char *meltcit1__EACHSTRMAP_str = NULL;
	  const char *meltcit1__EACHSTRMAP_nam = NULL;
    /*_.NAM__V5*/ meltfptr[4] = NULL;
    /*_.ENT__V6*/ meltfptr[5] = NULL;
	  meltcit1__EACHSTRMAP_str =
	    ((struct meltmapstrings_st *) /*_.DIC__V2*/ meltfptr[1])->
	    entab[meltcit1__EACHSTRMAP_ix].e_at;
	  if (!meltcit1__EACHSTRMAP_str
	      || meltcit1__EACHSTRMAP_str == HTAB_DELETED_ENTRY)
	    continue;		/*foreach_in_mapstring meltcit1__EACHSTRMAP inside before */
	  /*_.ENT__V6*/ meltfptr[5] =
	    ((struct meltmapstrings_st *) /*_.DIC__V2*/ meltfptr[1])->
	    entab[meltcit1__EACHSTRMAP_ix].e_va;
	  if (! /*_.ENT__V6*/ meltfptr[5])
	    continue;
	  if (melt_is_instance_of
	      ((melt_ptr_t) /*_.ENT__V6*/ meltfptr[5],
	       (melt_ptr_t) MELT_PREDEF (CLASS_NAMED))
	      && ( /*_.NAM__V5*/ meltfptr[4] =
		  melt_object_nth_field ((melt_ptr_t) /*_.ENT__V6*/
					 meltfptr[5],
					 MELTFIELD_NAMED_NAME)) != NULL
	      && (meltcit1__EACHSTRMAP_nam =
		  melt_string_str ((melt_ptr_t) /*_.NAM__V5*/ meltfptr[4])) !=
	      (char *) 0
	      && !strcmp (meltcit1__EACHSTRMAP_nam, meltcit1__EACHSTRMAP_str))
	    /*_.NAM__V5*/ meltfptr[4] = /*_.NAM__V5*/ meltfptr[4];
	  else
	    {
      /*_.NAM__V5*/ meltfptr[4] = NULL;
      /*_.NAM__V5*/ meltfptr[4] =
		meltgc_new_stringdup ((meltobject_ptr_t)
				      MELT_PREDEF (DISCR_STRING),
				      meltcit1__EACHSTRMAP_str);
	    }
	  meltcit1__EACHSTRMAP_str = (const char *) 0;
	  meltcit1__EACHSTRMAP_nam = (const char *) 0;




#if MELT_HAVE_DEBUG
	  MELT_LOCATION ("warmelt-outobj.melt:4502:/ cppif.then");
	  /*^block */
	  /*anyblock */
	  {

	    /*^checksignal */
	    MELT_CHECK_SIGNAL ();
	    ;
    /*_#IS_A__L2*/ meltfnum[1] =
	      melt_is_instance_of ((melt_ptr_t) ( /*_.ENT__V6*/ meltfptr[5]),
				   (melt_ptr_t) (( /*!CLASS_NAMED */
						  meltfrout->tabval[1])));;
	    MELT_LOCATION ("warmelt-outobj.melt:4502:/ cond");
	    /*cond */ if ( /*_#IS_A__L2*/ meltfnum[1])	/*then */
	      {
		/*^cond.then */
		/*_.IFELSE___V8*/ meltfptr[7] = ( /*nil */ NULL);;
	      }
	    else
	      {
		MELT_LOCATION ("warmelt-outobj.melt:4502:/ cond.else");

		/*^block */
		/*anyblock */
		{




		  {
		    /*^locexp */
		    melt_assert_failed (("check ent named"),
					("warmelt-outobj.melt")
					? ("warmelt-outobj.melt") : __FILE__,
					(4502) ? (4502) : __LINE__,
					__FUNCTION__);
		    ;
		  }
		  ;
		/*clear *//*_.IFELSE___V8*/ meltfptr[7] = 0;
		  /*epilog */
		}
		;
	      }
	    ;
	    /*^compute */
	    /*_.IFCPP___V7*/ meltfptr[6] = /*_.IFELSE___V8*/ meltfptr[7];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-outobj.melt:4502:/ clear");
	      /*clear *//*_#IS_A__L2*/ meltfnum[1] = 0;
	    /*^clear */
	      /*clear *//*_.IFELSE___V8*/ meltfptr[7] = 0;
	  }

#else /*MELT_HAVE_DEBUG */
	  /*^cppif.else */
	  /*_.IFCPP___V7*/ meltfptr[6] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:4503:/ locexp");
	    meltgc_append_list ((melt_ptr_t) ( /*_.ENTLIST__V4*/ meltfptr[3]),
				(melt_ptr_t) ( /*_.ENT__V6*/ meltfptr[5]));
	  }
	  ;
	  /* end foreach_in_mapstring meltcit1__EACHSTRMAP */
    /*_.NAM__V5*/ meltfptr[4] = NULL;
    /*_.ENT__V6*/ meltfptr[5] = NULL;
	}

      /*citerepilog */

      MELT_LOCATION ("warmelt-outobj.melt:4499:/ clear");
	    /*clear *//*_.NAM__V5*/ meltfptr[4] = 0;
      /*^clear */
	    /*clear *//*_.ENT__V6*/ meltfptr[5] = 0;
      /*^clear */
	    /*clear *//*_.IFCPP___V7*/ meltfptr[6] = 0;
    }				/*endciterblock FOREACH_IN_MAPSTRING */
    ;
    MELT_LOCATION ("warmelt-outobj.melt:4504:/ quasiblock");


    /*^checksignal */
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      /*_.RAWTUP__V10*/ meltfptr[9] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!LIST_TO_MULTIPLE */ meltfrout->tabval[2])),
		    (melt_ptr_t) ( /*_.ENTLIST__V4*/ meltfptr[3]), (""),
		    (union meltparam_un *) 0, "", (union meltparam_un *) 0);
    }
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:4506:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MULTIPLE_LENGTH__L3*/ meltfnum[1] =
	(melt_multiple_length
	 ((melt_ptr_t) ( /*_.RAWTUP__V10*/ meltfptr[9])));;
      /*^compute */
   /*_#I__L4*/ meltfnum[3] =
	(( /*_#DICNT__L1*/ meltfnum[0]) ==
	 ( /*_#MULTIPLE_LENGTH__L3*/ meltfnum[1]));;
      MELT_LOCATION ("warmelt-outobj.melt:4506:/ cond");
      /*cond */ if ( /*_#I__L4*/ meltfnum[3])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V12*/ meltfptr[11] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-outobj.melt:4506:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("sorted_named_dict_tuple check tuple length is dict count"), ("warmelt-outobj.melt") ? ("warmelt-outobj.melt") : __FILE__, (4506) ? (4506) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V12*/ meltfptr[11] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V11*/ meltfptr[10] = /*_.IFELSE___V12*/ meltfptr[11];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:4506:/ clear");
	     /*clear *//*_#MULTIPLE_LENGTH__L3*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_#I__L4*/ meltfnum[3] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V12*/ meltfptr[11] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V11*/ meltfptr[10] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    /*^compute */
 /*_.MULTIPLE_SORT__V13*/ meltfptr[11] =
      meltgc_sort_multiple ((melt_ptr_t) ( /*_.RAWTUP__V10*/ meltfptr[9]),
			    (melt_ptr_t) (( /*!COMPARE_NAMED_ALPHA */
					   meltfrout->tabval[3])),
			    (melt_ptr_t) (( /*!DISCR_MULTIPLE */ meltfrout->
					   tabval[4])));;
    /*^compute */
    /*_.LET___V9*/ meltfptr[7] = /*_.MULTIPLE_SORT__V13*/ meltfptr[11];;

    MELT_LOCATION ("warmelt-outobj.melt:4504:/ clear");
	   /*clear *//*_.RAWTUP__V10*/ meltfptr[9] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V11*/ meltfptr[10] = 0;
    /*^clear */
	   /*clear *//*_.MULTIPLE_SORT__V13*/ meltfptr[11] = 0;
    /*_.LET___V3*/ meltfptr[2] = /*_.LET___V9*/ meltfptr[7];;

    MELT_LOCATION ("warmelt-outobj.melt:4496:/ clear");
	   /*clear *//*_#DICNT__L1*/ meltfnum[0] = 0;
    /*^clear */
	   /*clear *//*_.ENTLIST__V4*/ meltfptr[3] = 0;
    /*^clear */
	   /*clear *//*_.LET___V9*/ meltfptr[7] = 0;
    MELT_LOCATION ("warmelt-outobj.melt:4495:/ quasiblock");


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.LET___V3*/ meltfptr[2];;

    {
      MELT_LOCATION ("warmelt-outobj.melt:4495:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.LET___V3*/ meltfptr[2] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("SORTED_NAMED_DICT_TUPLE", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_120_warmelt_outobj_SORTED_NAMED_DICT_TUPLE_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_120_warmelt_outobj_SORTED_NAMED_DICT_TUPLE */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_121_warmelt_outobj_OUTPUT_EXPORTED_OFFSETS (meltclosure_ptr_t
						     meltclosp_,
						     melt_ptr_t
						     meltfirstargp_,
						     const
						     melt_argdescr_cell_t
						     meltxargdescr_[],
						     union meltparam_un *
						     meltxargtab_,
						     const
						     melt_argdescr_cell_t
						     meltxresdescr_[],
						     union meltparam_un *
						     meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_121_warmelt_outobj_OUTPUT_EXPORTED_OFFSETS_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_121_warmelt_outobj_OUTPUT_EXPORTED_OFFSETS_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 29
    melt_ptr_t mcfr_varptr[29];
#define MELTFRAM_NBVARNUM 15
    long mcfr_varnum[15];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_121_warmelt_outobj_OUTPUT_EXPORTED_OFFSETS is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_121_warmelt_outobj_OUTPUT_EXPORTED_OFFSETS_st
	 *) meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 29; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_121_warmelt_outobj_OUTPUT_EXPORTED_OFFSETS nbval 29*/
  meltfram__.mcfr_nbvar = 29 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("OUTPUT_EXPORTED_OFFSETS", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-outobj.melt:4512:/ getarg");
 /*_.MODCTX__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.DECLBUF__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.DECLBUF__V3*/ meltfptr[2])) !=
	      NULL);


  /*getarg#2 */
  /*^getarg */
  if (meltxargdescr_[1] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.IMPLBUF__V4*/ meltfptr[3] =
    (meltxargtab_[1].meltbp_aptr) ? (*(meltxargtab_[1].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3])) !=
	      NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:4513:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L1*/ meltfnum[0] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.MODCTX__V2*/ meltfptr[1]),
			     (melt_ptr_t) (( /*!CLASS_MODULE_CONTEXT */
					    meltfrout->tabval[0])));;
      MELT_LOCATION ("warmelt-outobj.melt:4513:/ cond");
      /*cond */ if ( /*_#IS_A__L1*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V6*/ meltfptr[5] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-outobj.melt:4513:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check modctx"),
				  ("warmelt-outobj.melt")
				  ? ("warmelt-outobj.melt") : __FILE__,
				  (4513) ? (4513) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V6*/ meltfptr[5] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V5*/ meltfptr[4] = /*_.IFELSE___V6*/ meltfptr[5];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:4513:/ clear");
	     /*clear *//*_#IS_A__L1*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V6*/ meltfptr[5] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V5*/ meltfptr[4] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-outobj.melt:4514:/ quasiblock");


    MELT_LOCATION ("warmelt-outobj.melt:4515:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.MODCTX__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 1, "MOCX_EXPFIELDICT");
  /*_.RAWDICTFIELDS__V7*/ meltfptr[5] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-outobj.melt:4516:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      /*_.SORTEDFIELDS__V8*/ meltfptr[7] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!SORTED_NAMED_DICT_TUPLE */ meltfrout->tabval[1])),
		    (melt_ptr_t) ( /*_.RAWDICTFIELDS__V7*/ meltfptr[5]), (""),
		    (union meltparam_un *) 0, "", (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:4517:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.MODCTX__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 2, "MOCX_EXPCLASSDICT");
  /*_.RAWDICTCLASSES__V9*/ meltfptr[8] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-outobj.melt:4518:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      /*_.SORTEDCLASSES__V10*/ meltfptr[9] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!SORTED_NAMED_DICT_TUPLE */ meltfrout->tabval[1])),
		    (melt_ptr_t) ( /*_.RAWDICTCLASSES__V9*/ meltfptr[8]),
		    (""), (union meltparam_un *) 0, "",
		    (union meltparam_un *) 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:4520:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				(0), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:4521:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				(0), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:4522:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   ("/* exported "));
    }
    ;
 /*_#MAPSTRING_COUNT__L2*/ meltfnum[0] =
      (melt_count_mapstrings
       ((struct meltmapstrings_st *) ( /*_.RAWDICTFIELDS__V7*/
				      meltfptr[5])));;

    {
      MELT_LOCATION ("warmelt-outobj.melt:4523:/ locexp");
      meltgc_add_strbuf_dec ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			     ( /*_#MAPSTRING_COUNT__L2*/ meltfnum[0]));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:4524:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   (" field offsets */"));
    }
    ;
    /*citerblock FOREACH_IN_MULTIPLE */
    {
      /* start foreach_in_multiple meltcit1__EACHTUP */
      long meltcit1__EACHTUP_ln =
	melt_multiple_length ((melt_ptr_t) /*_.SORTEDFIELDS__V8*/
			      meltfptr[7]);
      for ( /*_#IX__L3*/ meltfnum[2] = 0;
	   ( /*_#IX__L3*/ meltfnum[2] >= 0)
	   && ( /*_#IX__L3*/ meltfnum[2] < meltcit1__EACHTUP_ln);
	/*_#IX__L3*/ meltfnum[2]++)
	{
	  /*_.FLD__V11*/ meltfptr[10] =
	    melt_multiple_nth ((melt_ptr_t)
			       ( /*_.SORTEDFIELDS__V8*/ meltfptr[7]),
			       /*_#IX__L3*/ meltfnum[2]);




#if MELT_HAVE_DEBUG
	  MELT_LOCATION ("warmelt-outobj.melt:4528:/ cppif.then");
	  /*^block */
	  /*anyblock */
	  {

	    /*^checksignal */
	    MELT_CHECK_SIGNAL ();
	    ;
    /*_#IS_A__L4*/ meltfnum[3] =
	      melt_is_instance_of ((melt_ptr_t)
				   ( /*_.FLD__V11*/ meltfptr[10]),
				   (melt_ptr_t) (( /*!CLASS_FIELD */
						  meltfrout->tabval[2])));;
	    MELT_LOCATION ("warmelt-outobj.melt:4528:/ cond");
	    /*cond */ if ( /*_#IS_A__L4*/ meltfnum[3])	/*then */
	      {
		/*^cond.then */
		/*_.IFELSE___V13*/ meltfptr[12] = ( /*nil */ NULL);;
	      }
	    else
	      {
		MELT_LOCATION ("warmelt-outobj.melt:4528:/ cond.else");

		/*^block */
		/*anyblock */
		{




		  {
		    /*^locexp */
		    melt_assert_failed (("check fld"),
					("warmelt-outobj.melt")
					? ("warmelt-outobj.melt") : __FILE__,
					(4528) ? (4528) : __LINE__,
					__FUNCTION__);
		    ;
		  }
		  ;
		/*clear *//*_.IFELSE___V13*/ meltfptr[12] = 0;
		  /*epilog */
		}
		;
	      }
	    ;
	    /*^compute */
	    /*_.IFCPP___V12*/ meltfptr[11] = /*_.IFELSE___V13*/ meltfptr[12];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-outobj.melt:4528:/ clear");
	      /*clear *//*_#IS_A__L4*/ meltfnum[3] = 0;
	    /*^clear */
	      /*clear *//*_.IFELSE___V13*/ meltfptr[12] = 0;
	  }

#else /*MELT_HAVE_DEBUG */
	  /*^cppif.else */
	  /*_.IFCPP___V12*/ meltfptr[11] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:4529:/ locexp");
	    meltgc_strbuf_add_indent ((melt_ptr_t)
				      ( /*_.IMPLBUF__V4*/ meltfptr[3]), (0),
				      0);
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:4530:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t)
				 ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				 ("MELT_EXTERN const int meltfieldoff__"));
	  }
	  ;
	  MELT_LOCATION ("warmelt-outobj.melt:4531:/ getslot");
	  {
	    melt_ptr_t slot = NULL, obj = NULL;
	    obj = (melt_ptr_t) ( /*_.FLD__V11*/ meltfptr[10]) /*=obj*/ ;
	    melt_object_get_field (slot, obj, 1, "NAMED_NAME");
   /*_.NAMED_NAME__V14*/ meltfptr[12] = slot;
	  };
	  ;

	  {
	    /*^locexp */
	    meltgc_add_strbuf_cident ((melt_ptr_t)
				      ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				      melt_string_str ((melt_ptr_t)
						       ( /*_.NAMED_NAME__V14*/
							meltfptr[12])));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:4532:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t)
				 ( /*_.IMPLBUF__V4*/ meltfptr[3]), (";"));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:4533:/ locexp");
	    meltgc_strbuf_add_indent ((melt_ptr_t)
				      ( /*_.IMPLBUF__V4*/ meltfptr[3]), (0),
				      0);
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:4534:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t)
				 ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				 ("const int meltfieldoff__"));
	  }
	  ;
	  MELT_LOCATION ("warmelt-outobj.melt:4535:/ getslot");
	  {
	    melt_ptr_t slot = NULL, obj = NULL;
	    obj = (melt_ptr_t) ( /*_.FLD__V11*/ meltfptr[10]) /*=obj*/ ;
	    melt_object_get_field (slot, obj, 1, "NAMED_NAME");
   /*_.NAMED_NAME__V15*/ meltfptr[14] = slot;
	  };
	  ;

	  {
	    /*^locexp */
	    meltgc_add_strbuf_cident ((melt_ptr_t)
				      ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				      melt_string_str ((melt_ptr_t)
						       ( /*_.NAMED_NAME__V15*/
							meltfptr[14])));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:4536:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t)
				 ( /*_.IMPLBUF__V4*/ meltfptr[3]), (" = "));
	  }
	  ;
  /*_#GET_INT__L5*/ meltfnum[3] =
	    (melt_get_int ((melt_ptr_t) ( /*_.FLD__V11*/ meltfptr[10])));;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:4537:/ locexp");
	    meltgc_add_strbuf_dec ((melt_ptr_t)
				   ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				   ( /*_#GET_INT__L5*/ meltfnum[3]));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:4538:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t)
				 ( /*_.IMPLBUF__V4*/ meltfptr[3]), (";"));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:4539:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t)
				 ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				 (" /* in "));
	  }
	  ;
	  MELT_LOCATION ("warmelt-outobj.melt:4540:/ cond");
	  /*cond */ if (
			 /*ifisa */
			 melt_is_instance_of ((melt_ptr_t)
					      ( /*_.FLD__V11*/ meltfptr[10]),
					      (melt_ptr_t) (( /*!CLASS_FIELD */ meltfrout->tabval[2])))
	    )			/*then */
	    {
	      /*^cond.then */
	      /*^getslot */
	      {
		melt_ptr_t slot = NULL, obj = NULL;
		obj = (melt_ptr_t) ( /*_.FLD__V11*/ meltfptr[10]) /*=obj*/ ;
		melt_object_get_field (slot, obj, 2, "FLD_OWNCLASS");
    /*_.FLD_OWNCLASS__V16*/ meltfptr[15] = slot;
	      };
	      ;
	    }
	  else
	    {			/*^cond.else */

   /*_.FLD_OWNCLASS__V16*/ meltfptr[15] = NULL;;
	    }
	  ;
	  MELT_LOCATION ("warmelt-outobj.melt:4540:/ cond");
	  /*cond */ if (
			 /*ifisa */
			 melt_is_instance_of ((melt_ptr_t)
					      ( /*_.FLD_OWNCLASS__V16*/
					       meltfptr[15]),
					      (melt_ptr_t) (( /*!CLASS_NAMED */ meltfrout->tabval[3])))
	    )			/*then */
	    {
	      /*^cond.then */
	      /*^getslot */
	      {
		melt_ptr_t slot = NULL, obj = NULL;
		obj =
		  (melt_ptr_t) ( /*_.FLD_OWNCLASS__V16*/ meltfptr[15])
		  /*=obj*/ ;
		melt_object_get_field (slot, obj, 1, "NAMED_NAME");
    /*_.NAMED_NAME__V17*/ meltfptr[16] = slot;
	      };
	      ;
	    }
	  else
	    {			/*^cond.else */

   /*_.NAMED_NAME__V17*/ meltfptr[16] = NULL;;
	    }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:4540:/ locexp");
	    /*add2sbuf_string */
	      meltgc_add_strbuf ((melt_ptr_t)
				 ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				 melt_string_str ((melt_ptr_t)
						  ( /*_.NAMED_NAME__V17*/
						   meltfptr[16])));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:4541:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t)
				 ( /*_.IMPLBUF__V4*/ meltfptr[3]), (" */"));
	  }
	  ;

#if MELT_HAVE_DEBUG
	  MELT_LOCATION ("warmelt-outobj.melt:4542:/ cppif.then");
	  /*^block */
	  /*anyblock */
	  {

	    /*^checksignal */
	    MELT_CHECK_SIGNAL ();
	    ;
    /*_#STRBUF_USEDLENGTH__L6*/ meltfnum[5] =
	      melt_strbuf_usedlength ((melt_ptr_t)
				      ( /*_.IMPLBUF__V4*/ meltfptr[3]));;
	    MELT_LOCATION ("warmelt-outobj.melt:4543:/ cond");
	    /*cond */ if (
			   /*ifisa */
			   melt_is_instance_of ((melt_ptr_t)
						(( /*!BUFFER_LIMIT_CONT */
						  meltfrout->tabval[4])),
						(melt_ptr_t) (( /*!CLASS_REFERENCE */ meltfrout->tabval[5])))
	      )			/*then */
	      {
		/*^cond.then */
		/*^getslot */
		{
		  melt_ptr_t slot = NULL, obj = NULL;
		  obj =
		    (melt_ptr_t) (( /*!BUFFER_LIMIT_CONT */ meltfrout->
				   tabval[4])) /*=obj*/ ;
		  melt_object_get_field (slot, obj, 0, "REFERENCED_VALUE");
      /*_.REFERENCED_VALUE__V19*/ meltfptr[18] = slot;
		};
		;
	      }
	    else
	      {			/*^cond.else */

     /*_.REFERENCED_VALUE__V19*/ meltfptr[18] = NULL;;
	      }
	    ;
	    /*^compute */
    /*_#GET_INT__L7*/ meltfnum[6] =
	      (melt_get_int
	       ((melt_ptr_t) ( /*_.REFERENCED_VALUE__V19*/ meltfptr[18])));;
	    /*^compute */
    /*_#I__L8*/ meltfnum[7] =
	      (( /*_#STRBUF_USEDLENGTH__L6*/ meltfnum[5]) <
	       ( /*_#GET_INT__L7*/ meltfnum[6]));;
	    MELT_LOCATION ("warmelt-outobj.melt:4542:/ cond");
	    /*cond */ if ( /*_#I__L8*/ meltfnum[7])	/*then */
	      {
		/*^cond.then */
		/*_.IFELSE___V20*/ meltfptr[19] = ( /*nil */ NULL);;
	      }
	    else
	      {
		MELT_LOCATION ("warmelt-outobj.melt:4542:/ cond.else");

		/*^block */
		/*anyblock */
		{




		  {
		    /*^locexp */
		    melt_assert_failed (("check limited implbuf"),
					("warmelt-outobj.melt")
					? ("warmelt-outobj.melt") : __FILE__,
					(4542) ? (4542) : __LINE__,
					__FUNCTION__);
		    ;
		  }
		  ;
		/*clear *//*_.IFELSE___V20*/ meltfptr[19] = 0;
		  /*epilog */
		}
		;
	      }
	    ;
	    /*^compute */
	    /*_.IFCPP___V18*/ meltfptr[17] = /*_.IFELSE___V20*/ meltfptr[19];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-outobj.melt:4542:/ clear");
	      /*clear *//*_#STRBUF_USEDLENGTH__L6*/ meltfnum[5] = 0;
	    /*^clear */
	      /*clear *//*_.REFERENCED_VALUE__V19*/ meltfptr[18] = 0;
	    /*^clear */
	      /*clear *//*_#GET_INT__L7*/ meltfnum[6] = 0;
	    /*^clear */
	      /*clear *//*_#I__L8*/ meltfnum[7] = 0;
	    /*^clear */
	      /*clear *//*_.IFELSE___V20*/ meltfptr[19] = 0;
	  }

#else /*MELT_HAVE_DEBUG */
	  /*^cppif.else */
	  /*_.IFCPP___V18*/ meltfptr[17] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
	  ;
	  if ( /*_#IX__L3*/ meltfnum[2] < 0)
	    break;
	}			/* end  foreach_in_multiple meltcit1__EACHTUP */

      /*citerepilog */

      MELT_LOCATION ("warmelt-outobj.melt:4525:/ clear");
	    /*clear *//*_.FLD__V11*/ meltfptr[10] = 0;
      /*^clear */
	    /*clear *//*_#IX__L3*/ meltfnum[2] = 0;
      /*^clear */
	    /*clear *//*_.IFCPP___V12*/ meltfptr[11] = 0;
      /*^clear */
	    /*clear *//*_.NAMED_NAME__V14*/ meltfptr[12] = 0;
      /*^clear */
	    /*clear *//*_.NAMED_NAME__V15*/ meltfptr[14] = 0;
      /*^clear */
	    /*clear *//*_#GET_INT__L5*/ meltfnum[3] = 0;
      /*^clear */
	    /*clear *//*_.FLD_OWNCLASS__V16*/ meltfptr[15] = 0;
      /*^clear */
	    /*clear *//*_.NAMED_NAME__V17*/ meltfptr[16] = 0;
      /*^clear */
	    /*clear *//*_.IFCPP___V18*/ meltfptr[17] = 0;
    }				/*endciterblock FOREACH_IN_MULTIPLE */
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:4545:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				(0), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:4546:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				(0), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:4547:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   ("/* exported "));
    }
    ;
 /*_#MAPSTRING_COUNT__L9*/ meltfnum[5] =
      (melt_count_mapstrings
       ((struct meltmapstrings_st *) ( /*_.RAWDICTCLASSES__V9*/
				      meltfptr[8])));;

    {
      MELT_LOCATION ("warmelt-outobj.melt:4548:/ locexp");
      meltgc_add_strbuf_dec ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			     ( /*_#MAPSTRING_COUNT__L9*/ meltfnum[5]));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:4549:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   (" class lengths */"));
    }
    ;
    /*citerblock FOREACH_IN_MULTIPLE */
    {
      /* start foreach_in_multiple meltcit2__EACHTUP */
      long meltcit2__EACHTUP_ln =
	melt_multiple_length ((melt_ptr_t) /*_.SORTEDCLASSES__V10*/
			      meltfptr[9]);
      for ( /*_#IX__L10*/ meltfnum[6] = 0;
	   ( /*_#IX__L10*/ meltfnum[6] >= 0)
	   && ( /*_#IX__L10*/ meltfnum[6] < meltcit2__EACHTUP_ln);
	/*_#IX__L10*/ meltfnum[6]++)
	{
	  /*_.CLA__V21*/ meltfptr[18] =
	    melt_multiple_nth ((melt_ptr_t)
			       ( /*_.SORTEDCLASSES__V10*/ meltfptr[9]),
			       /*_#IX__L10*/ meltfnum[6]);




#if MELT_HAVE_DEBUG
	  MELT_LOCATION ("warmelt-outobj.melt:4553:/ cppif.then");
	  /*^block */
	  /*anyblock */
	  {

	    /*^checksignal */
	    MELT_CHECK_SIGNAL ();
	    ;
    /*_#IS_A__L11*/ meltfnum[7] =
	      melt_is_instance_of ((melt_ptr_t)
				   ( /*_.CLA__V21*/ meltfptr[18]),
				   (melt_ptr_t) (( /*!CLASS_CLASS */
						  meltfrout->tabval[6])));;
	    MELT_LOCATION ("warmelt-outobj.melt:4553:/ cond");
	    /*cond */ if ( /*_#IS_A__L11*/ meltfnum[7])	/*then */
	      {
		/*^cond.then */
		/*_.IFELSE___V23*/ meltfptr[22] = ( /*nil */ NULL);;
	      }
	    else
	      {
		MELT_LOCATION ("warmelt-outobj.melt:4553:/ cond.else");

		/*^block */
		/*anyblock */
		{




		  {
		    /*^locexp */
		    melt_assert_failed (("check cla"),
					("warmelt-outobj.melt")
					? ("warmelt-outobj.melt") : __FILE__,
					(4553) ? (4553) : __LINE__,
					__FUNCTION__);
		    ;
		  }
		  ;
		/*clear *//*_.IFELSE___V23*/ meltfptr[22] = 0;
		  /*epilog */
		}
		;
	      }
	    ;
	    /*^compute */
	    /*_.IFCPP___V22*/ meltfptr[19] = /*_.IFELSE___V23*/ meltfptr[22];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-outobj.melt:4553:/ clear");
	      /*clear *//*_#IS_A__L11*/ meltfnum[7] = 0;
	    /*^clear */
	      /*clear *//*_.IFELSE___V23*/ meltfptr[22] = 0;
	  }

#else /*MELT_HAVE_DEBUG */
	  /*^cppif.else */
	  /*_.IFCPP___V22*/ meltfptr[19] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:4554:/ locexp");
	    meltgc_strbuf_add_indent ((melt_ptr_t)
				      ( /*_.IMPLBUF__V4*/ meltfptr[3]), (0),
				      0);
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:4555:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t)
				 ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				 ("MELT_EXTERN const int meltclasslen__"));
	  }
	  ;
	  MELT_LOCATION ("warmelt-outobj.melt:4556:/ getslot");
	  {
	    melt_ptr_t slot = NULL, obj = NULL;
	    obj = (melt_ptr_t) ( /*_.CLA__V21*/ meltfptr[18]) /*=obj*/ ;
	    melt_object_get_field (slot, obj, 1, "NAMED_NAME");
   /*_.NAMED_NAME__V24*/ meltfptr[22] = slot;
	  };
	  ;

	  {
	    /*^locexp */
	    meltgc_add_strbuf_cident ((melt_ptr_t)
				      ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				      melt_string_str ((melt_ptr_t)
						       ( /*_.NAMED_NAME__V24*/
							meltfptr[22])));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:4557:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t)
				 ( /*_.IMPLBUF__V4*/ meltfptr[3]), (";"));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:4558:/ locexp");
	    meltgc_strbuf_add_indent ((melt_ptr_t)
				      ( /*_.IMPLBUF__V4*/ meltfptr[3]), (0),
				      0);
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:4559:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t)
				 ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				 ("const int meltclasslen__"));
	  }
	  ;
	  MELT_LOCATION ("warmelt-outobj.melt:4560:/ getslot");
	  {
	    melt_ptr_t slot = NULL, obj = NULL;
	    obj = (melt_ptr_t) ( /*_.CLA__V21*/ meltfptr[18]) /*=obj*/ ;
	    melt_object_get_field (slot, obj, 1, "NAMED_NAME");
   /*_.NAMED_NAME__V25*/ meltfptr[24] = slot;
	  };
	  ;

	  {
	    /*^locexp */
	    meltgc_add_strbuf_cident ((melt_ptr_t)
				      ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				      melt_string_str ((melt_ptr_t)
						       ( /*_.NAMED_NAME__V25*/
							meltfptr[24])));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:4561:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t)
				 ( /*_.IMPLBUF__V4*/ meltfptr[3]), (" = "));
	  }
	  ;
	  MELT_LOCATION ("warmelt-outobj.melt:4563:/ getslot");
	  {
	    melt_ptr_t slot = NULL, obj = NULL;
	    obj = (melt_ptr_t) ( /*_.CLA__V21*/ meltfptr[18]) /*=obj*/ ;
	    melt_object_get_field (slot, obj, 6, "CLASS_FIELDS");
   /*_.CLASS_FIELDS__V26*/ meltfptr[25] = slot;
	  };
	  ;
  /*_#MULTIPLE_LENGTH__L12*/ meltfnum[7] =
	    (melt_multiple_length
	     ((melt_ptr_t) ( /*_.CLASS_FIELDS__V26*/ meltfptr[25])));;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:4562:/ locexp");
	    meltgc_add_strbuf_dec ((melt_ptr_t)
				   ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				   ( /*_#MULTIPLE_LENGTH__L12*/ meltfnum[7]));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:4564:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t)
				 ( /*_.IMPLBUF__V4*/ meltfptr[3]), (";"));
	  }
	  ;

#if MELT_HAVE_DEBUG
	  MELT_LOCATION ("warmelt-outobj.melt:4565:/ cppif.then");
	  /*^block */
	  /*anyblock */
	  {

	    /*^checksignal */
	    MELT_CHECK_SIGNAL ();
	    ;
    /*_#STRBUF_USEDLENGTH__L13*/ meltfnum[12] =
	      melt_strbuf_usedlength ((melt_ptr_t)
				      ( /*_.IMPLBUF__V4*/ meltfptr[3]));;
	    MELT_LOCATION ("warmelt-outobj.melt:4566:/ cond");
	    /*cond */ if (
			   /*ifisa */
			   melt_is_instance_of ((melt_ptr_t)
						(( /*!BUFFER_LIMIT_CONT */
						  meltfrout->tabval[4])),
						(melt_ptr_t) (( /*!CLASS_REFERENCE */ meltfrout->tabval[5])))
	      )			/*then */
	      {
		/*^cond.then */
		/*^getslot */
		{
		  melt_ptr_t slot = NULL, obj = NULL;
		  obj =
		    (melt_ptr_t) (( /*!BUFFER_LIMIT_CONT */ meltfrout->
				   tabval[4])) /*=obj*/ ;
		  melt_object_get_field (slot, obj, 0, "REFERENCED_VALUE");
      /*_.REFERENCED_VALUE__V28*/ meltfptr[27] = slot;
		};
		;
	      }
	    else
	      {			/*^cond.else */

     /*_.REFERENCED_VALUE__V28*/ meltfptr[27] = NULL;;
	      }
	    ;
	    /*^compute */
    /*_#GET_INT__L14*/ meltfnum[13] =
	      (melt_get_int
	       ((melt_ptr_t) ( /*_.REFERENCED_VALUE__V28*/ meltfptr[27])));;
	    /*^compute */
    /*_#I__L15*/ meltfnum[14] =
	      (( /*_#STRBUF_USEDLENGTH__L13*/ meltfnum[12]) <
	       ( /*_#GET_INT__L14*/ meltfnum[13]));;
	    MELT_LOCATION ("warmelt-outobj.melt:4565:/ cond");
	    /*cond */ if ( /*_#I__L15*/ meltfnum[14])	/*then */
	      {
		/*^cond.then */
		/*_.IFELSE___V29*/ meltfptr[28] = ( /*nil */ NULL);;
	      }
	    else
	      {
		MELT_LOCATION ("warmelt-outobj.melt:4565:/ cond.else");

		/*^block */
		/*anyblock */
		{




		  {
		    /*^locexp */
		    melt_assert_failed (("check limited implbuf"),
					("warmelt-outobj.melt")
					? ("warmelt-outobj.melt") : __FILE__,
					(4565) ? (4565) : __LINE__,
					__FUNCTION__);
		    ;
		  }
		  ;
		/*clear *//*_.IFELSE___V29*/ meltfptr[28] = 0;
		  /*epilog */
		}
		;
	      }
	    ;
	    /*^compute */
	    /*_.IFCPP___V27*/ meltfptr[26] = /*_.IFELSE___V29*/ meltfptr[28];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-outobj.melt:4565:/ clear");
	      /*clear *//*_#STRBUF_USEDLENGTH__L13*/ meltfnum[12] = 0;
	    /*^clear */
	      /*clear *//*_.REFERENCED_VALUE__V28*/ meltfptr[27] = 0;
	    /*^clear */
	      /*clear *//*_#GET_INT__L14*/ meltfnum[13] = 0;
	    /*^clear */
	      /*clear *//*_#I__L15*/ meltfnum[14] = 0;
	    /*^clear */
	      /*clear *//*_.IFELSE___V29*/ meltfptr[28] = 0;
	  }

#else /*MELT_HAVE_DEBUG */
	  /*^cppif.else */
	  /*_.IFCPP___V27*/ meltfptr[26] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
	  ;
	  if ( /*_#IX__L10*/ meltfnum[6] < 0)
	    break;
	}			/* end  foreach_in_multiple meltcit2__EACHTUP */

      /*citerepilog */

      MELT_LOCATION ("warmelt-outobj.melt:4550:/ clear");
	    /*clear *//*_.CLA__V21*/ meltfptr[18] = 0;
      /*^clear */
	    /*clear *//*_#IX__L10*/ meltfnum[6] = 0;
      /*^clear */
	    /*clear *//*_.IFCPP___V22*/ meltfptr[19] = 0;
      /*^clear */
	    /*clear *//*_.NAMED_NAME__V24*/ meltfptr[22] = 0;
      /*^clear */
	    /*clear *//*_.NAMED_NAME__V25*/ meltfptr[24] = 0;
      /*^clear */
	    /*clear *//*_.CLASS_FIELDS__V26*/ meltfptr[25] = 0;
      /*^clear */
	    /*clear *//*_#MULTIPLE_LENGTH__L12*/ meltfnum[7] = 0;
      /*^clear */
	    /*clear *//*_.IFCPP___V27*/ meltfptr[26] = 0;
    }				/*endciterblock FOREACH_IN_MULTIPLE */
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:4568:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				(0), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:4569:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				(0), 0);
    }
    ;

    MELT_LOCATION ("warmelt-outobj.melt:4514:/ clear");
	   /*clear *//*_.RAWDICTFIELDS__V7*/ meltfptr[5] = 0;
    /*^clear */
	   /*clear *//*_.SORTEDFIELDS__V8*/ meltfptr[7] = 0;
    /*^clear */
	   /*clear *//*_.RAWDICTCLASSES__V9*/ meltfptr[8] = 0;
    /*^clear */
	   /*clear *//*_.SORTEDCLASSES__V10*/ meltfptr[9] = 0;
    /*^clear */
	   /*clear *//*_#MAPSTRING_COUNT__L2*/ meltfnum[0] = 0;
    /*^clear */
	   /*clear *//*_#MAPSTRING_COUNT__L9*/ meltfnum[5] = 0;
    MELT_LOCATION ("warmelt-outobj.melt:4512:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V5*/ meltfptr[4] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("OUTPUT_EXPORTED_OFFSETS", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_121_warmelt_outobj_OUTPUT_EXPORTED_OFFSETS_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*noretval */ NULL);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_121_warmelt_outobj_OUTPUT_EXPORTED_OFFSETS */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_122_warmelt_outobj_NTH_SECUNDARY_FILE (meltclosure_ptr_t meltclosp_,
						melt_ptr_t meltfirstargp_,
						const melt_argdescr_cell_t
						meltxargdescr_[],
						union meltparam_un *
						meltxargtab_,
						const melt_argdescr_cell_t
						meltxresdescr_[],
						union meltparam_un *
						meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_122_warmelt_outobj_NTH_SECUNDARY_FILE_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_122_warmelt_outobj_NTH_SECUNDARY_FILE_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 25
    melt_ptr_t mcfr_varptr[25];
#define MELTFRAM_NBVARNUM 7
    long mcfr_varnum[7];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_122_warmelt_outobj_NTH_SECUNDARY_FILE is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_122_warmelt_outobj_NTH_SECUNDARY_FILE_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 25; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_122_warmelt_outobj_NTH_SECUNDARY_FILE nbval 25*/
  meltfram__.mcfr_nbvar = 25 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("NTH_SECUNDARY_FILE", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-outobj.melt:4591:/ getarg");
 /*_.MODCTX__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.MODNAMSTR__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.MODNAMSTR__V3*/ meltfptr[2])) !=
	      NULL);


  /*getarg#2 */
  /*^getarg */
  if (meltxargdescr_[1] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.DECLBUF__V4*/ meltfptr[3] =
    (meltxargtab_[1].meltbp_aptr) ? (*(meltxargtab_[1].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.DECLBUF__V4*/ meltfptr[3])) !=
	      NULL);


  /*getarg#3 */
  /*^getarg */
  if (meltxargdescr_[2] != MELTBPAR_LONG)
    goto lab_endgetargs;
 /*_#IX__L1*/ meltfnum[0] = meltxargtab_[2].meltbp_long;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:4592:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L2*/ meltfnum[1] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.MODCTX__V2*/ meltfptr[1]),
			     (melt_ptr_t) (( /*!CLASS_MODULE_CONTEXT */
					    meltfrout->tabval[0])));;
      MELT_LOCATION ("warmelt-outobj.melt:4592:/ cond");
      /*cond */ if ( /*_#IS_A__L2*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V6*/ meltfptr[5] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-outobj.melt:4592:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check modctx"),
				  ("warmelt-outobj.melt")
				  ? ("warmelt-outobj.melt") : __FILE__,
				  (4592) ? (4592) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V6*/ meltfptr[5] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V5*/ meltfptr[4] = /*_.IFELSE___V6*/ meltfptr[5];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:4592:/ clear");
	     /*clear *//*_#IS_A__L2*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V6*/ meltfptr[5] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V5*/ meltfptr[4] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:4593:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_STRING__L3*/ meltfnum[1] =
	(melt_magic_discr ((melt_ptr_t) ( /*_.MODNAMSTR__V3*/ meltfptr[2])) ==
	 MELTOBMAG_STRING);;
      MELT_LOCATION ("warmelt-outobj.melt:4593:/ cond");
      /*cond */ if ( /*_#IS_STRING__L3*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V8*/ meltfptr[7] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-outobj.melt:4593:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check modnamstr"),
				  ("warmelt-outobj.melt")
				  ? ("warmelt-outobj.melt") : __FILE__,
				  (4593) ? (4593) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V8*/ meltfptr[7] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V7*/ meltfptr[5] = /*_.IFELSE___V8*/ meltfptr[7];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:4593:/ clear");
	     /*clear *//*_#IS_STRING__L3*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V8*/ meltfptr[7] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V7*/ meltfptr[5] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:4594:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_STRBUF__L4*/ meltfnum[1] =
	(melt_magic_discr ((melt_ptr_t) ( /*_.DECLBUF__V4*/ meltfptr[3])) ==
	 MELTOBMAG_STRBUF);;
      MELT_LOCATION ("warmelt-outobj.melt:4594:/ cond");
      /*cond */ if ( /*_#IS_STRBUF__L4*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V10*/ meltfptr[9] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-outobj.melt:4594:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check declbuf"),
				  ("warmelt-outobj.melt")
				  ? ("warmelt-outobj.melt") : __FILE__,
				  (4594) ? (4594) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V10*/ meltfptr[9] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V9*/ meltfptr[7] = /*_.IFELSE___V10*/ meltfptr[9];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:4594:/ clear");
	     /*clear *//*_#IS_STRBUF__L4*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V10*/ meltfptr[9] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V9*/ meltfptr[7] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-outobj.melt:4595:/ quasiblock");


    /*^cond */
    /*cond */ if (
		   /*ifisa */
		   melt_is_instance_of ((melt_ptr_t)
					( /*_.MODCTX__V2*/ meltfptr[1]),
					(melt_ptr_t) (( /*!CLASS_MODULE_CONTEXT */ meltfrout->tabval[0])))
      )				/*then */
      {
	/*^cond.then */
	/*^getslot */
	{
	  melt_ptr_t slot = NULL, obj = NULL;
	  obj = (melt_ptr_t) ( /*_.MODCTX__V2*/ meltfptr[1]) /*=obj*/ ;
	  melt_object_get_field (slot, obj, 5, "MOCX_FILETUPLE");
   /*_.MOFILES__V12*/ meltfptr[11] = slot;
	};
	;
      }
    else
      {				/*^cond.else */

  /*_.MOFILES__V12*/ meltfptr[11] = NULL;;
      }
    ;
    /*^compute */
 /*_#NBFILES__L5*/ meltfnum[1] =
      (melt_multiple_length
       ((melt_ptr_t) ( /*_.MOFILES__V12*/ meltfptr[11])));;
    /*^compute */
 /*_.NTHFILE__V13*/ meltfptr[12] =
      (melt_multiple_nth
       ((melt_ptr_t) ( /*_.MOFILES__V12*/ meltfptr[11]),
	( /*_#IX__L1*/ meltfnum[0])));;
    MELT_LOCATION ("warmelt-outobj.melt:4599:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^cond */
    /*cond */ if ( /*_.NTHFILE__V13*/ meltfptr[12])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  /*^checksignal */
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^quasiblock */


	  /*_.RETVAL___V1*/ meltfptr[0] = /*_.NTHFILE__V13*/ meltfptr[12];;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:4599:/ locexp");
	    /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
	    if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	      melt_warn_for_no_expected_secondary_results ();
	    /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
	    ;
	  }
	  ;
	  /*^finalreturn */
	  ;
	  /*finalret */ goto labend_rout;
	  /*_.IF___V14*/ meltfptr[13] = /*_.RETURN___V15*/ meltfptr[14];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-outobj.melt:4599:/ clear");
	     /*clear *//*_.RETURN___V15*/ meltfptr[14] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

  /*_.IF___V14*/ meltfptr[13] = NULL;;
      }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:4600:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#I__L6*/ meltfnum[5] =
      (( /*_#IX__L1*/ meltfnum[0]) <= (0));;
    MELT_LOCATION ("warmelt-outobj.melt:4600:/ cond");
    /*cond */ if ( /*_#I__L6*/ meltfnum[5])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  /*^quasiblock */


   /*_.RETVAL___V1*/ meltfptr[0] = NULL;;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:4600:/ locexp");
	    /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
	    if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	      melt_warn_for_no_expected_secondary_results ();
	    /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
	    ;
	  }
	  ;
	  /*^finalreturn */
	  ;
	  /*finalret */ goto labend_rout;
	  /*_.IF___V16*/ meltfptr[14] = /*_.RETURN___V17*/ meltfptr[16];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-outobj.melt:4600:/ clear");
	     /*clear *//*_.RETURN___V17*/ meltfptr[16] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

  /*_.IF___V16*/ meltfptr[14] = NULL;;
      }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:4601:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#I__L7*/ meltfnum[6] =
      (( /*_#IX__L1*/ meltfnum[0]) >= ( /*_#NBFILES__L5*/ meltfnum[1]));;
    MELT_LOCATION ("warmelt-outobj.melt:4601:/ cond");
    /*cond */ if ( /*_#I__L7*/ meltfnum[6])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  /*^quasiblock */


   /*_.RETVAL___V1*/ meltfptr[0] = NULL;;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:4601:/ locexp");
	    /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
	    if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	      melt_warn_for_no_expected_secondary_results ();
	    /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
	    ;
	  }
	  ;
	  /*^finalreturn */
	  ;
	  /*finalret */ goto labend_rout;
	  /*_.IF___V18*/ meltfptr[16] = /*_.RETURN___V19*/ meltfptr[18];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-outobj.melt:4601:/ clear");
	     /*clear *//*_.RETURN___V19*/ meltfptr[18] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

  /*_.IF___V18*/ meltfptr[16] = NULL;;
      }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:4603:/ quasiblock");


 /*_.PATH__V21*/ meltfptr[20] =
      meltgc_new_string_generated_c_filename	/*  generated_c_filename */
      ((meltobject_ptr_t) (( /*!DISCR_STRING */ meltfrout->tabval[1])),
       melt_string_str ((melt_ptr_t) ( /*_.MODNAMSTR__V3*/ meltfptr[2])),
       melt_string_str ((melt_ptr_t) (( /*nil */ NULL))),
       ( /*_#IX__L1*/ meltfnum[0]));;
    /*^compute */
 /*_.IMPLBUF__V22*/ meltfptr[21] =
      (melt_ptr_t)
      meltgc_new_strbuf ((meltobject_ptr_t)
			 (( /*!DISCR_STRBUF */ meltfrout->tabval[2])),
			 (const char *) 0);;
    MELT_LOCATION ("warmelt-outobj.melt:4609:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*^rawallocobj */
    /*rawallocobj */
    {
      melt_ptr_t newobj = 0;
      melt_raw_object_create (newobj,
			      (melt_ptr_t) (( /*!CLASS_SECONDARY_C_FILE */
					     meltfrout->tabval[3])), (4),
			      "CLASS_SECONDARY_C_FILE");
  /*_.INST__V24*/ meltfptr[23] =
	newobj;
    };
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @SECFIL_MODNAM",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V24*/ meltfptr[23])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V24*/ meltfptr[23]), (0),
			  ( /*_.MODNAMSTR__V3*/ meltfptr[2]),
			  "SECFIL_MODNAM");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @SECFIL_PATH",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V24*/ meltfptr[23])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V24*/ meltfptr[23]), (1),
			  ( /*_.PATH__V21*/ meltfptr[20]), "SECFIL_PATH");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @SECFIL_DECLBUF",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V24*/ meltfptr[23])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V24*/ meltfptr[23]), (2),
			  ( /*_.DECLBUF__V4*/ meltfptr[3]), "SECFIL_DECLBUF");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @SECFIL_IMPLBUF",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V24*/ meltfptr[23])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V24*/ meltfptr[23]), (3),
			  ( /*_.IMPLBUF__V22*/ meltfptr[21]),
			  "SECFIL_IMPLBUF");
    ;
    /*^touchobj */

    melt_dbgtrace_written_object ( /*_.INST__V24*/ meltfptr[23],
				  "newly made instance");
    ;
    /*_.NEWFILE__V23*/ meltfptr[22] = /*_.INST__V24*/ meltfptr[23];;

    {
      MELT_LOCATION ("warmelt-outobj.melt:4615:/ locexp");
      melt_put_int ((melt_ptr_t) ( /*_.NEWFILE__V23*/ meltfptr[22]),
		    ( /*_#IX__L1*/ meltfnum[0]));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:4616:/ locexp");
      meltgc_multiple_put_nth ((melt_ptr_t)
			       ( /*_.MOFILES__V12*/ meltfptr[11]),
			       ( /*_#IX__L1*/ meltfnum[0]),
			       (melt_ptr_t) ( /*_.NEWFILE__V23*/
					     meltfptr[22]));
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:4617:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.NEWFILE__V23*/ meltfptr[22];;

    {
      MELT_LOCATION ("warmelt-outobj.melt:4617:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*_.LET___V20*/ meltfptr[18] = /*_.RETURN___V25*/ meltfptr[24];;

    MELT_LOCATION ("warmelt-outobj.melt:4603:/ clear");
	   /*clear *//*_.PATH__V21*/ meltfptr[20] = 0;
    /*^clear */
	   /*clear *//*_.IMPLBUF__V22*/ meltfptr[21] = 0;
    /*^clear */
	   /*clear *//*_.NEWFILE__V23*/ meltfptr[22] = 0;
    /*^clear */
	   /*clear *//*_.RETURN___V25*/ meltfptr[24] = 0;
    /*_.LET___V11*/ meltfptr[9] = /*_.LET___V20*/ meltfptr[18];;

    MELT_LOCATION ("warmelt-outobj.melt:4595:/ clear");
	   /*clear *//*_.MOFILES__V12*/ meltfptr[11] = 0;
    /*^clear */
	   /*clear *//*_#NBFILES__L5*/ meltfnum[1] = 0;
    /*^clear */
	   /*clear *//*_.NTHFILE__V13*/ meltfptr[12] = 0;
    /*^clear */
	   /*clear *//*_.IF___V14*/ meltfptr[13] = 0;
    /*^clear */
	   /*clear *//*_#I__L6*/ meltfnum[5] = 0;
    /*^clear */
	   /*clear *//*_.IF___V16*/ meltfptr[14] = 0;
    /*^clear */
	   /*clear *//*_#I__L7*/ meltfnum[6] = 0;
    /*^clear */
	   /*clear *//*_.IF___V18*/ meltfptr[16] = 0;
    /*^clear */
	   /*clear *//*_.LET___V20*/ meltfptr[18] = 0;
    MELT_LOCATION ("warmelt-outobj.melt:4591:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.LET___V11*/ meltfptr[9];;

    {
      MELT_LOCATION ("warmelt-outobj.melt:4591:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V5*/ meltfptr[4] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V7*/ meltfptr[5] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V9*/ meltfptr[7] = 0;
    /*^clear */
	   /*clear *//*_.LET___V11*/ meltfptr[9] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("NTH_SECUNDARY_FILE", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_122_warmelt_outobj_NTH_SECUNDARY_FILE_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_122_warmelt_outobj_NTH_SECUNDARY_FILE */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_123_warmelt_outobj_OUTPUT_MELT_DESCRIPTOR (meltclosure_ptr_t
						    meltclosp_,
						    melt_ptr_t meltfirstargp_,
						    const melt_argdescr_cell_t
						    meltxargdescr_[],
						    union meltparam_un *
						    meltxargtab_,
						    const melt_argdescr_cell_t
						    meltxresdescr_[],
						    union meltparam_un *
						    meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_123_warmelt_outobj_OUTPUT_MELT_DESCRIPTOR_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_123_warmelt_outobj_OUTPUT_MELT_DESCRIPTOR_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 164
    melt_ptr_t mcfr_varptr[164];
#define MELTFRAM_NBVARNUM 41
    long mcfr_varnum[41];
/*others*/
    const char *loc_CSTRING__o0;
    const char *loc_CSTRING__o1;
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_123_warmelt_outobj_OUTPUT_MELT_DESCRIPTOR is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_123_warmelt_outobj_OUTPUT_MELT_DESCRIPTOR_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 164; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_123_warmelt_outobj_OUTPUT_MELT_DESCRIPTOR nbval 164*/
  meltfram__.mcfr_nbvar = 164 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("OUTPUT_MELT_DESCRIPTOR", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-outobj.melt:4625:/ getarg");
 /*_.MODNAMSTR__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.SECFILES__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.SECFILES__V3*/ meltfptr[2])) !=
	      NULL);


  /*getarg#2 */
  /*^getarg */
  if (meltxargdescr_[1] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.MODCTX__V4*/ meltfptr[3] =
    (meltxargtab_[1].meltbp_aptr) ? (*(meltxargtab_[1].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.MODCTX__V4*/ meltfptr[3])) !=
	      NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:4626:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L1*/ meltfnum[0] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-outobj.melt:4626:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L1*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-outobj.melt:4626:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[9];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-outobj.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 4626;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "output_melt_descriptor modnamstr=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.MODNAMSTR__V2*/ meltfptr[1];
	      /*^apply.arg */
	      argtab[5].meltbp_cstring = " secfiles=";
	      /*^apply.arg */
	      argtab[6].meltbp_aptr =
		(melt_ptr_t *) & /*_.SECFILES__V3*/ meltfptr[2];
	      /*^apply.arg */
	      argtab[7].meltbp_cstring = " modctx=";
	      /*^apply.arg */
	      argtab[8].meltbp_aptr =
		(melt_ptr_t *) & /*_.MODCTX__V4*/ meltfptr[3];
	      /*_.MELT_DEBUG_FUN__V7*/ meltfptr[6] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V6*/ meltfptr[5] = /*_.MELT_DEBUG_FUN__V7*/ meltfptr[6];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-outobj.melt:4626:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V7*/ meltfptr[6] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V6*/ meltfptr[5] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-outobj.melt:4626:/ quasiblock");


      /*_.PROGN___V8*/ meltfptr[6] = /*_.IF___V6*/ meltfptr[5];;
      /*^compute */
      /*_.IFCPP___V5*/ meltfptr[4] = /*_.PROGN___V8*/ meltfptr[6];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:4626:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L1*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IF___V6*/ meltfptr[5] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V8*/ meltfptr[6] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V5*/ meltfptr[4] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:4627:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_STRING__L3*/ meltfnum[1] =
	(melt_magic_discr ((melt_ptr_t) ( /*_.MODNAMSTR__V2*/ meltfptr[1])) ==
	 MELTOBMAG_STRING);;
      MELT_LOCATION ("warmelt-outobj.melt:4627:/ cond");
      /*cond */ if ( /*_#IS_STRING__L3*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V10*/ meltfptr[6] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-outobj.melt:4627:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check modnamstr"),
				  ("warmelt-outobj.melt")
				  ? ("warmelt-outobj.melt") : __FILE__,
				  (4627) ? (4627) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V10*/ meltfptr[6] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V9*/ meltfptr[5] = /*_.IFELSE___V10*/ meltfptr[6];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:4627:/ clear");
	     /*clear *//*_#IS_STRING__L3*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V10*/ meltfptr[6] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V9*/ meltfptr[5] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:4628:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L4*/ meltfnum[0] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.MODCTX__V4*/ meltfptr[3]),
			     (melt_ptr_t) (( /*!CLASS_MODULE_CONTEXT */
					    meltfrout->tabval[1])));;
      MELT_LOCATION ("warmelt-outobj.melt:4628:/ cond");
      /*cond */ if ( /*_#IS_A__L4*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V12*/ meltfptr[11] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-outobj.melt:4628:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check modctx"),
				  ("warmelt-outobj.melt")
				  ? ("warmelt-outobj.melt") : __FILE__,
				  (4628) ? (4628) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V12*/ meltfptr[11] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V11*/ meltfptr[6] = /*_.IFELSE___V12*/ meltfptr[11];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:4628:/ clear");
	     /*clear *//*_#IS_A__L4*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V12*/ meltfptr[11] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V11*/ meltfptr[6] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-outobj.melt:4629:/ quasiblock");


 /*_#NBSECFILES__L5*/ meltfnum[1] =
      (melt_multiple_length
       ((melt_ptr_t) ( /*_.SECFILES__V3*/ meltfptr[2])));;
    /*^compute */
 /*_#LASTSECFILEIX__L6*/ meltfnum[0] = 0;;
    /*^compute */
 /*_.DEBUF__V14*/ meltfptr[13] =
      (melt_ptr_t)
      meltgc_new_strbuf ((meltobject_ptr_t)
			 (( /*!DISCR_STRBUF */ meltfrout->tabval[2])),
			 (const char *) 0);;
    /*^compute */
 /*_.TIBUF__V15*/ meltfptr[14] =
      (melt_ptr_t)
      meltgc_new_strbuf ((meltobject_ptr_t)
			 (( /*!DISCR_STRBUF */ meltfrout->tabval[2])),
			 (const char *) 0);;
    /*^compute */
 /*_.MKBUF__V16*/ meltfptr[15] =
      (melt_ptr_t)
      meltgc_new_strbuf ((meltobject_ptr_t)
			 (( /*!DISCR_STRBUF */ meltfrout->tabval[2])),
			 (const char *) 0);;
    /*^compute */
 /*_.PATHLIST__V17*/ meltfptr[16] =
      (meltgc_new_list
       ((meltobject_ptr_t) (( /*!DISCR_LIST */ meltfrout->tabval[3]))));;
    /*^compute */
 /*_.MD5LIST__V18*/ meltfptr[17] =
      (meltgc_new_list
       ((meltobject_ptr_t) (( /*!DISCR_LIST */ meltfrout->tabval[3]))));;
    /*^compute */
 /*_.MODBUILDSTR__V19*/ meltfptr[18] =
      (meltgc_new_string_nakedbasename
       ((meltobject_ptr_t) (( /*!DISCR_STRING */ meltfrout->tabval[4])),
	melt_string_str ((melt_ptr_t) ( /*_.MODNAMSTR__V2*/ meltfptr[1]))));;
    MELT_LOCATION ("warmelt-outobj.melt:4638:/ quasiblock");


 /*_.BUF__V21*/ meltfptr[20] =
      (melt_ptr_t)
      meltgc_new_strbuf ((meltobject_ptr_t)
			 (( /*!DISCR_STRBUF */ meltfrout->tabval[2])),
			 (const char *) 0);;

    {
      MELT_LOCATION ("warmelt-outobj.melt:4640:/ locexp");
      meltgc_add_out_cident ((melt_ptr_t) ( /*_.BUF__V21*/ meltfptr[20]),
			     melt_string_str ((melt_ptr_t)
					      ( /*_.MODNAMSTR__V2*/
					       meltfptr[1])));
    }
    ;
 /*_.STRBUF2STRING__V22*/ meltfptr[21] =
      (meltgc_new_stringdup
       ((meltobject_ptr_t) (( /*!DISCR_STRING */ meltfrout->tabval[4])),
	melt_strbuf_str ((melt_ptr_t) ( /*_.BUF__V21*/ meltfptr[20]))));;
    /*^compute */
    /*_.LET___V20*/ meltfptr[19] = /*_.STRBUF2STRING__V22*/ meltfptr[21];;

    MELT_LOCATION ("warmelt-outobj.melt:4638:/ clear");
	   /*clear *//*_.BUF__V21*/ meltfptr[20] = 0;
    /*^clear */
	   /*clear *//*_.STRBUF2STRING__V22*/ meltfptr[21] = 0;
    /*_.MODCIDENT__V23*/ meltfptr[20] = /*_.LET___V20*/ meltfptr[19];;
    MELT_LOCATION ("warmelt-outobj.melt:4642:/ quasiblock");


 /*_.BUF__V25*/ meltfptr[24] =
      (melt_ptr_t)
      meltgc_new_strbuf ((meltobject_ptr_t)
			 (( /*!DISCR_STRBUF */ meltfrout->tabval[2])),
			 (const char *) 0);;

    {
      MELT_LOCATION ("warmelt-outobj.melt:4644:/ locexp");
      meltgc_add_out_cident ((melt_ptr_t) ( /*_.BUF__V25*/ meltfptr[24]),
			     melt_string_str ((melt_ptr_t)
					      ( /*_.MODBUILDSTR__V19*/
					       meltfptr[18])));
    }
    ;
 /*_.STRBUF2STRING__V26*/ meltfptr[25] =
      (meltgc_new_stringdup
       ((meltobject_ptr_t) (( /*!DISCR_STRING */ meltfrout->tabval[4])),
	melt_strbuf_str ((melt_ptr_t) ( /*_.BUF__V25*/ meltfptr[24]))));;
    /*^compute */
    /*_.LET___V24*/ meltfptr[21] = /*_.STRBUF2STRING__V26*/ meltfptr[25];;

    MELT_LOCATION ("warmelt-outobj.melt:4642:/ clear");
	   /*clear *//*_.BUF__V25*/ meltfptr[24] = 0;
    /*^clear */
	   /*clear *//*_.STRBUF2STRING__V26*/ meltfptr[25] = 0;
    /*_.MODBUILDCIDENT__V27*/ meltfptr[24] = /*_.LET___V24*/ meltfptr[21];;
    MELT_LOCATION ("warmelt-outobj.melt:4646:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[2];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_cstring = "$(GCCMELTGEN_BUILD)";
      /*^apply.arg */
      argtab[1].meltbp_aptr =
	(melt_ptr_t *) & /*_.MODBUILDSTR__V19*/ meltfptr[18];
      /*_.MODPREFSTR__V28*/ meltfptr[25] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!STRING4OUT */ meltfrout->tabval[5])),
		    (melt_ptr_t) (( /*!DISCR_STRING */ meltfrout->tabval[4])),
		    (MELTBPARSTR_CSTRING MELTBPARSTR_PTR ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:4647:/ cond");
    /*cond */ if (
		   /*ifisa */
		   melt_is_instance_of ((melt_ptr_t)
					( /*_.MODCTX__V4*/ meltfptr[3]),
					(melt_ptr_t) (( /*!CLASS_MODULE_CONTEXT */ meltfrout->tabval[1])))
      )				/*then */
      {
	/*^cond.then */
	/*^getslot */
	{
	  melt_ptr_t slot = NULL, obj = NULL;
	  obj = (melt_ptr_t) ( /*_.MODCTX__V4*/ meltfptr[3]) /*=obj*/ ;
	  melt_object_get_field (slot, obj, 10, "MOCX_PACKAGEPCLIST");
   /*_.PACKAGEPCLIST__V29*/ meltfptr[28] = slot;
	};
	;
      }
    else
      {				/*^cond.else */

  /*_.PACKAGEPCLIST__V29*/ meltfptr[28] = NULL;;
      }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:4649:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#IS_A__L7*/ meltfnum[6] =
      melt_is_instance_of ((melt_ptr_t) ( /*_.MODCTX__V4*/ meltfptr[3]),
			   (melt_ptr_t) (( /*!CLASS_RUNNING_EXTENSION_MODULE_CONTEXT */ meltfrout->tabval[6])));;
    MELT_LOCATION ("warmelt-outobj.melt:4649:/ cond");
    /*cond */ if ( /*_#IS_A__L7*/ meltfnum[6])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-outobj.melt:4650:/ blockmultialloc");
	  /*multiallocblock */
	  {
	    struct meltletrec_1_st
	    {
	      struct MELT_MULTIPLE_STRUCT (2) rtup_0__TUPLREC__x1;
	      long meltletrec_1_endgap;
	    } *meltletrec_1_ptr = 0;
	    meltletrec_1_ptr =
	      (struct meltletrec_1_st *)
	      meltgc_allocate (sizeof (struct meltletrec_1_st), 0);
	    /*^blockmultialloc.initfill */
	    /*inimult rtup_0__TUPLREC__x1 */
 /*_.TUPLREC___V32*/ meltfptr[31] =
	      (melt_ptr_t) & meltletrec_1_ptr->rtup_0__TUPLREC__x1;
	    meltletrec_1_ptr->rtup_0__TUPLREC__x1.discr =
	      (meltobject_ptr_t) (((melt_ptr_t)
				   (MELT_PREDEF (DISCR_MULTIPLE))));
	    meltletrec_1_ptr->rtup_0__TUPLREC__x1.nbval = 2;


	    /*^putuple */
	    /*putupl#1 */
	    melt_assertmsg ("putupl [:4650] #1 checktup",
			    melt_magic_discr ((melt_ptr_t)
					      ( /*_.TUPLREC___V32*/
					       meltfptr[31])) ==
			    MELTOBMAG_MULTIPLE);
	    melt_assertmsg ("putupl [:4650] #1 checkoff",
			    (0 >= 0
			     && 0 <
			     melt_multiple_length ((melt_ptr_t)
						   ( /*_.TUPLREC___V32*/
						    meltfptr[31]))));
	    ((meltmultiple_ptr_t) ( /*_.TUPLREC___V32*/ meltfptr[31]))->
	      tabval[0] =
	      (melt_ptr_t) (( /*!konst_7 */ meltfrout->tabval[7]));
	    ;
	    /*^putuple */
	    /*putupl#2 */
	    melt_assertmsg ("putupl [:4650] #2 checktup",
			    melt_magic_discr ((melt_ptr_t)
					      ( /*_.TUPLREC___V32*/
					       meltfptr[31])) ==
			    MELTOBMAG_MULTIPLE);
	    melt_assertmsg ("putupl [:4650] #2 checkoff",
			    (1 >= 0
			     && 1 <
			     melt_multiple_length ((melt_ptr_t)
						   ( /*_.TUPLREC___V32*/
						    meltfptr[31]))));
	    ((meltmultiple_ptr_t) ( /*_.TUPLREC___V32*/ meltfptr[31]))->
	      tabval[1] =
	      (melt_ptr_t) (( /*!konst_8 */ meltfrout->tabval[8]));
	    ;
	    /*^touch */
	    meltgc_touch ( /*_.TUPLREC___V32*/ meltfptr[31]);
	    ;
	    /*_.TUPLE___V31*/ meltfptr[30] =
	      /*_.TUPLREC___V32*/ meltfptr[31];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-outobj.melt:4650:/ clear");
	      /*clear *//*_.TUPLREC___V32*/ meltfptr[31] = 0;
	    /*^clear */
	      /*clear *//*_.TUPLREC___V32*/ meltfptr[31] = 0;
	  }			/*end multiallocblock */
	  ;
	  /*_.FLAVORTUPLE__V30*/ meltfptr[29] =
	    /*_.TUPLE___V31*/ meltfptr[30];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-outobj.melt:4649:/ clear");
	     /*clear *//*_.TUPLE___V31*/ meltfptr[30] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-outobj.melt:4651:/ blockmultialloc");
	  /*multiallocblock */
	  {
	    struct meltletrec_2_st
	    {
	      struct MELT_MULTIPLE_STRUCT (4) rtup_0__TUPLREC__x2;
	      long meltletrec_2_endgap;
	    } *meltletrec_2_ptr = 0;
	    meltletrec_2_ptr =
	      (struct meltletrec_2_st *)
	      meltgc_allocate (sizeof (struct meltletrec_2_st), 0);
	    /*^blockmultialloc.initfill */
	    /*inimult rtup_0__TUPLREC__x2 */
 /*_.TUPLREC___V34*/ meltfptr[30] =
	      (melt_ptr_t) & meltletrec_2_ptr->rtup_0__TUPLREC__x2;
	    meltletrec_2_ptr->rtup_0__TUPLREC__x2.discr =
	      (meltobject_ptr_t) (((melt_ptr_t)
				   (MELT_PREDEF (DISCR_MULTIPLE))));
	    meltletrec_2_ptr->rtup_0__TUPLREC__x2.nbval = 4;


	    /*^putuple */
	    /*putupl#3 */
	    melt_assertmsg ("putupl [:4651] #3 checktup",
			    melt_magic_discr ((melt_ptr_t)
					      ( /*_.TUPLREC___V34*/
					       meltfptr[30])) ==
			    MELTOBMAG_MULTIPLE);
	    melt_assertmsg ("putupl [:4651] #3 checkoff",
			    (0 >= 0
			     && 0 <
			     melt_multiple_length ((melt_ptr_t)
						   ( /*_.TUPLREC___V34*/
						    meltfptr[30]))));
	    ((meltmultiple_ptr_t) ( /*_.TUPLREC___V34*/ meltfptr[30]))->
	      tabval[0] =
	      (melt_ptr_t) (( /*!konst_9 */ meltfrout->tabval[9]));
	    ;
	    /*^putuple */
	    /*putupl#4 */
	    melt_assertmsg ("putupl [:4651] #4 checktup",
			    melt_magic_discr ((melt_ptr_t)
					      ( /*_.TUPLREC___V34*/
					       meltfptr[30])) ==
			    MELTOBMAG_MULTIPLE);
	    melt_assertmsg ("putupl [:4651] #4 checkoff",
			    (1 >= 0
			     && 1 <
			     melt_multiple_length ((melt_ptr_t)
						   ( /*_.TUPLREC___V34*/
						    meltfptr[30]))));
	    ((meltmultiple_ptr_t) ( /*_.TUPLREC___V34*/ meltfptr[30]))->
	      tabval[1] =
	      (melt_ptr_t) (( /*!konst_10 */ meltfrout->tabval[10]));
	    ;
	    /*^putuple */
	    /*putupl#5 */
	    melt_assertmsg ("putupl [:4651] #5 checktup",
			    melt_magic_discr ((melt_ptr_t)
					      ( /*_.TUPLREC___V34*/
					       meltfptr[30])) ==
			    MELTOBMAG_MULTIPLE);
	    melt_assertmsg ("putupl [:4651] #5 checkoff",
			    (2 >= 0
			     && 2 <
			     melt_multiple_length ((melt_ptr_t)
						   ( /*_.TUPLREC___V34*/
						    meltfptr[30]))));
	    ((meltmultiple_ptr_t) ( /*_.TUPLREC___V34*/ meltfptr[30]))->
	      tabval[2] =
	      (melt_ptr_t) (( /*!konst_11 */ meltfrout->tabval[11]));
	    ;
	    /*^putuple */
	    /*putupl#6 */
	    melt_assertmsg ("putupl [:4651] #6 checktup",
			    melt_magic_discr ((melt_ptr_t)
					      ( /*_.TUPLREC___V34*/
					       meltfptr[30])) ==
			    MELTOBMAG_MULTIPLE);
	    melt_assertmsg ("putupl [:4651] #6 checkoff",
			    (3 >= 0
			     && 3 <
			     melt_multiple_length ((melt_ptr_t)
						   ( /*_.TUPLREC___V34*/
						    meltfptr[30]))));
	    ((meltmultiple_ptr_t) ( /*_.TUPLREC___V34*/ meltfptr[30]))->
	      tabval[3] =
	      (melt_ptr_t) (( /*!konst_12 */ meltfrout->tabval[12]));
	    ;
	    /*^touch */
	    meltgc_touch ( /*_.TUPLREC___V34*/ meltfptr[30]);
	    ;
	    /*_.TUPLE___V33*/ meltfptr[31] =
	      /*_.TUPLREC___V34*/ meltfptr[30];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-outobj.melt:4651:/ clear");
	      /*clear *//*_.TUPLREC___V34*/ meltfptr[30] = 0;
	    /*^clear */
	      /*clear *//*_.TUPLREC___V34*/ meltfptr[30] = 0;
	  }			/*end multiallocblock */
	  ;
	  /*_.FLAVORTUPLE__V30*/ meltfptr[29] =
	    /*_.TUPLE___V33*/ meltfptr[31];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-outobj.melt:4649:/ clear");
	     /*clear *//*_.TUPLE___V33*/ meltfptr[31] = 0;
	}
	;
      }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:4653:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.DEBUF__V14*/ meltfptr[13]),
			   ("/** GENERATED MELT DESCRIPTOR FILE "));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:4654:/ locexp");
      meltgc_add_strbuf_ccomment ((melt_ptr_t)
				  ( /*_.DEBUF__V14*/ meltfptr[13]),
				  melt_string_str ((melt_ptr_t)
						   ( /*_.MODNAMSTR__V2*/
						    meltfptr[1])));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:4655:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.DEBUF__V14*/ meltfptr[13]),
			   ("+meltdesc.c \n** NEVER EDIT OR MOVE THIS, IT IS GENERATED & PARSED\
! **/"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:4656:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.DEBUF__V14*/ meltfptr[13]),
				(0), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:4657:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.DEBUF__V14*/ meltfptr[13]),
			   ("/* These identifiers are generated in warmelt-outobj.melt \
\n & handled in melt-runtime.c carefully. */"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:4658:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.DEBUF__V14*/ meltfptr[13]),
				(0), 0);
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:4659:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[3];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_cstring = "## GENERATED MELT MAKE FRAGMENT FILE ";
      /*^apply.arg */
      argtab[1].meltbp_aptr =
	(melt_ptr_t *) & /*_.MODNAMSTR__V2*/ meltfptr[1];
      /*^apply.arg */
      argtab[2].meltbp_cstring = "+meltbuild.mk\n";
      /*_.ADD2OUT__V35*/ meltfptr[30] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!ADD2OUT */ meltfrout->tabval[13])),
		    (melt_ptr_t) ( /*_.MKBUF__V16*/ meltfptr[15]),
		    (MELTBPARSTR_CSTRING MELTBPARSTR_PTR MELTBPARSTR_CSTRING
		     ""), argtab, "", (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:4660:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_cstring =
	"## NEVER EDIT THIS FILE, generated from warmelt-outobj.melt by output_melt_de\
scriptor\n";
      /*_.ADD2OUT__V36*/ meltfptr[31] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!ADD2OUT */ meltfrout->tabval[13])),
		    (melt_ptr_t) ( /*_.MKBUF__V16*/ meltfptr[15]),
		    (MELTBPARSTR_CSTRING ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:4661:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_cstring =
	"\n\t     #ifdef __cplusplus\n\t     /* explicitly declare as extern\
 \"C\" our dlsym-ed symbols */\n\t     extern \"C\" const char melt_versionme\
ltstr[]\t    ;\n\t     extern \"C\" const char melt_genversionstr[\
]\t\t    ;\n\t     extern \"C\" const char melt_modulename[]\t\t  \
  ;\n\t     extern \"C\" const char melt_modulerealpath[]\t    ;\
\n\t     extern \"C\" const char melt_prepromd5meltrun[]\t    ;\
\n\t     extern \"C\" const char melt_primaryhexmd5[]\t\t    ;\
\n\t     extern \"C\" const char* const melt_secondaryhexmd5tab[] \
;\n\t     extern \"C\" const int melt_lastsecfileindex\t\t    ;\
\n\t     extern \"C\" const char melt_cumulated_hexmd5[]\t    ;\
\n\n\t     extern \"C\" {\n\
\t     #endif /*__cplusplus */\n\t     ";
      /*_.ADD2OUT__V37*/ meltfptr[36] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!ADD2OUT */ meltfrout->tabval[13])),
		    (melt_ptr_t) ( /*_.DEBUF__V14*/ meltfptr[13]),
		    (MELTBPARSTR_CSTRING ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:4677:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.DEBUF__V14*/ meltfptr[13]),
				(0), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:4678:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.DEBUF__V14*/ meltfptr[13]),
			   ("/* version of the GCC compiler & MELT runtime generating this */"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:4679:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.DEBUF__V14*/ meltfptr[13]),
				(0), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:4680:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.DEBUF__V14*/ meltfptr[13]),
			   ("const char melt_genversionstr[]=\""));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:4683:/ locexp");
      /* output_melt_descriptor GENVERSCH__1 + */
      meltgc_add_strbuf_cstr ((melt_ptr_t) /*_.DEBUF__V14*/ meltfptr[13],
			      melt_gccversionstr);
      ;
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:4686:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.DEBUF__V14*/ meltfptr[13]),
			   ("\""));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:4687:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.DEBUF__V14*/ meltfptr[13]),
				(0), 0);
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:4688:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_cstring =
	"\n\t     #ifdef __cplusplus\n\t     \" (in C++)\"\
\n\t     #else\n\t     \" (in C)\"\
\n\t     #endif\n\t\t\
\t\t\t;\n\t     ";
      /*_.ADD2OUT__V38*/ meltfptr[37] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!ADD2OUT */ meltfrout->tabval[13])),
		    (melt_ptr_t) ( /*_.DEBUF__V14*/ meltfptr[13]),
		    (MELTBPARSTR_CSTRING ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:4696:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.DEBUF__V14*/ meltfptr[13]),
				(0), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:4697:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.DEBUF__V14*/ meltfptr[13]),
			   ("const char melt_versionmeltstr[]=\""));
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:4698:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_cstring = "## generated by MELT version ";
      /*_.ADD2OUT__V39*/ meltfptr[38] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!ADD2OUT */ meltfrout->tabval[13])),
		    (melt_ptr_t) ( /*_.MKBUF__V16*/ meltfptr[15]),
		    (MELTBPARSTR_CSTRING ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:4701:/ locexp");
      /* output_melt_descriptor GENVMELTCH__1 + */
      meltgc_add_strbuf_cstr ((melt_ptr_t) /*_.DEBUF__V14*/ meltfptr[13],
			      melt_version_str ());
      meltgc_add_strbuf_cstr ((melt_ptr_t) /*_.MKBUF__V16*/ meltfptr[15],
			      melt_version_str ());
      ;
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:4705:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.DEBUF__V14*/ meltfptr[13]),
			   ("\";"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:4706:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.DEBUF__V14*/ meltfptr[13]),
				(0), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:4707:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.MKBUF__V16*/ meltfptr[15]),
				(0), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:4709:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.DEBUF__V14*/ meltfptr[13]),
				(0), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:4710:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.DEBUF__V14*/ meltfptr[13]),
			   ("/* source name & real path of the module */"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:4711:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.DEBUF__V14*/ meltfptr[13]),
				(0), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:4712:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.DEBUF__V14*/ meltfptr[13]),
			   ("/*MELTMODULENAME "));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:4713:/ locexp");
      meltgc_add_strbuf_ccomment ((melt_ptr_t)
				  ( /*_.DEBUF__V14*/ meltfptr[13]),
				  melt_string_str ((melt_ptr_t)
						   ( /*_.MODNAMSTR__V2*/
						    meltfptr[1])));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:4714:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.DEBUF__V14*/ meltfptr[13]),
			   (" */"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:4715:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.DEBUF__V14*/ meltfptr[13]),
				(0), 0);
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:4716:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_cstring = "#module name and identifier\n";
      /*_.ADD2OUT__V40*/ meltfptr[39] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!ADD2OUT */ meltfrout->tabval[13])),
		    (melt_ptr_t) ( /*_.MKBUF__V16*/ meltfptr[15]),
		    (MELTBPARSTR_CSTRING ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:4717:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[2];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_cstring = "MELTGEN_MODULENAME=";
      /*^apply.arg */
      argtab[1].meltbp_aptr =
	(melt_ptr_t *) & /*_.MODNAMSTR__V2*/ meltfptr[1];
      /*_.ADD2OUT__V41*/ meltfptr[40] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!ADD2OUT */ meltfrout->tabval[13])),
		    (melt_ptr_t) ( /*_.MKBUF__V16*/ meltfptr[15]),
		    (MELTBPARSTR_CSTRING MELTBPARSTR_PTR ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:4718:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.MKBUF__V16*/ meltfptr[15]),
				(0), 0);
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:4719:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[2];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_cstring = "MELTGEN_MODULEIDENT=";
      /*^apply.arg */
      argtab[1].meltbp_aptr =
	(melt_ptr_t *) & /*_.MODCIDENT__V23*/ meltfptr[20];
      /*_.ADD2OUT__V42*/ meltfptr[41] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!ADD2OUT */ meltfrout->tabval[13])),
		    (melt_ptr_t) ( /*_.MKBUF__V16*/ meltfptr[15]),
		    (MELTBPARSTR_CSTRING MELTBPARSTR_PTR ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:4720:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.MKBUF__V16*/ meltfptr[15]),
				(0), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:4721:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.DEBUF__V14*/ meltfptr[13]),
			   ("const char melt_modulename[]=\""));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:4724:/ locexp");
      /* output_melt_descriptor GENMODNAMSTR__1 + */
      meltgc_add_strbuf_cstr ((melt_ptr_t) /*_.DEBUF__V14*/ meltfptr[13],
			      lbasename (melt_string_str
					 ((melt_ptr_t) /*_.MODNAMSTR__V2*/
					  meltfptr[1])));
      ;
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:4729:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.DEBUF__V14*/ meltfptr[13]),
			   ("\";"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:4730:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.DEBUF__V14*/ meltfptr[13]),
				(0), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:4731:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.DEBUF__V14*/ meltfptr[13]),
			   ("const char melt_modulerealpath[]=\""));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:4734:/ locexp");
      /* output_melt_descriptor GENRPATHCH__1 + */
      {
	char *lrp =
	  lrealpath (melt_string_str
		     ((melt_ptr_t) /*_.MODNAMSTR__V2*/ meltfptr[1]));
	if (!melt_flag_bootstrapping && !IS_ABSOLUTE_PATH (lrp))
	  {
	    meltgc_add_strbuf_cstr ((melt_ptr_t) /*_.DEBUF__V14*/
				    meltfptr[13], getpwd ());
	    meltgc_add_strbuf ((melt_ptr_t) /*_.DEBUF__V14*/ meltfptr[13],
			       "/");
	    meltgc_add_strbuf_cstr ((melt_ptr_t) /*_.DEBUF__V14*/
				    meltfptr[13], lrp);
	  }
	else if (melt_flag_bootstrapping)
	  {
	    meltgc_add_strbuf_cstr ((melt_ptr_t) /*_.DEBUF__V14*/
				    meltfptr[13], melt_module_dir);
	    meltgc_add_strbuf ((melt_ptr_t) /*_.DEBUF__V14*/ meltfptr[13],
			       "/");
	    meltgc_add_strbuf_cstr ((melt_ptr_t) /*_.DEBUF__V14*/
				    meltfptr[13], lbasename (lrp));
	  }
	else
	  meltgc_add_strbuf_cstr ((melt_ptr_t) /*_.DEBUF__V14*/ meltfptr[13],
				  lrp);
	free (lrp);
      } /* end output_melt_descriptor GENRPATHCH__1 */ ;
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:4750:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.DEBUF__V14*/ meltfptr[13]),
			   ("\";"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:4751:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.DEBUF__V14*/ meltfptr[13]),
				(0), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:4752:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.DEBUF__V14*/ meltfptr[13]),
				(0), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:4753:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.DEBUF__V14*/ meltfptr[13]),
			   ("/* hash of preprocessed melt-run.h generating this */"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:4754:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.DEBUF__V14*/ meltfptr[13]),
				(0), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:4755:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.DEBUF__V14*/ meltfptr[13]),
			   ("const char melt_prepromd5meltrun[]=\""));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:4758:/ locexp");
      /* output_melt_descriptor GENVERSCH__2 */
      meltgc_add_strbuf_cstr ((melt_ptr_t) /*_.DEBUF__V14*/ meltfptr[13],
			      MELT_RUN_HASHMD5);
      ;
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:4761:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.DEBUF__V14*/ meltfptr[13]),
			   ("\";"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:4762:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.DEBUF__V14*/ meltfptr[13]),
				(0), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:4764:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.DEBUF__V14*/ meltfptr[13]),
			   ("/* hexmd5checksum of primary C file */"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:4765:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.DEBUF__V14*/ meltfptr[13]),
				(0), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:4766:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.DEBUF__V14*/ meltfptr[13]),
			   ("const char melt_primaryhexmd5[]=\""));
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:4767:/ quasiblock");


    MELT_LOCATION ("warmelt-outobj.melt:4768:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[2];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & /*_.MODNAMSTR__V2*/ meltfptr[1];
      /*^apply.arg */
      argtab[1].meltbp_cstring = ".c";
      /*_.PRIMPATH__V44*/ meltfptr[43] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!STRING4OUT */ meltfrout->tabval[5])),
		    (melt_ptr_t) (( /*!DISCR_STRING */ meltfrout->tabval[4])),
		    (MELTBPARSTR_PTR MELTBPARSTR_CSTRING ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;
 /*_.PRIMMD5S__V45*/ meltfptr[44] =
      (meltgc_string_hex_md5sum_file
       (melt_string_str ((melt_ptr_t) /*_.PRIMPATH__V44*/ meltfptr[43])));;
    /*^compute */
 /*_.MKRULEBUF__V46*/ meltfptr[45] =
      (melt_ptr_t)
      meltgc_new_strbuf ((meltobject_ptr_t)
			 (( /*!DISCR_STRBUF */ meltfrout->tabval[2])),
			 (const char *) 0);;
    /*^compute */
 /*_.SECMD5BUF__V47*/ meltfptr[46] =
      (melt_ptr_t)
      meltgc_new_strbuf ((meltobject_ptr_t)
			 (( /*!DISCR_STRBUF */ meltfrout->tabval[2])),
			 (const char *) 0);;
    /*^compute */
 /*_.SECPATHBUF__V48*/ meltfptr[47] =
      (melt_ptr_t)
      meltgc_new_strbuf ((meltobject_ptr_t)
			 (( /*!DISCR_STRBUF */ meltfrout->tabval[2])),
			 (const char *) 0);;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:4774:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L8*/ meltfnum[7] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-outobj.melt:4774:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L8*/ meltfnum[7])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L9*/ meltfnum[8] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-outobj.melt:4774:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L9*/ meltfnum[8];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-outobj.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 4774;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "output_melt_descriptor primpath=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.PRIMPATH__V44*/ meltfptr[43];
	      /*_.MELT_DEBUG_FUN__V51*/ meltfptr[50] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V50*/ meltfptr[49] =
	      /*_.MELT_DEBUG_FUN__V51*/ meltfptr[50];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-outobj.melt:4774:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L9*/ meltfnum[8] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V51*/ meltfptr[50] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V50*/ meltfptr[49] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-outobj.melt:4774:/ quasiblock");


      /*_.PROGN___V52*/ meltfptr[50] = /*_.IF___V50*/ meltfptr[49];;
      /*^compute */
      /*_.IFCPP___V49*/ meltfptr[48] = /*_.PROGN___V52*/ meltfptr[50];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:4774:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L8*/ meltfnum[7] = 0;
      /*^clear */
	     /*clear *//*_.IF___V50*/ meltfptr[49] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V52*/ meltfptr[50] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V49*/ meltfptr[48] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:4775:/ locexp");
      meltgc_append_list ((melt_ptr_t) ( /*_.PATHLIST__V17*/ meltfptr[16]),
			  (melt_ptr_t) ( /*_.PRIMPATH__V44*/ meltfptr[43]));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:4776:/ locexp");
      meltgc_append_list ((melt_ptr_t) ( /*_.MD5LIST__V18*/ meltfptr[17]),
			  (melt_ptr_t) ( /*_.PRIMMD5S__V45*/ meltfptr[44]));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:4777:/ locexp");
      meltgc_add_strbuf_cstr ((melt_ptr_t) ( /*_.DEBUF__V14*/ meltfptr[13]),
			      melt_string_str ((melt_ptr_t)
					       ( /*_.PRIMMD5S__V45*/
						meltfptr[44])));
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:4778:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_cstring = "#primary path and checksum\n";
      /*_.ADD2OUT__V53*/ meltfptr[49] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!ADD2OUT */ meltfrout->tabval[13])),
		    (melt_ptr_t) ( /*_.MKBUF__V16*/ meltfptr[15]),
		    (MELTBPARSTR_CSTRING ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:4779:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[4];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_cstring = "MELTGENMOD_PRIMPATH_";
      /*^apply.arg */
      argtab[1].meltbp_aptr =
	(melt_ptr_t *) & /*_.MODCIDENT__V23*/ meltfptr[20];
      /*^apply.arg */
      argtab[2].meltbp_cstring = "=";
      /*^apply.arg */
      argtab[3].meltbp_aptr =
	(melt_ptr_t *) & /*_.PRIMPATH__V44*/ meltfptr[43];
      /*_.ADD2OUT__V54*/ meltfptr[50] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!ADD2OUT */ meltfrout->tabval[13])),
		    (melt_ptr_t) ( /*_.MKBUF__V16*/ meltfptr[15]),
		    (MELTBPARSTR_CSTRING MELTBPARSTR_PTR MELTBPARSTR_CSTRING
		     MELTBPARSTR_PTR ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:4780:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.MKBUF__V16*/ meltfptr[15]),
				(0), 0);
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:4781:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[4];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_cstring = "MELTGENMOD_MD5PRIM_";
      /*^apply.arg */
      argtab[1].meltbp_aptr =
	(melt_ptr_t *) & /*_.MODCIDENT__V23*/ meltfptr[20];
      /*^apply.arg */
      argtab[2].meltbp_cstring = "=";
      /*^apply.arg */
      argtab[3].meltbp_aptr =
	(melt_ptr_t *) & /*_.PRIMMD5S__V45*/ meltfptr[44];
      /*_.ADD2OUT__V55*/ meltfptr[54] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!ADD2OUT */ meltfrout->tabval[13])),
		    (melt_ptr_t) ( /*_.MKBUF__V16*/ meltfptr[15]),
		    (MELTBPARSTR_CSTRING MELTBPARSTR_PTR MELTBPARSTR_CSTRING
		     MELTBPARSTR_PTR ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:4782:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.MKBUF__V16*/ meltfptr[15]),
				(0), 0);
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:4784:/ quasiblock");


    /*^checksignal */
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & ( /*!DISCR_MULTIPLE */ meltfrout->tabval[15]);
      /*_.PACKAGEPCTUP__V57*/ meltfptr[56] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!LIST_TO_MULTIPLE */ meltfrout->tabval[14])),
		    (melt_ptr_t) ( /*_.PACKAGEPCLIST__V29*/ meltfptr[28]),
		    (MELTBPARSTR_PTR ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;
 /*_#NBPACKAGEPC__L10*/ meltfnum[8] =
      (melt_multiple_length
       ((melt_ptr_t) ( /*_.PACKAGEPCTUP__V57*/ meltfptr[56])));;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:4787:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L11*/ meltfnum[7] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-outobj.melt:4787:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L11*/ meltfnum[7])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L12*/ meltfnum[11] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-outobj.melt:4787:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long =
		/*_#THE_MELTCALLCOUNT__L12*/ meltfnum[11];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-outobj.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 4787;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring =
		"output_melt_descriptor packagepctup=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.PACKAGEPCTUP__V57*/ meltfptr[56];
	      /*_.MELT_DEBUG_FUN__V60*/ meltfptr[59] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V59*/ meltfptr[58] =
	      /*_.MELT_DEBUG_FUN__V60*/ meltfptr[59];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-outobj.melt:4787:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L12*/ meltfnum[11] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V60*/ meltfptr[59] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V59*/ meltfptr[58] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-outobj.melt:4787:/ quasiblock");


      /*_.PROGN___V61*/ meltfptr[59] = /*_.IF___V59*/ meltfptr[58];;
      /*^compute */
      /*_.IFCPP___V58*/ meltfptr[57] = /*_.PROGN___V61*/ meltfptr[59];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:4787:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L11*/ meltfnum[7] = 0;
      /*^clear */
	     /*clear *//*_.IF___V59*/ meltfptr[58] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V61*/ meltfptr[59] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V58*/ meltfptr[57] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-outobj.melt:4788:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^cond */
    /*cond */ if ( /*_#NBPACKAGEPC__L10*/ meltfnum[8])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-outobj.melt:4789:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^apply */
	  /*apply */
	  {
	    union meltparam_un argtab[3];
	    memset (&argtab, 0, sizeof (argtab));
	    /*^apply.arg */
	    argtab[0].meltbp_cstring = "# ";
	    /*^apply.arg */
	    argtab[1].meltbp_long = /*_#NBPACKAGEPC__L10*/ meltfnum[8];
	    /*^apply.arg */
	    argtab[2].meltbp_cstring = " packages for pkg-config utility\n";
	    /*_.ADD2OUT__V63*/ meltfptr[59] =
	      melt_apply ((meltclosure_ptr_t)
			  (( /*!ADD2OUT */ meltfrout->tabval[13])),
			  (melt_ptr_t) ( /*_.MKBUF__V16*/ meltfptr[15]),
			  (MELTBPARSTR_CSTRING MELTBPARSTR_LONG
			   MELTBPARSTR_CSTRING ""), argtab, "",
			  (union meltparam_un *) 0);
	  }
	  ;
	  MELT_LOCATION ("warmelt-outobj.melt:4790:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^apply */
	  /*apply */
	  {
	    union meltparam_un argtab[3];
	    memset (&argtab, 0, sizeof (argtab));
	    /*^apply.arg */
	    argtab[0].meltbp_cstring = "MELTGENMOD_PACKAGELIST_PKGCONFIG_";
	    /*^apply.arg */
	    argtab[1].meltbp_aptr =
	      (melt_ptr_t *) & /*_.MODCIDENT__V23*/ meltfptr[20];
	    /*^apply.arg */
	    argtab[2].meltbp_cstring = "=";
	    /*_.ADD2OUT__V64*/ meltfptr[63] =
	      melt_apply ((meltclosure_ptr_t)
			  (( /*!ADD2OUT */ meltfrout->tabval[13])),
			  (melt_ptr_t) ( /*_.MKBUF__V16*/ meltfptr[15]),
			  (MELTBPARSTR_CSTRING MELTBPARSTR_PTR
			   MELTBPARSTR_CSTRING ""), argtab, "",
			  (union meltparam_un *) 0);
	  }
	  ;
	  /*citerblock FOREACH_IN_MULTIPLE */
	  {
	    /* start foreach_in_multiple meltcit1__EACHTUP */
	    long meltcit1__EACHTUP_ln =
	      melt_multiple_length ((melt_ptr_t) /*_.PACKAGEPCTUP__V57*/
				    meltfptr[56]);
	    for ( /*_#PKIX__L13*/ meltfnum[11] = 0;
		 ( /*_#PKIX__L13*/ meltfnum[11] >= 0)
		 && ( /*_#PKIX__L13*/ meltfnum[11] < meltcit1__EACHTUP_ln);
	/*_#PKIX__L13*/ meltfnum[11]++)
	      {
		/*_.CURPACK__V65*/ meltfptr[64] =
		  melt_multiple_nth ((melt_ptr_t)
				     ( /*_.PACKAGEPCTUP__V57*/ meltfptr[56]),
				     /*_#PKIX__L13*/ meltfnum[11]);



		MELT_LOCATION ("warmelt-outobj.melt:4794:/ checksignal");
		MELT_CHECK_SIGNAL ();
		;
		/*^apply */
		/*apply */
		{
		  union meltparam_un argtab[2];
		  memset (&argtab, 0, sizeof (argtab));
		  /*^apply.arg */
		  argtab[0].meltbp_cstring = " ";
		  /*^apply.arg */
		  argtab[1].meltbp_aptr =
		    (melt_ptr_t *) & /*_.CURPACK__V65*/ meltfptr[64];
		  /*_.ADD2OUT__V66*/ meltfptr[65] =
		    melt_apply ((meltclosure_ptr_t)
				(( /*!ADD2OUT */ meltfrout->tabval[13])),
				(melt_ptr_t) ( /*_.MKBUF__V16*/ meltfptr[15]),
				(MELTBPARSTR_CSTRING MELTBPARSTR_PTR ""),
				argtab, "", (union meltparam_un *) 0);
		}
		;
		if ( /*_#PKIX__L13*/ meltfnum[11] < 0)
		  break;
	      }			/* end  foreach_in_multiple meltcit1__EACHTUP */

	    /*citerepilog */

	    MELT_LOCATION ("warmelt-outobj.melt:4791:/ clear");
	      /*clear *//*_.CURPACK__V65*/ meltfptr[64] = 0;
	    /*^clear */
	      /*clear *//*_#PKIX__L13*/ meltfnum[11] = 0;
	    /*^clear */
	      /*clear *//*_.ADD2OUT__V66*/ meltfptr[65] = 0;
	  }			/*endciterblock FOREACH_IN_MULTIPLE */
	  ;
	  MELT_LOCATION ("warmelt-outobj.melt:4795:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^apply */
	  /*apply */
	  {
	    union meltparam_un argtab[1];
	    memset (&argtab, 0, sizeof (argtab));
	    /*^apply.arg */
	    argtab[0].meltbp_cstring =
	      "\n# global name for these pkg-config packages:\n";
	    /*_.ADD2OUT__V67*/ meltfptr[66] =
	      melt_apply ((meltclosure_ptr_t)
			  (( /*!ADD2OUT */ meltfrout->tabval[13])),
			  (melt_ptr_t) ( /*_.MKBUF__V16*/ meltfptr[15]),
			  (MELTBPARSTR_CSTRING ""), argtab, "",
			  (union meltparam_un *) 0);
	  }
	  ;
	  MELT_LOCATION ("warmelt-outobj.melt:4796:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^apply */
	  /*apply */
	  {
	    union meltparam_un argtab[3];
	    memset (&argtab, 0, sizeof (argtab));
	    /*^apply.arg */
	    argtab[0].meltbp_cstring =
	      "MELTGENMOD_PACKAGELIST=$(MELTGENMOD_PACKAGELIST_PKGCONFIG_";
	    /*^apply.arg */
	    argtab[1].meltbp_aptr =
	      (melt_ptr_t *) & /*_.MODCIDENT__V23*/ meltfptr[20];
	    /*^apply.arg */
	    argtab[2].meltbp_cstring = ")\n";
	    /*_.ADD2OUT__V68*/ meltfptr[67] =
	      melt_apply ((meltclosure_ptr_t)
			  (( /*!ADD2OUT */ meltfrout->tabval[13])),
			  (melt_ptr_t) ( /*_.MKBUF__V16*/ meltfptr[15]),
			  (MELTBPARSTR_CSTRING MELTBPARSTR_PTR
			   MELTBPARSTR_CSTRING ""), argtab, "",
			  (union meltparam_un *) 0);
	  }
	  ;
	  MELT_LOCATION ("warmelt-outobj.melt:4788:/ quasiblock");


	  /*_.PROGN___V69*/ meltfptr[68] = /*_.ADD2OUT__V68*/ meltfptr[67];;
	  /*^compute */
	  /*_.IF___V62*/ meltfptr[58] = /*_.PROGN___V69*/ meltfptr[68];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-outobj.melt:4788:/ clear");
	     /*clear *//*_.ADD2OUT__V63*/ meltfptr[59] = 0;
	  /*^clear */
	     /*clear *//*_.ADD2OUT__V64*/ meltfptr[63] = 0;
	  /*^clear */
	     /*clear *//*_.ADD2OUT__V67*/ meltfptr[66] = 0;
	  /*^clear */
	     /*clear *//*_.ADD2OUT__V68*/ meltfptr[67] = 0;
	  /*^clear */
	     /*clear *//*_.PROGN___V69*/ meltfptr[68] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

  /*_.IF___V62*/ meltfptr[58] = NULL;;
      }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:4798:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^cond */
    /*cond */ if ( /*_#NBPACKAGEPC__L10*/ meltfnum[8])	/*then */
      {
	/*^cond.then */
	/*_.IFELSE___V70*/ meltfptr[59] = ( /*nil */ NULL);;
      }
    else
      {
	MELT_LOCATION ("warmelt-outobj.melt:4798:/ cond.else");

	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-outobj.melt:4799:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^apply */
	  /*apply */
	  {
	    union meltparam_un argtab[1];
	    memset (&argtab, 0, sizeof (argtab));
	    /*^apply.arg */
	    argtab[0].meltbp_cstring =
	      "# no packages needed thru pkg-config\n";
	    /*_.ADD2OUT__V71*/ meltfptr[63] =
	      melt_apply ((meltclosure_ptr_t)
			  (( /*!ADD2OUT */ meltfrout->tabval[13])),
			  (melt_ptr_t) ( /*_.MKBUF__V16*/ meltfptr[15]),
			  (MELTBPARSTR_CSTRING ""), argtab, "",
			  (union meltparam_un *) 0);
	  }
	  ;
	  MELT_LOCATION ("warmelt-outobj.melt:4798:/ quasiblock");


	  /*_.PROGN___V72*/ meltfptr[66] = /*_.ADD2OUT__V71*/ meltfptr[63];;
	  /*^compute */
	  /*_.IFELSE___V70*/ meltfptr[59] = /*_.PROGN___V72*/ meltfptr[66];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-outobj.melt:4798:/ clear");
	     /*clear *//*_.ADD2OUT__V71*/ meltfptr[63] = 0;
	  /*^clear */
	     /*clear *//*_.PROGN___V72*/ meltfptr[66] = 0;
	}
	;
      }
    ;
    /*_.LET___V56*/ meltfptr[55] = /*_.IFELSE___V70*/ meltfptr[59];;

    MELT_LOCATION ("warmelt-outobj.melt:4784:/ clear");
	   /*clear *//*_.PACKAGEPCTUP__V57*/ meltfptr[56] = 0;
    /*^clear */
	   /*clear *//*_#NBPACKAGEPC__L10*/ meltfnum[8] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V58*/ meltfptr[57] = 0;
    /*^clear */
	   /*clear *//*_.IF___V62*/ meltfptr[58] = 0;
    /*^clear */
	   /*clear *//*_.IFELSE___V70*/ meltfptr[59] = 0;
    MELT_LOCATION ("warmelt-outobj.melt:4801:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[3];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_cstring = "### generated dependencies for ";
      /*^apply.arg */
      argtab[1].meltbp_aptr =
	(melt_ptr_t *) & /*_.MODNAMSTR__V2*/ meltfptr[1];
      /*^apply.arg */
      argtab[2].meltbp_cstring = "\n\n";
      /*_.ADD2OUT__V73*/ meltfptr[67] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!ADD2OUT */ meltfrout->tabval[13])),
		    (melt_ptr_t) ( /*_.MKRULEBUF__V46*/ meltfptr[45]),
		    (MELTBPARSTR_CSTRING MELTBPARSTR_PTR MELTBPARSTR_CSTRING
		     ""), argtab, "", (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:4802:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[3];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_cstring = "# dependency for primary of ";
      /*^apply.arg */
      argtab[1].meltbp_aptr =
	(melt_ptr_t *) & /*_.MODCIDENT__V23*/ meltfptr[20];
      /*^apply.arg */
      argtab[2].meltbp_cstring = "\n";
      /*_.ADD2OUT__V74*/ meltfptr[68] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!ADD2OUT */ meltfrout->tabval[13])),
		    (melt_ptr_t) ( /*_.MKRULEBUF__V46*/ meltfptr[45]),
		    (MELTBPARSTR_CSTRING MELTBPARSTR_PTR MELTBPARSTR_CSTRING
		     ""), argtab, "", (union meltparam_un *) 0);
    }
    ;
    /*citerblock FOREACH_IN_MULTIPLE */
    {
      /* start foreach_in_multiple meltcit2__EACHTUP */
      long meltcit2__EACHTUP_ln =
	melt_multiple_length ((melt_ptr_t) /*_.FLAVORTUPLE__V30*/
			      meltfptr[29]);
      for ( /*_#FLAVIX__L14*/ meltfnum[7] = 0;
	   ( /*_#FLAVIX__L14*/ meltfnum[7] >= 0)
	   && ( /*_#FLAVIX__L14*/ meltfnum[7] < meltcit2__EACHTUP_ln);
	/*_#FLAVIX__L14*/ meltfnum[7]++)
	{
	  /*_.CURFLAV__V75*/ meltfptr[63] =
	    melt_multiple_nth ((melt_ptr_t)
			       ( /*_.FLAVORTUPLE__V30*/ meltfptr[29]),
			       /*_#FLAVIX__L14*/ meltfnum[7]);



	  MELT_LOCATION ("warmelt-outobj.melt:4806:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^apply */
	  /*apply */
	  {
	    union meltparam_un argtab[8];
	    memset (&argtab, 0, sizeof (argtab));
	    /*^apply.arg */
	    argtab[0].meltbp_aptr =
	      (melt_ptr_t *) & /*_.MODPREFSTR__V28*/ meltfptr[25];
	    /*^apply.arg */
	    argtab[1].meltbp_cstring = ".";
	    /*^apply.arg */
	    argtab[2].meltbp_aptr =
	      (melt_ptr_t *) & /*_.PRIMMD5S__V45*/ meltfptr[44];
	    /*^apply.arg */
	    argtab[3].meltbp_cstring = ".";
	    /*^apply.arg */
	    argtab[4].meltbp_aptr =
	      (melt_ptr_t *) & /*_.CURFLAV__V75*/ meltfptr[63];
	    /*^apply.arg */
	    argtab[5].meltbp_cstring = ".meltpic.o: ";
	    /*^apply.arg */
	    argtab[6].meltbp_aptr =
	      (melt_ptr_t *) & /*_.PRIMPATH__V44*/ meltfptr[43];
	    /*^apply.arg */
	    argtab[7].meltbp_cstring = "\n";
	    /*_.ADD2OUT__V76*/ meltfptr[66] =
	      melt_apply ((meltclosure_ptr_t)
			  (( /*!ADD2OUT */ meltfrout->tabval[13])),
			  (melt_ptr_t) ( /*_.MKRULEBUF__V46*/ meltfptr[45]),
			  (MELTBPARSTR_PTR MELTBPARSTR_CSTRING MELTBPARSTR_PTR
			   MELTBPARSTR_CSTRING MELTBPARSTR_PTR
			   MELTBPARSTR_CSTRING MELTBPARSTR_PTR
			   MELTBPARSTR_CSTRING ""), argtab, "",
			  (union meltparam_un *) 0);
	  }
	  ;
	  if ( /*_#FLAVIX__L14*/ meltfnum[7] < 0)
	    break;
	}			/* end  foreach_in_multiple meltcit2__EACHTUP */

      /*citerepilog */

      MELT_LOCATION ("warmelt-outobj.melt:4803:/ clear");
	    /*clear *//*_.CURFLAV__V75*/ meltfptr[63] = 0;
      /*^clear */
	    /*clear *//*_#FLAVIX__L14*/ meltfnum[7] = 0;
      /*^clear */
	    /*clear *//*_.ADD2OUT__V76*/ meltfptr[66] = 0;
    }				/*endciterblock FOREACH_IN_MULTIPLE */
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:4808:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.DEBUF__V14*/ meltfptr[13]),
			   ("\";"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:4809:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.DEBUF__V14*/ meltfptr[13]),
				(0), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:4811:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.DEBUF__V14*/ meltfptr[13]),
				(0), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:4812:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.DEBUF__V14*/ meltfptr[13]),
			   ("/* hexmd5checksum of secondary C files */"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:4813:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.DEBUF__V14*/ meltfptr[13]),
				(0), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:4814:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.DEBUF__V14*/ meltfptr[13]),
			   ("const char* const melt_secondaryhexmd5tab[]={"));
    }
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:4816:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L15*/ meltfnum[8] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-outobj.melt:4816:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L15*/ meltfnum[8])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L16*/ meltfnum[15] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-outobj.melt:4816:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long =
		/*_#THE_MELTCALLCOUNT__L16*/ meltfnum[15];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-outobj.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 4816;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "output_melt_descriptor secfiles=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.SECFILES__V3*/ meltfptr[2];
	      /*_.MELT_DEBUG_FUN__V79*/ meltfptr[58] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V78*/ meltfptr[57] =
	      /*_.MELT_DEBUG_FUN__V79*/ meltfptr[58];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-outobj.melt:4816:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L16*/ meltfnum[15] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V79*/ meltfptr[58] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V78*/ meltfptr[57] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-outobj.melt:4816:/ quasiblock");


      /*_.PROGN___V80*/ meltfptr[59] = /*_.IF___V78*/ meltfptr[57];;
      /*^compute */
      /*_.IFCPP___V77*/ meltfptr[56] = /*_.PROGN___V80*/ meltfptr[59];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:4816:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L15*/ meltfnum[8] = 0;
      /*^clear */
	     /*clear *//*_.IF___V78*/ meltfptr[57] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V80*/ meltfptr[59] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V77*/ meltfptr[56] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    /*citerblock FOREACH_IN_MULTIPLE */
    {
      /* start foreach_in_multiple meltcit3__EACHTUP */
      long meltcit3__EACHTUP_ln =
	melt_multiple_length ((melt_ptr_t) /*_.SECFILES__V3*/ meltfptr[2]);
      for ( /*_#FILIX__L17*/ meltfnum[15] = 0;
	   ( /*_#FILIX__L17*/ meltfnum[15] >= 0)
	   && ( /*_#FILIX__L17*/ meltfnum[15] < meltcit3__EACHTUP_ln);
	/*_#FILIX__L17*/ meltfnum[15]++)
	{
	  /*_.CURFIL__V81*/ meltfptr[58] =
	    melt_multiple_nth ((melt_ptr_t) ( /*_.SECFILES__V3*/ meltfptr[2]),
			       /*_#FILIX__L17*/ meltfnum[15]);




#if MELT_HAVE_DEBUG
	  MELT_LOCATION ("warmelt-outobj.melt:4820:/ cppif.then");
	  /*^block */
	  /*anyblock */
	  {


	    {
	      /*^locexp */
	      /*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	      melt_dbgcounter++;
#endif
	      ;
	    }
	    ;
	    /*^checksignal */
	    MELT_CHECK_SIGNAL ();
	    ;
    /*_#MELT_NEED_DBG__L18*/ meltfnum[8] =
	      /*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	      ( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	      0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	      ;;
	    MELT_LOCATION ("warmelt-outobj.melt:4820:/ cond");
	    /*cond */ if ( /*_#MELT_NEED_DBG__L18*/ meltfnum[8])	/*then */
	      {
		/*^cond.then */
		/*^block */
		/*anyblock */
		{

      /*_#THE_MELTCALLCOUNT__L19*/ meltfnum[18] =
#ifdef meltcallcount
		    meltcallcount	/* the_meltcallcount */
#else
		    0L
#endif /* meltcallcount the_meltcallcount */
		    ;;
		  MELT_LOCATION ("warmelt-outobj.melt:4820:/ checksignal");
		  MELT_CHECK_SIGNAL ();
		  ;
		  /*^apply */
		  /*apply */
		  {
		    union meltparam_un argtab[7];
		    memset (&argtab, 0, sizeof (argtab));
		    /*^apply.arg */
		    argtab[0].meltbp_long =
		      /*_#THE_MELTCALLCOUNT__L19*/ meltfnum[18];
		    /*^apply.arg */
		    argtab[1].meltbp_cstring = "warmelt-outobj.melt";
		    /*^apply.arg */
		    argtab[2].meltbp_long = 4820;
		    /*^apply.arg */
		    argtab[3].meltbp_cstring =
		      "output_melt_descriptor curfil=";
		    /*^apply.arg */
		    argtab[4].meltbp_aptr =
		      (melt_ptr_t *) & /*_.CURFIL__V81*/ meltfptr[58];
		    /*^apply.arg */
		    argtab[5].meltbp_cstring = " filix=";
		    /*^apply.arg */
		    argtab[6].meltbp_long = /*_#FILIX__L17*/ meltfnum[15];
		    /*_.MELT_DEBUG_FUN__V84*/ meltfptr[83] =
		      melt_apply ((meltclosure_ptr_t)
				  (( /*!MELT_DEBUG_FUN */ meltfrout->
				    tabval[0])),
				  (melt_ptr_t) (( /*nil */ NULL)),
				  (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_PTR MELTBPARSTR_CSTRING
				   MELTBPARSTR_LONG ""), argtab, "",
				  (union meltparam_un *) 0);
		  }
		  ;
		  /*_.IF___V83*/ meltfptr[59] =
		    /*_.MELT_DEBUG_FUN__V84*/ meltfptr[83];;
		  /*epilog */

		  MELT_LOCATION ("warmelt-outobj.melt:4820:/ clear");
		/*clear *//*_#THE_MELTCALLCOUNT__L19*/ meltfnum[18] = 0;
		  /*^clear */
		/*clear *//*_.MELT_DEBUG_FUN__V84*/ meltfptr[83] = 0;
		}
		;
	      }
	    else
	      {			/*^cond.else */

     /*_.IF___V83*/ meltfptr[59] = NULL;;
	      }
	    ;
	    MELT_LOCATION ("warmelt-outobj.melt:4820:/ quasiblock");


	    /*_.PROGN___V85*/ meltfptr[83] = /*_.IF___V83*/ meltfptr[59];;
	    /*^compute */
	    /*_.IFCPP___V82*/ meltfptr[57] = /*_.PROGN___V85*/ meltfptr[83];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-outobj.melt:4820:/ clear");
	      /*clear *//*_#MELT_NEED_DBG__L18*/ meltfnum[8] = 0;
	    /*^clear */
	      /*clear *//*_.IF___V83*/ meltfptr[59] = 0;
	    /*^clear */
	      /*clear *//*_.PROGN___V85*/ meltfptr[83] = 0;
	  }

#else /*MELT_HAVE_DEBUG */
	  /*^cppif.else */
	  /*_.IFCPP___V82*/ meltfptr[57] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:4821:/ locexp");
	    meltgc_strbuf_add_indent ((melt_ptr_t)
				      ( /*_.DEBUF__V14*/ meltfptr[13]), (1),
				      0);
	  }
	  ;
	  MELT_LOCATION ("warmelt-outobj.melt:4822:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^cond */
	  /*cond */ if ( /*_.CURFIL__V81*/ meltfptr[58])	/*then */
	    {
	      /*^cond.then */
	      /*^block */
	      /*anyblock */
	      {


#if MELT_HAVE_DEBUG
		MELT_LOCATION ("warmelt-outobj.melt:4824:/ cppif.then");
		/*^block */
		/*anyblock */
		{

		  /*^checksignal */
		  MELT_CHECK_SIGNAL ();
		  ;
      /*_#IS_A__L20*/ meltfnum[18] =
		    melt_is_instance_of ((melt_ptr_t)
					 ( /*_.CURFIL__V81*/ meltfptr[58]),
					 (melt_ptr_t) (( /*!CLASS_SECONDARY_C_FILE */ meltfrout->tabval[16])));;
		  MELT_LOCATION ("warmelt-outobj.melt:4824:/ cond");
		  /*cond */ if ( /*_#IS_A__L20*/ meltfnum[18])	/*then */
		    {
		      /*^cond.then */
		      /*_.IFELSE___V87*/ meltfptr[83] = ( /*nil */ NULL);;
		    }
		  else
		    {
		      MELT_LOCATION ("warmelt-outobj.melt:4824:/ cond.else");

		      /*^block */
		      /*anyblock */
		      {




			{
			  /*^locexp */
			  melt_assert_failed (("output_melt_descriptor check curfil"), ("warmelt-outobj.melt") ? ("warmelt-outobj.melt") : __FILE__, (4824) ? (4824) : __LINE__, __FUNCTION__);
			  ;
			}
			;
		  /*clear *//*_.IFELSE___V87*/ meltfptr[83] = 0;
			/*epilog */
		      }
		      ;
		    }
		  ;
		  /*^compute */
		  /*_.IFCPP___V86*/ meltfptr[59] =
		    /*_.IFELSE___V87*/ meltfptr[83];;
		  /*epilog */

		  MELT_LOCATION ("warmelt-outobj.melt:4824:/ clear");
		/*clear *//*_#IS_A__L20*/ meltfnum[18] = 0;
		  /*^clear */
		/*clear *//*_.IFELSE___V87*/ meltfptr[83] = 0;
		}

#else /*MELT_HAVE_DEBUG */
		/*^cppif.else */
		/*_.IFCPP___V86*/ meltfptr[59] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
		;
		MELT_LOCATION ("warmelt-outobj.melt:4826:/ compute");
		/*_#LASTSECFILEIX__L6*/ meltfnum[0] =
		  /*_#SETQ___L21*/ meltfnum[8] =
		  /*_#FILIX__L17*/ meltfnum[15];;
		MELT_LOCATION ("warmelt-outobj.melt:4827:/ quasiblock");


		/*^cond */
		/*cond */ if (
			       /*ifisa */
			       melt_is_instance_of ((melt_ptr_t)
						    ( /*_.CURFIL__V81*/
						     meltfptr[58]),
						    (melt_ptr_t) (( /*!CLASS_SECONDARY_C_FILE */ meltfrout->tabval[16])))
		  )		/*then */
		  {
		    /*^cond.then */
		    /*^getslot */
		    {
		      melt_ptr_t slot = NULL, obj = NULL;
		      obj =
			(melt_ptr_t) ( /*_.CURFIL__V81*/ meltfptr[58])
			/*=obj*/ ;
		      melt_object_get_field (slot, obj, 1, "SECFIL_PATH");
      /*_.SECPATH__V88*/ meltfptr[83] = slot;
		    };
		    ;
		  }
		else
		  {		/*^cond.else */

     /*_.SECPATH__V88*/ meltfptr[83] = NULL;;
		  }
		;
		/*^compute */
    /*_.SECMD5S__V89*/ meltfptr[88] =
		  (meltgc_string_hex_md5sum_file
		   (melt_string_str
		    ((melt_ptr_t) /*_.SECPATH__V88*/ meltfptr[83])));;

		{
		  MELT_LOCATION ("warmelt-outobj.melt:4830:/ locexp");
		  meltgc_append_list ((melt_ptr_t)
				      ( /*_.PATHLIST__V17*/ meltfptr[16]),
				      (melt_ptr_t) ( /*_.SECPATH__V88*/
						    meltfptr[83]));
		}
		;

		{
		  MELT_LOCATION ("warmelt-outobj.melt:4831:/ locexp");
		  meltgc_append_list ((melt_ptr_t)
				      ( /*_.MD5LIST__V18*/ meltfptr[17]),
				      (melt_ptr_t) ( /*_.SECMD5S__V89*/
						    meltfptr[88]));
		}
		;

		{
		  MELT_LOCATION ("warmelt-outobj.melt:4832:/ locexp");
		  /*add2sbuf_strconst */
		    meltgc_add_strbuf ((melt_ptr_t)
				       ( /*_.DEBUF__V14*/ meltfptr[13]),
				       ("/*sechexmd5checksum "));
		}
		;

		{
		  MELT_LOCATION ("warmelt-outobj.melt:4833:/ locexp");
		  meltgc_add_strbuf_ccomment ((melt_ptr_t)
					      ( /*_.DEBUF__V14*/
					       meltfptr[13]),
					      melt_string_str ((melt_ptr_t)
							       ( /*_.SECPATH__V88*/ meltfptr[83])));
		}
		;

		{
		  MELT_LOCATION ("warmelt-outobj.melt:4834:/ locexp");
		  /*add2sbuf_strconst */
		    meltgc_add_strbuf ((melt_ptr_t)
				       ( /*_.DEBUF__V14*/ meltfptr[13]),
				       (" #"));
		}
		;
    /*_#GET_INT__L22*/ meltfnum[18] =
		  (melt_get_int
		   ((melt_ptr_t) ( /*_.CURFIL__V81*/ meltfptr[58])));;

		{
		  MELT_LOCATION ("warmelt-outobj.melt:4835:/ locexp");
		  meltgc_add_strbuf_dec ((melt_ptr_t)
					 ( /*_.DEBUF__V14*/ meltfptr[13]),
					 ( /*_#GET_INT__L22*/ meltfnum[18]));
		}
		;

		{
		  MELT_LOCATION ("warmelt-outobj.melt:4836:/ locexp");
		  /*add2sbuf_strconst */
		    meltgc_add_strbuf ((melt_ptr_t)
				       ( /*_.DEBUF__V14*/ meltfptr[13]),
				       (" */ \""));
		}
		;

		{
		  MELT_LOCATION ("warmelt-outobj.melt:4837:/ locexp");
		  meltgc_add_strbuf_cstr ((melt_ptr_t)
					  ( /*_.DEBUF__V14*/ meltfptr[13]),
					  melt_string_str ((melt_ptr_t)
							   ( /*_.SECMD5S__V89*/ meltfptr[88])));
		}
		;

		{
		  MELT_LOCATION ("warmelt-outobj.melt:4838:/ locexp");
		  /*add2sbuf_strconst */
		    meltgc_add_strbuf ((melt_ptr_t)
				       ( /*_.DEBUF__V14*/ meltfptr[13]),
				       ("\","));
		}
		;
		MELT_LOCATION ("warmelt-outobj.melt:4839:/ checksignal");
		MELT_CHECK_SIGNAL ();
		;
		/*^apply */
		/*apply */
		{
		  union meltparam_un argtab[2];
		  memset (&argtab, 0, sizeof (argtab));
		  /*^apply.arg */
		  argtab[0].meltbp_cstring = " ";
		  /*^apply.arg */
		  argtab[1].meltbp_aptr =
		    (melt_ptr_t *) & /*_.SECPATH__V88*/ meltfptr[83];
		  /*_.ADD2OUT__V90*/ meltfptr[89] =
		    melt_apply ((meltclosure_ptr_t)
				(( /*!ADD2OUT */ meltfrout->tabval[13])),
				(melt_ptr_t) ( /*_.SECPATHBUF__V48*/
					      meltfptr[47]),
				(MELTBPARSTR_CSTRING MELTBPARSTR_PTR ""),
				argtab, "", (union meltparam_un *) 0);
		}
		;
		MELT_LOCATION ("warmelt-outobj.melt:4840:/ checksignal");
		MELT_CHECK_SIGNAL ();
		;
		/*^apply */
		/*apply */
		{
		  union meltparam_un argtab[2];
		  memset (&argtab, 0, sizeof (argtab));
		  /*^apply.arg */
		  argtab[0].meltbp_cstring = " ";
		  /*^apply.arg */
		  argtab[1].meltbp_aptr =
		    (melt_ptr_t *) & /*_.SECMD5S__V89*/ meltfptr[88];
		  /*_.ADD2OUT__V91*/ meltfptr[90] =
		    melt_apply ((meltclosure_ptr_t)
				(( /*!ADD2OUT */ meltfrout->tabval[13])),
				(melt_ptr_t) ( /*_.SECMD5BUF__V47*/
					      meltfptr[46]),
				(MELTBPARSTR_CSTRING MELTBPARSTR_PTR ""),
				argtab, "", (union meltparam_un *) 0);
		}
		;
		MELT_LOCATION ("warmelt-outobj.melt:4842:/ checksignal");
		MELT_CHECK_SIGNAL ();
		;
		/*^apply */
		/*apply */
		{
		  union meltparam_un argtab[5];
		  memset (&argtab, 0, sizeof (argtab));
		  /*^apply.arg */
		  argtab[0].meltbp_cstring = "# dependencies for secondary ";
		  /*^apply.arg */
		  argtab[1].meltbp_long = /*_#FILIX__L17*/ meltfnum[15];
		  /*^apply.arg */
		  argtab[2].meltbp_cstring = " of ";
		  /*^apply.arg */
		  argtab[3].meltbp_aptr =
		    (melt_ptr_t *) & /*_.MODCIDENT__V23*/ meltfptr[20];
		  /*^apply.arg */
		  argtab[4].meltbp_cstring = "\n";
		  /*_.ADD2OUT__V92*/ meltfptr[91] =
		    melt_apply ((meltclosure_ptr_t)
				(( /*!ADD2OUT */ meltfrout->tabval[13])),
				(melt_ptr_t) ( /*_.MKRULEBUF__V46*/
					      meltfptr[45]),
				(MELTBPARSTR_CSTRING MELTBPARSTR_LONG
				 MELTBPARSTR_CSTRING MELTBPARSTR_PTR
				 MELTBPARSTR_CSTRING ""), argtab, "",
				(union meltparam_un *) 0);
		}
		;
		/*citerblock FOREACH_IN_MULTIPLE */
		{
		  /* start foreach_in_multiple meltcit4__EACHTUP */
		  long meltcit4__EACHTUP_ln =
		    melt_multiple_length ((melt_ptr_t) /*_.FLAVORTUPLE__V30*/
					  meltfptr[29]);
		  for ( /*_#FLAVIX__L23*/ meltfnum[22] = 0;
		       ( /*_#FLAVIX__L23*/ meltfnum[22] >= 0)
		       && ( /*_#FLAVIX__L23*/ meltfnum[22] <
			   meltcit4__EACHTUP_ln);
	/*_#FLAVIX__L23*/ meltfnum[22]++)
		    {
		      /*_.CURFLAV__V93*/ meltfptr[92] =
			melt_multiple_nth ((melt_ptr_t)
					   ( /*_.FLAVORTUPLE__V30*/
					    meltfptr[29]), /*_#FLAVIX__L23*/
					   meltfnum[22]);



		      MELT_LOCATION
			("warmelt-outobj.melt:4847:/ checksignal");
		      MELT_CHECK_SIGNAL ();
		      ;
     /*_#I__L24*/ meltfnum[23] =
			(( /*_#FILIX__L17*/ meltfnum[15]) < (10));;
		      MELT_LOCATION ("warmelt-outobj.melt:4847:/ cond");
		      /*cond */ if ( /*_#I__L24*/ meltfnum[23])	/*then */
			{
			  /*^cond.then */
     /*_?*/ meltfram__.loc_CSTRING__o0 = "0";;
			}
		      else
			{
			  MELT_LOCATION
			    ("warmelt-outobj.melt:4847:/ cond.else");

      /*_?*/ meltfram__.loc_CSTRING__o0 = "";;
			}
		      ;
		      MELT_LOCATION
			("warmelt-outobj.melt:4849:/ checksignal");
		      MELT_CHECK_SIGNAL ();
		      ;
     /*_#I__L25*/ meltfnum[24] =
			(( /*_#FILIX__L17*/ meltfnum[15]) < (10));;
		      MELT_LOCATION ("warmelt-outobj.melt:4849:/ cond");
		      /*cond */ if ( /*_#I__L25*/ meltfnum[24])	/*then */
			{
			  /*^cond.then */
     /*_?*/ meltfram__.loc_CSTRING__o1 = "0";;
			}
		      else
			{
			  MELT_LOCATION
			    ("warmelt-outobj.melt:4849:/ cond.else");

      /*_?*/ meltfram__.loc_CSTRING__o1 = "";;
			}
		      ;
		      MELT_LOCATION
			("warmelt-outobj.melt:4846:/ checksignal");
		      MELT_CHECK_SIGNAL ();
		      ;
		      /*^apply */
		      /*apply */
		      {
			union meltparam_un argtab[15];
			memset (&argtab, 0, sizeof (argtab));
			/*^apply.arg */
			argtab[0].meltbp_aptr =
			  (melt_ptr_t *) & /*_.MODPREFSTR__V28*/ meltfptr[25];
			/*^apply.arg */
			argtab[1].meltbp_cstring = "+";
			/*^apply.arg */
			argtab[2].meltbp_cstring =
			  /*_?*/ meltfram__.loc_CSTRING__o0;
			/*^apply.arg */
			argtab[3].meltbp_long = /*_#FILIX__L17*/ meltfnum[15];
			/*^apply.arg */
			argtab[4].meltbp_cstring = ".";
			/*^apply.arg */
			argtab[5].meltbp_aptr =
			  (melt_ptr_t *) & /*_.SECMD5S__V89*/ meltfptr[88];
			/*^apply.arg */
			argtab[6].meltbp_cstring = ".";
			/*^apply.arg */
			argtab[7].meltbp_aptr =
			  (melt_ptr_t *) & /*_.CURFLAV__V93*/ meltfptr[92];
			/*^apply.arg */
			argtab[8].meltbp_cstring = ".meltpic.o: ";
			/*^apply.arg */
			argtab[9].meltbp_aptr =
			  (melt_ptr_t *) & /*_.MODNAMSTR__V2*/ meltfptr[1];
			/*^apply.arg */
			argtab[10].meltbp_cstring = "+";
			/*^apply.arg */
			argtab[11].meltbp_cstring =
			  /*_?*/ meltfram__.loc_CSTRING__o1;
			/*^apply.arg */
			argtab[12].meltbp_long =
			  /*_#FILIX__L17*/ meltfnum[15];
			/*^apply.arg */
			argtab[13].meltbp_cstring = ".c";
			/*^apply.arg */
			argtab[14].meltbp_cstring = "\n";
			/*_.ADD2OUT__V94*/ meltfptr[93] =
			  melt_apply ((meltclosure_ptr_t)
				      (( /*!ADD2OUT */ meltfrout->
					tabval[13])),
				      (melt_ptr_t) ( /*_.MKRULEBUF__V46*/
						    meltfptr[45]),
				      (MELTBPARSTR_PTR MELTBPARSTR_CSTRING
				       MELTBPARSTR_CSTRING MELTBPARSTR_LONG
				       MELTBPARSTR_CSTRING MELTBPARSTR_PTR
				       MELTBPARSTR_CSTRING MELTBPARSTR_PTR
				       MELTBPARSTR_CSTRING MELTBPARSTR_PTR
				       MELTBPARSTR_CSTRING MELTBPARSTR_CSTRING
				       MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				       MELTBPARSTR_CSTRING ""), argtab, "",
				      (union meltparam_un *) 0);
		      }
		      ;
		      if ( /*_#FLAVIX__L23*/ meltfnum[22] < 0)
			break;
		    }		/* end  foreach_in_multiple meltcit4__EACHTUP */

		  /*citerepilog */

		  MELT_LOCATION ("warmelt-outobj.melt:4843:/ clear");
	       /*clear *//*_.CURFLAV__V93*/ meltfptr[92] = 0;
		  /*^clear */
	       /*clear *//*_#FLAVIX__L23*/ meltfnum[22] = 0;
		  /*^clear */
	       /*clear *//*_#I__L24*/ meltfnum[23] = 0;
		  /*^clear */
	       /*clear *//*_?*/ meltfram__.loc_CSTRING__o0 = 0;
		  /*^clear */
	       /*clear *//*_#I__L25*/ meltfnum[24] = 0;
		  /*^clear */
	       /*clear *//*_?*/ meltfram__.loc_CSTRING__o1 = 0;
		  /*^clear */
	       /*clear *//*_.ADD2OUT__V94*/ meltfptr[93] = 0;
		}		/*endciterblock FOREACH_IN_MULTIPLE */
		;

		MELT_LOCATION ("warmelt-outobj.melt:4827:/ clear");
	      /*clear *//*_.SECPATH__V88*/ meltfptr[83] = 0;
		/*^clear */
	      /*clear *//*_.SECMD5S__V89*/ meltfptr[88] = 0;
		/*^clear */
	      /*clear *//*_#GET_INT__L22*/ meltfnum[18] = 0;
		/*^clear */
	      /*clear *//*_.ADD2OUT__V90*/ meltfptr[89] = 0;
		/*^clear */
	      /*clear *//*_.ADD2OUT__V91*/ meltfptr[90] = 0;
		/*^clear */
	      /*clear *//*_.ADD2OUT__V92*/ meltfptr[91] = 0;
		MELT_LOCATION ("warmelt-outobj.melt:4823:/ quasiblock");


		/*epilog */

		MELT_LOCATION ("warmelt-outobj.melt:4822:/ clear");
	      /*clear *//*_.IFCPP___V86*/ meltfptr[59] = 0;
		/*^clear */
	      /*clear *//*_#SETQ___L21*/ meltfnum[8] = 0;
	      }
	      ;
	    }
	  else
	    {			/*^cond.else */

	      /*^block */
	      /*anyblock */
	      {


		{
		  MELT_LOCATION ("warmelt-outobj.melt:4853:/ locexp");
		  /*add2sbuf_strconst */
		    meltgc_add_strbuf ((melt_ptr_t)
				       ( /*_.DEBUF__V14*/ meltfptr[13]),
				       ("/*nosecfile*/ (const char*)0,"));
		}
		;
		/*epilog */
	      }
	      ;
	    }
	  ;
	  if ( /*_#FILIX__L17*/ meltfnum[15] < 0)
	    break;
	}			/* end  foreach_in_multiple meltcit3__EACHTUP */

      /*citerepilog */

      MELT_LOCATION ("warmelt-outobj.melt:4817:/ clear");
	    /*clear *//*_.CURFIL__V81*/ meltfptr[58] = 0;
      /*^clear */
	    /*clear *//*_#FILIX__L17*/ meltfnum[15] = 0;
      /*^clear */
	    /*clear *//*_.IFCPP___V82*/ meltfptr[57] = 0;
    }				/*endciterblock FOREACH_IN_MULTIPLE */
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:4855:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.DEBUF__V14*/ meltfptr[13]),
				(1), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:4856:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.DEBUF__V14*/ meltfptr[13]),
			   ("(const char*)0 };"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:4857:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.DEBUF__V14*/ meltfptr[13]),
				(0), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:4859:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.DEBUF__V14*/ meltfptr[13]),
				(0), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:4860:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.DEBUF__V14*/ meltfptr[13]),
			   ("/* last index of secondary files */"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:4861:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.DEBUF__V14*/ meltfptr[13]),
				(0), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:4862:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.DEBUF__V14*/ meltfptr[13]),
			   ("const int melt_lastsecfileindex="));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:4863:/ locexp");
      meltgc_add_strbuf_dec ((melt_ptr_t) ( /*_.DEBUF__V14*/ meltfptr[13]),
			     ( /*_#LASTSECFILEIX__L6*/ meltfnum[0]));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:4864:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.DEBUF__V14*/ meltfptr[13]),
			   (";"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:4865:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.DEBUF__V14*/ meltfptr[13]),
				(0), 0);
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:4866:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_cstring = "#secondary paths and checksums\n";
      /*_.ADD2OUT__V95*/ meltfptr[83] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!ADD2OUT */ meltfrout->tabval[13])),
		    (melt_ptr_t) ( /*_.MKBUF__V16*/ meltfptr[15]),
		    (MELTBPARSTR_CSTRING ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;
 /*_.STRBUF2STRING__V96*/ meltfptr[88] =
      (meltgc_new_stringdup
       ((meltobject_ptr_t) (( /*!DISCR_STRING */ meltfrout->tabval[4])),
	melt_strbuf_str ((melt_ptr_t)
			 ( /*_.SECPATHBUF__V48*/ meltfptr[47]))));;
    MELT_LOCATION ("warmelt-outobj.melt:4867:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[5];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_cstring = "MELTGENMOD_SECONDARY_FILES_";
      /*^apply.arg */
      argtab[1].meltbp_aptr =
	(melt_ptr_t *) & /*_.MODCIDENT__V23*/ meltfptr[20];
      /*^apply.arg */
      argtab[2].meltbp_cstring = "=";
      /*^apply.arg */
      argtab[3].meltbp_aptr =
	(melt_ptr_t *) & /*_.STRBUF2STRING__V96*/ meltfptr[88];
      /*^apply.arg */
      argtab[4].meltbp_cstring = "\n";
      /*_.ADD2OUT__V97*/ meltfptr[89] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!ADD2OUT */ meltfrout->tabval[13])),
		    (melt_ptr_t) ( /*_.MKBUF__V16*/ meltfptr[15]),
		    (MELTBPARSTR_CSTRING MELTBPARSTR_PTR MELTBPARSTR_CSTRING
		     MELTBPARSTR_PTR MELTBPARSTR_CSTRING ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:4868:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.MKBUF__V16*/ meltfptr[15]),
				(0), 0);
    }
    ;
 /*_.STRBUF2STRING__V98*/ meltfptr[90] =
      (meltgc_new_stringdup
       ((meltobject_ptr_t) (( /*!DISCR_STRING */ meltfrout->tabval[4])),
	melt_strbuf_str ((melt_ptr_t)
			 ( /*_.SECMD5BUF__V47*/ meltfptr[46]))));;
    MELT_LOCATION ("warmelt-outobj.melt:4869:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[5];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_cstring = "MELTGENMOD_SECONDARY_MD5SUMS_";
      /*^apply.arg */
      argtab[1].meltbp_aptr =
	(melt_ptr_t *) & /*_.MODCIDENT__V23*/ meltfptr[20];
      /*^apply.arg */
      argtab[2].meltbp_cstring = "=";
      /*^apply.arg */
      argtab[3].meltbp_aptr =
	(melt_ptr_t *) & /*_.STRBUF2STRING__V98*/ meltfptr[90];
      /*^apply.arg */
      argtab[4].meltbp_cstring = "\n";
      /*_.ADD2OUT__V99*/ meltfptr[91] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!ADD2OUT */ meltfrout->tabval[13])),
		    (melt_ptr_t) ( /*_.MKBUF__V16*/ meltfptr[15]),
		    (MELTBPARSTR_CSTRING MELTBPARSTR_PTR MELTBPARSTR_CSTRING
		     MELTBPARSTR_PTR MELTBPARSTR_CSTRING ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:4870:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.MKBUF__V16*/ meltfptr[15]),
				(0), 0);
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:4872:/ quasiblock");


    MELT_LOCATION ("warmelt-outobj.melt:4873:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      /*_.LIST_TO_MULTIPLE__V101*/ meltfptr[100] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!LIST_TO_MULTIPLE */ meltfrout->tabval[14])),
		    (melt_ptr_t) ( /*_.PATHLIST__V17*/ meltfptr[16]), (""),
		    (union meltparam_un *) 0, "", (union meltparam_un *) 0);
    }
    ;
 /*_.CUMPATHMDS__V102*/ meltfptr[101] =
      (meltgc_string_hex_md5sum_file_sequence
       ((melt_ptr_t) /*_.LIST_TO_MULTIPLE__V101*/ meltfptr[100]));;
    /*^compute */
 /*_.MIDSTR__V103*/ meltfptr[102] =
      (meltgc_new_string_nakedbasename
       ((meltobject_ptr_t) (( /*!DISCR_STRING */ meltfrout->tabval[4])),
	melt_string_str ((melt_ptr_t) ( /*_.MODNAMSTR__V2*/ meltfptr[1]))));;
    MELT_LOCATION ("warmelt-outobj.melt:4876:/ quasiblock");


 /*_.MOUT__V105*/ meltfptr[104] =
      (melt_ptr_t)
      meltgc_new_strbuf ((meltobject_ptr_t)
			 (( /*!DISCR_STRBUF */ meltfrout->tabval[2])),
			 (const char *) 0);;
    MELT_LOCATION ("warmelt-outobj.melt:4878:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_cstring = "meltmod_";
      /*_.ADD2OUT__V106*/ meltfptr[105] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!ADD2OUT */ meltfrout->tabval[13])),
		    (melt_ptr_t) ( /*_.MOUT__V105*/ meltfptr[104]),
		    (MELTBPARSTR_CSTRING ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:4879:/ locexp");
      meltgc_add_strbuf_cident ((melt_ptr_t)
				( /*_.MOUT__V105*/ meltfptr[104]),
				melt_string_str ((melt_ptr_t)
						 ( /*_.MIDSTR__V103*/
						  meltfptr[102])));
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:4880:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_cstring = "_mds__";
      /*_.ADD2OUT__V107*/ meltfptr[106] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!ADD2OUT */ meltfrout->tabval[13])),
		    (melt_ptr_t) ( /*_.MOUT__V105*/ meltfptr[104]),
		    (MELTBPARSTR_CSTRING ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:4881:/ locexp");
      meltgc_add_strbuf_cident ((melt_ptr_t)
				( /*_.MOUT__V105*/ meltfptr[104]),
				melt_string_str ((melt_ptr_t)
						 ( /*_.CUMPATHMDS__V102*/
						  meltfptr[101])));
    }
    ;
 /*_.STRBUF2STRING__V108*/ meltfptr[107] =
      (meltgc_new_stringdup
       ((meltobject_ptr_t) (( /*!DISCR_STRING */ meltfrout->tabval[4])),
	melt_strbuf_str ((melt_ptr_t) ( /*_.MOUT__V105*/ meltfptr[104]))));;
    /*^compute */
    /*_.LET___V104*/ meltfptr[103] = /*_.STRBUF2STRING__V108*/ meltfptr[107];;

    MELT_LOCATION ("warmelt-outobj.melt:4876:/ clear");
	   /*clear *//*_.MOUT__V105*/ meltfptr[104] = 0;
    /*^clear */
	   /*clear *//*_.ADD2OUT__V106*/ meltfptr[105] = 0;
    /*^clear */
	   /*clear *//*_.ADD2OUT__V107*/ meltfptr[106] = 0;
    /*^clear */
	   /*clear *//*_.STRBUF2STRING__V108*/ meltfptr[107] = 0;
    /*_.MODIDNAM__V109*/ meltfptr[104] = /*_.LET___V104*/ meltfptr[103];;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:4885:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_STRING__L26*/ meltfnum[18] =
	(melt_magic_discr
	 ((melt_ptr_t) ( /*_.CUMPATHMDS__V102*/ meltfptr[101])) ==
	 MELTOBMAG_STRING);;
      MELT_LOCATION ("warmelt-outobj.melt:4885:/ cond");
      /*cond */ if ( /*_#IS_STRING__L26*/ meltfnum[18])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V111*/ meltfptr[106] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-outobj.melt:4885:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check cumpathmds"),
				  ("warmelt-outobj.melt")
				  ? ("warmelt-outobj.melt") : __FILE__,
				  (4885) ? (4885) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V111*/ meltfptr[106] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V110*/ meltfptr[105] = /*_.IFELSE___V111*/ meltfptr[106];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:4885:/ clear");
	     /*clear *//*_#IS_STRING__L26*/ meltfnum[18] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V111*/ meltfptr[106] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V110*/ meltfptr[105] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-outobj.melt:4887:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[3];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_cstring = "## dependency for descriptor object of ";
      /*^apply.arg */
      argtab[1].meltbp_aptr =
	(melt_ptr_t *) & /*_.MODNAMSTR__V2*/ meltfptr[1];
      /*^apply.arg */
      argtab[2].meltbp_cstring = "\n";
      /*_.ADD2OUT__V112*/ meltfptr[107] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!ADD2OUT */ meltfrout->tabval[13])),
		    (melt_ptr_t) ( /*_.MKRULEBUF__V46*/ meltfptr[45]),
		    (MELTBPARSTR_CSTRING MELTBPARSTR_PTR MELTBPARSTR_CSTRING
		     ""), argtab, "", (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:4888:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[8];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & /*_.MODPREFSTR__V28*/ meltfptr[25];
      /*^apply.arg */
      argtab[1].meltbp_cstring = ".";
      /*^apply.arg */
      argtab[2].meltbp_aptr =
	(melt_ptr_t *) & /*_.CUMPATHMDS__V102*/ meltfptr[101];
      /*^apply.arg */
      argtab[3].meltbp_cstring = ".descriptor.meltpic.o: ";
      /*^apply.arg */
      argtab[4].meltbp_aptr =
	(melt_ptr_t *) & /*_.MODNAMSTR__V2*/ meltfptr[1];
      /*^apply.arg */
      argtab[5].meltbp_cstring = "+meltdesc.c ";
      /*^apply.arg */
      argtab[6].meltbp_aptr =
	(melt_ptr_t *) & /*_.MODNAMSTR__V2*/ meltfptr[1];
      /*^apply.arg */
      argtab[7].meltbp_cstring = "+melttime.h\n";
      /*_.ADD2OUT__V113*/ meltfptr[106] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!ADD2OUT */ meltfrout->tabval[13])),
		    (melt_ptr_t) ( /*_.MKRULEBUF__V46*/ meltfptr[45]),
		    (MELTBPARSTR_PTR MELTBPARSTR_CSTRING MELTBPARSTR_PTR
		     MELTBPARSTR_CSTRING MELTBPARSTR_PTR MELTBPARSTR_CSTRING
		     MELTBPARSTR_PTR MELTBPARSTR_CSTRING ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:4891:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[3];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_cstring = "## dependency for modules ";
      /*^apply.arg */
      argtab[1].meltbp_aptr =
	(melt_ptr_t *) & /*_.MODNAMSTR__V2*/ meltfptr[1];
      /*^apply.arg */
      argtab[2].meltbp_cstring = "\n";
      /*_.ADD2OUT__V114*/ meltfptr[113] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!ADD2OUT */ meltfrout->tabval[13])),
		    (melt_ptr_t) ( /*_.MKRULEBUF__V46*/ meltfptr[45]),
		    (MELTBPARSTR_CSTRING MELTBPARSTR_PTR MELTBPARSTR_CSTRING
		     ""), argtab, "", (union meltparam_un *) 0);
    }
    ;
    /*citerblock FOREACH_IN_MULTIPLE */
    {
      /* start foreach_in_multiple meltcit5__EACHTUP */
      long meltcit5__EACHTUP_ln =
	melt_multiple_length ((melt_ptr_t) /*_.FLAVORTUPLE__V30*/
			      meltfptr[29]);
      for ( /*_#FLAVIX__L27*/ meltfnum[8] = 0;
	   ( /*_#FLAVIX__L27*/ meltfnum[8] >= 0)
	   && ( /*_#FLAVIX__L27*/ meltfnum[8] < meltcit5__EACHTUP_ln);
	/*_#FLAVIX__L27*/ meltfnum[8]++)
	{
	  /*_.CURFLAV__V115*/ meltfptr[114] =
	    melt_multiple_nth ((melt_ptr_t)
			       ( /*_.FLAVORTUPLE__V30*/ meltfptr[29]),
			       /*_#FLAVIX__L27*/ meltfnum[8]);



	  MELT_LOCATION ("warmelt-outobj.melt:4895:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^apply */
	  /*apply */
	  {
	    union meltparam_un argtab[5];
	    memset (&argtab, 0, sizeof (argtab));
	    /*^apply.arg */
	    argtab[0].meltbp_cstring = "\n#### dependencies for flavor ";
	    /*^apply.arg */
	    argtab[1].meltbp_aptr =
	      (melt_ptr_t *) & /*_.CURFLAV__V115*/ meltfptr[114];
	    /*^apply.arg */
	    argtab[2].meltbp_cstring = " of module ";
	    /*^apply.arg */
	    argtab[3].meltbp_aptr =
	      (melt_ptr_t *) & /*_.MODNAMSTR__V2*/ meltfptr[1];
	    /*^apply.arg */
	    argtab[4].meltbp_cstring = "\n\n";
	    /*_.ADD2OUT__V116*/ meltfptr[115] =
	      melt_apply ((meltclosure_ptr_t)
			  (( /*!ADD2OUT */ meltfrout->tabval[13])),
			  (melt_ptr_t) ( /*_.MKRULEBUF__V46*/ meltfptr[45]),
			  (MELTBPARSTR_CSTRING MELTBPARSTR_PTR
			   MELTBPARSTR_CSTRING MELTBPARSTR_PTR
			   MELTBPARSTR_CSTRING ""), argtab, "",
			  (union meltparam_un *) 0);
	  }
	  ;
	  MELT_LOCATION ("warmelt-outobj.melt:4897:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^apply */
	  /*apply */
	  {
	    union meltparam_un argtab[5];
	    memset (&argtab, 0, sizeof (argtab));
	    /*^apply.arg */
	    argtab[0].meltbp_cstring = "\n\n## new dependency for flavor ";
	    /*^apply.arg */
	    argtab[1].meltbp_aptr =
	      (melt_ptr_t *) & /*_.CURFLAV__V115*/ meltfptr[114];
	    /*^apply.arg */
	    argtab[2].meltbp_cstring = " of module ";
	    /*^apply.arg */
	    argtab[3].meltbp_aptr =
	      (melt_ptr_t *) & /*_.MODNAMSTR__V2*/ meltfptr[1];
	    /*^apply.arg */
	    argtab[4].meltbp_cstring = "\n";
	    /*_.ADD2OUT__V117*/ meltfptr[116] =
	      melt_apply ((meltclosure_ptr_t)
			  (( /*!ADD2OUT */ meltfrout->tabval[13])),
			  (melt_ptr_t) ( /*_.MKRULEBUF__V46*/ meltfptr[45]),
			  (MELTBPARSTR_CSTRING MELTBPARSTR_PTR
			   MELTBPARSTR_CSTRING MELTBPARSTR_PTR
			   MELTBPARSTR_CSTRING ""), argtab, "",
			  (union meltparam_un *) 0);
	  }
	  ;
	  MELT_LOCATION ("warmelt-outobj.melt:4898:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^apply */
	  /*apply */
	  {
	    union meltparam_un argtab[11];
	    memset (&argtab, 0, sizeof (argtab));
	    /*^apply.arg */
	    argtab[0].meltbp_aptr =
	      (melt_ptr_t *) & /*_.MODPREFSTR__V28*/ meltfptr[25];
	    /*^apply.arg */
	    argtab[1].meltbp_cstring = ".meltmod-";
	    /*^apply.arg */
	    argtab[2].meltbp_aptr =
	      (melt_ptr_t *) & /*_.CUMPATHMDS__V102*/ meltfptr[101];
	    /*^apply.arg */
	    argtab[3].meltbp_cstring = ".";
	    /*^apply.arg */
	    argtab[4].meltbp_aptr =
	      (melt_ptr_t *) & /*_.CURFLAV__V115*/ meltfptr[114];
	    /*^apply.arg */
	    argtab[5].meltbp_cstring = ".so: \\\n";
	    /*^apply.arg */
	    argtab[6].meltbp_cstring = "  ";
	    /*^apply.arg */
	    argtab[7].meltbp_aptr =
	      (melt_ptr_t *) & /*_.MODPREFSTR__V28*/ meltfptr[25];
	    /*^apply.arg */
	    argtab[8].meltbp_cstring = ".";
	    /*^apply.arg */
	    argtab[9].meltbp_aptr =
	      (melt_ptr_t *) & /*_.CUMPATHMDS__V102*/ meltfptr[101];
	    /*^apply.arg */
	    argtab[10].meltbp_cstring = ".descriptor.meltpic.o ";
	    /*_.ADD2OUT__V118*/ meltfptr[117] =
	      melt_apply ((meltclosure_ptr_t)
			  (( /*!ADD2OUT */ meltfrout->tabval[13])),
			  (melt_ptr_t) ( /*_.MKRULEBUF__V46*/ meltfptr[45]),
			  (MELTBPARSTR_PTR MELTBPARSTR_CSTRING MELTBPARSTR_PTR
			   MELTBPARSTR_CSTRING MELTBPARSTR_PTR
			   MELTBPARSTR_CSTRING MELTBPARSTR_CSTRING
			   MELTBPARSTR_PTR MELTBPARSTR_CSTRING MELTBPARSTR_PTR
			   MELTBPARSTR_CSTRING ""), argtab, "",
			  (union meltparam_un *) 0);
	  }
	  ;
	  MELT_LOCATION ("warmelt-outobj.melt:4901:/ quasiblock");


	  /*^checksignal */
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^apply */
	  /*apply */
	  {
	    union meltparam_un argtab[1];
	    memset (&argtab, 0, sizeof (argtab));
	    /*^apply.arg */
	    argtab[0].meltbp_aptr =
	      (melt_ptr_t *) & ( /*!DISCR_MULTIPLE */ meltfrout->tabval[15]);
	    /*_.PATHTUP__V119*/ meltfptr[118] =
	      melt_apply ((meltclosure_ptr_t)
			  (( /*!LIST_TO_MULTIPLE */ meltfrout->tabval[14])),
			  (melt_ptr_t) ( /*_.PATHLIST__V17*/ meltfptr[16]),
			  (MELTBPARSTR_PTR ""), argtab, "",
			  (union meltparam_un *) 0);
	  }
	  ;
	  MELT_LOCATION ("warmelt-outobj.melt:4902:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^apply */
	  /*apply */
	  {
	    union meltparam_un argtab[1];
	    memset (&argtab, 0, sizeof (argtab));
	    /*^apply.arg */
	    argtab[0].meltbp_aptr =
	      (melt_ptr_t *) & ( /*!DISCR_MULTIPLE */ meltfrout->tabval[15]);
	    /*_.MD5TUP__V120*/ meltfptr[119] =
	      melt_apply ((meltclosure_ptr_t)
			  (( /*!LIST_TO_MULTIPLE */ meltfrout->tabval[14])),
			  (melt_ptr_t) ( /*_.MD5LIST__V18*/ meltfptr[17]),
			  (MELTBPARSTR_PTR ""), argtab, "",
			  (union meltparam_un *) 0);
	  }
	  ;

#if MELT_HAVE_DEBUG
	  MELT_LOCATION ("warmelt-outobj.melt:4904:/ cppif.then");
	  /*^block */
	  /*anyblock */
	  {

	    /*^checksignal */
	    MELT_CHECK_SIGNAL ();
	    ;
    /*_#MULTIPLE_LENGTH__L28*/ meltfnum[18] =
	      (melt_multiple_length
	       ((melt_ptr_t) ( /*_.PATHTUP__V119*/ meltfptr[118])));;
	    /*^compute */
    /*_#MULTIPLE_LENGTH__L29*/ meltfnum[28] =
	      (melt_multiple_length
	       ((melt_ptr_t) ( /*_.MD5TUP__V120*/ meltfptr[119])));;
	    /*^compute */
    /*_#I__L30*/ meltfnum[29] =
	      (( /*_#MULTIPLE_LENGTH__L28*/ meltfnum[18]) ==
	       ( /*_#MULTIPLE_LENGTH__L29*/ meltfnum[28]));;
	    MELT_LOCATION ("warmelt-outobj.melt:4904:/ cond");
	    /*cond */ if ( /*_#I__L30*/ meltfnum[29])	/*then */
	      {
		/*^cond.then */
		/*_.IFELSE___V122*/ meltfptr[121] = ( /*nil */ NULL);;
	      }
	    else
	      {
		MELT_LOCATION ("warmelt-outobj.melt:4904:/ cond.else");

		/*^block */
		/*anyblock */
		{




		  {
		    /*^locexp */
		    melt_assert_failed (("check same length pathtup md5tup"),
					("warmelt-outobj.melt")
					? ("warmelt-outobj.melt") : __FILE__,
					(4904) ? (4904) : __LINE__,
					__FUNCTION__);
		    ;
		  }
		  ;
		/*clear *//*_.IFELSE___V122*/ meltfptr[121] = 0;
		  /*epilog */
		}
		;
	      }
	    ;
	    /*^compute */
	    /*_.IFCPP___V121*/ meltfptr[120] =
	      /*_.IFELSE___V122*/ meltfptr[121];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-outobj.melt:4904:/ clear");
	      /*clear *//*_#MULTIPLE_LENGTH__L28*/ meltfnum[18] = 0;
	    /*^clear */
	      /*clear *//*_#MULTIPLE_LENGTH__L29*/ meltfnum[28] = 0;
	    /*^clear */
	      /*clear *//*_#I__L30*/ meltfnum[29] = 0;
	    /*^clear */
	      /*clear *//*_.IFELSE___V122*/ meltfptr[121] = 0;
	  }

#else /*MELT_HAVE_DEBUG */
	  /*^cppif.else */
	  /*_.IFCPP___V121*/ meltfptr[120] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
	  ;
	  /*citerblock FOREACH_IN_MULTIPLE */
	  {
	    /* start foreach_in_multiple meltcit6__EACHTUP */
	    long meltcit6__EACHTUP_ln =
	      melt_multiple_length ((melt_ptr_t) /*_.PATHTUP__V119*/
				    meltfptr[118]);
	    for ( /*_#IX__L31*/ meltfnum[18] = 0;
		 ( /*_#IX__L31*/ meltfnum[18] >= 0)
		 && ( /*_#IX__L31*/ meltfnum[18] < meltcit6__EACHTUP_ln);
	/*_#IX__L31*/ meltfnum[18]++)
	      {
		/*_.CURPATH__V123*/ meltfptr[121] =
		  melt_multiple_nth ((melt_ptr_t)
				     ( /*_.PATHTUP__V119*/ meltfptr[118]),
				     /*_#IX__L31*/ meltfnum[18]);



		MELT_LOCATION ("warmelt-outobj.melt:4908:/ quasiblock");


   /*_.CURMD5__V125*/ meltfptr[124] =
		  (melt_multiple_nth
		   ((melt_ptr_t) ( /*_.MD5TUP__V120*/ meltfptr[119]),
		    ( /*_#IX__L31*/ meltfnum[18])));;

#if MELT_HAVE_DEBUG
		MELT_LOCATION ("warmelt-outobj.melt:4910:/ cppif.then");
		/*^block */
		/*anyblock */
		{


		  {
		    /*^locexp */
		    /*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
		    melt_dbgcounter++;
#endif
		    ;
		  }
		  ;
		  /*^checksignal */
		  MELT_CHECK_SIGNAL ();
		  ;
     /*_#MELT_NEED_DBG__L32*/ meltfnum[28] =
		    /*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
		    ( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
		    0		/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
		    ;;
		  MELT_LOCATION ("warmelt-outobj.melt:4910:/ cond");
		  /*cond */ if ( /*_#MELT_NEED_DBG__L32*/ meltfnum[28])	/*then */
		    {
		      /*^cond.then */
		      /*^block */
		      /*anyblock */
		      {

       /*_#THE_MELTCALLCOUNT__L33*/ meltfnum[29] =
#ifdef meltcallcount
			  meltcallcount	/* the_meltcallcount */
#else
			  0L
#endif /* meltcallcount the_meltcallcount */
			  ;;
			MELT_LOCATION
			  ("warmelt-outobj.melt:4910:/ checksignal");
			MELT_CHECK_SIGNAL ();
			;
			/*^apply */
			/*apply */
			{
			  union meltparam_un argtab[9];
			  memset (&argtab, 0, sizeof (argtab));
			  /*^apply.arg */
			  argtab[0].meltbp_long =
			    /*_#THE_MELTCALLCOUNT__L33*/ meltfnum[29];
			  /*^apply.arg */
			  argtab[1].meltbp_cstring = "warmelt-outobj.melt";
			  /*^apply.arg */
			  argtab[2].meltbp_long = 4910;
			  /*^apply.arg */
			  argtab[3].meltbp_cstring =
			    "output_melt_descriptor curpath=";
			  /*^apply.arg */
			  argtab[4].meltbp_aptr =
			    (melt_ptr_t *) & /*_.CURPATH__V123*/
			    meltfptr[121];
			  /*^apply.arg */
			  argtab[5].meltbp_cstring = " ix=";
			  /*^apply.arg */
			  argtab[6].meltbp_long = /*_#IX__L31*/ meltfnum[18];
			  /*^apply.arg */
			  argtab[7].meltbp_cstring = " curmd5=";
			  /*^apply.arg */
			  argtab[8].meltbp_aptr =
			    (melt_ptr_t *) & /*_.CURMD5__V125*/ meltfptr[124];
			  /*_.MELT_DEBUG_FUN__V128*/ meltfptr[127] =
			    melt_apply ((meltclosure_ptr_t)
					(( /*!MELT_DEBUG_FUN */ meltfrout->
					  tabval[0])),
					(melt_ptr_t) (( /*nil */ NULL)),
					(MELTBPARSTR_LONG MELTBPARSTR_CSTRING
					 MELTBPARSTR_LONG MELTBPARSTR_CSTRING
					 MELTBPARSTR_PTR MELTBPARSTR_CSTRING
					 MELTBPARSTR_LONG MELTBPARSTR_CSTRING
					 MELTBPARSTR_PTR ""), argtab, "",
					(union meltparam_un *) 0);
			}
			;
			/*_.IF___V127*/ meltfptr[126] =
			  /*_.MELT_DEBUG_FUN__V128*/ meltfptr[127];;
			/*epilog */

			MELT_LOCATION ("warmelt-outobj.melt:4910:/ clear");
		 /*clear *//*_#THE_MELTCALLCOUNT__L33*/ meltfnum[29] =
			  0;
			/*^clear */
		 /*clear *//*_.MELT_DEBUG_FUN__V128*/ meltfptr[127] =
			  0;
		      }
		      ;
		    }
		  else
		    {		/*^cond.else */

      /*_.IF___V127*/ meltfptr[126] = NULL;;
		    }
		  ;
		  MELT_LOCATION ("warmelt-outobj.melt:4910:/ quasiblock");


		  /*_.PROGN___V129*/ meltfptr[127] =
		    /*_.IF___V127*/ meltfptr[126];;
		  /*^compute */
		  /*_.IFCPP___V126*/ meltfptr[125] =
		    /*_.PROGN___V129*/ meltfptr[127];;
		  /*epilog */

		  MELT_LOCATION ("warmelt-outobj.melt:4910:/ clear");
	       /*clear *//*_#MELT_NEED_DBG__L32*/ meltfnum[28] = 0;
		  /*^clear */
	       /*clear *//*_.IF___V127*/ meltfptr[126] = 0;
		  /*^clear */
	       /*clear *//*_.PROGN___V129*/ meltfptr[127] = 0;
		}

#else /*MELT_HAVE_DEBUG */
		/*^cppif.else */
		/*_.IFCPP___V126*/ meltfptr[125] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
		;
		/*^compute */
   /*_.MAKE_STRING_NAKEDBASENAME__V130*/ meltfptr[126] =
		  (meltgc_new_string_nakedbasename
		   ((meltobject_ptr_t)
		    (( /*!DISCR_STRING */ meltfrout->tabval[4])),
		    melt_string_str ((melt_ptr_t)
				     ( /*_.CURPATH__V123*/ meltfptr[121]))));;
		MELT_LOCATION ("warmelt-outobj.melt:4911:/ checksignal");
		MELT_CHECK_SIGNAL ();
		;
		/*^apply */
		/*apply */
		{
		  union meltparam_un argtab[8];
		  memset (&argtab, 0, sizeof (argtab));
		  /*^apply.arg */
		  argtab[0].meltbp_cstring = " \\\n   ";
		  /*^apply.arg */
		  argtab[1].meltbp_cstring = "$(GCCMELTGEN_BUILD)";
		  /*^apply.arg */
		  argtab[2].meltbp_aptr =
		    (melt_ptr_t *) & /*_.MAKE_STRING_NAKEDBASENAME__V130*/
		    meltfptr[126];
		  /*^apply.arg */
		  argtab[3].meltbp_cstring = ".";
		  /*^apply.arg */
		  argtab[4].meltbp_aptr =
		    (melt_ptr_t *) & /*_.CURMD5__V125*/ meltfptr[124];
		  /*^apply.arg */
		  argtab[5].meltbp_cstring = ".";
		  /*^apply.arg */
		  argtab[6].meltbp_aptr =
		    (melt_ptr_t *) & /*_.CURFLAV__V115*/ meltfptr[114];
		  /*^apply.arg */
		  argtab[7].meltbp_cstring = ".meltpic.o";
		  /*_.ADD2OUT__V131*/ meltfptr[127] =
		    melt_apply ((meltclosure_ptr_t)
				(( /*!ADD2OUT */ meltfrout->tabval[13])),
				(melt_ptr_t) ( /*_.MKRULEBUF__V46*/
					      meltfptr[45]),
				(MELTBPARSTR_CSTRING MELTBPARSTR_CSTRING
				 MELTBPARSTR_PTR MELTBPARSTR_CSTRING
				 MELTBPARSTR_PTR MELTBPARSTR_CSTRING
				 MELTBPARSTR_PTR MELTBPARSTR_CSTRING ""),
				argtab, "", (union meltparam_un *) 0);
		}
		;
		/*_.LET___V124*/ meltfptr[123] =
		  /*_.ADD2OUT__V131*/ meltfptr[127];;

		MELT_LOCATION ("warmelt-outobj.melt:4908:/ clear");
	     /*clear *//*_.CURMD5__V125*/ meltfptr[124] = 0;
		/*^clear */
	     /*clear *//*_.IFCPP___V126*/ meltfptr[125] = 0;
		/*^clear */
	     /*clear *//*_.MAKE_STRING_NAKEDBASENAME__V130*/ meltfptr[126]
		  = 0;
		/*^clear */
	     /*clear *//*_.ADD2OUT__V131*/ meltfptr[127] = 0;
		if ( /*_#IX__L31*/ meltfnum[18] < 0)
		  break;
	      }			/* end  foreach_in_multiple meltcit6__EACHTUP */

	    /*citerepilog */

	    MELT_LOCATION ("warmelt-outobj.melt:4905:/ clear");
	     /*clear *//*_.CURPATH__V123*/ meltfptr[121] = 0;
	    /*^clear */
	     /*clear *//*_#IX__L31*/ meltfnum[18] = 0;
	    /*^clear */
	     /*clear *//*_.LET___V124*/ meltfptr[123] = 0;
	  }			/*endciterblock FOREACH_IN_MULTIPLE */
	  ;

	  MELT_LOCATION ("warmelt-outobj.melt:4901:/ clear");
	    /*clear *//*_.PATHTUP__V119*/ meltfptr[118] = 0;
	  /*^clear */
	    /*clear *//*_.MD5TUP__V120*/ meltfptr[119] = 0;
	  /*^clear */
	    /*clear *//*_.IFCPP___V121*/ meltfptr[120] = 0;
	  MELT_LOCATION ("warmelt-outobj.melt:4916:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^apply */
	  /*apply */
	  {
	    union meltparam_un argtab[1];
	    memset (&argtab, 0, sizeof (argtab));
	    /*^apply.arg */
	    argtab[0].meltbp_cstring = "\n\n";
	    /*_.ADD2OUT__V132*/ meltfptr[124] =
	      melt_apply ((meltclosure_ptr_t)
			  (( /*!ADD2OUT */ meltfrout->tabval[13])),
			  (melt_ptr_t) ( /*_.MKRULEBUF__V46*/ meltfptr[45]),
			  (MELTBPARSTR_CSTRING ""), argtab, "",
			  (union meltparam_un *) 0);
	  }
	  ;
	  if ( /*_#FLAVIX__L27*/ meltfnum[8] < 0)
	    break;
	}			/* end  foreach_in_multiple meltcit5__EACHTUP */

      /*citerepilog */

      MELT_LOCATION ("warmelt-outobj.melt:4892:/ clear");
	    /*clear *//*_.CURFLAV__V115*/ meltfptr[114] = 0;
      /*^clear */
	    /*clear *//*_#FLAVIX__L27*/ meltfnum[8] = 0;
      /*^clear */
	    /*clear *//*_.ADD2OUT__V116*/ meltfptr[115] = 0;
      /*^clear */
	    /*clear *//*_.ADD2OUT__V117*/ meltfptr[116] = 0;
      /*^clear */
	    /*clear *//*_.ADD2OUT__V118*/ meltfptr[117] = 0;
      /*^clear */
	    /*clear *//*_.ADD2OUT__V132*/ meltfptr[124] = 0;
    }				/*endciterblock FOREACH_IN_MULTIPLE */
    ;
    MELT_LOCATION ("warmelt-outobj.melt:4919:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[3];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_cstring = "\n#end of generated dependencies for ";
      /*^apply.arg */
      argtab[1].meltbp_aptr =
	(melt_ptr_t *) & /*_.MODNAMSTR__V2*/ meltfptr[1];
      /*^apply.arg */
      argtab[2].meltbp_cstring = "\n\n";
      /*_.ADD2OUT__V133*/ meltfptr[125] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!ADD2OUT */ meltfrout->tabval[13])),
		    (melt_ptr_t) ( /*_.MKRULEBUF__V46*/ meltfptr[45]),
		    (MELTBPARSTR_CSTRING MELTBPARSTR_PTR MELTBPARSTR_CSTRING
		     ""), argtab, "", (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:4920:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & /*_.MKRULEBUF__V46*/ meltfptr[45];
      /*_.ADD2OUT__V134*/ meltfptr[126] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!ADD2OUT */ meltfrout->tabval[13])),
		    (melt_ptr_t) ( /*_.MKBUF__V16*/ meltfptr[15]),
		    (MELTBPARSTR_PTR ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:4921:/ compute");
    /*_.MKRULEBUF__V46*/ meltfptr[45] = /*_.SETQ___V135*/ meltfptr[127] =
      ( /*nil */ NULL);;
    MELT_LOCATION ("warmelt-outobj.melt:4922:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_cstring = "## cumulated checksum and naked name\n";
      /*_.ADD2OUT__V136*/ meltfptr[118] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!ADD2OUT */ meltfrout->tabval[13])),
		    (melt_ptr_t) ( /*_.MKBUF__V16*/ meltfptr[15]),
		    (MELTBPARSTR_CSTRING ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:4923:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[5];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_cstring = "MELTGENMOD_CUMULATED_MD5SUM_";
      /*^apply.arg */
      argtab[1].meltbp_aptr =
	(melt_ptr_t *) & /*_.MODCIDENT__V23*/ meltfptr[20];
      /*^apply.arg */
      argtab[2].meltbp_cstring = "=";
      /*^apply.arg */
      argtab[3].meltbp_aptr =
	(melt_ptr_t *) & /*_.CUMPATHMDS__V102*/ meltfptr[101];
      /*^apply.arg */
      argtab[4].meltbp_cstring = "\n";
      /*_.ADD2OUT__V137*/ meltfptr[119] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!ADD2OUT */ meltfrout->tabval[13])),
		    (melt_ptr_t) ( /*_.MKBUF__V16*/ meltfptr[15]),
		    (MELTBPARSTR_CSTRING MELTBPARSTR_PTR MELTBPARSTR_CSTRING
		     MELTBPARSTR_PTR MELTBPARSTR_CSTRING ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:4924:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[5];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_cstring = "MELTGENMOD_NAKED_NAME_";
      /*^apply.arg */
      argtab[1].meltbp_aptr =
	(melt_ptr_t *) & /*_.MODCIDENT__V23*/ meltfptr[20];
      /*^apply.arg */
      argtab[2].meltbp_cstring = "=";
      /*^apply.arg */
      argtab[3].meltbp_aptr =
	(melt_ptr_t *) & /*_.MIDSTR__V103*/ meltfptr[102];
      /*^apply.arg */
      argtab[4].meltbp_cstring = "\n";
      /*_.ADD2OUT__V138*/ meltfptr[120] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!ADD2OUT */ meltfrout->tabval[13])),
		    (melt_ptr_t) ( /*_.MKBUF__V16*/ meltfptr[15]),
		    (MELTBPARSTR_CSTRING MELTBPARSTR_PTR MELTBPARSTR_CSTRING
		     MELTBPARSTR_PTR MELTBPARSTR_CSTRING ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:4925:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.DEBUF__V14*/ meltfptr[13]),
				(0), 0);
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:4926:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[7];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_cstring =
	"/* cumulated checksum of primary & secondary files */\
\nconst char melt_cumulated_hexmd5[]=\"";
      /*^apply.arg */
      argtab[1].meltbp_aptr =
	(melt_ptr_t *) & /*_.CUMPATHMDS__V102*/ meltfptr[101];
      /*^apply.arg */
      argtab[2].meltbp_cstring = "\" ;\n\n/* include the timestamp file */\
\n#define ";
      /*^apply.arg */
      argtab[3].meltbp_aptr =
	(melt_ptr_t *) & /*_.MODIDNAM__V109*/ meltfptr[104];
      /*^apply.arg */
      argtab[4].meltbp_cstring = " 1\n#include \"";
      /*^apply.arg */
      argtab[5].meltbp_aptr =
	(melt_ptr_t *) & /*_.MIDSTR__V103*/ meltfptr[102];
      /*^apply.arg */
      argtab[6].meltbp_cstring = "+melttime.h\"\n\t ";
      /*_.ADD2OUT__V139*/ meltfptr[138] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!ADD2OUT */ meltfrout->tabval[13])),
		    (melt_ptr_t) ( /*_.DEBUF__V14*/ meltfptr[13]),
		    (MELTBPARSTR_CSTRING MELTBPARSTR_PTR MELTBPARSTR_CSTRING
		     MELTBPARSTR_PTR MELTBPARSTR_CSTRING MELTBPARSTR_PTR
		     MELTBPARSTR_CSTRING ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:4938:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.TIBUF__V15*/ meltfptr[14]),
			   ("/** GENERATED MELT TIMESTAMP FILE "));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:4939:/ locexp");
      meltgc_add_strbuf_ccomment ((melt_ptr_t)
				  ( /*_.TIBUF__V15*/ meltfptr[14]),
				  melt_string_str ((melt_ptr_t)
						   ( /*_.MODNAMSTR__V2*/
						    meltfptr[1])));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:4940:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.TIBUF__V15*/ meltfptr[14]),
			   ("+melttime.h \n** NEVER EDIT OR MOVE THIS, IT IS GENERATED & PARSED\
! **/"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:4941:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.TIBUF__V15*/ meltfptr[14]),
				(0), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:4942:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.TIBUF__V15*/ meltfptr[14]),
			   ("/* These identifiers are generated in warmelt-outobj.melt \
\n & handled in melt-runtime.c carefully. */"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:4943:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.TIBUF__V15*/ meltfptr[14]),
				(0), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:4944:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.TIBUF__V15*/ meltfptr[14]),
				(0), 0);
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:4945:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[9];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_cstring = "\n/* This ";
      /*^apply.arg */
      argtab[1].meltbp_aptr =
	(melt_ptr_t *) & /*_.MIDSTR__V103*/ meltfptr[102];
      /*^apply.arg */
      argtab[2].meltbp_cstring = "+melttime.h is included from ";
      /*^apply.arg */
      argtab[3].meltbp_aptr =
	(melt_ptr_t *) & /*_.MIDSTR__V103*/ meltfptr[102];
      /*^apply.arg */
      argtab[4].meltbp_cstring = "+meltdesc.c only. */\n#if ";
      /*^apply.arg */
      argtab[5].meltbp_aptr =
	(melt_ptr_t *) & /*_.MODIDNAM__V109*/ meltfptr[104];
      /*^apply.arg */
      argtab[6].meltbp_cstring = "\n/* MELT generation timestamp for ";
      /*^apply.arg */
      argtab[7].meltbp_aptr =
	(melt_ptr_t *) & /*_.MODNAMSTR__V2*/ meltfptr[1];
      /*^apply.arg */
      argtab[8].meltbp_cstring =
	" */\n\n#ifdef __cplusplus\n/* these symbols are extern \"C\" since\
 dlsym-ed */\nextern \"C\" const char melt_gen_timestamp[] ;\
\nextern \"C\" const long long melt_gen_timenum ;\
\nextern \"C\" const char melt_build_timestamp[] ;\
\nextern \"C\" {\n#endif /*__cplusplu\
s */\n\n\t\t ";
      /*_.ADD2OUT__V140*/ meltfptr[139] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!ADD2OUT */ meltfrout->tabval[13])),
		    (melt_ptr_t) ( /*_.TIBUF__V15*/ meltfptr[14]),
		    (MELTBPARSTR_CSTRING MELTBPARSTR_PTR MELTBPARSTR_CSTRING
		     MELTBPARSTR_PTR MELTBPARSTR_CSTRING MELTBPARSTR_PTR
		     MELTBPARSTR_CSTRING MELTBPARSTR_PTR MELTBPARSTR_CSTRING
		     ""), argtab, "", (union meltparam_un *) 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:4960:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.TIBUF__V15*/ meltfptr[14]),
				(0), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:4963:/ locexp");
      /* output_melt_descriptor GENTIMCH__1 + */
      {
	time_t now = 0;
	char nowbuf[64];
	time (&now);
	memset (nowbuf, 0, sizeof (nowbuf));
	strftime (nowbuf, sizeof (nowbuf) - 1, "%c %Z", localtime (&now));
	if (melt_flag_bootstrapping)
	  meltgc_add_strbuf ((melt_ptr_t) /*_.TIBUF__V15*/ meltfptr[14],
			     "/*MELT BOOTSTRAP*/\n");
	meltgc_add_strbuf ((melt_ptr_t) /*_.TIBUF__V15*/ meltfptr[14],
			   "const char melt_gen_timestamp[]=\"");
	meltgc_add_strbuf_cstr ((melt_ptr_t) /*_.TIBUF__V15*/ meltfptr[14],
				nowbuf);
	meltgc_add_strbuf ((melt_ptr_t) /*_.TIBUF__V15*/ meltfptr[14],
			   "\";\n");
	/*  GENTIMCH__1 don't use time_t, it is not a predefined C type! */
	meltgc_strbuf_printf ((melt_ptr_t) /*_.TIBUF__V15*/ meltfptr[14],
			      "const long long melt_gen_timenum=%lld;",
			      (long long) now);
      } /* output_melt_descriptor GENTIMCH__1 - */ ;
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:4980:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.TIBUF__V15*/ meltfptr[14]),
				(0), 0);
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:4981:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_cstring =
	"\n\t\t const char melt_build_timestamp[]= __DATE__ \"@\" __TIME__\
\n\t\t #ifdef __cplusplus\n\t\t \" (in C++)\"\
\n\t\t #else\n\t\t \" (in C)\"\n\t\t #endif /*__cplus\
plus*/\n\t\t\t\t\t;\n\t\t ";
      /*_.ADD2OUT__V141*/ meltfptr[140] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!ADD2OUT */ meltfrout->tabval[13])),
		    (melt_ptr_t) ( /*_.TIBUF__V15*/ meltfptr[14]),
		    (MELTBPARSTR_CSTRING ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:4990:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.TIBUF__V15*/ meltfptr[14]),
				(0), 0);
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:4991:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[7];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_cstring =
	"\n\t\t #ifdef __cplusplus\n\t\t }  /* end extern C timestamp */\
\n\t\t #endif /*__cplusplus */\n\n\t\t #els\
e /* ! ";
      /*^apply.arg */
      argtab[1].meltbp_aptr =
	(melt_ptr_t *) & /*_.MODIDNAM__V109*/ meltfptr[104];
      /*^apply.arg */
      argtab[2].meltbp_cstring =
	" */\n\t\t #error invalid timestamp file for ";
      /*^apply.arg */
      argtab[3].meltbp_aptr =
	(melt_ptr_t *) & /*_.MODNAMSTR__V2*/ meltfptr[1];
      /*^apply.arg */
      argtab[4].meltbp_cstring = " \n\t\t #endif /* ";
      /*^apply.arg */
      argtab[5].meltbp_aptr =
	(melt_ptr_t *) & /*_.MODIDNAM__V109*/ meltfptr[104];
      /*^apply.arg */
      argtab[6].meltbp_cstring = " */\n\t\t ";
      /*_.ADD2OUT__V142*/ meltfptr[141] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!ADD2OUT */ meltfrout->tabval[13])),
		    (melt_ptr_t) ( /*_.TIBUF__V15*/ meltfptr[14]),
		    (MELTBPARSTR_CSTRING MELTBPARSTR_PTR MELTBPARSTR_CSTRING
		     MELTBPARSTR_PTR MELTBPARSTR_CSTRING MELTBPARSTR_PTR
		     MELTBPARSTR_CSTRING ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:5000:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.TIBUF__V15*/ meltfptr[14]),
				(0), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:5002:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.DEBUF__V14*/ meltfptr[13]),
				(0), 0);
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:5003:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_cstring =
	"\n\t\t #ifdef __cplusplus\n\t\t }\t  /* end extern C descriptor */\
\
\n\t\t #endif /*__cplusplus\
 */\n\t\t ";
      /*_.ADD2OUT__V143*/ meltfptr[142] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!ADD2OUT */ meltfrout->tabval[13])),
		    (melt_ptr_t) ( /*_.DEBUF__V14*/ meltfptr[13]),
		    (MELTBPARSTR_CSTRING ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:5008:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.DEBUF__V14*/ meltfptr[13]),
				(0), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:5009:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.DEBUF__V14*/ meltfptr[13]),
			   ("/* end of melt descriptor file */"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:5010:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.DEBUF__V14*/ meltfptr[13]),
				(0), 0);
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:5011:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[3];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_cstring = "\n## eof of generated ";
      /*^apply.arg */
      argtab[1].meltbp_aptr =
	(melt_ptr_t *) & /*_.MODNAMSTR__V2*/ meltfptr[1];
      /*^apply.arg */
      argtab[2].meltbp_cstring = "+meltbuild.mk\n";
      /*_.ADD2OUT__V144*/ meltfptr[143] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!ADD2OUT */ meltfrout->tabval[13])),
		    (melt_ptr_t) ( /*_.MKBUF__V16*/ meltfptr[15]),
		    (MELTBPARSTR_CSTRING MELTBPARSTR_PTR MELTBPARSTR_CSTRING
		     ""), argtab, "", (union meltparam_un *) 0);
    }
    ;
    /*_.LET___V100*/ meltfptr[59] = /*_.ADD2OUT__V144*/ meltfptr[143];;

    MELT_LOCATION ("warmelt-outobj.melt:4872:/ clear");
	   /*clear *//*_.LIST_TO_MULTIPLE__V101*/ meltfptr[100] = 0;
    /*^clear */
	   /*clear *//*_.CUMPATHMDS__V102*/ meltfptr[101] = 0;
    /*^clear */
	   /*clear *//*_.MIDSTR__V103*/ meltfptr[102] = 0;
    /*^clear */
	   /*clear *//*_.LET___V104*/ meltfptr[103] = 0;
    /*^clear */
	   /*clear *//*_.MODIDNAM__V109*/ meltfptr[104] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V110*/ meltfptr[105] = 0;
    /*^clear */
	   /*clear *//*_.ADD2OUT__V112*/ meltfptr[107] = 0;
    /*^clear */
	   /*clear *//*_.ADD2OUT__V113*/ meltfptr[106] = 0;
    /*^clear */
	   /*clear *//*_.ADD2OUT__V114*/ meltfptr[113] = 0;
    /*^clear */
	   /*clear *//*_.ADD2OUT__V133*/ meltfptr[125] = 0;
    /*^clear */
	   /*clear *//*_.ADD2OUT__V134*/ meltfptr[126] = 0;
    /*^clear */
	   /*clear *//*_.SETQ___V135*/ meltfptr[127] = 0;
    /*^clear */
	   /*clear *//*_.ADD2OUT__V136*/ meltfptr[118] = 0;
    /*^clear */
	   /*clear *//*_.ADD2OUT__V137*/ meltfptr[119] = 0;
    /*^clear */
	   /*clear *//*_.ADD2OUT__V138*/ meltfptr[120] = 0;
    /*^clear */
	   /*clear *//*_.ADD2OUT__V139*/ meltfptr[138] = 0;
    /*^clear */
	   /*clear *//*_.ADD2OUT__V140*/ meltfptr[139] = 0;
    /*^clear */
	   /*clear *//*_.ADD2OUT__V141*/ meltfptr[140] = 0;
    /*^clear */
	   /*clear *//*_.ADD2OUT__V142*/ meltfptr[141] = 0;
    /*^clear */
	   /*clear *//*_.ADD2OUT__V143*/ meltfptr[142] = 0;
    /*^clear */
	   /*clear *//*_.ADD2OUT__V144*/ meltfptr[143] = 0;
    MELT_LOCATION ("warmelt-outobj.melt:5013:/ quasiblock");


    MELT_LOCATION ("warmelt-outobj.melt:5014:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[2];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & /*_.MODNAMSTR__V2*/ meltfptr[1];
      /*^apply.arg */
      argtab[1].meltbp_cstring = "+meltdesc.c";
      /*_.MELTDESCPATH__V146*/ meltfptr[101] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!STRING4OUT */ meltfrout->tabval[5])),
		    (melt_ptr_t) (( /*!DISCR_STRING */ meltfrout->tabval[4])),
		    (MELTBPARSTR_PTR MELTBPARSTR_CSTRING ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:5015:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[2];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & /*_.MODNAMSTR__V2*/ meltfptr[1];
      /*^apply.arg */
      argtab[1].meltbp_cstring = "+melttime.h";
      /*_.MELTTIMEPATH__V147*/ meltfptr[102] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!STRING4OUT */ meltfrout->tabval[5])),
		    (melt_ptr_t) (( /*!DISCR_STRING */ meltfrout->tabval[4])),
		    (MELTBPARSTR_PTR MELTBPARSTR_CSTRING ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:5016:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[2];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & /*_.MODNAMSTR__V2*/ meltfptr[1];
      /*^apply.arg */
      argtab[1].meltbp_cstring = "+meltbuild.mk";
      /*_.MELTMAKEPATH__V148*/ meltfptr[103] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!STRING4OUT */ meltfrout->tabval[5])),
		    (melt_ptr_t) (( /*!DISCR_STRING */ meltfrout->tabval[4])),
		    (MELTBPARSTR_PTR MELTBPARSTR_CSTRING ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:5018:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L34*/ meltfnum[29] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-outobj.melt:5018:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L34*/ meltfnum[29])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L35*/ meltfnum[28] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-outobj.melt:5018:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[7];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long =
		/*_#THE_MELTCALLCOUNT__L35*/ meltfnum[28];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-outobj.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 5018;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring =
		"output_melt_descriptor meltdescpath=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.MELTDESCPATH__V146*/ meltfptr[101];
	      /*^apply.arg */
	      argtab[5].meltbp_cstring = "\n* debuf=";
	      /*^apply.arg */
	      argtab[6].meltbp_aptr =
		(melt_ptr_t *) & /*_.DEBUF__V14*/ meltfptr[13];
	      /*_.MELT_DEBUG_FUN__V151*/ meltfptr[107] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V150*/ meltfptr[105] =
	      /*_.MELT_DEBUG_FUN__V151*/ meltfptr[107];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-outobj.melt:5018:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L35*/ meltfnum[28] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V151*/ meltfptr[107] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V150*/ meltfptr[105] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-outobj.melt:5018:/ quasiblock");


      /*_.PROGN___V152*/ meltfptr[106] = /*_.IF___V150*/ meltfptr[105];;
      /*^compute */
      /*_.IFCPP___V149*/ meltfptr[104] = /*_.PROGN___V152*/ meltfptr[106];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:5018:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L34*/ meltfnum[29] = 0;
      /*^clear */
	     /*clear *//*_.IF___V150*/ meltfptr[105] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V152*/ meltfptr[106] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V149*/ meltfptr[104] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:5020:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L36*/ meltfnum[28] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-outobj.melt:5020:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L36*/ meltfnum[28])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L37*/ meltfnum[29] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-outobj.melt:5020:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[7];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long =
		/*_#THE_MELTCALLCOUNT__L37*/ meltfnum[29];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-outobj.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 5020;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring =
		"output_melt_descriptor melttimepath=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.MELTTIMEPATH__V147*/ meltfptr[102];
	      /*^apply.arg */
	      argtab[5].meltbp_cstring = "\n* tibuf=";
	      /*^apply.arg */
	      argtab[6].meltbp_aptr =
		(melt_ptr_t *) & /*_.TIBUF__V15*/ meltfptr[14];
	      /*_.MELT_DEBUG_FUN__V155*/ meltfptr[126] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V154*/ meltfptr[125] =
	      /*_.MELT_DEBUG_FUN__V155*/ meltfptr[126];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-outobj.melt:5020:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L37*/ meltfnum[29] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V155*/ meltfptr[126] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V154*/ meltfptr[125] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-outobj.melt:5020:/ quasiblock");


      /*_.PROGN___V156*/ meltfptr[127] = /*_.IF___V154*/ meltfptr[125];;
      /*^compute */
      /*_.IFCPP___V153*/ meltfptr[113] = /*_.PROGN___V156*/ meltfptr[127];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:5020:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L36*/ meltfnum[28] = 0;
      /*^clear */
	     /*clear *//*_.IF___V154*/ meltfptr[125] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V156*/ meltfptr[127] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V153*/ meltfptr[113] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:5022:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L38*/ meltfnum[29] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-outobj.melt:5022:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L38*/ meltfnum[29])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L39*/ meltfnum[28] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-outobj.melt:5022:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[7];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long =
		/*_#THE_MELTCALLCOUNT__L39*/ meltfnum[28];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-outobj.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 5022;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring =
		"output_melt_descriptor meltmakepath=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.MELTMAKEPATH__V148*/ meltfptr[103];
	      /*^apply.arg */
	      argtab[5].meltbp_cstring = "\n* mkbuf=";
	      /*^apply.arg */
	      argtab[6].meltbp_aptr =
		(melt_ptr_t *) & /*_.MKBUF__V16*/ meltfptr[15];
	      /*_.MELT_DEBUG_FUN__V159*/ meltfptr[120] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V158*/ meltfptr[119] =
	      /*_.MELT_DEBUG_FUN__V159*/ meltfptr[120];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-outobj.melt:5022:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L39*/ meltfnum[28] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V159*/ meltfptr[120] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V158*/ meltfptr[119] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-outobj.melt:5022:/ quasiblock");


      /*_.PROGN___V160*/ meltfptr[138] = /*_.IF___V158*/ meltfptr[119];;
      /*^compute */
      /*_.IFCPP___V157*/ meltfptr[118] = /*_.PROGN___V160*/ meltfptr[138];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:5022:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L38*/ meltfnum[29] = 0;
      /*^clear */
	     /*clear *//*_.IF___V158*/ meltfptr[119] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V160*/ meltfptr[138] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V157*/ meltfptr[118] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:5024:/ locexp");
      /*output_sbuf_no_overwrite_strval */
	melt_output_strbuf_to_file_no_overwrite ((melt_ptr_t)
						 ( /*_.DEBUF__V14*/
						  meltfptr[13]),
						 melt_string_str ((melt_ptr_t)
								  /*_.MELTDESCPATH__V146*/
								  meltfptr
								  [101]));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:5025:/ locexp");
      /*output_sbuf_no_overwrite_strval */
	melt_output_strbuf_to_file_no_overwrite ((melt_ptr_t)
						 ( /*_.TIBUF__V15*/
						  meltfptr[14]),
						 melt_string_str ((melt_ptr_t)
								  /*_.MELTTIMEPATH__V147*/
								  meltfptr
								  [102]));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:5026:/ locexp");
      /*output_sbuf_no_overwrite_strval */
	melt_output_strbuf_to_file_no_overwrite ((melt_ptr_t)
						 ( /*_.MKBUF__V16*/
						  meltfptr[15]),
						 melt_string_str ((melt_ptr_t)
								  /*_.MELTMAKEPATH__V148*/
								  meltfptr
								  [103]));
    }
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:5027:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L40*/ meltfnum[28] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-outobj.melt:5027:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L40*/ meltfnum[28])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L41*/ meltfnum[29] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-outobj.melt:5027:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long =
		/*_#THE_MELTCALLCOUNT__L41*/ meltfnum[29];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-outobj.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 5027;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring =
		"output_melt_descriptor final modctx=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.MODCTX__V4*/ meltfptr[3];
	      /*_.MELT_DEBUG_FUN__V163*/ meltfptr[141] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V162*/ meltfptr[140] =
	      /*_.MELT_DEBUG_FUN__V163*/ meltfptr[141];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-outobj.melt:5027:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L41*/ meltfnum[29] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V163*/ meltfptr[141] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V162*/ meltfptr[140] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-outobj.melt:5027:/ quasiblock");


      /*_.PROGN___V164*/ meltfptr[142] = /*_.IF___V162*/ meltfptr[140];;
      /*^compute */
      /*_.IFCPP___V161*/ meltfptr[139] = /*_.PROGN___V164*/ meltfptr[142];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:5027:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L40*/ meltfnum[28] = 0;
      /*^clear */
	     /*clear *//*_.IF___V162*/ meltfptr[140] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V164*/ meltfptr[142] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V161*/ meltfptr[139] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    /*^compute */
    /*_.LET___V145*/ meltfptr[100] = /*_.IFCPP___V161*/ meltfptr[139];;

    MELT_LOCATION ("warmelt-outobj.melt:5013:/ clear");
	   /*clear *//*_.MELTDESCPATH__V146*/ meltfptr[101] = 0;
    /*^clear */
	   /*clear *//*_.MELTTIMEPATH__V147*/ meltfptr[102] = 0;
    /*^clear */
	   /*clear *//*_.MELTMAKEPATH__V148*/ meltfptr[103] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V149*/ meltfptr[104] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V153*/ meltfptr[113] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V157*/ meltfptr[118] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V161*/ meltfptr[139] = 0;
    /*_.LET___V43*/ meltfptr[42] = /*_.LET___V145*/ meltfptr[100];;

    MELT_LOCATION ("warmelt-outobj.melt:4767:/ clear");
	   /*clear *//*_.PRIMPATH__V44*/ meltfptr[43] = 0;
    /*^clear */
	   /*clear *//*_.PRIMMD5S__V45*/ meltfptr[44] = 0;
    /*^clear */
	   /*clear *//*_.MKRULEBUF__V46*/ meltfptr[45] = 0;
    /*^clear */
	   /*clear *//*_.SECMD5BUF__V47*/ meltfptr[46] = 0;
    /*^clear */
	   /*clear *//*_.SECPATHBUF__V48*/ meltfptr[47] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V49*/ meltfptr[48] = 0;
    /*^clear */
	   /*clear *//*_.ADD2OUT__V53*/ meltfptr[49] = 0;
    /*^clear */
	   /*clear *//*_.ADD2OUT__V54*/ meltfptr[50] = 0;
    /*^clear */
	   /*clear *//*_.ADD2OUT__V55*/ meltfptr[54] = 0;
    /*^clear */
	   /*clear *//*_.LET___V56*/ meltfptr[55] = 0;
    /*^clear */
	   /*clear *//*_.ADD2OUT__V73*/ meltfptr[67] = 0;
    /*^clear */
	   /*clear *//*_.ADD2OUT__V74*/ meltfptr[68] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V77*/ meltfptr[56] = 0;
    /*^clear */
	   /*clear *//*_.ADD2OUT__V95*/ meltfptr[83] = 0;
    /*^clear */
	   /*clear *//*_.STRBUF2STRING__V96*/ meltfptr[88] = 0;
    /*^clear */
	   /*clear *//*_.ADD2OUT__V97*/ meltfptr[89] = 0;
    /*^clear */
	   /*clear *//*_.STRBUF2STRING__V98*/ meltfptr[90] = 0;
    /*^clear */
	   /*clear *//*_.ADD2OUT__V99*/ meltfptr[91] = 0;
    /*^clear */
	   /*clear *//*_.LET___V100*/ meltfptr[59] = 0;
    /*^clear */
	   /*clear *//*_.LET___V145*/ meltfptr[100] = 0;
    /*_.LET___V13*/ meltfptr[11] = /*_.LET___V43*/ meltfptr[42];;

    MELT_LOCATION ("warmelt-outobj.melt:4629:/ clear");
	   /*clear *//*_#NBSECFILES__L5*/ meltfnum[1] = 0;
    /*^clear */
	   /*clear *//*_#LASTSECFILEIX__L6*/ meltfnum[0] = 0;
    /*^clear */
	   /*clear *//*_.DEBUF__V14*/ meltfptr[13] = 0;
    /*^clear */
	   /*clear *//*_.TIBUF__V15*/ meltfptr[14] = 0;
    /*^clear */
	   /*clear *//*_.MKBUF__V16*/ meltfptr[15] = 0;
    /*^clear */
	   /*clear *//*_.PATHLIST__V17*/ meltfptr[16] = 0;
    /*^clear */
	   /*clear *//*_.MD5LIST__V18*/ meltfptr[17] = 0;
    /*^clear */
	   /*clear *//*_.MODBUILDSTR__V19*/ meltfptr[18] = 0;
    /*^clear */
	   /*clear *//*_.LET___V20*/ meltfptr[19] = 0;
    /*^clear */
	   /*clear *//*_.MODCIDENT__V23*/ meltfptr[20] = 0;
    /*^clear */
	   /*clear *//*_.LET___V24*/ meltfptr[21] = 0;
    /*^clear */
	   /*clear *//*_.MODBUILDCIDENT__V27*/ meltfptr[24] = 0;
    /*^clear */
	   /*clear *//*_.MODPREFSTR__V28*/ meltfptr[25] = 0;
    /*^clear */
	   /*clear *//*_.PACKAGEPCLIST__V29*/ meltfptr[28] = 0;
    /*^clear */
	   /*clear *//*_#IS_A__L7*/ meltfnum[6] = 0;
    /*^clear */
	   /*clear *//*_.FLAVORTUPLE__V30*/ meltfptr[29] = 0;
    /*^clear */
	   /*clear *//*_.ADD2OUT__V35*/ meltfptr[30] = 0;
    /*^clear */
	   /*clear *//*_.ADD2OUT__V36*/ meltfptr[31] = 0;
    /*^clear */
	   /*clear *//*_.ADD2OUT__V37*/ meltfptr[36] = 0;
    /*^clear */
	   /*clear *//*_.ADD2OUT__V38*/ meltfptr[37] = 0;
    /*^clear */
	   /*clear *//*_.ADD2OUT__V39*/ meltfptr[38] = 0;
    /*^clear */
	   /*clear *//*_.ADD2OUT__V40*/ meltfptr[39] = 0;
    /*^clear */
	   /*clear *//*_.ADD2OUT__V41*/ meltfptr[40] = 0;
    /*^clear */
	   /*clear *//*_.ADD2OUT__V42*/ meltfptr[41] = 0;
    /*^clear */
	   /*clear *//*_.LET___V43*/ meltfptr[42] = 0;
    MELT_LOCATION ("warmelt-outobj.melt:4625:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.LET___V13*/ meltfptr[11];;

    {
      MELT_LOCATION ("warmelt-outobj.melt:4625:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V5*/ meltfptr[4] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V9*/ meltfptr[5] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V11*/ meltfptr[6] = 0;
    /*^clear */
	   /*clear *//*_.LET___V13*/ meltfptr[11] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("OUTPUT_MELT_DESCRIPTOR", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_123_warmelt_outobj_OUTPUT_MELT_DESCRIPTOR_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_123_warmelt_outobj_OUTPUT_MELT_DESCRIPTOR */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_124_warmelt_outobj_SYNTESTGEN_ANY (meltclosure_ptr_t meltclosp_,
					    melt_ptr_t meltfirstargp_,
					    const melt_argdescr_cell_t
					    meltxargdescr_[],
					    union meltparam_un * meltxargtab_,
					    const melt_argdescr_cell_t
					    meltxresdescr_[],
					    union meltparam_un * meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_124_warmelt_outobj_SYNTESTGEN_ANY_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_124_warmelt_outobj_SYNTESTGEN_ANY_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 21
    melt_ptr_t mcfr_varptr[21];
#define MELTFRAM_NBVARNUM 6
    long mcfr_varnum[6];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_124_warmelt_outobj_SYNTESTGEN_ANY is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_124_warmelt_outobj_SYNTESTGEN_ANY_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 21; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_124_warmelt_outobj_SYNTESTGEN_ANY nbval 21*/
  meltfram__.mcfr_nbvar = 21 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("SYNTESTGEN_ANY", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-outobj.melt:5044:/ getarg");
 /*_.RECV__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.GENDEV__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.GENDEV__V3*/ meltfptr[2])) !=
	      NULL);


  /*getarg#2 */
  /*^getarg */
  if (meltxargdescr_[1] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.SBUF__V4*/ meltfptr[3] =
    (meltxargtab_[1].meltbp_aptr) ? (*(meltxargtab_[1].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.SBUF__V4*/ meltfptr[3])) !=
	      NULL);


  /*getarg#3 */
  /*^getarg */
  if (meltxargdescr_[2] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.MODCTX__V5*/ meltfptr[4] =
    (meltxargtab_[2].meltbp_aptr) ? (*(meltxargtab_[2].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.MODCTX__V5*/ meltfptr[4])) !=
	      NULL);


  /*getarg#4 */
  /*^getarg */
  if (meltxargdescr_[3] != MELTBPAR_LONG)
    goto lab_endgetargs;
 /*_#IX__L1*/ meltfnum[0] = meltxargtab_[3].meltbp_long;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {

    MELT_LOCATION ("warmelt-outobj.melt:5045:/ quasiblock");


 /*_.DIS__V7*/ meltfptr[6] =
      ((melt_ptr_t)
       (melt_discr ((melt_ptr_t) ( /*_.RECV__V2*/ meltfptr[1]))));;
    MELT_LOCATION ("warmelt-outobj.melt:5046:/ cond");
    /*cond */ if (
		   /*ifisa */
		   melt_is_instance_of ((melt_ptr_t)
					( /*_.DIS__V7*/ meltfptr[6]),
					(melt_ptr_t) (( /*!CLASS_NAMED */
						       meltfrout->tabval[0])))
      )				/*then */
      {
	/*^cond.then */
	/*^getslot */
	{
	  melt_ptr_t slot = NULL, obj = NULL;
	  obj = (melt_ptr_t) ( /*_.DIS__V7*/ meltfptr[6]) /*=obj*/ ;
	  melt_object_get_field (slot, obj, 1, "NAMED_NAME");
   /*_.DISNAME__V8*/ meltfptr[7] = slot;
	};
	;
      }
    else
      {				/*^cond.else */

  /*_.DISNAME__V8*/ meltfptr[7] = NULL;;
      }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:5047:/ cond");
    /*cond */ if (
		   /*ifisa */
		   melt_is_instance_of ((melt_ptr_t)
					( /*_.GENDEV__V3*/ meltfptr[2]),
					(melt_ptr_t) (( /*!CLASS_LOCATED */
						       meltfrout->tabval[1])))
      )				/*then */
      {
	/*^cond.then */
	/*^getslot */
	{
	  melt_ptr_t slot = NULL, obj = NULL;
	  obj = (melt_ptr_t) ( /*_.GENDEV__V3*/ meltfptr[2]) /*=obj*/ ;
	  melt_object_get_field (slot, obj, 1, "LOCA_LOCATION");
   /*_.DLOC__V9*/ meltfptr[8] = slot;
	};
	;
      }
    else
      {				/*^cond.else */

  /*_.DLOC__V9*/ meltfptr[8] = NULL;;
      }
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:5049:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L2*/ meltfnum[1] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-outobj.melt:5049:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L2*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L3*/ meltfnum[2] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-outobj.melt:5049:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[15];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L3*/ meltfnum[2];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-outobj.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 5049;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "syntestgen_any recv=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.RECV__V2*/ meltfptr[1];
	      /*^apply.arg */
	      argtab[5].meltbp_cstring = "\n* dis=";
	      /*^apply.arg */
	      argtab[6].meltbp_aptr =
		(melt_ptr_t *) & /*_.DIS__V7*/ meltfptr[6];
	      /*^apply.arg */
	      argtab[7].meltbp_cstring = "\n* gendev=";
	      /*^apply.arg */
	      argtab[8].meltbp_aptr =
		(melt_ptr_t *) & /*_.GENDEV__V3*/ meltfptr[2];
	      /*^apply.arg */
	      argtab[9].meltbp_cstring = "\n* sbuf=";
	      /*^apply.arg */
	      argtab[10].meltbp_aptr =
		(melt_ptr_t *) & /*_.SBUF__V4*/ meltfptr[3];
	      /*^apply.arg */
	      argtab[11].meltbp_cstring = "\n* modctx=";
	      /*^apply.arg */
	      argtab[12].meltbp_aptr =
		(melt_ptr_t *) & /*_.MODCTX__V5*/ meltfptr[4];
	      /*^apply.arg */
	      argtab[13].meltbp_cstring = "\n* ix=";
	      /*^apply.arg */
	      argtab[14].meltbp_long = /*_#IX__L1*/ meltfnum[0];
	      /*_.MELT_DEBUG_FUN__V12*/ meltfptr[11] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[2])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V11*/ meltfptr[10] =
	      /*_.MELT_DEBUG_FUN__V12*/ meltfptr[11];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-outobj.melt:5049:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L3*/ meltfnum[2] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V12*/ meltfptr[11] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V11*/ meltfptr[10] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-outobj.melt:5049:/ quasiblock");


      /*_.PROGN___V13*/ meltfptr[11] = /*_.IF___V11*/ meltfptr[10];;
      /*^compute */
      /*_.IFCPP___V10*/ meltfptr[9] = /*_.PROGN___V13*/ meltfptr[11];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:5049:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L2*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IF___V11*/ meltfptr[10] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V13*/ meltfptr[11] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V10*/ meltfptr[9] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:5054:/ locexp");
      melt_error_str ((melt_ptr_t) ( /*_.DLOC__V9*/ meltfptr[8]),
		      ("unimplemented SYNTAX_TEST_GENERATOR method for "),
		      (melt_ptr_t) ( /*_.DISNAME__V8*/ meltfptr[7]));
    }
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:5055:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L4*/ meltfnum[2] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.GENDEV__V3*/ meltfptr[2]),
			     (melt_ptr_t) (( /*!CLASS_SOURCE_GENERATOR_DEVICE */ meltfrout->tabval[3])));;
      MELT_LOCATION ("warmelt-outobj.melt:5055:/ cond");
      /*cond */ if ( /*_#IS_A__L4*/ meltfnum[2])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V15*/ meltfptr[11] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-outobj.melt:5055:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check gendev"),
				  ("warmelt-outobj.melt")
				  ? ("warmelt-outobj.melt") : __FILE__,
				  (5055) ? (5055) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V15*/ meltfptr[11] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V14*/ meltfptr[10] = /*_.IFELSE___V15*/ meltfptr[11];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:5055:/ clear");
	     /*clear *//*_#IS_A__L4*/ meltfnum[2] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V15*/ meltfptr[11] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V14*/ meltfptr[10] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:5056:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_STRBUF__L5*/ meltfnum[1] =
	(melt_magic_discr ((melt_ptr_t) ( /*_.SBUF__V4*/ meltfptr[3])) ==
	 MELTOBMAG_STRBUF);;
      MELT_LOCATION ("warmelt-outobj.melt:5056:/ cond");
      /*cond */ if ( /*_#IS_STRBUF__L5*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V17*/ meltfptr[16] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-outobj.melt:5056:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check sbuf"),
				  ("warmelt-outobj.melt")
				  ? ("warmelt-outobj.melt") : __FILE__,
				  (5056) ? (5056) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V17*/ meltfptr[16] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V16*/ meltfptr[11] = /*_.IFELSE___V17*/ meltfptr[16];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:5056:/ clear");
	     /*clear *//*_#IS_STRBUF__L5*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V17*/ meltfptr[16] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V16*/ meltfptr[11] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:5057:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L6*/ meltfnum[2] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.MODCTX__V5*/ meltfptr[4]),
			     (melt_ptr_t) (( /*!CLASS_MODULE_CONTEXT */
					    meltfrout->tabval[4])));;
      MELT_LOCATION ("warmelt-outobj.melt:5057:/ cond");
      /*cond */ if ( /*_#IS_A__L6*/ meltfnum[2])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V19*/ meltfptr[18] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-outobj.melt:5057:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check modctx"),
				  ("warmelt-outobj.melt")
				  ? ("warmelt-outobj.melt") : __FILE__,
				  (5057) ? (5057) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V19*/ meltfptr[18] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V18*/ meltfptr[16] = /*_.IFELSE___V19*/ meltfptr[18];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:5057:/ clear");
	     /*clear *//*_#IS_A__L6*/ meltfnum[2] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V19*/ meltfptr[18] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V18*/ meltfptr[16] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:5058:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
      /*^cond */
      /*cond */ if (( /*nil */ NULL))	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V21*/ meltfptr[20] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-outobj.melt:5058:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("@$@unimplemented syntax_test_generator"),
				  ("warmelt-outobj.melt")
				  ? ("warmelt-outobj.melt") : __FILE__,
				  (5058) ? (5058) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V21*/ meltfptr[20] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V20*/ meltfptr[18] = /*_.IFELSE___V21*/ meltfptr[20];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:5058:/ clear");
	     /*clear *//*_.IFELSE___V21*/ meltfptr[20] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V20*/ meltfptr[18] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    /*^compute */
    /*_.LET___V6*/ meltfptr[5] = /*_.IFCPP___V20*/ meltfptr[18];;

    MELT_LOCATION ("warmelt-outobj.melt:5045:/ clear");
	   /*clear *//*_.DIS__V7*/ meltfptr[6] = 0;
    /*^clear */
	   /*clear *//*_.DISNAME__V8*/ meltfptr[7] = 0;
    /*^clear */
	   /*clear *//*_.DLOC__V9*/ meltfptr[8] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V10*/ meltfptr[9] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V14*/ meltfptr[10] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V16*/ meltfptr[11] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V18*/ meltfptr[16] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V20*/ meltfptr[18] = 0;
    MELT_LOCATION ("warmelt-outobj.melt:5044:/ quasiblock");


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.LET___V6*/ meltfptr[5];;

    {
      MELT_LOCATION ("warmelt-outobj.melt:5044:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.LET___V6*/ meltfptr[5] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("SYNTESTGEN_ANY", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_124_warmelt_outobj_SYNTESTGEN_ANY_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_124_warmelt_outobj_SYNTESTGEN_ANY */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_125_warmelt_outobj_SUBSTITUTE_FORMALS_FOR_SYNTEST (meltclosure_ptr_t
							    meltclosp_,
							    melt_ptr_t
							    meltfirstargp_,
							    const
							    melt_argdescr_cell_t
							    meltxargdescr_[],
							    union meltparam_un
							    * meltxargtab_,
							    const
							    melt_argdescr_cell_t
							    meltxresdescr_[],
							    union meltparam_un
							    * meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_125_warmelt_outobj_SUBSTITUTE_FORMALS_FOR_SYNTEST_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_125_warmelt_outobj_SUBSTITUTE_FORMALS_FOR_SYNTEST_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 35
    melt_ptr_t mcfr_varptr[35];
#define MELTFRAM_NBVARNUM 11
    long mcfr_varnum[11];
/*others*/
    const char *loc_CSTRING__o0;
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_125_warmelt_outobj_SUBSTITUTE_FORMALS_FOR_SYNTEST is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct
	 frame_meltrout_125_warmelt_outobj_SUBSTITUTE_FORMALS_FOR_SYNTEST_st
	 *) meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 35; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_125_warmelt_outobj_SUBSTITUTE_FORMALS_FOR_SYNTEST nbval 35*/
  meltfram__.mcfr_nbvar = 35 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("SUBSTITUTE_FORMALS_FOR_SYNTEST", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-outobj.melt:5064:/ getarg");
 /*_.SBUF__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.REPLMAP__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.REPLMAP__V3*/ meltfptr[2])) !=
	      NULL);


  /*getarg#2 */
  /*^getarg */
  if (meltxargdescr_[1] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.FORMALS__V4*/ meltfptr[3] =
    (meltxargtab_[1].meltbp_aptr) ? (*(meltxargtab_[1].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.FORMALS__V4*/ meltfptr[3])) !=
	      NULL);


  /*getarg#3 */
  /*^getarg */
  if (meltxargdescr_[2] != MELTBPAR_CSTRING)
    goto lab_endgetargs;
 /*_?*/ meltfram__.loc_CSTRING__o0 = meltxargtab_[2].meltbp_cstring;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:5065:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L1*/ meltfnum[0] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-outobj.melt:5065:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L1*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-outobj.melt:5065:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[11];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-outobj.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 5065;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring =
		"substitute_formals_for_syntest sbuf=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.SBUF__V2*/ meltfptr[1];
	      /*^apply.arg */
	      argtab[5].meltbp_cstring = "\n replmap=";
	      /*^apply.arg */
	      argtab[6].meltbp_aptr =
		(melt_ptr_t *) & /*_.REPLMAP__V3*/ meltfptr[2];
	      /*^apply.arg */
	      argtab[7].meltbp_cstring = "\n* formals=";
	      /*^apply.arg */
	      argtab[8].meltbp_aptr =
		(melt_ptr_t *) & /*_.FORMALS__V4*/ meltfptr[3];
	      /*^apply.arg */
	      argtab[9].meltbp_cstring = "\n prefix=";
	      /*^apply.arg */
	      argtab[10].meltbp_cstring = /*_?*/ meltfram__.loc_CSTRING__o0;
	      /*_.MELT_DEBUG_FUN__V7*/ meltfptr[6] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR MELTBPARSTR_CSTRING
			     MELTBPARSTR_CSTRING ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V6*/ meltfptr[5] = /*_.MELT_DEBUG_FUN__V7*/ meltfptr[6];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-outobj.melt:5065:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V7*/ meltfptr[6] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V6*/ meltfptr[5] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-outobj.melt:5065:/ quasiblock");


      /*_.PROGN___V8*/ meltfptr[6] = /*_.IF___V6*/ meltfptr[5];;
      /*^compute */
      /*_.IFCPP___V5*/ meltfptr[4] = /*_.PROGN___V8*/ meltfptr[6];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:5065:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L1*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IF___V6*/ meltfptr[5] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V8*/ meltfptr[6] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V5*/ meltfptr[4] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:5066:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_STRBUF__L3*/ meltfnum[1] =
	(melt_magic_discr ((melt_ptr_t) ( /*_.SBUF__V2*/ meltfptr[1])) ==
	 MELTOBMAG_STRBUF);;
      MELT_LOCATION ("warmelt-outobj.melt:5066:/ cond");
      /*cond */ if ( /*_#IS_STRBUF__L3*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V10*/ meltfptr[6] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-outobj.melt:5066:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check sbuf"),
				  ("warmelt-outobj.melt")
				  ? ("warmelt-outobj.melt") : __FILE__,
				  (5066) ? (5066) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V10*/ meltfptr[6] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V9*/ meltfptr[5] = /*_.IFELSE___V10*/ meltfptr[6];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:5066:/ clear");
	     /*clear *//*_#IS_STRBUF__L3*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V10*/ meltfptr[6] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V9*/ meltfptr[5] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:5067:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_MAPOBJECT__L4*/ meltfnum[0] =
	/*is_mapobject: */
	(melt_magic_discr ((melt_ptr_t) ( /*_.REPLMAP__V3*/ meltfptr[2])) ==
	 MELTOBMAG_MAPOBJECTS);;
      MELT_LOCATION ("warmelt-outobj.melt:5067:/ cond");
      /*cond */ if ( /*_#IS_MAPOBJECT__L4*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V12*/ meltfptr[11] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-outobj.melt:5067:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check replmap"),
				  ("warmelt-outobj.melt")
				  ? ("warmelt-outobj.melt") : __FILE__,
				  (5067) ? (5067) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V12*/ meltfptr[11] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V11*/ meltfptr[6] = /*_.IFELSE___V12*/ meltfptr[11];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:5067:/ clear");
	     /*clear *//*_#IS_MAPOBJECT__L4*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V12*/ meltfptr[11] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V11*/ meltfptr[6] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:5068:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_MULTIPLE_OR_NULL__L5*/ meltfnum[1] =
	(( /*_.FORMALS__V4*/ meltfptr[3]) == NULL
	 ||
	 (melt_unsafe_magic_discr
	  ((melt_ptr_t) ( /*_.FORMALS__V4*/ meltfptr[3])) ==
	  MELTOBMAG_MULTIPLE));;
      MELT_LOCATION ("warmelt-outobj.melt:5068:/ cond");
      /*cond */ if ( /*_#IS_MULTIPLE_OR_NULL__L5*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V14*/ meltfptr[13] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-outobj.melt:5068:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check formals"),
				  ("warmelt-outobj.melt")
				  ? ("warmelt-outobj.melt") : __FILE__,
				  (5068) ? (5068) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V14*/ meltfptr[13] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V13*/ meltfptr[11] = /*_.IFELSE___V14*/ meltfptr[13];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:5068:/ clear");
	     /*clear *//*_#IS_MULTIPLE_OR_NULL__L5*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V14*/ meltfptr[13] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V13*/ meltfptr[11] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    /*citerblock FOREACH_IN_MULTIPLE */
    {
      /* start foreach_in_multiple meltcit1__EACHTUP */
      long meltcit1__EACHTUP_ln =
	melt_multiple_length ((melt_ptr_t) /*_.FORMALS__V4*/ meltfptr[3]);
      for ( /*_#IX__L6*/ meltfnum[0] = 0;
	   ( /*_#IX__L6*/ meltfnum[0] >= 0)
	   && ( /*_#IX__L6*/ meltfnum[0] < meltcit1__EACHTUP_ln);
	/*_#IX__L6*/ meltfnum[0]++)
	{
	  /*_.CURFORMAL__V15*/ meltfptr[13] =
	    melt_multiple_nth ((melt_ptr_t) ( /*_.FORMALS__V4*/ meltfptr[3]),
			       /*_#IX__L6*/ meltfnum[0]);




#if MELT_HAVE_DEBUG
	  MELT_LOCATION ("warmelt-outobj.melt:5072:/ cppif.then");
	  /*^block */
	  /*anyblock */
	  {


	    {
	      /*^locexp */
	      /*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	      melt_dbgcounter++;
#endif
	      ;
	    }
	    ;
	    /*^checksignal */
	    MELT_CHECK_SIGNAL ();
	    ;
    /*_#MELT_NEED_DBG__L7*/ meltfnum[1] =
	      /*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	      ( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	      0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	      ;;
	    MELT_LOCATION ("warmelt-outobj.melt:5072:/ cond");
	    /*cond */ if ( /*_#MELT_NEED_DBG__L7*/ meltfnum[1])	/*then */
	      {
		/*^cond.then */
		/*^block */
		/*anyblock */
		{

      /*_#THE_MELTCALLCOUNT__L8*/ meltfnum[7] =
#ifdef meltcallcount
		    meltcallcount	/* the_meltcallcount */
#else
		    0L
#endif /* meltcallcount the_meltcallcount */
		    ;;
		  MELT_LOCATION ("warmelt-outobj.melt:5072:/ checksignal");
		  MELT_CHECK_SIGNAL ();
		  ;
		  /*^apply */
		  /*apply */
		  {
		    union meltparam_un argtab[7];
		    memset (&argtab, 0, sizeof (argtab));
		    /*^apply.arg */
		    argtab[0].meltbp_long =
		      /*_#THE_MELTCALLCOUNT__L8*/ meltfnum[7];
		    /*^apply.arg */
		    argtab[1].meltbp_cstring = "warmelt-outobj.melt";
		    /*^apply.arg */
		    argtab[2].meltbp_long = 5072;
		    /*^apply.arg */
		    argtab[3].meltbp_cstring =
		      "substitute_formals_for_syntest curformal=";
		    /*^apply.arg */
		    argtab[4].meltbp_aptr =
		      (melt_ptr_t *) & /*_.CURFORMAL__V15*/ meltfptr[13];
		    /*^apply.arg */
		    argtab[5].meltbp_cstring = " ix=";
		    /*^apply.arg */
		    argtab[6].meltbp_long = /*_#IX__L6*/ meltfnum[0];
		    /*_.MELT_DEBUG_FUN__V18*/ meltfptr[17] =
		      melt_apply ((meltclosure_ptr_t)
				  (( /*!MELT_DEBUG_FUN */ meltfrout->
				    tabval[0])),
				  (melt_ptr_t) (( /*nil */ NULL)),
				  (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_PTR MELTBPARSTR_CSTRING
				   MELTBPARSTR_LONG ""), argtab, "",
				  (union meltparam_un *) 0);
		  }
		  ;
		  /*_.IF___V17*/ meltfptr[16] =
		    /*_.MELT_DEBUG_FUN__V18*/ meltfptr[17];;
		  /*epilog */

		  MELT_LOCATION ("warmelt-outobj.melt:5072:/ clear");
		/*clear *//*_#THE_MELTCALLCOUNT__L8*/ meltfnum[7] = 0;
		  /*^clear */
		/*clear *//*_.MELT_DEBUG_FUN__V18*/ meltfptr[17] = 0;
		}
		;
	      }
	    else
	      {			/*^cond.else */

     /*_.IF___V17*/ meltfptr[16] = NULL;;
	      }
	    ;
	    MELT_LOCATION ("warmelt-outobj.melt:5072:/ quasiblock");


	    /*_.PROGN___V19*/ meltfptr[17] = /*_.IF___V17*/ meltfptr[16];;
	    /*^compute */
	    /*_.IFCPP___V16*/ meltfptr[15] = /*_.PROGN___V19*/ meltfptr[17];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-outobj.melt:5072:/ clear");
	      /*clear *//*_#MELT_NEED_DBG__L7*/ meltfnum[1] = 0;
	    /*^clear */
	      /*clear *//*_.IF___V17*/ meltfptr[16] = 0;
	    /*^clear */
	      /*clear *//*_.PROGN___V19*/ meltfptr[17] = 0;
	  }

#else /*MELT_HAVE_DEBUG */
	  /*^cppif.else */
	  /*_.IFCPP___V16*/ meltfptr[15] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
	  ;

#if MELT_HAVE_DEBUG
	  MELT_LOCATION ("warmelt-outobj.melt:5073:/ cppif.then");
	  /*^block */
	  /*anyblock */
	  {

	    /*^checksignal */
	    MELT_CHECK_SIGNAL ();
	    ;
    /*_#IS_A__L9*/ meltfnum[7] =
	      melt_is_instance_of ((melt_ptr_t)
				   ( /*_.CURFORMAL__V15*/ meltfptr[13]),
				   (melt_ptr_t) (( /*!CLASS_FORMAL_BINDING */
						  meltfrout->tabval[1])));;
	    MELT_LOCATION ("warmelt-outobj.melt:5073:/ cond");
	    /*cond */ if ( /*_#IS_A__L9*/ meltfnum[7])	/*then */
	      {
		/*^cond.then */
		/*_.IFELSE___V21*/ meltfptr[17] = ( /*nil */ NULL);;
	      }
	    else
	      {
		MELT_LOCATION ("warmelt-outobj.melt:5073:/ cond.else");

		/*^block */
		/*anyblock */
		{




		  {
		    /*^locexp */
		    melt_assert_failed (("check curformal"),
					("warmelt-outobj.melt")
					? ("warmelt-outobj.melt") : __FILE__,
					(5073) ? (5073) : __LINE__,
					__FUNCTION__);
		    ;
		  }
		  ;
		/*clear *//*_.IFELSE___V21*/ meltfptr[17] = 0;
		  /*epilog */
		}
		;
	      }
	    ;
	    /*^compute */
	    /*_.IFCPP___V20*/ meltfptr[16] = /*_.IFELSE___V21*/ meltfptr[17];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-outobj.melt:5073:/ clear");
	      /*clear *//*_#IS_A__L9*/ meltfnum[7] = 0;
	    /*^clear */
	      /*clear *//*_.IFELSE___V21*/ meltfptr[17] = 0;
	  }

#else /*MELT_HAVE_DEBUG */
	  /*^cppif.else */
	  /*_.IFCPP___V20*/ meltfptr[16] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
	  ;
	  MELT_LOCATION ("warmelt-outobj.melt:5074:/ quasiblock");


	  /*^cond */
	  /*cond */ if (
			 /*ifisa */
			 melt_is_instance_of ((melt_ptr_t)
					      ( /*_.CURFORMAL__V15*/
					       meltfptr[13]),
					      (melt_ptr_t) (( /*!CLASS_ANY_BINDING */ meltfrout->tabval[2])))
	    )			/*then */
	    {
	      /*^cond.then */
	      /*^getslot */
	      {
		melt_ptr_t slot = NULL, obj = NULL;
		obj =
		  (melt_ptr_t) ( /*_.CURFORMAL__V15*/ meltfptr[13]) /*=obj*/ ;
		melt_object_get_field (slot, obj, 0, "BINDER");
    /*_.FSYMB__V22*/ meltfptr[17] = slot;
	      };
	      ;
	    }
	  else
	    {			/*^cond.else */

   /*_.FSYMB__V22*/ meltfptr[17] = NULL;;
	    }
	  ;
	  MELT_LOCATION ("warmelt-outobj.melt:5075:/ cond");
	  /*cond */ if (
			 /*ifisa */
			 melt_is_instance_of ((melt_ptr_t)
					      ( /*_.CURFORMAL__V15*/
					       meltfptr[13]),
					      (melt_ptr_t) (( /*!CLASS_FORMAL_BINDING */ meltfrout->tabval[1])))
	    )			/*then */
	    {
	      /*^cond.then */
	      /*^getslot */
	      {
		melt_ptr_t slot = NULL, obj = NULL;
		obj =
		  (melt_ptr_t) ( /*_.CURFORMAL__V15*/ meltfptr[13]) /*=obj*/ ;
		melt_object_get_field (slot, obj, 1, "FBIND_TYPE");
    /*_.FTYPE__V23*/ meltfptr[22] = slot;
	      };
	      ;
	    }
	  else
	    {			/*^cond.else */

   /*_.FTYPE__V23*/ meltfptr[22] = NULL;;
	    }
	  ;
	  MELT_LOCATION ("warmelt-outobj.melt:5076:/ quasiblock");


  /*_.NAMBUF__V25*/ meltfptr[24] =
	    (melt_ptr_t)
	    meltgc_new_strbuf ((meltobject_ptr_t)
			       (( /*!DISCR_STRBUF */ meltfrout->tabval[3])),
			       (const char *) 0);;
	  MELT_LOCATION ("warmelt-outobj.melt:5078:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^apply */
	  /*apply */
	  {
	    union meltparam_un argtab[3];
	    memset (&argtab, 0, sizeof (argtab));
	    /*^apply.arg */
	    argtab[0].meltbp_cstring = /*_?*/ meltfram__.loc_CSTRING__o0;
	    /*^apply.arg */
	    argtab[1].meltbp_long = /*_#IX__L6*/ meltfnum[0];
	    /*^apply.arg */
	    argtab[2].meltbp_cstring = "_";
	    /*_.ADD2OUT__V26*/ meltfptr[25] =
	      melt_apply ((meltclosure_ptr_t)
			  (( /*!ADD2OUT */ meltfrout->tabval[4])),
			  (melt_ptr_t) ( /*_.NAMBUF__V25*/ meltfptr[24]),
			  (MELTBPARSTR_CSTRING MELTBPARSTR_LONG
			   MELTBPARSTR_CSTRING ""), argtab, "",
			  (union meltparam_un *) 0);
	  }
	  ;
	  MELT_LOCATION ("warmelt-outobj.melt:5079:/ cond");
	  /*cond */ if (
			 /*ifisa */
			 melt_is_instance_of ((melt_ptr_t)
					      ( /*_.FSYMB__V22*/
					       meltfptr[17]),
					      (melt_ptr_t) (( /*!CLASS_NAMED */ meltfrout->tabval[5])))
	    )			/*then */
	    {
	      /*^cond.then */
	      /*^getslot */
	      {
		melt_ptr_t slot = NULL, obj = NULL;
		obj = (melt_ptr_t) ( /*_.FSYMB__V22*/ meltfptr[17]) /*=obj*/ ;
		melt_object_get_field (slot, obj, 1, "NAMED_NAME");
    /*_.NAMED_NAME__V27*/ meltfptr[26] = slot;
	      };
	      ;
	    }
	  else
	    {			/*^cond.else */

   /*_.NAMED_NAME__V27*/ meltfptr[26] = NULL;;
	    }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:5079:/ locexp");
	    meltgc_add_strbuf_cident ((melt_ptr_t)
				      ( /*_.NAMBUF__V25*/ meltfptr[24]),
				      melt_string_str ((melt_ptr_t)
						       ( /*_.NAMED_NAME__V27*/
							meltfptr[26])));
	  }
	  ;
  /*_.STRBUF2STRING__V28*/ meltfptr[27] =
	    (meltgc_new_stringdup
	     ((meltobject_ptr_t)
	      (( /*!DISCR_VERBATIM_STRING */ meltfrout->tabval[6])),
	      melt_strbuf_str ((melt_ptr_t)
			       ( /*_.NAMBUF__V25*/ meltfptr[24]))));;
	  /*^compute */
	  /*_.LET___V24*/ meltfptr[23] =
	    /*_.STRBUF2STRING__V28*/ meltfptr[27];;

	  MELT_LOCATION ("warmelt-outobj.melt:5076:/ clear");
	    /*clear *//*_.NAMBUF__V25*/ meltfptr[24] = 0;
	  /*^clear */
	    /*clear *//*_.ADD2OUT__V26*/ meltfptr[25] = 0;
	  /*^clear */
	    /*clear *//*_.NAMED_NAME__V27*/ meltfptr[26] = 0;
	  /*^clear */
	    /*clear *//*_.STRBUF2STRING__V28*/ meltfptr[27] = 0;
	  /*_.GENSY__V29*/ meltfptr[24] = /*_.LET___V24*/ meltfptr[23];;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:5082:/ locexp");
	    meltgc_put_mapobjects ((meltmapobjects_ptr_t)
				   ( /*_.REPLMAP__V3*/ meltfptr[2]),
				   (meltobject_ptr_t) ( /*_.FSYMB__V22*/
						       meltfptr[17]),
				   (melt_ptr_t) ( /*_.GENSY__V29*/
						 meltfptr[24]));
	  }
	  ;

#if MELT_HAVE_DEBUG
	  MELT_LOCATION ("warmelt-outobj.melt:5083:/ cppif.then");
	  /*^block */
	  /*anyblock */
	  {


	    {
	      /*^locexp */
	      /*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	      melt_dbgcounter++;
#endif
	      ;
	    }
	    ;
	    /*^checksignal */
	    MELT_CHECK_SIGNAL ();
	    ;
    /*_#MELT_NEED_DBG__L10*/ meltfnum[1] =
	      /*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	      ( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	      0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	      ;;
	    MELT_LOCATION ("warmelt-outobj.melt:5083:/ cond");
	    /*cond */ if ( /*_#MELT_NEED_DBG__L10*/ meltfnum[1])	/*then */
	      {
		/*^cond.then */
		/*^block */
		/*anyblock */
		{

      /*_#THE_MELTCALLCOUNT__L11*/ meltfnum[7] =
#ifdef meltcallcount
		    meltcallcount	/* the_meltcallcount */
#else
		    0L
#endif /* meltcallcount the_meltcallcount */
		    ;;
		  MELT_LOCATION ("warmelt-outobj.melt:5083:/ checksignal");
		  MELT_CHECK_SIGNAL ();
		  ;
		  /*^apply */
		  /*apply */
		  {
		    union meltparam_un argtab[9];
		    memset (&argtab, 0, sizeof (argtab));
		    /*^apply.arg */
		    argtab[0].meltbp_long =
		      /*_#THE_MELTCALLCOUNT__L11*/ meltfnum[7];
		    /*^apply.arg */
		    argtab[1].meltbp_cstring = "warmelt-outobj.melt";
		    /*^apply.arg */
		    argtab[2].meltbp_long = 5083;
		    /*^apply.arg */
		    argtab[3].meltbp_cstring =
		      "substitute_formals_for_syntest updated replmap=";
		    /*^apply.arg */
		    argtab[4].meltbp_aptr =
		      (melt_ptr_t *) & /*_.REPLMAP__V3*/ meltfptr[2];
		    /*^apply.arg */
		    argtab[5].meltbp_cstring = "\n with fsymb=";
		    /*^apply.arg */
		    argtab[6].meltbp_aptr =
		      (melt_ptr_t *) & /*_.FSYMB__V22*/ meltfptr[17];
		    /*^apply.arg */
		    argtab[7].meltbp_cstring = "\n with gensy=";
		    /*^apply.arg */
		    argtab[8].meltbp_aptr =
		      (melt_ptr_t *) & /*_.GENSY__V29*/ meltfptr[24];
		    /*_.MELT_DEBUG_FUN__V32*/ meltfptr[27] =
		      melt_apply ((meltclosure_ptr_t)
				  (( /*!MELT_DEBUG_FUN */ meltfrout->
				    tabval[0])),
				  (melt_ptr_t) (( /*nil */ NULL)),
				  (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_PTR MELTBPARSTR_CSTRING
				   MELTBPARSTR_PTR MELTBPARSTR_CSTRING
				   MELTBPARSTR_PTR ""), argtab, "",
				  (union meltparam_un *) 0);
		  }
		  ;
		  /*_.IF___V31*/ meltfptr[26] =
		    /*_.MELT_DEBUG_FUN__V32*/ meltfptr[27];;
		  /*epilog */

		  MELT_LOCATION ("warmelt-outobj.melt:5083:/ clear");
		/*clear *//*_#THE_MELTCALLCOUNT__L11*/ meltfnum[7] = 0;
		  /*^clear */
		/*clear *//*_.MELT_DEBUG_FUN__V32*/ meltfptr[27] = 0;
		}
		;
	      }
	    else
	      {			/*^cond.else */

     /*_.IF___V31*/ meltfptr[26] = NULL;;
	      }
	    ;
	    MELT_LOCATION ("warmelt-outobj.melt:5083:/ quasiblock");


	    /*_.PROGN___V33*/ meltfptr[27] = /*_.IF___V31*/ meltfptr[26];;
	    /*^compute */
	    /*_.IFCPP___V30*/ meltfptr[25] = /*_.PROGN___V33*/ meltfptr[27];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-outobj.melt:5083:/ clear");
	      /*clear *//*_#MELT_NEED_DBG__L10*/ meltfnum[1] = 0;
	    /*^clear */
	      /*clear *//*_.IF___V31*/ meltfptr[26] = 0;
	    /*^clear */
	      /*clear *//*_.PROGN___V33*/ meltfptr[27] = 0;
	  }

#else /*MELT_HAVE_DEBUG */
	  /*^cppif.else */
	  /*_.IFCPP___V30*/ meltfptr[25] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
	  ;
	  MELT_LOCATION ("warmelt-outobj.melt:5084:/ cond");
	  /*cond */ if (
			 /*ifisa */
			 melt_is_instance_of ((melt_ptr_t)
					      ( /*_.FTYPE__V23*/
					       meltfptr[22]),
					      (melt_ptr_t) (( /*!CLASS_CTYPE */ meltfrout->tabval[7])))
	    )			/*then */
	    {
	      /*^cond.then */
	      /*^getslot */
	      {
		melt_ptr_t slot = NULL, obj = NULL;
		obj = (melt_ptr_t) ( /*_.FTYPE__V23*/ meltfptr[22]) /*=obj*/ ;
		melt_object_get_field (slot, obj, 3, "CTYPE_CNAME");
    /*_.CTYPE_CNAME__V34*/ meltfptr[26] = slot;
	      };
	      ;
	    }
	  else
	    {			/*^cond.else */

   /*_.CTYPE_CNAME__V34*/ meltfptr[26] = NULL;;
	    }
	  ;
	  MELT_LOCATION ("warmelt-outobj.melt:5084:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^apply */
	  /*apply */
	  {
	    union meltparam_un argtab[4];
	    memset (&argtab, 0, sizeof (argtab));
	    /*^apply.arg */
	    argtab[0].meltbp_aptr =
	      (melt_ptr_t *) & /*_.CTYPE_CNAME__V34*/ meltfptr[26];
	    /*^apply.arg */
	    argtab[1].meltbp_cstring = " ";
	    /*^apply.arg */
	    argtab[2].meltbp_aptr =
	      (melt_ptr_t *) & /*_.GENSY__V29*/ meltfptr[24];
	    /*^apply.arg */
	    argtab[3].meltbp_cstring = " =0;";
	    /*_.ADD2OUT__V35*/ meltfptr[27] =
	      melt_apply ((meltclosure_ptr_t)
			  (( /*!ADD2OUT */ meltfrout->tabval[4])),
			  (melt_ptr_t) ( /*_.SBUF__V2*/ meltfptr[1]),
			  (MELTBPARSTR_PTR MELTBPARSTR_CSTRING MELTBPARSTR_PTR
			   MELTBPARSTR_CSTRING ""), argtab, "",
			  (union meltparam_un *) 0);
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:5085:/ locexp");
	    meltgc_strbuf_add_indent ((melt_ptr_t)
				      ( /*_.SBUF__V2*/ meltfptr[1]), (1), 0);
	  }
	  ;

	  MELT_LOCATION ("warmelt-outobj.melt:5074:/ clear");
	    /*clear *//*_.FSYMB__V22*/ meltfptr[17] = 0;
	  /*^clear */
	    /*clear *//*_.FTYPE__V23*/ meltfptr[22] = 0;
	  /*^clear */
	    /*clear *//*_.LET___V24*/ meltfptr[23] = 0;
	  /*^clear */
	    /*clear *//*_.GENSY__V29*/ meltfptr[24] = 0;
	  /*^clear */
	    /*clear *//*_.IFCPP___V30*/ meltfptr[25] = 0;
	  /*^clear */
	    /*clear *//*_.CTYPE_CNAME__V34*/ meltfptr[26] = 0;
	  /*^clear */
	    /*clear *//*_.ADD2OUT__V35*/ meltfptr[27] = 0;
	  if ( /*_#IX__L6*/ meltfnum[0] < 0)
	    break;
	}			/* end  foreach_in_multiple meltcit1__EACHTUP */

      /*citerepilog */

      MELT_LOCATION ("warmelt-outobj.melt:5069:/ clear");
	    /*clear *//*_.CURFORMAL__V15*/ meltfptr[13] = 0;
      /*^clear */
	    /*clear *//*_#IX__L6*/ meltfnum[0] = 0;
      /*^clear */
	    /*clear *//*_.IFCPP___V16*/ meltfptr[15] = 0;
      /*^clear */
	    /*clear *//*_.IFCPP___V20*/ meltfptr[16] = 0;
    }				/*endciterblock FOREACH_IN_MULTIPLE */
    ;
    MELT_LOCATION ("warmelt-outobj.melt:5064:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V5*/ meltfptr[4] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V9*/ meltfptr[5] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V11*/ meltfptr[6] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V13*/ meltfptr[11] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("SUBSTITUTE_FORMALS_FOR_SYNTEST", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_125_warmelt_outobj_SUBSTITUTE_FORMALS_FOR_SYNTEST_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*noretval */ NULL);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_125_warmelt_outobj_SUBSTITUTE_FORMALS_FOR_SYNTEST */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_126_warmelt_outobj_EXPAND_TUPLE_FOR_SYNTEST (meltclosure_ptr_t
						      meltclosp_,
						      melt_ptr_t
						      meltfirstargp_,
						      const
						      melt_argdescr_cell_t
						      meltxargdescr_[],
						      union meltparam_un *
						      meltxargtab_,
						      const
						      melt_argdescr_cell_t
						      meltxresdescr_[],
						      union meltparam_un *
						      meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_126_warmelt_outobj_EXPAND_TUPLE_FOR_SYNTEST_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_126_warmelt_outobj_EXPAND_TUPLE_FOR_SYNTEST_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 33
    melt_ptr_t mcfr_varptr[33];
#define MELTFRAM_NBVARNUM 12
    long mcfr_varnum[12];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_126_warmelt_outobj_EXPAND_TUPLE_FOR_SYNTEST is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_126_warmelt_outobj_EXPAND_TUPLE_FOR_SYNTEST_st
	 *) meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 33; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_126_warmelt_outobj_EXPAND_TUPLE_FOR_SYNTEST nbval 33*/
  meltfram__.mcfr_nbvar = 33 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("EXPAND_TUPLE_FOR_SYNTEST", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-outobj.melt:5090:/ getarg");
 /*_.SBUF__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.REPLMAP__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.REPLMAP__V3*/ meltfptr[2])) !=
	      NULL);


  /*getarg#2 */
  /*^getarg */
  if (meltxargdescr_[1] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.TUP__V4*/ meltfptr[3] =
    (meltxargtab_[1].meltbp_aptr) ? (*(meltxargtab_[1].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.TUP__V4*/ meltfptr[3])) != NULL);


  /*getarg#3 */
  /*^getarg */
  if (meltxargdescr_[2] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.LOC__V5*/ meltfptr[4] =
    (meltxargtab_[2].meltbp_aptr) ? (*(meltxargtab_[2].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.LOC__V5*/ meltfptr[4])) != NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:5091:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L1*/ meltfnum[0] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-outobj.melt:5091:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L1*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-outobj.melt:5091:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[9];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-outobj.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 5091;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "expand_tuple_for_syntest sbuf=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.SBUF__V2*/ meltfptr[1];
	      /*^apply.arg */
	      argtab[5].meltbp_cstring = "\n replmap=";
	      /*^apply.arg */
	      argtab[6].meltbp_aptr =
		(melt_ptr_t *) & /*_.REPLMAP__V3*/ meltfptr[2];
	      /*^apply.arg */
	      argtab[7].meltbp_cstring = "\n tup=";
	      /*^apply.arg */
	      argtab[8].meltbp_aptr =
		(melt_ptr_t *) & /*_.TUP__V4*/ meltfptr[3];
	      /*_.MELT_DEBUG_FUN__V8*/ meltfptr[7] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V7*/ meltfptr[6] = /*_.MELT_DEBUG_FUN__V8*/ meltfptr[7];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-outobj.melt:5091:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V8*/ meltfptr[7] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V7*/ meltfptr[6] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-outobj.melt:5091:/ quasiblock");


      /*_.PROGN___V9*/ meltfptr[7] = /*_.IF___V7*/ meltfptr[6];;
      /*^compute */
      /*_.IFCPP___V6*/ meltfptr[5] = /*_.PROGN___V9*/ meltfptr[7];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:5091:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L1*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IF___V7*/ meltfptr[6] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V9*/ meltfptr[7] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V6*/ meltfptr[5] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    /*citerblock FOREACH_IN_MULTIPLE */
    {
      /* start foreach_in_multiple meltcit1__EACHTUP */
      long meltcit1__EACHTUP_ln =
	melt_multiple_length ((melt_ptr_t) /*_.TUP__V4*/ meltfptr[3]);
      for ( /*_#PIX__L3*/ meltfnum[1] = 0;
	   ( /*_#PIX__L3*/ meltfnum[1] >= 0)
	   && ( /*_#PIX__L3*/ meltfnum[1] < meltcit1__EACHTUP_ln);
	/*_#PIX__L3*/ meltfnum[1]++)
	{
	  /*_.CUREXPAN__V10*/ meltfptr[6] =
	    melt_multiple_nth ((melt_ptr_t) ( /*_.TUP__V4*/ meltfptr[3]),
			       /*_#PIX__L3*/ meltfnum[1]);




#if MELT_HAVE_DEBUG
	  MELT_LOCATION ("warmelt-outobj.melt:5095:/ cppif.then");
	  /*^block */
	  /*anyblock */
	  {


	    {
	      /*^locexp */
	      /*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	      melt_dbgcounter++;
#endif
	      ;
	    }
	    ;
	    /*^checksignal */
	    MELT_CHECK_SIGNAL ();
	    ;
    /*_#MELT_NEED_DBG__L4*/ meltfnum[0] =
	      /*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	      ( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	      0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	      ;;
	    MELT_LOCATION ("warmelt-outobj.melt:5095:/ cond");
	    /*cond */ if ( /*_#MELT_NEED_DBG__L4*/ meltfnum[0])	/*then */
	      {
		/*^cond.then */
		/*^block */
		/*anyblock */
		{

      /*_#THE_MELTCALLCOUNT__L5*/ meltfnum[4] =
#ifdef meltcallcount
		    meltcallcount	/* the_meltcallcount */
#else
		    0L
#endif /* meltcallcount the_meltcallcount */
		    ;;
		  MELT_LOCATION ("warmelt-outobj.melt:5095:/ checksignal");
		  MELT_CHECK_SIGNAL ();
		  ;
		  /*^apply */
		  /*apply */
		  {
		    union meltparam_un argtab[5];
		    memset (&argtab, 0, sizeof (argtab));
		    /*^apply.arg */
		    argtab[0].meltbp_long =
		      /*_#THE_MELTCALLCOUNT__L5*/ meltfnum[4];
		    /*^apply.arg */
		    argtab[1].meltbp_cstring = "warmelt-outobj.melt";
		    /*^apply.arg */
		    argtab[2].meltbp_long = 5095;
		    /*^apply.arg */
		    argtab[3].meltbp_cstring =
		      "expand_tuple_for_syntest curexpan=";
		    /*^apply.arg */
		    argtab[4].meltbp_aptr =
		      (melt_ptr_t *) & /*_.CUREXPAN__V10*/ meltfptr[6];
		    /*_.MELT_DEBUG_FUN__V13*/ meltfptr[12] =
		      melt_apply ((meltclosure_ptr_t)
				  (( /*!MELT_DEBUG_FUN */ meltfrout->
				    tabval[0])),
				  (melt_ptr_t) (( /*nil */ NULL)),
				  (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_PTR ""), argtab, "",
				  (union meltparam_un *) 0);
		  }
		  ;
		  /*_.IF___V12*/ meltfptr[11] =
		    /*_.MELT_DEBUG_FUN__V13*/ meltfptr[12];;
		  /*epilog */

		  MELT_LOCATION ("warmelt-outobj.melt:5095:/ clear");
		/*clear *//*_#THE_MELTCALLCOUNT__L5*/ meltfnum[4] = 0;
		  /*^clear */
		/*clear *//*_.MELT_DEBUG_FUN__V13*/ meltfptr[12] = 0;
		}
		;
	      }
	    else
	      {			/*^cond.else */

     /*_.IF___V12*/ meltfptr[11] = NULL;;
	      }
	    ;
	    MELT_LOCATION ("warmelt-outobj.melt:5095:/ quasiblock");


	    /*_.PROGN___V14*/ meltfptr[12] = /*_.IF___V12*/ meltfptr[11];;
	    /*^compute */
	    /*_.IFCPP___V11*/ meltfptr[7] = /*_.PROGN___V14*/ meltfptr[12];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-outobj.melt:5095:/ clear");
	      /*clear *//*_#MELT_NEED_DBG__L4*/ meltfnum[0] = 0;
	    /*^clear */
	      /*clear *//*_.IF___V12*/ meltfptr[11] = 0;
	    /*^clear */
	      /*clear *//*_.PROGN___V14*/ meltfptr[12] = 0;
	  }

#else /*MELT_HAVE_DEBUG */
	  /*^cppif.else */
	  /*_.IFCPP___V11*/ meltfptr[7] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
	  ;
	  MELT_LOCATION ("warmelt-outobj.melt:5096:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
  /*_#IS_OBJECT__L6*/ meltfnum[4] =
	    (melt_magic_discr
	     ((melt_ptr_t) ( /*_.CUREXPAN__V10*/ meltfptr[6])) ==
	     MELTOBMAG_OBJECT);;
	  MELT_LOCATION ("warmelt-outobj.melt:5096:/ cond");
	  /*cond */ if ( /*_#IS_OBJECT__L6*/ meltfnum[4])	/*then */
	    {
	      /*^cond.then */
	      /*^block */
	      /*anyblock */
	      {

		MELT_LOCATION ("warmelt-outobj.melt:5097:/ quasiblock");


    /*_.CUREPL__V17*/ meltfptr[16] =
		  /*mapobject_get */
		  melt_get_mapobjects ((meltmapobjects_ptr_t)
				       ( /*_.REPLMAP__V3*/ meltfptr[2]),
				       (meltobject_ptr_t) ( /*_.CUREXPAN__V10*/ meltfptr[6]));;

#if MELT_HAVE_DEBUG
		MELT_LOCATION ("warmelt-outobj.melt:5099:/ cppif.then");
		/*^block */
		/*anyblock */
		{


		  {
		    /*^locexp */
		    /*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
		    melt_dbgcounter++;
#endif
		    ;
		  }
		  ;
		  /*^checksignal */
		  MELT_CHECK_SIGNAL ();
		  ;
      /*_#MELT_NEED_DBG__L7*/ meltfnum[0] =
		    /*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
		    ( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
		    0		/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
		    ;;
		  MELT_LOCATION ("warmelt-outobj.melt:5099:/ cond");
		  /*cond */ if ( /*_#MELT_NEED_DBG__L7*/ meltfnum[0])	/*then */
		    {
		      /*^cond.then */
		      /*^block */
		      /*anyblock */
		      {

	/*_#THE_MELTCALLCOUNT__L8*/ meltfnum[7] =
#ifdef meltcallcount
			  meltcallcount	/* the_meltcallcount */
#else
			  0L
#endif /* meltcallcount the_meltcallcount */
			  ;;
			MELT_LOCATION
			  ("warmelt-outobj.melt:5099:/ checksignal");
			MELT_CHECK_SIGNAL ();
			;
			/*^apply */
			/*apply */
			{
			  union meltparam_un argtab[5];
			  memset (&argtab, 0, sizeof (argtab));
			  /*^apply.arg */
			  argtab[0].meltbp_long =
			    /*_#THE_MELTCALLCOUNT__L8*/ meltfnum[7];
			  /*^apply.arg */
			  argtab[1].meltbp_cstring = "warmelt-outobj.melt";
			  /*^apply.arg */
			  argtab[2].meltbp_long = 5099;
			  /*^apply.arg */
			  argtab[3].meltbp_cstring =
			    "expand_tuple_for_syntest curepl=";
			  /*^apply.arg */
			  argtab[4].meltbp_aptr =
			    (melt_ptr_t *) & /*_.CUREPL__V17*/ meltfptr[16];
			  /*_.MELT_DEBUG_FUN__V20*/ meltfptr[19] =
			    melt_apply ((meltclosure_ptr_t)
					(( /*!MELT_DEBUG_FUN */ meltfrout->
					  tabval[0])),
					(melt_ptr_t) (( /*nil */ NULL)),
					(MELTBPARSTR_LONG MELTBPARSTR_CSTRING
					 MELTBPARSTR_LONG MELTBPARSTR_CSTRING
					 MELTBPARSTR_PTR ""), argtab, "",
					(union meltparam_un *) 0);
			}
			;
			/*_.IF___V19*/ meltfptr[18] =
			  /*_.MELT_DEBUG_FUN__V20*/ meltfptr[19];;
			/*epilog */

			MELT_LOCATION ("warmelt-outobj.melt:5099:/ clear");
		  /*clear *//*_#THE_MELTCALLCOUNT__L8*/ meltfnum[7] = 0;
			/*^clear */
		  /*clear *//*_.MELT_DEBUG_FUN__V20*/ meltfptr[19] = 0;
		      }
		      ;
		    }
		  else
		    {		/*^cond.else */

       /*_.IF___V19*/ meltfptr[18] = NULL;;
		    }
		  ;
		  MELT_LOCATION ("warmelt-outobj.melt:5099:/ quasiblock");


		  /*_.PROGN___V21*/ meltfptr[19] =
		    /*_.IF___V19*/ meltfptr[18];;
		  /*^compute */
		  /*_.IFCPP___V18*/ meltfptr[17] =
		    /*_.PROGN___V21*/ meltfptr[19];;
		  /*epilog */

		  MELT_LOCATION ("warmelt-outobj.melt:5099:/ clear");
		/*clear *//*_#MELT_NEED_DBG__L7*/ meltfnum[0] = 0;
		  /*^clear */
		/*clear *//*_.IF___V19*/ meltfptr[18] = 0;
		  /*^clear */
		/*clear *//*_.PROGN___V21*/ meltfptr[19] = 0;
		}

#else /*MELT_HAVE_DEBUG */
		/*^cppif.else */
		/*_.IFCPP___V18*/ meltfptr[17] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
		;
		MELT_LOCATION ("warmelt-outobj.melt:5100:/ checksignal");
		MELT_CHECK_SIGNAL ();
		;
		/*^cond */
		/*cond */ if ( /*_.CUREPL__V17*/ meltfptr[16])	/*then */
		  {
		    /*^cond.then */
		    /*^block */
		    /*anyblock */
		    {

		      MELT_LOCATION
			("warmelt-outobj.melt:5101:/ checksignal");
		      MELT_CHECK_SIGNAL ();
		      ;
		      /*^apply */
		      /*apply */
		      {
			union meltparam_un argtab[1];
			memset (&argtab, 0, sizeof (argtab));
			/*^apply.arg */
			argtab[0].meltbp_aptr =
			  (melt_ptr_t *) & /*_.CUREPL__V17*/ meltfptr[16];
			/*_.ADD2OUT__V23*/ meltfptr[19] =
			  melt_apply ((meltclosure_ptr_t)
				      (( /*!ADD2OUT */ meltfrout->tabval[1])),
				      (melt_ptr_t) ( /*_.SBUF__V2*/
						    meltfptr[1]),
				      (MELTBPARSTR_PTR ""), argtab, "",
				      (union meltparam_un *) 0);
		      }
		      ;
		      /*_.IFELSE___V22*/ meltfptr[18] =
			/*_.ADD2OUT__V23*/ meltfptr[19];;
		      /*epilog */

		      MELT_LOCATION ("warmelt-outobj.melt:5100:/ clear");
		/*clear *//*_.ADD2OUT__V23*/ meltfptr[19] = 0;
		    }
		    ;
		  }
		else
		  {		/*^cond.else */

		    /*^block */
		    /*anyblock */
		    {

		      MELT_LOCATION ("warmelt-outobj.melt:5102:/ quasiblock");


		      /*^cond */
		      /*cond */ if (
				     /*ifisa */
				     melt_is_instance_of ((melt_ptr_t)
							  ( /*_.CUREXPAN__V10*/ meltfptr[6]),
							  (melt_ptr_t) (( /*!CLASS_NAMED */ meltfrout->tabval[2])))
			)	/*then */
			{
			  /*^cond.then */
			  /*^getslot */
			  {
			    melt_ptr_t slot = NULL, obj = NULL;
			    obj =
			      (melt_ptr_t) ( /*_.CUREXPAN__V10*/ meltfptr[6])
			      /*=obj*/ ;
			    melt_object_get_field (slot, obj, 1,
						   "NAMED_NAME");
	/*_.CUREXPNAM__V24*/ meltfptr[19] = slot;
			  };
			  ;
			}
		      else
			{	/*^cond.else */

       /*_.CUREXPNAM__V24*/ meltfptr[19] = NULL;;
			}
		      ;

#if MELT_HAVE_DEBUG
		      MELT_LOCATION ("warmelt-outobj.melt:5104:/ cppif.then");
		      /*^block */
		      /*anyblock */
		      {


			{
			  /*^locexp */
			  /*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
			  melt_dbgcounter++;
#endif
			  ;
			}
			;
			/*^checksignal */
			MELT_CHECK_SIGNAL ();
			;
	/*_#MELT_NEED_DBG__L9*/ meltfnum[7] =
			  /*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
			  ( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
			  0	/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
			  ;;
			MELT_LOCATION ("warmelt-outobj.melt:5104:/ cond");
			/*cond */ if ( /*_#MELT_NEED_DBG__L9*/ meltfnum[7])	/*then */
			  {
			    /*^cond.then */
			    /*^block */
			    /*anyblock */
			    {

	  /*_#THE_MELTCALLCOUNT__L10*/ meltfnum[0] =
#ifdef meltcallcount
				meltcallcount	/* the_meltcallcount */
#else
				0L
#endif /* meltcallcount the_meltcallcount */
				;;
			      MELT_LOCATION
				("warmelt-outobj.melt:5104:/ checksignal");
			      MELT_CHECK_SIGNAL ();
			      ;
			      /*^apply */
			      /*apply */
			      {
				union meltparam_un argtab[7];
				memset (&argtab, 0, sizeof (argtab));
				/*^apply.arg */
				argtab[0].meltbp_long =
				  /*_#THE_MELTCALLCOUNT__L10*/ meltfnum[0];
				/*^apply.arg */
				argtab[1].meltbp_cstring =
				  "warmelt-outobj.melt";
				/*^apply.arg */
				argtab[2].meltbp_long = 5104;
				/*^apply.arg */
				argtab[3].meltbp_cstring =
				  "expand_tuple_for_syntest no curepl for curexpan=";
				/*^apply.arg */
				argtab[4].meltbp_aptr =
				  (melt_ptr_t *) & /*_.CUREXPAN__V10*/
				  meltfptr[6];
				/*^apply.arg */
				argtab[5].meltbp_cstring = " replmap=";
				/*^apply.arg */
				argtab[6].meltbp_aptr =
				  (melt_ptr_t *) & /*_.REPLMAP__V3*/
				  meltfptr[2];
				/*_.MELT_DEBUG_FUN__V27*/ meltfptr[26] =
				  melt_apply ((meltclosure_ptr_t)
					      (( /*!MELT_DEBUG_FUN */
						meltfrout->tabval[0])),
					      (melt_ptr_t) (( /*nil */ NULL)),
					      (MELTBPARSTR_LONG
					       MELTBPARSTR_CSTRING
					       MELTBPARSTR_LONG
					       MELTBPARSTR_CSTRING
					       MELTBPARSTR_PTR
					       MELTBPARSTR_CSTRING
					       MELTBPARSTR_PTR ""), argtab,
					      "", (union meltparam_un *) 0);
			      }
			      ;
			      /*_.IF___V26*/ meltfptr[25] =
				/*_.MELT_DEBUG_FUN__V27*/ meltfptr[26];;
			      /*epilog */

			      MELT_LOCATION
				("warmelt-outobj.melt:5104:/ clear");
		    /*clear *//*_#THE_MELTCALLCOUNT__L10*/
				meltfnum[0] = 0;
			      /*^clear */
		    /*clear *//*_.MELT_DEBUG_FUN__V27*/ meltfptr[26]
				= 0;
			    }
			    ;
			  }
			else
			  {	/*^cond.else */

	 /*_.IF___V26*/ meltfptr[25] = NULL;;
			  }
			;
			MELT_LOCATION
			  ("warmelt-outobj.melt:5104:/ quasiblock");


			/*_.PROGN___V28*/ meltfptr[26] =
			  /*_.IF___V26*/ meltfptr[25];;
			/*^compute */
			/*_.IFCPP___V25*/ meltfptr[24] =
			  /*_.PROGN___V28*/ meltfptr[26];;
			/*epilog */

			MELT_LOCATION ("warmelt-outobj.melt:5104:/ clear");
		  /*clear *//*_#MELT_NEED_DBG__L9*/ meltfnum[7] = 0;
			/*^clear */
		  /*clear *//*_.IF___V26*/ meltfptr[25] = 0;
			/*^clear */
		  /*clear *//*_.PROGN___V28*/ meltfptr[26] = 0;
		      }

#else /*MELT_HAVE_DEBUG */
		      /*^cppif.else */
		      /*_.IFCPP___V25*/ meltfptr[24] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
		      ;

		      {
			MELT_LOCATION ("warmelt-outobj.melt:5105:/ locexp");
			melt_warning_str (0,
					  (melt_ptr_t) ( /*_.LOC__V5*/
							meltfptr[4]),
					  ("unexpected symbol in expansion [syntax check]"),
					  (melt_ptr_t) ( /*_.CUREXPNAM__V24*/
							meltfptr[19]));
		      }
		      ;
		      /*_.IFELSE___V22*/ meltfptr[18] =
			/*_.IFCPP___V25*/ meltfptr[24];;

		      MELT_LOCATION ("warmelt-outobj.melt:5102:/ clear");
		/*clear *//*_.CUREXPNAM__V24*/ meltfptr[19] = 0;
		      /*^clear */
		/*clear *//*_.IFCPP___V25*/ meltfptr[24] = 0;
		      /*epilog */
		    }
		    ;
		  }
		;
		/*_.LET___V16*/ meltfptr[12] =
		  /*_.IFELSE___V22*/ meltfptr[18];;

		MELT_LOCATION ("warmelt-outobj.melt:5097:/ clear");
	      /*clear *//*_.CUREPL__V17*/ meltfptr[16] = 0;
		/*^clear */
	      /*clear *//*_.IFCPP___V18*/ meltfptr[17] = 0;
		/*^clear */
	      /*clear *//*_.IFELSE___V22*/ meltfptr[18] = 0;
		/*_.IFELSE___V15*/ meltfptr[11] =
		  /*_.LET___V16*/ meltfptr[12];;
		/*epilog */

		MELT_LOCATION ("warmelt-outobj.melt:5096:/ clear");
	      /*clear *//*_.LET___V16*/ meltfptr[12] = 0;
	      }
	      ;
	    }
	  else
	    {			/*^cond.else */

	      /*^block */
	      /*anyblock */
	      {

		MELT_LOCATION ("warmelt-outobj.melt:5108:/ checksignal");
		MELT_CHECK_SIGNAL ();
		;
		/*^apply */
		/*apply */
		{
		  union meltparam_un argtab[1];
		  memset (&argtab, 0, sizeof (argtab));
		  /*^apply.arg */
		  argtab[0].meltbp_aptr =
		    (melt_ptr_t *) & /*_.CUREXPAN__V10*/ meltfptr[6];
		  /*_.ADD2OUT__V29*/ meltfptr[25] =
		    melt_apply ((meltclosure_ptr_t)
				(( /*!ADD2OUT */ meltfrout->tabval[1])),
				(melt_ptr_t) ( /*_.SBUF__V2*/ meltfptr[1]),
				(MELTBPARSTR_PTR ""), argtab, "",
				(union meltparam_un *) 0);
		}
		;
		/*_.IFELSE___V15*/ meltfptr[11] =
		  /*_.ADD2OUT__V29*/ meltfptr[25];;
		/*epilog */

		MELT_LOCATION ("warmelt-outobj.melt:5096:/ clear");
	      /*clear *//*_.ADD2OUT__V29*/ meltfptr[25] = 0;
	      }
	      ;
	    }
	  ;
	  if ( /*_#PIX__L3*/ meltfnum[1] < 0)
	    break;
	}			/* end  foreach_in_multiple meltcit1__EACHTUP */

      /*citerepilog */

      MELT_LOCATION ("warmelt-outobj.melt:5092:/ clear");
	    /*clear *//*_.CUREXPAN__V10*/ meltfptr[6] = 0;
      /*^clear */
	    /*clear *//*_#PIX__L3*/ meltfnum[1] = 0;
      /*^clear */
	    /*clear *//*_.IFCPP___V11*/ meltfptr[7] = 0;
      /*^clear */
	    /*clear *//*_#IS_OBJECT__L6*/ meltfnum[4] = 0;
      /*^clear */
	    /*clear *//*_.IFELSE___V15*/ meltfptr[11] = 0;
    }				/*endciterblock FOREACH_IN_MULTIPLE */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:5110:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L11*/ meltfnum[0] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-outobj.melt:5110:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L11*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L12*/ meltfnum[7] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-outobj.melt:5110:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long =
		/*_#THE_MELTCALLCOUNT__L12*/ meltfnum[7];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-outobj.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 5110;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring =
		"expand_tuple_for_syntest done sbuf=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.SBUF__V2*/ meltfptr[1];
	      /*_.MELT_DEBUG_FUN__V32*/ meltfptr[24] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V31*/ meltfptr[19] =
	      /*_.MELT_DEBUG_FUN__V32*/ meltfptr[24];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-outobj.melt:5110:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L12*/ meltfnum[7] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V32*/ meltfptr[24] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V31*/ meltfptr[19] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-outobj.melt:5110:/ quasiblock");


      /*_.PROGN___V33*/ meltfptr[16] = /*_.IF___V31*/ meltfptr[19];;
      /*^compute */
      /*_.IFCPP___V30*/ meltfptr[26] = /*_.PROGN___V33*/ meltfptr[16];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:5110:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L11*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IF___V31*/ meltfptr[19] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V33*/ meltfptr[16] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V30*/ meltfptr[26] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-outobj.melt:5090:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.IFCPP___V30*/ meltfptr[26];;

    {
      MELT_LOCATION ("warmelt-outobj.melt:5090:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V6*/ meltfptr[5] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V30*/ meltfptr[26] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("EXPAND_TUPLE_FOR_SYNTEST", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_126_warmelt_outobj_EXPAND_TUPLE_FOR_SYNTEST_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_126_warmelt_outobj_EXPAND_TUPLE_FOR_SYNTEST */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_127_warmelt_outobj_SYNTESTGEN_PRIMITIVE (meltclosure_ptr_t
						  meltclosp_,
						  melt_ptr_t meltfirstargp_,
						  const melt_argdescr_cell_t
						  meltxargdescr_[],
						  union meltparam_un *
						  meltxargtab_,
						  const melt_argdescr_cell_t
						  meltxresdescr_[],
						  union meltparam_un *
						  meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_127_warmelt_outobj_SYNTESTGEN_PRIMITIVE_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_127_warmelt_outobj_SYNTESTGEN_PRIMITIVE_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 55
    melt_ptr_t mcfr_varptr[55];
#define MELTFRAM_NBVARNUM 17
    long mcfr_varnum[17];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_127_warmelt_outobj_SYNTESTGEN_PRIMITIVE is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_127_warmelt_outobj_SYNTESTGEN_PRIMITIVE_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 55; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_127_warmelt_outobj_SYNTESTGEN_PRIMITIVE nbval 55*/
  meltfram__.mcfr_nbvar = 55 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("SYNTESTGEN_PRIMITIVE", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-outobj.melt:5114:/ getarg");
 /*_.RECV__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.GENDEV__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.GENDEV__V3*/ meltfptr[2])) !=
	      NULL);


  /*getarg#2 */
  /*^getarg */
  if (meltxargdescr_[1] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.SBUF__V4*/ meltfptr[3] =
    (meltxargtab_[1].meltbp_aptr) ? (*(meltxargtab_[1].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.SBUF__V4*/ meltfptr[3])) !=
	      NULL);


  /*getarg#3 */
  /*^getarg */
  if (meltxargdescr_[2] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.MODCTX__V5*/ meltfptr[4] =
    (meltxargtab_[2].meltbp_aptr) ? (*(meltxargtab_[2].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.MODCTX__V5*/ meltfptr[4])) !=
	      NULL);


  /*getarg#4 */
  /*^getarg */
  if (meltxargdescr_[3] != MELTBPAR_LONG)
    goto lab_endgetargs;
 /*_#IX__L1*/ meltfnum[0] = meltxargtab_[3].meltbp_long;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:5115:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L2*/ meltfnum[1] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-outobj.melt:5115:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L2*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L3*/ meltfnum[2] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-outobj.melt:5115:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[7];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L3*/ meltfnum[2];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-outobj.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 5115;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "syntestgen_primitive recv=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.RECV__V2*/ meltfptr[1];
	      /*^apply.arg */
	      argtab[5].meltbp_cstring = " gendev=";
	      /*^apply.arg */
	      argtab[6].meltbp_aptr =
		(melt_ptr_t *) & /*_.GENDEV__V3*/ meltfptr[2];
	      /*_.MELT_DEBUG_FUN__V8*/ meltfptr[7] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V7*/ meltfptr[6] = /*_.MELT_DEBUG_FUN__V8*/ meltfptr[7];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-outobj.melt:5115:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L3*/ meltfnum[2] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V8*/ meltfptr[7] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V7*/ meltfptr[6] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-outobj.melt:5115:/ quasiblock");


      /*_.PROGN___V9*/ meltfptr[7] = /*_.IF___V7*/ meltfptr[6];;
      /*^compute */
      /*_.IFCPP___V6*/ meltfptr[5] = /*_.PROGN___V9*/ meltfptr[7];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:5115:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L2*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IF___V7*/ meltfptr[6] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V9*/ meltfptr[7] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V6*/ meltfptr[5] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:5116:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L4*/ meltfnum[2] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.RECV__V2*/ meltfptr[1]),
			     (melt_ptr_t) (( /*!CLASS_PRIMITIVE */ meltfrout->
					    tabval[1])));;
      MELT_LOCATION ("warmelt-outobj.melt:5116:/ cond");
      /*cond */ if ( /*_#IS_A__L4*/ meltfnum[2])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V11*/ meltfptr[7] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-outobj.melt:5116:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check recv"),
				  ("warmelt-outobj.melt")
				  ? ("warmelt-outobj.melt") : __FILE__,
				  (5116) ? (5116) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V11*/ meltfptr[7] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V10*/ meltfptr[6] = /*_.IFELSE___V11*/ meltfptr[7];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:5116:/ clear");
	     /*clear *//*_#IS_A__L4*/ meltfnum[2] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V11*/ meltfptr[7] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V10*/ meltfptr[6] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:5117:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L5*/ meltfnum[1] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.GENDEV__V3*/ meltfptr[2]),
			     (melt_ptr_t) (( /*!CLASS_SOURCE_GENERATOR_DEVICE */ meltfrout->tabval[2])));;
      MELT_LOCATION ("warmelt-outobj.melt:5117:/ cond");
      /*cond */ if ( /*_#IS_A__L5*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V13*/ meltfptr[12] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-outobj.melt:5117:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check gendev"),
				  ("warmelt-outobj.melt")
				  ? ("warmelt-outobj.melt") : __FILE__,
				  (5117) ? (5117) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V13*/ meltfptr[12] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V12*/ meltfptr[7] = /*_.IFELSE___V13*/ meltfptr[12];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:5117:/ clear");
	     /*clear *//*_#IS_A__L5*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V13*/ meltfptr[12] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V12*/ meltfptr[7] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:5118:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_STRBUF__L6*/ meltfnum[2] =
	(melt_magic_discr ((melt_ptr_t) ( /*_.SBUF__V4*/ meltfptr[3])) ==
	 MELTOBMAG_STRBUF);;
      MELT_LOCATION ("warmelt-outobj.melt:5118:/ cond");
      /*cond */ if ( /*_#IS_STRBUF__L6*/ meltfnum[2])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V15*/ meltfptr[14] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-outobj.melt:5118:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check sbuf"),
				  ("warmelt-outobj.melt")
				  ? ("warmelt-outobj.melt") : __FILE__,
				  (5118) ? (5118) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V15*/ meltfptr[14] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V14*/ meltfptr[12] = /*_.IFELSE___V15*/ meltfptr[14];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:5118:/ clear");
	     /*clear *//*_#IS_STRBUF__L6*/ meltfnum[2] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V15*/ meltfptr[14] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V14*/ meltfptr[12] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:5119:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L7*/ meltfnum[1] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.MODCTX__V5*/ meltfptr[4]),
			     (melt_ptr_t) (( /*!CLASS_MODULE_CONTEXT */
					    meltfrout->tabval[3])));;
      MELT_LOCATION ("warmelt-outobj.melt:5119:/ cond");
      /*cond */ if ( /*_#IS_A__L7*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V17*/ meltfptr[16] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-outobj.melt:5119:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check modctx"),
				  ("warmelt-outobj.melt")
				  ? ("warmelt-outobj.melt") : __FILE__,
				  (5119) ? (5119) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V17*/ meltfptr[16] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V16*/ meltfptr[14] = /*_.IFELSE___V17*/ meltfptr[16];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:5119:/ clear");
	     /*clear *//*_#IS_A__L7*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V17*/ meltfptr[16] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V16*/ meltfptr[14] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-outobj.melt:5120:/ quasiblock");


    /*^cond */
    /*cond */ if (
		   /*ifisa */
		   melt_is_instance_of ((melt_ptr_t)
					( /*_.GENDEV__V3*/ meltfptr[2]),
					(melt_ptr_t) (( /*!CLASS_LOCATED */
						       meltfrout->tabval[4])))
      )				/*then */
      {
	/*^cond.then */
	/*^getslot */
	{
	  melt_ptr_t slot = NULL, obj = NULL;
	  obj = (melt_ptr_t) ( /*_.GENDEV__V3*/ meltfptr[2]) /*=obj*/ ;
	  melt_object_get_field (slot, obj, 1, "LOCA_LOCATION");
   /*_.DLOC__V18*/ meltfptr[16] = slot;
	};
	;
      }
    else
      {				/*^cond.else */

  /*_.DLOC__V18*/ meltfptr[16] = NULL;;
      }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:5121:/ cond");
    /*cond */ if (
		   /*ifisa */
		   melt_is_instance_of ((melt_ptr_t)
					( /*_.GENDEV__V3*/ meltfptr[2]),
					(melt_ptr_t) (( /*!CLASS_SOURCE_GENERATOR_DEVICE */ meltfrout->tabval[2])))
      )				/*then */
      {
	/*^cond.then */
	/*^getslot */
	{
	  melt_ptr_t slot = NULL, obj = NULL;
	  obj = (melt_ptr_t) ( /*_.GENDEV__V3*/ meltfptr[2]) /*=obj*/ ;
	  melt_object_get_field (slot, obj, 2, "SRCGEN_DEFIN");
   /*_.PRIDEF__V19*/ meltfptr[18] = slot;
	};
	;
      }
    else
      {				/*^cond.else */

  /*_.PRIDEF__V19*/ meltfptr[18] = NULL;;
      }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:5122:/ cond");
    /*cond */ if (
		   /*ifisa */
		   melt_is_instance_of ((melt_ptr_t)
					( /*_.GENDEV__V3*/ meltfptr[2]),
					(melt_ptr_t) (( /*!CLASS_SOURCE_GENERATOR_DEVICE */ meltfrout->tabval[2])))
      )				/*then */
      {
	/*^cond.then */
	/*^getslot */
	{
	  melt_ptr_t slot = NULL, obj = NULL;
	  obj = (melt_ptr_t) ( /*_.GENDEV__V3*/ meltfptr[2]) /*=obj*/ ;
	  melt_object_get_field (slot, obj, 3, "SRCGEN_REPR");
   /*_.PRIREP__V20*/ meltfptr[19] = slot;
	};
	;
      }
    else
      {				/*^cond.else */

  /*_.PRIREP__V20*/ meltfptr[19] = NULL;;
      }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:5123:/ cond");
    /*cond */ if (
		   /*ifisa */
		   melt_is_instance_of ((melt_ptr_t)
					( /*_.RECV__V2*/ meltfptr[1]),
					(melt_ptr_t) (( /*!CLASS_NAMED */
						       meltfrout->tabval[5])))
      )				/*then */
      {
	/*^cond.then */
	/*^getslot */
	{
	  melt_ptr_t slot = NULL, obj = NULL;
	  obj = (melt_ptr_t) ( /*_.RECV__V2*/ meltfptr[1]) /*=obj*/ ;
	  melt_object_get_field (slot, obj, 1, "NAMED_NAME");
   /*_.PRINAM__V21*/ meltfptr[20] = slot;
	};
	;
      }
    else
      {				/*^cond.else */

  /*_.PRINAM__V21*/ meltfptr[20] = NULL;;
      }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:5124:/ cond");
    /*cond */ if (
		   /*ifisa */
		   melt_is_instance_of ((melt_ptr_t)
					( /*_.RECV__V2*/ meltfptr[1]),
					(melt_ptr_t) (( /*!CLASS_PRIMITIVE */
						       meltfrout->tabval[1])))
      )				/*then */
      {
	/*^cond.then */
	/*^getslot */
	{
	  melt_ptr_t slot = NULL, obj = NULL;
	  obj = (melt_ptr_t) ( /*_.RECV__V2*/ meltfptr[1]) /*=obj*/ ;
	  melt_object_get_field (slot, obj, 2, "PRIM_FORMALS");
   /*_.PRIFORMALS__V22*/ meltfptr[21] = slot;
	};
	;
      }
    else
      {				/*^cond.else */

  /*_.PRIFORMALS__V22*/ meltfptr[21] = NULL;;
      }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:5125:/ cond");
    /*cond */ if (
		   /*ifisa */
		   melt_is_instance_of ((melt_ptr_t)
					( /*_.PRIDEF__V19*/ meltfptr[18]),
					(melt_ptr_t) (( /*!CLASS_SOURCE_DEFPRIMITIVE */ meltfrout->tabval[6])))
      )				/*then */
      {
	/*^cond.then */
	/*^getslot */
	{
	  melt_ptr_t slot = NULL, obj = NULL;
	  obj = (melt_ptr_t) ( /*_.PRIDEF__V19*/ meltfptr[18]) /*=obj*/ ;
	  melt_object_get_field (slot, obj, 7, "SPRIM_EXPLOC");
   /*_.SPRIM_EXPLOC__V23*/ meltfptr[22] = slot;
	};
	;
      }
    else
      {				/*^cond.else */

  /*_.SPRIM_EXPLOC__V23*/ meltfptr[22] = NULL;;
      }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:5125:/ cond");
    /*cond */ if ( /*_.SPRIM_EXPLOC__V23*/ meltfptr[22])	/*then */
      {
	/*^cond.then */
	/*_.EXPLOC__V24*/ meltfptr[23] =
	  /*_.SPRIM_EXPLOC__V23*/ meltfptr[22];;
      }
    else
      {
	MELT_LOCATION ("warmelt-outobj.melt:5125:/ cond.else");

	/*_.EXPLOC__V24*/ meltfptr[23] = /*_.DLOC__V18*/ meltfptr[16];;
      }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:5126:/ cond");
    /*cond */ if (
		   /*ifisa */
		   melt_is_instance_of ((melt_ptr_t)
					( /*_.RECV__V2*/ meltfptr[1]),
					(melt_ptr_t) (( /*!CLASS_PRIMITIVE */
						       meltfrout->tabval[1])))
      )				/*then */
      {
	/*^cond.then */
	/*^getslot */
	{
	  melt_ptr_t slot = NULL, obj = NULL;
	  obj = (melt_ptr_t) ( /*_.RECV__V2*/ meltfptr[1]) /*=obj*/ ;
	  melt_object_get_field (slot, obj, 3, "PRIM_TYPE");
   /*_.PRITYPE__V25*/ meltfptr[24] = slot;
	};
	;
      }
    else
      {				/*^cond.else */

  /*_.PRITYPE__V25*/ meltfptr[24] = NULL;;
      }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:5127:/ cond");
    /*cond */ if (
		   /*ifisa */
		   melt_is_instance_of ((melt_ptr_t)
					( /*_.RECV__V2*/ meltfptr[1]),
					(melt_ptr_t) (( /*!CLASS_PRIMITIVE */
						       meltfrout->tabval[1])))
      )				/*then */
      {
	/*^cond.then */
	/*^getslot */
	{
	  melt_ptr_t slot = NULL, obj = NULL;
	  obj = (melt_ptr_t) ( /*_.RECV__V2*/ meltfptr[1]) /*=obj*/ ;
	  melt_object_get_field (slot, obj, 4, "PRIM_EXPANSION");
   /*_.PRIMEXPAN__V26*/ meltfptr[25] = slot;
	};
	;
      }
    else
      {				/*^cond.else */

  /*_.PRIMEXPAN__V26*/ meltfptr[25] = NULL;;
      }
    ;
    /*^compute */
 /*_#MULTIPLE_LENGTH__L8*/ meltfnum[2] =
      (melt_multiple_length
       ((melt_ptr_t) ( /*_.PRIFORMALS__V22*/ meltfptr[21])));;
    /*^compute */
 /*_#I__L9*/ meltfnum[1] =
      ((2) * ( /*_#MULTIPLE_LENGTH__L8*/ meltfnum[2]));;
    /*^compute */
 /*_#I__L10*/ meltfnum[9] =
      ((5) + ( /*_#I__L9*/ meltfnum[1]));;
    /*^compute */
 /*_.REPLMAP__V27*/ meltfptr[26] =
      (meltgc_new_mapobjects
       ((meltobject_ptr_t) (( /*!DISCR_MAP_OBJECTS */ meltfrout->tabval[7])),
	( /*_#I__L10*/ meltfnum[9])));;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:5131:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L11*/ meltfnum[10] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.PRIDEF__V19*/ meltfptr[18]),
			     (melt_ptr_t) (( /*!CLASS_SOURCE_DEFPRIMITIVE */
					    meltfrout->tabval[6])));;
      MELT_LOCATION ("warmelt-outobj.melt:5131:/ cond");
      /*cond */ if ( /*_#IS_A__L11*/ meltfnum[10])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V29*/ meltfptr[28] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-outobj.melt:5131:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check pridef"),
				  ("warmelt-outobj.melt")
				  ? ("warmelt-outobj.melt") : __FILE__,
				  (5131) ? (5131) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V29*/ meltfptr[28] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V28*/ meltfptr[27] = /*_.IFELSE___V29*/ meltfptr[28];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:5131:/ clear");
	     /*clear *//*_#IS_A__L11*/ meltfnum[10] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V29*/ meltfptr[28] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V28*/ meltfptr[27] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:5132:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#__L12*/ meltfnum[10] =
	(( /*_.PRIREP__V20*/ meltfptr[19]) == ( /*_.RECV__V2*/ meltfptr[1]));;
      MELT_LOCATION ("warmelt-outobj.melt:5132:/ cond");
      /*cond */ if ( /*_#__L12*/ meltfnum[10])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V31*/ meltfptr[30] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-outobj.melt:5132:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check prirep"),
				  ("warmelt-outobj.melt")
				  ? ("warmelt-outobj.melt") : __FILE__,
				  (5132) ? (5132) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V31*/ meltfptr[30] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V30*/ meltfptr[28] = /*_.IFELSE___V31*/ meltfptr[30];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:5132:/ clear");
	     /*clear *//*_#__L12*/ meltfnum[10] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V31*/ meltfptr[30] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V30*/ meltfptr[28] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-outobj.melt:5133:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_cstring = "/*primitive-syntax ";
      /*_.ADD2OUT__V32*/ meltfptr[30] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!ADD2OUT */ meltfrout->tabval[8])),
		    (melt_ptr_t) ( /*_.SBUF__V4*/ meltfptr[3]),
		    (MELTBPARSTR_CSTRING ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:5134:/ locexp");
      meltgc_add_strbuf_ccomment ((melt_ptr_t) ( /*_.SBUF__V4*/ meltfptr[3]),
				  melt_string_str ((melt_ptr_t)
						   ( /*_.PRINAM__V21*/
						    meltfptr[20])));
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:5135:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_cstring = "*/ {";
      /*_.ADD2OUT__V33*/ meltfptr[32] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!ADD2OUT */ meltfrout->tabval[8])),
		    (melt_ptr_t) ( /*_.SBUF__V4*/ meltfptr[3]),
		    (MELTBPARSTR_CSTRING ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:5136:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.SBUF__V4*/ meltfptr[3]),
				(1), 0);
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:5137:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[3];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & /*_.REPLMAP__V27*/ meltfptr[26];
      /*^apply.arg */
      argtab[1].meltbp_aptr =
	(melt_ptr_t *) & /*_.PRIFORMALS__V22*/ meltfptr[21];
      /*^apply.arg */
      argtab[2].meltbp_cstring = "primf_";
      /*_.SUBSTITUTE_FORMALS_FOR_SYNTEST__V34*/ meltfptr[33] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!SUBSTITUTE_FORMALS_FOR_SYNTEST */ meltfrout->
		      tabval[9])), (melt_ptr_t) ( /*_.SBUF__V4*/ meltfptr[3]),
		    (MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_CSTRING ""),
		    argtab, "", (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:5139:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#__L13*/ meltfnum[10] =
      (( /*_.PRITYPE__V25*/ meltfptr[24]) !=
       (( /*!CTYPE_VOID */ meltfrout->tabval[10])));;
    MELT_LOCATION ("warmelt-outobj.melt:5139:/ cond");
    /*cond */ if ( /*_#__L13*/ meltfnum[10])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-outobj.melt:5140:/ quasiblock");


	  /*^cond */
	  /*cond */ if (
			 /*ifisa */
			 melt_is_instance_of ((melt_ptr_t)
					      ( /*_.PRITYPE__V25*/
					       meltfptr[24]),
					      (melt_ptr_t) (( /*!CLASS_CTYPE */ meltfrout->tabval[11])))
	    )			/*then */
	    {
	      /*^cond.then */
	      /*^getslot */
	      {
		melt_ptr_t slot = NULL, obj = NULL;
		obj =
		  (melt_ptr_t) ( /*_.PRITYPE__V25*/ meltfptr[24]) /*=obj*/ ;
		melt_object_get_field (slot, obj, 3, "CTYPE_CNAME");
     /*_.PRITYNAM__V37*/ meltfptr[36] = slot;
	      };
	      ;
	    }
	  else
	    {			/*^cond.else */

    /*_.PRITYNAM__V37*/ meltfptr[36] = NULL;;
	    }
	  ;
	  MELT_LOCATION ("warmelt-outobj.melt:5142:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^apply */
	  /*apply */
	  {
	    union meltparam_un argtab[4];
	    memset (&argtab, 0, sizeof (argtab));
	    /*^apply.arg */
	    argtab[0].meltbp_aptr =
	      (melt_ptr_t *) & /*_.PRITYNAM__V37*/ meltfptr[36];
	    /*^apply.arg */
	    argtab[1].meltbp_cstring = " primres_";
	    /*^apply.arg */
	    argtab[2].meltbp_long = /*_#IX__L1*/ meltfnum[0];
	    /*^apply.arg */
	    argtab[3].meltbp_cstring = " = ";
	    /*_.ADD2OUT__V38*/ meltfptr[37] =
	      melt_apply ((meltclosure_ptr_t)
			  (( /*!ADD2OUT */ meltfrout->tabval[8])),
			  (melt_ptr_t) ( /*_.SBUF__V4*/ meltfptr[3]),
			  (MELTBPARSTR_PTR MELTBPARSTR_CSTRING
			   MELTBPARSTR_LONG MELTBPARSTR_CSTRING ""), argtab,
			  "", (union meltparam_un *) 0);
	  }
	  ;
	  /*_.LET___V36*/ meltfptr[35] = /*_.ADD2OUT__V38*/ meltfptr[37];;

	  MELT_LOCATION ("warmelt-outobj.melt:5140:/ clear");
	     /*clear *//*_.PRITYNAM__V37*/ meltfptr[36] = 0;
	  /*^clear */
	     /*clear *//*_.ADD2OUT__V38*/ meltfptr[37] = 0;
	  /*_.IF___V35*/ meltfptr[34] = /*_.LET___V36*/ meltfptr[35];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-outobj.melt:5139:/ clear");
	     /*clear *//*_.LET___V36*/ meltfptr[35] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

  /*_.IF___V35*/ meltfptr[34] = NULL;;
      }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:5145:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^cond */
    /*cond */ if ( /*_.EXPLOC__V24*/ meltfptr[23])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  /*^checksignal */
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^apply */
	  /*apply */
	  {
	    union meltparam_un argtab[3];
	    memset (&argtab, 0, sizeof (argtab));
	    /*^apply.arg */
	    argtab[0].meltbp_aptr =
	      (melt_ptr_t *) & /*_.SBUF__V4*/ meltfptr[3];
	    /*^apply.arg */
	    argtab[1].meltbp_long = 0;
	    /*^apply.arg */
	    argtab[2].meltbp_cstring = "primitive-syntax";
	    /*_.OUTPUT_RAW_LOCATION__V40*/ meltfptr[37] =
	      melt_apply ((meltclosure_ptr_t)
			  (( /*!OUTPUT_RAW_LOCATION */ meltfrout->
			    tabval[12])),
			  (melt_ptr_t) ( /*_.EXPLOC__V24*/ meltfptr[23]),
			  (MELTBPARSTR_PTR MELTBPARSTR_LONG
			   MELTBPARSTR_CSTRING ""), argtab, "",
			  (union meltparam_un *) 0);
	  }
	  ;
	  /*_.IF___V39*/ meltfptr[36] =
	    /*_.OUTPUT_RAW_LOCATION__V40*/ meltfptr[37];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-outobj.melt:5145:/ clear");
	     /*clear *//*_.OUTPUT_RAW_LOCATION__V40*/ meltfptr[37] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

  /*_.IF___V39*/ meltfptr[36] = NULL;;
      }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:5146:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#__L14*/ meltfnum[13] =
      (( /*_.PRITYPE__V25*/ meltfptr[24]) !=
       (( /*!CTYPE_VOID */ meltfrout->tabval[10])));;
    MELT_LOCATION ("warmelt-outobj.melt:5146:/ cond");
    /*cond */ if ( /*_#__L14*/ meltfnum[13])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-outobj.melt:5147:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^apply */
	  /*apply */
	  {
	    union meltparam_un argtab[1];
	    memset (&argtab, 0, sizeof (argtab));
	    /*^apply.arg */
	    argtab[0].meltbp_cstring = "    ";
	    /*_.ADD2OUT__V42*/ meltfptr[37] =
	      melt_apply ((meltclosure_ptr_t)
			  (( /*!ADD2OUT */ meltfrout->tabval[8])),
			  (melt_ptr_t) ( /*_.SBUF__V4*/ meltfptr[3]),
			  (MELTBPARSTR_CSTRING ""), argtab, "",
			  (union meltparam_un *) 0);
	  }
	  ;
	  /*_.IF___V41*/ meltfptr[35] = /*_.ADD2OUT__V42*/ meltfptr[37];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-outobj.melt:5146:/ clear");
	     /*clear *//*_.ADD2OUT__V42*/ meltfptr[37] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

  /*_.IF___V41*/ meltfptr[35] = NULL;;
      }
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:5148:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L15*/ meltfnum[14] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-outobj.melt:5148:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L15*/ meltfnum[14])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L16*/ meltfnum[15] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-outobj.melt:5148:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long =
		/*_#THE_MELTCALLCOUNT__L16*/ meltfnum[15];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-outobj.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 5148;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "syntestgen_primitive primexpan=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.PRIMEXPAN__V26*/ meltfptr[25];
	      /*_.MELT_DEBUG_FUN__V45*/ meltfptr[44] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V44*/ meltfptr[43] =
	      /*_.MELT_DEBUG_FUN__V45*/ meltfptr[44];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-outobj.melt:5148:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L16*/ meltfnum[15] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V45*/ meltfptr[44] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V44*/ meltfptr[43] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-outobj.melt:5148:/ quasiblock");


      /*_.PROGN___V46*/ meltfptr[44] = /*_.IF___V44*/ meltfptr[43];;
      /*^compute */
      /*_.IFCPP___V43*/ meltfptr[37] = /*_.PROGN___V46*/ meltfptr[44];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:5148:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L15*/ meltfnum[14] = 0;
      /*^clear */
	     /*clear *//*_.IF___V44*/ meltfptr[43] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V46*/ meltfptr[44] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V43*/ meltfptr[37] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-outobj.melt:5149:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[3];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & /*_.REPLMAP__V27*/ meltfptr[26];
      /*^apply.arg */
      argtab[1].meltbp_aptr =
	(melt_ptr_t *) & /*_.PRIMEXPAN__V26*/ meltfptr[25];
      /*^apply.arg */
      argtab[2].meltbp_aptr = (melt_ptr_t *) & /*_.EXPLOC__V24*/ meltfptr[23];
      /*_.EXPAND_TUPLE_FOR_SYNTEST__V47*/ meltfptr[43] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!EXPAND_TUPLE_FOR_SYNTEST */ meltfrout->tabval[13])),
		    (melt_ptr_t) ( /*_.SBUF__V4*/ meltfptr[3]),
		    (MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_PTR ""),
		    argtab, "", (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:5151:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#__L17*/ meltfnum[15] =
      (( /*_.PRITYPE__V25*/ meltfptr[24]) !=
       (( /*!CTYPE_VOID */ meltfrout->tabval[10])));;
    MELT_LOCATION ("warmelt-outobj.melt:5151:/ cond");
    /*cond */ if ( /*_#__L17*/ meltfnum[15])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-outobj.melt:5152:/ quasiblock");


	  /*^cond */
	  /*cond */ if (
			 /*ifisa */
			 melt_is_instance_of ((melt_ptr_t)
					      ( /*_.PRITYPE__V25*/
					       meltfptr[24]),
					      (melt_ptr_t) (( /*!CLASS_CTYPE */ meltfrout->tabval[11])))
	    )			/*then */
	    {
	      /*^cond.then */
	      /*^getslot */
	      {
		melt_ptr_t slot = NULL, obj = NULL;
		obj =
		  (melt_ptr_t) ( /*_.PRITYPE__V25*/ meltfptr[24]) /*=obj*/ ;
		melt_object_get_field (slot, obj, 3, "CTYPE_CNAME");
     /*_.PRITYNAM__V50*/ meltfptr[49] = slot;
	      };
	      ;
	    }
	  else
	    {			/*^cond.else */

    /*_.PRITYNAM__V50*/ meltfptr[49] = NULL;;
	    }
	  ;
	  MELT_LOCATION ("warmelt-outobj.melt:5154:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^apply */
	  /*apply */
	  {
	    union meltparam_un argtab[1];
	    memset (&argtab, 0, sizeof (argtab));
	    /*^apply.arg */
	    argtab[0].meltbp_cstring = ";";
	    /*_.ADD2OUT__V51*/ meltfptr[50] =
	      melt_apply ((meltclosure_ptr_t)
			  (( /*!ADD2OUT */ meltfrout->tabval[8])),
			  (melt_ptr_t) ( /*_.SBUF__V4*/ meltfptr[3]),
			  (MELTBPARSTR_CSTRING ""), argtab, "",
			  (union meltparam_un *) 0);
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:5155:/ locexp");
	    meltgc_strbuf_add_indent ((melt_ptr_t)
				      ( /*_.SBUF__V4*/ meltfptr[3]), (2), 0);
	  }
	  ;
	  MELT_LOCATION ("warmelt-outobj.melt:5156:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^apply */
	  /*apply */
	  {
	    union meltparam_un argtab[3];
	    memset (&argtab, 0, sizeof (argtab));
	    /*^apply.arg */
	    argtab[0].meltbp_cstring = "if (primres_";
	    /*^apply.arg */
	    argtab[1].meltbp_long = /*_#IX__L1*/ meltfnum[0];
	    /*^apply.arg */
	    argtab[2].meltbp_cstring = ") return";
	    /*_.ADD2OUT__V52*/ meltfptr[51] =
	      melt_apply ((meltclosure_ptr_t)
			  (( /*!ADD2OUT */ meltfrout->tabval[8])),
			  (melt_ptr_t) ( /*_.SBUF__V4*/ meltfptr[3]),
			  (MELTBPARSTR_CSTRING MELTBPARSTR_LONG
			   MELTBPARSTR_CSTRING ""), argtab, "",
			  (union meltparam_un *) 0);
	  }
	  ;
	  /*_.LET___V49*/ meltfptr[48] = /*_.ADD2OUT__V52*/ meltfptr[51];;

	  MELT_LOCATION ("warmelt-outobj.melt:5152:/ clear");
	     /*clear *//*_.PRITYNAM__V50*/ meltfptr[49] = 0;
	  /*^clear */
	     /*clear *//*_.ADD2OUT__V51*/ meltfptr[50] = 0;
	  /*^clear */
	     /*clear *//*_.ADD2OUT__V52*/ meltfptr[51] = 0;
	  /*_.IF___V48*/ meltfptr[44] = /*_.LET___V49*/ meltfptr[48];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-outobj.melt:5151:/ clear");
	     /*clear *//*_.LET___V49*/ meltfptr[48] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

  /*_.IF___V48*/ meltfptr[44] = NULL;;
      }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:5159:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_cstring = ";";
      /*_.ADD2OUT__V53*/ meltfptr[49] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!ADD2OUT */ meltfrout->tabval[8])),
		    (melt_ptr_t) ( /*_.SBUF__V4*/ meltfptr[3]),
		    (MELTBPARSTR_CSTRING ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:5160:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.SBUF__V4*/ meltfptr[3]),
				(1), 0);
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:5161:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_cstring = "} /*end primitive-syntax ";
      /*_.ADD2OUT__V54*/ meltfptr[50] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!ADD2OUT */ meltfrout->tabval[8])),
		    (melt_ptr_t) ( /*_.SBUF__V4*/ meltfptr[3]),
		    (MELTBPARSTR_CSTRING ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:5162:/ locexp");
      meltgc_add_strbuf_ccomment ((melt_ptr_t) ( /*_.SBUF__V4*/ meltfptr[3]),
				  melt_string_str ((melt_ptr_t)
						   ( /*_.PRINAM__V21*/
						    meltfptr[20])));
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:5163:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_cstring = "*/";
      /*_.ADD2OUT__V55*/ meltfptr[51] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!ADD2OUT */ meltfrout->tabval[8])),
		    (melt_ptr_t) ( /*_.SBUF__V4*/ meltfptr[3]),
		    (MELTBPARSTR_CSTRING ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:5164:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.SBUF__V4*/ meltfptr[3]),
				(0), 0);
    }
    ;

    MELT_LOCATION ("warmelt-outobj.melt:5120:/ clear");
	   /*clear *//*_.DLOC__V18*/ meltfptr[16] = 0;
    /*^clear */
	   /*clear *//*_.PRIDEF__V19*/ meltfptr[18] = 0;
    /*^clear */
	   /*clear *//*_.PRIREP__V20*/ meltfptr[19] = 0;
    /*^clear */
	   /*clear *//*_.PRINAM__V21*/ meltfptr[20] = 0;
    /*^clear */
	   /*clear *//*_.PRIFORMALS__V22*/ meltfptr[21] = 0;
    /*^clear */
	   /*clear *//*_.SPRIM_EXPLOC__V23*/ meltfptr[22] = 0;
    /*^clear */
	   /*clear *//*_.EXPLOC__V24*/ meltfptr[23] = 0;
    /*^clear */
	   /*clear *//*_.PRITYPE__V25*/ meltfptr[24] = 0;
    /*^clear */
	   /*clear *//*_.PRIMEXPAN__V26*/ meltfptr[25] = 0;
    /*^clear */
	   /*clear *//*_#MULTIPLE_LENGTH__L8*/ meltfnum[2] = 0;
    /*^clear */
	   /*clear *//*_#I__L9*/ meltfnum[1] = 0;
    /*^clear */
	   /*clear *//*_#I__L10*/ meltfnum[9] = 0;
    /*^clear */
	   /*clear *//*_.REPLMAP__V27*/ meltfptr[26] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V28*/ meltfptr[27] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V30*/ meltfptr[28] = 0;
    /*^clear */
	   /*clear *//*_.ADD2OUT__V32*/ meltfptr[30] = 0;
    /*^clear */
	   /*clear *//*_.ADD2OUT__V33*/ meltfptr[32] = 0;
    /*^clear */
	   /*clear *//*_.SUBSTITUTE_FORMALS_FOR_SYNTEST__V34*/ meltfptr[33] = 0;
    /*^clear */
	   /*clear *//*_#__L13*/ meltfnum[10] = 0;
    /*^clear */
	   /*clear *//*_.IF___V35*/ meltfptr[34] = 0;
    /*^clear */
	   /*clear *//*_.IF___V39*/ meltfptr[36] = 0;
    /*^clear */
	   /*clear *//*_#__L14*/ meltfnum[13] = 0;
    /*^clear */
	   /*clear *//*_.IF___V41*/ meltfptr[35] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V43*/ meltfptr[37] = 0;
    /*^clear */
	   /*clear *//*_.EXPAND_TUPLE_FOR_SYNTEST__V47*/ meltfptr[43] = 0;
    /*^clear */
	   /*clear *//*_#__L17*/ meltfnum[15] = 0;
    /*^clear */
	   /*clear *//*_.IF___V48*/ meltfptr[44] = 0;
    /*^clear */
	   /*clear *//*_.ADD2OUT__V53*/ meltfptr[49] = 0;
    /*^clear */
	   /*clear *//*_.ADD2OUT__V54*/ meltfptr[50] = 0;
    /*^clear */
	   /*clear *//*_.ADD2OUT__V55*/ meltfptr[51] = 0;
    MELT_LOCATION ("warmelt-outobj.melt:5114:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V6*/ meltfptr[5] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V10*/ meltfptr[6] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V12*/ meltfptr[7] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V14*/ meltfptr[12] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V16*/ meltfptr[14] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("SYNTESTGEN_PRIMITIVE", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_127_warmelt_outobj_SYNTESTGEN_PRIMITIVE_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*noretval */ NULL);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_127_warmelt_outobj_SYNTESTGEN_PRIMITIVE */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_128_warmelt_outobj_SYNTESTGEN_CITERATOR (meltclosure_ptr_t
						  meltclosp_,
						  melt_ptr_t meltfirstargp_,
						  const melt_argdescr_cell_t
						  meltxargdescr_[],
						  union meltparam_un *
						  meltxargtab_,
						  const melt_argdescr_cell_t
						  meltxresdescr_[],
						  union meltparam_un *
						  meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_128_warmelt_outobj_SYNTESTGEN_CITERATOR_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_128_warmelt_outobj_SYNTESTGEN_CITERATOR_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 82
    melt_ptr_t mcfr_varptr[82];
#define MELTFRAM_NBVARNUM 23
    long mcfr_varnum[23];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_128_warmelt_outobj_SYNTESTGEN_CITERATOR is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_128_warmelt_outobj_SYNTESTGEN_CITERATOR_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 82; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_128_warmelt_outobj_SYNTESTGEN_CITERATOR nbval 82*/
  meltfram__.mcfr_nbvar = 82 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("SYNTESTGEN_CITERATOR", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-outobj.melt:5174:/ getarg");
 /*_.RECV__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.GENDEV__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.GENDEV__V3*/ meltfptr[2])) !=
	      NULL);


  /*getarg#2 */
  /*^getarg */
  if (meltxargdescr_[1] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.SBUF__V4*/ meltfptr[3] =
    (meltxargtab_[1].meltbp_aptr) ? (*(meltxargtab_[1].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.SBUF__V4*/ meltfptr[3])) !=
	      NULL);


  /*getarg#3 */
  /*^getarg */
  if (meltxargdescr_[2] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.MODCTX__V5*/ meltfptr[4] =
    (meltxargtab_[2].meltbp_aptr) ? (*(meltxargtab_[2].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.MODCTX__V5*/ meltfptr[4])) !=
	      NULL);


  /*getarg#4 */
  /*^getarg */
  if (meltxargdescr_[3] != MELTBPAR_LONG)
    goto lab_endgetargs;
 /*_#IX__L1*/ meltfnum[0] = meltxargtab_[3].meltbp_long;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:5175:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L2*/ meltfnum[1] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-outobj.melt:5175:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L2*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L3*/ meltfnum[2] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-outobj.melt:5175:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[7];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L3*/ meltfnum[2];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-outobj.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 5175;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "syntestgen_citerator recv=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.RECV__V2*/ meltfptr[1];
	      /*^apply.arg */
	      argtab[5].meltbp_cstring = " gendev=";
	      /*^apply.arg */
	      argtab[6].meltbp_aptr =
		(melt_ptr_t *) & /*_.GENDEV__V3*/ meltfptr[2];
	      /*_.MELT_DEBUG_FUN__V8*/ meltfptr[7] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V7*/ meltfptr[6] = /*_.MELT_DEBUG_FUN__V8*/ meltfptr[7];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-outobj.melt:5175:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L3*/ meltfnum[2] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V8*/ meltfptr[7] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V7*/ meltfptr[6] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-outobj.melt:5175:/ quasiblock");


      /*_.PROGN___V9*/ meltfptr[7] = /*_.IF___V7*/ meltfptr[6];;
      /*^compute */
      /*_.IFCPP___V6*/ meltfptr[5] = /*_.PROGN___V9*/ meltfptr[7];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:5175:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L2*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IF___V7*/ meltfptr[6] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V9*/ meltfptr[7] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V6*/ meltfptr[5] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:5176:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L4*/ meltfnum[2] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.RECV__V2*/ meltfptr[1]),
			     (melt_ptr_t) (( /*!CLASS_CITERATOR */ meltfrout->
					    tabval[1])));;
      MELT_LOCATION ("warmelt-outobj.melt:5176:/ cond");
      /*cond */ if ( /*_#IS_A__L4*/ meltfnum[2])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V11*/ meltfptr[7] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-outobj.melt:5176:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check recv"),
				  ("warmelt-outobj.melt")
				  ? ("warmelt-outobj.melt") : __FILE__,
				  (5176) ? (5176) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V11*/ meltfptr[7] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V10*/ meltfptr[6] = /*_.IFELSE___V11*/ meltfptr[7];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:5176:/ clear");
	     /*clear *//*_#IS_A__L4*/ meltfnum[2] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V11*/ meltfptr[7] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V10*/ meltfptr[6] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:5177:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L5*/ meltfnum[1] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.GENDEV__V3*/ meltfptr[2]),
			     (melt_ptr_t) (( /*!CLASS_SOURCE_GENERATOR_DEVICE */ meltfrout->tabval[2])));;
      MELT_LOCATION ("warmelt-outobj.melt:5177:/ cond");
      /*cond */ if ( /*_#IS_A__L5*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V13*/ meltfptr[12] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-outobj.melt:5177:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check gendev"),
				  ("warmelt-outobj.melt")
				  ? ("warmelt-outobj.melt") : __FILE__,
				  (5177) ? (5177) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V13*/ meltfptr[12] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V12*/ meltfptr[7] = /*_.IFELSE___V13*/ meltfptr[12];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:5177:/ clear");
	     /*clear *//*_#IS_A__L5*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V13*/ meltfptr[12] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V12*/ meltfptr[7] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:5178:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_STRBUF__L6*/ meltfnum[2] =
	(melt_magic_discr ((melt_ptr_t) ( /*_.SBUF__V4*/ meltfptr[3])) ==
	 MELTOBMAG_STRBUF);;
      MELT_LOCATION ("warmelt-outobj.melt:5178:/ cond");
      /*cond */ if ( /*_#IS_STRBUF__L6*/ meltfnum[2])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V15*/ meltfptr[14] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-outobj.melt:5178:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check sbuf"),
				  ("warmelt-outobj.melt")
				  ? ("warmelt-outobj.melt") : __FILE__,
				  (5178) ? (5178) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V15*/ meltfptr[14] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V14*/ meltfptr[12] = /*_.IFELSE___V15*/ meltfptr[14];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:5178:/ clear");
	     /*clear *//*_#IS_STRBUF__L6*/ meltfnum[2] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V15*/ meltfptr[14] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V14*/ meltfptr[12] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:5179:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L7*/ meltfnum[1] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.MODCTX__V5*/ meltfptr[4]),
			     (melt_ptr_t) (( /*!CLASS_MODULE_CONTEXT */
					    meltfrout->tabval[3])));;
      MELT_LOCATION ("warmelt-outobj.melt:5179:/ cond");
      /*cond */ if ( /*_#IS_A__L7*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V17*/ meltfptr[16] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-outobj.melt:5179:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check modctx"),
				  ("warmelt-outobj.melt")
				  ? ("warmelt-outobj.melt") : __FILE__,
				  (5179) ? (5179) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V17*/ meltfptr[16] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V16*/ meltfptr[14] = /*_.IFELSE___V17*/ meltfptr[16];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:5179:/ clear");
	     /*clear *//*_#IS_A__L7*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V17*/ meltfptr[16] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V16*/ meltfptr[14] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-outobj.melt:5180:/ quasiblock");


    /*^cond */
    /*cond */ if (
		   /*ifisa */
		   melt_is_instance_of ((melt_ptr_t)
					( /*_.GENDEV__V3*/ meltfptr[2]),
					(melt_ptr_t) (( /*!CLASS_LOCATED */
						       meltfrout->tabval[4])))
      )				/*then */
      {
	/*^cond.then */
	/*^getslot */
	{
	  melt_ptr_t slot = NULL, obj = NULL;
	  obj = (melt_ptr_t) ( /*_.GENDEV__V3*/ meltfptr[2]) /*=obj*/ ;
	  melt_object_get_field (slot, obj, 1, "LOCA_LOCATION");
   /*_.DLOC__V18*/ meltfptr[16] = slot;
	};
	;
      }
    else
      {				/*^cond.else */

  /*_.DLOC__V18*/ meltfptr[16] = NULL;;
      }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:5181:/ cond");
    /*cond */ if (
		   /*ifisa */
		   melt_is_instance_of ((melt_ptr_t)
					( /*_.GENDEV__V3*/ meltfptr[2]),
					(melt_ptr_t) (( /*!CLASS_SOURCE_GENERATOR_DEVICE */ meltfrout->tabval[2])))
      )				/*then */
      {
	/*^cond.then */
	/*^getslot */
	{
	  melt_ptr_t slot = NULL, obj = NULL;
	  obj = (melt_ptr_t) ( /*_.GENDEV__V3*/ meltfptr[2]) /*=obj*/ ;
	  melt_object_get_field (slot, obj, 2, "SRCGEN_DEFIN");
   /*_.CITDEF__V19*/ meltfptr[18] = slot;
	};
	;
      }
    else
      {				/*^cond.else */

  /*_.CITDEF__V19*/ meltfptr[18] = NULL;;
      }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:5182:/ cond");
    /*cond */ if (
		   /*ifisa */
		   melt_is_instance_of ((melt_ptr_t)
					( /*_.GENDEV__V3*/ meltfptr[2]),
					(melt_ptr_t) (( /*!CLASS_SOURCE_GENERATOR_DEVICE */ meltfrout->tabval[2])))
      )				/*then */
      {
	/*^cond.then */
	/*^getslot */
	{
	  melt_ptr_t slot = NULL, obj = NULL;
	  obj = (melt_ptr_t) ( /*_.GENDEV__V3*/ meltfptr[2]) /*=obj*/ ;
	  melt_object_get_field (slot, obj, 3, "SRCGEN_REPR");
   /*_.CITREP__V20*/ meltfptr[19] = slot;
	};
	;
      }
    else
      {				/*^cond.else */

  /*_.CITREP__V20*/ meltfptr[19] = NULL;;
      }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:5183:/ cond");
    /*cond */ if (
		   /*ifisa */
		   melt_is_instance_of ((melt_ptr_t)
					( /*_.RECV__V2*/ meltfptr[1]),
					(melt_ptr_t) (( /*!CLASS_NAMED */
						       meltfrout->tabval[5])))
      )				/*then */
      {
	/*^cond.then */
	/*^getslot */
	{
	  melt_ptr_t slot = NULL, obj = NULL;
	  obj = (melt_ptr_t) ( /*_.RECV__V2*/ meltfptr[1]) /*=obj*/ ;
	  melt_object_get_field (slot, obj, 1, "NAMED_NAME");
   /*_.CITNAM__V21*/ meltfptr[20] = slot;
	};
	;
      }
    else
      {				/*^cond.else */

  /*_.CITNAM__V21*/ meltfptr[20] = NULL;;
      }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:5184:/ cond");
    /*cond */ if (
		   /*ifisa */
		   melt_is_instance_of ((melt_ptr_t)
					( /*_.RECV__V2*/ meltfptr[1]),
					(melt_ptr_t) (( /*!CLASS_CITERATOR */
						       meltfrout->tabval[1])))
      )				/*then */
      {
	/*^cond.then */
	/*^getslot */
	{
	  melt_ptr_t slot = NULL, obj = NULL;
	  obj = (melt_ptr_t) ( /*_.RECV__V2*/ meltfptr[1]) /*=obj*/ ;
	  melt_object_get_field (slot, obj, 2, "CITER_START_FORMALS");
   /*_.CITSTAFORMALS__V22*/ meltfptr[21] = slot;
	};
	;
      }
    else
      {				/*^cond.else */

  /*_.CITSTAFORMALS__V22*/ meltfptr[21] = NULL;;
      }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:5185:/ cond");
    /*cond */ if (
		   /*ifisa */
		   melt_is_instance_of ((melt_ptr_t)
					( /*_.RECV__V2*/ meltfptr[1]),
					(melt_ptr_t) (( /*!CLASS_CITERATOR */
						       meltfrout->tabval[1])))
      )				/*then */
      {
	/*^cond.then */
	/*^getslot */
	{
	  melt_ptr_t slot = NULL, obj = NULL;
	  obj = (melt_ptr_t) ( /*_.RECV__V2*/ meltfptr[1]) /*=obj*/ ;
	  melt_object_get_field (slot, obj, 3, "CITER_STATE");
   /*_.CITSTATE__V23*/ meltfptr[22] = slot;
	};
	;
      }
    else
      {				/*^cond.else */

  /*_.CITSTATE__V23*/ meltfptr[22] = NULL;;
      }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:5186:/ cond");
    /*cond */ if (
		   /*ifisa */
		   melt_is_instance_of ((melt_ptr_t)
					( /*_.RECV__V2*/ meltfptr[1]),
					(melt_ptr_t) (( /*!CLASS_CITERATOR */
						       meltfrout->tabval[1])))
      )				/*then */
      {
	/*^cond.then */
	/*^getslot */
	{
	  melt_ptr_t slot = NULL, obj = NULL;
	  obj = (melt_ptr_t) ( /*_.RECV__V2*/ meltfptr[1]) /*=obj*/ ;
	  melt_object_get_field (slot, obj, 4, "CITER_BODY_FORMALS");
   /*_.CITBODFORMALS__V24*/ meltfptr[23] = slot;
	};
	;
      }
    else
      {				/*^cond.else */

  /*_.CITBODFORMALS__V24*/ meltfptr[23] = NULL;;
      }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:5187:/ cond");
    /*cond */ if (
		   /*ifisa */
		   melt_is_instance_of ((melt_ptr_t)
					( /*_.RECV__V2*/ meltfptr[1]),
					(melt_ptr_t) (( /*!CLASS_CITERATOR */
						       meltfrout->tabval[1])))
      )				/*then */
      {
	/*^cond.then */
	/*^getslot */
	{
	  melt_ptr_t slot = NULL, obj = NULL;
	  obj = (melt_ptr_t) ( /*_.RECV__V2*/ meltfptr[1]) /*=obj*/ ;
	  melt_object_get_field (slot, obj, 5, "CITER_EXPBEFORE");
   /*_.CITEXPBEFO__V25*/ meltfptr[24] = slot;
	};
	;
      }
    else
      {				/*^cond.else */

  /*_.CITEXPBEFO__V25*/ meltfptr[24] = NULL;;
      }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:5188:/ cond");
    /*cond */ if (
		   /*ifisa */
		   melt_is_instance_of ((melt_ptr_t)
					( /*_.RECV__V2*/ meltfptr[1]),
					(melt_ptr_t) (( /*!CLASS_CITERATOR */
						       meltfrout->tabval[1])))
      )				/*then */
      {
	/*^cond.then */
	/*^getslot */
	{
	  melt_ptr_t slot = NULL, obj = NULL;
	  obj = (melt_ptr_t) ( /*_.RECV__V2*/ meltfptr[1]) /*=obj*/ ;
	  melt_object_get_field (slot, obj, 6, "CITER_EXPAFTER");
   /*_.CITEXPAFTE__V26*/ meltfptr[25] = slot;
	};
	;
      }
    else
      {				/*^cond.else */

  /*_.CITEXPAFTE__V26*/ meltfptr[25] = NULL;;
      }
    ;
    /*^compute */
 /*_#MULTIPLE_LENGTH__L8*/ meltfnum[2] =
      (melt_multiple_length
       ((melt_ptr_t) ( /*_.CITSTAFORMALS__V22*/ meltfptr[21])));;
    /*^compute */
 /*_#I__L9*/ meltfnum[1] =
      ((3) + ( /*_#MULTIPLE_LENGTH__L8*/ meltfnum[2]));;
    /*^compute */
 /*_#I__L10*/ meltfnum[9] =
      ((2) * ( /*_#I__L9*/ meltfnum[1]));;
    /*^compute */
 /*_.REPLMAP__V27*/ meltfptr[26] =
      (meltgc_new_mapobjects
       ((meltobject_ptr_t) (( /*!DISCR_MAP_OBJECTS */ meltfrout->tabval[6])),
	( /*_#I__L10*/ meltfnum[9])));;
    MELT_LOCATION ("warmelt-outobj.melt:5190:/ quasiblock");


 /*_.NBUF__V29*/ meltfptr[28] =
      (melt_ptr_t)
      meltgc_new_strbuf ((meltobject_ptr_t)
			 (( /*!DISCR_STRBUF */ meltfrout->tabval[7])),
			 (const char *) 0);;
    MELT_LOCATION ("warmelt-outobj.melt:5192:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[3];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_cstring = "meltcitstate_";
      /*^apply.arg */
      argtab[1].meltbp_long = /*_#IX__L1*/ meltfnum[0];
      /*^apply.arg */
      argtab[2].meltbp_cstring = "_";
      /*_.ADD2OUT__V30*/ meltfptr[29] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!ADD2OUT */ meltfrout->tabval[8])),
		    (melt_ptr_t) ( /*_.NBUF__V29*/ meltfptr[28]),
		    (MELTBPARSTR_CSTRING MELTBPARSTR_LONG MELTBPARSTR_CSTRING
		     ""), argtab, "", (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:5193:/ cond");
    /*cond */ if (
		   /*ifisa */
		   melt_is_instance_of ((melt_ptr_t)
					( /*_.CITSTATE__V23*/ meltfptr[22]),
					(melt_ptr_t) (( /*!CLASS_NAMED */
						       meltfrout->tabval[5])))
      )				/*then */
      {
	/*^cond.then */
	/*^getslot */
	{
	  melt_ptr_t slot = NULL, obj = NULL;
	  obj = (melt_ptr_t) ( /*_.CITSTATE__V23*/ meltfptr[22]) /*=obj*/ ;
	  melt_object_get_field (slot, obj, 1, "NAMED_NAME");
   /*_.NAMED_NAME__V31*/ meltfptr[30] = slot;
	};
	;
      }
    else
      {				/*^cond.else */

  /*_.NAMED_NAME__V31*/ meltfptr[30] = NULL;;
      }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:5193:/ locexp");
      meltgc_add_strbuf_cident ((melt_ptr_t) ( /*_.NBUF__V29*/ meltfptr[28]),
				melt_string_str ((melt_ptr_t)
						 ( /*_.NAMED_NAME__V31*/
						  meltfptr[30])));
    }
    ;
 /*_.STRBUF2STRING__V32*/ meltfptr[31] =
      (meltgc_new_stringdup
       ((meltobject_ptr_t)
	(( /*!DISCR_VERBATIM_STRING */ meltfrout->tabval[9])),
	melt_strbuf_str ((melt_ptr_t) ( /*_.NBUF__V29*/ meltfptr[28]))));;
    /*^compute */
    /*_.LET___V28*/ meltfptr[27] = /*_.STRBUF2STRING__V32*/ meltfptr[31];;

    MELT_LOCATION ("warmelt-outobj.melt:5190:/ clear");
	   /*clear *//*_.NBUF__V29*/ meltfptr[28] = 0;
    /*^clear */
	   /*clear *//*_.ADD2OUT__V30*/ meltfptr[29] = 0;
    /*^clear */
	   /*clear *//*_.NAMED_NAME__V31*/ meltfptr[30] = 0;
    /*^clear */
	   /*clear *//*_.STRBUF2STRING__V32*/ meltfptr[31] = 0;
    /*_.REPSTATNAM__V33*/ meltfptr[28] = /*_.LET___V28*/ meltfptr[27];;
    MELT_LOCATION ("warmelt-outobj.melt:5195:/ cond");
    /*cond */ if (
		   /*ifisa */
		   melt_is_instance_of ((melt_ptr_t)
					( /*_.CITDEF__V19*/ meltfptr[18]),
					(melt_ptr_t) (( /*!CLASS_SOURCE_DEFCITERATOR */ meltfrout->tabval[10])))
      )				/*then */
      {
	/*^cond.then */
	/*^getslot */
	{
	  melt_ptr_t slot = NULL, obj = NULL;
	  obj = (melt_ptr_t) ( /*_.CITDEF__V19*/ meltfptr[18]) /*=obj*/ ;
	  melt_object_get_field (slot, obj, 6, "SCITERDEF_BEFORELOC");
   /*_.SCITERDEF_BEFORELOC__V34*/ meltfptr[29] = slot;
	};
	;
      }
    else
      {				/*^cond.else */

  /*_.SCITERDEF_BEFORELOC__V34*/ meltfptr[29] = NULL;;
      }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:5195:/ cond");
    /*cond */ if ( /*_.SCITERDEF_BEFORELOC__V34*/ meltfptr[29])	/*then */
      {
	/*^cond.then */
	/*_.BEFLOC__V35*/ meltfptr[30] =
	  /*_.SCITERDEF_BEFORELOC__V34*/ meltfptr[29];;
      }
    else
      {
	MELT_LOCATION ("warmelt-outobj.melt:5195:/ cond.else");

	/*_.BEFLOC__V35*/ meltfptr[30] = /*_.DLOC__V18*/ meltfptr[16];;
      }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:5196:/ cond");
    /*cond */ if (
		   /*ifisa */
		   melt_is_instance_of ((melt_ptr_t)
					( /*_.CITDEF__V19*/ meltfptr[18]),
					(melt_ptr_t) (( /*!CLASS_SOURCE_DEFCITERATOR */ meltfrout->tabval[10])))
      )				/*then */
      {
	/*^cond.then */
	/*^getslot */
	{
	  melt_ptr_t slot = NULL, obj = NULL;
	  obj = (melt_ptr_t) ( /*_.CITDEF__V19*/ meltfptr[18]) /*=obj*/ ;
	  melt_object_get_field (slot, obj, 7, "SCITERDEF_AFTERLOC");
   /*_.SCITERDEF_AFTERLOC__V36*/ meltfptr[31] = slot;
	};
	;
      }
    else
      {				/*^cond.else */

  /*_.SCITERDEF_AFTERLOC__V36*/ meltfptr[31] = NULL;;
      }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:5196:/ cond");
    /*cond */ if ( /*_.SCITERDEF_AFTERLOC__V36*/ meltfptr[31])	/*then */
      {
	/*^cond.then */
	/*_.AFTLOC__V37*/ meltfptr[36] =
	  /*_.SCITERDEF_AFTERLOC__V36*/ meltfptr[31];;
      }
    else
      {
	MELT_LOCATION ("warmelt-outobj.melt:5196:/ cond.else");

	/*_.AFTLOC__V37*/ meltfptr[36] = /*_.DLOC__V18*/ meltfptr[16];;
      }
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:5198:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L11*/ meltfnum[10] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.CITDEF__V19*/ meltfptr[18]),
			     (melt_ptr_t) (( /*!CLASS_SOURCE_DEFCITERATOR */
					    meltfrout->tabval[10])));;
      MELT_LOCATION ("warmelt-outobj.melt:5198:/ cond");
      /*cond */ if ( /*_#IS_A__L11*/ meltfnum[10])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V39*/ meltfptr[38] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-outobj.melt:5198:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check citdef"),
				  ("warmelt-outobj.melt")
				  ? ("warmelt-outobj.melt") : __FILE__,
				  (5198) ? (5198) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V39*/ meltfptr[38] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V38*/ meltfptr[37] = /*_.IFELSE___V39*/ meltfptr[38];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:5198:/ clear");
	     /*clear *//*_#IS_A__L11*/ meltfnum[10] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V39*/ meltfptr[38] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V38*/ meltfptr[37] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:5199:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#__L12*/ meltfnum[10] =
	(( /*_.CITREP__V20*/ meltfptr[19]) == ( /*_.RECV__V2*/ meltfptr[1]));;
      MELT_LOCATION ("warmelt-outobj.melt:5199:/ cond");
      /*cond */ if ( /*_#__L12*/ meltfnum[10])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V41*/ meltfptr[40] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-outobj.melt:5199:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check citrep"),
				  ("warmelt-outobj.melt")
				  ? ("warmelt-outobj.melt") : __FILE__,
				  (5199) ? (5199) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V41*/ meltfptr[40] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V40*/ meltfptr[38] = /*_.IFELSE___V41*/ meltfptr[40];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:5199:/ clear");
	     /*clear *//*_#__L12*/ meltfnum[10] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V41*/ meltfptr[40] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V40*/ meltfptr[38] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-outobj.melt:5200:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_cstring = "/*citerator-syntax ";
      /*_.ADD2OUT__V42*/ meltfptr[40] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!ADD2OUT */ meltfrout->tabval[8])),
		    (melt_ptr_t) ( /*_.SBUF__V4*/ meltfptr[3]),
		    (MELTBPARSTR_CSTRING ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:5201:/ locexp");
      meltgc_add_strbuf_ccomment ((melt_ptr_t) ( /*_.SBUF__V4*/ meltfptr[3]),
				  melt_string_str ((melt_ptr_t)
						   ( /*_.CITNAM__V21*/
						    meltfptr[20])));
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:5202:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_cstring = "*/ {";
      /*_.ADD2OUT__V43*/ meltfptr[42] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!ADD2OUT */ meltfrout->tabval[8])),
		    (melt_ptr_t) ( /*_.SBUF__V4*/ meltfptr[3]),
		    (MELTBPARSTR_CSTRING ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:5203:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.SBUF__V4*/ meltfptr[3]),
				(1), 0);
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:5204:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^cond */
    /*cond */ if ( /*_.DLOC__V18*/ meltfptr[16])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  /*^checksignal */
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^apply */
	  /*apply */
	  {
	    union meltparam_un argtab[3];
	    memset (&argtab, 0, sizeof (argtab));
	    /*^apply.arg */
	    argtab[0].meltbp_aptr =
	      (melt_ptr_t *) & /*_.SBUF__V4*/ meltfptr[3];
	    /*^apply.arg */
	    argtab[1].meltbp_long = 0;
	    /*^apply.arg */
	    argtab[2].meltbp_cstring = "citerator-syntax";
	    /*_.OUTPUT_RAW_LOCATION__V45*/ meltfptr[44] =
	      melt_apply ((meltclosure_ptr_t)
			  (( /*!OUTPUT_RAW_LOCATION */ meltfrout->
			    tabval[11])),
			  (melt_ptr_t) ( /*_.DLOC__V18*/ meltfptr[16]),
			  (MELTBPARSTR_PTR MELTBPARSTR_LONG
			   MELTBPARSTR_CSTRING ""), argtab, "",
			  (union meltparam_un *) 0);
	  }
	  ;
	  /*_.IF___V44*/ meltfptr[43] =
	    /*_.OUTPUT_RAW_LOCATION__V45*/ meltfptr[44];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-outobj.melt:5204:/ clear");
	     /*clear *//*_.OUTPUT_RAW_LOCATION__V45*/ meltfptr[44] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

  /*_.IF___V44*/ meltfptr[43] = NULL;;
      }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:5205:/ locexp");
      meltgc_put_mapobjects ((meltmapobjects_ptr_t)
			     ( /*_.REPLMAP__V27*/ meltfptr[26]),
			     (meltobject_ptr_t) ( /*_.CITSTATE__V23*/
						 meltfptr[22]),
			     (melt_ptr_t) ( /*_.REPSTATNAM__V33*/
					   meltfptr[28]));
    }
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:5206:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L13*/ meltfnum[10] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-outobj.melt:5206:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L13*/ meltfnum[10])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L14*/ meltfnum[13] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-outobj.melt:5206:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long =
		/*_#THE_MELTCALLCOUNT__L14*/ meltfnum[13];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-outobj.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 5206;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "syntestgen_citerator replmap=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.REPLMAP__V27*/ meltfptr[26];
	      /*_.MELT_DEBUG_FUN__V48*/ meltfptr[47] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V47*/ meltfptr[46] =
	      /*_.MELT_DEBUG_FUN__V48*/ meltfptr[47];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-outobj.melt:5206:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L14*/ meltfnum[13] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V48*/ meltfptr[47] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V47*/ meltfptr[46] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-outobj.melt:5206:/ quasiblock");


      /*_.PROGN___V49*/ meltfptr[47] = /*_.IF___V47*/ meltfptr[46];;
      /*^compute */
      /*_.IFCPP___V46*/ meltfptr[44] = /*_.PROGN___V49*/ meltfptr[47];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:5206:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L13*/ meltfnum[10] = 0;
      /*^clear */
	     /*clear *//*_.IF___V47*/ meltfptr[46] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V49*/ meltfptr[47] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V46*/ meltfptr[44] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-outobj.melt:5207:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[3];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & /*_.REPLMAP__V27*/ meltfptr[26];
      /*^apply.arg */
      argtab[1].meltbp_aptr =
	(melt_ptr_t *) & /*_.CITSTAFORMALS__V22*/ meltfptr[21];
      /*^apply.arg */
      argtab[2].meltbp_cstring = "citstart_";
      /*_.SUBSTITUTE_FORMALS_FOR_SYNTEST__V50*/ meltfptr[46] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!SUBSTITUTE_FORMALS_FOR_SYNTEST */ meltfrout->
		      tabval[12])),
		    (melt_ptr_t) ( /*_.SBUF__V4*/ meltfptr[3]),
		    (MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_CSTRING ""),
		    argtab, "", (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:5208:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[3];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & /*_.REPLMAP__V27*/ meltfptr[26];
      /*^apply.arg */
      argtab[1].meltbp_aptr =
	(melt_ptr_t *) & /*_.CITBODFORMALS__V24*/ meltfptr[23];
      /*^apply.arg */
      argtab[2].meltbp_cstring = "citbody_";
      /*_.SUBSTITUTE_FORMALS_FOR_SYNTEST__V51*/ meltfptr[47] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!SUBSTITUTE_FORMALS_FOR_SYNTEST */ meltfrout->
		      tabval[12])),
		    (melt_ptr_t) ( /*_.SBUF__V4*/ meltfptr[3]),
		    (MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_CSTRING ""),
		    argtab, "", (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:5209:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^cond */
    /*cond */ if ( /*_.BEFLOC__V35*/ meltfptr[30])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  /*^checksignal */
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^apply */
	  /*apply */
	  {
	    union meltparam_un argtab[3];
	    memset (&argtab, 0, sizeof (argtab));
	    /*^apply.arg */
	    argtab[0].meltbp_aptr =
	      (melt_ptr_t *) & /*_.SBUF__V4*/ meltfptr[3];
	    /*^apply.arg */
	    argtab[1].meltbp_long = 0;
	    /*^apply.arg */
	    argtab[2].meltbp_cstring = "citerator-syntax-before";
	    /*_.OUTPUT_RAW_LOCATION__V53*/ meltfptr[52] =
	      melt_apply ((meltclosure_ptr_t)
			  (( /*!OUTPUT_RAW_LOCATION */ meltfrout->
			    tabval[11])),
			  (melt_ptr_t) ( /*_.BEFLOC__V35*/ meltfptr[30]),
			  (MELTBPARSTR_PTR MELTBPARSTR_LONG
			   MELTBPARSTR_CSTRING ""), argtab, "",
			  (union meltparam_un *) 0);
	  }
	  ;
	  /*_.IF___V52*/ meltfptr[51] =
	    /*_.OUTPUT_RAW_LOCATION__V53*/ meltfptr[52];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-outobj.melt:5209:/ clear");
	     /*clear *//*_.OUTPUT_RAW_LOCATION__V53*/ meltfptr[52] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

  /*_.IF___V52*/ meltfptr[51] = NULL;;
      }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:5210:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[3];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & /*_.REPLMAP__V27*/ meltfptr[26];
      /*^apply.arg */
      argtab[1].meltbp_aptr =
	(melt_ptr_t *) & /*_.CITEXPBEFO__V25*/ meltfptr[24];
      /*^apply.arg */
      argtab[2].meltbp_aptr = (melt_ptr_t *) & /*_.BEFLOC__V35*/ meltfptr[30];
      /*_.EXPAND_TUPLE_FOR_SYNTEST__V54*/ meltfptr[52] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!EXPAND_TUPLE_FOR_SYNTEST */ meltfrout->tabval[13])),
		    (melt_ptr_t) ( /*_.SBUF__V4*/ meltfptr[3]),
		    (MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_PTR ""),
		    argtab, "", (union meltparam_un *) 0);
    }
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:5211:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L15*/ meltfnum[13] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-outobj.melt:5211:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L15*/ meltfnum[13])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L16*/ meltfnum[10] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-outobj.melt:5211:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long =
		/*_#THE_MELTCALLCOUNT__L16*/ meltfnum[10];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-outobj.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 5211;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring =
		"syntestgen_citerator after start formals replmap=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.REPLMAP__V27*/ meltfptr[26];
	      /*_.MELT_DEBUG_FUN__V57*/ meltfptr[56] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V56*/ meltfptr[55] =
	      /*_.MELT_DEBUG_FUN__V57*/ meltfptr[56];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-outobj.melt:5211:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L16*/ meltfnum[10] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V57*/ meltfptr[56] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V56*/ meltfptr[55] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-outobj.melt:5211:/ quasiblock");


      /*_.PROGN___V58*/ meltfptr[56] = /*_.IF___V56*/ meltfptr[55];;
      /*^compute */
      /*_.IFCPP___V55*/ meltfptr[54] = /*_.PROGN___V58*/ meltfptr[56];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:5211:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L15*/ meltfnum[13] = 0;
      /*^clear */
	     /*clear *//*_.IF___V56*/ meltfptr[55] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V58*/ meltfptr[56] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V55*/ meltfptr[54] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:5212:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.SBUF__V4*/ meltfptr[3]),
				(0), 0);
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:5213:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[3];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_cstring = "/**fictive body of citerator ";
      /*^apply.arg */
      argtab[1].meltbp_aptr =
	(melt_ptr_t *) & /*_.REPSTATNAM__V33*/ meltfptr[28];
      /*^apply.arg */
      argtab[2].meltbp_cstring = " **/";
      /*_.ADD2OUT__V59*/ meltfptr[55] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!ADD2OUT */ meltfrout->tabval[8])),
		    (melt_ptr_t) ( /*_.SBUF__V4*/ meltfptr[3]),
		    (MELTBPARSTR_CSTRING MELTBPARSTR_PTR MELTBPARSTR_CSTRING
		     ""), argtab, "", (union meltparam_un *) 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:5214:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.SBUF__V4*/ meltfptr[3]),
				(0), 0);
    }
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:5215:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L17*/ meltfnum[10] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-outobj.melt:5215:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L17*/ meltfnum[10])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L18*/ meltfnum[13] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-outobj.melt:5215:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long =
		/*_#THE_MELTCALLCOUNT__L18*/ meltfnum[13];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-outobj.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 5215;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring =
		"syntestgen_citerator citbodformals=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.CITBODFORMALS__V24*/ meltfptr[23];
	      /*_.MELT_DEBUG_FUN__V62*/ meltfptr[61] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V61*/ meltfptr[60] =
	      /*_.MELT_DEBUG_FUN__V62*/ meltfptr[61];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-outobj.melt:5215:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L18*/ meltfnum[13] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V62*/ meltfptr[61] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V61*/ meltfptr[60] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-outobj.melt:5215:/ quasiblock");


      /*_.PROGN___V63*/ meltfptr[61] = /*_.IF___V61*/ meltfptr[60];;
      /*^compute */
      /*_.IFCPP___V60*/ meltfptr[56] = /*_.PROGN___V63*/ meltfptr[61];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:5215:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L17*/ meltfnum[10] = 0;
      /*^clear */
	     /*clear *//*_.IF___V61*/ meltfptr[60] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V63*/ meltfptr[61] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V60*/ meltfptr[56] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    /*citerblock FOREACH_IN_MULTIPLE */
    {
      /* start foreach_in_multiple meltcit1__EACHTUP */
      long meltcit1__EACHTUP_ln =
	melt_multiple_length ((melt_ptr_t) /*_.CITBODFORMALS__V24*/
			      meltfptr[23]);
      for ( /*_#BODFORMIX__L19*/ meltfnum[13] = 0;
	   ( /*_#BODFORMIX__L19*/ meltfnum[13] >= 0)
	   && ( /*_#BODFORMIX__L19*/ meltfnum[13] < meltcit1__EACHTUP_ln);
	/*_#BODFORMIX__L19*/ meltfnum[13]++)
	{
	  /*_.CURBODFORMBIND__V64*/ meltfptr[60] =
	    melt_multiple_nth ((melt_ptr_t)
			       ( /*_.CITBODFORMALS__V24*/ meltfptr[23]),
			       /*_#BODFORMIX__L19*/ meltfnum[13]);




#if MELT_HAVE_DEBUG
	  MELT_LOCATION ("warmelt-outobj.melt:5219:/ cppif.then");
	  /*^block */
	  /*anyblock */
	  {


	    {
	      /*^locexp */
	      /*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	      melt_dbgcounter++;
#endif
	      ;
	    }
	    ;
	    /*^checksignal */
	    MELT_CHECK_SIGNAL ();
	    ;
    /*_#MELT_NEED_DBG__L20*/ meltfnum[10] =
	      /*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	      ( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	      0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	      ;;
	    MELT_LOCATION ("warmelt-outobj.melt:5219:/ cond");
	    /*cond */ if ( /*_#MELT_NEED_DBG__L20*/ meltfnum[10])	/*then */
	      {
		/*^cond.then */
		/*^block */
		/*anyblock */
		{

      /*_#THE_MELTCALLCOUNT__L21*/ meltfnum[20] =
#ifdef meltcallcount
		    meltcallcount	/* the_meltcallcount */
#else
		    0L
#endif /* meltcallcount the_meltcallcount */
		    ;;
		  MELT_LOCATION ("warmelt-outobj.melt:5219:/ checksignal");
		  MELT_CHECK_SIGNAL ();
		  ;
		  /*^apply */
		  /*apply */
		  {
		    union meltparam_un argtab[5];
		    memset (&argtab, 0, sizeof (argtab));
		    /*^apply.arg */
		    argtab[0].meltbp_long =
		      /*_#THE_MELTCALLCOUNT__L21*/ meltfnum[20];
		    /*^apply.arg */
		    argtab[1].meltbp_cstring = "warmelt-outobj.melt";
		    /*^apply.arg */
		    argtab[2].meltbp_long = 5219;
		    /*^apply.arg */
		    argtab[3].meltbp_cstring =
		      "syntestgen_citerator curbodformbind=";
		    /*^apply.arg */
		    argtab[4].meltbp_aptr =
		      (melt_ptr_t *) & /*_.CURBODFORMBIND__V64*/ meltfptr[60];
		    /*_.MELT_DEBUG_FUN__V67*/ meltfptr[66] =
		      melt_apply ((meltclosure_ptr_t)
				  (( /*!MELT_DEBUG_FUN */ meltfrout->
				    tabval[0])),
				  (melt_ptr_t) (( /*nil */ NULL)),
				  (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_PTR ""), argtab, "",
				  (union meltparam_un *) 0);
		  }
		  ;
		  /*_.IF___V66*/ meltfptr[65] =
		    /*_.MELT_DEBUG_FUN__V67*/ meltfptr[66];;
		  /*epilog */

		  MELT_LOCATION ("warmelt-outobj.melt:5219:/ clear");
		/*clear *//*_#THE_MELTCALLCOUNT__L21*/ meltfnum[20] = 0;
		  /*^clear */
		/*clear *//*_.MELT_DEBUG_FUN__V67*/ meltfptr[66] = 0;
		}
		;
	      }
	    else
	      {			/*^cond.else */

     /*_.IF___V66*/ meltfptr[65] = NULL;;
	      }
	    ;
	    MELT_LOCATION ("warmelt-outobj.melt:5219:/ quasiblock");


	    /*_.PROGN___V68*/ meltfptr[66] = /*_.IF___V66*/ meltfptr[65];;
	    /*^compute */
	    /*_.IFCPP___V65*/ meltfptr[61] = /*_.PROGN___V68*/ meltfptr[66];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-outobj.melt:5219:/ clear");
	      /*clear *//*_#MELT_NEED_DBG__L20*/ meltfnum[10] = 0;
	    /*^clear */
	      /*clear *//*_.IF___V66*/ meltfptr[65] = 0;
	    /*^clear */
	      /*clear *//*_.PROGN___V68*/ meltfptr[66] = 0;
	  }

#else /*MELT_HAVE_DEBUG */
	  /*^cppif.else */
	  /*_.IFCPP___V65*/ meltfptr[61] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
	  ;
	  MELT_LOCATION ("warmelt-outobj.melt:5220:/ quasiblock");


	  MELT_LOCATION ("warmelt-outobj.melt:5221:/ cond");
	  /*cond */ if (
			 /*ifisa */
			 melt_is_instance_of ((melt_ptr_t)
					      ( /*_.CURBODFORMBIND__V64*/
					       meltfptr[60]),
					      (melt_ptr_t) (( /*!CLASS_ANY_BINDING */ meltfrout->tabval[14])))
	    )			/*then */
	    {
	      /*^cond.then */
	      /*^getslot */
	      {
		melt_ptr_t slot = NULL, obj = NULL;
		obj =
		  (melt_ptr_t) ( /*_.CURBODFORMBIND__V64*/ meltfptr[60])
		  /*=obj*/ ;
		melt_object_get_field (slot, obj, 0, "BINDER");
    /*_.CURBODFORMAL__V69*/ meltfptr[65] = slot;
	      };
	      ;
	    }
	  else
	    {			/*^cond.else */

   /*_.CURBODFORMAL__V69*/ meltfptr[65] = NULL;;
	    }
	  ;
	  /*^compute */
  /*_.NAMBODFORMAL__V70*/ meltfptr[66] =
	    /*mapobject_get */
	    melt_get_mapobjects ((meltmapobjects_ptr_t)
				 ( /*_.REPLMAP__V27*/ meltfptr[26]),
				 (meltobject_ptr_t) ( /*_.CURBODFORMAL__V69*/
						     meltfptr[65]));;

#if MELT_HAVE_DEBUG
	  MELT_LOCATION ("warmelt-outobj.melt:5224:/ cppif.then");
	  /*^block */
	  /*anyblock */
	  {


	    {
	      /*^locexp */
	      /*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	      melt_dbgcounter++;
#endif
	      ;
	    }
	    ;
	    /*^checksignal */
	    MELT_CHECK_SIGNAL ();
	    ;
    /*_#MELT_NEED_DBG__L22*/ meltfnum[20] =
	      /*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	      ( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	      0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	      ;;
	    MELT_LOCATION ("warmelt-outobj.melt:5224:/ cond");
	    /*cond */ if ( /*_#MELT_NEED_DBG__L22*/ meltfnum[20])	/*then */
	      {
		/*^cond.then */
		/*^block */
		/*anyblock */
		{

      /*_#THE_MELTCALLCOUNT__L23*/ meltfnum[10] =
#ifdef meltcallcount
		    meltcallcount	/* the_meltcallcount */
#else
		    0L
#endif /* meltcallcount the_meltcallcount */
		    ;;
		  MELT_LOCATION ("warmelt-outobj.melt:5224:/ checksignal");
		  MELT_CHECK_SIGNAL ();
		  ;
		  /*^apply */
		  /*apply */
		  {
		    union meltparam_un argtab[5];
		    memset (&argtab, 0, sizeof (argtab));
		    /*^apply.arg */
		    argtab[0].meltbp_long =
		      /*_#THE_MELTCALLCOUNT__L23*/ meltfnum[10];
		    /*^apply.arg */
		    argtab[1].meltbp_cstring = "warmelt-outobj.melt";
		    /*^apply.arg */
		    argtab[2].meltbp_long = 5224;
		    /*^apply.arg */
		    argtab[3].meltbp_cstring =
		      "syntestgen_citerator nambodformal=";
		    /*^apply.arg */
		    argtab[4].meltbp_aptr =
		      (melt_ptr_t *) & /*_.NAMBODFORMAL__V70*/ meltfptr[66];
		    /*_.MELT_DEBUG_FUN__V73*/ meltfptr[72] =
		      melt_apply ((meltclosure_ptr_t)
				  (( /*!MELT_DEBUG_FUN */ meltfrout->
				    tabval[0])),
				  (melt_ptr_t) (( /*nil */ NULL)),
				  (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_PTR ""), argtab, "",
				  (union meltparam_un *) 0);
		  }
		  ;
		  /*_.IF___V72*/ meltfptr[71] =
		    /*_.MELT_DEBUG_FUN__V73*/ meltfptr[72];;
		  /*epilog */

		  MELT_LOCATION ("warmelt-outobj.melt:5224:/ clear");
		/*clear *//*_#THE_MELTCALLCOUNT__L23*/ meltfnum[10] = 0;
		  /*^clear */
		/*clear *//*_.MELT_DEBUG_FUN__V73*/ meltfptr[72] = 0;
		}
		;
	      }
	    else
	      {			/*^cond.else */

     /*_.IF___V72*/ meltfptr[71] = NULL;;
	      }
	    ;
	    MELT_LOCATION ("warmelt-outobj.melt:5224:/ quasiblock");


	    /*_.PROGN___V74*/ meltfptr[72] = /*_.IF___V72*/ meltfptr[71];;
	    /*^compute */
	    /*_.IFCPP___V71*/ meltfptr[70] = /*_.PROGN___V74*/ meltfptr[72];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-outobj.melt:5224:/ clear");
	      /*clear *//*_#MELT_NEED_DBG__L22*/ meltfnum[20] = 0;
	    /*^clear */
	      /*clear *//*_.IF___V72*/ meltfptr[71] = 0;
	    /*^clear */
	      /*clear *//*_.PROGN___V74*/ meltfptr[72] = 0;
	  }

#else /*MELT_HAVE_DEBUG */
	  /*^cppif.else */
	  /*_.IFCPP___V71*/ meltfptr[70] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
	  ;

#if MELT_HAVE_DEBUG
	  MELT_LOCATION ("warmelt-outobj.melt:5225:/ cppif.then");
	  /*^block */
	  /*anyblock */
	  {

	    /*^checksignal */
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^cond */
	    /*cond */ if ( /*_.NAMBODFORMAL__V70*/ meltfptr[66])	/*then */
	      {
		/*^cond.then */
		/*_.IFELSE___V76*/ meltfptr[72] = ( /*nil */ NULL);;
	      }
	    else
	      {
		MELT_LOCATION ("warmelt-outobj.melt:5225:/ cond.else");

		/*^block */
		/*anyblock */
		{




		  {
		    /*^locexp */
		    melt_assert_failed (("check nambodformal"),
					("warmelt-outobj.melt")
					? ("warmelt-outobj.melt") : __FILE__,
					(5225) ? (5225) : __LINE__,
					__FUNCTION__);
		    ;
		  }
		  ;
		/*clear *//*_.IFELSE___V76*/ meltfptr[72] = 0;
		  /*epilog */
		}
		;
	      }
	    ;
	    /*^compute */
	    /*_.IFCPP___V75*/ meltfptr[71] = /*_.IFELSE___V76*/ meltfptr[72];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-outobj.melt:5225:/ clear");
	      /*clear *//*_.IFELSE___V76*/ meltfptr[72] = 0;
	  }

#else /*MELT_HAVE_DEBUG */
	  /*^cppif.else */
	  /*_.IFCPP___V75*/ meltfptr[71] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
	  ;
	  MELT_LOCATION ("warmelt-outobj.melt:5226:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^apply */
	  /*apply */
	  {
	    union meltparam_un argtab[3];
	    memset (&argtab, 0, sizeof (argtab));
	    /*^apply.arg */
	    argtab[0].meltbp_cstring = "if (";
	    /*^apply.arg */
	    argtab[1].meltbp_aptr =
	      (melt_ptr_t *) & /*_.NAMBODFORMAL__V70*/ meltfptr[66];
	    /*^apply.arg */
	    argtab[2].meltbp_cstring = ") return;";
	    /*_.ADD2OUT__V77*/ meltfptr[72] =
	      melt_apply ((meltclosure_ptr_t)
			  (( /*!ADD2OUT */ meltfrout->tabval[8])),
			  (melt_ptr_t) ( /*_.SBUF__V4*/ meltfptr[3]),
			  (MELTBPARSTR_CSTRING MELTBPARSTR_PTR
			   MELTBPARSTR_CSTRING ""), argtab, "",
			  (union meltparam_un *) 0);
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:5227:/ locexp");
	    meltgc_strbuf_add_indent ((melt_ptr_t)
				      ( /*_.SBUF__V4*/ meltfptr[3]), (0), 0);
	  }
	  ;

	  MELT_LOCATION ("warmelt-outobj.melt:5220:/ clear");
	    /*clear *//*_.CURBODFORMAL__V69*/ meltfptr[65] = 0;
	  /*^clear */
	    /*clear *//*_.NAMBODFORMAL__V70*/ meltfptr[66] = 0;
	  /*^clear */
	    /*clear *//*_.IFCPP___V71*/ meltfptr[70] = 0;
	  /*^clear */
	    /*clear *//*_.IFCPP___V75*/ meltfptr[71] = 0;
	  /*^clear */
	    /*clear *//*_.ADD2OUT__V77*/ meltfptr[72] = 0;
	  if ( /*_#BODFORMIX__L19*/ meltfnum[13] < 0)
	    break;
	}			/* end  foreach_in_multiple meltcit1__EACHTUP */

      /*citerepilog */

      MELT_LOCATION ("warmelt-outobj.melt:5216:/ clear");
	    /*clear *//*_.CURBODFORMBIND__V64*/ meltfptr[60] = 0;
      /*^clear */
	    /*clear *//*_#BODFORMIX__L19*/ meltfnum[13] = 0;
      /*^clear */
	    /*clear *//*_.IFCPP___V65*/ meltfptr[61] = 0;
    }				/*endciterblock FOREACH_IN_MULTIPLE */
    ;
    MELT_LOCATION ("warmelt-outobj.melt:5229:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^cond */
    /*cond */ if ( /*_.AFTLOC__V37*/ meltfptr[36])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  /*^checksignal */
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^apply */
	  /*apply */
	  {
	    union meltparam_un argtab[3];
	    memset (&argtab, 0, sizeof (argtab));
	    /*^apply.arg */
	    argtab[0].meltbp_aptr =
	      (melt_ptr_t *) & /*_.SBUF__V4*/ meltfptr[3];
	    /*^apply.arg */
	    argtab[1].meltbp_long = 0;
	    /*^apply.arg */
	    argtab[2].meltbp_cstring = "citerator-syntax-after";
	    /*_.OUTPUT_RAW_LOCATION__V79*/ meltfptr[66] =
	      melt_apply ((meltclosure_ptr_t)
			  (( /*!OUTPUT_RAW_LOCATION */ meltfrout->
			    tabval[11])),
			  (melt_ptr_t) ( /*_.AFTLOC__V37*/ meltfptr[36]),
			  (MELTBPARSTR_PTR MELTBPARSTR_LONG
			   MELTBPARSTR_CSTRING ""), argtab, "",
			  (union meltparam_un *) 0);
	  }
	  ;
	  /*_.IF___V78*/ meltfptr[65] =
	    /*_.OUTPUT_RAW_LOCATION__V79*/ meltfptr[66];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-outobj.melt:5229:/ clear");
	     /*clear *//*_.OUTPUT_RAW_LOCATION__V79*/ meltfptr[66] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

  /*_.IF___V78*/ meltfptr[65] = NULL;;
      }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:5230:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[3];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & /*_.REPLMAP__V27*/ meltfptr[26];
      /*^apply.arg */
      argtab[1].meltbp_aptr =
	(melt_ptr_t *) & /*_.CITEXPAFTE__V26*/ meltfptr[25];
      /*^apply.arg */
      argtab[2].meltbp_aptr = (melt_ptr_t *) & /*_.AFTLOC__V37*/ meltfptr[36];
      /*_.EXPAND_TUPLE_FOR_SYNTEST__V80*/ meltfptr[70] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!EXPAND_TUPLE_FOR_SYNTEST */ meltfrout->tabval[13])),
		    (melt_ptr_t) ( /*_.SBUF__V4*/ meltfptr[3]),
		    (MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_PTR ""),
		    argtab, "", (union meltparam_un *) 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:5231:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.SBUF__V4*/ meltfptr[3]),
				(0), 0);
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:5232:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_cstring = "} /*end citerator-syntax ";
      /*_.ADD2OUT__V81*/ meltfptr[71] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!ADD2OUT */ meltfrout->tabval[8])),
		    (melt_ptr_t) ( /*_.SBUF__V4*/ meltfptr[3]),
		    (MELTBPARSTR_CSTRING ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:5233:/ locexp");
      meltgc_add_strbuf_ccomment ((melt_ptr_t) ( /*_.SBUF__V4*/ meltfptr[3]),
				  melt_string_str ((melt_ptr_t)
						   ( /*_.CITNAM__V21*/
						    meltfptr[20])));
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:5234:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_cstring = "*/";
      /*_.ADD2OUT__V82*/ meltfptr[72] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!ADD2OUT */ meltfrout->tabval[8])),
		    (melt_ptr_t) ( /*_.SBUF__V4*/ meltfptr[3]),
		    (MELTBPARSTR_CSTRING ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:5235:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.SBUF__V4*/ meltfptr[3]),
				(0), 0);
    }
    ;

    MELT_LOCATION ("warmelt-outobj.melt:5180:/ clear");
	   /*clear *//*_.DLOC__V18*/ meltfptr[16] = 0;
    /*^clear */
	   /*clear *//*_.CITDEF__V19*/ meltfptr[18] = 0;
    /*^clear */
	   /*clear *//*_.CITREP__V20*/ meltfptr[19] = 0;
    /*^clear */
	   /*clear *//*_.CITNAM__V21*/ meltfptr[20] = 0;
    /*^clear */
	   /*clear *//*_.CITSTAFORMALS__V22*/ meltfptr[21] = 0;
    /*^clear */
	   /*clear *//*_.CITSTATE__V23*/ meltfptr[22] = 0;
    /*^clear */
	   /*clear *//*_.CITBODFORMALS__V24*/ meltfptr[23] = 0;
    /*^clear */
	   /*clear *//*_.CITEXPBEFO__V25*/ meltfptr[24] = 0;
    /*^clear */
	   /*clear *//*_.CITEXPAFTE__V26*/ meltfptr[25] = 0;
    /*^clear */
	   /*clear *//*_#MULTIPLE_LENGTH__L8*/ meltfnum[2] = 0;
    /*^clear */
	   /*clear *//*_#I__L9*/ meltfnum[1] = 0;
    /*^clear */
	   /*clear *//*_#I__L10*/ meltfnum[9] = 0;
    /*^clear */
	   /*clear *//*_.REPLMAP__V27*/ meltfptr[26] = 0;
    /*^clear */
	   /*clear *//*_.LET___V28*/ meltfptr[27] = 0;
    /*^clear */
	   /*clear *//*_.REPSTATNAM__V33*/ meltfptr[28] = 0;
    /*^clear */
	   /*clear *//*_.SCITERDEF_BEFORELOC__V34*/ meltfptr[29] = 0;
    /*^clear */
	   /*clear *//*_.BEFLOC__V35*/ meltfptr[30] = 0;
    /*^clear */
	   /*clear *//*_.SCITERDEF_AFTERLOC__V36*/ meltfptr[31] = 0;
    /*^clear */
	   /*clear *//*_.AFTLOC__V37*/ meltfptr[36] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V38*/ meltfptr[37] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V40*/ meltfptr[38] = 0;
    /*^clear */
	   /*clear *//*_.ADD2OUT__V42*/ meltfptr[40] = 0;
    /*^clear */
	   /*clear *//*_.ADD2OUT__V43*/ meltfptr[42] = 0;
    /*^clear */
	   /*clear *//*_.IF___V44*/ meltfptr[43] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V46*/ meltfptr[44] = 0;
    /*^clear */
	   /*clear *//*_.SUBSTITUTE_FORMALS_FOR_SYNTEST__V50*/ meltfptr[46] = 0;
    /*^clear */
	   /*clear *//*_.SUBSTITUTE_FORMALS_FOR_SYNTEST__V51*/ meltfptr[47] = 0;
    /*^clear */
	   /*clear *//*_.IF___V52*/ meltfptr[51] = 0;
    /*^clear */
	   /*clear *//*_.EXPAND_TUPLE_FOR_SYNTEST__V54*/ meltfptr[52] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V55*/ meltfptr[54] = 0;
    /*^clear */
	   /*clear *//*_.ADD2OUT__V59*/ meltfptr[55] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V60*/ meltfptr[56] = 0;
    /*^clear */
	   /*clear *//*_.IF___V78*/ meltfptr[65] = 0;
    /*^clear */
	   /*clear *//*_.EXPAND_TUPLE_FOR_SYNTEST__V80*/ meltfptr[70] = 0;
    /*^clear */
	   /*clear *//*_.ADD2OUT__V81*/ meltfptr[71] = 0;
    /*^clear */
	   /*clear *//*_.ADD2OUT__V82*/ meltfptr[72] = 0;
    MELT_LOCATION ("warmelt-outobj.melt:5174:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V6*/ meltfptr[5] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V10*/ meltfptr[6] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V12*/ meltfptr[7] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V14*/ meltfptr[12] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V16*/ meltfptr[14] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("SYNTESTGEN_CITERATOR", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_128_warmelt_outobj_SYNTESTGEN_CITERATOR_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*noretval */ NULL);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_128_warmelt_outobj_SYNTESTGEN_CITERATOR */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_129_warmelt_outobj_SYNTESTGEN_CMATCHER (meltclosure_ptr_t meltclosp_,
						 melt_ptr_t meltfirstargp_,
						 const melt_argdescr_cell_t
						 meltxargdescr_[],
						 union meltparam_un *
						 meltxargtab_,
						 const melt_argdescr_cell_t
						 meltxresdescr_[],
						 union meltparam_un *
						 meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_129_warmelt_outobj_SYNTESTGEN_CMATCHER_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_129_warmelt_outobj_SYNTESTGEN_CMATCHER_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 83
    melt_ptr_t mcfr_varptr[83];
#define MELTFRAM_NBVARNUM 17
    long mcfr_varnum[17];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_129_warmelt_outobj_SYNTESTGEN_CMATCHER is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_129_warmelt_outobj_SYNTESTGEN_CMATCHER_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 83; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_129_warmelt_outobj_SYNTESTGEN_CMATCHER nbval 83*/
  meltfram__.mcfr_nbvar = 83 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("SYNTESTGEN_CMATCHER", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-outobj.melt:5242:/ getarg");
 /*_.RECV__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.GENDEV__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.GENDEV__V3*/ meltfptr[2])) !=
	      NULL);


  /*getarg#2 */
  /*^getarg */
  if (meltxargdescr_[1] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.SBUF__V4*/ meltfptr[3] =
    (meltxargtab_[1].meltbp_aptr) ? (*(meltxargtab_[1].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.SBUF__V4*/ meltfptr[3])) !=
	      NULL);


  /*getarg#3 */
  /*^getarg */
  if (meltxargdescr_[2] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.MODCTX__V5*/ meltfptr[4] =
    (meltxargtab_[2].meltbp_aptr) ? (*(meltxargtab_[2].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.MODCTX__V5*/ meltfptr[4])) !=
	      NULL);


  /*getarg#4 */
  /*^getarg */
  if (meltxargdescr_[3] != MELTBPAR_LONG)
    goto lab_endgetargs;
 /*_#IX__L1*/ meltfnum[0] = meltxargtab_[3].meltbp_long;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:5243:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L2*/ meltfnum[1] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-outobj.melt:5243:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L2*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L3*/ meltfnum[2] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-outobj.melt:5243:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[7];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L3*/ meltfnum[2];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-outobj.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 5243;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "syntestgen_cmatcher recv=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.RECV__V2*/ meltfptr[1];
	      /*^apply.arg */
	      argtab[5].meltbp_cstring = " gendev=";
	      /*^apply.arg */
	      argtab[6].meltbp_aptr =
		(melt_ptr_t *) & /*_.GENDEV__V3*/ meltfptr[2];
	      /*_.MELT_DEBUG_FUN__V8*/ meltfptr[7] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V7*/ meltfptr[6] = /*_.MELT_DEBUG_FUN__V8*/ meltfptr[7];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-outobj.melt:5243:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L3*/ meltfnum[2] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V8*/ meltfptr[7] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V7*/ meltfptr[6] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-outobj.melt:5243:/ quasiblock");


      /*_.PROGN___V9*/ meltfptr[7] = /*_.IF___V7*/ meltfptr[6];;
      /*^compute */
      /*_.IFCPP___V6*/ meltfptr[5] = /*_.PROGN___V9*/ meltfptr[7];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:5243:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L2*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IF___V7*/ meltfptr[6] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V9*/ meltfptr[7] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V6*/ meltfptr[5] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:5244:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L4*/ meltfnum[2] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.RECV__V2*/ meltfptr[1]),
			     (melt_ptr_t) (( /*!CLASS_CMATCHER */ meltfrout->
					    tabval[1])));;
      MELT_LOCATION ("warmelt-outobj.melt:5244:/ cond");
      /*cond */ if ( /*_#IS_A__L4*/ meltfnum[2])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V11*/ meltfptr[7] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-outobj.melt:5244:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check recv"),
				  ("warmelt-outobj.melt")
				  ? ("warmelt-outobj.melt") : __FILE__,
				  (5244) ? (5244) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V11*/ meltfptr[7] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V10*/ meltfptr[6] = /*_.IFELSE___V11*/ meltfptr[7];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:5244:/ clear");
	     /*clear *//*_#IS_A__L4*/ meltfnum[2] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V11*/ meltfptr[7] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V10*/ meltfptr[6] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:5245:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L5*/ meltfnum[1] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.GENDEV__V3*/ meltfptr[2]),
			     (melt_ptr_t) (( /*!CLASS_SOURCE_GENERATOR_DEVICE */ meltfrout->tabval[2])));;
      MELT_LOCATION ("warmelt-outobj.melt:5245:/ cond");
      /*cond */ if ( /*_#IS_A__L5*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V13*/ meltfptr[12] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-outobj.melt:5245:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check gendev"),
				  ("warmelt-outobj.melt")
				  ? ("warmelt-outobj.melt") : __FILE__,
				  (5245) ? (5245) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V13*/ meltfptr[12] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V12*/ meltfptr[7] = /*_.IFELSE___V13*/ meltfptr[12];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:5245:/ clear");
	     /*clear *//*_#IS_A__L5*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V13*/ meltfptr[12] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V12*/ meltfptr[7] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:5246:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_STRBUF__L6*/ meltfnum[2] =
	(melt_magic_discr ((melt_ptr_t) ( /*_.SBUF__V4*/ meltfptr[3])) ==
	 MELTOBMAG_STRBUF);;
      MELT_LOCATION ("warmelt-outobj.melt:5246:/ cond");
      /*cond */ if ( /*_#IS_STRBUF__L6*/ meltfnum[2])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V15*/ meltfptr[14] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-outobj.melt:5246:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check sbuf"),
				  ("warmelt-outobj.melt")
				  ? ("warmelt-outobj.melt") : __FILE__,
				  (5246) ? (5246) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V15*/ meltfptr[14] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V14*/ meltfptr[12] = /*_.IFELSE___V15*/ meltfptr[14];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:5246:/ clear");
	     /*clear *//*_#IS_STRBUF__L6*/ meltfnum[2] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V15*/ meltfptr[14] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V14*/ meltfptr[12] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:5247:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L7*/ meltfnum[1] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.MODCTX__V5*/ meltfptr[4]),
			     (melt_ptr_t) (( /*!CLASS_MODULE_CONTEXT */
					    meltfrout->tabval[3])));;
      MELT_LOCATION ("warmelt-outobj.melt:5247:/ cond");
      /*cond */ if ( /*_#IS_A__L7*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V17*/ meltfptr[16] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-outobj.melt:5247:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check modctx"),
				  ("warmelt-outobj.melt")
				  ? ("warmelt-outobj.melt") : __FILE__,
				  (5247) ? (5247) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V17*/ meltfptr[16] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V16*/ meltfptr[14] = /*_.IFELSE___V17*/ meltfptr[16];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:5247:/ clear");
	     /*clear *//*_#IS_A__L7*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V17*/ meltfptr[16] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V16*/ meltfptr[14] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-outobj.melt:5248:/ quasiblock");


    /*^cond */
    /*cond */ if (
		   /*ifisa */
		   melt_is_instance_of ((melt_ptr_t)
					( /*_.GENDEV__V3*/ meltfptr[2]),
					(melt_ptr_t) (( /*!CLASS_LOCATED */
						       meltfrout->tabval[4])))
      )				/*then */
      {
	/*^cond.then */
	/*^getslot */
	{
	  melt_ptr_t slot = NULL, obj = NULL;
	  obj = (melt_ptr_t) ( /*_.GENDEV__V3*/ meltfptr[2]) /*=obj*/ ;
	  melt_object_get_field (slot, obj, 1, "LOCA_LOCATION");
   /*_.DLOC__V18*/ meltfptr[16] = slot;
	};
	;
      }
    else
      {				/*^cond.else */

  /*_.DLOC__V18*/ meltfptr[16] = NULL;;
      }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:5249:/ cond");
    /*cond */ if (
		   /*ifisa */
		   melt_is_instance_of ((melt_ptr_t)
					( /*_.GENDEV__V3*/ meltfptr[2]),
					(melt_ptr_t) (( /*!CLASS_SOURCE_GENERATOR_DEVICE */ meltfrout->tabval[2])))
      )				/*then */
      {
	/*^cond.then */
	/*^getslot */
	{
	  melt_ptr_t slot = NULL, obj = NULL;
	  obj = (melt_ptr_t) ( /*_.GENDEV__V3*/ meltfptr[2]) /*=obj*/ ;
	  melt_object_get_field (slot, obj, 2, "SRCGEN_DEFIN");
   /*_.CMATDEF__V19*/ meltfptr[18] = slot;
	};
	;
      }
    else
      {				/*^cond.else */

  /*_.CMATDEF__V19*/ meltfptr[18] = NULL;;
      }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:5250:/ cond");
    /*cond */ if (
		   /*ifisa */
		   melt_is_instance_of ((melt_ptr_t)
					( /*_.GENDEV__V3*/ meltfptr[2]),
					(melt_ptr_t) (( /*!CLASS_SOURCE_GENERATOR_DEVICE */ meltfrout->tabval[2])))
      )				/*then */
      {
	/*^cond.then */
	/*^getslot */
	{
	  melt_ptr_t slot = NULL, obj = NULL;
	  obj = (melt_ptr_t) ( /*_.GENDEV__V3*/ meltfptr[2]) /*=obj*/ ;
	  melt_object_get_field (slot, obj, 3, "SRCGEN_REPR");
   /*_.CMATREP__V20*/ meltfptr[19] = slot;
	};
	;
      }
    else
      {				/*^cond.else */

  /*_.CMATREP__V20*/ meltfptr[19] = NULL;;
      }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:5251:/ cond");
    /*cond */ if (
		   /*ifisa */
		   melt_is_instance_of ((melt_ptr_t)
					( /*_.RECV__V2*/ meltfptr[1]),
					(melt_ptr_t) (( /*!CLASS_NAMED */
						       meltfrout->tabval[5])))
      )				/*then */
      {
	/*^cond.then */
	/*^getslot */
	{
	  melt_ptr_t slot = NULL, obj = NULL;
	  obj = (melt_ptr_t) ( /*_.RECV__V2*/ meltfptr[1]) /*=obj*/ ;
	  melt_object_get_field (slot, obj, 1, "NAMED_NAME");
   /*_.CMATNAM__V21*/ meltfptr[20] = slot;
	};
	;
      }
    else
      {				/*^cond.else */

  /*_.CMATNAM__V21*/ meltfptr[20] = NULL;;
      }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:5252:/ cond");
    /*cond */ if (
		   /*ifisa */
		   melt_is_instance_of ((melt_ptr_t)
					( /*_.RECV__V2*/ meltfptr[1]),
					(melt_ptr_t) (( /*!CLASS_ANY_MATCHER */ meltfrout->tabval[6])))
      )				/*then */
      {
	/*^cond.then */
	/*^getslot */
	{
	  melt_ptr_t slot = NULL, obj = NULL;
	  obj = (melt_ptr_t) ( /*_.RECV__V2*/ meltfptr[1]) /*=obj*/ ;
	  melt_object_get_field (slot, obj, 2, "AMATCH_IN");
   /*_.CMATINS__V22*/ meltfptr[21] = slot;
	};
	;
      }
    else
      {				/*^cond.else */

  /*_.CMATINS__V22*/ meltfptr[21] = NULL;;
      }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:5253:/ cond");
    /*cond */ if (
		   /*ifisa */
		   melt_is_instance_of ((melt_ptr_t)
					( /*_.RECV__V2*/ meltfptr[1]),
					(melt_ptr_t) (( /*!CLASS_ANY_MATCHER */ meltfrout->tabval[6])))
      )				/*then */
      {
	/*^cond.then */
	/*^getslot */
	{
	  melt_ptr_t slot = NULL, obj = NULL;
	  obj = (melt_ptr_t) ( /*_.RECV__V2*/ meltfptr[1]) /*=obj*/ ;
	  melt_object_get_field (slot, obj, 3, "AMATCH_MATCHBIND");
   /*_.CMATBIND__V23*/ meltfptr[22] = slot;
	};
	;
      }
    else
      {				/*^cond.else */

  /*_.CMATBIND__V23*/ meltfptr[22] = NULL;;
      }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:5254:/ cond");
    /*cond */ if (
		   /*ifisa */
		   melt_is_instance_of ((melt_ptr_t)
					( /*_.RECV__V2*/ meltfptr[1]),
					(melt_ptr_t) (( /*!CLASS_ANY_MATCHER */ meltfrout->tabval[6])))
      )				/*then */
      {
	/*^cond.then */
	/*^getslot */
	{
	  melt_ptr_t slot = NULL, obj = NULL;
	  obj = (melt_ptr_t) ( /*_.RECV__V2*/ meltfptr[1]) /*=obj*/ ;
	  melt_object_get_field (slot, obj, 4, "AMATCH_OUT");
   /*_.CMATOUT__V24*/ meltfptr[23] = slot;
	};
	;
      }
    else
      {				/*^cond.else */

  /*_.CMATOUT__V24*/ meltfptr[23] = NULL;;
      }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:5255:/ cond");
    /*cond */ if (
		   /*ifisa */
		   melt_is_instance_of ((melt_ptr_t)
					( /*_.RECV__V2*/ meltfptr[1]),
					(melt_ptr_t) (( /*!CLASS_CMATCHER */
						       meltfrout->tabval[1])))
      )				/*then */
      {
	/*^cond.then */
	/*^getslot */
	{
	  melt_ptr_t slot = NULL, obj = NULL;
	  obj = (melt_ptr_t) ( /*_.RECV__V2*/ meltfptr[1]) /*=obj*/ ;
	  melt_object_get_field (slot, obj, 5, "CMATCH_STATE");
   /*_.CMATSTATE__V25*/ meltfptr[24] = slot;
	};
	;
      }
    else
      {				/*^cond.else */

  /*_.CMATSTATE__V25*/ meltfptr[24] = NULL;;
      }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:5256:/ cond");
    /*cond */ if (
		   /*ifisa */
		   melt_is_instance_of ((melt_ptr_t)
					( /*_.RECV__V2*/ meltfptr[1]),
					(melt_ptr_t) (( /*!CLASS_CMATCHER */
						       meltfrout->tabval[1])))
      )				/*then */
      {
	/*^cond.then */
	/*^getslot */
	{
	  melt_ptr_t slot = NULL, obj = NULL;
	  obj = (melt_ptr_t) ( /*_.RECV__V2*/ meltfptr[1]) /*=obj*/ ;
	  melt_object_get_field (slot, obj, 6, "CMATCH_EXPTEST");
   /*_.CMATTEST__V26*/ meltfptr[25] = slot;
	};
	;
      }
    else
      {				/*^cond.else */

  /*_.CMATTEST__V26*/ meltfptr[25] = NULL;;
      }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:5257:/ cond");
    /*cond */ if (
		   /*ifisa */
		   melt_is_instance_of ((melt_ptr_t)
					( /*_.RECV__V2*/ meltfptr[1]),
					(melt_ptr_t) (( /*!CLASS_CMATCHER */
						       meltfrout->tabval[1])))
      )				/*then */
      {
	/*^cond.then */
	/*^getslot */
	{
	  melt_ptr_t slot = NULL, obj = NULL;
	  obj = (melt_ptr_t) ( /*_.RECV__V2*/ meltfptr[1]) /*=obj*/ ;
	  melt_object_get_field (slot, obj, 7, "CMATCH_EXPFILL");
   /*_.CMATFILL__V27*/ meltfptr[26] = slot;
	};
	;
      }
    else
      {				/*^cond.else */

  /*_.CMATFILL__V27*/ meltfptr[26] = NULL;;
      }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:5258:/ cond");
    /*cond */ if (
		   /*ifisa */
		   melt_is_instance_of ((melt_ptr_t)
					( /*_.RECV__V2*/ meltfptr[1]),
					(melt_ptr_t) (( /*!CLASS_CMATCHER */
						       meltfrout->tabval[1])))
      )				/*then */
      {
	/*^cond.then */
	/*^getslot */
	{
	  melt_ptr_t slot = NULL, obj = NULL;
	  obj = (melt_ptr_t) ( /*_.RECV__V2*/ meltfptr[1]) /*=obj*/ ;
	  melt_object_get_field (slot, obj, 8, "CMATCH_EXPOPER");
   /*_.CMATOPER__V28*/ meltfptr[27] = slot;
	};
	;
      }
    else
      {				/*^cond.else */

  /*_.CMATOPER__V28*/ meltfptr[27] = NULL;;
      }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:5259:/ cond");
    /*cond */ if (
		   /*ifisa */
		   melt_is_instance_of ((melt_ptr_t)
					( /*_.CMATDEF__V19*/ meltfptr[18]),
					(melt_ptr_t) (( /*!CLASS_SOURCE_DEFCMATCHER */ meltfrout->tabval[7])))
      )				/*then */
      {
	/*^cond.then */
	/*^getslot */
	{
	  melt_ptr_t slot = NULL, obj = NULL;
	  obj = (melt_ptr_t) ( /*_.CMATDEF__V19*/ meltfptr[18]) /*=obj*/ ;
	  melt_object_get_field (slot, obj, 6, "SCMATDEF_TESTLOC");
   /*_.SCMATDEF_TESTLOC__V29*/ meltfptr[28] = slot;
	};
	;
      }
    else
      {				/*^cond.else */

  /*_.SCMATDEF_TESTLOC__V29*/ meltfptr[28] = NULL;;
      }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:5259:/ cond");
    /*cond */ if ( /*_.SCMATDEF_TESTLOC__V29*/ meltfptr[28])	/*then */
      {
	/*^cond.then */
	/*_.TESTLOC__V30*/ meltfptr[29] =
	  /*_.SCMATDEF_TESTLOC__V29*/ meltfptr[28];;
      }
    else
      {
	MELT_LOCATION ("warmelt-outobj.melt:5259:/ cond.else");

	/*_.TESTLOC__V30*/ meltfptr[29] = /*_.DLOC__V18*/ meltfptr[16];;
      }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:5260:/ cond");
    /*cond */ if (
		   /*ifisa */
		   melt_is_instance_of ((melt_ptr_t)
					( /*_.CMATDEF__V19*/ meltfptr[18]),
					(melt_ptr_t) (( /*!CLASS_SOURCE_DEFCMATCHER */ meltfrout->tabval[7])))
      )				/*then */
      {
	/*^cond.then */
	/*^getslot */
	{
	  melt_ptr_t slot = NULL, obj = NULL;
	  obj = (melt_ptr_t) ( /*_.CMATDEF__V19*/ meltfptr[18]) /*=obj*/ ;
	  melt_object_get_field (slot, obj, 7, "SCMATDEF_FILLLOC");
   /*_.SCMATDEF_FILLLOC__V31*/ meltfptr[30] = slot;
	};
	;
      }
    else
      {				/*^cond.else */

  /*_.SCMATDEF_FILLLOC__V31*/ meltfptr[30] = NULL;;
      }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:5260:/ cond");
    /*cond */ if ( /*_.SCMATDEF_FILLLOC__V31*/ meltfptr[30])	/*then */
      {
	/*^cond.then */
	/*_.FILLLOC__V32*/ meltfptr[31] =
	  /*_.SCMATDEF_FILLLOC__V31*/ meltfptr[30];;
      }
    else
      {
	MELT_LOCATION ("warmelt-outobj.melt:5260:/ cond.else");

	/*_.FILLLOC__V32*/ meltfptr[31] = /*_.DLOC__V18*/ meltfptr[16];;
      }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:5261:/ cond");
    /*cond */ if (
		   /*ifisa */
		   melt_is_instance_of ((melt_ptr_t)
					( /*_.CMATDEF__V19*/ meltfptr[18]),
					(melt_ptr_t) (( /*!CLASS_SOURCE_DEFCMATCHER */ meltfrout->tabval[7])))
      )				/*then */
      {
	/*^cond.then */
	/*^getslot */
	{
	  melt_ptr_t slot = NULL, obj = NULL;
	  obj = (melt_ptr_t) ( /*_.CMATDEF__V19*/ meltfptr[18]) /*=obj*/ ;
	  melt_object_get_field (slot, obj, 8, "SCMATDEF_OPERLOC");
   /*_.SCMATDEF_OPERLOC__V33*/ meltfptr[32] = slot;
	};
	;
      }
    else
      {				/*^cond.else */

  /*_.SCMATDEF_OPERLOC__V33*/ meltfptr[32] = NULL;;
      }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:5261:/ cond");
    /*cond */ if ( /*_.SCMATDEF_OPERLOC__V33*/ meltfptr[32])	/*then */
      {
	/*^cond.then */
	/*_.OPERLOC__V34*/ meltfptr[33] =
	  /*_.SCMATDEF_OPERLOC__V33*/ meltfptr[32];;
      }
    else
      {
	MELT_LOCATION ("warmelt-outobj.melt:5261:/ cond.else");

	/*_.OPERLOC__V34*/ meltfptr[33] = /*_.DLOC__V18*/ meltfptr[16];;
      }
    ;
    /*^compute */
 /*_#MULTIPLE_LENGTH__L8*/ meltfnum[2] =
      (melt_multiple_length
       ((melt_ptr_t) ( /*_.CMATINS__V22*/ meltfptr[21])));;
    /*^compute */
 /*_#MULTIPLE_LENGTH__L9*/ meltfnum[1] =
      (melt_multiple_length
       ((melt_ptr_t) ( /*_.CMATOUT__V24*/ meltfptr[23])));;
    /*^compute */
 /*_#I__L10*/ meltfnum[9] =
      (( /*_#MULTIPLE_LENGTH__L8*/ meltfnum[2]) +
       ( /*_#MULTIPLE_LENGTH__L9*/ meltfnum[1]));;
    /*^compute */
 /*_#I__L11*/ meltfnum[10] =
      ((2) * ( /*_#I__L10*/ meltfnum[9]));;
    /*^compute */
 /*_#I__L12*/ meltfnum[11] =
      ((5) + ( /*_#I__L11*/ meltfnum[10]));;
    /*^compute */
 /*_.REPLMAP__V35*/ meltfptr[34] =
      (meltgc_new_mapobjects
       ((meltobject_ptr_t) (( /*!DISCR_MAP_OBJECTS */ meltfrout->tabval[8])),
	( /*_#I__L12*/ meltfnum[11])));;
    MELT_LOCATION ("warmelt-outobj.melt:5265:/ quasiblock");


 /*_.NBUF__V37*/ meltfptr[36] =
      (melt_ptr_t)
      meltgc_new_strbuf ((meltobject_ptr_t)
			 (( /*!DISCR_STRBUF */ meltfrout->tabval[9])),
			 (const char *) 0);;
    MELT_LOCATION ("warmelt-outobj.melt:5267:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[3];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_cstring = "cmatstate_";
      /*^apply.arg */
      argtab[1].meltbp_long = /*_#IX__L1*/ meltfnum[0];
      /*^apply.arg */
      argtab[2].meltbp_cstring = "_";
      /*_.ADD2OUT__V38*/ meltfptr[37] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!ADD2OUT */ meltfrout->tabval[10])),
		    (melt_ptr_t) ( /*_.NBUF__V37*/ meltfptr[36]),
		    (MELTBPARSTR_CSTRING MELTBPARSTR_LONG MELTBPARSTR_CSTRING
		     ""), argtab, "", (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:5268:/ cond");
    /*cond */ if (
		   /*ifisa */
		   melt_is_instance_of ((melt_ptr_t)
					( /*_.CMATSTATE__V25*/ meltfptr[24]),
					(melt_ptr_t) (( /*!CLASS_NAMED */
						       meltfrout->tabval[5])))
      )				/*then */
      {
	/*^cond.then */
	/*^getslot */
	{
	  melt_ptr_t slot = NULL, obj = NULL;
	  obj = (melt_ptr_t) ( /*_.CMATSTATE__V25*/ meltfptr[24]) /*=obj*/ ;
	  melt_object_get_field (slot, obj, 1, "NAMED_NAME");
   /*_.NAMED_NAME__V39*/ meltfptr[38] = slot;
	};
	;
      }
    else
      {				/*^cond.else */

  /*_.NAMED_NAME__V39*/ meltfptr[38] = NULL;;
      }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:5268:/ locexp");
      meltgc_add_strbuf_cident ((melt_ptr_t) ( /*_.NBUF__V37*/ meltfptr[36]),
				melt_string_str ((melt_ptr_t)
						 ( /*_.NAMED_NAME__V39*/
						  meltfptr[38])));
    }
    ;
 /*_.STRBUF2STRING__V40*/ meltfptr[39] =
      (meltgc_new_stringdup
       ((meltobject_ptr_t)
	(( /*!DISCR_VERBATIM_STRING */ meltfrout->tabval[11])),
	melt_strbuf_str ((melt_ptr_t) ( /*_.NBUF__V37*/ meltfptr[36]))));;
    /*^compute */
    /*_.LET___V36*/ meltfptr[35] = /*_.STRBUF2STRING__V40*/ meltfptr[39];;

    MELT_LOCATION ("warmelt-outobj.melt:5265:/ clear");
	   /*clear *//*_.NBUF__V37*/ meltfptr[36] = 0;
    /*^clear */
	   /*clear *//*_.ADD2OUT__V38*/ meltfptr[37] = 0;
    /*^clear */
	   /*clear *//*_.NAMED_NAME__V39*/ meltfptr[38] = 0;
    /*^clear */
	   /*clear *//*_.STRBUF2STRING__V40*/ meltfptr[39] = 0;
    /*_.REPSTATNAM__V41*/ meltfptr[36] = /*_.LET___V36*/ meltfptr[35];;
    MELT_LOCATION ("warmelt-outobj.melt:5271:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_cstring = "/*cmatcher-syntax ";
      /*_.ADD2OUT__V42*/ meltfptr[37] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!ADD2OUT */ meltfrout->tabval[10])),
		    (melt_ptr_t) ( /*_.SBUF__V4*/ meltfptr[3]),
		    (MELTBPARSTR_CSTRING ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:5272:/ locexp");
      meltgc_add_strbuf_ccomment ((melt_ptr_t) ( /*_.SBUF__V4*/ meltfptr[3]),
				  melt_string_str ((melt_ptr_t)
						   ( /*_.CMATNAM__V21*/
						    meltfptr[20])));
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:5273:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_cstring = "**/ {";
      /*_.ADD2OUT__V43*/ meltfptr[38] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!ADD2OUT */ meltfrout->tabval[10])),
		    (melt_ptr_t) ( /*_.SBUF__V4*/ meltfptr[3]),
		    (MELTBPARSTR_CSTRING ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:5274:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.SBUF__V4*/ meltfptr[3]),
				(1), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:5275:/ locexp");
      meltgc_put_mapobjects ((meltmapobjects_ptr_t)
			     ( /*_.REPLMAP__V35*/ meltfptr[34]),
			     (meltobject_ptr_t) ( /*_.CMATSTATE__V25*/
						 meltfptr[24]),
			     (melt_ptr_t) ( /*_.REPSTATNAM__V41*/
					   meltfptr[36]));
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:5276:/ blockmultialloc");
    /*multiallocblock */
    {
      struct meltletrec_1_st
      {
	struct MELT_MULTIPLE_STRUCT (1) rtup_0__TUPLREC__x3;
	long meltletrec_1_endgap;
      } *meltletrec_1_ptr = 0;
      meltletrec_1_ptr =
	(struct meltletrec_1_st *)
	meltgc_allocate (sizeof (struct meltletrec_1_st), 0);
      /*^blockmultialloc.initfill */
      /*inimult rtup_0__TUPLREC__x3 */
 /*_.TUPLREC___V45*/ meltfptr[44] =
	(melt_ptr_t) & meltletrec_1_ptr->rtup_0__TUPLREC__x3;
      meltletrec_1_ptr->rtup_0__TUPLREC__x3.discr =
	(meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (DISCR_MULTIPLE))));
      meltletrec_1_ptr->rtup_0__TUPLREC__x3.nbval = 1;


      /*^putuple */
      /*putupl#7 */
      melt_assertmsg ("putupl [:5276] #7 checktup",
		      melt_magic_discr ((melt_ptr_t)
					( /*_.TUPLREC___V45*/ meltfptr[44]))
		      == MELTOBMAG_MULTIPLE);
      melt_assertmsg ("putupl [:5276] #7 checkoff",
		      (0 >= 0
		       && 0 <
		       melt_multiple_length ((melt_ptr_t)
					     ( /*_.TUPLREC___V45*/
					      meltfptr[44]))));
      ((meltmultiple_ptr_t) ( /*_.TUPLREC___V45*/ meltfptr[44]))->tabval[0] =
	(melt_ptr_t) ( /*_.CMATBIND__V23*/ meltfptr[22]);
      ;
      /*^touch */
      meltgc_touch ( /*_.TUPLREC___V45*/ meltfptr[44]);
      ;
      /*_.TUPLE___V44*/ meltfptr[39] = /*_.TUPLREC___V45*/ meltfptr[44];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:5276:/ clear");
	    /*clear *//*_.TUPLREC___V45*/ meltfptr[44] = 0;
      /*^clear */
	    /*clear *//*_.TUPLREC___V45*/ meltfptr[44] = 0;
    }				/*end multiallocblock */
    ;
    /*^checksignal */
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[3];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & /*_.REPLMAP__V35*/ meltfptr[34];
      /*^apply.arg */
      argtab[1].meltbp_aptr = (melt_ptr_t *) & /*_.TUPLE___V44*/ meltfptr[39];
      /*^apply.arg */
      argtab[2].meltbp_cstring = "cmatched_";
      /*_.SUBSTITUTE_FORMALS_FOR_SYNTEST__V46*/ meltfptr[44] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!SUBSTITUTE_FORMALS_FOR_SYNTEST */ meltfrout->
		      tabval[12])),
		    (melt_ptr_t) ( /*_.SBUF__V4*/ meltfptr[3]),
		    (MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_CSTRING ""),
		    argtab, "", (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:5277:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[3];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & /*_.REPLMAP__V35*/ meltfptr[34];
      /*^apply.arg */
      argtab[1].meltbp_aptr =
	(melt_ptr_t *) & /*_.CMATINS__V22*/ meltfptr[21];
      /*^apply.arg */
      argtab[2].meltbp_cstring = "cmatinput_";
      /*_.SUBSTITUTE_FORMALS_FOR_SYNTEST__V47*/ meltfptr[46] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!SUBSTITUTE_FORMALS_FOR_SYNTEST */ meltfrout->
		      tabval[12])),
		    (melt_ptr_t) ( /*_.SBUF__V4*/ meltfptr[3]),
		    (MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_CSTRING ""),
		    argtab, "", (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:5278:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[3];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & /*_.REPLMAP__V35*/ meltfptr[34];
      /*^apply.arg */
      argtab[1].meltbp_aptr =
	(melt_ptr_t *) & /*_.CMATOUT__V24*/ meltfptr[23];
      /*^apply.arg */
      argtab[2].meltbp_cstring = "cmatoutput_";
      /*_.SUBSTITUTE_FORMALS_FOR_SYNTEST__V48*/ meltfptr[47] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!SUBSTITUTE_FORMALS_FOR_SYNTEST */ meltfrout->
		      tabval[12])),
		    (melt_ptr_t) ( /*_.SBUF__V4*/ meltfptr[3]),
		    (MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_CSTRING ""),
		    argtab, "", (union meltparam_un *) 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:5279:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.SBUF__V4*/ meltfptr[3]),
				(1), 0);
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:5280:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^cond */
    /*cond */ if ( /*_.TESTLOC__V30*/ meltfptr[29])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  /*^checksignal */
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^apply */
	  /*apply */
	  {
	    union meltparam_un argtab[3];
	    memset (&argtab, 0, sizeof (argtab));
	    /*^apply.arg */
	    argtab[0].meltbp_aptr =
	      (melt_ptr_t *) & /*_.SBUF__V4*/ meltfptr[3];
	    /*^apply.arg */
	    argtab[1].meltbp_long = 0;
	    /*^apply.arg */
	    argtab[2].meltbp_cstring = "cmatcher-syntax-test";
	    /*_.OUTPUT_RAW_LOCATION__V50*/ meltfptr[49] =
	      melt_apply ((meltclosure_ptr_t)
			  (( /*!OUTPUT_RAW_LOCATION */ meltfrout->
			    tabval[13])),
			  (melt_ptr_t) ( /*_.TESTLOC__V30*/ meltfptr[29]),
			  (MELTBPARSTR_PTR MELTBPARSTR_LONG
			   MELTBPARSTR_CSTRING ""), argtab, "",
			  (union meltparam_un *) 0);
	  }
	  ;
	  /*_.IF___V49*/ meltfptr[48] =
	    /*_.OUTPUT_RAW_LOCATION__V50*/ meltfptr[49];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-outobj.melt:5280:/ clear");
	     /*clear *//*_.OUTPUT_RAW_LOCATION__V50*/ meltfptr[49] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

  /*_.IF___V49*/ meltfptr[48] = NULL;;
      }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:5281:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[3];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_cstring = "if (/*cmatcher-test ";
      /*^apply.arg */
      argtab[1].meltbp_aptr =
	(melt_ptr_t *) & /*_.CMATNAM__V21*/ meltfptr[20];
      /*^apply.arg */
      argtab[2].meltbp_cstring = ":*/";
      /*_.ADD2OUT__V51*/ meltfptr[49] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!ADD2OUT */ meltfrout->tabval[10])),
		    (melt_ptr_t) ( /*_.SBUF__V4*/ meltfptr[3]),
		    (MELTBPARSTR_CSTRING MELTBPARSTR_PTR MELTBPARSTR_CSTRING
		     ""), argtab, "", (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:5282:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[3];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & /*_.REPLMAP__V35*/ meltfptr[34];
      /*^apply.arg */
      argtab[1].meltbp_aptr =
	(melt_ptr_t *) & /*_.CMATTEST__V26*/ meltfptr[25];
      /*^apply.arg */
      argtab[2].meltbp_aptr =
	(melt_ptr_t *) & /*_.TESTLOC__V30*/ meltfptr[29];
      /*_.EXPAND_TUPLE_FOR_SYNTEST__V52*/ meltfptr[51] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!EXPAND_TUPLE_FOR_SYNTEST */ meltfrout->tabval[14])),
		    (melt_ptr_t) ( /*_.SBUF__V4*/ meltfptr[3]),
		    (MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_PTR ""),
		    argtab, "", (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:5283:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_cstring = "/*cmatcher-endtest*/) {";
      /*_.ADD2OUT__V53*/ meltfptr[52] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!ADD2OUT */ meltfrout->tabval[10])),
		    (melt_ptr_t) ( /*_.SBUF__V4*/ meltfptr[3]),
		    (MELTBPARSTR_CSTRING ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:5284:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.SBUF__V4*/ meltfptr[3]),
				(2), 0);
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:5285:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^cond */
    /*cond */ if ( /*_.FILLLOC__V32*/ meltfptr[31])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  /*^checksignal */
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^apply */
	  /*apply */
	  {
	    union meltparam_un argtab[3];
	    memset (&argtab, 0, sizeof (argtab));
	    /*^apply.arg */
	    argtab[0].meltbp_aptr =
	      (melt_ptr_t *) & /*_.SBUF__V4*/ meltfptr[3];
	    /*^apply.arg */
	    argtab[1].meltbp_long = 0;
	    /*^apply.arg */
	    argtab[2].meltbp_cstring = "cmatcher-syntax-fill";
	    /*_.OUTPUT_RAW_LOCATION__V55*/ meltfptr[54] =
	      melt_apply ((meltclosure_ptr_t)
			  (( /*!OUTPUT_RAW_LOCATION */ meltfrout->
			    tabval[13])),
			  (melt_ptr_t) ( /*_.FILLLOC__V32*/ meltfptr[31]),
			  (MELTBPARSTR_PTR MELTBPARSTR_LONG
			   MELTBPARSTR_CSTRING ""), argtab, "",
			  (union meltparam_un *) 0);
	  }
	  ;
	  /*_.IF___V54*/ meltfptr[53] =
	    /*_.OUTPUT_RAW_LOCATION__V55*/ meltfptr[54];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-outobj.melt:5285:/ clear");
	     /*clear *//*_.OUTPUT_RAW_LOCATION__V55*/ meltfptr[54] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

  /*_.IF___V54*/ meltfptr[53] = NULL;;
      }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:5286:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[3];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_cstring = "/*cmatcher-fill ";
      /*^apply.arg */
      argtab[1].meltbp_aptr =
	(melt_ptr_t *) & /*_.CMATNAM__V21*/ meltfptr[20];
      /*^apply.arg */
      argtab[2].meltbp_cstring = ":*/";
      /*_.ADD2OUT__V56*/ meltfptr[54] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!ADD2OUT */ meltfrout->tabval[10])),
		    (melt_ptr_t) ( /*_.SBUF__V4*/ meltfptr[3]),
		    (MELTBPARSTR_CSTRING MELTBPARSTR_PTR MELTBPARSTR_CSTRING
		     ""), argtab, "", (union meltparam_un *) 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:5287:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.SBUF__V4*/ meltfptr[3]),
				(2), 0);
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:5288:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[3];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & /*_.REPLMAP__V35*/ meltfptr[34];
      /*^apply.arg */
      argtab[1].meltbp_aptr =
	(melt_ptr_t *) & /*_.CMATFILL__V27*/ meltfptr[26];
      /*^apply.arg */
      argtab[2].meltbp_aptr =
	(melt_ptr_t *) & /*_.FILLLOC__V32*/ meltfptr[31];
      /*_.EXPAND_TUPLE_FOR_SYNTEST__V57*/ meltfptr[56] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!EXPAND_TUPLE_FOR_SYNTEST */ meltfrout->tabval[14])),
		    (melt_ptr_t) ( /*_.SBUF__V4*/ meltfptr[3]),
		    (MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_PTR ""),
		    argtab, "", (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:5289:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_cstring = ";";
      /*_.ADD2OUT__V58*/ meltfptr[57] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!ADD2OUT */ meltfrout->tabval[10])),
		    (melt_ptr_t) ( /*_.SBUF__V4*/ meltfptr[3]),
		    (MELTBPARSTR_CSTRING ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:5290:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.SBUF__V4*/ meltfptr[3]),
				(2), 0);
    }
    ;
    /*citerblock FOREACH_IN_MULTIPLE */
    {
      /* start foreach_in_multiple meltcit1__EACHTUP */
      long meltcit1__EACHTUP_ln =
	melt_multiple_length ((melt_ptr_t) /*_.CMATOUT__V24*/ meltfptr[23]);
      for ( /*_#OUTIX__L13*/ meltfnum[12] = 0;
	   ( /*_#OUTIX__L13*/ meltfnum[12] >= 0)
	   && ( /*_#OUTIX__L13*/ meltfnum[12] < meltcit1__EACHTUP_ln);
	/*_#OUTIX__L13*/ meltfnum[12]++)
	{
	  /*_.CURMATOUTBIND__V59*/ meltfptr[58] =
	    melt_multiple_nth ((melt_ptr_t)
			       ( /*_.CMATOUT__V24*/ meltfptr[23]),
			       /*_#OUTIX__L13*/ meltfnum[12]);




#if MELT_HAVE_DEBUG
	  MELT_LOCATION ("warmelt-outobj.melt:5294:/ cppif.then");
	  /*^block */
	  /*anyblock */
	  {


	    {
	      /*^locexp */
	      /*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	      melt_dbgcounter++;
#endif
	      ;
	    }
	    ;
	    /*^checksignal */
	    MELT_CHECK_SIGNAL ();
	    ;
    /*_#MELT_NEED_DBG__L14*/ meltfnum[13] =
	      /*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	      ( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	      0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	      ;;
	    MELT_LOCATION ("warmelt-outobj.melt:5294:/ cond");
	    /*cond */ if ( /*_#MELT_NEED_DBG__L14*/ meltfnum[13])	/*then */
	      {
		/*^cond.then */
		/*^block */
		/*anyblock */
		{

      /*_#THE_MELTCALLCOUNT__L15*/ meltfnum[14] =
#ifdef meltcallcount
		    meltcallcount	/* the_meltcallcount */
#else
		    0L
#endif /* meltcallcount the_meltcallcount */
		    ;;
		  MELT_LOCATION ("warmelt-outobj.melt:5294:/ checksignal");
		  MELT_CHECK_SIGNAL ();
		  ;
		  /*^apply */
		  /*apply */
		  {
		    union meltparam_un argtab[5];
		    memset (&argtab, 0, sizeof (argtab));
		    /*^apply.arg */
		    argtab[0].meltbp_long =
		      /*_#THE_MELTCALLCOUNT__L15*/ meltfnum[14];
		    /*^apply.arg */
		    argtab[1].meltbp_cstring = "warmelt-outobj.melt";
		    /*^apply.arg */
		    argtab[2].meltbp_long = 5294;
		    /*^apply.arg */
		    argtab[3].meltbp_cstring =
		      "syntestgen_cmatcher curmatoutbind=";
		    /*^apply.arg */
		    argtab[4].meltbp_aptr =
		      (melt_ptr_t *) & /*_.CURMATOUTBIND__V59*/ meltfptr[58];
		    /*_.MELT_DEBUG_FUN__V62*/ meltfptr[61] =
		      melt_apply ((meltclosure_ptr_t)
				  (( /*!MELT_DEBUG_FUN */ meltfrout->
				    tabval[0])),
				  (melt_ptr_t) (( /*nil */ NULL)),
				  (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_PTR ""), argtab, "",
				  (union meltparam_un *) 0);
		  }
		  ;
		  /*_.IF___V61*/ meltfptr[60] =
		    /*_.MELT_DEBUG_FUN__V62*/ meltfptr[61];;
		  /*epilog */

		  MELT_LOCATION ("warmelt-outobj.melt:5294:/ clear");
		/*clear *//*_#THE_MELTCALLCOUNT__L15*/ meltfnum[14] = 0;
		  /*^clear */
		/*clear *//*_.MELT_DEBUG_FUN__V62*/ meltfptr[61] = 0;
		}
		;
	      }
	    else
	      {			/*^cond.else */

     /*_.IF___V61*/ meltfptr[60] = NULL;;
	      }
	    ;
	    MELT_LOCATION ("warmelt-outobj.melt:5294:/ quasiblock");


	    /*_.PROGN___V63*/ meltfptr[61] = /*_.IF___V61*/ meltfptr[60];;
	    /*^compute */
	    /*_.IFCPP___V60*/ meltfptr[59] = /*_.PROGN___V63*/ meltfptr[61];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-outobj.melt:5294:/ clear");
	      /*clear *//*_#MELT_NEED_DBG__L14*/ meltfnum[13] = 0;
	    /*^clear */
	      /*clear *//*_.IF___V61*/ meltfptr[60] = 0;
	    /*^clear */
	      /*clear *//*_.PROGN___V63*/ meltfptr[61] = 0;
	  }

#else /*MELT_HAVE_DEBUG */
	  /*^cppif.else */
	  /*_.IFCPP___V60*/ meltfptr[59] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
	  ;
	  MELT_LOCATION ("warmelt-outobj.melt:5295:/ quasiblock");


	  MELT_LOCATION ("warmelt-outobj.melt:5296:/ cond");
	  /*cond */ if (
			 /*ifisa */
			 melt_is_instance_of ((melt_ptr_t)
					      ( /*_.CURMATOUTBIND__V59*/
					       meltfptr[58]),
					      (melt_ptr_t) (( /*!CLASS_ANY_BINDING */ meltfrout->tabval[15])))
	    )			/*then */
	    {
	      /*^cond.then */
	      /*^getslot */
	      {
		melt_ptr_t slot = NULL, obj = NULL;
		obj =
		  (melt_ptr_t) ( /*_.CURMATOUTBIND__V59*/ meltfptr[58])
		  /*=obj*/ ;
		melt_object_get_field (slot, obj, 0, "BINDER");
    /*_.CUROUTFORMAL__V64*/ meltfptr[60] = slot;
	      };
	      ;
	    }
	  else
	    {			/*^cond.else */

   /*_.CUROUTFORMAL__V64*/ meltfptr[60] = NULL;;
	    }
	  ;
	  /*^compute */
  /*_.NAMOUTFORMAL__V65*/ meltfptr[61] =
	    /*mapobject_get */
	    melt_get_mapobjects ((meltmapobjects_ptr_t)
				 ( /*_.REPLMAP__V35*/ meltfptr[34]),
				 (meltobject_ptr_t) ( /*_.CUROUTFORMAL__V64*/
						     meltfptr[60]));;

#if MELT_HAVE_DEBUG
	  MELT_LOCATION ("warmelt-outobj.melt:5299:/ cppif.then");
	  /*^block */
	  /*anyblock */
	  {


	    {
	      /*^locexp */
	      /*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	      melt_dbgcounter++;
#endif
	      ;
	    }
	    ;
	    /*^checksignal */
	    MELT_CHECK_SIGNAL ();
	    ;
    /*_#MELT_NEED_DBG__L16*/ meltfnum[14] =
	      /*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	      ( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	      0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	      ;;
	    MELT_LOCATION ("warmelt-outobj.melt:5299:/ cond");
	    /*cond */ if ( /*_#MELT_NEED_DBG__L16*/ meltfnum[14])	/*then */
	      {
		/*^cond.then */
		/*^block */
		/*anyblock */
		{

      /*_#THE_MELTCALLCOUNT__L17*/ meltfnum[13] =
#ifdef meltcallcount
		    meltcallcount	/* the_meltcallcount */
#else
		    0L
#endif /* meltcallcount the_meltcallcount */
		    ;;
		  MELT_LOCATION ("warmelt-outobj.melt:5299:/ checksignal");
		  MELT_CHECK_SIGNAL ();
		  ;
		  /*^apply */
		  /*apply */
		  {
		    union meltparam_un argtab[7];
		    memset (&argtab, 0, sizeof (argtab));
		    /*^apply.arg */
		    argtab[0].meltbp_long =
		      /*_#THE_MELTCALLCOUNT__L17*/ meltfnum[13];
		    /*^apply.arg */
		    argtab[1].meltbp_cstring = "warmelt-outobj.melt";
		    /*^apply.arg */
		    argtab[2].meltbp_long = 5299;
		    /*^apply.arg */
		    argtab[3].meltbp_cstring =
		      "syntestgen_cmatcher curmatoutbind=";
		    /*^apply.arg */
		    argtab[4].meltbp_aptr =
		      (melt_ptr_t *) & /*_.CURMATOUTBIND__V59*/ meltfptr[58];
		    /*^apply.arg */
		    argtab[5].meltbp_cstring = " namoutformal=";
		    /*^apply.arg */
		    argtab[6].meltbp_aptr =
		      (melt_ptr_t *) & /*_.NAMOUTFORMAL__V65*/ meltfptr[61];
		    /*_.MELT_DEBUG_FUN__V68*/ meltfptr[67] =
		      melt_apply ((meltclosure_ptr_t)
				  (( /*!MELT_DEBUG_FUN */ meltfrout->
				    tabval[0])),
				  (melt_ptr_t) (( /*nil */ NULL)),
				  (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_PTR MELTBPARSTR_CSTRING
				   MELTBPARSTR_PTR ""), argtab, "",
				  (union meltparam_un *) 0);
		  }
		  ;
		  /*_.IF___V67*/ meltfptr[66] =
		    /*_.MELT_DEBUG_FUN__V68*/ meltfptr[67];;
		  /*epilog */

		  MELT_LOCATION ("warmelt-outobj.melt:5299:/ clear");
		/*clear *//*_#THE_MELTCALLCOUNT__L17*/ meltfnum[13] = 0;
		  /*^clear */
		/*clear *//*_.MELT_DEBUG_FUN__V68*/ meltfptr[67] = 0;
		}
		;
	      }
	    else
	      {			/*^cond.else */

     /*_.IF___V67*/ meltfptr[66] = NULL;;
	      }
	    ;
	    MELT_LOCATION ("warmelt-outobj.melt:5299:/ quasiblock");


	    /*_.PROGN___V69*/ meltfptr[67] = /*_.IF___V67*/ meltfptr[66];;
	    /*^compute */
	    /*_.IFCPP___V66*/ meltfptr[65] = /*_.PROGN___V69*/ meltfptr[67];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-outobj.melt:5299:/ clear");
	      /*clear *//*_#MELT_NEED_DBG__L16*/ meltfnum[14] = 0;
	    /*^clear */
	      /*clear *//*_.IF___V67*/ meltfptr[66] = 0;
	    /*^clear */
	      /*clear *//*_.PROGN___V69*/ meltfptr[67] = 0;
	  }

#else /*MELT_HAVE_DEBUG */
	  /*^cppif.else */
	  /*_.IFCPP___V66*/ meltfptr[65] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
	  ;

#if MELT_HAVE_DEBUG
	  MELT_LOCATION ("warmelt-outobj.melt:5301:/ cppif.then");
	  /*^block */
	  /*anyblock */
	  {

	    /*^checksignal */
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^cond */
	    /*cond */ if ( /*_.NAMOUTFORMAL__V65*/ meltfptr[61])	/*then */
	      {
		/*^cond.then */
		/*_.IFELSE___V71*/ meltfptr[67] = ( /*nil */ NULL);;
	      }
	    else
	      {
		MELT_LOCATION ("warmelt-outobj.melt:5301:/ cond.else");

		/*^block */
		/*anyblock */
		{




		  {
		    /*^locexp */
		    melt_assert_failed (("check namoutformal "),
					("warmelt-outobj.melt")
					? ("warmelt-outobj.melt") : __FILE__,
					(5301) ? (5301) : __LINE__,
					__FUNCTION__);
		    ;
		  }
		  ;
		/*clear *//*_.IFELSE___V71*/ meltfptr[67] = 0;
		  /*epilog */
		}
		;
	      }
	    ;
	    /*^compute */
	    /*_.IFCPP___V70*/ meltfptr[66] = /*_.IFELSE___V71*/ meltfptr[67];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-outobj.melt:5301:/ clear");
	      /*clear *//*_.IFELSE___V71*/ meltfptr[67] = 0;
	  }

#else /*MELT_HAVE_DEBUG */
	  /*^cppif.else */
	  /*_.IFCPP___V70*/ meltfptr[66] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
	  ;
	  MELT_LOCATION ("warmelt-outobj.melt:5302:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^apply */
	  /*apply */
	  {
	    union meltparam_un argtab[3];
	    memset (&argtab, 0, sizeof (argtab));
	    /*^apply.arg */
	    argtab[0].meltbp_cstring = "if (";
	    /*^apply.arg */
	    argtab[1].meltbp_aptr =
	      (melt_ptr_t *) & /*_.NAMOUTFORMAL__V65*/ meltfptr[61];
	    /*^apply.arg */
	    argtab[2].meltbp_cstring = ") return;";
	    /*_.ADD2OUT__V72*/ meltfptr[67] =
	      melt_apply ((meltclosure_ptr_t)
			  (( /*!ADD2OUT */ meltfrout->tabval[10])),
			  (melt_ptr_t) ( /*_.SBUF__V4*/ meltfptr[3]),
			  (MELTBPARSTR_CSTRING MELTBPARSTR_PTR
			   MELTBPARSTR_CSTRING ""), argtab, "",
			  (union meltparam_un *) 0);
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:5303:/ locexp");
	    meltgc_strbuf_add_indent ((melt_ptr_t)
				      ( /*_.SBUF__V4*/ meltfptr[3]), (2), 0);
	  }
	  ;

	  MELT_LOCATION ("warmelt-outobj.melt:5295:/ clear");
	    /*clear *//*_.CUROUTFORMAL__V64*/ meltfptr[60] = 0;
	  /*^clear */
	    /*clear *//*_.NAMOUTFORMAL__V65*/ meltfptr[61] = 0;
	  /*^clear */
	    /*clear *//*_.IFCPP___V66*/ meltfptr[65] = 0;
	  /*^clear */
	    /*clear *//*_.IFCPP___V70*/ meltfptr[66] = 0;
	  /*^clear */
	    /*clear *//*_.ADD2OUT__V72*/ meltfptr[67] = 0;
	  if ( /*_#OUTIX__L13*/ meltfnum[12] < 0)
	    break;
	}			/* end  foreach_in_multiple meltcit1__EACHTUP */

      /*citerepilog */

      MELT_LOCATION ("warmelt-outobj.melt:5291:/ clear");
	    /*clear *//*_.CURMATOUTBIND__V59*/ meltfptr[58] = 0;
      /*^clear */
	    /*clear *//*_#OUTIX__L13*/ meltfnum[12] = 0;
      /*^clear */
	    /*clear *//*_.IFCPP___V60*/ meltfptr[59] = 0;
    }				/*endciterblock FOREACH_IN_MULTIPLE */
    ;
    MELT_LOCATION ("warmelt-outobj.melt:5306:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[3];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_cstring = "} /*cmatcher-endfill ";
      /*^apply.arg */
      argtab[1].meltbp_aptr =
	(melt_ptr_t *) & /*_.CMATNAM__V21*/ meltfptr[20];
      /*^apply.arg */
      argtab[2].meltbp_cstring = " */ else return;";
      /*_.ADD2OUT__V73*/ meltfptr[60] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!ADD2OUT */ meltfrout->tabval[10])),
		    (melt_ptr_t) ( /*_.SBUF__V4*/ meltfptr[3]),
		    (MELTBPARSTR_CSTRING MELTBPARSTR_PTR MELTBPARSTR_CSTRING
		     ""), argtab, "", (union meltparam_un *) 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:5307:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.SBUF__V4*/ meltfptr[3]),
				(1), 0);
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:5308:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^cond */
    /*cond */ if ( /*_.CMATOPER__V28*/ meltfptr[27])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-outobj.melt:5310:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^cond */
	  /*cond */ if ( /*_.OPERLOC__V34*/ meltfptr[33])	/*then */
	    {
	      /*^cond.then */
	      /*^block */
	      /*anyblock */
	      {

		/*^checksignal */
		MELT_CHECK_SIGNAL ();
		;
		/*^apply */
		/*apply */
		{
		  union meltparam_un argtab[3];
		  memset (&argtab, 0, sizeof (argtab));
		  /*^apply.arg */
		  argtab[0].meltbp_aptr =
		    (melt_ptr_t *) & /*_.SBUF__V4*/ meltfptr[3];
		  /*^apply.arg */
		  argtab[1].meltbp_long = 0;
		  /*^apply.arg */
		  argtab[2].meltbp_cstring = "cmatcher-syntax-oper";
		  /*_.OUTPUT_RAW_LOCATION__V75*/ meltfptr[65] =
		    melt_apply ((meltclosure_ptr_t)
				(( /*!OUTPUT_RAW_LOCATION */ meltfrout->
				  tabval[13])),
				(melt_ptr_t) ( /*_.OPERLOC__V34*/
					      meltfptr[33]),
				(MELTBPARSTR_PTR MELTBPARSTR_LONG
				 MELTBPARSTR_CSTRING ""), argtab, "",
				(union meltparam_un *) 0);
		}
		;
		/*_.IF___V74*/ meltfptr[61] =
		  /*_.OUTPUT_RAW_LOCATION__V75*/ meltfptr[65];;
		/*epilog */

		MELT_LOCATION ("warmelt-outobj.melt:5310:/ clear");
	       /*clear *//*_.OUTPUT_RAW_LOCATION__V75*/ meltfptr[65] = 0;
	      }
	      ;
	    }
	  else
	    {			/*^cond.else */

    /*_.IF___V74*/ meltfptr[61] = NULL;;
	    }
	  ;
	  MELT_LOCATION ("warmelt-outobj.melt:5312:/ cond");
	  /*cond */ if (
			 /*ifisa */
			 melt_is_instance_of ((melt_ptr_t)
					      ( /*_.CMATBIND__V23*/
					       meltfptr[22]),
					      (melt_ptr_t) (( /*!CLASS_ANY_BINDING */ meltfrout->tabval[15])))
	    )			/*then */
	    {
	      /*^cond.then */
	      /*^getslot */
	      {
		melt_ptr_t slot = NULL, obj = NULL;
		obj =
		  (melt_ptr_t) ( /*_.CMATBIND__V23*/ meltfptr[22]) /*=obj*/ ;
		melt_object_get_field (slot, obj, 0, "BINDER");
     /*_.BINDER__V76*/ meltfptr[66] = slot;
	      };
	      ;
	    }
	  else
	    {			/*^cond.else */

    /*_.BINDER__V76*/ meltfptr[66] = NULL;;
	    }
	  ;
	  MELT_LOCATION ("warmelt-outobj.melt:5312:/ blockmultialloc");
	  /*multiallocblock */
	  {
	    struct meltletrec_2_st
	    {
	      struct MELT_MULTIPLE_STRUCT (3) rtup_0__TUPLREC__x4;
	      long meltletrec_2_endgap;
	    } *meltletrec_2_ptr = 0;
	    meltletrec_2_ptr =
	      (struct meltletrec_2_st *)
	      meltgc_allocate (sizeof (struct meltletrec_2_st), 0);
	    /*^blockmultialloc.initfill */
	    /*inimult rtup_0__TUPLREC__x4 */
 /*_.TUPLREC___V78*/ meltfptr[65] =
	      (melt_ptr_t) & meltletrec_2_ptr->rtup_0__TUPLREC__x4;
	    meltletrec_2_ptr->rtup_0__TUPLREC__x4.discr =
	      (meltobject_ptr_t) (((melt_ptr_t)
				   (MELT_PREDEF (DISCR_MULTIPLE))));
	    meltletrec_2_ptr->rtup_0__TUPLREC__x4.nbval = 3;


	    /*^putuple */
	    /*putupl#8 */
	    melt_assertmsg ("putupl [:5312] #8 checktup",
			    melt_magic_discr ((melt_ptr_t)
					      ( /*_.TUPLREC___V78*/
					       meltfptr[65])) ==
			    MELTOBMAG_MULTIPLE);
	    melt_assertmsg ("putupl [:5312] #8 checkoff",
			    (0 >= 0
			     && 0 <
			     melt_multiple_length ((melt_ptr_t)
						   ( /*_.TUPLREC___V78*/
						    meltfptr[65]))));
	    ((meltmultiple_ptr_t) ( /*_.TUPLREC___V78*/ meltfptr[65]))->
	      tabval[0] =
	      (melt_ptr_t) (( /*!konst_16 */ meltfrout->tabval[16]));
	    ;
	    /*^putuple */
	    /*putupl#9 */
	    melt_assertmsg ("putupl [:5312] #9 checktup",
			    melt_magic_discr ((melt_ptr_t)
					      ( /*_.TUPLREC___V78*/
					       meltfptr[65])) ==
			    MELTOBMAG_MULTIPLE);
	    melt_assertmsg ("putupl [:5312] #9 checkoff",
			    (1 >= 0
			     && 1 <
			     melt_multiple_length ((melt_ptr_t)
						   ( /*_.TUPLREC___V78*/
						    meltfptr[65]))));
	    ((meltmultiple_ptr_t) ( /*_.TUPLREC___V78*/ meltfptr[65]))->
	      tabval[1] = (melt_ptr_t) ( /*_.BINDER__V76*/ meltfptr[66]);
	    ;
	    /*^putuple */
	    /*putupl#10 */
	    melt_assertmsg ("putupl [:5312] #10 checktup",
			    melt_magic_discr ((melt_ptr_t)
					      ( /*_.TUPLREC___V78*/
					       meltfptr[65])) ==
			    MELTOBMAG_MULTIPLE);
	    melt_assertmsg ("putupl [:5312] #10 checkoff",
			    (2 >= 0
			     && 2 <
			     melt_multiple_length ((melt_ptr_t)
						   ( /*_.TUPLREC___V78*/
						    meltfptr[65]))));
	    ((meltmultiple_ptr_t) ( /*_.TUPLREC___V78*/ meltfptr[65]))->
	      tabval[2] =
	      (melt_ptr_t) (( /*!konst_17 */ meltfrout->tabval[17]));
	    ;
	    /*^touch */
	    meltgc_touch ( /*_.TUPLREC___V78*/ meltfptr[65]);
	    ;
	    /*_.TUPLE___V77*/ meltfptr[67] =
	      /*_.TUPLREC___V78*/ meltfptr[65];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-outobj.melt:5312:/ clear");
	      /*clear *//*_.TUPLREC___V78*/ meltfptr[65] = 0;
	    /*^clear */
	      /*clear *//*_.TUPLREC___V78*/ meltfptr[65] = 0;
	  }			/*end multiallocblock */
	  ;
	  MELT_LOCATION ("warmelt-outobj.melt:5311:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^apply */
	  /*apply */
	  {
	    union meltparam_un argtab[3];
	    memset (&argtab, 0, sizeof (argtab));
	    /*^apply.arg */
	    argtab[0].meltbp_aptr =
	      (melt_ptr_t *) & /*_.REPLMAP__V35*/ meltfptr[34];
	    /*^apply.arg */
	    argtab[1].meltbp_aptr =
	      (melt_ptr_t *) & /*_.TUPLE___V77*/ meltfptr[67];
	    /*^apply.arg */
	    argtab[2].meltbp_aptr =
	      (melt_ptr_t *) & /*_.OPERLOC__V34*/ meltfptr[33];
	    /*_.EXPAND_TUPLE_FOR_SYNTEST__V79*/ meltfptr[65] =
	      melt_apply ((meltclosure_ptr_t)
			  (( /*!EXPAND_TUPLE_FOR_SYNTEST */ meltfrout->
			    tabval[14])),
			  (melt_ptr_t) ( /*_.SBUF__V4*/ meltfptr[3]),
			  (MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_PTR
			   ""), argtab, "", (union meltparam_un *) 0);
	  }
	  ;
	  MELT_LOCATION ("warmelt-outobj.melt:5314:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^apply */
	  /*apply */
	  {
	    union meltparam_un argtab[3];
	    memset (&argtab, 0, sizeof (argtab));
	    /*^apply.arg */
	    argtab[0].meltbp_aptr =
	      (melt_ptr_t *) & /*_.REPLMAP__V35*/ meltfptr[34];
	    /*^apply.arg */
	    argtab[1].meltbp_aptr =
	      (melt_ptr_t *) & /*_.CMATOPER__V28*/ meltfptr[27];
	    /*^apply.arg */
	    argtab[2].meltbp_aptr =
	      (melt_ptr_t *) & /*_.OPERLOC__V34*/ meltfptr[33];
	    /*_.EXPAND_TUPLE_FOR_SYNTEST__V80*/ meltfptr[79] =
	      melt_apply ((meltclosure_ptr_t)
			  (( /*!EXPAND_TUPLE_FOR_SYNTEST */ meltfrout->
			    tabval[14])),
			  (melt_ptr_t) ( /*_.SBUF__V4*/ meltfptr[3]),
			  (MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_PTR
			   ""), argtab, "", (union meltparam_un *) 0);
	  }
	  ;
	  MELT_LOCATION ("warmelt-outobj.melt:5315:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^apply */
	  /*apply */
	  {
	    union meltparam_un argtab[1];
	    memset (&argtab, 0, sizeof (argtab));
	    /*^apply.arg */
	    argtab[0].meltbp_cstring = ";";
	    /*_.ADD2OUT__V81*/ meltfptr[80] =
	      melt_apply ((meltclosure_ptr_t)
			  (( /*!ADD2OUT */ meltfrout->tabval[10])),
			  (melt_ptr_t) ( /*_.SBUF__V4*/ meltfptr[3]),
			  (MELTBPARSTR_CSTRING ""), argtab, "",
			  (union meltparam_un *) 0);
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:5316:/ locexp");
	    meltgc_strbuf_add_indent ((melt_ptr_t)
				      ( /*_.SBUF__V4*/ meltfptr[3]), (2), 0);
	  }
	  ;
	  MELT_LOCATION ("warmelt-outobj.melt:5309:/ quasiblock");


	  /*epilog */

	  MELT_LOCATION ("warmelt-outobj.melt:5308:/ clear");
	     /*clear *//*_.IF___V74*/ meltfptr[61] = 0;
	  /*^clear */
	     /*clear *//*_.BINDER__V76*/ meltfptr[66] = 0;
	  /*^clear */
	     /*clear *//*_.TUPLE___V77*/ meltfptr[67] = 0;
	  /*^clear */
	     /*clear *//*_.EXPAND_TUPLE_FOR_SYNTEST__V79*/ meltfptr[65] = 0;
	  /*^clear */
	     /*clear *//*_.EXPAND_TUPLE_FOR_SYNTEST__V80*/ meltfptr[79] = 0;
	  /*^clear */
	     /*clear *//*_.ADD2OUT__V81*/ meltfptr[80] = 0;
	}
	;
      }				/*noelse */
    ;
    MELT_LOCATION ("warmelt-outobj.melt:5318:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_cstring = "} /*end cmatcher-syntax ";
      /*_.ADD2OUT__V82*/ meltfptr[61] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!ADD2OUT */ meltfrout->tabval[10])),
		    (melt_ptr_t) ( /*_.SBUF__V4*/ meltfptr[3]),
		    (MELTBPARSTR_CSTRING ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:5319:/ locexp");
      meltgc_add_strbuf_ccomment ((melt_ptr_t) ( /*_.SBUF__V4*/ meltfptr[3]),
				  melt_string_str ((melt_ptr_t)
						   ( /*_.CMATNAM__V21*/
						    meltfptr[20])));
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:5320:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_cstring = "**/";
      /*_.ADD2OUT__V83*/ meltfptr[66] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!ADD2OUT */ meltfrout->tabval[10])),
		    (melt_ptr_t) ( /*_.SBUF__V4*/ meltfptr[3]),
		    (MELTBPARSTR_CSTRING ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:5321:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.SBUF__V4*/ meltfptr[3]),
				(0), 0);
    }
    ;

    MELT_LOCATION ("warmelt-outobj.melt:5248:/ clear");
	   /*clear *//*_.DLOC__V18*/ meltfptr[16] = 0;
    /*^clear */
	   /*clear *//*_.CMATDEF__V19*/ meltfptr[18] = 0;
    /*^clear */
	   /*clear *//*_.CMATREP__V20*/ meltfptr[19] = 0;
    /*^clear */
	   /*clear *//*_.CMATNAM__V21*/ meltfptr[20] = 0;
    /*^clear */
	   /*clear *//*_.CMATINS__V22*/ meltfptr[21] = 0;
    /*^clear */
	   /*clear *//*_.CMATBIND__V23*/ meltfptr[22] = 0;
    /*^clear */
	   /*clear *//*_.CMATOUT__V24*/ meltfptr[23] = 0;
    /*^clear */
	   /*clear *//*_.CMATSTATE__V25*/ meltfptr[24] = 0;
    /*^clear */
	   /*clear *//*_.CMATTEST__V26*/ meltfptr[25] = 0;
    /*^clear */
	   /*clear *//*_.CMATFILL__V27*/ meltfptr[26] = 0;
    /*^clear */
	   /*clear *//*_.CMATOPER__V28*/ meltfptr[27] = 0;
    /*^clear */
	   /*clear *//*_.SCMATDEF_TESTLOC__V29*/ meltfptr[28] = 0;
    /*^clear */
	   /*clear *//*_.TESTLOC__V30*/ meltfptr[29] = 0;
    /*^clear */
	   /*clear *//*_.SCMATDEF_FILLLOC__V31*/ meltfptr[30] = 0;
    /*^clear */
	   /*clear *//*_.FILLLOC__V32*/ meltfptr[31] = 0;
    /*^clear */
	   /*clear *//*_.SCMATDEF_OPERLOC__V33*/ meltfptr[32] = 0;
    /*^clear */
	   /*clear *//*_.OPERLOC__V34*/ meltfptr[33] = 0;
    /*^clear */
	   /*clear *//*_#MULTIPLE_LENGTH__L8*/ meltfnum[2] = 0;
    /*^clear */
	   /*clear *//*_#MULTIPLE_LENGTH__L9*/ meltfnum[1] = 0;
    /*^clear */
	   /*clear *//*_#I__L10*/ meltfnum[9] = 0;
    /*^clear */
	   /*clear *//*_#I__L11*/ meltfnum[10] = 0;
    /*^clear */
	   /*clear *//*_#I__L12*/ meltfnum[11] = 0;
    /*^clear */
	   /*clear *//*_.REPLMAP__V35*/ meltfptr[34] = 0;
    /*^clear */
	   /*clear *//*_.LET___V36*/ meltfptr[35] = 0;
    /*^clear */
	   /*clear *//*_.REPSTATNAM__V41*/ meltfptr[36] = 0;
    /*^clear */
	   /*clear *//*_.ADD2OUT__V42*/ meltfptr[37] = 0;
    /*^clear */
	   /*clear *//*_.ADD2OUT__V43*/ meltfptr[38] = 0;
    /*^clear */
	   /*clear *//*_.TUPLE___V44*/ meltfptr[39] = 0;
    /*^clear */
	   /*clear *//*_.SUBSTITUTE_FORMALS_FOR_SYNTEST__V46*/ meltfptr[44] = 0;
    /*^clear */
	   /*clear *//*_.SUBSTITUTE_FORMALS_FOR_SYNTEST__V47*/ meltfptr[46] = 0;
    /*^clear */
	   /*clear *//*_.SUBSTITUTE_FORMALS_FOR_SYNTEST__V48*/ meltfptr[47] = 0;
    /*^clear */
	   /*clear *//*_.IF___V49*/ meltfptr[48] = 0;
    /*^clear */
	   /*clear *//*_.ADD2OUT__V51*/ meltfptr[49] = 0;
    /*^clear */
	   /*clear *//*_.EXPAND_TUPLE_FOR_SYNTEST__V52*/ meltfptr[51] = 0;
    /*^clear */
	   /*clear *//*_.ADD2OUT__V53*/ meltfptr[52] = 0;
    /*^clear */
	   /*clear *//*_.IF___V54*/ meltfptr[53] = 0;
    /*^clear */
	   /*clear *//*_.ADD2OUT__V56*/ meltfptr[54] = 0;
    /*^clear */
	   /*clear *//*_.EXPAND_TUPLE_FOR_SYNTEST__V57*/ meltfptr[56] = 0;
    /*^clear */
	   /*clear *//*_.ADD2OUT__V58*/ meltfptr[57] = 0;
    /*^clear */
	   /*clear *//*_.ADD2OUT__V73*/ meltfptr[60] = 0;
    /*^clear */
	   /*clear *//*_.ADD2OUT__V82*/ meltfptr[61] = 0;
    /*^clear */
	   /*clear *//*_.ADD2OUT__V83*/ meltfptr[66] = 0;
    MELT_LOCATION ("warmelt-outobj.melt:5242:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V6*/ meltfptr[5] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V10*/ meltfptr[6] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V12*/ meltfptr[7] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V14*/ meltfptr[12] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V16*/ meltfptr[14] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("SYNTESTGEN_CMATCHER", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_129_warmelt_outobj_SYNTESTGEN_CMATCHER_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*noretval */ NULL);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_129_warmelt_outobj_SYNTESTGEN_CMATCHER */



/**** end of warmelt-outobj+04.c ****/
