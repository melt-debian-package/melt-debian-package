/** GENERATED MELT TIMESTAMP FILE meltbuild-sources/warmelt-debug+melttime.h 
** NEVER EDIT OR MOVE THIS, IT IS GENERATED & PARSED! **/
/* These identifiers are generated in warmelt-outobj.melt 
 & handled in melt-runtime.c carefully. */


/* This warmelt-debug+melttime.h is included from warmelt-debug+meltdesc.c only. */
#if meltmod_warmelt_debug_mds__79d1cbb80df03cf76ada97f6049420ea
/* MELT generation timestamp for meltbuild-sources/warmelt-debug */

#ifdef __cplusplus
/* these symbols are extern "C" since dlsym-ed */
extern "C" const char melt_gen_timestamp[] ;
extern "C" const long long melt_gen_timenum ;
extern "C" const char melt_build_timestamp[] ;
extern "C" {
#endif /*__cplusplus */

		 
/*MELT BOOTSTRAP*/
const char melt_gen_timestamp[]="Wed Oct 10 13:49:39 2012 CEST";
const long long melt_gen_timenum=1349869779;

		 const char melt_build_timestamp[]= __DATE__ "@" __TIME__
		 #ifdef __cplusplus
		 " (in C++)"
		 #else
		 " (in C)"
		 #endif /*__cplusplus*/
					;
		 

		 #ifdef __cplusplus
		 }  /* end extern C timestamp */
		 #endif /*__cplusplus */

		 #else /* ! meltmod_warmelt_debug_mds__79d1cbb80df03cf76ada97f6049420ea */
		 #error invalid timestamp file for meltbuild-sources/warmelt-debug 
		 #endif /* meltmod_warmelt_debug_mds__79d1cbb80df03cf76ada97f6049420ea */
		 
