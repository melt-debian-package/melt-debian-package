/* GCC MELT GENERATED FILE warmelt-first+01.c - DO NOT EDIT */
/* secondary MELT generated C file of rank #1 */
#include "melt-run.h"


/* used hash from melt-run.h when compiling this file: */
MELT_EXTERN const char meltrun_used_md5_melt_f1[] =
  MELT_RUN_HASHMD5 /* from melt-run.h */ ;


/**** warmelt-first+01.c declarations ****/


/***************************************************
***
    Copyright 2008, 2009, 2010, 2011, 2012 Free Software Foundation, Inc.
    Contributed by Basile Starynkevitch <basile@starynkevitch.net>

    This file is part of GCC.

    GCC is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3, or (at your option)
    any later version.

    GCC is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with GCC; see the file COPYING3.  If not see
    <http://www.gnu.org/licenses/>.
***
****************************************************/

/* initial MELT module */
#define MELT_HAS_INITIAL_ENVIRONMENT 0	/*initial */

struct melt_callframe_st;	/*defined in melt-runtime.h */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_1_warmelt_first_IS_EMPTY_STRING (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_2_warmelt_first_IS_NON_EMPTY_STRING (meltclosure_ptr_t meltclosp_,
					      melt_ptr_t meltfirstargp_,
					      const melt_argdescr_cell_t
					      meltxargdescr_[],
					      union meltparam_un
					      *meltxargtab_,
					      const melt_argdescr_cell_t
					      meltxresdescr_[],
					      union meltparam_un
					      *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_3_warmelt_first_INSTALL_CTYPE_DESCR (meltclosure_ptr_t meltclosp_,
					      melt_ptr_t meltfirstargp_,
					      const melt_argdescr_cell_t
					      meltxargdescr_[],
					      union meltparam_un
					      *meltxargtab_,
					      const melt_argdescr_cell_t
					      meltxresdescr_[],
					      union meltparam_un
					      *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_4_warmelt_first_ADD_NEW_SYMBOL_TOKEN (meltclosure_ptr_t meltclosp_,
					       melt_ptr_t meltfirstargp_,
					       const melt_argdescr_cell_t
					       meltxargdescr_[],
					       union meltparam_un
					       *meltxargtab_,
					       const melt_argdescr_cell_t
					       meltxresdescr_[],
					       union meltparam_un
					       *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_5_warmelt_first_ADD_NEW_KEYWORD_TOKEN (meltclosure_ptr_t meltclosp_,
						melt_ptr_t meltfirstargp_,
						const melt_argdescr_cell_t
						meltxargdescr_[],
						union meltparam_un
						*meltxargtab_,
						const melt_argdescr_cell_t
						meltxresdescr_[],
						union meltparam_un
						*meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_6_warmelt_first_INTERN_SYMBOL (meltclosure_ptr_t meltclosp_,
					melt_ptr_t meltfirstargp_,
					const melt_argdescr_cell_t
					meltxargdescr_[],
					union meltparam_un *meltxargtab_,
					const melt_argdescr_cell_t
					meltxresdescr_[],
					union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_7_warmelt_first_INTERN_KEYWORD (meltclosure_ptr_t meltclosp_,
					 melt_ptr_t meltfirstargp_,
					 const melt_argdescr_cell_t
					 meltxargdescr_[],
					 union meltparam_un *meltxargtab_,
					 const melt_argdescr_cell_t
					 meltxresdescr_[],
					 union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_8_warmelt_first_CLONE_SYMBOL (meltclosure_ptr_t meltclosp_,
				       melt_ptr_t meltfirstargp_,
				       const melt_argdescr_cell_t
				       meltxargdescr_[],
				       union meltparam_un *meltxargtab_,
				       const melt_argdescr_cell_t
				       meltxresdescr_[],
				       union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_9_warmelt_first_INITFRESH_CONTENVMAKER (meltclosure_ptr_t meltclosp_,
						 melt_ptr_t meltfirstargp_,
						 const melt_argdescr_cell_t
						 meltxargdescr_[],
						 union meltparam_un
						 *meltxargtab_,
						 const melt_argdescr_cell_t
						 meltxresdescr_[],
						 union meltparam_un
						 *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_10_warmelt_first_INITVALUE_EXPORTER (meltclosure_ptr_t meltclosp_,
					      melt_ptr_t meltfirstargp_,
					      const melt_argdescr_cell_t
					      meltxargdescr_[],
					      union meltparam_un
					      *meltxargtab_,
					      const melt_argdescr_cell_t
					      meltxresdescr_[],
					      union meltparam_un
					      *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_11_warmelt_first_INITVALUE_IMPORTER (meltclosure_ptr_t meltclosp_,
					      melt_ptr_t meltfirstargp_,
					      const melt_argdescr_cell_t
					      meltxargdescr_[],
					      union meltparam_un
					      *meltxargtab_,
					      const melt_argdescr_cell_t
					      meltxresdescr_[],
					      union meltparam_un
					      *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_12_warmelt_first_INITMACRO_EXPORTER (meltclosure_ptr_t meltclosp_,
					      melt_ptr_t meltfirstargp_,
					      const melt_argdescr_cell_t
					      meltxargdescr_[],
					      union meltparam_un
					      *meltxargtab_,
					      const melt_argdescr_cell_t
					      meltxresdescr_[],
					      union meltparam_un
					      *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_13_warmelt_first_INITPATMACRO_EXPORTER (meltclosure_ptr_t meltclosp_,
						 melt_ptr_t meltfirstargp_,
						 const melt_argdescr_cell_t
						 meltxargdescr_[],
						 union meltparam_un
						 *meltxargtab_,
						 const melt_argdescr_cell_t
						 meltxresdescr_[],
						 union meltparam_un
						 *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_14_warmelt_first_INIT_EXITFINALIZER (meltclosure_ptr_t meltclosp_,
					      melt_ptr_t meltfirstargp_,
					      const melt_argdescr_cell_t
					      meltxargdescr_[],
					      union meltparam_un
					      *meltxargtab_,
					      const melt_argdescr_cell_t
					      meltxresdescr_[],
					      union meltparam_un
					      *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_15_warmelt_first_AT_EXIT_FIRST (meltclosure_ptr_t meltclosp_,
					 melt_ptr_t meltfirstargp_,
					 const melt_argdescr_cell_t
					 meltxargdescr_[],
					 union meltparam_un *meltxargtab_,
					 const melt_argdescr_cell_t
					 meltxresdescr_[],
					 union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_16_warmelt_first_AT_EXIT_LAST (meltclosure_ptr_t meltclosp_,
					melt_ptr_t meltfirstargp_,
					const melt_argdescr_cell_t
					meltxargdescr_[],
					union meltparam_un *meltxargtab_,
					const melt_argdescr_cell_t
					meltxresdescr_[],
					union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_17_warmelt_first_END_MELT_PASS_RUNNER (meltclosure_ptr_t meltclosp_,
						melt_ptr_t meltfirstargp_,
						const melt_argdescr_cell_t
						meltxargdescr_[],
						union meltparam_un
						*meltxargtab_,
						const melt_argdescr_cell_t
						meltxresdescr_[],
						union meltparam_un
						*meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_18_warmelt_first_LAMBDA___1__ (meltclosure_ptr_t meltclosp_,
					melt_ptr_t meltfirstargp_,
					const melt_argdescr_cell_t
					meltxargdescr_[],
					union meltparam_un *meltxargtab_,
					const melt_argdescr_cell_t
					meltxresdescr_[],
					union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_19_warmelt_first_LAMBDA___2__ (meltclosure_ptr_t meltclosp_,
					melt_ptr_t meltfirstargp_,
					const melt_argdescr_cell_t
					meltxargdescr_[],
					union meltparam_un *meltxargtab_,
					const melt_argdescr_cell_t
					meltxresdescr_[],
					union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_20_warmelt_first_LAMBDA___3__ (meltclosure_ptr_t meltclosp_,
					melt_ptr_t meltfirstargp_,
					const melt_argdescr_cell_t
					meltxargdescr_[],
					union meltparam_un *meltxargtab_,
					const melt_argdescr_cell_t
					meltxresdescr_[],
					union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_21_warmelt_first_AT_END_MELT_PASS_FIRST (meltclosure_ptr_t
						  meltclosp_,
						  melt_ptr_t meltfirstargp_,
						  const melt_argdescr_cell_t
						  meltxargdescr_[],
						  union meltparam_un
						  *meltxargtab_,
						  const melt_argdescr_cell_t
						  meltxresdescr_[],
						  union meltparam_un
						  *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_22_warmelt_first_AT_END_MELT_PASS_LAST (meltclosure_ptr_t meltclosp_,
						 melt_ptr_t meltfirstargp_,
						 const melt_argdescr_cell_t
						 meltxargdescr_[],
						 union meltparam_un
						 *meltxargtab_,
						 const melt_argdescr_cell_t
						 meltxresdescr_[],
						 union meltparam_un
						 *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_23_warmelt_first_INIT_UNITSTARTER (meltclosure_ptr_t meltclosp_,
					    melt_ptr_t meltfirstargp_,
					    const melt_argdescr_cell_t
					    meltxargdescr_[],
					    union meltparam_un *meltxargtab_,
					    const melt_argdescr_cell_t
					    meltxresdescr_[],
					    union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_24_warmelt_first_LAMBDA___4__ (meltclosure_ptr_t meltclosp_,
					melt_ptr_t meltfirstargp_,
					const melt_argdescr_cell_t
					meltxargdescr_[],
					union meltparam_un *meltxargtab_,
					const melt_argdescr_cell_t
					meltxresdescr_[],
					union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_25_warmelt_first_LAMBDA___5__ (meltclosure_ptr_t meltclosp_,
					melt_ptr_t meltfirstargp_,
					const melt_argdescr_cell_t
					meltxargdescr_[],
					union meltparam_un *meltxargtab_,
					const melt_argdescr_cell_t
					meltxresdescr_[],
					union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_26_warmelt_first_LAMBDA___6__ (meltclosure_ptr_t meltclosp_,
					melt_ptr_t meltfirstargp_,
					const melt_argdescr_cell_t
					meltxargdescr_[],
					union meltparam_un *meltxargtab_,
					const melt_argdescr_cell_t
					meltxresdescr_[],
					union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_27_warmelt_first_AT_START_UNIT_FIRST (meltclosure_ptr_t meltclosp_,
					       melt_ptr_t meltfirstargp_,
					       const melt_argdescr_cell_t
					       meltxargdescr_[],
					       union meltparam_un
					       *meltxargtab_,
					       const melt_argdescr_cell_t
					       meltxresdescr_[],
					       union meltparam_un
					       *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_28_warmelt_first_AT_START_UNIT_LAST (meltclosure_ptr_t meltclosp_,
					      melt_ptr_t meltfirstargp_,
					      const melt_argdescr_cell_t
					      meltxargdescr_[],
					      union meltparam_un
					      *meltxargtab_,
					      const melt_argdescr_cell_t
					      meltxresdescr_[],
					      union meltparam_un
					      *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_29_warmelt_first_INIT_UNITFINISHER (meltclosure_ptr_t meltclosp_,
					     melt_ptr_t meltfirstargp_,
					     const melt_argdescr_cell_t
					     meltxargdescr_[],
					     union meltparam_un *meltxargtab_,
					     const melt_argdescr_cell_t
					     meltxresdescr_[],
					     union meltparam_un
					     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_30_warmelt_first_LAMBDA___7__ (meltclosure_ptr_t meltclosp_,
					melt_ptr_t meltfirstargp_,
					const melt_argdescr_cell_t
					meltxargdescr_[],
					union meltparam_un *meltxargtab_,
					const melt_argdescr_cell_t
					meltxresdescr_[],
					union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_31_warmelt_first_LAMBDA___8__ (meltclosure_ptr_t meltclosp_,
					melt_ptr_t meltfirstargp_,
					const melt_argdescr_cell_t
					meltxargdescr_[],
					union meltparam_un *meltxargtab_,
					const melt_argdescr_cell_t
					meltxresdescr_[],
					union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_32_warmelt_first_LAMBDA___9__ (meltclosure_ptr_t meltclosp_,
					melt_ptr_t meltfirstargp_,
					const melt_argdescr_cell_t
					meltxargdescr_[],
					union meltparam_un *meltxargtab_,
					const melt_argdescr_cell_t
					meltxresdescr_[],
					union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_33_warmelt_first_AT_FINISH_UNIT_FIRST (meltclosure_ptr_t meltclosp_,
						melt_ptr_t meltfirstargp_,
						const melt_argdescr_cell_t
						meltxargdescr_[],
						union meltparam_un
						*meltxargtab_,
						const melt_argdescr_cell_t
						meltxresdescr_[],
						union meltparam_un
						*meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_34_warmelt_first_AT_FINISH_UNIT_LAST (meltclosure_ptr_t meltclosp_,
					       melt_ptr_t meltfirstargp_,
					       const melt_argdescr_cell_t
					       meltxargdescr_[],
					       union meltparam_un
					       *meltxargtab_,
					       const melt_argdescr_cell_t
					       meltxresdescr_[],
					       union meltparam_un
					       *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_35_warmelt_first_INIT_OPTIONSETTER (meltclosure_ptr_t meltclosp_,
					     melt_ptr_t meltfirstargp_,
					     const melt_argdescr_cell_t
					     meltxargdescr_[],
					     union meltparam_un *meltxargtab_,
					     const melt_argdescr_cell_t
					     meltxresdescr_[],
					     union meltparam_un
					     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_36_warmelt_first_REGISTER_OPTION (meltclosure_ptr_t meltclosp_,
					   melt_ptr_t meltfirstargp_,
					   const melt_argdescr_cell_t
					   meltxargdescr_[],
					   union meltparam_un *meltxargtab_,
					   const melt_argdescr_cell_t
					   meltxresdescr_[],
					   union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_37_warmelt_first_OPTION_HELPER_FUN (meltclosure_ptr_t meltclosp_,
					     melt_ptr_t meltfirstargp_,
					     const melt_argdescr_cell_t
					     meltxargdescr_[],
					     union meltparam_un *meltxargtab_,
					     const melt_argdescr_cell_t
					     meltxresdescr_[],
					     union meltparam_un
					     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_38_warmelt_first_LAMBDA___10__ (meltclosure_ptr_t meltclosp_,
					 melt_ptr_t meltfirstargp_,
					 const melt_argdescr_cell_t
					 meltxargdescr_[],
					 union meltparam_un *meltxargtab_,
					 const melt_argdescr_cell_t
					 meltxresdescr_[],
					 union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_39_warmelt_first_MAPOBJECT_EVERY (meltclosure_ptr_t meltclosp_,
					   melt_ptr_t meltfirstargp_,
					   const melt_argdescr_cell_t
					   meltxargdescr_[],
					   union meltparam_un *meltxargtab_,
					   const melt_argdescr_cell_t
					   meltxresdescr_[],
					   union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_40_warmelt_first_MAPOBJECT_ITERATE_TEST (meltclosure_ptr_t
						  meltclosp_,
						  melt_ptr_t meltfirstargp_,
						  const melt_argdescr_cell_t
						  meltxargdescr_[],
						  union meltparam_un
						  *meltxargtab_,
						  const melt_argdescr_cell_t
						  meltxresdescr_[],
						  union meltparam_un
						  *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_41_warmelt_first_LIST_EVERY (meltclosure_ptr_t meltclosp_,
				      melt_ptr_t meltfirstargp_,
				      const melt_argdescr_cell_t
				      meltxargdescr_[],
				      union meltparam_un *meltxargtab_,
				      const melt_argdescr_cell_t
				      meltxresdescr_[],
				      union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_42_warmelt_first_LIST_ITERATE_TEST (meltclosure_ptr_t meltclosp_,
					     melt_ptr_t meltfirstargp_,
					     const melt_argdescr_cell_t
					     meltxargdescr_[],
					     union meltparam_un *meltxargtab_,
					     const melt_argdescr_cell_t
					     meltxresdescr_[],
					     union meltparam_un
					     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_43_warmelt_first_LIST_APPEND2LIST (meltclosure_ptr_t meltclosp_,
					    melt_ptr_t meltfirstargp_,
					    const melt_argdescr_cell_t
					    meltxargdescr_[],
					    union meltparam_un *meltxargtab_,
					    const melt_argdescr_cell_t
					    meltxresdescr_[],
					    union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_44_warmelt_first_LIST_MAP (meltclosure_ptr_t meltclosp_,
				    melt_ptr_t meltfirstargp_,
				    const melt_argdescr_cell_t
				    meltxargdescr_[],
				    union meltparam_un *meltxargtab_,
				    const melt_argdescr_cell_t
				    meltxresdescr_[],
				    union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_45_warmelt_first_LIST_FIND (meltclosure_ptr_t meltclosp_,
				     melt_ptr_t meltfirstargp_,
				     const melt_argdescr_cell_t
				     meltxargdescr_[],
				     union meltparam_un *meltxargtab_,
				     const melt_argdescr_cell_t
				     meltxresdescr_[],
				     union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_46_warmelt_first_LIST_TO_MULTIPLE (meltclosure_ptr_t meltclosp_,
					    melt_ptr_t meltfirstargp_,
					    const melt_argdescr_cell_t
					    meltxargdescr_[],
					    union meltparam_un *meltxargtab_,
					    const melt_argdescr_cell_t
					    meltxresdescr_[],
					    union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_47_warmelt_first_LAMBDA___11__ (meltclosure_ptr_t meltclosp_,
					 melt_ptr_t meltfirstargp_,
					 const melt_argdescr_cell_t
					 meltxargdescr_[],
					 union meltparam_un *meltxargtab_,
					 const melt_argdescr_cell_t
					 meltxresdescr_[],
					 union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_48_warmelt_first_PAIRLIST_TO_MULTIPLE (meltclosure_ptr_t meltclosp_,
						melt_ptr_t meltfirstargp_,
						const melt_argdescr_cell_t
						meltxargdescr_[],
						union meltparam_un
						*meltxargtab_,
						const melt_argdescr_cell_t
						meltxresdescr_[],
						union meltparam_un
						*meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_49_warmelt_first_MULTIPLE_ITERATE_TEST (meltclosure_ptr_t meltclosp_,
						 melt_ptr_t meltfirstargp_,
						 const melt_argdescr_cell_t
						 meltxargdescr_[],
						 union meltparam_un
						 *meltxargtab_,
						 const melt_argdescr_cell_t
						 meltxresdescr_[],
						 union meltparam_un
						 *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_50_warmelt_first_MULTIPLE_MAP (meltclosure_ptr_t meltclosp_,
					melt_ptr_t meltfirstargp_,
					const melt_argdescr_cell_t
					meltxargdescr_[],
					union meltparam_un *meltxargtab_,
					const melt_argdescr_cell_t
					meltxresdescr_[],
					union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_51_warmelt_first_MULTIPLE_TO_LIST (meltclosure_ptr_t meltclosp_,
					    melt_ptr_t meltfirstargp_,
					    const melt_argdescr_cell_t
					    meltxargdescr_[],
					    union meltparam_un *meltxargtab_,
					    const melt_argdescr_cell_t
					    meltxresdescr_[],
					    union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_52_warmelt_first_CLOSURE_EVERY (meltclosure_ptr_t meltclosp_,
					 melt_ptr_t meltfirstargp_,
					 const melt_argdescr_cell_t
					 meltxargdescr_[],
					 union meltparam_un *meltxargtab_,
					 const melt_argdescr_cell_t
					 meltxresdescr_[],
					 union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_53_warmelt_first_ROUTINE_EVERY (meltclosure_ptr_t meltclosp_,
					 melt_ptr_t meltfirstargp_,
					 const melt_argdescr_cell_t
					 meltxargdescr_[],
					 union meltparam_un *meltxargtab_,
					 const melt_argdescr_cell_t
					 meltxresdescr_[],
					 union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_54_warmelt_first_INSTALL_METHOD (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_55_warmelt_first_COMPARE_OBJ_RANKED (meltclosure_ptr_t meltclosp_,
					      melt_ptr_t meltfirstargp_,
					      const melt_argdescr_cell_t
					      meltxargdescr_[],
					      union meltparam_un
					      *meltxargtab_,
					      const melt_argdescr_cell_t
					      meltxresdescr_[],
					      union meltparam_un
					      *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_56_warmelt_first_MAPOBJECT_SORTED_ATTRIBUTE_TUPLE (meltclosure_ptr_t
							    meltclosp_,
							    melt_ptr_t
							    meltfirstargp_,
							    const
							    melt_argdescr_cell_t
							    meltxargdescr_[],
							    union meltparam_un
							    *meltxargtab_,
							    const
							    melt_argdescr_cell_t
							    meltxresdescr_[],
							    union meltparam_un
							    *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_57_warmelt_first_LAMBDA___12__ (meltclosure_ptr_t meltclosp_,
					 melt_ptr_t meltfirstargp_,
					 const melt_argdescr_cell_t
					 meltxargdescr_[],
					 union meltparam_un *meltxargtab_,
					 const melt_argdescr_cell_t
					 meltxresdescr_[],
					 union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_58_warmelt_first_LAMBDA___13__ (meltclosure_ptr_t meltclosp_,
					 melt_ptr_t meltfirstargp_,
					 const melt_argdescr_cell_t
					 meltxargdescr_[],
					 union meltparam_un *meltxargtab_,
					 const melt_argdescr_cell_t
					 meltxresdescr_[],
					 union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_59_warmelt_first_LAMBDA___14__ (meltclosure_ptr_t meltclosp_,
					 melt_ptr_t meltfirstargp_,
					 const melt_argdescr_cell_t
					 meltxargdescr_[],
					 union meltparam_un *meltxargtab_,
					 const melt_argdescr_cell_t
					 meltxresdescr_[],
					 union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_60_warmelt_first_FRESH_ENV (meltclosure_ptr_t meltclosp_,
				     melt_ptr_t meltfirstargp_,
				     const melt_argdescr_cell_t
				     meltxargdescr_[],
				     union meltparam_un *meltxargtab_,
				     const melt_argdescr_cell_t
				     meltxresdescr_[],
				     union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_61_warmelt_first_FIND_ENV (meltclosure_ptr_t meltclosp_,
				    melt_ptr_t meltfirstargp_,
				    const melt_argdescr_cell_t
				    meltxargdescr_[],
				    union meltparam_un *meltxargtab_,
				    const melt_argdescr_cell_t
				    meltxresdescr_[],
				    union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_62_warmelt_first_FIND_ENV_DEBUG (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_63_warmelt_first_FIND_ENCLOSING_ENV (meltclosure_ptr_t meltclosp_,
					      melt_ptr_t meltfirstargp_,
					      const melt_argdescr_cell_t
					      meltxargdescr_[],
					      union meltparam_un
					      *meltxargtab_,
					      const melt_argdescr_cell_t
					      meltxresdescr_[],
					      union meltparam_un
					      *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_64_warmelt_first_PUT_ENV (meltclosure_ptr_t meltclosp_,
				   melt_ptr_t meltfirstargp_,
				   const melt_argdescr_cell_t
				   meltxargdescr_[],
				   union meltparam_un *meltxargtab_,
				   const melt_argdescr_cell_t
				   meltxresdescr_[],
				   union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_65_warmelt_first_OVERWRITE_ENV (meltclosure_ptr_t meltclosp_,
					 melt_ptr_t meltfirstargp_,
					 const melt_argdescr_cell_t
					 meltxargdescr_[],
					 union meltparam_un *meltxargtab_,
					 const melt_argdescr_cell_t
					 meltxresdescr_[],
					 union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_66_warmelt_first_POST_INITIALIZATION (meltclosure_ptr_t meltclosp_,
					       melt_ptr_t meltfirstargp_,
					       const melt_argdescr_cell_t
					       meltxargdescr_[],
					       union meltparam_un
					       *meltxargtab_,
					       const melt_argdescr_cell_t
					       meltxresdescr_[],
					       union meltparam_un
					       *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_67_warmelt_first_RETRIEVE_DICTIONNARY_CTYPE_GTY (meltclosure_ptr_t
							  meltclosp_,
							  melt_ptr_t
							  meltfirstargp_,
							  const
							  melt_argdescr_cell_t
							  meltxargdescr_[],
							  union meltparam_un
							  *meltxargtab_,
							  const
							  melt_argdescr_cell_t
							  meltxresdescr_[],
							  union meltparam_un
							  *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_68_warmelt_first_RETRIEVE_DICTIONNARY_CTYPE (meltclosure_ptr_t
						      meltclosp_,
						      melt_ptr_t
						      meltfirstargp_,
						      const
						      melt_argdescr_cell_t
						      meltxargdescr_[],
						      union meltparam_un
						      *meltxargtab_,
						      const
						      melt_argdescr_cell_t
						      meltxresdescr_[],
						      union meltparam_un
						      *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_69_warmelt_first_DEBUG_MSG_FUN (meltclosure_ptr_t meltclosp_,
					 melt_ptr_t meltfirstargp_,
					 const melt_argdescr_cell_t
					 meltxargdescr_[],
					 union meltparam_un *meltxargtab_,
					 const melt_argdescr_cell_t
					 meltxresdescr_[],
					 union meltparam_un *meltxrestab_);



MELT_EXTERN void *melt_start_this_module (void *);


/* define different names when debugging or not */
#if MELT_HAVE_DEBUG
MELT_EXTERN const char meltmodule_warmelt_first__melt_have_debug_enabled[];
#define melt_have_debug_string meltmodule_warmelt_first__melt_have_debug_enabled
#else /*!MELT_HAVE_DEBUG */
MELT_EXTERN const char meltmodule_warmelt_first__melt_have_debug_disabled[];
#define melt_have_debug_string meltmodule_warmelt_first__melt_have_debug_disabled
#endif /*!MELT_HAVE_DEBUG */


struct frame_melt_start_this_module_st;
void MELT_MODULE_VISIBILITY meltmod__warmelt_first__initialmeltchunk_0 (struct
									frame_melt_start_this_module_st
									*,
									char
									*);
void MELT_MODULE_VISIBILITY meltmod__warmelt_first__initialmeltchunk_1 (struct
									frame_melt_start_this_module_st
									*,
									char
									*);
void MELT_MODULE_VISIBILITY meltmod__warmelt_first__initialmeltchunk_2 (struct
									frame_melt_start_this_module_st
									*,
									char
									*);
void MELT_MODULE_VISIBILITY meltmod__warmelt_first__initialmeltchunk_3 (struct
									frame_melt_start_this_module_st
									*,
									char
									*);
void MELT_MODULE_VISIBILITY meltmod__warmelt_first__initialmeltchunk_4 (struct
									frame_melt_start_this_module_st
									*,
									char
									*);
void MELT_MODULE_VISIBILITY meltmod__warmelt_first__initialmeltchunk_5 (struct
									frame_melt_start_this_module_st
									*,
									char
									*);
void MELT_MODULE_VISIBILITY meltmod__warmelt_first__initialmeltchunk_6 (struct
									frame_melt_start_this_module_st
									*,
									char
									*);
void MELT_MODULE_VISIBILITY meltmod__warmelt_first__initialmeltchunk_7 (struct
									frame_melt_start_this_module_st
									*,
									char
									*);
void MELT_MODULE_VISIBILITY meltmod__warmelt_first__initialmeltchunk_8 (struct
									frame_melt_start_this_module_st
									*,
									char
									*);
void MELT_MODULE_VISIBILITY meltmod__warmelt_first__initialmeltchunk_9 (struct
									frame_melt_start_this_module_st
									*,
									char
									*);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_first__initialmeltchunk_10 (struct
					     frame_melt_start_this_module_st
					     *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_first__initialmeltchunk_11 (struct
					     frame_melt_start_this_module_st
					     *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_first__initialmeltchunk_12 (struct
					     frame_melt_start_this_module_st
					     *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_first__initialmeltchunk_13 (struct
					     frame_melt_start_this_module_st
					     *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_first__initialmeltchunk_14 (struct
					     frame_melt_start_this_module_st
					     *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_first__initialmeltchunk_15 (struct
					     frame_melt_start_this_module_st
					     *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_first__initialmeltchunk_16 (struct
					     frame_melt_start_this_module_st
					     *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_first__initialmeltchunk_17 (struct
					     frame_melt_start_this_module_st
					     *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_first__initialmeltchunk_18 (struct
					     frame_melt_start_this_module_st
					     *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_first__initialmeltchunk_19 (struct
					     frame_melt_start_this_module_st
					     *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_first__initialmeltchunk_20 (struct
					     frame_melt_start_this_module_st
					     *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_first__initialmeltchunk_21 (struct
					     frame_melt_start_this_module_st
					     *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_first__initialmeltchunk_22 (struct
					     frame_melt_start_this_module_st
					     *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_first__initialmeltchunk_23 (struct
					     frame_melt_start_this_module_st
					     *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_first__initialmeltchunk_24 (struct
					     frame_melt_start_this_module_st
					     *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_first__initialmeltchunk_25 (struct
					     frame_melt_start_this_module_st
					     *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_first__initialmeltchunk_26 (struct
					     frame_melt_start_this_module_st
					     *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_first__initialmeltchunk_27 (struct
					     frame_melt_start_this_module_st
					     *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_first__initialmeltchunk_28 (struct
					     frame_melt_start_this_module_st
					     *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_first__initialmeltchunk_29 (struct
					     frame_melt_start_this_module_st
					     *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_first__initialmeltchunk_30 (struct
					     frame_melt_start_this_module_st
					     *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_first__initialmeltchunk_31 (struct
					     frame_melt_start_this_module_st
					     *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_first__initialmeltchunk_32 (struct
					     frame_melt_start_this_module_st
					     *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_first__initialmeltchunk_33 (struct
					     frame_melt_start_this_module_st
					     *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_first__initialmeltchunk_34 (struct
					     frame_melt_start_this_module_st
					     *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_first__initialmeltchunk_35 (struct
					     frame_melt_start_this_module_st
					     *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_first__initialmeltchunk_36 (struct
					     frame_melt_start_this_module_st
					     *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_first__initialmeltchunk_37 (struct
					     frame_melt_start_this_module_st
					     *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_first__initialmeltchunk_38 (struct
					     frame_melt_start_this_module_st
					     *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_first__initialmeltchunk_39 (struct
					     frame_melt_start_this_module_st
					     *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_first__initialmeltchunk_40 (struct
					     frame_melt_start_this_module_st
					     *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_first__initialmeltchunk_41 (struct
					     frame_melt_start_this_module_st
					     *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_first__initialmeltchunk_42 (struct
					     frame_melt_start_this_module_st
					     *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_first__initialmeltchunk_43 (struct
					     frame_melt_start_this_module_st
					     *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_first__initialmeltchunk_44 (struct
					     frame_melt_start_this_module_st
					     *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_first__initialmeltchunk_45 (struct
					     frame_melt_start_this_module_st
					     *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_first__initialmeltchunk_46 (struct
					     frame_melt_start_this_module_st
					     *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_first__initialmeltchunk_47 (struct
					     frame_melt_start_this_module_st
					     *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_first__initialmeltchunk_48 (struct
					     frame_melt_start_this_module_st
					     *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_first__initialmeltchunk_49 (struct
					     frame_melt_start_this_module_st
					     *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_first__initialmeltchunk_50 (struct
					     frame_melt_start_this_module_st
					     *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_first__initialmeltchunk_51 (struct
					     frame_melt_start_this_module_st
					     *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_first__initialmeltchunk_52 (struct
					     frame_melt_start_this_module_st
					     *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_first__initialmeltchunk_53 (struct
					     frame_melt_start_this_module_st
					     *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_first__initialmeltchunk_54 (struct
					     frame_melt_start_this_module_st
					     *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_first__initialmeltchunk_55 (struct
					     frame_melt_start_this_module_st
					     *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_first__initialmeltchunk_56 (struct
					     frame_melt_start_this_module_st
					     *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_first__initialmeltchunk_57 (struct
					     frame_melt_start_this_module_st
					     *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_first__initialmeltchunk_58 (struct
					     frame_melt_start_this_module_st
					     *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_first__initialmeltchunk_59 (struct
					     frame_melt_start_this_module_st
					     *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_first__forward_or_mark_module_start_frame (struct
							    melt_callframe_st
							    *fp, int marking);
#define meltmarking_melt_start_this_module  meltmod__warmelt_first__forward_or_mark_module_start_frame



/**** warmelt-first+01.c implementations ****/




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_5_warmelt_first_ADD_NEW_KEYWORD_TOKEN (meltclosure_ptr_t meltclosp_,
						melt_ptr_t meltfirstargp_,
						const melt_argdescr_cell_t
						meltxargdescr_[],
						union meltparam_un
						*meltxargtab_,
						const melt_argdescr_cell_t
						meltxresdescr_[],
						union meltparam_un
						*meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_5_warmelt_first_ADD_NEW_KEYWORD_TOKEN_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_5_warmelt_first_ADD_NEW_KEYWORD_TOKEN_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 9
    melt_ptr_t mcfr_varptr[9];
#define MELTFRAM_NBVARNUM 1
    long mcfr_varnum[1];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_5_warmelt_first_ADD_NEW_KEYWORD_TOKEN is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_5_warmelt_first_ADD_NEW_KEYWORD_TOKEN_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 9; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_5_warmelt_first_ADD_NEW_KEYWORD_TOKEN nbval 9*/
  meltfram__.mcfr_nbvar = 9 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("ADD_NEW_KEYWORD_TOKEN", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-first.melt:2755:/ getarg");
 /*_.SYDA__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.STR__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.STR__V3*/ meltfptr[2])) != NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-first.melt:2756:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L1*/ meltfnum[0] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.SYDA__V2*/ meltfptr[1]),
			     (melt_ptr_t) (( /*!CLASS_SYSTEM_DATA */
					    meltfrout->tabval[0])));;
      MELT_LOCATION ("warmelt-first.melt:2756:/ cond");
      /*cond */ if ( /*_#IS_A__L1*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V5*/ meltfptr[4] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-first.melt:2756:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check syda"),
				  ("warmelt-first.melt")
				  ? ("warmelt-first.melt") : __FILE__,
				  (2756) ? (2756) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V5*/ meltfptr[4] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V4*/ meltfptr[3] = /*_.IFELSE___V5*/ meltfptr[4];;
      /*epilog */

      MELT_LOCATION ("warmelt-first.melt:2756:/ clear");
	     /*clear *//*_#IS_A__L1*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V5*/ meltfptr[4] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V4*/ meltfptr[3] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-first.melt:2757:/ quasiblock");


    /*^checksignal */
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*^rawallocobj */
    /*rawallocobj */
    {
      melt_ptr_t newobj = 0;
      melt_raw_object_create (newobj,
			      (melt_ptr_t) (( /*!CLASS_KEYWORD */ meltfrout->
					     tabval[1])), (3),
			      "CLASS_KEYWORD");
  /*_.INST__V8*/ meltfptr[7] =
	newobj;
    };
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @NAMED_NAME",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V8*/ meltfptr[7])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V8*/ meltfptr[7]), (1),
			  ( /*_.STR__V3*/ meltfptr[2]), "NAMED_NAME");
    ;
    /*^touchobj */

    melt_dbgtrace_written_object ( /*_.INST__V8*/ meltfptr[7],
				  "newly made instance");
    ;
    /*_.KW__V7*/ meltfptr[6] = /*_.INST__V8*/ meltfptr[7];;
    MELT_LOCATION ("warmelt-first.melt:2758:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.SYDA__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 7, "SYSDATA_KEYWDICT");
  /*_.KWDICT__V9*/ meltfptr[8] = slot;
    };
    ;

    {
      MELT_LOCATION ("warmelt-first.melt:2760:/ locexp");
      meltgc_put_mapstrings ((struct meltmapstrings_st *) ( /*_.KWDICT__V9*/
							   meltfptr[8]),
			     melt_string_str ((melt_ptr_t)
					      ( /*_.STR__V3*/ meltfptr[2])),
			     (melt_ptr_t) ( /*_.KW__V7*/ meltfptr[6]));
    }
    ;
    /*_.LET___V6*/ meltfptr[4] = /*_.KW__V7*/ meltfptr[6];;

    MELT_LOCATION ("warmelt-first.melt:2757:/ clear");
	   /*clear *//*_.KW__V7*/ meltfptr[6] = 0;
    /*^clear */
	   /*clear *//*_.KWDICT__V9*/ meltfptr[8] = 0;
    MELT_LOCATION ("warmelt-first.melt:2755:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.LET___V6*/ meltfptr[4];;

    {
      MELT_LOCATION ("warmelt-first.melt:2755:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V4*/ meltfptr[3] = 0;
    /*^clear */
	   /*clear *//*_.LET___V6*/ meltfptr[4] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("ADD_NEW_KEYWORD_TOKEN", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_5_warmelt_first_ADD_NEW_KEYWORD_TOKEN_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_5_warmelt_first_ADD_NEW_KEYWORD_TOKEN */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_6_warmelt_first_INTERN_SYMBOL (meltclosure_ptr_t meltclosp_,
					melt_ptr_t meltfirstargp_,
					const melt_argdescr_cell_t
					meltxargdescr_[],
					union meltparam_un * meltxargtab_,
					const melt_argdescr_cell_t
					meltxresdescr_[],
					union meltparam_un * meltxrestab_)
{
  long current_blocklevel_signals_meltrout_6_warmelt_first_INTERN_SYMBOL_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_6_warmelt_first_INTERN_SYMBOL_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 13
    melt_ptr_t mcfr_varptr[13];
#define MELTFRAM_NBVARNUM 2
    long mcfr_varnum[2];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_6_warmelt_first_INTERN_SYMBOL is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_6_warmelt_first_INTERN_SYMBOL_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 13; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_6_warmelt_first_INTERN_SYMBOL nbval 13*/
  meltfram__.mcfr_nbvar = 13 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("INTERN_SYMBOL", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-first.melt:2764:/ getarg");
 /*_.INIDAT__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.SYMB__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.SYMB__V3*/ meltfptr[2])) !=
	      NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-first.melt:2765:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L1*/ meltfnum[0] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.INIDAT__V2*/ meltfptr[1]),
			     (melt_ptr_t) (( /*!CLASS_SYSTEM_DATA */
					    meltfrout->tabval[0])));;
      MELT_LOCATION ("warmelt-first.melt:2765:/ cond");
      /*cond */ if ( /*_#IS_A__L1*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V5*/ meltfptr[4] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-first.melt:2765:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check inidat"),
				  ("warmelt-first.melt")
				  ? ("warmelt-first.melt") : __FILE__,
				  (2765) ? (2765) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V5*/ meltfptr[4] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V4*/ meltfptr[3] = /*_.IFELSE___V5*/ meltfptr[4];;
      /*epilog */

      MELT_LOCATION ("warmelt-first.melt:2765:/ clear");
	     /*clear *//*_#IS_A__L1*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V5*/ meltfptr[4] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V4*/ meltfptr[3] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-first.melt:2766:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L2*/ meltfnum[0] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.SYMB__V3*/ meltfptr[2]),
			     (melt_ptr_t) (( /*!CLASS_SYMBOL */ meltfrout->
					    tabval[1])));;
      MELT_LOCATION ("warmelt-first.melt:2766:/ cond");
      /*cond */ if ( /*_#IS_A__L2*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V7*/ meltfptr[6] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-first.melt:2766:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check sym"),
				  ("warmelt-first.melt")
				  ? ("warmelt-first.melt") : __FILE__,
				  (2766) ? (2766) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V7*/ meltfptr[6] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V6*/ meltfptr[4] = /*_.IFELSE___V7*/ meltfptr[6];;
      /*epilog */

      MELT_LOCATION ("warmelt-first.melt:2766:/ clear");
	     /*clear *//*_#IS_A__L2*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V7*/ meltfptr[6] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V6*/ meltfptr[4] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-first.melt:2767:/ quasiblock");


    /*^getslot */
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.SYMB__V3*/ meltfptr[2]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 1, "NAMED_NAME");
  /*_.SYNAME__V9*/ meltfptr[8] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-first.melt:2768:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.INIDAT__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 6, "SYSDATA_SYMBOLDICT");
  /*_.SYDICT__V10*/ meltfptr[9] = slot;
    };
    ;
 /*_.OLDSY__V11*/ meltfptr[10] =
      (melt_get_mapstrings
       ((struct meltmapstrings_st *) ( /*_.SYDICT__V10*/ meltfptr[9]),
	melt_string_str ((melt_ptr_t) ( /*_.SYNAME__V9*/ meltfptr[8]))));;
    MELT_LOCATION ("warmelt-first.melt:2770:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^cond */
    /*cond */ if ( /*_.OLDSY__V11*/ meltfptr[10])	/*then */
      {
	/*^cond.then */
	/*_.IFELSE___V12*/ meltfptr[11] = /*_.OLDSY__V11*/ meltfptr[10];;
      }
    else
      {
	MELT_LOCATION ("warmelt-first.melt:2770:/ cond.else");

	/*^block */
	/*anyblock */
	{


	  {
	    MELT_LOCATION ("warmelt-first.melt:2773:/ locexp");
	    meltgc_put_mapstrings ((struct meltmapstrings_st *) ( /*_.SYDICT__V10*/ meltfptr[9]),
				   melt_string_str ((melt_ptr_t)
						    ( /*_.SYNAME__V9*/
						     meltfptr[8])),
				   (melt_ptr_t) ( /*_.SYMB__V3*/
						 meltfptr[2]));
	  }
	  ;
	  MELT_LOCATION ("warmelt-first.melt:2772:/ quasiblock");


	  /*_.PROGN___V13*/ meltfptr[12] = /*_.SYMB__V3*/ meltfptr[2];;
	  /*^compute */
	  /*_.IFELSE___V12*/ meltfptr[11] = /*_.PROGN___V13*/ meltfptr[12];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-first.melt:2770:/ clear");
	     /*clear *//*_.PROGN___V13*/ meltfptr[12] = 0;
	}
	;
      }
    ;
    /*_.LET___V8*/ meltfptr[6] = /*_.IFELSE___V12*/ meltfptr[11];;

    MELT_LOCATION ("warmelt-first.melt:2767:/ clear");
	   /*clear *//*_.SYNAME__V9*/ meltfptr[8] = 0;
    /*^clear */
	   /*clear *//*_.SYDICT__V10*/ meltfptr[9] = 0;
    /*^clear */
	   /*clear *//*_.OLDSY__V11*/ meltfptr[10] = 0;
    /*^clear */
	   /*clear *//*_.IFELSE___V12*/ meltfptr[11] = 0;
    MELT_LOCATION ("warmelt-first.melt:2764:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.LET___V8*/ meltfptr[6];;

    {
      MELT_LOCATION ("warmelt-first.melt:2764:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V4*/ meltfptr[3] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V6*/ meltfptr[4] = 0;
    /*^clear */
	   /*clear *//*_.LET___V8*/ meltfptr[6] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("INTERN_SYMBOL", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_6_warmelt_first_INTERN_SYMBOL_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_6_warmelt_first_INTERN_SYMBOL */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_7_warmelt_first_INTERN_KEYWORD (meltclosure_ptr_t meltclosp_,
					 melt_ptr_t meltfirstargp_,
					 const melt_argdescr_cell_t
					 meltxargdescr_[],
					 union meltparam_un * meltxargtab_,
					 const melt_argdescr_cell_t
					 meltxresdescr_[],
					 union meltparam_un * meltxrestab_)
{
  long current_blocklevel_signals_meltrout_7_warmelt_first_INTERN_KEYWORD_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_7_warmelt_first_INTERN_KEYWORD_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 13
    melt_ptr_t mcfr_varptr[13];
#define MELTFRAM_NBVARNUM 2
    long mcfr_varnum[2];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_7_warmelt_first_INTERN_KEYWORD is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_7_warmelt_first_INTERN_KEYWORD_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 13; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_7_warmelt_first_INTERN_KEYWORD nbval 13*/
  meltfram__.mcfr_nbvar = 13 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("INTERN_KEYWORD", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-first.melt:2779:/ getarg");
 /*_.INIDAT__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.KEYW__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.KEYW__V3*/ meltfptr[2])) !=
	      NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-first.melt:2780:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L1*/ meltfnum[0] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.INIDAT__V2*/ meltfptr[1]),
			     (melt_ptr_t) (( /*!CLASS_SYSTEM_DATA */
					    meltfrout->tabval[0])));;
      MELT_LOCATION ("warmelt-first.melt:2780:/ cond");
      /*cond */ if ( /*_#IS_A__L1*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V5*/ meltfptr[4] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-first.melt:2780:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check inidat"),
				  ("warmelt-first.melt")
				  ? ("warmelt-first.melt") : __FILE__,
				  (2780) ? (2780) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V5*/ meltfptr[4] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V4*/ meltfptr[3] = /*_.IFELSE___V5*/ meltfptr[4];;
      /*epilog */

      MELT_LOCATION ("warmelt-first.melt:2780:/ clear");
	     /*clear *//*_#IS_A__L1*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V5*/ meltfptr[4] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V4*/ meltfptr[3] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-first.melt:2781:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L2*/ meltfnum[0] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.KEYW__V3*/ meltfptr[2]),
			     (melt_ptr_t) (( /*!CLASS_KEYWORD */ meltfrout->
					    tabval[1])));;
      MELT_LOCATION ("warmelt-first.melt:2781:/ cond");
      /*cond */ if ( /*_#IS_A__L2*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V7*/ meltfptr[6] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-first.melt:2781:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check keyw"),
				  ("warmelt-first.melt")
				  ? ("warmelt-first.melt") : __FILE__,
				  (2781) ? (2781) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V7*/ meltfptr[6] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V6*/ meltfptr[4] = /*_.IFELSE___V7*/ meltfptr[6];;
      /*epilog */

      MELT_LOCATION ("warmelt-first.melt:2781:/ clear");
	     /*clear *//*_#IS_A__L2*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V7*/ meltfptr[6] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V6*/ meltfptr[4] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-first.melt:2782:/ quasiblock");


    /*^getslot */
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.KEYW__V3*/ meltfptr[2]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 1, "NAMED_NAME");
  /*_.KWNAME__V9*/ meltfptr[8] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-first.melt:2783:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.INIDAT__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 7, "SYSDATA_KEYWDICT");
  /*_.KWDICT__V10*/ meltfptr[9] = slot;
    };
    ;
 /*_.OLDKW__V11*/ meltfptr[10] =
      (melt_get_mapstrings
       ((struct meltmapstrings_st *) ( /*_.KWDICT__V10*/ meltfptr[9]),
	melt_string_str ((melt_ptr_t) ( /*_.KWNAME__V9*/ meltfptr[8]))));;
    MELT_LOCATION ("warmelt-first.melt:2785:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^cond */
    /*cond */ if ( /*_.OLDKW__V11*/ meltfptr[10])	/*then */
      {
	/*^cond.then */
	/*_.IFELSE___V12*/ meltfptr[11] = /*_.OLDKW__V11*/ meltfptr[10];;
      }
    else
      {
	MELT_LOCATION ("warmelt-first.melt:2785:/ cond.else");

	/*^block */
	/*anyblock */
	{


	  {
	    MELT_LOCATION ("warmelt-first.melt:2787:/ locexp");
	    meltgc_put_mapstrings ((struct meltmapstrings_st *) ( /*_.KWDICT__V10*/ meltfptr[9]),
				   melt_string_str ((melt_ptr_t)
						    ( /*_.KWNAME__V9*/
						     meltfptr[8])),
				   (melt_ptr_t) ( /*_.KEYW__V3*/
						 meltfptr[2]));
	  }
	  ;
	  /*^quasiblock */


	  /*_.PROGN___V13*/ meltfptr[12] = /*_.KEYW__V3*/ meltfptr[2];;
	  /*^compute */
	  /*_.IFELSE___V12*/ meltfptr[11] = /*_.PROGN___V13*/ meltfptr[12];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-first.melt:2785:/ clear");
	     /*clear *//*_.PROGN___V13*/ meltfptr[12] = 0;
	}
	;
      }
    ;
    /*_.LET___V8*/ meltfptr[6] = /*_.IFELSE___V12*/ meltfptr[11];;

    MELT_LOCATION ("warmelt-first.melt:2782:/ clear");
	   /*clear *//*_.KWNAME__V9*/ meltfptr[8] = 0;
    /*^clear */
	   /*clear *//*_.KWDICT__V10*/ meltfptr[9] = 0;
    /*^clear */
	   /*clear *//*_.OLDKW__V11*/ meltfptr[10] = 0;
    /*^clear */
	   /*clear *//*_.IFELSE___V12*/ meltfptr[11] = 0;
    MELT_LOCATION ("warmelt-first.melt:2779:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.LET___V8*/ meltfptr[6];;

    {
      MELT_LOCATION ("warmelt-first.melt:2779:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V4*/ meltfptr[3] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V6*/ meltfptr[4] = 0;
    /*^clear */
	   /*clear *//*_.LET___V8*/ meltfptr[6] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("INTERN_KEYWORD", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_7_warmelt_first_INTERN_KEYWORD_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_7_warmelt_first_INTERN_KEYWORD */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_8_warmelt_first_CLONE_SYMBOL (meltclosure_ptr_t meltclosp_,
				       melt_ptr_t meltfirstargp_,
				       const melt_argdescr_cell_t
				       meltxargdescr_[],
				       union meltparam_un * meltxargtab_,
				       const melt_argdescr_cell_t
				       meltxresdescr_[],
				       union meltparam_un * meltxrestab_)
{
  long current_blocklevel_signals_meltrout_8_warmelt_first_CLONE_SYMBOL_melt =
    melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_8_warmelt_first_CLONE_SYMBOL_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 22
    melt_ptr_t mcfr_varptr[22];
#define MELTFRAM_NBVARNUM 8
    long mcfr_varnum[8];
/*others*/
    const char *loc_CSTRING__o0;
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_8_warmelt_first_CLONE_SYMBOL is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_8_warmelt_first_CLONE_SYMBOL_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 22; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_8_warmelt_first_CLONE_SYMBOL nbval 22*/
  meltfram__.mcfr_nbvar = 22 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("CLONE_SYMBOL", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-first.melt:2795:/ getarg");
 /*_.SYMB__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {

    MELT_LOCATION ("warmelt-first.melt:2797:/ quasiblock");


    /*^getslot */
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj =
	(melt_ptr_t) (( /*!CONTAINER_CLONEMAPSTRING */ meltfrout->
		       tabval[0])) /*=obj*/ ;
      melt_object_get_field (slot, obj, 0, "REFERENCED_VALUE");
  /*_.MAPSTR__V4*/ meltfptr[3] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-first.melt:2800:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#IS_STRING__L1*/ meltfnum[0] =
      (melt_magic_discr ((melt_ptr_t) ( /*_.SYMB__V2*/ meltfptr[1])) ==
       MELTOBMAG_STRING);;
    MELT_LOCATION ("warmelt-first.melt:2800:/ cond");
    /*cond */ if ( /*_#IS_STRING__L1*/ meltfnum[0])	/*then */
      {
	/*^cond.then */
	/*_.SYNAM__V5*/ meltfptr[4] = /*_.SYMB__V2*/ meltfptr[1];;
      }
    else
      {
	MELT_LOCATION ("warmelt-first.melt:2800:/ cond.else");

	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-first.melt:2802:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
   /*_#IS_A__L2*/ meltfnum[1] =
	    melt_is_instance_of ((melt_ptr_t) ( /*_.SYMB__V2*/ meltfptr[1]),
				 (melt_ptr_t) (( /*!CLASS_NAMED */ meltfrout->
						tabval[1])));;
	  MELT_LOCATION ("warmelt-first.melt:2802:/ cond");
	  /*cond */ if ( /*_#IS_A__L2*/ meltfnum[1])	/*then */
	    {
	      /*^cond.then */
	      /*^block */
	      /*anyblock */
	      {

		MELT_LOCATION ("warmelt-first.melt:2803:/ getslot");
		{
		  melt_ptr_t slot = NULL, obj = NULL;
		  obj = (melt_ptr_t) ( /*_.SYMB__V2*/ meltfptr[1]) /*=obj*/ ;
		  melt_object_get_field (slot, obj, 1, "NAMED_NAME");
      /*_.NAMED_NAME__V7*/ meltfptr[6] = slot;
		};
		;
		/*_.IFELSE___V6*/ meltfptr[5] =
		  /*_.NAMED_NAME__V7*/ meltfptr[6];;
		/*epilog */

		MELT_LOCATION ("warmelt-first.melt:2802:/ clear");
	       /*clear *//*_.NAMED_NAME__V7*/ meltfptr[6] = 0;
	      }
	      ;
	    }
	  else
	    {			/*^cond.else */

	      /*^block */
	      /*anyblock */
	      {

		MELT_LOCATION ("warmelt-first.melt:2805:/ quasiblock");


     /*_.DISCRIM__V9*/ meltfptr[8] =
		  ((melt_ptr_t)
		   (melt_discr
		    ((melt_ptr_t) ( /*_.SYMB__V2*/ meltfptr[1]))));;
		MELT_LOCATION ("warmelt-first.melt:2805:/ cond");
		/*cond */ if (
			       /*ifisa */
			       melt_is_instance_of ((melt_ptr_t)
						    ( /*_.DISCRIM__V9*/
						     meltfptr[8]),
						    (melt_ptr_t) (( /*!CLASS_NAMED */ meltfrout->tabval[1])))
		  )		/*then */
		  {
		    /*^cond.then */
		    /*^getslot */
		    {
		      melt_ptr_t slot = NULL, obj = NULL;
		      obj =
			(melt_ptr_t) ( /*_.DISCRIM__V9*/ meltfptr[8]) /*=obj*/
			;
		      melt_object_get_field (slot, obj, 1, "NAMED_NAME");
       /*_.DISCRINAM__V10*/ meltfptr[9] = slot;
		    };
		    ;
		  }
		else
		  {		/*^cond.else */

      /*_.DISCRINAM__V10*/ meltfptr[9] = NULL;;
		  }
		;

		{
		  MELT_LOCATION ("warmelt-first.melt:2807:/ locexp");
		  warning (0, "MELT WARNING MSG [#%ld]::: %s - %s",
			   melt_dbgcounter,
			   ("clone_symbol got invalid argument of discriminant"),
			   melt_string_str ((melt_ptr_t)
					    ( /*_.DISCRINAM__V10*/
					     meltfptr[9])));
		}
		;

		{
		  MELT_LOCATION ("warmelt-first.melt:2809:/ locexp");

#if MELT_HAVE_DEBUG
		  if (melt_need_debug (0))
		    melt_dbgshortbacktrace (("clone_symbol error.."), (15));
#endif
		  ;
		}
		;
		MELT_LOCATION ("warmelt-first.melt:2810:/ quasiblock");


     /*_?*/ meltfram__.loc_CSTRING__o0 =
		  (char *) 0;;

		{
		  MELT_LOCATION ("warmelt-first.melt:2813:/ locexp");
		  /* clone_symbol CLONAMSTR__1 */
		  {
		    static char clonambuf_CLONAMSTR__1[100];
		    const char *s =
		      melt_string_str ((melt_ptr_t) /*_.DISCRINAM__V10*/
				       meltfptr[9]);
		    if (s)
		      s = strchr (s, '_');
		    if (!s)
		      s = "_What";
		    memset (clonambuf_CLONAMSTR__1, 0,
			    sizeof (clonambuf_CLONAMSTR__1));
		    snprintf (clonambuf_CLONAMSTR__1,
			      sizeof (clonambuf_CLONAMSTR__1),
			      "Cloned_Melt%s", s);
		      /*_?*/ meltfram__.loc_CSTRING__o0 =
		      clonambuf_CLONAMSTR__1;
		  }		/* end clone_symbol CLONAMSTR__1 */
		  ;
		}
		;
     /*_.MAKE_STRINGCONST__V12*/ meltfptr[11] =
		  (meltgc_new_stringdup
		   ((meltobject_ptr_t)
		    (( /*!DISCR_STRING */ meltfrout->tabval[2])),
		    ( /*_?*/ meltfram__.loc_CSTRING__o0)));;
		/*^compute */
		/*_.LET___V11*/ meltfptr[10] =
		  /*_.MAKE_STRINGCONST__V12*/ meltfptr[11];;

		MELT_LOCATION ("warmelt-first.melt:2810:/ clear");
	       /*clear *//*_?*/ meltfram__.loc_CSTRING__o0 = 0;
		/*^clear */
	       /*clear *//*_.MAKE_STRINGCONST__V12*/ meltfptr[11] = 0;
		/*_.LET___V8*/ meltfptr[6] = /*_.LET___V11*/ meltfptr[10];;

		MELT_LOCATION ("warmelt-first.melt:2805:/ clear");
	       /*clear *//*_.DISCRIM__V9*/ meltfptr[8] = 0;
		/*^clear */
	       /*clear *//*_.DISCRINAM__V10*/ meltfptr[9] = 0;
		/*^clear */
	       /*clear *//*_.LET___V11*/ meltfptr[10] = 0;
		MELT_LOCATION ("warmelt-first.melt:2804:/ quasiblock");


		/*_.PROGN___V13*/ meltfptr[11] = /*_.LET___V8*/ meltfptr[6];;
		/*^compute */
		/*_.IFELSE___V6*/ meltfptr[5] =
		  /*_.PROGN___V13*/ meltfptr[11];;
		/*epilog */

		MELT_LOCATION ("warmelt-first.melt:2802:/ clear");
	       /*clear *//*_.LET___V8*/ meltfptr[6] = 0;
		/*^clear */
	       /*clear *//*_.PROGN___V13*/ meltfptr[11] = 0;
	      }
	      ;
	    }
	  ;
	  /*_.SYNAM__V5*/ meltfptr[4] = /*_.IFELSE___V6*/ meltfptr[5];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-first.melt:2800:/ clear");
	     /*clear *//*_#IS_A__L2*/ meltfnum[1] = 0;
	  /*^clear */
	     /*clear *//*_.IFELSE___V6*/ meltfptr[5] = 0;
	}
	;
      }
    ;
 /*_.BOXI__V14*/ meltfptr[8] =
      (melt_get_mapstrings
       ((struct meltmapstrings_st *) ( /*_.MAPSTR__V4*/ meltfptr[3]),
	melt_string_str ((melt_ptr_t) ( /*_.SYNAM__V5*/ meltfptr[4]))));;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-first.melt:2829:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_STRING__L3*/ meltfnum[1] =
	(melt_magic_discr ((melt_ptr_t) ( /*_.SYNAM__V5*/ meltfptr[4])) ==
	 MELTOBMAG_STRING);;
      MELT_LOCATION ("warmelt-first.melt:2829:/ cond");
      /*cond */ if ( /*_#IS_STRING__L3*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V16*/ meltfptr[10] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-first.melt:2829:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check synam"),
				  ("warmelt-first.melt")
				  ? ("warmelt-first.melt") : __FILE__,
				  (2829) ? (2829) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V16*/ meltfptr[10] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V15*/ meltfptr[9] = /*_.IFELSE___V16*/ meltfptr[10];;
      /*epilog */

      MELT_LOCATION ("warmelt-first.melt:2829:/ clear");
	     /*clear *//*_#IS_STRING__L3*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V16*/ meltfptr[10] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V15*/ meltfptr[9] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-first.melt:2830:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#IS_INTEGERBOX__L4*/ meltfnum[1] =
      (melt_magic_discr ((melt_ptr_t) ( /*_.BOXI__V14*/ meltfptr[8])) ==
       MELTOBMAG_INT);;
    /*^compute */
 /*_#NOT__L5*/ meltfnum[4] =
      (!( /*_#IS_INTEGERBOX__L4*/ meltfnum[1]));;
    MELT_LOCATION ("warmelt-first.melt:2830:/ cond");
    /*cond */ if ( /*_#NOT__L5*/ meltfnum[4])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

   /*_.MAKE_INTEGERBOX__V17*/ meltfptr[6] =
	    (meltgc_new_int
	     ((meltobject_ptr_t)
	      (( /*!DISCR_INTEGER */ meltfrout->tabval[3])), (0)));;
	  MELT_LOCATION ("warmelt-first.melt:2832:/ compute");
	  /*_.BOXI__V14*/ meltfptr[8] = /*_.SETQ___V18*/ meltfptr[11] =
	    /*_.MAKE_INTEGERBOX__V17*/ meltfptr[6];;

	  {
	    MELT_LOCATION ("warmelt-first.melt:2833:/ locexp");
	    meltgc_put_mapstrings ((struct meltmapstrings_st *) ( /*_.MAPSTR__V4*/ meltfptr[3]),
				   melt_string_str ((melt_ptr_t)
						    ( /*_.SYNAM__V5*/
						     meltfptr[4])),
				   (melt_ptr_t) ( /*_.BOXI__V14*/
						 meltfptr[8]));
	  }
	  ;
	  MELT_LOCATION ("warmelt-first.melt:2831:/ quasiblock");


	  /*epilog */

	  MELT_LOCATION ("warmelt-first.melt:2830:/ clear");
	     /*clear *//*_.MAKE_INTEGERBOX__V17*/ meltfptr[6] = 0;
	  /*^clear */
	     /*clear *//*_.SETQ___V18*/ meltfptr[11] = 0;
	}
	;
      }				/*noelse */
    ;
    MELT_LOCATION ("warmelt-first.melt:2834:/ quasiblock");


 /*_#I__L6*/ meltfnum[5] =
      (melt_get_int ((melt_ptr_t) ( /*_.BOXI__V14*/ meltfptr[8])));;
    /*^compute */
 /*_#I__L7*/ meltfnum[6] =
      (( /*_#I__L6*/ meltfnum[5]) + (1));;
    MELT_LOCATION ("warmelt-first.melt:2835:/ compute");
    /*_#I__L6*/ meltfnum[5] = /*_#SETQ___L8*/ meltfnum[7] =
      /*_#I__L7*/ meltfnum[6];;

    {
      MELT_LOCATION ("warmelt-first.melt:2836:/ locexp");
      melt_put_int ((melt_ptr_t) ( /*_.BOXI__V14*/ meltfptr[8]),
		    ( /*_#I__L6*/ meltfnum[5]));
    }
    ;
    MELT_LOCATION ("warmelt-first.melt:2837:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_.MAKE_INTEGERBOX__V20*/ meltfptr[10] =
      (meltgc_new_int
       ((meltobject_ptr_t) (( /*!DISCR_INTEGER */ meltfrout->tabval[3])),
	( /*_#I__L6*/ meltfnum[5])));;
    MELT_LOCATION ("warmelt-first.melt:2837:/ quasiblock");


    /*^rawallocobj */
    /*rawallocobj */
    {
      melt_ptr_t newobj = 0;
      melt_raw_object_create (newobj,
			      (melt_ptr_t) (( /*!CLASS_CLONED_SYMBOL */
					     meltfrout->tabval[4])), (4),
			      "CLASS_CLONED_SYMBOL");
  /*_.INST__V22*/ meltfptr[11] =
	newobj;
    };
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @NAMED_NAME",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V22*/ meltfptr[11])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V22*/ meltfptr[11]), (1),
			  ( /*_.SYNAM__V5*/ meltfptr[4]), "NAMED_NAME");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @CSYM_URANK",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V22*/ meltfptr[11])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V22*/ meltfptr[11]), (3),
			  ( /*_.MAKE_INTEGERBOX__V20*/ meltfptr[10]),
			  "CSYM_URANK");
    ;
    /*^touchobj */

    melt_dbgtrace_written_object ( /*_.INST__V22*/ meltfptr[11],
				  "newly made instance");
    ;
    /*_.INST___V21*/ meltfptr[6] = /*_.INST__V22*/ meltfptr[11];;
    /*^compute */
    /*_.LET___V19*/ meltfptr[5] = /*_.INST___V21*/ meltfptr[6];;

    MELT_LOCATION ("warmelt-first.melt:2834:/ clear");
	   /*clear *//*_#I__L6*/ meltfnum[5] = 0;
    /*^clear */
	   /*clear *//*_#I__L7*/ meltfnum[6] = 0;
    /*^clear */
	   /*clear *//*_#SETQ___L8*/ meltfnum[7] = 0;
    /*^clear */
	   /*clear *//*_.MAKE_INTEGERBOX__V20*/ meltfptr[10] = 0;
    /*^clear */
	   /*clear *//*_.INST___V21*/ meltfptr[6] = 0;
    /*_.LET___V3*/ meltfptr[2] = /*_.LET___V19*/ meltfptr[5];;

    MELT_LOCATION ("warmelt-first.melt:2797:/ clear");
	   /*clear *//*_.MAPSTR__V4*/ meltfptr[3] = 0;
    /*^clear */
	   /*clear *//*_#IS_STRING__L1*/ meltfnum[0] = 0;
    /*^clear */
	   /*clear *//*_.SYNAM__V5*/ meltfptr[4] = 0;
    /*^clear */
	   /*clear *//*_.BOXI__V14*/ meltfptr[8] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V15*/ meltfptr[9] = 0;
    /*^clear */
	   /*clear *//*_#IS_INTEGERBOX__L4*/ meltfnum[1] = 0;
    /*^clear */
	   /*clear *//*_#NOT__L5*/ meltfnum[4] = 0;
    /*^clear */
	   /*clear *//*_.LET___V19*/ meltfptr[5] = 0;
    MELT_LOCATION ("warmelt-first.melt:2795:/ quasiblock");


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.LET___V3*/ meltfptr[2];;

    {
      MELT_LOCATION ("warmelt-first.melt:2795:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.LET___V3*/ meltfptr[2] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("CLONE_SYMBOL", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_8_warmelt_first_CLONE_SYMBOL_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_8_warmelt_first_CLONE_SYMBOL */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_9_warmelt_first_INITFRESH_CONTENVMAKER (meltclosure_ptr_t meltclosp_,
						 melt_ptr_t meltfirstargp_,
						 const melt_argdescr_cell_t
						 meltxargdescr_[],
						 union meltparam_un *
						 meltxargtab_,
						 const melt_argdescr_cell_t
						 meltxresdescr_[],
						 union meltparam_un *
						 meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_9_warmelt_first_INITFRESH_CONTENVMAKER_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_9_warmelt_first_INITFRESH_CONTENVMAKER_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 9
    melt_ptr_t mcfr_varptr[9];
#define MELTFRAM_NBVARNUM 1
    long mcfr_varnum[1];
/*others*/
    const char *loc_CSTRING__o0;
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_9_warmelt_first_INITFRESH_CONTENVMAKER is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_9_warmelt_first_INITFRESH_CONTENVMAKER_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 9; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_9_warmelt_first_INITFRESH_CONTENVMAKER nbval 9*/
  meltfram__.mcfr_nbvar = 9 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("INITFRESH_CONTENVMAKER", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-first.melt:2846:/ getarg");
 /*_.PREVENV__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_CSTRING)
    goto lab_endgetargs;
 /*_?*/ meltfram__.loc_CSTRING__o0 = meltxargtab_[0].meltbp_cstring;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {

    MELT_LOCATION ("warmelt-first.melt:2847:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#MELT_NEED_DBG__L1*/ meltfnum[0] =
      /*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
      ( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
      0				/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
      ;;
    MELT_LOCATION ("warmelt-first.melt:2847:/ cond");
    /*cond */ if ( /*_#MELT_NEED_DBG__L1*/ meltfnum[0])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{


	  {
	    MELT_LOCATION ("warmelt-first.melt:2848:/ locexp");

#if MELT_HAVE_DEBUG
	    if (melt_need_debug (0))
	      melt_dbgshortbacktrace (("initfresh_contenvmaker"), (15));
#endif
	    ;
	  }
	  ;
	  /*epilog */
	}
	;
      }				/*noelse */
    ;
    MELT_LOCATION ("warmelt-first.melt:2849:/ quasiblock");


    MELT_LOCATION ("warmelt-first.melt:2850:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^cond */
    /*cond */ if ( /*_?*/ meltfram__.loc_CSTRING__o0)	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

   /*_.MAKE_STRINGCONST__V5*/ meltfptr[4] =
	    (meltgc_new_stringdup
	     ((meltobject_ptr_t) (( /*!DISCR_STRING */ meltfrout->tabval[0])),
	      ( /*_?*/ meltfram__.loc_CSTRING__o0)));;
	  /*^compute */
	  /*_.DESCR__V4*/ meltfptr[3] =
	    /*_.MAKE_STRINGCONST__V5*/ meltfptr[4];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-first.melt:2850:/ clear");
	     /*clear *//*_.MAKE_STRINGCONST__V5*/ meltfptr[4] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

  /*_.DESCR__V4*/ meltfptr[3] = NULL;;
      }
    ;
    MELT_LOCATION ("warmelt-first.melt:2851:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.DESCR__V4*/ meltfptr[3];
      /*_.NEWENV__V6*/ meltfptr[4] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!FRESH_ENV */ meltfrout->tabval[1])),
		    (melt_ptr_t) ( /*_.PREVENV__V2*/ meltfptr[1]),
		    (MELTBPARSTR_PTR ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-first.melt:2852:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*^rawallocobj */
    /*rawallocobj */
    {
      melt_ptr_t newobj = 0;
      melt_raw_object_create (newobj,
			      (melt_ptr_t) (( /*!CLASS_REFERENCE */
					     meltfrout->tabval[2])), (1),
			      "CLASS_REFERENCE");
  /*_.INST__V8*/ meltfptr[7] =
	newobj;
    };
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @REFERENCED_VALUE",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V8*/ meltfptr[7])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V8*/ meltfptr[7]), (0),
			  ( /*_.NEWENV__V6*/ meltfptr[4]),
			  "REFERENCED_VALUE");
    ;
    /*^touchobj */

    melt_dbgtrace_written_object ( /*_.INST__V8*/ meltfptr[7],
				  "newly made instance");
    ;
    /*_.NEWCONT__V7*/ meltfptr[6] = /*_.INST__V8*/ meltfptr[7];;
    MELT_LOCATION ("warmelt-first.melt:2855:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.NEWCONT__V7*/ meltfptr[6];;

    {
      MELT_LOCATION ("warmelt-first.melt:2855:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*_.LET___V3*/ meltfptr[2] = /*_.RETURN___V9*/ meltfptr[8];;

    MELT_LOCATION ("warmelt-first.melt:2849:/ clear");
	   /*clear *//*_.DESCR__V4*/ meltfptr[3] = 0;
    /*^clear */
	   /*clear *//*_.NEWENV__V6*/ meltfptr[4] = 0;
    /*^clear */
	   /*clear *//*_.NEWCONT__V7*/ meltfptr[6] = 0;
    /*^clear */
	   /*clear *//*_.RETURN___V9*/ meltfptr[8] = 0;
    MELT_LOCATION ("warmelt-first.melt:2846:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.LET___V3*/ meltfptr[2];;

    {
      MELT_LOCATION ("warmelt-first.melt:2846:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_#MELT_NEED_DBG__L1*/ meltfnum[0] = 0;
    /*^clear */
	   /*clear *//*_.LET___V3*/ meltfptr[2] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("INITFRESH_CONTENVMAKER", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_9_warmelt_first_INITFRESH_CONTENVMAKER_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_9_warmelt_first_INITFRESH_CONTENVMAKER */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_10_warmelt_first_INITVALUE_EXPORTER (meltclosure_ptr_t meltclosp_,
					      melt_ptr_t meltfirstargp_,
					      const melt_argdescr_cell_t
					      meltxargdescr_[],
					      union meltparam_un *
					      meltxargtab_,
					      const melt_argdescr_cell_t
					      meltxresdescr_[],
					      union meltparam_un *
					      meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_10_warmelt_first_INITVALUE_EXPORTER_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_10_warmelt_first_INITVALUE_EXPORTER_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 64
    melt_ptr_t mcfr_varptr[64];
#define MELTFRAM_NBVARNUM 30
    long mcfr_varnum[30];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_10_warmelt_first_INITVALUE_EXPORTER is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_10_warmelt_first_INITVALUE_EXPORTER_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 64; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_10_warmelt_first_INITVALUE_EXPORTER nbval 64*/
  meltfram__.mcfr_nbvar = 64 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("INITVALUE_EXPORTER", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-first.melt:2859:/ getarg");
 /*_.SYM__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.VAL__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.VAL__V3*/ meltfptr[2])) != NULL);


  /*getarg#2 */
  /*^getarg */
  if (meltxargdescr_[1] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.CONTENV__V4*/ meltfptr[3] =
    (meltxargtab_[1].meltbp_aptr) ? (*(meltxargtab_[1].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.CONTENV__V4*/ meltfptr[3])) !=
	      NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {

    MELT_LOCATION ("warmelt-first.melt:2860:/ quasiblock");


    /*_.PARENV__V6*/ meltfptr[5] = ( /*!konst_0 */ meltfrout->tabval[0]);;
    MELT_LOCATION ("warmelt-first.melt:2861:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#NULL__L1*/ meltfnum[0] =
      (( /*_.CONTENV__V4*/ meltfptr[3]) == NULL);;
    MELT_LOCATION ("warmelt-first.melt:2861:/ cond");
    /*cond */ if ( /*_#NULL__L1*/ meltfnum[0])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-first.melt:2863:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^cond */
	  /*cond */ if ( /*_.PARENV__V6*/ meltfptr[5])	/*then */
	    {
	      /*^cond.then */
	      /*^block */
	      /*anyblock */
	      {

		MELT_LOCATION ("warmelt-first.melt:2864:/ getslot");
		{
		  melt_ptr_t slot = NULL, obj = NULL;
		  obj = (melt_ptr_t) ( /*_.SYM__V2*/ meltfptr[1]) /*=obj*/ ;
		  melt_object_get_field (slot, obj, 1, "NAMED_NAME");
      /*_.NAMED_NAME__V8*/ meltfptr[7] = slot;
		};
		;

		{
		  /*^locexp */
		  warning (0, "MELT WARNING MSG [#%ld]::: %s - %s",
			   melt_dbgcounter,
			   ("exporting value too early with null environment container"),
			   melt_string_str ((melt_ptr_t)
					    ( /*_.NAMED_NAME__V8*/
					     meltfptr[7])));
		}
		;
		/*epilog */

		MELT_LOCATION ("warmelt-first.melt:2863:/ clear");
	       /*clear *//*_.NAMED_NAME__V8*/ meltfptr[7] = 0;
	      }
	      ;
	    }			/*noelse */
	  ;
	  MELT_LOCATION ("warmelt-first.melt:2865:/ quasiblock");


   /*_.RETVAL___V1*/ meltfptr[0] = NULL;;

	  {
	    MELT_LOCATION ("warmelt-first.melt:2865:/ locexp");
	    /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
	    if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	      melt_warn_for_no_expected_secondary_results ();
	    /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
	    ;
	  }
	  ;
	  /*^finalreturn */
	  ;
	  /*finalret */ goto labend_rout;
	  MELT_LOCATION ("warmelt-first.melt:2862:/ quasiblock");


	  /*_.PROGN___V10*/ meltfptr[9] = /*_.RETURN___V9*/ meltfptr[7];;
	  /*^compute */
	  /*_.IF___V7*/ meltfptr[6] = /*_.PROGN___V10*/ meltfptr[9];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-first.melt:2861:/ clear");
	     /*clear *//*_.RETURN___V9*/ meltfptr[7] = 0;
	  /*^clear */
	     /*clear *//*_.PROGN___V10*/ meltfptr[9] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

  /*_.IF___V7*/ meltfptr[6] = NULL;;
      }
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-first.melt:2867:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L2*/ meltfnum[1] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.SYM__V2*/ meltfptr[1]),
			     (melt_ptr_t) (( /*!CLASS_SYMBOL */ meltfrout->
					    tabval[1])));;
      MELT_LOCATION ("warmelt-first.melt:2867:/ cond");
      /*cond */ if ( /*_#IS_A__L2*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V12*/ meltfptr[9] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-first.melt:2867:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check sym"),
				  ("warmelt-first.melt")
				  ? ("warmelt-first.melt") : __FILE__,
				  (2867) ? (2867) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V12*/ meltfptr[9] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V11*/ meltfptr[7] = /*_.IFELSE___V12*/ meltfptr[9];;
      /*epilog */

      MELT_LOCATION ("warmelt-first.melt:2867:/ clear");
	     /*clear *//*_#IS_A__L2*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V12*/ meltfptr[9] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V11*/ meltfptr[7] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-first.melt:2868:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L3*/ meltfnum[1] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.CONTENV__V4*/ meltfptr[3]),
			     (melt_ptr_t) (( /*!CLASS_REFERENCE */ meltfrout->
					    tabval[2])));;
      MELT_LOCATION ("warmelt-first.melt:2868:/ cond");
      /*cond */ if ( /*_#IS_A__L3*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V14*/ meltfptr[13] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-first.melt:2868:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check contenv"),
				  ("warmelt-first.melt")
				  ? ("warmelt-first.melt") : __FILE__,
				  (2868) ? (2868) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V14*/ meltfptr[13] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V13*/ meltfptr[9] = /*_.IFELSE___V14*/ meltfptr[13];;
      /*epilog */

      MELT_LOCATION ("warmelt-first.melt:2868:/ clear");
	     /*clear *//*_#IS_A__L3*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V14*/ meltfptr[13] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V13*/ meltfptr[9] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-first.melt:2869:/ quasiblock");


    /*^getslot */
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.CONTENV__V4*/ meltfptr[3]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 0, "REFERENCED_VALUE");
  /*_.ENV__V16*/ meltfptr[15] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-first.melt:2870:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^checksignal */
    MELT_CHECK_SIGNAL ();
    ;
 /*_#NULL__L4*/ meltfnum[1] =
      (( /*_.ENV__V16*/ meltfptr[15]) == NULL);;
    MELT_LOCATION ("warmelt-first.melt:2870:/ cond");
    /*cond */ if ( /*_#NULL__L4*/ meltfnum[1])	/*then */
      {
	/*^cond.then */
	/*_.IF___V17*/ meltfptr[16] = /*_.PARENV__V6*/ meltfptr[5];;
      }
    else
      {
	MELT_LOCATION ("warmelt-first.melt:2870:/ cond.else");

  /*_.IF___V17*/ meltfptr[16] = NULL;;
      }
    ;
    MELT_LOCATION ("warmelt-first.melt:2870:/ cond");
    /*cond */ if ( /*_.IF___V17*/ meltfptr[16])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-first.melt:2872:/ getslot");
	  {
	    melt_ptr_t slot = NULL, obj = NULL;
	    obj = (melt_ptr_t) ( /*_.SYM__V2*/ meltfptr[1]) /*=obj*/ ;
	    melt_object_get_field (slot, obj, 1, "NAMED_NAME");
    /*_.NAMED_NAME__V19*/ meltfptr[18] = slot;
	  };
	  ;

	  {
	    /*^locexp */
	    inform (UNKNOWN_LOCATION, ("MELT INFORM [#%ld]: %s - %s"),
		    melt_dbgcounter,
		    ("exporting value too early with null environment"),
		    melt_string_str ((melt_ptr_t)
				     ( /*_.NAMED_NAME__V19*/ meltfptr[18])));
	  }
	  ;
	  MELT_LOCATION ("warmelt-first.melt:2873:/ quasiblock");


   /*_.RETVAL___V1*/ meltfptr[0] = NULL;;

	  {
	    MELT_LOCATION ("warmelt-first.melt:2873:/ locexp");
	    /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
	    if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	      melt_warn_for_no_expected_secondary_results ();
	    /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
	    ;
	  }
	  ;
	  /*^finalreturn */
	  ;
	  /*finalret */ goto labend_rout;
	  MELT_LOCATION ("warmelt-first.melt:2871:/ quasiblock");


	  /*_.PROGN___V21*/ meltfptr[20] = /*_.RETURN___V20*/ meltfptr[19];;
	  /*^compute */
	  /*_.IF___V18*/ meltfptr[17] = /*_.PROGN___V21*/ meltfptr[20];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-first.melt:2870:/ clear");
	     /*clear *//*_.NAMED_NAME__V19*/ meltfptr[18] = 0;
	  /*^clear */
	     /*clear *//*_.RETURN___V20*/ meltfptr[19] = 0;
	  /*^clear */
	     /*clear *//*_.PROGN___V21*/ meltfptr[20] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

  /*_.IF___V18*/ meltfptr[17] = NULL;;
      }
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-first.melt:2875:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L5*/ meltfnum[4] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.ENV__V16*/ meltfptr[15]),
			     (melt_ptr_t) (( /*!CLASS_ENVIRONMENT */
					    meltfrout->tabval[3])));;
      MELT_LOCATION ("warmelt-first.melt:2875:/ cond");
      /*cond */ if ( /*_#IS_A__L5*/ meltfnum[4])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V23*/ meltfptr[19] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-first.melt:2875:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check good env"),
				  ("warmelt-first.melt")
				  ? ("warmelt-first.melt") : __FILE__,
				  (2875) ? (2875) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V23*/ meltfptr[19] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V22*/ meltfptr[18] = /*_.IFELSE___V23*/ meltfptr[19];;
      /*epilog */

      MELT_LOCATION ("warmelt-first.melt:2875:/ clear");
	     /*clear *//*_#IS_A__L5*/ meltfnum[4] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V23*/ meltfptr[19] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V22*/ meltfptr[18] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-first.melt:2876:/ quasiblock");


    MELT_LOCATION ("warmelt-first.melt:2877:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^cond */
    /*cond */ if ( /*_.PARENV__V6*/ meltfptr[5])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  /*^checksignal */
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^apply */
	  /*apply */
	  {
	    union meltparam_un argtab[1];
	    memset (&argtab, 0, sizeof (argtab));
	    /*^apply.arg */
	    argtab[0].meltbp_aptr =
	      (melt_ptr_t *) & /*_.SYM__V2*/ meltfptr[1];
	    /*_.FIND_ENV__V26*/ meltfptr[25] =
	      melt_apply ((meltclosure_ptr_t)
			  (( /*!FIND_ENV */ meltfrout->tabval[4])),
			  (melt_ptr_t) ( /*_.PARENV__V6*/ meltfptr[5]),
			  (MELTBPARSTR_PTR ""), argtab, "",
			  (union meltparam_un *) 0);
	  }
	  ;
	  /*_.PREVBIND__V25*/ meltfptr[19] =
	    /*_.FIND_ENV__V26*/ meltfptr[25];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-first.melt:2877:/ clear");
	     /*clear *//*_.FIND_ENV__V26*/ meltfptr[25] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

  /*_.PREVBIND__V25*/ meltfptr[19] = NULL;;
      }
    ;
    MELT_LOCATION ("warmelt-first.melt:2878:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*^rawallocobj */
    /*rawallocobj */
    {
      melt_ptr_t newobj = 0;
      melt_raw_object_create (newobj,
			      (melt_ptr_t) (( /*!CLASS_VALUE_BINDING */
					     meltfrout->tabval[5])), (2),
			      "CLASS_VALUE_BINDING");
  /*_.INST__V28*/ meltfptr[27] =
	newobj;
    };
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @BINDER",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V28*/ meltfptr[27])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V28*/ meltfptr[27]), (0),
			  ( /*_.SYM__V2*/ meltfptr[1]), "BINDER");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @VBIND_VALUE",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V28*/ meltfptr[27])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V28*/ meltfptr[27]), (1),
			  ( /*_.VAL__V3*/ meltfptr[2]), "VBIND_VALUE");
    ;
    /*^touchobj */

    melt_dbgtrace_written_object ( /*_.INST__V28*/ meltfptr[27],
				  "newly made instance");
    ;
    /*_.VALBIND__V27*/ meltfptr[25] = /*_.INST__V28*/ meltfptr[27];;
    MELT_LOCATION ("warmelt-first.melt:2883:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.SYM__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 1, "NAMED_NAME");
  /*_.SYMNAM__V29*/ meltfptr[28] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-first.melt:2886:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#NULL__L6*/ meltfnum[4] =
      (( /*_.PREVBIND__V25*/ meltfptr[19]) == NULL);;
    MELT_LOCATION ("warmelt-first.melt:2886:/ cond");
    /*cond */ if ( /*_#NULL__L6*/ meltfnum[4])	/*then */
      {
	/*^cond.then */
	/*_.IFELSE___V30*/ meltfptr[29] = ( /*nil */ NULL);;
      }
    else
      {
	MELT_LOCATION ("warmelt-first.melt:2886:/ cond.else");

	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-first.melt:2887:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^checksignal */
	  MELT_CHECK_SIGNAL ();
	  ;
   /*_#IS_A__L7*/ meltfnum[6] =
	    melt_is_instance_of ((melt_ptr_t)
				 ( /*_.PREVBIND__V25*/ meltfptr[19]),
				 (melt_ptr_t) (( /*!CLASS_SELECTOR_BINDING */
						meltfrout->tabval[6])));;
	  MELT_LOCATION ("warmelt-first.melt:2887:/ cond");
	  /*cond */ if ( /*_#IS_A__L7*/ meltfnum[6])	/*then */
	    {
	      /*^cond.then */
	      /*^block */
	      /*anyblock */
	      {

     /*_#IS_A__L9*/ meltfnum[8] =
		  melt_is_instance_of ((melt_ptr_t)
				       ( /*_.VAL__V3*/ meltfptr[2]),
				       (melt_ptr_t) (( /*!CLASS_SELECTOR */
						      meltfrout->
						      tabval[7])));;
		/*^compute */
		/*_#IF___L8*/ meltfnum[7] = /*_#IS_A__L9*/ meltfnum[8];;
		/*epilog */

		MELT_LOCATION ("warmelt-first.melt:2887:/ clear");
	       /*clear *//*_#IS_A__L9*/ meltfnum[8] = 0;
	      }
	      ;
	    }
	  else
	    {			/*^cond.else */

    /*_#IF___L8*/ meltfnum[7] = 0;;
	    }
	  ;
	  MELT_LOCATION ("warmelt-first.melt:2887:/ cond");
	  /*cond */ if ( /*_#IF___L8*/ meltfnum[7])	/*then */
	    {
	      /*^cond.then */
	      /*^block */
	      /*anyblock */
	      {


		{
		  MELT_LOCATION ("warmelt-first.melt:2890:/ locexp");
		  warning (0, "MELT WARNING MSG [#%ld]::: %s - %s",
			   melt_dbgcounter,
			   ("not exporting previous bound selector"),
			   melt_string_str ((melt_ptr_t)
					    ( /*_.SYMNAM__V29*/
					     meltfptr[28])));
		}
		;
		MELT_LOCATION ("warmelt-first.melt:2891:/ quasiblock");


     /*_.RETVAL___V1*/ meltfptr[0] = NULL;;

		{
		  MELT_LOCATION ("warmelt-first.melt:2891:/ locexp");
		  /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
		  if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
		    melt_warn_for_no_expected_secondary_results ();
		  /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
		  ;
		}
		;
		/*^finalreturn */
		;
		/*finalret */ goto labend_rout;
		MELT_LOCATION ("warmelt-first.melt:2887:/ quasiblock");


		/*_.PROGN___V33*/ meltfptr[32] =
		  /*_.RETURN___V32*/ meltfptr[31];;
		/*^compute */
		/*_.IFELSE___V31*/ meltfptr[30] =
		  /*_.PROGN___V33*/ meltfptr[32];;
		/*epilog */

		MELT_LOCATION ("warmelt-first.melt:2887:/ clear");
	       /*clear *//*_.RETURN___V32*/ meltfptr[31] = 0;
		/*^clear */
	       /*clear *//*_.PROGN___V33*/ meltfptr[32] = 0;
	      }
	      ;
	    }
	  else
	    {			/*^cond.else */

	      /*^block */
	      /*anyblock */
	      {

		MELT_LOCATION ("warmelt-first.melt:2892:/ checksignal");
		MELT_CHECK_SIGNAL ();
		;
		/*^checksignal */
		MELT_CHECK_SIGNAL ();
		;
     /*_#IS_A__L10*/ meltfnum[8] =
		  melt_is_instance_of ((melt_ptr_t)
				       ( /*_.PREVBIND__V25*/ meltfptr[19]),
				       (melt_ptr_t) (( /*!CLASS_INSTANCE_BINDING */ meltfrout->tabval[8])));;
		MELT_LOCATION ("warmelt-first.melt:2892:/ cond");
		/*cond */ if ( /*_#IS_A__L10*/ meltfnum[8])	/*then */
		  {
		    /*^cond.then */
		    /*^block */
		    /*anyblock */
		    {

       /*_#IS_OBJECT__L12*/ meltfnum[11] =
			(melt_magic_discr
			 ((melt_ptr_t) ( /*_.VAL__V3*/ meltfptr[2])) ==
			 MELTOBMAG_OBJECT);;
		      /*^compute */
		      /*_#IF___L11*/ meltfnum[10] =
			/*_#IS_OBJECT__L12*/ meltfnum[11];;
		      /*epilog */

		      MELT_LOCATION ("warmelt-first.melt:2892:/ clear");
		 /*clear *//*_#IS_OBJECT__L12*/ meltfnum[11] = 0;
		    }
		    ;
		  }
		else
		  {		/*^cond.else */

      /*_#IF___L11*/ meltfnum[10] = 0;;
		  }
		;
		MELT_LOCATION ("warmelt-first.melt:2892:/ cond");
		/*cond */ if ( /*_#IF___L11*/ meltfnum[10])	/*then */
		  {
		    /*^cond.then */
		    /*^block */
		    /*anyblock */
		    {


		      {
			MELT_LOCATION ("warmelt-first.melt:2895:/ locexp");
			warning (0, "MELT WARNING MSG [#%ld]::: %s - %s",
				 melt_dbgcounter,
				 ("not exporting previous bound instance"),
				 melt_string_str ((melt_ptr_t)
						  ( /*_.SYMNAM__V29*/
						   meltfptr[28])));
		      }
		      ;
		      MELT_LOCATION ("warmelt-first.melt:2896:/ quasiblock");


       /*_.RETVAL___V1*/ meltfptr[0] = NULL;;

		      {
			MELT_LOCATION ("warmelt-first.melt:2896:/ locexp");
			/*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
			if (meltxresdescr_ && meltxresdescr_[0]
			    && meltxrestab_)
			  melt_warn_for_no_expected_secondary_results ();
			/* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
			;
		      }
		      ;
		      /*^finalreturn */
		      ;
		      /*finalret */ goto labend_rout;
		      MELT_LOCATION ("warmelt-first.melt:2892:/ quasiblock");


		      /*_.PROGN___V36*/ meltfptr[35] =
			/*_.RETURN___V35*/ meltfptr[32];;
		      /*^compute */
		      /*_.IFELSE___V34*/ meltfptr[31] =
			/*_.PROGN___V36*/ meltfptr[35];;
		      /*epilog */

		      MELT_LOCATION ("warmelt-first.melt:2892:/ clear");
		 /*clear *//*_.RETURN___V35*/ meltfptr[32] = 0;
		      /*^clear */
		 /*clear *//*_.PROGN___V36*/ meltfptr[35] = 0;
		    }
		    ;
		  }
		else
		  {		/*^cond.else */

		    /*^block */
		    /*anyblock */
		    {

		      MELT_LOCATION ("warmelt-first.melt:2897:/ checksignal");
		      MELT_CHECK_SIGNAL ();
		      ;
		      /*^checksignal */
		      MELT_CHECK_SIGNAL ();
		      ;
       /*_#IS_A__L13*/ meltfnum[11] =
			melt_is_instance_of ((melt_ptr_t)
					     ( /*_.PREVBIND__V25*/
					      meltfptr[19]),
					     (melt_ptr_t) (( /*!CLASS_PRIMITIVE_BINDING */ meltfrout->tabval[9])));;
		      MELT_LOCATION ("warmelt-first.melt:2897:/ cond");
		      /*cond */ if ( /*_#IS_A__L13*/ meltfnum[11])	/*then */
			{
			  /*^cond.then */
			  /*^block */
			  /*anyblock */
			  {

	 /*_#IS_A__L15*/ meltfnum[14] =
			      melt_is_instance_of ((melt_ptr_t)
						   ( /*_.VAL__V3*/
						    meltfptr[2]),
						   (melt_ptr_t) (( /*!CLASS_PRIMITIVE */ meltfrout->tabval[10])));;
			    /*^compute */
			    /*_#IF___L14*/ meltfnum[13] =
			      /*_#IS_A__L15*/ meltfnum[14];;
			    /*epilog */

			    MELT_LOCATION ("warmelt-first.melt:2897:/ clear");
		   /*clear *//*_#IS_A__L15*/ meltfnum[14] = 0;
			  }
			  ;
			}
		      else
			{	/*^cond.else */

	/*_#IF___L14*/ meltfnum[13] = 0;;
			}
		      ;
		      MELT_LOCATION ("warmelt-first.melt:2897:/ cond");
		      /*cond */ if ( /*_#IF___L14*/ meltfnum[13])	/*then */
			{
			  /*^cond.then */
			  /*^block */
			  /*anyblock */
			  {


			    {
			      MELT_LOCATION
				("warmelt-first.melt:2900:/ locexp");
			      warning (0,
				       "MELT WARNING MSG [#%ld]::: %s - %s",
				       melt_dbgcounter,
				       ("not exporting previous bound primitive"),
				       melt_string_str ((melt_ptr_t)
							( /*_.SYMNAM__V29*/
							 meltfptr[28])));
			    }
			    ;
			    MELT_LOCATION
			      ("warmelt-first.melt:2901:/ quasiblock");


	 /*_.RETVAL___V1*/ meltfptr[0] = NULL;;

			    {
			      MELT_LOCATION
				("warmelt-first.melt:2901:/ locexp");
			      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
			      if (meltxresdescr_ && meltxresdescr_[0]
				  && meltxrestab_)
				melt_warn_for_no_expected_secondary_results
				  ();
			      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
			      ;
			    }
			    ;
			    /*^finalreturn */
			    ;
			    /*finalret */ goto labend_rout;
			    MELT_LOCATION
			      ("warmelt-first.melt:2897:/ quasiblock");


			    /*_.PROGN___V39*/ meltfptr[38] =
			      /*_.RETURN___V38*/ meltfptr[35];;
			    /*^compute */
			    /*_.IFELSE___V37*/ meltfptr[32] =
			      /*_.PROGN___V39*/ meltfptr[38];;
			    /*epilog */

			    MELT_LOCATION ("warmelt-first.melt:2897:/ clear");
		   /*clear *//*_.RETURN___V38*/ meltfptr[35] = 0;
			    /*^clear */
		   /*clear *//*_.PROGN___V39*/ meltfptr[38] = 0;
			  }
			  ;
			}
		      else
			{	/*^cond.else */

			  /*^block */
			  /*anyblock */
			  {

			    MELT_LOCATION
			      ("warmelt-first.melt:2902:/ checksignal");
			    MELT_CHECK_SIGNAL ();
			    ;
			    /*^checksignal */
			    MELT_CHECK_SIGNAL ();
			    ;
	 /*_#IS_A__L16*/ meltfnum[14] =
			      melt_is_instance_of ((melt_ptr_t)
						   ( /*_.PREVBIND__V25*/
						    meltfptr[19]),
						   (melt_ptr_t) (( /*!CLASS_FUNCTION_BINDING */ meltfrout->tabval[11])));;
			    MELT_LOCATION ("warmelt-first.melt:2902:/ cond");
			    /*cond */ if ( /*_#IS_A__L16*/ meltfnum[14])	/*then */
			      {
				/*^cond.then */
				/*^block */
				/*anyblock */
				{

	   /*_#IS_CLOSURE__L18*/ meltfnum[17] =
				    (melt_magic_discr
				     ((melt_ptr_t)
				      ( /*_.VAL__V3*/ meltfptr[2])) ==
				     MELTOBMAG_CLOSURE);;
				  /*^compute */
				  /*_#IF___L17*/ meltfnum[16] =
				    /*_#IS_CLOSURE__L18*/ meltfnum[17];;
				  /*epilog */

				  MELT_LOCATION
				    ("warmelt-first.melt:2902:/ clear");
		     /*clear *//*_#IS_CLOSURE__L18*/ meltfnum[17]
				    = 0;
				}
				;
			      }
			    else
			      {	/*^cond.else */

	  /*_#IF___L17*/ meltfnum[16] = 0;;
			      }
			    ;
			    MELT_LOCATION ("warmelt-first.melt:2902:/ cond");
			    /*cond */ if ( /*_#IF___L17*/ meltfnum[16])	/*then */
			      {
				/*^cond.then */
				/*^block */
				/*anyblock */
				{


				  {
				    MELT_LOCATION
				      ("warmelt-first.melt:2905:/ locexp");
				    warning (0,
					     "MELT WARNING MSG [#%ld]::: %s - %s",
					     melt_dbgcounter,
					     ("not exporting previous bound function"),
					     melt_string_str ((melt_ptr_t)
							      ( /*_.SYMNAM__V29*/ meltfptr[28])));
				  }
				  ;
				  MELT_LOCATION
				    ("warmelt-first.melt:2906:/ quasiblock");


	   /*_.RETVAL___V1*/ meltfptr[0] = NULL;;

				  {
				    MELT_LOCATION
				      ("warmelt-first.melt:2906:/ locexp");
				    /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
				    if (meltxresdescr_ && meltxresdescr_[0]
					&& meltxrestab_)
				      melt_warn_for_no_expected_secondary_results
					();
				    /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
				    ;
				  }
				  ;
				  /*^finalreturn */
				  ;
				  /*finalret */ goto labend_rout;
				  MELT_LOCATION
				    ("warmelt-first.melt:2902:/ quasiblock");


				  /*_.PROGN___V42*/ meltfptr[41] =
				    /*_.RETURN___V41*/ meltfptr[38];;
				  /*^compute */
				  /*_.IFELSE___V40*/ meltfptr[35] =
				    /*_.PROGN___V42*/ meltfptr[41];;
				  /*epilog */

				  MELT_LOCATION
				    ("warmelt-first.melt:2902:/ clear");
		     /*clear *//*_.RETURN___V41*/ meltfptr[38] =
				    0;
				  /*^clear */
		     /*clear *//*_.PROGN___V42*/ meltfptr[41] =
				    0;
				}
				;
			      }
			    else
			      {	/*^cond.else */

				/*^block */
				/*anyblock */
				{

				  MELT_LOCATION
				    ("warmelt-first.melt:2907:/ checksignal");
				  MELT_CHECK_SIGNAL ();
				  ;
				  /*^checksignal */
				  MELT_CHECK_SIGNAL ();
				  ;
	   /*_#IS_A__L19*/ meltfnum[17] =
				    melt_is_instance_of ((melt_ptr_t)
							 ( /*_.PREVBIND__V25*/
							  meltfptr[19]),
							 (melt_ptr_t) (( /*!CLASS_CLASS_BINDING */ meltfrout->tabval[12])));;
				  MELT_LOCATION
				    ("warmelt-first.melt:2907:/ cond");
				  /*cond */ if ( /*_#IS_A__L19*/ meltfnum[17])	/*then */
				    {
				      /*^cond.then */
				      /*^block */
				      /*anyblock */
				      {

	     /*_#IS_A__L21*/ meltfnum[20] =
					  melt_is_instance_of ((melt_ptr_t)
							       ( /*_.VAL__V3*/
								meltfptr[2]),
							       (melt_ptr_t) (( /*!CLASS_CLASS */ meltfrout->tabval[13])));;
					/*^compute */
					/*_#IF___L20*/ meltfnum[19] =
					  /*_#IS_A__L21*/ meltfnum[20];;
					/*epilog */

					MELT_LOCATION
					  ("warmelt-first.melt:2907:/ clear");
		       /*clear *//*_#IS_A__L21*/ meltfnum[20]
					  = 0;
				      }
				      ;
				    }
				  else
				    {	/*^cond.else */

	    /*_#IF___L20*/ meltfnum[19] = 0;;
				    }
				  ;
				  MELT_LOCATION
				    ("warmelt-first.melt:2907:/ cond");
				  /*cond */ if ( /*_#IF___L20*/ meltfnum[19])	/*then */
				    {
				      /*^cond.then */
				      /*^block */
				      /*anyblock */
				      {


					{
					  MELT_LOCATION
					    ("warmelt-first.melt:2910:/ locexp");
					  warning (0,
						   "MELT WARNING MSG [#%ld]::: %s - %s",
						   melt_dbgcounter,
						   ("not exporting previous bound class"),
						   melt_string_str ((melt_ptr_t) ( /*_.SYMNAM__V29*/ meltfptr[28])));
					}
					;
					MELT_LOCATION
					  ("warmelt-first.melt:2911:/ quasiblock");


	     /*_.RETVAL___V1*/ meltfptr[0] = NULL;;

					{
					  MELT_LOCATION
					    ("warmelt-first.melt:2911:/ locexp");
					  /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
					  if (meltxresdescr_
					      && meltxresdescr_[0]
					      && meltxrestab_)
					    melt_warn_for_no_expected_secondary_results
					      ();
					  /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
					  ;
					}
					;
					/*^finalreturn */
					;
					/*finalret */ goto labend_rout;
					MELT_LOCATION
					  ("warmelt-first.melt:2907:/ quasiblock");


					/*_.PROGN___V45*/ meltfptr[44] =
					  /*_.RETURN___V44*/ meltfptr[41];;
					/*^compute */
					/*_.IFELSE___V43*/ meltfptr[38] =
					  /*_.PROGN___V45*/ meltfptr[44];;
					/*epilog */

					MELT_LOCATION
					  ("warmelt-first.melt:2907:/ clear");
		       /*clear *//*_.RETURN___V44*/
					  meltfptr[41] = 0;
					/*^clear */
		       /*clear *//*_.PROGN___V45*/
					  meltfptr[44] = 0;
				      }
				      ;
				    }
				  else
				    {	/*^cond.else */

				      /*^block */
				      /*anyblock */
				      {

					MELT_LOCATION
					  ("warmelt-first.melt:2912:/ checksignal");
					MELT_CHECK_SIGNAL ();
					;
					/*^checksignal */
					MELT_CHECK_SIGNAL ();
					;
	     /*_#IS_A__L22*/ meltfnum[20] =
					  melt_is_instance_of ((melt_ptr_t)
							       ( /*_.PREVBIND__V25*/ meltfptr[19]), (melt_ptr_t) (( /*!CLASS_FIELD_BINDING */ meltfrout->tabval[14])));;
					MELT_LOCATION
					  ("warmelt-first.melt:2912:/ cond");
					/*cond */ if ( /*_#IS_A__L22*/ meltfnum[20])	/*then */
					  {
					    /*^cond.then */
					    /*^block */
					    /*anyblock */
					    {

	       /*_#IS_A__L24*/ meltfnum[23] =
						melt_is_instance_of ((melt_ptr_t) ( /*_.VAL__V3*/ meltfptr[2]), (melt_ptr_t) (( /*!CLASS_CLASS */ meltfrout->tabval[13])));;
					      /*^compute */
					      /*_#IF___L23*/ meltfnum[22] =
						/*_#IS_A__L24*/ meltfnum[23];;
					      /*epilog */

					      MELT_LOCATION
						("warmelt-first.melt:2912:/ clear");
			 /*clear *//*_#IS_A__L24*/
						meltfnum[23] = 0;
					    }
					    ;
					  }
					else
					  {	/*^cond.else */

	      /*_#IF___L23*/ meltfnum[22] = 0;;
					  }
					;
					MELT_LOCATION
					  ("warmelt-first.melt:2912:/ cond");
					/*cond */ if ( /*_#IF___L23*/ meltfnum[22])	/*then */
					  {
					    /*^cond.then */
					    /*^block */
					    /*anyblock */
					    {


					      {
						MELT_LOCATION
						  ("warmelt-first.melt:2915:/ locexp");
						warning (0,
							 "MELT WARNING MSG [#%ld]::: %s - %s",
							 melt_dbgcounter,
							 ("not exporting previous bound field"),
							 melt_string_str ((melt_ptr_t) ( /*_.SYMNAM__V29*/ meltfptr[28])));
					      }
					      ;
					      MELT_LOCATION
						("warmelt-first.melt:2916:/ quasiblock");


	       /*_.RETVAL___V1*/ meltfptr[0] =
						NULL;;

					      {
						MELT_LOCATION
						  ("warmelt-first.melt:2916:/ locexp");
						/*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
						if (meltxresdescr_
						    && meltxresdescr_[0]
						    && meltxrestab_)
						  melt_warn_for_no_expected_secondary_results
						    ();
						/* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
						;
					      }
					      ;
					      /*^finalreturn */
					      ;
					      /*finalret */ goto labend_rout;
					      MELT_LOCATION
						("warmelt-first.melt:2912:/ quasiblock");


					      /*_.PROGN___V48*/ meltfptr[47] =
						/*_.RETURN___V47*/
						meltfptr[44];;
					      /*^compute */
					      /*_.IFELSE___V46*/ meltfptr[41]
						=
						/*_.PROGN___V48*/
						meltfptr[47];;
					      /*epilog */

					      MELT_LOCATION
						("warmelt-first.melt:2912:/ clear");
			 /*clear *//*_.RETURN___V47*/
						meltfptr[44] = 0;
					      /*^clear */
			 /*clear *//*_.PROGN___V48*/
						meltfptr[47] = 0;
					    }
					    ;
					  }
					else
					  {	/*^cond.else */

					    /*^block */
					    /*anyblock */
					    {

					      MELT_LOCATION
						("warmelt-first.melt:2918:/ checksignal");
					      MELT_CHECK_SIGNAL ();
					      ;
					      /*^checksignal */
					      MELT_CHECK_SIGNAL ();
					      ;
	       /*_#IS_A__L25*/ meltfnum[23] =
						melt_is_instance_of ((melt_ptr_t) ( /*_.PREVBIND__V25*/ meltfptr[19]), (melt_ptr_t) (( /*!CLASS_INSTANCE_BINDING */ meltfrout->tabval[8])));;
					      MELT_LOCATION
						("warmelt-first.melt:2918:/ cond");
					      /*cond */ if ( /*_#IS_A__L25*/ meltfnum[23])	/*then */
						{
						  /*^cond.then */
						  /*^block */
						  /*anyblock */
						  {

		 /*_#IS_OBJECT__L27*/
						      meltfnum[26] =
						      (melt_magic_discr
						       ((melt_ptr_t)
							( /*_.VAL__V3*/
							 meltfptr[2])) ==
						       MELTOBMAG_OBJECT);;
						    /*^compute */
						    /*_#IF___L26*/
						      meltfnum[25] =
						      /*_#IS_OBJECT__L27*/
						      meltfnum[26];;
						    /*epilog */

						    MELT_LOCATION
						      ("warmelt-first.melt:2918:/ clear");
			   /*clear *//*_#IS_OBJECT__L27*/
						      meltfnum[26] = 0;
						  }
						  ;
						}
					      else
						{	/*^cond.else */

		/*_#IF___L26*/ meltfnum[25]
						    = 0;;
						}
					      ;
					      MELT_LOCATION
						("warmelt-first.melt:2918:/ cond");
					      /*cond */ if ( /*_#IF___L26*/ meltfnum[25])	/*then */
						{
						  /*^cond.then */
						  /*^block */
						  /*anyblock */
						  {


						    {
						      MELT_LOCATION
							("warmelt-first.melt:2921:/ locexp");
						      warning (0,
							       "MELT WARNING MSG [#%ld]::: %s - %s",
							       melt_dbgcounter,
							       ("not exporting previous bound instance"),
							       melt_string_str
							       ((melt_ptr_t)
								( /*_.SYMNAM__V29*/ meltfptr[28])));
						    }
						    ;
						    MELT_LOCATION
						      ("warmelt-first.melt:2922:/ quasiblock");


		 /*_.RETVAL___V1*/
						      meltfptr[0] = NULL;;

						    {
						      MELT_LOCATION
							("warmelt-first.melt:2922:/ locexp");
						      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
						      if (meltxresdescr_
							  && meltxresdescr_[0]
							  && meltxrestab_)
							melt_warn_for_no_expected_secondary_results
							  ();
						      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
						      ;
						    }
						    ;
						    /*^finalreturn */
						    ;
						    /*finalret */ goto
						      labend_rout;
						    MELT_LOCATION
						      ("warmelt-first.melt:2918:/ quasiblock");


						    /*_.PROGN___V51*/
						      meltfptr[50] =
						      /*_.RETURN___V50*/
						      meltfptr[47];;
						    /*^compute */
						    /*_.IFELSE___V49*/
						      meltfptr[44] =
						      /*_.PROGN___V51*/
						      meltfptr[50];;
						    /*epilog */

						    MELT_LOCATION
						      ("warmelt-first.melt:2918:/ clear");
			   /*clear *//*_.RETURN___V50*/
						      meltfptr[47] = 0;
						    /*^clear */
			   /*clear *//*_.PROGN___V51*/
						      meltfptr[50] = 0;
						  }
						  ;
						}
					      else
						{	/*^cond.else */

						  /*^block */
						  /*anyblock */
						  {

						    MELT_LOCATION
						      ("warmelt-first.melt:2924:/ checksignal");
						    MELT_CHECK_SIGNAL ();
						    ;
		 /*_#IS_A__L28*/
						      meltfnum[26] =
						      melt_is_instance_of ((melt_ptr_t) ( /*_.PREVBIND__V25*/ meltfptr[19]), (melt_ptr_t) (( /*!CLASS_VALUE_BINDING */ meltfrout->tabval[5])));;
						    MELT_LOCATION
						      ("warmelt-first.melt:2924:/ cond");
						    /*cond */ if ( /*_#IS_A__L28*/ meltfnum[26])	/*then */
						      {
							/*^cond.then */
							/*^block */
							/*anyblock */
							{

							  MELT_LOCATION
							    ("warmelt-first.melt:2925:/ quasiblock");


							  /*^getslot */
							  {
							    melt_ptr_t slot =
							      NULL, obj =
							      NULL;
							    obj =
							      (melt_ptr_t) ( /*_.PREVBIND__V25*/ meltfptr[19]) /*=obj*/ ;
							    melt_object_get_field
							      (slot, obj, 1,
							       "VBIND_VALUE");
		    /*_.PREVAL__V54*/
							      meltfptr
							      [53] = slot;
							  };
							  ;
		   /*_.PREVDISCR__V55*/
							    meltfptr[54] =
							    ((melt_ptr_t)
							     (melt_discr
							      ((melt_ptr_t)
							       ( /*_.PREVAL__V54*/ meltfptr[53]))));;
							  /*^compute */
		   /*_.CURDISCR__V56*/
							    meltfptr[55] =
							    ((melt_ptr_t)
							     (melt_discr
							      ((melt_ptr_t)
							       ( /*_.VAL__V3*/
								meltfptr
								[2]))));;
							  MELT_LOCATION
							    ("warmelt-first.melt:2929:/ checksignal");
							  MELT_CHECK_SIGNAL
							    ();
							  ;
		   /*_#__L29*/
							    meltfnum[28] =
							    (( /*_.PREVDISCR__V55*/ meltfptr[54]) == ( /*_.CURDISCR__V56*/ meltfptr[55]));;
							  MELT_LOCATION
							    ("warmelt-first.melt:2929:/ cond");
							  /*cond */ if ( /*_#__L29*/ meltfnum[28])	/*then */
							    {
							      /*^cond.then */
							      /*^block */
							      /*anyblock */
							      {


								{
								  MELT_LOCATION
								    ("warmelt-first.melt:2931:/ locexp");
								  warning (0,
									   "MELT WARNING MSG [#%ld]::: %s - %s",
									   melt_dbgcounter,
									   ("not exporting previous bound homogenous value"),
									   melt_string_str
									   ((melt_ptr_t) ( /*_.SYMNAM__V29*/ meltfptr[28])));
								}
								;
								MELT_LOCATION
								  ("warmelt-first.melt:2933:/ getslot");
								{
								  melt_ptr_t
								    slot =
								    NULL,
								    obj =
								    NULL;
								  obj =
								    (melt_ptr_t)
								    ( /*_.PREVDISCR__V55*/ meltfptr[54]) /*=obj*/ ;
								  melt_object_get_field
								    (slot,
								     obj, 1,
								     "NAMED_NAME");
		      /*_.NAMED_NAME__V58*/
								    meltfptr
								    [57]
								    = slot;
								};
								;

								{
								  MELT_LOCATION
								    ("warmelt-first.melt:2932:/ locexp");
								  warning (0,
									   "MELT WARNING MSG [#%ld]::: %s - %s",
									   melt_dbgcounter,
									   ("common value discrim"),
									   melt_string_str
									   ((melt_ptr_t) ( /*_.NAMED_NAME__V58*/ meltfptr[57])));
								}
								;
								MELT_LOCATION
								  ("warmelt-first.melt:2934:/ quasiblock");


		     /*_.RETVAL___V1*/
								  meltfptr
								  [0] = NULL;;

								{
								  MELT_LOCATION
								    ("warmelt-first.melt:2934:/ locexp");
								  /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
								  if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
								    melt_warn_for_no_expected_secondary_results
								      ();
								  /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
								  ;
								}
								;
								/*^finalreturn */
								;
								/*finalret */
								  goto
								  labend_rout;
								MELT_LOCATION
								  ("warmelt-first.melt:2930:/ quasiblock");


								/*_.PROGN___V60*/
								  meltfptr[59]
								  =
								  /*_.RETURN___V59*/
								  meltfptr
								  [58];;
								/*^compute */
								/*_.IF___V57*/
								  meltfptr[56]
								  =
								  /*_.PROGN___V60*/
								  meltfptr
								  [59];;
								/*epilog */

								MELT_LOCATION
								  ("warmelt-first.melt:2929:/ clear");
			       /*clear *//*_.NAMED_NAME__V58*/
								  meltfptr
								  [57] = 0;
								/*^clear */
			       /*clear *//*_.RETURN___V59*/
								  meltfptr
								  [58] = 0;
								/*^clear */
			       /*clear *//*_.PROGN___V60*/
								  meltfptr
								  [59] = 0;
							      }
							      ;
							    }
							  else
							    {	/*^cond.else */

		    /*_.IF___V57*/
								meltfptr
								[56] = NULL;;
							    }
							  ;
							  /*^compute */
							  /*_.LET___V53*/
							    meltfptr[50] =
							    /*_.IF___V57*/
							    meltfptr[56];;

							  MELT_LOCATION
							    ("warmelt-first.melt:2925:/ clear");
			     /*clear *//*_.PREVAL__V54*/
							    meltfptr[53] = 0;
							  /*^clear */
			     /*clear *//*_.PREVDISCR__V55*/
							    meltfptr[54] = 0;
							  /*^clear */
			     /*clear *//*_.CURDISCR__V56*/
							    meltfptr[55] = 0;
							  /*^clear */
			     /*clear *//*_#__L29*/
							    meltfnum[28] = 0;
							  /*^clear */
			     /*clear *//*_.IF___V57*/
							    meltfptr[56] = 0;
							  /*_.IFELSE___V52*/
							    meltfptr[47] =
							    /*_.LET___V53*/
							    meltfptr[50];;
							  /*epilog */

							  MELT_LOCATION
							    ("warmelt-first.melt:2924:/ clear");
			     /*clear *//*_.LET___V53*/
							    meltfptr[50] = 0;
							}
							;
						      }
						    else
						      {	/*^cond.else */

		  /*_.IFELSE___V52*/
							  meltfptr
							  [47] = NULL;;
						      }
						    ;
						    /*^compute */
						    /*_.IFELSE___V49*/
						      meltfptr[44] =
						      /*_.IFELSE___V52*/
						      meltfptr[47];;
						    /*epilog */

						    MELT_LOCATION
						      ("warmelt-first.melt:2918:/ clear");
			   /*clear *//*_#IS_A__L28*/
						      meltfnum[26] = 0;
						    /*^clear */
			   /*clear *//*_.IFELSE___V52*/
						      meltfptr[47] = 0;
						  }
						  ;
						}
					      ;
					      /*_.IFELSE___V46*/ meltfptr[41]
						=
						/*_.IFELSE___V49*/
						meltfptr[44];;
					      /*epilog */

					      MELT_LOCATION
						("warmelt-first.melt:2912:/ clear");
			 /*clear *//*_#IS_A__L25*/
						meltfnum[23] = 0;
					      /*^clear */
			 /*clear *//*_#IF___L26*/
						meltfnum[25] = 0;
					      /*^clear */
			 /*clear *//*_.IFELSE___V49*/
						meltfptr[44] = 0;
					    }
					    ;
					  }
					;
					/*_.IFELSE___V43*/ meltfptr[38] =
					  /*_.IFELSE___V46*/ meltfptr[41];;
					/*epilog */

					MELT_LOCATION
					  ("warmelt-first.melt:2907:/ clear");
		       /*clear *//*_#IS_A__L22*/ meltfnum[20]
					  = 0;
					/*^clear */
		       /*clear *//*_#IF___L23*/ meltfnum[22]
					  = 0;
					/*^clear */
		       /*clear *//*_.IFELSE___V46*/
					  meltfptr[41] = 0;
				      }
				      ;
				    }
				  ;
				  /*_.IFELSE___V40*/ meltfptr[35] =
				    /*_.IFELSE___V43*/ meltfptr[38];;
				  /*epilog */

				  MELT_LOCATION
				    ("warmelt-first.melt:2902:/ clear");
		     /*clear *//*_#IS_A__L19*/ meltfnum[17] = 0;
				  /*^clear */
		     /*clear *//*_#IF___L20*/ meltfnum[19] = 0;
				  /*^clear */
		     /*clear *//*_.IFELSE___V43*/ meltfptr[38] =
				    0;
				}
				;
			      }
			    ;
			    /*_.IFELSE___V37*/ meltfptr[32] =
			      /*_.IFELSE___V40*/ meltfptr[35];;
			    /*epilog */

			    MELT_LOCATION ("warmelt-first.melt:2897:/ clear");
		   /*clear *//*_#IS_A__L16*/ meltfnum[14] = 0;
			    /*^clear */
		   /*clear *//*_#IF___L17*/ meltfnum[16] = 0;
			    /*^clear */
		   /*clear *//*_.IFELSE___V40*/ meltfptr[35] = 0;
			  }
			  ;
			}
		      ;
		      /*_.IFELSE___V34*/ meltfptr[31] =
			/*_.IFELSE___V37*/ meltfptr[32];;
		      /*epilog */

		      MELT_LOCATION ("warmelt-first.melt:2892:/ clear");
		 /*clear *//*_#IS_A__L13*/ meltfnum[11] = 0;
		      /*^clear */
		 /*clear *//*_#IF___L14*/ meltfnum[13] = 0;
		      /*^clear */
		 /*clear *//*_.IFELSE___V37*/ meltfptr[32] = 0;
		    }
		    ;
		  }
		;
		/*_.IFELSE___V31*/ meltfptr[30] =
		  /*_.IFELSE___V34*/ meltfptr[31];;
		/*epilog */

		MELT_LOCATION ("warmelt-first.melt:2887:/ clear");
	       /*clear *//*_#IS_A__L10*/ meltfnum[8] = 0;
		/*^clear */
	       /*clear *//*_#IF___L11*/ meltfnum[10] = 0;
		/*^clear */
	       /*clear *//*_.IFELSE___V34*/ meltfptr[31] = 0;
	      }
	      ;
	    }
	  ;
	  /*_.IFELSE___V30*/ meltfptr[29] = /*_.IFELSE___V31*/ meltfptr[30];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-first.melt:2886:/ clear");
	     /*clear *//*_#IS_A__L7*/ meltfnum[6] = 0;
	  /*^clear */
	     /*clear *//*_#IF___L8*/ meltfnum[7] = 0;
	  /*^clear */
	     /*clear *//*_.IFELSE___V31*/ meltfptr[30] = 0;
	}
	;
      }
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-first.melt:2937:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L30*/ meltfnum[28] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.VALBIND__V27*/ meltfptr[25]),
			     (melt_ptr_t) (( /*!CLASS_ANY_BINDING */
					    meltfrout->tabval[15])));;
      MELT_LOCATION ("warmelt-first.melt:2937:/ cond");
      /*cond */ if ( /*_#IS_A__L30*/ meltfnum[28])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V62*/ meltfptr[58] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-first.melt:2937:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check valbind"),
				  ("warmelt-first.melt")
				  ? ("warmelt-first.melt") : __FILE__,
				  (2937) ? (2937) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V62*/ meltfptr[58] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V61*/ meltfptr[57] = /*_.IFELSE___V62*/ meltfptr[58];;
      /*epilog */

      MELT_LOCATION ("warmelt-first.melt:2937:/ clear");
	     /*clear *//*_#IS_A__L30*/ meltfnum[28] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V62*/ meltfptr[58] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V61*/ meltfptr[57] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-first.melt:2938:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & /*_.VALBIND__V27*/ meltfptr[25];
      /*_.PUT_ENV__V63*/ meltfptr[59] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!PUT_ENV */ meltfrout->tabval[16])),
		    (melt_ptr_t) ( /*_.ENV__V16*/ meltfptr[15]),
		    (MELTBPARSTR_PTR ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-first.melt:2939:/ quasiblock");


 /*_.RETVAL___V1*/ meltfptr[0] = NULL;;

    {
      MELT_LOCATION ("warmelt-first.melt:2939:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*_.LET___V24*/ meltfptr[20] = /*_.RETURN___V64*/ meltfptr[53];;

    MELT_LOCATION ("warmelt-first.melt:2876:/ clear");
	   /*clear *//*_.PREVBIND__V25*/ meltfptr[19] = 0;
    /*^clear */
	   /*clear *//*_.VALBIND__V27*/ meltfptr[25] = 0;
    /*^clear */
	   /*clear *//*_.SYMNAM__V29*/ meltfptr[28] = 0;
    /*^clear */
	   /*clear *//*_#NULL__L6*/ meltfnum[4] = 0;
    /*^clear */
	   /*clear *//*_.IFELSE___V30*/ meltfptr[29] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V61*/ meltfptr[57] = 0;
    /*^clear */
	   /*clear *//*_.PUT_ENV__V63*/ meltfptr[59] = 0;
    /*^clear */
	   /*clear *//*_.RETURN___V64*/ meltfptr[53] = 0;
    /*_.LET___V15*/ meltfptr[13] = /*_.LET___V24*/ meltfptr[20];;

    MELT_LOCATION ("warmelt-first.melt:2869:/ clear");
	   /*clear *//*_.ENV__V16*/ meltfptr[15] = 0;
    /*^clear */
	   /*clear *//*_#NULL__L4*/ meltfnum[1] = 0;
    /*^clear */
	   /*clear *//*_.IF___V17*/ meltfptr[16] = 0;
    /*^clear */
	   /*clear *//*_.IF___V18*/ meltfptr[17] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V22*/ meltfptr[18] = 0;
    /*^clear */
	   /*clear *//*_.LET___V24*/ meltfptr[20] = 0;
    /*_.LET___V5*/ meltfptr[4] = /*_.LET___V15*/ meltfptr[13];;

    MELT_LOCATION ("warmelt-first.melt:2860:/ clear");
	   /*clear *//*_.PARENV__V6*/ meltfptr[5] = 0;
    /*^clear */
	   /*clear *//*_#NULL__L1*/ meltfnum[0] = 0;
    /*^clear */
	   /*clear *//*_.IF___V7*/ meltfptr[6] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V11*/ meltfptr[7] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V13*/ meltfptr[9] = 0;
    /*^clear */
	   /*clear *//*_.LET___V15*/ meltfptr[13] = 0;
    MELT_LOCATION ("warmelt-first.melt:2859:/ quasiblock");


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.LET___V5*/ meltfptr[4];;

    {
      MELT_LOCATION ("warmelt-first.melt:2859:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.LET___V5*/ meltfptr[4] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("INITVALUE_EXPORTER", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_10_warmelt_first_INITVALUE_EXPORTER_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_10_warmelt_first_INITVALUE_EXPORTER */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_11_warmelt_first_INITVALUE_IMPORTER (meltclosure_ptr_t meltclosp_,
					      melt_ptr_t meltfirstargp_,
					      const melt_argdescr_cell_t
					      meltxargdescr_[],
					      union meltparam_un *
					      meltxargtab_,
					      const melt_argdescr_cell_t
					      meltxresdescr_[],
					      union meltparam_un *
					      meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_11_warmelt_first_INITVALUE_IMPORTER_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_11_warmelt_first_INITVALUE_IMPORTER_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 24
    melt_ptr_t mcfr_varptr[24];
#define MELTFRAM_NBVARNUM 6
    long mcfr_varnum[6];
/*others*/
    const char *loc_CSTRING__o0;
    const char *loc_CSTRING__o1;
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_11_warmelt_first_INITVALUE_IMPORTER is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_11_warmelt_first_INITVALUE_IMPORTER_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 24; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_11_warmelt_first_INITVALUE_IMPORTER nbval 24*/
  meltfram__.mcfr_nbvar = 24 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("INITVALUE_IMPORTER", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-first.melt:2944:/ getarg");
 /*_.SYM__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.PARENV__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.PARENV__V3*/ meltfptr[2])) !=
	      NULL);


  /*getarg#2 */
  /*^getarg */
  if (meltxargdescr_[1] != MELTBPAR_CSTRING)
    goto lab_endgetargs;
 /*_?*/ meltfram__.loc_CSTRING__o0 = meltxargtab_[1].meltbp_cstring;

  /*getarg#3 */
  /*^getarg */
  if (meltxargdescr_[2] != MELTBPAR_CSTRING)
    goto lab_endgetargs;
 /*_?*/ meltfram__.loc_CSTRING__o1 = meltxargtab_[2].meltbp_cstring;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


    {
      MELT_LOCATION ("warmelt-first.melt:2945:/ locexp");
      /* ENSUREMODNAM__1 */ if (! /*_?*/ meltfram__.loc_CSTRING__o1)
								/*_?*/
	meltfram__.loc_CSTRING__o1 = "???";
      ;
    }
    ;
    MELT_LOCATION ("warmelt-first.melt:2947:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#IS_NOT_A__L1*/ meltfnum[0] =
      !melt_is_instance_of ((melt_ptr_t) ( /*_.SYM__V2*/ meltfptr[1]),
			    (melt_ptr_t) (( /*!CLASS_SYMBOL */ meltfrout->
					   tabval[0])));;
    MELT_LOCATION ("warmelt-first.melt:2947:/ cond");
    /*cond */ if ( /*_#IS_NOT_A__L1*/ meltfnum[0])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{


	  {
	    MELT_LOCATION ("warmelt-first.melt:2950:/ locexp");
	    /* ERRFAILIMPORT__1 */
	    if ( /*_?*/ meltfram__.loc_CSTRING__o0)
	      error ("MELT [%s]: imported symbol %s not found",
		     /*_?*/ meltfram__.loc_CSTRING__o1, /*_?*/
		     meltfram__.loc_CSTRING__o0);
	    else
	      error ("MELT [%s]: importing non symbol", /*_?*/
		     meltfram__.loc_CSTRING__o1);
	    ;
	  }
	  ;
	  MELT_LOCATION ("warmelt-first.melt:2957:/ quasiblock");


   /*_.RETVAL___V1*/ meltfptr[0] = NULL;;

	  {
	    MELT_LOCATION ("warmelt-first.melt:2957:/ locexp");
	    /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
	    if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	      melt_warn_for_no_expected_secondary_results ();
	    /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
	    ;
	  }
	  ;
	  /*^finalreturn */
	  ;
	  /*finalret */ goto labend_rout;
	  MELT_LOCATION ("warmelt-first.melt:2948:/ quasiblock");


	  /*_.PROGN___V6*/ meltfptr[5] = /*_.RETURN___V5*/ meltfptr[4];;
	  /*^compute */
	  /*_.IF___V4*/ meltfptr[3] = /*_.PROGN___V6*/ meltfptr[5];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-first.melt:2947:/ clear");
	     /*clear *//*_.RETURN___V5*/ meltfptr[4] = 0;
	  /*^clear */
	     /*clear *//*_.PROGN___V6*/ meltfptr[5] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

  /*_.IF___V4*/ meltfptr[3] = NULL;;
      }
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-first.melt:2959:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L2*/ meltfnum[1] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.SYM__V2*/ meltfptr[1]),
			     (melt_ptr_t) (( /*!CLASS_SYMBOL */ meltfrout->
					    tabval[0])));;
      MELT_LOCATION ("warmelt-first.melt:2959:/ cond");
      /*cond */ if ( /*_#IS_A__L2*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V8*/ meltfptr[5] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-first.melt:2959:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check sym"),
				  ("warmelt-first.melt")
				  ? ("warmelt-first.melt") : __FILE__,
				  (2959) ? (2959) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V8*/ meltfptr[5] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V7*/ meltfptr[4] = /*_.IFELSE___V8*/ meltfptr[5];;
      /*epilog */

      MELT_LOCATION ("warmelt-first.melt:2959:/ clear");
	     /*clear *//*_#IS_A__L2*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V8*/ meltfptr[5] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V7*/ meltfptr[4] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-first.melt:2960:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L3*/ meltfnum[1] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.PARENV__V3*/ meltfptr[2]),
			     (melt_ptr_t) (( /*!CLASS_ENVIRONMENT */
					    meltfrout->tabval[1])));;
      MELT_LOCATION ("warmelt-first.melt:2960:/ cond");
      /*cond */ if ( /*_#IS_A__L3*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V10*/ meltfptr[9] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-first.melt:2960:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check parenv"),
				  ("warmelt-first.melt")
				  ? ("warmelt-first.melt") : __FILE__,
				  (2960) ? (2960) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V10*/ meltfptr[9] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V9*/ meltfptr[5] = /*_.IFELSE___V10*/ meltfptr[9];;
      /*epilog */

      MELT_LOCATION ("warmelt-first.melt:2960:/ clear");
	     /*clear *//*_#IS_A__L3*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V10*/ meltfptr[9] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V9*/ meltfptr[5] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-first.melt:2961:/ quasiblock");


    /*^checksignal */
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.SYM__V2*/ meltfptr[1];
      /*_.VALBIND__V12*/ meltfptr[11] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!FIND_ENV */ meltfrout->tabval[2])),
		    (melt_ptr_t) ( /*_.PARENV__V3*/ meltfptr[2]),
		    (MELTBPARSTR_PTR ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-first.melt:2963:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#IS_NOT_A__L4*/ meltfnum[1] =
      !melt_is_instance_of ((melt_ptr_t) ( /*_.VALBIND__V12*/ meltfptr[11]),
			    (melt_ptr_t) (( /*!CLASS_VALUE_BINDING */
					   meltfrout->tabval[3])));;
    MELT_LOCATION ("warmelt-first.melt:2963:/ cond");
    /*cond */ if ( /*_#IS_NOT_A__L4*/ meltfnum[1])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-first.melt:2964:/ quasiblock");


	  /*^cond */
	  /*cond */ if (
			 /*ifisa */
			 melt_is_instance_of ((melt_ptr_t)
					      ( /*_.SYM__V2*/ meltfptr[1]),
					      (melt_ptr_t) (( /*!CLASS_NAMED */ meltfrout->tabval[4])))
	    )			/*then */
	    {
	      /*^cond.then */
	      /*^getslot */
	      {
		melt_ptr_t slot = NULL, obj = NULL;
		obj = (melt_ptr_t) ( /*_.SYM__V2*/ meltfptr[1]) /*=obj*/ ;
		melt_object_get_field (slot, obj, 1, "NAMED_NAME");
     /*_.SYMNAM__V15*/ meltfptr[14] = slot;
	      };
	      ;
	    }
	  else
	    {			/*^cond.else */

    /*_.SYMNAM__V15*/ meltfptr[14] = NULL;;
	    }
	  ;
	  /*^compute */
   /*_.BINDISCR__V16*/ meltfptr[15] =
	    ((melt_ptr_t)
	     (melt_discr ((melt_ptr_t) ( /*_.VALBIND__V12*/ meltfptr[11]))));;
	  MELT_LOCATION ("warmelt-first.melt:2966:/ cond");
	  /*cond */ if (
			 /*ifisa */
			 melt_is_instance_of ((melt_ptr_t)
					      ( /*_.BINDISCR__V16*/
					       meltfptr[15]),
					      (melt_ptr_t) (( /*!CLASS_NAMED */ meltfrout->tabval[4])))
	    )			/*then */
	    {
	      /*^cond.then */
	      /*^getslot */
	      {
		melt_ptr_t slot = NULL, obj = NULL;
		obj =
		  (melt_ptr_t) ( /*_.BINDISCR__V16*/ meltfptr[15]) /*=obj*/ ;
		melt_object_get_field (slot, obj, 1, "NAMED_NAME");
     /*_.BINDISCRNAM__V17*/ meltfptr[16] = slot;
	      };
	      ;
	    }
	  else
	    {			/*^cond.else */

    /*_.BINDISCRNAM__V17*/ meltfptr[16] = NULL;;
	    }
	  ;

#if MELT_HAVE_DEBUG
	  MELT_LOCATION ("warmelt-first.melt:2968:/ cppif.then");
	  /*^block */
	  /*anyblock */
	  {

	    /*^checksignal */
	    MELT_CHECK_SIGNAL ();
	    ;
     /*_#IS_STRING__L5*/ meltfnum[4] =
	      (melt_magic_discr
	       ((melt_ptr_t) ( /*_.SYMNAM__V15*/ meltfptr[14])) ==
	       MELTOBMAG_STRING);;
	    MELT_LOCATION ("warmelt-first.melt:2968:/ cond");
	    /*cond */ if ( /*_#IS_STRING__L5*/ meltfnum[4])	/*then */
	      {
		/*^cond.then */
		/*_.IFELSE___V19*/ meltfptr[18] = ( /*nil */ NULL);;
	      }
	    else
	      {
		MELT_LOCATION ("warmelt-first.melt:2968:/ cond.else");

		/*^block */
		/*anyblock */
		{




		  {
		    /*^locexp */
		    melt_assert_failed (("check symnam"),
					("warmelt-first.melt")
					? ("warmelt-first.melt") : __FILE__,
					(2968) ? (2968) : __LINE__,
					__FUNCTION__);
		    ;
		  }
		  ;
		 /*clear *//*_.IFELSE___V19*/ meltfptr[18] = 0;
		  /*epilog */
		}
		;
	      }
	    ;
	    /*^compute */
	    /*_.IFCPP___V18*/ meltfptr[17] = /*_.IFELSE___V19*/ meltfptr[18];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-first.melt:2968:/ clear");
	       /*clear *//*_#IS_STRING__L5*/ meltfnum[4] = 0;
	    /*^clear */
	       /*clear *//*_.IFELSE___V19*/ meltfptr[18] = 0;
	  }

#else /*MELT_HAVE_DEBUG */
	  /*^cppif.else */
	  /*_.IFCPP___V18*/ meltfptr[17] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
	  ;

	  {
	    MELT_LOCATION ("warmelt-first.melt:2970:/ locexp");
	    /* ERRBADIMPORT__1 start */
	    {
	      const char *ERRBADIMPORT__1_str =
		melt_string_str ((melt_ptr_t) /*_.SYMNAM__V15*/ meltfptr[14]);
	      error
		("MELT [%s]: imported symbol %s has unexpected binding of %s",
			     /*_?*/ meltfram__.loc_CSTRING__o1,
		 ERRBADIMPORT__1_str ? ERRBADIMPORT__1_str : /*_?*/
		 meltfram__.loc_CSTRING__o0,
		 melt_string_str ((melt_ptr_t) /*_.BINDISCRNAM__V17*/
				  meltfptr[16]));
	    } /* ERRBADIMPORT__1 end */ ;
	  }
	  ;
	  MELT_LOCATION ("warmelt-first.melt:2977:/ quasiblock");


   /*_.RETVAL___V1*/ meltfptr[0] = NULL;;

	  {
	    MELT_LOCATION ("warmelt-first.melt:2977:/ locexp");
	    /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
	    if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	      melt_warn_for_no_expected_secondary_results ();
	    /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
	    ;
	  }
	  ;
	  /*^finalreturn */
	  ;
	  /*finalret */ goto labend_rout;
	  /*_.LET___V14*/ meltfptr[13] = /*_.RETURN___V20*/ meltfptr[18];;

	  MELT_LOCATION ("warmelt-first.melt:2964:/ clear");
	     /*clear *//*_.SYMNAM__V15*/ meltfptr[14] = 0;
	  /*^clear */
	     /*clear *//*_.BINDISCR__V16*/ meltfptr[15] = 0;
	  /*^clear */
	     /*clear *//*_.BINDISCRNAM__V17*/ meltfptr[16] = 0;
	  /*^clear */
	     /*clear *//*_.IFCPP___V18*/ meltfptr[17] = 0;
	  /*^clear */
	     /*clear *//*_.RETURN___V20*/ meltfptr[18] = 0;
	  /*_.IF___V13*/ meltfptr[12] = /*_.LET___V14*/ meltfptr[13];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-first.melt:2963:/ clear");
	     /*clear *//*_.LET___V14*/ meltfptr[13] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

  /*_.IF___V13*/ meltfptr[12] = NULL;;
      }
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-first.melt:2979:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L6*/ meltfnum[4] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.VALBIND__V12*/ meltfptr[11]),
			     (melt_ptr_t) (( /*!CLASS_VALUE_BINDING */
					    meltfrout->tabval[3])));;
      MELT_LOCATION ("warmelt-first.melt:2979:/ cond");
      /*cond */ if ( /*_#IS_A__L6*/ meltfnum[4])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V22*/ meltfptr[15] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-first.melt:2979:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check valbind"),
				  ("warmelt-first.melt")
				  ? ("warmelt-first.melt") : __FILE__,
				  (2979) ? (2979) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V22*/ meltfptr[15] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V21*/ meltfptr[14] = /*_.IFELSE___V22*/ meltfptr[15];;
      /*epilog */

      MELT_LOCATION ("warmelt-first.melt:2979:/ clear");
	     /*clear *//*_#IS_A__L6*/ meltfnum[4] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V22*/ meltfptr[15] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V21*/ meltfptr[14] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-first.melt:2980:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.VALBIND__V12*/ meltfptr[11]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 1, "VBIND_VALUE");
  /*_.VBIND_VALUE__V23*/ meltfptr[16] = slot;
    };
    ;
    /*^checksignal */
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.VBIND_VALUE__V23*/ meltfptr[16];;

    {
      MELT_LOCATION ("warmelt-first.melt:2980:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*_.LET___V11*/ meltfptr[9] = /*_.RETURN___V24*/ meltfptr[17];;

    MELT_LOCATION ("warmelt-first.melt:2961:/ clear");
	   /*clear *//*_.VALBIND__V12*/ meltfptr[11] = 0;
    /*^clear */
	   /*clear *//*_#IS_NOT_A__L4*/ meltfnum[1] = 0;
    /*^clear */
	   /*clear *//*_.IF___V13*/ meltfptr[12] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V21*/ meltfptr[14] = 0;
    /*^clear */
	   /*clear *//*_.VBIND_VALUE__V23*/ meltfptr[16] = 0;
    /*^clear */
	   /*clear *//*_.RETURN___V24*/ meltfptr[17] = 0;
    MELT_LOCATION ("warmelt-first.melt:2944:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.LET___V11*/ meltfptr[9];;

    {
      MELT_LOCATION ("warmelt-first.melt:2944:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_#IS_NOT_A__L1*/ meltfnum[0] = 0;
    /*^clear */
	   /*clear *//*_.IF___V4*/ meltfptr[3] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V7*/ meltfptr[4] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V9*/ meltfptr[5] = 0;
    /*^clear */
	   /*clear *//*_.LET___V11*/ meltfptr[9] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("INITVALUE_IMPORTER", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_11_warmelt_first_INITVALUE_IMPORTER_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_11_warmelt_first_INITVALUE_IMPORTER */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_12_warmelt_first_INITMACRO_EXPORTER (meltclosure_ptr_t meltclosp_,
					      melt_ptr_t meltfirstargp_,
					      const melt_argdescr_cell_t
					      meltxargdescr_[],
					      union meltparam_un *
					      meltxargtab_,
					      const melt_argdescr_cell_t
					      meltxresdescr_[],
					      union meltparam_un *
					      meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_12_warmelt_first_INITMACRO_EXPORTER_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_12_warmelt_first_INITMACRO_EXPORTER_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 27
    melt_ptr_t mcfr_varptr[27];
#define MELTFRAM_NBVARNUM 6
    long mcfr_varnum[6];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_12_warmelt_first_INITMACRO_EXPORTER is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_12_warmelt_first_INITMACRO_EXPORTER_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 27; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_12_warmelt_first_INITMACRO_EXPORTER nbval 27*/
  meltfram__.mcfr_nbvar = 27 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("INITMACRO_EXPORTER", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-first.melt:2984:/ getarg");
 /*_.SYM__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.VAL__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.VAL__V3*/ meltfptr[2])) != NULL);


  /*getarg#2 */
  /*^getarg */
  if (meltxargdescr_[1] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.CONTENV__V4*/ meltfptr[3] =
    (meltxargtab_[1].meltbp_aptr) ? (*(meltxargtab_[1].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.CONTENV__V4*/ meltfptr[3])) !=
	      NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-first.melt:2985:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L1*/ meltfnum[0] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.SYM__V2*/ meltfptr[1]),
			     (melt_ptr_t) (( /*!CLASS_SYMBOL */ meltfrout->
					    tabval[0])));;
      MELT_LOCATION ("warmelt-first.melt:2985:/ cond");
      /*cond */ if ( /*_#IS_A__L1*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V6*/ meltfptr[5] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-first.melt:2985:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check sym"),
				  ("warmelt-first.melt")
				  ? ("warmelt-first.melt") : __FILE__,
				  (2985) ? (2985) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V6*/ meltfptr[5] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V5*/ meltfptr[4] = /*_.IFELSE___V6*/ meltfptr[5];;
      /*epilog */

      MELT_LOCATION ("warmelt-first.melt:2985:/ clear");
	     /*clear *//*_#IS_A__L1*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V6*/ meltfptr[5] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V5*/ meltfptr[4] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-first.melt:2986:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#NULL__L2*/ meltfnum[0] =
      (( /*_.CONTENV__V4*/ meltfptr[3]) == NULL);;
    MELT_LOCATION ("warmelt-first.melt:2986:/ cond");
    /*cond */ if ( /*_#NULL__L2*/ meltfnum[0])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-first.melt:2988:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^cond */
	  /*cond */ if (( /*!konst_1 */ meltfrout->tabval[1]))	/*then */
	    {
	      /*^cond.then */
	      /*^block */
	      /*anyblock */
	      {

		MELT_LOCATION ("warmelt-first.melt:2990:/ getslot");
		{
		  melt_ptr_t slot = NULL, obj = NULL;
		  obj = (melt_ptr_t) ( /*_.SYM__V2*/ meltfptr[1]) /*=obj*/ ;
		  melt_object_get_field (slot, obj, 1, "NAMED_NAME");
      /*_.NAMED_NAME__V8*/ meltfptr[7] = slot;
		};
		;

		{
		  MELT_LOCATION ("warmelt-first.melt:2989:/ locexp");
		  warning (0, "MELT WARNING MSG [#%ld]::: %s - %s",
			   melt_dbgcounter,
			   ("exporting macro too early with null environment container"),
			   melt_string_str ((melt_ptr_t)
					    ( /*_.NAMED_NAME__V8*/
					     meltfptr[7])));
		}
		;
		/*epilog */

		MELT_LOCATION ("warmelt-first.melt:2988:/ clear");
	       /*clear *//*_.NAMED_NAME__V8*/ meltfptr[7] = 0;
	      }
	      ;
	    }			/*noelse */
	  ;
	  MELT_LOCATION ("warmelt-first.melt:2991:/ quasiblock");


   /*_.RETVAL___V1*/ meltfptr[0] = NULL;;

	  {
	    MELT_LOCATION ("warmelt-first.melt:2991:/ locexp");
	    /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
	    if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	      melt_warn_for_no_expected_secondary_results ();
	    /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
	    ;
	  }
	  ;
	  /*^finalreturn */
	  ;
	  /*finalret */ goto labend_rout;
	  MELT_LOCATION ("warmelt-first.melt:2987:/ quasiblock");


	  /*_.PROGN___V10*/ meltfptr[9] = /*_.RETURN___V9*/ meltfptr[7];;
	  /*^compute */
	  /*_.IF___V7*/ meltfptr[5] = /*_.PROGN___V10*/ meltfptr[9];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-first.melt:2986:/ clear");
	     /*clear *//*_.RETURN___V9*/ meltfptr[7] = 0;
	  /*^clear */
	     /*clear *//*_.PROGN___V10*/ meltfptr[9] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

  /*_.IF___V7*/ meltfptr[5] = NULL;;
      }
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-first.melt:2993:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L3*/ meltfnum[2] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.CONTENV__V4*/ meltfptr[3]),
			     (melt_ptr_t) (( /*!CLASS_REFERENCE */ meltfrout->
					    tabval[2])));;
      MELT_LOCATION ("warmelt-first.melt:2993:/ cond");
      /*cond */ if ( /*_#IS_A__L3*/ meltfnum[2])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V12*/ meltfptr[9] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-first.melt:2993:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check contenv"),
				  ("warmelt-first.melt")
				  ? ("warmelt-first.melt") : __FILE__,
				  (2993) ? (2993) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V12*/ meltfptr[9] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V11*/ meltfptr[7] = /*_.IFELSE___V12*/ meltfptr[9];;
      /*epilog */

      MELT_LOCATION ("warmelt-first.melt:2993:/ clear");
	     /*clear *//*_#IS_A__L3*/ meltfnum[2] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V12*/ meltfptr[9] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V11*/ meltfptr[7] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-first.melt:2994:/ quasiblock");


    /*^getslot */
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.CONTENV__V4*/ meltfptr[3]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 0, "REFERENCED_VALUE");
  /*_.ENV__V14*/ meltfptr[13] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-first.melt:2995:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#NULL__L4*/ meltfnum[2] =
      (( /*_.ENV__V14*/ meltfptr[13]) == NULL);;
    MELT_LOCATION ("warmelt-first.melt:2995:/ cond");
    /*cond */ if ( /*_#NULL__L4*/ meltfnum[2])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-first.melt:2998:/ getslot");
	  {
	    melt_ptr_t slot = NULL, obj = NULL;
	    obj = (melt_ptr_t) ( /*_.SYM__V2*/ meltfptr[1]) /*=obj*/ ;
	    melt_object_get_field (slot, obj, 1, "NAMED_NAME");
    /*_.NAMED_NAME__V16*/ meltfptr[15] = slot;
	  };
	  ;

	  {
	    MELT_LOCATION ("warmelt-first.melt:2997:/ locexp");
	    inform (UNKNOWN_LOCATION, ("MELT INFORM [#%ld]: %s - %s"),
		    melt_dbgcounter,
		    ("exporting macro too early with null environment"),
		    melt_string_str ((melt_ptr_t)
				     ( /*_.NAMED_NAME__V16*/ meltfptr[15])));
	  }
	  ;
	  MELT_LOCATION ("warmelt-first.melt:2999:/ quasiblock");


   /*_.RETVAL___V1*/ meltfptr[0] = NULL;;

	  {
	    MELT_LOCATION ("warmelt-first.melt:2999:/ locexp");
	    /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
	    if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	      melt_warn_for_no_expected_secondary_results ();
	    /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
	    ;
	  }
	  ;
	  /*^finalreturn */
	  ;
	  /*finalret */ goto labend_rout;
	  MELT_LOCATION ("warmelt-first.melt:2996:/ quasiblock");


	  /*_.PROGN___V18*/ meltfptr[17] = /*_.RETURN___V17*/ meltfptr[16];;
	  /*^compute */
	  /*_.IF___V15*/ meltfptr[14] = /*_.PROGN___V18*/ meltfptr[17];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-first.melt:2995:/ clear");
	     /*clear *//*_.NAMED_NAME__V16*/ meltfptr[15] = 0;
	  /*^clear */
	     /*clear *//*_.RETURN___V17*/ meltfptr[16] = 0;
	  /*^clear */
	     /*clear *//*_.PROGN___V18*/ meltfptr[17] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

  /*_.IF___V15*/ meltfptr[14] = NULL;;
      }
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-first.melt:3001:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L5*/ meltfnum[4] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.ENV__V14*/ meltfptr[13]),
			     (melt_ptr_t) (( /*!CLASS_ENVIRONMENT */
					    meltfrout->tabval[3])));;
      MELT_LOCATION ("warmelt-first.melt:3001:/ cond");
      /*cond */ if ( /*_#IS_A__L5*/ meltfnum[4])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V20*/ meltfptr[16] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-first.melt:3001:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check env"),
				  ("warmelt-first.melt")
				  ? ("warmelt-first.melt") : __FILE__,
				  (3001) ? (3001) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V20*/ meltfptr[16] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V19*/ meltfptr[15] = /*_.IFELSE___V20*/ meltfptr[16];;
      /*epilog */

      MELT_LOCATION ("warmelt-first.melt:3001:/ clear");
	     /*clear *//*_#IS_A__L5*/ meltfnum[4] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V20*/ meltfptr[16] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V19*/ meltfptr[15] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-first.melt:3002:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_CLOSURE__L6*/ meltfnum[4] =
	(melt_magic_discr ((melt_ptr_t) ( /*_.VAL__V3*/ meltfptr[2])) ==
	 MELTOBMAG_CLOSURE);;
      MELT_LOCATION ("warmelt-first.melt:3002:/ cond");
      /*cond */ if ( /*_#IS_CLOSURE__L6*/ meltfnum[4])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V22*/ meltfptr[16] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-first.melt:3002:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check val is closure"),
				  ("warmelt-first.melt")
				  ? ("warmelt-first.melt") : __FILE__,
				  (3002) ? (3002) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V22*/ meltfptr[16] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V21*/ meltfptr[17] = /*_.IFELSE___V22*/ meltfptr[16];;
      /*epilog */

      MELT_LOCATION ("warmelt-first.melt:3002:/ clear");
	     /*clear *//*_#IS_CLOSURE__L6*/ meltfnum[4] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V22*/ meltfptr[16] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V21*/ meltfptr[17] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-first.melt:3003:/ quasiblock");


    /*^checksignal */
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*^rawallocobj */
    /*rawallocobj */
    {
      melt_ptr_t newobj = 0;
      melt_raw_object_create (newobj,
			      (melt_ptr_t) (( /*!CLASS_MACRO_BINDING */
					     meltfrout->tabval[4])), (2),
			      "CLASS_MACRO_BINDING");
  /*_.INST__V25*/ meltfptr[24] =
	newobj;
    };
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @BINDER",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V25*/ meltfptr[24])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V25*/ meltfptr[24]), (0),
			  ( /*_.SYM__V2*/ meltfptr[1]), "BINDER");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @MBIND_EXPANSER",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V25*/ meltfptr[24])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V25*/ meltfptr[24]), (1),
			  ( /*_.VAL__V3*/ meltfptr[2]), "MBIND_EXPANSER");
    ;
    /*^touchobj */

    melt_dbgtrace_written_object ( /*_.INST__V25*/ meltfptr[24],
				  "newly made instance");
    ;
    /*_.MACBIND__V24*/ meltfptr[23] = /*_.INST__V25*/ meltfptr[24];;
    MELT_LOCATION ("warmelt-first.melt:3008:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & /*_.MACBIND__V24*/ meltfptr[23];
      /*_.PUT_ENV__V26*/ meltfptr[25] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!PUT_ENV */ meltfrout->tabval[5])),
		    (melt_ptr_t) ( /*_.ENV__V14*/ meltfptr[13]),
		    (MELTBPARSTR_PTR ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-first.melt:3009:/ quasiblock");


 /*_.RETVAL___V1*/ meltfptr[0] = NULL;;

    {
      MELT_LOCATION ("warmelt-first.melt:3009:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*_.LET___V23*/ meltfptr[16] = /*_.RETURN___V27*/ meltfptr[26];;

    MELT_LOCATION ("warmelt-first.melt:3003:/ clear");
	   /*clear *//*_.MACBIND__V24*/ meltfptr[23] = 0;
    /*^clear */
	   /*clear *//*_.PUT_ENV__V26*/ meltfptr[25] = 0;
    /*^clear */
	   /*clear *//*_.RETURN___V27*/ meltfptr[26] = 0;
    /*_.LET___V13*/ meltfptr[9] = /*_.LET___V23*/ meltfptr[16];;

    MELT_LOCATION ("warmelt-first.melt:2994:/ clear");
	   /*clear *//*_.ENV__V14*/ meltfptr[13] = 0;
    /*^clear */
	   /*clear *//*_#NULL__L4*/ meltfnum[2] = 0;
    /*^clear */
	   /*clear *//*_.IF___V15*/ meltfptr[14] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V19*/ meltfptr[15] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V21*/ meltfptr[17] = 0;
    /*^clear */
	   /*clear *//*_.LET___V23*/ meltfptr[16] = 0;
    MELT_LOCATION ("warmelt-first.melt:2984:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.LET___V13*/ meltfptr[9];;

    {
      MELT_LOCATION ("warmelt-first.melt:2984:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V5*/ meltfptr[4] = 0;
    /*^clear */
	   /*clear *//*_#NULL__L2*/ meltfnum[0] = 0;
    /*^clear */
	   /*clear *//*_.IF___V7*/ meltfptr[5] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V11*/ meltfptr[7] = 0;
    /*^clear */
	   /*clear *//*_.LET___V13*/ meltfptr[9] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("INITMACRO_EXPORTER", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_12_warmelt_first_INITMACRO_EXPORTER_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_12_warmelt_first_INITMACRO_EXPORTER */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_13_warmelt_first_INITPATMACRO_EXPORTER (meltclosure_ptr_t meltclosp_,
						 melt_ptr_t meltfirstargp_,
						 const melt_argdescr_cell_t
						 meltxargdescr_[],
						 union meltparam_un *
						 meltxargtab_,
						 const melt_argdescr_cell_t
						 meltxresdescr_[],
						 union meltparam_un *
						 meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_13_warmelt_first_INITPATMACRO_EXPORTER_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_13_warmelt_first_INITPATMACRO_EXPORTER_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 29
    melt_ptr_t mcfr_varptr[29];
#define MELTFRAM_NBVARNUM 7
    long mcfr_varnum[7];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_13_warmelt_first_INITPATMACRO_EXPORTER is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_13_warmelt_first_INITPATMACRO_EXPORTER_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 29; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_13_warmelt_first_INITPATMACRO_EXPORTER nbval 29*/
  meltfram__.mcfr_nbvar = 29 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("INITPATMACRO_EXPORTER", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-first.melt:3013:/ getarg");
 /*_.SYM__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.MACVAL__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.MACVAL__V3*/ meltfptr[2])) !=
	      NULL);


  /*getarg#2 */
  /*^getarg */
  if (meltxargdescr_[1] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.PATVAL__V4*/ meltfptr[3] =
    (meltxargtab_[1].meltbp_aptr) ? (*(meltxargtab_[1].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.PATVAL__V4*/ meltfptr[3])) !=
	      NULL);


  /*getarg#3 */
  /*^getarg */
  if (meltxargdescr_[2] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.CONTENV__V5*/ meltfptr[4] =
    (meltxargtab_[2].meltbp_aptr) ? (*(meltxargtab_[2].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.CONTENV__V5*/ meltfptr[4])) !=
	      NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-first.melt:3014:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L1*/ meltfnum[0] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.SYM__V2*/ meltfptr[1]),
			     (melt_ptr_t) (( /*!CLASS_SYMBOL */ meltfrout->
					    tabval[0])));;
      MELT_LOCATION ("warmelt-first.melt:3014:/ cond");
      /*cond */ if ( /*_#IS_A__L1*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V7*/ meltfptr[6] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-first.melt:3014:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check sym"),
				  ("warmelt-first.melt")
				  ? ("warmelt-first.melt") : __FILE__,
				  (3014) ? (3014) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V7*/ meltfptr[6] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V6*/ meltfptr[5] = /*_.IFELSE___V7*/ meltfptr[6];;
      /*epilog */

      MELT_LOCATION ("warmelt-first.melt:3014:/ clear");
	     /*clear *//*_#IS_A__L1*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V7*/ meltfptr[6] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V6*/ meltfptr[5] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-first.melt:3015:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#NULL__L2*/ meltfnum[0] =
      (( /*_.CONTENV__V5*/ meltfptr[4]) == NULL);;
    MELT_LOCATION ("warmelt-first.melt:3015:/ cond");
    /*cond */ if ( /*_#NULL__L2*/ meltfnum[0])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-first.melt:3020:/ quasiblock");


   /*_.RETVAL___V1*/ meltfptr[0] = NULL;;

	  {
	    MELT_LOCATION ("warmelt-first.melt:3020:/ locexp");
	    /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
	    if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	      melt_warn_for_no_expected_secondary_results ();
	    /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
	    ;
	  }
	  ;
	  /*^finalreturn */
	  ;
	  /*finalret */ goto labend_rout;
	  MELT_LOCATION ("warmelt-first.melt:3016:/ quasiblock");


	  /*_.PROGN___V10*/ meltfptr[9] = /*_.RETURN___V9*/ meltfptr[8];;
	  /*^compute */
	  /*_.IF___V8*/ meltfptr[6] = /*_.PROGN___V10*/ meltfptr[9];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-first.melt:3015:/ clear");
	     /*clear *//*_.RETURN___V9*/ meltfptr[8] = 0;
	  /*^clear */
	     /*clear *//*_.PROGN___V10*/ meltfptr[9] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

  /*_.IF___V8*/ meltfptr[6] = NULL;;
      }
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-first.melt:3022:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L3*/ meltfnum[2] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.CONTENV__V5*/ meltfptr[4]),
			     (melt_ptr_t) (( /*!CLASS_REFERENCE */ meltfrout->
					    tabval[1])));;
      MELT_LOCATION ("warmelt-first.melt:3022:/ cond");
      /*cond */ if ( /*_#IS_A__L3*/ meltfnum[2])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V12*/ meltfptr[9] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-first.melt:3022:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check contenv"),
				  ("warmelt-first.melt")
				  ? ("warmelt-first.melt") : __FILE__,
				  (3022) ? (3022) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V12*/ meltfptr[9] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V11*/ meltfptr[8] = /*_.IFELSE___V12*/ meltfptr[9];;
      /*epilog */

      MELT_LOCATION ("warmelt-first.melt:3022:/ clear");
	     /*clear *//*_#IS_A__L3*/ meltfnum[2] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V12*/ meltfptr[9] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V11*/ meltfptr[8] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-first.melt:3023:/ quasiblock");


    /*^getslot */
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.CONTENV__V5*/ meltfptr[4]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 0, "REFERENCED_VALUE");
  /*_.ENV__V14*/ meltfptr[13] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-first.melt:3024:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#NULL__L4*/ meltfnum[2] =
      (( /*_.ENV__V14*/ meltfptr[13]) == NULL);;
    MELT_LOCATION ("warmelt-first.melt:3024:/ cond");
    /*cond */ if ( /*_#NULL__L4*/ meltfnum[2])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-first.melt:3027:/ getslot");
	  {
	    melt_ptr_t slot = NULL, obj = NULL;
	    obj = (melt_ptr_t) ( /*_.SYM__V2*/ meltfptr[1]) /*=obj*/ ;
	    melt_object_get_field (slot, obj, 1, "NAMED_NAME");
    /*_.NAMED_NAME__V16*/ meltfptr[15] = slot;
	  };
	  ;

	  {
	    MELT_LOCATION ("warmelt-first.melt:3026:/ locexp");
	    inform (UNKNOWN_LOCATION, ("MELT INFORM [#%ld]: %s - %s"),
		    melt_dbgcounter,
		    ("exporting patmacro too early with null environment"),
		    melt_string_str ((melt_ptr_t)
				     ( /*_.NAMED_NAME__V16*/ meltfptr[15])));
	  }
	  ;
	  MELT_LOCATION ("warmelt-first.melt:3028:/ quasiblock");


   /*_.RETVAL___V1*/ meltfptr[0] = NULL;;

	  {
	    MELT_LOCATION ("warmelt-first.melt:3028:/ locexp");
	    /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
	    if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	      melt_warn_for_no_expected_secondary_results ();
	    /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
	    ;
	  }
	  ;
	  /*^finalreturn */
	  ;
	  /*finalret */ goto labend_rout;
	  MELT_LOCATION ("warmelt-first.melt:3025:/ quasiblock");


	  /*_.PROGN___V18*/ meltfptr[17] = /*_.RETURN___V17*/ meltfptr[16];;
	  /*^compute */
	  /*_.IF___V15*/ meltfptr[14] = /*_.PROGN___V18*/ meltfptr[17];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-first.melt:3024:/ clear");
	     /*clear *//*_.NAMED_NAME__V16*/ meltfptr[15] = 0;
	  /*^clear */
	     /*clear *//*_.RETURN___V17*/ meltfptr[16] = 0;
	  /*^clear */
	     /*clear *//*_.PROGN___V18*/ meltfptr[17] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

  /*_.IF___V15*/ meltfptr[14] = NULL;;
      }
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-first.melt:3030:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L5*/ meltfnum[4] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.ENV__V14*/ meltfptr[13]),
			     (melt_ptr_t) (( /*!CLASS_ENVIRONMENT */
					    meltfrout->tabval[2])));;
      MELT_LOCATION ("warmelt-first.melt:3030:/ cond");
      /*cond */ if ( /*_#IS_A__L5*/ meltfnum[4])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V20*/ meltfptr[16] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-first.melt:3030:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check env"),
				  ("warmelt-first.melt")
				  ? ("warmelt-first.melt") : __FILE__,
				  (3030) ? (3030) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V20*/ meltfptr[16] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V19*/ meltfptr[15] = /*_.IFELSE___V20*/ meltfptr[16];;
      /*epilog */

      MELT_LOCATION ("warmelt-first.melt:3030:/ clear");
	     /*clear *//*_#IS_A__L5*/ meltfnum[4] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V20*/ meltfptr[16] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V19*/ meltfptr[15] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-first.melt:3031:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_CLOSURE__L6*/ meltfnum[4] =
	(melt_magic_discr ((melt_ptr_t) ( /*_.MACVAL__V3*/ meltfptr[2])) ==
	 MELTOBMAG_CLOSURE);;
      MELT_LOCATION ("warmelt-first.melt:3031:/ cond");
      /*cond */ if ( /*_#IS_CLOSURE__L6*/ meltfnum[4])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V22*/ meltfptr[16] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-first.melt:3031:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check macval is closure"),
				  ("warmelt-first.melt")
				  ? ("warmelt-first.melt") : __FILE__,
				  (3031) ? (3031) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V22*/ meltfptr[16] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V21*/ meltfptr[17] = /*_.IFELSE___V22*/ meltfptr[16];;
      /*epilog */

      MELT_LOCATION ("warmelt-first.melt:3031:/ clear");
	     /*clear *//*_#IS_CLOSURE__L6*/ meltfnum[4] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V22*/ meltfptr[16] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V21*/ meltfptr[17] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-first.melt:3032:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_CLOSURE__L7*/ meltfnum[4] =
	(melt_magic_discr ((melt_ptr_t) ( /*_.PATVAL__V4*/ meltfptr[3])) ==
	 MELTOBMAG_CLOSURE);;
      MELT_LOCATION ("warmelt-first.melt:3032:/ cond");
      /*cond */ if ( /*_#IS_CLOSURE__L7*/ meltfnum[4])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V24*/ meltfptr[23] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-first.melt:3032:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check patval is closure"),
				  ("warmelt-first.melt")
				  ? ("warmelt-first.melt") : __FILE__,
				  (3032) ? (3032) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V24*/ meltfptr[23] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V23*/ meltfptr[16] = /*_.IFELSE___V24*/ meltfptr[23];;
      /*epilog */

      MELT_LOCATION ("warmelt-first.melt:3032:/ clear");
	     /*clear *//*_#IS_CLOSURE__L7*/ meltfnum[4] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V24*/ meltfptr[23] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V23*/ meltfptr[16] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-first.melt:3033:/ quasiblock");


    /*^checksignal */
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*^rawallocobj */
    /*rawallocobj */
    {
      melt_ptr_t newobj = 0;
      melt_raw_object_create (newobj,
			      (melt_ptr_t) (( /*!CLASS_PATMACRO_BINDING */
					     meltfrout->tabval[3])), (3),
			      "CLASS_PATMACRO_BINDING");
  /*_.INST__V27*/ meltfptr[26] =
	newobj;
    };
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @BINDER",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V27*/ meltfptr[26])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V27*/ meltfptr[26]), (0),
			  ( /*_.SYM__V2*/ meltfptr[1]), "BINDER");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @MBIND_EXPANSER",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V27*/ meltfptr[26])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V27*/ meltfptr[26]), (1),
			  ( /*_.MACVAL__V3*/ meltfptr[2]), "MBIND_EXPANSER");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @PATBIND_EXPANSER",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V27*/ meltfptr[26])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V27*/ meltfptr[26]), (2),
			  ( /*_.PATVAL__V4*/ meltfptr[3]),
			  "PATBIND_EXPANSER");
    ;
    /*^touchobj */

    melt_dbgtrace_written_object ( /*_.INST__V27*/ meltfptr[26],
				  "newly made instance");
    ;
    /*_.MACBIND__V26*/ meltfptr[25] = /*_.INST__V27*/ meltfptr[26];;
    MELT_LOCATION ("warmelt-first.melt:3039:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & /*_.MACBIND__V26*/ meltfptr[25];
      /*_.PUT_ENV__V28*/ meltfptr[27] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!PUT_ENV */ meltfrout->tabval[4])),
		    (melt_ptr_t) ( /*_.ENV__V14*/ meltfptr[13]),
		    (MELTBPARSTR_PTR ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-first.melt:3040:/ quasiblock");


 /*_.RETVAL___V1*/ meltfptr[0] = NULL;;

    {
      MELT_LOCATION ("warmelt-first.melt:3040:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*_.LET___V25*/ meltfptr[23] = /*_.RETURN___V29*/ meltfptr[28];;

    MELT_LOCATION ("warmelt-first.melt:3033:/ clear");
	   /*clear *//*_.MACBIND__V26*/ meltfptr[25] = 0;
    /*^clear */
	   /*clear *//*_.PUT_ENV__V28*/ meltfptr[27] = 0;
    /*^clear */
	   /*clear *//*_.RETURN___V29*/ meltfptr[28] = 0;
    /*_.LET___V13*/ meltfptr[9] = /*_.LET___V25*/ meltfptr[23];;

    MELT_LOCATION ("warmelt-first.melt:3023:/ clear");
	   /*clear *//*_.ENV__V14*/ meltfptr[13] = 0;
    /*^clear */
	   /*clear *//*_#NULL__L4*/ meltfnum[2] = 0;
    /*^clear */
	   /*clear *//*_.IF___V15*/ meltfptr[14] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V19*/ meltfptr[15] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V21*/ meltfptr[17] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V23*/ meltfptr[16] = 0;
    /*^clear */
	   /*clear *//*_.LET___V25*/ meltfptr[23] = 0;
    MELT_LOCATION ("warmelt-first.melt:3013:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.LET___V13*/ meltfptr[9];;

    {
      MELT_LOCATION ("warmelt-first.melt:3013:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V6*/ meltfptr[5] = 0;
    /*^clear */
	   /*clear *//*_#NULL__L2*/ meltfnum[0] = 0;
    /*^clear */
	   /*clear *//*_.IF___V8*/ meltfptr[6] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V11*/ meltfptr[8] = 0;
    /*^clear */
	   /*clear *//*_.LET___V13*/ meltfptr[9] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("INITPATMACRO_EXPORTER", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_13_warmelt_first_INITPATMACRO_EXPORTER_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_13_warmelt_first_INITPATMACRO_EXPORTER */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_14_warmelt_first_INIT_EXITFINALIZER (meltclosure_ptr_t meltclosp_,
					      melt_ptr_t meltfirstargp_,
					      const melt_argdescr_cell_t
					      meltxargdescr_[],
					      union meltparam_un *
					      meltxargtab_,
					      const melt_argdescr_cell_t
					      meltxresdescr_[],
					      union meltparam_un *
					      meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_14_warmelt_first_INIT_EXITFINALIZER_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_14_warmelt_first_INIT_EXITFINALIZER_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 16
    melt_ptr_t mcfr_varptr[16];
#define MELTFRAM_NBVARNUM 1
    long mcfr_varnum[1];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_14_warmelt_first_INIT_EXITFINALIZER is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_14_warmelt_first_INIT_EXITFINALIZER_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 16; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_14_warmelt_first_INIT_EXITFINALIZER nbval 16*/
  meltfram__.mcfr_nbvar = 16 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("INIT_EXITFINALIZER", meltcallcount);
/*getargs*/
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
  MELT_LOCATION ("warmelt-first.melt:3063:/ block");
/*anyblock*/
  {


    {
      MELT_LOCATION ("warmelt-first.melt:3065:/ locexp");
      debugeprintf ("init_exitfinalizer INITSTARTCHK__1 start clock %ld",
		    (long) clock ());
      ;
    }
    ;
    MELT_LOCATION ("warmelt-first.melt:3067:/ quasiblock");


    /*^getslot */
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj =
	(melt_ptr_t) (( /*!FINAL_DELAYED_QUEUE */ meltfrout->
		       tabval[0])) /*=obj*/ ;
      melt_object_get_field (slot, obj, 2, "DELQU_FIRST");
  /*_.FIRSTLIST__V2*/ meltfptr[1] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-first.melt:3068:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj =
	(melt_ptr_t) (( /*!FINAL_DELAYED_QUEUE */ meltfrout->
		       tabval[0])) /*=obj*/ ;
      melt_object_get_field (slot, obj, 3, "DELQU_LAST");
  /*_.LASTLIST__V3*/ meltfptr[2] = slot;
    };
    ;
 /*_.REVLASTLIST__V4*/ meltfptr[3] =
      (meltgc_new_list
       ((meltobject_ptr_t) (( /*!DISCR_LIST */ meltfrout->tabval[1]))));;
    MELT_LOCATION ("warmelt-first.melt:3070:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*^rawallocobj */
    /*rawallocobj */
    {
      melt_ptr_t newobj = 0;
      melt_raw_object_create (newobj,
			      (melt_ptr_t) (( /*!CLASS_REFERENCE */
					     meltfrout->tabval[2])), (1),
			      "CLASS_REFERENCE");
  /*_.INST__V6*/ meltfptr[5] =
	newobj;
    };
    ;
    /*^touchobj */

    melt_dbgtrace_written_object ( /*_.INST__V6*/ meltfptr[5],
				  "newly made instance");
    ;
    /*_.RESCONT__V5*/ meltfptr[4] = /*_.INST__V6*/ meltfptr[5];;
    /*citerblock FOREACH_IN_LIST */
    {
      /* start foreach_in_list meltcit1__EACHLIST */
      for ( /*_.CURPAIR__V7*/ meltfptr[6] =
	   melt_list_first ((melt_ptr_t) /*_.FIRSTLIST__V2*/ meltfptr[1]);
	   melt_magic_discr ((melt_ptr_t) /*_.CURPAIR__V7*/ meltfptr[6]) ==
	   MELTOBMAG_PAIR;
	   /*_.CURPAIR__V7*/ meltfptr[6] =
	   melt_pair_tail ((melt_ptr_t) /*_.CURPAIR__V7*/ meltfptr[6]))
	{
	  /*_.FIRSTPROC__V8*/ meltfptr[7] =
	    melt_pair_head ((melt_ptr_t) /*_.CURPAIR__V7*/ meltfptr[6]);



	  {
	    MELT_LOCATION ("warmelt-first.melt:3076:/ locexp");
	    debugeprintf ("init_exitfinalizer FIRSTROUTCHK__1 firstproc=%p",
			  (void *) /*_.FIRSTPROC__V8*/ meltfptr[7]);
	    ;
	  }
	  ;
	  MELT_LOCATION ("warmelt-first.melt:3078:/ quasiblock");


	  /*^getslot */
	  {
	    melt_ptr_t slot = NULL, obj = NULL;
	    obj = (melt_ptr_t) ( /*_.RESCONT__V5*/ meltfptr[4]) /*=obj*/ ;
	    melt_object_get_field (slot, obj, 0, "REFERENCED_VALUE");
   /*_.PREVRES__V9*/ meltfptr[8] = slot;
	  };
	  ;
	  MELT_LOCATION ("warmelt-first.melt:3079:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^apply */
	  /*apply */
	  {
	    union meltparam_un argtab[1];
	    memset (&argtab, 0, sizeof (argtab));
	    /*^apply.arg */
	    argtab[0].meltbp_aptr =
	      (melt_ptr_t *) & ( /*!FINAL_DELAYED_QUEUE */ meltfrout->
				tabval[0]);
	    /*_.NEXTRES__V10*/ meltfptr[9] =
	      melt_apply ((meltclosure_ptr_t)
			  ( /*_.FIRSTPROC__V8*/ meltfptr[7]),
			  (melt_ptr_t) ( /*_.PREVRES__V9*/ meltfptr[8]),
			  (MELTBPARSTR_PTR ""), argtab, "",
			  (union meltparam_un *) 0);
	  }
	  ;
	  MELT_LOCATION ("warmelt-first.melt:3081:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^quasiblock */


	  /*^putslot */
	  /*putslot */
	  melt_assertmsg ("putslot checkobj @REFERENCED_VALUE",
			  melt_magic_discr ((melt_ptr_t)
					    ( /*_.RESCONT__V5*/ meltfptr[4]))
			  == MELTOBMAG_OBJECT);
	  melt_putfield_object (( /*_.RESCONT__V5*/ meltfptr[4]), (0),
				( /*_.NEXTRES__V10*/ meltfptr[9]),
				"REFERENCED_VALUE");
	  ;
	  /*^touch */
	  meltgc_touch ( /*_.RESCONT__V5*/ meltfptr[4]);
	  ;
	  /*^touchobj */

	  melt_dbgtrace_written_object ( /*_.RESCONT__V5*/ meltfptr[4],
					"put-fields");
	  ;


	  MELT_LOCATION ("warmelt-first.melt:3078:/ clear");
	    /*clear *//*_.PREVRES__V9*/ meltfptr[8] = 0;
	  /*^clear */
	    /*clear *//*_.NEXTRES__V10*/ meltfptr[9] = 0;
	}			/* end foreach_in_list meltcit1__EACHLIST */
     /*_.CURPAIR__V7*/ meltfptr[6] = NULL;
     /*_.FIRSTPROC__V8*/ meltfptr[7] = NULL;


      /*citerepilog */

      MELT_LOCATION ("warmelt-first.melt:3073:/ clear");
	    /*clear *//*_.CURPAIR__V7*/ meltfptr[6] = 0;
      /*^clear */
	    /*clear *//*_.FIRSTPROC__V8*/ meltfptr[7] = 0;
    }				/*endciterblock FOREACH_IN_LIST */
    ;
    /*citerblock FOREACH_IN_LIST */
    {
      /* start foreach_in_list meltcit2__EACHLIST */
      for ( /*_.CURPAIR__V11*/ meltfptr[8] =
	   melt_list_first ((melt_ptr_t) /*_.LASTLIST__V3*/ meltfptr[2]);
	   melt_magic_discr ((melt_ptr_t) /*_.CURPAIR__V11*/ meltfptr[8]) ==
	   MELTOBMAG_PAIR;
	   /*_.CURPAIR__V11*/ meltfptr[8] =
	   melt_pair_tail ((melt_ptr_t) /*_.CURPAIR__V11*/ meltfptr[8]))
	{
	  /*_.LASTPROC__V12*/ meltfptr[9] =
	    melt_pair_head ((melt_ptr_t) /*_.CURPAIR__V11*/ meltfptr[8]);



	  {
	    MELT_LOCATION ("warmelt-first.melt:3087:/ locexp");
	    debugeprintf ("init_exitfinalizer LASTREVCHK__1 lastproc=%p",
			  (void *) /*_.LASTPROC__V12*/ meltfptr[9]);
	    ;
	  }
	  ;
	  MELT_LOCATION ("warmelt-first.melt:3089:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
  /*_#IS_CLOSURE__L1*/ meltfnum[0] =
	    (melt_magic_discr
	     ((melt_ptr_t) ( /*_.LASTPROC__V12*/ meltfptr[9])) ==
	     MELTOBMAG_CLOSURE);;
	  MELT_LOCATION ("warmelt-first.melt:3089:/ cond");
	  /*cond */ if ( /*_#IS_CLOSURE__L1*/ meltfnum[0])	/*then */
	    {
	      /*^cond.then */
	      /*^block */
	      /*anyblock */
	      {


		{
		  /*^locexp */
		  meltgc_prepend_list ((melt_ptr_t)
				       ( /*_.REVLASTLIST__V4*/ meltfptr[3]),
				       (melt_ptr_t) ( /*_.LASTPROC__V12*/
						     meltfptr[9]));
		}
		;
		/*epilog */
	      }
	      ;
	    }			/*noelse */
	  ;
	}			/* end foreach_in_list meltcit2__EACHLIST */
     /*_.CURPAIR__V11*/ meltfptr[8] = NULL;
     /*_.LASTPROC__V12*/ meltfptr[9] = NULL;


      /*citerepilog */

      MELT_LOCATION ("warmelt-first.melt:3084:/ clear");
	    /*clear *//*_.CURPAIR__V11*/ meltfptr[8] = 0;
      /*^clear */
	    /*clear *//*_.LASTPROC__V12*/ meltfptr[9] = 0;
      /*^clear */
	    /*clear *//*_#IS_CLOSURE__L1*/ meltfnum[0] = 0;
    }				/*endciterblock FOREACH_IN_LIST */
    ;
    /*citerblock FOREACH_IN_LIST */
    {
      /* start foreach_in_list meltcit3__EACHLIST */
      for ( /*_.CURPAIR__V13*/ meltfptr[12] =
	   melt_list_first ((melt_ptr_t) /*_.REVLASTLIST__V4*/ meltfptr[3]);
	   melt_magic_discr ((melt_ptr_t) /*_.CURPAIR__V13*/ meltfptr[12]) ==
	   MELTOBMAG_PAIR;
	   /*_.CURPAIR__V13*/ meltfptr[12] =
	   melt_pair_tail ((melt_ptr_t) /*_.CURPAIR__V13*/ meltfptr[12]))
	{
	  /*_.LASTPROC__V14*/ meltfptr[13] =
	    melt_pair_head ((melt_ptr_t) /*_.CURPAIR__V13*/ meltfptr[12]);



	  {
	    MELT_LOCATION ("warmelt-first.melt:3094:/ locexp");
	    debugeprintf ("init_exitfinalizer LASTREVPROCCHK__1 lastproc=%p",
			  (void *) /*_.LASTPROC__V14*/ meltfptr[13]);
	    ;
	  }
	  ;
	  MELT_LOCATION ("warmelt-first.melt:3096:/ quasiblock");


	  /*^getslot */
	  {
	    melt_ptr_t slot = NULL, obj = NULL;
	    obj = (melt_ptr_t) ( /*_.RESCONT__V5*/ meltfptr[4]) /*=obj*/ ;
	    melt_object_get_field (slot, obj, 0, "REFERENCED_VALUE");
   /*_.PREVRES__V15*/ meltfptr[14] = slot;
	  };
	  ;
	  MELT_LOCATION ("warmelt-first.melt:3097:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^apply */
	  /*apply */
	  {
	    union meltparam_un argtab[1];
	    memset (&argtab, 0, sizeof (argtab));
	    /*^apply.arg */
	    argtab[0].meltbp_aptr =
	      (melt_ptr_t *) & ( /*!FINAL_DELAYED_QUEUE */ meltfrout->
				tabval[0]);
	    /*_.NEXTRES__V16*/ meltfptr[15] =
	      melt_apply ((meltclosure_ptr_t)
			  ( /*_.LASTPROC__V14*/ meltfptr[13]),
			  (melt_ptr_t) ( /*_.PREVRES__V15*/ meltfptr[14]),
			  (MELTBPARSTR_PTR ""), argtab, "",
			  (union meltparam_un *) 0);
	  }
	  ;
	  MELT_LOCATION ("warmelt-first.melt:3099:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^quasiblock */


	  /*^putslot */
	  /*putslot */
	  melt_assertmsg ("putslot checkobj @REFERENCED_VALUE",
			  melt_magic_discr ((melt_ptr_t)
					    ( /*_.RESCONT__V5*/ meltfptr[4]))
			  == MELTOBMAG_OBJECT);
	  melt_putfield_object (( /*_.RESCONT__V5*/ meltfptr[4]), (0),
				( /*_.NEXTRES__V16*/ meltfptr[15]),
				"REFERENCED_VALUE");
	  ;
	  /*^touch */
	  meltgc_touch ( /*_.RESCONT__V5*/ meltfptr[4]);
	  ;
	  /*^touchobj */

	  melt_dbgtrace_written_object ( /*_.RESCONT__V5*/ meltfptr[4],
					"put-fields");
	  ;


	  MELT_LOCATION ("warmelt-first.melt:3096:/ clear");
	    /*clear *//*_.PREVRES__V15*/ meltfptr[14] = 0;
	  /*^clear */
	    /*clear *//*_.NEXTRES__V16*/ meltfptr[15] = 0;
	}			/* end foreach_in_list meltcit3__EACHLIST */
     /*_.CURPAIR__V13*/ meltfptr[12] = NULL;
     /*_.LASTPROC__V14*/ meltfptr[13] = NULL;


      /*citerepilog */

      MELT_LOCATION ("warmelt-first.melt:3091:/ clear");
	    /*clear *//*_.CURPAIR__V13*/ meltfptr[12] = 0;
      /*^clear */
	    /*clear *//*_.LASTPROC__V14*/ meltfptr[13] = 0;
    }				/*endciterblock FOREACH_IN_LIST */
    ;

    {
      MELT_LOCATION ("warmelt-first.melt:3102:/ locexp");
      debugeprintf ("init_exitfinalizer INITENDCHK__1 start clock %ld",
		    (long) clock ());
      ;
    }
    ;

    MELT_LOCATION ("warmelt-first.melt:3067:/ clear");
	   /*clear *//*_.FIRSTLIST__V2*/ meltfptr[1] = 0;
    /*^clear */
	   /*clear *//*_.LASTLIST__V3*/ meltfptr[2] = 0;
    /*^clear */
	   /*clear *//*_.REVLASTLIST__V4*/ meltfptr[3] = 0;
    /*^clear */
	   /*clear *//*_.RESCONT__V5*/ meltfptr[4] = 0;
    MELT_LOCATION ("warmelt-first.melt:3063:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*epilog */
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("INIT_EXITFINALIZER", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_14_warmelt_first_INIT_EXITFINALIZER_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*noretval */ NULL);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_14_warmelt_first_INIT_EXITFINALIZER */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_15_warmelt_first_AT_EXIT_FIRST (meltclosure_ptr_t meltclosp_,
					 melt_ptr_t meltfirstargp_,
					 const melt_argdescr_cell_t
					 meltxargdescr_[],
					 union meltparam_un * meltxargtab_,
					 const melt_argdescr_cell_t
					 meltxresdescr_[],
					 union meltparam_un * meltxrestab_)
{
  long current_blocklevel_signals_meltrout_15_warmelt_first_AT_EXIT_FIRST_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_15_warmelt_first_AT_EXIT_FIRST_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 3
    melt_ptr_t mcfr_varptr[3];
#define MELTFRAM_NBVARNUM 1
    long mcfr_varnum[1];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_15_warmelt_first_AT_EXIT_FIRST is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_15_warmelt_first_AT_EXIT_FIRST_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 3; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_15_warmelt_first_AT_EXIT_FIRST nbval 3*/
  meltfram__.mcfr_nbvar = 3 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("AT_EXIT_FIRST", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-first.melt:3107:/ getarg");
 /*_.FUN__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {

    MELT_LOCATION ("warmelt-first.melt:3110:/ quasiblock");


    /*^getslot */
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj =
	(melt_ptr_t) (( /*!FINAL_DELAYED_QUEUE */ meltfrout->
		       tabval[0])) /*=obj*/ ;
      melt_object_get_field (slot, obj, 2, "DELQU_FIRST");
  /*_.FIRSTLIST__V3*/ meltfptr[2] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-first.melt:3112:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#IS_CLOSURE__L1*/ meltfnum[0] =
      (melt_magic_discr ((melt_ptr_t) ( /*_.FUN__V2*/ meltfptr[1])) ==
       MELTOBMAG_CLOSURE);;
    MELT_LOCATION ("warmelt-first.melt:3112:/ cond");
    /*cond */ if ( /*_#IS_CLOSURE__L1*/ meltfnum[0])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{


	  {
	    /*^locexp */
	    meltgc_append_list ((melt_ptr_t)
				( /*_.FIRSTLIST__V3*/ meltfptr[2]),
				(melt_ptr_t) ( /*_.FUN__V2*/ meltfptr[1]));
	  }
	  ;
	  /*epilog */
	}
	;
      }				/*noelse */
    ;

    MELT_LOCATION ("warmelt-first.melt:3110:/ clear");
	   /*clear *//*_.FIRSTLIST__V3*/ meltfptr[2] = 0;
    /*^clear */
	   /*clear *//*_#IS_CLOSURE__L1*/ meltfnum[0] = 0;
    /*epilog */
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("AT_EXIT_FIRST", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_15_warmelt_first_AT_EXIT_FIRST_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*noretval */ NULL);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_15_warmelt_first_AT_EXIT_FIRST */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_16_warmelt_first_AT_EXIT_LAST (meltclosure_ptr_t meltclosp_,
					melt_ptr_t meltfirstargp_,
					const melt_argdescr_cell_t
					meltxargdescr_[],
					union meltparam_un * meltxargtab_,
					const melt_argdescr_cell_t
					meltxresdescr_[],
					union meltparam_un * meltxrestab_)
{
  long current_blocklevel_signals_meltrout_16_warmelt_first_AT_EXIT_LAST_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_16_warmelt_first_AT_EXIT_LAST_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 3
    melt_ptr_t mcfr_varptr[3];
#define MELTFRAM_NBVARNUM 1
    long mcfr_varnum[1];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_16_warmelt_first_AT_EXIT_LAST is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_16_warmelt_first_AT_EXIT_LAST_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 3; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_16_warmelt_first_AT_EXIT_LAST nbval 3*/
  meltfram__.mcfr_nbvar = 3 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("AT_EXIT_LAST", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-first.melt:3115:/ getarg");
 /*_.FUN__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {

    MELT_LOCATION ("warmelt-first.melt:3118:/ quasiblock");


    /*^getslot */
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj =
	(melt_ptr_t) (( /*!FINAL_DELAYED_QUEUE */ meltfrout->
		       tabval[0])) /*=obj*/ ;
      melt_object_get_field (slot, obj, 3, "DELQU_LAST");
  /*_.LASTLIST__V3*/ meltfptr[2] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-first.melt:3120:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#IS_CLOSURE__L1*/ meltfnum[0] =
      (melt_magic_discr ((melt_ptr_t) ( /*_.FUN__V2*/ meltfptr[1])) ==
       MELTOBMAG_CLOSURE);;
    MELT_LOCATION ("warmelt-first.melt:3120:/ cond");
    /*cond */ if ( /*_#IS_CLOSURE__L1*/ meltfnum[0])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{


	  {
	    /*^locexp */
	    meltgc_append_list ((melt_ptr_t)
				( /*_.LASTLIST__V3*/ meltfptr[2]),
				(melt_ptr_t) ( /*_.FUN__V2*/ meltfptr[1]));
	  }
	  ;
	  /*epilog */
	}
	;
      }				/*noelse */
    ;

    MELT_LOCATION ("warmelt-first.melt:3118:/ clear");
	   /*clear *//*_.LASTLIST__V3*/ meltfptr[2] = 0;
    /*^clear */
	   /*clear *//*_#IS_CLOSURE__L1*/ meltfnum[0] = 0;
    /*epilog */
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("AT_EXIT_LAST", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_16_warmelt_first_AT_EXIT_LAST_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*noretval */ NULL);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_16_warmelt_first_AT_EXIT_LAST */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_17_warmelt_first_END_MELT_PASS_RUNNER (meltclosure_ptr_t meltclosp_,
						melt_ptr_t meltfirstargp_,
						const melt_argdescr_cell_t
						meltxargdescr_[],
						union meltparam_un *
						meltxargtab_,
						const melt_argdescr_cell_t
						meltxresdescr_[],
						union meltparam_un *
						meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_17_warmelt_first_END_MELT_PASS_RUNNER_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_17_warmelt_first_END_MELT_PASS_RUNNER_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 19
    melt_ptr_t mcfr_varptr[19];
#define MELTFRAM_NBVARNUM 1
    long mcfr_varnum[1];
/*others*/
    const char *loc_CSTRING__o0;
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_17_warmelt_first_END_MELT_PASS_RUNNER is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_17_warmelt_first_END_MELT_PASS_RUNNER_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 19; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_17_warmelt_first_END_MELT_PASS_RUNNER nbval 19*/
  meltfram__.mcfr_nbvar = 19 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("END_MELT_PASS_RUNNER", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-first.melt:3132:/ getarg");
 /*_.NOARG__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_CSTRING)
    goto lab_endgetargs;
 /*_?*/ meltfram__.loc_CSTRING__o0 = meltxargtab_[0].meltbp_cstring;

  /*getarg#2 */
  /*^getarg */
  if (meltxargdescr_[1] != MELTBPAR_LONG)
    goto lab_endgetargs;
 /*_#PASSNUMBER__L1*/ meltfnum[0] = meltxargtab_[1].meltbp_long;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {

    MELT_LOCATION ("warmelt-first.melt:3133:/ quasiblock");


    /*^getslot */
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj =
	(melt_ptr_t) (( /*!END_MELT_PASS_EXECUTION_DELAYED_QUEUE */
		       meltfrout->tabval[0])) /*=obj*/ ;
      melt_object_get_field (slot, obj, 2, "DELQU_FIRST");
  /*_.FIRSTLIST__V4*/ meltfptr[3] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-first.melt:3134:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj =
	(melt_ptr_t) (( /*!END_MELT_PASS_EXECUTION_DELAYED_QUEUE */
		       meltfrout->tabval[0])) /*=obj*/ ;
      melt_object_get_field (slot, obj, 3, "DELQU_LAST");
  /*_.LASTLIST__V5*/ meltfptr[4] = slot;
    };
    ;
 /*_.REVLASTLIST__V6*/ meltfptr[5] =
      (meltgc_new_list
       ((meltobject_ptr_t) (( /*!DISCR_LIST */ meltfrout->tabval[1]))));;
    MELT_LOCATION ("warmelt-first.melt:3136:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*^rawallocobj */
    /*rawallocobj */
    {
      melt_ptr_t newobj = 0;
      melt_raw_object_create (newobj,
			      (melt_ptr_t) (( /*!CLASS_REFERENCE */
					     meltfrout->tabval[2])), (1),
			      "CLASS_REFERENCE");
  /*_.INST__V8*/ meltfptr[7] =
	newobj;
    };
    ;
    /*^touchobj */

    melt_dbgtrace_written_object ( /*_.INST__V8*/ meltfptr[7],
				  "newly made instance");
    ;
    /*_.RESCONT__V7*/ meltfptr[6] = /*_.INST__V8*/ meltfptr[7];;
    /*^compute */
 /*_.VALPASSNAME__V9*/ meltfptr[8] =
      (meltgc_new_stringdup
       ((meltobject_ptr_t) (( /*!DISCR_STRING */ meltfrout->tabval[3])),
	( /*_?*/ meltfram__.loc_CSTRING__o0)));;
    /*^compute */
 /*_.VALPASSNUMBER__V10*/ meltfptr[9] =
      (meltgc_new_int
       ((meltobject_ptr_t)
	(( /*!DISCR_CONSTANT_INTEGER */ meltfrout->tabval[4])),
	( /*_#PASSNUMBER__L1*/ meltfnum[0])));;
    MELT_LOCATION ("warmelt-first.melt:3142:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^cond */
    /*cond */ if (
		   /*ifisa */
		   melt_is_instance_of ((melt_ptr_t)
					(( /*!END_MELT_PASS_EXECUTION_DELAYED_QUEUE */ meltfrout->tabval[0])),
					(melt_ptr_t) (( /*!CLASS_DELAYED_QUEUE */ meltfrout->tabval[5])))
      )				/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  /*^putslot */
	  /*putslot */
	  melt_assertmsg ("putslot checkobj @DELQU_FIRST",
			  melt_magic_discr ((melt_ptr_t)
					    (( /*!END_MELT_PASS_EXECUTION_DELAYED_QUEUE */ meltfrout->tabval[0]))) == MELTOBMAG_OBJECT);
	  melt_putfield_object ((( /*!END_MELT_PASS_EXECUTION_DELAYED_QUEUE */
				  meltfrout->tabval[0])), (2),
				(( /*nil */ NULL)), "DELQU_FIRST");
	  ;
	  /*^putslot */
	  /*putslot */
	  melt_assertmsg ("putslot checkobj @DELQU_LAST",
			  melt_magic_discr ((melt_ptr_t)
					    (( /*!END_MELT_PASS_EXECUTION_DELAYED_QUEUE */ meltfrout->tabval[0]))) == MELTOBMAG_OBJECT);
	  melt_putfield_object ((( /*!END_MELT_PASS_EXECUTION_DELAYED_QUEUE */
				  meltfrout->tabval[0])), (3),
				(( /*nil */ NULL)), "DELQU_LAST");
	  ;
	  /*^touch */
	  meltgc_touch (( /*!END_MELT_PASS_EXECUTION_DELAYED_QUEUE */
			 meltfrout->tabval[0]));
	  ;
	  /*^touchobj */

	  melt_dbgtrace_written_object (( /*!END_MELT_PASS_EXECUTION_DELAYED_QUEUE */ meltfrout->tabval[0]), "put-fields");
	  ;
	  /*epilog */
	}
	;
      }				/*noelse */
    ;
    MELT_LOCATION ("warmelt-first.melt:3149:/ quasiblock");


    /*^newclosure */
		 /*newclosure *//*_.LAMBDA___V12*/ meltfptr[11] =
      (melt_ptr_t)
      meltgc_new_closure ((meltobject_ptr_t)
			  (((melt_ptr_t) (MELT_PREDEF (DISCR_CLOSURE)))),
			  (meltroutine_ptr_t) (( /*!konst_7 */ meltfrout->
						tabval[7])), (3));
    ;
    /*^putclosedv */
    /*putclosv */
    melt_assertmsg ("putclosv checkclo",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.LAMBDA___V12*/ meltfptr[11])) ==
		    MELTOBMAG_CLOSURE);
    melt_assertmsg ("putclosv checkoff", 0 >= 0
		    && 0 <
		    melt_closure_size ((melt_ptr_t)
				       ( /*_.LAMBDA___V12*/ meltfptr[11])));
    ((meltclosure_ptr_t) /*_.LAMBDA___V12*/ meltfptr[11])->tabval[0] =
      (melt_ptr_t) ( /*_.RESCONT__V7*/ meltfptr[6]);
    ;
    /*^putclosedv */
    /*putclosv */
    melt_assertmsg ("putclosv checkclo",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.LAMBDA___V12*/ meltfptr[11])) ==
		    MELTOBMAG_CLOSURE);
    melt_assertmsg ("putclosv checkoff", 1 >= 0
		    && 1 <
		    melt_closure_size ((melt_ptr_t)
				       ( /*_.LAMBDA___V12*/ meltfptr[11])));
    ((meltclosure_ptr_t) /*_.LAMBDA___V12*/ meltfptr[11])->tabval[1] =
      (melt_ptr_t) ( /*_.VALPASSNAME__V9*/ meltfptr[8]);
    ;
    /*^putclosedv */
    /*putclosv */
    melt_assertmsg ("putclosv checkclo",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.LAMBDA___V12*/ meltfptr[11])) ==
		    MELTOBMAG_CLOSURE);
    melt_assertmsg ("putclosv checkoff", 2 >= 0
		    && 2 <
		    melt_closure_size ((melt_ptr_t)
				       ( /*_.LAMBDA___V12*/ meltfptr[11])));
    ((meltclosure_ptr_t) /*_.LAMBDA___V12*/ meltfptr[11])->tabval[2] =
      (melt_ptr_t) ( /*_.VALPASSNUMBER__V10*/ meltfptr[9]);
    ;
    /*_.LAMBDA___V11*/ meltfptr[10] = /*_.LAMBDA___V12*/ meltfptr[11];;
    MELT_LOCATION ("warmelt-first.melt:3147:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & /*_.LAMBDA___V11*/ meltfptr[10];
      /*_.LIST_EVERY__V13*/ meltfptr[12] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!LIST_EVERY */ meltfrout->tabval[6])),
		    (melt_ptr_t) ( /*_.FIRSTLIST__V4*/ meltfptr[3]),
		    (MELTBPARSTR_PTR ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-first.melt:3158:/ quasiblock");


    /*^newclosure */
		 /*newclosure *//*_.LAMBDA___V15*/ meltfptr[14] =
      (melt_ptr_t)
      meltgc_new_closure ((meltobject_ptr_t)
			  (((melt_ptr_t) (MELT_PREDEF (DISCR_CLOSURE)))),
			  (meltroutine_ptr_t) (( /*!konst_8 */ meltfrout->
						tabval[8])), (1));
    ;
    /*^putclosedv */
    /*putclosv */
    melt_assertmsg ("putclosv checkclo",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.LAMBDA___V15*/ meltfptr[14])) ==
		    MELTOBMAG_CLOSURE);
    melt_assertmsg ("putclosv checkoff", 0 >= 0
		    && 0 <
		    melt_closure_size ((melt_ptr_t)
				       ( /*_.LAMBDA___V15*/ meltfptr[14])));
    ((meltclosure_ptr_t) /*_.LAMBDA___V15*/ meltfptr[14])->tabval[0] =
      (melt_ptr_t) ( /*_.REVLASTLIST__V6*/ meltfptr[5]);
    ;
    /*_.LAMBDA___V14*/ meltfptr[13] = /*_.LAMBDA___V15*/ meltfptr[14];;
    MELT_LOCATION ("warmelt-first.melt:3156:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & /*_.LAMBDA___V14*/ meltfptr[13];
      /*_.LIST_EVERY__V16*/ meltfptr[15] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!LIST_EVERY */ meltfrout->tabval[6])),
		    (melt_ptr_t) ( /*_.LASTLIST__V5*/ meltfptr[4]),
		    (MELTBPARSTR_PTR ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-first.melt:3163:/ quasiblock");


    /*^newclosure */
		 /*newclosure *//*_.LAMBDA___V18*/ meltfptr[17] =
      (melt_ptr_t)
      meltgc_new_closure ((meltobject_ptr_t)
			  (((melt_ptr_t) (MELT_PREDEF (DISCR_CLOSURE)))),
			  (meltroutine_ptr_t) (( /*!konst_9 */ meltfrout->
						tabval[9])), (3));
    ;
    /*^putclosedv */
    /*putclosv */
    melt_assertmsg ("putclosv checkclo",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.LAMBDA___V18*/ meltfptr[17])) ==
		    MELTOBMAG_CLOSURE);
    melt_assertmsg ("putclosv checkoff", 0 >= 0
		    && 0 <
		    melt_closure_size ((melt_ptr_t)
				       ( /*_.LAMBDA___V18*/ meltfptr[17])));
    ((meltclosure_ptr_t) /*_.LAMBDA___V18*/ meltfptr[17])->tabval[0] =
      (melt_ptr_t) ( /*_.RESCONT__V7*/ meltfptr[6]);
    ;
    /*^putclosedv */
    /*putclosv */
    melt_assertmsg ("putclosv checkclo",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.LAMBDA___V18*/ meltfptr[17])) ==
		    MELTOBMAG_CLOSURE);
    melt_assertmsg ("putclosv checkoff", 1 >= 0
		    && 1 <
		    melt_closure_size ((melt_ptr_t)
				       ( /*_.LAMBDA___V18*/ meltfptr[17])));
    ((meltclosure_ptr_t) /*_.LAMBDA___V18*/ meltfptr[17])->tabval[1] =
      (melt_ptr_t) ( /*_.VALPASSNAME__V9*/ meltfptr[8]);
    ;
    /*^putclosedv */
    /*putclosv */
    melt_assertmsg ("putclosv checkclo",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.LAMBDA___V18*/ meltfptr[17])) ==
		    MELTOBMAG_CLOSURE);
    melt_assertmsg ("putclosv checkoff", 2 >= 0
		    && 2 <
		    melt_closure_size ((melt_ptr_t)
				       ( /*_.LAMBDA___V18*/ meltfptr[17])));
    ((meltclosure_ptr_t) /*_.LAMBDA___V18*/ meltfptr[17])->tabval[2] =
      (melt_ptr_t) ( /*_.VALPASSNUMBER__V10*/ meltfptr[9]);
    ;
    /*_.LAMBDA___V17*/ meltfptr[16] = /*_.LAMBDA___V18*/ meltfptr[17];;
    MELT_LOCATION ("warmelt-first.melt:3161:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & /*_.LAMBDA___V17*/ meltfptr[16];
      /*_.LIST_EVERY__V19*/ meltfptr[18] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!LIST_EVERY */ meltfrout->tabval[6])),
		    (melt_ptr_t) ( /*_.REVLASTLIST__V6*/ meltfptr[5]),
		    (MELTBPARSTR_PTR ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;
    /*_.LET___V3*/ meltfptr[2] = /*_.LIST_EVERY__V19*/ meltfptr[18];;

    MELT_LOCATION ("warmelt-first.melt:3133:/ clear");
	   /*clear *//*_.FIRSTLIST__V4*/ meltfptr[3] = 0;
    /*^clear */
	   /*clear *//*_.LASTLIST__V5*/ meltfptr[4] = 0;
    /*^clear */
	   /*clear *//*_.REVLASTLIST__V6*/ meltfptr[5] = 0;
    /*^clear */
	   /*clear *//*_.RESCONT__V7*/ meltfptr[6] = 0;
    /*^clear */
	   /*clear *//*_.VALPASSNAME__V9*/ meltfptr[8] = 0;
    /*^clear */
	   /*clear *//*_.VALPASSNUMBER__V10*/ meltfptr[9] = 0;
    /*^clear */
	   /*clear *//*_.LAMBDA___V11*/ meltfptr[10] = 0;
    /*^clear */
	   /*clear *//*_.LIST_EVERY__V13*/ meltfptr[12] = 0;
    /*^clear */
	   /*clear *//*_.LAMBDA___V14*/ meltfptr[13] = 0;
    /*^clear */
	   /*clear *//*_.LIST_EVERY__V16*/ meltfptr[15] = 0;
    /*^clear */
	   /*clear *//*_.LAMBDA___V17*/ meltfptr[16] = 0;
    /*^clear */
	   /*clear *//*_.LIST_EVERY__V19*/ meltfptr[18] = 0;
    MELT_LOCATION ("warmelt-first.melt:3132:/ quasiblock");


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.LET___V3*/ meltfptr[2];;

    {
      MELT_LOCATION ("warmelt-first.melt:3132:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.LET___V3*/ meltfptr[2] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("END_MELT_PASS_RUNNER", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_17_warmelt_first_END_MELT_PASS_RUNNER_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_17_warmelt_first_END_MELT_PASS_RUNNER */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_18_warmelt_first_LAMBDA___1__ (meltclosure_ptr_t meltclosp_,
					melt_ptr_t meltfirstargp_,
					const melt_argdescr_cell_t
					meltxargdescr_[],
					union meltparam_un * meltxargtab_,
					const melt_argdescr_cell_t
					meltxresdescr_[],
					union meltparam_un * meltxrestab_)
{
  long current_blocklevel_signals_meltrout_18_warmelt_first_LAMBDA___1___melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_18_warmelt_first_LAMBDA___1___st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 4
    melt_ptr_t mcfr_varptr[4];
/*no varnum*/
#define MELTFRAM_NBVARNUM /*none*/0
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_18_warmelt_first_LAMBDA___1__ is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_18_warmelt_first_LAMBDA___1___st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 4; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_18_warmelt_first_LAMBDA___1__ nbval 4*/
  meltfram__.mcfr_nbvar = 4 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("LAMBDA_", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-first.melt:3149:/ getarg");
 /*_.FIRSTPROC__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {

    MELT_LOCATION ("warmelt-first.melt:3150:/ quasiblock");


    /*^getslot */
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) (( /*~RESCONT */ meltfclos->tabval[0])) /*=obj*/ ;
      melt_object_get_field (slot, obj, 0, "REFERENCED_VALUE");
  /*_.PREVRES__V3*/ meltfptr[2] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-first.melt:3151:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[2];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & ( /*~VALPASSNAME */ meltfclos->tabval[1]);
      /*^apply.arg */
      argtab[1].meltbp_aptr =
	(melt_ptr_t *) & ( /*~VALPASSNUMBER */ meltfclos->tabval[2]);
      /*_.NEXTRES__V4*/ meltfptr[3] =
	melt_apply ((meltclosure_ptr_t) ( /*_.FIRSTPROC__V2*/ meltfptr[1]),
		    (melt_ptr_t) ( /*_.PREVRES__V3*/ meltfptr[2]),
		    (MELTBPARSTR_PTR MELTBPARSTR_PTR ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-first.melt:3153:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @REFERENCED_VALUE",
		    melt_magic_discr ((melt_ptr_t)
				      (( /*~RESCONT */ meltfclos->
					tabval[0]))) == MELTOBMAG_OBJECT);
    melt_putfield_object ((( /*~RESCONT */ meltfclos->tabval[0])), (0),
			  ( /*_.NEXTRES__V4*/ meltfptr[3]),
			  "REFERENCED_VALUE");
    ;
    /*^touch */
    meltgc_touch (( /*~RESCONT */ meltfclos->tabval[0]));
    ;
    /*^touchobj */

    melt_dbgtrace_written_object (( /*~RESCONT */ meltfclos->tabval[0]),
				  "put-fields");
    ;


    MELT_LOCATION ("warmelt-first.melt:3150:/ clear");
	   /*clear *//*_.PREVRES__V3*/ meltfptr[2] = 0;
    /*^clear */
	   /*clear *//*_.NEXTRES__V4*/ meltfptr[3] = 0;
    /*epilog */
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("LAMBDA_", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_18_warmelt_first_LAMBDA___1___melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*noretval */ NULL);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_18_warmelt_first_LAMBDA___1__ */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_19_warmelt_first_LAMBDA___2__ (meltclosure_ptr_t meltclosp_,
					melt_ptr_t meltfirstargp_,
					const melt_argdescr_cell_t
					meltxargdescr_[],
					union meltparam_un * meltxargtab_,
					const melt_argdescr_cell_t
					meltxresdescr_[],
					union meltparam_un * meltxrestab_)
{
  long current_blocklevel_signals_meltrout_19_warmelt_first_LAMBDA___2___melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_19_warmelt_first_LAMBDA___2___st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 2
    melt_ptr_t mcfr_varptr[2];
#define MELTFRAM_NBVARNUM 1
    long mcfr_varnum[1];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_19_warmelt_first_LAMBDA___2__ is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_19_warmelt_first_LAMBDA___2___st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 2; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_19_warmelt_first_LAMBDA___2__ nbval 2*/
  meltfram__.mcfr_nbvar = 2 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("LAMBDA_", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-first.melt:3158:/ getarg");
 /*_.LASTPROC__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {

    MELT_LOCATION ("warmelt-first.melt:3159:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#IS_CLOSURE__L1*/ meltfnum[0] =
      (melt_magic_discr ((melt_ptr_t) ( /*_.LASTPROC__V2*/ meltfptr[1])) ==
       MELTOBMAG_CLOSURE);;
    MELT_LOCATION ("warmelt-first.melt:3159:/ cond");
    /*cond */ if ( /*_#IS_CLOSURE__L1*/ meltfnum[0])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{


	  {
	    /*^locexp */
	    meltgc_prepend_list ((melt_ptr_t)
				 (( /*~REVLASTLIST */ meltfclos->tabval[0])),
				 (melt_ptr_t) ( /*_.LASTPROC__V2*/
					       meltfptr[1]));
	  }
	  ;
	  /*epilog */
	}
	;
      }				/*noelse */
    ;
    /*epilog */

    MELT_LOCATION ("warmelt-first.melt:3158:/ clear");
	   /*clear *//*_#IS_CLOSURE__L1*/ meltfnum[0] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("LAMBDA_", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_19_warmelt_first_LAMBDA___2___melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*noretval */ NULL);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_19_warmelt_first_LAMBDA___2__ */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_20_warmelt_first_LAMBDA___3__ (meltclosure_ptr_t meltclosp_,
					melt_ptr_t meltfirstargp_,
					const melt_argdescr_cell_t
					meltxargdescr_[],
					union meltparam_un * meltxargtab_,
					const melt_argdescr_cell_t
					meltxresdescr_[],
					union meltparam_un * meltxrestab_)
{
  long current_blocklevel_signals_meltrout_20_warmelt_first_LAMBDA___3___melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_20_warmelt_first_LAMBDA___3___st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 4
    melt_ptr_t mcfr_varptr[4];
/*no varnum*/
#define MELTFRAM_NBVARNUM /*none*/0
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_20_warmelt_first_LAMBDA___3__ is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_20_warmelt_first_LAMBDA___3___st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 4; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_20_warmelt_first_LAMBDA___3__ nbval 4*/
  meltfram__.mcfr_nbvar = 4 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("LAMBDA_", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-first.melt:3163:/ getarg");
 /*_.LASTPROC__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {

    MELT_LOCATION ("warmelt-first.melt:3164:/ quasiblock");


    /*^getslot */
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) (( /*~RESCONT */ meltfclos->tabval[0])) /*=obj*/ ;
      melt_object_get_field (slot, obj, 0, "REFERENCED_VALUE");
  /*_.PREVRES__V3*/ meltfptr[2] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-first.melt:3165:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[2];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & ( /*~VALPASSNAME */ meltfclos->tabval[1]);
      /*^apply.arg */
      argtab[1].meltbp_aptr =
	(melt_ptr_t *) & ( /*~VALPASSNUMBER */ meltfclos->tabval[2]);
      /*_.NEXTRES__V4*/ meltfptr[3] =
	melt_apply ((meltclosure_ptr_t) ( /*_.LASTPROC__V2*/ meltfptr[1]),
		    (melt_ptr_t) ( /*_.PREVRES__V3*/ meltfptr[2]),
		    (MELTBPARSTR_PTR MELTBPARSTR_PTR ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-first.melt:3167:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @REFERENCED_VALUE",
		    melt_magic_discr ((melt_ptr_t)
				      (( /*~RESCONT */ meltfclos->
					tabval[0]))) == MELTOBMAG_OBJECT);
    melt_putfield_object ((( /*~RESCONT */ meltfclos->tabval[0])), (0),
			  ( /*_.NEXTRES__V4*/ meltfptr[3]),
			  "REFERENCED_VALUE");
    ;
    /*^touch */
    meltgc_touch (( /*~RESCONT */ meltfclos->tabval[0]));
    ;
    /*^touchobj */

    melt_dbgtrace_written_object (( /*~RESCONT */ meltfclos->tabval[0]),
				  "put-fields");
    ;


    MELT_LOCATION ("warmelt-first.melt:3164:/ clear");
	   /*clear *//*_.PREVRES__V3*/ meltfptr[2] = 0;
    /*^clear */
	   /*clear *//*_.NEXTRES__V4*/ meltfptr[3] = 0;
    /*epilog */
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("LAMBDA_", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_20_warmelt_first_LAMBDA___3___melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*noretval */ NULL);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_20_warmelt_first_LAMBDA___3__ */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_21_warmelt_first_AT_END_MELT_PASS_FIRST (meltclosure_ptr_t
						  meltclosp_,
						  melt_ptr_t meltfirstargp_,
						  const melt_argdescr_cell_t
						  meltxargdescr_[],
						  union meltparam_un *
						  meltxargtab_,
						  const melt_argdescr_cell_t
						  meltxresdescr_[],
						  union meltparam_un *
						  meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_21_warmelt_first_AT_END_MELT_PASS_FIRST_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_21_warmelt_first_AT_END_MELT_PASS_FIRST_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 6
    melt_ptr_t mcfr_varptr[6];
#define MELTFRAM_NBVARNUM 2
    long mcfr_varnum[2];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_21_warmelt_first_AT_END_MELT_PASS_FIRST is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_21_warmelt_first_AT_END_MELT_PASS_FIRST_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 6; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_21_warmelt_first_AT_END_MELT_PASS_FIRST nbval 6*/
  meltfram__.mcfr_nbvar = 6 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("AT_END_MELT_PASS_FIRST", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-first.melt:3172:/ getarg");
 /*_.FUN__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {

    MELT_LOCATION ("warmelt-first.melt:3178:/ quasiblock");


    /*^getslot */
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj =
	(melt_ptr_t) (( /*!END_MELT_PASS_EXECUTION_DELAYED_QUEUE */
		       meltfrout->tabval[0])) /*=obj*/ ;
      melt_object_get_field (slot, obj, 2, "DELQU_FIRST");
  /*_.FIRSTLIST__V3*/ meltfptr[2] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-first.melt:3180:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#NULL__L1*/ meltfnum[0] =
      (( /*_.FIRSTLIST__V3*/ meltfptr[2]) == NULL);;
    MELT_LOCATION ("warmelt-first.melt:3180:/ cond");
    /*cond */ if ( /*_#NULL__L1*/ meltfnum[0])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

   /*_.MAKE_LIST__V4*/ meltfptr[3] =
	    (meltgc_new_list
	     ((meltobject_ptr_t)
	      (( /*!DISCR_LIST */ meltfrout->tabval[1]))));;
	  MELT_LOCATION ("warmelt-first.melt:3182:/ compute");
	  /*_.FIRSTLIST__V3*/ meltfptr[2] = /*_.SETQ___V5*/ meltfptr[4] =
	    /*_.MAKE_LIST__V4*/ meltfptr[3];;
	  MELT_LOCATION ("warmelt-first.melt:3183:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
   /*_.MAKE_LIST__V6*/ meltfptr[5] =
	    (meltgc_new_list
	     ((meltobject_ptr_t)
	      (( /*!DISCR_LIST */ meltfrout->tabval[1]))));;
	  MELT_LOCATION ("warmelt-first.melt:3183:/ cond");
	  /*cond */ if (
			 /*ifisa */
			 melt_is_instance_of ((melt_ptr_t)
					      (( /*!END_MELT_PASS_EXECUTION_DELAYED_QUEUE */ meltfrout->tabval[0])),
					      (melt_ptr_t) (( /*!CLASS_DELAYED_QUEUE */ meltfrout->tabval[2])))
	    )			/*then */
	    {
	      /*^cond.then */
	      /*^block */
	      /*anyblock */
	      {

		/*^putslot */
		/*putslot */
		melt_assertmsg ("putslot checkobj @DELQU_FIRST",
				melt_magic_discr ((melt_ptr_t)
						  (( /*!END_MELT_PASS_EXECUTION_DELAYED_QUEUE */ meltfrout->tabval[0]))) == MELTOBMAG_OBJECT);
		melt_putfield_object ((( /*!END_MELT_PASS_EXECUTION_DELAYED_QUEUE */ meltfrout->tabval[0])), (2), ( /*_.FIRSTLIST__V3*/ meltfptr[2]), "DELQU_FIRST");
		;
		/*^putslot */
		/*putslot */
		melt_assertmsg ("putslot checkobj @DELQU_LAST",
				melt_magic_discr ((melt_ptr_t)
						  (( /*!END_MELT_PASS_EXECUTION_DELAYED_QUEUE */ meltfrout->tabval[0]))) == MELTOBMAG_OBJECT);
		melt_putfield_object ((( /*!END_MELT_PASS_EXECUTION_DELAYED_QUEUE */ meltfrout->tabval[0])), (3), ( /*_.MAKE_LIST__V6*/ meltfptr[5]), "DELQU_LAST");
		;
		/*^touch */
		meltgc_touch (( /*!END_MELT_PASS_EXECUTION_DELAYED_QUEUE */
			       meltfrout->tabval[0]));
		;
		/*^touchobj */

		melt_dbgtrace_written_object (( /*!END_MELT_PASS_EXECUTION_DELAYED_QUEUE */ meltfrout->tabval[0]), "put-fields");
		;
		/*epilog */
	      }
	      ;
	    }			/*noelse */
	  ;
	  MELT_LOCATION ("warmelt-first.melt:3187:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^cond */
	  /*cond */ if (
			 /*ifisa */
			 melt_is_instance_of ((melt_ptr_t)
					      (( /*!INITIAL_SYSTEM_DATA */
						meltfrout->tabval[3])),
					      (melt_ptr_t) (( /*!CLASS_SYSTEM_DATA */ meltfrout->tabval[5])))
	    )			/*then */
	    {
	      /*^cond.then */
	      /*^block */
	      /*anyblock */
	      {

		/*^putslot */
		/*putslot */
		melt_assertmsg
		  ("putslot checkobj @SYSDATA_MELTPASS_AFTER_HOOK",
		   melt_magic_discr ((melt_ptr_t)
				     (( /*!INITIAL_SYSTEM_DATA */ meltfrout->
				       tabval[3]))) == MELTOBMAG_OBJECT);
		melt_putfield_object ((( /*!INITIAL_SYSTEM_DATA */ meltfrout->
					tabval[3])), (26),
				      (( /*!END_MELT_PASS_RUNNER */
					meltfrout->tabval[4])),
				      "SYSDATA_MELTPASS_AFTER_HOOK");
		;
		/*^touch */
		meltgc_touch (( /*!INITIAL_SYSTEM_DATA */ meltfrout->
			       tabval[3]));
		;
		/*^touchobj */

		melt_dbgtrace_written_object (( /*!INITIAL_SYSTEM_DATA */
					       meltfrout->tabval[3]),
					      "put-fields");
		;
		/*epilog */
	      }
	      ;
	    }			/*noelse */
	  ;
	  MELT_LOCATION ("warmelt-first.melt:3181:/ quasiblock");


	  /*epilog */

	  MELT_LOCATION ("warmelt-first.melt:3180:/ clear");
	     /*clear *//*_.MAKE_LIST__V4*/ meltfptr[3] = 0;
	  /*^clear */
	     /*clear *//*_.SETQ___V5*/ meltfptr[4] = 0;
	  /*^clear */
	     /*clear *//*_.MAKE_LIST__V6*/ meltfptr[5] = 0;
	}
	;
      }				/*noelse */
    ;
    MELT_LOCATION ("warmelt-first.melt:3190:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#IS_CLOSURE__L2*/ meltfnum[1] =
      (melt_magic_discr ((melt_ptr_t) ( /*_.FUN__V2*/ meltfptr[1])) ==
       MELTOBMAG_CLOSURE);;
    MELT_LOCATION ("warmelt-first.melt:3190:/ cond");
    /*cond */ if ( /*_#IS_CLOSURE__L2*/ meltfnum[1])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{


	  {
	    /*^locexp */
	    meltgc_append_list ((melt_ptr_t)
				( /*_.FIRSTLIST__V3*/ meltfptr[2]),
				(melt_ptr_t) ( /*_.FUN__V2*/ meltfptr[1]));
	  }
	  ;
	  /*epilog */
	}
	;
      }				/*noelse */
    ;

    MELT_LOCATION ("warmelt-first.melt:3178:/ clear");
	   /*clear *//*_.FIRSTLIST__V3*/ meltfptr[2] = 0;
    /*^clear */
	   /*clear *//*_#NULL__L1*/ meltfnum[0] = 0;
    /*^clear */
	   /*clear *//*_#IS_CLOSURE__L2*/ meltfnum[1] = 0;
    /*epilog */
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("AT_END_MELT_PASS_FIRST", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_21_warmelt_first_AT_END_MELT_PASS_FIRST_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*noretval */ NULL);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_21_warmelt_first_AT_END_MELT_PASS_FIRST */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_22_warmelt_first_AT_END_MELT_PASS_LAST (meltclosure_ptr_t meltclosp_,
						 melt_ptr_t meltfirstargp_,
						 const melt_argdescr_cell_t
						 meltxargdescr_[],
						 union meltparam_un *
						 meltxargtab_,
						 const melt_argdescr_cell_t
						 meltxresdescr_[],
						 union meltparam_un *
						 meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_22_warmelt_first_AT_END_MELT_PASS_LAST_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_22_warmelt_first_AT_END_MELT_PASS_LAST_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 6
    melt_ptr_t mcfr_varptr[6];
#define MELTFRAM_NBVARNUM 2
    long mcfr_varnum[2];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_22_warmelt_first_AT_END_MELT_PASS_LAST is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_22_warmelt_first_AT_END_MELT_PASS_LAST_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 6; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_22_warmelt_first_AT_END_MELT_PASS_LAST nbval 6*/
  meltfram__.mcfr_nbvar = 6 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("AT_END_MELT_PASS_LAST", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-first.melt:3193:/ getarg");
 /*_.FUN__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {

    MELT_LOCATION ("warmelt-first.melt:3197:/ quasiblock");


    /*^getslot */
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj =
	(melt_ptr_t) (( /*!FINAL_DELAYED_QUEUE */ meltfrout->
		       tabval[0])) /*=obj*/ ;
      melt_object_get_field (slot, obj, 3, "DELQU_LAST");
  /*_.LASTLIST__V3*/ meltfptr[2] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-first.melt:3199:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#NULL__L1*/ meltfnum[0] =
      (( /*_.LASTLIST__V3*/ meltfptr[2]) == NULL);;
    MELT_LOCATION ("warmelt-first.melt:3199:/ cond");
    /*cond */ if ( /*_#NULL__L1*/ meltfnum[0])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

   /*_.MAKE_LIST__V4*/ meltfptr[3] =
	    (meltgc_new_list
	     ((meltobject_ptr_t)
	      (( /*!DISCR_LIST */ meltfrout->tabval[1]))));;
	  MELT_LOCATION ("warmelt-first.melt:3201:/ compute");
	  /*_.LASTLIST__V3*/ meltfptr[2] = /*_.SETQ___V5*/ meltfptr[4] =
	    /*_.MAKE_LIST__V4*/ meltfptr[3];;
	  MELT_LOCATION ("warmelt-first.melt:3202:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
   /*_.MAKE_LIST__V6*/ meltfptr[5] =
	    (meltgc_new_list
	     ((meltobject_ptr_t)
	      (( /*!DISCR_LIST */ meltfrout->tabval[1]))));;
	  MELT_LOCATION ("warmelt-first.melt:3202:/ cond");
	  /*cond */ if (
			 /*ifisa */
			 melt_is_instance_of ((melt_ptr_t)
					      (( /*!END_MELT_PASS_EXECUTION_DELAYED_QUEUE */ meltfrout->tabval[2])),
					      (melt_ptr_t) (( /*!CLASS_DELAYED_QUEUE */ meltfrout->tabval[3])))
	    )			/*then */
	    {
	      /*^cond.then */
	      /*^block */
	      /*anyblock */
	      {

		/*^putslot */
		/*putslot */
		melt_assertmsg ("putslot checkobj @DELQU_FIRST",
				melt_magic_discr ((melt_ptr_t)
						  (( /*!END_MELT_PASS_EXECUTION_DELAYED_QUEUE */ meltfrout->tabval[2]))) == MELTOBMAG_OBJECT);
		melt_putfield_object ((( /*!END_MELT_PASS_EXECUTION_DELAYED_QUEUE */ meltfrout->tabval[2])), (2), ( /*_.MAKE_LIST__V6*/ meltfptr[5]), "DELQU_FIRST");
		;
		/*^putslot */
		/*putslot */
		melt_assertmsg ("putslot checkobj @DELQU_LAST",
				melt_magic_discr ((melt_ptr_t)
						  (( /*!END_MELT_PASS_EXECUTION_DELAYED_QUEUE */ meltfrout->tabval[2]))) == MELTOBMAG_OBJECT);
		melt_putfield_object ((( /*!END_MELT_PASS_EXECUTION_DELAYED_QUEUE */ meltfrout->tabval[2])), (3), ( /*_.LASTLIST__V3*/ meltfptr[2]), "DELQU_LAST");
		;
		/*^touch */
		meltgc_touch (( /*!END_MELT_PASS_EXECUTION_DELAYED_QUEUE */
			       meltfrout->tabval[2]));
		;
		/*^touchobj */

		melt_dbgtrace_written_object (( /*!END_MELT_PASS_EXECUTION_DELAYED_QUEUE */ meltfrout->tabval[2]), "put-fields");
		;
		/*epilog */
	      }
	      ;
	    }			/*noelse */
	  ;
	  MELT_LOCATION ("warmelt-first.melt:3206:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^cond */
	  /*cond */ if (
			 /*ifisa */
			 melt_is_instance_of ((melt_ptr_t)
					      (( /*!INITIAL_SYSTEM_DATA */
						meltfrout->tabval[4])),
					      (melt_ptr_t) (( /*!CLASS_SYSTEM_DATA */ meltfrout->tabval[6])))
	    )			/*then */
	    {
	      /*^cond.then */
	      /*^block */
	      /*anyblock */
	      {

		/*^putslot */
		/*putslot */
		melt_assertmsg
		  ("putslot checkobj @SYSDATA_MELTPASS_AFTER_HOOK",
		   melt_magic_discr ((melt_ptr_t)
				     (( /*!INITIAL_SYSTEM_DATA */ meltfrout->
				       tabval[4]))) == MELTOBMAG_OBJECT);
		melt_putfield_object ((( /*!INITIAL_SYSTEM_DATA */ meltfrout->
					tabval[4])), (26),
				      (( /*!END_MELT_PASS_RUNNER */
					meltfrout->tabval[5])),
				      "SYSDATA_MELTPASS_AFTER_HOOK");
		;
		/*^touch */
		meltgc_touch (( /*!INITIAL_SYSTEM_DATA */ meltfrout->
			       tabval[4]));
		;
		/*^touchobj */

		melt_dbgtrace_written_object (( /*!INITIAL_SYSTEM_DATA */
					       meltfrout->tabval[4]),
					      "put-fields");
		;
		/*epilog */
	      }
	      ;
	    }			/*noelse */
	  ;
	  MELT_LOCATION ("warmelt-first.melt:3200:/ quasiblock");


	  /*epilog */

	  MELT_LOCATION ("warmelt-first.melt:3199:/ clear");
	     /*clear *//*_.MAKE_LIST__V4*/ meltfptr[3] = 0;
	  /*^clear */
	     /*clear *//*_.SETQ___V5*/ meltfptr[4] = 0;
	  /*^clear */
	     /*clear *//*_.MAKE_LIST__V6*/ meltfptr[5] = 0;
	}
	;
      }				/*noelse */
    ;
    MELT_LOCATION ("warmelt-first.melt:3209:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#IS_CLOSURE__L2*/ meltfnum[1] =
      (melt_magic_discr ((melt_ptr_t) ( /*_.FUN__V2*/ meltfptr[1])) ==
       MELTOBMAG_CLOSURE);;
    MELT_LOCATION ("warmelt-first.melt:3209:/ cond");
    /*cond */ if ( /*_#IS_CLOSURE__L2*/ meltfnum[1])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{


	  {
	    /*^locexp */
	    meltgc_append_list ((melt_ptr_t)
				( /*_.LASTLIST__V3*/ meltfptr[2]),
				(melt_ptr_t) ( /*_.FUN__V2*/ meltfptr[1]));
	  }
	  ;
	  /*epilog */
	}
	;
      }				/*noelse */
    ;

    MELT_LOCATION ("warmelt-first.melt:3197:/ clear");
	   /*clear *//*_.LASTLIST__V3*/ meltfptr[2] = 0;
    /*^clear */
	   /*clear *//*_#NULL__L1*/ meltfnum[0] = 0;
    /*^clear */
	   /*clear *//*_#IS_CLOSURE__L2*/ meltfnum[1] = 0;
    /*epilog */
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("AT_END_MELT_PASS_LAST", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_22_warmelt_first_AT_END_MELT_PASS_LAST_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*noretval */ NULL);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_22_warmelt_first_AT_END_MELT_PASS_LAST */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_23_warmelt_first_INIT_UNITSTARTER (meltclosure_ptr_t meltclosp_,
					    melt_ptr_t meltfirstargp_,
					    const melt_argdescr_cell_t
					    meltxargdescr_[],
					    union meltparam_un * meltxargtab_,
					    const melt_argdescr_cell_t
					    meltxresdescr_[],
					    union meltparam_un * meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_23_warmelt_first_INIT_UNITSTARTER_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_23_warmelt_first_INIT_UNITSTARTER_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 16
    melt_ptr_t mcfr_varptr[16];
/*no varnum*/
#define MELTFRAM_NBVARNUM /*none*/0
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_23_warmelt_first_INIT_UNITSTARTER is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_23_warmelt_first_INIT_UNITSTARTER_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 16; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_23_warmelt_first_INIT_UNITSTARTER nbval 16*/
  meltfram__.mcfr_nbvar = 16 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("INIT_UNITSTARTER", meltcallcount);
/*getargs*/
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
  MELT_LOCATION ("warmelt-first.melt:3220:/ block");
/*anyblock*/
  {

    MELT_LOCATION ("warmelt-first.melt:3221:/ quasiblock");


    /*^getslot */
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj =
	(melt_ptr_t) (( /*!START_UNIT_DELAYED_QUEUE */ meltfrout->
		       tabval[0])) /*=obj*/ ;
      melt_object_get_field (slot, obj, 2, "DELQU_FIRST");
  /*_.FIRSTLIST__V3*/ meltfptr[2] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-first.melt:3222:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj =
	(melt_ptr_t) (( /*!START_UNIT_DELAYED_QUEUE */ meltfrout->
		       tabval[0])) /*=obj*/ ;
      melt_object_get_field (slot, obj, 3, "DELQU_LAST");
  /*_.LASTLIST__V4*/ meltfptr[3] = slot;
    };
    ;
 /*_.REVLASTLIST__V5*/ meltfptr[4] =
      (meltgc_new_list
       ((meltobject_ptr_t) (( /*!DISCR_LIST */ meltfrout->tabval[1]))));;
    MELT_LOCATION ("warmelt-first.melt:3224:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*^rawallocobj */
    /*rawallocobj */
    {
      melt_ptr_t newobj = 0;
      melt_raw_object_create (newobj,
			      (melt_ptr_t) (( /*!CLASS_REFERENCE */
					     meltfrout->tabval[2])), (1),
			      "CLASS_REFERENCE");
  /*_.INST__V7*/ meltfptr[6] =
	newobj;
    };
    ;
    /*^touchobj */

    melt_dbgtrace_written_object ( /*_.INST__V7*/ meltfptr[6],
				  "newly made instance");
    ;
    /*_.RESCONT__V6*/ meltfptr[5] = /*_.INST__V7*/ meltfptr[6];;
    MELT_LOCATION ("warmelt-first.melt:3229:/ quasiblock");


    /*^newclosure */
		 /*newclosure *//*_.LAMBDA___V9*/ meltfptr[8] =
      (melt_ptr_t)
      meltgc_new_closure ((meltobject_ptr_t)
			  (((melt_ptr_t) (MELT_PREDEF (DISCR_CLOSURE)))),
			  (meltroutine_ptr_t) (( /*!konst_5 */ meltfrout->
						tabval[5])), (1));
    ;
    /*^putclosedv */
    /*putclosv */
    melt_assertmsg ("putclosv checkclo",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.LAMBDA___V9*/ meltfptr[8])) ==
		    MELTOBMAG_CLOSURE);
    melt_assertmsg ("putclosv checkoff", 0 >= 0
		    && 0 <
		    melt_closure_size ((melt_ptr_t)
				       ( /*_.LAMBDA___V9*/ meltfptr[8])));
    ((meltclosure_ptr_t) /*_.LAMBDA___V9*/ meltfptr[8])->tabval[0] =
      (melt_ptr_t) ( /*_.RESCONT__V6*/ meltfptr[5]);
    ;
    /*_.LAMBDA___V8*/ meltfptr[7] = /*_.LAMBDA___V9*/ meltfptr[8];;
    MELT_LOCATION ("warmelt-first.melt:3227:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.LAMBDA___V8*/ meltfptr[7];
      /*_.LIST_EVERY__V10*/ meltfptr[9] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!LIST_EVERY */ meltfrout->tabval[3])),
		    (melt_ptr_t) ( /*_.FIRSTLIST__V3*/ meltfptr[2]),
		    (MELTBPARSTR_PTR ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-first.melt:3238:/ quasiblock");


    /*^newclosure */
		 /*newclosure *//*_.LAMBDA___V12*/ meltfptr[11] =
      (melt_ptr_t)
      meltgc_new_closure ((meltobject_ptr_t)
			  (((melt_ptr_t) (MELT_PREDEF (DISCR_CLOSURE)))),
			  (meltroutine_ptr_t) (( /*!konst_6 */ meltfrout->
						tabval[6])), (1));
    ;
    /*^putclosedv */
    /*putclosv */
    melt_assertmsg ("putclosv checkclo",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.LAMBDA___V12*/ meltfptr[11])) ==
		    MELTOBMAG_CLOSURE);
    melt_assertmsg ("putclosv checkoff", 0 >= 0
		    && 0 <
		    melt_closure_size ((melt_ptr_t)
				       ( /*_.LAMBDA___V12*/ meltfptr[11])));
    ((meltclosure_ptr_t) /*_.LAMBDA___V12*/ meltfptr[11])->tabval[0] =
      (melt_ptr_t) ( /*_.REVLASTLIST__V5*/ meltfptr[4]);
    ;
    /*_.LAMBDA___V11*/ meltfptr[10] = /*_.LAMBDA___V12*/ meltfptr[11];;
    MELT_LOCATION ("warmelt-first.melt:3236:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & /*_.LAMBDA___V11*/ meltfptr[10];
      /*_.LIST_EVERY__V13*/ meltfptr[12] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!LIST_EVERY */ meltfrout->tabval[3])),
		    (melt_ptr_t) ( /*_.LASTLIST__V4*/ meltfptr[3]),
		    (MELTBPARSTR_PTR ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-first.melt:3243:/ quasiblock");


    /*^newclosure */
		 /*newclosure *//*_.LAMBDA___V15*/ meltfptr[14] =
      (melt_ptr_t)
      meltgc_new_closure ((meltobject_ptr_t)
			  (((melt_ptr_t) (MELT_PREDEF (DISCR_CLOSURE)))),
			  (meltroutine_ptr_t) (( /*!konst_8 */ meltfrout->
						tabval[8])), (1));
    ;
    /*^putclosedv */
    /*putclosv */
    melt_assertmsg ("putclosv checkclo",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.LAMBDA___V15*/ meltfptr[14])) ==
		    MELTOBMAG_CLOSURE);
    melt_assertmsg ("putclosv checkoff", 0 >= 0
		    && 0 <
		    melt_closure_size ((melt_ptr_t)
				       ( /*_.LAMBDA___V15*/ meltfptr[14])));
    ((meltclosure_ptr_t) /*_.LAMBDA___V15*/ meltfptr[14])->tabval[0] =
      (melt_ptr_t) ( /*_.RESCONT__V6*/ meltfptr[5]);
    ;
    /*_.LAMBDA___V14*/ meltfptr[13] = /*_.LAMBDA___V15*/ meltfptr[14];;
    MELT_LOCATION ("warmelt-first.melt:3241:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & /*_.LAMBDA___V14*/ meltfptr[13];
      /*_.LIST_EVERY__V16*/ meltfptr[15] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!LIST_EVERY */ meltfrout->tabval[3])),
		    (melt_ptr_t) ( /*_.REVLASTLIST__V5*/ meltfptr[4]),
		    (MELTBPARSTR_PTR ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;
    /*_.LET___V2*/ meltfptr[1] = /*_.LIST_EVERY__V16*/ meltfptr[15];;

    MELT_LOCATION ("warmelt-first.melt:3221:/ clear");
	   /*clear *//*_.FIRSTLIST__V3*/ meltfptr[2] = 0;
    /*^clear */
	   /*clear *//*_.LASTLIST__V4*/ meltfptr[3] = 0;
    /*^clear */
	   /*clear *//*_.REVLASTLIST__V5*/ meltfptr[4] = 0;
    /*^clear */
	   /*clear *//*_.RESCONT__V6*/ meltfptr[5] = 0;
    /*^clear */
	   /*clear *//*_.LAMBDA___V8*/ meltfptr[7] = 0;
    /*^clear */
	   /*clear *//*_.LIST_EVERY__V10*/ meltfptr[9] = 0;
    /*^clear */
	   /*clear *//*_.LAMBDA___V11*/ meltfptr[10] = 0;
    /*^clear */
	   /*clear *//*_.LIST_EVERY__V13*/ meltfptr[12] = 0;
    /*^clear */
	   /*clear *//*_.LAMBDA___V14*/ meltfptr[13] = 0;
    /*^clear */
	   /*clear *//*_.LIST_EVERY__V16*/ meltfptr[15] = 0;
    MELT_LOCATION ("warmelt-first.melt:3220:/ quasiblock");


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.LET___V2*/ meltfptr[1];;

    {
      MELT_LOCATION ("warmelt-first.melt:3220:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.LET___V2*/ meltfptr[1] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("INIT_UNITSTARTER", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_23_warmelt_first_INIT_UNITSTARTER_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_23_warmelt_first_INIT_UNITSTARTER */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_24_warmelt_first_LAMBDA___4__ (meltclosure_ptr_t meltclosp_,
					melt_ptr_t meltfirstargp_,
					const melt_argdescr_cell_t
					meltxargdescr_[],
					union meltparam_un * meltxargtab_,
					const melt_argdescr_cell_t
					meltxresdescr_[],
					union meltparam_un * meltxrestab_)
{
  long current_blocklevel_signals_meltrout_24_warmelt_first_LAMBDA___4___melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_24_warmelt_first_LAMBDA___4___st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 4
    melt_ptr_t mcfr_varptr[4];
/*no varnum*/
#define MELTFRAM_NBVARNUM /*none*/0
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_24_warmelt_first_LAMBDA___4__ is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_24_warmelt_first_LAMBDA___4___st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 4; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_24_warmelt_first_LAMBDA___4__ nbval 4*/
  meltfram__.mcfr_nbvar = 4 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("LAMBDA_", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-first.melt:3229:/ getarg");
 /*_.FIRSTPROC__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {

    MELT_LOCATION ("warmelt-first.melt:3230:/ quasiblock");


    /*^getslot */
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) (( /*~RESCONT */ meltfclos->tabval[0])) /*=obj*/ ;
      melt_object_get_field (slot, obj, 0, "REFERENCED_VALUE");
  /*_.PREVRES__V3*/ meltfptr[2] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-first.melt:3231:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & ( /*!START_UNIT_DELAYED_QUEUE */ meltfrout->
			  tabval[0]);
      /*_.NEXTRES__V4*/ meltfptr[3] =
	melt_apply ((meltclosure_ptr_t) ( /*_.FIRSTPROC__V2*/ meltfptr[1]),
		    (melt_ptr_t) ( /*_.PREVRES__V3*/ meltfptr[2]),
		    (MELTBPARSTR_PTR ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-first.melt:3233:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @REFERENCED_VALUE",
		    melt_magic_discr ((melt_ptr_t)
				      (( /*~RESCONT */ meltfclos->
					tabval[0]))) == MELTOBMAG_OBJECT);
    melt_putfield_object ((( /*~RESCONT */ meltfclos->tabval[0])), (0),
			  ( /*_.NEXTRES__V4*/ meltfptr[3]),
			  "REFERENCED_VALUE");
    ;
    /*^touch */
    meltgc_touch (( /*~RESCONT */ meltfclos->tabval[0]));
    ;
    /*^touchobj */

    melt_dbgtrace_written_object (( /*~RESCONT */ meltfclos->tabval[0]),
				  "put-fields");
    ;


    MELT_LOCATION ("warmelt-first.melt:3230:/ clear");
	   /*clear *//*_.PREVRES__V3*/ meltfptr[2] = 0;
    /*^clear */
	   /*clear *//*_.NEXTRES__V4*/ meltfptr[3] = 0;
    /*epilog */
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("LAMBDA_", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_24_warmelt_first_LAMBDA___4___melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*noretval */ NULL);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_24_warmelt_first_LAMBDA___4__ */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_25_warmelt_first_LAMBDA___5__ (meltclosure_ptr_t meltclosp_,
					melt_ptr_t meltfirstargp_,
					const melt_argdescr_cell_t
					meltxargdescr_[],
					union meltparam_un * meltxargtab_,
					const melt_argdescr_cell_t
					meltxresdescr_[],
					union meltparam_un * meltxrestab_)
{
  long current_blocklevel_signals_meltrout_25_warmelt_first_LAMBDA___5___melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_25_warmelt_first_LAMBDA___5___st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 2
    melt_ptr_t mcfr_varptr[2];
#define MELTFRAM_NBVARNUM 1
    long mcfr_varnum[1];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_25_warmelt_first_LAMBDA___5__ is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_25_warmelt_first_LAMBDA___5___st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 2; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_25_warmelt_first_LAMBDA___5__ nbval 2*/
  meltfram__.mcfr_nbvar = 2 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("LAMBDA_", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-first.melt:3238:/ getarg");
 /*_.LASTPROC__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {

    MELT_LOCATION ("warmelt-first.melt:3239:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#IS_CLOSURE__L1*/ meltfnum[0] =
      (melt_magic_discr ((melt_ptr_t) ( /*_.LASTPROC__V2*/ meltfptr[1])) ==
       MELTOBMAG_CLOSURE);;
    MELT_LOCATION ("warmelt-first.melt:3239:/ cond");
    /*cond */ if ( /*_#IS_CLOSURE__L1*/ meltfnum[0])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{


	  {
	    /*^locexp */
	    meltgc_prepend_list ((melt_ptr_t)
				 (( /*~REVLASTLIST */ meltfclos->tabval[0])),
				 (melt_ptr_t) ( /*_.LASTPROC__V2*/
					       meltfptr[1]));
	  }
	  ;
	  /*epilog */
	}
	;
      }				/*noelse */
    ;
    /*epilog */

    MELT_LOCATION ("warmelt-first.melt:3238:/ clear");
	   /*clear *//*_#IS_CLOSURE__L1*/ meltfnum[0] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("LAMBDA_", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_25_warmelt_first_LAMBDA___5___melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*noretval */ NULL);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_25_warmelt_first_LAMBDA___5__ */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_26_warmelt_first_LAMBDA___6__ (meltclosure_ptr_t meltclosp_,
					melt_ptr_t meltfirstargp_,
					const melt_argdescr_cell_t
					meltxargdescr_[],
					union meltparam_un * meltxargtab_,
					const melt_argdescr_cell_t
					meltxresdescr_[],
					union meltparam_un * meltxrestab_)
{
  long current_blocklevel_signals_meltrout_26_warmelt_first_LAMBDA___6___melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_26_warmelt_first_LAMBDA___6___st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 4
    melt_ptr_t mcfr_varptr[4];
/*no varnum*/
#define MELTFRAM_NBVARNUM /*none*/0
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_26_warmelt_first_LAMBDA___6__ is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_26_warmelt_first_LAMBDA___6___st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 4; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_26_warmelt_first_LAMBDA___6__ nbval 4*/
  meltfram__.mcfr_nbvar = 4 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("LAMBDA_", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-first.melt:3243:/ getarg");
 /*_.LASTPROC__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {

    MELT_LOCATION ("warmelt-first.melt:3244:/ quasiblock");


    /*^getslot */
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) (( /*~RESCONT */ meltfclos->tabval[0])) /*=obj*/ ;
      melt_object_get_field (slot, obj, 0, "REFERENCED_VALUE");
  /*_.PREVRES__V3*/ meltfptr[2] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-first.melt:3245:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & ( /*!START_UNIT_DELAYED_QUEUE */ meltfrout->
			  tabval[0]);
      /*_.NEXTRES__V4*/ meltfptr[3] =
	melt_apply ((meltclosure_ptr_t) ( /*_.LASTPROC__V2*/ meltfptr[1]),
		    (melt_ptr_t) ( /*_.PREVRES__V3*/ meltfptr[2]),
		    (MELTBPARSTR_PTR ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-first.melt:3247:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @REFERENCED_VALUE",
		    melt_magic_discr ((melt_ptr_t)
				      (( /*~RESCONT */ meltfclos->
					tabval[0]))) == MELTOBMAG_OBJECT);
    melt_putfield_object ((( /*~RESCONT */ meltfclos->tabval[0])), (0),
			  ( /*_.NEXTRES__V4*/ meltfptr[3]),
			  "REFERENCED_VALUE");
    ;
    /*^touch */
    meltgc_touch (( /*~RESCONT */ meltfclos->tabval[0]));
    ;
    /*^touchobj */

    melt_dbgtrace_written_object (( /*~RESCONT */ meltfclos->tabval[0]),
				  "put-fields");
    ;


    MELT_LOCATION ("warmelt-first.melt:3244:/ clear");
	   /*clear *//*_.PREVRES__V3*/ meltfptr[2] = 0;
    /*^clear */
	   /*clear *//*_.NEXTRES__V4*/ meltfptr[3] = 0;
    /*epilog */
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("LAMBDA_", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_26_warmelt_first_LAMBDA___6___melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*noretval */ NULL);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_26_warmelt_first_LAMBDA___6__ */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_27_warmelt_first_AT_START_UNIT_FIRST (meltclosure_ptr_t meltclosp_,
					       melt_ptr_t meltfirstargp_,
					       const melt_argdescr_cell_t
					       meltxargdescr_[],
					       union meltparam_un *
					       meltxargtab_,
					       const melt_argdescr_cell_t
					       meltxresdescr_[],
					       union meltparam_un *
					       meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_27_warmelt_first_AT_START_UNIT_FIRST_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_27_warmelt_first_AT_START_UNIT_FIRST_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 3
    melt_ptr_t mcfr_varptr[3];
#define MELTFRAM_NBVARNUM 1
    long mcfr_varnum[1];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_27_warmelt_first_AT_START_UNIT_FIRST is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_27_warmelt_first_AT_START_UNIT_FIRST_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 3; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_27_warmelt_first_AT_START_UNIT_FIRST nbval 3*/
  meltfram__.mcfr_nbvar = 3 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("AT_START_UNIT_FIRST", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-first.melt:3252:/ getarg");
 /*_.FUN__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {

    MELT_LOCATION ("warmelt-first.melt:3254:/ quasiblock");


    /*^getslot */
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj =
	(melt_ptr_t) (( /*!START_UNIT_DELAYED_QUEUE */ meltfrout->
		       tabval[0])) /*=obj*/ ;
      melt_object_get_field (slot, obj, 2, "DELQU_FIRST");
  /*_.FIRSTLIST__V3*/ meltfptr[2] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-first.melt:3256:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#IS_CLOSURE__L1*/ meltfnum[0] =
      (melt_magic_discr ((melt_ptr_t) ( /*_.FUN__V2*/ meltfptr[1])) ==
       MELTOBMAG_CLOSURE);;
    MELT_LOCATION ("warmelt-first.melt:3256:/ cond");
    /*cond */ if ( /*_#IS_CLOSURE__L1*/ meltfnum[0])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{


	  {
	    /*^locexp */
	    meltgc_append_list ((melt_ptr_t)
				( /*_.FIRSTLIST__V3*/ meltfptr[2]),
				(melt_ptr_t) ( /*_.FUN__V2*/ meltfptr[1]));
	  }
	  ;
	  /*epilog */
	}
	;
      }				/*noelse */
    ;

    MELT_LOCATION ("warmelt-first.melt:3254:/ clear");
	   /*clear *//*_.FIRSTLIST__V3*/ meltfptr[2] = 0;
    /*^clear */
	   /*clear *//*_#IS_CLOSURE__L1*/ meltfnum[0] = 0;
    /*epilog */
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("AT_START_UNIT_FIRST", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_27_warmelt_first_AT_START_UNIT_FIRST_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*noretval */ NULL);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_27_warmelt_first_AT_START_UNIT_FIRST */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_28_warmelt_first_AT_START_UNIT_LAST (meltclosure_ptr_t meltclosp_,
					      melt_ptr_t meltfirstargp_,
					      const melt_argdescr_cell_t
					      meltxargdescr_[],
					      union meltparam_un *
					      meltxargtab_,
					      const melt_argdescr_cell_t
					      meltxresdescr_[],
					      union meltparam_un *
					      meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_28_warmelt_first_AT_START_UNIT_LAST_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_28_warmelt_first_AT_START_UNIT_LAST_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 3
    melt_ptr_t mcfr_varptr[3];
#define MELTFRAM_NBVARNUM 1
    long mcfr_varnum[1];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_28_warmelt_first_AT_START_UNIT_LAST is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_28_warmelt_first_AT_START_UNIT_LAST_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 3; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_28_warmelt_first_AT_START_UNIT_LAST nbval 3*/
  meltfram__.mcfr_nbvar = 3 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("AT_START_UNIT_LAST", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-first.melt:3259:/ getarg");
 /*_.FUN__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {

    MELT_LOCATION ("warmelt-first.melt:3261:/ quasiblock");


    /*^getslot */
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj =
	(melt_ptr_t) (( /*!START_UNIT_DELAYED_QUEUE */ meltfrout->
		       tabval[0])) /*=obj*/ ;
      melt_object_get_field (slot, obj, 3, "DELQU_LAST");
  /*_.LASTLIST__V3*/ meltfptr[2] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-first.melt:3263:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#IS_CLOSURE__L1*/ meltfnum[0] =
      (melt_magic_discr ((melt_ptr_t) ( /*_.FUN__V2*/ meltfptr[1])) ==
       MELTOBMAG_CLOSURE);;
    MELT_LOCATION ("warmelt-first.melt:3263:/ cond");
    /*cond */ if ( /*_#IS_CLOSURE__L1*/ meltfnum[0])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{


	  {
	    /*^locexp */
	    meltgc_append_list ((melt_ptr_t)
				( /*_.LASTLIST__V3*/ meltfptr[2]),
				(melt_ptr_t) ( /*_.FUN__V2*/ meltfptr[1]));
	  }
	  ;
	  /*epilog */
	}
	;
      }				/*noelse */
    ;

    MELT_LOCATION ("warmelt-first.melt:3261:/ clear");
	   /*clear *//*_.LASTLIST__V3*/ meltfptr[2] = 0;
    /*^clear */
	   /*clear *//*_#IS_CLOSURE__L1*/ meltfnum[0] = 0;
    /*epilog */
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("AT_START_UNIT_LAST", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_28_warmelt_first_AT_START_UNIT_LAST_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*noretval */ NULL);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_28_warmelt_first_AT_START_UNIT_LAST */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_29_warmelt_first_INIT_UNITFINISHER (meltclosure_ptr_t meltclosp_,
					     melt_ptr_t meltfirstargp_,
					     const melt_argdescr_cell_t
					     meltxargdescr_[],
					     union meltparam_un *
					     meltxargtab_,
					     const melt_argdescr_cell_t
					     meltxresdescr_[],
					     union meltparam_un *
					     meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_29_warmelt_first_INIT_UNITFINISHER_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_29_warmelt_first_INIT_UNITFINISHER_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 16
    melt_ptr_t mcfr_varptr[16];
/*no varnum*/
#define MELTFRAM_NBVARNUM /*none*/0
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_29_warmelt_first_INIT_UNITFINISHER is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_29_warmelt_first_INIT_UNITFINISHER_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 16; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_29_warmelt_first_INIT_UNITFINISHER nbval 16*/
  meltfram__.mcfr_nbvar = 16 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("INIT_UNITFINISHER", meltcallcount);
/*getargs*/
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
  MELT_LOCATION ("warmelt-first.melt:3274:/ block");
/*anyblock*/
  {

    MELT_LOCATION ("warmelt-first.melt:3275:/ quasiblock");


    /*^getslot */
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj =
	(melt_ptr_t) (( /*!FINISH_UNIT_DELAYED_QUEUE */ meltfrout->
		       tabval[0])) /*=obj*/ ;
      melt_object_get_field (slot, obj, 2, "DELQU_FIRST");
  /*_.FIRSTLIST__V3*/ meltfptr[2] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-first.melt:3276:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj =
	(melt_ptr_t) (( /*!FINISH_UNIT_DELAYED_QUEUE */ meltfrout->
		       tabval[0])) /*=obj*/ ;
      melt_object_get_field (slot, obj, 3, "DELQU_LAST");
  /*_.LASTLIST__V4*/ meltfptr[3] = slot;
    };
    ;
 /*_.REVLASTLIST__V5*/ meltfptr[4] =
      (meltgc_new_list
       ((meltobject_ptr_t) (( /*!DISCR_LIST */ meltfrout->tabval[1]))));;
    MELT_LOCATION ("warmelt-first.melt:3278:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*^rawallocobj */
    /*rawallocobj */
    {
      melt_ptr_t newobj = 0;
      melt_raw_object_create (newobj,
			      (melt_ptr_t) (( /*!CLASS_REFERENCE */
					     meltfrout->tabval[2])), (1),
			      "CLASS_REFERENCE");
  /*_.INST__V7*/ meltfptr[6] =
	newobj;
    };
    ;
    /*^touchobj */

    melt_dbgtrace_written_object ( /*_.INST__V7*/ meltfptr[6],
				  "newly made instance");
    ;
    /*_.RESCONT__V6*/ meltfptr[5] = /*_.INST__V7*/ meltfptr[6];;
    MELT_LOCATION ("warmelt-first.melt:3283:/ quasiblock");


    /*^newclosure */
		 /*newclosure *//*_.LAMBDA___V9*/ meltfptr[8] =
      (melt_ptr_t)
      meltgc_new_closure ((meltobject_ptr_t)
			  (((melt_ptr_t) (MELT_PREDEF (DISCR_CLOSURE)))),
			  (meltroutine_ptr_t) (( /*!konst_5 */ meltfrout->
						tabval[5])), (1));
    ;
    /*^putclosedv */
    /*putclosv */
    melt_assertmsg ("putclosv checkclo",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.LAMBDA___V9*/ meltfptr[8])) ==
		    MELTOBMAG_CLOSURE);
    melt_assertmsg ("putclosv checkoff", 0 >= 0
		    && 0 <
		    melt_closure_size ((melt_ptr_t)
				       ( /*_.LAMBDA___V9*/ meltfptr[8])));
    ((meltclosure_ptr_t) /*_.LAMBDA___V9*/ meltfptr[8])->tabval[0] =
      (melt_ptr_t) ( /*_.RESCONT__V6*/ meltfptr[5]);
    ;
    /*_.LAMBDA___V8*/ meltfptr[7] = /*_.LAMBDA___V9*/ meltfptr[8];;
    MELT_LOCATION ("warmelt-first.melt:3281:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.LAMBDA___V8*/ meltfptr[7];
      /*_.LIST_EVERY__V10*/ meltfptr[9] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!LIST_EVERY */ meltfrout->tabval[3])),
		    (melt_ptr_t) ( /*_.FIRSTLIST__V3*/ meltfptr[2]),
		    (MELTBPARSTR_PTR ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-first.melt:3292:/ quasiblock");


    /*^newclosure */
		 /*newclosure *//*_.LAMBDA___V12*/ meltfptr[11] =
      (melt_ptr_t)
      meltgc_new_closure ((meltobject_ptr_t)
			  (((melt_ptr_t) (MELT_PREDEF (DISCR_CLOSURE)))),
			  (meltroutine_ptr_t) (( /*!konst_6 */ meltfrout->
						tabval[6])), (1));
    ;
    /*^putclosedv */
    /*putclosv */
    melt_assertmsg ("putclosv checkclo",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.LAMBDA___V12*/ meltfptr[11])) ==
		    MELTOBMAG_CLOSURE);
    melt_assertmsg ("putclosv checkoff", 0 >= 0
		    && 0 <
		    melt_closure_size ((melt_ptr_t)
				       ( /*_.LAMBDA___V12*/ meltfptr[11])));
    ((meltclosure_ptr_t) /*_.LAMBDA___V12*/ meltfptr[11])->tabval[0] =
      (melt_ptr_t) ( /*_.REVLASTLIST__V5*/ meltfptr[4]);
    ;
    /*_.LAMBDA___V11*/ meltfptr[10] = /*_.LAMBDA___V12*/ meltfptr[11];;
    MELT_LOCATION ("warmelt-first.melt:3290:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & /*_.LAMBDA___V11*/ meltfptr[10];
      /*_.LIST_EVERY__V13*/ meltfptr[12] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!LIST_EVERY */ meltfrout->tabval[3])),
		    (melt_ptr_t) ( /*_.LASTLIST__V4*/ meltfptr[3]),
		    (MELTBPARSTR_PTR ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-first.melt:3297:/ quasiblock");


    /*^newclosure */
		 /*newclosure *//*_.LAMBDA___V15*/ meltfptr[14] =
      (melt_ptr_t)
      meltgc_new_closure ((meltobject_ptr_t)
			  (((melt_ptr_t) (MELT_PREDEF (DISCR_CLOSURE)))),
			  (meltroutine_ptr_t) (( /*!konst_8 */ meltfrout->
						tabval[8])), (1));
    ;
    /*^putclosedv */
    /*putclosv */
    melt_assertmsg ("putclosv checkclo",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.LAMBDA___V15*/ meltfptr[14])) ==
		    MELTOBMAG_CLOSURE);
    melt_assertmsg ("putclosv checkoff", 0 >= 0
		    && 0 <
		    melt_closure_size ((melt_ptr_t)
				       ( /*_.LAMBDA___V15*/ meltfptr[14])));
    ((meltclosure_ptr_t) /*_.LAMBDA___V15*/ meltfptr[14])->tabval[0] =
      (melt_ptr_t) ( /*_.RESCONT__V6*/ meltfptr[5]);
    ;
    /*_.LAMBDA___V14*/ meltfptr[13] = /*_.LAMBDA___V15*/ meltfptr[14];;
    MELT_LOCATION ("warmelt-first.melt:3295:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & /*_.LAMBDA___V14*/ meltfptr[13];
      /*_.LIST_EVERY__V16*/ meltfptr[15] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!LIST_EVERY */ meltfrout->tabval[3])),
		    (melt_ptr_t) ( /*_.REVLASTLIST__V5*/ meltfptr[4]),
		    (MELTBPARSTR_PTR ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;
    /*_.LET___V2*/ meltfptr[1] = /*_.LIST_EVERY__V16*/ meltfptr[15];;

    MELT_LOCATION ("warmelt-first.melt:3275:/ clear");
	   /*clear *//*_.FIRSTLIST__V3*/ meltfptr[2] = 0;
    /*^clear */
	   /*clear *//*_.LASTLIST__V4*/ meltfptr[3] = 0;
    /*^clear */
	   /*clear *//*_.REVLASTLIST__V5*/ meltfptr[4] = 0;
    /*^clear */
	   /*clear *//*_.RESCONT__V6*/ meltfptr[5] = 0;
    /*^clear */
	   /*clear *//*_.LAMBDA___V8*/ meltfptr[7] = 0;
    /*^clear */
	   /*clear *//*_.LIST_EVERY__V10*/ meltfptr[9] = 0;
    /*^clear */
	   /*clear *//*_.LAMBDA___V11*/ meltfptr[10] = 0;
    /*^clear */
	   /*clear *//*_.LIST_EVERY__V13*/ meltfptr[12] = 0;
    /*^clear */
	   /*clear *//*_.LAMBDA___V14*/ meltfptr[13] = 0;
    /*^clear */
	   /*clear *//*_.LIST_EVERY__V16*/ meltfptr[15] = 0;
    MELT_LOCATION ("warmelt-first.melt:3274:/ quasiblock");


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.LET___V2*/ meltfptr[1];;

    {
      MELT_LOCATION ("warmelt-first.melt:3274:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.LET___V2*/ meltfptr[1] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("INIT_UNITFINISHER", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_29_warmelt_first_INIT_UNITFINISHER_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_29_warmelt_first_INIT_UNITFINISHER */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_30_warmelt_first_LAMBDA___7__ (meltclosure_ptr_t meltclosp_,
					melt_ptr_t meltfirstargp_,
					const melt_argdescr_cell_t
					meltxargdescr_[],
					union meltparam_un * meltxargtab_,
					const melt_argdescr_cell_t
					meltxresdescr_[],
					union meltparam_un * meltxrestab_)
{
  long current_blocklevel_signals_meltrout_30_warmelt_first_LAMBDA___7___melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_30_warmelt_first_LAMBDA___7___st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 4
    melt_ptr_t mcfr_varptr[4];
/*no varnum*/
#define MELTFRAM_NBVARNUM /*none*/0
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_30_warmelt_first_LAMBDA___7__ is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_30_warmelt_first_LAMBDA___7___st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 4; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_30_warmelt_first_LAMBDA___7__ nbval 4*/
  meltfram__.mcfr_nbvar = 4 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("LAMBDA_", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-first.melt:3283:/ getarg");
 /*_.FIRSTPROC__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {

    MELT_LOCATION ("warmelt-first.melt:3284:/ quasiblock");


    /*^getslot */
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) (( /*~RESCONT */ meltfclos->tabval[0])) /*=obj*/ ;
      melt_object_get_field (slot, obj, 0, "REFERENCED_VALUE");
  /*_.PREVRES__V3*/ meltfptr[2] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-first.melt:3285:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & ( /*!FINISH_UNIT_DELAYED_QUEUE */ meltfrout->
			  tabval[0]);
      /*_.NEXTRES__V4*/ meltfptr[3] =
	melt_apply ((meltclosure_ptr_t) ( /*_.FIRSTPROC__V2*/ meltfptr[1]),
		    (melt_ptr_t) ( /*_.PREVRES__V3*/ meltfptr[2]),
		    (MELTBPARSTR_PTR ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-first.melt:3287:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @REFERENCED_VALUE",
		    melt_magic_discr ((melt_ptr_t)
				      (( /*~RESCONT */ meltfclos->
					tabval[0]))) == MELTOBMAG_OBJECT);
    melt_putfield_object ((( /*~RESCONT */ meltfclos->tabval[0])), (0),
			  ( /*_.NEXTRES__V4*/ meltfptr[3]),
			  "REFERENCED_VALUE");
    ;
    /*^touch */
    meltgc_touch (( /*~RESCONT */ meltfclos->tabval[0]));
    ;
    /*^touchobj */

    melt_dbgtrace_written_object (( /*~RESCONT */ meltfclos->tabval[0]),
				  "put-fields");
    ;


    MELT_LOCATION ("warmelt-first.melt:3284:/ clear");
	   /*clear *//*_.PREVRES__V3*/ meltfptr[2] = 0;
    /*^clear */
	   /*clear *//*_.NEXTRES__V4*/ meltfptr[3] = 0;
    /*epilog */
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("LAMBDA_", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_30_warmelt_first_LAMBDA___7___melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*noretval */ NULL);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_30_warmelt_first_LAMBDA___7__ */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_31_warmelt_first_LAMBDA___8__ (meltclosure_ptr_t meltclosp_,
					melt_ptr_t meltfirstargp_,
					const melt_argdescr_cell_t
					meltxargdescr_[],
					union meltparam_un * meltxargtab_,
					const melt_argdescr_cell_t
					meltxresdescr_[],
					union meltparam_un * meltxrestab_)
{
  long current_blocklevel_signals_meltrout_31_warmelt_first_LAMBDA___8___melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_31_warmelt_first_LAMBDA___8___st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 2
    melt_ptr_t mcfr_varptr[2];
#define MELTFRAM_NBVARNUM 1
    long mcfr_varnum[1];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_31_warmelt_first_LAMBDA___8__ is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_31_warmelt_first_LAMBDA___8___st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 2; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_31_warmelt_first_LAMBDA___8__ nbval 2*/
  meltfram__.mcfr_nbvar = 2 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("LAMBDA_", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-first.melt:3292:/ getarg");
 /*_.LASTPROC__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {

    MELT_LOCATION ("warmelt-first.melt:3293:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#IS_CLOSURE__L1*/ meltfnum[0] =
      (melt_magic_discr ((melt_ptr_t) ( /*_.LASTPROC__V2*/ meltfptr[1])) ==
       MELTOBMAG_CLOSURE);;
    MELT_LOCATION ("warmelt-first.melt:3293:/ cond");
    /*cond */ if ( /*_#IS_CLOSURE__L1*/ meltfnum[0])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{


	  {
	    /*^locexp */
	    meltgc_prepend_list ((melt_ptr_t)
				 (( /*~REVLASTLIST */ meltfclos->tabval[0])),
				 (melt_ptr_t) ( /*_.LASTPROC__V2*/
					       meltfptr[1]));
	  }
	  ;
	  /*epilog */
	}
	;
      }				/*noelse */
    ;
    /*epilog */

    MELT_LOCATION ("warmelt-first.melt:3292:/ clear");
	   /*clear *//*_#IS_CLOSURE__L1*/ meltfnum[0] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("LAMBDA_", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_31_warmelt_first_LAMBDA___8___melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*noretval */ NULL);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_31_warmelt_first_LAMBDA___8__ */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_32_warmelt_first_LAMBDA___9__ (meltclosure_ptr_t meltclosp_,
					melt_ptr_t meltfirstargp_,
					const melt_argdescr_cell_t
					meltxargdescr_[],
					union meltparam_un * meltxargtab_,
					const melt_argdescr_cell_t
					meltxresdescr_[],
					union meltparam_un * meltxrestab_)
{
  long current_blocklevel_signals_meltrout_32_warmelt_first_LAMBDA___9___melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_32_warmelt_first_LAMBDA___9___st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 4
    melt_ptr_t mcfr_varptr[4];
/*no varnum*/
#define MELTFRAM_NBVARNUM /*none*/0
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_32_warmelt_first_LAMBDA___9__ is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_32_warmelt_first_LAMBDA___9___st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 4; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_32_warmelt_first_LAMBDA___9__ nbval 4*/
  meltfram__.mcfr_nbvar = 4 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("LAMBDA_", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-first.melt:3297:/ getarg");
 /*_.LASTPROC__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {

    MELT_LOCATION ("warmelt-first.melt:3298:/ quasiblock");


    /*^getslot */
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) (( /*~RESCONT */ meltfclos->tabval[0])) /*=obj*/ ;
      melt_object_get_field (slot, obj, 0, "REFERENCED_VALUE");
  /*_.PREVRES__V3*/ meltfptr[2] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-first.melt:3299:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & ( /*!FINISH_UNIT_DELAYED_QUEUE */ meltfrout->
			  tabval[0]);
      /*_.NEXTRES__V4*/ meltfptr[3] =
	melt_apply ((meltclosure_ptr_t) ( /*_.LASTPROC__V2*/ meltfptr[1]),
		    (melt_ptr_t) ( /*_.PREVRES__V3*/ meltfptr[2]),
		    (MELTBPARSTR_PTR ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-first.melt:3301:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @REFERENCED_VALUE",
		    melt_magic_discr ((melt_ptr_t)
				      (( /*~RESCONT */ meltfclos->
					tabval[0]))) == MELTOBMAG_OBJECT);
    melt_putfield_object ((( /*~RESCONT */ meltfclos->tabval[0])), (0),
			  ( /*_.NEXTRES__V4*/ meltfptr[3]),
			  "REFERENCED_VALUE");
    ;
    /*^touch */
    meltgc_touch (( /*~RESCONT */ meltfclos->tabval[0]));
    ;
    /*^touchobj */

    melt_dbgtrace_written_object (( /*~RESCONT */ meltfclos->tabval[0]),
				  "put-fields");
    ;


    MELT_LOCATION ("warmelt-first.melt:3298:/ clear");
	   /*clear *//*_.PREVRES__V3*/ meltfptr[2] = 0;
    /*^clear */
	   /*clear *//*_.NEXTRES__V4*/ meltfptr[3] = 0;
    /*epilog */
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("LAMBDA_", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_32_warmelt_first_LAMBDA___9___melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*noretval */ NULL);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_32_warmelt_first_LAMBDA___9__ */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_33_warmelt_first_AT_FINISH_UNIT_FIRST (meltclosure_ptr_t meltclosp_,
						melt_ptr_t meltfirstargp_,
						const melt_argdescr_cell_t
						meltxargdescr_[],
						union meltparam_un *
						meltxargtab_,
						const melt_argdescr_cell_t
						meltxresdescr_[],
						union meltparam_un *
						meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_33_warmelt_first_AT_FINISH_UNIT_FIRST_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_33_warmelt_first_AT_FINISH_UNIT_FIRST_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 3
    melt_ptr_t mcfr_varptr[3];
#define MELTFRAM_NBVARNUM 1
    long mcfr_varnum[1];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_33_warmelt_first_AT_FINISH_UNIT_FIRST is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_33_warmelt_first_AT_FINISH_UNIT_FIRST_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 3; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_33_warmelt_first_AT_FINISH_UNIT_FIRST nbval 3*/
  meltfram__.mcfr_nbvar = 3 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("AT_FINISH_UNIT_FIRST", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-first.melt:3306:/ getarg");
 /*_.FUN__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {

    MELT_LOCATION ("warmelt-first.melt:3309:/ quasiblock");


    /*^getslot */
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj =
	(melt_ptr_t) (( /*!FINISH_UNIT_DELAYED_QUEUE */ meltfrout->
		       tabval[0])) /*=obj*/ ;
      melt_object_get_field (slot, obj, 2, "DELQU_FIRST");
  /*_.FIRSTLIST__V3*/ meltfptr[2] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-first.melt:3311:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#IS_CLOSURE__L1*/ meltfnum[0] =
      (melt_magic_discr ((melt_ptr_t) ( /*_.FUN__V2*/ meltfptr[1])) ==
       MELTOBMAG_CLOSURE);;
    MELT_LOCATION ("warmelt-first.melt:3311:/ cond");
    /*cond */ if ( /*_#IS_CLOSURE__L1*/ meltfnum[0])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{


	  {
	    /*^locexp */
	    meltgc_append_list ((melt_ptr_t)
				( /*_.FIRSTLIST__V3*/ meltfptr[2]),
				(melt_ptr_t) ( /*_.FUN__V2*/ meltfptr[1]));
	  }
	  ;
	  /*epilog */
	}
	;
      }				/*noelse */
    ;

    MELT_LOCATION ("warmelt-first.melt:3309:/ clear");
	   /*clear *//*_.FIRSTLIST__V3*/ meltfptr[2] = 0;
    /*^clear */
	   /*clear *//*_#IS_CLOSURE__L1*/ meltfnum[0] = 0;
    /*epilog */
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("AT_FINISH_UNIT_FIRST", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_33_warmelt_first_AT_FINISH_UNIT_FIRST_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*noretval */ NULL);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_33_warmelt_first_AT_FINISH_UNIT_FIRST */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_34_warmelt_first_AT_FINISH_UNIT_LAST (meltclosure_ptr_t meltclosp_,
					       melt_ptr_t meltfirstargp_,
					       const melt_argdescr_cell_t
					       meltxargdescr_[],
					       union meltparam_un *
					       meltxargtab_,
					       const melt_argdescr_cell_t
					       meltxresdescr_[],
					       union meltparam_un *
					       meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_34_warmelt_first_AT_FINISH_UNIT_LAST_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_34_warmelt_first_AT_FINISH_UNIT_LAST_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 3
    melt_ptr_t mcfr_varptr[3];
#define MELTFRAM_NBVARNUM 1
    long mcfr_varnum[1];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_34_warmelt_first_AT_FINISH_UNIT_LAST is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_34_warmelt_first_AT_FINISH_UNIT_LAST_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 3; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_34_warmelt_first_AT_FINISH_UNIT_LAST nbval 3*/
  meltfram__.mcfr_nbvar = 3 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("AT_FINISH_UNIT_LAST", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-first.melt:3314:/ getarg");
 /*_.FUN__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {

    MELT_LOCATION ("warmelt-first.melt:3317:/ quasiblock");


    /*^getslot */
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj =
	(melt_ptr_t) (( /*!FINISH_UNIT_DELAYED_QUEUE */ meltfrout->
		       tabval[0])) /*=obj*/ ;
      melt_object_get_field (slot, obj, 3, "DELQU_LAST");
  /*_.LASTLIST__V3*/ meltfptr[2] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-first.melt:3319:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#IS_CLOSURE__L1*/ meltfnum[0] =
      (melt_magic_discr ((melt_ptr_t) ( /*_.FUN__V2*/ meltfptr[1])) ==
       MELTOBMAG_CLOSURE);;
    MELT_LOCATION ("warmelt-first.melt:3319:/ cond");
    /*cond */ if ( /*_#IS_CLOSURE__L1*/ meltfnum[0])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{


	  {
	    /*^locexp */
	    meltgc_append_list ((melt_ptr_t)
				( /*_.LASTLIST__V3*/ meltfptr[2]),
				(melt_ptr_t) ( /*_.FUN__V2*/ meltfptr[1]));
	  }
	  ;
	  /*epilog */
	}
	;
      }				/*noelse */
    ;

    MELT_LOCATION ("warmelt-first.melt:3317:/ clear");
	   /*clear *//*_.LASTLIST__V3*/ meltfptr[2] = 0;
    /*^clear */
	   /*clear *//*_#IS_CLOSURE__L1*/ meltfnum[0] = 0;
    /*epilog */
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("AT_FINISH_UNIT_LAST", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_34_warmelt_first_AT_FINISH_UNIT_LAST_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*noretval */ NULL);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_34_warmelt_first_AT_FINISH_UNIT_LAST */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_35_warmelt_first_INIT_OPTIONSETTER (meltclosure_ptr_t meltclosp_,
					     melt_ptr_t meltfirstargp_,
					     const melt_argdescr_cell_t
					     meltxargdescr_[],
					     union meltparam_un *
					     meltxargtab_,
					     const melt_argdescr_cell_t
					     meltxresdescr_[],
					     union meltparam_un *
					     meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_35_warmelt_first_INIT_OPTIONSETTER_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_35_warmelt_first_INIT_OPTIONSETTER_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 18
    melt_ptr_t mcfr_varptr[18];
#define MELTFRAM_NBVARNUM 2
    long mcfr_varnum[2];
/*others*/
    const char *loc_CSTRING__o0;
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_35_warmelt_first_INIT_OPTIONSETTER is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_35_warmelt_first_INIT_OPTIONSETTER_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 18; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_35_warmelt_first_INIT_OPTIONSETTER nbval 18*/
  meltfram__.mcfr_nbvar = 18 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("INIT_OPTIONSETTER", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-first.melt:3336:/ getarg");
 /*_.OPTSYMB__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_CSTRING)
    goto lab_endgetargs;
 /*_?*/ meltfram__.loc_CSTRING__o0 = meltxargtab_[0].meltbp_cstring;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {

    MELT_LOCATION ("warmelt-first.melt:3337:/ quasiblock");


    /*^getslot */
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj =
	(melt_ptr_t) (( /*!OPTION_MAP_CONTAINER */ meltfrout->
		       tabval[0])) /*=obj*/ ;
      melt_object_get_field (slot, obj, 0, "REFERENCED_VALUE");
  /*_.OPTMAP__V4*/ meltfptr[3] = slot;
    };
    ;
 /*_.OPTDEC__V5*/ meltfptr[4] =
      /*mapobject_get */
      melt_get_mapobjects ((meltmapobjects_ptr_t)
			   ( /*_.OPTMAP__V4*/ meltfptr[3]),
			   (meltobject_ptr_t) ( /*_.OPTSYMB__V2*/
					       meltfptr[1]));;
    MELT_LOCATION ("warmelt-first.melt:3339:/ cond");
    /*cond */ if (
		   /*ifisa */
		   melt_is_instance_of ((melt_ptr_t)
					( /*_.OPTSYMB__V2*/ meltfptr[1]),
					(melt_ptr_t) (( /*!CLASS_NAMED */
						       meltfrout->tabval[1])))
      )				/*then */
      {
	/*^cond.then */
	/*^getslot */
	{
	  melt_ptr_t slot = NULL, obj = NULL;
	  obj = (melt_ptr_t) ( /*_.OPTSYMB__V2*/ meltfptr[1]) /*=obj*/ ;
	  melt_object_get_field (slot, obj, 1, "NAMED_NAME");
   /*_.OPTNAME__V6*/ meltfptr[5] = slot;
	};
	;
      }
    else
      {				/*^cond.else */

  /*_.OPTNAME__V6*/ meltfptr[5] = NULL;;
      }
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-first.melt:3341:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L1*/ meltfnum[0] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.OPTSYMB__V2*/ meltfptr[1]),
			     (melt_ptr_t) (( /*!CLASS_SYMBOL */ meltfrout->
					    tabval[2])));;
      MELT_LOCATION ("warmelt-first.melt:3341:/ cond");
      /*cond */ if ( /*_#IS_A__L1*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V8*/ meltfptr[7] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-first.melt:3341:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check optsymb"),
				  ("warmelt-first.melt")
				  ? ("warmelt-first.melt") : __FILE__,
				  (3341) ? (3341) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V8*/ meltfptr[7] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V7*/ meltfptr[6] = /*_.IFELSE___V8*/ meltfptr[7];;
      /*epilog */

      MELT_LOCATION ("warmelt-first.melt:3341:/ clear");
	     /*clear *//*_#IS_A__L1*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V8*/ meltfptr[7] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V7*/ meltfptr[6] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-first.melt:3342:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^cond */
    /*cond */ if ( /*_.OPTDEC__V5*/ meltfptr[4])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-first.melt:3343:/ quasiblock");


	  /*^cond */
	  /*cond */ if (
			 /*ifisa */
			 melt_is_instance_of ((melt_ptr_t)
					      ( /*_.OPTDEC__V5*/ meltfptr[4]),
					      (melt_ptr_t) (( /*!CLASS_OPTION_DESCRIPTOR */ meltfrout->tabval[3])))
	    )			/*then */
	    {
	      /*^cond.then */
	      /*^getslot */
	      {
		melt_ptr_t slot = NULL, obj = NULL;
		obj = (melt_ptr_t) ( /*_.OPTDEC__V5*/ meltfptr[4]) /*=obj*/ ;
		melt_object_get_field (slot, obj, 1, "OPTDESC_FUN");
     /*_.OPTFUN__V11*/ meltfptr[10] = slot;
	      };
	      ;
	    }
	  else
	    {			/*^cond.else */

    /*_.OPTFUN__V11*/ meltfptr[10] = NULL;;
	    }
	  ;

#if MELT_HAVE_DEBUG
	  MELT_LOCATION ("warmelt-first.melt:3345:/ cppif.then");
	  /*^block */
	  /*anyblock */
	  {

	    /*^checksignal */
	    MELT_CHECK_SIGNAL ();
	    ;
     /*_#IS_A__L2*/ meltfnum[0] =
	      melt_is_instance_of ((melt_ptr_t)
				   ( /*_.OPTDEC__V5*/ meltfptr[4]),
				   (melt_ptr_t) (( /*!CLASS_OPTION_DESCRIPTOR */ meltfrout->tabval[3])));;
	    MELT_LOCATION ("warmelt-first.melt:3345:/ cond");
	    /*cond */ if ( /*_#IS_A__L2*/ meltfnum[0])	/*then */
	      {
		/*^cond.then */
		/*_.IFELSE___V13*/ meltfptr[12] = ( /*nil */ NULL);;
	      }
	    else
	      {
		MELT_LOCATION ("warmelt-first.melt:3345:/ cond.else");

		/*^block */
		/*anyblock */
		{




		  {
		    /*^locexp */
		    melt_assert_failed (("check optdec"),
					("warmelt-first.melt")
					? ("warmelt-first.melt") : __FILE__,
					(3345) ? (3345) : __LINE__,
					__FUNCTION__);
		    ;
		  }
		  ;
		 /*clear *//*_.IFELSE___V13*/ meltfptr[12] = 0;
		  /*epilog */
		}
		;
	      }
	    ;
	    /*^compute */
	    /*_.IFCPP___V12*/ meltfptr[11] = /*_.IFELSE___V13*/ meltfptr[12];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-first.melt:3345:/ clear");
	       /*clear *//*_#IS_A__L2*/ meltfnum[0] = 0;
	    /*^clear */
	       /*clear *//*_.IFELSE___V13*/ meltfptr[12] = 0;
	  }

#else /*MELT_HAVE_DEBUG */
	  /*^cppif.else */
	  /*_.IFCPP___V12*/ meltfptr[11] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
	  ;
	  MELT_LOCATION ("warmelt-first.melt:3346:/ quasiblock");


	  /*^checksignal */
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^apply */
	  /*apply */
	  {
	    union meltparam_un argtab[1];
	    memset (&argtab, 0, sizeof (argtab));
	    /*^apply.arg */
	    argtab[0].meltbp_cstring = /*_?*/ meltfram__.loc_CSTRING__o0;
	    /*_.OPTRES__V15*/ meltfptr[14] =
	      melt_apply ((meltclosure_ptr_t)
			  ( /*_.OPTFUN__V11*/ meltfptr[10]),
			  (melt_ptr_t) ( /*_.OPTSYMB__V2*/ meltfptr[1]),
			  (MELTBPARSTR_CSTRING ""), argtab, "",
			  (union meltparam_un *) 0);
	  }
	  ;
	  MELT_LOCATION ("warmelt-first.melt:3348:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^quasiblock */


	  /*_.RETVAL___V1*/ meltfptr[0] = /*_.OPTRES__V15*/ meltfptr[14];;

	  {
	    MELT_LOCATION ("warmelt-first.melt:3348:/ locexp");
	    /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
	    if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	      melt_warn_for_no_expected_secondary_results ();
	    /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
	    ;
	  }
	  ;
	  /*^finalreturn */
	  ;
	  /*finalret */ goto labend_rout;
	  /*_.LET___V14*/ meltfptr[12] = /*_.RETURN___V16*/ meltfptr[15];;

	  MELT_LOCATION ("warmelt-first.melt:3346:/ clear");
	     /*clear *//*_.OPTRES__V15*/ meltfptr[14] = 0;
	  /*^clear */
	     /*clear *//*_.RETURN___V16*/ meltfptr[15] = 0;
	  /*_.LET___V10*/ meltfptr[9] = /*_.LET___V14*/ meltfptr[12];;

	  MELT_LOCATION ("warmelt-first.melt:3343:/ clear");
	     /*clear *//*_.OPTFUN__V11*/ meltfptr[10] = 0;
	  /*^clear */
	     /*clear *//*_.IFCPP___V12*/ meltfptr[11] = 0;
	  /*^clear */
	     /*clear *//*_.LET___V14*/ meltfptr[12] = 0;
	  /*_.IFELSE___V9*/ meltfptr[7] = /*_.LET___V10*/ meltfptr[9];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-first.melt:3342:/ clear");
	     /*clear *//*_.LET___V10*/ meltfptr[9] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

	/*^block */
	/*anyblock */
	{


	  {
	    MELT_LOCATION ("warmelt-first.melt:3351:/ locexp");
	    warning (0,
		     "unrecognized MELT option %s. Use -f[plugin-arg-]melt-option=help",
		     melt_string_str ((melt_ptr_t) /*_.OPTNAME__V6*/
				      meltfptr[5]));
	  }
	  ;
	  MELT_LOCATION ("warmelt-first.melt:3353:/ quasiblock");


   /*_.RETVAL___V1*/ meltfptr[0] = NULL;;

	  {
	    MELT_LOCATION ("warmelt-first.melt:3353:/ locexp");
	    /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
	    if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	      melt_warn_for_no_expected_secondary_results ();
	    /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
	    ;
	  }
	  ;
	  /*^finalreturn */
	  ;
	  /*finalret */ goto labend_rout;
	  MELT_LOCATION ("warmelt-first.melt:3349:/ quasiblock");


	  /*_.PROGN___V18*/ meltfptr[15] = /*_.RETURN___V17*/ meltfptr[14];;
	  /*^compute */
	  /*_.IFELSE___V9*/ meltfptr[7] = /*_.PROGN___V18*/ meltfptr[15];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-first.melt:3342:/ clear");
	     /*clear *//*_.RETURN___V17*/ meltfptr[14] = 0;
	  /*^clear */
	     /*clear *//*_.PROGN___V18*/ meltfptr[15] = 0;
	}
	;
      }
    ;
    /*_.LET___V3*/ meltfptr[2] = /*_.IFELSE___V9*/ meltfptr[7];;

    MELT_LOCATION ("warmelt-first.melt:3337:/ clear");
	   /*clear *//*_.OPTMAP__V4*/ meltfptr[3] = 0;
    /*^clear */
	   /*clear *//*_.OPTDEC__V5*/ meltfptr[4] = 0;
    /*^clear */
	   /*clear *//*_.OPTNAME__V6*/ meltfptr[5] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V7*/ meltfptr[6] = 0;
    /*^clear */
	   /*clear *//*_.IFELSE___V9*/ meltfptr[7] = 0;
    MELT_LOCATION ("warmelt-first.melt:3336:/ quasiblock");


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.LET___V3*/ meltfptr[2];;

    {
      MELT_LOCATION ("warmelt-first.melt:3336:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.LET___V3*/ meltfptr[2] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("INIT_OPTIONSETTER", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_35_warmelt_first_INIT_OPTIONSETTER_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_35_warmelt_first_INIT_OPTIONSETTER */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_36_warmelt_first_REGISTER_OPTION (meltclosure_ptr_t meltclosp_,
					   melt_ptr_t meltfirstargp_,
					   const melt_argdescr_cell_t
					   meltxargdescr_[],
					   union meltparam_un * meltxargtab_,
					   const melt_argdescr_cell_t
					   meltxresdescr_[],
					   union meltparam_un * meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_36_warmelt_first_REGISTER_OPTION_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_36_warmelt_first_REGISTER_OPTION_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 7
    melt_ptr_t mcfr_varptr[7];
#define MELTFRAM_NBVARNUM 5
    long mcfr_varnum[5];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_36_warmelt_first_REGISTER_OPTION is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_36_warmelt_first_REGISTER_OPTION_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 7; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_36_warmelt_first_REGISTER_OPTION nbval 7*/
  meltfram__.mcfr_nbvar = 7 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("REGISTER_OPTION", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-first.melt:3356:/ getarg");
 /*_.OPTSYMB__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.OPTHELP__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.OPTHELP__V3*/ meltfptr[2])) !=
	      NULL);


  /*getarg#2 */
  /*^getarg */
  if (meltxargdescr_[1] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.OPTFUN__V4*/ meltfptr[3] =
    (meltxargtab_[1].meltbp_aptr) ? (*(meltxargtab_[1].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.OPTFUN__V4*/ meltfptr[3])) !=
	      NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {

    MELT_LOCATION ("warmelt-first.melt:3359:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^checksignal */
    MELT_CHECK_SIGNAL ();
    ;
 /*_#IS_A__L1*/ meltfnum[0] =
      melt_is_instance_of ((melt_ptr_t) ( /*_.OPTSYMB__V2*/ meltfptr[1]),
			   (melt_ptr_t) (( /*!CLASS_SYMBOL */ meltfrout->
					  tabval[0])));;
    MELT_LOCATION ("warmelt-first.melt:3359:/ cond");
    /*cond */ if ( /*_#IS_A__L1*/ meltfnum[0])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  /*^checksignal */
	  MELT_CHECK_SIGNAL ();
	  ;
   /*_#IS_STRING__L3*/ meltfnum[2] =
	    (melt_magic_discr ((melt_ptr_t) ( /*_.OPTHELP__V3*/ meltfptr[2]))
	     == MELTOBMAG_STRING);;
	  MELT_LOCATION ("warmelt-first.melt:3359:/ cond");
	  /*cond */ if ( /*_#IS_STRING__L3*/ meltfnum[2])	/*then */
	    {
	      /*^cond.then */
	      /*^block */
	      /*anyblock */
	      {

     /*_#IS_CLOSURE__L5*/ meltfnum[4] =
		  (melt_magic_discr
		   ((melt_ptr_t) ( /*_.OPTFUN__V4*/ meltfptr[3])) ==
		   MELTOBMAG_CLOSURE);;
		/*^compute */
		/*_#IF___L4*/ meltfnum[3] = /*_#IS_CLOSURE__L5*/ meltfnum[4];;
		/*epilog */

		MELT_LOCATION ("warmelt-first.melt:3359:/ clear");
	       /*clear *//*_#IS_CLOSURE__L5*/ meltfnum[4] = 0;
	      }
	      ;
	    }
	  else
	    {			/*^cond.else */

    /*_#IF___L4*/ meltfnum[3] = 0;;
	    }
	  ;
	  /*^compute */
	  /*_#IF___L2*/ meltfnum[1] = /*_#IF___L4*/ meltfnum[3];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-first.melt:3359:/ clear");
	     /*clear *//*_#IS_STRING__L3*/ meltfnum[2] = 0;
	  /*^clear */
	     /*clear *//*_#IF___L4*/ meltfnum[3] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

  /*_#IF___L2*/ meltfnum[1] = 0;;
      }
    ;
    MELT_LOCATION ("warmelt-first.melt:3359:/ cond");
    /*cond */ if ( /*_#IF___L2*/ meltfnum[1])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-first.melt:3362:/ quasiblock");


	  /*^getslot */
	  {
	    melt_ptr_t slot = NULL, obj = NULL;
	    obj =
	      (melt_ptr_t) (( /*!OPTION_MAP_CONTAINER */ meltfrout->
			     tabval[1])) /*=obj*/ ;
	    melt_object_get_field (slot, obj, 0, "REFERENCED_VALUE");
    /*_.OPTMAP__V5*/ meltfptr[4] = slot;
	  };
	  ;
	  MELT_LOCATION ("warmelt-first.melt:3363:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^quasiblock */


	  /*^rawallocobj */
	  /*rawallocobj */
	  {
	    melt_ptr_t newobj = 0;
	    melt_raw_object_create (newobj,
				    (melt_ptr_t) (( /*!CLASS_OPTION_DESCRIPTOR */ meltfrout->tabval[2])), (3), "CLASS_OPTION_DESCRIPTOR");
    /*_.INST__V7*/ meltfptr[6] =
	      newobj;
	  };
	  ;
	  /*^putslot */
	  /*putslot */
	  melt_assertmsg ("putslot checkobj @OPTDESC_NAME",
			  melt_magic_discr ((melt_ptr_t)
					    ( /*_.INST__V7*/ meltfptr[6])) ==
			  MELTOBMAG_OBJECT);
	  melt_putfield_object (( /*_.INST__V7*/ meltfptr[6]), (0),
				( /*_.OPTSYMB__V2*/ meltfptr[1]),
				"OPTDESC_NAME");
	  ;
	  /*^putslot */
	  /*putslot */
	  melt_assertmsg ("putslot checkobj @OPTDESC_FUN",
			  melt_magic_discr ((melt_ptr_t)
					    ( /*_.INST__V7*/ meltfptr[6])) ==
			  MELTOBMAG_OBJECT);
	  melt_putfield_object (( /*_.INST__V7*/ meltfptr[6]), (1),
				( /*_.OPTFUN__V4*/ meltfptr[3]),
				"OPTDESC_FUN");
	  ;
	  /*^putslot */
	  /*putslot */
	  melt_assertmsg ("putslot checkobj @OPTDESC_HELP",
			  melt_magic_discr ((melt_ptr_t)
					    ( /*_.INST__V7*/ meltfptr[6])) ==
			  MELTOBMAG_OBJECT);
	  melt_putfield_object (( /*_.INST__V7*/ meltfptr[6]), (2),
				( /*_.OPTHELP__V3*/ meltfptr[2]),
				"OPTDESC_HELP");
	  ;
	  /*^touchobj */

	  melt_dbgtrace_written_object ( /*_.INST__V7*/ meltfptr[6],
					"newly made instance");
	  ;
	  /*_.OPTDESC__V6*/ meltfptr[5] = /*_.INST__V7*/ meltfptr[6];;

	  {
	    MELT_LOCATION ("warmelt-first.melt:3368:/ locexp");
	    meltgc_put_mapobjects ((meltmapobjects_ptr_t)
				   ( /*_.OPTMAP__V5*/ meltfptr[4]),
				   (meltobject_ptr_t) ( /*_.OPTSYMB__V2*/
						       meltfptr[1]),
				   (melt_ptr_t) ( /*_.OPTDESC__V6*/
						 meltfptr[5]));
	  }
	  ;

	  MELT_LOCATION ("warmelt-first.melt:3362:/ clear");
	     /*clear *//*_.OPTMAP__V5*/ meltfptr[4] = 0;
	  /*^clear */
	     /*clear *//*_.OPTDESC__V6*/ meltfptr[5] = 0;
	  /*epilog */
	}
	;
      }				/*noelse */
    ;
    /*epilog */

    MELT_LOCATION ("warmelt-first.melt:3356:/ clear");
	   /*clear *//*_#IS_A__L1*/ meltfnum[0] = 0;
    /*^clear */
	   /*clear *//*_#IF___L2*/ meltfnum[1] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("REGISTER_OPTION", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_36_warmelt_first_REGISTER_OPTION_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*noretval */ NULL);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_36_warmelt_first_REGISTER_OPTION */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_37_warmelt_first_OPTION_HELPER_FUN (meltclosure_ptr_t meltclosp_,
					     melt_ptr_t meltfirstargp_,
					     const melt_argdescr_cell_t
					     meltxargdescr_[],
					     union meltparam_un *
					     meltxargtab_,
					     const melt_argdescr_cell_t
					     meltxresdescr_[],
					     union meltparam_un *
					     meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_37_warmelt_first_OPTION_HELPER_FUN_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_37_warmelt_first_OPTION_HELPER_FUN_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 18
    melt_ptr_t mcfr_varptr[18];
#define MELTFRAM_NBVARNUM 2
    long mcfr_varnum[2];
/*others*/
    const char *loc_CSTRING__o0;
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_37_warmelt_first_OPTION_HELPER_FUN is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_37_warmelt_first_OPTION_HELPER_FUN_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 18; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_37_warmelt_first_OPTION_HELPER_FUN nbval 18*/
  meltfram__.mcfr_nbvar = 18 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("OPTION_HELPER_FUN", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-first.melt:3371:/ getarg");
 /*_.HELPSYMB__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_CSTRING)
    goto lab_endgetargs;
 /*_?*/ meltfram__.loc_CSTRING__o0 = meltxargtab_[0].meltbp_cstring;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {

    MELT_LOCATION ("warmelt-first.melt:3372:/ quasiblock");


    /*^getslot */
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj =
	(melt_ptr_t) (( /*!OPTION_MAP_CONTAINER */ meltfrout->
		       tabval[0])) /*=obj*/ ;
      melt_object_get_field (slot, obj, 0, "REFERENCED_VALUE");
  /*_.OPTMAP__V4*/ meltfptr[3] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-first.melt:3373:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^cond */
    /*cond */ if ( /*_?*/ meltfram__.loc_CSTRING__o0)	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

   /*_.GET_RAW_SYMBOL__V6*/ meltfptr[5] =
	    meltgc_named_symbol ((const char *) /*_?*/ meltfram__.
				 loc_CSTRING__o0, MELT_GET);;
	  /*^compute */
	  /*_.SYMB__V5*/ meltfptr[4] = /*_.GET_RAW_SYMBOL__V6*/ meltfptr[5];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-first.melt:3373:/ clear");
	     /*clear *//*_.GET_RAW_SYMBOL__V6*/ meltfptr[5] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

  /*_.SYMB__V5*/ meltfptr[4] = NULL;;
      }
    ;
    /*^compute */
 /*_.OPTD__V7*/ meltfptr[5] =
      /*mapobject_get */
      melt_get_mapobjects ((meltmapobjects_ptr_t)
			   ( /*_.OPTMAP__V4*/ meltfptr[3]),
			   (meltobject_ptr_t) ( /*_.SYMB__V5*/ meltfptr[4]));;
    MELT_LOCATION ("warmelt-first.melt:3377:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#IS_A__L1*/ meltfnum[0] =
      melt_is_instance_of ((melt_ptr_t) ( /*_.OPTD__V7*/ meltfptr[5]),
			   (melt_ptr_t) (( /*!CLASS_OPTION_DESCRIPTOR */
					  meltfrout->tabval[1])));;
    MELT_LOCATION ("warmelt-first.melt:3377:/ cond");
    /*cond */ if ( /*_#IS_A__L1*/ meltfnum[0])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-first.melt:3378:/ quasiblock");


	  /*^getslot */
	  {
	    melt_ptr_t slot = NULL, obj = NULL;
	    obj = (melt_ptr_t) ( /*_.OPTD__V7*/ meltfptr[5]) /*=obj*/ ;
	    melt_object_get_field (slot, obj, 2, "OPTDESC_HELP");
    /*_.OPTHELP__V10*/ meltfptr[9] = slot;
	  };
	  ;

	  {
	    MELT_LOCATION ("warmelt-first.melt:3381:/ locexp");
	    inform (UNKNOWN_LOCATION, "MELT help for option %s : %s",
		    /*_?*/ meltfram__.loc_CSTRING__o0,
		    melt_string_str ((melt_ptr_t) /*_.OPTHELP__V10*/
				     meltfptr[9]));
	  }
	  ;
	  MELT_LOCATION ("warmelt-first.melt:3384:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^quasiblock */


	  /*_.RETVAL___V1*/ meltfptr[0] = /*_.HELPSYMB__V2*/ meltfptr[1];;

	  {
	    MELT_LOCATION ("warmelt-first.melt:3384:/ locexp");
	    /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
	    if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	      melt_warn_for_no_expected_secondary_results ();
	    /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
	    ;
	  }
	  ;
	  /*^finalreturn */
	  ;
	  /*finalret */ goto labend_rout;
	  /*_.LET___V9*/ meltfptr[8] = /*_.RETURN___V11*/ meltfptr[10];;

	  MELT_LOCATION ("warmelt-first.melt:3378:/ clear");
	     /*clear *//*_.OPTHELP__V10*/ meltfptr[9] = 0;
	  /*^clear */
	     /*clear *//*_.RETURN___V11*/ meltfptr[10] = 0;
	  /*_.IFELSE___V8*/ meltfptr[7] = /*_.LET___V9*/ meltfptr[8];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-first.melt:3377:/ clear");
	     /*clear *//*_.LET___V9*/ meltfptr[8] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-first.melt:3387:/ quasiblock");


	  /*^checksignal */
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^apply */
	  /*apply */
	  {
	    /*_.SORTEDSYMBTUP__V13*/ meltfptr[10] =
	      melt_apply ((meltclosure_ptr_t)
			  (( /*!MAPOBJECT_SORTED_ATTRIBUTE_TUPLE */
			    meltfrout->tabval[2])),
			  (melt_ptr_t) ( /*_.OPTMAP__V4*/ meltfptr[3]), (""),
			  (union meltparam_un *) 0, "",
			  (union meltparam_un *) 0);
	  }
	  ;
	  MELT_LOCATION ("warmelt-first.melt:3389:/ quasiblock");


	  /*^newclosure */
		   /*newclosure *//*_.LAMBDA___V15*/ meltfptr[14] =
	    (melt_ptr_t)
	    meltgc_new_closure ((meltobject_ptr_t)
				(((melt_ptr_t)
				  (MELT_PREDEF (DISCR_CLOSURE)))),
				(meltroutine_ptr_t) (( /*!konst_5 */
						      meltfrout->tabval[5])),
				(0));
	  ;
	  /*_.LAMBDA___V14*/ meltfptr[8] = /*_.LAMBDA___V15*/ meltfptr[14];;
	  MELT_LOCATION ("warmelt-first.melt:3388:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^apply */
	  /*apply */
	  {
	    union meltparam_un argtab[1];
	    memset (&argtab, 0, sizeof (argtab));
	    /*^apply.arg */
	    argtab[0].meltbp_aptr =
	      (melt_ptr_t *) & /*_.LAMBDA___V14*/ meltfptr[8];
	    /*_.SORTEDNAMETUP__V16*/ meltfptr[15] =
	      melt_apply ((meltclosure_ptr_t)
			  (( /*!MULTIPLE_MAP */ meltfrout->tabval[3])),
			  (melt_ptr_t) ( /*_.SORTEDSYMBTUP__V13*/
					meltfptr[10]), (MELTBPARSTR_PTR ""),
			  argtab, "", (union meltparam_un *) 0);
	  }
	  ;
   /*_#NBSYMB__L2*/ meltfnum[1] =
	    (melt_multiple_length
	     ((melt_ptr_t) ( /*_.SORTEDSYMBTUP__V13*/ meltfptr[10])));;

	  {
	    MELT_LOCATION ("warmelt-first.melt:3395:/ locexp");
	    /* option_helper_fun INFORMOPTION__1 start */
	    {
	      int i = 0;
	      inform (UNKNOWN_LOCATION, "There are %d MELT options",
		      (int) /*_#NBSYMB__L2*/ meltfnum[1]);
	      for (i = 0; i < (int) /*_#NBSYMB__L2*/ meltfnum[1];
		   i += 2)
		{
		  const char *n1 =
		    melt_string_str ((melt_ptr_t)
				     melt_multiple_nth ((melt_ptr_t)
							/*_.SORTEDNAMETUP__V16*/
							meltfptr[15], i));
		  const char *n2 =
		    melt_string_str ((melt_ptr_t)
				     melt_multiple_nth ((melt_ptr_t)
							/*_.SORTEDNAMETUP__V16*/
							meltfptr[15], i + 1));
		  if (n1 && n2)
		    inform (UNKNOWN_LOCATION,
			    "possible MELT options: %s & %s", n1, n2);
		  else
		    inform (UNKNOWN_LOCATION, "possible MELT option: %s", n1);
		};
	      inform (UNKNOWN_LOCATION,
		      "Use -f[plugin-arg-]melt-option=help=X for help about MELT option X");
	    }			/* option_helper_fun INFORMOPTION__1 end */
	    ;
	  }
	  ;
	  MELT_LOCATION ("warmelt-first.melt:3411:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^quasiblock */


	  /*_.RETVAL___V1*/ meltfptr[0] = /*_.HELPSYMB__V2*/ meltfptr[1];;

	  {
	    MELT_LOCATION ("warmelt-first.melt:3411:/ locexp");
	    /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
	    if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	      melt_warn_for_no_expected_secondary_results ();
	    /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
	    ;
	  }
	  ;
	  /*^finalreturn */
	  ;
	  /*finalret */ goto labend_rout;
	  /*_.LET___V12*/ meltfptr[9] = /*_.RETURN___V17*/ meltfptr[16];;

	  MELT_LOCATION ("warmelt-first.melt:3387:/ clear");
	     /*clear *//*_.SORTEDSYMBTUP__V13*/ meltfptr[10] = 0;
	  /*^clear */
	     /*clear *//*_.LAMBDA___V14*/ meltfptr[8] = 0;
	  /*^clear */
	     /*clear *//*_.SORTEDNAMETUP__V16*/ meltfptr[15] = 0;
	  /*^clear */
	     /*clear *//*_#NBSYMB__L2*/ meltfnum[1] = 0;
	  /*^clear */
	     /*clear *//*_.RETURN___V17*/ meltfptr[16] = 0;
	  MELT_LOCATION ("warmelt-first.melt:3386:/ quasiblock");


	  /*_.PROGN___V18*/ meltfptr[10] = /*_.LET___V12*/ meltfptr[9];;
	  /*^compute */
	  /*_.IFELSE___V8*/ meltfptr[7] = /*_.PROGN___V18*/ meltfptr[10];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-first.melt:3377:/ clear");
	     /*clear *//*_.LET___V12*/ meltfptr[9] = 0;
	  /*^clear */
	     /*clear *//*_.PROGN___V18*/ meltfptr[10] = 0;
	}
	;
      }
    ;
    /*_.LET___V3*/ meltfptr[2] = /*_.IFELSE___V8*/ meltfptr[7];;

    MELT_LOCATION ("warmelt-first.melt:3372:/ clear");
	   /*clear *//*_.OPTMAP__V4*/ meltfptr[3] = 0;
    /*^clear */
	   /*clear *//*_.SYMB__V5*/ meltfptr[4] = 0;
    /*^clear */
	   /*clear *//*_.OPTD__V7*/ meltfptr[5] = 0;
    /*^clear */
	   /*clear *//*_#IS_A__L1*/ meltfnum[0] = 0;
    /*^clear */
	   /*clear *//*_.IFELSE___V8*/ meltfptr[7] = 0;
    MELT_LOCATION ("warmelt-first.melt:3371:/ quasiblock");


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.LET___V3*/ meltfptr[2];;

    {
      MELT_LOCATION ("warmelt-first.melt:3371:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.LET___V3*/ meltfptr[2] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("OPTION_HELPER_FUN", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_37_warmelt_first_OPTION_HELPER_FUN_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_37_warmelt_first_OPTION_HELPER_FUN */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_38_warmelt_first_LAMBDA___10__ (meltclosure_ptr_t meltclosp_,
					 melt_ptr_t meltfirstargp_,
					 const melt_argdescr_cell_t
					 meltxargdescr_[],
					 union meltparam_un * meltxargtab_,
					 const melt_argdescr_cell_t
					 meltxresdescr_[],
					 union meltparam_un * meltxrestab_)
{
  long current_blocklevel_signals_meltrout_38_warmelt_first_LAMBDA___10___melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_38_warmelt_first_LAMBDA___10___st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 3
    melt_ptr_t mcfr_varptr[3];
/*no varnum*/
#define MELTFRAM_NBVARNUM /*none*/0
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_38_warmelt_first_LAMBDA___10__ is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_38_warmelt_first_LAMBDA___10___st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 3; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_38_warmelt_first_LAMBDA___10__ nbval 3*/
  meltfram__.mcfr_nbvar = 3 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("LAMBDA_", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-first.melt:3389:/ getarg");
 /*_.SY__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {

    /*^cond */
    /*cond */ if (
		   /*ifisa */
		   melt_is_instance_of ((melt_ptr_t)
					( /*_.SY__V2*/ meltfptr[1]),
					(melt_ptr_t) (( /*!CLASS_NAMED */
						       meltfrout->tabval[0])))
      )				/*then */
      {
	/*^cond.then */
	/*^getslot */
	{
	  melt_ptr_t slot = NULL, obj = NULL;
	  obj = (melt_ptr_t) ( /*_.SY__V2*/ meltfptr[1]) /*=obj*/ ;
	  melt_object_get_field (slot, obj, 1, "NAMED_NAME");
   /*_.NAMED_NAME__V3*/ meltfptr[2] = slot;
	};
	;
      }
    else
      {				/*^cond.else */

  /*_.NAMED_NAME__V3*/ meltfptr[2] = NULL;;
      }
    ;
    MELT_LOCATION ("warmelt-first.melt:3389:/ quasiblock");


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.NAMED_NAME__V3*/ meltfptr[2];;

    {
      MELT_LOCATION ("warmelt-first.melt:3389:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.NAMED_NAME__V3*/ meltfptr[2] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("LAMBDA_", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_38_warmelt_first_LAMBDA___10___melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_38_warmelt_first_LAMBDA___10__ */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_39_warmelt_first_MAPOBJECT_EVERY (meltclosure_ptr_t meltclosp_,
					   melt_ptr_t meltfirstargp_,
					   const melt_argdescr_cell_t
					   meltxargdescr_[],
					   union meltparam_un * meltxargtab_,
					   const melt_argdescr_cell_t
					   meltxresdescr_[],
					   union meltparam_un * meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_39_warmelt_first_MAPOBJECT_EVERY_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_39_warmelt_first_MAPOBJECT_EVERY_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 6
    melt_ptr_t mcfr_varptr[6];
#define MELTFRAM_NBVARNUM 2
    long mcfr_varnum[2];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_39_warmelt_first_MAPOBJECT_EVERY is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_39_warmelt_first_MAPOBJECT_EVERY_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 6; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_39_warmelt_first_MAPOBJECT_EVERY nbval 6*/
  meltfram__.mcfr_nbvar = 6 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("MAPOBJECT_EVERY", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-first.melt:3471:/ getarg");
 /*_.MAP__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.F__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.F__V3*/ meltfptr[2])) != NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {

    MELT_LOCATION ("warmelt-first.melt:3475:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#IS_MAPOBJECT__L1*/ meltfnum[0] =
      /*is_mapobject: */
      (melt_magic_discr ((melt_ptr_t) ( /*_.MAP__V2*/ meltfptr[1])) ==
       MELTOBMAG_MAPOBJECTS);;
    MELT_LOCATION ("warmelt-first.melt:3475:/ cond");
    /*cond */ if ( /*_#IS_MAPOBJECT__L1*/ meltfnum[0])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-first.melt:3476:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
   /*_#IS_CLOSURE__L2*/ meltfnum[1] =
	    (melt_magic_discr ((melt_ptr_t) ( /*_.F__V3*/ meltfptr[2])) ==
	     MELTOBMAG_CLOSURE);;
	  MELT_LOCATION ("warmelt-first.melt:3476:/ cond");
	  /*cond */ if ( /*_#IS_CLOSURE__L2*/ meltfnum[1])	/*then */
	    {
	      /*^cond.then */
	      /*^block */
	      /*anyblock */
	      {

		/*citerblock FOREACH_IN_MAPOBJECT */
		{
		  /* foreach_in_mapobject meltcit1__EACHOBMAP : */ int
		    meltcit1__EACHOBMAP_ix = 0, meltcit1__EACHOBMAP_siz = 0;
		  for (meltcit1__EACHOBMAP_ix = 0;
		       /* we retrieve in meltcit1__EACHOBMAP_siz the size at each iteration since it could change. */
		       meltcit1__EACHOBMAP_ix >= 0
		       && (meltcit1__EACHOBMAP_siz =
			   melt_size_mapobjects ((meltmapobjects_ptr_t)
						 /*_.MAP__V2*/ meltfptr[1])) >
		       0
		       && meltcit1__EACHOBMAP_ix < meltcit1__EACHOBMAP_siz;
		       meltcit1__EACHOBMAP_ix++)
		    {
    /*_.CURAT__V4*/ meltfptr[3] = NULL;
    /*_.CURVAL__V5*/ meltfptr[4] = NULL;
		      /*_.CURAT__V4*/ meltfptr[3] =
			(melt_ptr_t) (((meltmapobjects_ptr_t) /*_.MAP__V2*/
				       meltfptr[1])->
				      entab[meltcit1__EACHOBMAP_ix].e_at);
		      if ( /*_.CURAT__V4*/ meltfptr[3] == HTAB_DELETED_ENTRY)
			{				     /*_.CURAT__V4*/
			  meltfptr[3] = NULL;
			  continue;
			};
		      if (! /*_.CURAT__V4*/ meltfptr[3])
			continue;
		      /*_.CURVAL__V5*/ meltfptr[4] =
			((meltmapobjects_ptr_t) /*_.MAP__V2*/ meltfptr[1])->
			entab[meltcit1__EACHOBMAP_ix].e_va;
		      if (! /*_.CURVAL__V5*/ meltfptr[4])
			continue;



		      MELT_LOCATION ("warmelt-first.melt:3480:/ checksignal");
		      MELT_CHECK_SIGNAL ();
		      ;
		      /*^apply */
		      /*apply */
		      {
			union meltparam_un argtab[1];
			memset (&argtab, 0, sizeof (argtab));
			/*^apply.arg */
			argtab[0].meltbp_aptr =
			  (melt_ptr_t *) & /*_.CURVAL__V5*/ meltfptr[4];
			/*_.F__V6*/ meltfptr[5] =
			  melt_apply ((meltclosure_ptr_t)
				      ( /*_.F__V3*/ meltfptr[2]),
				      (melt_ptr_t) ( /*_.CURAT__V4*/
						    meltfptr[3]),
				      (MELTBPARSTR_PTR ""), argtab, "",
				      (union meltparam_un *) 0);
		      }
		      ;
		      /* foreach_in_mapobject end meltcit1__EACHOBMAP */
    /*_.CURAT__V4*/ meltfptr[3] = NULL;
    /*_.CURVAL__V5*/ meltfptr[4] = NULL;
		    }


		  /*citerepilog */

		  MELT_LOCATION ("warmelt-first.melt:3477:/ clear");
		/*clear *//*_.CURAT__V4*/ meltfptr[3] = 0;
		  /*^clear */
		/*clear *//*_.CURVAL__V5*/ meltfptr[4] = 0;
		  /*^clear */
		/*clear *//*_.F__V6*/ meltfptr[5] = 0;
		}		/*endciterblock FOREACH_IN_MAPOBJECT */
		;
		/*epilog */
	      }
	      ;
	    }			/*noelse */
	  ;
	  /*epilog */

	  MELT_LOCATION ("warmelt-first.melt:3475:/ clear");
	     /*clear *//*_#IS_CLOSURE__L2*/ meltfnum[1] = 0;
	}
	;
      }				/*noelse */
    ;
    /*epilog */

    MELT_LOCATION ("warmelt-first.melt:3471:/ clear");
	   /*clear *//*_#IS_MAPOBJECT__L1*/ meltfnum[0] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("MAPOBJECT_EVERY", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_39_warmelt_first_MAPOBJECT_EVERY_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*noretval */ NULL);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_39_warmelt_first_MAPOBJECT_EVERY */



/**** end of warmelt-first+01.c ****/
