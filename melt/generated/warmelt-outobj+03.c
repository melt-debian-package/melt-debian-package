/* GCC MELT GENERATED FILE warmelt-outobj+03.c - DO NOT EDIT */
/* secondary MELT generated C file of rank #3 */
#include "melt-run.h"


/* used hash from melt-run.h when compiling this file: */
MELT_EXTERN const char meltrun_used_md5_melt_f3[] =
  MELT_RUN_HASHMD5 /* from melt-run.h */ ;


/**** warmelt-outobj+03.c declarations ****/


/***************************************************
***
    Copyright (C) 2008 - 2012 Free Software Foundation, Inc.
    Contributed by Basile Starynkevitch <basile@starynkevitch.net>

    This file is part of GCC.

    GCC is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3, or (at your option)
    any later version.

    GCC is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with GCC; see the file COPYING3.  If not see
    <http://www.gnu.org/licenses/>.
***
****************************************************/

/* ordinary MELT module */
#define MELT_HAS_INITIAL_ENVIRONMENT 1	/*usual */

struct melt_callframe_st;	/*defined in melt-runtime.h */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_1_warmelt_outobj_GET_CODE_BUFFER_LIMIT (meltclosure_ptr_t meltclosp_,
						 melt_ptr_t meltfirstargp_,
						 const melt_argdescr_cell_t
						 meltxargdescr_[],
						 union meltparam_un
						 *meltxargtab_,
						 const melt_argdescr_cell_t
						 meltxresdescr_[],
						 union meltparam_un
						 *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_2_warmelt_outobj_PUT_CODE_BUFFER_LIMIT (meltclosure_ptr_t meltclosp_,
						 melt_ptr_t meltfirstargp_,
						 const melt_argdescr_cell_t
						 meltxargdescr_[],
						 union meltparam_un
						 *meltxargtab_,
						 const melt_argdescr_cell_t
						 meltxresdescr_[],
						 union meltparam_un
						 *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_3_warmelt_outobj_CODE_BUFFER_LIMIT_OPTSET (meltclosure_ptr_t
						    meltclosp_,
						    melt_ptr_t meltfirstargp_,
						    const melt_argdescr_cell_t
						    meltxargdescr_[],
						    union meltparam_un
						    *meltxargtab_,
						    const melt_argdescr_cell_t
						    meltxresdescr_[],
						    union meltparam_un
						    *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_4_warmelt_outobj_OUTDECLINIT_ROOT (meltclosure_ptr_t meltclosp_,
					    melt_ptr_t meltfirstargp_,
					    const melt_argdescr_cell_t
					    meltxargdescr_[],
					    union meltparam_un *meltxargtab_,
					    const melt_argdescr_cell_t
					    meltxresdescr_[],
					    union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_5_warmelt_outobj_OUTPUCOD_OBJINIELEM (meltclosure_ptr_t meltclosp_,
					       melt_ptr_t meltfirstargp_,
					       const melt_argdescr_cell_t
					       meltxargdescr_[],
					       union meltparam_un
					       *meltxargtab_,
					       const melt_argdescr_cell_t
					       meltxresdescr_[],
					       union meltparam_un
					       *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_6_warmelt_outobj_OUTCINITFILL_ROOT (meltclosure_ptr_t meltclosp_,
					     melt_ptr_t meltfirstargp_,
					     const melt_argdescr_cell_t
					     meltxargdescr_[],
					     union meltparam_un *meltxargtab_,
					     const melt_argdescr_cell_t
					     meltxresdescr_[],
					     union meltparam_un
					     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_7_warmelt_outobj_OUTCINITPREDEF_ROOT (meltclosure_ptr_t meltclosp_,
					       melt_ptr_t meltfirstargp_,
					       const melt_argdescr_cell_t
					       meltxargdescr_[],
					       union meltparam_un
					       *meltxargtab_,
					       const melt_argdescr_cell_t
					       meltxresdescr_[],
					       union meltparam_un
					       *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_8_warmelt_outobj_OUTPUT_PREDEF (meltclosure_ptr_t meltclosp_,
					 melt_ptr_t meltfirstargp_,
					 const melt_argdescr_cell_t
					 meltxargdescr_[],
					 union meltparam_un *meltxargtab_,
					 const melt_argdescr_cell_t
					 meltxresdescr_[],
					 union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_9_warmelt_outobj_OUTPUCOD_PREDEF (meltclosure_ptr_t meltclosp_,
					   melt_ptr_t meltfirstargp_,
					   const melt_argdescr_cell_t
					   meltxargdescr_[],
					   union meltparam_un *meltxargtab_,
					   const melt_argdescr_cell_t
					   meltxresdescr_[],
					   union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_10_warmelt_outobj_OUTPUCOD_NIL (meltclosure_ptr_t meltclosp_,
					 melt_ptr_t meltfirstargp_,
					 const melt_argdescr_cell_t
					 meltxargdescr_[],
					 union meltparam_un *meltxargtab_,
					 const melt_argdescr_cell_t
					 meltxresdescr_[],
					 union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_11_warmelt_outobj_OUTPUCOD_OBJEXPANDPUREVAL (meltclosure_ptr_t
						      meltclosp_,
						      melt_ptr_t
						      meltfirstargp_,
						      const
						      melt_argdescr_cell_t
						      meltxargdescr_[],
						      union meltparam_un
						      *meltxargtab_,
						      const
						      melt_argdescr_cell_t
						      meltxresdescr_[],
						      union meltparam_un
						      *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_12_warmelt_outobj_OUTDECLINIT_OBJINITOBJECT (meltclosure_ptr_t
						      meltclosp_,
						      melt_ptr_t
						      meltfirstargp_,
						      const
						      melt_argdescr_cell_t
						      meltxargdescr_[],
						      union meltparam_un
						      *meltxargtab_,
						      const
						      melt_argdescr_cell_t
						      meltxresdescr_[],
						      union meltparam_un
						      *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_13_warmelt_outobj_OUTCINITFILL_OBJINITOBJECT (meltclosure_ptr_t
						       meltclosp_,
						       melt_ptr_t
						       meltfirstargp_,
						       const
						       melt_argdescr_cell_t
						       meltxargdescr_[],
						       union meltparam_un
						       *meltxargtab_,
						       const
						       melt_argdescr_cell_t
						       meltxresdescr_[],
						       union meltparam_un
						       *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_14_warmelt_outobj_OUTCINITPREDEF_OBJINITOBJECT (meltclosure_ptr_t
							 meltclosp_,
							 melt_ptr_t
							 meltfirstargp_,
							 const
							 melt_argdescr_cell_t
							 meltxargdescr_[],
							 union meltparam_un
							 *meltxargtab_,
							 const
							 melt_argdescr_cell_t
							 meltxresdescr_[],
							 union meltparam_un
							 *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_15_warmelt_outobj_OUTDECLINIT_OBJINITMULTIPLE (meltclosure_ptr_t
							meltclosp_,
							melt_ptr_t
							meltfirstargp_,
							const
							melt_argdescr_cell_t
							meltxargdescr_[],
							union meltparam_un
							*meltxargtab_,
							const
							melt_argdescr_cell_t
							meltxresdescr_[],
							union meltparam_un
							*meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_16_warmelt_outobj_OUTCINITFILL_OBJINITMULTIPLE (meltclosure_ptr_t
							 meltclosp_,
							 melt_ptr_t
							 meltfirstargp_,
							 const
							 melt_argdescr_cell_t
							 meltxargdescr_[],
							 union meltparam_un
							 *meltxargtab_,
							 const
							 melt_argdescr_cell_t
							 meltxresdescr_[],
							 union meltparam_un
							 *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_17_warmelt_outobj_OUTDECLINIT_OBJINITCLOSURE (meltclosure_ptr_t
						       meltclosp_,
						       melt_ptr_t
						       meltfirstargp_,
						       const
						       melt_argdescr_cell_t
						       meltxargdescr_[],
						       union meltparam_un
						       *meltxargtab_,
						       const
						       melt_argdescr_cell_t
						       meltxresdescr_[],
						       union meltparam_un
						       *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_18_warmelt_outobj_OUTCINITFILL_OBJINITCLOSURE (meltclosure_ptr_t
							meltclosp_,
							melt_ptr_t
							meltfirstargp_,
							const
							melt_argdescr_cell_t
							meltxargdescr_[],
							union meltparam_un
							*meltxargtab_,
							const
							melt_argdescr_cell_t
							meltxresdescr_[],
							union meltparam_un
							*meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_19_warmelt_outobj_OUTDECLINIT_OBJINITROUTINE (meltclosure_ptr_t
						       meltclosp_,
						       melt_ptr_t
						       meltfirstargp_,
						       const
						       melt_argdescr_cell_t
						       meltxargdescr_[],
						       union meltparam_un
						       *meltxargtab_,
						       const
						       melt_argdescr_cell_t
						       meltxresdescr_[],
						       union meltparam_un
						       *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_20_warmelt_outobj_OUTCINITFILL_OBJINITROUTINE (meltclosure_ptr_t
							meltclosp_,
							melt_ptr_t
							meltfirstargp_,
							const
							melt_argdescr_cell_t
							meltxargdescr_[],
							union meltparam_un
							*meltxargtab_,
							const
							melt_argdescr_cell_t
							meltxresdescr_[],
							union meltparam_un
							*meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_21_warmelt_outobj_OUTDECLINIT_OBJINITSTRING (meltclosure_ptr_t
						      meltclosp_,
						      melt_ptr_t
						      meltfirstargp_,
						      const
						      melt_argdescr_cell_t
						      meltxargdescr_[],
						      union meltparam_un
						      *meltxargtab_,
						      const
						      melt_argdescr_cell_t
						      meltxresdescr_[],
						      union meltparam_un
						      *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_22_warmelt_outobj_OUTCINITFILL_OBJINITSTRING (meltclosure_ptr_t
						       meltclosp_,
						       melt_ptr_t
						       meltfirstargp_,
						       const
						       melt_argdescr_cell_t
						       meltxargdescr_[],
						       union meltparam_un
						       *meltxargtab_,
						       const
						       melt_argdescr_cell_t
						       meltxresdescr_[],
						       union meltparam_un
						       *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_23_warmelt_outobj_OUTDECLINIT_OBJINITBOXEDINTEGER (meltclosure_ptr_t
							    meltclosp_,
							    melt_ptr_t
							    meltfirstargp_,
							    const
							    melt_argdescr_cell_t
							    meltxargdescr_[],
							    union meltparam_un
							    *meltxargtab_,
							    const
							    melt_argdescr_cell_t
							    meltxresdescr_[],
							    union meltparam_un
							    *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_24_warmelt_outobj_OUTCINITFILL_OBJINITBOXEDINTEGER (meltclosure_ptr_t
							     meltclosp_,
							     melt_ptr_t
							     meltfirstargp_,
							     const
							     melt_argdescr_cell_t
							     meltxargdescr_[],
							     union
							     meltparam_un
							     *meltxargtab_,
							     const
							     melt_argdescr_cell_t
							     meltxresdescr_[],
							     union
							     meltparam_un
							     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_25_warmelt_outobj_OUTDECLINIT_OBJINITPAIR (meltclosure_ptr_t
						    meltclosp_,
						    melt_ptr_t meltfirstargp_,
						    const melt_argdescr_cell_t
						    meltxargdescr_[],
						    union meltparam_un
						    *meltxargtab_,
						    const melt_argdescr_cell_t
						    meltxresdescr_[],
						    union meltparam_un
						    *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_26_warmelt_outobj_OUTCINITFILL_OBJINITPAIR (meltclosure_ptr_t
						     meltclosp_,
						     melt_ptr_t
						     meltfirstargp_,
						     const
						     melt_argdescr_cell_t
						     meltxargdescr_[],
						     union meltparam_un
						     *meltxargtab_,
						     const
						     melt_argdescr_cell_t
						     meltxresdescr_[],
						     union meltparam_un
						     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_27_warmelt_outobj_OUTDECLINIT_OBJINITLIST (meltclosure_ptr_t
						    meltclosp_,
						    melt_ptr_t meltfirstargp_,
						    const melt_argdescr_cell_t
						    meltxargdescr_[],
						    union meltparam_un
						    *meltxargtab_,
						    const melt_argdescr_cell_t
						    meltxresdescr_[],
						    union meltparam_un
						    *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_28_warmelt_outobj_OUTCINITFILL_OBJINITLIST (meltclosure_ptr_t
						     meltclosp_,
						     melt_ptr_t
						     meltfirstargp_,
						     const
						     melt_argdescr_cell_t
						     meltxargdescr_[],
						     union meltparam_un
						     *meltxargtab_,
						     const
						     melt_argdescr_cell_t
						     meltxresdescr_[],
						     union meltparam_un
						     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_29_warmelt_outobj_OUTPUCOD_ANYDISCR (meltclosure_ptr_t meltclosp_,
					      melt_ptr_t meltfirstargp_,
					      const melt_argdescr_cell_t
					      meltxargdescr_[],
					      union meltparam_un
					      *meltxargtab_,
					      const melt_argdescr_cell_t
					      meltxresdescr_[],
					      union meltparam_un
					      *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_30_warmelt_outobj_OUTPUCOD_NULL (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_31_warmelt_outobj_OUTPUCOD_CATCHALL_ROOT (meltclosure_ptr_t
						   meltclosp_,
						   melt_ptr_t meltfirstargp_,
						   const melt_argdescr_cell_t
						   meltxargdescr_[],
						   union meltparam_un
						   *meltxargtab_,
						   const melt_argdescr_cell_t
						   meltxresdescr_[],
						   union meltparam_un
						   *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_32_warmelt_outobj_OUTPUT_RAW_LOCATION (meltclosure_ptr_t meltclosp_,
						melt_ptr_t meltfirstargp_,
						const melt_argdescr_cell_t
						meltxargdescr_[],
						union meltparam_un
						*meltxargtab_,
						const melt_argdescr_cell_t
						meltxresdescr_[],
						union meltparam_un
						*meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_33_warmelt_outobj_LINE_AND_FILE_OF_LOCATION (meltclosure_ptr_t
						      meltclosp_,
						      melt_ptr_t
						      meltfirstargp_,
						      const
						      melt_argdescr_cell_t
						      meltxargdescr_[],
						      union meltparam_un
						      *meltxargtab_,
						      const
						      melt_argdescr_cell_t
						      meltxresdescr_[],
						      union meltparam_un
						      *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_34_warmelt_outobj_OUTPUT_LOCATION (meltclosure_ptr_t meltclosp_,
					    melt_ptr_t meltfirstargp_,
					    const melt_argdescr_cell_t
					    meltxargdescr_[],
					    union meltparam_un *meltxargtab_,
					    const melt_argdescr_cell_t
					    meltxresdescr_[],
					    union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_35_warmelt_outobj_OUTPUT_CURFRAME_DECLSTRUCT (meltclosure_ptr_t
						       meltclosp_,
						       melt_ptr_t
						       meltfirstargp_,
						       const
						       melt_argdescr_cell_t
						       meltxargdescr_[],
						       union meltparam_un
						       *meltxargtab_,
						       const
						       melt_argdescr_cell_t
						       meltxresdescr_[],
						       union meltparam_un
						       *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_36_warmelt_outobj_LAMBDA___1__ (meltclosure_ptr_t meltclosp_,
					 melt_ptr_t meltfirstargp_,
					 const melt_argdescr_cell_t
					 meltxargdescr_[],
					 union meltparam_un *meltxargtab_,
					 const melt_argdescr_cell_t
					 meltxresdescr_[],
					 union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_37_warmelt_outobj_OUTPUCOD_MARKER (meltclosure_ptr_t meltclosp_,
					    melt_ptr_t meltfirstargp_,
					    const melt_argdescr_cell_t
					    meltxargdescr_[],
					    union meltparam_un *meltxargtab_,
					    const melt_argdescr_cell_t
					    meltxresdescr_[],
					    union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_38_warmelt_outobj_LAMBDA___2__ (meltclosure_ptr_t meltclosp_,
					 melt_ptr_t meltfirstargp_,
					 const melt_argdescr_cell_t
					 meltxargdescr_[],
					 union meltparam_un *meltxargtab_,
					 const melt_argdescr_cell_t
					 meltxresdescr_[],
					 union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_39_warmelt_outobj_OUTPUT_CURFRAME_DECLSTRUCT_INIT (meltclosure_ptr_t
							    meltclosp_,
							    melt_ptr_t
							    meltfirstargp_,
							    const
							    melt_argdescr_cell_t
							    meltxargdescr_[],
							    union meltparam_un
							    *meltxargtab_,
							    const
							    melt_argdescr_cell_t
							    meltxresdescr_[],
							    union meltparam_un
							    *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_40_warmelt_outobj_OUTPUCOD_PROCROUTINE (meltclosure_ptr_t meltclosp_,
						 melt_ptr_t meltfirstargp_,
						 const melt_argdescr_cell_t
						 meltxargdescr_[],
						 union meltparam_un
						 *meltxargtab_,
						 const melt_argdescr_cell_t
						 meltxresdescr_[],
						 union meltparam_un
						 *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_41_warmelt_outobj_LAMBDA___3__ (meltclosure_ptr_t meltclosp_,
					 melt_ptr_t meltfirstargp_,
					 const melt_argdescr_cell_t
					 meltxargdescr_[],
					 union meltparam_un *meltxargtab_,
					 const melt_argdescr_cell_t
					 meltxresdescr_[],
					 union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_42_warmelt_outobj_OUTPUT_CURFRAME_CDAT_STRUCT (meltclosure_ptr_t
							meltclosp_,
							melt_ptr_t
							meltfirstargp_,
							const
							melt_argdescr_cell_t
							meltxargdescr_[],
							union meltparam_un
							*meltxargtab_,
							const
							melt_argdescr_cell_t
							meltxresdescr_[],
							union meltparam_un
							*meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_43_warmelt_outobj_OUTPUT_CURFRAME_CDAT_FILL (meltclosure_ptr_t
						      meltclosp_,
						      melt_ptr_t
						      meltfirstargp_,
						      const
						      melt_argdescr_cell_t
						      meltxargdescr_[],
						      union meltparam_un
						      *meltxargtab_,
						      const
						      melt_argdescr_cell_t
						      meltxresdescr_[],
						      union meltparam_un
						      *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_44_warmelt_outobj_OUTPUCOD_INITIALMODULEROUTINE (meltclosure_ptr_t
							  meltclosp_,
							  melt_ptr_t
							  meltfirstargp_,
							  const
							  melt_argdescr_cell_t
							  meltxargdescr_[],
							  union meltparam_un
							  *meltxargtab_,
							  const
							  melt_argdescr_cell_t
							  meltxresdescr_[],
							  union meltparam_un
							  *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_45_warmelt_outobj_LAMBDA___4__ (meltclosure_ptr_t meltclosp_,
					 melt_ptr_t meltfirstargp_,
					 const melt_argdescr_cell_t
					 meltxargdescr_[],
					 union meltparam_un *meltxargtab_,
					 const melt_argdescr_cell_t
					 meltxresdescr_[],
					 union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_46_warmelt_outobj_OUTPUCOD_INITIALEXTENSIONROUTINE (meltclosure_ptr_t
							     meltclosp_,
							     melt_ptr_t
							     meltfirstargp_,
							     const
							     melt_argdescr_cell_t
							     meltxargdescr_[],
							     union
							     meltparam_un
							     *meltxargtab_,
							     const
							     melt_argdescr_cell_t
							     meltxresdescr_[],
							     union
							     meltparam_un
							     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_47_warmelt_outobj_LAMBDA___5__ (meltclosure_ptr_t meltclosp_,
					 melt_ptr_t meltfirstargp_,
					 const melt_argdescr_cell_t
					 meltxargdescr_[],
					 union meltparam_un *meltxargtab_,
					 const melt_argdescr_cell_t
					 meltxresdescr_[],
					 union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_48_warmelt_outobj_OUTPUCOD_GETARG (meltclosure_ptr_t meltclosp_,
					    melt_ptr_t meltfirstargp_,
					    const melt_argdescr_cell_t
					    meltxargdescr_[],
					    union meltparam_un *meltxargtab_,
					    const melt_argdescr_cell_t
					    meltxresdescr_[],
					    union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_49_warmelt_outobj_OUTPUCOD_GETARGREST (meltclosure_ptr_t meltclosp_,
						melt_ptr_t meltfirstargp_,
						const melt_argdescr_cell_t
						meltxargdescr_[],
						union meltparam_un
						*meltxargtab_,
						const melt_argdescr_cell_t
						meltxresdescr_[],
						union meltparam_un
						*meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_50_warmelt_outobj_OUTPUCOD_OBJLOCV (meltclosure_ptr_t meltclosp_,
					     melt_ptr_t meltfirstargp_,
					     const melt_argdescr_cell_t
					     meltxargdescr_[],
					     union meltparam_un *meltxargtab_,
					     const melt_argdescr_cell_t
					     meltxresdescr_[],
					     union meltparam_un
					     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_51_warmelt_outobj_OUTPUCOD_OBJCLOCCV (meltclosure_ptr_t meltclosp_,
					       melt_ptr_t meltfirstargp_,
					       const melt_argdescr_cell_t
					       meltxargdescr_[],
					       union meltparam_un
					       *meltxargtab_,
					       const melt_argdescr_cell_t
					       meltxresdescr_[],
					       union meltparam_un
					       *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_52_warmelt_outobj_OUTPUCOD_OBJCONSTV (meltclosure_ptr_t meltclosp_,
					       melt_ptr_t meltfirstargp_,
					       const melt_argdescr_cell_t
					       meltxargdescr_[],
					       union meltparam_un
					       *meltxargtab_,
					       const melt_argdescr_cell_t
					       meltxresdescr_[],
					       union meltparam_un
					       *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_53_warmelt_outobj_OUTPUT_CODE_INSTRUCTIONS_LIST (meltclosure_ptr_t
							  meltclosp_,
							  melt_ptr_t
							  meltfirstargp_,
							  const
							  melt_argdescr_cell_t
							  meltxargdescr_[],
							  union meltparam_un
							  *meltxargtab_,
							  const
							  melt_argdescr_cell_t
							  meltxresdescr_[],
							  union meltparam_un
							  *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_54_warmelt_outobj_OUTPUCOD_OBJCHECKSIGNAL (meltclosure_ptr_t
						    meltclosp_,
						    melt_ptr_t meltfirstargp_,
						    const melt_argdescr_cell_t
						    meltxargdescr_[],
						    union meltparam_un
						    *meltxargtab_,
						    const melt_argdescr_cell_t
						    meltxresdescr_[],
						    union meltparam_un
						    *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_55_warmelt_outobj_OUTPUCOD_OBJANYBLOCK (meltclosure_ptr_t meltclosp_,
						 melt_ptr_t meltfirstargp_,
						 const melt_argdescr_cell_t
						 meltxargdescr_[],
						 union meltparam_un
						 *meltxargtab_,
						 const melt_argdescr_cell_t
						 meltxresdescr_[],
						 union meltparam_un
						 *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_56_warmelt_outobj_OUTPUCOD_OBJMULTIALLOCBLOCK (meltclosure_ptr_t
							meltclosp_,
							melt_ptr_t
							meltfirstargp_,
							const
							melt_argdescr_cell_t
							meltxargdescr_[],
							union meltparam_un
							*meltxargtab_,
							const
							melt_argdescr_cell_t
							meltxresdescr_[],
							union meltparam_un
							*meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_57_warmelt_outobj_OUTPUCOD_OBJCITERBLOCK (meltclosure_ptr_t
						   meltclosp_,
						   melt_ptr_t meltfirstargp_,
						   const melt_argdescr_cell_t
						   meltxargdescr_[],
						   union meltparam_un
						   *meltxargtab_,
						   const melt_argdescr_cell_t
						   meltxresdescr_[],
						   union meltparam_un
						   *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_58_warmelt_outobj_OUTPUCOD_OBJCOMMENTINSTR (meltclosure_ptr_t
						     meltclosp_,
						     melt_ptr_t
						     meltfirstargp_,
						     const
						     melt_argdescr_cell_t
						     meltxargdescr_[],
						     union meltparam_un
						     *meltxargtab_,
						     const
						     melt_argdescr_cell_t
						     meltxresdescr_[],
						     union meltparam_un
						     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_59_warmelt_outobj_OUTPUCOD_OBJCOMMENTEDBLOCK (meltclosure_ptr_t
						       meltclosp_,
						       melt_ptr_t
						       meltfirstargp_,
						       const
						       melt_argdescr_cell_t
						       meltxargdescr_[],
						       union meltparam_un
						       *meltxargtab_,
						       const
						       melt_argdescr_cell_t
						       meltxresdescr_[],
						       union meltparam_un
						       *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_60_warmelt_outobj_OUTPUCOD_OBJLABELINSTR (meltclosure_ptr_t
						   meltclosp_,
						   melt_ptr_t meltfirstargp_,
						   const melt_argdescr_cell_t
						   meltxargdescr_[],
						   union meltparam_un
						   *meltxargtab_,
						   const melt_argdescr_cell_t
						   meltxresdescr_[],
						   union meltparam_un
						   *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_61_warmelt_outobj_OUTPUCOD_OBJGOTOINSTR (meltclosure_ptr_t
						  meltclosp_,
						  melt_ptr_t meltfirstargp_,
						  const melt_argdescr_cell_t
						  meltxargdescr_[],
						  union meltparam_un
						  *meltxargtab_,
						  const melt_argdescr_cell_t
						  meltxresdescr_[],
						  union meltparam_un
						  *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_62_warmelt_outobj_ADD2SBUF_CLONSYM (meltclosure_ptr_t meltclosp_,
					     melt_ptr_t meltfirstargp_,
					     const melt_argdescr_cell_t
					     meltxargdescr_[],
					     union meltparam_un *meltxargtab_,
					     const melt_argdescr_cell_t
					     meltxresdescr_[],
					     union meltparam_un
					     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_63_warmelt_outobj_OUTPUCOD_OBJLOOP (meltclosure_ptr_t meltclosp_,
					     melt_ptr_t meltfirstargp_,
					     const melt_argdescr_cell_t
					     meltxargdescr_[],
					     union meltparam_un *meltxargtab_,
					     const melt_argdescr_cell_t
					     meltxresdescr_[],
					     union meltparam_un
					     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_64_warmelt_outobj_LAMBDA___6__ (meltclosure_ptr_t meltclosp_,
					 melt_ptr_t meltfirstargp_,
					 const melt_argdescr_cell_t
					 meltxargdescr_[],
					 union meltparam_un *meltxargtab_,
					 const melt_argdescr_cell_t
					 meltxresdescr_[],
					 union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_65_warmelt_outobj_LAMBDA___7__ (meltclosure_ptr_t meltclosp_,
					 melt_ptr_t meltfirstargp_,
					 const melt_argdescr_cell_t
					 meltxargdescr_[],
					 union meltparam_un *meltxargtab_,
					 const melt_argdescr_cell_t
					 meltxresdescr_[],
					 union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_66_warmelt_outobj_OUTPUCOD_OBJEXIT (meltclosure_ptr_t meltclosp_,
					     melt_ptr_t meltfirstargp_,
					     const melt_argdescr_cell_t
					     meltxargdescr_[],
					     union meltparam_un *meltxargtab_,
					     const melt_argdescr_cell_t
					     meltxresdescr_[],
					     union meltparam_un
					     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_67_warmelt_outobj_OUTPUCOD_OBJAGAIN (meltclosure_ptr_t meltclosp_,
					      melt_ptr_t meltfirstargp_,
					      const melt_argdescr_cell_t
					      meltxargdescr_[],
					      union meltparam_un
					      *meltxargtab_,
					      const melt_argdescr_cell_t
					      meltxresdescr_[],
					      union meltparam_un
					      *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_68_warmelt_outobj_OUTPUCOD_OBJCOMPUTE (meltclosure_ptr_t meltclosp_,
						melt_ptr_t meltfirstargp_,
						const melt_argdescr_cell_t
						meltxargdescr_[],
						union meltparam_un
						*meltxargtab_,
						const melt_argdescr_cell_t
						meltxresdescr_[],
						union meltparam_un
						*meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_69_warmelt_outobj_LAMBDA___8__ (meltclosure_ptr_t meltclosp_,
					 melt_ptr_t meltfirstargp_,
					 const melt_argdescr_cell_t
					 meltxargdescr_[],
					 union meltparam_un *meltxargtab_,
					 const melt_argdescr_cell_t
					 meltxresdescr_[],
					 union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_70_warmelt_outobj_LAMBDA___9__ (meltclosure_ptr_t meltclosp_,
					 melt_ptr_t meltfirstargp_,
					 const melt_argdescr_cell_t
					 meltxargdescr_[],
					 union meltparam_un *meltxargtab_,
					 const melt_argdescr_cell_t
					 meltxresdescr_[],
					 union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_71_warmelt_outobj_LAMBDA___10__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_72_warmelt_outobj_OUTPUCOD_OBJCOND (meltclosure_ptr_t meltclosp_,
					     melt_ptr_t meltfirstargp_,
					     const melt_argdescr_cell_t
					     meltxargdescr_[],
					     union meltparam_un *meltxargtab_,
					     const melt_argdescr_cell_t
					     meltxresdescr_[],
					     union meltparam_un
					     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_73_warmelt_outobj_OUTPUCOD_OBJCPPIF (meltclosure_ptr_t meltclosp_,
					      melt_ptr_t meltfirstargp_,
					      const melt_argdescr_cell_t
					      meltxargdescr_[],
					      union meltparam_un
					      *meltxargtab_,
					      const melt_argdescr_cell_t
					      meltxresdescr_[],
					      union meltparam_un
					      *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_74_warmelt_outobj_OUTPUCOD_OBJINTERNSYMBOL (meltclosure_ptr_t
						     meltclosp_,
						     melt_ptr_t
						     meltfirstargp_,
						     const
						     melt_argdescr_cell_t
						     meltxargdescr_[],
						     union meltparam_un
						     *meltxargtab_,
						     const
						     melt_argdescr_cell_t
						     meltxresdescr_[],
						     union meltparam_un
						     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_75_warmelt_outobj_OUTPUCOD_OBJINTERNKEYWORD (meltclosure_ptr_t
						      meltclosp_,
						      melt_ptr_t
						      meltfirstargp_,
						      const
						      melt_argdescr_cell_t
						      meltxargdescr_[],
						      union meltparam_un
						      *meltxargtab_,
						      const
						      melt_argdescr_cell_t
						      meltxresdescr_[],
						      union meltparam_un
						      *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_76_warmelt_outobj_OUTPUCOD_OBJGETNAMEDSYMBOL (meltclosure_ptr_t
						       meltclosp_,
						       melt_ptr_t
						       meltfirstargp_,
						       const
						       melt_argdescr_cell_t
						       meltxargdescr_[],
						       union meltparam_un
						       *meltxargtab_,
						       const
						       melt_argdescr_cell_t
						       meltxresdescr_[],
						       union meltparam_un
						       *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_77_warmelt_outobj_OUTPUCOD_OBJGETNAMEDKEYWORD (meltclosure_ptr_t
							meltclosp_,
							melt_ptr_t
							meltfirstargp_,
							const
							melt_argdescr_cell_t
							meltxargdescr_[],
							union meltparam_un
							*meltxargtab_,
							const
							melt_argdescr_cell_t
							meltxresdescr_[],
							union meltparam_un
							*meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_78_warmelt_outobj_OUTPUCOD_OBJAPPLY (meltclosure_ptr_t meltclosp_,
					      melt_ptr_t meltfirstargp_,
					      const melt_argdescr_cell_t
					      meltxargdescr_[],
					      union meltparam_un
					      *meltxargtab_,
					      const melt_argdescr_cell_t
					      meltxresdescr_[],
					      union meltparam_un
					      *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_79_warmelt_outobj_LAMBDA___11__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_80_warmelt_outobj_LAMBDA___12__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_81_warmelt_outobj_OUTPUCOD_OBJMSEND (meltclosure_ptr_t meltclosp_,
					      melt_ptr_t meltfirstargp_,
					      const melt_argdescr_cell_t
					      meltxargdescr_[],
					      union meltparam_un
					      *meltxargtab_,
					      const melt_argdescr_cell_t
					      meltxresdescr_[],
					      union meltparam_un
					      *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_82_warmelt_outobj_LAMBDA___13__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_83_warmelt_outobj_OUTPUCOD_OBJMULTIAPPLY (meltclosure_ptr_t
						   meltclosp_,
						   melt_ptr_t meltfirstargp_,
						   const melt_argdescr_cell_t
						   meltxargdescr_[],
						   union meltparam_un
						   *meltxargtab_,
						   const melt_argdescr_cell_t
						   meltxresdescr_[],
						   union meltparam_un
						   *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_84_warmelt_outobj_LAMBDA___14__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_85_warmelt_outobj_LAMBDA___15__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_86_warmelt_outobj_LAMBDA___16__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_87_warmelt_outobj_LAMBDA___17__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_88_warmelt_outobj_LAMBDA___18__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_89_warmelt_outobj_OUTPUCOD_OBJMULTIMSEND (meltclosure_ptr_t
						   meltclosp_,
						   melt_ptr_t meltfirstargp_,
						   const melt_argdescr_cell_t
						   meltxargdescr_[],
						   union meltparam_un
						   *meltxargtab_,
						   const melt_argdescr_cell_t
						   meltxresdescr_[],
						   union meltparam_un
						   *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_90_warmelt_outobj_LAMBDA___19__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_91_warmelt_outobj_LAMBDA___20__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_92_warmelt_outobj_LAMBDA___21__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_93_warmelt_outobj_LAMBDA___22__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_94_warmelt_outobj_LAMBDA___23__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_95_warmelt_outobj_OUTPUCOD_OBJCLEAR (meltclosure_ptr_t meltclosp_,
					      melt_ptr_t meltfirstargp_,
					      const melt_argdescr_cell_t
					      meltxargdescr_[],
					      union meltparam_un
					      *meltxargtab_,
					      const melt_argdescr_cell_t
					      meltxresdescr_[],
					      union meltparam_un
					      *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_96_warmelt_outobj_OUTPUCOD_OBJRAWALLOCOBJ (meltclosure_ptr_t
						    meltclosp_,
						    melt_ptr_t meltfirstargp_,
						    const melt_argdescr_cell_t
						    meltxargdescr_[],
						    union meltparam_un
						    *meltxargtab_,
						    const melt_argdescr_cell_t
						    meltxresdescr_[],
						    union meltparam_un
						    *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_97_warmelt_outobj_OUTPUCOD_OBJNEWCLOSURE (meltclosure_ptr_t
						   meltclosp_,
						   melt_ptr_t meltfirstargp_,
						   const melt_argdescr_cell_t
						   meltxargdescr_[],
						   union meltparam_un
						   *meltxargtab_,
						   const melt_argdescr_cell_t
						   meltxresdescr_[],
						   union meltparam_un
						   *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_98_warmelt_outobj_LAMBDA___24__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_99_warmelt_outobj_OUTPUCOD_OBJTOUCH (meltclosure_ptr_t meltclosp_,
					      melt_ptr_t meltfirstargp_,
					      const melt_argdescr_cell_t
					      meltxargdescr_[],
					      union meltparam_un
					      *meltxargtab_,
					      const melt_argdescr_cell_t
					      meltxresdescr_[],
					      union meltparam_un
					      *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_100_warmelt_outobj_OUTPUCOD_DBGTRACEWRITEOBJ (meltclosure_ptr_t
						       meltclosp_,
						       melt_ptr_t
						       meltfirstargp_,
						       const
						       melt_argdescr_cell_t
						       meltxargdescr_[],
						       union meltparam_un
						       *meltxargtab_,
						       const
						       melt_argdescr_cell_t
						       meltxresdescr_[],
						       union meltparam_un
						       *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_101_warmelt_outobj_OUTPUCOD_OBJPUTUPLE (meltclosure_ptr_t meltclosp_,
						 melt_ptr_t meltfirstargp_,
						 const melt_argdescr_cell_t
						 meltxargdescr_[],
						 union meltparam_un
						 *meltxargtab_,
						 const melt_argdescr_cell_t
						 meltxresdescr_[],
						 union meltparam_un
						 *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_102_warmelt_outobj_OUTPUCOD_OBJPUTPAIRHEAD (meltclosure_ptr_t
						     meltclosp_,
						     melt_ptr_t
						     meltfirstargp_,
						     const
						     melt_argdescr_cell_t
						     meltxargdescr_[],
						     union meltparam_un
						     *meltxargtab_,
						     const
						     melt_argdescr_cell_t
						     meltxresdescr_[],
						     union meltparam_un
						     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_103_warmelt_outobj_OUTPUCOD_OBJPUTPAIRTAIL (meltclosure_ptr_t
						     meltclosp_,
						     melt_ptr_t
						     meltfirstargp_,
						     const
						     melt_argdescr_cell_t
						     meltxargdescr_[],
						     union meltparam_un
						     *meltxargtab_,
						     const
						     melt_argdescr_cell_t
						     meltxresdescr_[],
						     union meltparam_un
						     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_104_warmelt_outobj_OUTPUCOD_OBJPUTLIST (meltclosure_ptr_t meltclosp_,
						 melt_ptr_t meltfirstargp_,
						 const melt_argdescr_cell_t
						 meltxargdescr_[],
						 union meltparam_un
						 *meltxargtab_,
						 const melt_argdescr_cell_t
						 meltxresdescr_[],
						 union meltparam_un
						 *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_105_warmelt_outobj_OUTPUCOD_OBJGETSLOT (meltclosure_ptr_t meltclosp_,
						 melt_ptr_t meltfirstargp_,
						 const melt_argdescr_cell_t
						 meltxargdescr_[],
						 union meltparam_un
						 *meltxargtab_,
						 const melt_argdescr_cell_t
						 meltxresdescr_[],
						 union meltparam_un
						 *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_106_warmelt_outobj_LAMBDA___25__ (meltclosure_ptr_t meltclosp_,
					   melt_ptr_t meltfirstargp_,
					   const melt_argdescr_cell_t
					   meltxargdescr_[],
					   union meltparam_un *meltxargtab_,
					   const melt_argdescr_cell_t
					   meltxresdescr_[],
					   union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_107_warmelt_outobj_OUTPUCOD_OBJPUTSLOT (meltclosure_ptr_t meltclosp_,
						 melt_ptr_t meltfirstargp_,
						 const melt_argdescr_cell_t
						 meltxargdescr_[],
						 union meltparam_un
						 *meltxargtab_,
						 const melt_argdescr_cell_t
						 meltxresdescr_[],
						 union meltparam_un
						 *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_108_warmelt_outobj_OUTPUCOD_OBJPUTCLOSUROUT (meltclosure_ptr_t
						      meltclosp_,
						      melt_ptr_t
						      meltfirstargp_,
						      const
						      melt_argdescr_cell_t
						      meltxargdescr_[],
						      union meltparam_un
						      *meltxargtab_,
						      const
						      melt_argdescr_cell_t
						      meltxresdescr_[],
						      union meltparam_un
						      *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_109_warmelt_outobj_OUTPUCOD_OBJPUTCLOSEDV (meltclosure_ptr_t
						    meltclosp_,
						    melt_ptr_t meltfirstargp_,
						    const melt_argdescr_cell_t
						    meltxargdescr_[],
						    union meltparam_un
						    *meltxargtab_,
						    const melt_argdescr_cell_t
						    meltxresdescr_[],
						    union meltparam_un
						    *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_110_warmelt_outobj_OUTPUCOD_OBJPUTCLOSEDNOTNULLV (meltclosure_ptr_t
							   meltclosp_,
							   melt_ptr_t
							   meltfirstargp_,
							   const
							   melt_argdescr_cell_t
							   meltxargdescr_[],
							   union meltparam_un
							   *meltxargtab_,
							   const
							   melt_argdescr_cell_t
							   meltxresdescr_[],
							   union meltparam_un
							   *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_111_warmelt_outobj_OUTPUCOD_OBJPUTROUTCONST (meltclosure_ptr_t
						      meltclosp_,
						      melt_ptr_t
						      meltfirstargp_,
						      const
						      melt_argdescr_cell_t
						      meltxargdescr_[],
						      union meltparam_un
						      *meltxargtab_,
						      const
						      melt_argdescr_cell_t
						      meltxresdescr_[],
						      union meltparam_un
						      *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_112_warmelt_outobj_OUTPUCOD_OBJPUTROUTCONSTNOTNULL (meltclosure_ptr_t
							     meltclosp_,
							     melt_ptr_t
							     meltfirstargp_,
							     const
							     melt_argdescr_cell_t
							     meltxargdescr_[],
							     union
							     meltparam_un
							     *meltxargtab_,
							     const
							     melt_argdescr_cell_t
							     meltxresdescr_[],
							     union
							     meltparam_un
							     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_113_warmelt_outobj_OUTPUCOD_OBJPUTXTRARESULT (meltclosure_ptr_t
						       meltclosp_,
						       melt_ptr_t
						       meltfirstargp_,
						       const
						       melt_argdescr_cell_t
						       meltxargdescr_[],
						       union meltparam_un
						       *meltxargtab_,
						       const
						       melt_argdescr_cell_t
						       meltxresdescr_[],
						       union meltparam_un
						       *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_114_warmelt_outobj_OUTPUCOD_OBJEXPV (meltclosure_ptr_t meltclosp_,
					      melt_ptr_t meltfirstargp_,
					      const melt_argdescr_cell_t
					      meltxargdescr_[],
					      union meltparam_un
					      *meltxargtab_,
					      const melt_argdescr_cell_t
					      meltxresdescr_[],
					      union meltparam_un
					      *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_115_warmelt_outobj_OUTPUCOD_OBJLOCATEDEXPV (meltclosure_ptr_t
						     meltclosp_,
						     melt_ptr_t
						     meltfirstargp_,
						     const
						     melt_argdescr_cell_t
						     meltxargdescr_[],
						     union meltparam_un
						     *meltxargtab_,
						     const
						     melt_argdescr_cell_t
						     meltxresdescr_[],
						     union meltparam_un
						     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_116_warmelt_outobj_OUTPUCOD_VERBATIMSTRING (meltclosure_ptr_t
						     meltclosp_,
						     melt_ptr_t
						     meltfirstargp_,
						     const
						     melt_argdescr_cell_t
						     meltxargdescr_[],
						     union meltparam_un
						     *meltxargtab_,
						     const
						     melt_argdescr_cell_t
						     meltxresdescr_[],
						     union meltparam_un
						     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_117_warmelt_outobj_OUTPUCOD_STRING (meltclosure_ptr_t meltclosp_,
					     melt_ptr_t meltfirstargp_,
					     const melt_argdescr_cell_t
					     meltxargdescr_[],
					     union meltparam_un *meltxargtab_,
					     const melt_argdescr_cell_t
					     meltxresdescr_[],
					     union meltparam_un
					     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_118_warmelt_outobj_OUTPUCOD_INTEGER (meltclosure_ptr_t meltclosp_,
					      melt_ptr_t meltfirstargp_,
					      const melt_argdescr_cell_t
					      meltxargdescr_[],
					      union meltparam_un
					      *meltxargtab_,
					      const melt_argdescr_cell_t
					      meltxresdescr_[],
					      union meltparam_un
					      *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_119_warmelt_outobj_OUTPUCOD_FINALRETURN (meltclosure_ptr_t
						  meltclosp_,
						  melt_ptr_t meltfirstargp_,
						  const melt_argdescr_cell_t
						  meltxargdescr_[],
						  union meltparam_un
						  *meltxargtab_,
						  const melt_argdescr_cell_t
						  meltxresdescr_[],
						  union meltparam_un
						  *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_120_warmelt_outobj_SORTED_NAMED_DICT_TUPLE (meltclosure_ptr_t
						     meltclosp_,
						     melt_ptr_t
						     meltfirstargp_,
						     const
						     melt_argdescr_cell_t
						     meltxargdescr_[],
						     union meltparam_un
						     *meltxargtab_,
						     const
						     melt_argdescr_cell_t
						     meltxresdescr_[],
						     union meltparam_un
						     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_121_warmelt_outobj_OUTPUT_EXPORTED_OFFSETS (meltclosure_ptr_t
						     meltclosp_,
						     melt_ptr_t
						     meltfirstargp_,
						     const
						     melt_argdescr_cell_t
						     meltxargdescr_[],
						     union meltparam_un
						     *meltxargtab_,
						     const
						     melt_argdescr_cell_t
						     meltxresdescr_[],
						     union meltparam_un
						     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_122_warmelt_outobj_NTH_SECUNDARY_FILE (meltclosure_ptr_t meltclosp_,
						melt_ptr_t meltfirstargp_,
						const melt_argdescr_cell_t
						meltxargdescr_[],
						union meltparam_un
						*meltxargtab_,
						const melt_argdescr_cell_t
						meltxresdescr_[],
						union meltparam_un
						*meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_123_warmelt_outobj_OUTPUT_MELT_DESCRIPTOR (meltclosure_ptr_t
						    meltclosp_,
						    melt_ptr_t meltfirstargp_,
						    const melt_argdescr_cell_t
						    meltxargdescr_[],
						    union meltparam_un
						    *meltxargtab_,
						    const melt_argdescr_cell_t
						    meltxresdescr_[],
						    union meltparam_un
						    *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_124_warmelt_outobj_SYNTESTGEN_ANY (meltclosure_ptr_t meltclosp_,
					    melt_ptr_t meltfirstargp_,
					    const melt_argdescr_cell_t
					    meltxargdescr_[],
					    union meltparam_un *meltxargtab_,
					    const melt_argdescr_cell_t
					    meltxresdescr_[],
					    union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_125_warmelt_outobj_SUBSTITUTE_FORMALS_FOR_SYNTEST (meltclosure_ptr_t
							    meltclosp_,
							    melt_ptr_t
							    meltfirstargp_,
							    const
							    melt_argdescr_cell_t
							    meltxargdescr_[],
							    union meltparam_un
							    *meltxargtab_,
							    const
							    melt_argdescr_cell_t
							    meltxresdescr_[],
							    union meltparam_un
							    *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_126_warmelt_outobj_EXPAND_TUPLE_FOR_SYNTEST (meltclosure_ptr_t
						      meltclosp_,
						      melt_ptr_t
						      meltfirstargp_,
						      const
						      melt_argdescr_cell_t
						      meltxargdescr_[],
						      union meltparam_un
						      *meltxargtab_,
						      const
						      melt_argdescr_cell_t
						      meltxresdescr_[],
						      union meltparam_un
						      *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_127_warmelt_outobj_SYNTESTGEN_PRIMITIVE (meltclosure_ptr_t
						  meltclosp_,
						  melt_ptr_t meltfirstargp_,
						  const melt_argdescr_cell_t
						  meltxargdescr_[],
						  union meltparam_un
						  *meltxargtab_,
						  const melt_argdescr_cell_t
						  meltxresdescr_[],
						  union meltparam_un
						  *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_128_warmelt_outobj_SYNTESTGEN_CITERATOR (meltclosure_ptr_t
						  meltclosp_,
						  melt_ptr_t meltfirstargp_,
						  const melt_argdescr_cell_t
						  meltxargdescr_[],
						  union meltparam_un
						  *meltxargtab_,
						  const melt_argdescr_cell_t
						  meltxresdescr_[],
						  union meltparam_un
						  *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_129_warmelt_outobj_SYNTESTGEN_CMATCHER (meltclosure_ptr_t meltclosp_,
						 melt_ptr_t meltfirstargp_,
						 const melt_argdescr_cell_t
						 meltxargdescr_[],
						 union meltparam_un
						 *meltxargtab_,
						 const melt_argdescr_cell_t
						 meltxresdescr_[],
						 union meltparam_un
						 *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_130_warmelt_outobj_EMIT_SYNTAX_TESTING_ROUTINE (meltclosure_ptr_t
							 meltclosp_,
							 melt_ptr_t
							 meltfirstargp_,
							 const
							 melt_argdescr_cell_t
							 meltxargdescr_[],
							 union meltparam_un
							 *meltxargtab_,
							 const
							 melt_argdescr_cell_t
							 meltxresdescr_[],
							 union meltparam_un
							 *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_131_warmelt_outobj_NORMADECLB_MACROEXPANDED_LIST (meltclosure_ptr_t
							   meltclosp_,
							   melt_ptr_t
							   meltfirstargp_,
							   const
							   melt_argdescr_cell_t
							   meltxargdescr_[],
							   union meltparam_un
							   *meltxargtab_,
							   const
							   melt_argdescr_cell_t
							   meltxresdescr_[],
							   union meltparam_un
							   *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_132_warmelt_outobj_LAMBDA___26__ (meltclosure_ptr_t meltclosp_,
					   melt_ptr_t meltfirstargp_,
					   const melt_argdescr_cell_t
					   meltxargdescr_[],
					   union meltparam_un *meltxargtab_,
					   const melt_argdescr_cell_t
					   meltxresdescr_[],
					   union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_133_warmelt_outobj_TRANSLATE_MACROEXPANDED_LIST (meltclosure_ptr_t
							  meltclosp_,
							  melt_ptr_t
							  meltfirstargp_,
							  const
							  melt_argdescr_cell_t
							  meltxargdescr_[],
							  union meltparam_un
							  *meltxargtab_,
							  const
							  melt_argdescr_cell_t
							  meltxresdescr_[],
							  union meltparam_un
							  *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_134_warmelt_outobj_LAMBDA___27__ (meltclosure_ptr_t meltclosp_,
					   melt_ptr_t meltfirstargp_,
					   const melt_argdescr_cell_t
					   meltxargdescr_[],
					   union meltparam_un *meltxargtab_,
					   const melt_argdescr_cell_t
					   meltxresdescr_[],
					   union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_135_warmelt_outobj_FATAL_COMPILE_ERROR (meltclosure_ptr_t meltclosp_,
						 melt_ptr_t meltfirstargp_,
						 const melt_argdescr_cell_t
						 meltxargdescr_[],
						 union meltparam_un
						 *meltxargtab_,
						 const melt_argdescr_cell_t
						 meltxresdescr_[],
						 union meltparam_un
						 *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_136_warmelt_outobj_COMPILE_LIST_SEXPR (meltclosure_ptr_t meltclosp_,
						melt_ptr_t meltfirstargp_,
						const melt_argdescr_cell_t
						meltxargdescr_[],
						union meltparam_un
						*meltxargtab_,
						const melt_argdescr_cell_t
						meltxresdescr_[],
						union meltparam_un
						*meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_137_warmelt_outobj_LAMBDA___28__ (meltclosure_ptr_t meltclosp_,
					   melt_ptr_t meltfirstargp_,
					   const melt_argdescr_cell_t
					   meltxargdescr_[],
					   union meltparam_un *meltxargtab_,
					   const melt_argdescr_cell_t
					   meltxresdescr_[],
					   union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_138_warmelt_outobj_AUTOBOX_NORMAL_RETURN (meltclosure_ptr_t
						   meltclosp_,
						   melt_ptr_t meltfirstargp_,
						   const melt_argdescr_cell_t
						   meltxargdescr_[],
						   union meltparam_un
						   *meltxargtab_,
						   const melt_argdescr_cell_t
						   meltxresdescr_[],
						   union meltparam_un
						   *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_139_warmelt_outobj_TRANSLATE_RUN_MELT_EXPRESSIONS (meltclosure_ptr_t
							    meltclosp_,
							    melt_ptr_t
							    meltfirstargp_,
							    const
							    melt_argdescr_cell_t
							    meltxargdescr_[],
							    union meltparam_un
							    *meltxargtab_,
							    const
							    melt_argdescr_cell_t
							    meltxresdescr_[],
							    union meltparam_un
							    *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_140_warmelt_outobj_LAMBDA___29__ (meltclosure_ptr_t meltclosp_,
					   melt_ptr_t meltfirstargp_,
					   const melt_argdescr_cell_t
					   meltxargdescr_[],
					   union meltparam_un *meltxargtab_,
					   const melt_argdescr_cell_t
					   meltxresdescr_[],
					   union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_141_warmelt_outobj_LAMBDA___30__ (meltclosure_ptr_t meltclosp_,
					   melt_ptr_t meltfirstargp_,
					   const melt_argdescr_cell_t
					   meltxargdescr_[],
					   union meltparam_un *meltxargtab_,
					   const melt_argdescr_cell_t
					   meltxresdescr_[],
					   union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_142_warmelt_outobj_LAMBDA___31__ (meltclosure_ptr_t meltclosp_,
					   melt_ptr_t meltfirstargp_,
					   const melt_argdescr_cell_t
					   meltxargdescr_[],
					   union meltparam_un *meltxargtab_,
					   const melt_argdescr_cell_t
					   meltxresdescr_[],
					   union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_143_warmelt_outobj_READ_MELT_EXPRESSIONS (meltclosure_ptr_t
						   meltclosp_,
						   melt_ptr_t meltfirstargp_,
						   const melt_argdescr_cell_t
						   meltxargdescr_[],
						   union meltparam_un
						   *meltxargtab_,
						   const melt_argdescr_cell_t
						   meltxresdescr_[],
						   union meltparam_un
						   *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_144_warmelt_outobj_TRANSLATE_TO_C_MODULE_MELT_SOURCES
(meltclosure_ptr_t meltclosp_, melt_ptr_t meltfirstargp_, const melt_argdescr_cell_t meltxargdescr_[],
union meltparam_un *meltxargtab_, const melt_argdescr_cell_t meltxresdescr_[],
union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_145_warmelt_outobj_GENERATE_GPLV3PLUS_COPYRIGHT_NOTICE_C_COMMENT
(meltclosure_ptr_t meltclosp_, melt_ptr_t meltfirstargp_, const melt_argdescr_cell_t meltxargdescr_[],
union meltparam_un *meltxargtab_, const melt_argdescr_cell_t meltxresdescr_[],
union meltparam_un *meltxrestab_);



MELT_EXTERN void *melt_start_this_module (void *);


/* define different names when debugging or not */
#if MELT_HAVE_DEBUG
MELT_EXTERN const char meltmodule_warmelt_outobj__melt_have_debug_enabled[];
#define melt_have_debug_string meltmodule_warmelt_outobj__melt_have_debug_enabled
#else /*!MELT_HAVE_DEBUG */
MELT_EXTERN const char meltmodule_warmelt_outobj__melt_have_debug_disabled[];
#define melt_have_debug_string meltmodule_warmelt_outobj__melt_have_debug_disabled
#endif /*!MELT_HAVE_DEBUG */


struct frame_melt_start_this_module_st;
void MELT_MODULE_VISIBILITY
meltmod__warmelt_outobj__initialmeltchunk_0 (struct
					     frame_melt_start_this_module_st
					     *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_outobj__initialmeltchunk_1 (struct
					     frame_melt_start_this_module_st
					     *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_outobj__initialmeltchunk_2 (struct
					     frame_melt_start_this_module_st
					     *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_outobj__initialmeltchunk_3 (struct
					     frame_melt_start_this_module_st
					     *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_outobj__initialmeltchunk_4 (struct
					     frame_melt_start_this_module_st
					     *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_outobj__initialmeltchunk_5 (struct
					     frame_melt_start_this_module_st
					     *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_outobj__initialmeltchunk_6 (struct
					     frame_melt_start_this_module_st
					     *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_outobj__initialmeltchunk_7 (struct
					     frame_melt_start_this_module_st
					     *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_outobj__initialmeltchunk_8 (struct
					     frame_melt_start_this_module_st
					     *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_outobj__initialmeltchunk_9 (struct
					     frame_melt_start_this_module_st
					     *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_outobj__initialmeltchunk_10 (struct
					      frame_melt_start_this_module_st
					      *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_outobj__initialmeltchunk_11 (struct
					      frame_melt_start_this_module_st
					      *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_outobj__initialmeltchunk_12 (struct
					      frame_melt_start_this_module_st
					      *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_outobj__initialmeltchunk_13 (struct
					      frame_melt_start_this_module_st
					      *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_outobj__initialmeltchunk_14 (struct
					      frame_melt_start_this_module_st
					      *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_outobj__initialmeltchunk_15 (struct
					      frame_melt_start_this_module_st
					      *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_outobj__initialmeltchunk_16 (struct
					      frame_melt_start_this_module_st
					      *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_outobj__initialmeltchunk_17 (struct
					      frame_melt_start_this_module_st
					      *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_outobj__forward_or_mark_module_start_frame (struct
							     melt_callframe_st
							     *fp,
							     int marking);
#define meltmarking_melt_start_this_module  meltmod__warmelt_outobj__forward_or_mark_module_start_frame



/**** warmelt-outobj+03.c implementations ****/




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_60_warmelt_outobj_OUTPUCOD_OBJLABELINSTR (meltclosure_ptr_t
						   meltclosp_,
						   melt_ptr_t meltfirstargp_,
						   const melt_argdescr_cell_t
						   meltxargdescr_[],
						   union meltparam_un
						   *meltxargtab_,
						   const melt_argdescr_cell_t
						   meltxresdescr_[],
						   union meltparam_un
						   *meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_60_warmelt_outobj_OUTPUCOD_OBJLABELINSTR_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_60_warmelt_outobj_OUTPUCOD_OBJLABELINSTR_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 14
    melt_ptr_t mcfr_varptr[14];
#define MELTFRAM_NBVARNUM 6
    long mcfr_varnum[6];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_60_warmelt_outobj_OUTPUCOD_OBJLABELINSTR is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_60_warmelt_outobj_OUTPUCOD_OBJLABELINSTR_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 14; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_60_warmelt_outobj_OUTPUCOD_OBJLABELINSTR nbval 14*/
  meltfram__.mcfr_nbvar = 14 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("OUTPUCOD_OBJLABELINSTR", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-outobj.melt:2845:/ getarg");
 /*_.OBLAB__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.DECLBUF__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.DECLBUF__V3*/ meltfptr[2])) !=
	      NULL);


  /*getarg#2 */
  /*^getarg */
  if (meltxargdescr_[1] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.IMPLBUF__V4*/ meltfptr[3] =
    (meltxargtab_[1].meltbp_aptr) ? (*(meltxargtab_[1].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3])) !=
	      NULL);


  /*getarg#3 */
  /*^getarg */
  if (meltxargdescr_[2] != MELTBPAR_LONG)
    goto lab_endgetargs;
 /*_#DEPTH__L1*/ meltfnum[0] = meltxargtab_[2].meltbp_long;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:2846:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L2*/ meltfnum[1] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.OBLAB__V2*/ meltfptr[1]),
			     (melt_ptr_t) (( /*!CLASS_OBJLABELINSTR */
					    meltfrout->tabval[0])));;
      MELT_LOCATION ("warmelt-outobj.melt:2846:/ cond");
      /*cond */ if ( /*_#IS_A__L2*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V6*/ meltfptr[5] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-outobj.melt:2846:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check oblab"),
				  ("warmelt-outobj.melt")
				  ? ("warmelt-outobj.melt") : __FILE__,
				  (2846) ? (2846) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V6*/ meltfptr[5] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V5*/ meltfptr[4] = /*_.IFELSE___V6*/ meltfptr[5];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:2846:/ clear");
	     /*clear *//*_#IS_A__L2*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V6*/ meltfptr[5] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V5*/ meltfptr[4] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:2847:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				( /*_#DEPTH__L1*/ meltfnum[0]), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:2848:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   ("/*objlabel*/ "));
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:2849:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.OBLAB__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 1, "OBLAB_PREFIX");
  /*_.OBLAB_PREFIX__V7*/ meltfptr[5] = slot;
    };
    ;

    {
      /*^locexp */
      /*add2sbuf_string */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   melt_string_str ((melt_ptr_t)
					    ( /*_.OBLAB_PREFIX__V7*/
					     meltfptr[5])));
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:2850:/ quasiblock");


    /*^getslot */
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.OBLAB__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 2, "OBLAB_RANK");
  /*_.OBRANK__V8*/ meltfptr[7] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-outobj.melt:2851:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^cond */
    /*cond */ if ( /*_.OBRANK__V8*/ meltfptr[7])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-outobj.melt:2852:/ getslot");
	  {
	    melt_ptr_t slot = NULL, obj = NULL;
	    obj = (melt_ptr_t) ( /*_.OBLAB__V2*/ meltfptr[1]) /*=obj*/ ;
	    melt_object_get_field (slot, obj, 2, "OBLAB_RANK");
    /*_.OBLAB_RANK__V9*/ meltfptr[8] = slot;
	  };
	  ;
   /*_#GET_INT__L3*/ meltfnum[1] =
	    (melt_get_int
	     ((melt_ptr_t) ( /*_.OBLAB_RANK__V9*/ meltfptr[8])));;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:2852:/ locexp");
	    meltgc_add_strbuf_dec ((melt_ptr_t)
				   ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				   ( /*_#GET_INT__L3*/ meltfnum[1]));
	  }
	  ;
	  /*epilog */

	  MELT_LOCATION ("warmelt-outobj.melt:2851:/ clear");
	     /*clear *//*_.OBLAB_RANK__V9*/ meltfptr[8] = 0;
	  /*^clear */
	     /*clear *//*_#GET_INT__L3*/ meltfnum[1] = 0;
	}
	;
      }				/*noelse */
    ;

    MELT_LOCATION ("warmelt-outobj.melt:2850:/ clear");
	   /*clear *//*_.OBRANK__V8*/ meltfptr[7] = 0;

    {
      MELT_LOCATION ("warmelt-outobj.melt:2853:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   (": ;"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:2854:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				( /*_#DEPTH__L1*/ meltfnum[0]), 0);
    }
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:2855:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#STRBUF_USEDLENGTH__L4*/ meltfnum[1] =
	melt_strbuf_usedlength ((melt_ptr_t)
				( /*_.IMPLBUF__V4*/ meltfptr[3]));;
      MELT_LOCATION ("warmelt-outobj.melt:2856:/ cond");
      /*cond */ if (
		     /*ifisa */
		     melt_is_instance_of ((melt_ptr_t)
					  (( /*!BUFFER_LIMIT_CONT */
					    meltfrout->tabval[1])),
					  (melt_ptr_t) (( /*!CLASS_REFERENCE */ meltfrout->tabval[2])))
	)			/*then */
	{
	  /*^cond.then */
	  /*^getslot */
	  {
	    melt_ptr_t slot = NULL, obj = NULL;
	    obj =
	      (melt_ptr_t) (( /*!BUFFER_LIMIT_CONT */ meltfrout->
			     tabval[1])) /*=obj*/ ;
	    melt_object_get_field (slot, obj, 0, "REFERENCED_VALUE");
     /*_.REFERENCED_VALUE__V11*/ meltfptr[7] = slot;
	  };
	  ;
	}
      else
	{			/*^cond.else */

    /*_.REFERENCED_VALUE__V11*/ meltfptr[7] = NULL;;
	}
      ;
      /*^compute */
   /*_#GET_INT__L5*/ meltfnum[4] =
	(melt_get_int
	 ((melt_ptr_t) ( /*_.REFERENCED_VALUE__V11*/ meltfptr[7])));;
      /*^compute */
   /*_#I__L6*/ meltfnum[5] =
	(( /*_#STRBUF_USEDLENGTH__L4*/ meltfnum[1]) <
	 ( /*_#GET_INT__L5*/ meltfnum[4]));;
      MELT_LOCATION ("warmelt-outobj.melt:2855:/ cond");
      /*cond */ if ( /*_#I__L6*/ meltfnum[5])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V12*/ meltfptr[11] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-outobj.melt:2855:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check limited implbuf"),
				  ("warmelt-outobj.melt")
				  ? ("warmelt-outobj.melt") : __FILE__,
				  (2855) ? (2855) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V12*/ meltfptr[11] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V10*/ meltfptr[8] = /*_.IFELSE___V12*/ meltfptr[11];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:2855:/ clear");
	     /*clear *//*_#STRBUF_USEDLENGTH__L4*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.REFERENCED_VALUE__V11*/ meltfptr[7] = 0;
      /*^clear */
	     /*clear *//*_#GET_INT__L5*/ meltfnum[4] = 0;
      /*^clear */
	     /*clear *//*_#I__L6*/ meltfnum[5] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V12*/ meltfptr[11] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V10*/ meltfptr[8] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-outobj.melt:2857:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.OBLAB__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 0, "OBI_LOC");
  /*_.OBI_LOC__V13*/ meltfptr[7] = slot;
    };
    ;
    /*^checksignal */
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[3];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.IMPLBUF__V4*/ meltfptr[3];
      /*^apply.arg */
      argtab[1].meltbp_long = /*_#DEPTH__L1*/ meltfnum[0];
      /*^apply.arg */
      argtab[2].meltbp_cstring = "objlabel";
      /*_.OUTPUT_LOCATION__V14*/ meltfptr[11] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!OUTPUT_LOCATION */ meltfrout->tabval[3])),
		    (melt_ptr_t) ( /*_.OBI_LOC__V13*/ meltfptr[7]),
		    (MELTBPARSTR_PTR MELTBPARSTR_LONG MELTBPARSTR_CSTRING ""),
		    argtab, "", (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:2845:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.OUTPUT_LOCATION__V14*/ meltfptr[11];;

    {
      MELT_LOCATION ("warmelt-outobj.melt:2845:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V5*/ meltfptr[4] = 0;
    /*^clear */
	   /*clear *//*_.OBLAB_PREFIX__V7*/ meltfptr[5] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V10*/ meltfptr[8] = 0;
    /*^clear */
	   /*clear *//*_.OBI_LOC__V13*/ meltfptr[7] = 0;
    /*^clear */
	   /*clear *//*_.OUTPUT_LOCATION__V14*/ meltfptr[11] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("OUTPUCOD_OBJLABELINSTR", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_60_warmelt_outobj_OUTPUCOD_OBJLABELINSTR_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_60_warmelt_outobj_OUTPUCOD_OBJLABELINSTR */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_61_warmelt_outobj_OUTPUCOD_OBJGOTOINSTR (meltclosure_ptr_t
						  meltclosp_,
						  melt_ptr_t meltfirstargp_,
						  const melt_argdescr_cell_t
						  meltxargdescr_[],
						  union meltparam_un *
						  meltxargtab_,
						  const melt_argdescr_cell_t
						  meltxresdescr_[],
						  union meltparam_un *
						  meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_61_warmelt_outobj_OUTPUCOD_OBJGOTOINSTR_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_61_warmelt_outobj_OUTPUCOD_OBJGOTOINSTR_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 13
    melt_ptr_t mcfr_varptr[13];
#define MELTFRAM_NBVARNUM 6
    long mcfr_varnum[6];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_61_warmelt_outobj_OUTPUCOD_OBJGOTOINSTR is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_61_warmelt_outobj_OUTPUCOD_OBJGOTOINSTR_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 13; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_61_warmelt_outobj_OUTPUCOD_OBJGOTOINSTR nbval 13*/
  meltfram__.mcfr_nbvar = 13 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("OUTPUCOD_OBJGOTOINSTR", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-outobj.melt:2864:/ getarg");
 /*_.OBGOTO__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.DECLBUF__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.DECLBUF__V3*/ meltfptr[2])) !=
	      NULL);


  /*getarg#2 */
  /*^getarg */
  if (meltxargdescr_[1] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.IMPLBUF__V4*/ meltfptr[3] =
    (meltxargtab_[1].meltbp_aptr) ? (*(meltxargtab_[1].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3])) !=
	      NULL);


  /*getarg#3 */
  /*^getarg */
  if (meltxargdescr_[2] != MELTBPAR_LONG)
    goto lab_endgetargs;
 /*_#DEPTH__L1*/ meltfnum[0] = meltxargtab_[2].meltbp_long;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:2865:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L2*/ meltfnum[1] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.OBGOTO__V2*/ meltfptr[1]),
			     (melt_ptr_t) (( /*!CLASS_OBJGOTOINSTR */
					    meltfrout->tabval[0])));;
      MELT_LOCATION ("warmelt-outobj.melt:2865:/ cond");
      /*cond */ if ( /*_#IS_A__L2*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V6*/ meltfptr[5] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-outobj.melt:2865:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check obgoto"),
				  ("warmelt-outobj.melt")
				  ? ("warmelt-outobj.melt") : __FILE__,
				  (2865) ? (2865) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V6*/ meltfptr[5] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V5*/ meltfptr[4] = /*_.IFELSE___V6*/ meltfptr[5];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:2865:/ clear");
	     /*clear *//*_#IS_A__L2*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V6*/ meltfptr[5] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V5*/ meltfptr[4] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-outobj.melt:2866:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.OBGOTO__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 0, "OBI_LOC");
  /*_.OBI_LOC__V7*/ meltfptr[5] = slot;
    };
    ;
    /*^checksignal */
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[3];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.IMPLBUF__V4*/ meltfptr[3];
      /*^apply.arg */
      argtab[1].meltbp_long = /*_#DEPTH__L1*/ meltfnum[0];
      /*^apply.arg */
      argtab[2].meltbp_cstring = "objgoto";
      /*_.OUTPUT_LOCATION__V8*/ meltfptr[7] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!OUTPUT_LOCATION */ meltfrout->tabval[1])),
		    (melt_ptr_t) ( /*_.OBI_LOC__V7*/ meltfptr[5]),
		    (MELTBPARSTR_PTR MELTBPARSTR_LONG MELTBPARSTR_CSTRING ""),
		    argtab, "", (union meltparam_un *) 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:2867:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   ("/*objgoto*/ goto "));
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:2868:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.OBGOTO__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 1, "OBGOTO_PREFIX");
  /*_.OBGOTO_PREFIX__V9*/ meltfptr[8] = slot;
    };
    ;

    {
      /*^locexp */
      /*add2sbuf_string */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   melt_string_str ((melt_ptr_t)
					    ( /*_.OBGOTO_PREFIX__V9*/
					     meltfptr[8])));
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:2869:/ quasiblock");


    /*^getslot */
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.OBGOTO__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 2, "OBGOTO_RANK");
  /*_.OBRANK__V10*/ meltfptr[9] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-outobj.melt:2870:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^cond */
    /*cond */ if ( /*_.OBRANK__V10*/ meltfptr[9])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

   /*_#GET_INT__L3*/ meltfnum[1] =
	    (melt_get_int ((melt_ptr_t) ( /*_.OBRANK__V10*/ meltfptr[9])));;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:2871:/ locexp");
	    meltgc_add_strbuf_dec ((melt_ptr_t)
				   ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				   ( /*_#GET_INT__L3*/ meltfnum[1]));
	  }
	  ;
	  /*epilog */

	  MELT_LOCATION ("warmelt-outobj.melt:2870:/ clear");
	     /*clear *//*_#GET_INT__L3*/ meltfnum[1] = 0;
	}
	;
      }				/*noelse */
    ;

    MELT_LOCATION ("warmelt-outobj.melt:2869:/ clear");
	   /*clear *//*_.OBRANK__V10*/ meltfptr[9] = 0;

    {
      MELT_LOCATION ("warmelt-outobj.melt:2872:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   (";"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:2873:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				( /*_#DEPTH__L1*/ meltfnum[0]), 0);
    }
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:2874:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#STRBUF_USEDLENGTH__L4*/ meltfnum[1] =
	melt_strbuf_usedlength ((melt_ptr_t)
				( /*_.IMPLBUF__V4*/ meltfptr[3]));;
      MELT_LOCATION ("warmelt-outobj.melt:2875:/ cond");
      /*cond */ if (
		     /*ifisa */
		     melt_is_instance_of ((melt_ptr_t)
					  (( /*!BUFFER_LIMIT_CONT */
					    meltfrout->tabval[2])),
					  (melt_ptr_t) (( /*!CLASS_REFERENCE */ meltfrout->tabval[3])))
	)			/*then */
	{
	  /*^cond.then */
	  /*^getslot */
	  {
	    melt_ptr_t slot = NULL, obj = NULL;
	    obj =
	      (melt_ptr_t) (( /*!BUFFER_LIMIT_CONT */ meltfrout->
			     tabval[2])) /*=obj*/ ;
	    melt_object_get_field (slot, obj, 0, "REFERENCED_VALUE");
     /*_.REFERENCED_VALUE__V12*/ meltfptr[11] = slot;
	  };
	  ;
	}
      else
	{			/*^cond.else */

    /*_.REFERENCED_VALUE__V12*/ meltfptr[11] = NULL;;
	}
      ;
      /*^compute */
   /*_#GET_INT__L5*/ meltfnum[4] =
	(melt_get_int
	 ((melt_ptr_t) ( /*_.REFERENCED_VALUE__V12*/ meltfptr[11])));;
      /*^compute */
   /*_#I__L6*/ meltfnum[5] =
	(( /*_#STRBUF_USEDLENGTH__L4*/ meltfnum[1]) <
	 ( /*_#GET_INT__L5*/ meltfnum[4]));;
      MELT_LOCATION ("warmelt-outobj.melt:2874:/ cond");
      /*cond */ if ( /*_#I__L6*/ meltfnum[5])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V13*/ meltfptr[12] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-outobj.melt:2874:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check limited implbuf"),
				  ("warmelt-outobj.melt")
				  ? ("warmelt-outobj.melt") : __FILE__,
				  (2874) ? (2874) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V13*/ meltfptr[12] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V11*/ meltfptr[9] = /*_.IFELSE___V13*/ meltfptr[12];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:2874:/ clear");
	     /*clear *//*_#STRBUF_USEDLENGTH__L4*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.REFERENCED_VALUE__V12*/ meltfptr[11] = 0;
      /*^clear */
	     /*clear *//*_#GET_INT__L5*/ meltfnum[4] = 0;
      /*^clear */
	     /*clear *//*_#I__L6*/ meltfnum[5] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V13*/ meltfptr[12] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V11*/ meltfptr[9] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-outobj.melt:2864:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.IFCPP___V11*/ meltfptr[9];;

    {
      MELT_LOCATION ("warmelt-outobj.melt:2864:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V5*/ meltfptr[4] = 0;
    /*^clear */
	   /*clear *//*_.OBI_LOC__V7*/ meltfptr[5] = 0;
    /*^clear */
	   /*clear *//*_.OUTPUT_LOCATION__V8*/ meltfptr[7] = 0;
    /*^clear */
	   /*clear *//*_.OBGOTO_PREFIX__V9*/ meltfptr[8] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V11*/ meltfptr[9] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("OUTPUCOD_OBJGOTOINSTR", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_61_warmelt_outobj_OUTPUCOD_OBJGOTOINSTR_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_61_warmelt_outobj_OUTPUCOD_OBJGOTOINSTR */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_62_warmelt_outobj_ADD2SBUF_CLONSYM (meltclosure_ptr_t meltclosp_,
					     melt_ptr_t meltfirstargp_,
					     const melt_argdescr_cell_t
					     meltxargdescr_[],
					     union meltparam_un *
					     meltxargtab_,
					     const melt_argdescr_cell_t
					     meltxresdescr_[],
					     union meltparam_un *
					     meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_62_warmelt_outobj_ADD2SBUF_CLONSYM_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_62_warmelt_outobj_ADD2SBUF_CLONSYM_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 13
    melt_ptr_t mcfr_varptr[13];
#define MELTFRAM_NBVARNUM 6
    long mcfr_varnum[6];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_62_warmelt_outobj_ADD2SBUF_CLONSYM is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_62_warmelt_outobj_ADD2SBUF_CLONSYM_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 13; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_62_warmelt_outobj_ADD2SBUF_CLONSYM nbval 13*/
  meltfram__.mcfr_nbvar = 13 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("ADD2SBUF_CLONSYM", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-outobj.melt:2881:/ getarg");
 /*_.SBUF__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.CSY__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.CSY__V3*/ meltfptr[2])) != NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:2882:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_STRBUF__L1*/ meltfnum[0] =
	(melt_magic_discr ((melt_ptr_t) ( /*_.SBUF__V2*/ meltfptr[1])) ==
	 MELTOBMAG_STRBUF);;
      MELT_LOCATION ("warmelt-outobj.melt:2882:/ cond");
      /*cond */ if ( /*_#IS_STRBUF__L1*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V5*/ meltfptr[4] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-outobj.melt:2882:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check sbuf"),
				  ("warmelt-outobj.melt")
				  ? ("warmelt-outobj.melt") : __FILE__,
				  (2882) ? (2882) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V5*/ meltfptr[4] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V4*/ meltfptr[3] = /*_.IFELSE___V5*/ meltfptr[4];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:2882:/ clear");
	     /*clear *//*_#IS_STRBUF__L1*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V5*/ meltfptr[4] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V4*/ meltfptr[3] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:2883:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L2*/ meltfnum[0] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.CSY__V3*/ meltfptr[2]),
			     (melt_ptr_t) (( /*!CLASS_CLONED_SYMBOL */
					    meltfrout->tabval[0])));;
      MELT_LOCATION ("warmelt-outobj.melt:2883:/ cond");
      /*cond */ if ( /*_#IS_A__L2*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V7*/ meltfptr[6] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-outobj.melt:2883:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check csy"),
				  ("warmelt-outobj.melt")
				  ? ("warmelt-outobj.melt") : __FILE__,
				  (2883) ? (2883) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V7*/ meltfptr[6] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V6*/ meltfptr[4] = /*_.IFELSE___V7*/ meltfptr[6];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:2883:/ clear");
	     /*clear *//*_#IS_A__L2*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V7*/ meltfptr[6] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V6*/ meltfptr[4] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-outobj.melt:2884:/ quasiblock");


    /*^getslot */
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.CSY__V3*/ meltfptr[2]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 1, "NAMED_NAME");
  /*_.CNAM__V9*/ meltfptr[8] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-outobj.melt:2885:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.CSY__V3*/ meltfptr[2]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 3, "CSYM_URANK");
  /*_.CSYM_URANK__V10*/ meltfptr[9] = slot;
    };
    ;
 /*_#RK__L3*/ meltfnum[0] =
      (melt_get_int ((melt_ptr_t) ( /*_.CSYM_URANK__V10*/ meltfptr[9])));;

    {
      MELT_LOCATION ("warmelt-outobj.melt:2886:/ locexp");
      meltgc_add_strbuf_cident ((melt_ptr_t) ( /*_.SBUF__V2*/ meltfptr[1]),
				melt_string_str ((melt_ptr_t)
						 ( /*_.CNAM__V9*/
						  meltfptr[8])));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:2887:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V2*/ meltfptr[1]), ("_"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:2888:/ locexp");
      meltgc_add_strbuf_dec ((melt_ptr_t) ( /*_.SBUF__V2*/ meltfptr[1]),
			     ( /*_#RK__L3*/ meltfnum[0]));
    }
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:2889:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#STRBUF_USEDLENGTH__L4*/ meltfnum[3] =
	melt_strbuf_usedlength ((melt_ptr_t) ( /*_.SBUF__V2*/ meltfptr[1]));;
      MELT_LOCATION ("warmelt-outobj.melt:2890:/ cond");
      /*cond */ if (
		     /*ifisa */
		     melt_is_instance_of ((melt_ptr_t)
					  (( /*!BUFFER_LIMIT_CONT */
					    meltfrout->tabval[1])),
					  (melt_ptr_t) (( /*!CLASS_REFERENCE */ meltfrout->tabval[2])))
	)			/*then */
	{
	  /*^cond.then */
	  /*^getslot */
	  {
	    melt_ptr_t slot = NULL, obj = NULL;
	    obj =
	      (melt_ptr_t) (( /*!BUFFER_LIMIT_CONT */ meltfrout->
			     tabval[1])) /*=obj*/ ;
	    melt_object_get_field (slot, obj, 0, "REFERENCED_VALUE");
     /*_.REFERENCED_VALUE__V12*/ meltfptr[11] = slot;
	  };
	  ;
	}
      else
	{			/*^cond.else */

    /*_.REFERENCED_VALUE__V12*/ meltfptr[11] = NULL;;
	}
      ;
      /*^compute */
   /*_#GET_INT__L5*/ meltfnum[4] =
	(melt_get_int
	 ((melt_ptr_t) ( /*_.REFERENCED_VALUE__V12*/ meltfptr[11])));;
      /*^compute */
   /*_#I__L6*/ meltfnum[5] =
	(( /*_#STRBUF_USEDLENGTH__L4*/ meltfnum[3]) <
	 ( /*_#GET_INT__L5*/ meltfnum[4]));;
      MELT_LOCATION ("warmelt-outobj.melt:2889:/ cond");
      /*cond */ if ( /*_#I__L6*/ meltfnum[5])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V13*/ meltfptr[12] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-outobj.melt:2889:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check limited sbuf"),
				  ("warmelt-outobj.melt")
				  ? ("warmelt-outobj.melt") : __FILE__,
				  (2889) ? (2889) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V13*/ meltfptr[12] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V11*/ meltfptr[10] = /*_.IFELSE___V13*/ meltfptr[12];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:2889:/ clear");
	     /*clear *//*_#STRBUF_USEDLENGTH__L4*/ meltfnum[3] = 0;
      /*^clear */
	     /*clear *//*_.REFERENCED_VALUE__V12*/ meltfptr[11] = 0;
      /*^clear */
	     /*clear *//*_#GET_INT__L5*/ meltfnum[4] = 0;
      /*^clear */
	     /*clear *//*_#I__L6*/ meltfnum[5] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V13*/ meltfptr[12] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V11*/ meltfptr[10] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    /*^compute */
    /*_.LET___V8*/ meltfptr[6] = /*_.IFCPP___V11*/ meltfptr[10];;

    MELT_LOCATION ("warmelt-outobj.melt:2884:/ clear");
	   /*clear *//*_.CNAM__V9*/ meltfptr[8] = 0;
    /*^clear */
	   /*clear *//*_.CSYM_URANK__V10*/ meltfptr[9] = 0;
    /*^clear */
	   /*clear *//*_#RK__L3*/ meltfnum[0] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V11*/ meltfptr[10] = 0;
    MELT_LOCATION ("warmelt-outobj.melt:2881:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.LET___V8*/ meltfptr[6];;

    {
      MELT_LOCATION ("warmelt-outobj.melt:2881:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V4*/ meltfptr[3] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V6*/ meltfptr[4] = 0;
    /*^clear */
	   /*clear *//*_.LET___V8*/ meltfptr[6] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("ADD2SBUF_CLONSYM", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_62_warmelt_outobj_ADD2SBUF_CLONSYM_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_62_warmelt_outobj_ADD2SBUF_CLONSYM */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_63_warmelt_outobj_OUTPUCOD_OBJLOOP (meltclosure_ptr_t meltclosp_,
					     melt_ptr_t meltfirstargp_,
					     const melt_argdescr_cell_t
					     meltxargdescr_[],
					     union meltparam_un *
					     meltxargtab_,
					     const melt_argdescr_cell_t
					     meltxresdescr_[],
					     union meltparam_un *
					     meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_63_warmelt_outobj_OUTPUCOD_OBJLOOP_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_63_warmelt_outobj_OUTPUCOD_OBJLOOP_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 33
    melt_ptr_t mcfr_varptr[33];
#define MELTFRAM_NBVARNUM 13
    long mcfr_varnum[13];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_63_warmelt_outobj_OUTPUCOD_OBJLOOP is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_63_warmelt_outobj_OUTPUCOD_OBJLOOP_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 33; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_63_warmelt_outobj_OUTPUCOD_OBJLOOP nbval 33*/
  meltfram__.mcfr_nbvar = 33 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("OUTPUCOD_OBJLOOP", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-outobj.melt:2894:/ getarg");
 /*_.OBLO__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.DECLBUF__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.DECLBUF__V3*/ meltfptr[2])) !=
	      NULL);


  /*getarg#2 */
  /*^getarg */
  if (meltxargdescr_[1] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.IMPLBUF__V4*/ meltfptr[3] =
    (meltxargtab_[1].meltbp_aptr) ? (*(meltxargtab_[1].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3])) !=
	      NULL);


  /*getarg#3 */
  /*^getarg */
  if (meltxargdescr_[2] != MELTBPAR_LONG)
    goto lab_endgetargs;
 /*_#DEPTH__L1*/ meltfnum[0] = meltxargtab_[2].meltbp_long;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:2895:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L2*/ meltfnum[1] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.OBLO__V2*/ meltfptr[1]),
			     (melt_ptr_t) (( /*!CLASS_OBJLOOP */ meltfrout->
					    tabval[0])));;
      MELT_LOCATION ("warmelt-outobj.melt:2895:/ cond");
      /*cond */ if ( /*_#IS_A__L2*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V6*/ meltfptr[5] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-outobj.melt:2895:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check oblo"),
				  ("warmelt-outobj.melt")
				  ? ("warmelt-outobj.melt") : __FILE__,
				  (2895) ? (2895) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V6*/ meltfptr[5] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V5*/ meltfptr[4] = /*_.IFELSE___V6*/ meltfptr[5];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:2895:/ clear");
	     /*clear *//*_#IS_A__L2*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V6*/ meltfptr[5] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V5*/ meltfptr[4] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-outobj.melt:2896:/ quasiblock");


    /*^getslot */
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.OBLO__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 1, "OBLO_BODYL");
  /*_.BODYL__V8*/ meltfptr[7] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-outobj.melt:2897:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.OBLO__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 2, "OBLO_EPIL");
  /*_.EPIL__V9*/ meltfptr[8] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-outobj.melt:2898:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.OBLO__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 3, "OBLOOP_LABEL");
  /*_.LAB__V10*/ meltfptr[9] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-outobj.melt:2899:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.OBLO__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 0, "OBI_LOC");
  /*_.OLOC__V11*/ meltfptr[10] = slot;
    };
    ;
 /*_#I__L3*/ meltfnum[1] =
      (( /*_#DEPTH__L1*/ meltfnum[0]) + (1));;
    /*^compute */
 /*_.BOXDEPTHP1__V12*/ meltfptr[11] =
      (meltgc_new_int
       ((meltobject_ptr_t) (( /*!DISCR_INTEGER */ meltfrout->tabval[1])),
	( /*_#I__L3*/ meltfnum[1])));;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:2902:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L4*/ meltfnum[3] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.LAB__V10*/ meltfptr[9]),
			     (melt_ptr_t) (( /*!CLASS_CLONED_SYMBOL */
					    meltfrout->tabval[2])));;
      MELT_LOCATION ("warmelt-outobj.melt:2902:/ cond");
      /*cond */ if ( /*_#IS_A__L4*/ meltfnum[3])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V14*/ meltfptr[13] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-outobj.melt:2902:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check lab"),
				  ("warmelt-outobj.melt")
				  ? ("warmelt-outobj.melt") : __FILE__,
				  (2902) ? (2902) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V14*/ meltfptr[13] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V13*/ meltfptr[12] = /*_.IFELSE___V14*/ meltfptr[13];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:2902:/ clear");
	     /*clear *//*_#IS_A__L4*/ meltfnum[3] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V14*/ meltfptr[13] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V13*/ meltfptr[12] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-outobj.melt:2903:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[3];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.IMPLBUF__V4*/ meltfptr[3];
      /*^apply.arg */
      argtab[1].meltbp_long = /*_#DEPTH__L1*/ meltfnum[0];
      /*^apply.arg */
      argtab[2].meltbp_cstring = "loop";
      /*_.OUTPUT_LOCATION__V15*/ meltfptr[13] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!OUTPUT_LOCATION */ meltfrout->tabval[3])),
		    (melt_ptr_t) ( /*_.OLOC__V11*/ meltfptr[10]),
		    (MELTBPARSTR_PTR MELTBPARSTR_LONG MELTBPARSTR_CSTRING ""),
		    argtab, "", (union meltparam_un *) 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:2904:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   ("/*loop*/{ labloop_"));
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:2905:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.LAB__V10*/ meltfptr[9];
      /*_.ADD2SBUF_CLONSYM__V16*/ meltfptr[15] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!ADD2SBUF_CLONSYM */ meltfrout->tabval[4])),
		    (melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
		    (MELTBPARSTR_PTR ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:2906:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   (":;"));
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:2907:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#IS_LIST__L5*/ meltfnum[3] =
      (melt_magic_discr ((melt_ptr_t) ( /*_.BODYL__V8*/ meltfptr[7])) ==
       MELTOBMAG_LIST);;
    MELT_LOCATION ("warmelt-outobj.melt:2907:/ cond");
    /*cond */ if ( /*_#IS_LIST__L5*/ meltfnum[3])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-outobj.melt:2909:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^apply */
	  /*apply */
	  {
	    union meltparam_un argtab[3];
	    memset (&argtab, 0, sizeof (argtab));
	    /*^apply.arg */
	    argtab[0].meltbp_aptr =
	      (melt_ptr_t *) & /*_.IMPLBUF__V4*/ meltfptr[3];
	    /*^apply.arg */
	    argtab[1].meltbp_long = /*_#DEPTH__L1*/ meltfnum[0];
	    /*^apply.arg */
	    argtab[2].meltbp_cstring = "loopbody";
	    /*_.OUTPUT_LOCATION__V18*/ meltfptr[17] =
	      melt_apply ((meltclosure_ptr_t)
			  (( /*!OUTPUT_LOCATION */ meltfrout->tabval[3])),
			  (melt_ptr_t) ( /*_.OLOC__V11*/ meltfptr[10]),
			  (MELTBPARSTR_PTR MELTBPARSTR_LONG
			   MELTBPARSTR_CSTRING ""), argtab, "",
			  (union meltparam_un *) 0);
	  }
	  ;
   /*_#I__L6*/ meltfnum[5] =
	    (( /*_#DEPTH__L1*/ meltfnum[0]) + (1));;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:2910:/ locexp");
	    meltgc_strbuf_add_indent ((melt_ptr_t)
				      ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				      ( /*_#I__L6*/ meltfnum[5]), 0);
	  }
	  ;
	  MELT_LOCATION ("warmelt-outobj.melt:2913:/ quasiblock");


	  /*^newclosure */
		   /*newclosure *//*_.LAMBDA___V20*/ meltfptr[19] =
	    (melt_ptr_t)
	    meltgc_new_closure ((meltobject_ptr_t)
				(((melt_ptr_t)
				  (MELT_PREDEF (DISCR_CLOSURE)))),
				(meltroutine_ptr_t) (( /*!konst_8 */
						      meltfrout->tabval[8])),
				(3));
	  ;
	  /*^putclosedv */
	  /*putclosv */
	  melt_assertmsg ("putclosv checkclo",
			  melt_magic_discr ((melt_ptr_t)
					    ( /*_.LAMBDA___V20*/
					     meltfptr[19])) ==
			  MELTOBMAG_CLOSURE);
	  melt_assertmsg ("putclosv checkoff", 0 >= 0
			  && 0 <
			  melt_closure_size ((melt_ptr_t)
					     ( /*_.LAMBDA___V20*/
					      meltfptr[19])));
	  ((meltclosure_ptr_t) /*_.LAMBDA___V20*/ meltfptr[19])->tabval[0] =
	    (melt_ptr_t) ( /*_.BOXDEPTHP1__V12*/ meltfptr[11]);
	  ;
	  /*^putclosedv */
	  /*putclosv */
	  melt_assertmsg ("putclosv checkclo",
			  melt_magic_discr ((melt_ptr_t)
					    ( /*_.LAMBDA___V20*/
					     meltfptr[19])) ==
			  MELTOBMAG_CLOSURE);
	  melt_assertmsg ("putclosv checkoff", 1 >= 0
			  && 1 <
			  melt_closure_size ((melt_ptr_t)
					     ( /*_.LAMBDA___V20*/
					      meltfptr[19])));
	  ((meltclosure_ptr_t) /*_.LAMBDA___V20*/ meltfptr[19])->tabval[1] =
	    (melt_ptr_t) ( /*_.DECLBUF__V3*/ meltfptr[2]);
	  ;
	  /*^putclosedv */
	  /*putclosv */
	  melt_assertmsg ("putclosv checkclo",
			  melt_magic_discr ((melt_ptr_t)
					    ( /*_.LAMBDA___V20*/
					     meltfptr[19])) ==
			  MELTOBMAG_CLOSURE);
	  melt_assertmsg ("putclosv checkoff", 2 >= 0
			  && 2 <
			  melt_closure_size ((melt_ptr_t)
					     ( /*_.LAMBDA___V20*/
					      meltfptr[19])));
	  ((meltclosure_ptr_t) /*_.LAMBDA___V20*/ meltfptr[19])->tabval[2] =
	    (melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]);
	  ;
	  /*_.LAMBDA___V19*/ meltfptr[18] = /*_.LAMBDA___V20*/ meltfptr[19];;
	  MELT_LOCATION ("warmelt-outobj.melt:2911:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^apply */
	  /*apply */
	  {
	    union meltparam_un argtab[1];
	    memset (&argtab, 0, sizeof (argtab));
	    /*^apply.arg */
	    argtab[0].meltbp_aptr =
	      (melt_ptr_t *) & /*_.LAMBDA___V19*/ meltfptr[18];
	    /*_.LIST_EVERY__V21*/ meltfptr[20] =
	      melt_apply ((meltclosure_ptr_t)
			  (( /*!LIST_EVERY */ meltfrout->tabval[5])),
			  (melt_ptr_t) ( /*_.BODYL__V8*/ meltfptr[7]),
			  (MELTBPARSTR_PTR ""), argtab, "",
			  (union meltparam_un *) 0);
	  }
	  ;
	  MELT_LOCATION ("warmelt-outobj.melt:2908:/ quasiblock");


	  /*_.PROGN___V22*/ meltfptr[21] =
	    /*_.LIST_EVERY__V21*/ meltfptr[20];;
	  /*^compute */
	  /*_.IF___V17*/ meltfptr[16] = /*_.PROGN___V22*/ meltfptr[21];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-outobj.melt:2907:/ clear");
	     /*clear *//*_.OUTPUT_LOCATION__V18*/ meltfptr[17] = 0;
	  /*^clear */
	     /*clear *//*_#I__L6*/ meltfnum[5] = 0;
	  /*^clear */
	     /*clear *//*_.LAMBDA___V19*/ meltfptr[18] = 0;
	  /*^clear */
	     /*clear *//*_.LIST_EVERY__V21*/ meltfptr[20] = 0;
	  /*^clear */
	     /*clear *//*_.PROGN___V22*/ meltfptr[21] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

  /*_.IF___V17*/ meltfptr[16] = NULL;;
      }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:2919:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   (";"));
    }
    ;
 /*_#I__L7*/ meltfnum[5] =
      (( /*_#DEPTH__L1*/ meltfnum[0]) + (1));;

    {
      MELT_LOCATION ("warmelt-outobj.melt:2920:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				( /*_#I__L7*/ meltfnum[5]), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:2921:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   (" goto labloop_"));
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:2922:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.LAB__V10*/ meltfptr[9];
      /*_.ADD2SBUF_CLONSYM__V23*/ meltfptr[17] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!ADD2SBUF_CLONSYM */ meltfrout->tabval[4])),
		    (melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
		    (MELTBPARSTR_PTR ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:2923:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   (";"));
    }
    ;
 /*_#I__L8*/ meltfnum[7] =
      (( /*_#DEPTH__L1*/ meltfnum[0]) + (1));;

    {
      MELT_LOCATION ("warmelt-outobj.melt:2924:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				( /*_#I__L8*/ meltfnum[7]), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:2925:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   (" labexit_"));
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:2926:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.LAB__V10*/ meltfptr[9];
      /*_.ADD2SBUF_CLONSYM__V24*/ meltfptr[18] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!ADD2SBUF_CLONSYM */ meltfrout->tabval[4])),
		    (melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
		    (MELTBPARSTR_PTR ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:2927:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   (":;"));
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:2928:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#IS_LIST__L9*/ meltfnum[8] =
      (melt_magic_discr ((melt_ptr_t) ( /*_.EPIL__V9*/ meltfptr[8])) ==
       MELTOBMAG_LIST);;
    MELT_LOCATION ("warmelt-outobj.melt:2928:/ cond");
    /*cond */ if ( /*_#IS_LIST__L9*/ meltfnum[8])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-outobj.melt:2930:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^apply */
	  /*apply */
	  {
	    union meltparam_un argtab[3];
	    memset (&argtab, 0, sizeof (argtab));
	    /*^apply.arg */
	    argtab[0].meltbp_aptr =
	      (melt_ptr_t *) & /*_.IMPLBUF__V4*/ meltfptr[3];
	    /*^apply.arg */
	    argtab[1].meltbp_long = /*_#DEPTH__L1*/ meltfnum[0];
	    /*^apply.arg */
	    argtab[2].meltbp_cstring = "loopepilog";
	    /*_.OUTPUT_LOCATION__V26*/ meltfptr[21] =
	      melt_apply ((meltclosure_ptr_t)
			  (( /*!OUTPUT_LOCATION */ meltfrout->tabval[3])),
			  (melt_ptr_t) ( /*_.OLOC__V11*/ meltfptr[10]),
			  (MELTBPARSTR_PTR MELTBPARSTR_LONG
			   MELTBPARSTR_CSTRING ""), argtab, "",
			  (union meltparam_un *) 0);
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:2931:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t)
				 ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				 ("/*loopepilog*/"));
	  }
	  ;
   /*_#I__L10*/ meltfnum[9] =
	    (( /*_#DEPTH__L1*/ meltfnum[0]) + (1));;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:2932:/ locexp");
	    meltgc_strbuf_add_indent ((melt_ptr_t)
				      ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				      ( /*_#I__L10*/ meltfnum[9]), 0);
	  }
	  ;
	  MELT_LOCATION ("warmelt-outobj.melt:2935:/ quasiblock");


	  /*^newclosure */
		   /*newclosure *//*_.LAMBDA___V28*/ meltfptr[27] =
	    (melt_ptr_t)
	    meltgc_new_closure ((meltobject_ptr_t)
				(((melt_ptr_t)
				  (MELT_PREDEF (DISCR_CLOSURE)))),
				(meltroutine_ptr_t) (( /*!konst_13 */
						      meltfrout->tabval[13])),
				(3));
	  ;
	  /*^putclosedv */
	  /*putclosv */
	  melt_assertmsg ("putclosv checkclo",
			  melt_magic_discr ((melt_ptr_t)
					    ( /*_.LAMBDA___V28*/
					     meltfptr[27])) ==
			  MELTOBMAG_CLOSURE);
	  melt_assertmsg ("putclosv checkoff", 0 >= 0
			  && 0 <
			  melt_closure_size ((melt_ptr_t)
					     ( /*_.LAMBDA___V28*/
					      meltfptr[27])));
	  ((meltclosure_ptr_t) /*_.LAMBDA___V28*/ meltfptr[27])->tabval[0] =
	    (melt_ptr_t) ( /*_.BOXDEPTHP1__V12*/ meltfptr[11]);
	  ;
	  /*^putclosedv */
	  /*putclosv */
	  melt_assertmsg ("putclosv checkclo",
			  melt_magic_discr ((melt_ptr_t)
					    ( /*_.LAMBDA___V28*/
					     meltfptr[27])) ==
			  MELTOBMAG_CLOSURE);
	  melt_assertmsg ("putclosv checkoff", 1 >= 0
			  && 1 <
			  melt_closure_size ((melt_ptr_t)
					     ( /*_.LAMBDA___V28*/
					      meltfptr[27])));
	  ((meltclosure_ptr_t) /*_.LAMBDA___V28*/ meltfptr[27])->tabval[1] =
	    (melt_ptr_t) ( /*_.DECLBUF__V3*/ meltfptr[2]);
	  ;
	  /*^putclosedv */
	  /*putclosv */
	  melt_assertmsg ("putclosv checkclo",
			  melt_magic_discr ((melt_ptr_t)
					    ( /*_.LAMBDA___V28*/
					     meltfptr[27])) ==
			  MELTOBMAG_CLOSURE);
	  melt_assertmsg ("putclosv checkoff", 2 >= 0
			  && 2 <
			  melt_closure_size ((melt_ptr_t)
					     ( /*_.LAMBDA___V28*/
					      meltfptr[27])));
	  ((meltclosure_ptr_t) /*_.LAMBDA___V28*/ meltfptr[27])->tabval[2] =
	    (melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]);
	  ;
	  /*_.LAMBDA___V27*/ meltfptr[26] = /*_.LAMBDA___V28*/ meltfptr[27];;
	  MELT_LOCATION ("warmelt-outobj.melt:2933:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^apply */
	  /*apply */
	  {
	    union meltparam_un argtab[1];
	    memset (&argtab, 0, sizeof (argtab));
	    /*^apply.arg */
	    argtab[0].meltbp_aptr =
	      (melt_ptr_t *) & /*_.LAMBDA___V27*/ meltfptr[26];
	    /*_.LIST_EVERY__V29*/ meltfptr[28] =
	      melt_apply ((meltclosure_ptr_t)
			  (( /*!LIST_EVERY */ meltfrout->tabval[5])),
			  (melt_ptr_t) ( /*_.EPIL__V9*/ meltfptr[8]),
			  (MELTBPARSTR_PTR ""), argtab, "",
			  (union meltparam_un *) 0);
	  }
	  ;
	  MELT_LOCATION ("warmelt-outobj.melt:2929:/ quasiblock");


	  /*_.PROGN___V30*/ meltfptr[29] =
	    /*_.LIST_EVERY__V29*/ meltfptr[28];;
	  /*^compute */
	  /*_.IF___V25*/ meltfptr[20] = /*_.PROGN___V30*/ meltfptr[29];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-outobj.melt:2928:/ clear");
	     /*clear *//*_.OUTPUT_LOCATION__V26*/ meltfptr[21] = 0;
	  /*^clear */
	     /*clear *//*_#I__L10*/ meltfnum[9] = 0;
	  /*^clear */
	     /*clear *//*_.LAMBDA___V27*/ meltfptr[26] = 0;
	  /*^clear */
	     /*clear *//*_.LIST_EVERY__V29*/ meltfptr[28] = 0;
	  /*^clear */
	     /*clear *//*_.PROGN___V30*/ meltfptr[29] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

  /*_.IF___V25*/ meltfptr[20] = NULL;;
      }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:2943:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   ("}"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:2944:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				( /*_#DEPTH__L1*/ meltfnum[0]), 0);
    }
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:2945:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#STRBUF_USEDLENGTH__L11*/ meltfnum[9] =
	melt_strbuf_usedlength ((melt_ptr_t)
				( /*_.IMPLBUF__V4*/ meltfptr[3]));;
      MELT_LOCATION ("warmelt-outobj.melt:2946:/ cond");
      /*cond */ if (
		     /*ifisa */
		     melt_is_instance_of ((melt_ptr_t)
					  (( /*!BUFFER_LIMIT_CONT */
					    meltfrout->tabval[14])),
					  (melt_ptr_t) (( /*!CLASS_REFERENCE */ meltfrout->tabval[15])))
	)			/*then */
	{
	  /*^cond.then */
	  /*^getslot */
	  {
	    melt_ptr_t slot = NULL, obj = NULL;
	    obj =
	      (melt_ptr_t) (( /*!BUFFER_LIMIT_CONT */ meltfrout->
			     tabval[14])) /*=obj*/ ;
	    melt_object_get_field (slot, obj, 0, "REFERENCED_VALUE");
     /*_.REFERENCED_VALUE__V32*/ meltfptr[26] = slot;
	  };
	  ;
	}
      else
	{			/*^cond.else */

    /*_.REFERENCED_VALUE__V32*/ meltfptr[26] = NULL;;
	}
      ;
      /*^compute */
   /*_#GET_INT__L12*/ meltfnum[11] =
	(melt_get_int
	 ((melt_ptr_t) ( /*_.REFERENCED_VALUE__V32*/ meltfptr[26])));;
      /*^compute */
   /*_#I__L13*/ meltfnum[12] =
	(( /*_#STRBUF_USEDLENGTH__L11*/ meltfnum[9]) <
	 ( /*_#GET_INT__L12*/ meltfnum[11]));;
      MELT_LOCATION ("warmelt-outobj.melt:2945:/ cond");
      /*cond */ if ( /*_#I__L13*/ meltfnum[12])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V33*/ meltfptr[28] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-outobj.melt:2945:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check limited implbuf"),
				  ("warmelt-outobj.melt")
				  ? ("warmelt-outobj.melt") : __FILE__,
				  (2945) ? (2945) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V33*/ meltfptr[28] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V31*/ meltfptr[21] = /*_.IFELSE___V33*/ meltfptr[28];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:2945:/ clear");
	     /*clear *//*_#STRBUF_USEDLENGTH__L11*/ meltfnum[9] = 0;
      /*^clear */
	     /*clear *//*_.REFERENCED_VALUE__V32*/ meltfptr[26] = 0;
      /*^clear */
	     /*clear *//*_#GET_INT__L12*/ meltfnum[11] = 0;
      /*^clear */
	     /*clear *//*_#I__L13*/ meltfnum[12] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V33*/ meltfptr[28] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V31*/ meltfptr[21] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    /*^compute */
    /*_.LET___V7*/ meltfptr[5] = /*_.IFCPP___V31*/ meltfptr[21];;

    MELT_LOCATION ("warmelt-outobj.melt:2896:/ clear");
	   /*clear *//*_.BODYL__V8*/ meltfptr[7] = 0;
    /*^clear */
	   /*clear *//*_.EPIL__V9*/ meltfptr[8] = 0;
    /*^clear */
	   /*clear *//*_.LAB__V10*/ meltfptr[9] = 0;
    /*^clear */
	   /*clear *//*_.OLOC__V11*/ meltfptr[10] = 0;
    /*^clear */
	   /*clear *//*_#I__L3*/ meltfnum[1] = 0;
    /*^clear */
	   /*clear *//*_.BOXDEPTHP1__V12*/ meltfptr[11] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V13*/ meltfptr[12] = 0;
    /*^clear */
	   /*clear *//*_.OUTPUT_LOCATION__V15*/ meltfptr[13] = 0;
    /*^clear */
	   /*clear *//*_.ADD2SBUF_CLONSYM__V16*/ meltfptr[15] = 0;
    /*^clear */
	   /*clear *//*_#IS_LIST__L5*/ meltfnum[3] = 0;
    /*^clear */
	   /*clear *//*_.IF___V17*/ meltfptr[16] = 0;
    /*^clear */
	   /*clear *//*_#I__L7*/ meltfnum[5] = 0;
    /*^clear */
	   /*clear *//*_.ADD2SBUF_CLONSYM__V23*/ meltfptr[17] = 0;
    /*^clear */
	   /*clear *//*_#I__L8*/ meltfnum[7] = 0;
    /*^clear */
	   /*clear *//*_.ADD2SBUF_CLONSYM__V24*/ meltfptr[18] = 0;
    /*^clear */
	   /*clear *//*_#IS_LIST__L9*/ meltfnum[8] = 0;
    /*^clear */
	   /*clear *//*_.IF___V25*/ meltfptr[20] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V31*/ meltfptr[21] = 0;
    MELT_LOCATION ("warmelt-outobj.melt:2894:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.LET___V7*/ meltfptr[5];;

    {
      MELT_LOCATION ("warmelt-outobj.melt:2894:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V5*/ meltfptr[4] = 0;
    /*^clear */
	   /*clear *//*_.LET___V7*/ meltfptr[5] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("OUTPUCOD_OBJLOOP", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_63_warmelt_outobj_OUTPUCOD_OBJLOOP_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_63_warmelt_outobj_OUTPUCOD_OBJLOOP */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_64_warmelt_outobj_LAMBDA___6__ (meltclosure_ptr_t meltclosp_,
					 melt_ptr_t meltfirstargp_,
					 const melt_argdescr_cell_t
					 meltxargdescr_[],
					 union meltparam_un * meltxargtab_,
					 const melt_argdescr_cell_t
					 meltxresdescr_[],
					 union meltparam_un * meltxrestab_)
{
  long current_blocklevel_signals_meltrout_64_warmelt_outobj_LAMBDA___6___melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_64_warmelt_outobj_LAMBDA___6___st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 4
    melt_ptr_t mcfr_varptr[4];
#define MELTFRAM_NBVARNUM 4
    long mcfr_varnum[4];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_64_warmelt_outobj_LAMBDA___6__ is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_64_warmelt_outobj_LAMBDA___6___st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 4; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_64_warmelt_outobj_LAMBDA___6__ nbval 4*/
  meltfram__.mcfr_nbvar = 4 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("LAMBDA_", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-outobj.melt:2913:/ getarg");
 /*_.CURBODY__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {

    MELT_LOCATION ("warmelt-outobj.melt:2914:/ quasiblock");


 /*_#DEPTHP1__L1*/ meltfnum[0] =
      (melt_get_int
       ((melt_ptr_t) (( /*~BOXDEPTHP1 */ meltfclos->tabval[0]))));;
    MELT_LOCATION ("warmelt-outobj.melt:2915:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^checksignal */
    MELT_CHECK_SIGNAL ();
    ;
    /*^cond */
    /*cond */ if ( /*_.CURBODY__V2*/ meltfptr[1])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

   /*_#IS_A__L3*/ meltfnum[2] =
	    melt_is_instance_of ((melt_ptr_t)
				 ( /*_.CURBODY__V2*/ meltfptr[1]),
				 (melt_ptr_t) (( /*!CLASS_OBJPUREVALUE */
						meltfrout->tabval[0])));;
	  /*^compute */
   /*_#NOT__L4*/ meltfnum[3] =
	    (!( /*_#IS_A__L3*/ meltfnum[2]));;
	  /*^compute */
	  /*_#IF___L2*/ meltfnum[1] = /*_#NOT__L4*/ meltfnum[3];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-outobj.melt:2915:/ clear");
	     /*clear *//*_#IS_A__L3*/ meltfnum[2] = 0;
	  /*^clear */
	     /*clear *//*_#NOT__L4*/ meltfnum[3] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

  /*_#IF___L2*/ meltfnum[1] = 0;;
      }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:2915:/ cond");
    /*cond */ if ( /*_#IF___L2*/ meltfnum[1])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-outobj.melt:2916:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^msend */
	  /*msend */
	  {
	    union meltparam_un argtab[3];
	    memset (&argtab, 0, sizeof (argtab));
	    /*^ojbmsend.arg */
	    argtab[0].meltbp_aptr =
	      (melt_ptr_t *) & ( /*~DECLBUF */ meltfclos->tabval[1]);
	    /*^ojbmsend.arg */
	    argtab[1].meltbp_aptr =
	      (melt_ptr_t *) & ( /*~IMPLBUF */ meltfclos->tabval[2]);
	    /*^ojbmsend.arg */
	    argtab[2].meltbp_long = /*_#DEPTHP1__L1*/ meltfnum[0];
	    /*_.OUTPUT_C_CODE__V4*/ meltfptr[3] =
	      meltgc_send ((melt_ptr_t) ( /*_.CURBODY__V2*/ meltfptr[1]),
			   (melt_ptr_t) (( /*!OUTPUT_C_CODE */ meltfrout->
					  tabval[1])),
			   (MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_LONG
			    ""), argtab, "", (union meltparam_un *) 0);
	  }
	  ;
	  /*_.IF___V3*/ meltfptr[2] = /*_.OUTPUT_C_CODE__V4*/ meltfptr[3];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-outobj.melt:2915:/ clear");
	     /*clear *//*_.OUTPUT_C_CODE__V4*/ meltfptr[3] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

  /*_.IF___V3*/ meltfptr[2] = NULL;;
      }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:2917:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t)
			   (( /*~IMPLBUF */ meltfclos->tabval[2])), (";"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:2918:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t)
				(( /*~IMPLBUF */ meltfclos->tabval[2])),
				( /*_#DEPTHP1__L1*/ meltfnum[0]), 0);
    }
    ;

    MELT_LOCATION ("warmelt-outobj.melt:2914:/ clear");
	   /*clear *//*_#DEPTHP1__L1*/ meltfnum[0] = 0;
    /*^clear */
	   /*clear *//*_#IF___L2*/ meltfnum[1] = 0;
    /*^clear */
	   /*clear *//*_.IF___V3*/ meltfptr[2] = 0;
    /*epilog */
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("LAMBDA_", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_64_warmelt_outobj_LAMBDA___6___melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*noretval */ NULL);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_64_warmelt_outobj_LAMBDA___6__ */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_65_warmelt_outobj_LAMBDA___7__ (meltclosure_ptr_t meltclosp_,
					 melt_ptr_t meltfirstargp_,
					 const melt_argdescr_cell_t
					 meltxargdescr_[],
					 union meltparam_un * meltxargtab_,
					 const melt_argdescr_cell_t
					 meltxresdescr_[],
					 union meltparam_un * meltxrestab_)
{
  long current_blocklevel_signals_meltrout_65_warmelt_outobj_LAMBDA___7___melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_65_warmelt_outobj_LAMBDA___7___st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 7
    melt_ptr_t mcfr_varptr[7];
#define MELTFRAM_NBVARNUM 7
    long mcfr_varnum[7];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_65_warmelt_outobj_LAMBDA___7__ is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_65_warmelt_outobj_LAMBDA___7___st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 7; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_65_warmelt_outobj_LAMBDA___7__ nbval 7*/
  meltfram__.mcfr_nbvar = 7 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("LAMBDA_", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-outobj.melt:2935:/ getarg");
 /*_.CUREPIL__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {

    MELT_LOCATION ("warmelt-outobj.melt:2936:/ quasiblock");


 /*_#DEPTHP1__L1*/ meltfnum[0] =
      (melt_get_int
       ((melt_ptr_t) (( /*~BOXDEPTHP1 */ meltfclos->tabval[0]))));;
    MELT_LOCATION ("warmelt-outobj.melt:2937:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^checksignal */
    MELT_CHECK_SIGNAL ();
    ;
    /*^cond */
    /*cond */ if ( /*_.CUREPIL__V2*/ meltfptr[1])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

   /*_#IS_A__L3*/ meltfnum[2] =
	    melt_is_instance_of ((melt_ptr_t)
				 ( /*_.CUREPIL__V2*/ meltfptr[1]),
				 (melt_ptr_t) (( /*!CLASS_OBJPUREVALUE */
						meltfrout->tabval[0])));;
	  /*^compute */
   /*_#NOT__L4*/ meltfnum[3] =
	    (!( /*_#IS_A__L3*/ meltfnum[2]));;
	  /*^compute */
	  /*_#IF___L2*/ meltfnum[1] = /*_#NOT__L4*/ meltfnum[3];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-outobj.melt:2937:/ clear");
	     /*clear *//*_#IS_A__L3*/ meltfnum[2] = 0;
	  /*^clear */
	     /*clear *//*_#NOT__L4*/ meltfnum[3] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

  /*_#IF___L2*/ meltfnum[1] = 0;;
      }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:2937:/ cond");
    /*cond */ if ( /*_#IF___L2*/ meltfnum[1])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-outobj.melt:2938:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^msend */
	  /*msend */
	  {
	    union meltparam_un argtab[3];
	    memset (&argtab, 0, sizeof (argtab));
	    /*^ojbmsend.arg */
	    argtab[0].meltbp_aptr =
	      (melt_ptr_t *) & ( /*~DECLBUF */ meltfclos->tabval[1]);
	    /*^ojbmsend.arg */
	    argtab[1].meltbp_aptr =
	      (melt_ptr_t *) & ( /*~IMPLBUF */ meltfclos->tabval[2]);
	    /*^ojbmsend.arg */
	    argtab[2].meltbp_long = /*_#DEPTHP1__L1*/ meltfnum[0];
	    /*_.OUTPUT_C_CODE__V4*/ meltfptr[3] =
	      meltgc_send ((melt_ptr_t) ( /*_.CUREPIL__V2*/ meltfptr[1]),
			   (melt_ptr_t) (( /*!OUTPUT_C_CODE */ meltfrout->
					  tabval[1])),
			   (MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_LONG
			    ""), argtab, "", (union meltparam_un *) 0);
	  }
	  ;
	  /*_.IF___V3*/ meltfptr[2] = /*_.OUTPUT_C_CODE__V4*/ meltfptr[3];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-outobj.melt:2937:/ clear");
	     /*clear *//*_.OUTPUT_C_CODE__V4*/ meltfptr[3] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

  /*_.IF___V3*/ meltfptr[2] = NULL;;
      }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:2939:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t)
			   (( /*~IMPLBUF */ meltfclos->tabval[2])), (";"));
    }
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:2940:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#STRBUF_USEDLENGTH__L5*/ meltfnum[2] =
	melt_strbuf_usedlength ((melt_ptr_t)
				(( /*~IMPLBUF */ meltfclos->tabval[2])));;
      MELT_LOCATION ("warmelt-outobj.melt:2941:/ cond");
      /*cond */ if (
		     /*ifisa */
		     melt_is_instance_of ((melt_ptr_t)
					  (( /*!BUFFER_LIMIT_CONT */
					    meltfrout->tabval[2])),
					  (melt_ptr_t) (( /*!CLASS_REFERENCE */ meltfrout->tabval[3])))
	)			/*then */
	{
	  /*^cond.then */
	  /*^getslot */
	  {
	    melt_ptr_t slot = NULL, obj = NULL;
	    obj =
	      (melt_ptr_t) (( /*!BUFFER_LIMIT_CONT */ meltfrout->
			     tabval[2])) /*=obj*/ ;
	    melt_object_get_field (slot, obj, 0, "REFERENCED_VALUE");
     /*_.REFERENCED_VALUE__V6*/ meltfptr[5] = slot;
	  };
	  ;
	}
      else
	{			/*^cond.else */

    /*_.REFERENCED_VALUE__V6*/ meltfptr[5] = NULL;;
	}
      ;
      /*^compute */
   /*_#GET_INT__L6*/ meltfnum[3] =
	(melt_get_int
	 ((melt_ptr_t) ( /*_.REFERENCED_VALUE__V6*/ meltfptr[5])));;
      /*^compute */
   /*_#I__L7*/ meltfnum[6] =
	(( /*_#STRBUF_USEDLENGTH__L5*/ meltfnum[2]) <
	 ( /*_#GET_INT__L6*/ meltfnum[3]));;
      MELT_LOCATION ("warmelt-outobj.melt:2940:/ cond");
      /*cond */ if ( /*_#I__L7*/ meltfnum[6])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V7*/ meltfptr[6] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-outobj.melt:2940:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check limited implbuf"),
				  ("warmelt-outobj.melt")
				  ? ("warmelt-outobj.melt") : __FILE__,
				  (2940) ? (2940) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V7*/ meltfptr[6] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V5*/ meltfptr[3] = /*_.IFELSE___V7*/ meltfptr[6];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:2940:/ clear");
	     /*clear *//*_#STRBUF_USEDLENGTH__L5*/ meltfnum[2] = 0;
      /*^clear */
	     /*clear *//*_.REFERENCED_VALUE__V6*/ meltfptr[5] = 0;
      /*^clear */
	     /*clear *//*_#GET_INT__L6*/ meltfnum[3] = 0;
      /*^clear */
	     /*clear *//*_#I__L7*/ meltfnum[6] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V7*/ meltfptr[6] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V5*/ meltfptr[3] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:2942:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t)
				(( /*~IMPLBUF */ meltfclos->tabval[2])),
				( /*_#DEPTHP1__L1*/ meltfnum[0]), 0);
    }
    ;

    MELT_LOCATION ("warmelt-outobj.melt:2936:/ clear");
	   /*clear *//*_#DEPTHP1__L1*/ meltfnum[0] = 0;
    /*^clear */
	   /*clear *//*_#IF___L2*/ meltfnum[1] = 0;
    /*^clear */
	   /*clear *//*_.IF___V3*/ meltfptr[2] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V5*/ meltfptr[3] = 0;
    /*epilog */
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("LAMBDA_", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_65_warmelt_outobj_LAMBDA___7___melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*noretval */ NULL);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_65_warmelt_outobj_LAMBDA___7__ */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_66_warmelt_outobj_OUTPUCOD_OBJEXIT (meltclosure_ptr_t meltclosp_,
					     melt_ptr_t meltfirstargp_,
					     const melt_argdescr_cell_t
					     meltxargdescr_[],
					     union meltparam_un *
					     meltxargtab_,
					     const melt_argdescr_cell_t
					     meltxresdescr_[],
					     union meltparam_un *
					     meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_66_warmelt_outobj_OUTPUCOD_OBJEXIT_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_66_warmelt_outobj_OUTPUCOD_OBJEXIT_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 16
    melt_ptr_t mcfr_varptr[16];
#define MELTFRAM_NBVARNUM 6
    long mcfr_varnum[6];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_66_warmelt_outobj_OUTPUCOD_OBJEXIT is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_66_warmelt_outobj_OUTPUCOD_OBJEXIT_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 16; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_66_warmelt_outobj_OUTPUCOD_OBJEXIT nbval 16*/
  meltfram__.mcfr_nbvar = 16 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("OUTPUCOD_OBJEXIT", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-outobj.melt:2953:/ getarg");
 /*_.OBXI__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.DECLBUF__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.DECLBUF__V3*/ meltfptr[2])) !=
	      NULL);


  /*getarg#2 */
  /*^getarg */
  if (meltxargdescr_[1] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.IMPLBUF__V4*/ meltfptr[3] =
    (meltxargtab_[1].meltbp_aptr) ? (*(meltxargtab_[1].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3])) !=
	      NULL);


  /*getarg#3 */
  /*^getarg */
  if (meltxargdescr_[2] != MELTBPAR_LONG)
    goto lab_endgetargs;
 /*_#DEPTH__L1*/ meltfnum[0] = meltxargtab_[2].meltbp_long;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:2954:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L2*/ meltfnum[1] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.OBXI__V2*/ meltfptr[1]),
			     (melt_ptr_t) (( /*!CLASS_OBJEXIT */ meltfrout->
					    tabval[0])));;
      MELT_LOCATION ("warmelt-outobj.melt:2954:/ cond");
      /*cond */ if ( /*_#IS_A__L2*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V6*/ meltfptr[5] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-outobj.melt:2954:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check obxi"),
				  ("warmelt-outobj.melt")
				  ? ("warmelt-outobj.melt") : __FILE__,
				  (2954) ? (2954) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V6*/ meltfptr[5] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V5*/ meltfptr[4] = /*_.IFELSE___V6*/ meltfptr[5];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:2954:/ clear");
	     /*clear *//*_#IS_A__L2*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V6*/ meltfptr[5] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V5*/ meltfptr[4] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-outobj.melt:2955:/ quasiblock");


    /*^getslot */
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.OBXI__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 1, "OBEXIT_LABEL");
  /*_.OLAB__V8*/ meltfptr[7] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-outobj.melt:2956:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.OBXI__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 0, "OBI_LOC");
  /*_.LOC__V9*/ meltfptr[8] = slot;
    };
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:2958:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L3*/ meltfnum[1] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.OLAB__V8*/ meltfptr[7]),
			     (melt_ptr_t) (( /*!CLASS_CLONED_SYMBOL */
					    meltfrout->tabval[1])));;
      MELT_LOCATION ("warmelt-outobj.melt:2958:/ cond");
      /*cond */ if ( /*_#IS_A__L3*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V11*/ meltfptr[10] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-outobj.melt:2958:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check olab"),
				  ("warmelt-outobj.melt")
				  ? ("warmelt-outobj.melt") : __FILE__,
				  (2958) ? (2958) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V11*/ meltfptr[10] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V10*/ meltfptr[9] = /*_.IFELSE___V11*/ meltfptr[10];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:2958:/ clear");
	     /*clear *//*_#IS_A__L3*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V11*/ meltfptr[10] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V10*/ meltfptr[9] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-outobj.melt:2959:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[3];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.IMPLBUF__V4*/ meltfptr[3];
      /*^apply.arg */
      argtab[1].meltbp_long = /*_#DEPTH__L1*/ meltfnum[0];
      /*^apply.arg */
      argtab[2].meltbp_cstring = "exit";
      /*_.OUTPUT_LOCATION__V12*/ meltfptr[10] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!OUTPUT_LOCATION */ meltfrout->tabval[2])),
		    (melt_ptr_t) ( /*_.LOC__V9*/ meltfptr[8]),
		    (MELTBPARSTR_PTR MELTBPARSTR_LONG MELTBPARSTR_CSTRING ""),
		    argtab, "", (union meltparam_un *) 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:2960:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   ("/*exit*/{"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:2961:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				( /*_#DEPTH__L1*/ meltfnum[0]), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:2962:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   (" goto labexit_"));
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:2963:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.OLAB__V8*/ meltfptr[7];
      /*_.ADD2SBUF_CLONSYM__V13*/ meltfptr[12] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!ADD2SBUF_CLONSYM */ meltfrout->tabval[3])),
		    (melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
		    (MELTBPARSTR_PTR ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:2964:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   (";}"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:2965:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				( /*_#DEPTH__L1*/ meltfnum[0]), 0);
    }
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:2966:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#STRBUF_USEDLENGTH__L4*/ meltfnum[1] =
	melt_strbuf_usedlength ((melt_ptr_t)
				( /*_.IMPLBUF__V4*/ meltfptr[3]));;
      MELT_LOCATION ("warmelt-outobj.melt:2967:/ cond");
      /*cond */ if (
		     /*ifisa */
		     melt_is_instance_of ((melt_ptr_t)
					  (( /*!BUFFER_LIMIT_CONT */
					    meltfrout->tabval[4])),
					  (melt_ptr_t) (( /*!CLASS_REFERENCE */ meltfrout->tabval[5])))
	)			/*then */
	{
	  /*^cond.then */
	  /*^getslot */
	  {
	    melt_ptr_t slot = NULL, obj = NULL;
	    obj =
	      (melt_ptr_t) (( /*!BUFFER_LIMIT_CONT */ meltfrout->
			     tabval[4])) /*=obj*/ ;
	    melt_object_get_field (slot, obj, 0, "REFERENCED_VALUE");
     /*_.REFERENCED_VALUE__V15*/ meltfptr[14] = slot;
	  };
	  ;
	}
      else
	{			/*^cond.else */

    /*_.REFERENCED_VALUE__V15*/ meltfptr[14] = NULL;;
	}
      ;
      /*^compute */
   /*_#GET_INT__L5*/ meltfnum[4] =
	(melt_get_int
	 ((melt_ptr_t) ( /*_.REFERENCED_VALUE__V15*/ meltfptr[14])));;
      /*^compute */
   /*_#I__L6*/ meltfnum[5] =
	(( /*_#STRBUF_USEDLENGTH__L4*/ meltfnum[1]) <
	 ( /*_#GET_INT__L5*/ meltfnum[4]));;
      MELT_LOCATION ("warmelt-outobj.melt:2966:/ cond");
      /*cond */ if ( /*_#I__L6*/ meltfnum[5])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V16*/ meltfptr[15] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-outobj.melt:2966:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check limited implbuf"),
				  ("warmelt-outobj.melt")
				  ? ("warmelt-outobj.melt") : __FILE__,
				  (2966) ? (2966) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V16*/ meltfptr[15] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V14*/ meltfptr[13] = /*_.IFELSE___V16*/ meltfptr[15];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:2966:/ clear");
	     /*clear *//*_#STRBUF_USEDLENGTH__L4*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.REFERENCED_VALUE__V15*/ meltfptr[14] = 0;
      /*^clear */
	     /*clear *//*_#GET_INT__L5*/ meltfnum[4] = 0;
      /*^clear */
	     /*clear *//*_#I__L6*/ meltfnum[5] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V16*/ meltfptr[15] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V14*/ meltfptr[13] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    /*^compute */
    /*_.LET___V7*/ meltfptr[5] = /*_.IFCPP___V14*/ meltfptr[13];;

    MELT_LOCATION ("warmelt-outobj.melt:2955:/ clear");
	   /*clear *//*_.OLAB__V8*/ meltfptr[7] = 0;
    /*^clear */
	   /*clear *//*_.LOC__V9*/ meltfptr[8] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V10*/ meltfptr[9] = 0;
    /*^clear */
	   /*clear *//*_.OUTPUT_LOCATION__V12*/ meltfptr[10] = 0;
    /*^clear */
	   /*clear *//*_.ADD2SBUF_CLONSYM__V13*/ meltfptr[12] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V14*/ meltfptr[13] = 0;
    MELT_LOCATION ("warmelt-outobj.melt:2953:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.LET___V7*/ meltfptr[5];;

    {
      MELT_LOCATION ("warmelt-outobj.melt:2953:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V5*/ meltfptr[4] = 0;
    /*^clear */
	   /*clear *//*_.LET___V7*/ meltfptr[5] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("OUTPUCOD_OBJEXIT", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_66_warmelt_outobj_OUTPUCOD_OBJEXIT_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_66_warmelt_outobj_OUTPUCOD_OBJEXIT */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_67_warmelt_outobj_OUTPUCOD_OBJAGAIN (meltclosure_ptr_t meltclosp_,
					      melt_ptr_t meltfirstargp_,
					      const melt_argdescr_cell_t
					      meltxargdescr_[],
					      union meltparam_un *
					      meltxargtab_,
					      const melt_argdescr_cell_t
					      meltxresdescr_[],
					      union meltparam_un *
					      meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_67_warmelt_outobj_OUTPUCOD_OBJAGAIN_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_67_warmelt_outobj_OUTPUCOD_OBJAGAIN_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 16
    melt_ptr_t mcfr_varptr[16];
#define MELTFRAM_NBVARNUM 6
    long mcfr_varnum[6];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_67_warmelt_outobj_OUTPUCOD_OBJAGAIN is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_67_warmelt_outobj_OUTPUCOD_OBJAGAIN_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 16; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_67_warmelt_outobj_OUTPUCOD_OBJAGAIN nbval 16*/
  meltfram__.mcfr_nbvar = 16 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("OUTPUCOD_OBJAGAIN", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-outobj.melt:2973:/ getarg");
 /*_.OBAG__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.DECLBUF__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.DECLBUF__V3*/ meltfptr[2])) !=
	      NULL);


  /*getarg#2 */
  /*^getarg */
  if (meltxargdescr_[1] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.IMPLBUF__V4*/ meltfptr[3] =
    (meltxargtab_[1].meltbp_aptr) ? (*(meltxargtab_[1].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3])) !=
	      NULL);


  /*getarg#3 */
  /*^getarg */
  if (meltxargdescr_[2] != MELTBPAR_LONG)
    goto lab_endgetargs;
 /*_#DEPTH__L1*/ meltfnum[0] = meltxargtab_[2].meltbp_long;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:2974:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L2*/ meltfnum[1] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.OBAG__V2*/ meltfptr[1]),
			     (melt_ptr_t) (( /*!CLASS_OBJAGAIN */ meltfrout->
					    tabval[0])));;
      MELT_LOCATION ("warmelt-outobj.melt:2974:/ cond");
      /*cond */ if ( /*_#IS_A__L2*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V6*/ meltfptr[5] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-outobj.melt:2974:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check obag"),
				  ("warmelt-outobj.melt")
				  ? ("warmelt-outobj.melt") : __FILE__,
				  (2974) ? (2974) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V6*/ meltfptr[5] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V5*/ meltfptr[4] = /*_.IFELSE___V6*/ meltfptr[5];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:2974:/ clear");
	     /*clear *//*_#IS_A__L2*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V6*/ meltfptr[5] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V5*/ meltfptr[4] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-outobj.melt:2975:/ quasiblock");


    /*^getslot */
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.OBAG__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 1, "OBAGAIN_LABEL");
  /*_.OLAB__V8*/ meltfptr[7] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-outobj.melt:2976:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.OBAG__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 0, "OBI_LOC");
  /*_.LOC__V9*/ meltfptr[8] = slot;
    };
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:2978:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L3*/ meltfnum[1] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.OLAB__V8*/ meltfptr[7]),
			     (melt_ptr_t) (( /*!CLASS_CLONED_SYMBOL */
					    meltfrout->tabval[1])));;
      MELT_LOCATION ("warmelt-outobj.melt:2978:/ cond");
      /*cond */ if ( /*_#IS_A__L3*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V11*/ meltfptr[10] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-outobj.melt:2978:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check olab"),
				  ("warmelt-outobj.melt")
				  ? ("warmelt-outobj.melt") : __FILE__,
				  (2978) ? (2978) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V11*/ meltfptr[10] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V10*/ meltfptr[9] = /*_.IFELSE___V11*/ meltfptr[10];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:2978:/ clear");
	     /*clear *//*_#IS_A__L3*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V11*/ meltfptr[10] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V10*/ meltfptr[9] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-outobj.melt:2979:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[3];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.IMPLBUF__V4*/ meltfptr[3];
      /*^apply.arg */
      argtab[1].meltbp_long = /*_#DEPTH__L1*/ meltfnum[0];
      /*^apply.arg */
      argtab[2].meltbp_cstring = "again";
      /*_.OUTPUT_LOCATION__V12*/ meltfptr[10] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!OUTPUT_LOCATION */ meltfrout->tabval[2])),
		    (melt_ptr_t) ( /*_.LOC__V9*/ meltfptr[8]),
		    (MELTBPARSTR_PTR MELTBPARSTR_LONG MELTBPARSTR_CSTRING ""),
		    argtab, "", (union meltparam_un *) 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:2980:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   ("/*again*/{"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:2981:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				( /*_#DEPTH__L1*/ meltfnum[0]), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:2982:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   (" goto labloop_"));
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:2983:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.OLAB__V8*/ meltfptr[7];
      /*_.ADD2SBUF_CLONSYM__V13*/ meltfptr[12] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!ADD2SBUF_CLONSYM */ meltfrout->tabval[3])),
		    (melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
		    (MELTBPARSTR_PTR ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:2984:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   (";}"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:2985:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				( /*_#DEPTH__L1*/ meltfnum[0]), 0);
    }
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:2986:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#STRBUF_USEDLENGTH__L4*/ meltfnum[1] =
	melt_strbuf_usedlength ((melt_ptr_t)
				( /*_.IMPLBUF__V4*/ meltfptr[3]));;
      MELT_LOCATION ("warmelt-outobj.melt:2987:/ cond");
      /*cond */ if (
		     /*ifisa */
		     melt_is_instance_of ((melt_ptr_t)
					  (( /*!BUFFER_LIMIT_CONT */
					    meltfrout->tabval[4])),
					  (melt_ptr_t) (( /*!CLASS_REFERENCE */ meltfrout->tabval[5])))
	)			/*then */
	{
	  /*^cond.then */
	  /*^getslot */
	  {
	    melt_ptr_t slot = NULL, obj = NULL;
	    obj =
	      (melt_ptr_t) (( /*!BUFFER_LIMIT_CONT */ meltfrout->
			     tabval[4])) /*=obj*/ ;
	    melt_object_get_field (slot, obj, 0, "REFERENCED_VALUE");
     /*_.REFERENCED_VALUE__V15*/ meltfptr[14] = slot;
	  };
	  ;
	}
      else
	{			/*^cond.else */

    /*_.REFERENCED_VALUE__V15*/ meltfptr[14] = NULL;;
	}
      ;
      /*^compute */
   /*_#GET_INT__L5*/ meltfnum[4] =
	(melt_get_int
	 ((melt_ptr_t) ( /*_.REFERENCED_VALUE__V15*/ meltfptr[14])));;
      /*^compute */
   /*_#I__L6*/ meltfnum[5] =
	(( /*_#STRBUF_USEDLENGTH__L4*/ meltfnum[1]) <
	 ( /*_#GET_INT__L5*/ meltfnum[4]));;
      MELT_LOCATION ("warmelt-outobj.melt:2986:/ cond");
      /*cond */ if ( /*_#I__L6*/ meltfnum[5])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V16*/ meltfptr[15] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-outobj.melt:2986:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check limited implbuf"),
				  ("warmelt-outobj.melt")
				  ? ("warmelt-outobj.melt") : __FILE__,
				  (2986) ? (2986) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V16*/ meltfptr[15] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V14*/ meltfptr[13] = /*_.IFELSE___V16*/ meltfptr[15];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:2986:/ clear");
	     /*clear *//*_#STRBUF_USEDLENGTH__L4*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.REFERENCED_VALUE__V15*/ meltfptr[14] = 0;
      /*^clear */
	     /*clear *//*_#GET_INT__L5*/ meltfnum[4] = 0;
      /*^clear */
	     /*clear *//*_#I__L6*/ meltfnum[5] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V16*/ meltfptr[15] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V14*/ meltfptr[13] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    /*^compute */
    /*_.LET___V7*/ meltfptr[5] = /*_.IFCPP___V14*/ meltfptr[13];;

    MELT_LOCATION ("warmelt-outobj.melt:2975:/ clear");
	   /*clear *//*_.OLAB__V8*/ meltfptr[7] = 0;
    /*^clear */
	   /*clear *//*_.LOC__V9*/ meltfptr[8] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V10*/ meltfptr[9] = 0;
    /*^clear */
	   /*clear *//*_.OUTPUT_LOCATION__V12*/ meltfptr[10] = 0;
    /*^clear */
	   /*clear *//*_.ADD2SBUF_CLONSYM__V13*/ meltfptr[12] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V14*/ meltfptr[13] = 0;
    MELT_LOCATION ("warmelt-outobj.melt:2973:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.LET___V7*/ meltfptr[5];;

    {
      MELT_LOCATION ("warmelt-outobj.melt:2973:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V5*/ meltfptr[4] = 0;
    /*^clear */
	   /*clear *//*_.LET___V7*/ meltfptr[5] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("OUTPUCOD_OBJAGAIN", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_67_warmelt_outobj_OUTPUCOD_OBJAGAIN_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_67_warmelt_outobj_OUTPUCOD_OBJAGAIN */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_68_warmelt_outobj_OUTPUCOD_OBJCOMPUTE (meltclosure_ptr_t meltclosp_,
						melt_ptr_t meltfirstargp_,
						const melt_argdescr_cell_t
						meltxargdescr_[],
						union meltparam_un *
						meltxargtab_,
						const melt_argdescr_cell_t
						meltxresdescr_[],
						union meltparam_un *
						meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_68_warmelt_outobj_OUTPUCOD_OBJCOMPUTE_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_68_warmelt_outobj_OUTPUCOD_OBJCOMPUTE_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 31
    melt_ptr_t mcfr_varptr[31];
#define MELTFRAM_NBVARNUM 16
    long mcfr_varnum[16];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_68_warmelt_outobj_OUTPUCOD_OBJCOMPUTE is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_68_warmelt_outobj_OUTPUCOD_OBJCOMPUTE_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 31; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_68_warmelt_outobj_OUTPUCOD_OBJCOMPUTE nbval 31*/
  meltfram__.mcfr_nbvar = 31 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("OUTPUCOD_OBJCOMPUTE", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-outobj.melt:2992:/ getarg");
 /*_.OBCOMP__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.DECLBUF__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.DECLBUF__V3*/ meltfptr[2])) !=
	      NULL);


  /*getarg#2 */
  /*^getarg */
  if (meltxargdescr_[1] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.IMPLBUF__V4*/ meltfptr[3] =
    (meltxargtab_[1].meltbp_aptr) ? (*(meltxargtab_[1].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3])) !=
	      NULL);


  /*getarg#3 */
  /*^getarg */
  if (meltxargdescr_[2] != MELTBPAR_LONG)
    goto lab_endgetargs;
 /*_#DEPTH__L1*/ meltfnum[0] = meltxargtab_[2].meltbp_long;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:2993:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L2*/ meltfnum[1] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.OBCOMP__V2*/ meltfptr[1]),
			     (melt_ptr_t) (( /*!CLASS_OBJCOMPUTE */
					    meltfrout->tabval[0])));;
      MELT_LOCATION ("warmelt-outobj.melt:2993:/ cond");
      /*cond */ if ( /*_#IS_A__L2*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V6*/ meltfptr[5] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-outobj.melt:2993:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check obcomp"),
				  ("warmelt-outobj.melt")
				  ? ("warmelt-outobj.melt") : __FILE__,
				  (2993) ? (2993) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V6*/ meltfptr[5] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V5*/ meltfptr[4] = /*_.IFELSE___V6*/ meltfptr[5];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:2993:/ clear");
	     /*clear *//*_#IS_A__L2*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V6*/ meltfptr[5] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V5*/ meltfptr[4] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-outobj.melt:2994:/ quasiblock");


    /*^getslot */
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.OBCOMP__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 1, "OBDI_DESTLIST");
  /*_.CDEST__V8*/ meltfptr[7] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-outobj.melt:2995:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.OBCOMP__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 0, "OBI_LOC");
  /*_.CLOC__V9*/ meltfptr[8] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-outobj.melt:2996:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.OBCOMP__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 2, "OBCPT_EXPR");
  /*_.CEXP__V10*/ meltfptr[9] = slot;
    };
    ;
 /*_#I__L3*/ meltfnum[1] =
      (( /*_#DEPTH__L1*/ meltfnum[0]) + (1));;
    /*^compute */
 /*_.BOXDEPTHP1__V11*/ meltfptr[10] =
      (meltgc_new_int
       ((meltobject_ptr_t) (( /*!DISCR_INTEGER */ meltfrout->tabval[1])),
	( /*_#I__L3*/ meltfnum[1])));;
    MELT_LOCATION ("warmelt-outobj.melt:2999:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[3];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.IMPLBUF__V4*/ meltfptr[3];
      /*^apply.arg */
      argtab[1].meltbp_long = /*_#DEPTH__L1*/ meltfnum[0];
      /*^apply.arg */
      argtab[2].meltbp_cstring = "compute";
      /*_.OUTPUT_LOCATION__V12*/ meltfptr[11] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!OUTPUT_LOCATION */ meltfrout->tabval[2])),
		    (melt_ptr_t) ( /*_.CLOC__V9*/ meltfptr[8]),
		    (MELTBPARSTR_PTR MELTBPARSTR_LONG MELTBPARSTR_CSTRING ""),
		    argtab, "", (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:3000:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#IS_LIST__L4*/ meltfnum[3] =
      (melt_magic_discr ((melt_ptr_t) ( /*_.CDEST__V8*/ meltfptr[7])) ==
       MELTOBMAG_LIST);;
    MELT_LOCATION ("warmelt-outobj.melt:3000:/ cond");
    /*cond */ if ( /*_#IS_LIST__L4*/ meltfnum[3])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-outobj.melt:3003:/ quasiblock");


	  /*^newclosure */
		   /*newclosure *//*_.LAMBDA___V15*/ meltfptr[14] =
	    (melt_ptr_t)
	    meltgc_new_closure ((meltobject_ptr_t)
				(((melt_ptr_t)
				  (MELT_PREDEF (DISCR_CLOSURE)))),
				(meltroutine_ptr_t) (( /*!konst_5 */
						      meltfrout->tabval[5])),
				(3));
	  ;
	  /*^putclosedv */
	  /*putclosv */
	  melt_assertmsg ("putclosv checkclo",
			  melt_magic_discr ((melt_ptr_t)
					    ( /*_.LAMBDA___V15*/
					     meltfptr[14])) ==
			  MELTOBMAG_CLOSURE);
	  melt_assertmsg ("putclosv checkoff", 0 >= 0
			  && 0 <
			  melt_closure_size ((melt_ptr_t)
					     ( /*_.LAMBDA___V15*/
					      meltfptr[14])));
	  ((meltclosure_ptr_t) /*_.LAMBDA___V15*/ meltfptr[14])->tabval[0] =
	    (melt_ptr_t) ( /*_.DECLBUF__V3*/ meltfptr[2]);
	  ;
	  /*^putclosedv */
	  /*putclosv */
	  melt_assertmsg ("putclosv checkclo",
			  melt_magic_discr ((melt_ptr_t)
					    ( /*_.LAMBDA___V15*/
					     meltfptr[14])) ==
			  MELTOBMAG_CLOSURE);
	  melt_assertmsg ("putclosv checkoff", 1 >= 0
			  && 1 <
			  melt_closure_size ((melt_ptr_t)
					     ( /*_.LAMBDA___V15*/
					      meltfptr[14])));
	  ((meltclosure_ptr_t) /*_.LAMBDA___V15*/ meltfptr[14])->tabval[1] =
	    (melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]);
	  ;
	  /*^putclosedv */
	  /*putclosv */
	  melt_assertmsg ("putclosv checkclo",
			  melt_magic_discr ((melt_ptr_t)
					    ( /*_.LAMBDA___V15*/
					     meltfptr[14])) ==
			  MELTOBMAG_CLOSURE);
	  melt_assertmsg ("putclosv checkoff", 2 >= 0
			  && 2 <
			  melt_closure_size ((melt_ptr_t)
					     ( /*_.LAMBDA___V15*/
					      meltfptr[14])));
	  ((meltclosure_ptr_t) /*_.LAMBDA___V15*/ meltfptr[14])->tabval[2] =
	    (melt_ptr_t) ( /*_.BOXDEPTHP1__V11*/ meltfptr[10]);
	  ;
	  /*_.LAMBDA___V14*/ meltfptr[13] = /*_.LAMBDA___V15*/ meltfptr[14];;
	  MELT_LOCATION ("warmelt-outobj.melt:3001:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^apply */
	  /*apply */
	  {
	    union meltparam_un argtab[1];
	    memset (&argtab, 0, sizeof (argtab));
	    /*^apply.arg */
	    argtab[0].meltbp_aptr =
	      (melt_ptr_t *) & /*_.LAMBDA___V14*/ meltfptr[13];
	    /*_.LIST_EVERY__V16*/ meltfptr[15] =
	      melt_apply ((meltclosure_ptr_t)
			  (( /*!LIST_EVERY */ meltfrout->tabval[3])),
			  (melt_ptr_t) ( /*_.CDEST__V8*/ meltfptr[7]),
			  (MELTBPARSTR_PTR ""), argtab, "",
			  (union meltparam_un *) 0);
	  }
	  ;
	  /*_.IF___V13*/ meltfptr[12] = /*_.LIST_EVERY__V16*/ meltfptr[15];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-outobj.melt:3000:/ clear");
	     /*clear *//*_.LAMBDA___V14*/ meltfptr[13] = 0;
	  /*^clear */
	     /*clear *//*_.LIST_EVERY__V16*/ meltfptr[15] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

  /*_.IF___V13*/ meltfptr[12] = NULL;;
      }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:3007:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#IS_LIST__L5*/ meltfnum[4] =
      (melt_magic_discr ((melt_ptr_t) ( /*_.CEXP__V10*/ meltfptr[9])) ==
       MELTOBMAG_LIST);;
    MELT_LOCATION ("warmelt-outobj.melt:3007:/ cond");
    /*cond */ if ( /*_#IS_LIST__L5*/ meltfnum[4])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-outobj.melt:3008:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
   /*_#LIST_LENGTH__L6*/ meltfnum[5] =
	    (melt_list_length ((melt_ptr_t) ( /*_.CEXP__V10*/ meltfptr[9])));;
	  /*^compute */
   /*_#I__L7*/ meltfnum[6] =
	    (( /*_#LIST_LENGTH__L6*/ meltfnum[5]) > (2));;
	  MELT_LOCATION ("warmelt-outobj.melt:3008:/ cond");
	  /*cond */ if ( /*_#I__L7*/ meltfnum[6])	/*then */
	    {
	      /*^cond.then */
	      /*^block */
	      /*anyblock */
	      {

     /*_#I__L8*/ meltfnum[7] =
		  ((1) + ( /*_#DEPTH__L1*/ meltfnum[0]));;

		{
		  MELT_LOCATION ("warmelt-outobj.melt:3009:/ locexp");
		  meltgc_strbuf_add_indent ((melt_ptr_t)
					    ( /*_.IMPLBUF__V4*/ meltfptr[3]),
					    ( /*_#I__L8*/ meltfnum[7]), 0);
		}
		;
		/*epilog */

		MELT_LOCATION ("warmelt-outobj.melt:3008:/ clear");
	       /*clear *//*_#I__L8*/ meltfnum[7] = 0;
	      }
	      ;
	    }			/*noelse */
	  ;
	  MELT_LOCATION ("warmelt-outobj.melt:3012:/ quasiblock");


	  /*^newclosure */
		   /*newclosure *//*_.LAMBDA___V19*/ meltfptr[18] =
	    (melt_ptr_t)
	    meltgc_new_closure ((meltobject_ptr_t)
				(((melt_ptr_t)
				  (MELT_PREDEF (DISCR_CLOSURE)))),
				(meltroutine_ptr_t) (( /*!konst_9 */
						      meltfrout->tabval[9])),
				(3));
	  ;
	  /*^putclosedv */
	  /*putclosv */
	  melt_assertmsg ("putclosv checkclo",
			  melt_magic_discr ((melt_ptr_t)
					    ( /*_.LAMBDA___V19*/
					     meltfptr[18])) ==
			  MELTOBMAG_CLOSURE);
	  melt_assertmsg ("putclosv checkoff", 0 >= 0
			  && 0 <
			  melt_closure_size ((melt_ptr_t)
					     ( /*_.LAMBDA___V19*/
					      meltfptr[18])));
	  ((meltclosure_ptr_t) /*_.LAMBDA___V19*/ meltfptr[18])->tabval[0] =
	    (melt_ptr_t) ( /*_.DECLBUF__V3*/ meltfptr[2]);
	  ;
	  /*^putclosedv */
	  /*putclosv */
	  melt_assertmsg ("putclosv checkclo",
			  melt_magic_discr ((melt_ptr_t)
					    ( /*_.LAMBDA___V19*/
					     meltfptr[18])) ==
			  MELTOBMAG_CLOSURE);
	  melt_assertmsg ("putclosv checkoff", 1 >= 0
			  && 1 <
			  melt_closure_size ((melt_ptr_t)
					     ( /*_.LAMBDA___V19*/
					      meltfptr[18])));
	  ((meltclosure_ptr_t) /*_.LAMBDA___V19*/ meltfptr[18])->tabval[1] =
	    (melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]);
	  ;
	  /*^putclosedv */
	  /*putclosv */
	  melt_assertmsg ("putclosv checkclo",
			  melt_magic_discr ((melt_ptr_t)
					    ( /*_.LAMBDA___V19*/
					     meltfptr[18])) ==
			  MELTOBMAG_CLOSURE);
	  melt_assertmsg ("putclosv checkoff", 2 >= 0
			  && 2 <
			  melt_closure_size ((melt_ptr_t)
					     ( /*_.LAMBDA___V19*/
					      meltfptr[18])));
	  ((meltclosure_ptr_t) /*_.LAMBDA___V19*/ meltfptr[18])->tabval[2] =
	    (melt_ptr_t) ( /*_.BOXDEPTHP1__V11*/ meltfptr[10]);
	  ;
	  /*_.LAMBDA___V18*/ meltfptr[15] = /*_.LAMBDA___V19*/ meltfptr[18];;
	  MELT_LOCATION ("warmelt-outobj.melt:3010:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^apply */
	  /*apply */
	  {
	    union meltparam_un argtab[1];
	    memset (&argtab, 0, sizeof (argtab));
	    /*^apply.arg */
	    argtab[0].meltbp_aptr =
	      (melt_ptr_t *) & /*_.LAMBDA___V18*/ meltfptr[15];
	    /*_.LIST_EVERY__V20*/ meltfptr[19] =
	      melt_apply ((meltclosure_ptr_t)
			  (( /*!LIST_EVERY */ meltfrout->tabval[3])),
			  (melt_ptr_t) ( /*_.CEXP__V10*/ meltfptr[9]),
			  (MELTBPARSTR_PTR ""), argtab, "",
			  (union meltparam_un *) 0);
	  }
	  ;
	  MELT_LOCATION ("warmelt-outobj.melt:3007:/ quasiblock");


	  /*_.PROGN___V21*/ meltfptr[20] =
	    /*_.LIST_EVERY__V20*/ meltfptr[19];;
	  /*^compute */
	  /*_.IFELSE___V17*/ meltfptr[13] = /*_.PROGN___V21*/ meltfptr[20];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-outobj.melt:3007:/ clear");
	     /*clear *//*_#LIST_LENGTH__L6*/ meltfnum[5] = 0;
	  /*^clear */
	     /*clear *//*_#I__L7*/ meltfnum[6] = 0;
	  /*^clear */
	     /*clear *//*_.LAMBDA___V18*/ meltfptr[15] = 0;
	  /*^clear */
	     /*clear *//*_.LIST_EVERY__V20*/ meltfptr[19] = 0;
	  /*^clear */
	     /*clear *//*_.PROGN___V21*/ meltfptr[20] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-outobj.melt:3017:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
   /*_#IS_MULTIPLE__L9*/ meltfnum[7] =
	    (melt_magic_discr ((melt_ptr_t) ( /*_.CEXP__V10*/ meltfptr[9])) ==
	     MELTOBMAG_MULTIPLE);;
	  MELT_LOCATION ("warmelt-outobj.melt:3017:/ cond");
	  /*cond */ if ( /*_#IS_MULTIPLE__L9*/ meltfnum[7])	/*then */
	    {
	      /*^cond.then */
	      /*^block */
	      /*anyblock */
	      {

		MELT_LOCATION ("warmelt-outobj.melt:3018:/ checksignal");
		MELT_CHECK_SIGNAL ();
		;
     /*_#MULTIPLE_LENGTH__L10*/ meltfnum[5] =
		  (melt_multiple_length
		   ((melt_ptr_t) ( /*_.CEXP__V10*/ meltfptr[9])));;
		/*^compute */
     /*_#I__L11*/ meltfnum[6] =
		  (( /*_#MULTIPLE_LENGTH__L10*/ meltfnum[5]) > (2));;
		MELT_LOCATION ("warmelt-outobj.melt:3018:/ cond");
		/*cond */ if ( /*_#I__L11*/ meltfnum[6])	/*then */
		  {
		    /*^cond.then */
		    /*^block */
		    /*anyblock */
		    {

       /*_#I__L12*/ meltfnum[11] =
			((1) + ( /*_#DEPTH__L1*/ meltfnum[0]));;

		      {
			MELT_LOCATION ("warmelt-outobj.melt:3019:/ locexp");
			meltgc_strbuf_add_indent ((melt_ptr_t)
						  ( /*_.IMPLBUF__V4*/
						   meltfptr[3]),
						  ( /*_#I__L12*/
						   meltfnum[11]), 0);
		      }
		      ;
		      /*epilog */

		      MELT_LOCATION ("warmelt-outobj.melt:3018:/ clear");
		 /*clear *//*_#I__L12*/ meltfnum[11] = 0;
		    }
		    ;
		  }		/*noelse */
		;
		MELT_LOCATION ("warmelt-outobj.melt:3022:/ quasiblock");


		/*^newclosure */
		     /*newclosure *//*_.LAMBDA___V24*/ meltfptr[20] =
		  (melt_ptr_t)
		  meltgc_new_closure ((meltobject_ptr_t)
				      (((melt_ptr_t)
					(MELT_PREDEF (DISCR_CLOSURE)))),
				      (meltroutine_ptr_t) (( /*!konst_14 */
							    meltfrout->
							    tabval[14])),
				      (3));
		;
		/*^putclosedv */
		/*putclosv */
		melt_assertmsg ("putclosv checkclo",
				melt_magic_discr ((melt_ptr_t)
						  ( /*_.LAMBDA___V24*/
						   meltfptr[20])) ==
				MELTOBMAG_CLOSURE);
		melt_assertmsg ("putclosv checkoff", 0 >= 0
				&& 0 <
				melt_closure_size ((melt_ptr_t)
						   ( /*_.LAMBDA___V24*/
						    meltfptr[20])));
		((meltclosure_ptr_t) /*_.LAMBDA___V24*/ meltfptr[20])->
		  tabval[0] = (melt_ptr_t) ( /*_.DECLBUF__V3*/ meltfptr[2]);
		;
		/*^putclosedv */
		/*putclosv */
		melt_assertmsg ("putclosv checkclo",
				melt_magic_discr ((melt_ptr_t)
						  ( /*_.LAMBDA___V24*/
						   meltfptr[20])) ==
				MELTOBMAG_CLOSURE);
		melt_assertmsg ("putclosv checkoff", 1 >= 0
				&& 1 <
				melt_closure_size ((melt_ptr_t)
						   ( /*_.LAMBDA___V24*/
						    meltfptr[20])));
		((meltclosure_ptr_t) /*_.LAMBDA___V24*/ meltfptr[20])->
		  tabval[1] = (melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]);
		;
		/*^putclosedv */
		/*putclosv */
		melt_assertmsg ("putclosv checkclo",
				melt_magic_discr ((melt_ptr_t)
						  ( /*_.LAMBDA___V24*/
						   meltfptr[20])) ==
				MELTOBMAG_CLOSURE);
		melt_assertmsg ("putclosv checkoff", 2 >= 0
				&& 2 <
				melt_closure_size ((melt_ptr_t)
						   ( /*_.LAMBDA___V24*/
						    meltfptr[20])));
		((meltclosure_ptr_t) /*_.LAMBDA___V24*/ meltfptr[20])->
		  tabval[2] =
		  (melt_ptr_t) ( /*_.BOXDEPTHP1__V11*/ meltfptr[10]);
		;
		/*_.LAMBDA___V23*/ meltfptr[19] =
		  /*_.LAMBDA___V24*/ meltfptr[20];;
		MELT_LOCATION ("warmelt-outobj.melt:3020:/ checksignal");
		MELT_CHECK_SIGNAL ();
		;
		/*^apply */
		/*apply */
		{
		  union meltparam_un argtab[1];
		  memset (&argtab, 0, sizeof (argtab));
		  /*^apply.arg */
		  argtab[0].meltbp_aptr =
		    (melt_ptr_t *) & /*_.LAMBDA___V23*/ meltfptr[19];
		  /*_.MULTIPLE_EVERY__V25*/ meltfptr[24] =
		    melt_apply ((meltclosure_ptr_t)
				(( /*!MULTIPLE_EVERY */ meltfrout->
				  tabval[10])),
				(melt_ptr_t) ( /*_.CEXP__V10*/ meltfptr[9]),
				(MELTBPARSTR_PTR ""), argtab, "",
				(union meltparam_un *) 0);
		}
		;
		MELT_LOCATION ("warmelt-outobj.melt:3017:/ quasiblock");


		/*_.PROGN___V26*/ meltfptr[25] =
		  /*_.MULTIPLE_EVERY__V25*/ meltfptr[24];;
		/*^compute */
		/*_.IFELSE___V22*/ meltfptr[15] =
		  /*_.PROGN___V26*/ meltfptr[25];;
		/*epilog */

		MELT_LOCATION ("warmelt-outobj.melt:3017:/ clear");
	       /*clear *//*_#MULTIPLE_LENGTH__L10*/ meltfnum[5] = 0;
		/*^clear */
	       /*clear *//*_#I__L11*/ meltfnum[6] = 0;
		/*^clear */
	       /*clear *//*_.LAMBDA___V23*/ meltfptr[19] = 0;
		/*^clear */
	       /*clear *//*_.MULTIPLE_EVERY__V25*/ meltfptr[24] = 0;
		/*^clear */
	       /*clear *//*_.PROGN___V26*/ meltfptr[25] = 0;
	      }
	      ;
	    }
	  else
	    {			/*^cond.else */

	      /*^block */
	      /*anyblock */
	      {

     /*_#I__L13*/ meltfnum[11] =
		  (( /*_#DEPTH__L1*/ meltfnum[0]) + (1));;
		MELT_LOCATION ("warmelt-outobj.melt:3028:/ checksignal");
		MELT_CHECK_SIGNAL ();
		;
		/*^msend */
		/*msend */
		{
		  union meltparam_un argtab[3];
		  memset (&argtab, 0, sizeof (argtab));
		  /*^ojbmsend.arg */
		  argtab[0].meltbp_aptr =
		    (melt_ptr_t *) & /*_.DECLBUF__V3*/ meltfptr[2];
		  /*^ojbmsend.arg */
		  argtab[1].meltbp_aptr =
		    (melt_ptr_t *) & /*_.IMPLBUF__V4*/ meltfptr[3];
		  /*^ojbmsend.arg */
		  argtab[2].meltbp_long = /*_#I__L13*/ meltfnum[11];
		  /*_.OUTPUT_C_CODE__V27*/ meltfptr[19] =
		    meltgc_send ((melt_ptr_t) ( /*_.CEXP__V10*/ meltfptr[9]),
				 (melt_ptr_t) (( /*!OUTPUT_C_CODE */
						meltfrout->tabval[15])),
				 (MELTBPARSTR_PTR MELTBPARSTR_PTR
				  MELTBPARSTR_LONG ""), argtab, "",
				 (union meltparam_un *) 0);
		}
		;
		MELT_LOCATION ("warmelt-outobj.melt:3027:/ quasiblock");


		/*_.PROGN___V28*/ meltfptr[24] =
		  /*_.OUTPUT_C_CODE__V27*/ meltfptr[19];;
		/*^compute */
		/*_.IFELSE___V22*/ meltfptr[15] =
		  /*_.PROGN___V28*/ meltfptr[24];;
		/*epilog */

		MELT_LOCATION ("warmelt-outobj.melt:3017:/ clear");
	       /*clear *//*_#I__L13*/ meltfnum[11] = 0;
		/*^clear */
	       /*clear *//*_.OUTPUT_C_CODE__V27*/ meltfptr[19] = 0;
		/*^clear */
	       /*clear *//*_.PROGN___V28*/ meltfptr[24] = 0;
	      }
	      ;
	    }
	  ;
	  /*_.IFELSE___V17*/ meltfptr[13] = /*_.IFELSE___V22*/ meltfptr[15];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-outobj.melt:3007:/ clear");
	     /*clear *//*_#IS_MULTIPLE__L9*/ meltfnum[7] = 0;
	  /*^clear */
	     /*clear *//*_.IFELSE___V22*/ meltfptr[15] = 0;
	}
	;
      }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:3030:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   (";"));
    }
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:3031:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#STRBUF_USEDLENGTH__L14*/ meltfnum[5] =
	melt_strbuf_usedlength ((melt_ptr_t)
				( /*_.IMPLBUF__V4*/ meltfptr[3]));;
      MELT_LOCATION ("warmelt-outobj.melt:3032:/ cond");
      /*cond */ if (
		     /*ifisa */
		     melt_is_instance_of ((melt_ptr_t)
					  (( /*!BUFFER_LIMIT_CONT */
					    meltfrout->tabval[16])),
					  (melt_ptr_t) (( /*!CLASS_REFERENCE */ meltfrout->tabval[17])))
	)			/*then */
	{
	  /*^cond.then */
	  /*^getslot */
	  {
	    melt_ptr_t slot = NULL, obj = NULL;
	    obj =
	      (melt_ptr_t) (( /*!BUFFER_LIMIT_CONT */ meltfrout->
			     tabval[16])) /*=obj*/ ;
	    melt_object_get_field (slot, obj, 0, "REFERENCED_VALUE");
     /*_.REFERENCED_VALUE__V30*/ meltfptr[19] = slot;
	  };
	  ;
	}
      else
	{			/*^cond.else */

    /*_.REFERENCED_VALUE__V30*/ meltfptr[19] = NULL;;
	}
      ;
      /*^compute */
   /*_#GET_INT__L15*/ meltfnum[6] =
	(melt_get_int
	 ((melt_ptr_t) ( /*_.REFERENCED_VALUE__V30*/ meltfptr[19])));;
      /*^compute */
   /*_#I__L16*/ meltfnum[11] =
	(( /*_#STRBUF_USEDLENGTH__L14*/ meltfnum[5]) <
	 ( /*_#GET_INT__L15*/ meltfnum[6]));;
      MELT_LOCATION ("warmelt-outobj.melt:3031:/ cond");
      /*cond */ if ( /*_#I__L16*/ meltfnum[11])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V31*/ meltfptr[24] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-outobj.melt:3031:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check limited implbuf"),
				  ("warmelt-outobj.melt")
				  ? ("warmelt-outobj.melt") : __FILE__,
				  (3031) ? (3031) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V31*/ meltfptr[24] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V29*/ meltfptr[25] = /*_.IFELSE___V31*/ meltfptr[24];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:3031:/ clear");
	     /*clear *//*_#STRBUF_USEDLENGTH__L14*/ meltfnum[5] = 0;
      /*^clear */
	     /*clear *//*_.REFERENCED_VALUE__V30*/ meltfptr[19] = 0;
      /*^clear */
	     /*clear *//*_#GET_INT__L15*/ meltfnum[6] = 0;
      /*^clear */
	     /*clear *//*_#I__L16*/ meltfnum[11] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V31*/ meltfptr[24] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V29*/ meltfptr[25] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    /*^compute */
    /*_.LET___V7*/ meltfptr[5] = /*_.IFCPP___V29*/ meltfptr[25];;

    MELT_LOCATION ("warmelt-outobj.melt:2994:/ clear");
	   /*clear *//*_.CDEST__V8*/ meltfptr[7] = 0;
    /*^clear */
	   /*clear *//*_.CLOC__V9*/ meltfptr[8] = 0;
    /*^clear */
	   /*clear *//*_.CEXP__V10*/ meltfptr[9] = 0;
    /*^clear */
	   /*clear *//*_#I__L3*/ meltfnum[1] = 0;
    /*^clear */
	   /*clear *//*_.BOXDEPTHP1__V11*/ meltfptr[10] = 0;
    /*^clear */
	   /*clear *//*_.OUTPUT_LOCATION__V12*/ meltfptr[11] = 0;
    /*^clear */
	   /*clear *//*_#IS_LIST__L4*/ meltfnum[3] = 0;
    /*^clear */
	   /*clear *//*_.IF___V13*/ meltfptr[12] = 0;
    /*^clear */
	   /*clear *//*_#IS_LIST__L5*/ meltfnum[4] = 0;
    /*^clear */
	   /*clear *//*_.IFELSE___V17*/ meltfptr[13] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V29*/ meltfptr[25] = 0;
    MELT_LOCATION ("warmelt-outobj.melt:2992:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.LET___V7*/ meltfptr[5];;

    {
      MELT_LOCATION ("warmelt-outobj.melt:2992:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V5*/ meltfptr[4] = 0;
    /*^clear */
	   /*clear *//*_.LET___V7*/ meltfptr[5] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("OUTPUCOD_OBJCOMPUTE", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_68_warmelt_outobj_OUTPUCOD_OBJCOMPUTE_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_68_warmelt_outobj_OUTPUCOD_OBJCOMPUTE */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_69_warmelt_outobj_LAMBDA___8__ (meltclosure_ptr_t meltclosp_,
					 melt_ptr_t meltfirstargp_,
					 const melt_argdescr_cell_t
					 meltxargdescr_[],
					 union meltparam_un * meltxargtab_,
					 const melt_argdescr_cell_t
					 meltxresdescr_[],
					 union meltparam_un * meltxrestab_)
{
  long current_blocklevel_signals_meltrout_69_warmelt_outobj_LAMBDA___8___melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_69_warmelt_outobj_LAMBDA___8___st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 4
    melt_ptr_t mcfr_varptr[4];
#define MELTFRAM_NBVARNUM 1
    long mcfr_varnum[1];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_69_warmelt_outobj_LAMBDA___8__ is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_69_warmelt_outobj_LAMBDA___8___st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 4; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_69_warmelt_outobj_LAMBDA___8__ nbval 4*/
  meltfram__.mcfr_nbvar = 4 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("LAMBDA_", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-outobj.melt:3003:/ getarg");
 /*_.DESTCUR__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {

 /*_#GET_INT__L1*/ meltfnum[0] =
      (melt_get_int
       ((melt_ptr_t) (( /*~BOXDEPTHP1 */ meltfclos->tabval[2]))));;
    MELT_LOCATION ("warmelt-outobj.melt:3004:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^msend */
    /*msend */
    {
      union meltparam_un argtab[3];
      memset (&argtab, 0, sizeof (argtab));
      /*^ojbmsend.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & ( /*~DECLBUF */ meltfclos->tabval[0]);
      /*^ojbmsend.arg */
      argtab[1].meltbp_aptr =
	(melt_ptr_t *) & ( /*~IMPLBUF */ meltfclos->tabval[1]);
      /*^ojbmsend.arg */
      argtab[2].meltbp_long = /*_#GET_INT__L1*/ meltfnum[0];
      /*_.OUTPUT_C_CODE__V3*/ meltfptr[2] =
	meltgc_send ((melt_ptr_t) ( /*_.DESTCUR__V2*/ meltfptr[1]),
		     (melt_ptr_t) (( /*!OUTPUT_C_CODE */ meltfrout->
				    tabval[0])),
		     (MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_LONG ""),
		     argtab, "", (union meltparam_un *) 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:3005:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t)
			   (( /*~IMPLBUF */ meltfclos->tabval[1])), (" = "));
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:3003:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V4*/ meltfptr[3] = ( /*nil */ NULL);;
    MELT_LOCATION ("warmelt-outobj.melt:3003:/ quasiblock");


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.RETVAL___V4*/ meltfptr[3];;

    {
      MELT_LOCATION ("warmelt-outobj.melt:3003:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;

    /*^clear */
	   /*clear *//*_.RETVAL___V4*/ meltfptr[3] = 0;
    /*epilog */

    /*^clear */
	   /*clear *//*_#GET_INT__L1*/ meltfnum[0] = 0;
    /*^clear */
	   /*clear *//*_.OUTPUT_C_CODE__V3*/ meltfptr[2] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("LAMBDA_", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_69_warmelt_outobj_LAMBDA___8___melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_69_warmelt_outobj_LAMBDA___8__ */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_70_warmelt_outobj_LAMBDA___9__ (meltclosure_ptr_t meltclosp_,
					 melt_ptr_t meltfirstargp_,
					 const melt_argdescr_cell_t
					 meltxargdescr_[],
					 union meltparam_un * meltxargtab_,
					 const melt_argdescr_cell_t
					 meltxresdescr_[],
					 union meltparam_un * meltxrestab_)
{
  long current_blocklevel_signals_meltrout_70_warmelt_outobj_LAMBDA___9___melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_70_warmelt_outobj_LAMBDA___9___st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 6
    melt_ptr_t mcfr_varptr[6];
#define MELTFRAM_NBVARNUM 4
    long mcfr_varnum[4];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_70_warmelt_outobj_LAMBDA___9__ is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_70_warmelt_outobj_LAMBDA___9___st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 6; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_70_warmelt_outobj_LAMBDA___9__ nbval 6*/
  meltfram__.mcfr_nbvar = 6 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("LAMBDA_", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-outobj.melt:3012:/ getarg");
 /*_.EXPCUR__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {

 /*_#GET_INT__L1*/ meltfnum[0] =
      (melt_get_int
       ((melt_ptr_t) (( /*~BOXDEPTHP1 */ meltfclos->tabval[2]))));;
    MELT_LOCATION ("warmelt-outobj.melt:3013:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^msend */
    /*msend */
    {
      union meltparam_un argtab[3];
      memset (&argtab, 0, sizeof (argtab));
      /*^ojbmsend.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & ( /*~DECLBUF */ meltfclos->tabval[0]);
      /*^ojbmsend.arg */
      argtab[1].meltbp_aptr =
	(melt_ptr_t *) & ( /*~IMPLBUF */ meltfclos->tabval[1]);
      /*^ojbmsend.arg */
      argtab[2].meltbp_long = /*_#GET_INT__L1*/ meltfnum[0];
      /*_.OUTPUT_C_CODE__V3*/ meltfptr[2] =
	meltgc_send ((melt_ptr_t) ( /*_.EXPCUR__V2*/ meltfptr[1]),
		     (melt_ptr_t) (( /*!OUTPUT_C_CODE */ meltfrout->
				    tabval[0])),
		     (MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_LONG ""),
		     argtab, "", (union meltparam_un *) 0);
    }
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:3014:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#STRBUF_USEDLENGTH__L2*/ meltfnum[1] =
	melt_strbuf_usedlength ((melt_ptr_t)
				(( /*~IMPLBUF */ meltfclos->tabval[1])));;
      MELT_LOCATION ("warmelt-outobj.melt:3015:/ cond");
      /*cond */ if (
		     /*ifisa */
		     melt_is_instance_of ((melt_ptr_t)
					  (( /*!BUFFER_LIMIT_CONT */
					    meltfrout->tabval[1])),
					  (melt_ptr_t) (( /*!CLASS_REFERENCE */ meltfrout->tabval[2])))
	)			/*then */
	{
	  /*^cond.then */
	  /*^getslot */
	  {
	    melt_ptr_t slot = NULL, obj = NULL;
	    obj =
	      (melt_ptr_t) (( /*!BUFFER_LIMIT_CONT */ meltfrout->
			     tabval[1])) /*=obj*/ ;
	    melt_object_get_field (slot, obj, 0, "REFERENCED_VALUE");
     /*_.REFERENCED_VALUE__V5*/ meltfptr[4] = slot;
	  };
	  ;
	}
      else
	{			/*^cond.else */

    /*_.REFERENCED_VALUE__V5*/ meltfptr[4] = NULL;;
	}
      ;
      /*^compute */
   /*_#GET_INT__L3*/ meltfnum[2] =
	(melt_get_int
	 ((melt_ptr_t) ( /*_.REFERENCED_VALUE__V5*/ meltfptr[4])));;
      /*^compute */
   /*_#I__L4*/ meltfnum[3] =
	(( /*_#STRBUF_USEDLENGTH__L2*/ meltfnum[1]) <
	 ( /*_#GET_INT__L3*/ meltfnum[2]));;
      MELT_LOCATION ("warmelt-outobj.melt:3014:/ cond");
      /*cond */ if ( /*_#I__L4*/ meltfnum[3])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V6*/ meltfptr[5] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-outobj.melt:3014:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check limited implbuf"),
				  ("warmelt-outobj.melt")
				  ? ("warmelt-outobj.melt") : __FILE__,
				  (3014) ? (3014) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V6*/ meltfptr[5] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V4*/ meltfptr[3] = /*_.IFELSE___V6*/ meltfptr[5];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:3014:/ clear");
	     /*clear *//*_#STRBUF_USEDLENGTH__L2*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.REFERENCED_VALUE__V5*/ meltfptr[4] = 0;
      /*^clear */
	     /*clear *//*_#GET_INT__L3*/ meltfnum[2] = 0;
      /*^clear */
	     /*clear *//*_#I__L4*/ meltfnum[3] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V6*/ meltfptr[5] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V4*/ meltfptr[3] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-outobj.melt:3012:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.IFCPP___V4*/ meltfptr[3];;

    {
      MELT_LOCATION ("warmelt-outobj.melt:3012:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_#GET_INT__L1*/ meltfnum[0] = 0;
    /*^clear */
	   /*clear *//*_.OUTPUT_C_CODE__V3*/ meltfptr[2] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V4*/ meltfptr[3] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("LAMBDA_", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_70_warmelt_outobj_LAMBDA___9___melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_70_warmelt_outobj_LAMBDA___9__ */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_71_warmelt_outobj_LAMBDA___10__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un * meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un * meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_71_warmelt_outobj_LAMBDA___10___melt =
    melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_71_warmelt_outobj_LAMBDA___10___st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 6
    melt_ptr_t mcfr_varptr[6];
#define MELTFRAM_NBVARNUM 4
    long mcfr_varnum[4];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_71_warmelt_outobj_LAMBDA___10__ is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_71_warmelt_outobj_LAMBDA___10___st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 6; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_71_warmelt_outobj_LAMBDA___10__ nbval 6*/
  meltfram__.mcfr_nbvar = 6 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("LAMBDA_", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-outobj.melt:3022:/ getarg");
 /*_.EXPCUR__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {

 /*_#GET_INT__L1*/ meltfnum[0] =
      (melt_get_int
       ((melt_ptr_t) (( /*~BOXDEPTHP1 */ meltfclos->tabval[2]))));;
    MELT_LOCATION ("warmelt-outobj.melt:3023:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^msend */
    /*msend */
    {
      union meltparam_un argtab[3];
      memset (&argtab, 0, sizeof (argtab));
      /*^ojbmsend.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & ( /*~DECLBUF */ meltfclos->tabval[0]);
      /*^ojbmsend.arg */
      argtab[1].meltbp_aptr =
	(melt_ptr_t *) & ( /*~IMPLBUF */ meltfclos->tabval[1]);
      /*^ojbmsend.arg */
      argtab[2].meltbp_long = /*_#GET_INT__L1*/ meltfnum[0];
      /*_.OUTPUT_C_CODE__V3*/ meltfptr[2] =
	meltgc_send ((melt_ptr_t) ( /*_.EXPCUR__V2*/ meltfptr[1]),
		     (melt_ptr_t) (( /*!OUTPUT_C_CODE */ meltfrout->
				    tabval[0])),
		     (MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_LONG ""),
		     argtab, "", (union meltparam_un *) 0);
    }
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:3024:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#STRBUF_USEDLENGTH__L2*/ meltfnum[1] =
	melt_strbuf_usedlength ((melt_ptr_t)
				(( /*~IMPLBUF */ meltfclos->tabval[1])));;
      MELT_LOCATION ("warmelt-outobj.melt:3025:/ cond");
      /*cond */ if (
		     /*ifisa */
		     melt_is_instance_of ((melt_ptr_t)
					  (( /*!BUFFER_LIMIT_CONT */
					    meltfrout->tabval[1])),
					  (melt_ptr_t) (( /*!CLASS_REFERENCE */ meltfrout->tabval[2])))
	)			/*then */
	{
	  /*^cond.then */
	  /*^getslot */
	  {
	    melt_ptr_t slot = NULL, obj = NULL;
	    obj =
	      (melt_ptr_t) (( /*!BUFFER_LIMIT_CONT */ meltfrout->
			     tabval[1])) /*=obj*/ ;
	    melt_object_get_field (slot, obj, 0, "REFERENCED_VALUE");
     /*_.REFERENCED_VALUE__V5*/ meltfptr[4] = slot;
	  };
	  ;
	}
      else
	{			/*^cond.else */

    /*_.REFERENCED_VALUE__V5*/ meltfptr[4] = NULL;;
	}
      ;
      /*^compute */
   /*_#GET_INT__L3*/ meltfnum[2] =
	(melt_get_int
	 ((melt_ptr_t) ( /*_.REFERENCED_VALUE__V5*/ meltfptr[4])));;
      /*^compute */
   /*_#I__L4*/ meltfnum[3] =
	(( /*_#STRBUF_USEDLENGTH__L2*/ meltfnum[1]) <
	 ( /*_#GET_INT__L3*/ meltfnum[2]));;
      MELT_LOCATION ("warmelt-outobj.melt:3024:/ cond");
      /*cond */ if ( /*_#I__L4*/ meltfnum[3])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V6*/ meltfptr[5] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-outobj.melt:3024:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check limited implbuf"),
				  ("warmelt-outobj.melt")
				  ? ("warmelt-outobj.melt") : __FILE__,
				  (3024) ? (3024) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V6*/ meltfptr[5] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V4*/ meltfptr[3] = /*_.IFELSE___V6*/ meltfptr[5];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:3024:/ clear");
	     /*clear *//*_#STRBUF_USEDLENGTH__L2*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.REFERENCED_VALUE__V5*/ meltfptr[4] = 0;
      /*^clear */
	     /*clear *//*_#GET_INT__L3*/ meltfnum[2] = 0;
      /*^clear */
	     /*clear *//*_#I__L4*/ meltfnum[3] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V6*/ meltfptr[5] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V4*/ meltfptr[3] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-outobj.melt:3022:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.IFCPP___V4*/ meltfptr[3];;

    {
      MELT_LOCATION ("warmelt-outobj.melt:3022:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_#GET_INT__L1*/ meltfnum[0] = 0;
    /*^clear */
	   /*clear *//*_.OUTPUT_C_CODE__V3*/ meltfptr[2] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V4*/ meltfptr[3] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("LAMBDA_", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_71_warmelt_outobj_LAMBDA___10___melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_71_warmelt_outobj_LAMBDA___10__ */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_72_warmelt_outobj_OUTPUCOD_OBJCOND (meltclosure_ptr_t meltclosp_,
					     melt_ptr_t meltfirstargp_,
					     const melt_argdescr_cell_t
					     meltxargdescr_[],
					     union meltparam_un *
					     meltxargtab_,
					     const melt_argdescr_cell_t
					     meltxresdescr_[],
					     union meltparam_un *
					     meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_72_warmelt_outobj_OUTPUCOD_OBJCOND_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_72_warmelt_outobj_OUTPUCOD_OBJCOND_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 22
    melt_ptr_t mcfr_varptr[22];
#define MELTFRAM_NBVARNUM 17
    long mcfr_varnum[17];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_72_warmelt_outobj_OUTPUCOD_OBJCOND is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_72_warmelt_outobj_OUTPUCOD_OBJCOND_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 22; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_72_warmelt_outobj_OUTPUCOD_OBJCOND nbval 22*/
  meltfram__.mcfr_nbvar = 22 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("OUTPUCOD_OBJCOND", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-outobj.melt:3038:/ getarg");
 /*_.OCOND__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.DECLBUF__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.DECLBUF__V3*/ meltfptr[2])) !=
	      NULL);


  /*getarg#2 */
  /*^getarg */
  if (meltxargdescr_[1] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.IMPLBUF__V4*/ meltfptr[3] =
    (meltxargtab_[1].meltbp_aptr) ? (*(meltxargtab_[1].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3])) !=
	      NULL);


  /*getarg#3 */
  /*^getarg */
  if (meltxargdescr_[2] != MELTBPAR_LONG)
    goto lab_endgetargs;
 /*_#DEPTH__L1*/ meltfnum[0] = meltxargtab_[2].meltbp_long;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:3039:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L2*/ meltfnum[1] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.OCOND__V2*/ meltfptr[1]),
			     (melt_ptr_t) (( /*!CLASS_OBJCOND */ meltfrout->
					    tabval[0])));;
      MELT_LOCATION ("warmelt-outobj.melt:3039:/ cond");
      /*cond */ if ( /*_#IS_A__L2*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V6*/ meltfptr[5] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-outobj.melt:3039:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check ocond"),
				  ("warmelt-outobj.melt")
				  ? ("warmelt-outobj.melt") : __FILE__,
				  (3039) ? (3039) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V6*/ meltfptr[5] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V5*/ meltfptr[4] = /*_.IFELSE___V6*/ meltfptr[5];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:3039:/ clear");
	     /*clear *//*_#IS_A__L2*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V6*/ meltfptr[5] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V5*/ meltfptr[4] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-outobj.melt:3040:/ quasiblock");


    /*^getslot */
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.OCOND__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 0, "OBI_LOC");
  /*_.CLOC__V8*/ meltfptr[7] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-outobj.melt:3041:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.OCOND__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 1, "OBCOND_TEST");
  /*_.CTEST__V9*/ meltfptr[8] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-outobj.melt:3042:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.OCOND__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 2, "OBCOND_THEN");
  /*_.CTHEN__V10*/ meltfptr[9] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-outobj.melt:3043:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.OCOND__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 3, "OBCOND_ELSE");
  /*_.CELSE__V11*/ meltfptr[10] = slot;
    };
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:3045:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#NOTNULL__L3*/ meltfnum[1] =
	(( /*_.CTEST__V9*/ meltfptr[8]) != NULL);;
      MELT_LOCATION ("warmelt-outobj.melt:3045:/ cond");
      /*cond */ if ( /*_#NOTNULL__L3*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V13*/ meltfptr[12] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-outobj.melt:3045:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check ctest"),
				  ("warmelt-outobj.melt")
				  ? ("warmelt-outobj.melt") : __FILE__,
				  (3045) ? (3045) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V13*/ meltfptr[12] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V12*/ meltfptr[11] = /*_.IFELSE___V13*/ meltfptr[12];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:3045:/ clear");
	     /*clear *//*_#NOTNULL__L3*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V13*/ meltfptr[12] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V12*/ meltfptr[11] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-outobj.melt:3046:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[3];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.IMPLBUF__V4*/ meltfptr[3];
      /*^apply.arg */
      argtab[1].meltbp_long = /*_#DEPTH__L1*/ meltfnum[0];
      /*^apply.arg */
      argtab[2].meltbp_cstring = "cond";
      /*_.OUTPUT_LOCATION__V14*/ meltfptr[12] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!OUTPUT_LOCATION */ meltfrout->tabval[1])),
		    (melt_ptr_t) ( /*_.CLOC__V8*/ meltfptr[7]),
		    (MELTBPARSTR_PTR MELTBPARSTR_LONG MELTBPARSTR_CSTRING ""),
		    argtab, "", (union meltparam_un *) 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:3047:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   ("/*cond*/ if ("));
    }
    ;
 /*_#I__L4*/ meltfnum[1] =
      (( /*_#DEPTH__L1*/ meltfnum[0]) + (1));;
    MELT_LOCATION ("warmelt-outobj.melt:3048:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^msend */
    /*msend */
    {
      union meltparam_un argtab[3];
      memset (&argtab, 0, sizeof (argtab));
      /*^ojbmsend.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.DECLBUF__V3*/ meltfptr[2];
      /*^ojbmsend.arg */
      argtab[1].meltbp_aptr = (melt_ptr_t *) & /*_.IMPLBUF__V4*/ meltfptr[3];
      /*^ojbmsend.arg */
      argtab[2].meltbp_long = /*_#I__L4*/ meltfnum[1];
      /*_.OUTPUT_C_CODE__V15*/ meltfptr[14] =
	meltgc_send ((melt_ptr_t) ( /*_.CTEST__V9*/ meltfptr[8]),
		     (melt_ptr_t) (( /*!OUTPUT_C_CODE */ meltfrout->
				    tabval[2])),
		     (MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_LONG ""),
		     argtab, "", (union meltparam_un *) 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:3049:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   (") /*then*/ {"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:3050:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				( /*_#DEPTH__L1*/ meltfnum[0]), 0);
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:3051:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^checksignal */
    MELT_CHECK_SIGNAL ();
    ;
    /*^cond */
    /*cond */ if ( /*_.CTHEN__V10*/ meltfptr[9])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

   /*_#IS_A__L6*/ meltfnum[5] =
	    melt_is_instance_of ((melt_ptr_t) ( /*_.CTHEN__V10*/ meltfptr[9]),
				 (melt_ptr_t) (( /*!CLASS_OBJPUREVALUE */
						meltfrout->tabval[3])));;
	  /*^compute */
   /*_#NOT__L7*/ meltfnum[6] =
	    (!( /*_#IS_A__L6*/ meltfnum[5]));;
	  /*^compute */
	  /*_#IF___L5*/ meltfnum[4] = /*_#NOT__L7*/ meltfnum[6];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-outobj.melt:3051:/ clear");
	     /*clear *//*_#IS_A__L6*/ meltfnum[5] = 0;
	  /*^clear */
	     /*clear *//*_#NOT__L7*/ meltfnum[6] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

  /*_#IF___L5*/ meltfnum[4] = 0;;
      }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:3051:/ cond");
    /*cond */ if ( /*_#IF___L5*/ meltfnum[4])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-outobj.melt:3053:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^apply */
	  /*apply */
	  {
	    union meltparam_un argtab[3];
	    memset (&argtab, 0, sizeof (argtab));
	    /*^apply.arg */
	    argtab[0].meltbp_aptr =
	      (melt_ptr_t *) & /*_.IMPLBUF__V4*/ meltfptr[3];
	    /*^apply.arg */
	    argtab[1].meltbp_long = /*_#DEPTH__L1*/ meltfnum[0];
	    /*^apply.arg */
	    argtab[2].meltbp_cstring = "cond.then";
	    /*_.OUTPUT_LOCATION__V16*/ meltfptr[15] =
	      melt_apply ((meltclosure_ptr_t)
			  (( /*!OUTPUT_LOCATION */ meltfrout->tabval[1])),
			  (melt_ptr_t) ( /*_.CLOC__V8*/ meltfptr[7]),
			  (MELTBPARSTR_PTR MELTBPARSTR_LONG
			   MELTBPARSTR_CSTRING ""), argtab, "",
			  (union meltparam_un *) 0);
	  }
	  ;
   /*_#I__L8*/ meltfnum[5] =
	    (( /*_#DEPTH__L1*/ meltfnum[0]) + (1));;
	  MELT_LOCATION ("warmelt-outobj.melt:3054:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^msend */
	  /*msend */
	  {
	    union meltparam_un argtab[3];
	    memset (&argtab, 0, sizeof (argtab));
	    /*^ojbmsend.arg */
	    argtab[0].meltbp_aptr =
	      (melt_ptr_t *) & /*_.DECLBUF__V3*/ meltfptr[2];
	    /*^ojbmsend.arg */
	    argtab[1].meltbp_aptr =
	      (melt_ptr_t *) & /*_.IMPLBUF__V4*/ meltfptr[3];
	    /*^ojbmsend.arg */
	    argtab[2].meltbp_long = /*_#I__L8*/ meltfnum[5];
	    /*_.OUTPUT_C_CODE__V17*/ meltfptr[16] =
	      meltgc_send ((melt_ptr_t) ( /*_.CTHEN__V10*/ meltfptr[9]),
			   (melt_ptr_t) (( /*!OUTPUT_C_CODE */ meltfrout->
					  tabval[2])),
			   (MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_LONG
			    ""), argtab, "", (union meltparam_un *) 0);
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:3055:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t)
				 ( /*_.IMPLBUF__V4*/ meltfptr[3]), (";"));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:3056:/ locexp");
	    meltgc_strbuf_add_indent ((melt_ptr_t)
				      ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				      ( /*_#DEPTH__L1*/ meltfnum[0]), 0);
	  }
	  ;
	  MELT_LOCATION ("warmelt-outobj.melt:3052:/ quasiblock");


	  /*epilog */

	  MELT_LOCATION ("warmelt-outobj.melt:3051:/ clear");
	     /*clear *//*_.OUTPUT_LOCATION__V16*/ meltfptr[15] = 0;
	  /*^clear */
	     /*clear *//*_#I__L8*/ meltfnum[5] = 0;
	  /*^clear */
	     /*clear *//*_.OUTPUT_C_CODE__V17*/ meltfptr[16] = 0;
	}
	;
      }				/*noelse */
    ;
    MELT_LOCATION ("warmelt-outobj.melt:3059:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^checksignal */
    MELT_CHECK_SIGNAL ();
    ;
    /*^cond */
    /*cond */ if ( /*_.CELSE__V11*/ meltfptr[10])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

   /*_#IS_A__L10*/ meltfnum[5] =
	    melt_is_instance_of ((melt_ptr_t)
				 ( /*_.CELSE__V11*/ meltfptr[10]),
				 (melt_ptr_t) (( /*!CLASS_OBJPUREVALUE */
						meltfrout->tabval[3])));;
	  /*^compute */
   /*_#NOT__L11*/ meltfnum[10] =
	    (!( /*_#IS_A__L10*/ meltfnum[5]));;
	  /*^compute */
	  /*_#IF___L9*/ meltfnum[6] = /*_#NOT__L11*/ meltfnum[10];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-outobj.melt:3059:/ clear");
	     /*clear *//*_#IS_A__L10*/ meltfnum[5] = 0;
	  /*^clear */
	     /*clear *//*_#NOT__L11*/ meltfnum[10] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

  /*_#IF___L9*/ meltfnum[6] = 0;;
      }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:3059:/ cond");
    /*cond */ if ( /*_#IF___L9*/ meltfnum[6])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{


	  {
	    MELT_LOCATION ("warmelt-outobj.melt:3061:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t)
				 ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				 ("} else {"));
	  }
	  ;
	  MELT_LOCATION ("warmelt-outobj.melt:3062:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^apply */
	  /*apply */
	  {
	    union meltparam_un argtab[3];
	    memset (&argtab, 0, sizeof (argtab));
	    /*^apply.arg */
	    argtab[0].meltbp_aptr =
	      (melt_ptr_t *) & /*_.IMPLBUF__V4*/ meltfptr[3];
	    /*^apply.arg */
	    argtab[1].meltbp_long = /*_#DEPTH__L1*/ meltfnum[0];
	    /*^apply.arg */
	    argtab[2].meltbp_cstring = "cond.else";
	    /*_.OUTPUT_LOCATION__V18*/ meltfptr[15] =
	      melt_apply ((meltclosure_ptr_t)
			  (( /*!OUTPUT_LOCATION */ meltfrout->tabval[1])),
			  (melt_ptr_t) ( /*_.CLOC__V8*/ meltfptr[7]),
			  (MELTBPARSTR_PTR MELTBPARSTR_LONG
			   MELTBPARSTR_CSTRING ""), argtab, "",
			  (union meltparam_un *) 0);
	  }
	  ;
   /*_#I__L12*/ meltfnum[5] =
	    (( /*_#DEPTH__L1*/ meltfnum[0]) + (1));;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:3063:/ locexp");
	    meltgc_strbuf_add_indent ((melt_ptr_t)
				      ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				      ( /*_#I__L12*/ meltfnum[5]), 0);
	  }
	  ;
   /*_#I__L13*/ meltfnum[10] =
	    (( /*_#DEPTH__L1*/ meltfnum[0]) + (1));;
	  MELT_LOCATION ("warmelt-outobj.melt:3064:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^msend */
	  /*msend */
	  {
	    union meltparam_un argtab[3];
	    memset (&argtab, 0, sizeof (argtab));
	    /*^ojbmsend.arg */
	    argtab[0].meltbp_aptr =
	      (melt_ptr_t *) & /*_.DECLBUF__V3*/ meltfptr[2];
	    /*^ojbmsend.arg */
	    argtab[1].meltbp_aptr =
	      (melt_ptr_t *) & /*_.IMPLBUF__V4*/ meltfptr[3];
	    /*^ojbmsend.arg */
	    argtab[2].meltbp_long = /*_#I__L13*/ meltfnum[10];
	    /*_.OUTPUT_C_CODE__V19*/ meltfptr[16] =
	      meltgc_send ((melt_ptr_t) ( /*_.CELSE__V11*/ meltfptr[10]),
			   (melt_ptr_t) (( /*!OUTPUT_C_CODE */ meltfrout->
					  tabval[2])),
			   (MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_LONG
			    ""), argtab, "", (union meltparam_un *) 0);
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:3065:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t)
				 ( /*_.IMPLBUF__V4*/ meltfptr[3]), (";"));
	  }
	  ;
   /*_#I__L14*/ meltfnum[13] =
	    (( /*_#DEPTH__L1*/ meltfnum[0]) + (1));;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:3066:/ locexp");
	    meltgc_strbuf_add_indent ((melt_ptr_t)
				      ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				      ( /*_#I__L14*/ meltfnum[13]), 0);
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:3067:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t)
				 ( /*_.IMPLBUF__V4*/ meltfptr[3]), ("}"));
	  }
	  ;
	  MELT_LOCATION ("warmelt-outobj.melt:3060:/ quasiblock");


	  /*epilog */

	  MELT_LOCATION ("warmelt-outobj.melt:3059:/ clear");
	     /*clear *//*_.OUTPUT_LOCATION__V18*/ meltfptr[15] = 0;
	  /*^clear */
	     /*clear *//*_#I__L12*/ meltfnum[5] = 0;
	  /*^clear */
	     /*clear *//*_#I__L13*/ meltfnum[10] = 0;
	  /*^clear */
	     /*clear *//*_.OUTPUT_C_CODE__V19*/ meltfptr[16] = 0;
	  /*^clear */
	     /*clear *//*_#I__L14*/ meltfnum[13] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

	/*^block */
	/*anyblock */
	{


	  {
	    MELT_LOCATION ("warmelt-outobj.melt:3069:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t)
				 ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				 ("} /*noelse*/"));
	  }
	  ;
	  /*epilog */
	}
	;
      }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:3071:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				( /*_#DEPTH__L1*/ meltfnum[0]), 0);
    }
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:3072:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#STRBUF_USEDLENGTH__L15*/ meltfnum[5] =
	melt_strbuf_usedlength ((melt_ptr_t)
				( /*_.IMPLBUF__V4*/ meltfptr[3]));;
      MELT_LOCATION ("warmelt-outobj.melt:3073:/ cond");
      /*cond */ if (
		     /*ifisa */
		     melt_is_instance_of ((melt_ptr_t)
					  (( /*!BUFFER_LIMIT_CONT */
					    meltfrout->tabval[4])),
					  (melt_ptr_t) (( /*!CLASS_REFERENCE */ meltfrout->tabval[5])))
	)			/*then */
	{
	  /*^cond.then */
	  /*^getslot */
	  {
	    melt_ptr_t slot = NULL, obj = NULL;
	    obj =
	      (melt_ptr_t) (( /*!BUFFER_LIMIT_CONT */ meltfrout->
			     tabval[4])) /*=obj*/ ;
	    melt_object_get_field (slot, obj, 0, "REFERENCED_VALUE");
     /*_.REFERENCED_VALUE__V21*/ meltfptr[16] = slot;
	  };
	  ;
	}
      else
	{			/*^cond.else */

    /*_.REFERENCED_VALUE__V21*/ meltfptr[16] = NULL;;
	}
      ;
      /*^compute */
   /*_#GET_INT__L16*/ meltfnum[10] =
	(melt_get_int
	 ((melt_ptr_t) ( /*_.REFERENCED_VALUE__V21*/ meltfptr[16])));;
      /*^compute */
   /*_#I__L17*/ meltfnum[13] =
	(( /*_#STRBUF_USEDLENGTH__L15*/ meltfnum[5]) <
	 ( /*_#GET_INT__L16*/ meltfnum[10]));;
      MELT_LOCATION ("warmelt-outobj.melt:3072:/ cond");
      /*cond */ if ( /*_#I__L17*/ meltfnum[13])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V22*/ meltfptr[21] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-outobj.melt:3072:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check limited implbuf"),
				  ("warmelt-outobj.melt")
				  ? ("warmelt-outobj.melt") : __FILE__,
				  (3072) ? (3072) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V22*/ meltfptr[21] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V20*/ meltfptr[15] = /*_.IFELSE___V22*/ meltfptr[21];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:3072:/ clear");
	     /*clear *//*_#STRBUF_USEDLENGTH__L15*/ meltfnum[5] = 0;
      /*^clear */
	     /*clear *//*_.REFERENCED_VALUE__V21*/ meltfptr[16] = 0;
      /*^clear */
	     /*clear *//*_#GET_INT__L16*/ meltfnum[10] = 0;
      /*^clear */
	     /*clear *//*_#I__L17*/ meltfnum[13] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V22*/ meltfptr[21] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V20*/ meltfptr[15] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    /*^compute */
    /*_.LET___V7*/ meltfptr[5] = /*_.IFCPP___V20*/ meltfptr[15];;

    MELT_LOCATION ("warmelt-outobj.melt:3040:/ clear");
	   /*clear *//*_.CLOC__V8*/ meltfptr[7] = 0;
    /*^clear */
	   /*clear *//*_.CTEST__V9*/ meltfptr[8] = 0;
    /*^clear */
	   /*clear *//*_.CTHEN__V10*/ meltfptr[9] = 0;
    /*^clear */
	   /*clear *//*_.CELSE__V11*/ meltfptr[10] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V12*/ meltfptr[11] = 0;
    /*^clear */
	   /*clear *//*_.OUTPUT_LOCATION__V14*/ meltfptr[12] = 0;
    /*^clear */
	   /*clear *//*_#I__L4*/ meltfnum[1] = 0;
    /*^clear */
	   /*clear *//*_.OUTPUT_C_CODE__V15*/ meltfptr[14] = 0;
    /*^clear */
	   /*clear *//*_#IF___L5*/ meltfnum[4] = 0;
    /*^clear */
	   /*clear *//*_#IF___L9*/ meltfnum[6] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V20*/ meltfptr[15] = 0;
    MELT_LOCATION ("warmelt-outobj.melt:3038:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.LET___V7*/ meltfptr[5];;

    {
      MELT_LOCATION ("warmelt-outobj.melt:3038:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V5*/ meltfptr[4] = 0;
    /*^clear */
	   /*clear *//*_.LET___V7*/ meltfptr[5] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("OUTPUCOD_OBJCOND", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_72_warmelt_outobj_OUTPUCOD_OBJCOND_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_72_warmelt_outobj_OUTPUCOD_OBJCOND */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_73_warmelt_outobj_OUTPUCOD_OBJCPPIF (meltclosure_ptr_t meltclosp_,
					      melt_ptr_t meltfirstargp_,
					      const melt_argdescr_cell_t
					      meltxargdescr_[],
					      union meltparam_un *
					      meltxargtab_,
					      const melt_argdescr_cell_t
					      meltxresdescr_[],
					      union meltparam_un *
					      meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_73_warmelt_outobj_OUTPUCOD_OBJCPPIF_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_73_warmelt_outobj_OUTPUCOD_OBJCPPIF_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 21
    melt_ptr_t mcfr_varptr[21];
#define MELTFRAM_NBVARNUM 7
    long mcfr_varnum[7];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_73_warmelt_outobj_OUTPUCOD_OBJCPPIF is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_73_warmelt_outobj_OUTPUCOD_OBJCPPIF_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 21; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_73_warmelt_outobj_OUTPUCOD_OBJCPPIF nbval 21*/
  meltfram__.mcfr_nbvar = 21 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("OUTPUCOD_OBJCPPIF", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-outobj.melt:3080:/ getarg");
 /*_.OPIF__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.DECLBUF__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.DECLBUF__V3*/ meltfptr[2])) !=
	      NULL);


  /*getarg#2 */
  /*^getarg */
  if (meltxargdescr_[1] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.IMPLBUF__V4*/ meltfptr[3] =
    (meltxargtab_[1].meltbp_aptr) ? (*(meltxargtab_[1].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3])) !=
	      NULL);


  /*getarg#3 */
  /*^getarg */
  if (meltxargdescr_[2] != MELTBPAR_LONG)
    goto lab_endgetargs;
 /*_#DEPTH__L1*/ meltfnum[0] = meltxargtab_[2].meltbp_long;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:3081:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L2*/ meltfnum[1] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.OPIF__V2*/ meltfptr[1]),
			     (melt_ptr_t) (( /*!CLASS_OBJCPPIF */ meltfrout->
					    tabval[0])));;
      MELT_LOCATION ("warmelt-outobj.melt:3081:/ cond");
      /*cond */ if ( /*_#IS_A__L2*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V6*/ meltfptr[5] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-outobj.melt:3081:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check opif"),
				  ("warmelt-outobj.melt")
				  ? ("warmelt-outobj.melt") : __FILE__,
				  (3081) ? (3081) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V6*/ meltfptr[5] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V5*/ meltfptr[4] = /*_.IFELSE___V6*/ meltfptr[5];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:3081:/ clear");
	     /*clear *//*_#IS_A__L2*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V6*/ meltfptr[5] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V5*/ meltfptr[4] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-outobj.melt:3082:/ quasiblock");


    /*^getslot */
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.OPIF__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 0, "OBI_LOC");
  /*_.CLOC__V8*/ meltfptr[7] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-outobj.melt:3083:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.OPIF__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 1, "OBIFP_COND");
  /*_.CCOND__V9*/ meltfptr[8] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-outobj.melt:3084:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.OPIF__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 2, "OBIFP_THEN");
  /*_.CTHEN__V10*/ meltfptr[9] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-outobj.melt:3085:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.OPIF__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 3, "OBIFP_ELSE");
  /*_.CELSE__V11*/ meltfptr[10] = slot;
    };
    ;
 /*_#DEPTHP1__L3*/ meltfnum[1] =
      ((1) + ( /*_#DEPTH__L1*/ meltfnum[0]));;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:3088:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_STRING__L4*/ meltfnum[3] =
	(melt_magic_discr ((melt_ptr_t) ( /*_.CCOND__V9*/ meltfptr[8])) ==
	 MELTOBMAG_STRING);;
      MELT_LOCATION ("warmelt-outobj.melt:3088:/ cond");
      /*cond */ if ( /*_#IS_STRING__L4*/ meltfnum[3])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V13*/ meltfptr[12] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-outobj.melt:3088:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check ccond"),
				  ("warmelt-outobj.melt")
				  ? ("warmelt-outobj.melt") : __FILE__,
				  (3088) ? (3088) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V13*/ meltfptr[12] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V12*/ meltfptr[11] = /*_.IFELSE___V13*/ meltfptr[12];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:3088:/ clear");
	     /*clear *//*_#IS_STRING__L4*/ meltfnum[3] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V13*/ meltfptr[12] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V12*/ meltfptr[11] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-outobj.melt:3089:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[3];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.IMPLBUF__V4*/ meltfptr[3];
      /*^apply.arg */
      argtab[1].meltbp_long = /*_#DEPTH__L1*/ meltfnum[0];
      /*^apply.arg */
      argtab[2].meltbp_cstring = "cppif";
      /*_.OUTPUT_RAW_LOCATION__V14*/ meltfptr[12] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!OUTPUT_RAW_LOCATION */ meltfrout->tabval[1])),
		    (melt_ptr_t) ( /*_.CLOC__V8*/ meltfptr[7]),
		    (MELTBPARSTR_PTR MELTBPARSTR_LONG MELTBPARSTR_CSTRING ""),
		    argtab, "", (union meltparam_un *) 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:3090:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   ("#if "));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:3091:/ locexp");
      /*add2sbuf_string */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   melt_string_str ((melt_ptr_t)
					    ( /*_.CCOND__V9*/ meltfptr[8])));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:3092:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				( /*_#DEPTHP1__L3*/ meltfnum[1]), 0);
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:3093:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[3];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.IMPLBUF__V4*/ meltfptr[3];
      /*^apply.arg */
      argtab[1].meltbp_long = /*_#DEPTH__L1*/ meltfnum[0];
      /*^apply.arg */
      argtab[2].meltbp_cstring = "cppif.then";
      /*_.OUTPUT_LOCATION__V15*/ meltfptr[14] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!OUTPUT_LOCATION */ meltfrout->tabval[2])),
		    (melt_ptr_t) ( /*_.CLOC__V8*/ meltfptr[7]),
		    (MELTBPARSTR_PTR MELTBPARSTR_LONG MELTBPARSTR_CSTRING ""),
		    argtab, "", (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:3094:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^msend */
    /*msend */
    {
      union meltparam_un argtab[3];
      memset (&argtab, 0, sizeof (argtab));
      /*^ojbmsend.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.DECLBUF__V3*/ meltfptr[2];
      /*^ojbmsend.arg */
      argtab[1].meltbp_aptr = (melt_ptr_t *) & /*_.IMPLBUF__V4*/ meltfptr[3];
      /*^ojbmsend.arg */
      argtab[2].meltbp_long = /*_#DEPTHP1__L3*/ meltfnum[1];
      /*_.OUTPUT_C_CODE__V16*/ meltfptr[15] =
	meltgc_send ((melt_ptr_t) ( /*_.CTHEN__V10*/ meltfptr[9]),
		     (melt_ptr_t) (( /*!OUTPUT_C_CODE */ meltfrout->
				    tabval[3])),
		     (MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_LONG ""),
		     argtab, "", (union meltparam_un *) 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:3095:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				( /*_#DEPTHP1__L3*/ meltfnum[1]), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:3096:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   ("#else /*"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:3097:/ locexp");
      /*add2sbuf_string */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   melt_string_str ((melt_ptr_t)
					    ( /*_.CCOND__V9*/ meltfptr[8])));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:3098:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   ("*/"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:3099:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				( /*_#DEPTHP1__L3*/ meltfnum[1]), 0);
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:3100:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[3];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.IMPLBUF__V4*/ meltfptr[3];
      /*^apply.arg */
      argtab[1].meltbp_long = /*_#DEPTH__L1*/ meltfnum[0];
      /*^apply.arg */
      argtab[2].meltbp_cstring = "cppif.else";
      /*_.OUTPUT_LOCATION__V17*/ meltfptr[16] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!OUTPUT_LOCATION */ meltfrout->tabval[2])),
		    (melt_ptr_t) ( /*_.CLOC__V8*/ meltfptr[7]),
		    (MELTBPARSTR_PTR MELTBPARSTR_LONG MELTBPARSTR_CSTRING ""),
		    argtab, "", (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:3101:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^msend */
    /*msend */
    {
      union meltparam_un argtab[3];
      memset (&argtab, 0, sizeof (argtab));
      /*^ojbmsend.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.DECLBUF__V3*/ meltfptr[2];
      /*^ojbmsend.arg */
      argtab[1].meltbp_aptr = (melt_ptr_t *) & /*_.IMPLBUF__V4*/ meltfptr[3];
      /*^ojbmsend.arg */
      argtab[2].meltbp_long = /*_#DEPTHP1__L3*/ meltfnum[1];
      /*_.OUTPUT_C_CODE__V18*/ meltfptr[17] =
	meltgc_send ((melt_ptr_t) ( /*_.CELSE__V11*/ meltfptr[10]),
		     (melt_ptr_t) (( /*!OUTPUT_C_CODE */ meltfrout->
				    tabval[3])),
		     (MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_LONG ""),
		     argtab, "", (union meltparam_un *) 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:3102:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				( /*_#DEPTHP1__L3*/ meltfnum[1]), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:3103:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   ("#endif /*"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:3104:/ locexp");
      /*add2sbuf_string */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   melt_string_str ((melt_ptr_t)
					    ( /*_.CCOND__V9*/ meltfptr[8])));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:3105:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   ("*/"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:3106:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				( /*_#DEPTHP1__L3*/ meltfnum[1]), 0);
    }
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:3107:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#STRBUF_USEDLENGTH__L5*/ meltfnum[3] =
	melt_strbuf_usedlength ((melt_ptr_t)
				( /*_.IMPLBUF__V4*/ meltfptr[3]));;
      MELT_LOCATION ("warmelt-outobj.melt:3108:/ cond");
      /*cond */ if (
		     /*ifisa */
		     melt_is_instance_of ((melt_ptr_t)
					  (( /*!BUFFER_LIMIT_CONT */
					    meltfrout->tabval[4])),
					  (melt_ptr_t) (( /*!CLASS_REFERENCE */ meltfrout->tabval[5])))
	)			/*then */
	{
	  /*^cond.then */
	  /*^getslot */
	  {
	    melt_ptr_t slot = NULL, obj = NULL;
	    obj =
	      (melt_ptr_t) (( /*!BUFFER_LIMIT_CONT */ meltfrout->
			     tabval[4])) /*=obj*/ ;
	    melt_object_get_field (slot, obj, 0, "REFERENCED_VALUE");
     /*_.REFERENCED_VALUE__V20*/ meltfptr[19] = slot;
	  };
	  ;
	}
      else
	{			/*^cond.else */

    /*_.REFERENCED_VALUE__V20*/ meltfptr[19] = NULL;;
	}
      ;
      /*^compute */
   /*_#GET_INT__L6*/ meltfnum[5] =
	(melt_get_int
	 ((melt_ptr_t) ( /*_.REFERENCED_VALUE__V20*/ meltfptr[19])));;
      /*^compute */
   /*_#I__L7*/ meltfnum[6] =
	(( /*_#STRBUF_USEDLENGTH__L5*/ meltfnum[3]) <
	 ( /*_#GET_INT__L6*/ meltfnum[5]));;
      MELT_LOCATION ("warmelt-outobj.melt:3107:/ cond");
      /*cond */ if ( /*_#I__L7*/ meltfnum[6])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V21*/ meltfptr[20] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-outobj.melt:3107:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check limited implbuf"),
				  ("warmelt-outobj.melt")
				  ? ("warmelt-outobj.melt") : __FILE__,
				  (3107) ? (3107) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V21*/ meltfptr[20] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V19*/ meltfptr[18] = /*_.IFELSE___V21*/ meltfptr[20];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:3107:/ clear");
	     /*clear *//*_#STRBUF_USEDLENGTH__L5*/ meltfnum[3] = 0;
      /*^clear */
	     /*clear *//*_.REFERENCED_VALUE__V20*/ meltfptr[19] = 0;
      /*^clear */
	     /*clear *//*_#GET_INT__L6*/ meltfnum[5] = 0;
      /*^clear */
	     /*clear *//*_#I__L7*/ meltfnum[6] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V21*/ meltfptr[20] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V19*/ meltfptr[18] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    /*^compute */
    /*_.LET___V7*/ meltfptr[5] = /*_.IFCPP___V19*/ meltfptr[18];;

    MELT_LOCATION ("warmelt-outobj.melt:3082:/ clear");
	   /*clear *//*_.CLOC__V8*/ meltfptr[7] = 0;
    /*^clear */
	   /*clear *//*_.CCOND__V9*/ meltfptr[8] = 0;
    /*^clear */
	   /*clear *//*_.CTHEN__V10*/ meltfptr[9] = 0;
    /*^clear */
	   /*clear *//*_.CELSE__V11*/ meltfptr[10] = 0;
    /*^clear */
	   /*clear *//*_#DEPTHP1__L3*/ meltfnum[1] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V12*/ meltfptr[11] = 0;
    /*^clear */
	   /*clear *//*_.OUTPUT_RAW_LOCATION__V14*/ meltfptr[12] = 0;
    /*^clear */
	   /*clear *//*_.OUTPUT_LOCATION__V15*/ meltfptr[14] = 0;
    /*^clear */
	   /*clear *//*_.OUTPUT_C_CODE__V16*/ meltfptr[15] = 0;
    /*^clear */
	   /*clear *//*_.OUTPUT_LOCATION__V17*/ meltfptr[16] = 0;
    /*^clear */
	   /*clear *//*_.OUTPUT_C_CODE__V18*/ meltfptr[17] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V19*/ meltfptr[18] = 0;
    MELT_LOCATION ("warmelt-outobj.melt:3080:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.LET___V7*/ meltfptr[5];;

    {
      MELT_LOCATION ("warmelt-outobj.melt:3080:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V5*/ meltfptr[4] = 0;
    /*^clear */
	   /*clear *//*_.LET___V7*/ meltfptr[5] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("OUTPUCOD_OBJCPPIF", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_73_warmelt_outobj_OUTPUCOD_OBJCPPIF_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_73_warmelt_outobj_OUTPUCOD_OBJCPPIF */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_74_warmelt_outobj_OUTPUCOD_OBJINTERNSYMBOL (meltclosure_ptr_t
						     meltclosp_,
						     melt_ptr_t
						     meltfirstargp_,
						     const
						     melt_argdescr_cell_t
						     meltxargdescr_[],
						     union meltparam_un *
						     meltxargtab_,
						     const
						     melt_argdescr_cell_t
						     meltxresdescr_[],
						     union meltparam_un *
						     meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_74_warmelt_outobj_OUTPUCOD_OBJINTERNSYMBOL_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_74_warmelt_outobj_OUTPUCOD_OBJINTERNSYMBOL_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 23
    melt_ptr_t mcfr_varptr[23];
#define MELTFRAM_NBVARNUM 7
    long mcfr_varnum[7];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_74_warmelt_outobj_OUTPUCOD_OBJINTERNSYMBOL is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_74_warmelt_outobj_OUTPUCOD_OBJINTERNSYMBOL_st
	 *) meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 23; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_74_warmelt_outobj_OUTPUCOD_OBJINTERNSYMBOL nbval 23*/
  meltfram__.mcfr_nbvar = 23 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("OUTPUCOD_OBJINTERNSYMBOL", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-outobj.melt:3113:/ getarg");
 /*_.OISY__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.DECLBUF__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.DECLBUF__V3*/ meltfptr[2])) !=
	      NULL);


  /*getarg#2 */
  /*^getarg */
  if (meltxargdescr_[1] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.IMPLBUF__V4*/ meltfptr[3] =
    (meltxargtab_[1].meltbp_aptr) ? (*(meltxargtab_[1].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3])) !=
	      NULL);


  /*getarg#3 */
  /*^getarg */
  if (meltxargdescr_[2] != MELTBPAR_LONG)
    goto lab_endgetargs;
 /*_#DEPTH__L1*/ meltfnum[0] = meltxargtab_[2].meltbp_long;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:3114:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L2*/ meltfnum[1] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.OISY__V2*/ meltfptr[1]),
			     (melt_ptr_t) (( /*!CLASS_OBJINTERNSYMBOL */
					    meltfrout->tabval[0])));;
      MELT_LOCATION ("warmelt-outobj.melt:3114:/ cond");
      /*cond */ if ( /*_#IS_A__L2*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V6*/ meltfptr[5] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-outobj.melt:3114:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check oisy"),
				  ("warmelt-outobj.melt")
				  ? ("warmelt-outobj.melt") : __FILE__,
				  (3114) ? (3114) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V6*/ meltfptr[5] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V5*/ meltfptr[4] = /*_.IFELSE___V6*/ meltfptr[5];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:3114:/ clear");
	     /*clear *//*_#IS_A__L2*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V6*/ meltfptr[5] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V5*/ meltfptr[4] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-outobj.melt:3115:/ quasiblock");


    /*^getslot */
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.OISY__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 0, "OBI_LOC");
  /*_.CLOC__V8*/ meltfptr[7] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-outobj.melt:3116:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.OISY__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 1, "OBINTERN_IOBJ");
  /*_.OIOBJ__V9*/ meltfptr[8] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-outobj.melt:3117:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.OIOBJ__V9*/ meltfptr[8]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 2, "OIE_DATA");
  /*_.OIDAT__V10*/ meltfptr[9] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-outobj.melt:3118:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.OIOBJ__V9*/ meltfptr[8]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 4, "OIE_LOCVAR");
  /*_.OILOCV__V11*/ meltfptr[10] = slot;
    };
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:3120:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L3*/ meltfnum[1] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.OIOBJ__V9*/ meltfptr[8]),
			     (melt_ptr_t) (( /*!CLASS_OBJINITOBJECT */
					    meltfrout->tabval[1])));;
      MELT_LOCATION ("warmelt-outobj.melt:3120:/ cond");
      /*cond */ if ( /*_#IS_A__L3*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V13*/ meltfptr[12] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-outobj.melt:3120:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check oiobj"),
				  ("warmelt-outobj.melt")
				  ? ("warmelt-outobj.melt") : __FILE__,
				  (3120) ? (3120) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V13*/ meltfptr[12] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V12*/ meltfptr[11] = /*_.IFELSE___V13*/ meltfptr[12];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:3120:/ clear");
	     /*clear *//*_#IS_A__L3*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V13*/ meltfptr[12] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V12*/ meltfptr[11] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:3121:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L4*/ meltfnum[1] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.OIDAT__V10*/ meltfptr[9]),
			     (melt_ptr_t) (( /*!CLASS_NREP_DATASYMBOL */
					    meltfrout->tabval[2])));;
      MELT_LOCATION ("warmelt-outobj.melt:3121:/ cond");
      /*cond */ if ( /*_#IS_A__L4*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V15*/ meltfptr[14] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-outobj.melt:3121:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check oidat"),
				  ("warmelt-outobj.melt")
				  ? ("warmelt-outobj.melt") : __FILE__,
				  (3121) ? (3121) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V15*/ meltfptr[14] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V14*/ meltfptr[12] = /*_.IFELSE___V15*/ meltfptr[14];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:3121:/ clear");
	     /*clear *//*_#IS_A__L4*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V15*/ meltfptr[14] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V14*/ meltfptr[12] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-outobj.melt:3122:/ quasiblock");


    /*^getslot */
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.OIDAT__V10*/ meltfptr[9]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 9, "NDSY_NAMESTR");
  /*_.NSY__V16*/ meltfptr[14] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-outobj.melt:3123:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^cond */
    /*cond */ if ( /*_.CLOC__V8*/ meltfptr[7])	/*then */
      {
	/*^cond.then */
	/*_.IFELSE___V17*/ meltfptr[16] = /*_.CLOC__V8*/ meltfptr[7];;
      }
    else
      {
	MELT_LOCATION ("warmelt-outobj.melt:3123:/ cond.else");

	/*^block */
	/*anyblock */
	{

	  /*^getslot */
	  {
	    melt_ptr_t slot = NULL, obj = NULL;
	    obj = (melt_ptr_t) ( /*_.OIDAT__V10*/ meltfptr[9]) /*=obj*/ ;
	    melt_object_get_field (slot, obj, 0, "NREP_LOC");
    /*_.NREP_LOC__V18*/ meltfptr[17] = slot;
	  };
	  ;
	  /*_.IFELSE___V17*/ meltfptr[16] = /*_.NREP_LOC__V18*/ meltfptr[17];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-outobj.melt:3123:/ clear");
	     /*clear *//*_.NREP_LOC__V18*/ meltfptr[17] = 0;
	}
	;
      }
    ;
    /*^checksignal */
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[3];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.IMPLBUF__V4*/ meltfptr[3];
      /*^apply.arg */
      argtab[1].meltbp_long = 1;
      /*^apply.arg */
      argtab[2].meltbp_cstring = "internsymbol";
      /*_.OUTPUT_LOCATION__V19*/ meltfptr[17] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!OUTPUT_LOCATION */ meltfrout->tabval[3])),
		    (melt_ptr_t) ( /*_.IFELSE___V17*/ meltfptr[16]),
		    (MELTBPARSTR_PTR MELTBPARSTR_LONG MELTBPARSTR_CSTRING ""),
		    argtab, "", (union meltparam_un *) 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:3124:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   ("/*internsym:"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:3125:/ locexp");
      /*add2sbuf_string */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   melt_string_str ((melt_ptr_t)
					    ( /*_.NSY__V16*/ meltfptr[14])));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:3126:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   ("*/"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:3127:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				( /*_#DEPTH__L1*/ meltfnum[0]), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:3128:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   ("(void) meltgc_intern_symbol((melt_ptr_t)("));
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:3129:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^msend */
    /*msend */
    {
      union meltparam_un argtab[3];
      memset (&argtab, 0, sizeof (argtab));
      /*^ojbmsend.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.DECLBUF__V3*/ meltfptr[2];
      /*^ojbmsend.arg */
      argtab[1].meltbp_aptr = (melt_ptr_t *) & /*_.IMPLBUF__V4*/ meltfptr[3];
      /*^ojbmsend.arg */
      argtab[2].meltbp_long = /*_#DEPTH__L1*/ meltfnum[0];
      /*_.OUTPUT_C_CODE__V20*/ meltfptr[19] =
	meltgc_send ((melt_ptr_t) ( /*_.OILOCV__V11*/ meltfptr[10]),
		     (melt_ptr_t) (( /*!OUTPUT_C_CODE */ meltfrout->
				    tabval[4])),
		     (MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_LONG ""),
		     argtab, "", (union meltparam_un *) 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:3130:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   ("));"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:3131:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				( /*_#DEPTH__L1*/ meltfnum[0]), 0);
    }
    ;

    MELT_LOCATION ("warmelt-outobj.melt:3122:/ clear");
	   /*clear *//*_.NSY__V16*/ meltfptr[14] = 0;
    /*^clear */
	   /*clear *//*_.IFELSE___V17*/ meltfptr[16] = 0;
    /*^clear */
	   /*clear *//*_.OUTPUT_LOCATION__V19*/ meltfptr[17] = 0;
    /*^clear */
	   /*clear *//*_.OUTPUT_C_CODE__V20*/ meltfptr[19] = 0;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:3132:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#STRBUF_USEDLENGTH__L5*/ meltfnum[1] =
	melt_strbuf_usedlength ((melt_ptr_t)
				( /*_.IMPLBUF__V4*/ meltfptr[3]));;
      MELT_LOCATION ("warmelt-outobj.melt:3133:/ cond");
      /*cond */ if (
		     /*ifisa */
		     melt_is_instance_of ((melt_ptr_t)
					  (( /*!BUFFER_LIMIT_CONT */
					    meltfrout->tabval[5])),
					  (melt_ptr_t) (( /*!CLASS_REFERENCE */ meltfrout->tabval[6])))
	)			/*then */
	{
	  /*^cond.then */
	  /*^getslot */
	  {
	    melt_ptr_t slot = NULL, obj = NULL;
	    obj =
	      (melt_ptr_t) (( /*!BUFFER_LIMIT_CONT */ meltfrout->
			     tabval[5])) /*=obj*/ ;
	    melt_object_get_field (slot, obj, 0, "REFERENCED_VALUE");
     /*_.REFERENCED_VALUE__V22*/ meltfptr[16] = slot;
	  };
	  ;
	}
      else
	{			/*^cond.else */

    /*_.REFERENCED_VALUE__V22*/ meltfptr[16] = NULL;;
	}
      ;
      /*^compute */
   /*_#GET_INT__L6*/ meltfnum[5] =
	(melt_get_int
	 ((melt_ptr_t) ( /*_.REFERENCED_VALUE__V22*/ meltfptr[16])));;
      /*^compute */
   /*_#I__L7*/ meltfnum[6] =
	(( /*_#STRBUF_USEDLENGTH__L5*/ meltfnum[1]) <
	 ( /*_#GET_INT__L6*/ meltfnum[5]));;
      MELT_LOCATION ("warmelt-outobj.melt:3132:/ cond");
      /*cond */ if ( /*_#I__L7*/ meltfnum[6])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V23*/ meltfptr[17] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-outobj.melt:3132:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check limited implbuf"),
				  ("warmelt-outobj.melt")
				  ? ("warmelt-outobj.melt") : __FILE__,
				  (3132) ? (3132) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V23*/ meltfptr[17] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V21*/ meltfptr[14] = /*_.IFELSE___V23*/ meltfptr[17];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:3132:/ clear");
	     /*clear *//*_#STRBUF_USEDLENGTH__L5*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.REFERENCED_VALUE__V22*/ meltfptr[16] = 0;
      /*^clear */
	     /*clear *//*_#GET_INT__L6*/ meltfnum[5] = 0;
      /*^clear */
	     /*clear *//*_#I__L7*/ meltfnum[6] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V23*/ meltfptr[17] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V21*/ meltfptr[14] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    /*^compute */
    /*_.LET___V7*/ meltfptr[5] = /*_.IFCPP___V21*/ meltfptr[14];;

    MELT_LOCATION ("warmelt-outobj.melt:3115:/ clear");
	   /*clear *//*_.CLOC__V8*/ meltfptr[7] = 0;
    /*^clear */
	   /*clear *//*_.OIOBJ__V9*/ meltfptr[8] = 0;
    /*^clear */
	   /*clear *//*_.OIDAT__V10*/ meltfptr[9] = 0;
    /*^clear */
	   /*clear *//*_.OILOCV__V11*/ meltfptr[10] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V12*/ meltfptr[11] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V14*/ meltfptr[12] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V21*/ meltfptr[14] = 0;
    MELT_LOCATION ("warmelt-outobj.melt:3113:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.LET___V7*/ meltfptr[5];;

    {
      MELT_LOCATION ("warmelt-outobj.melt:3113:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V5*/ meltfptr[4] = 0;
    /*^clear */
	   /*clear *//*_.LET___V7*/ meltfptr[5] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("OUTPUCOD_OBJINTERNSYMBOL", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_74_warmelt_outobj_OUTPUCOD_OBJINTERNSYMBOL_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_74_warmelt_outobj_OUTPUCOD_OBJINTERNSYMBOL */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_75_warmelt_outobj_OUTPUCOD_OBJINTERNKEYWORD (meltclosure_ptr_t
						      meltclosp_,
						      melt_ptr_t
						      meltfirstargp_,
						      const
						      melt_argdescr_cell_t
						      meltxargdescr_[],
						      union meltparam_un *
						      meltxargtab_,
						      const
						      melt_argdescr_cell_t
						      meltxresdescr_[],
						      union meltparam_un *
						      meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_75_warmelt_outobj_OUTPUCOD_OBJINTERNKEYWORD_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_75_warmelt_outobj_OUTPUCOD_OBJINTERNKEYWORD_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 21
    melt_ptr_t mcfr_varptr[21];
#define MELTFRAM_NBVARNUM 6
    long mcfr_varnum[6];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_75_warmelt_outobj_OUTPUCOD_OBJINTERNKEYWORD is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_75_warmelt_outobj_OUTPUCOD_OBJINTERNKEYWORD_st
	 *) meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 21; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_75_warmelt_outobj_OUTPUCOD_OBJINTERNKEYWORD nbval 21*/
  meltfram__.mcfr_nbvar = 21 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("OUTPUCOD_OBJINTERNKEYWORD", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-outobj.melt:3138:/ getarg");
 /*_.OIKW__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.DECLBUF__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.DECLBUF__V3*/ meltfptr[2])) !=
	      NULL);


  /*getarg#2 */
  /*^getarg */
  if (meltxargdescr_[1] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.IMPLBUF__V4*/ meltfptr[3] =
    (meltxargtab_[1].meltbp_aptr) ? (*(meltxargtab_[1].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3])) !=
	      NULL);


  /*getarg#3 */
  /*^getarg */
  if (meltxargdescr_[2] != MELTBPAR_LONG)
    goto lab_endgetargs;
 /*_#DEPTH__L1*/ meltfnum[0] = meltxargtab_[2].meltbp_long;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:3139:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L2*/ meltfnum[1] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.OIKW__V2*/ meltfptr[1]),
			     (melt_ptr_t) (( /*!CLASS_OBJINTERNKEYWORD */
					    meltfrout->tabval[0])));;
      MELT_LOCATION ("warmelt-outobj.melt:3139:/ cond");
      /*cond */ if ( /*_#IS_A__L2*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V6*/ meltfptr[5] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-outobj.melt:3139:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check oikw"),
				  ("warmelt-outobj.melt")
				  ? ("warmelt-outobj.melt") : __FILE__,
				  (3139) ? (3139) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V6*/ meltfptr[5] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V5*/ meltfptr[4] = /*_.IFELSE___V6*/ meltfptr[5];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:3139:/ clear");
	     /*clear *//*_#IS_A__L2*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V6*/ meltfptr[5] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V5*/ meltfptr[4] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-outobj.melt:3140:/ quasiblock");


    /*^getslot */
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.OIKW__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 0, "OBI_LOC");
  /*_.CLOC__V8*/ meltfptr[7] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-outobj.melt:3141:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.OIKW__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 1, "OBINTERN_IOBJ");
  /*_.OIOBJ__V9*/ meltfptr[8] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-outobj.melt:3142:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.OIOBJ__V9*/ meltfptr[8]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 2, "OIE_DATA");
  /*_.OIDAT__V10*/ meltfptr[9] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-outobj.melt:3143:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.OIOBJ__V9*/ meltfptr[8]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 4, "OIE_LOCVAR");
  /*_.OILOCV__V11*/ meltfptr[10] = slot;
    };
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:3145:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L3*/ meltfnum[1] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.OIDAT__V10*/ meltfptr[9]),
			     (melt_ptr_t) (( /*!CLASS_NREP_DATAKEYWORD */
					    meltfrout->tabval[1])));;
      MELT_LOCATION ("warmelt-outobj.melt:3145:/ cond");
      /*cond */ if ( /*_#IS_A__L3*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V13*/ meltfptr[12] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-outobj.melt:3145:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check oidat"),
				  ("warmelt-outobj.melt")
				  ? ("warmelt-outobj.melt") : __FILE__,
				  (3145) ? (3145) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V13*/ meltfptr[12] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V12*/ meltfptr[11] = /*_.IFELSE___V13*/ meltfptr[12];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:3145:/ clear");
	     /*clear *//*_#IS_A__L3*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V13*/ meltfptr[12] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V12*/ meltfptr[11] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-outobj.melt:3146:/ quasiblock");


    /*^getslot */
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.OIDAT__V10*/ meltfptr[9]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 9, "NDSY_NAMESTR");
  /*_.NSY__V14*/ meltfptr[12] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-outobj.melt:3147:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^cond */
    /*cond */ if ( /*_.CLOC__V8*/ meltfptr[7])	/*then */
      {
	/*^cond.then */
	/*_.IFELSE___V15*/ meltfptr[14] = /*_.CLOC__V8*/ meltfptr[7];;
      }
    else
      {
	MELT_LOCATION ("warmelt-outobj.melt:3147:/ cond.else");

	/*^block */
	/*anyblock */
	{

	  /*^getslot */
	  {
	    melt_ptr_t slot = NULL, obj = NULL;
	    obj = (melt_ptr_t) ( /*_.OIDAT__V10*/ meltfptr[9]) /*=obj*/ ;
	    melt_object_get_field (slot, obj, 0, "NREP_LOC");
    /*_.NREP_LOC__V16*/ meltfptr[15] = slot;
	  };
	  ;
	  /*_.IFELSE___V15*/ meltfptr[14] = /*_.NREP_LOC__V16*/ meltfptr[15];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-outobj.melt:3147:/ clear");
	     /*clear *//*_.NREP_LOC__V16*/ meltfptr[15] = 0;
	}
	;
      }
    ;
    /*^checksignal */
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[3];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.IMPLBUF__V4*/ meltfptr[3];
      /*^apply.arg */
      argtab[1].meltbp_long = /*_#DEPTH__L1*/ meltfnum[0];
      /*^apply.arg */
      argtab[2].meltbp_cstring = "internkeyword";
      /*_.OUTPUT_LOCATION__V17*/ meltfptr[15] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!OUTPUT_LOCATION */ meltfrout->tabval[2])),
		    (melt_ptr_t) ( /*_.IFELSE___V15*/ meltfptr[14]),
		    (MELTBPARSTR_PTR MELTBPARSTR_LONG MELTBPARSTR_CSTRING ""),
		    argtab, "", (union meltparam_un *) 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:3148:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   ("/*internkeyw:"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:3149:/ locexp");
      /*add2sbuf_string */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   melt_string_str ((melt_ptr_t)
					    ( /*_.NSY__V14*/ meltfptr[12])));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:3150:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   ("*/"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:3151:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				( /*_#DEPTH__L1*/ meltfnum[0]), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:3152:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   ("(void) meltgc_intern_keyword((melt_ptr_t)("));
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:3153:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^msend */
    /*msend */
    {
      union meltparam_un argtab[3];
      memset (&argtab, 0, sizeof (argtab));
      /*^ojbmsend.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.DECLBUF__V3*/ meltfptr[2];
      /*^ojbmsend.arg */
      argtab[1].meltbp_aptr = (melt_ptr_t *) & /*_.IMPLBUF__V4*/ meltfptr[3];
      /*^ojbmsend.arg */
      argtab[2].meltbp_long = /*_#DEPTH__L1*/ meltfnum[0];
      /*_.OUTPUT_C_CODE__V18*/ meltfptr[17] =
	meltgc_send ((melt_ptr_t) ( /*_.OILOCV__V11*/ meltfptr[10]),
		     (melt_ptr_t) (( /*!OUTPUT_C_CODE */ meltfrout->
				    tabval[3])),
		     (MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_LONG ""),
		     argtab, "", (union meltparam_un *) 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:3154:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   ("));"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:3155:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				( /*_#DEPTH__L1*/ meltfnum[0]), 0);
    }
    ;

    MELT_LOCATION ("warmelt-outobj.melt:3146:/ clear");
	   /*clear *//*_.NSY__V14*/ meltfptr[12] = 0;
    /*^clear */
	   /*clear *//*_.IFELSE___V15*/ meltfptr[14] = 0;
    /*^clear */
	   /*clear *//*_.OUTPUT_LOCATION__V17*/ meltfptr[15] = 0;
    /*^clear */
	   /*clear *//*_.OUTPUT_C_CODE__V18*/ meltfptr[17] = 0;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:3156:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#STRBUF_USEDLENGTH__L4*/ meltfnum[1] =
	melt_strbuf_usedlength ((melt_ptr_t)
				( /*_.IMPLBUF__V4*/ meltfptr[3]));;
      MELT_LOCATION ("warmelt-outobj.melt:3157:/ cond");
      /*cond */ if (
		     /*ifisa */
		     melt_is_instance_of ((melt_ptr_t)
					  (( /*!BUFFER_LIMIT_CONT */
					    meltfrout->tabval[4])),
					  (melt_ptr_t) (( /*!CLASS_REFERENCE */ meltfrout->tabval[5])))
	)			/*then */
	{
	  /*^cond.then */
	  /*^getslot */
	  {
	    melt_ptr_t slot = NULL, obj = NULL;
	    obj =
	      (melt_ptr_t) (( /*!BUFFER_LIMIT_CONT */ meltfrout->
			     tabval[4])) /*=obj*/ ;
	    melt_object_get_field (slot, obj, 0, "REFERENCED_VALUE");
     /*_.REFERENCED_VALUE__V20*/ meltfptr[14] = slot;
	  };
	  ;
	}
      else
	{			/*^cond.else */

    /*_.REFERENCED_VALUE__V20*/ meltfptr[14] = NULL;;
	}
      ;
      /*^compute */
   /*_#GET_INT__L5*/ meltfnum[4] =
	(melt_get_int
	 ((melt_ptr_t) ( /*_.REFERENCED_VALUE__V20*/ meltfptr[14])));;
      /*^compute */
   /*_#I__L6*/ meltfnum[5] =
	(( /*_#STRBUF_USEDLENGTH__L4*/ meltfnum[1]) <
	 ( /*_#GET_INT__L5*/ meltfnum[4]));;
      MELT_LOCATION ("warmelt-outobj.melt:3156:/ cond");
      /*cond */ if ( /*_#I__L6*/ meltfnum[5])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V21*/ meltfptr[15] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-outobj.melt:3156:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check limited implbuf"),
				  ("warmelt-outobj.melt")
				  ? ("warmelt-outobj.melt") : __FILE__,
				  (3156) ? (3156) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V21*/ meltfptr[15] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V19*/ meltfptr[12] = /*_.IFELSE___V21*/ meltfptr[15];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:3156:/ clear");
	     /*clear *//*_#STRBUF_USEDLENGTH__L4*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.REFERENCED_VALUE__V20*/ meltfptr[14] = 0;
      /*^clear */
	     /*clear *//*_#GET_INT__L5*/ meltfnum[4] = 0;
      /*^clear */
	     /*clear *//*_#I__L6*/ meltfnum[5] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V21*/ meltfptr[15] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V19*/ meltfptr[12] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    /*^compute */
    /*_.LET___V7*/ meltfptr[5] = /*_.IFCPP___V19*/ meltfptr[12];;

    MELT_LOCATION ("warmelt-outobj.melt:3140:/ clear");
	   /*clear *//*_.CLOC__V8*/ meltfptr[7] = 0;
    /*^clear */
	   /*clear *//*_.OIOBJ__V9*/ meltfptr[8] = 0;
    /*^clear */
	   /*clear *//*_.OIDAT__V10*/ meltfptr[9] = 0;
    /*^clear */
	   /*clear *//*_.OILOCV__V11*/ meltfptr[10] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V12*/ meltfptr[11] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V19*/ meltfptr[12] = 0;
    MELT_LOCATION ("warmelt-outobj.melt:3138:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.LET___V7*/ meltfptr[5];;

    {
      MELT_LOCATION ("warmelt-outobj.melt:3138:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V5*/ meltfptr[4] = 0;
    /*^clear */
	   /*clear *//*_.LET___V7*/ meltfptr[5] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("OUTPUCOD_OBJINTERNKEYWORD", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_75_warmelt_outobj_OUTPUCOD_OBJINTERNKEYWORD_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_75_warmelt_outobj_OUTPUCOD_OBJINTERNKEYWORD */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_76_warmelt_outobj_OUTPUCOD_OBJGETNAMEDSYMBOL (meltclosure_ptr_t
						       meltclosp_,
						       melt_ptr_t
						       meltfirstargp_,
						       const
						       melt_argdescr_cell_t
						       meltxargdescr_[],
						       union meltparam_un *
						       meltxargtab_,
						       const
						       melt_argdescr_cell_t
						       meltxresdescr_[],
						       union meltparam_un *
						       meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_76_warmelt_outobj_OUTPUCOD_OBJGETNAMEDSYMBOL_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_76_warmelt_outobj_OUTPUCOD_OBJGETNAMEDSYMBOL_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 25
    melt_ptr_t mcfr_varptr[25];
#define MELTFRAM_NBVARNUM 11
    long mcfr_varnum[11];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_76_warmelt_outobj_OUTPUCOD_OBJGETNAMEDSYMBOL is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_76_warmelt_outobj_OUTPUCOD_OBJGETNAMEDSYMBOL_st
	 *) meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 25; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_76_warmelt_outobj_OUTPUCOD_OBJGETNAMEDSYMBOL nbval 25*/
  meltfram__.mcfr_nbvar = 25 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("OUTPUCOD_OBJGETNAMEDSYMBOL", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-outobj.melt:3162:/ getarg");
 /*_.OGSY__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.DECLBUF__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.DECLBUF__V3*/ meltfptr[2])) !=
	      NULL);


  /*getarg#2 */
  /*^getarg */
  if (meltxargdescr_[1] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.IMPLBUF__V4*/ meltfptr[3] =
    (meltxargtab_[1].meltbp_aptr) ? (*(meltxargtab_[1].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3])) !=
	      NULL);


  /*getarg#3 */
  /*^getarg */
  if (meltxargdescr_[2] != MELTBPAR_LONG)
    goto lab_endgetargs;
 /*_#DEPTH__L1*/ meltfnum[0] = meltxargtab_[2].meltbp_long;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:3163:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L2*/ meltfnum[1] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.OGSY__V2*/ meltfptr[1]),
			     (melt_ptr_t) (( /*!CLASS_OBJGETNAMEDSYMBOL */
					    meltfrout->tabval[0])));;
      MELT_LOCATION ("warmelt-outobj.melt:3163:/ cond");
      /*cond */ if ( /*_#IS_A__L2*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V6*/ meltfptr[5] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-outobj.melt:3163:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check ogsy"),
				  ("warmelt-outobj.melt")
				  ? ("warmelt-outobj.melt") : __FILE__,
				  (3163) ? (3163) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V6*/ meltfptr[5] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V5*/ meltfptr[4] = /*_.IFELSE___V6*/ meltfptr[5];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:3163:/ clear");
	     /*clear *//*_#IS_A__L2*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V6*/ meltfptr[5] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V5*/ meltfptr[4] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-outobj.melt:3164:/ quasiblock");


    /*^getslot */
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.OGSY__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 0, "OBI_LOC");
  /*_.CLOC__V8*/ meltfptr[7] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-outobj.melt:3165:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.OGSY__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 1, "OBGNAMED_IOBJ");
  /*_.OIOBJ__V9*/ meltfptr[8] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-outobj.melt:3166:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.OIOBJ__V9*/ meltfptr[8]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 2, "OIE_DATA");
  /*_.OGDAT__V10*/ meltfptr[9] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-outobj.melt:3167:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.OIOBJ__V9*/ meltfptr[8]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 4, "OIE_LOCVAR");
  /*_.OILOCV__V11*/ meltfptr[10] = slot;
    };
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:3169:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L3*/ meltfnum[1] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.OIOBJ__V9*/ meltfptr[8]),
			     (melt_ptr_t) (( /*!CLASS_OBJINITOBJECT */
					    meltfrout->tabval[1])));;
      MELT_LOCATION ("warmelt-outobj.melt:3169:/ cond");
      /*cond */ if ( /*_#IS_A__L3*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V13*/ meltfptr[12] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-outobj.melt:3169:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check oiobj"),
				  ("warmelt-outobj.melt")
				  ? ("warmelt-outobj.melt") : __FILE__,
				  (3169) ? (3169) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V13*/ meltfptr[12] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V12*/ meltfptr[11] = /*_.IFELSE___V13*/ meltfptr[12];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:3169:/ clear");
	     /*clear *//*_#IS_A__L3*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V13*/ meltfptr[12] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V12*/ meltfptr[11] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:3170:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L4*/ meltfnum[1] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.OGDAT__V10*/ meltfptr[9]),
			     (melt_ptr_t) (( /*!CLASS_NREP_DATASYMBOL */
					    meltfrout->tabval[2])));;
      MELT_LOCATION ("warmelt-outobj.melt:3170:/ cond");
      /*cond */ if ( /*_#IS_A__L4*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V15*/ meltfptr[14] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-outobj.melt:3170:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check ogdat"),
				  ("warmelt-outobj.melt")
				  ? ("warmelt-outobj.melt") : __FILE__,
				  (3170) ? (3170) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V15*/ meltfptr[14] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V14*/ meltfptr[12] = /*_.IFELSE___V15*/ meltfptr[14];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:3170:/ clear");
	     /*clear *//*_#IS_A__L4*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V15*/ meltfptr[14] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V14*/ meltfptr[12] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-outobj.melt:3171:/ quasiblock");


    /*^getslot */
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.OGDAT__V10*/ meltfptr[9]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 9, "NDSY_NAMESTR");
  /*_.NSY__V17*/ meltfptr[16] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-outobj.melt:3173:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^cond */
    /*cond */ if ( /*_.CLOC__V8*/ meltfptr[7])	/*then */
      {
	/*^cond.then */
	/*_.IFELSE___V18*/ meltfptr[17] = /*_.CLOC__V8*/ meltfptr[7];;
      }
    else
      {
	MELT_LOCATION ("warmelt-outobj.melt:3173:/ cond.else");

	/*^block */
	/*anyblock */
	{

	  /*^getslot */
	  {
	    melt_ptr_t slot = NULL, obj = NULL;
	    obj = (melt_ptr_t) ( /*_.OGDAT__V10*/ meltfptr[9]) /*=obj*/ ;
	    melt_object_get_field (slot, obj, 0, "NREP_LOC");
    /*_.NREP_LOC__V19*/ meltfptr[18] = slot;
	  };
	  ;
	  /*_.IFELSE___V18*/ meltfptr[17] = /*_.NREP_LOC__V19*/ meltfptr[18];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-outobj.melt:3173:/ clear");
	     /*clear *//*_.NREP_LOC__V19*/ meltfptr[18] = 0;
	}
	;
      }
    ;
    /*^checksignal */
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[3];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.IMPLBUF__V4*/ meltfptr[3];
      /*^apply.arg */
      argtab[1].meltbp_long = /*_#DEPTH__L1*/ meltfnum[0];
      /*^apply.arg */
      argtab[2].meltbp_cstring = "getnamedsymbol";
      /*_.OUTPUT_LOCATION__V20*/ meltfptr[18] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!OUTPUT_LOCATION */ meltfrout->tabval[3])),
		    (melt_ptr_t) ( /*_.IFELSE___V18*/ meltfptr[17]),
		    (MELTBPARSTR_PTR MELTBPARSTR_LONG MELTBPARSTR_CSTRING ""),
		    argtab, "", (union meltparam_un *) 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:3174:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   ("/*getnamedsym:"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:3175:/ locexp");
      /*add2sbuf_string */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   melt_string_str ((melt_ptr_t)
					    ( /*_.NSY__V17*/ meltfptr[16])));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:3176:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   ("*/"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:3177:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				( /*_#DEPTH__L1*/ meltfnum[0]), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:3178:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   ("{ melt_ptr_t sy_"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:3179:/ locexp");
      meltgc_add_strbuf_cident ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				melt_string_str ((melt_ptr_t)
						 ( /*_.NSY__V17*/
						  meltfptr[16])));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:3180:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   (" = meltgc_named_symbol(\""));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:3181:/ locexp");
      /*add2sbuf_string */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   melt_string_str ((melt_ptr_t)
					    ( /*_.NSY__V17*/ meltfptr[16])));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:3182:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   ("\", MELT_GET);"));
    }
    ;
 /*_#I__L5*/ meltfnum[1] =
      (( /*_#DEPTH__L1*/ meltfnum[0]) + (1));;

    {
      MELT_LOCATION ("warmelt-outobj.melt:3183:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				( /*_#I__L5*/ meltfnum[1]), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:3184:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   ("if (sy_"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:3185:/ locexp");
      meltgc_add_strbuf_cident ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				melt_string_str ((melt_ptr_t)
						 ( /*_.NSY__V17*/
						  meltfptr[16])));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:3186:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   (" && NULL == "));
    }
    ;
 /*_#I__L6*/ meltfnum[5] =
      (( /*_#DEPTH__L1*/ meltfnum[0]) + (1));;
    MELT_LOCATION ("warmelt-outobj.melt:3187:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^msend */
    /*msend */
    {
      union meltparam_un argtab[3];
      memset (&argtab, 0, sizeof (argtab));
      /*^ojbmsend.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.DECLBUF__V3*/ meltfptr[2];
      /*^ojbmsend.arg */
      argtab[1].meltbp_aptr = (melt_ptr_t *) & /*_.IMPLBUF__V4*/ meltfptr[3];
      /*^ojbmsend.arg */
      argtab[2].meltbp_long = /*_#I__L6*/ meltfnum[5];
      /*_.OUTPUT_C_CODE__V21*/ meltfptr[20] =
	meltgc_send ((melt_ptr_t) ( /*_.OILOCV__V11*/ meltfptr[10]),
		     (melt_ptr_t) (( /*!OUTPUT_C_CODE */ meltfrout->
				    tabval[4])),
		     (MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_LONG ""),
		     argtab, "", (union meltparam_un *) 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:3188:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   (")"));
    }
    ;
 /*_#I__L7*/ meltfnum[6] =
      (( /*_#DEPTH__L1*/ meltfnum[0]) + (1));;

    {
      MELT_LOCATION ("warmelt-outobj.melt:3189:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				( /*_#I__L7*/ meltfnum[6]), 0);
    }
    ;
 /*_#I__L8*/ meltfnum[7] =
      (( /*_#DEPTH__L1*/ meltfnum[0]) + (1));;
    MELT_LOCATION ("warmelt-outobj.melt:3190:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^msend */
    /*msend */
    {
      union meltparam_un argtab[3];
      memset (&argtab, 0, sizeof (argtab));
      /*^ojbmsend.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.DECLBUF__V3*/ meltfptr[2];
      /*^ojbmsend.arg */
      argtab[1].meltbp_aptr = (melt_ptr_t *) & /*_.IMPLBUF__V4*/ meltfptr[3];
      /*^ojbmsend.arg */
      argtab[2].meltbp_long = /*_#I__L8*/ meltfnum[7];
      /*_.OUTPUT_C_CODE__V22*/ meltfptr[21] =
	meltgc_send ((melt_ptr_t) ( /*_.OILOCV__V11*/ meltfptr[10]),
		     (melt_ptr_t) (( /*!OUTPUT_C_CODE */ meltfrout->
				    tabval[4])),
		     (MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_LONG ""),
		     argtab, "", (union meltparam_un *) 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:3191:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   (" = (melt_ptr_t) sy_"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:3192:/ locexp");
      meltgc_add_strbuf_cident ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				melt_string_str ((melt_ptr_t)
						 ( /*_.NSY__V17*/
						  meltfptr[16])));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:3193:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   ("; }"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:3194:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				( /*_#DEPTH__L1*/ meltfnum[0]), 0);
    }
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:3195:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#STRBUF_USEDLENGTH__L9*/ meltfnum[8] =
	melt_strbuf_usedlength ((melt_ptr_t)
				( /*_.IMPLBUF__V4*/ meltfptr[3]));;
      MELT_LOCATION ("warmelt-outobj.melt:3196:/ cond");
      /*cond */ if (
		     /*ifisa */
		     melt_is_instance_of ((melt_ptr_t)
					  (( /*!BUFFER_LIMIT_CONT */
					    meltfrout->tabval[5])),
					  (melt_ptr_t) (( /*!CLASS_REFERENCE */ meltfrout->tabval[6])))
	)			/*then */
	{
	  /*^cond.then */
	  /*^getslot */
	  {
	    melt_ptr_t slot = NULL, obj = NULL;
	    obj =
	      (melt_ptr_t) (( /*!BUFFER_LIMIT_CONT */ meltfrout->
			     tabval[5])) /*=obj*/ ;
	    melt_object_get_field (slot, obj, 0, "REFERENCED_VALUE");
     /*_.REFERENCED_VALUE__V24*/ meltfptr[23] = slot;
	  };
	  ;
	}
      else
	{			/*^cond.else */

    /*_.REFERENCED_VALUE__V24*/ meltfptr[23] = NULL;;
	}
      ;
      /*^compute */
   /*_#GET_INT__L10*/ meltfnum[9] =
	(melt_get_int
	 ((melt_ptr_t) ( /*_.REFERENCED_VALUE__V24*/ meltfptr[23])));;
      /*^compute */
   /*_#I__L11*/ meltfnum[10] =
	(( /*_#STRBUF_USEDLENGTH__L9*/ meltfnum[8]) <
	 ( /*_#GET_INT__L10*/ meltfnum[9]));;
      MELT_LOCATION ("warmelt-outobj.melt:3195:/ cond");
      /*cond */ if ( /*_#I__L11*/ meltfnum[10])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V25*/ meltfptr[24] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-outobj.melt:3195:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check limited implbuf"),
				  ("warmelt-outobj.melt")
				  ? ("warmelt-outobj.melt") : __FILE__,
				  (3195) ? (3195) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V25*/ meltfptr[24] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V23*/ meltfptr[22] = /*_.IFELSE___V25*/ meltfptr[24];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:3195:/ clear");
	     /*clear *//*_#STRBUF_USEDLENGTH__L9*/ meltfnum[8] = 0;
      /*^clear */
	     /*clear *//*_.REFERENCED_VALUE__V24*/ meltfptr[23] = 0;
      /*^clear */
	     /*clear *//*_#GET_INT__L10*/ meltfnum[9] = 0;
      /*^clear */
	     /*clear *//*_#I__L11*/ meltfnum[10] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V25*/ meltfptr[24] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V23*/ meltfptr[22] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    /*^compute */
    /*_.LET___V16*/ meltfptr[14] = /*_.IFCPP___V23*/ meltfptr[22];;

    MELT_LOCATION ("warmelt-outobj.melt:3171:/ clear");
	   /*clear *//*_.NSY__V17*/ meltfptr[16] = 0;
    /*^clear */
	   /*clear *//*_.IFELSE___V18*/ meltfptr[17] = 0;
    /*^clear */
	   /*clear *//*_.OUTPUT_LOCATION__V20*/ meltfptr[18] = 0;
    /*^clear */
	   /*clear *//*_#I__L5*/ meltfnum[1] = 0;
    /*^clear */
	   /*clear *//*_#I__L6*/ meltfnum[5] = 0;
    /*^clear */
	   /*clear *//*_.OUTPUT_C_CODE__V21*/ meltfptr[20] = 0;
    /*^clear */
	   /*clear *//*_#I__L7*/ meltfnum[6] = 0;
    /*^clear */
	   /*clear *//*_#I__L8*/ meltfnum[7] = 0;
    /*^clear */
	   /*clear *//*_.OUTPUT_C_CODE__V22*/ meltfptr[21] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V23*/ meltfptr[22] = 0;
    /*_.LET___V7*/ meltfptr[5] = /*_.LET___V16*/ meltfptr[14];;

    MELT_LOCATION ("warmelt-outobj.melt:3164:/ clear");
	   /*clear *//*_.CLOC__V8*/ meltfptr[7] = 0;
    /*^clear */
	   /*clear *//*_.OIOBJ__V9*/ meltfptr[8] = 0;
    /*^clear */
	   /*clear *//*_.OGDAT__V10*/ meltfptr[9] = 0;
    /*^clear */
	   /*clear *//*_.OILOCV__V11*/ meltfptr[10] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V12*/ meltfptr[11] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V14*/ meltfptr[12] = 0;
    /*^clear */
	   /*clear *//*_.LET___V16*/ meltfptr[14] = 0;
    MELT_LOCATION ("warmelt-outobj.melt:3162:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.LET___V7*/ meltfptr[5];;

    {
      MELT_LOCATION ("warmelt-outobj.melt:3162:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V5*/ meltfptr[4] = 0;
    /*^clear */
	   /*clear *//*_.LET___V7*/ meltfptr[5] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("OUTPUCOD_OBJGETNAMEDSYMBOL", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_76_warmelt_outobj_OUTPUCOD_OBJGETNAMEDSYMBOL_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_76_warmelt_outobj_OUTPUCOD_OBJGETNAMEDSYMBOL */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_77_warmelt_outobj_OUTPUCOD_OBJGETNAMEDKEYWORD (meltclosure_ptr_t
							meltclosp_,
							melt_ptr_t
							meltfirstargp_,
							const
							melt_argdescr_cell_t
							meltxargdescr_[],
							union meltparam_un *
							meltxargtab_,
							const
							melt_argdescr_cell_t
							meltxresdescr_[],
							union meltparam_un *
							meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_77_warmelt_outobj_OUTPUCOD_OBJGETNAMEDKEYWORD_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_77_warmelt_outobj_OUTPUCOD_OBJGETNAMEDKEYWORD_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 24
    melt_ptr_t mcfr_varptr[24];
#define MELTFRAM_NBVARNUM 9
    long mcfr_varnum[9];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_77_warmelt_outobj_OUTPUCOD_OBJGETNAMEDKEYWORD is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct
	 frame_meltrout_77_warmelt_outobj_OUTPUCOD_OBJGETNAMEDKEYWORD_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 24; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_77_warmelt_outobj_OUTPUCOD_OBJGETNAMEDKEYWORD nbval 24*/
  meltfram__.mcfr_nbvar = 24 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("OUTPUCOD_OBJGETNAMEDKEYWORD", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-outobj.melt:3201:/ getarg");
 /*_.OGKW__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.DECLBUF__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.DECLBUF__V3*/ meltfptr[2])) !=
	      NULL);


  /*getarg#2 */
  /*^getarg */
  if (meltxargdescr_[1] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.IMPLBUF__V4*/ meltfptr[3] =
    (meltxargtab_[1].meltbp_aptr) ? (*(meltxargtab_[1].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3])) !=
	      NULL);


  /*getarg#3 */
  /*^getarg */
  if (meltxargdescr_[2] != MELTBPAR_LONG)
    goto lab_endgetargs;
 /*_#DEPTH__L1*/ meltfnum[0] = meltxargtab_[2].meltbp_long;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:3202:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L2*/ meltfnum[1] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.OGKW__V2*/ meltfptr[1]),
			     (melt_ptr_t) (( /*!CLASS_OBJGETNAMEDKEYWORD */
					    meltfrout->tabval[0])));;
      MELT_LOCATION ("warmelt-outobj.melt:3202:/ cond");
      /*cond */ if ( /*_#IS_A__L2*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V6*/ meltfptr[5] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-outobj.melt:3202:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check ogkw"),
				  ("warmelt-outobj.melt")
				  ? ("warmelt-outobj.melt") : __FILE__,
				  (3202) ? (3202) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V6*/ meltfptr[5] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V5*/ meltfptr[4] = /*_.IFELSE___V6*/ meltfptr[5];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:3202:/ clear");
	     /*clear *//*_#IS_A__L2*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V6*/ meltfptr[5] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V5*/ meltfptr[4] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-outobj.melt:3203:/ quasiblock");


    /*^getslot */
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.OGKW__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 0, "OBI_LOC");
  /*_.CLOC__V8*/ meltfptr[7] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-outobj.melt:3204:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.OGKW__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 1, "OBGNAMED_IOBJ");
  /*_.OIOBJ__V9*/ meltfptr[8] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-outobj.melt:3205:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.OIOBJ__V9*/ meltfptr[8]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 2, "OIE_DATA");
  /*_.OGDAT__V10*/ meltfptr[9] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-outobj.melt:3206:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.OIOBJ__V9*/ meltfptr[8]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 4, "OIE_LOCVAR");
  /*_.OILOCV__V11*/ meltfptr[10] = slot;
    };
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:3208:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L3*/ meltfnum[1] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.OIOBJ__V9*/ meltfptr[8]),
			     (melt_ptr_t) (( /*!CLASS_OBJINITOBJECT */
					    meltfrout->tabval[1])));;
      MELT_LOCATION ("warmelt-outobj.melt:3208:/ cond");
      /*cond */ if ( /*_#IS_A__L3*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V13*/ meltfptr[12] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-outobj.melt:3208:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check oiobj"),
				  ("warmelt-outobj.melt")
				  ? ("warmelt-outobj.melt") : __FILE__,
				  (3208) ? (3208) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V13*/ meltfptr[12] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V12*/ meltfptr[11] = /*_.IFELSE___V13*/ meltfptr[12];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:3208:/ clear");
	     /*clear *//*_#IS_A__L3*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V13*/ meltfptr[12] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V12*/ meltfptr[11] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:3209:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L4*/ meltfnum[1] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.OGDAT__V10*/ meltfptr[9]),
			     (melt_ptr_t) (( /*!CLASS_NREP_DATAKEYWORD */
					    meltfrout->tabval[2])));;
      MELT_LOCATION ("warmelt-outobj.melt:3209:/ cond");
      /*cond */ if ( /*_#IS_A__L4*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V15*/ meltfptr[14] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-outobj.melt:3209:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check ogdat"),
				  ("warmelt-outobj.melt")
				  ? ("warmelt-outobj.melt") : __FILE__,
				  (3209) ? (3209) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V15*/ meltfptr[14] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V14*/ meltfptr[12] = /*_.IFELSE___V15*/ meltfptr[14];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:3209:/ clear");
	     /*clear *//*_#IS_A__L4*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V15*/ meltfptr[14] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V14*/ meltfptr[12] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-outobj.melt:3210:/ quasiblock");


    /*^getslot */
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.OGDAT__V10*/ meltfptr[9]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 9, "NDSY_NAMESTR");
  /*_.NKW__V17*/ meltfptr[16] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-outobj.melt:3211:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^cond */
    /*cond */ if ( /*_.CLOC__V8*/ meltfptr[7])	/*then */
      {
	/*^cond.then */
	/*_.IFELSE___V18*/ meltfptr[17] = /*_.CLOC__V8*/ meltfptr[7];;
      }
    else
      {
	MELT_LOCATION ("warmelt-outobj.melt:3211:/ cond.else");

	/*^block */
	/*anyblock */
	{

	  /*^getslot */
	  {
	    melt_ptr_t slot = NULL, obj = NULL;
	    obj = (melt_ptr_t) ( /*_.OGDAT__V10*/ meltfptr[9]) /*=obj*/ ;
	    melt_object_get_field (slot, obj, 0, "NREP_LOC");
    /*_.NREP_LOC__V19*/ meltfptr[18] = slot;
	  };
	  ;
	  /*_.IFELSE___V18*/ meltfptr[17] = /*_.NREP_LOC__V19*/ meltfptr[18];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-outobj.melt:3211:/ clear");
	     /*clear *//*_.NREP_LOC__V19*/ meltfptr[18] = 0;
	}
	;
      }
    ;
    /*^checksignal */
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[3];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.IMPLBUF__V4*/ meltfptr[3];
      /*^apply.arg */
      argtab[1].meltbp_long = /*_#DEPTH__L1*/ meltfnum[0];
      /*^apply.arg */
      argtab[2].meltbp_cstring = "getnamedkeyword";
      /*_.OUTPUT_LOCATION__V20*/ meltfptr[18] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!OUTPUT_LOCATION */ meltfrout->tabval[3])),
		    (melt_ptr_t) ( /*_.IFELSE___V18*/ meltfptr[17]),
		    (MELTBPARSTR_PTR MELTBPARSTR_LONG MELTBPARSTR_CSTRING ""),
		    argtab, "", (union meltparam_un *) 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:3212:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   ("/*getnamedkeyw:"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:3213:/ locexp");
      /*add2sbuf_string */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   melt_string_str ((melt_ptr_t)
					    ( /*_.NKW__V17*/ meltfptr[16])));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:3214:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   ("*/"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:3215:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				( /*_#DEPTH__L1*/ meltfnum[0]), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:3216:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   ("{ melt_ptr_t kw_"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:3217:/ locexp");
      meltgc_add_strbuf_cident ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				melt_string_str ((melt_ptr_t)
						 ( /*_.NKW__V17*/
						  meltfptr[16])));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:3218:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   (" = meltgc_named_keyword(\""));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:3219:/ locexp");
      /*add2sbuf_string */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   melt_string_str ((melt_ptr_t)
					    ( /*_.NKW__V17*/ meltfptr[16])));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:3220:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   ("\", MELT_GET);"));
    }
    ;
 /*_#I__L5*/ meltfnum[1] =
      (( /*_#DEPTH__L1*/ meltfnum[0]) + (1));;

    {
      MELT_LOCATION ("warmelt-outobj.melt:3221:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				( /*_#I__L5*/ meltfnum[1]), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:3222:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   ("if (kw_"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:3223:/ locexp");
      meltgc_add_strbuf_cident ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				melt_string_str ((melt_ptr_t)
						 ( /*_.NKW__V17*/
						  meltfptr[16])));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:3224:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   (") "));
    }
    ;
 /*_#I__L6*/ meltfnum[5] =
      (( /*_#DEPTH__L1*/ meltfnum[0]) + (1));;
    MELT_LOCATION ("warmelt-outobj.melt:3225:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^msend */
    /*msend */
    {
      union meltparam_un argtab[3];
      memset (&argtab, 0, sizeof (argtab));
      /*^ojbmsend.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.DECLBUF__V3*/ meltfptr[2];
      /*^ojbmsend.arg */
      argtab[1].meltbp_aptr = (melt_ptr_t *) & /*_.IMPLBUF__V4*/ meltfptr[3];
      /*^ojbmsend.arg */
      argtab[2].meltbp_long = /*_#I__L6*/ meltfnum[5];
      /*_.OUTPUT_C_CODE__V21*/ meltfptr[20] =
	meltgc_send ((melt_ptr_t) ( /*_.OILOCV__V11*/ meltfptr[10]),
		     (melt_ptr_t) (( /*!OUTPUT_C_CODE */ meltfrout->
				    tabval[4])),
		     (MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_LONG ""),
		     argtab, "", (union meltparam_un *) 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:3226:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   (" = (melt_ptr_t) kw_"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:3227:/ locexp");
      meltgc_add_strbuf_cident ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				melt_string_str ((melt_ptr_t)
						 ( /*_.NKW__V17*/
						  meltfptr[16])));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:3228:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   ("; }"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:3229:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				( /*_#DEPTH__L1*/ meltfnum[0]), 0);
    }
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:3230:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#STRBUF_USEDLENGTH__L7*/ meltfnum[6] =
	melt_strbuf_usedlength ((melt_ptr_t)
				( /*_.IMPLBUF__V4*/ meltfptr[3]));;
      MELT_LOCATION ("warmelt-outobj.melt:3231:/ cond");
      /*cond */ if (
		     /*ifisa */
		     melt_is_instance_of ((melt_ptr_t)
					  (( /*!BUFFER_LIMIT_CONT */
					    meltfrout->tabval[5])),
					  (melt_ptr_t) (( /*!CLASS_REFERENCE */ meltfrout->tabval[6])))
	)			/*then */
	{
	  /*^cond.then */
	  /*^getslot */
	  {
	    melt_ptr_t slot = NULL, obj = NULL;
	    obj =
	      (melt_ptr_t) (( /*!BUFFER_LIMIT_CONT */ meltfrout->
			     tabval[5])) /*=obj*/ ;
	    melt_object_get_field (slot, obj, 0, "REFERENCED_VALUE");
     /*_.REFERENCED_VALUE__V23*/ meltfptr[22] = slot;
	  };
	  ;
	}
      else
	{			/*^cond.else */

    /*_.REFERENCED_VALUE__V23*/ meltfptr[22] = NULL;;
	}
      ;
      /*^compute */
   /*_#GET_INT__L8*/ meltfnum[7] =
	(melt_get_int
	 ((melt_ptr_t) ( /*_.REFERENCED_VALUE__V23*/ meltfptr[22])));;
      /*^compute */
   /*_#I__L9*/ meltfnum[8] =
	(( /*_#STRBUF_USEDLENGTH__L7*/ meltfnum[6]) <
	 ( /*_#GET_INT__L8*/ meltfnum[7]));;
      MELT_LOCATION ("warmelt-outobj.melt:3230:/ cond");
      /*cond */ if ( /*_#I__L9*/ meltfnum[8])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V24*/ meltfptr[23] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-outobj.melt:3230:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check limited implbuf"),
				  ("warmelt-outobj.melt")
				  ? ("warmelt-outobj.melt") : __FILE__,
				  (3230) ? (3230) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V24*/ meltfptr[23] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V22*/ meltfptr[21] = /*_.IFELSE___V24*/ meltfptr[23];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:3230:/ clear");
	     /*clear *//*_#STRBUF_USEDLENGTH__L7*/ meltfnum[6] = 0;
      /*^clear */
	     /*clear *//*_.REFERENCED_VALUE__V23*/ meltfptr[22] = 0;
      /*^clear */
	     /*clear *//*_#GET_INT__L8*/ meltfnum[7] = 0;
      /*^clear */
	     /*clear *//*_#I__L9*/ meltfnum[8] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V24*/ meltfptr[23] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V22*/ meltfptr[21] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    /*^compute */
    /*_.LET___V16*/ meltfptr[14] = /*_.IFCPP___V22*/ meltfptr[21];;

    MELT_LOCATION ("warmelt-outobj.melt:3210:/ clear");
	   /*clear *//*_.NKW__V17*/ meltfptr[16] = 0;
    /*^clear */
	   /*clear *//*_.IFELSE___V18*/ meltfptr[17] = 0;
    /*^clear */
	   /*clear *//*_.OUTPUT_LOCATION__V20*/ meltfptr[18] = 0;
    /*^clear */
	   /*clear *//*_#I__L5*/ meltfnum[1] = 0;
    /*^clear */
	   /*clear *//*_#I__L6*/ meltfnum[5] = 0;
    /*^clear */
	   /*clear *//*_.OUTPUT_C_CODE__V21*/ meltfptr[20] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V22*/ meltfptr[21] = 0;
    /*_.LET___V7*/ meltfptr[5] = /*_.LET___V16*/ meltfptr[14];;

    MELT_LOCATION ("warmelt-outobj.melt:3203:/ clear");
	   /*clear *//*_.CLOC__V8*/ meltfptr[7] = 0;
    /*^clear */
	   /*clear *//*_.OIOBJ__V9*/ meltfptr[8] = 0;
    /*^clear */
	   /*clear *//*_.OGDAT__V10*/ meltfptr[9] = 0;
    /*^clear */
	   /*clear *//*_.OILOCV__V11*/ meltfptr[10] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V12*/ meltfptr[11] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V14*/ meltfptr[12] = 0;
    /*^clear */
	   /*clear *//*_.LET___V16*/ meltfptr[14] = 0;
    MELT_LOCATION ("warmelt-outobj.melt:3201:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.LET___V7*/ meltfptr[5];;

    {
      MELT_LOCATION ("warmelt-outobj.melt:3201:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V5*/ meltfptr[4] = 0;
    /*^clear */
	   /*clear *//*_.LET___V7*/ meltfptr[5] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("OUTPUCOD_OBJGETNAMEDKEYWORD", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_77_warmelt_outobj_OUTPUCOD_OBJGETNAMEDKEYWORD_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_77_warmelt_outobj_OUTPUCOD_OBJGETNAMEDKEYWORD */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_78_warmelt_outobj_OUTPUCOD_OBJAPPLY (meltclosure_ptr_t meltclosp_,
					      melt_ptr_t meltfirstargp_,
					      const melt_argdescr_cell_t
					      meltxargdescr_[],
					      union meltparam_un *
					      meltxargtab_,
					      const melt_argdescr_cell_t
					      meltxresdescr_[],
					      union meltparam_un *
					      meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_78_warmelt_outobj_OUTPUCOD_OBJAPPLY_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_78_warmelt_outobj_OUTPUCOD_OBJAPPLY_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 46
    melt_ptr_t mcfr_varptr[46];
#define MELTFRAM_NBVARNUM 32
    long mcfr_varnum[32];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_78_warmelt_outobj_OUTPUCOD_OBJAPPLY is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_78_warmelt_outobj_OUTPUCOD_OBJAPPLY_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 46; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_78_warmelt_outobj_OUTPUCOD_OBJAPPLY nbval 46*/
  meltfram__.mcfr_nbvar = 46 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("OUTPUCOD_OBJAPPLY", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-outobj.melt:3237:/ getarg");
 /*_.OAPP__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.DECLBUF__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.DECLBUF__V3*/ meltfptr[2])) !=
	      NULL);


  /*getarg#2 */
  /*^getarg */
  if (meltxargdescr_[1] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.IMPLBUF__V4*/ meltfptr[3] =
    (meltxargtab_[1].meltbp_aptr) ? (*(meltxargtab_[1].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3])) !=
	      NULL);


  /*getarg#3 */
  /*^getarg */
  if (meltxargdescr_[2] != MELTBPAR_LONG)
    goto lab_endgetargs;
 /*_#DEPTH__L1*/ meltfnum[0] = meltxargtab_[2].meltbp_long;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:3238:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L2*/ meltfnum[1] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.OAPP__V2*/ meltfptr[1]),
			     (melt_ptr_t) (( /*!CLASS_OBJAPPLY */ meltfrout->
					    tabval[0])));;
      MELT_LOCATION ("warmelt-outobj.melt:3238:/ cond");
      /*cond */ if ( /*_#IS_A__L2*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V6*/ meltfptr[5] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-outobj.melt:3238:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check oapp"),
				  ("warmelt-outobj.melt")
				  ? ("warmelt-outobj.melt") : __FILE__,
				  (3238) ? (3238) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V6*/ meltfptr[5] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V5*/ meltfptr[4] = /*_.IFELSE___V6*/ meltfptr[5];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:3238:/ clear");
	     /*clear *//*_#IS_A__L2*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V6*/ meltfptr[5] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V5*/ meltfptr[4] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-outobj.melt:3239:/ quasiblock");


    MELT_LOCATION ("warmelt-outobj.melt:3240:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.OAPP__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 0, "OBI_LOC");
  /*_.ALOC__V8*/ meltfptr[7] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-outobj.melt:3241:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.OAPP__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 1, "OBDI_DESTLIST");
  /*_.ADEST__V9*/ meltfptr[8] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-outobj.melt:3242:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.OAPP__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 2, "OBAPP_CLOS");
  /*_.OCLOS__V10*/ meltfptr[9] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-outobj.melt:3243:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.OAPP__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 3, "OBAPP_ARGS");
  /*_.OARGS__V11*/ meltfptr[10] = slot;
    };
    ;
 /*_#NBARG__L3*/ meltfnum[1] =
      (melt_multiple_length ((melt_ptr_t) ( /*_.OARGS__V11*/ meltfptr[10])));;
    /*^compute */
 /*_.PARAMDESCLIST__V12*/ meltfptr[11] =
      (meltgc_new_list
       ((meltobject_ptr_t) (( /*!DISCR_LIST */ meltfrout->tabval[1]))));;
    /*^compute */
 /*_#I__L4*/ meltfnum[3] =
      ((1) + ( /*_#DEPTH__L1*/ meltfnum[0]));;
    /*^compute */
 /*_.BOXDEPTHP1__V13*/ meltfptr[12] =
      (meltgc_new_int
       ((meltobject_ptr_t) (( /*!DISCR_INTEGER */ meltfrout->tabval[2])),
	( /*_#I__L4*/ meltfnum[3])));;
    MELT_LOCATION ("warmelt-outobj.melt:3248:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[3];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.IMPLBUF__V4*/ meltfptr[3];
      /*^apply.arg */
      argtab[1].meltbp_long = /*_#DEPTH__L1*/ meltfnum[0];
      /*^apply.arg */
      argtab[2].meltbp_cstring = "apply";
      /*_.OUTPUT_LOCATION__V14*/ meltfptr[13] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!OUTPUT_LOCATION */ meltfrout->tabval[3])),
		    (melt_ptr_t) ( /*_.ALOC__V8*/ meltfptr[7]),
		    (MELTBPARSTR_PTR MELTBPARSTR_LONG MELTBPARSTR_CSTRING ""),
		    argtab, "", (union meltparam_un *) 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:3249:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   ("/*apply*/{"));
    }
    ;
 /*_#I__L5*/ meltfnum[4] =
      ((1) + ( /*_#DEPTH__L1*/ meltfnum[0]));;

    {
      MELT_LOCATION ("warmelt-outobj.melt:3250:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				( /*_#I__L5*/ meltfnum[4]), 0);
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:3251:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#I__L6*/ meltfnum[5] =
      (( /*_#NBARG__L3*/ meltfnum[1]) > (1));;
    MELT_LOCATION ("warmelt-outobj.melt:3251:/ cond");
    /*cond */ if ( /*_#I__L6*/ meltfnum[5])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{


	  {
	    MELT_LOCATION ("warmelt-outobj.melt:3253:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t)
				 ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				 ("union meltparam_un argtab["));
	  }
	  ;
   /*_#I__L7*/ meltfnum[6] =
	    (( /*_#NBARG__L3*/ meltfnum[1]) - (1));;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:3254:/ locexp");
	    meltgc_add_strbuf_dec ((melt_ptr_t)
				   ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				   ( /*_#I__L7*/ meltfnum[6]));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:3255:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t)
				 ( /*_.IMPLBUF__V4*/ meltfptr[3]), ("];"));
	  }
	  ;
   /*_#I__L8*/ meltfnum[7] =
	    ((1) + ( /*_#DEPTH__L1*/ meltfnum[0]));;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:3256:/ locexp");
	    meltgc_strbuf_add_indent ((melt_ptr_t)
				      ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				      ( /*_#I__L8*/ meltfnum[7]), 0);
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:3257:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t)
				 ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				 ("memset(&argtab, 0, sizeof(argtab));"));
	  }
	  ;
   /*_#I__L9*/ meltfnum[8] =
	    ((1) + ( /*_#DEPTH__L1*/ meltfnum[0]));;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:3258:/ locexp");
	    meltgc_strbuf_add_indent ((melt_ptr_t)
				      ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				      ( /*_#I__L9*/ meltfnum[8]), 0);
	  }
	  ;
	  /*citerblock FOREACH_IN_MULTIPLE */
	  {
	    /* start foreach_in_multiple meltcit1__EACHTUP */
	    long meltcit1__EACHTUP_ln =
	      melt_multiple_length ((melt_ptr_t) /*_.OARGS__V11*/
				    meltfptr[10]);
	    for ( /*_#CURANK__L10*/ meltfnum[9] = 0;
		 ( /*_#CURANK__L10*/ meltfnum[9] >= 0)
		 && ( /*_#CURANK__L10*/ meltfnum[9] < meltcit1__EACHTUP_ln);
	/*_#CURANK__L10*/ meltfnum[9]++)
	      {
		/*_.CURARG__V15*/ meltfptr[14] =
		  melt_multiple_nth ((melt_ptr_t)
				     ( /*_.OARGS__V11*/ meltfptr[10]),
				     /*_#CURANK__L10*/ meltfnum[9]);




#if MELT_HAVE_DEBUG
		MELT_LOCATION ("warmelt-outobj.melt:3263:/ cppif.then");
		/*^block */
		/*anyblock */
		{

		  /*^checksignal */
		  MELT_CHECK_SIGNAL ();
		  ;
      /*_#IS_A__L11*/ meltfnum[10] =
		    melt_is_instance_of ((melt_ptr_t)
					 ( /*_.CURARG__V15*/ meltfptr[14]),
					 (melt_ptr_t) (( /*!CLASS_OBJINSTR */
							meltfrout->
							tabval[4])));;
		  /*^compute */
      /*_#NOT__L12*/ meltfnum[11] =
		    (!( /*_#IS_A__L11*/ meltfnum[10]));;
		  MELT_LOCATION ("warmelt-outobj.melt:3263:/ cond");
		  /*cond */ if ( /*_#NOT__L12*/ meltfnum[11])	/*then */
		    {
		      /*^cond.then */
		      /*_.IFELSE___V17*/ meltfptr[16] = ( /*nil */ NULL);;
		    }
		  else
		    {
		      MELT_LOCATION ("warmelt-outobj.melt:3263:/ cond.else");

		      /*^block */
		      /*anyblock */
		      {




			{
			  /*^locexp */
			  melt_assert_failed (("outputcod_objapply check curarg not objinstr"), ("warmelt-outobj.melt") ? ("warmelt-outobj.melt") : __FILE__, (3263) ? (3263) : __LINE__, __FUNCTION__);
			  ;
			}
			;
		  /*clear *//*_.IFELSE___V17*/ meltfptr[16] = 0;
			/*epilog */
		      }
		      ;
		    }
		  ;
		  /*^compute */
		  /*_.IFCPP___V16*/ meltfptr[15] =
		    /*_.IFELSE___V17*/ meltfptr[16];;
		  /*epilog */

		  MELT_LOCATION ("warmelt-outobj.melt:3263:/ clear");
		/*clear *//*_#IS_A__L11*/ meltfnum[10] = 0;
		  /*^clear */
		/*clear *//*_#NOT__L12*/ meltfnum[11] = 0;
		  /*^clear */
		/*clear *//*_.IFELSE___V17*/ meltfptr[16] = 0;
		}

#else /*MELT_HAVE_DEBUG */
		/*^cppif.else */
		/*_.IFCPP___V16*/ meltfptr[15] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
		;
		MELT_LOCATION ("warmelt-outobj.melt:3264:/ checksignal");
		MELT_CHECK_SIGNAL ();
		;
    /*_#I__L13*/ meltfnum[10] =
		  (( /*_#CURANK__L10*/ meltfnum[9]) > (0));;
		MELT_LOCATION ("warmelt-outobj.melt:3264:/ cond");
		/*cond */ if ( /*_#I__L13*/ meltfnum[10])	/*then */
		  {
		    /*^cond.then */
		    /*^block */
		    /*anyblock */
		    {

		      MELT_LOCATION ("warmelt-outobj.melt:3265:/ quasiblock");


		      /*^checksignal */
		      MELT_CHECK_SIGNAL ();
		      ;
		      /*^msend */
		      /*msend */
		      {
			union meltparam_un argtab[1];
			memset (&argtab, 0, sizeof (argtab));
			/*^ojbmsend.arg */
			argtab[0].meltbp_aptr = /*nil */ (melt_ptr_t *) NULL;
			/*_.CURCTYP__V18*/ meltfptr[16] =
			  meltgc_send ((melt_ptr_t)
				       ( /*_.CURARG__V15*/ meltfptr[14]),
				       (melt_ptr_t) (( /*!GET_CTYPE */
						      meltfrout->tabval[5])),
				       (MELTBPARSTR_PTR ""), argtab, "",
				       (union meltparam_un *) 0);
		      }
		      ;

#if MELT_HAVE_DEBUG
		      MELT_LOCATION ("warmelt-outobj.melt:3266:/ cppif.then");
		      /*^block */
		      /*anyblock */
		      {

			/*^checksignal */
			MELT_CHECK_SIGNAL ();
			;
	/*_#IS_A__L14*/ meltfnum[11] =
			  melt_is_instance_of ((melt_ptr_t)
					       ( /*_.CURCTYP__V18*/
						meltfptr[16]),
					       (melt_ptr_t) (( /*!CLASS_CTYPE */ meltfrout->tabval[6])));;
			MELT_LOCATION ("warmelt-outobj.melt:3266:/ cond");
			/*cond */ if ( /*_#IS_A__L14*/ meltfnum[11])	/*then */
			  {
			    /*^cond.then */
			    /*_.IFELSE___V20*/ meltfptr[19] =
			      ( /*nil */ NULL);;
			  }
			else
			  {
			    MELT_LOCATION
			      ("warmelt-outobj.melt:3266:/ cond.else");

			    /*^block */
			    /*anyblock */
			    {




			      {
				/*^locexp */
				melt_assert_failed (("check curctyp"),
						    ("warmelt-outobj.melt")
						    ? ("warmelt-outobj.melt")
						    : __FILE__,
						    (3266) ? (3266) :
						    __LINE__, __FUNCTION__);
				;
			      }
			      ;
		    /*clear *//*_.IFELSE___V20*/ meltfptr[19] = 0;
			      /*epilog */
			    }
			    ;
			  }
			;
			/*^compute */
			/*_.IFCPP___V19*/ meltfptr[18] =
			  /*_.IFELSE___V20*/ meltfptr[19];;
			/*epilog */

			MELT_LOCATION ("warmelt-outobj.melt:3266:/ clear");
		  /*clear *//*_#IS_A__L14*/ meltfnum[11] = 0;
			/*^clear */
		  /*clear *//*_.IFELSE___V20*/ meltfptr[19] = 0;
		      }

#else /*MELT_HAVE_DEBUG */
		      /*^cppif.else */
		      /*_.IFCPP___V19*/ meltfptr[18] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
		      ;
		      /*^compute */
      /*_#GET_INT__L15*/ meltfnum[11] =
			(melt_get_int
			 ((melt_ptr_t)
			  ( /*_.BOXDEPTHP1__V13*/ meltfptr[12])));;
		      MELT_LOCATION
			("warmelt-outobj.melt:3267:/ checksignal");
		      MELT_CHECK_SIGNAL ();
		      ;
		      /*^apply */
		      /*apply */
		      {
			union meltparam_un argtab[3];
			memset (&argtab, 0, sizeof (argtab));
			/*^apply.arg */
			argtab[0].meltbp_aptr =
			  (melt_ptr_t *) & /*_.IMPLBUF__V4*/ meltfptr[3];
			/*^apply.arg */
			argtab[1].meltbp_long =
			  /*_#GET_INT__L15*/ meltfnum[11];
			/*^apply.arg */
			argtab[2].meltbp_cstring = "apply.arg";
			/*_.OUTPUT_LOCATION__V21*/ meltfptr[19] =
			  melt_apply ((meltclosure_ptr_t)
				      (( /*!OUTPUT_LOCATION */ meltfrout->
					tabval[3])),
				      (melt_ptr_t) ( /*_.ALOC__V8*/
						    meltfptr[7]),
				      (MELTBPARSTR_PTR MELTBPARSTR_LONG
				       MELTBPARSTR_CSTRING ""), argtab, "",
				      (union meltparam_un *) 0);
		      }
		      ;

		      {
			MELT_LOCATION ("warmelt-outobj.melt:3268:/ locexp");
			/*add2sbuf_strconst */
			  meltgc_add_strbuf ((melt_ptr_t)
					     ( /*_.IMPLBUF__V4*/ meltfptr[3]),
					     ("argtab["));
		      }
		      ;
      /*_#I__L16*/ meltfnum[15] =
			(( /*_#CURANK__L10*/ meltfnum[9]) - (1));;

		      {
			MELT_LOCATION ("warmelt-outobj.melt:3269:/ locexp");
			meltgc_add_strbuf_dec ((melt_ptr_t)
					       ( /*_.IMPLBUF__V4*/
						meltfptr[3]),
					       ( /*_#I__L16*/ meltfnum[15]));
		      }
		      ;

		      {
			MELT_LOCATION ("warmelt-outobj.melt:3270:/ locexp");
			/*add2sbuf_strconst */
			  meltgc_add_strbuf ((melt_ptr_t)
					     ( /*_.IMPLBUF__V4*/ meltfptr[3]),
					     ("]."));
		      }
		      ;
		      MELT_LOCATION ("warmelt-outobj.melt:3271:/ getslot");
		      {
			melt_ptr_t slot = NULL, obj = NULL;
			obj =
			  (melt_ptr_t) ( /*_.CURCTYP__V18*/ meltfptr[16])
			  /*=obj*/ ;
			melt_object_get_field (slot, obj, 5,
					       "CTYPE_PARSTRING");
       /*_.CTYPE_PARSTRING__V22*/ meltfptr[21] = slot;
		      };
		      ;

		      {
			/*^locexp */
			meltgc_append_list ((melt_ptr_t)
					    ( /*_.PARAMDESCLIST__V12*/
					     meltfptr[11]),
					    (melt_ptr_t) ( /*_.CTYPE_PARSTRING__V22*/ meltfptr[21]));
		      }
		      ;
		      MELT_LOCATION
			("warmelt-outobj.melt:3272:/ checksignal");
		      MELT_CHECK_SIGNAL ();
		      ;
      /*_#NULL__L17*/ meltfnum[16] =
			(( /*_.CURARG__V15*/ meltfptr[14]) == NULL);;
		      MELT_LOCATION ("warmelt-outobj.melt:3272:/ cond");
		      /*cond */ if ( /*_#NULL__L17*/ meltfnum[16])	/*then */
			{
			  /*^cond.then */
			  /*^block */
			  /*anyblock */
			  {




			    {
			      MELT_LOCATION
				("warmelt-outobj.melt:3273:/ locexp");
			      /*add2sbuf_strconst */
				meltgc_add_strbuf ((melt_ptr_t)
						   ( /*_.IMPLBUF__V4*/
						    meltfptr[3]),
						   ("meltbp_aptr = (melt_ptr_t*)NULL"));
			    }
			    ;
		  /*clear *//*_.IFELSE___V23*/ meltfptr[22] = 0;
			    /*epilog */
			  }
			  ;
			}
		      else
			{
			  MELT_LOCATION
			    ("warmelt-outobj.melt:3272:/ cond.else");

			  /*^block */
			  /*anyblock */
			  {

			    MELT_LOCATION
			      ("warmelt-outobj.melt:3274:/ checksignal");
			    MELT_CHECK_SIGNAL ();
			    ;
	/*_#IS_A__L18*/ meltfnum[17] =
			      melt_is_instance_of ((melt_ptr_t)
						   ( /*_.CURARG__V15*/
						    meltfptr[14]),
						   (melt_ptr_t) (( /*!CLASS_OBJNIL */ meltfrout->tabval[7])));;
			    MELT_LOCATION ("warmelt-outobj.melt:3274:/ cond");
			    /*cond */ if ( /*_#IS_A__L18*/ meltfnum[17])	/*then */
			      {
				/*^cond.then */
				/*^block */
				/*anyblock */
				{




				  {
				    MELT_LOCATION
				      ("warmelt-outobj.melt:3275:/ locexp");
				    /*add2sbuf_strconst */
				      meltgc_add_strbuf ((melt_ptr_t)
							 ( /*_.IMPLBUF__V4*/
							  meltfptr[3]),
							 ("meltbp_aptr = /*nil*/(melt_ptr_t*)NULL"));
				  }
				  ;
		    /*clear *//*_.IFELSE___V24*/ meltfptr[23] =
				    0;
				  /*epilog */
				}
				;
			      }
			    else
			      {
				MELT_LOCATION
				  ("warmelt-outobj.melt:3274:/ cond.else");

				/*^block */
				/*anyblock */
				{

				  MELT_LOCATION
				    ("warmelt-outobj.melt:3276:/ checksignal");
				  MELT_CHECK_SIGNAL ();
				  ;
	  /*_#__L19*/ meltfnum[18] =
				    (( /*_.CURCTYP__V18*/ meltfptr[16]) ==
				     (( /*!CTYPE_VALUE */ meltfrout->
				       tabval[8])));;
				  MELT_LOCATION
				    ("warmelt-outobj.melt:3276:/ cond");
				  /*cond */ if ( /*_#__L19*/ meltfnum[18])	/*then */
				    {
				      /*^cond.then */
				      /*^block */
				      /*anyblock */
				      {


					{
					  MELT_LOCATION
					    ("warmelt-outobj.melt:3277:/ locexp");
					  /*add2sbuf_strconst */
					    meltgc_add_strbuf ((melt_ptr_t)
							       ( /*_.IMPLBUF__V4*/ meltfptr[3]), ("meltbp_aptr = (melt_ptr_t*) &"));
					}
					;
	    /*_#GET_INT__L20*/ meltfnum[19] =
					  (melt_get_int
					   ((melt_ptr_t)
					    ( /*_.BOXDEPTHP1__V13*/
					     meltfptr[12])));;
					MELT_LOCATION
					  ("warmelt-outobj.melt:3278:/ checksignal");
					MELT_CHECK_SIGNAL ();
					;
					/*^msend */
					/*msend */
					{
					  union meltparam_un argtab[3];
					  memset (&argtab, 0,
						  sizeof (argtab));
					  /*^ojbmsend.arg */
					  argtab[0].meltbp_aptr =
					    (melt_ptr_t *) & /*_.DECLBUF__V3*/
					    meltfptr[2];
					  /*^ojbmsend.arg */
					  argtab[1].meltbp_aptr =
					    (melt_ptr_t *) & /*_.IMPLBUF__V4*/
					    meltfptr[3];
					  /*^ojbmsend.arg */
					  argtab[2].meltbp_long =
					    /*_#GET_INT__L20*/ meltfnum[19];
					  /*_.OUTPUT_C_CODE__V26*/
					    meltfptr[25] =
					    meltgc_send ((melt_ptr_t)
							 ( /*_.CURARG__V15*/
							  meltfptr[14]),
							 (melt_ptr_t) (( /*!OUTPUT_C_CODE */ meltfrout->tabval[9])), (MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_LONG ""), argtab, "", (union meltparam_un *) 0);
					}
					;
					MELT_LOCATION
					  ("warmelt-outobj.melt:3276:/ quasiblock");


					/*_.PROGN___V27*/ meltfptr[26] =
					  /*_.OUTPUT_C_CODE__V26*/
					  meltfptr[25];;
					/*^compute */
					/*_.IFELSE___V25*/ meltfptr[24] =
					  /*_.PROGN___V27*/ meltfptr[26];;
					/*epilog */

					MELT_LOCATION
					  ("warmelt-outobj.melt:3276:/ clear");
		      /*clear *//*_#GET_INT__L20*/
					  meltfnum[19] = 0;
					/*^clear */
		      /*clear *//*_.OUTPUT_C_CODE__V26*/
					  meltfptr[25] = 0;
					/*^clear */
		      /*clear *//*_.PROGN___V27*/
					  meltfptr[26] = 0;
				      }
				      ;
				    }
				  else
				    {	/*^cond.else */

				      /*^block */
				      /*anyblock */
				      {

					MELT_LOCATION
					  ("warmelt-outobj.melt:3281:/ getslot");
					{
					  melt_ptr_t slot = NULL, obj = NULL;
					  obj =
					    (melt_ptr_t) ( /*_.CURCTYP__V18*/
							  meltfptr[16])
					    /*=obj*/ ;
					  melt_object_get_field (slot, obj, 6,
								 "CTYPE_ARGFIELD");
	     /*_.CTYPE_ARGFIELD__V28*/
					    meltfptr[25] = slot;
					};
					;

					{
					  /*^locexp */
					  /*add2sbuf_string */
					    meltgc_add_strbuf ((melt_ptr_t)
							       ( /*_.IMPLBUF__V4*/ meltfptr[3]),
							       melt_string_str
							       ((melt_ptr_t)
								( /*_.CTYPE_ARGFIELD__V28*/ meltfptr[25])));
					}
					;

					{
					  MELT_LOCATION
					    ("warmelt-outobj.melt:3282:/ locexp");
					  /*add2sbuf_strconst */
					    meltgc_add_strbuf ((melt_ptr_t)
							       ( /*_.IMPLBUF__V4*/ meltfptr[3]), (" = "));
					}
					;
	    /*_#GET_INT__L21*/ meltfnum[19] =
					  (melt_get_int
					   ((melt_ptr_t)
					    ( /*_.BOXDEPTHP1__V13*/
					     meltfptr[12])));;
					MELT_LOCATION
					  ("warmelt-outobj.melt:3283:/ checksignal");
					MELT_CHECK_SIGNAL ();
					;
					/*^msend */
					/*msend */
					{
					  union meltparam_un argtab[3];
					  memset (&argtab, 0,
						  sizeof (argtab));
					  /*^ojbmsend.arg */
					  argtab[0].meltbp_aptr =
					    (melt_ptr_t *) & /*_.DECLBUF__V3*/
					    meltfptr[2];
					  /*^ojbmsend.arg */
					  argtab[1].meltbp_aptr =
					    (melt_ptr_t *) & /*_.IMPLBUF__V4*/
					    meltfptr[3];
					  /*^ojbmsend.arg */
					  argtab[2].meltbp_long =
					    /*_#GET_INT__L21*/ meltfnum[19];
					  /*_.OUTPUT_C_CODE__V29*/
					    meltfptr[26] =
					    meltgc_send ((melt_ptr_t)
							 ( /*_.CURARG__V15*/
							  meltfptr[14]),
							 (melt_ptr_t) (( /*!OUTPUT_C_CODE */ meltfrout->tabval[9])), (MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_LONG ""), argtab, "", (union meltparam_un *) 0);
					}
					;
					MELT_LOCATION
					  ("warmelt-outobj.melt:3280:/ quasiblock");


					/*_.PROGN___V30*/ meltfptr[29] =
					  /*_.OUTPUT_C_CODE__V29*/
					  meltfptr[26];;
					/*^compute */
					/*_.IFELSE___V25*/ meltfptr[24] =
					  /*_.PROGN___V30*/ meltfptr[29];;
					/*epilog */

					MELT_LOCATION
					  ("warmelt-outobj.melt:3276:/ clear");
		      /*clear *//*_.CTYPE_ARGFIELD__V28*/
					  meltfptr[25] = 0;
					/*^clear */
		      /*clear *//*_#GET_INT__L21*/
					  meltfnum[19] = 0;
					/*^clear */
		      /*clear *//*_.OUTPUT_C_CODE__V29*/
					  meltfptr[26] = 0;
					/*^clear */
		      /*clear *//*_.PROGN___V30*/
					  meltfptr[29] = 0;
				      }
				      ;
				    }
				  ;
				  /*_.IFELSE___V24*/ meltfptr[23] =
				    /*_.IFELSE___V25*/ meltfptr[24];;
				  /*epilog */

				  MELT_LOCATION
				    ("warmelt-outobj.melt:3274:/ clear");
		    /*clear *//*_#__L19*/ meltfnum[18] = 0;
				  /*^clear */
		    /*clear *//*_.IFELSE___V25*/ meltfptr[24] =
				    0;
				}
				;
			      }
			    ;
			    /*_.IFELSE___V23*/ meltfptr[22] =
			      /*_.IFELSE___V24*/ meltfptr[23];;
			    /*epilog */

			    MELT_LOCATION
			      ("warmelt-outobj.melt:3272:/ clear");
		  /*clear *//*_#IS_A__L18*/ meltfnum[17] = 0;
			    /*^clear */
		  /*clear *//*_.IFELSE___V24*/ meltfptr[23] = 0;
			  }
			  ;
			}
		      ;

		      {
			MELT_LOCATION ("warmelt-outobj.melt:3285:/ locexp");
			/*add2sbuf_strconst */
			  meltgc_add_strbuf ((melt_ptr_t)
					     ( /*_.IMPLBUF__V4*/ meltfptr[3]),
					     (";"));
		      }
		      ;
      /*_#GET_INT__L22*/ meltfnum[19] =
			(melt_get_int
			 ((melt_ptr_t)
			  ( /*_.BOXDEPTHP1__V13*/ meltfptr[12])));;

		      {
			MELT_LOCATION ("warmelt-outobj.melt:3286:/ locexp");
			meltgc_strbuf_add_indent ((melt_ptr_t)
						  ( /*_.IMPLBUF__V4*/
						   meltfptr[3]),
						  ( /*_#GET_INT__L22*/
						   meltfnum[19]), 0);
		      }
		      ;

		      MELT_LOCATION ("warmelt-outobj.melt:3265:/ clear");
		/*clear *//*_.CURCTYP__V18*/ meltfptr[16] = 0;
		      /*^clear */
		/*clear *//*_.IFCPP___V19*/ meltfptr[18] = 0;
		      /*^clear */
		/*clear *//*_#GET_INT__L15*/ meltfnum[11] = 0;
		      /*^clear */
		/*clear *//*_.OUTPUT_LOCATION__V21*/ meltfptr[19] = 0;
		      /*^clear */
		/*clear *//*_#I__L16*/ meltfnum[15] = 0;
		      /*^clear */
		/*clear *//*_.CTYPE_PARSTRING__V22*/ meltfptr[21] = 0;
		      /*^clear */
		/*clear *//*_#NULL__L17*/ meltfnum[16] = 0;
		      /*^clear */
		/*clear *//*_.IFELSE___V23*/ meltfptr[22] = 0;
		      /*^clear */
		/*clear *//*_#GET_INT__L22*/ meltfnum[19] = 0;
		      /*epilog */
		    }
		    ;
		  }		/*noelse */
		;

#if MELT_HAVE_DEBUG
		MELT_LOCATION ("warmelt-outobj.melt:3288:/ cppif.then");
		/*^block */
		/*anyblock */
		{

		  /*^checksignal */
		  MELT_CHECK_SIGNAL ();
		  ;
      /*_#STRBUF_USEDLENGTH__L23*/ meltfnum[18] =
		    melt_strbuf_usedlength ((melt_ptr_t)
					    ( /*_.IMPLBUF__V4*/
					     meltfptr[3]));;
		  MELT_LOCATION ("warmelt-outobj.melt:3289:/ cond");
		  /*cond */ if (
				 /*ifisa */
				 melt_is_instance_of ((melt_ptr_t)
						      (( /*!BUFFER_LIMIT_CONT */ meltfrout->tabval[10])),
						      (melt_ptr_t) (( /*!CLASS_REFERENCE */ meltfrout->tabval[11])))
		    )		/*then */
		    {
		      /*^cond.then */
		      /*^getslot */
		      {
			melt_ptr_t slot = NULL, obj = NULL;
			obj =
			  (melt_ptr_t) (( /*!BUFFER_LIMIT_CONT */ meltfrout->
					 tabval[10])) /*=obj*/ ;
			melt_object_get_field (slot, obj, 0,
					       "REFERENCED_VALUE");
	/*_.REFERENCED_VALUE__V32*/ meltfptr[26] = slot;
		      };
		      ;
		    }
		  else
		    {		/*^cond.else */

       /*_.REFERENCED_VALUE__V32*/ meltfptr[26] = NULL;;
		    }
		  ;
		  /*^compute */
      /*_#GET_INT__L24*/ meltfnum[17] =
		    (melt_get_int
		     ((melt_ptr_t)
		      ( /*_.REFERENCED_VALUE__V32*/ meltfptr[26])));;
		  /*^compute */
      /*_#I__L25*/ meltfnum[11] =
		    (( /*_#STRBUF_USEDLENGTH__L23*/ meltfnum[18]) <
		     ( /*_#GET_INT__L24*/ meltfnum[17]));;
		  MELT_LOCATION ("warmelt-outobj.melt:3288:/ cond");
		  /*cond */ if ( /*_#I__L25*/ meltfnum[11])	/*then */
		    {
		      /*^cond.then */
		      /*_.IFELSE___V33*/ meltfptr[29] = ( /*nil */ NULL);;
		    }
		  else
		    {
		      MELT_LOCATION ("warmelt-outobj.melt:3288:/ cond.else");

		      /*^block */
		      /*anyblock */
		      {




			{
			  /*^locexp */
			  melt_assert_failed (("check limited implbuf"),
					      ("warmelt-outobj.melt")
					      ? ("warmelt-outobj.melt") :
					      __FILE__,
					      (3288) ? (3288) : __LINE__,
					      __FUNCTION__);
			  ;
			}
			;
		  /*clear *//*_.IFELSE___V33*/ meltfptr[29] = 0;
			/*epilog */
		      }
		      ;
		    }
		  ;
		  /*^compute */
		  /*_.IFCPP___V31*/ meltfptr[25] =
		    /*_.IFELSE___V33*/ meltfptr[29];;
		  /*epilog */

		  MELT_LOCATION ("warmelt-outobj.melt:3288:/ clear");
		/*clear *//*_#STRBUF_USEDLENGTH__L23*/ meltfnum[18] = 0;
		  /*^clear */
		/*clear *//*_.REFERENCED_VALUE__V32*/ meltfptr[26] = 0;
		  /*^clear */
		/*clear *//*_#GET_INT__L24*/ meltfnum[17] = 0;
		  /*^clear */
		/*clear *//*_#I__L25*/ meltfnum[11] = 0;
		  /*^clear */
		/*clear *//*_.IFELSE___V33*/ meltfptr[29] = 0;
		}

#else /*MELT_HAVE_DEBUG */
		/*^cppif.else */
		/*_.IFCPP___V31*/ meltfptr[25] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
		;
		if ( /*_#CURANK__L10*/ meltfnum[9] < 0)
		  break;
	      }			/* end  foreach_in_multiple meltcit1__EACHTUP */

	    /*citerepilog */

	    MELT_LOCATION ("warmelt-outobj.melt:3260:/ clear");
	      /*clear *//*_.CURARG__V15*/ meltfptr[14] = 0;
	    /*^clear */
	      /*clear *//*_#CURANK__L10*/ meltfnum[9] = 0;
	    /*^clear */
	      /*clear *//*_.IFCPP___V16*/ meltfptr[15] = 0;
	    /*^clear */
	      /*clear *//*_#I__L13*/ meltfnum[10] = 0;
	    /*^clear */
	      /*clear *//*_.IFCPP___V31*/ meltfptr[25] = 0;
	  }			/*endciterblock FOREACH_IN_MULTIPLE */
	  ;
	  MELT_LOCATION ("warmelt-outobj.melt:3252:/ quasiblock");


	  /*epilog */

	  MELT_LOCATION ("warmelt-outobj.melt:3251:/ clear");
	     /*clear *//*_#I__L7*/ meltfnum[6] = 0;
	  /*^clear */
	     /*clear *//*_#I__L8*/ meltfnum[7] = 0;
	  /*^clear */
	     /*clear *//*_#I__L9*/ meltfnum[8] = 0;
	}
	;
      }				/*noelse */
    ;
    MELT_LOCATION ("warmelt-outobj.melt:3295:/ quasiblock");


    /*^newclosure */
		 /*newclosure *//*_.LAMBDA___V35*/ meltfptr[23] =
      (melt_ptr_t)
      meltgc_new_closure ((meltobject_ptr_t)
			  (((melt_ptr_t) (MELT_PREDEF (DISCR_CLOSURE)))),
			  (meltroutine_ptr_t) (( /*!konst_16 */ meltfrout->
						tabval[16])), (3));
    ;
    /*^putclosedv */
    /*putclosv */
    melt_assertmsg ("putclosv checkclo",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.LAMBDA___V35*/ meltfptr[23])) ==
		    MELTOBMAG_CLOSURE);
    melt_assertmsg ("putclosv checkoff", 0 >= 0
		    && 0 <
		    melt_closure_size ((melt_ptr_t)
				       ( /*_.LAMBDA___V35*/ meltfptr[23])));
    ((meltclosure_ptr_t) /*_.LAMBDA___V35*/ meltfptr[23])->tabval[0] =
      (melt_ptr_t) ( /*_.DECLBUF__V3*/ meltfptr[2]);
    ;
    /*^putclosedv */
    /*putclosv */
    melt_assertmsg ("putclosv checkclo",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.LAMBDA___V35*/ meltfptr[23])) ==
		    MELTOBMAG_CLOSURE);
    melt_assertmsg ("putclosv checkoff", 1 >= 0
		    && 1 <
		    melt_closure_size ((melt_ptr_t)
				       ( /*_.LAMBDA___V35*/ meltfptr[23])));
    ((meltclosure_ptr_t) /*_.LAMBDA___V35*/ meltfptr[23])->tabval[1] =
      (melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]);
    ;
    /*^putclosedv */
    /*putclosv */
    melt_assertmsg ("putclosv checkclo",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.LAMBDA___V35*/ meltfptr[23])) ==
		    MELTOBMAG_CLOSURE);
    melt_assertmsg ("putclosv checkoff", 2 >= 0
		    && 2 <
		    melt_closure_size ((melt_ptr_t)
				       ( /*_.LAMBDA___V35*/ meltfptr[23])));
    ((meltclosure_ptr_t) /*_.LAMBDA___V35*/ meltfptr[23])->tabval[2] =
      (melt_ptr_t) ( /*_.BOXDEPTHP1__V13*/ meltfptr[12]);
    ;
    /*_.LAMBDA___V34*/ meltfptr[24] = /*_.LAMBDA___V35*/ meltfptr[23];;
    MELT_LOCATION ("warmelt-outobj.melt:3293:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & /*_.LAMBDA___V34*/ meltfptr[24];
      /*_.LIST_EVERY__V36*/ meltfptr[16] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!LIST_EVERY */ meltfrout->tabval[12])),
		    (melt_ptr_t) ( /*_.ADEST__V9*/ meltfptr[8]),
		    (MELTBPARSTR_PTR ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:3301:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   (" melt_apply ((meltclosure_ptr_t)("));
    }
    ;
 /*_#I__L26*/ meltfnum[15] =
      ((1) + ( /*_#DEPTH__L1*/ meltfnum[0]));;
    MELT_LOCATION ("warmelt-outobj.melt:3302:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^msend */
    /*msend */
    {
      union meltparam_un argtab[3];
      memset (&argtab, 0, sizeof (argtab));
      /*^ojbmsend.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.DECLBUF__V3*/ meltfptr[2];
      /*^ojbmsend.arg */
      argtab[1].meltbp_aptr = (melt_ptr_t *) & /*_.IMPLBUF__V4*/ meltfptr[3];
      /*^ojbmsend.arg */
      argtab[2].meltbp_long = /*_#I__L26*/ meltfnum[15];
      /*_.OUTPUT_C_CODE__V37*/ meltfptr[18] =
	meltgc_send ((melt_ptr_t) ( /*_.OCLOS__V10*/ meltfptr[9]),
		     (melt_ptr_t) (( /*!OUTPUT_C_CODE */ meltfrout->
				    tabval[9])),
		     (MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_LONG ""),
		     argtab, "", (union meltparam_un *) 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:3303:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   ("), (melt_ptr_t)("));
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:3305:/ quasiblock");


 /*_.FIRSTARG__V39*/ meltfptr[21] =
      (melt_multiple_nth
       ((melt_ptr_t) ( /*_.OARGS__V11*/ meltfptr[10]), (0)));;
    /*^compute */
 /*_#I__L27*/ meltfnum[16] =
      ((1) + ( /*_#DEPTH__L1*/ meltfnum[0]));;
    MELT_LOCATION ("warmelt-outobj.melt:3306:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^msend */
    /*msend */
    {
      union meltparam_un argtab[3];
      memset (&argtab, 0, sizeof (argtab));
      /*^ojbmsend.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.DECLBUF__V3*/ meltfptr[2];
      /*^ojbmsend.arg */
      argtab[1].meltbp_aptr = (melt_ptr_t *) & /*_.IMPLBUF__V4*/ meltfptr[3];
      /*^ojbmsend.arg */
      argtab[2].meltbp_long = /*_#I__L27*/ meltfnum[16];
      /*_.OUTPUT_C_CODE__V40*/ meltfptr[22] =
	meltgc_send ((melt_ptr_t) ( /*_.FIRSTARG__V39*/ meltfptr[21]),
		     (melt_ptr_t) (( /*!OUTPUT_C_CODE */ meltfrout->
				    tabval[9])),
		     (MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_LONG ""),
		     argtab, "", (union meltparam_un *) 0);
    }
    ;
    /*_.LET___V38*/ meltfptr[19] = /*_.OUTPUT_C_CODE__V40*/ meltfptr[22];;

    MELT_LOCATION ("warmelt-outobj.melt:3305:/ clear");
	   /*clear *//*_.FIRSTARG__V39*/ meltfptr[21] = 0;
    /*^clear */
	   /*clear *//*_#I__L27*/ meltfnum[16] = 0;
    /*^clear */
	   /*clear *//*_.OUTPUT_C_CODE__V40*/ meltfptr[22] = 0;

    {
      MELT_LOCATION ("warmelt-outobj.melt:3308:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   ("), ("));
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:3312:/ quasiblock");


    /*^newclosure */
		 /*newclosure *//*_.LAMBDA___V42*/ meltfptr[29] =
      (melt_ptr_t)
      meltgc_new_closure ((meltobject_ptr_t)
			  (((melt_ptr_t) (MELT_PREDEF (DISCR_CLOSURE)))),
			  (meltroutine_ptr_t) (( /*!konst_17 */ meltfrout->
						tabval[17])), (1));
    ;
    /*^putclosedv */
    /*putclosv */
    melt_assertmsg ("putclosv checkclo",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.LAMBDA___V42*/ meltfptr[29])) ==
		    MELTOBMAG_CLOSURE);
    melt_assertmsg ("putclosv checkoff", 0 >= 0
		    && 0 <
		    melt_closure_size ((melt_ptr_t)
				       ( /*_.LAMBDA___V42*/ meltfptr[29])));
    ((meltclosure_ptr_t) /*_.LAMBDA___V42*/ meltfptr[29])->tabval[0] =
      (melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]);
    ;
    /*_.LAMBDA___V41*/ meltfptr[26] = /*_.LAMBDA___V42*/ meltfptr[29];;
    MELT_LOCATION ("warmelt-outobj.melt:3310:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & /*_.LAMBDA___V41*/ meltfptr[26];
      /*_.LIST_EVERY__V43*/ meltfptr[21] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!LIST_EVERY */ meltfrout->tabval[12])),
		    (melt_ptr_t) ( /*_.PARAMDESCLIST__V12*/ meltfptr[11]),
		    (MELTBPARSTR_PTR ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:3315:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   ("\"\"), "));
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:3317:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#I__L28*/ meltfnum[19] =
      (( /*_#NBARG__L3*/ meltfnum[1]) > (1));;
    MELT_LOCATION ("warmelt-outobj.melt:3317:/ cond");
    /*cond */ if ( /*_#I__L28*/ meltfnum[19])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{


	  {
	    MELT_LOCATION ("warmelt-outobj.melt:3318:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t)
				 ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				 ("argtab,"));
	  }
	  ;
	  /*epilog */
	}
	;
      }
    else
      {
	MELT_LOCATION ("warmelt-outobj.melt:3317:/ cond.else");

	/*^block */
	/*anyblock */
	{


	  {
	    MELT_LOCATION ("warmelt-outobj.melt:3319:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t)
				 ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				 ("(union meltparam_un*)0,"));
	  }
	  ;
	  /*epilog */
	}
	;
      }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:3321:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   (" \"\", (union meltparam_un*)0"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:3322:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   (");"));
    }
    ;
 /*_#I__L29*/ meltfnum[18] =
      ((1) + ( /*_#DEPTH__L1*/ meltfnum[0]));;

    {
      MELT_LOCATION ("warmelt-outobj.melt:3323:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				( /*_#I__L29*/ meltfnum[18]), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:3324:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   ("}"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:3325:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				( /*_#DEPTH__L1*/ meltfnum[0]), 0);
    }
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:3326:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#STRBUF_USEDLENGTH__L30*/ meltfnum[17] =
	melt_strbuf_usedlength ((melt_ptr_t)
				( /*_.IMPLBUF__V4*/ meltfptr[3]));;
      MELT_LOCATION ("warmelt-outobj.melt:3327:/ cond");
      /*cond */ if (
		     /*ifisa */
		     melt_is_instance_of ((melt_ptr_t)
					  (( /*!BUFFER_LIMIT_CONT */
					    meltfrout->tabval[10])),
					  (melt_ptr_t) (( /*!CLASS_REFERENCE */ meltfrout->tabval[11])))
	)			/*then */
	{
	  /*^cond.then */
	  /*^getslot */
	  {
	    melt_ptr_t slot = NULL, obj = NULL;
	    obj =
	      (melt_ptr_t) (( /*!BUFFER_LIMIT_CONT */ meltfrout->
			     tabval[10])) /*=obj*/ ;
	    melt_object_get_field (slot, obj, 0, "REFERENCED_VALUE");
     /*_.REFERENCED_VALUE__V45*/ meltfptr[44] = slot;
	  };
	  ;
	}
      else
	{			/*^cond.else */

    /*_.REFERENCED_VALUE__V45*/ meltfptr[44] = NULL;;
	}
      ;
      /*^compute */
   /*_#GET_INT__L31*/ meltfnum[11] =
	(melt_get_int
	 ((melt_ptr_t) ( /*_.REFERENCED_VALUE__V45*/ meltfptr[44])));;
      /*^compute */
   /*_#I__L32*/ meltfnum[6] =
	(( /*_#STRBUF_USEDLENGTH__L30*/ meltfnum[17]) <
	 ( /*_#GET_INT__L31*/ meltfnum[11]));;
      MELT_LOCATION ("warmelt-outobj.melt:3326:/ cond");
      /*cond */ if ( /*_#I__L32*/ meltfnum[6])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V46*/ meltfptr[45] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-outobj.melt:3326:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check limited implbuf"),
				  ("warmelt-outobj.melt")
				  ? ("warmelt-outobj.melt") : __FILE__,
				  (3326) ? (3326) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V46*/ meltfptr[45] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V44*/ meltfptr[22] = /*_.IFELSE___V46*/ meltfptr[45];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:3326:/ clear");
	     /*clear *//*_#STRBUF_USEDLENGTH__L30*/ meltfnum[17] = 0;
      /*^clear */
	     /*clear *//*_.REFERENCED_VALUE__V45*/ meltfptr[44] = 0;
      /*^clear */
	     /*clear *//*_#GET_INT__L31*/ meltfnum[11] = 0;
      /*^clear */
	     /*clear *//*_#I__L32*/ meltfnum[6] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V46*/ meltfptr[45] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V44*/ meltfptr[22] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    /*^compute */
    /*_.LET___V7*/ meltfptr[5] = /*_.IFCPP___V44*/ meltfptr[22];;

    MELT_LOCATION ("warmelt-outobj.melt:3239:/ clear");
	   /*clear *//*_.ALOC__V8*/ meltfptr[7] = 0;
    /*^clear */
	   /*clear *//*_.ADEST__V9*/ meltfptr[8] = 0;
    /*^clear */
	   /*clear *//*_.OCLOS__V10*/ meltfptr[9] = 0;
    /*^clear */
	   /*clear *//*_.OARGS__V11*/ meltfptr[10] = 0;
    /*^clear */
	   /*clear *//*_#NBARG__L3*/ meltfnum[1] = 0;
    /*^clear */
	   /*clear *//*_.PARAMDESCLIST__V12*/ meltfptr[11] = 0;
    /*^clear */
	   /*clear *//*_#I__L4*/ meltfnum[3] = 0;
    /*^clear */
	   /*clear *//*_.BOXDEPTHP1__V13*/ meltfptr[12] = 0;
    /*^clear */
	   /*clear *//*_.OUTPUT_LOCATION__V14*/ meltfptr[13] = 0;
    /*^clear */
	   /*clear *//*_#I__L5*/ meltfnum[4] = 0;
    /*^clear */
	   /*clear *//*_#I__L6*/ meltfnum[5] = 0;
    /*^clear */
	   /*clear *//*_.LAMBDA___V34*/ meltfptr[24] = 0;
    /*^clear */
	   /*clear *//*_.LIST_EVERY__V36*/ meltfptr[16] = 0;
    /*^clear */
	   /*clear *//*_#I__L26*/ meltfnum[15] = 0;
    /*^clear */
	   /*clear *//*_.OUTPUT_C_CODE__V37*/ meltfptr[18] = 0;
    /*^clear */
	   /*clear *//*_.LET___V38*/ meltfptr[19] = 0;
    /*^clear */
	   /*clear *//*_.LAMBDA___V41*/ meltfptr[26] = 0;
    /*^clear */
	   /*clear *//*_.LIST_EVERY__V43*/ meltfptr[21] = 0;
    /*^clear */
	   /*clear *//*_#I__L28*/ meltfnum[19] = 0;
    /*^clear */
	   /*clear *//*_#I__L29*/ meltfnum[18] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V44*/ meltfptr[22] = 0;
    MELT_LOCATION ("warmelt-outobj.melt:3237:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.LET___V7*/ meltfptr[5];;

    {
      MELT_LOCATION ("warmelt-outobj.melt:3237:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V5*/ meltfptr[4] = 0;
    /*^clear */
	   /*clear *//*_.LET___V7*/ meltfptr[5] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("OUTPUCOD_OBJAPPLY", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_78_warmelt_outobj_OUTPUCOD_OBJAPPLY_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_78_warmelt_outobj_OUTPUCOD_OBJAPPLY */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_79_warmelt_outobj_LAMBDA___11__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un * meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un * meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_79_warmelt_outobj_LAMBDA___11___melt =
    melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_79_warmelt_outobj_LAMBDA___11___st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 6
    melt_ptr_t mcfr_varptr[6];
#define MELTFRAM_NBVARNUM 4
    long mcfr_varnum[4];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_79_warmelt_outobj_LAMBDA___11__ is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_79_warmelt_outobj_LAMBDA___11___st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 6; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_79_warmelt_outobj_LAMBDA___11__ nbval 6*/
  meltfram__.mcfr_nbvar = 6 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("LAMBDA_", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-outobj.melt:3295:/ getarg");
 /*_.CURDEST__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {

 /*_#GET_INT__L1*/ meltfnum[0] =
      (melt_get_int
       ((melt_ptr_t) (( /*~BOXDEPTHP1 */ meltfclos->tabval[2]))));;
    MELT_LOCATION ("warmelt-outobj.melt:3296:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^msend */
    /*msend */
    {
      union meltparam_un argtab[3];
      memset (&argtab, 0, sizeof (argtab));
      /*^ojbmsend.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & ( /*~DECLBUF */ meltfclos->tabval[0]);
      /*^ojbmsend.arg */
      argtab[1].meltbp_aptr =
	(melt_ptr_t *) & ( /*~IMPLBUF */ meltfclos->tabval[1]);
      /*^ojbmsend.arg */
      argtab[2].meltbp_long = /*_#GET_INT__L1*/ meltfnum[0];
      /*_.OUTPUT_C_CODE__V3*/ meltfptr[2] =
	meltgc_send ((melt_ptr_t) ( /*_.CURDEST__V2*/ meltfptr[1]),
		     (melt_ptr_t) (( /*!OUTPUT_C_CODE */ meltfrout->
				    tabval[0])),
		     (MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_LONG ""),
		     argtab, "", (union meltparam_un *) 0);
    }
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:3297:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#STRBUF_USEDLENGTH__L2*/ meltfnum[1] =
	melt_strbuf_usedlength ((melt_ptr_t)
				(( /*~IMPLBUF */ meltfclos->tabval[1])));;
      MELT_LOCATION ("warmelt-outobj.melt:3298:/ cond");
      /*cond */ if (
		     /*ifisa */
		     melt_is_instance_of ((melt_ptr_t)
					  (( /*!BUFFER_LIMIT_CONT */
					    meltfrout->tabval[1])),
					  (melt_ptr_t) (( /*!CLASS_REFERENCE */ meltfrout->tabval[2])))
	)			/*then */
	{
	  /*^cond.then */
	  /*^getslot */
	  {
	    melt_ptr_t slot = NULL, obj = NULL;
	    obj =
	      (melt_ptr_t) (( /*!BUFFER_LIMIT_CONT */ meltfrout->
			     tabval[1])) /*=obj*/ ;
	    melt_object_get_field (slot, obj, 0, "REFERENCED_VALUE");
     /*_.REFERENCED_VALUE__V5*/ meltfptr[4] = slot;
	  };
	  ;
	}
      else
	{			/*^cond.else */

    /*_.REFERENCED_VALUE__V5*/ meltfptr[4] = NULL;;
	}
      ;
      /*^compute */
   /*_#GET_INT__L3*/ meltfnum[2] =
	(melt_get_int
	 ((melt_ptr_t) ( /*_.REFERENCED_VALUE__V5*/ meltfptr[4])));;
      /*^compute */
   /*_#I__L4*/ meltfnum[3] =
	(( /*_#STRBUF_USEDLENGTH__L2*/ meltfnum[1]) <
	 ( /*_#GET_INT__L3*/ meltfnum[2]));;
      MELT_LOCATION ("warmelt-outobj.melt:3297:/ cond");
      /*cond */ if ( /*_#I__L4*/ meltfnum[3])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V6*/ meltfptr[5] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-outobj.melt:3297:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check limited implbuf"),
				  ("warmelt-outobj.melt")
				  ? ("warmelt-outobj.melt") : __FILE__,
				  (3297) ? (3297) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V6*/ meltfptr[5] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V4*/ meltfptr[3] = /*_.IFELSE___V6*/ meltfptr[5];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:3297:/ clear");
	     /*clear *//*_#STRBUF_USEDLENGTH__L2*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.REFERENCED_VALUE__V5*/ meltfptr[4] = 0;
      /*^clear */
	     /*clear *//*_#GET_INT__L3*/ meltfnum[2] = 0;
      /*^clear */
	     /*clear *//*_#I__L4*/ meltfnum[3] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V6*/ meltfptr[5] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V4*/ meltfptr[3] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:3299:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t)
			   (( /*~IMPLBUF */ meltfclos->tabval[1])), (" = "));
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:3295:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*epilog */

    /*^clear */
	   /*clear *//*_#GET_INT__L1*/ meltfnum[0] = 0;
    /*^clear */
	   /*clear *//*_.OUTPUT_C_CODE__V3*/ meltfptr[2] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V4*/ meltfptr[3] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("LAMBDA_", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_79_warmelt_outobj_LAMBDA___11___melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*noretval */ NULL);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_79_warmelt_outobj_LAMBDA___11__ */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_80_warmelt_outobj_LAMBDA___12__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un * meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un * meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_80_warmelt_outobj_LAMBDA___12___melt =
    melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_80_warmelt_outobj_LAMBDA___12___st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 2
    melt_ptr_t mcfr_varptr[2];
/*no varnum*/
#define MELTFRAM_NBVARNUM /*none*/0
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_80_warmelt_outobj_LAMBDA___12__ is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_80_warmelt_outobj_LAMBDA___12___st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 2; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_80_warmelt_outobj_LAMBDA___12__ nbval 2*/
  meltfram__.mcfr_nbvar = 2 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("LAMBDA_", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-outobj.melt:3312:/ getarg");
 /*_.PARD__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


    {
      MELT_LOCATION ("warmelt-outobj.melt:3313:/ locexp");
      /*add2sbuf_string */
	meltgc_add_strbuf ((melt_ptr_t)
			   (( /*~IMPLBUF */ meltfclos->tabval[0])),
			   melt_string_str ((melt_ptr_t)
					    ( /*_.PARD__V2*/ meltfptr[1])));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:3314:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t)
			   (( /*~IMPLBUF */ meltfclos->tabval[0])), (" "));
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:3312:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*epilog */
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("LAMBDA_", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_80_warmelt_outobj_LAMBDA___12___melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*noretval */ NULL);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_80_warmelt_outobj_LAMBDA___12__ */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_81_warmelt_outobj_OUTPUCOD_OBJMSEND (meltclosure_ptr_t meltclosp_,
					      melt_ptr_t meltfirstargp_,
					      const melt_argdescr_cell_t
					      meltxargdescr_[],
					      union meltparam_un *
					      meltxargtab_,
					      const melt_argdescr_cell_t
					      meltxresdescr_[],
					      union meltparam_un *
					      meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_81_warmelt_outobj_OUTPUCOD_OBJMSEND_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_81_warmelt_outobj_OUTPUCOD_OBJMSEND_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 50
    melt_ptr_t mcfr_varptr[50];
#define MELTFRAM_NBVARNUM 34
    long mcfr_varnum[34];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_81_warmelt_outobj_OUTPUCOD_OBJMSEND is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_81_warmelt_outobj_OUTPUCOD_OBJMSEND_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 50; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_81_warmelt_outobj_OUTPUCOD_OBJMSEND nbval 50*/
  meltfram__.mcfr_nbvar = 50 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("OUTPUCOD_OBJMSEND", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-outobj.melt:3334:/ getarg");
 /*_.OMSEND__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.DECLBUF__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.DECLBUF__V3*/ meltfptr[2])) !=
	      NULL);


  /*getarg#2 */
  /*^getarg */
  if (meltxargdescr_[1] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.IMPLBUF__V4*/ meltfptr[3] =
    (meltxargtab_[1].meltbp_aptr) ? (*(meltxargtab_[1].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3])) !=
	      NULL);


  /*getarg#3 */
  /*^getarg */
  if (meltxargdescr_[2] != MELTBPAR_LONG)
    goto lab_endgetargs;
 /*_#DEPTH__L1*/ meltfnum[0] = meltxargtab_[2].meltbp_long;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:3335:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L2*/ meltfnum[1] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.OMSEND__V2*/ meltfptr[1]),
			     (melt_ptr_t) (( /*!CLASS_OBJMSEND */ meltfrout->
					    tabval[0])));;
      MELT_LOCATION ("warmelt-outobj.melt:3335:/ cond");
      /*cond */ if ( /*_#IS_A__L2*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V6*/ meltfptr[5] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-outobj.melt:3335:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check omsend"),
				  ("warmelt-outobj.melt")
				  ? ("warmelt-outobj.melt") : __FILE__,
				  (3335) ? (3335) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V6*/ meltfptr[5] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V5*/ meltfptr[4] = /*_.IFELSE___V6*/ meltfptr[5];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:3335:/ clear");
	     /*clear *//*_#IS_A__L2*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V6*/ meltfptr[5] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V5*/ meltfptr[4] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-outobj.melt:3336:/ quasiblock");


    /*^getslot */
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.OMSEND__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 0, "OBI_LOC");
  /*_.OLOC__V8*/ meltfptr[7] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-outobj.melt:3337:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.OMSEND__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 1, "OBDI_DESTLIST");
  /*_.ODEST__V9*/ meltfptr[8] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-outobj.melt:3338:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.OMSEND__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 2, "OBMSND_SEL");
  /*_.OSEL__V10*/ meltfptr[9] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-outobj.melt:3339:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.OMSEND__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 3, "OBMSND_RECV");
  /*_.ORECV__V11*/ meltfptr[10] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-outobj.melt:3340:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.OMSEND__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 4, "OBMSND_ARGS");
  /*_.OARGS__V12*/ meltfptr[11] = slot;
    };
    ;
 /*_#NBARG__L3*/ meltfnum[1] =
      (melt_multiple_length ((melt_ptr_t) ( /*_.OARGS__V12*/ meltfptr[11])));;
    /*^compute */
 /*_.PARAMDESCLIST__V13*/ meltfptr[12] =
      (meltgc_new_list
       ((meltobject_ptr_t) (( /*!DISCR_LIST */ meltfrout->tabval[1]))));;
    /*^compute */
 /*_#I__L4*/ meltfnum[3] =
      ((1) + ( /*_#DEPTH__L1*/ meltfnum[0]));;
    /*^compute */
 /*_.BOXDEPTHP1__V14*/ meltfptr[13] =
      (meltgc_new_int
       ((meltobject_ptr_t) (( /*!DISCR_INTEGER */ meltfrout->tabval[2])),
	( /*_#I__L4*/ meltfnum[3])));;
    MELT_LOCATION ("warmelt-outobj.melt:3345:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[3];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.IMPLBUF__V4*/ meltfptr[3];
      /*^apply.arg */
      argtab[1].meltbp_long = /*_#DEPTH__L1*/ meltfnum[0];
      /*^apply.arg */
      argtab[2].meltbp_cstring = "msend";
      /*_.OUTPUT_LOCATION__V15*/ meltfptr[14] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!OUTPUT_LOCATION */ meltfrout->tabval[3])),
		    (melt_ptr_t) ( /*_.OLOC__V8*/ meltfptr[7]),
		    (MELTBPARSTR_PTR MELTBPARSTR_LONG MELTBPARSTR_CSTRING ""),
		    argtab, "", (union meltparam_un *) 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:3346:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   ("/*msend*/{"));
    }
    ;
 /*_#I__L5*/ meltfnum[4] =
      ((1) + ( /*_#DEPTH__L1*/ meltfnum[0]));;

    {
      MELT_LOCATION ("warmelt-outobj.melt:3347:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				( /*_#I__L5*/ meltfnum[4]), 0);
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:3348:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#I__L6*/ meltfnum[5] =
      (( /*_#NBARG__L3*/ meltfnum[1]) > (0));;
    MELT_LOCATION ("warmelt-outobj.melt:3348:/ cond");
    /*cond */ if ( /*_#I__L6*/ meltfnum[5])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{


	  {
	    MELT_LOCATION ("warmelt-outobj.melt:3353:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t)
				 ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				 ("union meltparam_un argtab["));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:3354:/ locexp");
	    meltgc_add_strbuf_dec ((melt_ptr_t)
				   ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				   ( /*_#NBARG__L3*/ meltfnum[1]));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:3355:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t)
				 ( /*_.IMPLBUF__V4*/ meltfptr[3]), ("];"));
	  }
	  ;
   /*_#I__L7*/ meltfnum[6] =
	    ((1) + ( /*_#DEPTH__L1*/ meltfnum[0]));;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:3356:/ locexp");
	    meltgc_strbuf_add_indent ((melt_ptr_t)
				      ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				      ( /*_#I__L7*/ meltfnum[6]), 0);
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:3357:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t)
				 ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				 ("memset(&argtab, 0, sizeof(argtab));"));
	  }
	  ;
   /*_#I__L8*/ meltfnum[7] =
	    ((1) + ( /*_#DEPTH__L1*/ meltfnum[0]));;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:3358:/ locexp");
	    meltgc_strbuf_add_indent ((melt_ptr_t)
				      ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				      ( /*_#I__L8*/ meltfnum[7]), 0);
	  }
	  ;
	  /*citerblock FOREACH_IN_MULTIPLE */
	  {
	    /* start foreach_in_multiple meltcit1__EACHTUP */
	    long meltcit1__EACHTUP_ln =
	      melt_multiple_length ((melt_ptr_t) /*_.OARGS__V12*/
				    meltfptr[11]);
	    for ( /*_#CURANK__L9*/ meltfnum[8] = 0;
		 ( /*_#CURANK__L9*/ meltfnum[8] >= 0)
		 && ( /*_#CURANK__L9*/ meltfnum[8] < meltcit1__EACHTUP_ln);
	/*_#CURANK__L9*/ meltfnum[8]++)
	      {
		/*_.CURARG__V16*/ meltfptr[15] =
		  melt_multiple_nth ((melt_ptr_t)
				     ( /*_.OARGS__V12*/ meltfptr[11]),
				     /*_#CURANK__L9*/ meltfnum[8]);



		MELT_LOCATION ("warmelt-outobj.melt:3363:/ quasiblock");


		/*^checksignal */
		MELT_CHECK_SIGNAL ();
		;
		/*^msend */
		/*msend */
		{
		  union meltparam_un argtab[1];
		  memset (&argtab, 0, sizeof (argtab));
		  /*^ojbmsend.arg */
		  argtab[0].meltbp_aptr = /*nil */ (melt_ptr_t *) NULL;
		  /*_.CURCTYP__V17*/ meltfptr[16] =
		    meltgc_send ((melt_ptr_t)
				 ( /*_.CURARG__V16*/ meltfptr[15]),
				 (melt_ptr_t) (( /*!GET_CTYPE */ meltfrout->
						tabval[4])),
				 (MELTBPARSTR_PTR ""), argtab, "",
				 (union meltparam_un *) 0);
		}
		;

#if MELT_HAVE_DEBUG
		MELT_LOCATION ("warmelt-outobj.melt:3364:/ cppif.then");
		/*^block */
		/*anyblock */
		{

		  /*^checksignal */
		  MELT_CHECK_SIGNAL ();
		  ;
      /*_#IS_A__L10*/ meltfnum[9] =
		    melt_is_instance_of ((melt_ptr_t)
					 ( /*_.CURCTYP__V17*/ meltfptr[16]),
					 (melt_ptr_t) (( /*!CLASS_CTYPE */
							meltfrout->
							tabval[5])));;
		  MELT_LOCATION ("warmelt-outobj.melt:3364:/ cond");
		  /*cond */ if ( /*_#IS_A__L10*/ meltfnum[9])	/*then */
		    {
		      /*^cond.then */
		      /*_.IFELSE___V19*/ meltfptr[18] = ( /*nil */ NULL);;
		    }
		  else
		    {
		      MELT_LOCATION ("warmelt-outobj.melt:3364:/ cond.else");

		      /*^block */
		      /*anyblock */
		      {




			{
			  /*^locexp */
			  melt_assert_failed (("check curctyp"),
					      ("warmelt-outobj.melt")
					      ? ("warmelt-outobj.melt") :
					      __FILE__,
					      (3364) ? (3364) : __LINE__,
					      __FUNCTION__);
			  ;
			}
			;
		  /*clear *//*_.IFELSE___V19*/ meltfptr[18] = 0;
			/*epilog */
		      }
		      ;
		    }
		  ;
		  /*^compute */
		  /*_.IFCPP___V18*/ meltfptr[17] =
		    /*_.IFELSE___V19*/ meltfptr[18];;
		  /*epilog */

		  MELT_LOCATION ("warmelt-outobj.melt:3364:/ clear");
		/*clear *//*_#IS_A__L10*/ meltfnum[9] = 0;
		  /*^clear */
		/*clear *//*_.IFELSE___V19*/ meltfptr[18] = 0;
		}

#else /*MELT_HAVE_DEBUG */
		/*^cppif.else */
		/*_.IFCPP___V18*/ meltfptr[17] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
		;
		/*^compute */
    /*_#GET_INT__L11*/ meltfnum[9] =
		  (melt_get_int
		   ((melt_ptr_t) ( /*_.BOXDEPTHP1__V14*/ meltfptr[13])));;
		MELT_LOCATION ("warmelt-outobj.melt:3365:/ checksignal");
		MELT_CHECK_SIGNAL ();
		;
		/*^apply */
		/*apply */
		{
		  union meltparam_un argtab[3];
		  memset (&argtab, 0, sizeof (argtab));
		  /*^apply.arg */
		  argtab[0].meltbp_aptr =
		    (melt_ptr_t *) & /*_.IMPLBUF__V4*/ meltfptr[3];
		  /*^apply.arg */
		  argtab[1].meltbp_long = /*_#GET_INT__L11*/ meltfnum[9];
		  /*^apply.arg */
		  argtab[2].meltbp_cstring = "ojbmsend.arg";
		  /*_.OUTPUT_LOCATION__V20*/ meltfptr[18] =
		    melt_apply ((meltclosure_ptr_t)
				(( /*!OUTPUT_LOCATION */ meltfrout->
				  tabval[3])),
				(melt_ptr_t) ( /*_.OLOC__V8*/ meltfptr[7]),
				(MELTBPARSTR_PTR MELTBPARSTR_LONG
				 MELTBPARSTR_CSTRING ""), argtab, "",
				(union meltparam_un *) 0);
		}
		;

		{
		  MELT_LOCATION ("warmelt-outobj.melt:3366:/ locexp");
		  /*add2sbuf_strconst */
		    meltgc_add_strbuf ((melt_ptr_t)
				       ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				       ("argtab["));
		}
		;

		{
		  MELT_LOCATION ("warmelt-outobj.melt:3367:/ locexp");
		  meltgc_add_strbuf_dec ((melt_ptr_t)
					 ( /*_.IMPLBUF__V4*/ meltfptr[3]),
					 ( /*_#CURANK__L9*/ meltfnum[8]));
		}
		;

		{
		  MELT_LOCATION ("warmelt-outobj.melt:3368:/ locexp");
		  /*add2sbuf_strconst */
		    meltgc_add_strbuf ((melt_ptr_t)
				       ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				       ("]."));
		}
		;
		MELT_LOCATION ("warmelt-outobj.melt:3369:/ getslot");
		{
		  melt_ptr_t slot = NULL, obj = NULL;
		  obj =
		    (melt_ptr_t) ( /*_.CURCTYP__V17*/ meltfptr[16]) /*=obj*/ ;
		  melt_object_get_field (slot, obj, 5, "CTYPE_PARSTRING");
     /*_.CTYPE_PARSTRING__V21*/ meltfptr[20] = slot;
		};
		;

		{
		  /*^locexp */
		  meltgc_append_list ((melt_ptr_t)
				      ( /*_.PARAMDESCLIST__V13*/
				       meltfptr[12]),
				      (melt_ptr_t) ( /*_.CTYPE_PARSTRING__V21*/ meltfptr[20]));
		}
		;
		MELT_LOCATION ("warmelt-outobj.melt:3370:/ checksignal");
		MELT_CHECK_SIGNAL ();
		;
    /*_#NULL__L12*/ meltfnum[11] =
		  (( /*_.CURARG__V16*/ meltfptr[15]) == NULL);;
		MELT_LOCATION ("warmelt-outobj.melt:3370:/ cond");
		/*cond */ if ( /*_#NULL__L12*/ meltfnum[11])	/*then */
		  {
		    /*^cond.then */
		    /*^block */
		    /*anyblock */
		    {




		      {
			MELT_LOCATION ("warmelt-outobj.melt:3371:/ locexp");
			/*add2sbuf_strconst */
			  meltgc_add_strbuf ((melt_ptr_t)
					     ( /*_.IMPLBUF__V4*/ meltfptr[3]),
					     ("meltbp_aptr = (melt_ptr_t*)NULL"));
		      }
		      ;
		/*clear *//*_.IFELSE___V22*/ meltfptr[21] = 0;
		      /*epilog */
		    }
		    ;
		  }
		else
		  {
		    MELT_LOCATION ("warmelt-outobj.melt:3370:/ cond.else");

		    /*^block */
		    /*anyblock */
		    {

		      MELT_LOCATION
			("warmelt-outobj.melt:3373:/ checksignal");
		      MELT_CHECK_SIGNAL ();
		      ;
      /*_#IS_A__L13*/ meltfnum[12] =
			melt_is_instance_of ((melt_ptr_t)
					     ( /*_.CURARG__V16*/
					      meltfptr[15]),
					     (melt_ptr_t) (( /*!CLASS_OBJNIL */ meltfrout->tabval[6])));;
		      MELT_LOCATION ("warmelt-outobj.melt:3373:/ cond");
		      /*cond */ if ( /*_#IS_A__L13*/ meltfnum[12])	/*then */
			{
			  /*^cond.then */
			  /*^block */
			  /*anyblock */
			  {




			    {
			      MELT_LOCATION
				("warmelt-outobj.melt:3374:/ locexp");
			      /*add2sbuf_strconst */
				meltgc_add_strbuf ((melt_ptr_t)
						   ( /*_.IMPLBUF__V4*/
						    meltfptr[3]),
						   ("meltbp_aptr = /*nil*/(melt_ptr_t*)NULL"));
			    }
			    ;
		  /*clear *//*_.IFELSE___V23*/ meltfptr[22] = 0;
			    /*epilog */
			  }
			  ;
			}
		      else
			{
			  MELT_LOCATION
			    ("warmelt-outobj.melt:3373:/ cond.else");

			  /*^block */
			  /*anyblock */
			  {

			    MELT_LOCATION
			      ("warmelt-outobj.melt:3376:/ checksignal");
			    MELT_CHECK_SIGNAL ();
			    ;
	/*_#__L14*/ meltfnum[13] =
			      (( /*_.CURCTYP__V17*/ meltfptr[16]) ==
			       (( /*!CTYPE_VALUE */ meltfrout->tabval[7])));;
			    MELT_LOCATION ("warmelt-outobj.melt:3376:/ cond");
			    /*cond */ if ( /*_#__L14*/ meltfnum[13])	/*then */
			      {
				/*^cond.then */
				/*^block */
				/*anyblock */
				{


				  {
				    MELT_LOCATION
				      ("warmelt-outobj.melt:3377:/ locexp");
				    /*add2sbuf_strconst */
				      meltgc_add_strbuf ((melt_ptr_t)
							 ( /*_.IMPLBUF__V4*/
							  meltfptr[3]),
							 ("meltbp_aptr = (melt_ptr_t*) &"));
				  }
				  ;
	  /*_#GET_INT__L15*/ meltfnum[14] =
				    (melt_get_int
				     ((melt_ptr_t)
				      ( /*_.BOXDEPTHP1__V14*/
				       meltfptr[13])));;
				  MELT_LOCATION
				    ("warmelt-outobj.melt:3378:/ checksignal");
				  MELT_CHECK_SIGNAL ();
				  ;
				  /*^msend */
				  /*msend */
				  {
				    union meltparam_un argtab[3];
				    memset (&argtab, 0, sizeof (argtab));
				    /*^ojbmsend.arg */
				    argtab[0].meltbp_aptr =
				      (melt_ptr_t *) & /*_.DECLBUF__V3*/
				      meltfptr[2];
				    /*^ojbmsend.arg */
				    argtab[1].meltbp_aptr =
				      (melt_ptr_t *) & /*_.IMPLBUF__V4*/
				      meltfptr[3];
				    /*^ojbmsend.arg */
				    argtab[2].meltbp_long =
				      /*_#GET_INT__L15*/ meltfnum[14];
				    /*_.OUTPUT_C_CODE__V25*/ meltfptr[24] =
				      meltgc_send ((melt_ptr_t)
						   ( /*_.CURARG__V16*/
						    meltfptr[15]),
						   (melt_ptr_t) (( /*!OUTPUT_C_CODE */ meltfrout->tabval[8])), (MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_LONG ""), argtab, "", (union meltparam_un *) 0);
				  }
				  ;
				  MELT_LOCATION
				    ("warmelt-outobj.melt:3376:/ quasiblock");


				  /*_.PROGN___V26*/ meltfptr[25] =
				    /*_.OUTPUT_C_CODE__V25*/ meltfptr[24];;
				  /*^compute */
				  /*_.IFELSE___V24*/ meltfptr[23] =
				    /*_.PROGN___V26*/ meltfptr[25];;
				  /*epilog */

				  MELT_LOCATION
				    ("warmelt-outobj.melt:3376:/ clear");
		    /*clear *//*_#GET_INT__L15*/ meltfnum[14] =
				    0;
				  /*^clear */
		    /*clear *//*_.OUTPUT_C_CODE__V25*/
				    meltfptr[24] = 0;
				  /*^clear */
		    /*clear *//*_.PROGN___V26*/ meltfptr[25] =
				    0;
				}
				;
			      }
			    else
			      {	/*^cond.else */

				/*^block */
				/*anyblock */
				{


#if MELT_HAVE_DEBUG
				  MELT_LOCATION
				    ("warmelt-outobj.melt:3381:/ cppif.then");
				  /*^block */
				  /*anyblock */
				  {

				    /*^checksignal */
				    MELT_CHECK_SIGNAL ();
				    ;
	    /*_#IS_MULTIPLE__L16*/ meltfnum[14] =
				      (melt_magic_discr
				       ((melt_ptr_t)
					( /*_.CURARG__V16*/ meltfptr[15])) ==
				       MELTOBMAG_MULTIPLE);;
				    /*^compute */
	    /*_#NOT__L17*/ meltfnum[16] =
				      (!( /*_#IS_MULTIPLE__L16*/
					 meltfnum[14]));;
				    MELT_LOCATION
				      ("warmelt-outobj.melt:3381:/ cond");
				    /*cond */ if ( /*_#NOT__L17*/ meltfnum[16])	/*then */
				      {
					/*^cond.then */
					/*_.IFELSE___V28*/ meltfptr[25] =
					  ( /*nil */ NULL);;
				      }
				    else
				      {
					MELT_LOCATION
					  ("warmelt-outobj.melt:3381:/ cond.else");

					/*^block */
					/*anyblock */
					{




					  {
					    /*^locexp */
					    melt_assert_failed (("check curarg is not multiple"), ("warmelt-outobj.melt") ? ("warmelt-outobj.melt") : __FILE__, (3381) ? (3381) : __LINE__, __FUNCTION__);
					    ;
					  }
					  ;
			/*clear *//*_.IFELSE___V28*/
					    meltfptr[25] = 0;
					  /*epilog */
					}
					;
				      }
				    ;
				    /*^compute */
				    /*_.IFCPP___V27*/ meltfptr[24] =
				      /*_.IFELSE___V28*/ meltfptr[25];;
				    /*epilog */

				    MELT_LOCATION
				      ("warmelt-outobj.melt:3381:/ clear");
		      /*clear *//*_#IS_MULTIPLE__L16*/
				      meltfnum[14] = 0;
				    /*^clear */
		      /*clear *//*_#NOT__L17*/ meltfnum[16] = 0;
				    /*^clear */
		      /*clear *//*_.IFELSE___V28*/ meltfptr[25]
				      = 0;
				  }

#else /*MELT_HAVE_DEBUG */
				  /*^cppif.else */
				  /*_.IFCPP___V27*/ meltfptr[24] =
				    ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
				  ;
				  MELT_LOCATION
				    ("warmelt-outobj.melt:3382:/ getslot");
				  {
				    melt_ptr_t slot = NULL, obj = NULL;
				    obj =
				      (melt_ptr_t) ( /*_.CURCTYP__V17*/
						    meltfptr[16]) /*=obj*/ ;
				    melt_object_get_field (slot, obj, 6,
							   "CTYPE_ARGFIELD");
	   /*_.CTYPE_ARGFIELD__V29*/ meltfptr[25] =
				      slot;
				  };
				  ;

				  {
				    /*^locexp */
				    /*add2sbuf_string */
				      meltgc_add_strbuf ((melt_ptr_t)
							 ( /*_.IMPLBUF__V4*/
							  meltfptr[3]),
							 melt_string_str ((melt_ptr_t) ( /*_.CTYPE_ARGFIELD__V29*/ meltfptr[25])));
				  }
				  ;

				  {
				    MELT_LOCATION
				      ("warmelt-outobj.melt:3383:/ locexp");
				    /*add2sbuf_strconst */
				      meltgc_add_strbuf ((melt_ptr_t)
							 ( /*_.IMPLBUF__V4*/
							  meltfptr[3]),
							 (" = "));
				  }
				  ;
	  /*_#GET_INT__L18*/ meltfnum[14] =
				    (melt_get_int
				     ((melt_ptr_t)
				      ( /*_.BOXDEPTHP1__V14*/
				       meltfptr[13])));;
				  MELT_LOCATION
				    ("warmelt-outobj.melt:3384:/ checksignal");
				  MELT_CHECK_SIGNAL ();
				  ;
				  /*^msend */
				  /*msend */
				  {
				    union meltparam_un argtab[3];
				    memset (&argtab, 0, sizeof (argtab));
				    /*^ojbmsend.arg */
				    argtab[0].meltbp_aptr =
				      (melt_ptr_t *) & /*_.DECLBUF__V3*/
				      meltfptr[2];
				    /*^ojbmsend.arg */
				    argtab[1].meltbp_aptr =
				      (melt_ptr_t *) & /*_.IMPLBUF__V4*/
				      meltfptr[3];
				    /*^ojbmsend.arg */
				    argtab[2].meltbp_long =
				      /*_#GET_INT__L18*/ meltfnum[14];
				    /*_.OUTPUT_C_CODE__V30*/ meltfptr[29] =
				      meltgc_send ((melt_ptr_t)
						   ( /*_.CURARG__V16*/
						    meltfptr[15]),
						   (melt_ptr_t) (( /*!OUTPUT_C_CODE */ meltfrout->tabval[8])), (MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_LONG ""), argtab, "", (union meltparam_un *) 0);
				  }
				  ;
				  MELT_LOCATION
				    ("warmelt-outobj.melt:3380:/ quasiblock");


				  /*_.PROGN___V31*/ meltfptr[30] =
				    /*_.OUTPUT_C_CODE__V30*/ meltfptr[29];;
				  /*^compute */
				  /*_.IFELSE___V24*/ meltfptr[23] =
				    /*_.PROGN___V31*/ meltfptr[30];;
				  /*epilog */

				  MELT_LOCATION
				    ("warmelt-outobj.melt:3376:/ clear");
		    /*clear *//*_.IFCPP___V27*/ meltfptr[24] =
				    0;
				  /*^clear */
		    /*clear *//*_.CTYPE_ARGFIELD__V29*/
				    meltfptr[25] = 0;
				  /*^clear */
		    /*clear *//*_#GET_INT__L18*/ meltfnum[14] =
				    0;
				  /*^clear */
		    /*clear *//*_.OUTPUT_C_CODE__V30*/
				    meltfptr[29] = 0;
				  /*^clear */
		    /*clear *//*_.PROGN___V31*/ meltfptr[30] =
				    0;
				}
				;
			      }
			    ;
			    /*_.IFELSE___V23*/ meltfptr[22] =
			      /*_.IFELSE___V24*/ meltfptr[23];;
			    /*epilog */

			    MELT_LOCATION
			      ("warmelt-outobj.melt:3373:/ clear");
		  /*clear *//*_#__L14*/ meltfnum[13] = 0;
			    /*^clear */
		  /*clear *//*_.IFELSE___V24*/ meltfptr[23] = 0;
			  }
			  ;
			}
		      ;
		      /*_.IFELSE___V22*/ meltfptr[21] =
			/*_.IFELSE___V23*/ meltfptr[22];;
		      /*epilog */

		      MELT_LOCATION ("warmelt-outobj.melt:3370:/ clear");
		/*clear *//*_#IS_A__L13*/ meltfnum[12] = 0;
		      /*^clear */
		/*clear *//*_.IFELSE___V23*/ meltfptr[22] = 0;
		    }
		    ;
		  }
		;

		{
		  MELT_LOCATION ("warmelt-outobj.melt:3386:/ locexp");
		  /*add2sbuf_strconst */
		    meltgc_add_strbuf ((melt_ptr_t)
				       ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				       (";"));
		}
		;

#if MELT_HAVE_DEBUG
		MELT_LOCATION ("warmelt-outobj.melt:3387:/ cppif.then");
		/*^block */
		/*anyblock */
		{

		  /*^checksignal */
		  MELT_CHECK_SIGNAL ();
		  ;
      /*_#STRBUF_USEDLENGTH__L19*/ meltfnum[16] =
		    melt_strbuf_usedlength ((melt_ptr_t)
					    ( /*_.IMPLBUF__V4*/
					     meltfptr[3]));;
		  MELT_LOCATION ("warmelt-outobj.melt:3388:/ cond");
		  /*cond */ if (
				 /*ifisa */
				 melt_is_instance_of ((melt_ptr_t)
						      (( /*!BUFFER_LIMIT_CONT */ meltfrout->tabval[9])),
						      (melt_ptr_t) (( /*!CLASS_REFERENCE */ meltfrout->tabval[10])))
		    )		/*then */
		    {
		      /*^cond.then */
		      /*^getslot */
		      {
			melt_ptr_t slot = NULL, obj = NULL;
			obj =
			  (melt_ptr_t) (( /*!BUFFER_LIMIT_CONT */ meltfrout->
					 tabval[9])) /*=obj*/ ;
			melt_object_get_field (slot, obj, 0,
					       "REFERENCED_VALUE");
	/*_.REFERENCED_VALUE__V33*/ meltfptr[25] = slot;
		      };
		      ;
		    }
		  else
		    {		/*^cond.else */

       /*_.REFERENCED_VALUE__V33*/ meltfptr[25] = NULL;;
		    }
		  ;
		  /*^compute */
      /*_#GET_INT__L20*/ meltfnum[14] =
		    (melt_get_int
		     ((melt_ptr_t)
		      ( /*_.REFERENCED_VALUE__V33*/ meltfptr[25])));;
		  /*^compute */
      /*_#I__L21*/ meltfnum[13] =
		    (( /*_#STRBUF_USEDLENGTH__L19*/ meltfnum[16]) <
		     ( /*_#GET_INT__L20*/ meltfnum[14]));;
		  MELT_LOCATION ("warmelt-outobj.melt:3387:/ cond");
		  /*cond */ if ( /*_#I__L21*/ meltfnum[13])	/*then */
		    {
		      /*^cond.then */
		      /*_.IFELSE___V34*/ meltfptr[29] = ( /*nil */ NULL);;
		    }
		  else
		    {
		      MELT_LOCATION ("warmelt-outobj.melt:3387:/ cond.else");

		      /*^block */
		      /*anyblock */
		      {




			{
			  /*^locexp */
			  melt_assert_failed (("check limited implbuf"),
					      ("warmelt-outobj.melt")
					      ? ("warmelt-outobj.melt") :
					      __FILE__,
					      (3387) ? (3387) : __LINE__,
					      __FUNCTION__);
			  ;
			}
			;
		  /*clear *//*_.IFELSE___V34*/ meltfptr[29] = 0;
			/*epilog */
		      }
		      ;
		    }
		  ;
		  /*^compute */
		  /*_.IFCPP___V32*/ meltfptr[24] =
		    /*_.IFELSE___V34*/ meltfptr[29];;
		  /*epilog */

		  MELT_LOCATION ("warmelt-outobj.melt:3387:/ clear");
		/*clear *//*_#STRBUF_USEDLENGTH__L19*/ meltfnum[16] = 0;
		  /*^clear */
		/*clear *//*_.REFERENCED_VALUE__V33*/ meltfptr[25] = 0;
		  /*^clear */
		/*clear *//*_#GET_INT__L20*/ meltfnum[14] = 0;
		  /*^clear */
		/*clear *//*_#I__L21*/ meltfnum[13] = 0;
		  /*^clear */
		/*clear *//*_.IFELSE___V34*/ meltfptr[29] = 0;
		}

#else /*MELT_HAVE_DEBUG */
		/*^cppif.else */
		/*_.IFCPP___V32*/ meltfptr[24] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
		;
		/*^compute */
    /*_#GET_INT__L22*/ meltfnum[12] =
		  (melt_get_int
		   ((melt_ptr_t) ( /*_.BOXDEPTHP1__V14*/ meltfptr[13])));;

		{
		  MELT_LOCATION ("warmelt-outobj.melt:3389:/ locexp");
		  meltgc_strbuf_add_indent ((melt_ptr_t)
					    ( /*_.IMPLBUF__V4*/ meltfptr[3]),
					    ( /*_#GET_INT__L22*/
					     meltfnum[12]), 0);
		}
		;

		MELT_LOCATION ("warmelt-outobj.melt:3363:/ clear");
	      /*clear *//*_.CURCTYP__V17*/ meltfptr[16] = 0;
		/*^clear */
	      /*clear *//*_.IFCPP___V18*/ meltfptr[17] = 0;
		/*^clear */
	      /*clear *//*_#GET_INT__L11*/ meltfnum[9] = 0;
		/*^clear */
	      /*clear *//*_.OUTPUT_LOCATION__V20*/ meltfptr[18] = 0;
		/*^clear */
	      /*clear *//*_.CTYPE_PARSTRING__V21*/ meltfptr[20] = 0;
		/*^clear */
	      /*clear *//*_#NULL__L12*/ meltfnum[11] = 0;
		/*^clear */
	      /*clear *//*_.IFELSE___V22*/ meltfptr[21] = 0;
		/*^clear */
	      /*clear *//*_.IFCPP___V32*/ meltfptr[24] = 0;
		/*^clear */
	      /*clear *//*_#GET_INT__L22*/ meltfnum[12] = 0;
		if ( /*_#CURANK__L9*/ meltfnum[8] < 0)
		  break;
	      }			/* end  foreach_in_multiple meltcit1__EACHTUP */

	    /*citerepilog */

	    MELT_LOCATION ("warmelt-outobj.melt:3360:/ clear");
	      /*clear *//*_.CURARG__V16*/ meltfptr[15] = 0;
	    /*^clear */
	      /*clear *//*_#CURANK__L9*/ meltfnum[8] = 0;
	  }			/*endciterblock FOREACH_IN_MULTIPLE */
	  ;
	  MELT_LOCATION ("warmelt-outobj.melt:3352:/ quasiblock");


	  /*epilog */

	  MELT_LOCATION ("warmelt-outobj.melt:3348:/ clear");
	     /*clear *//*_#I__L7*/ meltfnum[6] = 0;
	  /*^clear */
	     /*clear *//*_#I__L8*/ meltfnum[7] = 0;
	}
	;
      }				/*noelse */
    ;
    /*citerblock FOREACH_IN_LIST */
    {
      /* start foreach_in_list meltcit2__EACHLIST */
      for ( /*_.CURPAIR__V35*/ meltfptr[30] =
	   melt_list_first ((melt_ptr_t) /*_.ODEST__V9*/ meltfptr[8]);
	   melt_magic_discr ((melt_ptr_t) /*_.CURPAIR__V35*/ meltfptr[30]) ==
	   MELTOBMAG_PAIR;
	   /*_.CURPAIR__V35*/ meltfptr[30] =
	   melt_pair_tail ((melt_ptr_t) /*_.CURPAIR__V35*/ meltfptr[30]))
	{
	  /*_.CURDEST__V36*/ meltfptr[23] =
	    melt_pair_head ((melt_ptr_t) /*_.CURPAIR__V35*/ meltfptr[30]);


  /*_#GET_INT__L23*/ meltfnum[16] =
	    (melt_get_int
	     ((melt_ptr_t) ( /*_.BOXDEPTHP1__V14*/ meltfptr[13])));;
	  MELT_LOCATION ("warmelt-outobj.melt:3396:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^msend */
	  /*msend */
	  {
	    union meltparam_un argtab[3];
	    memset (&argtab, 0, sizeof (argtab));
	    /*^ojbmsend.arg */
	    argtab[0].meltbp_aptr =
	      (melt_ptr_t *) & /*_.DECLBUF__V3*/ meltfptr[2];
	    /*^ojbmsend.arg */
	    argtab[1].meltbp_aptr =
	      (melt_ptr_t *) & /*_.IMPLBUF__V4*/ meltfptr[3];
	    /*^ojbmsend.arg */
	    argtab[2].meltbp_long = /*_#GET_INT__L23*/ meltfnum[16];
	    /*_.OUTPUT_C_CODE__V37*/ meltfptr[22] =
	      meltgc_send ((melt_ptr_t) ( /*_.CURDEST__V36*/ meltfptr[23]),
			   (melt_ptr_t) (( /*!OUTPUT_C_CODE */ meltfrout->
					  tabval[8])),
			   (MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_LONG
			    ""), argtab, "", (union meltparam_un *) 0);
	  }
	  ;

#if MELT_HAVE_DEBUG
	  MELT_LOCATION ("warmelt-outobj.melt:3397:/ cppif.then");
	  /*^block */
	  /*anyblock */
	  {

	    /*^checksignal */
	    MELT_CHECK_SIGNAL ();
	    ;
    /*_#STRBUF_USEDLENGTH__L24*/ meltfnum[14] =
	      melt_strbuf_usedlength ((melt_ptr_t)
				      ( /*_.IMPLBUF__V4*/ meltfptr[3]));;
	    MELT_LOCATION ("warmelt-outobj.melt:3398:/ cond");
	    /*cond */ if (
			   /*ifisa */
			   melt_is_instance_of ((melt_ptr_t)
						(( /*!BUFFER_LIMIT_CONT */
						  meltfrout->tabval[9])),
						(melt_ptr_t) (( /*!CLASS_REFERENCE */ meltfrout->tabval[10])))
	      )			/*then */
	      {
		/*^cond.then */
		/*^getslot */
		{
		  melt_ptr_t slot = NULL, obj = NULL;
		  obj =
		    (melt_ptr_t) (( /*!BUFFER_LIMIT_CONT */ meltfrout->
				   tabval[9])) /*=obj*/ ;
		  melt_object_get_field (slot, obj, 0, "REFERENCED_VALUE");
      /*_.REFERENCED_VALUE__V39*/ meltfptr[29] = slot;
		};
		;
	      }
	    else
	      {			/*^cond.else */

     /*_.REFERENCED_VALUE__V39*/ meltfptr[29] = NULL;;
	      }
	    ;
	    /*^compute */
    /*_#GET_INT__L25*/ meltfnum[13] =
	      (melt_get_int
	       ((melt_ptr_t) ( /*_.REFERENCED_VALUE__V39*/ meltfptr[29])));;
	    /*^compute */
    /*_#I__L26*/ meltfnum[9] =
	      (( /*_#STRBUF_USEDLENGTH__L24*/ meltfnum[14]) <
	       ( /*_#GET_INT__L25*/ meltfnum[13]));;
	    MELT_LOCATION ("warmelt-outobj.melt:3397:/ cond");
	    /*cond */ if ( /*_#I__L26*/ meltfnum[9])	/*then */
	      {
		/*^cond.then */
		/*_.IFELSE___V40*/ meltfptr[16] = ( /*nil */ NULL);;
	      }
	    else
	      {
		MELT_LOCATION ("warmelt-outobj.melt:3397:/ cond.else");

		/*^block */
		/*anyblock */
		{




		  {
		    /*^locexp */
		    melt_assert_failed (("check limited implbuf"),
					("warmelt-outobj.melt")
					? ("warmelt-outobj.melt") : __FILE__,
					(3397) ? (3397) : __LINE__,
					__FUNCTION__);
		    ;
		  }
		  ;
		/*clear *//*_.IFELSE___V40*/ meltfptr[16] = 0;
		  /*epilog */
		}
		;
	      }
	    ;
	    /*^compute */
	    /*_.IFCPP___V38*/ meltfptr[25] = /*_.IFELSE___V40*/ meltfptr[16];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-outobj.melt:3397:/ clear");
	      /*clear *//*_#STRBUF_USEDLENGTH__L24*/ meltfnum[14] = 0;
	    /*^clear */
	      /*clear *//*_.REFERENCED_VALUE__V39*/ meltfptr[29] = 0;
	    /*^clear */
	      /*clear *//*_#GET_INT__L25*/ meltfnum[13] = 0;
	    /*^clear */
	      /*clear *//*_#I__L26*/ meltfnum[9] = 0;
	    /*^clear */
	      /*clear *//*_.IFELSE___V40*/ meltfptr[16] = 0;
	  }

#else /*MELT_HAVE_DEBUG */
	  /*^cppif.else */
	  /*_.IFCPP___V38*/ meltfptr[25] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:3399:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t)
				 ( /*_.IMPLBUF__V4*/ meltfptr[3]), (" = "));
	  }
	  ;
	}			/* end foreach_in_list meltcit2__EACHLIST */
     /*_.CURPAIR__V35*/ meltfptr[30] = NULL;
     /*_.CURDEST__V36*/ meltfptr[23] = NULL;


      /*citerepilog */

      MELT_LOCATION ("warmelt-outobj.melt:3393:/ clear");
	    /*clear *//*_.CURPAIR__V35*/ meltfptr[30] = 0;
      /*^clear */
	    /*clear *//*_.CURDEST__V36*/ meltfptr[23] = 0;
      /*^clear */
	    /*clear *//*_#GET_INT__L23*/ meltfnum[16] = 0;
      /*^clear */
	    /*clear *//*_.OUTPUT_C_CODE__V37*/ meltfptr[22] = 0;
      /*^clear */
	    /*clear *//*_.IFCPP___V38*/ meltfptr[25] = 0;
    }				/*endciterblock FOREACH_IN_LIST */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:3401:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_OBJECT__L27*/ meltfnum[11] =
	(melt_magic_discr ((melt_ptr_t) ( /*_.ORECV__V11*/ meltfptr[10])) ==
	 MELTOBMAG_OBJECT);;
      MELT_LOCATION ("warmelt-outobj.melt:3401:/ cond");
      /*cond */ if ( /*_#IS_OBJECT__L27*/ meltfnum[11])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V42*/ meltfptr[18] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-outobj.melt:3401:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check orecv object"),
				  ("warmelt-outobj.melt")
				  ? ("warmelt-outobj.melt") : __FILE__,
				  (3401) ? (3401) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V42*/ meltfptr[18] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V41*/ meltfptr[17] = /*_.IFELSE___V42*/ meltfptr[18];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:3401:/ clear");
	     /*clear *//*_#IS_OBJECT__L27*/ meltfnum[11] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V42*/ meltfptr[18] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V41*/ meltfptr[17] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:3403:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   ("meltgc_send((melt_ptr_t)("));
    }
    ;
 /*_#I__L28*/ meltfnum[12] =
      (( /*_#DEPTH__L1*/ meltfnum[0]) + (1));;
    MELT_LOCATION ("warmelt-outobj.melt:3404:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^msend */
    /*msend */
    {
      union meltparam_un argtab[3];
      memset (&argtab, 0, sizeof (argtab));
      /*^ojbmsend.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.DECLBUF__V3*/ meltfptr[2];
      /*^ojbmsend.arg */
      argtab[1].meltbp_aptr = (melt_ptr_t *) & /*_.IMPLBUF__V4*/ meltfptr[3];
      /*^ojbmsend.arg */
      argtab[2].meltbp_long = /*_#I__L28*/ meltfnum[12];
      /*_.OUTPUT_C_CODE__V43*/ meltfptr[20] =
	meltgc_send ((melt_ptr_t) ( /*_.ORECV__V11*/ meltfptr[10]),
		     (melt_ptr_t) (( /*!OUTPUT_C_CODE */ meltfrout->
				    tabval[8])),
		     (MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_LONG ""),
		     argtab, "", (union meltparam_un *) 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:3405:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   ("), (melt_ptr_t)("));
    }
    ;
 /*_#I__L29*/ meltfnum[6] =
      (( /*_#DEPTH__L1*/ meltfnum[0]) + (1));;
    MELT_LOCATION ("warmelt-outobj.melt:3406:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^msend */
    /*msend */
    {
      union meltparam_un argtab[3];
      memset (&argtab, 0, sizeof (argtab));
      /*^ojbmsend.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.DECLBUF__V3*/ meltfptr[2];
      /*^ojbmsend.arg */
      argtab[1].meltbp_aptr = (melt_ptr_t *) & /*_.IMPLBUF__V4*/ meltfptr[3];
      /*^ojbmsend.arg */
      argtab[2].meltbp_long = /*_#I__L29*/ meltfnum[6];
      /*_.OUTPUT_C_CODE__V44*/ meltfptr[21] =
	meltgc_send ((melt_ptr_t) ( /*_.OSEL__V10*/ meltfptr[9]),
		     (melt_ptr_t) (( /*!OUTPUT_C_CODE */ meltfrout->
				    tabval[8])),
		     (MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_LONG ""),
		     argtab, "", (union meltparam_un *) 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:3407:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   ("), ("));
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:3411:/ quasiblock");


    /*^newclosure */
		 /*newclosure *//*_.LAMBDA___V46*/ meltfptr[29] =
      (melt_ptr_t)
      meltgc_new_closure ((meltobject_ptr_t)
			  (((melt_ptr_t) (MELT_PREDEF (DISCR_CLOSURE)))),
			  (meltroutine_ptr_t) (( /*!konst_12 */ meltfrout->
						tabval[12])), (1));
    ;
    /*^putclosedv */
    /*putclosv */
    melt_assertmsg ("putclosv checkclo",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.LAMBDA___V46*/ meltfptr[29])) ==
		    MELTOBMAG_CLOSURE);
    melt_assertmsg ("putclosv checkoff", 0 >= 0
		    && 0 <
		    melt_closure_size ((melt_ptr_t)
				       ( /*_.LAMBDA___V46*/ meltfptr[29])));
    ((meltclosure_ptr_t) /*_.LAMBDA___V46*/ meltfptr[29])->tabval[0] =
      (melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]);
    ;
    /*_.LAMBDA___V45*/ meltfptr[24] = /*_.LAMBDA___V46*/ meltfptr[29];;
    MELT_LOCATION ("warmelt-outobj.melt:3409:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & /*_.LAMBDA___V45*/ meltfptr[24];
      /*_.LIST_EVERY__V47*/ meltfptr[16] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!LIST_EVERY */ meltfrout->tabval[11])),
		    (melt_ptr_t) ( /*_.PARAMDESCLIST__V13*/ meltfptr[12]),
		    (MELTBPARSTR_PTR ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:3414:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   ("\"\"), "));
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:3415:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#I__L30*/ meltfnum[7] =
      (( /*_#NBARG__L3*/ meltfnum[1]) > (0));;
    MELT_LOCATION ("warmelt-outobj.melt:3415:/ cond");
    /*cond */ if ( /*_#I__L30*/ meltfnum[7])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{


	  {
	    MELT_LOCATION ("warmelt-outobj.melt:3416:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t)
				 ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				 ("argtab,"));
	  }
	  ;
	  /*epilog */
	}
	;
      }
    else
      {
	MELT_LOCATION ("warmelt-outobj.melt:3415:/ cond.else");

	/*^block */
	/*anyblock */
	{


	  {
	    MELT_LOCATION ("warmelt-outobj.melt:3417:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t)
				 ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				 ("(union meltparam_un*)0,"));
	  }
	  ;
	  /*epilog */
	}
	;
      }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:3419:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   (" \"\", (union meltparam_un*)0"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:3420:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   (");"));
    }
    ;
 /*_#I__L31*/ meltfnum[14] =
      ((1) + ( /*_#DEPTH__L1*/ meltfnum[0]));;

    {
      MELT_LOCATION ("warmelt-outobj.melt:3421:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				( /*_#I__L31*/ meltfnum[14]), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:3422:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   ("}"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:3423:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				( /*_#DEPTH__L1*/ meltfnum[0]), 0);
    }
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:3424:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#STRBUF_USEDLENGTH__L32*/ meltfnum[13] =
	melt_strbuf_usedlength ((melt_ptr_t)
				( /*_.IMPLBUF__V4*/ meltfptr[3]));;
      MELT_LOCATION ("warmelt-outobj.melt:3425:/ cond");
      /*cond */ if (
		     /*ifisa */
		     melt_is_instance_of ((melt_ptr_t)
					  (( /*!BUFFER_LIMIT_CONT */
					    meltfrout->tabval[9])),
					  (melt_ptr_t) (( /*!CLASS_REFERENCE */ meltfrout->tabval[10])))
	)			/*then */
	{
	  /*^cond.then */
	  /*^getslot */
	  {
	    melt_ptr_t slot = NULL, obj = NULL;
	    obj =
	      (melt_ptr_t) (( /*!BUFFER_LIMIT_CONT */ meltfrout->
			     tabval[9])) /*=obj*/ ;
	    melt_object_get_field (slot, obj, 0, "REFERENCED_VALUE");
     /*_.REFERENCED_VALUE__V49*/ meltfptr[48] = slot;
	  };
	  ;
	}
      else
	{			/*^cond.else */

    /*_.REFERENCED_VALUE__V49*/ meltfptr[48] = NULL;;
	}
      ;
      /*^compute */
   /*_#GET_INT__L33*/ meltfnum[9] =
	(melt_get_int
	 ((melt_ptr_t) ( /*_.REFERENCED_VALUE__V49*/ meltfptr[48])));;
      /*^compute */
   /*_#I__L34*/ meltfnum[11] =
	(( /*_#STRBUF_USEDLENGTH__L32*/ meltfnum[13]) <
	 ( /*_#GET_INT__L33*/ meltfnum[9]));;
      MELT_LOCATION ("warmelt-outobj.melt:3424:/ cond");
      /*cond */ if ( /*_#I__L34*/ meltfnum[11])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V50*/ meltfptr[49] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-outobj.melt:3424:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check limited implbuf"),
				  ("warmelt-outobj.melt")
				  ? ("warmelt-outobj.melt") : __FILE__,
				  (3424) ? (3424) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V50*/ meltfptr[49] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V48*/ meltfptr[18] = /*_.IFELSE___V50*/ meltfptr[49];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:3424:/ clear");
	     /*clear *//*_#STRBUF_USEDLENGTH__L32*/ meltfnum[13] = 0;
      /*^clear */
	     /*clear *//*_.REFERENCED_VALUE__V49*/ meltfptr[48] = 0;
      /*^clear */
	     /*clear *//*_#GET_INT__L33*/ meltfnum[9] = 0;
      /*^clear */
	     /*clear *//*_#I__L34*/ meltfnum[11] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V50*/ meltfptr[49] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V48*/ meltfptr[18] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    /*^compute */
    /*_.LET___V7*/ meltfptr[5] = /*_.IFCPP___V48*/ meltfptr[18];;

    MELT_LOCATION ("warmelt-outobj.melt:3336:/ clear");
	   /*clear *//*_.OLOC__V8*/ meltfptr[7] = 0;
    /*^clear */
	   /*clear *//*_.ODEST__V9*/ meltfptr[8] = 0;
    /*^clear */
	   /*clear *//*_.OSEL__V10*/ meltfptr[9] = 0;
    /*^clear */
	   /*clear *//*_.ORECV__V11*/ meltfptr[10] = 0;
    /*^clear */
	   /*clear *//*_.OARGS__V12*/ meltfptr[11] = 0;
    /*^clear */
	   /*clear *//*_#NBARG__L3*/ meltfnum[1] = 0;
    /*^clear */
	   /*clear *//*_.PARAMDESCLIST__V13*/ meltfptr[12] = 0;
    /*^clear */
	   /*clear *//*_#I__L4*/ meltfnum[3] = 0;
    /*^clear */
	   /*clear *//*_.BOXDEPTHP1__V14*/ meltfptr[13] = 0;
    /*^clear */
	   /*clear *//*_.OUTPUT_LOCATION__V15*/ meltfptr[14] = 0;
    /*^clear */
	   /*clear *//*_#I__L5*/ meltfnum[4] = 0;
    /*^clear */
	   /*clear *//*_#I__L6*/ meltfnum[5] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V41*/ meltfptr[17] = 0;
    /*^clear */
	   /*clear *//*_#I__L28*/ meltfnum[12] = 0;
    /*^clear */
	   /*clear *//*_.OUTPUT_C_CODE__V43*/ meltfptr[20] = 0;
    /*^clear */
	   /*clear *//*_#I__L29*/ meltfnum[6] = 0;
    /*^clear */
	   /*clear *//*_.OUTPUT_C_CODE__V44*/ meltfptr[21] = 0;
    /*^clear */
	   /*clear *//*_.LAMBDA___V45*/ meltfptr[24] = 0;
    /*^clear */
	   /*clear *//*_.LIST_EVERY__V47*/ meltfptr[16] = 0;
    /*^clear */
	   /*clear *//*_#I__L30*/ meltfnum[7] = 0;
    /*^clear */
	   /*clear *//*_#I__L31*/ meltfnum[14] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V48*/ meltfptr[18] = 0;
    MELT_LOCATION ("warmelt-outobj.melt:3334:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.LET___V7*/ meltfptr[5];;

    {
      MELT_LOCATION ("warmelt-outobj.melt:3334:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V5*/ meltfptr[4] = 0;
    /*^clear */
	   /*clear *//*_.LET___V7*/ meltfptr[5] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("OUTPUCOD_OBJMSEND", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_81_warmelt_outobj_OUTPUCOD_OBJMSEND_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_81_warmelt_outobj_OUTPUCOD_OBJMSEND */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_82_warmelt_outobj_LAMBDA___13__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un * meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un * meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_82_warmelt_outobj_LAMBDA___13___melt =
    melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_82_warmelt_outobj_LAMBDA___13___st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 2
    melt_ptr_t mcfr_varptr[2];
/*no varnum*/
#define MELTFRAM_NBVARNUM /*none*/0
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_82_warmelt_outobj_LAMBDA___13__ is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_82_warmelt_outobj_LAMBDA___13___st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 2; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_82_warmelt_outobj_LAMBDA___13__ nbval 2*/
  meltfram__.mcfr_nbvar = 2 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("LAMBDA_", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-outobj.melt:3411:/ getarg");
 /*_.PARD__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


    {
      MELT_LOCATION ("warmelt-outobj.melt:3412:/ locexp");
      /*add2sbuf_string */
	meltgc_add_strbuf ((melt_ptr_t)
			   (( /*~IMPLBUF */ meltfclos->tabval[0])),
			   melt_string_str ((melt_ptr_t)
					    ( /*_.PARD__V2*/ meltfptr[1])));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:3413:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t)
			   (( /*~IMPLBUF */ meltfclos->tabval[0])), (" "));
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:3411:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*epilog */
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("LAMBDA_", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_82_warmelt_outobj_LAMBDA___13___melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*noretval */ NULL);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_82_warmelt_outobj_LAMBDA___13__ */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_83_warmelt_outobj_OUTPUCOD_OBJMULTIAPPLY (meltclosure_ptr_t
						   meltclosp_,
						   melt_ptr_t meltfirstargp_,
						   const melt_argdescr_cell_t
						   meltxargdescr_[],
						   union meltparam_un *
						   meltxargtab_,
						   const melt_argdescr_cell_t
						   meltxresdescr_[],
						   union meltparam_un *
						   meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_83_warmelt_outobj_OUTPUCOD_OBJMULTIAPPLY_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_83_warmelt_outobj_OUTPUCOD_OBJMULTIAPPLY_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 63
    melt_ptr_t mcfr_varptr[63];
#define MELTFRAM_NBVARNUM 41
    long mcfr_varnum[41];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_83_warmelt_outobj_OUTPUCOD_OBJMULTIAPPLY is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_83_warmelt_outobj_OUTPUCOD_OBJMULTIAPPLY_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 63; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_83_warmelt_outobj_OUTPUCOD_OBJMULTIAPPLY nbval 63*/
  meltfram__.mcfr_nbvar = 63 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("OUTPUCOD_OBJMULTIAPPLY", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-outobj.melt:3432:/ getarg");
 /*_.OAPP__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.DECLBUF__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.DECLBUF__V3*/ meltfptr[2])) !=
	      NULL);


  /*getarg#2 */
  /*^getarg */
  if (meltxargdescr_[1] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.IMPLBUF__V4*/ meltfptr[3] =
    (meltxargtab_[1].meltbp_aptr) ? (*(meltxargtab_[1].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3])) !=
	      NULL);


  /*getarg#3 */
  /*^getarg */
  if (meltxargdescr_[2] != MELTBPAR_LONG)
    goto lab_endgetargs;
 /*_#DEPTH__L1*/ meltfnum[0] = meltxargtab_[2].meltbp_long;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:3433:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L2*/ meltfnum[1] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.OAPP__V2*/ meltfptr[1]),
			     (melt_ptr_t) (( /*!CLASS_OBJMULTIAPPLY */
					    meltfrout->tabval[0])));;
      MELT_LOCATION ("warmelt-outobj.melt:3433:/ cond");
      /*cond */ if ( /*_#IS_A__L2*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V6*/ meltfptr[5] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-outobj.melt:3433:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check oapp"),
				  ("warmelt-outobj.melt")
				  ? ("warmelt-outobj.melt") : __FILE__,
				  (3433) ? (3433) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V6*/ meltfptr[5] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V5*/ meltfptr[4] = /*_.IFELSE___V6*/ meltfptr[5];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:3433:/ clear");
	     /*clear *//*_#IS_A__L2*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V6*/ meltfptr[5] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V5*/ meltfptr[4] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-outobj.melt:3434:/ quasiblock");


    MELT_LOCATION ("warmelt-outobj.melt:3435:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.OAPP__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 0, "OBI_LOC");
  /*_.ALOC__V8*/ meltfptr[7] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-outobj.melt:3436:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.OAPP__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 1, "OBDI_DESTLIST");
  /*_.ADEST__V9*/ meltfptr[8] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-outobj.melt:3437:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.OAPP__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 2, "OBAPP_CLOS");
  /*_.OCLOS__V10*/ meltfptr[9] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-outobj.melt:3438:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.OAPP__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 3, "OBAPP_ARGS");
  /*_.OARGS__V11*/ meltfptr[10] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-outobj.melt:3439:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.OAPP__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 4, "OBMULTAPP_XRES");
  /*_.OXRES__V12*/ meltfptr[11] = slot;
    };
    ;
 /*_#NBARG__L3*/ meltfnum[1] =
      (melt_multiple_length ((melt_ptr_t) ( /*_.OARGS__V11*/ meltfptr[10])));;
    /*^compute */
 /*_#NBXRES__L4*/ meltfnum[3] =
      (melt_multiple_length ((melt_ptr_t) ( /*_.OXRES__V12*/ meltfptr[11])));;
    /*^compute */
 /*_.PARAMDESCLIST__V13*/ meltfptr[12] =
      (meltgc_new_list
       ((meltobject_ptr_t) (( /*!DISCR_LIST */ meltfrout->tabval[1]))));;
    /*^compute */
 /*_.RESDESCLIST__V14*/ meltfptr[13] =
      (meltgc_new_list
       ((meltobject_ptr_t) (( /*!DISCR_LIST */ meltfrout->tabval[1]))));;
    /*^compute */
 /*_#I__L5*/ meltfnum[4] =
      ((1) + ( /*_#DEPTH__L1*/ meltfnum[0]));;
    /*^compute */
 /*_.BOXDEPTHP1__V15*/ meltfptr[14] =
      (meltgc_new_int
       ((meltobject_ptr_t) (( /*!DISCR_INTEGER */ meltfrout->tabval[2])),
	( /*_#I__L5*/ meltfnum[4])));;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:3446:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_MULTIPLE_OR_NULL__L6*/ meltfnum[5] =
	(( /*_.OARGS__V11*/ meltfptr[10]) == NULL
	 ||
	 (melt_unsafe_magic_discr
	  ((melt_ptr_t) ( /*_.OARGS__V11*/ meltfptr[10])) ==
	  MELTOBMAG_MULTIPLE));;
      MELT_LOCATION ("warmelt-outobj.melt:3446:/ cond");
      /*cond */ if ( /*_#IS_MULTIPLE_OR_NULL__L6*/ meltfnum[5])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V17*/ meltfptr[16] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-outobj.melt:3446:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check oargs"),
				  ("warmelt-outobj.melt")
				  ? ("warmelt-outobj.melt") : __FILE__,
				  (3446) ? (3446) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V17*/ meltfptr[16] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V16*/ meltfptr[15] = /*_.IFELSE___V17*/ meltfptr[16];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:3446:/ clear");
	     /*clear *//*_#IS_MULTIPLE_OR_NULL__L6*/ meltfnum[5] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V17*/ meltfptr[16] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V16*/ meltfptr[15] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:3447:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_MULTIPLE_OR_NULL__L7*/ meltfnum[5] =
	(( /*_.OXRES__V12*/ meltfptr[11]) == NULL
	 ||
	 (melt_unsafe_magic_discr
	  ((melt_ptr_t) ( /*_.OXRES__V12*/ meltfptr[11])) ==
	  MELTOBMAG_MULTIPLE));;
      MELT_LOCATION ("warmelt-outobj.melt:3447:/ cond");
      /*cond */ if ( /*_#IS_MULTIPLE_OR_NULL__L7*/ meltfnum[5])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V19*/ meltfptr[18] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-outobj.melt:3447:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check oxres"),
				  ("warmelt-outobj.melt")
				  ? ("warmelt-outobj.melt") : __FILE__,
				  (3447) ? (3447) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V19*/ meltfptr[18] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V18*/ meltfptr[16] = /*_.IFELSE___V19*/ meltfptr[18];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:3447:/ clear");
	     /*clear *//*_#IS_MULTIPLE_OR_NULL__L7*/ meltfnum[5] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V19*/ meltfptr[18] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V18*/ meltfptr[16] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-outobj.melt:3448:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[3];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.IMPLBUF__V4*/ meltfptr[3];
      /*^apply.arg */
      argtab[1].meltbp_long = /*_#DEPTH__L1*/ meltfnum[0];
      /*^apply.arg */
      argtab[2].meltbp_cstring = "multiapply";
      /*_.OUTPUT_LOCATION__V20*/ meltfptr[18] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!OUTPUT_LOCATION */ meltfrout->tabval[3])),
		    (melt_ptr_t) ( /*_.ALOC__V8*/ meltfptr[7]),
		    (MELTBPARSTR_PTR MELTBPARSTR_LONG MELTBPARSTR_CSTRING ""),
		    argtab, "", (union meltparam_un *) 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:3449:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   ("/*multiapply "));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:3450:/ locexp");
      meltgc_add_strbuf_dec ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			     ( /*_#NBARG__L3*/ meltfnum[1]));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:3451:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   ("args, "));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:3452:/ locexp");
      meltgc_add_strbuf_dec ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			     ( /*_#NBXRES__L4*/ meltfnum[3]));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:3453:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   ("x.res*/ "));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:3454:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   ("{"));
    }
    ;
 /*_#I__L8*/ meltfnum[5] =
      ((1) + ( /*_#DEPTH__L1*/ meltfnum[0]));;

    {
      MELT_LOCATION ("warmelt-outobj.melt:3455:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				( /*_#I__L8*/ meltfnum[5]), 0);
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:3456:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#I__L9*/ meltfnum[8] =
      (( /*_#NBARG__L3*/ meltfnum[1]) > (1));;
    MELT_LOCATION ("warmelt-outobj.melt:3456:/ cond");
    /*cond */ if ( /*_#I__L9*/ meltfnum[8])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{


	  {
	    MELT_LOCATION ("warmelt-outobj.melt:3458:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t)
				 ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				 ("union meltparam_un argtab["));
	  }
	  ;
   /*_#I__L10*/ meltfnum[9] =
	    (( /*_#NBARG__L3*/ meltfnum[1]) - (1));;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:3459:/ locexp");
	    meltgc_add_strbuf_dec ((melt_ptr_t)
				   ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				   ( /*_#I__L10*/ meltfnum[9]));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:3460:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t)
				 ( /*_.IMPLBUF__V4*/ meltfptr[3]), ("];"));
	  }
	  ;
   /*_#I__L11*/ meltfnum[10] =
	    ((1) + ( /*_#DEPTH__L1*/ meltfnum[0]));;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:3461:/ locexp");
	    meltgc_strbuf_add_indent ((melt_ptr_t)
				      ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				      ( /*_#I__L11*/ meltfnum[10]), 0);
	  }
	  ;
	  MELT_LOCATION ("warmelt-outobj.melt:3457:/ quasiblock");


	  /*epilog */

	  MELT_LOCATION ("warmelt-outobj.melt:3456:/ clear");
	     /*clear *//*_#I__L10*/ meltfnum[9] = 0;
	  /*^clear */
	     /*clear *//*_#I__L11*/ meltfnum[10] = 0;
	}
	;
      }				/*noelse */
    ;
    MELT_LOCATION ("warmelt-outobj.melt:3463:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#I__L12*/ meltfnum[9] =
      (( /*_#NBXRES__L4*/ meltfnum[3]) > (0));;
    MELT_LOCATION ("warmelt-outobj.melt:3463:/ cond");
    /*cond */ if ( /*_#I__L12*/ meltfnum[9])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

   /*_#I__L13*/ meltfnum[10] =
	    ((1) + ( /*_#DEPTH__L1*/ meltfnum[0]));;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:3465:/ locexp");
	    meltgc_strbuf_add_indent ((melt_ptr_t)
				      ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				      ( /*_#I__L13*/ meltfnum[10]), 0);
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:3466:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t)
				 ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				 ("union meltparam_un restab["));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:3467:/ locexp");
	    meltgc_add_strbuf_dec ((melt_ptr_t)
				   ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				   ( /*_#NBXRES__L4*/ meltfnum[3]));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:3468:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t)
				 ( /*_.IMPLBUF__V4*/ meltfptr[3]), ("];"));
	  }
	  ;
   /*_#I__L14*/ meltfnum[13] =
	    ((1) + ( /*_#DEPTH__L1*/ meltfnum[0]));;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:3469:/ locexp");
	    meltgc_strbuf_add_indent ((melt_ptr_t)
				      ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				      ( /*_#I__L14*/ meltfnum[13]), 0);
	  }
	  ;
	  MELT_LOCATION ("warmelt-outobj.melt:3464:/ quasiblock");


	  /*epilog */

	  MELT_LOCATION ("warmelt-outobj.melt:3463:/ clear");
	     /*clear *//*_#I__L13*/ meltfnum[10] = 0;
	  /*^clear */
	     /*clear *//*_#I__L14*/ meltfnum[13] = 0;
	}
	;
      }				/*noelse */
    ;
    MELT_LOCATION ("warmelt-outobj.melt:3471:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#I__L15*/ meltfnum[10] =
      (( /*_#NBXRES__L4*/ meltfnum[3]) > (0));;
    MELT_LOCATION ("warmelt-outobj.melt:3471:/ cond");
    /*cond */ if ( /*_#I__L15*/ meltfnum[10])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{


	  {
	    MELT_LOCATION ("warmelt-outobj.melt:3473:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t)
				 ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				 ("memset(&restab, 0, sizeof(restab));"));
	  }
	  ;
   /*_#I__L16*/ meltfnum[13] =
	    ((1) + ( /*_#DEPTH__L1*/ meltfnum[0]));;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:3474:/ locexp");
	    meltgc_strbuf_add_indent ((melt_ptr_t)
				      ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				      ( /*_#I__L16*/ meltfnum[13]), 0);
	  }
	  ;
	  MELT_LOCATION ("warmelt-outobj.melt:3478:/ quasiblock");


	  /*^newclosure */
		   /*newclosure *//*_.LAMBDA___V23*/ meltfptr[22] =
	    (melt_ptr_t)
	    meltgc_new_closure ((meltobject_ptr_t)
				(((melt_ptr_t)
				  (MELT_PREDEF (DISCR_CLOSURE)))),
				(meltroutine_ptr_t) (( /*!konst_6 */
						      meltfrout->tabval[6])),
				(1));
	  ;
	  /*^putclosedv */
	  /*putclosv */
	  melt_assertmsg ("putclosv checkclo",
			  melt_magic_discr ((melt_ptr_t)
					    ( /*_.LAMBDA___V23*/
					     meltfptr[22])) ==
			  MELTOBMAG_CLOSURE);
	  melt_assertmsg ("putclosv checkoff", 0 >= 0
			  && 0 <
			  melt_closure_size ((melt_ptr_t)
					     ( /*_.LAMBDA___V23*/
					      meltfptr[22])));
	  ((meltclosure_ptr_t) /*_.LAMBDA___V23*/ meltfptr[22])->tabval[0] =
	    (melt_ptr_t) ( /*_.RESDESCLIST__V14*/ meltfptr[13]);
	  ;
	  /*_.LAMBDA___V22*/ meltfptr[21] = /*_.LAMBDA___V23*/ meltfptr[22];;
	  MELT_LOCATION ("warmelt-outobj.melt:3476:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^apply */
	  /*apply */
	  {
	    union meltparam_un argtab[1];
	    memset (&argtab, 0, sizeof (argtab));
	    /*^apply.arg */
	    argtab[0].meltbp_aptr =
	      (melt_ptr_t *) & /*_.LAMBDA___V22*/ meltfptr[21];
	    /*_.MULTIPLE_EVERY__V24*/ meltfptr[23] =
	      melt_apply ((meltclosure_ptr_t)
			  (( /*!MULTIPLE_EVERY */ meltfrout->tabval[4])),
			  (melt_ptr_t) ( /*_.OXRES__V12*/ meltfptr[11]),
			  (MELTBPARSTR_PTR ""), argtab, "",
			  (union meltparam_un *) 0);
	  }
	  ;
	  MELT_LOCATION ("warmelt-outobj.melt:3472:/ quasiblock");


	  /*_.PROGN___V25*/ meltfptr[24] =
	    /*_.MULTIPLE_EVERY__V24*/ meltfptr[23];;
	  /*^compute */
	  /*_.IF___V21*/ meltfptr[20] = /*_.PROGN___V25*/ meltfptr[24];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-outobj.melt:3471:/ clear");
	     /*clear *//*_#I__L16*/ meltfnum[13] = 0;
	  /*^clear */
	     /*clear *//*_.LAMBDA___V22*/ meltfptr[21] = 0;
	  /*^clear */
	     /*clear *//*_.MULTIPLE_EVERY__V24*/ meltfptr[23] = 0;
	  /*^clear */
	     /*clear *//*_.PROGN___V25*/ meltfptr[24] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

  /*_.IF___V21*/ meltfptr[20] = NULL;;
      }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:3481:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#I__L17*/ meltfnum[13] =
      (( /*_#NBARG__L3*/ meltfnum[1]) > (1));;
    MELT_LOCATION ("warmelt-outobj.melt:3481:/ cond");
    /*cond */ if ( /*_#I__L17*/ meltfnum[13])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{


	  {
	    MELT_LOCATION ("warmelt-outobj.melt:3483:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t)
				 ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				 ("memset(&argtab, 0, sizeof(argtab));"));
	  }
	  ;
   /*_#I__L18*/ meltfnum[17] =
	    ((1) + ( /*_#DEPTH__L1*/ meltfnum[0]));;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:3484:/ locexp");
	    meltgc_strbuf_add_indent ((melt_ptr_t)
				      ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				      ( /*_#I__L18*/ meltfnum[17]), 0);
	  }
	  ;
	  /*citerblock FOREACH_IN_MULTIPLE */
	  {
	    /* start foreach_in_multiple meltcit1__EACHTUP */
	    long meltcit1__EACHTUP_ln =
	      melt_multiple_length ((melt_ptr_t) /*_.OARGS__V11*/
				    meltfptr[10]);
	    for ( /*_#CURANK__L19*/ meltfnum[18] = 0;
		 ( /*_#CURANK__L19*/ meltfnum[18] >= 0)
		 && ( /*_#CURANK__L19*/ meltfnum[18] < meltcit1__EACHTUP_ln);
	/*_#CURANK__L19*/ meltfnum[18]++)
	      {
		/*_.CURARG__V26*/ meltfptr[21] =
		  melt_multiple_nth ((melt_ptr_t)
				     ( /*_.OARGS__V11*/ meltfptr[10]),
				     /*_#CURANK__L19*/ meltfnum[18]);



		MELT_LOCATION ("warmelt-outobj.melt:3489:/ checksignal");
		MELT_CHECK_SIGNAL ();
		;
    /*_#I__L20*/ meltfnum[19] =
		  (( /*_#CURANK__L19*/ meltfnum[18]) > (0));;
		MELT_LOCATION ("warmelt-outobj.melt:3489:/ cond");
		/*cond */ if ( /*_#I__L20*/ meltfnum[19])	/*then */
		  {
		    /*^cond.then */
		    /*^block */
		    /*anyblock */
		    {

		      MELT_LOCATION ("warmelt-outobj.melt:3490:/ quasiblock");


		      /*^checksignal */
		      MELT_CHECK_SIGNAL ();
		      ;
		      /*^msend */
		      /*msend */
		      {
			union meltparam_un argtab[1];
			memset (&argtab, 0, sizeof (argtab));
			/*^ojbmsend.arg */
			argtab[0].meltbp_aptr = /*nil */ (melt_ptr_t *) NULL;
			/*_.CURCTYP__V27*/ meltfptr[23] =
			  meltgc_send ((melt_ptr_t)
				       ( /*_.CURARG__V26*/ meltfptr[21]),
				       (melt_ptr_t) (( /*!GET_CTYPE */
						      meltfrout->tabval[7])),
				       (MELTBPARSTR_PTR ""), argtab, "",
				       (union meltparam_un *) 0);
		      }
		      ;

#if MELT_HAVE_DEBUG
		      MELT_LOCATION ("warmelt-outobj.melt:3491:/ cppif.then");
		      /*^block */
		      /*anyblock */
		      {

			/*^checksignal */
			MELT_CHECK_SIGNAL ();
			;
	/*_#IS_A__L21*/ meltfnum[20] =
			  melt_is_instance_of ((melt_ptr_t)
					       ( /*_.CURCTYP__V27*/
						meltfptr[23]),
					       (melt_ptr_t) (( /*!CLASS_CTYPE */ meltfrout->tabval[8])));;
			MELT_LOCATION ("warmelt-outobj.melt:3491:/ cond");
			/*cond */ if ( /*_#IS_A__L21*/ meltfnum[20])	/*then */
			  {
			    /*^cond.then */
			    /*_.IFELSE___V29*/ meltfptr[28] =
			      ( /*nil */ NULL);;
			  }
			else
			  {
			    MELT_LOCATION
			      ("warmelt-outobj.melt:3491:/ cond.else");

			    /*^block */
			    /*anyblock */
			    {




			      {
				/*^locexp */
				melt_assert_failed (("check curctyp"),
						    ("warmelt-outobj.melt")
						    ? ("warmelt-outobj.melt")
						    : __FILE__,
						    (3491) ? (3491) :
						    __LINE__, __FUNCTION__);
				;
			      }
			      ;
		    /*clear *//*_.IFELSE___V29*/ meltfptr[28] = 0;
			      /*epilog */
			    }
			    ;
			  }
			;
			/*^compute */
			/*_.IFCPP___V28*/ meltfptr[24] =
			  /*_.IFELSE___V29*/ meltfptr[28];;
			/*epilog */

			MELT_LOCATION ("warmelt-outobj.melt:3491:/ clear");
		  /*clear *//*_#IS_A__L21*/ meltfnum[20] = 0;
			/*^clear */
		  /*clear *//*_.IFELSE___V29*/ meltfptr[28] = 0;
		      }

#else /*MELT_HAVE_DEBUG */
		      /*^cppif.else */
		      /*_.IFCPP___V28*/ meltfptr[24] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
		      ;
		      /*^compute */
      /*_#GET_INT__L22*/ meltfnum[20] =
			(melt_get_int
			 ((melt_ptr_t)
			  ( /*_.BOXDEPTHP1__V15*/ meltfptr[14])));;
		      MELT_LOCATION
			("warmelt-outobj.melt:3492:/ checksignal");
		      MELT_CHECK_SIGNAL ();
		      ;
		      /*^apply */
		      /*apply */
		      {
			union meltparam_un argtab[3];
			memset (&argtab, 0, sizeof (argtab));
			/*^apply.arg */
			argtab[0].meltbp_aptr =
			  (melt_ptr_t *) & /*_.IMPLBUF__V4*/ meltfptr[3];
			/*^apply.arg */
			argtab[1].meltbp_long =
			  /*_#GET_INT__L22*/ meltfnum[20];
			/*^apply.arg */
			argtab[2].meltbp_cstring = "multiapply.arg";
			/*_.OUTPUT_LOCATION__V30*/ meltfptr[28] =
			  melt_apply ((meltclosure_ptr_t)
				      (( /*!OUTPUT_LOCATION */ meltfrout->
					tabval[3])),
				      (melt_ptr_t) ( /*_.ALOC__V8*/
						    meltfptr[7]),
				      (MELTBPARSTR_PTR MELTBPARSTR_LONG
				       MELTBPARSTR_CSTRING ""), argtab, "",
				      (union meltparam_un *) 0);
		      }
		      ;

		      {
			MELT_LOCATION ("warmelt-outobj.melt:3493:/ locexp");
			/*add2sbuf_strconst */
			  meltgc_add_strbuf ((melt_ptr_t)
					     ( /*_.IMPLBUF__V4*/ meltfptr[3]),
					     ("argtab["));
		      }
		      ;
      /*_#I__L23*/ meltfnum[22] =
			(( /*_#CURANK__L19*/ meltfnum[18]) - (1));;

		      {
			MELT_LOCATION ("warmelt-outobj.melt:3494:/ locexp");
			meltgc_add_strbuf_dec ((melt_ptr_t)
					       ( /*_.IMPLBUF__V4*/
						meltfptr[3]),
					       ( /*_#I__L23*/ meltfnum[22]));
		      }
		      ;

		      {
			MELT_LOCATION ("warmelt-outobj.melt:3495:/ locexp");
			/*add2sbuf_strconst */
			  meltgc_add_strbuf ((melt_ptr_t)
					     ( /*_.IMPLBUF__V4*/ meltfptr[3]),
					     ("]."));
		      }
		      ;
		      MELT_LOCATION ("warmelt-outobj.melt:3496:/ getslot");
		      {
			melt_ptr_t slot = NULL, obj = NULL;
			obj =
			  (melt_ptr_t) ( /*_.CURCTYP__V27*/ meltfptr[23])
			  /*=obj*/ ;
			melt_object_get_field (slot, obj, 5,
					       "CTYPE_PARSTRING");
       /*_.CTYPE_PARSTRING__V31*/ meltfptr[30] = slot;
		      };
		      ;

		      {
			/*^locexp */
			meltgc_append_list ((melt_ptr_t)
					    ( /*_.PARAMDESCLIST__V13*/
					     meltfptr[12]),
					    (melt_ptr_t) ( /*_.CTYPE_PARSTRING__V31*/ meltfptr[30]));
		      }
		      ;
		      MELT_LOCATION
			("warmelt-outobj.melt:3498:/ checksignal");
		      MELT_CHECK_SIGNAL ();
		      ;
      /*_#NULL__L24*/ meltfnum[23] =
			(( /*_.CURARG__V26*/ meltfptr[21]) == NULL);;
		      MELT_LOCATION ("warmelt-outobj.melt:3498:/ cond");
		      /*cond */ if ( /*_#NULL__L24*/ meltfnum[23])	/*then */
			{
			  /*^cond.then */
			  /*^block */
			  /*anyblock */
			  {




			    {
			      MELT_LOCATION
				("warmelt-outobj.melt:3499:/ locexp");
			      /*add2sbuf_strconst */
				meltgc_add_strbuf ((melt_ptr_t)
						   ( /*_.IMPLBUF__V4*/
						    meltfptr[3]),
						   ("meltbp_aptr = (melt_ptr_t*)NULL"));
			    }
			    ;
		  /*clear *//*_.IFELSE___V32*/ meltfptr[31] = 0;
			    /*epilog */
			  }
			  ;
			}
		      else
			{
			  MELT_LOCATION
			    ("warmelt-outobj.melt:3498:/ cond.else");

			  /*^block */
			  /*anyblock */
			  {

			    MELT_LOCATION
			      ("warmelt-outobj.melt:3500:/ checksignal");
			    MELT_CHECK_SIGNAL ();
			    ;
	/*_#__L25*/ meltfnum[24] =
			      (( /*_.CURCTYP__V27*/ meltfptr[23]) ==
			       (( /*!CTYPE_VALUE */ meltfrout->tabval[9])));;
			    MELT_LOCATION ("warmelt-outobj.melt:3500:/ cond");
			    /*cond */ if ( /*_#__L25*/ meltfnum[24])	/*then */
			      {
				/*^cond.then */
				/*^block */
				/*anyblock */
				{


				  {
				    MELT_LOCATION
				      ("warmelt-outobj.melt:3501:/ locexp");
				    /*add2sbuf_strconst */
				      meltgc_add_strbuf ((melt_ptr_t)
							 ( /*_.IMPLBUF__V4*/
							  meltfptr[3]),
							 ("meltbp_aptr = (melt_ptr_t*) &"));
				  }
				  ;
	  /*_#GET_INT__L26*/ meltfnum[25] =
				    (melt_get_int
				     ((melt_ptr_t)
				      ( /*_.BOXDEPTHP1__V15*/
				       meltfptr[14])));;
				  MELT_LOCATION
				    ("warmelt-outobj.melt:3502:/ checksignal");
				  MELT_CHECK_SIGNAL ();
				  ;
				  /*^msend */
				  /*msend */
				  {
				    union meltparam_un argtab[3];
				    memset (&argtab, 0, sizeof (argtab));
				    /*^ojbmsend.arg */
				    argtab[0].meltbp_aptr =
				      (melt_ptr_t *) & /*_.DECLBUF__V3*/
				      meltfptr[2];
				    /*^ojbmsend.arg */
				    argtab[1].meltbp_aptr =
				      (melt_ptr_t *) & /*_.IMPLBUF__V4*/
				      meltfptr[3];
				    /*^ojbmsend.arg */
				    argtab[2].meltbp_long =
				      /*_#GET_INT__L26*/ meltfnum[25];
				    /*_.OUTPUT_C_CODE__V34*/ meltfptr[33] =
				      meltgc_send ((melt_ptr_t)
						   ( /*_.CURARG__V26*/
						    meltfptr[21]),
						   (melt_ptr_t) (( /*!OUTPUT_C_CODE */ meltfrout->tabval[10])), (MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_LONG ""), argtab, "", (union meltparam_un *) 0);
				  }
				  ;
				  MELT_LOCATION
				    ("warmelt-outobj.melt:3500:/ quasiblock");


				  /*_.PROGN___V35*/ meltfptr[34] =
				    /*_.OUTPUT_C_CODE__V34*/ meltfptr[33];;
				  /*^compute */
				  /*_.IFELSE___V33*/ meltfptr[32] =
				    /*_.PROGN___V35*/ meltfptr[34];;
				  /*epilog */

				  MELT_LOCATION
				    ("warmelt-outobj.melt:3500:/ clear");
		    /*clear *//*_#GET_INT__L26*/ meltfnum[25] =
				    0;
				  /*^clear */
		    /*clear *//*_.OUTPUT_C_CODE__V34*/
				    meltfptr[33] = 0;
				  /*^clear */
		    /*clear *//*_.PROGN___V35*/ meltfptr[34] =
				    0;
				}
				;
			      }
			    else
			      {	/*^cond.else */

				/*^block */
				/*anyblock */
				{

				  MELT_LOCATION
				    ("warmelt-outobj.melt:3505:/ getslot");
				  {
				    melt_ptr_t slot = NULL, obj = NULL;
				    obj =
				      (melt_ptr_t) ( /*_.CURCTYP__V27*/
						    meltfptr[23]) /*=obj*/ ;
				    melt_object_get_field (slot, obj, 6,
							   "CTYPE_ARGFIELD");
	   /*_.CTYPE_ARGFIELD__V36*/ meltfptr[33] =
				      slot;
				  };
				  ;

				  {
				    /*^locexp */
				    /*add2sbuf_string */
				      meltgc_add_strbuf ((melt_ptr_t)
							 ( /*_.IMPLBUF__V4*/
							  meltfptr[3]),
							 melt_string_str ((melt_ptr_t) ( /*_.CTYPE_ARGFIELD__V36*/ meltfptr[33])));
				  }
				  ;

				  {
				    MELT_LOCATION
				      ("warmelt-outobj.melt:3506:/ locexp");
				    /*add2sbuf_strconst */
				      meltgc_add_strbuf ((melt_ptr_t)
							 ( /*_.IMPLBUF__V4*/
							  meltfptr[3]),
							 (" = "));
				  }
				  ;
	  /*_#GET_INT__L27*/ meltfnum[25] =
				    (melt_get_int
				     ((melt_ptr_t)
				      ( /*_.BOXDEPTHP1__V15*/
				       meltfptr[14])));;
				  MELT_LOCATION
				    ("warmelt-outobj.melt:3507:/ checksignal");
				  MELT_CHECK_SIGNAL ();
				  ;
				  /*^msend */
				  /*msend */
				  {
				    union meltparam_un argtab[3];
				    memset (&argtab, 0, sizeof (argtab));
				    /*^ojbmsend.arg */
				    argtab[0].meltbp_aptr =
				      (melt_ptr_t *) & /*_.DECLBUF__V3*/
				      meltfptr[2];
				    /*^ojbmsend.arg */
				    argtab[1].meltbp_aptr =
				      (melt_ptr_t *) & /*_.IMPLBUF__V4*/
				      meltfptr[3];
				    /*^ojbmsend.arg */
				    argtab[2].meltbp_long =
				      /*_#GET_INT__L27*/ meltfnum[25];
				    /*_.OUTPUT_C_CODE__V37*/ meltfptr[34] =
				      meltgc_send ((melt_ptr_t)
						   ( /*_.CURARG__V26*/
						    meltfptr[21]),
						   (melt_ptr_t) (( /*!OUTPUT_C_CODE */ meltfrout->tabval[10])), (MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_LONG ""), argtab, "", (union meltparam_un *) 0);
				  }
				  ;
				  MELT_LOCATION
				    ("warmelt-outobj.melt:3504:/ quasiblock");


				  /*_.PROGN___V38*/ meltfptr[37] =
				    /*_.OUTPUT_C_CODE__V37*/ meltfptr[34];;
				  /*^compute */
				  /*_.IFELSE___V33*/ meltfptr[32] =
				    /*_.PROGN___V38*/ meltfptr[37];;
				  /*epilog */

				  MELT_LOCATION
				    ("warmelt-outobj.melt:3500:/ clear");
		    /*clear *//*_.CTYPE_ARGFIELD__V36*/
				    meltfptr[33] = 0;
				  /*^clear */
		    /*clear *//*_#GET_INT__L27*/ meltfnum[25] =
				    0;
				  /*^clear */
		    /*clear *//*_.OUTPUT_C_CODE__V37*/
				    meltfptr[34] = 0;
				  /*^clear */
		    /*clear *//*_.PROGN___V38*/ meltfptr[37] =
				    0;
				}
				;
			      }
			    ;
			    /*_.IFELSE___V32*/ meltfptr[31] =
			      /*_.IFELSE___V33*/ meltfptr[32];;
			    /*epilog */

			    MELT_LOCATION
			      ("warmelt-outobj.melt:3498:/ clear");
		  /*clear *//*_#__L25*/ meltfnum[24] = 0;
			    /*^clear */
		  /*clear *//*_.IFELSE___V33*/ meltfptr[32] = 0;
			  }
			  ;
			}
		      ;

		      {
			MELT_LOCATION ("warmelt-outobj.melt:3509:/ locexp");
			/*add2sbuf_strconst */
			  meltgc_add_strbuf ((melt_ptr_t)
					     ( /*_.IMPLBUF__V4*/ meltfptr[3]),
					     (";"));
		      }
		      ;

		      MELT_LOCATION ("warmelt-outobj.melt:3490:/ clear");
		/*clear *//*_.CURCTYP__V27*/ meltfptr[23] = 0;
		      /*^clear */
		/*clear *//*_.IFCPP___V28*/ meltfptr[24] = 0;
		      /*^clear */
		/*clear *//*_#GET_INT__L22*/ meltfnum[20] = 0;
		      /*^clear */
		/*clear *//*_.OUTPUT_LOCATION__V30*/ meltfptr[28] = 0;
		      /*^clear */
		/*clear *//*_#I__L23*/ meltfnum[22] = 0;
		      /*^clear */
		/*clear *//*_.CTYPE_PARSTRING__V31*/ meltfptr[30] = 0;
		      /*^clear */
		/*clear *//*_#NULL__L24*/ meltfnum[23] = 0;
		      /*^clear */
		/*clear *//*_.IFELSE___V32*/ meltfptr[31] = 0;
		      /*epilog */
		    }
		    ;
		  }		/*noelse */
		;

#if MELT_HAVE_DEBUG
		MELT_LOCATION ("warmelt-outobj.melt:3511:/ cppif.then");
		/*^block */
		/*anyblock */
		{

		  /*^checksignal */
		  MELT_CHECK_SIGNAL ();
		  ;
      /*_#STRBUF_USEDLENGTH__L28*/ meltfnum[25] =
		    melt_strbuf_usedlength ((melt_ptr_t)
					    ( /*_.IMPLBUF__V4*/
					     meltfptr[3]));;
		  MELT_LOCATION ("warmelt-outobj.melt:3512:/ cond");
		  /*cond */ if (
				 /*ifisa */
				 melt_is_instance_of ((melt_ptr_t)
						      (( /*!BUFFER_LIMIT_CONT */ meltfrout->tabval[11])),
						      (melt_ptr_t) (( /*!CLASS_REFERENCE */ meltfrout->tabval[12])))
		    )		/*then */
		    {
		      /*^cond.then */
		      /*^getslot */
		      {
			melt_ptr_t slot = NULL, obj = NULL;
			obj =
			  (melt_ptr_t) (( /*!BUFFER_LIMIT_CONT */ meltfrout->
					 tabval[11])) /*=obj*/ ;
			melt_object_get_field (slot, obj, 0,
					       "REFERENCED_VALUE");
	/*_.REFERENCED_VALUE__V40*/ meltfptr[34] = slot;
		      };
		      ;
		    }
		  else
		    {		/*^cond.else */

       /*_.REFERENCED_VALUE__V40*/ meltfptr[34] = NULL;;
		    }
		  ;
		  /*^compute */
      /*_#GET_INT__L29*/ meltfnum[24] =
		    (melt_get_int
		     ((melt_ptr_t)
		      ( /*_.REFERENCED_VALUE__V40*/ meltfptr[34])));;
		  /*^compute */
      /*_#I__L30*/ meltfnum[20] =
		    (( /*_#STRBUF_USEDLENGTH__L28*/ meltfnum[25]) <
		     ( /*_#GET_INT__L29*/ meltfnum[24]));;
		  MELT_LOCATION ("warmelt-outobj.melt:3511:/ cond");
		  /*cond */ if ( /*_#I__L30*/ meltfnum[20])	/*then */
		    {
		      /*^cond.then */
		      /*_.IFELSE___V41*/ meltfptr[37] = ( /*nil */ NULL);;
		    }
		  else
		    {
		      MELT_LOCATION ("warmelt-outobj.melt:3511:/ cond.else");

		      /*^block */
		      /*anyblock */
		      {




			{
			  /*^locexp */
			  melt_assert_failed (("check limited implbuf"),
					      ("warmelt-outobj.melt")
					      ? ("warmelt-outobj.melt") :
					      __FILE__,
					      (3511) ? (3511) : __LINE__,
					      __FUNCTION__);
			  ;
			}
			;
		  /*clear *//*_.IFELSE___V41*/ meltfptr[37] = 0;
			/*epilog */
		      }
		      ;
		    }
		  ;
		  /*^compute */
		  /*_.IFCPP___V39*/ meltfptr[33] =
		    /*_.IFELSE___V41*/ meltfptr[37];;
		  /*epilog */

		  MELT_LOCATION ("warmelt-outobj.melt:3511:/ clear");
		/*clear *//*_#STRBUF_USEDLENGTH__L28*/ meltfnum[25] = 0;
		  /*^clear */
		/*clear *//*_.REFERENCED_VALUE__V40*/ meltfptr[34] = 0;
		  /*^clear */
		/*clear *//*_#GET_INT__L29*/ meltfnum[24] = 0;
		  /*^clear */
		/*clear *//*_#I__L30*/ meltfnum[20] = 0;
		  /*^clear */
		/*clear *//*_.IFELSE___V41*/ meltfptr[37] = 0;
		}

#else /*MELT_HAVE_DEBUG */
		/*^cppif.else */
		/*_.IFCPP___V39*/ meltfptr[33] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
		;
		if ( /*_#CURANK__L19*/ meltfnum[18] < 0)
		  break;
	      }			/* end  foreach_in_multiple meltcit1__EACHTUP */

	    /*citerepilog */

	    MELT_LOCATION ("warmelt-outobj.melt:3486:/ clear");
	      /*clear *//*_.CURARG__V26*/ meltfptr[21] = 0;
	    /*^clear */
	      /*clear *//*_#CURANK__L19*/ meltfnum[18] = 0;
	    /*^clear */
	      /*clear *//*_#I__L20*/ meltfnum[19] = 0;
	    /*^clear */
	      /*clear *//*_.IFCPP___V39*/ meltfptr[33] = 0;
	  }			/*endciterblock FOREACH_IN_MULTIPLE */
	  ;
   /*_#GET_INT__L31*/ meltfnum[22] =
	    (melt_get_int
	     ((melt_ptr_t) ( /*_.BOXDEPTHP1__V15*/ meltfptr[14])));;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:3514:/ locexp");
	    meltgc_strbuf_add_indent ((melt_ptr_t)
				      ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				      ( /*_#GET_INT__L31*/ meltfnum[22]), 0);
	  }
	  ;
	  MELT_LOCATION ("warmelt-outobj.melt:3482:/ quasiblock");


	  /*epilog */

	  MELT_LOCATION ("warmelt-outobj.melt:3481:/ clear");
	     /*clear *//*_#I__L18*/ meltfnum[17] = 0;
	  /*^clear */
	     /*clear *//*_#GET_INT__L31*/ meltfnum[22] = 0;
	}
	;
      }				/*noelse */
    ;
    MELT_LOCATION ("warmelt-outobj.melt:3517:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#I__L32*/ meltfnum[23] =
      (( /*_#NBXRES__L4*/ meltfnum[3]) > (0));;
    MELT_LOCATION ("warmelt-outobj.melt:3517:/ cond");
    /*cond */ if ( /*_#I__L32*/ meltfnum[23])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-outobj.melt:3521:/ quasiblock");


	  /*^newclosure */
		   /*newclosure *//*_.LAMBDA___V44*/ meltfptr[24] =
	    (melt_ptr_t)
	    meltgc_new_closure ((meltobject_ptr_t)
				(((melt_ptr_t)
				  (MELT_PREDEF (DISCR_CLOSURE)))),
				(meltroutine_ptr_t) (( /*!konst_18 */
						      meltfrout->tabval[18])),
				(4));
	  ;
	  /*^putclosedv */
	  /*putclosv */
	  melt_assertmsg ("putclosv checkclo",
			  melt_magic_discr ((melt_ptr_t)
					    ( /*_.LAMBDA___V44*/
					     meltfptr[24])) ==
			  MELTOBMAG_CLOSURE);
	  melt_assertmsg ("putclosv checkoff", 0 >= 0
			  && 0 <
			  melt_closure_size ((melt_ptr_t)
					     ( /*_.LAMBDA___V44*/
					      meltfptr[24])));
	  ((meltclosure_ptr_t) /*_.LAMBDA___V44*/ meltfptr[24])->tabval[0] =
	    (melt_ptr_t) ( /*_.ALOC__V8*/ meltfptr[7]);
	  ;
	  /*^putclosedv */
	  /*putclosv */
	  melt_assertmsg ("putclosv checkclo",
			  melt_magic_discr ((melt_ptr_t)
					    ( /*_.LAMBDA___V44*/
					     meltfptr[24])) ==
			  MELTOBMAG_CLOSURE);
	  melt_assertmsg ("putclosv checkoff", 1 >= 0
			  && 1 <
			  melt_closure_size ((melt_ptr_t)
					     ( /*_.LAMBDA___V44*/
					      meltfptr[24])));
	  ((meltclosure_ptr_t) /*_.LAMBDA___V44*/ meltfptr[24])->tabval[1] =
	    (melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]);
	  ;
	  /*^putclosedv */
	  /*putclosv */
	  melt_assertmsg ("putclosv checkclo",
			  melt_magic_discr ((melt_ptr_t)
					    ( /*_.LAMBDA___V44*/
					     meltfptr[24])) ==
			  MELTOBMAG_CLOSURE);
	  melt_assertmsg ("putclosv checkoff", 2 >= 0
			  && 2 <
			  melt_closure_size ((melt_ptr_t)
					     ( /*_.LAMBDA___V44*/
					      meltfptr[24])));
	  ((meltclosure_ptr_t) /*_.LAMBDA___V44*/ meltfptr[24])->tabval[2] =
	    (melt_ptr_t) ( /*_.BOXDEPTHP1__V15*/ meltfptr[14]);
	  ;
	  /*^putclosedv */
	  /*putclosv */
	  melt_assertmsg ("putclosv checkclo",
			  melt_magic_discr ((melt_ptr_t)
					    ( /*_.LAMBDA___V44*/
					     meltfptr[24])) ==
			  MELTOBMAG_CLOSURE);
	  melt_assertmsg ("putclosv checkoff", 3 >= 0
			  && 3 <
			  melt_closure_size ((melt_ptr_t)
					     ( /*_.LAMBDA___V44*/
					      meltfptr[24])));
	  ((meltclosure_ptr_t) /*_.LAMBDA___V44*/ meltfptr[24])->tabval[3] =
	    (melt_ptr_t) ( /*_.DECLBUF__V3*/ meltfptr[2]);
	  ;
	  /*_.LAMBDA___V43*/ meltfptr[23] = /*_.LAMBDA___V44*/ meltfptr[24];;
	  MELT_LOCATION ("warmelt-outobj.melt:3519:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^apply */
	  /*apply */
	  {
	    union meltparam_un argtab[1];
	    memset (&argtab, 0, sizeof (argtab));
	    /*^apply.arg */
	    argtab[0].meltbp_aptr =
	      (melt_ptr_t *) & /*_.LAMBDA___V43*/ meltfptr[23];
	    /*_.MULTIPLE_EVERY__V45*/ meltfptr[28] =
	      melt_apply ((meltclosure_ptr_t)
			  (( /*!MULTIPLE_EVERY */ meltfrout->tabval[4])),
			  (melt_ptr_t) ( /*_.OXRES__V12*/ meltfptr[11]),
			  (MELTBPARSTR_PTR ""), argtab, "",
			  (union meltparam_un *) 0);
	  }
	  ;
	  MELT_LOCATION ("warmelt-outobj.melt:3518:/ quasiblock");


	  /*_.PROGN___V46*/ meltfptr[30] =
	    /*_.MULTIPLE_EVERY__V45*/ meltfptr[28];;
	  /*^compute */
	  /*_.IF___V42*/ meltfptr[32] = /*_.PROGN___V46*/ meltfptr[30];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-outobj.melt:3517:/ clear");
	     /*clear *//*_.LAMBDA___V43*/ meltfptr[23] = 0;
	  /*^clear */
	     /*clear *//*_.MULTIPLE_EVERY__V45*/ meltfptr[28] = 0;
	  /*^clear */
	     /*clear *//*_.PROGN___V46*/ meltfptr[30] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

  /*_.IF___V42*/ meltfptr[32] = NULL;;
      }
    ;
    /*^compute */
 /*_#GET_INT__L33*/ meltfnum[25] =
      (melt_get_int ((melt_ptr_t) ( /*_.BOXDEPTHP1__V15*/ meltfptr[14])));;
    MELT_LOCATION ("warmelt-outobj.melt:3545:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[3];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.IMPLBUF__V4*/ meltfptr[3];
      /*^apply.arg */
      argtab[1].meltbp_long = /*_#GET_INT__L33*/ meltfnum[25];
      /*^apply.arg */
      argtab[2].meltbp_cstring = "multiapply.appl";
      /*_.OUTPUT_LOCATION__V47*/ meltfptr[31] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!OUTPUT_LOCATION */ meltfrout->tabval[3])),
		    (melt_ptr_t) ( /*_.ALOC__V8*/ meltfptr[7]),
		    (MELTBPARSTR_PTR MELTBPARSTR_LONG MELTBPARSTR_CSTRING ""),
		    argtab, "", (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:3549:/ quasiblock");


    /*^newclosure */
		 /*newclosure *//*_.LAMBDA___V49*/ meltfptr[37] =
      (melt_ptr_t)
      meltgc_new_closure ((meltobject_ptr_t)
			  (((melt_ptr_t) (MELT_PREDEF (DISCR_CLOSURE)))),
			  (meltroutine_ptr_t) (( /*!konst_21 */ meltfrout->
						tabval[21])), (3));
    ;
    /*^putclosedv */
    /*putclosv */
    melt_assertmsg ("putclosv checkclo",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.LAMBDA___V49*/ meltfptr[37])) ==
		    MELTOBMAG_CLOSURE);
    melt_assertmsg ("putclosv checkoff", 0 >= 0
		    && 0 <
		    melt_closure_size ((melt_ptr_t)
				       ( /*_.LAMBDA___V49*/ meltfptr[37])));
    ((meltclosure_ptr_t) /*_.LAMBDA___V49*/ meltfptr[37])->tabval[0] =
      (melt_ptr_t) ( /*_.DECLBUF__V3*/ meltfptr[2]);
    ;
    /*^putclosedv */
    /*putclosv */
    melt_assertmsg ("putclosv checkclo",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.LAMBDA___V49*/ meltfptr[37])) ==
		    MELTOBMAG_CLOSURE);
    melt_assertmsg ("putclosv checkoff", 1 >= 0
		    && 1 <
		    melt_closure_size ((melt_ptr_t)
				       ( /*_.LAMBDA___V49*/ meltfptr[37])));
    ((meltclosure_ptr_t) /*_.LAMBDA___V49*/ meltfptr[37])->tabval[1] =
      (melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]);
    ;
    /*^putclosedv */
    /*putclosv */
    melt_assertmsg ("putclosv checkclo",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.LAMBDA___V49*/ meltfptr[37])) ==
		    MELTOBMAG_CLOSURE);
    melt_assertmsg ("putclosv checkoff", 2 >= 0
		    && 2 <
		    melt_closure_size ((melt_ptr_t)
				       ( /*_.LAMBDA___V49*/ meltfptr[37])));
    ((meltclosure_ptr_t) /*_.LAMBDA___V49*/ meltfptr[37])->tabval[2] =
      (melt_ptr_t) ( /*_.BOXDEPTHP1__V15*/ meltfptr[14]);
    ;
    /*_.LAMBDA___V48*/ meltfptr[34] = /*_.LAMBDA___V49*/ meltfptr[37];;
    MELT_LOCATION ("warmelt-outobj.melt:3547:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & /*_.LAMBDA___V48*/ meltfptr[34];
      /*_.LIST_EVERY__V50*/ meltfptr[23] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!LIST_EVERY */ meltfrout->tabval[19])),
		    (melt_ptr_t) ( /*_.ADEST__V9*/ meltfptr[8]),
		    (MELTBPARSTR_PTR ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:3553:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   (" melt_apply ((meltclosure_ptr_t)("));
    }
    ;
 /*_#I__L34*/ meltfnum[24] =
      ((1) + ( /*_#DEPTH__L1*/ meltfnum[0]));;
    MELT_LOCATION ("warmelt-outobj.melt:3554:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^msend */
    /*msend */
    {
      union meltparam_un argtab[3];
      memset (&argtab, 0, sizeof (argtab));
      /*^ojbmsend.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.DECLBUF__V3*/ meltfptr[2];
      /*^ojbmsend.arg */
      argtab[1].meltbp_aptr = (melt_ptr_t *) & /*_.IMPLBUF__V4*/ meltfptr[3];
      /*^ojbmsend.arg */
      argtab[2].meltbp_long = /*_#I__L34*/ meltfnum[24];
      /*_.OUTPUT_C_CODE__V51*/ meltfptr[28] =
	meltgc_send ((melt_ptr_t) ( /*_.OCLOS__V10*/ meltfptr[9]),
		     (melt_ptr_t) (( /*!OUTPUT_C_CODE */ meltfrout->
				    tabval[10])),
		     (MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_LONG ""),
		     argtab, "", (union meltparam_un *) 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:3555:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   ("), (melt_ptr_t)("));
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:3557:/ quasiblock");


 /*_.FIRSTARG__V53*/ meltfptr[52] =
      (melt_multiple_nth
       ((melt_ptr_t) ( /*_.OARGS__V11*/ meltfptr[10]), (0)));;
    /*^compute */
 /*_#I__L35*/ meltfnum[20] =
      ((1) + ( /*_#DEPTH__L1*/ meltfnum[0]));;
    MELT_LOCATION ("warmelt-outobj.melt:3558:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^msend */
    /*msend */
    {
      union meltparam_un argtab[3];
      memset (&argtab, 0, sizeof (argtab));
      /*^ojbmsend.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.DECLBUF__V3*/ meltfptr[2];
      /*^ojbmsend.arg */
      argtab[1].meltbp_aptr = (melt_ptr_t *) & /*_.IMPLBUF__V4*/ meltfptr[3];
      /*^ojbmsend.arg */
      argtab[2].meltbp_long = /*_#I__L35*/ meltfnum[20];
      /*_.OUTPUT_C_CODE__V54*/ meltfptr[53] =
	meltgc_send ((melt_ptr_t) ( /*_.FIRSTARG__V53*/ meltfptr[52]),
		     (melt_ptr_t) (( /*!OUTPUT_C_CODE */ meltfrout->
				    tabval[10])),
		     (MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_LONG ""),
		     argtab, "", (union meltparam_un *) 0);
    }
    ;
    /*_.LET___V52*/ meltfptr[30] = /*_.OUTPUT_C_CODE__V54*/ meltfptr[53];;

    MELT_LOCATION ("warmelt-outobj.melt:3557:/ clear");
	   /*clear *//*_.FIRSTARG__V53*/ meltfptr[52] = 0;
    /*^clear */
	   /*clear *//*_#I__L35*/ meltfnum[20] = 0;
    /*^clear */
	   /*clear *//*_.OUTPUT_C_CODE__V54*/ meltfptr[53] = 0;

    {
      MELT_LOCATION ("warmelt-outobj.melt:3560:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   ("), ("));
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:3564:/ quasiblock");


    /*^newclosure */
		 /*newclosure *//*_.LAMBDA___V56*/ meltfptr[53] =
      (melt_ptr_t)
      meltgc_new_closure ((meltobject_ptr_t)
			  (((melt_ptr_t) (MELT_PREDEF (DISCR_CLOSURE)))),
			  (meltroutine_ptr_t) (( /*!konst_22 */ meltfrout->
						tabval[22])), (1));
    ;
    /*^putclosedv */
    /*putclosv */
    melt_assertmsg ("putclosv checkclo",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.LAMBDA___V56*/ meltfptr[53])) ==
		    MELTOBMAG_CLOSURE);
    melt_assertmsg ("putclosv checkoff", 0 >= 0
		    && 0 <
		    melt_closure_size ((melt_ptr_t)
				       ( /*_.LAMBDA___V56*/ meltfptr[53])));
    ((meltclosure_ptr_t) /*_.LAMBDA___V56*/ meltfptr[53])->tabval[0] =
      (melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]);
    ;
    /*_.LAMBDA___V55*/ meltfptr[52] = /*_.LAMBDA___V56*/ meltfptr[53];;
    MELT_LOCATION ("warmelt-outobj.melt:3562:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & /*_.LAMBDA___V55*/ meltfptr[52];
      /*_.LIST_EVERY__V57*/ meltfptr[56] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!LIST_EVERY */ meltfrout->tabval[19])),
		    (melt_ptr_t) ( /*_.PARAMDESCLIST__V13*/ meltfptr[12]),
		    (MELTBPARSTR_PTR ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:3567:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   ("\"\"), "));
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:3569:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#I__L36*/ meltfnum[17] =
      (( /*_#NBARG__L3*/ meltfnum[1]) > (1));;
    MELT_LOCATION ("warmelt-outobj.melt:3569:/ cond");
    /*cond */ if ( /*_#I__L36*/ meltfnum[17])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{


	  {
	    MELT_LOCATION ("warmelt-outobj.melt:3570:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t)
				 ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				 ("argtab, ("));
	  }
	  ;
	  /*epilog */
	}
	;
      }
    else
      {
	MELT_LOCATION ("warmelt-outobj.melt:3569:/ cond.else");

	/*^block */
	/*anyblock */
	{


	  {
	    MELT_LOCATION ("warmelt-outobj.melt:3571:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t)
				 ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				 ("(union meltparam_un*)0, ("));
	  }
	  ;
	  /*epilog */
	}
	;
      }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:3575:/ quasiblock");


    /*^newclosure */
		 /*newclosure *//*_.LAMBDA___V59*/ meltfptr[58] =
      (melt_ptr_t)
      meltgc_new_closure ((meltobject_ptr_t)
			  (((melt_ptr_t) (MELT_PREDEF (DISCR_CLOSURE)))),
			  (meltroutine_ptr_t) (( /*!konst_23 */ meltfrout->
						tabval[23])), (1));
    ;
    /*^putclosedv */
    /*putclosv */
    melt_assertmsg ("putclosv checkclo",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.LAMBDA___V59*/ meltfptr[58])) ==
		    MELTOBMAG_CLOSURE);
    melt_assertmsg ("putclosv checkoff", 0 >= 0
		    && 0 <
		    melt_closure_size ((melt_ptr_t)
				       ( /*_.LAMBDA___V59*/ meltfptr[58])));
    ((meltclosure_ptr_t) /*_.LAMBDA___V59*/ meltfptr[58])->tabval[0] =
      (melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]);
    ;
    /*_.LAMBDA___V58*/ meltfptr[57] = /*_.LAMBDA___V59*/ meltfptr[58];;
    MELT_LOCATION ("warmelt-outobj.melt:3573:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & /*_.LAMBDA___V58*/ meltfptr[57];
      /*_.LIST_EVERY__V60*/ meltfptr[59] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!LIST_EVERY */ meltfrout->tabval[19])),
		    (melt_ptr_t) ( /*_.RESDESCLIST__V14*/ meltfptr[13]),
		    (MELTBPARSTR_PTR ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:3578:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   ("\"\"), "));
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:3580:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#I__L37*/ meltfnum[22] =
      (( /*_#NBXRES__L4*/ meltfnum[3]) > (0));;
    MELT_LOCATION ("warmelt-outobj.melt:3580:/ cond");
    /*cond */ if ( /*_#I__L37*/ meltfnum[22])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{


	  {
	    MELT_LOCATION ("warmelt-outobj.melt:3581:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t)
				 ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				 ("restab"));
	  }
	  ;
	  /*epilog */
	}
	;
      }
    else
      {
	MELT_LOCATION ("warmelt-outobj.melt:3580:/ cond.else");

	/*^block */
	/*anyblock */
	{


	  {
	    MELT_LOCATION ("warmelt-outobj.melt:3582:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t)
				 ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				 ("(union meltparam_un*)0"));
	  }
	  ;
	  /*epilog */
	}
	;
      }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:3583:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   (");"));
    }
    ;
 /*_#I__L38*/ meltfnum[20] =
      ((1) + ( /*_#DEPTH__L1*/ meltfnum[0]));;

    {
      MELT_LOCATION ("warmelt-outobj.melt:3584:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				( /*_#I__L38*/ meltfnum[20]), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:3585:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   ("}"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:3586:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				( /*_#DEPTH__L1*/ meltfnum[0]), 0);
    }
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:3587:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#STRBUF_USEDLENGTH__L39*/ meltfnum[38] =
	melt_strbuf_usedlength ((melt_ptr_t)
				( /*_.IMPLBUF__V4*/ meltfptr[3]));;
      MELT_LOCATION ("warmelt-outobj.melt:3588:/ cond");
      /*cond */ if (
		     /*ifisa */
		     melt_is_instance_of ((melt_ptr_t)
					  (( /*!BUFFER_LIMIT_CONT */
					    meltfrout->tabval[11])),
					  (melt_ptr_t) (( /*!CLASS_REFERENCE */ meltfrout->tabval[12])))
	)			/*then */
	{
	  /*^cond.then */
	  /*^getslot */
	  {
	    melt_ptr_t slot = NULL, obj = NULL;
	    obj =
	      (melt_ptr_t) (( /*!BUFFER_LIMIT_CONT */ meltfrout->
			     tabval[11])) /*=obj*/ ;
	    melt_object_get_field (slot, obj, 0, "REFERENCED_VALUE");
     /*_.REFERENCED_VALUE__V62*/ meltfptr[61] = slot;
	  };
	  ;
	}
      else
	{			/*^cond.else */

    /*_.REFERENCED_VALUE__V62*/ meltfptr[61] = NULL;;
	}
      ;
      /*^compute */
   /*_#GET_INT__L40*/ meltfnum[39] =
	(melt_get_int
	 ((melt_ptr_t) ( /*_.REFERENCED_VALUE__V62*/ meltfptr[61])));;
      /*^compute */
   /*_#I__L41*/ meltfnum[40] =
	(( /*_#STRBUF_USEDLENGTH__L39*/ meltfnum[38]) <
	 ( /*_#GET_INT__L40*/ meltfnum[39]));;
      MELT_LOCATION ("warmelt-outobj.melt:3587:/ cond");
      /*cond */ if ( /*_#I__L41*/ meltfnum[40])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V63*/ meltfptr[62] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-outobj.melt:3587:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check limited implbuf"),
				  ("warmelt-outobj.melt")
				  ? ("warmelt-outobj.melt") : __FILE__,
				  (3587) ? (3587) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V63*/ meltfptr[62] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V61*/ meltfptr[60] = /*_.IFELSE___V63*/ meltfptr[62];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:3587:/ clear");
	     /*clear *//*_#STRBUF_USEDLENGTH__L39*/ meltfnum[38] = 0;
      /*^clear */
	     /*clear *//*_.REFERENCED_VALUE__V62*/ meltfptr[61] = 0;
      /*^clear */
	     /*clear *//*_#GET_INT__L40*/ meltfnum[39] = 0;
      /*^clear */
	     /*clear *//*_#I__L41*/ meltfnum[40] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V63*/ meltfptr[62] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V61*/ meltfptr[60] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    /*^compute */
    /*_.LET___V7*/ meltfptr[5] = /*_.IFCPP___V61*/ meltfptr[60];;

    MELT_LOCATION ("warmelt-outobj.melt:3434:/ clear");
	   /*clear *//*_.ALOC__V8*/ meltfptr[7] = 0;
    /*^clear */
	   /*clear *//*_.ADEST__V9*/ meltfptr[8] = 0;
    /*^clear */
	   /*clear *//*_.OCLOS__V10*/ meltfptr[9] = 0;
    /*^clear */
	   /*clear *//*_.OARGS__V11*/ meltfptr[10] = 0;
    /*^clear */
	   /*clear *//*_.OXRES__V12*/ meltfptr[11] = 0;
    /*^clear */
	   /*clear *//*_#NBARG__L3*/ meltfnum[1] = 0;
    /*^clear */
	   /*clear *//*_#NBXRES__L4*/ meltfnum[3] = 0;
    /*^clear */
	   /*clear *//*_.PARAMDESCLIST__V13*/ meltfptr[12] = 0;
    /*^clear */
	   /*clear *//*_.RESDESCLIST__V14*/ meltfptr[13] = 0;
    /*^clear */
	   /*clear *//*_#I__L5*/ meltfnum[4] = 0;
    /*^clear */
	   /*clear *//*_.BOXDEPTHP1__V15*/ meltfptr[14] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V16*/ meltfptr[15] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V18*/ meltfptr[16] = 0;
    /*^clear */
	   /*clear *//*_.OUTPUT_LOCATION__V20*/ meltfptr[18] = 0;
    /*^clear */
	   /*clear *//*_#I__L8*/ meltfnum[5] = 0;
    /*^clear */
	   /*clear *//*_#I__L9*/ meltfnum[8] = 0;
    /*^clear */
	   /*clear *//*_#I__L12*/ meltfnum[9] = 0;
    /*^clear */
	   /*clear *//*_#I__L15*/ meltfnum[10] = 0;
    /*^clear */
	   /*clear *//*_.IF___V21*/ meltfptr[20] = 0;
    /*^clear */
	   /*clear *//*_#I__L17*/ meltfnum[13] = 0;
    /*^clear */
	   /*clear *//*_#I__L32*/ meltfnum[23] = 0;
    /*^clear */
	   /*clear *//*_.IF___V42*/ meltfptr[32] = 0;
    /*^clear */
	   /*clear *//*_#GET_INT__L33*/ meltfnum[25] = 0;
    /*^clear */
	   /*clear *//*_.OUTPUT_LOCATION__V47*/ meltfptr[31] = 0;
    /*^clear */
	   /*clear *//*_.LAMBDA___V48*/ meltfptr[34] = 0;
    /*^clear */
	   /*clear *//*_.LIST_EVERY__V50*/ meltfptr[23] = 0;
    /*^clear */
	   /*clear *//*_#I__L34*/ meltfnum[24] = 0;
    /*^clear */
	   /*clear *//*_.OUTPUT_C_CODE__V51*/ meltfptr[28] = 0;
    /*^clear */
	   /*clear *//*_.LET___V52*/ meltfptr[30] = 0;
    /*^clear */
	   /*clear *//*_.LAMBDA___V55*/ meltfptr[52] = 0;
    /*^clear */
	   /*clear *//*_.LIST_EVERY__V57*/ meltfptr[56] = 0;
    /*^clear */
	   /*clear *//*_#I__L36*/ meltfnum[17] = 0;
    /*^clear */
	   /*clear *//*_.LAMBDA___V58*/ meltfptr[57] = 0;
    /*^clear */
	   /*clear *//*_.LIST_EVERY__V60*/ meltfptr[59] = 0;
    /*^clear */
	   /*clear *//*_#I__L37*/ meltfnum[22] = 0;
    /*^clear */
	   /*clear *//*_#I__L38*/ meltfnum[20] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V61*/ meltfptr[60] = 0;
    MELT_LOCATION ("warmelt-outobj.melt:3432:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.LET___V7*/ meltfptr[5];;

    {
      MELT_LOCATION ("warmelt-outobj.melt:3432:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V5*/ meltfptr[4] = 0;
    /*^clear */
	   /*clear *//*_.LET___V7*/ meltfptr[5] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("OUTPUCOD_OBJMULTIAPPLY", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_83_warmelt_outobj_OUTPUCOD_OBJMULTIAPPLY_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_83_warmelt_outobj_OUTPUCOD_OBJMULTIAPPLY */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_84_warmelt_outobj_LAMBDA___14__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un * meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un * meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_84_warmelt_outobj_LAMBDA___14___melt =
    melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_84_warmelt_outobj_LAMBDA___14___st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 4
    melt_ptr_t mcfr_varptr[4];
#define MELTFRAM_NBVARNUM 1
    long mcfr_varnum[1];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_84_warmelt_outobj_LAMBDA___14__ is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_84_warmelt_outobj_LAMBDA___14___st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 4; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_84_warmelt_outobj_LAMBDA___14__ nbval 4*/
  meltfram__.mcfr_nbvar = 4 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("LAMBDA_", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-outobj.melt:3478:/ getarg");
 /*_.CURES__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_LONG)
    goto lab_endgetargs;
 /*_#CURANK__L1*/ meltfnum[0] = meltxargtab_[0].meltbp_long;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {

    MELT_LOCATION ("warmelt-outobj.melt:3479:/ quasiblock");


    /*^checksignal */
    MELT_CHECK_SIGNAL ();
    ;
    /*^msend */
    /*msend */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^ojbmsend.arg */
      argtab[0].meltbp_aptr = /*nil */ (melt_ptr_t *) NULL;
      /*_.CURCTYP__V3*/ meltfptr[2] =
	meltgc_send ((melt_ptr_t) ( /*_.CURES__V2*/ meltfptr[1]),
		     (melt_ptr_t) (( /*!GET_CTYPE */ meltfrout->tabval[0])),
		     (MELTBPARSTR_PTR ""), argtab, "",
		     (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:3480:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.CURCTYP__V3*/ meltfptr[2]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 5, "CTYPE_PARSTRING");
  /*_.CTYPE_PARSTRING__V4*/ meltfptr[3] = slot;
    };
    ;

    {
      /*^locexp */
      meltgc_append_list ((melt_ptr_t)
			  (( /*~RESDESCLIST */ meltfclos->tabval[0])),
			  (melt_ptr_t) ( /*_.CTYPE_PARSTRING__V4*/
					meltfptr[3]));
    }
    ;

    MELT_LOCATION ("warmelt-outobj.melt:3479:/ clear");
	   /*clear *//*_.CURCTYP__V3*/ meltfptr[2] = 0;
    /*^clear */
	   /*clear *//*_.CTYPE_PARSTRING__V4*/ meltfptr[3] = 0;
    /*epilog */
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("LAMBDA_", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_84_warmelt_outobj_LAMBDA___14___melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*noretval */ NULL);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_84_warmelt_outobj_LAMBDA___14__ */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_85_warmelt_outobj_LAMBDA___15__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un * meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un * meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_85_warmelt_outobj_LAMBDA___15___melt =
    melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_85_warmelt_outobj_LAMBDA___15___st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 13
    melt_ptr_t mcfr_varptr[13];
#define MELTFRAM_NBVARNUM 8
    long mcfr_varnum[8];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_85_warmelt_outobj_LAMBDA___15__ is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_85_warmelt_outobj_LAMBDA___15___st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 13; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_85_warmelt_outobj_LAMBDA___15__ nbval 13*/
  meltfram__.mcfr_nbvar = 13 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("LAMBDA_", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-outobj.melt:3521:/ getarg");
 /*_.CURES__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_LONG)
    goto lab_endgetargs;
 /*_#CURANK__L1*/ meltfnum[0] = meltxargtab_[0].meltbp_long;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {

    MELT_LOCATION ("warmelt-outobj.melt:3522:/ quasiblock");


    /*^checksignal */
    MELT_CHECK_SIGNAL ();
    ;
    /*^msend */
    /*msend */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^ojbmsend.arg */
      argtab[0].meltbp_aptr = /*nil */ (melt_ptr_t *) NULL;
      /*_.CURESTYP__V3*/ meltfptr[2] =
	meltgc_send ((melt_ptr_t) ( /*_.CURES__V2*/ meltfptr[1]),
		     (melt_ptr_t) (( /*!GET_CTYPE */ meltfrout->tabval[0])),
		     (MELTBPARSTR_PTR ""), argtab, "",
		     (union meltparam_un *) 0);
    }
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:3523:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L2*/ meltfnum[1] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.CURESTYP__V3*/ meltfptr[2]),
			     (melt_ptr_t) (( /*!CLASS_CTYPE */ meltfrout->
					    tabval[1])));;
      MELT_LOCATION ("warmelt-outobj.melt:3523:/ cond");
      /*cond */ if ( /*_#IS_A__L2*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V5*/ meltfptr[4] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-outobj.melt:3523:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check curestyp"),
				  ("warmelt-outobj.melt")
				  ? ("warmelt-outobj.melt") : __FILE__,
				  (3523) ? (3523) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V5*/ meltfptr[4] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V4*/ meltfptr[3] = /*_.IFELSE___V5*/ meltfptr[4];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:3523:/ clear");
	     /*clear *//*_#IS_A__L2*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V5*/ meltfptr[4] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V4*/ meltfptr[3] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    /*^compute */
 /*_#GET_INT__L3*/ meltfnum[1] =
      (melt_get_int
       ((melt_ptr_t) (( /*~BOXDEPTHP1 */ meltfclos->tabval[2]))));;
    MELT_LOCATION ("warmelt-outobj.melt:3524:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[3];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & ( /*~IMPLBUF */ meltfclos->tabval[1]);
      /*^apply.arg */
      argtab[1].meltbp_long = /*_#GET_INT__L3*/ meltfnum[1];
      /*^apply.arg */
      argtab[2].meltbp_cstring = "multiapply.xres";
      /*_.OUTPUT_LOCATION__V6*/ meltfptr[4] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!OUTPUT_LOCATION */ meltfrout->tabval[2])),
		    (melt_ptr_t) (( /*~ALOC */ meltfclos->tabval[0])),
		    (MELTBPARSTR_PTR MELTBPARSTR_LONG MELTBPARSTR_CSTRING ""),
		    argtab, "", (union meltparam_un *) 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:3525:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t)
			   (( /*~IMPLBUF */ meltfclos->tabval[1])),
			   ("restab["));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:3526:/ locexp");
      meltgc_add_strbuf_dec ((melt_ptr_t)
			     (( /*~IMPLBUF */ meltfclos->tabval[1])),
			     ( /*_#CURANK__L1*/ meltfnum[0]));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:3527:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t)
			   (( /*~IMPLBUF */ meltfclos->tabval[1])), ("]."));
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:3529:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#NULL__L4*/ meltfnum[3] =
      (( /*_.CURES__V2*/ meltfptr[1]) == NULL);;
    MELT_LOCATION ("warmelt-outobj.melt:3529:/ cond");
    /*cond */ if ( /*_#NULL__L4*/ meltfnum[3])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{




	  {
	    MELT_LOCATION ("warmelt-outobj.melt:3530:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t)
				 (( /*~IMPLBUF */ meltfclos->tabval[1])),
				 ("meltbp_aptr = (melt_ptr_t*)NULL"));
	  }
	  ;
	     /*clear *//*_.IFELSE___V7*/ meltfptr[6] = 0;
	  /*epilog */
	}
	;
      }
    else
      {
	MELT_LOCATION ("warmelt-outobj.melt:3529:/ cond.else");

	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-outobj.melt:3531:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
   /*_#__L5*/ meltfnum[4] =
	    (( /*_.CURESTYP__V3*/ meltfptr[2]) ==
	     (( /*!CTYPE_VALUE */ meltfrout->tabval[3])));;
	  MELT_LOCATION ("warmelt-outobj.melt:3531:/ cond");
	  /*cond */ if ( /*_#__L5*/ meltfnum[4])	/*then */
	    {
	      /*^cond.then */
	      /*^block */
	      /*anyblock */
	      {


		{
		  MELT_LOCATION ("warmelt-outobj.melt:3532:/ locexp");
		  /*add2sbuf_strconst */
		    meltgc_add_strbuf ((melt_ptr_t)
				       (( /*~IMPLBUF */ meltfclos->
					 tabval[1])),
				       ("meltbp_aptr = (melt_ptr_t*) &"));
		}
		;
     /*_#GET_INT__L6*/ meltfnum[5] =
		  (melt_get_int
		   ((melt_ptr_t)
		    (( /*~BOXDEPTHP1 */ meltfclos->tabval[2]))));;
		MELT_LOCATION ("warmelt-outobj.melt:3533:/ checksignal");
		MELT_CHECK_SIGNAL ();
		;
		/*^msend */
		/*msend */
		{
		  union meltparam_un argtab[3];
		  memset (&argtab, 0, sizeof (argtab));
		  /*^ojbmsend.arg */
		  argtab[0].meltbp_aptr =
		    (melt_ptr_t *) & ( /*~DECLBUF */ meltfclos->tabval[3]);
		  /*^ojbmsend.arg */
		  argtab[1].meltbp_aptr =
		    (melt_ptr_t *) & ( /*~IMPLBUF */ meltfclos->tabval[1]);
		  /*^ojbmsend.arg */
		  argtab[2].meltbp_long = /*_#GET_INT__L6*/ meltfnum[5];
		  /*_.OUTPUT_C_CODE__V9*/ meltfptr[8] =
		    meltgc_send ((melt_ptr_t) ( /*_.CURES__V2*/ meltfptr[1]),
				 (melt_ptr_t) (( /*!OUTPUT_C_CODE */
						meltfrout->tabval[4])),
				 (MELTBPARSTR_PTR MELTBPARSTR_PTR
				  MELTBPARSTR_LONG ""), argtab, "",
				 (union meltparam_un *) 0);
		}
		;
		MELT_LOCATION ("warmelt-outobj.melt:3531:/ quasiblock");


		/*_.PROGN___V10*/ meltfptr[9] =
		  /*_.OUTPUT_C_CODE__V9*/ meltfptr[8];;
		/*^compute */
		/*_.IFELSE___V8*/ meltfptr[7] =
		  /*_.PROGN___V10*/ meltfptr[9];;
		/*epilog */

		MELT_LOCATION ("warmelt-outobj.melt:3531:/ clear");
	       /*clear *//*_#GET_INT__L6*/ meltfnum[5] = 0;
		/*^clear */
	       /*clear *//*_.OUTPUT_C_CODE__V9*/ meltfptr[8] = 0;
		/*^clear */
	       /*clear *//*_.PROGN___V10*/ meltfptr[9] = 0;
	      }
	      ;
	    }
	  else
	    {			/*^cond.else */

	      /*^block */
	      /*anyblock */
	      {

		MELT_LOCATION ("warmelt-outobj.melt:3536:/ getslot");
		{
		  melt_ptr_t slot = NULL, obj = NULL;
		  obj =
		    (melt_ptr_t) ( /*_.CURESTYP__V3*/ meltfptr[2]) /*=obj*/ ;
		  melt_object_get_field (slot, obj, 7, "CTYPE_RESFIELD");
      /*_.CTYPE_RESFIELD__V11*/ meltfptr[8] = slot;
		};
		;

		{
		  /*^locexp */
		  /*add2sbuf_string */
		    meltgc_add_strbuf ((melt_ptr_t)
				       (( /*~IMPLBUF */ meltfclos->
					 tabval[1])),
				       melt_string_str ((melt_ptr_t)
							( /*_.CTYPE_RESFIELD__V11*/ meltfptr[8])));
		}
		;

		{
		  MELT_LOCATION ("warmelt-outobj.melt:3537:/ locexp");
		  /*add2sbuf_strconst */
		    meltgc_add_strbuf ((melt_ptr_t)
				       (( /*~IMPLBUF */ meltfclos->
					 tabval[1])), (" =  & "));
		}
		;
     /*_#GET_INT__L7*/ meltfnum[5] =
		  (melt_get_int
		   ((melt_ptr_t)
		    (( /*~BOXDEPTHP1 */ meltfclos->tabval[2]))));;
		MELT_LOCATION ("warmelt-outobj.melt:3538:/ checksignal");
		MELT_CHECK_SIGNAL ();
		;
		/*^msend */
		/*msend */
		{
		  union meltparam_un argtab[3];
		  memset (&argtab, 0, sizeof (argtab));
		  /*^ojbmsend.arg */
		  argtab[0].meltbp_aptr =
		    (melt_ptr_t *) & ( /*~DECLBUF */ meltfclos->tabval[3]);
		  /*^ojbmsend.arg */
		  argtab[1].meltbp_aptr =
		    (melt_ptr_t *) & ( /*~IMPLBUF */ meltfclos->tabval[1]);
		  /*^ojbmsend.arg */
		  argtab[2].meltbp_long = /*_#GET_INT__L7*/ meltfnum[5];
		  /*_.OUTPUT_C_CODE__V12*/ meltfptr[9] =
		    meltgc_send ((melt_ptr_t) ( /*_.CURES__V2*/ meltfptr[1]),
				 (melt_ptr_t) (( /*!OUTPUT_C_CODE */
						meltfrout->tabval[4])),
				 (MELTBPARSTR_PTR MELTBPARSTR_PTR
				  MELTBPARSTR_LONG ""), argtab, "",
				 (union meltparam_un *) 0);
		}
		;
		MELT_LOCATION ("warmelt-outobj.melt:3535:/ quasiblock");


		/*_.PROGN___V13*/ meltfptr[12] =
		  /*_.OUTPUT_C_CODE__V12*/ meltfptr[9];;
		/*^compute */
		/*_.IFELSE___V8*/ meltfptr[7] =
		  /*_.PROGN___V13*/ meltfptr[12];;
		/*epilog */

		MELT_LOCATION ("warmelt-outobj.melt:3531:/ clear");
	       /*clear *//*_.CTYPE_RESFIELD__V11*/ meltfptr[8] = 0;
		/*^clear */
	       /*clear *//*_#GET_INT__L7*/ meltfnum[5] = 0;
		/*^clear */
	       /*clear *//*_.OUTPUT_C_CODE__V12*/ meltfptr[9] = 0;
		/*^clear */
	       /*clear *//*_.PROGN___V13*/ meltfptr[12] = 0;
	      }
	      ;
	    }
	  ;
	  /*_.IFELSE___V7*/ meltfptr[6] = /*_.IFELSE___V8*/ meltfptr[7];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-outobj.melt:3529:/ clear");
	     /*clear *//*_#__L5*/ meltfnum[4] = 0;
	  /*^clear */
	     /*clear *//*_.IFELSE___V8*/ meltfptr[7] = 0;
	}
	;
      }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:3540:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t)
			   (( /*~IMPLBUF */ meltfclos->tabval[1])), (";"));
    }
    ;
 /*_#GET_INT__L8*/ meltfnum[5] =
      (melt_get_int
       ((melt_ptr_t) (( /*~BOXDEPTHP1 */ meltfclos->tabval[2]))));;

    {
      MELT_LOCATION ("warmelt-outobj.melt:3541:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t)
				(( /*~IMPLBUF */ meltfclos->tabval[1])),
				( /*_#GET_INT__L8*/ meltfnum[5]), 0);
    }
    ;

    MELT_LOCATION ("warmelt-outobj.melt:3522:/ clear");
	   /*clear *//*_.CURESTYP__V3*/ meltfptr[2] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V4*/ meltfptr[3] = 0;
    /*^clear */
	   /*clear *//*_#GET_INT__L3*/ meltfnum[1] = 0;
    /*^clear */
	   /*clear *//*_.OUTPUT_LOCATION__V6*/ meltfptr[4] = 0;
    /*^clear */
	   /*clear *//*_#NULL__L4*/ meltfnum[3] = 0;
    /*^clear */
	   /*clear *//*_.IFELSE___V7*/ meltfptr[6] = 0;
    /*^clear */
	   /*clear *//*_#GET_INT__L8*/ meltfnum[5] = 0;
    /*epilog */
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("LAMBDA_", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_85_warmelt_outobj_LAMBDA___15___melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*noretval */ NULL);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_85_warmelt_outobj_LAMBDA___15__ */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_86_warmelt_outobj_LAMBDA___16__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un * meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un * meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_86_warmelt_outobj_LAMBDA___16___melt =
    melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_86_warmelt_outobj_LAMBDA___16___st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 3
    melt_ptr_t mcfr_varptr[3];
#define MELTFRAM_NBVARNUM 1
    long mcfr_varnum[1];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_86_warmelt_outobj_LAMBDA___16__ is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_86_warmelt_outobj_LAMBDA___16___st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 3; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_86_warmelt_outobj_LAMBDA___16__ nbval 3*/
  meltfram__.mcfr_nbvar = 3 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("LAMBDA_", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-outobj.melt:3549:/ getarg");
 /*_.CURDEST__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {

 /*_#GET_INT__L1*/ meltfnum[0] =
      (melt_get_int
       ((melt_ptr_t) (( /*~BOXDEPTHP1 */ meltfclos->tabval[2]))));;
    MELT_LOCATION ("warmelt-outobj.melt:3550:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^msend */
    /*msend */
    {
      union meltparam_un argtab[3];
      memset (&argtab, 0, sizeof (argtab));
      /*^ojbmsend.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & ( /*~DECLBUF */ meltfclos->tabval[0]);
      /*^ojbmsend.arg */
      argtab[1].meltbp_aptr =
	(melt_ptr_t *) & ( /*~IMPLBUF */ meltfclos->tabval[1]);
      /*^ojbmsend.arg */
      argtab[2].meltbp_long = /*_#GET_INT__L1*/ meltfnum[0];
      /*_.OUTPUT_C_CODE__V3*/ meltfptr[2] =
	meltgc_send ((melt_ptr_t) ( /*_.CURDEST__V2*/ meltfptr[1]),
		     (melt_ptr_t) (( /*!OUTPUT_C_CODE */ meltfrout->
				    tabval[0])),
		     (MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_LONG ""),
		     argtab, "", (union meltparam_un *) 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:3551:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t)
			   (( /*~IMPLBUF */ meltfclos->tabval[1])), (" = "));
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:3549:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*epilog */

    /*^clear */
	   /*clear *//*_#GET_INT__L1*/ meltfnum[0] = 0;
    /*^clear */
	   /*clear *//*_.OUTPUT_C_CODE__V3*/ meltfptr[2] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("LAMBDA_", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_86_warmelt_outobj_LAMBDA___16___melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*noretval */ NULL);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_86_warmelt_outobj_LAMBDA___16__ */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_87_warmelt_outobj_LAMBDA___17__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un * meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un * meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_87_warmelt_outobj_LAMBDA___17___melt =
    melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_87_warmelt_outobj_LAMBDA___17___st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 2
    melt_ptr_t mcfr_varptr[2];
/*no varnum*/
#define MELTFRAM_NBVARNUM /*none*/0
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_87_warmelt_outobj_LAMBDA___17__ is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_87_warmelt_outobj_LAMBDA___17___st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 2; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_87_warmelt_outobj_LAMBDA___17__ nbval 2*/
  meltfram__.mcfr_nbvar = 2 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("LAMBDA_", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-outobj.melt:3564:/ getarg");
 /*_.PARD__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


    {
      MELT_LOCATION ("warmelt-outobj.melt:3565:/ locexp");
      /*add2sbuf_string */
	meltgc_add_strbuf ((melt_ptr_t)
			   (( /*~IMPLBUF */ meltfclos->tabval[0])),
			   melt_string_str ((melt_ptr_t)
					    ( /*_.PARD__V2*/ meltfptr[1])));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:3566:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t)
			   (( /*~IMPLBUF */ meltfclos->tabval[0])), (" "));
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:3564:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*epilog */
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("LAMBDA_", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_87_warmelt_outobj_LAMBDA___17___melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*noretval */ NULL);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_87_warmelt_outobj_LAMBDA___17__ */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_88_warmelt_outobj_LAMBDA___18__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un * meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un * meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_88_warmelt_outobj_LAMBDA___18___melt =
    melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_88_warmelt_outobj_LAMBDA___18___st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 2
    melt_ptr_t mcfr_varptr[2];
/*no varnum*/
#define MELTFRAM_NBVARNUM /*none*/0
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_88_warmelt_outobj_LAMBDA___18__ is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_88_warmelt_outobj_LAMBDA___18___st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 2; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_88_warmelt_outobj_LAMBDA___18__ nbval 2*/
  meltfram__.mcfr_nbvar = 2 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("LAMBDA_", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-outobj.melt:3575:/ getarg");
 /*_.RESD__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


    {
      MELT_LOCATION ("warmelt-outobj.melt:3576:/ locexp");
      /*add2sbuf_string */
	meltgc_add_strbuf ((melt_ptr_t)
			   (( /*~IMPLBUF */ meltfclos->tabval[0])),
			   melt_string_str ((melt_ptr_t)
					    ( /*_.RESD__V2*/ meltfptr[1])));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:3577:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t)
			   (( /*~IMPLBUF */ meltfclos->tabval[0])), (" "));
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:3575:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*epilog */
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("LAMBDA_", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_88_warmelt_outobj_LAMBDA___18___melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*noretval */ NULL);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_88_warmelt_outobj_LAMBDA___18__ */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_89_warmelt_outobj_OUTPUCOD_OBJMULTIMSEND (meltclosure_ptr_t
						   meltclosp_,
						   melt_ptr_t meltfirstargp_,
						   const melt_argdescr_cell_t
						   meltxargdescr_[],
						   union meltparam_un *
						   meltxargtab_,
						   const melt_argdescr_cell_t
						   meltxresdescr_[],
						   union meltparam_un *
						   meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_89_warmelt_outobj_OUTPUCOD_OBJMULTIMSEND_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_89_warmelt_outobj_OUTPUCOD_OBJMULTIMSEND_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 58
    melt_ptr_t mcfr_varptr[58];
#define MELTFRAM_NBVARNUM 36
    long mcfr_varnum[36];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_89_warmelt_outobj_OUTPUCOD_OBJMULTIMSEND is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_89_warmelt_outobj_OUTPUCOD_OBJMULTIMSEND_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 58; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_89_warmelt_outobj_OUTPUCOD_OBJMULTIMSEND nbval 58*/
  meltfram__.mcfr_nbvar = 58 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("OUTPUCOD_OBJMULTIMSEND", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-outobj.melt:3597:/ getarg");
 /*_.OMSND__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.DECLBUF__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.DECLBUF__V3*/ meltfptr[2])) !=
	      NULL);


  /*getarg#2 */
  /*^getarg */
  if (meltxargdescr_[1] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.IMPLBUF__V4*/ meltfptr[3] =
    (meltxargtab_[1].meltbp_aptr) ? (*(meltxargtab_[1].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3])) !=
	      NULL);


  /*getarg#3 */
  /*^getarg */
  if (meltxargdescr_[2] != MELTBPAR_LONG)
    goto lab_endgetargs;
 /*_#DEPTH__L1*/ meltfnum[0] = meltxargtab_[2].meltbp_long;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:3598:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L2*/ meltfnum[1] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.OMSND__V2*/ meltfptr[1]),
			     (melt_ptr_t) (( /*!CLASS_OBJMULTIMSEND */
					    meltfrout->tabval[0])));;
      MELT_LOCATION ("warmelt-outobj.melt:3598:/ cond");
      /*cond */ if ( /*_#IS_A__L2*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V6*/ meltfptr[5] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-outobj.melt:3598:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check omsnd"),
				  ("warmelt-outobj.melt")
				  ? ("warmelt-outobj.melt") : __FILE__,
				  (3598) ? (3598) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V6*/ meltfptr[5] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V5*/ meltfptr[4] = /*_.IFELSE___V6*/ meltfptr[5];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:3598:/ clear");
	     /*clear *//*_#IS_A__L2*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V6*/ meltfptr[5] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V5*/ meltfptr[4] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-outobj.melt:3599:/ quasiblock");


    /*^getslot */
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.OMSND__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 0, "OBI_LOC");
  /*_.OLOC__V8*/ meltfptr[7] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-outobj.melt:3600:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.OMSND__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 1, "OBDI_DESTLIST");
  /*_.ODEST__V9*/ meltfptr[8] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-outobj.melt:3601:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.OMSND__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 2, "OBMSND_SEL");
  /*_.OSEL__V10*/ meltfptr[9] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-outobj.melt:3602:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.OMSND__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 3, "OBMSND_RECV");
  /*_.ORECV__V11*/ meltfptr[10] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-outobj.melt:3603:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.OMSND__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 4, "OBMSND_ARGS");
  /*_.OARGS__V12*/ meltfptr[11] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-outobj.melt:3604:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.OMSND__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 5, "OBMULTSND_XRES");
  /*_.OXRES__V13*/ meltfptr[12] = slot;
    };
    ;
 /*_#NBARG__L3*/ meltfnum[1] =
      (melt_multiple_length ((melt_ptr_t) ( /*_.OARGS__V12*/ meltfptr[11])));;
    /*^compute */
 /*_#NBXRES__L4*/ meltfnum[3] =
      (melt_multiple_length ((melt_ptr_t) ( /*_.OXRES__V13*/ meltfptr[12])));;
    /*^compute */
 /*_.PARAMDESCLIST__V14*/ meltfptr[13] =
      (meltgc_new_list
       ((meltobject_ptr_t) (( /*!DISCR_LIST */ meltfrout->tabval[1]))));;
    /*^compute */
 /*_.RESDESCLIST__V15*/ meltfptr[14] =
      (meltgc_new_list
       ((meltobject_ptr_t) (( /*!DISCR_LIST */ meltfrout->tabval[1]))));;
    /*^compute */
 /*_#I__L5*/ meltfnum[4] =
      ((1) + ( /*_#DEPTH__L1*/ meltfnum[0]));;
    /*^compute */
 /*_.BOXDEPTHP1__V16*/ meltfptr[15] =
      (meltgc_new_int
       ((meltobject_ptr_t) (( /*!DISCR_INTEGER */ meltfrout->tabval[2])),
	( /*_#I__L5*/ meltfnum[4])));;
    MELT_LOCATION ("warmelt-outobj.melt:3611:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[3];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.IMPLBUF__V4*/ meltfptr[3];
      /*^apply.arg */
      argtab[1].meltbp_long = /*_#DEPTH__L1*/ meltfnum[0];
      /*^apply.arg */
      argtab[2].meltbp_cstring = "multimsend";
      /*_.OUTPUT_LOCATION__V17*/ meltfptr[16] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!OUTPUT_LOCATION */ meltfrout->tabval[3])),
		    (melt_ptr_t) ( /*_.OLOC__V8*/ meltfptr[7]),
		    (MELTBPARSTR_PTR MELTBPARSTR_LONG MELTBPARSTR_CSTRING ""),
		    argtab, "", (union meltparam_un *) 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:3612:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   ("/*multimsend*/{"));
    }
    ;
 /*_#I__L6*/ meltfnum[5] =
      ((1) + ( /*_#DEPTH__L1*/ meltfnum[0]));;

    {
      MELT_LOCATION ("warmelt-outobj.melt:3613:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				( /*_#I__L6*/ meltfnum[5]), 0);
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:3614:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#I__L7*/ meltfnum[6] =
      (( /*_#NBARG__L3*/ meltfnum[1]) > (0));;
    MELT_LOCATION ("warmelt-outobj.melt:3614:/ cond");
    /*cond */ if ( /*_#I__L7*/ meltfnum[6])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{


	  {
	    MELT_LOCATION ("warmelt-outobj.melt:3616:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t)
				 ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				 ("union meltparam_un argtab["));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:3617:/ locexp");
	    meltgc_add_strbuf_dec ((melt_ptr_t)
				   ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				   ( /*_#NBARG__L3*/ meltfnum[1]));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:3618:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t)
				 ( /*_.IMPLBUF__V4*/ meltfptr[3]), ("];"));
	  }
	  ;
   /*_#I__L8*/ meltfnum[7] =
	    ((1) + ( /*_#DEPTH__L1*/ meltfnum[0]));;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:3619:/ locexp");
	    meltgc_strbuf_add_indent ((melt_ptr_t)
				      ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				      ( /*_#I__L8*/ meltfnum[7]), 0);
	  }
	  ;
	  MELT_LOCATION ("warmelt-outobj.melt:3615:/ quasiblock");


	  /*epilog */

	  MELT_LOCATION ("warmelt-outobj.melt:3614:/ clear");
	     /*clear *//*_#I__L8*/ meltfnum[7] = 0;
	}
	;
      }				/*noelse */
    ;
    MELT_LOCATION ("warmelt-outobj.melt:3621:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#I__L9*/ meltfnum[7] =
      (( /*_#NBXRES__L4*/ meltfnum[3]) > (0));;
    MELT_LOCATION ("warmelt-outobj.melt:3621:/ cond");
    /*cond */ if ( /*_#I__L9*/ meltfnum[7])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{


	  {
	    MELT_LOCATION ("warmelt-outobj.melt:3623:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t)
				 ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				 ("union meltparam_un restab["));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:3624:/ locexp");
	    meltgc_add_strbuf_dec ((melt_ptr_t)
				   ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				   ( /*_#NBXRES__L4*/ meltfnum[3]));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:3625:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t)
				 ( /*_.IMPLBUF__V4*/ meltfptr[3]), ("];"));
	  }
	  ;
   /*_#I__L10*/ meltfnum[9] =
	    ((1) + ( /*_#DEPTH__L1*/ meltfnum[0]));;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:3626:/ locexp");
	    meltgc_strbuf_add_indent ((melt_ptr_t)
				      ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				      ( /*_#I__L10*/ meltfnum[9]), 0);
	  }
	  ;
	  MELT_LOCATION ("warmelt-outobj.melt:3630:/ quasiblock");


	  /*^newclosure */
		   /*newclosure *//*_.LAMBDA___V20*/ meltfptr[19] =
	    (melt_ptr_t)
	    meltgc_new_closure ((meltobject_ptr_t)
				(((melt_ptr_t)
				  (MELT_PREDEF (DISCR_CLOSURE)))),
				(meltroutine_ptr_t) (( /*!konst_6 */
						      meltfrout->tabval[6])),
				(1));
	  ;
	  /*^putclosedv */
	  /*putclosv */
	  melt_assertmsg ("putclosv checkclo",
			  melt_magic_discr ((melt_ptr_t)
					    ( /*_.LAMBDA___V20*/
					     meltfptr[19])) ==
			  MELTOBMAG_CLOSURE);
	  melt_assertmsg ("putclosv checkoff", 0 >= 0
			  && 0 <
			  melt_closure_size ((melt_ptr_t)
					     ( /*_.LAMBDA___V20*/
					      meltfptr[19])));
	  ((meltclosure_ptr_t) /*_.LAMBDA___V20*/ meltfptr[19])->tabval[0] =
	    (melt_ptr_t) ( /*_.RESDESCLIST__V15*/ meltfptr[14]);
	  ;
	  /*_.LAMBDA___V19*/ meltfptr[18] = /*_.LAMBDA___V20*/ meltfptr[19];;
	  MELT_LOCATION ("warmelt-outobj.melt:3628:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^apply */
	  /*apply */
	  {
	    union meltparam_un argtab[1];
	    memset (&argtab, 0, sizeof (argtab));
	    /*^apply.arg */
	    argtab[0].meltbp_aptr =
	      (melt_ptr_t *) & /*_.LAMBDA___V19*/ meltfptr[18];
	    /*_.MULTIPLE_EVERY__V21*/ meltfptr[20] =
	      melt_apply ((meltclosure_ptr_t)
			  (( /*!MULTIPLE_EVERY */ meltfrout->tabval[4])),
			  (melt_ptr_t) ( /*_.OXRES__V13*/ meltfptr[12]),
			  (MELTBPARSTR_PTR ""), argtab, "",
			  (union meltparam_un *) 0);
	  }
	  ;
	  MELT_LOCATION ("warmelt-outobj.melt:3622:/ quasiblock");


	  /*_.PROGN___V22*/ meltfptr[21] =
	    /*_.MULTIPLE_EVERY__V21*/ meltfptr[20];;
	  /*^compute */
	  /*_.IF___V18*/ meltfptr[17] = /*_.PROGN___V22*/ meltfptr[21];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-outobj.melt:3621:/ clear");
	     /*clear *//*_#I__L10*/ meltfnum[9] = 0;
	  /*^clear */
	     /*clear *//*_.LAMBDA___V19*/ meltfptr[18] = 0;
	  /*^clear */
	     /*clear *//*_.MULTIPLE_EVERY__V21*/ meltfptr[20] = 0;
	  /*^clear */
	     /*clear *//*_.PROGN___V22*/ meltfptr[21] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

  /*_.IF___V18*/ meltfptr[17] = NULL;;
      }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:3634:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#I__L11*/ meltfnum[9] =
      (( /*_#NBARG__L3*/ meltfnum[1]) > (0));;
    MELT_LOCATION ("warmelt-outobj.melt:3634:/ cond");
    /*cond */ if ( /*_#I__L11*/ meltfnum[9])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{


	  {
	    MELT_LOCATION ("warmelt-outobj.melt:3636:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t)
				 ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				 ("memset(&argtab, 0, sizeof(argtab));"));
	  }
	  ;
   /*_#I__L12*/ meltfnum[11] =
	    ((1) + ( /*_#DEPTH__L1*/ meltfnum[0]));;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:3637:/ locexp");
	    meltgc_strbuf_add_indent ((melt_ptr_t)
				      ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				      ( /*_#I__L12*/ meltfnum[11]), 0);
	  }
	  ;
	  MELT_LOCATION ("warmelt-outobj.melt:3635:/ quasiblock");


	  /*epilog */

	  MELT_LOCATION ("warmelt-outobj.melt:3634:/ clear");
	     /*clear *//*_#I__L12*/ meltfnum[11] = 0;
	}
	;
      }				/*noelse */
    ;
    MELT_LOCATION ("warmelt-outobj.melt:3638:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#I__L13*/ meltfnum[11] =
      (( /*_#NBXRES__L4*/ meltfnum[3]) > (0));;
    MELT_LOCATION ("warmelt-outobj.melt:3638:/ cond");
    /*cond */ if ( /*_#I__L13*/ meltfnum[11])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{


	  {
	    MELT_LOCATION ("warmelt-outobj.melt:3640:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t)
				 ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				 ("memset(&restab, 0, sizeof(restab));"));
	  }
	  ;
   /*_#I__L14*/ meltfnum[13] =
	    ((1) + ( /*_#DEPTH__L1*/ meltfnum[0]));;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:3641:/ locexp");
	    meltgc_strbuf_add_indent ((melt_ptr_t)
				      ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				      ( /*_#I__L14*/ meltfnum[13]), 0);
	  }
	  ;
	  MELT_LOCATION ("warmelt-outobj.melt:3639:/ quasiblock");


	  /*epilog */

	  MELT_LOCATION ("warmelt-outobj.melt:3638:/ clear");
	     /*clear *//*_#I__L14*/ meltfnum[13] = 0;
	}
	;
      }				/*noelse */
    ;
    MELT_LOCATION ("warmelt-outobj.melt:3643:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#I__L15*/ meltfnum[13] =
      (( /*_#NBARG__L3*/ meltfnum[1]) > (0));;
    MELT_LOCATION ("warmelt-outobj.melt:3643:/ cond");
    /*cond */ if ( /*_#I__L15*/ meltfnum[13])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  /*citerblock FOREACH_IN_MULTIPLE */
	  {
	    /* start foreach_in_multiple meltcit1__EACHTUP */
	    long meltcit1__EACHTUP_ln =
	      melt_multiple_length ((melt_ptr_t) /*_.OARGS__V12*/
				    meltfptr[11]);
	    for ( /*_#CURANK__L16*/ meltfnum[15] = 0;
		 ( /*_#CURANK__L16*/ meltfnum[15] >= 0)
		 && ( /*_#CURANK__L16*/ meltfnum[15] < meltcit1__EACHTUP_ln);
	/*_#CURANK__L16*/ meltfnum[15]++)
	      {
		/*_.CURARG__V23*/ meltfptr[18] =
		  melt_multiple_nth ((melt_ptr_t)
				     ( /*_.OARGS__V12*/ meltfptr[11]),
				     /*_#CURANK__L16*/ meltfnum[15]);



		MELT_LOCATION ("warmelt-outobj.melt:3649:/ quasiblock");


		/*^checksignal */
		MELT_CHECK_SIGNAL ();
		;
		/*^msend */
		/*msend */
		{
		  union meltparam_un argtab[1];
		  memset (&argtab, 0, sizeof (argtab));
		  /*^ojbmsend.arg */
		  argtab[0].meltbp_aptr = /*nil */ (melt_ptr_t *) NULL;
		  /*_.CURCTYP__V24*/ meltfptr[20] =
		    meltgc_send ((melt_ptr_t)
				 ( /*_.CURARG__V23*/ meltfptr[18]),
				 (melt_ptr_t) (( /*!GET_CTYPE */ meltfrout->
						tabval[7])),
				 (MELTBPARSTR_PTR ""), argtab, "",
				 (union meltparam_un *) 0);
		}
		;

#if MELT_HAVE_DEBUG
		MELT_LOCATION ("warmelt-outobj.melt:3650:/ cppif.then");
		/*^block */
		/*anyblock */
		{

		  /*^checksignal */
		  MELT_CHECK_SIGNAL ();
		  ;
      /*_#IS_A__L17*/ meltfnum[16] =
		    melt_is_instance_of ((melt_ptr_t)
					 ( /*_.CURCTYP__V24*/ meltfptr[20]),
					 (melt_ptr_t) (( /*!CLASS_CTYPE */
							meltfrout->
							tabval[8])));;
		  MELT_LOCATION ("warmelt-outobj.melt:3650:/ cond");
		  /*cond */ if ( /*_#IS_A__L17*/ meltfnum[16])	/*then */
		    {
		      /*^cond.then */
		      /*_.IFELSE___V26*/ meltfptr[25] = ( /*nil */ NULL);;
		    }
		  else
		    {
		      MELT_LOCATION ("warmelt-outobj.melt:3650:/ cond.else");

		      /*^block */
		      /*anyblock */
		      {




			{
			  /*^locexp */
			  melt_assert_failed (("check curctyp"),
					      ("warmelt-outobj.melt")
					      ? ("warmelt-outobj.melt") :
					      __FILE__,
					      (3650) ? (3650) : __LINE__,
					      __FUNCTION__);
			  ;
			}
			;
		  /*clear *//*_.IFELSE___V26*/ meltfptr[25] = 0;
			/*epilog */
		      }
		      ;
		    }
		  ;
		  /*^compute */
		  /*_.IFCPP___V25*/ meltfptr[21] =
		    /*_.IFELSE___V26*/ meltfptr[25];;
		  /*epilog */

		  MELT_LOCATION ("warmelt-outobj.melt:3650:/ clear");
		/*clear *//*_#IS_A__L17*/ meltfnum[16] = 0;
		  /*^clear */
		/*clear *//*_.IFELSE___V26*/ meltfptr[25] = 0;
		}

#else /*MELT_HAVE_DEBUG */
		/*^cppif.else */
		/*_.IFCPP___V25*/ meltfptr[21] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
		;
		/*^compute */
    /*_#GET_INT__L18*/ meltfnum[16] =
		  (melt_get_int
		   ((melt_ptr_t) ( /*_.BOXDEPTHP1__V16*/ meltfptr[15])));;
		MELT_LOCATION ("warmelt-outobj.melt:3651:/ checksignal");
		MELT_CHECK_SIGNAL ();
		;
		/*^apply */
		/*apply */
		{
		  union meltparam_un argtab[3];
		  memset (&argtab, 0, sizeof (argtab));
		  /*^apply.arg */
		  argtab[0].meltbp_aptr =
		    (melt_ptr_t *) & /*_.IMPLBUF__V4*/ meltfptr[3];
		  /*^apply.arg */
		  argtab[1].meltbp_long = /*_#GET_INT__L18*/ meltfnum[16];
		  /*^apply.arg */
		  argtab[2].meltbp_cstring = "multimsend.arg";
		  /*_.OUTPUT_LOCATION__V27*/ meltfptr[25] =
		    melt_apply ((meltclosure_ptr_t)
				(( /*!OUTPUT_LOCATION */ meltfrout->
				  tabval[3])),
				(melt_ptr_t) ( /*_.OLOC__V8*/ meltfptr[7]),
				(MELTBPARSTR_PTR MELTBPARSTR_LONG
				 MELTBPARSTR_CSTRING ""), argtab, "",
				(union meltparam_un *) 0);
		}
		;

		{
		  MELT_LOCATION ("warmelt-outobj.melt:3652:/ locexp");
		  /*add2sbuf_strconst */
		    meltgc_add_strbuf ((melt_ptr_t)
				       ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				       ("argtab["));
		}
		;

		{
		  MELT_LOCATION ("warmelt-outobj.melt:3653:/ locexp");
		  meltgc_add_strbuf_dec ((melt_ptr_t)
					 ( /*_.IMPLBUF__V4*/ meltfptr[3]),
					 ( /*_#CURANK__L16*/ meltfnum[15]));
		}
		;

		{
		  MELT_LOCATION ("warmelt-outobj.melt:3654:/ locexp");
		  /*add2sbuf_strconst */
		    meltgc_add_strbuf ((melt_ptr_t)
				       ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				       ("]."));
		}
		;
		MELT_LOCATION ("warmelt-outobj.melt:3655:/ getslot");
		{
		  melt_ptr_t slot = NULL, obj = NULL;
		  obj =
		    (melt_ptr_t) ( /*_.CURCTYP__V24*/ meltfptr[20]) /*=obj*/ ;
		  melt_object_get_field (slot, obj, 5, "CTYPE_PARSTRING");
     /*_.CTYPE_PARSTRING__V28*/ meltfptr[27] = slot;
		};
		;

		{
		  /*^locexp */
		  meltgc_append_list ((melt_ptr_t)
				      ( /*_.PARAMDESCLIST__V14*/
				       meltfptr[13]),
				      (melt_ptr_t) ( /*_.CTYPE_PARSTRING__V28*/ meltfptr[27]));
		}
		;
		MELT_LOCATION ("warmelt-outobj.melt:3657:/ checksignal");
		MELT_CHECK_SIGNAL ();
		;
    /*_#NULL__L19*/ meltfnum[18] =
		  (( /*_.CURARG__V23*/ meltfptr[18]) == NULL);;
		MELT_LOCATION ("warmelt-outobj.melt:3657:/ cond");
		/*cond */ if ( /*_#NULL__L19*/ meltfnum[18])	/*then */
		  {
		    /*^cond.then */
		    /*^block */
		    /*anyblock */
		    {




		      {
			MELT_LOCATION ("warmelt-outobj.melt:3658:/ locexp");
			/*add2sbuf_strconst */
			  meltgc_add_strbuf ((melt_ptr_t)
					     ( /*_.IMPLBUF__V4*/ meltfptr[3]),
					     ("meltbp_aptr = (melt_ptr_t*)NULL"));
		      }
		      ;
		/*clear *//*_.IFELSE___V29*/ meltfptr[28] = 0;
		      /*epilog */
		    }
		    ;
		  }
		else
		  {
		    MELT_LOCATION ("warmelt-outobj.melt:3657:/ cond.else");

		    /*^block */
		    /*anyblock */
		    {

		      MELT_LOCATION
			("warmelt-outobj.melt:3659:/ checksignal");
		      MELT_CHECK_SIGNAL ();
		      ;
      /*_#__L20*/ meltfnum[19] =
			(( /*_.CURCTYP__V24*/ meltfptr[20]) ==
			 (( /*!CTYPE_VALUE */ meltfrout->tabval[9])));;
		      MELT_LOCATION ("warmelt-outobj.melt:3659:/ cond");
		      /*cond */ if ( /*_#__L20*/ meltfnum[19])	/*then */
			{
			  /*^cond.then */
			  /*^block */
			  /*anyblock */
			  {


			    {
			      MELT_LOCATION
				("warmelt-outobj.melt:3660:/ locexp");
			      /*add2sbuf_strconst */
				meltgc_add_strbuf ((melt_ptr_t)
						   ( /*_.IMPLBUF__V4*/
						    meltfptr[3]),
						   ("meltbp_aptr = (melt_ptr_t*) &"));
			    }
			    ;
	/*_#GET_INT__L21*/ meltfnum[20] =
			      (melt_get_int
			       ((melt_ptr_t)
				( /*_.BOXDEPTHP1__V16*/ meltfptr[15])));;
			    MELT_LOCATION
			      ("warmelt-outobj.melt:3661:/ checksignal");
			    MELT_CHECK_SIGNAL ();
			    ;
			    /*^msend */
			    /*msend */
			    {
			      union meltparam_un argtab[3];
			      memset (&argtab, 0, sizeof (argtab));
			      /*^ojbmsend.arg */
			      argtab[0].meltbp_aptr =
				(melt_ptr_t *) & /*_.DECLBUF__V3*/
				meltfptr[2];
			      /*^ojbmsend.arg */
			      argtab[1].meltbp_aptr =
				(melt_ptr_t *) & /*_.IMPLBUF__V4*/
				meltfptr[3];
			      /*^ojbmsend.arg */
			      argtab[2].meltbp_long =
				/*_#GET_INT__L21*/ meltfnum[20];
			      /*_.OUTPUT_C_CODE__V31*/ meltfptr[30] =
				meltgc_send ((melt_ptr_t)
					     ( /*_.CURARG__V23*/
					      meltfptr[18]),
					     (melt_ptr_t) (( /*!OUTPUT_C_CODE */ meltfrout->tabval[10])), (MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_LONG ""), argtab, "", (union meltparam_un *) 0);
			    }
			    ;
			    MELT_LOCATION
			      ("warmelt-outobj.melt:3659:/ quasiblock");


			    /*_.PROGN___V32*/ meltfptr[31] =
			      /*_.OUTPUT_C_CODE__V31*/ meltfptr[30];;
			    /*^compute */
			    /*_.IFELSE___V30*/ meltfptr[29] =
			      /*_.PROGN___V32*/ meltfptr[31];;
			    /*epilog */

			    MELT_LOCATION
			      ("warmelt-outobj.melt:3659:/ clear");
		  /*clear *//*_#GET_INT__L21*/ meltfnum[20] = 0;
			    /*^clear */
		  /*clear *//*_.OUTPUT_C_CODE__V31*/ meltfptr[30] =
			      0;
			    /*^clear */
		  /*clear *//*_.PROGN___V32*/ meltfptr[31] = 0;
			  }
			  ;
			}
		      else
			{	/*^cond.else */

			  /*^block */
			  /*anyblock */
			  {

			    MELT_LOCATION
			      ("warmelt-outobj.melt:3664:/ getslot");
			    {
			      melt_ptr_t slot = NULL, obj = NULL;
			      obj =
				(melt_ptr_t) ( /*_.CURCTYP__V24*/
					      meltfptr[20]) /*=obj*/ ;
			      melt_object_get_field (slot, obj, 6,
						     "CTYPE_ARGFIELD");
	 /*_.CTYPE_ARGFIELD__V33*/ meltfptr[30] = slot;
			    };
			    ;

			    {
			      /*^locexp */
			      /*add2sbuf_string */
				meltgc_add_strbuf ((melt_ptr_t)
						   ( /*_.IMPLBUF__V4*/
						    meltfptr[3]),
						   melt_string_str ((melt_ptr_t) ( /*_.CTYPE_ARGFIELD__V33*/ meltfptr[30])));
			    }
			    ;

			    {
			      MELT_LOCATION
				("warmelt-outobj.melt:3665:/ locexp");
			      /*add2sbuf_strconst */
				meltgc_add_strbuf ((melt_ptr_t)
						   ( /*_.IMPLBUF__V4*/
						    meltfptr[3]), (" = "));
			    }
			    ;
	/*_#GET_INT__L22*/ meltfnum[20] =
			      (melt_get_int
			       ((melt_ptr_t)
				( /*_.BOXDEPTHP1__V16*/ meltfptr[15])));;
			    MELT_LOCATION
			      ("warmelt-outobj.melt:3666:/ checksignal");
			    MELT_CHECK_SIGNAL ();
			    ;
			    /*^msend */
			    /*msend */
			    {
			      union meltparam_un argtab[3];
			      memset (&argtab, 0, sizeof (argtab));
			      /*^ojbmsend.arg */
			      argtab[0].meltbp_aptr =
				(melt_ptr_t *) & /*_.DECLBUF__V3*/
				meltfptr[2];
			      /*^ojbmsend.arg */
			      argtab[1].meltbp_aptr =
				(melt_ptr_t *) & /*_.IMPLBUF__V4*/
				meltfptr[3];
			      /*^ojbmsend.arg */
			      argtab[2].meltbp_long =
				/*_#GET_INT__L22*/ meltfnum[20];
			      /*_.OUTPUT_C_CODE__V34*/ meltfptr[31] =
				meltgc_send ((melt_ptr_t)
					     ( /*_.CURARG__V23*/
					      meltfptr[18]),
					     (melt_ptr_t) (( /*!OUTPUT_C_CODE */ meltfrout->tabval[10])), (MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_LONG ""), argtab, "", (union meltparam_un *) 0);
			    }
			    ;
			    MELT_LOCATION
			      ("warmelt-outobj.melt:3663:/ quasiblock");


			    /*_.PROGN___V35*/ meltfptr[34] =
			      /*_.OUTPUT_C_CODE__V34*/ meltfptr[31];;
			    /*^compute */
			    /*_.IFELSE___V30*/ meltfptr[29] =
			      /*_.PROGN___V35*/ meltfptr[34];;
			    /*epilog */

			    MELT_LOCATION
			      ("warmelt-outobj.melt:3659:/ clear");
		  /*clear *//*_.CTYPE_ARGFIELD__V33*/ meltfptr[30] =
			      0;
			    /*^clear */
		  /*clear *//*_#GET_INT__L22*/ meltfnum[20] = 0;
			    /*^clear */
		  /*clear *//*_.OUTPUT_C_CODE__V34*/ meltfptr[31] =
			      0;
			    /*^clear */
		  /*clear *//*_.PROGN___V35*/ meltfptr[34] = 0;
			  }
			  ;
			}
		      ;
		      /*_.IFELSE___V29*/ meltfptr[28] =
			/*_.IFELSE___V30*/ meltfptr[29];;
		      /*epilog */

		      MELT_LOCATION ("warmelt-outobj.melt:3657:/ clear");
		/*clear *//*_#__L20*/ meltfnum[19] = 0;
		      /*^clear */
		/*clear *//*_.IFELSE___V30*/ meltfptr[29] = 0;
		    }
		    ;
		  }
		;

		{
		  MELT_LOCATION ("warmelt-outobj.melt:3668:/ locexp");
		  /*add2sbuf_strconst */
		    meltgc_add_strbuf ((melt_ptr_t)
				       ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				       (";"));
		}
		;

		MELT_LOCATION ("warmelt-outobj.melt:3649:/ clear");
	      /*clear *//*_.CURCTYP__V24*/ meltfptr[20] = 0;
		/*^clear */
	      /*clear *//*_.IFCPP___V25*/ meltfptr[21] = 0;
		/*^clear */
	      /*clear *//*_#GET_INT__L18*/ meltfnum[16] = 0;
		/*^clear */
	      /*clear *//*_.OUTPUT_LOCATION__V27*/ meltfptr[25] = 0;
		/*^clear */
	      /*clear *//*_.CTYPE_PARSTRING__V28*/ meltfptr[27] = 0;
		/*^clear */
	      /*clear *//*_#NULL__L19*/ meltfnum[18] = 0;
		/*^clear */
	      /*clear *//*_.IFELSE___V29*/ meltfptr[28] = 0;

#if MELT_HAVE_DEBUG
		MELT_LOCATION ("warmelt-outobj.melt:3670:/ cppif.then");
		/*^block */
		/*anyblock */
		{

		  /*^checksignal */
		  MELT_CHECK_SIGNAL ();
		  ;
      /*_#STRBUF_USEDLENGTH__L23*/ meltfnum[20] =
		    melt_strbuf_usedlength ((melt_ptr_t)
					    ( /*_.IMPLBUF__V4*/
					     meltfptr[3]));;
		  MELT_LOCATION ("warmelt-outobj.melt:3671:/ cond");
		  /*cond */ if (
				 /*ifisa */
				 melt_is_instance_of ((melt_ptr_t)
						      (( /*!BUFFER_LIMIT_CONT */ meltfrout->tabval[11])),
						      (melt_ptr_t) (( /*!CLASS_REFERENCE */ meltfrout->tabval[12])))
		    )		/*then */
		    {
		      /*^cond.then */
		      /*^getslot */
		      {
			melt_ptr_t slot = NULL, obj = NULL;
			obj =
			  (melt_ptr_t) (( /*!BUFFER_LIMIT_CONT */ meltfrout->
					 tabval[11])) /*=obj*/ ;
			melt_object_get_field (slot, obj, 0,
					       "REFERENCED_VALUE");
	/*_.REFERENCED_VALUE__V37*/ meltfptr[31] = slot;
		      };
		      ;
		    }
		  else
		    {		/*^cond.else */

       /*_.REFERENCED_VALUE__V37*/ meltfptr[31] = NULL;;
		    }
		  ;
		  /*^compute */
      /*_#GET_INT__L24*/ meltfnum[19] =
		    (melt_get_int
		     ((melt_ptr_t)
		      ( /*_.REFERENCED_VALUE__V37*/ meltfptr[31])));;
		  /*^compute */
      /*_#I__L25*/ meltfnum[16] =
		    (( /*_#STRBUF_USEDLENGTH__L23*/ meltfnum[20]) <
		     ( /*_#GET_INT__L24*/ meltfnum[19]));;
		  MELT_LOCATION ("warmelt-outobj.melt:3670:/ cond");
		  /*cond */ if ( /*_#I__L25*/ meltfnum[16])	/*then */
		    {
		      /*^cond.then */
		      /*_.IFELSE___V38*/ meltfptr[34] = ( /*nil */ NULL);;
		    }
		  else
		    {
		      MELT_LOCATION ("warmelt-outobj.melt:3670:/ cond.else");

		      /*^block */
		      /*anyblock */
		      {




			{
			  /*^locexp */
			  melt_assert_failed (("check limited implbuf"),
					      ("warmelt-outobj.melt")
					      ? ("warmelt-outobj.melt") :
					      __FILE__,
					      (3670) ? (3670) : __LINE__,
					      __FUNCTION__);
			  ;
			}
			;
		  /*clear *//*_.IFELSE___V38*/ meltfptr[34] = 0;
			/*epilog */
		      }
		      ;
		    }
		  ;
		  /*^compute */
		  /*_.IFCPP___V36*/ meltfptr[30] =
		    /*_.IFELSE___V38*/ meltfptr[34];;
		  /*epilog */

		  MELT_LOCATION ("warmelt-outobj.melt:3670:/ clear");
		/*clear *//*_#STRBUF_USEDLENGTH__L23*/ meltfnum[20] = 0;
		  /*^clear */
		/*clear *//*_.REFERENCED_VALUE__V37*/ meltfptr[31] = 0;
		  /*^clear */
		/*clear *//*_#GET_INT__L24*/ meltfnum[19] = 0;
		  /*^clear */
		/*clear *//*_#I__L25*/ meltfnum[16] = 0;
		  /*^clear */
		/*clear *//*_.IFELSE___V38*/ meltfptr[34] = 0;
		}

#else /*MELT_HAVE_DEBUG */
		/*^cppif.else */
		/*_.IFCPP___V36*/ meltfptr[30] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
		;
		if ( /*_#CURANK__L16*/ meltfnum[15] < 0)
		  break;
	      }			/* end  foreach_in_multiple meltcit1__EACHTUP */

	    /*citerepilog */

	    MELT_LOCATION ("warmelt-outobj.melt:3646:/ clear");
	      /*clear *//*_.CURARG__V23*/ meltfptr[18] = 0;
	    /*^clear */
	      /*clear *//*_#CURANK__L16*/ meltfnum[15] = 0;
	    /*^clear */
	      /*clear *//*_.IFCPP___V36*/ meltfptr[30] = 0;
	  }			/*endciterblock FOREACH_IN_MULTIPLE */
	  ;
   /*_#GET_INT__L26*/ meltfnum[18] =
	    (melt_get_int
	     ((melt_ptr_t) ( /*_.BOXDEPTHP1__V16*/ meltfptr[15])));;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:3673:/ locexp");
	    meltgc_strbuf_add_indent ((melt_ptr_t)
				      ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				      ( /*_#GET_INT__L26*/ meltfnum[18]), 0);
	  }
	  ;
	  MELT_LOCATION ("warmelt-outobj.melt:3644:/ quasiblock");


	  /*epilog */

	  MELT_LOCATION ("warmelt-outobj.melt:3643:/ clear");
	     /*clear *//*_#GET_INT__L26*/ meltfnum[18] = 0;
	}
	;
      }				/*noelse */
    ;
    MELT_LOCATION ("warmelt-outobj.melt:3676:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#I__L27*/ meltfnum[20] =
      (( /*_#NBXRES__L4*/ meltfnum[3]) > (0));;
    MELT_LOCATION ("warmelt-outobj.melt:3676:/ cond");
    /*cond */ if ( /*_#I__L27*/ meltfnum[20])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-outobj.melt:3680:/ quasiblock");


	  /*^newclosure */
		   /*newclosure *//*_.LAMBDA___V41*/ meltfptr[21] =
	    (melt_ptr_t)
	    meltgc_new_closure ((meltobject_ptr_t)
				(((melt_ptr_t)
				  (MELT_PREDEF (DISCR_CLOSURE)))),
				(meltroutine_ptr_t) (( /*!konst_18 */
						      meltfrout->tabval[18])),
				(4));
	  ;
	  /*^putclosedv */
	  /*putclosv */
	  melt_assertmsg ("putclosv checkclo",
			  melt_magic_discr ((melt_ptr_t)
					    ( /*_.LAMBDA___V41*/
					     meltfptr[21])) ==
			  MELTOBMAG_CLOSURE);
	  melt_assertmsg ("putclosv checkoff", 0 >= 0
			  && 0 <
			  melt_closure_size ((melt_ptr_t)
					     ( /*_.LAMBDA___V41*/
					      meltfptr[21])));
	  ((meltclosure_ptr_t) /*_.LAMBDA___V41*/ meltfptr[21])->tabval[0] =
	    (melt_ptr_t) ( /*_.OLOC__V8*/ meltfptr[7]);
	  ;
	  /*^putclosedv */
	  /*putclosv */
	  melt_assertmsg ("putclosv checkclo",
			  melt_magic_discr ((melt_ptr_t)
					    ( /*_.LAMBDA___V41*/
					     meltfptr[21])) ==
			  MELTOBMAG_CLOSURE);
	  melt_assertmsg ("putclosv checkoff", 1 >= 0
			  && 1 <
			  melt_closure_size ((melt_ptr_t)
					     ( /*_.LAMBDA___V41*/
					      meltfptr[21])));
	  ((meltclosure_ptr_t) /*_.LAMBDA___V41*/ meltfptr[21])->tabval[1] =
	    (melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]);
	  ;
	  /*^putclosedv */
	  /*putclosv */
	  melt_assertmsg ("putclosv checkclo",
			  melt_magic_discr ((melt_ptr_t)
					    ( /*_.LAMBDA___V41*/
					     meltfptr[21])) ==
			  MELTOBMAG_CLOSURE);
	  melt_assertmsg ("putclosv checkoff", 2 >= 0
			  && 2 <
			  melt_closure_size ((melt_ptr_t)
					     ( /*_.LAMBDA___V41*/
					      meltfptr[21])));
	  ((meltclosure_ptr_t) /*_.LAMBDA___V41*/ meltfptr[21])->tabval[2] =
	    (melt_ptr_t) ( /*_.BOXDEPTHP1__V16*/ meltfptr[15]);
	  ;
	  /*^putclosedv */
	  /*putclosv */
	  melt_assertmsg ("putclosv checkclo",
			  melt_magic_discr ((melt_ptr_t)
					    ( /*_.LAMBDA___V41*/
					     meltfptr[21])) ==
			  MELTOBMAG_CLOSURE);
	  melt_assertmsg ("putclosv checkoff", 3 >= 0
			  && 3 <
			  melt_closure_size ((melt_ptr_t)
					     ( /*_.LAMBDA___V41*/
					      meltfptr[21])));
	  ((meltclosure_ptr_t) /*_.LAMBDA___V41*/ meltfptr[21])->tabval[3] =
	    (melt_ptr_t) ( /*_.DECLBUF__V3*/ meltfptr[2]);
	  ;
	  /*_.LAMBDA___V40*/ meltfptr[20] = /*_.LAMBDA___V41*/ meltfptr[21];;
	  MELT_LOCATION ("warmelt-outobj.melt:3678:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^apply */
	  /*apply */
	  {
	    union meltparam_un argtab[1];
	    memset (&argtab, 0, sizeof (argtab));
	    /*^apply.arg */
	    argtab[0].meltbp_aptr =
	      (melt_ptr_t *) & /*_.LAMBDA___V40*/ meltfptr[20];
	    /*_.MULTIPLE_EVERY__V42*/ meltfptr[25] =
	      melt_apply ((meltclosure_ptr_t)
			  (( /*!MULTIPLE_EVERY */ meltfrout->tabval[4])),
			  (melt_ptr_t) ( /*_.OXRES__V13*/ meltfptr[12]),
			  (MELTBPARSTR_PTR ""), argtab, "",
			  (union meltparam_un *) 0);
	  }
	  ;
	  MELT_LOCATION ("warmelt-outobj.melt:3677:/ quasiblock");


	  /*_.PROGN___V43*/ meltfptr[27] =
	    /*_.MULTIPLE_EVERY__V42*/ meltfptr[25];;
	  /*^compute */
	  /*_.IF___V39*/ meltfptr[29] = /*_.PROGN___V43*/ meltfptr[27];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-outobj.melt:3676:/ clear");
	     /*clear *//*_.LAMBDA___V40*/ meltfptr[20] = 0;
	  /*^clear */
	     /*clear *//*_.MULTIPLE_EVERY__V42*/ meltfptr[25] = 0;
	  /*^clear */
	     /*clear *//*_.PROGN___V43*/ meltfptr[27] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

  /*_.IF___V39*/ meltfptr[29] = NULL;;
      }
    ;
    /*^compute */
 /*_#GET_INT__L28*/ meltfnum[19] =
      (melt_get_int ((melt_ptr_t) ( /*_.BOXDEPTHP1__V16*/ meltfptr[15])));;
    MELT_LOCATION ("warmelt-outobj.melt:3704:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[3];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.IMPLBUF__V4*/ meltfptr[3];
      /*^apply.arg */
      argtab[1].meltbp_long = /*_#GET_INT__L28*/ meltfnum[19];
      /*^apply.arg */
      argtab[2].meltbp_cstring = "multimsend.send";
      /*_.OUTPUT_LOCATION__V44*/ meltfptr[28] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!OUTPUT_LOCATION */ meltfrout->tabval[3])),
		    (melt_ptr_t) ( /*_.OLOC__V8*/ meltfptr[7]),
		    (MELTBPARSTR_PTR MELTBPARSTR_LONG MELTBPARSTR_CSTRING ""),
		    argtab, "", (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:3708:/ quasiblock");


    /*^newclosure */
		 /*newclosure *//*_.LAMBDA___V46*/ meltfptr[34] =
      (melt_ptr_t)
      meltgc_new_closure ((meltobject_ptr_t)
			  (((melt_ptr_t) (MELT_PREDEF (DISCR_CLOSURE)))),
			  (meltroutine_ptr_t) (( /*!konst_21 */ meltfrout->
						tabval[21])), (3));
    ;
    /*^putclosedv */
    /*putclosv */
    melt_assertmsg ("putclosv checkclo",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.LAMBDA___V46*/ meltfptr[34])) ==
		    MELTOBMAG_CLOSURE);
    melt_assertmsg ("putclosv checkoff", 0 >= 0
		    && 0 <
		    melt_closure_size ((melt_ptr_t)
				       ( /*_.LAMBDA___V46*/ meltfptr[34])));
    ((meltclosure_ptr_t) /*_.LAMBDA___V46*/ meltfptr[34])->tabval[0] =
      (melt_ptr_t) ( /*_.DECLBUF__V3*/ meltfptr[2]);
    ;
    /*^putclosedv */
    /*putclosv */
    melt_assertmsg ("putclosv checkclo",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.LAMBDA___V46*/ meltfptr[34])) ==
		    MELTOBMAG_CLOSURE);
    melt_assertmsg ("putclosv checkoff", 1 >= 0
		    && 1 <
		    melt_closure_size ((melt_ptr_t)
				       ( /*_.LAMBDA___V46*/ meltfptr[34])));
    ((meltclosure_ptr_t) /*_.LAMBDA___V46*/ meltfptr[34])->tabval[1] =
      (melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]);
    ;
    /*^putclosedv */
    /*putclosv */
    melt_assertmsg ("putclosv checkclo",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.LAMBDA___V46*/ meltfptr[34])) ==
		    MELTOBMAG_CLOSURE);
    melt_assertmsg ("putclosv checkoff", 2 >= 0
		    && 2 <
		    melt_closure_size ((melt_ptr_t)
				       ( /*_.LAMBDA___V46*/ meltfptr[34])));
    ((meltclosure_ptr_t) /*_.LAMBDA___V46*/ meltfptr[34])->tabval[2] =
      (melt_ptr_t) ( /*_.BOXDEPTHP1__V16*/ meltfptr[15]);
    ;
    /*_.LAMBDA___V45*/ meltfptr[31] = /*_.LAMBDA___V46*/ meltfptr[34];;
    MELT_LOCATION ("warmelt-outobj.melt:3706:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & /*_.LAMBDA___V45*/ meltfptr[31];
      /*_.LIST_EVERY__V47*/ meltfptr[20] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!LIST_EVERY */ meltfrout->tabval[19])),
		    (melt_ptr_t) ( /*_.ODEST__V9*/ meltfptr[8]),
		    (MELTBPARSTR_PTR ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:3712:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   (" meltgc_send ((melt_ptr_t)("));
    }
    ;
 /*_#I__L29*/ meltfnum[16] =
      ((1) + ( /*_#DEPTH__L1*/ meltfnum[0]));;
    MELT_LOCATION ("warmelt-outobj.melt:3713:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^msend */
    /*msend */
    {
      union meltparam_un argtab[3];
      memset (&argtab, 0, sizeof (argtab));
      /*^ojbmsend.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.DECLBUF__V3*/ meltfptr[2];
      /*^ojbmsend.arg */
      argtab[1].meltbp_aptr = (melt_ptr_t *) & /*_.IMPLBUF__V4*/ meltfptr[3];
      /*^ojbmsend.arg */
      argtab[2].meltbp_long = /*_#I__L29*/ meltfnum[16];
      /*_.OUTPUT_C_CODE__V48*/ meltfptr[25] =
	meltgc_send ((melt_ptr_t) ( /*_.ORECV__V11*/ meltfptr[10]),
		     (melt_ptr_t) (( /*!OUTPUT_C_CODE */ meltfrout->
				    tabval[10])),
		     (MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_LONG ""),
		     argtab, "", (union meltparam_un *) 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:3714:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   ("), ((melt_ptr_t)("));
    }
    ;
 /*_#I__L30*/ meltfnum[18] =
      ((1) + ( /*_#DEPTH__L1*/ meltfnum[0]));;
    MELT_LOCATION ("warmelt-outobj.melt:3716:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^msend */
    /*msend */
    {
      union meltparam_un argtab[3];
      memset (&argtab, 0, sizeof (argtab));
      /*^ojbmsend.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.DECLBUF__V3*/ meltfptr[2];
      /*^ojbmsend.arg */
      argtab[1].meltbp_aptr = (melt_ptr_t *) & /*_.IMPLBUF__V4*/ meltfptr[3];
      /*^ojbmsend.arg */
      argtab[2].meltbp_long = /*_#I__L30*/ meltfnum[18];
      /*_.OUTPUT_C_CODE__V49*/ meltfptr[27] =
	meltgc_send ((melt_ptr_t) ( /*_.OSEL__V10*/ meltfptr[9]),
		     (melt_ptr_t) (( /*!OUTPUT_C_CODE */ meltfrout->
				    tabval[10])),
		     (MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_LONG ""),
		     argtab, "", (union meltparam_un *) 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:3717:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   (")), ("));
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:3721:/ quasiblock");


    /*^newclosure */
		 /*newclosure *//*_.LAMBDA___V51*/ meltfptr[50] =
      (melt_ptr_t)
      meltgc_new_closure ((meltobject_ptr_t)
			  (((melt_ptr_t) (MELT_PREDEF (DISCR_CLOSURE)))),
			  (meltroutine_ptr_t) (( /*!konst_22 */ meltfrout->
						tabval[22])), (1));
    ;
    /*^putclosedv */
    /*putclosv */
    melt_assertmsg ("putclosv checkclo",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.LAMBDA___V51*/ meltfptr[50])) ==
		    MELTOBMAG_CLOSURE);
    melt_assertmsg ("putclosv checkoff", 0 >= 0
		    && 0 <
		    melt_closure_size ((melt_ptr_t)
				       ( /*_.LAMBDA___V51*/ meltfptr[50])));
    ((meltclosure_ptr_t) /*_.LAMBDA___V51*/ meltfptr[50])->tabval[0] =
      (melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]);
    ;
    /*_.LAMBDA___V50*/ meltfptr[49] = /*_.LAMBDA___V51*/ meltfptr[50];;
    MELT_LOCATION ("warmelt-outobj.melt:3719:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & /*_.LAMBDA___V50*/ meltfptr[49];
      /*_.LIST_EVERY__V52*/ meltfptr[51] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!LIST_EVERY */ meltfrout->tabval[19])),
		    (melt_ptr_t) ( /*_.PARAMDESCLIST__V14*/ meltfptr[13]),
		    (MELTBPARSTR_PTR ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:3724:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   ("\"\"), "));
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:3726:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#I__L31*/ meltfnum[30] =
      (( /*_#NBARG__L3*/ meltfnum[1]) > (0));;
    MELT_LOCATION ("warmelt-outobj.melt:3726:/ cond");
    /*cond */ if ( /*_#I__L31*/ meltfnum[30])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{


	  {
	    MELT_LOCATION ("warmelt-outobj.melt:3727:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t)
				 ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				 ("argtab, ("));
	  }
	  ;
	  /*epilog */
	}
	;
      }
    else
      {
	MELT_LOCATION ("warmelt-outobj.melt:3726:/ cond.else");

	/*^block */
	/*anyblock */
	{


	  {
	    MELT_LOCATION ("warmelt-outobj.melt:3728:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t)
				 ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				 ("(union meltparam_un*)0, ("));
	  }
	  ;
	  /*epilog */
	}
	;
      }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:3732:/ quasiblock");


    /*^newclosure */
		 /*newclosure *//*_.LAMBDA___V54*/ meltfptr[53] =
      (melt_ptr_t)
      meltgc_new_closure ((meltobject_ptr_t)
			  (((melt_ptr_t) (MELT_PREDEF (DISCR_CLOSURE)))),
			  (meltroutine_ptr_t) (( /*!konst_23 */ meltfrout->
						tabval[23])), (1));
    ;
    /*^putclosedv */
    /*putclosv */
    melt_assertmsg ("putclosv checkclo",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.LAMBDA___V54*/ meltfptr[53])) ==
		    MELTOBMAG_CLOSURE);
    melt_assertmsg ("putclosv checkoff", 0 >= 0
		    && 0 <
		    melt_closure_size ((melt_ptr_t)
				       ( /*_.LAMBDA___V54*/ meltfptr[53])));
    ((meltclosure_ptr_t) /*_.LAMBDA___V54*/ meltfptr[53])->tabval[0] =
      (melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]);
    ;
    /*_.LAMBDA___V53*/ meltfptr[52] = /*_.LAMBDA___V54*/ meltfptr[53];;
    MELT_LOCATION ("warmelt-outobj.melt:3730:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & /*_.LAMBDA___V53*/ meltfptr[52];
      /*_.LIST_EVERY__V55*/ meltfptr[54] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!LIST_EVERY */ meltfrout->tabval[19])),
		    (melt_ptr_t) ( /*_.RESDESCLIST__V15*/ meltfptr[14]),
		    (MELTBPARSTR_PTR ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:3735:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   ("\"\"), "));
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:3737:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#I__L32*/ meltfnum[31] =
      (( /*_#NBXRES__L4*/ meltfnum[3]) > (0));;
    MELT_LOCATION ("warmelt-outobj.melt:3737:/ cond");
    /*cond */ if ( /*_#I__L32*/ meltfnum[31])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{


	  {
	    MELT_LOCATION ("warmelt-outobj.melt:3738:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t)
				 ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				 ("restab"));
	  }
	  ;
	  /*epilog */
	}
	;
      }
    else
      {
	MELT_LOCATION ("warmelt-outobj.melt:3737:/ cond.else");

	/*^block */
	/*anyblock */
	{


	  {
	    MELT_LOCATION ("warmelt-outobj.melt:3739:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t)
				 ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				 ("(union meltparam_un*)0"));
	  }
	  ;
	  /*epilog */
	}
	;
      }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:3740:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   (");"));
    }
    ;
 /*_#I__L33*/ meltfnum[32] =
      ((1) + ( /*_#DEPTH__L1*/ meltfnum[0]));;

    {
      MELT_LOCATION ("warmelt-outobj.melt:3741:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				( /*_#I__L33*/ meltfnum[32]), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:3742:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   ("}"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:3743:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				( /*_#DEPTH__L1*/ meltfnum[0]), 0);
    }
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:3744:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#STRBUF_USEDLENGTH__L34*/ meltfnum[33] =
	melt_strbuf_usedlength ((melt_ptr_t)
				( /*_.IMPLBUF__V4*/ meltfptr[3]));;
      MELT_LOCATION ("warmelt-outobj.melt:3745:/ cond");
      /*cond */ if (
		     /*ifisa */
		     melt_is_instance_of ((melt_ptr_t)
					  (( /*!BUFFER_LIMIT_CONT */
					    meltfrout->tabval[11])),
					  (melt_ptr_t) (( /*!CLASS_REFERENCE */ meltfrout->tabval[12])))
	)			/*then */
	{
	  /*^cond.then */
	  /*^getslot */
	  {
	    melt_ptr_t slot = NULL, obj = NULL;
	    obj =
	      (melt_ptr_t) (( /*!BUFFER_LIMIT_CONT */ meltfrout->
			     tabval[11])) /*=obj*/ ;
	    melt_object_get_field (slot, obj, 0, "REFERENCED_VALUE");
     /*_.REFERENCED_VALUE__V57*/ meltfptr[56] = slot;
	  };
	  ;
	}
      else
	{			/*^cond.else */

    /*_.REFERENCED_VALUE__V57*/ meltfptr[56] = NULL;;
	}
      ;
      /*^compute */
   /*_#GET_INT__L35*/ meltfnum[34] =
	(melt_get_int
	 ((melt_ptr_t) ( /*_.REFERENCED_VALUE__V57*/ meltfptr[56])));;
      /*^compute */
   /*_#I__L36*/ meltfnum[35] =
	(( /*_#STRBUF_USEDLENGTH__L34*/ meltfnum[33]) <
	 ( /*_#GET_INT__L35*/ meltfnum[34]));;
      MELT_LOCATION ("warmelt-outobj.melt:3744:/ cond");
      /*cond */ if ( /*_#I__L36*/ meltfnum[35])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V58*/ meltfptr[57] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-outobj.melt:3744:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check limited implbuf"),
				  ("warmelt-outobj.melt")
				  ? ("warmelt-outobj.melt") : __FILE__,
				  (3744) ? (3744) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V58*/ meltfptr[57] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V56*/ meltfptr[55] = /*_.IFELSE___V58*/ meltfptr[57];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:3744:/ clear");
	     /*clear *//*_#STRBUF_USEDLENGTH__L34*/ meltfnum[33] = 0;
      /*^clear */
	     /*clear *//*_.REFERENCED_VALUE__V57*/ meltfptr[56] = 0;
      /*^clear */
	     /*clear *//*_#GET_INT__L35*/ meltfnum[34] = 0;
      /*^clear */
	     /*clear *//*_#I__L36*/ meltfnum[35] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V58*/ meltfptr[57] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V56*/ meltfptr[55] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    /*^compute */
    /*_.LET___V7*/ meltfptr[5] = /*_.IFCPP___V56*/ meltfptr[55];;

    MELT_LOCATION ("warmelt-outobj.melt:3599:/ clear");
	   /*clear *//*_.OLOC__V8*/ meltfptr[7] = 0;
    /*^clear */
	   /*clear *//*_.ODEST__V9*/ meltfptr[8] = 0;
    /*^clear */
	   /*clear *//*_.OSEL__V10*/ meltfptr[9] = 0;
    /*^clear */
	   /*clear *//*_.ORECV__V11*/ meltfptr[10] = 0;
    /*^clear */
	   /*clear *//*_.OARGS__V12*/ meltfptr[11] = 0;
    /*^clear */
	   /*clear *//*_.OXRES__V13*/ meltfptr[12] = 0;
    /*^clear */
	   /*clear *//*_#NBARG__L3*/ meltfnum[1] = 0;
    /*^clear */
	   /*clear *//*_#NBXRES__L4*/ meltfnum[3] = 0;
    /*^clear */
	   /*clear *//*_.PARAMDESCLIST__V14*/ meltfptr[13] = 0;
    /*^clear */
	   /*clear *//*_.RESDESCLIST__V15*/ meltfptr[14] = 0;
    /*^clear */
	   /*clear *//*_#I__L5*/ meltfnum[4] = 0;
    /*^clear */
	   /*clear *//*_.BOXDEPTHP1__V16*/ meltfptr[15] = 0;
    /*^clear */
	   /*clear *//*_.OUTPUT_LOCATION__V17*/ meltfptr[16] = 0;
    /*^clear */
	   /*clear *//*_#I__L6*/ meltfnum[5] = 0;
    /*^clear */
	   /*clear *//*_#I__L7*/ meltfnum[6] = 0;
    /*^clear */
	   /*clear *//*_#I__L9*/ meltfnum[7] = 0;
    /*^clear */
	   /*clear *//*_.IF___V18*/ meltfptr[17] = 0;
    /*^clear */
	   /*clear *//*_#I__L11*/ meltfnum[9] = 0;
    /*^clear */
	   /*clear *//*_#I__L13*/ meltfnum[11] = 0;
    /*^clear */
	   /*clear *//*_#I__L15*/ meltfnum[13] = 0;
    /*^clear */
	   /*clear *//*_#I__L27*/ meltfnum[20] = 0;
    /*^clear */
	   /*clear *//*_.IF___V39*/ meltfptr[29] = 0;
    /*^clear */
	   /*clear *//*_#GET_INT__L28*/ meltfnum[19] = 0;
    /*^clear */
	   /*clear *//*_.OUTPUT_LOCATION__V44*/ meltfptr[28] = 0;
    /*^clear */
	   /*clear *//*_.LAMBDA___V45*/ meltfptr[31] = 0;
    /*^clear */
	   /*clear *//*_.LIST_EVERY__V47*/ meltfptr[20] = 0;
    /*^clear */
	   /*clear *//*_#I__L29*/ meltfnum[16] = 0;
    /*^clear */
	   /*clear *//*_.OUTPUT_C_CODE__V48*/ meltfptr[25] = 0;
    /*^clear */
	   /*clear *//*_#I__L30*/ meltfnum[18] = 0;
    /*^clear */
	   /*clear *//*_.OUTPUT_C_CODE__V49*/ meltfptr[27] = 0;
    /*^clear */
	   /*clear *//*_.LAMBDA___V50*/ meltfptr[49] = 0;
    /*^clear */
	   /*clear *//*_.LIST_EVERY__V52*/ meltfptr[51] = 0;
    /*^clear */
	   /*clear *//*_#I__L31*/ meltfnum[30] = 0;
    /*^clear */
	   /*clear *//*_.LAMBDA___V53*/ meltfptr[52] = 0;
    /*^clear */
	   /*clear *//*_.LIST_EVERY__V55*/ meltfptr[54] = 0;
    /*^clear */
	   /*clear *//*_#I__L32*/ meltfnum[31] = 0;
    /*^clear */
	   /*clear *//*_#I__L33*/ meltfnum[32] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V56*/ meltfptr[55] = 0;
    MELT_LOCATION ("warmelt-outobj.melt:3597:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.LET___V7*/ meltfptr[5];;

    {
      MELT_LOCATION ("warmelt-outobj.melt:3597:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V5*/ meltfptr[4] = 0;
    /*^clear */
	   /*clear *//*_.LET___V7*/ meltfptr[5] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("OUTPUCOD_OBJMULTIMSEND", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_89_warmelt_outobj_OUTPUCOD_OBJMULTIMSEND_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_89_warmelt_outobj_OUTPUCOD_OBJMULTIMSEND */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_90_warmelt_outobj_LAMBDA___19__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un * meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un * meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_90_warmelt_outobj_LAMBDA___19___melt =
    melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_90_warmelt_outobj_LAMBDA___19___st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 4
    melt_ptr_t mcfr_varptr[4];
#define MELTFRAM_NBVARNUM 1
    long mcfr_varnum[1];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_90_warmelt_outobj_LAMBDA___19__ is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_90_warmelt_outobj_LAMBDA___19___st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 4; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_90_warmelt_outobj_LAMBDA___19__ nbval 4*/
  meltfram__.mcfr_nbvar = 4 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("LAMBDA_", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-outobj.melt:3630:/ getarg");
 /*_.CURES__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_LONG)
    goto lab_endgetargs;
 /*_#CURANK__L1*/ meltfnum[0] = meltxargtab_[0].meltbp_long;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {

    MELT_LOCATION ("warmelt-outobj.melt:3631:/ quasiblock");


    /*^checksignal */
    MELT_CHECK_SIGNAL ();
    ;
    /*^msend */
    /*msend */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^ojbmsend.arg */
      argtab[0].meltbp_aptr = /*nil */ (melt_ptr_t *) NULL;
      /*_.CURESTYP__V3*/ meltfptr[2] =
	meltgc_send ((melt_ptr_t) ( /*_.CURES__V2*/ meltfptr[1]),
		     (melt_ptr_t) (( /*!GET_CTYPE */ meltfrout->tabval[0])),
		     (MELTBPARSTR_PTR ""), argtab, "",
		     (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:3632:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.CURESTYP__V3*/ meltfptr[2]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 5, "CTYPE_PARSTRING");
  /*_.CTYPE_PARSTRING__V4*/ meltfptr[3] = slot;
    };
    ;

    {
      /*^locexp */
      meltgc_append_list ((melt_ptr_t)
			  (( /*~RESDESCLIST */ meltfclos->tabval[0])),
			  (melt_ptr_t) ( /*_.CTYPE_PARSTRING__V4*/
					meltfptr[3]));
    }
    ;

    MELT_LOCATION ("warmelt-outobj.melt:3631:/ clear");
	   /*clear *//*_.CURESTYP__V3*/ meltfptr[2] = 0;
    /*^clear */
	   /*clear *//*_.CTYPE_PARSTRING__V4*/ meltfptr[3] = 0;
    /*epilog */
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("LAMBDA_", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_90_warmelt_outobj_LAMBDA___19___melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*noretval */ NULL);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_90_warmelt_outobj_LAMBDA___19__ */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_91_warmelt_outobj_LAMBDA___20__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un * meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un * meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_91_warmelt_outobj_LAMBDA___20___melt =
    melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_91_warmelt_outobj_LAMBDA___20___st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 13
    melt_ptr_t mcfr_varptr[13];
#define MELTFRAM_NBVARNUM 7
    long mcfr_varnum[7];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_91_warmelt_outobj_LAMBDA___20__ is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_91_warmelt_outobj_LAMBDA___20___st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 13; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_91_warmelt_outobj_LAMBDA___20__ nbval 13*/
  meltfram__.mcfr_nbvar = 13 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("LAMBDA_", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-outobj.melt:3680:/ getarg");
 /*_.CURES__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_LONG)
    goto lab_endgetargs;
 /*_#CURANK__L1*/ meltfnum[0] = meltxargtab_[0].meltbp_long;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {

    MELT_LOCATION ("warmelt-outobj.melt:3681:/ quasiblock");


    /*^checksignal */
    MELT_CHECK_SIGNAL ();
    ;
    /*^msend */
    /*msend */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^ojbmsend.arg */
      argtab[0].meltbp_aptr = /*nil */ (melt_ptr_t *) NULL;
      /*_.CURESTYP__V3*/ meltfptr[2] =
	meltgc_send ((melt_ptr_t) ( /*_.CURES__V2*/ meltfptr[1]),
		     (melt_ptr_t) (( /*!GET_CTYPE */ meltfrout->tabval[0])),
		     (MELTBPARSTR_PTR ""), argtab, "",
		     (union meltparam_un *) 0);
    }
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:3682:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L2*/ meltfnum[1] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.CURESTYP__V3*/ meltfptr[2]),
			     (melt_ptr_t) (( /*!CLASS_CTYPE */ meltfrout->
					    tabval[1])));;
      MELT_LOCATION ("warmelt-outobj.melt:3682:/ cond");
      /*cond */ if ( /*_#IS_A__L2*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V5*/ meltfptr[4] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-outobj.melt:3682:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check curestyp"),
				  ("warmelt-outobj.melt")
				  ? ("warmelt-outobj.melt") : __FILE__,
				  (3682) ? (3682) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V5*/ meltfptr[4] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V4*/ meltfptr[3] = /*_.IFELSE___V5*/ meltfptr[4];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:3682:/ clear");
	     /*clear *//*_#IS_A__L2*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V5*/ meltfptr[4] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V4*/ meltfptr[3] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    /*^compute */
 /*_#GET_INT__L3*/ meltfnum[1] =
      (melt_get_int
       ((melt_ptr_t) (( /*~BOXDEPTHP1 */ meltfclos->tabval[2]))));;
    MELT_LOCATION ("warmelt-outobj.melt:3683:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[3];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & ( /*~IMPLBUF */ meltfclos->tabval[1]);
      /*^apply.arg */
      argtab[1].meltbp_long = /*_#GET_INT__L3*/ meltfnum[1];
      /*^apply.arg */
      argtab[2].meltbp_cstring = "multimsend.xres";
      /*_.OUTPUT_LOCATION__V6*/ meltfptr[4] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!OUTPUT_LOCATION */ meltfrout->tabval[2])),
		    (melt_ptr_t) (( /*~OLOC */ meltfclos->tabval[0])),
		    (MELTBPARSTR_PTR MELTBPARSTR_LONG MELTBPARSTR_CSTRING ""),
		    argtab, "", (union meltparam_un *) 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:3684:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t)
			   (( /*~IMPLBUF */ meltfclos->tabval[1])),
			   ("restab["));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:3685:/ locexp");
      meltgc_add_strbuf_dec ((melt_ptr_t)
			     (( /*~IMPLBUF */ meltfclos->tabval[1])),
			     ( /*_#CURANK__L1*/ meltfnum[0]));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:3686:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t)
			   (( /*~IMPLBUF */ meltfclos->tabval[1])), ("]."));
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:3688:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#NULL__L4*/ meltfnum[3] =
      (( /*_.CURES__V2*/ meltfptr[1]) == NULL);;
    MELT_LOCATION ("warmelt-outobj.melt:3688:/ cond");
    /*cond */ if ( /*_#NULL__L4*/ meltfnum[3])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{




	  {
	    MELT_LOCATION ("warmelt-outobj.melt:3689:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t)
				 (( /*~IMPLBUF */ meltfclos->tabval[1])),
				 ("meltbp_aptr = (melt_ptr_t*)NULL"));
	  }
	  ;
	     /*clear *//*_.IFELSE___V7*/ meltfptr[6] = 0;
	  /*epilog */
	}
	;
      }
    else
      {
	MELT_LOCATION ("warmelt-outobj.melt:3688:/ cond.else");

	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-outobj.melt:3691:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
   /*_#__L5*/ meltfnum[4] =
	    (( /*_.CURESTYP__V3*/ meltfptr[2]) ==
	     (( /*!CTYPE_VALUE */ meltfrout->tabval[3])));;
	  MELT_LOCATION ("warmelt-outobj.melt:3691:/ cond");
	  /*cond */ if ( /*_#__L5*/ meltfnum[4])	/*then */
	    {
	      /*^cond.then */
	      /*^block */
	      /*anyblock */
	      {


		{
		  MELT_LOCATION ("warmelt-outobj.melt:3692:/ locexp");
		  /*add2sbuf_strconst */
		    meltgc_add_strbuf ((melt_ptr_t)
				       (( /*~IMPLBUF */ meltfclos->
					 tabval[1])),
				       ("meltbp_aptr = (melt_ptr_t*) &"));
		}
		;
     /*_#GET_INT__L6*/ meltfnum[5] =
		  (melt_get_int
		   ((melt_ptr_t)
		    (( /*~BOXDEPTHP1 */ meltfclos->tabval[2]))));;
		MELT_LOCATION ("warmelt-outobj.melt:3693:/ checksignal");
		MELT_CHECK_SIGNAL ();
		;
		/*^msend */
		/*msend */
		{
		  union meltparam_un argtab[3];
		  memset (&argtab, 0, sizeof (argtab));
		  /*^ojbmsend.arg */
		  argtab[0].meltbp_aptr =
		    (melt_ptr_t *) & ( /*~DECLBUF */ meltfclos->tabval[3]);
		  /*^ojbmsend.arg */
		  argtab[1].meltbp_aptr =
		    (melt_ptr_t *) & ( /*~IMPLBUF */ meltfclos->tabval[1]);
		  /*^ojbmsend.arg */
		  argtab[2].meltbp_long = /*_#GET_INT__L6*/ meltfnum[5];
		  /*_.OUTPUT_C_CODE__V9*/ meltfptr[8] =
		    meltgc_send ((melt_ptr_t) ( /*_.CURES__V2*/ meltfptr[1]),
				 (melt_ptr_t) (( /*!OUTPUT_C_CODE */
						meltfrout->tabval[4])),
				 (MELTBPARSTR_PTR MELTBPARSTR_PTR
				  MELTBPARSTR_LONG ""), argtab, "",
				 (union meltparam_un *) 0);
		}
		;
		MELT_LOCATION ("warmelt-outobj.melt:3691:/ quasiblock");


		/*_.PROGN___V10*/ meltfptr[9] =
		  /*_.OUTPUT_C_CODE__V9*/ meltfptr[8];;
		/*^compute */
		/*_.IFELSE___V8*/ meltfptr[7] =
		  /*_.PROGN___V10*/ meltfptr[9];;
		/*epilog */

		MELT_LOCATION ("warmelt-outobj.melt:3691:/ clear");
	       /*clear *//*_#GET_INT__L6*/ meltfnum[5] = 0;
		/*^clear */
	       /*clear *//*_.OUTPUT_C_CODE__V9*/ meltfptr[8] = 0;
		/*^clear */
	       /*clear *//*_.PROGN___V10*/ meltfptr[9] = 0;
	      }
	      ;
	    }
	  else
	    {			/*^cond.else */

	      /*^block */
	      /*anyblock */
	      {

		MELT_LOCATION ("warmelt-outobj.melt:3696:/ getslot");
		{
		  melt_ptr_t slot = NULL, obj = NULL;
		  obj =
		    (melt_ptr_t) ( /*_.CURESTYP__V3*/ meltfptr[2]) /*=obj*/ ;
		  melt_object_get_field (slot, obj, 7, "CTYPE_RESFIELD");
      /*_.CTYPE_RESFIELD__V11*/ meltfptr[8] = slot;
		};
		;

		{
		  /*^locexp */
		  /*add2sbuf_string */
		    meltgc_add_strbuf ((melt_ptr_t)
				       (( /*~IMPLBUF */ meltfclos->
					 tabval[1])),
				       melt_string_str ((melt_ptr_t)
							( /*_.CTYPE_RESFIELD__V11*/ meltfptr[8])));
		}
		;

		{
		  MELT_LOCATION ("warmelt-outobj.melt:3697:/ locexp");
		  /*add2sbuf_strconst */
		    meltgc_add_strbuf ((melt_ptr_t)
				       (( /*~IMPLBUF */ meltfclos->
					 tabval[1])), (" = "));
		}
		;
     /*_#GET_INT__L7*/ meltfnum[5] =
		  (melt_get_int
		   ((melt_ptr_t)
		    (( /*~BOXDEPTHP1 */ meltfclos->tabval[2]))));;
		MELT_LOCATION ("warmelt-outobj.melt:3698:/ checksignal");
		MELT_CHECK_SIGNAL ();
		;
		/*^msend */
		/*msend */
		{
		  union meltparam_un argtab[3];
		  memset (&argtab, 0, sizeof (argtab));
		  /*^ojbmsend.arg */
		  argtab[0].meltbp_aptr =
		    (melt_ptr_t *) & ( /*~DECLBUF */ meltfclos->tabval[3]);
		  /*^ojbmsend.arg */
		  argtab[1].meltbp_aptr =
		    (melt_ptr_t *) & ( /*~IMPLBUF */ meltfclos->tabval[1]);
		  /*^ojbmsend.arg */
		  argtab[2].meltbp_long = /*_#GET_INT__L7*/ meltfnum[5];
		  /*_.OUTPUT_C_CODE__V12*/ meltfptr[9] =
		    meltgc_send ((melt_ptr_t) ( /*_.CURES__V2*/ meltfptr[1]),
				 (melt_ptr_t) (( /*!OUTPUT_C_CODE */
						meltfrout->tabval[4])),
				 (MELTBPARSTR_PTR MELTBPARSTR_PTR
				  MELTBPARSTR_LONG ""), argtab, "",
				 (union meltparam_un *) 0);
		}
		;
		MELT_LOCATION ("warmelt-outobj.melt:3695:/ quasiblock");


		/*_.PROGN___V13*/ meltfptr[12] =
		  /*_.OUTPUT_C_CODE__V12*/ meltfptr[9];;
		/*^compute */
		/*_.IFELSE___V8*/ meltfptr[7] =
		  /*_.PROGN___V13*/ meltfptr[12];;
		/*epilog */

		MELT_LOCATION ("warmelt-outobj.melt:3691:/ clear");
	       /*clear *//*_.CTYPE_RESFIELD__V11*/ meltfptr[8] = 0;
		/*^clear */
	       /*clear *//*_#GET_INT__L7*/ meltfnum[5] = 0;
		/*^clear */
	       /*clear *//*_.OUTPUT_C_CODE__V12*/ meltfptr[9] = 0;
		/*^clear */
	       /*clear *//*_.PROGN___V13*/ meltfptr[12] = 0;
	      }
	      ;
	    }
	  ;
	  /*_.IFELSE___V7*/ meltfptr[6] = /*_.IFELSE___V8*/ meltfptr[7];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-outobj.melt:3688:/ clear");
	     /*clear *//*_#__L5*/ meltfnum[4] = 0;
	  /*^clear */
	     /*clear *//*_.IFELSE___V8*/ meltfptr[7] = 0;
	}
	;
      }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:3700:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t)
			   (( /*~IMPLBUF */ meltfclos->tabval[1])), (";"));
    }
    ;

    MELT_LOCATION ("warmelt-outobj.melt:3681:/ clear");
	   /*clear *//*_.CURESTYP__V3*/ meltfptr[2] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V4*/ meltfptr[3] = 0;
    /*^clear */
	   /*clear *//*_#GET_INT__L3*/ meltfnum[1] = 0;
    /*^clear */
	   /*clear *//*_.OUTPUT_LOCATION__V6*/ meltfptr[4] = 0;
    /*^clear */
	   /*clear *//*_#NULL__L4*/ meltfnum[3] = 0;
    /*^clear */
	   /*clear *//*_.IFELSE___V7*/ meltfptr[6] = 0;
    /*epilog */
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("LAMBDA_", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_91_warmelt_outobj_LAMBDA___20___melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*noretval */ NULL);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_91_warmelt_outobj_LAMBDA___20__ */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_92_warmelt_outobj_LAMBDA___21__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un * meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un * meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_92_warmelt_outobj_LAMBDA___21___melt =
    melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_92_warmelt_outobj_LAMBDA___21___st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 3
    melt_ptr_t mcfr_varptr[3];
#define MELTFRAM_NBVARNUM 1
    long mcfr_varnum[1];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_92_warmelt_outobj_LAMBDA___21__ is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_92_warmelt_outobj_LAMBDA___21___st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 3; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_92_warmelt_outobj_LAMBDA___21__ nbval 3*/
  meltfram__.mcfr_nbvar = 3 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("LAMBDA_", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-outobj.melt:3708:/ getarg");
 /*_.CURDEST__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {

 /*_#GET_INT__L1*/ meltfnum[0] =
      (melt_get_int
       ((melt_ptr_t) (( /*~BOXDEPTHP1 */ meltfclos->tabval[2]))));;
    MELT_LOCATION ("warmelt-outobj.melt:3709:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^msend */
    /*msend */
    {
      union meltparam_un argtab[3];
      memset (&argtab, 0, sizeof (argtab));
      /*^ojbmsend.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & ( /*~DECLBUF */ meltfclos->tabval[0]);
      /*^ojbmsend.arg */
      argtab[1].meltbp_aptr =
	(melt_ptr_t *) & ( /*~IMPLBUF */ meltfclos->tabval[1]);
      /*^ojbmsend.arg */
      argtab[2].meltbp_long = /*_#GET_INT__L1*/ meltfnum[0];
      /*_.OUTPUT_C_CODE__V3*/ meltfptr[2] =
	meltgc_send ((melt_ptr_t) ( /*_.CURDEST__V2*/ meltfptr[1]),
		     (melt_ptr_t) (( /*!OUTPUT_C_CODE */ meltfrout->
				    tabval[0])),
		     (MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_LONG ""),
		     argtab, "", (union meltparam_un *) 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:3710:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t)
			   (( /*~IMPLBUF */ meltfclos->tabval[1])), (" = "));
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:3708:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*epilog */

    /*^clear */
	   /*clear *//*_#GET_INT__L1*/ meltfnum[0] = 0;
    /*^clear */
	   /*clear *//*_.OUTPUT_C_CODE__V3*/ meltfptr[2] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("LAMBDA_", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_92_warmelt_outobj_LAMBDA___21___melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*noretval */ NULL);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_92_warmelt_outobj_LAMBDA___21__ */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_93_warmelt_outobj_LAMBDA___22__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un * meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un * meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_93_warmelt_outobj_LAMBDA___22___melt =
    melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_93_warmelt_outobj_LAMBDA___22___st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 2
    melt_ptr_t mcfr_varptr[2];
/*no varnum*/
#define MELTFRAM_NBVARNUM /*none*/0
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_93_warmelt_outobj_LAMBDA___22__ is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_93_warmelt_outobj_LAMBDA___22___st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 2; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_93_warmelt_outobj_LAMBDA___22__ nbval 2*/
  meltfram__.mcfr_nbvar = 2 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("LAMBDA_", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-outobj.melt:3721:/ getarg");
 /*_.PARD__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


    {
      MELT_LOCATION ("warmelt-outobj.melt:3722:/ locexp");
      /*add2sbuf_string */
	meltgc_add_strbuf ((melt_ptr_t)
			   (( /*~IMPLBUF */ meltfclos->tabval[0])),
			   melt_string_str ((melt_ptr_t)
					    ( /*_.PARD__V2*/ meltfptr[1])));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:3723:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t)
			   (( /*~IMPLBUF */ meltfclos->tabval[0])), (" "));
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:3721:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*epilog */
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("LAMBDA_", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_93_warmelt_outobj_LAMBDA___22___melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*noretval */ NULL);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_93_warmelt_outobj_LAMBDA___22__ */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_94_warmelt_outobj_LAMBDA___23__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un * meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un * meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_94_warmelt_outobj_LAMBDA___23___melt =
    melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_94_warmelt_outobj_LAMBDA___23___st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 2
    melt_ptr_t mcfr_varptr[2];
/*no varnum*/
#define MELTFRAM_NBVARNUM /*none*/0
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_94_warmelt_outobj_LAMBDA___23__ is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_94_warmelt_outobj_LAMBDA___23___st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 2; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_94_warmelt_outobj_LAMBDA___23__ nbval 2*/
  meltfram__.mcfr_nbvar = 2 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("LAMBDA_", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-outobj.melt:3732:/ getarg");
 /*_.RESD__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


    {
      MELT_LOCATION ("warmelt-outobj.melt:3733:/ locexp");
      /*add2sbuf_string */
	meltgc_add_strbuf ((melt_ptr_t)
			   (( /*~IMPLBUF */ meltfclos->tabval[0])),
			   melt_string_str ((melt_ptr_t)
					    ( /*_.RESD__V2*/ meltfptr[1])));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:3734:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t)
			   (( /*~IMPLBUF */ meltfclos->tabval[0])), (" "));
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:3732:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*epilog */
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("LAMBDA_", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_94_warmelt_outobj_LAMBDA___23___melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*noretval */ NULL);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_94_warmelt_outobj_LAMBDA___23__ */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_95_warmelt_outobj_OUTPUCOD_OBJCLEAR (meltclosure_ptr_t meltclosp_,
					      melt_ptr_t meltfirstargp_,
					      const melt_argdescr_cell_t
					      meltxargdescr_[],
					      union meltparam_un *
					      meltxargtab_,
					      const melt_argdescr_cell_t
					      meltxresdescr_[],
					      union meltparam_un *
					      meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_95_warmelt_outobj_OUTPUCOD_OBJCLEAR_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_95_warmelt_outobj_OUTPUCOD_OBJCLEAR_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 14
    melt_ptr_t mcfr_varptr[14];
#define MELTFRAM_NBVARNUM 6
    long mcfr_varnum[6];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_95_warmelt_outobj_OUTPUCOD_OBJCLEAR is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_95_warmelt_outobj_OUTPUCOD_OBJCLEAR_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 14; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_95_warmelt_outobj_OUTPUCOD_OBJCLEAR nbval 14*/
  meltfram__.mcfr_nbvar = 14 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("OUTPUCOD_OBJCLEAR", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-outobj.melt:3752:/ getarg");
 /*_.OCLEAR__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.DECLBUF__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.DECLBUF__V3*/ meltfptr[2])) !=
	      NULL);


  /*getarg#2 */
  /*^getarg */
  if (meltxargdescr_[1] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.IMPLBUF__V4*/ meltfptr[3] =
    (meltxargtab_[1].meltbp_aptr) ? (*(meltxargtab_[1].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3])) !=
	      NULL);


  /*getarg#3 */
  /*^getarg */
  if (meltxargdescr_[2] != MELTBPAR_LONG)
    goto lab_endgetargs;
 /*_#DEPTH__L1*/ meltfnum[0] = meltxargtab_[2].meltbp_long;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:3753:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L2*/ meltfnum[1] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.OCLEAR__V2*/ meltfptr[1]),
			     (melt_ptr_t) (( /*!CLASS_OBJCLEAR */ meltfrout->
					    tabval[0])));;
      MELT_LOCATION ("warmelt-outobj.melt:3753:/ cond");
      /*cond */ if ( /*_#IS_A__L2*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V6*/ meltfptr[5] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-outobj.melt:3753:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check oclear"),
				  ("warmelt-outobj.melt")
				  ? ("warmelt-outobj.melt") : __FILE__,
				  (3753) ? (3753) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V6*/ meltfptr[5] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V5*/ meltfptr[4] = /*_.IFELSE___V6*/ meltfptr[5];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:3753:/ clear");
	     /*clear *//*_#IS_A__L2*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V6*/ meltfptr[5] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V5*/ meltfptr[4] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-outobj.melt:3754:/ quasiblock");


    /*^getslot */
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.OCLEAR__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 0, "OBI_LOC");
  /*_.CLOC__V8*/ meltfptr[7] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-outobj.melt:3755:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.OCLEAR__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 1, "OCLR_VLOC");
  /*_.CVL__V9*/ meltfptr[8] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-outobj.melt:3757:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[3];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.IMPLBUF__V4*/ meltfptr[3];
      /*^apply.arg */
      argtab[1].meltbp_long = /*_#DEPTH__L1*/ meltfnum[0];
      /*^apply.arg */
      argtab[2].meltbp_cstring = "clear";
      /*_.OUTPUT_LOCATION__V10*/ meltfptr[9] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!OUTPUT_LOCATION */ meltfrout->tabval[1])),
		    (melt_ptr_t) ( /*_.CLOC__V8*/ meltfptr[7]),
		    (MELTBPARSTR_PTR MELTBPARSTR_LONG MELTBPARSTR_CSTRING ""),
		    argtab, "", (union meltparam_un *) 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:3758:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   ("/*clear*/ "));
    }
    ;
 /*_#I__L3*/ meltfnum[1] =
      (( /*_#DEPTH__L1*/ meltfnum[0]) + (1));;
    MELT_LOCATION ("warmelt-outobj.melt:3759:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^msend */
    /*msend */
    {
      union meltparam_un argtab[3];
      memset (&argtab, 0, sizeof (argtab));
      /*^ojbmsend.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.DECLBUF__V3*/ meltfptr[2];
      /*^ojbmsend.arg */
      argtab[1].meltbp_aptr = (melt_ptr_t *) & /*_.IMPLBUF__V4*/ meltfptr[3];
      /*^ojbmsend.arg */
      argtab[2].meltbp_long = /*_#I__L3*/ meltfnum[1];
      /*_.OUTPUT_C_CODE__V11*/ meltfptr[10] =
	meltgc_send ((melt_ptr_t) ( /*_.CVL__V9*/ meltfptr[8]),
		     (melt_ptr_t) (( /*!OUTPUT_C_CODE */ meltfrout->
				    tabval[2])),
		     (MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_LONG ""),
		     argtab, "", (union meltparam_un *) 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:3760:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   (" = 0 "));
    }
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:3761:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#STRBUF_USEDLENGTH__L4*/ meltfnum[3] =
	melt_strbuf_usedlength ((melt_ptr_t)
				( /*_.IMPLBUF__V4*/ meltfptr[3]));;
      MELT_LOCATION ("warmelt-outobj.melt:3762:/ cond");
      /*cond */ if (
		     /*ifisa */
		     melt_is_instance_of ((melt_ptr_t)
					  (( /*!BUFFER_LIMIT_CONT */
					    meltfrout->tabval[3])),
					  (melt_ptr_t) (( /*!CLASS_REFERENCE */ meltfrout->tabval[4])))
	)			/*then */
	{
	  /*^cond.then */
	  /*^getslot */
	  {
	    melt_ptr_t slot = NULL, obj = NULL;
	    obj =
	      (melt_ptr_t) (( /*!BUFFER_LIMIT_CONT */ meltfrout->
			     tabval[3])) /*=obj*/ ;
	    melt_object_get_field (slot, obj, 0, "REFERENCED_VALUE");
     /*_.REFERENCED_VALUE__V13*/ meltfptr[12] = slot;
	  };
	  ;
	}
      else
	{			/*^cond.else */

    /*_.REFERENCED_VALUE__V13*/ meltfptr[12] = NULL;;
	}
      ;
      /*^compute */
   /*_#GET_INT__L5*/ meltfnum[4] =
	(melt_get_int
	 ((melt_ptr_t) ( /*_.REFERENCED_VALUE__V13*/ meltfptr[12])));;
      /*^compute */
   /*_#I__L6*/ meltfnum[5] =
	(( /*_#STRBUF_USEDLENGTH__L4*/ meltfnum[3]) <
	 ( /*_#GET_INT__L5*/ meltfnum[4]));;
      MELT_LOCATION ("warmelt-outobj.melt:3761:/ cond");
      /*cond */ if ( /*_#I__L6*/ meltfnum[5])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V14*/ meltfptr[13] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-outobj.melt:3761:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check limited implbuf"),
				  ("warmelt-outobj.melt")
				  ? ("warmelt-outobj.melt") : __FILE__,
				  (3761) ? (3761) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V14*/ meltfptr[13] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V12*/ meltfptr[11] = /*_.IFELSE___V14*/ meltfptr[13];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:3761:/ clear");
	     /*clear *//*_#STRBUF_USEDLENGTH__L4*/ meltfnum[3] = 0;
      /*^clear */
	     /*clear *//*_.REFERENCED_VALUE__V13*/ meltfptr[12] = 0;
      /*^clear */
	     /*clear *//*_#GET_INT__L5*/ meltfnum[4] = 0;
      /*^clear */
	     /*clear *//*_#I__L6*/ meltfnum[5] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V14*/ meltfptr[13] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V12*/ meltfptr[11] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    /*^compute */
    /*_.LET___V7*/ meltfptr[5] = /*_.IFCPP___V12*/ meltfptr[11];;

    MELT_LOCATION ("warmelt-outobj.melt:3754:/ clear");
	   /*clear *//*_.CLOC__V8*/ meltfptr[7] = 0;
    /*^clear */
	   /*clear *//*_.CVL__V9*/ meltfptr[8] = 0;
    /*^clear */
	   /*clear *//*_.OUTPUT_LOCATION__V10*/ meltfptr[9] = 0;
    /*^clear */
	   /*clear *//*_#I__L3*/ meltfnum[1] = 0;
    /*^clear */
	   /*clear *//*_.OUTPUT_C_CODE__V11*/ meltfptr[10] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V12*/ meltfptr[11] = 0;
    MELT_LOCATION ("warmelt-outobj.melt:3752:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.LET___V7*/ meltfptr[5];;

    {
      MELT_LOCATION ("warmelt-outobj.melt:3752:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V5*/ meltfptr[4] = 0;
    /*^clear */
	   /*clear *//*_.LET___V7*/ meltfptr[5] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("OUTPUCOD_OBJCLEAR", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_95_warmelt_outobj_OUTPUCOD_OBJCLEAR_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_95_warmelt_outobj_OUTPUCOD_OBJCLEAR */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_96_warmelt_outobj_OUTPUCOD_OBJRAWALLOCOBJ (meltclosure_ptr_t
						    meltclosp_,
						    melt_ptr_t meltfirstargp_,
						    const melt_argdescr_cell_t
						    meltxargdescr_[],
						    union meltparam_un *
						    meltxargtab_,
						    const melt_argdescr_cell_t
						    meltxresdescr_[],
						    union meltparam_un *
						    meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_96_warmelt_outobj_OUTPUCOD_OBJRAWALLOCOBJ_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_96_warmelt_outobj_OUTPUCOD_OBJRAWALLOCOBJ_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 26
    melt_ptr_t mcfr_varptr[26];
#define MELTFRAM_NBVARNUM 16
    long mcfr_varnum[16];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_96_warmelt_outobj_OUTPUCOD_OBJRAWALLOCOBJ is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_96_warmelt_outobj_OUTPUCOD_OBJRAWALLOCOBJ_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 26; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_96_warmelt_outobj_OUTPUCOD_OBJRAWALLOCOBJ nbval 26*/
  meltfram__.mcfr_nbvar = 26 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("OUTPUCOD_OBJRAWALLOCOBJ", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-outobj.melt:3768:/ getarg");
 /*_.ORALOB__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.DECLBUF__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.DECLBUF__V3*/ meltfptr[2])) !=
	      NULL);


  /*getarg#2 */
  /*^getarg */
  if (meltxargdescr_[1] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.IMPLBUF__V4*/ meltfptr[3] =
    (meltxargtab_[1].meltbp_aptr) ? (*(meltxargtab_[1].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3])) !=
	      NULL);


  /*getarg#3 */
  /*^getarg */
  if (meltxargdescr_[2] != MELTBPAR_LONG)
    goto lab_endgetargs;
 /*_#DEPTH__L1*/ meltfnum[0] = meltxargtab_[2].meltbp_long;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:3769:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L2*/ meltfnum[1] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.ORALOB__V2*/ meltfptr[1]),
			     (melt_ptr_t) (( /*!CLASS_OBJRAWALLOCOBJ */
					    meltfrout->tabval[0])));;
      MELT_LOCATION ("warmelt-outobj.melt:3769:/ cond");
      /*cond */ if ( /*_#IS_A__L2*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V6*/ meltfptr[5] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-outobj.melt:3769:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check oralob"),
				  ("warmelt-outobj.melt")
				  ? ("warmelt-outobj.melt") : __FILE__,
				  (3769) ? (3769) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V6*/ meltfptr[5] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V5*/ meltfptr[4] = /*_.IFELSE___V6*/ meltfptr[5];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:3769:/ clear");
	     /*clear *//*_#IS_A__L2*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V6*/ meltfptr[5] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V5*/ meltfptr[4] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-outobj.melt:3770:/ quasiblock");


    /*^getslot */
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.ORALOB__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 0, "OBI_LOC");
  /*_.ILOC__V7*/ meltfptr[5] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-outobj.melt:3771:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.ORALOB__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 2, "OBRALLOBJ_CLASS");
  /*_.ICLASS__V8*/ meltfptr[7] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-outobj.melt:3772:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.ORALOB__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 4, "OBRALLOBJ_CLASSNAME");
  /*_.ICLANAME__V9*/ meltfptr[8] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-outobj.melt:3773:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.ORALOB__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 3, "OBRALLOBJ_LEN");
  /*_.ILEN__V10*/ meltfptr[9] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-outobj.melt:3774:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.ORALOB__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 1, "OBDI_DESTLIST");
  /*_.DESTLIST__V11*/ meltfptr[10] = slot;
    };
    ;
 /*_#I__L3*/ meltfnum[1] =
      (( /*_#DEPTH__L1*/ meltfnum[0]) + (1));;
    /*^compute */
 /*_.BOXDEPTHP1__V12*/ meltfptr[11] =
      (meltgc_new_int
       ((meltobject_ptr_t) (( /*!DISCR_INTEGER */ meltfrout->tabval[1])),
	( /*_#I__L3*/ meltfnum[1])));;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:3777:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L4*/ meltfnum[3] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.ICLASS__V8*/ meltfptr[7]),
			     (melt_ptr_t) (( /*!CLASS_OBJVALUE */ meltfrout->
					    tabval[2])));;
      MELT_LOCATION ("warmelt-outobj.melt:3777:/ cond");
      /*cond */ if ( /*_#IS_A__L4*/ meltfnum[3])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V14*/ meltfptr[13] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-outobj.melt:3777:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("outpucod_objrawallocobj check iclass"),
				  ("warmelt-outobj.melt")
				  ? ("warmelt-outobj.melt") : __FILE__,
				  (3777) ? (3777) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V14*/ meltfptr[13] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V13*/ meltfptr[12] = /*_.IFELSE___V14*/ meltfptr[13];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:3777:/ clear");
	     /*clear *//*_#IS_A__L4*/ meltfnum[3] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V14*/ meltfptr[13] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V13*/ meltfptr[12] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-outobj.melt:3778:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[3];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.IMPLBUF__V4*/ meltfptr[3];
      /*^apply.arg */
      argtab[1].meltbp_long = /*_#DEPTH__L1*/ meltfnum[0];
      /*^apply.arg */
      argtab[2].meltbp_cstring = "rawallocobj";
      /*_.OUTPUT_LOCATION__V15*/ meltfptr[13] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!OUTPUT_LOCATION */ meltfrout->tabval[3])),
		    (melt_ptr_t) ( /*_.ILOC__V7*/ meltfptr[5]),
		    (MELTBPARSTR_PTR MELTBPARSTR_LONG MELTBPARSTR_CSTRING ""),
		    argtab, "", (union meltparam_un *) 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:3779:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   ("/*rawallocobj*/ { melt_ptr_t newobj = 0;"));
    }
    ;
 /*_#I__L5*/ meltfnum[3] =
      (( /*_#DEPTH__L1*/ meltfnum[0]) + (1));;

    {
      MELT_LOCATION ("warmelt-outobj.melt:3780:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				( /*_#I__L5*/ meltfnum[3]), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:3781:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   ("melt_raw_object_create(newobj,(melt_ptr_t)("));
    }
    ;
 /*_#GET_INT__L6*/ meltfnum[5] =
      (melt_get_int ((melt_ptr_t) ( /*_.BOXDEPTHP1__V12*/ meltfptr[11])));;
    MELT_LOCATION ("warmelt-outobj.melt:3782:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^msend */
    /*msend */
    {
      union meltparam_un argtab[3];
      memset (&argtab, 0, sizeof (argtab));
      /*^ojbmsend.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.DECLBUF__V3*/ meltfptr[2];
      /*^ojbmsend.arg */
      argtab[1].meltbp_aptr = (melt_ptr_t *) & /*_.IMPLBUF__V4*/ meltfptr[3];
      /*^ojbmsend.arg */
      argtab[2].meltbp_long = /*_#GET_INT__L6*/ meltfnum[5];
      /*_.OUTPUT_C_CODE__V16*/ meltfptr[15] =
	meltgc_send ((melt_ptr_t) ( /*_.ICLASS__V8*/ meltfptr[7]),
		     (melt_ptr_t) (( /*!OUTPUT_C_CODE */ meltfrout->
				    tabval[4])),
		     (MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_LONG ""),
		     argtab, "", (union meltparam_un *) 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:3783:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   ("), ("));
    }
    ;
 /*_#GET_INT__L7*/ meltfnum[6] =
      (melt_get_int ((melt_ptr_t) ( /*_.BOXDEPTHP1__V12*/ meltfptr[11])));;
    MELT_LOCATION ("warmelt-outobj.melt:3784:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^msend */
    /*msend */
    {
      union meltparam_un argtab[3];
      memset (&argtab, 0, sizeof (argtab));
      /*^ojbmsend.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.DECLBUF__V3*/ meltfptr[2];
      /*^ojbmsend.arg */
      argtab[1].meltbp_aptr = (melt_ptr_t *) & /*_.IMPLBUF__V4*/ meltfptr[3];
      /*^ojbmsend.arg */
      argtab[2].meltbp_long = /*_#GET_INT__L7*/ meltfnum[6];
      /*_.OUTPUT_C_CODE__V17*/ meltfptr[16] =
	meltgc_send ((melt_ptr_t) ( /*_.ILEN__V10*/ meltfptr[9]),
		     (melt_ptr_t) (( /*!OUTPUT_C_CODE */ meltfrout->
				    tabval[4])),
		     (MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_LONG ""),
		     argtab, "", (union meltparam_un *) 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:3785:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   ("), \""));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:3786:/ locexp");
      meltgc_add_strbuf_cstr ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			      melt_string_str ((melt_ptr_t)
					       ( /*_.ICLANAME__V9*/
						meltfptr[8])));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:3787:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   ("\");"));
    }
    ;
    /*citerblock FOREACH_IN_LIST */
    {
      /* start foreach_in_list meltcit1__EACHLIST */
      for ( /*_.DSTPAIR__V18*/ meltfptr[17] =
	   melt_list_first ((melt_ptr_t) /*_.DESTLIST__V11*/ meltfptr[10]);
	   melt_magic_discr ((melt_ptr_t) /*_.DSTPAIR__V18*/ meltfptr[17]) ==
	   MELTOBMAG_PAIR;
	   /*_.DSTPAIR__V18*/ meltfptr[17] =
	   melt_pair_tail ((melt_ptr_t) /*_.DSTPAIR__V18*/ meltfptr[17]))
	{
	  /*_.DST__V19*/ meltfptr[18] =
	    melt_pair_head ((melt_ptr_t) /*_.DSTPAIR__V18*/ meltfptr[17]);


  /*_#I__L8*/ meltfnum[7] =
	    (( /*_#DEPTH__L1*/ meltfnum[0]) + (1));;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:3791:/ locexp");
	    meltgc_strbuf_add_indent ((melt_ptr_t)
				      ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				      ( /*_#I__L8*/ meltfnum[7]), 0);
	  }
	  ;
  /*_#GET_INT__L9*/ meltfnum[8] =
	    (melt_get_int
	     ((melt_ptr_t) ( /*_.BOXDEPTHP1__V12*/ meltfptr[11])));;
	  MELT_LOCATION ("warmelt-outobj.melt:3792:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^msend */
	  /*msend */
	  {
	    union meltparam_un argtab[3];
	    memset (&argtab, 0, sizeof (argtab));
	    /*^ojbmsend.arg */
	    argtab[0].meltbp_aptr =
	      (melt_ptr_t *) & /*_.DECLBUF__V3*/ meltfptr[2];
	    /*^ojbmsend.arg */
	    argtab[1].meltbp_aptr =
	      (melt_ptr_t *) & /*_.IMPLBUF__V4*/ meltfptr[3];
	    /*^ojbmsend.arg */
	    argtab[2].meltbp_long = /*_#GET_INT__L9*/ meltfnum[8];
	    /*_.OUTPUT_C_CODE__V20*/ meltfptr[19] =
	      meltgc_send ((melt_ptr_t) ( /*_.DST__V19*/ meltfptr[18]),
			   (melt_ptr_t) (( /*!OUTPUT_C_CODE */ meltfrout->
					  tabval[4])),
			   (MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_LONG
			    ""), argtab, "", (union meltparam_un *) 0);
	  }
	  ;

#if MELT_HAVE_DEBUG
	  MELT_LOCATION ("warmelt-outobj.melt:3793:/ cppif.then");
	  /*^block */
	  /*anyblock */
	  {

	    /*^checksignal */
	    MELT_CHECK_SIGNAL ();
	    ;
    /*_#STRBUF_USEDLENGTH__L10*/ meltfnum[9] =
	      melt_strbuf_usedlength ((melt_ptr_t)
				      ( /*_.IMPLBUF__V4*/ meltfptr[3]));;
	    MELT_LOCATION ("warmelt-outobj.melt:3794:/ cond");
	    /*cond */ if (
			   /*ifisa */
			   melt_is_instance_of ((melt_ptr_t)
						(( /*!BUFFER_LIMIT_CONT */
						  meltfrout->tabval[5])),
						(melt_ptr_t) (( /*!CLASS_REFERENCE */ meltfrout->tabval[6])))
	      )			/*then */
	      {
		/*^cond.then */
		/*^getslot */
		{
		  melt_ptr_t slot = NULL, obj = NULL;
		  obj =
		    (melt_ptr_t) (( /*!BUFFER_LIMIT_CONT */ meltfrout->
				   tabval[5])) /*=obj*/ ;
		  melt_object_get_field (slot, obj, 0, "REFERENCED_VALUE");
      /*_.REFERENCED_VALUE__V22*/ meltfptr[21] = slot;
		};
		;
	      }
	    else
	      {			/*^cond.else */

     /*_.REFERENCED_VALUE__V22*/ meltfptr[21] = NULL;;
	      }
	    ;
	    /*^compute */
    /*_#GET_INT__L11*/ meltfnum[10] =
	      (melt_get_int
	       ((melt_ptr_t) ( /*_.REFERENCED_VALUE__V22*/ meltfptr[21])));;
	    /*^compute */
    /*_#I__L12*/ meltfnum[11] =
	      (( /*_#STRBUF_USEDLENGTH__L10*/ meltfnum[9]) <
	       ( /*_#GET_INT__L11*/ meltfnum[10]));;
	    MELT_LOCATION ("warmelt-outobj.melt:3793:/ cond");
	    /*cond */ if ( /*_#I__L12*/ meltfnum[11])	/*then */
	      {
		/*^cond.then */
		/*_.IFELSE___V23*/ meltfptr[22] = ( /*nil */ NULL);;
	      }
	    else
	      {
		MELT_LOCATION ("warmelt-outobj.melt:3793:/ cond.else");

		/*^block */
		/*anyblock */
		{




		  {
		    /*^locexp */
		    melt_assert_failed (("check limited implbuf"),
					("warmelt-outobj.melt")
					? ("warmelt-outobj.melt") : __FILE__,
					(3793) ? (3793) : __LINE__,
					__FUNCTION__);
		    ;
		  }
		  ;
		/*clear *//*_.IFELSE___V23*/ meltfptr[22] = 0;
		  /*epilog */
		}
		;
	      }
	    ;
	    /*^compute */
	    /*_.IFCPP___V21*/ meltfptr[20] = /*_.IFELSE___V23*/ meltfptr[22];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-outobj.melt:3793:/ clear");
	      /*clear *//*_#STRBUF_USEDLENGTH__L10*/ meltfnum[9] = 0;
	    /*^clear */
	      /*clear *//*_.REFERENCED_VALUE__V22*/ meltfptr[21] = 0;
	    /*^clear */
	      /*clear *//*_#GET_INT__L11*/ meltfnum[10] = 0;
	    /*^clear */
	      /*clear *//*_#I__L12*/ meltfnum[11] = 0;
	    /*^clear */
	      /*clear *//*_.IFELSE___V23*/ meltfptr[22] = 0;
	  }

#else /*MELT_HAVE_DEBUG */
	  /*^cppif.else */
	  /*_.IFCPP___V21*/ meltfptr[20] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:3795:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t)
				 ( /*_.IMPLBUF__V4*/ meltfptr[3]), (" ="));
	  }
	  ;
	}			/* end foreach_in_list meltcit1__EACHLIST */
     /*_.DSTPAIR__V18*/ meltfptr[17] = NULL;
     /*_.DST__V19*/ meltfptr[18] = NULL;


      /*citerepilog */

      MELT_LOCATION ("warmelt-outobj.melt:3788:/ clear");
	    /*clear *//*_.DSTPAIR__V18*/ meltfptr[17] = 0;
      /*^clear */
	    /*clear *//*_.DST__V19*/ meltfptr[18] = 0;
      /*^clear */
	    /*clear *//*_#I__L8*/ meltfnum[7] = 0;
      /*^clear */
	    /*clear *//*_#GET_INT__L9*/ meltfnum[8] = 0;
      /*^clear */
	    /*clear *//*_.OUTPUT_C_CODE__V20*/ meltfptr[19] = 0;
      /*^clear */
	    /*clear *//*_.IFCPP___V21*/ meltfptr[20] = 0;
    }				/*endciterblock FOREACH_IN_LIST */
    ;

    MELT_LOCATION ("warmelt-outobj.melt:3770:/ clear");
	   /*clear *//*_.ILOC__V7*/ meltfptr[5] = 0;
    /*^clear */
	   /*clear *//*_.ICLASS__V8*/ meltfptr[7] = 0;
    /*^clear */
	   /*clear *//*_.ICLANAME__V9*/ meltfptr[8] = 0;
    /*^clear */
	   /*clear *//*_.ILEN__V10*/ meltfptr[9] = 0;
    /*^clear */
	   /*clear *//*_.DESTLIST__V11*/ meltfptr[10] = 0;
    /*^clear */
	   /*clear *//*_#I__L3*/ meltfnum[1] = 0;
    /*^clear */
	   /*clear *//*_.BOXDEPTHP1__V12*/ meltfptr[11] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V13*/ meltfptr[12] = 0;
    /*^clear */
	   /*clear *//*_.OUTPUT_LOCATION__V15*/ meltfptr[13] = 0;
    /*^clear */
	   /*clear *//*_#I__L5*/ meltfnum[3] = 0;
    /*^clear */
	   /*clear *//*_#GET_INT__L6*/ meltfnum[5] = 0;
    /*^clear */
	   /*clear *//*_.OUTPUT_C_CODE__V16*/ meltfptr[15] = 0;
    /*^clear */
	   /*clear *//*_#GET_INT__L7*/ meltfnum[6] = 0;
    /*^clear */
	   /*clear *//*_.OUTPUT_C_CODE__V17*/ meltfptr[16] = 0;
 /*_#I__L13*/ meltfnum[9] =
      (( /*_#DEPTH__L1*/ meltfnum[0]) + (1));;

    {
      MELT_LOCATION ("warmelt-outobj.melt:3796:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				( /*_#I__L13*/ meltfnum[9]), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:3797:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   ("newobj; };"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:3798:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				( /*_#DEPTH__L1*/ meltfnum[0]), 0);
    }
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:3799:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#STRBUF_USEDLENGTH__L14*/ meltfnum[10] =
	melt_strbuf_usedlength ((melt_ptr_t)
				( /*_.IMPLBUF__V4*/ meltfptr[3]));;
      MELT_LOCATION ("warmelt-outobj.melt:3800:/ cond");
      /*cond */ if (
		     /*ifisa */
		     melt_is_instance_of ((melt_ptr_t)
					  (( /*!BUFFER_LIMIT_CONT */
					    meltfrout->tabval[5])),
					  (melt_ptr_t) (( /*!CLASS_REFERENCE */ meltfrout->tabval[6])))
	)			/*then */
	{
	  /*^cond.then */
	  /*^getslot */
	  {
	    melt_ptr_t slot = NULL, obj = NULL;
	    obj =
	      (melt_ptr_t) (( /*!BUFFER_LIMIT_CONT */ meltfrout->
			     tabval[5])) /*=obj*/ ;
	    melt_object_get_field (slot, obj, 0, "REFERENCED_VALUE");
     /*_.REFERENCED_VALUE__V25*/ meltfptr[22] = slot;
	  };
	  ;
	}
      else
	{			/*^cond.else */

    /*_.REFERENCED_VALUE__V25*/ meltfptr[22] = NULL;;
	}
      ;
      /*^compute */
   /*_#GET_INT__L15*/ meltfnum[11] =
	(melt_get_int
	 ((melt_ptr_t) ( /*_.REFERENCED_VALUE__V25*/ meltfptr[22])));;
      /*^compute */
   /*_#I__L16*/ meltfnum[1] =
	(( /*_#STRBUF_USEDLENGTH__L14*/ meltfnum[10]) <
	 ( /*_#GET_INT__L15*/ meltfnum[11]));;
      MELT_LOCATION ("warmelt-outobj.melt:3799:/ cond");
      /*cond */ if ( /*_#I__L16*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V26*/ meltfptr[5] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-outobj.melt:3799:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check limited implbuf"),
				  ("warmelt-outobj.melt")
				  ? ("warmelt-outobj.melt") : __FILE__,
				  (3799) ? (3799) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V26*/ meltfptr[5] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V24*/ meltfptr[21] = /*_.IFELSE___V26*/ meltfptr[5];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:3799:/ clear");
	     /*clear *//*_#STRBUF_USEDLENGTH__L14*/ meltfnum[10] = 0;
      /*^clear */
	     /*clear *//*_.REFERENCED_VALUE__V25*/ meltfptr[22] = 0;
      /*^clear */
	     /*clear *//*_#GET_INT__L15*/ meltfnum[11] = 0;
      /*^clear */
	     /*clear *//*_#I__L16*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V26*/ meltfptr[5] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V24*/ meltfptr[21] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-outobj.melt:3768:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.IFCPP___V24*/ meltfptr[21];;

    {
      MELT_LOCATION ("warmelt-outobj.melt:3768:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V5*/ meltfptr[4] = 0;
    /*^clear */
	   /*clear *//*_#I__L13*/ meltfnum[9] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V24*/ meltfptr[21] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("OUTPUCOD_OBJRAWALLOCOBJ", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_96_warmelt_outobj_OUTPUCOD_OBJRAWALLOCOBJ_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_96_warmelt_outobj_OUTPUCOD_OBJRAWALLOCOBJ */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_97_warmelt_outobj_OUTPUCOD_OBJNEWCLOSURE (meltclosure_ptr_t
						   meltclosp_,
						   melt_ptr_t meltfirstargp_,
						   const melt_argdescr_cell_t
						   meltxargdescr_[],
						   union meltparam_un *
						   meltxargtab_,
						   const melt_argdescr_cell_t
						   meltxresdescr_[],
						   union meltparam_un *
						   meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_97_warmelt_outobj_OUTPUCOD_OBJNEWCLOSURE_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_97_warmelt_outobj_OUTPUCOD_OBJNEWCLOSURE_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 23
    melt_ptr_t mcfr_varptr[23];
#define MELTFRAM_NBVARNUM 10
    long mcfr_varnum[10];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_97_warmelt_outobj_OUTPUCOD_OBJNEWCLOSURE is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_97_warmelt_outobj_OUTPUCOD_OBJNEWCLOSURE_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 23; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_97_warmelt_outobj_OUTPUCOD_OBJNEWCLOSURE nbval 23*/
  meltfram__.mcfr_nbvar = 23 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("OUTPUCOD_OBJNEWCLOSURE", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-outobj.melt:3806:/ getarg");
 /*_.OBNCLO__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.DECLBUF__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.DECLBUF__V3*/ meltfptr[2])) !=
	      NULL);


  /*getarg#2 */
  /*^getarg */
  if (meltxargdescr_[1] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.IMPLBUF__V4*/ meltfptr[3] =
    (meltxargtab_[1].meltbp_aptr) ? (*(meltxargtab_[1].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3])) !=
	      NULL);


  /*getarg#3 */
  /*^getarg */
  if (meltxargdescr_[2] != MELTBPAR_LONG)
    goto lab_endgetargs;
 /*_#DEPTH__L1*/ meltfnum[0] = meltxargtab_[2].meltbp_long;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:3807:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L2*/ meltfnum[1] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.OBNCLO__V2*/ meltfptr[1]),
			     (melt_ptr_t) (( /*!CLASS_OBJNEWCLOSURE */
					    meltfrout->tabval[0])));;
      MELT_LOCATION ("warmelt-outobj.melt:3807:/ cond");
      /*cond */ if ( /*_#IS_A__L2*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V6*/ meltfptr[5] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-outobj.melt:3807:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check oralob"),
				  ("warmelt-outobj.melt")
				  ? ("warmelt-outobj.melt") : __FILE__,
				  (3807) ? (3807) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V6*/ meltfptr[5] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V5*/ meltfptr[4] = /*_.IFELSE___V6*/ meltfptr[5];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:3807:/ clear");
	     /*clear *//*_#IS_A__L2*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V6*/ meltfptr[5] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V5*/ meltfptr[4] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-outobj.melt:3808:/ quasiblock");


    /*^getslot */
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.OBNCLO__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 0, "OBI_LOC");
  /*_.ILOC__V8*/ meltfptr[7] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-outobj.melt:3809:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.OBNCLO__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 2, "OBNCLO_DISCR");
  /*_.ODISCR__V9*/ meltfptr[8] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-outobj.melt:3810:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.OBNCLO__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 3, "OBNCLO_ROUT");
  /*_.OROUT__V10*/ meltfptr[9] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-outobj.melt:3811:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.OBNCLO__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 4, "OBNCLO_LEN");
  /*_.OLEN__V11*/ meltfptr[10] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-outobj.melt:3812:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.OBNCLO__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 1, "OBDI_DESTLIST");
  /*_.DESTLIST__V12*/ meltfptr[11] = slot;
    };
    ;
 /*_#I__L3*/ meltfnum[1] =
      (( /*_#DEPTH__L1*/ meltfnum[0]) + (1));;
    /*^compute */
 /*_.BOXDEPTHP1__V13*/ meltfptr[12] =
      (meltgc_new_int
       ((meltobject_ptr_t) (( /*!DISCR_INTEGER */ meltfrout->tabval[1])),
	( /*_#I__L3*/ meltfnum[1])));;
    MELT_LOCATION ("warmelt-outobj.melt:3815:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[3];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.IMPLBUF__V4*/ meltfptr[3];
      /*^apply.arg */
      argtab[1].meltbp_long = /*_#DEPTH__L1*/ meltfnum[0];
      /*^apply.arg */
      argtab[2].meltbp_cstring = "newclosure";
      /*_.OUTPUT_LOCATION__V14*/ meltfptr[13] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!OUTPUT_LOCATION */ meltfrout->tabval[2])),
		    (melt_ptr_t) ( /*_.ILOC__V8*/ meltfptr[7]),
		    (MELTBPARSTR_PTR MELTBPARSTR_LONG MELTBPARSTR_CSTRING ""),
		    argtab, "", (union meltparam_un *) 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:3816:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   (" /*newclosure*/ "));
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:3819:/ quasiblock");


    /*^newclosure */
		 /*newclosure *//*_.LAMBDA___V16*/ meltfptr[15] =
      (melt_ptr_t)
      meltgc_new_closure ((meltobject_ptr_t)
			  (((melt_ptr_t) (MELT_PREDEF (DISCR_CLOSURE)))),
			  (meltroutine_ptr_t) (( /*!konst_7 */ meltfrout->
						tabval[7])), (3));
    ;
    /*^putclosedv */
    /*putclosv */
    melt_assertmsg ("putclosv checkclo",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.LAMBDA___V16*/ meltfptr[15])) ==
		    MELTOBMAG_CLOSURE);
    melt_assertmsg ("putclosv checkoff", 0 >= 0
		    && 0 <
		    melt_closure_size ((melt_ptr_t)
				       ( /*_.LAMBDA___V16*/ meltfptr[15])));
    ((meltclosure_ptr_t) /*_.LAMBDA___V16*/ meltfptr[15])->tabval[0] =
      (melt_ptr_t) ( /*_.DECLBUF__V3*/ meltfptr[2]);
    ;
    /*^putclosedv */
    /*putclosv */
    melt_assertmsg ("putclosv checkclo",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.LAMBDA___V16*/ meltfptr[15])) ==
		    MELTOBMAG_CLOSURE);
    melt_assertmsg ("putclosv checkoff", 1 >= 0
		    && 1 <
		    melt_closure_size ((melt_ptr_t)
				       ( /*_.LAMBDA___V16*/ meltfptr[15])));
    ((meltclosure_ptr_t) /*_.LAMBDA___V16*/ meltfptr[15])->tabval[1] =
      (melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]);
    ;
    /*^putclosedv */
    /*putclosv */
    melt_assertmsg ("putclosv checkclo",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.LAMBDA___V16*/ meltfptr[15])) ==
		    MELTOBMAG_CLOSURE);
    melt_assertmsg ("putclosv checkoff", 2 >= 0
		    && 2 <
		    melt_closure_size ((melt_ptr_t)
				       ( /*_.LAMBDA___V16*/ meltfptr[15])));
    ((meltclosure_ptr_t) /*_.LAMBDA___V16*/ meltfptr[15])->tabval[2] =
      (melt_ptr_t) ( /*_.BOXDEPTHP1__V13*/ meltfptr[12]);
    ;
    /*_.LAMBDA___V15*/ meltfptr[14] = /*_.LAMBDA___V16*/ meltfptr[15];;
    MELT_LOCATION ("warmelt-outobj.melt:3817:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & /*_.LAMBDA___V15*/ meltfptr[14];
      /*_.LIST_EVERY__V17*/ meltfptr[16] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!LIST_EVERY */ meltfrout->tabval[3])),
		    (melt_ptr_t) ( /*_.DESTLIST__V12*/ meltfptr[11]),
		    (MELTBPARSTR_PTR ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;
 /*_#I__L4*/ meltfnum[3] =
      (( /*_#DEPTH__L1*/ meltfnum[0]) + (1));;

    {
      MELT_LOCATION ("warmelt-outobj.melt:3824:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				( /*_#I__L4*/ meltfnum[3]), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:3825:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   ("(melt_ptr_t) meltgc_new_closure((meltobject_ptr_t)("));
    }
    ;
 /*_#GET_INT__L5*/ meltfnum[4] =
      (melt_get_int ((melt_ptr_t) ( /*_.BOXDEPTHP1__V13*/ meltfptr[12])));;
    MELT_LOCATION ("warmelt-outobj.melt:3826:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^msend */
    /*msend */
    {
      union meltparam_un argtab[3];
      memset (&argtab, 0, sizeof (argtab));
      /*^ojbmsend.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.DECLBUF__V3*/ meltfptr[2];
      /*^ojbmsend.arg */
      argtab[1].meltbp_aptr = (melt_ptr_t *) & /*_.IMPLBUF__V4*/ meltfptr[3];
      /*^ojbmsend.arg */
      argtab[2].meltbp_long = /*_#GET_INT__L5*/ meltfnum[4];
      /*_.OUTPUT_C_CODE__V18*/ meltfptr[17] =
	meltgc_send ((melt_ptr_t) ( /*_.ODISCR__V9*/ meltfptr[8]),
		     (melt_ptr_t) (( /*!OUTPUT_C_CODE */ meltfrout->
				    tabval[8])),
		     (MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_LONG ""),
		     argtab, "", (union meltparam_un *) 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:3827:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   ("), (meltroutine_ptr_t)("));
    }
    ;
 /*_#GET_INT__L6*/ meltfnum[5] =
      (melt_get_int ((melt_ptr_t) ( /*_.BOXDEPTHP1__V13*/ meltfptr[12])));;
    MELT_LOCATION ("warmelt-outobj.melt:3828:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^msend */
    /*msend */
    {
      union meltparam_un argtab[3];
      memset (&argtab, 0, sizeof (argtab));
      /*^ojbmsend.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.DECLBUF__V3*/ meltfptr[2];
      /*^ojbmsend.arg */
      argtab[1].meltbp_aptr = (melt_ptr_t *) & /*_.IMPLBUF__V4*/ meltfptr[3];
      /*^ojbmsend.arg */
      argtab[2].meltbp_long = /*_#GET_INT__L6*/ meltfnum[5];
      /*_.OUTPUT_C_CODE__V19*/ meltfptr[18] =
	meltgc_send ((melt_ptr_t) ( /*_.OROUT__V10*/ meltfptr[9]),
		     (melt_ptr_t) (( /*!OUTPUT_C_CODE */ meltfrout->
				    tabval[8])),
		     (MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_LONG ""),
		     argtab, "", (union meltparam_un *) 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:3829:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   ("), ("));
    }
    ;
 /*_#GET_INT__L7*/ meltfnum[6] =
      (melt_get_int ((melt_ptr_t) ( /*_.BOXDEPTHP1__V13*/ meltfptr[12])));;
    MELT_LOCATION ("warmelt-outobj.melt:3830:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^msend */
    /*msend */
    {
      union meltparam_un argtab[3];
      memset (&argtab, 0, sizeof (argtab));
      /*^ojbmsend.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.DECLBUF__V3*/ meltfptr[2];
      /*^ojbmsend.arg */
      argtab[1].meltbp_aptr = (melt_ptr_t *) & /*_.IMPLBUF__V4*/ meltfptr[3];
      /*^ojbmsend.arg */
      argtab[2].meltbp_long = /*_#GET_INT__L7*/ meltfnum[6];
      /*_.OUTPUT_C_CODE__V20*/ meltfptr[19] =
	meltgc_send ((melt_ptr_t) ( /*_.OLEN__V11*/ meltfptr[10]),
		     (melt_ptr_t) (( /*!OUTPUT_C_CODE */ meltfrout->
				    tabval[8])),
		     (MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_LONG ""),
		     argtab, "", (union meltparam_un *) 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:3831:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   ("));"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:3832:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				( /*_#DEPTH__L1*/ meltfnum[0]), 0);
    }
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:3833:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#STRBUF_USEDLENGTH__L8*/ meltfnum[7] =
	melt_strbuf_usedlength ((melt_ptr_t)
				( /*_.IMPLBUF__V4*/ meltfptr[3]));;
      MELT_LOCATION ("warmelt-outobj.melt:3834:/ cond");
      /*cond */ if (
		     /*ifisa */
		     melt_is_instance_of ((melt_ptr_t)
					  (( /*!BUFFER_LIMIT_CONT */
					    meltfrout->tabval[9])),
					  (melt_ptr_t) (( /*!CLASS_REFERENCE */ meltfrout->tabval[10])))
	)			/*then */
	{
	  /*^cond.then */
	  /*^getslot */
	  {
	    melt_ptr_t slot = NULL, obj = NULL;
	    obj =
	      (melt_ptr_t) (( /*!BUFFER_LIMIT_CONT */ meltfrout->
			     tabval[9])) /*=obj*/ ;
	    melt_object_get_field (slot, obj, 0, "REFERENCED_VALUE");
     /*_.REFERENCED_VALUE__V22*/ meltfptr[21] = slot;
	  };
	  ;
	}
      else
	{			/*^cond.else */

    /*_.REFERENCED_VALUE__V22*/ meltfptr[21] = NULL;;
	}
      ;
      /*^compute */
   /*_#GET_INT__L9*/ meltfnum[8] =
	(melt_get_int
	 ((melt_ptr_t) ( /*_.REFERENCED_VALUE__V22*/ meltfptr[21])));;
      /*^compute */
   /*_#I__L10*/ meltfnum[9] =
	(( /*_#STRBUF_USEDLENGTH__L8*/ meltfnum[7]) <
	 ( /*_#GET_INT__L9*/ meltfnum[8]));;
      MELT_LOCATION ("warmelt-outobj.melt:3833:/ cond");
      /*cond */ if ( /*_#I__L10*/ meltfnum[9])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V23*/ meltfptr[22] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-outobj.melt:3833:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check limited implbuf"),
				  ("warmelt-outobj.melt")
				  ? ("warmelt-outobj.melt") : __FILE__,
				  (3833) ? (3833) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V23*/ meltfptr[22] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V21*/ meltfptr[20] = /*_.IFELSE___V23*/ meltfptr[22];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:3833:/ clear");
	     /*clear *//*_#STRBUF_USEDLENGTH__L8*/ meltfnum[7] = 0;
      /*^clear */
	     /*clear *//*_.REFERENCED_VALUE__V22*/ meltfptr[21] = 0;
      /*^clear */
	     /*clear *//*_#GET_INT__L9*/ meltfnum[8] = 0;
      /*^clear */
	     /*clear *//*_#I__L10*/ meltfnum[9] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V23*/ meltfptr[22] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V21*/ meltfptr[20] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    /*^compute */
    /*_.LET___V7*/ meltfptr[5] = /*_.IFCPP___V21*/ meltfptr[20];;

    MELT_LOCATION ("warmelt-outobj.melt:3808:/ clear");
	   /*clear *//*_.ILOC__V8*/ meltfptr[7] = 0;
    /*^clear */
	   /*clear *//*_.ODISCR__V9*/ meltfptr[8] = 0;
    /*^clear */
	   /*clear *//*_.OROUT__V10*/ meltfptr[9] = 0;
    /*^clear */
	   /*clear *//*_.OLEN__V11*/ meltfptr[10] = 0;
    /*^clear */
	   /*clear *//*_.DESTLIST__V12*/ meltfptr[11] = 0;
    /*^clear */
	   /*clear *//*_#I__L3*/ meltfnum[1] = 0;
    /*^clear */
	   /*clear *//*_.BOXDEPTHP1__V13*/ meltfptr[12] = 0;
    /*^clear */
	   /*clear *//*_.OUTPUT_LOCATION__V14*/ meltfptr[13] = 0;
    /*^clear */
	   /*clear *//*_.LAMBDA___V15*/ meltfptr[14] = 0;
    /*^clear */
	   /*clear *//*_.LIST_EVERY__V17*/ meltfptr[16] = 0;
    /*^clear */
	   /*clear *//*_#I__L4*/ meltfnum[3] = 0;
    /*^clear */
	   /*clear *//*_#GET_INT__L5*/ meltfnum[4] = 0;
    /*^clear */
	   /*clear *//*_.OUTPUT_C_CODE__V18*/ meltfptr[17] = 0;
    /*^clear */
	   /*clear *//*_#GET_INT__L6*/ meltfnum[5] = 0;
    /*^clear */
	   /*clear *//*_.OUTPUT_C_CODE__V19*/ meltfptr[18] = 0;
    /*^clear */
	   /*clear *//*_#GET_INT__L7*/ meltfnum[6] = 0;
    /*^clear */
	   /*clear *//*_.OUTPUT_C_CODE__V20*/ meltfptr[19] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V21*/ meltfptr[20] = 0;
    MELT_LOCATION ("warmelt-outobj.melt:3806:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.LET___V7*/ meltfptr[5];;

    {
      MELT_LOCATION ("warmelt-outobj.melt:3806:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V5*/ meltfptr[4] = 0;
    /*^clear */
	   /*clear *//*_.LET___V7*/ meltfptr[5] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("OUTPUCOD_OBJNEWCLOSURE", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_97_warmelt_outobj_OUTPUCOD_OBJNEWCLOSURE_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_97_warmelt_outobj_OUTPUCOD_OBJNEWCLOSURE */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_98_warmelt_outobj_LAMBDA___24__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un * meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un * meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_98_warmelt_outobj_LAMBDA___24___melt =
    melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_98_warmelt_outobj_LAMBDA___24___st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 6
    melt_ptr_t mcfr_varptr[6];
#define MELTFRAM_NBVARNUM 4
    long mcfr_varnum[4];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_98_warmelt_outobj_LAMBDA___24__ is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_98_warmelt_outobj_LAMBDA___24___st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 6; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_98_warmelt_outobj_LAMBDA___24__ nbval 6*/
  meltfram__.mcfr_nbvar = 6 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("LAMBDA_", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-outobj.melt:3819:/ getarg");
 /*_.DST__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {

 /*_#GET_INT__L1*/ meltfnum[0] =
      (melt_get_int
       ((melt_ptr_t) (( /*~BOXDEPTHP1 */ meltfclos->tabval[2]))));;
    MELT_LOCATION ("warmelt-outobj.melt:3820:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^msend */
    /*msend */
    {
      union meltparam_un argtab[3];
      memset (&argtab, 0, sizeof (argtab));
      /*^ojbmsend.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & ( /*~DECLBUF */ meltfclos->tabval[0]);
      /*^ojbmsend.arg */
      argtab[1].meltbp_aptr =
	(melt_ptr_t *) & ( /*~IMPLBUF */ meltfclos->tabval[1]);
      /*^ojbmsend.arg */
      argtab[2].meltbp_long = /*_#GET_INT__L1*/ meltfnum[0];
      /*_.OUTPUT_C_CODE__V3*/ meltfptr[2] =
	meltgc_send ((melt_ptr_t) ( /*_.DST__V2*/ meltfptr[1]),
		     (melt_ptr_t) (( /*!OUTPUT_C_CODE */ meltfrout->
				    tabval[0])),
		     (MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_LONG ""),
		     argtab, "", (union meltparam_un *) 0);
    }
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:3821:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#STRBUF_USEDLENGTH__L2*/ meltfnum[1] =
	melt_strbuf_usedlength ((melt_ptr_t)
				(( /*~IMPLBUF */ meltfclos->tabval[1])));;
      MELT_LOCATION ("warmelt-outobj.melt:3822:/ cond");
      /*cond */ if (
		     /*ifisa */
		     melt_is_instance_of ((melt_ptr_t)
					  (( /*!BUFFER_LIMIT_CONT */
					    meltfrout->tabval[1])),
					  (melt_ptr_t) (( /*!CLASS_REFERENCE */ meltfrout->tabval[2])))
	)			/*then */
	{
	  /*^cond.then */
	  /*^getslot */
	  {
	    melt_ptr_t slot = NULL, obj = NULL;
	    obj =
	      (melt_ptr_t) (( /*!BUFFER_LIMIT_CONT */ meltfrout->
			     tabval[1])) /*=obj*/ ;
	    melt_object_get_field (slot, obj, 0, "REFERENCED_VALUE");
     /*_.REFERENCED_VALUE__V5*/ meltfptr[4] = slot;
	  };
	  ;
	}
      else
	{			/*^cond.else */

    /*_.REFERENCED_VALUE__V5*/ meltfptr[4] = NULL;;
	}
      ;
      /*^compute */
   /*_#GET_INT__L3*/ meltfnum[2] =
	(melt_get_int
	 ((melt_ptr_t) ( /*_.REFERENCED_VALUE__V5*/ meltfptr[4])));;
      /*^compute */
   /*_#I__L4*/ meltfnum[3] =
	(( /*_#STRBUF_USEDLENGTH__L2*/ meltfnum[1]) <
	 ( /*_#GET_INT__L3*/ meltfnum[2]));;
      MELT_LOCATION ("warmelt-outobj.melt:3821:/ cond");
      /*cond */ if ( /*_#I__L4*/ meltfnum[3])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V6*/ meltfptr[5] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-outobj.melt:3821:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check limited implbuf"),
				  ("warmelt-outobj.melt")
				  ? ("warmelt-outobj.melt") : __FILE__,
				  (3821) ? (3821) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V6*/ meltfptr[5] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V4*/ meltfptr[3] = /*_.IFELSE___V6*/ meltfptr[5];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:3821:/ clear");
	     /*clear *//*_#STRBUF_USEDLENGTH__L2*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.REFERENCED_VALUE__V5*/ meltfptr[4] = 0;
      /*^clear */
	     /*clear *//*_#GET_INT__L3*/ meltfnum[2] = 0;
      /*^clear */
	     /*clear *//*_#I__L4*/ meltfnum[3] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V6*/ meltfptr[5] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V4*/ meltfptr[3] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:3823:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t)
			   (( /*~IMPLBUF */ meltfclos->tabval[1])), (" ="));
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:3819:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*epilog */

    /*^clear */
	   /*clear *//*_#GET_INT__L1*/ meltfnum[0] = 0;
    /*^clear */
	   /*clear *//*_.OUTPUT_C_CODE__V3*/ meltfptr[2] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V4*/ meltfptr[3] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("LAMBDA_", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_98_warmelt_outobj_LAMBDA___24___melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*noretval */ NULL);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_98_warmelt_outobj_LAMBDA___24__ */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_99_warmelt_outobj_OUTPUCOD_OBJTOUCH (meltclosure_ptr_t meltclosp_,
					      melt_ptr_t meltfirstargp_,
					      const melt_argdescr_cell_t
					      meltxargdescr_[],
					      union meltparam_un *
					      meltxargtab_,
					      const melt_argdescr_cell_t
					      meltxresdescr_[],
					      union meltparam_un *
					      meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_99_warmelt_outobj_OUTPUCOD_OBJTOUCH_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_99_warmelt_outobj_OUTPUCOD_OBJTOUCH_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 11
    melt_ptr_t mcfr_varptr[11];
#define MELTFRAM_NBVARNUM 2
    long mcfr_varnum[2];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_99_warmelt_outobj_OUTPUCOD_OBJTOUCH is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_99_warmelt_outobj_OUTPUCOD_OBJTOUCH_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 11; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_99_warmelt_outobj_OUTPUCOD_OBJTOUCH nbval 11*/
  meltfram__.mcfr_nbvar = 11 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("OUTPUCOD_OBJTOUCH", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-outobj.melt:3839:/ getarg");
 /*_.OTOUCH__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.DECLBUF__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.DECLBUF__V3*/ meltfptr[2])) !=
	      NULL);


  /*getarg#2 */
  /*^getarg */
  if (meltxargdescr_[1] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.IMPLBUF__V4*/ meltfptr[3] =
    (meltxargtab_[1].meltbp_aptr) ? (*(meltxargtab_[1].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3])) !=
	      NULL);


  /*getarg#3 */
  /*^getarg */
  if (meltxargdescr_[2] != MELTBPAR_LONG)
    goto lab_endgetargs;
 /*_#DEPTH__L1*/ meltfnum[0] = meltxargtab_[2].meltbp_long;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:3840:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L2*/ meltfnum[1] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.OTOUCH__V2*/ meltfptr[1]),
			     (melt_ptr_t) (( /*!CLASS_OBJTOUCH */ meltfrout->
					    tabval[0])));;
      MELT_LOCATION ("warmelt-outobj.melt:3840:/ cond");
      /*cond */ if ( /*_#IS_A__L2*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V6*/ meltfptr[5] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-outobj.melt:3840:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check otouch"),
				  ("warmelt-outobj.melt")
				  ? ("warmelt-outobj.melt") : __FILE__,
				  (3840) ? (3840) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V6*/ meltfptr[5] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V5*/ meltfptr[4] = /*_.IFELSE___V6*/ meltfptr[5];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:3840:/ clear");
	     /*clear *//*_#IS_A__L2*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V6*/ meltfptr[5] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V5*/ meltfptr[4] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-outobj.melt:3841:/ quasiblock");


    /*^getslot */
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.OTOUCH__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 0, "OBI_LOC");
  /*_.ILOC__V7*/ meltfptr[5] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-outobj.melt:3842:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.OTOUCH__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 1, "OTOUCH_VAL");
  /*_.TOUCHED__V8*/ meltfptr[7] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-outobj.melt:3843:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.OTOUCH__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 2, "OTOUCH_COMMENT");
  /*_.COMM__V9*/ meltfptr[8] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-outobj.melt:3845:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[3];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.IMPLBUF__V4*/ meltfptr[3];
      /*^apply.arg */
      argtab[1].meltbp_long = /*_#DEPTH__L1*/ meltfnum[0];
      /*^apply.arg */
      argtab[2].meltbp_cstring = "touch";
      /*_.OUTPUT_LOCATION__V10*/ meltfptr[9] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!OUTPUT_LOCATION */ meltfrout->tabval[1])),
		    (melt_ptr_t) ( /*_.ILOC__V7*/ meltfptr[5]),
		    (MELTBPARSTR_PTR MELTBPARSTR_LONG MELTBPARSTR_CSTRING ""),
		    argtab, "", (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:3846:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^cond */
    /*cond */ if ( /*_.COMM__V9*/ meltfptr[8])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{


	  {
	    MELT_LOCATION ("warmelt-outobj.melt:3848:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t)
				 ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				 ("/*touch:"));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:3849:/ locexp");
	    meltgc_add_strbuf_cident ((melt_ptr_t)
				      ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				      melt_string_str ((melt_ptr_t)
						       ( /*_.COMM__V9*/
							meltfptr[8])));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:3850:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t)
				 ( /*_.IMPLBUF__V4*/ meltfptr[3]), ("*/"));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:3851:/ locexp");
	    meltgc_strbuf_add_indent ((melt_ptr_t)
				      ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				      ( /*_#DEPTH__L1*/ meltfnum[0]), 0);
	  }
	  ;
	  MELT_LOCATION ("warmelt-outobj.melt:3847:/ quasiblock");


	  /*epilog */
	}
	;
      }				/*noelse */
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:3853:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   ("meltgc_touch("));
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:3854:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^msend */
    /*msend */
    {
      union meltparam_un argtab[3];
      memset (&argtab, 0, sizeof (argtab));
      /*^ojbmsend.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.DECLBUF__V3*/ meltfptr[2];
      /*^ojbmsend.arg */
      argtab[1].meltbp_aptr = (melt_ptr_t *) & /*_.IMPLBUF__V4*/ meltfptr[3];
      /*^ojbmsend.arg */
      argtab[2].meltbp_long = /*_#DEPTH__L1*/ meltfnum[0];
      /*_.OUTPUT_C_CODE__V11*/ meltfptr[10] =
	meltgc_send ((melt_ptr_t) ( /*_.TOUCHED__V8*/ meltfptr[7]),
		     (melt_ptr_t) (( /*!OUTPUT_C_CODE */ meltfrout->
				    tabval[2])),
		     (MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_LONG ""),
		     argtab, "", (union meltparam_un *) 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:3855:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   (");"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:3856:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				( /*_#DEPTH__L1*/ meltfnum[0]), 0);
    }
    ;

    MELT_LOCATION ("warmelt-outobj.melt:3841:/ clear");
	   /*clear *//*_.ILOC__V7*/ meltfptr[5] = 0;
    /*^clear */
	   /*clear *//*_.TOUCHED__V8*/ meltfptr[7] = 0;
    /*^clear */
	   /*clear *//*_.COMM__V9*/ meltfptr[8] = 0;
    /*^clear */
	   /*clear *//*_.OUTPUT_LOCATION__V10*/ meltfptr[9] = 0;
    /*^clear */
	   /*clear *//*_.OUTPUT_C_CODE__V11*/ meltfptr[10] = 0;
    MELT_LOCATION ("warmelt-outobj.melt:3839:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V5*/ meltfptr[4] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("OUTPUCOD_OBJTOUCH", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_99_warmelt_outobj_OUTPUCOD_OBJTOUCH_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*noretval */ NULL);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_99_warmelt_outobj_OUTPUCOD_OBJTOUCH */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_100_warmelt_outobj_OUTPUCOD_DBGTRACEWRITEOBJ (meltclosure_ptr_t
						       meltclosp_,
						       melt_ptr_t
						       meltfirstargp_,
						       const
						       melt_argdescr_cell_t
						       meltxargdescr_[],
						       union meltparam_un *
						       meltxargtab_,
						       const
						       melt_argdescr_cell_t
						       meltxresdescr_[],
						       union meltparam_un *
						       meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_100_warmelt_outobj_OUTPUCOD_DBGTRACEWRITEOBJ_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_100_warmelt_outobj_OUTPUCOD_DBGTRACEWRITEOBJ_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 11
    melt_ptr_t mcfr_varptr[11];
#define MELTFRAM_NBVARNUM 3
    long mcfr_varnum[3];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_100_warmelt_outobj_OUTPUCOD_DBGTRACEWRITEOBJ is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_100_warmelt_outobj_OUTPUCOD_DBGTRACEWRITEOBJ_st
	 *) meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 11; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_100_warmelt_outobj_OUTPUCOD_DBGTRACEWRITEOBJ nbval 11*/
  meltfram__.mcfr_nbvar = 11 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("OUTPUCOD_DBGTRACEWRITEOBJ", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-outobj.melt:3864:/ getarg");
 /*_.OTWRO__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.DECLBUF__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.DECLBUF__V3*/ meltfptr[2])) !=
	      NULL);


  /*getarg#2 */
  /*^getarg */
  if (meltxargdescr_[1] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.IMPLBUF__V4*/ meltfptr[3] =
    (meltxargtab_[1].meltbp_aptr) ? (*(meltxargtab_[1].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3])) !=
	      NULL);


  /*getarg#3 */
  /*^getarg */
  if (meltxargdescr_[2] != MELTBPAR_LONG)
    goto lab_endgetargs;
 /*_#DEPTH__L1*/ meltfnum[0] = meltxargtab_[2].meltbp_long;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:3865:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L2*/ meltfnum[1] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.OTWRO__V2*/ meltfptr[1]),
			     (melt_ptr_t) (( /*!CLASS_OBJDBGTRACEWRITEOBJ */
					    meltfrout->tabval[0])));;
      MELT_LOCATION ("warmelt-outobj.melt:3865:/ cond");
      /*cond */ if ( /*_#IS_A__L2*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V6*/ meltfptr[5] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-outobj.melt:3865:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check otwro"),
				  ("warmelt-outobj.melt")
				  ? ("warmelt-outobj.melt") : __FILE__,
				  (3865) ? (3865) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V6*/ meltfptr[5] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V5*/ meltfptr[4] = /*_.IFELSE___V6*/ meltfptr[5];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:3865:/ clear");
	     /*clear *//*_#IS_A__L2*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V6*/ meltfptr[5] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V5*/ meltfptr[4] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-outobj.melt:3866:/ quasiblock");


    /*^getslot */
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.OTWRO__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 0, "OBI_LOC");
  /*_.ILOC__V7*/ meltfptr[5] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-outobj.melt:3867:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.OTWRO__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 1, "OBDTW_WRITTENOBJ");
  /*_.OWRITTEN__V8*/ meltfptr[7] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-outobj.melt:3868:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.OTWRO__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 2, "OBDTW_MESSAGE");
  /*_.MSG__V9*/ meltfptr[8] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-outobj.melt:3870:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[3];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.IMPLBUF__V4*/ meltfptr[3];
      /*^apply.arg */
      argtab[1].meltbp_long = /*_#DEPTH__L1*/ meltfnum[0];
      /*^apply.arg */
      argtab[2].meltbp_cstring = "touchobj";
      /*_.OUTPUT_LOCATION__V10*/ meltfptr[9] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!OUTPUT_LOCATION */ meltfrout->tabval[1])),
		    (melt_ptr_t) ( /*_.ILOC__V7*/ meltfptr[5]),
		    (MELTBPARSTR_PTR MELTBPARSTR_LONG MELTBPARSTR_CSTRING ""),
		    argtab, "", (union meltparam_un *) 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:3871:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				( /*_#DEPTH__L1*/ meltfnum[0]), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:3872:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   ("melt_dbgtrace_written_object ("));
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:3873:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^msend */
    /*msend */
    {
      union meltparam_un argtab[3];
      memset (&argtab, 0, sizeof (argtab));
      /*^ojbmsend.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.DECLBUF__V3*/ meltfptr[2];
      /*^ojbmsend.arg */
      argtab[1].meltbp_aptr = (melt_ptr_t *) & /*_.IMPLBUF__V4*/ meltfptr[3];
      /*^ojbmsend.arg */
      argtab[2].meltbp_long = /*_#DEPTH__L1*/ meltfnum[0];
      /*_.OUTPUT_C_CODE__V11*/ meltfptr[10] =
	meltgc_send ((melt_ptr_t) ( /*_.OWRITTEN__V8*/ meltfptr[7]),
		     (melt_ptr_t) (( /*!OUTPUT_C_CODE */ meltfrout->
				    tabval[2])),
		     (MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_LONG ""),
		     argtab, "", (union meltparam_un *) 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:3874:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   (", \""));
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:3875:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#IS_STRING__L3*/ meltfnum[1] =
      (melt_magic_discr ((melt_ptr_t) ( /*_.MSG__V9*/ meltfptr[8])) ==
       MELTOBMAG_STRING);;
    MELT_LOCATION ("warmelt-outobj.melt:3875:/ cond");
    /*cond */ if ( /*_#IS_STRING__L3*/ meltfnum[1])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{


	  {
	    MELT_LOCATION ("warmelt-outobj.melt:3876:/ locexp");
	    meltgc_add_strbuf_cstr ((melt_ptr_t)
				    ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				    melt_string_str ((melt_ptr_t)
						     ( /*_.MSG__V9*/
						      meltfptr[8])));
	  }
	  ;
	  /*epilog */
	}
	;
      }
    else
      {
	MELT_LOCATION ("warmelt-outobj.melt:3875:/ cond.else");

	/*^block */
	/*anyblock */
	{


	  {
	    MELT_LOCATION ("warmelt-outobj.melt:3877:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t)
				 ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				 ("*written object*"));
	  }
	  ;
	  /*epilog */
	}
	;
      }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:3878:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   ("\");"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:3879:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				( /*_#DEPTH__L1*/ meltfnum[0]), 0);
    }
    ;

    MELT_LOCATION ("warmelt-outobj.melt:3866:/ clear");
	   /*clear *//*_.ILOC__V7*/ meltfptr[5] = 0;
    /*^clear */
	   /*clear *//*_.OWRITTEN__V8*/ meltfptr[7] = 0;
    /*^clear */
	   /*clear *//*_.MSG__V9*/ meltfptr[8] = 0;
    /*^clear */
	   /*clear *//*_.OUTPUT_LOCATION__V10*/ meltfptr[9] = 0;
    /*^clear */
	   /*clear *//*_.OUTPUT_C_CODE__V11*/ meltfptr[10] = 0;
    /*^clear */
	   /*clear *//*_#IS_STRING__L3*/ meltfnum[1] = 0;
    MELT_LOCATION ("warmelt-outobj.melt:3864:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V5*/ meltfptr[4] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("OUTPUCOD_DBGTRACEWRITEOBJ", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_100_warmelt_outobj_OUTPUCOD_DBGTRACEWRITEOBJ_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*noretval */ NULL);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_100_warmelt_outobj_OUTPUCOD_DBGTRACEWRITEOBJ */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_101_warmelt_outobj_OUTPUCOD_OBJPUTUPLE (meltclosure_ptr_t meltclosp_,
						 melt_ptr_t meltfirstargp_,
						 const melt_argdescr_cell_t
						 meltxargdescr_[],
						 union meltparam_un *
						 meltxargtab_,
						 const melt_argdescr_cell_t
						 meltxresdescr_[],
						 union meltparam_un *
						 meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_101_warmelt_outobj_OUTPUCOD_OBJPUTUPLE_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_101_warmelt_outobj_OUTPUCOD_OBJPUTUPLE_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 25
    melt_ptr_t mcfr_varptr[25];
#define MELTFRAM_NBVARNUM 12
    long mcfr_varnum[12];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_101_warmelt_outobj_OUTPUCOD_OBJPUTUPLE is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_101_warmelt_outobj_OUTPUCOD_OBJPUTUPLE_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 25; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_101_warmelt_outobj_OUTPUCOD_OBJPUTUPLE nbval 25*/
  meltfram__.mcfr_nbvar = 25 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("OUTPUCOD_OBJPUTUPLE", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-outobj.melt:3884:/ getarg");
 /*_.OPTUP__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.DECLBUF__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.DECLBUF__V3*/ meltfptr[2])) !=
	      NULL);


  /*getarg#2 */
  /*^getarg */
  if (meltxargdescr_[1] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.IMPLBUF__V4*/ meltfptr[3] =
    (meltxargtab_[1].meltbp_aptr) ? (*(meltxargtab_[1].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3])) !=
	      NULL);


  /*getarg#3 */
  /*^getarg */
  if (meltxargdescr_[2] != MELTBPAR_LONG)
    goto lab_endgetargs;
 /*_#DEPTH__L1*/ meltfnum[0] = meltxargtab_[2].meltbp_long;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:3885:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L2*/ meltfnum[1] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.OPTUP__V2*/ meltfptr[1]),
			     (melt_ptr_t) (( /*!CLASS_OBJPUTUPLE */
					    meltfrout->tabval[0])));;
      MELT_LOCATION ("warmelt-outobj.melt:3885:/ cond");
      /*cond */ if ( /*_#IS_A__L2*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V6*/ meltfptr[5] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-outobj.melt:3885:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check optyp"),
				  ("warmelt-outobj.melt")
				  ? ("warmelt-outobj.melt") : __FILE__,
				  (3885) ? (3885) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V6*/ meltfptr[5] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V5*/ meltfptr[4] = /*_.IFELSE___V6*/ meltfptr[5];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:3885:/ clear");
	     /*clear *//*_#IS_A__L2*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V6*/ meltfptr[5] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V5*/ meltfptr[4] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-outobj.melt:3886:/ quasiblock");


    /*^getslot */
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.OPTUP__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 0, "OBI_LOC");
  /*_.ILOC__V8*/ meltfptr[7] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-outobj.melt:3887:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.OPTUP__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 1, "OPUTU_TUPLED");
  /*_.OTUP__V9*/ meltfptr[8] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-outobj.melt:3888:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.OPTUP__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 2, "OPUTU_OFFSET");
  /*_.OOFF__V10*/ meltfptr[9] = slot;
    };
    ;
 /*_#UNIQRANK__L3*/ meltfnum[1] = 0;;
    MELT_LOCATION ("warmelt-outobj.melt:3890:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.OPTUP__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 3, "OPUTU_VALUE");
  /*_.OVAL__V11*/ meltfptr[10] = slot;
    };
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:3893:/ locexp");
      {				/* outpucod_objputuple UNIQRANKSET__1 */
	static long UNIQRANKSET__1_cnt;
	UNIQRANKSET__1_cnt++;
		/*_#UNIQRANK__L3*/ meltfnum[1] = UNIQRANKSET__1_cnt;
      } /* end  outpucod_objputuple UNIQRANKSET__1 */ ;
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:3900:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    MELT_LOCATION ("warmelt-outobj.melt:3898:/ quasiblock");


    /*^multiapply */
    /*multiapply 1args, 1x.res */
    {

      union meltparam_un restab[1];
      memset (&restab, 0, sizeof (restab));
      /*^multiapply.xres */
      restab[0].meltbp_aptr = (melt_ptr_t *) & /*_.FILEV__V14*/ meltfptr[13];
      /*^multiapply.appl */
      /*_.LINEV__V13*/ meltfptr[12] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!LINE_AND_FILE_OF_LOCATION */ meltfrout->tabval[1])),
		    (melt_ptr_t) ( /*_.ILOC__V8*/ meltfptr[7]), (""),
		    (union meltparam_un *) 0, (MELTBPARSTR_PTR ""), restab);
    }
    ;
    /*^quasiblock */


    MELT_LOCATION ("warmelt-outobj.melt:3901:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[3];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.IMPLBUF__V4*/ meltfptr[3];
      /*^apply.arg */
      argtab[1].meltbp_long = /*_#DEPTH__L1*/ meltfnum[0];
      /*^apply.arg */
      argtab[2].meltbp_cstring = "putuple";
      /*_.OUTPUT_LOCATION__V15*/ meltfptr[14] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!OUTPUT_LOCATION */ meltfrout->tabval[2])),
		    (melt_ptr_t) ( /*_.ILOC__V8*/ meltfptr[7]),
		    (MELTBPARSTR_PTR MELTBPARSTR_LONG MELTBPARSTR_CSTRING ""),
		    argtab, "", (union meltparam_un *) 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:3902:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   ("/*putupl"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:3903:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   ("#"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:3904:/ locexp");
      meltgc_add_strbuf_dec ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			     ( /*_#UNIQRANK__L3*/ meltfnum[1]));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:3905:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   ("*/"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:3906:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				( /*_#DEPTH__L1*/ meltfnum[0]), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:3907:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   ("melt_assertmsg(\"putupl "));
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:3908:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#GET_INT__L4*/ meltfnum[3] =
      (melt_get_int ((melt_ptr_t) ( /*_.LINEV__V13*/ meltfptr[12])));;
    /*^compute */
 /*_#I__L5*/ meltfnum[4] =
      (( /*_#GET_INT__L4*/ meltfnum[3]) > (0));;
    MELT_LOCATION ("warmelt-outobj.melt:3908:/ cond");
    /*cond */ if ( /*_#I__L5*/ meltfnum[4])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{


	  {
	    MELT_LOCATION ("warmelt-outobj.melt:3910:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t)
				 ( /*_.IMPLBUF__V4*/ meltfptr[3]), ("["));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:3911:/ locexp");
	    /*add2sbuf_string */
	      meltgc_add_strbuf ((melt_ptr_t)
				 ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				 melt_string_str ((melt_ptr_t)
						  ( /*_.FILEV__V14*/
						   meltfptr[13])));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:3912:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t)
				 ( /*_.IMPLBUF__V4*/ meltfptr[3]), (":"));
	  }
	  ;
   /*_#GET_INT__L6*/ meltfnum[5] =
	    (melt_get_int ((melt_ptr_t) ( /*_.LINEV__V13*/ meltfptr[12])));;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:3913:/ locexp");
	    meltgc_add_strbuf_dec ((melt_ptr_t)
				   ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				   ( /*_#GET_INT__L6*/ meltfnum[5]));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:3914:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t)
				 ( /*_.IMPLBUF__V4*/ meltfptr[3]), ("] "));
	  }
	  ;
	  MELT_LOCATION ("warmelt-outobj.melt:3909:/ quasiblock");


	  /*epilog */

	  MELT_LOCATION ("warmelt-outobj.melt:3908:/ clear");
	     /*clear *//*_#GET_INT__L6*/ meltfnum[5] = 0;
	}
	;
      }				/*noelse */
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:3916:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   ("#"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:3917:/ locexp");
      meltgc_add_strbuf_dec ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			     ( /*_#UNIQRANK__L3*/ meltfnum[1]));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:3918:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   (" checktup\", melt_magic_discr((melt_ptr_t)("));
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:3919:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^msend */
    /*msend */
    {
      union meltparam_un argtab[3];
      memset (&argtab, 0, sizeof (argtab));
      /*^ojbmsend.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.DECLBUF__V3*/ meltfptr[2];
      /*^ojbmsend.arg */
      argtab[1].meltbp_aptr = (melt_ptr_t *) & /*_.IMPLBUF__V4*/ meltfptr[3];
      /*^ojbmsend.arg */
      argtab[2].meltbp_long = /*_#DEPTH__L1*/ meltfnum[0];
      /*_.OUTPUT_C_CODE__V16*/ meltfptr[15] =
	meltgc_send ((melt_ptr_t) ( /*_.OTUP__V9*/ meltfptr[8]),
		     (melt_ptr_t) (( /*!OUTPUT_C_CODE */ meltfrout->
				    tabval[3])),
		     (MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_LONG ""),
		     argtab, "", (union meltparam_un *) 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:3920:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   ("))== MELTOBMAG_MULTIPLE);"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:3921:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				( /*_#DEPTH__L1*/ meltfnum[0]), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:3922:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   ("melt_assertmsg(\"putupl "));
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:3923:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#GET_INT__L7*/ meltfnum[5] =
      (melt_get_int ((melt_ptr_t) ( /*_.LINEV__V13*/ meltfptr[12])));;
    /*^compute */
 /*_#I__L8*/ meltfnum[7] =
      (( /*_#GET_INT__L7*/ meltfnum[5]) > (0));;
    MELT_LOCATION ("warmelt-outobj.melt:3923:/ cond");
    /*cond */ if ( /*_#I__L8*/ meltfnum[7])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{


	  {
	    MELT_LOCATION ("warmelt-outobj.melt:3925:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t)
				 ( /*_.IMPLBUF__V4*/ meltfptr[3]), ("["));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:3926:/ locexp");
	    /*add2sbuf_string */
	      meltgc_add_strbuf ((melt_ptr_t)
				 ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				 melt_string_str ((melt_ptr_t)
						  ( /*_.FILEV__V14*/
						   meltfptr[13])));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:3927:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t)
				 ( /*_.IMPLBUF__V4*/ meltfptr[3]), (":"));
	  }
	  ;
   /*_#GET_INT__L9*/ meltfnum[8] =
	    (melt_get_int ((melt_ptr_t) ( /*_.LINEV__V13*/ meltfptr[12])));;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:3928:/ locexp");
	    meltgc_add_strbuf_dec ((melt_ptr_t)
				   ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				   ( /*_#GET_INT__L9*/ meltfnum[8]));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:3929:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t)
				 ( /*_.IMPLBUF__V4*/ meltfptr[3]), ("] "));
	  }
	  ;
	  MELT_LOCATION ("warmelt-outobj.melt:3924:/ quasiblock");


	  /*epilog */

	  MELT_LOCATION ("warmelt-outobj.melt:3923:/ clear");
	     /*clear *//*_#GET_INT__L9*/ meltfnum[8] = 0;
	}
	;
      }				/*noelse */
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:3931:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   ("#"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:3932:/ locexp");
      meltgc_add_strbuf_dec ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			     ( /*_#UNIQRANK__L3*/ meltfnum[1]));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:3933:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   (" checkoff\", ("));
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:3934:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^msend */
    /*msend */
    {
      union meltparam_un argtab[3];
      memset (&argtab, 0, sizeof (argtab));
      /*^ojbmsend.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.DECLBUF__V3*/ meltfptr[2];
      /*^ojbmsend.arg */
      argtab[1].meltbp_aptr = (melt_ptr_t *) & /*_.IMPLBUF__V4*/ meltfptr[3];
      /*^ojbmsend.arg */
      argtab[2].meltbp_long = /*_#DEPTH__L1*/ meltfnum[0];
      /*_.OUTPUT_C_CODE__V17*/ meltfptr[16] =
	meltgc_send ((melt_ptr_t) ( /*_.OOFF__V10*/ meltfptr[9]),
		     (melt_ptr_t) (( /*!OUTPUT_C_CODE */ meltfrout->
				    tabval[3])),
		     (MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_LONG ""),
		     argtab, "", (union meltparam_un *) 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:3935:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   (">=0 && "));
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:3936:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^msend */
    /*msend */
    {
      union meltparam_un argtab[3];
      memset (&argtab, 0, sizeof (argtab));
      /*^ojbmsend.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.DECLBUF__V3*/ meltfptr[2];
      /*^ojbmsend.arg */
      argtab[1].meltbp_aptr = (melt_ptr_t *) & /*_.IMPLBUF__V4*/ meltfptr[3];
      /*^ojbmsend.arg */
      argtab[2].meltbp_long = /*_#DEPTH__L1*/ meltfnum[0];
      /*_.OUTPUT_C_CODE__V18*/ meltfptr[17] =
	meltgc_send ((melt_ptr_t) ( /*_.OOFF__V10*/ meltfptr[9]),
		     (melt_ptr_t) (( /*!OUTPUT_C_CODE */ meltfrout->
				    tabval[3])),
		     (MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_LONG ""),
		     argtab, "", (union meltparam_un *) 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:3937:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   ("< melt_multiple_length((melt_ptr_t)("));
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:3938:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^msend */
    /*msend */
    {
      union meltparam_un argtab[3];
      memset (&argtab, 0, sizeof (argtab));
      /*^ojbmsend.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.DECLBUF__V3*/ meltfptr[2];
      /*^ojbmsend.arg */
      argtab[1].meltbp_aptr = (melt_ptr_t *) & /*_.IMPLBUF__V4*/ meltfptr[3];
      /*^ojbmsend.arg */
      argtab[2].meltbp_long = /*_#DEPTH__L1*/ meltfnum[0];
      /*_.OUTPUT_C_CODE__V19*/ meltfptr[18] =
	meltgc_send ((melt_ptr_t) ( /*_.OTUP__V9*/ meltfptr[8]),
		     (melt_ptr_t) (( /*!OUTPUT_C_CODE */ meltfrout->
				    tabval[3])),
		     (MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_LONG ""),
		     argtab, "", (union meltparam_un *) 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:3939:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   ("))));"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:3940:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				( /*_#DEPTH__L1*/ meltfnum[0]), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:3941:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   ("((meltmultiple_ptr_t)("));
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:3942:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^msend */
    /*msend */
    {
      union meltparam_un argtab[3];
      memset (&argtab, 0, sizeof (argtab));
      /*^ojbmsend.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.DECLBUF__V3*/ meltfptr[2];
      /*^ojbmsend.arg */
      argtab[1].meltbp_aptr = (melt_ptr_t *) & /*_.IMPLBUF__V4*/ meltfptr[3];
      /*^ojbmsend.arg */
      argtab[2].meltbp_long = /*_#DEPTH__L1*/ meltfnum[0];
      /*_.OUTPUT_C_CODE__V20*/ meltfptr[19] =
	meltgc_send ((melt_ptr_t) ( /*_.OTUP__V9*/ meltfptr[8]),
		     (melt_ptr_t) (( /*!OUTPUT_C_CODE */ meltfrout->
				    tabval[3])),
		     (MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_LONG ""),
		     argtab, "", (union meltparam_un *) 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:3943:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   ("))->tabval["));
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:3944:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^msend */
    /*msend */
    {
      union meltparam_un argtab[3];
      memset (&argtab, 0, sizeof (argtab));
      /*^ojbmsend.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.DECLBUF__V3*/ meltfptr[2];
      /*^ojbmsend.arg */
      argtab[1].meltbp_aptr = (melt_ptr_t *) & /*_.IMPLBUF__V4*/ meltfptr[3];
      /*^ojbmsend.arg */
      argtab[2].meltbp_long = /*_#DEPTH__L1*/ meltfnum[0];
      /*_.OUTPUT_C_CODE__V21*/ meltfptr[20] =
	meltgc_send ((melt_ptr_t) ( /*_.OOFF__V10*/ meltfptr[9]),
		     (melt_ptr_t) (( /*!OUTPUT_C_CODE */ meltfrout->
				    tabval[3])),
		     (MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_LONG ""),
		     argtab, "", (union meltparam_un *) 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:3945:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   ("] = (melt_ptr_t)("));
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:3946:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^msend */
    /*msend */
    {
      union meltparam_un argtab[3];
      memset (&argtab, 0, sizeof (argtab));
      /*^ojbmsend.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.DECLBUF__V3*/ meltfptr[2];
      /*^ojbmsend.arg */
      argtab[1].meltbp_aptr = (melt_ptr_t *) & /*_.IMPLBUF__V4*/ meltfptr[3];
      /*^ojbmsend.arg */
      argtab[2].meltbp_long = /*_#DEPTH__L1*/ meltfnum[0];
      /*_.OUTPUT_C_CODE__V22*/ meltfptr[21] =
	meltgc_send ((melt_ptr_t) ( /*_.OVAL__V11*/ meltfptr[10]),
		     (melt_ptr_t) (( /*!OUTPUT_C_CODE */ meltfrout->
				    tabval[3])),
		     (MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_LONG ""),
		     argtab, "", (union meltparam_un *) 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:3947:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   (");"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:3948:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				( /*_#DEPTH__L1*/ meltfnum[0]), 0);
    }
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:3949:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#STRBUF_USEDLENGTH__L10*/ meltfnum[8] =
	melt_strbuf_usedlength ((melt_ptr_t)
				( /*_.IMPLBUF__V4*/ meltfptr[3]));;
      MELT_LOCATION ("warmelt-outobj.melt:3950:/ cond");
      /*cond */ if (
		     /*ifisa */
		     melt_is_instance_of ((melt_ptr_t)
					  (( /*!BUFFER_LIMIT_CONT */
					    meltfrout->tabval[4])),
					  (melt_ptr_t) (( /*!CLASS_REFERENCE */ meltfrout->tabval[5])))
	)			/*then */
	{
	  /*^cond.then */
	  /*^getslot */
	  {
	    melt_ptr_t slot = NULL, obj = NULL;
	    obj =
	      (melt_ptr_t) (( /*!BUFFER_LIMIT_CONT */ meltfrout->
			     tabval[4])) /*=obj*/ ;
	    melt_object_get_field (slot, obj, 0, "REFERENCED_VALUE");
     /*_.REFERENCED_VALUE__V24*/ meltfptr[23] = slot;
	  };
	  ;
	}
      else
	{			/*^cond.else */

    /*_.REFERENCED_VALUE__V24*/ meltfptr[23] = NULL;;
	}
      ;
      /*^compute */
   /*_#GET_INT__L11*/ meltfnum[10] =
	(melt_get_int
	 ((melt_ptr_t) ( /*_.REFERENCED_VALUE__V24*/ meltfptr[23])));;
      /*^compute */
   /*_#I__L12*/ meltfnum[11] =
	(( /*_#STRBUF_USEDLENGTH__L10*/ meltfnum[8]) <
	 ( /*_#GET_INT__L11*/ meltfnum[10]));;
      MELT_LOCATION ("warmelt-outobj.melt:3949:/ cond");
      /*cond */ if ( /*_#I__L12*/ meltfnum[11])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V25*/ meltfptr[24] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-outobj.melt:3949:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check limited implbuf"),
				  ("warmelt-outobj.melt")
				  ? ("warmelt-outobj.melt") : __FILE__,
				  (3949) ? (3949) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V25*/ meltfptr[24] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V23*/ meltfptr[22] = /*_.IFELSE___V25*/ meltfptr[24];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:3949:/ clear");
	     /*clear *//*_#STRBUF_USEDLENGTH__L10*/ meltfnum[8] = 0;
      /*^clear */
	     /*clear *//*_.REFERENCED_VALUE__V24*/ meltfptr[23] = 0;
      /*^clear */
	     /*clear *//*_#GET_INT__L11*/ meltfnum[10] = 0;
      /*^clear */
	     /*clear *//*_#I__L12*/ meltfnum[11] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V25*/ meltfptr[24] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V23*/ meltfptr[22] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-outobj.melt:3898:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*_.MULTI___V12*/ meltfptr[11] = /*_.IFCPP___V23*/ meltfptr[22];;

    MELT_LOCATION ("warmelt-outobj.melt:3898:/ clear");
	   /*clear *//*_.OUTPUT_LOCATION__V15*/ meltfptr[14] = 0;
    /*^clear */
	   /*clear *//*_#GET_INT__L4*/ meltfnum[3] = 0;
    /*^clear */
	   /*clear *//*_#I__L5*/ meltfnum[4] = 0;
    /*^clear */
	   /*clear *//*_.OUTPUT_C_CODE__V16*/ meltfptr[15] = 0;
    /*^clear */
	   /*clear *//*_#GET_INT__L7*/ meltfnum[5] = 0;
    /*^clear */
	   /*clear *//*_#I__L8*/ meltfnum[7] = 0;
    /*^clear */
	   /*clear *//*_.OUTPUT_C_CODE__V17*/ meltfptr[16] = 0;
    /*^clear */
	   /*clear *//*_.OUTPUT_C_CODE__V18*/ meltfptr[17] = 0;
    /*^clear */
	   /*clear *//*_.OUTPUT_C_CODE__V19*/ meltfptr[18] = 0;
    /*^clear */
	   /*clear *//*_.OUTPUT_C_CODE__V20*/ meltfptr[19] = 0;
    /*^clear */
	   /*clear *//*_.OUTPUT_C_CODE__V21*/ meltfptr[20] = 0;
    /*^clear */
	   /*clear *//*_.OUTPUT_C_CODE__V22*/ meltfptr[21] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V23*/ meltfptr[22] = 0;

    /*^clear */
	   /*clear *//*_.FILEV__V14*/ meltfptr[13] = 0;
    /*_.LET___V7*/ meltfptr[5] = /*_.MULTI___V12*/ meltfptr[11];;

    MELT_LOCATION ("warmelt-outobj.melt:3886:/ clear");
	   /*clear *//*_.ILOC__V8*/ meltfptr[7] = 0;
    /*^clear */
	   /*clear *//*_.OTUP__V9*/ meltfptr[8] = 0;
    /*^clear */
	   /*clear *//*_.OOFF__V10*/ meltfptr[9] = 0;
    /*^clear */
	   /*clear *//*_#UNIQRANK__L3*/ meltfnum[1] = 0;
    /*^clear */
	   /*clear *//*_.OVAL__V11*/ meltfptr[10] = 0;
    /*^clear */
	   /*clear *//*_.MULTI___V12*/ meltfptr[11] = 0;
    MELT_LOCATION ("warmelt-outobj.melt:3884:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.LET___V7*/ meltfptr[5];;

    {
      MELT_LOCATION ("warmelt-outobj.melt:3884:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V5*/ meltfptr[4] = 0;
    /*^clear */
	   /*clear *//*_.LET___V7*/ meltfptr[5] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("OUTPUCOD_OBJPUTUPLE", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_101_warmelt_outobj_OUTPUCOD_OBJPUTUPLE_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_101_warmelt_outobj_OUTPUCOD_OBJPUTUPLE */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_102_warmelt_outobj_OUTPUCOD_OBJPUTPAIRHEAD (meltclosure_ptr_t
						     meltclosp_,
						     melt_ptr_t
						     meltfirstargp_,
						     const
						     melt_argdescr_cell_t
						     meltxargdescr_[],
						     union meltparam_un *
						     meltxargtab_,
						     const
						     melt_argdescr_cell_t
						     meltxresdescr_[],
						     union meltparam_un *
						     meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_102_warmelt_outobj_OUTPUCOD_OBJPUTPAIRHEAD_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_102_warmelt_outobj_OUTPUCOD_OBJPUTPAIRHEAD_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 17
    melt_ptr_t mcfr_varptr[17];
#define MELTFRAM_NBVARNUM 5
    long mcfr_varnum[5];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_102_warmelt_outobj_OUTPUCOD_OBJPUTPAIRHEAD is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_102_warmelt_outobj_OUTPUCOD_OBJPUTPAIRHEAD_st
	 *) meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 17; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_102_warmelt_outobj_OUTPUCOD_OBJPUTPAIRHEAD nbval 17*/
  meltfram__.mcfr_nbvar = 17 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("OUTPUCOD_OBJPUTPAIRHEAD", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-outobj.melt:3959:/ getarg");
 /*_.OPUT__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.DECLBUF__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.DECLBUF__V3*/ meltfptr[2])) !=
	      NULL);


  /*getarg#2 */
  /*^getarg */
  if (meltxargdescr_[1] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.IMPLBUF__V4*/ meltfptr[3] =
    (meltxargtab_[1].meltbp_aptr) ? (*(meltxargtab_[1].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3])) !=
	      NULL);


  /*getarg#3 */
  /*^getarg */
  if (meltxargdescr_[2] != MELTBPAR_LONG)
    goto lab_endgetargs;
 /*_#DEPTH__L1*/ meltfnum[0] = meltxargtab_[2].meltbp_long;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {

    MELT_LOCATION ("warmelt-outobj.melt:3960:/ quasiblock");


    /*^getslot */
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.OPUT__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 0, "OBI_LOC");
  /*_.OLOC__V6*/ meltfptr[5] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-outobj.melt:3961:/ cond");
    /*cond */ if (
		   /*ifisa */
		   melt_is_instance_of ((melt_ptr_t)
					( /*_.OPUT__V2*/ meltfptr[1]),
					(melt_ptr_t) (( /*!CLASS_OBJPUTPAIR */
						       meltfrout->tabval[0])))
      )				/*then */
      {
	/*^cond.then */
	/*^getslot */
	{
	  melt_ptr_t slot = NULL, obj = NULL;
	  obj = (melt_ptr_t) ( /*_.OPUT__V2*/ meltfptr[1]) /*=obj*/ ;
	  melt_object_get_field (slot, obj, 1, "OPUTP_PAIR");
   /*_.OPAIR__V7*/ meltfptr[6] = slot;
	};
	;
      }
    else
      {				/*^cond.else */

  /*_.OPAIR__V7*/ meltfptr[6] = NULL;;
      }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:3962:/ cond");
    /*cond */ if (
		   /*ifisa */
		   melt_is_instance_of ((melt_ptr_t)
					( /*_.OPUT__V2*/ meltfptr[1]),
					(melt_ptr_t) (( /*!CLASS_OBJPUTPAIRHEAD */ meltfrout->tabval[1])))
      )				/*then */
      {
	/*^cond.then */
	/*^getslot */
	{
	  melt_ptr_t slot = NULL, obj = NULL;
	  obj = (melt_ptr_t) ( /*_.OPUT__V2*/ meltfptr[1]) /*=obj*/ ;
	  melt_object_get_field (slot, obj, 2, "OPUTP_HEAD");
   /*_.OHEAD__V8*/ meltfptr[7] = slot;
	};
	;
      }
    else
      {				/*^cond.else */

  /*_.OHEAD__V8*/ meltfptr[7] = NULL;;
      }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:3963:/ cond");
    /*cond */ if (
		   /*ifisa */
		   melt_is_instance_of ((melt_ptr_t)
					(( /*!OBJPUTPAIRHEAD_COUNTER */
					  meltfrout->tabval[2])),
					(melt_ptr_t) (( /*!CLASS_REFERENCE */
						       meltfrout->tabval[3])))
      )				/*then */
      {
	/*^cond.then */
	/*^getslot */
	{
	  melt_ptr_t slot = NULL, obj = NULL;
	  obj =
	    (melt_ptr_t) (( /*!OBJPUTPAIRHEAD_COUNTER */ meltfrout->
			   tabval[2])) /*=obj*/ ;
	  melt_object_get_field (slot, obj, 0, "REFERENCED_VALUE");
   /*_.OLDCOUNT__V9*/ meltfptr[8] = slot;
	};
	;
      }
    else
      {				/*^cond.else */

  /*_.OLDCOUNT__V9*/ meltfptr[8] = NULL;;
      }
    ;
    /*^compute */
 /*_.NEWCOUNT__V10*/ meltfptr[9] =
      /*+ivi */
      ((melt_magic_discr ((melt_ptr_t) /*_.OLDCOUNT__V9*/ meltfptr[8]) ==
	MELTOBMAG_INT) ?
       (meltgc_new_int
	((meltobject_ptr_t) MELT_PREDEF (DISCR_CONSTANT_INTEGER),
	 (melt_unsafe_boxed_int_value
	  ((melt_ptr_t) /*_.OLDCOUNT__V9*/ meltfptr[8]) + (1))))
       : (melt_ptr_t) NULL);;
    MELT_LOCATION ("warmelt-outobj.melt:3966:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[3];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.IMPLBUF__V4*/ meltfptr[3];
      /*^apply.arg */
      argtab[1].meltbp_long = /*_#DEPTH__L1*/ meltfnum[0];
      /*^apply.arg */
      argtab[2].meltbp_cstring = "putpairhead";
      /*_.OUTPUT_LOCATION__V11*/ meltfptr[10] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!OUTPUT_LOCATION */ meltfrout->tabval[4])),
		    (melt_ptr_t) ( /*_.OLOC__V6*/ meltfptr[5]),
		    (MELTBPARSTR_PTR MELTBPARSTR_LONG MELTBPARSTR_CSTRING ""),
		    argtab, "", (union meltparam_un *) 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:3967:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   ("/*putpairhead*/"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:3968:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				( /*_#DEPTH__L1*/ meltfnum[0]), 0);
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:3969:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^cond */
    /*cond */ if (
		   /*ifisa */
		   melt_is_instance_of ((melt_ptr_t)
					(( /*!OBJPUTPAIRHEAD_COUNTER */
					  meltfrout->tabval[2])),
					(melt_ptr_t) (( /*!CLASS_REFERENCE */
						       meltfrout->tabval[3])))
      )				/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  /*^putslot */
	  /*putslot */
	  melt_assertmsg ("putslot checkobj @REFERENCED_VALUE",
			  melt_magic_discr ((melt_ptr_t)
					    (( /*!OBJPUTPAIRHEAD_COUNTER */
					      meltfrout->tabval[2]))) ==
			  MELTOBMAG_OBJECT);
	  melt_putfield_object ((( /*!OBJPUTPAIRHEAD_COUNTER */ meltfrout->
				  tabval[2])), (0),
				( /*_.NEWCOUNT__V10*/ meltfptr[9]),
				"REFERENCED_VALUE");
	  ;
	  /*^touch */
	  meltgc_touch (( /*!OBJPUTPAIRHEAD_COUNTER */ meltfrout->tabval[2]));
	  ;
	  /*^touchobj */

	  melt_dbgtrace_written_object (( /*!OBJPUTPAIRHEAD_COUNTER */
					 meltfrout->tabval[2]), "put-fields");
	  ;
	  /*epilog */
	}
	;
      }				/*noelse */
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:3970:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   ("melt_assertmsg(\"putpairhead /"));
    }
    ;
 /*_#GET_INT__L2*/ meltfnum[1] =
      (melt_get_int ((melt_ptr_t) ( /*_.NEWCOUNT__V10*/ meltfptr[9])));;

    {
      MELT_LOCATION ("warmelt-outobj.melt:3971:/ locexp");
      meltgc_add_strbuf_hex ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			     ( /*_#GET_INT__L2*/ meltfnum[1]));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:3972:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   (" checkpair\", melt_magic_discr((melt_ptr_t)("));
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:3973:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^msend */
    /*msend */
    {
      union meltparam_un argtab[3];
      memset (&argtab, 0, sizeof (argtab));
      /*^ojbmsend.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.DECLBUF__V3*/ meltfptr[2];
      /*^ojbmsend.arg */
      argtab[1].meltbp_aptr = (melt_ptr_t *) & /*_.IMPLBUF__V4*/ meltfptr[3];
      /*^ojbmsend.arg */
      argtab[2].meltbp_long = /*_#DEPTH__L1*/ meltfnum[0];
      /*_.OUTPUT_C_CODE__V12*/ meltfptr[11] =
	meltgc_send ((melt_ptr_t) ( /*_.OPAIR__V7*/ meltfptr[6]),
		     (melt_ptr_t) (( /*!OUTPUT_C_CODE */ meltfrout->
				    tabval[5])),
		     (MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_LONG ""),
		     argtab, "", (union meltparam_un *) 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:3974:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   ("))== MELTOBMAG_PAIR);"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:3975:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				( /*_#DEPTH__L1*/ meltfnum[0]), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:3976:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   ("((meltpair_ptr_t)("));
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:3977:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^msend */
    /*msend */
    {
      union meltparam_un argtab[3];
      memset (&argtab, 0, sizeof (argtab));
      /*^ojbmsend.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.DECLBUF__V3*/ meltfptr[2];
      /*^ojbmsend.arg */
      argtab[1].meltbp_aptr = (melt_ptr_t *) & /*_.IMPLBUF__V4*/ meltfptr[3];
      /*^ojbmsend.arg */
      argtab[2].meltbp_long = /*_#DEPTH__L1*/ meltfnum[0];
      /*_.OUTPUT_C_CODE__V13*/ meltfptr[12] =
	meltgc_send ((melt_ptr_t) ( /*_.OPAIR__V7*/ meltfptr[6]),
		     (melt_ptr_t) (( /*!OUTPUT_C_CODE */ meltfrout->
				    tabval[5])),
		     (MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_LONG ""),
		     argtab, "", (union meltparam_un *) 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:3978:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   ("))->hd = (melt_ptr_t) ("));
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:3979:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^msend */
    /*msend */
    {
      union meltparam_un argtab[3];
      memset (&argtab, 0, sizeof (argtab));
      /*^ojbmsend.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.DECLBUF__V3*/ meltfptr[2];
      /*^ojbmsend.arg */
      argtab[1].meltbp_aptr = (melt_ptr_t *) & /*_.IMPLBUF__V4*/ meltfptr[3];
      /*^ojbmsend.arg */
      argtab[2].meltbp_long = /*_#DEPTH__L1*/ meltfnum[0];
      /*_.OUTPUT_C_CODE__V14*/ meltfptr[13] =
	meltgc_send ((melt_ptr_t) ( /*_.OHEAD__V8*/ meltfptr[7]),
		     (melt_ptr_t) (( /*!OUTPUT_C_CODE */ meltfrout->
				    tabval[5])),
		     (MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_LONG ""),
		     argtab, "", (union meltparam_un *) 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:3980:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   (");"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:3981:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				( /*_#DEPTH__L1*/ meltfnum[0]), 0);
    }
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:3982:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#STRBUF_USEDLENGTH__L3*/ meltfnum[2] =
	melt_strbuf_usedlength ((melt_ptr_t)
				( /*_.IMPLBUF__V4*/ meltfptr[3]));;
      MELT_LOCATION ("warmelt-outobj.melt:3983:/ cond");
      /*cond */ if (
		     /*ifisa */
		     melt_is_instance_of ((melt_ptr_t)
					  (( /*!BUFFER_LIMIT_CONT */
					    meltfrout->tabval[6])),
					  (melt_ptr_t) (( /*!CLASS_REFERENCE */ meltfrout->tabval[3])))
	)			/*then */
	{
	  /*^cond.then */
	  /*^getslot */
	  {
	    melt_ptr_t slot = NULL, obj = NULL;
	    obj =
	      (melt_ptr_t) (( /*!BUFFER_LIMIT_CONT */ meltfrout->
			     tabval[6])) /*=obj*/ ;
	    melt_object_get_field (slot, obj, 0, "REFERENCED_VALUE");
     /*_.REFERENCED_VALUE__V16*/ meltfptr[15] = slot;
	  };
	  ;
	}
      else
	{			/*^cond.else */

    /*_.REFERENCED_VALUE__V16*/ meltfptr[15] = NULL;;
	}
      ;
      /*^compute */
   /*_#GET_INT__L4*/ meltfnum[3] =
	(melt_get_int
	 ((melt_ptr_t) ( /*_.REFERENCED_VALUE__V16*/ meltfptr[15])));;
      /*^compute */
   /*_#I__L5*/ meltfnum[4] =
	(( /*_#STRBUF_USEDLENGTH__L3*/ meltfnum[2]) <
	 ( /*_#GET_INT__L4*/ meltfnum[3]));;
      MELT_LOCATION ("warmelt-outobj.melt:3982:/ cond");
      /*cond */ if ( /*_#I__L5*/ meltfnum[4])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V17*/ meltfptr[16] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-outobj.melt:3982:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check limited implbuf"),
				  ("warmelt-outobj.melt")
				  ? ("warmelt-outobj.melt") : __FILE__,
				  (3982) ? (3982) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V17*/ meltfptr[16] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V15*/ meltfptr[14] = /*_.IFELSE___V17*/ meltfptr[16];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:3982:/ clear");
	     /*clear *//*_#STRBUF_USEDLENGTH__L3*/ meltfnum[2] = 0;
      /*^clear */
	     /*clear *//*_.REFERENCED_VALUE__V16*/ meltfptr[15] = 0;
      /*^clear */
	     /*clear *//*_#GET_INT__L4*/ meltfnum[3] = 0;
      /*^clear */
	     /*clear *//*_#I__L5*/ meltfnum[4] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V17*/ meltfptr[16] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V15*/ meltfptr[14] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    /*^compute */
    /*_.LET___V5*/ meltfptr[4] = /*_.IFCPP___V15*/ meltfptr[14];;

    MELT_LOCATION ("warmelt-outobj.melt:3960:/ clear");
	   /*clear *//*_.OLOC__V6*/ meltfptr[5] = 0;
    /*^clear */
	   /*clear *//*_.OPAIR__V7*/ meltfptr[6] = 0;
    /*^clear */
	   /*clear *//*_.OHEAD__V8*/ meltfptr[7] = 0;
    /*^clear */
	   /*clear *//*_.OLDCOUNT__V9*/ meltfptr[8] = 0;
    /*^clear */
	   /*clear *//*_.NEWCOUNT__V10*/ meltfptr[9] = 0;
    /*^clear */
	   /*clear *//*_.OUTPUT_LOCATION__V11*/ meltfptr[10] = 0;
    /*^clear */
	   /*clear *//*_#GET_INT__L2*/ meltfnum[1] = 0;
    /*^clear */
	   /*clear *//*_.OUTPUT_C_CODE__V12*/ meltfptr[11] = 0;
    /*^clear */
	   /*clear *//*_.OUTPUT_C_CODE__V13*/ meltfptr[12] = 0;
    /*^clear */
	   /*clear *//*_.OUTPUT_C_CODE__V14*/ meltfptr[13] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V15*/ meltfptr[14] = 0;
    MELT_LOCATION ("warmelt-outobj.melt:3959:/ quasiblock");


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.LET___V5*/ meltfptr[4];;

    {
      MELT_LOCATION ("warmelt-outobj.melt:3959:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.LET___V5*/ meltfptr[4] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("OUTPUCOD_OBJPUTPAIRHEAD", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_102_warmelt_outobj_OUTPUCOD_OBJPUTPAIRHEAD_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_102_warmelt_outobj_OUTPUCOD_OBJPUTPAIRHEAD */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_103_warmelt_outobj_OUTPUCOD_OBJPUTPAIRTAIL (meltclosure_ptr_t
						     meltclosp_,
						     melt_ptr_t
						     meltfirstargp_,
						     const
						     melt_argdescr_cell_t
						     meltxargdescr_[],
						     union meltparam_un *
						     meltxargtab_,
						     const
						     melt_argdescr_cell_t
						     meltxresdescr_[],
						     union meltparam_un *
						     meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_103_warmelt_outobj_OUTPUCOD_OBJPUTPAIRTAIL_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_103_warmelt_outobj_OUTPUCOD_OBJPUTPAIRTAIL_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 15
    melt_ptr_t mcfr_varptr[15];
#define MELTFRAM_NBVARNUM 5
    long mcfr_varnum[5];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_103_warmelt_outobj_OUTPUCOD_OBJPUTPAIRTAIL is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_103_warmelt_outobj_OUTPUCOD_OBJPUTPAIRTAIL_st
	 *) meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 15; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_103_warmelt_outobj_OUTPUCOD_OBJPUTPAIRTAIL nbval 15*/
  meltfram__.mcfr_nbvar = 15 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("OUTPUCOD_OBJPUTPAIRTAIL", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-outobj.melt:3988:/ getarg");
 /*_.OPUT__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.DECLBUF__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.DECLBUF__V3*/ meltfptr[2])) !=
	      NULL);


  /*getarg#2 */
  /*^getarg */
  if (meltxargdescr_[1] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.IMPLBUF__V4*/ meltfptr[3] =
    (meltxargtab_[1].meltbp_aptr) ? (*(meltxargtab_[1].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3])) !=
	      NULL);


  /*getarg#3 */
  /*^getarg */
  if (meltxargdescr_[2] != MELTBPAR_LONG)
    goto lab_endgetargs;
 /*_#DEPTH__L1*/ meltfnum[0] = meltxargtab_[2].meltbp_long;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {

    MELT_LOCATION ("warmelt-outobj.melt:3989:/ quasiblock");


    /*^getslot */
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.OPUT__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 0, "OBI_LOC");
  /*_.OLOC__V6*/ meltfptr[5] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-outobj.melt:3990:/ cond");
    /*cond */ if (
		   /*ifisa */
		   melt_is_instance_of ((melt_ptr_t)
					( /*_.OPUT__V2*/ meltfptr[1]),
					(melt_ptr_t) (( /*!CLASS_OBJPUTPAIR */
						       meltfrout->tabval[0])))
      )				/*then */
      {
	/*^cond.then */
	/*^getslot */
	{
	  melt_ptr_t slot = NULL, obj = NULL;
	  obj = (melt_ptr_t) ( /*_.OPUT__V2*/ meltfptr[1]) /*=obj*/ ;
	  melt_object_get_field (slot, obj, 1, "OPUTP_PAIR");
   /*_.OPAIR__V7*/ meltfptr[6] = slot;
	};
	;
      }
    else
      {				/*^cond.else */

  /*_.OPAIR__V7*/ meltfptr[6] = NULL;;
      }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:3991:/ cond");
    /*cond */ if (
		   /*ifisa */
		   melt_is_instance_of ((melt_ptr_t)
					( /*_.OPUT__V2*/ meltfptr[1]),
					(melt_ptr_t) (( /*!CLASS_OBJPUTPAIRTAIL */ meltfrout->tabval[1])))
      )				/*then */
      {
	/*^cond.then */
	/*^getslot */
	{
	  melt_ptr_t slot = NULL, obj = NULL;
	  obj = (melt_ptr_t) ( /*_.OPUT__V2*/ meltfptr[1]) /*=obj*/ ;
	  melt_object_get_field (slot, obj, 2, "OPUTP_TAIL");
   /*_.OTAIL__V8*/ meltfptr[7] = slot;
	};
	;
      }
    else
      {				/*^cond.else */

  /*_.OTAIL__V8*/ meltfptr[7] = NULL;;
      }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:3993:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[3];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.IMPLBUF__V4*/ meltfptr[3];
      /*^apply.arg */
      argtab[1].meltbp_long = /*_#DEPTH__L1*/ meltfnum[0];
      /*^apply.arg */
      argtab[2].meltbp_cstring = "putpairtail";
      /*_.OUTPUT_LOCATION__V9*/ meltfptr[8] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!OUTPUT_LOCATION */ meltfrout->tabval[2])),
		    (melt_ptr_t) ( /*_.OLOC__V6*/ meltfptr[5]),
		    (MELTBPARSTR_PTR MELTBPARSTR_LONG MELTBPARSTR_CSTRING ""),
		    argtab, "", (union meltparam_un *) 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:3994:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   ("/*putpairtail*/"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:3995:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				( /*_#DEPTH__L1*/ meltfnum[0]), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:3996:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   ("melt_assertmsg(\"putpairtail /"));
    }
    ;
 /*_#OBJ_HASH__L2*/ meltfnum[1] =
      (melt_obj_hash ((melt_ptr_t) ( /*_.OPUT__V2*/ meltfptr[1])));;

    {
      MELT_LOCATION ("warmelt-outobj.melt:3997:/ locexp");
      meltgc_add_strbuf_hex ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			     ( /*_#OBJ_HASH__L2*/ meltfnum[1]));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:3998:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   (" checkpair\", melt_magic_discr((melt_ptr_t)("));
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:3999:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^msend */
    /*msend */
    {
      union meltparam_un argtab[3];
      memset (&argtab, 0, sizeof (argtab));
      /*^ojbmsend.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.DECLBUF__V3*/ meltfptr[2];
      /*^ojbmsend.arg */
      argtab[1].meltbp_aptr = (melt_ptr_t *) & /*_.IMPLBUF__V4*/ meltfptr[3];
      /*^ojbmsend.arg */
      argtab[2].meltbp_long = /*_#DEPTH__L1*/ meltfnum[0];
      /*_.OUTPUT_C_CODE__V10*/ meltfptr[9] =
	meltgc_send ((melt_ptr_t) ( /*_.OPAIR__V7*/ meltfptr[6]),
		     (melt_ptr_t) (( /*!OUTPUT_C_CODE */ meltfrout->
				    tabval[3])),
		     (MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_LONG ""),
		     argtab, "", (union meltparam_un *) 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:4000:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   ("))== MELTOBMAG_PAIR);"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:4001:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				( /*_#DEPTH__L1*/ meltfnum[0]), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:4002:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   ("((meltpair_ptr_t)("));
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:4003:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^msend */
    /*msend */
    {
      union meltparam_un argtab[3];
      memset (&argtab, 0, sizeof (argtab));
      /*^ojbmsend.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.DECLBUF__V3*/ meltfptr[2];
      /*^ojbmsend.arg */
      argtab[1].meltbp_aptr = (melt_ptr_t *) & /*_.IMPLBUF__V4*/ meltfptr[3];
      /*^ojbmsend.arg */
      argtab[2].meltbp_long = /*_#DEPTH__L1*/ meltfnum[0];
      /*_.OUTPUT_C_CODE__V11*/ meltfptr[10] =
	meltgc_send ((melt_ptr_t) ( /*_.OPAIR__V7*/ meltfptr[6]),
		     (melt_ptr_t) (( /*!OUTPUT_C_CODE */ meltfrout->
				    tabval[3])),
		     (MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_LONG ""),
		     argtab, "", (union meltparam_un *) 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:4004:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   ("))->tl = (meltpair_ptr_t) ("));
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:4005:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^msend */
    /*msend */
    {
      union meltparam_un argtab[3];
      memset (&argtab, 0, sizeof (argtab));
      /*^ojbmsend.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.DECLBUF__V3*/ meltfptr[2];
      /*^ojbmsend.arg */
      argtab[1].meltbp_aptr = (melt_ptr_t *) & /*_.IMPLBUF__V4*/ meltfptr[3];
      /*^ojbmsend.arg */
      argtab[2].meltbp_long = /*_#DEPTH__L1*/ meltfnum[0];
      /*_.OUTPUT_C_CODE__V12*/ meltfptr[11] =
	meltgc_send ((melt_ptr_t) ( /*_.OTAIL__V8*/ meltfptr[7]),
		     (melt_ptr_t) (( /*!OUTPUT_C_CODE */ meltfrout->
				    tabval[3])),
		     (MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_LONG ""),
		     argtab, "", (union meltparam_un *) 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:4006:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   (");"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:4007:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				( /*_#DEPTH__L1*/ meltfnum[0]), 0);
    }
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:4008:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#STRBUF_USEDLENGTH__L3*/ meltfnum[2] =
	melt_strbuf_usedlength ((melt_ptr_t)
				( /*_.IMPLBUF__V4*/ meltfptr[3]));;
      MELT_LOCATION ("warmelt-outobj.melt:4009:/ cond");
      /*cond */ if (
		     /*ifisa */
		     melt_is_instance_of ((melt_ptr_t)
					  (( /*!BUFFER_LIMIT_CONT */
					    meltfrout->tabval[4])),
					  (melt_ptr_t) (( /*!CLASS_REFERENCE */ meltfrout->tabval[5])))
	)			/*then */
	{
	  /*^cond.then */
	  /*^getslot */
	  {
	    melt_ptr_t slot = NULL, obj = NULL;
	    obj =
	      (melt_ptr_t) (( /*!BUFFER_LIMIT_CONT */ meltfrout->
			     tabval[4])) /*=obj*/ ;
	    melt_object_get_field (slot, obj, 0, "REFERENCED_VALUE");
     /*_.REFERENCED_VALUE__V14*/ meltfptr[13] = slot;
	  };
	  ;
	}
      else
	{			/*^cond.else */

    /*_.REFERENCED_VALUE__V14*/ meltfptr[13] = NULL;;
	}
      ;
      /*^compute */
   /*_#GET_INT__L4*/ meltfnum[3] =
	(melt_get_int
	 ((melt_ptr_t) ( /*_.REFERENCED_VALUE__V14*/ meltfptr[13])));;
      /*^compute */
   /*_#I__L5*/ meltfnum[4] =
	(( /*_#STRBUF_USEDLENGTH__L3*/ meltfnum[2]) <
	 ( /*_#GET_INT__L4*/ meltfnum[3]));;
      MELT_LOCATION ("warmelt-outobj.melt:4008:/ cond");
      /*cond */ if ( /*_#I__L5*/ meltfnum[4])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V15*/ meltfptr[14] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-outobj.melt:4008:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check limited implbuf"),
				  ("warmelt-outobj.melt")
				  ? ("warmelt-outobj.melt") : __FILE__,
				  (4008) ? (4008) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V15*/ meltfptr[14] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V13*/ meltfptr[12] = /*_.IFELSE___V15*/ meltfptr[14];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:4008:/ clear");
	     /*clear *//*_#STRBUF_USEDLENGTH__L3*/ meltfnum[2] = 0;
      /*^clear */
	     /*clear *//*_.REFERENCED_VALUE__V14*/ meltfptr[13] = 0;
      /*^clear */
	     /*clear *//*_#GET_INT__L4*/ meltfnum[3] = 0;
      /*^clear */
	     /*clear *//*_#I__L5*/ meltfnum[4] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V15*/ meltfptr[14] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V13*/ meltfptr[12] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    /*^compute */
    /*_.LET___V5*/ meltfptr[4] = /*_.IFCPP___V13*/ meltfptr[12];;

    MELT_LOCATION ("warmelt-outobj.melt:3989:/ clear");
	   /*clear *//*_.OLOC__V6*/ meltfptr[5] = 0;
    /*^clear */
	   /*clear *//*_.OPAIR__V7*/ meltfptr[6] = 0;
    /*^clear */
	   /*clear *//*_.OTAIL__V8*/ meltfptr[7] = 0;
    /*^clear */
	   /*clear *//*_.OUTPUT_LOCATION__V9*/ meltfptr[8] = 0;
    /*^clear */
	   /*clear *//*_#OBJ_HASH__L2*/ meltfnum[1] = 0;
    /*^clear */
	   /*clear *//*_.OUTPUT_C_CODE__V10*/ meltfptr[9] = 0;
    /*^clear */
	   /*clear *//*_.OUTPUT_C_CODE__V11*/ meltfptr[10] = 0;
    /*^clear */
	   /*clear *//*_.OUTPUT_C_CODE__V12*/ meltfptr[11] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V13*/ meltfptr[12] = 0;
    MELT_LOCATION ("warmelt-outobj.melt:3988:/ quasiblock");


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.LET___V5*/ meltfptr[4];;

    {
      MELT_LOCATION ("warmelt-outobj.melt:3988:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.LET___V5*/ meltfptr[4] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("OUTPUCOD_OBJPUTPAIRTAIL", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_103_warmelt_outobj_OUTPUCOD_OBJPUTPAIRTAIL_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_103_warmelt_outobj_OUTPUCOD_OBJPUTPAIRTAIL */



/**** end of warmelt-outobj+03.c ****/
