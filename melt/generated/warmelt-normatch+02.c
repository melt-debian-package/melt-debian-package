/* GCC MELT GENERATED FILE warmelt-normatch+02.c - DO NOT EDIT */
/* secondary MELT generated C file of rank #2 */
#include "melt-run.h"


/* used hash from melt-run.h when compiling this file: */
MELT_EXTERN const char meltrun_used_md5_melt_f2[] =
  MELT_RUN_HASHMD5 /* from melt-run.h */ ;


/**** warmelt-normatch+02.c declarations ****/


/***************************************************
***
    Copyright 2008, 2009, 2010, 2011, 2012 Free Software Foundation, Inc.
    Contributed by Basile Starynkevitch <basile@starynkevitch.net>

    This file is part of GCC.

    GCC is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3, or (at your option)
    any later version.

    GCC is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with GCC; see the file COPYING3.  If not see
    <http://www.gnu.org/licenses/>.
***
****************************************************/

/* ordinary MELT module */
#define MELT_HAS_INITIAL_ENVIRONMENT 1	/*usual */

struct melt_callframe_st;	/*defined in melt-runtime.h */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_1_warmelt_normatch_SCANPAT_ANYRECV (meltclosure_ptr_t meltclosp_,
					     melt_ptr_t meltfirstargp_,
					     const melt_argdescr_cell_t
					     meltxargdescr_[],
					     union meltparam_un *meltxargtab_,
					     const melt_argdescr_cell_t
					     meltxresdescr_[],
					     union meltparam_un
					     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_2_warmelt_normatch_SCANPAT_ANYPATTERN (meltclosure_ptr_t meltclosp_,
						melt_ptr_t meltfirstargp_,
						const melt_argdescr_cell_t
						meltxargdescr_[],
						union meltparam_un
						*meltxargtab_,
						const melt_argdescr_cell_t
						meltxresdescr_[],
						union meltparam_un
						*meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_3_warmelt_normatch_SCANPAT_SRCPATVAR (meltclosure_ptr_t meltclosp_,
					       melt_ptr_t meltfirstargp_,
					       const melt_argdescr_cell_t
					       meltxargdescr_[],
					       union meltparam_un
					       *meltxargtab_,
					       const melt_argdescr_cell_t
					       meltxresdescr_[],
					       union meltparam_un
					       *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_4_warmelt_normatch_SCANPAT_SRCPATJOKER (meltclosure_ptr_t meltclosp_,
						 melt_ptr_t meltfirstargp_,
						 const melt_argdescr_cell_t
						 meltxargdescr_[],
						 union meltparam_un
						 *meltxargtab_,
						 const melt_argdescr_cell_t
						 meltxresdescr_[],
						 union meltparam_un
						 *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_5_warmelt_normatch_SCANPAT_SRCPATCONSTANT (meltclosure_ptr_t
						    meltclosp_,
						    melt_ptr_t meltfirstargp_,
						    const melt_argdescr_cell_t
						    meltxargdescr_[],
						    union meltparam_un
						    *meltxargtab_,
						    const melt_argdescr_cell_t
						    meltxresdescr_[],
						    union meltparam_un
						    *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_6_warmelt_normatch_SCANPAT_SRCPATCONSTRUCT (meltclosure_ptr_t
						     meltclosp_,
						     melt_ptr_t
						     meltfirstargp_,
						     const
						     melt_argdescr_cell_t
						     meltxargdescr_[],
						     union meltparam_un
						     *meltxargtab_,
						     const
						     melt_argdescr_cell_t
						     meltxresdescr_[],
						     union meltparam_un
						     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_7_warmelt_normatch_SCANPAT_SRCPATOBJECT (meltclosure_ptr_t
						  meltclosp_,
						  melt_ptr_t meltfirstargp_,
						  const melt_argdescr_cell_t
						  meltxargdescr_[],
						  union meltparam_un
						  *meltxargtab_,
						  const melt_argdescr_cell_t
						  meltxresdescr_[],
						  union meltparam_un
						  *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_8_warmelt_normatch_SCANPAT_SRCPATCOMPOSITE (meltclosure_ptr_t
						     meltclosp_,
						     melt_ptr_t
						     meltfirstargp_,
						     const
						     melt_argdescr_cell_t
						     meltxargdescr_[],
						     union meltparam_un
						     *meltxargtab_,
						     const
						     melt_argdescr_cell_t
						     meltxresdescr_[],
						     union meltparam_un
						     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_9_warmelt_normatch_SCANPAT_SRCPATOR (meltclosure_ptr_t meltclosp_,
					      melt_ptr_t meltfirstargp_,
					      const melt_argdescr_cell_t
					      meltxargdescr_[],
					      union meltparam_un
					      *meltxargtab_,
					      const melt_argdescr_cell_t
					      meltxresdescr_[],
					      union meltparam_un
					      *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_10_warmelt_normatch_LAMBDA___1__ (meltclosure_ptr_t meltclosp_,
					   melt_ptr_t meltfirstargp_,
					   const melt_argdescr_cell_t
					   meltxargdescr_[],
					   union meltparam_un *meltxargtab_,
					   const melt_argdescr_cell_t
					   meltxresdescr_[],
					   union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_11_warmelt_normatch_SCANPAT_SRCPATAND (meltclosure_ptr_t meltclosp_,
						melt_ptr_t meltfirstargp_,
						const melt_argdescr_cell_t
						meltxargdescr_[],
						union meltparam_un
						*meltxargtab_,
						const melt_argdescr_cell_t
						meltxresdescr_[],
						union meltparam_un
						*meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_12_warmelt_normatch_REGISTER_NEW_NORMTESTER (meltclosure_ptr_t
						      meltclosp_,
						      melt_ptr_t
						      meltfirstargp_,
						      const
						      melt_argdescr_cell_t
						      meltxargdescr_[],
						      union meltparam_un
						      *meltxargtab_,
						      const
						      melt_argdescr_cell_t
						      meltxresdescr_[],
						      union meltparam_un
						      *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_13_warmelt_normatch_PUT_TESTER_THEN (meltclosure_ptr_t meltclosp_,
					      melt_ptr_t meltfirstargp_,
					      const melt_argdescr_cell_t
					      meltxargdescr_[],
					      union meltparam_un
					      *meltxargtab_,
					      const melt_argdescr_cell_t
					      meltxresdescr_[],
					      union meltparam_un
					      *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_14_warmelt_normatch_SET_NEW_TESTER_LAST_THEN (meltclosure_ptr_t
						       meltclosp_,
						       melt_ptr_t
						       meltfirstargp_,
						       const
						       melt_argdescr_cell_t
						       meltxargdescr_[],
						       union meltparam_un
						       *meltxargtab_,
						       const
						       melt_argdescr_cell_t
						       meltxresdescr_[],
						       union meltparam_un
						       *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_15_warmelt_normatch_SET_NEW_TESTER_ALL_ELSES (meltclosure_ptr_t
						       meltclosp_,
						       melt_ptr_t
						       meltfirstargp_,
						       const
						       melt_argdescr_cell_t
						       meltxargdescr_[],
						       union meltparam_un
						       *meltxargtab_,
						       const
						       melt_argdescr_cell_t
						       meltxresdescr_[],
						       union meltparam_un
						       *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_16_warmelt_normatch_NORMPAT_ANYRECV (meltclosure_ptr_t meltclosp_,
					      melt_ptr_t meltfirstargp_,
					      const melt_argdescr_cell_t
					      meltxargdescr_[],
					      union meltparam_un
					      *meltxargtab_,
					      const melt_argdescr_cell_t
					      meltxresdescr_[],
					      union meltparam_un
					      *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_17_warmelt_normatch_NORMPAT_ANYPAT (meltclosure_ptr_t meltclosp_,
					     melt_ptr_t meltfirstargp_,
					     const melt_argdescr_cell_t
					     meltxargdescr_[],
					     union meltparam_un *meltxargtab_,
					     const melt_argdescr_cell_t
					     meltxresdescr_[],
					     union meltparam_un
					     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_18_warmelt_normatch_NORMVARPAT_GENREUSETEST (meltclosure_ptr_t
						      meltclosp_,
						      melt_ptr_t
						      meltfirstargp_,
						      const
						      melt_argdescr_cell_t
						      meltxargdescr_[],
						      union meltparam_un
						      *meltxargtab_,
						      const
						      melt_argdescr_cell_t
						      meltxresdescr_[],
						      union meltparam_un
						      *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_19_warmelt_normatch_LAMBDA___2__ (meltclosure_ptr_t meltclosp_,
					   melt_ptr_t meltfirstargp_,
					   const melt_argdescr_cell_t
					   meltxargdescr_[],
					   union meltparam_un *meltxargtab_,
					   const melt_argdescr_cell_t
					   meltxresdescr_[],
					   union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_20_warmelt_normatch_NORMPAT_VARIABLEPAT (meltclosure_ptr_t
						  meltclosp_,
						  melt_ptr_t meltfirstargp_,
						  const melt_argdescr_cell_t
						  meltxargdescr_[],
						  union meltparam_un
						  *meltxargtab_,
						  const melt_argdescr_cell_t
						  meltxresdescr_[],
						  union meltparam_un
						  *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_21_warmelt_normatch_NORMPAT_JOKERPAT (meltclosure_ptr_t meltclosp_,
					       melt_ptr_t meltfirstargp_,
					       const melt_argdescr_cell_t
					       meltxargdescr_[],
					       union meltparam_un
					       *meltxargtab_,
					       const melt_argdescr_cell_t
					       meltxresdescr_[],
					       union meltparam_un
					       *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_22_warmelt_normatch_NORMPAT_INSTANCEPAT (meltclosure_ptr_t
						  meltclosp_,
						  melt_ptr_t meltfirstargp_,
						  const melt_argdescr_cell_t
						  meltxargdescr_[],
						  union meltparam_un
						  *meltxargtab_,
						  const melt_argdescr_cell_t
						  meltxresdescr_[],
						  union meltparam_un
						  *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_23_warmelt_normatch_LAMBDA___3__ (meltclosure_ptr_t meltclosp_,
					   melt_ptr_t meltfirstargp_,
					   const melt_argdescr_cell_t
					   meltxargdescr_[],
					   union meltparam_un *meltxargtab_,
					   const melt_argdescr_cell_t
					   meltxresdescr_[],
					   union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_24_warmelt_normatch_LAMBDA___4__ (meltclosure_ptr_t meltclosp_,
					   melt_ptr_t meltfirstargp_,
					   const melt_argdescr_cell_t
					   meltxargdescr_[],
					   union meltparam_un *meltxargtab_,
					   const melt_argdescr_cell_t
					   meltxresdescr_[],
					   union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_25_warmelt_normatch_LAMBDA___5__ (meltclosure_ptr_t meltclosp_,
					   melt_ptr_t meltfirstargp_,
					   const melt_argdescr_cell_t
					   meltxargdescr_[],
					   union meltparam_un *meltxargtab_,
					   const melt_argdescr_cell_t
					   meltxresdescr_[],
					   union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_26_warmelt_normatch_NORMPAT_TUPLEPAT (meltclosure_ptr_t meltclosp_,
					       melt_ptr_t meltfirstargp_,
					       const melt_argdescr_cell_t
					       meltxargdescr_[],
					       union meltparam_un
					       *meltxargtab_,
					       const melt_argdescr_cell_t
					       meltxresdescr_[],
					       union meltparam_un
					       *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_27_warmelt_normatch_LAMBDA___6__ (meltclosure_ptr_t meltclosp_,
					   melt_ptr_t meltfirstargp_,
					   const melt_argdescr_cell_t
					   meltxargdescr_[],
					   union meltparam_un *meltxargtab_,
					   const melt_argdescr_cell_t
					   meltxresdescr_[],
					   union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_28_warmelt_normatch_LAMBDA___7__ (meltclosure_ptr_t meltclosp_,
					   melt_ptr_t meltfirstargp_,
					   const melt_argdescr_cell_t
					   meltxargdescr_[],
					   union meltparam_un *meltxargtab_,
					   const melt_argdescr_cell_t
					   meltxresdescr_[],
					   union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_29_warmelt_normatch_LAMBDA___8__ (meltclosure_ptr_t meltclosp_,
					   melt_ptr_t meltfirstargp_,
					   const melt_argdescr_cell_t
					   meltxargdescr_[],
					   union meltparam_un *meltxargtab_,
					   const melt_argdescr_cell_t
					   meltxresdescr_[],
					   union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_30_warmelt_normatch_NORMPAT_ANDPAT (meltclosure_ptr_t meltclosp_,
					     melt_ptr_t meltfirstargp_,
					     const melt_argdescr_cell_t
					     meltxargdescr_[],
					     union meltparam_un *meltxargtab_,
					     const melt_argdescr_cell_t
					     meltxresdescr_[],
					     union meltparam_un
					     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_31_warmelt_normatch_LAMBDA___9__ (meltclosure_ptr_t meltclosp_,
					   melt_ptr_t meltfirstargp_,
					   const melt_argdescr_cell_t
					   meltxargdescr_[],
					   union meltparam_un *meltxargtab_,
					   const melt_argdescr_cell_t
					   meltxresdescr_[],
					   union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_32_warmelt_normatch_NORMPAT_ORPAT (meltclosure_ptr_t meltclosp_,
					    melt_ptr_t meltfirstargp_,
					    const melt_argdescr_cell_t
					    meltxargdescr_[],
					    union meltparam_un *meltxargtab_,
					    const melt_argdescr_cell_t
					    meltxresdescr_[],
					    union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_33_warmelt_normatch_LAMBDA___10__ (meltclosure_ptr_t meltclosp_,
					    melt_ptr_t meltfirstargp_,
					    const melt_argdescr_cell_t
					    meltxargdescr_[],
					    union meltparam_un *meltxargtab_,
					    const melt_argdescr_cell_t
					    meltxresdescr_[],
					    union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_34_warmelt_normatch_LAMBDA___11__ (meltclosure_ptr_t meltclosp_,
					    melt_ptr_t meltfirstargp_,
					    const melt_argdescr_cell_t
					    meltxargdescr_[],
					    union meltparam_un *meltxargtab_,
					    const melt_argdescr_cell_t
					    meltxresdescr_[],
					    union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_35_warmelt_normatch_LAMBDA___12__ (meltclosure_ptr_t meltclosp_,
					    melt_ptr_t meltfirstargp_,
					    const melt_argdescr_cell_t
					    meltxargdescr_[],
					    union meltparam_un *meltxargtab_,
					    const melt_argdescr_cell_t
					    meltxresdescr_[],
					    union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_36_warmelt_normatch_NORMPAT_ANYMATCHPAT (meltclosure_ptr_t
						  meltclosp_,
						  melt_ptr_t meltfirstargp_,
						  const melt_argdescr_cell_t
						  meltxargdescr_[],
						  union meltparam_un
						  *meltxargtab_,
						  const melt_argdescr_cell_t
						  meltxresdescr_[],
						  union meltparam_un
						  *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_37_warmelt_normatch_LAMBDA___13__ (meltclosure_ptr_t meltclosp_,
					    melt_ptr_t meltfirstargp_,
					    const melt_argdescr_cell_t
					    meltxargdescr_[],
					    union meltparam_un *meltxargtab_,
					    const melt_argdescr_cell_t
					    meltxresdescr_[],
					    union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_38_warmelt_normatch_LAMBDA___14__ (meltclosure_ptr_t meltclosp_,
					    melt_ptr_t meltfirstargp_,
					    const melt_argdescr_cell_t
					    meltxargdescr_[],
					    union meltparam_un *meltxargtab_,
					    const melt_argdescr_cell_t
					    meltxresdescr_[],
					    union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_39_warmelt_normatch_LAMBDA___15__ (meltclosure_ptr_t meltclosp_,
					    melt_ptr_t meltfirstargp_,
					    const melt_argdescr_cell_t
					    meltxargdescr_[],
					    union meltparam_un *meltxargtab_,
					    const melt_argdescr_cell_t
					    meltxresdescr_[],
					    union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_40_warmelt_normatch_LAMBDA___16__ (meltclosure_ptr_t meltclosp_,
					    melt_ptr_t meltfirstargp_,
					    const melt_argdescr_cell_t
					    meltxargdescr_[],
					    union meltparam_un *meltxargtab_,
					    const melt_argdescr_cell_t
					    meltxresdescr_[],
					    union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_41_warmelt_normatch_NORMPAT_CONSTPAT (meltclosure_ptr_t meltclosp_,
					       melt_ptr_t meltfirstargp_,
					       const melt_argdescr_cell_t
					       meltxargdescr_[],
					       union meltparam_un
					       *meltxargtab_,
					       const melt_argdescr_cell_t
					       meltxresdescr_[],
					       union meltparam_un
					       *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_42_warmelt_normatch_LAMBDA___17__ (meltclosure_ptr_t meltclosp_,
					    melt_ptr_t meltfirstargp_,
					    const melt_argdescr_cell_t
					    meltxargdescr_[],
					    union meltparam_un *meltxargtab_,
					    const melt_argdescr_cell_t
					    meltxresdescr_[],
					    union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_43_warmelt_normatch_MATCH_GRAPHIC_OPTSET (meltclosure_ptr_t
						   meltclosp_,
						   melt_ptr_t meltfirstargp_,
						   const melt_argdescr_cell_t
						   meltxargdescr_[],
						   union meltparam_un
						   *meltxargtab_,
						   const melt_argdescr_cell_t
						   meltxresdescr_[],
						   union meltparam_un
						   *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_44_warmelt_normatch_MG_OUT_NODE_NAME (meltclosure_ptr_t meltclosp_,
					       melt_ptr_t meltfirstargp_,
					       const melt_argdescr_cell_t
					       meltxargdescr_[],
					       union meltparam_un
					       *meltxargtab_,
					       const melt_argdescr_cell_t
					       meltxresdescr_[],
					       union meltparam_un
					       *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_45_warmelt_normatch_MGLABEL_ANY (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_46_warmelt_normatch_MGLABEL_ANY_TEST (meltclosure_ptr_t meltclosp_,
					       melt_ptr_t meltfirstargp_,
					       const melt_argdescr_cell_t
					       meltxargdescr_[],
					       union meltparam_un
					       *meltxargtab_,
					       const melt_argdescr_cell_t
					       meltxresdescr_[],
					       union meltparam_un
					       *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_47_warmelt_normatch_MGLABEL_INSTANCE_TEST (meltclosure_ptr_t
						    meltclosp_,
						    melt_ptr_t meltfirstargp_,
						    const melt_argdescr_cell_t
						    meltxargdescr_[],
						    union meltparam_un
						    *meltxargtab_,
						    const melt_argdescr_cell_t
						    meltxresdescr_[],
						    union meltparam_un
						    *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_48_warmelt_normatch_MGLABEL_SUCCESS_TEST (meltclosure_ptr_t
						   meltclosp_,
						   melt_ptr_t meltfirstargp_,
						   const melt_argdescr_cell_t
						   meltxargdescr_[],
						   union meltparam_un
						   *meltxargtab_,
						   const melt_argdescr_cell_t
						   meltxresdescr_[],
						   union meltparam_un
						   *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_49_warmelt_normatch_MATCHGRAPHIC_TESTS (meltclosure_ptr_t meltclosp_,
						 melt_ptr_t meltfirstargp_,
						 const melt_argdescr_cell_t
						 meltxargdescr_[],
						 union meltparam_un
						 *meltxargtab_,
						 const melt_argdescr_cell_t
						 meltxresdescr_[],
						 union meltparam_un
						 *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_50_warmelt_normatch_MG_DRAW_MATCH_GRAPHVIZ_FILE (meltclosure_ptr_t
							  meltclosp_,
							  melt_ptr_t
							  meltfirstargp_,
							  const
							  melt_argdescr_cell_t
							  meltxargdescr_[],
							  union meltparam_un
							  *meltxargtab_,
							  const
							  melt_argdescr_cell_t
							  meltxresdescr_[],
							  union meltparam_un
							  *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_51_warmelt_normatch_NORMEXP_MATCH (meltclosure_ptr_t meltclosp_,
					    melt_ptr_t meltfirstargp_,
					    const melt_argdescr_cell_t
					    meltxargdescr_[],
					    union meltparam_un *meltxargtab_,
					    const melt_argdescr_cell_t
					    meltxresdescr_[],
					    union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_52_warmelt_normatch_LAMBDA___18__ (meltclosure_ptr_t meltclosp_,
					    melt_ptr_t meltfirstargp_,
					    const melt_argdescr_cell_t
					    meltxargdescr_[],
					    union meltparam_un *meltxargtab_,
					    const melt_argdescr_cell_t
					    meltxresdescr_[],
					    union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_53_warmelt_normatch_NORMBIND_MATCHBIND (meltclosure_ptr_t meltclosp_,
						 melt_ptr_t meltfirstargp_,
						 const melt_argdescr_cell_t
						 meltxargdescr_[],
						 union meltparam_un
						 *meltxargtab_,
						 const melt_argdescr_cell_t
						 meltxresdescr_[],
						 union meltparam_un
						 *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_54_warmelt_normatch_MAKE_MATCH_FLAG (meltclosure_ptr_t meltclosp_,
					      melt_ptr_t meltfirstargp_,
					      const melt_argdescr_cell_t
					      meltxargdescr_[],
					      union meltparam_un
					      *meltxargtab_,
					      const melt_argdescr_cell_t
					      meltxresdescr_[],
					      union meltparam_un
					      *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_55_warmelt_normatch_SCANSUBPAT_ANYRECV (meltclosure_ptr_t meltclosp_,
						 melt_ptr_t meltfirstargp_,
						 const melt_argdescr_cell_t
						 meltxargdescr_[],
						 union meltparam_un
						 *meltxargtab_,
						 const melt_argdescr_cell_t
						 meltxresdescr_[],
						 union meltparam_un
						 *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_56_warmelt_normatch_SCANSTEPDATA_ANYRECV (meltclosure_ptr_t
						   meltclosp_,
						   melt_ptr_t meltfirstargp_,
						   const melt_argdescr_cell_t
						   meltxargdescr_[],
						   union meltparam_un
						   *meltxargtab_,
						   const melt_argdescr_cell_t
						   meltxresdescr_[],
						   union meltparam_un
						   *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_57_warmelt_normatch_SCANSTEPFLAG_ANYRECV (meltclosure_ptr_t
						   meltclosp_,
						   melt_ptr_t meltfirstargp_,
						   const melt_argdescr_cell_t
						   meltxargdescr_[],
						   union meltparam_un
						   *meltxargtab_,
						   const melt_argdescr_cell_t
						   meltxresdescr_[],
						   union meltparam_un
						   *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_58_warmelt_normatch_SCANSUBPAT_NOOP (meltclosure_ptr_t meltclosp_,
					      melt_ptr_t meltfirstargp_,
					      const melt_argdescr_cell_t
					      meltxargdescr_[],
					      union meltparam_un
					      *meltxargtab_,
					      const melt_argdescr_cell_t
					      meltxresdescr_[],
					      union meltparam_un
					      *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_59_warmelt_normatch_SCANSUBPAT_OR (meltclosure_ptr_t meltclosp_,
					    melt_ptr_t meltfirstargp_,
					    const melt_argdescr_cell_t
					    meltxargdescr_[],
					    union meltparam_un *meltxargtab_,
					    const melt_argdescr_cell_t
					    meltxresdescr_[],
					    union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_60_warmelt_normatch_SCANSUBPAT_AND (meltclosure_ptr_t meltclosp_,
					     melt_ptr_t meltfirstargp_,
					     const melt_argdescr_cell_t
					     meltxargdescr_[],
					     union meltparam_un *meltxargtab_,
					     const melt_argdescr_cell_t
					     meltxresdescr_[],
					     union meltparam_un
					     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_61_warmelt_normatch_SCANSUBPAT_CONSTRUCT (meltclosure_ptr_t
						   meltclosp_,
						   melt_ptr_t meltfirstargp_,
						   const melt_argdescr_cell_t
						   meltxargdescr_[],
						   union meltparam_un
						   *meltxargtab_,
						   const melt_argdescr_cell_t
						   meltxresdescr_[],
						   union meltparam_un
						   *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_62_warmelt_normatch_SCANSUBPAT_OBJECT (meltclosure_ptr_t meltclosp_,
						melt_ptr_t meltfirstargp_,
						const melt_argdescr_cell_t
						meltxargdescr_[],
						union meltparam_un
						*meltxargtab_,
						const melt_argdescr_cell_t
						meltxresdescr_[],
						union meltparam_un
						*meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_63_warmelt_normatch_FILL_MATCHCASE (meltclosure_ptr_t meltclosp_,
					     melt_ptr_t meltfirstargp_,
					     const melt_argdescr_cell_t
					     meltxargdescr_[],
					     union meltparam_un *meltxargtab_,
					     const melt_argdescr_cell_t
					     meltxresdescr_[],
					     union meltparam_un
					     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_64_warmelt_normatch_LAMBDA___19__ (meltclosure_ptr_t meltclosp_,
					    melt_ptr_t meltfirstargp_,
					    const melt_argdescr_cell_t
					    meltxargdescr_[],
					    union meltparam_un *meltxargtab_,
					    const melt_argdescr_cell_t
					    meltxresdescr_[],
					    union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_65_warmelt_normatch_PUTTHEN_MATCHANY (meltclosure_ptr_t meltclosp_,
					       melt_ptr_t meltfirstargp_,
					       const melt_argdescr_cell_t
					       meltxargdescr_[],
					       union meltparam_un
					       *meltxargtab_,
					       const melt_argdescr_cell_t
					       meltxresdescr_[],
					       union meltparam_un
					       *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_66_warmelt_normatch_START_STEP (meltclosure_ptr_t meltclosp_,
					 melt_ptr_t meltfirstargp_,
					 const melt_argdescr_cell_t
					 meltxargdescr_[],
					 union meltparam_un *meltxargtab_,
					 const melt_argdescr_cell_t
					 meltxresdescr_[],
					 union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_67_warmelt_normatch_PUTTHEN_MATCHTHEN (meltclosure_ptr_t meltclosp_,
						melt_ptr_t meltfirstargp_,
						const melt_argdescr_cell_t
						meltxargdescr_[],
						union meltparam_un
						*meltxargtab_,
						const melt_argdescr_cell_t
						meltxresdescr_[],
						union meltparam_un
						*meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_68_warmelt_normatch_PUTTHEN_MATCHGROUP (meltclosure_ptr_t meltclosp_,
						 melt_ptr_t meltfirstargp_,
						 const melt_argdescr_cell_t
						 meltxargdescr_[],
						 union meltparam_un
						 *meltxargtab_,
						 const melt_argdescr_cell_t
						 meltxresdescr_[],
						 union meltparam_un
						 *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_69_warmelt_normatch_PUTELSE_MATCHANY (meltclosure_ptr_t meltclosp_,
					       melt_ptr_t meltfirstargp_,
					       const melt_argdescr_cell_t
					       meltxargdescr_[],
					       union meltparam_un
					       *meltxargtab_,
					       const melt_argdescr_cell_t
					       meltxresdescr_[],
					       union meltparam_un
					       *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_70_warmelt_normatch_PUTELSE_MATCHSTEPTHEN (meltclosure_ptr_t
						    meltclosp_,
						    melt_ptr_t meltfirstargp_,
						    const melt_argdescr_cell_t
						    meltxargdescr_[],
						    union meltparam_un
						    *meltxargtab_,
						    const melt_argdescr_cell_t
						    meltxresdescr_[],
						    union meltparam_un
						    *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_71_warmelt_normatch_PUTELSE_MATCHTEST (meltclosure_ptr_t meltclosp_,
						melt_ptr_t meltfirstargp_,
						const melt_argdescr_cell_t
						meltxargdescr_[],
						union meltparam_un
						*meltxargtab_,
						const melt_argdescr_cell_t
						meltxresdescr_[],
						union meltparam_un
						*meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_72_warmelt_normatch_PUTELSE_MATCHGROUP (meltclosure_ptr_t meltclosp_,
						 melt_ptr_t meltfirstargp_,
						 const melt_argdescr_cell_t
						 meltxargdescr_[],
						 union meltparam_un
						 *meltxargtab_,
						 const melt_argdescr_cell_t
						 meltxresdescr_[],
						 union meltparam_un
						 *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_73_warmelt_normatch_SCANSTEPDATA_TESTINS (meltclosure_ptr_t
						   meltclosp_,
						   melt_ptr_t meltfirstargp_,
						   const melt_argdescr_cell_t
						   meltxargdescr_[],
						   union meltparam_un
						   *meltxargtab_,
						   const melt_argdescr_cell_t
						   meltxresdescr_[],
						   union meltparam_un
						   *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_74_warmelt_normatch_SCANSTEPDATA_TESTTUPLE (meltclosure_ptr_t
						     meltclosp_,
						     melt_ptr_t
						     meltfirstargp_,
						     const
						     melt_argdescr_cell_t
						     meltxargdescr_[],
						     union meltparam_un
						     *meltxargtab_,
						     const
						     melt_argdescr_cell_t
						     meltxresdescr_[],
						     union meltparam_un
						     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_75_warmelt_normatch_SCANSTEPDATA_TESTWITHFLAG (meltclosure_ptr_t
							meltclosp_,
							melt_ptr_t
							meltfirstargp_,
							const
							melt_argdescr_cell_t
							meltxargdescr_[],
							union meltparam_un
							*meltxargtab_,
							const
							melt_argdescr_cell_t
							meltxresdescr_[],
							union meltparam_un
							*meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_76_warmelt_normatch_SCANSTEPDATA_TESTMATCHER (meltclosure_ptr_t
						       meltclosp_,
						       melt_ptr_t
						       meltfirstargp_,
						       const
						       melt_argdescr_cell_t
						       meltxargdescr_[],
						       union meltparam_un
						       *meltxargtab_,
						       const
						       melt_argdescr_cell_t
						       meltxresdescr_[],
						       union meltparam_un
						       *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_77_warmelt_normatch_SCANSTEPFLAG_STEPWITHFLAG (meltclosure_ptr_t
							meltclosp_,
							melt_ptr_t
							meltfirstargp_,
							const
							melt_argdescr_cell_t
							meltxargdescr_[],
							union meltparam_un
							*meltxargtab_,
							const
							melt_argdescr_cell_t
							meltxresdescr_[],
							union meltparam_un
							*meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_78_warmelt_normatch_SCANSTEPFLAG_STEPFLAGOPER (meltclosure_ptr_t
							meltclosp_,
							melt_ptr_t
							meltfirstargp_,
							const
							melt_argdescr_cell_t
							meltxargdescr_[],
							union meltparam_un
							*meltxargtab_,
							const
							melt_argdescr_cell_t
							meltxresdescr_[],
							union meltparam_un
							*meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_79_warmelt_normatch_SCANSTEPFLAG_STEPWITHDATA (meltclosure_ptr_t
							meltclosp_,
							melt_ptr_t
							meltfirstargp_,
							const
							melt_argdescr_cell_t
							meltxargdescr_[],
							union meltparam_un
							*meltxargtab_,
							const
							melt_argdescr_cell_t
							meltxresdescr_[],
							union meltparam_un
							*meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_80_warmelt_normatch_TRANSLPAT_ANYRECV (meltclosure_ptr_t meltclosp_,
						melt_ptr_t meltfirstargp_,
						const melt_argdescr_cell_t
						meltxargdescr_[],
						union meltparam_un
						*meltxargtab_,
						const melt_argdescr_cell_t
						meltxresdescr_[],
						union meltparam_un
						*meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_81_warmelt_normatch_TRANSLPAT_JOKERPAT (meltclosure_ptr_t meltclosp_,
						 melt_ptr_t meltfirstargp_,
						 const melt_argdescr_cell_t
						 meltxargdescr_[],
						 union meltparam_un
						 *meltxargtab_,
						 const melt_argdescr_cell_t
						 meltxresdescr_[],
						 union meltparam_un
						 *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_82_warmelt_normatch_TRANSLPAT_CONSTPAT (meltclosure_ptr_t meltclosp_,
						 melt_ptr_t meltfirstargp_,
						 const melt_argdescr_cell_t
						 meltxargdescr_[],
						 union meltparam_un
						 *meltxargtab_,
						 const melt_argdescr_cell_t
						 meltxresdescr_[],
						 union meltparam_un
						 *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_83_warmelt_normatch_TRANSLPAT_LISTPAT (meltclosure_ptr_t meltclosp_,
						melt_ptr_t meltfirstargp_,
						const melt_argdescr_cell_t
						meltxargdescr_[],
						union meltparam_un
						*meltxargtab_,
						const melt_argdescr_cell_t
						meltxresdescr_[],
						union meltparam_un
						*meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_84_warmelt_normatch_TRANSLPAT_TUPLEPAT (meltclosure_ptr_t meltclosp_,
						 melt_ptr_t meltfirstargp_,
						 const melt_argdescr_cell_t
						 meltxargdescr_[],
						 union meltparam_un
						 *meltxargtab_,
						 const melt_argdescr_cell_t
						 meltxresdescr_[],
						 union meltparam_un
						 *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_85_warmelt_normatch_LAMBDA___20__ (meltclosure_ptr_t meltclosp_,
					    melt_ptr_t meltfirstargp_,
					    const melt_argdescr_cell_t
					    meltxargdescr_[],
					    union meltparam_un *meltxargtab_,
					    const melt_argdescr_cell_t
					    meltxresdescr_[],
					    union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_86_warmelt_normatch_TRANSLPAT_INSPAT (meltclosure_ptr_t meltclosp_,
					       melt_ptr_t meltfirstargp_,
					       const melt_argdescr_cell_t
					       meltxargdescr_[],
					       union meltparam_un
					       *meltxargtab_,
					       const melt_argdescr_cell_t
					       meltxresdescr_[],
					       union meltparam_un
					       *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_87_warmelt_normatch_TRANSLPAT_VARPAT (meltclosure_ptr_t meltclosp_,
					       melt_ptr_t meltfirstargp_,
					       const melt_argdescr_cell_t
					       meltxargdescr_[],
					       union meltparam_un
					       *meltxargtab_,
					       const melt_argdescr_cell_t
					       meltxresdescr_[],
					       union meltparam_un
					       *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_88_warmelt_normatch_TRANSLPAT_ANDPAT (meltclosure_ptr_t meltclosp_,
					       melt_ptr_t meltfirstargp_,
					       const melt_argdescr_cell_t
					       meltxargdescr_[],
					       union meltparam_un
					       *meltxargtab_,
					       const melt_argdescr_cell_t
					       meltxresdescr_[],
					       union meltparam_un
					       *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_89_warmelt_normatch_TRANSLPAT_ORPAT (meltclosure_ptr_t meltclosp_,
					      melt_ptr_t meltfirstargp_,
					      const melt_argdescr_cell_t
					      meltxargdescr_[],
					      union meltparam_un
					      *meltxargtab_,
					      const melt_argdescr_cell_t
					      meltxresdescr_[],
					      union meltparam_un
					      *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_90_warmelt_normatch_LAMBDA___21__ (meltclosure_ptr_t meltclosp_,
					    melt_ptr_t meltfirstargp_,
					    const melt_argdescr_cell_t
					    meltxargdescr_[],
					    union meltparam_un *meltxargtab_,
					    const melt_argdescr_cell_t
					    meltxresdescr_[],
					    union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_91_warmelt_normatch_LAMBDA___22__ (meltclosure_ptr_t meltclosp_,
					    melt_ptr_t meltfirstargp_,
					    const melt_argdescr_cell_t
					    meltxargdescr_[],
					    union meltparam_un *meltxargtab_,
					    const melt_argdescr_cell_t
					    meltxresdescr_[],
					    union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_92_warmelt_normatch_TRANSLPAT_PATMAT (meltclosure_ptr_t meltclosp_,
					       melt_ptr_t meltfirstargp_,
					       const melt_argdescr_cell_t
					       meltxargdescr_[],
					       union meltparam_un
					       *meltxargtab_,
					       const melt_argdescr_cell_t
					       meltxresdescr_[],
					       union meltparam_un
					       *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_93_warmelt_normatch_MGALTSTEP_ANY (meltclosure_ptr_t meltclosp_,
					    melt_ptr_t meltfirstargp_,
					    const melt_argdescr_cell_t
					    meltxargdescr_[],
					    union meltparam_un *meltxargtab_,
					    const melt_argdescr_cell_t
					    meltxresdescr_[],
					    union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_94_warmelt_normatch_MGALTSTEP_STEPTESTVAR (meltclosure_ptr_t
						    meltclosp_,
						    melt_ptr_t meltfirstargp_,
						    const melt_argdescr_cell_t
						    meltxargdescr_[],
						    union meltparam_un
						    *meltxargtab_,
						    const melt_argdescr_cell_t
						    meltxresdescr_[],
						    union meltparam_un
						    *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_95_warmelt_normatch_MGALTSTEP_STEPTESTINST (meltclosure_ptr_t
						     meltclosp_,
						     melt_ptr_t
						     meltfirstargp_,
						     const
						     melt_argdescr_cell_t
						     meltxargdescr_[],
						     union meltparam_un
						     *meltxargtab_,
						     const
						     melt_argdescr_cell_t
						     meltxresdescr_[],
						     union meltparam_un
						     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_96_warmelt_normatch_MGALTSTEP_STEPTESTMULT (meltclosure_ptr_t
						     meltclosp_,
						     melt_ptr_t
						     meltfirstargp_,
						     const
						     melt_argdescr_cell_t
						     meltxargdescr_[],
						     union meltparam_un
						     *meltxargtab_,
						     const
						     melt_argdescr_cell_t
						     meltxresdescr_[],
						     union meltparam_un
						     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_97_warmelt_normatch_MGALTSTEP_STEPTESTGROUP (meltclosure_ptr_t
						      meltclosp_,
						      melt_ptr_t
						      meltfirstargp_,
						      const
						      melt_argdescr_cell_t
						      meltxargdescr_[],
						      union meltparam_un
						      *meltxargtab_,
						      const
						      melt_argdescr_cell_t
						      meltxresdescr_[],
						      union meltparam_un
						      *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_98_warmelt_normatch_MGALTSTEP_STEPTESTMATCHER (meltclosure_ptr_t
							meltclosp_,
							melt_ptr_t
							meltfirstargp_,
							const
							melt_argdescr_cell_t
							meltxargdescr_[],
							union meltparam_un
							*meltxargtab_,
							const
							melt_argdescr_cell_t
							meltxresdescr_[],
							union meltparam_un
							*meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_99_warmelt_normatch_MGALTSTEP_STEPSUCCESS (meltclosure_ptr_t
						    meltclosp_,
						    melt_ptr_t meltfirstargp_,
						    const melt_argdescr_cell_t
						    meltxargdescr_[],
						    union meltparam_un
						    *meltxargtab_,
						    const melt_argdescr_cell_t
						    meltxresdescr_[],
						    union meltparam_un
						    *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_100_warmelt_normatch_MGALTSTEP_STEPCLEAR (meltclosure_ptr_t
						   meltclosp_,
						   melt_ptr_t meltfirstargp_,
						   const melt_argdescr_cell_t
						   meltxargdescr_[],
						   union meltparam_un
						   *meltxargtab_,
						   const melt_argdescr_cell_t
						   meltxresdescr_[],
						   union meltparam_un
						   *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_101_warmelt_normatch_MGALTSTEP_STEPFLAGSET (meltclosure_ptr_t
						     meltclosp_,
						     melt_ptr_t
						     meltfirstargp_,
						     const
						     melt_argdescr_cell_t
						     meltxargdescr_[],
						     union meltparam_un
						     *meltxargtab_,
						     const
						     melt_argdescr_cell_t
						     meltxresdescr_[],
						     union meltparam_un
						     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_102_warmelt_normatch_MGALTSTEP_STEPFLAGOPER (meltclosure_ptr_t
						      meltclosp_,
						      melt_ptr_t
						      meltfirstargp_,
						      const
						      melt_argdescr_cell_t
						      meltxargdescr_[],
						      union meltparam_un
						      *meltxargtab_,
						      const
						      melt_argdescr_cell_t
						      meltxresdescr_[],
						      union meltparam_un
						      *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_103_warmelt_normatch_TRANSLATE_MATCHCASE (meltclosure_ptr_t
						   meltclosp_,
						   melt_ptr_t meltfirstargp_,
						   const melt_argdescr_cell_t
						   meltxargdescr_[],
						   union meltparam_un
						   *meltxargtab_,
						   const melt_argdescr_cell_t
						   meltxresdescr_[],
						   union meltparam_un
						   *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_104_warmelt_normatch_LAMBDA___23__ (meltclosure_ptr_t meltclosp_,
					     melt_ptr_t meltfirstargp_,
					     const melt_argdescr_cell_t
					     meltxargdescr_[],
					     union meltparam_un *meltxargtab_,
					     const melt_argdescr_cell_t
					     meltxresdescr_[],
					     union meltparam_un
					     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_105_warmelt_normatch_ALTMATCH_NORMALIZE_FLAG (meltclosure_ptr_t
						       meltclosp_,
						       melt_ptr_t
						       meltfirstargp_,
						       const
						       melt_argdescr_cell_t
						       meltxargdescr_[],
						       union meltparam_un
						       *meltxargtab_,
						       const
						       melt_argdescr_cell_t
						       meltxresdescr_[],
						       union meltparam_un
						       *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_106_warmelt_normatch_MATCH_DATA_UPDATE_DATA_STEPS_INDEX
(meltclosure_ptr_t meltclosp_, melt_ptr_t meltfirstargp_, const melt_argdescr_cell_t meltxargdescr_[],
union meltparam_un *meltxargtab_, const melt_argdescr_cell_t meltxresdescr_[],
union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_107_warmelt_normatch_MATCH_STEP_INDEX (meltclosure_ptr_t meltclosp_,
						melt_ptr_t meltfirstargp_,
						const melt_argdescr_cell_t
						meltxargdescr_[],
						union meltparam_un
						*meltxargtab_,
						const melt_argdescr_cell_t
						meltxresdescr_[],
						union meltparam_un
						*meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_108_warmelt_normatch_COMPLETE_NORMSTEP_IF_LAST (meltclosure_ptr_t
							 meltclosp_,
							 melt_ptr_t
							 meltfirstargp_,
							 const
							 melt_argdescr_cell_t
							 meltxargdescr_[],
							 union meltparam_un
							 *meltxargtab_,
							 const
							 melt_argdescr_cell_t
							 meltxresdescr_[],
							 union meltparam_un
							 *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_109_warmelt_normatch_NORMSTEP_ANYRECV (meltclosure_ptr_t meltclosp_,
						melt_ptr_t meltfirstargp_,
						const melt_argdescr_cell_t
						meltxargdescr_[],
						union meltparam_un
						*meltxargtab_,
						const melt_argdescr_cell_t
						meltxresdescr_[],
						union meltparam_un
						*meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_110_warmelt_normatch_NORMSTEP_MFLAGSET (meltclosure_ptr_t meltclosp_,
						 melt_ptr_t meltfirstargp_,
						 const melt_argdescr_cell_t
						 meltxargdescr_[],
						 union meltparam_un
						 *meltxargtab_,
						 const melt_argdescr_cell_t
						 meltxresdescr_[],
						 union meltparam_un
						 *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_111_warmelt_normatch_NORMSTEP_MFLAGCONJ (meltclosure_ptr_t
						  meltclosp_,
						  melt_ptr_t meltfirstargp_,
						  const melt_argdescr_cell_t
						  meltxargdescr_[],
						  union meltparam_un
						  *meltxargtab_,
						  const melt_argdescr_cell_t
						  meltxresdescr_[],
						  union meltparam_un
						  *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_112_warmelt_normatch_NORMSTEP_MTESTINSTANCE (meltclosure_ptr_t
						      meltclosp_,
						      melt_ptr_t
						      meltfirstargp_,
						      const
						      melt_argdescr_cell_t
						      meltxargdescr_[],
						      union meltparam_un
						      *meltxargtab_,
						      const
						      melt_argdescr_cell_t
						      meltxresdescr_[],
						      union meltparam_un
						      *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_113_warmelt_normatch_NORMSTEP_MTESTMULTIPLE (meltclosure_ptr_t
						      meltclosp_,
						      melt_ptr_t
						      meltfirstargp_,
						      const
						      melt_argdescr_cell_t
						      meltxargdescr_[],
						      union meltparam_un
						      *meltxargtab_,
						      const
						      melt_argdescr_cell_t
						      meltxresdescr_[],
						      union meltparam_un
						      *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_114_warmelt_normatch_NORMSTEP_MTESTVAR (meltclosure_ptr_t meltclosp_,
						 melt_ptr_t meltfirstargp_,
						 const melt_argdescr_cell_t
						 meltxargdescr_[],
						 union meltparam_un
						 *meltxargtab_,
						 const melt_argdescr_cell_t
						 meltxresdescr_[],
						 union meltparam_un
						 *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_115_warmelt_normatch_NORMSTEP_MGROUP (meltclosure_ptr_t meltclosp_,
					       melt_ptr_t meltfirstargp_,
					       const melt_argdescr_cell_t
					       meltxargdescr_[],
					       union meltparam_un
					       *meltxargtab_,
					       const melt_argdescr_cell_t
					       meltxresdescr_[],
					       union meltparam_un
					       *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_116_warmelt_normatch_LAMBDA___24__ (meltclosure_ptr_t meltclosp_,
					     melt_ptr_t meltfirstargp_,
					     const melt_argdescr_cell_t
					     meltxargdescr_[],
					     union meltparam_un *meltxargtab_,
					     const melt_argdescr_cell_t
					     meltxresdescr_[],
					     union meltparam_un
					     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_117_warmelt_normatch_LAMBDA___25__ (meltclosure_ptr_t meltclosp_,
					     melt_ptr_t meltfirstargp_,
					     const melt_argdescr_cell_t
					     meltxargdescr_[],
					     union meltparam_un *meltxargtab_,
					     const melt_argdescr_cell_t
					     meltxresdescr_[],
					     union meltparam_un
					     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_118_warmelt_normatch_LAMBDA___26__ (meltclosure_ptr_t meltclosp_,
					     melt_ptr_t meltfirstargp_,
					     const melt_argdescr_cell_t
					     meltxargdescr_[],
					     union meltparam_un *meltxargtab_,
					     const melt_argdescr_cell_t
					     meltxresdescr_[],
					     union meltparam_un
					     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_119_warmelt_normatch_LAMBDA___27__ (meltclosure_ptr_t meltclosp_,
					     melt_ptr_t meltfirstargp_,
					     const melt_argdescr_cell_t
					     meltxargdescr_[],
					     union meltparam_un *meltxargtab_,
					     const melt_argdescr_cell_t
					     meltxresdescr_[],
					     union meltparam_un
					     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_120_warmelt_normatch_NORMSTEP_MSUCCWHENFLAG (meltclosure_ptr_t
						      meltclosp_,
						      melt_ptr_t
						      meltfirstargp_,
						      const
						      melt_argdescr_cell_t
						      meltxargdescr_[],
						      union meltparam_un
						      *meltxargtab_,
						      const
						      melt_argdescr_cell_t
						      meltxresdescr_[],
						      union meltparam_un
						      *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_121_warmelt_normatch_NORMTESTMATCH_CATCHALL (meltclosure_ptr_t
						      meltclosp_,
						      melt_ptr_t
						      meltfirstargp_,
						      const
						      melt_argdescr_cell_t
						      meltxargdescr_[],
						      union meltparam_un
						      *meltxargtab_,
						      const
						      melt_argdescr_cell_t
						      meltxresdescr_[],
						      union meltparam_un
						      *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_122_warmelt_normatch_NORMFILLMATCH_CATCHALL (meltclosure_ptr_t
						      meltclosp_,
						      melt_ptr_t
						      meltfirstargp_,
						      const
						      melt_argdescr_cell_t
						      meltxargdescr_[],
						      union meltparam_un
						      *meltxargtab_,
						      const
						      melt_argdescr_cell_t
						      meltxresdescr_[],
						      union meltparam_un
						      *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_123_warmelt_normatch_NORMTESTMATCH_CMATCHER (meltclosure_ptr_t
						      meltclosp_,
						      melt_ptr_t
						      meltfirstargp_,
						      const
						      melt_argdescr_cell_t
						      meltxargdescr_[],
						      union meltparam_un
						      *meltxargtab_,
						      const
						      melt_argdescr_cell_t
						      meltxresdescr_[],
						      union meltparam_un
						      *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_124_warmelt_normatch_NORMFILLMATCH_CMATCHER (meltclosure_ptr_t
						      meltclosp_,
						      melt_ptr_t
						      meltfirstargp_,
						      const
						      melt_argdescr_cell_t
						      meltxargdescr_[],
						      union meltparam_un
						      *meltxargtab_,
						      const
						      melt_argdescr_cell_t
						      meltxresdescr_[],
						      union meltparam_un
						      *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_125_warmelt_normatch_NORMSTEP_MTESTMATCHER (meltclosure_ptr_t
						     meltclosp_,
						     melt_ptr_t
						     meltfirstargp_,
						     const
						     melt_argdescr_cell_t
						     meltxargdescr_[],
						     union meltparam_un
						     *meltxargtab_,
						     const
						     melt_argdescr_cell_t
						     meltxresdescr_[],
						     union meltparam_un
						     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_126_warmelt_normatch_ALTMATCH_NORMALIZE_STEP (meltclosure_ptr_t
						       meltclosp_,
						       melt_ptr_t
						       meltfirstargp_,
						       const
						       melt_argdescr_cell_t
						       meltxargdescr_[],
						       union meltparam_un
						       *meltxargtab_,
						       const
						       melt_argdescr_cell_t
						       meltxresdescr_[],
						       union meltparam_un
						       *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_127_warmelt_normatch_ALTMATCH_NORMALIZE_MDATA (meltclosure_ptr_t
							meltclosp_,
							melt_ptr_t
							meltfirstargp_,
							const
							melt_argdescr_cell_t
							meltxargdescr_[],
							union meltparam_un
							*meltxargtab_,
							const
							melt_argdescr_cell_t
							meltxresdescr_[],
							union meltparam_un
							*meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_128_warmelt_normatch_ALTMATCH_HANDLE_NORMALIZED_MDATA
(meltclosure_ptr_t meltclosp_, melt_ptr_t meltfirstargp_, const melt_argdescr_cell_t meltxargdescr_[],
union meltparam_un *meltxargtab_, const melt_argdescr_cell_t meltxresdescr_[],
union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_129_warmelt_normatch_ALTMATCH_MAKE_MATCH_NORMALIZATION_CONTEXT
(meltclosure_ptr_t meltclosp_, melt_ptr_t meltfirstargp_, const melt_argdescr_cell_t meltxargdescr_[],
union meltparam_un *meltxargtab_, const melt_argdescr_cell_t meltxresdescr_[],
union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_130_warmelt_normatch_ALTMATCH_NORMALIZE_MATCH_CONTEXT
(meltclosure_ptr_t meltclosp_, melt_ptr_t meltfirstargp_, const melt_argdescr_cell_t meltxargdescr_[],
union meltparam_un *meltxargtab_, const melt_argdescr_cell_t meltxresdescr_[],
union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_131_warmelt_normatch_LAMBDA___28__ (meltclosure_ptr_t meltclosp_,
					     melt_ptr_t meltfirstargp_,
					     const melt_argdescr_cell_t
					     meltxargdescr_[],
					     union meltparam_un *meltxargtab_,
					     const melt_argdescr_cell_t
					     meltxresdescr_[],
					     union meltparam_un
					     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_132_warmelt_normatch_NORMEXP_ALTMATCH (meltclosure_ptr_t meltclosp_,
						melt_ptr_t meltfirstargp_,
						const melt_argdescr_cell_t
						meltxargdescr_[],
						union meltparam_un
						*meltxargtab_,
						const melt_argdescr_cell_t
						meltxresdescr_[],
						union meltparam_un
						*meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_133_warmelt_normatch_LAMBDA___29__ (meltclosure_ptr_t meltclosp_,
					     melt_ptr_t meltfirstargp_,
					     const melt_argdescr_cell_t
					     meltxargdescr_[],
					     union meltparam_un *meltxargtab_,
					     const melt_argdescr_cell_t
					     meltxresdescr_[],
					     union meltparam_un
					     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_134_warmelt_normatch_ALTERNATE_MATCH_OPTSET (meltclosure_ptr_t
						      meltclosp_,
						      melt_ptr_t
						      meltfirstargp_,
						      const
						      melt_argdescr_cell_t
						      meltxargdescr_[],
						      union meltparam_un
						      *meltxargtab_,
						      const
						      melt_argdescr_cell_t
						      meltxresdescr_[],
						      union meltparam_un
						      *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_135_warmelt_normatch_MG_ALTDRAW_GRAPHVIZ (meltclosure_ptr_t
						   meltclosp_,
						   melt_ptr_t meltfirstargp_,
						   const melt_argdescr_cell_t
						   meltxargdescr_[],
						   union meltparam_un
						   *meltxargtab_,
						   const melt_argdescr_cell_t
						   meltxresdescr_[],
						   union meltparam_un
						   *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_136_warmelt_normatch_LAMBDA___30__ (meltclosure_ptr_t meltclosp_,
					     melt_ptr_t meltfirstargp_,
					     const melt_argdescr_cell_t
					     meltxargdescr_[],
					     union meltparam_un *meltxargtab_,
					     const melt_argdescr_cell_t
					     meltxresdescr_[],
					     union meltparam_un
					     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_137_warmelt_normatch_LAMBDA___31__ (meltclosure_ptr_t meltclosp_,
					     melt_ptr_t meltfirstargp_,
					     const melt_argdescr_cell_t
					     meltxargdescr_[],
					     union meltparam_un *meltxargtab_,
					     const melt_argdescr_cell_t
					     meltxresdescr_[],
					     union meltparam_un
					     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_138_warmelt_normatch_LAMBDA___32__ (meltclosure_ptr_t meltclosp_,
					     melt_ptr_t meltfirstargp_,
					     const melt_argdescr_cell_t
					     meltxargdescr_[],
					     union meltparam_un *meltxargtab_,
					     const melt_argdescr_cell_t
					     meltxresdescr_[],
					     union meltparam_un
					     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_139_warmelt_normatch_LAMBDA___33__ (meltclosure_ptr_t meltclosp_,
					     melt_ptr_t meltfirstargp_,
					     const melt_argdescr_cell_t
					     meltxargdescr_[],
					     union meltparam_un *meltxargtab_,
					     const melt_argdescr_cell_t
					     meltxresdescr_[],
					     union meltparam_un
					     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_140_warmelt_normatch_LAMBDA___34__ (meltclosure_ptr_t meltclosp_,
					     melt_ptr_t meltfirstargp_,
					     const melt_argdescr_cell_t
					     meltxargdescr_[],
					     union meltparam_un *meltxargtab_,
					     const melt_argdescr_cell_t
					     meltxresdescr_[],
					     union meltparam_un
					     *meltxrestab_);



MELT_EXTERN void *melt_start_this_module (void *);


/* define different names when debugging or not */
#if MELT_HAVE_DEBUG
MELT_EXTERN const char meltmodule_warmelt_normatch__melt_have_debug_enabled[];
#define melt_have_debug_string meltmodule_warmelt_normatch__melt_have_debug_enabled
#else /*!MELT_HAVE_DEBUG */
MELT_EXTERN const char
  meltmodule_warmelt_normatch__melt_have_debug_disabled[];
#define melt_have_debug_string meltmodule_warmelt_normatch__melt_have_debug_disabled
#endif /*!MELT_HAVE_DEBUG */


struct frame_melt_start_this_module_st;
void MELT_MODULE_VISIBILITY
meltmod__warmelt_normatch__initialmeltchunk_0 (struct
					       frame_melt_start_this_module_st
					       *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_normatch__initialmeltchunk_1 (struct
					       frame_melt_start_this_module_st
					       *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_normatch__initialmeltchunk_2 (struct
					       frame_melt_start_this_module_st
					       *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_normatch__initialmeltchunk_3 (struct
					       frame_melt_start_this_module_st
					       *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_normatch__initialmeltchunk_4 (struct
					       frame_melt_start_this_module_st
					       *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_normatch__initialmeltchunk_5 (struct
					       frame_melt_start_this_module_st
					       *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_normatch__initialmeltchunk_6 (struct
					       frame_melt_start_this_module_st
					       *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_normatch__initialmeltchunk_7 (struct
					       frame_melt_start_this_module_st
					       *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_normatch__initialmeltchunk_8 (struct
					       frame_melt_start_this_module_st
					       *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_normatch__initialmeltchunk_9 (struct
					       frame_melt_start_this_module_st
					       *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_normatch__initialmeltchunk_10 (struct
						frame_melt_start_this_module_st
						*, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_normatch__initialmeltchunk_11 (struct
						frame_melt_start_this_module_st
						*, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_normatch__initialmeltchunk_12 (struct
						frame_melt_start_this_module_st
						*, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_normatch__initialmeltchunk_13 (struct
						frame_melt_start_this_module_st
						*, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_normatch__initialmeltchunk_14 (struct
						frame_melt_start_this_module_st
						*, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_normatch__initialmeltchunk_15 (struct
						frame_melt_start_this_module_st
						*, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_normatch__initialmeltchunk_16 (struct
						frame_melt_start_this_module_st
						*, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_normatch__initialmeltchunk_17 (struct
						frame_melt_start_this_module_st
						*, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_normatch__initialmeltchunk_18 (struct
						frame_melt_start_this_module_st
						*, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_normatch__initialmeltchunk_19 (struct
						frame_melt_start_this_module_st
						*, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_normatch__initialmeltchunk_20 (struct
						frame_melt_start_this_module_st
						*, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_normatch__initialmeltchunk_21 (struct
						frame_melt_start_this_module_st
						*, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_normatch__initialmeltchunk_22 (struct
						frame_melt_start_this_module_st
						*, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_normatch__initialmeltchunk_23 (struct
						frame_melt_start_this_module_st
						*, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_normatch__initialmeltchunk_24 (struct
						frame_melt_start_this_module_st
						*, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_normatch__initialmeltchunk_25 (struct
						frame_melt_start_this_module_st
						*, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_normatch__initialmeltchunk_26 (struct
						frame_melt_start_this_module_st
						*, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_normatch__initialmeltchunk_27 (struct
						frame_melt_start_this_module_st
						*, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_normatch__initialmeltchunk_28 (struct
						frame_melt_start_this_module_st
						*, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_normatch__initialmeltchunk_29 (struct
						frame_melt_start_this_module_st
						*, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_normatch__initialmeltchunk_30 (struct
						frame_melt_start_this_module_st
						*, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_normatch__initialmeltchunk_31 (struct
						frame_melt_start_this_module_st
						*, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_normatch__initialmeltchunk_32 (struct
						frame_melt_start_this_module_st
						*, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_normatch__initialmeltchunk_33 (struct
						frame_melt_start_this_module_st
						*, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_normatch__forward_or_mark_module_start_frame (struct
							       melt_callframe_st
							       *fp,
							       int marking);
#define meltmarking_melt_start_this_module  meltmod__warmelt_normatch__forward_or_mark_module_start_frame



/**** warmelt-normatch+02.c implementations ****/




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_47_warmelt_normatch_MGLABEL_INSTANCE_TEST (meltclosure_ptr_t
						    meltclosp_,
						    melt_ptr_t meltfirstargp_,
						    const melt_argdescr_cell_t
						    meltxargdescr_[],
						    union meltparam_un
						    *meltxargtab_,
						    const melt_argdescr_cell_t
						    meltxresdescr_[],
						    union meltparam_un
						    *meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_47_warmelt_normatch_MGLABEL_INSTANCE_TEST_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_47_warmelt_normatch_MGLABEL_INSTANCE_TEST_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 37
    melt_ptr_t mcfr_varptr[37];
#define MELTFRAM_NBVARNUM 16
    long mcfr_varnum[16];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_47_warmelt_normatch_MGLABEL_INSTANCE_TEST is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_47_warmelt_normatch_MGLABEL_INSTANCE_TEST_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 37; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_47_warmelt_normatch_MGLABEL_INSTANCE_TEST nbval 37*/
  meltfram__.mcfr_nbvar = 37 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("MGLABEL_INSTANCE_TEST", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-normatch.melt:2055:/ getarg");
 /*_.NTEST__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.MG__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.MG__V3*/ meltfptr[2])) != NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-normatch.melt:2056:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L1*/ meltfnum[0] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-normatch.melt:2056:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L1*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-normatch.melt:2056:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-normatch.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 2056;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "mglabel_instance_test start ntest";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.NTEST__V2*/ meltfptr[1];
	      /*_.MELT_DEBUG_FUN__V6*/ meltfptr[5] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V5*/ meltfptr[4] = /*_.MELT_DEBUG_FUN__V6*/ meltfptr[5];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-normatch.melt:2056:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V6*/ meltfptr[5] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V5*/ meltfptr[4] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-normatch.melt:2056:/ quasiblock");


      /*_.PROGN___V7*/ meltfptr[5] = /*_.IF___V5*/ meltfptr[4];;
      /*^compute */
      /*_.IFCPP___V4*/ meltfptr[3] = /*_.PROGN___V7*/ meltfptr[5];;
      /*epilog */

      MELT_LOCATION ("warmelt-normatch.melt:2056:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L1*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IF___V5*/ meltfptr[4] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V7*/ meltfptr[5] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V4*/ meltfptr[3] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-normatch.melt:2057:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L3*/ meltfnum[1] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.NTEST__V2*/ meltfptr[1]),
			     (melt_ptr_t) (( /*!CLASS_NORMTESTER_INSTANCE */
					    meltfrout->tabval[1])));;
      MELT_LOCATION ("warmelt-normatch.melt:2057:/ cond");
      /*cond */ if ( /*_#IS_A__L3*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V9*/ meltfptr[5] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-normatch.melt:2057:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check ntest"),
				  ("warmelt-normatch.melt")
				  ? ("warmelt-normatch.melt") : __FILE__,
				  (2057) ? (2057) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V9*/ meltfptr[5] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V8*/ meltfptr[4] = /*_.IFELSE___V9*/ meltfptr[5];;
      /*epilog */

      MELT_LOCATION ("warmelt-normatch.melt:2057:/ clear");
	     /*clear *//*_#IS_A__L3*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V9*/ meltfptr[5] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V8*/ meltfptr[4] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-normatch.melt:2058:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L4*/ meltfnum[0] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.MG__V3*/ meltfptr[2]),
			     (melt_ptr_t) (( /*!CLASS_MATCH_GRAPHIC */
					    meltfrout->tabval[2])));;
      MELT_LOCATION ("warmelt-normatch.melt:2058:/ cond");
      /*cond */ if ( /*_#IS_A__L4*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V11*/ meltfptr[10] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-normatch.melt:2058:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check mg"),
				  ("warmelt-normatch.melt")
				  ? ("warmelt-normatch.melt") : __FILE__,
				  (2058) ? (2058) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V11*/ meltfptr[10] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V10*/ meltfptr[5] = /*_.IFELSE___V11*/ meltfptr[10];;
      /*epilog */

      MELT_LOCATION ("warmelt-normatch.melt:2058:/ clear");
	     /*clear *//*_#IS_A__L4*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V11*/ meltfptr[10] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V10*/ meltfptr[5] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-normatch.melt:2059:/ quasiblock");


    /*^getslot */
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.NTEST__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 0, "NREP_LOC");
  /*_.NLOC__V13*/ meltfptr[12] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-normatch.melt:2060:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.NTEST__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 1, "NTEST_MATCHED");
  /*_.NMATCHED__V14*/ meltfptr[13] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-normatch.melt:2061:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.MG__V3*/ meltfptr[2]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 2, "MCHGX_NODOUT");
  /*_.NODOUT__V15*/ meltfptr[14] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-normatch.melt:2062:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.MG__V3*/ meltfptr[2]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 3, "MCHGX_EDGOUT");
  /*_.EDGOUT__V16*/ meltfptr[15] = slot;
    };
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-normatch.melt:2064:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L5*/ meltfnum[1] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-normatch.melt:2064:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L5*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L6*/ meltfnum[0] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-normatch.melt:2064:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[7];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L6*/ meltfnum[0];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-normatch.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 2064;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "mglabel_instance_test nloc=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.NLOC__V13*/ meltfptr[12];
	      /*^apply.arg */
	      argtab[5].meltbp_cstring = "nmatched=";
	      /*^apply.arg */
	      argtab[6].meltbp_aptr =
		(melt_ptr_t *) & /*_.NMATCHED__V14*/ meltfptr[13];
	      /*_.MELT_DEBUG_FUN__V19*/ meltfptr[18] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V18*/ meltfptr[17] =
	      /*_.MELT_DEBUG_FUN__V19*/ meltfptr[18];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-normatch.melt:2064:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L6*/ meltfnum[0] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V19*/ meltfptr[18] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V18*/ meltfptr[17] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-normatch.melt:2064:/ quasiblock");


      /*_.PROGN___V20*/ meltfptr[18] = /*_.IF___V18*/ meltfptr[17];;
      /*^compute */
      /*_.IFCPP___V17*/ meltfptr[16] = /*_.PROGN___V20*/ meltfptr[18];;
      /*epilog */

      MELT_LOCATION ("warmelt-normatch.melt:2064:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L5*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IF___V18*/ meltfptr[17] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V20*/ meltfptr[18] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V17*/ meltfptr[16] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-normatch.melt:2065:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_OUT__L7*/ meltfnum[0] =
	(melt_is_out ((melt_ptr_t) /*_.NODOUT__V15*/ meltfptr[14]));;
      MELT_LOCATION ("warmelt-normatch.melt:2065:/ cond");
      /*cond */ if ( /*_#IS_OUT__L7*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V22*/ meltfptr[18] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-normatch.melt:2065:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check nodout"),
				  ("warmelt-normatch.melt")
				  ? ("warmelt-normatch.melt") : __FILE__,
				  (2065) ? (2065) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V22*/ meltfptr[18] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V21*/ meltfptr[17] = /*_.IFELSE___V22*/ meltfptr[18];;
      /*epilog */

      MELT_LOCATION ("warmelt-normatch.melt:2065:/ clear");
	     /*clear *//*_#IS_OUT__L7*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V22*/ meltfptr[18] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V21*/ meltfptr[17] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-normatch.melt:2066:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_OUT__L8*/ meltfnum[1] =
	(melt_is_out ((melt_ptr_t) /*_.EDGOUT__V16*/ meltfptr[15]));;
      MELT_LOCATION ("warmelt-normatch.melt:2066:/ cond");
      /*cond */ if ( /*_#IS_OUT__L8*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V24*/ meltfptr[23] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-normatch.melt:2066:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check edgout"),
				  ("warmelt-normatch.melt")
				  ? ("warmelt-normatch.melt") : __FILE__,
				  (2066) ? (2066) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V24*/ meltfptr[23] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V23*/ meltfptr[18] = /*_.IFELSE___V24*/ meltfptr[23];;
      /*epilog */

      MELT_LOCATION ("warmelt-normatch.melt:2066:/ clear");
	     /*clear *//*_#IS_OUT__L8*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V24*/ meltfptr[23] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V23*/ meltfptr[18] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

    {
      MELT_LOCATION ("warmelt-normatch.melt:2068:/ locexp");
      meltgc_add_out ((melt_ptr_t) ( /*_.NODOUT__V15*/ meltfptr[14]),
		      ("<tr><td>"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-normatch.melt:2069:/ locexp");
      meltgc_add_out ((melt_ptr_t) ( /*_.NODOUT__V15*/ meltfptr[14]),
		      ("<font color=\"violet\" face=\"Times-Roman Bold\" point-size=\"10\">"));
    }
    ;
    MELT_LOCATION ("warmelt-normatch.melt:2071:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.NTEST__V2*/ meltfptr[1];
      /*_.MG_OUT_NODE_NAME__V25*/ meltfptr[23] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!MG_OUT_NODE_NAME */ meltfrout->tabval[3])),
		    (melt_ptr_t) ( /*_.NODOUT__V15*/ meltfptr[14]),
		    (MELTBPARSTR_PTR ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-normatch.melt:2072:/ locexp");
      meltgc_add_out ((melt_ptr_t) ( /*_.NODOUT__V15*/ meltfptr[14]),
		      ("</font></td> "));
    }
    ;

    {
      MELT_LOCATION ("warmelt-normatch.melt:2073:/ locexp");
      meltgc_add_out ((melt_ptr_t) ( /*_.NODOUT__V15*/ meltfptr[14]),
		      ("<td><font face=\"Courier\" point-size=\"8\">#"));
    }
    ;
 /*_#OBJ_HASH__L9*/ meltfnum[0] =
      (melt_obj_hash ((melt_ptr_t) ( /*_.NTEST__V2*/ meltfptr[1])));;

    {
      MELT_LOCATION ("warmelt-normatch.melt:2074:/ locexp");
      meltgc_add_out_hex ((melt_ptr_t) ( /*_.NODOUT__V15*/ meltfptr[14]),
			  ( /*_#OBJ_HASH__L9*/ meltfnum[0]));
    }
    ;

    {
      MELT_LOCATION ("warmelt-normatch.melt:2075:/ locexp");
      meltgc_add_out ((melt_ptr_t) ( /*_.NODOUT__V15*/ meltfptr[14]),
		      ("</font></td> "));
    }
    ;

    {
      MELT_LOCATION ("warmelt-normatch.melt:2076:/ locexp");
      meltgc_add_out ((melt_ptr_t) ( /*_.NODOUT__V15*/ meltfptr[14]),
		      ("<td><font face=\"Courier Italic\" point-size=\"8\">"));
    }
    ;
    MELT_LOCATION ("warmelt-normatch.melt:2079:/ cond");
    /*cond */ if (
		   /*ifisa */
		   melt_is_instance_of ((melt_ptr_t)
					( /*_.NTEST__V2*/ meltfptr[1]),
					(melt_ptr_t) (( /*!CLASS_NORMTESTER_INSTANCE */ meltfrout->tabval[1])))
      )				/*then */
      {
	/*^cond.then */
	/*^getslot */
	{
	  melt_ptr_t slot = NULL, obj = NULL;
	  obj = (melt_ptr_t) ( /*_.NTEST__V2*/ meltfptr[1]) /*=obj*/ ;
	  melt_object_get_field (slot, obj, 7, "NTINST_CLASS");
   /*_.NTINST_CLASS__V26*/ meltfptr[25] = slot;
	};
	;
      }
    else
      {				/*^cond.else */

  /*_.NTINST_CLASS__V26*/ meltfptr[25] = NULL;;
      }
    ;
    MELT_LOCATION ("warmelt-normatch.melt:2078:/ cond");
    /*cond */ if (
		   /*ifisa */
		   melt_is_instance_of ((melt_ptr_t)
					( /*_.NTINST_CLASS__V26*/
					 meltfptr[25]),
					(melt_ptr_t) (( /*!CLASS_NREP_SYMOCC */ meltfrout->tabval[4])))
      )				/*then */
      {
	/*^cond.then */
	/*^getslot */
	{
	  melt_ptr_t slot = NULL, obj = NULL;
	  obj =
	    (melt_ptr_t) ( /*_.NTINST_CLASS__V26*/ meltfptr[25]) /*=obj*/ ;
	  melt_object_get_field (slot, obj, 1, "NOCC_SYMB");
   /*_.NOCC_SYMB__V27*/ meltfptr[26] = slot;
	};
	;
      }
    else
      {				/*^cond.else */

  /*_.NOCC_SYMB__V27*/ meltfptr[26] = NULL;;
      }
    ;
    MELT_LOCATION ("warmelt-normatch.melt:2077:/ cond");
    /*cond */ if (
		   /*ifisa */
		   melt_is_instance_of ((melt_ptr_t)
					( /*_.NOCC_SYMB__V27*/ meltfptr[26]),
					(melt_ptr_t) (( /*!CLASS_NAMED */
						       meltfrout->tabval[5])))
      )				/*then */
      {
	/*^cond.then */
	/*^getslot */
	{
	  melt_ptr_t slot = NULL, obj = NULL;
	  obj = (melt_ptr_t) ( /*_.NOCC_SYMB__V27*/ meltfptr[26]) /*=obj*/ ;
	  melt_object_get_field (slot, obj, 1, "NAMED_NAME");
   /*_.NAMED_NAME__V28*/ meltfptr[27] = slot;
	};
	;
      }
    else
      {				/*^cond.else */

  /*_.NAMED_NAME__V28*/ meltfptr[27] = NULL;;
      }
    ;

    {
      MELT_LOCATION ("warmelt-normatch.melt:2077:/ locexp");
      meltgc_add_out ((melt_ptr_t) ( /*_.NODOUT__V15*/ meltfptr[14]),
		      melt_string_str ((melt_ptr_t)
				       ( /*_.NAMED_NAME__V28*/
					meltfptr[27])));
    }
    ;

    {
      MELT_LOCATION ("warmelt-normatch.melt:2080:/ locexp");
      meltgc_add_out ((melt_ptr_t) ( /*_.NODOUT__V15*/ meltfptr[14]),
		      ("</font></td> "));
    }
    ;

    {
      MELT_LOCATION ("warmelt-normatch.melt:2081:/ locexp");
      meltgc_add_out ((melt_ptr_t) ( /*_.NODOUT__V15*/ meltfptr[14]),
		      ("</tr>"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-normatch.melt:2082:/ locexp");
      meltgc_out_add_indent ((melt_ptr_t) ( /*_.NODOUT__V15*/ meltfptr[14]),
			     (2), 0);;
    }
    ;
    MELT_LOCATION ("warmelt-normatch.melt:2085:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#IS_MIXLOC__L10*/ meltfnum[1] =
      (melt_magic_discr ((melt_ptr_t) ( /*_.NLOC__V13*/ meltfptr[12])) ==
       MELTOBMAG_MIXLOC);;
    MELT_LOCATION ("warmelt-normatch.melt:2085:/ cond");
    /*cond */ if ( /*_#IS_MIXLOC__L10*/ meltfnum[1])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{


	  {
	    MELT_LOCATION ("warmelt-normatch.melt:2086:/ locexp");
	    meltgc_add_out ((melt_ptr_t) ( /*_.NODOUT__V15*/ meltfptr[14]),
			    ("<tr><td rowspan=\"2\">"));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-normatch.melt:2087:/ locexp");
	    meltgc_add_out ((melt_ptr_t) ( /*_.NODOUT__V15*/ meltfptr[14]),
			    ("<font face=\"Courier Italic\" point-size=\"8\">"));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-normatch.melt:2088:/ locexp");
	    /*add2outmixloc */
	      if (melt_magic_discr
		  ((melt_ptr_t) ( /*_.NLOC__V13*/ meltfptr[12])) ==
		  MELTOBMAG_MIXLOC)
	      {
		char locbuf[256];
		location_t tloc =
		  melt_location_mixloc ((melt_ptr_t) /*_.NLOC__V13*/
					meltfptr[12]);
		memset (locbuf, 0, sizeof (locbuf));
		snprintf (locbuf, sizeof (locbuf) - 1,
			  "{%.200s:%d}",
			  LOCATION_FILE (tloc), LOCATION_LINE (tloc));
		meltgc_add_out_raw ((melt_ptr_t) /*_.NODOUT__V15*/
				    meltfptr[14], locbuf);
	      }			/*end add2outmixloc */
	    ;
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-normatch.melt:2089:/ locexp");
	    meltgc_add_out ((melt_ptr_t) ( /*_.NODOUT__V15*/ meltfptr[14]),
			    ("</font></td> </tr>"));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-normatch.melt:2090:/ locexp");
	    meltgc_out_add_indent ((melt_ptr_t)
				   ( /*_.NODOUT__V15*/ meltfptr[14]), (2),
				   0);;
	  }
	  ;
	  MELT_LOCATION ("warmelt-normatch.melt:2085:/ quasiblock");


	  /*epilog */
	}
	;
      }
    else
      {				/*^cond.else */

  /*_.IFELSE___V29*/ meltfptr[28] = NULL;;
      }
    ;
    MELT_LOCATION ("warmelt-normatch.melt:2095:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#IS_A__L11*/ meltfnum[10] =
      melt_is_instance_of ((melt_ptr_t) ( /*_.NMATCHED__V14*/ meltfptr[13]),
			   (melt_ptr_t) (( /*!CLASS_NREP_LOCSYMOCC */
					  meltfrout->tabval[6])));;
    MELT_LOCATION ("warmelt-normatch.melt:2095:/ cond");
    /*cond */ if ( /*_#IS_A__L11*/ meltfnum[10])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-normatch.melt:2096:/ quasiblock");


	  /*^getslot */
	  {
	    melt_ptr_t slot = NULL, obj = NULL;
	    obj = (melt_ptr_t) ( /*_.NMATCHED__V14*/ meltfptr[13]) /*=obj*/ ;
	    melt_object_get_field (slot, obj, 0, "NREP_LOC");
    /*_.NMALOC__V30*/ meltfptr[29] = slot;
	  };
	  ;
	  MELT_LOCATION ("warmelt-normatch.melt:2097:/ getslot");
	  {
	    melt_ptr_t slot = NULL, obj = NULL;
	    obj = (melt_ptr_t) ( /*_.NMATCHED__V14*/ meltfptr[13]) /*=obj*/ ;
	    melt_object_get_field (slot, obj, 1, "NOCC_SYMB");
    /*_.NMASYMB__V31*/ meltfptr[30] = slot;
	  };
	  ;
	  MELT_LOCATION ("warmelt-normatch.melt:2099:/ cond");
	  /*cond */ if (
			 /*ifisa */
			 melt_is_instance_of ((melt_ptr_t)
					      ( /*_.NMASYMB__V31*/
					       meltfptr[30]),
					      (melt_ptr_t) (( /*!CLASS_CLONED_SYMBOL */ meltfrout->tabval[7])))
	    )			/*then */
	    {
	      /*^cond.then */
	      /*^getslot */
	      {
		melt_ptr_t slot = NULL, obj = NULL;
		obj =
		  (melt_ptr_t) ( /*_.NMASYMB__V31*/ meltfptr[30]) /*=obj*/ ;
		melt_object_get_field (slot, obj, 3, "CSYM_URANK");
     /*_.CSYM_URANK__V32*/ meltfptr[31] = slot;
	      };
	      ;
	    }
	  else
	    {			/*^cond.else */

    /*_.CSYM_URANK__V32*/ meltfptr[31] = NULL;;
	    }
	  ;
	  /*^compute */
   /*_#NMARANK__L12*/ meltfnum[11] =
	    (melt_get_int
	     ((melt_ptr_t) ( /*_.CSYM_URANK__V32*/ meltfptr[31])));;

	  {
	    MELT_LOCATION ("warmelt-normatch.melt:2101:/ locexp");
	    meltgc_add_out ((melt_ptr_t) ( /*_.NODOUT__V15*/ meltfptr[14]),
			    ("<tr><td>"));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-normatch.melt:2102:/ locexp");
	    meltgc_add_out ((melt_ptr_t) ( /*_.NODOUT__V15*/ meltfptr[14]),
			    ("<font face=\"Courier Bold\" point-size=\"8\">"));
	  }
	  ;
	  MELT_LOCATION ("warmelt-normatch.melt:2103:/ cond");
	  /*cond */ if (
			 /*ifisa */
			 melt_is_instance_of ((melt_ptr_t)
					      ( /*_.NMASYMB__V31*/
					       meltfptr[30]),
					      (melt_ptr_t) (( /*!CLASS_NAMED */ meltfrout->tabval[5])))
	    )			/*then */
	    {
	      /*^cond.then */
	      /*^getslot */
	      {
		melt_ptr_t slot = NULL, obj = NULL;
		obj =
		  (melt_ptr_t) ( /*_.NMASYMB__V31*/ meltfptr[30]) /*=obj*/ ;
		melt_object_get_field (slot, obj, 1, "NAMED_NAME");
     /*_.NAMED_NAME__V33*/ meltfptr[32] = slot;
	      };
	      ;
	    }
	  else
	    {			/*^cond.else */

    /*_.NAMED_NAME__V33*/ meltfptr[32] = NULL;;
	    }
	  ;

	  {
	    MELT_LOCATION ("warmelt-normatch.melt:2103:/ locexp");
	    meltgc_add_out ((melt_ptr_t) ( /*_.NODOUT__V15*/ meltfptr[14]),
			    melt_string_str ((melt_ptr_t)
					     ( /*_.NAMED_NAME__V33*/
					      meltfptr[32])));
	  }
	  ;
	  MELT_LOCATION ("warmelt-normatch.melt:2104:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
   /*_#I__L13*/ meltfnum[12] =
	    (( /*_#NMARANK__L12*/ meltfnum[11]) > (0));;
	  MELT_LOCATION ("warmelt-normatch.melt:2104:/ cond");
	  /*cond */ if ( /*_#I__L13*/ meltfnum[12])	/*then */
	    {
	      /*^cond.then */
	      /*^block */
	      /*anyblock */
	      {


		{
		  MELT_LOCATION ("warmelt-normatch.melt:2106:/ locexp");
		  meltgc_add_out ((melt_ptr_t)
				  ( /*_.NODOUT__V15*/ meltfptr[14]), (" #"));
		}
		;

		{
		  MELT_LOCATION ("warmelt-normatch.melt:2107:/ locexp");
		  meltgc_add_out_hex ((melt_ptr_t)
				      ( /*_.NODOUT__V15*/ meltfptr[14]),
				      ( /*_#NMARANK__L12*/ meltfnum[11]));
		}
		;

		{
		  MELT_LOCATION ("warmelt-normatch.melt:2108:/ locexp");
		  meltgc_add_out ((melt_ptr_t)
				  ( /*_.NODOUT__V15*/ meltfptr[14]), (" "));
		}
		;
		MELT_LOCATION ("warmelt-normatch.melt:2105:/ quasiblock");


		/*epilog */
	      }
	      ;
	    }			/*noelse */
	  ;

	  {
	    MELT_LOCATION ("warmelt-normatch.melt:2110:/ locexp");
	    meltgc_add_out ((melt_ptr_t) ( /*_.NODOUT__V15*/ meltfptr[14]),
			    ("</font></td>  <td><font point-size=\"8\">##"));
	  }
	  ;
   /*_#OBJ_HASH__L14*/ meltfnum[13] =
	    (melt_obj_hash
	     ((melt_ptr_t) ( /*_.NMATCHED__V14*/ meltfptr[13])));;

	  {
	    MELT_LOCATION ("warmelt-normatch.melt:2111:/ locexp");
	    meltgc_add_out_hex ((melt_ptr_t)
				( /*_.NODOUT__V15*/ meltfptr[14]),
				( /*_#OBJ_HASH__L14*/ meltfnum[13]));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-normatch.melt:2112:/ locexp");
	    meltgc_add_out ((melt_ptr_t) ( /*_.NODOUT__V15*/ meltfptr[14]),
			    ("</font></td> </tr>"));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-normatch.melt:2113:/ locexp");
	    meltgc_out_add_indent ((melt_ptr_t)
				   ( /*_.NODOUT__V15*/ meltfptr[14]), (2),
				   0);;
	  }
	  ;

	  MELT_LOCATION ("warmelt-normatch.melt:2096:/ clear");
	     /*clear *//*_.NMALOC__V30*/ meltfptr[29] = 0;
	  /*^clear */
	     /*clear *//*_.NMASYMB__V31*/ meltfptr[30] = 0;
	  /*^clear */
	     /*clear *//*_.CSYM_URANK__V32*/ meltfptr[31] = 0;
	  /*^clear */
	     /*clear *//*_#NMARANK__L12*/ meltfnum[11] = 0;
	  /*^clear */
	     /*clear *//*_.NAMED_NAME__V33*/ meltfptr[32] = 0;
	  /*^clear */
	     /*clear *//*_#I__L13*/ meltfnum[12] = 0;
	  /*^clear */
	     /*clear *//*_#OBJ_HASH__L14*/ meltfnum[13] = 0;
	  /*epilog */
	}
	;
      }				/*noelse */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-normatch.melt:2115:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L15*/ meltfnum[11] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-normatch.melt:2115:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L15*/ meltfnum[11])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L16*/ meltfnum[12] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-normatch.melt:2115:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long =
		/*_#THE_MELTCALLCOUNT__L16*/ meltfnum[12];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-normatch.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 2115;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "mglabel_instance_test end ntest";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.NTEST__V2*/ meltfptr[1];
	      /*_.MELT_DEBUG_FUN__V36*/ meltfptr[31] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V35*/ meltfptr[30] =
	      /*_.MELT_DEBUG_FUN__V36*/ meltfptr[31];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-normatch.melt:2115:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L16*/ meltfnum[12] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V36*/ meltfptr[31] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V35*/ meltfptr[30] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-normatch.melt:2115:/ quasiblock");


      /*_.PROGN___V37*/ meltfptr[32] = /*_.IF___V35*/ meltfptr[30];;
      /*^compute */
      /*_.IFCPP___V34*/ meltfptr[29] = /*_.PROGN___V37*/ meltfptr[32];;
      /*epilog */

      MELT_LOCATION ("warmelt-normatch.melt:2115:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L15*/ meltfnum[11] = 0;
      /*^clear */
	     /*clear *//*_.IF___V35*/ meltfptr[30] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V37*/ meltfptr[32] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V34*/ meltfptr[29] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    /*^compute */
    /*_.LET___V12*/ meltfptr[10] = /*_.IFCPP___V34*/ meltfptr[29];;

    MELT_LOCATION ("warmelt-normatch.melt:2059:/ clear");
	   /*clear *//*_.NLOC__V13*/ meltfptr[12] = 0;
    /*^clear */
	   /*clear *//*_.NMATCHED__V14*/ meltfptr[13] = 0;
    /*^clear */
	   /*clear *//*_.NODOUT__V15*/ meltfptr[14] = 0;
    /*^clear */
	   /*clear *//*_.EDGOUT__V16*/ meltfptr[15] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V17*/ meltfptr[16] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V21*/ meltfptr[17] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V23*/ meltfptr[18] = 0;
    /*^clear */
	   /*clear *//*_.MG_OUT_NODE_NAME__V25*/ meltfptr[23] = 0;
    /*^clear */
	   /*clear *//*_#OBJ_HASH__L9*/ meltfnum[0] = 0;
    /*^clear */
	   /*clear *//*_.NTINST_CLASS__V26*/ meltfptr[25] = 0;
    /*^clear */
	   /*clear *//*_.NOCC_SYMB__V27*/ meltfptr[26] = 0;
    /*^clear */
	   /*clear *//*_.NAMED_NAME__V28*/ meltfptr[27] = 0;
    /*^clear */
	   /*clear *//*_#IS_MIXLOC__L10*/ meltfnum[1] = 0;
    /*^clear */
	   /*clear *//*_.IFELSE___V29*/ meltfptr[28] = 0;
    /*^clear */
	   /*clear *//*_#IS_A__L11*/ meltfnum[10] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V34*/ meltfptr[29] = 0;
    MELT_LOCATION ("warmelt-normatch.melt:2055:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.LET___V12*/ meltfptr[10];;

    {
      MELT_LOCATION ("warmelt-normatch.melt:2055:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V4*/ meltfptr[3] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V8*/ meltfptr[4] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V10*/ meltfptr[5] = 0;
    /*^clear */
	   /*clear *//*_.LET___V12*/ meltfptr[10] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("MGLABEL_INSTANCE_TEST", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_47_warmelt_normatch_MGLABEL_INSTANCE_TEST_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_47_warmelt_normatch_MGLABEL_INSTANCE_TEST */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_48_warmelt_normatch_MGLABEL_SUCCESS_TEST (meltclosure_ptr_t
						   meltclosp_,
						   melt_ptr_t meltfirstargp_,
						   const melt_argdescr_cell_t
						   meltxargdescr_[],
						   union meltparam_un *
						   meltxargtab_,
						   const melt_argdescr_cell_t
						   meltxresdescr_[],
						   union meltparam_un *
						   meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_48_warmelt_normatch_MGLABEL_SUCCESS_TEST_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_48_warmelt_normatch_MGLABEL_SUCCESS_TEST_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 30
    melt_ptr_t mcfr_varptr[30];
#define MELTFRAM_NBVARNUM 12
    long mcfr_varnum[12];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_48_warmelt_normatch_MGLABEL_SUCCESS_TEST is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_48_warmelt_normatch_MGLABEL_SUCCESS_TEST_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 30; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_48_warmelt_normatch_MGLABEL_SUCCESS_TEST nbval 30*/
  meltfram__.mcfr_nbvar = 30 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("MGLABEL_SUCCESS_TEST", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-normatch.melt:2120:/ getarg");
 /*_.NTEST__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.MG__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.MG__V3*/ meltfptr[2])) != NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-normatch.melt:2121:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L1*/ meltfnum[0] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-normatch.melt:2121:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L1*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-normatch.melt:2121:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-normatch.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 2121;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "mglabel_success_test start ntest";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.NTEST__V2*/ meltfptr[1];
	      /*_.MELT_DEBUG_FUN__V6*/ meltfptr[5] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V5*/ meltfptr[4] = /*_.MELT_DEBUG_FUN__V6*/ meltfptr[5];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-normatch.melt:2121:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V6*/ meltfptr[5] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V5*/ meltfptr[4] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-normatch.melt:2121:/ quasiblock");


      /*_.PROGN___V7*/ meltfptr[5] = /*_.IF___V5*/ meltfptr[4];;
      /*^compute */
      /*_.IFCPP___V4*/ meltfptr[3] = /*_.PROGN___V7*/ meltfptr[5];;
      /*epilog */

      MELT_LOCATION ("warmelt-normatch.melt:2121:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L1*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IF___V5*/ meltfptr[4] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V7*/ meltfptr[5] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V4*/ meltfptr[3] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-normatch.melt:2122:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L3*/ meltfnum[1] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.NTEST__V2*/ meltfptr[1]),
			     (melt_ptr_t) (( /*!CLASS_NORMTESTER_SUCCESS */
					    meltfrout->tabval[1])));;
      MELT_LOCATION ("warmelt-normatch.melt:2122:/ cond");
      /*cond */ if ( /*_#IS_A__L3*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V9*/ meltfptr[5] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-normatch.melt:2122:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check ntest"),
				  ("warmelt-normatch.melt")
				  ? ("warmelt-normatch.melt") : __FILE__,
				  (2122) ? (2122) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V9*/ meltfptr[5] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V8*/ meltfptr[4] = /*_.IFELSE___V9*/ meltfptr[5];;
      /*epilog */

      MELT_LOCATION ("warmelt-normatch.melt:2122:/ clear");
	     /*clear *//*_#IS_A__L3*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V9*/ meltfptr[5] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V8*/ meltfptr[4] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-normatch.melt:2123:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L4*/ meltfnum[0] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.MG__V3*/ meltfptr[2]),
			     (melt_ptr_t) (( /*!CLASS_MATCH_GRAPHIC */
					    meltfrout->tabval[2])));;
      MELT_LOCATION ("warmelt-normatch.melt:2123:/ cond");
      /*cond */ if ( /*_#IS_A__L4*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V11*/ meltfptr[10] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-normatch.melt:2123:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check mg"),
				  ("warmelt-normatch.melt")
				  ? ("warmelt-normatch.melt") : __FILE__,
				  (2123) ? (2123) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V11*/ meltfptr[10] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V10*/ meltfptr[5] = /*_.IFELSE___V11*/ meltfptr[10];;
      /*epilog */

      MELT_LOCATION ("warmelt-normatch.melt:2123:/ clear");
	     /*clear *//*_#IS_A__L4*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V11*/ meltfptr[10] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V10*/ meltfptr[5] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-normatch.melt:2124:/ quasiblock");


    /*^getslot */
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.NTEST__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 0, "NREP_LOC");
  /*_.NLOC__V13*/ meltfptr[12] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-normatch.melt:2125:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.NTEST__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 1, "NTEST_MATCHED");
  /*_.NMATCHED__V14*/ meltfptr[13] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-normatch.melt:2126:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.MG__V3*/ meltfptr[2]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 2, "MCHGX_NODOUT");
  /*_.NODOUT__V15*/ meltfptr[14] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-normatch.melt:2127:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.MG__V3*/ meltfptr[2]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 3, "MCHGX_EDGOUT");
  /*_.EDGOUT__V16*/ meltfptr[15] = slot;
    };
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-normatch.melt:2129:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L5*/ meltfnum[1] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-normatch.melt:2129:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L5*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L6*/ meltfnum[0] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-normatch.melt:2129:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[7];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L6*/ meltfnum[0];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-normatch.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 2129;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "mglabel_success_test nloc=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.NLOC__V13*/ meltfptr[12];
	      /*^apply.arg */
	      argtab[5].meltbp_cstring = " nmatched=";
	      /*^apply.arg */
	      argtab[6].meltbp_aptr =
		(melt_ptr_t *) & /*_.NMATCHED__V14*/ meltfptr[13];
	      /*_.MELT_DEBUG_FUN__V19*/ meltfptr[18] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V18*/ meltfptr[17] =
	      /*_.MELT_DEBUG_FUN__V19*/ meltfptr[18];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-normatch.melt:2129:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L6*/ meltfnum[0] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V19*/ meltfptr[18] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V18*/ meltfptr[17] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-normatch.melt:2129:/ quasiblock");


      /*_.PROGN___V20*/ meltfptr[18] = /*_.IF___V18*/ meltfptr[17];;
      /*^compute */
      /*_.IFCPP___V17*/ meltfptr[16] = /*_.PROGN___V20*/ meltfptr[18];;
      /*epilog */

      MELT_LOCATION ("warmelt-normatch.melt:2129:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L5*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IF___V18*/ meltfptr[17] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V20*/ meltfptr[18] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V17*/ meltfptr[16] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-normatch.melt:2130:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_OUT__L7*/ meltfnum[0] =
	(melt_is_out ((melt_ptr_t) /*_.NODOUT__V15*/ meltfptr[14]));;
      MELT_LOCATION ("warmelt-normatch.melt:2130:/ cond");
      /*cond */ if ( /*_#IS_OUT__L7*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V22*/ meltfptr[18] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-normatch.melt:2130:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check nodout"),
				  ("warmelt-normatch.melt")
				  ? ("warmelt-normatch.melt") : __FILE__,
				  (2130) ? (2130) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V22*/ meltfptr[18] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V21*/ meltfptr[17] = /*_.IFELSE___V22*/ meltfptr[18];;
      /*epilog */

      MELT_LOCATION ("warmelt-normatch.melt:2130:/ clear");
	     /*clear *//*_#IS_OUT__L7*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V22*/ meltfptr[18] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V21*/ meltfptr[17] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-normatch.melt:2131:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_OUT__L8*/ meltfnum[1] =
	(melt_is_out ((melt_ptr_t) /*_.EDGOUT__V16*/ meltfptr[15]));;
      MELT_LOCATION ("warmelt-normatch.melt:2131:/ cond");
      /*cond */ if ( /*_#IS_OUT__L8*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V24*/ meltfptr[23] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-normatch.melt:2131:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check edgout"),
				  ("warmelt-normatch.melt")
				  ? ("warmelt-normatch.melt") : __FILE__,
				  (2131) ? (2131) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V24*/ meltfptr[23] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V23*/ meltfptr[18] = /*_.IFELSE___V24*/ meltfptr[23];;
      /*epilog */

      MELT_LOCATION ("warmelt-normatch.melt:2131:/ clear");
	     /*clear *//*_#IS_OUT__L8*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V24*/ meltfptr[23] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V23*/ meltfptr[18] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

    {
      MELT_LOCATION ("warmelt-normatch.melt:2133:/ locexp");
      meltgc_add_out ((melt_ptr_t) ( /*_.NODOUT__V15*/ meltfptr[14]),
		      ("<tr><td>"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-normatch.melt:2134:/ locexp");
      meltgc_add_out ((melt_ptr_t) ( /*_.NODOUT__V15*/ meltfptr[14]),
		      ("<font color=\"sienna\" face=\"Times-Roman Bold\" point-size=\"10\">"));
    }
    ;
    MELT_LOCATION ("warmelt-normatch.melt:2136:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.NTEST__V2*/ meltfptr[1];
      /*_.MG_OUT_NODE_NAME__V25*/ meltfptr[23] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!MG_OUT_NODE_NAME */ meltfrout->tabval[3])),
		    (melt_ptr_t) ( /*_.NODOUT__V15*/ meltfptr[14]),
		    (MELTBPARSTR_PTR ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-normatch.melt:2137:/ locexp");
      meltgc_add_out ((melt_ptr_t) ( /*_.NODOUT__V15*/ meltfptr[14]),
		      ("</font></td> "));
    }
    ;

    {
      MELT_LOCATION ("warmelt-normatch.melt:2138:/ locexp");
      meltgc_add_out ((melt_ptr_t) ( /*_.NODOUT__V15*/ meltfptr[14]),
		      ("<td><font face=\"Courier\" point-size=\"8\">#"));
    }
    ;
 /*_#OBJ_HASH__L9*/ meltfnum[0] =
      (melt_obj_hash ((melt_ptr_t) ( /*_.NTEST__V2*/ meltfptr[1])));;

    {
      MELT_LOCATION ("warmelt-normatch.melt:2139:/ locexp");
      meltgc_add_out_hex ((melt_ptr_t) ( /*_.NODOUT__V15*/ meltfptr[14]),
			  ( /*_#OBJ_HASH__L9*/ meltfnum[0]));
    }
    ;

    {
      MELT_LOCATION ("warmelt-normatch.melt:2140:/ locexp");
      meltgc_add_out ((melt_ptr_t) ( /*_.NODOUT__V15*/ meltfptr[14]),
		      ("</font></td> "));
    }
    ;

    {
      MELT_LOCATION ("warmelt-normatch.melt:2141:/ locexp");
      meltgc_add_out ((melt_ptr_t) ( /*_.NODOUT__V15*/ meltfptr[14]),
		      ("</tr>"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-normatch.melt:2142:/ locexp");
      meltgc_out_add_indent ((melt_ptr_t) ( /*_.NODOUT__V15*/ meltfptr[14]),
			     (2), 0);;
    }
    ;
    MELT_LOCATION ("warmelt-normatch.melt:2145:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#IS_MIXLOC__L10*/ meltfnum[1] =
      (melt_magic_discr ((melt_ptr_t) ( /*_.NLOC__V13*/ meltfptr[12])) ==
       MELTOBMAG_MIXLOC);;
    MELT_LOCATION ("warmelt-normatch.melt:2145:/ cond");
    /*cond */ if ( /*_#IS_MIXLOC__L10*/ meltfnum[1])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{


	  {
	    MELT_LOCATION ("warmelt-normatch.melt:2146:/ locexp");
	    meltgc_add_out ((melt_ptr_t) ( /*_.NODOUT__V15*/ meltfptr[14]),
			    ("<tr><td rowspan=\"2\">"));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-normatch.melt:2147:/ locexp");
	    meltgc_add_out ((melt_ptr_t) ( /*_.NODOUT__V15*/ meltfptr[14]),
			    ("<font face=\"Courier Italic\" point-size=\"8\">"));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-normatch.melt:2148:/ locexp");
	    /*add2outmixloc */
	      if (melt_magic_discr
		  ((melt_ptr_t) ( /*_.NLOC__V13*/ meltfptr[12])) ==
		  MELTOBMAG_MIXLOC)
	      {
		char locbuf[256];
		location_t tloc =
		  melt_location_mixloc ((melt_ptr_t) /*_.NLOC__V13*/
					meltfptr[12]);
		memset (locbuf, 0, sizeof (locbuf));
		snprintf (locbuf, sizeof (locbuf) - 1,
			  "{%.200s:%d}",
			  LOCATION_FILE (tloc), LOCATION_LINE (tloc));
		meltgc_add_out_raw ((melt_ptr_t) /*_.NODOUT__V15*/
				    meltfptr[14], locbuf);
	      }			/*end add2outmixloc */
	    ;
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-normatch.melt:2149:/ locexp");
	    meltgc_add_out ((melt_ptr_t) ( /*_.NODOUT__V15*/ meltfptr[14]),
			    ("</font></td> </tr>"));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-normatch.melt:2150:/ locexp");
	    meltgc_out_add_indent ((melt_ptr_t)
				   ( /*_.NODOUT__V15*/ meltfptr[14]), (2),
				   0);;
	  }
	  ;
	  MELT_LOCATION ("warmelt-normatch.melt:2145:/ quasiblock");


	  /*epilog */
	}
	;
      }
    else
      {				/*^cond.else */

  /*_.IFELSE___V26*/ meltfptr[25] = NULL;;
      }
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-normatch.melt:2153:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L11*/ meltfnum[10] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-normatch.melt:2153:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L11*/ meltfnum[10])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L12*/ meltfnum[11] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-normatch.melt:2153:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long =
		/*_#THE_MELTCALLCOUNT__L12*/ meltfnum[11];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-normatch.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 2153;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "mglabel_success_test end ntest";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.NTEST__V2*/ meltfptr[1];
	      /*_.MELT_DEBUG_FUN__V29*/ meltfptr[28] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V28*/ meltfptr[27] =
	      /*_.MELT_DEBUG_FUN__V29*/ meltfptr[28];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-normatch.melt:2153:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L12*/ meltfnum[11] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V29*/ meltfptr[28] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V28*/ meltfptr[27] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-normatch.melt:2153:/ quasiblock");


      /*_.PROGN___V30*/ meltfptr[28] = /*_.IF___V28*/ meltfptr[27];;
      /*^compute */
      /*_.IFCPP___V27*/ meltfptr[26] = /*_.PROGN___V30*/ meltfptr[28];;
      /*epilog */

      MELT_LOCATION ("warmelt-normatch.melt:2153:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L11*/ meltfnum[10] = 0;
      /*^clear */
	     /*clear *//*_.IF___V28*/ meltfptr[27] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V30*/ meltfptr[28] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V27*/ meltfptr[26] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    /*^compute */
    /*_.LET___V12*/ meltfptr[10] = /*_.IFCPP___V27*/ meltfptr[26];;

    MELT_LOCATION ("warmelt-normatch.melt:2124:/ clear");
	   /*clear *//*_.NLOC__V13*/ meltfptr[12] = 0;
    /*^clear */
	   /*clear *//*_.NMATCHED__V14*/ meltfptr[13] = 0;
    /*^clear */
	   /*clear *//*_.NODOUT__V15*/ meltfptr[14] = 0;
    /*^clear */
	   /*clear *//*_.EDGOUT__V16*/ meltfptr[15] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V17*/ meltfptr[16] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V21*/ meltfptr[17] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V23*/ meltfptr[18] = 0;
    /*^clear */
	   /*clear *//*_.MG_OUT_NODE_NAME__V25*/ meltfptr[23] = 0;
    /*^clear */
	   /*clear *//*_#OBJ_HASH__L9*/ meltfnum[0] = 0;
    /*^clear */
	   /*clear *//*_#IS_MIXLOC__L10*/ meltfnum[1] = 0;
    /*^clear */
	   /*clear *//*_.IFELSE___V26*/ meltfptr[25] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V27*/ meltfptr[26] = 0;
    MELT_LOCATION ("warmelt-normatch.melt:2120:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.LET___V12*/ meltfptr[10];;

    {
      MELT_LOCATION ("warmelt-normatch.melt:2120:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V4*/ meltfptr[3] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V8*/ meltfptr[4] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V10*/ meltfptr[5] = 0;
    /*^clear */
	   /*clear *//*_.LET___V12*/ meltfptr[10] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("MGLABEL_SUCCESS_TEST", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_48_warmelt_normatch_MGLABEL_SUCCESS_TEST_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_48_warmelt_normatch_MGLABEL_SUCCESS_TEST */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_49_warmelt_normatch_MATCHGRAPHIC_TESTS (meltclosure_ptr_t meltclosp_,
						 melt_ptr_t meltfirstargp_,
						 const melt_argdescr_cell_t
						 meltxargdescr_[],
						 union meltparam_un *
						 meltxargtab_,
						 const melt_argdescr_cell_t
						 meltxresdescr_[],
						 union meltparam_un *
						 meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_49_warmelt_normatch_MATCHGRAPHIC_TESTS_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_49_warmelt_normatch_MATCHGRAPHIC_TESTS_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 28
    melt_ptr_t mcfr_varptr[28];
#define MELTFRAM_NBVARNUM 11
    long mcfr_varnum[11];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_49_warmelt_normatch_MATCHGRAPHIC_TESTS is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_49_warmelt_normatch_MATCHGRAPHIC_TESTS_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 28; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_49_warmelt_normatch_MATCHGRAPHIC_TESTS nbval 28*/
  meltfram__.mcfr_nbvar = 28 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("MATCHGRAPHIC_TESTS", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-normatch.melt:2160:/ getarg");
 /*_.TESTUP__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.MG__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.MG__V3*/ meltfptr[2])) != NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-normatch.melt:2161:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L1*/ meltfnum[0] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-normatch.melt:2161:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L1*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-normatch.melt:2161:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[7];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-normatch.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 2161;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "matchgraphic_tests  start testup=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.TESTUP__V2*/ meltfptr[1];
	      /*^apply.arg */
	      argtab[5].meltbp_cstring = " mg=";
	      /*^apply.arg */
	      argtab[6].meltbp_aptr =
		(melt_ptr_t *) & /*_.MG__V3*/ meltfptr[2];
	      /*_.MELT_DEBUG_FUN__V6*/ meltfptr[5] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V5*/ meltfptr[4] = /*_.MELT_DEBUG_FUN__V6*/ meltfptr[5];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-normatch.melt:2161:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V6*/ meltfptr[5] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V5*/ meltfptr[4] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-normatch.melt:2161:/ quasiblock");


      /*_.PROGN___V7*/ meltfptr[5] = /*_.IF___V5*/ meltfptr[4];;
      /*^compute */
      /*_.IFCPP___V4*/ meltfptr[3] = /*_.PROGN___V7*/ meltfptr[5];;
      /*epilog */

      MELT_LOCATION ("warmelt-normatch.melt:2161:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L1*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IF___V5*/ meltfptr[4] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V7*/ meltfptr[5] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V4*/ meltfptr[3] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-normatch.melt:2162:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_MULTIPLE__L3*/ meltfnum[1] =
	(melt_magic_discr ((melt_ptr_t) ( /*_.TESTUP__V2*/ meltfptr[1])) ==
	 MELTOBMAG_MULTIPLE);;
      MELT_LOCATION ("warmelt-normatch.melt:2162:/ cond");
      /*cond */ if ( /*_#IS_MULTIPLE__L3*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V9*/ meltfptr[5] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-normatch.melt:2162:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check testup"),
				  ("warmelt-normatch.melt")
				  ? ("warmelt-normatch.melt") : __FILE__,
				  (2162) ? (2162) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V9*/ meltfptr[5] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V8*/ meltfptr[4] = /*_.IFELSE___V9*/ meltfptr[5];;
      /*epilog */

      MELT_LOCATION ("warmelt-normatch.melt:2162:/ clear");
	     /*clear *//*_#IS_MULTIPLE__L3*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V9*/ meltfptr[5] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V8*/ meltfptr[4] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-normatch.melt:2163:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L4*/ meltfnum[0] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.MG__V3*/ meltfptr[2]),
			     (melt_ptr_t) (( /*!CLASS_MATCH_GRAPHIC */
					    meltfrout->tabval[1])));;
      MELT_LOCATION ("warmelt-normatch.melt:2163:/ cond");
      /*cond */ if ( /*_#IS_A__L4*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V11*/ meltfptr[10] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-normatch.melt:2163:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check mg"),
				  ("warmelt-normatch.melt")
				  ? ("warmelt-normatch.melt") : __FILE__,
				  (2163) ? (2163) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V11*/ meltfptr[10] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V10*/ meltfptr[5] = /*_.IFELSE___V11*/ meltfptr[10];;
      /*epilog */

      MELT_LOCATION ("warmelt-normatch.melt:2163:/ clear");
	     /*clear *//*_#IS_A__L4*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V11*/ meltfptr[10] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V10*/ meltfptr[5] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-normatch.melt:2164:/ quasiblock");


    /*^getslot */
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.MG__V3*/ meltfptr[2]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 2, "MCHGX_NODOUT");
  /*_.NODOUT__V12*/ meltfptr[10] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-normatch.melt:2165:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.MG__V3*/ meltfptr[2]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 3, "MCHGX_EDGOUT");
  /*_.EDGOUT__V13*/ meltfptr[12] = slot;
    };
    ;

    {
      MELT_LOCATION ("warmelt-normatch.melt:2167:/ locexp");
      meltgc_out_add_indent ((melt_ptr_t) ( /*_.NODOUT__V12*/ meltfptr[10]),
			     (0), 0);;
    }
    ;

    {
      MELT_LOCATION ("warmelt-normatch.melt:2168:/ locexp");
      meltgc_out_add_indent ((melt_ptr_t) ( /*_.EDGOUT__V13*/ meltfptr[12]),
			     (0), 0);;
    }
    ;
    /*citerblock FOREACH_IN_MULTIPLE */
    {
      /* start foreach_in_multiple meltcit1__EACHTUP */
      long meltcit1__EACHTUP_ln =
	melt_multiple_length ((melt_ptr_t) /*_.TESTUP__V2*/ meltfptr[1]);
      for ( /*_#TIX__L5*/ meltfnum[1] = 0;
	   ( /*_#TIX__L5*/ meltfnum[1] >= 0)
	   && ( /*_#TIX__L5*/ meltfnum[1] < meltcit1__EACHTUP_ln);
	/*_#TIX__L5*/ meltfnum[1]++)
	{
	  /*_.CURTEST__V14*/ meltfptr[13] =
	    melt_multiple_nth ((melt_ptr_t) ( /*_.TESTUP__V2*/ meltfptr[1]),
			       /*_#TIX__L5*/ meltfnum[1]);




#if MELT_HAVE_DEBUG
	  MELT_LOCATION ("warmelt-normatch.melt:2172:/ cppif.then");
	  /*^block */
	  /*anyblock */
	  {

	    /*^checksignal */
	    MELT_CHECK_SIGNAL ();
	    ;
    /*_#IS_A__L6*/ meltfnum[0] =
	      melt_is_instance_of ((melt_ptr_t)
				   ( /*_.CURTEST__V14*/ meltfptr[13]),
				   (melt_ptr_t) (( /*!CLASS_NORMTESTER_ANY */
						  meltfrout->tabval[2])));;
	    MELT_LOCATION ("warmelt-normatch.melt:2172:/ cond");
	    /*cond */ if ( /*_#IS_A__L6*/ meltfnum[0])	/*then */
	      {
		/*^cond.then */
		/*_.IFELSE___V16*/ meltfptr[15] = ( /*nil */ NULL);;
	      }
	    else
	      {
		MELT_LOCATION ("warmelt-normatch.melt:2172:/ cond.else");

		/*^block */
		/*anyblock */
		{




		  {
		    /*^locexp */
		    melt_assert_failed (("check curtest"),
					("warmelt-normatch.melt")
					? ("warmelt-normatch.melt") :
					__FILE__, (2172) ? (2172) : __LINE__,
					__FUNCTION__);
		    ;
		  }
		  ;
		/*clear *//*_.IFELSE___V16*/ meltfptr[15] = 0;
		  /*epilog */
		}
		;
	      }
	    ;
	    /*^compute */
	    /*_.IFCPP___V15*/ meltfptr[14] = /*_.IFELSE___V16*/ meltfptr[15];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-normatch.melt:2172:/ clear");
	      /*clear *//*_#IS_A__L6*/ meltfnum[0] = 0;
	    /*^clear */
	      /*clear *//*_.IFELSE___V16*/ meltfptr[15] = 0;
	  }

#else /*MELT_HAVE_DEBUG */
	  /*^cppif.else */
	  /*_.IFCPP___V15*/ meltfptr[14] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
	  ;

#if MELT_HAVE_DEBUG
	  MELT_LOCATION ("warmelt-normatch.melt:2173:/ cppif.then");
	  /*^block */
	  /*anyblock */
	  {


	    {
	      /*^locexp */
	      /*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	      melt_dbgcounter++;
#endif
	      ;
	    }
	    ;
	    /*^checksignal */
	    MELT_CHECK_SIGNAL ();
	    ;
    /*_#MELT_NEED_DBG__L7*/ meltfnum[0] =
	      /*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	      ( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	      0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	      ;;
	    MELT_LOCATION ("warmelt-normatch.melt:2173:/ cond");
	    /*cond */ if ( /*_#MELT_NEED_DBG__L7*/ meltfnum[0])	/*then */
	      {
		/*^cond.then */
		/*^block */
		/*anyblock */
		{

      /*_#THE_MELTCALLCOUNT__L8*/ meltfnum[7] =
#ifdef meltcallcount
		    meltcallcount	/* the_meltcallcount */
#else
		    0L
#endif /* meltcallcount the_meltcallcount */
		    ;;
		  MELT_LOCATION ("warmelt-normatch.melt:2173:/ checksignal");
		  MELT_CHECK_SIGNAL ();
		  ;
		  /*^apply */
		  /*apply */
		  {
		    union meltparam_un argtab[5];
		    memset (&argtab, 0, sizeof (argtab));
		    /*^apply.arg */
		    argtab[0].meltbp_long =
		      /*_#THE_MELTCALLCOUNT__L8*/ meltfnum[7];
		    /*^apply.arg */
		    argtab[1].meltbp_cstring = "warmelt-normatch.melt";
		    /*^apply.arg */
		    argtab[2].meltbp_long = 2173;
		    /*^apply.arg */
		    argtab[3].meltbp_cstring = "matchgraphic_tests curtest";
		    /*^apply.arg */
		    argtab[4].meltbp_aptr =
		      (melt_ptr_t *) & /*_.CURTEST__V14*/ meltfptr[13];
		    /*_.MELT_DEBUG_FUN__V19*/ meltfptr[18] =
		      melt_apply ((meltclosure_ptr_t)
				  (( /*!MELT_DEBUG_FUN */ meltfrout->
				    tabval[0])),
				  (melt_ptr_t) (( /*nil */ NULL)),
				  (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_PTR ""), argtab, "",
				  (union meltparam_un *) 0);
		  }
		  ;
		  /*_.IF___V18*/ meltfptr[17] =
		    /*_.MELT_DEBUG_FUN__V19*/ meltfptr[18];;
		  /*epilog */

		  MELT_LOCATION ("warmelt-normatch.melt:2173:/ clear");
		/*clear *//*_#THE_MELTCALLCOUNT__L8*/ meltfnum[7] = 0;
		  /*^clear */
		/*clear *//*_.MELT_DEBUG_FUN__V19*/ meltfptr[18] = 0;
		}
		;
	      }
	    else
	      {			/*^cond.else */

     /*_.IF___V18*/ meltfptr[17] = NULL;;
	      }
	    ;
	    MELT_LOCATION ("warmelt-normatch.melt:2173:/ quasiblock");


	    /*_.PROGN___V20*/ meltfptr[18] = /*_.IF___V18*/ meltfptr[17];;
	    /*^compute */
	    /*_.IFCPP___V17*/ meltfptr[15] = /*_.PROGN___V20*/ meltfptr[18];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-normatch.melt:2173:/ clear");
	      /*clear *//*_#MELT_NEED_DBG__L7*/ meltfnum[0] = 0;
	    /*^clear */
	      /*clear *//*_.IF___V18*/ meltfptr[17] = 0;
	    /*^clear */
	      /*clear *//*_.PROGN___V20*/ meltfptr[18] = 0;
	  }

#else /*MELT_HAVE_DEBUG */
	  /*^cppif.else */
	  /*_.IFCPP___V17*/ meltfptr[15] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
	  ;
	  MELT_LOCATION ("warmelt-normatch.melt:2175:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^apply */
	  /*apply */
	  {
	    union meltparam_un argtab[1];
	    memset (&argtab, 0, sizeof (argtab));
	    /*^apply.arg */
	    argtab[0].meltbp_aptr =
	      (melt_ptr_t *) & /*_.CURTEST__V14*/ meltfptr[13];
	    /*_.MG_OUT_NODE_NAME__V21*/ meltfptr[17] =
	      melt_apply ((meltclosure_ptr_t)
			  (( /*!MG_OUT_NODE_NAME */ meltfrout->tabval[3])),
			  (melt_ptr_t) ( /*_.NODOUT__V12*/ meltfptr[10]),
			  (MELTBPARSTR_PTR ""), argtab, "",
			  (union meltparam_un *) 0);
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-normatch.melt:2176:/ locexp");
	    meltgc_add_out ((melt_ptr_t) ( /*_.NODOUT__V12*/ meltfptr[10]),
			    (" [ label=<"));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-normatch.melt:2177:/ locexp");
	    meltgc_add_out ((melt_ptr_t) ( /*_.NODOUT__V12*/ meltfptr[10]),
			    ("<table border=\"1\" cellborder=\"1\" cellspacing=\"1\" cellpadding\
=\"1\">"));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-normatch.melt:2178:/ locexp");
	    meltgc_out_add_indent ((melt_ptr_t)
				   ( /*_.NODOUT__V12*/ meltfptr[10]), (1),
				   0);;
	  }
	  ;
	  MELT_LOCATION ("warmelt-normatch.melt:2179:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^msend */
	  /*msend */
	  {
	    union meltparam_un argtab[1];
	    memset (&argtab, 0, sizeof (argtab));
	    /*^ojbmsend.arg */
	    argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.MG__V3*/ meltfptr[2];
	    /*_.MATCHGRAPHIC_LABEL__V22*/ meltfptr[18] =
	      meltgc_send ((melt_ptr_t) ( /*_.CURTEST__V14*/ meltfptr[13]),
			   (melt_ptr_t) (( /*!MATCHGRAPHIC_LABEL */
					  meltfrout->tabval[4])),
			   (MELTBPARSTR_PTR ""), argtab, "",
			   (union meltparam_un *) 0);
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-normatch.melt:2180:/ locexp");
	    meltgc_out_add_indent ((melt_ptr_t)
				   ( /*_.NODOUT__V12*/ meltfptr[10]), (1),
				   0);;
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-normatch.melt:2181:/ locexp");
	    meltgc_add_out ((melt_ptr_t) ( /*_.NODOUT__V12*/ meltfptr[10]),
			    ("</table>"));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-normatch.melt:2182:/ locexp");
	    meltgc_add_out ((melt_ptr_t) ( /*_.NODOUT__V12*/ meltfptr[10]),
			    (">, margin=0"));
	  }
	  ;
	  MELT_LOCATION ("warmelt-normatch.melt:2183:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
  /*_#I__L9*/ meltfnum[7] =
	    (( /*_#TIX__L5*/ meltfnum[1]) == (0));;
	  MELT_LOCATION ("warmelt-normatch.melt:2183:/ cond");
	  /*cond */ if ( /*_#I__L9*/ meltfnum[7])	/*then */
	    {
	      /*^cond.then */
	      /*^block */
	      /*anyblock */
	      {


		{
		  MELT_LOCATION ("warmelt-normatch.melt:2184:/ locexp");
		  meltgc_add_out ((melt_ptr_t)
				  ( /*_.NODOUT__V12*/ meltfptr[10]),
				  (", style=\"bold\""));
		}
		;
		/*epilog */
	      }
	      ;
	    }			/*noelse */
	  ;

	  {
	    MELT_LOCATION ("warmelt-normatch.melt:2185:/ locexp");
	    meltgc_add_out ((melt_ptr_t) ( /*_.NODOUT__V12*/ meltfptr[10]),
			    (" ];"));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-normatch.melt:2186:/ locexp");
	    meltgc_out_add_indent ((melt_ptr_t)
				   ( /*_.NODOUT__V12*/ meltfptr[10]), (0),
				   0);;
	  }
	  ;
	  MELT_LOCATION ("warmelt-normatch.melt:2188:/ quasiblock");


	  /*^getslot */
	  {
	    melt_ptr_t slot = NULL, obj = NULL;
	    obj = (melt_ptr_t) ( /*_.CURTEST__V14*/ meltfptr[13]) /*=obj*/ ;
	    melt_object_get_field (slot, obj, 2, "NTEST_THEN");
   /*_.NTHEN__V23*/ meltfptr[22] = slot;
	  };
	  ;
	  MELT_LOCATION ("warmelt-normatch.melt:2189:/ getslot");
	  {
	    melt_ptr_t slot = NULL, obj = NULL;
	    obj = (melt_ptr_t) ( /*_.CURTEST__V14*/ meltfptr[13]) /*=obj*/ ;
	    melt_object_get_field (slot, obj, 3, "NTEST_ELSE");
   /*_.NELSE__V24*/ meltfptr[23] = slot;
	  };
	  ;
	  MELT_LOCATION ("warmelt-normatch.melt:2191:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
  /*_#IS_A__L10*/ meltfnum[0] =
	    melt_is_instance_of ((melt_ptr_t)
				 ( /*_.NTHEN__V23*/ meltfptr[22]),
				 (melt_ptr_t) (( /*!CLASS_NORMTESTER_ANY */
						meltfrout->tabval[2])));;
	  MELT_LOCATION ("warmelt-normatch.melt:2191:/ cond");
	  /*cond */ if ( /*_#IS_A__L10*/ meltfnum[0])	/*then */
	    {
	      /*^cond.then */
	      /*^block */
	      /*anyblock */
	      {

		MELT_LOCATION ("warmelt-normatch.melt:2193:/ checksignal");
		MELT_CHECK_SIGNAL ();
		;
		/*^apply */
		/*apply */
		{
		  union meltparam_un argtab[1];
		  memset (&argtab, 0, sizeof (argtab));
		  /*^apply.arg */
		  argtab[0].meltbp_aptr =
		    (melt_ptr_t *) & /*_.CURTEST__V14*/ meltfptr[13];
		  /*_.MG_OUT_NODE_NAME__V25*/ meltfptr[24] =
		    melt_apply ((meltclosure_ptr_t)
				(( /*!MG_OUT_NODE_NAME */ meltfrout->
				  tabval[3])),
				(melt_ptr_t) ( /*_.EDGOUT__V13*/
					      meltfptr[12]),
				(MELTBPARSTR_PTR ""), argtab, "",
				(union meltparam_un *) 0);
		}
		;

		{
		  MELT_LOCATION ("warmelt-normatch.melt:2194:/ locexp");
		  meltgc_add_out ((melt_ptr_t)
				  ( /*_.EDGOUT__V13*/ meltfptr[12]),
				  (" -> /*then*/ "));
		}
		;
		MELT_LOCATION ("warmelt-normatch.melt:2195:/ checksignal");
		MELT_CHECK_SIGNAL ();
		;
		/*^apply */
		/*apply */
		{
		  union meltparam_un argtab[1];
		  memset (&argtab, 0, sizeof (argtab));
		  /*^apply.arg */
		  argtab[0].meltbp_aptr =
		    (melt_ptr_t *) & /*_.NTHEN__V23*/ meltfptr[22];
		  /*_.MG_OUT_NODE_NAME__V26*/ meltfptr[25] =
		    melt_apply ((meltclosure_ptr_t)
				(( /*!MG_OUT_NODE_NAME */ meltfrout->
				  tabval[3])),
				(melt_ptr_t) ( /*_.EDGOUT__V13*/
					      meltfptr[12]),
				(MELTBPARSTR_PTR ""), argtab, "",
				(union meltparam_un *) 0);
		}
		;

		{
		  MELT_LOCATION ("warmelt-normatch.melt:2196:/ locexp");
		  meltgc_add_out ((melt_ptr_t)
				  ( /*_.EDGOUT__V13*/ meltfptr[12]),
				  (" [ arrowhead=normal, color=green ];"));
		}
		;

		{
		  MELT_LOCATION ("warmelt-normatch.melt:2197:/ locexp");
		  meltgc_out_add_indent ((melt_ptr_t)
					 ( /*_.EDGOUT__V13*/ meltfptr[12]),
					 (0), 0);;
		}
		;
		MELT_LOCATION ("warmelt-normatch.melt:2192:/ quasiblock");


		/*epilog */

		MELT_LOCATION ("warmelt-normatch.melt:2191:/ clear");
	      /*clear *//*_.MG_OUT_NODE_NAME__V25*/ meltfptr[24] = 0;
		/*^clear */
	      /*clear *//*_.MG_OUT_NODE_NAME__V26*/ meltfptr[25] = 0;
	      }
	      ;
	    }			/*noelse */
	  ;
	  MELT_LOCATION ("warmelt-normatch.melt:2199:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
  /*_#IS_A__L11*/ meltfnum[10] =
	    melt_is_instance_of ((melt_ptr_t)
				 ( /*_.NELSE__V24*/ meltfptr[23]),
				 (melt_ptr_t) (( /*!CLASS_NORMTESTER_ANY */
						meltfrout->tabval[2])));;
	  MELT_LOCATION ("warmelt-normatch.melt:2199:/ cond");
	  /*cond */ if ( /*_#IS_A__L11*/ meltfnum[10])	/*then */
	    {
	      /*^cond.then */
	      /*^block */
	      /*anyblock */
	      {

		MELT_LOCATION ("warmelt-normatch.melt:2201:/ checksignal");
		MELT_CHECK_SIGNAL ();
		;
		/*^apply */
		/*apply */
		{
		  union meltparam_un argtab[1];
		  memset (&argtab, 0, sizeof (argtab));
		  /*^apply.arg */
		  argtab[0].meltbp_aptr =
		    (melt_ptr_t *) & /*_.CURTEST__V14*/ meltfptr[13];
		  /*_.MG_OUT_NODE_NAME__V27*/ meltfptr[24] =
		    melt_apply ((meltclosure_ptr_t)
				(( /*!MG_OUT_NODE_NAME */ meltfrout->
				  tabval[3])),
				(melt_ptr_t) ( /*_.EDGOUT__V13*/
					      meltfptr[12]),
				(MELTBPARSTR_PTR ""), argtab, "",
				(union meltparam_un *) 0);
		}
		;

		{
		  MELT_LOCATION ("warmelt-normatch.melt:2202:/ locexp");
		  meltgc_add_out ((melt_ptr_t)
				  ( /*_.EDGOUT__V13*/ meltfptr[12]),
				  (" -> /*else*/ "));
		}
		;
		MELT_LOCATION ("warmelt-normatch.melt:2203:/ checksignal");
		MELT_CHECK_SIGNAL ();
		;
		/*^apply */
		/*apply */
		{
		  union meltparam_un argtab[1];
		  memset (&argtab, 0, sizeof (argtab));
		  /*^apply.arg */
		  argtab[0].meltbp_aptr =
		    (melt_ptr_t *) & /*_.NELSE__V24*/ meltfptr[23];
		  /*_.MG_OUT_NODE_NAME__V28*/ meltfptr[25] =
		    melt_apply ((meltclosure_ptr_t)
				(( /*!MG_OUT_NODE_NAME */ meltfrout->
				  tabval[3])),
				(melt_ptr_t) ( /*_.EDGOUT__V13*/
					      meltfptr[12]),
				(MELTBPARSTR_PTR ""), argtab, "",
				(union meltparam_un *) 0);
		}
		;

		{
		  MELT_LOCATION ("warmelt-normatch.melt:2204:/ locexp");
		  meltgc_add_out ((melt_ptr_t)
				  ( /*_.EDGOUT__V13*/ meltfptr[12]),
				  (" [ arrowhead=diamond, color=red ];"));
		}
		;

		{
		  MELT_LOCATION ("warmelt-normatch.melt:2205:/ locexp");
		  meltgc_out_add_indent ((melt_ptr_t)
					 ( /*_.EDGOUT__V13*/ meltfptr[12]),
					 (0), 0);;
		}
		;
		MELT_LOCATION ("warmelt-normatch.melt:2200:/ quasiblock");


		/*epilog */

		MELT_LOCATION ("warmelt-normatch.melt:2199:/ clear");
	      /*clear *//*_.MG_OUT_NODE_NAME__V27*/ meltfptr[24] = 0;
		/*^clear */
	      /*clear *//*_.MG_OUT_NODE_NAME__V28*/ meltfptr[25] = 0;
	      }
	      ;
	    }			/*noelse */
	  ;

	  MELT_LOCATION ("warmelt-normatch.melt:2188:/ clear");
	    /*clear *//*_.NTHEN__V23*/ meltfptr[22] = 0;
	  /*^clear */
	    /*clear *//*_.NELSE__V24*/ meltfptr[23] = 0;
	  /*^clear */
	    /*clear *//*_#IS_A__L10*/ meltfnum[0] = 0;
	  /*^clear */
	    /*clear *//*_#IS_A__L11*/ meltfnum[10] = 0;
	  if ( /*_#TIX__L5*/ meltfnum[1] < 0)
	    break;
	}			/* end  foreach_in_multiple meltcit1__EACHTUP */

      /*citerepilog */

      MELT_LOCATION ("warmelt-normatch.melt:2169:/ clear");
	    /*clear *//*_.CURTEST__V14*/ meltfptr[13] = 0;
      /*^clear */
	    /*clear *//*_#TIX__L5*/ meltfnum[1] = 0;
      /*^clear */
	    /*clear *//*_.IFCPP___V15*/ meltfptr[14] = 0;
      /*^clear */
	    /*clear *//*_.IFCPP___V17*/ meltfptr[15] = 0;
      /*^clear */
	    /*clear *//*_.MG_OUT_NODE_NAME__V21*/ meltfptr[17] = 0;
      /*^clear */
	    /*clear *//*_.MATCHGRAPHIC_LABEL__V22*/ meltfptr[18] = 0;
      /*^clear */
	    /*clear *//*_#I__L9*/ meltfnum[7] = 0;
    }				/*endciterblock FOREACH_IN_MULTIPLE */
    ;

    MELT_LOCATION ("warmelt-normatch.melt:2164:/ clear");
	   /*clear *//*_.NODOUT__V12*/ meltfptr[10] = 0;
    /*^clear */
	   /*clear *//*_.EDGOUT__V13*/ meltfptr[12] = 0;
    MELT_LOCATION ("warmelt-normatch.melt:2160:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V4*/ meltfptr[3] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V8*/ meltfptr[4] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V10*/ meltfptr[5] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("MATCHGRAPHIC_TESTS", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_49_warmelt_normatch_MATCHGRAPHIC_TESTS_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*noretval */ NULL);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_49_warmelt_normatch_MATCHGRAPHIC_TESTS */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_50_warmelt_normatch_MG_DRAW_MATCH_GRAPHVIZ_FILE (meltclosure_ptr_t
							  meltclosp_,
							  melt_ptr_t
							  meltfirstargp_,
							  const
							  melt_argdescr_cell_t
							  meltxargdescr_[],
							  union meltparam_un *
							  meltxargtab_,
							  const
							  melt_argdescr_cell_t
							  meltxresdescr_[],
							  union meltparam_un *
							  meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_50_warmelt_normatch_MG_DRAW_MATCH_GRAPHVIZ_FILE_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_50_warmelt_normatch_MG_DRAW_MATCH_GRAPHVIZ_FILE_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 28
    melt_ptr_t mcfr_varptr[28];
#define MELTFRAM_NBVARNUM 11
    long mcfr_varnum[11];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_50_warmelt_normatch_MG_DRAW_MATCH_GRAPHVIZ_FILE is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct
	 frame_meltrout_50_warmelt_normatch_MG_DRAW_MATCH_GRAPHVIZ_FILE_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 28; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_50_warmelt_normatch_MG_DRAW_MATCH_GRAPHVIZ_FILE nbval 28*/
  meltfram__.mcfr_nbvar = 28 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("MG_DRAW_MATCH_GRAPHVIZ_FILE", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-normatch.melt:2213:/ getarg");
 /*_.NMATCH__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.DOTPREFIX__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.DOTPREFIX__V3*/ meltfptr[2])) !=
	      NULL);


  /*getarg#2 */
  /*^getarg */
  if (meltxargdescr_[1] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.TESTSTUPL__V4*/ meltfptr[3] =
    (meltxargtab_[1].meltbp_aptr) ? (*(meltxargtab_[1].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.TESTSTUPL__V4*/ meltfptr[3])) !=
	      NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {

    MELT_LOCATION ("warmelt-normatch.melt:2214:/ quasiblock");


 /*_#HCODNMATCH__L1*/ meltfnum[0] =
      (melt_obj_hash ((melt_ptr_t) ( /*_.NMATCH__V2*/ meltfptr[1])));;
    /*^compute */
 /*_#CNT__L2*/ meltfnum[1] = 0;;
    /*^compute */
 /*_#NBTESTS__L3*/ meltfnum[2] =
      (melt_multiple_length
       ((melt_ptr_t) ( /*_.TESTSTUPL__V4*/ meltfptr[3])));;
    /*^compute */
 /*_.PATHSBUF__V5*/ meltfptr[4] =
      (melt_ptr_t)
      meltgc_new_strbuf ((meltobject_ptr_t)
			 (( /*!DISCR_STRBUF */ meltfrout->tabval[0])),
			 (const char *) 0);;

    {
      MELT_LOCATION ("warmelt-normatch.melt:2223:/ locexp");
      /* mg_draw_match_graphviz_file UNIQCNT__1 */
      {
	static long uniqcounter;
	uniqcounter++;
	    /*_#CNT__L2*/ meltfnum[1] = uniqcounter;
      }
      ;
    }
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-normatch.melt:2228:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_STRBUF__L4*/ meltfnum[3] =
	(melt_magic_discr ((melt_ptr_t) ( /*_.PATHSBUF__V5*/ meltfptr[4])) ==
	 MELTOBMAG_STRBUF);;
      MELT_LOCATION ("warmelt-normatch.melt:2228:/ cond");
      /*cond */ if ( /*_#IS_STRBUF__L4*/ meltfnum[3])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V7*/ meltfptr[6] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-normatch.melt:2228:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check pathsbuf"),
				  ("warmelt-normatch.melt")
				  ? ("warmelt-normatch.melt") : __FILE__,
				  (2228) ? (2228) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V7*/ meltfptr[6] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V6*/ meltfptr[5] = /*_.IFELSE___V7*/ meltfptr[6];;
      /*epilog */

      MELT_LOCATION ("warmelt-normatch.melt:2228:/ clear");
	     /*clear *//*_#IS_STRBUF__L4*/ meltfnum[3] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V7*/ meltfptr[6] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V6*/ meltfptr[5] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

    {
      MELT_LOCATION ("warmelt-normatch.melt:2229:/ locexp");
      /*add2sbuf_string */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.PATHSBUF__V5*/ meltfptr[4]),
			   melt_string_str ((melt_ptr_t)
					    ( /*_.DOTPREFIX__V3*/
					     meltfptr[2])));
    }
    ;

    {
      MELT_LOCATION ("warmelt-normatch.melt:2230:/ locexp");
      meltgc_add_strbuf_dec ((melt_ptr_t) ( /*_.PATHSBUF__V5*/ meltfptr[4]),
			     ( /*_#CNT__L2*/ meltfnum[1]));
    }
    ;

    {
      MELT_LOCATION ("warmelt-normatch.melt:2231:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.PATHSBUF__V5*/ meltfptr[4]),
			   (".dot"));
    }
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-normatch.melt:2232:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L5*/ meltfnum[3] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-normatch.melt:2232:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L5*/ meltfnum[3])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L6*/ meltfnum[5] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-normatch.melt:2232:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L6*/ meltfnum[5];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-normatch.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 2232;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring =
		"mg_draw_match_graphviz_file pathsbuf";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.PATHSBUF__V5*/ meltfptr[4];
	      /*_.MELT_DEBUG_FUN__V10*/ meltfptr[9] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[1])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V9*/ meltfptr[8] =
	      /*_.MELT_DEBUG_FUN__V10*/ meltfptr[9];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-normatch.melt:2232:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L6*/ meltfnum[5] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V10*/ meltfptr[9] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V9*/ meltfptr[8] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-normatch.melt:2232:/ quasiblock");


      /*_.PROGN___V11*/ meltfptr[9] = /*_.IF___V9*/ meltfptr[8];;
      /*^compute */
      /*_.IFCPP___V8*/ meltfptr[6] = /*_.PROGN___V11*/ meltfptr[9];;
      /*epilog */

      MELT_LOCATION ("warmelt-normatch.melt:2232:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L5*/ meltfnum[3] = 0;
      /*^clear */
	     /*clear *//*_.IF___V9*/ meltfptr[8] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V11*/ meltfptr[9] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V8*/ meltfptr[6] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    /*^compute */
 /*_.STRBUF2STRING__V12*/ meltfptr[8] =
      (meltgc_new_stringdup
       ((meltobject_ptr_t) (( /*!DISCR_STRING */ meltfrout->tabval[2])),
	melt_strbuf_str ((melt_ptr_t) ( /*_.PATHSBUF__V5*/ meltfptr[4]))));;
    MELT_LOCATION ("warmelt-normatch.melt:2220:/ quasiblock");


    /*_.DOTFILENAME__V13*/ meltfptr[9] =
      /*_.STRBUF2STRING__V12*/ meltfptr[8];;
    /*^compute */
 /*_.NODBUF__V14*/ meltfptr[13] =
      (melt_ptr_t)
      meltgc_new_strbuf ((meltobject_ptr_t)
			 (( /*!DISCR_STRBUF */ meltfrout->tabval[0])),
			 (const char *) 0);;
    /*^compute */
 /*_.EDGBUF__V15*/ meltfptr[14] =
      (melt_ptr_t)
      meltgc_new_strbuf ((meltobject_ptr_t)
			 (( /*!DISCR_STRBUF */ meltfrout->tabval[0])),
			 (const char *) 0);;
    MELT_LOCATION ("warmelt-normatch.melt:2236:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*^rawallocobj */
    /*rawallocobj */
    {
      melt_ptr_t newobj = 0;
      melt_raw_object_create (newobj,
			      (melt_ptr_t) (( /*!CLASS_MATCH_GRAPHIC */
					     meltfrout->tabval[3])), (6),
			      "CLASS_MATCH_GRAPHIC");
  /*_.INST__V17*/ meltfptr[16] =
	newobj;
    };
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @MCHGX_FILENAME",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V17*/ meltfptr[16])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V17*/ meltfptr[16]), (1),
			  ( /*_.DOTFILENAME__V13*/ meltfptr[9]),
			  "MCHGX_FILENAME");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @MCHGX_NODOUT",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V17*/ meltfptr[16])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V17*/ meltfptr[16]), (2),
			  ( /*_.NODBUF__V14*/ meltfptr[13]), "MCHGX_NODOUT");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @MCHGX_EDGOUT",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V17*/ meltfptr[16])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V17*/ meltfptr[16]), (3),
			  ( /*_.EDGBUF__V15*/ meltfptr[14]), "MCHGX_EDGOUT");
    ;
    /*^touchobj */

    melt_dbgtrace_written_object ( /*_.INST__V17*/ meltfptr[16],
				  "newly made instance");
    ;
    /*_.MG__V16*/ meltfptr[15] = /*_.INST__V17*/ meltfptr[16];;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-normatch.melt:2242:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L7*/ meltfnum[5] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-normatch.melt:2242:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L7*/ meltfnum[5])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L8*/ meltfnum[3] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-normatch.melt:2242:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L8*/ meltfnum[3];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-normatch.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 2242;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring =
		"mg_draw_match_graphviz_file initial mg";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.MG__V16*/ meltfptr[15];
	      /*_.MELT_DEBUG_FUN__V20*/ meltfptr[19] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[1])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V19*/ meltfptr[18] =
	      /*_.MELT_DEBUG_FUN__V20*/ meltfptr[19];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-normatch.melt:2242:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L8*/ meltfnum[3] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V20*/ meltfptr[19] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V19*/ meltfptr[18] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-normatch.melt:2242:/ quasiblock");


      /*_.PROGN___V21*/ meltfptr[19] = /*_.IF___V19*/ meltfptr[18];;
      /*^compute */
      /*_.IFCPP___V18*/ meltfptr[17] = /*_.PROGN___V21*/ meltfptr[19];;
      /*epilog */

      MELT_LOCATION ("warmelt-normatch.melt:2242:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L7*/ meltfnum[5] = 0;
      /*^clear */
	     /*clear *//*_.IF___V19*/ meltfptr[18] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V21*/ meltfptr[19] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V18*/ meltfptr[17] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-normatch.melt:2243:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.MG__V16*/ meltfptr[15];
      /*_.MATCHGRAPHIC_TESTS__V22*/ meltfptr[18] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!MATCHGRAPHIC_TESTS */ meltfrout->tabval[4])),
		    (melt_ptr_t) ( /*_.TESTSTUPL__V4*/ meltfptr[3]),
		    (MELTBPARSTR_PTR ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-normatch.melt:2244:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L9*/ meltfnum[3] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-normatch.melt:2244:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L9*/ meltfnum[3])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L10*/ meltfnum[5] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-normatch.melt:2244:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long =
		/*_#THE_MELTCALLCOUNT__L10*/ meltfnum[5];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-normatch.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 2244;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring =
		"mg_draw_match_graphviz_file final mg";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.MG__V16*/ meltfptr[15];
	      /*_.MELT_DEBUG_FUN__V25*/ meltfptr[24] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[1])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V24*/ meltfptr[23] =
	      /*_.MELT_DEBUG_FUN__V25*/ meltfptr[24];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-normatch.melt:2244:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L10*/ meltfnum[5] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V25*/ meltfptr[24] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V24*/ meltfptr[23] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-normatch.melt:2244:/ quasiblock");


      /*_.PROGN___V26*/ meltfptr[24] = /*_.IF___V24*/ meltfptr[23];;
      /*^compute */
      /*_.IFCPP___V23*/ meltfptr[19] = /*_.PROGN___V26*/ meltfptr[24];;
      /*epilog */

      MELT_LOCATION ("warmelt-normatch.melt:2244:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L9*/ meltfnum[3] = 0;
      /*^clear */
	     /*clear *//*_.IF___V24*/ meltfptr[23] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V26*/ meltfptr[24] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V23*/ meltfptr[19] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-normatch.melt:2245:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_STRING__L11*/ meltfnum[5] =
	(melt_magic_discr ((melt_ptr_t) ( /*_.DOTFILENAME__V13*/ meltfptr[9]))
	 == MELTOBMAG_STRING);;
      MELT_LOCATION ("warmelt-normatch.melt:2245:/ cond");
      /*cond */ if ( /*_#IS_STRING__L11*/ meltfnum[5])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V28*/ meltfptr[24] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-normatch.melt:2245:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check dotfilename"),
				  ("warmelt-normatch.melt")
				  ? ("warmelt-normatch.melt") : __FILE__,
				  (2245) ? (2245) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V28*/ meltfptr[24] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V27*/ meltfptr[23] = /*_.IFELSE___V28*/ meltfptr[24];;
      /*epilog */

      MELT_LOCATION ("warmelt-normatch.melt:2245:/ clear");
	     /*clear *//*_#IS_STRING__L11*/ meltfnum[5] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V28*/ meltfptr[24] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V27*/ meltfptr[23] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

    {
      MELT_LOCATION ("warmelt-normatch.melt:2247:/ locexp");
      /* mg_draw_match_graphviz_file OUTPUTMG__1 */
      {
	time_t nowt = 0;
	char nowbuf[60];
	FILE *dotfil =
	  fopen (melt_string_str
		 ((melt_ptr_t) /*_.DOTFILENAME__V13*/ meltfptr[9]), "w");
	if (!dotfil)
	  melt_fatal_error ("failed to open matchdot file %s - %m",
			    melt_string_str ((melt_ptr_t)
					     /*_.DOTFILENAME__V13*/
					     meltfptr[9]));
	fprintf (dotfil, "// melt matchdot file %s\n",
		 melt_string_str ((melt_ptr_t) /*_.DOTFILENAME__V13*/
				  meltfptr[9]));
	time (&nowt);
	memset (nowbuf, 0, sizeof (nowbuf));
	strftime (nowbuf, sizeof (nowbuf) - 1,
		  "%Y %b %d %Hh%M", localtime (&nowt));
	fprintf (dotfil, "// generated %s\n", nowbuf);
	fprintf (dotfil, "digraph meltmatch_%lx {\n",
		      /*_#HCODNMATCH__L1*/ meltfnum[0]);
	fprintf (dotfil,
		 " graph [ label=\"Melt Match %d #%#lx %s\", pad=\"0.5\", margin=\"0.3\" ];\n",
		 (int) /*_#CNT__L2*/ meltfnum[1], /*_#HCODNMATCH__L1*/
		 meltfnum[0], nowbuf);
	fprintf (dotfil, " node [ shape=\"box\", fontsize=\"12\" ];\n");
	fprintf (dotfil, "// %d tests\n",
		 (int) /*_#NBTESTS__L3*/ meltfnum[2]);
	melt_putstrbuf (dotfil, (melt_ptr_t) /*_.NODBUF__V14*/ meltfptr[13]);
	fprintf (dotfil, "\n /// edges\n");
	melt_putstrbuf (dotfil, (melt_ptr_t) /*_.EDGBUF__V15*/ meltfptr[14]);
	fprintf (dotfil, "\n} // eof %s\n",
		 melt_string_str ((melt_ptr_t) /*_.DOTFILENAME__V13*/
				  meltfptr[9]));
	fclose (dotfil);
      }				/* end mg_draw_match_graphviz_file OUTPUTMG__1 */
      ;
    }
    ;

    MELT_LOCATION ("warmelt-normatch.melt:2214:/ clear");
	   /*clear *//*_#HCODNMATCH__L1*/ meltfnum[0] = 0;
    /*^clear */
	   /*clear *//*_#CNT__L2*/ meltfnum[1] = 0;
    /*^clear */
	   /*clear *//*_#NBTESTS__L3*/ meltfnum[2] = 0;
    /*^clear */
	   /*clear *//*_.PATHSBUF__V5*/ meltfptr[4] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V6*/ meltfptr[5] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V8*/ meltfptr[6] = 0;
    /*^clear */
	   /*clear *//*_.STRBUF2STRING__V12*/ meltfptr[8] = 0;
    /*^clear */
	   /*clear *//*_.DOTFILENAME__V13*/ meltfptr[9] = 0;
    /*^clear */
	   /*clear *//*_.NODBUF__V14*/ meltfptr[13] = 0;
    /*^clear */
	   /*clear *//*_.EDGBUF__V15*/ meltfptr[14] = 0;
    /*^clear */
	   /*clear *//*_.MG__V16*/ meltfptr[15] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V18*/ meltfptr[17] = 0;
    /*^clear */
	   /*clear *//*_.MATCHGRAPHIC_TESTS__V22*/ meltfptr[18] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V23*/ meltfptr[19] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V27*/ meltfptr[23] = 0;
    /*epilog */
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("MG_DRAW_MATCH_GRAPHVIZ_FILE", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_50_warmelt_normatch_MG_DRAW_MATCH_GRAPHVIZ_FILE_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*noretval */ NULL);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_50_warmelt_normatch_MG_DRAW_MATCH_GRAPHVIZ_FILE */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_51_warmelt_normatch_NORMEXP_MATCH (meltclosure_ptr_t meltclosp_,
					    melt_ptr_t meltfirstargp_,
					    const melt_argdescr_cell_t
					    meltxargdescr_[],
					    union meltparam_un * meltxargtab_,
					    const melt_argdescr_cell_t
					    meltxresdescr_[],
					    union meltparam_un * meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_51_warmelt_normatch_NORMEXP_MATCH_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_51_warmelt_normatch_NORMEXP_MATCH_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 328
    melt_ptr_t mcfr_varptr[328];
#define MELTFRAM_NBVARNUM 118
    long mcfr_varnum[118];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_51_warmelt_normatch_NORMEXP_MATCH is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_51_warmelt_normatch_NORMEXP_MATCH_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 328; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_51_warmelt_normatch_NORMEXP_MATCH nbval 328*/
  meltfram__.mcfr_nbvar = 328 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("NORMEXP_MATCH", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-normatch.melt:2280:/ getarg");
 /*_.RECV__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.ENV__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.ENV__V3*/ meltfptr[2])) != NULL);


  /*getarg#2 */
  /*^getarg */
  if (meltxargdescr_[1] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.NCX__V4*/ meltfptr[3] =
    (meltxargtab_[1].meltbp_aptr) ? (*(meltxargtab_[1].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.NCX__V4*/ meltfptr[3])) != NULL);


  /*getarg#3 */
  /*^getarg */
  if (meltxargdescr_[2] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.PSLOC__V5*/ meltfptr[4] =
    (meltxargtab_[2].meltbp_aptr) ? (*(meltxargtab_[2].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.PSLOC__V5*/ meltfptr[4])) !=
	      NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-normatch.melt:2281:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L1*/ meltfnum[0] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.RECV__V2*/ meltfptr[1]),
			     (melt_ptr_t) (( /*!CLASS_SOURCE_MATCH */
					    meltfrout->tabval[0])));;
      MELT_LOCATION ("warmelt-normatch.melt:2281:/ cond");
      /*cond */ if ( /*_#IS_A__L1*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V7*/ meltfptr[6] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-normatch.melt:2281:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check match recv"),
				  ("warmelt-normatch.melt")
				  ? ("warmelt-normatch.melt") : __FILE__,
				  (2281) ? (2281) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V7*/ meltfptr[6] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V6*/ meltfptr[5] = /*_.IFELSE___V7*/ meltfptr[6];;
      /*epilog */

      MELT_LOCATION ("warmelt-normatch.melt:2281:/ clear");
	     /*clear *//*_#IS_A__L1*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V7*/ meltfptr[6] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V6*/ meltfptr[5] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-normatch.melt:2282:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L2*/ meltfnum[0] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.ENV__V3*/ meltfptr[2]),
			     (melt_ptr_t) (( /*!CLASS_ENVIRONMENT */
					    meltfrout->tabval[1])));;
      MELT_LOCATION ("warmelt-normatch.melt:2282:/ cond");
      /*cond */ if ( /*_#IS_A__L2*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V9*/ meltfptr[8] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-normatch.melt:2282:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check env"),
				  ("warmelt-normatch.melt")
				  ? ("warmelt-normatch.melt") : __FILE__,
				  (2282) ? (2282) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V9*/ meltfptr[8] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V8*/ meltfptr[6] = /*_.IFELSE___V9*/ meltfptr[8];;
      /*epilog */

      MELT_LOCATION ("warmelt-normatch.melt:2282:/ clear");
	     /*clear *//*_#IS_A__L2*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V9*/ meltfptr[8] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V8*/ meltfptr[6] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-normatch.melt:2283:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L3*/ meltfnum[0] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.NCX__V4*/ meltfptr[3]),
			     (melt_ptr_t) (( /*!CLASS_NORMALIZATION_CONTEXT */
					    meltfrout->tabval[2])));;
      MELT_LOCATION ("warmelt-normatch.melt:2283:/ cond");
      /*cond */ if ( /*_#IS_A__L3*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V11*/ meltfptr[10] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-normatch.melt:2283:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check nctxt"),
				  ("warmelt-normatch.melt")
				  ? ("warmelt-normatch.melt") : __FILE__,
				  (2283) ? (2283) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V11*/ meltfptr[10] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V10*/ meltfptr[8] = /*_.IFELSE___V11*/ meltfptr[10];;
      /*epilog */

      MELT_LOCATION ("warmelt-normatch.melt:2283:/ clear");
	     /*clear *//*_#IS_A__L3*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V11*/ meltfptr[10] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V10*/ meltfptr[8] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-normatch.melt:2284:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L4*/ meltfnum[0] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-normatch.melt:2284:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L4*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L5*/ meltfnum[4] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-normatch.melt:2284:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L5*/ meltfnum[4];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-normatch.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 2284;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "normexp_match recv";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.RECV__V2*/ meltfptr[1];
	      /*_.MELT_DEBUG_FUN__V14*/ meltfptr[13] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[3])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V13*/ meltfptr[12] =
	      /*_.MELT_DEBUG_FUN__V14*/ meltfptr[13];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-normatch.melt:2284:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L5*/ meltfnum[4] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V14*/ meltfptr[13] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V13*/ meltfptr[12] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-normatch.melt:2284:/ quasiblock");


      /*_.PROGN___V15*/ meltfptr[13] = /*_.IF___V13*/ meltfptr[12];;
      /*^compute */
      /*_.IFCPP___V12*/ meltfptr[10] = /*_.PROGN___V15*/ meltfptr[13];;
      /*epilog */

      MELT_LOCATION ("warmelt-normatch.melt:2284:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L4*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IF___V13*/ meltfptr[12] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V15*/ meltfptr[13] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V12*/ meltfptr[10] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-normatch.melt:2285:/ quasiblock");


    /*^getslot */
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.RECV__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 1, "LOCA_LOCATION");
  /*_.SLOC__V17*/ meltfptr[13] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-normatch.melt:2286:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.RECV__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 2, "SMAT_MATCHEDX");
  /*_.SMATSX__V18*/ meltfptr[17] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-normatch.melt:2287:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.RECV__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 3, "SMAT_CASES");
  /*_.SCASES__V19*/ meltfptr[18] = slot;
    };
    ;
 /*_#NBCASES__L6*/ meltfnum[4] =
      (melt_multiple_length
       ((melt_ptr_t) ( /*_.SCASES__V19*/ meltfptr[18])));;
    /*^compute */
 /*_.TUPVARMAP__V20*/ meltfptr[19] =
      (meltgc_new_multiple
       ((meltobject_ptr_t) (( /*!DISCR_MULTIPLE */ meltfrout->tabval[4])),
	( /*_#NBCASES__L6*/ meltfnum[4])));;
    /*^compute */
 /*_.TUPCSTMAP__V21*/ meltfptr[20] =
      (meltgc_new_multiple
       ((meltobject_ptr_t) (( /*!DISCR_MULTIPLE */ meltfrout->tabval[4])),
	( /*_#NBCASES__L6*/ meltfnum[4])));;
    /*^compute */
 /*_#I__L7*/ meltfnum[0] =
      ((5) * ( /*_#NBCASES__L6*/ meltfnum[4]));;
    /*^compute */
 /*_#I__L8*/ meltfnum[7] =
      ((20) + ( /*_#I__L7*/ meltfnum[0]));;
    /*^compute */
 /*_.STUFFMAP__V22*/ meltfptr[21] =
      (meltgc_new_mapobjects
       ((meltobject_ptr_t) (( /*!DISCR_MAP_OBJECTS */ meltfrout->tabval[5])),
	( /*_#I__L8*/ meltfnum[7])));;
    /*^compute */
 /*_.SHABINDLIST__V23*/ meltfptr[22] =
      (meltgc_new_list
       ((meltobject_ptr_t) (( /*!DISCR_LIST */ meltfrout->tabval[6]))));;
    /*^compute */
 /*_.TESTLIST__V24*/ meltfptr[23] =
      (meltgc_new_list
       ((meltobject_ptr_t) (( /*!DISCR_LIST */ meltfrout->tabval[6]))));;
    /*^compute */
    /*_.WHOLECTYPE__V25*/ meltfptr[24] = ( /*nil */ NULL);;
    /*^compute */
    /*_.OLDTESTER__V26*/ meltfptr[25] = ( /*nil */ NULL);;
    MELT_LOCATION ("warmelt-normatch.melt:2298:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      /*_.CINTSYMB__V27*/ meltfptr[26] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!CLONE_SYMBOL */ meltfrout->tabval[7])),
		    (melt_ptr_t) (( /*!konst_8_MATCH_INTER_ */ meltfrout->
				   tabval[8])), (""),
		    (union meltparam_un *) 0, "", (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-normatch.melt:2299:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*^rawallocobj */
    /*rawallocobj */
    {
      melt_ptr_t newobj = 0;
      melt_raw_object_create (newobj,
			      (melt_ptr_t) (( /*!CLASS_NREP_CHECKSIGNAL */
					     meltfrout->tabval[9])), (1),
			      "CLASS_NREP_CHECKSIGNAL");
  /*_.INST__V29*/ meltfptr[28] =
	newobj;
    };
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @NREP_LOC",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V29*/ meltfptr[28])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V29*/ meltfptr[28]), (0),
			  ( /*_.SLOC__V17*/ meltfptr[13]), "NREP_LOC");
    ;
    /*^touchobj */

    melt_dbgtrace_written_object ( /*_.INST__V29*/ meltfptr[28],
				  "newly made instance");
    ;
    /*_.NCHINT__V28*/ meltfptr[27] = /*_.INST__V29*/ meltfptr[28];;
    MELT_LOCATION ("warmelt-normatch.melt:2301:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*^rawallocobj */
    /*rawallocobj */
    {
      melt_ptr_t newobj = 0;
      melt_raw_object_create (newobj,
			      (melt_ptr_t) (( /*!CLASS_NORMAL_LET_BINDING */
					     meltfrout->tabval[10])), (4),
			      "CLASS_NORMAL_LET_BINDING");
  /*_.INST__V31*/ meltfptr[30] =
	newobj;
    };
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @BINDER",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V31*/ meltfptr[30])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V31*/ meltfptr[30]), (0),
			  ( /*_.CINTSYMB__V27*/ meltfptr[26]), "BINDER");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @LETBIND_TYPE",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V31*/ meltfptr[30])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V31*/ meltfptr[30]), (1),
			  (( /*!CTYPE_VOID */ meltfrout->tabval[11])),
			  "LETBIND_TYPE");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @LETBIND_EXPR",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V31*/ meltfptr[30])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V31*/ meltfptr[30]), (2),
			  ( /*_.NCHINT__V28*/ meltfptr[27]), "LETBIND_EXPR");
    ;
    /*^touchobj */

    melt_dbgtrace_written_object ( /*_.INST__V31*/ meltfptr[30],
				  "newly made instance");
    ;
    /*_.CINTBIND__V30*/ meltfptr[29] = /*_.INST__V31*/ meltfptr[30];;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-normatch.melt:2306:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L9*/ meltfnum[8] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-normatch.melt:2306:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L9*/ meltfnum[8])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L10*/ meltfnum[9] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-normatch.melt:2306:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[7];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long =
		/*_#THE_MELTCALLCOUNT__L10*/ meltfnum[9];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-normatch.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 2306;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "normexp_match smatsx=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.SMATSX__V18*/ meltfptr[17];
	      /*^apply.arg */
	      argtab[5].meltbp_cstring = " cintbind=";
	      /*^apply.arg */
	      argtab[6].meltbp_aptr =
		(melt_ptr_t *) & /*_.CINTBIND__V30*/ meltfptr[29];
	      /*_.MELT_DEBUG_FUN__V34*/ meltfptr[33] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[3])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V33*/ meltfptr[32] =
	      /*_.MELT_DEBUG_FUN__V34*/ meltfptr[33];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-normatch.melt:2306:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L10*/ meltfnum[9] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V34*/ meltfptr[33] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V33*/ meltfptr[32] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-normatch.melt:2306:/ quasiblock");


      /*_.PROGN___V35*/ meltfptr[33] = /*_.IF___V33*/ meltfptr[32];;
      /*^compute */
      /*_.IFCPP___V32*/ meltfptr[31] = /*_.PROGN___V35*/ meltfptr[33];;
      /*epilog */

      MELT_LOCATION ("warmelt-normatch.melt:2306:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L9*/ meltfnum[8] = 0;
      /*^clear */
	     /*clear *//*_.IF___V33*/ meltfptr[32] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V35*/ meltfptr[33] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V32*/ meltfptr[31] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-normatch.melt:2309:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    MELT_LOCATION ("warmelt-normatch.melt:2307:/ quasiblock");


    /*^multimsend */
    /*multimsend */
    {
      union meltparam_un argtab[3];
      union meltparam_un restab[1];
      memset (&argtab, 0, sizeof (argtab));
      memset (&restab, 0, sizeof (restab));
      /*^multimsend.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.ENV__V3*/ meltfptr[2];	/*^multimsend.arg */
      argtab[1].meltbp_aptr = (melt_ptr_t *) & /*_.NCX__V4*/ meltfptr[3];	/*^multimsend.arg */
      argtab[2].meltbp_aptr = (melt_ptr_t *) & /*_.SLOC__V17*/ meltfptr[13];
      /*^multimsend.xres */
      restab[0].meltbp_aptr = (melt_ptr_t *) & /*_.NBINDMATX__V38*/ meltfptr[37];	/*^multimsend.send */
      /*_.NMATX__V37*/ meltfptr[33] =
	meltgc_send ((melt_ptr_t) ( /*_.SMATSX__V18*/ meltfptr[17]),
		     ((melt_ptr_t)
		      (( /*!NORMAL_EXP */ meltfrout->tabval[12]))),
		     (MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_PTR ""),
		     argtab, (MELTBPARSTR_PTR ""), restab);
    }
    ;
    /*^quasiblock */



#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-normatch.melt:2310:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L11*/ meltfnum[9] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-normatch.melt:2310:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L11*/ meltfnum[9])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L12*/ meltfnum[8] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-normatch.melt:2310:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[9];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long =
		/*_#THE_MELTCALLCOUNT__L12*/ meltfnum[8];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-normatch.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 2310;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "normexp_match nmatx=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.NMATX__V37*/ meltfptr[33];
	      /*^apply.arg */
	      argtab[5].meltbp_cstring = " nbindmatx=";
	      /*^apply.arg */
	      argtab[6].meltbp_aptr =
		(melt_ptr_t *) & /*_.NBINDMATX__V38*/ meltfptr[37];
	      /*^apply.arg */
	      argtab[7].meltbp_cstring = " scases=";
	      /*^apply.arg */
	      argtab[8].meltbp_aptr =
		(melt_ptr_t *) & /*_.SCASES__V19*/ meltfptr[18];
	      /*_.MELT_DEBUG_FUN__V41*/ meltfptr[40] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[3])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V40*/ meltfptr[39] =
	      /*_.MELT_DEBUG_FUN__V41*/ meltfptr[40];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-normatch.melt:2310:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L12*/ meltfnum[8] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V41*/ meltfptr[40] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V40*/ meltfptr[39] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-normatch.melt:2310:/ quasiblock");


      /*_.PROGN___V42*/ meltfptr[40] = /*_.IF___V40*/ meltfptr[39];;
      /*^compute */
      /*_.IFCPP___V39*/ meltfptr[38] = /*_.PROGN___V42*/ meltfptr[40];;
      /*epilog */

      MELT_LOCATION ("warmelt-normatch.melt:2310:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L11*/ meltfnum[9] = 0;
      /*^clear */
	     /*clear *//*_.IF___V40*/ meltfptr[39] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V42*/ meltfptr[40] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V39*/ meltfptr[38] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-normatch.melt:2311:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#IS_LIST__L13*/ meltfnum[8] =
      (melt_magic_discr ((melt_ptr_t) ( /*_.NBINDMATX__V38*/ meltfptr[37])) ==
       MELTOBMAG_LIST);;
    MELT_LOCATION ("warmelt-normatch.melt:2311:/ cond");
    /*cond */ if ( /*_#IS_LIST__L13*/ meltfnum[8])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{




	  {
	    MELT_LOCATION ("warmelt-normatch.melt:2312:/ locexp");
	    meltgc_append_list ((melt_ptr_t)
				( /*_.NBINDMATX__V38*/ meltfptr[37]),
				(melt_ptr_t) ( /*_.CINTBIND__V30*/
					      meltfptr[29]));
	  }
	  ;
	     /*clear *//*_.IFELSE___V43*/ meltfptr[39] = 0;
	  /*epilog */
	}
	;
      }
    else
      {
	MELT_LOCATION ("warmelt-normatch.melt:2311:/ cond.else");

	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-normatch.melt:2313:/ blockmultialloc");
	  /*multiallocblock */
	  {
	    struct meltletrec_1_st
	    {
	      struct meltpair_st rpair_0__CINTBIND_x1;
	      struct meltlist_st rlist_1__LIST_;
	      long meltletrec_1_endgap;
	    } *meltletrec_1_ptr = 0;
	    meltletrec_1_ptr =
	      (struct meltletrec_1_st *)
	      meltgc_allocate (sizeof (struct meltletrec_1_st), 0);
	    /*^blockmultialloc.initfill */
	    /*inipair rpair_0__CINTBIND_x1 */
     /*_.CINTBIND__V45*/ meltfptr[44] =
	      (melt_ptr_t) & meltletrec_1_ptr->rpair_0__CINTBIND_x1;
	    meltletrec_1_ptr->rpair_0__CINTBIND_x1.discr =
	      (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (DISCR_PAIR))));

	    /*inilist rlist_1__LIST_ */
     /*_.LIST___V46*/ meltfptr[45] =
	      (melt_ptr_t) & meltletrec_1_ptr->rlist_1__LIST_;
	    meltletrec_1_ptr->rlist_1__LIST_.discr =
	      (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (DISCR_LIST))));



	    /*^putpairhead */
	    /*putpairhead */
	    melt_assertmsg ("putpairhead /1 checkpair",
			    melt_magic_discr ((melt_ptr_t)
					      ( /*_.CINTBIND__V45*/
					       meltfptr[44])) ==
			    MELTOBMAG_PAIR);
	    ((meltpair_ptr_t) ( /*_.CINTBIND__V45*/ meltfptr[44]))->hd =
	      (melt_ptr_t) ( /*_.CINTBIND__V30*/ meltfptr[29]);
	    ;
	    /*^touch */
	    meltgc_touch ( /*_.CINTBIND__V45*/ meltfptr[44]);
	    ;
	    /*^putlist */
	    /*putlist */
	    melt_assertmsg ("putlist checklist",
			    melt_magic_discr ((melt_ptr_t)
					      ( /*_.LIST___V46*/
					       meltfptr[45])) ==
			    MELTOBMAG_LIST);
	    ((meltlist_ptr_t) ( /*_.LIST___V46*/ meltfptr[45]))->first =
	      (meltpair_ptr_t) ( /*_.CINTBIND__V45*/ meltfptr[44]);
	    ((meltlist_ptr_t) ( /*_.LIST___V46*/ meltfptr[45]))->last =
	      (meltpair_ptr_t) ( /*_.CINTBIND__V45*/ meltfptr[44]);
	    ;
	    /*^touch */
	    meltgc_touch ( /*_.LIST___V46*/ meltfptr[45]);
	    ;
	    /*_.LIST___V44*/ meltfptr[40] = /*_.LIST___V46*/ meltfptr[45];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-normatch.melt:2313:/ clear");
	      /*clear *//*_.CINTBIND__V45*/ meltfptr[44] = 0;
	    /*^clear */
	      /*clear *//*_.LIST___V46*/ meltfptr[45] = 0;
	    /*^clear */
	      /*clear *//*_.CINTBIND__V45*/ meltfptr[44] = 0;
	    /*^clear */
	      /*clear *//*_.LIST___V46*/ meltfptr[45] = 0;
	  }			/*end multiallocblock */
	  ;
	  /*^compute */
	  /*_.NBINDMATX__V38*/ meltfptr[37] = /*_.SETQ___V47*/ meltfptr[44] =
	    /*_.LIST___V44*/ meltfptr[40];;
	  /*_.IFELSE___V43*/ meltfptr[39] = /*_.SETQ___V47*/ meltfptr[44];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-normatch.melt:2311:/ clear");
	     /*clear *//*_.LIST___V44*/ meltfptr[40] = 0;
	  /*^clear */
	     /*clear *//*_.SETQ___V47*/ meltfptr[44] = 0;
	}
	;
      }
    ;
    MELT_LOCATION ("warmelt-normatch.melt:2315:/ quasiblock");


    /*^checksignal */
    MELT_CHECK_SIGNAL ();
    ;
    /*^msend */
    /*msend */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^ojbmsend.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.ENV__V3*/ meltfptr[2];
      /*_.CTYP__V49*/ meltfptr[40] =
	meltgc_send ((melt_ptr_t) ( /*_.NMATX__V37*/ meltfptr[33]),
		     (melt_ptr_t) (( /*!GET_CTYPE */ meltfrout->tabval[13])),
		     (MELTBPARSTR_PTR ""), argtab, "",
		     (union meltparam_un *) 0);
    }
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-normatch.melt:2317:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L14*/ meltfnum[9] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-normatch.melt:2317:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L14*/ meltfnum[9])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L15*/ meltfnum[14] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-normatch.melt:2317:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long =
		/*_#THE_MELTCALLCOUNT__L15*/ meltfnum[14];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-normatch.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 2317;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "normexp_match ctyp";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.CTYP__V49*/ meltfptr[40];
	      /*_.MELT_DEBUG_FUN__V52*/ meltfptr[51] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[3])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V51*/ meltfptr[50] =
	      /*_.MELT_DEBUG_FUN__V52*/ meltfptr[51];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-normatch.melt:2317:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L15*/ meltfnum[14] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V52*/ meltfptr[51] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V51*/ meltfptr[50] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-normatch.melt:2317:/ quasiblock");


      /*_.PROGN___V53*/ meltfptr[51] = /*_.IF___V51*/ meltfptr[50];;
      /*^compute */
      /*_.IFCPP___V50*/ meltfptr[44] = /*_.PROGN___V53*/ meltfptr[51];;
      /*epilog */

      MELT_LOCATION ("warmelt-normatch.melt:2317:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L14*/ meltfnum[9] = 0;
      /*^clear */
	     /*clear *//*_.IF___V51*/ meltfptr[50] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V53*/ meltfptr[51] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V50*/ meltfptr[44] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-normatch.melt:2320:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#IS_OBJECT__L16*/ meltfnum[14] =
      (melt_magic_discr ((melt_ptr_t) ( /*_.NMATX__V37*/ meltfptr[33])) ==
       MELTOBMAG_OBJECT);;
    /*^compute */
 /*_#NOT__L17*/ meltfnum[9] =
      (!( /*_#IS_OBJECT__L16*/ meltfnum[14]));;
    MELT_LOCATION ("warmelt-normatch.melt:2320:/ cond");
    /*cond */ if ( /*_#NOT__L17*/ meltfnum[9])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-normatch.melt:2321:/ quasiblock");


	  MELT_LOCATION ("warmelt-normatch.melt:2322:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^apply */
	  /*apply */
	  {
	    /*_.CSYM__V56*/ meltfptr[55] =
	      melt_apply ((meltclosure_ptr_t)
			  (( /*!CLONE_SYMBOL */ meltfrout->tabval[7])),
			  (melt_ptr_t) (( /*!konst_14_MATCHED_ */ meltfrout->
					 tabval[14])), (""),
			  (union meltparam_un *) 0, "",
			  (union meltparam_un *) 0);
	  }
	  ;
	  MELT_LOCATION ("warmelt-normatch.melt:2323:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^quasiblock */


	  /*^rawallocobj */
	  /*rawallocobj */
	  {
	    melt_ptr_t newobj = 0;
	    melt_raw_object_create (newobj,
				    (melt_ptr_t) (( /*!CLASS_NORMAL_LET_BINDING */ meltfrout->tabval[10])), (4), "CLASS_NORMAL_LET_BINDING");
    /*_.INST__V58*/ meltfptr[57] =
	      newobj;
	  };
	  ;
	  /*^putslot */
	  /*putslot */
	  melt_assertmsg ("putslot checkobj @LETBIND_LOC",
			  melt_magic_discr ((melt_ptr_t)
					    ( /*_.INST__V58*/ meltfptr[57]))
			  == MELTOBMAG_OBJECT);
	  melt_putfield_object (( /*_.INST__V58*/ meltfptr[57]), (3),
				( /*_.SLOC__V17*/ meltfptr[13]),
				"LETBIND_LOC");
	  ;
	  /*^putslot */
	  /*putslot */
	  melt_assertmsg ("putslot checkobj @BINDER",
			  melt_magic_discr ((melt_ptr_t)
					    ( /*_.INST__V58*/ meltfptr[57]))
			  == MELTOBMAG_OBJECT);
	  melt_putfield_object (( /*_.INST__V58*/ meltfptr[57]), (0),
				( /*_.CSYM__V56*/ meltfptr[55]), "BINDER");
	  ;
	  /*^putslot */
	  /*putslot */
	  melt_assertmsg ("putslot checkobj @LETBIND_TYPE",
			  melt_magic_discr ((melt_ptr_t)
					    ( /*_.INST__V58*/ meltfptr[57]))
			  == MELTOBMAG_OBJECT);
	  melt_putfield_object (( /*_.INST__V58*/ meltfptr[57]), (1),
				( /*_.CTYP__V49*/ meltfptr[40]),
				"LETBIND_TYPE");
	  ;
	  /*^putslot */
	  /*putslot */
	  melt_assertmsg ("putslot checkobj @LETBIND_EXPR",
			  melt_magic_discr ((melt_ptr_t)
					    ( /*_.INST__V58*/ meltfptr[57]))
			  == MELTOBMAG_OBJECT);
	  melt_putfield_object (( /*_.INST__V58*/ meltfptr[57]), (2),
				( /*_.NMATX__V37*/ meltfptr[33]),
				"LETBIND_EXPR");
	  ;
	  /*^touchobj */

	  melt_dbgtrace_written_object ( /*_.INST__V58*/ meltfptr[57],
					"newly made instance");
	  ;
	  /*_.CBIND__V57*/ meltfptr[56] = /*_.INST__V58*/ meltfptr[57];;
	  MELT_LOCATION ("warmelt-normatch.melt:2328:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^quasiblock */


	  /*^rawallocobj */
	  /*rawallocobj */
	  {
	    melt_ptr_t newobj = 0;
	    melt_raw_object_create (newobj,
				    (melt_ptr_t) (( /*!CLASS_NREP_LOCSYMOCC */
						   meltfrout->tabval[15])),
				    (4), "CLASS_NREP_LOCSYMOCC");
    /*_.INST__V60*/ meltfptr[59] =
	      newobj;
	  };
	  ;
	  /*^putslot */
	  /*putslot */
	  melt_assertmsg ("putslot checkobj @NREP_LOC",
			  melt_magic_discr ((melt_ptr_t)
					    ( /*_.INST__V60*/ meltfptr[59]))
			  == MELTOBMAG_OBJECT);
	  melt_putfield_object (( /*_.INST__V60*/ meltfptr[59]), (0),
				( /*_.SLOC__V17*/ meltfptr[13]), "NREP_LOC");
	  ;
	  /*^putslot */
	  /*putslot */
	  melt_assertmsg ("putslot checkobj @NOCC_CTYP",
			  melt_magic_discr ((melt_ptr_t)
					    ( /*_.INST__V60*/ meltfptr[59]))
			  == MELTOBMAG_OBJECT);
	  melt_putfield_object (( /*_.INST__V60*/ meltfptr[59]), (2),
				( /*_.CTYP__V49*/ meltfptr[40]), "NOCC_CTYP");
	  ;
	  /*^putslot */
	  /*putslot */
	  melt_assertmsg ("putslot checkobj @NOCC_SYMB",
			  melt_magic_discr ((melt_ptr_t)
					    ( /*_.INST__V60*/ meltfptr[59]))
			  == MELTOBMAG_OBJECT);
	  melt_putfield_object (( /*_.INST__V60*/ meltfptr[59]), (1),
				( /*_.CSYM__V56*/ meltfptr[55]), "NOCC_SYMB");
	  ;
	  /*^putslot */
	  /*putslot */
	  melt_assertmsg ("putslot checkobj @NOCC_BIND",
			  melt_magic_discr ((melt_ptr_t)
					    ( /*_.INST__V60*/ meltfptr[59]))
			  == MELTOBMAG_OBJECT);
	  melt_putfield_object (( /*_.INST__V60*/ meltfptr[59]), (3),
				( /*_.CBIND__V57*/ meltfptr[56]),
				"NOCC_BIND");
	  ;
	  /*^touchobj */

	  melt_dbgtrace_written_object ( /*_.INST__V60*/ meltfptr[59],
					"newly made instance");
	  ;
	  /*_.CLOCC__V59*/ meltfptr[58] = /*_.INST__V60*/ meltfptr[59];;

#if MELT_HAVE_DEBUG
	  MELT_LOCATION ("warmelt-normatch.melt:2334:/ cppif.then");
	  /*^block */
	  /*anyblock */
	  {

	    /*^checksignal */
	    MELT_CHECK_SIGNAL ();
	    ;
     /*_#NULL__L18*/ meltfnum[17] =
	      (( /*_.NBINDMATX__V38*/ meltfptr[37]) == NULL);;
	    MELT_LOCATION ("warmelt-normatch.melt:2334:/ cond");
	    /*cond */ if ( /*_#NULL__L18*/ meltfnum[17])	/*then */
	      {
		/*^cond.then */
		/*_.IFELSE___V62*/ meltfptr[61] = ( /*nil */ NULL);;
	      }
	    else
	      {
		MELT_LOCATION ("warmelt-normatch.melt:2334:/ cond.else");

		/*^block */
		/*anyblock */
		{




		  {
		    /*^locexp */
		    melt_assert_failed (("check no binding"),
					("warmelt-normatch.melt")
					? ("warmelt-normatch.melt") :
					__FILE__, (2334) ? (2334) : __LINE__,
					__FUNCTION__);
		    ;
		  }
		  ;
		 /*clear *//*_.IFELSE___V62*/ meltfptr[61] = 0;
		  /*epilog */
		}
		;
	      }
	    ;
	    /*^compute */
	    /*_.IFCPP___V61*/ meltfptr[60] = /*_.IFELSE___V62*/ meltfptr[61];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-normatch.melt:2334:/ clear");
	       /*clear *//*_#NULL__L18*/ meltfnum[17] = 0;
	    /*^clear */
	       /*clear *//*_.IFELSE___V62*/ meltfptr[61] = 0;
	  }

#else /*MELT_HAVE_DEBUG */
	  /*^cppif.else */
	  /*_.IFCPP___V61*/ meltfptr[60] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
	  ;

#if MELT_HAVE_DEBUG
	  MELT_LOCATION ("warmelt-normatch.melt:2335:/ cppif.then");
	  /*^block */
	  /*anyblock */
	  {


	    {
	      /*^locexp */
	      /*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	      melt_dbgcounter++;
#endif
	      ;
	    }
	    ;
	    /*^checksignal */
	    MELT_CHECK_SIGNAL ();
	    ;
     /*_#MELT_NEED_DBG__L19*/ meltfnum[17] =
	      /*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	      ( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	      0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	      ;;
	    MELT_LOCATION ("warmelt-normatch.melt:2335:/ cond");
	    /*cond */ if ( /*_#MELT_NEED_DBG__L19*/ meltfnum[17])	/*then */
	      {
		/*^cond.then */
		/*^block */
		/*anyblock */
		{

       /*_#THE_MELTCALLCOUNT__L20*/ meltfnum[19] =
#ifdef meltcallcount
		    meltcallcount	/* the_meltcallcount */
#else
		    0L
#endif /* meltcallcount the_meltcallcount */
		    ;;
		  MELT_LOCATION ("warmelt-normatch.melt:2335:/ checksignal");
		  MELT_CHECK_SIGNAL ();
		  ;
		  /*^apply */
		  /*apply */
		  {
		    union meltparam_un argtab[5];
		    memset (&argtab, 0, sizeof (argtab));
		    /*^apply.arg */
		    argtab[0].meltbp_long =
		      /*_#THE_MELTCALLCOUNT__L20*/ meltfnum[19];
		    /*^apply.arg */
		    argtab[1].meltbp_cstring = "warmelt-normatch.melt";
		    /*^apply.arg */
		    argtab[2].meltbp_long = 2335;
		    /*^apply.arg */
		    argtab[3].meltbp_cstring = "normexp_match clocc=";
		    /*^apply.arg */
		    argtab[4].meltbp_aptr =
		      (melt_ptr_t *) & /*_.CLOCC__V59*/ meltfptr[58];
		    /*_.MELT_DEBUG_FUN__V65*/ meltfptr[64] =
		      melt_apply ((meltclosure_ptr_t)
				  (( /*!MELT_DEBUG_FUN */ meltfrout->
				    tabval[3])),
				  (melt_ptr_t) (( /*nil */ NULL)),
				  (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_PTR ""), argtab, "",
				  (union meltparam_un *) 0);
		  }
		  ;
		  /*_.IF___V64*/ meltfptr[63] =
		    /*_.MELT_DEBUG_FUN__V65*/ meltfptr[64];;
		  /*epilog */

		  MELT_LOCATION ("warmelt-normatch.melt:2335:/ clear");
		 /*clear *//*_#THE_MELTCALLCOUNT__L20*/ meltfnum[19] = 0;
		  /*^clear */
		 /*clear *//*_.MELT_DEBUG_FUN__V65*/ meltfptr[64] = 0;
		}
		;
	      }
	    else
	      {			/*^cond.else */

      /*_.IF___V64*/ meltfptr[63] = NULL;;
	      }
	    ;
	    MELT_LOCATION ("warmelt-normatch.melt:2335:/ quasiblock");


	    /*_.PROGN___V66*/ meltfptr[64] = /*_.IF___V64*/ meltfptr[63];;
	    /*^compute */
	    /*_.IFCPP___V63*/ meltfptr[61] = /*_.PROGN___V66*/ meltfptr[64];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-normatch.melt:2335:/ clear");
	       /*clear *//*_#MELT_NEED_DBG__L19*/ meltfnum[17] = 0;
	    /*^clear */
	       /*clear *//*_.IF___V64*/ meltfptr[63] = 0;
	    /*^clear */
	       /*clear *//*_.PROGN___V66*/ meltfptr[64] = 0;
	  }

#else /*MELT_HAVE_DEBUG */
	  /*^cppif.else */
	  /*_.IFCPP___V63*/ meltfptr[61] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
	  ;
	  MELT_LOCATION ("warmelt-normatch.melt:2337:/ cond");
	  /*cond */ if (
			 /*ifisa */
			 melt_is_instance_of ((melt_ptr_t)
					      ( /*_.NCX__V4*/ meltfptr[3]),
					      (melt_ptr_t) (( /*!CLASS_NORMALIZATION_CONTEXT */ meltfrout->tabval[2])))
	    )			/*then */
	    {
	      /*^cond.then */
	      /*^getslot */
	      {
		melt_ptr_t slot = NULL, obj = NULL;
		obj = (melt_ptr_t) ( /*_.NCX__V4*/ meltfptr[3]) /*=obj*/ ;
		melt_object_get_field (slot, obj, 6, "NCTX_SYMBCACHEMAP");
     /*_.NCTX_SYMBCACHEMAP__V67*/ meltfptr[63] = slot;
	      };
	      ;
	    }
	  else
	    {			/*^cond.else */

    /*_.NCTX_SYMBCACHEMAP__V67*/ meltfptr[63] = NULL;;
	    }
	  ;

	  {
	    MELT_LOCATION ("warmelt-normatch.melt:2337:/ locexp");
	    meltgc_put_mapobjects ((meltmapobjects_ptr_t)
				   ( /*_.NCTX_SYMBCACHEMAP__V67*/
				    meltfptr[63]),
				   (meltobject_ptr_t) ( /*_.CSYM__V56*/
						       meltfptr[55]),
				   (melt_ptr_t) ( /*_.CLOCC__V59*/
						 meltfptr[58]));
	  }
	  ;
	  MELT_LOCATION ("warmelt-normatch.melt:2338:/ blockmultialloc");
	  /*multiallocblock */
	  {
	    struct meltletrec_2_st
	    {
	      struct meltpair_st rpair_0__CBIND_x1;
	      struct meltlist_st rlist_1__LIST_;
	      long meltletrec_2_endgap;
	    } *meltletrec_2_ptr = 0;
	    meltletrec_2_ptr =
	      (struct meltletrec_2_st *)
	      meltgc_allocate (sizeof (struct meltletrec_2_st), 0);
	    /*^blockmultialloc.initfill */
	    /*inipair rpair_0__CBIND_x1 */
     /*_.CBIND__V69*/ meltfptr[68] =
	      (melt_ptr_t) & meltletrec_2_ptr->rpair_0__CBIND_x1;
	    meltletrec_2_ptr->rpair_0__CBIND_x1.discr =
	      (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (DISCR_PAIR))));

	    /*inilist rlist_1__LIST_ */
     /*_.LIST___V70*/ meltfptr[69] =
	      (melt_ptr_t) & meltletrec_2_ptr->rlist_1__LIST_;
	    meltletrec_2_ptr->rlist_1__LIST_.discr =
	      (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (DISCR_LIST))));



	    /*^putpairhead */
	    /*putpairhead */
	    melt_assertmsg ("putpairhead /2 checkpair",
			    melt_magic_discr ((melt_ptr_t)
					      ( /*_.CBIND__V69*/
					       meltfptr[68])) ==
			    MELTOBMAG_PAIR);
	    ((meltpair_ptr_t) ( /*_.CBIND__V69*/ meltfptr[68]))->hd =
	      (melt_ptr_t) ( /*_.CBIND__V57*/ meltfptr[56]);
	    ;
	    /*^touch */
	    meltgc_touch ( /*_.CBIND__V69*/ meltfptr[68]);
	    ;
	    /*^putlist */
	    /*putlist */
	    melt_assertmsg ("putlist checklist",
			    melt_magic_discr ((melt_ptr_t)
					      ( /*_.LIST___V70*/
					       meltfptr[69])) ==
			    MELTOBMAG_LIST);
	    ((meltlist_ptr_t) ( /*_.LIST___V70*/ meltfptr[69]))->first =
	      (meltpair_ptr_t) ( /*_.CBIND__V69*/ meltfptr[68]);
	    ((meltlist_ptr_t) ( /*_.LIST___V70*/ meltfptr[69]))->last =
	      (meltpair_ptr_t) ( /*_.CBIND__V69*/ meltfptr[68]);
	    ;
	    /*^touch */
	    meltgc_touch ( /*_.LIST___V70*/ meltfptr[69]);
	    ;
	    /*_.LIST___V68*/ meltfptr[64] = /*_.LIST___V70*/ meltfptr[69];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-normatch.melt:2338:/ clear");
	      /*clear *//*_.CBIND__V69*/ meltfptr[68] = 0;
	    /*^clear */
	      /*clear *//*_.LIST___V70*/ meltfptr[69] = 0;
	    /*^clear */
	      /*clear *//*_.CBIND__V69*/ meltfptr[68] = 0;
	    /*^clear */
	      /*clear *//*_.LIST___V70*/ meltfptr[69] = 0;
	  }			/*end multiallocblock */
	  ;
	  /*^compute */
	  /*_.NBINDMATX__V38*/ meltfptr[37] = /*_.SETQ___V71*/ meltfptr[68] =
	    /*_.LIST___V68*/ meltfptr[64];;
	  MELT_LOCATION ("warmelt-normatch.melt:2339:/ compute");
	  /*_.NMATX__V37*/ meltfptr[33] = /*_.SETQ___V72*/ meltfptr[69] =
	    /*_.CLOCC__V59*/ meltfptr[58];;
	  /*_.LET___V55*/ meltfptr[51] = /*_.SETQ___V72*/ meltfptr[69];;

	  MELT_LOCATION ("warmelt-normatch.melt:2321:/ clear");
	     /*clear *//*_.CSYM__V56*/ meltfptr[55] = 0;
	  /*^clear */
	     /*clear *//*_.CBIND__V57*/ meltfptr[56] = 0;
	  /*^clear */
	     /*clear *//*_.CLOCC__V59*/ meltfptr[58] = 0;
	  /*^clear */
	     /*clear *//*_.IFCPP___V61*/ meltfptr[60] = 0;
	  /*^clear */
	     /*clear *//*_.IFCPP___V63*/ meltfptr[61] = 0;
	  /*^clear */
	     /*clear *//*_.NCTX_SYMBCACHEMAP__V67*/ meltfptr[63] = 0;
	  /*^clear */
	     /*clear *//*_.LIST___V68*/ meltfptr[64] = 0;
	  /*^clear */
	     /*clear *//*_.SETQ___V71*/ meltfptr[68] = 0;
	  /*^clear */
	     /*clear *//*_.SETQ___V72*/ meltfptr[69] = 0;
	  /*_.IF___V54*/ meltfptr[50] = /*_.LET___V55*/ meltfptr[51];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-normatch.melt:2320:/ clear");
	     /*clear *//*_.LET___V55*/ meltfptr[51] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

  /*_.IF___V54*/ meltfptr[50] = NULL;;
      }
    ;
    MELT_LOCATION ("warmelt-normatch.melt:2342:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#NULL__L21*/ meltfnum[19] =
      (( /*_.NBINDMATX__V38*/ meltfptr[37]) == NULL);;
    MELT_LOCATION ("warmelt-normatch.melt:2342:/ cond");
    /*cond */ if ( /*_#NULL__L21*/ meltfnum[19])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

   /*_.MAKE_LIST__V74*/ meltfptr[56] =
	    (meltgc_new_list
	     ((meltobject_ptr_t)
	      (( /*!DISCR_LIST */ meltfrout->tabval[6]))));;
	  MELT_LOCATION ("warmelt-normatch.melt:2342:/ compute");
	  /*_.NBINDMATX__V38*/ meltfptr[37] = /*_.SETQ___V75*/ meltfptr[58] =
	    /*_.MAKE_LIST__V74*/ meltfptr[56];;
	  /*_.IF___V73*/ meltfptr[55] = /*_.SETQ___V75*/ meltfptr[58];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-normatch.melt:2342:/ clear");
	     /*clear *//*_.MAKE_LIST__V74*/ meltfptr[56] = 0;
	  /*^clear */
	     /*clear *//*_.SETQ___V75*/ meltfptr[58] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

  /*_.IF___V73*/ meltfptr[55] = NULL;;
      }
    ;
    /*citerblock FOREACH_IN_MULTIPLE */
    {
      /* start foreach_in_multiple meltcit1__EACHTUP */
      long meltcit1__EACHTUP_ln =
	melt_multiple_length ((melt_ptr_t) /*_.SCASES__V19*/ meltfptr[18]);
      for ( /*_#IX__L22*/ meltfnum[17] = 0;
	   ( /*_#IX__L22*/ meltfnum[17] >= 0)
	   && ( /*_#IX__L22*/ meltfnum[17] < meltcit1__EACHTUP_ln);
	/*_#IX__L22*/ meltfnum[17]++)
	{
	  /*_.CURCAS__V76*/ meltfptr[60] =
	    melt_multiple_nth ((melt_ptr_t) ( /*_.SCASES__V19*/ meltfptr[18]),
			       /*_#IX__L22*/ meltfnum[17]);




#if MELT_HAVE_DEBUG
	  MELT_LOCATION ("warmelt-normatch.melt:2348:/ cppif.then");
	  /*^block */
	  /*anyblock */
	  {


	    {
	      /*^locexp */
	      /*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	      melt_dbgcounter++;
#endif
	      ;
	    }
	    ;
	    /*^checksignal */
	    MELT_CHECK_SIGNAL ();
	    ;
    /*_#MELT_NEED_DBG__L23*/ meltfnum[22] =
	      /*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	      ( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	      0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	      ;;
	    MELT_LOCATION ("warmelt-normatch.melt:2348:/ cond");
	    /*cond */ if ( /*_#MELT_NEED_DBG__L23*/ meltfnum[22])	/*then */
	      {
		/*^cond.then */
		/*^block */
		/*anyblock */
		{

      /*_#THE_MELTCALLCOUNT__L24*/ meltfnum[23] =
#ifdef meltcallcount
		    meltcallcount	/* the_meltcallcount */
#else
		    0L
#endif /* meltcallcount the_meltcallcount */
		    ;;
		  MELT_LOCATION ("warmelt-normatch.melt:2348:/ checksignal");
		  MELT_CHECK_SIGNAL ();
		  ;
		  /*^apply */
		  /*apply */
		  {
		    union meltparam_un argtab[5];
		    memset (&argtab, 0, sizeof (argtab));
		    /*^apply.arg */
		    argtab[0].meltbp_long =
		      /*_#THE_MELTCALLCOUNT__L24*/ meltfnum[23];
		    /*^apply.arg */
		    argtab[1].meltbp_cstring = "warmelt-normatch.melt";
		    /*^apply.arg */
		    argtab[2].meltbp_long = 2348;
		    /*^apply.arg */
		    argtab[3].meltbp_cstring = "normexp_match curcas";
		    /*^apply.arg */
		    argtab[4].meltbp_aptr =
		      (melt_ptr_t *) & /*_.CURCAS__V76*/ meltfptr[60];
		    /*_.MELT_DEBUG_FUN__V79*/ meltfptr[64] =
		      melt_apply ((meltclosure_ptr_t)
				  (( /*!MELT_DEBUG_FUN */ meltfrout->
				    tabval[3])),
				  (melt_ptr_t) (( /*nil */ NULL)),
				  (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_PTR ""), argtab, "",
				  (union meltparam_un *) 0);
		  }
		  ;
		  /*_.IF___V78*/ meltfptr[63] =
		    /*_.MELT_DEBUG_FUN__V79*/ meltfptr[64];;
		  /*epilog */

		  MELT_LOCATION ("warmelt-normatch.melt:2348:/ clear");
		/*clear *//*_#THE_MELTCALLCOUNT__L24*/ meltfnum[23] = 0;
		  /*^clear */
		/*clear *//*_.MELT_DEBUG_FUN__V79*/ meltfptr[64] = 0;
		}
		;
	      }
	    else
	      {			/*^cond.else */

     /*_.IF___V78*/ meltfptr[63] = NULL;;
	      }
	    ;
	    MELT_LOCATION ("warmelt-normatch.melt:2348:/ quasiblock");


	    /*_.PROGN___V80*/ meltfptr[68] = /*_.IF___V78*/ meltfptr[63];;
	    /*^compute */
	    /*_.IFCPP___V77*/ meltfptr[61] = /*_.PROGN___V80*/ meltfptr[68];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-normatch.melt:2348:/ clear");
	      /*clear *//*_#MELT_NEED_DBG__L23*/ meltfnum[22] = 0;
	    /*^clear */
	      /*clear *//*_.IF___V78*/ meltfptr[63] = 0;
	    /*^clear */
	      /*clear *//*_.PROGN___V80*/ meltfptr[68] = 0;
	  }

#else /*MELT_HAVE_DEBUG */
	  /*^cppif.else */
	  /*_.IFCPP___V77*/ meltfptr[61] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
	  ;

#if MELT_HAVE_DEBUG
	  MELT_LOCATION ("warmelt-normatch.melt:2349:/ cppif.then");
	  /*^block */
	  /*anyblock */
	  {

	    /*^checksignal */
	    MELT_CHECK_SIGNAL ();
	    ;
    /*_#IS_A__L25*/ meltfnum[23] =
	      melt_is_instance_of ((melt_ptr_t)
				   ( /*_.CURCAS__V76*/ meltfptr[60]),
				   (melt_ptr_t) (( /*!CLASS_SOURCE_MATCH_CASE */ meltfrout->tabval[16])));;
	    MELT_LOCATION ("warmelt-normatch.melt:2349:/ cond");
	    /*cond */ if ( /*_#IS_A__L25*/ meltfnum[23])	/*then */
	      {
		/*^cond.then */
		/*_.IFELSE___V82*/ meltfptr[51] = ( /*nil */ NULL);;
	      }
	    else
	      {
		MELT_LOCATION ("warmelt-normatch.melt:2349:/ cond.else");

		/*^block */
		/*anyblock */
		{




		  {
		    /*^locexp */
		    melt_assert_failed (("check curcas"),
					("warmelt-normatch.melt")
					? ("warmelt-normatch.melt") :
					__FILE__, (2349) ? (2349) : __LINE__,
					__FUNCTION__);
		    ;
		  }
		  ;
		/*clear *//*_.IFELSE___V82*/ meltfptr[51] = 0;
		  /*epilog */
		}
		;
	      }
	    ;
	    /*^compute */
	    /*_.IFCPP___V81*/ meltfptr[69] = /*_.IFELSE___V82*/ meltfptr[51];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-normatch.melt:2349:/ clear");
	      /*clear *//*_#IS_A__L25*/ meltfnum[23] = 0;
	    /*^clear */
	      /*clear *//*_.IFELSE___V82*/ meltfptr[51] = 0;
	  }

#else /*MELT_HAVE_DEBUG */
	  /*^cppif.else */
	  /*_.IFCPP___V81*/ meltfptr[69] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
	  ;
	  MELT_LOCATION ("warmelt-normatch.melt:2350:/ quasiblock");


	  MELT_LOCATION ("warmelt-normatch.melt:2351:/ getslot");
	  {
	    melt_ptr_t slot = NULL, obj = NULL;
	    obj = (melt_ptr_t) ( /*_.CURCAS__V76*/ meltfptr[60]) /*=obj*/ ;
	    melt_object_get_field (slot, obj, 1, "LOCA_LOCATION");
   /*_.CURLOC__V84*/ meltfptr[58] = slot;
	  };
	  ;
	  MELT_LOCATION ("warmelt-normatch.melt:2352:/ getslot");
	  {
	    melt_ptr_t slot = NULL, obj = NULL;
	    obj = (melt_ptr_t) ( /*_.CURCAS__V76*/ meltfptr[60]) /*=obj*/ ;
	    melt_object_get_field (slot, obj, 2, "SCAM_PATT");
   /*_.CURPAT__V85*/ meltfptr[64] = slot;
	  };
	  ;
	  MELT_LOCATION ("warmelt-normatch.melt:2353:/ getslot");
	  {
	    melt_ptr_t slot = NULL, obj = NULL;
	    obj = (melt_ptr_t) ( /*_.CURCAS__V76*/ meltfptr[60]) /*=obj*/ ;
	    melt_object_get_field (slot, obj, 3, "SCAM_BODY");
   /*_.CURBODY__V86*/ meltfptr[63] = slot;
	  };
	  ;
  /*_.MAPVAR__V87*/ meltfptr[68] =
	    (meltgc_new_mapobjects
	     ((meltobject_ptr_t)
	      (( /*!DISCR_MAP_OBJECTS */ meltfrout->tabval[5])), (13)));;
	  /*^compute */
  /*_.MAPCST__V88*/ meltfptr[51] =
	    (meltgc_new_mapobjects
	     ((meltobject_ptr_t)
	      (( /*!DISCR_MAP_OBJECTS */ meltfrout->tabval[5])), (11)));;
	  /*^compute */
  /*_.MAPOR__V89*/ meltfptr[88] =
	    (meltgc_new_mapobjects
	     ((meltobject_ptr_t)
	      (( /*!DISCR_MAP_OBJECTS */ meltfrout->tabval[5])), (7)));;
	  /*^compute */
  /*_.PVARLOCMAP__V90*/ meltfptr[89] =
	    (meltgc_new_mapobjects
	     ((meltobject_ptr_t)
	      (( /*!DISCR_MAP_OBJECTS */ meltfrout->tabval[5])), (23)));;
	  /*^compute */
  /*_.VARHDLERLIST__V91*/ meltfptr[90] =
	    (meltgc_new_list
	     ((meltobject_ptr_t)
	      (( /*!DISCR_LIST */ meltfrout->tabval[6]))));;
	  MELT_LOCATION ("warmelt-normatch.melt:2359:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^quasiblock */


	  /*^rawallocobj */
	  /*rawallocobj */
	  {
	    melt_ptr_t newobj = 0;
	    melt_raw_object_create (newobj,
				    (melt_ptr_t) (( /*!CLASS_PATTERN_CONTEXT */ meltfrout->tabval[17])), (11), "CLASS_PATTERN_CONTEXT");
   /*_.INST__V93*/ meltfptr[92] =
	      newobj;
	  };
	  ;
	  /*^putslot */
	  /*putslot */
	  melt_assertmsg ("putslot checkobj @PCTN_NORMCTXT",
			  melt_magic_discr ((melt_ptr_t)
					    ( /*_.INST__V93*/ meltfptr[92]))
			  == MELTOBMAG_OBJECT);
	  melt_putfield_object (( /*_.INST__V93*/ meltfptr[92]), (0),
				( /*_.NCX__V4*/ meltfptr[3]),
				"PCTN_NORMCTXT");
	  ;
	  /*^putslot */
	  /*putslot */
	  melt_assertmsg ("putslot checkobj @PCTN_SRC",
			  melt_magic_discr ((melt_ptr_t)
					    ( /*_.INST__V93*/ meltfptr[92]))
			  == MELTOBMAG_OBJECT);
	  melt_putfield_object (( /*_.INST__V93*/ meltfptr[92]), (1),
				( /*_.RECV__V2*/ meltfptr[1]), "PCTN_SRC");
	  ;
	  /*^putslot */
	  /*putslot */
	  melt_assertmsg ("putslot checkobj @PCTN_ENV",
			  melt_magic_discr ((melt_ptr_t)
					    ( /*_.INST__V93*/ meltfptr[92]))
			  == MELTOBMAG_OBJECT);
	  melt_putfield_object (( /*_.INST__V93*/ meltfptr[92]), (2),
				( /*_.ENV__V3*/ meltfptr[2]), "PCTN_ENV");
	  ;
	  /*^putslot */
	  /*putslot */
	  melt_assertmsg ("putslot checkobj @PCTN_MAPATVAR",
			  melt_magic_discr ((melt_ptr_t)
					    ( /*_.INST__V93*/ meltfptr[92]))
			  == MELTOBMAG_OBJECT);
	  melt_putfield_object (( /*_.INST__V93*/ meltfptr[92]), (3),
				( /*_.MAPVAR__V87*/ meltfptr[68]),
				"PCTN_MAPATVAR");
	  ;
	  /*^putslot */
	  /*putslot */
	  melt_assertmsg ("putslot checkobj @PCTN_MAPATCST",
			  melt_magic_discr ((melt_ptr_t)
					    ( /*_.INST__V93*/ meltfptr[92]))
			  == MELTOBMAG_OBJECT);
	  melt_putfield_object (( /*_.INST__V93*/ meltfptr[92]), (4),
				( /*_.MAPCST__V88*/ meltfptr[51]),
				"PCTN_MAPATCST");
	  ;
	  /*^putslot */
	  /*putslot */
	  melt_assertmsg ("putslot checkobj @PCTN_MAPOR",
			  melt_magic_discr ((melt_ptr_t)
					    ( /*_.INST__V93*/ meltfptr[92]))
			  == MELTOBMAG_OBJECT);
	  melt_putfield_object (( /*_.INST__V93*/ meltfptr[92]), (5),
				( /*_.MAPOR__V89*/ meltfptr[88]),
				"PCTN_MAPOR");
	  ;
	  /*^putslot */
	  /*putslot */
	  melt_assertmsg ("putslot checkobj @PCTN_BINDLIST",
			  melt_magic_discr ((melt_ptr_t)
					    ( /*_.INST__V93*/ meltfptr[92]))
			  == MELTOBMAG_OBJECT);
	  melt_putfield_object (( /*_.INST__V93*/ meltfptr[92]), (6),
				( /*_.SHABINDLIST__V23*/ meltfptr[22]),
				"PCTN_BINDLIST");
	  ;
	  /*^putslot */
	  /*putslot */
	  melt_assertmsg ("putslot checkobj @PCTN_STUFFMAP",
			  melt_magic_discr ((melt_ptr_t)
					    ( /*_.INST__V93*/ meltfptr[92]))
			  == MELTOBMAG_OBJECT);
	  melt_putfield_object (( /*_.INST__V93*/ meltfptr[92]), (7),
				( /*_.STUFFMAP__V22*/ meltfptr[21]),
				"PCTN_STUFFMAP");
	  ;
	  /*^putslot */
	  /*putslot */
	  melt_assertmsg ("putslot checkobj @PCTN_PVARLOCMAP",
			  melt_magic_discr ((melt_ptr_t)
					    ( /*_.INST__V93*/ meltfptr[92]))
			  == MELTOBMAG_OBJECT);
	  melt_putfield_object (( /*_.INST__V93*/ meltfptr[92]), (8),
				( /*_.PVARLOCMAP__V90*/ meltfptr[89]),
				"PCTN_PVARLOCMAP");
	  ;
	  /*^putslot */
	  /*putslot */
	  melt_assertmsg ("putslot checkobj @PCTN_TESTS",
			  melt_magic_discr ((melt_ptr_t)
					    ( /*_.INST__V93*/ meltfptr[92]))
			  == MELTOBMAG_OBJECT);
	  melt_putfield_object (( /*_.INST__V93*/ meltfptr[92]), (9),
				( /*_.TESTLIST__V24*/ meltfptr[23]),
				"PCTN_TESTS");
	  ;
	  /*^putslot */
	  /*putslot */
	  melt_assertmsg ("putslot checkobj @PCTN_VARHANDLERS",
			  melt_magic_discr ((melt_ptr_t)
					    ( /*_.INST__V93*/ meltfptr[92]))
			  == MELTOBMAG_OBJECT);
	  melt_putfield_object (( /*_.INST__V93*/ meltfptr[92]), (10),
				( /*_.VARHDLERLIST__V91*/ meltfptr[90]),
				"PCTN_VARHANDLERS");
	  ;
	  /*^touchobj */

	  melt_dbgtrace_written_object ( /*_.INST__V93*/ meltfptr[92],
					"newly made instance");
	  ;
	  /*_.PCN__V92*/ meltfptr[91] = /*_.INST__V93*/ meltfptr[92];;
	  MELT_LOCATION ("warmelt-normatch.melt:2372:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^quasiblock */


	  /*^rawallocobj */
	  /*rawallocobj */
	  {
	    melt_ptr_t newobj = 0;
	    melt_raw_object_create (newobj,
				    (melt_ptr_t) (( /*!CLASS_REFERENCE */
						   meltfrout->tabval[18])),
				    (1), "CLASS_REFERENCE");
   /*_.INST__V95*/ meltfptr[94] =
	      newobj;
	  };
	  ;
	  /*^touchobj */

	  melt_dbgtrace_written_object ( /*_.INST__V95*/ meltfptr[94],
					"newly made instance");
	  ;
	  /*_.NTESTCONT__V94*/ meltfptr[93] = /*_.INST__V95*/ meltfptr[94];;

#if MELT_HAVE_DEBUG
	  MELT_LOCATION ("warmelt-normatch.melt:2374:/ cppif.then");
	  /*^block */
	  /*anyblock */
	  {


	    {
	      /*^locexp */
	      /*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	      melt_dbgcounter++;
#endif
	      ;
	    }
	    ;
	    /*^checksignal */
	    MELT_CHECK_SIGNAL ();
	    ;
    /*_#MELT_NEED_DBG__L26*/ meltfnum[22] =
	      /*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	      ( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	      0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	      ;;
	    MELT_LOCATION ("warmelt-normatch.melt:2374:/ cond");
	    /*cond */ if ( /*_#MELT_NEED_DBG__L26*/ meltfnum[22])	/*then */
	      {
		/*^cond.then */
		/*^block */
		/*anyblock */
		{

      /*_#THE_MELTCALLCOUNT__L27*/ meltfnum[23] =
#ifdef meltcallcount
		    meltcallcount	/* the_meltcallcount */
#else
		    0L
#endif /* meltcallcount the_meltcallcount */
		    ;;
		  MELT_LOCATION ("warmelt-normatch.melt:2374:/ checksignal");
		  MELT_CHECK_SIGNAL ();
		  ;
		  /*^apply */
		  /*apply */
		  {
		    union meltparam_un argtab[5];
		    memset (&argtab, 0, sizeof (argtab));
		    /*^apply.arg */
		    argtab[0].meltbp_long =
		      /*_#THE_MELTCALLCOUNT__L27*/ meltfnum[23];
		    /*^apply.arg */
		    argtab[1].meltbp_cstring = "warmelt-normatch.melt";
		    /*^apply.arg */
		    argtab[2].meltbp_long = 2374;
		    /*^apply.arg */
		    argtab[3].meltbp_cstring =
		      "normexp_match curpat before scan_pattern";
		    /*^apply.arg */
		    argtab[4].meltbp_aptr =
		      (melt_ptr_t *) & /*_.CURPAT__V85*/ meltfptr[64];
		    /*_.MELT_DEBUG_FUN__V98*/ meltfptr[97] =
		      melt_apply ((meltclosure_ptr_t)
				  (( /*!MELT_DEBUG_FUN */ meltfrout->
				    tabval[3])),
				  (melt_ptr_t) (( /*nil */ NULL)),
				  (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_PTR ""), argtab, "",
				  (union meltparam_un *) 0);
		  }
		  ;
		  /*_.IF___V97*/ meltfptr[96] =
		    /*_.MELT_DEBUG_FUN__V98*/ meltfptr[97];;
		  /*epilog */

		  MELT_LOCATION ("warmelt-normatch.melt:2374:/ clear");
		/*clear *//*_#THE_MELTCALLCOUNT__L27*/ meltfnum[23] = 0;
		  /*^clear */
		/*clear *//*_.MELT_DEBUG_FUN__V98*/ meltfptr[97] = 0;
		}
		;
	      }
	    else
	      {			/*^cond.else */

     /*_.IF___V97*/ meltfptr[96] = NULL;;
	      }
	    ;
	    MELT_LOCATION ("warmelt-normatch.melt:2374:/ quasiblock");


	    /*_.PROGN___V99*/ meltfptr[97] = /*_.IF___V97*/ meltfptr[96];;
	    /*^compute */
	    /*_.IFCPP___V96*/ meltfptr[95] = /*_.PROGN___V99*/ meltfptr[97];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-normatch.melt:2374:/ clear");
	      /*clear *//*_#MELT_NEED_DBG__L26*/ meltfnum[22] = 0;
	    /*^clear */
	      /*clear *//*_.IF___V97*/ meltfptr[96] = 0;
	    /*^clear */
	      /*clear *//*_.PROGN___V99*/ meltfptr[97] = 0;
	  }

#else /*MELT_HAVE_DEBUG */
	  /*^cppif.else */
	  /*_.IFCPP___V96*/ meltfptr[95] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
	  ;

#if MELT_HAVE_DEBUG
	  MELT_LOCATION ("warmelt-normatch.melt:2377:/ cppif.then");
	  /*^block */
	  /*anyblock */
	  {

	    /*^checksignal */
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^cond */
	    /*cond */ if ( /*_.CURPAT__V85*/ meltfptr[64])	/*then */
	      {
		/*^cond.then */
		/*_.IFELSE___V101*/ meltfptr[97] = ( /*nil */ NULL);;
	      }
	    else
	      {
		MELT_LOCATION ("warmelt-normatch.melt:2377:/ cond.else");

		/*^block */
		/*anyblock */
		{




		  {
		    /*^locexp */
		    melt_assert_failed (("normexp_match check curpat"),
					("warmelt-normatch.melt")
					? ("warmelt-normatch.melt") :
					__FILE__, (2377) ? (2377) : __LINE__,
					__FUNCTION__);
		    ;
		  }
		  ;
		/*clear *//*_.IFELSE___V101*/ meltfptr[97] = 0;
		  /*epilog */
		}
		;
	      }
	    ;
	    /*^compute */
	    /*_.IFCPP___V100*/ meltfptr[96] =
	      /*_.IFELSE___V101*/ meltfptr[97];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-normatch.melt:2377:/ clear");
	      /*clear *//*_.IFELSE___V101*/ meltfptr[97] = 0;
	  }

#else /*MELT_HAVE_DEBUG */
	  /*^cppif.else */
	  /*_.IFCPP___V100*/ meltfptr[96] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
	  ;
	  MELT_LOCATION ("warmelt-normatch.melt:2378:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^msend */
	  /*msend */
	  {
	    union meltparam_un argtab[3];
	    memset (&argtab, 0, sizeof (argtab));
	    /*^ojbmsend.arg */
	    argtab[0].meltbp_aptr =
	      (melt_ptr_t *) & /*_.CURLOC__V84*/ meltfptr[58];
	    /*^ojbmsend.arg */
	    argtab[1].meltbp_aptr =
	      (melt_ptr_t *) & /*_.CTYP__V49*/ meltfptr[40];
	    /*^ojbmsend.arg */
	    argtab[2].meltbp_aptr =
	      (melt_ptr_t *) & /*_.PCN__V92*/ meltfptr[91];
	    /*_.SCAN_PATTERN__V102*/ meltfptr[97] =
	      meltgc_send ((melt_ptr_t) ( /*_.CURPAT__V85*/ meltfptr[64]),
			   (melt_ptr_t) (( /*!SCAN_PATTERN */ meltfrout->
					  tabval[19])),
			   (MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_PTR
			    ""), argtab, "", (union meltparam_un *) 0);
	  }
	  ;

#if MELT_HAVE_DEBUG
	  MELT_LOCATION ("warmelt-normatch.melt:2379:/ cppif.then");
	  /*^block */
	  /*anyblock */
	  {


	    {
	      /*^locexp */
	      /*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	      melt_dbgcounter++;
#endif
	      ;
	    }
	    ;
	    /*^checksignal */
	    MELT_CHECK_SIGNAL ();
	    ;
    /*_#MELT_NEED_DBG__L28*/ meltfnum[23] =
	      /*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	      ( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	      0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	      ;;
	    MELT_LOCATION ("warmelt-normatch.melt:2379:/ cond");
	    /*cond */ if ( /*_#MELT_NEED_DBG__L28*/ meltfnum[23])	/*then */
	      {
		/*^cond.then */
		/*^block */
		/*anyblock */
		{

      /*_#THE_MELTCALLCOUNT__L29*/ meltfnum[22] =
#ifdef meltcallcount
		    meltcallcount	/* the_meltcallcount */
#else
		    0L
#endif /* meltcallcount the_meltcallcount */
		    ;;
		  MELT_LOCATION ("warmelt-normatch.melt:2379:/ checksignal");
		  MELT_CHECK_SIGNAL ();
		  ;
		  /*^apply */
		  /*apply */
		  {
		    union meltparam_un argtab[11];
		    memset (&argtab, 0, sizeof (argtab));
		    /*^apply.arg */
		    argtab[0].meltbp_long =
		      /*_#THE_MELTCALLCOUNT__L29*/ meltfnum[22];
		    /*^apply.arg */
		    argtab[1].meltbp_cstring = "warmelt-normatch.melt";
		    /*^apply.arg */
		    argtab[2].meltbp_long = 2379;
		    /*^apply.arg */
		    argtab[3].meltbp_cstring =
		      "normexp_match  after scan_pattern curpat=";
		    /*^apply.arg */
		    argtab[4].meltbp_aptr =
		      (melt_ptr_t *) & /*_.CURPAT__V85*/ meltfptr[64];
		    /*^apply.arg */
		    argtab[5].meltbp_cstring = " mapvar=";
		    /*^apply.arg */
		    argtab[6].meltbp_aptr =
		      (melt_ptr_t *) & /*_.MAPVAR__V87*/ meltfptr[68];
		    /*^apply.arg */
		    argtab[7].meltbp_cstring = " pvarlocmap=";
		    /*^apply.arg */
		    argtab[8].meltbp_aptr =
		      (melt_ptr_t *) & /*_.PVARLOCMAP__V90*/ meltfptr[89];
		    /*^apply.arg */
		    argtab[9].meltbp_cstring = " mapcst=";
		    /*^apply.arg */
		    argtab[10].meltbp_aptr =
		      (melt_ptr_t *) & /*_.MAPCST__V88*/ meltfptr[51];
		    /*_.MELT_DEBUG_FUN__V105*/ meltfptr[104] =
		      melt_apply ((meltclosure_ptr_t)
				  (( /*!MELT_DEBUG_FUN */ meltfrout->
				    tabval[3])),
				  (melt_ptr_t) (( /*nil */ NULL)),
				  (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_PTR MELTBPARSTR_CSTRING
				   MELTBPARSTR_PTR MELTBPARSTR_CSTRING
				   MELTBPARSTR_PTR MELTBPARSTR_CSTRING
				   MELTBPARSTR_PTR ""), argtab, "",
				  (union meltparam_un *) 0);
		  }
		  ;
		  /*_.IF___V104*/ meltfptr[103] =
		    /*_.MELT_DEBUG_FUN__V105*/ meltfptr[104];;
		  /*epilog */

		  MELT_LOCATION ("warmelt-normatch.melt:2379:/ clear");
		/*clear *//*_#THE_MELTCALLCOUNT__L29*/ meltfnum[22] = 0;
		  /*^clear */
		/*clear *//*_.MELT_DEBUG_FUN__V105*/ meltfptr[104] = 0;
		}
		;
	      }
	    else
	      {			/*^cond.else */

     /*_.IF___V104*/ meltfptr[103] = NULL;;
	      }
	    ;
	    MELT_LOCATION ("warmelt-normatch.melt:2379:/ quasiblock");


	    /*_.PROGN___V106*/ meltfptr[104] = /*_.IF___V104*/ meltfptr[103];;
	    /*^compute */
	    /*_.IFCPP___V103*/ meltfptr[102] =
	      /*_.PROGN___V106*/ meltfptr[104];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-normatch.melt:2379:/ clear");
	      /*clear *//*_#MELT_NEED_DBG__L28*/ meltfnum[23] = 0;
	    /*^clear */
	      /*clear *//*_.IF___V104*/ meltfptr[103] = 0;
	    /*^clear */
	      /*clear *//*_.PROGN___V106*/ meltfptr[104] = 0;
	  }

#else /*MELT_HAVE_DEBUG */
	  /*^cppif.else */
	  /*_.IFCPP___V103*/ meltfptr[102] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
	  ;

	  {
	    MELT_LOCATION ("warmelt-normatch.melt:2382:/ locexp");
	    meltgc_multiple_put_nth ((melt_ptr_t)
				     ( /*_.TUPVARMAP__V20*/ meltfptr[19]),
				     ( /*_#IX__L22*/ meltfnum[17]),
				     (melt_ptr_t) ( /*_.MAPVAR__V87*/
						   meltfptr[68]));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-normatch.melt:2383:/ locexp");
	    meltgc_multiple_put_nth ((melt_ptr_t)
				     ( /*_.TUPCSTMAP__V21*/ meltfptr[20]),
				     ( /*_#IX__L22*/ meltfnum[17]),
				     (melt_ptr_t) ( /*_.MAPCST__V88*/
						   meltfptr[51]));
	  }
	  ;

#if MELT_HAVE_DEBUG
	  MELT_LOCATION ("warmelt-normatch.melt:2384:/ cppif.then");
	  /*^block */
	  /*anyblock */
	  {


	    {
	      /*^locexp */
	      /*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	      melt_dbgcounter++;
#endif
	      ;
	    }
	    ;
	    /*^checksignal */
	    MELT_CHECK_SIGNAL ();
	    ;
    /*_#MELT_NEED_DBG__L30*/ meltfnum[22] =
	      /*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	      ( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	      0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	      ;;
	    MELT_LOCATION ("warmelt-normatch.melt:2384:/ cond");
	    /*cond */ if ( /*_#MELT_NEED_DBG__L30*/ meltfnum[22])	/*then */
	      {
		/*^cond.then */
		/*^block */
		/*anyblock */
		{

      /*_#THE_MELTCALLCOUNT__L31*/ meltfnum[23] =
#ifdef meltcallcount
		    meltcallcount	/* the_meltcallcount */
#else
		    0L
#endif /* meltcallcount the_meltcallcount */
		    ;;
		  MELT_LOCATION ("warmelt-normatch.melt:2384:/ checksignal");
		  MELT_CHECK_SIGNAL ();
		  ;
		  /*^apply */
		  /*apply */
		  {
		    union meltparam_un argtab[7];
		    memset (&argtab, 0, sizeof (argtab));
		    /*^apply.arg */
		    argtab[0].meltbp_long =
		      /*_#THE_MELTCALLCOUNT__L31*/ meltfnum[23];
		    /*^apply.arg */
		    argtab[1].meltbp_cstring = "warmelt-normatch.melt";
		    /*^apply.arg */
		    argtab[2].meltbp_long = 2384;
		    /*^apply.arg */
		    argtab[3].meltbp_cstring =
		      "normexp_match  before normal_pattern shabindlist=";
		    /*^apply.arg */
		    argtab[4].meltbp_aptr =
		      (melt_ptr_t *) & /*_.SHABINDLIST__V23*/ meltfptr[22];
		    /*^apply.arg */
		    argtab[5].meltbp_cstring = " curpat=";
		    /*^apply.arg */
		    argtab[6].meltbp_aptr =
		      (melt_ptr_t *) & /*_.CURPAT__V85*/ meltfptr[64];
		    /*_.MELT_DEBUG_FUN__V109*/ meltfptr[108] =
		      melt_apply ((meltclosure_ptr_t)
				  (( /*!MELT_DEBUG_FUN */ meltfrout->
				    tabval[3])),
				  (melt_ptr_t) (( /*nil */ NULL)),
				  (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_PTR MELTBPARSTR_CSTRING
				   MELTBPARSTR_PTR ""), argtab, "",
				  (union meltparam_un *) 0);
		  }
		  ;
		  /*_.IF___V108*/ meltfptr[104] =
		    /*_.MELT_DEBUG_FUN__V109*/ meltfptr[108];;
		  /*epilog */

		  MELT_LOCATION ("warmelt-normatch.melt:2384:/ clear");
		/*clear *//*_#THE_MELTCALLCOUNT__L31*/ meltfnum[23] = 0;
		  /*^clear */
		/*clear *//*_.MELT_DEBUG_FUN__V109*/ meltfptr[108] = 0;
		}
		;
	      }
	    else
	      {			/*^cond.else */

     /*_.IF___V108*/ meltfptr[104] = NULL;;
	      }
	    ;
	    MELT_LOCATION ("warmelt-normatch.melt:2384:/ quasiblock");


	    /*_.PROGN___V110*/ meltfptr[108] = /*_.IF___V108*/ meltfptr[104];;
	    /*^compute */
	    /*_.IFCPP___V107*/ meltfptr[103] =
	      /*_.PROGN___V110*/ meltfptr[108];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-normatch.melt:2384:/ clear");
	      /*clear *//*_#MELT_NEED_DBG__L30*/ meltfnum[22] = 0;
	    /*^clear */
	      /*clear *//*_.IF___V108*/ meltfptr[104] = 0;
	    /*^clear */
	      /*clear *//*_.PROGN___V110*/ meltfptr[108] = 0;
	  }

#else /*MELT_HAVE_DEBUG */
	  /*^cppif.else */
	  /*_.IFCPP___V107*/ meltfptr[103] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
	  ;
	  MELT_LOCATION ("warmelt-normatch.melt:2389:/ quasiblock");


	  /*^newclosure */
		  /*newclosure *//*_.LAMBDA___V112*/ meltfptr[108] =
	    (melt_ptr_t)
	    meltgc_new_closure ((meltobject_ptr_t)
				(((melt_ptr_t)
				  (MELT_PREDEF (DISCR_CLOSURE)))),
				(meltroutine_ptr_t) (( /*!konst_23 */
						      meltfrout->tabval[23])),
				(1));
	  ;
	  /*^putclosedv */
	  /*putclosv */
	  melt_assertmsg ("putclosv checkclo",
			  melt_magic_discr ((melt_ptr_t)
					    ( /*_.LAMBDA___V112*/
					     meltfptr[108])) ==
			  MELTOBMAG_CLOSURE);
	  melt_assertmsg ("putclosv checkoff", 0 >= 0
			  && 0 <
			  melt_closure_size ((melt_ptr_t)
					     ( /*_.LAMBDA___V112*/
					      meltfptr[108])));
	  ((meltclosure_ptr_t) /*_.LAMBDA___V112*/ meltfptr[108])->tabval[0] =
	    (melt_ptr_t) ( /*_.NTESTCONT__V94*/ meltfptr[93]);
	  ;
	  /*_.LAMBDA___V111*/ meltfptr[104] =
	    /*_.LAMBDA___V112*/ meltfptr[108];;
	  MELT_LOCATION ("warmelt-normatch.melt:2387:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^msend */
	  /*msend */
	  {
	    union meltparam_un argtab[3];
	    memset (&argtab, 0, sizeof (argtab));
	    /*^ojbmsend.arg */
	    argtab[0].meltbp_aptr =
	      (melt_ptr_t *) & /*_.NMATX__V37*/ meltfptr[33];
	    /*^ojbmsend.arg */
	    argtab[1].meltbp_aptr =
	      (melt_ptr_t *) & /*_.LAMBDA___V111*/ meltfptr[104];
	    /*^ojbmsend.arg */
	    argtab[2].meltbp_aptr =
	      (melt_ptr_t *) & /*_.PCN__V92*/ meltfptr[91];
	    /*_.NORMAL_PATTERN__V113*/ meltfptr[112] =
	      meltgc_send ((melt_ptr_t) ( /*_.CURPAT__V85*/ meltfptr[64]),
			   (melt_ptr_t) (( /*!NORMAL_PATTERN */ meltfrout->
					  tabval[20])),
			   (MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_PTR
			    ""), argtab, "", (union meltparam_un *) 0);
	  }
	  ;

#if MELT_HAVE_DEBUG
	  MELT_LOCATION ("warmelt-normatch.melt:2396:/ cppif.then");
	  /*^block */
	  /*anyblock */
	  {


	    {
	      /*^locexp */
	      /*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	      melt_dbgcounter++;
#endif
	      ;
	    }
	    ;
	    /*^checksignal */
	    MELT_CHECK_SIGNAL ();
	    ;
    /*_#MELT_NEED_DBG__L32*/ meltfnum[23] =
	      /*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	      ( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	      0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	      ;;
	    MELT_LOCATION ("warmelt-normatch.melt:2396:/ cond");
	    /*cond */ if ( /*_#MELT_NEED_DBG__L32*/ meltfnum[23])	/*then */
	      {
		/*^cond.then */
		/*^block */
		/*anyblock */
		{

      /*_#THE_MELTCALLCOUNT__L33*/ meltfnum[22] =
#ifdef meltcallcount
		    meltcallcount	/* the_meltcallcount */
#else
		    0L
#endif /* meltcallcount the_meltcallcount */
		    ;;
		  MELT_LOCATION ("warmelt-normatch.melt:2396:/ checksignal");
		  MELT_CHECK_SIGNAL ();
		  ;
		  /*^apply */
		  /*apply */
		  {
		    union meltparam_un argtab[11];
		    memset (&argtab, 0, sizeof (argtab));
		    /*^apply.arg */
		    argtab[0].meltbp_long =
		      /*_#THE_MELTCALLCOUNT__L33*/ meltfnum[22];
		    /*^apply.arg */
		    argtab[1].meltbp_cstring = "warmelt-normatch.melt";
		    /*^apply.arg */
		    argtab[2].meltbp_long = 2396;
		    /*^apply.arg */
		    argtab[3].meltbp_cstring =
		      "normexp_match  after normal_pattern curpat=";
		    /*^apply.arg */
		    argtab[4].meltbp_aptr =
		      (melt_ptr_t *) & /*_.CURPAT__V85*/ meltfptr[64];
		    /*^apply.arg */
		    argtab[5].meltbp_cstring = " pvarlocmap=";
		    /*^apply.arg */
		    argtab[6].meltbp_aptr =
		      (melt_ptr_t *) & /*_.PVARLOCMAP__V90*/ meltfptr[89];
		    /*^apply.arg */
		    argtab[7].meltbp_cstring = "ntestcont= ";
		    /*^apply.arg */
		    argtab[8].meltbp_aptr =
		      (melt_ptr_t *) & /*_.NTESTCONT__V94*/ meltfptr[93];
		    /*^apply.arg */
		    argtab[9].meltbp_cstring = "shabindlist= ";
		    /*^apply.arg */
		    argtab[10].meltbp_aptr =
		      (melt_ptr_t *) & /*_.SHABINDLIST__V23*/ meltfptr[22];
		    /*_.MELT_DEBUG_FUN__V116*/ meltfptr[115] =
		      melt_apply ((meltclosure_ptr_t)
				  (( /*!MELT_DEBUG_FUN */ meltfrout->
				    tabval[3])),
				  (melt_ptr_t) (( /*nil */ NULL)),
				  (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_PTR MELTBPARSTR_CSTRING
				   MELTBPARSTR_PTR MELTBPARSTR_CSTRING
				   MELTBPARSTR_PTR MELTBPARSTR_CSTRING
				   MELTBPARSTR_PTR ""), argtab, "",
				  (union meltparam_un *) 0);
		  }
		  ;
		  /*_.IF___V115*/ meltfptr[114] =
		    /*_.MELT_DEBUG_FUN__V116*/ meltfptr[115];;
		  /*epilog */

		  MELT_LOCATION ("warmelt-normatch.melt:2396:/ clear");
		/*clear *//*_#THE_MELTCALLCOUNT__L33*/ meltfnum[22] = 0;
		  /*^clear */
		/*clear *//*_.MELT_DEBUG_FUN__V116*/ meltfptr[115] = 0;
		}
		;
	      }
	    else
	      {			/*^cond.else */

     /*_.IF___V115*/ meltfptr[114] = NULL;;
	      }
	    ;
	    MELT_LOCATION ("warmelt-normatch.melt:2396:/ quasiblock");


	    /*_.PROGN___V117*/ meltfptr[115] = /*_.IF___V115*/ meltfptr[114];;
	    /*^compute */
	    /*_.IFCPP___V114*/ meltfptr[113] =
	      /*_.PROGN___V117*/ meltfptr[115];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-normatch.melt:2396:/ clear");
	      /*clear *//*_#MELT_NEED_DBG__L32*/ meltfnum[23] = 0;
	    /*^clear */
	      /*clear *//*_.IF___V115*/ meltfptr[114] = 0;
	    /*^clear */
	      /*clear *//*_.PROGN___V117*/ meltfptr[115] = 0;
	  }

#else /*MELT_HAVE_DEBUG */
	  /*^cppif.else */
	  /*_.IFCPP___V114*/ meltfptr[113] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
	  ;
	  MELT_LOCATION ("warmelt-normatch.melt:2401:/ quasiblock");


	  /*^cond */
	  /*cond */ if (
			 /*ifisa */
			 melt_is_instance_of ((melt_ptr_t)
					      ( /*_.PCN__V92*/ meltfptr[91]),
					      (melt_ptr_t) (( /*!CLASS_PATTERN_CONTEXT */ meltfrout->tabval[17])))
	    )			/*then */
	    {
	      /*^cond.then */
	      /*^getslot */
	      {
		melt_ptr_t slot = NULL, obj = NULL;
		obj = (melt_ptr_t) ( /*_.PCN__V92*/ meltfptr[91]) /*=obj*/ ;
		melt_object_get_field (slot, obj, 2, "PCTN_ENV");
    /*_.NEWENV__V119*/ meltfptr[115] = slot;
	      };
	      ;
	    }
	  else
	    {			/*^cond.else */

   /*_.NEWENV__V119*/ meltfptr[115] = NULL;;
	    }
	  ;
	  MELT_LOCATION ("warmelt-normatch.melt:2402:/ cond");
	  /*cond */ if (
			 /*ifisa */
			 melt_is_instance_of ((melt_ptr_t)
					      ( /*_.NTESTCONT__V94*/
					       meltfptr[93]),
					      (melt_ptr_t) (( /*!CLASS_REFERENCE */ meltfrout->tabval[18])))
	    )			/*then */
	    {
	      /*^cond.then */
	      /*^getslot */
	      {
		melt_ptr_t slot = NULL, obj = NULL;
		obj =
		  (melt_ptr_t) ( /*_.NTESTCONT__V94*/ meltfptr[93]) /*=obj*/ ;
		melt_object_get_field (slot, obj, 0, "REFERENCED_VALUE");
    /*_.CURTESTER__V120*/ meltfptr[119] = slot;
	      };
	      ;
	    }
	  else
	    {			/*^cond.else */

   /*_.CURTESTER__V120*/ meltfptr[119] = NULL;;
	    }
	  ;
	  MELT_LOCATION ("warmelt-normatch.melt:2408:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
  /*_.MAKE_LIST__V121*/ meltfptr[120] =
	    (meltgc_new_list
	     ((meltobject_ptr_t)
	      (( /*!DISCR_LIST */ meltfrout->tabval[6]))));;
	  MELT_LOCATION ("warmelt-normatch.melt:2408:/ quasiblock");


	  /*^rawallocobj */
	  /*rawallocobj */
	  {
	    melt_ptr_t newobj = 0;
	    melt_raw_object_create (newobj,
				    (melt_ptr_t) (( /*!CLASS_NORMTESTER_SUCCESS */ meltfrout->tabval[24])), (8), "CLASS_NORMTESTER_SUCCESS");
   /*_.INST__V123*/ meltfptr[122] =
	      newobj;
	  };
	  ;
	  /*^putslot */
	  /*putslot */
	  melt_assertmsg ("putslot checkobj @NREP_LOC",
			  melt_magic_discr ((melt_ptr_t)
					    ( /*_.INST__V123*/ meltfptr[122]))
			  == MELTOBMAG_OBJECT);
	  melt_putfield_object (( /*_.INST__V123*/ meltfptr[122]), (0),
				( /*_.CURLOC__V84*/ meltfptr[58]),
				"NREP_LOC");
	  ;
	  /*^putslot */
	  /*putslot */
	  melt_assertmsg ("putslot checkobj @NTEST_MATCHED",
			  melt_magic_discr ((melt_ptr_t)
					    ( /*_.INST__V123*/ meltfptr[122]))
			  == MELTOBMAG_OBJECT);
	  melt_putfield_object (( /*_.INST__V123*/ meltfptr[122]), (1),
				(( /*nil */ NULL)), "NTEST_MATCHED");
	  ;
	  /*^putslot */
	  /*putslot */
	  melt_assertmsg ("putslot checkobj @NTEST_THEN",
			  melt_magic_discr ((melt_ptr_t)
					    ( /*_.INST__V123*/ meltfptr[122]))
			  == MELTOBMAG_OBJECT);
	  melt_putfield_object (( /*_.INST__V123*/ meltfptr[122]), (2),
				(( /*nil */ NULL)), "NTEST_THEN");
	  ;
	  /*^putslot */
	  /*putslot */
	  melt_assertmsg ("putslot checkobj @NTEST_ELSE",
			  melt_magic_discr ((melt_ptr_t)
					    ( /*_.INST__V123*/ meltfptr[122]))
			  == MELTOBMAG_OBJECT);
	  melt_putfield_object (( /*_.INST__V123*/ meltfptr[122]), (3),
				(( /*nil */ NULL)), "NTEST_ELSE");
	  ;
	  /*^putslot */
	  /*putslot */
	  melt_assertmsg ("putslot checkobj @NTEST_COMEFROM",
			  melt_magic_discr ((melt_ptr_t)
					    ( /*_.INST__V123*/ meltfptr[122]))
			  == MELTOBMAG_OBJECT);
	  melt_putfield_object (( /*_.INST__V123*/ meltfptr[122]), (6),
				( /*_.MAKE_LIST__V121*/ meltfptr[120]),
				"NTEST_COMEFROM");
	  ;
	  /*^putslot */
	  /*putslot */
	  melt_assertmsg ("putslot checkobj @NTSUCCESS_DO",
			  melt_magic_discr ((melt_ptr_t)
					    ( /*_.INST__V123*/ meltfptr[122]))
			  == MELTOBMAG_OBJECT);
	  melt_putfield_object (( /*_.INST__V123*/ meltfptr[122]), (7),
				(( /*nil */ NULL)), "NTSUCCESS_DO");
	  ;
	  /*^touchobj */

	  melt_dbgtrace_written_object ( /*_.INST__V123*/ meltfptr[122],
					"newly made instance");
	  ;
	  /*_.NEWSUCTESTER__V122*/ meltfptr[121] =
	    /*_.INST__V123*/ meltfptr[122];;

#if MELT_HAVE_DEBUG
	  MELT_LOCATION ("warmelt-normatch.melt:2420:/ cppif.then");
	  /*^block */
	  /*anyblock */
	  {


	    {
	      /*^locexp */
	      /*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	      melt_dbgcounter++;
#endif
	      ;
	    }
	    ;
	    /*^checksignal */
	    MELT_CHECK_SIGNAL ();
	    ;
    /*_#MELT_NEED_DBG__L34*/ meltfnum[22] =
	      /*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	      ( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	      0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	      ;;
	    MELT_LOCATION ("warmelt-normatch.melt:2420:/ cond");
	    /*cond */ if ( /*_#MELT_NEED_DBG__L34*/ meltfnum[22])	/*then */
	      {
		/*^cond.then */
		/*^block */
		/*anyblock */
		{

      /*_#THE_MELTCALLCOUNT__L35*/ meltfnum[23] =
#ifdef meltcallcount
		    meltcallcount	/* the_meltcallcount */
#else
		    0L
#endif /* meltcallcount the_meltcallcount */
		    ;;
		  MELT_LOCATION ("warmelt-normatch.melt:2420:/ checksignal");
		  MELT_CHECK_SIGNAL ();
		  ;
		  /*^apply */
		  /*apply */
		  {
		    union meltparam_un argtab[15];
		    memset (&argtab, 0, sizeof (argtab));
		    /*^apply.arg */
		    argtab[0].meltbp_long =
		      /*_#THE_MELTCALLCOUNT__L35*/ meltfnum[23];
		    /*^apply.arg */
		    argtab[1].meltbp_cstring = "warmelt-normatch.melt";
		    /*^apply.arg */
		    argtab[2].meltbp_long = 2420;
		    /*^apply.arg */
		    argtab[3].meltbp_cstring =
		      "normexp_match  after normal_pattern newenv=";
		    /*^apply.arg */
		    argtab[4].meltbp_aptr =
		      (melt_ptr_t *) & /*_.NEWENV__V119*/ meltfptr[115];
		    /*^apply.arg */
		    argtab[5].meltbp_cstring = " newsuctester=";
		    /*^apply.arg */
		    argtab[6].meltbp_aptr =
		      (melt_ptr_t *) & /*_.NEWSUCTESTER__V122*/ meltfptr[121];
		    /*^apply.arg */
		    argtab[7].meltbp_cstring = " pvarlocmap=";
		    /*^apply.arg */
		    argtab[8].meltbp_aptr =
		      (melt_ptr_t *) & /*_.PVARLOCMAP__V90*/ meltfptr[89];
		    /*^apply.arg */
		    argtab[9].meltbp_cstring = " curcas=";
		    /*^apply.arg */
		    argtab[10].meltbp_aptr =
		      (melt_ptr_t *) & /*_.CURCAS__V76*/ meltfptr[60];
		    /*^apply.arg */
		    argtab[11].meltbp_cstring = " curtester=";
		    /*^apply.arg */
		    argtab[12].meltbp_aptr =
		      (melt_ptr_t *) & /*_.CURTESTER__V120*/ meltfptr[119];
		    /*^apply.arg */
		    argtab[13].meltbp_cstring = " oldtester=";
		    /*^apply.arg */
		    argtab[14].meltbp_aptr =
		      (melt_ptr_t *) & /*_.OLDTESTER__V26*/ meltfptr[25];
		    /*_.MELT_DEBUG_FUN__V126*/ meltfptr[125] =
		      melt_apply ((meltclosure_ptr_t)
				  (( /*!MELT_DEBUG_FUN */ meltfrout->
				    tabval[3])),
				  (melt_ptr_t) (( /*nil */ NULL)),
				  (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_PTR MELTBPARSTR_CSTRING
				   MELTBPARSTR_PTR MELTBPARSTR_CSTRING
				   MELTBPARSTR_PTR MELTBPARSTR_CSTRING
				   MELTBPARSTR_PTR MELTBPARSTR_CSTRING
				   MELTBPARSTR_PTR MELTBPARSTR_CSTRING
				   MELTBPARSTR_PTR ""), argtab, "",
				  (union meltparam_un *) 0);
		  }
		  ;
		  /*_.IF___V125*/ meltfptr[124] =
		    /*_.MELT_DEBUG_FUN__V126*/ meltfptr[125];;
		  /*epilog */

		  MELT_LOCATION ("warmelt-normatch.melt:2420:/ clear");
		/*clear *//*_#THE_MELTCALLCOUNT__L35*/ meltfnum[23] = 0;
		  /*^clear */
		/*clear *//*_.MELT_DEBUG_FUN__V126*/ meltfptr[125] = 0;
		}
		;
	      }
	    else
	      {			/*^cond.else */

     /*_.IF___V125*/ meltfptr[124] = NULL;;
	      }
	    ;
	    MELT_LOCATION ("warmelt-normatch.melt:2420:/ quasiblock");


	    /*_.PROGN___V127*/ meltfptr[125] = /*_.IF___V125*/ meltfptr[124];;
	    /*^compute */
	    /*_.IFCPP___V124*/ meltfptr[123] =
	      /*_.PROGN___V127*/ meltfptr[125];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-normatch.melt:2420:/ clear");
	      /*clear *//*_#MELT_NEED_DBG__L34*/ meltfnum[22] = 0;
	    /*^clear */
	      /*clear *//*_.IF___V125*/ meltfptr[124] = 0;
	    /*^clear */
	      /*clear *//*_.PROGN___V127*/ meltfptr[125] = 0;
	  }

#else /*MELT_HAVE_DEBUG */
	  /*^cppif.else */
	  /*_.IFCPP___V124*/ meltfptr[123] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
	  ;

	  {
	    MELT_LOCATION ("warmelt-normatch.melt:2428:/ locexp");
	    meltgc_append_list ((melt_ptr_t)
				( /*_.TESTLIST__V24*/ meltfptr[23]),
				(melt_ptr_t) ( /*_.NEWSUCTESTER__V122*/
					      meltfptr[121]));
	  }
	  ;
	  MELT_LOCATION ("warmelt-normatch.melt:2430:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
  /*_#NULL__L36*/ meltfnum[23] =
	    (( /*_.CURTESTER__V120*/ meltfptr[119]) == NULL);;
	  MELT_LOCATION ("warmelt-normatch.melt:2430:/ cond");
	  /*cond */ if ( /*_#NULL__L36*/ meltfnum[23])	/*then */
	    {
	      /*^cond.then */
	      /*^block */
	      /*anyblock */
	      {


#if MELT_HAVE_DEBUG
		MELT_LOCATION ("warmelt-normatch.melt:2431:/ cppif.then");
		/*^block */
		/*anyblock */
		{


		  {
		    /*^locexp */
		    /*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
		    melt_dbgcounter++;
#endif
		    ;
		  }
		  ;
		  /*^checksignal */
		  MELT_CHECK_SIGNAL ();
		  ;
      /*_#MELT_NEED_DBG__L37*/ meltfnum[22] =
		    /*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
		    ( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
		    0		/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
		    ;;
		  MELT_LOCATION ("warmelt-normatch.melt:2431:/ cond");
		  /*cond */ if ( /*_#MELT_NEED_DBG__L37*/ meltfnum[22])	/*then */
		    {
		      /*^cond.then */
		      /*^block */
		      /*anyblock */
		      {

	/*_#THE_MELTCALLCOUNT__L38*/ meltfnum[37] =
#ifdef meltcallcount
			  meltcallcount	/* the_meltcallcount */
#else
			  0L
#endif /* meltcallcount the_meltcallcount */
			  ;;
			MELT_LOCATION
			  ("warmelt-normatch.melt:2431:/ checksignal");
			MELT_CHECK_SIGNAL ();
			;
			/*^apply */
			/*apply */
			{
			  union meltparam_un argtab[5];
			  memset (&argtab, 0, sizeof (argtab));
			  /*^apply.arg */
			  argtab[0].meltbp_long =
			    /*_#THE_MELTCALLCOUNT__L38*/ meltfnum[37];
			  /*^apply.arg */
			  argtab[1].meltbp_cstring = "warmelt-normatch.melt";
			  /*^apply.arg */
			  argtab[2].meltbp_long = 2431;
			  /*^apply.arg */
			  argtab[3].meltbp_cstring =
			    "normexp_match curcas for null curtester";
			  /*^apply.arg */
			  argtab[4].meltbp_aptr =
			    (melt_ptr_t *) & /*_.CURCAS__V76*/ meltfptr[60];
			  /*_.MELT_DEBUG_FUN__V131*/ meltfptr[130] =
			    melt_apply ((meltclosure_ptr_t)
					(( /*!MELT_DEBUG_FUN */ meltfrout->
					  tabval[3])),
					(melt_ptr_t) (( /*nil */ NULL)),
					(MELTBPARSTR_LONG MELTBPARSTR_CSTRING
					 MELTBPARSTR_LONG MELTBPARSTR_CSTRING
					 MELTBPARSTR_PTR ""), argtab, "",
					(union meltparam_un *) 0);
			}
			;
			/*_.IF___V130*/ meltfptr[129] =
			  /*_.MELT_DEBUG_FUN__V131*/ meltfptr[130];;
			/*epilog */

			MELT_LOCATION ("warmelt-normatch.melt:2431:/ clear");
		  /*clear *//*_#THE_MELTCALLCOUNT__L38*/ meltfnum[37] =
			  0;
			/*^clear */
		  /*clear *//*_.MELT_DEBUG_FUN__V131*/ meltfptr[130] =
			  0;
		      }
		      ;
		    }
		  else
		    {		/*^cond.else */

       /*_.IF___V130*/ meltfptr[129] = NULL;;
		    }
		  ;
		  MELT_LOCATION ("warmelt-normatch.melt:2431:/ quasiblock");


		  /*_.PROGN___V132*/ meltfptr[130] =
		    /*_.IF___V130*/ meltfptr[129];;
		  /*^compute */
		  /*_.IFCPP___V129*/ meltfptr[125] =
		    /*_.PROGN___V132*/ meltfptr[130];;
		  /*epilog */

		  MELT_LOCATION ("warmelt-normatch.melt:2431:/ clear");
		/*clear *//*_#MELT_NEED_DBG__L37*/ meltfnum[22] = 0;
		  /*^clear */
		/*clear *//*_.IF___V130*/ meltfptr[129] = 0;
		  /*^clear */
		/*clear *//*_.PROGN___V132*/ meltfptr[130] = 0;
		}

#else /*MELT_HAVE_DEBUG */
		/*^cppif.else */
		/*_.IFCPP___V129*/ meltfptr[125] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
		;
		MELT_LOCATION ("warmelt-normatch.melt:2434:/ checksignal");
		MELT_CHECK_SIGNAL ();
		;
    /*_#MULTIPLE_LENGTH__L39*/ meltfnum[37] =
		  (melt_multiple_length
		   ((melt_ptr_t) ( /*_.SCASES__V19*/ meltfptr[18])));;
		/*^compute */
    /*_#I__L40*/ meltfnum[22] =
		  (( /*_#MULTIPLE_LENGTH__L39*/ meltfnum[37]) - (1));;
		/*^compute */
    /*_#I__L41*/ meltfnum[40] =
		  (( /*_#IX__L22*/ meltfnum[17]) <
		   ( /*_#I__L40*/ meltfnum[22]));;
		MELT_LOCATION ("warmelt-normatch.melt:2434:/ cond");
		/*cond */ if ( /*_#I__L41*/ meltfnum[40])	/*then */
		  {
		    /*^cond.then */
		    /*^block */
		    /*anyblock */
		    {


		      {
			MELT_LOCATION ("warmelt-normatch.melt:2435:/ locexp");
			/* error_plain */
			  melt_error_str ((melt_ptr_t)
					  ( /*_.SLOC__V17*/ meltfptr[13]),
					  ("joker case in MATCH is not last"),
					  (melt_ptr_t) 0);
		      }
		      ;
		      /*epilog */
		    }
		    ;
		  }		/*noelse */
		;
		MELT_LOCATION ("warmelt-normatch.melt:2437:/ compute");
		/*_.CURTESTER__V120*/ meltfptr[119] =
		  /*_.SETQ___V133*/ meltfptr[129] =
		  /*_.NEWSUCTESTER__V122*/ meltfptr[121];;

#if MELT_HAVE_DEBUG
		MELT_LOCATION ("warmelt-normatch.melt:2438:/ cppif.then");
		/*^block */
		/*anyblock */
		{


		  {
		    /*^locexp */
		    /*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
		    melt_dbgcounter++;
#endif
		    ;
		  }
		  ;
		  /*^checksignal */
		  MELT_CHECK_SIGNAL ();
		  ;
      /*_#MELT_NEED_DBG__L42*/ meltfnum[41] =
		    /*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
		    ( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
		    0		/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
		    ;;
		  MELT_LOCATION ("warmelt-normatch.melt:2438:/ cond");
		  /*cond */ if ( /*_#MELT_NEED_DBG__L42*/ meltfnum[41])	/*then */
		    {
		      /*^cond.then */
		      /*^block */
		      /*anyblock */
		      {

	/*_#THE_MELTCALLCOUNT__L43*/ meltfnum[42] =
#ifdef meltcallcount
			  meltcallcount	/* the_meltcallcount */
#else
			  0L
#endif /* meltcallcount the_meltcallcount */
			  ;;
			MELT_LOCATION
			  ("warmelt-normatch.melt:2438:/ checksignal");
			MELT_CHECK_SIGNAL ();
			;
			/*^apply */
			/*apply */
			{
			  union meltparam_un argtab[5];
			  memset (&argtab, 0, sizeof (argtab));
			  /*^apply.arg */
			  argtab[0].meltbp_long =
			    /*_#THE_MELTCALLCOUNT__L43*/ meltfnum[42];
			  /*^apply.arg */
			  argtab[1].meltbp_cstring = "warmelt-normatch.melt";
			  /*^apply.arg */
			  argtab[2].meltbp_long = 2438;
			  /*^apply.arg */
			  argtab[3].meltbp_cstring =
			    "normexp_match curestester set to success";
			  /*^apply.arg */
			  argtab[4].meltbp_aptr =
			    (melt_ptr_t *) & /*_.CURTESTER__V120*/
			    meltfptr[119];
			  /*_.MELT_DEBUG_FUN__V136*/ meltfptr[135] =
			    melt_apply ((meltclosure_ptr_t)
					(( /*!MELT_DEBUG_FUN */ meltfrout->
					  tabval[3])),
					(melt_ptr_t) (( /*nil */ NULL)),
					(MELTBPARSTR_LONG MELTBPARSTR_CSTRING
					 MELTBPARSTR_LONG MELTBPARSTR_CSTRING
					 MELTBPARSTR_PTR ""), argtab, "",
					(union meltparam_un *) 0);
			}
			;
			/*_.IF___V135*/ meltfptr[134] =
			  /*_.MELT_DEBUG_FUN__V136*/ meltfptr[135];;
			/*epilog */

			MELT_LOCATION ("warmelt-normatch.melt:2438:/ clear");
		  /*clear *//*_#THE_MELTCALLCOUNT__L43*/ meltfnum[42] =
			  0;
			/*^clear */
		  /*clear *//*_.MELT_DEBUG_FUN__V136*/ meltfptr[135] =
			  0;
		      }
		      ;
		    }
		  else
		    {		/*^cond.else */

       /*_.IF___V135*/ meltfptr[134] = NULL;;
		    }
		  ;
		  MELT_LOCATION ("warmelt-normatch.melt:2438:/ quasiblock");


		  /*_.PROGN___V137*/ meltfptr[135] =
		    /*_.IF___V135*/ meltfptr[134];;
		  /*^compute */
		  /*_.IFCPP___V134*/ meltfptr[130] =
		    /*_.PROGN___V137*/ meltfptr[135];;
		  /*epilog */

		  MELT_LOCATION ("warmelt-normatch.melt:2438:/ clear");
		/*clear *//*_#MELT_NEED_DBG__L42*/ meltfnum[41] = 0;
		  /*^clear */
		/*clear *//*_.IF___V135*/ meltfptr[134] = 0;
		  /*^clear */
		/*clear *//*_.PROGN___V137*/ meltfptr[135] = 0;
		}

#else /*MELT_HAVE_DEBUG */
		/*^cppif.else */
		/*_.IFCPP___V134*/ meltfptr[130] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
		;
		MELT_LOCATION ("warmelt-normatch.melt:2430:/ quasiblock");


		/*_.PROGN___V138*/ meltfptr[134] =
		  /*_.IFCPP___V134*/ meltfptr[130];;
		/*^compute */
		/*_.IFELSE___V128*/ meltfptr[124] =
		  /*_.PROGN___V138*/ meltfptr[134];;
		/*epilog */

		MELT_LOCATION ("warmelt-normatch.melt:2430:/ clear");
	      /*clear *//*_.IFCPP___V129*/ meltfptr[125] = 0;
		/*^clear */
	      /*clear *//*_#MULTIPLE_LENGTH__L39*/ meltfnum[37] = 0;
		/*^clear */
	      /*clear *//*_#I__L40*/ meltfnum[22] = 0;
		/*^clear */
	      /*clear *//*_#I__L41*/ meltfnum[40] = 0;
		/*^clear */
	      /*clear *//*_.SETQ___V133*/ meltfptr[129] = 0;
		/*^clear */
	      /*clear *//*_.IFCPP___V134*/ meltfptr[130] = 0;
		/*^clear */
	      /*clear *//*_.PROGN___V138*/ meltfptr[134] = 0;
	      }
	      ;
	    }
	  else
	    {			/*^cond.else */

	      /*^block */
	      /*anyblock */
	      {


#if MELT_HAVE_DEBUG
		MELT_LOCATION ("warmelt-normatch.melt:2442:/ cppif.then");
		/*^block */
		/*anyblock */
		{

		  /*^checksignal */
		  MELT_CHECK_SIGNAL ();
		  ;
      /*_#IS_NOT_A__L44*/ meltfnum[42] =
		    !melt_is_instance_of ((melt_ptr_t)
					  ( /*_.CURTESTER__V120*/
					   meltfptr[119]),
					  (melt_ptr_t) (( /*!CLASS_NORMTESTER_SUCCESS */ meltfrout->tabval[24])));;
		  MELT_LOCATION ("warmelt-normatch.melt:2442:/ cond");
		  /*cond */ if ( /*_#IS_NOT_A__L44*/ meltfnum[42])	/*then */
		    {
		      /*^cond.then */
		      /*_.IFELSE___V140*/ meltfptr[125] = ( /*nil */ NULL);;
		    }
		  else
		    {
		      MELT_LOCATION
			("warmelt-normatch.melt:2442:/ cond.else");

		      /*^block */
		      /*anyblock */
		      {




			{
			  /*^locexp */
			  melt_assert_failed (("check curtester not success"),
					      ("warmelt-normatch.melt")
					      ? ("warmelt-normatch.melt") :
					      __FILE__,
					      (2442) ? (2442) : __LINE__,
					      __FUNCTION__);
			  ;
			}
			;
		  /*clear *//*_.IFELSE___V140*/ meltfptr[125] = 0;
			/*epilog */
		      }
		      ;
		    }
		  ;
		  /*^compute */
		  /*_.IFCPP___V139*/ meltfptr[135] =
		    /*_.IFELSE___V140*/ meltfptr[125];;
		  /*epilog */

		  MELT_LOCATION ("warmelt-normatch.melt:2442:/ clear");
		/*clear *//*_#IS_NOT_A__L44*/ meltfnum[42] = 0;
		  /*^clear */
		/*clear *//*_.IFELSE___V140*/ meltfptr[125] = 0;
		}

#else /*MELT_HAVE_DEBUG */
		/*^cppif.else */
		/*_.IFCPP___V139*/ meltfptr[135] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
		;
		MELT_LOCATION ("warmelt-normatch.melt:2445:/ quasiblock");


		/*^checksignal */
		MELT_CHECK_SIGNAL ();
		;
		/*^quasiblock */


		/*^rawallocobj */
		/*rawallocobj */
		{
		  melt_ptr_t newobj = 0;
		  melt_raw_object_create (newobj,
					  (melt_ptr_t) (( /*!CLASS_REFERENCE */ meltfrout->tabval[18])), (1), "CLASS_REFERENCE");
     /*_.INST__V143*/ meltfptr[134] =
		    newobj;
		};
		;
		/*^putslot */
		/*putslot */
		melt_assertmsg ("putslot checkobj @REFERENCED_VALUE",
				melt_magic_discr ((melt_ptr_t)
						  ( /*_.INST__V143*/
						   meltfptr[134])) ==
				MELTOBMAG_OBJECT);
		melt_putfield_object (( /*_.INST__V143*/ meltfptr[134]), (0),
				      ( /*_.CURTESTER__V120*/ meltfptr[119]),
				      "REFERENCED_VALUE");
		;
		/*^touchobj */

		melt_dbgtrace_written_object ( /*_.INST__V143*/ meltfptr[134],
					      "newly made instance");
		;
		/*_.CURTESTCONT__V142*/ meltfptr[130] =
		  /*_.INST__V143*/ meltfptr[134];;

#if MELT_HAVE_DEBUG
		MELT_LOCATION ("warmelt-normatch.melt:2447:/ cppif.then");
		/*^block */
		/*anyblock */
		{


		  {
		    /*^locexp */
		    /*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
		    melt_dbgcounter++;
#endif
		    ;
		  }
		  ;
		  /*^checksignal */
		  MELT_CHECK_SIGNAL ();
		  ;
      /*_#MELT_NEED_DBG__L45*/ meltfnum[41] =
		    /*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
		    ( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
		    0		/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
		    ;;
		  MELT_LOCATION ("warmelt-normatch.melt:2447:/ cond");
		  /*cond */ if ( /*_#MELT_NEED_DBG__L45*/ meltfnum[41])	/*then */
		    {
		      /*^cond.then */
		      /*^block */
		      /*anyblock */
		      {

	/*_#THE_MELTCALLCOUNT__L46*/ meltfnum[37] =
#ifdef meltcallcount
			  meltcallcount	/* the_meltcallcount */
#else
			  0L
#endif /* meltcallcount the_meltcallcount */
			  ;;
			MELT_LOCATION
			  ("warmelt-normatch.melt:2447:/ checksignal");
			MELT_CHECK_SIGNAL ();
			;
			/*^apply */
			/*apply */
			{
			  union meltparam_un argtab[7];
			  memset (&argtab, 0, sizeof (argtab));
			  /*^apply.arg */
			  argtab[0].meltbp_long =
			    /*_#THE_MELTCALLCOUNT__L46*/ meltfnum[37];
			  /*^apply.arg */
			  argtab[1].meltbp_cstring = "warmelt-normatch.melt";
			  /*^apply.arg */
			  argtab[2].meltbp_long = 2447;
			  /*^apply.arg */
			  argtab[3].meltbp_cstring =
			    "normexp_match our curtestcont=";
			  /*^apply.arg */
			  argtab[4].meltbp_aptr =
			    (melt_ptr_t *) & /*_.CURTESTCONT__V142*/
			    meltfptr[130];
			  /*^apply.arg */
			  argtab[5].meltbp_cstring = " newsuctester=";
			  /*^apply.arg */
			  argtab[6].meltbp_aptr =
			    (melt_ptr_t *) & /*_.NEWSUCTESTER__V122*/
			    meltfptr[121];
			  /*_.MELT_DEBUG_FUN__V146*/ meltfptr[145] =
			    melt_apply ((meltclosure_ptr_t)
					(( /*!MELT_DEBUG_FUN */ meltfrout->
					  tabval[3])),
					(melt_ptr_t) (( /*nil */ NULL)),
					(MELTBPARSTR_LONG MELTBPARSTR_CSTRING
					 MELTBPARSTR_LONG MELTBPARSTR_CSTRING
					 MELTBPARSTR_PTR MELTBPARSTR_CSTRING
					 MELTBPARSTR_PTR ""), argtab, "",
					(union meltparam_un *) 0);
			}
			;
			/*_.IF___V145*/ meltfptr[144] =
			  /*_.MELT_DEBUG_FUN__V146*/ meltfptr[145];;
			/*epilog */

			MELT_LOCATION ("warmelt-normatch.melt:2447:/ clear");
		  /*clear *//*_#THE_MELTCALLCOUNT__L46*/ meltfnum[37] =
			  0;
			/*^clear */
		  /*clear *//*_.MELT_DEBUG_FUN__V146*/ meltfptr[145] =
			  0;
		      }
		      ;
		    }
		  else
		    {		/*^cond.else */

       /*_.IF___V145*/ meltfptr[144] = NULL;;
		    }
		  ;
		  MELT_LOCATION ("warmelt-normatch.melt:2447:/ quasiblock");


		  /*_.PROGN___V147*/ meltfptr[145] =
		    /*_.IF___V145*/ meltfptr[144];;
		  /*^compute */
		  /*_.IFCPP___V144*/ meltfptr[125] =
		    /*_.PROGN___V147*/ meltfptr[145];;
		  /*epilog */

		  MELT_LOCATION ("warmelt-normatch.melt:2447:/ clear");
		/*clear *//*_#MELT_NEED_DBG__L45*/ meltfnum[41] = 0;
		  /*^clear */
		/*clear *//*_.IF___V145*/ meltfptr[144] = 0;
		  /*^clear */
		/*clear *//*_.PROGN___V147*/ meltfptr[145] = 0;
		}

#else /*MELT_HAVE_DEBUG */
		/*^cppif.else */
		/*_.IFCPP___V144*/ meltfptr[125] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
		;
		MELT_LOCATION ("warmelt-normatch.melt:2449:/ checksignal");
		MELT_CHECK_SIGNAL ();
		;
		/*^apply */
		/*apply */
		{
		  union meltparam_un argtab[1];
		  memset (&argtab, 0, sizeof (argtab));
		  /*^apply.arg */
		  argtab[0].meltbp_aptr =
		    (melt_ptr_t *) & /*_.CURTESTCONT__V142*/ meltfptr[130];
		  /*_.SET_NEW_TESTER_LAST_THEN__V148*/ meltfptr[144] =
		    melt_apply ((meltclosure_ptr_t)
				(( /*!SET_NEW_TESTER_LAST_THEN */ meltfrout->
				  tabval[25])),
				(melt_ptr_t) ( /*_.NEWSUCTESTER__V122*/
					      meltfptr[121]),
				(MELTBPARSTR_PTR ""), argtab, "",
				(union meltparam_un *) 0);
		}
		;

#if MELT_HAVE_DEBUG
		MELT_LOCATION ("warmelt-normatch.melt:2450:/ cppif.then");
		/*^block */
		/*anyblock */
		{


		  {
		    /*^locexp */
		    /*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
		    melt_dbgcounter++;
#endif
		    ;
		  }
		  ;
		  /*^checksignal */
		  MELT_CHECK_SIGNAL ();
		  ;
      /*_#MELT_NEED_DBG__L47*/ meltfnum[22] =
		    /*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
		    ( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
		    0		/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
		    ;;
		  MELT_LOCATION ("warmelt-normatch.melt:2450:/ cond");
		  /*cond */ if ( /*_#MELT_NEED_DBG__L47*/ meltfnum[22])	/*then */
		    {
		      /*^cond.then */
		      /*^block */
		      /*anyblock */
		      {

	/*_#THE_MELTCALLCOUNT__L48*/ meltfnum[40] =
#ifdef meltcallcount
			  meltcallcount	/* the_meltcallcount */
#else
			  0L
#endif /* meltcallcount the_meltcallcount */
			  ;;
			MELT_LOCATION
			  ("warmelt-normatch.melt:2450:/ checksignal");
			MELT_CHECK_SIGNAL ();
			;
			/*^apply */
			/*apply */
			{
			  union meltparam_un argtab[5];
			  memset (&argtab, 0, sizeof (argtab));
			  /*^apply.arg */
			  argtab[0].meltbp_long =
			    /*_#THE_MELTCALLCOUNT__L48*/ meltfnum[40];
			  /*^apply.arg */
			  argtab[1].meltbp_cstring = "warmelt-normatch.melt";
			  /*^apply.arg */
			  argtab[2].meltbp_long = 2450;
			  /*^apply.arg */
			  argtab[3].meltbp_cstring =
			    "normexp_match final curtestcont";
			  /*^apply.arg */
			  argtab[4].meltbp_aptr =
			    (melt_ptr_t *) & /*_.CURTESTCONT__V142*/
			    meltfptr[130];
			  /*_.MELT_DEBUG_FUN__V151*/ meltfptr[150] =
			    melt_apply ((meltclosure_ptr_t)
					(( /*!MELT_DEBUG_FUN */ meltfrout->
					  tabval[3])),
					(melt_ptr_t) (( /*nil */ NULL)),
					(MELTBPARSTR_LONG MELTBPARSTR_CSTRING
					 MELTBPARSTR_LONG MELTBPARSTR_CSTRING
					 MELTBPARSTR_PTR ""), argtab, "",
					(union meltparam_un *) 0);
			}
			;
			/*_.IF___V150*/ meltfptr[149] =
			  /*_.MELT_DEBUG_FUN__V151*/ meltfptr[150];;
			/*epilog */

			MELT_LOCATION ("warmelt-normatch.melt:2450:/ clear");
		  /*clear *//*_#THE_MELTCALLCOUNT__L48*/ meltfnum[40] =
			  0;
			/*^clear */
		  /*clear *//*_.MELT_DEBUG_FUN__V151*/ meltfptr[150] =
			  0;
		      }
		      ;
		    }
		  else
		    {		/*^cond.else */

       /*_.IF___V150*/ meltfptr[149] = NULL;;
		    }
		  ;
		  MELT_LOCATION ("warmelt-normatch.melt:2450:/ quasiblock");


		  /*_.PROGN___V152*/ meltfptr[150] =
		    /*_.IF___V150*/ meltfptr[149];;
		  /*^compute */
		  /*_.IFCPP___V149*/ meltfptr[145] =
		    /*_.PROGN___V152*/ meltfptr[150];;
		  /*epilog */

		  MELT_LOCATION ("warmelt-normatch.melt:2450:/ clear");
		/*clear *//*_#MELT_NEED_DBG__L47*/ meltfnum[22] = 0;
		  /*^clear */
		/*clear *//*_.IF___V150*/ meltfptr[149] = 0;
		  /*^clear */
		/*clear *//*_.PROGN___V152*/ meltfptr[150] = 0;
		}

#else /*MELT_HAVE_DEBUG */
		/*^cppif.else */
		/*_.IFCPP___V149*/ meltfptr[145] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
		;
		/*^compute */
		/*_.LET___V141*/ meltfptr[129] =
		  /*_.IFCPP___V149*/ meltfptr[145];;

		MELT_LOCATION ("warmelt-normatch.melt:2445:/ clear");
	      /*clear *//*_.CURTESTCONT__V142*/ meltfptr[130] = 0;
		/*^clear */
	      /*clear *//*_.IFCPP___V144*/ meltfptr[125] = 0;
		/*^clear */
	      /*clear *//*_.SET_NEW_TESTER_LAST_THEN__V148*/ meltfptr[144] =
		  0;
		/*^clear */
	      /*clear *//*_.IFCPP___V149*/ meltfptr[145] = 0;
		MELT_LOCATION ("warmelt-normatch.melt:2441:/ quasiblock");


		/*_.PROGN___V153*/ meltfptr[149] =
		  /*_.LET___V141*/ meltfptr[129];;
		/*^compute */
		/*_.IFELSE___V128*/ meltfptr[124] =
		  /*_.PROGN___V153*/ meltfptr[149];;
		/*epilog */

		MELT_LOCATION ("warmelt-normatch.melt:2430:/ clear");
	      /*clear *//*_.IFCPP___V139*/ meltfptr[135] = 0;
		/*^clear */
	      /*clear *//*_.LET___V141*/ meltfptr[129] = 0;
		/*^clear */
	      /*clear *//*_.PROGN___V153*/ meltfptr[149] = 0;
	      }
	      ;
	    }
	  ;

#if MELT_HAVE_DEBUG
	  MELT_LOCATION ("warmelt-normatch.melt:2454:/ cppif.then");
	  /*^block */
	  /*anyblock */
	  {


	    {
	      /*^locexp */
	      /*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	      melt_dbgcounter++;
#endif
	      ;
	    }
	    ;
	    /*^checksignal */
	    MELT_CHECK_SIGNAL ();
	    ;
    /*_#MELT_NEED_DBG__L49*/ meltfnum[42] =
	      /*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	      ( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	      0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	      ;;
	    MELT_LOCATION ("warmelt-normatch.melt:2454:/ cond");
	    /*cond */ if ( /*_#MELT_NEED_DBG__L49*/ meltfnum[42])	/*then */
	      {
		/*^cond.then */
		/*^block */
		/*anyblock */
		{

      /*_#THE_MELTCALLCOUNT__L50*/ meltfnum[37] =
#ifdef meltcallcount
		    meltcallcount	/* the_meltcallcount */
#else
		    0L
#endif /* meltcallcount the_meltcallcount */
		    ;;
		  MELT_LOCATION ("warmelt-normatch.melt:2454:/ checksignal");
		  MELT_CHECK_SIGNAL ();
		  ;
		  /*^apply */
		  /*apply */
		  {
		    union meltparam_un argtab[5];
		    memset (&argtab, 0, sizeof (argtab));
		    /*^apply.arg */
		    argtab[0].meltbp_long =
		      /*_#THE_MELTCALLCOUNT__L50*/ meltfnum[37];
		    /*^apply.arg */
		    argtab[1].meltbp_cstring = "warmelt-normatch.melt";
		    /*^apply.arg */
		    argtab[2].meltbp_long = 2454;
		    /*^apply.arg */
		    argtab[3].meltbp_cstring =
		      "normexp_match final curtester";
		    /*^apply.arg */
		    argtab[4].meltbp_aptr =
		      (melt_ptr_t *) & /*_.CURTESTER__V120*/ meltfptr[119];
		    /*_.MELT_DEBUG_FUN__V156*/ meltfptr[125] =
		      melt_apply ((meltclosure_ptr_t)
				  (( /*!MELT_DEBUG_FUN */ meltfrout->
				    tabval[3])),
				  (melt_ptr_t) (( /*nil */ NULL)),
				  (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_PTR ""), argtab, "",
				  (union meltparam_un *) 0);
		  }
		  ;
		  /*_.IF___V155*/ meltfptr[130] =
		    /*_.MELT_DEBUG_FUN__V156*/ meltfptr[125];;
		  /*epilog */

		  MELT_LOCATION ("warmelt-normatch.melt:2454:/ clear");
		/*clear *//*_#THE_MELTCALLCOUNT__L50*/ meltfnum[37] = 0;
		  /*^clear */
		/*clear *//*_.MELT_DEBUG_FUN__V156*/ meltfptr[125] = 0;
		}
		;
	      }
	    else
	      {			/*^cond.else */

     /*_.IF___V155*/ meltfptr[130] = NULL;;
	      }
	    ;
	    MELT_LOCATION ("warmelt-normatch.melt:2454:/ quasiblock");


	    /*_.PROGN___V157*/ meltfptr[144] = /*_.IF___V155*/ meltfptr[130];;
	    /*^compute */
	    /*_.IFCPP___V154*/ meltfptr[150] =
	      /*_.PROGN___V157*/ meltfptr[144];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-normatch.melt:2454:/ clear");
	      /*clear *//*_#MELT_NEED_DBG__L49*/ meltfnum[42] = 0;
	    /*^clear */
	      /*clear *//*_.IF___V155*/ meltfptr[130] = 0;
	    /*^clear */
	      /*clear *//*_.PROGN___V157*/ meltfptr[144] = 0;
	  }

#else /*MELT_HAVE_DEBUG */
	  /*^cppif.else */
	  /*_.IFCPP___V154*/ meltfptr[150] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
	  ;

#if MELT_HAVE_DEBUG
	  MELT_LOCATION ("warmelt-normatch.melt:2455:/ cppif.then");
	  /*^block */
	  /*anyblock */
	  {

	    /*^checksignal */
	    MELT_CHECK_SIGNAL ();
	    ;
    /*_#IS_A__L51*/ meltfnum[41] =
	      melt_is_instance_of ((melt_ptr_t)
				   ( /*_.CURTESTER__V120*/ meltfptr[119]),
				   (melt_ptr_t) (( /*!CLASS_NORMTESTER_ANY */
						  meltfrout->tabval[26])));;
	    MELT_LOCATION ("warmelt-normatch.melt:2455:/ cond");
	    /*cond */ if ( /*_#IS_A__L51*/ meltfnum[41])	/*then */
	      {
		/*^cond.then */
		/*_.IFELSE___V159*/ meltfptr[135] = ( /*nil */ NULL);;
	      }
	    else
	      {
		MELT_LOCATION ("warmelt-normatch.melt:2455:/ cond.else");

		/*^block */
		/*anyblock */
		{




		  {
		    /*^locexp */
		    melt_assert_failed (("check final curtester"),
					("warmelt-normatch.melt")
					? ("warmelt-normatch.melt") :
					__FILE__, (2455) ? (2455) : __LINE__,
					__FUNCTION__);
		    ;
		  }
		  ;
		/*clear *//*_.IFELSE___V159*/ meltfptr[135] = 0;
		  /*epilog */
		}
		;
	      }
	    ;
	    /*^compute */
	    /*_.IFCPP___V158*/ meltfptr[145] =
	      /*_.IFELSE___V159*/ meltfptr[135];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-normatch.melt:2455:/ clear");
	      /*clear *//*_#IS_A__L51*/ meltfnum[41] = 0;
	    /*^clear */
	      /*clear *//*_.IFELSE___V159*/ meltfptr[135] = 0;
	  }

#else /*MELT_HAVE_DEBUG */
	  /*^cppif.else */
	  /*_.IFCPP___V158*/ meltfptr[145] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
	  ;
	  MELT_LOCATION ("warmelt-normatch.melt:2457:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
  /*_#IS_A__L52*/ meltfnum[40] =
	    melt_is_instance_of ((melt_ptr_t)
				 ( /*_.OLDTESTER__V26*/ meltfptr[25]),
				 (melt_ptr_t) (( /*!CLASS_NORMTESTER_ANYTESTER */ meltfrout->tabval[27])));;
	  MELT_LOCATION ("warmelt-normatch.melt:2457:/ cond");
	  /*cond */ if ( /*_#IS_A__L52*/ meltfnum[40])	/*then */
	    {
	      /*^cond.then */
	      /*^block */
	      /*anyblock */
	      {


#if MELT_HAVE_DEBUG
		MELT_LOCATION ("warmelt-normatch.melt:2459:/ cppif.then");
		/*^block */
		/*anyblock */
		{


		  {
		    /*^locexp */
		    /*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
		    melt_dbgcounter++;
#endif
		    ;
		  }
		  ;
		  /*^checksignal */
		  MELT_CHECK_SIGNAL ();
		  ;
      /*_#MELT_NEED_DBG__L53*/ meltfnum[22] =
		    /*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
		    ( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
		    0		/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
		    ;;
		  MELT_LOCATION ("warmelt-normatch.melt:2459:/ cond");
		  /*cond */ if ( /*_#MELT_NEED_DBG__L53*/ meltfnum[22])	/*then */
		    {
		      /*^cond.then */
		      /*^block */
		      /*anyblock */
		      {

	/*_#THE_MELTCALLCOUNT__L54*/ meltfnum[37] =
#ifdef meltcallcount
			  meltcallcount	/* the_meltcallcount */
#else
			  0L
#endif /* meltcallcount the_meltcallcount */
			  ;;
			MELT_LOCATION
			  ("warmelt-normatch.melt:2459:/ checksignal");
			MELT_CHECK_SIGNAL ();
			;
			/*^apply */
			/*apply */
			{
			  union meltparam_un argtab[5];
			  memset (&argtab, 0, sizeof (argtab));
			  /*^apply.arg */
			  argtab[0].meltbp_long =
			    /*_#THE_MELTCALLCOUNT__L54*/ meltfnum[37];
			  /*^apply.arg */
			  argtab[1].meltbp_cstring = "warmelt-normatch.melt";
			  /*^apply.arg */
			  argtab[2].meltbp_long = 2459;
			  /*^apply.arg */
			  argtab[3].meltbp_cstring =
			    "normexp_match initial oldtester";
			  /*^apply.arg */
			  argtab[4].meltbp_aptr =
			    (melt_ptr_t *) & /*_.OLDTESTER__V26*/
			    meltfptr[25];
			  /*_.MELT_DEBUG_FUN__V163*/ meltfptr[130] =
			    melt_apply ((meltclosure_ptr_t)
					(( /*!MELT_DEBUG_FUN */ meltfrout->
					  tabval[3])),
					(melt_ptr_t) (( /*nil */ NULL)),
					(MELTBPARSTR_LONG MELTBPARSTR_CSTRING
					 MELTBPARSTR_LONG MELTBPARSTR_CSTRING
					 MELTBPARSTR_PTR ""), argtab, "",
					(union meltparam_un *) 0);
			}
			;
			/*_.IF___V162*/ meltfptr[125] =
			  /*_.MELT_DEBUG_FUN__V163*/ meltfptr[130];;
			/*epilog */

			MELT_LOCATION ("warmelt-normatch.melt:2459:/ clear");
		  /*clear *//*_#THE_MELTCALLCOUNT__L54*/ meltfnum[37] =
			  0;
			/*^clear */
		  /*clear *//*_.MELT_DEBUG_FUN__V163*/ meltfptr[130] =
			  0;
		      }
		      ;
		    }
		  else
		    {		/*^cond.else */

       /*_.IF___V162*/ meltfptr[125] = NULL;;
		    }
		  ;
		  MELT_LOCATION ("warmelt-normatch.melt:2459:/ quasiblock");


		  /*_.PROGN___V164*/ meltfptr[144] =
		    /*_.IF___V162*/ meltfptr[125];;
		  /*^compute */
		  /*_.IFCPP___V161*/ meltfptr[149] =
		    /*_.PROGN___V164*/ meltfptr[144];;
		  /*epilog */

		  MELT_LOCATION ("warmelt-normatch.melt:2459:/ clear");
		/*clear *//*_#MELT_NEED_DBG__L53*/ meltfnum[22] = 0;
		  /*^clear */
		/*clear *//*_.IF___V162*/ meltfptr[125] = 0;
		  /*^clear */
		/*clear *//*_.PROGN___V164*/ meltfptr[144] = 0;
		}

#else /*MELT_HAVE_DEBUG */
		/*^cppif.else */
		/*_.IFCPP___V161*/ meltfptr[149] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
		;
		MELT_LOCATION ("warmelt-normatch.melt:2460:/ checksignal");
		MELT_CHECK_SIGNAL ();
		;
		/*^apply */
		/*apply */
		{
		  union meltparam_un argtab[1];
		  memset (&argtab, 0, sizeof (argtab));
		  /*^apply.arg */
		  argtab[0].meltbp_aptr =
		    (melt_ptr_t *) & /*_.OLDTESTER__V26*/ meltfptr[25];
		  /*_.SET_NEW_TESTER_ALL_ELSES__V165*/ meltfptr[135] =
		    melt_apply ((meltclosure_ptr_t)
				(( /*!SET_NEW_TESTER_ALL_ELSES */ meltfrout->
				  tabval[28])),
				(melt_ptr_t) ( /*_.CURTESTER__V120*/
					      meltfptr[119]),
				(MELTBPARSTR_PTR ""), argtab, "",
				(union meltparam_un *) 0);
		}
		;

#if MELT_HAVE_DEBUG
		MELT_LOCATION ("warmelt-normatch.melt:2461:/ cppif.then");
		/*^block */
		/*anyblock */
		{


		  {
		    /*^locexp */
		    /*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
		    melt_dbgcounter++;
#endif
		    ;
		  }
		  ;
		  /*^checksignal */
		  MELT_CHECK_SIGNAL ();
		  ;
      /*_#MELT_NEED_DBG__L55*/ meltfnum[42] =
		    /*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
		    ( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
		    0		/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
		    ;;
		  MELT_LOCATION ("warmelt-normatch.melt:2461:/ cond");
		  /*cond */ if ( /*_#MELT_NEED_DBG__L55*/ meltfnum[42])	/*then */
		    {
		      /*^cond.then */
		      /*^block */
		      /*anyblock */
		      {

	/*_#THE_MELTCALLCOUNT__L56*/ meltfnum[41] =
#ifdef meltcallcount
			  meltcallcount	/* the_meltcallcount */
#else
			  0L
#endif /* meltcallcount the_meltcallcount */
			  ;;
			MELT_LOCATION
			  ("warmelt-normatch.melt:2461:/ checksignal");
			MELT_CHECK_SIGNAL ();
			;
			/*^apply */
			/*apply */
			{
			  union meltparam_un argtab[5];
			  memset (&argtab, 0, sizeof (argtab));
			  /*^apply.arg */
			  argtab[0].meltbp_long =
			    /*_#THE_MELTCALLCOUNT__L56*/ meltfnum[41];
			  /*^apply.arg */
			  argtab[1].meltbp_cstring = "warmelt-normatch.melt";
			  /*^apply.arg */
			  argtab[2].meltbp_long = 2461;
			  /*^apply.arg */
			  argtab[3].meltbp_cstring =
			    "normexp_match final oldtester";
			  /*^apply.arg */
			  argtab[4].meltbp_aptr =
			    (melt_ptr_t *) & /*_.OLDTESTER__V26*/
			    meltfptr[25];
			  /*_.MELT_DEBUG_FUN__V168*/ meltfptr[144] =
			    melt_apply ((meltclosure_ptr_t)
					(( /*!MELT_DEBUG_FUN */ meltfrout->
					  tabval[3])),
					(melt_ptr_t) (( /*nil */ NULL)),
					(MELTBPARSTR_LONG MELTBPARSTR_CSTRING
					 MELTBPARSTR_LONG MELTBPARSTR_CSTRING
					 MELTBPARSTR_PTR ""), argtab, "",
					(union meltparam_un *) 0);
			}
			;
			/*_.IF___V167*/ meltfptr[125] =
			  /*_.MELT_DEBUG_FUN__V168*/ meltfptr[144];;
			/*epilog */

			MELT_LOCATION ("warmelt-normatch.melt:2461:/ clear");
		  /*clear *//*_#THE_MELTCALLCOUNT__L56*/ meltfnum[41] =
			  0;
			/*^clear */
		  /*clear *//*_.MELT_DEBUG_FUN__V168*/ meltfptr[144] =
			  0;
		      }
		      ;
		    }
		  else
		    {		/*^cond.else */

       /*_.IF___V167*/ meltfptr[125] = NULL;;
		    }
		  ;
		  MELT_LOCATION ("warmelt-normatch.melt:2461:/ quasiblock");


		  /*_.PROGN___V169*/ meltfptr[144] =
		    /*_.IF___V167*/ meltfptr[125];;
		  /*^compute */
		  /*_.IFCPP___V166*/ meltfptr[130] =
		    /*_.PROGN___V169*/ meltfptr[144];;
		  /*epilog */

		  MELT_LOCATION ("warmelt-normatch.melt:2461:/ clear");
		/*clear *//*_#MELT_NEED_DBG__L55*/ meltfnum[42] = 0;
		  /*^clear */
		/*clear *//*_.IF___V167*/ meltfptr[125] = 0;
		  /*^clear */
		/*clear *//*_.PROGN___V169*/ meltfptr[144] = 0;
		}

#else /*MELT_HAVE_DEBUG */
		/*^cppif.else */
		/*_.IFCPP___V166*/ meltfptr[130] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
		;
		MELT_LOCATION ("warmelt-normatch.melt:2458:/ quasiblock");


		/*_.PROGN___V170*/ meltfptr[125] =
		  /*_.IFCPP___V166*/ meltfptr[130];;
		/*^compute */
		/*_.IF___V160*/ meltfptr[129] =
		  /*_.PROGN___V170*/ meltfptr[125];;
		/*epilog */

		MELT_LOCATION ("warmelt-normatch.melt:2457:/ clear");
	      /*clear *//*_.IFCPP___V161*/ meltfptr[149] = 0;
		/*^clear */
	      /*clear *//*_.SET_NEW_TESTER_ALL_ELSES__V165*/ meltfptr[135] =
		  0;
		/*^clear */
	      /*clear *//*_.IFCPP___V166*/ meltfptr[130] = 0;
		/*^clear */
	      /*clear *//*_.PROGN___V170*/ meltfptr[125] = 0;
	      }
	      ;
	    }
	  else
	    {			/*^cond.else */

   /*_.IF___V160*/ meltfptr[129] = NULL;;
	    }
	  ;
	  MELT_LOCATION ("warmelt-normatch.melt:2464:/ compute");
	  /*_.OLDTESTER__V26*/ meltfptr[25] =
	    /*_.SETQ___V171*/ meltfptr[144] =
	    /*_.CURTESTER__V120*/ meltfptr[119];;

#if MELT_HAVE_DEBUG
	  MELT_LOCATION ("warmelt-normatch.melt:2465:/ cppif.then");
	  /*^block */
	  /*anyblock */
	  {


	    {
	      /*^locexp */
	      /*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	      melt_dbgcounter++;
#endif
	      ;
	    }
	    ;
	    /*^checksignal */
	    MELT_CHECK_SIGNAL ();
	    ;
    /*_#MELT_NEED_DBG__L57*/ meltfnum[37] =
	      /*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	      ( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	      0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	      ;;
	    MELT_LOCATION ("warmelt-normatch.melt:2465:/ cond");
	    /*cond */ if ( /*_#MELT_NEED_DBG__L57*/ meltfnum[37])	/*then */
	      {
		/*^cond.then */
		/*^block */
		/*anyblock */
		{

      /*_#THE_MELTCALLCOUNT__L58*/ meltfnum[22] =
#ifdef meltcallcount
		    meltcallcount	/* the_meltcallcount */
#else
		    0L
#endif /* meltcallcount the_meltcallcount */
		    ;;
		  MELT_LOCATION ("warmelt-normatch.melt:2465:/ checksignal");
		  MELT_CHECK_SIGNAL ();
		  ;
		  /*^apply */
		  /*apply */
		  {
		    union meltparam_un argtab[7];
		    memset (&argtab, 0, sizeof (argtab));
		    /*^apply.arg */
		    argtab[0].meltbp_long =
		      /*_#THE_MELTCALLCOUNT__L58*/ meltfnum[22];
		    /*^apply.arg */
		    argtab[1].meltbp_cstring = "warmelt-normatch.melt";
		    /*^apply.arg */
		    argtab[2].meltbp_long = 2465;
		    /*^apply.arg */
		    argtab[3].meltbp_cstring =
		      "normexp_match after lastesterloop pcn=";
		    /*^apply.arg */
		    argtab[4].meltbp_aptr =
		      (melt_ptr_t *) & /*_.PCN__V92*/ meltfptr[91];
		    /*^apply.arg */
		    argtab[5].meltbp_cstring = " pvarlocmap=";
		    /*^apply.arg */
		    argtab[6].meltbp_aptr =
		      (melt_ptr_t *) & /*_.PVARLOCMAP__V90*/ meltfptr[89];
		    /*_.MELT_DEBUG_FUN__V174*/ meltfptr[130] =
		      melt_apply ((meltclosure_ptr_t)
				  (( /*!MELT_DEBUG_FUN */ meltfrout->
				    tabval[3])),
				  (melt_ptr_t) (( /*nil */ NULL)),
				  (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_PTR MELTBPARSTR_CSTRING
				   MELTBPARSTR_PTR ""), argtab, "",
				  (union meltparam_un *) 0);
		  }
		  ;
		  /*_.IF___V173*/ meltfptr[135] =
		    /*_.MELT_DEBUG_FUN__V174*/ meltfptr[130];;
		  /*epilog */

		  MELT_LOCATION ("warmelt-normatch.melt:2465:/ clear");
		/*clear *//*_#THE_MELTCALLCOUNT__L58*/ meltfnum[22] = 0;
		  /*^clear */
		/*clear *//*_.MELT_DEBUG_FUN__V174*/ meltfptr[130] = 0;
		}
		;
	      }
	    else
	      {			/*^cond.else */

     /*_.IF___V173*/ meltfptr[135] = NULL;;
	      }
	    ;
	    MELT_LOCATION ("warmelt-normatch.melt:2465:/ quasiblock");


	    /*_.PROGN___V175*/ meltfptr[125] = /*_.IF___V173*/ meltfptr[135];;
	    /*^compute */
	    /*_.IFCPP___V172*/ meltfptr[149] =
	      /*_.PROGN___V175*/ meltfptr[125];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-normatch.melt:2465:/ clear");
	      /*clear *//*_#MELT_NEED_DBG__L57*/ meltfnum[37] = 0;
	    /*^clear */
	      /*clear *//*_.IF___V173*/ meltfptr[135] = 0;
	    /*^clear */
	      /*clear *//*_.PROGN___V175*/ meltfptr[125] = 0;
	  }

#else /*MELT_HAVE_DEBUG */
	  /*^cppif.else */
	  /*_.IFCPP___V172*/ meltfptr[149] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
	  ;
	  MELT_LOCATION ("warmelt-normatch.melt:2467:/ quasiblock");


	  /*^checksignal */
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^apply */
	  /*apply */
	  {
	    /*_.SORTEDVARS__V177*/ meltfptr[135] =
	      melt_apply ((meltclosure_ptr_t)
			  (( /*!MAPOBJECT_SORTED_ATTRIBUTE_TUPLE */
			    meltfrout->tabval[29])),
			  (melt_ptr_t) ( /*_.PVARLOCMAP__V90*/ meltfptr[89]),
			  (""), (union meltparam_un *) 0, "",
			  (union meltparam_un *) 0);
	  }
	  ;
  /*_#MULTIPLE_LENGTH__L59*/ meltfnum[41] =
	    (melt_multiple_length
	     ((melt_ptr_t) ( /*_.SORTEDVARS__V177*/ meltfptr[135])));;
	  /*^compute */
  /*_.SORTEDBINDINGS__V178*/ meltfptr[125] =
	    (meltgc_new_multiple
	     ((meltobject_ptr_t)
	      (( /*!DISCR_MULTIPLE */ meltfrout->tabval[4])),
	      ( /*_#MULTIPLE_LENGTH__L59*/ meltfnum[41])));;
	  MELT_LOCATION ("warmelt-normatch.melt:2469:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^apply */
	  /*apply */
	  {
	    /*_.FRESHNEWENV__V179*/ meltfptr[178] =
	      melt_apply ((meltclosure_ptr_t)
			  (( /*!FRESH_ENV */ meltfrout->tabval[30])),
			  (melt_ptr_t) ( /*_.NEWENV__V119*/ meltfptr[115]),
			  (""), (union meltparam_un *) 0, "",
			  (union meltparam_un *) 0);
	  }
	  ;

#if MELT_HAVE_DEBUG
	  MELT_LOCATION ("warmelt-normatch.melt:2471:/ cppif.then");
	  /*^block */
	  /*anyblock */
	  {


	    {
	      /*^locexp */
	      /*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	      melt_dbgcounter++;
#endif
	      ;
	    }
	    ;
	    /*^checksignal */
	    MELT_CHECK_SIGNAL ();
	    ;
    /*_#MELT_NEED_DBG__L60*/ meltfnum[42] =
	      /*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	      ( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	      0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	      ;;
	    MELT_LOCATION ("warmelt-normatch.melt:2471:/ cond");
	    /*cond */ if ( /*_#MELT_NEED_DBG__L60*/ meltfnum[42])	/*then */
	      {
		/*^cond.then */
		/*^block */
		/*anyblock */
		{

      /*_#THE_MELTCALLCOUNT__L61*/ meltfnum[22] =
#ifdef meltcallcount
		    meltcallcount	/* the_meltcallcount */
#else
		    0L
#endif /* meltcallcount the_meltcallcount */
		    ;;
		  MELT_LOCATION ("warmelt-normatch.melt:2471:/ checksignal");
		  MELT_CHECK_SIGNAL ();
		  ;
		  /*^apply */
		  /*apply */
		  {
		    union meltparam_un argtab[5];
		    memset (&argtab, 0, sizeof (argtab));
		    /*^apply.arg */
		    argtab[0].meltbp_long =
		      /*_#THE_MELTCALLCOUNT__L61*/ meltfnum[22];
		    /*^apply.arg */
		    argtab[1].meltbp_cstring = "warmelt-normatch.melt";
		    /*^apply.arg */
		    argtab[2].meltbp_long = 2471;
		    /*^apply.arg */
		    argtab[3].meltbp_cstring = "normexp_match sortedvars";
		    /*^apply.arg */
		    argtab[4].meltbp_aptr =
		      (melt_ptr_t *) & /*_.SORTEDVARS__V177*/ meltfptr[135];
		    /*_.MELT_DEBUG_FUN__V182*/ meltfptr[181] =
		      melt_apply ((meltclosure_ptr_t)
				  (( /*!MELT_DEBUG_FUN */ meltfrout->
				    tabval[3])),
				  (melt_ptr_t) (( /*nil */ NULL)),
				  (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_PTR ""), argtab, "",
				  (union meltparam_un *) 0);
		  }
		  ;
		  /*_.IF___V181*/ meltfptr[180] =
		    /*_.MELT_DEBUG_FUN__V182*/ meltfptr[181];;
		  /*epilog */

		  MELT_LOCATION ("warmelt-normatch.melt:2471:/ clear");
		/*clear *//*_#THE_MELTCALLCOUNT__L61*/ meltfnum[22] = 0;
		  /*^clear */
		/*clear *//*_.MELT_DEBUG_FUN__V182*/ meltfptr[181] = 0;
		}
		;
	      }
	    else
	      {			/*^cond.else */

     /*_.IF___V181*/ meltfptr[180] = NULL;;
	      }
	    ;
	    MELT_LOCATION ("warmelt-normatch.melt:2471:/ quasiblock");


	    /*_.PROGN___V183*/ meltfptr[181] = /*_.IF___V181*/ meltfptr[180];;
	    /*^compute */
	    /*_.IFCPP___V180*/ meltfptr[179] =
	      /*_.PROGN___V183*/ meltfptr[181];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-normatch.melt:2471:/ clear");
	      /*clear *//*_#MELT_NEED_DBG__L60*/ meltfnum[42] = 0;
	    /*^clear */
	      /*clear *//*_.IF___V181*/ meltfptr[180] = 0;
	    /*^clear */
	      /*clear *//*_.PROGN___V183*/ meltfptr[181] = 0;
	  }

#else /*MELT_HAVE_DEBUG */
	  /*^cppif.else */
	  /*_.IFCPP___V180*/ meltfptr[179] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
	  ;
	  /*citerblock FOREACH_IN_MULTIPLE */
	  {
	    /* start foreach_in_multiple meltcit2__EACHTUP */
	    long meltcit2__EACHTUP_ln =
	      melt_multiple_length ((melt_ptr_t) /*_.SORTEDVARS__V177*/
				    meltfptr[135]);
	    for ( /*_#SVIX__L62*/ meltfnum[37] = 0;
		 ( /*_#SVIX__L62*/ meltfnum[37] >= 0)
		 && ( /*_#SVIX__L62*/ meltfnum[37] < meltcit2__EACHTUP_ln);
	/*_#SVIX__L62*/ meltfnum[37]++)
	      {
		/*_.SVAR__V184*/ meltfptr[180] =
		  melt_multiple_nth ((melt_ptr_t)
				     ( /*_.SORTEDVARS__V177*/ meltfptr[135]),
				     /*_#SVIX__L62*/ meltfnum[37]);




#if MELT_HAVE_DEBUG
		MELT_LOCATION ("warmelt-normatch.melt:2476:/ cppif.then");
		/*^block */
		/*anyblock */
		{


		  {
		    /*^locexp */
		    /*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
		    melt_dbgcounter++;
#endif
		    ;
		  }
		  ;
		  /*^checksignal */
		  MELT_CHECK_SIGNAL ();
		  ;
     /*_#MELT_NEED_DBG__L63*/ meltfnum[22] =
		    /*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
		    ( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
		    0		/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
		    ;;
		  MELT_LOCATION ("warmelt-normatch.melt:2476:/ cond");
		  /*cond */ if ( /*_#MELT_NEED_DBG__L63*/ meltfnum[22])	/*then */
		    {
		      /*^cond.then */
		      /*^block */
		      /*anyblock */
		      {

       /*_#THE_MELTCALLCOUNT__L64*/ meltfnum[42] =
#ifdef meltcallcount
			  meltcallcount	/* the_meltcallcount */
#else
			  0L
#endif /* meltcallcount the_meltcallcount */
			  ;;
			MELT_LOCATION
			  ("warmelt-normatch.melt:2476:/ checksignal");
			MELT_CHECK_SIGNAL ();
			;
			/*^apply */
			/*apply */
			{
			  union meltparam_un argtab[5];
			  memset (&argtab, 0, sizeof (argtab));
			  /*^apply.arg */
			  argtab[0].meltbp_long =
			    /*_#THE_MELTCALLCOUNT__L64*/ meltfnum[42];
			  /*^apply.arg */
			  argtab[1].meltbp_cstring = "warmelt-normatch.melt";
			  /*^apply.arg */
			  argtab[2].meltbp_long = 2476;
			  /*^apply.arg */
			  argtab[3].meltbp_cstring = "normexp_match svar";
			  /*^apply.arg */
			  argtab[4].meltbp_aptr =
			    (melt_ptr_t *) & /*_.SVAR__V184*/ meltfptr[180];
			  /*_.MELT_DEBUG_FUN__V187*/ meltfptr[186] =
			    melt_apply ((meltclosure_ptr_t)
					(( /*!MELT_DEBUG_FUN */ meltfrout->
					  tabval[3])),
					(melt_ptr_t) (( /*nil */ NULL)),
					(MELTBPARSTR_LONG MELTBPARSTR_CSTRING
					 MELTBPARSTR_LONG MELTBPARSTR_CSTRING
					 MELTBPARSTR_PTR ""), argtab, "",
					(union meltparam_un *) 0);
			}
			;
			/*_.IF___V186*/ meltfptr[185] =
			  /*_.MELT_DEBUG_FUN__V187*/ meltfptr[186];;
			/*epilog */

			MELT_LOCATION ("warmelt-normatch.melt:2476:/ clear");
		 /*clear *//*_#THE_MELTCALLCOUNT__L64*/ meltfnum[42] =
			  0;
			/*^clear */
		 /*clear *//*_.MELT_DEBUG_FUN__V187*/ meltfptr[186] =
			  0;
		      }
		      ;
		    }
		  else
		    {		/*^cond.else */

      /*_.IF___V186*/ meltfptr[185] = NULL;;
		    }
		  ;
		  MELT_LOCATION ("warmelt-normatch.melt:2476:/ quasiblock");


		  /*_.PROGN___V188*/ meltfptr[186] =
		    /*_.IF___V186*/ meltfptr[185];;
		  /*^compute */
		  /*_.IFCPP___V185*/ meltfptr[181] =
		    /*_.PROGN___V188*/ meltfptr[186];;
		  /*epilog */

		  MELT_LOCATION ("warmelt-normatch.melt:2476:/ clear");
	       /*clear *//*_#MELT_NEED_DBG__L63*/ meltfnum[22] = 0;
		  /*^clear */
	       /*clear *//*_.IF___V186*/ meltfptr[185] = 0;
		  /*^clear */
	       /*clear *//*_.PROGN___V188*/ meltfptr[186] = 0;
		}

#else /*MELT_HAVE_DEBUG */
		/*^cppif.else */
		/*_.IFCPP___V185*/ meltfptr[181] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
		;
		MELT_LOCATION ("warmelt-normatch.melt:2477:/ quasiblock");


   /*_.SOCC__V190*/ meltfptr[186] =
		  /*mapobject_get */
		  melt_get_mapobjects ((meltmapobjects_ptr_t)
				       ( /*_.PVARLOCMAP__V90*/ meltfptr[89]),
				       (meltobject_ptr_t) ( /*_.SVAR__V184*/
							   meltfptr[180]));;

#if MELT_HAVE_DEBUG
		MELT_LOCATION ("warmelt-normatch.melt:2479:/ cppif.then");
		/*^block */
		/*anyblock */
		{


		  {
		    /*^locexp */
		    /*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
		    melt_dbgcounter++;
#endif
		    ;
		  }
		  ;
		  /*^checksignal */
		  MELT_CHECK_SIGNAL ();
		  ;
     /*_#MELT_NEED_DBG__L65*/ meltfnum[42] =
		    /*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
		    ( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
		    0		/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
		    ;;
		  MELT_LOCATION ("warmelt-normatch.melt:2479:/ cond");
		  /*cond */ if ( /*_#MELT_NEED_DBG__L65*/ meltfnum[42])	/*then */
		    {
		      /*^cond.then */
		      /*^block */
		      /*anyblock */
		      {

       /*_#THE_MELTCALLCOUNT__L66*/ meltfnum[22] =
#ifdef meltcallcount
			  meltcallcount	/* the_meltcallcount */
#else
			  0L
#endif /* meltcallcount the_meltcallcount */
			  ;;
			MELT_LOCATION
			  ("warmelt-normatch.melt:2479:/ checksignal");
			MELT_CHECK_SIGNAL ();
			;
			/*^apply */
			/*apply */
			{
			  union meltparam_un argtab[5];
			  memset (&argtab, 0, sizeof (argtab));
			  /*^apply.arg */
			  argtab[0].meltbp_long =
			    /*_#THE_MELTCALLCOUNT__L66*/ meltfnum[22];
			  /*^apply.arg */
			  argtab[1].meltbp_cstring = "warmelt-normatch.melt";
			  /*^apply.arg */
			  argtab[2].meltbp_long = 2479;
			  /*^apply.arg */
			  argtab[3].meltbp_cstring = "normexp_match socc";
			  /*^apply.arg */
			  argtab[4].meltbp_aptr =
			    (melt_ptr_t *) & /*_.SOCC__V190*/ meltfptr[186];
			  /*_.MELT_DEBUG_FUN__V193*/ meltfptr[192] =
			    melt_apply ((meltclosure_ptr_t)
					(( /*!MELT_DEBUG_FUN */ meltfrout->
					  tabval[3])),
					(melt_ptr_t) (( /*nil */ NULL)),
					(MELTBPARSTR_LONG MELTBPARSTR_CSTRING
					 MELTBPARSTR_LONG MELTBPARSTR_CSTRING
					 MELTBPARSTR_PTR ""), argtab, "",
					(union meltparam_un *) 0);
			}
			;
			/*_.IF___V192*/ meltfptr[191] =
			  /*_.MELT_DEBUG_FUN__V193*/ meltfptr[192];;
			/*epilog */

			MELT_LOCATION ("warmelt-normatch.melt:2479:/ clear");
		 /*clear *//*_#THE_MELTCALLCOUNT__L66*/ meltfnum[22] =
			  0;
			/*^clear */
		 /*clear *//*_.MELT_DEBUG_FUN__V193*/ meltfptr[192] =
			  0;
		      }
		      ;
		    }
		  else
		    {		/*^cond.else */

      /*_.IF___V192*/ meltfptr[191] = NULL;;
		    }
		  ;
		  MELT_LOCATION ("warmelt-normatch.melt:2479:/ quasiblock");


		  /*_.PROGN___V194*/ meltfptr[192] =
		    /*_.IF___V192*/ meltfptr[191];;
		  /*^compute */
		  /*_.IFCPP___V191*/ meltfptr[190] =
		    /*_.PROGN___V194*/ meltfptr[192];;
		  /*epilog */

		  MELT_LOCATION ("warmelt-normatch.melt:2479:/ clear");
	       /*clear *//*_#MELT_NEED_DBG__L65*/ meltfnum[42] = 0;
		  /*^clear */
	       /*clear *//*_.IF___V192*/ meltfptr[191] = 0;
		  /*^clear */
	       /*clear *//*_.PROGN___V194*/ meltfptr[192] = 0;
		}

#else /*MELT_HAVE_DEBUG */
		/*^cppif.else */
		/*_.IFCPP___V191*/ meltfptr[190] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
		;

#if MELT_HAVE_DEBUG
		MELT_LOCATION ("warmelt-normatch.melt:2480:/ cppif.then");
		/*^block */
		/*anyblock */
		{

		  /*^checksignal */
		  MELT_CHECK_SIGNAL ();
		  ;
     /*_#IS_A__L67*/ meltfnum[22] =
		    melt_is_instance_of ((melt_ptr_t)
					 ( /*_.SOCC__V190*/ meltfptr[186]),
					 (melt_ptr_t) (( /*!CLASS_NREP_LOCSYMOCC */ meltfrout->tabval[15])));;
		  MELT_LOCATION ("warmelt-normatch.melt:2480:/ cond");
		  /*cond */ if ( /*_#IS_A__L67*/ meltfnum[22])	/*then */
		    {
		      /*^cond.then */
		      /*_.IFELSE___V196*/ meltfptr[192] = ( /*nil */ NULL);;
		    }
		  else
		    {
		      MELT_LOCATION
			("warmelt-normatch.melt:2480:/ cond.else");

		      /*^block */
		      /*anyblock */
		      {




			{
			  /*^locexp */
			  melt_assert_failed (("check socc"),
					      ("warmelt-normatch.melt")
					      ? ("warmelt-normatch.melt") :
					      __FILE__,
					      (2480) ? (2480) : __LINE__,
					      __FUNCTION__);
			  ;
			}
			;
		 /*clear *//*_.IFELSE___V196*/ meltfptr[192] = 0;
			/*epilog */
		      }
		      ;
		    }
		  ;
		  /*^compute */
		  /*_.IFCPP___V195*/ meltfptr[191] =
		    /*_.IFELSE___V196*/ meltfptr[192];;
		  /*epilog */

		  MELT_LOCATION ("warmelt-normatch.melt:2480:/ clear");
	       /*clear *//*_#IS_A__L67*/ meltfnum[22] = 0;
		  /*^clear */
	       /*clear *//*_.IFELSE___V196*/ meltfptr[192] = 0;
		}

#else /*MELT_HAVE_DEBUG */
		/*^cppif.else */
		/*_.IFCPP___V195*/ meltfptr[191] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
		;
		MELT_LOCATION ("warmelt-normatch.melt:2481:/ quasiblock");


		/*^getslot */
		{
		  melt_ptr_t slot = NULL, obj = NULL;
		  obj =
		    (melt_ptr_t) ( /*_.SOCC__V190*/ meltfptr[186]) /*=obj*/ ;
		  melt_object_get_field (slot, obj, 3, "NOCC_BIND");
    /*_.SBIND__V198*/ meltfptr[197] = slot;
		};
		;

#if MELT_HAVE_DEBUG
		MELT_LOCATION ("warmelt-normatch.melt:2482:/ cppif.then");
		/*^block */
		/*anyblock */
		{


		  {
		    /*^locexp */
		    /*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
		    melt_dbgcounter++;
#endif
		    ;
		  }
		  ;
		  /*^checksignal */
		  MELT_CHECK_SIGNAL ();
		  ;
     /*_#MELT_NEED_DBG__L68*/ meltfnum[42] =
		    /*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
		    ( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
		    0		/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
		    ;;
		  MELT_LOCATION ("warmelt-normatch.melt:2482:/ cond");
		  /*cond */ if ( /*_#MELT_NEED_DBG__L68*/ meltfnum[42])	/*then */
		    {
		      /*^cond.then */
		      /*^block */
		      /*anyblock */
		      {

       /*_#THE_MELTCALLCOUNT__L69*/ meltfnum[22] =
#ifdef meltcallcount
			  meltcallcount	/* the_meltcallcount */
#else
			  0L
#endif /* meltcallcount the_meltcallcount */
			  ;;
			MELT_LOCATION
			  ("warmelt-normatch.melt:2482:/ checksignal");
			MELT_CHECK_SIGNAL ();
			;
			/*^apply */
			/*apply */
			{
			  union meltparam_un argtab[5];
			  memset (&argtab, 0, sizeof (argtab));
			  /*^apply.arg */
			  argtab[0].meltbp_long =
			    /*_#THE_MELTCALLCOUNT__L69*/ meltfnum[22];
			  /*^apply.arg */
			  argtab[1].meltbp_cstring = "warmelt-normatch.melt";
			  /*^apply.arg */
			  argtab[2].meltbp_long = 2482;
			  /*^apply.arg */
			  argtab[3].meltbp_cstring = "normexp_match sbind";
			  /*^apply.arg */
			  argtab[4].meltbp_aptr =
			    (melt_ptr_t *) & /*_.SBIND__V198*/ meltfptr[197];
			  /*_.MELT_DEBUG_FUN__V201*/ meltfptr[200] =
			    melt_apply ((meltclosure_ptr_t)
					(( /*!MELT_DEBUG_FUN */ meltfrout->
					  tabval[3])),
					(melt_ptr_t) (( /*nil */ NULL)),
					(MELTBPARSTR_LONG MELTBPARSTR_CSTRING
					 MELTBPARSTR_LONG MELTBPARSTR_CSTRING
					 MELTBPARSTR_PTR ""), argtab, "",
					(union meltparam_un *) 0);
			}
			;
			/*_.IF___V200*/ meltfptr[199] =
			  /*_.MELT_DEBUG_FUN__V201*/ meltfptr[200];;
			/*epilog */

			MELT_LOCATION ("warmelt-normatch.melt:2482:/ clear");
		 /*clear *//*_#THE_MELTCALLCOUNT__L69*/ meltfnum[22] =
			  0;
			/*^clear */
		 /*clear *//*_.MELT_DEBUG_FUN__V201*/ meltfptr[200] =
			  0;
		      }
		      ;
		    }
		  else
		    {		/*^cond.else */

      /*_.IF___V200*/ meltfptr[199] = NULL;;
		    }
		  ;
		  MELT_LOCATION ("warmelt-normatch.melt:2482:/ quasiblock");


		  /*_.PROGN___V202*/ meltfptr[200] =
		    /*_.IF___V200*/ meltfptr[199];;
		  /*^compute */
		  /*_.IFCPP___V199*/ meltfptr[198] =
		    /*_.PROGN___V202*/ meltfptr[200];;
		  /*epilog */

		  MELT_LOCATION ("warmelt-normatch.melt:2482:/ clear");
	       /*clear *//*_#MELT_NEED_DBG__L68*/ meltfnum[42] = 0;
		  /*^clear */
	       /*clear *//*_.IF___V200*/ meltfptr[199] = 0;
		  /*^clear */
	       /*clear *//*_.PROGN___V202*/ meltfptr[200] = 0;
		}

#else /*MELT_HAVE_DEBUG */
		/*^cppif.else */
		/*_.IFCPP___V199*/ meltfptr[198] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
		;

		{
		  MELT_LOCATION ("warmelt-normatch.melt:2483:/ locexp");
		  meltgc_multiple_put_nth ((melt_ptr_t)
					   ( /*_.SORTEDBINDINGS__V178*/
					    meltfptr[125]),
					   ( /*_#SVIX__L62*/ meltfnum[37]),
					   (melt_ptr_t) ( /*_.SBIND__V198*/
							 meltfptr[197]));
		}
		;
		MELT_LOCATION ("warmelt-normatch.melt:2484:/ checksignal");
		MELT_CHECK_SIGNAL ();
		;
		/*^apply */
		/*apply */
		{
		  union meltparam_un argtab[1];
		  memset (&argtab, 0, sizeof (argtab));
		  /*^apply.arg */
		  argtab[0].meltbp_aptr =
		    (melt_ptr_t *) & /*_.SBIND__V198*/ meltfptr[197];
		  /*_.PUT_ENV__V203*/ meltfptr[199] =
		    melt_apply ((meltclosure_ptr_t)
				(( /*!PUT_ENV */ meltfrout->tabval[31])),
				(melt_ptr_t) ( /*_.FRESHNEWENV__V179*/
					      meltfptr[178]),
				(MELTBPARSTR_PTR ""), argtab, "",
				(union meltparam_un *) 0);
		}
		;
		/*_.LET___V197*/ meltfptr[192] =
		  /*_.PUT_ENV__V203*/ meltfptr[199];;

		MELT_LOCATION ("warmelt-normatch.melt:2481:/ clear");
	     /*clear *//*_.SBIND__V198*/ meltfptr[197] = 0;
		/*^clear */
	     /*clear *//*_.IFCPP___V199*/ meltfptr[198] = 0;
		/*^clear */
	     /*clear *//*_.PUT_ENV__V203*/ meltfptr[199] = 0;
		/*_.LET___V189*/ meltfptr[185] =
		  /*_.LET___V197*/ meltfptr[192];;

		MELT_LOCATION ("warmelt-normatch.melt:2477:/ clear");
	     /*clear *//*_.SOCC__V190*/ meltfptr[186] = 0;
		/*^clear */
	     /*clear *//*_.IFCPP___V191*/ meltfptr[190] = 0;
		/*^clear */
	     /*clear *//*_.IFCPP___V195*/ meltfptr[191] = 0;
		/*^clear */
	     /*clear *//*_.LET___V197*/ meltfptr[192] = 0;
		if ( /*_#SVIX__L62*/ meltfnum[37] < 0)
		  break;
	      }			/* end  foreach_in_multiple meltcit2__EACHTUP */

	    /*citerepilog */

	    MELT_LOCATION ("warmelt-normatch.melt:2473:/ clear");
	     /*clear *//*_.SVAR__V184*/ meltfptr[180] = 0;
	    /*^clear */
	     /*clear *//*_#SVIX__L62*/ meltfnum[37] = 0;
	    /*^clear */
	     /*clear *//*_.IFCPP___V185*/ meltfptr[181] = 0;
	    /*^clear */
	     /*clear *//*_.LET___V189*/ meltfptr[185] = 0;
	  }			/*endciterblock FOREACH_IN_MULTIPLE */
	  ;

#if MELT_HAVE_DEBUG
	  MELT_LOCATION ("warmelt-normatch.melt:2487:/ cppif.then");
	  /*^block */
	  /*anyblock */
	  {


	    {
	      /*^locexp */
	      /*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	      melt_dbgcounter++;
#endif
	      ;
	    }
	    ;
	    /*^checksignal */
	    MELT_CHECK_SIGNAL ();
	    ;
    /*_#MELT_NEED_DBG__L70*/ meltfnum[22] =
	      /*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	      ( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	      0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	      ;;
	    MELT_LOCATION ("warmelt-normatch.melt:2487:/ cond");
	    /*cond */ if ( /*_#MELT_NEED_DBG__L70*/ meltfnum[22])	/*then */
	      {
		/*^cond.then */
		/*^block */
		/*anyblock */
		{

      /*_#THE_MELTCALLCOUNT__L71*/ meltfnum[42] =
#ifdef meltcallcount
		    meltcallcount	/* the_meltcallcount */
#else
		    0L
#endif /* meltcallcount the_meltcallcount */
		    ;;
		  MELT_LOCATION ("warmelt-normatch.melt:2487:/ checksignal");
		  MELT_CHECK_SIGNAL ();
		  ;
		  /*^apply */
		  /*apply */
		  {
		    union meltparam_un argtab[7];
		    memset (&argtab, 0, sizeof (argtab));
		    /*^apply.arg */
		    argtab[0].meltbp_long =
		      /*_#THE_MELTCALLCOUNT__L71*/ meltfnum[42];
		    /*^apply.arg */
		    argtab[1].meltbp_cstring = "warmelt-normatch.melt";
		    /*^apply.arg */
		    argtab[2].meltbp_long = 2487;
		    /*^apply.arg */
		    argtab[3].meltbp_cstring = "normexp_match freshnewenv=";
		    /*^apply.arg */
		    argtab[4].meltbp_aptr =
		      (melt_ptr_t *) & /*_.FRESHNEWENV__V179*/ meltfptr[178];
		    /*^apply.arg */
		    argtab[5].meltbp_cstring = " sortedbindings=";
		    /*^apply.arg */
		    argtab[6].meltbp_aptr =
		      (melt_ptr_t *) & /*_.SORTEDBINDINGS__V178*/
		      meltfptr[125];
		    /*_.MELT_DEBUG_FUN__V206*/ meltfptr[198] =
		      melt_apply ((meltclosure_ptr_t)
				  (( /*!MELT_DEBUG_FUN */ meltfrout->
				    tabval[3])),
				  (melt_ptr_t) (( /*nil */ NULL)),
				  (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_PTR MELTBPARSTR_CSTRING
				   MELTBPARSTR_PTR ""), argtab, "",
				  (union meltparam_un *) 0);
		  }
		  ;
		  /*_.IF___V205*/ meltfptr[197] =
		    /*_.MELT_DEBUG_FUN__V206*/ meltfptr[198];;
		  /*epilog */

		  MELT_LOCATION ("warmelt-normatch.melt:2487:/ clear");
		/*clear *//*_#THE_MELTCALLCOUNT__L71*/ meltfnum[42] = 0;
		  /*^clear */
		/*clear *//*_.MELT_DEBUG_FUN__V206*/ meltfptr[198] = 0;
		}
		;
	      }
	    else
	      {			/*^cond.else */

     /*_.IF___V205*/ meltfptr[197] = NULL;;
	      }
	    ;
	    MELT_LOCATION ("warmelt-normatch.melt:2487:/ quasiblock");


	    /*_.PROGN___V207*/ meltfptr[199] = /*_.IF___V205*/ meltfptr[197];;
	    /*^compute */
	    /*_.IFCPP___V204*/ meltfptr[200] =
	      /*_.PROGN___V207*/ meltfptr[199];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-normatch.melt:2487:/ clear");
	      /*clear *//*_#MELT_NEED_DBG__L70*/ meltfnum[22] = 0;
	    /*^clear */
	      /*clear *//*_.IF___V205*/ meltfptr[197] = 0;
	    /*^clear */
	      /*clear *//*_.PROGN___V207*/ meltfptr[199] = 0;
	  }

#else /*MELT_HAVE_DEBUG */
	  /*^cppif.else */
	  /*_.IFCPP___V204*/ meltfptr[200] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
	  ;

#if MELT_HAVE_DEBUG
	  MELT_LOCATION ("warmelt-normatch.melt:2489:/ cppif.then");
	  /*^block */
	  /*anyblock */
	  {

	    /*^checksignal */
	    MELT_CHECK_SIGNAL ();
	    ;
    /*_#IS_MULTIPLE__L72*/ meltfnum[42] =
	      (melt_magic_discr
	       ((melt_ptr_t) ( /*_.SORTEDBINDINGS__V178*/ meltfptr[125])) ==
	       MELTOBMAG_MULTIPLE);;
	    MELT_LOCATION ("warmelt-normatch.melt:2489:/ cond");
	    /*cond */ if ( /*_#IS_MULTIPLE__L72*/ meltfnum[42])	/*then */
	      {
		/*^cond.then */
		/*_.IFELSE___V209*/ meltfptr[190] = ( /*nil */ NULL);;
	      }
	    else
	      {
		MELT_LOCATION ("warmelt-normatch.melt:2489:/ cond.else");

		/*^block */
		/*anyblock */
		{




		  {
		    /*^locexp */
		    melt_assert_failed (("check sortedbindings"),
					("warmelt-normatch.melt")
					? ("warmelt-normatch.melt") :
					__FILE__, (2489) ? (2489) : __LINE__,
					__FUNCTION__);
		    ;
		  }
		  ;
		/*clear *//*_.IFELSE___V209*/ meltfptr[190] = 0;
		  /*epilog */
		}
		;
	      }
	    ;
	    /*^compute */
	    /*_.IFCPP___V208*/ meltfptr[186] =
	      /*_.IFELSE___V209*/ meltfptr[190];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-normatch.melt:2489:/ clear");
	      /*clear *//*_#IS_MULTIPLE__L72*/ meltfnum[42] = 0;
	    /*^clear */
	      /*clear *//*_.IFELSE___V209*/ meltfptr[190] = 0;
	  }

#else /*MELT_HAVE_DEBUG */
	  /*^cppif.else */
	  /*_.IFCPP___V208*/ meltfptr[186] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
	  ;
	  MELT_LOCATION ("warmelt-normatch.melt:2490:/ quasiblock");


  /*_.SUBINDLIST__V211*/ meltfptr[192] =
	    (meltgc_new_list
	     ((meltobject_ptr_t)
	      (( /*!DISCR_LIST */ meltfrout->tabval[6]))));;
	  /*citerblock FOREACH_IN_MULTIPLE */
	  {
	    /* start foreach_in_multiple meltcit3__EACHTUP */
	    long meltcit3__EACHTUP_ln =
	      melt_multiple_length ((melt_ptr_t) /*_.SORTEDBINDINGS__V178*/
				    meltfptr[125]);
	    for ( /*_#SORTBIX__L73*/ meltfnum[22] = 0;
		 ( /*_#SORTBIX__L73*/ meltfnum[22] >= 0)
		 && ( /*_#SORTBIX__L73*/ meltfnum[22] < meltcit3__EACHTUP_ln);
	/*_#SORTBIX__L73*/ meltfnum[22]++)
	      {
		/*_.CURSORTBIND__V212*/ meltfptr[198] =
		  melt_multiple_nth ((melt_ptr_t)
				     ( /*_.SORTEDBINDINGS__V178*/
				      meltfptr[125]), /*_#SORTBIX__L73*/
				     meltfnum[22]);




#if MELT_HAVE_DEBUG
		MELT_LOCATION ("warmelt-normatch.melt:2495:/ cppif.then");
		/*^block */
		/*anyblock */
		{


		  {
		    /*^locexp */
		    /*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
		    melt_dbgcounter++;
#endif
		    ;
		  }
		  ;
		  /*^checksignal */
		  MELT_CHECK_SIGNAL ();
		  ;
     /*_#MELT_NEED_DBG__L74*/ meltfnum[42] =
		    /*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
		    ( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
		    0		/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
		    ;;
		  MELT_LOCATION ("warmelt-normatch.melt:2495:/ cond");
		  /*cond */ if ( /*_#MELT_NEED_DBG__L74*/ meltfnum[42])	/*then */
		    {
		      /*^cond.then */
		      /*^block */
		      /*anyblock */
		      {

       /*_#THE_MELTCALLCOUNT__L75*/ meltfnum[74] =
#ifdef meltcallcount
			  meltcallcount	/* the_meltcallcount */
#else
			  0L
#endif /* meltcallcount the_meltcallcount */
			  ;;
			MELT_LOCATION
			  ("warmelt-normatch.melt:2495:/ checksignal");
			MELT_CHECK_SIGNAL ();
			;
			/*^apply */
			/*apply */
			{
			  union meltparam_un argtab[5];
			  memset (&argtab, 0, sizeof (argtab));
			  /*^apply.arg */
			  argtab[0].meltbp_long =
			    /*_#THE_MELTCALLCOUNT__L75*/ meltfnum[74];
			  /*^apply.arg */
			  argtab[1].meltbp_cstring = "warmelt-normatch.melt";
			  /*^apply.arg */
			  argtab[2].meltbp_long = 2495;
			  /*^apply.arg */
			  argtab[3].meltbp_cstring =
			    "normexp_match cursortbind";
			  /*^apply.arg */
			  argtab[4].meltbp_aptr =
			    (melt_ptr_t *) & /*_.CURSORTBIND__V212*/
			    meltfptr[198];
			  /*_.MELT_DEBUG_FUN__V215*/ meltfptr[190] =
			    melt_apply ((meltclosure_ptr_t)
					(( /*!MELT_DEBUG_FUN */ meltfrout->
					  tabval[3])),
					(melt_ptr_t) (( /*nil */ NULL)),
					(MELTBPARSTR_LONG MELTBPARSTR_CSTRING
					 MELTBPARSTR_LONG MELTBPARSTR_CSTRING
					 MELTBPARSTR_PTR ""), argtab, "",
					(union meltparam_un *) 0);
			}
			;
			/*_.IF___V214*/ meltfptr[199] =
			  /*_.MELT_DEBUG_FUN__V215*/ meltfptr[190];;
			/*epilog */

			MELT_LOCATION ("warmelt-normatch.melt:2495:/ clear");
		 /*clear *//*_#THE_MELTCALLCOUNT__L75*/ meltfnum[74] =
			  0;
			/*^clear */
		 /*clear *//*_.MELT_DEBUG_FUN__V215*/ meltfptr[190] =
			  0;
		      }
		      ;
		    }
		  else
		    {		/*^cond.else */

      /*_.IF___V214*/ meltfptr[199] = NULL;;
		    }
		  ;
		  MELT_LOCATION ("warmelt-normatch.melt:2495:/ quasiblock");


		  /*_.PROGN___V216*/ meltfptr[190] =
		    /*_.IF___V214*/ meltfptr[199];;
		  /*^compute */
		  /*_.IFCPP___V213*/ meltfptr[197] =
		    /*_.PROGN___V216*/ meltfptr[190];;
		  /*epilog */

		  MELT_LOCATION ("warmelt-normatch.melt:2495:/ clear");
	       /*clear *//*_#MELT_NEED_DBG__L74*/ meltfnum[42] = 0;
		  /*^clear */
	       /*clear *//*_.IF___V214*/ meltfptr[199] = 0;
		  /*^clear */
	       /*clear *//*_.PROGN___V216*/ meltfptr[190] = 0;
		}

#else /*MELT_HAVE_DEBUG */
		/*^cppif.else */
		/*_.IFCPP___V213*/ meltfptr[197] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
		;

#if MELT_HAVE_DEBUG
		MELT_LOCATION ("warmelt-normatch.melt:2496:/ cppif.then");
		/*^block */
		/*anyblock */
		{

		  /*^checksignal */
		  MELT_CHECK_SIGNAL ();
		  ;
     /*_#IS_A__L76*/ meltfnum[74] =
		    melt_is_instance_of ((melt_ptr_t)
					 ( /*_.CURSORTBIND__V212*/
					  meltfptr[198]),
					 (melt_ptr_t) (( /*!CLASS_NORMAL_LET_BINDING */ meltfrout->tabval[10])));;
		  MELT_LOCATION ("warmelt-normatch.melt:2496:/ cond");
		  /*cond */ if ( /*_#IS_A__L76*/ meltfnum[74])	/*then */
		    {
		      /*^cond.then */
		      /*_.IFELSE___V218*/ meltfptr[190] = ( /*nil */ NULL);;
		    }
		  else
		    {
		      MELT_LOCATION
			("warmelt-normatch.melt:2496:/ cond.else");

		      /*^block */
		      /*anyblock */
		      {




			{
			  /*^locexp */
			  melt_assert_failed (("check cursortbind"),
					      ("warmelt-normatch.melt")
					      ? ("warmelt-normatch.melt") :
					      __FILE__,
					      (2496) ? (2496) : __LINE__,
					      __FUNCTION__);
			  ;
			}
			;
		 /*clear *//*_.IFELSE___V218*/ meltfptr[190] = 0;
			/*epilog */
		      }
		      ;
		    }
		  ;
		  /*^compute */
		  /*_.IFCPP___V217*/ meltfptr[199] =
		    /*_.IFELSE___V218*/ meltfptr[190];;
		  /*epilog */

		  MELT_LOCATION ("warmelt-normatch.melt:2496:/ clear");
	       /*clear *//*_#IS_A__L76*/ meltfnum[74] = 0;
		  /*^clear */
	       /*clear *//*_.IFELSE___V218*/ meltfptr[190] = 0;
		}

#else /*MELT_HAVE_DEBUG */
		/*^cppif.else */
		/*_.IFCPP___V217*/ meltfptr[199] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
		;

		{
		  MELT_LOCATION ("warmelt-normatch.melt:2498:/ locexp");
		  meltgc_append_list ((melt_ptr_t)
				      ( /*_.SUBINDLIST__V211*/ meltfptr[192]),
				      (melt_ptr_t) ( /*_.CURSORTBIND__V212*/
						    meltfptr[198]));
		}
		;
		if ( /*_#SORTBIX__L73*/ meltfnum[22] < 0)
		  break;
	      }			/* end  foreach_in_multiple meltcit3__EACHTUP */

	    /*citerepilog */

	    MELT_LOCATION ("warmelt-normatch.melt:2492:/ clear");
	     /*clear *//*_.CURSORTBIND__V212*/ meltfptr[198] = 0;
	    /*^clear */
	     /*clear *//*_#SORTBIX__L73*/ meltfnum[22] = 0;
	    /*^clear */
	     /*clear *//*_.IFCPP___V213*/ meltfptr[197] = 0;
	    /*^clear */
	     /*clear *//*_.IFCPP___V217*/ meltfptr[199] = 0;
	  }			/*endciterblock FOREACH_IN_MULTIPLE */
	  ;

#if MELT_HAVE_DEBUG
	  MELT_LOCATION ("warmelt-normatch.melt:2500:/ cppif.then");
	  /*^block */
	  /*anyblock */
	  {


	    {
	      /*^locexp */
	      /*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	      melt_dbgcounter++;
#endif
	      ;
	    }
	    ;
	    /*^checksignal */
	    MELT_CHECK_SIGNAL ();
	    ;
    /*_#MELT_NEED_DBG__L77*/ meltfnum[42] =
	      /*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	      ( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	      0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	      ;;
	    MELT_LOCATION ("warmelt-normatch.melt:2500:/ cond");
	    /*cond */ if ( /*_#MELT_NEED_DBG__L77*/ meltfnum[42])	/*then */
	      {
		/*^cond.then */
		/*^block */
		/*anyblock */
		{

      /*_#THE_MELTCALLCOUNT__L78*/ meltfnum[74] =
#ifdef meltcallcount
		    meltcallcount	/* the_meltcallcount */
#else
		    0L
#endif /* meltcallcount the_meltcallcount */
		    ;;
		  MELT_LOCATION ("warmelt-normatch.melt:2500:/ checksignal");
		  MELT_CHECK_SIGNAL ();
		  ;
		  /*^apply */
		  /*apply */
		  {
		    union meltparam_un argtab[5];
		    memset (&argtab, 0, sizeof (argtab));
		    /*^apply.arg */
		    argtab[0].meltbp_long =
		      /*_#THE_MELTCALLCOUNT__L78*/ meltfnum[74];
		    /*^apply.arg */
		    argtab[1].meltbp_cstring = "warmelt-normatch.melt";
		    /*^apply.arg */
		    argtab[2].meltbp_long = 2500;
		    /*^apply.arg */
		    argtab[3].meltbp_cstring =
		      "normexp_match curbody before normalize_tuple";
		    /*^apply.arg */
		    argtab[4].meltbp_aptr =
		      (melt_ptr_t *) & /*_.CURBODY__V86*/ meltfptr[63];
		    /*_.MELT_DEBUG_FUN__V221*/ meltfptr[220] =
		      melt_apply ((meltclosure_ptr_t)
				  (( /*!MELT_DEBUG_FUN */ meltfrout->
				    tabval[3])),
				  (melt_ptr_t) (( /*nil */ NULL)),
				  (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_PTR ""), argtab, "",
				  (union meltparam_un *) 0);
		  }
		  ;
		  /*_.IF___V220*/ meltfptr[219] =
		    /*_.MELT_DEBUG_FUN__V221*/ meltfptr[220];;
		  /*epilog */

		  MELT_LOCATION ("warmelt-normatch.melt:2500:/ clear");
		/*clear *//*_#THE_MELTCALLCOUNT__L78*/ meltfnum[74] = 0;
		  /*^clear */
		/*clear *//*_.MELT_DEBUG_FUN__V221*/ meltfptr[220] = 0;
		}
		;
	      }
	    else
	      {			/*^cond.else */

     /*_.IF___V220*/ meltfptr[219] = NULL;;
	      }
	    ;
	    MELT_LOCATION ("warmelt-normatch.melt:2500:/ quasiblock");


	    /*_.PROGN___V222*/ meltfptr[220] = /*_.IF___V220*/ meltfptr[219];;
	    /*^compute */
	    /*_.IFCPP___V219*/ meltfptr[190] =
	      /*_.PROGN___V222*/ meltfptr[220];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-normatch.melt:2500:/ clear");
	      /*clear *//*_#MELT_NEED_DBG__L77*/ meltfnum[42] = 0;
	    /*^clear */
	      /*clear *//*_.IF___V220*/ meltfptr[219] = 0;
	    /*^clear */
	      /*clear *//*_.PROGN___V222*/ meltfptr[220] = 0;
	  }

#else /*MELT_HAVE_DEBUG */
	  /*^cppif.else */
	  /*_.IFCPP___V219*/ meltfptr[190] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
	  ;
	  MELT_LOCATION ("warmelt-normatch.melt:2503:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  MELT_LOCATION ("warmelt-normatch.melt:2501:/ quasiblock");


	  /*^multiapply */
	  /*multiapply 4args, 1x.res */
	  {
	    union meltparam_un argtab[3];

	    union meltparam_un restab[1];
	    memset (&restab, 0, sizeof (restab));
	    memset (&argtab, 0, sizeof (argtab));
	    /*^multiapply.arg */
	    argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.FRESHNEWENV__V179*/ meltfptr[178];	/*^multiapply.arg */
	    argtab[1].meltbp_aptr = (melt_ptr_t *) & /*_.NCX__V4*/ meltfptr[3];	/*^multiapply.arg */
	    argtab[2].meltbp_aptr =
	      (melt_ptr_t *) & /*_.CURLOC__V84*/ meltfptr[58];
	    /*^multiapply.xres */
	    restab[0].meltbp_aptr =
	      (melt_ptr_t *) & /*_.BINDSBODY__V225*/ meltfptr[224];
	    /*^multiapply.appl */
	    /*_.NBODY__V224*/ meltfptr[220] =
	      melt_apply ((meltclosure_ptr_t)
			  (( /*!NORMALIZE_TUPLE */ meltfrout->tabval[32])),
			  (melt_ptr_t) ( /*_.CURBODY__V86*/ meltfptr[63]),
			  (MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_PTR
			   ""), argtab, (MELTBPARSTR_PTR ""), restab);
	  }
	  ;
	  /*^quasiblock */



#if MELT_HAVE_DEBUG
	  MELT_LOCATION ("warmelt-normatch.melt:2504:/ cppif.then");
	  /*^block */
	  /*anyblock */
	  {


	    {
	      /*^locexp */
	      /*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	      melt_dbgcounter++;
#endif
	      ;
	    }
	    ;
	    /*^checksignal */
	    MELT_CHECK_SIGNAL ();
	    ;
    /*_#MELT_NEED_DBG__L79*/ meltfnum[74] =
	      /*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	      ( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	      0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	      ;;
	    MELT_LOCATION ("warmelt-normatch.melt:2504:/ cond");
	    /*cond */ if ( /*_#MELT_NEED_DBG__L79*/ meltfnum[74])	/*then */
	      {
		/*^cond.then */
		/*^block */
		/*anyblock */
		{

      /*_#THE_MELTCALLCOUNT__L80*/ meltfnum[42] =
#ifdef meltcallcount
		    meltcallcount	/* the_meltcallcount */
#else
		    0L
#endif /* meltcallcount the_meltcallcount */
		    ;;
		  MELT_LOCATION ("warmelt-normatch.melt:2504:/ checksignal");
		  MELT_CHECK_SIGNAL ();
		  ;
		  /*^apply */
		  /*apply */
		  {
		    union meltparam_un argtab[9];
		    memset (&argtab, 0, sizeof (argtab));
		    /*^apply.arg */
		    argtab[0].meltbp_long =
		      /*_#THE_MELTCALLCOUNT__L80*/ meltfnum[42];
		    /*^apply.arg */
		    argtab[1].meltbp_cstring = "warmelt-normatch.melt";
		    /*^apply.arg */
		    argtab[2].meltbp_long = 2504;
		    /*^apply.arg */
		    argtab[3].meltbp_cstring =
		      "normexp_match  after normalize_tuple curbody=";
		    /*^apply.arg */
		    argtab[4].meltbp_aptr =
		      (melt_ptr_t *) & /*_.CURBODY__V86*/ meltfptr[63];
		    /*^apply.arg */
		    argtab[5].meltbp_cstring = " nbody=";
		    /*^apply.arg */
		    argtab[6].meltbp_aptr =
		      (melt_ptr_t *) & /*_.NBODY__V224*/ meltfptr[220];
		    /*^apply.arg */
		    argtab[7].meltbp_cstring = " bindsbody=";
		    /*^apply.arg */
		    argtab[8].meltbp_aptr =
		      (melt_ptr_t *) & /*_.BINDSBODY__V225*/ meltfptr[224];
		    /*_.MELT_DEBUG_FUN__V228*/ meltfptr[227] =
		      melt_apply ((meltclosure_ptr_t)
				  (( /*!MELT_DEBUG_FUN */ meltfrout->
				    tabval[3])),
				  (melt_ptr_t) (( /*nil */ NULL)),
				  (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_PTR MELTBPARSTR_CSTRING
				   MELTBPARSTR_PTR MELTBPARSTR_CSTRING
				   MELTBPARSTR_PTR ""), argtab, "",
				  (union meltparam_un *) 0);
		  }
		  ;
		  /*_.IF___V227*/ meltfptr[226] =
		    /*_.MELT_DEBUG_FUN__V228*/ meltfptr[227];;
		  /*epilog */

		  MELT_LOCATION ("warmelt-normatch.melt:2504:/ clear");
		/*clear *//*_#THE_MELTCALLCOUNT__L80*/ meltfnum[42] = 0;
		  /*^clear */
		/*clear *//*_.MELT_DEBUG_FUN__V228*/ meltfptr[227] = 0;
		}
		;
	      }
	    else
	      {			/*^cond.else */

     /*_.IF___V227*/ meltfptr[226] = NULL;;
	      }
	    ;
	    MELT_LOCATION ("warmelt-normatch.melt:2504:/ quasiblock");


	    /*_.PROGN___V229*/ meltfptr[227] = /*_.IF___V227*/ meltfptr[226];;
	    /*^compute */
	    /*_.IFCPP___V226*/ meltfptr[225] =
	      /*_.PROGN___V229*/ meltfptr[227];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-normatch.melt:2504:/ clear");
	      /*clear *//*_#MELT_NEED_DBG__L79*/ meltfnum[74] = 0;
	    /*^clear */
	      /*clear *//*_.IF___V227*/ meltfptr[226] = 0;
	    /*^clear */
	      /*clear *//*_.PROGN___V229*/ meltfptr[227] = 0;
	  }

#else /*MELT_HAVE_DEBUG */
	  /*^cppif.else */
	  /*_.IFCPP___V226*/ meltfptr[225] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
	  ;

#if MELT_HAVE_DEBUG
	  MELT_LOCATION ("warmelt-normatch.melt:2507:/ cppif.then");
	  /*^block */
	  /*anyblock */
	  {

	    /*^checksignal */
	    MELT_CHECK_SIGNAL ();
	    ;
    /*_#IS_LIST_OR_NULL__L81*/ meltfnum[42] =
	      (( /*_.BINDSBODY__V225*/ meltfptr[224]) == NULL
	       ||
	       (melt_unsafe_magic_discr
		((melt_ptr_t) ( /*_.BINDSBODY__V225*/ meltfptr[224])) ==
		MELTOBMAG_LIST));;
	    MELT_LOCATION ("warmelt-normatch.melt:2507:/ cond");
	    /*cond */ if ( /*_#IS_LIST_OR_NULL__L81*/ meltfnum[42])	/*then */
	      {
		/*^cond.then */
		/*_.IFELSE___V231*/ meltfptr[227] = ( /*nil */ NULL);;
	      }
	    else
	      {
		MELT_LOCATION ("warmelt-normatch.melt:2507:/ cond.else");

		/*^block */
		/*anyblock */
		{




		  {
		    /*^locexp */
		    melt_assert_failed (("check bindsbody"),
					("warmelt-normatch.melt")
					? ("warmelt-normatch.melt") :
					__FILE__, (2507) ? (2507) : __LINE__,
					__FUNCTION__);
		    ;
		  }
		  ;
		/*clear *//*_.IFELSE___V231*/ meltfptr[227] = 0;
		  /*epilog */
		}
		;
	      }
	    ;
	    /*^compute */
	    /*_.IFCPP___V230*/ meltfptr[226] =
	      /*_.IFELSE___V231*/ meltfptr[227];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-normatch.melt:2507:/ clear");
	      /*clear *//*_#IS_LIST_OR_NULL__L81*/ meltfnum[42] = 0;
	    /*^clear */
	      /*clear *//*_.IFELSE___V231*/ meltfptr[227] = 0;
	  }

#else /*MELT_HAVE_DEBUG */
	  /*^cppif.else */
	  /*_.IFCPP___V230*/ meltfptr[226] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
	  ;
	  /*citerblock FOREACH_IN_LIST */
	  {
	    /* start foreach_in_list meltcit4__EACHLIST */
	    for ( /*_.BINDBODYPAIR__V232*/ meltfptr[227] =
		 melt_list_first ((melt_ptr_t) /*_.BINDSBODY__V225*/
				  meltfptr[224]);
		 melt_magic_discr ((melt_ptr_t) /*_.BINDBODYPAIR__V232*/
				   meltfptr[227]) == MELTOBMAG_PAIR;
		 /*_.BINDBODYPAIR__V232*/ meltfptr[227] =
		 melt_pair_tail ((melt_ptr_t) /*_.BINDBODYPAIR__V232*/
				 meltfptr[227]))
	      {
		/*_.CURBINDBODY__V233*/ meltfptr[232] =
		  melt_pair_head ((melt_ptr_t) /*_.BINDBODYPAIR__V232*/
				  meltfptr[227]);



#if MELT_HAVE_DEBUG
		MELT_LOCATION ("warmelt-normatch.melt:2511:/ cppif.then");
		/*^block */
		/*anyblock */
		{


		  {
		    /*^locexp */
		    /*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
		    melt_dbgcounter++;
#endif
		    ;
		  }
		  ;
		  /*^checksignal */
		  MELT_CHECK_SIGNAL ();
		  ;
     /*_#MELT_NEED_DBG__L82*/ meltfnum[74] =
		    /*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
		    ( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
		    0		/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
		    ;;
		  MELT_LOCATION ("warmelt-normatch.melt:2511:/ cond");
		  /*cond */ if ( /*_#MELT_NEED_DBG__L82*/ meltfnum[74])	/*then */
		    {
		      /*^cond.then */
		      /*^block */
		      /*anyblock */
		      {

       /*_#THE_MELTCALLCOUNT__L83*/ meltfnum[42] =
#ifdef meltcallcount
			  meltcallcount	/* the_meltcallcount */
#else
			  0L
#endif /* meltcallcount the_meltcallcount */
			  ;;
			MELT_LOCATION
			  ("warmelt-normatch.melt:2511:/ checksignal");
			MELT_CHECK_SIGNAL ();
			;
			/*^apply */
			/*apply */
			{
			  union meltparam_un argtab[5];
			  memset (&argtab, 0, sizeof (argtab));
			  /*^apply.arg */
			  argtab[0].meltbp_long =
			    /*_#THE_MELTCALLCOUNT__L83*/ meltfnum[42];
			  /*^apply.arg */
			  argtab[1].meltbp_cstring = "warmelt-normatch.melt";
			  /*^apply.arg */
			  argtab[2].meltbp_long = 2511;
			  /*^apply.arg */
			  argtab[3].meltbp_cstring =
			    "normexp_match curbindbody";
			  /*^apply.arg */
			  argtab[4].meltbp_aptr =
			    (melt_ptr_t *) & /*_.CURBINDBODY__V233*/
			    meltfptr[232];
			  /*_.MELT_DEBUG_FUN__V236*/ meltfptr[235] =
			    melt_apply ((meltclosure_ptr_t)
					(( /*!MELT_DEBUG_FUN */ meltfrout->
					  tabval[3])),
					(melt_ptr_t) (( /*nil */ NULL)),
					(MELTBPARSTR_LONG MELTBPARSTR_CSTRING
					 MELTBPARSTR_LONG MELTBPARSTR_CSTRING
					 MELTBPARSTR_PTR ""), argtab, "",
					(union meltparam_un *) 0);
			}
			;
			/*_.IF___V235*/ meltfptr[234] =
			  /*_.MELT_DEBUG_FUN__V236*/ meltfptr[235];;
			/*epilog */

			MELT_LOCATION ("warmelt-normatch.melt:2511:/ clear");
		 /*clear *//*_#THE_MELTCALLCOUNT__L83*/ meltfnum[42] =
			  0;
			/*^clear */
		 /*clear *//*_.MELT_DEBUG_FUN__V236*/ meltfptr[235] =
			  0;
		      }
		      ;
		    }
		  else
		    {		/*^cond.else */

      /*_.IF___V235*/ meltfptr[234] = NULL;;
		    }
		  ;
		  MELT_LOCATION ("warmelt-normatch.melt:2511:/ quasiblock");


		  /*_.PROGN___V237*/ meltfptr[235] =
		    /*_.IF___V235*/ meltfptr[234];;
		  /*^compute */
		  /*_.IFCPP___V234*/ meltfptr[233] =
		    /*_.PROGN___V237*/ meltfptr[235];;
		  /*epilog */

		  MELT_LOCATION ("warmelt-normatch.melt:2511:/ clear");
	       /*clear *//*_#MELT_NEED_DBG__L82*/ meltfnum[74] = 0;
		  /*^clear */
	       /*clear *//*_.IF___V235*/ meltfptr[234] = 0;
		  /*^clear */
	       /*clear *//*_.PROGN___V237*/ meltfptr[235] = 0;
		}

#else /*MELT_HAVE_DEBUG */
		/*^cppif.else */
		/*_.IFCPP___V234*/ meltfptr[233] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
		;

		{
		  MELT_LOCATION ("warmelt-normatch.melt:2512:/ locexp");
		  meltgc_append_list ((melt_ptr_t)
				      ( /*_.SUBINDLIST__V211*/ meltfptr[192]),
				      (melt_ptr_t) ( /*_.CURBINDBODY__V233*/
						    meltfptr[232]));
		}
		;
	      }			/* end foreach_in_list meltcit4__EACHLIST */
     /*_.BINDBODYPAIR__V232*/ meltfptr[227] = NULL;
     /*_.CURBINDBODY__V233*/ meltfptr[232] = NULL;


	    /*citerepilog */

	    MELT_LOCATION ("warmelt-normatch.melt:2508:/ clear");
	     /*clear *//*_.BINDBODYPAIR__V232*/ meltfptr[227] = 0;
	    /*^clear */
	     /*clear *//*_.CURBINDBODY__V233*/ meltfptr[232] = 0;
	    /*^clear */
	     /*clear *//*_.IFCPP___V234*/ meltfptr[233] = 0;
	  }			/*endciterblock FOREACH_IN_LIST */
	  ;

#if MELT_HAVE_DEBUG
	  MELT_LOCATION ("warmelt-normatch.melt:2514:/ cppif.then");
	  /*^block */
	  /*anyblock */
	  {


	    {
	      /*^locexp */
	      /*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	      melt_dbgcounter++;
#endif
	      ;
	    }
	    ;
	    /*^checksignal */
	    MELT_CHECK_SIGNAL ();
	    ;
    /*_#MELT_NEED_DBG__L84*/ meltfnum[42] =
	      /*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	      ( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	      0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	      ;;
	    MELT_LOCATION ("warmelt-normatch.melt:2514:/ cond");
	    /*cond */ if ( /*_#MELT_NEED_DBG__L84*/ meltfnum[42])	/*then */
	      {
		/*^cond.then */
		/*^block */
		/*anyblock */
		{

      /*_#THE_MELTCALLCOUNT__L85*/ meltfnum[74] =
#ifdef meltcallcount
		    meltcallcount	/* the_meltcallcount */
#else
		    0L
#endif /* meltcallcount the_meltcallcount */
		    ;;
		  MELT_LOCATION ("warmelt-normatch.melt:2514:/ checksignal");
		  MELT_CHECK_SIGNAL ();
		  ;
		  /*^apply */
		  /*apply */
		  {
		    union meltparam_un argtab[5];
		    memset (&argtab, 0, sizeof (argtab));
		    /*^apply.arg */
		    argtab[0].meltbp_long =
		      /*_#THE_MELTCALLCOUNT__L85*/ meltfnum[74];
		    /*^apply.arg */
		    argtab[1].meltbp_cstring = "warmelt-normatch.melt";
		    /*^apply.arg */
		    argtab[2].meltbp_long = 2514;
		    /*^apply.arg */
		    argtab[3].meltbp_cstring =
		      "normexp_match final subindlist";
		    /*^apply.arg */
		    argtab[4].meltbp_aptr =
		      (melt_ptr_t *) & /*_.SUBINDLIST__V211*/ meltfptr[192];
		    /*_.MELT_DEBUG_FUN__V240*/ meltfptr[239] =
		      melt_apply ((meltclosure_ptr_t)
				  (( /*!MELT_DEBUG_FUN */ meltfrout->
				    tabval[3])),
				  (melt_ptr_t) (( /*nil */ NULL)),
				  (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_PTR ""), argtab, "",
				  (union meltparam_un *) 0);
		  }
		  ;
		  /*_.IF___V239*/ meltfptr[235] =
		    /*_.MELT_DEBUG_FUN__V240*/ meltfptr[239];;
		  /*epilog */

		  MELT_LOCATION ("warmelt-normatch.melt:2514:/ clear");
		/*clear *//*_#THE_MELTCALLCOUNT__L85*/ meltfnum[74] = 0;
		  /*^clear */
		/*clear *//*_.MELT_DEBUG_FUN__V240*/ meltfptr[239] = 0;
		}
		;
	      }
	    else
	      {			/*^cond.else */

     /*_.IF___V239*/ meltfptr[235] = NULL;;
	      }
	    ;
	    MELT_LOCATION ("warmelt-normatch.melt:2514:/ quasiblock");


	    /*_.PROGN___V241*/ meltfptr[239] = /*_.IF___V239*/ meltfptr[235];;
	    /*^compute */
	    /*_.IFCPP___V238*/ meltfptr[234] =
	      /*_.PROGN___V241*/ meltfptr[239];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-normatch.melt:2514:/ clear");
	      /*clear *//*_#MELT_NEED_DBG__L84*/ meltfnum[42] = 0;
	    /*^clear */
	      /*clear *//*_.IF___V239*/ meltfptr[235] = 0;
	    /*^clear */
	      /*clear *//*_.PROGN___V241*/ meltfptr[239] = 0;
	  }

#else /*MELT_HAVE_DEBUG */
	  /*^cppif.else */
	  /*_.IFCPP___V238*/ meltfptr[234] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
	  ;
	  MELT_LOCATION ("warmelt-normatch.melt:2515:/ quasiblock");


	  /*^checksignal */
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^apply */
	  /*apply */
	  {
	    union meltparam_un argtab[2];
	    memset (&argtab, 0, sizeof (argtab));
	    /*^apply.arg */
	    argtab[0].meltbp_aptr =
	      (melt_ptr_t *) & /*_.SUBINDLIST__V211*/ meltfptr[192];
	    /*^apply.arg */
	    argtab[1].meltbp_aptr =
	      (melt_ptr_t *) & /*_.CURLOC__V84*/ meltfptr[58];
	    /*_.WNLET__V243*/ meltfptr[239] =
	      melt_apply ((meltclosure_ptr_t)
			  (( /*!WRAP_NORMAL_LETSEQ */ meltfrout->tabval[33])),
			  (melt_ptr_t) ( /*_.NBODY__V224*/ meltfptr[220]),
			  (MELTBPARSTR_PTR MELTBPARSTR_PTR ""), argtab, "",
			  (union meltparam_un *) 0);
	  }
	  ;
  /*_.LASTNBODY__V244*/ meltfptr[243] =
	    (melt_multiple_nth
	     ((melt_ptr_t) ( /*_.NBODY__V224*/ meltfptr[220]), (-1)));;
	  MELT_LOCATION ("warmelt-normatch.melt:2517:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^msend */
	  /*msend */
	  {
	    union meltparam_un argtab[1];
	    memset (&argtab, 0, sizeof (argtab));
	    /*^ojbmsend.arg */
	    argtab[0].meltbp_aptr =
	      (melt_ptr_t *) & /*_.FRESHNEWENV__V179*/ meltfptr[178];
	    /*_.LASTCTYPE__V245*/ meltfptr[244] =
	      meltgc_send ((melt_ptr_t)
			   ( /*_.LASTNBODY__V244*/ meltfptr[243]),
			   (melt_ptr_t) (( /*!GET_CTYPE */ meltfrout->
					  tabval[13])), (MELTBPARSTR_PTR ""),
			   argtab, "", (union meltparam_un *) 0);
	  }
	  ;

#if MELT_HAVE_DEBUG
	  MELT_LOCATION ("warmelt-normatch.melt:2519:/ cppif.then");
	  /*^block */
	  /*anyblock */
	  {


	    {
	      /*^locexp */
	      /*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	      melt_dbgcounter++;
#endif
	      ;
	    }
	    ;
	    /*^checksignal */
	    MELT_CHECK_SIGNAL ();
	    ;
    /*_#MELT_NEED_DBG__L86*/ meltfnum[74] =
	      /*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	      ( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	      0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	      ;;
	    MELT_LOCATION ("warmelt-normatch.melt:2519:/ cond");
	    /*cond */ if ( /*_#MELT_NEED_DBG__L86*/ meltfnum[74])	/*then */
	      {
		/*^cond.then */
		/*^block */
		/*anyblock */
		{

      /*_#THE_MELTCALLCOUNT__L87*/ meltfnum[42] =
#ifdef meltcallcount
		    meltcallcount	/* the_meltcallcount */
#else
		    0L
#endif /* meltcallcount the_meltcallcount */
		    ;;
		  MELT_LOCATION ("warmelt-normatch.melt:2519:/ checksignal");
		  MELT_CHECK_SIGNAL ();
		  ;
		  /*^apply */
		  /*apply */
		  {
		    union meltparam_un argtab[9];
		    memset (&argtab, 0, sizeof (argtab));
		    /*^apply.arg */
		    argtab[0].meltbp_long =
		      /*_#THE_MELTCALLCOUNT__L87*/ meltfnum[42];
		    /*^apply.arg */
		    argtab[1].meltbp_cstring = "warmelt-normatch.melt";
		    /*^apply.arg */
		    argtab[2].meltbp_long = 2519;
		    /*^apply.arg */
		    argtab[3].meltbp_cstring = "normexp_match wnlet=";
		    /*^apply.arg */
		    argtab[4].meltbp_aptr =
		      (melt_ptr_t *) & /*_.WNLET__V243*/ meltfptr[239];
		    /*^apply.arg */
		    argtab[5].meltbp_cstring = " lastnbody=";
		    /*^apply.arg */
		    argtab[6].meltbp_aptr =
		      (melt_ptr_t *) & /*_.LASTNBODY__V244*/ meltfptr[243];
		    /*^apply.arg */
		    argtab[7].meltbp_cstring = " lastctype=";
		    /*^apply.arg */
		    argtab[8].meltbp_aptr =
		      (melt_ptr_t *) & /*_.LASTCTYPE__V245*/ meltfptr[244];
		    /*_.MELT_DEBUG_FUN__V248*/ meltfptr[247] =
		      melt_apply ((meltclosure_ptr_t)
				  (( /*!MELT_DEBUG_FUN */ meltfrout->
				    tabval[3])),
				  (melt_ptr_t) (( /*nil */ NULL)),
				  (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_PTR MELTBPARSTR_CSTRING
				   MELTBPARSTR_PTR MELTBPARSTR_CSTRING
				   MELTBPARSTR_PTR ""), argtab, "",
				  (union meltparam_un *) 0);
		  }
		  ;
		  /*_.IF___V247*/ meltfptr[246] =
		    /*_.MELT_DEBUG_FUN__V248*/ meltfptr[247];;
		  /*epilog */

		  MELT_LOCATION ("warmelt-normatch.melt:2519:/ clear");
		/*clear *//*_#THE_MELTCALLCOUNT__L87*/ meltfnum[42] = 0;
		  /*^clear */
		/*clear *//*_.MELT_DEBUG_FUN__V248*/ meltfptr[247] = 0;
		}
		;
	      }
	    else
	      {			/*^cond.else */

     /*_.IF___V247*/ meltfptr[246] = NULL;;
	      }
	    ;
	    MELT_LOCATION ("warmelt-normatch.melt:2519:/ quasiblock");


	    /*_.PROGN___V249*/ meltfptr[247] = /*_.IF___V247*/ meltfptr[246];;
	    /*^compute */
	    /*_.IFCPP___V246*/ meltfptr[245] =
	      /*_.PROGN___V249*/ meltfptr[247];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-normatch.melt:2519:/ clear");
	      /*clear *//*_#MELT_NEED_DBG__L86*/ meltfnum[74] = 0;
	    /*^clear */
	      /*clear *//*_.IF___V247*/ meltfptr[246] = 0;
	    /*^clear */
	      /*clear *//*_.PROGN___V249*/ meltfptr[247] = 0;
	  }

#else /*MELT_HAVE_DEBUG */
	  /*^cppif.else */
	  /*_.IFCPP___V246*/ meltfptr[245] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
	  ;
	  MELT_LOCATION ("warmelt-normatch.melt:2522:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^cond */
	  /*cond */ if (
			 /*ifisa */
			 melt_is_instance_of ((melt_ptr_t)
					      ( /*_.NEWSUCTESTER__V122*/
					       meltfptr[121]),
					      (melt_ptr_t) (( /*!CLASS_NORMTESTER_SUCCESS */ meltfrout->tabval[24])))
	    )			/*then */
	    {
	      /*^cond.then */
	      /*^block */
	      /*anyblock */
	      {

		/*^putslot */
		/*putslot */
		melt_assertmsg ("putslot checkobj @NTSUCCESS_DO",
				melt_magic_discr ((melt_ptr_t)
						  ( /*_.NEWSUCTESTER__V122*/
						   meltfptr[121])) ==
				MELTOBMAG_OBJECT);
		melt_putfield_object (( /*_.NEWSUCTESTER__V122*/
				       meltfptr[121]), (7),
				      ( /*_.WNLET__V243*/ meltfptr[239]),
				      "NTSUCCESS_DO");
		;
		/*^touch */
		meltgc_touch ( /*_.NEWSUCTESTER__V122*/ meltfptr[121]);
		;
		/*^touchobj */

		melt_dbgtrace_written_object ( /*_.NEWSUCTESTER__V122*/
					      meltfptr[121], "put-fields");
		;
		/*epilog */
	      }
	      ;
	    }			/*noelse */
	  ;
	  MELT_LOCATION ("warmelt-normatch.melt:2525:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
  /*_#NULL__L88*/ meltfnum[42] =
	    (( /*_.WHOLECTYPE__V25*/ meltfptr[24]) == NULL);;
	  MELT_LOCATION ("warmelt-normatch.melt:2525:/ cond");
	  /*cond */ if ( /*_#NULL__L88*/ meltfnum[42])	/*then */
	    {
	      /*^cond.then */
	      /*^block */
	      /*anyblock */
	      {

		MELT_LOCATION ("warmelt-normatch.melt:2526:/ compute");
		/*_.WHOLECTYPE__V25*/ meltfptr[24] =
		  /*_.SETQ___V251*/ meltfptr[247] =
		  /*_.LASTCTYPE__V245*/ meltfptr[244];;
		/*_.IFELSE___V250*/ meltfptr[246] =
		  /*_.SETQ___V251*/ meltfptr[247];;
		/*epilog */

		MELT_LOCATION ("warmelt-normatch.melt:2525:/ clear");
	      /*clear *//*_.SETQ___V251*/ meltfptr[247] = 0;
	      }
	      ;
	    }
	  else
	    {			/*^cond.else */

	      /*^block */
	      /*anyblock */
	      {

		MELT_LOCATION ("warmelt-normatch.melt:2527:/ checksignal");
		MELT_CHECK_SIGNAL ();
		;
    /*_#__L89*/ meltfnum[74] =
		  (( /*_.LASTCTYPE__V245*/ meltfptr[244]) ==
		   (( /*!CTYPE_VOID */ meltfrout->tabval[11])));;
		MELT_LOCATION ("warmelt-normatch.melt:2527:/ cond");
		/*cond */ if ( /*_#__L89*/ meltfnum[74])	/*then */
		  {
		    /*^cond.then */
		    /*^block */
		    /*anyblock */
		    {

		      MELT_LOCATION ("warmelt-normatch.melt:2528:/ compute");
		      /*_.WHOLECTYPE__V25*/ meltfptr[24] =
			/*_.SETQ___V253*/ meltfptr[252] =
			( /*!CTYPE_VOID */ meltfrout->tabval[11]);;
		      /*_.IFELSE___V252*/ meltfptr[247] =
			/*_.SETQ___V253*/ meltfptr[252];;
		      /*epilog */

		      MELT_LOCATION ("warmelt-normatch.melt:2527:/ clear");
		/*clear *//*_.SETQ___V253*/ meltfptr[252] = 0;
		    }
		    ;
		  }
		else
		  {		/*^cond.else */

		    /*^block */
		    /*anyblock */
		    {

		      MELT_LOCATION
			("warmelt-normatch.melt:2529:/ checksignal");
		      MELT_CHECK_SIGNAL ();
		      ;
      /*_#__L90*/ meltfnum[89] =
			(( /*_.WHOLECTYPE__V25*/ meltfptr[24]) ==
			 (( /*!CTYPE_VOID */ meltfrout->tabval[11])));;
		      MELT_LOCATION ("warmelt-normatch.melt:2529:/ cond");
		      /*cond */ if ( /*_#__L90*/ meltfnum[89])	/*then */
			{
			  /*^cond.then */
			  /*_.IFELSE___V254*/ meltfptr[252] =
			    ( /*nil */ NULL);;
			}
		      else
			{
			  MELT_LOCATION
			    ("warmelt-normatch.melt:2529:/ cond.else");

			  /*^block */
			  /*anyblock */
			  {

			    MELT_LOCATION
			      ("warmelt-normatch.melt:2531:/ checksignal");
			    MELT_CHECK_SIGNAL ();
			    ;
	/*_#__L91*/ meltfnum[90] =
			      (( /*_.WHOLECTYPE__V25*/ meltfptr[24]) !=
			       ( /*_.LASTCTYPE__V245*/ meltfptr[244]));;
			    MELT_LOCATION
			      ("warmelt-normatch.melt:2531:/ cond");
			    /*cond */ if ( /*_#__L91*/ meltfnum[90])	/*then */
			      {
				/*^cond.then */
				/*^block */
				/*anyblock */
				{

				  MELT_LOCATION
				    ("warmelt-normatch.melt:2533:/ cond");
				  /*cond */ if (
						 /*ifisa */
						 melt_is_instance_of ((melt_ptr_t) ( /*_.LASTCTYPE__V245*/ meltfptr[244]),
								      (melt_ptr_t) (( /*!CLASS_NAMED */ meltfrout->tabval[34])))
				    )	/*then */
				    {
				      /*^cond.then */
				      /*^getslot */
				      {
					melt_ptr_t slot = NULL, obj = NULL;
					obj =
					  (melt_ptr_t) ( /*_.LASTCTYPE__V245*/
							meltfptr[244])
					  /*=obj*/ ;
					melt_object_get_field (slot, obj, 1,
							       "NAMED_NAME");
	    /*_.NAMED_NAME__V256*/ meltfptr[255] =
					  slot;
				      };
				      ;
				    }
				  else
				    {	/*^cond.else */

	   /*_.NAMED_NAME__V256*/ meltfptr[255] =
					NULL;;
				    }
				  ;

				  {
				    MELT_LOCATION
				      ("warmelt-normatch.melt:2532:/ locexp");
				    melt_error_str ((melt_ptr_t)
						    ( /*_.CURLOC__V84*/
						     meltfptr[58]),
						    ("invalid type of match case"),
						    (melt_ptr_t) ( /*_.NAMED_NAME__V256*/ meltfptr[255]));
				  }
				  ;
				  MELT_LOCATION
				    ("warmelt-normatch.melt:2535:/ cond");
				  /*cond */ if (
						 /*ifisa */
						 melt_is_instance_of ((melt_ptr_t) ( /*_.WHOLECTYPE__V25*/ meltfptr[24]),
								      (melt_ptr_t) (( /*!CLASS_NAMED */ meltfrout->tabval[34])))
				    )	/*then */
				    {
				      /*^cond.then */
				      /*^getslot */
				      {
					melt_ptr_t slot = NULL, obj = NULL;
					obj =
					  (melt_ptr_t) ( /*_.WHOLECTYPE__V25*/
							meltfptr[24]) /*=obj*/
					  ;
					melt_object_get_field (slot, obj, 1,
							       "NAMED_NAME");
	    /*_.NAMED_NAME__V257*/ meltfptr[256] =
					  slot;
				      };
				      ;
				    }
				  else
				    {	/*^cond.else */

	   /*_.NAMED_NAME__V257*/ meltfptr[256] =
					NULL;;
				    }
				  ;

				  {
				    MELT_LOCATION
				      ("warmelt-normatch.melt:2534:/ locexp");
				    melt_inform_str ((melt_ptr_t)
						     ( /*_.SLOC__V17*/
						      meltfptr[13]),
						     ("expected type of match case"),
						     (melt_ptr_t) ( /*_.NAMED_NAME__V257*/ meltfptr[256]));
				  }
				  ;
				  MELT_LOCATION
				    ("warmelt-normatch.melt:2531:/ quasiblock");


				  /*epilog */

				  /*^clear */
		    /*clear *//*_.NAMED_NAME__V256*/
				    meltfptr[255] = 0;
				  /*^clear */
		    /*clear *//*_.NAMED_NAME__V257*/
				    meltfptr[256] = 0;
				}
				;
			      }
			    else
			      {	/*^cond.else */

	 /*_.IFELSE___V255*/ meltfptr[254] = NULL;;
			      }
			    ;
			    /*^compute */
			    /*_.IFELSE___V254*/ meltfptr[252] =
			      /*_.IFELSE___V255*/ meltfptr[254];;
			    /*epilog */

			    MELT_LOCATION
			      ("warmelt-normatch.melt:2529:/ clear");
		  /*clear *//*_#__L91*/ meltfnum[90] = 0;
			    /*^clear */
		  /*clear *//*_.IFELSE___V255*/ meltfptr[254] = 0;
			  }
			  ;
			}
		      ;
		      /*_.IFELSE___V252*/ meltfptr[247] =
			/*_.IFELSE___V254*/ meltfptr[252];;
		      /*epilog */

		      MELT_LOCATION ("warmelt-normatch.melt:2527:/ clear");
		/*clear *//*_#__L90*/ meltfnum[89] = 0;
		      /*^clear */
		/*clear *//*_.IFELSE___V254*/ meltfptr[252] = 0;
		    }
		    ;
		  }
		;
		/*_.IFELSE___V250*/ meltfptr[246] =
		  /*_.IFELSE___V252*/ meltfptr[247];;
		/*epilog */

		MELT_LOCATION ("warmelt-normatch.melt:2525:/ clear");
	      /*clear *//*_#__L89*/ meltfnum[74] = 0;
		/*^clear */
	      /*clear *//*_.IFELSE___V252*/ meltfptr[247] = 0;
	      }
	      ;
	    }
	  ;

#if MELT_HAVE_DEBUG
	  MELT_LOCATION ("warmelt-normatch.melt:2537:/ cppif.then");
	  /*^block */
	  /*anyblock */
	  {


	    {
	      /*^locexp */
	      /*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	      melt_dbgcounter++;
#endif
	      ;
	    }
	    ;
	    /*^checksignal */
	    MELT_CHECK_SIGNAL ();
	    ;
    /*_#MELT_NEED_DBG__L92*/ meltfnum[90] =
	      /*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	      ( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	      0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	      ;;
	    MELT_LOCATION ("warmelt-normatch.melt:2537:/ cond");
	    /*cond */ if ( /*_#MELT_NEED_DBG__L92*/ meltfnum[90])	/*then */
	      {
		/*^cond.then */
		/*^block */
		/*anyblock */
		{

      /*_#THE_MELTCALLCOUNT__L93*/ meltfnum[89] =
#ifdef meltcallcount
		    meltcallcount	/* the_meltcallcount */
#else
		    0L
#endif /* meltcallcount the_meltcallcount */
		    ;;
		  MELT_LOCATION ("warmelt-normatch.melt:2537:/ checksignal");
		  MELT_CHECK_SIGNAL ();
		  ;
		  /*^apply */
		  /*apply */
		  {
		    union meltparam_un argtab[5];
		    memset (&argtab, 0, sizeof (argtab));
		    /*^apply.arg */
		    argtab[0].meltbp_long =
		      /*_#THE_MELTCALLCOUNT__L93*/ meltfnum[89];
		    /*^apply.arg */
		    argtab[1].meltbp_cstring = "warmelt-normatch.melt";
		    /*^apply.arg */
		    argtab[2].meltbp_long = 2537;
		    /*^apply.arg */
		    argtab[3].meltbp_cstring =
		      "normexp_match updated newsuctester";
		    /*^apply.arg */
		    argtab[4].meltbp_aptr =
		      (melt_ptr_t *) & /*_.NEWSUCTESTER__V122*/ meltfptr[121];
		    /*_.MELT_DEBUG_FUN__V260*/ meltfptr[254] =
		      melt_apply ((meltclosure_ptr_t)
				  (( /*!MELT_DEBUG_FUN */ meltfrout->
				    tabval[3])),
				  (melt_ptr_t) (( /*nil */ NULL)),
				  (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_PTR ""), argtab, "",
				  (union meltparam_un *) 0);
		  }
		  ;
		  /*_.IF___V259*/ meltfptr[256] =
		    /*_.MELT_DEBUG_FUN__V260*/ meltfptr[254];;
		  /*epilog */

		  MELT_LOCATION ("warmelt-normatch.melt:2537:/ clear");
		/*clear *//*_#THE_MELTCALLCOUNT__L93*/ meltfnum[89] = 0;
		  /*^clear */
		/*clear *//*_.MELT_DEBUG_FUN__V260*/ meltfptr[254] = 0;
		}
		;
	      }
	    else
	      {			/*^cond.else */

     /*_.IF___V259*/ meltfptr[256] = NULL;;
	      }
	    ;
	    MELT_LOCATION ("warmelt-normatch.melt:2537:/ quasiblock");


	    /*_.PROGN___V261*/ meltfptr[252] = /*_.IF___V259*/ meltfptr[256];;
	    /*^compute */
	    /*_.IFCPP___V258*/ meltfptr[255] =
	      /*_.PROGN___V261*/ meltfptr[252];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-normatch.melt:2537:/ clear");
	      /*clear *//*_#MELT_NEED_DBG__L92*/ meltfnum[90] = 0;
	    /*^clear */
	      /*clear *//*_.IF___V259*/ meltfptr[256] = 0;
	    /*^clear */
	      /*clear *//*_.PROGN___V261*/ meltfptr[252] = 0;
	  }

#else /*MELT_HAVE_DEBUG */
	  /*^cppif.else */
	  /*_.IFCPP___V258*/ meltfptr[255] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
	  ;
	  /*^compute */
	  /*_.LET___V242*/ meltfptr[235] = /*_.IFCPP___V258*/ meltfptr[255];;

	  MELT_LOCATION ("warmelt-normatch.melt:2515:/ clear");
	    /*clear *//*_.WNLET__V243*/ meltfptr[239] = 0;
	  /*^clear */
	    /*clear *//*_.LASTNBODY__V244*/ meltfptr[243] = 0;
	  /*^clear */
	    /*clear *//*_.LASTCTYPE__V245*/ meltfptr[244] = 0;
	  /*^clear */
	    /*clear *//*_.IFCPP___V246*/ meltfptr[245] = 0;
	  /*^clear */
	    /*clear *//*_#NULL__L88*/ meltfnum[42] = 0;
	  /*^clear */
	    /*clear *//*_.IFELSE___V250*/ meltfptr[246] = 0;
	  /*^clear */
	    /*clear *//*_.IFCPP___V258*/ meltfptr[255] = 0;
	  MELT_LOCATION ("warmelt-normatch.melt:2501:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*_.MULTI___V223*/ meltfptr[219] = /*_.LET___V242*/ meltfptr[235];;

	  MELT_LOCATION ("warmelt-normatch.melt:2501:/ clear");
	    /*clear *//*_.IFCPP___V226*/ meltfptr[225] = 0;
	  /*^clear */
	    /*clear *//*_.IFCPP___V230*/ meltfptr[226] = 0;
	  /*^clear */
	    /*clear *//*_.IFCPP___V238*/ meltfptr[234] = 0;
	  /*^clear */
	    /*clear *//*_.LET___V242*/ meltfptr[235] = 0;

	  /*^clear */
	    /*clear *//*_.BINDSBODY__V225*/ meltfptr[224] = 0;
	  /*_.LET___V210*/ meltfptr[191] = /*_.MULTI___V223*/ meltfptr[219];;

	  MELT_LOCATION ("warmelt-normatch.melt:2490:/ clear");
	    /*clear *//*_.SUBINDLIST__V211*/ meltfptr[192] = 0;
	  /*^clear */
	    /*clear *//*_.IFCPP___V219*/ meltfptr[190] = 0;
	  /*^clear */
	    /*clear *//*_.MULTI___V223*/ meltfptr[219] = 0;
	  /*_.LET___V176*/ meltfptr[130] = /*_.LET___V210*/ meltfptr[191];;

	  MELT_LOCATION ("warmelt-normatch.melt:2467:/ clear");
	    /*clear *//*_.SORTEDVARS__V177*/ meltfptr[135] = 0;
	  /*^clear */
	    /*clear *//*_#MULTIPLE_LENGTH__L59*/ meltfnum[41] = 0;
	  /*^clear */
	    /*clear *//*_.SORTEDBINDINGS__V178*/ meltfptr[125] = 0;
	  /*^clear */
	    /*clear *//*_.FRESHNEWENV__V179*/ meltfptr[178] = 0;
	  /*^clear */
	    /*clear *//*_.IFCPP___V180*/ meltfptr[179] = 0;
	  /*^clear */
	    /*clear *//*_.IFCPP___V204*/ meltfptr[200] = 0;
	  /*^clear */
	    /*clear *//*_.IFCPP___V208*/ meltfptr[186] = 0;
	  /*^clear */
	    /*clear *//*_.LET___V210*/ meltfptr[191] = 0;
	  /*_.LET___V118*/ meltfptr[114] = /*_.LET___V176*/ meltfptr[130];;

	  MELT_LOCATION ("warmelt-normatch.melt:2401:/ clear");
	    /*clear *//*_.NEWENV__V119*/ meltfptr[115] = 0;
	  /*^clear */
	    /*clear *//*_.CURTESTER__V120*/ meltfptr[119] = 0;
	  /*^clear */
	    /*clear *//*_.MAKE_LIST__V121*/ meltfptr[120] = 0;
	  /*^clear */
	    /*clear *//*_.NEWSUCTESTER__V122*/ meltfptr[121] = 0;
	  /*^clear */
	    /*clear *//*_.IFCPP___V124*/ meltfptr[123] = 0;
	  /*^clear */
	    /*clear *//*_#NULL__L36*/ meltfnum[23] = 0;
	  /*^clear */
	    /*clear *//*_.IFELSE___V128*/ meltfptr[124] = 0;
	  /*^clear */
	    /*clear *//*_.IFCPP___V154*/ meltfptr[150] = 0;
	  /*^clear */
	    /*clear *//*_.IFCPP___V158*/ meltfptr[145] = 0;
	  /*^clear */
	    /*clear *//*_#IS_A__L52*/ meltfnum[40] = 0;
	  /*^clear */
	    /*clear *//*_.IF___V160*/ meltfptr[129] = 0;
	  /*^clear */
	    /*clear *//*_.SETQ___V171*/ meltfptr[144] = 0;
	  /*^clear */
	    /*clear *//*_.IFCPP___V172*/ meltfptr[149] = 0;
	  /*^clear */
	    /*clear *//*_.LET___V176*/ meltfptr[130] = 0;
	  /*_.LET___V83*/ meltfptr[56] = /*_.LET___V118*/ meltfptr[114];;

	  MELT_LOCATION ("warmelt-normatch.melt:2350:/ clear");
	    /*clear *//*_.CURLOC__V84*/ meltfptr[58] = 0;
	  /*^clear */
	    /*clear *//*_.CURPAT__V85*/ meltfptr[64] = 0;
	  /*^clear */
	    /*clear *//*_.CURBODY__V86*/ meltfptr[63] = 0;
	  /*^clear */
	    /*clear *//*_.MAPVAR__V87*/ meltfptr[68] = 0;
	  /*^clear */
	    /*clear *//*_.MAPCST__V88*/ meltfptr[51] = 0;
	  /*^clear */
	    /*clear *//*_.MAPOR__V89*/ meltfptr[88] = 0;
	  /*^clear */
	    /*clear *//*_.PVARLOCMAP__V90*/ meltfptr[89] = 0;
	  /*^clear */
	    /*clear *//*_.VARHDLERLIST__V91*/ meltfptr[90] = 0;
	  /*^clear */
	    /*clear *//*_.PCN__V92*/ meltfptr[91] = 0;
	  /*^clear */
	    /*clear *//*_.NTESTCONT__V94*/ meltfptr[93] = 0;
	  /*^clear */
	    /*clear *//*_.IFCPP___V96*/ meltfptr[95] = 0;
	  /*^clear */
	    /*clear *//*_.IFCPP___V100*/ meltfptr[96] = 0;
	  /*^clear */
	    /*clear *//*_.SCAN_PATTERN__V102*/ meltfptr[97] = 0;
	  /*^clear */
	    /*clear *//*_.IFCPP___V103*/ meltfptr[102] = 0;
	  /*^clear */
	    /*clear *//*_.IFCPP___V107*/ meltfptr[103] = 0;
	  /*^clear */
	    /*clear *//*_.LAMBDA___V111*/ meltfptr[104] = 0;
	  /*^clear */
	    /*clear *//*_.NORMAL_PATTERN__V113*/ meltfptr[112] = 0;
	  /*^clear */
	    /*clear *//*_.IFCPP___V114*/ meltfptr[113] = 0;
	  /*^clear */
	    /*clear *//*_.LET___V118*/ meltfptr[114] = 0;
	  if ( /*_#IX__L22*/ meltfnum[17] < 0)
	    break;
	}			/* end  foreach_in_multiple meltcit1__EACHTUP */

      /*citerepilog */

      MELT_LOCATION ("warmelt-normatch.melt:2345:/ clear");
	    /*clear *//*_.CURCAS__V76*/ meltfptr[60] = 0;
      /*^clear */
	    /*clear *//*_#IX__L22*/ meltfnum[17] = 0;
      /*^clear */
	    /*clear *//*_.IFCPP___V77*/ meltfptr[61] = 0;
      /*^clear */
	    /*clear *//*_.IFCPP___V81*/ meltfptr[69] = 0;
      /*^clear */
	    /*clear *//*_.LET___V83*/ meltfptr[56] = 0;
    }				/*endciterblock FOREACH_IN_MULTIPLE */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-normatch.melt:2540:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L94*/ meltfnum[74] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-normatch.melt:2540:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L94*/ meltfnum[74])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L95*/ meltfnum[89] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-normatch.melt:2545:/ quasiblock");


     /*_.STMPKLIST__V265*/ meltfptr[252] =
	      (meltgc_new_list
	       ((meltobject_ptr_t)
		(( /*!DISCR_LIST */ meltfrout->tabval[6]))));;
	    /*citerblock FOREACH_IN_MAPOBJECT */
	    {
	      /* foreach_in_mapobject meltcit5__EACHOBMAP : */ int
		meltcit5__EACHOBMAP_ix = 0, meltcit5__EACHOBMAP_siz = 0;
	      for (meltcit5__EACHOBMAP_ix = 0;
		   /* we retrieve in meltcit5__EACHOBMAP_siz the size at each iteration since it could change. */
		   meltcit5__EACHOBMAP_ix >= 0
		   && (meltcit5__EACHOBMAP_siz =
		       melt_size_mapobjects ((meltmapobjects_ptr_t)
					     /*_.STUFFMAP__V22*/
					     meltfptr[21])) > 0
		   && meltcit5__EACHOBMAP_ix < meltcit5__EACHOBMAP_siz;
		   meltcit5__EACHOBMAP_ix++)
		{
    /*_.LOCOCCK__V266*/ meltfptr[239] = NULL;
    /*_.LITEST__V267*/ meltfptr[243] = NULL;
		  /*_.LOCOCCK__V266*/ meltfptr[239] =
		    (melt_ptr_t) (((meltmapobjects_ptr_t) /*_.STUFFMAP__V22*/
				   meltfptr[21])->
				  entab[meltcit5__EACHOBMAP_ix].e_at);
		  if ( /*_.LOCOCCK__V266*/ meltfptr[239] ==
		      HTAB_DELETED_ENTRY)
		    {						   /*_.LOCOCCK__V266*/
		      meltfptr[239] = NULL;
		      continue;
		    };
		  if (! /*_.LOCOCCK__V266*/ meltfptr[239])
		    continue;
		  /*_.LITEST__V267*/ meltfptr[243] =
		    ((meltmapobjects_ptr_t) /*_.STUFFMAP__V22*/
		     meltfptr[21])->entab[meltcit5__EACHOBMAP_ix].e_va;
		  if (! /*_.LITEST__V267*/ meltfptr[243])
		    continue;




		  {
		    MELT_LOCATION ("warmelt-normatch.melt:2549:/ locexp");
		    meltgc_append_list ((melt_ptr_t)
					( /*_.STMPKLIST__V265*/
					 meltfptr[252]),
					(melt_ptr_t) ( /*_.LOCOCCK__V266*/
						      meltfptr[239]));
		  }
		  ;
		  /* foreach_in_mapobject end meltcit5__EACHOBMAP */
    /*_.LOCOCCK__V266*/ meltfptr[239] = NULL;
    /*_.LITEST__V267*/ meltfptr[243] = NULL;
		}


	      /*citerepilog */

	      MELT_LOCATION ("warmelt-normatch.melt:2546:/ clear");
		/*clear *//*_.LOCOCCK__V266*/ meltfptr[239] = 0;
	      /*^clear */
		/*clear *//*_.LITEST__V267*/ meltfptr[243] = 0;
	    }			/*endciterblock FOREACH_IN_MAPOBJECT */
	    ;
	    /*_.LET___V264*/ meltfptr[256] =
	      /*_.STMPKLIST__V265*/ meltfptr[252];;

	    MELT_LOCATION ("warmelt-normatch.melt:2545:/ clear");
	       /*clear *//*_.STMPKLIST__V265*/ meltfptr[252] = 0;
	    MELT_LOCATION ("warmelt-normatch.melt:2540:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[13];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long =
		/*_#THE_MELTCALLCOUNT__L95*/ meltfnum[89];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-normatch.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 2540;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "normexp_match tupvarmap=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.TUPVARMAP__V20*/ meltfptr[19];
	      /*^apply.arg */
	      argtab[5].meltbp_cstring = " tupcstmap=";
	      /*^apply.arg */
	      argtab[6].meltbp_aptr =
		(melt_ptr_t *) & /*_.TUPCSTMAP__V21*/ meltfptr[20];
	      /*^apply.arg */
	      argtab[7].meltbp_cstring = " testlist=";
	      /*^apply.arg */
	      argtab[8].meltbp_aptr =
		(melt_ptr_t *) & /*_.TESTLIST__V24*/ meltfptr[23];
	      /*^apply.arg */
	      argtab[9].meltbp_cstring = " stuffmap=";
	      /*^apply.arg */
	      argtab[10].meltbp_aptr =
		(melt_ptr_t *) & /*_.STUFFMAP__V22*/ meltfptr[21];
	      /*^apply.arg */
	      argtab[11].meltbp_cstring = " stuffmapkeylist=";
	      /*^apply.arg */
	      argtab[12].meltbp_aptr =
		(melt_ptr_t *) & /*_.LET___V264*/ meltfptr[256];
	      /*_.MELT_DEBUG_FUN__V268*/ meltfptr[244] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[3])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V263*/ meltfptr[254] =
	      /*_.MELT_DEBUG_FUN__V268*/ meltfptr[244];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-normatch.melt:2540:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L95*/ meltfnum[89] = 0;
	    /*^clear */
	       /*clear *//*_.LET___V264*/ meltfptr[256] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V268*/ meltfptr[244] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V263*/ meltfptr[254] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-normatch.melt:2540:/ quasiblock");


      /*_.PROGN___V269*/ meltfptr[245] = /*_.IF___V263*/ meltfptr[254];;
      /*^compute */
      /*_.IFCPP___V262*/ meltfptr[247] = /*_.PROGN___V269*/ meltfptr[245];;
      /*epilog */

      MELT_LOCATION ("warmelt-normatch.melt:2540:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L94*/ meltfnum[74] = 0;
      /*^clear */
	     /*clear *//*_.IF___V263*/ meltfptr[254] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V269*/ meltfptr[245] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V262*/ meltfptr[247] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-normatch.melt:2552:/ quasiblock");


    MELT_LOCATION ("warmelt-normatch.melt:2553:/ quasiblock");


    /*^checksignal */
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & ( /*!DISCR_MULTIPLE */ meltfrout->tabval[4]);
      /*_.TT__V272*/ meltfptr[225] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!LIST_TO_MULTIPLE */ meltfrout->tabval[35])),
		    (melt_ptr_t) ( /*_.TESTLIST__V24*/ meltfptr[23]),
		    (MELTBPARSTR_PTR ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-normatch.melt:2554:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L96*/ meltfnum[90] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-normatch.melt:2554:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L96*/ meltfnum[90])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L97*/ meltfnum[42] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-normatch.melt:2554:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long =
		/*_#THE_MELTCALLCOUNT__L97*/ meltfnum[42];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-normatch.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 2554;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "normexp_match teststupl";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.TT__V272*/ meltfptr[225];
	      /*_.MELT_DEBUG_FUN__V275*/ meltfptr[235] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[3])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V274*/ meltfptr[234] =
	      /*_.MELT_DEBUG_FUN__V275*/ meltfptr[235];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-normatch.melt:2554:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L97*/ meltfnum[42] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V275*/ meltfptr[235] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V274*/ meltfptr[234] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-normatch.melt:2554:/ quasiblock");


      /*_.PROGN___V276*/ meltfptr[220] = /*_.IF___V274*/ meltfptr[234];;
      /*^compute */
      /*_.IFCPP___V273*/ meltfptr[226] = /*_.PROGN___V276*/ meltfptr[220];;
      /*epilog */

      MELT_LOCATION ("warmelt-normatch.melt:2554:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L96*/ meltfnum[90] = 0;
      /*^clear */
	     /*clear *//*_.IF___V274*/ meltfptr[234] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V276*/ meltfptr[220] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V273*/ meltfptr[226] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    /*^compute */
    /*_.LET___V271*/ meltfptr[255] = /*_.TT__V272*/ meltfptr[225];;

    MELT_LOCATION ("warmelt-normatch.melt:2553:/ clear");
	   /*clear *//*_.TT__V272*/ meltfptr[225] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V273*/ meltfptr[226] = 0;
    /*_.TESTSTUPL__V277*/ meltfptr[224] = /*_.LET___V271*/ meltfptr[255];;
    MELT_LOCATION ("warmelt-normatch.melt:2556:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*^rawallocobj */
    /*rawallocobj */
    {
      melt_ptr_t newobj = 0;
      melt_raw_object_create (newobj,
			      (melt_ptr_t) (( /*!CLASS_NREP_MATCH */
					     meltfrout->tabval[36])), (5),
			      "CLASS_NREP_MATCH");
  /*_.INST__V279*/ meltfptr[190] =
	newobj;
    };
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @NREP_LOC",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V279*/ meltfptr[190])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V279*/ meltfptr[190]), (0),
			  ( /*_.SLOC__V17*/ meltfptr[13]), "NREP_LOC");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @NEXPR_CTYP",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V279*/ meltfptr[190])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V279*/ meltfptr[190]), (1),
			  ( /*_.WHOLECTYPE__V25*/ meltfptr[24]),
			  "NEXPR_CTYP");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @NMATCH_TESTS",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V279*/ meltfptr[190])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V279*/ meltfptr[190]), (2),
			  ( /*_.TESTSTUPL__V277*/ meltfptr[224]),
			  "NMATCH_TESTS");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @NMATCH_STUFFMAP",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V279*/ meltfptr[190])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V279*/ meltfptr[190]), (3),
			  ( /*_.STUFFMAP__V22*/ meltfptr[21]),
			  "NMATCH_STUFFMAP");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @NMATCH_MATCHED",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V279*/ meltfptr[190])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V279*/ meltfptr[190]), (4),
			  ( /*_.NMATX__V37*/ meltfptr[33]), "NMATCH_MATCHED");
    ;
    /*^touchobj */

    melt_dbgtrace_written_object ( /*_.INST__V279*/ meltfptr[190],
				  "newly made instance");
    ;
    /*_.NMATCH__V278*/ meltfptr[192] = /*_.INST__V279*/ meltfptr[190];;
    MELT_LOCATION ("warmelt-normatch.melt:2563:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      /*_.CSYM__V280*/ meltfptr[219] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!CLONE_SYMBOL */ meltfrout->tabval[7])),
		    (melt_ptr_t) (( /*!konst_37_MATCHRES_ */ meltfrout->
				   tabval[37])), (""),
		    (union meltparam_un *) 0, "", (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-normatch.melt:2564:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*^rawallocobj */
    /*rawallocobj */
    {
      melt_ptr_t newobj = 0;
      melt_raw_object_create (newobj,
			      (melt_ptr_t) (( /*!CLASS_NREP_LOCSYMOCC */
					     meltfrout->tabval[15])), (4),
			      "CLASS_NREP_LOCSYMOCC");
  /*_.INST__V282*/ meltfptr[125] =
	newobj;
    };
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @NREP_LOC",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V282*/ meltfptr[125])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V282*/ meltfptr[125]), (0),
			  ( /*_.SLOC__V17*/ meltfptr[13]), "NREP_LOC");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @NOCC_CTYP",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V282*/ meltfptr[125])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V282*/ meltfptr[125]), (2),
			  ( /*_.WHOLECTYPE__V25*/ meltfptr[24]), "NOCC_CTYP");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @NOCC_SYMB",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V282*/ meltfptr[125])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V282*/ meltfptr[125]), (1),
			  ( /*_.CSYM__V280*/ meltfptr[219]), "NOCC_SYMB");
    ;
    /*^touchobj */

    melt_dbgtrace_written_object ( /*_.INST__V282*/ meltfptr[125],
				  "newly made instance");
    ;
    /*_.CLOCC__V281*/ meltfptr[135] = /*_.INST__V282*/ meltfptr[125];;
    MELT_LOCATION ("warmelt-normatch.melt:2568:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*^rawallocobj */
    /*rawallocobj */
    {
      melt_ptr_t newobj = 0;
      melt_raw_object_create (newobj,
			      (melt_ptr_t) (( /*!CLASS_NORMAL_LET_BINDING */
					     meltfrout->tabval[10])), (4),
			      "CLASS_NORMAL_LET_BINDING");
  /*_.INST__V284*/ meltfptr[179] =
	newobj;
    };
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @LETBIND_LOC",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V284*/ meltfptr[179])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V284*/ meltfptr[179]), (3),
			  ( /*_.SLOC__V17*/ meltfptr[13]), "LETBIND_LOC");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @BINDER",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V284*/ meltfptr[179])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V284*/ meltfptr[179]), (0),
			  ( /*_.CSYM__V280*/ meltfptr[219]), "BINDER");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @LETBIND_TYPE",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V284*/ meltfptr[179])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V284*/ meltfptr[179]), (1),
			  ( /*_.WHOLECTYPE__V25*/ meltfptr[24]),
			  "LETBIND_TYPE");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @LETBIND_EXPR",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V284*/ meltfptr[179])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V284*/ meltfptr[179]), (2),
			  ( /*_.NMATCH__V278*/ meltfptr[192]),
			  "LETBIND_EXPR");
    ;
    /*^touchobj */

    melt_dbgtrace_written_object ( /*_.INST__V284*/ meltfptr[179],
				  "newly made instance");
    ;
    /*_.CBIND__V283*/ meltfptr[178] = /*_.INST__V284*/ meltfptr[179];;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-normatch.melt:2574:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L98*/ meltfnum[41] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-normatch.melt:2574:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L98*/ meltfnum[41])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L99*/ meltfnum[23] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-normatch.melt:2574:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[7];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long =
		/*_#THE_MELTCALLCOUNT__L99*/ meltfnum[23];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-normatch.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 2574;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "normexp_match csym=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.CSYM__V280*/ meltfptr[219];
	      /*^apply.arg */
	      argtab[5].meltbp_cstring = "\n cbind=";
	      /*^apply.arg */
	      argtab[6].meltbp_aptr =
		(melt_ptr_t *) & /*_.CBIND__V283*/ meltfptr[178];
	      /*_.MELT_DEBUG_FUN__V287*/ meltfptr[191] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[3])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V286*/ meltfptr[186] =
	      /*_.MELT_DEBUG_FUN__V287*/ meltfptr[191];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-normatch.melt:2574:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L99*/ meltfnum[23] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V287*/ meltfptr[191] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V286*/ meltfptr[186] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-normatch.melt:2574:/ quasiblock");


      /*_.PROGN___V288*/ meltfptr[115] = /*_.IF___V286*/ meltfptr[186];;
      /*^compute */
      /*_.IFCPP___V285*/ meltfptr[200] = /*_.PROGN___V288*/ meltfptr[115];;
      /*epilog */

      MELT_LOCATION ("warmelt-normatch.melt:2574:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L98*/ meltfnum[41] = 0;
      /*^clear */
	     /*clear *//*_.IF___V286*/ meltfptr[186] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V288*/ meltfptr[115] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V285*/ meltfptr[200] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-normatch.melt:2575:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^cond */
    /*cond */ if (
		   /*ifisa */
		   melt_is_instance_of ((melt_ptr_t)
					( /*_.CLOCC__V281*/ meltfptr[135]),
					(melt_ptr_t) (( /*!CLASS_NREP_SYMOCC */ meltfrout->tabval[38])))
      )				/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  /*^putslot */
	  /*putslot */
	  melt_assertmsg ("putslot checkobj @NOCC_BIND",
			  melt_magic_discr ((melt_ptr_t)
					    ( /*_.CLOCC__V281*/
					     meltfptr[135])) ==
			  MELTOBMAG_OBJECT);
	  melt_putfield_object (( /*_.CLOCC__V281*/ meltfptr[135]), (3),
				( /*_.CBIND__V283*/ meltfptr[178]),
				"NOCC_BIND");
	  ;
	  /*^touch */
	  meltgc_touch ( /*_.CLOCC__V281*/ meltfptr[135]);
	  ;
	  /*^touchobj */

	  melt_dbgtrace_written_object ( /*_.CLOCC__V281*/ meltfptr[135],
					"put-fields");
	  ;
	  /*epilog */
	}
	;
      }				/*noelse */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-normatch.melt:2576:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L100*/ meltfnum[40] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-normatch.melt:2576:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L100*/ meltfnum[40])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L101*/ meltfnum[89] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-normatch.melt:2576:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long =
		/*_#THE_MELTCALLCOUNT__L101*/ meltfnum[89];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-normatch.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 2576;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "normexp_match clocc";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.CLOCC__V281*/ meltfptr[135];
	      /*_.MELT_DEBUG_FUN__V291*/ meltfptr[121] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[3])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V290*/ meltfptr[120] =
	      /*_.MELT_DEBUG_FUN__V291*/ meltfptr[121];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-normatch.melt:2576:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L101*/ meltfnum[89] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V291*/ meltfptr[121] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V290*/ meltfptr[120] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-normatch.melt:2576:/ quasiblock");


      /*_.PROGN___V292*/ meltfptr[123] = /*_.IF___V290*/ meltfptr[120];;
      /*^compute */
      /*_.IFCPP___V289*/ meltfptr[119] = /*_.PROGN___V292*/ meltfptr[123];;
      /*epilog */

      MELT_LOCATION ("warmelt-normatch.melt:2576:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L100*/ meltfnum[40] = 0;
      /*^clear */
	     /*clear *//*_.IF___V290*/ meltfptr[120] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V292*/ meltfptr[123] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V289*/ meltfptr[119] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-normatch.melt:2578:/ cond");
    /*cond */ if (
		   /*ifisa */
		   melt_is_instance_of ((melt_ptr_t)
					( /*_.NCX__V4*/ meltfptr[3]),
					(melt_ptr_t) (( /*!CLASS_NORMALIZATION_CONTEXT */ meltfrout->tabval[2])))
      )				/*then */
      {
	/*^cond.then */
	/*^getslot */
	{
	  melt_ptr_t slot = NULL, obj = NULL;
	  obj = (melt_ptr_t) ( /*_.NCX__V4*/ meltfptr[3]) /*=obj*/ ;
	  melt_object_get_field (slot, obj, 6, "NCTX_SYMBCACHEMAP");
   /*_.NCTX_SYMBCACHEMAP__V293*/ meltfptr[124] = slot;
	};
	;
      }
    else
      {				/*^cond.else */

  /*_.NCTX_SYMBCACHEMAP__V293*/ meltfptr[124] = NULL;;
      }
    ;

    {
      MELT_LOCATION ("warmelt-normatch.melt:2578:/ locexp");
      meltgc_put_mapobjects ((meltmapobjects_ptr_t)
			     ( /*_.NCTX_SYMBCACHEMAP__V293*/ meltfptr[124]),
			     (meltobject_ptr_t) ( /*_.CSYM__V280*/
						 meltfptr[219]),
			     (melt_ptr_t) ( /*_.CLOCC__V281*/ meltfptr[135]));
    }
    ;
    /*citerblock FOREACH_IN_MULTIPLE */
    {
      /* start foreach_in_multiple meltcit6__EACHTUP */
      long meltcit6__EACHTUP_ln =
	melt_multiple_length ((melt_ptr_t) /*_.TESTSTUPL__V277*/
			      meltfptr[224]);
      for ( /*_#TSTIX__L102*/ meltfnum[74] = 0;
	   ( /*_#TSTIX__L102*/ meltfnum[74] >= 0)
	   && ( /*_#TSTIX__L102*/ meltfnum[74] < meltcit6__EACHTUP_ln);
	/*_#TSTIX__L102*/ meltfnum[74]++)
	{
	  /*_.CURTEST__V294*/ meltfptr[150] =
	    melt_multiple_nth ((melt_ptr_t)
			       ( /*_.TESTSTUPL__V277*/ meltfptr[224]),
			       /*_#TSTIX__L102*/ meltfnum[74]);




#if MELT_HAVE_DEBUG
	  MELT_LOCATION ("warmelt-normatch.melt:2583:/ cppif.then");
	  /*^block */
	  /*anyblock */
	  {


	    {
	      /*^locexp */
	      /*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	      melt_dbgcounter++;
#endif
	      ;
	    }
	    ;
	    /*^checksignal */
	    MELT_CHECK_SIGNAL ();
	    ;
    /*_#MELT_NEED_DBG__L103*/ meltfnum[42] =
	      /*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	      ( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	      0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	      ;;
	    MELT_LOCATION ("warmelt-normatch.melt:2583:/ cond");
	    /*cond */ if ( /*_#MELT_NEED_DBG__L103*/ meltfnum[42])	/*then */
	      {
		/*^cond.then */
		/*^block */
		/*anyblock */
		{

      /*_#THE_MELTCALLCOUNT__L104*/ meltfnum[90] =
#ifdef meltcallcount
		    meltcallcount	/* the_meltcallcount */
#else
		    0L
#endif /* meltcallcount the_meltcallcount */
		    ;;
		  MELT_LOCATION ("warmelt-normatch.melt:2583:/ checksignal");
		  MELT_CHECK_SIGNAL ();
		  ;
		  /*^apply */
		  /*apply */
		  {
		    union meltparam_un argtab[5];
		    memset (&argtab, 0, sizeof (argtab));
		    /*^apply.arg */
		    argtab[0].meltbp_long =
		      /*_#THE_MELTCALLCOUNT__L104*/ meltfnum[90];
		    /*^apply.arg */
		    argtab[1].meltbp_cstring = "warmelt-normatch.melt";
		    /*^apply.arg */
		    argtab[2].meltbp_long = 2583;
		    /*^apply.arg */
		    argtab[3].meltbp_cstring = "normexp_match raw curtest";
		    /*^apply.arg */
		    argtab[4].meltbp_aptr =
		      (melt_ptr_t *) & /*_.CURTEST__V294*/ meltfptr[150];
		    /*_.MELT_DEBUG_FUN__V297*/ meltfptr[144] =
		      melt_apply ((meltclosure_ptr_t)
				  (( /*!MELT_DEBUG_FUN */ meltfrout->
				    tabval[3])),
				  (melt_ptr_t) (( /*nil */ NULL)),
				  (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_PTR ""), argtab, "",
				  (union meltparam_un *) 0);
		  }
		  ;
		  /*_.IF___V296*/ meltfptr[129] =
		    /*_.MELT_DEBUG_FUN__V297*/ meltfptr[144];;
		  /*epilog */

		  MELT_LOCATION ("warmelt-normatch.melt:2583:/ clear");
		/*clear *//*_#THE_MELTCALLCOUNT__L104*/ meltfnum[90] = 0;
		  /*^clear */
		/*clear *//*_.MELT_DEBUG_FUN__V297*/ meltfptr[144] = 0;
		}
		;
	      }
	    else
	      {			/*^cond.else */

     /*_.IF___V296*/ meltfptr[129] = NULL;;
	      }
	    ;
	    MELT_LOCATION ("warmelt-normatch.melt:2583:/ quasiblock");


	    /*_.PROGN___V298*/ meltfptr[149] = /*_.IF___V296*/ meltfptr[129];;
	    /*^compute */
	    /*_.IFCPP___V295*/ meltfptr[145] =
	      /*_.PROGN___V298*/ meltfptr[149];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-normatch.melt:2583:/ clear");
	      /*clear *//*_#MELT_NEED_DBG__L103*/ meltfnum[42] = 0;
	    /*^clear */
	      /*clear *//*_.IF___V296*/ meltfptr[129] = 0;
	    /*^clear */
	      /*clear *//*_.PROGN___V298*/ meltfptr[149] = 0;
	  }

#else /*MELT_HAVE_DEBUG */
	  /*^cppif.else */
	  /*_.IFCPP___V295*/ meltfptr[145] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
	  ;

#if MELT_HAVE_DEBUG
	  MELT_LOCATION ("warmelt-normatch.melt:2584:/ cppif.then");
	  /*^block */
	  /*anyblock */
	  {

	    /*^checksignal */
	    MELT_CHECK_SIGNAL ();
	    ;
    /*_#IS_A__L105*/ meltfnum[23] =
	      melt_is_instance_of ((melt_ptr_t)
				   ( /*_.CURTEST__V294*/ meltfptr[150]),
				   (melt_ptr_t) (( /*!CLASS_NORMTESTER_ANY */
						  meltfrout->tabval[26])));;
	    MELT_LOCATION ("warmelt-normatch.melt:2584:/ cond");
	    /*cond */ if ( /*_#IS_A__L105*/ meltfnum[23])	/*then */
	      {
		/*^cond.then */
		/*_.IFELSE___V300*/ meltfptr[58] = ( /*nil */ NULL);;
	      }
	    else
	      {
		MELT_LOCATION ("warmelt-normatch.melt:2584:/ cond.else");

		/*^block */
		/*anyblock */
		{




		  {
		    /*^locexp */
		    melt_assert_failed (("check curtest"),
					("warmelt-normatch.melt")
					? ("warmelt-normatch.melt") :
					__FILE__, (2584) ? (2584) : __LINE__,
					__FUNCTION__);
		    ;
		  }
		  ;
		/*clear *//*_.IFELSE___V300*/ meltfptr[58] = 0;
		  /*epilog */
		}
		;
	      }
	    ;
	    /*^compute */
	    /*_.IFCPP___V299*/ meltfptr[130] =
	      /*_.IFELSE___V300*/ meltfptr[58];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-normatch.melt:2584:/ clear");
	      /*clear *//*_#IS_A__L105*/ meltfnum[23] = 0;
	    /*^clear */
	      /*clear *//*_.IFELSE___V300*/ meltfptr[58] = 0;
	  }

#else /*MELT_HAVE_DEBUG */
	  /*^cppif.else */
	  /*_.IFCPP___V299*/ meltfptr[130] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
	  ;

#if MELT_HAVE_DEBUG
	  MELT_LOCATION ("warmelt-normatch.melt:2585:/ cppif.then");
	  /*^block */
	  /*anyblock */
	  {

	    /*^checksignal */
	    MELT_CHECK_SIGNAL ();
	    ;
    /*_#I__L106*/ meltfnum[41] =
	      (( /*_#TSTIX__L102*/ meltfnum[74]) >= (0));;
	    MELT_LOCATION ("warmelt-normatch.melt:2585:/ cond");
	    /*cond */ if ( /*_#I__L106*/ meltfnum[41])	/*then */
	      {
		/*^cond.then */
		/*_.IFELSE___V302*/ meltfptr[63] = ( /*nil */ NULL);;
	      }
	    else
	      {
		MELT_LOCATION ("warmelt-normatch.melt:2585:/ cond.else");

		/*^block */
		/*anyblock */
		{




		  {
		    /*^locexp */
		    melt_assert_failed (("check tstix"),
					("warmelt-normatch.melt")
					? ("warmelt-normatch.melt") :
					__FILE__, (2585) ? (2585) : __LINE__,
					__FUNCTION__);
		    ;
		  }
		  ;
		/*clear *//*_.IFELSE___V302*/ meltfptr[63] = 0;
		  /*epilog */
		}
		;
	      }
	    ;
	    /*^compute */
	    /*_.IFCPP___V301*/ meltfptr[64] =
	      /*_.IFELSE___V302*/ meltfptr[63];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-normatch.melt:2585:/ clear");
	      /*clear *//*_#I__L106*/ meltfnum[41] = 0;
	    /*^clear */
	      /*clear *//*_.IFELSE___V302*/ meltfptr[63] = 0;
	  }

#else /*MELT_HAVE_DEBUG */
	  /*^cppif.else */
	  /*_.IFCPP___V301*/ meltfptr[64] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
	  ;

#if MELT_HAVE_DEBUG
	  MELT_LOCATION ("warmelt-normatch.melt:2586:/ cppif.then");
	  /*^block */
	  /*anyblock */
	  {

	    /*^checksignal */
	    MELT_CHECK_SIGNAL ();
	    ;
    /*_#I__L107*/ meltfnum[89] =
	      (( /*_#TSTIX__L102*/ meltfnum[74]) < (32767));;
	    MELT_LOCATION ("warmelt-normatch.melt:2586:/ cond");
	    /*cond */ if ( /*_#I__L107*/ meltfnum[89])	/*then */
	      {
		/*^cond.then */
		/*_.IFELSE___V304*/ meltfptr[51] = ( /*nil */ NULL);;
	      }
	    else
	      {
		MELT_LOCATION ("warmelt-normatch.melt:2586:/ cond.else");

		/*^block */
		/*anyblock */
		{




		  {
		    /*^locexp */
		    melt_assert_failed (("check tstix not too big"),
					("warmelt-normatch.melt")
					? ("warmelt-normatch.melt") :
					__FILE__, (2586) ? (2586) : __LINE__,
					__FUNCTION__);
		    ;
		  }
		  ;
		/*clear *//*_.IFELSE___V304*/ meltfptr[51] = 0;
		  /*epilog */
		}
		;
	      }
	    ;
	    /*^compute */
	    /*_.IFCPP___V303*/ meltfptr[68] =
	      /*_.IFELSE___V304*/ meltfptr[51];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-normatch.melt:2586:/ clear");
	      /*clear *//*_#I__L107*/ meltfnum[89] = 0;
	    /*^clear */
	      /*clear *//*_.IFELSE___V304*/ meltfptr[51] = 0;
	  }

#else /*MELT_HAVE_DEBUG */
	  /*^cppif.else */
	  /*_.IFCPP___V303*/ meltfptr[68] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
	  ;

	  {
	    MELT_LOCATION ("warmelt-normatch.melt:2587:/ locexp");
	    debugnum (("normexp_match testindex tstix"),
		      ( /*_#TSTIX__L102*/ meltfnum[74]));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-normatch.melt:2588:/ locexp");
	    melt_put_int ((melt_ptr_t) ( /*_.CURTEST__V294*/ meltfptr[150]),
			  ( /*_#TSTIX__L102*/ meltfnum[74]));
	  }
	  ;
	  MELT_LOCATION ("warmelt-normatch.melt:2589:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^cond */
	  /*cond */ if (
			 /*ifisa */
			 melt_is_instance_of ((melt_ptr_t)
					      ( /*_.CURTEST__V294*/
					       meltfptr[150]),
					      (melt_ptr_t) (( /*!CLASS_NORMTESTER_ANY */ meltfrout->tabval[26])))
	    )			/*then */
	    {
	      /*^cond.then */
	      /*^block */
	      /*anyblock */
	      {

		/*^putslot */
		/*putslot */
		melt_assertmsg ("putslot checkobj @NTEST_NORMATCH",
				melt_magic_discr ((melt_ptr_t)
						  ( /*_.CURTEST__V294*/
						   meltfptr[150])) ==
				MELTOBMAG_OBJECT);
		melt_putfield_object (( /*_.CURTEST__V294*/ meltfptr[150]),
				      (4),
				      ( /*_.NMATCH__V278*/ meltfptr[192]),
				      "NTEST_NORMATCH");
		;
		/*^touch */
		meltgc_touch ( /*_.CURTEST__V294*/ meltfptr[150]);
		;
		/*^touchobj */

		melt_dbgtrace_written_object ( /*_.CURTEST__V294*/
					      meltfptr[150], "put-fields");
		;
		/*epilog */
	      }
	      ;
	    }			/*noelse */
	  ;

#if MELT_HAVE_DEBUG
	  MELT_LOCATION ("warmelt-normatch.melt:2590:/ cppif.then");
	  /*^block */
	  /*anyblock */
	  {


	    {
	      /*^locexp */
	      /*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	      melt_dbgcounter++;
#endif
	      ;
	    }
	    ;
	    /*^checksignal */
	    MELT_CHECK_SIGNAL ();
	    ;
    /*_#MELT_NEED_DBG__L108*/ meltfnum[40] =
	      /*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	      ( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	      0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	      ;;
	    MELT_LOCATION ("warmelt-normatch.melt:2590:/ cond");
	    /*cond */ if ( /*_#MELT_NEED_DBG__L108*/ meltfnum[40])	/*then */
	      {
		/*^cond.then */
		/*^block */
		/*anyblock */
		{

      /*_#THE_MELTCALLCOUNT__L109*/ meltfnum[90] =
#ifdef meltcallcount
		    meltcallcount	/* the_meltcallcount */
#else
		    0L
#endif /* meltcallcount the_meltcallcount */
		    ;;
		  MELT_LOCATION ("warmelt-normatch.melt:2590:/ checksignal");
		  MELT_CHECK_SIGNAL ();
		  ;
		  /*^apply */
		  /*apply */
		  {
		    union meltparam_un argtab[5];
		    memset (&argtab, 0, sizeof (argtab));
		    /*^apply.arg */
		    argtab[0].meltbp_long =
		      /*_#THE_MELTCALLCOUNT__L109*/ meltfnum[90];
		    /*^apply.arg */
		    argtab[1].meltbp_cstring = "warmelt-normatch.melt";
		    /*^apply.arg */
		    argtab[2].meltbp_long = 2590;
		    /*^apply.arg */
		    argtab[3].meltbp_cstring =
		      "normexp_match indexed curtest";
		    /*^apply.arg */
		    argtab[4].meltbp_aptr =
		      (melt_ptr_t *) & /*_.CURTEST__V294*/ meltfptr[150];
		    /*_.MELT_DEBUG_FUN__V307*/ meltfptr[90] =
		      melt_apply ((meltclosure_ptr_t)
				  (( /*!MELT_DEBUG_FUN */ meltfrout->
				    tabval[3])),
				  (melt_ptr_t) (( /*nil */ NULL)),
				  (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_PTR ""), argtab, "",
				  (union meltparam_un *) 0);
		  }
		  ;
		  /*_.IF___V306*/ meltfptr[89] =
		    /*_.MELT_DEBUG_FUN__V307*/ meltfptr[90];;
		  /*epilog */

		  MELT_LOCATION ("warmelt-normatch.melt:2590:/ clear");
		/*clear *//*_#THE_MELTCALLCOUNT__L109*/ meltfnum[90] = 0;
		  /*^clear */
		/*clear *//*_.MELT_DEBUG_FUN__V307*/ meltfptr[90] = 0;
		}
		;
	      }
	    else
	      {			/*^cond.else */

     /*_.IF___V306*/ meltfptr[89] = NULL;;
	      }
	    ;
	    MELT_LOCATION ("warmelt-normatch.melt:2590:/ quasiblock");


	    /*_.PROGN___V308*/ meltfptr[91] = /*_.IF___V306*/ meltfptr[89];;
	    /*^compute */
	    /*_.IFCPP___V305*/ meltfptr[88] =
	      /*_.PROGN___V308*/ meltfptr[91];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-normatch.melt:2590:/ clear");
	      /*clear *//*_#MELT_NEED_DBG__L108*/ meltfnum[40] = 0;
	    /*^clear */
	      /*clear *//*_.IF___V306*/ meltfptr[89] = 0;
	    /*^clear */
	      /*clear *//*_.PROGN___V308*/ meltfptr[91] = 0;
	  }

#else /*MELT_HAVE_DEBUG */
	  /*^cppif.else */
	  /*_.IFCPP___V305*/ meltfptr[88] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
	  ;
	  if ( /*_#TSTIX__L102*/ meltfnum[74] < 0)
	    break;
	}			/* end  foreach_in_multiple meltcit6__EACHTUP */

      /*citerepilog */

      MELT_LOCATION ("warmelt-normatch.melt:2580:/ clear");
	    /*clear *//*_.CURTEST__V294*/ meltfptr[150] = 0;
      /*^clear */
	    /*clear *//*_#TSTIX__L102*/ meltfnum[74] = 0;
      /*^clear */
	    /*clear *//*_.IFCPP___V295*/ meltfptr[145] = 0;
      /*^clear */
	    /*clear *//*_.IFCPP___V299*/ meltfptr[130] = 0;
      /*^clear */
	    /*clear *//*_.IFCPP___V301*/ meltfptr[64] = 0;
      /*^clear */
	    /*clear *//*_.IFCPP___V303*/ meltfptr[68] = 0;
      /*^clear */
	    /*clear *//*_.IFCPP___V305*/ meltfptr[88] = 0;
    }				/*endciterblock FOREACH_IN_MULTIPLE */
    ;
    MELT_LOCATION ("warmelt-normatch.melt:2592:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#MELT_NEED_DBG__L110*/ meltfnum[42] =
      /*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
      ( /*melt_need_dbg */ melt_need_debug ((int) 1))
#else
      0				/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
      ;;
    MELT_LOCATION ("warmelt-normatch.melt:2592:/ cond");
    /*cond */ if ( /*_#MELT_NEED_DBG__L110*/ meltfnum[42])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-normatch.melt:2593:/ quasiblock");


	  /*^cond */
	  /*cond */ if (
			 /*ifisa */
			 melt_is_instance_of ((melt_ptr_t)
					      (( /*!MATCH_GRAPHIC_DOT_PREFIX */ meltfrout->tabval[39])),
					      (melt_ptr_t) (( /*!CLASS_REFERENCE */ meltfrout->tabval[18])))
	    )			/*then */
	    {
	      /*^cond.then */
	      /*^getslot */
	      {
		melt_ptr_t slot = NULL, obj = NULL;
		obj =
		  (melt_ptr_t) (( /*!MATCH_GRAPHIC_DOT_PREFIX */ meltfrout->
				 tabval[39])) /*=obj*/ ;
		melt_object_get_field (slot, obj, 0, "REFERENCED_VALUE");
     /*_.DOTPREFIX__V311*/ meltfptr[96] = slot;
	      };
	      ;
	    }
	  else
	    {			/*^cond.else */

    /*_.DOTPREFIX__V311*/ meltfptr[96] = NULL;;
	    }
	  ;

#if MELT_HAVE_DEBUG
	  MELT_LOCATION ("warmelt-normatch.melt:2594:/ cppif.then");
	  /*^block */
	  /*anyblock */
	  {


	    {
	      /*^locexp */
	      /*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	      melt_dbgcounter++;
#endif
	      ;
	    }
	    ;
	    /*^checksignal */
	    MELT_CHECK_SIGNAL ();
	    ;
     /*_#MELT_NEED_DBG__L111*/ meltfnum[23] =
	      /*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	      ( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	      0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	      ;;
	    MELT_LOCATION ("warmelt-normatch.melt:2594:/ cond");
	    /*cond */ if ( /*_#MELT_NEED_DBG__L111*/ meltfnum[23])	/*then */
	      {
		/*^cond.then */
		/*^block */
		/*anyblock */
		{

       /*_#THE_MELTCALLCOUNT__L112*/ meltfnum[41] =
#ifdef meltcallcount
		    meltcallcount	/* the_meltcallcount */
#else
		    0L
#endif /* meltcallcount the_meltcallcount */
		    ;;
		  MELT_LOCATION ("warmelt-normatch.melt:2594:/ checksignal");
		  MELT_CHECK_SIGNAL ();
		  ;
		  /*^apply */
		  /*apply */
		  {
		    union meltparam_un argtab[7];
		    memset (&argtab, 0, sizeof (argtab));
		    /*^apply.arg */
		    argtab[0].meltbp_long =
		      /*_#THE_MELTCALLCOUNT__L112*/ meltfnum[41];
		    /*^apply.arg */
		    argtab[1].meltbp_cstring = "warmelt-normatch.melt";
		    /*^apply.arg */
		    argtab[2].meltbp_long = 2594;
		    /*^apply.arg */
		    argtab[3].meltbp_cstring =
		      "normexp_match match_graphic_dot_prefix=";
		    /*^apply.arg */
		    argtab[4].meltbp_aptr =
		      (melt_ptr_t *) & ( /*!MATCH_GRAPHIC_DOT_PREFIX */
					meltfrout->tabval[39]);
		    /*^apply.arg */
		    argtab[5].meltbp_cstring = " dotprefix=";
		    /*^apply.arg */
		    argtab[6].meltbp_aptr =
		      (melt_ptr_t *) & /*_.DOTPREFIX__V311*/ meltfptr[96];
		    /*_.MELT_DEBUG_FUN__V314*/ meltfptr[103] =
		      melt_apply ((meltclosure_ptr_t)
				  (( /*!MELT_DEBUG_FUN */ meltfrout->
				    tabval[3])),
				  (melt_ptr_t) (( /*nil */ NULL)),
				  (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_PTR MELTBPARSTR_CSTRING
				   MELTBPARSTR_PTR ""), argtab, "",
				  (union meltparam_un *) 0);
		  }
		  ;
		  /*_.IF___V313*/ meltfptr[102] =
		    /*_.MELT_DEBUG_FUN__V314*/ meltfptr[103];;
		  /*epilog */

		  MELT_LOCATION ("warmelt-normatch.melt:2594:/ clear");
		 /*clear *//*_#THE_MELTCALLCOUNT__L112*/ meltfnum[41] = 0;
		  /*^clear */
		 /*clear *//*_.MELT_DEBUG_FUN__V314*/ meltfptr[103] = 0;
		}
		;
	      }
	    else
	      {			/*^cond.else */

      /*_.IF___V313*/ meltfptr[102] = NULL;;
	      }
	    ;
	    MELT_LOCATION ("warmelt-normatch.melt:2594:/ quasiblock");


	    /*_.PROGN___V315*/ meltfptr[104] = /*_.IF___V313*/ meltfptr[102];;
	    /*^compute */
	    /*_.IFCPP___V312*/ meltfptr[97] =
	      /*_.PROGN___V315*/ meltfptr[104];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-normatch.melt:2594:/ clear");
	       /*clear *//*_#MELT_NEED_DBG__L111*/ meltfnum[23] = 0;
	    /*^clear */
	       /*clear *//*_.IF___V313*/ meltfptr[102] = 0;
	    /*^clear */
	       /*clear *//*_.PROGN___V315*/ meltfptr[104] = 0;
	  }

#else /*MELT_HAVE_DEBUG */
	  /*^cppif.else */
	  /*_.IFCPP___V312*/ meltfptr[97] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
	  ;
	  MELT_LOCATION ("warmelt-normatch.melt:2596:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
   /*_#IS_STRING__L113*/ meltfnum[89] =
	    (melt_magic_discr
	     ((melt_ptr_t) ( /*_.DOTPREFIX__V311*/ meltfptr[96])) ==
	     MELTOBMAG_STRING);;
	  MELT_LOCATION ("warmelt-normatch.melt:2596:/ cond");
	  /*cond */ if ( /*_#IS_STRING__L113*/ meltfnum[89])	/*then */
	    {
	      /*^cond.then */
	      /*^block */
	      /*anyblock */
	      {

		MELT_LOCATION ("warmelt-normatch.melt:2597:/ checksignal");
		MELT_CHECK_SIGNAL ();
		;
		/*^apply */
		/*apply */
		{
		  union meltparam_un argtab[2];
		  memset (&argtab, 0, sizeof (argtab));
		  /*^apply.arg */
		  argtab[0].meltbp_aptr =
		    (melt_ptr_t *) & /*_.DOTPREFIX__V311*/ meltfptr[96];
		  /*^apply.arg */
		  argtab[1].meltbp_aptr =
		    (melt_ptr_t *) & /*_.TESTSTUPL__V277*/ meltfptr[224];
		  /*_.MG_DRAW_MATCH_GRAPHVIZ_FILE__V317*/ meltfptr[113] =
		    melt_apply ((meltclosure_ptr_t)
				(( /*!MG_DRAW_MATCH_GRAPHVIZ_FILE */
				  meltfrout->tabval[40])),
				(melt_ptr_t) ( /*_.NMATCH__V278*/
					      meltfptr[192]),
				(MELTBPARSTR_PTR MELTBPARSTR_PTR ""), argtab,
				"", (union meltparam_un *) 0);
		}
		;
		/*_.IF___V316*/ meltfptr[112] =
		  /*_.MG_DRAW_MATCH_GRAPHVIZ_FILE__V317*/ meltfptr[113];;
		/*epilog */

		MELT_LOCATION ("warmelt-normatch.melt:2596:/ clear");
	       /*clear *//*_.MG_DRAW_MATCH_GRAPHVIZ_FILE__V317*/
		  meltfptr[113] = 0;
	      }
	      ;
	    }
	  else
	    {			/*^cond.else */

    /*_.IF___V316*/ meltfptr[112] = NULL;;
	    }
	  ;
	  /*^compute */
	  /*_.LET___V310*/ meltfptr[95] = /*_.IF___V316*/ meltfptr[112];;

	  MELT_LOCATION ("warmelt-normatch.melt:2593:/ clear");
	     /*clear *//*_.DOTPREFIX__V311*/ meltfptr[96] = 0;
	  /*^clear */
	     /*clear *//*_.IFCPP___V312*/ meltfptr[97] = 0;
	  /*^clear */
	     /*clear *//*_#IS_STRING__L113*/ meltfnum[89] = 0;
	  /*^clear */
	     /*clear *//*_.IF___V316*/ meltfptr[112] = 0;
	  /*_.IF___V309*/ meltfptr[93] = /*_.LET___V310*/ meltfptr[95];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-normatch.melt:2592:/ clear");
	     /*clear *//*_.LET___V310*/ meltfptr[95] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

  /*_.IF___V309*/ meltfptr[93] = NULL;;
      }
    ;
    MELT_LOCATION ("warmelt-normatch.melt:2599:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @NOCC_BIND",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.CLOCC__V281*/ meltfptr[135])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.CLOCC__V281*/ meltfptr[135]), (3),
			  ( /*_.CBIND__V283*/ meltfptr[178]), "NOCC_BIND");
    ;
    /*^touch */
    meltgc_touch ( /*_.CLOCC__V281*/ meltfptr[135]);
    ;
    /*^touchobj */

    melt_dbgtrace_written_object ( /*_.CLOCC__V281*/ meltfptr[135],
				  "put-fields");
    ;


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-normatch.melt:2600:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L114*/ meltfnum[90] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-normatch.melt:2600:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L114*/ meltfnum[90])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L115*/ meltfnum[40] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-normatch.melt:2600:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[7];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long =
		/*_#THE_MELTCALLCOUNT__L115*/ meltfnum[40];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-normatch.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 2600;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "normexp_match final teststupl=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.TESTSTUPL__V277*/ meltfptr[224];
	      /*^apply.arg */
	      argtab[5].meltbp_cstring = " nbindmatx=";
	      /*^apply.arg */
	      argtab[6].meltbp_aptr =
		(melt_ptr_t *) & /*_.NBINDMATX__V38*/ meltfptr[37];
	      /*_.MELT_DEBUG_FUN__V320*/ meltfptr[256] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[3])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V319*/ meltfptr[252] =
	      /*_.MELT_DEBUG_FUN__V320*/ meltfptr[256];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-normatch.melt:2600:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L115*/ meltfnum[40] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V320*/ meltfptr[256] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V319*/ meltfptr[252] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-normatch.melt:2600:/ quasiblock");


      /*_.PROGN___V321*/ meltfptr[244] = /*_.IF___V319*/ meltfptr[252];;
      /*^compute */
      /*_.IFCPP___V318*/ meltfptr[114] = /*_.PROGN___V321*/ meltfptr[244];;
      /*epilog */

      MELT_LOCATION ("warmelt-normatch.melt:2600:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L114*/ meltfnum[90] = 0;
      /*^clear */
	     /*clear *//*_.IF___V319*/ meltfptr[252] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V321*/ meltfptr[244] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V318*/ meltfptr[114] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-normatch.melt:2601:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_LIST__L116*/ meltfnum[41] =
	(melt_magic_discr ((melt_ptr_t) ( /*_.NBINDMATX__V38*/ meltfptr[37]))
	 == MELTOBMAG_LIST);;
      MELT_LOCATION ("warmelt-normatch.melt:2601:/ cond");
      /*cond */ if ( /*_#IS_LIST__L116*/ meltfnum[41])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V323*/ meltfptr[245] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-normatch.melt:2601:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check nbindmatx"),
				  ("warmelt-normatch.melt")
				  ? ("warmelt-normatch.melt") : __FILE__,
				  (2601) ? (2601) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V323*/ meltfptr[245] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V322*/ meltfptr[254] = /*_.IFELSE___V323*/ meltfptr[245];;
      /*epilog */

      MELT_LOCATION ("warmelt-normatch.melt:2601:/ clear");
	     /*clear *//*_#IS_LIST__L116*/ meltfnum[41] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V323*/ meltfptr[245] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V322*/ meltfptr[254] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

    {
      MELT_LOCATION ("warmelt-normatch.melt:2602:/ locexp");
      meltgc_append_list ((melt_ptr_t) ( /*_.NBINDMATX__V38*/ meltfptr[37]),
			  (melt_ptr_t) ( /*_.CBIND__V283*/ meltfptr[178]));
    }
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-normatch.melt:2603:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L117*/ meltfnum[23] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-normatch.melt:2603:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L117*/ meltfnum[23])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L118*/ meltfnum[89] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-normatch.melt:2603:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[11];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long =
		/*_#THE_MELTCALLCOUNT__L118*/ meltfnum[89];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-normatch.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 2603;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "normexp_match final nmatch=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.NMATCH__V278*/ meltfptr[192];
	      /*^apply.arg */
	      argtab[5].meltbp_cstring = " cbind=";
	      /*^apply.arg */
	      argtab[6].meltbp_aptr =
		(melt_ptr_t *) & /*_.CBIND__V283*/ meltfptr[178];
	      /*^apply.arg */
	      argtab[7].meltbp_cstring = "returned nbindmatx=";
	      /*^apply.arg */
	      argtab[8].meltbp_aptr =
		(melt_ptr_t *) & /*_.NBINDMATX__V38*/ meltfptr[37];
	      /*^apply.arg */
	      argtab[9].meltbp_cstring = " returned clocc=";
	      /*^apply.arg */
	      argtab[10].meltbp_aptr =
		(melt_ptr_t *) & /*_.CLOCC__V281*/ meltfptr[135];
	      /*_.MELT_DEBUG_FUN__V326*/ meltfptr[220] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[3])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V325*/ meltfptr[234] =
	      /*_.MELT_DEBUG_FUN__V326*/ meltfptr[220];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-normatch.melt:2603:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L118*/ meltfnum[89] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V326*/ meltfptr[220] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V325*/ meltfptr[234] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-normatch.melt:2603:/ quasiblock");


      /*_.PROGN___V327*/ meltfptr[225] = /*_.IF___V325*/ meltfptr[234];;
      /*^compute */
      /*_.IFCPP___V324*/ meltfptr[235] = /*_.PROGN___V327*/ meltfptr[225];;
      /*epilog */

      MELT_LOCATION ("warmelt-normatch.melt:2603:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L117*/ meltfnum[23] = 0;
      /*^clear */
	     /*clear *//*_.IF___V325*/ meltfptr[234] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V327*/ meltfptr[225] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V324*/ meltfptr[235] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-normatch.melt:2606:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.CLOCC__V281*/ meltfptr[135];;
    MELT_LOCATION ("warmelt-normatch.melt:2606:/ putxtraresult");
    if (!meltxrestab_ || !meltxresdescr_)
      goto labend_rout;
    if (meltxresdescr_[0] != MELTBPAR_PTR)
      goto labend_rout;
    if (meltxrestab_[0].meltbp_aptr)
      *(meltxrestab_[0].meltbp_aptr) =
	(melt_ptr_t) ( /*_.NBINDMATX__V38*/ meltfptr[37]);
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*_.LET___V270*/ meltfptr[246] = /*_.RETURN___V328*/ meltfptr[226];;

    MELT_LOCATION ("warmelt-normatch.melt:2552:/ clear");
	   /*clear *//*_.LET___V271*/ meltfptr[255] = 0;
    /*^clear */
	   /*clear *//*_.TESTSTUPL__V277*/ meltfptr[224] = 0;
    /*^clear */
	   /*clear *//*_.NMATCH__V278*/ meltfptr[192] = 0;
    /*^clear */
	   /*clear *//*_.CSYM__V280*/ meltfptr[219] = 0;
    /*^clear */
	   /*clear *//*_.CLOCC__V281*/ meltfptr[135] = 0;
    /*^clear */
	   /*clear *//*_.CBIND__V283*/ meltfptr[178] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V285*/ meltfptr[200] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V289*/ meltfptr[119] = 0;
    /*^clear */
	   /*clear *//*_.NCTX_SYMBCACHEMAP__V293*/ meltfptr[124] = 0;
    /*^clear */
	   /*clear *//*_#MELT_NEED_DBG__L110*/ meltfnum[42] = 0;
    /*^clear */
	   /*clear *//*_.IF___V309*/ meltfptr[93] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V318*/ meltfptr[114] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V322*/ meltfptr[254] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V324*/ meltfptr[235] = 0;
    /*^clear */
	   /*clear *//*_.RETURN___V328*/ meltfptr[226] = 0;
    /*_.LET___V48*/ meltfptr[45] = /*_.LET___V270*/ meltfptr[246];;

    MELT_LOCATION ("warmelt-normatch.melt:2315:/ clear");
	   /*clear *//*_.CTYP__V49*/ meltfptr[40] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V50*/ meltfptr[44] = 0;
    /*^clear */
	   /*clear *//*_#IS_OBJECT__L16*/ meltfnum[14] = 0;
    /*^clear */
	   /*clear *//*_#NOT__L17*/ meltfnum[9] = 0;
    /*^clear */
	   /*clear *//*_.IF___V54*/ meltfptr[50] = 0;
    /*^clear */
	   /*clear *//*_#NULL__L21*/ meltfnum[19] = 0;
    /*^clear */
	   /*clear *//*_.IF___V73*/ meltfptr[55] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V262*/ meltfptr[247] = 0;
    /*^clear */
	   /*clear *//*_.LET___V270*/ meltfptr[246] = 0;
    MELT_LOCATION ("warmelt-normatch.melt:2307:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*_.MULTI___V36*/ meltfptr[32] = /*_.LET___V48*/ meltfptr[45];;

    MELT_LOCATION ("warmelt-normatch.melt:2307:/ clear");
	   /*clear *//*_.IFCPP___V39*/ meltfptr[38] = 0;
    /*^clear */
	   /*clear *//*_#IS_LIST__L13*/ meltfnum[8] = 0;
    /*^clear */
	   /*clear *//*_.IFELSE___V43*/ meltfptr[39] = 0;
    /*^clear */
	   /*clear *//*_.LET___V48*/ meltfptr[45] = 0;

    /*^clear */
	   /*clear *//*_.NBINDMATX__V38*/ meltfptr[37] = 0;
    /*_.LET___V16*/ meltfptr[12] = /*_.MULTI___V36*/ meltfptr[32];;

    MELT_LOCATION ("warmelt-normatch.melt:2285:/ clear");
	   /*clear *//*_.SLOC__V17*/ meltfptr[13] = 0;
    /*^clear */
	   /*clear *//*_.SMATSX__V18*/ meltfptr[17] = 0;
    /*^clear */
	   /*clear *//*_.SCASES__V19*/ meltfptr[18] = 0;
    /*^clear */
	   /*clear *//*_#NBCASES__L6*/ meltfnum[4] = 0;
    /*^clear */
	   /*clear *//*_.TUPVARMAP__V20*/ meltfptr[19] = 0;
    /*^clear */
	   /*clear *//*_.TUPCSTMAP__V21*/ meltfptr[20] = 0;
    /*^clear */
	   /*clear *//*_#I__L7*/ meltfnum[0] = 0;
    /*^clear */
	   /*clear *//*_#I__L8*/ meltfnum[7] = 0;
    /*^clear */
	   /*clear *//*_.STUFFMAP__V22*/ meltfptr[21] = 0;
    /*^clear */
	   /*clear *//*_.SHABINDLIST__V23*/ meltfptr[22] = 0;
    /*^clear */
	   /*clear *//*_.TESTLIST__V24*/ meltfptr[23] = 0;
    /*^clear */
	   /*clear *//*_.WHOLECTYPE__V25*/ meltfptr[24] = 0;
    /*^clear */
	   /*clear *//*_.OLDTESTER__V26*/ meltfptr[25] = 0;
    /*^clear */
	   /*clear *//*_.CINTSYMB__V27*/ meltfptr[26] = 0;
    /*^clear */
	   /*clear *//*_.NCHINT__V28*/ meltfptr[27] = 0;
    /*^clear */
	   /*clear *//*_.CINTBIND__V30*/ meltfptr[29] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V32*/ meltfptr[31] = 0;
    /*^clear */
	   /*clear *//*_.MULTI___V36*/ meltfptr[32] = 0;
    MELT_LOCATION ("warmelt-normatch.melt:2280:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.LET___V16*/ meltfptr[12];;

    {
      MELT_LOCATION ("warmelt-normatch.melt:2280:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V6*/ meltfptr[5] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V8*/ meltfptr[6] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V10*/ meltfptr[8] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V12*/ meltfptr[10] = 0;
    /*^clear */
	   /*clear *//*_.LET___V16*/ meltfptr[12] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("NORMEXP_MATCH", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_51_warmelt_normatch_NORMEXP_MATCH_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_51_warmelt_normatch_NORMEXP_MATCH */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_52_warmelt_normatch_LAMBDA___18__ (meltclosure_ptr_t meltclosp_,
					    melt_ptr_t meltfirstargp_,
					    const melt_argdescr_cell_t
					    meltxargdescr_[],
					    union meltparam_un * meltxargtab_,
					    const melt_argdescr_cell_t
					    meltxresdescr_[],
					    union meltparam_un * meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_52_warmelt_normatch_LAMBDA___18___melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_52_warmelt_normatch_LAMBDA___18___st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 9
    melt_ptr_t mcfr_varptr[9];
#define MELTFRAM_NBVARNUM 3
    long mcfr_varnum[3];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_52_warmelt_normatch_LAMBDA___18__ is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_52_warmelt_normatch_LAMBDA___18___st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 9; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_52_warmelt_normatch_LAMBDA___18__ nbval 9*/
  meltfram__.mcfr_nbvar = 9 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("LAMBDA_", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-normatch.melt:2389:/ getarg");
 /*_.TESTER__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-normatch.melt:2390:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L1*/ meltfnum[0] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-normatch.melt:2390:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L1*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-normatch.melt:2390:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-normatch.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 2390;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "normexp_match.lambda tester";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.TESTER__V2*/ meltfptr[1];
	      /*_.MELT_DEBUG_FUN__V5*/ meltfptr[4] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V4*/ meltfptr[3] = /*_.MELT_DEBUG_FUN__V5*/ meltfptr[4];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-normatch.melt:2390:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V5*/ meltfptr[4] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V4*/ meltfptr[3] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-normatch.melt:2390:/ quasiblock");


      /*_.PROGN___V6*/ meltfptr[4] = /*_.IF___V4*/ meltfptr[3];;
      /*^compute */
      /*_.IFCPP___V3*/ meltfptr[2] = /*_.PROGN___V6*/ meltfptr[4];;
      /*epilog */

      MELT_LOCATION ("warmelt-normatch.melt:2390:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L1*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IF___V4*/ meltfptr[3] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V6*/ meltfptr[4] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V3*/ meltfptr[2] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

    {
      MELT_LOCATION ("warmelt-normatch.melt:2391:/ locexp");

#if MELT_HAVE_DEBUG
      if (melt_need_debug (0))
	melt_dbgshortbacktrace (("normexp_match lambda tester"), (12));
#endif
      ;
    }
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-normatch.melt:2392:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
      MELT_LOCATION ("warmelt-normatch.melt:2393:/ getslot");
      {
	melt_ptr_t slot = NULL, obj = NULL;
	obj =
	  (melt_ptr_t) (( /*~NTESTCONT */ meltfclos->tabval[0])) /*=obj*/ ;
	melt_object_get_field (slot, obj, 0, "REFERENCED_VALUE");
    /*_.REFERENCED_VALUE__V8*/ meltfptr[4] = slot;
      };
      ;
   /*_#NULL__L3*/ meltfnum[1] =
	(( /*_.REFERENCED_VALUE__V8*/ meltfptr[4]) == NULL);;
      MELT_LOCATION ("warmelt-normatch.melt:2392:/ cond");
      /*cond */ if ( /*_#NULL__L3*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V9*/ meltfptr[8] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-normatch.melt:2392:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check empty ntestcont"),
				  ("warmelt-normatch.melt")
				  ? ("warmelt-normatch.melt") : __FILE__,
				  (2392) ? (2392) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V9*/ meltfptr[8] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V7*/ meltfptr[3] = /*_.IFELSE___V9*/ meltfptr[8];;
      /*epilog */

      MELT_LOCATION ("warmelt-normatch.melt:2392:/ clear");
	     /*clear *//*_.REFERENCED_VALUE__V8*/ meltfptr[4] = 0;
      /*^clear */
	     /*clear *//*_#NULL__L3*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V9*/ meltfptr[8] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V7*/ meltfptr[3] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-normatch.melt:2394:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^cond */
    /*cond */ if (
		   /*ifisa */
		   melt_is_instance_of ((melt_ptr_t)
					(( /*~NTESTCONT */ meltfclos->
					  tabval[0])),
					(melt_ptr_t) (( /*!CLASS_REFERENCE */
						       meltfrout->tabval[1])))
      )				/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  /*^putslot */
	  /*putslot */
	  melt_assertmsg ("putslot checkobj @REFERENCED_VALUE",
			  melt_magic_discr ((melt_ptr_t)
					    (( /*~NTESTCONT */ meltfclos->
					      tabval[0]))) ==
			  MELTOBMAG_OBJECT);
	  melt_putfield_object ((( /*~NTESTCONT */ meltfclos->tabval[0])),
				(0), ( /*_.TESTER__V2*/ meltfptr[1]),
				"REFERENCED_VALUE");
	  ;
	  /*^touch */
	  meltgc_touch (( /*~NTESTCONT */ meltfclos->tabval[0]));
	  ;
	  /*^touchobj */

	  melt_dbgtrace_written_object (( /*~NTESTCONT */ meltfclos->
					 tabval[0]), "put-fields");
	  ;
	  /*epilog */
	}
	;
      }				/*noelse */
    ;
    MELT_LOCATION ("warmelt-normatch.melt:2389:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V3*/ meltfptr[2] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V7*/ meltfptr[3] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("LAMBDA_", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_52_warmelt_normatch_LAMBDA___18___melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*noretval */ NULL);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_52_warmelt_normatch_LAMBDA___18__ */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_53_warmelt_normatch_NORMBIND_MATCHBIND (meltclosure_ptr_t meltclosp_,
						 melt_ptr_t meltfirstargp_,
						 const melt_argdescr_cell_t
						 meltxargdescr_[],
						 union meltparam_un *
						 meltxargtab_,
						 const melt_argdescr_cell_t
						 meltxresdescr_[],
						 union meltparam_un *
						 meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_53_warmelt_normatch_NORMBIND_MATCHBIND_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_53_warmelt_normatch_NORMBIND_MATCHBIND_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 33
    melt_ptr_t mcfr_varptr[33];
#define MELTFRAM_NBVARNUM 11
    long mcfr_varnum[11];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_53_warmelt_normatch_NORMBIND_MATCHBIND is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_53_warmelt_normatch_NORMBIND_MATCHBIND_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 33; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_53_warmelt_normatch_NORMBIND_MATCHBIND nbval 33*/
  meltfram__.mcfr_nbvar = 33 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("NORMBIND_MATCHBIND", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-normatch.melt:2690:/ getarg");
 /*_.BIND__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.ENV__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.ENV__V3*/ meltfptr[2])) != NULL);


  /*getarg#2 */
  /*^getarg */
  if (meltxargdescr_[1] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.NCX__V4*/ meltfptr[3] =
    (meltxargtab_[1].meltbp_aptr) ? (*(meltxargtab_[1].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.NCX__V4*/ meltfptr[3])) != NULL);


  /*getarg#3 */
  /*^getarg */
  if (meltxargdescr_[2] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.PROCS__V5*/ meltfptr[4] =
    (meltxargtab_[2].meltbp_aptr) ? (*(meltxargtab_[2].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.PROCS__V5*/ meltfptr[4])) !=
	      NULL);


  /*getarg#4 */
  /*^getarg */
  if (meltxargdescr_[3] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.PSLOC__V6*/ meltfptr[5] =
    (meltxargtab_[3].meltbp_aptr) ? (*(meltxargtab_[3].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.PSLOC__V6*/ meltfptr[5])) !=
	      NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-normatch.melt:2691:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L1*/ meltfnum[0] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.BIND__V2*/ meltfptr[1]),
			     (melt_ptr_t) (( /*!CLASS_MATCHED_BINDING */
					    meltfrout->tabval[0])));;
      MELT_LOCATION ("warmelt-normatch.melt:2691:/ cond");
      /*cond */ if ( /*_#IS_A__L1*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V8*/ meltfptr[7] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-normatch.melt:2691:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check bind"),
				  ("warmelt-normatch.melt")
				  ? ("warmelt-normatch.melt") : __FILE__,
				  (2691) ? (2691) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V8*/ meltfptr[7] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V7*/ meltfptr[6] = /*_.IFELSE___V8*/ meltfptr[7];;
      /*epilog */

      MELT_LOCATION ("warmelt-normatch.melt:2691:/ clear");
	     /*clear *//*_#IS_A__L1*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V8*/ meltfptr[7] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V7*/ meltfptr[6] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-normatch.melt:2692:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L2*/ meltfnum[0] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.ENV__V3*/ meltfptr[2]),
			     (melt_ptr_t) (( /*!CLASS_ENVIRONMENT */
					    meltfrout->tabval[1])));;
      MELT_LOCATION ("warmelt-normatch.melt:2692:/ cond");
      /*cond */ if ( /*_#IS_A__L2*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V10*/ meltfptr[9] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-normatch.melt:2692:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check env"),
				  ("warmelt-normatch.melt")
				  ? ("warmelt-normatch.melt") : __FILE__,
				  (2692) ? (2692) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V10*/ meltfptr[9] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V9*/ meltfptr[7] = /*_.IFELSE___V10*/ meltfptr[9];;
      /*epilog */

      MELT_LOCATION ("warmelt-normatch.melt:2692:/ clear");
	     /*clear *//*_#IS_A__L2*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V10*/ meltfptr[9] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V9*/ meltfptr[7] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-normatch.melt:2693:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L3*/ meltfnum[0] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.NCX__V4*/ meltfptr[3]),
			     (melt_ptr_t) (( /*!CLASS_NORMALIZATION_CONTEXT */
					    meltfrout->tabval[2])));;
      MELT_LOCATION ("warmelt-normatch.melt:2693:/ cond");
      /*cond */ if ( /*_#IS_A__L3*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V12*/ meltfptr[11] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-normatch.melt:2693:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check ncx"),
				  ("warmelt-normatch.melt")
				  ? ("warmelt-normatch.melt") : __FILE__,
				  (2693) ? (2693) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V12*/ meltfptr[11] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V11*/ meltfptr[9] = /*_.IFELSE___V12*/ meltfptr[11];;
      /*epilog */

      MELT_LOCATION ("warmelt-normatch.melt:2693:/ clear");
	     /*clear *//*_#IS_A__L3*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V12*/ meltfptr[11] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V11*/ meltfptr[9] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-normatch.melt:2694:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L4*/ meltfnum[0] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-normatch.melt:2694:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L4*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L5*/ meltfnum[4] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-normatch.melt:2694:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[7];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L5*/ meltfnum[4];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-normatch.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 2694;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "normbind_matchbind bind=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.BIND__V2*/ meltfptr[1];
	      /*^apply.arg */
	      argtab[5].meltbp_cstring = "\n ncx=";
	      /*^apply.arg */
	      argtab[6].meltbp_aptr =
		(melt_ptr_t *) & /*_.NCX__V4*/ meltfptr[3];
	      /*_.MELT_DEBUG_FUN__V15*/ meltfptr[14] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[3])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V14*/ meltfptr[13] =
	      /*_.MELT_DEBUG_FUN__V15*/ meltfptr[14];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-normatch.melt:2694:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L5*/ meltfnum[4] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V15*/ meltfptr[14] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V14*/ meltfptr[13] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-normatch.melt:2694:/ quasiblock");


      /*_.PROGN___V16*/ meltfptr[14] = /*_.IF___V14*/ meltfptr[13];;
      /*^compute */
      /*_.IFCPP___V13*/ meltfptr[11] = /*_.PROGN___V16*/ meltfptr[14];;
      /*epilog */

      MELT_LOCATION ("warmelt-normatch.melt:2694:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L4*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IF___V14*/ meltfptr[13] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V16*/ meltfptr[14] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V13*/ meltfptr[11] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

    {
      MELT_LOCATION ("warmelt-normatch.melt:2695:/ locexp");

#if MELT_HAVE_DEBUG
      if (melt_need_debug (0))
	melt_dbgshortbacktrace (("normbind_matchbind"), (18));
#endif
      ;
    }
    ;
    MELT_LOCATION ("warmelt-normatch.melt:2696:/ quasiblock");


    /*^getslot */
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.NCX__V4*/ meltfptr[3]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 6, "NCTX_SYMBCACHEMAP");
  /*_.SYCMAP__V18*/ meltfptr[14] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-normatch.melt:2697:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.BIND__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 0, "BINDER");
  /*_.SYMB__V19*/ meltfptr[18] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-normatch.melt:2698:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.BIND__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 1, "MATCHBIND_DATA");
  /*_.MDATA__V20*/ meltfptr[19] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-normatch.melt:2699:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.BIND__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 2, "MATCHBIND_NBOCC");
  /*_.BOXNBOCC__V21*/ meltfptr[20] = slot;
    };
    ;
 /*_#NBOCC__L6*/ meltfnum[4] =
      (melt_get_int ((melt_ptr_t) ( /*_.BOXNBOCC__V21*/ meltfptr[20])));;
    MELT_LOCATION ("warmelt-normatch.melt:2702:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    MELT_LOCATION ("warmelt-normatch.melt:2704:/ cond");
    /*cond */ if (
		   /*ifisa */
		   melt_is_instance_of ((melt_ptr_t)
					( /*_.MDATA__V20*/ meltfptr[19]),
					(melt_ptr_t) (( /*!CLASS_MATCHED_DATA */ meltfrout->tabval[5])))
      )				/*then */
      {
	/*^cond.then */
	/*^getslot */
	{
	  melt_ptr_t slot = NULL, obj = NULL;
	  obj = (melt_ptr_t) ( /*_.MDATA__V20*/ meltfptr[19]) /*=obj*/ ;
	  melt_object_get_field (slot, obj, 2, "MDATA_CTYPE");
   /*_.MDATA_CTYPE__V22*/ meltfptr[21] = slot;
	};
	;
      }
    else
      {				/*^cond.else */

  /*_.MDATA_CTYPE__V22*/ meltfptr[21] = NULL;;
      }
    ;
    MELT_LOCATION ("warmelt-normatch.melt:2702:/ quasiblock");


    /*^rawallocobj */
    /*rawallocobj */
    {
      melt_ptr_t newobj = 0;
      melt_raw_object_create (newobj,
			      (melt_ptr_t) (( /*!CLASS_NREP_LOCSYMOCC */
					     meltfrout->tabval[4])), (4),
			      "CLASS_NREP_LOCSYMOCC");
  /*_.INST__V24*/ meltfptr[23] =
	newobj;
    };
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @NREP_LOC",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V24*/ meltfptr[23])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V24*/ meltfptr[23]), (0),
			  ( /*_.PSLOC__V6*/ meltfptr[5]), "NREP_LOC");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @NOCC_CTYP",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V24*/ meltfptr[23])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V24*/ meltfptr[23]), (2),
			  ( /*_.MDATA_CTYPE__V22*/ meltfptr[21]),
			  "NOCC_CTYP");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @NOCC_SYMB",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V24*/ meltfptr[23])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V24*/ meltfptr[23]), (1),
			  ( /*_.SYMB__V19*/ meltfptr[18]), "NOCC_SYMB");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @NOCC_BIND",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V24*/ meltfptr[23])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V24*/ meltfptr[23]), (3),
			  ( /*_.BIND__V2*/ meltfptr[1]), "NOCC_BIND");
    ;
    /*^touchobj */

    melt_dbgtrace_written_object ( /*_.INST__V24*/ meltfptr[23],
				  "newly made instance");
    ;
    /*_.SYOCC__V23*/ meltfptr[22] = /*_.INST__V24*/ meltfptr[23];;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-normatch.melt:2709:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L7*/ meltfnum[0] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-normatch.melt:2709:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L7*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L8*/ meltfnum[7] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    /*^compute */
     /*_.DISCRIM__V27*/ meltfptr[26] =
	      ((melt_ptr_t)
	       (melt_discr ((melt_ptr_t) ( /*_.MDATA__V20*/ meltfptr[19]))));;
	    MELT_LOCATION ("warmelt-normatch.melt:2709:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[9];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L8*/ meltfnum[7];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-normatch.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 2709;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "normbind_matchbind syocc=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.SYOCC__V23*/ meltfptr[22];
	      /*^apply.arg */
	      argtab[5].meltbp_cstring = "\n* mdata=";
	      /*^apply.arg */
	      argtab[6].meltbp_aptr =
		(melt_ptr_t *) & /*_.MDATA__V20*/ meltfptr[19];
	      /*^apply.arg */
	      argtab[7].meltbp_cstring = "\n of discrim:";
	      /*^apply.arg */
	      argtab[8].meltbp_aptr =
		(melt_ptr_t *) & /*_.DISCRIM__V27*/ meltfptr[26];
	      /*_.MELT_DEBUG_FUN__V28*/ meltfptr[27] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[3])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V26*/ meltfptr[25] =
	      /*_.MELT_DEBUG_FUN__V28*/ meltfptr[27];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-normatch.melt:2709:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L8*/ meltfnum[7] = 0;
	    /*^clear */
	       /*clear *//*_.DISCRIM__V27*/ meltfptr[26] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V28*/ meltfptr[27] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V26*/ meltfptr[25] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-normatch.melt:2709:/ quasiblock");


      /*_.PROGN___V29*/ meltfptr[26] = /*_.IF___V26*/ meltfptr[25];;
      /*^compute */
      /*_.IFCPP___V25*/ meltfptr[24] = /*_.PROGN___V29*/ meltfptr[26];;
      /*epilog */

      MELT_LOCATION ("warmelt-normatch.melt:2709:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L7*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IF___V26*/ meltfptr[25] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V29*/ meltfptr[26] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V25*/ meltfptr[24] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    /*^compute */
 /*_#I__L9*/ meltfnum[7] =
      (( /*_#NBOCC__L6*/ meltfnum[4]) + (1));;

    {
      MELT_LOCATION ("warmelt-normatch.melt:2712:/ locexp");
      melt_put_int ((melt_ptr_t) ( /*_.BOXNBOCC__V21*/ meltfptr[20]),
		    ( /*_#I__L9*/ meltfnum[7]));
    }
    ;

    {
      MELT_LOCATION ("warmelt-normatch.melt:2714:/ locexp");
      meltgc_put_mapobjects ((meltmapobjects_ptr_t)
			     ( /*_.SYCMAP__V18*/ meltfptr[14]),
			     (meltobject_ptr_t) ( /*_.SYMB__V19*/
						 meltfptr[18]),
			     (melt_ptr_t) ( /*_.SYOCC__V23*/ meltfptr[22]));
    }
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-normatch.melt:2715:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L10*/ meltfnum[0] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-normatch.melt:2715:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L10*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L11*/ meltfnum[10] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-normatch.melt:2715:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[7];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long =
		/*_#THE_MELTCALLCOUNT__L11*/ meltfnum[10];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-normatch.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 2715;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "normbind_matchbind updated sycmap=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.SYCMAP__V18*/ meltfptr[14];
	      /*^apply.arg */
	      argtab[5].meltbp_cstring = "\n for syocc=";
	      /*^apply.arg */
	      argtab[6].meltbp_aptr =
		(melt_ptr_t *) & /*_.SYOCC__V23*/ meltfptr[22];
	      /*_.MELT_DEBUG_FUN__V32*/ meltfptr[26] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[3])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V31*/ meltfptr[25] =
	      /*_.MELT_DEBUG_FUN__V32*/ meltfptr[26];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-normatch.melt:2715:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L11*/ meltfnum[10] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V32*/ meltfptr[26] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V31*/ meltfptr[25] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-normatch.melt:2715:/ quasiblock");


      /*_.PROGN___V33*/ meltfptr[26] = /*_.IF___V31*/ meltfptr[25];;
      /*^compute */
      /*_.IFCPP___V30*/ meltfptr[27] = /*_.PROGN___V33*/ meltfptr[26];;
      /*epilog */

      MELT_LOCATION ("warmelt-normatch.melt:2715:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L10*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IF___V31*/ meltfptr[25] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V33*/ meltfptr[26] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V30*/ meltfptr[27] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    /*^compute */
    /*_.LET___V17*/ meltfptr[13] = /*_.SYOCC__V23*/ meltfptr[22];;

    MELT_LOCATION ("warmelt-normatch.melt:2696:/ clear");
	   /*clear *//*_.SYCMAP__V18*/ meltfptr[14] = 0;
    /*^clear */
	   /*clear *//*_.SYMB__V19*/ meltfptr[18] = 0;
    /*^clear */
	   /*clear *//*_.MDATA__V20*/ meltfptr[19] = 0;
    /*^clear */
	   /*clear *//*_.BOXNBOCC__V21*/ meltfptr[20] = 0;
    /*^clear */
	   /*clear *//*_#NBOCC__L6*/ meltfnum[4] = 0;
    /*^clear */
	   /*clear *//*_.MDATA_CTYPE__V22*/ meltfptr[21] = 0;
    /*^clear */
	   /*clear *//*_.SYOCC__V23*/ meltfptr[22] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V25*/ meltfptr[24] = 0;
    /*^clear */
	   /*clear *//*_#I__L9*/ meltfnum[7] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V30*/ meltfptr[27] = 0;
    MELT_LOCATION ("warmelt-normatch.melt:2690:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.LET___V17*/ meltfptr[13];;

    {
      MELT_LOCATION ("warmelt-normatch.melt:2690:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V7*/ meltfptr[6] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V9*/ meltfptr[7] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V11*/ meltfptr[9] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V13*/ meltfptr[11] = 0;
    /*^clear */
	   /*clear *//*_.LET___V17*/ meltfptr[13] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("NORMBIND_MATCHBIND", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_53_warmelt_normatch_NORMBIND_MATCHBIND_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_53_warmelt_normatch_NORMBIND_MATCHBIND */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_54_warmelt_normatch_MAKE_MATCH_FLAG (meltclosure_ptr_t meltclosp_,
					      melt_ptr_t meltfirstargp_,
					      const melt_argdescr_cell_t
					      meltxargdescr_[],
					      union meltparam_un *
					      meltxargtab_,
					      const melt_argdescr_cell_t
					      meltxresdescr_[],
					      union meltparam_un *
					      meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_54_warmelt_normatch_MAKE_MATCH_FLAG_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_54_warmelt_normatch_MAKE_MATCH_FLAG_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 32
    melt_ptr_t mcfr_varptr[32];
#define MELTFRAM_NBVARNUM 13
    long mcfr_varnum[13];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_54_warmelt_normatch_MAKE_MATCH_FLAG is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_54_warmelt_normatch_MAKE_MATCH_FLAG_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 32; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_54_warmelt_normatch_MAKE_MATCH_FLAG nbval 32*/
  meltfram__.mcfr_nbvar = 32 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("MAKE_MATCH_FLAG", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-normatch.melt:2894:/ getarg");
 /*_.SPAT__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.MCX__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.MCX__V3*/ meltfptr[2])) != NULL);


  /*getarg#2 */
  /*^getarg */
  if (meltxargdescr_[1] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.STR__V4*/ meltfptr[3] =
    (meltxargtab_[1].meltbp_aptr) ? (*(meltxargtab_[1].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.STR__V4*/ meltfptr[3])) != NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-normatch.melt:2895:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L1*/ meltfnum[0] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-normatch.melt:2895:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L1*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-normatch.melt:2895:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[9];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-normatch.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 2895;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "make_match_flag spat=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.SPAT__V2*/ meltfptr[1];
	      /*^apply.arg */
	      argtab[5].meltbp_cstring = " mcx=";
	      /*^apply.arg */
	      argtab[6].meltbp_aptr =
		(melt_ptr_t *) & /*_.MCX__V3*/ meltfptr[2];
	      /*^apply.arg */
	      argtab[7].meltbp_cstring = " str=";
	      /*^apply.arg */
	      argtab[8].meltbp_aptr =
		(melt_ptr_t *) & /*_.STR__V4*/ meltfptr[3];
	      /*_.MELT_DEBUG_FUN__V7*/ meltfptr[6] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V6*/ meltfptr[5] = /*_.MELT_DEBUG_FUN__V7*/ meltfptr[6];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-normatch.melt:2895:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V7*/ meltfptr[6] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V6*/ meltfptr[5] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-normatch.melt:2895:/ quasiblock");


      /*_.PROGN___V8*/ meltfptr[6] = /*_.IF___V6*/ meltfptr[5];;
      /*^compute */
      /*_.IFCPP___V5*/ meltfptr[4] = /*_.PROGN___V8*/ meltfptr[6];;
      /*epilog */

      MELT_LOCATION ("warmelt-normatch.melt:2895:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L1*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IF___V6*/ meltfptr[5] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V8*/ meltfptr[6] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V5*/ meltfptr[4] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-normatch.melt:2896:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L3*/ meltfnum[1] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.SPAT__V2*/ meltfptr[1]),
			     (melt_ptr_t) (( /*!CLASS_SOURCE_PATTERN */
					    meltfrout->tabval[1])));;
      MELT_LOCATION ("warmelt-normatch.melt:2896:/ cond");
      /*cond */ if ( /*_#IS_A__L3*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V10*/ meltfptr[6] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-normatch.melt:2896:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check spat"),
				  ("warmelt-normatch.melt")
				  ? ("warmelt-normatch.melt") : __FILE__,
				  (2896) ? (2896) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V10*/ meltfptr[6] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V9*/ meltfptr[5] = /*_.IFELSE___V10*/ meltfptr[6];;
      /*epilog */

      MELT_LOCATION ("warmelt-normatch.melt:2896:/ clear");
	     /*clear *//*_#IS_A__L3*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V10*/ meltfptr[6] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V9*/ meltfptr[5] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-normatch.melt:2897:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L4*/ meltfnum[0] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.MCX__V3*/ meltfptr[2]),
			     (melt_ptr_t) (( /*!CLASS_MATCHING_CONTEXT */
					    meltfrout->tabval[2])));;
      MELT_LOCATION ("warmelt-normatch.melt:2897:/ cond");
      /*cond */ if ( /*_#IS_A__L4*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V12*/ meltfptr[11] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-normatch.melt:2897:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check mcx"),
				  ("warmelt-normatch.melt")
				  ? ("warmelt-normatch.melt") : __FILE__,
				  (2897) ? (2897) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V12*/ meltfptr[11] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V11*/ meltfptr[6] = /*_.IFELSE___V12*/ meltfptr[11];;
      /*epilog */

      MELT_LOCATION ("warmelt-normatch.melt:2897:/ clear");
	     /*clear *//*_#IS_A__L4*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V12*/ meltfptr[11] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V11*/ meltfptr[6] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-normatch.melt:2898:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_STRING__L5*/ meltfnum[1] =
	(melt_magic_discr ((melt_ptr_t) ( /*_.STR__V4*/ meltfptr[3])) ==
	 MELTOBMAG_STRING);;
      MELT_LOCATION ("warmelt-normatch.melt:2898:/ cond");
      /*cond */ if ( /*_#IS_STRING__L5*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V14*/ meltfptr[13] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-normatch.melt:2898:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check str"),
				  ("warmelt-normatch.melt")
				  ? ("warmelt-normatch.melt") : __FILE__,
				  (2898) ? (2898) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V14*/ meltfptr[13] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V13*/ meltfptr[11] = /*_.IFELSE___V14*/ meltfptr[13];;
      /*epilog */

      MELT_LOCATION ("warmelt-normatch.melt:2898:/ clear");
	     /*clear *//*_#IS_STRING__L5*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V14*/ meltfptr[13] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V13*/ meltfptr[11] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-normatch.melt:2899:/ quasiblock");


    /*^getslot */
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.SPAT__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 1, "LOCA_LOCATION");
  /*_.SLOC__V16*/ meltfptr[15] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-normatch.melt:2900:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.MCX__V3*/ meltfptr[2]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 8, "MCTX_FLAGS");
  /*_.FLAGLIST__V17*/ meltfptr[16] = slot;
    };
    ;
 /*_.LIST_LAST__V18*/ meltfptr[17] =
      (melt_list_last ((melt_ptr_t) ( /*_.FLAGLIST__V17*/ meltfptr[16])));;
    /*^compute */
 /*_.LASTFLAG__V19*/ meltfptr[18] =
      (melt_pair_head ((melt_ptr_t) ( /*_.LIST_LAST__V18*/ meltfptr[17])));;
    MELT_LOCATION ("warmelt-normatch.melt:2902:/ cond");
    /*cond */ if (
		   /*ifisa */
		   melt_is_instance_of ((melt_ptr_t)
					( /*_.LASTFLAG__V19*/ meltfptr[18]),
					(melt_ptr_t) (( /*!CLASS_MATCH_FLAG */
						       meltfrout->tabval[3])))
      )				/*then */
      {
	/*^cond.then */
	/*^getslot */
	{
	  melt_ptr_t slot = NULL, obj = NULL;
	  obj = (melt_ptr_t) ( /*_.LASTFLAG__V19*/ meltfptr[18]) /*=obj*/ ;
	  melt_object_get_field (slot, obj, 3, "MFLAG_RANK");
   /*_.MFLAG_RANK__V20*/ meltfptr[19] = slot;
	};
	;
      }
    else
      {				/*^cond.else */

  /*_.MFLAG_RANK__V20*/ meltfptr[19] = NULL;;
      }
    ;
    /*^compute */
 /*_#LASTRANK__L6*/ meltfnum[0] =
      (melt_get_int ((melt_ptr_t) ( /*_.MFLAG_RANK__V20*/ meltfptr[19])));;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-normatch.melt:2904:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_LIST__L7*/ meltfnum[1] =
	(melt_magic_discr ((melt_ptr_t) ( /*_.FLAGLIST__V17*/ meltfptr[16]))
	 == MELTOBMAG_LIST);;
      MELT_LOCATION ("warmelt-normatch.melt:2904:/ cond");
      /*cond */ if ( /*_#IS_LIST__L7*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V22*/ meltfptr[21] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-normatch.melt:2904:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check flaglist"),
				  ("warmelt-normatch.melt")
				  ? ("warmelt-normatch.melt") : __FILE__,
				  (2904) ? (2904) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V22*/ meltfptr[21] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V21*/ meltfptr[20] = /*_.IFELSE___V22*/ meltfptr[21];;
      /*epilog */

      MELT_LOCATION ("warmelt-normatch.melt:2904:/ clear");
	     /*clear *//*_#IS_LIST__L7*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V22*/ meltfptr[21] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V21*/ meltfptr[20] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-normatch.melt:2905:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#NULL__L8*/ meltfnum[1] =
	(( /*_.LASTFLAG__V19*/ meltfptr[18]) == NULL);;
      MELT_LOCATION ("warmelt-normatch.melt:2905:/ cond");
      /*cond */ if ( /*_#NULL__L8*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_#OR___L9*/ meltfnum[8] = /*_#NULL__L8*/ meltfnum[1];;
	}
      else
	{
	  MELT_LOCATION ("warmelt-normatch.melt:2905:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {

     /*_#I__L10*/ meltfnum[9] =
	      (( /*_#LASTRANK__L6*/ meltfnum[0]) > (0));;
	    /*^compute */
	    /*_#OR___L9*/ meltfnum[8] = /*_#I__L10*/ meltfnum[9];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-normatch.melt:2905:/ clear");
	       /*clear *//*_#I__L10*/ meltfnum[9] = 0;
	  }
	  ;
	}
      ;
      /*^cond */
      /*cond */ if ( /*_#OR___L9*/ meltfnum[8])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V24*/ meltfptr[23] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-normatch.melt:2905:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check lastrank"),
				  ("warmelt-normatch.melt")
				  ? ("warmelt-normatch.melt") : __FILE__,
				  (2905) ? (2905) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V24*/ meltfptr[23] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V23*/ meltfptr[21] = /*_.IFELSE___V24*/ meltfptr[23];;
      /*epilog */

      MELT_LOCATION ("warmelt-normatch.melt:2905:/ clear");
	     /*clear *//*_#NULL__L8*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_#OR___L9*/ meltfnum[8] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V24*/ meltfptr[23] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V23*/ meltfptr[21] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-normatch.melt:2906:/ quasiblock");


    MELT_LOCATION ("warmelt-normatch.melt:2907:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#I__L11*/ meltfnum[9] =
      (( /*_#LASTRANK__L6*/ meltfnum[0]) + (1));;
    /*^compute */
 /*_.MAKE_INTEGERBOX__V26*/ meltfptr[25] =
      (meltgc_new_int
       ((meltobject_ptr_t)
	(( /*!DISCR_CONSTANT_INTEGER */ meltfrout->tabval[4])),
	( /*_#I__L11*/ meltfnum[9])));;
    MELT_LOCATION ("warmelt-normatch.melt:2907:/ quasiblock");


    /*^rawallocobj */
    /*rawallocobj */
    {
      melt_ptr_t newobj = 0;
      melt_raw_object_create (newobj,
			      (melt_ptr_t) (( /*!CLASS_MATCH_FLAG */
					     meltfrout->tabval[3])), (6),
			      "CLASS_MATCH_FLAG");
  /*_.INST__V28*/ meltfptr[27] =
	newobj;
    };
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @LOCA_LOCATION",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V28*/ meltfptr[27])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V28*/ meltfptr[27]), (1),
			  ( /*_.SLOC__V16*/ meltfptr[15]), "LOCA_LOCATION");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @MFLAG_SPAT",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V28*/ meltfptr[27])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V28*/ meltfptr[27]), (2),
			  ( /*_.SPAT__V2*/ meltfptr[1]), "MFLAG_SPAT");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @MFLAG_RANK",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V28*/ meltfptr[27])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V28*/ meltfptr[27]), (3),
			  ( /*_.MAKE_INTEGERBOX__V26*/ meltfptr[25]),
			  "MFLAG_RANK");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @MFLAG_STRING",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V28*/ meltfptr[27])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V28*/ meltfptr[27]), (4),
			  ( /*_.STR__V4*/ meltfptr[3]), "MFLAG_STRING");
    ;
    /*^touchobj */

    melt_dbgtrace_written_object ( /*_.INST__V28*/ meltfptr[27],
				  "newly made instance");
    ;
    /*_.MFLAG__V27*/ meltfptr[26] = /*_.INST__V28*/ meltfptr[27];;

    {
      MELT_LOCATION ("warmelt-normatch.melt:2915:/ locexp");
      meltgc_append_list ((melt_ptr_t) ( /*_.FLAGLIST__V17*/ meltfptr[16]),
			  (melt_ptr_t) ( /*_.MFLAG__V27*/ meltfptr[26]));
    }
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-normatch.melt:2916:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L12*/ meltfnum[1] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-normatch.melt:2916:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L12*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L13*/ meltfnum[8] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-normatch.melt:2916:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long =
		/*_#THE_MELTCALLCOUNT__L13*/ meltfnum[8];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-normatch.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 2916;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "make_match_flag return mflag";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.MFLAG__V27*/ meltfptr[26];
	      /*_.MELT_DEBUG_FUN__V31*/ meltfptr[30] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V30*/ meltfptr[29] =
	      /*_.MELT_DEBUG_FUN__V31*/ meltfptr[30];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-normatch.melt:2916:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L13*/ meltfnum[8] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V31*/ meltfptr[30] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V30*/ meltfptr[29] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-normatch.melt:2916:/ quasiblock");


      /*_.PROGN___V32*/ meltfptr[30] = /*_.IF___V30*/ meltfptr[29];;
      /*^compute */
      /*_.IFCPP___V29*/ meltfptr[28] = /*_.PROGN___V32*/ meltfptr[30];;
      /*epilog */

      MELT_LOCATION ("warmelt-normatch.melt:2916:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L12*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IF___V30*/ meltfptr[29] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V32*/ meltfptr[30] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V29*/ meltfptr[28] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    /*^compute */
    /*_.LET___V25*/ meltfptr[23] = /*_.MFLAG__V27*/ meltfptr[26];;

    MELT_LOCATION ("warmelt-normatch.melt:2906:/ clear");
	   /*clear *//*_#I__L11*/ meltfnum[9] = 0;
    /*^clear */
	   /*clear *//*_.MAKE_INTEGERBOX__V26*/ meltfptr[25] = 0;
    /*^clear */
	   /*clear *//*_.MFLAG__V27*/ meltfptr[26] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V29*/ meltfptr[28] = 0;
    /*_.LET___V15*/ meltfptr[13] = /*_.LET___V25*/ meltfptr[23];;

    MELT_LOCATION ("warmelt-normatch.melt:2899:/ clear");
	   /*clear *//*_.SLOC__V16*/ meltfptr[15] = 0;
    /*^clear */
	   /*clear *//*_.FLAGLIST__V17*/ meltfptr[16] = 0;
    /*^clear */
	   /*clear *//*_.LIST_LAST__V18*/ meltfptr[17] = 0;
    /*^clear */
	   /*clear *//*_.LASTFLAG__V19*/ meltfptr[18] = 0;
    /*^clear */
	   /*clear *//*_.MFLAG_RANK__V20*/ meltfptr[19] = 0;
    /*^clear */
	   /*clear *//*_#LASTRANK__L6*/ meltfnum[0] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V21*/ meltfptr[20] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V23*/ meltfptr[21] = 0;
    /*^clear */
	   /*clear *//*_.LET___V25*/ meltfptr[23] = 0;
    MELT_LOCATION ("warmelt-normatch.melt:2894:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.LET___V15*/ meltfptr[13];;

    {
      MELT_LOCATION ("warmelt-normatch.melt:2894:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V5*/ meltfptr[4] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V9*/ meltfptr[5] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V11*/ meltfptr[6] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V13*/ meltfptr[11] = 0;
    /*^clear */
	   /*clear *//*_.LET___V15*/ meltfptr[13] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("MAKE_MATCH_FLAG", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_54_warmelt_normatch_MAKE_MATCH_FLAG_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_54_warmelt_normatch_MAKE_MATCH_FLAG */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_55_warmelt_normatch_SCANSUBPAT_ANYRECV (meltclosure_ptr_t meltclosp_,
						 melt_ptr_t meltfirstargp_,
						 const melt_argdescr_cell_t
						 meltxargdescr_[],
						 union meltparam_un *
						 meltxargtab_,
						 const melt_argdescr_cell_t
						 meltxresdescr_[],
						 union meltparam_un *
						 meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_55_warmelt_normatch_SCANSUBPAT_ANYRECV_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_55_warmelt_normatch_SCANSUBPAT_ANYRECV_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 13
    melt_ptr_t mcfr_varptr[13];
#define MELTFRAM_NBVARNUM 2
    long mcfr_varnum[2];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_55_warmelt_normatch_SCANSUBPAT_ANYRECV is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_55_warmelt_normatch_SCANSUBPAT_ANYRECV_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 13; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_55_warmelt_normatch_SCANSUBPAT_ANYRECV nbval 13*/
  meltfram__.mcfr_nbvar = 13 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("SCANSUBPAT_ANYRECV", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-normatch.melt:2939:/ getarg");
 /*_.RECV__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.FUN__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.FUN__V3*/ meltfptr[2])) != NULL);


  /*getarg#2 */
  /*^getarg */
  if (meltxargdescr_[1] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.CTX__V4*/ meltfptr[3] =
    (meltxargtab_[1].meltbp_aptr) ? (*(meltxargtab_[1].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.CTX__V4*/ meltfptr[3])) != NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {

    MELT_LOCATION ("warmelt-normatch.melt:2940:/ quasiblock");


 /*_.DIS__V6*/ meltfptr[5] =
      ((melt_ptr_t)
       (melt_discr ((melt_ptr_t) ( /*_.RECV__V2*/ meltfptr[1]))));;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-normatch.melt:2941:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L1*/ meltfnum[0] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-normatch.melt:2941:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L1*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-normatch.melt:2941:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[7];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-normatch.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 2941;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "scansubpat_anyrecv recv=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.RECV__V2*/ meltfptr[1];
	      /*^apply.arg */
	      argtab[5].meltbp_cstring = " of discrim=";
	      /*^apply.arg */
	      argtab[6].meltbp_aptr =
		(melt_ptr_t *) & /*_.DIS__V6*/ meltfptr[5];
	      /*_.MELT_DEBUG_FUN__V9*/ meltfptr[8] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V8*/ meltfptr[7] = /*_.MELT_DEBUG_FUN__V9*/ meltfptr[8];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-normatch.melt:2941:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V9*/ meltfptr[8] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V8*/ meltfptr[7] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-normatch.melt:2941:/ quasiblock");


      /*_.PROGN___V10*/ meltfptr[8] = /*_.IF___V8*/ meltfptr[7];;
      /*^compute */
      /*_.IFCPP___V7*/ meltfptr[6] = /*_.PROGN___V10*/ meltfptr[8];;
      /*epilog */

      MELT_LOCATION ("warmelt-normatch.melt:2941:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L1*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IF___V8*/ meltfptr[7] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V10*/ meltfptr[8] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V7*/ meltfptr[6] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-normatch.melt:2942:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.DIS__V6*/ meltfptr[5]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 1, "NAMED_NAME");
  /*_.NAMED_NAME__V11*/ meltfptr[7] = slot;
    };
    ;

    {
      /*^locexp */
      error ("MELT ERROR MSG [#%ld]::: %s - %s", melt_dbgcounter,
	     ("unexpected scan_subpatterns for"),
	     melt_string_str ((melt_ptr_t)
			      ( /*_.NAMED_NAME__V11*/ meltfptr[7])));
    }
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-normatch.melt:2943:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
      /*^cond */
      /*cond */ if (( /*nil */ NULL))	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V13*/ meltfptr[12] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-normatch.melt:2943:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("@$@unexpected scansubpat_anyrecv"),
				  ("warmelt-normatch.melt")
				  ? ("warmelt-normatch.melt") : __FILE__,
				  (2943) ? (2943) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V13*/ meltfptr[12] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V12*/ meltfptr[8] = /*_.IFELSE___V13*/ meltfptr[12];;
      /*epilog */

      MELT_LOCATION ("warmelt-normatch.melt:2943:/ clear");
	     /*clear *//*_.IFELSE___V13*/ meltfptr[12] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V12*/ meltfptr[8] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    /*^compute */
    /*_.LET___V5*/ meltfptr[4] = /*_.IFCPP___V12*/ meltfptr[8];;

    MELT_LOCATION ("warmelt-normatch.melt:2940:/ clear");
	   /*clear *//*_.DIS__V6*/ meltfptr[5] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V7*/ meltfptr[6] = 0;
    /*^clear */
	   /*clear *//*_.NAMED_NAME__V11*/ meltfptr[7] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V12*/ meltfptr[8] = 0;
    MELT_LOCATION ("warmelt-normatch.melt:2939:/ quasiblock");


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.LET___V5*/ meltfptr[4];;

    {
      MELT_LOCATION ("warmelt-normatch.melt:2939:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.LET___V5*/ meltfptr[4] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("SCANSUBPAT_ANYRECV", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_55_warmelt_normatch_SCANSUBPAT_ANYRECV_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_55_warmelt_normatch_SCANSUBPAT_ANYRECV */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_56_warmelt_normatch_SCANSTEPDATA_ANYRECV (meltclosure_ptr_t
						   meltclosp_,
						   melt_ptr_t meltfirstargp_,
						   const melt_argdescr_cell_t
						   meltxargdescr_[],
						   union meltparam_un *
						   meltxargtab_,
						   const melt_argdescr_cell_t
						   meltxresdescr_[],
						   union meltparam_un *
						   meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_56_warmelt_normatch_SCANSTEPDATA_ANYRECV_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_56_warmelt_normatch_SCANSTEPDATA_ANYRECV_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 13
    melt_ptr_t mcfr_varptr[13];
#define MELTFRAM_NBVARNUM 2
    long mcfr_varnum[2];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_56_warmelt_normatch_SCANSTEPDATA_ANYRECV is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_56_warmelt_normatch_SCANSTEPDATA_ANYRECV_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 13; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_56_warmelt_normatch_SCANSTEPDATA_ANYRECV nbval 13*/
  meltfram__.mcfr_nbvar = 13 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("SCANSTEPDATA_ANYRECV", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-normatch.melt:2947:/ getarg");
 /*_.RECV__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.FUN__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.FUN__V3*/ meltfptr[2])) != NULL);


  /*getarg#2 */
  /*^getarg */
  if (meltxargdescr_[1] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.CTX__V4*/ meltfptr[3] =
    (meltxargtab_[1].meltbp_aptr) ? (*(meltxargtab_[1].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.CTX__V4*/ meltfptr[3])) != NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {

    MELT_LOCATION ("warmelt-normatch.melt:2948:/ quasiblock");


 /*_.DIS__V6*/ meltfptr[5] =
      ((melt_ptr_t)
       (melt_discr ((melt_ptr_t) ( /*_.RECV__V2*/ meltfptr[1]))));;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-normatch.melt:2949:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L1*/ meltfnum[0] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-normatch.melt:2949:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L1*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-normatch.melt:2949:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[7];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-normatch.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 2949;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "scanstepdata_anyrecv recv=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.RECV__V2*/ meltfptr[1];
	      /*^apply.arg */
	      argtab[5].meltbp_cstring = " of discrim=";
	      /*^apply.arg */
	      argtab[6].meltbp_aptr =
		(melt_ptr_t *) & /*_.DIS__V6*/ meltfptr[5];
	      /*_.MELT_DEBUG_FUN__V9*/ meltfptr[8] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V8*/ meltfptr[7] = /*_.MELT_DEBUG_FUN__V9*/ meltfptr[8];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-normatch.melt:2949:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V9*/ meltfptr[8] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V8*/ meltfptr[7] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-normatch.melt:2949:/ quasiblock");


      /*_.PROGN___V10*/ meltfptr[8] = /*_.IF___V8*/ meltfptr[7];;
      /*^compute */
      /*_.IFCPP___V7*/ meltfptr[6] = /*_.PROGN___V10*/ meltfptr[8];;
      /*epilog */

      MELT_LOCATION ("warmelt-normatch.melt:2949:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L1*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IF___V8*/ meltfptr[7] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V10*/ meltfptr[8] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V7*/ meltfptr[6] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-normatch.melt:2950:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.DIS__V6*/ meltfptr[5]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 1, "NAMED_NAME");
  /*_.NAMED_NAME__V11*/ meltfptr[7] = slot;
    };
    ;

    {
      /*^locexp */
      error ("MELT ERROR MSG [#%ld]::: %s - %s", melt_dbgcounter,
	     ("unexpected scan_step_data for"),
	     melt_string_str ((melt_ptr_t)
			      ( /*_.NAMED_NAME__V11*/ meltfptr[7])));
    }
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-normatch.melt:2951:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
      /*^cond */
      /*cond */ if (( /*nil */ NULL))	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V13*/ meltfptr[12] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-normatch.melt:2951:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("@$@unexpected scanstepdata_anyrecv"),
				  ("warmelt-normatch.melt")
				  ? ("warmelt-normatch.melt") : __FILE__,
				  (2951) ? (2951) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V13*/ meltfptr[12] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V12*/ meltfptr[8] = /*_.IFELSE___V13*/ meltfptr[12];;
      /*epilog */

      MELT_LOCATION ("warmelt-normatch.melt:2951:/ clear");
	     /*clear *//*_.IFELSE___V13*/ meltfptr[12] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V12*/ meltfptr[8] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    /*^compute */
    /*_.LET___V5*/ meltfptr[4] = /*_.IFCPP___V12*/ meltfptr[8];;

    MELT_LOCATION ("warmelt-normatch.melt:2948:/ clear");
	   /*clear *//*_.DIS__V6*/ meltfptr[5] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V7*/ meltfptr[6] = 0;
    /*^clear */
	   /*clear *//*_.NAMED_NAME__V11*/ meltfptr[7] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V12*/ meltfptr[8] = 0;
    MELT_LOCATION ("warmelt-normatch.melt:2947:/ quasiblock");


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.LET___V5*/ meltfptr[4];;

    {
      MELT_LOCATION ("warmelt-normatch.melt:2947:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.LET___V5*/ meltfptr[4] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("SCANSTEPDATA_ANYRECV", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_56_warmelt_normatch_SCANSTEPDATA_ANYRECV_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_56_warmelt_normatch_SCANSTEPDATA_ANYRECV */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_57_warmelt_normatch_SCANSTEPFLAG_ANYRECV (meltclosure_ptr_t
						   meltclosp_,
						   melt_ptr_t meltfirstargp_,
						   const melt_argdescr_cell_t
						   meltxargdescr_[],
						   union meltparam_un *
						   meltxargtab_,
						   const melt_argdescr_cell_t
						   meltxresdescr_[],
						   union meltparam_un *
						   meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_57_warmelt_normatch_SCANSTEPFLAG_ANYRECV_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_57_warmelt_normatch_SCANSTEPFLAG_ANYRECV_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 13
    melt_ptr_t mcfr_varptr[13];
#define MELTFRAM_NBVARNUM 2
    long mcfr_varnum[2];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_57_warmelt_normatch_SCANSTEPFLAG_ANYRECV is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_57_warmelt_normatch_SCANSTEPFLAG_ANYRECV_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 13; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_57_warmelt_normatch_SCANSTEPFLAG_ANYRECV nbval 13*/
  meltfram__.mcfr_nbvar = 13 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("SCANSTEPFLAG_ANYRECV", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-normatch.melt:2955:/ getarg");
 /*_.RECV__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.FUN__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.FUN__V3*/ meltfptr[2])) != NULL);


  /*getarg#2 */
  /*^getarg */
  if (meltxargdescr_[1] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.CTX__V4*/ meltfptr[3] =
    (meltxargtab_[1].meltbp_aptr) ? (*(meltxargtab_[1].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.CTX__V4*/ meltfptr[3])) != NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {

    MELT_LOCATION ("warmelt-normatch.melt:2956:/ quasiblock");


 /*_.DIS__V6*/ meltfptr[5] =
      ((melt_ptr_t)
       (melt_discr ((melt_ptr_t) ( /*_.RECV__V2*/ meltfptr[1]))));;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-normatch.melt:2957:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L1*/ meltfnum[0] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-normatch.melt:2957:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L1*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-normatch.melt:2957:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[7];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-normatch.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 2957;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "scanstepflag_anyrecv recv=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.RECV__V2*/ meltfptr[1];
	      /*^apply.arg */
	      argtab[5].meltbp_cstring = " of discrim=";
	      /*^apply.arg */
	      argtab[6].meltbp_aptr =
		(melt_ptr_t *) & /*_.DIS__V6*/ meltfptr[5];
	      /*_.MELT_DEBUG_FUN__V9*/ meltfptr[8] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V8*/ meltfptr[7] = /*_.MELT_DEBUG_FUN__V9*/ meltfptr[8];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-normatch.melt:2957:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V9*/ meltfptr[8] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V8*/ meltfptr[7] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-normatch.melt:2957:/ quasiblock");


      /*_.PROGN___V10*/ meltfptr[8] = /*_.IF___V8*/ meltfptr[7];;
      /*^compute */
      /*_.IFCPP___V7*/ meltfptr[6] = /*_.PROGN___V10*/ meltfptr[8];;
      /*epilog */

      MELT_LOCATION ("warmelt-normatch.melt:2957:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L1*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IF___V8*/ meltfptr[7] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V10*/ meltfptr[8] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V7*/ meltfptr[6] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-normatch.melt:2958:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.DIS__V6*/ meltfptr[5]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 1, "NAMED_NAME");
  /*_.NAMED_NAME__V11*/ meltfptr[7] = slot;
    };
    ;

    {
      /*^locexp */
      error ("MELT ERROR MSG [#%ld]::: %s - %s", melt_dbgcounter,
	     ("unexpected scan_step_flag for"),
	     melt_string_str ((melt_ptr_t)
			      ( /*_.NAMED_NAME__V11*/ meltfptr[7])));
    }
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-normatch.melt:2959:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
      /*^cond */
      /*cond */ if (( /*nil */ NULL))	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V13*/ meltfptr[12] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-normatch.melt:2959:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("@$@unexpected scanstepflag_anyrecv"),
				  ("warmelt-normatch.melt")
				  ? ("warmelt-normatch.melt") : __FILE__,
				  (2959) ? (2959) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V13*/ meltfptr[12] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V12*/ meltfptr[8] = /*_.IFELSE___V13*/ meltfptr[12];;
      /*epilog */

      MELT_LOCATION ("warmelt-normatch.melt:2959:/ clear");
	     /*clear *//*_.IFELSE___V13*/ meltfptr[12] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V12*/ meltfptr[8] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    /*^compute */
    /*_.LET___V5*/ meltfptr[4] = /*_.IFCPP___V12*/ meltfptr[8];;

    MELT_LOCATION ("warmelt-normatch.melt:2956:/ clear");
	   /*clear *//*_.DIS__V6*/ meltfptr[5] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V7*/ meltfptr[6] = 0;
    /*^clear */
	   /*clear *//*_.NAMED_NAME__V11*/ meltfptr[7] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V12*/ meltfptr[8] = 0;
    MELT_LOCATION ("warmelt-normatch.melt:2955:/ quasiblock");


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.LET___V5*/ meltfptr[4];;

    {
      MELT_LOCATION ("warmelt-normatch.melt:2955:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.LET___V5*/ meltfptr[4] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("SCANSTEPFLAG_ANYRECV", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_57_warmelt_normatch_SCANSTEPFLAG_ANYRECV_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_57_warmelt_normatch_SCANSTEPFLAG_ANYRECV */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_58_warmelt_normatch_SCANSUBPAT_NOOP (meltclosure_ptr_t meltclosp_,
					      melt_ptr_t meltfirstargp_,
					      const melt_argdescr_cell_t
					      meltxargdescr_[],
					      union meltparam_un *
					      meltxargtab_,
					      const melt_argdescr_cell_t
					      meltxresdescr_[],
					      union meltparam_un *
					      meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_58_warmelt_normatch_SCANSUBPAT_NOOP_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_58_warmelt_normatch_SCANSUBPAT_NOOP_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 9
    melt_ptr_t mcfr_varptr[9];
#define MELTFRAM_NBVARNUM 2
    long mcfr_varnum[2];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_58_warmelt_normatch_SCANSUBPAT_NOOP is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_58_warmelt_normatch_SCANSUBPAT_NOOP_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 9; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_58_warmelt_normatch_SCANSUBPAT_NOOP nbval 9*/
  meltfram__.mcfr_nbvar = 9 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("SCANSUBPAT_NOOP", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-normatch.melt:2963:/ getarg");
 /*_.RECV__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.FUN__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.FUN__V3*/ meltfptr[2])) != NULL);


  /*getarg#2 */
  /*^getarg */
  if (meltxargdescr_[1] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.CTX__V4*/ meltfptr[3] =
    (meltxargtab_[1].meltbp_aptr) ? (*(meltxargtab_[1].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.CTX__V4*/ meltfptr[3])) != NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-normatch.melt:2964:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L1*/ meltfnum[0] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-normatch.melt:2964:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L1*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-normatch.melt:2964:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-normatch.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 2964;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "scansubpat_noop recv=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.RECV__V2*/ meltfptr[1];
	      /*_.MELT_DEBUG_FUN__V7*/ meltfptr[6] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V6*/ meltfptr[5] = /*_.MELT_DEBUG_FUN__V7*/ meltfptr[6];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-normatch.melt:2964:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V7*/ meltfptr[6] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V6*/ meltfptr[5] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-normatch.melt:2964:/ quasiblock");


      /*_.PROGN___V8*/ meltfptr[6] = /*_.IF___V6*/ meltfptr[5];;
      /*^compute */
      /*_.IFCPP___V5*/ meltfptr[4] = /*_.PROGN___V8*/ meltfptr[6];;
      /*epilog */

      MELT_LOCATION ("warmelt-normatch.melt:2964:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L1*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IF___V6*/ meltfptr[5] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V8*/ meltfptr[6] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V5*/ meltfptr[4] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-normatch.melt:2963:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V9*/ meltfptr[5] = ( /*nil */ NULL);;
    MELT_LOCATION ("warmelt-normatch.melt:2963:/ quasiblock");


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.RETVAL___V9*/ meltfptr[5];;

    {
      MELT_LOCATION ("warmelt-normatch.melt:2963:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;

    /*^clear */
	   /*clear *//*_.RETVAL___V9*/ meltfptr[5] = 0;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V5*/ meltfptr[4] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("SCANSUBPAT_NOOP", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_58_warmelt_normatch_SCANSUBPAT_NOOP_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_58_warmelt_normatch_SCANSUBPAT_NOOP */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_59_warmelt_normatch_SCANSUBPAT_OR (meltclosure_ptr_t meltclosp_,
					    melt_ptr_t meltfirstargp_,
					    const melt_argdescr_cell_t
					    meltxargdescr_[],
					    union meltparam_un * meltxargtab_,
					    const melt_argdescr_cell_t
					    meltxresdescr_[],
					    union meltparam_un * meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_59_warmelt_normatch_SCANSUBPAT_OR_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_59_warmelt_normatch_SCANSUBPAT_OR_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 11
    melt_ptr_t mcfr_varptr[11];
#define MELTFRAM_NBVARNUM 3
    long mcfr_varnum[3];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_59_warmelt_normatch_SCANSUBPAT_OR is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_59_warmelt_normatch_SCANSUBPAT_OR_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 11; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_59_warmelt_normatch_SCANSUBPAT_OR nbval 11*/
  meltfram__.mcfr_nbvar = 11 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("SCANSUBPAT_OR", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-normatch.melt:2970:/ getarg");
 /*_.RECV__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.FUN__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.FUN__V3*/ meltfptr[2])) != NULL);


  /*getarg#2 */
  /*^getarg */
  if (meltxargdescr_[1] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.CTX__V4*/ meltfptr[3] =
    (meltxargtab_[1].meltbp_aptr) ? (*(meltxargtab_[1].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.CTX__V4*/ meltfptr[3])) != NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-normatch.melt:2971:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L1*/ meltfnum[0] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-normatch.melt:2971:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L1*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-normatch.melt:2971:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-normatch.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 2971;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "scansubpat_or recv=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.RECV__V2*/ meltfptr[1];
	      /*_.MELT_DEBUG_FUN__V7*/ meltfptr[6] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V6*/ meltfptr[5] = /*_.MELT_DEBUG_FUN__V7*/ meltfptr[6];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-normatch.melt:2971:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V7*/ meltfptr[6] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V6*/ meltfptr[5] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-normatch.melt:2971:/ quasiblock");


      /*_.PROGN___V8*/ meltfptr[6] = /*_.IF___V6*/ meltfptr[5];;
      /*^compute */
      /*_.IFCPP___V5*/ meltfptr[4] = /*_.PROGN___V8*/ meltfptr[6];;
      /*epilog */

      MELT_LOCATION ("warmelt-normatch.melt:2971:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L1*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IF___V6*/ meltfptr[5] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V8*/ meltfptr[6] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V5*/ meltfptr[4] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-normatch.melt:2972:/ quasiblock");


    /*^getslot */
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.RECV__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 3, "ORPAT_DISJ");
  /*_.DISJTUP__V9*/ meltfptr[5] = slot;
    };
    ;
    /*citerblock FOREACH_IN_MULTIPLE */
    {
      /* start foreach_in_multiple meltcit1__EACHTUP */
      long meltcit1__EACHTUP_ln =
	melt_multiple_length ((melt_ptr_t) /*_.DISJTUP__V9*/ meltfptr[5]);
      for ( /*_#DIX__L3*/ meltfnum[1] = 0;
	   ( /*_#DIX__L3*/ meltfnum[1] >= 0)
	   && ( /*_#DIX__L3*/ meltfnum[1] < meltcit1__EACHTUP_ln);
	/*_#DIX__L3*/ meltfnum[1]++)
	{
	  /*_.CURDISJ__V10*/ meltfptr[6] =
	    melt_multiple_nth ((melt_ptr_t) ( /*_.DISJTUP__V9*/ meltfptr[5]),
			       /*_#DIX__L3*/ meltfnum[1]);



	  MELT_LOCATION ("warmelt-normatch.melt:2976:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^apply */
	  /*apply */
	  {
	    union meltparam_un argtab[1];
	    memset (&argtab, 0, sizeof (argtab));
	    /*^apply.arg */
	    argtab[0].meltbp_aptr =
	      (melt_ptr_t *) & /*_.CTX__V4*/ meltfptr[3];
	    /*_.FUN__V11*/ meltfptr[10] =
	      melt_apply ((meltclosure_ptr_t) ( /*_.FUN__V3*/ meltfptr[2]),
			  (melt_ptr_t) ( /*_.CURDISJ__V10*/ meltfptr[6]),
			  (MELTBPARSTR_PTR ""), argtab, "",
			  (union meltparam_un *) 0);
	  }
	  ;
	  if ( /*_#DIX__L3*/ meltfnum[1] < 0)
	    break;
	}			/* end  foreach_in_multiple meltcit1__EACHTUP */

      /*citerepilog */

      MELT_LOCATION ("warmelt-normatch.melt:2973:/ clear");
	    /*clear *//*_.CURDISJ__V10*/ meltfptr[6] = 0;
      /*^clear */
	    /*clear *//*_#DIX__L3*/ meltfnum[1] = 0;
      /*^clear */
	    /*clear *//*_.FUN__V11*/ meltfptr[10] = 0;
    }				/*endciterblock FOREACH_IN_MULTIPLE */
    ;

    MELT_LOCATION ("warmelt-normatch.melt:2972:/ clear");
	   /*clear *//*_.DISJTUP__V9*/ meltfptr[5] = 0;
    MELT_LOCATION ("warmelt-normatch.melt:2970:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V5*/ meltfptr[4] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("SCANSUBPAT_OR", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_59_warmelt_normatch_SCANSUBPAT_OR_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*noretval */ NULL);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_59_warmelt_normatch_SCANSUBPAT_OR */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_60_warmelt_normatch_SCANSUBPAT_AND (meltclosure_ptr_t meltclosp_,
					     melt_ptr_t meltfirstargp_,
					     const melt_argdescr_cell_t
					     meltxargdescr_[],
					     union meltparam_un *
					     meltxargtab_,
					     const melt_argdescr_cell_t
					     meltxresdescr_[],
					     union meltparam_un *
					     meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_60_warmelt_normatch_SCANSUBPAT_AND_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_60_warmelt_normatch_SCANSUBPAT_AND_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 15
    melt_ptr_t mcfr_varptr[15];
#define MELTFRAM_NBVARNUM 5
    long mcfr_varnum[5];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_60_warmelt_normatch_SCANSUBPAT_AND is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_60_warmelt_normatch_SCANSUBPAT_AND_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 15; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_60_warmelt_normatch_SCANSUBPAT_AND nbval 15*/
  meltfram__.mcfr_nbvar = 15 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("SCANSUBPAT_AND", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-normatch.melt:2979:/ getarg");
 /*_.RECV__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.FUN__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.FUN__V3*/ meltfptr[2])) != NULL);


  /*getarg#2 */
  /*^getarg */
  if (meltxargdescr_[1] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.CTX__V4*/ meltfptr[3] =
    (meltxargtab_[1].meltbp_aptr) ? (*(meltxargtab_[1].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.CTX__V4*/ meltfptr[3])) != NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-normatch.melt:2980:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L1*/ meltfnum[0] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-normatch.melt:2980:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L1*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-normatch.melt:2980:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-normatch.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 2980;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "scansubpat_and recv=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.RECV__V2*/ meltfptr[1];
	      /*_.MELT_DEBUG_FUN__V7*/ meltfptr[6] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V6*/ meltfptr[5] = /*_.MELT_DEBUG_FUN__V7*/ meltfptr[6];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-normatch.melt:2980:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V7*/ meltfptr[6] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V6*/ meltfptr[5] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-normatch.melt:2980:/ quasiblock");


      /*_.PROGN___V8*/ meltfptr[6] = /*_.IF___V6*/ meltfptr[5];;
      /*^compute */
      /*_.IFCPP___V5*/ meltfptr[4] = /*_.PROGN___V8*/ meltfptr[6];;
      /*epilog */

      MELT_LOCATION ("warmelt-normatch.melt:2980:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L1*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IF___V6*/ meltfptr[5] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V8*/ meltfptr[6] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V5*/ meltfptr[4] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-normatch.melt:2981:/ quasiblock");


    /*^getslot */
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.RECV__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 3, "ANDPAT_CONJ");
  /*_.CONJTUP__V9*/ meltfptr[5] = slot;
    };
    ;
    /*citerblock FOREACH_IN_MULTIPLE */
    {
      /* start foreach_in_multiple meltcit1__EACHTUP */
      long meltcit1__EACHTUP_ln =
	melt_multiple_length ((melt_ptr_t) /*_.CONJTUP__V9*/ meltfptr[5]);
      for ( /*_#CIX__L3*/ meltfnum[1] = 0;
	   ( /*_#CIX__L3*/ meltfnum[1] >= 0)
	   && ( /*_#CIX__L3*/ meltfnum[1] < meltcit1__EACHTUP_ln);
	/*_#CIX__L3*/ meltfnum[1]++)
	{
	  /*_.CURCONJ__V10*/ meltfptr[6] =
	    melt_multiple_nth ((melt_ptr_t) ( /*_.CONJTUP__V9*/ meltfptr[5]),
			       /*_#CIX__L3*/ meltfnum[1]);




#if MELT_HAVE_DEBUG
	  MELT_LOCATION ("warmelt-normatch.melt:2985:/ cppif.then");
	  /*^block */
	  /*anyblock */
	  {


	    {
	      /*^locexp */
	      /*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	      melt_dbgcounter++;
#endif
	      ;
	    }
	    ;
	    /*^checksignal */
	    MELT_CHECK_SIGNAL ();
	    ;
    /*_#MELT_NEED_DBG__L4*/ meltfnum[0] =
	      /*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	      ( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	      0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	      ;;
	    MELT_LOCATION ("warmelt-normatch.melt:2985:/ cond");
	    /*cond */ if ( /*_#MELT_NEED_DBG__L4*/ meltfnum[0])	/*then */
	      {
		/*^cond.then */
		/*^block */
		/*anyblock */
		{

      /*_#THE_MELTCALLCOUNT__L5*/ meltfnum[4] =
#ifdef meltcallcount
		    meltcallcount	/* the_meltcallcount */
#else
		    0L
#endif /* meltcallcount the_meltcallcount */
		    ;;
		  MELT_LOCATION ("warmelt-normatch.melt:2985:/ checksignal");
		  MELT_CHECK_SIGNAL ();
		  ;
		  /*^apply */
		  /*apply */
		  {
		    union meltparam_un argtab[7];
		    memset (&argtab, 0, sizeof (argtab));
		    /*^apply.arg */
		    argtab[0].meltbp_long =
		      /*_#THE_MELTCALLCOUNT__L5*/ meltfnum[4];
		    /*^apply.arg */
		    argtab[1].meltbp_cstring = "warmelt-normatch.melt";
		    /*^apply.arg */
		    argtab[2].meltbp_long = 2985;
		    /*^apply.arg */
		    argtab[3].meltbp_cstring = "scansubpat_and curconj=";
		    /*^apply.arg */
		    argtab[4].meltbp_aptr =
		      (melt_ptr_t *) & /*_.CURCONJ__V10*/ meltfptr[6];
		    /*^apply.arg */
		    argtab[5].meltbp_cstring = " cix=";
		    /*^apply.arg */
		    argtab[6].meltbp_long = /*_#CIX__L3*/ meltfnum[1];
		    /*_.MELT_DEBUG_FUN__V13*/ meltfptr[12] =
		      melt_apply ((meltclosure_ptr_t)
				  (( /*!MELT_DEBUG_FUN */ meltfrout->
				    tabval[0])),
				  (melt_ptr_t) (( /*nil */ NULL)),
				  (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_PTR MELTBPARSTR_CSTRING
				   MELTBPARSTR_LONG ""), argtab, "",
				  (union meltparam_un *) 0);
		  }
		  ;
		  /*_.IF___V12*/ meltfptr[11] =
		    /*_.MELT_DEBUG_FUN__V13*/ meltfptr[12];;
		  /*epilog */

		  MELT_LOCATION ("warmelt-normatch.melt:2985:/ clear");
		/*clear *//*_#THE_MELTCALLCOUNT__L5*/ meltfnum[4] = 0;
		  /*^clear */
		/*clear *//*_.MELT_DEBUG_FUN__V13*/ meltfptr[12] = 0;
		}
		;
	      }
	    else
	      {			/*^cond.else */

     /*_.IF___V12*/ meltfptr[11] = NULL;;
	      }
	    ;
	    MELT_LOCATION ("warmelt-normatch.melt:2985:/ quasiblock");


	    /*_.PROGN___V14*/ meltfptr[12] = /*_.IF___V12*/ meltfptr[11];;
	    /*^compute */
	    /*_.IFCPP___V11*/ meltfptr[10] = /*_.PROGN___V14*/ meltfptr[12];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-normatch.melt:2985:/ clear");
	      /*clear *//*_#MELT_NEED_DBG__L4*/ meltfnum[0] = 0;
	    /*^clear */
	      /*clear *//*_.IF___V12*/ meltfptr[11] = 0;
	    /*^clear */
	      /*clear *//*_.PROGN___V14*/ meltfptr[12] = 0;
	  }

#else /*MELT_HAVE_DEBUG */
	  /*^cppif.else */
	  /*_.IFCPP___V11*/ meltfptr[10] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
	  ;
	  MELT_LOCATION ("warmelt-normatch.melt:2986:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^apply */
	  /*apply */
	  {
	    union meltparam_un argtab[1];
	    memset (&argtab, 0, sizeof (argtab));
	    /*^apply.arg */
	    argtab[0].meltbp_aptr =
	      (melt_ptr_t *) & /*_.CTX__V4*/ meltfptr[3];
	    /*_.FUN__V15*/ meltfptr[11] =
	      melt_apply ((meltclosure_ptr_t) ( /*_.FUN__V3*/ meltfptr[2]),
			  (melt_ptr_t) ( /*_.CURCONJ__V10*/ meltfptr[6]),
			  (MELTBPARSTR_PTR ""), argtab, "",
			  (union meltparam_un *) 0);
	  }
	  ;
	  if ( /*_#CIX__L3*/ meltfnum[1] < 0)
	    break;
	}			/* end  foreach_in_multiple meltcit1__EACHTUP */

      /*citerepilog */

      MELT_LOCATION ("warmelt-normatch.melt:2982:/ clear");
	    /*clear *//*_.CURCONJ__V10*/ meltfptr[6] = 0;
      /*^clear */
	    /*clear *//*_#CIX__L3*/ meltfnum[1] = 0;
      /*^clear */
	    /*clear *//*_.IFCPP___V11*/ meltfptr[10] = 0;
      /*^clear */
	    /*clear *//*_.FUN__V15*/ meltfptr[11] = 0;
    }				/*endciterblock FOREACH_IN_MULTIPLE */
    ;

    MELT_LOCATION ("warmelt-normatch.melt:2981:/ clear");
	   /*clear *//*_.CONJTUP__V9*/ meltfptr[5] = 0;
    MELT_LOCATION ("warmelt-normatch.melt:2979:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V5*/ meltfptr[4] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("SCANSUBPAT_AND", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_60_warmelt_normatch_SCANSUBPAT_AND_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*noretval */ NULL);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_60_warmelt_normatch_SCANSUBPAT_AND */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_61_warmelt_normatch_SCANSUBPAT_CONSTRUCT (meltclosure_ptr_t
						   meltclosp_,
						   melt_ptr_t meltfirstargp_,
						   const melt_argdescr_cell_t
						   meltxargdescr_[],
						   union meltparam_un *
						   meltxargtab_,
						   const melt_argdescr_cell_t
						   meltxresdescr_[],
						   union meltparam_un *
						   meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_61_warmelt_normatch_SCANSUBPAT_CONSTRUCT_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_61_warmelt_normatch_SCANSUBPAT_CONSTRUCT_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 25
    melt_ptr_t mcfr_varptr[25];
#define MELTFRAM_NBVARNUM 10
    long mcfr_varnum[10];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_61_warmelt_normatch_SCANSUBPAT_CONSTRUCT is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_61_warmelt_normatch_SCANSUBPAT_CONSTRUCT_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 25; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_61_warmelt_normatch_SCANSUBPAT_CONSTRUCT nbval 25*/
  meltfram__.mcfr_nbvar = 25 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("SCANSUBPAT_CONSTRUCT", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-normatch.melt:2990:/ getarg");
 /*_.RECV__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.FUN__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.FUN__V3*/ meltfptr[2])) != NULL);


  /*getarg#2 */
  /*^getarg */
  if (meltxargdescr_[1] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.CTX__V4*/ meltfptr[3] =
    (meltxargtab_[1].meltbp_aptr) ? (*(meltxargtab_[1].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.CTX__V4*/ meltfptr[3])) != NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-normatch.melt:2991:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L1*/ meltfnum[0] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-normatch.melt:2991:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L1*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-normatch.melt:2991:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[7];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-normatch.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 2991;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "scansubpat_construct recv=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.RECV__V2*/ meltfptr[1];
	      /*^apply.arg */
	      argtab[5].meltbp_cstring = " fun=";
	      /*^apply.arg */
	      argtab[6].meltbp_aptr =
		(melt_ptr_t *) & /*_.FUN__V3*/ meltfptr[2];
	      /*_.MELT_DEBUG_FUN__V7*/ meltfptr[6] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V6*/ meltfptr[5] = /*_.MELT_DEBUG_FUN__V7*/ meltfptr[6];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-normatch.melt:2991:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V7*/ meltfptr[6] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V6*/ meltfptr[5] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-normatch.melt:2991:/ quasiblock");


      /*_.PROGN___V8*/ meltfptr[6] = /*_.IF___V6*/ meltfptr[5];;
      /*^compute */
      /*_.IFCPP___V5*/ meltfptr[4] = /*_.PROGN___V8*/ meltfptr[6];;
      /*epilog */

      MELT_LOCATION ("warmelt-normatch.melt:2991:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L1*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IF___V6*/ meltfptr[5] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V8*/ meltfptr[6] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V5*/ meltfptr[4] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

    {
      MELT_LOCATION ("warmelt-normatch.melt:2992:/ locexp");

#if MELT_HAVE_DEBUG
      if (melt_need_debug (0))
	melt_dbgshortbacktrace (("scansubpat_construct"), (10));
#endif
      ;
    }
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-normatch.melt:2993:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L3*/ meltfnum[1] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.RECV__V2*/ meltfptr[1]),
			     (melt_ptr_t) (( /*!CLASS_SOURCE_PATTERN_CONSTRUCT */ meltfrout->tabval[1])));;
      MELT_LOCATION ("warmelt-normatch.melt:2993:/ cond");
      /*cond */ if ( /*_#IS_A__L3*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V10*/ meltfptr[6] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-normatch.melt:2993:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check recv"),
				  ("warmelt-normatch.melt")
				  ? ("warmelt-normatch.melt") : __FILE__,
				  (2993) ? (2993) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V10*/ meltfptr[6] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V9*/ meltfptr[5] = /*_.IFELSE___V10*/ meltfptr[6];;
      /*epilog */

      MELT_LOCATION ("warmelt-normatch.melt:2993:/ clear");
	     /*clear *//*_#IS_A__L3*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V10*/ meltfptr[6] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V9*/ meltfptr[5] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-normatch.melt:2994:/ quasiblock");


    /*^cond */
    /*cond */ if (
		   /*ifisa */
		   melt_is_instance_of ((melt_ptr_t)
					( /*_.RECV__V2*/ meltfptr[1]),
					(melt_ptr_t) (( /*!CLASS_SOURCE_PATTERN_CONSTRUCT */ meltfrout->tabval[1])))
      )				/*then */
      {
	/*^cond.then */
	/*^getslot */
	{
	  melt_ptr_t slot = NULL, obj = NULL;
	  obj = (melt_ptr_t) ( /*_.RECV__V2*/ meltfptr[1]) /*=obj*/ ;
	  melt_object_get_field (slot, obj, 3, "CTPAT_SUBPA");
   /*_.SUBPATUP__V11*/ meltfptr[6] = slot;
	};
	;
      }
    else
      {				/*^cond.else */

  /*_.SUBPATUP__V11*/ meltfptr[6] = NULL;;
      }
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-normatch.melt:2995:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L4*/ meltfnum[0] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-normatch.melt:2995:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L4*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L5*/ meltfnum[1] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-normatch.melt:2995:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L5*/ meltfnum[1];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-normatch.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 2995;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "scansubpat_construct subpatup=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.SUBPATUP__V11*/ meltfptr[6];
	      /*_.MELT_DEBUG_FUN__V14*/ meltfptr[13] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V13*/ meltfptr[12] =
	      /*_.MELT_DEBUG_FUN__V14*/ meltfptr[13];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-normatch.melt:2995:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L5*/ meltfnum[1] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V14*/ meltfptr[13] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V13*/ meltfptr[12] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-normatch.melt:2995:/ quasiblock");


      /*_.PROGN___V15*/ meltfptr[13] = /*_.IF___V13*/ meltfptr[12];;
      /*^compute */
      /*_.IFCPP___V12*/ meltfptr[11] = /*_.PROGN___V15*/ meltfptr[13];;
      /*epilog */

      MELT_LOCATION ("warmelt-normatch.melt:2995:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L4*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IF___V13*/ meltfptr[12] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V15*/ meltfptr[13] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V12*/ meltfptr[11] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    /*citerblock FOREACH_IN_MULTIPLE */
    {
      /* start foreach_in_multiple meltcit1__EACHTUP */
      long meltcit1__EACHTUP_ln =
	melt_multiple_length ((melt_ptr_t) /*_.SUBPATUP__V11*/ meltfptr[6]);
      for ( /*_#PIX__L6*/ meltfnum[1] = 0;
	   ( /*_#PIX__L6*/ meltfnum[1] >= 0)
	   && ( /*_#PIX__L6*/ meltfnum[1] < meltcit1__EACHTUP_ln);
	/*_#PIX__L6*/ meltfnum[1]++)
	{
	  /*_.CURPA__V16*/ meltfptr[12] =
	    melt_multiple_nth ((melt_ptr_t)
			       ( /*_.SUBPATUP__V11*/ meltfptr[6]),
			       /*_#PIX__L6*/ meltfnum[1]);




#if MELT_HAVE_DEBUG
	  MELT_LOCATION ("warmelt-normatch.melt:2999:/ cppif.then");
	  /*^block */
	  /*anyblock */
	  {


	    {
	      /*^locexp */
	      /*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	      melt_dbgcounter++;
#endif
	      ;
	    }
	    ;
	    /*^checksignal */
	    MELT_CHECK_SIGNAL ();
	    ;
    /*_#MELT_NEED_DBG__L7*/ meltfnum[0] =
	      /*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	      ( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	      0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	      ;;
	    MELT_LOCATION ("warmelt-normatch.melt:2999:/ cond");
	    /*cond */ if ( /*_#MELT_NEED_DBG__L7*/ meltfnum[0])	/*then */
	      {
		/*^cond.then */
		/*^block */
		/*anyblock */
		{

      /*_#THE_MELTCALLCOUNT__L8*/ meltfnum[7] =
#ifdef meltcallcount
		    meltcallcount	/* the_meltcallcount */
#else
		    0L
#endif /* meltcallcount the_meltcallcount */
		    ;;
		  MELT_LOCATION ("warmelt-normatch.melt:2999:/ checksignal");
		  MELT_CHECK_SIGNAL ();
		  ;
		  /*^apply */
		  /*apply */
		  {
		    union meltparam_un argtab[7];
		    memset (&argtab, 0, sizeof (argtab));
		    /*^apply.arg */
		    argtab[0].meltbp_long =
		      /*_#THE_MELTCALLCOUNT__L8*/ meltfnum[7];
		    /*^apply.arg */
		    argtab[1].meltbp_cstring = "warmelt-normatch.melt";
		    /*^apply.arg */
		    argtab[2].meltbp_long = 2999;
		    /*^apply.arg */
		    argtab[3].meltbp_cstring =
		      "scansubpat_construct before curpa=";
		    /*^apply.arg */
		    argtab[4].meltbp_aptr =
		      (melt_ptr_t *) & /*_.CURPA__V16*/ meltfptr[12];
		    /*^apply.arg */
		    argtab[5].meltbp_cstring = " pix=";
		    /*^apply.arg */
		    argtab[6].meltbp_long = /*_#PIX__L6*/ meltfnum[1];
		    /*_.MELT_DEBUG_FUN__V19*/ meltfptr[18] =
		      melt_apply ((meltclosure_ptr_t)
				  (( /*!MELT_DEBUG_FUN */ meltfrout->
				    tabval[0])),
				  (melt_ptr_t) (( /*nil */ NULL)),
				  (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_PTR MELTBPARSTR_CSTRING
				   MELTBPARSTR_LONG ""), argtab, "",
				  (union meltparam_un *) 0);
		  }
		  ;
		  /*_.IF___V18*/ meltfptr[17] =
		    /*_.MELT_DEBUG_FUN__V19*/ meltfptr[18];;
		  /*epilog */

		  MELT_LOCATION ("warmelt-normatch.melt:2999:/ clear");
		/*clear *//*_#THE_MELTCALLCOUNT__L8*/ meltfnum[7] = 0;
		  /*^clear */
		/*clear *//*_.MELT_DEBUG_FUN__V19*/ meltfptr[18] = 0;
		}
		;
	      }
	    else
	      {			/*^cond.else */

     /*_.IF___V18*/ meltfptr[17] = NULL;;
	      }
	    ;
	    MELT_LOCATION ("warmelt-normatch.melt:2999:/ quasiblock");


	    /*_.PROGN___V20*/ meltfptr[18] = /*_.IF___V18*/ meltfptr[17];;
	    /*^compute */
	    /*_.IFCPP___V17*/ meltfptr[13] = /*_.PROGN___V20*/ meltfptr[18];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-normatch.melt:2999:/ clear");
	      /*clear *//*_#MELT_NEED_DBG__L7*/ meltfnum[0] = 0;
	    /*^clear */
	      /*clear *//*_.IF___V18*/ meltfptr[17] = 0;
	    /*^clear */
	      /*clear *//*_.PROGN___V20*/ meltfptr[18] = 0;
	  }

#else /*MELT_HAVE_DEBUG */
	  /*^cppif.else */
	  /*_.IFCPP___V17*/ meltfptr[13] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
	  ;
	  MELT_LOCATION ("warmelt-normatch.melt:3000:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^apply */
	  /*apply */
	  {
	    union meltparam_un argtab[1];
	    memset (&argtab, 0, sizeof (argtab));
	    /*^apply.arg */
	    argtab[0].meltbp_aptr =
	      (melt_ptr_t *) & /*_.CTX__V4*/ meltfptr[3];
	    /*_.FUN__V21*/ meltfptr[17] =
	      melt_apply ((meltclosure_ptr_t) ( /*_.FUN__V3*/ meltfptr[2]),
			  (melt_ptr_t) ( /*_.CURPA__V16*/ meltfptr[12]),
			  (MELTBPARSTR_PTR ""), argtab, "",
			  (union meltparam_un *) 0);
	  }
	  ;

#if MELT_HAVE_DEBUG
	  MELT_LOCATION ("warmelt-normatch.melt:3001:/ cppif.then");
	  /*^block */
	  /*anyblock */
	  {


	    {
	      /*^locexp */
	      /*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	      melt_dbgcounter++;
#endif
	      ;
	    }
	    ;
	    /*^checksignal */
	    MELT_CHECK_SIGNAL ();
	    ;
    /*_#MELT_NEED_DBG__L9*/ meltfnum[7] =
	      /*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	      ( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	      0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	      ;;
	    MELT_LOCATION ("warmelt-normatch.melt:3001:/ cond");
	    /*cond */ if ( /*_#MELT_NEED_DBG__L9*/ meltfnum[7])	/*then */
	      {
		/*^cond.then */
		/*^block */
		/*anyblock */
		{

      /*_#THE_MELTCALLCOUNT__L10*/ meltfnum[0] =
#ifdef meltcallcount
		    meltcallcount	/* the_meltcallcount */
#else
		    0L
#endif /* meltcallcount the_meltcallcount */
		    ;;
		  MELT_LOCATION ("warmelt-normatch.melt:3001:/ checksignal");
		  MELT_CHECK_SIGNAL ();
		  ;
		  /*^apply */
		  /*apply */
		  {
		    union meltparam_un argtab[7];
		    memset (&argtab, 0, sizeof (argtab));
		    /*^apply.arg */
		    argtab[0].meltbp_long =
		      /*_#THE_MELTCALLCOUNT__L10*/ meltfnum[0];
		    /*^apply.arg */
		    argtab[1].meltbp_cstring = "warmelt-normatch.melt";
		    /*^apply.arg */
		    argtab[2].meltbp_long = 3001;
		    /*^apply.arg */
		    argtab[3].meltbp_cstring =
		      "scansubpat_construct after curpa=";
		    /*^apply.arg */
		    argtab[4].meltbp_aptr =
		      (melt_ptr_t *) & /*_.CURPA__V16*/ meltfptr[12];
		    /*^apply.arg */
		    argtab[5].meltbp_cstring = " pix=";
		    /*^apply.arg */
		    argtab[6].meltbp_long = /*_#PIX__L6*/ meltfnum[1];
		    /*_.MELT_DEBUG_FUN__V24*/ meltfptr[23] =
		      melt_apply ((meltclosure_ptr_t)
				  (( /*!MELT_DEBUG_FUN */ meltfrout->
				    tabval[0])),
				  (melt_ptr_t) (( /*nil */ NULL)),
				  (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_PTR MELTBPARSTR_CSTRING
				   MELTBPARSTR_LONG ""), argtab, "",
				  (union meltparam_un *) 0);
		  }
		  ;
		  /*_.IF___V23*/ meltfptr[22] =
		    /*_.MELT_DEBUG_FUN__V24*/ meltfptr[23];;
		  /*epilog */

		  MELT_LOCATION ("warmelt-normatch.melt:3001:/ clear");
		/*clear *//*_#THE_MELTCALLCOUNT__L10*/ meltfnum[0] = 0;
		  /*^clear */
		/*clear *//*_.MELT_DEBUG_FUN__V24*/ meltfptr[23] = 0;
		}
		;
	      }
	    else
	      {			/*^cond.else */

     /*_.IF___V23*/ meltfptr[22] = NULL;;
	      }
	    ;
	    MELT_LOCATION ("warmelt-normatch.melt:3001:/ quasiblock");


	    /*_.PROGN___V25*/ meltfptr[23] = /*_.IF___V23*/ meltfptr[22];;
	    /*^compute */
	    /*_.IFCPP___V22*/ meltfptr[18] = /*_.PROGN___V25*/ meltfptr[23];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-normatch.melt:3001:/ clear");
	      /*clear *//*_#MELT_NEED_DBG__L9*/ meltfnum[7] = 0;
	    /*^clear */
	      /*clear *//*_.IF___V23*/ meltfptr[22] = 0;
	    /*^clear */
	      /*clear *//*_.PROGN___V25*/ meltfptr[23] = 0;
	  }

#else /*MELT_HAVE_DEBUG */
	  /*^cppif.else */
	  /*_.IFCPP___V22*/ meltfptr[18] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
	  ;
	  if ( /*_#PIX__L6*/ meltfnum[1] < 0)
	    break;
	}			/* end  foreach_in_multiple meltcit1__EACHTUP */

      /*citerepilog */

      MELT_LOCATION ("warmelt-normatch.melt:2996:/ clear");
	    /*clear *//*_.CURPA__V16*/ meltfptr[12] = 0;
      /*^clear */
	    /*clear *//*_#PIX__L6*/ meltfnum[1] = 0;
      /*^clear */
	    /*clear *//*_.IFCPP___V17*/ meltfptr[13] = 0;
      /*^clear */
	    /*clear *//*_.FUN__V21*/ meltfptr[17] = 0;
      /*^clear */
	    /*clear *//*_.IFCPP___V22*/ meltfptr[18] = 0;
    }				/*endciterblock FOREACH_IN_MULTIPLE */
    ;

    MELT_LOCATION ("warmelt-normatch.melt:2994:/ clear");
	   /*clear *//*_.SUBPATUP__V11*/ meltfptr[6] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V12*/ meltfptr[11] = 0;
    MELT_LOCATION ("warmelt-normatch.melt:2990:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V5*/ meltfptr[4] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V9*/ meltfptr[5] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("SCANSUBPAT_CONSTRUCT", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_61_warmelt_normatch_SCANSUBPAT_CONSTRUCT_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*noretval */ NULL);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_61_warmelt_normatch_SCANSUBPAT_CONSTRUCT */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_62_warmelt_normatch_SCANSUBPAT_OBJECT (meltclosure_ptr_t meltclosp_,
						melt_ptr_t meltfirstargp_,
						const melt_argdescr_cell_t
						meltxargdescr_[],
						union meltparam_un *
						meltxargtab_,
						const melt_argdescr_cell_t
						meltxresdescr_[],
						union meltparam_un *
						meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_62_warmelt_normatch_SCANSUBPAT_OBJECT_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_62_warmelt_normatch_SCANSUBPAT_OBJECT_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 13
    melt_ptr_t mcfr_varptr[13];
#define MELTFRAM_NBVARNUM 4
    long mcfr_varnum[4];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_62_warmelt_normatch_SCANSUBPAT_OBJECT is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_62_warmelt_normatch_SCANSUBPAT_OBJECT_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 13; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_62_warmelt_normatch_SCANSUBPAT_OBJECT nbval 13*/
  meltfram__.mcfr_nbvar = 13 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("SCANSUBPAT_OBJECT", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-normatch.melt:3005:/ getarg");
 /*_.RECV__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.FUN__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.FUN__V3*/ meltfptr[2])) != NULL);


  /*getarg#2 */
  /*^getarg */
  if (meltxargdescr_[1] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.CTX__V4*/ meltfptr[3] =
    (meltxargtab_[1].meltbp_aptr) ? (*(meltxargtab_[1].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.CTX__V4*/ meltfptr[3])) != NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-normatch.melt:3006:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L1*/ meltfnum[0] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-normatch.melt:3006:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L1*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-normatch.melt:3006:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-normatch.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 3006;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "scansubpat_object recv=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.RECV__V2*/ meltfptr[1];
	      /*_.MELT_DEBUG_FUN__V7*/ meltfptr[6] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V6*/ meltfptr[5] = /*_.MELT_DEBUG_FUN__V7*/ meltfptr[6];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-normatch.melt:3006:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V7*/ meltfptr[6] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V6*/ meltfptr[5] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-normatch.melt:3006:/ quasiblock");


      /*_.PROGN___V8*/ meltfptr[6] = /*_.IF___V6*/ meltfptr[5];;
      /*^compute */
      /*_.IFCPP___V5*/ meltfptr[4] = /*_.PROGN___V8*/ meltfptr[6];;
      /*epilog */

      MELT_LOCATION ("warmelt-normatch.melt:3006:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L1*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IF___V6*/ meltfptr[5] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V8*/ meltfptr[6] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V5*/ meltfptr[4] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-normatch.melt:3007:/ quasiblock");


    /*^getslot */
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.RECV__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 4, "SPAT_FIELDS");
  /*_.PATFIELTUP__V9*/ meltfptr[5] = slot;
    };
    ;
    /*citerblock FOREACH_IN_MULTIPLE */
    {
      /* start foreach_in_multiple meltcit1__EACHTUP */
      long meltcit1__EACHTUP_ln =
	melt_multiple_length ((melt_ptr_t) /*_.PATFIELTUP__V9*/ meltfptr[5]);
      for ( /*_#FLIX__L3*/ meltfnum[1] = 0;
	   ( /*_#FLIX__L3*/ meltfnum[1] >= 0)
	   && ( /*_#FLIX__L3*/ meltfnum[1] < meltcit1__EACHTUP_ln);
	/*_#FLIX__L3*/ meltfnum[1]++)
	{
	  /*_.CURPATFLD__V10*/ meltfptr[6] =
	    melt_multiple_nth ((melt_ptr_t)
			       ( /*_.PATFIELTUP__V9*/ meltfptr[5]),
			       /*_#FLIX__L3*/ meltfnum[1]);



	  MELT_LOCATION ("warmelt-normatch.melt:3011:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
  /*_#IS_A__L4*/ meltfnum[0] =
	    melt_is_instance_of ((melt_ptr_t)
				 ( /*_.CURPATFLD__V10*/ meltfptr[6]),
				 (melt_ptr_t) (( /*!CLASS_SOURCE_FIELD_PATTERN */ meltfrout->tabval[1])));;
	  MELT_LOCATION ("warmelt-normatch.melt:3011:/ cond");
	  /*cond */ if ( /*_#IS_A__L4*/ meltfnum[0])	/*then */
	    {
	      /*^cond.then */
	      /*^block */
	      /*anyblock */
	      {

		MELT_LOCATION ("warmelt-normatch.melt:3012:/ getslot");
		{
		  melt_ptr_t slot = NULL, obj = NULL;
		  obj =
		    (melt_ptr_t) ( /*_.CURPATFLD__V10*/ meltfptr[6]) /*=obj*/
		    ;
		  melt_object_get_field (slot, obj, 3, "SPAF_PATTERN");
     /*_.SPAF_PATTERN__V12*/ meltfptr[11] = slot;
		};
		;
		/*^checksignal */
		MELT_CHECK_SIGNAL ();
		;
		/*^apply */
		/*apply */
		{
		  union meltparam_un argtab[1];
		  memset (&argtab, 0, sizeof (argtab));
		  /*^apply.arg */
		  argtab[0].meltbp_aptr =
		    (melt_ptr_t *) & /*_.CTX__V4*/ meltfptr[3];
		  /*_.FUN__V13*/ meltfptr[12] =
		    melt_apply ((meltclosure_ptr_t)
				( /*_.FUN__V3*/ meltfptr[2]),
				(melt_ptr_t) ( /*_.SPAF_PATTERN__V12*/
					      meltfptr[11]),
				(MELTBPARSTR_PTR ""), argtab, "",
				(union meltparam_un *) 0);
		}
		;
		/*_.IF___V11*/ meltfptr[10] = /*_.FUN__V13*/ meltfptr[12];;
		/*epilog */

		MELT_LOCATION ("warmelt-normatch.melt:3011:/ clear");
	      /*clear *//*_.SPAF_PATTERN__V12*/ meltfptr[11] = 0;
		/*^clear */
	      /*clear *//*_.FUN__V13*/ meltfptr[12] = 0;
	      }
	      ;
	    }
	  else
	    {			/*^cond.else */

   /*_.IF___V11*/ meltfptr[10] = NULL;;
	    }
	  ;
	  if ( /*_#FLIX__L3*/ meltfnum[1] < 0)
	    break;
	}			/* end  foreach_in_multiple meltcit1__EACHTUP */

      /*citerepilog */

      MELT_LOCATION ("warmelt-normatch.melt:3008:/ clear");
	    /*clear *//*_.CURPATFLD__V10*/ meltfptr[6] = 0;
      /*^clear */
	    /*clear *//*_#FLIX__L3*/ meltfnum[1] = 0;
      /*^clear */
	    /*clear *//*_#IS_A__L4*/ meltfnum[0] = 0;
      /*^clear */
	    /*clear *//*_.IF___V11*/ meltfptr[10] = 0;
    }				/*endciterblock FOREACH_IN_MULTIPLE */
    ;

    MELT_LOCATION ("warmelt-normatch.melt:3007:/ clear");
	   /*clear *//*_.PATFIELTUP__V9*/ meltfptr[5] = 0;
    MELT_LOCATION ("warmelt-normatch.melt:3005:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V5*/ meltfptr[4] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("SCANSUBPAT_OBJECT", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_62_warmelt_normatch_SCANSUBPAT_OBJECT_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*noretval */ NULL);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_62_warmelt_normatch_SCANSUBPAT_OBJECT */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_63_warmelt_normatch_FILL_MATCHCASE (meltclosure_ptr_t meltclosp_,
					     melt_ptr_t meltfirstargp_,
					     const melt_argdescr_cell_t
					     meltxargdescr_[],
					     union meltparam_un *
					     meltxargtab_,
					     const melt_argdescr_cell_t
					     meltxresdescr_[],
					     union meltparam_un *
					     meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_63_warmelt_normatch_FILL_MATCHCASE_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_63_warmelt_normatch_FILL_MATCHCASE_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 31
    melt_ptr_t mcfr_varptr[31];
#define MELTFRAM_NBVARNUM 11
    long mcfr_varnum[11];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_63_warmelt_normatch_FILL_MATCHCASE is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_63_warmelt_normatch_FILL_MATCHCASE_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 31; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_63_warmelt_normatch_FILL_MATCHCASE nbval 31*/
  meltfram__.mcfr_nbvar = 31 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("FILL_MATCHCASE", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-normatch.melt:3016:/ getarg");
 /*_.CURMCASE__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.SLOC__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.SLOC__V3*/ meltfptr[2])) !=
	      NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-normatch.melt:3017:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L1*/ meltfnum[0] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-normatch.melt:3017:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L1*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-normatch.melt:3017:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-normatch.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 3017;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "fill_matchcase curmcase";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.CURMCASE__V2*/ meltfptr[1];
	      /*_.MELT_DEBUG_FUN__V6*/ meltfptr[5] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V5*/ meltfptr[4] = /*_.MELT_DEBUG_FUN__V6*/ meltfptr[5];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-normatch.melt:3017:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V6*/ meltfptr[5] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V5*/ meltfptr[4] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-normatch.melt:3017:/ quasiblock");


      /*_.PROGN___V7*/ meltfptr[5] = /*_.IF___V5*/ meltfptr[4];;
      /*^compute */
      /*_.IFCPP___V4*/ meltfptr[3] = /*_.PROGN___V7*/ meltfptr[5];;
      /*epilog */

      MELT_LOCATION ("warmelt-normatch.melt:3017:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L1*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IF___V5*/ meltfptr[4] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V7*/ meltfptr[5] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V4*/ meltfptr[3] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-normatch.melt:3018:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L3*/ meltfnum[1] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.CURMCASE__V2*/ meltfptr[1]),
			     (melt_ptr_t) (( /*!CLASS_MATCH_CASE */
					    meltfrout->tabval[1])));;
      MELT_LOCATION ("warmelt-normatch.melt:3018:/ cond");
      /*cond */ if ( /*_#IS_A__L3*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V9*/ meltfptr[5] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-normatch.melt:3018:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check curmcase"),
				  ("warmelt-normatch.melt")
				  ? ("warmelt-normatch.melt") : __FILE__,
				  (3018) ? (3018) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V9*/ meltfptr[5] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V8*/ meltfptr[4] = /*_.IFELSE___V9*/ meltfptr[5];;
      /*epilog */

      MELT_LOCATION ("warmelt-normatch.melt:3018:/ clear");
	     /*clear *//*_#IS_A__L3*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V9*/ meltfptr[5] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V8*/ meltfptr[4] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-normatch.melt:3019:/ quasiblock");


 /*_.OURPATVARMAP__V10*/ meltfptr[5] =
      (meltgc_new_mapobjects
       ((meltobject_ptr_t) (( /*!DISCR_MAP_OBJECTS */ meltfrout->tabval[2])),
	(17)));;
    MELT_LOCATION ("warmelt-normatch.melt:3021:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.CURMCASE__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 2, "MCASE_SOURCE");
  /*_.CURSCAS__V11*/ meltfptr[10] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-normatch.melt:3022:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.CURSCAS__V11*/ meltfptr[10]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 2, "SCAM_PATT");
  /*_.CURPAT__V12*/ meltfptr[11] = slot;
    };
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-normatch.melt:3024:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L4*/ meltfnum[0] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-normatch.melt:3024:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L4*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L5*/ meltfnum[1] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-normatch.melt:3024:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L5*/ meltfnum[1];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-normatch.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 3024;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "fill_matchcase ourpatvarmap=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.OURPATVARMAP__V10*/ meltfptr[5];
	      /*_.MELT_DEBUG_FUN__V15*/ meltfptr[14] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V14*/ meltfptr[13] =
	      /*_.MELT_DEBUG_FUN__V15*/ meltfptr[14];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-normatch.melt:3024:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L5*/ meltfnum[1] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V15*/ meltfptr[14] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V14*/ meltfptr[13] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-normatch.melt:3024:/ quasiblock");


      /*_.PROGN___V16*/ meltfptr[14] = /*_.IF___V14*/ meltfptr[13];;
      /*^compute */
      /*_.IFCPP___V13*/ meltfptr[12] = /*_.PROGN___V16*/ meltfptr[14];;
      /*epilog */

      MELT_LOCATION ("warmelt-normatch.melt:3024:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L4*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IF___V14*/ meltfptr[13] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V16*/ meltfptr[14] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V13*/ meltfptr[12] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-normatch.melt:3025:/ blockmultialloc");
    /*multiallocblock */
    {
      struct meltletrec_1_st
      {
	struct MELT_CLOSURE_STRUCT (2) rclo_0__VARPATSCANNER;
	long meltletrec_1_endgap;
      } *meltletrec_1_ptr = 0;
      meltletrec_1_ptr =
	(struct meltletrec_1_st *)
	meltgc_allocate (sizeof (struct meltletrec_1_st), 0);
      /*^blockmultialloc.initfill */
      /*iniclos rclo_0__VARPATSCANNER */
   /*_.VARPATSCANNER__V18*/ meltfptr[14] =
	(melt_ptr_t) & meltletrec_1_ptr->rclo_0__VARPATSCANNER;
      meltletrec_1_ptr->rclo_0__VARPATSCANNER.discr =
	(meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (DISCR_CLOSURE))));
      meltletrec_1_ptr->rclo_0__VARPATSCANNER.nbval = 2;
      meltletrec_1_ptr->rclo_0__VARPATSCANNER.rout =
	(meltroutine_ptr_t) (( /*!konst_7 */ meltfrout->tabval[7]));



      MELT_LOCATION ("warmelt-normatch.melt:3027:/ putclosurout");
      /*putclosurout#1 */
      melt_assertmsg ("putclosrout#1 checkclo",
		      melt_magic_discr ((melt_ptr_t)
					( /*_.VARPATSCANNER__V18*/
					 meltfptr[14])) == MELTOBMAG_CLOSURE);
      melt_assertmsg ("putclosrout#1 checkrout",
		      melt_magic_discr ((melt_ptr_t)
					(( /*!konst_7 */ meltfrout->
					  tabval[7]))) == MELTOBMAG_ROUTINE);
      ((meltclosure_ptr_t) /*_.VARPATSCANNER__V18*/ meltfptr[14])->rout =
	(meltroutine_ptr_t) (( /*!konst_7 */ meltfrout->tabval[7]));
      ;
      /*^putclosedv */
      /*putclosv */
      melt_assertmsg ("putclosv checkclo",
		      melt_magic_discr ((melt_ptr_t)
					( /*_.VARPATSCANNER__V18*/
					 meltfptr[14])) == MELTOBMAG_CLOSURE);
      melt_assertmsg ("putclosv checkoff", 0 >= 0
		      && 0 <
		      melt_closure_size ((melt_ptr_t)
					 ( /*_.VARPATSCANNER__V18*/
					  meltfptr[14])));
      ((meltclosure_ptr_t) /*_.VARPATSCANNER__V18*/ meltfptr[14])->tabval[0] =
	(melt_ptr_t) ( /*_.OURPATVARMAP__V10*/ meltfptr[5]);
      ;
      /*^putclosedv */
      /*putclosv */
      melt_assertmsg ("putclosv checkclo",
		      melt_magic_discr ((melt_ptr_t)
					( /*_.VARPATSCANNER__V18*/
					 meltfptr[14])) == MELTOBMAG_CLOSURE);
      melt_assertmsg ("putclosv checkoff", 1 >= 0
		      && 1 <
		      melt_closure_size ((melt_ptr_t)
					 ( /*_.VARPATSCANNER__V18*/
					  meltfptr[14])));
      ((meltclosure_ptr_t) /*_.VARPATSCANNER__V18*/ meltfptr[14])->tabval[1] =
	(melt_ptr_t) ( /*_.VARPATSCANNER__V18*/ meltfptr[14]);
      ;
      /*^touch */
      meltgc_touch ( /*_.VARPATSCANNER__V18*/ meltfptr[14]);
      ;

#if MELT_HAVE_DEBUG
      MELT_LOCATION ("warmelt-normatch.melt:3055:/ cppif.then");
      /*^block */
      /*anyblock */
      {


	{
	  /*^locexp */
	  /*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	  melt_dbgcounter++;
#endif
	  ;
	}
	;
	/*^checksignal */
	MELT_CHECK_SIGNAL ();
	;
    /*_#MELT_NEED_DBG__L6*/ meltfnum[1] =
	  /*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	  ( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	  0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	  ;;
	MELT_LOCATION ("warmelt-normatch.melt:3055:/ cond");
	/*cond */ if ( /*_#MELT_NEED_DBG__L6*/ meltfnum[1])	/*then */
	  {
	    /*^cond.then */
	    /*^block */
	    /*anyblock */
	    {

      /*_#THE_MELTCALLCOUNT__L7*/ meltfnum[0] =
#ifdef meltcallcount
		meltcallcount	/* the_meltcallcount */
#else
		0L
#endif /* meltcallcount the_meltcallcount */
		;;
	      MELT_LOCATION ("warmelt-normatch.melt:3055:/ checksignal");
	      MELT_CHECK_SIGNAL ();
	      ;
	      /*^apply */
	      /*apply */
	      {
		union meltparam_un argtab[9];
		memset (&argtab, 0, sizeof (argtab));
		/*^apply.arg */
		argtab[0].meltbp_long =
		  /*_#THE_MELTCALLCOUNT__L7*/ meltfnum[0];
		/*^apply.arg */
		argtab[1].meltbp_cstring = "warmelt-normatch.melt";
		/*^apply.arg */
		argtab[2].meltbp_long = 3055;
		/*^apply.arg */
		argtab[3].meltbp_cstring =
		  "fill_matchcase before varpatscanner curpat=";
		/*^apply.arg */
		argtab[4].meltbp_aptr =
		  (melt_ptr_t *) & /*_.CURPAT__V12*/ meltfptr[11];
		/*^apply.arg */
		argtab[5].meltbp_cstring = " ourpatvarmap=";
		/*^apply.arg */
		argtab[6].meltbp_aptr =
		  (melt_ptr_t *) & /*_.OURPATVARMAP__V10*/ meltfptr[5];
		/*^apply.arg */
		argtab[7].meltbp_cstring = "\n varpatscanner=";
		/*^apply.arg */
		argtab[8].meltbp_aptr =
		  (melt_ptr_t *) & /*_.VARPATSCANNER__V18*/ meltfptr[14];
		/*_.MELT_DEBUG_FUN__V21*/ meltfptr[20] =
		  melt_apply ((meltclosure_ptr_t)
			      (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			      (melt_ptr_t) (( /*nil */ NULL)),
			      (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			       MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			       MELTBPARSTR_PTR MELTBPARSTR_CSTRING
			       MELTBPARSTR_PTR MELTBPARSTR_CSTRING
			       MELTBPARSTR_PTR ""), argtab, "",
			      (union meltparam_un *) 0);
	      }
	      ;
	      /*_.IF___V20*/ meltfptr[19] =
		/*_.MELT_DEBUG_FUN__V21*/ meltfptr[20];;
	      /*epilog */

	      MELT_LOCATION ("warmelt-normatch.melt:3055:/ clear");
		/*clear *//*_#THE_MELTCALLCOUNT__L7*/ meltfnum[0] = 0;
	      /*^clear */
		/*clear *//*_.MELT_DEBUG_FUN__V21*/ meltfptr[20] = 0;
	    }
	    ;
	  }
	else
	  {			/*^cond.else */

     /*_.IF___V20*/ meltfptr[19] = NULL;;
	  }
	;
	MELT_LOCATION ("warmelt-normatch.melt:3055:/ quasiblock");


	/*_.PROGN___V22*/ meltfptr[20] = /*_.IF___V20*/ meltfptr[19];;
	/*^compute */
	/*_.IFCPP___V19*/ meltfptr[18] = /*_.PROGN___V22*/ meltfptr[20];;
	/*epilog */

	MELT_LOCATION ("warmelt-normatch.melt:3055:/ clear");
	      /*clear *//*_#MELT_NEED_DBG__L6*/ meltfnum[1] = 0;
	/*^clear */
	      /*clear *//*_.IF___V20*/ meltfptr[19] = 0;
	/*^clear */
	      /*clear *//*_.PROGN___V22*/ meltfptr[20] = 0;
      }

#else /*MELT_HAVE_DEBUG */
      /*^cppif.else */
      /*_.IFCPP___V19*/ meltfptr[18] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
      ;
      MELT_LOCATION ("warmelt-normatch.melt:3056:/ checksignal");
      MELT_CHECK_SIGNAL ();
      ;
      /*^apply */
      /*apply */
      {
	union meltparam_un argtab[1];
	memset (&argtab, 0, sizeof (argtab));
	/*^apply.arg */
	argtab[0].meltbp_aptr =
	  (melt_ptr_t *) & /*_.OURPATVARMAP__V10*/ meltfptr[5];
	/*_.VARPATSCANNER__V23*/ meltfptr[19] =
	  melt_apply ((meltclosure_ptr_t)
		      ( /*_.VARPATSCANNER__V18*/ meltfptr[14]),
		      (melt_ptr_t) ( /*_.CURPAT__V12*/ meltfptr[11]),
		      (MELTBPARSTR_PTR ""), argtab, "",
		      (union meltparam_un *) 0);
      }
      ;

#if MELT_HAVE_DEBUG
      MELT_LOCATION ("warmelt-normatch.melt:3057:/ cppif.then");
      /*^block */
      /*anyblock */
      {


	{
	  /*^locexp */
	  /*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	  melt_dbgcounter++;
#endif
	  ;
	}
	;
	/*^checksignal */
	MELT_CHECK_SIGNAL ();
	;
    /*_#MELT_NEED_DBG__L8*/ meltfnum[0] =
	  /*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	  ( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	  0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	  ;;
	MELT_LOCATION ("warmelt-normatch.melt:3057:/ cond");
	/*cond */ if ( /*_#MELT_NEED_DBG__L8*/ meltfnum[0])	/*then */
	  {
	    /*^cond.then */
	    /*^block */
	    /*anyblock */
	    {

      /*_#THE_MELTCALLCOUNT__L9*/ meltfnum[1] =
#ifdef meltcallcount
		meltcallcount	/* the_meltcallcount */
#else
		0L
#endif /* meltcallcount the_meltcallcount */
		;;
	      MELT_LOCATION ("warmelt-normatch.melt:3057:/ checksignal");
	      MELT_CHECK_SIGNAL ();
	      ;
	      /*^apply */
	      /*apply */
	      {
		union meltparam_un argtab[7];
		memset (&argtab, 0, sizeof (argtab));
		/*^apply.arg */
		argtab[0].meltbp_long =
		  /*_#THE_MELTCALLCOUNT__L9*/ meltfnum[1];
		/*^apply.arg */
		argtab[1].meltbp_cstring = "warmelt-normatch.melt";
		/*^apply.arg */
		argtab[2].meltbp_long = 3057;
		/*^apply.arg */
		argtab[3].meltbp_cstring =
		  "fill_matchcase after varpatscanner  curpat=";
		/*^apply.arg */
		argtab[4].meltbp_aptr =
		  (melt_ptr_t *) & /*_.CURPAT__V12*/ meltfptr[11];
		/*^apply.arg */
		argtab[5].meltbp_cstring = "\n ourpatvarmap=";
		/*^apply.arg */
		argtab[6].meltbp_aptr =
		  (melt_ptr_t *) & /*_.OURPATVARMAP__V10*/ meltfptr[5];
		/*_.MELT_DEBUG_FUN__V26*/ meltfptr[25] =
		  melt_apply ((meltclosure_ptr_t)
			      (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			      (melt_ptr_t) (( /*nil */ NULL)),
			      (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			       MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			       MELTBPARSTR_PTR MELTBPARSTR_CSTRING
			       MELTBPARSTR_PTR ""), argtab, "",
			      (union meltparam_un *) 0);
	      }
	      ;
	      /*_.IF___V25*/ meltfptr[24] =
		/*_.MELT_DEBUG_FUN__V26*/ meltfptr[25];;
	      /*epilog */

	      MELT_LOCATION ("warmelt-normatch.melt:3057:/ clear");
		/*clear *//*_#THE_MELTCALLCOUNT__L9*/ meltfnum[1] = 0;
	      /*^clear */
		/*clear *//*_.MELT_DEBUG_FUN__V26*/ meltfptr[25] = 0;
	    }
	    ;
	  }
	else
	  {			/*^cond.else */

     /*_.IF___V25*/ meltfptr[24] = NULL;;
	  }
	;
	MELT_LOCATION ("warmelt-normatch.melt:3057:/ quasiblock");


	/*_.PROGN___V27*/ meltfptr[25] = /*_.IF___V25*/ meltfptr[24];;
	/*^compute */
	/*_.IFCPP___V24*/ meltfptr[20] = /*_.PROGN___V27*/ meltfptr[25];;
	/*epilog */

	MELT_LOCATION ("warmelt-normatch.melt:3057:/ clear");
	      /*clear *//*_#MELT_NEED_DBG__L8*/ meltfnum[0] = 0;
	/*^clear */
	      /*clear *//*_.IF___V25*/ meltfptr[24] = 0;
	/*^clear */
	      /*clear *//*_.PROGN___V27*/ meltfptr[25] = 0;
      }

#else /*MELT_HAVE_DEBUG */
      /*^cppif.else */
      /*_.IFCPP___V24*/ meltfptr[20] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
      ;
      /*^compute */
      /*_.LETREC___V17*/ meltfptr[13] = /*_.IFCPP___V24*/ meltfptr[20];;
      /*epilog */

      MELT_LOCATION ("warmelt-normatch.melt:3025:/ clear");
	    /*clear *//*_.VARPATSCANNER__V18*/ meltfptr[14] = 0;
      /*^clear */
	    /*clear *//*_.VARPATSCANNER__V18*/ meltfptr[14] = 0;
      /*^clear */
	    /*clear *//*_.IFCPP___V19*/ meltfptr[18] = 0;
      /*^clear */
	    /*clear *//*_.VARPATSCANNER__V23*/ meltfptr[19] = 0;
      /*^clear */
	    /*clear *//*_.IFCPP___V24*/ meltfptr[20] = 0;
    }				/*end multiallocblock */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-normatch.melt:3059:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L10*/ meltfnum[1] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-normatch.melt:3059:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L10*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L11*/ meltfnum[0] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-normatch.melt:3059:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[7];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long =
		/*_#THE_MELTCALLCOUNT__L11*/ meltfnum[0];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-normatch.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 3059;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "fill_matchcase final ourpatvarmap=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.OURPATVARMAP__V10*/ meltfptr[5];
	      /*^apply.arg */
	      argtab[5].meltbp_cstring = " for curmcase=";
	      /*^apply.arg */
	      argtab[6].meltbp_aptr =
		(melt_ptr_t *) & /*_.CURMCASE__V2*/ meltfptr[1];
	      /*_.MELT_DEBUG_FUN__V30*/ meltfptr[14] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V29*/ meltfptr[25] =
	      /*_.MELT_DEBUG_FUN__V30*/ meltfptr[14];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-normatch.melt:3059:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L11*/ meltfnum[0] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V30*/ meltfptr[14] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V29*/ meltfptr[25] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-normatch.melt:3059:/ quasiblock");


      /*_.PROGN___V31*/ meltfptr[18] = /*_.IF___V29*/ meltfptr[25];;
      /*^compute */
      /*_.IFCPP___V28*/ meltfptr[24] = /*_.PROGN___V31*/ meltfptr[18];;
      /*epilog */

      MELT_LOCATION ("warmelt-normatch.melt:3059:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L10*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IF___V29*/ meltfptr[25] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V31*/ meltfptr[18] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V28*/ meltfptr[24] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-normatch.melt:3060:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^cond */
    /*cond */ if (
		   /*ifisa */
		   melt_is_instance_of ((melt_ptr_t)
					( /*_.CURMCASE__V2*/ meltfptr[1]),
					(melt_ptr_t) (( /*!CLASS_MATCH_CASE */
						       meltfrout->tabval[1])))
      )				/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  /*^putslot */
	  /*putslot */
	  melt_assertmsg ("putslot checkobj @MCASE_VARMAP",
			  melt_magic_discr ((melt_ptr_t)
					    ( /*_.CURMCASE__V2*/ meltfptr[1]))
			  == MELTOBMAG_OBJECT);
	  melt_putfield_object (( /*_.CURMCASE__V2*/ meltfptr[1]), (4),
				( /*_.OURPATVARMAP__V10*/ meltfptr[5]),
				"MCASE_VARMAP");
	  ;
	  /*^touch */
	  meltgc_touch ( /*_.CURMCASE__V2*/ meltfptr[1]);
	  ;
	  /*^touchobj */

	  melt_dbgtrace_written_object ( /*_.CURMCASE__V2*/ meltfptr[1],
					"put-fields");
	  ;
	  /*epilog */
	}
	;
      }				/*noelse */
    ;

    MELT_LOCATION ("warmelt-normatch.melt:3019:/ clear");
	   /*clear *//*_.OURPATVARMAP__V10*/ meltfptr[5] = 0;
    /*^clear */
	   /*clear *//*_.CURSCAS__V11*/ meltfptr[10] = 0;
    /*^clear */
	   /*clear *//*_.CURPAT__V12*/ meltfptr[11] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V13*/ meltfptr[12] = 0;
    /*^clear */
	   /*clear *//*_.LETREC___V17*/ meltfptr[13] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V28*/ meltfptr[24] = 0;
    MELT_LOCATION ("warmelt-normatch.melt:3016:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V4*/ meltfptr[3] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V8*/ meltfptr[4] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("FILL_MATCHCASE", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_63_warmelt_normatch_FILL_MATCHCASE_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*noretval */ NULL);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_63_warmelt_normatch_FILL_MATCHCASE */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_64_warmelt_normatch_LAMBDA___19__ (meltclosure_ptr_t meltclosp_,
					    melt_ptr_t meltfirstargp_,
					    const melt_argdescr_cell_t
					    meltxargdescr_[],
					    union meltparam_un * meltxargtab_,
					    const melt_argdescr_cell_t
					    meltxresdescr_[],
					    union meltparam_un * meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_64_warmelt_normatch_LAMBDA___19___melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_64_warmelt_normatch_LAMBDA___19___st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 32
    melt_ptr_t mcfr_varptr[32];
#define MELTFRAM_NBVARNUM 12
    long mcfr_varnum[12];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_64_warmelt_normatch_LAMBDA___19__ is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_64_warmelt_normatch_LAMBDA___19___st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 32; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_64_warmelt_normatch_LAMBDA___19__ nbval 32*/
  meltfram__.mcfr_nbvar = 32 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("LAMBDA_", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-normatch.melt:3027:/ getarg");
 /*_.PAT__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.PATVARMAP__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.PATVARMAP__V3*/ meltfptr[2])) !=
	      NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-normatch.melt:3028:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L1*/ meltfnum[0] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-normatch.melt:3028:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L1*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-normatch.melt:3028:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[7];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-normatch.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 3028;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "fill_matchcase/varpatscanner pat=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.PAT__V2*/ meltfptr[1];
	      /*^apply.arg */
	      argtab[5].meltbp_cstring = " patvarmap=";
	      /*^apply.arg */
	      argtab[6].meltbp_aptr =
		(melt_ptr_t *) & /*_.PATVARMAP__V3*/ meltfptr[2];
	      /*_.MELT_DEBUG_FUN__V6*/ meltfptr[5] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V5*/ meltfptr[4] = /*_.MELT_DEBUG_FUN__V6*/ meltfptr[5];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-normatch.melt:3028:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V6*/ meltfptr[5] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V5*/ meltfptr[4] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-normatch.melt:3028:/ quasiblock");


      /*_.PROGN___V7*/ meltfptr[5] = /*_.IF___V5*/ meltfptr[4];;
      /*^compute */
      /*_.IFCPP___V4*/ meltfptr[3] = /*_.PROGN___V7*/ meltfptr[5];;
      /*epilog */

      MELT_LOCATION ("warmelt-normatch.melt:3028:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L1*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IF___V5*/ meltfptr[4] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V7*/ meltfptr[5] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V4*/ meltfptr[3] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-normatch.melt:3029:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_MAPOBJECT__L3*/ meltfnum[1] =
	/*is_mapobject: */
	(melt_magic_discr ((melt_ptr_t) ( /*_.PATVARMAP__V3*/ meltfptr[2])) ==
	 MELTOBMAG_MAPOBJECTS);;
      MELT_LOCATION ("warmelt-normatch.melt:3029:/ cond");
      /*cond */ if ( /*_#IS_MAPOBJECT__L3*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V9*/ meltfptr[5] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-normatch.melt:3029:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check patvarmap"),
				  ("warmelt-normatch.melt")
				  ? ("warmelt-normatch.melt") : __FILE__,
				  (3029) ? (3029) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V9*/ meltfptr[5] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V8*/ meltfptr[4] = /*_.IFELSE___V9*/ meltfptr[5];;
      /*epilog */

      MELT_LOCATION ("warmelt-normatch.melt:3029:/ clear");
	     /*clear *//*_#IS_MAPOBJECT__L3*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V9*/ meltfptr[5] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V8*/ meltfptr[4] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-normatch.melt:3030:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#__L4*/ meltfnum[0] =
	(( /*_.PATVARMAP__V3*/ meltfptr[2]) ==
	 (( /*~OURPATVARMAP */ meltfclos->tabval[0])));;
      MELT_LOCATION ("warmelt-normatch.melt:3030:/ cond");
      /*cond */ if ( /*_#__L4*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V11*/ meltfptr[10] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-normatch.melt:3030:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("same patvarmap"),
				  ("warmelt-normatch.melt")
				  ? ("warmelt-normatch.melt") : __FILE__,
				  (3030) ? (3030) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V11*/ meltfptr[10] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V10*/ meltfptr[5] = /*_.IFELSE___V11*/ meltfptr[10];;
      /*epilog */

      MELT_LOCATION ("warmelt-normatch.melt:3030:/ clear");
	     /*clear *//*_#__L4*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V11*/ meltfptr[10] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V10*/ meltfptr[5] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

    {
      MELT_LOCATION ("warmelt-normatch.melt:3031:/ locexp");

#if MELT_HAVE_DEBUG
      if (melt_need_debug (0))
	melt_dbgshortbacktrace (("fill_matchcase/varpatscanner"), (10));
#endif
      ;
    }
    ;
    MELT_LOCATION ("warmelt-normatch.melt:3032:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#IS_A__L5*/ meltfnum[1] =
      melt_is_instance_of ((melt_ptr_t) ( /*_.PAT__V2*/ meltfptr[1]),
			   (melt_ptr_t) (( /*!CLASS_SOURCE_PATTERN_VARIABLE */
					  meltfrout->tabval[1])));;
    MELT_LOCATION ("warmelt-normatch.melt:3032:/ cond");
    /*cond */ if ( /*_#IS_A__L5*/ meltfnum[1])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-normatch.melt:3033:/ quasiblock");


	  MELT_LOCATION ("warmelt-normatch.melt:3034:/ getslot");
	  {
	    melt_ptr_t slot = NULL, obj = NULL;
	    obj = (melt_ptr_t) ( /*_.PAT__V2*/ meltfptr[1]) /*=obj*/ ;
	    melt_object_get_field (slot, obj, 3, "SPATVAR_SYMB");
    /*_.PVARSYMB__V13*/ meltfptr[12] = slot;
	  };
	  ;
   /*_.VAROCCLIST__V14*/ meltfptr[13] =
	    /*mapobject_get */
	    melt_get_mapobjects ((meltmapobjects_ptr_t)
				 ( /*_.PATVARMAP__V3*/ meltfptr[2]),
				 (meltobject_ptr_t) ( /*_.PVARSYMB__V13*/
						     meltfptr[12]));;
	  MELT_LOCATION ("warmelt-normatch.melt:3037:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
   /*_#NULL__L6*/ meltfnum[0] =
	    (( /*_.VAROCCLIST__V14*/ meltfptr[13]) == NULL);;
	  MELT_LOCATION ("warmelt-normatch.melt:3037:/ cond");
	  /*cond */ if ( /*_#NULL__L6*/ meltfnum[0])	/*then */
	    {
	      /*^cond.then */
	      /*^block */
	      /*anyblock */
	      {

     /*_.MAKE_LIST__V16*/ meltfptr[15] =
		  (meltgc_new_list
		   ((meltobject_ptr_t)
		    (( /*!DISCR_LIST */ meltfrout->tabval[2]))));;
		MELT_LOCATION ("warmelt-normatch.melt:3039:/ compute");
		/*_.VAROCCLIST__V14*/ meltfptr[13] =
		  /*_.SETQ___V17*/ meltfptr[16] =
		  /*_.MAKE_LIST__V16*/ meltfptr[15];;

		{
		  MELT_LOCATION ("warmelt-normatch.melt:3040:/ locexp");
		  meltgc_put_mapobjects ((meltmapobjects_ptr_t)
					 ( /*_.PATVARMAP__V3*/ meltfptr[2]),
					 (meltobject_ptr_t) ( /*_.PVARSYMB__V13*/ meltfptr[12]),
					 (melt_ptr_t) ( /*_.VAROCCLIST__V14*/
						       meltfptr[13]));
		}
		;

#if MELT_HAVE_DEBUG
		MELT_LOCATION ("warmelt-normatch.melt:3041:/ cppif.then");
		/*^block */
		/*anyblock */
		{


		  {
		    /*^locexp */
		    /*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
		    melt_dbgcounter++;
#endif
		    ;
		  }
		  ;
		  /*^checksignal */
		  MELT_CHECK_SIGNAL ();
		  ;
       /*_#MELT_NEED_DBG__L7*/ meltfnum[6] =
		    /*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
		    ( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
		    0		/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
		    ;;
		  MELT_LOCATION ("warmelt-normatch.melt:3041:/ cond");
		  /*cond */ if ( /*_#MELT_NEED_DBG__L7*/ meltfnum[6])	/*then */
		    {
		      /*^cond.then */
		      /*^block */
		      /*anyblock */
		      {

	 /*_#THE_MELTCALLCOUNT__L8*/ meltfnum[7] =
#ifdef meltcallcount
			  meltcallcount	/* the_meltcallcount */
#else
			  0L
#endif /* meltcallcount the_meltcallcount */
			  ;;
			MELT_LOCATION
			  ("warmelt-normatch.melt:3041:/ checksignal");
			MELT_CHECK_SIGNAL ();
			;
			/*^apply */
			/*apply */
			{
			  union meltparam_un argtab[7];
			  memset (&argtab, 0, sizeof (argtab));
			  /*^apply.arg */
			  argtab[0].meltbp_long =
			    /*_#THE_MELTCALLCOUNT__L8*/ meltfnum[7];
			  /*^apply.arg */
			  argtab[1].meltbp_cstring = "warmelt-normatch.melt";
			  /*^apply.arg */
			  argtab[2].meltbp_long = 3041;
			  /*^apply.arg */
			  argtab[3].meltbp_cstring =
			    "fill_matchcase/varpatscanner updated patvarmap=";
			  /*^apply.arg */
			  argtab[4].meltbp_aptr =
			    (melt_ptr_t *) & /*_.PATVARMAP__V3*/ meltfptr[2];
			  /*^apply.arg */
			  argtab[5].meltbp_cstring = " for pat=";
			  /*^apply.arg */
			  argtab[6].meltbp_aptr =
			    (melt_ptr_t *) & /*_.PAT__V2*/ meltfptr[1];
			  /*_.MELT_DEBUG_FUN__V20*/ meltfptr[19] =
			    melt_apply ((meltclosure_ptr_t)
					(( /*!MELT_DEBUG_FUN */ meltfrout->
					  tabval[0])),
					(melt_ptr_t) (( /*nil */ NULL)),
					(MELTBPARSTR_LONG MELTBPARSTR_CSTRING
					 MELTBPARSTR_LONG MELTBPARSTR_CSTRING
					 MELTBPARSTR_PTR MELTBPARSTR_CSTRING
					 MELTBPARSTR_PTR ""), argtab, "",
					(union meltparam_un *) 0);
			}
			;
			/*_.IF___V19*/ meltfptr[18] =
			  /*_.MELT_DEBUG_FUN__V20*/ meltfptr[19];;
			/*epilog */

			MELT_LOCATION ("warmelt-normatch.melt:3041:/ clear");
		   /*clear *//*_#THE_MELTCALLCOUNT__L8*/ meltfnum[7] = 0;
			/*^clear */
		   /*clear *//*_.MELT_DEBUG_FUN__V20*/ meltfptr[19] = 0;
		      }
		      ;
		    }
		  else
		    {		/*^cond.else */

	/*_.IF___V19*/ meltfptr[18] = NULL;;
		    }
		  ;
		  MELT_LOCATION ("warmelt-normatch.melt:3041:/ quasiblock");


		  /*_.PROGN___V21*/ meltfptr[19] =
		    /*_.IF___V19*/ meltfptr[18];;
		  /*^compute */
		  /*_.IFCPP___V18*/ meltfptr[17] =
		    /*_.PROGN___V21*/ meltfptr[19];;
		  /*epilog */

		  MELT_LOCATION ("warmelt-normatch.melt:3041:/ clear");
		 /*clear *//*_#MELT_NEED_DBG__L7*/ meltfnum[6] = 0;
		  /*^clear */
		 /*clear *//*_.IF___V19*/ meltfptr[18] = 0;
		  /*^clear */
		 /*clear *//*_.PROGN___V21*/ meltfptr[19] = 0;
		}

#else /*MELT_HAVE_DEBUG */
		/*^cppif.else */
		/*_.IFCPP___V18*/ meltfptr[17] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
		;
		MELT_LOCATION ("warmelt-normatch.melt:3038:/ quasiblock");


		/*_.PROGN___V22*/ meltfptr[18] =
		  /*_.IFCPP___V18*/ meltfptr[17];;
		/*^compute */
		/*_.IF___V15*/ meltfptr[14] = /*_.PROGN___V22*/ meltfptr[18];;
		/*epilog */

		MELT_LOCATION ("warmelt-normatch.melt:3037:/ clear");
	       /*clear *//*_.MAKE_LIST__V16*/ meltfptr[15] = 0;
		/*^clear */
	       /*clear *//*_.SETQ___V17*/ meltfptr[16] = 0;
		/*^clear */
	       /*clear *//*_.IFCPP___V18*/ meltfptr[17] = 0;
		/*^clear */
	       /*clear *//*_.PROGN___V22*/ meltfptr[18] = 0;
	      }
	      ;
	    }
	  else
	    {			/*^cond.else */

    /*_.IF___V15*/ meltfptr[14] = NULL;;
	    }
	  ;

	  {
	    MELT_LOCATION ("warmelt-normatch.melt:3043:/ locexp");
	    meltgc_append_list ((melt_ptr_t)
				( /*_.VAROCCLIST__V14*/ meltfptr[13]),
				(melt_ptr_t) ( /*_.PAT__V2*/ meltfptr[1]));
	  }
	  ;
	  /*_.IFELSE___V12*/ meltfptr[10] = /*_.IF___V15*/ meltfptr[14];;

	  MELT_LOCATION ("warmelt-normatch.melt:3033:/ clear");
	     /*clear *//*_.PVARSYMB__V13*/ meltfptr[12] = 0;
	  /*^clear */
	     /*clear *//*_.VAROCCLIST__V14*/ meltfptr[13] = 0;
	  /*^clear */
	     /*clear *//*_#NULL__L6*/ meltfnum[0] = 0;
	  /*^clear */
	     /*clear *//*_.IF___V15*/ meltfptr[14] = 0;
	  /*epilog */
	}
	;
      }
    else
      {
	MELT_LOCATION ("warmelt-normatch.melt:3032:/ cond.else");

	/*^block */
	/*anyblock */
	{


#if MELT_HAVE_DEBUG
	  MELT_LOCATION ("warmelt-normatch.melt:3046:/ cppif.then");
	  /*^block */
	  /*anyblock */
	  {


	    {
	      /*^locexp */
	      /*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	      melt_dbgcounter++;
#endif
	      ;
	    }
	    ;
	    /*^checksignal */
	    MELT_CHECK_SIGNAL ();
	    ;
     /*_#MELT_NEED_DBG__L9*/ meltfnum[7] =
	      /*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	      ( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	      0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	      ;;
	    MELT_LOCATION ("warmelt-normatch.melt:3046:/ cond");
	    /*cond */ if ( /*_#MELT_NEED_DBG__L9*/ meltfnum[7])	/*then */
	      {
		/*^cond.then */
		/*^block */
		/*anyblock */
		{

       /*_#THE_MELTCALLCOUNT__L10*/ meltfnum[6] =
#ifdef meltcallcount
		    meltcallcount	/* the_meltcallcount */
#else
		    0L
#endif /* meltcallcount the_meltcallcount */
		    ;;
		  MELT_LOCATION ("warmelt-normatch.melt:3046:/ checksignal");
		  MELT_CHECK_SIGNAL ();
		  ;
		  /*^apply */
		  /*apply */
		  {
		    union meltparam_un argtab[7];
		    memset (&argtab, 0, sizeof (argtab));
		    /*^apply.arg */
		    argtab[0].meltbp_long =
		      /*_#THE_MELTCALLCOUNT__L10*/ meltfnum[6];
		    /*^apply.arg */
		    argtab[1].meltbp_cstring = "warmelt-normatch.melt";
		    /*^apply.arg */
		    argtab[2].meltbp_long = 3046;
		    /*^apply.arg */
		    argtab[3].meltbp_cstring =
		      "fill_matchcase/varpatscanner before scan_subpatterns pat=";
		    /*^apply.arg */
		    argtab[4].meltbp_aptr =
		      (melt_ptr_t *) & /*_.PAT__V2*/ meltfptr[1];
		    /*^apply.arg */
		    argtab[5].meltbp_cstring = "\n ourpatvarmap=";
		    /*^apply.arg */
		    argtab[6].meltbp_aptr =
		      (melt_ptr_t *) & ( /*~OURPATVARMAP */ meltfclos->
					tabval[0]);
		    /*_.MELT_DEBUG_FUN__V25*/ meltfptr[16] =
		      melt_apply ((meltclosure_ptr_t)
				  (( /*!MELT_DEBUG_FUN */ meltfrout->
				    tabval[0])),
				  (melt_ptr_t) (( /*nil */ NULL)),
				  (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_PTR MELTBPARSTR_CSTRING
				   MELTBPARSTR_PTR ""), argtab, "",
				  (union meltparam_un *) 0);
		  }
		  ;
		  /*_.IF___V24*/ meltfptr[15] =
		    /*_.MELT_DEBUG_FUN__V25*/ meltfptr[16];;
		  /*epilog */

		  MELT_LOCATION ("warmelt-normatch.melt:3046:/ clear");
		 /*clear *//*_#THE_MELTCALLCOUNT__L10*/ meltfnum[6] = 0;
		  /*^clear */
		 /*clear *//*_.MELT_DEBUG_FUN__V25*/ meltfptr[16] = 0;
		}
		;
	      }
	    else
	      {			/*^cond.else */

      /*_.IF___V24*/ meltfptr[15] = NULL;;
	      }
	    ;
	    MELT_LOCATION ("warmelt-normatch.melt:3046:/ quasiblock");


	    /*_.PROGN___V26*/ meltfptr[17] = /*_.IF___V24*/ meltfptr[15];;
	    /*^compute */
	    /*_.IFCPP___V23*/ meltfptr[19] = /*_.PROGN___V26*/ meltfptr[17];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-normatch.melt:3046:/ clear");
	       /*clear *//*_#MELT_NEED_DBG__L9*/ meltfnum[7] = 0;
	    /*^clear */
	       /*clear *//*_.IF___V24*/ meltfptr[15] = 0;
	    /*^clear */
	       /*clear *//*_.PROGN___V26*/ meltfptr[17] = 0;
	  }

#else /*MELT_HAVE_DEBUG */
	  /*^cppif.else */
	  /*_.IFCPP___V23*/ meltfptr[19] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
	  ;
	  MELT_LOCATION ("warmelt-normatch.melt:3048:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^msend */
	  /*msend */
	  {
	    union meltparam_un argtab[2];
	    memset (&argtab, 0, sizeof (argtab));
	    /*^ojbmsend.arg */
	    argtab[0].meltbp_aptr =
	      (melt_ptr_t *) & ( /*~VARPATSCANNER */ meltfclos->tabval[1]);
	    /*^ojbmsend.arg */
	    argtab[1].meltbp_aptr =
	      (melt_ptr_t *) & ( /*~OURPATVARMAP */ meltfclos->tabval[0]);
	    /*_.SCAN_SUBPATTERNS__V27*/ meltfptr[18] =
	      meltgc_send ((melt_ptr_t) ( /*_.PAT__V2*/ meltfptr[1]),
			   (melt_ptr_t) (( /*!SCAN_SUBPATTERNS */ meltfrout->
					  tabval[3])),
			   (MELTBPARSTR_PTR MELTBPARSTR_PTR ""), argtab, "",
			   (union meltparam_un *) 0);
	  }
	  ;

#if MELT_HAVE_DEBUG
	  MELT_LOCATION ("warmelt-normatch.melt:3049:/ cppif.then");
	  /*^block */
	  /*anyblock */
	  {


	    {
	      /*^locexp */
	      /*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	      melt_dbgcounter++;
#endif
	      ;
	    }
	    ;
	    /*^checksignal */
	    MELT_CHECK_SIGNAL ();
	    ;
     /*_#MELT_NEED_DBG__L11*/ meltfnum[0] =
	      /*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	      ( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	      0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	      ;;
	    MELT_LOCATION ("warmelt-normatch.melt:3049:/ cond");
	    /*cond */ if ( /*_#MELT_NEED_DBG__L11*/ meltfnum[0])	/*then */
	      {
		/*^cond.then */
		/*^block */
		/*anyblock */
		{

       /*_#THE_MELTCALLCOUNT__L12*/ meltfnum[6] =
#ifdef meltcallcount
		    meltcallcount	/* the_meltcallcount */
#else
		    0L
#endif /* meltcallcount the_meltcallcount */
		    ;;
		  MELT_LOCATION ("warmelt-normatch.melt:3049:/ checksignal");
		  MELT_CHECK_SIGNAL ();
		  ;
		  /*^apply */
		  /*apply */
		  {
		    union meltparam_un argtab[7];
		    memset (&argtab, 0, sizeof (argtab));
		    /*^apply.arg */
		    argtab[0].meltbp_long =
		      /*_#THE_MELTCALLCOUNT__L12*/ meltfnum[6];
		    /*^apply.arg */
		    argtab[1].meltbp_cstring = "warmelt-normatch.melt";
		    /*^apply.arg */
		    argtab[2].meltbp_long = 3049;
		    /*^apply.arg */
		    argtab[3].meltbp_cstring =
		      "fill_matchcase/varpatscanner after scan_subpatterns pat=";
		    /*^apply.arg */
		    argtab[4].meltbp_aptr =
		      (melt_ptr_t *) & /*_.PAT__V2*/ meltfptr[1];
		    /*^apply.arg */
		    argtab[5].meltbp_cstring = "\n ourpatvarmap=";
		    /*^apply.arg */
		    argtab[6].meltbp_aptr =
		      (melt_ptr_t *) & ( /*~OURPATVARMAP */ meltfclos->
					tabval[0]);
		    /*_.MELT_DEBUG_FUN__V30*/ meltfptr[14] =
		      melt_apply ((meltclosure_ptr_t)
				  (( /*!MELT_DEBUG_FUN */ meltfrout->
				    tabval[0])),
				  (melt_ptr_t) (( /*nil */ NULL)),
				  (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_PTR MELTBPARSTR_CSTRING
				   MELTBPARSTR_PTR ""), argtab, "",
				  (union meltparam_un *) 0);
		  }
		  ;
		  /*_.IF___V29*/ meltfptr[13] =
		    /*_.MELT_DEBUG_FUN__V30*/ meltfptr[14];;
		  /*epilog */

		  MELT_LOCATION ("warmelt-normatch.melt:3049:/ clear");
		 /*clear *//*_#THE_MELTCALLCOUNT__L12*/ meltfnum[6] = 0;
		  /*^clear */
		 /*clear *//*_.MELT_DEBUG_FUN__V30*/ meltfptr[14] = 0;
		}
		;
	      }
	    else
	      {			/*^cond.else */

      /*_.IF___V29*/ meltfptr[13] = NULL;;
	      }
	    ;
	    MELT_LOCATION ("warmelt-normatch.melt:3049:/ quasiblock");


	    /*_.PROGN___V31*/ meltfptr[16] = /*_.IF___V29*/ meltfptr[13];;
	    /*^compute */
	    /*_.IFCPP___V28*/ meltfptr[12] = /*_.PROGN___V31*/ meltfptr[16];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-normatch.melt:3049:/ clear");
	       /*clear *//*_#MELT_NEED_DBG__L11*/ meltfnum[0] = 0;
	    /*^clear */
	       /*clear *//*_.IF___V29*/ meltfptr[13] = 0;
	    /*^clear */
	       /*clear *//*_.PROGN___V31*/ meltfptr[16] = 0;
	  }

#else /*MELT_HAVE_DEBUG */
	  /*^cppif.else */
	  /*_.IFCPP___V28*/ meltfptr[12] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
	  ;
	  MELT_LOCATION ("warmelt-normatch.melt:3045:/ quasiblock");


	  /*_.PROGN___V32*/ meltfptr[15] = /*_.IFCPP___V28*/ meltfptr[12];;
	  /*^compute */
	  /*_.IFELSE___V12*/ meltfptr[10] = /*_.PROGN___V32*/ meltfptr[15];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-normatch.melt:3032:/ clear");
	     /*clear *//*_.IFCPP___V23*/ meltfptr[19] = 0;
	  /*^clear */
	     /*clear *//*_.SCAN_SUBPATTERNS__V27*/ meltfptr[18] = 0;
	  /*^clear */
	     /*clear *//*_.IFCPP___V28*/ meltfptr[12] = 0;
	  /*^clear */
	     /*clear *//*_.PROGN___V32*/ meltfptr[15] = 0;
	}
	;
      }
    ;
    MELT_LOCATION ("warmelt-normatch.melt:3027:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.IFELSE___V12*/ meltfptr[10];;

    {
      MELT_LOCATION ("warmelt-normatch.melt:3027:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V4*/ meltfptr[3] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V8*/ meltfptr[4] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V10*/ meltfptr[5] = 0;
    /*^clear */
	   /*clear *//*_#IS_A__L5*/ meltfnum[1] = 0;
    /*^clear */
	   /*clear *//*_.IFELSE___V12*/ meltfptr[10] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("LAMBDA_", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_64_warmelt_normatch_LAMBDA___19___melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_64_warmelt_normatch_LAMBDA___19__ */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_65_warmelt_normatch_PUTTHEN_MATCHANY (meltclosure_ptr_t meltclosp_,
					       melt_ptr_t meltfirstargp_,
					       const melt_argdescr_cell_t
					       meltxargdescr_[],
					       union meltparam_un *
					       meltxargtab_,
					       const melt_argdescr_cell_t
					       meltxresdescr_[],
					       union meltparam_un *
					       meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_65_warmelt_normatch_PUTTHEN_MATCHANY_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_65_warmelt_normatch_PUTTHEN_MATCHANY_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 12
    melt_ptr_t mcfr_varptr[12];
#define MELTFRAM_NBVARNUM 2
    long mcfr_varnum[2];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_65_warmelt_normatch_PUTTHEN_MATCHANY is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_65_warmelt_normatch_PUTTHEN_MATCHANY_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 12; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_65_warmelt_normatch_PUTTHEN_MATCHANY nbval 12*/
  meltfram__.mcfr_nbvar = 12 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("PUTTHEN_MATCHANY", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-normatch.melt:3071:/ getarg");
 /*_.RECV__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.THENSTEP__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.THENSTEP__V3*/ meltfptr[2])) !=
	      NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {

    MELT_LOCATION ("warmelt-normatch.melt:3072:/ quasiblock");


 /*_.DIS__V5*/ meltfptr[4] =
      ((melt_ptr_t)
       (melt_discr ((melt_ptr_t) ( /*_.RECV__V2*/ meltfptr[1]))));;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-normatch.melt:3074:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L1*/ meltfnum[0] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-normatch.melt:3074:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L1*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-normatch.melt:3074:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[9];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-normatch.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 3074;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "putelse_mathany recv=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.RECV__V2*/ meltfptr[1];
	      /*^apply.arg */
	      argtab[5].meltbp_cstring = "thenstep=";
	      /*^apply.arg */
	      argtab[6].meltbp_aptr =
		(melt_ptr_t *) & /*_.THENSTEP__V3*/ meltfptr[2];
	      /*^apply.arg */
	      argtab[7].meltbp_cstring = " for discrim=";
	      /*^apply.arg */
	      argtab[8].meltbp_aptr =
		(melt_ptr_t *) & /*_.DIS__V5*/ meltfptr[4];
	      /*_.MELT_DEBUG_FUN__V8*/ meltfptr[7] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V7*/ meltfptr[6] = /*_.MELT_DEBUG_FUN__V8*/ meltfptr[7];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-normatch.melt:3074:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V8*/ meltfptr[7] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V7*/ meltfptr[6] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-normatch.melt:3074:/ quasiblock");


      /*_.PROGN___V9*/ meltfptr[7] = /*_.IF___V7*/ meltfptr[6];;
      /*^compute */
      /*_.IFCPP___V6*/ meltfptr[5] = /*_.PROGN___V9*/ meltfptr[7];;
      /*epilog */

      MELT_LOCATION ("warmelt-normatch.melt:3074:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L1*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IF___V7*/ meltfptr[6] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V9*/ meltfptr[7] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V6*/ meltfptr[5] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-normatch.melt:3075:/ cond");
    /*cond */ if (
		   /*ifisa */
		   melt_is_instance_of ((melt_ptr_t)
					( /*_.DIS__V5*/ meltfptr[4]),
					(melt_ptr_t) (( /*!CLASS_NAMED */
						       meltfrout->tabval[1])))
      )				/*then */
      {
	/*^cond.then */
	/*^getslot */
	{
	  melt_ptr_t slot = NULL, obj = NULL;
	  obj = (melt_ptr_t) ( /*_.DIS__V5*/ meltfptr[4]) /*=obj*/ ;
	  melt_object_get_field (slot, obj, 1, "NAMED_NAME");
   /*_.NAMED_NAME__V10*/ meltfptr[6] = slot;
	};
	;
      }
    else
      {				/*^cond.else */

  /*_.NAMED_NAME__V10*/ meltfptr[6] = NULL;;
      }
    ;

    {
      MELT_LOCATION ("warmelt-normatch.melt:3075:/ locexp");
      error ("MELT ERROR MSG [#%ld]::: %s - %s", melt_dbgcounter,
	     ("unexpected PUT_THEN_MATCH for "),
	     melt_string_str ((melt_ptr_t)
			      ( /*_.NAMED_NAME__V10*/ meltfptr[6])));
    }
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-normatch.melt:3076:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
      /*^cond */
      /*cond */ if (( /*nil */ NULL))	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V12*/ meltfptr[11] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-normatch.melt:3076:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("@$@unexpected PUT_THEN_MATCH [putthen_matchany]"), ("warmelt-normatch.melt") ? ("warmelt-normatch.melt") : __FILE__, (3076) ? (3076) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V12*/ meltfptr[11] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V11*/ meltfptr[7] = /*_.IFELSE___V12*/ meltfptr[11];;
      /*epilog */

      MELT_LOCATION ("warmelt-normatch.melt:3076:/ clear");
	     /*clear *//*_.IFELSE___V12*/ meltfptr[11] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V11*/ meltfptr[7] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    /*^compute */
    /*_.LET___V4*/ meltfptr[3] = /*_.IFCPP___V11*/ meltfptr[7];;

    MELT_LOCATION ("warmelt-normatch.melt:3072:/ clear");
	   /*clear *//*_.DIS__V5*/ meltfptr[4] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V6*/ meltfptr[5] = 0;
    /*^clear */
	   /*clear *//*_.NAMED_NAME__V10*/ meltfptr[6] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V11*/ meltfptr[7] = 0;
    MELT_LOCATION ("warmelt-normatch.melt:3071:/ quasiblock");


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.LET___V4*/ meltfptr[3];;

    {
      MELT_LOCATION ("warmelt-normatch.melt:3071:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.LET___V4*/ meltfptr[3] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("PUTTHEN_MATCHANY", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_65_warmelt_normatch_PUTTHEN_MATCHANY_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_65_warmelt_normatch_PUTTHEN_MATCHANY */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_66_warmelt_normatch_START_STEP (meltclosure_ptr_t meltclosp_,
					 melt_ptr_t meltfirstargp_,
					 const melt_argdescr_cell_t
					 meltxargdescr_[],
					 union meltparam_un * meltxargtab_,
					 const melt_argdescr_cell_t
					 meltxresdescr_[],
					 union meltparam_un * meltxrestab_)
{
  long current_blocklevel_signals_meltrout_66_warmelt_normatch_START_STEP_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_66_warmelt_normatch_START_STEP_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 26
    melt_ptr_t mcfr_varptr[26];
#define MELTFRAM_NBVARNUM 10
    long mcfr_varnum[10];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_66_warmelt_normatch_START_STEP is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_66_warmelt_normatch_START_STEP_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 26; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_66_warmelt_normatch_START_STEP nbval 26*/
  meltfram__.mcfr_nbvar = 26 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("START_STEP", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-normatch.melt:3080:/ getarg");
 /*_.STEP__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {

    MELT_LOCATION ("warmelt-normatch.melt:3082:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#NULL__L1*/ meltfnum[0] =
      (( /*_.STEP__V2*/ meltfptr[1]) == NULL);;
    MELT_LOCATION ("warmelt-normatch.melt:3082:/ cond");
    /*cond */ if ( /*_#NULL__L1*/ meltfnum[0])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-normatch.melt:3083:/ quasiblock");


   /*_.RETVAL___V1*/ meltfptr[0] = NULL;;

	  {
	    MELT_LOCATION ("warmelt-normatch.melt:3083:/ locexp");
	    /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
	    if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	      melt_warn_for_no_expected_secondary_results ();
	    /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
	    ;
	  }
	  ;
	  /*^finalreturn */
	  ;
	  /*finalret */ goto labend_rout;
	  /*_.IFELSE___V3*/ meltfptr[2] = /*_.RETURN___V4*/ meltfptr[3];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-normatch.melt:3082:/ clear");
	     /*clear *//*_.RETURN___V4*/ meltfptr[3] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-normatch.melt:3085:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
   /*_#IS_A__L2*/ meltfnum[1] =
	    melt_is_instance_of ((melt_ptr_t) ( /*_.STEP__V2*/ meltfptr[1]),
				 (melt_ptr_t) (( /*!CLASS_MATCH_STEP_TEST_GROUP */ meltfrout->tabval[0])));;
	  MELT_LOCATION ("warmelt-normatch.melt:3085:/ cond");
	  /*cond */ if ( /*_#IS_A__L2*/ meltfnum[1])	/*then */
	    {
	      /*^cond.then */
	      /*^block */
	      /*anyblock */
	      {

		MELT_LOCATION ("warmelt-normatch.melt:3086:/ quasiblock");


		/*^getslot */
		{
		  melt_ptr_t slot = NULL, obj = NULL;
		  obj = (melt_ptr_t) ( /*_.STEP__V2*/ meltfptr[1]) /*=obj*/ ;
		  melt_object_get_field (slot, obj, 6, "MSTGROUP_START");
      /*_.STASTEP__V7*/ meltfptr[6] = slot;
		};
		;

#if MELT_HAVE_DEBUG
		MELT_LOCATION ("warmelt-normatch.melt:3088:/ cppif.then");
		/*^block */
		/*anyblock */
		{


		  {
		    /*^locexp */
		    /*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
		    melt_dbgcounter++;
#endif
		    ;
		  }
		  ;
		  /*^checksignal */
		  MELT_CHECK_SIGNAL ();
		  ;
       /*_#MELT_NEED_DBG__L3*/ meltfnum[2] =
		    /*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
		    ( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
		    0		/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
		    ;;
		  MELT_LOCATION ("warmelt-normatch.melt:3088:/ cond");
		  /*cond */ if ( /*_#MELT_NEED_DBG__L3*/ meltfnum[2])	/*then */
		    {
		      /*^cond.then */
		      /*^block */
		      /*anyblock */
		      {

	 /*_#THE_MELTCALLCOUNT__L4*/ meltfnum[3] =
#ifdef meltcallcount
			  meltcallcount	/* the_meltcallcount */
#else
			  0L
#endif /* meltcallcount the_meltcallcount */
			  ;;
			MELT_LOCATION
			  ("warmelt-normatch.melt:3088:/ checksignal");
			MELT_CHECK_SIGNAL ();
			;
			/*^apply */
			/*apply */
			{
			  union meltparam_un argtab[5];
			  memset (&argtab, 0, sizeof (argtab));
			  /*^apply.arg */
			  argtab[0].meltbp_long =
			    /*_#THE_MELTCALLCOUNT__L4*/ meltfnum[3];
			  /*^apply.arg */
			  argtab[1].meltbp_cstring = "warmelt-normatch.melt";
			  /*^apply.arg */
			  argtab[2].meltbp_long = 3088;
			  /*^apply.arg */
			  argtab[3].meltbp_cstring = "start_step group step";
			  /*^apply.arg */
			  argtab[4].meltbp_aptr =
			    (melt_ptr_t *) & /*_.STEP__V2*/ meltfptr[1];
			  /*_.MELT_DEBUG_FUN__V10*/ meltfptr[9] =
			    melt_apply ((meltclosure_ptr_t)
					(( /*!MELT_DEBUG_FUN */ meltfrout->
					  tabval[1])),
					(melt_ptr_t) (( /*nil */ NULL)),
					(MELTBPARSTR_LONG MELTBPARSTR_CSTRING
					 MELTBPARSTR_LONG MELTBPARSTR_CSTRING
					 MELTBPARSTR_PTR ""), argtab, "",
					(union meltparam_un *) 0);
			}
			;
			/*_.IF___V9*/ meltfptr[8] =
			  /*_.MELT_DEBUG_FUN__V10*/ meltfptr[9];;
			/*epilog */

			MELT_LOCATION ("warmelt-normatch.melt:3088:/ clear");
		   /*clear *//*_#THE_MELTCALLCOUNT__L4*/ meltfnum[3] = 0;
			/*^clear */
		   /*clear *//*_.MELT_DEBUG_FUN__V10*/ meltfptr[9] = 0;
		      }
		      ;
		    }
		  else
		    {		/*^cond.else */

	/*_.IF___V9*/ meltfptr[8] = NULL;;
		    }
		  ;
		  MELT_LOCATION ("warmelt-normatch.melt:3088:/ quasiblock");


		  /*_.PROGN___V11*/ meltfptr[9] = /*_.IF___V9*/ meltfptr[8];;
		  /*^compute */
		  /*_.IFCPP___V8*/ meltfptr[7] =
		    /*_.PROGN___V11*/ meltfptr[9];;
		  /*epilog */

		  MELT_LOCATION ("warmelt-normatch.melt:3088:/ clear");
		 /*clear *//*_#MELT_NEED_DBG__L3*/ meltfnum[2] = 0;
		  /*^clear */
		 /*clear *//*_.IF___V9*/ meltfptr[8] = 0;
		  /*^clear */
		 /*clear *//*_.PROGN___V11*/ meltfptr[9] = 0;
		}

#else /*MELT_HAVE_DEBUG */
		/*^cppif.else */
		/*_.IFCPP___V8*/ meltfptr[7] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
		;

#if MELT_HAVE_DEBUG
		MELT_LOCATION ("warmelt-normatch.melt:3089:/ cppif.then");
		/*^block */
		/*anyblock */
		{


		  {
		    /*^locexp */
		    /*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
		    melt_dbgcounter++;
#endif
		    ;
		  }
		  ;
		  /*^checksignal */
		  MELT_CHECK_SIGNAL ();
		  ;
       /*_#MELT_NEED_DBG__L5*/ meltfnum[3] =
		    /*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
		    ( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
		    0		/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
		    ;;
		  MELT_LOCATION ("warmelt-normatch.melt:3089:/ cond");
		  /*cond */ if ( /*_#MELT_NEED_DBG__L5*/ meltfnum[3])	/*then */
		    {
		      /*^cond.then */
		      /*^block */
		      /*anyblock */
		      {

	 /*_#THE_MELTCALLCOUNT__L6*/ meltfnum[2] =
#ifdef meltcallcount
			  meltcallcount	/* the_meltcallcount */
#else
			  0L
#endif /* meltcallcount the_meltcallcount */
			  ;;
			MELT_LOCATION
			  ("warmelt-normatch.melt:3089:/ checksignal");
			MELT_CHECK_SIGNAL ();
			;
			/*^apply */
			/*apply */
			{
			  union meltparam_un argtab[5];
			  memset (&argtab, 0, sizeof (argtab));
			  /*^apply.arg */
			  argtab[0].meltbp_long =
			    /*_#THE_MELTCALLCOUNT__L6*/ meltfnum[2];
			  /*^apply.arg */
			  argtab[1].meltbp_cstring = "warmelt-normatch.melt";
			  /*^apply.arg */
			  argtab[2].meltbp_long = 3089;
			  /*^apply.arg */
			  argtab[3].meltbp_cstring = "stastep return stastep";
			  /*^apply.arg */
			  argtab[4].meltbp_aptr =
			    (melt_ptr_t *) & /*_.STASTEP__V7*/ meltfptr[6];
			  /*_.MELT_DEBUG_FUN__V14*/ meltfptr[13] =
			    melt_apply ((meltclosure_ptr_t)
					(( /*!MELT_DEBUG_FUN */ meltfrout->
					  tabval[1])),
					(melt_ptr_t) (( /*nil */ NULL)),
					(MELTBPARSTR_LONG MELTBPARSTR_CSTRING
					 MELTBPARSTR_LONG MELTBPARSTR_CSTRING
					 MELTBPARSTR_PTR ""), argtab, "",
					(union meltparam_un *) 0);
			}
			;
			/*_.IF___V13*/ meltfptr[9] =
			  /*_.MELT_DEBUG_FUN__V14*/ meltfptr[13];;
			/*epilog */

			MELT_LOCATION ("warmelt-normatch.melt:3089:/ clear");
		   /*clear *//*_#THE_MELTCALLCOUNT__L6*/ meltfnum[2] = 0;
			/*^clear */
		   /*clear *//*_.MELT_DEBUG_FUN__V14*/ meltfptr[13] = 0;
		      }
		      ;
		    }
		  else
		    {		/*^cond.else */

	/*_.IF___V13*/ meltfptr[9] = NULL;;
		    }
		  ;
		  MELT_LOCATION ("warmelt-normatch.melt:3089:/ quasiblock");


		  /*_.PROGN___V15*/ meltfptr[13] =
		    /*_.IF___V13*/ meltfptr[9];;
		  /*^compute */
		  /*_.IFCPP___V12*/ meltfptr[8] =
		    /*_.PROGN___V15*/ meltfptr[13];;
		  /*epilog */

		  MELT_LOCATION ("warmelt-normatch.melt:3089:/ clear");
		 /*clear *//*_#MELT_NEED_DBG__L5*/ meltfnum[3] = 0;
		  /*^clear */
		 /*clear *//*_.IF___V13*/ meltfptr[9] = 0;
		  /*^clear */
		 /*clear *//*_.PROGN___V15*/ meltfptr[13] = 0;
		}

#else /*MELT_HAVE_DEBUG */
		/*^cppif.else */
		/*_.IFCPP___V12*/ meltfptr[8] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
		;

#if MELT_HAVE_DEBUG
		MELT_LOCATION ("warmelt-normatch.melt:3090:/ cppif.then");
		/*^block */
		/*anyblock */
		{

		  /*^checksignal */
		  MELT_CHECK_SIGNAL ();
		  ;
       /*_#IS_A__L7*/ meltfnum[2] =
		    melt_is_instance_of ((melt_ptr_t)
					 ( /*_.STASTEP__V7*/ meltfptr[6]),
					 (melt_ptr_t) (( /*!CLASS_MATCH_STEP */ meltfrout->tabval[2])));;
		  MELT_LOCATION ("warmelt-normatch.melt:3090:/ cond");
		  /*cond */ if ( /*_#IS_A__L7*/ meltfnum[2])	/*then */
		    {
		      /*^cond.then */
		      /*_.IFELSE___V17*/ meltfptr[13] = ( /*nil */ NULL);;
		    }
		  else
		    {
		      MELT_LOCATION
			("warmelt-normatch.melt:3090:/ cond.else");

		      /*^block */
		      /*anyblock */
		      {




			{
			  /*^locexp */
			  melt_assert_failed (("check stastep"),
					      ("warmelt-normatch.melt")
					      ? ("warmelt-normatch.melt") :
					      __FILE__,
					      (3090) ? (3090) : __LINE__,
					      __FUNCTION__);
			  ;
			}
			;
		   /*clear *//*_.IFELSE___V17*/ meltfptr[13] = 0;
			/*epilog */
		      }
		      ;
		    }
		  ;
		  /*^compute */
		  /*_.IFCPP___V16*/ meltfptr[9] =
		    /*_.IFELSE___V17*/ meltfptr[13];;
		  /*epilog */

		  MELT_LOCATION ("warmelt-normatch.melt:3090:/ clear");
		 /*clear *//*_#IS_A__L7*/ meltfnum[2] = 0;
		  /*^clear */
		 /*clear *//*_.IFELSE___V17*/ meltfptr[13] = 0;
		}

#else /*MELT_HAVE_DEBUG */
		/*^cppif.else */
		/*_.IFCPP___V16*/ meltfptr[9] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
		;
		MELT_LOCATION ("warmelt-normatch.melt:3091:/ checksignal");
		MELT_CHECK_SIGNAL ();
		;
		/*^quasiblock */


		/*_.RETVAL___V1*/ meltfptr[0] =
		  /*_.STASTEP__V7*/ meltfptr[6];;

		{
		  MELT_LOCATION ("warmelt-normatch.melt:3091:/ locexp");
		  /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
		  if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
		    melt_warn_for_no_expected_secondary_results ();
		  /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
		  ;
		}
		;
		/*^finalreturn */
		;
		/*finalret */ goto labend_rout;
		/*_.LET___V6*/ meltfptr[5] = /*_.RETURN___V18*/ meltfptr[13];;

		MELT_LOCATION ("warmelt-normatch.melt:3086:/ clear");
	       /*clear *//*_.STASTEP__V7*/ meltfptr[6] = 0;
		/*^clear */
	       /*clear *//*_.IFCPP___V8*/ meltfptr[7] = 0;
		/*^clear */
	       /*clear *//*_.IFCPP___V12*/ meltfptr[8] = 0;
		/*^clear */
	       /*clear *//*_.IFCPP___V16*/ meltfptr[9] = 0;
		/*^clear */
	       /*clear *//*_.RETURN___V18*/ meltfptr[13] = 0;
		/*_.IFELSE___V5*/ meltfptr[3] = /*_.LET___V6*/ meltfptr[5];;
		/*epilog */

		MELT_LOCATION ("warmelt-normatch.melt:3085:/ clear");
	       /*clear *//*_.LET___V6*/ meltfptr[5] = 0;
	      }
	      ;
	    }
	  else
	    {			/*^cond.else */

	      /*^block */
	      /*anyblock */
	      {


#if MELT_HAVE_DEBUG
		MELT_LOCATION ("warmelt-normatch.melt:3093:/ cppif.then");
		/*^block */
		/*anyblock */
		{


		  {
		    /*^locexp */
		    /*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
		    melt_dbgcounter++;
#endif
		    ;
		  }
		  ;
		  /*^checksignal */
		  MELT_CHECK_SIGNAL ();
		  ;
       /*_#MELT_NEED_DBG__L8*/ meltfnum[3] =
		    /*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
		    ( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
		    0		/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
		    ;;
		  MELT_LOCATION ("warmelt-normatch.melt:3093:/ cond");
		  /*cond */ if ( /*_#MELT_NEED_DBG__L8*/ meltfnum[3])	/*then */
		    {
		      /*^cond.then */
		      /*^block */
		      /*anyblock */
		      {

	 /*_#THE_MELTCALLCOUNT__L9*/ meltfnum[2] =
#ifdef meltcallcount
			  meltcallcount	/* the_meltcallcount */
#else
			  0L
#endif /* meltcallcount the_meltcallcount */
			  ;;
			MELT_LOCATION
			  ("warmelt-normatch.melt:3093:/ checksignal");
			MELT_CHECK_SIGNAL ();
			;
			/*^apply */
			/*apply */
			{
			  union meltparam_un argtab[5];
			  memset (&argtab, 0, sizeof (argtab));
			  /*^apply.arg */
			  argtab[0].meltbp_long =
			    /*_#THE_MELTCALLCOUNT__L9*/ meltfnum[2];
			  /*^apply.arg */
			  argtab[1].meltbp_cstring = "warmelt-normatch.melt";
			  /*^apply.arg */
			  argtab[2].meltbp_long = 3093;
			  /*^apply.arg */
			  argtab[3].meltbp_cstring =
			    "start_step ordinary step";
			  /*^apply.arg */
			  argtab[4].meltbp_aptr =
			    (melt_ptr_t *) & /*_.STEP__V2*/ meltfptr[1];
			  /*_.MELT_DEBUG_FUN__V21*/ meltfptr[8] =
			    melt_apply ((meltclosure_ptr_t)
					(( /*!MELT_DEBUG_FUN */ meltfrout->
					  tabval[1])),
					(melt_ptr_t) (( /*nil */ NULL)),
					(MELTBPARSTR_LONG MELTBPARSTR_CSTRING
					 MELTBPARSTR_LONG MELTBPARSTR_CSTRING
					 MELTBPARSTR_PTR ""), argtab, "",
					(union meltparam_un *) 0);
			}
			;
			/*_.IF___V20*/ meltfptr[7] =
			  /*_.MELT_DEBUG_FUN__V21*/ meltfptr[8];;
			/*epilog */

			MELT_LOCATION ("warmelt-normatch.melt:3093:/ clear");
		   /*clear *//*_#THE_MELTCALLCOUNT__L9*/ meltfnum[2] = 0;
			/*^clear */
		   /*clear *//*_.MELT_DEBUG_FUN__V21*/ meltfptr[8] = 0;
		      }
		      ;
		    }
		  else
		    {		/*^cond.else */

	/*_.IF___V20*/ meltfptr[7] = NULL;;
		    }
		  ;
		  MELT_LOCATION ("warmelt-normatch.melt:3093:/ quasiblock");


		  /*_.PROGN___V22*/ meltfptr[9] = /*_.IF___V20*/ meltfptr[7];;
		  /*^compute */
		  /*_.IFCPP___V19*/ meltfptr[6] =
		    /*_.PROGN___V22*/ meltfptr[9];;
		  /*epilog */

		  MELT_LOCATION ("warmelt-normatch.melt:3093:/ clear");
		 /*clear *//*_#MELT_NEED_DBG__L8*/ meltfnum[3] = 0;
		  /*^clear */
		 /*clear *//*_.IF___V20*/ meltfptr[7] = 0;
		  /*^clear */
		 /*clear *//*_.PROGN___V22*/ meltfptr[9] = 0;
		}

#else /*MELT_HAVE_DEBUG */
		/*^cppif.else */
		/*_.IFCPP___V19*/ meltfptr[6] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
		;

#if MELT_HAVE_DEBUG
		MELT_LOCATION ("warmelt-normatch.melt:3094:/ cppif.then");
		/*^block */
		/*anyblock */
		{

		  /*^checksignal */
		  MELT_CHECK_SIGNAL ();
		  ;
       /*_#IS_A__L10*/ meltfnum[2] =
		    melt_is_instance_of ((melt_ptr_t)
					 ( /*_.STEP__V2*/ meltfptr[1]),
					 (melt_ptr_t) (( /*!CLASS_MATCH_STEP */ meltfrout->tabval[2])));;
		  MELT_LOCATION ("warmelt-normatch.melt:3094:/ cond");
		  /*cond */ if ( /*_#IS_A__L10*/ meltfnum[2])	/*then */
		    {
		      /*^cond.then */
		      /*_.IFELSE___V24*/ meltfptr[5] = ( /*nil */ NULL);;
		    }
		  else
		    {
		      MELT_LOCATION
			("warmelt-normatch.melt:3094:/ cond.else");

		      /*^block */
		      /*anyblock */
		      {




			{
			  /*^locexp */
			  melt_assert_failed (("check step"),
					      ("warmelt-normatch.melt")
					      ? ("warmelt-normatch.melt") :
					      __FILE__,
					      (3094) ? (3094) : __LINE__,
					      __FUNCTION__);
			  ;
			}
			;
		   /*clear *//*_.IFELSE___V24*/ meltfptr[5] = 0;
			/*epilog */
		      }
		      ;
		    }
		  ;
		  /*^compute */
		  /*_.IFCPP___V23*/ meltfptr[13] =
		    /*_.IFELSE___V24*/ meltfptr[5];;
		  /*epilog */

		  MELT_LOCATION ("warmelt-normatch.melt:3094:/ clear");
		 /*clear *//*_#IS_A__L10*/ meltfnum[2] = 0;
		  /*^clear */
		 /*clear *//*_.IFELSE___V24*/ meltfptr[5] = 0;
		}

#else /*MELT_HAVE_DEBUG */
		/*^cppif.else */
		/*_.IFCPP___V23*/ meltfptr[13] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
		;
		MELT_LOCATION ("warmelt-normatch.melt:3095:/ checksignal");
		MELT_CHECK_SIGNAL ();
		;
		/*^quasiblock */


		/*_.RETVAL___V1*/ meltfptr[0] = /*_.STEP__V2*/ meltfptr[1];;

		{
		  MELT_LOCATION ("warmelt-normatch.melt:3095:/ locexp");
		  /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
		  if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
		    melt_warn_for_no_expected_secondary_results ();
		  /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
		  ;
		}
		;
		/*^finalreturn */
		;
		/*finalret */ goto labend_rout;
		MELT_LOCATION ("warmelt-normatch.melt:3092:/ quasiblock");


		/*_.PROGN___V26*/ meltfptr[7] =
		  /*_.RETURN___V25*/ meltfptr[8];;
		/*^compute */
		/*_.IFELSE___V5*/ meltfptr[3] =
		  /*_.PROGN___V26*/ meltfptr[7];;
		/*epilog */

		MELT_LOCATION ("warmelt-normatch.melt:3085:/ clear");
	       /*clear *//*_.IFCPP___V19*/ meltfptr[6] = 0;
		/*^clear */
	       /*clear *//*_.IFCPP___V23*/ meltfptr[13] = 0;
		/*^clear */
	       /*clear *//*_.RETURN___V25*/ meltfptr[8] = 0;
		/*^clear */
	       /*clear *//*_.PROGN___V26*/ meltfptr[7] = 0;
	      }
	      ;
	    }
	  ;
	  /*_.IFELSE___V3*/ meltfptr[2] = /*_.IFELSE___V5*/ meltfptr[3];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-normatch.melt:3082:/ clear");
	     /*clear *//*_#IS_A__L2*/ meltfnum[1] = 0;
	  /*^clear */
	     /*clear *//*_.IFELSE___V5*/ meltfptr[3] = 0;
	}
	;
      }
    ;
    MELT_LOCATION ("warmelt-normatch.melt:3080:/ quasiblock");


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.IFELSE___V3*/ meltfptr[2];;

    {
      MELT_LOCATION ("warmelt-normatch.melt:3080:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_#NULL__L1*/ meltfnum[0] = 0;
    /*^clear */
	   /*clear *//*_.IFELSE___V3*/ meltfptr[2] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("START_STEP", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_66_warmelt_normatch_START_STEP_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_66_warmelt_normatch_START_STEP */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_67_warmelt_normatch_PUTTHEN_MATCHTHEN (meltclosure_ptr_t meltclosp_,
						melt_ptr_t meltfirstargp_,
						const melt_argdescr_cell_t
						meltxargdescr_[],
						union meltparam_un *
						meltxargtab_,
						const melt_argdescr_cell_t
						meltxresdescr_[],
						union meltparam_un *
						meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_67_warmelt_normatch_PUTTHEN_MATCHTHEN_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_67_warmelt_normatch_PUTTHEN_MATCHTHEN_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 56
    melt_ptr_t mcfr_varptr[56];
#define MELTFRAM_NBVARNUM 24
    long mcfr_varnum[24];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_67_warmelt_normatch_PUTTHEN_MATCHTHEN is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_67_warmelt_normatch_PUTTHEN_MATCHTHEN_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 56; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_67_warmelt_normatch_PUTTHEN_MATCHTHEN nbval 56*/
  meltfram__.mcfr_nbvar = 56 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("PUTTHEN_MATCHTHEN", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-normatch.melt:3099:/ getarg");
 /*_.RECV__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.THENSTEP__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.THENSTEP__V3*/ meltfptr[2])) !=
	      NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-normatch.melt:3100:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L1*/ meltfnum[0] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-normatch.melt:3100:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L1*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-normatch.melt:3100:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-normatch.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 3100;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "putthen_matchthen recv";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.RECV__V2*/ meltfptr[1];
	      /*_.MELT_DEBUG_FUN__V6*/ meltfptr[5] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V5*/ meltfptr[4] = /*_.MELT_DEBUG_FUN__V6*/ meltfptr[5];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-normatch.melt:3100:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V6*/ meltfptr[5] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V5*/ meltfptr[4] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-normatch.melt:3100:/ quasiblock");


      /*_.PROGN___V7*/ meltfptr[5] = /*_.IF___V5*/ meltfptr[4];;
      /*^compute */
      /*_.IFCPP___V4*/ meltfptr[3] = /*_.PROGN___V7*/ meltfptr[5];;
      /*epilog */

      MELT_LOCATION ("warmelt-normatch.melt:3100:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L1*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IF___V5*/ meltfptr[4] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V7*/ meltfptr[5] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V4*/ meltfptr[3] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-normatch.melt:3101:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L3*/ meltfnum[1] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.RECV__V2*/ meltfptr[1]),
			     (melt_ptr_t) (( /*!CLASS_MATCH_STEP_THEN */
					    meltfrout->tabval[1])));;
      MELT_LOCATION ("warmelt-normatch.melt:3101:/ cond");
      /*cond */ if ( /*_#IS_A__L3*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V9*/ meltfptr[5] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-normatch.melt:3101:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check recv"),
				  ("warmelt-normatch.melt")
				  ? ("warmelt-normatch.melt") : __FILE__,
				  (3101) ? (3101) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V9*/ meltfptr[5] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V8*/ meltfptr[4] = /*_.IFELSE___V9*/ meltfptr[5];;
      /*epilog */

      MELT_LOCATION ("warmelt-normatch.melt:3101:/ clear");
	     /*clear *//*_#IS_A__L3*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V9*/ meltfptr[5] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V8*/ meltfptr[4] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-normatch.melt:3102:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L4*/ meltfnum[0] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-normatch.melt:3102:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L4*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L5*/ meltfnum[1] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-normatch.melt:3102:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L5*/ meltfnum[1];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-normatch.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 3102;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "putthen_matchthen thenstep";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.THENSTEP__V3*/ meltfptr[2];
	      /*_.MELT_DEBUG_FUN__V12*/ meltfptr[11] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V11*/ meltfptr[10] =
	      /*_.MELT_DEBUG_FUN__V12*/ meltfptr[11];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-normatch.melt:3102:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L5*/ meltfnum[1] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V12*/ meltfptr[11] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V11*/ meltfptr[10] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-normatch.melt:3102:/ quasiblock");


      /*_.PROGN___V13*/ meltfptr[11] = /*_.IF___V11*/ meltfptr[10];;
      /*^compute */
      /*_.IFCPP___V10*/ meltfptr[5] = /*_.PROGN___V13*/ meltfptr[11];;
      /*epilog */

      MELT_LOCATION ("warmelt-normatch.melt:3102:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L4*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IF___V11*/ meltfptr[10] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V13*/ meltfptr[11] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V10*/ meltfptr[5] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-normatch.melt:3103:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L6*/ meltfnum[1] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.THENSTEP__V3*/ meltfptr[2]),
			     (melt_ptr_t) (( /*!CLASS_MATCH_STEP */
					    meltfrout->tabval[2])));;
      MELT_LOCATION ("warmelt-normatch.melt:3103:/ cond");
      /*cond */ if ( /*_#IS_A__L6*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V15*/ meltfptr[11] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-normatch.melt:3103:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check thenstep"),
				  ("warmelt-normatch.melt")
				  ? ("warmelt-normatch.melt") : __FILE__,
				  (3103) ? (3103) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V15*/ meltfptr[11] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V14*/ meltfptr[10] = /*_.IFELSE___V15*/ meltfptr[11];;
      /*epilog */

      MELT_LOCATION ("warmelt-normatch.melt:3103:/ clear");
	     /*clear *//*_#IS_A__L6*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V15*/ meltfptr[11] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V14*/ meltfptr[10] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-normatch.melt:3104:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#__L7*/ meltfnum[0] =
	(( /*_.RECV__V2*/ meltfptr[1]) != ( /*_.THENSTEP__V3*/ meltfptr[2]));;
      MELT_LOCATION ("warmelt-normatch.melt:3104:/ cond");
      /*cond */ if ( /*_#__L7*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V17*/ meltfptr[16] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-normatch.melt:3104:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check recv!=then"),
				  ("warmelt-normatch.melt")
				  ? ("warmelt-normatch.melt") : __FILE__,
				  (3104) ? (3104) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V17*/ meltfptr[16] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V16*/ meltfptr[11] = /*_.IFELSE___V17*/ meltfptr[16];;
      /*epilog */

      MELT_LOCATION ("warmelt-normatch.melt:3104:/ clear");
	     /*clear *//*_#__L7*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V17*/ meltfptr[16] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V16*/ meltfptr[11] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-normatch.melt:3106:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_APPLICATION_SHALLOWER__L8*/ meltfnum[1] =
	(melt_application_depth () < 100);;
      MELT_LOCATION ("warmelt-normatch.melt:3106:/ cond");
      /*cond */ if ( /*_#MELT_APPLICATION_SHALLOWER__L8*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V19*/ meltfptr[18] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-normatch.melt:3106:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("putthen_matchthen check shallow100"),
				  ("warmelt-normatch.melt")
				  ? ("warmelt-normatch.melt") : __FILE__,
				  (3106) ? (3106) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V19*/ meltfptr[18] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V18*/ meltfptr[16] = /*_.IFELSE___V19*/ meltfptr[18];;
      /*epilog */

      MELT_LOCATION ("warmelt-normatch.melt:3106:/ clear");
	     /*clear *//*_#MELT_APPLICATION_SHALLOWER__L8*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V19*/ meltfptr[18] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V18*/ meltfptr[16] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-normatch.melt:3109:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#IS_A__L9*/ meltfnum[0] =
      melt_is_instance_of ((melt_ptr_t) ( /*_.RECV__V2*/ meltfptr[1]),
			   (melt_ptr_t) (( /*!CLASS_MATCH_STEP_TEST_INSTANCE */ meltfrout->tabval[3])));;
    MELT_LOCATION ("warmelt-normatch.melt:3109:/ cond");
    /*cond */ if ( /*_#IS_A__L9*/ meltfnum[0])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{


	  {
	    MELT_LOCATION ("warmelt-normatch.melt:3110:/ locexp");

#if MELT_HAVE_DEBUG
	    if (melt_need_debug (0))
	      melt_dbgshortbacktrace (("putthen_matchthen testinstance!!!"),
				      (20));
#endif
	    ;
	  }
	  ;
	  /*epilog */
	}
	;
      }				/*noelse */
    ;
    MELT_LOCATION ("warmelt-normatch.melt:3111:/ quasiblock");


    /*^getslot */
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.RECV__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 2, "MSTEP_THEN");
  /*_.MYTHEN__V21*/ meltfptr[20] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-normatch.melt:3112:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      /*_.STARTHEN__V22*/ meltfptr[21] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!START_STEP */ meltfrout->tabval[4])),
		    (melt_ptr_t) ( /*_.THENSTEP__V3*/ meltfptr[2]), (""),
		    (union meltparam_un *) 0, "", (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-normatch.melt:3114:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#__L10*/ meltfnum[1] =
      (( /*_.RECV__V2*/ meltfptr[1]) == ( /*_.STARTHEN__V22*/ meltfptr[21]));;
    MELT_LOCATION ("warmelt-normatch.melt:3114:/ cond");
    /*cond */ if ( /*_#__L10*/ meltfnum[1])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{


#if MELT_HAVE_DEBUG
	  MELT_LOCATION ("warmelt-normatch.melt:3116:/ cppif.then");
	  /*^block */
	  /*anyblock */
	  {


	    {
	      /*^locexp */
	      /*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	      melt_dbgcounter++;
#endif
	      ;
	    }
	    ;
	    /*^checksignal */
	    MELT_CHECK_SIGNAL ();
	    ;
     /*_#MELT_NEED_DBG__L11*/ meltfnum[10] =
	      /*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	      ( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	      0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	      ;;
	    MELT_LOCATION ("warmelt-normatch.melt:3116:/ cond");
	    /*cond */ if ( /*_#MELT_NEED_DBG__L11*/ meltfnum[10])	/*then */
	      {
		/*^cond.then */
		/*^block */
		/*anyblock */
		{

       /*_#THE_MELTCALLCOUNT__L12*/ meltfnum[11] =
#ifdef meltcallcount
		    meltcallcount	/* the_meltcallcount */
#else
		    0L
#endif /* meltcallcount the_meltcallcount */
		    ;;
		  MELT_LOCATION ("warmelt-normatch.melt:3116:/ checksignal");
		  MELT_CHECK_SIGNAL ();
		  ;
		  /*^apply */
		  /*apply */
		  {
		    union meltparam_un argtab[5];
		    memset (&argtab, 0, sizeof (argtab));
		    /*^apply.arg */
		    argtab[0].meltbp_long =
		      /*_#THE_MELTCALLCOUNT__L12*/ meltfnum[11];
		    /*^apply.arg */
		    argtab[1].meltbp_cstring = "warmelt-normatch.melt";
		    /*^apply.arg */
		    argtab[2].meltbp_long = 3116;
		    /*^apply.arg */
		    argtab[3].meltbp_cstring =
		      "putthen_matchthen recv same starthen";
		    /*^apply.arg */
		    argtab[4].meltbp_aptr =
		      (melt_ptr_t *) & /*_.STARTHEN__V22*/ meltfptr[21];
		    /*_.MELT_DEBUG_FUN__V26*/ meltfptr[25] =
		      melt_apply ((meltclosure_ptr_t)
				  (( /*!MELT_DEBUG_FUN */ meltfrout->
				    tabval[0])),
				  (melt_ptr_t) (( /*nil */ NULL)),
				  (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_PTR ""), argtab, "",
				  (union meltparam_un *) 0);
		  }
		  ;
		  /*_.IF___V25*/ meltfptr[24] =
		    /*_.MELT_DEBUG_FUN__V26*/ meltfptr[25];;
		  /*epilog */

		  MELT_LOCATION ("warmelt-normatch.melt:3116:/ clear");
		 /*clear *//*_#THE_MELTCALLCOUNT__L12*/ meltfnum[11] = 0;
		  /*^clear */
		 /*clear *//*_.MELT_DEBUG_FUN__V26*/ meltfptr[25] = 0;
		}
		;
	      }
	    else
	      {			/*^cond.else */

      /*_.IF___V25*/ meltfptr[24] = NULL;;
	      }
	    ;
	    MELT_LOCATION ("warmelt-normatch.melt:3116:/ quasiblock");


	    /*_.PROGN___V27*/ meltfptr[25] = /*_.IF___V25*/ meltfptr[24];;
	    /*^compute */
	    /*_.IFCPP___V24*/ meltfptr[23] = /*_.PROGN___V27*/ meltfptr[25];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-normatch.melt:3116:/ clear");
	       /*clear *//*_#MELT_NEED_DBG__L11*/ meltfnum[10] = 0;
	    /*^clear */
	       /*clear *//*_.IF___V25*/ meltfptr[24] = 0;
	    /*^clear */
	       /*clear *//*_.PROGN___V27*/ meltfptr[25] = 0;
	  }

#else /*MELT_HAVE_DEBUG */
	  /*^cppif.else */
	  /*_.IFCPP___V24*/ meltfptr[23] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
	  ;
	  MELT_LOCATION ("warmelt-normatch.melt:3117:/ quasiblock");


   /*_.RETVAL___V1*/ meltfptr[0] = NULL;;

	  {
	    MELT_LOCATION ("warmelt-normatch.melt:3117:/ locexp");
	    /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
	    if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	      melt_warn_for_no_expected_secondary_results ();
	    /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
	    ;
	  }
	  ;
	  /*^finalreturn */
	  ;
	  /*finalret */ goto labend_rout;
	  MELT_LOCATION ("warmelt-normatch.melt:3115:/ quasiblock");


	  /*_.PROGN___V29*/ meltfptr[25] = /*_.RETURN___V28*/ meltfptr[24];;
	  /*^compute */
	  /*_.IF___V23*/ meltfptr[22] = /*_.PROGN___V29*/ meltfptr[25];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-normatch.melt:3114:/ clear");
	     /*clear *//*_.IFCPP___V24*/ meltfptr[23] = 0;
	  /*^clear */
	     /*clear *//*_.RETURN___V28*/ meltfptr[24] = 0;
	  /*^clear */
	     /*clear *//*_.PROGN___V29*/ meltfptr[25] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

  /*_.IF___V23*/ meltfptr[22] = NULL;;
      }
    ;
    MELT_LOCATION ("warmelt-normatch.melt:3118:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#__L13*/ meltfnum[11] =
      (( /*_.MYTHEN__V21*/ meltfptr[20]) ==
       ( /*_.STARTHEN__V22*/ meltfptr[21]));;
    MELT_LOCATION ("warmelt-normatch.melt:3118:/ cond");
    /*cond */ if ( /*_#__L13*/ meltfnum[11])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{


#if MELT_HAVE_DEBUG
	  MELT_LOCATION ("warmelt-normatch.melt:3120:/ cppif.then");
	  /*^block */
	  /*anyblock */
	  {


	    {
	      /*^locexp */
	      /*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	      melt_dbgcounter++;
#endif
	      ;
	    }
	    ;
	    /*^checksignal */
	    MELT_CHECK_SIGNAL ();
	    ;
     /*_#MELT_NEED_DBG__L14*/ meltfnum[10] =
	      /*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	      ( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	      0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	      ;;
	    MELT_LOCATION ("warmelt-normatch.melt:3120:/ cond");
	    /*cond */ if ( /*_#MELT_NEED_DBG__L14*/ meltfnum[10])	/*then */
	      {
		/*^cond.then */
		/*^block */
		/*anyblock */
		{

       /*_#THE_MELTCALLCOUNT__L15*/ meltfnum[14] =
#ifdef meltcallcount
		    meltcallcount	/* the_meltcallcount */
#else
		    0L
#endif /* meltcallcount the_meltcallcount */
		    ;;
		  MELT_LOCATION ("warmelt-normatch.melt:3120:/ checksignal");
		  MELT_CHECK_SIGNAL ();
		  ;
		  /*^apply */
		  /*apply */
		  {
		    union meltparam_un argtab[5];
		    memset (&argtab, 0, sizeof (argtab));
		    /*^apply.arg */
		    argtab[0].meltbp_long =
		      /*_#THE_MELTCALLCOUNT__L15*/ meltfnum[14];
		    /*^apply.arg */
		    argtab[1].meltbp_cstring = "warmelt-normatch.melt";
		    /*^apply.arg */
		    argtab[2].meltbp_long = 3120;
		    /*^apply.arg */
		    argtab[3].meltbp_cstring =
		      "putthen_matchthen mythen same starthen";
		    /*^apply.arg */
		    argtab[4].meltbp_aptr =
		      (melt_ptr_t *) & /*_.STARTHEN__V22*/ meltfptr[21];
		    /*_.MELT_DEBUG_FUN__V33*/ meltfptr[32] =
		      melt_apply ((meltclosure_ptr_t)
				  (( /*!MELT_DEBUG_FUN */ meltfrout->
				    tabval[0])),
				  (melt_ptr_t) (( /*nil */ NULL)),
				  (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_PTR ""), argtab, "",
				  (union meltparam_un *) 0);
		  }
		  ;
		  /*_.IF___V32*/ meltfptr[25] =
		    /*_.MELT_DEBUG_FUN__V33*/ meltfptr[32];;
		  /*epilog */

		  MELT_LOCATION ("warmelt-normatch.melt:3120:/ clear");
		 /*clear *//*_#THE_MELTCALLCOUNT__L15*/ meltfnum[14] = 0;
		  /*^clear */
		 /*clear *//*_.MELT_DEBUG_FUN__V33*/ meltfptr[32] = 0;
		}
		;
	      }
	    else
	      {			/*^cond.else */

      /*_.IF___V32*/ meltfptr[25] = NULL;;
	      }
	    ;
	    MELT_LOCATION ("warmelt-normatch.melt:3120:/ quasiblock");


	    /*_.PROGN___V34*/ meltfptr[32] = /*_.IF___V32*/ meltfptr[25];;
	    /*^compute */
	    /*_.IFCPP___V31*/ meltfptr[24] = /*_.PROGN___V34*/ meltfptr[32];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-normatch.melt:3120:/ clear");
	       /*clear *//*_#MELT_NEED_DBG__L14*/ meltfnum[10] = 0;
	    /*^clear */
	       /*clear *//*_.IF___V32*/ meltfptr[25] = 0;
	    /*^clear */
	       /*clear *//*_.PROGN___V34*/ meltfptr[32] = 0;
	  }

#else /*MELT_HAVE_DEBUG */
	  /*^cppif.else */
	  /*_.IFCPP___V31*/ meltfptr[24] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
	  ;
	  MELT_LOCATION ("warmelt-normatch.melt:3121:/ quasiblock");


   /*_.RETVAL___V1*/ meltfptr[0] = NULL;;

	  {
	    MELT_LOCATION ("warmelt-normatch.melt:3121:/ locexp");
	    /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
	    if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	      melt_warn_for_no_expected_secondary_results ();
	    /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
	    ;
	  }
	  ;
	  /*^finalreturn */
	  ;
	  /*finalret */ goto labend_rout;
	  MELT_LOCATION ("warmelt-normatch.melt:3119:/ quasiblock");


	  /*_.PROGN___V36*/ meltfptr[32] = /*_.RETURN___V35*/ meltfptr[25];;
	  /*^compute */
	  /*_.IF___V30*/ meltfptr[23] = /*_.PROGN___V36*/ meltfptr[32];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-normatch.melt:3118:/ clear");
	     /*clear *//*_.IFCPP___V31*/ meltfptr[24] = 0;
	  /*^clear */
	     /*clear *//*_.RETURN___V35*/ meltfptr[25] = 0;
	  /*^clear */
	     /*clear *//*_.PROGN___V36*/ meltfptr[32] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

  /*_.IF___V30*/ meltfptr[23] = NULL;;
      }
    ;
    MELT_LOCATION ("warmelt-normatch.melt:3122:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#NULL__L16*/ meltfnum[14] =
      (( /*_.MYTHEN__V21*/ meltfptr[20]) == NULL);;
    MELT_LOCATION ("warmelt-normatch.melt:3122:/ cond");
    /*cond */ if ( /*_#NULL__L16*/ meltfnum[14])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-normatch.melt:3124:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^quasiblock */


	  /*^putslot */
	  /*putslot */
	  melt_assertmsg ("putslot checkobj @MSTEP_THEN",
			  melt_magic_discr ((melt_ptr_t)
					    ( /*_.RECV__V2*/ meltfptr[1])) ==
			  MELTOBMAG_OBJECT);
	  melt_putfield_object (( /*_.RECV__V2*/ meltfptr[1]), (2),
				( /*_.STARTHEN__V22*/ meltfptr[21]),
				"MSTEP_THEN");
	  ;
	  /*^touch */
	  meltgc_touch ( /*_.RECV__V2*/ meltfptr[1]);
	  ;
	  /*^touchobj */

	  melt_dbgtrace_written_object ( /*_.RECV__V2*/ meltfptr[1],
					"put-fields");
	  ;


#if MELT_HAVE_DEBUG
	  MELT_LOCATION ("warmelt-normatch.melt:3125:/ cppif.then");
	  /*^block */
	  /*anyblock */
	  {


	    {
	      /*^locexp */
	      /*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	      melt_dbgcounter++;
#endif
	      ;
	    }
	    ;
	    /*^checksignal */
	    MELT_CHECK_SIGNAL ();
	    ;
     /*_#MELT_NEED_DBG__L17*/ meltfnum[10] =
	      /*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	      ( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	      0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	      ;;
	    MELT_LOCATION ("warmelt-normatch.melt:3125:/ cond");
	    /*cond */ if ( /*_#MELT_NEED_DBG__L17*/ meltfnum[10])	/*then */
	      {
		/*^cond.then */
		/*^block */
		/*anyblock */
		{

       /*_#THE_MELTCALLCOUNT__L18*/ meltfnum[17] =
#ifdef meltcallcount
		    meltcallcount	/* the_meltcallcount */
#else
		    0L
#endif /* meltcallcount the_meltcallcount */
		    ;;
		  MELT_LOCATION ("warmelt-normatch.melt:3125:/ checksignal");
		  MELT_CHECK_SIGNAL ();
		  ;
		  /*^apply */
		  /*apply */
		  {
		    union meltparam_un argtab[5];
		    memset (&argtab, 0, sizeof (argtab));
		    /*^apply.arg */
		    argtab[0].meltbp_long =
		      /*_#THE_MELTCALLCOUNT__L18*/ meltfnum[17];
		    /*^apply.arg */
		    argtab[1].meltbp_cstring = "warmelt-normatch.melt";
		    /*^apply.arg */
		    argtab[2].meltbp_long = 3125;
		    /*^apply.arg */
		    argtab[3].meltbp_cstring =
		      "putthen_matchthen updated recv";
		    /*^apply.arg */
		    argtab[4].meltbp_aptr =
		      (melt_ptr_t *) & /*_.RECV__V2*/ meltfptr[1];
		    /*_.MELT_DEBUG_FUN__V40*/ meltfptr[39] =
		      melt_apply ((meltclosure_ptr_t)
				  (( /*!MELT_DEBUG_FUN */ meltfrout->
				    tabval[0])),
				  (melt_ptr_t) (( /*nil */ NULL)),
				  (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_PTR ""), argtab, "",
				  (union meltparam_un *) 0);
		  }
		  ;
		  /*_.IF___V39*/ meltfptr[32] =
		    /*_.MELT_DEBUG_FUN__V40*/ meltfptr[39];;
		  /*epilog */

		  MELT_LOCATION ("warmelt-normatch.melt:3125:/ clear");
		 /*clear *//*_#THE_MELTCALLCOUNT__L18*/ meltfnum[17] = 0;
		  /*^clear */
		 /*clear *//*_.MELT_DEBUG_FUN__V40*/ meltfptr[39] = 0;
		}
		;
	      }
	    else
	      {			/*^cond.else */

      /*_.IF___V39*/ meltfptr[32] = NULL;;
	      }
	    ;
	    MELT_LOCATION ("warmelt-normatch.melt:3125:/ quasiblock");


	    /*_.PROGN___V41*/ meltfptr[39] = /*_.IF___V39*/ meltfptr[32];;
	    /*^compute */
	    /*_.IFCPP___V38*/ meltfptr[25] = /*_.PROGN___V41*/ meltfptr[39];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-normatch.melt:3125:/ clear");
	       /*clear *//*_#MELT_NEED_DBG__L17*/ meltfnum[10] = 0;
	    /*^clear */
	       /*clear *//*_.IF___V39*/ meltfptr[32] = 0;
	    /*^clear */
	       /*clear *//*_.PROGN___V41*/ meltfptr[39] = 0;
	  }

#else /*MELT_HAVE_DEBUG */
	  /*^cppif.else */
	  /*_.IFCPP___V38*/ meltfptr[25] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
	  ;
	  MELT_LOCATION ("warmelt-normatch.melt:3123:/ quasiblock");


	  /*_.PROGN___V42*/ meltfptr[32] = /*_.IFCPP___V38*/ meltfptr[25];;
	  /*^compute */
	  /*_.IFELSE___V37*/ meltfptr[24] = /*_.PROGN___V42*/ meltfptr[32];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-normatch.melt:3122:/ clear");
	     /*clear *//*_.IFCPP___V38*/ meltfptr[25] = 0;
	  /*^clear */
	     /*clear *//*_.PROGN___V42*/ meltfptr[32] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

	/*^block */
	/*anyblock */
	{


#if MELT_HAVE_DEBUG
	  MELT_LOCATION ("warmelt-normatch.melt:3129:/ cppif.then");
	  /*^block */
	  /*anyblock */
	  {


	    {
	      /*^locexp */
	      /*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	      melt_dbgcounter++;
#endif
	      ;
	    }
	    ;
	    /*^checksignal */
	    MELT_CHECK_SIGNAL ();
	    ;
     /*_#MELT_NEED_DBG__L19*/ meltfnum[17] =
	      /*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	      ( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	      0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	      ;;
	    MELT_LOCATION ("warmelt-normatch.melt:3129:/ cond");
	    /*cond */ if ( /*_#MELT_NEED_DBG__L19*/ meltfnum[17])	/*then */
	      {
		/*^cond.then */
		/*^block */
		/*anyblock */
		{

       /*_#THE_MELTCALLCOUNT__L20*/ meltfnum[10] =
#ifdef meltcallcount
		    meltcallcount	/* the_meltcallcount */
#else
		    0L
#endif /* meltcallcount the_meltcallcount */
		    ;;
		  MELT_LOCATION ("warmelt-normatch.melt:3129:/ checksignal");
		  MELT_CHECK_SIGNAL ();
		  ;
		  /*^apply */
		  /*apply */
		  {
		    union meltparam_un argtab[5];
		    memset (&argtab, 0, sizeof (argtab));
		    /*^apply.arg */
		    argtab[0].meltbp_long =
		      /*_#THE_MELTCALLCOUNT__L20*/ meltfnum[10];
		    /*^apply.arg */
		    argtab[1].meltbp_cstring = "warmelt-normatch.melt";
		    /*^apply.arg */
		    argtab[2].meltbp_long = 3129;
		    /*^apply.arg */
		    argtab[3].meltbp_cstring =
		      "putthen_matchthen recursing in mythen";
		    /*^apply.arg */
		    argtab[4].meltbp_aptr =
		      (melt_ptr_t *) & /*_.MYTHEN__V21*/ meltfptr[20];
		    /*_.MELT_DEBUG_FUN__V45*/ meltfptr[32] =
		      melt_apply ((meltclosure_ptr_t)
				  (( /*!MELT_DEBUG_FUN */ meltfrout->
				    tabval[0])),
				  (melt_ptr_t) (( /*nil */ NULL)),
				  (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_PTR ""), argtab, "",
				  (union meltparam_un *) 0);
		  }
		  ;
		  /*_.IF___V44*/ meltfptr[25] =
		    /*_.MELT_DEBUG_FUN__V45*/ meltfptr[32];;
		  /*epilog */

		  MELT_LOCATION ("warmelt-normatch.melt:3129:/ clear");
		 /*clear *//*_#THE_MELTCALLCOUNT__L20*/ meltfnum[10] = 0;
		  /*^clear */
		 /*clear *//*_.MELT_DEBUG_FUN__V45*/ meltfptr[32] = 0;
		}
		;
	      }
	    else
	      {			/*^cond.else */

      /*_.IF___V44*/ meltfptr[25] = NULL;;
	      }
	    ;
	    MELT_LOCATION ("warmelt-normatch.melt:3129:/ quasiblock");


	    /*_.PROGN___V46*/ meltfptr[32] = /*_.IF___V44*/ meltfptr[25];;
	    /*^compute */
	    /*_.IFCPP___V43*/ meltfptr[39] = /*_.PROGN___V46*/ meltfptr[32];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-normatch.melt:3129:/ clear");
	       /*clear *//*_#MELT_NEED_DBG__L19*/ meltfnum[17] = 0;
	    /*^clear */
	       /*clear *//*_.IF___V44*/ meltfptr[25] = 0;
	    /*^clear */
	       /*clear *//*_.PROGN___V46*/ meltfptr[32] = 0;
	  }

#else /*MELT_HAVE_DEBUG */
	  /*^cppif.else */
	  /*_.IFCPP___V43*/ meltfptr[39] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
	  ;

#if MELT_HAVE_DEBUG
	  MELT_LOCATION ("warmelt-normatch.melt:3130:/ cppif.then");
	  /*^block */
	  /*anyblock */
	  {


	    {
	      /*^locexp */
	      /*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	      melt_dbgcounter++;
#endif
	      ;
	    }
	    ;
	    /*^checksignal */
	    MELT_CHECK_SIGNAL ();
	    ;
     /*_#MELT_NEED_DBG__L21*/ meltfnum[10] =
	      /*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	      ( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	      0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	      ;;
	    MELT_LOCATION ("warmelt-normatch.melt:3130:/ cond");
	    /*cond */ if ( /*_#MELT_NEED_DBG__L21*/ meltfnum[10])	/*then */
	      {
		/*^cond.then */
		/*^block */
		/*anyblock */
		{

       /*_#THE_MELTCALLCOUNT__L22*/ meltfnum[17] =
#ifdef meltcallcount
		    meltcallcount	/* the_meltcallcount */
#else
		    0L
#endif /* meltcallcount the_meltcallcount */
		    ;;
		  MELT_LOCATION ("warmelt-normatch.melt:3130:/ checksignal");
		  MELT_CHECK_SIGNAL ();
		  ;
		  /*^apply */
		  /*apply */
		  {
		    union meltparam_un argtab[5];
		    memset (&argtab, 0, sizeof (argtab));
		    /*^apply.arg */
		    argtab[0].meltbp_long =
		      /*_#THE_MELTCALLCOUNT__L22*/ meltfnum[17];
		    /*^apply.arg */
		    argtab[1].meltbp_cstring = "warmelt-normatch.melt";
		    /*^apply.arg */
		    argtab[2].meltbp_long = 3130;
		    /*^apply.arg */
		    argtab[3].meltbp_cstring =
		      "putthen_matchthen recursing for starthen";
		    /*^apply.arg */
		    argtab[4].meltbp_aptr =
		      (melt_ptr_t *) & /*_.STARTHEN__V22*/ meltfptr[21];
		    /*_.MELT_DEBUG_FUN__V49*/ meltfptr[48] =
		      melt_apply ((meltclosure_ptr_t)
				  (( /*!MELT_DEBUG_FUN */ meltfrout->
				    tabval[0])),
				  (melt_ptr_t) (( /*nil */ NULL)),
				  (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_PTR ""), argtab, "",
				  (union meltparam_un *) 0);
		  }
		  ;
		  /*_.IF___V48*/ meltfptr[32] =
		    /*_.MELT_DEBUG_FUN__V49*/ meltfptr[48];;
		  /*epilog */

		  MELT_LOCATION ("warmelt-normatch.melt:3130:/ clear");
		 /*clear *//*_#THE_MELTCALLCOUNT__L22*/ meltfnum[17] = 0;
		  /*^clear */
		 /*clear *//*_.MELT_DEBUG_FUN__V49*/ meltfptr[48] = 0;
		}
		;
	      }
	    else
	      {			/*^cond.else */

      /*_.IF___V48*/ meltfptr[32] = NULL;;
	      }
	    ;
	    MELT_LOCATION ("warmelt-normatch.melt:3130:/ quasiblock");


	    /*_.PROGN___V50*/ meltfptr[48] = /*_.IF___V48*/ meltfptr[32];;
	    /*^compute */
	    /*_.IFCPP___V47*/ meltfptr[25] = /*_.PROGN___V50*/ meltfptr[48];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-normatch.melt:3130:/ clear");
	       /*clear *//*_#MELT_NEED_DBG__L21*/ meltfnum[10] = 0;
	    /*^clear */
	       /*clear *//*_.IF___V48*/ meltfptr[32] = 0;
	    /*^clear */
	       /*clear *//*_.PROGN___V50*/ meltfptr[48] = 0;
	  }

#else /*MELT_HAVE_DEBUG */
	  /*^cppif.else */
	  /*_.IFCPP___V47*/ meltfptr[25] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
	  ;
	  MELT_LOCATION ("warmelt-normatch.melt:3131:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^msend */
	  /*msend */
	  {
	    union meltparam_un argtab[1];
	    memset (&argtab, 0, sizeof (argtab));
	    /*^ojbmsend.arg */
	    argtab[0].meltbp_aptr =
	      (melt_ptr_t *) & /*_.STARTHEN__V22*/ meltfptr[21];
	    /*_.PUT_THEN_MATCH__V51*/ meltfptr[32] =
	      meltgc_send ((melt_ptr_t) ( /*_.MYTHEN__V21*/ meltfptr[20]),
			   (melt_ptr_t) (( /*!PUT_THEN_MATCH */ meltfrout->
					  tabval[5])), (MELTBPARSTR_PTR ""),
			   argtab, "", (union meltparam_un *) 0);
	  }
	  ;

#if MELT_HAVE_DEBUG
	  MELT_LOCATION ("warmelt-normatch.melt:3132:/ cppif.then");
	  /*^block */
	  /*anyblock */
	  {


	    {
	      /*^locexp */
	      /*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	      melt_dbgcounter++;
#endif
	      ;
	    }
	    ;
	    /*^checksignal */
	    MELT_CHECK_SIGNAL ();
	    ;
     /*_#MELT_NEED_DBG__L23*/ meltfnum[17] =
	      /*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	      ( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	      0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	      ;;
	    MELT_LOCATION ("warmelt-normatch.melt:3132:/ cond");
	    /*cond */ if ( /*_#MELT_NEED_DBG__L23*/ meltfnum[17])	/*then */
	      {
		/*^cond.then */
		/*^block */
		/*anyblock */
		{

       /*_#THE_MELTCALLCOUNT__L24*/ meltfnum[10] =
#ifdef meltcallcount
		    meltcallcount	/* the_meltcallcount */
#else
		    0L
#endif /* meltcallcount the_meltcallcount */
		    ;;
		  MELT_LOCATION ("warmelt-normatch.melt:3132:/ checksignal");
		  MELT_CHECK_SIGNAL ();
		  ;
		  /*^apply */
		  /*apply */
		  {
		    union meltparam_un argtab[5];
		    memset (&argtab, 0, sizeof (argtab));
		    /*^apply.arg */
		    argtab[0].meltbp_long =
		      /*_#THE_MELTCALLCOUNT__L24*/ meltfnum[10];
		    /*^apply.arg */
		    argtab[1].meltbp_cstring = "warmelt-normatch.melt";
		    /*^apply.arg */
		    argtab[2].meltbp_long = 3132;
		    /*^apply.arg */
		    argtab[3].meltbp_cstring = "putthen_matchthen did mythen";
		    /*^apply.arg */
		    argtab[4].meltbp_aptr =
		      (melt_ptr_t *) & /*_.MYTHEN__V21*/ meltfptr[20];
		    /*_.MELT_DEBUG_FUN__V54*/ meltfptr[53] =
		      melt_apply ((meltclosure_ptr_t)
				  (( /*!MELT_DEBUG_FUN */ meltfrout->
				    tabval[0])),
				  (melt_ptr_t) (( /*nil */ NULL)),
				  (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_PTR ""), argtab, "",
				  (union meltparam_un *) 0);
		  }
		  ;
		  /*_.IF___V53*/ meltfptr[52] =
		    /*_.MELT_DEBUG_FUN__V54*/ meltfptr[53];;
		  /*epilog */

		  MELT_LOCATION ("warmelt-normatch.melt:3132:/ clear");
		 /*clear *//*_#THE_MELTCALLCOUNT__L24*/ meltfnum[10] = 0;
		  /*^clear */
		 /*clear *//*_.MELT_DEBUG_FUN__V54*/ meltfptr[53] = 0;
		}
		;
	      }
	    else
	      {			/*^cond.else */

      /*_.IF___V53*/ meltfptr[52] = NULL;;
	      }
	    ;
	    MELT_LOCATION ("warmelt-normatch.melt:3132:/ quasiblock");


	    /*_.PROGN___V55*/ meltfptr[53] = /*_.IF___V53*/ meltfptr[52];;
	    /*^compute */
	    /*_.IFCPP___V52*/ meltfptr[48] = /*_.PROGN___V55*/ meltfptr[53];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-normatch.melt:3132:/ clear");
	       /*clear *//*_#MELT_NEED_DBG__L23*/ meltfnum[17] = 0;
	    /*^clear */
	       /*clear *//*_.IF___V53*/ meltfptr[52] = 0;
	    /*^clear */
	       /*clear *//*_.PROGN___V55*/ meltfptr[53] = 0;
	  }

#else /*MELT_HAVE_DEBUG */
	  /*^cppif.else */
	  /*_.IFCPP___V52*/ meltfptr[48] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
	  ;
	  MELT_LOCATION ("warmelt-normatch.melt:3128:/ quasiblock");


	  /*_.PROGN___V56*/ meltfptr[52] = /*_.IFCPP___V52*/ meltfptr[48];;
	  /*^compute */
	  /*_.IFELSE___V37*/ meltfptr[24] = /*_.PROGN___V56*/ meltfptr[52];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-normatch.melt:3122:/ clear");
	     /*clear *//*_.IFCPP___V43*/ meltfptr[39] = 0;
	  /*^clear */
	     /*clear *//*_.IFCPP___V47*/ meltfptr[25] = 0;
	  /*^clear */
	     /*clear *//*_.PUT_THEN_MATCH__V51*/ meltfptr[32] = 0;
	  /*^clear */
	     /*clear *//*_.IFCPP___V52*/ meltfptr[48] = 0;
	  /*^clear */
	     /*clear *//*_.PROGN___V56*/ meltfptr[52] = 0;
	}
	;
      }
    ;
    /*_.LET___V20*/ meltfptr[18] = /*_.IFELSE___V37*/ meltfptr[24];;

    MELT_LOCATION ("warmelt-normatch.melt:3111:/ clear");
	   /*clear *//*_.MYTHEN__V21*/ meltfptr[20] = 0;
    /*^clear */
	   /*clear *//*_.STARTHEN__V22*/ meltfptr[21] = 0;
    /*^clear */
	   /*clear *//*_#__L10*/ meltfnum[1] = 0;
    /*^clear */
	   /*clear *//*_.IF___V23*/ meltfptr[22] = 0;
    /*^clear */
	   /*clear *//*_#__L13*/ meltfnum[11] = 0;
    /*^clear */
	   /*clear *//*_.IF___V30*/ meltfptr[23] = 0;
    /*^clear */
	   /*clear *//*_#NULL__L16*/ meltfnum[14] = 0;
    /*^clear */
	   /*clear *//*_.IFELSE___V37*/ meltfptr[24] = 0;
    MELT_LOCATION ("warmelt-normatch.melt:3099:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.LET___V20*/ meltfptr[18];;

    {
      MELT_LOCATION ("warmelt-normatch.melt:3099:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V4*/ meltfptr[3] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V8*/ meltfptr[4] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V10*/ meltfptr[5] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V14*/ meltfptr[10] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V16*/ meltfptr[11] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V18*/ meltfptr[16] = 0;
    /*^clear */
	   /*clear *//*_#IS_A__L9*/ meltfnum[0] = 0;
    /*^clear */
	   /*clear *//*_.LET___V20*/ meltfptr[18] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("PUTTHEN_MATCHTHEN", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_67_warmelt_normatch_PUTTHEN_MATCHTHEN_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_67_warmelt_normatch_PUTTHEN_MATCHTHEN */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_68_warmelt_normatch_PUTTHEN_MATCHGROUP (meltclosure_ptr_t meltclosp_,
						 melt_ptr_t meltfirstargp_,
						 const melt_argdescr_cell_t
						 meltxargdescr_[],
						 union meltparam_un *
						 meltxargtab_,
						 const melt_argdescr_cell_t
						 meltxresdescr_[],
						 union meltparam_un *
						 meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_68_warmelt_normatch_PUTTHEN_MATCHGROUP_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_68_warmelt_normatch_PUTTHEN_MATCHGROUP_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 36
    melt_ptr_t mcfr_varptr[36];
#define MELTFRAM_NBVARNUM 13
    long mcfr_varnum[13];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_68_warmelt_normatch_PUTTHEN_MATCHGROUP is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_68_warmelt_normatch_PUTTHEN_MATCHGROUP_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 36; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_68_warmelt_normatch_PUTTHEN_MATCHGROUP nbval 36*/
  meltfram__.mcfr_nbvar = 36 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("PUTTHEN_MATCHGROUP", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-normatch.melt:3138:/ getarg");
 /*_.RECV__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.THENSTEP__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.THENSTEP__V3*/ meltfptr[2])) !=
	      NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-normatch.melt:3139:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L1*/ meltfnum[0] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-normatch.melt:3139:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L1*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-normatch.melt:3139:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-normatch.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 3139;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "putthen_matchgroup recv";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.RECV__V2*/ meltfptr[1];
	      /*_.MELT_DEBUG_FUN__V6*/ meltfptr[5] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V5*/ meltfptr[4] = /*_.MELT_DEBUG_FUN__V6*/ meltfptr[5];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-normatch.melt:3139:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V6*/ meltfptr[5] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V5*/ meltfptr[4] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-normatch.melt:3139:/ quasiblock");


      /*_.PROGN___V7*/ meltfptr[5] = /*_.IF___V5*/ meltfptr[4];;
      /*^compute */
      /*_.IFCPP___V4*/ meltfptr[3] = /*_.PROGN___V7*/ meltfptr[5];;
      /*epilog */

      MELT_LOCATION ("warmelt-normatch.melt:3139:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L1*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IF___V5*/ meltfptr[4] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V7*/ meltfptr[5] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V4*/ meltfptr[3] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-normatch.melt:3140:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L3*/ meltfnum[1] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.RECV__V2*/ meltfptr[1]),
			     (melt_ptr_t) (( /*!CLASS_MATCH_STEP_TEST_GROUP */
					    meltfrout->tabval[1])));;
      MELT_LOCATION ("warmelt-normatch.melt:3140:/ cond");
      /*cond */ if ( /*_#IS_A__L3*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V9*/ meltfptr[5] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-normatch.melt:3140:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check recv"),
				  ("warmelt-normatch.melt")
				  ? ("warmelt-normatch.melt") : __FILE__,
				  (3140) ? (3140) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V9*/ meltfptr[5] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V8*/ meltfptr[4] = /*_.IFELSE___V9*/ meltfptr[5];;
      /*epilog */

      MELT_LOCATION ("warmelt-normatch.melt:3140:/ clear");
	     /*clear *//*_#IS_A__L3*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V9*/ meltfptr[5] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V8*/ meltfptr[4] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-normatch.melt:3141:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L4*/ meltfnum[0] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-normatch.melt:3141:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L4*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L5*/ meltfnum[1] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-normatch.melt:3141:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L5*/ meltfnum[1];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-normatch.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 3141;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "putthen_matchgroup thenstep";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.THENSTEP__V3*/ meltfptr[2];
	      /*_.MELT_DEBUG_FUN__V12*/ meltfptr[11] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V11*/ meltfptr[10] =
	      /*_.MELT_DEBUG_FUN__V12*/ meltfptr[11];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-normatch.melt:3141:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L5*/ meltfnum[1] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V12*/ meltfptr[11] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V11*/ meltfptr[10] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-normatch.melt:3141:/ quasiblock");


      /*_.PROGN___V13*/ meltfptr[11] = /*_.IF___V11*/ meltfptr[10];;
      /*^compute */
      /*_.IFCPP___V10*/ meltfptr[5] = /*_.PROGN___V13*/ meltfptr[11];;
      /*epilog */

      MELT_LOCATION ("warmelt-normatch.melt:3141:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L4*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IF___V11*/ meltfptr[10] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V13*/ meltfptr[11] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V10*/ meltfptr[5] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-normatch.melt:3142:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L6*/ meltfnum[1] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.THENSTEP__V3*/ meltfptr[2]),
			     (melt_ptr_t) (( /*!CLASS_MATCH_STEP */
					    meltfrout->tabval[2])));;
      MELT_LOCATION ("warmelt-normatch.melt:3142:/ cond");
      /*cond */ if ( /*_#IS_A__L6*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V15*/ meltfptr[11] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-normatch.melt:3142:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check thenstep"),
				  ("warmelt-normatch.melt")
				  ? ("warmelt-normatch.melt") : __FILE__,
				  (3142) ? (3142) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V15*/ meltfptr[11] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V14*/ meltfptr[10] = /*_.IFELSE___V15*/ meltfptr[11];;
      /*epilog */

      MELT_LOCATION ("warmelt-normatch.melt:3142:/ clear");
	     /*clear *//*_#IS_A__L6*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V15*/ meltfptr[11] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V14*/ meltfptr[10] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-normatch.melt:3143:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.THENSTEP__V3*/ meltfptr[2];
      /*_.PUTTHEN_MATCHTHEN__V16*/ meltfptr[11] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!PUTTHEN_MATCHTHEN */ meltfrout->tabval[3])),
		    (melt_ptr_t) ( /*_.RECV__V2*/ meltfptr[1]),
		    (MELTBPARSTR_PTR ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-normatch.melt:3144:/ quasiblock");


    /*^getslot */
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.RECV__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 7, "MSTGROUP_THEN");
  /*_.THENGROUP__V18*/ meltfptr[17] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-normatch.melt:3146:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#NULL__L7*/ meltfnum[0] =
      (( /*_.THENGROUP__V18*/ meltfptr[17]) == NULL);;
    MELT_LOCATION ("warmelt-normatch.melt:3146:/ cond");
    /*cond */ if ( /*_#NULL__L7*/ meltfnum[0])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-normatch.melt:3147:/ quasiblock");


   /*_.RETVAL___V1*/ meltfptr[0] = NULL;;

	  {
	    MELT_LOCATION ("warmelt-normatch.melt:3147:/ locexp");
	    /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
	    if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	      melt_warn_for_no_expected_secondary_results ();
	    /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
	    ;
	  }
	  ;
	  /*^finalreturn */
	  ;
	  /*finalret */ goto labend_rout;
	  /*_.IFELSE___V19*/ meltfptr[18] = /*_.RETURN___V20*/ meltfptr[19];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-normatch.melt:3146:/ clear");
	     /*clear *//*_.RETURN___V20*/ meltfptr[19] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-normatch.melt:3148:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
   /*_#IS_A__L8*/ meltfnum[1] =
	    melt_is_instance_of ((melt_ptr_t)
				 ( /*_.THENGROUP__V18*/ meltfptr[17]),
				 (melt_ptr_t) (( /*!CLASS_MATCH_STEP */
						meltfrout->tabval[2])));;
	  MELT_LOCATION ("warmelt-normatch.melt:3148:/ cond");
	  /*cond */ if ( /*_#IS_A__L8*/ meltfnum[1])	/*then */
	    {
	      /*^cond.then */
	      /*^block */
	      /*anyblock */
	      {

		MELT_LOCATION ("warmelt-normatch.melt:3149:/ checksignal");
		MELT_CHECK_SIGNAL ();
		;
		/*^msend */
		/*msend */
		{
		  union meltparam_un argtab[1];
		  memset (&argtab, 0, sizeof (argtab));
		  /*^ojbmsend.arg */
		  argtab[0].meltbp_aptr =
		    (melt_ptr_t *) & /*_.THENSTEP__V3*/ meltfptr[2];
		  /*_.PUT_THEN_MATCH__V22*/ meltfptr[21] =
		    meltgc_send ((melt_ptr_t)
				 ( /*_.THENGROUP__V18*/ meltfptr[17]),
				 (melt_ptr_t) (( /*!PUT_THEN_MATCH */
						meltfrout->tabval[4])),
				 (MELTBPARSTR_PTR ""), argtab, "",
				 (union meltparam_un *) 0);
		}
		;
		/*_.IFELSE___V21*/ meltfptr[19] =
		  /*_.PUT_THEN_MATCH__V22*/ meltfptr[21];;
		/*epilog */

		MELT_LOCATION ("warmelt-normatch.melt:3148:/ clear");
	       /*clear *//*_.PUT_THEN_MATCH__V22*/ meltfptr[21] = 0;
	      }
	      ;
	    }
	  else
	    {			/*^cond.else */

	      /*^block */
	      /*anyblock */
	      {

		MELT_LOCATION ("warmelt-normatch.melt:3150:/ checksignal");
		MELT_CHECK_SIGNAL ();
		;
     /*_#IS_LIST__L9*/ meltfnum[8] =
		  (melt_magic_discr
		   ((melt_ptr_t) ( /*_.THENGROUP__V18*/ meltfptr[17])) ==
		   MELTOBMAG_LIST);;
		MELT_LOCATION ("warmelt-normatch.melt:3150:/ cond");
		/*cond */ if ( /*_#IS_LIST__L9*/ meltfnum[8])	/*then */
		  {
		    /*^cond.then */
		    /*^block */
		    /*anyblock */
		    {

		      /*citerblock FOREACH_IN_LIST */
		      {
			/* start foreach_in_list meltcit1__EACHLIST */
			for ( /*_.CURPAIR__V24*/ meltfptr[23] =
			     melt_list_first ((melt_ptr_t)
					      /*_.THENGROUP__V18*/
					      meltfptr[17]);
			     melt_magic_discr ((melt_ptr_t) /*_.CURPAIR__V24*/
					       meltfptr[23]) ==
			     MELTOBMAG_PAIR;
			     /*_.CURPAIR__V24*/ meltfptr[23] =
			     melt_pair_tail ((melt_ptr_t) /*_.CURPAIR__V24*/
					     meltfptr[23]))
			  {
			    /*_.CURTHEN__V25*/ meltfptr[24] =
			      melt_pair_head ((melt_ptr_t) /*_.CURPAIR__V24*/
					      meltfptr[23]);


			    MELT_LOCATION
			      ("warmelt-normatch.melt:3154:/ checksignal");
			    MELT_CHECK_SIGNAL ();
			    ;
			    /*^msend */
			    /*msend */
			    {
			      union meltparam_un argtab[1];
			      memset (&argtab, 0, sizeof (argtab));
			      /*^ojbmsend.arg */
			      argtab[0].meltbp_aptr =
				(melt_ptr_t *) & /*_.THENSTEP__V3*/
				meltfptr[2];
			      /*_.PUT_THEN_MATCH__V26*/ meltfptr[25] =
				meltgc_send ((melt_ptr_t)
					     ( /*_.CURTHEN__V25*/
					      meltfptr[24]),
					     (melt_ptr_t) (( /*!PUT_THEN_MATCH */ meltfrout->tabval[4])), (MELTBPARSTR_PTR ""), argtab, "", (union meltparam_un *) 0);
			    }
			    ;
			    /*_.IFELSE___V23*/ meltfptr[21] =
			      /*_.PUT_THEN_MATCH__V26*/ meltfptr[25];;
			  }	/* end foreach_in_list meltcit1__EACHLIST */
     /*_.CURPAIR__V24*/ meltfptr[23] = NULL;
     /*_.CURTHEN__V25*/ meltfptr[24] = NULL;


			/*citerepilog */

			MELT_LOCATION ("warmelt-normatch.melt:3151:/ clear");
		  /*clear *//*_.CURPAIR__V24*/ meltfptr[23] = 0;
			/*^clear */
		  /*clear *//*_.CURTHEN__V25*/ meltfptr[24] = 0;
			/*^clear */
		  /*clear *//*_.PUT_THEN_MATCH__V26*/ meltfptr[25] = 0;
		      }		/*endciterblock FOREACH_IN_LIST */
		      ;
		      /*epilog */
		    }
		    ;
		  }
		else
		  {
		    MELT_LOCATION ("warmelt-normatch.melt:3150:/ cond.else");

		    /*^block */
		    /*anyblock */
		    {

		      MELT_LOCATION
			("warmelt-normatch.melt:3155:/ checksignal");
		      MELT_CHECK_SIGNAL ();
		      ;
       /*_#IS_MULTIPLE__L10*/ meltfnum[9] =
			(melt_magic_discr
			 ((melt_ptr_t) ( /*_.THENGROUP__V18*/ meltfptr[17]))
			 == MELTOBMAG_MULTIPLE);;
		      MELT_LOCATION ("warmelt-normatch.melt:3155:/ cond");
		      /*cond */ if ( /*_#IS_MULTIPLE__L10*/ meltfnum[9])	/*then */
			{
			  /*^cond.then */
			  /*^block */
			  /*anyblock */
			  {

			    /*citerblock FOREACH_IN_MULTIPLE */
			    {
			      /* start foreach_in_multiple meltcit2__EACHTUP */
			      long meltcit2__EACHTUP_ln =
				melt_multiple_length ((melt_ptr_t)
						      /*_.THENGROUP__V18*/
						      meltfptr[17]);
			      for ( /*_#THIX__L11*/ meltfnum[10] = 0;
				   ( /*_#THIX__L11*/ meltfnum[10] >= 0)
				   && ( /*_#THIX__L11*/ meltfnum[10] <
				       meltcit2__EACHTUP_ln);
	/*_#THIX__L11*/ meltfnum[10]++)
				{
				  /*_.CURTHEN__V28*/ meltfptr[27] =
				    melt_multiple_nth ((melt_ptr_t)
						       ( /*_.THENGROUP__V18*/
							meltfptr[17]),
						       /*_#THIX__L11*/
						       meltfnum[10]);



				  MELT_LOCATION
				    ("warmelt-normatch.melt:3159:/ checksignal");
				  MELT_CHECK_SIGNAL ();
				  ;
				  /*^msend */
				  /*msend */
				  {
				    union meltparam_un argtab[1];
				    memset (&argtab, 0, sizeof (argtab));
				    /*^ojbmsend.arg */
				    argtab[0].meltbp_aptr =
				      (melt_ptr_t *) & /*_.THENSTEP__V3*/
				      meltfptr[2];
				    /*_.PUT_THEN_MATCH__V29*/ meltfptr[28] =
				      meltgc_send ((melt_ptr_t)
						   ( /*_.CURTHEN__V28*/
						    meltfptr[27]),
						   (melt_ptr_t) (( /*!PUT_THEN_MATCH */ meltfrout->tabval[4])), (MELTBPARSTR_PTR ""), argtab, "", (union meltparam_un *) 0);
				  }
				  ;
				  /*_.IFELSE___V27*/ meltfptr[26] =
				    /*_.PUT_THEN_MATCH__V29*/ meltfptr[28];;
				  if ( /*_#THIX__L11*/ meltfnum[10] < 0)
				    break;
				}	/* end  foreach_in_multiple meltcit2__EACHTUP */

			      /*citerepilog */

			      MELT_LOCATION
				("warmelt-normatch.melt:3156:/ clear");
		    /*clear *//*_.CURTHEN__V28*/ meltfptr[27] = 0;
			      /*^clear */
		    /*clear *//*_#THIX__L11*/ meltfnum[10] = 0;
			      /*^clear */
		    /*clear *//*_.PUT_THEN_MATCH__V29*/ meltfptr[28]
				= 0;
			    }	/*endciterblock FOREACH_IN_MULTIPLE */
			    ;
			    /*epilog */
			  }
			  ;
			}
		      else
			{
			  MELT_LOCATION
			    ("warmelt-normatch.melt:3155:/ cond.else");

			  /*^block */
			  /*anyblock */
			  {


#if MELT_HAVE_DEBUG
			    MELT_LOCATION
			      ("warmelt-normatch.melt:3161:/ cppif.then");
			    /*^block */
			    /*anyblock */
			    {


			      {
				/*^locexp */
				/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
				melt_dbgcounter++;
#endif
				;
			      }
			      ;
			      /*^checksignal */
			      MELT_CHECK_SIGNAL ();
			      ;
	   /*_#MELT_NEED_DBG__L12*/ meltfnum[11] =
				/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
				( /*melt_need_dbg */
				 melt_need_debug ((int) 0))
#else
				0	/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
				;;
			      MELT_LOCATION
				("warmelt-normatch.melt:3161:/ cond");
			      /*cond */ if ( /*_#MELT_NEED_DBG__L12*/ meltfnum[11])	/*then */
				{
				  /*^cond.then */
				  /*^block */
				  /*anyblock */
				  {

	     /*_#THE_MELTCALLCOUNT__L13*/ meltfnum[12]
				      =
#ifdef meltcallcount
				      meltcallcount	/* the_meltcallcount */
#else
				      0L
#endif /* meltcallcount the_meltcallcount */
				      ;;
				    MELT_LOCATION
				      ("warmelt-normatch.melt:3161:/ checksignal");
				    MELT_CHECK_SIGNAL ();
				    ;
				    /*^apply */
				    /*apply */
				    {
				      union meltparam_un argtab[5];
				      memset (&argtab, 0, sizeof (argtab));
				      /*^apply.arg */
				      argtab[0].meltbp_long =
					/*_#THE_MELTCALLCOUNT__L13*/
					meltfnum[12];
				      /*^apply.arg */
				      argtab[1].meltbp_cstring =
					"warmelt-normatch.melt";
				      /*^apply.arg */
				      argtab[2].meltbp_long = 3161;
				      /*^apply.arg */
				      argtab[3].meltbp_cstring =
					"putthen_matchgroup bad thengroup";
				      /*^apply.arg */
				      argtab[4].meltbp_aptr =
					(melt_ptr_t *) & /*_.THENGROUP__V18*/
					meltfptr[17];
				      /*_.MELT_DEBUG_FUN__V32*/ meltfptr[31] =
					melt_apply ((meltclosure_ptr_t)
						    (( /*!MELT_DEBUG_FUN */
						      meltfrout->tabval[0])),
						    (melt_ptr_t) (( /*nil */
								   NULL)),
						    (MELTBPARSTR_LONG
						     MELTBPARSTR_CSTRING
						     MELTBPARSTR_LONG
						     MELTBPARSTR_CSTRING
						     MELTBPARSTR_PTR ""),
						    argtab, "",
						    (union meltparam_un *) 0);
				    }
				    ;
				    /*_.IF___V31*/ meltfptr[30] =
				      /*_.MELT_DEBUG_FUN__V32*/ meltfptr[31];;
				    /*epilog */

				    MELT_LOCATION
				      ("warmelt-normatch.melt:3161:/ clear");
		       /*clear *//*_#THE_MELTCALLCOUNT__L13*/
				      meltfnum[12] = 0;
				    /*^clear */
		       /*clear *//*_.MELT_DEBUG_FUN__V32*/
				      meltfptr[31] = 0;
				  }
				  ;
				}
			      else
				{	/*^cond.else */

	    /*_.IF___V31*/ meltfptr[30] = NULL;;
				}
			      ;
			      MELT_LOCATION
				("warmelt-normatch.melt:3161:/ quasiblock");


			      /*_.PROGN___V33*/ meltfptr[31] =
				/*_.IF___V31*/ meltfptr[30];;
			      /*^compute */
			      /*_.IFCPP___V30*/ meltfptr[29] =
				/*_.PROGN___V33*/ meltfptr[31];;
			      /*epilog */

			      MELT_LOCATION
				("warmelt-normatch.melt:3161:/ clear");
		     /*clear *//*_#MELT_NEED_DBG__L12*/ meltfnum[11]
				= 0;
			      /*^clear */
		     /*clear *//*_.IF___V31*/ meltfptr[30] = 0;
			      /*^clear */
		     /*clear *//*_.PROGN___V33*/ meltfptr[31] = 0;
			    }

#else /*MELT_HAVE_DEBUG */
			    /*^cppif.else */
			    /*_.IFCPP___V30*/ meltfptr[29] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
			    ;

#if MELT_HAVE_DEBUG
			    MELT_LOCATION
			      ("warmelt-normatch.melt:3162:/ cppif.then");
			    /*^block */
			    /*anyblock */
			    {

			      /*^checksignal */
			      MELT_CHECK_SIGNAL ();
			      ;
			      /*^cond */
			      /*cond */ if (( /*nil */ NULL))	/*then */
				{
				  /*^cond.then */
				  /*_.IFELSE___V35*/ meltfptr[31] =
				    ( /*nil */ NULL);;
				}
			      else
				{
				  MELT_LOCATION
				    ("warmelt-normatch.melt:3162:/ cond.else");

				  /*^block */
				  /*anyblock */
				  {




				    {
				      /*^locexp */
				      melt_assert_failed (("bad thengroup"),
							  ("warmelt-normatch.melt")
							  ?
							  ("warmelt-normatch.melt")
							  : __FILE__,
							  (3162) ? (3162) :
							  __LINE__,
							  __FUNCTION__);
				      ;
				    }
				    ;
		       /*clear *//*_.IFELSE___V35*/ meltfptr[31]
				      = 0;
				    /*epilog */
				  }
				  ;
				}
			      ;
			      /*^compute */
			      /*_.IFCPP___V34*/ meltfptr[30] =
				/*_.IFELSE___V35*/ meltfptr[31];;
			      /*epilog */

			      MELT_LOCATION
				("warmelt-normatch.melt:3162:/ clear");
		     /*clear *//*_.IFELSE___V35*/ meltfptr[31] = 0;
			    }

#else /*MELT_HAVE_DEBUG */
			    /*^cppif.else */
			    /*_.IFCPP___V34*/ meltfptr[30] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
			    ;
			    MELT_LOCATION
			      ("warmelt-normatch.melt:3160:/ quasiblock");


			    /*_.PROGN___V36*/ meltfptr[31] =
			      /*_.IFCPP___V34*/ meltfptr[30];;
			    /*^compute */
			    /*_.IFELSE___V27*/ meltfptr[26] =
			      /*_.PROGN___V36*/ meltfptr[31];;
			    /*epilog */

			    MELT_LOCATION
			      ("warmelt-normatch.melt:3155:/ clear");
		   /*clear *//*_.IFCPP___V30*/ meltfptr[29] = 0;
			    /*^clear */
		   /*clear *//*_.IFCPP___V34*/ meltfptr[30] = 0;
			    /*^clear */
		   /*clear *//*_.PROGN___V36*/ meltfptr[31] = 0;
			  }
			  ;
			}
		      ;
		      /*_.IFELSE___V23*/ meltfptr[21] =
			/*_.IFELSE___V27*/ meltfptr[26];;
		      /*epilog */

		      MELT_LOCATION ("warmelt-normatch.melt:3150:/ clear");
		 /*clear *//*_#IS_MULTIPLE__L10*/ meltfnum[9] = 0;
		      /*^clear */
		 /*clear *//*_.IFELSE___V27*/ meltfptr[26] = 0;
		    }
		    ;
		  }
		;
		/*_.IFELSE___V21*/ meltfptr[19] =
		  /*_.IFELSE___V23*/ meltfptr[21];;
		/*epilog */

		MELT_LOCATION ("warmelt-normatch.melt:3148:/ clear");
	       /*clear *//*_#IS_LIST__L9*/ meltfnum[8] = 0;
		/*^clear */
	       /*clear *//*_.IFELSE___V23*/ meltfptr[21] = 0;
	      }
	      ;
	    }
	  ;
	  /*_.IFELSE___V19*/ meltfptr[18] = /*_.IFELSE___V21*/ meltfptr[19];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-normatch.melt:3146:/ clear");
	     /*clear *//*_#IS_A__L8*/ meltfnum[1] = 0;
	  /*^clear */
	     /*clear *//*_.IFELSE___V21*/ meltfptr[19] = 0;
	}
	;
      }
    ;
    /*_.LET___V17*/ meltfptr[16] = /*_.IFELSE___V19*/ meltfptr[18];;

    MELT_LOCATION ("warmelt-normatch.melt:3144:/ clear");
	   /*clear *//*_.THENGROUP__V18*/ meltfptr[17] = 0;
    /*^clear */
	   /*clear *//*_#NULL__L7*/ meltfnum[0] = 0;
    /*^clear */
	   /*clear *//*_.IFELSE___V19*/ meltfptr[18] = 0;
    MELT_LOCATION ("warmelt-normatch.melt:3138:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.LET___V17*/ meltfptr[16];;

    {
      MELT_LOCATION ("warmelt-normatch.melt:3138:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V4*/ meltfptr[3] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V8*/ meltfptr[4] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V10*/ meltfptr[5] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V14*/ meltfptr[10] = 0;
    /*^clear */
	   /*clear *//*_.PUTTHEN_MATCHTHEN__V16*/ meltfptr[11] = 0;
    /*^clear */
	   /*clear *//*_.LET___V17*/ meltfptr[16] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("PUTTHEN_MATCHGROUP", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_68_warmelt_normatch_PUTTHEN_MATCHGROUP_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_68_warmelt_normatch_PUTTHEN_MATCHGROUP */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_69_warmelt_normatch_PUTELSE_MATCHANY (meltclosure_ptr_t meltclosp_,
					       melt_ptr_t meltfirstargp_,
					       const melt_argdescr_cell_t
					       meltxargdescr_[],
					       union meltparam_un *
					       meltxargtab_,
					       const melt_argdescr_cell_t
					       meltxresdescr_[],
					       union meltparam_un *
					       meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_69_warmelt_normatch_PUTELSE_MATCHANY_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_69_warmelt_normatch_PUTELSE_MATCHANY_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 12
    melt_ptr_t mcfr_varptr[12];
#define MELTFRAM_NBVARNUM 2
    long mcfr_varnum[2];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_69_warmelt_normatch_PUTELSE_MATCHANY is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_69_warmelt_normatch_PUTELSE_MATCHANY_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 12; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_69_warmelt_normatch_PUTELSE_MATCHANY nbval 12*/
  meltfram__.mcfr_nbvar = 12 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("PUTELSE_MATCHANY", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-normatch.melt:3173:/ getarg");
 /*_.RECV__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.ELSESTEP__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.ELSESTEP__V3*/ meltfptr[2])) !=
	      NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {

    MELT_LOCATION ("warmelt-normatch.melt:3174:/ quasiblock");


 /*_.DIS__V5*/ meltfptr[4] =
      ((melt_ptr_t)
       (melt_discr ((melt_ptr_t) ( /*_.RECV__V2*/ meltfptr[1]))));;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-normatch.melt:3176:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L1*/ meltfnum[0] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-normatch.melt:3176:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L1*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-normatch.melt:3176:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[9];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-normatch.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 3176;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "putelse_mathany recv=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.RECV__V2*/ meltfptr[1];
	      /*^apply.arg */
	      argtab[5].meltbp_cstring = "elsestep=";
	      /*^apply.arg */
	      argtab[6].meltbp_aptr =
		(melt_ptr_t *) & /*_.ELSESTEP__V3*/ meltfptr[2];
	      /*^apply.arg */
	      argtab[7].meltbp_cstring = " for discrim=";
	      /*^apply.arg */
	      argtab[8].meltbp_aptr =
		(melt_ptr_t *) & /*_.DIS__V5*/ meltfptr[4];
	      /*_.MELT_DEBUG_FUN__V8*/ meltfptr[7] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V7*/ meltfptr[6] = /*_.MELT_DEBUG_FUN__V8*/ meltfptr[7];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-normatch.melt:3176:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V8*/ meltfptr[7] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V7*/ meltfptr[6] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-normatch.melt:3176:/ quasiblock");


      /*_.PROGN___V9*/ meltfptr[7] = /*_.IF___V7*/ meltfptr[6];;
      /*^compute */
      /*_.IFCPP___V6*/ meltfptr[5] = /*_.PROGN___V9*/ meltfptr[7];;
      /*epilog */

      MELT_LOCATION ("warmelt-normatch.melt:3176:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L1*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IF___V7*/ meltfptr[6] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V9*/ meltfptr[7] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V6*/ meltfptr[5] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-normatch.melt:3177:/ cond");
    /*cond */ if (
		   /*ifisa */
		   melt_is_instance_of ((melt_ptr_t)
					( /*_.DIS__V5*/ meltfptr[4]),
					(melt_ptr_t) (( /*!CLASS_NAMED */
						       meltfrout->tabval[1])))
      )				/*then */
      {
	/*^cond.then */
	/*^getslot */
	{
	  melt_ptr_t slot = NULL, obj = NULL;
	  obj = (melt_ptr_t) ( /*_.DIS__V5*/ meltfptr[4]) /*=obj*/ ;
	  melt_object_get_field (slot, obj, 1, "NAMED_NAME");
   /*_.NAMED_NAME__V10*/ meltfptr[6] = slot;
	};
	;
      }
    else
      {				/*^cond.else */

  /*_.NAMED_NAME__V10*/ meltfptr[6] = NULL;;
      }
    ;

    {
      MELT_LOCATION ("warmelt-normatch.melt:3177:/ locexp");
      error ("MELT ERROR MSG [#%ld]::: %s - %s", melt_dbgcounter,
	     ("unexpected PUT_ELSE_MATCH for "),
	     melt_string_str ((melt_ptr_t)
			      ( /*_.NAMED_NAME__V10*/ meltfptr[6])));
    }
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-normatch.melt:3178:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
      /*^cond */
      /*cond */ if (( /*nil */ NULL))	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V12*/ meltfptr[11] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-normatch.melt:3178:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("@$@unexpected PUT_ELSE_MATCH [putelse_matchany]"), ("warmelt-normatch.melt") ? ("warmelt-normatch.melt") : __FILE__, (3178) ? (3178) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V12*/ meltfptr[11] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V11*/ meltfptr[7] = /*_.IFELSE___V12*/ meltfptr[11];;
      /*epilog */

      MELT_LOCATION ("warmelt-normatch.melt:3178:/ clear");
	     /*clear *//*_.IFELSE___V12*/ meltfptr[11] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V11*/ meltfptr[7] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    /*^compute */
    /*_.LET___V4*/ meltfptr[3] = /*_.IFCPP___V11*/ meltfptr[7];;

    MELT_LOCATION ("warmelt-normatch.melt:3174:/ clear");
	   /*clear *//*_.DIS__V5*/ meltfptr[4] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V6*/ meltfptr[5] = 0;
    /*^clear */
	   /*clear *//*_.NAMED_NAME__V10*/ meltfptr[6] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V11*/ meltfptr[7] = 0;
    MELT_LOCATION ("warmelt-normatch.melt:3173:/ quasiblock");


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.LET___V4*/ meltfptr[3];;

    {
      MELT_LOCATION ("warmelt-normatch.melt:3173:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.LET___V4*/ meltfptr[3] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("PUTELSE_MATCHANY", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_69_warmelt_normatch_PUTELSE_MATCHANY_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_69_warmelt_normatch_PUTELSE_MATCHANY */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_70_warmelt_normatch_PUTELSE_MATCHSTEPTHEN (meltclosure_ptr_t
						    meltclosp_,
						    melt_ptr_t meltfirstargp_,
						    const melt_argdescr_cell_t
						    meltxargdescr_[],
						    union meltparam_un *
						    meltxargtab_,
						    const melt_argdescr_cell_t
						    meltxresdescr_[],
						    union meltparam_un *
						    meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_70_warmelt_normatch_PUTELSE_MATCHSTEPTHEN_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_70_warmelt_normatch_PUTELSE_MATCHSTEPTHEN_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 64
    melt_ptr_t mcfr_varptr[64];
#define MELTFRAM_NBVARNUM 26
    long mcfr_varnum[26];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_70_warmelt_normatch_PUTELSE_MATCHSTEPTHEN is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_70_warmelt_normatch_PUTELSE_MATCHSTEPTHEN_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 64; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_70_warmelt_normatch_PUTELSE_MATCHSTEPTHEN nbval 64*/
  meltfram__.mcfr_nbvar = 64 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("PUTELSE_MATCHSTEPTHEN", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-normatch.melt:3183:/ getarg");
 /*_.RECV__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.ELSESTEP__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.ELSESTEP__V3*/ meltfptr[2])) !=
	      NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-normatch.melt:3184:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L1*/ meltfnum[0] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-normatch.melt:3184:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L1*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-normatch.melt:3184:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-normatch.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 3184;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "putelse_matchstepthen recv";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.RECV__V2*/ meltfptr[1];
	      /*_.MELT_DEBUG_FUN__V6*/ meltfptr[5] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V5*/ meltfptr[4] = /*_.MELT_DEBUG_FUN__V6*/ meltfptr[5];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-normatch.melt:3184:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V6*/ meltfptr[5] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V5*/ meltfptr[4] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-normatch.melt:3184:/ quasiblock");


      /*_.PROGN___V7*/ meltfptr[5] = /*_.IF___V5*/ meltfptr[4];;
      /*^compute */
      /*_.IFCPP___V4*/ meltfptr[3] = /*_.PROGN___V7*/ meltfptr[5];;
      /*epilog */

      MELT_LOCATION ("warmelt-normatch.melt:3184:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L1*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IF___V5*/ meltfptr[4] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V7*/ meltfptr[5] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V4*/ meltfptr[3] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-normatch.melt:3185:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L3*/ meltfnum[1] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.RECV__V2*/ meltfptr[1]),
			     (melt_ptr_t) (( /*!CLASS_MATCH_STEP_THEN */
					    meltfrout->tabval[1])));;
      MELT_LOCATION ("warmelt-normatch.melt:3185:/ cond");
      /*cond */ if ( /*_#IS_A__L3*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V9*/ meltfptr[5] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-normatch.melt:3185:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check recv"),
				  ("warmelt-normatch.melt")
				  ? ("warmelt-normatch.melt") : __FILE__,
				  (3185) ? (3185) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V9*/ meltfptr[5] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V8*/ meltfptr[4] = /*_.IFELSE___V9*/ meltfptr[5];;
      /*epilog */

      MELT_LOCATION ("warmelt-normatch.melt:3185:/ clear");
	     /*clear *//*_#IS_A__L3*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V9*/ meltfptr[5] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V8*/ meltfptr[4] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-normatch.melt:3186:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L4*/ meltfnum[0] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-normatch.melt:3186:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L4*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L5*/ meltfnum[1] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-normatch.melt:3186:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L5*/ meltfnum[1];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-normatch.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 3186;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "putelse_matchstepthen elsestep";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.ELSESTEP__V3*/ meltfptr[2];
	      /*_.MELT_DEBUG_FUN__V12*/ meltfptr[11] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V11*/ meltfptr[10] =
	      /*_.MELT_DEBUG_FUN__V12*/ meltfptr[11];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-normatch.melt:3186:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L5*/ meltfnum[1] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V12*/ meltfptr[11] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V11*/ meltfptr[10] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-normatch.melt:3186:/ quasiblock");


      /*_.PROGN___V13*/ meltfptr[11] = /*_.IF___V11*/ meltfptr[10];;
      /*^compute */
      /*_.IFCPP___V10*/ meltfptr[5] = /*_.PROGN___V13*/ meltfptr[11];;
      /*epilog */

      MELT_LOCATION ("warmelt-normatch.melt:3186:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L4*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IF___V11*/ meltfptr[10] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V13*/ meltfptr[11] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V10*/ meltfptr[5] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-normatch.melt:3187:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L6*/ meltfnum[1] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.ELSESTEP__V3*/ meltfptr[2]),
			     (melt_ptr_t) (( /*!CLASS_MATCH_STEP */
					    meltfrout->tabval[2])));;
      MELT_LOCATION ("warmelt-normatch.melt:3187:/ cond");
      /*cond */ if ( /*_#IS_A__L6*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V15*/ meltfptr[11] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-normatch.melt:3187:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check elsestep"),
				  ("warmelt-normatch.melt")
				  ? ("warmelt-normatch.melt") : __FILE__,
				  (3187) ? (3187) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V15*/ meltfptr[11] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V14*/ meltfptr[10] = /*_.IFELSE___V15*/ meltfptr[11];;
      /*epilog */

      MELT_LOCATION ("warmelt-normatch.melt:3187:/ clear");
	     /*clear *//*_#IS_A__L6*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V15*/ meltfptr[11] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V14*/ meltfptr[10] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-normatch.melt:3189:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_APPLICATION_SHALLOWER__L7*/ meltfnum[0] =
	(melt_application_depth () < 100);;
      MELT_LOCATION ("warmelt-normatch.melt:3189:/ cond");
      /*cond */ if ( /*_#MELT_APPLICATION_SHALLOWER__L7*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V17*/ meltfptr[16] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-normatch.melt:3189:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("putelse_matchstepthen check shallow100"),
				  ("warmelt-normatch.melt")
				  ? ("warmelt-normatch.melt") : __FILE__,
				  (3189) ? (3189) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V17*/ meltfptr[16] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V16*/ meltfptr[11] = /*_.IFELSE___V17*/ meltfptr[16];;
      /*epilog */

      MELT_LOCATION ("warmelt-normatch.melt:3189:/ clear");
	     /*clear *//*_#MELT_APPLICATION_SHALLOWER__L7*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V17*/ meltfptr[16] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V16*/ meltfptr[11] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-normatch.melt:3191:/ quasiblock");


    MELT_LOCATION ("warmelt-normatch.melt:3192:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.RECV__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 2, "MSTEP_THEN");
  /*_.MYTHEN__V19*/ meltfptr[18] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-normatch.melt:3193:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      /*_.ELSESTART__V20*/ meltfptr[19] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!START_STEP */ meltfrout->tabval[3])),
		    (melt_ptr_t) ( /*_.ELSESTEP__V3*/ meltfptr[2]), (""),
		    (union meltparam_un *) 0, "", (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-normatch.melt:3196:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#__L8*/ meltfnum[1] =
      (( /*_.RECV__V2*/ meltfptr[1]) ==
       ( /*_.ELSESTART__V20*/ meltfptr[19]));;
    MELT_LOCATION ("warmelt-normatch.melt:3196:/ cond");
    /*cond */ if ( /*_#__L8*/ meltfnum[1])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{


#if MELT_HAVE_DEBUG
	  MELT_LOCATION ("warmelt-normatch.melt:3197:/ cppif.then");
	  /*^block */
	  /*anyblock */
	  {


	    {
	      /*^locexp */
	      /*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	      melt_dbgcounter++;
#endif
	      ;
	    }
	    ;
	    /*^checksignal */
	    MELT_CHECK_SIGNAL ();
	    ;
     /*_#MELT_NEED_DBG__L9*/ meltfnum[0] =
	      /*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	      ( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	      0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	      ;;
	    MELT_LOCATION ("warmelt-normatch.melt:3197:/ cond");
	    /*cond */ if ( /*_#MELT_NEED_DBG__L9*/ meltfnum[0])	/*then */
	      {
		/*^cond.then */
		/*^block */
		/*anyblock */
		{

       /*_#THE_MELTCALLCOUNT__L10*/ meltfnum[9] =
#ifdef meltcallcount
		    meltcallcount	/* the_meltcallcount */
#else
		    0L
#endif /* meltcallcount the_meltcallcount */
		    ;;
		  MELT_LOCATION ("warmelt-normatch.melt:3197:/ checksignal");
		  MELT_CHECK_SIGNAL ();
		  ;
		  /*^apply */
		  /*apply */
		  {
		    union meltparam_un argtab[5];
		    memset (&argtab, 0, sizeof (argtab));
		    /*^apply.arg */
		    argtab[0].meltbp_long =
		      /*_#THE_MELTCALLCOUNT__L10*/ meltfnum[9];
		    /*^apply.arg */
		    argtab[1].meltbp_cstring = "warmelt-normatch.melt";
		    /*^apply.arg */
		    argtab[2].meltbp_long = 3197;
		    /*^apply.arg */
		    argtab[3].meltbp_cstring =
		      "putelse_matchstepthen recv same elsestart";
		    /*^apply.arg */
		    argtab[4].meltbp_aptr =
		      (melt_ptr_t *) & /*_.RECV__V2*/ meltfptr[1];
		    /*_.MELT_DEBUG_FUN__V24*/ meltfptr[23] =
		      melt_apply ((meltclosure_ptr_t)
				  (( /*!MELT_DEBUG_FUN */ meltfrout->
				    tabval[0])),
				  (melt_ptr_t) (( /*nil */ NULL)),
				  (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_PTR ""), argtab, "",
				  (union meltparam_un *) 0);
		  }
		  ;
		  /*_.IF___V23*/ meltfptr[22] =
		    /*_.MELT_DEBUG_FUN__V24*/ meltfptr[23];;
		  /*epilog */

		  MELT_LOCATION ("warmelt-normatch.melt:3197:/ clear");
		 /*clear *//*_#THE_MELTCALLCOUNT__L10*/ meltfnum[9] = 0;
		  /*^clear */
		 /*clear *//*_.MELT_DEBUG_FUN__V24*/ meltfptr[23] = 0;
		}
		;
	      }
	    else
	      {			/*^cond.else */

      /*_.IF___V23*/ meltfptr[22] = NULL;;
	      }
	    ;
	    MELT_LOCATION ("warmelt-normatch.melt:3197:/ quasiblock");


	    /*_.PROGN___V25*/ meltfptr[23] = /*_.IF___V23*/ meltfptr[22];;
	    /*^compute */
	    /*_.IFCPP___V22*/ meltfptr[21] = /*_.PROGN___V25*/ meltfptr[23];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-normatch.melt:3197:/ clear");
	       /*clear *//*_#MELT_NEED_DBG__L9*/ meltfnum[0] = 0;
	    /*^clear */
	       /*clear *//*_.IF___V23*/ meltfptr[22] = 0;
	    /*^clear */
	       /*clear *//*_.PROGN___V25*/ meltfptr[23] = 0;
	  }

#else /*MELT_HAVE_DEBUG */
	  /*^cppif.else */
	  /*_.IFCPP___V22*/ meltfptr[21] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
	  ;
	  MELT_LOCATION ("warmelt-normatch.melt:3198:/ quasiblock");


   /*_.RETVAL___V1*/ meltfptr[0] = NULL;;

	  {
	    MELT_LOCATION ("warmelt-normatch.melt:3198:/ locexp");
	    /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
	    if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	      melt_warn_for_no_expected_secondary_results ();
	    /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
	    ;
	  }
	  ;
	  /*^finalreturn */
	  ;
	  /*finalret */ goto labend_rout;
	  MELT_LOCATION ("warmelt-normatch.melt:3196:/ quasiblock");


	  /*_.PROGN___V27*/ meltfptr[23] = /*_.RETURN___V26*/ meltfptr[22];;
	  /*^compute */
	  /*_.IFELSE___V21*/ meltfptr[20] = /*_.PROGN___V27*/ meltfptr[23];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-normatch.melt:3196:/ clear");
	     /*clear *//*_.IFCPP___V22*/ meltfptr[21] = 0;
	  /*^clear */
	     /*clear *//*_.RETURN___V26*/ meltfptr[22] = 0;
	  /*^clear */
	     /*clear *//*_.PROGN___V27*/ meltfptr[23] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-normatch.melt:3199:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
   /*_#__L11*/ meltfnum[9] =
	    (( /*_.MYTHEN__V19*/ meltfptr[18]) ==
	     ( /*_.ELSESTEP__V3*/ meltfptr[2]));;
	  MELT_LOCATION ("warmelt-normatch.melt:3199:/ cond");
	  /*cond */ if ( /*_#__L11*/ meltfnum[9])	/*then */
	    {
	      /*^cond.then */
	      /*^block */
	      /*anyblock */
	      {


#if MELT_HAVE_DEBUG
		MELT_LOCATION ("warmelt-normatch.melt:3200:/ cppif.then");
		/*^block */
		/*anyblock */
		{


		  {
		    /*^locexp */
		    /*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
		    melt_dbgcounter++;
#endif
		    ;
		  }
		  ;
		  /*^checksignal */
		  MELT_CHECK_SIGNAL ();
		  ;
       /*_#MELT_NEED_DBG__L12*/ meltfnum[0] =
		    /*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
		    ( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
		    0		/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
		    ;;
		  MELT_LOCATION ("warmelt-normatch.melt:3200:/ cond");
		  /*cond */ if ( /*_#MELT_NEED_DBG__L12*/ meltfnum[0])	/*then */
		    {
		      /*^cond.then */
		      /*^block */
		      /*anyblock */
		      {

	 /*_#THE_MELTCALLCOUNT__L13*/ meltfnum[12] =
#ifdef meltcallcount
			  meltcallcount	/* the_meltcallcount */
#else
			  0L
#endif /* meltcallcount the_meltcallcount */
			  ;;
			MELT_LOCATION
			  ("warmelt-normatch.melt:3200:/ checksignal");
			MELT_CHECK_SIGNAL ();
			;
			/*^apply */
			/*apply */
			{
			  union meltparam_un argtab[5];
			  memset (&argtab, 0, sizeof (argtab));
			  /*^apply.arg */
			  argtab[0].meltbp_long =
			    /*_#THE_MELTCALLCOUNT__L13*/ meltfnum[12];
			  /*^apply.arg */
			  argtab[1].meltbp_cstring = "warmelt-normatch.melt";
			  /*^apply.arg */
			  argtab[2].meltbp_long = 3200;
			  /*^apply.arg */
			  argtab[3].meltbp_cstring =
			    "putelse_matchstepthen mythen same elsestep";
			  /*^apply.arg */
			  argtab[4].meltbp_aptr =
			    (melt_ptr_t *) & /*_.MYTHEN__V19*/ meltfptr[18];
			  /*_.MELT_DEBUG_FUN__V31*/ meltfptr[30] =
			    melt_apply ((meltclosure_ptr_t)
					(( /*!MELT_DEBUG_FUN */ meltfrout->
					  tabval[0])),
					(melt_ptr_t) (( /*nil */ NULL)),
					(MELTBPARSTR_LONG MELTBPARSTR_CSTRING
					 MELTBPARSTR_LONG MELTBPARSTR_CSTRING
					 MELTBPARSTR_PTR ""), argtab, "",
					(union meltparam_un *) 0);
			}
			;
			/*_.IF___V30*/ meltfptr[23] =
			  /*_.MELT_DEBUG_FUN__V31*/ meltfptr[30];;
			/*epilog */

			MELT_LOCATION ("warmelt-normatch.melt:3200:/ clear");
		   /*clear *//*_#THE_MELTCALLCOUNT__L13*/ meltfnum[12] =
			  0;
			/*^clear */
		   /*clear *//*_.MELT_DEBUG_FUN__V31*/ meltfptr[30] = 0;
		      }
		      ;
		    }
		  else
		    {		/*^cond.else */

	/*_.IF___V30*/ meltfptr[23] = NULL;;
		    }
		  ;
		  MELT_LOCATION ("warmelt-normatch.melt:3200:/ quasiblock");


		  /*_.PROGN___V32*/ meltfptr[30] =
		    /*_.IF___V30*/ meltfptr[23];;
		  /*^compute */
		  /*_.IFCPP___V29*/ meltfptr[22] =
		    /*_.PROGN___V32*/ meltfptr[30];;
		  /*epilog */

		  MELT_LOCATION ("warmelt-normatch.melt:3200:/ clear");
		 /*clear *//*_#MELT_NEED_DBG__L12*/ meltfnum[0] = 0;
		  /*^clear */
		 /*clear *//*_.IF___V30*/ meltfptr[23] = 0;
		  /*^clear */
		 /*clear *//*_.PROGN___V32*/ meltfptr[30] = 0;
		}

#else /*MELT_HAVE_DEBUG */
		/*^cppif.else */
		/*_.IFCPP___V29*/ meltfptr[22] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
		;
		MELT_LOCATION ("warmelt-normatch.melt:3201:/ quasiblock");


     /*_.RETVAL___V1*/ meltfptr[0] = NULL;;

		{
		  MELT_LOCATION ("warmelt-normatch.melt:3201:/ locexp");
		  /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
		  if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
		    melt_warn_for_no_expected_secondary_results ();
		  /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
		  ;
		}
		;
		/*^finalreturn */
		;
		/*finalret */ goto labend_rout;
		MELT_LOCATION ("warmelt-normatch.melt:3199:/ quasiblock");


		/*_.PROGN___V34*/ meltfptr[30] =
		  /*_.RETURN___V33*/ meltfptr[23];;
		/*^compute */
		/*_.IFELSE___V28*/ meltfptr[21] =
		  /*_.PROGN___V34*/ meltfptr[30];;
		/*epilog */

		MELT_LOCATION ("warmelt-normatch.melt:3199:/ clear");
	       /*clear *//*_.IFCPP___V29*/ meltfptr[22] = 0;
		/*^clear */
	       /*clear *//*_.RETURN___V33*/ meltfptr[23] = 0;
		/*^clear */
	       /*clear *//*_.PROGN___V34*/ meltfptr[30] = 0;
	      }
	      ;
	    }
	  else
	    {			/*^cond.else */

	      /*^block */
	      /*anyblock */
	      {

		MELT_LOCATION ("warmelt-normatch.melt:3202:/ checksignal");
		MELT_CHECK_SIGNAL ();
		;
     /*_#__L14*/ meltfnum[12] =
		  (( /*_.MYTHEN__V19*/ meltfptr[18]) ==
		   ( /*_.ELSESTART__V20*/ meltfptr[19]));;
		MELT_LOCATION ("warmelt-normatch.melt:3202:/ cond");
		/*cond */ if ( /*_#__L14*/ meltfnum[12])	/*then */
		  {
		    /*^cond.then */
		    /*^block */
		    /*anyblock */
		    {


#if MELT_HAVE_DEBUG
		      MELT_LOCATION
			("warmelt-normatch.melt:3203:/ cppif.then");
		      /*^block */
		      /*anyblock */
		      {


			{
			  /*^locexp */
			  /*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
			  melt_dbgcounter++;
#endif
			  ;
			}
			;
			/*^checksignal */
			MELT_CHECK_SIGNAL ();
			;
	 /*_#MELT_NEED_DBG__L15*/ meltfnum[0] =
			  /*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
			  ( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
			  0	/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
			  ;;
			MELT_LOCATION ("warmelt-normatch.melt:3203:/ cond");
			/*cond */ if ( /*_#MELT_NEED_DBG__L15*/ meltfnum[0])	/*then */
			  {
			    /*^cond.then */
			    /*^block */
			    /*anyblock */
			    {

	   /*_#THE_MELTCALLCOUNT__L16*/ meltfnum[15] =
#ifdef meltcallcount
				meltcallcount	/* the_meltcallcount */
#else
				0L
#endif /* meltcallcount the_meltcallcount */
				;;
			      MELT_LOCATION
				("warmelt-normatch.melt:3203:/ checksignal");
			      MELT_CHECK_SIGNAL ();
			      ;
			      /*^apply */
			      /*apply */
			      {
				union meltparam_un argtab[5];
				memset (&argtab, 0, sizeof (argtab));
				/*^apply.arg */
				argtab[0].meltbp_long =
				  /*_#THE_MELTCALLCOUNT__L16*/ meltfnum[15];
				/*^apply.arg */
				argtab[1].meltbp_cstring =
				  "warmelt-normatch.melt";
				/*^apply.arg */
				argtab[2].meltbp_long = 3203;
				/*^apply.arg */
				argtab[3].meltbp_cstring =
				  "putelse_matchstepthen mythen same elsestart";
				/*^apply.arg */
				argtab[4].meltbp_aptr =
				  (melt_ptr_t *) & /*_.MYTHEN__V19*/
				  meltfptr[18];
				/*_.MELT_DEBUG_FUN__V38*/ meltfptr[37] =
				  melt_apply ((meltclosure_ptr_t)
					      (( /*!MELT_DEBUG_FUN */
						meltfrout->tabval[0])),
					      (melt_ptr_t) (( /*nil */ NULL)),
					      (MELTBPARSTR_LONG
					       MELTBPARSTR_CSTRING
					       MELTBPARSTR_LONG
					       MELTBPARSTR_CSTRING
					       MELTBPARSTR_PTR ""), argtab,
					      "", (union meltparam_un *) 0);
			      }
			      ;
			      /*_.IF___V37*/ meltfptr[30] =
				/*_.MELT_DEBUG_FUN__V38*/ meltfptr[37];;
			      /*epilog */

			      MELT_LOCATION
				("warmelt-normatch.melt:3203:/ clear");
		     /*clear *//*_#THE_MELTCALLCOUNT__L16*/
				meltfnum[15] = 0;
			      /*^clear */
		     /*clear *//*_.MELT_DEBUG_FUN__V38*/ meltfptr[37]
				= 0;
			    }
			    ;
			  }
			else
			  {	/*^cond.else */

	  /*_.IF___V37*/ meltfptr[30] = NULL;;
			  }
			;
			MELT_LOCATION
			  ("warmelt-normatch.melt:3203:/ quasiblock");


			/*_.PROGN___V39*/ meltfptr[37] =
			  /*_.IF___V37*/ meltfptr[30];;
			/*^compute */
			/*_.IFCPP___V36*/ meltfptr[23] =
			  /*_.PROGN___V39*/ meltfptr[37];;
			/*epilog */

			MELT_LOCATION ("warmelt-normatch.melt:3203:/ clear");
		   /*clear *//*_#MELT_NEED_DBG__L15*/ meltfnum[0] = 0;
			/*^clear */
		   /*clear *//*_.IF___V37*/ meltfptr[30] = 0;
			/*^clear */
		   /*clear *//*_.PROGN___V39*/ meltfptr[37] = 0;
		      }

#else /*MELT_HAVE_DEBUG */
		      /*^cppif.else */
		      /*_.IFCPP___V36*/ meltfptr[23] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
		      ;
		      MELT_LOCATION
			("warmelt-normatch.melt:3204:/ quasiblock");


       /*_.RETVAL___V1*/ meltfptr[0] = NULL;;

		      {
			MELT_LOCATION ("warmelt-normatch.melt:3204:/ locexp");
			/*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
			if (meltxresdescr_ && meltxresdescr_[0]
			    && meltxrestab_)
			  melt_warn_for_no_expected_secondary_results ();
			/* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
			;
		      }
		      ;
		      /*^finalreturn */
		      ;
		      /*finalret */ goto labend_rout;
		      MELT_LOCATION
			("warmelt-normatch.melt:3202:/ quasiblock");


		      /*_.PROGN___V41*/ meltfptr[37] =
			/*_.RETURN___V40*/ meltfptr[30];;
		      /*^compute */
		      /*_.IFELSE___V35*/ meltfptr[22] =
			/*_.PROGN___V41*/ meltfptr[37];;
		      /*epilog */

		      MELT_LOCATION ("warmelt-normatch.melt:3202:/ clear");
		 /*clear *//*_.IFCPP___V36*/ meltfptr[23] = 0;
		      /*^clear */
		 /*clear *//*_.RETURN___V40*/ meltfptr[30] = 0;
		      /*^clear */
		 /*clear *//*_.PROGN___V41*/ meltfptr[37] = 0;
		    }
		    ;
		  }
		else
		  {		/*^cond.else */

		    /*^block */
		    /*anyblock */
		    {

		      MELT_LOCATION
			("warmelt-normatch.melt:3205:/ checksignal");
		      MELT_CHECK_SIGNAL ();
		      ;
		      /*^cond */
		      /*cond */ if ( /*_.MYTHEN__V19*/ meltfptr[18])	/*then */
			{
			  /*^cond.then */
			  /*^block */
			  /*anyblock */
			  {


#if MELT_HAVE_DEBUG
			    MELT_LOCATION
			      ("warmelt-normatch.melt:3206:/ cppif.then");
			    /*^block */
			    /*anyblock */
			    {


			      {
				/*^locexp */
				/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
				melt_dbgcounter++;
#endif
				;
			      }
			      ;
			      /*^checksignal */
			      MELT_CHECK_SIGNAL ();
			      ;
	   /*_#MELT_NEED_DBG__L17*/ meltfnum[15] =
				/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
				( /*melt_need_dbg */
				 melt_need_debug ((int) 0))
#else
				0	/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
				;;
			      MELT_LOCATION
				("warmelt-normatch.melt:3206:/ cond");
			      /*cond */ if ( /*_#MELT_NEED_DBG__L17*/ meltfnum[15])	/*then */
				{
				  /*^cond.then */
				  /*^block */
				  /*anyblock */
				  {

	     /*_#THE_MELTCALLCOUNT__L18*/ meltfnum[0] =
#ifdef meltcallcount
				      meltcallcount	/* the_meltcallcount */
#else
				      0L
#endif /* meltcallcount the_meltcallcount */
				      ;;
				    MELT_LOCATION
				      ("warmelt-normatch.melt:3206:/ checksignal");
				    MELT_CHECK_SIGNAL ();
				    ;
				    /*^apply */
				    /*apply */
				    {
				      union meltparam_un argtab[5];
				      memset (&argtab, 0, sizeof (argtab));
				      /*^apply.arg */
				      argtab[0].meltbp_long =
					/*_#THE_MELTCALLCOUNT__L18*/
					meltfnum[0];
				      /*^apply.arg */
				      argtab[1].meltbp_cstring =
					"warmelt-normatch.melt";
				      /*^apply.arg */
				      argtab[2].meltbp_long = 3206;
				      /*^apply.arg */
				      argtab[3].meltbp_cstring =
					"putelse_matchstepthen recursing in mythen";
				      /*^apply.arg */
				      argtab[4].meltbp_aptr =
					(melt_ptr_t *) & /*_.MYTHEN__V19*/
					meltfptr[18];
				      /*_.MELT_DEBUG_FUN__V45*/ meltfptr[44] =
					melt_apply ((meltclosure_ptr_t)
						    (( /*!MELT_DEBUG_FUN */
						      meltfrout->tabval[0])),
						    (melt_ptr_t) (( /*nil */
								   NULL)),
						    (MELTBPARSTR_LONG
						     MELTBPARSTR_CSTRING
						     MELTBPARSTR_LONG
						     MELTBPARSTR_CSTRING
						     MELTBPARSTR_PTR ""),
						    argtab, "",
						    (union meltparam_un *) 0);
				    }
				    ;
				    /*_.IF___V44*/ meltfptr[37] =
				      /*_.MELT_DEBUG_FUN__V45*/ meltfptr[44];;
				    /*epilog */

				    MELT_LOCATION
				      ("warmelt-normatch.melt:3206:/ clear");
		       /*clear *//*_#THE_MELTCALLCOUNT__L18*/
				      meltfnum[0] = 0;
				    /*^clear */
		       /*clear *//*_.MELT_DEBUG_FUN__V45*/
				      meltfptr[44] = 0;
				  }
				  ;
				}
			      else
				{	/*^cond.else */

	    /*_.IF___V44*/ meltfptr[37] = NULL;;
				}
			      ;
			      MELT_LOCATION
				("warmelt-normatch.melt:3206:/ quasiblock");


			      /*_.PROGN___V46*/ meltfptr[44] =
				/*_.IF___V44*/ meltfptr[37];;
			      /*^compute */
			      /*_.IFCPP___V43*/ meltfptr[30] =
				/*_.PROGN___V46*/ meltfptr[44];;
			      /*epilog */

			      MELT_LOCATION
				("warmelt-normatch.melt:3206:/ clear");
		     /*clear *//*_#MELT_NEED_DBG__L17*/ meltfnum[15]
				= 0;
			      /*^clear */
		     /*clear *//*_.IF___V44*/ meltfptr[37] = 0;
			      /*^clear */
		     /*clear *//*_.PROGN___V46*/ meltfptr[44] = 0;
			    }

#else /*MELT_HAVE_DEBUG */
			    /*^cppif.else */
			    /*_.IFCPP___V43*/ meltfptr[30] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
			    ;

#if MELT_HAVE_DEBUG
			    MELT_LOCATION
			      ("warmelt-normatch.melt:3207:/ cppif.then");
			    /*^block */
			    /*anyblock */
			    {


			      {
				/*^locexp */
				/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
				melt_dbgcounter++;
#endif
				;
			      }
			      ;
			      /*^checksignal */
			      MELT_CHECK_SIGNAL ();
			      ;
	   /*_#MELT_NEED_DBG__L19*/ meltfnum[0] =
				/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
				( /*melt_need_dbg */
				 melt_need_debug ((int) 0))
#else
				0	/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
				;;
			      MELT_LOCATION
				("warmelt-normatch.melt:3207:/ cond");
			      /*cond */ if ( /*_#MELT_NEED_DBG__L19*/ meltfnum[0])	/*then */
				{
				  /*^cond.then */
				  /*^block */
				  /*anyblock */
				  {

	     /*_#THE_MELTCALLCOUNT__L20*/ meltfnum[15]
				      =
#ifdef meltcallcount
				      meltcallcount	/* the_meltcallcount */
#else
				      0L
#endif /* meltcallcount the_meltcallcount */
				      ;;
				    MELT_LOCATION
				      ("warmelt-normatch.melt:3207:/ checksignal");
				    MELT_CHECK_SIGNAL ();
				    ;
				    /*^apply */
				    /*apply */
				    {
				      union meltparam_un argtab[5];
				      memset (&argtab, 0, sizeof (argtab));
				      /*^apply.arg */
				      argtab[0].meltbp_long =
					/*_#THE_MELTCALLCOUNT__L20*/
					meltfnum[15];
				      /*^apply.arg */
				      argtab[1].meltbp_cstring =
					"warmelt-normatch.melt";
				      /*^apply.arg */
				      argtab[2].meltbp_long = 3207;
				      /*^apply.arg */
				      argtab[3].meltbp_cstring =
					"putelse_matchstepthen recursing for elsestart";
				      /*^apply.arg */
				      argtab[4].meltbp_aptr =
					(melt_ptr_t *) & /*_.ELSESTART__V20*/
					meltfptr[19];
				      /*_.MELT_DEBUG_FUN__V49*/ meltfptr[48] =
					melt_apply ((meltclosure_ptr_t)
						    (( /*!MELT_DEBUG_FUN */
						      meltfrout->tabval[0])),
						    (melt_ptr_t) (( /*nil */
								   NULL)),
						    (MELTBPARSTR_LONG
						     MELTBPARSTR_CSTRING
						     MELTBPARSTR_LONG
						     MELTBPARSTR_CSTRING
						     MELTBPARSTR_PTR ""),
						    argtab, "",
						    (union meltparam_un *) 0);
				    }
				    ;
				    /*_.IF___V48*/ meltfptr[44] =
				      /*_.MELT_DEBUG_FUN__V49*/ meltfptr[48];;
				    /*epilog */

				    MELT_LOCATION
				      ("warmelt-normatch.melt:3207:/ clear");
		       /*clear *//*_#THE_MELTCALLCOUNT__L20*/
				      meltfnum[15] = 0;
				    /*^clear */
		       /*clear *//*_.MELT_DEBUG_FUN__V49*/
				      meltfptr[48] = 0;
				  }
				  ;
				}
			      else
				{	/*^cond.else */

	    /*_.IF___V48*/ meltfptr[44] = NULL;;
				}
			      ;
			      MELT_LOCATION
				("warmelt-normatch.melt:3207:/ quasiblock");


			      /*_.PROGN___V50*/ meltfptr[48] =
				/*_.IF___V48*/ meltfptr[44];;
			      /*^compute */
			      /*_.IFCPP___V47*/ meltfptr[37] =
				/*_.PROGN___V50*/ meltfptr[48];;
			      /*epilog */

			      MELT_LOCATION
				("warmelt-normatch.melt:3207:/ clear");
		     /*clear *//*_#MELT_NEED_DBG__L19*/ meltfnum[0] =
				0;
			      /*^clear */
		     /*clear *//*_.IF___V48*/ meltfptr[44] = 0;
			      /*^clear */
		     /*clear *//*_.PROGN___V50*/ meltfptr[48] = 0;
			    }

#else /*MELT_HAVE_DEBUG */
			    /*^cppif.else */
			    /*_.IFCPP___V47*/ meltfptr[37] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
			    ;

#if MELT_HAVE_DEBUG
			    MELT_LOCATION
			      ("warmelt-normatch.melt:3208:/ cppif.then");
			    /*^block */
			    /*anyblock */
			    {


			      {
				/*^locexp */
				/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
				melt_dbgcounter++;
#endif
				;
			      }
			      ;
			      /*^checksignal */
			      MELT_CHECK_SIGNAL ();
			      ;
	   /*_#MELT_NEED_DBG__L21*/ meltfnum[15] =
				/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
				( /*melt_need_dbg */
				 melt_need_debug ((int) 0))
#else
				0	/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
				;;
			      MELT_LOCATION
				("warmelt-normatch.melt:3208:/ cond");
			      /*cond */ if ( /*_#MELT_NEED_DBG__L21*/ meltfnum[15])	/*then */
				{
				  /*^cond.then */
				  /*^block */
				  /*anyblock */
				  {

	     /*_#THE_MELTCALLCOUNT__L22*/ meltfnum[0] =
#ifdef meltcallcount
				      meltcallcount	/* the_meltcallcount */
#else
				      0L
#endif /* meltcallcount the_meltcallcount */
				      ;;
				    MELT_LOCATION
				      ("warmelt-normatch.melt:3208:/ checksignal");
				    MELT_CHECK_SIGNAL ();
				    ;
				    /*^apply */
				    /*apply */
				    {
				      union meltparam_un argtab[5];
				      memset (&argtab, 0, sizeof (argtab));
				      /*^apply.arg */
				      argtab[0].meltbp_long =
					/*_#THE_MELTCALLCOUNT__L22*/
					meltfnum[0];
				      /*^apply.arg */
				      argtab[1].meltbp_cstring =
					"warmelt-normatch.melt";
				      /*^apply.arg */
				      argtab[2].meltbp_long = 3208;
				      /*^apply.arg */
				      argtab[3].meltbp_cstring =
					"putelse_matchstepthen recursing from recv";
				      /*^apply.arg */
				      argtab[4].meltbp_aptr =
					(melt_ptr_t *) & /*_.RECV__V2*/
					meltfptr[1];
				      /*_.MELT_DEBUG_FUN__V53*/ meltfptr[52] =
					melt_apply ((meltclosure_ptr_t)
						    (( /*!MELT_DEBUG_FUN */
						      meltfrout->tabval[0])),
						    (melt_ptr_t) (( /*nil */
								   NULL)),
						    (MELTBPARSTR_LONG
						     MELTBPARSTR_CSTRING
						     MELTBPARSTR_LONG
						     MELTBPARSTR_CSTRING
						     MELTBPARSTR_PTR ""),
						    argtab, "",
						    (union meltparam_un *) 0);
				    }
				    ;
				    /*_.IF___V52*/ meltfptr[48] =
				      /*_.MELT_DEBUG_FUN__V53*/ meltfptr[52];;
				    /*epilog */

				    MELT_LOCATION
				      ("warmelt-normatch.melt:3208:/ clear");
		       /*clear *//*_#THE_MELTCALLCOUNT__L22*/
				      meltfnum[0] = 0;
				    /*^clear */
		       /*clear *//*_.MELT_DEBUG_FUN__V53*/
				      meltfptr[52] = 0;
				  }
				  ;
				}
			      else
				{	/*^cond.else */

	    /*_.IF___V52*/ meltfptr[48] = NULL;;
				}
			      ;
			      MELT_LOCATION
				("warmelt-normatch.melt:3208:/ quasiblock");


			      /*_.PROGN___V54*/ meltfptr[52] =
				/*_.IF___V52*/ meltfptr[48];;
			      /*^compute */
			      /*_.IFCPP___V51*/ meltfptr[44] =
				/*_.PROGN___V54*/ meltfptr[52];;
			      /*epilog */

			      MELT_LOCATION
				("warmelt-normatch.melt:3208:/ clear");
		     /*clear *//*_#MELT_NEED_DBG__L21*/ meltfnum[15]
				= 0;
			      /*^clear */
		     /*clear *//*_.IF___V52*/ meltfptr[48] = 0;
			      /*^clear */
		     /*clear *//*_.PROGN___V54*/ meltfptr[52] = 0;
			    }

#else /*MELT_HAVE_DEBUG */
			    /*^cppif.else */
			    /*_.IFCPP___V51*/ meltfptr[44] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
			    ;
			    MELT_LOCATION
			      ("warmelt-normatch.melt:3209:/ checksignal");
			    MELT_CHECK_SIGNAL ();
			    ;
			    /*^msend */
			    /*msend */
			    {
			      union meltparam_un argtab[1];
			      memset (&argtab, 0, sizeof (argtab));
			      /*^ojbmsend.arg */
			      argtab[0].meltbp_aptr =
				(melt_ptr_t *) & /*_.ELSESTART__V20*/
				meltfptr[19];
			      /*_.PUT_ELSE_MATCH__V55*/ meltfptr[48] =
				meltgc_send ((melt_ptr_t)
					     ( /*_.MYTHEN__V19*/
					      meltfptr[18]),
					     (melt_ptr_t) (( /*!PUT_ELSE_MATCH */ meltfrout->tabval[4])), (MELTBPARSTR_PTR ""), argtab, "", (union meltparam_un *) 0);
			    }
			    ;

#if MELT_HAVE_DEBUG
			    MELT_LOCATION
			      ("warmelt-normatch.melt:3210:/ cppif.then");
			    /*^block */
			    /*anyblock */
			    {


			      {
				/*^locexp */
				/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
				melt_dbgcounter++;
#endif
				;
			      }
			      ;
			      /*^checksignal */
			      MELT_CHECK_SIGNAL ();
			      ;
	   /*_#MELT_NEED_DBG__L23*/ meltfnum[0] =
				/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
				( /*melt_need_dbg */
				 melt_need_debug ((int) 0))
#else
				0	/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
				;;
			      MELT_LOCATION
				("warmelt-normatch.melt:3210:/ cond");
			      /*cond */ if ( /*_#MELT_NEED_DBG__L23*/ meltfnum[0])	/*then */
				{
				  /*^cond.then */
				  /*^block */
				  /*anyblock */
				  {

	     /*_#THE_MELTCALLCOUNT__L24*/ meltfnum[15]
				      =
#ifdef meltcallcount
				      meltcallcount	/* the_meltcallcount */
#else
				      0L
#endif /* meltcallcount the_meltcallcount */
				      ;;
				    MELT_LOCATION
				      ("warmelt-normatch.melt:3210:/ checksignal");
				    MELT_CHECK_SIGNAL ();
				    ;
				    /*^apply */
				    /*apply */
				    {
				      union meltparam_un argtab[5];
				      memset (&argtab, 0, sizeof (argtab));
				      /*^apply.arg */
				      argtab[0].meltbp_long =
					/*_#THE_MELTCALLCOUNT__L24*/
					meltfnum[15];
				      /*^apply.arg */
				      argtab[1].meltbp_cstring =
					"warmelt-normatch.melt";
				      /*^apply.arg */
				      argtab[2].meltbp_long = 3210;
				      /*^apply.arg */
				      argtab[3].meltbp_cstring =
					"putelse_matchstepthen done recursing in mythen";
				      /*^apply.arg */
				      argtab[4].meltbp_aptr =
					(melt_ptr_t *) & /*_.MYTHEN__V19*/
					meltfptr[18];
				      /*_.MELT_DEBUG_FUN__V58*/ meltfptr[57] =
					melt_apply ((meltclosure_ptr_t)
						    (( /*!MELT_DEBUG_FUN */
						      meltfrout->tabval[0])),
						    (melt_ptr_t) (( /*nil */
								   NULL)),
						    (MELTBPARSTR_LONG
						     MELTBPARSTR_CSTRING
						     MELTBPARSTR_LONG
						     MELTBPARSTR_CSTRING
						     MELTBPARSTR_PTR ""),
						    argtab, "",
						    (union meltparam_un *) 0);
				    }
				    ;
				    /*_.IF___V57*/ meltfptr[56] =
				      /*_.MELT_DEBUG_FUN__V58*/ meltfptr[57];;
				    /*epilog */

				    MELT_LOCATION
				      ("warmelt-normatch.melt:3210:/ clear");
		       /*clear *//*_#THE_MELTCALLCOUNT__L24*/
				      meltfnum[15] = 0;
				    /*^clear */
		       /*clear *//*_.MELT_DEBUG_FUN__V58*/
				      meltfptr[57] = 0;
				  }
				  ;
				}
			      else
				{	/*^cond.else */

	    /*_.IF___V57*/ meltfptr[56] = NULL;;
				}
			      ;
			      MELT_LOCATION
				("warmelt-normatch.melt:3210:/ quasiblock");


			      /*_.PROGN___V59*/ meltfptr[57] =
				/*_.IF___V57*/ meltfptr[56];;
			      /*^compute */
			      /*_.IFCPP___V56*/ meltfptr[52] =
				/*_.PROGN___V59*/ meltfptr[57];;
			      /*epilog */

			      MELT_LOCATION
				("warmelt-normatch.melt:3210:/ clear");
		     /*clear *//*_#MELT_NEED_DBG__L23*/ meltfnum[0] =
				0;
			      /*^clear */
		     /*clear *//*_.IF___V57*/ meltfptr[56] = 0;
			      /*^clear */
		     /*clear *//*_.PROGN___V59*/ meltfptr[57] = 0;
			    }

#else /*MELT_HAVE_DEBUG */
			    /*^cppif.else */
			    /*_.IFCPP___V56*/ meltfptr[52] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
			    ;
			    MELT_LOCATION
			      ("warmelt-normatch.melt:3205:/ quasiblock");


			    /*_.PROGN___V60*/ meltfptr[56] =
			      /*_.IFCPP___V56*/ meltfptr[52];;
			    /*^compute */
			    /*_.IFELSE___V42*/ meltfptr[23] =
			      /*_.PROGN___V60*/ meltfptr[56];;
			    /*epilog */

			    MELT_LOCATION
			      ("warmelt-normatch.melt:3205:/ clear");
		   /*clear *//*_.IFCPP___V43*/ meltfptr[30] = 0;
			    /*^clear */
		   /*clear *//*_.IFCPP___V47*/ meltfptr[37] = 0;
			    /*^clear */
		   /*clear *//*_.IFCPP___V51*/ meltfptr[44] = 0;
			    /*^clear */
		   /*clear *//*_.PUT_ELSE_MATCH__V55*/ meltfptr[48] =
			      0;
			    /*^clear */
		   /*clear *//*_.IFCPP___V56*/ meltfptr[52] = 0;
			    /*^clear */
		   /*clear *//*_.PROGN___V60*/ meltfptr[56] = 0;
			  }
			  ;
			}
		      else
			{	/*^cond.else */

	/*_.IFELSE___V42*/ meltfptr[23] = NULL;;
			}
		      ;
		      /*^compute */
		      /*_.IFELSE___V35*/ meltfptr[22] =
			/*_.IFELSE___V42*/ meltfptr[23];;
		      /*epilog */

		      MELT_LOCATION ("warmelt-normatch.melt:3202:/ clear");
		 /*clear *//*_.IFELSE___V42*/ meltfptr[23] = 0;
		    }
		    ;
		  }
		;
		/*_.IFELSE___V28*/ meltfptr[21] =
		  /*_.IFELSE___V35*/ meltfptr[22];;
		/*epilog */

		MELT_LOCATION ("warmelt-normatch.melt:3199:/ clear");
	       /*clear *//*_#__L14*/ meltfnum[12] = 0;
		/*^clear */
	       /*clear *//*_.IFELSE___V35*/ meltfptr[22] = 0;
	      }
	      ;
	    }
	  ;
	  /*_.IFELSE___V21*/ meltfptr[20] = /*_.IFELSE___V28*/ meltfptr[21];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-normatch.melt:3196:/ clear");
	     /*clear *//*_#__L11*/ meltfnum[9] = 0;
	  /*^clear */
	     /*clear *//*_.IFELSE___V28*/ meltfptr[21] = 0;
	}
	;
      }
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-normatch.melt:3212:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L25*/ meltfnum[15] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-normatch.melt:3212:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L25*/ meltfnum[15])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L26*/ meltfnum[0] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-normatch.melt:3212:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long =
		/*_#THE_MELTCALLCOUNT__L26*/ meltfnum[0];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-normatch.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 3212;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "putelse_matchstepthen end recv";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.RECV__V2*/ meltfptr[1];
	      /*_.MELT_DEBUG_FUN__V63*/ meltfptr[37] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V62*/ meltfptr[30] =
	      /*_.MELT_DEBUG_FUN__V63*/ meltfptr[37];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-normatch.melt:3212:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L26*/ meltfnum[0] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V63*/ meltfptr[37] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V62*/ meltfptr[30] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-normatch.melt:3212:/ quasiblock");


      /*_.PROGN___V64*/ meltfptr[44] = /*_.IF___V62*/ meltfptr[30];;
      /*^compute */
      /*_.IFCPP___V61*/ meltfptr[57] = /*_.PROGN___V64*/ meltfptr[44];;
      /*epilog */

      MELT_LOCATION ("warmelt-normatch.melt:3212:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L25*/ meltfnum[15] = 0;
      /*^clear */
	     /*clear *//*_.IF___V62*/ meltfptr[30] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V64*/ meltfptr[44] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V61*/ meltfptr[57] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    /*^compute */
    /*_.LET___V18*/ meltfptr[16] = /*_.IFCPP___V61*/ meltfptr[57];;

    MELT_LOCATION ("warmelt-normatch.melt:3191:/ clear");
	   /*clear *//*_.MYTHEN__V19*/ meltfptr[18] = 0;
    /*^clear */
	   /*clear *//*_.ELSESTART__V20*/ meltfptr[19] = 0;
    /*^clear */
	   /*clear *//*_#__L8*/ meltfnum[1] = 0;
    /*^clear */
	   /*clear *//*_.IFELSE___V21*/ meltfptr[20] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V61*/ meltfptr[57] = 0;
    MELT_LOCATION ("warmelt-normatch.melt:3183:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.LET___V18*/ meltfptr[16];;

    {
      MELT_LOCATION ("warmelt-normatch.melt:3183:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V4*/ meltfptr[3] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V8*/ meltfptr[4] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V10*/ meltfptr[5] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V14*/ meltfptr[10] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V16*/ meltfptr[11] = 0;
    /*^clear */
	   /*clear *//*_.LET___V18*/ meltfptr[16] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("PUTELSE_MATCHSTEPTHEN", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_70_warmelt_normatch_PUTELSE_MATCHSTEPTHEN_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_70_warmelt_normatch_PUTELSE_MATCHSTEPTHEN */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_71_warmelt_normatch_PUTELSE_MATCHTEST (meltclosure_ptr_t meltclosp_,
						melt_ptr_t meltfirstargp_,
						const melt_argdescr_cell_t
						meltxargdescr_[],
						union meltparam_un *
						meltxargtab_,
						const melt_argdescr_cell_t
						meltxresdescr_[],
						union meltparam_un *
						meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_71_warmelt_normatch_PUTELSE_MATCHTEST_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_71_warmelt_normatch_PUTELSE_MATCHTEST_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 89
    melt_ptr_t mcfr_varptr[89];
#define MELTFRAM_NBVARNUM 36
    long mcfr_varnum[36];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_71_warmelt_normatch_PUTELSE_MATCHTEST is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_71_warmelt_normatch_PUTELSE_MATCHTEST_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 89; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_71_warmelt_normatch_PUTELSE_MATCHTEST nbval 89*/
  meltfram__.mcfr_nbvar = 89 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("PUTELSE_MATCHTEST", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-normatch.melt:3218:/ getarg");
 /*_.RECV__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.ELSESTEP__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.ELSESTEP__V3*/ meltfptr[2])) !=
	      NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-normatch.melt:3219:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L1*/ meltfnum[0] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-normatch.melt:3219:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L1*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-normatch.melt:3219:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-normatch.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 3219;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "putelse_matchtest recv";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.RECV__V2*/ meltfptr[1];
	      /*_.MELT_DEBUG_FUN__V6*/ meltfptr[5] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V5*/ meltfptr[4] = /*_.MELT_DEBUG_FUN__V6*/ meltfptr[5];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-normatch.melt:3219:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V6*/ meltfptr[5] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V5*/ meltfptr[4] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-normatch.melt:3219:/ quasiblock");


      /*_.PROGN___V7*/ meltfptr[5] = /*_.IF___V5*/ meltfptr[4];;
      /*^compute */
      /*_.IFCPP___V4*/ meltfptr[3] = /*_.PROGN___V7*/ meltfptr[5];;
      /*epilog */

      MELT_LOCATION ("warmelt-normatch.melt:3219:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L1*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IF___V5*/ meltfptr[4] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V7*/ meltfptr[5] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V4*/ meltfptr[3] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-normatch.melt:3220:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L3*/ meltfnum[1] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.RECV__V2*/ meltfptr[1]),
			     (melt_ptr_t) (( /*!CLASS_MATCH_STEP_TEST */
					    meltfrout->tabval[1])));;
      MELT_LOCATION ("warmelt-normatch.melt:3220:/ cond");
      /*cond */ if ( /*_#IS_A__L3*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V9*/ meltfptr[5] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-normatch.melt:3220:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check recv"),
				  ("warmelt-normatch.melt")
				  ? ("warmelt-normatch.melt") : __FILE__,
				  (3220) ? (3220) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V9*/ meltfptr[5] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V8*/ meltfptr[4] = /*_.IFELSE___V9*/ meltfptr[5];;
      /*epilog */

      MELT_LOCATION ("warmelt-normatch.melt:3220:/ clear");
	     /*clear *//*_#IS_A__L3*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V9*/ meltfptr[5] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V8*/ meltfptr[4] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-normatch.melt:3221:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L4*/ meltfnum[0] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-normatch.melt:3221:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L4*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L5*/ meltfnum[1] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-normatch.melt:3221:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L5*/ meltfnum[1];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-normatch.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 3221;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "putelse_matchtest elsestep";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.ELSESTEP__V3*/ meltfptr[2];
	      /*_.MELT_DEBUG_FUN__V12*/ meltfptr[11] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V11*/ meltfptr[10] =
	      /*_.MELT_DEBUG_FUN__V12*/ meltfptr[11];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-normatch.melt:3221:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L5*/ meltfnum[1] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V12*/ meltfptr[11] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V11*/ meltfptr[10] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-normatch.melt:3221:/ quasiblock");


      /*_.PROGN___V13*/ meltfptr[11] = /*_.IF___V11*/ meltfptr[10];;
      /*^compute */
      /*_.IFCPP___V10*/ meltfptr[5] = /*_.PROGN___V13*/ meltfptr[11];;
      /*epilog */

      MELT_LOCATION ("warmelt-normatch.melt:3221:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L4*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IF___V11*/ meltfptr[10] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V13*/ meltfptr[11] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V10*/ meltfptr[5] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-normatch.melt:3222:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L6*/ meltfnum[1] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.ELSESTEP__V3*/ meltfptr[2]),
			     (melt_ptr_t) (( /*!CLASS_MATCH_STEP */
					    meltfrout->tabval[2])));;
      MELT_LOCATION ("warmelt-normatch.melt:3222:/ cond");
      /*cond */ if ( /*_#IS_A__L6*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V15*/ meltfptr[11] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-normatch.melt:3222:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check elsestep"),
				  ("warmelt-normatch.melt")
				  ? ("warmelt-normatch.melt") : __FILE__,
				  (3222) ? (3222) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V15*/ meltfptr[11] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V14*/ meltfptr[10] = /*_.IFELSE___V15*/ meltfptr[11];;
      /*epilog */

      MELT_LOCATION ("warmelt-normatch.melt:3222:/ clear");
	     /*clear *//*_#IS_A__L6*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V15*/ meltfptr[11] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V14*/ meltfptr[10] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-normatch.melt:3224:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_APPLICATION_SHALLOWER__L7*/ meltfnum[0] =
	(melt_application_depth () < 100);;
      MELT_LOCATION ("warmelt-normatch.melt:3224:/ cond");
      /*cond */ if ( /*_#MELT_APPLICATION_SHALLOWER__L7*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V17*/ meltfptr[16] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-normatch.melt:3224:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("putelse_matchtest check shallow100"),
				  ("warmelt-normatch.melt")
				  ? ("warmelt-normatch.melt") : __FILE__,
				  (3224) ? (3224) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V17*/ meltfptr[16] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V16*/ meltfptr[11] = /*_.IFELSE___V17*/ meltfptr[16];;
      /*epilog */

      MELT_LOCATION ("warmelt-normatch.melt:3224:/ clear");
	     /*clear *//*_#MELT_APPLICATION_SHALLOWER__L7*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V17*/ meltfptr[16] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V16*/ meltfptr[11] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-normatch.melt:3226:/ quasiblock");


    MELT_LOCATION ("warmelt-normatch.melt:3227:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.RECV__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 5, "MSTEP_ELSE");
  /*_.MYELSE__V19*/ meltfptr[18] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-normatch.melt:3228:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.RECV__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 2, "MSTEP_THEN");
  /*_.MYTHEN__V20*/ meltfptr[19] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-normatch.melt:3229:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      /*_.ELSESTART__V21*/ meltfptr[20] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!START_STEP */ meltfrout->tabval[3])),
		    (melt_ptr_t) ( /*_.ELSESTEP__V3*/ meltfptr[2]), (""),
		    (union meltparam_un *) 0, "", (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-normatch.melt:3232:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#__L8*/ meltfnum[1] =
      (( /*_.RECV__V2*/ meltfptr[1]) == ( /*_.ELSESTEP__V3*/ meltfptr[2]));;
    MELT_LOCATION ("warmelt-normatch.melt:3232:/ cond");
    /*cond */ if ( /*_#__L8*/ meltfnum[1])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{


#if MELT_HAVE_DEBUG
	  MELT_LOCATION ("warmelt-normatch.melt:3233:/ cppif.then");
	  /*^block */
	  /*anyblock */
	  {


	    {
	      /*^locexp */
	      /*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	      melt_dbgcounter++;
#endif
	      ;
	    }
	    ;
	    /*^checksignal */
	    MELT_CHECK_SIGNAL ();
	    ;
     /*_#MELT_NEED_DBG__L9*/ meltfnum[0] =
	      /*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	      ( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	      0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	      ;;
	    MELT_LOCATION ("warmelt-normatch.melt:3233:/ cond");
	    /*cond */ if ( /*_#MELT_NEED_DBG__L9*/ meltfnum[0])	/*then */
	      {
		/*^cond.then */
		/*^block */
		/*anyblock */
		{

       /*_#THE_MELTCALLCOUNT__L10*/ meltfnum[9] =
#ifdef meltcallcount
		    meltcallcount	/* the_meltcallcount */
#else
		    0L
#endif /* meltcallcount the_meltcallcount */
		    ;;
		  MELT_LOCATION ("warmelt-normatch.melt:3233:/ checksignal");
		  MELT_CHECK_SIGNAL ();
		  ;
		  /*^apply */
		  /*apply */
		  {
		    union meltparam_un argtab[5];
		    memset (&argtab, 0, sizeof (argtab));
		    /*^apply.arg */
		    argtab[0].meltbp_long =
		      /*_#THE_MELTCALLCOUNT__L10*/ meltfnum[9];
		    /*^apply.arg */
		    argtab[1].meltbp_cstring = "warmelt-normatch.melt";
		    /*^apply.arg */
		    argtab[2].meltbp_long = 3233;
		    /*^apply.arg */
		    argtab[3].meltbp_cstring =
		      "putelse_matchtest recv same as elsestep";
		    /*^apply.arg */
		    argtab[4].meltbp_aptr =
		      (melt_ptr_t *) & /*_.RECV__V2*/ meltfptr[1];
		    /*_.MELT_DEBUG_FUN__V25*/ meltfptr[24] =
		      melt_apply ((meltclosure_ptr_t)
				  (( /*!MELT_DEBUG_FUN */ meltfrout->
				    tabval[0])),
				  (melt_ptr_t) (( /*nil */ NULL)),
				  (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_PTR ""), argtab, "",
				  (union meltparam_un *) 0);
		  }
		  ;
		  /*_.IF___V24*/ meltfptr[23] =
		    /*_.MELT_DEBUG_FUN__V25*/ meltfptr[24];;
		  /*epilog */

		  MELT_LOCATION ("warmelt-normatch.melt:3233:/ clear");
		 /*clear *//*_#THE_MELTCALLCOUNT__L10*/ meltfnum[9] = 0;
		  /*^clear */
		 /*clear *//*_.MELT_DEBUG_FUN__V25*/ meltfptr[24] = 0;
		}
		;
	      }
	    else
	      {			/*^cond.else */

      /*_.IF___V24*/ meltfptr[23] = NULL;;
	      }
	    ;
	    MELT_LOCATION ("warmelt-normatch.melt:3233:/ quasiblock");


	    /*_.PROGN___V26*/ meltfptr[24] = /*_.IF___V24*/ meltfptr[23];;
	    /*^compute */
	    /*_.IFCPP___V23*/ meltfptr[22] = /*_.PROGN___V26*/ meltfptr[24];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-normatch.melt:3233:/ clear");
	       /*clear *//*_#MELT_NEED_DBG__L9*/ meltfnum[0] = 0;
	    /*^clear */
	       /*clear *//*_.IF___V24*/ meltfptr[23] = 0;
	    /*^clear */
	       /*clear *//*_.PROGN___V26*/ meltfptr[24] = 0;
	  }

#else /*MELT_HAVE_DEBUG */
	  /*^cppif.else */
	  /*_.IFCPP___V23*/ meltfptr[22] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
	  ;
	  MELT_LOCATION ("warmelt-normatch.melt:3234:/ quasiblock");


   /*_.RETVAL___V1*/ meltfptr[0] = NULL;;

	  {
	    MELT_LOCATION ("warmelt-normatch.melt:3234:/ locexp");
	    /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
	    if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	      melt_warn_for_no_expected_secondary_results ();
	    /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
	    ;
	  }
	  ;
	  /*^finalreturn */
	  ;
	  /*finalret */ goto labend_rout;
	  MELT_LOCATION ("warmelt-normatch.melt:3232:/ quasiblock");


	  /*_.PROGN___V28*/ meltfptr[24] = /*_.RETURN___V27*/ meltfptr[23];;
	  /*^compute */
	  /*_.IFELSE___V22*/ meltfptr[21] = /*_.PROGN___V28*/ meltfptr[24];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-normatch.melt:3232:/ clear");
	     /*clear *//*_.IFCPP___V23*/ meltfptr[22] = 0;
	  /*^clear */
	     /*clear *//*_.RETURN___V27*/ meltfptr[23] = 0;
	  /*^clear */
	     /*clear *//*_.PROGN___V28*/ meltfptr[24] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-normatch.melt:3235:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
   /*_#__L11*/ meltfnum[9] =
	    (( /*_.RECV__V2*/ meltfptr[1]) ==
	     ( /*_.ELSESTART__V21*/ meltfptr[20]));;
	  MELT_LOCATION ("warmelt-normatch.melt:3235:/ cond");
	  /*cond */ if ( /*_#__L11*/ meltfnum[9])	/*then */
	    {
	      /*^cond.then */
	      /*^block */
	      /*anyblock */
	      {


#if MELT_HAVE_DEBUG
		MELT_LOCATION ("warmelt-normatch.melt:3236:/ cppif.then");
		/*^block */
		/*anyblock */
		{


		  {
		    /*^locexp */
		    /*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
		    melt_dbgcounter++;
#endif
		    ;
		  }
		  ;
		  /*^checksignal */
		  MELT_CHECK_SIGNAL ();
		  ;
       /*_#MELT_NEED_DBG__L12*/ meltfnum[0] =
		    /*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
		    ( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
		    0		/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
		    ;;
		  MELT_LOCATION ("warmelt-normatch.melt:3236:/ cond");
		  /*cond */ if ( /*_#MELT_NEED_DBG__L12*/ meltfnum[0])	/*then */
		    {
		      /*^cond.then */
		      /*^block */
		      /*anyblock */
		      {

	 /*_#THE_MELTCALLCOUNT__L13*/ meltfnum[12] =
#ifdef meltcallcount
			  meltcallcount	/* the_meltcallcount */
#else
			  0L
#endif /* meltcallcount the_meltcallcount */
			  ;;
			MELT_LOCATION
			  ("warmelt-normatch.melt:3236:/ checksignal");
			MELT_CHECK_SIGNAL ();
			;
			/*^apply */
			/*apply */
			{
			  union meltparam_un argtab[5];
			  memset (&argtab, 0, sizeof (argtab));
			  /*^apply.arg */
			  argtab[0].meltbp_long =
			    /*_#THE_MELTCALLCOUNT__L13*/ meltfnum[12];
			  /*^apply.arg */
			  argtab[1].meltbp_cstring = "warmelt-normatch.melt";
			  /*^apply.arg */
			  argtab[2].meltbp_long = 3236;
			  /*^apply.arg */
			  argtab[3].meltbp_cstring =
			    "putelse_matchtest recv same as elsestart";
			  /*^apply.arg */
			  argtab[4].meltbp_aptr =
			    (melt_ptr_t *) & /*_.RECV__V2*/ meltfptr[1];
			  /*_.MELT_DEBUG_FUN__V32*/ meltfptr[31] =
			    melt_apply ((meltclosure_ptr_t)
					(( /*!MELT_DEBUG_FUN */ meltfrout->
					  tabval[0])),
					(melt_ptr_t) (( /*nil */ NULL)),
					(MELTBPARSTR_LONG MELTBPARSTR_CSTRING
					 MELTBPARSTR_LONG MELTBPARSTR_CSTRING
					 MELTBPARSTR_PTR ""), argtab, "",
					(union meltparam_un *) 0);
			}
			;
			/*_.IF___V31*/ meltfptr[24] =
			  /*_.MELT_DEBUG_FUN__V32*/ meltfptr[31];;
			/*epilog */

			MELT_LOCATION ("warmelt-normatch.melt:3236:/ clear");
		   /*clear *//*_#THE_MELTCALLCOUNT__L13*/ meltfnum[12] =
			  0;
			/*^clear */
		   /*clear *//*_.MELT_DEBUG_FUN__V32*/ meltfptr[31] = 0;
		      }
		      ;
		    }
		  else
		    {		/*^cond.else */

	/*_.IF___V31*/ meltfptr[24] = NULL;;
		    }
		  ;
		  MELT_LOCATION ("warmelt-normatch.melt:3236:/ quasiblock");


		  /*_.PROGN___V33*/ meltfptr[31] =
		    /*_.IF___V31*/ meltfptr[24];;
		  /*^compute */
		  /*_.IFCPP___V30*/ meltfptr[23] =
		    /*_.PROGN___V33*/ meltfptr[31];;
		  /*epilog */

		  MELT_LOCATION ("warmelt-normatch.melt:3236:/ clear");
		 /*clear *//*_#MELT_NEED_DBG__L12*/ meltfnum[0] = 0;
		  /*^clear */
		 /*clear *//*_.IF___V31*/ meltfptr[24] = 0;
		  /*^clear */
		 /*clear *//*_.PROGN___V33*/ meltfptr[31] = 0;
		}

#else /*MELT_HAVE_DEBUG */
		/*^cppif.else */
		/*_.IFCPP___V30*/ meltfptr[23] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
		;
		MELT_LOCATION ("warmelt-normatch.melt:3237:/ quasiblock");


     /*_.RETVAL___V1*/ meltfptr[0] = NULL;;

		{
		  MELT_LOCATION ("warmelt-normatch.melt:3237:/ locexp");
		  /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
		  if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
		    melt_warn_for_no_expected_secondary_results ();
		  /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
		  ;
		}
		;
		/*^finalreturn */
		;
		/*finalret */ goto labend_rout;
		MELT_LOCATION ("warmelt-normatch.melt:3235:/ quasiblock");


		/*_.PROGN___V35*/ meltfptr[31] =
		  /*_.RETURN___V34*/ meltfptr[24];;
		/*^compute */
		/*_.IFELSE___V29*/ meltfptr[22] =
		  /*_.PROGN___V35*/ meltfptr[31];;
		/*epilog */

		MELT_LOCATION ("warmelt-normatch.melt:3235:/ clear");
	       /*clear *//*_.IFCPP___V30*/ meltfptr[23] = 0;
		/*^clear */
	       /*clear *//*_.RETURN___V34*/ meltfptr[24] = 0;
		/*^clear */
	       /*clear *//*_.PROGN___V35*/ meltfptr[31] = 0;
	      }
	      ;
	    }
	  else
	    {			/*^cond.else */

	      /*^block */
	      /*anyblock */
	      {

		MELT_LOCATION ("warmelt-normatch.melt:3238:/ checksignal");
		MELT_CHECK_SIGNAL ();
		;
     /*_#__L14*/ meltfnum[12] =
		  (( /*_.MYELSE__V19*/ meltfptr[18]) ==
		   ( /*_.ELSESTART__V21*/ meltfptr[20]));;
		MELT_LOCATION ("warmelt-normatch.melt:3238:/ cond");
		/*cond */ if ( /*_#__L14*/ meltfnum[12])	/*then */
		  {
		    /*^cond.then */
		    /*^block */
		    /*anyblock */
		    {


#if MELT_HAVE_DEBUG
		      MELT_LOCATION
			("warmelt-normatch.melt:3239:/ cppif.then");
		      /*^block */
		      /*anyblock */
		      {


			{
			  /*^locexp */
			  /*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
			  melt_dbgcounter++;
#endif
			  ;
			}
			;
			/*^checksignal */
			MELT_CHECK_SIGNAL ();
			;
	 /*_#MELT_NEED_DBG__L15*/ meltfnum[0] =
			  /*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
			  ( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
			  0	/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
			  ;;
			MELT_LOCATION ("warmelt-normatch.melt:3239:/ cond");
			/*cond */ if ( /*_#MELT_NEED_DBG__L15*/ meltfnum[0])	/*then */
			  {
			    /*^cond.then */
			    /*^block */
			    /*anyblock */
			    {

	   /*_#THE_MELTCALLCOUNT__L16*/ meltfnum[15] =
#ifdef meltcallcount
				meltcallcount	/* the_meltcallcount */
#else
				0L
#endif /* meltcallcount the_meltcallcount */
				;;
			      MELT_LOCATION
				("warmelt-normatch.melt:3239:/ checksignal");
			      MELT_CHECK_SIGNAL ();
			      ;
			      /*^apply */
			      /*apply */
			      {
				union meltparam_un argtab[5];
				memset (&argtab, 0, sizeof (argtab));
				/*^apply.arg */
				argtab[0].meltbp_long =
				  /*_#THE_MELTCALLCOUNT__L16*/ meltfnum[15];
				/*^apply.arg */
				argtab[1].meltbp_cstring =
				  "warmelt-normatch.melt";
				/*^apply.arg */
				argtab[2].meltbp_long = 3239;
				/*^apply.arg */
				argtab[3].meltbp_cstring =
				  "putelse_matchtest myelse same as elsestart";
				/*^apply.arg */
				argtab[4].meltbp_aptr =
				  (melt_ptr_t *) & /*_.RECV__V2*/ meltfptr[1];
				/*_.MELT_DEBUG_FUN__V39*/ meltfptr[38] =
				  melt_apply ((meltclosure_ptr_t)
					      (( /*!MELT_DEBUG_FUN */
						meltfrout->tabval[0])),
					      (melt_ptr_t) (( /*nil */ NULL)),
					      (MELTBPARSTR_LONG
					       MELTBPARSTR_CSTRING
					       MELTBPARSTR_LONG
					       MELTBPARSTR_CSTRING
					       MELTBPARSTR_PTR ""), argtab,
					      "", (union meltparam_un *) 0);
			      }
			      ;
			      /*_.IF___V38*/ meltfptr[31] =
				/*_.MELT_DEBUG_FUN__V39*/ meltfptr[38];;
			      /*epilog */

			      MELT_LOCATION
				("warmelt-normatch.melt:3239:/ clear");
		     /*clear *//*_#THE_MELTCALLCOUNT__L16*/
				meltfnum[15] = 0;
			      /*^clear */
		     /*clear *//*_.MELT_DEBUG_FUN__V39*/ meltfptr[38]
				= 0;
			    }
			    ;
			  }
			else
			  {	/*^cond.else */

	  /*_.IF___V38*/ meltfptr[31] = NULL;;
			  }
			;
			MELT_LOCATION
			  ("warmelt-normatch.melt:3239:/ quasiblock");


			/*_.PROGN___V40*/ meltfptr[38] =
			  /*_.IF___V38*/ meltfptr[31];;
			/*^compute */
			/*_.IFCPP___V37*/ meltfptr[24] =
			  /*_.PROGN___V40*/ meltfptr[38];;
			/*epilog */

			MELT_LOCATION ("warmelt-normatch.melt:3239:/ clear");
		   /*clear *//*_#MELT_NEED_DBG__L15*/ meltfnum[0] = 0;
			/*^clear */
		   /*clear *//*_.IF___V38*/ meltfptr[31] = 0;
			/*^clear */
		   /*clear *//*_.PROGN___V40*/ meltfptr[38] = 0;
		      }

#else /*MELT_HAVE_DEBUG */
		      /*^cppif.else */
		      /*_.IFCPP___V37*/ meltfptr[24] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
		      ;
		      MELT_LOCATION
			("warmelt-normatch.melt:3240:/ quasiblock");


       /*_.RETVAL___V1*/ meltfptr[0] = NULL;;

		      {
			MELT_LOCATION ("warmelt-normatch.melt:3240:/ locexp");
			/*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
			if (meltxresdescr_ && meltxresdescr_[0]
			    && meltxrestab_)
			  melt_warn_for_no_expected_secondary_results ();
			/* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
			;
		      }
		      ;
		      /*^finalreturn */
		      ;
		      /*finalret */ goto labend_rout;
		      MELT_LOCATION
			("warmelt-normatch.melt:3238:/ quasiblock");


		      /*_.PROGN___V42*/ meltfptr[38] =
			/*_.RETURN___V41*/ meltfptr[31];;
		      /*^compute */
		      /*_.IFELSE___V36*/ meltfptr[23] =
			/*_.PROGN___V42*/ meltfptr[38];;
		      /*epilog */

		      MELT_LOCATION ("warmelt-normatch.melt:3238:/ clear");
		 /*clear *//*_.IFCPP___V37*/ meltfptr[24] = 0;
		      /*^clear */
		 /*clear *//*_.RETURN___V41*/ meltfptr[31] = 0;
		      /*^clear */
		 /*clear *//*_.PROGN___V42*/ meltfptr[38] = 0;
		    }
		    ;
		  }
		else
		  {		/*^cond.else */

		    /*^block */
		    /*anyblock */
		    {

		      MELT_LOCATION
			("warmelt-normatch.melt:3241:/ checksignal");
		      MELT_CHECK_SIGNAL ();
		      ;
       /*_#__L17*/ meltfnum[15] =
			(( /*_.MYELSE__V19*/ meltfptr[18]) ==
			 ( /*_.ELSESTEP__V3*/ meltfptr[2]));;
		      MELT_LOCATION ("warmelt-normatch.melt:3241:/ cond");
		      /*cond */ if ( /*_#__L17*/ meltfnum[15])	/*then */
			{
			  /*^cond.then */
			  /*^block */
			  /*anyblock */
			  {


#if MELT_HAVE_DEBUG
			    MELT_LOCATION
			      ("warmelt-normatch.melt:3242:/ cppif.then");
			    /*^block */
			    /*anyblock */
			    {


			      {
				/*^locexp */
				/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
				melt_dbgcounter++;
#endif
				;
			      }
			      ;
			      /*^checksignal */
			      MELT_CHECK_SIGNAL ();
			      ;
	   /*_#MELT_NEED_DBG__L18*/ meltfnum[0] =
				/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
				( /*melt_need_dbg */
				 melt_need_debug ((int) 0))
#else
				0	/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
				;;
			      MELT_LOCATION
				("warmelt-normatch.melt:3242:/ cond");
			      /*cond */ if ( /*_#MELT_NEED_DBG__L18*/ meltfnum[0])	/*then */
				{
				  /*^cond.then */
				  /*^block */
				  /*anyblock */
				  {

	     /*_#THE_MELTCALLCOUNT__L19*/ meltfnum[18]
				      =
#ifdef meltcallcount
				      meltcallcount	/* the_meltcallcount */
#else
				      0L
#endif /* meltcallcount the_meltcallcount */
				      ;;
				    MELT_LOCATION
				      ("warmelt-normatch.melt:3242:/ checksignal");
				    MELT_CHECK_SIGNAL ();
				    ;
				    /*^apply */
				    /*apply */
				    {
				      union meltparam_un argtab[5];
				      memset (&argtab, 0, sizeof (argtab));
				      /*^apply.arg */
				      argtab[0].meltbp_long =
					/*_#THE_MELTCALLCOUNT__L19*/
					meltfnum[18];
				      /*^apply.arg */
				      argtab[1].meltbp_cstring =
					"warmelt-normatch.melt";
				      /*^apply.arg */
				      argtab[2].meltbp_long = 3242;
				      /*^apply.arg */
				      argtab[3].meltbp_cstring =
					"putelse_matchtest myelse same as elsestep";
				      /*^apply.arg */
				      argtab[4].meltbp_aptr =
					(melt_ptr_t *) & /*_.RECV__V2*/
					meltfptr[1];
				      /*_.MELT_DEBUG_FUN__V46*/ meltfptr[45] =
					melt_apply ((meltclosure_ptr_t)
						    (( /*!MELT_DEBUG_FUN */
						      meltfrout->tabval[0])),
						    (melt_ptr_t) (( /*nil */
								   NULL)),
						    (MELTBPARSTR_LONG
						     MELTBPARSTR_CSTRING
						     MELTBPARSTR_LONG
						     MELTBPARSTR_CSTRING
						     MELTBPARSTR_PTR ""),
						    argtab, "",
						    (union meltparam_un *) 0);
				    }
				    ;
				    /*_.IF___V45*/ meltfptr[38] =
				      /*_.MELT_DEBUG_FUN__V46*/ meltfptr[45];;
				    /*epilog */

				    MELT_LOCATION
				      ("warmelt-normatch.melt:3242:/ clear");
		       /*clear *//*_#THE_MELTCALLCOUNT__L19*/
				      meltfnum[18] = 0;
				    /*^clear */
		       /*clear *//*_.MELT_DEBUG_FUN__V46*/
				      meltfptr[45] = 0;
				  }
				  ;
				}
			      else
				{	/*^cond.else */

	    /*_.IF___V45*/ meltfptr[38] = NULL;;
				}
			      ;
			      MELT_LOCATION
				("warmelt-normatch.melt:3242:/ quasiblock");


			      /*_.PROGN___V47*/ meltfptr[45] =
				/*_.IF___V45*/ meltfptr[38];;
			      /*^compute */
			      /*_.IFCPP___V44*/ meltfptr[31] =
				/*_.PROGN___V47*/ meltfptr[45];;
			      /*epilog */

			      MELT_LOCATION
				("warmelt-normatch.melt:3242:/ clear");
		     /*clear *//*_#MELT_NEED_DBG__L18*/ meltfnum[0] =
				0;
			      /*^clear */
		     /*clear *//*_.IF___V45*/ meltfptr[38] = 0;
			      /*^clear */
		     /*clear *//*_.PROGN___V47*/ meltfptr[45] = 0;
			    }

#else /*MELT_HAVE_DEBUG */
			    /*^cppif.else */
			    /*_.IFCPP___V44*/ meltfptr[31] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
			    ;
			    MELT_LOCATION
			      ("warmelt-normatch.melt:3243:/ quasiblock");


	 /*_.RETVAL___V1*/ meltfptr[0] = NULL;;

			    {
			      MELT_LOCATION
				("warmelt-normatch.melt:3243:/ locexp");
			      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
			      if (meltxresdescr_ && meltxresdescr_[0]
				  && meltxrestab_)
				melt_warn_for_no_expected_secondary_results
				  ();
			      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
			      ;
			    }
			    ;
			    /*^finalreturn */
			    ;
			    /*finalret */ goto labend_rout;
			    MELT_LOCATION
			      ("warmelt-normatch.melt:3241:/ quasiblock");


			    /*_.PROGN___V49*/ meltfptr[45] =
			      /*_.RETURN___V48*/ meltfptr[38];;
			    /*^compute */
			    /*_.IFELSE___V43*/ meltfptr[24] =
			      /*_.PROGN___V49*/ meltfptr[45];;
			    /*epilog */

			    MELT_LOCATION
			      ("warmelt-normatch.melt:3241:/ clear");
		   /*clear *//*_.IFCPP___V44*/ meltfptr[31] = 0;
			    /*^clear */
		   /*clear *//*_.RETURN___V48*/ meltfptr[38] = 0;
			    /*^clear */
		   /*clear *//*_.PROGN___V49*/ meltfptr[45] = 0;
			  }
			  ;
			}
		      else
			{	/*^cond.else */

			  /*^block */
			  /*anyblock */
			  {

			    MELT_LOCATION
			      ("warmelt-normatch.melt:3244:/ checksignal");
			    MELT_CHECK_SIGNAL ();
			    ;
	 /*_#__L20*/ meltfnum[18] =
			      (( /*_.MYTHEN__V20*/ meltfptr[19]) ==
			       ( /*_.ELSESTEP__V3*/ meltfptr[2]));;
			    MELT_LOCATION
			      ("warmelt-normatch.melt:3244:/ cond");
			    /*cond */ if ( /*_#__L20*/ meltfnum[18])	/*then */
			      {
				/*^cond.then */
				/*^block */
				/*anyblock */
				{


#if MELT_HAVE_DEBUG
				  MELT_LOCATION
				    ("warmelt-normatch.melt:3245:/ cppif.then");
				  /*^block */
				  /*anyblock */
				  {


				    {
				      /*^locexp */
				      /*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
				      melt_dbgcounter++;
#endif
				      ;
				    }
				    ;
				    /*^checksignal */
				    MELT_CHECK_SIGNAL ();
				    ;
	     /*_#MELT_NEED_DBG__L21*/ meltfnum[0] =
				      /*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
				      ( /*melt_need_dbg */
				       melt_need_debug ((int) 0))
#else
				      0	/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
				      ;;
				    MELT_LOCATION
				      ("warmelt-normatch.melt:3245:/ cond");
				    /*cond */ if ( /*_#MELT_NEED_DBG__L21*/ meltfnum[0])	/*then */
				      {
					/*^cond.then */
					/*^block */
					/*anyblock */
					{

	       /*_#THE_MELTCALLCOUNT__L22*/
					    meltfnum[21] =
#ifdef meltcallcount
					    meltcallcount	/* the_meltcallcount */
#else
					    0L
#endif /* meltcallcount the_meltcallcount */
					    ;;
					  MELT_LOCATION
					    ("warmelt-normatch.melt:3245:/ checksignal");
					  MELT_CHECK_SIGNAL ();
					  ;
					  /*^apply */
					  /*apply */
					  {
					    union meltparam_un argtab[5];
					    memset (&argtab, 0,
						    sizeof (argtab));
					    /*^apply.arg */
					    argtab[0].meltbp_long =
					      /*_#THE_MELTCALLCOUNT__L22*/
					      meltfnum[21];
					    /*^apply.arg */
					    argtab[1].meltbp_cstring =
					      "warmelt-normatch.melt";
					    /*^apply.arg */
					    argtab[2].meltbp_long = 3245;
					    /*^apply.arg */
					    argtab[3].meltbp_cstring =
					      "putelse_matchtest mythen same as elsestep";
					    /*^apply.arg */
					    argtab[4].meltbp_aptr =
					      (melt_ptr_t *) & /*_.RECV__V2*/
					      meltfptr[1];
					    /*_.MELT_DEBUG_FUN__V53*/
					      meltfptr[52] =
					      melt_apply ((meltclosure_ptr_t)
							  (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])), (melt_ptr_t) (( /*nil */ NULL)), (MELTBPARSTR_LONG MELTBPARSTR_CSTRING MELTBPARSTR_LONG MELTBPARSTR_CSTRING MELTBPARSTR_PTR ""), argtab, "", (union meltparam_un *) 0);
					  }
					  ;
					  /*_.IF___V52*/ meltfptr[45] =
					    /*_.MELT_DEBUG_FUN__V53*/
					    meltfptr[52];;
					  /*epilog */

					  MELT_LOCATION
					    ("warmelt-normatch.melt:3245:/ clear");
			 /*clear *//*_#THE_MELTCALLCOUNT__L22*/
					    meltfnum[21] = 0;
					  /*^clear */
			 /*clear *//*_.MELT_DEBUG_FUN__V53*/
					    meltfptr[52] = 0;
					}
					;
				      }
				    else
				      {	/*^cond.else */

	      /*_.IF___V52*/ meltfptr[45] = NULL;;
				      }
				    ;
				    MELT_LOCATION
				      ("warmelt-normatch.melt:3245:/ quasiblock");


				    /*_.PROGN___V54*/ meltfptr[52] =
				      /*_.IF___V52*/ meltfptr[45];;
				    /*^compute */
				    /*_.IFCPP___V51*/ meltfptr[38] =
				      /*_.PROGN___V54*/ meltfptr[52];;
				    /*epilog */

				    MELT_LOCATION
				      ("warmelt-normatch.melt:3245:/ clear");
		       /*clear *//*_#MELT_NEED_DBG__L21*/
				      meltfnum[0] = 0;
				    /*^clear */
		       /*clear *//*_.IF___V52*/ meltfptr[45] = 0;
				    /*^clear */
		       /*clear *//*_.PROGN___V54*/ meltfptr[52] =
				      0;
				  }

#else /*MELT_HAVE_DEBUG */
				  /*^cppif.else */
				  /*_.IFCPP___V51*/ meltfptr[38] =
				    ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
				  ;
				  MELT_LOCATION
				    ("warmelt-normatch.melt:3246:/ quasiblock");


	   /*_.RETVAL___V1*/ meltfptr[0] = NULL;;

				  {
				    MELT_LOCATION
				      ("warmelt-normatch.melt:3246:/ locexp");
				    /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
				    if (meltxresdescr_ && meltxresdescr_[0]
					&& meltxrestab_)
				      melt_warn_for_no_expected_secondary_results
					();
				    /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
				    ;
				  }
				  ;
				  /*^finalreturn */
				  ;
				  /*finalret */ goto labend_rout;
				  MELT_LOCATION
				    ("warmelt-normatch.melt:3244:/ quasiblock");


				  /*_.PROGN___V56*/ meltfptr[52] =
				    /*_.RETURN___V55*/ meltfptr[45];;
				  /*^compute */
				  /*_.IFELSE___V50*/ meltfptr[31] =
				    /*_.PROGN___V56*/ meltfptr[52];;
				  /*epilog */

				  MELT_LOCATION
				    ("warmelt-normatch.melt:3244:/ clear");
		     /*clear *//*_.IFCPP___V51*/ meltfptr[38] =
				    0;
				  /*^clear */
		     /*clear *//*_.RETURN___V55*/ meltfptr[45] =
				    0;
				  /*^clear */
		     /*clear *//*_.PROGN___V56*/ meltfptr[52] =
				    0;
				}
				;
			      }
			    else
			      {	/*^cond.else */

				/*^block */
				/*anyblock */
				{

				  MELT_LOCATION
				    ("warmelt-normatch.melt:3247:/ checksignal");
				  MELT_CHECK_SIGNAL ();
				  ;
	   /*_#__L23*/ meltfnum[21] =
				    (( /*_.MYTHEN__V20*/ meltfptr[19]) ==
				     ( /*_.ELSESTART__V21*/ meltfptr[20]));;
				  MELT_LOCATION
				    ("warmelt-normatch.melt:3247:/ cond");
				  /*cond */ if ( /*_#__L23*/ meltfnum[21])	/*then */
				    {
				      /*^cond.then */
				      /*^block */
				      /*anyblock */
				      {


#if MELT_HAVE_DEBUG
					MELT_LOCATION
					  ("warmelt-normatch.melt:3248:/ cppif.then");
					/*^block */
					/*anyblock */
					{


					  {
					    /*^locexp */
					    /*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
					    melt_dbgcounter++;
#endif
					    ;
					  }
					  ;
					  /*^checksignal */
					  MELT_CHECK_SIGNAL ();
					  ;
	       /*_#MELT_NEED_DBG__L24*/ meltfnum[0]
					    =
					    /*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
					    ( /*melt_need_dbg */
					     melt_need_debug ((int) 0))
#else
					    0	/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
					    ;;
					  MELT_LOCATION
					    ("warmelt-normatch.melt:3248:/ cond");
					  /*cond */ if ( /*_#MELT_NEED_DBG__L24*/ meltfnum[0])	/*then */
					    {
					      /*^cond.then */
					      /*^block */
					      /*anyblock */
					      {

		 /*_#THE_MELTCALLCOUNT__L25*/
						  meltfnum[24] =
#ifdef meltcallcount
						  meltcallcount	/* the_meltcallcount */
#else
						  0L
#endif /* meltcallcount the_meltcallcount */
						  ;;
						MELT_LOCATION
						  ("warmelt-normatch.melt:3248:/ checksignal");
						MELT_CHECK_SIGNAL ();
						;
						/*^apply */
						/*apply */
						{
						  union meltparam_un
						    argtab[5];
						  memset (&argtab, 0,
							  sizeof (argtab));
						  /*^apply.arg */
						  argtab[0].meltbp_long =
						    /*_#THE_MELTCALLCOUNT__L25*/
						    meltfnum[24];
						  /*^apply.arg */
						  argtab[1].meltbp_cstring =
						    "warmelt-normatch.melt";
						  /*^apply.arg */
						  argtab[2].meltbp_long =
						    3248;
						  /*^apply.arg */
						  argtab[3].meltbp_cstring =
						    "putelse_matchtest mythen same as elsestart";
						  /*^apply.arg */
						  argtab[4].meltbp_aptr =
						    (melt_ptr_t *) &
						    /*_.RECV__V2*/
						    meltfptr[1];
						  /*_.MELT_DEBUG_FUN__V60*/
						    meltfptr[59] =
						    melt_apply ((meltclosure_ptr_t) (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])), (melt_ptr_t) (( /*nil */ NULL)), (MELTBPARSTR_LONG MELTBPARSTR_CSTRING MELTBPARSTR_LONG MELTBPARSTR_CSTRING MELTBPARSTR_PTR ""), argtab, "", (union meltparam_un *) 0);
						}
						;
						/*_.IF___V59*/ meltfptr[52] =
						  /*_.MELT_DEBUG_FUN__V60*/
						  meltfptr[59];;
						/*epilog */

						MELT_LOCATION
						  ("warmelt-normatch.melt:3248:/ clear");
			   /*clear *//*_#THE_MELTCALLCOUNT__L25*/
						  meltfnum[24] = 0;
						/*^clear */
			   /*clear *//*_.MELT_DEBUG_FUN__V60*/
						  meltfptr[59] = 0;
					      }
					      ;
					    }
					  else
					    {	/*^cond.else */

		/*_.IF___V59*/ meltfptr[52] =
						NULL;;
					    }
					  ;
					  MELT_LOCATION
					    ("warmelt-normatch.melt:3248:/ quasiblock");


					  /*_.PROGN___V61*/ meltfptr[59] =
					    /*_.IF___V59*/ meltfptr[52];;
					  /*^compute */
					  /*_.IFCPP___V58*/ meltfptr[45] =
					    /*_.PROGN___V61*/ meltfptr[59];;
					  /*epilog */

					  MELT_LOCATION
					    ("warmelt-normatch.melt:3248:/ clear");
			 /*clear *//*_#MELT_NEED_DBG__L24*/
					    meltfnum[0] = 0;
					  /*^clear */
			 /*clear *//*_.IF___V59*/
					    meltfptr[52] = 0;
					  /*^clear */
			 /*clear *//*_.PROGN___V61*/
					    meltfptr[59] = 0;
					}

#else /*MELT_HAVE_DEBUG */
					/*^cppif.else */
					/*_.IFCPP___V58*/ meltfptr[45] =
					  ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
					;
					MELT_LOCATION
					  ("warmelt-normatch.melt:3249:/ quasiblock");


	     /*_.RETVAL___V1*/ meltfptr[0] = NULL;;

					{
					  MELT_LOCATION
					    ("warmelt-normatch.melt:3249:/ locexp");
					  /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
					  if (meltxresdescr_
					      && meltxresdescr_[0]
					      && meltxrestab_)
					    melt_warn_for_no_expected_secondary_results
					      ();
					  /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
					  ;
					}
					;
					/*^finalreturn */
					;
					/*finalret */ goto labend_rout;
					MELT_LOCATION
					  ("warmelt-normatch.melt:3247:/ quasiblock");


					/*_.PROGN___V63*/ meltfptr[59] =
					  /*_.RETURN___V62*/ meltfptr[52];;
					/*^compute */
					/*_.IFELSE___V57*/ meltfptr[38] =
					  /*_.PROGN___V63*/ meltfptr[59];;
					/*epilog */

					MELT_LOCATION
					  ("warmelt-normatch.melt:3247:/ clear");
		       /*clear *//*_.IFCPP___V58*/
					  meltfptr[45] = 0;
					/*^clear */
		       /*clear *//*_.RETURN___V62*/
					  meltfptr[52] = 0;
					/*^clear */
		       /*clear *//*_.PROGN___V63*/
					  meltfptr[59] = 0;
				      }
				      ;
				    }
				  else
				    {	/*^cond.else */

				      /*^block */
				      /*anyblock */
				      {

					MELT_LOCATION
					  ("warmelt-normatch.melt:3250:/ checksignal");
					MELT_CHECK_SIGNAL ();
					;
	     /*_#NULL__L26*/ meltfnum[24] =
					  (( /*_.MYELSE__V19*/ meltfptr[18])
					   == NULL);;
					MELT_LOCATION
					  ("warmelt-normatch.melt:3250:/ cond");
					/*cond */ if ( /*_#NULL__L26*/ meltfnum[24])	/*then */
					  {
					    /*^cond.then */
					    /*^block */
					    /*anyblock */
					    {

					      MELT_LOCATION
						("warmelt-normatch.melt:3251:/ checksignal");
					      MELT_CHECK_SIGNAL ();
					      ;
					      /*^quasiblock */


					      /*^putslot */
					      /*putslot */
					      melt_assertmsg
						("putslot checkobj @MSTEP_ELSE",
						 melt_magic_discr ((melt_ptr_t) ( /*_.RECV__V2*/ meltfptr[1])) == MELTOBMAG_OBJECT);
					      melt_putfield_object (( /*_.RECV__V2*/ meltfptr[1]), (5), ( /*_.ELSESTART__V21*/ meltfptr[20]), "MSTEP_ELSE");
					      ;
					      /*^touch */
					      meltgc_touch ( /*_.RECV__V2*/
							    meltfptr[1]);
					      ;
					      /*^touchobj */

					      melt_dbgtrace_written_object ( /*_.RECV__V2*/ meltfptr[1], "put-fields");
					      ;


#if MELT_HAVE_DEBUG
					      MELT_LOCATION
						("warmelt-normatch.melt:3252:/ cppif.then");
					      /*^block */
					      /*anyblock */
					      {


						{
						  /*^locexp */
						  /*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
						  melt_dbgcounter++;
#endif
						  ;
						}
						;
						/*^checksignal */
						MELT_CHECK_SIGNAL ();
						;
		 /*_#MELT_NEED_DBG__L27*/
						  meltfnum[0] =
						  /*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
						  ( /*melt_need_dbg */
						   melt_need_debug ((int) 0))
#else
						  0	/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
						  ;;
						MELT_LOCATION
						  ("warmelt-normatch.melt:3252:/ cond");
						/*cond */ if ( /*_#MELT_NEED_DBG__L27*/ meltfnum[0])	/*then */
						  {
						    /*^cond.then */
						    /*^block */
						    /*anyblock */
						    {

		   /*_#THE_MELTCALLCOUNT__L28*/
							meltfnum[27] =
#ifdef meltcallcount
							meltcallcount	/* the_meltcallcount */
#else
							0L
#endif /* meltcallcount the_meltcallcount */
							;;
						      MELT_LOCATION
							("warmelt-normatch.melt:3252:/ checksignal");
						      MELT_CHECK_SIGNAL ();
						      ;
						      /*^apply */
						      /*apply */
						      {
							union meltparam_un
							  argtab[5];
							memset (&argtab, 0,
								sizeof
								(argtab));
							/*^apply.arg */
							argtab[0].
							  meltbp_long =
							  /*_#THE_MELTCALLCOUNT__L28*/
							  meltfnum[27];
							/*^apply.arg */
							argtab[1].
							  meltbp_cstring =
							  "warmelt-normatch.melt";
							/*^apply.arg */
							argtab[2].
							  meltbp_long = 3252;
							/*^apply.arg */
							argtab[3].
							  meltbp_cstring =
							  "putelse_matchtest updated recv";
							/*^apply.arg */
							argtab[4].
							  meltbp_aptr =
							  (melt_ptr_t *) &
							  /*_.RECV__V2*/
							  meltfptr[1];
							/*_.MELT_DEBUG_FUN__V67*/
							  meltfptr[66] =
							  melt_apply ((meltclosure_ptr_t) (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])), (melt_ptr_t) (( /*nil */ NULL)), (MELTBPARSTR_LONG MELTBPARSTR_CSTRING MELTBPARSTR_LONG MELTBPARSTR_CSTRING MELTBPARSTR_PTR ""), argtab, "", (union meltparam_un *) 0);
						      }
						      ;
						      /*_.IF___V66*/
							meltfptr[59] =
							/*_.MELT_DEBUG_FUN__V67*/
							meltfptr[66];;
						      /*epilog */

						      MELT_LOCATION
							("warmelt-normatch.melt:3252:/ clear");
			     /*clear *//*_#THE_MELTCALLCOUNT__L28*/
							meltfnum[27] = 0;
						      /*^clear */
			     /*clear *//*_.MELT_DEBUG_FUN__V67*/
							meltfptr[66] = 0;
						    }
						    ;
						  }
						else
						  {	/*^cond.else */

		  /*_.IF___V66*/
						      meltfptr[59] = NULL;;
						  }
						;
						MELT_LOCATION
						  ("warmelt-normatch.melt:3252:/ quasiblock");


						/*_.PROGN___V68*/ meltfptr[66]
						  =
						  /*_.IF___V66*/
						  meltfptr[59];;
						/*^compute */
						/*_.IFCPP___V65*/ meltfptr[52]
						  =
						  /*_.PROGN___V68*/
						  meltfptr[66];;
						/*epilog */

						MELT_LOCATION
						  ("warmelt-normatch.melt:3252:/ clear");
			   /*clear *//*_#MELT_NEED_DBG__L27*/
						  meltfnum[0] = 0;
						/*^clear */
			   /*clear *//*_.IF___V66*/
						  meltfptr[59] = 0;
						/*^clear */
			   /*clear *//*_.PROGN___V68*/
						  meltfptr[66] = 0;
					      }

#else /*MELT_HAVE_DEBUG */
					      /*^cppif.else */
					      /*_.IFCPP___V65*/ meltfptr[52] =
						( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
					      ;

					      {
						MELT_LOCATION
						  ("warmelt-normatch.melt:3253:/ locexp");

#if MELT_HAVE_DEBUG
						if (melt_need_debug (0))
						  melt_dbgshortbacktrace (("putelse_matchtest"), (12));
#endif
						;
					      }
					      ;
					      MELT_LOCATION
						("warmelt-normatch.melt:3250:/ quasiblock");


					      /*epilog */

					      /*^clear */
			 /*clear *//*_.IFCPP___V65*/
						meltfptr[52] = 0;
					    }
					    ;
					  }
					else
					  {	/*^cond.else */

					    /*^block */
					    /*anyblock */
					    {


#if MELT_HAVE_DEBUG
					      MELT_LOCATION
						("warmelt-normatch.melt:3256:/ cppif.then");
					      /*^block */
					      /*anyblock */
					      {


						{
						  /*^locexp */
						  /*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
						  melt_dbgcounter++;
#endif
						  ;
						}
						;
						/*^checksignal */
						MELT_CHECK_SIGNAL ();
						;
		 /*_#MELT_NEED_DBG__L29*/
						  meltfnum[27] =
						  /*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
						  ( /*melt_need_dbg */
						   melt_need_debug ((int) 0))
#else
						  0	/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
						  ;;
						MELT_LOCATION
						  ("warmelt-normatch.melt:3256:/ cond");
						/*cond */ if ( /*_#MELT_NEED_DBG__L29*/ meltfnum[27])	/*then */
						  {
						    /*^cond.then */
						    /*^block */
						    /*anyblock */
						    {

		   /*_#THE_MELTCALLCOUNT__L30*/
							meltfnum[0] =
#ifdef meltcallcount
							meltcallcount	/* the_meltcallcount */
#else
							0L
#endif /* meltcallcount the_meltcallcount */
							;;
						      MELT_LOCATION
							("warmelt-normatch.melt:3256:/ checksignal");
						      MELT_CHECK_SIGNAL ();
						      ;
						      /*^apply */
						      /*apply */
						      {
							union meltparam_un
							  argtab[5];
							memset (&argtab, 0,
								sizeof
								(argtab));
							/*^apply.arg */
							argtab[0].
							  meltbp_long =
							  /*_#THE_MELTCALLCOUNT__L30*/
							  meltfnum[0];
							/*^apply.arg */
							argtab[1].
							  meltbp_cstring =
							  "warmelt-normatch.melt";
							/*^apply.arg */
							argtab[2].
							  meltbp_long = 3256;
							/*^apply.arg */
							argtab[3].
							  meltbp_cstring =
							  "putelse_matchtest myelse appending then";
							/*^apply.arg */
							argtab[4].
							  meltbp_aptr =
							  (melt_ptr_t *) &
							  /*_.MYELSE__V19*/
							  meltfptr[18];
							/*_.MELT_DEBUG_FUN__V71*/
							  meltfptr[52] =
							  melt_apply ((meltclosure_ptr_t) (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])), (melt_ptr_t) (( /*nil */ NULL)), (MELTBPARSTR_LONG MELTBPARSTR_CSTRING MELTBPARSTR_LONG MELTBPARSTR_CSTRING MELTBPARSTR_PTR ""), argtab, "", (union meltparam_un *) 0);
						      }
						      ;
						      /*_.IF___V70*/
							meltfptr[66] =
							/*_.MELT_DEBUG_FUN__V71*/
							meltfptr[52];;
						      /*epilog */

						      MELT_LOCATION
							("warmelt-normatch.melt:3256:/ clear");
			     /*clear *//*_#THE_MELTCALLCOUNT__L30*/
							meltfnum[0] = 0;
						      /*^clear */
			     /*clear *//*_.MELT_DEBUG_FUN__V71*/
							meltfptr[52] = 0;
						    }
						    ;
						  }
						else
						  {	/*^cond.else */

		  /*_.IF___V70*/
						      meltfptr[66] = NULL;;
						  }
						;
						MELT_LOCATION
						  ("warmelt-normatch.melt:3256:/ quasiblock");


						/*_.PROGN___V72*/ meltfptr[52]
						  =
						  /*_.IF___V70*/
						  meltfptr[66];;
						/*^compute */
						/*_.IFCPP___V69*/ meltfptr[59]
						  =
						  /*_.PROGN___V72*/
						  meltfptr[52];;
						/*epilog */

						MELT_LOCATION
						  ("warmelt-normatch.melt:3256:/ clear");
			   /*clear *//*_#MELT_NEED_DBG__L29*/
						  meltfnum[27] = 0;
						/*^clear */
			   /*clear *//*_.IF___V70*/
						  meltfptr[66] = 0;
						/*^clear */
			   /*clear *//*_.PROGN___V72*/
						  meltfptr[52] = 0;
					      }

#else /*MELT_HAVE_DEBUG */
					      /*^cppif.else */
					      /*_.IFCPP___V69*/ meltfptr[59] =
						( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
					      ;
					      MELT_LOCATION
						("warmelt-normatch.melt:3257:/ checksignal");
					      MELT_CHECK_SIGNAL ();
					      ;
					      /*^msend */
					      /*msend */
					      {
						union meltparam_un argtab[1];
						memset (&argtab, 0,
							sizeof (argtab));
						/*^ojbmsend.arg */
						argtab[0].meltbp_aptr =
						  (melt_ptr_t *) &
						  /*_.ELSESTART__V21*/
						  meltfptr[20];
						/*_.PUT_THEN_MATCH__V73*/
						  meltfptr[66] =
						  meltgc_send ((melt_ptr_t)
							       ( /*_.MYELSE__V19*/ meltfptr[18]), (melt_ptr_t) (( /*!PUT_THEN_MATCH */ meltfrout->tabval[4])), (MELTBPARSTR_PTR ""), argtab, "", (union meltparam_un *) 0);
					      }
					      ;
					      MELT_LOCATION
						("warmelt-normatch.melt:3255:/ quasiblock");


					      /*_.PROGN___V74*/ meltfptr[52] =
						/*_.PUT_THEN_MATCH__V73*/
						meltfptr[66];;
					      /*^compute */
					      /*_.IFELSE___V64*/ meltfptr[45]
						=
						/*_.PROGN___V74*/
						meltfptr[52];;
					      /*epilog */

					      MELT_LOCATION
						("warmelt-normatch.melt:3250:/ clear");
			 /*clear *//*_.IFCPP___V69*/
						meltfptr[59] = 0;
					      /*^clear */
			 /*clear *//*_.PUT_THEN_MATCH__V73*/
						meltfptr[66] = 0;
					      /*^clear */
			 /*clear *//*_.PROGN___V74*/
						meltfptr[52] = 0;
					    }
					    ;
					  }
					;
					/*_.IFELSE___V57*/ meltfptr[38] =
					  /*_.IFELSE___V64*/ meltfptr[45];;
					/*epilog */

					MELT_LOCATION
					  ("warmelt-normatch.melt:3247:/ clear");
		       /*clear *//*_#NULL__L26*/ meltfnum[24]
					  = 0;
					/*^clear */
		       /*clear *//*_.IFELSE___V64*/
					  meltfptr[45] = 0;
				      }
				      ;
				    }
				  ;
				  /*_.IFELSE___V50*/ meltfptr[31] =
				    /*_.IFELSE___V57*/ meltfptr[38];;
				  /*epilog */

				  MELT_LOCATION
				    ("warmelt-normatch.melt:3244:/ clear");
		     /*clear *//*_#__L23*/ meltfnum[21] = 0;
				  /*^clear */
		     /*clear *//*_.IFELSE___V57*/ meltfptr[38] =
				    0;
				}
				;
			      }
			    ;
			    /*_.IFELSE___V43*/ meltfptr[24] =
			      /*_.IFELSE___V50*/ meltfptr[31];;
			    /*epilog */

			    MELT_LOCATION
			      ("warmelt-normatch.melt:3241:/ clear");
		   /*clear *//*_#__L20*/ meltfnum[18] = 0;
			    /*^clear */
		   /*clear *//*_.IFELSE___V50*/ meltfptr[31] = 0;
			  }
			  ;
			}
		      ;
		      /*_.IFELSE___V36*/ meltfptr[23] =
			/*_.IFELSE___V43*/ meltfptr[24];;
		      /*epilog */

		      MELT_LOCATION ("warmelt-normatch.melt:3238:/ clear");
		 /*clear *//*_#__L17*/ meltfnum[15] = 0;
		      /*^clear */
		 /*clear *//*_.IFELSE___V43*/ meltfptr[24] = 0;
		    }
		    ;
		  }
		;
		/*_.IFELSE___V29*/ meltfptr[22] =
		  /*_.IFELSE___V36*/ meltfptr[23];;
		/*epilog */

		MELT_LOCATION ("warmelt-normatch.melt:3235:/ clear");
	       /*clear *//*_#__L14*/ meltfnum[12] = 0;
		/*^clear */
	       /*clear *//*_.IFELSE___V36*/ meltfptr[23] = 0;
	      }
	      ;
	    }
	  ;
	  /*_.IFELSE___V22*/ meltfptr[21] = /*_.IFELSE___V29*/ meltfptr[22];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-normatch.melt:3232:/ clear");
	     /*clear *//*_#__L11*/ meltfnum[9] = 0;
	  /*^clear */
	     /*clear *//*_.IFELSE___V29*/ meltfptr[22] = 0;
	}
	;
      }
    ;
    MELT_LOCATION ("warmelt-normatch.melt:3260:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^cond */
    /*cond */ if ( /*_.MYTHEN__V20*/ meltfptr[19])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{


#if MELT_HAVE_DEBUG
	  MELT_LOCATION ("warmelt-normatch.melt:3262:/ cppif.then");
	  /*^block */
	  /*anyblock */
	  {


	    {
	      /*^locexp */
	      /*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	      melt_dbgcounter++;
#endif
	      ;
	    }
	    ;
	    /*^checksignal */
	    MELT_CHECK_SIGNAL ();
	    ;
     /*_#MELT_NEED_DBG__L31*/ meltfnum[0] =
	      /*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	      ( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	      0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	      ;;
	    MELT_LOCATION ("warmelt-normatch.melt:3262:/ cond");
	    /*cond */ if ( /*_#MELT_NEED_DBG__L31*/ meltfnum[0])	/*then */
	      {
		/*^cond.then */
		/*^block */
		/*anyblock */
		{

       /*_#THE_MELTCALLCOUNT__L32*/ meltfnum[27] =
#ifdef meltcallcount
		    meltcallcount	/* the_meltcallcount */
#else
		    0L
#endif /* meltcallcount the_meltcallcount */
		    ;;
		  MELT_LOCATION ("warmelt-normatch.melt:3262:/ checksignal");
		  MELT_CHECK_SIGNAL ();
		  ;
		  /*^apply */
		  /*apply */
		  {
		    union meltparam_un argtab[5];
		    memset (&argtab, 0, sizeof (argtab));
		    /*^apply.arg */
		    argtab[0].meltbp_long =
		      /*_#THE_MELTCALLCOUNT__L32*/ meltfnum[27];
		    /*^apply.arg */
		    argtab[1].meltbp_cstring = "warmelt-normatch.melt";
		    /*^apply.arg */
		    argtab[2].meltbp_long = 3262;
		    /*^apply.arg */
		    argtab[3].meltbp_cstring =
		      "putelse_matchtest recursing in mythen";
		    /*^apply.arg */
		    argtab[4].meltbp_aptr =
		      (melt_ptr_t *) & /*_.MYTHEN__V20*/ meltfptr[19];
		    /*_.MELT_DEBUG_FUN__V78*/ meltfptr[45] =
		      melt_apply ((meltclosure_ptr_t)
				  (( /*!MELT_DEBUG_FUN */ meltfrout->
				    tabval[0])),
				  (melt_ptr_t) (( /*nil */ NULL)),
				  (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_PTR ""), argtab, "",
				  (union meltparam_un *) 0);
		  }
		  ;
		  /*_.IF___V77*/ meltfptr[52] =
		    /*_.MELT_DEBUG_FUN__V78*/ meltfptr[45];;
		  /*epilog */

		  MELT_LOCATION ("warmelt-normatch.melt:3262:/ clear");
		 /*clear *//*_#THE_MELTCALLCOUNT__L32*/ meltfnum[27] = 0;
		  /*^clear */
		 /*clear *//*_.MELT_DEBUG_FUN__V78*/ meltfptr[45] = 0;
		}
		;
	      }
	    else
	      {			/*^cond.else */

      /*_.IF___V77*/ meltfptr[52] = NULL;;
	      }
	    ;
	    MELT_LOCATION ("warmelt-normatch.melt:3262:/ quasiblock");


	    /*_.PROGN___V79*/ meltfptr[38] = /*_.IF___V77*/ meltfptr[52];;
	    /*^compute */
	    /*_.IFCPP___V76*/ meltfptr[66] = /*_.PROGN___V79*/ meltfptr[38];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-normatch.melt:3262:/ clear");
	       /*clear *//*_#MELT_NEED_DBG__L31*/ meltfnum[0] = 0;
	    /*^clear */
	       /*clear *//*_.IF___V77*/ meltfptr[52] = 0;
	    /*^clear */
	       /*clear *//*_.PROGN___V79*/ meltfptr[38] = 0;
	  }

#else /*MELT_HAVE_DEBUG */
	  /*^cppif.else */
	  /*_.IFCPP___V76*/ meltfptr[66] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
	  ;

#if MELT_HAVE_DEBUG
	  MELT_LOCATION ("warmelt-normatch.melt:3263:/ cppif.then");
	  /*^block */
	  /*anyblock */
	  {


	    {
	      /*^locexp */
	      /*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	      melt_dbgcounter++;
#endif
	      ;
	    }
	    ;
	    /*^checksignal */
	    MELT_CHECK_SIGNAL ();
	    ;
     /*_#MELT_NEED_DBG__L33*/ meltfnum[24] =
	      /*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	      ( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	      0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	      ;;
	    MELT_LOCATION ("warmelt-normatch.melt:3263:/ cond");
	    /*cond */ if ( /*_#MELT_NEED_DBG__L33*/ meltfnum[24])	/*then */
	      {
		/*^cond.then */
		/*^block */
		/*anyblock */
		{

       /*_#THE_MELTCALLCOUNT__L34*/ meltfnum[21] =
#ifdef meltcallcount
		    meltcallcount	/* the_meltcallcount */
#else
		    0L
#endif /* meltcallcount the_meltcallcount */
		    ;;
		  MELT_LOCATION ("warmelt-normatch.melt:3263:/ checksignal");
		  MELT_CHECK_SIGNAL ();
		  ;
		  /*^apply */
		  /*apply */
		  {
		    union meltparam_un argtab[5];
		    memset (&argtab, 0, sizeof (argtab));
		    /*^apply.arg */
		    argtab[0].meltbp_long =
		      /*_#THE_MELTCALLCOUNT__L34*/ meltfnum[21];
		    /*^apply.arg */
		    argtab[1].meltbp_cstring = "warmelt-normatch.melt";
		    /*^apply.arg */
		    argtab[2].meltbp_long = 3263;
		    /*^apply.arg */
		    argtab[3].meltbp_cstring =
		      "putelse_matchtest recursing with elsestart";
		    /*^apply.arg */
		    argtab[4].meltbp_aptr =
		      (melt_ptr_t *) & /*_.ELSESTART__V21*/ meltfptr[20];
		    /*_.MELT_DEBUG_FUN__V82*/ meltfptr[23] =
		      melt_apply ((meltclosure_ptr_t)
				  (( /*!MELT_DEBUG_FUN */ meltfrout->
				    tabval[0])),
				  (melt_ptr_t) (( /*nil */ NULL)),
				  (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_PTR ""), argtab, "",
				  (union meltparam_un *) 0);
		  }
		  ;
		  /*_.IF___V81*/ meltfptr[24] =
		    /*_.MELT_DEBUG_FUN__V82*/ meltfptr[23];;
		  /*epilog */

		  MELT_LOCATION ("warmelt-normatch.melt:3263:/ clear");
		 /*clear *//*_#THE_MELTCALLCOUNT__L34*/ meltfnum[21] = 0;
		  /*^clear */
		 /*clear *//*_.MELT_DEBUG_FUN__V82*/ meltfptr[23] = 0;
		}
		;
	      }
	    else
	      {			/*^cond.else */

      /*_.IF___V81*/ meltfptr[24] = NULL;;
	      }
	    ;
	    MELT_LOCATION ("warmelt-normatch.melt:3263:/ quasiblock");


	    /*_.PROGN___V83*/ meltfptr[22] = /*_.IF___V81*/ meltfptr[24];;
	    /*^compute */
	    /*_.IFCPP___V80*/ meltfptr[31] = /*_.PROGN___V83*/ meltfptr[22];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-normatch.melt:3263:/ clear");
	       /*clear *//*_#MELT_NEED_DBG__L33*/ meltfnum[24] = 0;
	    /*^clear */
	       /*clear *//*_.IF___V81*/ meltfptr[24] = 0;
	    /*^clear */
	       /*clear *//*_.PROGN___V83*/ meltfptr[22] = 0;
	  }

#else /*MELT_HAVE_DEBUG */
	  /*^cppif.else */
	  /*_.IFCPP___V80*/ meltfptr[31] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
	  ;
	  MELT_LOCATION ("warmelt-normatch.melt:3264:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^msend */
	  /*msend */
	  {
	    union meltparam_un argtab[1];
	    memset (&argtab, 0, sizeof (argtab));
	    /*^ojbmsend.arg */
	    argtab[0].meltbp_aptr =
	      (melt_ptr_t *) & /*_.ELSESTART__V21*/ meltfptr[20];
	    /*_.PUT_ELSE_MATCH__V84*/ meltfptr[45] =
	      meltgc_send ((melt_ptr_t) ( /*_.MYTHEN__V20*/ meltfptr[19]),
			   (melt_ptr_t) (( /*!PUT_ELSE_MATCH */ meltfrout->
					  tabval[5])), (MELTBPARSTR_PTR ""),
			   argtab, "", (union meltparam_un *) 0);
	  }
	  ;
	  MELT_LOCATION ("warmelt-normatch.melt:3261:/ quasiblock");


	  /*_.PROGN___V85*/ meltfptr[52] =
	    /*_.PUT_ELSE_MATCH__V84*/ meltfptr[45];;
	  /*^compute */
	  /*_.IF___V75*/ meltfptr[59] = /*_.PROGN___V85*/ meltfptr[52];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-normatch.melt:3260:/ clear");
	     /*clear *//*_.IFCPP___V76*/ meltfptr[66] = 0;
	  /*^clear */
	     /*clear *//*_.IFCPP___V80*/ meltfptr[31] = 0;
	  /*^clear */
	     /*clear *//*_.PUT_ELSE_MATCH__V84*/ meltfptr[45] = 0;
	  /*^clear */
	     /*clear *//*_.PROGN___V85*/ meltfptr[52] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

  /*_.IF___V75*/ meltfptr[59] = NULL;;
      }
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-normatch.melt:3266:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L35*/ meltfnum[18] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-normatch.melt:3266:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L35*/ meltfnum[18])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L36*/ meltfnum[15] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-normatch.melt:3266:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long =
		/*_#THE_MELTCALLCOUNT__L36*/ meltfnum[15];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-normatch.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 3266;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "putelse_matchtest end recv";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.RECV__V2*/ meltfptr[1];
	      /*_.MELT_DEBUG_FUN__V88*/ meltfptr[24] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V87*/ meltfptr[23] =
	      /*_.MELT_DEBUG_FUN__V88*/ meltfptr[24];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-normatch.melt:3266:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L36*/ meltfnum[15] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V88*/ meltfptr[24] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V87*/ meltfptr[23] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-normatch.melt:3266:/ quasiblock");


      /*_.PROGN___V89*/ meltfptr[22] = /*_.IF___V87*/ meltfptr[23];;
      /*^compute */
      /*_.IFCPP___V86*/ meltfptr[38] = /*_.PROGN___V89*/ meltfptr[22];;
      /*epilog */

      MELT_LOCATION ("warmelt-normatch.melt:3266:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L35*/ meltfnum[18] = 0;
      /*^clear */
	     /*clear *//*_.IF___V87*/ meltfptr[23] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V89*/ meltfptr[22] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V86*/ meltfptr[38] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    /*^compute */
    /*_.LET___V18*/ meltfptr[16] = /*_.IFCPP___V86*/ meltfptr[38];;

    MELT_LOCATION ("warmelt-normatch.melt:3226:/ clear");
	   /*clear *//*_.MYELSE__V19*/ meltfptr[18] = 0;
    /*^clear */
	   /*clear *//*_.MYTHEN__V20*/ meltfptr[19] = 0;
    /*^clear */
	   /*clear *//*_.ELSESTART__V21*/ meltfptr[20] = 0;
    /*^clear */
	   /*clear *//*_#__L8*/ meltfnum[1] = 0;
    /*^clear */
	   /*clear *//*_.IFELSE___V22*/ meltfptr[21] = 0;
    /*^clear */
	   /*clear *//*_.IF___V75*/ meltfptr[59] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V86*/ meltfptr[38] = 0;
    MELT_LOCATION ("warmelt-normatch.melt:3218:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.LET___V18*/ meltfptr[16];;

    {
      MELT_LOCATION ("warmelt-normatch.melt:3218:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V4*/ meltfptr[3] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V8*/ meltfptr[4] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V10*/ meltfptr[5] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V14*/ meltfptr[10] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V16*/ meltfptr[11] = 0;
    /*^clear */
	   /*clear *//*_.LET___V18*/ meltfptr[16] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("PUTELSE_MATCHTEST", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_71_warmelt_normatch_PUTELSE_MATCHTEST_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_71_warmelt_normatch_PUTELSE_MATCHTEST */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_72_warmelt_normatch_PUTELSE_MATCHGROUP (meltclosure_ptr_t meltclosp_,
						 melt_ptr_t meltfirstargp_,
						 const melt_argdescr_cell_t
						 meltxargdescr_[],
						 union meltparam_un *
						 meltxargtab_,
						 const melt_argdescr_cell_t
						 meltxresdescr_[],
						 union meltparam_un *
						 meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_72_warmelt_normatch_PUTELSE_MATCHGROUP_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_72_warmelt_normatch_PUTELSE_MATCHGROUP_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 68
    melt_ptr_t mcfr_varptr[68];
#define MELTFRAM_NBVARNUM 27
    long mcfr_varnum[27];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_72_warmelt_normatch_PUTELSE_MATCHGROUP is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_72_warmelt_normatch_PUTELSE_MATCHGROUP_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 68; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_72_warmelt_normatch_PUTELSE_MATCHGROUP nbval 68*/
  meltfram__.mcfr_nbvar = 68 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("PUTELSE_MATCHGROUP", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-normatch.melt:3272:/ getarg");
 /*_.RECV__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.ELSESTEP__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.ELSESTEP__V3*/ meltfptr[2])) !=
	      NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-normatch.melt:3273:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L1*/ meltfnum[0] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-normatch.melt:3273:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L1*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-normatch.melt:3273:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-normatch.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 3273;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "putelse_matchgroup recv";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.RECV__V2*/ meltfptr[1];
	      /*_.MELT_DEBUG_FUN__V6*/ meltfptr[5] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V5*/ meltfptr[4] = /*_.MELT_DEBUG_FUN__V6*/ meltfptr[5];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-normatch.melt:3273:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V6*/ meltfptr[5] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V5*/ meltfptr[4] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-normatch.melt:3273:/ quasiblock");


      /*_.PROGN___V7*/ meltfptr[5] = /*_.IF___V5*/ meltfptr[4];;
      /*^compute */
      /*_.IFCPP___V4*/ meltfptr[3] = /*_.PROGN___V7*/ meltfptr[5];;
      /*epilog */

      MELT_LOCATION ("warmelt-normatch.melt:3273:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L1*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IF___V5*/ meltfptr[4] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V7*/ meltfptr[5] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V4*/ meltfptr[3] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-normatch.melt:3274:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L3*/ meltfnum[1] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.RECV__V2*/ meltfptr[1]),
			     (melt_ptr_t) (( /*!CLASS_MATCH_STEP_TEST_GROUP */
					    meltfrout->tabval[1])));;
      MELT_LOCATION ("warmelt-normatch.melt:3274:/ cond");
      /*cond */ if ( /*_#IS_A__L3*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V9*/ meltfptr[5] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-normatch.melt:3274:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check recv"),
				  ("warmelt-normatch.melt")
				  ? ("warmelt-normatch.melt") : __FILE__,
				  (3274) ? (3274) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V9*/ meltfptr[5] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V8*/ meltfptr[4] = /*_.IFELSE___V9*/ meltfptr[5];;
      /*epilog */

      MELT_LOCATION ("warmelt-normatch.melt:3274:/ clear");
	     /*clear *//*_#IS_A__L3*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V9*/ meltfptr[5] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V8*/ meltfptr[4] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-normatch.melt:3275:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L4*/ meltfnum[0] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-normatch.melt:3275:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L4*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L5*/ meltfnum[1] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-normatch.melt:3275:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L5*/ meltfnum[1];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-normatch.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 3275;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "putelse_matchgroup elsestep";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.ELSESTEP__V3*/ meltfptr[2];
	      /*_.MELT_DEBUG_FUN__V12*/ meltfptr[11] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V11*/ meltfptr[10] =
	      /*_.MELT_DEBUG_FUN__V12*/ meltfptr[11];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-normatch.melt:3275:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L5*/ meltfnum[1] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V12*/ meltfptr[11] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V11*/ meltfptr[10] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-normatch.melt:3275:/ quasiblock");


      /*_.PROGN___V13*/ meltfptr[11] = /*_.IF___V11*/ meltfptr[10];;
      /*^compute */
      /*_.IFCPP___V10*/ meltfptr[5] = /*_.PROGN___V13*/ meltfptr[11];;
      /*epilog */

      MELT_LOCATION ("warmelt-normatch.melt:3275:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L4*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IF___V11*/ meltfptr[10] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V13*/ meltfptr[11] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V10*/ meltfptr[5] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-normatch.melt:3276:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L6*/ meltfnum[1] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.ELSESTEP__V3*/ meltfptr[2]),
			     (melt_ptr_t) (( /*!CLASS_MATCH_STEP */
					    meltfrout->tabval[2])));;
      MELT_LOCATION ("warmelt-normatch.melt:3276:/ cond");
      /*cond */ if ( /*_#IS_A__L6*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V15*/ meltfptr[11] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-normatch.melt:3276:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check elsestep"),
				  ("warmelt-normatch.melt")
				  ? ("warmelt-normatch.melt") : __FILE__,
				  (3276) ? (3276) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V15*/ meltfptr[11] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V14*/ meltfptr[10] = /*_.IFELSE___V15*/ meltfptr[11];;
      /*epilog */

      MELT_LOCATION ("warmelt-normatch.melt:3276:/ clear");
	     /*clear *//*_#IS_A__L6*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V15*/ meltfptr[11] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V14*/ meltfptr[10] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-normatch.melt:3277:/ quasiblock");


    /*^getslot */
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.RECV__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 8, "MSTGROUP_ELSE");
  /*_.ELSEGROUP__V17*/ meltfptr[16] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-normatch.melt:3278:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      /*_.ELSESTART__V18*/ meltfptr[17] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!START_STEP */ meltfrout->tabval[3])),
		    (melt_ptr_t) ( /*_.ELSESTEP__V3*/ meltfptr[2]), (""),
		    (union meltparam_un *) 0, "", (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-normatch.melt:3279:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.RECV__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 6, "MSTGROUP_START");
  /*_.STARTGROUP__V19*/ meltfptr[18] = slot;
    };
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-normatch.melt:3281:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L7*/ meltfnum[0] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-normatch.melt:3281:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L7*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L8*/ meltfnum[1] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-normatch.melt:3281:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L8*/ meltfnum[1];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-normatch.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 3281;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "putelse_matchgroup elsestart";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.ELSESTART__V18*/ meltfptr[17];
	      /*_.MELT_DEBUG_FUN__V22*/ meltfptr[21] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V21*/ meltfptr[20] =
	      /*_.MELT_DEBUG_FUN__V22*/ meltfptr[21];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-normatch.melt:3281:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L8*/ meltfnum[1] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V22*/ meltfptr[21] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V21*/ meltfptr[20] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-normatch.melt:3281:/ quasiblock");


      /*_.PROGN___V23*/ meltfptr[21] = /*_.IF___V21*/ meltfptr[20];;
      /*^compute */
      /*_.IFCPP___V20*/ meltfptr[19] = /*_.PROGN___V23*/ meltfptr[21];;
      /*epilog */

      MELT_LOCATION ("warmelt-normatch.melt:3281:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L7*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IF___V21*/ meltfptr[20] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V23*/ meltfptr[21] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V20*/ meltfptr[19] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-normatch.melt:3282:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^cond */
    /*cond */ if ( /*_.STARTGROUP__V19*/ meltfptr[18])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{


#if MELT_HAVE_DEBUG
	  MELT_LOCATION ("warmelt-normatch.melt:3284:/ cppif.then");
	  /*^block */
	  /*anyblock */
	  {


	    {
	      /*^locexp */
	      /*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	      melt_dbgcounter++;
#endif
	      ;
	    }
	    ;
	    /*^checksignal */
	    MELT_CHECK_SIGNAL ();
	    ;
     /*_#MELT_NEED_DBG__L9*/ meltfnum[1] =
	      /*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	      ( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	      0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	      ;;
	    MELT_LOCATION ("warmelt-normatch.melt:3284:/ cond");
	    /*cond */ if ( /*_#MELT_NEED_DBG__L9*/ meltfnum[1])	/*then */
	      {
		/*^cond.then */
		/*^block */
		/*anyblock */
		{

       /*_#THE_MELTCALLCOUNT__L10*/ meltfnum[0] =
#ifdef meltcallcount
		    meltcallcount	/* the_meltcallcount */
#else
		    0L
#endif /* meltcallcount the_meltcallcount */
		    ;;
		  MELT_LOCATION ("warmelt-normatch.melt:3284:/ checksignal");
		  MELT_CHECK_SIGNAL ();
		  ;
		  /*^apply */
		  /*apply */
		  {
		    union meltparam_un argtab[5];
		    memset (&argtab, 0, sizeof (argtab));
		    /*^apply.arg */
		    argtab[0].meltbp_long =
		      /*_#THE_MELTCALLCOUNT__L10*/ meltfnum[0];
		    /*^apply.arg */
		    argtab[1].meltbp_cstring = "warmelt-normatch.melt";
		    /*^apply.arg */
		    argtab[2].meltbp_long = 3284;
		    /*^apply.arg */
		    argtab[3].meltbp_cstring =
		      "putelse_matchgroup recursing startgroup";
		    /*^apply.arg */
		    argtab[4].meltbp_aptr =
		      (melt_ptr_t *) & /*_.STARTGROUP__V19*/ meltfptr[18];
		    /*_.MELT_DEBUG_FUN__V27*/ meltfptr[26] =
		      melt_apply ((meltclosure_ptr_t)
				  (( /*!MELT_DEBUG_FUN */ meltfrout->
				    tabval[0])),
				  (melt_ptr_t) (( /*nil */ NULL)),
				  (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_PTR ""), argtab, "",
				  (union meltparam_un *) 0);
		  }
		  ;
		  /*_.IF___V26*/ meltfptr[25] =
		    /*_.MELT_DEBUG_FUN__V27*/ meltfptr[26];;
		  /*epilog */

		  MELT_LOCATION ("warmelt-normatch.melt:3284:/ clear");
		 /*clear *//*_#THE_MELTCALLCOUNT__L10*/ meltfnum[0] = 0;
		  /*^clear */
		 /*clear *//*_.MELT_DEBUG_FUN__V27*/ meltfptr[26] = 0;
		}
		;
	      }
	    else
	      {			/*^cond.else */

      /*_.IF___V26*/ meltfptr[25] = NULL;;
	      }
	    ;
	    MELT_LOCATION ("warmelt-normatch.melt:3284:/ quasiblock");


	    /*_.PROGN___V28*/ meltfptr[26] = /*_.IF___V26*/ meltfptr[25];;
	    /*^compute */
	    /*_.IFCPP___V25*/ meltfptr[21] = /*_.PROGN___V28*/ meltfptr[26];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-normatch.melt:3284:/ clear");
	       /*clear *//*_#MELT_NEED_DBG__L9*/ meltfnum[1] = 0;
	    /*^clear */
	       /*clear *//*_.IF___V26*/ meltfptr[25] = 0;
	    /*^clear */
	       /*clear *//*_.PROGN___V28*/ meltfptr[26] = 0;
	  }

#else /*MELT_HAVE_DEBUG */
	  /*^cppif.else */
	  /*_.IFCPP___V25*/ meltfptr[21] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
	  ;
	  MELT_LOCATION ("warmelt-normatch.melt:3285:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^msend */
	  /*msend */
	  {
	    union meltparam_un argtab[1];
	    memset (&argtab, 0, sizeof (argtab));
	    /*^ojbmsend.arg */
	    argtab[0].meltbp_aptr =
	      (melt_ptr_t *) & /*_.ELSESTART__V18*/ meltfptr[17];
	    /*_.PUT_ELSE_MATCH__V29*/ meltfptr[25] =
	      meltgc_send ((melt_ptr_t) ( /*_.STARTGROUP__V19*/ meltfptr[18]),
			   (melt_ptr_t) (( /*!PUT_ELSE_MATCH */ meltfrout->
					  tabval[4])), (MELTBPARSTR_PTR ""),
			   argtab, "", (union meltparam_un *) 0);
	  }
	  ;
	  MELT_LOCATION ("warmelt-normatch.melt:3283:/ quasiblock");


	  /*_.PROGN___V30*/ meltfptr[26] =
	    /*_.PUT_ELSE_MATCH__V29*/ meltfptr[25];;
	  /*^compute */
	  /*_.IF___V24*/ meltfptr[20] = /*_.PROGN___V30*/ meltfptr[26];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-normatch.melt:3282:/ clear");
	     /*clear *//*_.IFCPP___V25*/ meltfptr[21] = 0;
	  /*^clear */
	     /*clear *//*_.PUT_ELSE_MATCH__V29*/ meltfptr[25] = 0;
	  /*^clear */
	     /*clear *//*_.PROGN___V30*/ meltfptr[26] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

  /*_.IF___V24*/ meltfptr[20] = NULL;;
      }
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-normatch.melt:3287:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L11*/ meltfnum[0] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-normatch.melt:3287:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L11*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L12*/ meltfnum[1] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-normatch.melt:3287:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long =
		/*_#THE_MELTCALLCOUNT__L12*/ meltfnum[1];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-normatch.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 3287;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "putelse_matchgroup elsegroup";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.ELSEGROUP__V17*/ meltfptr[16];
	      /*_.MELT_DEBUG_FUN__V33*/ meltfptr[26] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V32*/ meltfptr[25] =
	      /*_.MELT_DEBUG_FUN__V33*/ meltfptr[26];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-normatch.melt:3287:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L12*/ meltfnum[1] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V33*/ meltfptr[26] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V32*/ meltfptr[25] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-normatch.melt:3287:/ quasiblock");


      /*_.PROGN___V34*/ meltfptr[26] = /*_.IF___V32*/ meltfptr[25];;
      /*^compute */
      /*_.IFCPP___V31*/ meltfptr[21] = /*_.PROGN___V34*/ meltfptr[26];;
      /*epilog */

      MELT_LOCATION ("warmelt-normatch.melt:3287:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L11*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IF___V32*/ meltfptr[25] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V34*/ meltfptr[26] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V31*/ meltfptr[21] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-normatch.melt:3289:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#NULL__L13*/ meltfnum[1] =
      (( /*_.ELSEGROUP__V17*/ meltfptr[16]) == NULL);;
    MELT_LOCATION ("warmelt-normatch.melt:3289:/ cond");
    /*cond */ if ( /*_#NULL__L13*/ meltfnum[1])	/*then */
      {
	/*^cond.then */
	/*_.IFELSE___V35*/ meltfptr[25] = ( /*nil */ NULL);;
      }
    else
      {
	MELT_LOCATION ("warmelt-normatch.melt:3289:/ cond.else");

	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-normatch.melt:3291:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
   /*_#IS_A__L14*/ meltfnum[0] =
	    melt_is_instance_of ((melt_ptr_t)
				 ( /*_.ELSEGROUP__V17*/ meltfptr[16]),
				 (melt_ptr_t) (( /*!CLASS_MATCH_STEP */
						meltfrout->tabval[2])));;
	  MELT_LOCATION ("warmelt-normatch.melt:3291:/ cond");
	  /*cond */ if ( /*_#IS_A__L14*/ meltfnum[0])	/*then */
	    {
	      /*^cond.then */
	      /*^block */
	      /*anyblock */
	      {


#if MELT_HAVE_DEBUG
		MELT_LOCATION ("warmelt-normatch.melt:3292:/ cppif.then");
		/*^block */
		/*anyblock */
		{


		  {
		    /*^locexp */
		    /*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
		    melt_dbgcounter++;
#endif
		    ;
		  }
		  ;
		  /*^checksignal */
		  MELT_CHECK_SIGNAL ();
		  ;
       /*_#MELT_NEED_DBG__L15*/ meltfnum[14] =
		    /*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
		    ( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
		    0		/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
		    ;;
		  MELT_LOCATION ("warmelt-normatch.melt:3292:/ cond");
		  /*cond */ if ( /*_#MELT_NEED_DBG__L15*/ meltfnum[14])	/*then */
		    {
		      /*^cond.then */
		      /*^block */
		      /*anyblock */
		      {

	 /*_#THE_MELTCALLCOUNT__L16*/ meltfnum[15] =
#ifdef meltcallcount
			  meltcallcount	/* the_meltcallcount */
#else
			  0L
#endif /* meltcallcount the_meltcallcount */
			  ;;
			MELT_LOCATION
			  ("warmelt-normatch.melt:3292:/ checksignal");
			MELT_CHECK_SIGNAL ();
			;
			/*^apply */
			/*apply */
			{
			  union meltparam_un argtab[5];
			  memset (&argtab, 0, sizeof (argtab));
			  /*^apply.arg */
			  argtab[0].meltbp_long =
			    /*_#THE_MELTCALLCOUNT__L16*/ meltfnum[15];
			  /*^apply.arg */
			  argtab[1].meltbp_cstring = "warmelt-normatch.melt";
			  /*^apply.arg */
			  argtab[2].meltbp_long = 3292;
			  /*^apply.arg */
			  argtab[3].meltbp_cstring =
			    "putelse_matchgroup elsegroup step";
			  /*^apply.arg */
			  argtab[4].meltbp_aptr =
			    (melt_ptr_t *) & /*_.ELSEGROUP__V17*/
			    meltfptr[16];
			  /*_.MELT_DEBUG_FUN__V39*/ meltfptr[38] =
			    melt_apply ((meltclosure_ptr_t)
					(( /*!MELT_DEBUG_FUN */ meltfrout->
					  tabval[0])),
					(melt_ptr_t) (( /*nil */ NULL)),
					(MELTBPARSTR_LONG MELTBPARSTR_CSTRING
					 MELTBPARSTR_LONG MELTBPARSTR_CSTRING
					 MELTBPARSTR_PTR ""), argtab, "",
					(union meltparam_un *) 0);
			}
			;
			/*_.IF___V38*/ meltfptr[37] =
			  /*_.MELT_DEBUG_FUN__V39*/ meltfptr[38];;
			/*epilog */

			MELT_LOCATION ("warmelt-normatch.melt:3292:/ clear");
		   /*clear *//*_#THE_MELTCALLCOUNT__L16*/ meltfnum[15] =
			  0;
			/*^clear */
		   /*clear *//*_.MELT_DEBUG_FUN__V39*/ meltfptr[38] = 0;
		      }
		      ;
		    }
		  else
		    {		/*^cond.else */

	/*_.IF___V38*/ meltfptr[37] = NULL;;
		    }
		  ;
		  MELT_LOCATION ("warmelt-normatch.melt:3292:/ quasiblock");


		  /*_.PROGN___V40*/ meltfptr[38] =
		    /*_.IF___V38*/ meltfptr[37];;
		  /*^compute */
		  /*_.IFCPP___V37*/ meltfptr[36] =
		    /*_.PROGN___V40*/ meltfptr[38];;
		  /*epilog */

		  MELT_LOCATION ("warmelt-normatch.melt:3292:/ clear");
		 /*clear *//*_#MELT_NEED_DBG__L15*/ meltfnum[14] = 0;
		  /*^clear */
		 /*clear *//*_.IF___V38*/ meltfptr[37] = 0;
		  /*^clear */
		 /*clear *//*_.PROGN___V40*/ meltfptr[38] = 0;
		}

#else /*MELT_HAVE_DEBUG */
		/*^cppif.else */
		/*_.IFCPP___V37*/ meltfptr[36] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
		;
		MELT_LOCATION ("warmelt-normatch.melt:3293:/ checksignal");
		MELT_CHECK_SIGNAL ();
		;
		/*^msend */
		/*msend */
		{
		  union meltparam_un argtab[1];
		  memset (&argtab, 0, sizeof (argtab));
		  /*^ojbmsend.arg */
		  argtab[0].meltbp_aptr =
		    (melt_ptr_t *) & /*_.ELSESTART__V18*/ meltfptr[17];
		  /*_.PUT_ELSE_MATCH__V41*/ meltfptr[37] =
		    meltgc_send ((melt_ptr_t)
				 ( /*_.ELSEGROUP__V17*/ meltfptr[16]),
				 (melt_ptr_t) (( /*!PUT_ELSE_MATCH */
						meltfrout->tabval[4])),
				 (MELTBPARSTR_PTR ""), argtab, "",
				 (union meltparam_un *) 0);
		}
		;
		MELT_LOCATION ("warmelt-normatch.melt:3291:/ quasiblock");


		/*_.PROGN___V42*/ meltfptr[38] =
		  /*_.PUT_ELSE_MATCH__V41*/ meltfptr[37];;
		/*^compute */
		/*_.IFELSE___V36*/ meltfptr[26] =
		  /*_.PROGN___V42*/ meltfptr[38];;
		/*epilog */

		MELT_LOCATION ("warmelt-normatch.melt:3291:/ clear");
	       /*clear *//*_.IFCPP___V37*/ meltfptr[36] = 0;
		/*^clear */
	       /*clear *//*_.PUT_ELSE_MATCH__V41*/ meltfptr[37] = 0;
		/*^clear */
	       /*clear *//*_.PROGN___V42*/ meltfptr[38] = 0;
	      }
	      ;
	    }
	  else
	    {			/*^cond.else */

	      /*^block */
	      /*anyblock */
	      {

		MELT_LOCATION ("warmelt-normatch.melt:3294:/ checksignal");
		MELT_CHECK_SIGNAL ();
		;
     /*_#IS_LIST__L17*/ meltfnum[15] =
		  (melt_magic_discr
		   ((melt_ptr_t) ( /*_.ELSEGROUP__V17*/ meltfptr[16])) ==
		   MELTOBMAG_LIST);;
		MELT_LOCATION ("warmelt-normatch.melt:3294:/ cond");
		/*cond */ if ( /*_#IS_LIST__L17*/ meltfnum[15])	/*then */
		  {
		    /*^cond.then */
		    /*^block */
		    /*anyblock */
		    {

		      /*citerblock FOREACH_IN_LIST */
		      {
			/* start foreach_in_list meltcit1__EACHLIST */
			for ( /*_.CURPAIR__V44*/ meltfptr[37] =
			     melt_list_first ((melt_ptr_t)
					      /*_.ELSEGROUP__V17*/
					      meltfptr[16]);
			     melt_magic_discr ((melt_ptr_t) /*_.CURPAIR__V44*/
					       meltfptr[37]) ==
			     MELTOBMAG_PAIR;
			     /*_.CURPAIR__V44*/ meltfptr[37] =
			     melt_pair_tail ((melt_ptr_t) /*_.CURPAIR__V44*/
					     meltfptr[37]))
			  {
			    /*_.CURELSE__V45*/ meltfptr[38] =
			      melt_pair_head ((melt_ptr_t) /*_.CURPAIR__V44*/
					      meltfptr[37]);



#if MELT_HAVE_DEBUG
			    MELT_LOCATION
			      ("warmelt-normatch.melt:3298:/ cppif.then");
			    /*^block */
			    /*anyblock */
			    {


			      {
				/*^locexp */
				/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
				melt_dbgcounter++;
#endif
				;
			      }
			      ;
			      /*^checksignal */
			      MELT_CHECK_SIGNAL ();
			      ;
	  /*_#MELT_NEED_DBG__L18*/ meltfnum[14] =
				/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
				( /*melt_need_dbg */
				 melt_need_debug ((int) 0))
#else
				0	/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
				;;
			      MELT_LOCATION
				("warmelt-normatch.melt:3298:/ cond");
			      /*cond */ if ( /*_#MELT_NEED_DBG__L18*/ meltfnum[14])	/*then */
				{
				  /*^cond.then */
				  /*^block */
				  /*anyblock */
				  {

	    /*_#THE_MELTCALLCOUNT__L19*/ meltfnum[18]
				      =
#ifdef meltcallcount
				      meltcallcount	/* the_meltcallcount */
#else
				      0L
#endif /* meltcallcount the_meltcallcount */
				      ;;
				    MELT_LOCATION
				      ("warmelt-normatch.melt:3298:/ checksignal");
				    MELT_CHECK_SIGNAL ();
				    ;
				    /*^apply */
				    /*apply */
				    {
				      union meltparam_un argtab[5];
				      memset (&argtab, 0, sizeof (argtab));
				      /*^apply.arg */
				      argtab[0].meltbp_long =
					/*_#THE_MELTCALLCOUNT__L19*/
					meltfnum[18];
				      /*^apply.arg */
				      argtab[1].meltbp_cstring =
					"warmelt-normatch.melt";
				      /*^apply.arg */
				      argtab[2].meltbp_long = 3298;
				      /*^apply.arg */
				      argtab[3].meltbp_cstring =
					"putelse_matchgroup curelse from list";
				      /*^apply.arg */
				      argtab[4].meltbp_aptr =
					(melt_ptr_t *) & /*_.CURELSE__V45*/
					meltfptr[38];
				      /*_.MELT_DEBUG_FUN__V48*/ meltfptr[47] =
					melt_apply ((meltclosure_ptr_t)
						    (( /*!MELT_DEBUG_FUN */
						      meltfrout->tabval[0])),
						    (melt_ptr_t) (( /*nil */
								   NULL)),
						    (MELTBPARSTR_LONG
						     MELTBPARSTR_CSTRING
						     MELTBPARSTR_LONG
						     MELTBPARSTR_CSTRING
						     MELTBPARSTR_PTR ""),
						    argtab, "",
						    (union meltparam_un *) 0);
				    }
				    ;
				    /*_.IF___V47*/ meltfptr[46] =
				      /*_.MELT_DEBUG_FUN__V48*/ meltfptr[47];;
				    /*epilog */

				    MELT_LOCATION
				      ("warmelt-normatch.melt:3298:/ clear");
		      /*clear *//*_#THE_MELTCALLCOUNT__L19*/
				      meltfnum[18] = 0;
				    /*^clear */
		      /*clear *//*_.MELT_DEBUG_FUN__V48*/
				      meltfptr[47] = 0;
				  }
				  ;
				}
			      else
				{	/*^cond.else */

	   /*_.IF___V47*/ meltfptr[46] = NULL;;
				}
			      ;
			      MELT_LOCATION
				("warmelt-normatch.melt:3298:/ quasiblock");


			      /*_.PROGN___V49*/ meltfptr[47] =
				/*_.IF___V47*/ meltfptr[46];;
			      /*^compute */
			      /*_.IFCPP___V46*/ meltfptr[45] =
				/*_.PROGN___V49*/ meltfptr[47];;
			      /*epilog */

			      MELT_LOCATION
				("warmelt-normatch.melt:3298:/ clear");
		    /*clear *//*_#MELT_NEED_DBG__L18*/ meltfnum[14]
				= 0;
			      /*^clear */
		    /*clear *//*_.IF___V47*/ meltfptr[46] = 0;
			      /*^clear */
		    /*clear *//*_.PROGN___V49*/ meltfptr[47] = 0;
			    }

#else /*MELT_HAVE_DEBUG */
			    /*^cppif.else */
			    /*_.IFCPP___V46*/ meltfptr[45] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
			    ;
			    MELT_LOCATION
			      ("warmelt-normatch.melt:3299:/ checksignal");
			    MELT_CHECK_SIGNAL ();
			    ;
			    /*^msend */
			    /*msend */
			    {
			      union meltparam_un argtab[1];
			      memset (&argtab, 0, sizeof (argtab));
			      /*^ojbmsend.arg */
			      argtab[0].meltbp_aptr =
				(melt_ptr_t *) & /*_.ELSESTART__V18*/
				meltfptr[17];
			      /*_.PUT_ELSE_MATCH__V50*/ meltfptr[46] =
				meltgc_send ((melt_ptr_t)
					     ( /*_.CURELSE__V45*/
					      meltfptr[38]),
					     (melt_ptr_t) (( /*!PUT_ELSE_MATCH */ meltfrout->tabval[4])), (MELTBPARSTR_PTR ""), argtab, "", (union meltparam_un *) 0);
			    }
			    ;
			    /*_.IFELSE___V43*/ meltfptr[36] =
			      /*_.PUT_ELSE_MATCH__V50*/ meltfptr[46];;
			  }	/* end foreach_in_list meltcit1__EACHLIST */
     /*_.CURPAIR__V44*/ meltfptr[37] = NULL;
     /*_.CURELSE__V45*/ meltfptr[38] = NULL;


			/*citerepilog */

			MELT_LOCATION ("warmelt-normatch.melt:3295:/ clear");
		  /*clear *//*_.CURPAIR__V44*/ meltfptr[37] = 0;
			/*^clear */
		  /*clear *//*_.CURELSE__V45*/ meltfptr[38] = 0;
			/*^clear */
		  /*clear *//*_.IFCPP___V46*/ meltfptr[45] = 0;
			/*^clear */
		  /*clear *//*_.PUT_ELSE_MATCH__V50*/ meltfptr[46] = 0;
		      }		/*endciterblock FOREACH_IN_LIST */
		      ;
		      /*epilog */
		    }
		    ;
		  }
		else
		  {
		    MELT_LOCATION ("warmelt-normatch.melt:3294:/ cond.else");

		    /*^block */
		    /*anyblock */
		    {

		      MELT_LOCATION
			("warmelt-normatch.melt:3300:/ checksignal");
		      MELT_CHECK_SIGNAL ();
		      ;
       /*_#IS_MULTIPLE__L20*/ meltfnum[18] =
			(melt_magic_discr
			 ((melt_ptr_t) ( /*_.ELSEGROUP__V17*/ meltfptr[16]))
			 == MELTOBMAG_MULTIPLE);;
		      MELT_LOCATION ("warmelt-normatch.melt:3300:/ cond");
		      /*cond */ if ( /*_#IS_MULTIPLE__L20*/ meltfnum[18])	/*then */
			{
			  /*^cond.then */
			  /*^block */
			  /*anyblock */
			  {

			    /*citerblock FOREACH_IN_MULTIPLE */
			    {
			      /* start foreach_in_multiple meltcit2__EACHTUP */
			      long meltcit2__EACHTUP_ln =
				melt_multiple_length ((melt_ptr_t)
						      /*_.ELSEGROUP__V17*/
						      meltfptr[16]);
			      for ( /*_#THIX__L21*/ meltfnum[14] = 0;
				   ( /*_#THIX__L21*/ meltfnum[14] >= 0)
				   && ( /*_#THIX__L21*/ meltfnum[14] <
				       meltcit2__EACHTUP_ln);
	/*_#THIX__L21*/ meltfnum[14]++)
				{
				  /*_.CURELSE__V52*/ meltfptr[51] =
				    melt_multiple_nth ((melt_ptr_t)
						       ( /*_.ELSEGROUP__V17*/
							meltfptr[16]),
						       /*_#THIX__L21*/
						       meltfnum[14]);




#if MELT_HAVE_DEBUG
				  MELT_LOCATION
				    ("warmelt-normatch.melt:3304:/ cppif.then");
				  /*^block */
				  /*anyblock */
				  {


				    {
				      /*^locexp */
				      /*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
				      melt_dbgcounter++;
#endif
				      ;
				    }
				    ;
				    /*^checksignal */
				    MELT_CHECK_SIGNAL ();
				    ;
	    /*_#MELT_NEED_DBG__L22*/ meltfnum[21] =
				      /*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
				      ( /*melt_need_dbg */
				       melt_need_debug ((int) 0))
#else
				      0	/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
				      ;;
				    MELT_LOCATION
				      ("warmelt-normatch.melt:3304:/ cond");
				    /*cond */ if ( /*_#MELT_NEED_DBG__L22*/ meltfnum[21])	/*then */
				      {
					/*^cond.then */
					/*^block */
					/*anyblock */
					{

	      /*_#THE_MELTCALLCOUNT__L23*/
					    meltfnum[22] =
#ifdef meltcallcount
					    meltcallcount	/* the_meltcallcount */
#else
					    0L
#endif /* meltcallcount the_meltcallcount */
					    ;;
					  MELT_LOCATION
					    ("warmelt-normatch.melt:3304:/ checksignal");
					  MELT_CHECK_SIGNAL ();
					  ;
					  /*^apply */
					  /*apply */
					  {
					    union meltparam_un argtab[5];
					    memset (&argtab, 0,
						    sizeof (argtab));
					    /*^apply.arg */
					    argtab[0].meltbp_long =
					      /*_#THE_MELTCALLCOUNT__L23*/
					      meltfnum[22];
					    /*^apply.arg */
					    argtab[1].meltbp_cstring =
					      "warmelt-normatch.melt";
					    /*^apply.arg */
					    argtab[2].meltbp_long = 3304;
					    /*^apply.arg */
					    argtab[3].meltbp_cstring =
					      "putelse_matchgroup curelse from tuple";
					    /*^apply.arg */
					    argtab[4].meltbp_aptr =
					      (melt_ptr_t *) &
					      /*_.CURELSE__V52*/ meltfptr[51];
					    /*_.MELT_DEBUG_FUN__V55*/
					      meltfptr[54] =
					      melt_apply ((meltclosure_ptr_t)
							  (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])), (melt_ptr_t) (( /*nil */ NULL)), (MELTBPARSTR_LONG MELTBPARSTR_CSTRING MELTBPARSTR_LONG MELTBPARSTR_CSTRING MELTBPARSTR_PTR ""), argtab, "", (union meltparam_un *) 0);
					  }
					  ;
					  /*_.IF___V54*/ meltfptr[53] =
					    /*_.MELT_DEBUG_FUN__V55*/
					    meltfptr[54];;
					  /*epilog */

					  MELT_LOCATION
					    ("warmelt-normatch.melt:3304:/ clear");
			/*clear *//*_#THE_MELTCALLCOUNT__L23*/
					    meltfnum[22] = 0;
					  /*^clear */
			/*clear *//*_.MELT_DEBUG_FUN__V55*/
					    meltfptr[54] = 0;
					}
					;
				      }
				    else
				      {	/*^cond.else */

	     /*_.IF___V54*/ meltfptr[53] = NULL;;
				      }
				    ;
				    MELT_LOCATION
				      ("warmelt-normatch.melt:3304:/ quasiblock");


				    /*_.PROGN___V56*/ meltfptr[54] =
				      /*_.IF___V54*/ meltfptr[53];;
				    /*^compute */
				    /*_.IFCPP___V53*/ meltfptr[52] =
				      /*_.PROGN___V56*/ meltfptr[54];;
				    /*epilog */

				    MELT_LOCATION
				      ("warmelt-normatch.melt:3304:/ clear");
		      /*clear *//*_#MELT_NEED_DBG__L22*/
				      meltfnum[21] = 0;
				    /*^clear */
		      /*clear *//*_.IF___V54*/ meltfptr[53] = 0;
				    /*^clear */
		      /*clear *//*_.PROGN___V56*/ meltfptr[54] =
				      0;
				  }

#else /*MELT_HAVE_DEBUG */
				  /*^cppif.else */
				  /*_.IFCPP___V53*/ meltfptr[52] =
				    ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
				  ;
				  MELT_LOCATION
				    ("warmelt-normatch.melt:3305:/ checksignal");
				  MELT_CHECK_SIGNAL ();
				  ;
				  /*^msend */
				  /*msend */
				  {
				    union meltparam_un argtab[1];
				    memset (&argtab, 0, sizeof (argtab));
				    /*^ojbmsend.arg */
				    argtab[0].meltbp_aptr =
				      (melt_ptr_t *) & /*_.ELSESTART__V18*/
				      meltfptr[17];
				    /*_.PUT_ELSE_MATCH__V57*/ meltfptr[53] =
				      meltgc_send ((melt_ptr_t)
						   ( /*_.CURELSE__V52*/
						    meltfptr[51]),
						   (melt_ptr_t) (( /*!PUT_ELSE_MATCH */ meltfrout->tabval[4])), (MELTBPARSTR_PTR ""), argtab, "", (union meltparam_un *) 0);
				  }
				  ;
				  /*_.IFELSE___V51*/ meltfptr[47] =
				    /*_.PUT_ELSE_MATCH__V57*/ meltfptr[53];;
				  if ( /*_#THIX__L21*/ meltfnum[14] < 0)
				    break;
				}	/* end  foreach_in_multiple meltcit2__EACHTUP */

			      /*citerepilog */

			      MELT_LOCATION
				("warmelt-normatch.melt:3301:/ clear");
		    /*clear *//*_.CURELSE__V52*/ meltfptr[51] = 0;
			      /*^clear */
		    /*clear *//*_#THIX__L21*/ meltfnum[14] = 0;
			      /*^clear */
		    /*clear *//*_.IFCPP___V53*/ meltfptr[52] = 0;
			      /*^clear */
		    /*clear *//*_.PUT_ELSE_MATCH__V57*/ meltfptr[53]
				= 0;
			    }	/*endciterblock FOREACH_IN_MULTIPLE */
			    ;
			    /*epilog */
			  }
			  ;
			}
		      else
			{
			  MELT_LOCATION
			    ("warmelt-normatch.melt:3300:/ cond.else");

			  /*^block */
			  /*anyblock */
			  {


#if MELT_HAVE_DEBUG
			    MELT_LOCATION
			      ("warmelt-normatch.melt:3307:/ cppif.then");
			    /*^block */
			    /*anyblock */
			    {


			      {
				/*^locexp */
				/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
				melt_dbgcounter++;
#endif
				;
			      }
			      ;
			      /*^checksignal */
			      MELT_CHECK_SIGNAL ();
			      ;
	   /*_#MELT_NEED_DBG__L24*/ meltfnum[22] =
				/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
				( /*melt_need_dbg */
				 melt_need_debug ((int) 0))
#else
				0	/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
				;;
			      MELT_LOCATION
				("warmelt-normatch.melt:3307:/ cond");
			      /*cond */ if ( /*_#MELT_NEED_DBG__L24*/ meltfnum[22])	/*then */
				{
				  /*^cond.then */
				  /*^block */
				  /*anyblock */
				  {

	     /*_#THE_MELTCALLCOUNT__L25*/ meltfnum[21]
				      =
#ifdef meltcallcount
				      meltcallcount	/* the_meltcallcount */
#else
				      0L
#endif /* meltcallcount the_meltcallcount */
				      ;;
				    MELT_LOCATION
				      ("warmelt-normatch.melt:3307:/ checksignal");
				    MELT_CHECK_SIGNAL ();
				    ;
				    /*^apply */
				    /*apply */
				    {
				      union meltparam_un argtab[5];
				      memset (&argtab, 0, sizeof (argtab));
				      /*^apply.arg */
				      argtab[0].meltbp_long =
					/*_#THE_MELTCALLCOUNT__L25*/
					meltfnum[21];
				      /*^apply.arg */
				      argtab[1].meltbp_cstring =
					"warmelt-normatch.melt";
				      /*^apply.arg */
				      argtab[2].meltbp_long = 3307;
				      /*^apply.arg */
				      argtab[3].meltbp_cstring =
					"putelse_matchgroup bad elsegroup";
				      /*^apply.arg */
				      argtab[4].meltbp_aptr =
					(melt_ptr_t *) & /*_.ELSEGROUP__V17*/
					meltfptr[16];
				      /*_.MELT_DEBUG_FUN__V60*/ meltfptr[59] =
					melt_apply ((meltclosure_ptr_t)
						    (( /*!MELT_DEBUG_FUN */
						      meltfrout->tabval[0])),
						    (melt_ptr_t) (( /*nil */
								   NULL)),
						    (MELTBPARSTR_LONG
						     MELTBPARSTR_CSTRING
						     MELTBPARSTR_LONG
						     MELTBPARSTR_CSTRING
						     MELTBPARSTR_PTR ""),
						    argtab, "",
						    (union meltparam_un *) 0);
				    }
				    ;
				    /*_.IF___V59*/ meltfptr[58] =
				      /*_.MELT_DEBUG_FUN__V60*/ meltfptr[59];;
				    /*epilog */

				    MELT_LOCATION
				      ("warmelt-normatch.melt:3307:/ clear");
		       /*clear *//*_#THE_MELTCALLCOUNT__L25*/
				      meltfnum[21] = 0;
				    /*^clear */
		       /*clear *//*_.MELT_DEBUG_FUN__V60*/
				      meltfptr[59] = 0;
				  }
				  ;
				}
			      else
				{	/*^cond.else */

	    /*_.IF___V59*/ meltfptr[58] = NULL;;
				}
			      ;
			      MELT_LOCATION
				("warmelt-normatch.melt:3307:/ quasiblock");


			      /*_.PROGN___V61*/ meltfptr[59] =
				/*_.IF___V59*/ meltfptr[58];;
			      /*^compute */
			      /*_.IFCPP___V58*/ meltfptr[54] =
				/*_.PROGN___V61*/ meltfptr[59];;
			      /*epilog */

			      MELT_LOCATION
				("warmelt-normatch.melt:3307:/ clear");
		     /*clear *//*_#MELT_NEED_DBG__L24*/ meltfnum[22]
				= 0;
			      /*^clear */
		     /*clear *//*_.IF___V59*/ meltfptr[58] = 0;
			      /*^clear */
		     /*clear *//*_.PROGN___V61*/ meltfptr[59] = 0;
			    }

#else /*MELT_HAVE_DEBUG */
			    /*^cppif.else */
			    /*_.IFCPP___V58*/ meltfptr[54] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
			    ;

#if MELT_HAVE_DEBUG
			    MELT_LOCATION
			      ("warmelt-normatch.melt:3308:/ cppif.then");
			    /*^block */
			    /*anyblock */
			    {

			      /*^checksignal */
			      MELT_CHECK_SIGNAL ();
			      ;
			      /*^cond */
			      /*cond */ if (( /*nil */ NULL))	/*then */
				{
				  /*^cond.then */
				  /*_.IFELSE___V63*/ meltfptr[59] =
				    ( /*nil */ NULL);;
				}
			      else
				{
				  MELT_LOCATION
				    ("warmelt-normatch.melt:3308:/ cond.else");

				  /*^block */
				  /*anyblock */
				  {




				    {
				      /*^locexp */
				      melt_assert_failed (("bad elsegroup"),
							  ("warmelt-normatch.melt")
							  ?
							  ("warmelt-normatch.melt")
							  : __FILE__,
							  (3308) ? (3308) :
							  __LINE__,
							  __FUNCTION__);
				      ;
				    }
				    ;
		       /*clear *//*_.IFELSE___V63*/ meltfptr[59]
				      = 0;
				    /*epilog */
				  }
				  ;
				}
			      ;
			      /*^compute */
			      /*_.IFCPP___V62*/ meltfptr[58] =
				/*_.IFELSE___V63*/ meltfptr[59];;
			      /*epilog */

			      MELT_LOCATION
				("warmelt-normatch.melt:3308:/ clear");
		     /*clear *//*_.IFELSE___V63*/ meltfptr[59] = 0;
			    }

#else /*MELT_HAVE_DEBUG */
			    /*^cppif.else */
			    /*_.IFCPP___V62*/ meltfptr[58] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
			    ;
			    MELT_LOCATION
			      ("warmelt-normatch.melt:3306:/ quasiblock");


			    /*_.PROGN___V64*/ meltfptr[59] =
			      /*_.IFCPP___V62*/ meltfptr[58];;
			    /*^compute */
			    /*_.IFELSE___V51*/ meltfptr[47] =
			      /*_.PROGN___V64*/ meltfptr[59];;
			    /*epilog */

			    MELT_LOCATION
			      ("warmelt-normatch.melt:3300:/ clear");
		   /*clear *//*_.IFCPP___V58*/ meltfptr[54] = 0;
			    /*^clear */
		   /*clear *//*_.IFCPP___V62*/ meltfptr[58] = 0;
			    /*^clear */
		   /*clear *//*_.PROGN___V64*/ meltfptr[59] = 0;
			  }
			  ;
			}
		      ;
		      /*_.IFELSE___V43*/ meltfptr[36] =
			/*_.IFELSE___V51*/ meltfptr[47];;
		      /*epilog */

		      MELT_LOCATION ("warmelt-normatch.melt:3294:/ clear");
		 /*clear *//*_#IS_MULTIPLE__L20*/ meltfnum[18] = 0;
		      /*^clear */
		 /*clear *//*_.IFELSE___V51*/ meltfptr[47] = 0;
		    }
		    ;
		  }
		;
		/*_.IFELSE___V36*/ meltfptr[26] =
		  /*_.IFELSE___V43*/ meltfptr[36];;
		/*epilog */

		MELT_LOCATION ("warmelt-normatch.melt:3291:/ clear");
	       /*clear *//*_#IS_LIST__L17*/ meltfnum[15] = 0;
		/*^clear */
	       /*clear *//*_.IFELSE___V43*/ meltfptr[36] = 0;
	      }
	      ;
	    }
	  ;
	  /*_.IFELSE___V35*/ meltfptr[25] = /*_.IFELSE___V36*/ meltfptr[26];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-normatch.melt:3289:/ clear");
	     /*clear *//*_#IS_A__L14*/ meltfnum[0] = 0;
	  /*^clear */
	     /*clear *//*_.IFELSE___V36*/ meltfptr[26] = 0;
	}
	;
      }
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-normatch.melt:3309:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L26*/ meltfnum[21] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-normatch.melt:3309:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L26*/ meltfnum[21])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L27*/ meltfnum[22] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-normatch.melt:3309:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long =
		/*_#THE_MELTCALLCOUNT__L27*/ meltfnum[22];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-normatch.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 3309;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "putelse_matchgroup end recv";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.RECV__V2*/ meltfptr[1];
	      /*_.MELT_DEBUG_FUN__V67*/ meltfptr[59] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V66*/ meltfptr[58] =
	      /*_.MELT_DEBUG_FUN__V67*/ meltfptr[59];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-normatch.melt:3309:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L27*/ meltfnum[22] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V67*/ meltfptr[59] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V66*/ meltfptr[58] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-normatch.melt:3309:/ quasiblock");


      /*_.PROGN___V68*/ meltfptr[47] = /*_.IF___V66*/ meltfptr[58];;
      /*^compute */
      /*_.IFCPP___V65*/ meltfptr[54] = /*_.PROGN___V68*/ meltfptr[47];;
      /*epilog */

      MELT_LOCATION ("warmelt-normatch.melt:3309:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L26*/ meltfnum[21] = 0;
      /*^clear */
	     /*clear *//*_.IF___V66*/ meltfptr[58] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V68*/ meltfptr[47] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V65*/ meltfptr[54] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    /*^compute */
    /*_.LET___V16*/ meltfptr[11] = /*_.IFCPP___V65*/ meltfptr[54];;

    MELT_LOCATION ("warmelt-normatch.melt:3277:/ clear");
	   /*clear *//*_.ELSEGROUP__V17*/ meltfptr[16] = 0;
    /*^clear */
	   /*clear *//*_.ELSESTART__V18*/ meltfptr[17] = 0;
    /*^clear */
	   /*clear *//*_.STARTGROUP__V19*/ meltfptr[18] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V20*/ meltfptr[19] = 0;
    /*^clear */
	   /*clear *//*_.IF___V24*/ meltfptr[20] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V31*/ meltfptr[21] = 0;
    /*^clear */
	   /*clear *//*_#NULL__L13*/ meltfnum[1] = 0;
    /*^clear */
	   /*clear *//*_.IFELSE___V35*/ meltfptr[25] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V65*/ meltfptr[54] = 0;
    MELT_LOCATION ("warmelt-normatch.melt:3272:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.LET___V16*/ meltfptr[11];;

    {
      MELT_LOCATION ("warmelt-normatch.melt:3272:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V4*/ meltfptr[3] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V8*/ meltfptr[4] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V10*/ meltfptr[5] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V14*/ meltfptr[10] = 0;
    /*^clear */
	   /*clear *//*_.LET___V16*/ meltfptr[11] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("PUTELSE_MATCHGROUP", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_72_warmelt_normatch_PUTELSE_MATCHGROUP_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_72_warmelt_normatch_PUTELSE_MATCHGROUP */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_73_warmelt_normatch_SCANSTEPDATA_TESTINS (meltclosure_ptr_t
						   meltclosp_,
						   melt_ptr_t meltfirstargp_,
						   const melt_argdescr_cell_t
						   meltxargdescr_[],
						   union meltparam_un *
						   meltxargtab_,
						   const melt_argdescr_cell_t
						   meltxresdescr_[],
						   union meltparam_un *
						   meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_73_warmelt_normatch_SCANSTEPDATA_TESTINS_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_73_warmelt_normatch_SCANSTEPDATA_TESTINS_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 14
    melt_ptr_t mcfr_varptr[14];
#define MELTFRAM_NBVARNUM 4
    long mcfr_varnum[4];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_73_warmelt_normatch_SCANSTEPDATA_TESTINS is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_73_warmelt_normatch_SCANSTEPDATA_TESTINS_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 14; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_73_warmelt_normatch_SCANSTEPDATA_TESTINS nbval 14*/
  meltfram__.mcfr_nbvar = 14 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("SCANSTEPDATA_TESTINS", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-normatch.melt:3315:/ getarg");
 /*_.STEP__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.FUN__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.FUN__V3*/ meltfptr[2])) != NULL);


  /*getarg#2 */
  /*^getarg */
  if (meltxargdescr_[1] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.CTX__V4*/ meltfptr[3] =
    (meltxargtab_[1].meltbp_aptr) ? (*(meltxargtab_[1].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.CTX__V4*/ meltfptr[3])) != NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-normatch.melt:3316:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L1*/ meltfnum[0] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.STEP__V2*/ meltfptr[1]),
			     (melt_ptr_t) (( /*!CLASS_MATCH_STEP_TEST_INSTANCE */ meltfrout->tabval[0])));;
      MELT_LOCATION ("warmelt-normatch.melt:3316:/ cond");
      /*cond */ if ( /*_#IS_A__L1*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V6*/ meltfptr[5] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-normatch.melt:3316:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check step"),
				  ("warmelt-normatch.melt")
				  ? ("warmelt-normatch.melt") : __FILE__,
				  (3316) ? (3316) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V6*/ meltfptr[5] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V5*/ meltfptr[4] = /*_.IFELSE___V6*/ meltfptr[5];;
      /*epilog */

      MELT_LOCATION ("warmelt-normatch.melt:3316:/ clear");
	     /*clear *//*_#IS_A__L1*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V6*/ meltfptr[5] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V5*/ meltfptr[4] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-normatch.melt:3317:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L2*/ meltfnum[0] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-normatch.melt:3317:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L2*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L3*/ meltfnum[2] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-normatch.melt:3317:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[7];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L3*/ meltfnum[2];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-normatch.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 3317;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "scanstepdata_testins step=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.STEP__V2*/ meltfptr[1];
	      /*^apply.arg */
	      argtab[5].meltbp_cstring = " fun=";
	      /*^apply.arg */
	      argtab[6].meltbp_aptr =
		(melt_ptr_t *) & /*_.FUN__V3*/ meltfptr[2];
	      /*_.MELT_DEBUG_FUN__V9*/ meltfptr[8] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[1])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V8*/ meltfptr[7] = /*_.MELT_DEBUG_FUN__V9*/ meltfptr[8];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-normatch.melt:3317:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L3*/ meltfnum[2] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V9*/ meltfptr[8] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V8*/ meltfptr[7] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-normatch.melt:3317:/ quasiblock");


      /*_.PROGN___V10*/ meltfptr[8] = /*_.IF___V8*/ meltfptr[7];;
      /*^compute */
      /*_.IFCPP___V7*/ meltfptr[5] = /*_.PROGN___V10*/ meltfptr[8];;
      /*epilog */

      MELT_LOCATION ("warmelt-normatch.melt:3317:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L2*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IF___V8*/ meltfptr[7] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V10*/ meltfptr[8] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V7*/ meltfptr[5] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-normatch.melt:3318:/ quasiblock");


    /*^getslot */
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.STEP__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 7, "MSTINS_SLOTS");
  /*_.SLOTUP__V11*/ meltfptr[7] = slot;
    };
    ;
    /*citerblock FOREACH_IN_MULTIPLE */
    {
      /* start foreach_in_multiple meltcit1__EACHTUP */
      long meltcit1__EACHTUP_ln =
	melt_multiple_length ((melt_ptr_t) /*_.SLOTUP__V11*/ meltfptr[7]);
      for ( /*_#IX__L4*/ meltfnum[2] = 0;
	   ( /*_#IX__L4*/ meltfnum[2] >= 0)
	   && ( /*_#IX__L4*/ meltfnum[2] < meltcit1__EACHTUP_ln);
	/*_#IX__L4*/ meltfnum[2]++)
	{
	  /*_.CURSLOT__V12*/ meltfptr[8] =
	    melt_multiple_nth ((melt_ptr_t) ( /*_.SLOTUP__V11*/ meltfptr[7]),
			       /*_#IX__L4*/ meltfnum[2]);



	  MELT_LOCATION ("warmelt-normatch.melt:3323:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^cond */
	  /*cond */ if ( /*_.CURSLOT__V12*/ meltfptr[8])	/*then */
	    {
	      /*^cond.then */
	      /*^block */
	      /*anyblock */
	      {

		MELT_LOCATION ("warmelt-normatch.melt:3324:/ checksignal");
		MELT_CHECK_SIGNAL ();
		;
		/*^apply */
		/*apply */
		{
		  union meltparam_un argtab[1];
		  memset (&argtab, 0, sizeof (argtab));
		  /*^apply.arg */
		  argtab[0].meltbp_aptr =
		    (melt_ptr_t *) & /*_.CTX__V4*/ meltfptr[3];
		  /*_.FUN__V14*/ meltfptr[13] =
		    melt_apply ((meltclosure_ptr_t)
				( /*_.FUN__V3*/ meltfptr[2]),
				(melt_ptr_t) ( /*_.CURSLOT__V12*/
					      meltfptr[8]),
				(MELTBPARSTR_PTR ""), argtab, "",
				(union meltparam_un *) 0);
		}
		;
		/*_.IF___V13*/ meltfptr[12] = /*_.FUN__V14*/ meltfptr[13];;
		/*epilog */

		MELT_LOCATION ("warmelt-normatch.melt:3323:/ clear");
	      /*clear *//*_.FUN__V14*/ meltfptr[13] = 0;
	      }
	      ;
	    }
	  else
	    {			/*^cond.else */

   /*_.IF___V13*/ meltfptr[12] = NULL;;
	    }
	  ;
	  if ( /*_#IX__L4*/ meltfnum[2] < 0)
	    break;
	}			/* end  foreach_in_multiple meltcit1__EACHTUP */

      /*citerepilog */

      MELT_LOCATION ("warmelt-normatch.melt:3320:/ clear");
	    /*clear *//*_.CURSLOT__V12*/ meltfptr[8] = 0;
      /*^clear */
	    /*clear *//*_#IX__L4*/ meltfnum[2] = 0;
      /*^clear */
	    /*clear *//*_.IF___V13*/ meltfptr[12] = 0;
    }				/*endciterblock FOREACH_IN_MULTIPLE */
    ;

    MELT_LOCATION ("warmelt-normatch.melt:3318:/ clear");
	   /*clear *//*_.SLOTUP__V11*/ meltfptr[7] = 0;
    MELT_LOCATION ("warmelt-normatch.melt:3315:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V5*/ meltfptr[4] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V7*/ meltfptr[5] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("SCANSTEPDATA_TESTINS", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_73_warmelt_normatch_SCANSTEPDATA_TESTINS_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*noretval */ NULL);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_73_warmelt_normatch_SCANSTEPDATA_TESTINS */



/**** end of warmelt-normatch+02.c ****/
