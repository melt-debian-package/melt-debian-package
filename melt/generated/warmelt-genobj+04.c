/* GCC MELT GENERATED FILE warmelt-genobj+04.c - DO NOT EDIT */
/* secondary MELT generated C file of rank #4 */
#include "melt-run.h"


/* used hash from melt-run.h when compiling this file: */
MELT_EXTERN const char meltrun_used_md5_melt_f4[] =
  MELT_RUN_HASHMD5 /* from melt-run.h */ ;


/**** warmelt-genobj+04.c declarations ****/


/***************************************************
***
    Copyright 2008 - 2012 Free Software Foundation, Inc.
    Contributed by Basile Starynkevitch <basile@starynkevitch.net>

    This file is part of GCC.

    GCC is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3, or (at your option)
    any later version.

    GCC is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with GCC; see the file COPYING3.  If not see
    <http://www.gnu.org/licenses/>.
***
****************************************************/

/* ordinary MELT module */
#define MELT_HAS_INITIAL_ENVIRONMENT 1	/*usual */

struct melt_callframe_st;	/*defined in melt-runtime.h */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_1_warmelt_genobj_MAKE_OBJLOCATEDEXP (meltclosure_ptr_t meltclosp_,
					      melt_ptr_t meltfirstargp_,
					      const melt_argdescr_cell_t
					      meltxargdescr_[],
					      union meltparam_un
					      *meltxargtab_,
					      const melt_argdescr_cell_t
					      meltxresdescr_[],
					      union meltparam_un
					      *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_2_warmelt_genobj_MAKE_OBJCOMPUTE (meltclosure_ptr_t meltclosp_,
					   melt_ptr_t meltfirstargp_,
					   const melt_argdescr_cell_t
					   meltxargdescr_[],
					   union meltparam_un *meltxargtab_,
					   const melt_argdescr_cell_t
					   meltxresdescr_[],
					   union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_3_warmelt_genobj_MAKE_OBJEXPANDPUREVAL (meltclosure_ptr_t meltclosp_,
						 melt_ptr_t meltfirstargp_,
						 const melt_argdescr_cell_t
						 meltxargdescr_[],
						 union meltparam_un
						 *meltxargtab_,
						 const melt_argdescr_cell_t
						 meltxresdescr_[],
						 union meltparam_un
						 *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_4_warmelt_genobj_COMPILOBJ_CATCHALL_NREP (meltclosure_ptr_t
						   meltclosp_,
						   melt_ptr_t meltfirstargp_,
						   const melt_argdescr_cell_t
						   meltxargdescr_[],
						   union meltparam_un
						   *meltxargtab_,
						   const melt_argdescr_cell_t
						   meltxresdescr_[],
						   union meltparam_un
						   *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_5_warmelt_genobj_PUTOBJDEST_CATCHALL_OBJCODE (meltclosure_ptr_t
						       meltclosp_,
						       melt_ptr_t
						       meltfirstargp_,
						       const
						       melt_argdescr_cell_t
						       meltxargdescr_[],
						       union meltparam_un
						       *meltxargtab_,
						       const
						       melt_argdescr_cell_t
						       meltxresdescr_[],
						       union meltparam_un
						       *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_6_warmelt_genobj_PUTOBJDEST_CATCHALL_ANYDISCR (meltclosure_ptr_t
							meltclosp_,
							melt_ptr_t
							meltfirstargp_,
							const
							melt_argdescr_cell_t
							meltxargdescr_[],
							union meltparam_un
							*meltxargtab_,
							const
							melt_argdescr_cell_t
							meltxresdescr_[],
							union meltparam_un
							*meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_7_warmelt_genobj_GETCTYPE_OBJVALUE (meltclosure_ptr_t meltclosp_,
					     melt_ptr_t meltfirstargp_,
					     const melt_argdescr_cell_t
					     meltxargdescr_[],
					     union meltparam_un *meltxargtab_,
					     const melt_argdescr_cell_t
					     meltxresdescr_[],
					     union meltparam_un
					     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_8_warmelt_genobj_GECTYP_OBJNIL (meltclosure_ptr_t meltclosp_,
					 melt_ptr_t meltfirstargp_,
					 const melt_argdescr_cell_t
					 meltxargdescr_[],
					 union meltparam_un *meltxargtab_,
					 const melt_argdescr_cell_t
					 meltxresdescr_[],
					 union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_9_warmelt_genobj_GECTYP_OBJINITSTRING (meltclosure_ptr_t meltclosp_,
						melt_ptr_t meltfirstargp_,
						const melt_argdescr_cell_t
						meltxargdescr_[],
						union meltparam_un
						*meltxargtab_,
						const melt_argdescr_cell_t
						meltxresdescr_[],
						union meltparam_un
						*meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_10_warmelt_genobj_DBGOUT_ROUTINEOBJ (meltclosure_ptr_t meltclosp_,
					      melt_ptr_t meltfirstargp_,
					      const melt_argdescr_cell_t
					      meltxargdescr_[],
					      union meltparam_un
					      *meltxargtab_,
					      const melt_argdescr_cell_t
					      meltxresdescr_[],
					      union meltparam_un
					      *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_11_warmelt_genobj_PUTOBJDEST_OBJDESTINSTR (meltclosure_ptr_t
						    meltclosp_,
						    melt_ptr_t meltfirstargp_,
						    const melt_argdescr_cell_t
						    meltxargdescr_[],
						    union meltparam_un
						    *meltxargtab_,
						    const melt_argdescr_cell_t
						    meltxresdescr_[],
						    union meltparam_un
						    *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_12_warmelt_genobj_PUTOBJDEST_OBJFINALRETURN (meltclosure_ptr_t
						      meltclosp_,
						      melt_ptr_t
						      meltfirstargp_,
						      const
						      melt_argdescr_cell_t
						      meltxargdescr_[],
						      union meltparam_un
						      *meltxargtab_,
						      const
						      melt_argdescr_cell_t
						      meltxresdescr_[],
						      union meltparam_un
						      *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_13_warmelt_genobj_VARIADIC_IDSTR (meltclosure_ptr_t meltclosp_,
					   melt_ptr_t meltfirstargp_,
					   const melt_argdescr_cell_t
					   meltxargdescr_[],
					   union meltparam_un *meltxargtab_,
					   const melt_argdescr_cell_t
					   meltxresdescr_[],
					   union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_14_warmelt_genobj_VARIADIC_INDEX_IDSTR (meltclosure_ptr_t meltclosp_,
						 melt_ptr_t meltfirstargp_,
						 const melt_argdescr_cell_t
						 meltxargdescr_[],
						 union meltparam_un
						 *meltxargtab_,
						 const melt_argdescr_cell_t
						 meltxresdescr_[],
						 union meltparam_un
						 *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_15_warmelt_genobj_VARIADIC_LENGTH_IDSTR (meltclosure_ptr_t
						  meltclosp_,
						  melt_ptr_t meltfirstargp_,
						  const melt_argdescr_cell_t
						  meltxargdescr_[],
						  union meltparam_un
						  *meltxargtab_,
						  const melt_argdescr_cell_t
						  meltxresdescr_[],
						  union meltparam_un
						  *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_16_warmelt_genobj_COMPILE2OBJ_PROCEDURE (meltclosure_ptr_t
						  meltclosp_,
						  melt_ptr_t meltfirstargp_,
						  const melt_argdescr_cell_t
						  meltxargdescr_[],
						  union meltparam_un
						  *meltxargtab_,
						  const melt_argdescr_cell_t
						  meltxresdescr_[],
						  union meltparam_un
						  *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_17_warmelt_genobj_LAMBDA___1__ (meltclosure_ptr_t meltclosp_,
					 melt_ptr_t meltfirstargp_,
					 const melt_argdescr_cell_t
					 meltxargdescr_[],
					 union meltparam_un *meltxargtab_,
					 const melt_argdescr_cell_t
					 meltxresdescr_[],
					 union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_18_warmelt_genobj_LAMBDA___2__ (meltclosure_ptr_t meltclosp_,
					 melt_ptr_t meltfirstargp_,
					 const melt_argdescr_cell_t
					 meltxargdescr_[],
					 union meltparam_un *meltxargtab_,
					 const melt_argdescr_cell_t
					 meltxresdescr_[],
					 union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_19_warmelt_genobj_APPEND_COMMENT (meltclosure_ptr_t meltclosp_,
					   melt_ptr_t meltfirstargp_,
					   const melt_argdescr_cell_t
					   meltxargdescr_[],
					   union meltparam_un *meltxargtab_,
					   const melt_argdescr_cell_t
					   meltxresdescr_[],
					   union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_20_warmelt_genobj_APPEND_COMMENTCONST (meltclosure_ptr_t meltclosp_,
						melt_ptr_t meltfirstargp_,
						const melt_argdescr_cell_t
						meltxargdescr_[],
						union meltparam_un
						*meltxargtab_,
						const melt_argdescr_cell_t
						meltxresdescr_[],
						union meltparam_un
						*meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_21_warmelt_genobj_COMPILE2OBJ_INITPROC (meltclosure_ptr_t meltclosp_,
						 melt_ptr_t meltfirstargp_,
						 const melt_argdescr_cell_t
						 meltxargdescr_[],
						 union meltparam_un
						 *meltxargtab_,
						 const melt_argdescr_cell_t
						 meltxresdescr_[],
						 union meltparam_un
						 *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_22_warmelt_genobj_LAMBDA___3__ (meltclosure_ptr_t meltclosp_,
					 melt_ptr_t meltfirstargp_,
					 const melt_argdescr_cell_t
					 meltxargdescr_[],
					 union meltparam_un *meltxargtab_,
					 const melt_argdescr_cell_t
					 meltxresdescr_[],
					 union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_23_warmelt_genobj_LAMBDA___4__ (meltclosure_ptr_t meltclosp_,
					 melt_ptr_t meltfirstargp_,
					 const melt_argdescr_cell_t
					 meltxargdescr_[],
					 union meltparam_un *meltxargtab_,
					 const melt_argdescr_cell_t
					 meltxresdescr_[],
					 union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_24_warmelt_genobj_LAMBDA___5__ (meltclosure_ptr_t meltclosp_,
					 melt_ptr_t meltfirstargp_,
					 const melt_argdescr_cell_t
					 meltxargdescr_[],
					 union meltparam_un *meltxargtab_,
					 const melt_argdescr_cell_t
					 meltxresdescr_[],
					 union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_25_warmelt_genobj_LAMBDA___6__ (meltclosure_ptr_t meltclosp_,
					 melt_ptr_t meltfirstargp_,
					 const melt_argdescr_cell_t
					 meltxargdescr_[],
					 union meltparam_un *meltxargtab_,
					 const melt_argdescr_cell_t
					 meltxresdescr_[],
					 union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_26_warmelt_genobj_LAMBDA___7__ (meltclosure_ptr_t meltclosp_,
					 melt_ptr_t meltfirstargp_,
					 const melt_argdescr_cell_t
					 meltxargdescr_[],
					 union meltparam_un *meltxargtab_,
					 const melt_argdescr_cell_t
					 meltxresdescr_[],
					 union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_27_warmelt_genobj_LAMBDA___8__ (meltclosure_ptr_t meltclosp_,
					 melt_ptr_t meltfirstargp_,
					 const melt_argdescr_cell_t
					 meltxargdescr_[],
					 union meltparam_un *meltxargtab_,
					 const melt_argdescr_cell_t
					 meltxresdescr_[],
					 union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_28_warmelt_genobj_LAMBDA___9__ (meltclosure_ptr_t meltclosp_,
					 melt_ptr_t meltfirstargp_,
					 const melt_argdescr_cell_t
					 meltxargdescr_[],
					 union meltparam_un *meltxargtab_,
					 const melt_argdescr_cell_t
					 meltxresdescr_[],
					 union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_29_warmelt_genobj_LAMBDA___10__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_30_warmelt_genobj_COMPILE2OBJ_INITEXTENDPROC (meltclosure_ptr_t
						       meltclosp_,
						       melt_ptr_t
						       meltfirstargp_,
						       const
						       melt_argdescr_cell_t
						       meltxargdescr_[],
						       union meltparam_un
						       *meltxargtab_,
						       const
						       melt_argdescr_cell_t
						       meltxresdescr_[],
						       union meltparam_un
						       *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_31_warmelt_genobj_LAMBDA___11__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_32_warmelt_genobj_LAMBDA___12__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_33_warmelt_genobj_LAMBDA___13__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_34_warmelt_genobj_LAMBDA___14__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_35_warmelt_genobj_LAMBDA___15__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_36_warmelt_genobj_LAMBDA___16__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_37_warmelt_genobj_GET_FREE_OBJLOCPTR (meltclosure_ptr_t meltclosp_,
					       melt_ptr_t meltfirstargp_,
					       const melt_argdescr_cell_t
					       meltxargdescr_[],
					       union meltparam_un
					       *meltxargtab_,
					       const melt_argdescr_cell_t
					       meltxresdescr_[],
					       union meltparam_un
					       *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_38_warmelt_genobj_GET_FREE_OBJLOCLONG (meltclosure_ptr_t meltclosp_,
						melt_ptr_t meltfirstargp_,
						const melt_argdescr_cell_t
						meltxargdescr_[],
						union meltparam_un
						*meltxargtab_,
						const melt_argdescr_cell_t
						meltxresdescr_[],
						union meltparam_un
						*meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_39_warmelt_genobj_GET_FREE_OBJLOCTYPED (meltclosure_ptr_t meltclosp_,
						 melt_ptr_t meltfirstargp_,
						 const melt_argdescr_cell_t
						 meltxargdescr_[],
						 union meltparam_un
						 *meltxargtab_,
						 const melt_argdescr_cell_t
						 meltxresdescr_[],
						 union meltparam_un
						 *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_40_warmelt_genobj_DISPOSE_OBJLOC (meltclosure_ptr_t meltclosp_,
					   melt_ptr_t meltfirstargp_,
					   const melt_argdescr_cell_t
					   meltxargdescr_[],
					   union meltparam_un *meltxargtab_,
					   const melt_argdescr_cell_t
					   meltxresdescr_[],
					   union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_41_warmelt_genobj_DISPOSE_BND_OBJ (meltclosure_ptr_t meltclosp_,
					    melt_ptr_t meltfirstargp_,
					    const melt_argdescr_cell_t
					    meltxargdescr_[],
					    union meltparam_un *meltxargtab_,
					    const melt_argdescr_cell_t
					    meltxresdescr_[],
					    union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_42_warmelt_genobj_COMPILOBJ_NREP_CHUNK (meltclosure_ptr_t meltclosp_,
						 melt_ptr_t meltfirstargp_,
						 const melt_argdescr_cell_t
						 meltxargdescr_[],
						 union meltparam_un
						 *meltxargtab_,
						 const melt_argdescr_cell_t
						 meltxresdescr_[],
						 union meltparam_un
						 *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_43_warmelt_genobj_LAMBDA___17__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_44_warmelt_genobj_GETCTYPE_TYPEXPR (meltclosure_ptr_t meltclosp_,
					     melt_ptr_t meltfirstargp_,
					     const melt_argdescr_cell_t
					     meltxargdescr_[],
					     union meltparam_un *meltxargtab_,
					     const melt_argdescr_cell_t
					     meltxresdescr_[],
					     union meltparam_un
					     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_45_warmelt_genobj_COMPILOBJ_NREP_NIL (meltclosure_ptr_t meltclosp_,
					       melt_ptr_t meltfirstargp_,
					       const melt_argdescr_cell_t
					       meltxargdescr_[],
					       union meltparam_un
					       *meltxargtab_,
					       const melt_argdescr_cell_t
					       meltxresdescr_[],
					       union meltparam_un
					       *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_46_warmelt_genobj_COMPILOBJ_NREP_COMMENT (meltclosure_ptr_t
						   meltclosp_,
						   melt_ptr_t meltfirstargp_,
						   const melt_argdescr_cell_t
						   meltxargdescr_[],
						   union meltparam_un
						   *meltxargtab_,
						   const melt_argdescr_cell_t
						   meltxresdescr_[],
						   union meltparam_un
						   *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_47_warmelt_genobj_COMPILOBJ_NREP_LOCSYMOCC (meltclosure_ptr_t
						     meltclosp_,
						     melt_ptr_t
						     meltfirstargp_,
						     const
						     melt_argdescr_cell_t
						     meltxargdescr_[],
						     union meltparam_un
						     *meltxargtab_,
						     const
						     melt_argdescr_cell_t
						     meltxresdescr_[],
						     union meltparam_un
						     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_48_warmelt_genobj_COMPILOBJ_NREP_CLOSEDOCC (meltclosure_ptr_t
						     meltclosp_,
						     melt_ptr_t
						     meltfirstargp_,
						     const
						     melt_argdescr_cell_t
						     meltxargdescr_[],
						     union meltparam_un
						     *meltxargtab_,
						     const
						     melt_argdescr_cell_t
						     meltxresdescr_[],
						     union meltparam_un
						     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_49_warmelt_genobj_COMPILOBJ_NREP_CONSTOCC (meltclosure_ptr_t
						    meltclosp_,
						    melt_ptr_t meltfirstargp_,
						    const melt_argdescr_cell_t
						    meltxargdescr_[],
						    union meltparam_un
						    *meltxargtab_,
						    const melt_argdescr_cell_t
						    meltxresdescr_[],
						    union meltparam_un
						    *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_50_warmelt_genobj_COMPILOBJ_NREP_IMPORTEDVAL (meltclosure_ptr_t
						       meltclosp_,
						       melt_ptr_t
						       meltfirstargp_,
						       const
						       melt_argdescr_cell_t
						       meltxargdescr_[],
						       union meltparam_un
						       *meltxargtab_,
						       const
						       melt_argdescr_cell_t
						       meltxresdescr_[],
						       union meltparam_un
						       *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_51_warmelt_genobj_COMPILOBJ_NREP_LITERALVALUE (meltclosure_ptr_t
							meltclosp_,
							melt_ptr_t
							meltfirstargp_,
							const
							melt_argdescr_cell_t
							meltxargdescr_[],
							union meltparam_un
							*meltxargtab_,
							const
							melt_argdescr_cell_t
							meltxresdescr_[],
							union meltparam_un
							*meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_52_warmelt_genobj_COMPILOBJ_NREP_DEFINEDCONSTANT (meltclosure_ptr_t
							   meltclosp_,
							   melt_ptr_t
							   meltfirstargp_,
							   const
							   melt_argdescr_cell_t
							   meltxargdescr_[],
							   union meltparam_un
							   *meltxargtab_,
							   const
							   melt_argdescr_cell_t
							   meltxresdescr_[],
							   union meltparam_un
							   *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_53_warmelt_genobj_COMPILOBJ_NREP_QUASICONSTANT (meltclosure_ptr_t
							 meltclosp_,
							 melt_ptr_t
							 meltfirstargp_,
							 const
							 melt_argdescr_cell_t
							 meltxargdescr_[],
							 union meltparam_un
							 *meltxargtab_,
							 const
							 melt_argdescr_cell_t
							 meltxresdescr_[],
							 union meltparam_un
							 *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_54_warmelt_genobj_COMPILOBJ_NREP_QUASICONST_CURRENT_MODULE_ENVIRONMENT_CONTAINER
(meltclosure_ptr_t meltclosp_, melt_ptr_t meltfirstargp_, const melt_argdescr_cell_t meltxargdescr_[],
union meltparam_un *meltxargtab_, const melt_argdescr_cell_t meltxresdescr_[],
union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_55_warmelt_genobj_COMPILOBJ_NREP_FOREVER (meltclosure_ptr_t
						   meltclosp_,
						   melt_ptr_t meltfirstargp_,
						   const melt_argdescr_cell_t
						   meltxargdescr_[],
						   union meltparam_un
						   *meltxargtab_,
						   const melt_argdescr_cell_t
						   meltxresdescr_[],
						   union meltparam_un
						   *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_56_warmelt_genobj_LAMBDA___18__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_57_warmelt_genobj_COMPILOBJ_NREP_EXIT (meltclosure_ptr_t meltclosp_,
						melt_ptr_t meltfirstargp_,
						const melt_argdescr_cell_t
						meltxargdescr_[],
						union meltparam_un
						*meltxargtab_,
						const melt_argdescr_cell_t
						meltxresdescr_[],
						union meltparam_un
						*meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_58_warmelt_genobj_COMPILOBJ_NREP_AGAIN (meltclosure_ptr_t meltclosp_,
						 melt_ptr_t meltfirstargp_,
						 const melt_argdescr_cell_t
						 meltxargdescr_[],
						 union meltparam_un
						 *meltxargtab_,
						 const melt_argdescr_cell_t
						 meltxresdescr_[],
						 union meltparam_un
						 *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_59_warmelt_genobj_COMPILOBJ_DISCRANY (meltclosure_ptr_t meltclosp_,
					       melt_ptr_t meltfirstargp_,
					       const melt_argdescr_cell_t
					       meltxargdescr_[],
					       union meltparam_un
					       *meltxargtab_,
					       const melt_argdescr_cell_t
					       meltxresdescr_[],
					       union meltparam_un
					       *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_60_warmelt_genobj_COMPILOBJ_NREP_LET (meltclosure_ptr_t meltclosp_,
					       melt_ptr_t meltfirstargp_,
					       const melt_argdescr_cell_t
					       meltxargdescr_[],
					       union meltparam_un
					       *meltxargtab_,
					       const melt_argdescr_cell_t
					       meltxresdescr_[],
					       union meltparam_un
					       *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_61_warmelt_genobj_LAMBDA___19__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_62_warmelt_genobj_LAMBDA___20__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_63_warmelt_genobj_LAMBDA___21__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_64_warmelt_genobj_FAIL_COMPILETRECFILL (meltclosure_ptr_t meltclosp_,
						 melt_ptr_t meltfirstargp_,
						 const melt_argdescr_cell_t
						 meltxargdescr_[],
						 union meltparam_un
						 *meltxargtab_,
						 const melt_argdescr_cell_t
						 meltxresdescr_[],
						 union meltparam_un
						 *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_65_warmelt_genobj_COMPILETREC_LAMBDA (meltclosure_ptr_t meltclosp_,
					       melt_ptr_t meltfirstargp_,
					       const melt_argdescr_cell_t
					       meltxargdescr_[],
					       union meltparam_un
					       *meltxargtab_,
					       const melt_argdescr_cell_t
					       meltxresdescr_[],
					       union meltparam_un
					       *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_66_warmelt_genobj_COMPILETREC_TUPLE (meltclosure_ptr_t meltclosp_,
					      melt_ptr_t meltfirstargp_,
					      const melt_argdescr_cell_t
					      meltxargdescr_[],
					      union meltparam_un
					      *meltxargtab_,
					      const melt_argdescr_cell_t
					      meltxresdescr_[],
					      union meltparam_un
					      *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_67_warmelt_genobj_COMPILETREC_PAIR (meltclosure_ptr_t meltclosp_,
					     melt_ptr_t meltfirstargp_,
					     const melt_argdescr_cell_t
					     meltxargdescr_[],
					     union meltparam_un *meltxargtab_,
					     const melt_argdescr_cell_t
					     meltxresdescr_[],
					     union meltparam_un
					     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_68_warmelt_genobj_COMPILETREC_LIST (meltclosure_ptr_t meltclosp_,
					     melt_ptr_t meltfirstargp_,
					     const melt_argdescr_cell_t
					     meltxargdescr_[],
					     union meltparam_un *meltxargtab_,
					     const melt_argdescr_cell_t
					     meltxresdescr_[],
					     union meltparam_un
					     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_69_warmelt_genobj_COMPILETREC_INSTANCE (meltclosure_ptr_t meltclosp_,
						 melt_ptr_t meltfirstargp_,
						 const melt_argdescr_cell_t
						 meltxargdescr_[],
						 union meltparam_un
						 *meltxargtab_,
						 const melt_argdescr_cell_t
						 meltxresdescr_[],
						 union meltparam_un
						 *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_70_warmelt_genobj_COMPILOBJ_NREP_LETREC (meltclosure_ptr_t
						  meltclosp_,
						  melt_ptr_t meltfirstargp_,
						  const melt_argdescr_cell_t
						  meltxargdescr_[],
						  union meltparam_un
						  *meltxargtab_,
						  const melt_argdescr_cell_t
						  meltxresdescr_[],
						  union meltparam_un
						  *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_71_warmelt_genobj_COMPILOBJ_NREP_CITERATION (meltclosure_ptr_t
						      meltclosp_,
						      melt_ptr_t
						      meltfirstargp_,
						      const
						      melt_argdescr_cell_t
						      meltxargdescr_[],
						      union meltparam_un
						      *meltxargtab_,
						      const
						      melt_argdescr_cell_t
						      meltxresdescr_[],
						      union meltparam_un
						      *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_72_warmelt_genobj_LAMBDA___22__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_73_warmelt_genobj_LAMBDA___23__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_74_warmelt_genobj_LAMBDA___24__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_75_warmelt_genobj_LAMBDA___25__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_76_warmelt_genobj_LAMBDA___26__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_77_warmelt_genobj_COMPILOBJ_NREP_SETQ (meltclosure_ptr_t meltclosp_,
						melt_ptr_t meltfirstargp_,
						const melt_argdescr_cell_t
						meltxargdescr_[],
						union meltparam_un
						*meltxargtab_,
						const melt_argdescr_cell_t
						meltxresdescr_[],
						union meltparam_un
						*meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_78_warmelt_genobj_COMPILOBJ_NREP_PROGN (meltclosure_ptr_t meltclosp_,
						 melt_ptr_t meltfirstargp_,
						 const melt_argdescr_cell_t
						 meltxargdescr_[],
						 union meltparam_un
						 *meltxargtab_,
						 const melt_argdescr_cell_t
						 meltxresdescr_[],
						 union meltparam_un
						 *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_79_warmelt_genobj_LAMBDA___27__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_80_warmelt_genobj_COMPILOBJ_NREP_MULTACC (meltclosure_ptr_t
						   meltclosp_,
						   melt_ptr_t meltfirstargp_,
						   const melt_argdescr_cell_t
						   meltxargdescr_[],
						   union meltparam_un
						   *meltxargtab_,
						   const melt_argdescr_cell_t
						   meltxresdescr_[],
						   union meltparam_un
						   *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_81_warmelt_genobj_LAMBDA___28__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_82_warmelt_genobj_COMPILOBJ_NREP_FIELDACC (meltclosure_ptr_t
						    meltclosp_,
						    melt_ptr_t meltfirstargp_,
						    const melt_argdescr_cell_t
						    meltxargdescr_[],
						    union meltparam_un
						    *meltxargtab_,
						    const melt_argdescr_cell_t
						    meltxresdescr_[],
						    union meltparam_un
						    *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_83_warmelt_genobj_LAMBDA___29__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_84_warmelt_genobj_COMPILOBJ_NREP_UNSAFE_GET_FIELD (meltclosure_ptr_t
							    meltclosp_,
							    melt_ptr_t
							    meltfirstargp_,
							    const
							    melt_argdescr_cell_t
							    meltxargdescr_[],
							    union meltparam_un
							    *meltxargtab_,
							    const
							    melt_argdescr_cell_t
							    meltxresdescr_[],
							    union meltparam_un
							    *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_85_warmelt_genobj_COMPILOBJ_NREP_UNSAFE_PUT_FIELDS (meltclosure_ptr_t
							     meltclosp_,
							     melt_ptr_t
							     meltfirstargp_,
							     const
							     melt_argdescr_cell_t
							     meltxargdescr_[],
							     union
							     meltparam_un
							     *meltxargtab_,
							     const
							     melt_argdescr_cell_t
							     meltxresdescr_[],
							     union
							     meltparam_un
							     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_86_warmelt_genobj_LAMBDA___30__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_87_warmelt_genobj_COMPILOBJ_NREP_CHECKSIGNAL (meltclosure_ptr_t
						       meltclosp_,
						       melt_ptr_t
						       meltfirstargp_,
						       const
						       melt_argdescr_cell_t
						       meltxargdescr_[],
						       union meltparam_un
						       *meltxargtab_,
						       const
						       melt_argdescr_cell_t
						       meltxresdescr_[],
						       union meltparam_un
						       *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_88_warmelt_genobj_COMPILOBJ_NREP_UNSAFE_NTH_COMPONENT
(meltclosure_ptr_t meltclosp_, melt_ptr_t meltfirstargp_, const melt_argdescr_cell_t meltxargdescr_[],
union meltparam_un *meltxargtab_, const melt_argdescr_cell_t meltxresdescr_[],
union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_89_warmelt_genobj_COMPILOBJ_NREP_APPLY (meltclosure_ptr_t meltclosp_,
						 melt_ptr_t meltfirstargp_,
						 const melt_argdescr_cell_t
						 meltxargdescr_[],
						 union meltparam_un
						 *meltxargtab_,
						 const melt_argdescr_cell_t
						 meltxresdescr_[],
						 union meltparam_un
						 *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_90_warmelt_genobj_LAMBDA___31__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_91_warmelt_genobj_COMPILOBJ_NREP_MULTIAPPLY (meltclosure_ptr_t
						      meltclosp_,
						      melt_ptr_t
						      meltfirstargp_,
						      const
						      melt_argdescr_cell_t
						      meltxargdescr_[],
						      union meltparam_un
						      *meltxargtab_,
						      const
						      melt_argdescr_cell_t
						      meltxresdescr_[],
						      union meltparam_un
						      *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_92_warmelt_genobj_LAMBDA___32__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_93_warmelt_genobj_LAMBDA___33__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_94_warmelt_genobj_LAMBDA___34__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_95_warmelt_genobj_LAMBDA___35__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_96_warmelt_genobj_LAMBDA___36__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_97_warmelt_genobj_COMPILOBJ_NREP_MSEND (meltclosure_ptr_t meltclosp_,
						 melt_ptr_t meltfirstargp_,
						 const melt_argdescr_cell_t
						 meltxargdescr_[],
						 union meltparam_un
						 *meltxargtab_,
						 const melt_argdescr_cell_t
						 meltxresdescr_[],
						 union meltparam_un
						 *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_98_warmelt_genobj_LAMBDA___37__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_99_warmelt_genobj_COMPILOBJ_NREP_MULTIMSEND (meltclosure_ptr_t
						      meltclosp_,
						      melt_ptr_t
						      meltfirstargp_,
						      const
						      melt_argdescr_cell_t
						      meltxargdescr_[],
						      union meltparam_un
						      *meltxargtab_,
						      const
						      melt_argdescr_cell_t
						      meltxresdescr_[],
						      union meltparam_un
						      *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_100_warmelt_genobj_LAMBDA___38__ (meltclosure_ptr_t meltclosp_,
					   melt_ptr_t meltfirstargp_,
					   const melt_argdescr_cell_t
					   meltxargdescr_[],
					   union meltparam_un *meltxargtab_,
					   const melt_argdescr_cell_t
					   meltxresdescr_[],
					   union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_101_warmelt_genobj_LAMBDA___39__ (meltclosure_ptr_t meltclosp_,
					   melt_ptr_t meltfirstargp_,
					   const melt_argdescr_cell_t
					   meltxargdescr_[],
					   union meltparam_un *meltxargtab_,
					   const melt_argdescr_cell_t
					   meltxresdescr_[],
					   union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_102_warmelt_genobj_LAMBDA___40__ (meltclosure_ptr_t meltclosp_,
					   melt_ptr_t meltfirstargp_,
					   const melt_argdescr_cell_t
					   meltxargdescr_[],
					   union meltparam_un *meltxargtab_,
					   const melt_argdescr_cell_t
					   meltxresdescr_[],
					   union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_103_warmelt_genobj_LAMBDA___41__ (meltclosure_ptr_t meltclosp_,
					   melt_ptr_t meltfirstargp_,
					   const melt_argdescr_cell_t
					   meltxargdescr_[],
					   union meltparam_un *meltxargtab_,
					   const melt_argdescr_cell_t
					   meltxresdescr_[],
					   union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_104_warmelt_genobj_COMPILOBJ_ANY_BINDING (meltclosure_ptr_t
						   meltclosp_,
						   melt_ptr_t meltfirstargp_,
						   const melt_argdescr_cell_t
						   meltxargdescr_[],
						   union meltparam_un
						   *meltxargtab_,
						   const melt_argdescr_cell_t
						   meltxresdescr_[],
						   union meltparam_un
						   *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_105_warmelt_genobj_COMPILOBJ_VALUE_BINDING (meltclosure_ptr_t
						     meltclosp_,
						     melt_ptr_t
						     meltfirstargp_,
						     const
						     melt_argdescr_cell_t
						     meltxargdescr_[],
						     union meltparam_un
						     *meltxargtab_,
						     const
						     melt_argdescr_cell_t
						     meltxresdescr_[],
						     union meltparam_un
						     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_106_warmelt_genobj_COMPILOBJ_FIXED_BINDING (meltclosure_ptr_t
						     meltclosp_,
						     melt_ptr_t
						     meltfirstargp_,
						     const
						     melt_argdescr_cell_t
						     meltxargdescr_[],
						     union meltparam_un
						     *meltxargtab_,
						     const
						     melt_argdescr_cell_t
						     meltxresdescr_[],
						     union meltparam_un
						     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_107_warmelt_genobj_COMPILOBJ_NORMAL_LET_BINDING (meltclosure_ptr_t
							  meltclosp_,
							  melt_ptr_t
							  meltfirstargp_,
							  const
							  melt_argdescr_cell_t
							  meltxargdescr_[],
							  union meltparam_un
							  *meltxargtab_,
							  const
							  melt_argdescr_cell_t
							  meltxresdescr_[],
							  union meltparam_un
							  *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_108_warmelt_genobj_COMPILOBJ_CONSLAMBDABIND (meltclosure_ptr_t
						      meltclosp_,
						      melt_ptr_t
						      meltfirstargp_,
						      const
						      melt_argdescr_cell_t
						      meltxargdescr_[],
						      union meltparam_un
						      *meltxargtab_,
						      const
						      melt_argdescr_cell_t
						      meltxresdescr_[],
						      union meltparam_un
						      *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_109_warmelt_genobj_COMPILOBJ_CONSTUPLEBIND (meltclosure_ptr_t
						     meltclosp_,
						     melt_ptr_t
						     meltfirstargp_,
						     const
						     melt_argdescr_cell_t
						     meltxargdescr_[],
						     union meltparam_un
						     *meltxargtab_,
						     const
						     melt_argdescr_cell_t
						     meltxresdescr_[],
						     union meltparam_un
						     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_110_warmelt_genobj_COMPILOBJ_CONSPAIRBIND (meltclosure_ptr_t
						    meltclosp_,
						    melt_ptr_t meltfirstargp_,
						    const melt_argdescr_cell_t
						    meltxargdescr_[],
						    union meltparam_un
						    *meltxargtab_,
						    const melt_argdescr_cell_t
						    meltxresdescr_[],
						    union meltparam_un
						    *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_111_warmelt_genobj_COMPILOBJ_CONSLISTBIND (meltclosure_ptr_t
						    meltclosp_,
						    melt_ptr_t meltfirstargp_,
						    const melt_argdescr_cell_t
						    meltxargdescr_[],
						    union meltparam_un
						    *meltxargtab_,
						    const melt_argdescr_cell_t
						    meltxresdescr_[],
						    union meltparam_un
						    *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_112_warmelt_genobj_COMPILOBJ_CONSINSTANCEBIND (meltclosure_ptr_t
							meltclosp_,
							melt_ptr_t
							meltfirstargp_,
							const
							melt_argdescr_cell_t
							meltxargdescr_[],
							union meltparam_un
							*meltxargtab_,
							const
							melt_argdescr_cell_t
							meltxresdescr_[],
							union meltparam_un
							*meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_113_warmelt_genobj_PUTOBJDEST_OBJVALUE (meltclosure_ptr_t meltclosp_,
						 melt_ptr_t meltfirstargp_,
						 const melt_argdescr_cell_t
						 meltxargdescr_[],
						 union meltparam_un
						 *meltxargtab_,
						 const melt_argdescr_cell_t
						 meltxresdescr_[],
						 union meltparam_un
						 *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_114_warmelt_genobj_PUTOBJDEST_INTEGER (meltclosure_ptr_t meltclosp_,
						melt_ptr_t meltfirstargp_,
						const melt_argdescr_cell_t
						meltxargdescr_[],
						union meltparam_un
						*meltxargtab_,
						const melt_argdescr_cell_t
						meltxresdescr_[],
						union meltparam_un
						*meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_115_warmelt_genobj_PUTOBJDEST_STRING (meltclosure_ptr_t meltclosp_,
					       melt_ptr_t meltfirstargp_,
					       const melt_argdescr_cell_t
					       meltxargdescr_[],
					       union meltparam_un
					       *meltxargtab_,
					       const melt_argdescr_cell_t
					       meltxresdescr_[],
					       union meltparam_un
					       *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_116_warmelt_genobj_PUTOBJDEST_NULL (meltclosure_ptr_t meltclosp_,
					     melt_ptr_t meltfirstargp_,
					     const melt_argdescr_cell_t
					     meltxargdescr_[],
					     union meltparam_un *meltxargtab_,
					     const melt_argdescr_cell_t
					     meltxresdescr_[],
					     union meltparam_un
					     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_117_warmelt_genobj_PUTOBJDEST_OBJANYBLOCK (meltclosure_ptr_t
						    meltclosp_,
						    melt_ptr_t meltfirstargp_,
						    const melt_argdescr_cell_t
						    meltxargdescr_[],
						    union meltparam_un
						    *meltxargtab_,
						    const melt_argdescr_cell_t
						    meltxresdescr_[],
						    union meltparam_un
						    *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_118_warmelt_genobj_PUTOBJDEST_OBJMULTIBLOCK (meltclosure_ptr_t
						      meltclosp_,
						      melt_ptr_t
						      meltfirstargp_,
						      const
						      melt_argdescr_cell_t
						      meltxargdescr_[],
						      union meltparam_un
						      *meltxargtab_,
						      const
						      melt_argdescr_cell_t
						      meltxresdescr_[],
						      union meltparam_un
						      *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_119_warmelt_genobj_PUTOBJDEST_OBJLOOP (meltclosure_ptr_t meltclosp_,
						melt_ptr_t meltfirstargp_,
						const melt_argdescr_cell_t
						meltxargdescr_[],
						union meltparam_un
						*meltxargtab_,
						const melt_argdescr_cell_t
						meltxresdescr_[],
						union meltparam_un
						*meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_120_warmelt_genobj_PUTOBJDEST_OBJEXIT (meltclosure_ptr_t meltclosp_,
						melt_ptr_t meltfirstargp_,
						const melt_argdescr_cell_t
						meltxargdescr_[],
						union meltparam_un
						*meltxargtab_,
						const melt_argdescr_cell_t
						meltxresdescr_[],
						union meltparam_un
						*meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_121_warmelt_genobj_COMPILOBJ_NREP_IF (meltclosure_ptr_t meltclosp_,
					       melt_ptr_t meltfirstargp_,
					       const melt_argdescr_cell_t
					       meltxargdescr_[],
					       union meltparam_un
					       *meltxargtab_,
					       const melt_argdescr_cell_t
					       meltxresdescr_[],
					       union meltparam_un
					       *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_122_warmelt_genobj_COMPILOBJ_NREP_IFISA (meltclosure_ptr_t
						  meltclosp_,
						  melt_ptr_t meltfirstargp_,
						  const melt_argdescr_cell_t
						  meltxargdescr_[],
						  union meltparam_un
						  *meltxargtab_,
						  const melt_argdescr_cell_t
						  meltxresdescr_[],
						  union meltparam_un
						  *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_123_warmelt_genobj_COMPILOBJ_NREP_IFTUPLESIZED (meltclosure_ptr_t
							 meltclosp_,
							 melt_ptr_t
							 meltfirstargp_,
							 const
							 melt_argdescr_cell_t
							 meltxargdescr_[],
							 union meltparam_un
							 *meltxargtab_,
							 const
							 melt_argdescr_cell_t
							 meltxresdescr_[],
							 union meltparam_un
							 *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_124_warmelt_genobj_COMPILOBJ_NREP_IFVARIADIC (meltclosure_ptr_t
						       meltclosp_,
						       melt_ptr_t
						       meltfirstargp_,
						       const
						       melt_argdescr_cell_t
						       meltxargdescr_[],
						       union meltparam_un
						       *meltxargtab_,
						       const
						       melt_argdescr_cell_t
						       meltxresdescr_[],
						       union meltparam_un
						       *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_125_warmelt_genobj_GETCTYPE_IFVARIADIC (meltclosure_ptr_t meltclosp_,
						 melt_ptr_t meltfirstargp_,
						 const melt_argdescr_cell_t
						 meltxargdescr_[],
						 union meltparam_un
						 *meltxargtab_,
						 const melt_argdescr_cell_t
						 meltxresdescr_[],
						 union meltparam_un
						 *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_126_warmelt_genobj_COMPILOBJ_NREP_VARIADIC_ARGUMENT
(meltclosure_ptr_t meltclosp_, melt_ptr_t meltfirstargp_, const melt_argdescr_cell_t meltxargdescr_[],
union meltparam_un *meltxargtab_, const melt_argdescr_cell_t meltxresdescr_[],
union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_127_warmelt_genobj_COMPILOBJ_NREP_CONSUMEVARIADIC (meltclosure_ptr_t
							    meltclosp_,
							    melt_ptr_t
							    meltfirstargp_,
							    const
							    melt_argdescr_cell_t
							    meltxargdescr_[],
							    union meltparam_un
							    *meltxargtab_,
							    const
							    melt_argdescr_cell_t
							    meltxresdescr_[],
							    union meltparam_un
							    *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_128_warmelt_genobj_PUTOBJDEST_OBJCOND (meltclosure_ptr_t meltclosp_,
						melt_ptr_t meltfirstargp_,
						const melt_argdescr_cell_t
						meltxargdescr_[],
						union meltparam_un
						*meltxargtab_,
						const melt_argdescr_cell_t
						meltxresdescr_[],
						union meltparam_un
						*meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_129_warmelt_genobj_COMPILOBJ_NREP_CPPIF (meltclosure_ptr_t
						  meltclosp_,
						  melt_ptr_t meltfirstargp_,
						  const melt_argdescr_cell_t
						  meltxargdescr_[],
						  union meltparam_un
						  *meltxargtab_,
						  const melt_argdescr_cell_t
						  meltxresdescr_[],
						  union meltparam_un
						  *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_130_warmelt_genobj_PUTOBJDEST_OBJCPPIF (meltclosure_ptr_t meltclosp_,
						 melt_ptr_t meltfirstargp_,
						 const melt_argdescr_cell_t
						 meltxargdescr_[],
						 union meltparam_un
						 *meltxargtab_,
						 const melt_argdescr_cell_t
						 meltxresdescr_[],
						 union meltparam_un
						 *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_131_warmelt_genobj_COMPILOBJ_NREP_RETURN (meltclosure_ptr_t
						   meltclosp_,
						   melt_ptr_t meltfirstargp_,
						   const melt_argdescr_cell_t
						   meltxargdescr_[],
						   union meltparam_un
						   *meltxargtab_,
						   const melt_argdescr_cell_t
						   meltxresdescr_[],
						   union meltparam_un
						   *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_132_warmelt_genobj_LAMBDA___42__ (meltclosure_ptr_t meltclosp_,
					   melt_ptr_t meltfirstargp_,
					   const melt_argdescr_cell_t
					   meltxargdescr_[],
					   union meltparam_un *meltxargtab_,
					   const melt_argdescr_cell_t
					   meltxresdescr_[],
					   union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_133_warmelt_genobj_COMPILOBJ_NREP_LAMBDA (meltclosure_ptr_t
						   meltclosp_,
						   melt_ptr_t meltfirstargp_,
						   const melt_argdescr_cell_t
						   meltxargdescr_[],
						   union meltparam_un
						   *meltxargtab_,
						   const melt_argdescr_cell_t
						   meltxresdescr_[],
						   union meltparam_un
						   *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_134_warmelt_genobj_LAMBDA___43__ (meltclosure_ptr_t meltclosp_,
					   melt_ptr_t meltfirstargp_,
					   const melt_argdescr_cell_t
					   meltxargdescr_[],
					   union meltparam_un *meltxargtab_,
					   const melt_argdescr_cell_t
					   meltxresdescr_[],
					   union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_135_warmelt_genobj_COMPILOBJ_NREP_MAKEINST (meltclosure_ptr_t
						     meltclosp_,
						     melt_ptr_t
						     meltfirstargp_,
						     const
						     melt_argdescr_cell_t
						     meltxargdescr_[],
						     union meltparam_un
						     *meltxargtab_,
						     const
						     melt_argdescr_cell_t
						     meltxresdescr_[],
						     union meltparam_un
						     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_136_warmelt_genobj_LAMBDA___44__ (meltclosure_ptr_t meltclosp_,
					   melt_ptr_t meltfirstargp_,
					   const melt_argdescr_cell_t
					   meltxargdescr_[],
					   union meltparam_un *meltxargtab_,
					   const melt_argdescr_cell_t
					   meltxresdescr_[],
					   union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_137_warmelt_genobj_COMPILOBJ_ROUTPROC (meltclosure_ptr_t meltclosp_,
						melt_ptr_t meltfirstargp_,
						const melt_argdescr_cell_t
						meltxargdescr_[],
						union meltparam_un
						*meltxargtab_,
						const melt_argdescr_cell_t
						meltxresdescr_[],
						union meltparam_un
						*meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_138_warmelt_genobj_COMPILOBJ_PREDEF (meltclosure_ptr_t meltclosp_,
					      melt_ptr_t meltfirstargp_,
					      const melt_argdescr_cell_t
					      meltxargdescr_[],
					      union meltparam_un
					      *meltxargtab_,
					      const melt_argdescr_cell_t
					      meltxresdescr_[],
					      union meltparam_un
					      *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_139_warmelt_genobj_COMPIL_DATA_AND_SLOTS_FILL (meltclosure_ptr_t
							meltclosp_,
							melt_ptr_t
							meltfirstargp_,
							const
							melt_argdescr_cell_t
							meltxargdescr_[],
							union meltparam_un
							*meltxargtab_,
							const
							melt_argdescr_cell_t
							meltxresdescr_[],
							union meltparam_un
							*meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_140_warmelt_genobj_LAMBDA___45__ (meltclosure_ptr_t meltclosp_,
					   melt_ptr_t meltfirstargp_,
					   const melt_argdescr_cell_t
					   meltxargdescr_[],
					   union meltparam_un *meltxargtab_,
					   const melt_argdescr_cell_t
					   meltxresdescr_[],
					   union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_141_warmelt_genobj_LAMBDA___46__ (meltclosure_ptr_t meltclosp_,
					   melt_ptr_t meltfirstargp_,
					   const melt_argdescr_cell_t
					   meltxargdescr_[],
					   union meltparam_un *meltxargtab_,
					   const melt_argdescr_cell_t
					   meltxresdescr_[],
					   union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_142_warmelt_genobj_LAMBDA___47__ (meltclosure_ptr_t meltclosp_,
					   melt_ptr_t meltfirstargp_,
					   const melt_argdescr_cell_t
					   meltxargdescr_[],
					   union meltparam_un *meltxargtab_,
					   const melt_argdescr_cell_t
					   meltxresdescr_[],
					   union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_143_warmelt_genobj_DISPOSE_DLOCBIND_AFTER_DATA_AND_SLOTS_FILL
(meltclosure_ptr_t meltclosp_, melt_ptr_t meltfirstargp_, const melt_argdescr_cell_t meltxargdescr_[],
union meltparam_un *meltxargtab_, const melt_argdescr_cell_t meltxresdescr_[],
union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_144_warmelt_genobj_LAMBDA___48__ (meltclosure_ptr_t meltclosp_,
					   melt_ptr_t meltfirstargp_,
					   const melt_argdescr_cell_t
					   meltxargdescr_[],
					   union meltparam_un *meltxargtab_,
					   const melt_argdescr_cell_t
					   meltxresdescr_[],
					   union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_145_warmelt_genobj_COMPILOBJ_DATASYMBOL (meltclosure_ptr_t
						  meltclosp_,
						  melt_ptr_t meltfirstargp_,
						  const melt_argdescr_cell_t
						  meltxargdescr_[],
						  union meltparam_un
						  *meltxargtab_,
						  const melt_argdescr_cell_t
						  meltxresdescr_[],
						  union meltparam_un
						  *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_146_warmelt_genobj_COMPILOBJ_DATAINSTANCE (meltclosure_ptr_t
						    meltclosp_,
						    melt_ptr_t meltfirstargp_,
						    const melt_argdescr_cell_t
						    meltxargdescr_[],
						    union meltparam_un
						    *meltxargtab_,
						    const melt_argdescr_cell_t
						    meltxresdescr_[],
						    union meltparam_un
						    *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_147_warmelt_genobj_COMPILOBJ_DATATUPLE (meltclosure_ptr_t meltclosp_,
						 melt_ptr_t meltfirstargp_,
						 const melt_argdescr_cell_t
						 meltxargdescr_[],
						 union meltparam_un
						 *meltxargtab_,
						 const melt_argdescr_cell_t
						 meltxresdescr_[],
						 union meltparam_un
						 *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_148_warmelt_genobj_LAMBDA___49__ (meltclosure_ptr_t meltclosp_,
					   melt_ptr_t meltfirstargp_,
					   const melt_argdescr_cell_t
					   meltxargdescr_[],
					   union meltparam_un *meltxargtab_,
					   const melt_argdescr_cell_t
					   meltxresdescr_[],
					   union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_149_warmelt_genobj_LAMBDA___50__ (meltclosure_ptr_t meltclosp_,
					   melt_ptr_t meltfirstargp_,
					   const melt_argdescr_cell_t
					   meltxargdescr_[],
					   union meltparam_un *meltxargtab_,
					   const melt_argdescr_cell_t
					   meltxresdescr_[],
					   union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_150_warmelt_genobj_COMPILOBJ_DATASTRING (meltclosure_ptr_t
						  meltclosp_,
						  melt_ptr_t meltfirstargp_,
						  const melt_argdescr_cell_t
						  meltxargdescr_[],
						  union meltparam_un
						  *meltxargtab_,
						  const melt_argdescr_cell_t
						  meltxresdescr_[],
						  union meltparam_un
						  *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_151_warmelt_genobj_COMPILOBJ_DATABOXEDINTEGER (meltclosure_ptr_t
							meltclosp_,
							melt_ptr_t
							meltfirstargp_,
							const
							melt_argdescr_cell_t
							meltxargdescr_[],
							union meltparam_un
							*meltxargtab_,
							const
							melt_argdescr_cell_t
							meltxresdescr_[],
							union meltparam_un
							*meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_152_warmelt_genobj_COMPILOBJ_DATACLOSURE (meltclosure_ptr_t
						   meltclosp_,
						   melt_ptr_t meltfirstargp_,
						   const melt_argdescr_cell_t
						   meltxargdescr_[],
						   union meltparam_un
						   *meltxargtab_,
						   const melt_argdescr_cell_t
						   meltxresdescr_[],
						   union meltparam_un
						   *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_153_warmelt_genobj_LAMBDA___51__ (meltclosure_ptr_t meltclosp_,
					   melt_ptr_t meltfirstargp_,
					   const melt_argdescr_cell_t
					   meltxargdescr_[],
					   union meltparam_un *meltxargtab_,
					   const melt_argdescr_cell_t
					   meltxresdescr_[],
					   union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_154_warmelt_genobj_COMPILOBJ_DATAROUTINE (meltclosure_ptr_t
						   meltclosp_,
						   melt_ptr_t meltfirstargp_,
						   const melt_argdescr_cell_t
						   meltxargdescr_[],
						   union meltparam_un
						   *meltxargtab_,
						   const melt_argdescr_cell_t
						   meltxresdescr_[],
						   union meltparam_un
						   *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_155_warmelt_genobj_LAMBDA___52__ (meltclosure_ptr_t meltclosp_,
					   melt_ptr_t meltfirstargp_,
					   const melt_argdescr_cell_t
					   meltxargdescr_[],
					   union meltparam_un *meltxargtab_,
					   const melt_argdescr_cell_t
					   meltxresdescr_[],
					   union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_156_warmelt_genobj_COMPILOBJ_QUASIDATA_CURRENT_MODULE_ENVIRONMENT_CONTAINER
(meltclosure_ptr_t meltclosp_, melt_ptr_t meltfirstargp_, const melt_argdescr_cell_t meltxargdescr_[],
union meltparam_un *meltxargtab_, const melt_argdescr_cell_t meltxresdescr_[],
union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_157_warmelt_genobj_COMPILOBJ_QUASIDATA_PARENT_MODULE_ENVIRONMENT
(meltclosure_ptr_t meltclosp_, melt_ptr_t meltfirstargp_, const melt_argdescr_cell_t meltxargdescr_[],
union meltparam_un *meltxargtab_, const melt_argdescr_cell_t meltxresdescr_[],
union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_158_warmelt_genobj_COMPILOBJ_NREP_STORE_PREDEFINED (meltclosure_ptr_t
							     meltclosp_,
							     melt_ptr_t
							     meltfirstargp_,
							     const
							     melt_argdescr_cell_t
							     meltxargdescr_[],
							     union
							     meltparam_un
							     *meltxargtab_,
							     const
							     melt_argdescr_cell_t
							     meltxresdescr_[],
							     union
							     meltparam_un
							     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_159_warmelt_genobj_COMPILOBJ_NREP_UPDATE_CURRENT_MODULE_ENVIRONMENT_CONTAINER
(meltclosure_ptr_t meltclosp_, melt_ptr_t meltfirstargp_, const melt_argdescr_cell_t meltxargdescr_[],
union meltparam_un *meltxargtab_, const melt_argdescr_cell_t meltxresdescr_[],
union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_160_warmelt_genobj_LAMBDA___53__ (meltclosure_ptr_t meltclosp_,
					   melt_ptr_t meltfirstargp_,
					   const melt_argdescr_cell_t
					   meltxargdescr_[],
					   union meltparam_un *meltxargtab_,
					   const melt_argdescr_cell_t
					   meltxresdescr_[],
					   union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_161_warmelt_genobj_COMPILOBJ_NREP_CHECK_RUNNING_MODULE_ENVIRONMENT_CONTAINER
(meltclosure_ptr_t meltclosp_, melt_ptr_t meltfirstargp_, const melt_argdescr_cell_t meltxargdescr_[],
union meltparam_un *meltxargtab_, const melt_argdescr_cell_t meltxresdescr_[],
union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_162_warmelt_genobj_LAMBDA___54__ (meltclosure_ptr_t meltclosp_,
					   melt_ptr_t meltfirstargp_,
					   const melt_argdescr_cell_t
					   meltxargdescr_[],
					   union meltparam_un *meltxargtab_,
					   const melt_argdescr_cell_t
					   meltxresdescr_[],
					   union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_163_warmelt_genobj_COMPILTST_ANYTESTER (meltclosure_ptr_t meltclosp_,
						 melt_ptr_t meltfirstargp_,
						 const melt_argdescr_cell_t
						 meltxargdescr_[],
						 union meltparam_un
						 *meltxargtab_,
						 const melt_argdescr_cell_t
						 meltxresdescr_[],
						 union meltparam_un
						 *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_164_warmelt_genobj_COMPILOBJ_NREP_MATCH (meltclosure_ptr_t
						  meltclosp_,
						  melt_ptr_t meltfirstargp_,
						  const melt_argdescr_cell_t
						  meltxargdescr_[],
						  union meltparam_un
						  *meltxargtab_,
						  const melt_argdescr_cell_t
						  meltxresdescr_[],
						  union meltparam_un
						  *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_165_warmelt_genobj_LAMBDA___55__ (meltclosure_ptr_t meltclosp_,
					   melt_ptr_t meltfirstargp_,
					   const melt_argdescr_cell_t
					   meltxargdescr_[],
					   union meltparam_un *meltxargtab_,
					   const melt_argdescr_cell_t
					   meltxresdescr_[],
					   union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_166_warmelt_genobj_COMPILOBJ_NREP_ALTMATCH (meltclosure_ptr_t
						     meltclosp_,
						     melt_ptr_t
						     meltfirstargp_,
						     const
						     melt_argdescr_cell_t
						     meltxargdescr_[],
						     union meltparam_un
						     *meltxargtab_,
						     const
						     melt_argdescr_cell_t
						     meltxresdescr_[],
						     union meltparam_un
						     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_167_warmelt_genobj_LAMBDA___56__ (meltclosure_ptr_t meltclosp_,
					   melt_ptr_t meltfirstargp_,
					   const melt_argdescr_cell_t
					   meltxargdescr_[],
					   union meltparam_un *meltxargtab_,
					   const melt_argdescr_cell_t
					   meltxresdescr_[],
					   union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_168_warmelt_genobj_COMPILOBJ_NREP_MATCHLABEL (meltclosure_ptr_t
						       meltclosp_,
						       melt_ptr_t
						       meltfirstargp_,
						       const
						       melt_argdescr_cell_t
						       meltxargdescr_[],
						       union meltparam_un
						       *meltxargtab_,
						       const
						       melt_argdescr_cell_t
						       meltxresdescr_[],
						       union meltparam_un
						       *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_169_warmelt_genobj_COMPILOBJ_NREP_MATCHFLAG (meltclosure_ptr_t
						      meltclosp_,
						      melt_ptr_t
						      meltfirstargp_,
						      const
						      melt_argdescr_cell_t
						      meltxargdescr_[],
						      union meltparam_un
						      *meltxargtab_,
						      const
						      melt_argdescr_cell_t
						      meltxresdescr_[],
						      union meltparam_un
						      *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_170_warmelt_genobj_COMPILOBJ_NREP_MATCHDATAINIT (meltclosure_ptr_t
							  meltclosp_,
							  melt_ptr_t
							  meltfirstargp_,
							  const
							  melt_argdescr_cell_t
							  meltxargdescr_[],
							  union meltparam_un
							  *meltxargtab_,
							  const
							  melt_argdescr_cell_t
							  meltxresdescr_[],
							  union meltparam_un
							  *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_171_warmelt_genobj_COMPILOBJ_NREP_MATCHEDATA (meltclosure_ptr_t
						       meltclosp_,
						       melt_ptr_t
						       meltfirstargp_,
						       const
						       melt_argdescr_cell_t
						       meltxargdescr_[],
						       union meltparam_un
						       *meltxargtab_,
						       const
						       melt_argdescr_cell_t
						       meltxresdescr_[],
						       union meltparam_un
						       *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_172_warmelt_genobj_COMPILOBJ_NREP_MATCHJUMP (meltclosure_ptr_t
						      meltclosp_,
						      melt_ptr_t
						      meltfirstargp_,
						      const
						      melt_argdescr_cell_t
						      meltxargdescr_[],
						      union meltparam_un
						      *meltxargtab_,
						      const
						      melt_argdescr_cell_t
						      meltxresdescr_[],
						      union meltparam_un
						      *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_173_warmelt_genobj_NORMTESTER_LABELINSTR (meltclosure_ptr_t
						   meltclosp_,
						   melt_ptr_t meltfirstargp_,
						   const melt_argdescr_cell_t
						   meltxargdescr_[],
						   union meltparam_un
						   *meltxargtab_,
						   const melt_argdescr_cell_t
						   meltxresdescr_[],
						   union meltparam_un
						   *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_174_warmelt_genobj_NORMTESTER_GOTOINSTR (meltclosure_ptr_t
						  meltclosp_,
						  melt_ptr_t meltfirstargp_,
						  const melt_argdescr_cell_t
						  meltxargdescr_[],
						  union meltparam_un
						  *meltxargtab_,
						  const melt_argdescr_cell_t
						  meltxresdescr_[],
						  union meltparam_un
						  *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_175_warmelt_genobj_ENDMATCH_GOTOINSTR (meltclosure_ptr_t meltclosp_,
						melt_ptr_t meltfirstargp_,
						const melt_argdescr_cell_t
						meltxargdescr_[],
						union meltparam_un
						*meltxargtab_,
						const melt_argdescr_cell_t
						meltxresdescr_[],
						union meltparam_un
						*meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_176_warmelt_genobj_TESTMATCH_GOTOINSTR (meltclosure_ptr_t meltclosp_,
						 melt_ptr_t meltfirstargp_,
						 const melt_argdescr_cell_t
						 meltxargdescr_[],
						 union meltparam_un
						 *meltxargtab_,
						 const melt_argdescr_cell_t
						 meltxresdescr_[],
						 union meltparam_un
						 *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_177_warmelt_genobj_NORMTESTER_FREE_OBJLOC_LIST (meltclosure_ptr_t
							 meltclosp_,
							 melt_ptr_t
							 meltfirstargp_,
							 const
							 melt_argdescr_cell_t
							 meltxargdescr_[],
							 union meltparam_un
							 *meltxargtab_,
							 const
							 melt_argdescr_cell_t
							 meltxresdescr_[],
							 union meltparam_un
							 *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_178_warmelt_genobj_LAMBDA___57__ (meltclosure_ptr_t meltclosp_,
					   melt_ptr_t meltfirstargp_,
					   const melt_argdescr_cell_t
					   meltxargdescr_[],
					   union meltparam_un *meltxargtab_,
					   const melt_argdescr_cell_t
					   meltxresdescr_[],
					   union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_179_warmelt_genobj_COMPILTST_NORMTESTER_ANY (meltclosure_ptr_t
						      meltclosp_,
						      melt_ptr_t
						      meltfirstargp_,
						      const
						      melt_argdescr_cell_t
						      meltxargdescr_[],
						      union meltparam_un
						      *meltxargtab_,
						      const
						      melt_argdescr_cell_t
						      meltxresdescr_[],
						      union meltparam_un
						      *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_180_warmelt_genobj_COMPILTST_NORMTESTER_MATCHER (meltclosure_ptr_t
							  meltclosp_,
							  melt_ptr_t
							  meltfirstargp_,
							  const
							  melt_argdescr_cell_t
							  meltxargdescr_[],
							  union meltparam_un
							  *meltxargtab_,
							  const
							  melt_argdescr_cell_t
							  meltxresdescr_[],
							  union meltparam_un
							  *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_181_warmelt_genobj_COMPILTST_NORMTESTER_INSTANCE (meltclosure_ptr_t
							   meltclosp_,
							   melt_ptr_t
							   meltfirstargp_,
							   const
							   melt_argdescr_cell_t
							   meltxargdescr_[],
							   union meltparam_un
							   *meltxargtab_,
							   const
							   melt_argdescr_cell_t
							   meltxresdescr_[],
							   union meltparam_un
							   *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_182_warmelt_genobj_COMPILTST_NORMTESTER_TUPLE (meltclosure_ptr_t
							meltclosp_,
							melt_ptr_t
							meltfirstargp_,
							const
							melt_argdescr_cell_t
							meltxargdescr_[],
							union meltparam_un
							*meltxargtab_,
							const
							melt_argdescr_cell_t
							meltxresdescr_[],
							union meltparam_un
							*meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_183_warmelt_genobj_COMPILTST_NORMTESTER_SAME (meltclosure_ptr_t
						       meltclosp_,
						       melt_ptr_t
						       meltfirstargp_,
						       const
						       melt_argdescr_cell_t
						       meltxargdescr_[],
						       union meltparam_un
						       *meltxargtab_,
						       const
						       melt_argdescr_cell_t
						       meltxresdescr_[],
						       union meltparam_un
						       *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_184_warmelt_genobj_COMPILTST_NORMTESTER_SUCCESS (meltclosure_ptr_t
							  meltclosp_,
							  melt_ptr_t
							  meltfirstargp_,
							  const
							  melt_argdescr_cell_t
							  meltxargdescr_[],
							  union meltparam_un
							  *meltxargtab_,
							  const
							  melt_argdescr_cell_t
							  meltxresdescr_[],
							  union meltparam_un
							  *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_185_warmelt_genobj_COMPILTST_NORMTESTER_ORCLEAR (meltclosure_ptr_t
							  meltclosp_,
							  melt_ptr_t
							  meltfirstargp_,
							  const
							  melt_argdescr_cell_t
							  meltxargdescr_[],
							  union meltparam_un
							  *meltxargtab_,
							  const
							  melt_argdescr_cell_t
							  meltxresdescr_[],
							  union meltparam_un
							  *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_186_warmelt_genobj_COMPILTST_NORMTESTER_ORTRANSMIT (meltclosure_ptr_t
							     meltclosp_,
							     melt_ptr_t
							     meltfirstargp_,
							     const
							     melt_argdescr_cell_t
							     meltxargdescr_[],
							     union
							     meltparam_un
							     *meltxargtab_,
							     const
							     melt_argdescr_cell_t
							     meltxresdescr_[],
							     union
							     meltparam_un
							     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_187_warmelt_genobj_COMPILTST_NORMTESTER_DISJUNCTION
(meltclosure_ptr_t meltclosp_, melt_ptr_t meltfirstargp_, const melt_argdescr_cell_t meltxargdescr_[],
union meltparam_un *meltxargtab_, const melt_argdescr_cell_t meltxresdescr_[],
union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_188_warmelt_genobj_COMPILMATCHER_CMATCHER (meltclosure_ptr_t
						    meltclosp_,
						    melt_ptr_t meltfirstargp_,
						    const melt_argdescr_cell_t
						    meltxargdescr_[],
						    union meltparam_un
						    *meltxargtab_,
						    const melt_argdescr_cell_t
						    meltxresdescr_[],
						    union meltparam_un
						    *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_189_warmelt_genobj_LAMBDA___58__ (meltclosure_ptr_t meltclosp_,
					   melt_ptr_t meltfirstargp_,
					   const melt_argdescr_cell_t
					   meltxargdescr_[],
					   union meltparam_un *meltxargtab_,
					   const melt_argdescr_cell_t
					   meltxresdescr_[],
					   union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_190_warmelt_genobj_LAMBDA___59__ (meltclosure_ptr_t meltclosp_,
					   melt_ptr_t meltfirstargp_,
					   const melt_argdescr_cell_t
					   meltxargdescr_[],
					   union meltparam_un *meltxargtab_,
					   const melt_argdescr_cell_t
					   meltxresdescr_[],
					   union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_191_warmelt_genobj_LAMBDA___60__ (meltclosure_ptr_t meltclosp_,
					   melt_ptr_t meltfirstargp_,
					   const melt_argdescr_cell_t
					   meltxargdescr_[],
					   union meltparam_un *meltxargtab_,
					   const melt_argdescr_cell_t
					   meltxresdescr_[],
					   union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_192_warmelt_genobj_LAMBDA___61__ (meltclosure_ptr_t meltclosp_,
					   melt_ptr_t meltfirstargp_,
					   const melt_argdescr_cell_t
					   meltxargdescr_[],
					   union meltparam_un *meltxargtab_,
					   const melt_argdescr_cell_t
					   meltxresdescr_[],
					   union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_193_warmelt_genobj_COMPILMATCHER_FUNMATCHER (meltclosure_ptr_t
						      meltclosp_,
						      melt_ptr_t
						      meltfirstargp_,
						      const
						      melt_argdescr_cell_t
						      meltxargdescr_[],
						      union meltparam_un
						      *meltxargtab_,
						      const
						      melt_argdescr_cell_t
						      meltxresdescr_[],
						      union meltparam_un
						      *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_194_warmelt_genobj_LAMBDA___62__ (meltclosure_ptr_t meltclosp_,
					   melt_ptr_t meltfirstargp_,
					   const melt_argdescr_cell_t
					   meltxargdescr_[],
					   union meltparam_un *meltxargtab_,
					   const melt_argdescr_cell_t
					   meltxresdescr_[],
					   union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_195_warmelt_genobj_LAMBDA___63__ (meltclosure_ptr_t meltclosp_,
					   melt_ptr_t meltfirstargp_,
					   const melt_argdescr_cell_t
					   meltxargdescr_[],
					   union meltparam_un *meltxargtab_,
					   const melt_argdescr_cell_t
					   meltxresdescr_[],
					   union meltparam_un *meltxrestab_);



MELT_EXTERN void *melt_start_this_module (void *);


/* define different names when debugging or not */
#if MELT_HAVE_DEBUG
MELT_EXTERN const char meltmodule_warmelt_genobj__melt_have_debug_enabled[];
#define melt_have_debug_string meltmodule_warmelt_genobj__melt_have_debug_enabled
#else /*!MELT_HAVE_DEBUG */
MELT_EXTERN const char meltmodule_warmelt_genobj__melt_have_debug_disabled[];
#define melt_have_debug_string meltmodule_warmelt_genobj__melt_have_debug_disabled
#endif /*!MELT_HAVE_DEBUG */


struct frame_melt_start_this_module_st;
void MELT_MODULE_VISIBILITY
meltmod__warmelt_genobj__initialmeltchunk_0 (struct
					     frame_melt_start_this_module_st
					     *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_genobj__initialmeltchunk_1 (struct
					     frame_melt_start_this_module_st
					     *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_genobj__initialmeltchunk_2 (struct
					     frame_melt_start_this_module_st
					     *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_genobj__initialmeltchunk_3 (struct
					     frame_melt_start_this_module_st
					     *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_genobj__initialmeltchunk_4 (struct
					     frame_melt_start_this_module_st
					     *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_genobj__initialmeltchunk_5 (struct
					     frame_melt_start_this_module_st
					     *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_genobj__initialmeltchunk_6 (struct
					     frame_melt_start_this_module_st
					     *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_genobj__initialmeltchunk_7 (struct
					     frame_melt_start_this_module_st
					     *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_genobj__initialmeltchunk_8 (struct
					     frame_melt_start_this_module_st
					     *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_genobj__initialmeltchunk_9 (struct
					     frame_melt_start_this_module_st
					     *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_genobj__initialmeltchunk_10 (struct
					      frame_melt_start_this_module_st
					      *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_genobj__initialmeltchunk_11 (struct
					      frame_melt_start_this_module_st
					      *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_genobj__initialmeltchunk_12 (struct
					      frame_melt_start_this_module_st
					      *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_genobj__initialmeltchunk_13 (struct
					      frame_melt_start_this_module_st
					      *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_genobj__initialmeltchunk_14 (struct
					      frame_melt_start_this_module_st
					      *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_genobj__initialmeltchunk_15 (struct
					      frame_melt_start_this_module_st
					      *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_genobj__initialmeltchunk_16 (struct
					      frame_melt_start_this_module_st
					      *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_genobj__initialmeltchunk_17 (struct
					      frame_melt_start_this_module_st
					      *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_genobj__initialmeltchunk_18 (struct
					      frame_melt_start_this_module_st
					      *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_genobj__initialmeltchunk_19 (struct
					      frame_melt_start_this_module_st
					      *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_genobj__initialmeltchunk_20 (struct
					      frame_melt_start_this_module_st
					      *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_genobj__initialmeltchunk_21 (struct
					      frame_melt_start_this_module_st
					      *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_genobj__initialmeltchunk_22 (struct
					      frame_melt_start_this_module_st
					      *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_genobj__initialmeltchunk_23 (struct
					      frame_melt_start_this_module_st
					      *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_genobj__initialmeltchunk_24 (struct
					      frame_melt_start_this_module_st
					      *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_genobj__initialmeltchunk_25 (struct
					      frame_melt_start_this_module_st
					      *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_genobj__initialmeltchunk_26 (struct
					      frame_melt_start_this_module_st
					      *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_genobj__initialmeltchunk_27 (struct
					      frame_melt_start_this_module_st
					      *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_genobj__initialmeltchunk_28 (struct
					      frame_melt_start_this_module_st
					      *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_genobj__initialmeltchunk_29 (struct
					      frame_melt_start_this_module_st
					      *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_genobj__initialmeltchunk_30 (struct
					      frame_melt_start_this_module_st
					      *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_genobj__initialmeltchunk_31 (struct
					      frame_melt_start_this_module_st
					      *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_genobj__initialmeltchunk_32 (struct
					      frame_melt_start_this_module_st
					      *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_genobj__initialmeltchunk_33 (struct
					      frame_melt_start_this_module_st
					      *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_genobj__initialmeltchunk_34 (struct
					      frame_melt_start_this_module_st
					      *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_genobj__initialmeltchunk_35 (struct
					      frame_melt_start_this_module_st
					      *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_genobj__initialmeltchunk_36 (struct
					      frame_melt_start_this_module_st
					      *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_genobj__initialmeltchunk_37 (struct
					      frame_melt_start_this_module_st
					      *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_genobj__initialmeltchunk_38 (struct
					      frame_melt_start_this_module_st
					      *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_genobj__initialmeltchunk_39 (struct
					      frame_melt_start_this_module_st
					      *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_genobj__initialmeltchunk_40 (struct
					      frame_melt_start_this_module_st
					      *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_genobj__initialmeltchunk_41 (struct
					      frame_melt_start_this_module_st
					      *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_genobj__initialmeltchunk_42 (struct
					      frame_melt_start_this_module_st
					      *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_genobj__initialmeltchunk_43 (struct
					      frame_melt_start_this_module_st
					      *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_genobj__initialmeltchunk_44 (struct
					      frame_melt_start_this_module_st
					      *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_genobj__initialmeltchunk_45 (struct
					      frame_melt_start_this_module_st
					      *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_genobj__forward_or_mark_module_start_frame (struct
							     melt_callframe_st
							     *fp,
							     int marking);
#define meltmarking_melt_start_this_module  meltmod__warmelt_genobj__forward_or_mark_module_start_frame



/**** warmelt-genobj+04.c implementations ****/




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_121_warmelt_genobj_COMPILOBJ_NREP_IF (meltclosure_ptr_t meltclosp_,
					       melt_ptr_t meltfirstargp_,
					       const melt_argdescr_cell_t
					       meltxargdescr_[],
					       union meltparam_un
					       *meltxargtab_,
					       const melt_argdescr_cell_t
					       meltxresdescr_[],
					       union meltparam_un
					       *meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_121_warmelt_genobj_COMPILOBJ_NREP_IF_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_121_warmelt_genobj_COMPILOBJ_NREP_IF_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 44
    melt_ptr_t mcfr_varptr[44];
#define MELTFRAM_NBVARNUM 15
    long mcfr_varnum[15];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_121_warmelt_genobj_COMPILOBJ_NREP_IF is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_121_warmelt_genobj_COMPILOBJ_NREP_IF_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 44; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_121_warmelt_genobj_COMPILOBJ_NREP_IF nbval 44*/
  meltfram__.mcfr_nbvar = 44 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("COMPILOBJ_NREP_IF", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-genobj.melt:4201:/ getarg");
 /*_.RIF__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.GCX__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.GCX__V3*/ meltfptr[2])) != NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:4202:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L1*/ meltfnum[0] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.RIF__V2*/ meltfptr[1]),
			     (melt_ptr_t) (( /*!CLASS_NREP_IF */ meltfrout->
					    tabval[0])));;
      MELT_LOCATION ("warmelt-genobj.melt:4202:/ cond");
      /*cond */ if ( /*_#IS_A__L1*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V5*/ meltfptr[4] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-genobj.melt:4202:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check rif"),
				  ("warmelt-genobj.melt")
				  ? ("warmelt-genobj.melt") : __FILE__,
				  (4202) ? (4202) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V5*/ meltfptr[4] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V4*/ meltfptr[3] = /*_.IFELSE___V5*/ meltfptr[4];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:4202:/ clear");
	     /*clear *//*_#IS_A__L1*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V5*/ meltfptr[4] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V4*/ meltfptr[3] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:4203:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L2*/ meltfnum[0] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.GCX__V3*/ meltfptr[2]),
			     (melt_ptr_t) (( /*!CLASS_C_GENERATION_CONTEXT */
					    meltfrout->tabval[1])));;
      MELT_LOCATION ("warmelt-genobj.melt:4203:/ cond");
      /*cond */ if ( /*_#IS_A__L2*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V7*/ meltfptr[6] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-genobj.melt:4203:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check gcx"),
				  ("warmelt-genobj.melt")
				  ? ("warmelt-genobj.melt") : __FILE__,
				  (4203) ? (4203) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V7*/ meltfptr[6] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V6*/ meltfptr[4] = /*_.IFELSE___V7*/ meltfptr[6];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:4203:/ clear");
	     /*clear *//*_#IS_A__L2*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V7*/ meltfptr[6] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V6*/ meltfptr[4] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:4204:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L3*/ meltfnum[0] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-genobj.melt:4204:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L3*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L4*/ meltfnum[3] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-genobj.melt:4204:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L4*/ meltfnum[3];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-genobj.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 4204;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "compilobj_nrep_if rif=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.RIF__V2*/ meltfptr[1];
	      /*_.MELT_DEBUG_FUN__V10*/ meltfptr[9] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[2])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V9*/ meltfptr[8] =
	      /*_.MELT_DEBUG_FUN__V10*/ meltfptr[9];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-genobj.melt:4204:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L4*/ meltfnum[3] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V10*/ meltfptr[9] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V9*/ meltfptr[8] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-genobj.melt:4204:/ quasiblock");


      /*_.PROGN___V11*/ meltfptr[9] = /*_.IF___V9*/ meltfptr[8];;
      /*^compute */
      /*_.IFCPP___V8*/ meltfptr[6] = /*_.PROGN___V11*/ meltfptr[9];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:4204:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L3*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IF___V9*/ meltfptr[8] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V11*/ meltfptr[9] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V8*/ meltfptr[6] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-genobj.melt:4205:/ quasiblock");


    /*^getslot */
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.RIF__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 0, "NREP_LOC");
  /*_.LOC__V13*/ meltfptr[9] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-genobj.melt:4206:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.RIF__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 4, "NIF_TEST");
  /*_.NTEST__V14*/ meltfptr[13] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-genobj.melt:4207:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.RIF__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 2, "NIF_THEN");
  /*_.NTHEN__V15*/ meltfptr[14] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-genobj.melt:4208:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.RIF__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 3, "NIF_ELSE");
  /*_.NELSE__V16*/ meltfptr[15] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-genobj.melt:4209:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.RIF__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 1, "NEXPR_CTYP");
  /*_.NCTYP__V17*/ meltfptr[16] = slot;
    };
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:4211:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L5*/ meltfnum[3] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.NCTYP__V17*/ meltfptr[16]),
			     (melt_ptr_t) (( /*!CLASS_CTYPE */ meltfrout->
					    tabval[3])));;
      MELT_LOCATION ("warmelt-genobj.melt:4211:/ cond");
      /*cond */ if ( /*_#IS_A__L5*/ meltfnum[3])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V19*/ meltfptr[18] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-genobj.melt:4211:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check nctyp"),
				  ("warmelt-genobj.melt")
				  ? ("warmelt-genobj.melt") : __FILE__,
				  (4211) ? (4211) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V19*/ meltfptr[18] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V18*/ meltfptr[17] = /*_.IFELSE___V19*/ meltfptr[18];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:4211:/ clear");
	     /*clear *//*_#IS_A__L5*/ meltfnum[3] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V19*/ meltfptr[18] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V18*/ meltfptr[17] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-genobj.melt:4212:/ quasiblock");


    /*^checksignal */
    MELT_CHECK_SIGNAL ();
    ;
    /*^msend */
    /*msend */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^ojbmsend.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.GCX__V3*/ meltfptr[2];
      /*_.OTEST__V21*/ meltfptr[20] =
	meltgc_send ((melt_ptr_t) ( /*_.NTEST__V14*/ meltfptr[13]),
		     (melt_ptr_t) (( /*!COMPILE_OBJ */ meltfrout->tabval[4])),
		     (MELTBPARSTR_PTR ""), argtab, "",
		     (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-genobj.melt:4213:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^msend */
    /*msend */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^ojbmsend.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.GCX__V3*/ meltfptr[2];
      /*_.OTHEN__V22*/ meltfptr[21] =
	meltgc_send ((melt_ptr_t) ( /*_.NTHEN__V15*/ meltfptr[14]),
		     (melt_ptr_t) (( /*!COMPILE_OBJ */ meltfrout->tabval[4])),
		     (MELTBPARSTR_PTR ""), argtab, "",
		     (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-genobj.melt:4214:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#IS_NOT_A__L6*/ meltfnum[0] =
      !melt_is_instance_of ((melt_ptr_t) ( /*_.NELSE__V16*/ meltfptr[15]),
			    (melt_ptr_t) (( /*!CLASS_NREP_NIL */ meltfrout->
					   tabval[5])));;
    MELT_LOCATION ("warmelt-genobj.melt:4214:/ cond");
    /*cond */ if ( /*_#IS_NOT_A__L6*/ meltfnum[0])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-genobj.melt:4215:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^msend */
	  /*msend */
	  {
	    union meltparam_un argtab[1];
	    memset (&argtab, 0, sizeof (argtab));
	    /*^ojbmsend.arg */
	    argtab[0].meltbp_aptr =
	      (melt_ptr_t *) & /*_.GCX__V3*/ meltfptr[2];
	    /*_.COMPILE_OBJ__V24*/ meltfptr[23] =
	      meltgc_send ((melt_ptr_t) ( /*_.NELSE__V16*/ meltfptr[15]),
			   (melt_ptr_t) (( /*!COMPILE_OBJ */ meltfrout->
					  tabval[4])), (MELTBPARSTR_PTR ""),
			   argtab, "", (union meltparam_un *) 0);
	  }
	  ;
	  /*_.OELSE__V23*/ meltfptr[22] =
	    /*_.COMPILE_OBJ__V24*/ meltfptr[23];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-genobj.melt:4214:/ clear");
	     /*clear *//*_.COMPILE_OBJ__V24*/ meltfptr[23] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

  /*_.OELSE__V23*/ meltfptr[22] = NULL;;
      }
    ;
    MELT_LOCATION ("warmelt-genobj.melt:4216:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*^rawallocobj */
    /*rawallocobj */
    {
      melt_ptr_t newobj = 0;
      melt_raw_object_create (newobj,
			      (melt_ptr_t) (( /*!CLASS_OBJCOND */ meltfrout->
					     tabval[6])), (4),
			      "CLASS_OBJCOND");
  /*_.INST__V26*/ meltfptr[25] =
	newobj;
    };
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OBI_LOC",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V26*/ meltfptr[25])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V26*/ meltfptr[25]), (0),
			  ( /*_.LOC__V13*/ meltfptr[9]), "OBI_LOC");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OBCOND_TEST",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V26*/ meltfptr[25])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V26*/ meltfptr[25]), (1),
			  ( /*_.OTEST__V21*/ meltfptr[20]), "OBCOND_TEST");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OBCOND_THEN",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V26*/ meltfptr[25])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V26*/ meltfptr[25]), (2),
			  ( /*_.OTHEN__V22*/ meltfptr[21]), "OBCOND_THEN");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OBCOND_ELSE",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V26*/ meltfptr[25])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V26*/ meltfptr[25]), (3),
			  ( /*_.OELSE__V23*/ meltfptr[22]), "OBCOND_ELSE");
    ;
    /*^touchobj */

    melt_dbgtrace_written_object ( /*_.INST__V26*/ meltfptr[25],
				  "newly made instance");
    ;
    /*_.OBIF__V25*/ meltfptr[23] = /*_.INST__V26*/ meltfptr[25];;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:4222:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L7*/ meltfnum[3] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-genobj.melt:4222:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L7*/ meltfnum[3])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L8*/ meltfnum[7] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-genobj.melt:4222:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[7];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L8*/ meltfnum[7];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-genobj.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 4222;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "compilobj_nrep_if ntest=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.NTEST__V14*/ meltfptr[13];
	      /*^apply.arg */
	      argtab[5].meltbp_cstring = " otest=";
	      /*^apply.arg */
	      argtab[6].meltbp_aptr =
		(melt_ptr_t *) & /*_.OTEST__V21*/ meltfptr[20];
	      /*_.MELT_DEBUG_FUN__V29*/ meltfptr[28] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[2])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V28*/ meltfptr[27] =
	      /*_.MELT_DEBUG_FUN__V29*/ meltfptr[28];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-genobj.melt:4222:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L8*/ meltfnum[7] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V29*/ meltfptr[28] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V28*/ meltfptr[27] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-genobj.melt:4222:/ quasiblock");


      /*_.PROGN___V30*/ meltfptr[28] = /*_.IF___V28*/ meltfptr[27];;
      /*^compute */
      /*_.IFCPP___V27*/ meltfptr[26] = /*_.PROGN___V30*/ meltfptr[28];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:4222:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L7*/ meltfnum[3] = 0;
      /*^clear */
	     /*clear *//*_.IF___V28*/ meltfptr[27] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V30*/ meltfptr[28] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V27*/ meltfptr[26] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:4223:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L9*/ meltfnum[7] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-genobj.melt:4223:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L9*/ meltfnum[7])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L10*/ meltfnum[3] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-genobj.melt:4223:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[7];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long =
		/*_#THE_MELTCALLCOUNT__L10*/ meltfnum[3];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-genobj.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 4223;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "compilobj_nrep_if nthen=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.NTHEN__V15*/ meltfptr[14];
	      /*^apply.arg */
	      argtab[5].meltbp_cstring = " otest=";
	      /*^apply.arg */
	      argtab[6].meltbp_aptr =
		(melt_ptr_t *) & /*_.OTHEN__V22*/ meltfptr[21];
	      /*_.MELT_DEBUG_FUN__V33*/ meltfptr[32] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[2])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V32*/ meltfptr[28] =
	      /*_.MELT_DEBUG_FUN__V33*/ meltfptr[32];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-genobj.melt:4223:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L10*/ meltfnum[3] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V33*/ meltfptr[32] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V32*/ meltfptr[28] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-genobj.melt:4223:/ quasiblock");


      /*_.PROGN___V34*/ meltfptr[32] = /*_.IF___V32*/ meltfptr[28];;
      /*^compute */
      /*_.IFCPP___V31*/ meltfptr[27] = /*_.PROGN___V34*/ meltfptr[32];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:4223:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L9*/ meltfnum[7] = 0;
      /*^clear */
	     /*clear *//*_.IF___V32*/ meltfptr[28] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V34*/ meltfptr[32] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V31*/ meltfptr[27] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:4224:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L11*/ meltfnum[3] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-genobj.melt:4224:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L11*/ meltfnum[3])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L12*/ meltfnum[7] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-genobj.melt:4224:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[7];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long =
		/*_#THE_MELTCALLCOUNT__L12*/ meltfnum[7];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-genobj.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 4224;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "compilobj_nrep_if nthen=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.NELSE__V16*/ meltfptr[15];
	      /*^apply.arg */
	      argtab[5].meltbp_cstring = " otest=";
	      /*^apply.arg */
	      argtab[6].meltbp_aptr =
		(melt_ptr_t *) & /*_.OELSE__V23*/ meltfptr[22];
	      /*_.MELT_DEBUG_FUN__V37*/ meltfptr[36] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[2])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V36*/ meltfptr[32] =
	      /*_.MELT_DEBUG_FUN__V37*/ meltfptr[36];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-genobj.melt:4224:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L12*/ meltfnum[7] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V37*/ meltfptr[36] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V36*/ meltfptr[32] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-genobj.melt:4224:/ quasiblock");


      /*_.PROGN___V38*/ meltfptr[36] = /*_.IF___V36*/ meltfptr[32];;
      /*^compute */
      /*_.IFCPP___V35*/ meltfptr[28] = /*_.PROGN___V38*/ meltfptr[36];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:4224:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L11*/ meltfnum[3] = 0;
      /*^clear */
	     /*clear *//*_.IF___V36*/ meltfptr[32] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V38*/ meltfptr[36] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V35*/ meltfptr[28] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:4225:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L13*/ meltfnum[7] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-genobj.melt:4225:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L13*/ meltfnum[7])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L14*/ meltfnum[3] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-genobj.melt:4225:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long =
		/*_#THE_MELTCALLCOUNT__L14*/ meltfnum[3];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-genobj.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 4225;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "compilobj_nrep_if obif=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.OBIF__V25*/ meltfptr[23];
	      /*_.MELT_DEBUG_FUN__V41*/ meltfptr[40] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[2])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V40*/ meltfptr[36] =
	      /*_.MELT_DEBUG_FUN__V41*/ meltfptr[40];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-genobj.melt:4225:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L14*/ meltfnum[3] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V41*/ meltfptr[40] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V40*/ meltfptr[36] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-genobj.melt:4225:/ quasiblock");


      /*_.PROGN___V42*/ meltfptr[40] = /*_.IF___V40*/ meltfptr[36];;
      /*^compute */
      /*_.IFCPP___V39*/ meltfptr[32] = /*_.PROGN___V42*/ meltfptr[40];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:4225:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L13*/ meltfnum[7] = 0;
      /*^clear */
	     /*clear *//*_.IF___V40*/ meltfptr[36] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V42*/ meltfptr[40] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V39*/ meltfptr[32] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:4226:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#NOTNULL__L15*/ meltfnum[3] =
	(( /*_.OTEST__V21*/ meltfptr[20]) != NULL);;
      MELT_LOCATION ("warmelt-genobj.melt:4226:/ cond");
      /*cond */ if ( /*_#NOTNULL__L15*/ meltfnum[3])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V44*/ meltfptr[40] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-genobj.melt:4226:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check otest"),
				  ("warmelt-genobj.melt")
				  ? ("warmelt-genobj.melt") : __FILE__,
				  (4226) ? (4226) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V44*/ meltfptr[40] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V43*/ meltfptr[36] = /*_.IFELSE___V44*/ meltfptr[40];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:4226:/ clear");
	     /*clear *//*_#NOTNULL__L15*/ meltfnum[3] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V44*/ meltfptr[40] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V43*/ meltfptr[36] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    /*^compute */
    /*_.LET___V20*/ meltfptr[18] = /*_.OBIF__V25*/ meltfptr[23];;

    MELT_LOCATION ("warmelt-genobj.melt:4212:/ clear");
	   /*clear *//*_.OTEST__V21*/ meltfptr[20] = 0;
    /*^clear */
	   /*clear *//*_.OTHEN__V22*/ meltfptr[21] = 0;
    /*^clear */
	   /*clear *//*_#IS_NOT_A__L6*/ meltfnum[0] = 0;
    /*^clear */
	   /*clear *//*_.OELSE__V23*/ meltfptr[22] = 0;
    /*^clear */
	   /*clear *//*_.OBIF__V25*/ meltfptr[23] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V27*/ meltfptr[26] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V31*/ meltfptr[27] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V35*/ meltfptr[28] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V39*/ meltfptr[32] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V43*/ meltfptr[36] = 0;
    /*_.LET___V12*/ meltfptr[8] = /*_.LET___V20*/ meltfptr[18];;

    MELT_LOCATION ("warmelt-genobj.melt:4205:/ clear");
	   /*clear *//*_.LOC__V13*/ meltfptr[9] = 0;
    /*^clear */
	   /*clear *//*_.NTEST__V14*/ meltfptr[13] = 0;
    /*^clear */
	   /*clear *//*_.NTHEN__V15*/ meltfptr[14] = 0;
    /*^clear */
	   /*clear *//*_.NELSE__V16*/ meltfptr[15] = 0;
    /*^clear */
	   /*clear *//*_.NCTYP__V17*/ meltfptr[16] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V18*/ meltfptr[17] = 0;
    /*^clear */
	   /*clear *//*_.LET___V20*/ meltfptr[18] = 0;
    MELT_LOCATION ("warmelt-genobj.melt:4201:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.LET___V12*/ meltfptr[8];;

    {
      MELT_LOCATION ("warmelt-genobj.melt:4201:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V4*/ meltfptr[3] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V6*/ meltfptr[4] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V8*/ meltfptr[6] = 0;
    /*^clear */
	   /*clear *//*_.LET___V12*/ meltfptr[8] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("COMPILOBJ_NREP_IF", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_121_warmelt_genobj_COMPILOBJ_NREP_IF_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_121_warmelt_genobj_COMPILOBJ_NREP_IF */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_122_warmelt_genobj_COMPILOBJ_NREP_IFISA (meltclosure_ptr_t
						  meltclosp_,
						  melt_ptr_t meltfirstargp_,
						  const melt_argdescr_cell_t
						  meltxargdescr_[],
						  union meltparam_un *
						  meltxargtab_,
						  const melt_argdescr_cell_t
						  meltxresdescr_[],
						  union meltparam_un *
						  meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_122_warmelt_genobj_COMPILOBJ_NREP_IFISA_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_122_warmelt_genobj_COMPILOBJ_NREP_IFISA_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 33
    melt_ptr_t mcfr_varptr[33];
#define MELTFRAM_NBVARNUM 8
    long mcfr_varnum[8];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_122_warmelt_genobj_COMPILOBJ_NREP_IFISA is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_122_warmelt_genobj_COMPILOBJ_NREP_IFISA_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 33; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_122_warmelt_genobj_COMPILOBJ_NREP_IFISA nbval 33*/
  meltfram__.mcfr_nbvar = 33 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("COMPILOBJ_NREP_IFISA", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-genobj.melt:4233:/ getarg");
 /*_.RIF__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.GCX__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.GCX__V3*/ meltfptr[2])) != NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:4234:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L1*/ meltfnum[0] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-genobj.melt:4234:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L1*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-genobj.melt:4234:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-genobj.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 4234;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "compilobj_nrep_ifisa rif=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.RIF__V2*/ meltfptr[1];
	      /*_.MELT_DEBUG_FUN__V6*/ meltfptr[5] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V5*/ meltfptr[4] = /*_.MELT_DEBUG_FUN__V6*/ meltfptr[5];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-genobj.melt:4234:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V6*/ meltfptr[5] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V5*/ meltfptr[4] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-genobj.melt:4234:/ quasiblock");


      /*_.PROGN___V7*/ meltfptr[5] = /*_.IF___V5*/ meltfptr[4];;
      /*^compute */
      /*_.IFCPP___V4*/ meltfptr[3] = /*_.PROGN___V7*/ meltfptr[5];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:4234:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L1*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IF___V5*/ meltfptr[4] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V7*/ meltfptr[5] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V4*/ meltfptr[3] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:4235:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L3*/ meltfnum[1] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.RIF__V2*/ meltfptr[1]),
			     (melt_ptr_t) (( /*!CLASS_NREP_IFISA */
					    meltfrout->tabval[1])));;
      MELT_LOCATION ("warmelt-genobj.melt:4235:/ cond");
      /*cond */ if ( /*_#IS_A__L3*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V9*/ meltfptr[5] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-genobj.melt:4235:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check rif"),
				  ("warmelt-genobj.melt")
				  ? ("warmelt-genobj.melt") : __FILE__,
				  (4235) ? (4235) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V9*/ meltfptr[5] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V8*/ meltfptr[4] = /*_.IFELSE___V9*/ meltfptr[5];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:4235:/ clear");
	     /*clear *//*_#IS_A__L3*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V9*/ meltfptr[5] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V8*/ meltfptr[4] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:4236:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L4*/ meltfnum[0] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.GCX__V3*/ meltfptr[2]),
			     (melt_ptr_t) (( /*!CLASS_C_GENERATION_CONTEXT */
					    meltfrout->tabval[2])));;
      MELT_LOCATION ("warmelt-genobj.melt:4236:/ cond");
      /*cond */ if ( /*_#IS_A__L4*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V11*/ meltfptr[10] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-genobj.melt:4236:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check gcx"),
				  ("warmelt-genobj.melt")
				  ? ("warmelt-genobj.melt") : __FILE__,
				  (4236) ? (4236) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V11*/ meltfptr[10] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V10*/ meltfptr[5] = /*_.IFELSE___V11*/ meltfptr[10];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:4236:/ clear");
	     /*clear *//*_#IS_A__L4*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V11*/ meltfptr[10] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V10*/ meltfptr[5] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-genobj.melt:4237:/ quasiblock");


    /*^getslot */
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.RIF__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 0, "NREP_LOC");
  /*_.LOC__V13*/ meltfptr[12] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-genobj.melt:4238:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.RIF__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 2, "NIF_THEN");
  /*_.NTHEN__V14*/ meltfptr[13] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-genobj.melt:4239:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.RIF__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 3, "NIF_ELSE");
  /*_.NELSE__V15*/ meltfptr[14] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-genobj.melt:4240:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.RIF__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 1, "NEXPR_CTYP");
  /*_.NCTYP__V16*/ meltfptr[15] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-genobj.melt:4241:/ cond");
    /*cond */ if (
		   /*ifisa */
		   melt_is_instance_of ((melt_ptr_t)
					( /*_.RIF__V2*/ meltfptr[1]),
					(melt_ptr_t) (( /*!CLASS_NREP_IFTESTVALUE */ meltfrout->tabval[3])))
      )				/*then */
      {
	/*^cond.then */
	/*^getslot */
	{
	  melt_ptr_t slot = NULL, obj = NULL;
	  obj = (melt_ptr_t) ( /*_.RIF__V2*/ meltfptr[1]) /*=obj*/ ;
	  melt_object_get_field (slot, obj, 4, "NIF_TESTVAL");
   /*_.NVAL__V17*/ meltfptr[16] = slot;
	};
	;
      }
    else
      {				/*^cond.else */

  /*_.NVAL__V17*/ meltfptr[16] = NULL;;
      }
    ;
    MELT_LOCATION ("warmelt-genobj.melt:4242:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.RIF__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 5, "NIFA_CLASS");
  /*_.NCLA__V18*/ meltfptr[17] = slot;
    };
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:4244:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L5*/ meltfnum[1] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.NCTYP__V16*/ meltfptr[15]),
			     (melt_ptr_t) (( /*!CLASS_CTYPE */ meltfrout->
					    tabval[4])));;
      MELT_LOCATION ("warmelt-genobj.melt:4244:/ cond");
      /*cond */ if ( /*_#IS_A__L5*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V20*/ meltfptr[19] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-genobj.melt:4244:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check nctyp"),
				  ("warmelt-genobj.melt")
				  ? ("warmelt-genobj.melt") : __FILE__,
				  (4244) ? (4244) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V20*/ meltfptr[19] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V19*/ meltfptr[18] = /*_.IFELSE___V20*/ meltfptr[19];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:4244:/ clear");
	     /*clear *//*_#IS_A__L5*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V20*/ meltfptr[19] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V19*/ meltfptr[18] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-genobj.melt:4245:/ quasiblock");


    /*^checksignal */
    MELT_CHECK_SIGNAL ();
    ;
    /*^msend */
    /*msend */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^ojbmsend.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.GCX__V3*/ meltfptr[2];
      /*_.OTHEN__V22*/ meltfptr[21] =
	meltgc_send ((melt_ptr_t) ( /*_.NTHEN__V14*/ meltfptr[13]),
		     (melt_ptr_t) (( /*!COMPILE_OBJ */ meltfrout->tabval[5])),
		     (MELTBPARSTR_PTR ""), argtab, "",
		     (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-genobj.melt:4246:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^msend */
    /*msend */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^ojbmsend.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.GCX__V3*/ meltfptr[2];
      /*_.OVAL__V23*/ meltfptr[22] =
	meltgc_send ((melt_ptr_t) ( /*_.NVAL__V17*/ meltfptr[16]),
		     (melt_ptr_t) (( /*!COMPILE_OBJ */ meltfrout->tabval[5])),
		     (MELTBPARSTR_PTR ""), argtab, "",
		     (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-genobj.melt:4247:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^msend */
    /*msend */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^ojbmsend.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.GCX__V3*/ meltfptr[2];
      /*_.OCLA__V24*/ meltfptr[23] =
	meltgc_send ((melt_ptr_t) ( /*_.NCLA__V18*/ meltfptr[17]),
		     (melt_ptr_t) (( /*!COMPILE_OBJ */ meltfrout->tabval[5])),
		     (MELTBPARSTR_PTR ""), argtab, "",
		     (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-genobj.melt:4248:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#IS_NOT_A__L6*/ meltfnum[0] =
      !melt_is_instance_of ((melt_ptr_t) ( /*_.NELSE__V15*/ meltfptr[14]),
			    (melt_ptr_t) (( /*!CLASS_NREP_NIL */ meltfrout->
					   tabval[6])));;
    MELT_LOCATION ("warmelt-genobj.melt:4248:/ cond");
    /*cond */ if ( /*_#IS_NOT_A__L6*/ meltfnum[0])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-genobj.melt:4249:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^msend */
	  /*msend */
	  {
	    union meltparam_un argtab[1];
	    memset (&argtab, 0, sizeof (argtab));
	    /*^ojbmsend.arg */
	    argtab[0].meltbp_aptr =
	      (melt_ptr_t *) & /*_.GCX__V3*/ meltfptr[2];
	    /*_.COMPILE_OBJ__V26*/ meltfptr[25] =
	      meltgc_send ((melt_ptr_t) ( /*_.NELSE__V15*/ meltfptr[14]),
			   (melt_ptr_t) (( /*!COMPILE_OBJ */ meltfrout->
					  tabval[5])), (MELTBPARSTR_PTR ""),
			   argtab, "", (union meltparam_un *) 0);
	  }
	  ;
	  /*_.OELSE__V25*/ meltfptr[24] =
	    /*_.COMPILE_OBJ__V26*/ meltfptr[25];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-genobj.melt:4248:/ clear");
	     /*clear *//*_.COMPILE_OBJ__V26*/ meltfptr[25] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

  /*_.OELSE__V25*/ meltfptr[24] = NULL;;
      }
    ;
    MELT_LOCATION ("warmelt-genobj.melt:4250:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[6];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & ( /*!CTYPE_LONG */ meltfrout->tabval[8]);
      /*^apply.arg */
      argtab[1].meltbp_cstring =
	"/*ifisa*/ melt_is_instance_of((melt_ptr_t)(";
      /*^apply.arg */
      argtab[2].meltbp_aptr = (melt_ptr_t *) & /*_.OVAL__V23*/ meltfptr[22];
      /*^apply.arg */
      argtab[3].meltbp_cstring =
	"), \n                                                             \
          (melt_ptr_t)(";
      /*^apply.arg */
      argtab[4].meltbp_aptr = (melt_ptr_t *) & /*_.OCLA__V24*/ meltfptr[23];
      /*^apply.arg */
      argtab[5].meltbp_cstring = "))\n\t\t\t\t      ";
      /*_.OTEST__V27*/ meltfptr[25] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!MAKE_OBJLOCATEDEXP */ meltfrout->tabval[7])),
		    (melt_ptr_t) ( /*_.LOC__V13*/ meltfptr[12]),
		    (MELTBPARSTR_PTR MELTBPARSTR_CSTRING MELTBPARSTR_PTR
		     MELTBPARSTR_CSTRING MELTBPARSTR_PTR MELTBPARSTR_CSTRING
		     ""), argtab, "", (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-genobj.melt:4254:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*^rawallocobj */
    /*rawallocobj */
    {
      melt_ptr_t newobj = 0;
      melt_raw_object_create (newobj,
			      (melt_ptr_t) (( /*!CLASS_OBJCOND */ meltfrout->
					     tabval[9])), (4),
			      "CLASS_OBJCOND");
  /*_.INST__V29*/ meltfptr[28] =
	newobj;
    };
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OBI_LOC",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V29*/ meltfptr[28])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V29*/ meltfptr[28]), (0),
			  ( /*_.LOC__V13*/ meltfptr[12]), "OBI_LOC");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OBCOND_TEST",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V29*/ meltfptr[28])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V29*/ meltfptr[28]), (1),
			  ( /*_.OTEST__V27*/ meltfptr[25]), "OBCOND_TEST");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OBCOND_THEN",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V29*/ meltfptr[28])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V29*/ meltfptr[28]), (2),
			  ( /*_.OTHEN__V22*/ meltfptr[21]), "OBCOND_THEN");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OBCOND_ELSE",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V29*/ meltfptr[28])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V29*/ meltfptr[28]), (3),
			  ( /*_.OELSE__V25*/ meltfptr[24]), "OBCOND_ELSE");
    ;
    /*^touchobj */

    melt_dbgtrace_written_object ( /*_.INST__V29*/ meltfptr[28],
				  "newly made instance");
    ;
    /*_.OBIF__V28*/ meltfptr[27] = /*_.INST__V29*/ meltfptr[28];;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:4260:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L7*/ meltfnum[1] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-genobj.melt:4260:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L7*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L8*/ meltfnum[7] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-genobj.melt:4260:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L8*/ meltfnum[7];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-genobj.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 4260;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "compilobj_nrep_ifisa obif=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.OBIF__V28*/ meltfptr[27];
	      /*_.MELT_DEBUG_FUN__V32*/ meltfptr[31] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V31*/ meltfptr[30] =
	      /*_.MELT_DEBUG_FUN__V32*/ meltfptr[31];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-genobj.melt:4260:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L8*/ meltfnum[7] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V32*/ meltfptr[31] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V31*/ meltfptr[30] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-genobj.melt:4260:/ quasiblock");


      /*_.PROGN___V33*/ meltfptr[31] = /*_.IF___V31*/ meltfptr[30];;
      /*^compute */
      /*_.IFCPP___V30*/ meltfptr[29] = /*_.PROGN___V33*/ meltfptr[31];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:4260:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L7*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IF___V31*/ meltfptr[30] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V33*/ meltfptr[31] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V30*/ meltfptr[29] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    /*^compute */
    /*_.LET___V21*/ meltfptr[19] = /*_.OBIF__V28*/ meltfptr[27];;

    MELT_LOCATION ("warmelt-genobj.melt:4245:/ clear");
	   /*clear *//*_.OTHEN__V22*/ meltfptr[21] = 0;
    /*^clear */
	   /*clear *//*_.OVAL__V23*/ meltfptr[22] = 0;
    /*^clear */
	   /*clear *//*_.OCLA__V24*/ meltfptr[23] = 0;
    /*^clear */
	   /*clear *//*_#IS_NOT_A__L6*/ meltfnum[0] = 0;
    /*^clear */
	   /*clear *//*_.OELSE__V25*/ meltfptr[24] = 0;
    /*^clear */
	   /*clear *//*_.OTEST__V27*/ meltfptr[25] = 0;
    /*^clear */
	   /*clear *//*_.OBIF__V28*/ meltfptr[27] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V30*/ meltfptr[29] = 0;
    /*_.LET___V12*/ meltfptr[10] = /*_.LET___V21*/ meltfptr[19];;

    MELT_LOCATION ("warmelt-genobj.melt:4237:/ clear");
	   /*clear *//*_.LOC__V13*/ meltfptr[12] = 0;
    /*^clear */
	   /*clear *//*_.NTHEN__V14*/ meltfptr[13] = 0;
    /*^clear */
	   /*clear *//*_.NELSE__V15*/ meltfptr[14] = 0;
    /*^clear */
	   /*clear *//*_.NCTYP__V16*/ meltfptr[15] = 0;
    /*^clear */
	   /*clear *//*_.NVAL__V17*/ meltfptr[16] = 0;
    /*^clear */
	   /*clear *//*_.NCLA__V18*/ meltfptr[17] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V19*/ meltfptr[18] = 0;
    /*^clear */
	   /*clear *//*_.LET___V21*/ meltfptr[19] = 0;
    MELT_LOCATION ("warmelt-genobj.melt:4233:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.LET___V12*/ meltfptr[10];;

    {
      MELT_LOCATION ("warmelt-genobj.melt:4233:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V4*/ meltfptr[3] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V8*/ meltfptr[4] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V10*/ meltfptr[5] = 0;
    /*^clear */
	   /*clear *//*_.LET___V12*/ meltfptr[10] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("COMPILOBJ_NREP_IFISA", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_122_warmelt_genobj_COMPILOBJ_NREP_IFISA_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_122_warmelt_genobj_COMPILOBJ_NREP_IFISA */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_123_warmelt_genobj_COMPILOBJ_NREP_IFTUPLESIZED (meltclosure_ptr_t
							 meltclosp_,
							 melt_ptr_t
							 meltfirstargp_,
							 const
							 melt_argdescr_cell_t
							 meltxargdescr_[],
							 union meltparam_un *
							 meltxargtab_,
							 const
							 melt_argdescr_cell_t
							 meltxresdescr_[],
							 union meltparam_un *
							 meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_123_warmelt_genobj_COMPILOBJ_NREP_IFTUPLESIZED_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_123_warmelt_genobj_COMPILOBJ_NREP_IFTUPLESIZED_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 34
    melt_ptr_t mcfr_varptr[34];
#define MELTFRAM_NBVARNUM 8
    long mcfr_varnum[8];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_123_warmelt_genobj_COMPILOBJ_NREP_IFTUPLESIZED is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct
	 frame_meltrout_123_warmelt_genobj_COMPILOBJ_NREP_IFTUPLESIZED_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 34; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_123_warmelt_genobj_COMPILOBJ_NREP_IFTUPLESIZED nbval 34*/
  meltfram__.mcfr_nbvar = 34 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("COMPILOBJ_NREP_IFTUPLESIZED", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-genobj.melt:4267:/ getarg");
 /*_.RIF__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.GCX__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.GCX__V3*/ meltfptr[2])) != NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:4268:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L1*/ meltfnum[0] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-genobj.melt:4268:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L1*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-genobj.melt:4268:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-genobj.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 4268;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "compilobj_nrep_ifisa rif=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.RIF__V2*/ meltfptr[1];
	      /*_.MELT_DEBUG_FUN__V6*/ meltfptr[5] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V5*/ meltfptr[4] = /*_.MELT_DEBUG_FUN__V6*/ meltfptr[5];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-genobj.melt:4268:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V6*/ meltfptr[5] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V5*/ meltfptr[4] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-genobj.melt:4268:/ quasiblock");


      /*_.PROGN___V7*/ meltfptr[5] = /*_.IF___V5*/ meltfptr[4];;
      /*^compute */
      /*_.IFCPP___V4*/ meltfptr[3] = /*_.PROGN___V7*/ meltfptr[5];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:4268:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L1*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IF___V5*/ meltfptr[4] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V7*/ meltfptr[5] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V4*/ meltfptr[3] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:4269:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L3*/ meltfnum[1] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.RIF__V2*/ meltfptr[1]),
			     (melt_ptr_t) (( /*!CLASS_NREP_IFTUPLESIZED */
					    meltfrout->tabval[1])));;
      MELT_LOCATION ("warmelt-genobj.melt:4269:/ cond");
      /*cond */ if ( /*_#IS_A__L3*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V9*/ meltfptr[5] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-genobj.melt:4269:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check rif"),
				  ("warmelt-genobj.melt")
				  ? ("warmelt-genobj.melt") : __FILE__,
				  (4269) ? (4269) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V9*/ meltfptr[5] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V8*/ meltfptr[4] = /*_.IFELSE___V9*/ meltfptr[5];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:4269:/ clear");
	     /*clear *//*_#IS_A__L3*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V9*/ meltfptr[5] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V8*/ meltfptr[4] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:4270:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L4*/ meltfnum[0] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.GCX__V3*/ meltfptr[2]),
			     (melt_ptr_t) (( /*!CLASS_C_GENERATION_CONTEXT */
					    meltfrout->tabval[2])));;
      MELT_LOCATION ("warmelt-genobj.melt:4270:/ cond");
      /*cond */ if ( /*_#IS_A__L4*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V11*/ meltfptr[10] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-genobj.melt:4270:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check gcx"),
				  ("warmelt-genobj.melt")
				  ? ("warmelt-genobj.melt") : __FILE__,
				  (4270) ? (4270) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V11*/ meltfptr[10] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V10*/ meltfptr[5] = /*_.IFELSE___V11*/ meltfptr[10];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:4270:/ clear");
	     /*clear *//*_#IS_A__L4*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V11*/ meltfptr[10] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V10*/ meltfptr[5] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-genobj.melt:4271:/ quasiblock");


    /*^getslot */
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.RIF__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 0, "NREP_LOC");
  /*_.LOC__V13*/ meltfptr[12] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-genobj.melt:4272:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.RIF__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 2, "NIF_THEN");
  /*_.NTHEN__V14*/ meltfptr[13] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-genobj.melt:4273:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.RIF__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 3, "NIF_ELSE");
  /*_.NELSE__V15*/ meltfptr[14] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-genobj.melt:4274:/ cond");
    /*cond */ if (
		   /*ifisa */
		   melt_is_instance_of ((melt_ptr_t)
					( /*_.RIF__V2*/ meltfptr[1]),
					(melt_ptr_t) (( /*!CLASS_NREP_IFTESTVALUE */ meltfrout->tabval[3])))
      )				/*then */
      {
	/*^cond.then */
	/*^getslot */
	{
	  melt_ptr_t slot = NULL, obj = NULL;
	  obj = (melt_ptr_t) ( /*_.RIF__V2*/ meltfptr[1]) /*=obj*/ ;
	  melt_object_get_field (slot, obj, 4, "NIF_TESTVAL");
   /*_.NVAL__V16*/ meltfptr[15] = slot;
	};
	;
      }
    else
      {				/*^cond.else */

  /*_.NVAL__V16*/ meltfptr[15] = NULL;;
      }
    ;
    MELT_LOCATION ("warmelt-genobj.melt:4275:/ cond");
    /*cond */ if (
		   /*ifisa */
		   melt_is_instance_of ((melt_ptr_t)
					( /*_.RIF__V2*/ meltfptr[1]),
					(melt_ptr_t) (( /*!CLASS_NREP_IFTUPLESIZED */ meltfrout->tabval[1])))
      )				/*then */
      {
	/*^cond.then */
	/*^getslot */
	{
	  melt_ptr_t slot = NULL, obj = NULL;
	  obj = (melt_ptr_t) ( /*_.RIF__V2*/ meltfptr[1]) /*=obj*/ ;
	  melt_object_get_field (slot, obj, 5, "NIF_TUPSIZ");
   /*_.NTSZ__V17*/ meltfptr[16] = slot;
	};
	;
      }
    else
      {				/*^cond.else */

  /*_.NTSZ__V17*/ meltfptr[16] = NULL;;
      }
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:4277:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L5*/ meltfnum[1] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-genobj.melt:4277:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L5*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L6*/ meltfnum[0] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-genobj.melt:4277:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[7];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L6*/ meltfnum[0];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-genobj.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 4277;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "compilobj_nrep_iftuplesized nval=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.NVAL__V16*/ meltfptr[15];
	      /*^apply.arg */
	      argtab[5].meltbp_cstring = " ntsz=";
	      /*^apply.arg */
	      argtab[6].meltbp_aptr =
		(melt_ptr_t *) & /*_.NTSZ__V17*/ meltfptr[16];
	      /*_.MELT_DEBUG_FUN__V20*/ meltfptr[19] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V19*/ meltfptr[18] =
	      /*_.MELT_DEBUG_FUN__V20*/ meltfptr[19];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-genobj.melt:4277:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L6*/ meltfnum[0] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V20*/ meltfptr[19] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V19*/ meltfptr[18] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-genobj.melt:4277:/ quasiblock");


      /*_.PROGN___V21*/ meltfptr[19] = /*_.IF___V19*/ meltfptr[18];;
      /*^compute */
      /*_.IFCPP___V18*/ meltfptr[17] = /*_.PROGN___V21*/ meltfptr[19];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:4277:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L5*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IF___V19*/ meltfptr[18] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V21*/ meltfptr[19] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V18*/ meltfptr[17] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-genobj.melt:4278:/ quasiblock");


    /*^checksignal */
    MELT_CHECK_SIGNAL ();
    ;
    /*^msend */
    /*msend */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^ojbmsend.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.GCX__V3*/ meltfptr[2];
      /*_.OTHEN__V23*/ meltfptr[19] =
	meltgc_send ((melt_ptr_t) ( /*_.NTHEN__V14*/ meltfptr[13]),
		     (melt_ptr_t) (( /*!COMPILE_OBJ */ meltfrout->tabval[4])),
		     (MELTBPARSTR_PTR ""), argtab, "",
		     (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-genobj.melt:4279:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^msend */
    /*msend */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^ojbmsend.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.GCX__V3*/ meltfptr[2];
      /*_.OVAL__V24*/ meltfptr[23] =
	meltgc_send ((melt_ptr_t) ( /*_.NVAL__V16*/ meltfptr[15]),
		     (melt_ptr_t) (( /*!COMPILE_OBJ */ meltfrout->tabval[4])),
		     (MELTBPARSTR_PTR ""), argtab, "",
		     (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-genobj.melt:4280:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^msend */
    /*msend */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^ojbmsend.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.GCX__V3*/ meltfptr[2];
      /*_.OELSE__V25*/ meltfptr[24] =
	meltgc_send ((melt_ptr_t) ( /*_.NELSE__V15*/ meltfptr[14]),
		     (melt_ptr_t) (( /*!COMPILE_OBJ */ meltfrout->tabval[4])),
		     (MELTBPARSTR_PTR ""), argtab, "",
		     (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-genobj.melt:4281:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^msend */
    /*msend */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^ojbmsend.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.GCX__V3*/ meltfptr[2];
      /*_.OVAL__V26*/ meltfptr[25] =
	meltgc_send ((melt_ptr_t) ( /*_.NVAL__V16*/ meltfptr[15]),
		     (melt_ptr_t) (( /*!COMPILE_OBJ */ meltfrout->tabval[4])),
		     (MELTBPARSTR_PTR ""), argtab, "",
		     (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-genobj.melt:4282:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^msend */
    /*msend */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^ojbmsend.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.GCX__V3*/ meltfptr[2];
      /*_.OTSZ__V27*/ meltfptr[26] =
	meltgc_send ((melt_ptr_t) ( /*_.NTSZ__V17*/ meltfptr[16]),
		     (melt_ptr_t) (( /*!COMPILE_OBJ */ meltfrout->tabval[4])),
		     (MELTBPARSTR_PTR ""), argtab, "",
		     (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-genobj.melt:4284:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[8];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & ( /*!CTYPE_LONG */ meltfrout->tabval[6]);
      /*^apply.arg */
      argtab[1].meltbp_cstring =
	"/*iftuplesized*/ melt_magic_discr((melt_ptr_t)(";
      /*^apply.arg */
      argtab[2].meltbp_aptr = (melt_ptr_t *) & /*_.OVAL__V26*/ meltfptr[25];
      /*^apply.arg */
      argtab[3].meltbp_cstring =
	")) == MELTOBMAG_MULTIPLE\n\t\t\t\t && ((meltmultiple_ptr_t)(";
      /*^apply.arg */
      argtab[4].meltbp_aptr = (melt_ptr_t *) & /*_.OVAL__V26*/ meltfptr[25];
      /*^apply.arg */
      argtab[5].meltbp_cstring = "))->nbval == (int)(";
      /*^apply.arg */
      argtab[6].meltbp_aptr = (melt_ptr_t *) & /*_.OTSZ__V27*/ meltfptr[26];
      /*^apply.arg */
      argtab[7].meltbp_cstring = ")";
      /*_.OTEST__V28*/ meltfptr[27] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!MAKE_OBJLOCATEDEXP */ meltfrout->tabval[5])),
		    (melt_ptr_t) ( /*_.LOC__V13*/ meltfptr[12]),
		    (MELTBPARSTR_PTR MELTBPARSTR_CSTRING MELTBPARSTR_PTR
		     MELTBPARSTR_CSTRING MELTBPARSTR_PTR MELTBPARSTR_CSTRING
		     MELTBPARSTR_PTR MELTBPARSTR_CSTRING ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-genobj.melt:4287:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*^rawallocobj */
    /*rawallocobj */
    {
      melt_ptr_t newobj = 0;
      melt_raw_object_create (newobj,
			      (melt_ptr_t) (( /*!CLASS_OBJCOND */ meltfrout->
					     tabval[7])), (4),
			      "CLASS_OBJCOND");
  /*_.INST__V30*/ meltfptr[29] =
	newobj;
    };
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OBI_LOC",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V30*/ meltfptr[29])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V30*/ meltfptr[29]), (0),
			  ( /*_.LOC__V13*/ meltfptr[12]), "OBI_LOC");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OBCOND_TEST",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V30*/ meltfptr[29])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V30*/ meltfptr[29]), (1),
			  ( /*_.OTEST__V28*/ meltfptr[27]), "OBCOND_TEST");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OBCOND_THEN",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V30*/ meltfptr[29])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V30*/ meltfptr[29]), (2),
			  ( /*_.OTHEN__V23*/ meltfptr[19]), "OBCOND_THEN");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OBCOND_ELSE",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V30*/ meltfptr[29])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V30*/ meltfptr[29]), (3),
			  ( /*_.OELSE__V25*/ meltfptr[24]), "OBCOND_ELSE");
    ;
    /*^touchobj */

    melt_dbgtrace_written_object ( /*_.INST__V30*/ meltfptr[29],
				  "newly made instance");
    ;
    /*_.OBIF__V29*/ meltfptr[28] = /*_.INST__V30*/ meltfptr[29];;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:4293:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L7*/ meltfnum[0] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-genobj.melt:4293:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L7*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L8*/ meltfnum[1] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-genobj.melt:4293:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L8*/ meltfnum[1];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-genobj.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 4293;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "compilobj_nrep_iftuplesized obif=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.OBIF__V29*/ meltfptr[28];
	      /*_.MELT_DEBUG_FUN__V33*/ meltfptr[32] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V32*/ meltfptr[31] =
	      /*_.MELT_DEBUG_FUN__V33*/ meltfptr[32];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-genobj.melt:4293:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L8*/ meltfnum[1] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V33*/ meltfptr[32] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V32*/ meltfptr[31] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-genobj.melt:4293:/ quasiblock");


      /*_.PROGN___V34*/ meltfptr[32] = /*_.IF___V32*/ meltfptr[31];;
      /*^compute */
      /*_.IFCPP___V31*/ meltfptr[30] = /*_.PROGN___V34*/ meltfptr[32];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:4293:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L7*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IF___V32*/ meltfptr[31] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V34*/ meltfptr[32] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V31*/ meltfptr[30] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    /*^compute */
    /*_.LET___V22*/ meltfptr[18] = /*_.OBIF__V29*/ meltfptr[28];;

    MELT_LOCATION ("warmelt-genobj.melt:4278:/ clear");
	   /*clear *//*_.OTHEN__V23*/ meltfptr[19] = 0;
    /*^clear */
	   /*clear *//*_.OVAL__V24*/ meltfptr[23] = 0;
    /*^clear */
	   /*clear *//*_.OELSE__V25*/ meltfptr[24] = 0;
    /*^clear */
	   /*clear *//*_.OVAL__V26*/ meltfptr[25] = 0;
    /*^clear */
	   /*clear *//*_.OTSZ__V27*/ meltfptr[26] = 0;
    /*^clear */
	   /*clear *//*_.OTEST__V28*/ meltfptr[27] = 0;
    /*^clear */
	   /*clear *//*_.OBIF__V29*/ meltfptr[28] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V31*/ meltfptr[30] = 0;
    /*_.LET___V12*/ meltfptr[10] = /*_.LET___V22*/ meltfptr[18];;

    MELT_LOCATION ("warmelt-genobj.melt:4271:/ clear");
	   /*clear *//*_.LOC__V13*/ meltfptr[12] = 0;
    /*^clear */
	   /*clear *//*_.NTHEN__V14*/ meltfptr[13] = 0;
    /*^clear */
	   /*clear *//*_.NELSE__V15*/ meltfptr[14] = 0;
    /*^clear */
	   /*clear *//*_.NVAL__V16*/ meltfptr[15] = 0;
    /*^clear */
	   /*clear *//*_.NTSZ__V17*/ meltfptr[16] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V18*/ meltfptr[17] = 0;
    /*^clear */
	   /*clear *//*_.LET___V22*/ meltfptr[18] = 0;
    MELT_LOCATION ("warmelt-genobj.melt:4267:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.LET___V12*/ meltfptr[10];;

    {
      MELT_LOCATION ("warmelt-genobj.melt:4267:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V4*/ meltfptr[3] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V8*/ meltfptr[4] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V10*/ meltfptr[5] = 0;
    /*^clear */
	   /*clear *//*_.LET___V12*/ meltfptr[10] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("COMPILOBJ_NREP_IFTUPLESIZED", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_123_warmelt_genobj_COMPILOBJ_NREP_IFTUPLESIZED_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_123_warmelt_genobj_COMPILOBJ_NREP_IFTUPLESIZED */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_124_warmelt_genobj_COMPILOBJ_NREP_IFVARIADIC (meltclosure_ptr_t
						       meltclosp_,
						       melt_ptr_t
						       meltfirstargp_,
						       const
						       melt_argdescr_cell_t
						       meltxargdescr_[],
						       union meltparam_un *
						       meltxargtab_,
						       const
						       melt_argdescr_cell_t
						       meltxresdescr_[],
						       union meltparam_un *
						       meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_124_warmelt_genobj_COMPILOBJ_NREP_IFVARIADIC_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_124_warmelt_genobj_COMPILOBJ_NREP_IFVARIADIC_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 106
    melt_ptr_t mcfr_varptr[106];
#define MELTFRAM_NBVARNUM 26
    long mcfr_varnum[26];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_124_warmelt_genobj_COMPILOBJ_NREP_IFVARIADIC is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_124_warmelt_genobj_COMPILOBJ_NREP_IFVARIADIC_st
	 *) meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 106; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_124_warmelt_genobj_COMPILOBJ_NREP_IFVARIADIC nbval 106*/
  meltfram__.mcfr_nbvar = 106 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("COMPILOBJ_NREP_IFVARIADIC", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-genobj.melt:4299:/ getarg");
 /*_.RIF__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.GCX__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.GCX__V3*/ meltfptr[2])) != NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:4300:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L1*/ meltfnum[0] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.RIF__V2*/ meltfptr[1]),
			     (melt_ptr_t) (( /*!CLASS_NREP_IFVARIADIC */
					    meltfrout->tabval[0])));;
      MELT_LOCATION ("warmelt-genobj.melt:4300:/ cond");
      /*cond */ if ( /*_#IS_A__L1*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V5*/ meltfptr[4] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-genobj.melt:4300:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check rif"),
				  ("warmelt-genobj.melt")
				  ? ("warmelt-genobj.melt") : __FILE__,
				  (4300) ? (4300) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V5*/ meltfptr[4] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V4*/ meltfptr[3] = /*_.IFELSE___V5*/ meltfptr[4];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:4300:/ clear");
	     /*clear *//*_#IS_A__L1*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V5*/ meltfptr[4] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V4*/ meltfptr[3] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:4301:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L2*/ meltfnum[0] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.GCX__V3*/ meltfptr[2]),
			     (melt_ptr_t) (( /*!CLASS_C_GENERATION_CONTEXT */
					    meltfrout->tabval[1])));;
      MELT_LOCATION ("warmelt-genobj.melt:4301:/ cond");
      /*cond */ if ( /*_#IS_A__L2*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V7*/ meltfptr[6] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-genobj.melt:4301:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check gcx"),
				  ("warmelt-genobj.melt")
				  ? ("warmelt-genobj.melt") : __FILE__,
				  (4301) ? (4301) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V7*/ meltfptr[6] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V6*/ meltfptr[4] = /*_.IFELSE___V7*/ meltfptr[6];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:4301:/ clear");
	     /*clear *//*_#IS_A__L2*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V7*/ meltfptr[6] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V6*/ meltfptr[4] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:4302:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L3*/ meltfnum[0] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-genobj.melt:4302:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L3*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L4*/ meltfnum[3] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-genobj.melt:4302:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L4*/ meltfnum[3];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-genobj.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 4302;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "compilobj_nrep_ifvariadic rif=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.RIF__V2*/ meltfptr[1];
	      /*_.MELT_DEBUG_FUN__V10*/ meltfptr[9] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[2])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V9*/ meltfptr[8] =
	      /*_.MELT_DEBUG_FUN__V10*/ meltfptr[9];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-genobj.melt:4302:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L4*/ meltfnum[3] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V10*/ meltfptr[9] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V9*/ meltfptr[8] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-genobj.melt:4302:/ quasiblock");


      /*_.PROGN___V11*/ meltfptr[9] = /*_.IF___V9*/ meltfptr[8];;
      /*^compute */
      /*_.IFCPP___V8*/ meltfptr[6] = /*_.PROGN___V11*/ meltfptr[9];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:4302:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L3*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IF___V9*/ meltfptr[8] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V11*/ meltfptr[9] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V8*/ meltfptr[6] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-genobj.melt:4303:/ quasiblock");


    /*^getslot */
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.RIF__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 0, "NREP_LOC");
  /*_.LOC__V13*/ meltfptr[9] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-genobj.melt:4304:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.RIF__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 2, "NIF_THEN");
  /*_.NTHEN__V14*/ meltfptr[13] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-genobj.melt:4305:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.RIF__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 3, "NIF_ELSE");
  /*_.NELSE__V15*/ meltfptr[14] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-genobj.melt:4306:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.RIF__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 4, "NIFV_VARIADIC");
  /*_.NVARIADIC__V16*/ meltfptr[15] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-genobj.melt:4307:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.RIF__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 5, "NIFV_CTYPES");
  /*_.NVCTYPES__V17*/ meltfptr[16] = slot;
    };
    ;
 /*_#NBVARARG__L5*/ meltfnum[3] =
      (melt_multiple_length
       ((melt_ptr_t) ( /*_.NVCTYPES__V17*/ meltfptr[16])));;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:4310:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_MULTIPLE_OR_NULL__L6*/ meltfnum[0] =
	(( /*_.NVCTYPES__V17*/ meltfptr[16]) == NULL
	 ||
	 (melt_unsafe_magic_discr
	  ((melt_ptr_t) ( /*_.NVCTYPES__V17*/ meltfptr[16])) ==
	  MELTOBMAG_MULTIPLE));;
      MELT_LOCATION ("warmelt-genobj.melt:4310:/ cond");
      /*cond */ if ( /*_#IS_MULTIPLE_OR_NULL__L6*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V19*/ meltfptr[18] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-genobj.melt:4310:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check nvctypes"),
				  ("warmelt-genobj.melt")
				  ? ("warmelt-genobj.melt") : __FILE__,
				  (4310) ? (4310) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V19*/ meltfptr[18] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V18*/ meltfptr[17] = /*_.IFELSE___V19*/ meltfptr[18];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:4310:/ clear");
	     /*clear *//*_#IS_MULTIPLE_OR_NULL__L6*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V19*/ meltfptr[18] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V18*/ meltfptr[17] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:4311:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L7*/ meltfnum[0] =
	melt_is_instance_of ((melt_ptr_t)
			     ( /*_.NVARIADIC__V16*/ meltfptr[15]),
			     (melt_ptr_t) (( /*!CLASS_SYMBOL */ meltfrout->
					    tabval[3])));;
      MELT_LOCATION ("warmelt-genobj.melt:4311:/ cond");
      /*cond */ if ( /*_#IS_A__L7*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V21*/ meltfptr[20] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-genobj.melt:4311:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check nvariadic"),
				  ("warmelt-genobj.melt")
				  ? ("warmelt-genobj.melt") : __FILE__,
				  (4311) ? (4311) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V21*/ meltfptr[20] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V20*/ meltfptr[18] = /*_.IFELSE___V21*/ meltfptr[20];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:4311:/ clear");
	     /*clear *//*_#IS_A__L7*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V21*/ meltfptr[20] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V20*/ meltfptr[18] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:4312:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L8*/ meltfnum[0] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-genobj.melt:4312:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L8*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L9*/ meltfnum[8] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-genobj.melt:4312:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[9];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L9*/ meltfnum[8];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-genobj.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 4312;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "compilobj_nrep_ifvariadic nthen=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.NTHEN__V14*/ meltfptr[13];
	      /*^apply.arg */
	      argtab[5].meltbp_cstring = " nelse=";
	      /*^apply.arg */
	      argtab[6].meltbp_aptr =
		(melt_ptr_t *) & /*_.NELSE__V15*/ meltfptr[14];
	      /*^apply.arg */
	      argtab[7].meltbp_cstring = " nvariadic=";
	      /*^apply.arg */
	      argtab[8].meltbp_aptr =
		(melt_ptr_t *) & /*_.NVARIADIC__V16*/ meltfptr[15];
	      /*_.MELT_DEBUG_FUN__V24*/ meltfptr[23] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[2])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V23*/ meltfptr[22] =
	      /*_.MELT_DEBUG_FUN__V24*/ meltfptr[23];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-genobj.melt:4312:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L9*/ meltfnum[8] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V24*/ meltfptr[23] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V23*/ meltfptr[22] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-genobj.melt:4312:/ quasiblock");


      /*_.PROGN___V25*/ meltfptr[23] = /*_.IF___V23*/ meltfptr[22];;
      /*^compute */
      /*_.IFCPP___V22*/ meltfptr[20] = /*_.PROGN___V25*/ meltfptr[23];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:4312:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L8*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IF___V23*/ meltfptr[22] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V25*/ meltfptr[23] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V22*/ meltfptr[20] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-genobj.melt:4313:/ quasiblock");


    MELT_LOCATION ("warmelt-genobj.melt:4314:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      /*_.OVARIADICINDEX__V27*/ meltfptr[23] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!VARIADIC_INDEX_IDSTR */ meltfrout->tabval[4])),
		    (melt_ptr_t) ( /*_.NVARIADIC__V16*/ meltfptr[15]), (""),
		    (union meltparam_un *) 0, "", (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-genobj.melt:4315:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      /*_.OVARIADICLENGTH__V28*/ meltfptr[27] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!VARIADIC_LENGTH_IDSTR */ meltfrout->tabval[5])),
		    (melt_ptr_t) ( /*_.NVARIADIC__V16*/ meltfptr[15]), (""),
		    (union meltparam_un *) 0, "", (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-genobj.melt:4317:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#I__L10*/ meltfnum[8] =
      (( /*_#NBVARARG__L5*/ meltfnum[3]) <= (0));;
    MELT_LOCATION ("warmelt-genobj.melt:4317:/ cond");
    /*cond */ if ( /*_#I__L10*/ meltfnum[8])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

   /*_.MAKE_STRINGCONST__V30*/ meltfptr[29] =
	    (meltgc_new_stringdup
	     ((meltobject_ptr_t)
	      (( /*!DISCR_VERBATIM_STRING */ meltfrout->tabval[6])),
	      ("/*ifvariadic nomore*/ ")));;
	  /*^compute */
   /*_.MAKE_STRINGCONST__V31*/ meltfptr[30] =
	    (meltgc_new_stringdup
	     ((meltobject_ptr_t)
	      (( /*!DISCR_VERBATIM_STRING */ meltfrout->tabval[6])),
	      (" == ")));;
	  MELT_LOCATION ("warmelt-genobj.melt:4318:/ blockmultialloc");
	  /*multiallocblock */
	  {
	    struct meltletrec_1_st
	    {
	      struct meltpair_st rpair_0__PAIROFLIST_x1;
	      struct meltpair_st rpair_1__OVARIADICINDEX_x1;
	      struct meltpair_st rpair_2__PAIROFLIST_x2;
	      struct meltpair_st rpair_3__OVARIADICLENGTH_x1;
	      struct meltlist_st rlist_4__LIST_;
	      long meltletrec_1_endgap;
	    } *meltletrec_1_ptr = 0;
	    meltletrec_1_ptr =
	      (struct meltletrec_1_st *)
	      meltgc_allocate (sizeof (struct meltletrec_1_st), 0);
	    /*^blockmultialloc.initfill */
	    /*inipair rpair_0__PAIROFLIST_x1 */
     /*_.PAIROFLIST__V33*/ meltfptr[32] =
	      (melt_ptr_t) & meltletrec_1_ptr->rpair_0__PAIROFLIST_x1;
	    meltletrec_1_ptr->rpair_0__PAIROFLIST_x1.discr =
	      (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (DISCR_PAIR))));

	    /*inipair rpair_1__OVARIADICINDEX_x1 */
     /*_.OVARIADICINDEX__V34*/ meltfptr[33] =
	      (melt_ptr_t) & meltletrec_1_ptr->rpair_1__OVARIADICINDEX_x1;
	    meltletrec_1_ptr->rpair_1__OVARIADICINDEX_x1.discr =
	      (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (DISCR_PAIR))));

	    /*inipair rpair_2__PAIROFLIST_x2 */
     /*_.PAIROFLIST__V35*/ meltfptr[34] =
	      (melt_ptr_t) & meltletrec_1_ptr->rpair_2__PAIROFLIST_x2;
	    meltletrec_1_ptr->rpair_2__PAIROFLIST_x2.discr =
	      (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (DISCR_PAIR))));

	    /*inipair rpair_3__OVARIADICLENGTH_x1 */
     /*_.OVARIADICLENGTH__V36*/ meltfptr[35] =
	      (melt_ptr_t) & meltletrec_1_ptr->rpair_3__OVARIADICLENGTH_x1;
	    meltletrec_1_ptr->rpair_3__OVARIADICLENGTH_x1.discr =
	      (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (DISCR_PAIR))));

	    /*inilist rlist_4__LIST_ */
     /*_.LIST___V37*/ meltfptr[36] =
	      (melt_ptr_t) & meltletrec_1_ptr->rlist_4__LIST_;
	    meltletrec_1_ptr->rlist_4__LIST_.discr =
	      (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (DISCR_LIST))));



	    MELT_LOCATION ("warmelt-genobj.melt:4319:/ putpairhead");
	    /*putpairhead */
	    melt_assertmsg ("putpairhead /7 checkpair",
			    melt_magic_discr ((melt_ptr_t)
					      ( /*_.PAIROFLIST__V33*/
					       meltfptr[32])) ==
			    MELTOBMAG_PAIR);
	    ((meltpair_ptr_t) ( /*_.PAIROFLIST__V33*/ meltfptr[32]))->hd =
	      (melt_ptr_t) ( /*_.MAKE_STRINGCONST__V30*/ meltfptr[29]);
	    ;
	    /*^putpairtail */
	    /*putpairtail */
	    melt_assertmsg ("putpairtail /3b02a38d checkpair",
			    melt_magic_discr ((melt_ptr_t)
					      ( /*_.PAIROFLIST__V33*/
					       meltfptr[32])) ==
			    MELTOBMAG_PAIR);
	    ((meltpair_ptr_t) ( /*_.PAIROFLIST__V33*/ meltfptr[32]))->tl =
	      (meltpair_ptr_t) ( /*_.OVARIADICINDEX__V34*/ meltfptr[33]);
	    ;
	    /*^touch */
	    meltgc_touch ( /*_.PAIROFLIST__V33*/ meltfptr[32]);
	    ;
	    MELT_LOCATION ("warmelt-genobj.melt:4318:/ putpairhead");
	    /*putpairhead */
	    melt_assertmsg ("putpairhead /8 checkpair",
			    melt_magic_discr ((melt_ptr_t)
					      ( /*_.OVARIADICINDEX__V34*/
					       meltfptr[33])) ==
			    MELTOBMAG_PAIR);
	    ((meltpair_ptr_t) ( /*_.OVARIADICINDEX__V34*/ meltfptr[33]))->hd =
	      (melt_ptr_t) ( /*_.OVARIADICINDEX__V27*/ meltfptr[23]);
	    ;
	    /*^putpairtail */
	    /*putpairtail */
	    melt_assertmsg ("putpairtail /204f89fa checkpair",
			    melt_magic_discr ((melt_ptr_t)
					      ( /*_.OVARIADICINDEX__V34*/
					       meltfptr[33])) ==
			    MELTOBMAG_PAIR);
	    ((meltpair_ptr_t) ( /*_.OVARIADICINDEX__V34*/ meltfptr[33]))->tl =
	      (meltpair_ptr_t) ( /*_.PAIROFLIST__V35*/ meltfptr[34]);
	    ;
	    /*^touch */
	    meltgc_touch ( /*_.OVARIADICINDEX__V34*/ meltfptr[33]);
	    ;
	    MELT_LOCATION ("warmelt-genobj.melt:4321:/ putpairhead");
	    /*putpairhead */
	    melt_assertmsg ("putpairhead /9 checkpair",
			    melt_magic_discr ((melt_ptr_t)
					      ( /*_.PAIROFLIST__V35*/
					       meltfptr[34])) ==
			    MELTOBMAG_PAIR);
	    ((meltpair_ptr_t) ( /*_.PAIROFLIST__V35*/ meltfptr[34]))->hd =
	      (melt_ptr_t) ( /*_.MAKE_STRINGCONST__V31*/ meltfptr[30]);
	    ;
	    /*^putpairtail */
	    /*putpairtail */
	    melt_assertmsg ("putpairtail /21fd447e checkpair",
			    melt_magic_discr ((melt_ptr_t)
					      ( /*_.PAIROFLIST__V35*/
					       meltfptr[34])) ==
			    MELTOBMAG_PAIR);
	    ((meltpair_ptr_t) ( /*_.PAIROFLIST__V35*/ meltfptr[34]))->tl =
	      (meltpair_ptr_t) ( /*_.OVARIADICLENGTH__V36*/ meltfptr[35]);
	    ;
	    /*^touch */
	    meltgc_touch ( /*_.PAIROFLIST__V35*/ meltfptr[34]);
	    ;
	    MELT_LOCATION ("warmelt-genobj.melt:4318:/ putpairhead");
	    /*putpairhead */
	    melt_assertmsg ("putpairhead /a checkpair",
			    melt_magic_discr ((melt_ptr_t)
					      ( /*_.OVARIADICLENGTH__V36*/
					       meltfptr[35])) ==
			    MELTOBMAG_PAIR);
	    ((meltpair_ptr_t) ( /*_.OVARIADICLENGTH__V36*/ meltfptr[35]))->
	      hd = (melt_ptr_t) ( /*_.OVARIADICLENGTH__V28*/ meltfptr[27]);
	    ;
	    /*^touch */
	    meltgc_touch ( /*_.OVARIADICLENGTH__V36*/ meltfptr[35]);
	    ;
	    /*^putlist */
	    /*putlist */
	    melt_assertmsg ("putlist checklist",
			    melt_magic_discr ((melt_ptr_t)
					      ( /*_.LIST___V37*/
					       meltfptr[36])) ==
			    MELTOBMAG_LIST);
	    ((meltlist_ptr_t) ( /*_.LIST___V37*/ meltfptr[36]))->first =
	      (meltpair_ptr_t) ( /*_.PAIROFLIST__V33*/ meltfptr[32]);
	    ((meltlist_ptr_t) ( /*_.LIST___V37*/ meltfptr[36]))->last =
	      (meltpair_ptr_t) ( /*_.OVARIADICLENGTH__V36*/ meltfptr[35]);
	    ;
	    /*^touch */
	    meltgc_touch ( /*_.LIST___V37*/ meltfptr[36]);
	    ;
	    /*_.LIST___V32*/ meltfptr[31] = /*_.LIST___V37*/ meltfptr[36];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-genobj.melt:4318:/ clear");
	      /*clear *//*_.PAIROFLIST__V33*/ meltfptr[32] = 0;
	    /*^clear */
	      /*clear *//*_.OVARIADICINDEX__V34*/ meltfptr[33] = 0;
	    /*^clear */
	      /*clear *//*_.PAIROFLIST__V35*/ meltfptr[34] = 0;
	    /*^clear */
	      /*clear *//*_.OVARIADICLENGTH__V36*/ meltfptr[35] = 0;
	    /*^clear */
	      /*clear *//*_.LIST___V37*/ meltfptr[36] = 0;
	    /*^clear */
	      /*clear *//*_.PAIROFLIST__V33*/ meltfptr[32] = 0;
	    /*^clear */
	      /*clear *//*_.OVARIADICINDEX__V34*/ meltfptr[33] = 0;
	    /*^clear */
	      /*clear *//*_.PAIROFLIST__V35*/ meltfptr[34] = 0;
	    /*^clear */
	      /*clear *//*_.OVARIADICLENGTH__V36*/ meltfptr[35] = 0;
	    /*^clear */
	      /*clear *//*_.LIST___V37*/ meltfptr[36] = 0;
	  }			/*end multiallocblock */
	  ;
	  /*_.OTESTCHKLIST__V29*/ meltfptr[28] =
	    /*_.LIST___V32*/ meltfptr[31];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-genobj.melt:4317:/ clear");
	     /*clear *//*_.MAKE_STRINGCONST__V30*/ meltfptr[29] = 0;
	  /*^clear */
	     /*clear *//*_.MAKE_STRINGCONST__V31*/ meltfptr[30] = 0;
	  /*^clear */
	     /*clear *//*_.LIST___V32*/ meltfptr[31] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-genobj.melt:4324:/ quasiblock");


   /*_.MAKE_STRINGCONST__V39*/ meltfptr[33] =
	    (meltgc_new_stringdup
	     ((meltobject_ptr_t)
	      (( /*!DISCR_VERBATIM_STRING */ meltfrout->tabval[6])),
	      ("/*ifvariadic arg#")));;
	  /*^compute */
   /*_.MAKE_INTEGERBOX__V40*/ meltfptr[34] =
	    (meltgc_new_int
	     ((meltobject_ptr_t)
	      (( /*!DISCR_CONSTANT_INTEGER */ meltfrout->tabval[7])),
	      ( /*_#NBVARARG__L5*/ meltfnum[3])));;
	  /*^compute */
   /*_.MAKE_STRINGCONST__V41*/ meltfptr[35] =
	    (meltgc_new_stringdup
	     ((meltobject_ptr_t)
	      (( /*!DISCR_VERBATIM_STRING */ meltfrout->tabval[6])),
	      ("*/ ")));;
	  /*^compute */
   /*_.MAKE_STRINGCONST__V42*/ meltfptr[36] =
	    (meltgc_new_stringdup
	     ((meltobject_ptr_t)
	      (( /*!DISCR_VERBATIM_STRING */ meltfrout->tabval[6])),
	      (">=0 && ")));;
	  /*^compute */
   /*_.MAKE_STRINGCONST__V43*/ meltfptr[29] =
	    (meltgc_new_stringdup
	     ((meltobject_ptr_t)
	      (( /*!DISCR_VERBATIM_STRING */ meltfrout->tabval[6])),
	      (" + ")));;
	  /*^compute */
   /*_.MAKE_INTEGERBOX__V44*/ meltfptr[30] =
	    (meltgc_new_int
	     ((meltobject_ptr_t)
	      (( /*!DISCR_CONSTANT_INTEGER */ meltfrout->tabval[7])),
	      ( /*_#NBVARARG__L5*/ meltfnum[3])));;
	  /*^compute */
   /*_.MAKE_STRINGCONST__V45*/ meltfptr[31] =
	    (meltgc_new_stringdup
	     ((meltobject_ptr_t)
	      (( /*!DISCR_VERBATIM_STRING */ meltfrout->tabval[6])),
	      (" <= ")));;
	  MELT_LOCATION ("warmelt-genobj.melt:4325:/ blockmultialloc");
	  /*multiallocblock */
	  {
	    struct meltletrec_2_st
	    {
	      struct meltpair_st rpair_0__PAIROFLIST_x3;
	      struct meltpair_st rpair_1__PAIROFLIST_x4;
	      struct meltpair_st rpair_2__PAIROFLIST_x5;
	      struct meltpair_st rpair_3__OVARIADICINDEX_x2;
	      struct meltpair_st rpair_4__PAIROFLIST_x6;
	      struct meltpair_st rpair_5__OVARIADICINDEX_x3;
	      struct meltpair_st rpair_6__PAIROFLIST_x7;
	      struct meltpair_st rpair_7__PAIROFLIST_x8;
	      struct meltpair_st rpair_8__PAIROFLIST_x9;
	      struct meltpair_st rpair_9__OVARIADICLENGTH_x2;
	      struct meltlist_st rlist_10__LIST_;
	      long meltletrec_2_endgap;
	    } *meltletrec_2_ptr = 0;
	    meltletrec_2_ptr =
	      (struct meltletrec_2_st *)
	      meltgc_allocate (sizeof (struct meltletrec_2_st), 0);
	    /*^blockmultialloc.initfill */
	    /*inipair rpair_0__PAIROFLIST_x3 */
     /*_.PAIROFLIST__V47*/ meltfptr[46] =
	      (melt_ptr_t) & meltletrec_2_ptr->rpair_0__PAIROFLIST_x3;
	    meltletrec_2_ptr->rpair_0__PAIROFLIST_x3.discr =
	      (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (DISCR_PAIR))));

	    /*inipair rpair_1__PAIROFLIST_x4 */
     /*_.PAIROFLIST__V48*/ meltfptr[47] =
	      (melt_ptr_t) & meltletrec_2_ptr->rpair_1__PAIROFLIST_x4;
	    meltletrec_2_ptr->rpair_1__PAIROFLIST_x4.discr =
	      (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (DISCR_PAIR))));

	    /*inipair rpair_2__PAIROFLIST_x5 */
     /*_.PAIROFLIST__V49*/ meltfptr[48] =
	      (melt_ptr_t) & meltletrec_2_ptr->rpair_2__PAIROFLIST_x5;
	    meltletrec_2_ptr->rpair_2__PAIROFLIST_x5.discr =
	      (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (DISCR_PAIR))));

	    /*inipair rpair_3__OVARIADICINDEX_x2 */
     /*_.OVARIADICINDEX__V50*/ meltfptr[49] =
	      (melt_ptr_t) & meltletrec_2_ptr->rpair_3__OVARIADICINDEX_x2;
	    meltletrec_2_ptr->rpair_3__OVARIADICINDEX_x2.discr =
	      (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (DISCR_PAIR))));

	    /*inipair rpair_4__PAIROFLIST_x6 */
     /*_.PAIROFLIST__V51*/ meltfptr[50] =
	      (melt_ptr_t) & meltletrec_2_ptr->rpair_4__PAIROFLIST_x6;
	    meltletrec_2_ptr->rpair_4__PAIROFLIST_x6.discr =
	      (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (DISCR_PAIR))));

	    /*inipair rpair_5__OVARIADICINDEX_x3 */
     /*_.OVARIADICINDEX__V52*/ meltfptr[51] =
	      (melt_ptr_t) & meltletrec_2_ptr->rpair_5__OVARIADICINDEX_x3;
	    meltletrec_2_ptr->rpair_5__OVARIADICINDEX_x3.discr =
	      (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (DISCR_PAIR))));

	    /*inipair rpair_6__PAIROFLIST_x7 */
     /*_.PAIROFLIST__V53*/ meltfptr[52] =
	      (melt_ptr_t) & meltletrec_2_ptr->rpair_6__PAIROFLIST_x7;
	    meltletrec_2_ptr->rpair_6__PAIROFLIST_x7.discr =
	      (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (DISCR_PAIR))));

	    /*inipair rpair_7__PAIROFLIST_x8 */
     /*_.PAIROFLIST__V54*/ meltfptr[53] =
	      (melt_ptr_t) & meltletrec_2_ptr->rpair_7__PAIROFLIST_x8;
	    meltletrec_2_ptr->rpair_7__PAIROFLIST_x8.discr =
	      (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (DISCR_PAIR))));

	    /*inipair rpair_8__PAIROFLIST_x9 */
     /*_.PAIROFLIST__V55*/ meltfptr[54] =
	      (melt_ptr_t) & meltletrec_2_ptr->rpair_8__PAIROFLIST_x9;
	    meltletrec_2_ptr->rpair_8__PAIROFLIST_x9.discr =
	      (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (DISCR_PAIR))));

	    /*inipair rpair_9__OVARIADICLENGTH_x2 */
     /*_.OVARIADICLENGTH__V56*/ meltfptr[55] =
	      (melt_ptr_t) & meltletrec_2_ptr->rpair_9__OVARIADICLENGTH_x2;
	    meltletrec_2_ptr->rpair_9__OVARIADICLENGTH_x2.discr =
	      (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (DISCR_PAIR))));

	    /*inilist rlist_10__LIST_ */
     /*_.LIST___V57*/ meltfptr[56] =
	      (melt_ptr_t) & meltletrec_2_ptr->rlist_10__LIST_;
	    meltletrec_2_ptr->rlist_10__LIST_.discr =
	      (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (DISCR_LIST))));



	    MELT_LOCATION ("warmelt-genobj.melt:4326:/ putpairhead");
	    /*putpairhead */
	    melt_assertmsg ("putpairhead /b checkpair",
			    melt_magic_discr ((melt_ptr_t)
					      ( /*_.PAIROFLIST__V47*/
					       meltfptr[46])) ==
			    MELTOBMAG_PAIR);
	    ((meltpair_ptr_t) ( /*_.PAIROFLIST__V47*/ meltfptr[46]))->hd =
	      (melt_ptr_t) ( /*_.MAKE_STRINGCONST__V39*/ meltfptr[33]);
	    ;
	    /*^putpairtail */
	    /*putpairtail */
	    melt_assertmsg ("putpairtail /e9490c3 checkpair",
			    melt_magic_discr ((melt_ptr_t)
					      ( /*_.PAIROFLIST__V47*/
					       meltfptr[46])) ==
			    MELTOBMAG_PAIR);
	    ((meltpair_ptr_t) ( /*_.PAIROFLIST__V47*/ meltfptr[46]))->tl =
	      (meltpair_ptr_t) ( /*_.PAIROFLIST__V48*/ meltfptr[47]);
	    ;
	    /*^touch */
	    meltgc_touch ( /*_.PAIROFLIST__V47*/ meltfptr[46]);
	    ;
	    MELT_LOCATION ("warmelt-genobj.melt:4327:/ putpairhead");
	    /*putpairhead */
	    melt_assertmsg ("putpairhead /c checkpair",
			    melt_magic_discr ((melt_ptr_t)
					      ( /*_.PAIROFLIST__V48*/
					       meltfptr[47])) ==
			    MELTOBMAG_PAIR);
	    ((meltpair_ptr_t) ( /*_.PAIROFLIST__V48*/ meltfptr[47]))->hd =
	      (melt_ptr_t) ( /*_.MAKE_INTEGERBOX__V40*/ meltfptr[34]);
	    ;
	    /*^putpairtail */
	    /*putpairtail */
	    melt_assertmsg ("putpairtail /3fc7e98a checkpair",
			    melt_magic_discr ((melt_ptr_t)
					      ( /*_.PAIROFLIST__V48*/
					       meltfptr[47])) ==
			    MELTOBMAG_PAIR);
	    ((meltpair_ptr_t) ( /*_.PAIROFLIST__V48*/ meltfptr[47]))->tl =
	      (meltpair_ptr_t) ( /*_.PAIROFLIST__V49*/ meltfptr[48]);
	    ;
	    /*^touch */
	    meltgc_touch ( /*_.PAIROFLIST__V48*/ meltfptr[47]);
	    ;
	    MELT_LOCATION ("warmelt-genobj.melt:4328:/ putpairhead");
	    /*putpairhead */
	    melt_assertmsg ("putpairhead /d checkpair",
			    melt_magic_discr ((melt_ptr_t)
					      ( /*_.PAIROFLIST__V49*/
					       meltfptr[48])) ==
			    MELTOBMAG_PAIR);
	    ((meltpair_ptr_t) ( /*_.PAIROFLIST__V49*/ meltfptr[48]))->hd =
	      (melt_ptr_t) ( /*_.MAKE_STRINGCONST__V41*/ meltfptr[35]);
	    ;
	    /*^putpairtail */
	    /*putpairtail */
	    melt_assertmsg ("putpairtail /a584437 checkpair",
			    melt_magic_discr ((melt_ptr_t)
					      ( /*_.PAIROFLIST__V49*/
					       meltfptr[48])) ==
			    MELTOBMAG_PAIR);
	    ((meltpair_ptr_t) ( /*_.PAIROFLIST__V49*/ meltfptr[48]))->tl =
	      (meltpair_ptr_t) ( /*_.OVARIADICINDEX__V50*/ meltfptr[49]);
	    ;
	    /*^touch */
	    meltgc_touch ( /*_.PAIROFLIST__V49*/ meltfptr[48]);
	    ;
	    MELT_LOCATION ("warmelt-genobj.melt:4325:/ putpairhead");
	    /*putpairhead */
	    melt_assertmsg ("putpairhead /e checkpair",
			    melt_magic_discr ((melt_ptr_t)
					      ( /*_.OVARIADICINDEX__V50*/
					       meltfptr[49])) ==
			    MELTOBMAG_PAIR);
	    ((meltpair_ptr_t) ( /*_.OVARIADICINDEX__V50*/ meltfptr[49]))->hd =
	      (melt_ptr_t) ( /*_.OVARIADICINDEX__V27*/ meltfptr[23]);
	    ;
	    /*^putpairtail */
	    /*putpairtail */
	    melt_assertmsg ("putpairtail /18a93959 checkpair",
			    melt_magic_discr ((melt_ptr_t)
					      ( /*_.OVARIADICINDEX__V50*/
					       meltfptr[49])) ==
			    MELTOBMAG_PAIR);
	    ((meltpair_ptr_t) ( /*_.OVARIADICINDEX__V50*/ meltfptr[49]))->tl =
	      (meltpair_ptr_t) ( /*_.PAIROFLIST__V51*/ meltfptr[50]);
	    ;
	    /*^touch */
	    meltgc_touch ( /*_.OVARIADICINDEX__V50*/ meltfptr[49]);
	    ;
	    MELT_LOCATION ("warmelt-genobj.melt:4330:/ putpairhead");
	    /*putpairhead */
	    melt_assertmsg ("putpairhead /f checkpair",
			    melt_magic_discr ((melt_ptr_t)
					      ( /*_.PAIROFLIST__V51*/
					       meltfptr[50])) ==
			    MELTOBMAG_PAIR);
	    ((meltpair_ptr_t) ( /*_.PAIROFLIST__V51*/ meltfptr[50]))->hd =
	      (melt_ptr_t) ( /*_.MAKE_STRINGCONST__V42*/ meltfptr[36]);
	    ;
	    /*^putpairtail */
	    /*putpairtail */
	    melt_assertmsg ("putpairtail /6a20fe9 checkpair",
			    melt_magic_discr ((melt_ptr_t)
					      ( /*_.PAIROFLIST__V51*/
					       meltfptr[50])) ==
			    MELTOBMAG_PAIR);
	    ((meltpair_ptr_t) ( /*_.PAIROFLIST__V51*/ meltfptr[50]))->tl =
	      (meltpair_ptr_t) ( /*_.OVARIADICINDEX__V52*/ meltfptr[51]);
	    ;
	    /*^touch */
	    meltgc_touch ( /*_.PAIROFLIST__V51*/ meltfptr[50]);
	    ;
	    MELT_LOCATION ("warmelt-genobj.melt:4325:/ putpairhead");
	    /*putpairhead */
	    melt_assertmsg ("putpairhead /10 checkpair",
			    melt_magic_discr ((melt_ptr_t)
					      ( /*_.OVARIADICINDEX__V52*/
					       meltfptr[51])) ==
			    MELTOBMAG_PAIR);
	    ((meltpair_ptr_t) ( /*_.OVARIADICINDEX__V52*/ meltfptr[51]))->hd =
	      (melt_ptr_t) ( /*_.OVARIADICINDEX__V27*/ meltfptr[23]);
	    ;
	    /*^putpairtail */
	    /*putpairtail */
	    melt_assertmsg ("putpairtail /2a54d92f checkpair",
			    melt_magic_discr ((melt_ptr_t)
					      ( /*_.OVARIADICINDEX__V52*/
					       meltfptr[51])) ==
			    MELTOBMAG_PAIR);
	    ((meltpair_ptr_t) ( /*_.OVARIADICINDEX__V52*/ meltfptr[51]))->tl =
	      (meltpair_ptr_t) ( /*_.PAIROFLIST__V53*/ meltfptr[52]);
	    ;
	    /*^touch */
	    meltgc_touch ( /*_.OVARIADICINDEX__V52*/ meltfptr[51]);
	    ;
	    MELT_LOCATION ("warmelt-genobj.melt:4332:/ putpairhead");
	    /*putpairhead */
	    melt_assertmsg ("putpairhead /11 checkpair",
			    melt_magic_discr ((melt_ptr_t)
					      ( /*_.PAIROFLIST__V53*/
					       meltfptr[52])) ==
			    MELTOBMAG_PAIR);
	    ((meltpair_ptr_t) ( /*_.PAIROFLIST__V53*/ meltfptr[52]))->hd =
	      (melt_ptr_t) ( /*_.MAKE_STRINGCONST__V43*/ meltfptr[29]);
	    ;
	    /*^putpairtail */
	    /*putpairtail */
	    melt_assertmsg ("putpairtail /71193cc checkpair",
			    melt_magic_discr ((melt_ptr_t)
					      ( /*_.PAIROFLIST__V53*/
					       meltfptr[52])) ==
			    MELTOBMAG_PAIR);
	    ((meltpair_ptr_t) ( /*_.PAIROFLIST__V53*/ meltfptr[52]))->tl =
	      (meltpair_ptr_t) ( /*_.PAIROFLIST__V54*/ meltfptr[53]);
	    ;
	    /*^touch */
	    meltgc_touch ( /*_.PAIROFLIST__V53*/ meltfptr[52]);
	    ;
	    MELT_LOCATION ("warmelt-genobj.melt:4333:/ putpairhead");
	    /*putpairhead */
	    melt_assertmsg ("putpairhead /12 checkpair",
			    melt_magic_discr ((melt_ptr_t)
					      ( /*_.PAIROFLIST__V54*/
					       meltfptr[53])) ==
			    MELTOBMAG_PAIR);
	    ((meltpair_ptr_t) ( /*_.PAIROFLIST__V54*/ meltfptr[53]))->hd =
	      (melt_ptr_t) ( /*_.MAKE_INTEGERBOX__V44*/ meltfptr[30]);
	    ;
	    /*^putpairtail */
	    /*putpairtail */
	    melt_assertmsg ("putpairtail /42b5c7a checkpair",
			    melt_magic_discr ((melt_ptr_t)
					      ( /*_.PAIROFLIST__V54*/
					       meltfptr[53])) ==
			    MELTOBMAG_PAIR);
	    ((meltpair_ptr_t) ( /*_.PAIROFLIST__V54*/ meltfptr[53]))->tl =
	      (meltpair_ptr_t) ( /*_.PAIROFLIST__V55*/ meltfptr[54]);
	    ;
	    /*^touch */
	    meltgc_touch ( /*_.PAIROFLIST__V54*/ meltfptr[53]);
	    ;
	    MELT_LOCATION ("warmelt-genobj.melt:4334:/ putpairhead");
	    /*putpairhead */
	    melt_assertmsg ("putpairhead /13 checkpair",
			    melt_magic_discr ((melt_ptr_t)
					      ( /*_.PAIROFLIST__V55*/
					       meltfptr[54])) ==
			    MELTOBMAG_PAIR);
	    ((meltpair_ptr_t) ( /*_.PAIROFLIST__V55*/ meltfptr[54]))->hd =
	      (melt_ptr_t) ( /*_.MAKE_STRINGCONST__V45*/ meltfptr[31]);
	    ;
	    /*^putpairtail */
	    /*putpairtail */
	    melt_assertmsg ("putpairtail /33945af9 checkpair",
			    melt_magic_discr ((melt_ptr_t)
					      ( /*_.PAIROFLIST__V55*/
					       meltfptr[54])) ==
			    MELTOBMAG_PAIR);
	    ((meltpair_ptr_t) ( /*_.PAIROFLIST__V55*/ meltfptr[54]))->tl =
	      (meltpair_ptr_t) ( /*_.OVARIADICLENGTH__V56*/ meltfptr[55]);
	    ;
	    /*^touch */
	    meltgc_touch ( /*_.PAIROFLIST__V55*/ meltfptr[54]);
	    ;
	    MELT_LOCATION ("warmelt-genobj.melt:4325:/ putpairhead");
	    /*putpairhead */
	    melt_assertmsg ("putpairhead /14 checkpair",
			    melt_magic_discr ((melt_ptr_t)
					      ( /*_.OVARIADICLENGTH__V56*/
					       meltfptr[55])) ==
			    MELTOBMAG_PAIR);
	    ((meltpair_ptr_t) ( /*_.OVARIADICLENGTH__V56*/ meltfptr[55]))->
	      hd = (melt_ptr_t) ( /*_.OVARIADICLENGTH__V28*/ meltfptr[27]);
	    ;
	    /*^touch */
	    meltgc_touch ( /*_.OVARIADICLENGTH__V56*/ meltfptr[55]);
	    ;
	    /*^putlist */
	    /*putlist */
	    melt_assertmsg ("putlist checklist",
			    melt_magic_discr ((melt_ptr_t)
					      ( /*_.LIST___V57*/
					       meltfptr[56])) ==
			    MELTOBMAG_LIST);
	    ((meltlist_ptr_t) ( /*_.LIST___V57*/ meltfptr[56]))->first =
	      (meltpair_ptr_t) ( /*_.PAIROFLIST__V47*/ meltfptr[46]);
	    ((meltlist_ptr_t) ( /*_.LIST___V57*/ meltfptr[56]))->last =
	      (meltpair_ptr_t) ( /*_.OVARIADICLENGTH__V56*/ meltfptr[55]);
	    ;
	    /*^touch */
	    meltgc_touch ( /*_.LIST___V57*/ meltfptr[56]);
	    ;
	    /*_.OVLIST__V46*/ meltfptr[45] = /*_.LIST___V57*/ meltfptr[56];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-genobj.melt:4325:/ clear");
	      /*clear *//*_.PAIROFLIST__V47*/ meltfptr[46] = 0;
	    /*^clear */
	      /*clear *//*_.PAIROFLIST__V48*/ meltfptr[47] = 0;
	    /*^clear */
	      /*clear *//*_.PAIROFLIST__V49*/ meltfptr[48] = 0;
	    /*^clear */
	      /*clear *//*_.OVARIADICINDEX__V50*/ meltfptr[49] = 0;
	    /*^clear */
	      /*clear *//*_.PAIROFLIST__V51*/ meltfptr[50] = 0;
	    /*^clear */
	      /*clear *//*_.OVARIADICINDEX__V52*/ meltfptr[51] = 0;
	    /*^clear */
	      /*clear *//*_.PAIROFLIST__V53*/ meltfptr[52] = 0;
	    /*^clear */
	      /*clear *//*_.PAIROFLIST__V54*/ meltfptr[53] = 0;
	    /*^clear */
	      /*clear *//*_.PAIROFLIST__V55*/ meltfptr[54] = 0;
	    /*^clear */
	      /*clear *//*_.OVARIADICLENGTH__V56*/ meltfptr[55] = 0;
	    /*^clear */
	      /*clear *//*_.LIST___V57*/ meltfptr[56] = 0;
	    /*^clear */
	      /*clear *//*_.PAIROFLIST__V47*/ meltfptr[46] = 0;
	    /*^clear */
	      /*clear *//*_.PAIROFLIST__V48*/ meltfptr[47] = 0;
	    /*^clear */
	      /*clear *//*_.PAIROFLIST__V49*/ meltfptr[48] = 0;
	    /*^clear */
	      /*clear *//*_.OVARIADICINDEX__V50*/ meltfptr[49] = 0;
	    /*^clear */
	      /*clear *//*_.PAIROFLIST__V51*/ meltfptr[50] = 0;
	    /*^clear */
	      /*clear *//*_.OVARIADICINDEX__V52*/ meltfptr[51] = 0;
	    /*^clear */
	      /*clear *//*_.PAIROFLIST__V53*/ meltfptr[52] = 0;
	    /*^clear */
	      /*clear *//*_.PAIROFLIST__V54*/ meltfptr[53] = 0;
	    /*^clear */
	      /*clear *//*_.PAIROFLIST__V55*/ meltfptr[54] = 0;
	    /*^clear */
	      /*clear *//*_.OVARIADICLENGTH__V56*/ meltfptr[55] = 0;
	    /*^clear */
	      /*clear *//*_.LIST___V57*/ meltfptr[56] = 0;
	  }			/*end multiallocblock */
	  ;
	  /*citerblock FOREACH_IN_MULTIPLE */
	  {
	    /* start foreach_in_multiple meltcit1__EACHTUP */
	    long meltcit1__EACHTUP_ln =
	      melt_multiple_length ((melt_ptr_t) /*_.NVCTYPES__V17*/
				    meltfptr[16]);
	    for ( /*_#IX__L11*/ meltfnum[0] = 0;
		 ( /*_#IX__L11*/ meltfnum[0] >= 0)
		 && ( /*_#IX__L11*/ meltfnum[0] < meltcit1__EACHTUP_ln);
	/*_#IX__L11*/ meltfnum[0]++)
	      {
		/*_.CURCTYP__V58*/ meltfptr[46] =
		  melt_multiple_nth ((melt_ptr_t)
				     ( /*_.NVCTYPES__V17*/ meltfptr[16]),
				     /*_#IX__L11*/ meltfnum[0]);




#if MELT_HAVE_DEBUG
		MELT_LOCATION ("warmelt-genobj.melt:4341:/ cppif.then");
		/*^block */
		/*anyblock */
		{


		  {
		    /*^locexp */
		    /*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
		    melt_dbgcounter++;
#endif
		    ;
		  }
		  ;
		  /*^checksignal */
		  MELT_CHECK_SIGNAL ();
		  ;
      /*_#MELT_NEED_DBG__L12*/ meltfnum[11] =
		    /*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
		    ( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
		    0		/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
		    ;;
		  MELT_LOCATION ("warmelt-genobj.melt:4341:/ cond");
		  /*cond */ if ( /*_#MELT_NEED_DBG__L12*/ meltfnum[11])	/*then */
		    {
		      /*^cond.then */
		      /*^block */
		      /*anyblock */
		      {

	/*_#THE_MELTCALLCOUNT__L13*/ meltfnum[12] =
#ifdef meltcallcount
			  meltcallcount	/* the_meltcallcount */
#else
			  0L
#endif /* meltcallcount the_meltcallcount */
			  ;;
			MELT_LOCATION
			  ("warmelt-genobj.melt:4341:/ checksignal");
			MELT_CHECK_SIGNAL ();
			;
			/*^apply */
			/*apply */
			{
			  union meltparam_un argtab[7];
			  memset (&argtab, 0, sizeof (argtab));
			  /*^apply.arg */
			  argtab[0].meltbp_long =
			    /*_#THE_MELTCALLCOUNT__L13*/ meltfnum[12];
			  /*^apply.arg */
			  argtab[1].meltbp_cstring = "warmelt-genobj.melt";
			  /*^apply.arg */
			  argtab[2].meltbp_long = 4341;
			  /*^apply.arg */
			  argtab[3].meltbp_cstring =
			    "compilobj_nrep_ifvariadic curctyp";
			  /*^apply.arg */
			  argtab[4].meltbp_aptr =
			    (melt_ptr_t *) & /*_.CURCTYP__V58*/ meltfptr[46];
			  /*^apply.arg */
			  argtab[5].meltbp_cstring = " ix=";
			  /*^apply.arg */
			  argtab[6].meltbp_long = /*_#IX__L11*/ meltfnum[0];
			  /*_.MELT_DEBUG_FUN__V61*/ meltfptr[49] =
			    melt_apply ((meltclosure_ptr_t)
					(( /*!MELT_DEBUG_FUN */ meltfrout->
					  tabval[2])),
					(melt_ptr_t) (( /*nil */ NULL)),
					(MELTBPARSTR_LONG MELTBPARSTR_CSTRING
					 MELTBPARSTR_LONG MELTBPARSTR_CSTRING
					 MELTBPARSTR_PTR MELTBPARSTR_CSTRING
					 MELTBPARSTR_LONG ""), argtab, "",
					(union meltparam_un *) 0);
			}
			;
			/*_.IF___V60*/ meltfptr[48] =
			  /*_.MELT_DEBUG_FUN__V61*/ meltfptr[49];;
			/*epilog */

			MELT_LOCATION ("warmelt-genobj.melt:4341:/ clear");
		  /*clear *//*_#THE_MELTCALLCOUNT__L13*/ meltfnum[12] =
			  0;
			/*^clear */
		  /*clear *//*_.MELT_DEBUG_FUN__V61*/ meltfptr[49] = 0;
		      }
		      ;
		    }
		  else
		    {		/*^cond.else */

       /*_.IF___V60*/ meltfptr[48] = NULL;;
		    }
		  ;
		  MELT_LOCATION ("warmelt-genobj.melt:4341:/ quasiblock");


		  /*_.PROGN___V62*/ meltfptr[50] =
		    /*_.IF___V60*/ meltfptr[48];;
		  /*^compute */
		  /*_.IFCPP___V59*/ meltfptr[47] =
		    /*_.PROGN___V62*/ meltfptr[50];;
		  /*epilog */

		  MELT_LOCATION ("warmelt-genobj.melt:4341:/ clear");
		/*clear *//*_#MELT_NEED_DBG__L12*/ meltfnum[11] = 0;
		  /*^clear */
		/*clear *//*_.IF___V60*/ meltfptr[48] = 0;
		  /*^clear */
		/*clear *//*_.PROGN___V62*/ meltfptr[50] = 0;
		}

#else /*MELT_HAVE_DEBUG */
		/*^cppif.else */
		/*_.IFCPP___V59*/ meltfptr[47] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
		;

#if MELT_HAVE_DEBUG
		MELT_LOCATION ("warmelt-genobj.melt:4342:/ cppif.then");
		/*^block */
		/*anyblock */
		{

		  /*^checksignal */
		  MELT_CHECK_SIGNAL ();
		  ;
      /*_#IS_A__L14*/ meltfnum[12] =
		    melt_is_instance_of ((melt_ptr_t)
					 ( /*_.CURCTYP__V58*/ meltfptr[46]),
					 (melt_ptr_t) (( /*!CLASS_CTYPE */
							meltfrout->
							tabval[8])));;
		  MELT_LOCATION ("warmelt-genobj.melt:4342:/ cond");
		  /*cond */ if ( /*_#IS_A__L14*/ meltfnum[12])	/*then */
		    {
		      /*^cond.then */
		      /*_.IFELSE___V64*/ meltfptr[52] = ( /*nil */ NULL);;
		    }
		  else
		    {
		      MELT_LOCATION ("warmelt-genobj.melt:4342:/ cond.else");

		      /*^block */
		      /*anyblock */
		      {




			{
			  /*^locexp */
			  melt_assert_failed (("check curctyp"),
					      ("warmelt-genobj.melt")
					      ? ("warmelt-genobj.melt") :
					      __FILE__,
					      (4342) ? (4342) : __LINE__,
					      __FUNCTION__);
			  ;
			}
			;
		  /*clear *//*_.IFELSE___V64*/ meltfptr[52] = 0;
			/*epilog */
		      }
		      ;
		    }
		  ;
		  /*^compute */
		  /*_.IFCPP___V63*/ meltfptr[51] =
		    /*_.IFELSE___V64*/ meltfptr[52];;
		  /*epilog */

		  MELT_LOCATION ("warmelt-genobj.melt:4342:/ clear");
		/*clear *//*_#IS_A__L14*/ meltfnum[12] = 0;
		  /*^clear */
		/*clear *//*_.IFELSE___V64*/ meltfptr[52] = 0;
		}

#else /*MELT_HAVE_DEBUG */
		/*^cppif.else */
		/*_.IFCPP___V63*/ meltfptr[51] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
		;
		/*^compute */
    /*_.MAKE_STRINGCONST__V65*/ meltfptr[53] =
		  (meltgc_new_stringdup
		   ((meltobject_ptr_t)
		    (( /*!DISCR_VERBATIM_STRING */ meltfrout->tabval[6])),
		    (" && meltxargdescr_[")));;

		{
		  MELT_LOCATION ("warmelt-genobj.melt:4343:/ locexp");
		  meltgc_append_list ((melt_ptr_t)
				      ( /*_.OVLIST__V46*/ meltfptr[45]),
				      (melt_ptr_t) ( /*_.MAKE_STRINGCONST__V65*/ meltfptr[53]));
		}
		;

		{
		  MELT_LOCATION ("warmelt-genobj.melt:4344:/ locexp");
		  meltgc_append_list ((melt_ptr_t)
				      ( /*_.OVLIST__V46*/ meltfptr[45]),
				      (melt_ptr_t) ( /*_.OVARIADICINDEX__V27*/
						    meltfptr[23]));
		}
		;
		MELT_LOCATION ("warmelt-genobj.melt:4345:/ checksignal");
		MELT_CHECK_SIGNAL ();
		;
		/*^cond */
		/*cond */ if ( /*_#IX__L11*/ meltfnum[0])	/*then */
		  {
		    /*^cond.then */
		    /*^block */
		    /*anyblock */
		    {

      /*_.MAKE_STRINGCONST__V66*/ meltfptr[54] =
			(meltgc_new_stringdup
			 ((meltobject_ptr_t)
			  (( /*!DISCR_VERBATIM_STRING */ meltfrout->
			    tabval[6])), (" + ")));;

		      {
			MELT_LOCATION ("warmelt-genobj.melt:4347:/ locexp");
			meltgc_append_list ((melt_ptr_t)
					    ( /*_.OVLIST__V46*/ meltfptr[45]),
					    (melt_ptr_t) ( /*_.MAKE_STRINGCONST__V66*/ meltfptr[54]));
		      }
		      ;
      /*_.MAKE_INTEGERBOX__V67*/ meltfptr[55] =
			(meltgc_new_int
			 ((meltobject_ptr_t)
			  (( /*!DISCR_CONSTANT_INTEGER */ meltfrout->
			    tabval[7])), ( /*_#IX__L11*/ meltfnum[0])));;

		      {
			MELT_LOCATION ("warmelt-genobj.melt:4348:/ locexp");
			meltgc_append_list ((melt_ptr_t)
					    ( /*_.OVLIST__V46*/ meltfptr[45]),
					    (melt_ptr_t) ( /*_.MAKE_INTEGERBOX__V67*/ meltfptr[55]));
		      }
		      ;
		      MELT_LOCATION ("warmelt-genobj.melt:4346:/ quasiblock");


		      /*epilog */

		      MELT_LOCATION ("warmelt-genobj.melt:4345:/ clear");
		/*clear *//*_.MAKE_STRINGCONST__V66*/ meltfptr[54] = 0;
		      /*^clear */
		/*clear *//*_.MAKE_INTEGERBOX__V67*/ meltfptr[55] = 0;
		    }
		    ;
		  }		/*noelse */
		;
    /*_.MAKE_STRINGCONST__V68*/ meltfptr[56] =
		  (meltgc_new_stringdup
		   ((meltobject_ptr_t)
		    (( /*!DISCR_VERBATIM_STRING */ meltfrout->tabval[6])),
		    ("]== ")));;

		{
		  MELT_LOCATION ("warmelt-genobj.melt:4349:/ locexp");
		  meltgc_append_list ((melt_ptr_t)
				      ( /*_.OVLIST__V46*/ meltfptr[45]),
				      (melt_ptr_t) ( /*_.MAKE_STRINGCONST__V68*/ meltfptr[56]));
		}
		;
		MELT_LOCATION ("warmelt-genobj.melt:4352:/ cond");
		/*cond */ if (
			       /*ifisa */
			       melt_is_instance_of ((melt_ptr_t)
						    ( /*_.CURCTYP__V58*/
						     meltfptr[46]),
						    (melt_ptr_t) (( /*!CLASS_CTYPE */ meltfrout->tabval[8])))
		  )		/*then */
		  {
		    /*^cond.then */
		    /*^getslot */
		    {
		      melt_ptr_t slot = NULL, obj = NULL;
		      obj =
			(melt_ptr_t) ( /*_.CURCTYP__V58*/ meltfptr[46])
			/*=obj*/ ;
		      melt_object_get_field (slot, obj, 4, "CTYPE_PARCHAR");
      /*_.CTYPE_PARCHAR__V69*/ meltfptr[49] = slot;
		    };
		    ;
		  }
		else
		  {		/*^cond.else */

     /*_.CTYPE_PARCHAR__V69*/ meltfptr[49] = NULL;;
		  }
		;
		/*^compute */
    /*_.MAKE_STRING__V70*/ meltfptr[48] =
		  (meltgc_new_stringdup
		   ((meltobject_ptr_t)
		    (( /*!DISCR_VERBATIM_STRING */ meltfrout->tabval[6])),
		    melt_string_str ((melt_ptr_t)
				     ( /*_.CTYPE_PARCHAR__V69*/
				      meltfptr[49]))));;

		{
		  MELT_LOCATION ("warmelt-genobj.melt:4350:/ locexp");
		  meltgc_append_list ((melt_ptr_t)
				      ( /*_.OVLIST__V46*/ meltfptr[45]),
				      (melt_ptr_t) ( /*_.MAKE_STRING__V70*/
						    meltfptr[48]));
		}
		;
		if ( /*_#IX__L11*/ meltfnum[0] < 0)
		  break;
	      }			/* end  foreach_in_multiple meltcit1__EACHTUP */

	    /*citerepilog */

	    MELT_LOCATION ("warmelt-genobj.melt:4338:/ clear");
	      /*clear *//*_.CURCTYP__V58*/ meltfptr[46] = 0;
	    /*^clear */
	      /*clear *//*_#IX__L11*/ meltfnum[0] = 0;
	    /*^clear */
	      /*clear *//*_.IFCPP___V59*/ meltfptr[47] = 0;
	    /*^clear */
	      /*clear *//*_.IFCPP___V63*/ meltfptr[51] = 0;
	    /*^clear */
	      /*clear *//*_.MAKE_STRINGCONST__V65*/ meltfptr[53] = 0;
	    /*^clear */
	      /*clear *//*_.MAKE_STRINGCONST__V68*/ meltfptr[56] = 0;
	    /*^clear */
	      /*clear *//*_.CTYPE_PARCHAR__V69*/ meltfptr[49] = 0;
	    /*^clear */
	      /*clear *//*_.MAKE_STRING__V70*/ meltfptr[48] = 0;
	  }			/*endciterblock FOREACH_IN_MULTIPLE */
	  ;

#if MELT_HAVE_DEBUG
	  MELT_LOCATION ("warmelt-genobj.melt:4354:/ cppif.then");
	  /*^block */
	  /*anyblock */
	  {


	    {
	      /*^locexp */
	      /*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	      melt_dbgcounter++;
#endif
	      ;
	    }
	    ;
	    /*^checksignal */
	    MELT_CHECK_SIGNAL ();
	    ;
     /*_#MELT_NEED_DBG__L15*/ meltfnum[11] =
	      /*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	      ( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	      0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	      ;;
	    MELT_LOCATION ("warmelt-genobj.melt:4354:/ cond");
	    /*cond */ if ( /*_#MELT_NEED_DBG__L15*/ meltfnum[11])	/*then */
	      {
		/*^cond.then */
		/*^block */
		/*anyblock */
		{

       /*_#THE_MELTCALLCOUNT__L16*/ meltfnum[12] =
#ifdef meltcallcount
		    meltcallcount	/* the_meltcallcount */
#else
		    0L
#endif /* meltcallcount the_meltcallcount */
		    ;;
		  MELT_LOCATION ("warmelt-genobj.melt:4354:/ checksignal");
		  MELT_CHECK_SIGNAL ();
		  ;
		  /*^apply */
		  /*apply */
		  {
		    union meltparam_un argtab[5];
		    memset (&argtab, 0, sizeof (argtab));
		    /*^apply.arg */
		    argtab[0].meltbp_long =
		      /*_#THE_MELTCALLCOUNT__L16*/ meltfnum[12];
		    /*^apply.arg */
		    argtab[1].meltbp_cstring = "warmelt-genobj.melt";
		    /*^apply.arg */
		    argtab[2].meltbp_long = 4354;
		    /*^apply.arg */
		    argtab[3].meltbp_cstring =
		      "compilobj_nrep_ifvariadic ovlist=";
		    /*^apply.arg */
		    argtab[4].meltbp_aptr =
		      (melt_ptr_t *) & /*_.OVLIST__V46*/ meltfptr[45];
		    /*_.MELT_DEBUG_FUN__V73*/ meltfptr[54] =
		      melt_apply ((meltclosure_ptr_t)
				  (( /*!MELT_DEBUG_FUN */ meltfrout->
				    tabval[2])),
				  (melt_ptr_t) (( /*nil */ NULL)),
				  (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_PTR ""), argtab, "",
				  (union meltparam_un *) 0);
		  }
		  ;
		  /*_.IF___V72*/ meltfptr[52] =
		    /*_.MELT_DEBUG_FUN__V73*/ meltfptr[54];;
		  /*epilog */

		  MELT_LOCATION ("warmelt-genobj.melt:4354:/ clear");
		 /*clear *//*_#THE_MELTCALLCOUNT__L16*/ meltfnum[12] = 0;
		  /*^clear */
		 /*clear *//*_.MELT_DEBUG_FUN__V73*/ meltfptr[54] = 0;
		}
		;
	      }
	    else
	      {			/*^cond.else */

      /*_.IF___V72*/ meltfptr[52] = NULL;;
	      }
	    ;
	    MELT_LOCATION ("warmelt-genobj.melt:4354:/ quasiblock");


	    /*_.PROGN___V74*/ meltfptr[55] = /*_.IF___V72*/ meltfptr[52];;
	    /*^compute */
	    /*_.IFCPP___V71*/ meltfptr[50] = /*_.PROGN___V74*/ meltfptr[55];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-genobj.melt:4354:/ clear");
	       /*clear *//*_#MELT_NEED_DBG__L15*/ meltfnum[11] = 0;
	    /*^clear */
	       /*clear *//*_.IF___V72*/ meltfptr[52] = 0;
	    /*^clear */
	       /*clear *//*_.PROGN___V74*/ meltfptr[55] = 0;
	  }

#else /*MELT_HAVE_DEBUG */
	  /*^cppif.else */
	  /*_.IFCPP___V71*/ meltfptr[50] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
	  ;
	  /*^compute */
	  /*_.LET___V38*/ meltfptr[32] = /*_.OVLIST__V46*/ meltfptr[45];;

	  MELT_LOCATION ("warmelt-genobj.melt:4324:/ clear");
	     /*clear *//*_.MAKE_STRINGCONST__V39*/ meltfptr[33] = 0;
	  /*^clear */
	     /*clear *//*_.MAKE_INTEGERBOX__V40*/ meltfptr[34] = 0;
	  /*^clear */
	     /*clear *//*_.MAKE_STRINGCONST__V41*/ meltfptr[35] = 0;
	  /*^clear */
	     /*clear *//*_.MAKE_STRINGCONST__V42*/ meltfptr[36] = 0;
	  /*^clear */
	     /*clear *//*_.MAKE_STRINGCONST__V43*/ meltfptr[29] = 0;
	  /*^clear */
	     /*clear *//*_.MAKE_INTEGERBOX__V44*/ meltfptr[30] = 0;
	  /*^clear */
	     /*clear *//*_.MAKE_STRINGCONST__V45*/ meltfptr[31] = 0;
	  /*^clear */
	     /*clear *//*_.OVLIST__V46*/ meltfptr[45] = 0;
	  /*^clear */
	     /*clear *//*_.IFCPP___V71*/ meltfptr[50] = 0;
	  /*_.OTESTCHKLIST__V29*/ meltfptr[28] =
	    /*_.LET___V38*/ meltfptr[32];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-genobj.melt:4317:/ clear");
	     /*clear *//*_.LET___V38*/ meltfptr[32] = 0;
	}
	;
      }
    ;
    MELT_LOCATION ("warmelt-genobj.melt:4357:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    MELT_LOCATION ("warmelt-genobj.melt:4358:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & ( /*!DISCR_MULTIPLE */ meltfrout->tabval[11]);
      /*_.LIST_TO_MULTIPLE__V75*/ meltfptr[54] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!LIST_TO_MULTIPLE */ meltfrout->tabval[10])),
		    (melt_ptr_t) ( /*_.OTESTCHKLIST__V29*/ meltfptr[28]),
		    (MELTBPARSTR_PTR ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-genobj.melt:4357:/ quasiblock");


    /*^rawallocobj */
    /*rawallocobj */
    {
      melt_ptr_t newobj = 0;
      melt_raw_object_create (newobj,
			      (melt_ptr_t) (( /*!CLASS_OBJEXPV */ meltfrout->
					     tabval[9])), (2),
			      "CLASS_OBJEXPV");
  /*_.INST__V77*/ meltfptr[55] =
	newobj;
    };
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OBX_CONT",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V77*/ meltfptr[55])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V77*/ meltfptr[55]), (1),
			  ( /*_.LIST_TO_MULTIPLE__V75*/ meltfptr[54]),
			  "OBX_CONT");
    ;
    /*^touchobj */

    melt_dbgtrace_written_object ( /*_.INST__V77*/ meltfptr[55],
				  "newly made instance");
    ;
    /*_.OTEST__V76*/ meltfptr[52] = /*_.INST__V77*/ meltfptr[55];;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:4361:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L17*/ meltfnum[12] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-genobj.melt:4361:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L17*/ meltfnum[12])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L18*/ meltfnum[11] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-genobj.melt:4361:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long =
		/*_#THE_MELTCALLCOUNT__L18*/ meltfnum[11];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-genobj.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 4361;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring =
		"compilobj_nrep_ifvariadic before compiling nthen=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.NTHEN__V14*/ meltfptr[13];
	      /*_.MELT_DEBUG_FUN__V80*/ meltfptr[35] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[2])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V79*/ meltfptr[34] =
	      /*_.MELT_DEBUG_FUN__V80*/ meltfptr[35];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-genobj.melt:4361:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L18*/ meltfnum[11] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V80*/ meltfptr[35] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V79*/ meltfptr[34] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-genobj.melt:4361:/ quasiblock");


      /*_.PROGN___V81*/ meltfptr[36] = /*_.IF___V79*/ meltfptr[34];;
      /*^compute */
      /*_.IFCPP___V78*/ meltfptr[33] = /*_.PROGN___V81*/ meltfptr[36];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:4361:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L17*/ meltfnum[12] = 0;
      /*^clear */
	     /*clear *//*_.IF___V79*/ meltfptr[34] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V81*/ meltfptr[36] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V78*/ meltfptr[33] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-genobj.melt:4362:/ quasiblock");


    /*^checksignal */
    MELT_CHECK_SIGNAL ();
    ;
    /*^msend */
    /*msend */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^ojbmsend.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.GCX__V3*/ meltfptr[2];
      /*_.OBJTHEN__V83*/ meltfptr[30] =
	meltgc_send ((melt_ptr_t) ( /*_.NTHEN__V14*/ meltfptr[13]),
		     (melt_ptr_t) (( /*!COMPILE_OBJ */ meltfrout->
				    tabval[12])), (MELTBPARSTR_PTR ""),
		     argtab, "", (union meltparam_un *) 0);
    }
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:4364:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L19*/ meltfnum[11] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-genobj.melt:4364:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L19*/ meltfnum[11])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L20*/ meltfnum[12] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-genobj.melt:4364:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[7];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long =
		/*_#THE_MELTCALLCOUNT__L20*/ meltfnum[12];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-genobj.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 4364;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring =
		"compilobj_nrep_ifvariadic after compiling nthen=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.NTHEN__V14*/ meltfptr[13];
	      /*^apply.arg */
	      argtab[5].meltbp_cstring = " objthen=";
	      /*^apply.arg */
	      argtab[6].meltbp_aptr =
		(melt_ptr_t *) & /*_.OBJTHEN__V83*/ meltfptr[30];
	      /*_.MELT_DEBUG_FUN__V86*/ meltfptr[50] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[2])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V85*/ meltfptr[45] =
	      /*_.MELT_DEBUG_FUN__V86*/ meltfptr[50];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-genobj.melt:4364:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L20*/ meltfnum[12] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V86*/ meltfptr[50] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V85*/ meltfptr[45] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-genobj.melt:4364:/ quasiblock");


      /*_.PROGN___V87*/ meltfptr[32] = /*_.IF___V85*/ meltfptr[45];;
      /*^compute */
      /*_.IFCPP___V84*/ meltfptr[31] = /*_.PROGN___V87*/ meltfptr[32];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:4364:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L19*/ meltfnum[11] = 0;
      /*^clear */
	     /*clear *//*_.IF___V85*/ meltfptr[45] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V87*/ meltfptr[32] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V84*/ meltfptr[31] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    /*^compute */
    /*_.LET___V82*/ meltfptr[29] = /*_.OBJTHEN__V83*/ meltfptr[30];;

    MELT_LOCATION ("warmelt-genobj.melt:4362:/ clear");
	   /*clear *//*_.OBJTHEN__V83*/ meltfptr[30] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V84*/ meltfptr[31] = 0;
    MELT_LOCATION ("warmelt-genobj.melt:4360:/ quasiblock");


    /*_.OTHEN__V88*/ meltfptr[35] = /*_.LET___V82*/ meltfptr[29];;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:4369:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L21*/ meltfnum[12] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-genobj.melt:4369:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L21*/ meltfnum[12])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L22*/ meltfnum[11] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-genobj.melt:4369:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long =
		/*_#THE_MELTCALLCOUNT__L22*/ meltfnum[11];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-genobj.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 4369;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring =
		"compilobj_nrep_ifvariadic before compiling nelse=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.NELSE__V15*/ meltfptr[14];
	      /*_.MELT_DEBUG_FUN__V91*/ meltfptr[50] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[2])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V90*/ meltfptr[36] =
	      /*_.MELT_DEBUG_FUN__V91*/ meltfptr[50];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-genobj.melt:4369:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L22*/ meltfnum[11] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V91*/ meltfptr[50] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V90*/ meltfptr[36] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-genobj.melt:4369:/ quasiblock");


      /*_.PROGN___V92*/ meltfptr[45] = /*_.IF___V90*/ meltfptr[36];;
      /*^compute */
      /*_.IFCPP___V89*/ meltfptr[34] = /*_.PROGN___V92*/ meltfptr[45];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:4369:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L21*/ meltfnum[12] = 0;
      /*^clear */
	     /*clear *//*_.IF___V90*/ meltfptr[36] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V92*/ meltfptr[45] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V89*/ meltfptr[34] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-genobj.melt:4370:/ quasiblock");


    /*^checksignal */
    MELT_CHECK_SIGNAL ();
    ;
    /*^msend */
    /*msend */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^ojbmsend.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.GCX__V3*/ meltfptr[2];
      /*_.OBJELSE__V94*/ meltfptr[30] =
	meltgc_send ((melt_ptr_t) ( /*_.NELSE__V15*/ meltfptr[14]),
		     (melt_ptr_t) (( /*!COMPILE_OBJ */ meltfrout->
				    tabval[12])), (MELTBPARSTR_PTR ""),
		     argtab, "", (union meltparam_un *) 0);
    }
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:4372:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L23*/ meltfnum[11] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-genobj.melt:4372:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L23*/ meltfnum[11])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L24*/ meltfnum[12] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-genobj.melt:4372:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[7];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long =
		/*_#THE_MELTCALLCOUNT__L24*/ meltfnum[12];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-genobj.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 4372;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring =
		"compilobj_nrep_ifvariadic after compiling nelse=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.NELSE__V15*/ meltfptr[14];
	      /*^apply.arg */
	      argtab[5].meltbp_cstring = " objelse=";
	      /*^apply.arg */
	      argtab[6].meltbp_aptr =
		(melt_ptr_t *) & /*_.OBJELSE__V94*/ meltfptr[30];
	      /*_.MELT_DEBUG_FUN__V97*/ meltfptr[36] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[2])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V96*/ meltfptr[50] =
	      /*_.MELT_DEBUG_FUN__V97*/ meltfptr[36];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-genobj.melt:4372:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L24*/ meltfnum[12] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V97*/ meltfptr[36] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V96*/ meltfptr[50] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-genobj.melt:4372:/ quasiblock");


      /*_.PROGN___V98*/ meltfptr[45] = /*_.IF___V96*/ meltfptr[50];;
      /*^compute */
      /*_.IFCPP___V95*/ meltfptr[31] = /*_.PROGN___V98*/ meltfptr[45];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:4372:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L23*/ meltfnum[11] = 0;
      /*^clear */
	     /*clear *//*_.IF___V96*/ meltfptr[50] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V98*/ meltfptr[45] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V95*/ meltfptr[31] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    /*^compute */
    /*_.LET___V93*/ meltfptr[32] = /*_.OBJELSE__V94*/ meltfptr[30];;

    MELT_LOCATION ("warmelt-genobj.melt:4370:/ clear");
	   /*clear *//*_.OBJELSE__V94*/ meltfptr[30] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V95*/ meltfptr[31] = 0;
    MELT_LOCATION ("warmelt-genobj.melt:4368:/ quasiblock");


    /*_.OELSE__V99*/ meltfptr[36] = /*_.LET___V93*/ meltfptr[32];;
    MELT_LOCATION ("warmelt-genobj.melt:4375:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*^rawallocobj */
    /*rawallocobj */
    {
      melt_ptr_t newobj = 0;
      melt_raw_object_create (newobj,
			      (melt_ptr_t) (( /*!CLASS_OBJCOND */ meltfrout->
					     tabval[13])), (4),
			      "CLASS_OBJCOND");
  /*_.INST__V101*/ meltfptr[45] =
	newobj;
    };
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OBI_LOC",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V101*/ meltfptr[45])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V101*/ meltfptr[45]), (0),
			  ( /*_.LOC__V13*/ meltfptr[9]), "OBI_LOC");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OBCOND_TEST",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V101*/ meltfptr[45])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V101*/ meltfptr[45]), (1),
			  ( /*_.OTEST__V76*/ meltfptr[52]), "OBCOND_TEST");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OBCOND_THEN",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V101*/ meltfptr[45])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V101*/ meltfptr[45]), (2),
			  ( /*_.OTHEN__V88*/ meltfptr[35]), "OBCOND_THEN");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OBCOND_ELSE",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V101*/ meltfptr[45])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V101*/ meltfptr[45]), (3),
			  ( /*_.OELSE__V99*/ meltfptr[36]), "OBCOND_ELSE");
    ;
    /*^touchobj */

    melt_dbgtrace_written_object ( /*_.INST__V101*/ meltfptr[45],
				  "newly made instance");
    ;
    /*_.OCOND__V100*/ meltfptr[50] = /*_.INST__V101*/ meltfptr[45];;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:4381:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L25*/ meltfnum[12] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-genobj.melt:4381:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L25*/ meltfnum[12])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L26*/ meltfnum[11] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-genobj.melt:4381:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long =
		/*_#THE_MELTCALLCOUNT__L26*/ meltfnum[11];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-genobj.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 4381;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "compilobj_nrep_ifvariadic ocond=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.OCOND__V100*/ meltfptr[50];
	      /*_.MELT_DEBUG_FUN__V104*/ meltfptr[103] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[2])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V103*/ meltfptr[31] =
	      /*_.MELT_DEBUG_FUN__V104*/ meltfptr[103];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-genobj.melt:4381:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L26*/ meltfnum[11] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V104*/ meltfptr[103] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V103*/ meltfptr[31] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-genobj.melt:4381:/ quasiblock");


      /*_.PROGN___V105*/ meltfptr[103] = /*_.IF___V103*/ meltfptr[31];;
      /*^compute */
      /*_.IFCPP___V102*/ meltfptr[30] = /*_.PROGN___V105*/ meltfptr[103];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:4381:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L25*/ meltfnum[12] = 0;
      /*^clear */
	     /*clear *//*_.IF___V103*/ meltfptr[31] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V105*/ meltfptr[103] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V102*/ meltfptr[30] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-genobj.melt:4382:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.OCOND__V100*/ meltfptr[50];;

    {
      MELT_LOCATION ("warmelt-genobj.melt:4382:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*_.LET___V26*/ meltfptr[22] = /*_.RETURN___V106*/ meltfptr[31];;

    MELT_LOCATION ("warmelt-genobj.melt:4313:/ clear");
	   /*clear *//*_.OVARIADICINDEX__V27*/ meltfptr[23] = 0;
    /*^clear */
	   /*clear *//*_.OVARIADICLENGTH__V28*/ meltfptr[27] = 0;
    /*^clear */
	   /*clear *//*_#I__L10*/ meltfnum[8] = 0;
    /*^clear */
	   /*clear *//*_.OTESTCHKLIST__V29*/ meltfptr[28] = 0;
    /*^clear */
	   /*clear *//*_.LIST_TO_MULTIPLE__V75*/ meltfptr[54] = 0;
    /*^clear */
	   /*clear *//*_.OTEST__V76*/ meltfptr[52] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V78*/ meltfptr[33] = 0;
    /*^clear */
	   /*clear *//*_.LET___V82*/ meltfptr[29] = 0;
    /*^clear */
	   /*clear *//*_.OTHEN__V88*/ meltfptr[35] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V89*/ meltfptr[34] = 0;
    /*^clear */
	   /*clear *//*_.LET___V93*/ meltfptr[32] = 0;
    /*^clear */
	   /*clear *//*_.OELSE__V99*/ meltfptr[36] = 0;
    /*^clear */
	   /*clear *//*_.OCOND__V100*/ meltfptr[50] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V102*/ meltfptr[30] = 0;
    /*^clear */
	   /*clear *//*_.RETURN___V106*/ meltfptr[31] = 0;
    /*_.LET___V12*/ meltfptr[8] = /*_.LET___V26*/ meltfptr[22];;

    MELT_LOCATION ("warmelt-genobj.melt:4303:/ clear");
	   /*clear *//*_.LOC__V13*/ meltfptr[9] = 0;
    /*^clear */
	   /*clear *//*_.NTHEN__V14*/ meltfptr[13] = 0;
    /*^clear */
	   /*clear *//*_.NELSE__V15*/ meltfptr[14] = 0;
    /*^clear */
	   /*clear *//*_.NVARIADIC__V16*/ meltfptr[15] = 0;
    /*^clear */
	   /*clear *//*_.NVCTYPES__V17*/ meltfptr[16] = 0;
    /*^clear */
	   /*clear *//*_#NBVARARG__L5*/ meltfnum[3] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V18*/ meltfptr[17] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V20*/ meltfptr[18] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V22*/ meltfptr[20] = 0;
    /*^clear */
	   /*clear *//*_.LET___V26*/ meltfptr[22] = 0;
    MELT_LOCATION ("warmelt-genobj.melt:4299:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.LET___V12*/ meltfptr[8];;

    {
      MELT_LOCATION ("warmelt-genobj.melt:4299:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V4*/ meltfptr[3] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V6*/ meltfptr[4] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V8*/ meltfptr[6] = 0;
    /*^clear */
	   /*clear *//*_.LET___V12*/ meltfptr[8] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("COMPILOBJ_NREP_IFVARIADIC", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_124_warmelt_genobj_COMPILOBJ_NREP_IFVARIADIC_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_124_warmelt_genobj_COMPILOBJ_NREP_IFVARIADIC */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_125_warmelt_genobj_GETCTYPE_IFVARIADIC (meltclosure_ptr_t meltclosp_,
						 melt_ptr_t meltfirstargp_,
						 const melt_argdescr_cell_t
						 meltxargdescr_[],
						 union meltparam_un *
						 meltxargtab_,
						 const melt_argdescr_cell_t
						 meltxresdescr_[],
						 union meltparam_un *
						 meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_125_warmelt_genobj_GETCTYPE_IFVARIADIC_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_125_warmelt_genobj_GETCTYPE_IFVARIADIC_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 5
    melt_ptr_t mcfr_varptr[5];
#define MELTFRAM_NBVARNUM 1
    long mcfr_varnum[1];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_125_warmelt_genobj_GETCTYPE_IFVARIADIC is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_125_warmelt_genobj_GETCTYPE_IFVARIADIC_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 5; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_125_warmelt_genobj_GETCTYPE_IFVARIADIC nbval 5*/
  meltfram__.mcfr_nbvar = 5 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("GETCTYPE_IFVARIADIC", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-genobj.melt:4385:/ getarg");
 /*_.RIF__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.ENV__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.ENV__V3*/ meltfptr[2])) != NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:4386:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L1*/ meltfnum[0] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.RIF__V2*/ meltfptr[1]),
			     (melt_ptr_t) (( /*!CLASS_NREP_IFVARIADIC */
					    meltfrout->tabval[0])));;
      MELT_LOCATION ("warmelt-genobj.melt:4386:/ cond");
      /*cond */ if ( /*_#IS_A__L1*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V5*/ meltfptr[4] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-genobj.melt:4386:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check ifvariadic"),
				  ("warmelt-genobj.melt")
				  ? ("warmelt-genobj.melt") : __FILE__,
				  (4386) ? (4386) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V5*/ meltfptr[4] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V4*/ meltfptr[3] = /*_.IFELSE___V5*/ meltfptr[4];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:4386:/ clear");
	     /*clear *//*_#IS_A__L1*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V5*/ meltfptr[4] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V4*/ meltfptr[3] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-genobj.melt:4385:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = ( /*!CTYPE_VOID */ meltfrout->tabval[1]);;

    {
      MELT_LOCATION ("warmelt-genobj.melt:4385:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V4*/ meltfptr[3] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("GETCTYPE_IFVARIADIC", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_125_warmelt_genobj_GETCTYPE_IFVARIADIC_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_125_warmelt_genobj_GETCTYPE_IFVARIADIC */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_126_warmelt_genobj_COMPILOBJ_NREP_VARIADIC_ARGUMENT
  (meltclosure_ptr_t meltclosp_, melt_ptr_t meltfirstargp_,
   const melt_argdescr_cell_t meltxargdescr_[],
   union meltparam_un * meltxargtab_,
   const melt_argdescr_cell_t meltxresdescr_[],
   union meltparam_un * meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_126_warmelt_genobj_COMPILOBJ_NREP_VARIADIC_ARGUMENT_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_126_warmelt_genobj_COMPILOBJ_NREP_VARIADIC_ARGUMENT_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 45
    melt_ptr_t mcfr_varptr[45];
#define MELTFRAM_NBVARNUM 9
    long mcfr_varnum[9];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_126_warmelt_genobj_COMPILOBJ_NREP_VARIADIC_ARGUMENT is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct
	 frame_meltrout_126_warmelt_genobj_COMPILOBJ_NREP_VARIADIC_ARGUMENT_st
	 *) meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 45; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_126_warmelt_genobj_COMPILOBJ_NREP_VARIADIC_ARGUMENT nbval 45*/
  meltfram__.mcfr_nbvar = 45 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("COMPILOBJ_NREP_VARIADIC_ARGUMENT", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-genobj.melt:4393:/ getarg");
 /*_.NVARG__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.GCX__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.GCX__V3*/ meltfptr[2])) != NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:4394:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L1*/ meltfnum[0] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-genobj.melt:4394:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L1*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-genobj.melt:4394:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-genobj.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 4394;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring =
		"compilobj_nrep_variadic_argument nvarg=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.NVARG__V2*/ meltfptr[1];
	      /*_.MELT_DEBUG_FUN__V6*/ meltfptr[5] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V5*/ meltfptr[4] = /*_.MELT_DEBUG_FUN__V6*/ meltfptr[5];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-genobj.melt:4394:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V6*/ meltfptr[5] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V5*/ meltfptr[4] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-genobj.melt:4394:/ quasiblock");


      /*_.PROGN___V7*/ meltfptr[5] = /*_.IF___V5*/ meltfptr[4];;
      /*^compute */
      /*_.IFCPP___V4*/ meltfptr[3] = /*_.PROGN___V7*/ meltfptr[5];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:4394:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L1*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IF___V5*/ meltfptr[4] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V7*/ meltfptr[5] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V4*/ meltfptr[3] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:4395:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L3*/ meltfnum[1] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.NVARG__V2*/ meltfptr[1]),
			     (melt_ptr_t) (( /*!CLASS_NREP_VARIADIC_ARGUMENT */ meltfrout->tabval[1])));;
      MELT_LOCATION ("warmelt-genobj.melt:4395:/ cond");
      /*cond */ if ( /*_#IS_A__L3*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V9*/ meltfptr[5] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-genobj.melt:4395:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check nvarg"),
				  ("warmelt-genobj.melt")
				  ? ("warmelt-genobj.melt") : __FILE__,
				  (4395) ? (4395) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V9*/ meltfptr[5] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V8*/ meltfptr[4] = /*_.IFELSE___V9*/ meltfptr[5];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:4395:/ clear");
	     /*clear *//*_#IS_A__L3*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V9*/ meltfptr[5] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V8*/ meltfptr[4] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:4396:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L4*/ meltfnum[0] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.GCX__V3*/ meltfptr[2]),
			     (melt_ptr_t) (( /*!CLASS_C_GENERATION_CONTEXT */
					    meltfrout->tabval[2])));;
      MELT_LOCATION ("warmelt-genobj.melt:4396:/ cond");
      /*cond */ if ( /*_#IS_A__L4*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V11*/ meltfptr[10] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-genobj.melt:4396:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check gcx"),
				  ("warmelt-genobj.melt")
				  ? ("warmelt-genobj.melt") : __FILE__,
				  (4396) ? (4396) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V11*/ meltfptr[10] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V10*/ meltfptr[5] = /*_.IFELSE___V11*/ meltfptr[10];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:4396:/ clear");
	     /*clear *//*_#IS_A__L4*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V11*/ meltfptr[10] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V10*/ meltfptr[5] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-genobj.melt:4397:/ quasiblock");


    /*^getslot */
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.NVARG__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 0, "NREP_LOC");
  /*_.NLOC__V13*/ meltfptr[12] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-genobj.melt:4398:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.NVARG__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 1, "NVARG_VARIADIC");
  /*_.NVARIADIC__V14*/ meltfptr[13] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-genobj.melt:4399:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.NVARG__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 2, "NVARG_CTYP");
  /*_.NCTYP__V15*/ meltfptr[14] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-genobj.melt:4400:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.NVARG__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 3, "NVARG_OFFSET");
  /*_.NOFFSET__V16*/ meltfptr[15] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-genobj.melt:4401:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      /*_.OVARIADICINDEX__V17*/ meltfptr[16] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!VARIADIC_INDEX_IDSTR */ meltfrout->tabval[3])),
		    (melt_ptr_t) ( /*_.NVARIADIC__V14*/ meltfptr[13]), (""),
		    (union meltparam_un *) 0, "", (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-genobj.melt:4402:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      /*_.OVARIADICLENGTH__V18*/ meltfptr[17] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!VARIADIC_LENGTH_IDSTR */ meltfrout->tabval[4])),
		    (melt_ptr_t) ( /*_.NVARIADIC__V14*/ meltfptr[13]), (""),
		    (union meltparam_un *) 0, "", (union meltparam_un *) 0);
    }
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:4404:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L5*/ meltfnum[1] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.NCTYP__V15*/ meltfptr[14]),
			     (melt_ptr_t) (( /*!CLASS_CTYPE */ meltfrout->
					    tabval[5])));;
      MELT_LOCATION ("warmelt-genobj.melt:4404:/ cond");
      /*cond */ if ( /*_#IS_A__L5*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V20*/ meltfptr[19] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-genobj.melt:4404:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check nctyp"),
				  ("warmelt-genobj.melt")
				  ? ("warmelt-genobj.melt") : __FILE__,
				  (4404) ? (4404) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V20*/ meltfptr[19] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V19*/ meltfptr[18] = /*_.IFELSE___V20*/ meltfptr[19];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:4404:/ clear");
	     /*clear *//*_#IS_A__L5*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V20*/ meltfptr[19] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V19*/ meltfptr[18] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:4405:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_INTEGERBOX__L6*/ meltfnum[0] =
	(melt_magic_discr ((melt_ptr_t) ( /*_.NOFFSET__V16*/ meltfptr[15])) ==
	 MELTOBMAG_INT);;
      MELT_LOCATION ("warmelt-genobj.melt:4405:/ cond");
      /*cond */ if ( /*_#IS_INTEGERBOX__L6*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V22*/ meltfptr[21] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-genobj.melt:4405:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check noffset"),
				  ("warmelt-genobj.melt")
				  ? ("warmelt-genobj.melt") : __FILE__,
				  (4405) ? (4405) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V22*/ meltfptr[21] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V21*/ meltfptr[19] = /*_.IFELSE___V22*/ meltfptr[21];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:4405:/ clear");
	     /*clear *//*_#IS_INTEGERBOX__L6*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V22*/ meltfptr[21] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V21*/ meltfptr[19] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-genobj.melt:4406:/ quasiblock");


    MELT_LOCATION ("warmelt-genobj.melt:4408:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#__L7*/ meltfnum[1] =
      (( /*_.NCTYP__V15*/ meltfptr[14]) ==
       (( /*!CTYPE_VALUE */ meltfrout->tabval[6])));;
    MELT_LOCATION ("warmelt-genobj.melt:4408:/ cond");
    /*cond */ if ( /*_#__L7*/ meltfnum[1])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

   /*_.MAKE_STRINGCONST__V25*/ meltfptr[24] =
	    (meltgc_new_stringdup
	     ((meltobject_ptr_t)
	      (( /*!DISCR_VERBATIM_STRING */ meltfrout->tabval[7])),
	      ("/*variadic argument value*/ ((meltxargtab_[")));;
	  /*^compute */
   /*_.MAKE_STRINGCONST__V26*/ meltfptr[25] =
	    (meltgc_new_stringdup
	     ((meltobject_ptr_t)
	      (( /*!DISCR_VERBATIM_STRING */ meltfrout->tabval[7])),
	      (" + ")));;
	  /*^compute */
   /*_.MAKE_STRINGCONST__V27*/ meltfptr[26] =
	    (meltgc_new_stringdup
	     ((meltobject_ptr_t)
	      (( /*!DISCR_VERBATIM_STRING */ meltfrout->tabval[7])),
	      ("].meltbp_aptr) ? (*(meltxargtab_[")));;
	  /*^compute */
   /*_.MAKE_STRINGCONST__V28*/ meltfptr[27] =
	    (meltgc_new_stringdup
	     ((meltobject_ptr_t)
	      (( /*!DISCR_VERBATIM_STRING */ meltfrout->tabval[7])),
	      (" + ")));;
	  /*^compute */
   /*_.MAKE_STRINGCONST__V29*/ meltfptr[28] =
	    (meltgc_new_stringdup
	     ((meltobject_ptr_t)
	      (( /*!DISCR_VERBATIM_STRING */ meltfrout->tabval[7])),
	      ("].meltbp_aptr)) : NULL)")));;
	  MELT_LOCATION ("warmelt-genobj.melt:4409:/ blockmultialloc");
	  /*multiallocblock */
	  {
	    struct meltletrec_1_st
	    {
	      struct MELT_MULTIPLE_STRUCT (9) rtup_0__TUPLREC__x7;
	      long meltletrec_1_endgap;
	    } *meltletrec_1_ptr = 0;
	    meltletrec_1_ptr =
	      (struct meltletrec_1_st *)
	      meltgc_allocate (sizeof (struct meltletrec_1_st), 0);
	    /*^blockmultialloc.initfill */
	    /*inimult rtup_0__TUPLREC__x7 */
 /*_.TUPLREC___V31*/ meltfptr[30] =
	      (melt_ptr_t) & meltletrec_1_ptr->rtup_0__TUPLREC__x7;
	    meltletrec_1_ptr->rtup_0__TUPLREC__x7.discr =
	      (meltobject_ptr_t) (((melt_ptr_t)
				   (MELT_PREDEF (DISCR_MULTIPLE))));
	    meltletrec_1_ptr->rtup_0__TUPLREC__x7.nbval = 9;


	    /*^putuple */
	    /*putupl#20 */
	    melt_assertmsg ("putupl [:4409] #20 checktup",
			    melt_magic_discr ((melt_ptr_t)
					      ( /*_.TUPLREC___V31*/
					       meltfptr[30])) ==
			    MELTOBMAG_MULTIPLE);
	    melt_assertmsg ("putupl [:4409] #20 checkoff",
			    (0 >= 0
			     && 0 <
			     melt_multiple_length ((melt_ptr_t)
						   ( /*_.TUPLREC___V31*/
						    meltfptr[30]))));
	    ((meltmultiple_ptr_t) ( /*_.TUPLREC___V31*/ meltfptr[30]))->
	      tabval[0] =
	      (melt_ptr_t) ( /*_.MAKE_STRINGCONST__V25*/ meltfptr[24]);
	    ;
	    /*^putuple */
	    /*putupl#21 */
	    melt_assertmsg ("putupl [:4409] #21 checktup",
			    melt_magic_discr ((melt_ptr_t)
					      ( /*_.TUPLREC___V31*/
					       meltfptr[30])) ==
			    MELTOBMAG_MULTIPLE);
	    melt_assertmsg ("putupl [:4409] #21 checkoff",
			    (1 >= 0
			     && 1 <
			     melt_multiple_length ((melt_ptr_t)
						   ( /*_.TUPLREC___V31*/
						    meltfptr[30]))));
	    ((meltmultiple_ptr_t) ( /*_.TUPLREC___V31*/ meltfptr[30]))->
	      tabval[1] =
	      (melt_ptr_t) ( /*_.OVARIADICINDEX__V17*/ meltfptr[16]);
	    ;
	    /*^putuple */
	    /*putupl#22 */
	    melt_assertmsg ("putupl [:4409] #22 checktup",
			    melt_magic_discr ((melt_ptr_t)
					      ( /*_.TUPLREC___V31*/
					       meltfptr[30])) ==
			    MELTOBMAG_MULTIPLE);
	    melt_assertmsg ("putupl [:4409] #22 checkoff",
			    (2 >= 0
			     && 2 <
			     melt_multiple_length ((melt_ptr_t)
						   ( /*_.TUPLREC___V31*/
						    meltfptr[30]))));
	    ((meltmultiple_ptr_t) ( /*_.TUPLREC___V31*/ meltfptr[30]))->
	      tabval[2] =
	      (melt_ptr_t) ( /*_.MAKE_STRINGCONST__V26*/ meltfptr[25]);
	    ;
	    /*^putuple */
	    /*putupl#23 */
	    melt_assertmsg ("putupl [:4409] #23 checktup",
			    melt_magic_discr ((melt_ptr_t)
					      ( /*_.TUPLREC___V31*/
					       meltfptr[30])) ==
			    MELTOBMAG_MULTIPLE);
	    melt_assertmsg ("putupl [:4409] #23 checkoff",
			    (3 >= 0
			     && 3 <
			     melt_multiple_length ((melt_ptr_t)
						   ( /*_.TUPLREC___V31*/
						    meltfptr[30]))));
	    ((meltmultiple_ptr_t) ( /*_.TUPLREC___V31*/ meltfptr[30]))->
	      tabval[3] = (melt_ptr_t) ( /*_.NOFFSET__V16*/ meltfptr[15]);
	    ;
	    /*^putuple */
	    /*putupl#24 */
	    melt_assertmsg ("putupl [:4409] #24 checktup",
			    melt_magic_discr ((melt_ptr_t)
					      ( /*_.TUPLREC___V31*/
					       meltfptr[30])) ==
			    MELTOBMAG_MULTIPLE);
	    melt_assertmsg ("putupl [:4409] #24 checkoff",
			    (4 >= 0
			     && 4 <
			     melt_multiple_length ((melt_ptr_t)
						   ( /*_.TUPLREC___V31*/
						    meltfptr[30]))));
	    ((meltmultiple_ptr_t) ( /*_.TUPLREC___V31*/ meltfptr[30]))->
	      tabval[4] =
	      (melt_ptr_t) ( /*_.MAKE_STRINGCONST__V27*/ meltfptr[26]);
	    ;
	    /*^putuple */
	    /*putupl#25 */
	    melt_assertmsg ("putupl [:4409] #25 checktup",
			    melt_magic_discr ((melt_ptr_t)
					      ( /*_.TUPLREC___V31*/
					       meltfptr[30])) ==
			    MELTOBMAG_MULTIPLE);
	    melt_assertmsg ("putupl [:4409] #25 checkoff",
			    (5 >= 0
			     && 5 <
			     melt_multiple_length ((melt_ptr_t)
						   ( /*_.TUPLREC___V31*/
						    meltfptr[30]))));
	    ((meltmultiple_ptr_t) ( /*_.TUPLREC___V31*/ meltfptr[30]))->
	      tabval[5] =
	      (melt_ptr_t) ( /*_.OVARIADICINDEX__V17*/ meltfptr[16]);
	    ;
	    /*^putuple */
	    /*putupl#26 */
	    melt_assertmsg ("putupl [:4409] #26 checktup",
			    melt_magic_discr ((melt_ptr_t)
					      ( /*_.TUPLREC___V31*/
					       meltfptr[30])) ==
			    MELTOBMAG_MULTIPLE);
	    melt_assertmsg ("putupl [:4409] #26 checkoff",
			    (6 >= 0
			     && 6 <
			     melt_multiple_length ((melt_ptr_t)
						   ( /*_.TUPLREC___V31*/
						    meltfptr[30]))));
	    ((meltmultiple_ptr_t) ( /*_.TUPLREC___V31*/ meltfptr[30]))->
	      tabval[6] =
	      (melt_ptr_t) ( /*_.MAKE_STRINGCONST__V28*/ meltfptr[27]);
	    ;
	    /*^putuple */
	    /*putupl#27 */
	    melt_assertmsg ("putupl [:4409] #27 checktup",
			    melt_magic_discr ((melt_ptr_t)
					      ( /*_.TUPLREC___V31*/
					       meltfptr[30])) ==
			    MELTOBMAG_MULTIPLE);
	    melt_assertmsg ("putupl [:4409] #27 checkoff",
			    (7 >= 0
			     && 7 <
			     melt_multiple_length ((melt_ptr_t)
						   ( /*_.TUPLREC___V31*/
						    meltfptr[30]))));
	    ((meltmultiple_ptr_t) ( /*_.TUPLREC___V31*/ meltfptr[30]))->
	      tabval[7] = (melt_ptr_t) ( /*_.NOFFSET__V16*/ meltfptr[15]);
	    ;
	    /*^putuple */
	    /*putupl#28 */
	    melt_assertmsg ("putupl [:4409] #28 checktup",
			    melt_magic_discr ((melt_ptr_t)
					      ( /*_.TUPLREC___V31*/
					       meltfptr[30])) ==
			    MELTOBMAG_MULTIPLE);
	    melt_assertmsg ("putupl [:4409] #28 checkoff",
			    (8 >= 0
			     && 8 <
			     melt_multiple_length ((melt_ptr_t)
						   ( /*_.TUPLREC___V31*/
						    meltfptr[30]))));
	    ((meltmultiple_ptr_t) ( /*_.TUPLREC___V31*/ meltfptr[30]))->
	      tabval[8] =
	      (melt_ptr_t) ( /*_.MAKE_STRINGCONST__V29*/ meltfptr[28]);
	    ;
	    /*^touch */
	    meltgc_touch ( /*_.TUPLREC___V31*/ meltfptr[30]);
	    ;
	    /*_.TUPLE___V30*/ meltfptr[29] =
	      /*_.TUPLREC___V31*/ meltfptr[30];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-genobj.melt:4409:/ clear");
	      /*clear *//*_.TUPLREC___V31*/ meltfptr[30] = 0;
	    /*^clear */
	      /*clear *//*_.TUPLREC___V31*/ meltfptr[30] = 0;
	  }			/*end multiallocblock */
	  ;
	  /*_.OTUPLE__V24*/ meltfptr[23] = /*_.TUPLE___V30*/ meltfptr[29];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-genobj.melt:4408:/ clear");
	     /*clear *//*_.MAKE_STRINGCONST__V25*/ meltfptr[24] = 0;
	  /*^clear */
	     /*clear *//*_.MAKE_STRINGCONST__V26*/ meltfptr[25] = 0;
	  /*^clear */
	     /*clear *//*_.MAKE_STRINGCONST__V27*/ meltfptr[26] = 0;
	  /*^clear */
	     /*clear *//*_.MAKE_STRINGCONST__V28*/ meltfptr[27] = 0;
	  /*^clear */
	     /*clear *//*_.MAKE_STRINGCONST__V29*/ meltfptr[28] = 0;
	  /*^clear */
	     /*clear *//*_.TUPLE___V30*/ meltfptr[29] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

	/*^block */
	/*anyblock */
	{

   /*_.MAKE_STRINGCONST__V32*/ meltfptr[30] =
	    (meltgc_new_stringdup
	     ((meltobject_ptr_t)
	      (( /*!DISCR_VERBATIM_STRING */ meltfrout->tabval[7])),
	      ("/*variadic argument stuff*/ meltxargtab_[")));;
	  /*^compute */
   /*_.MAKE_STRINGCONST__V33*/ meltfptr[24] =
	    (meltgc_new_stringdup
	     ((meltobject_ptr_t)
	      (( /*!DISCR_VERBATIM_STRING */ meltfrout->tabval[7])),
	      (" + ")));;
	  /*^compute */
   /*_.MAKE_STRINGCONST__V34*/ meltfptr[25] =
	    (meltgc_new_stringdup
	     ((meltobject_ptr_t)
	      (( /*!DISCR_VERBATIM_STRING */ meltfrout->tabval[7])),
	      ("].")));;
	  MELT_LOCATION ("warmelt-genobj.melt:4426:/ cond");
	  /*cond */ if (
			 /*ifisa */
			 melt_is_instance_of ((melt_ptr_t)
					      ( /*_.NCTYP__V15*/
					       meltfptr[14]),
					      (melt_ptr_t) (( /*!CLASS_CTYPE */ meltfrout->tabval[5])))
	    )			/*then */
	    {
	      /*^cond.then */
	      /*^getslot */
	      {
		melt_ptr_t slot = NULL, obj = NULL;
		obj = (melt_ptr_t) ( /*_.NCTYP__V15*/ meltfptr[14]) /*=obj*/ ;
		melt_object_get_field (slot, obj, 6, "CTYPE_ARGFIELD");
     /*_.CTYPE_ARGFIELD__V35*/ meltfptr[26] = slot;
	      };
	      ;
	    }
	  else
	    {			/*^cond.else */

    /*_.CTYPE_ARGFIELD__V35*/ meltfptr[26] = NULL;;
	    }
	  ;
	  /*^compute */
   /*_.MAKE_STRING__V36*/ meltfptr[27] =
	    (meltgc_new_stringdup
	     ((meltobject_ptr_t)
	      (( /*!DISCR_VERBATIM_STRING */ meltfrout->tabval[7])),
	      melt_string_str ((melt_ptr_t)
			       ( /*_.CTYPE_ARGFIELD__V35*/ meltfptr[26]))));;
	  MELT_LOCATION ("warmelt-genobj.melt:4420:/ blockmultialloc");
	  /*multiallocblock */
	  {
	    struct meltletrec_2_st
	    {
	      struct MELT_MULTIPLE_STRUCT (6) rtup_0__TUPLREC__x8;
	      long meltletrec_2_endgap;
	    } *meltletrec_2_ptr = 0;
	    meltletrec_2_ptr =
	      (struct meltletrec_2_st *)
	      meltgc_allocate (sizeof (struct meltletrec_2_st), 0);
	    /*^blockmultialloc.initfill */
	    /*inimult rtup_0__TUPLREC__x8 */
 /*_.TUPLREC___V38*/ meltfptr[29] =
	      (melt_ptr_t) & meltletrec_2_ptr->rtup_0__TUPLREC__x8;
	    meltletrec_2_ptr->rtup_0__TUPLREC__x8.discr =
	      (meltobject_ptr_t) (((melt_ptr_t)
				   (MELT_PREDEF (DISCR_MULTIPLE))));
	    meltletrec_2_ptr->rtup_0__TUPLREC__x8.nbval = 6;


	    /*^putuple */
	    /*putupl#29 */
	    melt_assertmsg ("putupl [:4420] #29 checktup",
			    melt_magic_discr ((melt_ptr_t)
					      ( /*_.TUPLREC___V38*/
					       meltfptr[29])) ==
			    MELTOBMAG_MULTIPLE);
	    melt_assertmsg ("putupl [:4420] #29 checkoff",
			    (0 >= 0
			     && 0 <
			     melt_multiple_length ((melt_ptr_t)
						   ( /*_.TUPLREC___V38*/
						    meltfptr[29]))));
	    ((meltmultiple_ptr_t) ( /*_.TUPLREC___V38*/ meltfptr[29]))->
	      tabval[0] =
	      (melt_ptr_t) ( /*_.MAKE_STRINGCONST__V32*/ meltfptr[30]);
	    ;
	    /*^putuple */
	    /*putupl#30 */
	    melt_assertmsg ("putupl [:4420] #30 checktup",
			    melt_magic_discr ((melt_ptr_t)
					      ( /*_.TUPLREC___V38*/
					       meltfptr[29])) ==
			    MELTOBMAG_MULTIPLE);
	    melt_assertmsg ("putupl [:4420] #30 checkoff",
			    (1 >= 0
			     && 1 <
			     melt_multiple_length ((melt_ptr_t)
						   ( /*_.TUPLREC___V38*/
						    meltfptr[29]))));
	    ((meltmultiple_ptr_t) ( /*_.TUPLREC___V38*/ meltfptr[29]))->
	      tabval[1] =
	      (melt_ptr_t) ( /*_.OVARIADICINDEX__V17*/ meltfptr[16]);
	    ;
	    /*^putuple */
	    /*putupl#31 */
	    melt_assertmsg ("putupl [:4420] #31 checktup",
			    melt_magic_discr ((melt_ptr_t)
					      ( /*_.TUPLREC___V38*/
					       meltfptr[29])) ==
			    MELTOBMAG_MULTIPLE);
	    melt_assertmsg ("putupl [:4420] #31 checkoff",
			    (2 >= 0
			     && 2 <
			     melt_multiple_length ((melt_ptr_t)
						   ( /*_.TUPLREC___V38*/
						    meltfptr[29]))));
	    ((meltmultiple_ptr_t) ( /*_.TUPLREC___V38*/ meltfptr[29]))->
	      tabval[2] =
	      (melt_ptr_t) ( /*_.MAKE_STRINGCONST__V33*/ meltfptr[24]);
	    ;
	    /*^putuple */
	    /*putupl#32 */
	    melt_assertmsg ("putupl [:4420] #32 checktup",
			    melt_magic_discr ((melt_ptr_t)
					      ( /*_.TUPLREC___V38*/
					       meltfptr[29])) ==
			    MELTOBMAG_MULTIPLE);
	    melt_assertmsg ("putupl [:4420] #32 checkoff",
			    (3 >= 0
			     && 3 <
			     melt_multiple_length ((melt_ptr_t)
						   ( /*_.TUPLREC___V38*/
						    meltfptr[29]))));
	    ((meltmultiple_ptr_t) ( /*_.TUPLREC___V38*/ meltfptr[29]))->
	      tabval[3] = (melt_ptr_t) ( /*_.NOFFSET__V16*/ meltfptr[15]);
	    ;
	    /*^putuple */
	    /*putupl#33 */
	    melt_assertmsg ("putupl [:4420] #33 checktup",
			    melt_magic_discr ((melt_ptr_t)
					      ( /*_.TUPLREC___V38*/
					       meltfptr[29])) ==
			    MELTOBMAG_MULTIPLE);
	    melt_assertmsg ("putupl [:4420] #33 checkoff",
			    (4 >= 0
			     && 4 <
			     melt_multiple_length ((melt_ptr_t)
						   ( /*_.TUPLREC___V38*/
						    meltfptr[29]))));
	    ((meltmultiple_ptr_t) ( /*_.TUPLREC___V38*/ meltfptr[29]))->
	      tabval[4] =
	      (melt_ptr_t) ( /*_.MAKE_STRINGCONST__V34*/ meltfptr[25]);
	    ;
	    /*^putuple */
	    /*putupl#34 */
	    melt_assertmsg ("putupl [:4420] #34 checktup",
			    melt_magic_discr ((melt_ptr_t)
					      ( /*_.TUPLREC___V38*/
					       meltfptr[29])) ==
			    MELTOBMAG_MULTIPLE);
	    melt_assertmsg ("putupl [:4420] #34 checkoff",
			    (5 >= 0
			     && 5 <
			     melt_multiple_length ((melt_ptr_t)
						   ( /*_.TUPLREC___V38*/
						    meltfptr[29]))));
	    ((meltmultiple_ptr_t) ( /*_.TUPLREC___V38*/ meltfptr[29]))->
	      tabval[5] = (melt_ptr_t) ( /*_.MAKE_STRING__V36*/ meltfptr[27]);
	    ;
	    /*^touch */
	    meltgc_touch ( /*_.TUPLREC___V38*/ meltfptr[29]);
	    ;
	    /*_.TUPLE___V37*/ meltfptr[28] =
	      /*_.TUPLREC___V38*/ meltfptr[29];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-genobj.melt:4420:/ clear");
	      /*clear *//*_.TUPLREC___V38*/ meltfptr[29] = 0;
	    /*^clear */
	      /*clear *//*_.TUPLREC___V38*/ meltfptr[29] = 0;
	  }			/*end multiallocblock */
	  ;
	  /*_.OTUPLE__V24*/ meltfptr[23] = /*_.TUPLE___V37*/ meltfptr[28];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-genobj.melt:4408:/ clear");
	     /*clear *//*_.MAKE_STRINGCONST__V32*/ meltfptr[30] = 0;
	  /*^clear */
	     /*clear *//*_.MAKE_STRINGCONST__V33*/ meltfptr[24] = 0;
	  /*^clear */
	     /*clear *//*_.MAKE_STRINGCONST__V34*/ meltfptr[25] = 0;
	  /*^clear */
	     /*clear *//*_.CTYPE_ARGFIELD__V35*/ meltfptr[26] = 0;
	  /*^clear */
	     /*clear *//*_.MAKE_STRING__V36*/ meltfptr[27] = 0;
	  /*^clear */
	     /*clear *//*_.TUPLE___V37*/ meltfptr[28] = 0;
	}
	;
      }
    ;
    MELT_LOCATION ("warmelt-genobj.melt:4428:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*^rawallocobj */
    /*rawallocobj */
    {
      melt_ptr_t newobj = 0;
      melt_raw_object_create (newobj,
			      (melt_ptr_t) (( /*!CLASS_OBJCOMPUTE */
					     meltfrout->tabval[8])), (4),
			      "CLASS_OBJCOMPUTE");
  /*_.INST__V40*/ meltfptr[30] =
	newobj;
    };
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OBI_LOC",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V40*/ meltfptr[30])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V40*/ meltfptr[30]), (0),
			  ( /*_.NLOC__V13*/ meltfptr[12]), "OBI_LOC");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OBCPT_TYPE",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V40*/ meltfptr[30])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V40*/ meltfptr[30]), (3),
			  ( /*_.NCTYP__V15*/ meltfptr[14]), "OBCPT_TYPE");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OBCPT_EXPR",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V40*/ meltfptr[30])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V40*/ meltfptr[30]), (2),
			  ( /*_.OTUPLE__V24*/ meltfptr[23]), "OBCPT_EXPR");
    ;
    /*^touchobj */

    melt_dbgtrace_written_object ( /*_.INST__V40*/ meltfptr[30],
				  "newly made instance");
    ;
    /*_.OCOMP__V39*/ meltfptr[29] = /*_.INST__V40*/ meltfptr[30];;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:4434:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L8*/ meltfnum[0] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-genobj.melt:4434:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L8*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L9*/ meltfnum[8] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-genobj.melt:4434:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L9*/ meltfnum[8];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-genobj.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 4434;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring =
		"compilobj_nrep_variadic_argument ocomp=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.OCOMP__V39*/ meltfptr[29];
	      /*_.MELT_DEBUG_FUN__V43*/ meltfptr[26] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V42*/ meltfptr[25] =
	      /*_.MELT_DEBUG_FUN__V43*/ meltfptr[26];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-genobj.melt:4434:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L9*/ meltfnum[8] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V43*/ meltfptr[26] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V42*/ meltfptr[25] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-genobj.melt:4434:/ quasiblock");


      /*_.PROGN___V44*/ meltfptr[27] = /*_.IF___V42*/ meltfptr[25];;
      /*^compute */
      /*_.IFCPP___V41*/ meltfptr[24] = /*_.PROGN___V44*/ meltfptr[27];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:4434:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L8*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IF___V42*/ meltfptr[25] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V44*/ meltfptr[27] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V41*/ meltfptr[24] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-genobj.melt:4435:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.OCOMP__V39*/ meltfptr[29];;

    {
      MELT_LOCATION ("warmelt-genobj.melt:4435:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*_.LET___V23*/ meltfptr[21] = /*_.RETURN___V45*/ meltfptr[28];;

    MELT_LOCATION ("warmelt-genobj.melt:4406:/ clear");
	   /*clear *//*_#__L7*/ meltfnum[1] = 0;
    /*^clear */
	   /*clear *//*_.OTUPLE__V24*/ meltfptr[23] = 0;
    /*^clear */
	   /*clear *//*_.OCOMP__V39*/ meltfptr[29] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V41*/ meltfptr[24] = 0;
    /*^clear */
	   /*clear *//*_.RETURN___V45*/ meltfptr[28] = 0;
    /*_.LET___V12*/ meltfptr[10] = /*_.LET___V23*/ meltfptr[21];;

    MELT_LOCATION ("warmelt-genobj.melt:4397:/ clear");
	   /*clear *//*_.NLOC__V13*/ meltfptr[12] = 0;
    /*^clear */
	   /*clear *//*_.NVARIADIC__V14*/ meltfptr[13] = 0;
    /*^clear */
	   /*clear *//*_.NCTYP__V15*/ meltfptr[14] = 0;
    /*^clear */
	   /*clear *//*_.NOFFSET__V16*/ meltfptr[15] = 0;
    /*^clear */
	   /*clear *//*_.OVARIADICINDEX__V17*/ meltfptr[16] = 0;
    /*^clear */
	   /*clear *//*_.OVARIADICLENGTH__V18*/ meltfptr[17] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V19*/ meltfptr[18] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V21*/ meltfptr[19] = 0;
    /*^clear */
	   /*clear *//*_.LET___V23*/ meltfptr[21] = 0;
    MELT_LOCATION ("warmelt-genobj.melt:4393:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.LET___V12*/ meltfptr[10];;

    {
      MELT_LOCATION ("warmelt-genobj.melt:4393:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V4*/ meltfptr[3] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V8*/ meltfptr[4] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V10*/ meltfptr[5] = 0;
    /*^clear */
	   /*clear *//*_.LET___V12*/ meltfptr[10] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("COMPILOBJ_NREP_VARIADIC_ARGUMENT", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_126_warmelt_genobj_COMPILOBJ_NREP_VARIADIC_ARGUMENT_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_126_warmelt_genobj_COMPILOBJ_NREP_VARIADIC_ARGUMENT */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_127_warmelt_genobj_COMPILOBJ_NREP_CONSUMEVARIADIC (meltclosure_ptr_t
							    meltclosp_,
							    melt_ptr_t
							    meltfirstargp_,
							    const
							    melt_argdescr_cell_t
							    meltxargdescr_[],
							    union meltparam_un
							    * meltxargtab_,
							    const
							    melt_argdescr_cell_t
							    meltxresdescr_[],
							    union meltparam_un
							    * meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_127_warmelt_genobj_COMPILOBJ_NREP_CONSUMEVARIADIC_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_127_warmelt_genobj_COMPILOBJ_NREP_CONSUMEVARIADIC_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 40
    melt_ptr_t mcfr_varptr[40];
#define MELTFRAM_NBVARNUM 11
    long mcfr_varnum[11];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_127_warmelt_genobj_COMPILOBJ_NREP_CONSUMEVARIADIC is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct
	 frame_meltrout_127_warmelt_genobj_COMPILOBJ_NREP_CONSUMEVARIADIC_st
	 *) meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 40; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_127_warmelt_genobj_COMPILOBJ_NREP_CONSUMEVARIADIC nbval 40*/
  meltfram__.mcfr_nbvar = 40 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("COMPILOBJ_NREP_CONSUMEVARIADIC", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-genobj.melt:4441:/ getarg");
 /*_.RCV__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.GCX__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.GCX__V3*/ meltfptr[2])) != NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:4442:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L1*/ meltfnum[0] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.RCV__V2*/ meltfptr[1]),
			     (melt_ptr_t) (( /*!CLASS_NREP_CONSUME_VARIADIC */
					    meltfrout->tabval[0])));;
      MELT_LOCATION ("warmelt-genobj.melt:4442:/ cond");
      /*cond */ if ( /*_#IS_A__L1*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V5*/ meltfptr[4] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-genobj.melt:4442:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check rcv"),
				  ("warmelt-genobj.melt")
				  ? ("warmelt-genobj.melt") : __FILE__,
				  (4442) ? (4442) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V5*/ meltfptr[4] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V4*/ meltfptr[3] = /*_.IFELSE___V5*/ meltfptr[4];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:4442:/ clear");
	     /*clear *//*_#IS_A__L1*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V5*/ meltfptr[4] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V4*/ meltfptr[3] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:4443:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L2*/ meltfnum[0] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.GCX__V3*/ meltfptr[2]),
			     (melt_ptr_t) (( /*!CLASS_C_GENERATION_CONTEXT */
					    meltfrout->tabval[1])));;
      MELT_LOCATION ("warmelt-genobj.melt:4443:/ cond");
      /*cond */ if ( /*_#IS_A__L2*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V7*/ meltfptr[6] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-genobj.melt:4443:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check gcx"),
				  ("warmelt-genobj.melt")
				  ? ("warmelt-genobj.melt") : __FILE__,
				  (4443) ? (4443) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V7*/ meltfptr[6] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V6*/ meltfptr[4] = /*_.IFELSE___V7*/ meltfptr[6];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:4443:/ clear");
	     /*clear *//*_#IS_A__L2*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V7*/ meltfptr[6] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V6*/ meltfptr[4] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:4444:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L3*/ meltfnum[0] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-genobj.melt:4444:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L3*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L4*/ meltfnum[3] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-genobj.melt:4444:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L4*/ meltfnum[3];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-genobj.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 4444;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring =
		"compilobj_nrep_consumevariadic rcv=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.RCV__V2*/ meltfptr[1];
	      /*_.MELT_DEBUG_FUN__V10*/ meltfptr[9] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[2])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V9*/ meltfptr[8] =
	      /*_.MELT_DEBUG_FUN__V10*/ meltfptr[9];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-genobj.melt:4444:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L4*/ meltfnum[3] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V10*/ meltfptr[9] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V9*/ meltfptr[8] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-genobj.melt:4444:/ quasiblock");


      /*_.PROGN___V11*/ meltfptr[9] = /*_.IF___V9*/ meltfptr[8];;
      /*^compute */
      /*_.IFCPP___V8*/ meltfptr[6] = /*_.PROGN___V11*/ meltfptr[9];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:4444:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L3*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IF___V9*/ meltfptr[8] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V11*/ meltfptr[9] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V8*/ meltfptr[6] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-genobj.melt:4445:/ quasiblock");


    /*^getslot */
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.RCV__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 0, "NREP_LOC");
  /*_.NLOC__V13*/ meltfptr[9] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-genobj.melt:4446:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.RCV__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 1, "NCONSVA_VARIADIC");
  /*_.NVARIADIC__V14*/ meltfptr[13] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-genobj.melt:4447:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.RCV__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 2, "NCONSVA_CTYPES");
  /*_.NCTYPES__V15*/ meltfptr[14] = slot;
    };
    ;
 /*_.OLIST__V16*/ meltfptr[15] =
      (meltgc_new_list
       ((meltobject_ptr_t) (( /*!DISCR_LIST */ meltfrout->tabval[3]))));;
    MELT_LOCATION ("warmelt-genobj.melt:4449:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      /*_.OVARIADICINDEX__V17*/ meltfptr[16] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!VARIADIC_INDEX_IDSTR */ meltfrout->tabval[4])),
		    (melt_ptr_t) ( /*_.NVARIADIC__V14*/ meltfptr[13]), (""),
		    (union meltparam_un *) 0, "", (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-genobj.melt:4450:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      /*_.OVARIADICLENGTH__V18*/ meltfptr[17] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!VARIADIC_LENGTH_IDSTR */ meltfrout->tabval[5])),
		    (melt_ptr_t) ( /*_.NVARIADIC__V14*/ meltfptr[13]), (""),
		    (union meltparam_un *) 0, "", (union meltparam_un *) 0);
    }
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:4452:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_MULTIPLE_OR_NULL__L5*/ meltfnum[3] =
	(( /*_.NCTYPES__V15*/ meltfptr[14]) == NULL
	 ||
	 (melt_unsafe_magic_discr
	  ((melt_ptr_t) ( /*_.NCTYPES__V15*/ meltfptr[14])) ==
	  MELTOBMAG_MULTIPLE));;
      MELT_LOCATION ("warmelt-genobj.melt:4452:/ cond");
      /*cond */ if ( /*_#IS_MULTIPLE_OR_NULL__L5*/ meltfnum[3])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V20*/ meltfptr[19] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-genobj.melt:4452:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check nctypes"),
				  ("warmelt-genobj.melt")
				  ? ("warmelt-genobj.melt") : __FILE__,
				  (4452) ? (4452) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V20*/ meltfptr[19] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V19*/ meltfptr[18] = /*_.IFELSE___V20*/ meltfptr[19];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:4452:/ clear");
	     /*clear *//*_#IS_MULTIPLE_OR_NULL__L5*/ meltfnum[3] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V20*/ meltfptr[19] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V19*/ meltfptr[18] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    /*^compute */
 /*_.MAKE_STRINGCONST__V21*/ meltfptr[19] =
      (meltgc_new_stringdup
       ((meltobject_ptr_t)
	(( /*!DISCR_VERBATIM_STRING */ meltfrout->tabval[6])),
	("/*consume variadic ")));;

    {
      MELT_LOCATION ("warmelt-genobj.melt:4453:/ locexp");
      meltgc_append_list ((melt_ptr_t) ( /*_.OLIST__V16*/ meltfptr[15]),
			  (melt_ptr_t) ( /*_.MAKE_STRINGCONST__V21*/
					meltfptr[19]));
    }
    ;
    /*citerblock FOREACH_IN_MULTIPLE */
    {
      /* start foreach_in_multiple meltcit1__EACHTUP */
      long meltcit1__EACHTUP_ln =
	melt_multiple_length ((melt_ptr_t) /*_.NCTYPES__V15*/ meltfptr[14]);
      for ( /*_#TYPIX__L6*/ meltfnum[0] = 0;
	   ( /*_#TYPIX__L6*/ meltfnum[0] >= 0)
	   && ( /*_#TYPIX__L6*/ meltfnum[0] < meltcit1__EACHTUP_ln);
	/*_#TYPIX__L6*/ meltfnum[0]++)
	{
	  /*_.CURCTYP__V22*/ meltfptr[21] =
	    melt_multiple_nth ((melt_ptr_t)
			       ( /*_.NCTYPES__V15*/ meltfptr[14]),
			       /*_#TYPIX__L6*/ meltfnum[0]);




#if MELT_HAVE_DEBUG
	  MELT_LOCATION ("warmelt-genobj.melt:4457:/ cppif.then");
	  /*^block */
	  /*anyblock */
	  {

	    /*^checksignal */
	    MELT_CHECK_SIGNAL ();
	    ;
    /*_#IS_A__L7*/ meltfnum[3] =
	      melt_is_instance_of ((melt_ptr_t)
				   ( /*_.CURCTYP__V22*/ meltfptr[21]),
				   (melt_ptr_t) (( /*!CLASS_CTYPE */
						  meltfrout->tabval[7])));;
	    MELT_LOCATION ("warmelt-genobj.melt:4457:/ cond");
	    /*cond */ if ( /*_#IS_A__L7*/ meltfnum[3])	/*then */
	      {
		/*^cond.then */
		/*_.IFELSE___V24*/ meltfptr[23] = ( /*nil */ NULL);;
	      }
	    else
	      {
		MELT_LOCATION ("warmelt-genobj.melt:4457:/ cond.else");

		/*^block */
		/*anyblock */
		{




		  {
		    /*^locexp */
		    melt_assert_failed (("check curctyp"),
					("warmelt-genobj.melt")
					? ("warmelt-genobj.melt") : __FILE__,
					(4457) ? (4457) : __LINE__,
					__FUNCTION__);
		    ;
		  }
		  ;
		/*clear *//*_.IFELSE___V24*/ meltfptr[23] = 0;
		  /*epilog */
		}
		;
	      }
	    ;
	    /*^compute */
	    /*_.IFCPP___V23*/ meltfptr[22] = /*_.IFELSE___V24*/ meltfptr[23];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-genobj.melt:4457:/ clear");
	      /*clear *//*_#IS_A__L7*/ meltfnum[3] = 0;
	    /*^clear */
	      /*clear *//*_.IFELSE___V24*/ meltfptr[23] = 0;
	  }

#else /*MELT_HAVE_DEBUG */
	  /*^cppif.else */
	  /*_.IFCPP___V23*/ meltfptr[22] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
	  ;
	  MELT_LOCATION ("warmelt-genobj.melt:4458:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
  /*_#__L8*/ meltfnum[3] =
	    (( /*_.CURCTYP__V22*/ meltfptr[21]) ==
	     (( /*!CTYPE_VALUE */ meltfrout->tabval[8])));;
	  MELT_LOCATION ("warmelt-genobj.melt:4458:/ cond");
	  /*cond */ if ( /*_#__L8*/ meltfnum[3])	/*then */
	    {
	      /*^cond.then */
	      /*^block */
	      /*anyblock */
	      {

    /*_.MAKE_STRINGCONST__V25*/ meltfptr[23] =
		  (meltgc_new_stringdup
		   ((meltobject_ptr_t)
		    (( /*!DISCR_VERBATIM_STRING */ meltfrout->tabval[6])),
		    ("Value")));;

		{
		  MELT_LOCATION ("warmelt-genobj.melt:4459:/ locexp");
		  meltgc_append_list ((melt_ptr_t)
				      ( /*_.OLIST__V16*/ meltfptr[15]),
				      (melt_ptr_t) ( /*_.MAKE_STRINGCONST__V25*/ meltfptr[23]));
		}
		;
		/*epilog */

		MELT_LOCATION ("warmelt-genobj.melt:4458:/ clear");
	      /*clear *//*_.MAKE_STRINGCONST__V25*/ meltfptr[23] = 0;
	      }
	      ;
	    }
	  else
	    {			/*^cond.else */

	      /*^block */
	      /*anyblock */
	      {

		MELT_LOCATION ("warmelt-genobj.melt:4460:/ cond");
		/*cond */ if (
			       /*ifisa */
			       melt_is_instance_of ((melt_ptr_t)
						    ( /*_.CURCTYP__V22*/
						     meltfptr[21]),
						    (melt_ptr_t) (( /*!CLASS_CTYPE */ meltfrout->tabval[7])))
		  )		/*then */
		  {
		    /*^cond.then */
		    /*^getslot */
		    {
		      melt_ptr_t slot = NULL, obj = NULL;
		      obj =
			(melt_ptr_t) ( /*_.CURCTYP__V22*/ meltfptr[21])
			/*=obj*/ ;
		      melt_object_get_field (slot, obj, 2, "CTYPE_KEYWORD");
      /*_.CTYPE_KEYWORD__V26*/ meltfptr[23] = slot;
		    };
		    ;
		  }
		else
		  {		/*^cond.else */

     /*_.CTYPE_KEYWORD__V26*/ meltfptr[23] = NULL;;
		  }
		;
		MELT_LOCATION ("warmelt-genobj.melt:4460:/ cond");
		/*cond */ if (
			       /*ifisa */
			       melt_is_instance_of ((melt_ptr_t)
						    ( /*_.CTYPE_KEYWORD__V26*/
						     meltfptr[23]),
						    (melt_ptr_t) (( /*!CLASS_NAMED */ meltfrout->tabval[9])))
		  )		/*then */
		  {
		    /*^cond.then */
		    /*^getslot */
		    {
		      melt_ptr_t slot = NULL, obj = NULL;
		      obj =
			(melt_ptr_t) ( /*_.CTYPE_KEYWORD__V26*/ meltfptr[23])
			/*=obj*/ ;
		      melt_object_get_field (slot, obj, 1, "NAMED_NAME");
      /*_.NAMED_NAME__V27*/ meltfptr[26] = slot;
		    };
		    ;
		  }
		else
		  {		/*^cond.else */

     /*_.NAMED_NAME__V27*/ meltfptr[26] = NULL;;
		  }
		;
		/*^compute */
    /*_.MAKE_STRING__V28*/ meltfptr[27] =
		  (meltgc_new_stringdup
		   ((meltobject_ptr_t)
		    (( /*!DISCR_VERBATIM_STRING */ meltfrout->tabval[6])),
		    melt_string_str ((melt_ptr_t)
				     ( /*_.NAMED_NAME__V27*/
				      meltfptr[26]))));;

		{
		  MELT_LOCATION ("warmelt-genobj.melt:4460:/ locexp");
		  meltgc_append_list ((melt_ptr_t)
				      ( /*_.OLIST__V16*/ meltfptr[15]),
				      (melt_ptr_t) ( /*_.MAKE_STRING__V28*/
						    meltfptr[27]));
		}
		;
		/*epilog */

		MELT_LOCATION ("warmelt-genobj.melt:4458:/ clear");
	      /*clear *//*_.CTYPE_KEYWORD__V26*/ meltfptr[23] = 0;
		/*^clear */
	      /*clear *//*_.NAMED_NAME__V27*/ meltfptr[26] = 0;
		/*^clear */
	      /*clear *//*_.MAKE_STRING__V28*/ meltfptr[27] = 0;
	      }
	      ;
	    }
	  ;
	  if ( /*_#TYPIX__L6*/ meltfnum[0] < 0)
	    break;
	}			/* end  foreach_in_multiple meltcit1__EACHTUP */

      /*citerepilog */

      MELT_LOCATION ("warmelt-genobj.melt:4454:/ clear");
	    /*clear *//*_.CURCTYP__V22*/ meltfptr[21] = 0;
      /*^clear */
	    /*clear *//*_#TYPIX__L6*/ meltfnum[0] = 0;
      /*^clear */
	    /*clear *//*_.IFCPP___V23*/ meltfptr[22] = 0;
      /*^clear */
	    /*clear *//*_#__L8*/ meltfnum[3] = 0;
    }				/*endciterblock FOREACH_IN_MULTIPLE */
    ;
 /*_.MAKE_STRINGCONST__V29*/ meltfptr[23] =
      (meltgc_new_stringdup
       ((meltobject_ptr_t)
	(( /*!DISCR_VERBATIM_STRING */ meltfrout->tabval[6])), (" !*/ ")));;

    {
      MELT_LOCATION ("warmelt-genobj.melt:4461:/ locexp");
      meltgc_append_list ((melt_ptr_t) ( /*_.OLIST__V16*/ meltfptr[15]),
			  (melt_ptr_t) ( /*_.MAKE_STRINGCONST__V29*/
					meltfptr[23]));
    }
    ;

    {
      MELT_LOCATION ("warmelt-genobj.melt:4462:/ locexp");
      meltgc_append_list ((melt_ptr_t) ( /*_.OLIST__V16*/ meltfptr[15]),
			  (melt_ptr_t) ( /*_.OVARIADICINDEX__V17*/
					meltfptr[16]));
    }
    ;
 /*_.MAKE_STRINGCONST__V30*/ meltfptr[26] =
      (meltgc_new_stringdup
       ((meltobject_ptr_t)
	(( /*!DISCR_VERBATIM_STRING */ meltfrout->tabval[6])), (" += ")));;

    {
      MELT_LOCATION ("warmelt-genobj.melt:4463:/ locexp");
      meltgc_append_list ((melt_ptr_t) ( /*_.OLIST__V16*/ meltfptr[15]),
			  (melt_ptr_t) ( /*_.MAKE_STRINGCONST__V30*/
					meltfptr[26]));
    }
    ;
 /*_#MULTIPLE_LENGTH__L9*/ meltfnum[8] =
      (melt_multiple_length
       ((melt_ptr_t) ( /*_.NCTYPES__V15*/ meltfptr[14])));;
    /*^compute */
 /*_.MAKE_INTEGERBOX__V31*/ meltfptr[27] =
      (meltgc_new_int
       ((meltobject_ptr_t) (( /*!DISCR_INTEGER */ meltfrout->tabval[10])),
	( /*_#MULTIPLE_LENGTH__L9*/ meltfnum[8])));;

    {
      MELT_LOCATION ("warmelt-genobj.melt:4464:/ locexp");
      meltgc_append_list ((melt_ptr_t) ( /*_.OLIST__V16*/ meltfptr[15]),
			  (melt_ptr_t) ( /*_.MAKE_INTEGERBOX__V31*/
					meltfptr[27]));
    }
    ;
    MELT_LOCATION ("warmelt-genobj.melt:4465:/ quasiblock");


    MELT_LOCATION ("warmelt-genobj.melt:4466:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      /*_.OTUPLE__V33*/ meltfptr[32] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!LIST_TO_MULTIPLE */ meltfrout->tabval[11])),
		    (melt_ptr_t) ( /*_.OLIST__V16*/ meltfptr[15]), (""),
		    (union meltparam_un *) 0, "", (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-genobj.melt:4467:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*^rawallocobj */
    /*rawallocobj */
    {
      melt_ptr_t newobj = 0;
      melt_raw_object_create (newobj,
			      (melt_ptr_t) (( /*!CLASS_OBJCOMPUTE */
					     meltfrout->tabval[12])), (4),
			      "CLASS_OBJCOMPUTE");
  /*_.INST__V35*/ meltfptr[34] =
	newobj;
    };
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OBI_LOC",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V35*/ meltfptr[34])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V35*/ meltfptr[34]), (0),
			  ( /*_.NLOC__V13*/ meltfptr[9]), "OBI_LOC");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OBCPT_TYPE",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V35*/ meltfptr[34])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V35*/ meltfptr[34]), (3),
			  (( /*!CTYPE_VOID */ meltfrout->tabval[13])),
			  "OBCPT_TYPE");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OBCPT_EXPR",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V35*/ meltfptr[34])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V35*/ meltfptr[34]), (2),
			  ( /*_.OTUPLE__V33*/ meltfptr[32]), "OBCPT_EXPR");
    ;
    /*^touchobj */

    melt_dbgtrace_written_object ( /*_.INST__V35*/ meltfptr[34],
				  "newly made instance");
    ;
    /*_.OCOMP__V34*/ meltfptr[33] = /*_.INST__V35*/ meltfptr[34];;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:4473:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L10*/ meltfnum[9] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-genobj.melt:4473:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L10*/ meltfnum[9])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L11*/ meltfnum[10] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-genobj.melt:4473:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long =
		/*_#THE_MELTCALLCOUNT__L11*/ meltfnum[10];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-genobj.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 4473;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring =
		"compilobj_nrep_consumevariadic ocomp=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.OCOMP__V34*/ meltfptr[33];
	      /*_.MELT_DEBUG_FUN__V38*/ meltfptr[37] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[2])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V37*/ meltfptr[36] =
	      /*_.MELT_DEBUG_FUN__V38*/ meltfptr[37];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-genobj.melt:4473:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L11*/ meltfnum[10] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V38*/ meltfptr[37] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V37*/ meltfptr[36] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-genobj.melt:4473:/ quasiblock");


      /*_.PROGN___V39*/ meltfptr[37] = /*_.IF___V37*/ meltfptr[36];;
      /*^compute */
      /*_.IFCPP___V36*/ meltfptr[35] = /*_.PROGN___V39*/ meltfptr[37];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:4473:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L10*/ meltfnum[9] = 0;
      /*^clear */
	     /*clear *//*_.IF___V37*/ meltfptr[36] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V39*/ meltfptr[37] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V36*/ meltfptr[35] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-genobj.melt:4474:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.OCOMP__V34*/ meltfptr[33];;

    {
      MELT_LOCATION ("warmelt-genobj.melt:4474:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*_.LET___V32*/ meltfptr[31] = /*_.RETURN___V40*/ meltfptr[36];;

    MELT_LOCATION ("warmelt-genobj.melt:4465:/ clear");
	   /*clear *//*_.OTUPLE__V33*/ meltfptr[32] = 0;
    /*^clear */
	   /*clear *//*_.OCOMP__V34*/ meltfptr[33] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V36*/ meltfptr[35] = 0;
    /*^clear */
	   /*clear *//*_.RETURN___V40*/ meltfptr[36] = 0;
    /*_.LET___V12*/ meltfptr[8] = /*_.LET___V32*/ meltfptr[31];;

    MELT_LOCATION ("warmelt-genobj.melt:4445:/ clear");
	   /*clear *//*_.NLOC__V13*/ meltfptr[9] = 0;
    /*^clear */
	   /*clear *//*_.NVARIADIC__V14*/ meltfptr[13] = 0;
    /*^clear */
	   /*clear *//*_.NCTYPES__V15*/ meltfptr[14] = 0;
    /*^clear */
	   /*clear *//*_.OLIST__V16*/ meltfptr[15] = 0;
    /*^clear */
	   /*clear *//*_.OVARIADICINDEX__V17*/ meltfptr[16] = 0;
    /*^clear */
	   /*clear *//*_.OVARIADICLENGTH__V18*/ meltfptr[17] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V19*/ meltfptr[18] = 0;
    /*^clear */
	   /*clear *//*_.MAKE_STRINGCONST__V21*/ meltfptr[19] = 0;
    /*^clear */
	   /*clear *//*_.MAKE_STRINGCONST__V29*/ meltfptr[23] = 0;
    /*^clear */
	   /*clear *//*_.MAKE_STRINGCONST__V30*/ meltfptr[26] = 0;
    /*^clear */
	   /*clear *//*_#MULTIPLE_LENGTH__L9*/ meltfnum[8] = 0;
    /*^clear */
	   /*clear *//*_.MAKE_INTEGERBOX__V31*/ meltfptr[27] = 0;
    /*^clear */
	   /*clear *//*_.LET___V32*/ meltfptr[31] = 0;
    MELT_LOCATION ("warmelt-genobj.melt:4441:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.LET___V12*/ meltfptr[8];;

    {
      MELT_LOCATION ("warmelt-genobj.melt:4441:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V4*/ meltfptr[3] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V6*/ meltfptr[4] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V8*/ meltfptr[6] = 0;
    /*^clear */
	   /*clear *//*_.LET___V12*/ meltfptr[8] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("COMPILOBJ_NREP_CONSUMEVARIADIC", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_127_warmelt_genobj_COMPILOBJ_NREP_CONSUMEVARIADIC_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_127_warmelt_genobj_COMPILOBJ_NREP_CONSUMEVARIADIC */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_128_warmelt_genobj_PUTOBJDEST_OBJCOND (meltclosure_ptr_t meltclosp_,
						melt_ptr_t meltfirstargp_,
						const melt_argdescr_cell_t
						meltxargdescr_[],
						union meltparam_un *
						meltxargtab_,
						const melt_argdescr_cell_t
						meltxresdescr_[],
						union meltparam_un *
						meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_128_warmelt_genobj_PUTOBJDEST_OBJCOND_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_128_warmelt_genobj_PUTOBJDEST_OBJCOND_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 13
    melt_ptr_t mcfr_varptr[13];
#define MELTFRAM_NBVARNUM 2
    long mcfr_varnum[2];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_128_warmelt_genobj_PUTOBJDEST_OBJCOND is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_128_warmelt_genobj_PUTOBJDEST_OBJCOND_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 13; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_128_warmelt_genobj_PUTOBJDEST_OBJCOND nbval 13*/
  meltfram__.mcfr_nbvar = 13 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("PUTOBJDEST_OBJCOND", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-genobj.melt:4479:/ getarg");
 /*_.RECV__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.DESTO__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.DESTO__V3*/ meltfptr[2])) !=
	      NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:4480:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L1*/ meltfnum[0] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.RECV__V2*/ meltfptr[1]),
			     (melt_ptr_t) (( /*!CLASS_OBJCOND */ meltfrout->
					    tabval[0])));;
      MELT_LOCATION ("warmelt-genobj.melt:4480:/ cond");
      /*cond */ if ( /*_#IS_A__L1*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V5*/ meltfptr[4] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-genobj.melt:4480:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check recv"),
				  ("warmelt-genobj.melt")
				  ? ("warmelt-genobj.melt") : __FILE__,
				  (4480) ? (4480) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V5*/ meltfptr[4] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V4*/ meltfptr[3] = /*_.IFELSE___V5*/ meltfptr[4];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:4480:/ clear");
	     /*clear *//*_#IS_A__L1*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V5*/ meltfptr[4] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V4*/ meltfptr[3] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:4481:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L2*/ meltfnum[0] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.DESTO__V3*/ meltfptr[2]),
			     (melt_ptr_t) (( /*!CLASS_OBJPUREVALUE */
					    meltfrout->tabval[1])));;
      MELT_LOCATION ("warmelt-genobj.melt:4481:/ cond");
      /*cond */ if ( /*_#IS_A__L2*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V7*/ meltfptr[6] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-genobj.melt:4481:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check desto"),
				  ("warmelt-genobj.melt")
				  ? ("warmelt-genobj.melt") : __FILE__,
				  (4481) ? (4481) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V7*/ meltfptr[6] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V6*/ meltfptr[4] = /*_.IFELSE___V7*/ meltfptr[6];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:4481:/ clear");
	     /*clear *//*_#IS_A__L2*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V7*/ meltfptr[6] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V6*/ meltfptr[4] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-genobj.melt:4482:/ quasiblock");


    /*^getslot */
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.RECV__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 2, "OBCOND_THEN");
  /*_.OTHEN__V9*/ meltfptr[8] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-genobj.melt:4483:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.RECV__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 3, "OBCOND_ELSE");
  /*_.OELSE__V10*/ meltfptr[9] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-genobj.melt:4485:/ quasiblock");


    /*^checksignal */
    MELT_CHECK_SIGNAL ();
    ;
    /*^msend */
    /*msend */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^ojbmsend.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.DESTO__V3*/ meltfptr[2];
      /*_.DTHEN__V12*/ meltfptr[11] =
	meltgc_send ((melt_ptr_t) ( /*_.OTHEN__V9*/ meltfptr[8]),
		     (melt_ptr_t) (( /*!PUT_OBJDEST */ meltfrout->tabval[2])),
		     (MELTBPARSTR_PTR ""), argtab, "",
		     (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-genobj.melt:4486:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^msend */
    /*msend */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^ojbmsend.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.DESTO__V3*/ meltfptr[2];
      /*_.DELSE__V13*/ meltfptr[12] =
	meltgc_send ((melt_ptr_t) ( /*_.OELSE__V10*/ meltfptr[9]),
		     (melt_ptr_t) (( /*!PUT_OBJDEST */ meltfrout->tabval[2])),
		     (MELTBPARSTR_PTR ""), argtab, "",
		     (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-genobj.melt:4487:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OBCOND_THEN",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.RECV__V2*/ meltfptr[1])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.RECV__V2*/ meltfptr[1]), (2),
			  ( /*_.DTHEN__V12*/ meltfptr[11]), "OBCOND_THEN");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OBCOND_ELSE",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.RECV__V2*/ meltfptr[1])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.RECV__V2*/ meltfptr[1]), (3),
			  ( /*_.DELSE__V13*/ meltfptr[12]), "OBCOND_ELSE");
    ;
    /*^touch */
    meltgc_touch ( /*_.RECV__V2*/ meltfptr[1]);
    ;
    /*^touchobj */

    melt_dbgtrace_written_object ( /*_.RECV__V2*/ meltfptr[1], "put-fields");
    ;

    /*_.LET___V11*/ meltfptr[10] = /*_.RECV__V2*/ meltfptr[1];;

    MELT_LOCATION ("warmelt-genobj.melt:4485:/ clear");
	   /*clear *//*_.DTHEN__V12*/ meltfptr[11] = 0;
    /*^clear */
	   /*clear *//*_.DELSE__V13*/ meltfptr[12] = 0;
    /*_.LET___V8*/ meltfptr[6] = /*_.LET___V11*/ meltfptr[10];;

    MELT_LOCATION ("warmelt-genobj.melt:4482:/ clear");
	   /*clear *//*_.OTHEN__V9*/ meltfptr[8] = 0;
    /*^clear */
	   /*clear *//*_.OELSE__V10*/ meltfptr[9] = 0;
    /*^clear */
	   /*clear *//*_.LET___V11*/ meltfptr[10] = 0;
    MELT_LOCATION ("warmelt-genobj.melt:4479:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.LET___V8*/ meltfptr[6];;

    {
      MELT_LOCATION ("warmelt-genobj.melt:4479:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V4*/ meltfptr[3] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V6*/ meltfptr[4] = 0;
    /*^clear */
	   /*clear *//*_.LET___V8*/ meltfptr[6] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("PUTOBJDEST_OBJCOND", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_128_warmelt_genobj_PUTOBJDEST_OBJCOND_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_128_warmelt_genobj_PUTOBJDEST_OBJCOND */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_129_warmelt_genobj_COMPILOBJ_NREP_CPPIF (meltclosure_ptr_t
						  meltclosp_,
						  melt_ptr_t meltfirstargp_,
						  const melt_argdescr_cell_t
						  meltxargdescr_[],
						  union meltparam_un *
						  meltxargtab_,
						  const melt_argdescr_cell_t
						  meltxresdescr_[],
						  union meltparam_un *
						  meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_129_warmelt_genobj_COMPILOBJ_NREP_CPPIF_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_129_warmelt_genobj_COMPILOBJ_NREP_CPPIF_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 24
    melt_ptr_t mcfr_varptr[24];
#define MELTFRAM_NBVARNUM 4
    long mcfr_varnum[4];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_129_warmelt_genobj_COMPILOBJ_NREP_CPPIF is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_129_warmelt_genobj_COMPILOBJ_NREP_CPPIF_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 24; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_129_warmelt_genobj_COMPILOBJ_NREP_CPPIF nbval 24*/
  meltfram__.mcfr_nbvar = 24 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("COMPILOBJ_NREP_CPPIF", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-genobj.melt:4496:/ getarg");
 /*_.PIF__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.GCX__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.GCX__V3*/ meltfptr[2])) != NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:4497:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L1*/ meltfnum[0] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.PIF__V2*/ meltfptr[1]),
			     (melt_ptr_t) (( /*!CLASS_NREP_CPPIF */
					    meltfrout->tabval[0])));;
      MELT_LOCATION ("warmelt-genobj.melt:4497:/ cond");
      /*cond */ if ( /*_#IS_A__L1*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V5*/ meltfptr[4] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-genobj.melt:4497:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check pif"),
				  ("warmelt-genobj.melt")
				  ? ("warmelt-genobj.melt") : __FILE__,
				  (4497) ? (4497) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V5*/ meltfptr[4] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V4*/ meltfptr[3] = /*_.IFELSE___V5*/ meltfptr[4];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:4497:/ clear");
	     /*clear *//*_#IS_A__L1*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V5*/ meltfptr[4] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V4*/ meltfptr[3] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:4498:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L2*/ meltfnum[0] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.GCX__V3*/ meltfptr[2]),
			     (melt_ptr_t) (( /*!CLASS_C_GENERATION_CONTEXT */
					    meltfrout->tabval[1])));;
      MELT_LOCATION ("warmelt-genobj.melt:4498:/ cond");
      /*cond */ if ( /*_#IS_A__L2*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V7*/ meltfptr[6] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-genobj.melt:4498:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check gcx"),
				  ("warmelt-genobj.melt")
				  ? ("warmelt-genobj.melt") : __FILE__,
				  (4498) ? (4498) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V7*/ meltfptr[6] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V6*/ meltfptr[4] = /*_.IFELSE___V7*/ meltfptr[6];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:4498:/ clear");
	     /*clear *//*_#IS_A__L2*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V7*/ meltfptr[6] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V6*/ meltfptr[4] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-genobj.melt:4499:/ quasiblock");


    /*^getslot */
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.PIF__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 0, "NREP_LOC");
  /*_.LOC__V9*/ meltfptr[8] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-genobj.melt:4500:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.PIF__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 1, "NIFP_COND");
  /*_.NCOND__V10*/ meltfptr[9] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-genobj.melt:4501:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.PIF__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 2, "NIFP_THEN");
  /*_.NTHEN__V11*/ meltfptr[10] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-genobj.melt:4502:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.PIF__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 3, "NIFP_ELSE");
  /*_.NELSE__V12*/ meltfptr[11] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-genobj.melt:4503:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.PIF__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 4, "NIFP_CTYP");
  /*_.CTYP__V13*/ meltfptr[12] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-genobj.melt:4505:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#IS_A__L3*/ meltfnum[0] =
      melt_is_instance_of ((melt_ptr_t) ( /*_.NCOND__V10*/ meltfptr[9]),
			   (melt_ptr_t) (( /*!CLASS_SYMBOL */ meltfrout->
					  tabval[2])));;
    MELT_LOCATION ("warmelt-genobj.melt:4505:/ cond");
    /*cond */ if ( /*_#IS_A__L3*/ meltfnum[0])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  /*^getslot */
	  {
	    melt_ptr_t slot = NULL, obj = NULL;
	    obj = (melt_ptr_t) ( /*_.NCOND__V10*/ meltfptr[9]) /*=obj*/ ;
	    melt_object_get_field (slot, obj, 1, "NAMED_NAME");
    /*_.NAMED_NAME__V15*/ meltfptr[14] = slot;
	  };
	  ;
	  /*_.SCOND__V14*/ meltfptr[13] = /*_.NAMED_NAME__V15*/ meltfptr[14];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-genobj.melt:4505:/ clear");
	     /*clear *//*_.NAMED_NAME__V15*/ meltfptr[14] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-genobj.melt:4506:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
   /*_#IS_STRING__L4*/ meltfnum[3] =
	    (melt_magic_discr ((melt_ptr_t) ( /*_.NCOND__V10*/ meltfptr[9]))
	     == MELTOBMAG_STRING);;
	  MELT_LOCATION ("warmelt-genobj.melt:4506:/ cond");
	  /*cond */ if ( /*_#IS_STRING__L4*/ meltfnum[3])	/*then */
	    {
	      /*^cond.then */
	      /*_.IFELSE___V16*/ meltfptr[14] = /*_.NCOND__V10*/ meltfptr[9];;
	    }
	  else
	    {
	      MELT_LOCATION ("warmelt-genobj.melt:4506:/ cond.else");

	      /*^block */
	      /*anyblock */
	      {


#if MELT_HAVE_DEBUG
		MELT_LOCATION ("warmelt-genobj.melt:4507:/ cppif.then");
		/*^block */
		/*anyblock */
		{

		  /*^checksignal */
		  MELT_CHECK_SIGNAL ();
		  ;
		  /*^cond */
		  /*cond */ if (( /*nil */ NULL))	/*then */
		    {
		      /*^cond.then */
		      /*_.IFELSE___V18*/ meltfptr[17] = ( /*nil */ NULL);;
		    }
		  else
		    {
		      MELT_LOCATION ("warmelt-genobj.melt:4507:/ cond.else");

		      /*^block */
		      /*anyblock */
		      {




			{
			  /*^locexp */
			  melt_assert_failed (("invalid ncond in nrep_cppif"),
					      ("warmelt-genobj.melt")
					      ? ("warmelt-genobj.melt") :
					      __FILE__,
					      (4507) ? (4507) : __LINE__,
					      __FUNCTION__);
			  ;
			}
			;
		   /*clear *//*_.IFELSE___V18*/ meltfptr[17] = 0;
			/*epilog */
		      }
		      ;
		    }
		  ;
		  /*^compute */
		  /*_.IFCPP___V17*/ meltfptr[16] =
		    /*_.IFELSE___V18*/ meltfptr[17];;
		  /*epilog */

		  MELT_LOCATION ("warmelt-genobj.melt:4507:/ clear");
		 /*clear *//*_.IFELSE___V18*/ meltfptr[17] = 0;
		}

#else /*MELT_HAVE_DEBUG */
		/*^cppif.else */
		/*_.IFCPP___V17*/ meltfptr[16] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
		;
		MELT_LOCATION ("warmelt-genobj.melt:4507:/ quasiblock");


		/*_.PROGN___V19*/ meltfptr[17] =
		  /*_.IFCPP___V17*/ meltfptr[16];;
		/*^compute */
		/*_.IFELSE___V16*/ meltfptr[14] =
		  /*_.PROGN___V19*/ meltfptr[17];;
		/*epilog */

		MELT_LOCATION ("warmelt-genobj.melt:4506:/ clear");
	       /*clear *//*_.IFCPP___V17*/ meltfptr[16] = 0;
		/*^clear */
	       /*clear *//*_.PROGN___V19*/ meltfptr[17] = 0;
	      }
	      ;
	    }
	  ;
	  /*_.SCOND__V14*/ meltfptr[13] = /*_.IFELSE___V16*/ meltfptr[14];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-genobj.melt:4505:/ clear");
	     /*clear *//*_#IS_STRING__L4*/ meltfnum[3] = 0;
	  /*^clear */
	     /*clear *//*_.IFELSE___V16*/ meltfptr[14] = 0;
	}
	;
      }
    ;
    MELT_LOCATION ("warmelt-genobj.melt:4508:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^msend */
    /*msend */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^ojbmsend.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.GCX__V3*/ meltfptr[2];
      /*_.OTHEN__V20*/ meltfptr[16] =
	meltgc_send ((melt_ptr_t) ( /*_.NTHEN__V11*/ meltfptr[10]),
		     (melt_ptr_t) (( /*!COMPILE_OBJ */ meltfrout->tabval[3])),
		     (MELTBPARSTR_PTR ""), argtab, "",
		     (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-genobj.melt:4509:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^msend */
    /*msend */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^ojbmsend.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.GCX__V3*/ meltfptr[2];
      /*_.OELSE__V21*/ meltfptr[17] =
	meltgc_send ((melt_ptr_t) ( /*_.NELSE__V12*/ meltfptr[11]),
		     (melt_ptr_t) (( /*!COMPILE_OBJ */ meltfrout->tabval[3])),
		     (MELTBPARSTR_PTR ""), argtab, "",
		     (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-genobj.melt:4510:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*^rawallocobj */
    /*rawallocobj */
    {
      melt_ptr_t newobj = 0;
      melt_raw_object_create (newobj,
			      (melt_ptr_t) (( /*!CLASS_OBJCPPIF */ meltfrout->
					     tabval[4])), (4),
			      "CLASS_OBJCPPIF");
  /*_.INST__V23*/ meltfptr[22] =
	newobj;
    };
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OBI_LOC",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V23*/ meltfptr[22])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V23*/ meltfptr[22]), (0),
			  ( /*_.LOC__V9*/ meltfptr[8]), "OBI_LOC");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OBIFP_COND",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V23*/ meltfptr[22])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V23*/ meltfptr[22]), (1),
			  ( /*_.SCOND__V14*/ meltfptr[13]), "OBIFP_COND");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OBIFP_THEN",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V23*/ meltfptr[22])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V23*/ meltfptr[22]), (2),
			  ( /*_.OTHEN__V20*/ meltfptr[16]), "OBIFP_THEN");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OBIFP_ELSE",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V23*/ meltfptr[22])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V23*/ meltfptr[22]), (3),
			  ( /*_.OELSE__V21*/ meltfptr[17]), "OBIFP_ELSE");
    ;
    /*^touchobj */

    melt_dbgtrace_written_object ( /*_.INST__V23*/ meltfptr[22],
				  "newly made instance");
    ;
    /*_.RES__V22*/ meltfptr[14] = /*_.INST__V23*/ meltfptr[22];;
    MELT_LOCATION ("warmelt-genobj.melt:4516:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.RES__V22*/ meltfptr[14];;

    {
      MELT_LOCATION ("warmelt-genobj.melt:4516:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*_.LET___V8*/ meltfptr[6] = /*_.RETURN___V24*/ meltfptr[23];;

    MELT_LOCATION ("warmelt-genobj.melt:4499:/ clear");
	   /*clear *//*_.LOC__V9*/ meltfptr[8] = 0;
    /*^clear */
	   /*clear *//*_.NCOND__V10*/ meltfptr[9] = 0;
    /*^clear */
	   /*clear *//*_.NTHEN__V11*/ meltfptr[10] = 0;
    /*^clear */
	   /*clear *//*_.NELSE__V12*/ meltfptr[11] = 0;
    /*^clear */
	   /*clear *//*_.CTYP__V13*/ meltfptr[12] = 0;
    /*^clear */
	   /*clear *//*_#IS_A__L3*/ meltfnum[0] = 0;
    /*^clear */
	   /*clear *//*_.SCOND__V14*/ meltfptr[13] = 0;
    /*^clear */
	   /*clear *//*_.OTHEN__V20*/ meltfptr[16] = 0;
    /*^clear */
	   /*clear *//*_.OELSE__V21*/ meltfptr[17] = 0;
    /*^clear */
	   /*clear *//*_.RES__V22*/ meltfptr[14] = 0;
    /*^clear */
	   /*clear *//*_.RETURN___V24*/ meltfptr[23] = 0;
    MELT_LOCATION ("warmelt-genobj.melt:4496:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.LET___V8*/ meltfptr[6];;

    {
      MELT_LOCATION ("warmelt-genobj.melt:4496:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V4*/ meltfptr[3] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V6*/ meltfptr[4] = 0;
    /*^clear */
	   /*clear *//*_.LET___V8*/ meltfptr[6] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("COMPILOBJ_NREP_CPPIF", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_129_warmelt_genobj_COMPILOBJ_NREP_CPPIF_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_129_warmelt_genobj_COMPILOBJ_NREP_CPPIF */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_130_warmelt_genobj_PUTOBJDEST_OBJCPPIF (meltclosure_ptr_t meltclosp_,
						 melt_ptr_t meltfirstargp_,
						 const melt_argdescr_cell_t
						 meltxargdescr_[],
						 union meltparam_un *
						 meltxargtab_,
						 const melt_argdescr_cell_t
						 meltxresdescr_[],
						 union meltparam_un *
						 meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_130_warmelt_genobj_PUTOBJDEST_OBJCPPIF_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_130_warmelt_genobj_PUTOBJDEST_OBJCPPIF_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 13
    melt_ptr_t mcfr_varptr[13];
#define MELTFRAM_NBVARNUM 2
    long mcfr_varnum[2];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_130_warmelt_genobj_PUTOBJDEST_OBJCPPIF is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_130_warmelt_genobj_PUTOBJDEST_OBJCPPIF_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 13; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_130_warmelt_genobj_PUTOBJDEST_OBJCPPIF nbval 13*/
  meltfram__.mcfr_nbvar = 13 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("PUTOBJDEST_OBJCPPIF", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-genobj.melt:4522:/ getarg");
 /*_.RECV__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.DESTO__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.DESTO__V3*/ meltfptr[2])) !=
	      NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:4523:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L1*/ meltfnum[0] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.RECV__V2*/ meltfptr[1]),
			     (melt_ptr_t) (( /*!CLASS_OBJCPPIF */ meltfrout->
					    tabval[0])));;
      MELT_LOCATION ("warmelt-genobj.melt:4523:/ cond");
      /*cond */ if ( /*_#IS_A__L1*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V5*/ meltfptr[4] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-genobj.melt:4523:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check recv"),
				  ("warmelt-genobj.melt")
				  ? ("warmelt-genobj.melt") : __FILE__,
				  (4523) ? (4523) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V5*/ meltfptr[4] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V4*/ meltfptr[3] = /*_.IFELSE___V5*/ meltfptr[4];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:4523:/ clear");
	     /*clear *//*_#IS_A__L1*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V5*/ meltfptr[4] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V4*/ meltfptr[3] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:4524:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L2*/ meltfnum[0] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.DESTO__V3*/ meltfptr[2]),
			     (melt_ptr_t) (( /*!CLASS_OBJPUREVALUE */
					    meltfrout->tabval[1])));;
      MELT_LOCATION ("warmelt-genobj.melt:4524:/ cond");
      /*cond */ if ( /*_#IS_A__L2*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V7*/ meltfptr[6] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-genobj.melt:4524:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check desto"),
				  ("warmelt-genobj.melt")
				  ? ("warmelt-genobj.melt") : __FILE__,
				  (4524) ? (4524) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V7*/ meltfptr[6] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V6*/ meltfptr[4] = /*_.IFELSE___V7*/ meltfptr[6];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:4524:/ clear");
	     /*clear *//*_#IS_A__L2*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V7*/ meltfptr[6] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V6*/ meltfptr[4] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-genobj.melt:4525:/ quasiblock");


    /*^getslot */
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.RECV__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 2, "OBIFP_THEN");
  /*_.OTHEN__V9*/ meltfptr[8] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-genobj.melt:4526:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.RECV__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 3, "OBIFP_ELSE");
  /*_.OELSE__V10*/ meltfptr[9] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-genobj.melt:4528:/ quasiblock");


    /*^checksignal */
    MELT_CHECK_SIGNAL ();
    ;
    /*^msend */
    /*msend */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^ojbmsend.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.DESTO__V3*/ meltfptr[2];
      /*_.DTHEN__V12*/ meltfptr[11] =
	meltgc_send ((melt_ptr_t) ( /*_.OTHEN__V9*/ meltfptr[8]),
		     (melt_ptr_t) (( /*!PUT_OBJDEST */ meltfrout->tabval[2])),
		     (MELTBPARSTR_PTR ""), argtab, "",
		     (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-genobj.melt:4529:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^msend */
    /*msend */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^ojbmsend.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.DESTO__V3*/ meltfptr[2];
      /*_.DELSE__V13*/ meltfptr[12] =
	meltgc_send ((melt_ptr_t) ( /*_.OELSE__V10*/ meltfptr[9]),
		     (melt_ptr_t) (( /*!PUT_OBJDEST */ meltfrout->tabval[2])),
		     (MELTBPARSTR_PTR ""), argtab, "",
		     (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-genobj.melt:4530:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OBIFP_THEN",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.RECV__V2*/ meltfptr[1])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.RECV__V2*/ meltfptr[1]), (2),
			  ( /*_.DTHEN__V12*/ meltfptr[11]), "OBIFP_THEN");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OBIFP_ELSE",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.RECV__V2*/ meltfptr[1])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.RECV__V2*/ meltfptr[1]), (3),
			  ( /*_.DELSE__V13*/ meltfptr[12]), "OBIFP_ELSE");
    ;
    /*^touch */
    meltgc_touch ( /*_.RECV__V2*/ meltfptr[1]);
    ;
    /*^touchobj */

    melt_dbgtrace_written_object ( /*_.RECV__V2*/ meltfptr[1], "put-fields");
    ;

    /*_.LET___V11*/ meltfptr[10] = /*_.RECV__V2*/ meltfptr[1];;

    MELT_LOCATION ("warmelt-genobj.melt:4528:/ clear");
	   /*clear *//*_.DTHEN__V12*/ meltfptr[11] = 0;
    /*^clear */
	   /*clear *//*_.DELSE__V13*/ meltfptr[12] = 0;
    /*_.LET___V8*/ meltfptr[6] = /*_.LET___V11*/ meltfptr[10];;

    MELT_LOCATION ("warmelt-genobj.melt:4525:/ clear");
	   /*clear *//*_.OTHEN__V9*/ meltfptr[8] = 0;
    /*^clear */
	   /*clear *//*_.OELSE__V10*/ meltfptr[9] = 0;
    /*^clear */
	   /*clear *//*_.LET___V11*/ meltfptr[10] = 0;
    MELT_LOCATION ("warmelt-genobj.melt:4522:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.LET___V8*/ meltfptr[6];;

    {
      MELT_LOCATION ("warmelt-genobj.melt:4522:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V4*/ meltfptr[3] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V6*/ meltfptr[4] = 0;
    /*^clear */
	   /*clear *//*_.LET___V8*/ meltfptr[6] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("PUTOBJDEST_OBJCPPIF", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_130_warmelt_genobj_PUTOBJDEST_OBJCPPIF_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_130_warmelt_genobj_PUTOBJDEST_OBJCPPIF */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_131_warmelt_genobj_COMPILOBJ_NREP_RETURN (meltclosure_ptr_t
						   meltclosp_,
						   melt_ptr_t meltfirstargp_,
						   const melt_argdescr_cell_t
						   meltxargdescr_[],
						   union meltparam_un *
						   meltxargtab_,
						   const melt_argdescr_cell_t
						   meltxresdescr_[],
						   union meltparam_un *
						   meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_131_warmelt_genobj_COMPILOBJ_NREP_RETURN_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_131_warmelt_genobj_COMPILOBJ_NREP_RETURN_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 58
    melt_ptr_t mcfr_varptr[58];
#define MELTFRAM_NBVARNUM 25
    long mcfr_varnum[25];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_131_warmelt_genobj_COMPILOBJ_NREP_RETURN is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_131_warmelt_genobj_COMPILOBJ_NREP_RETURN_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 58; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_131_warmelt_genobj_COMPILOBJ_NREP_RETURN nbval 58*/
  meltfram__.mcfr_nbvar = 58 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("COMPILOBJ_NREP_RETURN", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-genobj.melt:4539:/ getarg");
 /*_.NRET__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.GCX__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.GCX__V3*/ meltfptr[2])) != NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:4540:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L1*/ meltfnum[0] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.NRET__V2*/ meltfptr[1]),
			     (melt_ptr_t) (( /*!CLASS_NREP_RETURN */
					    meltfrout->tabval[0])));;
      MELT_LOCATION ("warmelt-genobj.melt:4540:/ cond");
      /*cond */ if ( /*_#IS_A__L1*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V5*/ meltfptr[4] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-genobj.melt:4540:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check nret"),
				  ("warmelt-genobj.melt")
				  ? ("warmelt-genobj.melt") : __FILE__,
				  (4540) ? (4540) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V5*/ meltfptr[4] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V4*/ meltfptr[3] = /*_.IFELSE___V5*/ meltfptr[4];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:4540:/ clear");
	     /*clear *//*_#IS_A__L1*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V5*/ meltfptr[4] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V4*/ meltfptr[3] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:4541:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L2*/ meltfnum[0] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.GCX__V3*/ meltfptr[2]),
			     (melt_ptr_t) (( /*!CLASS_C_GENERATION_CONTEXT */
					    meltfrout->tabval[1])));;
      MELT_LOCATION ("warmelt-genobj.melt:4541:/ cond");
      /*cond */ if ( /*_#IS_A__L2*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V7*/ meltfptr[6] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-genobj.melt:4541:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check gcx"),
				  ("warmelt-genobj.melt")
				  ? ("warmelt-genobj.melt") : __FILE__,
				  (4541) ? (4541) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V7*/ meltfptr[6] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V6*/ meltfptr[4] = /*_.IFELSE___V7*/ meltfptr[6];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:4541:/ clear");
	     /*clear *//*_#IS_A__L2*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V7*/ meltfptr[6] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V6*/ meltfptr[4] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:4542:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L3*/ meltfnum[0] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-genobj.melt:4542:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L3*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L4*/ meltfnum[3] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-genobj.melt:4542:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[7];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L4*/ meltfnum[3];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-genobj.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 4542;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "compilobj_nrep_return nret=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.NRET__V2*/ meltfptr[1];
	      /*^apply.arg */
	      argtab[5].meltbp_cstring = "\n gcx=";
	      /*^apply.arg */
	      argtab[6].meltbp_aptr =
		(melt_ptr_t *) & /*_.GCX__V3*/ meltfptr[2];
	      /*_.MELT_DEBUG_FUN__V10*/ meltfptr[9] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[2])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V9*/ meltfptr[8] =
	      /*_.MELT_DEBUG_FUN__V10*/ meltfptr[9];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-genobj.melt:4542:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L4*/ meltfnum[3] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V10*/ meltfptr[9] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V9*/ meltfptr[8] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-genobj.melt:4542:/ quasiblock");


      /*_.PROGN___V11*/ meltfptr[9] = /*_.IF___V9*/ meltfptr[8];;
      /*^compute */
      /*_.IFCPP___V8*/ meltfptr[6] = /*_.PROGN___V11*/ meltfptr[9];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:4542:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L3*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IF___V9*/ meltfptr[8] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V11*/ meltfptr[9] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V8*/ meltfptr[6] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-genobj.melt:4543:/ quasiblock");


    /*^getslot */
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.NRET__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 0, "NREP_LOC");
  /*_.RLOC__V13*/ meltfptr[9] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-genobj.melt:4544:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.NRET__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 1, "NRET_MAIN");
  /*_.RMAIN__V14*/ meltfptr[13] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-genobj.melt:4545:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.NRET__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 2, "NRET_REST");
  /*_.RREST__V15*/ meltfptr[14] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-genobj.melt:4546:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.GCX__V3*/ meltfptr[2]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 5, "GNCX_RETLOC");
  /*_.RETLOC__V16*/ meltfptr[15] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-genobj.melt:4547:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.GCX__V3*/ meltfptr[2]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 0, "GNCX_OBJROUT");
  /*_.OROUT__V17*/ meltfptr[16] = slot;
    };
    ;
 /*_.OLIS__V18*/ meltfptr[17] =
      (meltgc_new_list
       ((meltobject_ptr_t) (( /*!DISCR_LIST */ meltfrout->tabval[3]))));;
    MELT_LOCATION ("warmelt-genobj.melt:4549:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*^rawallocobj */
    /*rawallocobj */
    {
      melt_ptr_t newobj = 0;
      melt_raw_object_create (newobj,
			      (melt_ptr_t) (( /*!CLASS_OBJPLAINBLOCK */
					     meltfrout->tabval[4])), (3),
			      "CLASS_OBJPLAINBLOCK");
  /*_.INST__V20*/ meltfptr[19] =
	newobj;
    };
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OBI_LOC",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V20*/ meltfptr[19])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V20*/ meltfptr[19]), (0),
			  ( /*_.RLOC__V13*/ meltfptr[9]), "OBI_LOC");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OBLO_BODYL",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V20*/ meltfptr[19])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V20*/ meltfptr[19]), (1),
			  ( /*_.OLIS__V18*/ meltfptr[17]), "OBLO_BODYL");
    ;
    /*^touchobj */

    melt_dbgtrace_written_object ( /*_.INST__V20*/ meltfptr[19],
				  "newly made instance");
    ;
    /*_.OBLOCK__V19*/ meltfptr[18] = /*_.INST__V20*/ meltfptr[19];;
    MELT_LOCATION ("warmelt-genobj.melt:4552:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^msend */
    /*msend */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^ojbmsend.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.GCX__V3*/ meltfptr[2];
      /*_.OMAINV__V21*/ meltfptr[20] =
	meltgc_send ((melt_ptr_t) ( /*_.RMAIN__V14*/ meltfptr[13]),
		     (melt_ptr_t) (( /*!COMPILE_OBJ */ meltfrout->tabval[5])),
		     (MELTBPARSTR_PTR ""), argtab, "",
		     (union meltparam_un *) 0);
    }
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:4554:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L5*/ meltfnum[3] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.OROUT__V17*/ meltfptr[16]),
			     (melt_ptr_t) (( /*!CLASS_ROUTINEOBJ */
					    meltfrout->tabval[6])));;
      MELT_LOCATION ("warmelt-genobj.melt:4554:/ cond");
      /*cond */ if ( /*_#IS_A__L5*/ meltfnum[3])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V23*/ meltfptr[22] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-genobj.melt:4554:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check orout"),
				  ("warmelt-genobj.melt")
				  ? ("warmelt-genobj.melt") : __FILE__,
				  (4554) ? (4554) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V23*/ meltfptr[22] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V22*/ meltfptr[21] = /*_.IFELSE___V23*/ meltfptr[22];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:4554:/ clear");
	     /*clear *//*_#IS_A__L5*/ meltfnum[3] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V23*/ meltfptr[22] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V22*/ meltfptr[21] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:4555:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_MULTIPLE_OR_NULL__L6*/ meltfnum[0] =
	(( /*_.RREST__V15*/ meltfptr[14]) == NULL
	 ||
	 (melt_unsafe_magic_discr
	  ((melt_ptr_t) ( /*_.RREST__V15*/ meltfptr[14])) ==
	  MELTOBMAG_MULTIPLE));;
      MELT_LOCATION ("warmelt-genobj.melt:4555:/ cond");
      /*cond */ if ( /*_#IS_MULTIPLE_OR_NULL__L6*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V25*/ meltfptr[24] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-genobj.melt:4555:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check rrest"),
				  ("warmelt-genobj.melt")
				  ? ("warmelt-genobj.melt") : __FILE__,
				  (4555) ? (4555) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V25*/ meltfptr[24] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V24*/ meltfptr[22] = /*_.IFELSE___V25*/ meltfptr[24];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:4555:/ clear");
	     /*clear *//*_#IS_MULTIPLE_OR_NULL__L6*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V25*/ meltfptr[24] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V24*/ meltfptr[22] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-genobj.melt:4556:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^cond */
    /*cond */ if (
		   /*ifisa */
		   melt_is_instance_of ((melt_ptr_t)
					( /*_.OROUT__V17*/ meltfptr[16]),
					(melt_ptr_t) (( /*!CLASS_ROUTINEOBJ */
						       meltfrout->tabval[6])))
      )				/*then */
      {
	/*^cond.then */
	/*^getslot */
	{
	  melt_ptr_t slot = NULL, obj = NULL;
	  obj = (melt_ptr_t) ( /*_.OROUT__V17*/ meltfptr[16]) /*=obj*/ ;
	  melt_object_get_field (slot, obj, 7, "OBROUT_RETVAL");
   /*_.OBROUT_RETVAL__V26*/ meltfptr[24] = slot;
	};
	;
      }
    else
      {				/*^cond.else */

  /*_.OBROUT_RETVAL__V26*/ meltfptr[24] = NULL;;
      }
    ;
    /*^compute */
 /*_#NULL__L7*/ meltfnum[3] =
      (( /*_.OBROUT_RETVAL__V26*/ meltfptr[24]) == NULL);;
    MELT_LOCATION ("warmelt-genobj.melt:4556:/ cond");
    /*cond */ if ( /*_#NULL__L7*/ meltfnum[3])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-genobj.melt:4557:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^cond */
	  /*cond */ if (
			 /*ifisa */
			 melt_is_instance_of ((melt_ptr_t)
					      ( /*_.OROUT__V17*/
					       meltfptr[16]),
					      (melt_ptr_t) (( /*!CLASS_ROUTINEOBJ */ meltfrout->tabval[6])))
	    )			/*then */
	    {
	      /*^cond.then */
	      /*^block */
	      /*anyblock */
	      {

		/*^putslot */
		/*putslot */
		melt_assertmsg ("putslot checkobj @OBROUT_RETVAL",
				melt_magic_discr ((melt_ptr_t)
						  ( /*_.OROUT__V17*/
						   meltfptr[16])) ==
				MELTOBMAG_OBJECT);
		melt_putfield_object (( /*_.OROUT__V17*/ meltfptr[16]), (7),
				      ( /*_.RETLOC__V16*/ meltfptr[15]),
				      "OBROUT_RETVAL");
		;
		/*^touch */
		meltgc_touch ( /*_.OROUT__V17*/ meltfptr[16]);
		;
		/*^touchobj */

		melt_dbgtrace_written_object ( /*_.OROUT__V17*/ meltfptr[16],
					      "put-fields");
		;
		/*epilog */
	      }
	      ;
	    }			/*noelse */
	  ;
	  /*epilog */
	}
	;
      }				/*noelse */
    ;
    MELT_LOCATION ("warmelt-genobj.melt:4558:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^msend */
    /*msend */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^ojbmsend.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.RETLOC__V16*/ meltfptr[15];
      /*_.PUT_OBJDEST__V27*/ meltfptr[26] =
	meltgc_send ((melt_ptr_t) ( /*_.OMAINV__V21*/ meltfptr[20]),
		     (melt_ptr_t) (( /*!PUT_OBJDEST */ meltfrout->tabval[7])),
		     (MELTBPARSTR_PTR ""), argtab, "",
		     (union meltparam_un *) 0);
    }
    ;

    {
      /*^locexp */
      meltgc_append_list ((melt_ptr_t) ( /*_.OLIS__V18*/ meltfptr[17]),
			  (melt_ptr_t) ( /*_.PUT_OBJDEST__V27*/
					meltfptr[26]));
    }
    ;
    MELT_LOCATION ("warmelt-genobj.melt:4559:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^checksignal */
    MELT_CHECK_SIGNAL ();
    ;
 /*_#IS_A__L8*/ meltfnum[0] =
      melt_is_instance_of ((melt_ptr_t) ( /*_.OROUT__V17*/ meltfptr[16]),
			   (melt_ptr_t) (( /*!CLASS_INITIALROUTINEOBJ */
					  meltfrout->tabval[8])));;
    MELT_LOCATION ("warmelt-genobj.melt:4559:/ cond");
    /*cond */ if ( /*_#IS_A__L8*/ meltfnum[0])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

   /*_#MULTIPLE_LENGTH__L10*/ meltfnum[9] =
	    (melt_multiple_length
	     ((melt_ptr_t) ( /*_.RREST__V15*/ meltfptr[14])));;
	  /*^compute */
   /*_#I__L11*/ meltfnum[10] =
	    (( /*_#MULTIPLE_LENGTH__L10*/ meltfnum[9]) > (0));;
	  /*^compute */
	  /*_#IF___L9*/ meltfnum[8] = /*_#I__L11*/ meltfnum[10];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-genobj.melt:4559:/ clear");
	     /*clear *//*_#MULTIPLE_LENGTH__L10*/ meltfnum[9] = 0;
	  /*^clear */
	     /*clear *//*_#I__L11*/ meltfnum[10] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

  /*_#IF___L9*/ meltfnum[8] = 0;;
      }
    ;
    MELT_LOCATION ("warmelt-genobj.melt:4559:/ cond");
    /*cond */ if ( /*_#IF___L9*/ meltfnum[8])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{


#if MELT_HAVE_DEBUG
	  MELT_LOCATION ("warmelt-genobj.melt:4561:/ cppif.then");
	  /*^block */
	  /*anyblock */
	  {


	    {
	      /*^locexp */
	      /*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	      melt_dbgcounter++;
#endif
	      ;
	    }
	    ;
	    /*^checksignal */
	    MELT_CHECK_SIGNAL ();
	    ;
     /*_#MELT_NEED_DBG__L12*/ meltfnum[9] =
	      /*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	      ( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	      0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	      ;;
	    MELT_LOCATION ("warmelt-genobj.melt:4561:/ cond");
	    /*cond */ if ( /*_#MELT_NEED_DBG__L12*/ meltfnum[9])	/*then */
	      {
		/*^cond.then */
		/*^block */
		/*anyblock */
		{

       /*_#THE_MELTCALLCOUNT__L13*/ meltfnum[10] =
#ifdef meltcallcount
		    meltcallcount	/* the_meltcallcount */
#else
		    0L
#endif /* meltcallcount the_meltcallcount */
		    ;;
		  MELT_LOCATION ("warmelt-genobj.melt:4561:/ checksignal");
		  MELT_CHECK_SIGNAL ();
		  ;
		  /*^apply */
		  /*apply */
		  {
		    union meltparam_un argtab[7];
		    memset (&argtab, 0, sizeof (argtab));
		    /*^apply.arg */
		    argtab[0].meltbp_long =
		      /*_#THE_MELTCALLCOUNT__L13*/ meltfnum[10];
		    /*^apply.arg */
		    argtab[1].meltbp_cstring = "warmelt-genobj.melt";
		    /*^apply.arg */
		    argtab[2].meltbp_long = 4561;
		    /*^apply.arg */
		    argtab[3].meltbp_cstring =
		      "compilobj_nrep_return bad initial orout=";
		    /*^apply.arg */
		    argtab[4].meltbp_aptr =
		      (melt_ptr_t *) & /*_.OROUT__V17*/ meltfptr[16];
		    /*^apply.arg */
		    argtab[5].meltbp_cstring =
		      " with secondary results rrest=";
		    /*^apply.arg */
		    argtab[6].meltbp_aptr =
		      (melt_ptr_t *) & /*_.RREST__V15*/ meltfptr[14];
		    /*_.MELT_DEBUG_FUN__V31*/ meltfptr[30] =
		      melt_apply ((meltclosure_ptr_t)
				  (( /*!MELT_DEBUG_FUN */ meltfrout->
				    tabval[2])),
				  (melt_ptr_t) (( /*nil */ NULL)),
				  (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_PTR MELTBPARSTR_CSTRING
				   MELTBPARSTR_PTR ""), argtab, "",
				  (union meltparam_un *) 0);
		  }
		  ;
		  /*_.IF___V30*/ meltfptr[29] =
		    /*_.MELT_DEBUG_FUN__V31*/ meltfptr[30];;
		  /*epilog */

		  MELT_LOCATION ("warmelt-genobj.melt:4561:/ clear");
		 /*clear *//*_#THE_MELTCALLCOUNT__L13*/ meltfnum[10] = 0;
		  /*^clear */
		 /*clear *//*_.MELT_DEBUG_FUN__V31*/ meltfptr[30] = 0;
		}
		;
	      }
	    else
	      {			/*^cond.else */

      /*_.IF___V30*/ meltfptr[29] = NULL;;
	      }
	    ;
	    MELT_LOCATION ("warmelt-genobj.melt:4561:/ quasiblock");


	    /*_.PROGN___V32*/ meltfptr[30] = /*_.IF___V30*/ meltfptr[29];;
	    /*^compute */
	    /*_.IFCPP___V29*/ meltfptr[28] = /*_.PROGN___V32*/ meltfptr[30];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-genobj.melt:4561:/ clear");
	       /*clear *//*_#MELT_NEED_DBG__L12*/ meltfnum[9] = 0;
	    /*^clear */
	       /*clear *//*_.IF___V30*/ meltfptr[29] = 0;
	    /*^clear */
	       /*clear *//*_.PROGN___V32*/ meltfptr[30] = 0;
	  }

#else /*MELT_HAVE_DEBUG */
	  /*^cppif.else */
	  /*_.IFCPP___V29*/ meltfptr[28] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
	  ;

	  {
	    MELT_LOCATION ("warmelt-genobj.melt:4562:/ locexp");
	    /* error_plain */
	      melt_error_str ((melt_ptr_t) ( /*_.RLOC__V13*/ meltfptr[9]),
			      ("(RETURN ...) with secondary results outside of function"),
			      (melt_ptr_t) 0);
	  }
	  ;
	  MELT_LOCATION ("warmelt-genobj.melt:4563:/ quasiblock");


   /*_.RETVAL___V1*/ meltfptr[0] = NULL;;

	  {
	    MELT_LOCATION ("warmelt-genobj.melt:4563:/ locexp");
	    /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
	    if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	      melt_warn_for_no_expected_secondary_results ();
	    /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
	    ;
	  }
	  ;
	  /*^finalreturn */
	  ;
	  /*finalret */ goto labend_rout;
	  MELT_LOCATION ("warmelt-genobj.melt:4559:/ quasiblock");


	  /*_.PROGN___V34*/ meltfptr[30] = /*_.RETURN___V33*/ meltfptr[29];;
	  /*^compute */
	  /*_.IF___V28*/ meltfptr[27] = /*_.PROGN___V34*/ meltfptr[30];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-genobj.melt:4559:/ clear");
	     /*clear *//*_.IFCPP___V29*/ meltfptr[28] = 0;
	  /*^clear */
	     /*clear *//*_.RETURN___V33*/ meltfptr[29] = 0;
	  /*^clear */
	     /*clear *//*_.PROGN___V34*/ meltfptr[30] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

  /*_.IF___V28*/ meltfptr[27] = NULL;;
      }
    ;
    /*citerblock FOREACH_IN_MULTIPLE */
    {
      /* start foreach_in_multiple meltcit1__EACHTUP */
      long meltcit1__EACHTUP_ln =
	melt_multiple_length ((melt_ptr_t) /*_.RREST__V15*/ meltfptr[14]);
      for ( /*_#IX__L14*/ meltfnum[10] = 0;
	   ( /*_#IX__L14*/ meltfnum[10] >= 0)
	   && ( /*_#IX__L14*/ meltfnum[10] < meltcit1__EACHTUP_ln);
	/*_#IX__L14*/ meltfnum[10]++)
	{
	  /*_.RXTRA__V35*/ meltfptr[28] =
	    melt_multiple_nth ((melt_ptr_t) ( /*_.RREST__V15*/ meltfptr[14]),
			       /*_#IX__L14*/ meltfnum[10]);




#if MELT_HAVE_DEBUG
	  MELT_LOCATION ("warmelt-genobj.melt:4568:/ cppif.then");
	  /*^block */
	  /*anyblock */
	  {


	    {
	      /*^locexp */
	      /*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	      melt_dbgcounter++;
#endif
	      ;
	    }
	    ;
	    /*^checksignal */
	    MELT_CHECK_SIGNAL ();
	    ;
    /*_#MELT_NEED_DBG__L15*/ meltfnum[9] =
	      /*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	      ( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	      0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	      ;;
	    MELT_LOCATION ("warmelt-genobj.melt:4568:/ cond");
	    /*cond */ if ( /*_#MELT_NEED_DBG__L15*/ meltfnum[9])	/*then */
	      {
		/*^cond.then */
		/*^block */
		/*anyblock */
		{

      /*_#THE_MELTCALLCOUNT__L16*/ meltfnum[15] =
#ifdef meltcallcount
		    meltcallcount	/* the_meltcallcount */
#else
		    0L
#endif /* meltcallcount the_meltcallcount */
		    ;;
		  MELT_LOCATION ("warmelt-genobj.melt:4568:/ checksignal");
		  MELT_CHECK_SIGNAL ();
		  ;
		  /*^apply */
		  /*apply */
		  {
		    union meltparam_un argtab[7];
		    memset (&argtab, 0, sizeof (argtab));
		    /*^apply.arg */
		    argtab[0].meltbp_long =
		      /*_#THE_MELTCALLCOUNT__L16*/ meltfnum[15];
		    /*^apply.arg */
		    argtab[1].meltbp_cstring = "warmelt-genobj.melt";
		    /*^apply.arg */
		    argtab[2].meltbp_long = 4568;
		    /*^apply.arg */
		    argtab[3].meltbp_cstring = "compilobj_nrep_return rxtra=";
		    /*^apply.arg */
		    argtab[4].meltbp_aptr =
		      (melt_ptr_t *) & /*_.RXTRA__V35*/ meltfptr[28];
		    /*^apply.arg */
		    argtab[5].meltbp_cstring = " ix=";
		    /*^apply.arg */
		    argtab[6].meltbp_long = /*_#IX__L14*/ meltfnum[10];
		    /*_.MELT_DEBUG_FUN__V38*/ meltfptr[37] =
		      melt_apply ((meltclosure_ptr_t)
				  (( /*!MELT_DEBUG_FUN */ meltfrout->
				    tabval[2])),
				  (melt_ptr_t) (( /*nil */ NULL)),
				  (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_PTR MELTBPARSTR_CSTRING
				   MELTBPARSTR_LONG ""), argtab, "",
				  (union meltparam_un *) 0);
		  }
		  ;
		  /*_.IF___V37*/ meltfptr[30] =
		    /*_.MELT_DEBUG_FUN__V38*/ meltfptr[37];;
		  /*epilog */

		  MELT_LOCATION ("warmelt-genobj.melt:4568:/ clear");
		/*clear *//*_#THE_MELTCALLCOUNT__L16*/ meltfnum[15] = 0;
		  /*^clear */
		/*clear *//*_.MELT_DEBUG_FUN__V38*/ meltfptr[37] = 0;
		}
		;
	      }
	    else
	      {			/*^cond.else */

     /*_.IF___V37*/ meltfptr[30] = NULL;;
	      }
	    ;
	    MELT_LOCATION ("warmelt-genobj.melt:4568:/ quasiblock");


	    /*_.PROGN___V39*/ meltfptr[37] = /*_.IF___V37*/ meltfptr[30];;
	    /*^compute */
	    /*_.IFCPP___V36*/ meltfptr[29] = /*_.PROGN___V39*/ meltfptr[37];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-genobj.melt:4568:/ clear");
	      /*clear *//*_#MELT_NEED_DBG__L15*/ meltfnum[9] = 0;
	    /*^clear */
	      /*clear *//*_.IF___V37*/ meltfptr[30] = 0;
	    /*^clear */
	      /*clear *//*_.PROGN___V39*/ meltfptr[37] = 0;
	  }

#else /*MELT_HAVE_DEBUG */
	  /*^cppif.else */
	  /*_.IFCPP___V36*/ meltfptr[29] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
	  ;
	  MELT_LOCATION ("warmelt-genobj.melt:4569:/ quasiblock");


	  MELT_LOCATION ("warmelt-genobj.melt:4570:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
  /*_.MAKE_INTEGERBOX__V40*/ meltfptr[30] =
	    (meltgc_new_int
	     ((meltobject_ptr_t)
	      (( /*!DISCR_INTEGER */ meltfrout->tabval[10])),
	      ( /*_#IX__L14*/ meltfnum[10])));;
	  MELT_LOCATION ("warmelt-genobj.melt:4573:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^msend */
	  /*msend */
	  {
	    union meltparam_un argtab[1];
	    memset (&argtab, 0, sizeof (argtab));
	    /*^ojbmsend.arg */
	    argtab[0].meltbp_aptr =
	      (melt_ptr_t *) & /*_.GCX__V3*/ meltfptr[2];
	    /*_.COMPILE_OBJ__V41*/ meltfptr[37] =
	      meltgc_send ((melt_ptr_t) ( /*_.RXTRA__V35*/ meltfptr[28]),
			   (melt_ptr_t) (( /*!COMPILE_OBJ */ meltfrout->
					  tabval[5])), (MELTBPARSTR_PTR ""),
			   argtab, "", (union meltparam_un *) 0);
	  }
	  ;
	  MELT_LOCATION ("warmelt-genobj.melt:4570:/ quasiblock");


	  /*^rawallocobj */
	  /*rawallocobj */
	  {
	    melt_ptr_t newobj = 0;
	    melt_raw_object_create (newobj,
				    (melt_ptr_t) (( /*!CLASS_OBJPUTXTRARESULT */ meltfrout->tabval[9])), (3), "CLASS_OBJPUTXTRARESULT");
   /*_.INST__V43*/ meltfptr[42] =
	      newobj;
	  };
	  ;
	  /*^putslot */
	  /*putslot */
	  melt_assertmsg ("putslot checkobj @OBI_LOC",
			  melt_magic_discr ((melt_ptr_t)
					    ( /*_.INST__V43*/ meltfptr[42]))
			  == MELTOBMAG_OBJECT);
	  melt_putfield_object (( /*_.INST__V43*/ meltfptr[42]), (0),
				( /*_.RLOC__V13*/ meltfptr[9]), "OBI_LOC");
	  ;
	  /*^putslot */
	  /*putslot */
	  melt_assertmsg ("putslot checkobj @OBXRES_RANK",
			  melt_magic_discr ((melt_ptr_t)
					    ( /*_.INST__V43*/ meltfptr[42]))
			  == MELTOBMAG_OBJECT);
	  melt_putfield_object (( /*_.INST__V43*/ meltfptr[42]), (1),
				( /*_.MAKE_INTEGERBOX__V40*/ meltfptr[30]),
				"OBXRES_RANK");
	  ;
	  /*^putslot */
	  /*putslot */
	  melt_assertmsg ("putslot checkobj @OBXRES_OBLOC",
			  melt_magic_discr ((melt_ptr_t)
					    ( /*_.INST__V43*/ meltfptr[42]))
			  == MELTOBMAG_OBJECT);
	  melt_putfield_object (( /*_.INST__V43*/ meltfptr[42]), (2),
				( /*_.COMPILE_OBJ__V41*/ meltfptr[37]),
				"OBXRES_OBLOC");
	  ;
	  /*^touchobj */

	  melt_dbgtrace_written_object ( /*_.INST__V43*/ meltfptr[42],
					"newly made instance");
	  ;
	  /*_.OXRES__V42*/ meltfptr[41] = /*_.INST__V43*/ meltfptr[42];;

#if MELT_HAVE_DEBUG
	  MELT_LOCATION ("warmelt-genobj.melt:4575:/ cppif.then");
	  /*^block */
	  /*anyblock */
	  {


	    {
	      /*^locexp */
	      /*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	      melt_dbgcounter++;
#endif
	      ;
	    }
	    ;
	    /*^checksignal */
	    MELT_CHECK_SIGNAL ();
	    ;
    /*_#MELT_NEED_DBG__L17*/ meltfnum[15] =
	      /*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	      ( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	      0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	      ;;
	    MELT_LOCATION ("warmelt-genobj.melt:4575:/ cond");
	    /*cond */ if ( /*_#MELT_NEED_DBG__L17*/ meltfnum[15])	/*then */
	      {
		/*^cond.then */
		/*^block */
		/*anyblock */
		{

      /*_#THE_MELTCALLCOUNT__L18*/ meltfnum[9] =
#ifdef meltcallcount
		    meltcallcount	/* the_meltcallcount */
#else
		    0L
#endif /* meltcallcount the_meltcallcount */
		    ;;
		  MELT_LOCATION ("warmelt-genobj.melt:4575:/ checksignal");
		  MELT_CHECK_SIGNAL ();
		  ;
		  /*^apply */
		  /*apply */
		  {
		    union meltparam_un argtab[5];
		    memset (&argtab, 0, sizeof (argtab));
		    /*^apply.arg */
		    argtab[0].meltbp_long =
		      /*_#THE_MELTCALLCOUNT__L18*/ meltfnum[9];
		    /*^apply.arg */
		    argtab[1].meltbp_cstring = "warmelt-genobj.melt";
		    /*^apply.arg */
		    argtab[2].meltbp_long = 4575;
		    /*^apply.arg */
		    argtab[3].meltbp_cstring = "compilobj_nrep_return oxres=";
		    /*^apply.arg */
		    argtab[4].meltbp_aptr =
		      (melt_ptr_t *) & /*_.OXRES__V42*/ meltfptr[41];
		    /*_.MELT_DEBUG_FUN__V46*/ meltfptr[45] =
		      melt_apply ((meltclosure_ptr_t)
				  (( /*!MELT_DEBUG_FUN */ meltfrout->
				    tabval[2])),
				  (melt_ptr_t) (( /*nil */ NULL)),
				  (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_PTR ""), argtab, "",
				  (union meltparam_un *) 0);
		  }
		  ;
		  /*_.IF___V45*/ meltfptr[44] =
		    /*_.MELT_DEBUG_FUN__V46*/ meltfptr[45];;
		  /*epilog */

		  MELT_LOCATION ("warmelt-genobj.melt:4575:/ clear");
		/*clear *//*_#THE_MELTCALLCOUNT__L18*/ meltfnum[9] = 0;
		  /*^clear */
		/*clear *//*_.MELT_DEBUG_FUN__V46*/ meltfptr[45] = 0;
		}
		;
	      }
	    else
	      {			/*^cond.else */

     /*_.IF___V45*/ meltfptr[44] = NULL;;
	      }
	    ;
	    MELT_LOCATION ("warmelt-genobj.melt:4575:/ quasiblock");


	    /*_.PROGN___V47*/ meltfptr[45] = /*_.IF___V45*/ meltfptr[44];;
	    /*^compute */
	    /*_.IFCPP___V44*/ meltfptr[43] = /*_.PROGN___V47*/ meltfptr[45];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-genobj.melt:4575:/ clear");
	      /*clear *//*_#MELT_NEED_DBG__L17*/ meltfnum[15] = 0;
	    /*^clear */
	      /*clear *//*_.IF___V45*/ meltfptr[44] = 0;
	    /*^clear */
	      /*clear *//*_.PROGN___V47*/ meltfptr[45] = 0;
	  }

#else /*MELT_HAVE_DEBUG */
	  /*^cppif.else */
	  /*_.IFCPP___V44*/ meltfptr[43] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
	  ;

	  {
	    MELT_LOCATION ("warmelt-genobj.melt:4577:/ locexp");
	    meltgc_append_list ((melt_ptr_t) ( /*_.OLIS__V18*/ meltfptr[17]),
				(melt_ptr_t) ( /*_.OXRES__V42*/
					      meltfptr[41]));
	  }
	  ;

	  MELT_LOCATION ("warmelt-genobj.melt:4569:/ clear");
	    /*clear *//*_.MAKE_INTEGERBOX__V40*/ meltfptr[30] = 0;
	  /*^clear */
	    /*clear *//*_.COMPILE_OBJ__V41*/ meltfptr[37] = 0;
	  /*^clear */
	    /*clear *//*_.OXRES__V42*/ meltfptr[41] = 0;
	  /*^clear */
	    /*clear *//*_.IFCPP___V44*/ meltfptr[43] = 0;
	  if ( /*_#IX__L14*/ meltfnum[10] < 0)
	    break;
	}			/* end  foreach_in_multiple meltcit1__EACHTUP */

      /*citerepilog */

      MELT_LOCATION ("warmelt-genobj.melt:4565:/ clear");
	    /*clear *//*_.RXTRA__V35*/ meltfptr[28] = 0;
      /*^clear */
	    /*clear *//*_#IX__L14*/ meltfnum[10] = 0;
      /*^clear */
	    /*clear *//*_.IFCPP___V36*/ meltfptr[29] = 0;
    }				/*endciterblock FOREACH_IN_MULTIPLE */
    ;
    MELT_LOCATION ("warmelt-genobj.melt:4580:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#IS_NOT_A__L19*/ meltfnum[9] =
      !melt_is_instance_of ((melt_ptr_t) ( /*_.OROUT__V17*/ meltfptr[16]),
			    (melt_ptr_t) (( /*!CLASS_INITIALROUTINEOBJ */
					   meltfrout->tabval[8])));;
    MELT_LOCATION ("warmelt-genobj.melt:4580:/ cond");
    /*cond */ if ( /*_#IS_NOT_A__L19*/ meltfnum[9])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-genobj.melt:4581:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
   /*_#MULTIPLE_LENGTH__L20*/ meltfnum[15] =
	    (melt_multiple_length
	     ((melt_ptr_t) ( /*_.RREST__V15*/ meltfptr[14])));;
	  /*^compute */
   /*_#I__L21*/ meltfnum[20] =
	    (( /*_#MULTIPLE_LENGTH__L20*/ meltfnum[15]) == (0));;
	  MELT_LOCATION ("warmelt-genobj.melt:4581:/ cond");
	  /*cond */ if ( /*_#I__L21*/ meltfnum[20])	/*then */
	    {
	      /*^cond.then */
	      /*^block */
	      /*anyblock */
	      {

		MELT_LOCATION ("warmelt-genobj.melt:4582:/ quasiblock");


		MELT_LOCATION ("warmelt-genobj.melt:4583:/ checksignal");
		MELT_CHECK_SIGNAL ();
		;
		/*^apply */
		/*apply */
		{
		  union meltparam_un argtab[2];
		  memset (&argtab, 0, sizeof (argtab));
		  /*^apply.arg */
		  argtab[0].meltbp_aptr =
		    (melt_ptr_t *) & ( /*!CTYPE_VOID */ meltfrout->
				      tabval[12]);
		  /*^apply.arg */
		  argtab[1].meltbp_cstring =
		    " /*ochecknores compilobj_nrep_return*/\
\n\t\t\t\t\t#if MELT_HAVE_DEBUG\n\t\t\t\t\tif (meltxresdescr_ && meltxresdesc\
r_[0] && meltxrestab_)  melt_warn_for_no_expected_secondary_results\
()\t;\n\t\t\t\t\t/* we warned when secondary results are expected but\
 not returned. */\n\t\t\t\t\t#endif /*MELT_HAVE_DEBUG*/\n\t\t\t\t\t";
		  /*_.OCHECKNORES__V48*/ meltfptr[44] =
		    melt_apply ((meltclosure_ptr_t)
				(( /*!MAKE_OBJLOCATEDEXP */ meltfrout->
				  tabval[11])),
				(melt_ptr_t) ( /*_.RLOC__V13*/ meltfptr[9]),
				(MELTBPARSTR_PTR MELTBPARSTR_CSTRING ""),
				argtab, "", (union meltparam_un *) 0);
		}
		;

#if MELT_HAVE_DEBUG
		MELT_LOCATION ("warmelt-genobj.melt:4592:/ cppif.then");
		/*^block */
		/*anyblock */
		{


		  {
		    /*^locexp */
		    /*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
		    melt_dbgcounter++;
#endif
		    ;
		  }
		  ;
		  /*^checksignal */
		  MELT_CHECK_SIGNAL ();
		  ;
       /*_#MELT_NEED_DBG__L22*/ meltfnum[21] =
		    /*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
		    ( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
		    0		/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
		    ;;
		  MELT_LOCATION ("warmelt-genobj.melt:4592:/ cond");
		  /*cond */ if ( /*_#MELT_NEED_DBG__L22*/ meltfnum[21])	/*then */
		    {
		      /*^cond.then */
		      /*^block */
		      /*anyblock */
		      {

	 /*_#THE_MELTCALLCOUNT__L23*/ meltfnum[22] =
#ifdef meltcallcount
			  meltcallcount	/* the_meltcallcount */
#else
			  0L
#endif /* meltcallcount the_meltcallcount */
			  ;;
			MELT_LOCATION
			  ("warmelt-genobj.melt:4592:/ checksignal");
			MELT_CHECK_SIGNAL ();
			;
			/*^apply */
			/*apply */
			{
			  union meltparam_un argtab[5];
			  memset (&argtab, 0, sizeof (argtab));
			  /*^apply.arg */
			  argtab[0].meltbp_long =
			    /*_#THE_MELTCALLCOUNT__L23*/ meltfnum[22];
			  /*^apply.arg */
			  argtab[1].meltbp_cstring = "warmelt-genobj.melt";
			  /*^apply.arg */
			  argtab[2].meltbp_long = 4592;
			  /*^apply.arg */
			  argtab[3].meltbp_cstring =
			    "compilobj_nrep_return ochecknores=";
			  /*^apply.arg */
			  argtab[4].meltbp_aptr =
			    (melt_ptr_t *) & /*_.OCHECKNORES__V48*/
			    meltfptr[44];
			  /*_.MELT_DEBUG_FUN__V51*/ meltfptr[37] =
			    melt_apply ((meltclosure_ptr_t)
					(( /*!MELT_DEBUG_FUN */ meltfrout->
					  tabval[2])),
					(melt_ptr_t) (( /*nil */ NULL)),
					(MELTBPARSTR_LONG MELTBPARSTR_CSTRING
					 MELTBPARSTR_LONG MELTBPARSTR_CSTRING
					 MELTBPARSTR_PTR ""), argtab, "",
					(union meltparam_un *) 0);
			}
			;
			/*_.IF___V50*/ meltfptr[30] =
			  /*_.MELT_DEBUG_FUN__V51*/ meltfptr[37];;
			/*epilog */

			MELT_LOCATION ("warmelt-genobj.melt:4592:/ clear");
		   /*clear *//*_#THE_MELTCALLCOUNT__L23*/ meltfnum[22] =
			  0;
			/*^clear */
		   /*clear *//*_.MELT_DEBUG_FUN__V51*/ meltfptr[37] = 0;
		      }
		      ;
		    }
		  else
		    {		/*^cond.else */

	/*_.IF___V50*/ meltfptr[30] = NULL;;
		    }
		  ;
		  MELT_LOCATION ("warmelt-genobj.melt:4592:/ quasiblock");


		  /*_.PROGN___V52*/ meltfptr[41] =
		    /*_.IF___V50*/ meltfptr[30];;
		  /*^compute */
		  /*_.IFCPP___V49*/ meltfptr[45] =
		    /*_.PROGN___V52*/ meltfptr[41];;
		  /*epilog */

		  MELT_LOCATION ("warmelt-genobj.melt:4592:/ clear");
		 /*clear *//*_#MELT_NEED_DBG__L22*/ meltfnum[21] = 0;
		  /*^clear */
		 /*clear *//*_.IF___V50*/ meltfptr[30] = 0;
		  /*^clear */
		 /*clear *//*_.PROGN___V52*/ meltfptr[41] = 0;
		}

#else /*MELT_HAVE_DEBUG */
		/*^cppif.else */
		/*_.IFCPP___V49*/ meltfptr[45] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
		;

		{
		  MELT_LOCATION ("warmelt-genobj.melt:4593:/ locexp");
		  meltgc_append_list ((melt_ptr_t)
				      ( /*_.OLIS__V18*/ meltfptr[17]),
				      (melt_ptr_t) ( /*_.OCHECKNORES__V48*/
						    meltfptr[44]));
		}
		;

		MELT_LOCATION ("warmelt-genobj.melt:4582:/ clear");
	       /*clear *//*_.OCHECKNORES__V48*/ meltfptr[44] = 0;
		/*^clear */
	       /*clear *//*_.IFCPP___V49*/ meltfptr[45] = 0;
		/*epilog */
	      }
	      ;
	    }			/*noelse */
	  ;
	  /*epilog */

	  MELT_LOCATION ("warmelt-genobj.melt:4580:/ clear");
	     /*clear *//*_#MULTIPLE_LENGTH__L20*/ meltfnum[15] = 0;
	  /*^clear */
	     /*clear *//*_#I__L21*/ meltfnum[20] = 0;
	}
	;
      }				/*noelse */
    ;
    MELT_LOCATION ("warmelt-genobj.melt:4596:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*^rawallocobj */
    /*rawallocobj */
    {
      melt_ptr_t newobj = 0;
      melt_raw_object_create (newobj,
			      (melt_ptr_t) (( /*!CLASS_OBJFINALRETURN */
					     meltfrout->tabval[13])), (1),
			      "CLASS_OBJFINALRETURN");
  /*_.INST__V54*/ meltfptr[37] =
	newobj;
    };
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OBI_LOC",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V54*/ meltfptr[37])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V54*/ meltfptr[37]), (0),
			  ( /*_.RLOC__V13*/ meltfptr[9]), "OBI_LOC");
    ;
    /*^touchobj */

    melt_dbgtrace_written_object ( /*_.INST__V54*/ meltfptr[37],
				  "newly made instance");
    ;
    /*_.INST___V53*/ meltfptr[43] = /*_.INST__V54*/ meltfptr[37];;

    {
      MELT_LOCATION ("warmelt-genobj.melt:4595:/ locexp");
      meltgc_append_list ((melt_ptr_t) ( /*_.OLIS__V18*/ meltfptr[17]),
			  (melt_ptr_t) ( /*_.INST___V53*/ meltfptr[43]));
    }
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:4598:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L24*/ meltfnum[22] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-genobj.melt:4598:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L24*/ meltfnum[22])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L25*/ meltfnum[21] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-genobj.melt:4598:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long =
		/*_#THE_MELTCALLCOUNT__L25*/ meltfnum[21];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-genobj.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 4598;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring =
		"compilobj_nrep_return final oblock=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.OBLOCK__V19*/ meltfptr[18];
	      /*_.MELT_DEBUG_FUN__V57*/ meltfptr[44] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[2])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V56*/ meltfptr[41] =
	      /*_.MELT_DEBUG_FUN__V57*/ meltfptr[44];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-genobj.melt:4598:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L25*/ meltfnum[21] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V57*/ meltfptr[44] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V56*/ meltfptr[41] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-genobj.melt:4598:/ quasiblock");


      /*_.PROGN___V58*/ meltfptr[45] = /*_.IF___V56*/ meltfptr[41];;
      /*^compute */
      /*_.IFCPP___V55*/ meltfptr[30] = /*_.PROGN___V58*/ meltfptr[45];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:4598:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L24*/ meltfnum[22] = 0;
      /*^clear */
	     /*clear *//*_.IF___V56*/ meltfptr[41] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V58*/ meltfptr[45] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V55*/ meltfptr[30] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    /*^compute */
    /*_.LET___V12*/ meltfptr[8] = /*_.OBLOCK__V19*/ meltfptr[18];;

    MELT_LOCATION ("warmelt-genobj.melt:4543:/ clear");
	   /*clear *//*_.RLOC__V13*/ meltfptr[9] = 0;
    /*^clear */
	   /*clear *//*_.RMAIN__V14*/ meltfptr[13] = 0;
    /*^clear */
	   /*clear *//*_.RREST__V15*/ meltfptr[14] = 0;
    /*^clear */
	   /*clear *//*_.RETLOC__V16*/ meltfptr[15] = 0;
    /*^clear */
	   /*clear *//*_.OROUT__V17*/ meltfptr[16] = 0;
    /*^clear */
	   /*clear *//*_.OLIS__V18*/ meltfptr[17] = 0;
    /*^clear */
	   /*clear *//*_.OBLOCK__V19*/ meltfptr[18] = 0;
    /*^clear */
	   /*clear *//*_.OMAINV__V21*/ meltfptr[20] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V22*/ meltfptr[21] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V24*/ meltfptr[22] = 0;
    /*^clear */
	   /*clear *//*_.OBROUT_RETVAL__V26*/ meltfptr[24] = 0;
    /*^clear */
	   /*clear *//*_#NULL__L7*/ meltfnum[3] = 0;
    /*^clear */
	   /*clear *//*_.PUT_OBJDEST__V27*/ meltfptr[26] = 0;
    /*^clear */
	   /*clear *//*_#IS_A__L8*/ meltfnum[0] = 0;
    /*^clear */
	   /*clear *//*_#IF___L9*/ meltfnum[8] = 0;
    /*^clear */
	   /*clear *//*_.IF___V28*/ meltfptr[27] = 0;
    /*^clear */
	   /*clear *//*_#IS_NOT_A__L19*/ meltfnum[9] = 0;
    /*^clear */
	   /*clear *//*_.INST___V53*/ meltfptr[43] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V55*/ meltfptr[30] = 0;
    MELT_LOCATION ("warmelt-genobj.melt:4539:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.LET___V12*/ meltfptr[8];;

    {
      MELT_LOCATION ("warmelt-genobj.melt:4539:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V4*/ meltfptr[3] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V6*/ meltfptr[4] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V8*/ meltfptr[6] = 0;
    /*^clear */
	   /*clear *//*_.LET___V12*/ meltfptr[8] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("COMPILOBJ_NREP_RETURN", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_131_warmelt_genobj_COMPILOBJ_NREP_RETURN_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_131_warmelt_genobj_COMPILOBJ_NREP_RETURN */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_132_warmelt_genobj_LAMBDA___42__ (meltclosure_ptr_t meltclosp_,
					   melt_ptr_t meltfirstargp_,
					   const melt_argdescr_cell_t
					   meltxargdescr_[],
					   union meltparam_un * meltxargtab_,
					   const melt_argdescr_cell_t
					   meltxresdescr_[],
					   union meltparam_un * meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_132_warmelt_genobj_LAMBDA___42___melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_132_warmelt_genobj_LAMBDA___42___st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 3
    melt_ptr_t mcfr_varptr[3];
/*no varnum*/
#define MELTFRAM_NBVARNUM /*none*/0
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_132_warmelt_genobj_LAMBDA___42__ is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_132_warmelt_genobj_LAMBDA___42___st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 3; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_132_warmelt_genobj_LAMBDA___42__ nbval 3*/
  meltfram__.mcfr_nbvar = 3 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("LAMBDA_", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-genobj.melt:4605:/ getarg");
 /*_.RECV__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.ENV__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.ENV__V3*/ meltfptr[2])) != NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {

    /*_.RETVAL___V1*/ meltfptr[0] =
      ( /*!CTYPE_VALUE */ meltfrout->tabval[0]);;

    {
      MELT_LOCATION ("warmelt-genobj.melt:4605:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("LAMBDA_", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_132_warmelt_genobj_LAMBDA___42___melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_132_warmelt_genobj_LAMBDA___42__ */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_133_warmelt_genobj_COMPILOBJ_NREP_LAMBDA (meltclosure_ptr_t
						   meltclosp_,
						   melt_ptr_t meltfirstargp_,
						   const melt_argdescr_cell_t
						   meltxargdescr_[],
						   union meltparam_un *
						   meltxargtab_,
						   const melt_argdescr_cell_t
						   meltxresdescr_[],
						   union meltparam_un *
						   meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_133_warmelt_genobj_COMPILOBJ_NREP_LAMBDA_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_133_warmelt_genobj_COMPILOBJ_NREP_LAMBDA_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 40
    melt_ptr_t mcfr_varptr[40];
#define MELTFRAM_NBVARNUM 8
    long mcfr_varnum[8];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_133_warmelt_genobj_COMPILOBJ_NREP_LAMBDA is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_133_warmelt_genobj_COMPILOBJ_NREP_LAMBDA_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 40; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_133_warmelt_genobj_COMPILOBJ_NREP_LAMBDA nbval 40*/
  meltfram__.mcfr_nbvar = 40 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("COMPILOBJ_NREP_LAMBDA", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-genobj.melt:4613:/ getarg");
 /*_.NLAM__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.GCX__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.GCX__V3*/ meltfptr[2])) != NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:4614:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L1*/ meltfnum[0] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.GCX__V3*/ meltfptr[2]),
			     (melt_ptr_t) (( /*!CLASS_C_GENERATION_CONTEXT */
					    meltfrout->tabval[0])));;
      MELT_LOCATION ("warmelt-genobj.melt:4614:/ cond");
      /*cond */ if ( /*_#IS_A__L1*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V5*/ meltfptr[4] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-genobj.melt:4614:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check gcx"),
				  ("warmelt-genobj.melt")
				  ? ("warmelt-genobj.melt") : __FILE__,
				  (4614) ? (4614) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V5*/ meltfptr[4] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V4*/ meltfptr[3] = /*_.IFELSE___V5*/ meltfptr[4];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:4614:/ clear");
	     /*clear *//*_#IS_A__L1*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V5*/ meltfptr[4] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V4*/ meltfptr[3] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:4615:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L2*/ meltfnum[0] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.NLAM__V2*/ meltfptr[1]),
			     (melt_ptr_t) (( /*!CLASS_NREP_LAMBDA */
					    meltfrout->tabval[1])));;
      MELT_LOCATION ("warmelt-genobj.melt:4615:/ cond");
      /*cond */ if ( /*_#IS_A__L2*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V7*/ meltfptr[6] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-genobj.melt:4615:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check nlam"),
				  ("warmelt-genobj.melt")
				  ? ("warmelt-genobj.melt") : __FILE__,
				  (4615) ? (4615) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V7*/ meltfptr[6] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V6*/ meltfptr[4] = /*_.IFELSE___V7*/ meltfptr[6];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:4615:/ clear");
	     /*clear *//*_#IS_A__L2*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V7*/ meltfptr[6] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V6*/ meltfptr[4] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:4616:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L3*/ meltfnum[0] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-genobj.melt:4616:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L3*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L4*/ meltfnum[3] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-genobj.melt:4616:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L4*/ meltfnum[3];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-genobj.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 4616;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "compilobj_nrep_lambda nlam=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.NLAM__V2*/ meltfptr[1];
	      /*_.MELT_DEBUG_FUN__V10*/ meltfptr[9] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[2])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V9*/ meltfptr[8] =
	      /*_.MELT_DEBUG_FUN__V10*/ meltfptr[9];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-genobj.melt:4616:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L4*/ meltfnum[3] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V10*/ meltfptr[9] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V9*/ meltfptr[8] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-genobj.melt:4616:/ quasiblock");


      /*_.PROGN___V11*/ meltfptr[9] = /*_.IF___V9*/ meltfptr[8];;
      /*^compute */
      /*_.IFCPP___V8*/ meltfptr[6] = /*_.PROGN___V11*/ meltfptr[9];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:4616:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L3*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IF___V9*/ meltfptr[8] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V11*/ meltfptr[9] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V8*/ meltfptr[6] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-genobj.melt:4617:/ quasiblock");


    /*^getslot */
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.NLAM__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 0, "NREP_LOC");
  /*_.NLOC__V13*/ meltfptr[9] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-genobj.melt:4618:/ quasiblock");


    MELT_LOCATION ("warmelt-genobj.melt:4619:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.NLAM__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 1, "NLAMBDA_PROC");
  /*_.CHECKPRO__V15*/ meltfptr[14] = slot;
    };
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:4620:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L5*/ meltfnum[3] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.CHECKPRO__V15*/ meltfptr[14]),
			     (melt_ptr_t) (( /*!CLASS_NREP_ROUTPROC */
					    meltfrout->tabval[3])));;
      MELT_LOCATION ("warmelt-genobj.melt:4620:/ cond");
      /*cond */ if ( /*_#IS_A__L5*/ meltfnum[3])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V17*/ meltfptr[16] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-genobj.melt:4620:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check checkpro"),
				  ("warmelt-genobj.melt")
				  ? ("warmelt-genobj.melt") : __FILE__,
				  (4620) ? (4620) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V17*/ meltfptr[16] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V16*/ meltfptr[15] = /*_.IFELSE___V17*/ meltfptr[16];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:4620:/ clear");
	     /*clear *//*_#IS_A__L5*/ meltfnum[3] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V17*/ meltfptr[16] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V16*/ meltfptr[15] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    /*^compute */
    /*_.LET___V14*/ meltfptr[13] = /*_.CHECKPRO__V15*/ meltfptr[14];;

    MELT_LOCATION ("warmelt-genobj.melt:4618:/ clear");
	   /*clear *//*_.CHECKPRO__V15*/ meltfptr[14] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V16*/ meltfptr[15] = 0;
    /*_.NPRO__V18*/ meltfptr[16] = /*_.LET___V14*/ meltfptr[13];;
    MELT_LOCATION ("warmelt-genobj.melt:4622:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.NPRO__V18*/ meltfptr[16]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 2, "NRPRO_NAME");
  /*_.NAM__V19*/ meltfptr[14] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-genobj.melt:4623:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.NLAM__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 3, "NLAMBDA_CLOSEDV");
  /*_.NCLOVTUP__V20*/ meltfptr[15] = slot;
    };
    ;
 /*_#NBCLOSED__L6*/ meltfnum[0] =
      (melt_multiple_length
       ((melt_ptr_t) ( /*_.NCLOVTUP__V20*/ meltfptr[15])));;
    MELT_LOCATION ("warmelt-genobj.melt:4625:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.NAM__V19*/ meltfptr[14];
      /*_.LOCV__V21*/ meltfptr[20] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!GET_FREE_OBJLOCPTR */ meltfrout->tabval[4])),
		    (melt_ptr_t) ( /*_.GCX__V3*/ meltfptr[2]),
		    (MELTBPARSTR_PTR ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-genobj.melt:4626:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.NLAM__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 2, "NLAMBDA_CONSTROUT");
  /*_.NROU__V22*/ meltfptr[21] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-genobj.melt:4627:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.GCX__V3*/ meltfptr[2]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 0, "GNCX_OBJROUT");
  /*_.OROUT__V23*/ meltfptr[22] = slot;
    };
    ;
 /*_.OLIS__V24*/ meltfptr[23] =
      (meltgc_new_list
       ((meltobject_ptr_t) (( /*!DISCR_LIST */ meltfrout->tabval[5]))));;
    MELT_LOCATION ("warmelt-genobj.melt:4629:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*^rawallocobj */
    /*rawallocobj */
    {
      melt_ptr_t newobj = 0;
      melt_raw_object_create (newobj,
			      (melt_ptr_t) (( /*!CLASS_OBJPLAINBLOCK */
					     meltfrout->tabval[6])), (3),
			      "CLASS_OBJPLAINBLOCK");
  /*_.INST__V26*/ meltfptr[25] =
	newobj;
    };
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OBI_LOC",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V26*/ meltfptr[25])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V26*/ meltfptr[25]), (0),
			  ( /*_.NLOC__V13*/ meltfptr[9]), "OBI_LOC");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OBLO_BODYL",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V26*/ meltfptr[25])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V26*/ meltfptr[25]), (1),
			  ( /*_.OLIS__V24*/ meltfptr[23]), "OBLO_BODYL");
    ;
    /*^touchobj */

    melt_dbgtrace_written_object ( /*_.INST__V26*/ meltfptr[25],
				  "newly made instance");
    ;
    /*_.OBLOCK__V25*/ meltfptr[24] = /*_.INST__V26*/ meltfptr[25];;
    /*^compute */
 /*_.DESTLIST__V27*/ meltfptr[26] =
      (meltgc_new_list
       ((meltobject_ptr_t) (( /*!DISCR_LIST */ meltfrout->tabval[5]))));;

    {
      MELT_LOCATION ("warmelt-genobj.melt:4634:/ locexp");
      meltgc_append_list ((melt_ptr_t) ( /*_.DESTLIST__V27*/ meltfptr[26]),
			  (melt_ptr_t) ( /*_.LOCV__V21*/ meltfptr[20]));
    }
    ;
    MELT_LOCATION ("warmelt-genobj.melt:4636:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    MELT_LOCATION ("warmelt-genobj.melt:4642:/ quasiblock");


    /*^checksignal */
    MELT_CHECK_SIGNAL ();
    ;
    /*^msend */
    /*msend */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^ojbmsend.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.GCX__V3*/ meltfptr[2];
      /*_.CROU__V29*/ meltfptr[28] =
	meltgc_send ((melt_ptr_t) ( /*_.NROU__V22*/ meltfptr[21]),
		     (melt_ptr_t) (( /*!COMPILE_OBJ */ meltfrout->tabval[9])),
		     (MELTBPARSTR_PTR ""), argtab, "",
		     (union meltparam_un *) 0);
    }
    ;
    /*_.LET___V28*/ meltfptr[27] = /*_.CROU__V29*/ meltfptr[28];;

    MELT_LOCATION ("warmelt-genobj.melt:4642:/ clear");
	   /*clear *//*_.CROU__V29*/ meltfptr[28] = 0;
    MELT_LOCATION ("warmelt-genobj.melt:4641:/ quasiblock");


    /*_.PROGN___V30*/ meltfptr[28] = /*_.LET___V28*/ meltfptr[27];;
    /*^compute */
 /*_.MAKE_INTEGERBOX__V31*/ meltfptr[30] =
      (meltgc_new_int
       ((meltobject_ptr_t) (( /*!DISCR_INTEGER */ meltfrout->tabval[10])),
	( /*_#NBCLOSED__L6*/ meltfnum[0])));;
    MELT_LOCATION ("warmelt-genobj.melt:4636:/ quasiblock");


    /*^rawallocobj */
    /*rawallocobj */
    {
      melt_ptr_t newobj = 0;
      melt_raw_object_create (newobj,
			      (melt_ptr_t) (( /*!CLASS_OBJNEWCLOSURE */
					     meltfrout->tabval[7])), (5),
			      "CLASS_OBJNEWCLOSURE");
  /*_.INST__V33*/ meltfptr[32] =
	newobj;
    };
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OBI_LOC",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V33*/ meltfptr[32])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V33*/ meltfptr[32]), (0),
			  ( /*_.NLOC__V13*/ meltfptr[9]), "OBI_LOC");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OBNCLO_DISCR",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V33*/ meltfptr[32])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V33*/ meltfptr[32]), (2),
			  (( /*!DISCRCLOSURE_OBJPREDEF */ meltfrout->
			    tabval[8])), "OBNCLO_DISCR");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OBNCLO_ROUT",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V33*/ meltfptr[32])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V33*/ meltfptr[32]), (3),
			  ( /*_.PROGN___V30*/ meltfptr[28]), "OBNCLO_ROUT");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OBNCLO_LEN",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V33*/ meltfptr[32])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V33*/ meltfptr[32]), (4),
			  ( /*_.MAKE_INTEGERBOX__V31*/ meltfptr[30]),
			  "OBNCLO_LEN");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OBDI_DESTLIST",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V33*/ meltfptr[32])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V33*/ meltfptr[32]), (1),
			  ( /*_.DESTLIST__V27*/ meltfptr[26]),
			  "OBDI_DESTLIST");
    ;
    /*^touchobj */

    melt_dbgtrace_written_object ( /*_.INST__V33*/ meltfptr[32],
				  "newly made instance");
    ;
    /*_.INST___V32*/ meltfptr[31] = /*_.INST__V33*/ meltfptr[32];;

    {
      MELT_LOCATION ("warmelt-genobj.melt:4635:/ locexp");
      meltgc_append_list ((melt_ptr_t) ( /*_.OLIS__V24*/ meltfptr[23]),
			  (melt_ptr_t) ( /*_.INST___V32*/ meltfptr[31]));
    }
    ;
    MELT_LOCATION ("warmelt-genobj.melt:4649:/ quasiblock");


    /*^newclosure */
		 /*newclosure *//*_.LAMBDA___V35*/ meltfptr[34] =
      (melt_ptr_t)
      meltgc_new_closure ((meltobject_ptr_t)
			  (((melt_ptr_t) (MELT_PREDEF (DISCR_CLOSURE)))),
			  (meltroutine_ptr_t) (( /*!konst_15 */ meltfrout->
						tabval[15])), (4));
    ;
    /*^putclosedv */
    /*putclosv */
    melt_assertmsg ("putclosv checkclo",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.LAMBDA___V35*/ meltfptr[34])) ==
		    MELTOBMAG_CLOSURE);
    melt_assertmsg ("putclosv checkoff", 0 >= 0
		    && 0 <
		    melt_closure_size ((melt_ptr_t)
				       ( /*_.LAMBDA___V35*/ meltfptr[34])));
    ((meltclosure_ptr_t) /*_.LAMBDA___V35*/ meltfptr[34])->tabval[0] =
      (melt_ptr_t) ( /*_.OLIS__V24*/ meltfptr[23]);
    ;
    /*^putclosedv */
    /*putclosv */
    melt_assertmsg ("putclosv checkclo",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.LAMBDA___V35*/ meltfptr[34])) ==
		    MELTOBMAG_CLOSURE);
    melt_assertmsg ("putclosv checkoff", 1 >= 0
		    && 1 <
		    melt_closure_size ((melt_ptr_t)
				       ( /*_.LAMBDA___V35*/ meltfptr[34])));
    ((meltclosure_ptr_t) /*_.LAMBDA___V35*/ meltfptr[34])->tabval[1] =
      (melt_ptr_t) ( /*_.NLOC__V13*/ meltfptr[9]);
    ;
    /*^putclosedv */
    /*putclosv */
    melt_assertmsg ("putclosv checkclo",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.LAMBDA___V35*/ meltfptr[34])) ==
		    MELTOBMAG_CLOSURE);
    melt_assertmsg ("putclosv checkoff", 2 >= 0
		    && 2 <
		    melt_closure_size ((melt_ptr_t)
				       ( /*_.LAMBDA___V35*/ meltfptr[34])));
    ((meltclosure_ptr_t) /*_.LAMBDA___V35*/ meltfptr[34])->tabval[2] =
      (melt_ptr_t) ( /*_.LOCV__V21*/ meltfptr[20]);
    ;
    /*^putclosedv */
    /*putclosv */
    melt_assertmsg ("putclosv checkclo",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.LAMBDA___V35*/ meltfptr[34])) ==
		    MELTOBMAG_CLOSURE);
    melt_assertmsg ("putclosv checkoff", 3 >= 0
		    && 3 <
		    melt_closure_size ((melt_ptr_t)
				       ( /*_.LAMBDA___V35*/ meltfptr[34])));
    ((meltclosure_ptr_t) /*_.LAMBDA___V35*/ meltfptr[34])->tabval[3] =
      (melt_ptr_t) ( /*_.GCX__V3*/ meltfptr[2]);
    ;
    /*_.LAMBDA___V34*/ meltfptr[33] = /*_.LAMBDA___V35*/ meltfptr[34];;
    MELT_LOCATION ("warmelt-genobj.melt:4647:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & /*_.LAMBDA___V34*/ meltfptr[33];
      /*_.MULTIPLE_EVERY__V36*/ meltfptr[35] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!MULTIPLE_EVERY */ meltfrout->tabval[11])),
		    (melt_ptr_t) ( /*_.NCLOVTUP__V20*/ meltfptr[15]),
		    (MELTBPARSTR_PTR ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-genobj.melt:4658:/ locexp");
      meltgc_append_list ((melt_ptr_t) ( /*_.OLIS__V24*/ meltfptr[23]),
			  (melt_ptr_t) ( /*_.LOCV__V21*/ meltfptr[20]));
    }
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:4659:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L7*/ meltfnum[3] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-genobj.melt:4659:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L7*/ meltfnum[3])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L8*/ meltfnum[7] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-genobj.melt:4659:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L8*/ meltfnum[7];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-genobj.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 4659;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring =
		"compilobj_nrep_lambda result oblock=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.OBLOCK__V25*/ meltfptr[24];
	      /*_.MELT_DEBUG_FUN__V39*/ meltfptr[38] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[2])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V38*/ meltfptr[37] =
	      /*_.MELT_DEBUG_FUN__V39*/ meltfptr[38];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-genobj.melt:4659:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L8*/ meltfnum[7] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V39*/ meltfptr[38] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V38*/ meltfptr[37] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-genobj.melt:4659:/ quasiblock");


      /*_.PROGN___V40*/ meltfptr[38] = /*_.IF___V38*/ meltfptr[37];;
      /*^compute */
      /*_.IFCPP___V37*/ meltfptr[36] = /*_.PROGN___V40*/ meltfptr[38];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:4659:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L7*/ meltfnum[3] = 0;
      /*^clear */
	     /*clear *//*_.IF___V38*/ meltfptr[37] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V40*/ meltfptr[38] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V37*/ meltfptr[36] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    /*^compute */
    /*_.LET___V12*/ meltfptr[8] = /*_.OBLOCK__V25*/ meltfptr[24];;

    MELT_LOCATION ("warmelt-genobj.melt:4617:/ clear");
	   /*clear *//*_.NLOC__V13*/ meltfptr[9] = 0;
    /*^clear */
	   /*clear *//*_.LET___V14*/ meltfptr[13] = 0;
    /*^clear */
	   /*clear *//*_.NPRO__V18*/ meltfptr[16] = 0;
    /*^clear */
	   /*clear *//*_.NAM__V19*/ meltfptr[14] = 0;
    /*^clear */
	   /*clear *//*_.NCLOVTUP__V20*/ meltfptr[15] = 0;
    /*^clear */
	   /*clear *//*_#NBCLOSED__L6*/ meltfnum[0] = 0;
    /*^clear */
	   /*clear *//*_.LOCV__V21*/ meltfptr[20] = 0;
    /*^clear */
	   /*clear *//*_.NROU__V22*/ meltfptr[21] = 0;
    /*^clear */
	   /*clear *//*_.OROUT__V23*/ meltfptr[22] = 0;
    /*^clear */
	   /*clear *//*_.OLIS__V24*/ meltfptr[23] = 0;
    /*^clear */
	   /*clear *//*_.OBLOCK__V25*/ meltfptr[24] = 0;
    /*^clear */
	   /*clear *//*_.DESTLIST__V27*/ meltfptr[26] = 0;
    /*^clear */
	   /*clear *//*_.LET___V28*/ meltfptr[27] = 0;
    /*^clear */
	   /*clear *//*_.PROGN___V30*/ meltfptr[28] = 0;
    /*^clear */
	   /*clear *//*_.MAKE_INTEGERBOX__V31*/ meltfptr[30] = 0;
    /*^clear */
	   /*clear *//*_.INST___V32*/ meltfptr[31] = 0;
    /*^clear */
	   /*clear *//*_.LAMBDA___V34*/ meltfptr[33] = 0;
    /*^clear */
	   /*clear *//*_.MULTIPLE_EVERY__V36*/ meltfptr[35] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V37*/ meltfptr[36] = 0;
    MELT_LOCATION ("warmelt-genobj.melt:4613:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.LET___V12*/ meltfptr[8];;

    {
      MELT_LOCATION ("warmelt-genobj.melt:4613:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V4*/ meltfptr[3] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V6*/ meltfptr[4] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V8*/ meltfptr[6] = 0;
    /*^clear */
	   /*clear *//*_.LET___V12*/ meltfptr[8] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("COMPILOBJ_NREP_LAMBDA", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_133_warmelt_genobj_COMPILOBJ_NREP_LAMBDA_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_133_warmelt_genobj_COMPILOBJ_NREP_LAMBDA */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_134_warmelt_genobj_LAMBDA___43__ (meltclosure_ptr_t meltclosp_,
					   melt_ptr_t meltfirstargp_,
					   const melt_argdescr_cell_t
					   meltxargdescr_[],
					   union meltparam_un * meltxargtab_,
					   const melt_argdescr_cell_t
					   meltxresdescr_[],
					   union meltparam_un * meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_134_warmelt_genobj_LAMBDA___43___melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_134_warmelt_genobj_LAMBDA___43___st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 6
    melt_ptr_t mcfr_varptr[6];
#define MELTFRAM_NBVARNUM 1
    long mcfr_varnum[1];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_134_warmelt_genobj_LAMBDA___43__ is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_134_warmelt_genobj_LAMBDA___43___st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 6; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_134_warmelt_genobj_LAMBDA___43__ nbval 6*/
  meltfram__.mcfr_nbvar = 6 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("LAMBDA_", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-genobj.melt:4649:/ getarg");
 /*_.CLOV__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_LONG)
    goto lab_endgetargs;
 /*_#IX__L1*/ meltfnum[0] = meltxargtab_[0].meltbp_long;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {

    MELT_LOCATION ("warmelt-genobj.melt:4652:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_.MAKE_INTEGERBOX__V3*/ meltfptr[2] =
      (meltgc_new_int
       ((meltobject_ptr_t) (( /*!DISCR_INTEGER */ meltfrout->tabval[1])),
	( /*_#IX__L1*/ meltfnum[0])));;
    MELT_LOCATION ("warmelt-genobj.melt:4656:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^msend */
    /*msend */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^ojbmsend.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & ( /*~GCX */ meltfclos->tabval[3]);
      /*_.COMPILE_OBJ__V4*/ meltfptr[3] =
	meltgc_send ((melt_ptr_t) ( /*_.CLOV__V2*/ meltfptr[1]),
		     (melt_ptr_t) (( /*!COMPILE_OBJ */ meltfrout->tabval[2])),
		     (MELTBPARSTR_PTR ""), argtab, "",
		     (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-genobj.melt:4652:/ quasiblock");


    /*^rawallocobj */
    /*rawallocobj */
    {
      melt_ptr_t newobj = 0;
      melt_raw_object_create (newobj,
			      (melt_ptr_t) (( /*!CLASS_OBJPUTCLOSEDV */
					     meltfrout->tabval[0])), (4),
			      "CLASS_OBJPUTCLOSEDV");
  /*_.INST__V6*/ meltfptr[5] =
	newobj;
    };
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OBI_LOC",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V6*/ meltfptr[5])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V6*/ meltfptr[5]), (0),
			  (( /*~NLOC */ meltfclos->tabval[1])), "OBI_LOC");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OPCLOV_CLOS",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V6*/ meltfptr[5])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V6*/ meltfptr[5]), (1),
			  (( /*~LOCV */ meltfclos->tabval[2])),
			  "OPCLOV_CLOS");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OPCLOV_OFF",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V6*/ meltfptr[5])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V6*/ meltfptr[5]), (2),
			  ( /*_.MAKE_INTEGERBOX__V3*/ meltfptr[2]),
			  "OPCLOV_OFF");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OPCLOV_CVAL",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V6*/ meltfptr[5])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V6*/ meltfptr[5]), (3),
			  ( /*_.COMPILE_OBJ__V4*/ meltfptr[3]),
			  "OPCLOV_CVAL");
    ;
    /*^touchobj */

    melt_dbgtrace_written_object ( /*_.INST__V6*/ meltfptr[5],
				  "newly made instance");
    ;
    /*_.INST___V5*/ meltfptr[4] = /*_.INST__V6*/ meltfptr[5];;

    {
      MELT_LOCATION ("warmelt-genobj.melt:4650:/ locexp");
      meltgc_append_list ((melt_ptr_t) (( /*~OLIS */ meltfclos->tabval[0])),
			  (melt_ptr_t) ( /*_.INST___V5*/ meltfptr[4]));
    }
    ;
    /*epilog */

    MELT_LOCATION ("warmelt-genobj.melt:4649:/ clear");
	   /*clear *//*_.MAKE_INTEGERBOX__V3*/ meltfptr[2] = 0;
    /*^clear */
	   /*clear *//*_.COMPILE_OBJ__V4*/ meltfptr[3] = 0;
    /*^clear */
	   /*clear *//*_.INST___V5*/ meltfptr[4] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("LAMBDA_", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_134_warmelt_genobj_LAMBDA___43___melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*noretval */ NULL);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_134_warmelt_genobj_LAMBDA___43__ */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_135_warmelt_genobj_COMPILOBJ_NREP_MAKEINST (meltclosure_ptr_t
						     meltclosp_,
						     melt_ptr_t
						     meltfirstargp_,
						     const
						     melt_argdescr_cell_t
						     meltxargdescr_[],
						     union meltparam_un *
						     meltxargtab_,
						     const
						     melt_argdescr_cell_t
						     meltxresdescr_[],
						     union meltparam_un *
						     meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_135_warmelt_genobj_COMPILOBJ_NREP_MAKEINST_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_135_warmelt_genobj_COMPILOBJ_NREP_MAKEINST_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 39
    melt_ptr_t mcfr_varptr[39];
#define MELTFRAM_NBVARNUM 8
    long mcfr_varnum[8];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_135_warmelt_genobj_COMPILOBJ_NREP_MAKEINST is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_135_warmelt_genobj_COMPILOBJ_NREP_MAKEINST_st
	 *) meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 39; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_135_warmelt_genobj_COMPILOBJ_NREP_MAKEINST nbval 39*/
  meltfram__.mcfr_nbvar = 39 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("COMPILOBJ_NREP_MAKEINST", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-genobj.melt:4665:/ getarg");
 /*_.NMKI__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.GCX__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.GCX__V3*/ meltfptr[2])) != NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:4666:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L1*/ meltfnum[0] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.GCX__V3*/ meltfptr[2]),
			     (melt_ptr_t) (( /*!CLASS_C_GENERATION_CONTEXT */
					    meltfrout->tabval[0])));;
      MELT_LOCATION ("warmelt-genobj.melt:4666:/ cond");
      /*cond */ if ( /*_#IS_A__L1*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V5*/ meltfptr[4] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-genobj.melt:4666:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check gcx"),
				  ("warmelt-genobj.melt")
				  ? ("warmelt-genobj.melt") : __FILE__,
				  (4666) ? (4666) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V5*/ meltfptr[4] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V4*/ meltfptr[3] = /*_.IFELSE___V5*/ meltfptr[4];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:4666:/ clear");
	     /*clear *//*_#IS_A__L1*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V5*/ meltfptr[4] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V4*/ meltfptr[3] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:4667:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L2*/ meltfnum[0] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.NMKI__V2*/ meltfptr[1]),
			     (melt_ptr_t) (( /*!CLASS_NREP_INSTANCE */
					    meltfrout->tabval[1])));;
      MELT_LOCATION ("warmelt-genobj.melt:4667:/ cond");
      /*cond */ if ( /*_#IS_A__L2*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V7*/ meltfptr[6] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-genobj.melt:4667:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check nmki"),
				  ("warmelt-genobj.melt")
				  ? ("warmelt-genobj.melt") : __FILE__,
				  (4667) ? (4667) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V7*/ meltfptr[6] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V6*/ meltfptr[4] = /*_.IFELSE___V7*/ meltfptr[6];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:4667:/ clear");
	     /*clear *//*_#IS_A__L2*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V7*/ meltfptr[6] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V6*/ meltfptr[4] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:4668:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L3*/ meltfnum[0] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-genobj.melt:4668:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L3*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L4*/ meltfnum[3] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-genobj.melt:4668:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L4*/ meltfnum[3];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-genobj.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 4668;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "compilobj_nrep_makeinst nmki=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.NMKI__V2*/ meltfptr[1];
	      /*_.MELT_DEBUG_FUN__V10*/ meltfptr[9] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[2])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V9*/ meltfptr[8] =
	      /*_.MELT_DEBUG_FUN__V10*/ meltfptr[9];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-genobj.melt:4668:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L4*/ meltfnum[3] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V10*/ meltfptr[9] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V9*/ meltfptr[8] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-genobj.melt:4668:/ quasiblock");


      /*_.PROGN___V11*/ meltfptr[9] = /*_.IF___V9*/ meltfptr[8];;
      /*^compute */
      /*_.IFCPP___V8*/ meltfptr[6] = /*_.PROGN___V11*/ meltfptr[9];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:4668:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L3*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IF___V9*/ meltfptr[8] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V11*/ meltfptr[9] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V8*/ meltfptr[6] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-genobj.melt:4669:/ quasiblock");


    /*^getslot */
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.NMKI__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 0, "NREP_LOC");
  /*_.NLOC__V13*/ meltfptr[9] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-genobj.melt:4670:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.NMKI__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 1, "NMINS_CLASS");
  /*_.NCLA__V14*/ meltfptr[13] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-genobj.melt:4671:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.NMKI__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 2, "NMINS_CLADATA");
  /*_.NCLADAT__V15*/ meltfptr[14] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-genobj.melt:4672:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.NMKI__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 3, "NMINS_FIELDS");
  /*_.NFIELDS__V16*/ meltfptr[15] = slot;
    };
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:4674:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L5*/ meltfnum[3] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.NCLA__V14*/ meltfptr[13]),
			     (melt_ptr_t) (( /*!CLASS_CLASS */ meltfrout->
					    tabval[3])));;
      MELT_LOCATION ("warmelt-genobj.melt:4674:/ cond");
      /*cond */ if ( /*_#IS_A__L5*/ meltfnum[3])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V18*/ meltfptr[17] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-genobj.melt:4674:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check ncla"),
				  ("warmelt-genobj.melt")
				  ? ("warmelt-genobj.melt") : __FILE__,
				  (4674) ? (4674) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V18*/ meltfptr[17] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V17*/ meltfptr[16] = /*_.IFELSE___V18*/ meltfptr[17];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:4674:/ clear");
	     /*clear *//*_#IS_A__L5*/ meltfnum[3] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V18*/ meltfptr[17] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V17*/ meltfptr[16] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-genobj.melt:4675:/ quasiblock");


    /*^getslot */
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.NCLA__V14*/ meltfptr[13]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 6, "CLASS_FIELDS");
  /*_.CLASS_FIELDS__V20*/ meltfptr[19] = slot;
    };
    ;
 /*_#NBFLD__L6*/ meltfnum[0] =
      (melt_multiple_length
       ((melt_ptr_t) ( /*_.CLASS_FIELDS__V20*/ meltfptr[19])));;
    /*^compute */
 /*_.DESTLIST__V21*/ meltfptr[20] =
      (meltgc_new_list
       ((meltobject_ptr_t) (( /*!DISCR_LIST */ meltfrout->tabval[4]))));;
    MELT_LOCATION ("warmelt-genobj.melt:4677:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & ( /*!konst_6_INST */ meltfrout->tabval[6]);
      /*_.LOCV__V22*/ meltfptr[21] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!GET_FREE_OBJLOCPTR */ meltfrout->tabval[5])),
		    (melt_ptr_t) ( /*_.GCX__V3*/ meltfptr[2]),
		    (MELTBPARSTR_PTR ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;
 /*_.OLIS__V23*/ meltfptr[22] =
      (meltgc_new_list
       ((meltobject_ptr_t) (( /*!DISCR_LIST */ meltfrout->tabval[4]))));;
    MELT_LOCATION ("warmelt-genobj.melt:4679:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^msend */
    /*msend */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^ojbmsend.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.GCX__V3*/ meltfptr[2];
      /*_.OCLADAT__V24*/ meltfptr[23] =
	meltgc_send ((melt_ptr_t) ( /*_.NCLADAT__V15*/ meltfptr[14]),
		     (melt_ptr_t) (( /*!COMPILE_OBJ */ meltfrout->tabval[7])),
		     (MELTBPARSTR_PTR ""), argtab, "",
		     (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-genobj.melt:4680:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*^rawallocobj */
    /*rawallocobj */
    {
      melt_ptr_t newobj = 0;
      melt_raw_object_create (newobj,
			      (melt_ptr_t) (( /*!CLASS_OBJPLAINBLOCK */
					     meltfrout->tabval[8])), (3),
			      "CLASS_OBJPLAINBLOCK");
  /*_.INST__V26*/ meltfptr[25] =
	newobj;
    };
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OBI_LOC",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V26*/ meltfptr[25])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V26*/ meltfptr[25]), (0),
			  ( /*_.NLOC__V13*/ meltfptr[9]), "OBI_LOC");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OBLO_BODYL",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V26*/ meltfptr[25])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V26*/ meltfptr[25]), (1),
			  ( /*_.OLIS__V23*/ meltfptr[22]), "OBLO_BODYL");
    ;
    /*^touchobj */

    melt_dbgtrace_written_object ( /*_.INST__V26*/ meltfptr[25],
				  "newly made instance");
    ;
    /*_.OBLOCK__V25*/ meltfptr[24] = /*_.INST__V26*/ meltfptr[25];;
    MELT_LOCATION ("warmelt-genobj.melt:4683:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_.MAKE_INTEGERBOX__V27*/ meltfptr[26] =
      (meltgc_new_int
       ((meltobject_ptr_t) (( /*!DISCR_INTEGER */ meltfrout->tabval[10])),
	( /*_#NBFLD__L6*/ meltfnum[0])));;
    MELT_LOCATION ("warmelt-genobj.melt:4687:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.NCLA__V14*/ meltfptr[13]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 1, "NAMED_NAME");
  /*_.NAMED_NAME__V28*/ meltfptr[27] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-genobj.melt:4683:/ quasiblock");


    /*^rawallocobj */
    /*rawallocobj */
    {
      melt_ptr_t newobj = 0;
      melt_raw_object_create (newobj,
			      (melt_ptr_t) (( /*!CLASS_OBJRAWALLOCOBJ */
					     meltfrout->tabval[9])), (5),
			      "CLASS_OBJRAWALLOCOBJ");
  /*_.INST__V30*/ meltfptr[29] =
	newobj;
    };
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OBI_LOC",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V30*/ meltfptr[29])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V30*/ meltfptr[29]), (0),
			  ( /*_.NLOC__V13*/ meltfptr[9]), "OBI_LOC");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OBRALLOBJ_CLASS",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V30*/ meltfptr[29])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V30*/ meltfptr[29]), (2),
			  ( /*_.OCLADAT__V24*/ meltfptr[23]),
			  "OBRALLOBJ_CLASS");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OBRALLOBJ_LEN",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V30*/ meltfptr[29])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V30*/ meltfptr[29]), (3),
			  ( /*_.MAKE_INTEGERBOX__V27*/ meltfptr[26]),
			  "OBRALLOBJ_LEN");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OBRALLOBJ_CLASSNAME",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V30*/ meltfptr[29])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V30*/ meltfptr[29]), (4),
			  ( /*_.NAMED_NAME__V28*/ meltfptr[27]),
			  "OBRALLOBJ_CLASSNAME");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OBDI_DESTLIST",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V30*/ meltfptr[29])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V30*/ meltfptr[29]), (1),
			  ( /*_.DESTLIST__V21*/ meltfptr[20]),
			  "OBDI_DESTLIST");
    ;
    /*^touchobj */

    melt_dbgtrace_written_object ( /*_.INST__V30*/ meltfptr[29],
				  "newly made instance");
    ;
    /*_.ORALLOBJ__V29*/ meltfptr[28] = /*_.INST__V30*/ meltfptr[29];;

    {
      MELT_LOCATION ("warmelt-genobj.melt:4690:/ locexp");
      meltgc_append_list ((melt_ptr_t) ( /*_.DESTLIST__V21*/ meltfptr[20]),
			  (melt_ptr_t) ( /*_.LOCV__V22*/ meltfptr[21]));
    }
    ;

    {
      MELT_LOCATION ("warmelt-genobj.melt:4691:/ locexp");
      meltgc_append_list ((melt_ptr_t) ( /*_.OLIS__V23*/ meltfptr[22]),
			  (melt_ptr_t) ( /*_.ORALLOBJ__V29*/ meltfptr[28]));
    }
    ;
    MELT_LOCATION ("warmelt-genobj.melt:4695:/ quasiblock");


    /*^newclosure */
		 /*newclosure *//*_.LAMBDA___V32*/ meltfptr[31] =
      (melt_ptr_t)
      meltgc_new_closure ((meltobject_ptr_t)
			  (((melt_ptr_t) (MELT_PREDEF (DISCR_CLOSURE)))),
			  (meltroutine_ptr_t) (( /*!konst_18 */ meltfrout->
						tabval[18])), (4));
    ;
    /*^putclosedv */
    /*putclosv */
    melt_assertmsg ("putclosv checkclo",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.LAMBDA___V32*/ meltfptr[31])) ==
		    MELTOBMAG_CLOSURE);
    melt_assertmsg ("putclosv checkoff", 0 >= 0
		    && 0 <
		    melt_closure_size ((melt_ptr_t)
				       ( /*_.LAMBDA___V32*/ meltfptr[31])));
    ((meltclosure_ptr_t) /*_.LAMBDA___V32*/ meltfptr[31])->tabval[0] =
      (melt_ptr_t) ( /*_.OLIS__V23*/ meltfptr[22]);
    ;
    /*^putclosedv */
    /*putclosv */
    melt_assertmsg ("putclosv checkclo",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.LAMBDA___V32*/ meltfptr[31])) ==
		    MELTOBMAG_CLOSURE);
    melt_assertmsg ("putclosv checkoff", 1 >= 0
		    && 1 <
		    melt_closure_size ((melt_ptr_t)
				       ( /*_.LAMBDA___V32*/ meltfptr[31])));
    ((meltclosure_ptr_t) /*_.LAMBDA___V32*/ meltfptr[31])->tabval[1] =
      (melt_ptr_t) ( /*_.GCX__V3*/ meltfptr[2]);
    ;
    /*^putclosedv */
    /*putclosv */
    melt_assertmsg ("putclosv checkclo",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.LAMBDA___V32*/ meltfptr[31])) ==
		    MELTOBMAG_CLOSURE);
    melt_assertmsg ("putclosv checkoff", 2 >= 0
		    && 2 <
		    melt_closure_size ((melt_ptr_t)
				       ( /*_.LAMBDA___V32*/ meltfptr[31])));
    ((meltclosure_ptr_t) /*_.LAMBDA___V32*/ meltfptr[31])->tabval[2] =
      (melt_ptr_t) ( /*_.NLOC__V13*/ meltfptr[9]);
    ;
    /*^putclosedv */
    /*putclosv */
    melt_assertmsg ("putclosv checkclo",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.LAMBDA___V32*/ meltfptr[31])) ==
		    MELTOBMAG_CLOSURE);
    melt_assertmsg ("putclosv checkoff", 3 >= 0
		    && 3 <
		    melt_closure_size ((melt_ptr_t)
				       ( /*_.LAMBDA___V32*/ meltfptr[31])));
    ((meltclosure_ptr_t) /*_.LAMBDA___V32*/ meltfptr[31])->tabval[3] =
      (melt_ptr_t) ( /*_.LOCV__V22*/ meltfptr[21]);
    ;
    /*_.LAMBDA___V31*/ meltfptr[30] = /*_.LAMBDA___V32*/ meltfptr[31];;
    MELT_LOCATION ("warmelt-genobj.melt:4693:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & /*_.LAMBDA___V31*/ meltfptr[30];
      /*_.MULTIPLE_EVERY__V33*/ meltfptr[32] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!MULTIPLE_EVERY */ meltfrout->tabval[11])),
		    (melt_ptr_t) ( /*_.NFIELDS__V16*/ meltfptr[15]),
		    (MELTBPARSTR_PTR ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-genobj.melt:4716:/ quasiblock");


    /*^checksignal */
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*^rawallocobj */
    /*rawallocobj */
    {
      melt_ptr_t newobj = 0;
      melt_raw_object_create (newobj,
			      (melt_ptr_t) (( /*!CLASS_OBJDBGTRACEWRITEOBJ */
					     meltfrout->tabval[19])), (3),
			      "CLASS_OBJDBGTRACEWRITEOBJ");
  /*_.INST__V35*/ meltfptr[34] =
	newobj;
    };
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OBI_LOC",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V35*/ meltfptr[34])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V35*/ meltfptr[34]), (0),
			  ( /*_.NLOC__V13*/ meltfptr[9]), "OBI_LOC");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OBDTW_WRITTENOBJ",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V35*/ meltfptr[34])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V35*/ meltfptr[34]), (1),
			  ( /*_.LOCV__V22*/ meltfptr[21]),
			  "OBDTW_WRITTENOBJ");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OBDTW_MESSAGE",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V35*/ meltfptr[34])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V35*/ meltfptr[34]), (2),
			  (( /*!konst_20 */ meltfrout->tabval[20])),
			  "OBDTW_MESSAGE");
    ;
    /*^touchobj */

    melt_dbgtrace_written_object ( /*_.INST__V35*/ meltfptr[34],
				  "newly made instance");
    ;
    /*_.ODBGTR__V34*/ meltfptr[33] = /*_.INST__V35*/ meltfptr[34];;

    {
      MELT_LOCATION ("warmelt-genobj.melt:4722:/ locexp");
      meltgc_append_list ((melt_ptr_t) ( /*_.OLIS__V23*/ meltfptr[22]),
			  (melt_ptr_t) ( /*_.ODBGTR__V34*/ meltfptr[33]));
    }
    ;

    MELT_LOCATION ("warmelt-genobj.melt:4716:/ clear");
	   /*clear *//*_.ODBGTR__V34*/ meltfptr[33] = 0;

    {
      MELT_LOCATION ("warmelt-genobj.melt:4724:/ locexp");
      meltgc_append_list ((melt_ptr_t) ( /*_.OLIS__V23*/ meltfptr[22]),
			  (melt_ptr_t) ( /*_.LOCV__V22*/ meltfptr[21]));
    }
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:4725:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L7*/ meltfnum[3] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-genobj.melt:4725:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L7*/ meltfnum[3])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L8*/ meltfnum[7] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-genobj.melt:4725:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L8*/ meltfnum[7];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-genobj.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 4725;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring =
		"compilobj_nrep_makeinst result oblock=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.OBLOCK__V25*/ meltfptr[24];
	      /*_.MELT_DEBUG_FUN__V38*/ meltfptr[37] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[2])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V37*/ meltfptr[36] =
	      /*_.MELT_DEBUG_FUN__V38*/ meltfptr[37];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-genobj.melt:4725:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L8*/ meltfnum[7] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V38*/ meltfptr[37] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V37*/ meltfptr[36] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-genobj.melt:4725:/ quasiblock");


      /*_.PROGN___V39*/ meltfptr[37] = /*_.IF___V37*/ meltfptr[36];;
      /*^compute */
      /*_.IFCPP___V36*/ meltfptr[33] = /*_.PROGN___V39*/ meltfptr[37];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:4725:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L7*/ meltfnum[3] = 0;
      /*^clear */
	     /*clear *//*_.IF___V37*/ meltfptr[36] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V39*/ meltfptr[37] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V36*/ meltfptr[33] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    /*^compute */
    /*_.LET___V19*/ meltfptr[17] = /*_.OBLOCK__V25*/ meltfptr[24];;

    MELT_LOCATION ("warmelt-genobj.melt:4675:/ clear");
	   /*clear *//*_.CLASS_FIELDS__V20*/ meltfptr[19] = 0;
    /*^clear */
	   /*clear *//*_#NBFLD__L6*/ meltfnum[0] = 0;
    /*^clear */
	   /*clear *//*_.DESTLIST__V21*/ meltfptr[20] = 0;
    /*^clear */
	   /*clear *//*_.LOCV__V22*/ meltfptr[21] = 0;
    /*^clear */
	   /*clear *//*_.OLIS__V23*/ meltfptr[22] = 0;
    /*^clear */
	   /*clear *//*_.OCLADAT__V24*/ meltfptr[23] = 0;
    /*^clear */
	   /*clear *//*_.OBLOCK__V25*/ meltfptr[24] = 0;
    /*^clear */
	   /*clear *//*_.MAKE_INTEGERBOX__V27*/ meltfptr[26] = 0;
    /*^clear */
	   /*clear *//*_.NAMED_NAME__V28*/ meltfptr[27] = 0;
    /*^clear */
	   /*clear *//*_.ORALLOBJ__V29*/ meltfptr[28] = 0;
    /*^clear */
	   /*clear *//*_.LAMBDA___V31*/ meltfptr[30] = 0;
    /*^clear */
	   /*clear *//*_.MULTIPLE_EVERY__V33*/ meltfptr[32] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V36*/ meltfptr[33] = 0;
    /*_.LET___V12*/ meltfptr[8] = /*_.LET___V19*/ meltfptr[17];;

    MELT_LOCATION ("warmelt-genobj.melt:4669:/ clear");
	   /*clear *//*_.NLOC__V13*/ meltfptr[9] = 0;
    /*^clear */
	   /*clear *//*_.NCLA__V14*/ meltfptr[13] = 0;
    /*^clear */
	   /*clear *//*_.NCLADAT__V15*/ meltfptr[14] = 0;
    /*^clear */
	   /*clear *//*_.NFIELDS__V16*/ meltfptr[15] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V17*/ meltfptr[16] = 0;
    /*^clear */
	   /*clear *//*_.LET___V19*/ meltfptr[17] = 0;
    MELT_LOCATION ("warmelt-genobj.melt:4665:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.LET___V12*/ meltfptr[8];;

    {
      MELT_LOCATION ("warmelt-genobj.melt:4665:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V4*/ meltfptr[3] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V6*/ meltfptr[4] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V8*/ meltfptr[6] = 0;
    /*^clear */
	   /*clear *//*_.LET___V12*/ meltfptr[8] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("COMPILOBJ_NREP_MAKEINST", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_135_warmelt_genobj_COMPILOBJ_NREP_MAKEINST_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_135_warmelt_genobj_COMPILOBJ_NREP_MAKEINST */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_136_warmelt_genobj_LAMBDA___44__ (meltclosure_ptr_t meltclosp_,
					   melt_ptr_t meltfirstargp_,
					   const melt_argdescr_cell_t
					   meltxargdescr_[],
					   union meltparam_un * meltxargtab_,
					   const melt_argdescr_cell_t
					   meltxresdescr_[],
					   union meltparam_un * meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_136_warmelt_genobj_LAMBDA___44___melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_136_warmelt_genobj_LAMBDA___44___st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 17
    melt_ptr_t mcfr_varptr[17];
#define MELTFRAM_NBVARNUM 6
    long mcfr_varnum[6];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_136_warmelt_genobj_LAMBDA___44__ is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_136_warmelt_genobj_LAMBDA___44___st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 17; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_136_warmelt_genobj_LAMBDA___44__ nbval 17*/
  meltfram__.mcfr_nbvar = 17 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("LAMBDA_", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-genobj.melt:4695:/ getarg");
 /*_.CFLA__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_LONG)
    goto lab_endgetargs;
 /*_#IX__L1*/ meltfnum[0] = meltxargtab_[0].meltbp_long;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:4696:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L2*/ meltfnum[1] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.CFLA__V2*/ meltfptr[1]),
			     (melt_ptr_t) (( /*!CLASS_NREP_FIELDASSIGN */
					    meltfrout->tabval[0])));;
      MELT_LOCATION ("warmelt-genobj.melt:4696:/ cond");
      /*cond */ if ( /*_#IS_A__L2*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V4*/ meltfptr[3] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-genobj.melt:4696:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("compilobj_nrep_makeinst check cfla"),
				  ("warmelt-genobj.melt")
				  ? ("warmelt-genobj.melt") : __FILE__,
				  (4696) ? (4696) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V4*/ meltfptr[3] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V3*/ meltfptr[2] = /*_.IFELSE___V4*/ meltfptr[3];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:4696:/ clear");
	     /*clear *//*_#IS_A__L2*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V4*/ meltfptr[3] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V3*/ meltfptr[2] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-genobj.melt:4697:/ quasiblock");


    /*^getslot */
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.CFLA__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 0, "NREP_LOC");
  /*_.CFLOC__V5*/ meltfptr[3] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-genobj.melt:4698:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.CFLA__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 1, "NFLA_FIELD");
  /*_.CFIELD__V6*/ meltfptr[5] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-genobj.melt:4699:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.CFLA__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 2, "NFLA_VAL");
  /*_.CVAL__V7*/ meltfptr[6] = slot;
    };
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:4700:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L3*/ meltfnum[1] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.CFIELD__V6*/ meltfptr[5]),
			     (melt_ptr_t) (( /*!CLASS_FIELD */ meltfrout->
					    tabval[1])));;
      MELT_LOCATION ("warmelt-genobj.melt:4700:/ cond");
      /*cond */ if ( /*_#IS_A__L3*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V9*/ meltfptr[8] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-genobj.melt:4700:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("compilobj_nrep_makeinst check cfield"),
				  ("warmelt-genobj.melt")
				  ? ("warmelt-genobj.melt") : __FILE__,
				  (4700) ? (4700) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V9*/ meltfptr[8] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V8*/ meltfptr[7] = /*_.IFELSE___V9*/ meltfptr[8];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:4700:/ clear");
	     /*clear *//*_#IS_A__L3*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V9*/ meltfptr[8] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V8*/ meltfptr[7] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-genobj.melt:4703:/ quasiblock");


    /*^checksignal */
    MELT_CHECK_SIGNAL ();
    ;
    /*^msend */
    /*msend */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^ojbmsend.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & ( /*~GCX */ meltfclos->tabval[1]);
      /*_.OVAL__V11*/ meltfptr[10] =
	meltgc_send ((melt_ptr_t) ( /*_.CVAL__V7*/ meltfptr[6]),
		     (melt_ptr_t) (( /*!COMPILE_OBJ */ meltfrout->tabval[2])),
		     (MELTBPARSTR_PTR ""), argtab, "",
		     (union meltparam_un *) 0);
    }
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:4704:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L4*/ meltfnum[1] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.OVAL__V11*/ meltfptr[10]),
			     (melt_ptr_t) (( /*!CLASS_NREP */ meltfrout->
					    tabval[3])));;
      /*^compute */
   /*_#NOT__L5*/ meltfnum[4] =
	(!( /*_#IS_A__L4*/ meltfnum[1]));;
      MELT_LOCATION ("warmelt-genobj.melt:4704:/ cond");
      /*cond */ if ( /*_#NOT__L5*/ meltfnum[4])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V13*/ meltfptr[12] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-genobj.melt:4704:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("compilobj_nrep_makeinst check oval not nrep"), ("warmelt-genobj.melt") ? ("warmelt-genobj.melt") : __FILE__, (4704) ? (4704) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V13*/ meltfptr[12] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V12*/ meltfptr[11] = /*_.IFELSE___V13*/ meltfptr[12];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:4704:/ clear");
	     /*clear *//*_#IS_A__L4*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_#NOT__L5*/ meltfnum[4] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V13*/ meltfptr[12] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V12*/ meltfptr[11] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-genobj.melt:4705:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    MELT_LOCATION ("warmelt-genobj.melt:4706:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^cond */
    /*cond */ if ( /*_.CFLOC__V5*/ meltfptr[3])	/*then */
      {
	/*^cond.then */
	/*_.IFELSE___V14*/ meltfptr[12] = /*_.CFLOC__V5*/ meltfptr[3];;
      }
    else
      {
	MELT_LOCATION ("warmelt-genobj.melt:4706:/ cond.else");

	/*_.IFELSE___V14*/ meltfptr[12] = ( /*~NLOC */ meltfclos->tabval[2]);;
      }
    ;
    /*^compute */
 /*_#GET_INT__L6*/ meltfnum[1] =
      (melt_get_int ((melt_ptr_t) ( /*_.CFIELD__V6*/ meltfptr[5])));;
    /*^compute */
 /*_.MAKE_INTEGERBOX__V15*/ meltfptr[14] =
      (meltgc_new_int
       ((meltobject_ptr_t) (( /*!DISCR_INTEGER */ meltfrout->tabval[5])),
	( /*_#GET_INT__L6*/ meltfnum[1])));;
    MELT_LOCATION ("warmelt-genobj.melt:4705:/ quasiblock");


    /*^rawallocobj */
    /*rawallocobj */
    {
      melt_ptr_t newobj = 0;
      melt_raw_object_create (newobj,
			      (melt_ptr_t) (( /*!CLASS_OBJPUTSLOT */
					     meltfrout->tabval[4])), (5),
			      "CLASS_OBJPUTSLOT");
  /*_.INST__V17*/ meltfptr[16] =
	newobj;
    };
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OBI_LOC",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V17*/ meltfptr[16])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V17*/ meltfptr[16]), (0),
			  ( /*_.IFELSE___V14*/ meltfptr[12]), "OBI_LOC");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OSLOT_ODATA",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V17*/ meltfptr[16])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V17*/ meltfptr[16]), (1),
			  (( /*~LOCV */ meltfclos->tabval[3])),
			  "OSLOT_ODATA");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OSLOT_FIELD",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V17*/ meltfptr[16])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V17*/ meltfptr[16]), (3),
			  ( /*_.CFIELD__V6*/ meltfptr[5]), "OSLOT_FIELD");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OSLOT_OFFSET",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V17*/ meltfptr[16])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V17*/ meltfptr[16]), (2),
			  ( /*_.MAKE_INTEGERBOX__V15*/ meltfptr[14]),
			  "OSLOT_OFFSET");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OSLOT_VALUE",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V17*/ meltfptr[16])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V17*/ meltfptr[16]), (4),
			  ( /*_.OVAL__V11*/ meltfptr[10]), "OSLOT_VALUE");
    ;
    /*^touchobj */

    melt_dbgtrace_written_object ( /*_.INST__V17*/ meltfptr[16],
				  "newly made instance");
    ;
    /*_.INST___V16*/ meltfptr[15] = /*_.INST__V17*/ meltfptr[16];;
    /*^compute */
    /*_.LET___V10*/ meltfptr[8] = /*_.INST___V16*/ meltfptr[15];;

    MELT_LOCATION ("warmelt-genobj.melt:4703:/ clear");
	   /*clear *//*_.OVAL__V11*/ meltfptr[10] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V12*/ meltfptr[11] = 0;
    /*^clear */
	   /*clear *//*_.IFELSE___V14*/ meltfptr[12] = 0;
    /*^clear */
	   /*clear *//*_#GET_INT__L6*/ meltfnum[1] = 0;
    /*^clear */
	   /*clear *//*_.MAKE_INTEGERBOX__V15*/ meltfptr[14] = 0;
    /*^clear */
	   /*clear *//*_.INST___V16*/ meltfptr[15] = 0;

    {
      MELT_LOCATION ("warmelt-genobj.melt:4701:/ locexp");
      meltgc_append_list ((melt_ptr_t) (( /*~OLIS */ meltfclos->tabval[0])),
			  (melt_ptr_t) ( /*_.LET___V10*/ meltfptr[8]));
    }
    ;

    MELT_LOCATION ("warmelt-genobj.melt:4697:/ clear");
	   /*clear *//*_.CFLOC__V5*/ meltfptr[3] = 0;
    /*^clear */
	   /*clear *//*_.CFIELD__V6*/ meltfptr[5] = 0;
    /*^clear */
	   /*clear *//*_.CVAL__V7*/ meltfptr[6] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V8*/ meltfptr[7] = 0;
    /*^clear */
	   /*clear *//*_.LET___V10*/ meltfptr[8] = 0;
    MELT_LOCATION ("warmelt-genobj.melt:4695:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V3*/ meltfptr[2] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("LAMBDA_", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_136_warmelt_genobj_LAMBDA___44___melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*noretval */ NULL);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_136_warmelt_genobj_LAMBDA___44__ */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_137_warmelt_genobj_COMPILOBJ_ROUTPROC (meltclosure_ptr_t meltclosp_,
						melt_ptr_t meltfirstargp_,
						const melt_argdescr_cell_t
						meltxargdescr_[],
						union meltparam_un *
						meltxargtab_,
						const melt_argdescr_cell_t
						meltxresdescr_[],
						union meltparam_un *
						meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_137_warmelt_genobj_COMPILOBJ_ROUTPROC_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_137_warmelt_genobj_COMPILOBJ_ROUTPROC_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 13
    melt_ptr_t mcfr_varptr[13];
#define MELTFRAM_NBVARNUM 4
    long mcfr_varnum[4];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_137_warmelt_genobj_COMPILOBJ_ROUTPROC is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_137_warmelt_genobj_COMPILOBJ_ROUTPROC_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 13; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_137_warmelt_genobj_COMPILOBJ_ROUTPROC nbval 13*/
  meltfram__.mcfr_nbvar = 13 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("COMPILOBJ_ROUTPROC", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-genobj.melt:4733:/ getarg");
 /*_.NPRO__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.GCX__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.GCX__V3*/ meltfptr[2])) != NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:4734:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L1*/ meltfnum[0] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.GCX__V3*/ meltfptr[2]),
			     (melt_ptr_t) (( /*!CLASS_C_GENERATION_CONTEXT */
					    meltfrout->tabval[0])));;
      MELT_LOCATION ("warmelt-genobj.melt:4734:/ cond");
      /*cond */ if ( /*_#IS_A__L1*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V5*/ meltfptr[4] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-genobj.melt:4734:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check gcx"),
				  ("warmelt-genobj.melt")
				  ? ("warmelt-genobj.melt") : __FILE__,
				  (4734) ? (4734) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V5*/ meltfptr[4] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V4*/ meltfptr[3] = /*_.IFELSE___V5*/ meltfptr[4];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:4734:/ clear");
	     /*clear *//*_#IS_A__L1*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V5*/ meltfptr[4] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V4*/ meltfptr[3] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:4735:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L2*/ meltfnum[0] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.NPRO__V2*/ meltfptr[1]),
			     (melt_ptr_t) (( /*!CLASS_NREP_ROUTPROC */
					    meltfrout->tabval[1])));;
      MELT_LOCATION ("warmelt-genobj.melt:4735:/ cond");
      /*cond */ if ( /*_#IS_A__L2*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V7*/ meltfptr[6] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-genobj.melt:4735:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check npro"),
				  ("warmelt-genobj.melt")
				  ? ("warmelt-genobj.melt") : __FILE__,
				  (4735) ? (4735) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V7*/ meltfptr[6] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V6*/ meltfptr[4] = /*_.IFELSE___V7*/ meltfptr[6];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:4735:/ clear");
	     /*clear *//*_#IS_A__L2*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V7*/ meltfptr[6] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V6*/ meltfptr[4] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:4736:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L3*/ meltfnum[0] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-genobj.melt:4736:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L3*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L4*/ meltfnum[3] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-genobj.melt:4736:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[7];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L4*/ meltfnum[3];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-genobj.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 4736;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "compilobj_routproc npro=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.NPRO__V2*/ meltfptr[1];
	      /*^apply.arg */
	      argtab[5].meltbp_cstring = " gcx=";
	      /*^apply.arg */
	      argtab[6].meltbp_aptr =
		(melt_ptr_t *) & /*_.GCX__V3*/ meltfptr[2];
	      /*_.MELT_DEBUG_FUN__V10*/ meltfptr[9] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[2])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V9*/ meltfptr[8] =
	      /*_.MELT_DEBUG_FUN__V10*/ meltfptr[9];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-genobj.melt:4736:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L4*/ meltfnum[3] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V10*/ meltfptr[9] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V9*/ meltfptr[8] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-genobj.melt:4736:/ quasiblock");


      /*_.PROGN___V11*/ meltfptr[9] = /*_.IF___V9*/ meltfptr[8];;
      /*^compute */
      /*_.IFCPP___V8*/ meltfptr[6] = /*_.PROGN___V11*/ meltfptr[9];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:4736:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L3*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IF___V9*/ meltfptr[8] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V11*/ meltfptr[9] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V8*/ meltfptr[6] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:4737:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
      /*^cond */
      /*cond */ if (( /*nil */ NULL))	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V13*/ meltfptr[9] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-genobj.melt:4737:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("UNEXPECTED CALL TO compilobj_routproc"),
				  ("warmelt-genobj.melt")
				  ? ("warmelt-genobj.melt") : __FILE__,
				  (4737) ? (4737) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V13*/ meltfptr[9] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V12*/ meltfptr[8] = /*_.IFELSE___V13*/ meltfptr[9];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:4737:/ clear");
	     /*clear *//*_.IFELSE___V13*/ meltfptr[9] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V12*/ meltfptr[8] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-genobj.melt:4733:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.IFCPP___V12*/ meltfptr[8];;

    {
      MELT_LOCATION ("warmelt-genobj.melt:4733:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V4*/ meltfptr[3] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V6*/ meltfptr[4] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V8*/ meltfptr[6] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V12*/ meltfptr[8] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("COMPILOBJ_ROUTPROC", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_137_warmelt_genobj_COMPILOBJ_ROUTPROC_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_137_warmelt_genobj_COMPILOBJ_ROUTPROC */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_138_warmelt_genobj_COMPILOBJ_PREDEF (meltclosure_ptr_t meltclosp_,
					      melt_ptr_t meltfirstargp_,
					      const melt_argdescr_cell_t
					      meltxargdescr_[],
					      union meltparam_un *
					      meltxargtab_,
					      const melt_argdescr_cell_t
					      meltxresdescr_[],
					      union meltparam_un *
					      meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_138_warmelt_genobj_COMPILOBJ_PREDEF_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_138_warmelt_genobj_COMPILOBJ_PREDEF_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 9
    melt_ptr_t mcfr_varptr[9];
#define MELTFRAM_NBVARNUM 1
    long mcfr_varnum[1];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_138_warmelt_genobj_COMPILOBJ_PREDEF is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_138_warmelt_genobj_COMPILOBJ_PREDEF_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 9; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_138_warmelt_genobj_COMPILOBJ_PREDEF nbval 9*/
  meltfram__.mcfr_nbvar = 9 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("COMPILOBJ_PREDEF", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-genobj.melt:4742:/ getarg");
 /*_.NPR__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.GCX__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.GCX__V3*/ meltfptr[2])) != NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:4743:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L1*/ meltfnum[0] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.GCX__V3*/ meltfptr[2]),
			     (melt_ptr_t) (( /*!CLASS_C_GENERATION_CONTEXT */
					    meltfrout->tabval[0])));;
      MELT_LOCATION ("warmelt-genobj.melt:4743:/ cond");
      /*cond */ if ( /*_#IS_A__L1*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V5*/ meltfptr[4] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-genobj.melt:4743:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check gcx"),
				  ("warmelt-genobj.melt")
				  ? ("warmelt-genobj.melt") : __FILE__,
				  (4743) ? (4743) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V5*/ meltfptr[4] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V4*/ meltfptr[3] = /*_.IFELSE___V5*/ meltfptr[4];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:4743:/ clear");
	     /*clear *//*_#IS_A__L1*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V5*/ meltfptr[4] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V4*/ meltfptr[3] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-genobj.melt:4744:/ quasiblock");


    /*^checksignal */
    MELT_CHECK_SIGNAL ();
    ;
    MELT_LOCATION ("warmelt-genobj.melt:4746:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.NPR__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 1, "NRPREDEF");
  /*_.NRPREDEF__V7*/ meltfptr[6] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-genobj.melt:4744:/ quasiblock");


    /*^rawallocobj */
    /*rawallocobj */
    {
      melt_ptr_t newobj = 0;
      melt_raw_object_create (newobj,
			      (melt_ptr_t) (( /*!CLASS_OBJPREDEF */
					     meltfrout->tabval[1])), (2),
			      "CLASS_OBJPREDEF");
  /*_.INST__V9*/ meltfptr[8] =
	newobj;
    };
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OBV_TYPE",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V9*/ meltfptr[8])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V9*/ meltfptr[8]), (0),
			  (( /*!CTYPE_VALUE */ meltfrout->tabval[2])),
			  "OBV_TYPE");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OBPREDEF",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V9*/ meltfptr[8])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V9*/ meltfptr[8]), (1),
			  ( /*_.NRPREDEF__V7*/ meltfptr[6]), "OBPREDEF");
    ;
    /*^touchobj */

    melt_dbgtrace_written_object ( /*_.INST__V9*/ meltfptr[8],
				  "newly made instance");
    ;
    /*_.OPR__V8*/ meltfptr[7] = /*_.INST__V9*/ meltfptr[8];;
    /*^compute */
    /*_.LET___V6*/ meltfptr[4] = /*_.OPR__V8*/ meltfptr[7];;

    MELT_LOCATION ("warmelt-genobj.melt:4744:/ clear");
	   /*clear *//*_.NRPREDEF__V7*/ meltfptr[6] = 0;
    /*^clear */
	   /*clear *//*_.OPR__V8*/ meltfptr[7] = 0;
    MELT_LOCATION ("warmelt-genobj.melt:4742:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.LET___V6*/ meltfptr[4];;

    {
      MELT_LOCATION ("warmelt-genobj.melt:4742:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V4*/ meltfptr[3] = 0;
    /*^clear */
	   /*clear *//*_.LET___V6*/ meltfptr[4] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("COMPILOBJ_PREDEF", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_138_warmelt_genobj_COMPILOBJ_PREDEF_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_138_warmelt_genobj_COMPILOBJ_PREDEF */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_139_warmelt_genobj_COMPIL_DATA_AND_SLOTS_FILL (meltclosure_ptr_t
							meltclosp_,
							melt_ptr_t
							meltfirstargp_,
							const
							melt_argdescr_cell_t
							meltxargdescr_[],
							union meltparam_un *
							meltxargtab_,
							const
							melt_argdescr_cell_t
							meltxresdescr_[],
							union meltparam_un *
							meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_139_warmelt_genobj_COMPIL_DATA_AND_SLOTS_FILL_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_139_warmelt_genobj_COMPIL_DATA_AND_SLOTS_FILL_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 87
    melt_ptr_t mcfr_varptr[87];
#define MELTFRAM_NBVARNUM 17
    long mcfr_varnum[17];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_139_warmelt_genobj_COMPIL_DATA_AND_SLOTS_FILL is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct
	 frame_meltrout_139_warmelt_genobj_COMPIL_DATA_AND_SLOTS_FILL_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 87; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_139_warmelt_genobj_COMPIL_DATA_AND_SLOTS_FILL nbval 87*/
  meltfram__.mcfr_nbvar = 87 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("COMPIL_DATA_AND_SLOTS_FILL", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-genobj.melt:4758:/ getarg");
 /*_.NDAT__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.OBJ__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.OBJ__V3*/ meltfptr[2])) != NULL);


  /*getarg#2 */
  /*^getarg */
  if (meltxargdescr_[1] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.ODISCR__V4*/ meltfptr[3] =
    (meltxargtab_[1].meltbp_aptr) ? (*(meltxargtab_[1].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.ODISCR__V4*/ meltfptr[3])) !=
	      NULL);


  /*getarg#3 */
  /*^getarg */
  if (meltxargdescr_[2] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.IROUT__V5*/ meltfptr[4] =
    (meltxargtab_[2].meltbp_aptr) ? (*(meltxargtab_[2].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.IROUT__V5*/ meltfptr[4])) !=
	      NULL);


  /*getarg#4 */
  /*^getarg */
  if (meltxargdescr_[3] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.GCX__V6*/ meltfptr[5] =
    (meltxargtab_[3].meltbp_aptr) ? (*(meltxargtab_[3].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.GCX__V6*/ meltfptr[5])) != NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:4759:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L1*/ meltfnum[0] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.NDAT__V2*/ meltfptr[1]),
			     (melt_ptr_t) (( /*!CLASS_NREP_BOUND_DATA */
					    meltfrout->tabval[0])));;
      MELT_LOCATION ("warmelt-genobj.melt:4759:/ cond");
      /*cond */ if ( /*_#IS_A__L1*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V8*/ meltfptr[7] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-genobj.melt:4759:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("compil_data_and_slots_fill check ndat"),
				  ("warmelt-genobj.melt")
				  ? ("warmelt-genobj.melt") : __FILE__,
				  (4759) ? (4759) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V8*/ meltfptr[7] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V7*/ meltfptr[6] = /*_.IFELSE___V8*/ meltfptr[7];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:4759:/ clear");
	     /*clear *//*_#IS_A__L1*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V8*/ meltfptr[7] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V7*/ meltfptr[6] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:4760:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L2*/ meltfnum[0] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.OBJ__V3*/ meltfptr[2]),
			     (melt_ptr_t) (( /*!CLASS_OBJINITOBJECT */
					    meltfrout->tabval[1])));;
      MELT_LOCATION ("warmelt-genobj.melt:4760:/ cond");
      /*cond */ if ( /*_#IS_A__L2*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V10*/ meltfptr[9] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-genobj.melt:4760:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("compil_data_and_slots_fill check obj"),
				  ("warmelt-genobj.melt")
				  ? ("warmelt-genobj.melt") : __FILE__,
				  (4760) ? (4760) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V10*/ meltfptr[9] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V9*/ meltfptr[7] = /*_.IFELSE___V10*/ meltfptr[9];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:4760:/ clear");
	     /*clear *//*_#IS_A__L2*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V10*/ meltfptr[9] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V9*/ meltfptr[7] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:4761:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L3*/ meltfnum[0] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.ODISCR__V4*/ meltfptr[3]),
			     (melt_ptr_t) (( /*!CLASS_OBJVALUE */ meltfrout->
					    tabval[2])));;
      MELT_LOCATION ("warmelt-genobj.melt:4761:/ cond");
      /*cond */ if ( /*_#IS_A__L3*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V12*/ meltfptr[11] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-genobj.melt:4761:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("compil_data_and_slots_fill check odiscr"),
				  ("warmelt-genobj.melt")
				  ? ("warmelt-genobj.melt") : __FILE__,
				  (4761) ? (4761) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V12*/ meltfptr[11] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V11*/ meltfptr[9] = /*_.IFELSE___V12*/ meltfptr[11];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:4761:/ clear");
	     /*clear *//*_#IS_A__L3*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V12*/ meltfptr[11] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V11*/ meltfptr[9] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:4762:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L4*/ meltfnum[0] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.IROUT__V5*/ meltfptr[4]),
			     (melt_ptr_t) (( /*!CLASS_INITIALROUTINEOBJ */
					    meltfrout->tabval[3])));;
      MELT_LOCATION ("warmelt-genobj.melt:4762:/ cond");
      /*cond */ if ( /*_#IS_A__L4*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V14*/ meltfptr[13] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-genobj.melt:4762:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("compil_data_and_slots_fill check irout"),
				  ("warmelt-genobj.melt")
				  ? ("warmelt-genobj.melt") : __FILE__,
				  (4762) ? (4762) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V14*/ meltfptr[13] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V13*/ meltfptr[11] = /*_.IFELSE___V14*/ meltfptr[13];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:4762:/ clear");
	     /*clear *//*_#IS_A__L4*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V14*/ meltfptr[13] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V13*/ meltfptr[11] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:4763:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L5*/ meltfnum[0] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.GCX__V6*/ meltfptr[5]),
			     (melt_ptr_t) (( /*!CLASS_C_GENERATION_CONTEXT */
					    meltfrout->tabval[4])));;
      MELT_LOCATION ("warmelt-genobj.melt:4763:/ cond");
      /*cond */ if ( /*_#IS_A__L5*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V16*/ meltfptr[15] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-genobj.melt:4763:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("compil_data_and_slots_fill check gcx"),
				  ("warmelt-genobj.melt")
				  ? ("warmelt-genobj.melt") : __FILE__,
				  (4763) ? (4763) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V16*/ meltfptr[15] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V15*/ meltfptr[13] = /*_.IFELSE___V16*/ meltfptr[15];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:4763:/ clear");
	     /*clear *//*_#IS_A__L5*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V16*/ meltfptr[15] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V15*/ meltfptr[13] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:4764:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L6*/ meltfnum[0] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-genobj.melt:4764:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L6*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L7*/ meltfnum[6] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-genobj.melt:4764:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[11];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L7*/ meltfnum[6];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-genobj.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 4764;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring =
		"compil_data_and_slots_fill start ndat=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.NDAT__V2*/ meltfptr[1];
	      /*^apply.arg */
	      argtab[5].meltbp_cstring = " obj=";
	      /*^apply.arg */
	      argtab[6].meltbp_aptr =
		(melt_ptr_t *) & /*_.OBJ__V3*/ meltfptr[2];
	      /*^apply.arg */
	      argtab[7].meltbp_cstring = " odiscr=";
	      /*^apply.arg */
	      argtab[8].meltbp_aptr =
		(melt_ptr_t *) & /*_.ODISCR__V4*/ meltfptr[3];
	      /*^apply.arg */
	      argtab[9].meltbp_cstring = " irout=";
	      /*^apply.arg */
	      argtab[10].meltbp_aptr =
		(melt_ptr_t *) & /*_.IROUT__V5*/ meltfptr[4];
	      /*_.MELT_DEBUG_FUN__V19*/ meltfptr[18] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[5])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V18*/ meltfptr[17] =
	      /*_.MELT_DEBUG_FUN__V19*/ meltfptr[18];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-genobj.melt:4764:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L7*/ meltfnum[6] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V19*/ meltfptr[18] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V18*/ meltfptr[17] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-genobj.melt:4764:/ quasiblock");


      /*_.PROGN___V20*/ meltfptr[18] = /*_.IF___V18*/ meltfptr[17];;
      /*^compute */
      /*_.IFCPP___V17*/ meltfptr[15] = /*_.PROGN___V20*/ meltfptr[18];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:4764:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L6*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IF___V18*/ meltfptr[17] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V20*/ meltfptr[18] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V17*/ meltfptr[15] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-genobj.melt:4765:/ quasiblock");


    MELT_LOCATION ("warmelt-genobj.melt:4766:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#IS_A__L8*/ meltfnum[6] =
      melt_is_instance_of ((melt_ptr_t) ( /*_.NDAT__V2*/ meltfptr[1]),
			   (melt_ptr_t) (( /*!CLASS_NREP_DATAINSTANCE */
					  meltfrout->tabval[6])));;
    MELT_LOCATION ("warmelt-genobj.melt:4766:/ cond");
    /*cond */ if ( /*_#IS_A__L8*/ meltfnum[6])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  /*^getslot */
	  {
	    melt_ptr_t slot = NULL, obj = NULL;
	    obj = (melt_ptr_t) ( /*_.NDAT__V2*/ meltfptr[1]) /*=obj*/ ;
	    melt_object_get_field (slot, obj, 6, "NINST_PREDEF");
    /*_.NINST_PREDEF__V23*/ meltfptr[22] = slot;
	  };
	  ;
	  /*_.IPREDEF__V22*/ meltfptr[18] =
	    /*_.NINST_PREDEF__V23*/ meltfptr[22];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-genobj.melt:4766:/ clear");
	     /*clear *//*_.NINST_PREDEF__V23*/ meltfptr[22] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

  /*_.IPREDEF__V22*/ meltfptr[18] = NULL;;
      }
    ;
    MELT_LOCATION ("warmelt-genobj.melt:4767:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.IROUT__V5*/ meltfptr[4]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 3, "OBROUT_BODY");
  /*_.IBODYLIS__V24*/ meltfptr[22] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-genobj.melt:4768:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.IROUT__V5*/ meltfptr[4]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 12, "OIROUT_FILL");
  /*_.IFILLLIS__V25*/ meltfptr[24] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-genobj.melt:4769:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & ( /*!konst_8_VALDATA_ */ meltfrout->tabval[8]);
      /*_.LOCVAR__V26*/ meltfptr[25] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!GET_FREE_OBJLOCPTR */ meltfrout->tabval[7])),
		    (melt_ptr_t) ( /*_.GCX__V6*/ meltfptr[5]),
		    (MELTBPARSTR_PTR ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-genobj.melt:4770:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj =
	(melt_ptr_t) (( /*!konst_9_VALUEDATA_ */ meltfrout->
		       tabval[9])) /*=obj*/ ;
      melt_object_get_field (slot, obj, 1, "NAMED_NAME");
  /*_.COMM__V27*/ meltfptr[26] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-genobj.melt:4771:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.NDAT__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 0, "NREP_LOC");
  /*_.NLOC__V28*/ meltfptr[27] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-genobj.melt:4772:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.NDAT__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 3, "NDATA_LOCBIND");
  /*_.DLOCBIND__V29*/ meltfptr[28] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-genobj.melt:4773:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.GCX__V6*/ meltfptr[5]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 1, "GNCX_LOCMAP");
  /*_.LOCMAP__V30*/ meltfptr[29] = slot;
    };
    ;
    /*_.ISTMTLIS__V31*/ meltfptr[30] = /*_.IBODYLIS__V24*/ meltfptr[22];;
    /*^compute */
 /*_#MULTIPLE_LENGTH__L9*/ meltfnum[0] =
      (melt_multiple_length
       ((melt_ptr_t) ( /*_.LOCMAP__V30*/ meltfptr[29])));;
    /*^compute */
 /*_.TUPVAR__V32*/ meltfptr[31] =
      (meltgc_new_multiple
       ((meltobject_ptr_t) (( /*!DISCR_MULTIPLE */ meltfrout->tabval[10])),
	( /*_#MULTIPLE_LENGTH__L9*/ meltfnum[0])));;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:4778:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_LIST__L10*/ meltfnum[9] =
	(melt_magic_discr ((melt_ptr_t) ( /*_.IBODYLIS__V24*/ meltfptr[22]))
	 == MELTOBMAG_LIST);;
      MELT_LOCATION ("warmelt-genobj.melt:4778:/ cond");
      /*cond */ if ( /*_#IS_LIST__L10*/ meltfnum[9])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V34*/ meltfptr[33] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-genobj.melt:4778:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("compil_data_and_slots_fill check ibodylis"), ("warmelt-genobj.melt") ? ("warmelt-genobj.melt") : __FILE__, (4778) ? (4778) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V34*/ meltfptr[33] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V33*/ meltfptr[32] = /*_.IFELSE___V34*/ meltfptr[33];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:4778:/ clear");
	     /*clear *//*_#IS_LIST__L10*/ meltfnum[9] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V34*/ meltfptr[33] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V33*/ meltfptr[32] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:4779:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
      /*^getslot */
      {
	melt_ptr_t slot = NULL, obj = NULL;
	obj = (melt_ptr_t) ( /*_.OBJ__V3*/ meltfptr[2]) /*=obj*/ ;
	melt_object_get_field (slot, obj, 4, "OIE_LOCVAR");
    /*_.OIE_LOCVAR__V36*/ meltfptr[35] = slot;
      };
      ;
   /*_#NULL__L11*/ meltfnum[9] =
	(( /*_.OIE_LOCVAR__V36*/ meltfptr[35]) == NULL);;
      MELT_LOCATION ("warmelt-genobj.melt:4779:/ cond");
      /*cond */ if ( /*_#NULL__L11*/ meltfnum[9])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V37*/ meltfptr[36] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-genobj.melt:4779:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("compil_data_and_slots_fill check fresh obj"), ("warmelt-genobj.melt") ? ("warmelt-genobj.melt") : __FILE__, (4779) ? (4779) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V37*/ meltfptr[36] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V35*/ meltfptr[33] = /*_.IFELSE___V37*/ meltfptr[36];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:4779:/ clear");
	     /*clear *//*_.OIE_LOCVAR__V36*/ meltfptr[35] = 0;
      /*^clear */
	     /*clear *//*_#NULL__L11*/ meltfnum[9] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V37*/ meltfptr[36] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V35*/ meltfptr[33] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-genobj.melt:4780:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OIE_LOCVAR",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.OBJ__V3*/ meltfptr[2])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.OBJ__V3*/ meltfptr[2]), (4),
			  ( /*_.LOCVAR__V26*/ meltfptr[25]), "OIE_LOCVAR");
    ;
    /*^touch */
    meltgc_touch ( /*_.OBJ__V3*/ meltfptr[2]);
    ;
    /*^touchobj */

    melt_dbgtrace_written_object ( /*_.OBJ__V3*/ meltfptr[2], "put-fields");
    ;


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:4781:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_LIST__L12*/ meltfnum[9] =
	(melt_magic_discr ((melt_ptr_t) ( /*_.IFILLLIS__V25*/ meltfptr[24]))
	 == MELTOBMAG_LIST);;
      MELT_LOCATION ("warmelt-genobj.melt:4781:/ cond");
      /*cond */ if ( /*_#IS_LIST__L12*/ meltfnum[9])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V39*/ meltfptr[36] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-genobj.melt:4781:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("compil_data_and_slots_fill check ifilllis"), ("warmelt-genobj.melt") ? ("warmelt-genobj.melt") : __FILE__, (4781) ? (4781) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V39*/ meltfptr[36] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V38*/ meltfptr[35] = /*_.IFELSE___V39*/ meltfptr[36];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:4781:/ clear");
	     /*clear *//*_#IS_LIST__L12*/ meltfnum[9] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V39*/ meltfptr[36] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V38*/ meltfptr[35] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:4782:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_LIST__L13*/ meltfnum[9] =
	(melt_magic_discr ((melt_ptr_t) ( /*_.IBODYLIS__V24*/ meltfptr[22]))
	 == MELTOBMAG_LIST);;
      MELT_LOCATION ("warmelt-genobj.melt:4782:/ cond");
      /*cond */ if ( /*_#IS_LIST__L13*/ meltfnum[9])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V41*/ meltfptr[40] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-genobj.melt:4782:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("compil_data_and_slots_fill check ibodylis"), ("warmelt-genobj.melt") ? ("warmelt-genobj.melt") : __FILE__, (4782) ? (4782) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V41*/ meltfptr[40] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V40*/ meltfptr[36] = /*_.IFELSE___V41*/ meltfptr[40];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:4782:/ clear");
	     /*clear *//*_#IS_LIST__L13*/ meltfnum[9] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V41*/ meltfptr[40] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V40*/ meltfptr[36] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-genobj.melt:4784:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^cond */
    /*cond */ if ( /*_.IPREDEF__V22*/ meltfptr[18])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-genobj.melt:4785:/ quasiblock");


	  MELT_LOCATION ("warmelt-genobj.melt:4788:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
   /*_#IS_A__L14*/ meltfnum[9] =
	    melt_is_instance_of ((melt_ptr_t)
				 ( /*_.IPREDEF__V22*/ meltfptr[18]),
				 (melt_ptr_t) (( /*!CLASS_SYMBOL */
						meltfrout->tabval[11])));;
	  MELT_LOCATION ("warmelt-genobj.melt:4788:/ cond");
	  /*cond */ if ( /*_#IS_A__L14*/ meltfnum[9])	/*then */
	    {
	      /*^cond.then */
	      /*^block */
	      /*anyblock */
	      {

		MELT_LOCATION ("warmelt-genobj.melt:4789:/ checksignal");
		MELT_CHECK_SIGNAL ();
		;
     /*_.MAKE_STRINGCONST__V43*/ meltfptr[42] =
		  (meltgc_new_stringdup
		   ((meltobject_ptr_t)
		    (( /*!DISCR_VERBATIM_STRING */ meltfrout->tabval[13])),
		    ("MELTPREDEFIX(meltpredefinited,")));;
		MELT_LOCATION ("warmelt-genobj.melt:4795:/ getslot");
		{
		  melt_ptr_t slot = NULL, obj = NULL;
		  obj =
		    (melt_ptr_t) ( /*_.IPREDEF__V22*/ meltfptr[18]) /*=obj*/ ;
		  melt_object_get_field (slot, obj, 1, "NAMED_NAME");
      /*_.NAMED_NAME__V44*/ meltfptr[43] = slot;
		};
		;
     /*_.MAKE_STRING__V45*/ meltfptr[44] =
		  (meltgc_new_stringdup
		   ((meltobject_ptr_t)
		    (( /*!DISCR_VERBATIM_STRING */ meltfrout->tabval[13])),
		    melt_string_str ((melt_ptr_t)
				     ( /*_.NAMED_NAME__V44*/
				      meltfptr[43]))));;
		/*^compute */
     /*_.MAKE_STRINGCONST__V46*/ meltfptr[45] =
		  (meltgc_new_stringdup
		   ((meltobject_ptr_t)
		    (( /*!DISCR_VERBATIM_STRING */ meltfrout->tabval[13])),
		    (")")));;
		MELT_LOCATION ("warmelt-genobj.melt:4791:/ blockmultialloc");
		/*multiallocblock */
		{
		  struct meltletrec_1_st
		  {
		    struct MELT_MULTIPLE_STRUCT (3) rtup_0__TUPLREC__x9;
		    long meltletrec_1_endgap;
		  } *meltletrec_1_ptr = 0;
		  meltletrec_1_ptr =
		    (struct meltletrec_1_st *)
		    meltgc_allocate (sizeof (struct meltletrec_1_st), 0);
		  /*^blockmultialloc.initfill */
		  /*inimult rtup_0__TUPLREC__x9 */
 /*_.TUPLREC___V48*/ meltfptr[47] =
		    (melt_ptr_t) & meltletrec_1_ptr->rtup_0__TUPLREC__x9;
		  meltletrec_1_ptr->rtup_0__TUPLREC__x9.discr =
		    (meltobject_ptr_t) (((melt_ptr_t)
					 (MELT_PREDEF (DISCR_MULTIPLE))));
		  meltletrec_1_ptr->rtup_0__TUPLREC__x9.nbval = 3;


		  /*^putuple */
		  /*putupl#35 */
		  melt_assertmsg ("putupl [:4791] #35 checktup",
				  melt_magic_discr ((melt_ptr_t)
						    ( /*_.TUPLREC___V48*/
						     meltfptr[47])) ==
				  MELTOBMAG_MULTIPLE);
		  melt_assertmsg ("putupl [:4791] #35 checkoff",
				  (0 >= 0
				   && 0 <
				   melt_multiple_length ((melt_ptr_t)
							 ( /*_.TUPLREC___V48*/
							  meltfptr[47]))));
		  ((meltmultiple_ptr_t) ( /*_.TUPLREC___V48*/ meltfptr[47]))->
		    tabval[0] =
		    (melt_ptr_t) ( /*_.MAKE_STRINGCONST__V43*/ meltfptr[42]);
		  ;
		  /*^putuple */
		  /*putupl#36 */
		  melt_assertmsg ("putupl [:4791] #36 checktup",
				  melt_magic_discr ((melt_ptr_t)
						    ( /*_.TUPLREC___V48*/
						     meltfptr[47])) ==
				  MELTOBMAG_MULTIPLE);
		  melt_assertmsg ("putupl [:4791] #36 checkoff",
				  (1 >= 0
				   && 1 <
				   melt_multiple_length ((melt_ptr_t)
							 ( /*_.TUPLREC___V48*/
							  meltfptr[47]))));
		  ((meltmultiple_ptr_t) ( /*_.TUPLREC___V48*/ meltfptr[47]))->
		    tabval[1] =
		    (melt_ptr_t) ( /*_.MAKE_STRING__V45*/ meltfptr[44]);
		  ;
		  /*^putuple */
		  /*putupl#37 */
		  melt_assertmsg ("putupl [:4791] #37 checktup",
				  melt_magic_discr ((melt_ptr_t)
						    ( /*_.TUPLREC___V48*/
						     meltfptr[47])) ==
				  MELTOBMAG_MULTIPLE);
		  melt_assertmsg ("putupl [:4791] #37 checkoff",
				  (2 >= 0
				   && 2 <
				   melt_multiple_length ((melt_ptr_t)
							 ( /*_.TUPLREC___V48*/
							  meltfptr[47]))));
		  ((meltmultiple_ptr_t) ( /*_.TUPLREC___V48*/ meltfptr[47]))->
		    tabval[2] =
		    (melt_ptr_t) ( /*_.MAKE_STRINGCONST__V46*/ meltfptr[45]);
		  ;
		  /*^touch */
		  meltgc_touch ( /*_.TUPLREC___V48*/ meltfptr[47]);
		  ;
		  /*_.TUPLE___V47*/ meltfptr[46] =
		    /*_.TUPLREC___V48*/ meltfptr[47];;
		  /*epilog */

		  MELT_LOCATION ("warmelt-genobj.melt:4791:/ clear");
		/*clear *//*_.TUPLREC___V48*/ meltfptr[47] = 0;
		  /*^clear */
		/*clear *//*_.TUPLREC___V48*/ meltfptr[47] = 0;
		}		/*end multiallocblock */
		;
		MELT_LOCATION ("warmelt-genobj.melt:4789:/ quasiblock");


		/*^rawallocobj */
		/*rawallocobj */
		{
		  melt_ptr_t newobj = 0;
		  melt_raw_object_create (newobj,
					  (melt_ptr_t) (( /*!CLASS_OBJEXPV */
							 meltfrout->
							 tabval[12])), (2),
					  "CLASS_OBJEXPV");
      /*_.INST__V50*/ meltfptr[49] =
		    newobj;
		};
		;
		/*^putslot */
		/*putslot */
		melt_assertmsg ("putslot checkobj @OBX_CONT",
				melt_magic_discr ((melt_ptr_t)
						  ( /*_.INST__V50*/
						   meltfptr[49])) ==
				MELTOBMAG_OBJECT);
		melt_putfield_object (( /*_.INST__V50*/ meltfptr[49]), (1),
				      ( /*_.TUPLE___V47*/ meltfptr[46]),
				      "OBX_CONT");
		;
		/*^touchobj */

		melt_dbgtrace_written_object ( /*_.INST__V50*/ meltfptr[49],
					      "newly made instance");
		;
		/*_.INST___V49*/ meltfptr[47] = /*_.INST__V50*/ meltfptr[49];;
		/*^compute */
		/*_.OTESTPREDEF__V42*/ meltfptr[40] =
		  /*_.INST___V49*/ meltfptr[47];;
		/*epilog */

		MELT_LOCATION ("warmelt-genobj.melt:4788:/ clear");
	       /*clear *//*_.MAKE_STRINGCONST__V43*/ meltfptr[42] = 0;
		/*^clear */
	       /*clear *//*_.NAMED_NAME__V44*/ meltfptr[43] = 0;
		/*^clear */
	       /*clear *//*_.MAKE_STRING__V45*/ meltfptr[44] = 0;
		/*^clear */
	       /*clear *//*_.MAKE_STRINGCONST__V46*/ meltfptr[45] = 0;
		/*^clear */
	       /*clear *//*_.TUPLE___V47*/ meltfptr[46] = 0;
		/*^clear */
	       /*clear *//*_.INST___V49*/ meltfptr[47] = 0;
	      }
	      ;
	    }
	  else
	    {			/*^cond.else */

	      /*^block */
	      /*anyblock */
	      {

		MELT_LOCATION ("warmelt-genobj.melt:4800:/ checksignal");
		MELT_CHECK_SIGNAL ();
		;
     /*_#IS_INTEGERBOX__L15*/ meltfnum[14] =
		  (melt_magic_discr
		   ((melt_ptr_t) ( /*_.IPREDEF__V22*/ meltfptr[18])) ==
		   MELTOBMAG_INT);;
		MELT_LOCATION ("warmelt-genobj.melt:4800:/ cond");
		/*cond */ if ( /*_#IS_INTEGERBOX__L15*/ meltfnum[14])	/*then */
		  {
		    /*^cond.then */
		    /*^block */
		    /*anyblock */
		    {

		      MELT_LOCATION
			("warmelt-genobj.melt:4801:/ checksignal");
		      MELT_CHECK_SIGNAL ();
		      ;
       /*_.MAKE_STRINGCONST__V52*/ meltfptr[43] =
			(meltgc_new_stringdup
			 ((meltobject_ptr_t)
			  (( /*!DISCR_VERBATIM_STRING */ meltfrout->
			    tabval[13])), ("predefinited[")));;
		      /*^compute */
       /*_.MAKE_STRINGCONST__V53*/ meltfptr[44] =
			(meltgc_new_stringdup
			 ((meltobject_ptr_t)
			  (( /*!DISCR_VERBATIM_STRING */ meltfrout->
			    tabval[13])), ("]")));;
		      MELT_LOCATION
			("warmelt-genobj.melt:4803:/ blockmultialloc");
		      /*multiallocblock */
		      {
			struct meltletrec_2_st
			{
			  struct MELT_MULTIPLE_STRUCT (3)
			    rtup_0__TUPLREC__x10;
			  long meltletrec_2_endgap;
			} *meltletrec_2_ptr = 0;
			meltletrec_2_ptr =
			  (struct meltletrec_2_st *)
			  meltgc_allocate (sizeof (struct meltletrec_2_st),
					   0);
			/*^blockmultialloc.initfill */
			/*inimult rtup_0__TUPLREC__x10 */
 /*_.TUPLREC___V55*/ meltfptr[46] =
			  (melt_ptr_t) &
			  meltletrec_2_ptr->rtup_0__TUPLREC__x10;
			meltletrec_2_ptr->rtup_0__TUPLREC__x10.discr =
			  (meltobject_ptr_t) (((melt_ptr_t)
					       (MELT_PREDEF
						(DISCR_MULTIPLE))));
			meltletrec_2_ptr->rtup_0__TUPLREC__x10.nbval = 3;


			/*^putuple */
			/*putupl#38 */
			melt_assertmsg ("putupl [:4803] #38 checktup",
					melt_magic_discr ((melt_ptr_t)
							  ( /*_.TUPLREC___V55*/ meltfptr[46])) == MELTOBMAG_MULTIPLE);
			melt_assertmsg ("putupl [:4803] #38 checkoff",
					(0 >= 0
					 && 0 <
					 melt_multiple_length ((melt_ptr_t)
							       ( /*_.TUPLREC___V55*/ meltfptr[46]))));
			((meltmultiple_ptr_t)
			 ( /*_.TUPLREC___V55*/ meltfptr[46]))->tabval[0] =
     (melt_ptr_t) ( /*_.MAKE_STRINGCONST__V52*/ meltfptr[43]);
			;
			/*^putuple */
			/*putupl#39 */
			melt_assertmsg ("putupl [:4803] #39 checktup",
					melt_magic_discr ((melt_ptr_t)
							  ( /*_.TUPLREC___V55*/ meltfptr[46])) == MELTOBMAG_MULTIPLE);
			melt_assertmsg ("putupl [:4803] #39 checkoff",
					(1 >= 0
					 && 1 <
					 melt_multiple_length ((melt_ptr_t)
							       ( /*_.TUPLREC___V55*/ meltfptr[46]))));
			((meltmultiple_ptr_t)
			 ( /*_.TUPLREC___V55*/ meltfptr[46]))->tabval[1] =
     (melt_ptr_t) ( /*_.IPREDEF__V22*/ meltfptr[18]);
			;
			/*^putuple */
			/*putupl#40 */
			melt_assertmsg ("putupl [:4803] #40 checktup",
					melt_magic_discr ((melt_ptr_t)
							  ( /*_.TUPLREC___V55*/ meltfptr[46])) == MELTOBMAG_MULTIPLE);
			melt_assertmsg ("putupl [:4803] #40 checkoff",
					(2 >= 0
					 && 2 <
					 melt_multiple_length ((melt_ptr_t)
							       ( /*_.TUPLREC___V55*/ meltfptr[46]))));
			((meltmultiple_ptr_t)
			 ( /*_.TUPLREC___V55*/ meltfptr[46]))->tabval[2] =
     (melt_ptr_t) ( /*_.MAKE_STRINGCONST__V53*/ meltfptr[44]);
			;
			/*^touch */
			meltgc_touch ( /*_.TUPLREC___V55*/ meltfptr[46]);
			;
			/*_.TUPLE___V54*/ meltfptr[45] =
			  /*_.TUPLREC___V55*/ meltfptr[46];;
			/*epilog */

			MELT_LOCATION ("warmelt-genobj.melt:4803:/ clear");
		  /*clear *//*_.TUPLREC___V55*/ meltfptr[46] = 0;
			/*^clear */
		  /*clear *//*_.TUPLREC___V55*/ meltfptr[46] = 0;
		      }		/*end multiallocblock */
		      ;
		      MELT_LOCATION ("warmelt-genobj.melt:4801:/ quasiblock");


		      /*^rawallocobj */
		      /*rawallocobj */
		      {
			melt_ptr_t newobj = 0;
			melt_raw_object_create (newobj,
						(melt_ptr_t) (( /*!CLASS_OBJEXPV */ meltfrout->tabval[12])), (2), "CLASS_OBJEXPV");
	/*_.INST__V57*/ meltfptr[46] =
			  newobj;
		      };
		      ;
		      /*^putslot */
		      /*putslot */
		      melt_assertmsg ("putslot checkobj @OBX_CONT",
				      melt_magic_discr ((melt_ptr_t)
							( /*_.INST__V57*/
							 meltfptr[46])) ==
				      MELTOBMAG_OBJECT);
		      melt_putfield_object (( /*_.INST__V57*/ meltfptr[46]),
					    (1),
					    ( /*_.TUPLE___V54*/ meltfptr[45]),
					    "OBX_CONT");
		      ;
		      /*^touchobj */

		      melt_dbgtrace_written_object ( /*_.INST__V57*/
						    meltfptr[46],
						    "newly made instance");
		      ;
		      /*_.INST___V56*/ meltfptr[47] =
			/*_.INST__V57*/ meltfptr[46];;
		      /*^compute */
		      /*_.IFELSE___V51*/ meltfptr[42] =
			/*_.INST___V56*/ meltfptr[47];;
		      /*epilog */

		      MELT_LOCATION ("warmelt-genobj.melt:4800:/ clear");
		 /*clear *//*_.MAKE_STRINGCONST__V52*/ meltfptr[43] = 0;
		      /*^clear */
		 /*clear *//*_.MAKE_STRINGCONST__V53*/ meltfptr[44] = 0;
		      /*^clear */
		 /*clear *//*_.TUPLE___V54*/ meltfptr[45] = 0;
		      /*^clear */
		 /*clear *//*_.INST___V56*/ meltfptr[47] = 0;
		    }
		    ;
		  }
		else
		  {		/*^cond.else */

      /*_.IFELSE___V51*/ meltfptr[42] = NULL;;
		  }
		;
		/*^compute */
		/*_.OTESTPREDEF__V42*/ meltfptr[40] =
		  /*_.IFELSE___V51*/ meltfptr[42];;
		/*epilog */

		MELT_LOCATION ("warmelt-genobj.melt:4788:/ clear");
	       /*clear *//*_#IS_INTEGERBOX__L15*/ meltfnum[14] = 0;
		/*^clear */
	       /*clear *//*_.IFELSE___V51*/ meltfptr[42] = 0;
	      }
	      ;
	    }
	  ;
   /*_.INITLIS__V58*/ meltfptr[43] =
	    (meltgc_new_list
	     ((meltobject_ptr_t)
	      (( /*!DISCR_LIST */ meltfrout->tabval[14]))));;
	  MELT_LOCATION ("warmelt-genobj.melt:4813:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^quasiblock */


	  /*^rawallocobj */
	  /*rawallocobj */
	  {
	    melt_ptr_t newobj = 0;
	    melt_raw_object_create (newobj,
				    (melt_ptr_t) (( /*!CLASS_OBJCOMMENTEDBLOCK */ meltfrout->tabval[15])), (4), "CLASS_OBJCOMMENTEDBLOCK");
    /*_.INST__V60*/ meltfptr[45] =
	      newobj;
	  };
	  ;
	  /*^putslot */
	  /*putslot */
	  melt_assertmsg ("putslot checkobj @OBLO_BODYL",
			  melt_magic_discr ((melt_ptr_t)
					    ( /*_.INST__V60*/ meltfptr[45]))
			  == MELTOBMAG_OBJECT);
	  melt_putfield_object (( /*_.INST__V60*/ meltfptr[45]), (1),
				( /*_.INITLIS__V58*/ meltfptr[43]),
				"OBLO_BODYL");
	  ;
	  /*^putslot */
	  /*putslot */
	  melt_assertmsg ("putslot checkobj @OCOMBLO_COMMENT",
			  melt_magic_discr ((melt_ptr_t)
					    ( /*_.INST__V60*/ meltfptr[45]))
			  == MELTOBMAG_OBJECT);
	  melt_putfield_object (( /*_.INST__V60*/ meltfptr[45]), (3),
				(( /*!konst_16 */ meltfrout->tabval[16])),
				"OCOMBLO_COMMENT");
	  ;
	  /*^touchobj */

	  melt_dbgtrace_written_object ( /*_.INST__V60*/ meltfptr[45],
					"newly made instance");
	  ;
	  /*_.OBLOCKPREDEF__V59*/ meltfptr[44] =
	    /*_.INST__V60*/ meltfptr[45];;
	  MELT_LOCATION ("warmelt-genobj.melt:4817:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^quasiblock */


	  /*^rawallocobj */
	  /*rawallocobj */
	  {
	    melt_ptr_t newobj = 0;
	    melt_raw_object_create (newobj,
				    (melt_ptr_t) (( /*!CLASS_OBJCOND */
						   meltfrout->tabval[17])),
				    (4), "CLASS_OBJCOND");
    /*_.INST__V62*/ meltfptr[42] =
	      newobj;
	  };
	  ;
	  /*^putslot */
	  /*putslot */
	  melt_assertmsg ("putslot checkobj @OBI_LOC",
			  melt_magic_discr ((melt_ptr_t)
					    ( /*_.INST__V62*/ meltfptr[42]))
			  == MELTOBMAG_OBJECT);
	  melt_putfield_object (( /*_.INST__V62*/ meltfptr[42]), (0),
				( /*_.NLOC__V28*/ meltfptr[27]), "OBI_LOC");
	  ;
	  /*^putslot */
	  /*putslot */
	  melt_assertmsg ("putslot checkobj @OBCOND_TEST",
			  melt_magic_discr ((melt_ptr_t)
					    ( /*_.INST__V62*/ meltfptr[42]))
			  == MELTOBMAG_OBJECT);
	  melt_putfield_object (( /*_.INST__V62*/ meltfptr[42]), (1),
				( /*_.OTESTPREDEF__V42*/ meltfptr[40]),
				"OBCOND_TEST");
	  ;
	  /*^putslot */
	  /*putslot */
	  melt_assertmsg ("putslot checkobj @OBCOND_THEN",
			  melt_magic_discr ((melt_ptr_t)
					    ( /*_.INST__V62*/ meltfptr[42]))
			  == MELTOBMAG_OBJECT);
	  melt_putfield_object (( /*_.INST__V62*/ meltfptr[42]), (2),
				(( /*nil */ NULL)), "OBCOND_THEN");
	  ;
	  /*^putslot */
	  /*putslot */
	  melt_assertmsg ("putslot checkobj @OBCOND_ELSE",
			  melt_magic_discr ((melt_ptr_t)
					    ( /*_.INST__V62*/ meltfptr[42]))
			  == MELTOBMAG_OBJECT);
	  melt_putfield_object (( /*_.INST__V62*/ meltfptr[42]), (3),
				( /*_.OBLOCKPREDEF__V59*/ meltfptr[44]),
				"OBCOND_ELSE");
	  ;
	  /*^touchobj */

	  melt_dbgtrace_written_object ( /*_.INST__V62*/ meltfptr[42],
					"newly made instance");
	  ;
	  /*_.OCONDPREDEF__V61*/ meltfptr[47] = /*_.INST__V62*/ meltfptr[42];;
	  MELT_LOCATION ("warmelt-genobj.melt:4824:/ compute");
	  /*_.ISTMTLIS__V31*/ meltfptr[30] = /*_.SETQ___V63*/ meltfptr[62] =
	    /*_.INITLIS__V58*/ meltfptr[43];;

	  {
	    MELT_LOCATION ("warmelt-genobj.melt:4825:/ locexp");
	    meltgc_append_list ((melt_ptr_t)
				( /*_.IBODYLIS__V24*/ meltfptr[22]),
				(melt_ptr_t) ( /*_.OCONDPREDEF__V61*/
					      meltfptr[47]));
	  }
	  ;

	  MELT_LOCATION ("warmelt-genobj.melt:4785:/ clear");
	     /*clear *//*_#IS_A__L14*/ meltfnum[9] = 0;
	  /*^clear */
	     /*clear *//*_.OTESTPREDEF__V42*/ meltfptr[40] = 0;
	  /*^clear */
	     /*clear *//*_.INITLIS__V58*/ meltfptr[43] = 0;
	  /*^clear */
	     /*clear *//*_.OBLOCKPREDEF__V59*/ meltfptr[44] = 0;
	  /*^clear */
	     /*clear *//*_.OCONDPREDEF__V61*/ meltfptr[47] = 0;
	  /*^clear */
	     /*clear *//*_.SETQ___V63*/ meltfptr[62] = 0;
	  /*epilog */
	}
	;
      }				/*noelse */
    ;
    MELT_LOCATION ("warmelt-genobj.melt:4829:/ quasiblock");


    /*^newclosure */
		 /*newclosure *//*_.LAMBDA___V65*/ meltfptr[43] =
      (melt_ptr_t)
      meltgc_new_closure ((meltobject_ptr_t)
			  (((melt_ptr_t) (MELT_PREDEF (DISCR_CLOSURE)))),
			  (meltroutine_ptr_t) (( /*!konst_23 */ meltfrout->
						tabval[23])), (4));
    ;
    /*^putclosedv */
    /*putclosv */
    melt_assertmsg ("putclosv checkclo",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.LAMBDA___V65*/ meltfptr[43])) ==
		    MELTOBMAG_CLOSURE);
    melt_assertmsg ("putclosv checkoff", 0 >= 0
		    && 0 <
		    melt_closure_size ((melt_ptr_t)
				       ( /*_.LAMBDA___V65*/ meltfptr[43])));
    ((meltclosure_ptr_t) /*_.LAMBDA___V65*/ meltfptr[43])->tabval[0] =
      (melt_ptr_t) ( /*_.GCX__V6*/ meltfptr[5]);
    ;
    /*^putclosedv */
    /*putclosv */
    melt_assertmsg ("putclosv checkclo",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.LAMBDA___V65*/ meltfptr[43])) ==
		    MELTOBMAG_CLOSURE);
    melt_assertmsg ("putclosv checkoff", 1 >= 0
		    && 1 <
		    melt_closure_size ((melt_ptr_t)
				       ( /*_.LAMBDA___V65*/ meltfptr[43])));
    ((meltclosure_ptr_t) /*_.LAMBDA___V65*/ meltfptr[43])->tabval[1] =
      (melt_ptr_t) ( /*_.LOCMAP__V30*/ meltfptr[29]);
    ;
    /*^putclosedv */
    /*putclosv */
    melt_assertmsg ("putclosv checkclo",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.LAMBDA___V65*/ meltfptr[43])) ==
		    MELTOBMAG_CLOSURE);
    melt_assertmsg ("putclosv checkoff", 2 >= 0
		    && 2 <
		    melt_closure_size ((melt_ptr_t)
				       ( /*_.LAMBDA___V65*/ meltfptr[43])));
    ((meltclosure_ptr_t) /*_.LAMBDA___V65*/ meltfptr[43])->tabval[2] =
      (melt_ptr_t) ( /*_.ISTMTLIS__V31*/ meltfptr[30]);
    ;
    /*^putclosedv */
    /*putclosv */
    melt_assertmsg ("putclosv checkclo",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.LAMBDA___V65*/ meltfptr[43])) ==
		    MELTOBMAG_CLOSURE);
    melt_assertmsg ("putclosv checkoff", 3 >= 0
		    && 3 <
		    melt_closure_size ((melt_ptr_t)
				       ( /*_.LAMBDA___V65*/ meltfptr[43])));
    ((meltclosure_ptr_t) /*_.LAMBDA___V65*/ meltfptr[43])->tabval[3] =
      (melt_ptr_t) ( /*_.TUPVAR__V32*/ meltfptr[31]);
    ;
    /*_.LAMBDA___V64*/ meltfptr[40] = /*_.LAMBDA___V65*/ meltfptr[43];;
    MELT_LOCATION ("warmelt-genobj.melt:4827:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & /*_.LAMBDA___V64*/ meltfptr[40];
      /*_.MULTIPLE_EVERY__V66*/ meltfptr[44] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!MULTIPLE_EVERY */ meltfrout->tabval[18])),
		    (melt_ptr_t) ( /*_.DLOCBIND__V29*/ meltfptr[28]),
		    (MELTBPARSTR_PTR ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-genobj.melt:4846:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.NDAT__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 8, "NINST_SLOTS");
  /*_.NINST_SLOTS__V67*/ meltfptr[47] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-genobj.melt:4847:/ quasiblock");


    /*^newclosure */
		 /*newclosure *//*_.LAMBDA___V69*/ meltfptr[68] =
      (melt_ptr_t)
      meltgc_new_closure ((meltobject_ptr_t)
			  (((melt_ptr_t) (MELT_PREDEF (DISCR_CLOSURE)))),
			  (meltroutine_ptr_t) (( /*!konst_28 */ meltfrout->
						tabval[28])), (4));
    ;
    /*^putclosedv */
    /*putclosv */
    melt_assertmsg ("putclosv checkclo",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.LAMBDA___V69*/ meltfptr[68])) ==
		    MELTOBMAG_CLOSURE);
    melt_assertmsg ("putclosv checkoff", 0 >= 0
		    && 0 <
		    melt_closure_size ((melt_ptr_t)
				       ( /*_.LAMBDA___V69*/ meltfptr[68])));
    ((meltclosure_ptr_t) /*_.LAMBDA___V69*/ meltfptr[68])->tabval[0] =
      (melt_ptr_t) ( /*_.GCX__V6*/ meltfptr[5]);
    ;
    /*^putclosedv */
    /*putclosv */
    melt_assertmsg ("putclosv checkclo",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.LAMBDA___V69*/ meltfptr[68])) ==
		    MELTOBMAG_CLOSURE);
    melt_assertmsg ("putclosv checkoff", 1 >= 0
		    && 1 <
		    melt_closure_size ((melt_ptr_t)
				       ( /*_.LAMBDA___V69*/ meltfptr[68])));
    ((meltclosure_ptr_t) /*_.LAMBDA___V69*/ meltfptr[68])->tabval[1] =
      (melt_ptr_t) ( /*_.NLOC__V28*/ meltfptr[27]);
    ;
    /*^putclosedv */
    /*putclosv */
    melt_assertmsg ("putclosv checkclo",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.LAMBDA___V69*/ meltfptr[68])) ==
		    MELTOBMAG_CLOSURE);
    melt_assertmsg ("putclosv checkoff", 2 >= 0
		    && 2 <
		    melt_closure_size ((melt_ptr_t)
				       ( /*_.LAMBDA___V69*/ meltfptr[68])));
    ((meltclosure_ptr_t) /*_.LAMBDA___V69*/ meltfptr[68])->tabval[2] =
      (melt_ptr_t) ( /*_.OBJ__V3*/ meltfptr[2]);
    ;
    /*^putclosedv */
    /*putclosv */
    melt_assertmsg ("putclosv checkclo",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.LAMBDA___V69*/ meltfptr[68])) ==
		    MELTOBMAG_CLOSURE);
    melt_assertmsg ("putclosv checkoff", 3 >= 0
		    && 3 <
		    melt_closure_size ((melt_ptr_t)
				       ( /*_.LAMBDA___V69*/ meltfptr[68])));
    ((meltclosure_ptr_t) /*_.LAMBDA___V69*/ meltfptr[68])->tabval[3] =
      (melt_ptr_t) ( /*_.ISTMTLIS__V31*/ meltfptr[30]);
    ;
    /*_.LAMBDA___V68*/ meltfptr[62] = /*_.LAMBDA___V69*/ meltfptr[68];;
    MELT_LOCATION ("warmelt-genobj.melt:4845:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & /*_.LAMBDA___V68*/ meltfptr[62];
      /*_.MULTIPLE_EVERY__V70*/ meltfptr[69] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!MULTIPLE_EVERY */ meltfrout->tabval[18])),
		    (melt_ptr_t) ( /*_.NINST_SLOTS__V67*/ meltfptr[47]),
		    (MELTBPARSTR_PTR ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-genobj.melt:4867:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    MELT_LOCATION ("warmelt-genobj.melt:4868:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^cond */
    /*cond */ if ( /*_.NLOC__V28*/ meltfptr[27])	/*then */
      {
	/*^cond.then */
	/*_.IFELSE___V71*/ meltfptr[70] = /*_.NLOC__V28*/ meltfptr[27];;
      }
    else
      {
	MELT_LOCATION ("warmelt-genobj.melt:4868:/ cond.else");

	/*^block */
	/*anyblock */
	{

	  /*^getslot */
	  {
	    melt_ptr_t slot = NULL, obj = NULL;
	    obj = (melt_ptr_t) ( /*_.NDAT__V2*/ meltfptr[1]) /*=obj*/ ;
	    melt_object_get_field (slot, obj, 0, "NREP_LOC");
    /*_.NREP_LOC__V72*/ meltfptr[71] = slot;
	  };
	  ;
	  /*_.IFELSE___V71*/ meltfptr[70] = /*_.NREP_LOC__V72*/ meltfptr[71];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-genobj.melt:4868:/ clear");
	     /*clear *//*_.NREP_LOC__V72*/ meltfptr[71] = 0;
	}
	;
      }
    ;
    MELT_LOCATION ("warmelt-genobj.melt:4867:/ quasiblock");


    /*^rawallocobj */
    /*rawallocobj */
    {
      melt_ptr_t newobj = 0;
      melt_raw_object_create (newobj,
			      (melt_ptr_t) (( /*!CLASS_OBJTOUCH */ meltfrout->
					     tabval[29])), (3),
			      "CLASS_OBJTOUCH");
  /*_.INST__V74*/ meltfptr[73] =
	newobj;
    };
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OBI_LOC",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V74*/ meltfptr[73])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V74*/ meltfptr[73]), (0),
			  ( /*_.IFELSE___V71*/ meltfptr[70]), "OBI_LOC");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OTOUCH_COMMENT",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V74*/ meltfptr[73])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V74*/ meltfptr[73]), (2),
			  ( /*_.COMM__V27*/ meltfptr[26]), "OTOUCH_COMMENT");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OTOUCH_VAL",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V74*/ meltfptr[73])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V74*/ meltfptr[73]), (1),
			  ( /*_.OBJ__V3*/ meltfptr[2]), "OTOUCH_VAL");
    ;
    /*^touchobj */

    melt_dbgtrace_written_object ( /*_.INST__V74*/ meltfptr[73],
				  "newly made instance");
    ;
    /*_.INST___V73*/ meltfptr[71] = /*_.INST__V74*/ meltfptr[73];;

    {
      MELT_LOCATION ("warmelt-genobj.melt:4866:/ locexp");
      meltgc_append_list ((melt_ptr_t) ( /*_.ISTMTLIS__V31*/ meltfptr[30]),
			  (melt_ptr_t) ( /*_.INST___V73*/ meltfptr[71]));
    }
    ;
    MELT_LOCATION ("warmelt-genobj.melt:4873:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    MELT_LOCATION ("warmelt-genobj.melt:4874:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^cond */
    /*cond */ if ( /*_.NLOC__V28*/ meltfptr[27])	/*then */
      {
	/*^cond.then */
	/*_.IFELSE___V75*/ meltfptr[74] = /*_.NLOC__V28*/ meltfptr[27];;
      }
    else
      {
	MELT_LOCATION ("warmelt-genobj.melt:4874:/ cond.else");

	/*^block */
	/*anyblock */
	{

	  /*^getslot */
	  {
	    melt_ptr_t slot = NULL, obj = NULL;
	    obj = (melt_ptr_t) ( /*_.NDAT__V2*/ meltfptr[1]) /*=obj*/ ;
	    melt_object_get_field (slot, obj, 0, "NREP_LOC");
    /*_.NREP_LOC__V76*/ meltfptr[75] = slot;
	  };
	  ;
	  /*_.IFELSE___V75*/ meltfptr[74] = /*_.NREP_LOC__V76*/ meltfptr[75];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-genobj.melt:4874:/ clear");
	     /*clear *//*_.NREP_LOC__V76*/ meltfptr[75] = 0;
	}
	;
      }
    ;
    MELT_LOCATION ("warmelt-genobj.melt:4873:/ quasiblock");


    /*^rawallocobj */
    /*rawallocobj */
    {
      melt_ptr_t newobj = 0;
      melt_raw_object_create (newobj,
			      (melt_ptr_t) (( /*!CLASS_OBJDBGTRACEWRITEOBJ */
					     meltfrout->tabval[30])), (3),
			      "CLASS_OBJDBGTRACEWRITEOBJ");
  /*_.INST__V78*/ meltfptr[77] =
	newobj;
    };
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OBI_LOC",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V78*/ meltfptr[77])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V78*/ meltfptr[77]), (0),
			  ( /*_.IFELSE___V75*/ meltfptr[74]), "OBI_LOC");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OBDTW_WRITTENOBJ",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V78*/ meltfptr[77])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V78*/ meltfptr[77]), (1),
			  ( /*_.OBJ__V3*/ meltfptr[2]), "OBDTW_WRITTENOBJ");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OBDTW_MESSAGE",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V78*/ meltfptr[77])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V78*/ meltfptr[77]), (2),
			  (( /*!konst_31 */ meltfrout->tabval[31])),
			  "OBDTW_MESSAGE");
    ;
    /*^touchobj */

    melt_dbgtrace_written_object ( /*_.INST__V78*/ meltfptr[77],
				  "newly made instance");
    ;
    /*_.INST___V77*/ meltfptr[75] = /*_.INST__V78*/ meltfptr[77];;

    {
      MELT_LOCATION ("warmelt-genobj.melt:4872:/ locexp");
      meltgc_append_list ((melt_ptr_t) ( /*_.ISTMTLIS__V31*/ meltfptr[30]),
			  (melt_ptr_t) ( /*_.INST___V77*/ meltfptr[75]));
    }
    ;
    MELT_LOCATION ("warmelt-genobj.melt:4880:/ quasiblock");


    /*^newclosure */
		 /*newclosure *//*_.LAMBDA___V80*/ meltfptr[79] =
      (melt_ptr_t)
      meltgc_new_closure ((meltobject_ptr_t)
			  (((melt_ptr_t) (MELT_PREDEF (DISCR_CLOSURE)))),
			  (meltroutine_ptr_t) (( /*!konst_33 */ meltfrout->
						tabval[33])), (2));
    ;
    /*^putclosedv */
    /*putclosv */
    melt_assertmsg ("putclosv checkclo",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.LAMBDA___V80*/ meltfptr[79])) ==
		    MELTOBMAG_CLOSURE);
    melt_assertmsg ("putclosv checkoff", 0 >= 0
		    && 0 <
		    melt_closure_size ((melt_ptr_t)
				       ( /*_.LAMBDA___V80*/ meltfptr[79])));
    ((meltclosure_ptr_t) /*_.LAMBDA___V80*/ meltfptr[79])->tabval[0] =
      (melt_ptr_t) ( /*_.IBODYLIS__V24*/ meltfptr[22]);
    ;
    /*^putclosedv */
    /*putclosv */
    melt_assertmsg ("putclosv checkclo",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.LAMBDA___V80*/ meltfptr[79])) ==
		    MELTOBMAG_CLOSURE);
    melt_assertmsg ("putclosv checkoff", 1 >= 0
		    && 1 <
		    melt_closure_size ((melt_ptr_t)
				       ( /*_.LAMBDA___V80*/ meltfptr[79])));
    ((meltclosure_ptr_t) /*_.LAMBDA___V80*/ meltfptr[79])->tabval[1] =
      (melt_ptr_t) ( /*_.NLOC__V28*/ meltfptr[27]);
    ;
    /*_.LAMBDA___V79*/ meltfptr[78] = /*_.LAMBDA___V80*/ meltfptr[79];;
    MELT_LOCATION ("warmelt-genobj.melt:4878:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & /*_.LAMBDA___V79*/ meltfptr[78];
      /*_.MULTIPLE_EVERY__V81*/ meltfptr[80] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!MULTIPLE_EVERY */ meltfrout->tabval[18])),
		    (melt_ptr_t) ( /*_.TUPVAR__V32*/ meltfptr[31]),
		    (MELTBPARSTR_PTR ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-genobj.melt:4889:/ locexp");
      meltgc_append_list ((melt_ptr_t) ( /*_.IBODYLIS__V24*/ meltfptr[22]),
			  (melt_ptr_t) ( /*_.OBJ__V3*/ meltfptr[2]));
    }
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:4891:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L16*/ meltfnum[14] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-genobj.melt:4891:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L16*/ meltfnum[14])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L17*/ meltfnum[9] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-genobj.melt:4891:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long =
		/*_#THE_MELTCALLCOUNT__L17*/ meltfnum[9];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-genobj.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 4891;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring =
		"compil_data_and_slots_fill final dlocbind=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.DLOCBIND__V29*/ meltfptr[28];
	      /*_.MELT_DEBUG_FUN__V84*/ meltfptr[83] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[5])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V83*/ meltfptr[82] =
	      /*_.MELT_DEBUG_FUN__V84*/ meltfptr[83];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-genobj.melt:4891:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L17*/ meltfnum[9] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V84*/ meltfptr[83] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V83*/ meltfptr[82] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-genobj.melt:4891:/ quasiblock");


      /*_.PROGN___V85*/ meltfptr[83] = /*_.IF___V83*/ meltfptr[82];;
      /*^compute */
      /*_.IFCPP___V82*/ meltfptr[81] = /*_.PROGN___V85*/ meltfptr[83];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:4891:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L16*/ meltfnum[14] = 0;
      /*^clear */
	     /*clear *//*_.IF___V83*/ meltfptr[82] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V85*/ meltfptr[83] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V82*/ meltfptr[81] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-genobj.melt:4895:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.DLOCBIND__V29*/ meltfptr[28];;

    {
      MELT_LOCATION ("warmelt-genobj.melt:4895:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*_.LET___V21*/ meltfptr[17] = /*_.RETURN___V86*/ meltfptr[82];;

    MELT_LOCATION ("warmelt-genobj.melt:4765:/ clear");
	   /*clear *//*_#IS_A__L8*/ meltfnum[6] = 0;
    /*^clear */
	   /*clear *//*_.IPREDEF__V22*/ meltfptr[18] = 0;
    /*^clear */
	   /*clear *//*_.IBODYLIS__V24*/ meltfptr[22] = 0;
    /*^clear */
	   /*clear *//*_.IFILLLIS__V25*/ meltfptr[24] = 0;
    /*^clear */
	   /*clear *//*_.LOCVAR__V26*/ meltfptr[25] = 0;
    /*^clear */
	   /*clear *//*_.COMM__V27*/ meltfptr[26] = 0;
    /*^clear */
	   /*clear *//*_.NLOC__V28*/ meltfptr[27] = 0;
    /*^clear */
	   /*clear *//*_.DLOCBIND__V29*/ meltfptr[28] = 0;
    /*^clear */
	   /*clear *//*_.LOCMAP__V30*/ meltfptr[29] = 0;
    /*^clear */
	   /*clear *//*_.ISTMTLIS__V31*/ meltfptr[30] = 0;
    /*^clear */
	   /*clear *//*_#MULTIPLE_LENGTH__L9*/ meltfnum[0] = 0;
    /*^clear */
	   /*clear *//*_.TUPVAR__V32*/ meltfptr[31] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V33*/ meltfptr[32] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V35*/ meltfptr[33] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V38*/ meltfptr[35] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V40*/ meltfptr[36] = 0;
    /*^clear */
	   /*clear *//*_.LAMBDA___V64*/ meltfptr[40] = 0;
    /*^clear */
	   /*clear *//*_.MULTIPLE_EVERY__V66*/ meltfptr[44] = 0;
    /*^clear */
	   /*clear *//*_.NINST_SLOTS__V67*/ meltfptr[47] = 0;
    /*^clear */
	   /*clear *//*_.LAMBDA___V68*/ meltfptr[62] = 0;
    /*^clear */
	   /*clear *//*_.MULTIPLE_EVERY__V70*/ meltfptr[69] = 0;
    /*^clear */
	   /*clear *//*_.IFELSE___V71*/ meltfptr[70] = 0;
    /*^clear */
	   /*clear *//*_.INST___V73*/ meltfptr[71] = 0;
    /*^clear */
	   /*clear *//*_.IFELSE___V75*/ meltfptr[74] = 0;
    /*^clear */
	   /*clear *//*_.INST___V77*/ meltfptr[75] = 0;
    /*^clear */
	   /*clear *//*_.LAMBDA___V79*/ meltfptr[78] = 0;
    /*^clear */
	   /*clear *//*_.MULTIPLE_EVERY__V81*/ meltfptr[80] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V82*/ meltfptr[81] = 0;
    /*^clear */
	   /*clear *//*_.RETURN___V86*/ meltfptr[82] = 0;
    MELT_LOCATION ("warmelt-genobj.melt:4897:/ quasiblock");


 /*_.RETVAL___V1*/ meltfptr[0] = NULL;;

    {
      MELT_LOCATION ("warmelt-genobj.melt:4897:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    MELT_LOCATION ("warmelt-genobj.melt:4758:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.RETURN___V87*/ meltfptr[83];;

    {
      MELT_LOCATION ("warmelt-genobj.melt:4758:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V7*/ meltfptr[6] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V9*/ meltfptr[7] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V11*/ meltfptr[9] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V13*/ meltfptr[11] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V15*/ meltfptr[13] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V17*/ meltfptr[15] = 0;
    /*^clear */
	   /*clear *//*_.LET___V21*/ meltfptr[17] = 0;
    /*^clear */
	   /*clear *//*_.RETURN___V87*/ meltfptr[83] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("COMPIL_DATA_AND_SLOTS_FILL", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_139_warmelt_genobj_COMPIL_DATA_AND_SLOTS_FILL_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_139_warmelt_genobj_COMPIL_DATA_AND_SLOTS_FILL */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_140_warmelt_genobj_LAMBDA___45__ (meltclosure_ptr_t meltclosp_,
					   melt_ptr_t meltfirstargp_,
					   const melt_argdescr_cell_t
					   meltxargdescr_[],
					   union meltparam_un * meltxargtab_,
					   const melt_argdescr_cell_t
					   meltxresdescr_[],
					   union meltparam_un * meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_140_warmelt_genobj_LAMBDA___45___melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_140_warmelt_genobj_LAMBDA___45___st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 11
    melt_ptr_t mcfr_varptr[11];
#define MELTFRAM_NBVARNUM 2
    long mcfr_varnum[2];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_140_warmelt_genobj_LAMBDA___45__ is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_140_warmelt_genobj_LAMBDA___45___st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 11; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_140_warmelt_genobj_LAMBDA___45__ nbval 11*/
  meltfram__.mcfr_nbvar = 11 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("LAMBDA_", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-genobj.melt:4829:/ getarg");
 /*_.LBIND__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_LONG)
    goto lab_endgetargs;
 /*_#BINDRK__L1*/ meltfnum[0] = meltxargtab_[0].meltbp_long;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:4830:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L2*/ meltfnum[1] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.LBIND__V2*/ meltfptr[1]),
			     (melt_ptr_t) (( /*!CLASS_NORMAL_LET_BINDING */
					    meltfrout->tabval[0])));;
      MELT_LOCATION ("warmelt-genobj.melt:4830:/ cond");
      /*cond */ if ( /*_#IS_A__L2*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V4*/ meltfptr[3] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-genobj.melt:4830:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("compil_data_and_slots_fill check lbind"),
				  ("warmelt-genobj.melt")
				  ? ("warmelt-genobj.melt") : __FILE__,
				  (4830) ? (4830) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V4*/ meltfptr[3] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V3*/ meltfptr[2] = /*_.IFELSE___V4*/ meltfptr[3];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:4830:/ clear");
	     /*clear *//*_#IS_A__L2*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V4*/ meltfptr[3] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V3*/ meltfptr[2] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-genobj.melt:4831:/ quasiblock");


    /*^getslot */
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.LBIND__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 0, "BINDER");
  /*_.BDER__V5*/ meltfptr[3] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-genobj.melt:4832:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.LBIND__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 1, "LETBIND_TYPE");
  /*_.CTY__V6*/ meltfptr[5] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-genobj.melt:4833:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.LBIND__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 2, "LETBIND_EXPR");
  /*_.NEXP__V7*/ meltfptr[6] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-genobj.melt:4834:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[2];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.BDER__V5*/ meltfptr[3];
      /*^apply.arg */
      argtab[1].meltbp_aptr = (melt_ptr_t *) & /*_.CTY__V6*/ meltfptr[5];
      /*_.OBVA__V8*/ meltfptr[7] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!GET_FREE_OBJLOCTYPED */ meltfrout->tabval[1])),
		    (melt_ptr_t) (( /*~GCX */ meltfclos->tabval[0])),
		    (MELTBPARSTR_PTR MELTBPARSTR_PTR ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-genobj.melt:4835:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^msend */
    /*msend */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^ojbmsend.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & ( /*~GCX */ meltfclos->tabval[0]);
      /*_.OBNX__V9*/ meltfptr[8] =
	meltgc_send ((melt_ptr_t) ( /*_.NEXP__V7*/ meltfptr[6]),
		     (melt_ptr_t) (( /*!COMPILE_OBJ */ meltfrout->tabval[2])),
		     (MELTBPARSTR_PTR ""), argtab, "",
		     (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-genobj.melt:4837:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^cond */
    /*cond */ if ( /*_.OBVA__V8*/ meltfptr[7])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{


	  {
	    /*^locexp */
	    meltgc_put_mapobjects ((meltmapobjects_ptr_t)
				   (( /*~LOCMAP */ meltfclos->tabval[1])),
				   (meltobject_ptr_t) ( /*_.LBIND__V2*/
						       meltfptr[1]),
				   (melt_ptr_t) ( /*_.OBVA__V8*/
						 meltfptr[7]));
	  }
	  ;
	  /*epilog */
	}
	;
      }				/*noelse */
    ;
    MELT_LOCATION ("warmelt-genobj.melt:4838:/ quasiblock");


    /*^checksignal */
    MELT_CHECK_SIGNAL ();
    ;
    /*^cond */
    /*cond */ if ( /*_.OBVA__V8*/ meltfptr[7])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  /*^checksignal */
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^msend */
	  /*msend */
	  {
	    union meltparam_un argtab[1];
	    memset (&argtab, 0, sizeof (argtab));
	    /*^ojbmsend.arg */
	    argtab[0].meltbp_aptr =
	      (melt_ptr_t *) & /*_.OBVA__V8*/ meltfptr[7];
	    /*_.PUT_OBJDEST__V11*/ meltfptr[10] =
	      meltgc_send ((melt_ptr_t) ( /*_.OBNX__V9*/ meltfptr[8]),
			   (melt_ptr_t) (( /*!PUT_OBJDEST */ meltfrout->
					  tabval[3])), (MELTBPARSTR_PTR ""),
			   argtab, "", (union meltparam_un *) 0);
	  }
	  ;
	  /*_.OBMY__V10*/ meltfptr[9] = /*_.PUT_OBJDEST__V11*/ meltfptr[10];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-genobj.melt:4838:/ clear");
	     /*clear *//*_.PUT_OBJDEST__V11*/ meltfptr[10] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

	/*_.OBMY__V10*/ meltfptr[9] = /*_.OBNX__V9*/ meltfptr[8];;
      }
    ;

    {
      MELT_LOCATION ("warmelt-genobj.melt:4840:/ locexp");
      meltgc_append_list ((melt_ptr_t)
			  (( /*~ISTMTLIS */ meltfclos->tabval[2])),
			  (melt_ptr_t) ( /*_.OBMY__V10*/ meltfptr[9]));
    }
    ;

    MELT_LOCATION ("warmelt-genobj.melt:4838:/ clear");
	   /*clear *//*_.OBMY__V10*/ meltfptr[9] = 0;
    MELT_LOCATION ("warmelt-genobj.melt:4842:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^cond */
    /*cond */ if ( /*_.OBVA__V8*/ meltfptr[7])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{


	  {
	    /*^locexp */
	    meltgc_multiple_put_nth ((melt_ptr_t)
				     (( /*~TUPVAR */ meltfclos->tabval[3])),
				     ( /*_#BINDRK__L1*/ meltfnum[0]),
				     (melt_ptr_t) ( /*_.OBVA__V8*/
						   meltfptr[7]));
	  }
	  ;
	  /*epilog */
	}
	;
      }				/*noelse */
    ;

    MELT_LOCATION ("warmelt-genobj.melt:4831:/ clear");
	   /*clear *//*_.BDER__V5*/ meltfptr[3] = 0;
    /*^clear */
	   /*clear *//*_.CTY__V6*/ meltfptr[5] = 0;
    /*^clear */
	   /*clear *//*_.NEXP__V7*/ meltfptr[6] = 0;
    /*^clear */
	   /*clear *//*_.OBVA__V8*/ meltfptr[7] = 0;
    /*^clear */
	   /*clear *//*_.OBNX__V9*/ meltfptr[8] = 0;
    MELT_LOCATION ("warmelt-genobj.melt:4829:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V3*/ meltfptr[2] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("LAMBDA_", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_140_warmelt_genobj_LAMBDA___45___melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*noretval */ NULL);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_140_warmelt_genobj_LAMBDA___45__ */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_141_warmelt_genobj_LAMBDA___46__ (meltclosure_ptr_t meltclosp_,
					   melt_ptr_t meltfirstargp_,
					   const melt_argdescr_cell_t
					   meltxargdescr_[],
					   union meltparam_un * meltxargtab_,
					   const melt_argdescr_cell_t
					   meltxresdescr_[],
					   union meltparam_un * meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_141_warmelt_genobj_LAMBDA___46___melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_141_warmelt_genobj_LAMBDA___46___st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 8
    melt_ptr_t mcfr_varptr[8];
#define MELTFRAM_NBVARNUM 3
    long mcfr_varnum[3];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_141_warmelt_genobj_LAMBDA___46__ is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_141_warmelt_genobj_LAMBDA___46___st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 8; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_141_warmelt_genobj_LAMBDA___46__ nbval 8*/
  meltfram__.mcfr_nbvar = 8 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("LAMBDA_", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-genobj.melt:4847:/ getarg");
 /*_.SLOVAL__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_LONG)
    goto lab_endgetargs;
 /*_#SLORK__L1*/ meltfnum[0] = meltxargtab_[0].meltbp_long;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {

    MELT_LOCATION ("warmelt-genobj.melt:4848:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^cond */
    /*cond */ if ( /*_.SLOVAL__V2*/ meltfptr[1])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-genobj.melt:4850:/ quasiblock");


	  /*^checksignal */
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^msend */
	  /*msend */
	  {
	    union meltparam_un argtab[1];
	    memset (&argtab, 0, sizeof (argtab));
	    /*^ojbmsend.arg */
	    argtab[0].meltbp_aptr =
	      (melt_ptr_t *) & ( /*~GCX */ meltfclos->tabval[0]);
	    /*_.SLOBJ__V3*/ meltfptr[2] =
	      meltgc_send ((melt_ptr_t) ( /*_.SLOVAL__V2*/ meltfptr[1]),
			   (melt_ptr_t) (( /*!COMPILE_OBJ */ meltfrout->
					  tabval[0])), (MELTBPARSTR_PTR ""),
			   argtab, "", (union meltparam_un *) 0);
	  }
	  ;

#if MELT_HAVE_DEBUG
	  MELT_LOCATION ("warmelt-genobj.melt:4852:/ cppif.then");
	  /*^block */
	  /*anyblock */
	  {

	    /*^checksignal */
	    MELT_CHECK_SIGNAL ();
	    ;
     /*_#IS_A__L2*/ meltfnum[1] =
	      melt_is_instance_of ((melt_ptr_t)
				   ( /*_.SLOBJ__V3*/ meltfptr[2]),
				   (melt_ptr_t) (( /*!CLASS_NREP */
						  meltfrout->tabval[1])));;
	    /*^compute */
     /*_#NOT__L3*/ meltfnum[2] =
	      (!( /*_#IS_A__L2*/ meltfnum[1]));;
	    MELT_LOCATION ("warmelt-genobj.melt:4852:/ cond");
	    /*cond */ if ( /*_#NOT__L3*/ meltfnum[2])	/*then */
	      {
		/*^cond.then */
		/*_.IFELSE___V5*/ meltfptr[4] = ( /*nil */ NULL);;
	      }
	    else
	      {
		MELT_LOCATION ("warmelt-genobj.melt:4852:/ cond.else");

		/*^block */
		/*anyblock */
		{




		  {
		    /*^locexp */
		    melt_assert_failed (("compil_data_and_slots_fill check slobj not nrep"), ("warmelt-genobj.melt") ? ("warmelt-genobj.melt") : __FILE__, (4852) ? (4852) : __LINE__, __FUNCTION__);
		    ;
		  }
		  ;
		 /*clear *//*_.IFELSE___V5*/ meltfptr[4] = 0;
		  /*epilog */
		}
		;
	      }
	    ;
	    /*^compute */
	    /*_.IFCPP___V4*/ meltfptr[3] = /*_.IFELSE___V5*/ meltfptr[4];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-genobj.melt:4852:/ clear");
	       /*clear *//*_#IS_A__L2*/ meltfnum[1] = 0;
	    /*^clear */
	       /*clear *//*_#NOT__L3*/ meltfnum[2] = 0;
	    /*^clear */
	       /*clear *//*_.IFELSE___V5*/ meltfptr[4] = 0;
	  }

#else /*MELT_HAVE_DEBUG */
	  /*^cppif.else */
	  /*_.IFCPP___V4*/ meltfptr[3] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
	  ;
	  MELT_LOCATION ("warmelt-genobj.melt:4853:/ quasiblock");


   /*_.OOFF__V6*/ meltfptr[4] =
	    (meltgc_new_int
	     ((meltobject_ptr_t)
	      (( /*!DISCR_INTEGER */ meltfrout->tabval[2])),
	      ( /*_#SLORK__L1*/ meltfnum[0])));;
	  MELT_LOCATION ("warmelt-genobj.melt:4856:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^quasiblock */


	  /*^rawallocobj */
	  /*rawallocobj */
	  {
	    melt_ptr_t newobj = 0;
	    melt_raw_object_create (newobj,
				    (melt_ptr_t) (( /*!CLASS_OBJPUTSLOT */
						   meltfrout->tabval[3])),
				    (5), "CLASS_OBJPUTSLOT");
    /*_.INST__V8*/ meltfptr[7] =
	      newobj;
	  };
	  ;
	  /*^putslot */
	  /*putslot */
	  melt_assertmsg ("putslot checkobj @OBI_LOC",
			  melt_magic_discr ((melt_ptr_t)
					    ( /*_.INST__V8*/ meltfptr[7])) ==
			  MELTOBMAG_OBJECT);
	  melt_putfield_object (( /*_.INST__V8*/ meltfptr[7]), (0),
				(( /*~NLOC */ meltfclos->tabval[1])),
				"OBI_LOC");
	  ;
	  /*^putslot */
	  /*putslot */
	  melt_assertmsg ("putslot checkobj @OSLOT_ODATA",
			  melt_magic_discr ((melt_ptr_t)
					    ( /*_.INST__V8*/ meltfptr[7])) ==
			  MELTOBMAG_OBJECT);
	  melt_putfield_object (( /*_.INST__V8*/ meltfptr[7]), (1),
				(( /*~OBJ */ meltfclos->tabval[2])),
				"OSLOT_ODATA");
	  ;
	  /*^putslot */
	  /*putslot */
	  melt_assertmsg ("putslot checkobj @OSLOT_OFFSET",
			  melt_magic_discr ((melt_ptr_t)
					    ( /*_.INST__V8*/ meltfptr[7])) ==
			  MELTOBMAG_OBJECT);
	  melt_putfield_object (( /*_.INST__V8*/ meltfptr[7]), (2),
				( /*_.OOFF__V6*/ meltfptr[4]),
				"OSLOT_OFFSET");
	  ;
	  /*^putslot */
	  /*putslot */
	  melt_assertmsg ("putslot checkobj @OSLOT_VALUE",
			  melt_magic_discr ((melt_ptr_t)
					    ( /*_.INST__V8*/ meltfptr[7])) ==
			  MELTOBMAG_OBJECT);
	  melt_putfield_object (( /*_.INST__V8*/ meltfptr[7]), (4),
				( /*_.SLOBJ__V3*/ meltfptr[2]),
				"OSLOT_VALUE");
	  ;
	  /*^touchobj */

	  melt_dbgtrace_written_object ( /*_.INST__V8*/ meltfptr[7],
					"newly made instance");
	  ;
	  /*_.OPUT__V7*/ meltfptr[6] = /*_.INST__V8*/ meltfptr[7];;

	  {
	    MELT_LOCATION ("warmelt-genobj.melt:4863:/ locexp");
	    meltgc_append_list ((melt_ptr_t)
				(( /*~ISTMTLIS */ meltfclos->tabval[3])),
				(melt_ptr_t) ( /*_.OPUT__V7*/ meltfptr[6]));
	  }
	  ;

	  MELT_LOCATION ("warmelt-genobj.melt:4853:/ clear");
	     /*clear *//*_.OOFF__V6*/ meltfptr[4] = 0;
	  /*^clear */
	     /*clear *//*_.OPUT__V7*/ meltfptr[6] = 0;

	  MELT_LOCATION ("warmelt-genobj.melt:4850:/ clear");
	     /*clear *//*_.SLOBJ__V3*/ meltfptr[2] = 0;
	  /*^clear */
	     /*clear *//*_.IFCPP___V4*/ meltfptr[3] = 0;
	  /*epilog */
	}
	;
      }				/*noelse */
    ;
    /*epilog */
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("LAMBDA_", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_141_warmelt_genobj_LAMBDA___46___melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*noretval */ NULL);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_141_warmelt_genobj_LAMBDA___46__ */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_142_warmelt_genobj_LAMBDA___47__ (meltclosure_ptr_t meltclosp_,
					   melt_ptr_t meltfirstargp_,
					   const melt_argdescr_cell_t
					   meltxargdescr_[],
					   union meltparam_un * meltxargtab_,
					   const melt_argdescr_cell_t
					   meltxresdescr_[],
					   union meltparam_un * meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_142_warmelt_genobj_LAMBDA___47___melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_142_warmelt_genobj_LAMBDA___47___st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 4
    melt_ptr_t mcfr_varptr[4];
#define MELTFRAM_NBVARNUM 1
    long mcfr_varnum[1];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_142_warmelt_genobj_LAMBDA___47__ is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_142_warmelt_genobj_LAMBDA___47___st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 4; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_142_warmelt_genobj_LAMBDA___47__ nbval 4*/
  meltfram__.mcfr_nbvar = 4 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("LAMBDA_", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-genobj.melt:4880:/ getarg");
 /*_.OBVA__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_LONG)
    goto lab_endgetargs;
 /*_#IX__L1*/ meltfnum[0] = meltxargtab_[0].meltbp_long;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {

    MELT_LOCATION ("warmelt-genobj.melt:4881:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^cond */
    /*cond */ if ( /*_.OBVA__V2*/ meltfptr[1])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-genobj.melt:4884:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^quasiblock */


	  /*^rawallocobj */
	  /*rawallocobj */
	  {
	    melt_ptr_t newobj = 0;
	    melt_raw_object_create (newobj,
				    (melt_ptr_t) (( /*!CLASS_OBJCLEAR */
						   meltfrout->tabval[0])),
				    (2), "CLASS_OBJCLEAR");
    /*_.INST__V4*/ meltfptr[3] =
	      newobj;
	  };
	  ;
	  /*^putslot */
	  /*putslot */
	  melt_assertmsg ("putslot checkobj @OBI_LOC",
			  melt_magic_discr ((melt_ptr_t)
					    ( /*_.INST__V4*/ meltfptr[3])) ==
			  MELTOBMAG_OBJECT);
	  melt_putfield_object (( /*_.INST__V4*/ meltfptr[3]), (0),
				(( /*~NLOC */ meltfclos->tabval[1])),
				"OBI_LOC");
	  ;
	  /*^putslot */
	  /*putslot */
	  melt_assertmsg ("putslot checkobj @OCLR_VLOC",
			  melt_magic_discr ((melt_ptr_t)
					    ( /*_.INST__V4*/ meltfptr[3])) ==
			  MELTOBMAG_OBJECT);
	  melt_putfield_object (( /*_.INST__V4*/ meltfptr[3]), (1),
				( /*_.OBVA__V2*/ meltfptr[1]), "OCLR_VLOC");
	  ;
	  /*^touchobj */

	  melt_dbgtrace_written_object ( /*_.INST__V4*/ meltfptr[3],
					"newly made instance");
	  ;
	  /*_.INST___V3*/ meltfptr[2] = /*_.INST__V4*/ meltfptr[3];;

	  {
	    MELT_LOCATION ("warmelt-genobj.melt:4883:/ locexp");
	    meltgc_append_list ((melt_ptr_t)
				(( /*~IBODYLIS */ meltfclos->tabval[0])),
				(melt_ptr_t) ( /*_.INST___V3*/ meltfptr[2]));
	  }
	  ;
	  MELT_LOCATION ("warmelt-genobj.melt:4882:/ quasiblock");


	  /*epilog */

	  MELT_LOCATION ("warmelt-genobj.melt:4881:/ clear");
	     /*clear *//*_.INST___V3*/ meltfptr[2] = 0;
	}
	;
      }				/*noelse */
    ;
    /*epilog */
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("LAMBDA_", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_142_warmelt_genobj_LAMBDA___47___melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*noretval */ NULL);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_142_warmelt_genobj_LAMBDA___47__ */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_143_warmelt_genobj_DISPOSE_DLOCBIND_AFTER_DATA_AND_SLOTS_FILL
  (meltclosure_ptr_t meltclosp_, melt_ptr_t meltfirstargp_,
   const melt_argdescr_cell_t meltxargdescr_[],
   union meltparam_un * meltxargtab_,
   const melt_argdescr_cell_t meltxresdescr_[],
   union meltparam_un * meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_143_warmelt_genobj_DISPOSE_DLOCBIND_AFTER_DATA_AND_SLOTS_FILL_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_143_warmelt_genobj_DISPOSE_DLOCBIND_AFTER_DATA_AND_SLOTS_FILL_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 12
    melt_ptr_t mcfr_varptr[12];
#define MELTFRAM_NBVARNUM 3
    long mcfr_varnum[3];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_143_warmelt_genobj_DISPOSE_DLOCBIND_AFTER_DATA_AND_SLOTS_FILL is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct
	 frame_meltrout_143_warmelt_genobj_DISPOSE_DLOCBIND_AFTER_DATA_AND_SLOTS_FILL_st
	 *) meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 12; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_143_warmelt_genobj_DISPOSE_DLOCBIND_AFTER_DATA_AND_SLOTS_FILL nbval 12*/
  meltfram__.mcfr_nbvar = 12 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("DISPOSE_DLOCBIND_AFTER_DATA_AND_SLOTS_FILL",
		    meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-genobj.melt:4902:/ getarg");
 /*_.DLOCBIND__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.GCX__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.GCX__V3*/ meltfptr[2])) != NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:4903:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L1*/ meltfnum[0] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.GCX__V3*/ meltfptr[2]),
			     (melt_ptr_t) (( /*!CLASS_C_GENERATION_CONTEXT */
					    meltfrout->tabval[0])));;
      MELT_LOCATION ("warmelt-genobj.melt:4903:/ cond");
      /*cond */ if ( /*_#IS_A__L1*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V5*/ meltfptr[4] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-genobj.melt:4903:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check gcx"),
				  ("warmelt-genobj.melt")
				  ? ("warmelt-genobj.melt") : __FILE__,
				  (4903) ? (4903) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V5*/ meltfptr[4] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V4*/ meltfptr[3] = /*_.IFELSE___V5*/ meltfptr[4];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:4903:/ clear");
	     /*clear *//*_#IS_A__L1*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V5*/ meltfptr[4] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V4*/ meltfptr[3] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:4904:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L2*/ meltfnum[0] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-genobj.melt:4904:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L2*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L3*/ meltfnum[2] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-genobj.melt:4904:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L3*/ meltfnum[2];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-genobj.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 4904;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring =
		"dispose_dlocbind_after_data_and_slots_fill dlocbind=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.DLOCBIND__V2*/ meltfptr[1];
	      /*_.MELT_DEBUG_FUN__V8*/ meltfptr[7] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[1])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V7*/ meltfptr[6] = /*_.MELT_DEBUG_FUN__V8*/ meltfptr[7];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-genobj.melt:4904:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L3*/ meltfnum[2] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V8*/ meltfptr[7] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V7*/ meltfptr[6] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-genobj.melt:4904:/ quasiblock");


      /*_.PROGN___V9*/ meltfptr[7] = /*_.IF___V7*/ meltfptr[6];;
      /*^compute */
      /*_.IFCPP___V6*/ meltfptr[4] = /*_.PROGN___V9*/ meltfptr[7];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:4904:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L2*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IF___V7*/ meltfptr[6] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V9*/ meltfptr[7] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V6*/ meltfptr[4] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-genobj.melt:4907:/ quasiblock");


    /*^newclosure */
		 /*newclosure *//*_.LAMBDA___V11*/ meltfptr[7] =
      (melt_ptr_t)
      meltgc_new_closure ((meltobject_ptr_t)
			  (((melt_ptr_t) (MELT_PREDEF (DISCR_CLOSURE)))),
			  (meltroutine_ptr_t) (( /*!konst_4 */ meltfrout->
						tabval[4])), (1));
    ;
    /*^putclosedv */
    /*putclosv */
    melt_assertmsg ("putclosv checkclo",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.LAMBDA___V11*/ meltfptr[7])) ==
		    MELTOBMAG_CLOSURE);
    melt_assertmsg ("putclosv checkoff", 0 >= 0
		    && 0 <
		    melt_closure_size ((melt_ptr_t)
				       ( /*_.LAMBDA___V11*/ meltfptr[7])));
    ((meltclosure_ptr_t) /*_.LAMBDA___V11*/ meltfptr[7])->tabval[0] =
      (melt_ptr_t) ( /*_.GCX__V3*/ meltfptr[2]);
    ;
    /*_.LAMBDA___V10*/ meltfptr[6] = /*_.LAMBDA___V11*/ meltfptr[7];;
    MELT_LOCATION ("warmelt-genobj.melt:4905:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.LAMBDA___V10*/ meltfptr[6];
      /*_.MULTIPLE_EVERY__V12*/ meltfptr[11] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!MULTIPLE_EVERY */ meltfrout->tabval[2])),
		    (melt_ptr_t) ( /*_.DLOCBIND__V2*/ meltfptr[1]),
		    (MELTBPARSTR_PTR ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-genobj.melt:4902:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.MULTIPLE_EVERY__V12*/ meltfptr[11];;

    {
      MELT_LOCATION ("warmelt-genobj.melt:4902:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V4*/ meltfptr[3] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V6*/ meltfptr[4] = 0;
    /*^clear */
	   /*clear *//*_.LAMBDA___V10*/ meltfptr[6] = 0;
    /*^clear */
	   /*clear *//*_.MULTIPLE_EVERY__V12*/ meltfptr[11] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("DISPOSE_DLOCBIND_AFTER_DATA_AND_SLOTS_FILL",
		  meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_143_warmelt_genobj_DISPOSE_DLOCBIND_AFTER_DATA_AND_SLOTS_FILL_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_143_warmelt_genobj_DISPOSE_DLOCBIND_AFTER_DATA_AND_SLOTS_FILL */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_144_warmelt_genobj_LAMBDA___48__ (meltclosure_ptr_t meltclosp_,
					   melt_ptr_t meltfirstargp_,
					   const melt_argdescr_cell_t
					   meltxargdescr_[],
					   union meltparam_un * meltxargtab_,
					   const melt_argdescr_cell_t
					   meltxresdescr_[],
					   union meltparam_un * meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_144_warmelt_genobj_LAMBDA___48___melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_144_warmelt_genobj_LAMBDA___48___st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 3
    melt_ptr_t mcfr_varptr[3];
#define MELTFRAM_NBVARNUM 1
    long mcfr_varnum[1];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_144_warmelt_genobj_LAMBDA___48__ is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_144_warmelt_genobj_LAMBDA___48___st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 3; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_144_warmelt_genobj_LAMBDA___48__ nbval 3*/
  meltfram__.mcfr_nbvar = 3 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("LAMBDA_", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-genobj.melt:4907:/ getarg");
 /*_.BND__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_LONG)
    goto lab_endgetargs;
 /*_#IX__L1*/ meltfnum[0] = meltxargtab_[0].meltbp_long;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {

    /*^checksignal */
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & ( /*~GCX */ meltfclos->tabval[0]);
      /*_.DISPOSE_BND_OBJ__V3*/ meltfptr[2] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!DISPOSE_BND_OBJ */ meltfrout->tabval[0])),
		    (melt_ptr_t) ( /*_.BND__V2*/ meltfptr[1]),
		    (MELTBPARSTR_PTR ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.DISPOSE_BND_OBJ__V3*/ meltfptr[2];;

    {
      MELT_LOCATION ("warmelt-genobj.melt:4907:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.DISPOSE_BND_OBJ__V3*/ meltfptr[2] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("LAMBDA_", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_144_warmelt_genobj_LAMBDA___48___melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_144_warmelt_genobj_LAMBDA___48__ */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_145_warmelt_genobj_COMPILOBJ_DATASYMBOL (meltclosure_ptr_t
						  meltclosp_,
						  melt_ptr_t meltfirstargp_,
						  const melt_argdescr_cell_t
						  meltxargdescr_[],
						  union meltparam_un *
						  meltxargtab_,
						  const melt_argdescr_cell_t
						  meltxresdescr_[],
						  union meltparam_un *
						  meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_145_warmelt_genobj_COMPILOBJ_DATASYMBOL_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_145_warmelt_genobj_COMPILOBJ_DATASYMBOL_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 45
    melt_ptr_t mcfr_varptr[45];
#define MELTFRAM_NBVARNUM 13
    long mcfr_varnum[13];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_145_warmelt_genobj_COMPILOBJ_DATASYMBOL is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_145_warmelt_genobj_COMPILOBJ_DATASYMBOL_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 45; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_145_warmelt_genobj_COMPILOBJ_DATASYMBOL nbval 45*/
  meltfram__.mcfr_nbvar = 45 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("COMPILOBJ_DATASYMBOL", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-genobj.melt:4910:/ getarg");
 /*_.SYV__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.GCX__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.GCX__V3*/ meltfptr[2])) != NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:4911:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L1*/ meltfnum[0] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.GCX__V3*/ meltfptr[2]),
			     (melt_ptr_t) (( /*!CLASS_C_GENERATION_CONTEXT */
					    meltfrout->tabval[0])));;
      MELT_LOCATION ("warmelt-genobj.melt:4911:/ cond");
      /*cond */ if ( /*_#IS_A__L1*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V5*/ meltfptr[4] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-genobj.melt:4911:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check gcx"),
				  ("warmelt-genobj.melt")
				  ? ("warmelt-genobj.melt") : __FILE__,
				  (4911) ? (4911) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V5*/ meltfptr[4] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V4*/ meltfptr[3] = /*_.IFELSE___V5*/ meltfptr[4];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:4911:/ clear");
	     /*clear *//*_#IS_A__L1*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V5*/ meltfptr[4] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V4*/ meltfptr[3] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:4912:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L2*/ meltfnum[0] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.SYV__V2*/ meltfptr[1]),
			     (melt_ptr_t) (( /*!CLASS_NREP_DATASYMBOL */
					    meltfrout->tabval[1])));;
      MELT_LOCATION ("warmelt-genobj.melt:4912:/ cond");
      /*cond */ if ( /*_#IS_A__L2*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V7*/ meltfptr[6] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-genobj.melt:4912:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check syv"),
				  ("warmelt-genobj.melt")
				  ? ("warmelt-genobj.melt") : __FILE__,
				  (4912) ? (4912) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V7*/ meltfptr[6] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V6*/ meltfptr[4] = /*_.IFELSE___V7*/ meltfptr[6];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:4912:/ clear");
	     /*clear *//*_#IS_A__L2*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V7*/ meltfptr[6] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V6*/ meltfptr[4] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:4913:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L3*/ meltfnum[0] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-genobj.melt:4913:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L3*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L4*/ meltfnum[3] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-genobj.melt:4913:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L4*/ meltfnum[3];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-genobj.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 4913;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "compilobj_datasymbol syv=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.SYV__V2*/ meltfptr[1];
	      /*_.MELT_DEBUG_FUN__V10*/ meltfptr[9] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[2])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V9*/ meltfptr[8] =
	      /*_.MELT_DEBUG_FUN__V10*/ meltfptr[9];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-genobj.melt:4913:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L4*/ meltfnum[3] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V10*/ meltfptr[9] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V9*/ meltfptr[8] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-genobj.melt:4913:/ quasiblock");


      /*_.PROGN___V11*/ meltfptr[9] = /*_.IF___V9*/ meltfptr[8];;
      /*^compute */
      /*_.IFCPP___V8*/ meltfptr[6] = /*_.PROGN___V11*/ meltfptr[9];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:4913:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L3*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IF___V9*/ meltfptr[8] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V11*/ meltfptr[9] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V8*/ meltfptr[6] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-genobj.melt:4914:/ quasiblock");


    /*^getslot */
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.GCX__V3*/ meltfptr[2]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 6, "GNCX_COMPICACHE");
  /*_.COMPICACHE__V13*/ meltfptr[9] = slot;
    };
    ;
 /*_.CHOBJ__V14*/ meltfptr[13] =
      /*mapobject_get */
      melt_get_mapobjects ((meltmapobjects_ptr_t)
			   ( /*_.COMPICACHE__V13*/ meltfptr[9]),
			   (meltobject_ptr_t) ( /*_.SYV__V2*/ meltfptr[1]));;
    MELT_LOCATION ("warmelt-genobj.melt:4916:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.GCX__V3*/ meltfptr[2]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 0, "GNCX_OBJROUT");
  /*_.INIROUT__V15*/ meltfptr[14] = slot;
    };
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:4919:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L5*/ meltfnum[3] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.INIROUT__V15*/ meltfptr[14]),
			     (melt_ptr_t) (( /*!CLASS_INITIALROUTINEOBJ */
					    meltfrout->tabval[3])));;
      MELT_LOCATION ("warmelt-genobj.melt:4919:/ cond");
      /*cond */ if ( /*_#IS_A__L5*/ meltfnum[3])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V17*/ meltfptr[16] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-genobj.melt:4919:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check inirout"),
				  ("warmelt-genobj.melt")
				  ? ("warmelt-genobj.melt") : __FILE__,
				  (4919) ? (4919) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V17*/ meltfptr[16] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V16*/ meltfptr[15] = /*_.IFELSE___V17*/ meltfptr[16];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:4919:/ clear");
	     /*clear *//*_#IS_A__L5*/ meltfnum[3] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V17*/ meltfptr[16] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V16*/ meltfptr[15] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-genobj.melt:4920:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^cond */
    /*cond */ if ( /*_.CHOBJ__V14*/ meltfptr[13])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{


#if MELT_HAVE_DEBUG
	  MELT_LOCATION ("warmelt-genobj.melt:4922:/ cppif.then");
	  /*^block */
	  /*anyblock */
	  {


	    {
	      /*^locexp */
	      /*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	      melt_dbgcounter++;
#endif
	      ;
	    }
	    ;
	    /*^checksignal */
	    MELT_CHECK_SIGNAL ();
	    ;
     /*_#MELT_NEED_DBG__L6*/ meltfnum[0] =
	      /*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	      ( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	      0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	      ;;
	    MELT_LOCATION ("warmelt-genobj.melt:4922:/ cond");
	    /*cond */ if ( /*_#MELT_NEED_DBG__L6*/ meltfnum[0])	/*then */
	      {
		/*^cond.then */
		/*^block */
		/*anyblock */
		{

       /*_#THE_MELTCALLCOUNT__L7*/ meltfnum[3] =
#ifdef meltcallcount
		    meltcallcount	/* the_meltcallcount */
#else
		    0L
#endif /* meltcallcount the_meltcallcount */
		    ;;
		  MELT_LOCATION ("warmelt-genobj.melt:4922:/ checksignal");
		  MELT_CHECK_SIGNAL ();
		  ;
		  /*^apply */
		  /*apply */
		  {
		    union meltparam_un argtab[5];
		    memset (&argtab, 0, sizeof (argtab));
		    /*^apply.arg */
		    argtab[0].meltbp_long =
		      /*_#THE_MELTCALLCOUNT__L7*/ meltfnum[3];
		    /*^apply.arg */
		    argtab[1].meltbp_cstring = "warmelt-genobj.melt";
		    /*^apply.arg */
		    argtab[2].meltbp_long = 4922;
		    /*^apply.arg */
		    argtab[3].meltbp_cstring =
		      "compilobj_datasymbol found chobj=";
		    /*^apply.arg */
		    argtab[4].meltbp_aptr =
		      (melt_ptr_t *) & /*_.CHOBJ__V14*/ meltfptr[13];
		    /*_.MELT_DEBUG_FUN__V21*/ meltfptr[20] =
		      melt_apply ((meltclosure_ptr_t)
				  (( /*!MELT_DEBUG_FUN */ meltfrout->
				    tabval[2])),
				  (melt_ptr_t) (( /*nil */ NULL)),
				  (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_PTR ""), argtab, "",
				  (union meltparam_un *) 0);
		  }
		  ;
		  /*_.IF___V20*/ meltfptr[19] =
		    /*_.MELT_DEBUG_FUN__V21*/ meltfptr[20];;
		  /*epilog */

		  MELT_LOCATION ("warmelt-genobj.melt:4922:/ clear");
		 /*clear *//*_#THE_MELTCALLCOUNT__L7*/ meltfnum[3] = 0;
		  /*^clear */
		 /*clear *//*_.MELT_DEBUG_FUN__V21*/ meltfptr[20] = 0;
		}
		;
	      }
	    else
	      {			/*^cond.else */

      /*_.IF___V20*/ meltfptr[19] = NULL;;
	      }
	    ;
	    MELT_LOCATION ("warmelt-genobj.melt:4922:/ quasiblock");


	    /*_.PROGN___V22*/ meltfptr[20] = /*_.IF___V20*/ meltfptr[19];;
	    /*^compute */
	    /*_.IFCPP___V19*/ meltfptr[18] = /*_.PROGN___V22*/ meltfptr[20];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-genobj.melt:4922:/ clear");
	       /*clear *//*_#MELT_NEED_DBG__L6*/ meltfnum[0] = 0;
	    /*^clear */
	       /*clear *//*_.IF___V20*/ meltfptr[19] = 0;
	    /*^clear */
	       /*clear *//*_.PROGN___V22*/ meltfptr[20] = 0;
	  }

#else /*MELT_HAVE_DEBUG */
	  /*^cppif.else */
	  /*_.IFCPP___V19*/ meltfptr[18] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
	  ;
	  MELT_LOCATION ("warmelt-genobj.melt:4923:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^quasiblock */


	  /*_.RETVAL___V1*/ meltfptr[0] = /*_.CHOBJ__V14*/ meltfptr[13];;

	  {
	    MELT_LOCATION ("warmelt-genobj.melt:4923:/ locexp");
	    /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
	    if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	      melt_warn_for_no_expected_secondary_results ();
	    /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
	    ;
	  }
	  ;
	  /*^finalreturn */
	  ;
	  /*finalret */ goto labend_rout;
	  MELT_LOCATION ("warmelt-genobj.melt:4921:/ quasiblock");


	  /*_.PROGN___V24*/ meltfptr[20] = /*_.RETURN___V23*/ meltfptr[19];;
	  /*^compute */
	  /*_.IF___V18*/ meltfptr[16] = /*_.PROGN___V24*/ meltfptr[20];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-genobj.melt:4920:/ clear");
	     /*clear *//*_.IFCPP___V19*/ meltfptr[18] = 0;
	  /*^clear */
	     /*clear *//*_.RETURN___V23*/ meltfptr[19] = 0;
	  /*^clear */
	     /*clear *//*_.PROGN___V24*/ meltfptr[20] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

  /*_.IF___V18*/ meltfptr[16] = NULL;;
      }
    ;
    MELT_LOCATION ("warmelt-genobj.melt:4924:/ quasiblock");


    MELT_LOCATION ("warmelt-genobj.melt:4925:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.SYV__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 4, "NDATA_DISCRX");
  /*_.NDATA_DISCRX__V26*/ meltfptr[19] = slot;
    };
    ;
    /*^checksignal */
    MELT_CHECK_SIGNAL ();
    ;
    /*^msend */
    /*msend */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^ojbmsend.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.GCX__V3*/ meltfptr[2];
      /*_.ODISCR__V27*/ meltfptr[20] =
	meltgc_send ((melt_ptr_t) ( /*_.NDATA_DISCRX__V26*/ meltfptr[19]),
		     (melt_ptr_t) (( /*!COMPILE_OBJ */ meltfrout->tabval[4])),
		     (MELTBPARSTR_PTR ""), argtab, "",
		     (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-genobj.melt:4929:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*^rawallocobj */
    /*rawallocobj */
    {
      melt_ptr_t newobj = 0;
      melt_raw_object_create (newobj,
			      (melt_ptr_t) (( /*!CLASS_OBJINITUNIQUEOBJECT */
					     meltfrout->tabval[5])), (7),
			      "CLASS_OBJINITUNIQUEOBJECT");
  /*_.INST__V29*/ meltfptr[28] =
	newobj;
    };
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OBV_TYPE",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V29*/ meltfptr[28])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V29*/ meltfptr[28]), (0),
			  (( /*!CTYPE_VALUE */ meltfrout->tabval[6])),
			  "OBV_TYPE");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OIE_DATA",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V29*/ meltfptr[28])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V29*/ meltfptr[28]), (2),
			  ( /*_.SYV__V2*/ meltfptr[1]), "OIE_DATA");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OIE_DISCR",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V29*/ meltfptr[28])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V29*/ meltfptr[28]), (3),
			  ( /*_.ODISCR__V27*/ meltfptr[20]), "OIE_DISCR");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OIO_CLASS",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V29*/ meltfptr[28])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V29*/ meltfptr[28]), (6),
			  (( /*nil */ NULL)), "OIO_CLASS");
    ;
    /*^touchobj */

    melt_dbgtrace_written_object ( /*_.INST__V29*/ meltfptr[28],
				  "newly made instance");
    ;
    /*_.OBSYM__V28*/ meltfptr[27] = /*_.INST__V29*/ meltfptr[28];;

    {
      MELT_LOCATION ("warmelt-genobj.melt:4936:/ locexp");
      meltgc_put_mapobjects ((meltmapobjects_ptr_t)
			     ( /*_.COMPICACHE__V13*/ meltfptr[9]),
			     (meltobject_ptr_t) ( /*_.SYV__V2*/ meltfptr[1]),
			     (melt_ptr_t) ( /*_.OBSYM__V28*/ meltfptr[27]));
    }
    ;
    MELT_LOCATION ("warmelt-genobj.melt:4938:/ quasiblock");


 /*_.NAMBUF__V30*/ meltfptr[29] =
      (melt_ptr_t)
      meltgc_new_strbuf ((meltobject_ptr_t)
			 (( /*!DISCR_STRBUF */ meltfrout->tabval[7])),
			 (const char *) 0);;
    MELT_LOCATION ("warmelt-genobj.melt:4939:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.SYV__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 2, "NDATA_RANK");
  /*_.NDATA_RANK__V31*/ meltfptr[30] = slot;
    };
    ;
 /*_#SYRK__L8*/ meltfnum[3] =
      (melt_get_int ((melt_ptr_t) ( /*_.NDATA_RANK__V31*/ meltfptr[30])));;

    {
      MELT_LOCATION ("warmelt-genobj.melt:4941:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.NAMBUF__V30*/ meltfptr[29]),
			   ("dsym_"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-genobj.melt:4942:/ locexp");
      meltgc_add_strbuf_dec ((melt_ptr_t) ( /*_.NAMBUF__V30*/ meltfptr[29]),
			     ( /*_#SYRK__L8*/ meltfnum[3]));
    }
    ;

    {
      MELT_LOCATION ("warmelt-genobj.melt:4943:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.NAMBUF__V30*/ meltfptr[29]),
			   ("__"));
    }
    ;
    MELT_LOCATION ("warmelt-genobj.melt:4944:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.SYV__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 9, "NDSY_NAMESTR");
  /*_.NDSY_NAMESTR__V32*/ meltfptr[31] = slot;
    };
    ;

    {
      /*^locexp */
      meltgc_add_strbuf_cident ((melt_ptr_t)
				( /*_.NAMBUF__V30*/ meltfptr[29]),
				melt_string_str ((melt_ptr_t)
						 ( /*_.NDSY_NAMESTR__V32*/
						  meltfptr[31])));
    }
    ;
    MELT_LOCATION ("warmelt-genobj.melt:4945:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_.STRBUF2STRING__V33*/ meltfptr[32] =
      (meltgc_new_stringdup
       ((meltobject_ptr_t) (( /*!DISCR_STRING */ meltfrout->tabval[8])),
	melt_strbuf_str ((melt_ptr_t) ( /*_.NAMBUF__V30*/ meltfptr[29]))));;
    MELT_LOCATION ("warmelt-genobj.melt:4945:/ quasiblock");


    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OIE_CNAME",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.OBSYM__V28*/ meltfptr[27])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.OBSYM__V28*/ meltfptr[27]), (1),
			  ( /*_.STRBUF2STRING__V33*/ meltfptr[32]),
			  "OIE_CNAME");
    ;
    /*^touch */
    meltgc_touch ( /*_.OBSYM__V28*/ meltfptr[27]);
    ;
    /*^touchobj */

    melt_dbgtrace_written_object ( /*_.OBSYM__V28*/ meltfptr[27],
				  "put-fields");
    ;


    MELT_LOCATION ("warmelt-genobj.melt:4938:/ clear");
	   /*clear *//*_.NAMBUF__V30*/ meltfptr[29] = 0;
    /*^clear */
	   /*clear *//*_.NDATA_RANK__V31*/ meltfptr[30] = 0;
    /*^clear */
	   /*clear *//*_#SYRK__L8*/ meltfnum[3] = 0;
    /*^clear */
	   /*clear *//*_.NDSY_NAMESTR__V32*/ meltfptr[31] = 0;
    /*^clear */
	   /*clear *//*_.STRBUF2STRING__V33*/ meltfptr[32] = 0;
    MELT_LOCATION ("warmelt-genobj.melt:4948:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.SYV__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 8, "NINST_SLOTS");
  /*_.NINST_SLOTS__V34*/ meltfptr[29] = slot;
    };
    ;
 /*_#MULTIPLE_LENGTH__L9*/ meltfnum[0] =
      (melt_multiple_length
       ((melt_ptr_t) ( /*_.NINST_SLOTS__V34*/ meltfptr[29])));;

    {
      MELT_LOCATION ("warmelt-genobj.melt:4948:/ locexp");
      melt_put_int ((melt_ptr_t) ( /*_.OBSYM__V28*/ meltfptr[27]),
		    ( /*_#MULTIPLE_LENGTH__L9*/ meltfnum[0]));
    }
    ;
    MELT_LOCATION ("warmelt-genobj.melt:4949:/ quasiblock");


    /*^checksignal */
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[4];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.OBSYM__V28*/ meltfptr[27];
      /*^apply.arg */
      argtab[1].meltbp_aptr = (melt_ptr_t *) & /*_.ODISCR__V27*/ meltfptr[20];
      /*^apply.arg */
      argtab[2].meltbp_aptr =
	(melt_ptr_t *) & /*_.INIROUT__V15*/ meltfptr[14];
      /*^apply.arg */
      argtab[3].meltbp_aptr = (melt_ptr_t *) & /*_.GCX__V3*/ meltfptr[2];
      /*_.DLOCBIND__V36*/ meltfptr[31] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!COMPIL_DATA_AND_SLOTS_FILL */ meltfrout->
		      tabval[9])), (melt_ptr_t) ( /*_.SYV__V2*/ meltfptr[1]),
		    (MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_PTR
		     MELTBPARSTR_PTR ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:4950:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L10*/ meltfnum[3] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-genobj.melt:4950:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L10*/ meltfnum[3])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L11*/ meltfnum[10] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-genobj.melt:4950:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long =
		/*_#THE_MELTCALLCOUNT__L11*/ meltfnum[10];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-genobj.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 4950;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "compilobj_datasymbol dlocbind=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.DLOCBIND__V36*/ meltfptr[31];
	      /*_.MELT_DEBUG_FUN__V39*/ meltfptr[38] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[2])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V38*/ meltfptr[37] =
	      /*_.MELT_DEBUG_FUN__V39*/ meltfptr[38];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-genobj.melt:4950:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L11*/ meltfnum[10] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V39*/ meltfptr[38] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V38*/ meltfptr[37] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-genobj.melt:4950:/ quasiblock");


      /*_.PROGN___V40*/ meltfptr[38] = /*_.IF___V38*/ meltfptr[37];;
      /*^compute */
      /*_.IFCPP___V37*/ meltfptr[32] = /*_.PROGN___V40*/ meltfptr[38];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:4950:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L10*/ meltfnum[3] = 0;
      /*^clear */
	     /*clear *//*_.IF___V38*/ meltfptr[37] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V40*/ meltfptr[38] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V37*/ meltfptr[32] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    /*^compute */
    /*_.LET___V35*/ meltfptr[30] = /*_.IFCPP___V37*/ meltfptr[32];;

    MELT_LOCATION ("warmelt-genobj.melt:4949:/ clear");
	   /*clear *//*_.DLOCBIND__V36*/ meltfptr[31] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V37*/ meltfptr[32] = 0;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:4953:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L12*/ meltfnum[10] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-genobj.melt:4953:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L12*/ meltfnum[10])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L13*/ meltfnum[3] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-genobj.melt:4953:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long =
		/*_#THE_MELTCALLCOUNT__L13*/ meltfnum[3];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-genobj.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 4953;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "compilobj_datasymbol final obsym=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.OBSYM__V28*/ meltfptr[27];
	      /*_.MELT_DEBUG_FUN__V43*/ meltfptr[31] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[2])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V42*/ meltfptr[38] =
	      /*_.MELT_DEBUG_FUN__V43*/ meltfptr[31];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-genobj.melt:4953:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L13*/ meltfnum[3] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V43*/ meltfptr[31] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V42*/ meltfptr[38] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-genobj.melt:4953:/ quasiblock");


      /*_.PROGN___V44*/ meltfptr[32] = /*_.IF___V42*/ meltfptr[38];;
      /*^compute */
      /*_.IFCPP___V41*/ meltfptr[37] = /*_.PROGN___V44*/ meltfptr[32];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:4953:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L12*/ meltfnum[10] = 0;
      /*^clear */
	     /*clear *//*_.IF___V42*/ meltfptr[38] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V44*/ meltfptr[32] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V41*/ meltfptr[37] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-genobj.melt:4954:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.OBSYM__V28*/ meltfptr[27];;

    {
      MELT_LOCATION ("warmelt-genobj.melt:4954:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*_.LET___V25*/ meltfptr[18] = /*_.RETURN___V45*/ meltfptr[31];;

    MELT_LOCATION ("warmelt-genobj.melt:4924:/ clear");
	   /*clear *//*_.NDATA_DISCRX__V26*/ meltfptr[19] = 0;
    /*^clear */
	   /*clear *//*_.ODISCR__V27*/ meltfptr[20] = 0;
    /*^clear */
	   /*clear *//*_.OBSYM__V28*/ meltfptr[27] = 0;
    /*^clear */
	   /*clear *//*_.NINST_SLOTS__V34*/ meltfptr[29] = 0;
    /*^clear */
	   /*clear *//*_#MULTIPLE_LENGTH__L9*/ meltfnum[0] = 0;
    /*^clear */
	   /*clear *//*_.LET___V35*/ meltfptr[30] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V41*/ meltfptr[37] = 0;
    /*^clear */
	   /*clear *//*_.RETURN___V45*/ meltfptr[31] = 0;
    /*_.LET___V12*/ meltfptr[8] = /*_.LET___V25*/ meltfptr[18];;

    MELT_LOCATION ("warmelt-genobj.melt:4914:/ clear");
	   /*clear *//*_.COMPICACHE__V13*/ meltfptr[9] = 0;
    /*^clear */
	   /*clear *//*_.CHOBJ__V14*/ meltfptr[13] = 0;
    /*^clear */
	   /*clear *//*_.INIROUT__V15*/ meltfptr[14] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V16*/ meltfptr[15] = 0;
    /*^clear */
	   /*clear *//*_.IF___V18*/ meltfptr[16] = 0;
    /*^clear */
	   /*clear *//*_.LET___V25*/ meltfptr[18] = 0;
    MELT_LOCATION ("warmelt-genobj.melt:4910:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.LET___V12*/ meltfptr[8];;

    {
      MELT_LOCATION ("warmelt-genobj.melt:4910:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V4*/ meltfptr[3] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V6*/ meltfptr[4] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V8*/ meltfptr[6] = 0;
    /*^clear */
	   /*clear *//*_.LET___V12*/ meltfptr[8] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("COMPILOBJ_DATASYMBOL", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_145_warmelt_genobj_COMPILOBJ_DATASYMBOL_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_145_warmelt_genobj_COMPILOBJ_DATASYMBOL */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_146_warmelt_genobj_COMPILOBJ_DATAINSTANCE (meltclosure_ptr_t
						    meltclosp_,
						    melt_ptr_t meltfirstargp_,
						    const melt_argdescr_cell_t
						    meltxargdescr_[],
						    union meltparam_un *
						    meltxargtab_,
						    const melt_argdescr_cell_t
						    meltxresdescr_[],
						    union meltparam_un *
						    meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_146_warmelt_genobj_COMPILOBJ_DATAINSTANCE_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_146_warmelt_genobj_COMPILOBJ_DATAINSTANCE_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 55
    melt_ptr_t mcfr_varptr[55];
#define MELTFRAM_NBVARNUM 13
    long mcfr_varnum[13];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_146_warmelt_genobj_COMPILOBJ_DATAINSTANCE is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_146_warmelt_genobj_COMPILOBJ_DATAINSTANCE_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 55; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_146_warmelt_genobj_COMPILOBJ_DATAINSTANCE nbval 55*/
  meltfram__.mcfr_nbvar = 55 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("COMPILOBJ_DATAINSTANCE", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-genobj.melt:4961:/ getarg");
 /*_.DAI__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.GCX__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.GCX__V3*/ meltfptr[2])) != NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:4962:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L1*/ meltfnum[0] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.GCX__V3*/ meltfptr[2]),
			     (melt_ptr_t) (( /*!CLASS_C_GENERATION_CONTEXT */
					    meltfrout->tabval[0])));;
      MELT_LOCATION ("warmelt-genobj.melt:4962:/ cond");
      /*cond */ if ( /*_#IS_A__L1*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V5*/ meltfptr[4] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-genobj.melt:4962:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check gcx"),
				  ("warmelt-genobj.melt")
				  ? ("warmelt-genobj.melt") : __FILE__,
				  (4962) ? (4962) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V5*/ meltfptr[4] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V4*/ meltfptr[3] = /*_.IFELSE___V5*/ meltfptr[4];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:4962:/ clear");
	     /*clear *//*_#IS_A__L1*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V5*/ meltfptr[4] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V4*/ meltfptr[3] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:4963:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L2*/ meltfnum[0] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-genobj.melt:4963:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L2*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L3*/ meltfnum[2] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-genobj.melt:4963:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L3*/ meltfnum[2];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-genobj.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 4963;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "compilobj_datainstance dai=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.DAI__V2*/ meltfptr[1];
	      /*_.MELT_DEBUG_FUN__V8*/ meltfptr[7] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[1])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V7*/ meltfptr[6] = /*_.MELT_DEBUG_FUN__V8*/ meltfptr[7];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-genobj.melt:4963:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L3*/ meltfnum[2] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V8*/ meltfptr[7] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V7*/ meltfptr[6] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-genobj.melt:4963:/ quasiblock");


      /*_.PROGN___V9*/ meltfptr[7] = /*_.IF___V7*/ meltfptr[6];;
      /*^compute */
      /*_.IFCPP___V6*/ meltfptr[4] = /*_.PROGN___V9*/ meltfptr[7];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:4963:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L2*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IF___V7*/ meltfptr[6] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V9*/ meltfptr[7] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V6*/ meltfptr[4] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-genobj.melt:4964:/ quasiblock");


    /*^getslot */
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.GCX__V3*/ meltfptr[2]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 6, "GNCX_COMPICACHE");
  /*_.COMPICACHE__V11*/ meltfptr[7] = slot;
    };
    ;
 /*_.CHOBJ__V12*/ meltfptr[11] =
      /*mapobject_get */
      melt_get_mapobjects ((meltmapobjects_ptr_t)
			   ( /*_.COMPICACHE__V11*/ meltfptr[7]),
			   (meltobject_ptr_t) ( /*_.DAI__V2*/ meltfptr[1]));;
    MELT_LOCATION ("warmelt-genobj.melt:4966:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.GCX__V3*/ meltfptr[2]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 0, "GNCX_OBJROUT");
  /*_.INIROUT__V13*/ meltfptr[12] = slot;
    };
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:4969:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L4*/ meltfnum[2] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.INIROUT__V13*/ meltfptr[12]),
			     (melt_ptr_t) (( /*!CLASS_INITIALROUTINEOBJ */
					    meltfrout->tabval[2])));;
      MELT_LOCATION ("warmelt-genobj.melt:4969:/ cond");
      /*cond */ if ( /*_#IS_A__L4*/ meltfnum[2])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V15*/ meltfptr[14] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-genobj.melt:4969:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check inirout"),
				  ("warmelt-genobj.melt")
				  ? ("warmelt-genobj.melt") : __FILE__,
				  (4969) ? (4969) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V15*/ meltfptr[14] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V14*/ meltfptr[13] = /*_.IFELSE___V15*/ meltfptr[14];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:4969:/ clear");
	     /*clear *//*_#IS_A__L4*/ meltfnum[2] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V15*/ meltfptr[14] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V14*/ meltfptr[13] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-genobj.melt:4970:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^cond */
    /*cond */ if ( /*_.CHOBJ__V12*/ meltfptr[11])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{


#if MELT_HAVE_DEBUG
	  MELT_LOCATION ("warmelt-genobj.melt:4972:/ cppif.then");
	  /*^block */
	  /*anyblock */
	  {


	    {
	      /*^locexp */
	      /*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	      melt_dbgcounter++;
#endif
	      ;
	    }
	    ;
	    /*^checksignal */
	    MELT_CHECK_SIGNAL ();
	    ;
     /*_#MELT_NEED_DBG__L5*/ meltfnum[0] =
	      /*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	      ( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	      0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	      ;;
	    MELT_LOCATION ("warmelt-genobj.melt:4972:/ cond");
	    /*cond */ if ( /*_#MELT_NEED_DBG__L5*/ meltfnum[0])	/*then */
	      {
		/*^cond.then */
		/*^block */
		/*anyblock */
		{

       /*_#THE_MELTCALLCOUNT__L6*/ meltfnum[2] =
#ifdef meltcallcount
		    meltcallcount	/* the_meltcallcount */
#else
		    0L
#endif /* meltcallcount the_meltcallcount */
		    ;;
		  MELT_LOCATION ("warmelt-genobj.melt:4972:/ checksignal");
		  MELT_CHECK_SIGNAL ();
		  ;
		  /*^apply */
		  /*apply */
		  {
		    union meltparam_un argtab[5];
		    memset (&argtab, 0, sizeof (argtab));
		    /*^apply.arg */
		    argtab[0].meltbp_long =
		      /*_#THE_MELTCALLCOUNT__L6*/ meltfnum[2];
		    /*^apply.arg */
		    argtab[1].meltbp_cstring = "warmelt-genobj.melt";
		    /*^apply.arg */
		    argtab[2].meltbp_long = 4972;
		    /*^apply.arg */
		    argtab[3].meltbp_cstring =
		      "compilobj_datainstance return found chobj=";
		    /*^apply.arg */
		    argtab[4].meltbp_aptr =
		      (melt_ptr_t *) & /*_.CHOBJ__V12*/ meltfptr[11];
		    /*_.MELT_DEBUG_FUN__V19*/ meltfptr[18] =
		      melt_apply ((meltclosure_ptr_t)
				  (( /*!MELT_DEBUG_FUN */ meltfrout->
				    tabval[1])),
				  (melt_ptr_t) (( /*nil */ NULL)),
				  (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_PTR ""), argtab, "",
				  (union meltparam_un *) 0);
		  }
		  ;
		  /*_.IF___V18*/ meltfptr[17] =
		    /*_.MELT_DEBUG_FUN__V19*/ meltfptr[18];;
		  /*epilog */

		  MELT_LOCATION ("warmelt-genobj.melt:4972:/ clear");
		 /*clear *//*_#THE_MELTCALLCOUNT__L6*/ meltfnum[2] = 0;
		  /*^clear */
		 /*clear *//*_.MELT_DEBUG_FUN__V19*/ meltfptr[18] = 0;
		}
		;
	      }
	    else
	      {			/*^cond.else */

      /*_.IF___V18*/ meltfptr[17] = NULL;;
	      }
	    ;
	    MELT_LOCATION ("warmelt-genobj.melt:4972:/ quasiblock");


	    /*_.PROGN___V20*/ meltfptr[18] = /*_.IF___V18*/ meltfptr[17];;
	    /*^compute */
	    /*_.IFCPP___V17*/ meltfptr[16] = /*_.PROGN___V20*/ meltfptr[18];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-genobj.melt:4972:/ clear");
	       /*clear *//*_#MELT_NEED_DBG__L5*/ meltfnum[0] = 0;
	    /*^clear */
	       /*clear *//*_.IF___V18*/ meltfptr[17] = 0;
	    /*^clear */
	       /*clear *//*_.PROGN___V20*/ meltfptr[18] = 0;
	  }

#else /*MELT_HAVE_DEBUG */
	  /*^cppif.else */
	  /*_.IFCPP___V17*/ meltfptr[16] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
	  ;
	  MELT_LOCATION ("warmelt-genobj.melt:4973:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^quasiblock */


	  /*_.RETVAL___V1*/ meltfptr[0] = /*_.CHOBJ__V12*/ meltfptr[11];;

	  {
	    MELT_LOCATION ("warmelt-genobj.melt:4973:/ locexp");
	    /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
	    if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	      melt_warn_for_no_expected_secondary_results ();
	    /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
	    ;
	  }
	  ;
	  /*^finalreturn */
	  ;
	  /*finalret */ goto labend_rout;
	  MELT_LOCATION ("warmelt-genobj.melt:4971:/ quasiblock");


	  /*_.PROGN___V22*/ meltfptr[18] = /*_.RETURN___V21*/ meltfptr[17];;
	  /*^compute */
	  /*_.IF___V16*/ meltfptr[14] = /*_.PROGN___V22*/ meltfptr[18];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-genobj.melt:4970:/ clear");
	     /*clear *//*_.IFCPP___V17*/ meltfptr[16] = 0;
	  /*^clear */
	     /*clear *//*_.RETURN___V21*/ meltfptr[17] = 0;
	  /*^clear */
	     /*clear *//*_.PROGN___V22*/ meltfptr[18] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

  /*_.IF___V16*/ meltfptr[14] = NULL;;
      }
    ;
    MELT_LOCATION ("warmelt-genobj.melt:4974:/ quasiblock");


    /*^getslot */
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.DAI__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 0, "NREP_LOC");
  /*_.LOC__V24*/ meltfptr[17] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-genobj.melt:4975:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.DAI__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 1, "NDATA_NAME");
  /*_.NAM__V25*/ meltfptr[18] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-genobj.melt:4977:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.DAI__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 4, "NDATA_DISCRX");
  /*_.DISX__V26*/ meltfptr[25] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-genobj.melt:4978:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.DAI__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 2, "NDATA_RANK");
  /*_.DRANK__V27*/ meltfptr[26] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-genobj.melt:4979:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.DAI__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 7, "NINST_HASH");
  /*_.DHASH__V28*/ meltfptr[27] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-genobj.melt:4980:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.DAI__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 6, "NINST_PREDEF");
  /*_.DPREDEF__V29*/ meltfptr[28] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-genobj.melt:4981:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.DAI__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 8, "NINST_SLOTS");
  /*_.DSLOTS__V30*/ meltfptr[29] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-genobj.melt:4982:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.INIROUT__V13*/ meltfptr[12]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 2, "NINIT_TOPL");
  /*_.ININSL__V31*/ meltfptr[30] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-genobj.melt:4983:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.INIROUT__V13*/ meltfptr[12]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 12, "OIROUT_FILL");
  /*_.INIFILL__V32*/ meltfptr[31] = slot;
    };
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:4987:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_INTEGERBOX__L7*/ meltfnum[2] =
	(melt_magic_discr ((melt_ptr_t) ( /*_.DRANK__V27*/ meltfptr[26])) ==
	 MELTOBMAG_INT);;
      MELT_LOCATION ("warmelt-genobj.melt:4987:/ cond");
      /*cond */ if ( /*_#IS_INTEGERBOX__L7*/ meltfnum[2])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V34*/ meltfptr[33] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-genobj.melt:4987:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("compilobj_datainstance check drank"),
				  ("warmelt-genobj.melt")
				  ? ("warmelt-genobj.melt") : __FILE__,
				  (4987) ? (4987) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V34*/ meltfptr[33] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V33*/ meltfptr[32] = /*_.IFELSE___V34*/ meltfptr[33];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:4987:/ clear");
	     /*clear *//*_#IS_INTEGERBOX__L7*/ meltfnum[2] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V34*/ meltfptr[33] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V33*/ meltfptr[32] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-genobj.melt:4988:/ quasiblock");


    /*^checksignal */
    MELT_CHECK_SIGNAL ();
    ;
    /*^msend */
    /*msend */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^ojbmsend.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.GCX__V3*/ meltfptr[2];
      /*_.CDISX__V36*/ meltfptr[35] =
	meltgc_send ((melt_ptr_t) ( /*_.DISX__V26*/ meltfptr[25]),
		     (melt_ptr_t) (( /*!COMPILE_OBJ */ meltfrout->tabval[3])),
		     (MELTBPARSTR_PTR ""), argtab, "",
		     (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-genobj.melt:4990:/ quasiblock");


    MELT_LOCATION ("warmelt-genobj.melt:4992:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^cond */
    /*cond */ if ( /*_.DPREDEF__V29*/ meltfptr[28])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-genobj.melt:4993:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^quasiblock */


	  /*^rawallocobj */
	  /*rawallocobj */
	  {
	    melt_ptr_t newobj = 0;
	    melt_raw_object_create (newobj,
				    (melt_ptr_t) (( /*!CLASS_OBJINITUNIQUEOBJECT */ meltfrout->tabval[4])), (7), "CLASS_OBJINITUNIQUEOBJECT");
    /*_.INST__V40*/ meltfptr[39] =
	      newobj;
	  };
	  ;
	  /*^putslot */
	  /*putslot */
	  melt_assertmsg ("putslot checkobj @OBV_TYPE",
			  melt_magic_discr ((melt_ptr_t)
					    ( /*_.INST__V40*/ meltfptr[39]))
			  == MELTOBMAG_OBJECT);
	  melt_putfield_object (( /*_.INST__V40*/ meltfptr[39]), (0),
				(( /*!CTYPE_VALUE */ meltfrout->tabval[5])),
				"OBV_TYPE");
	  ;
	  /*^putslot */
	  /*putslot */
	  melt_assertmsg ("putslot checkobj @OIE_DATA",
			  melt_magic_discr ((melt_ptr_t)
					    ( /*_.INST__V40*/ meltfptr[39]))
			  == MELTOBMAG_OBJECT);
	  melt_putfield_object (( /*_.INST__V40*/ meltfptr[39]), (2),
				( /*_.DAI__V2*/ meltfptr[1]), "OIE_DATA");
	  ;
	  /*^putslot */
	  /*putslot */
	  melt_assertmsg ("putslot checkobj @OIE_DISCR",
			  melt_magic_discr ((melt_ptr_t)
					    ( /*_.INST__V40*/ meltfptr[39]))
			  == MELTOBMAG_OBJECT);
	  melt_putfield_object (( /*_.INST__V40*/ meltfptr[39]), (3),
				( /*_.CDISX__V36*/ meltfptr[35]),
				"OIE_DISCR");
	  ;
	  /*^putslot */
	  /*putslot */
	  melt_assertmsg ("putslot checkobj @OIO_PREDEF",
			  melt_magic_discr ((melt_ptr_t)
					    ( /*_.INST__V40*/ meltfptr[39]))
			  == MELTOBMAG_OBJECT);
	  melt_putfield_object (( /*_.INST__V40*/ meltfptr[39]), (5),
				( /*_.DPREDEF__V29*/ meltfptr[28]),
				"OIO_PREDEF");
	  ;
	  /*^putslot */
	  /*putslot */
	  melt_assertmsg ("putslot checkobj @OIO_CLASS",
			  melt_magic_discr ((melt_ptr_t)
					    ( /*_.INST__V40*/ meltfptr[39]))
			  == MELTOBMAG_OBJECT);
	  melt_putfield_object (( /*_.INST__V40*/ meltfptr[39]), (6),
				(( /*nil */ NULL)), "OIO_CLASS");
	  ;
	  /*^touchobj */

	  melt_dbgtrace_written_object ( /*_.INST__V40*/ meltfptr[39],
					"newly made instance");
	  ;
	  /*_.INST___V39*/ meltfptr[38] = /*_.INST__V40*/ meltfptr[39];;
	  /*^compute */
	  /*_.OINI__V38*/ meltfptr[37] = /*_.INST___V39*/ meltfptr[38];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-genobj.melt:4992:/ clear");
	     /*clear *//*_.INST___V39*/ meltfptr[38] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-genobj.melt:5001:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^quasiblock */


	  /*^rawallocobj */
	  /*rawallocobj */
	  {
	    melt_ptr_t newobj = 0;
	    melt_raw_object_create (newobj,
				    (melt_ptr_t) (( /*!CLASS_OBJINITOBJECT */
						   meltfrout->tabval[6])),
				    (7), "CLASS_OBJINITOBJECT");
    /*_.INST__V42*/ meltfptr[41] =
	      newobj;
	  };
	  ;
	  /*^putslot */
	  /*putslot */
	  melt_assertmsg ("putslot checkobj @OBV_TYPE",
			  melt_magic_discr ((melt_ptr_t)
					    ( /*_.INST__V42*/ meltfptr[41]))
			  == MELTOBMAG_OBJECT);
	  melt_putfield_object (( /*_.INST__V42*/ meltfptr[41]), (0),
				(( /*!CTYPE_VALUE */ meltfrout->tabval[5])),
				"OBV_TYPE");
	  ;
	  /*^putslot */
	  /*putslot */
	  melt_assertmsg ("putslot checkobj @OIE_DATA",
			  melt_magic_discr ((melt_ptr_t)
					    ( /*_.INST__V42*/ meltfptr[41]))
			  == MELTOBMAG_OBJECT);
	  melt_putfield_object (( /*_.INST__V42*/ meltfptr[41]), (2),
				( /*_.DAI__V2*/ meltfptr[1]), "OIE_DATA");
	  ;
	  /*^putslot */
	  /*putslot */
	  melt_assertmsg ("putslot checkobj @OIE_DISCR",
			  melt_magic_discr ((melt_ptr_t)
					    ( /*_.INST__V42*/ meltfptr[41]))
			  == MELTOBMAG_OBJECT);
	  melt_putfield_object (( /*_.INST__V42*/ meltfptr[41]), (3),
				( /*_.CDISX__V36*/ meltfptr[35]),
				"OIE_DISCR");
	  ;
	  /*^putslot */
	  /*putslot */
	  melt_assertmsg ("putslot checkobj @OIO_PREDEF",
			  melt_magic_discr ((melt_ptr_t)
					    ( /*_.INST__V42*/ meltfptr[41]))
			  == MELTOBMAG_OBJECT);
	  melt_putfield_object (( /*_.INST__V42*/ meltfptr[41]), (5),
				( /*_.DPREDEF__V29*/ meltfptr[28]),
				"OIO_PREDEF");
	  ;
	  /*^putslot */
	  /*putslot */
	  melt_assertmsg ("putslot checkobj @OIO_CLASS",
			  melt_magic_discr ((melt_ptr_t)
					    ( /*_.INST__V42*/ meltfptr[41]))
			  == MELTOBMAG_OBJECT);
	  melt_putfield_object (( /*_.INST__V42*/ meltfptr[41]), (6),
				(( /*nil */ NULL)), "OIO_CLASS");
	  ;
	  /*^touchobj */

	  melt_dbgtrace_written_object ( /*_.INST__V42*/ meltfptr[41],
					"newly made instance");
	  ;
	  /*_.INST___V41*/ meltfptr[38] = /*_.INST__V42*/ meltfptr[41];;
	  /*^compute */
	  /*_.OINI__V38*/ meltfptr[37] = /*_.INST___V41*/ meltfptr[38];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-genobj.melt:4992:/ clear");
	     /*clear *//*_.INST___V41*/ meltfptr[38] = 0;
	}
	;
      }
    ;
 /*_.NAMBUF__V43*/ meltfptr[38] =
      (melt_ptr_t)
      meltgc_new_strbuf ((meltobject_ptr_t)
			 (( /*!DISCR_STRBUF */ meltfrout->tabval[7])),
			 (const char *) 0);;
    /*^compute */
 /*_#DRK__L8*/ meltfnum[0] =
      (melt_get_int ((melt_ptr_t) ( /*_.DRANK__V27*/ meltfptr[26])));;

    {
      MELT_LOCATION ("warmelt-genobj.melt:5012:/ locexp");
      meltgc_put_mapobjects ((meltmapobjects_ptr_t)
			     ( /*_.COMPICACHE__V11*/ meltfptr[7]),
			     (meltobject_ptr_t) ( /*_.DAI__V2*/ meltfptr[1]),
			     (melt_ptr_t) ( /*_.OINI__V38*/ meltfptr[37]));
    }
    ;

    {
      MELT_LOCATION ("warmelt-genobj.melt:5013:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.NAMBUF__V43*/ meltfptr[38]),
			   ("dobj_"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-genobj.melt:5014:/ locexp");
      meltgc_add_strbuf_dec ((melt_ptr_t) ( /*_.NAMBUF__V43*/ meltfptr[38]),
			     ( /*_#DRK__L8*/ meltfnum[0]));
    }
    ;
    MELT_LOCATION ("warmelt-genobj.melt:5015:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^cond */
    /*cond */ if ( /*_.NAM__V25*/ meltfptr[18])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{


	  {
	    MELT_LOCATION ("warmelt-genobj.melt:5016:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t)
				 ( /*_.NAMBUF__V43*/ meltfptr[38]), ("__"));
	  }
	  ;
	  MELT_LOCATION ("warmelt-genobj.melt:5017:/ getslot");
	  {
	    melt_ptr_t slot = NULL, obj = NULL;
	    obj = (melt_ptr_t) ( /*_.NAM__V25*/ meltfptr[18]) /*=obj*/ ;
	    melt_object_get_field (slot, obj, 1, "NAMED_NAME");
    /*_.NAMED_NAME__V44*/ meltfptr[43] = slot;
	  };
	  ;

	  {
	    /*^locexp */
	    meltgc_add_strbuf_cident ((melt_ptr_t)
				      ( /*_.NAMBUF__V43*/ meltfptr[38]),
				      melt_string_str ((melt_ptr_t)
						       ( /*_.NAMED_NAME__V44*/
							meltfptr[43])));
	  }
	  ;
	  MELT_LOCATION ("warmelt-genobj.melt:5015:/ quasiblock");


	  /*epilog */

	  /*^clear */
	     /*clear *//*_.NAMED_NAME__V44*/ meltfptr[43] = 0;
	}
	;
      }				/*noelse */
    ;
    MELT_LOCATION ("warmelt-genobj.melt:5018:/ quasiblock");


 /*_.CNAM__V45*/ meltfptr[43] =
      (meltgc_new_stringdup
       ((meltobject_ptr_t) (( /*!DISCR_STRING */ meltfrout->tabval[8])),
	melt_strbuf_str ((melt_ptr_t) ( /*_.NAMBUF__V43*/ meltfptr[38]))));;
    MELT_LOCATION ("warmelt-genobj.melt:5019:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OIE_CNAME",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.OINI__V38*/ meltfptr[37])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.OINI__V38*/ meltfptr[37]), (1),
			  ( /*_.CNAM__V45*/ meltfptr[43]), "OIE_CNAME");
    ;
    /*^touch */
    meltgc_touch ( /*_.OINI__V38*/ meltfptr[37]);
    ;
    /*^touchobj */

    melt_dbgtrace_written_object ( /*_.OINI__V38*/ meltfptr[37],
				  "put-fields");
    ;


    MELT_LOCATION ("warmelt-genobj.melt:5018:/ clear");
	   /*clear *//*_.CNAM__V45*/ meltfptr[43] = 0;
 /*_#MULTIPLE_LENGTH__L9*/ meltfnum[2] =
      (melt_multiple_length
       ((melt_ptr_t) ( /*_.DSLOTS__V30*/ meltfptr[29])));;

    {
      MELT_LOCATION ("warmelt-genobj.melt:5021:/ locexp");
      melt_put_int ((melt_ptr_t) ( /*_.OINI__V38*/ meltfptr[37]),
		    ( /*_#MULTIPLE_LENGTH__L9*/ meltfnum[2]));
    }
    ;
    MELT_LOCATION ("warmelt-genobj.melt:5022:/ quasiblock");


    /*^checksignal */
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[4];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.OINI__V38*/ meltfptr[37];
      /*^apply.arg */
      argtab[1].meltbp_aptr = (melt_ptr_t *) & /*_.CDISX__V36*/ meltfptr[35];
      /*^apply.arg */
      argtab[2].meltbp_aptr =
	(melt_ptr_t *) & /*_.INIROUT__V13*/ meltfptr[12];
      /*^apply.arg */
      argtab[3].meltbp_aptr = (melt_ptr_t *) & /*_.GCX__V3*/ meltfptr[2];
      /*_.DLOCBIND__V47*/ meltfptr[46] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!COMPIL_DATA_AND_SLOTS_FILL */ meltfrout->
		      tabval[9])), (melt_ptr_t) ( /*_.DAI__V2*/ meltfptr[1]),
		    (MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_PTR
		     MELTBPARSTR_PTR ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:5023:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L10*/ meltfnum[9] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-genobj.melt:5023:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L10*/ meltfnum[9])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L11*/ meltfnum[10] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-genobj.melt:5023:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long =
		/*_#THE_MELTCALLCOUNT__L11*/ meltfnum[10];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-genobj.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 5023;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring =
		"compil_data_and_slots_fill dlocbind=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.DLOCBIND__V47*/ meltfptr[46];
	      /*_.MELT_DEBUG_FUN__V50*/ meltfptr[49] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[1])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V49*/ meltfptr[48] =
	      /*_.MELT_DEBUG_FUN__V50*/ meltfptr[49];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-genobj.melt:5023:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L11*/ meltfnum[10] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V50*/ meltfptr[49] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V49*/ meltfptr[48] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-genobj.melt:5023:/ quasiblock");


      /*_.PROGN___V51*/ meltfptr[49] = /*_.IF___V49*/ meltfptr[48];;
      /*^compute */
      /*_.IFCPP___V48*/ meltfptr[47] = /*_.PROGN___V51*/ meltfptr[49];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:5023:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L10*/ meltfnum[9] = 0;
      /*^clear */
	     /*clear *//*_.IF___V49*/ meltfptr[48] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V51*/ meltfptr[49] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V48*/ meltfptr[47] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    /*^compute */
    /*_.LET___V46*/ meltfptr[43] = /*_.IFCPP___V48*/ meltfptr[47];;

    MELT_LOCATION ("warmelt-genobj.melt:5022:/ clear");
	   /*clear *//*_.DLOCBIND__V47*/ meltfptr[46] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V48*/ meltfptr[47] = 0;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:5026:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L12*/ meltfnum[10] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-genobj.melt:5026:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L12*/ meltfnum[10])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L13*/ meltfnum[9] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-genobj.melt:5026:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long =
		/*_#THE_MELTCALLCOUNT__L13*/ meltfnum[9];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-genobj.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 5026;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "compilobj_datainstance final oini=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.OINI__V38*/ meltfptr[37];
	      /*_.MELT_DEBUG_FUN__V54*/ meltfptr[46] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[1])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V53*/ meltfptr[49] =
	      /*_.MELT_DEBUG_FUN__V54*/ meltfptr[46];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-genobj.melt:5026:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L13*/ meltfnum[9] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V54*/ meltfptr[46] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V53*/ meltfptr[49] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-genobj.melt:5026:/ quasiblock");


      /*_.PROGN___V55*/ meltfptr[47] = /*_.IF___V53*/ meltfptr[49];;
      /*^compute */
      /*_.IFCPP___V52*/ meltfptr[48] = /*_.PROGN___V55*/ meltfptr[47];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:5026:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L12*/ meltfnum[10] = 0;
      /*^clear */
	     /*clear *//*_.IF___V53*/ meltfptr[49] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V55*/ meltfptr[47] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V52*/ meltfptr[48] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    /*^compute */
    /*_.LET___V37*/ meltfptr[36] = /*_.OINI__V38*/ meltfptr[37];;

    MELT_LOCATION ("warmelt-genobj.melt:4990:/ clear");
	   /*clear *//*_.OINI__V38*/ meltfptr[37] = 0;
    /*^clear */
	   /*clear *//*_.NAMBUF__V43*/ meltfptr[38] = 0;
    /*^clear */
	   /*clear *//*_#DRK__L8*/ meltfnum[0] = 0;
    /*^clear */
	   /*clear *//*_#MULTIPLE_LENGTH__L9*/ meltfnum[2] = 0;
    /*^clear */
	   /*clear *//*_.LET___V46*/ meltfptr[43] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V52*/ meltfptr[48] = 0;
    /*_.LET___V35*/ meltfptr[33] = /*_.LET___V37*/ meltfptr[36];;

    MELT_LOCATION ("warmelt-genobj.melt:4988:/ clear");
	   /*clear *//*_.CDISX__V36*/ meltfptr[35] = 0;
    /*^clear */
	   /*clear *//*_.LET___V37*/ meltfptr[36] = 0;
    /*_.LET___V23*/ meltfptr[16] = /*_.LET___V35*/ meltfptr[33];;

    MELT_LOCATION ("warmelt-genobj.melt:4974:/ clear");
	   /*clear *//*_.LOC__V24*/ meltfptr[17] = 0;
    /*^clear */
	   /*clear *//*_.NAM__V25*/ meltfptr[18] = 0;
    /*^clear */
	   /*clear *//*_.DISX__V26*/ meltfptr[25] = 0;
    /*^clear */
	   /*clear *//*_.DRANK__V27*/ meltfptr[26] = 0;
    /*^clear */
	   /*clear *//*_.DHASH__V28*/ meltfptr[27] = 0;
    /*^clear */
	   /*clear *//*_.DPREDEF__V29*/ meltfptr[28] = 0;
    /*^clear */
	   /*clear *//*_.DSLOTS__V30*/ meltfptr[29] = 0;
    /*^clear */
	   /*clear *//*_.ININSL__V31*/ meltfptr[30] = 0;
    /*^clear */
	   /*clear *//*_.INIFILL__V32*/ meltfptr[31] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V33*/ meltfptr[32] = 0;
    /*^clear */
	   /*clear *//*_.LET___V35*/ meltfptr[33] = 0;
    /*_.LET___V10*/ meltfptr[6] = /*_.LET___V23*/ meltfptr[16];;

    MELT_LOCATION ("warmelt-genobj.melt:4964:/ clear");
	   /*clear *//*_.COMPICACHE__V11*/ meltfptr[7] = 0;
    /*^clear */
	   /*clear *//*_.CHOBJ__V12*/ meltfptr[11] = 0;
    /*^clear */
	   /*clear *//*_.INIROUT__V13*/ meltfptr[12] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V14*/ meltfptr[13] = 0;
    /*^clear */
	   /*clear *//*_.IF___V16*/ meltfptr[14] = 0;
    /*^clear */
	   /*clear *//*_.LET___V23*/ meltfptr[16] = 0;
    MELT_LOCATION ("warmelt-genobj.melt:4961:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.LET___V10*/ meltfptr[6];;

    {
      MELT_LOCATION ("warmelt-genobj.melt:4961:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V4*/ meltfptr[3] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V6*/ meltfptr[4] = 0;
    /*^clear */
	   /*clear *//*_.LET___V10*/ meltfptr[6] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("COMPILOBJ_DATAINSTANCE", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_146_warmelt_genobj_COMPILOBJ_DATAINSTANCE_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_146_warmelt_genobj_COMPILOBJ_DATAINSTANCE */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_147_warmelt_genobj_COMPILOBJ_DATATUPLE (meltclosure_ptr_t meltclosp_,
						 melt_ptr_t meltfirstargp_,
						 const melt_argdescr_cell_t
						 meltxargdescr_[],
						 union meltparam_un *
						 meltxargtab_,
						 const melt_argdescr_cell_t
						 meltxresdescr_[],
						 union meltparam_un *
						 meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_147_warmelt_genobj_COMPILOBJ_DATATUPLE_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_147_warmelt_genobj_COMPILOBJ_DATATUPLE_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 52
    melt_ptr_t mcfr_varptr[52];
#define MELTFRAM_NBVARNUM 11
    long mcfr_varnum[11];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_147_warmelt_genobj_COMPILOBJ_DATATUPLE is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_147_warmelt_genobj_COMPILOBJ_DATATUPLE_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 52; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_147_warmelt_genobj_COMPILOBJ_DATATUPLE nbval 52*/
  meltfram__.mcfr_nbvar = 52 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("COMPILOBJ_DATATUPLE", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-genobj.melt:5038:/ getarg");
 /*_.NTI__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.GCX__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.GCX__V3*/ meltfptr[2])) != NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:5039:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L1*/ meltfnum[0] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.NTI__V2*/ meltfptr[1]),
			     (melt_ptr_t) (( /*!CLASS_NREP_DATATUPLE */
					    meltfrout->tabval[0])));;
      MELT_LOCATION ("warmelt-genobj.melt:5039:/ cond");
      /*cond */ if ( /*_#IS_A__L1*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V5*/ meltfptr[4] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-genobj.melt:5039:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check nti"),
				  ("warmelt-genobj.melt")
				  ? ("warmelt-genobj.melt") : __FILE__,
				  (5039) ? (5039) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V5*/ meltfptr[4] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V4*/ meltfptr[3] = /*_.IFELSE___V5*/ meltfptr[4];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:5039:/ clear");
	     /*clear *//*_#IS_A__L1*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V5*/ meltfptr[4] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V4*/ meltfptr[3] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:5040:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L2*/ meltfnum[0] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.GCX__V3*/ meltfptr[2]),
			     (melt_ptr_t) (( /*!CLASS_C_GENERATION_CONTEXT */
					    meltfrout->tabval[1])));;
      MELT_LOCATION ("warmelt-genobj.melt:5040:/ cond");
      /*cond */ if ( /*_#IS_A__L2*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V7*/ meltfptr[6] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-genobj.melt:5040:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check gcx"),
				  ("warmelt-genobj.melt")
				  ? ("warmelt-genobj.melt") : __FILE__,
				  (5040) ? (5040) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V7*/ meltfptr[6] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V6*/ meltfptr[4] = /*_.IFELSE___V7*/ meltfptr[6];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:5040:/ clear");
	     /*clear *//*_#IS_A__L2*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V7*/ meltfptr[6] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V6*/ meltfptr[4] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:5041:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L3*/ meltfnum[0] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-genobj.melt:5041:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L3*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L4*/ meltfnum[3] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-genobj.melt:5041:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L4*/ meltfnum[3];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-genobj.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 5041;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "compilobj_datatuple nti=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.NTI__V2*/ meltfptr[1];
	      /*_.MELT_DEBUG_FUN__V10*/ meltfptr[9] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[2])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V9*/ meltfptr[8] =
	      /*_.MELT_DEBUG_FUN__V10*/ meltfptr[9];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-genobj.melt:5041:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L4*/ meltfnum[3] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V10*/ meltfptr[9] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V9*/ meltfptr[8] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-genobj.melt:5041:/ quasiblock");


      /*_.PROGN___V11*/ meltfptr[9] = /*_.IF___V9*/ meltfptr[8];;
      /*^compute */
      /*_.IFCPP___V8*/ meltfptr[6] = /*_.PROGN___V11*/ meltfptr[9];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:5041:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L3*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IF___V9*/ meltfptr[8] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V11*/ meltfptr[9] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V8*/ meltfptr[6] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-genobj.melt:5042:/ quasiblock");


    /*^getslot */
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.GCX__V3*/ meltfptr[2]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 6, "GNCX_COMPICACHE");
  /*_.COMPICACHE__V13*/ meltfptr[9] = slot;
    };
    ;
 /*_.CHOBJ__V14*/ meltfptr[13] =
      /*mapobject_get */
      melt_get_mapobjects ((meltmapobjects_ptr_t)
			   ( /*_.COMPICACHE__V13*/ meltfptr[9]),
			   (meltobject_ptr_t) ( /*_.NTI__V2*/ meltfptr[1]));;
    MELT_LOCATION ("warmelt-genobj.melt:5044:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.GCX__V3*/ meltfptr[2]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 0, "GNCX_OBJROUT");
  /*_.INIROUT__V15*/ meltfptr[14] = slot;
    };
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:5047:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L5*/ meltfnum[3] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.INIROUT__V15*/ meltfptr[14]),
			     (melt_ptr_t) (( /*!CLASS_INITIALROUTINEOBJ */
					    meltfrout->tabval[3])));;
      MELT_LOCATION ("warmelt-genobj.melt:5047:/ cond");
      /*cond */ if ( /*_#IS_A__L5*/ meltfnum[3])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V17*/ meltfptr[16] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-genobj.melt:5047:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check inirout"),
				  ("warmelt-genobj.melt")
				  ? ("warmelt-genobj.melt") : __FILE__,
				  (5047) ? (5047) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V17*/ meltfptr[16] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V16*/ meltfptr[15] = /*_.IFELSE___V17*/ meltfptr[16];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:5047:/ clear");
	     /*clear *//*_#IS_A__L5*/ meltfnum[3] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V17*/ meltfptr[16] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V16*/ meltfptr[15] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-genobj.melt:5048:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^cond */
    /*cond */ if ( /*_.CHOBJ__V14*/ meltfptr[13])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{


#if MELT_HAVE_DEBUG
	  MELT_LOCATION ("warmelt-genobj.melt:5050:/ cppif.then");
	  /*^block */
	  /*anyblock */
	  {


	    {
	      /*^locexp */
	      /*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	      melt_dbgcounter++;
#endif
	      ;
	    }
	    ;
	    /*^checksignal */
	    MELT_CHECK_SIGNAL ();
	    ;
     /*_#MELT_NEED_DBG__L6*/ meltfnum[0] =
	      /*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	      ( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	      0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	      ;;
	    MELT_LOCATION ("warmelt-genobj.melt:5050:/ cond");
	    /*cond */ if ( /*_#MELT_NEED_DBG__L6*/ meltfnum[0])	/*then */
	      {
		/*^cond.then */
		/*^block */
		/*anyblock */
		{

       /*_#THE_MELTCALLCOUNT__L7*/ meltfnum[3] =
#ifdef meltcallcount
		    meltcallcount	/* the_meltcallcount */
#else
		    0L
#endif /* meltcallcount the_meltcallcount */
		    ;;
		  MELT_LOCATION ("warmelt-genobj.melt:5050:/ checksignal");
		  MELT_CHECK_SIGNAL ();
		  ;
		  /*^apply */
		  /*apply */
		  {
		    union meltparam_un argtab[5];
		    memset (&argtab, 0, sizeof (argtab));
		    /*^apply.arg */
		    argtab[0].meltbp_long =
		      /*_#THE_MELTCALLCOUNT__L7*/ meltfnum[3];
		    /*^apply.arg */
		    argtab[1].meltbp_cstring = "warmelt-genobj.melt";
		    /*^apply.arg */
		    argtab[2].meltbp_long = 5050;
		    /*^apply.arg */
		    argtab[3].meltbp_cstring =
		      "compilobj_datatuple found chobj=";
		    /*^apply.arg */
		    argtab[4].meltbp_aptr =
		      (melt_ptr_t *) & /*_.CHOBJ__V14*/ meltfptr[13];
		    /*_.MELT_DEBUG_FUN__V21*/ meltfptr[20] =
		      melt_apply ((meltclosure_ptr_t)
				  (( /*!MELT_DEBUG_FUN */ meltfrout->
				    tabval[2])),
				  (melt_ptr_t) (( /*nil */ NULL)),
				  (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_PTR ""), argtab, "",
				  (union meltparam_un *) 0);
		  }
		  ;
		  /*_.IF___V20*/ meltfptr[19] =
		    /*_.MELT_DEBUG_FUN__V21*/ meltfptr[20];;
		  /*epilog */

		  MELT_LOCATION ("warmelt-genobj.melt:5050:/ clear");
		 /*clear *//*_#THE_MELTCALLCOUNT__L7*/ meltfnum[3] = 0;
		  /*^clear */
		 /*clear *//*_.MELT_DEBUG_FUN__V21*/ meltfptr[20] = 0;
		}
		;
	      }
	    else
	      {			/*^cond.else */

      /*_.IF___V20*/ meltfptr[19] = NULL;;
	      }
	    ;
	    MELT_LOCATION ("warmelt-genobj.melt:5050:/ quasiblock");


	    /*_.PROGN___V22*/ meltfptr[20] = /*_.IF___V20*/ meltfptr[19];;
	    /*^compute */
	    /*_.IFCPP___V19*/ meltfptr[18] = /*_.PROGN___V22*/ meltfptr[20];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-genobj.melt:5050:/ clear");
	       /*clear *//*_#MELT_NEED_DBG__L6*/ meltfnum[0] = 0;
	    /*^clear */
	       /*clear *//*_.IF___V20*/ meltfptr[19] = 0;
	    /*^clear */
	       /*clear *//*_.PROGN___V22*/ meltfptr[20] = 0;
	  }

#else /*MELT_HAVE_DEBUG */
	  /*^cppif.else */
	  /*_.IFCPP___V19*/ meltfptr[18] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
	  ;
	  MELT_LOCATION ("warmelt-genobj.melt:5051:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^quasiblock */


	  /*_.RETVAL___V1*/ meltfptr[0] = /*_.CHOBJ__V14*/ meltfptr[13];;

	  {
	    MELT_LOCATION ("warmelt-genobj.melt:5051:/ locexp");
	    /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
	    if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	      melt_warn_for_no_expected_secondary_results ();
	    /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
	    ;
	  }
	  ;
	  /*^finalreturn */
	  ;
	  /*finalret */ goto labend_rout;
	  MELT_LOCATION ("warmelt-genobj.melt:5049:/ quasiblock");


	  /*_.PROGN___V24*/ meltfptr[20] = /*_.RETURN___V23*/ meltfptr[19];;
	  /*^compute */
	  /*_.IF___V18*/ meltfptr[16] = /*_.PROGN___V24*/ meltfptr[20];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-genobj.melt:5048:/ clear");
	     /*clear *//*_.IFCPP___V19*/ meltfptr[18] = 0;
	  /*^clear */
	     /*clear *//*_.RETURN___V23*/ meltfptr[19] = 0;
	  /*^clear */
	     /*clear *//*_.PROGN___V24*/ meltfptr[20] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

  /*_.IF___V18*/ meltfptr[16] = NULL;;
      }
    ;
    MELT_LOCATION ("warmelt-genobj.melt:5052:/ quasiblock");


    /*^getslot */
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.NTI__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 5, "NTUP_COMP");
  /*_.NCOMPI__V26*/ meltfptr[19] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-genobj.melt:5055:/ quasiblock");


    /*^newclosure */
		 /*newclosure *//*_.LAMBDA___V28*/ meltfptr[27] =
      (melt_ptr_t)
      meltgc_new_closure ((meltobject_ptr_t)
			  (((melt_ptr_t) (MELT_PREDEF (DISCR_CLOSURE)))),
			  (meltroutine_ptr_t) (( /*!konst_6 */ meltfrout->
						tabval[6])), (1));
    ;
    /*^putclosedv */
    /*putclosv */
    melt_assertmsg ("putclosv checkclo",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.LAMBDA___V28*/ meltfptr[27])) ==
		    MELTOBMAG_CLOSURE);
    melt_assertmsg ("putclosv checkoff", 0 >= 0
		    && 0 <
		    melt_closure_size ((melt_ptr_t)
				       ( /*_.LAMBDA___V28*/ meltfptr[27])));
    ((meltclosure_ptr_t) /*_.LAMBDA___V28*/ meltfptr[27])->tabval[0] =
      (melt_ptr_t) ( /*_.GCX__V3*/ meltfptr[2]);
    ;
    /*_.LAMBDA___V27*/ meltfptr[20] = /*_.LAMBDA___V28*/ meltfptr[27];;
    MELT_LOCATION ("warmelt-genobj.melt:5053:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & /*_.LAMBDA___V27*/ meltfptr[20];
      /*_.OCOMPI__V29*/ meltfptr[28] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!MULTIPLE_MAP */ meltfrout->tabval[4])),
		    (melt_ptr_t) ( /*_.NCOMPI__V26*/ meltfptr[19]),
		    (MELTBPARSTR_PTR ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-genobj.melt:5056:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.NTI__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 4, "NDATA_DISCRX");
  /*_.DISX__V30*/ meltfptr[29] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-genobj.melt:5057:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.NTI__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 2, "NDATA_RANK");
  /*_.DRANK__V31*/ meltfptr[30] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-genobj.melt:5058:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^msend */
    /*msend */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^ojbmsend.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.GCX__V3*/ meltfptr[2];
      /*_.ODISCR__V32*/ meltfptr[31] =
	meltgc_send ((melt_ptr_t) ( /*_.DISX__V30*/ meltfptr[29]),
		     (melt_ptr_t) (( /*!COMPILE_OBJ */ meltfrout->tabval[7])),
		     (MELTBPARSTR_PTR ""), argtab, "",
		     (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-genobj.melt:5059:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.NTI__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 1, "NDATA_NAME");
  /*_.NAM__V33*/ meltfptr[32] = slot;
    };
    ;
 /*_.NAMBUF__V34*/ meltfptr[33] =
      (melt_ptr_t)
      meltgc_new_strbuf ((meltobject_ptr_t)
			 (( /*!DISCR_STRBUF */ meltfrout->tabval[8])),
			 (const char *) 0);;
    MELT_LOCATION ("warmelt-genobj.melt:5061:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & ( /*!konst_10_VALTUP_ */ meltfrout->tabval[10]);
      /*_.LOCVAR__V35*/ meltfptr[34] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!GET_FREE_OBJLOCPTR */ meltfrout->tabval[9])),
		    (melt_ptr_t) ( /*_.GCX__V3*/ meltfptr[2]),
		    (MELTBPARSTR_PTR ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;
 /*_#DRK__L8*/ meltfnum[3] =
      (melt_get_int ((melt_ptr_t) ( /*_.DRANK__V31*/ meltfptr[30])));;
    MELT_LOCATION ("warmelt-genobj.melt:5063:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.INIROUT__V15*/ meltfptr[14]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 3, "OBROUT_BODY");
  /*_.INIBODY__V36*/ meltfptr[35] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-genobj.melt:5064:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj =
	(melt_ptr_t) (( /*!konst_11_INITUP_ */ meltfrout->
		       tabval[11])) /*=obj*/ ;
      melt_object_get_field (slot, obj, 1, "NAMED_NAME");
  /*_.COMM__V37*/ meltfptr[36] = slot;
    };
    ;

    {
      MELT_LOCATION ("warmelt-genobj.melt:5066:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.NAMBUF__V34*/ meltfptr[33]),
			   ("dtup_"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-genobj.melt:5067:/ locexp");
      meltgc_add_strbuf_dec ((melt_ptr_t) ( /*_.NAMBUF__V34*/ meltfptr[33]),
			     ( /*_#DRK__L8*/ meltfnum[3]));
    }
    ;
    MELT_LOCATION ("warmelt-genobj.melt:5068:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^cond */
    /*cond */ if ( /*_.NAM__V33*/ meltfptr[32])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{


	  {
	    MELT_LOCATION ("warmelt-genobj.melt:5069:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t)
				 ( /*_.NAMBUF__V34*/ meltfptr[33]), ("__"));
	  }
	  ;
	  MELT_LOCATION ("warmelt-genobj.melt:5070:/ getslot");
	  {
	    melt_ptr_t slot = NULL, obj = NULL;
	    obj = (melt_ptr_t) ( /*_.NAM__V33*/ meltfptr[32]) /*=obj*/ ;
	    melt_object_get_field (slot, obj, 1, "NAMED_NAME");
    /*_.NAMED_NAME__V38*/ meltfptr[37] = slot;
	  };
	  ;

	  {
	    /*^locexp */
	    meltgc_add_strbuf_cident ((melt_ptr_t)
				      ( /*_.NAMBUF__V34*/ meltfptr[33]),
				      melt_string_str ((melt_ptr_t)
						       ( /*_.NAMED_NAME__V38*/
							meltfptr[37])));
	  }
	  ;
	  MELT_LOCATION ("warmelt-genobj.melt:5068:/ quasiblock");


	  /*epilog */

	  /*^clear */
	     /*clear *//*_.NAMED_NAME__V38*/ meltfptr[37] = 0;
	}
	;
      }				/*noelse */
    ;
    MELT_LOCATION ("warmelt-genobj.melt:5071:/ quasiblock");


    /*^checksignal */
    MELT_CHECK_SIGNAL ();
    ;
 /*_.STRBUF2STRING__V40*/ meltfptr[39] =
      (meltgc_new_stringdup
       ((meltobject_ptr_t) (( /*!DISCR_STRING */ meltfrout->tabval[14])),
	melt_strbuf_str ((melt_ptr_t) ( /*_.NAMBUF__V34*/ meltfptr[33]))));;
    MELT_LOCATION ("warmelt-genobj.melt:5071:/ quasiblock");


    /*^rawallocobj */
    /*rawallocobj */
    {
      melt_ptr_t newobj = 0;
      melt_raw_object_create (newobj,
			      (melt_ptr_t) (( /*!CLASS_OBJINITMULTIPLE */
					     meltfrout->tabval[12])), (6),
			      "CLASS_OBJINITMULTIPLE");
  /*_.INST__V42*/ meltfptr[41] =
	newobj;
    };
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OBV_TYPE",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V42*/ meltfptr[41])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V42*/ meltfptr[41]), (0),
			  (( /*!CTYPE_VALUE */ meltfrout->tabval[13])),
			  "OBV_TYPE");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OIE_DISCR",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V42*/ meltfptr[41])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V42*/ meltfptr[41]), (3),
			  ( /*_.ODISCR__V32*/ meltfptr[31]), "OIE_DISCR");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OIE_LOCVAR",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V42*/ meltfptr[41])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V42*/ meltfptr[41]), (4),
			  ( /*_.LOCVAR__V35*/ meltfptr[34]), "OIE_LOCVAR");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OIE_CNAME",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V42*/ meltfptr[41])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V42*/ meltfptr[41]), (1),
			  ( /*_.STRBUF2STRING__V40*/ meltfptr[39]),
			  "OIE_CNAME");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OIM_TUPVAL",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V42*/ meltfptr[41])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V42*/ meltfptr[41]), (5),
			  ( /*_.OCOMPI__V29*/ meltfptr[28]), "OIM_TUPVAL");
    ;
    /*^touchobj */

    melt_dbgtrace_written_object ( /*_.INST__V42*/ meltfptr[41],
				  "newly made instance");
    ;
    /*_.OTUP__V41*/ meltfptr[40] = /*_.INST__V42*/ meltfptr[41];;

    {
      MELT_LOCATION ("warmelt-genobj.melt:5079:/ locexp");
      meltgc_put_mapobjects ((meltmapobjects_ptr_t)
			     ( /*_.COMPICACHE__V13*/ meltfptr[9]),
			     (meltobject_ptr_t) ( /*_.NTI__V2*/ meltfptr[1]),
			     (melt_ptr_t) ( /*_.OTUP__V41*/ meltfptr[40]));
    }
    ;
 /*_#MULTIPLE_LENGTH__L9*/ meltfnum[0] =
      (melt_multiple_length
       ((melt_ptr_t) ( /*_.NCOMPI__V26*/ meltfptr[19])));;

    {
      MELT_LOCATION ("warmelt-genobj.melt:5080:/ locexp");
      melt_put_int ((melt_ptr_t) ( /*_.OTUP__V41*/ meltfptr[40]),
		    ( /*_#MULTIPLE_LENGTH__L9*/ meltfnum[0]));
    }
    ;
    MELT_LOCATION ("warmelt-genobj.melt:5083:/ quasiblock");


    /*^newclosure */
		 /*newclosure *//*_.LAMBDA___V44*/ meltfptr[43] =
      (melt_ptr_t)
      meltgc_new_closure ((meltobject_ptr_t)
			  (((melt_ptr_t) (MELT_PREDEF (DISCR_CLOSURE)))),
			  (meltroutine_ptr_t) (( /*!konst_18 */ meltfrout->
						tabval[18])), (3));
    ;
    /*^putclosedv */
    /*putclosv */
    melt_assertmsg ("putclosv checkclo",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.LAMBDA___V44*/ meltfptr[43])) ==
		    MELTOBMAG_CLOSURE);
    melt_assertmsg ("putclosv checkoff", 0 >= 0
		    && 0 <
		    melt_closure_size ((melt_ptr_t)
				       ( /*_.LAMBDA___V44*/ meltfptr[43])));
    ((meltclosure_ptr_t) /*_.LAMBDA___V44*/ meltfptr[43])->tabval[0] =
      (melt_ptr_t) ( /*_.OCOMPI__V29*/ meltfptr[28]);
    ;
    /*^putclosedv */
    /*putclosv */
    melt_assertmsg ("putclosv checkclo",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.LAMBDA___V44*/ meltfptr[43])) ==
		    MELTOBMAG_CLOSURE);
    melt_assertmsg ("putclosv checkoff", 1 >= 0
		    && 1 <
		    melt_closure_size ((melt_ptr_t)
				       ( /*_.LAMBDA___V44*/ meltfptr[43])));
    ((meltclosure_ptr_t) /*_.LAMBDA___V44*/ meltfptr[43])->tabval[1] =
      (melt_ptr_t) ( /*_.INIBODY__V36*/ meltfptr[35]);
    ;
    /*^putclosedv */
    /*putclosv */
    melt_assertmsg ("putclosv checkclo",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.LAMBDA___V44*/ meltfptr[43])) ==
		    MELTOBMAG_CLOSURE);
    melt_assertmsg ("putclosv checkoff", 2 >= 0
		    && 2 <
		    melt_closure_size ((melt_ptr_t)
				       ( /*_.LAMBDA___V44*/ meltfptr[43])));
    ((meltclosure_ptr_t) /*_.LAMBDA___V44*/ meltfptr[43])->tabval[2] =
      (melt_ptr_t) ( /*_.OTUP__V41*/ meltfptr[40]);
    ;
    /*_.LAMBDA___V43*/ meltfptr[42] = /*_.LAMBDA___V44*/ meltfptr[43];;
    MELT_LOCATION ("warmelt-genobj.melt:5081:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & /*_.LAMBDA___V43*/ meltfptr[42];
      /*_.MULTIPLE_EVERY__V45*/ meltfptr[44] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!MULTIPLE_EVERY */ meltfrout->tabval[15])),
		    (melt_ptr_t) ( /*_.NCOMPI__V26*/ meltfptr[19]),
		    (MELTBPARSTR_PTR ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-genobj.melt:5093:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*^rawallocobj */
    /*rawallocobj */
    {
      melt_ptr_t newobj = 0;
      melt_raw_object_create (newobj,
			      (melt_ptr_t) (( /*!CLASS_OBJTOUCH */ meltfrout->
					     tabval[19])), (3),
			      "CLASS_OBJTOUCH");
  /*_.INST__V47*/ meltfptr[46] =
	newobj;
    };
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OTOUCH_VAL",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V47*/ meltfptr[46])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V47*/ meltfptr[46]), (1),
			  ( /*_.OTUP__V41*/ meltfptr[40]), "OTOUCH_VAL");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OTOUCH_COMMENT",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V47*/ meltfptr[46])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V47*/ meltfptr[46]), (2),
			  ( /*_.COMM__V37*/ meltfptr[36]), "OTOUCH_COMMENT");
    ;
    /*^touchobj */

    melt_dbgtrace_written_object ( /*_.INST__V47*/ meltfptr[46],
				  "newly made instance");
    ;
    /*_.INST___V46*/ meltfptr[45] = /*_.INST__V47*/ meltfptr[46];;

    {
      MELT_LOCATION ("warmelt-genobj.melt:5092:/ locexp");
      meltgc_append_list ((melt_ptr_t) ( /*_.INIBODY__V36*/ meltfptr[35]),
			  (melt_ptr_t) ( /*_.INST___V46*/ meltfptr[45]));
    }
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:5096:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L10*/ meltfnum[9] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-genobj.melt:5096:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L10*/ meltfnum[9])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L11*/ meltfnum[10] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-genobj.melt:5096:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long =
		/*_#THE_MELTCALLCOUNT__L11*/ meltfnum[10];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-genobj.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 5096;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "compilobj datatuple otup=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.OTUP__V41*/ meltfptr[40];
	      /*_.MELT_DEBUG_FUN__V50*/ meltfptr[49] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[2])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V49*/ meltfptr[48] =
	      /*_.MELT_DEBUG_FUN__V50*/ meltfptr[49];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-genobj.melt:5096:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L11*/ meltfnum[10] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V50*/ meltfptr[49] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V49*/ meltfptr[48] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-genobj.melt:5096:/ quasiblock");


      /*_.PROGN___V51*/ meltfptr[49] = /*_.IF___V49*/ meltfptr[48];;
      /*^compute */
      /*_.IFCPP___V48*/ meltfptr[47] = /*_.PROGN___V51*/ meltfptr[49];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:5096:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L10*/ meltfnum[9] = 0;
      /*^clear */
	     /*clear *//*_.IF___V49*/ meltfptr[48] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V51*/ meltfptr[49] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V48*/ meltfptr[47] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-genobj.melt:5097:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.OTUP__V41*/ meltfptr[40];;

    {
      MELT_LOCATION ("warmelt-genobj.melt:5097:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*_.LET___V39*/ meltfptr[37] = /*_.RETURN___V52*/ meltfptr[48];;

    MELT_LOCATION ("warmelt-genobj.melt:5071:/ clear");
	   /*clear *//*_.STRBUF2STRING__V40*/ meltfptr[39] = 0;
    /*^clear */
	   /*clear *//*_.OTUP__V41*/ meltfptr[40] = 0;
    /*^clear */
	   /*clear *//*_#MULTIPLE_LENGTH__L9*/ meltfnum[0] = 0;
    /*^clear */
	   /*clear *//*_.LAMBDA___V43*/ meltfptr[42] = 0;
    /*^clear */
	   /*clear *//*_.MULTIPLE_EVERY__V45*/ meltfptr[44] = 0;
    /*^clear */
	   /*clear *//*_.INST___V46*/ meltfptr[45] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V48*/ meltfptr[47] = 0;
    /*^clear */
	   /*clear *//*_.RETURN___V52*/ meltfptr[48] = 0;
    /*_.LET___V25*/ meltfptr[18] = /*_.LET___V39*/ meltfptr[37];;

    MELT_LOCATION ("warmelt-genobj.melt:5052:/ clear");
	   /*clear *//*_.NCOMPI__V26*/ meltfptr[19] = 0;
    /*^clear */
	   /*clear *//*_.LAMBDA___V27*/ meltfptr[20] = 0;
    /*^clear */
	   /*clear *//*_.OCOMPI__V29*/ meltfptr[28] = 0;
    /*^clear */
	   /*clear *//*_.DISX__V30*/ meltfptr[29] = 0;
    /*^clear */
	   /*clear *//*_.DRANK__V31*/ meltfptr[30] = 0;
    /*^clear */
	   /*clear *//*_.ODISCR__V32*/ meltfptr[31] = 0;
    /*^clear */
	   /*clear *//*_.NAM__V33*/ meltfptr[32] = 0;
    /*^clear */
	   /*clear *//*_.NAMBUF__V34*/ meltfptr[33] = 0;
    /*^clear */
	   /*clear *//*_.LOCVAR__V35*/ meltfptr[34] = 0;
    /*^clear */
	   /*clear *//*_#DRK__L8*/ meltfnum[3] = 0;
    /*^clear */
	   /*clear *//*_.INIBODY__V36*/ meltfptr[35] = 0;
    /*^clear */
	   /*clear *//*_.COMM__V37*/ meltfptr[36] = 0;
    /*^clear */
	   /*clear *//*_.LET___V39*/ meltfptr[37] = 0;
    /*_.LET___V12*/ meltfptr[8] = /*_.LET___V25*/ meltfptr[18];;

    MELT_LOCATION ("warmelt-genobj.melt:5042:/ clear");
	   /*clear *//*_.COMPICACHE__V13*/ meltfptr[9] = 0;
    /*^clear */
	   /*clear *//*_.CHOBJ__V14*/ meltfptr[13] = 0;
    /*^clear */
	   /*clear *//*_.INIROUT__V15*/ meltfptr[14] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V16*/ meltfptr[15] = 0;
    /*^clear */
	   /*clear *//*_.IF___V18*/ meltfptr[16] = 0;
    /*^clear */
	   /*clear *//*_.LET___V25*/ meltfptr[18] = 0;
    MELT_LOCATION ("warmelt-genobj.melt:5038:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.LET___V12*/ meltfptr[8];;

    {
      MELT_LOCATION ("warmelt-genobj.melt:5038:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V4*/ meltfptr[3] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V6*/ meltfptr[4] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V8*/ meltfptr[6] = 0;
    /*^clear */
	   /*clear *//*_.LET___V12*/ meltfptr[8] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("COMPILOBJ_DATATUPLE", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_147_warmelt_genobj_COMPILOBJ_DATATUPLE_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_147_warmelt_genobj_COMPILOBJ_DATATUPLE */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_148_warmelt_genobj_LAMBDA___49__ (meltclosure_ptr_t meltclosp_,
					   melt_ptr_t meltfirstargp_,
					   const melt_argdescr_cell_t
					   meltxargdescr_[],
					   union meltparam_un * meltxargtab_,
					   const melt_argdescr_cell_t
					   meltxresdescr_[],
					   union meltparam_un * meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_148_warmelt_genobj_LAMBDA___49___melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_148_warmelt_genobj_LAMBDA___49___st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 4
    melt_ptr_t mcfr_varptr[4];
#define MELTFRAM_NBVARNUM 1
    long mcfr_varnum[1];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_148_warmelt_genobj_LAMBDA___49__ is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_148_warmelt_genobj_LAMBDA___49___st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 4; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_148_warmelt_genobj_LAMBDA___49__ nbval 4*/
  meltfram__.mcfr_nbvar = 4 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("LAMBDA_", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-genobj.melt:5055:/ getarg");
 /*_.C__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_LONG)
    goto lab_endgetargs;
 /*_#IX__L1*/ meltfnum[0] = meltxargtab_[0].meltbp_long;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {

    /*^checksignal */
    MELT_CHECK_SIGNAL ();
    ;
    /*^cond */
    /*cond */ if ( /*_.C__V2*/ meltfptr[1])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  /*^checksignal */
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^msend */
	  /*msend */
	  {
	    union meltparam_un argtab[1];
	    memset (&argtab, 0, sizeof (argtab));
	    /*^ojbmsend.arg */
	    argtab[0].meltbp_aptr =
	      (melt_ptr_t *) & ( /*~GCX */ meltfclos->tabval[0]);
	    /*_.COMPILE_OBJ__V4*/ meltfptr[3] =
	      meltgc_send ((melt_ptr_t) ( /*_.C__V2*/ meltfptr[1]),
			   (melt_ptr_t) (( /*!COMPILE_OBJ */ meltfrout->
					  tabval[0])), (MELTBPARSTR_PTR ""),
			   argtab, "", (union meltparam_un *) 0);
	  }
	  ;
	  /*_.IF___V3*/ meltfptr[2] = /*_.COMPILE_OBJ__V4*/ meltfptr[3];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-genobj.melt:5055:/ clear");
	     /*clear *//*_.COMPILE_OBJ__V4*/ meltfptr[3] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

  /*_.IF___V3*/ meltfptr[2] = NULL;;
      }
    ;
    MELT_LOCATION ("warmelt-genobj.melt:5055:/ quasiblock");


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.IF___V3*/ meltfptr[2];;

    {
      MELT_LOCATION ("warmelt-genobj.melt:5055:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IF___V3*/ meltfptr[2] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("LAMBDA_", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_148_warmelt_genobj_LAMBDA___49___melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_148_warmelt_genobj_LAMBDA___49__ */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_149_warmelt_genobj_LAMBDA___50__ (meltclosure_ptr_t meltclosp_,
					   melt_ptr_t meltfirstargp_,
					   const melt_argdescr_cell_t
					   meltxargdescr_[],
					   union meltparam_un * meltxargtab_,
					   const melt_argdescr_cell_t
					   meltxresdescr_[],
					   union meltparam_un * meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_149_warmelt_genobj_LAMBDA___50___melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_149_warmelt_genobj_LAMBDA___50___st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 6
    melt_ptr_t mcfr_varptr[6];
#define MELTFRAM_NBVARNUM 1
    long mcfr_varnum[1];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_149_warmelt_genobj_LAMBDA___50__ is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_149_warmelt_genobj_LAMBDA___50___st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 6; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_149_warmelt_genobj_LAMBDA___50__ nbval 6*/
  meltfram__.mcfr_nbvar = 6 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("LAMBDA_", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-genobj.melt:5083:/ getarg");
 /*_.SCOMP__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_LONG)
    goto lab_endgetargs;
 /*_#SRK__L1*/ meltfnum[0] = meltxargtab_[0].meltbp_long;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {

    MELT_LOCATION ("warmelt-genobj.melt:5084:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^cond */
    /*cond */ if ( /*_.SCOMP__V2*/ meltfptr[1])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-genobj.melt:5085:/ quasiblock");


   /*_.OCOMP__V3*/ meltfptr[2] =
	    (melt_multiple_nth
	     ((melt_ptr_t) (( /*~OCOMPI */ meltfclos->tabval[0])),
	      ( /*_#SRK__L1*/ meltfnum[0])));;
	  MELT_LOCATION ("warmelt-genobj.melt:5087:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
   /*_.MAKE_INTEGERBOX__V4*/ meltfptr[3] =
	    (meltgc_new_int
	     ((meltobject_ptr_t)
	      (( /*!DISCR_INTEGER */ meltfrout->tabval[1])),
	      ( /*_#SRK__L1*/ meltfnum[0])));;
	  MELT_LOCATION ("warmelt-genobj.melt:5087:/ quasiblock");


	  /*^rawallocobj */
	  /*rawallocobj */
	  {
	    melt_ptr_t newobj = 0;
	    melt_raw_object_create (newobj,
				    (melt_ptr_t) (( /*!CLASS_OBJPUTUPLE */
						   meltfrout->tabval[0])),
				    (4), "CLASS_OBJPUTUPLE");
    /*_.INST__V6*/ meltfptr[5] =
	      newobj;
	  };
	  ;
	  /*^putslot */
	  /*putslot */
	  melt_assertmsg ("putslot checkobj @OPUTU_TUPLED",
			  melt_magic_discr ((melt_ptr_t)
					    ( /*_.INST__V6*/ meltfptr[5])) ==
			  MELTOBMAG_OBJECT);
	  melt_putfield_object (( /*_.INST__V6*/ meltfptr[5]), (1),
				(( /*~OTUP */ meltfclos->tabval[2])),
				"OPUTU_TUPLED");
	  ;
	  /*^putslot */
	  /*putslot */
	  melt_assertmsg ("putslot checkobj @OPUTU_OFFSET",
			  melt_magic_discr ((melt_ptr_t)
					    ( /*_.INST__V6*/ meltfptr[5])) ==
			  MELTOBMAG_OBJECT);
	  melt_putfield_object (( /*_.INST__V6*/ meltfptr[5]), (2),
				( /*_.MAKE_INTEGERBOX__V4*/ meltfptr[3]),
				"OPUTU_OFFSET");
	  ;
	  /*^putslot */
	  /*putslot */
	  melt_assertmsg ("putslot checkobj @OPUTU_VALUE",
			  melt_magic_discr ((melt_ptr_t)
					    ( /*_.INST__V6*/ meltfptr[5])) ==
			  MELTOBMAG_OBJECT);
	  melt_putfield_object (( /*_.INST__V6*/ meltfptr[5]), (3),
				( /*_.OCOMP__V3*/ meltfptr[2]),
				"OPUTU_VALUE");
	  ;
	  /*^touchobj */

	  melt_dbgtrace_written_object ( /*_.INST__V6*/ meltfptr[5],
					"newly made instance");
	  ;
	  /*_.INST___V5*/ meltfptr[4] = /*_.INST__V6*/ meltfptr[5];;

	  {
	    MELT_LOCATION ("warmelt-genobj.melt:5086:/ locexp");
	    meltgc_append_list ((melt_ptr_t)
				(( /*~INIBODY */ meltfclos->tabval[1])),
				(melt_ptr_t) ( /*_.INST___V5*/ meltfptr[4]));
	  }
	  ;

	  MELT_LOCATION ("warmelt-genobj.melt:5085:/ clear");
	     /*clear *//*_.OCOMP__V3*/ meltfptr[2] = 0;
	  /*^clear */
	     /*clear *//*_.MAKE_INTEGERBOX__V4*/ meltfptr[3] = 0;
	  /*^clear */
	     /*clear *//*_.INST___V5*/ meltfptr[4] = 0;
	  /*epilog */
	}
	;
      }				/*noelse */
    ;
    /*epilog */
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("LAMBDA_", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_149_warmelt_genobj_LAMBDA___50___melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*noretval */ NULL);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_149_warmelt_genobj_LAMBDA___50__ */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_150_warmelt_genobj_COMPILOBJ_DATASTRING (meltclosure_ptr_t
						  meltclosp_,
						  melt_ptr_t meltfirstargp_,
						  const melt_argdescr_cell_t
						  meltxargdescr_[],
						  union meltparam_un *
						  meltxargtab_,
						  const melt_argdescr_cell_t
						  meltxresdescr_[],
						  union meltparam_un *
						  meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_150_warmelt_genobj_COMPILOBJ_DATASTRING_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_150_warmelt_genobj_COMPILOBJ_DATASTRING_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 28
    melt_ptr_t mcfr_varptr[28];
#define MELTFRAM_NBVARNUM 5
    long mcfr_varnum[5];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_150_warmelt_genobj_COMPILOBJ_DATASTRING is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_150_warmelt_genobj_COMPILOBJ_DATASTRING_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 28; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_150_warmelt_genobj_COMPILOBJ_DATASTRING nbval 28*/
  meltfram__.mcfr_nbvar = 28 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("COMPILOBJ_DATASTRING", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-genobj.melt:5108:/ getarg");
 /*_.NDS__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.GCX__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.GCX__V3*/ meltfptr[2])) != NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:5109:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L1*/ meltfnum[0] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.NDS__V2*/ meltfptr[1]),
			     (melt_ptr_t) (( /*!CLASS_NREP_DATASTRING */
					    meltfrout->tabval[0])));;
      MELT_LOCATION ("warmelt-genobj.melt:5109:/ cond");
      /*cond */ if ( /*_#IS_A__L1*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V5*/ meltfptr[4] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-genobj.melt:5109:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check nds"),
				  ("warmelt-genobj.melt")
				  ? ("warmelt-genobj.melt") : __FILE__,
				  (5109) ? (5109) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V5*/ meltfptr[4] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V4*/ meltfptr[3] = /*_.IFELSE___V5*/ meltfptr[4];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:5109:/ clear");
	     /*clear *//*_#IS_A__L1*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V5*/ meltfptr[4] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V4*/ meltfptr[3] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:5110:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L2*/ meltfnum[0] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.GCX__V3*/ meltfptr[2]),
			     (melt_ptr_t) (( /*!CLASS_C_GENERATION_CONTEXT */
					    meltfrout->tabval[1])));;
      MELT_LOCATION ("warmelt-genobj.melt:5110:/ cond");
      /*cond */ if ( /*_#IS_A__L2*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V7*/ meltfptr[6] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-genobj.melt:5110:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check gcx"),
				  ("warmelt-genobj.melt")
				  ? ("warmelt-genobj.melt") : __FILE__,
				  (5110) ? (5110) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V7*/ meltfptr[6] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V6*/ meltfptr[4] = /*_.IFELSE___V7*/ meltfptr[6];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:5110:/ clear");
	     /*clear *//*_#IS_A__L2*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V7*/ meltfptr[6] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V6*/ meltfptr[4] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-genobj.melt:5111:/ quasiblock");


    /*^getslot */
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.GCX__V3*/ meltfptr[2]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 6, "GNCX_COMPICACHE");
  /*_.COMPICACHE__V9*/ meltfptr[8] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-genobj.melt:5112:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.GCX__V3*/ meltfptr[2]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 0, "GNCX_OBJROUT");
  /*_.INIROUT__V10*/ meltfptr[9] = slot;
    };
    ;
 /*_.CHOBJ__V11*/ meltfptr[10] =
      /*mapobject_get */
      melt_get_mapobjects ((meltmapobjects_ptr_t)
			   ( /*_.COMPICACHE__V9*/ meltfptr[8]),
			   (meltobject_ptr_t) ( /*_.NDS__V2*/ meltfptr[1]));;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:5116:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L3*/ meltfnum[0] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.INIROUT__V10*/ meltfptr[9]),
			     (melt_ptr_t) (( /*!CLASS_INITIALROUTINEOBJ */
					    meltfrout->tabval[2])));;
      MELT_LOCATION ("warmelt-genobj.melt:5116:/ cond");
      /*cond */ if ( /*_#IS_A__L3*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V13*/ meltfptr[12] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-genobj.melt:5116:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check inirout"),
				  ("warmelt-genobj.melt")
				  ? ("warmelt-genobj.melt") : __FILE__,
				  (5116) ? (5116) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V13*/ meltfptr[12] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V12*/ meltfptr[11] = /*_.IFELSE___V13*/ meltfptr[12];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:5116:/ clear");
	     /*clear *//*_#IS_A__L3*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V13*/ meltfptr[12] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V12*/ meltfptr[11] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-genobj.melt:5117:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^cond */
    /*cond */ if ( /*_.CHOBJ__V11*/ meltfptr[10])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-genobj.melt:5119:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^quasiblock */


	  /*_.RETVAL___V1*/ meltfptr[0] = /*_.CHOBJ__V11*/ meltfptr[10];;

	  {
	    MELT_LOCATION ("warmelt-genobj.melt:5119:/ locexp");
	    /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
	    if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	      melt_warn_for_no_expected_secondary_results ();
	    /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
	    ;
	  }
	  ;
	  /*^finalreturn */
	  ;
	  /*finalret */ goto labend_rout;
	  MELT_LOCATION ("warmelt-genobj.melt:5118:/ quasiblock");


	  /*_.PROGN___V16*/ meltfptr[15] = /*_.RETURN___V15*/ meltfptr[14];;
	  /*^compute */
	  /*_.IF___V14*/ meltfptr[12] = /*_.PROGN___V16*/ meltfptr[15];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-genobj.melt:5117:/ clear");
	     /*clear *//*_.RETURN___V15*/ meltfptr[14] = 0;
	  /*^clear */
	     /*clear *//*_.PROGN___V16*/ meltfptr[15] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

  /*_.IF___V14*/ meltfptr[12] = NULL;;
      }
    ;
    MELT_LOCATION ("warmelt-genobj.melt:5120:/ quasiblock");


 /*_.NAMBUF__V18*/ meltfptr[15] =
      (melt_ptr_t)
      meltgc_new_strbuf ((meltobject_ptr_t)
			 (( /*!DISCR_STRBUF */ meltfrout->tabval[3])),
			 (const char *) 0);;
    MELT_LOCATION ("warmelt-genobj.melt:5122:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.NDS__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 4, "NDATA_DISCRX");
  /*_.NDISX__V19*/ meltfptr[18] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-genobj.melt:5123:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^msend */
    /*msend */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^ojbmsend.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.GCX__V3*/ meltfptr[2];
      /*_.ODISCR__V20*/ meltfptr[19] =
	meltgc_send ((melt_ptr_t) ( /*_.NDISX__V19*/ meltfptr[18]),
		     (melt_ptr_t) (( /*!COMPILE_OBJ */ meltfrout->tabval[4])),
		     (MELTBPARSTR_PTR ""), argtab, "",
		     (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-genobj.melt:5124:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.NDS__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 5, "NSTR_STRING");
  /*_.ODATA__V21*/ meltfptr[20] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-genobj.melt:5125:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.NDS__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 2, "NDATA_RANK");
  /*_.DRANK__V22*/ meltfptr[21] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-genobj.melt:5126:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.NDS__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 1, "NDATA_NAME");
  /*_.NAM__V23*/ meltfptr[22] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-genobj.melt:5127:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & ( /*!konst_6_VALSTR_ */ meltfrout->tabval[6]);
      /*_.LOCVAR__V24*/ meltfptr[23] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!GET_FREE_OBJLOCPTR */ meltfrout->tabval[5])),
		    (melt_ptr_t) ( /*_.GCX__V3*/ meltfptr[2]),
		    (MELTBPARSTR_PTR ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;
 /*_#DRK__L4*/ meltfnum[0] =
      (melt_get_int ((melt_ptr_t) ( /*_.DRANK__V22*/ meltfptr[21])));;
    MELT_LOCATION ("warmelt-genobj.melt:5129:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*^rawallocobj */
    /*rawallocobj */
    {
      melt_ptr_t newobj = 0;
      melt_raw_object_create (newobj,
			      (melt_ptr_t) (( /*!CLASS_OBJINITSTRING */
					     meltfrout->tabval[7])), (5),
			      "CLASS_OBJINITSTRING");
  /*_.INST__V26*/ meltfptr[25] =
	newobj;
    };
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OBV_TYPE",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V26*/ meltfptr[25])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V26*/ meltfptr[25]), (0),
			  (( /*!CTYPE_VALUE */ meltfrout->tabval[8])),
			  "OBV_TYPE");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OIE_DATA",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V26*/ meltfptr[25])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V26*/ meltfptr[25]), (2),
			  ( /*_.ODATA__V21*/ meltfptr[20]), "OIE_DATA");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OIE_DISCR",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V26*/ meltfptr[25])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V26*/ meltfptr[25]), (3),
			  ( /*_.ODISCR__V20*/ meltfptr[19]), "OIE_DISCR");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OIE_LOCVAR",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V26*/ meltfptr[25])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V26*/ meltfptr[25]), (4),
			  ( /*_.LOCVAR__V24*/ meltfptr[23]), "OIE_LOCVAR");
    ;
    /*^touchobj */

    melt_dbgtrace_written_object ( /*_.INST__V26*/ meltfptr[25],
				  "newly made instance");
    ;
    /*_.OSTR__V25*/ meltfptr[24] = /*_.INST__V26*/ meltfptr[25];;

    {
      MELT_LOCATION ("warmelt-genobj.melt:5136:/ locexp");
      meltgc_put_mapobjects ((meltmapobjects_ptr_t)
			     ( /*_.COMPICACHE__V9*/ meltfptr[8]),
			     (meltobject_ptr_t) ( /*_.NDS__V2*/ meltfptr[1]),
			     (melt_ptr_t) ( /*_.OSTR__V25*/ meltfptr[24]));
    }
    ;

    {
      MELT_LOCATION ("warmelt-genobj.melt:5137:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.NAMBUF__V18*/ meltfptr[15]),
			   ("dstr_"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-genobj.melt:5138:/ locexp");
      meltgc_add_strbuf_dec ((melt_ptr_t) ( /*_.NAMBUF__V18*/ meltfptr[15]),
			     ( /*_#DRK__L4*/ meltfnum[0]));
    }
    ;

    {
      MELT_LOCATION ("warmelt-genobj.melt:5139:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.NAMBUF__V18*/ meltfptr[15]),
			   ("__"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-genobj.melt:5140:/ locexp");
      meltgc_add_strbuf_cidentprefix ((melt_ptr_t)
				      ( /*_.NAMBUF__V18*/ meltfptr[15]),
				      melt_string_str ((melt_ptr_t)
						       ( /*_.ODATA__V21*/
							meltfptr[20])), (16));
    }
    ;
 /*_#STRING_LENGTH__L5*/ meltfnum[4] =
      melt_string_length ((melt_ptr_t) ( /*_.ODATA__V21*/ meltfptr[20]));;

    {
      MELT_LOCATION ("warmelt-genobj.melt:5141:/ locexp");
      melt_put_int ((melt_ptr_t) ( /*_.OSTR__V25*/ meltfptr[24]),
		    ( /*_#STRING_LENGTH__L5*/ meltfnum[4]));
    }
    ;
    MELT_LOCATION ("warmelt-genobj.melt:5142:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^cond */
    /*cond */ if ( /*_.NAM__V23*/ meltfptr[22])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{


	  {
	    MELT_LOCATION ("warmelt-genobj.melt:5144:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t)
				 ( /*_.NAMBUF__V18*/ meltfptr[15]), ("__"));
	  }
	  ;
	  MELT_LOCATION ("warmelt-genobj.melt:5145:/ getslot");
	  {
	    melt_ptr_t slot = NULL, obj = NULL;
	    obj = (melt_ptr_t) ( /*_.NAM__V23*/ meltfptr[22]) /*=obj*/ ;
	    melt_object_get_field (slot, obj, 1, "NAMED_NAME");
    /*_.NAMED_NAME__V27*/ meltfptr[26] = slot;
	  };
	  ;

	  {
	    /*^locexp */
	    meltgc_add_strbuf_cident ((melt_ptr_t)
				      ( /*_.NAMBUF__V18*/ meltfptr[15]),
				      melt_string_str ((melt_ptr_t)
						       ( /*_.NAMED_NAME__V27*/
							meltfptr[26])));
	  }
	  ;
	  MELT_LOCATION ("warmelt-genobj.melt:5143:/ quasiblock");


	  /*epilog */

	  MELT_LOCATION ("warmelt-genobj.melt:5142:/ clear");
	     /*clear *//*_.NAMED_NAME__V27*/ meltfptr[26] = 0;
	}
	;
      }				/*noelse */
    ;
    MELT_LOCATION ("warmelt-genobj.melt:5146:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_.STRBUF2STRING__V28*/ meltfptr[26] =
      (meltgc_new_stringdup
       ((meltobject_ptr_t) (( /*!DISCR_STRING */ meltfrout->tabval[9])),
	melt_strbuf_str ((melt_ptr_t) ( /*_.NAMBUF__V18*/ meltfptr[15]))));;
    MELT_LOCATION ("warmelt-genobj.melt:5146:/ quasiblock");


    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OIE_CNAME",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.OSTR__V25*/ meltfptr[24])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.OSTR__V25*/ meltfptr[24]), (1),
			  ( /*_.STRBUF2STRING__V28*/ meltfptr[26]),
			  "OIE_CNAME");
    ;
    /*^touch */
    meltgc_touch ( /*_.OSTR__V25*/ meltfptr[24]);
    ;
    /*^touchobj */

    melt_dbgtrace_written_object ( /*_.OSTR__V25*/ meltfptr[24],
				  "put-fields");
    ;

    /*_.LET___V17*/ meltfptr[14] = /*_.OSTR__V25*/ meltfptr[24];;

    MELT_LOCATION ("warmelt-genobj.melt:5120:/ clear");
	   /*clear *//*_.NAMBUF__V18*/ meltfptr[15] = 0;
    /*^clear */
	   /*clear *//*_.NDISX__V19*/ meltfptr[18] = 0;
    /*^clear */
	   /*clear *//*_.ODISCR__V20*/ meltfptr[19] = 0;
    /*^clear */
	   /*clear *//*_.ODATA__V21*/ meltfptr[20] = 0;
    /*^clear */
	   /*clear *//*_.DRANK__V22*/ meltfptr[21] = 0;
    /*^clear */
	   /*clear *//*_.NAM__V23*/ meltfptr[22] = 0;
    /*^clear */
	   /*clear *//*_.LOCVAR__V24*/ meltfptr[23] = 0;
    /*^clear */
	   /*clear *//*_#DRK__L4*/ meltfnum[0] = 0;
    /*^clear */
	   /*clear *//*_.OSTR__V25*/ meltfptr[24] = 0;
    /*^clear */
	   /*clear *//*_#STRING_LENGTH__L5*/ meltfnum[4] = 0;
    /*^clear */
	   /*clear *//*_.STRBUF2STRING__V28*/ meltfptr[26] = 0;
    /*_.LET___V8*/ meltfptr[6] = /*_.LET___V17*/ meltfptr[14];;

    MELT_LOCATION ("warmelt-genobj.melt:5111:/ clear");
	   /*clear *//*_.COMPICACHE__V9*/ meltfptr[8] = 0;
    /*^clear */
	   /*clear *//*_.INIROUT__V10*/ meltfptr[9] = 0;
    /*^clear */
	   /*clear *//*_.CHOBJ__V11*/ meltfptr[10] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V12*/ meltfptr[11] = 0;
    /*^clear */
	   /*clear *//*_.IF___V14*/ meltfptr[12] = 0;
    /*^clear */
	   /*clear *//*_.LET___V17*/ meltfptr[14] = 0;
    MELT_LOCATION ("warmelt-genobj.melt:5108:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.LET___V8*/ meltfptr[6];;

    {
      MELT_LOCATION ("warmelt-genobj.melt:5108:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V4*/ meltfptr[3] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V6*/ meltfptr[4] = 0;
    /*^clear */
	   /*clear *//*_.LET___V8*/ meltfptr[6] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("COMPILOBJ_DATASTRING", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_150_warmelt_genobj_COMPILOBJ_DATASTRING_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_150_warmelt_genobj_COMPILOBJ_DATASTRING */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_151_warmelt_genobj_COMPILOBJ_DATABOXEDINTEGER (meltclosure_ptr_t
							meltclosp_,
							melt_ptr_t
							meltfirstargp_,
							const
							melt_argdescr_cell_t
							meltxargdescr_[],
							union meltparam_un *
							meltxargtab_,
							const
							melt_argdescr_cell_t
							meltxresdescr_[],
							union meltparam_un *
							meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_151_warmelt_genobj_COMPILOBJ_DATABOXEDINTEGER_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_151_warmelt_genobj_COMPILOBJ_DATABOXEDINTEGER_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 28
    melt_ptr_t mcfr_varptr[28];
#define MELTFRAM_NBVARNUM 4
    long mcfr_varnum[4];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_151_warmelt_genobj_COMPILOBJ_DATABOXEDINTEGER is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct
	 frame_meltrout_151_warmelt_genobj_COMPILOBJ_DATABOXEDINTEGER_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 28; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_151_warmelt_genobj_COMPILOBJ_DATABOXEDINTEGER nbval 28*/
  meltfram__.mcfr_nbvar = 28 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("COMPILOBJ_DATABOXEDINTEGER", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-genobj.melt:5153:/ getarg");
 /*_.NDI__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.GCX__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.GCX__V3*/ meltfptr[2])) != NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:5154:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L1*/ meltfnum[0] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.NDI__V2*/ meltfptr[1]),
			     (melt_ptr_t) (( /*!CLASS_NREP_DATABOXEDINTEGER */
					    meltfrout->tabval[0])));;
      MELT_LOCATION ("warmelt-genobj.melt:5154:/ cond");
      /*cond */ if ( /*_#IS_A__L1*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V5*/ meltfptr[4] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-genobj.melt:5154:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check ndi"),
				  ("warmelt-genobj.melt")
				  ? ("warmelt-genobj.melt") : __FILE__,
				  (5154) ? (5154) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V5*/ meltfptr[4] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V4*/ meltfptr[3] = /*_.IFELSE___V5*/ meltfptr[4];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:5154:/ clear");
	     /*clear *//*_#IS_A__L1*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V5*/ meltfptr[4] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V4*/ meltfptr[3] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:5155:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L2*/ meltfnum[0] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.GCX__V3*/ meltfptr[2]),
			     (melt_ptr_t) (( /*!CLASS_C_GENERATION_CONTEXT */
					    meltfrout->tabval[1])));;
      MELT_LOCATION ("warmelt-genobj.melt:5155:/ cond");
      /*cond */ if ( /*_#IS_A__L2*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V7*/ meltfptr[6] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-genobj.melt:5155:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check gcx"),
				  ("warmelt-genobj.melt")
				  ? ("warmelt-genobj.melt") : __FILE__,
				  (5155) ? (5155) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V7*/ meltfptr[6] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V6*/ meltfptr[4] = /*_.IFELSE___V7*/ meltfptr[6];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:5155:/ clear");
	     /*clear *//*_#IS_A__L2*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V7*/ meltfptr[6] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V6*/ meltfptr[4] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-genobj.melt:5156:/ quasiblock");


    /*^getslot */
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.GCX__V3*/ meltfptr[2]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 6, "GNCX_COMPICACHE");
  /*_.COMPICACHE__V9*/ meltfptr[8] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-genobj.melt:5157:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.GCX__V3*/ meltfptr[2]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 0, "GNCX_OBJROUT");
  /*_.INIROUT__V10*/ meltfptr[9] = slot;
    };
    ;
 /*_.CHOBJ__V11*/ meltfptr[10] =
      /*mapobject_get */
      melt_get_mapobjects ((meltmapobjects_ptr_t)
			   ( /*_.COMPICACHE__V9*/ meltfptr[8]),
			   (meltobject_ptr_t) ( /*_.NDI__V2*/ meltfptr[1]));;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:5161:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L3*/ meltfnum[0] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.INIROUT__V10*/ meltfptr[9]),
			     (melt_ptr_t) (( /*!CLASS_INITIALROUTINEOBJ */
					    meltfrout->tabval[2])));;
      MELT_LOCATION ("warmelt-genobj.melt:5161:/ cond");
      /*cond */ if ( /*_#IS_A__L3*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V13*/ meltfptr[12] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-genobj.melt:5161:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check inirout"),
				  ("warmelt-genobj.melt")
				  ? ("warmelt-genobj.melt") : __FILE__,
				  (5161) ? (5161) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V13*/ meltfptr[12] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V12*/ meltfptr[11] = /*_.IFELSE___V13*/ meltfptr[12];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:5161:/ clear");
	     /*clear *//*_#IS_A__L3*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V13*/ meltfptr[12] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V12*/ meltfptr[11] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-genobj.melt:5162:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^cond */
    /*cond */ if ( /*_.CHOBJ__V11*/ meltfptr[10])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-genobj.melt:5164:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^quasiblock */


	  /*_.RETVAL___V1*/ meltfptr[0] = /*_.CHOBJ__V11*/ meltfptr[10];;

	  {
	    MELT_LOCATION ("warmelt-genobj.melt:5164:/ locexp");
	    /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
	    if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	      melt_warn_for_no_expected_secondary_results ();
	    /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
	    ;
	  }
	  ;
	  /*^finalreturn */
	  ;
	  /*finalret */ goto labend_rout;
	  MELT_LOCATION ("warmelt-genobj.melt:5163:/ quasiblock");


	  /*_.PROGN___V16*/ meltfptr[15] = /*_.RETURN___V15*/ meltfptr[14];;
	  /*^compute */
	  /*_.IF___V14*/ meltfptr[12] = /*_.PROGN___V16*/ meltfptr[15];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-genobj.melt:5162:/ clear");
	     /*clear *//*_.RETURN___V15*/ meltfptr[14] = 0;
	  /*^clear */
	     /*clear *//*_.PROGN___V16*/ meltfptr[15] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

  /*_.IF___V14*/ meltfptr[12] = NULL;;
      }
    ;
    MELT_LOCATION ("warmelt-genobj.melt:5165:/ quasiblock");


 /*_.NAMBUF__V18*/ meltfptr[15] =
      (melt_ptr_t)
      meltgc_new_strbuf ((meltobject_ptr_t)
			 (( /*!DISCR_STRBUF */ meltfrout->tabval[3])),
			 (const char *) 0);;
    MELT_LOCATION ("warmelt-genobj.melt:5167:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.NDI__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 4, "NDATA_DISCRX");
  /*_.NDISX__V19*/ meltfptr[18] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-genobj.melt:5168:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^msend */
    /*msend */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^ojbmsend.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.GCX__V3*/ meltfptr[2];
      /*_.ODISCR__V20*/ meltfptr[19] =
	meltgc_send ((melt_ptr_t) ( /*_.NDISX__V19*/ meltfptr[18]),
		     (melt_ptr_t) (( /*!COMPILE_OBJ */ meltfrout->tabval[4])),
		     (MELTBPARSTR_PTR ""), argtab, "",
		     (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-genobj.melt:5169:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.NDI__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 5, "NBOXINT_NUM");
  /*_.ODATA__V21*/ meltfptr[20] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-genobj.melt:5170:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.NDI__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 2, "NDATA_RANK");
  /*_.DRANK__V22*/ meltfptr[21] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-genobj.melt:5171:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.NDI__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 1, "NDATA_NAME");
  /*_.NAM__V23*/ meltfptr[22] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-genobj.melt:5172:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & ( /*!konst_6_VALBXINT_ */ meltfrout->tabval[6]);
      /*_.LOCVAR__V24*/ meltfptr[23] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!GET_FREE_OBJLOCPTR */ meltfrout->tabval[5])),
		    (melt_ptr_t) ( /*_.GCX__V3*/ meltfptr[2]),
		    (MELTBPARSTR_PTR ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;
 /*_#DRK__L4*/ meltfnum[0] =
      (melt_get_int ((melt_ptr_t) ( /*_.DRANK__V22*/ meltfptr[21])));;
    MELT_LOCATION ("warmelt-genobj.melt:5174:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*^rawallocobj */
    /*rawallocobj */
    {
      melt_ptr_t newobj = 0;
      melt_raw_object_create (newobj,
			      (melt_ptr_t) (( /*!CLASS_OBJINITBOXINTEGER */
					     meltfrout->tabval[7])), (5),
			      "CLASS_OBJINITBOXINTEGER");
  /*_.INST__V26*/ meltfptr[25] =
	newobj;
    };
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OBV_TYPE",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V26*/ meltfptr[25])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V26*/ meltfptr[25]), (0),
			  (( /*!CTYPE_VALUE */ meltfrout->tabval[8])),
			  "OBV_TYPE");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OIE_DATA",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V26*/ meltfptr[25])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V26*/ meltfptr[25]), (2),
			  ( /*_.ODATA__V21*/ meltfptr[20]), "OIE_DATA");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OIE_DISCR",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V26*/ meltfptr[25])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V26*/ meltfptr[25]), (3),
			  ( /*_.ODISCR__V20*/ meltfptr[19]), "OIE_DISCR");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OIE_LOCVAR",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V26*/ meltfptr[25])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V26*/ meltfptr[25]), (4),
			  ( /*_.LOCVAR__V24*/ meltfptr[23]), "OIE_LOCVAR");
    ;
    /*^touchobj */

    melt_dbgtrace_written_object ( /*_.INST__V26*/ meltfptr[25],
				  "newly made instance");
    ;
    /*_.OINT__V25*/ meltfptr[24] = /*_.INST__V26*/ meltfptr[25];;

    {
      MELT_LOCATION ("warmelt-genobj.melt:5181:/ locexp");
      meltgc_put_mapobjects ((meltmapobjects_ptr_t)
			     ( /*_.COMPICACHE__V9*/ meltfptr[8]),
			     (meltobject_ptr_t) ( /*_.NDI__V2*/ meltfptr[1]),
			     (melt_ptr_t) ( /*_.OINT__V25*/ meltfptr[24]));
    }
    ;

    {
      MELT_LOCATION ("warmelt-genobj.melt:5182:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.NAMBUF__V18*/ meltfptr[15]),
			   ("dint_"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-genobj.melt:5183:/ locexp");
      meltgc_add_strbuf_dec ((melt_ptr_t) ( /*_.NAMBUF__V18*/ meltfptr[15]),
			     ( /*_#DRK__L4*/ meltfnum[0]));
    }
    ;

    {
      MELT_LOCATION ("warmelt-genobj.melt:5184:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.NAMBUF__V18*/ meltfptr[15]),
			   ("__"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-genobj.melt:5185:/ locexp");
      meltgc_add_strbuf_cidentprefix ((melt_ptr_t)
				      ( /*_.NAMBUF__V18*/ meltfptr[15]),
				      melt_string_str ((melt_ptr_t)
						       ( /*_.ODATA__V21*/
							meltfptr[20])), (16));
    }
    ;
    MELT_LOCATION ("warmelt-genobj.melt:5186:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^cond */
    /*cond */ if ( /*_.NAM__V23*/ meltfptr[22])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{


	  {
	    MELT_LOCATION ("warmelt-genobj.melt:5188:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t)
				 ( /*_.NAMBUF__V18*/ meltfptr[15]), ("__"));
	  }
	  ;
	  MELT_LOCATION ("warmelt-genobj.melt:5189:/ getslot");
	  {
	    melt_ptr_t slot = NULL, obj = NULL;
	    obj = (melt_ptr_t) ( /*_.NAM__V23*/ meltfptr[22]) /*=obj*/ ;
	    melt_object_get_field (slot, obj, 1, "NAMED_NAME");
    /*_.NAMED_NAME__V27*/ meltfptr[26] = slot;
	  };
	  ;

	  {
	    /*^locexp */
	    meltgc_add_strbuf_cident ((melt_ptr_t)
				      ( /*_.NAMBUF__V18*/ meltfptr[15]),
				      melt_string_str ((melt_ptr_t)
						       ( /*_.NAMED_NAME__V27*/
							meltfptr[26])));
	  }
	  ;
	  MELT_LOCATION ("warmelt-genobj.melt:5187:/ quasiblock");


	  /*epilog */

	  MELT_LOCATION ("warmelt-genobj.melt:5186:/ clear");
	     /*clear *//*_.NAMED_NAME__V27*/ meltfptr[26] = 0;
	}
	;
      }				/*noelse */
    ;
    MELT_LOCATION ("warmelt-genobj.melt:5190:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_.STRBUF2STRING__V28*/ meltfptr[26] =
      (meltgc_new_stringdup
       ((meltobject_ptr_t) (( /*!DISCR_STRING */ meltfrout->tabval[9])),
	melt_strbuf_str ((melt_ptr_t) ( /*_.NAMBUF__V18*/ meltfptr[15]))));;
    MELT_LOCATION ("warmelt-genobj.melt:5190:/ quasiblock");


    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OIE_CNAME",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.OINT__V25*/ meltfptr[24])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.OINT__V25*/ meltfptr[24]), (1),
			  ( /*_.STRBUF2STRING__V28*/ meltfptr[26]),
			  "OIE_CNAME");
    ;
    /*^touch */
    meltgc_touch ( /*_.OINT__V25*/ meltfptr[24]);
    ;
    /*^touchobj */

    melt_dbgtrace_written_object ( /*_.OINT__V25*/ meltfptr[24],
				  "put-fields");
    ;

    /*_.LET___V17*/ meltfptr[14] = /*_.OINT__V25*/ meltfptr[24];;

    MELT_LOCATION ("warmelt-genobj.melt:5165:/ clear");
	   /*clear *//*_.NAMBUF__V18*/ meltfptr[15] = 0;
    /*^clear */
	   /*clear *//*_.NDISX__V19*/ meltfptr[18] = 0;
    /*^clear */
	   /*clear *//*_.ODISCR__V20*/ meltfptr[19] = 0;
    /*^clear */
	   /*clear *//*_.ODATA__V21*/ meltfptr[20] = 0;
    /*^clear */
	   /*clear *//*_.DRANK__V22*/ meltfptr[21] = 0;
    /*^clear */
	   /*clear *//*_.NAM__V23*/ meltfptr[22] = 0;
    /*^clear */
	   /*clear *//*_.LOCVAR__V24*/ meltfptr[23] = 0;
    /*^clear */
	   /*clear *//*_#DRK__L4*/ meltfnum[0] = 0;
    /*^clear */
	   /*clear *//*_.OINT__V25*/ meltfptr[24] = 0;
    /*^clear */
	   /*clear *//*_.STRBUF2STRING__V28*/ meltfptr[26] = 0;
    /*_.LET___V8*/ meltfptr[6] = /*_.LET___V17*/ meltfptr[14];;

    MELT_LOCATION ("warmelt-genobj.melt:5156:/ clear");
	   /*clear *//*_.COMPICACHE__V9*/ meltfptr[8] = 0;
    /*^clear */
	   /*clear *//*_.INIROUT__V10*/ meltfptr[9] = 0;
    /*^clear */
	   /*clear *//*_.CHOBJ__V11*/ meltfptr[10] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V12*/ meltfptr[11] = 0;
    /*^clear */
	   /*clear *//*_.IF___V14*/ meltfptr[12] = 0;
    /*^clear */
	   /*clear *//*_.LET___V17*/ meltfptr[14] = 0;
    MELT_LOCATION ("warmelt-genobj.melt:5153:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.LET___V8*/ meltfptr[6];;

    {
      MELT_LOCATION ("warmelt-genobj.melt:5153:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V4*/ meltfptr[3] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V6*/ meltfptr[4] = 0;
    /*^clear */
	   /*clear *//*_.LET___V8*/ meltfptr[6] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("COMPILOBJ_DATABOXEDINTEGER", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_151_warmelt_genobj_COMPILOBJ_DATABOXEDINTEGER_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_151_warmelt_genobj_COMPILOBJ_DATABOXEDINTEGER */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_152_warmelt_genobj_COMPILOBJ_DATACLOSURE (meltclosure_ptr_t
						   meltclosp_,
						   melt_ptr_t meltfirstargp_,
						   const melt_argdescr_cell_t
						   meltxargdescr_[],
						   union meltparam_un *
						   meltxargtab_,
						   const melt_argdescr_cell_t
						   meltxresdescr_[],
						   union meltparam_un *
						   meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_152_warmelt_genobj_COMPILOBJ_DATACLOSURE_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_152_warmelt_genobj_COMPILOBJ_DATACLOSURE_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 59
    melt_ptr_t mcfr_varptr[59];
#define MELTFRAM_NBVARNUM 12
    long mcfr_varnum[12];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_152_warmelt_genobj_COMPILOBJ_DATACLOSURE is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_152_warmelt_genobj_COMPILOBJ_DATACLOSURE_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 59; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_152_warmelt_genobj_COMPILOBJ_DATACLOSURE nbval 59*/
  meltfram__.mcfr_nbvar = 59 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("COMPILOBJ_DATACLOSURE", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-genobj.melt:5198:/ getarg");
 /*_.NCL__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.GCX__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.GCX__V3*/ meltfptr[2])) != NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:5199:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L1*/ meltfnum[0] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.NCL__V2*/ meltfptr[1]),
			     (melt_ptr_t) (( /*!CLASS_NREP_DATACLOSURE */
					    meltfrout->tabval[0])));;
      MELT_LOCATION ("warmelt-genobj.melt:5199:/ cond");
      /*cond */ if ( /*_#IS_A__L1*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V5*/ meltfptr[4] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-genobj.melt:5199:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check ncl"),
				  ("warmelt-genobj.melt")
				  ? ("warmelt-genobj.melt") : __FILE__,
				  (5199) ? (5199) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V5*/ meltfptr[4] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V4*/ meltfptr[3] = /*_.IFELSE___V5*/ meltfptr[4];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:5199:/ clear");
	     /*clear *//*_#IS_A__L1*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V5*/ meltfptr[4] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V4*/ meltfptr[3] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:5200:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L2*/ meltfnum[0] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.GCX__V3*/ meltfptr[2]),
			     (melt_ptr_t) (( /*!CLASS_C_GENERATION_CONTEXT */
					    meltfrout->tabval[1])));;
      MELT_LOCATION ("warmelt-genobj.melt:5200:/ cond");
      /*cond */ if ( /*_#IS_A__L2*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V7*/ meltfptr[6] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-genobj.melt:5200:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check gcx"),
				  ("warmelt-genobj.melt")
				  ? ("warmelt-genobj.melt") : __FILE__,
				  (5200) ? (5200) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V7*/ meltfptr[6] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V6*/ meltfptr[4] = /*_.IFELSE___V7*/ meltfptr[6];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:5200:/ clear");
	     /*clear *//*_#IS_A__L2*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V7*/ meltfptr[6] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V6*/ meltfptr[4] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:5201:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L3*/ meltfnum[0] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-genobj.melt:5201:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L3*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L4*/ meltfnum[3] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-genobj.melt:5201:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L4*/ meltfnum[3];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-genobj.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 5201;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "compilobj_dataclosure ncl=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.NCL__V2*/ meltfptr[1];
	      /*_.MELT_DEBUG_FUN__V10*/ meltfptr[9] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[2])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V9*/ meltfptr[8] =
	      /*_.MELT_DEBUG_FUN__V10*/ meltfptr[9];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-genobj.melt:5201:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L4*/ meltfnum[3] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V10*/ meltfptr[9] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V9*/ meltfptr[8] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-genobj.melt:5201:/ quasiblock");


      /*_.PROGN___V11*/ meltfptr[9] = /*_.IF___V9*/ meltfptr[8];;
      /*^compute */
      /*_.IFCPP___V8*/ meltfptr[6] = /*_.PROGN___V11*/ meltfptr[9];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:5201:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L3*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IF___V9*/ meltfptr[8] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V11*/ meltfptr[9] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V8*/ meltfptr[6] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-genobj.melt:5202:/ quasiblock");


    /*^getslot */
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.GCX__V3*/ meltfptr[2]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 6, "GNCX_COMPICACHE");
  /*_.COMPICACHE__V13*/ meltfptr[9] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-genobj.melt:5203:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.GCX__V3*/ meltfptr[2]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 0, "GNCX_OBJROUT");
  /*_.INIROUT__V14*/ meltfptr[13] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-genobj.melt:5204:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.INIROUT__V14*/ meltfptr[13]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 3, "OBROUT_BODY");
  /*_.INIBODY__V15*/ meltfptr[14] = slot;
    };
    ;
 /*_.CHOBJ__V16*/ meltfptr[15] =
      /*mapobject_get */
      melt_get_mapobjects ((meltmapobjects_ptr_t)
			   ( /*_.COMPICACHE__V13*/ meltfptr[9]),
			   (meltobject_ptr_t) ( /*_.NCL__V2*/ meltfptr[1]));;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:5207:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L5*/ meltfnum[3] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.INIROUT__V14*/ meltfptr[13]),
			     (melt_ptr_t) (( /*!CLASS_INITIALROUTINEOBJ */
					    meltfrout->tabval[3])));;
      MELT_LOCATION ("warmelt-genobj.melt:5207:/ cond");
      /*cond */ if ( /*_#IS_A__L5*/ meltfnum[3])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V18*/ meltfptr[17] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-genobj.melt:5207:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check inirout"),
				  ("warmelt-genobj.melt")
				  ? ("warmelt-genobj.melt") : __FILE__,
				  (5207) ? (5207) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V18*/ meltfptr[17] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V17*/ meltfptr[16] = /*_.IFELSE___V18*/ meltfptr[17];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:5207:/ clear");
	     /*clear *//*_#IS_A__L5*/ meltfnum[3] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V18*/ meltfptr[17] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V17*/ meltfptr[16] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-genobj.melt:5208:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^cond */
    /*cond */ if ( /*_.CHOBJ__V16*/ meltfptr[15])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{


#if MELT_HAVE_DEBUG
	  MELT_LOCATION ("warmelt-genobj.melt:5210:/ cppif.then");
	  /*^block */
	  /*anyblock */
	  {


	    {
	      /*^locexp */
	      /*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	      melt_dbgcounter++;
#endif
	      ;
	    }
	    ;
	    /*^checksignal */
	    MELT_CHECK_SIGNAL ();
	    ;
     /*_#MELT_NEED_DBG__L6*/ meltfnum[0] =
	      /*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	      ( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	      0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	      ;;
	    MELT_LOCATION ("warmelt-genobj.melt:5210:/ cond");
	    /*cond */ if ( /*_#MELT_NEED_DBG__L6*/ meltfnum[0])	/*then */
	      {
		/*^cond.then */
		/*^block */
		/*anyblock */
		{

       /*_#THE_MELTCALLCOUNT__L7*/ meltfnum[3] =
#ifdef meltcallcount
		    meltcallcount	/* the_meltcallcount */
#else
		    0L
#endif /* meltcallcount the_meltcallcount */
		    ;;
		  MELT_LOCATION ("warmelt-genobj.melt:5210:/ checksignal");
		  MELT_CHECK_SIGNAL ();
		  ;
		  /*^apply */
		  /*apply */
		  {
		    union meltparam_un argtab[5];
		    memset (&argtab, 0, sizeof (argtab));
		    /*^apply.arg */
		    argtab[0].meltbp_long =
		      /*_#THE_MELTCALLCOUNT__L7*/ meltfnum[3];
		    /*^apply.arg */
		    argtab[1].meltbp_cstring = "warmelt-genobj.melt";
		    /*^apply.arg */
		    argtab[2].meltbp_long = 5210;
		    /*^apply.arg */
		    argtab[3].meltbp_cstring =
		      "compilobj_dataclosure found chobj=";
		    /*^apply.arg */
		    argtab[4].meltbp_aptr =
		      (melt_ptr_t *) & /*_.CHOBJ__V16*/ meltfptr[15];
		    /*_.MELT_DEBUG_FUN__V22*/ meltfptr[21] =
		      melt_apply ((meltclosure_ptr_t)
				  (( /*!MELT_DEBUG_FUN */ meltfrout->
				    tabval[2])),
				  (melt_ptr_t) (( /*nil */ NULL)),
				  (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_PTR ""), argtab, "",
				  (union meltparam_un *) 0);
		  }
		  ;
		  /*_.IF___V21*/ meltfptr[20] =
		    /*_.MELT_DEBUG_FUN__V22*/ meltfptr[21];;
		  /*epilog */

		  MELT_LOCATION ("warmelt-genobj.melt:5210:/ clear");
		 /*clear *//*_#THE_MELTCALLCOUNT__L7*/ meltfnum[3] = 0;
		  /*^clear */
		 /*clear *//*_.MELT_DEBUG_FUN__V22*/ meltfptr[21] = 0;
		}
		;
	      }
	    else
	      {			/*^cond.else */

      /*_.IF___V21*/ meltfptr[20] = NULL;;
	      }
	    ;
	    MELT_LOCATION ("warmelt-genobj.melt:5210:/ quasiblock");


	    /*_.PROGN___V23*/ meltfptr[21] = /*_.IF___V21*/ meltfptr[20];;
	    /*^compute */
	    /*_.IFCPP___V20*/ meltfptr[19] = /*_.PROGN___V23*/ meltfptr[21];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-genobj.melt:5210:/ clear");
	       /*clear *//*_#MELT_NEED_DBG__L6*/ meltfnum[0] = 0;
	    /*^clear */
	       /*clear *//*_.IF___V21*/ meltfptr[20] = 0;
	    /*^clear */
	       /*clear *//*_.PROGN___V23*/ meltfptr[21] = 0;
	  }

#else /*MELT_HAVE_DEBUG */
	  /*^cppif.else */
	  /*_.IFCPP___V20*/ meltfptr[19] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
	  ;
	  MELT_LOCATION ("warmelt-genobj.melt:5211:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^quasiblock */


	  /*_.RETVAL___V1*/ meltfptr[0] = /*_.CHOBJ__V16*/ meltfptr[15];;

	  {
	    MELT_LOCATION ("warmelt-genobj.melt:5211:/ locexp");
	    /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
	    if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	      melt_warn_for_no_expected_secondary_results ();
	    /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
	    ;
	  }
	  ;
	  /*^finalreturn */
	  ;
	  /*finalret */ goto labend_rout;
	  MELT_LOCATION ("warmelt-genobj.melt:5209:/ quasiblock");


	  /*_.PROGN___V25*/ meltfptr[21] = /*_.RETURN___V24*/ meltfptr[20];;
	  /*^compute */
	  /*_.IF___V19*/ meltfptr[17] = /*_.PROGN___V25*/ meltfptr[21];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-genobj.melt:5208:/ clear");
	     /*clear *//*_.IFCPP___V20*/ meltfptr[19] = 0;
	  /*^clear */
	     /*clear *//*_.RETURN___V24*/ meltfptr[20] = 0;
	  /*^clear */
	     /*clear *//*_.PROGN___V25*/ meltfptr[21] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

  /*_.IF___V19*/ meltfptr[17] = NULL;;
      }
    ;
    MELT_LOCATION ("warmelt-genobj.melt:5212:/ quasiblock");


    MELT_LOCATION ("warmelt-genobj.melt:5213:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.NCL__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 1, "NDATA_NAME");
  /*_.NAM__V27*/ meltfptr[20] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-genobj.melt:5214:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.NCL__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 4, "NDATA_DISCRX");
  /*_.DISCX__V28*/ meltfptr[21] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-genobj.melt:5215:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.NCL__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 2, "NDATA_RANK");
  /*_.NRANK__V29*/ meltfptr[28] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-genobj.melt:5216:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.NCL__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 0, "NREP_LOC");
  /*_.NLOC__V30*/ meltfptr[29] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-genobj.melt:5217:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.NCL__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 5, "NDCLO_PROC");
  /*_.NPRO__V31*/ meltfptr[30] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-genobj.melt:5218:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.NCL__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 6, "NDCLO_CLOSV");
  /*_.NCLOV__V32*/ meltfptr[31] = slot;
    };
    ;
 /*_#NBCLOS__L8*/ meltfnum[3] =
      (melt_multiple_length ((melt_ptr_t) ( /*_.NCLOV__V32*/ meltfptr[31])));;
    /*^compute */
 /*_.NAMBUF__V33*/ meltfptr[32] =
      (melt_ptr_t)
      meltgc_new_strbuf ((meltobject_ptr_t)
			 (( /*!DISCR_STRBUF */ meltfrout->tabval[4])),
			 (const char *) 0);;
    MELT_LOCATION ("warmelt-genobj.melt:5221:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^msend */
    /*msend */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^ojbmsend.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.GCX__V3*/ meltfptr[2];
      /*_.ODISCR__V34*/ meltfptr[33] =
	meltgc_send ((melt_ptr_t) ( /*_.DISCX__V28*/ meltfptr[21]),
		     (melt_ptr_t) (( /*!COMPILE_OBJ */ meltfrout->tabval[5])),
		     (MELTBPARSTR_PTR ""), argtab, "",
		     (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-genobj.melt:5222:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & ( /*!konst_7_VALCLO_ */ meltfrout->tabval[7]);
      /*_.LOCVAR__V35*/ meltfptr[34] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!GET_FREE_OBJLOCPTR */ meltfrout->tabval[6])),
		    (melt_ptr_t) ( /*_.GCX__V3*/ meltfptr[2]),
		    (MELTBPARSTR_PTR ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-genobj.melt:5223:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj =
	(melt_ptr_t) (( /*!konst_8_DATACLOSURE_ */ meltfrout->
		       tabval[8])) /*=obj*/ ;
      melt_object_get_field (slot, obj, 1, "NAMED_NAME");
  /*_.COMM__V36*/ meltfptr[35] = slot;
    };
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:5225:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L9*/ meltfnum[0] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.NPRO__V31*/ meltfptr[30]),
			     (melt_ptr_t) (( /*!CLASS_NREP_ROUTPROC */
					    meltfrout->tabval[9])));;
      MELT_LOCATION ("warmelt-genobj.melt:5225:/ cond");
      /*cond */ if ( /*_#IS_A__L9*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V38*/ meltfptr[37] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-genobj.melt:5225:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check npro"),
				  ("warmelt-genobj.melt")
				  ? ("warmelt-genobj.melt") : __FILE__,
				  (5225) ? (5225) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V38*/ meltfptr[37] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V37*/ meltfptr[36] = /*_.IFELSE___V38*/ meltfptr[37];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:5225:/ clear");
	     /*clear *//*_#IS_A__L9*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V38*/ meltfptr[37] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V37*/ meltfptr[36] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

    {
      MELT_LOCATION ("warmelt-genobj.melt:5226:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.NAMBUF__V33*/ meltfptr[32]),
			   ("dclo_"));
    }
    ;
 /*_#GET_INT__L10*/ meltfnum[0] =
      (melt_get_int ((melt_ptr_t) ( /*_.NRANK__V29*/ meltfptr[28])));;

    {
      MELT_LOCATION ("warmelt-genobj.melt:5227:/ locexp");
      meltgc_add_strbuf_dec ((melt_ptr_t) ( /*_.NAMBUF__V33*/ meltfptr[32]),
			     ( /*_#GET_INT__L10*/ meltfnum[0]));
    }
    ;
    MELT_LOCATION ("warmelt-genobj.melt:5228:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^cond */
    /*cond */ if ( /*_.NAM__V27*/ meltfptr[20])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{


	  {
	    MELT_LOCATION ("warmelt-genobj.melt:5229:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t)
				 ( /*_.NAMBUF__V33*/ meltfptr[32]), ("__"));
	  }
	  ;
	  MELT_LOCATION ("warmelt-genobj.melt:5230:/ getslot");
	  {
	    melt_ptr_t slot = NULL, obj = NULL;
	    obj = (melt_ptr_t) ( /*_.NAM__V27*/ meltfptr[20]) /*=obj*/ ;
	    melt_object_get_field (slot, obj, 1, "NAMED_NAME");
    /*_.NAMED_NAME__V39*/ meltfptr[37] = slot;
	  };
	  ;

	  {
	    /*^locexp */
	    meltgc_add_strbuf_cident ((melt_ptr_t)
				      ( /*_.NAMBUF__V33*/ meltfptr[32]),
				      melt_string_str ((melt_ptr_t)
						       ( /*_.NAMED_NAME__V39*/
							meltfptr[37])));
	  }
	  ;
	  MELT_LOCATION ("warmelt-genobj.melt:5228:/ quasiblock");


	  /*epilog */

	  /*^clear */
	     /*clear *//*_.NAMED_NAME__V39*/ meltfptr[37] = 0;
	}
	;
      }				/*noelse */
    ;
    MELT_LOCATION ("warmelt-genobj.melt:5231:/ quasiblock");


    MELT_LOCATION ("warmelt-genobj.melt:5232:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.NPRO__V31*/ meltfptr[30]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 6, "NRPRO_DATAROUT");
  /*_.NDATAROU__V41*/ meltfptr[40] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-genobj.melt:5234:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_.STRBUF2STRING__V42*/ meltfptr[41] =
      (meltgc_new_stringdup
       ((meltobject_ptr_t) (( /*!DISCR_STRING */ meltfrout->tabval[12])),
	melt_strbuf_str ((melt_ptr_t) ( /*_.NAMBUF__V33*/ meltfptr[32]))));;
    MELT_LOCATION ("warmelt-genobj.melt:5234:/ quasiblock");


    /*^rawallocobj */
    /*rawallocobj */
    {
      melt_ptr_t newobj = 0;
      melt_raw_object_create (newobj,
			      (melt_ptr_t) (( /*!CLASS_OBJINITCLOSURE */
					     meltfrout->tabval[10])), (6),
			      "CLASS_OBJINITCLOSURE");
  /*_.INST__V44*/ meltfptr[43] =
	newobj;
    };
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OBV_TYPE",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V44*/ meltfptr[43])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V44*/ meltfptr[43]), (0),
			  (( /*!CTYPE_VALUE */ meltfrout->tabval[11])),
			  "OBV_TYPE");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OIE_DISCR",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V44*/ meltfptr[43])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V44*/ meltfptr[43]), (3),
			  ( /*_.ODISCR__V34*/ meltfptr[33]), "OIE_DISCR");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OIE_DATA",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V44*/ meltfptr[43])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V44*/ meltfptr[43]), (2),
			  ( /*_.NCL__V2*/ meltfptr[1]), "OIE_DATA");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OIE_LOCVAR",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V44*/ meltfptr[43])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V44*/ meltfptr[43]), (4),
			  ( /*_.LOCVAR__V35*/ meltfptr[34]), "OIE_LOCVAR");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OIE_CNAME",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V44*/ meltfptr[43])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V44*/ meltfptr[43]), (1),
			  ( /*_.STRBUF2STRING__V42*/ meltfptr[41]),
			  "OIE_CNAME");
    ;
    /*^touchobj */

    melt_dbgtrace_written_object ( /*_.INST__V44*/ meltfptr[43],
				  "newly made instance");
    ;
    /*_.OICLO__V43*/ meltfptr[42] = /*_.INST__V44*/ meltfptr[43];;

    {
      MELT_LOCATION ("warmelt-genobj.melt:5242:/ locexp");
      melt_put_int ((melt_ptr_t) ( /*_.OICLO__V43*/ meltfptr[42]),
		    ( /*_#NBCLOS__L8*/ meltfnum[3]));
    }
    ;

    {
      MELT_LOCATION ("warmelt-genobj.melt:5243:/ locexp");
      meltgc_put_mapobjects ((meltmapobjects_ptr_t)
			     ( /*_.COMPICACHE__V13*/ meltfptr[9]),
			     (meltobject_ptr_t) ( /*_.NCL__V2*/ meltfptr[1]),
			     (melt_ptr_t) ( /*_.OICLO__V43*/ meltfptr[42]));
    }
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:5244:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L11*/ meltfnum[10] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.NDATAROU__V41*/ meltfptr[40]),
			     (melt_ptr_t) (( /*!CLASS_NREP_DATAROUTINE */
					    meltfrout->tabval[13])));;
      MELT_LOCATION ("warmelt-genobj.melt:5244:/ cond");
      /*cond */ if ( /*_#IS_A__L11*/ meltfnum[10])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V46*/ meltfptr[45] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-genobj.melt:5244:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check ndatarou"),
				  ("warmelt-genobj.melt")
				  ? ("warmelt-genobj.melt") : __FILE__,
				  (5244) ? (5244) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V46*/ meltfptr[45] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V45*/ meltfptr[44] = /*_.IFELSE___V46*/ meltfptr[45];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:5244:/ clear");
	     /*clear *//*_#IS_A__L11*/ meltfnum[10] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V46*/ meltfptr[45] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V45*/ meltfptr[44] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-genobj.melt:5245:/ quasiblock");


 /*_.OCROUT__V48*/ meltfptr[47] =
      /*mapobject_get */
      melt_get_mapobjects ((meltmapobjects_ptr_t)
			   ( /*_.COMPICACHE__V13*/ meltfptr[9]),
			   (meltobject_ptr_t) ( /*_.NPRO__V31*/
					       meltfptr[30]));;
    MELT_LOCATION ("warmelt-genobj.melt:5248:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^msend */
    /*msend */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^ojbmsend.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.GCX__V3*/ meltfptr[2];
      /*_.ODATROUT__V49*/ meltfptr[48] =
	meltgc_send ((melt_ptr_t) ( /*_.NDATAROU__V41*/ meltfptr[40]),
		     (melt_ptr_t) (( /*!COMPILE_OBJ */ meltfrout->tabval[5])),
		     (MELTBPARSTR_PTR ""), argtab, "",
		     (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-genobj.melt:5249:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*^rawallocobj */
    /*rawallocobj */
    {
      melt_ptr_t newobj = 0;
      melt_raw_object_create (newobj,
			      (melt_ptr_t) (( /*!CLASS_OBJPUTCLOSUROUT */
					     meltfrout->tabval[14])), (3),
			      "CLASS_OBJPUTCLOSUROUT");
  /*_.INST__V51*/ meltfptr[50] =
	newobj;
    };
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OBI_LOC",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V51*/ meltfptr[50])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V51*/ meltfptr[50]), (0),
			  ( /*_.NLOC__V30*/ meltfptr[29]), "OBI_LOC");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OPCLOR_CLOS",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V51*/ meltfptr[50])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V51*/ meltfptr[50]), (1),
			  ( /*_.OICLO__V43*/ meltfptr[42]), "OPCLOR_CLOS");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OPCLOR_ROUT",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V51*/ meltfptr[50])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V51*/ meltfptr[50]), (2),
			  ( /*_.ODATROUT__V49*/ meltfptr[48]), "OPCLOR_ROUT");
    ;
    /*^touchobj */

    melt_dbgtrace_written_object ( /*_.INST__V51*/ meltfptr[50],
				  "newly made instance");
    ;
    /*_.OCPUTROUT__V50*/ meltfptr[49] = /*_.INST__V51*/ meltfptr[50];;
    /*^compute */
 /*_.BXOFF__V52*/ meltfptr[51] =
      (meltgc_new_int
       ((meltobject_ptr_t) (( /*!DISCR_INTEGER */ meltfrout->tabval[15])),
	(0)));;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:5257:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L12*/ meltfnum[10] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.OCROUT__V48*/ meltfptr[47]),
			     (melt_ptr_t) (( /*!CLASS_PROCROUTINEOBJ */
					    meltfrout->tabval[16])));;
      MELT_LOCATION ("warmelt-genobj.melt:5257:/ cond");
      /*cond */ if ( /*_#IS_A__L12*/ meltfnum[10])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V54*/ meltfptr[53] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-genobj.melt:5257:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check ocrout"),
				  ("warmelt-genobj.melt")
				  ? ("warmelt-genobj.melt") : __FILE__,
				  (5257) ? (5257) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V54*/ meltfptr[53] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V53*/ meltfptr[52] = /*_.IFELSE___V54*/ meltfptr[53];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:5257:/ clear");
	     /*clear *//*_#IS_A__L12*/ meltfnum[10] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V54*/ meltfptr[53] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V53*/ meltfptr[52] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

    {
      MELT_LOCATION ("warmelt-genobj.melt:5258:/ locexp");
      meltgc_append_list ((melt_ptr_t) ( /*_.INIBODY__V15*/ meltfptr[14]),
			  (melt_ptr_t) ( /*_.OCPUTROUT__V50*/ meltfptr[49]));
    }
    ;
    MELT_LOCATION ("warmelt-genobj.melt:5261:/ quasiblock");


    /*^newclosure */
		 /*newclosure *//*_.LAMBDA___V56*/ meltfptr[55] =
      (melt_ptr_t)
      meltgc_new_closure ((meltobject_ptr_t)
			  (((melt_ptr_t) (MELT_PREDEF (DISCR_CLOSURE)))),
			  (meltroutine_ptr_t) (( /*!konst_21 */ meltfrout->
						tabval[21])), (5));
    ;
    /*^putclosedv */
    /*putclosv */
    melt_assertmsg ("putclosv checkclo",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.LAMBDA___V56*/ meltfptr[55])) ==
		    MELTOBMAG_CLOSURE);
    melt_assertmsg ("putclosv checkoff", 0 >= 0
		    && 0 <
		    melt_closure_size ((melt_ptr_t)
				       ( /*_.LAMBDA___V56*/ meltfptr[55])));
    ((meltclosure_ptr_t) /*_.LAMBDA___V56*/ meltfptr[55])->tabval[0] =
      (melt_ptr_t) ( /*_.GCX__V3*/ meltfptr[2]);
    ;
    /*^putclosedv */
    /*putclosv */
    melt_assertmsg ("putclosv checkclo",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.LAMBDA___V56*/ meltfptr[55])) ==
		    MELTOBMAG_CLOSURE);
    melt_assertmsg ("putclosv checkoff", 1 >= 0
		    && 1 <
		    melt_closure_size ((melt_ptr_t)
				       ( /*_.LAMBDA___V56*/ meltfptr[55])));
    ((meltclosure_ptr_t) /*_.LAMBDA___V56*/ meltfptr[55])->tabval[1] =
      (melt_ptr_t) ( /*_.NLOC__V30*/ meltfptr[29]);
    ;
    /*^putclosedv */
    /*putclosv */
    melt_assertmsg ("putclosv checkclo",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.LAMBDA___V56*/ meltfptr[55])) ==
		    MELTOBMAG_CLOSURE);
    melt_assertmsg ("putclosv checkoff", 2 >= 0
		    && 2 <
		    melt_closure_size ((melt_ptr_t)
				       ( /*_.LAMBDA___V56*/ meltfptr[55])));
    ((meltclosure_ptr_t) /*_.LAMBDA___V56*/ meltfptr[55])->tabval[2] =
      (melt_ptr_t) ( /*_.OICLO__V43*/ meltfptr[42]);
    ;
    /*^putclosedv */
    /*putclosv */
    melt_assertmsg ("putclosv checkclo",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.LAMBDA___V56*/ meltfptr[55])) ==
		    MELTOBMAG_CLOSURE);
    melt_assertmsg ("putclosv checkoff", 3 >= 0
		    && 3 <
		    melt_closure_size ((melt_ptr_t)
				       ( /*_.LAMBDA___V56*/ meltfptr[55])));
    ((meltclosure_ptr_t) /*_.LAMBDA___V56*/ meltfptr[55])->tabval[3] =
      (melt_ptr_t) ( /*_.BXOFF__V52*/ meltfptr[51]);
    ;
    /*^putclosedv */
    /*putclosv */
    melt_assertmsg ("putclosv checkclo",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.LAMBDA___V56*/ meltfptr[55])) ==
		    MELTOBMAG_CLOSURE);
    melt_assertmsg ("putclosv checkoff", 4 >= 0
		    && 4 <
		    melt_closure_size ((melt_ptr_t)
				       ( /*_.LAMBDA___V56*/ meltfptr[55])));
    ((meltclosure_ptr_t) /*_.LAMBDA___V56*/ meltfptr[55])->tabval[4] =
      (melt_ptr_t) ( /*_.INIBODY__V15*/ meltfptr[14]);
    ;
    /*_.LAMBDA___V55*/ meltfptr[53] = /*_.LAMBDA___V56*/ meltfptr[55];;
    MELT_LOCATION ("warmelt-genobj.melt:5259:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & /*_.LAMBDA___V55*/ meltfptr[53];
      /*_.MULTIPLE_EVERY__V57*/ meltfptr[56] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!MULTIPLE_EVERY */ meltfrout->tabval[17])),
		    (melt_ptr_t) ( /*_.NCLOV__V32*/ meltfptr[31]),
		    (MELTBPARSTR_PTR ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-genobj.melt:5272:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*^rawallocobj */
    /*rawallocobj */
    {
      melt_ptr_t newobj = 0;
      melt_raw_object_create (newobj,
			      (melt_ptr_t) (( /*!CLASS_OBJTOUCH */ meltfrout->
					     tabval[22])), (3),
			      "CLASS_OBJTOUCH");
  /*_.INST__V59*/ meltfptr[58] =
	newobj;
    };
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OBI_LOC",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V59*/ meltfptr[58])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V59*/ meltfptr[58]), (0),
			  ( /*_.NLOC__V30*/ meltfptr[29]), "OBI_LOC");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OTOUCH_COMMENT",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V59*/ meltfptr[58])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V59*/ meltfptr[58]), (2),
			  ( /*_.COMM__V36*/ meltfptr[35]), "OTOUCH_COMMENT");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OTOUCH_VAL",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V59*/ meltfptr[58])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V59*/ meltfptr[58]), (1),
			  ( /*_.OICLO__V43*/ meltfptr[42]), "OTOUCH_VAL");
    ;
    /*^touchobj */

    melt_dbgtrace_written_object ( /*_.INST__V59*/ meltfptr[58],
				  "newly made instance");
    ;
    /*_.INST___V58*/ meltfptr[57] = /*_.INST__V59*/ meltfptr[58];;

    {
      MELT_LOCATION ("warmelt-genobj.melt:5272:/ locexp");
      meltgc_append_list ((melt_ptr_t) ( /*_.INIBODY__V15*/ meltfptr[14]),
			  (melt_ptr_t) ( /*_.INST___V58*/ meltfptr[57]));
    }
    ;
    /*_.LET___V47*/ meltfptr[45] = /*_.OICLO__V43*/ meltfptr[42];;

    MELT_LOCATION ("warmelt-genobj.melt:5245:/ clear");
	   /*clear *//*_.OCROUT__V48*/ meltfptr[47] = 0;
    /*^clear */
	   /*clear *//*_.ODATROUT__V49*/ meltfptr[48] = 0;
    /*^clear */
	   /*clear *//*_.OCPUTROUT__V50*/ meltfptr[49] = 0;
    /*^clear */
	   /*clear *//*_.BXOFF__V52*/ meltfptr[51] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V53*/ meltfptr[52] = 0;
    /*^clear */
	   /*clear *//*_.LAMBDA___V55*/ meltfptr[53] = 0;
    /*^clear */
	   /*clear *//*_.MULTIPLE_EVERY__V57*/ meltfptr[56] = 0;
    /*^clear */
	   /*clear *//*_.INST___V58*/ meltfptr[57] = 0;
    /*_.LET___V40*/ meltfptr[37] = /*_.LET___V47*/ meltfptr[45];;

    MELT_LOCATION ("warmelt-genobj.melt:5231:/ clear");
	   /*clear *//*_.NDATAROU__V41*/ meltfptr[40] = 0;
    /*^clear */
	   /*clear *//*_.STRBUF2STRING__V42*/ meltfptr[41] = 0;
    /*^clear */
	   /*clear *//*_.OICLO__V43*/ meltfptr[42] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V45*/ meltfptr[44] = 0;
    /*^clear */
	   /*clear *//*_.LET___V47*/ meltfptr[45] = 0;
    /*_.LET___V26*/ meltfptr[19] = /*_.LET___V40*/ meltfptr[37];;

    MELT_LOCATION ("warmelt-genobj.melt:5212:/ clear");
	   /*clear *//*_.NAM__V27*/ meltfptr[20] = 0;
    /*^clear */
	   /*clear *//*_.DISCX__V28*/ meltfptr[21] = 0;
    /*^clear */
	   /*clear *//*_.NRANK__V29*/ meltfptr[28] = 0;
    /*^clear */
	   /*clear *//*_.NLOC__V30*/ meltfptr[29] = 0;
    /*^clear */
	   /*clear *//*_.NPRO__V31*/ meltfptr[30] = 0;
    /*^clear */
	   /*clear *//*_.NCLOV__V32*/ meltfptr[31] = 0;
    /*^clear */
	   /*clear *//*_#NBCLOS__L8*/ meltfnum[3] = 0;
    /*^clear */
	   /*clear *//*_.NAMBUF__V33*/ meltfptr[32] = 0;
    /*^clear */
	   /*clear *//*_.ODISCR__V34*/ meltfptr[33] = 0;
    /*^clear */
	   /*clear *//*_.LOCVAR__V35*/ meltfptr[34] = 0;
    /*^clear */
	   /*clear *//*_.COMM__V36*/ meltfptr[35] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V37*/ meltfptr[36] = 0;
    /*^clear */
	   /*clear *//*_#GET_INT__L10*/ meltfnum[0] = 0;
    /*^clear */
	   /*clear *//*_.LET___V40*/ meltfptr[37] = 0;
    /*_.LET___V12*/ meltfptr[8] = /*_.LET___V26*/ meltfptr[19];;

    MELT_LOCATION ("warmelt-genobj.melt:5202:/ clear");
	   /*clear *//*_.COMPICACHE__V13*/ meltfptr[9] = 0;
    /*^clear */
	   /*clear *//*_.INIROUT__V14*/ meltfptr[13] = 0;
    /*^clear */
	   /*clear *//*_.INIBODY__V15*/ meltfptr[14] = 0;
    /*^clear */
	   /*clear *//*_.CHOBJ__V16*/ meltfptr[15] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V17*/ meltfptr[16] = 0;
    /*^clear */
	   /*clear *//*_.IF___V19*/ meltfptr[17] = 0;
    /*^clear */
	   /*clear *//*_.LET___V26*/ meltfptr[19] = 0;
    MELT_LOCATION ("warmelt-genobj.melt:5198:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.LET___V12*/ meltfptr[8];;

    {
      MELT_LOCATION ("warmelt-genobj.melt:5198:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V4*/ meltfptr[3] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V6*/ meltfptr[4] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V8*/ meltfptr[6] = 0;
    /*^clear */
	   /*clear *//*_.LET___V12*/ meltfptr[8] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("COMPILOBJ_DATACLOSURE", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_152_warmelt_genobj_COMPILOBJ_DATACLOSURE_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_152_warmelt_genobj_COMPILOBJ_DATACLOSURE */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_153_warmelt_genobj_LAMBDA___51__ (meltclosure_ptr_t meltclosp_,
					   melt_ptr_t meltfirstargp_,
					   const melt_argdescr_cell_t
					   meltxargdescr_[],
					   union meltparam_un * meltxargtab_,
					   const melt_argdescr_cell_t
					   meltxresdescr_[],
					   union meltparam_un * meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_153_warmelt_genobj_LAMBDA___51___melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_153_warmelt_genobj_LAMBDA___51___st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 6
    melt_ptr_t mcfr_varptr[6];
#define MELTFRAM_NBVARNUM 4
    long mcfr_varnum[4];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_153_warmelt_genobj_LAMBDA___51__ is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_153_warmelt_genobj_LAMBDA___51___st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 6; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_153_warmelt_genobj_LAMBDA___51__ nbval 6*/
  meltfram__.mcfr_nbvar = 6 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("LAMBDA_", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-genobj.melt:5261:/ getarg");
 /*_.CLOV__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_LONG)
    goto lab_endgetargs;
 /*_#IX__L1*/ meltfnum[0] = meltxargtab_[0].meltbp_long;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {

    MELT_LOCATION ("warmelt-genobj.melt:5262:/ quasiblock");


    /*^checksignal */
    MELT_CHECK_SIGNAL ();
    ;
    /*^msend */
    /*msend */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^ojbmsend.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & ( /*~GCX */ meltfclos->tabval[0]);
      /*_.CLOVAL__V3*/ meltfptr[2] =
	meltgc_send ((melt_ptr_t) ( /*_.CLOV__V2*/ meltfptr[1]),
		     (melt_ptr_t) (( /*!COMPILE_OBJ */ meltfrout->tabval[0])),
		     (MELTBPARSTR_PTR ""), argtab, "",
		     (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-genobj.melt:5263:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#GET_INT__L2*/ meltfnum[1] =
      (melt_get_int ((melt_ptr_t) (( /*~BXOFF */ meltfclos->tabval[3]))));;
    /*^compute */
 /*_.MAKE_INTEGERBOX__V4*/ meltfptr[3] =
      (meltgc_new_int
       ((meltobject_ptr_t) (( /*!DISCR_INTEGER */ meltfrout->tabval[2])),
	( /*_#GET_INT__L2*/ meltfnum[1])));;
    MELT_LOCATION ("warmelt-genobj.melt:5263:/ quasiblock");


    /*^rawallocobj */
    /*rawallocobj */
    {
      melt_ptr_t newobj = 0;
      melt_raw_object_create (newobj,
			      (melt_ptr_t) (( /*!CLASS_OBJPUTCLOSEDV */
					     meltfrout->tabval[1])), (4),
			      "CLASS_OBJPUTCLOSEDV");
  /*_.INST__V6*/ meltfptr[5] =
	newobj;
    };
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OBI_LOC",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V6*/ meltfptr[5])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V6*/ meltfptr[5]), (0),
			  (( /*~NLOC */ meltfclos->tabval[1])), "OBI_LOC");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OPCLOV_CLOS",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V6*/ meltfptr[5])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V6*/ meltfptr[5]), (1),
			  (( /*~OICLO */ meltfclos->tabval[2])),
			  "OPCLOV_CLOS");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OPCLOV_OFF",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V6*/ meltfptr[5])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V6*/ meltfptr[5]), (2),
			  ( /*_.MAKE_INTEGERBOX__V4*/ meltfptr[3]),
			  "OPCLOV_OFF");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OPCLOV_CVAL",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V6*/ meltfptr[5])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V6*/ meltfptr[5]), (3),
			  ( /*_.CLOVAL__V3*/ meltfptr[2]), "OPCLOV_CVAL");
    ;
    /*^touchobj */

    melt_dbgtrace_written_object ( /*_.INST__V6*/ meltfptr[5],
				  "newly made instance");
    ;
    /*_.OCPUTCLOS__V5*/ meltfptr[4] = /*_.INST__V6*/ meltfptr[5];;

    {
      MELT_LOCATION ("warmelt-genobj.melt:5269:/ locexp");
      meltgc_append_list ((melt_ptr_t)
			  (( /*~INIBODY */ meltfclos->tabval[4])),
			  (melt_ptr_t) ( /*_.OCPUTCLOS__V5*/ meltfptr[4]));
    }
    ;

    MELT_LOCATION ("warmelt-genobj.melt:5262:/ clear");
	   /*clear *//*_.CLOVAL__V3*/ meltfptr[2] = 0;
    /*^clear */
	   /*clear *//*_#GET_INT__L2*/ meltfnum[1] = 0;
    /*^clear */
	   /*clear *//*_.MAKE_INTEGERBOX__V4*/ meltfptr[3] = 0;
    /*^clear */
	   /*clear *//*_.OCPUTCLOS__V5*/ meltfptr[4] = 0;
 /*_#GET_INT__L3*/ meltfnum[1] =
      (melt_get_int ((melt_ptr_t) (( /*~BXOFF */ meltfclos->tabval[3]))));;
    /*^compute */
 /*_#I__L4*/ meltfnum[3] =
      (( /*_#GET_INT__L3*/ meltfnum[1]) + (1));;

    {
      MELT_LOCATION ("warmelt-genobj.melt:5271:/ locexp");
      melt_put_int ((melt_ptr_t) (( /*~BXOFF */ meltfclos->tabval[3])),
		    ( /*_#I__L4*/ meltfnum[3]));
    }
    ;
    MELT_LOCATION ("warmelt-genobj.melt:5261:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*epilog */

    /*^clear */
	   /*clear *//*_#GET_INT__L3*/ meltfnum[1] = 0;
    /*^clear */
	   /*clear *//*_#I__L4*/ meltfnum[3] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("LAMBDA_", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_153_warmelt_genobj_LAMBDA___51___melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*noretval */ NULL);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_153_warmelt_genobj_LAMBDA___51__ */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_154_warmelt_genobj_COMPILOBJ_DATAROUTINE (meltclosure_ptr_t
						   meltclosp_,
						   melt_ptr_t meltfirstargp_,
						   const melt_argdescr_cell_t
						   meltxargdescr_[],
						   union meltparam_un *
						   meltxargtab_,
						   const melt_argdescr_cell_t
						   meltxresdescr_[],
						   union meltparam_un *
						   meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_154_warmelt_genobj_COMPILOBJ_DATAROUTINE_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_154_warmelt_genobj_COMPILOBJ_DATAROUTINE_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 55
    melt_ptr_t mcfr_varptr[55];
#define MELTFRAM_NBVARNUM 13
    long mcfr_varnum[13];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_154_warmelt_genobj_COMPILOBJ_DATAROUTINE is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_154_warmelt_genobj_COMPILOBJ_DATAROUTINE_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 55; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_154_warmelt_genobj_COMPILOBJ_DATAROUTINE nbval 55*/
  meltfram__.mcfr_nbvar = 55 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("COMPILOBJ_DATAROUTINE", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-genobj.melt:5285:/ getarg");
 /*_.NDROU__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.GCX__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.GCX__V3*/ meltfptr[2])) != NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:5286:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L1*/ meltfnum[0] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.NDROU__V2*/ meltfptr[1]),
			     (melt_ptr_t) (( /*!CLASS_NREP_DATAROUTINE */
					    meltfrout->tabval[0])));;
      MELT_LOCATION ("warmelt-genobj.melt:5286:/ cond");
      /*cond */ if ( /*_#IS_A__L1*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V5*/ meltfptr[4] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-genobj.melt:5286:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check ndrou"),
				  ("warmelt-genobj.melt")
				  ? ("warmelt-genobj.melt") : __FILE__,
				  (5286) ? (5286) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V5*/ meltfptr[4] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V4*/ meltfptr[3] = /*_.IFELSE___V5*/ meltfptr[4];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:5286:/ clear");
	     /*clear *//*_#IS_A__L1*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V5*/ meltfptr[4] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V4*/ meltfptr[3] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:5287:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L2*/ meltfnum[0] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.GCX__V3*/ meltfptr[2]),
			     (melt_ptr_t) (( /*!CLASS_C_GENERATION_CONTEXT */
					    meltfrout->tabval[1])));;
      MELT_LOCATION ("warmelt-genobj.melt:5287:/ cond");
      /*cond */ if ( /*_#IS_A__L2*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V7*/ meltfptr[6] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-genobj.melt:5287:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check gcx"),
				  ("warmelt-genobj.melt")
				  ? ("warmelt-genobj.melt") : __FILE__,
				  (5287) ? (5287) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V7*/ meltfptr[6] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V6*/ meltfptr[4] = /*_.IFELSE___V7*/ meltfptr[6];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:5287:/ clear");
	     /*clear *//*_#IS_A__L2*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V7*/ meltfptr[6] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V6*/ meltfptr[4] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-genobj.melt:5288:/ quasiblock");


    /*^getslot */
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.GCX__V3*/ meltfptr[2]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 6, "GNCX_COMPICACHE");
  /*_.COMPICACHE__V9*/ meltfptr[8] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-genobj.melt:5289:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.GCX__V3*/ meltfptr[2]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 0, "GNCX_OBJROUT");
  /*_.INIROUT__V10*/ meltfptr[9] = slot;
    };
    ;
 /*_.CHOBJ__V11*/ meltfptr[10] =
      /*mapobject_get */
      melt_get_mapobjects ((meltmapobjects_ptr_t)
			   ( /*_.COMPICACHE__V9*/ meltfptr[8]),
			   (meltobject_ptr_t) ( /*_.NDROU__V2*/
					       meltfptr[1]));;
    /*^compute */
    /*_.UNUSED_STUFF__V12*/ meltfptr[11] = ( /*nil */ NULL);;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:5294:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L3*/ meltfnum[0] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.INIROUT__V10*/ meltfptr[9]),
			     (melt_ptr_t) (( /*!CLASS_INITIALROUTINEOBJ */
					    meltfrout->tabval[2])));;
      MELT_LOCATION ("warmelt-genobj.melt:5294:/ cond");
      /*cond */ if ( /*_#IS_A__L3*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V14*/ meltfptr[13] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-genobj.melt:5294:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check inirout"),
				  ("warmelt-genobj.melt")
				  ? ("warmelt-genobj.melt") : __FILE__,
				  (5294) ? (5294) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V14*/ meltfptr[13] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V13*/ meltfptr[12] = /*_.IFELSE___V14*/ meltfptr[13];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:5294:/ clear");
	     /*clear *//*_#IS_A__L3*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V14*/ meltfptr[13] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V13*/ meltfptr[12] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-genobj.melt:5295:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^cond */
    /*cond */ if ( /*_.CHOBJ__V11*/ meltfptr[10])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-genobj.melt:5297:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^quasiblock */


	  /*_.RETVAL___V1*/ meltfptr[0] = /*_.CHOBJ__V11*/ meltfptr[10];;

	  {
	    MELT_LOCATION ("warmelt-genobj.melt:5297:/ locexp");
	    /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
	    if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	      melt_warn_for_no_expected_secondary_results ();
	    /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
	    ;
	  }
	  ;
	  /*^finalreturn */
	  ;
	  /*finalret */ goto labend_rout;
	  MELT_LOCATION ("warmelt-genobj.melt:5296:/ quasiblock");


	  /*_.PROGN___V17*/ meltfptr[16] = /*_.RETURN___V16*/ meltfptr[15];;
	  /*^compute */
	  /*_.IF___V15*/ meltfptr[13] = /*_.PROGN___V17*/ meltfptr[16];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-genobj.melt:5295:/ clear");
	     /*clear *//*_.RETURN___V16*/ meltfptr[15] = 0;
	  /*^clear */
	     /*clear *//*_.PROGN___V17*/ meltfptr[16] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

  /*_.IF___V15*/ meltfptr[13] = NULL;;
      }
    ;
    MELT_LOCATION ("warmelt-genobj.melt:5298:/ quasiblock");


    /*^getslot */
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.NDROU__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 1, "NDATA_NAME");
  /*_.NAM__V19*/ meltfptr[16] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-genobj.melt:5299:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.NDROU__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 4, "NDATA_DISCRX");
  /*_.DISX__V20*/ meltfptr[19] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-genobj.melt:5300:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.NDROU__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 0, "NREP_LOC");
  /*_.NLOC__V21*/ meltfptr[20] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-genobj.melt:5301:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.NDROU__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 2, "NDATA_RANK");
  /*_.DRANK__V22*/ meltfptr[21] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-genobj.melt:5302:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.NDROU__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 1, "NDATA_NAME");
  /*_.NAM__V23*/ meltfptr[22] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-genobj.melt:5303:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.NDROU__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 5, "NDROU_PROC");
  /*_.NPRO__V24*/ meltfptr[23] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-genobj.melt:5304:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.INIROUT__V10*/ meltfptr[9]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 3, "OBROUT_BODY");
  /*_.INIBODY__V25*/ meltfptr[24] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-genobj.melt:5305:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & ( /*!konst_4_VALROUT_ */ meltfrout->tabval[4]);
      /*_.LOCVAR__V26*/ meltfptr[25] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!GET_FREE_OBJLOCPTR */ meltfrout->tabval[3])),
		    (melt_ptr_t) ( /*_.GCX__V3*/ meltfptr[2]),
		    (MELTBPARSTR_PTR ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-genobj.melt:5307:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#NULL__L4*/ meltfnum[0] =
      (( /*_.NLOC__V21*/ meltfptr[20]) == NULL);;
    MELT_LOCATION ("warmelt-genobj.melt:5307:/ cond");
    /*cond */ if ( /*_#NULL__L4*/ meltfnum[0])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-genobj.melt:5308:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
   /*_#IS_A__L5*/ meltfnum[4] =
	    melt_is_instance_of ((melt_ptr_t) ( /*_.NPRO__V24*/ meltfptr[23]),
				 (melt_ptr_t) (( /*!CLASS_NREP */ meltfrout->
						tabval[5])));;
	  MELT_LOCATION ("warmelt-genobj.melt:5308:/ cond");
	  /*cond */ if ( /*_#IS_A__L5*/ meltfnum[4])	/*then */
	    {
	      /*^cond.then */
	      /*^block */
	      /*anyblock */
	      {

		MELT_LOCATION ("warmelt-genobj.melt:5309:/ getslot");
		{
		  melt_ptr_t slot = NULL, obj = NULL;
		  obj =
		    (melt_ptr_t) ( /*_.NPRO__V24*/ meltfptr[23]) /*=obj*/ ;
		  melt_object_get_field (slot, obj, 0, "NREP_LOC");
      /*_.NREP_LOC__V29*/ meltfptr[28] = slot;
		};
		;
		/*^compute */
		/*_.NLOC__V21*/ meltfptr[20] = /*_.SETQ___V30*/ meltfptr[29] =
		  /*_.NREP_LOC__V29*/ meltfptr[28];;
		/*_.IF___V28*/ meltfptr[27] = /*_.SETQ___V30*/ meltfptr[29];;
		/*epilog */

		MELT_LOCATION ("warmelt-genobj.melt:5308:/ clear");
	       /*clear *//*_.NREP_LOC__V29*/ meltfptr[28] = 0;
		/*^clear */
	       /*clear *//*_.SETQ___V30*/ meltfptr[29] = 0;
	      }
	      ;
	    }
	  else
	    {			/*^cond.else */

    /*_.IF___V28*/ meltfptr[27] = NULL;;
	    }
	  ;
	  /*^compute */
	  /*_.IF___V27*/ meltfptr[26] = /*_.IF___V28*/ meltfptr[27];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-genobj.melt:5307:/ clear");
	     /*clear *//*_#IS_A__L5*/ meltfnum[4] = 0;
	  /*^clear */
	     /*clear *//*_.IF___V28*/ meltfptr[27] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

  /*_.IF___V27*/ meltfptr[26] = NULL;;
      }
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:5310:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_MAPOBJECT__L6*/ meltfnum[4] =
	/*is_mapobject: */
	(melt_magic_discr ((melt_ptr_t) ( /*_.COMPICACHE__V9*/ meltfptr[8]))
	 == MELTOBMAG_MAPOBJECTS);;
      MELT_LOCATION ("warmelt-genobj.melt:5310:/ cond");
      /*cond */ if ( /*_#IS_MAPOBJECT__L6*/ meltfnum[4])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V32*/ meltfptr[29] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-genobj.melt:5310:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check compicache"),
				  ("warmelt-genobj.melt")
				  ? ("warmelt-genobj.melt") : __FILE__,
				  (5310) ? (5310) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V32*/ meltfptr[29] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V31*/ meltfptr[28] = /*_.IFELSE___V32*/ meltfptr[29];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:5310:/ clear");
	     /*clear *//*_#IS_MAPOBJECT__L6*/ meltfnum[4] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V32*/ meltfptr[29] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V31*/ meltfptr[28] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-genobj.melt:5311:/ quasiblock");


 /*_.NAMBUF__V34*/ meltfptr[29] =
      (melt_ptr_t)
      meltgc_new_strbuf ((meltobject_ptr_t)
			 (( /*!DISCR_STRBUF */ meltfrout->tabval[6])),
			 (const char *) 0);;
    /*^compute */
 /*_#DRK__L7*/ meltfnum[4] =
      (melt_get_int ((melt_ptr_t) ( /*_.DRANK__V22*/ meltfptr[21])));;
    MELT_LOCATION ("warmelt-genobj.melt:5314:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^msend */
    /*msend */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^ojbmsend.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.GCX__V3*/ meltfptr[2];
      /*_.ODISCR__V35*/ meltfptr[34] =
	meltgc_send ((melt_ptr_t) ( /*_.DISX__V20*/ meltfptr[19]),
		     (melt_ptr_t) (( /*!COMPILE_OBJ */ meltfrout->tabval[7])),
		     (MELTBPARSTR_PTR ""), argtab, "",
		     (union meltparam_un *) 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-genobj.melt:5316:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.NAMBUF__V34*/ meltfptr[29]),
			   ("drout_"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-genobj.melt:5317:/ locexp");
      meltgc_add_strbuf_dec ((melt_ptr_t) ( /*_.NAMBUF__V34*/ meltfptr[29]),
			     ( /*_#DRK__L7*/ meltfnum[4]));
    }
    ;
    MELT_LOCATION ("warmelt-genobj.melt:5318:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^cond */
    /*cond */ if ( /*_.NAM__V23*/ meltfptr[22])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{


	  {
	    MELT_LOCATION ("warmelt-genobj.melt:5319:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t)
				 ( /*_.NAMBUF__V34*/ meltfptr[29]), ("__"));
	  }
	  ;
	  MELT_LOCATION ("warmelt-genobj.melt:5320:/ getslot");
	  {
	    melt_ptr_t slot = NULL, obj = NULL;
	    obj = (melt_ptr_t) ( /*_.NAM__V23*/ meltfptr[22]) /*=obj*/ ;
	    melt_object_get_field (slot, obj, 1, "NAMED_NAME");
    /*_.NAMED_NAME__V36*/ meltfptr[35] = slot;
	  };
	  ;

	  {
	    /*^locexp */
	    meltgc_add_strbuf_cident ((melt_ptr_t)
				      ( /*_.NAMBUF__V34*/ meltfptr[29]),
				      melt_string_str ((melt_ptr_t)
						       ( /*_.NAMED_NAME__V36*/
							meltfptr[35])));
	  }
	  ;
	  MELT_LOCATION ("warmelt-genobj.melt:5318:/ quasiblock");


	  /*epilog */

	  /*^clear */
	     /*clear *//*_.NAMED_NAME__V36*/ meltfptr[35] = 0;
	}
	;
      }				/*noelse */
    ;
    MELT_LOCATION ("warmelt-genobj.melt:5321:/ quasiblock");


    MELT_LOCATION ("warmelt-genobj.melt:5323:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_.STRBUF2STRING__V38*/ meltfptr[37] =
      (meltgc_new_stringdup
       ((meltobject_ptr_t) (( /*!DISCR_STRING */ meltfrout->tabval[10])),
	melt_strbuf_str ((melt_ptr_t) ( /*_.NAMBUF__V34*/ meltfptr[29]))));;
    /*^compute */
 /*_.MAPOBJECT_GET__V39*/ meltfptr[38] =
      /*mapobject_get */
      melt_get_mapobjects ((meltmapobjects_ptr_t)
			   ( /*_.COMPICACHE__V9*/ meltfptr[8]),
			   (meltobject_ptr_t) ( /*_.NPRO__V24*/
					       meltfptr[23]));;
    MELT_LOCATION ("warmelt-genobj.melt:5323:/ quasiblock");


    /*^rawallocobj */
    /*rawallocobj */
    {
      melt_ptr_t newobj = 0;
      melt_raw_object_create (newobj,
			      (melt_ptr_t) (( /*!CLASS_OBJINITROUTINE */
					     meltfrout->tabval[8])), (6),
			      "CLASS_OBJINITROUTINE");
  /*_.INST__V41*/ meltfptr[40] =
	newobj;
    };
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OBV_TYPE",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V41*/ meltfptr[40])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V41*/ meltfptr[40]), (0),
			  (( /*!CTYPE_VALUE */ meltfrout->tabval[9])),
			  "OBV_TYPE");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OIE_DISCR",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V41*/ meltfptr[40])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V41*/ meltfptr[40]), (3),
			  ( /*_.ODISCR__V35*/ meltfptr[34]), "OIE_DISCR");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OIE_DATA",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V41*/ meltfptr[40])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V41*/ meltfptr[40]), (2),
			  ( /*_.NDROU__V2*/ meltfptr[1]), "OIE_DATA");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OIE_LOCVAR",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V41*/ meltfptr[40])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V41*/ meltfptr[40]), (4),
			  ( /*_.LOCVAR__V26*/ meltfptr[25]), "OIE_LOCVAR");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OIE_CNAME",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V41*/ meltfptr[40])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V41*/ meltfptr[40]), (1),
			  ( /*_.STRBUF2STRING__V38*/ meltfptr[37]),
			  "OIE_CNAME");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OIR_PROCROUTINE",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V41*/ meltfptr[40])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V41*/ meltfptr[40]), (5),
			  ( /*_.MAPOBJECT_GET__V39*/ meltfptr[38]),
			  "OIR_PROCROUTINE");
    ;
    /*^touchobj */

    melt_dbgtrace_written_object ( /*_.INST__V41*/ meltfptr[40],
				  "newly made instance");
    ;
    /*_.OIROUT__V40*/ meltfptr[39] = /*_.INST__V41*/ meltfptr[40];;

    {
      MELT_LOCATION ("warmelt-genobj.melt:5332:/ locexp");
      meltgc_put_mapobjects ((meltmapobjects_ptr_t)
			     ( /*_.COMPICACHE__V9*/ meltfptr[8]),
			     (meltobject_ptr_t) ( /*_.NDROU__V2*/
						 meltfptr[1]),
			     (melt_ptr_t) ( /*_.OIROUT__V40*/ meltfptr[39]));
    }
    ;
    MELT_LOCATION ("warmelt-genobj.melt:5333:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#IS_A__L8*/ meltfnum[7] =
      melt_is_instance_of ((melt_ptr_t) ( /*_.NPRO__V24*/ meltfptr[23]),
			   (melt_ptr_t) (( /*!CLASS_NREP_ROUTPROC */
					  meltfrout->tabval[11])));;
    MELT_LOCATION ("warmelt-genobj.melt:5333:/ cond");
    /*cond */ if ( /*_#IS_A__L8*/ meltfnum[7])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-genobj.melt:5334:/ quasiblock");


	  /*^getslot */
	  {
	    melt_ptr_t slot = NULL, obj = NULL;
	    obj = (melt_ptr_t) ( /*_.NPRO__V24*/ meltfptr[23]) /*=obj*/ ;
	    melt_object_get_field (slot, obj, 5, "NRPRO_CONST");
    /*_.PCONSTL__V44*/ meltfptr[43] = slot;
	  };
	  ;
   /*_#NBCONST__L9*/ meltfnum[8] =
	    (melt_list_length
	     ((melt_ptr_t) ( /*_.PCONSTL__V44*/ meltfptr[43])));;
	  /*^compute */
   /*_.BXOFF__V45*/ meltfptr[44] =
	    (meltgc_new_int
	     ((meltobject_ptr_t)
	      (( /*!DISCR_INTEGER */ meltfrout->tabval[12])), (0)));;
	  MELT_LOCATION ("warmelt-genobj.melt:5337:/ getslot");
	  {
	    melt_ptr_t slot = NULL, obj = NULL;
	    obj =
	      (melt_ptr_t) (( /*!konst_13_IROUTVAL_ */ meltfrout->
			     tabval[13])) /*=obj*/ ;
	    melt_object_get_field (slot, obj, 1, "NAMED_NAME");
    /*_.COMM__V46*/ meltfptr[45] = slot;
	  };
	  ;

	  {
	    MELT_LOCATION ("warmelt-genobj.melt:5339:/ locexp");
	    melt_put_int ((melt_ptr_t) ( /*_.OIROUT__V40*/ meltfptr[39]),
			  ( /*_#NBCONST__L9*/ meltfnum[8]));
	  }
	  ;
	  MELT_LOCATION ("warmelt-genobj.melt:5342:/ quasiblock");


	  /*^newclosure */
		   /*newclosure *//*_.LAMBDA___V48*/ meltfptr[47] =
	    (melt_ptr_t)
	    meltgc_new_closure ((meltobject_ptr_t)
				(((melt_ptr_t)
				  (MELT_PREDEF (DISCR_CLOSURE)))),
				(meltroutine_ptr_t) (( /*!konst_23 */
						      meltfrout->tabval[23])),
				(5));
	  ;
	  /*^putclosedv */
	  /*putclosv */
	  melt_assertmsg ("putclosv checkclo",
			  melt_magic_discr ((melt_ptr_t)
					    ( /*_.LAMBDA___V48*/
					     meltfptr[47])) ==
			  MELTOBMAG_CLOSURE);
	  melt_assertmsg ("putclosv checkoff", 0 >= 0
			  && 0 <
			  melt_closure_size ((melt_ptr_t)
					     ( /*_.LAMBDA___V48*/
					      meltfptr[47])));
	  ((meltclosure_ptr_t) /*_.LAMBDA___V48*/ meltfptr[47])->tabval[0] =
	    (melt_ptr_t) ( /*_.GCX__V3*/ meltfptr[2]);
	  ;
	  /*^putclosedv */
	  /*putclosv */
	  melt_assertmsg ("putclosv checkclo",
			  melt_magic_discr ((melt_ptr_t)
					    ( /*_.LAMBDA___V48*/
					     meltfptr[47])) ==
			  MELTOBMAG_CLOSURE);
	  melt_assertmsg ("putclosv checkoff", 1 >= 0
			  && 1 <
			  melt_closure_size ((melt_ptr_t)
					     ( /*_.LAMBDA___V48*/
					      meltfptr[47])));
	  ((meltclosure_ptr_t) /*_.LAMBDA___V48*/ meltfptr[47])->tabval[1] =
	    (melt_ptr_t) ( /*_.BXOFF__V45*/ meltfptr[44]);
	  ;
	  /*^putclosedv */
	  /*putclosv */
	  melt_assertmsg ("putclosv checkclo",
			  melt_magic_discr ((melt_ptr_t)
					    ( /*_.LAMBDA___V48*/
					     meltfptr[47])) ==
			  MELTOBMAG_CLOSURE);
	  melt_assertmsg ("putclosv checkoff", 2 >= 0
			  && 2 <
			  melt_closure_size ((melt_ptr_t)
					     ( /*_.LAMBDA___V48*/
					      meltfptr[47])));
	  ((meltclosure_ptr_t) /*_.LAMBDA___V48*/ meltfptr[47])->tabval[2] =
	    (melt_ptr_t) ( /*_.NLOC__V21*/ meltfptr[20]);
	  ;
	  /*^putclosedv */
	  /*putclosv */
	  melt_assertmsg ("putclosv checkclo",
			  melt_magic_discr ((melt_ptr_t)
					    ( /*_.LAMBDA___V48*/
					     meltfptr[47])) ==
			  MELTOBMAG_CLOSURE);
	  melt_assertmsg ("putclosv checkoff", 3 >= 0
			  && 3 <
			  melt_closure_size ((melt_ptr_t)
					     ( /*_.LAMBDA___V48*/
					      meltfptr[47])));
	  ((meltclosure_ptr_t) /*_.LAMBDA___V48*/ meltfptr[47])->tabval[3] =
	    (melt_ptr_t) ( /*_.OIROUT__V40*/ meltfptr[39]);
	  ;
	  /*^putclosedv */
	  /*putclosv */
	  melt_assertmsg ("putclosv checkclo",
			  melt_magic_discr ((melt_ptr_t)
					    ( /*_.LAMBDA___V48*/
					     meltfptr[47])) ==
			  MELTOBMAG_CLOSURE);
	  melt_assertmsg ("putclosv checkoff", 4 >= 0
			  && 4 <
			  melt_closure_size ((melt_ptr_t)
					     ( /*_.LAMBDA___V48*/
					      meltfptr[47])));
	  ((meltclosure_ptr_t) /*_.LAMBDA___V48*/ meltfptr[47])->tabval[4] =
	    (melt_ptr_t) ( /*_.INIBODY__V25*/ meltfptr[24]);
	  ;
	  /*_.LAMBDA___V47*/ meltfptr[46] = /*_.LAMBDA___V48*/ meltfptr[47];;
	  MELT_LOCATION ("warmelt-genobj.melt:5340:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^apply */
	  /*apply */
	  {
	    union meltparam_un argtab[1];
	    memset (&argtab, 0, sizeof (argtab));
	    /*^apply.arg */
	    argtab[0].meltbp_aptr =
	      (melt_ptr_t *) & /*_.LAMBDA___V47*/ meltfptr[46];
	    /*_.LIST_EVERY__V49*/ meltfptr[48] =
	      melt_apply ((meltclosure_ptr_t)
			  (( /*!LIST_EVERY */ meltfrout->tabval[14])),
			  (melt_ptr_t) ( /*_.PCONSTL__V44*/ meltfptr[43]),
			  (MELTBPARSTR_PTR ""), argtab, "",
			  (union meltparam_un *) 0);
	  }
	  ;
	  MELT_LOCATION ("warmelt-genobj.melt:5389:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
   /*_#GET_INT__L10*/ meltfnum[9] =
	    (melt_get_int ((melt_ptr_t) ( /*_.BXOFF__V45*/ meltfptr[44])));;
	  /*^compute */
   /*_#I__L11*/ meltfnum[10] =
	    (( /*_#GET_INT__L10*/ meltfnum[9]) > (0));;
	  MELT_LOCATION ("warmelt-genobj.melt:5389:/ cond");
	  /*cond */ if ( /*_#I__L11*/ meltfnum[10])	/*then */
	    {
	      /*^cond.then */
	      /*^block */
	      /*anyblock */
	      {

		MELT_LOCATION ("warmelt-genobj.melt:5390:/ checksignal");
		MELT_CHECK_SIGNAL ();
		;
		/*^quasiblock */


		/*^rawallocobj */
		/*rawallocobj */
		{
		  melt_ptr_t newobj = 0;
		  melt_raw_object_create (newobj,
					  (melt_ptr_t) (( /*!CLASS_OBJTOUCH */
							 meltfrout->
							 tabval[24])), (3),
					  "CLASS_OBJTOUCH");
      /*_.INST__V51*/ meltfptr[50] =
		    newobj;
		};
		;
		/*^putslot */
		/*putslot */
		melt_assertmsg ("putslot checkobj @OBI_LOC",
				melt_magic_discr ((melt_ptr_t)
						  ( /*_.INST__V51*/
						   meltfptr[50])) ==
				MELTOBMAG_OBJECT);
		melt_putfield_object (( /*_.INST__V51*/ meltfptr[50]), (0),
				      ( /*_.NLOC__V21*/ meltfptr[20]),
				      "OBI_LOC");
		;
		/*^putslot */
		/*putslot */
		melt_assertmsg ("putslot checkobj @OTOUCH_VAL",
				melt_magic_discr ((melt_ptr_t)
						  ( /*_.INST__V51*/
						   meltfptr[50])) ==
				MELTOBMAG_OBJECT);
		melt_putfield_object (( /*_.INST__V51*/ meltfptr[50]), (1),
				      ( /*_.OIROUT__V40*/ meltfptr[39]),
				      "OTOUCH_VAL");
		;
		/*^putslot */
		/*putslot */
		melt_assertmsg ("putslot checkobj @OTOUCH_COMMENT",
				melt_magic_discr ((melt_ptr_t)
						  ( /*_.INST__V51*/
						   meltfptr[50])) ==
				MELTOBMAG_OBJECT);
		melt_putfield_object (( /*_.INST__V51*/ meltfptr[50]), (2),
				      ( /*_.COMM__V46*/ meltfptr[45]),
				      "OTOUCH_COMMENT");
		;
		/*^touchobj */

		melt_dbgtrace_written_object ( /*_.INST__V51*/ meltfptr[50],
					      "newly made instance");
		;
		/*_.INST___V50*/ meltfptr[49] = /*_.INST__V51*/ meltfptr[50];;

		{
		  MELT_LOCATION ("warmelt-genobj.melt:5390:/ locexp");
		  meltgc_append_list ((melt_ptr_t)
				      ( /*_.INIBODY__V25*/ meltfptr[24]),
				      (melt_ptr_t) ( /*_.INST___V50*/
						    meltfptr[49]));
		}
		;
		/*epilog */

		MELT_LOCATION ("warmelt-genobj.melt:5389:/ clear");
	       /*clear *//*_.INST___V50*/ meltfptr[49] = 0;
	      }
	      ;
	    }			/*noelse */
	  ;

#if MELT_HAVE_DEBUG
	  MELT_LOCATION ("warmelt-genobj.melt:5395:/ cppif.then");
	  /*^block */
	  /*anyblock */
	  {


	    {
	      /*^locexp */
	      /*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	      melt_dbgcounter++;
#endif
	      ;
	    }
	    ;
	    /*^checksignal */
	    MELT_CHECK_SIGNAL ();
	    ;
     /*_#MELT_NEED_DBG__L12*/ meltfnum[11] =
	      /*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	      ( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	      0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	      ;;
	    MELT_LOCATION ("warmelt-genobj.melt:5395:/ cond");
	    /*cond */ if ( /*_#MELT_NEED_DBG__L12*/ meltfnum[11])	/*then */
	      {
		/*^cond.then */
		/*^block */
		/*anyblock */
		{

       /*_#THE_MELTCALLCOUNT__L13*/ meltfnum[12] =
#ifdef meltcallcount
		    meltcallcount	/* the_meltcallcount */
#else
		    0L
#endif /* meltcallcount the_meltcallcount */
		    ;;
		  MELT_LOCATION ("warmelt-genobj.melt:5395:/ checksignal");
		  MELT_CHECK_SIGNAL ();
		  ;
		  /*^apply */
		  /*apply */
		  {
		    union meltparam_un argtab[5];
		    memset (&argtab, 0, sizeof (argtab));
		    /*^apply.arg */
		    argtab[0].meltbp_long =
		      /*_#THE_MELTCALLCOUNT__L13*/ meltfnum[12];
		    /*^apply.arg */
		    argtab[1].meltbp_cstring = "warmelt-genobj.melt";
		    /*^apply.arg */
		    argtab[2].meltbp_long = 5395;
		    /*^apply.arg */
		    argtab[3].meltbp_cstring =
		      "compilobj_dataroutine final oirout=";
		    /*^apply.arg */
		    argtab[4].meltbp_aptr =
		      (melt_ptr_t *) & /*_.OIROUT__V40*/ meltfptr[39];
		    /*_.MELT_DEBUG_FUN__V54*/ meltfptr[53] =
		      melt_apply ((meltclosure_ptr_t)
				  (( /*!MELT_DEBUG_FUN */ meltfrout->
				    tabval[25])),
				  (melt_ptr_t) (( /*nil */ NULL)),
				  (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_PTR ""), argtab, "",
				  (union meltparam_un *) 0);
		  }
		  ;
		  /*_.IF___V53*/ meltfptr[52] =
		    /*_.MELT_DEBUG_FUN__V54*/ meltfptr[53];;
		  /*epilog */

		  MELT_LOCATION ("warmelt-genobj.melt:5395:/ clear");
		 /*clear *//*_#THE_MELTCALLCOUNT__L13*/ meltfnum[12] = 0;
		  /*^clear */
		 /*clear *//*_.MELT_DEBUG_FUN__V54*/ meltfptr[53] = 0;
		}
		;
	      }
	    else
	      {			/*^cond.else */

      /*_.IF___V53*/ meltfptr[52] = NULL;;
	      }
	    ;
	    MELT_LOCATION ("warmelt-genobj.melt:5395:/ quasiblock");


	    /*_.PROGN___V55*/ meltfptr[53] = /*_.IF___V53*/ meltfptr[52];;
	    /*^compute */
	    /*_.IFCPP___V52*/ meltfptr[49] = /*_.PROGN___V55*/ meltfptr[53];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-genobj.melt:5395:/ clear");
	       /*clear *//*_#MELT_NEED_DBG__L12*/ meltfnum[11] = 0;
	    /*^clear */
	       /*clear *//*_.IF___V53*/ meltfptr[52] = 0;
	    /*^clear */
	       /*clear *//*_.PROGN___V55*/ meltfptr[53] = 0;
	  }

#else /*MELT_HAVE_DEBUG */
	  /*^cppif.else */
	  /*_.IFCPP___V52*/ meltfptr[49] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
	  ;
	  /*^compute */
	  /*_.LET___V43*/ meltfptr[42] = /*_.OIROUT__V40*/ meltfptr[39];;

	  MELT_LOCATION ("warmelt-genobj.melt:5334:/ clear");
	     /*clear *//*_.PCONSTL__V44*/ meltfptr[43] = 0;
	  /*^clear */
	     /*clear *//*_#NBCONST__L9*/ meltfnum[8] = 0;
	  /*^clear */
	     /*clear *//*_.BXOFF__V45*/ meltfptr[44] = 0;
	  /*^clear */
	     /*clear *//*_.COMM__V46*/ meltfptr[45] = 0;
	  /*^clear */
	     /*clear *//*_.LAMBDA___V47*/ meltfptr[46] = 0;
	  /*^clear */
	     /*clear *//*_.LIST_EVERY__V49*/ meltfptr[48] = 0;
	  /*^clear */
	     /*clear *//*_#GET_INT__L10*/ meltfnum[9] = 0;
	  /*^clear */
	     /*clear *//*_#I__L11*/ meltfnum[10] = 0;
	  /*^clear */
	     /*clear *//*_.IFCPP___V52*/ meltfptr[49] = 0;
	  /*_.IF___V42*/ meltfptr[41] = /*_.LET___V43*/ meltfptr[42];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-genobj.melt:5333:/ clear");
	     /*clear *//*_.LET___V43*/ meltfptr[42] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

  /*_.IF___V42*/ meltfptr[41] = NULL;;
      }
    ;
    /*^compute */
    /*_.LET___V37*/ meltfptr[35] = /*_.IF___V42*/ meltfptr[41];;

    MELT_LOCATION ("warmelt-genobj.melt:5321:/ clear");
	   /*clear *//*_.STRBUF2STRING__V38*/ meltfptr[37] = 0;
    /*^clear */
	   /*clear *//*_.MAPOBJECT_GET__V39*/ meltfptr[38] = 0;
    /*^clear */
	   /*clear *//*_.OIROUT__V40*/ meltfptr[39] = 0;
    /*^clear */
	   /*clear *//*_#IS_A__L8*/ meltfnum[7] = 0;
    /*^clear */
	   /*clear *//*_.IF___V42*/ meltfptr[41] = 0;
    /*_.LET___V33*/ meltfptr[27] = /*_.LET___V37*/ meltfptr[35];;

    MELT_LOCATION ("warmelt-genobj.melt:5311:/ clear");
	   /*clear *//*_.NAMBUF__V34*/ meltfptr[29] = 0;
    /*^clear */
	   /*clear *//*_#DRK__L7*/ meltfnum[4] = 0;
    /*^clear */
	   /*clear *//*_.ODISCR__V35*/ meltfptr[34] = 0;
    /*^clear */
	   /*clear *//*_.LET___V37*/ meltfptr[35] = 0;
    /*_.LET___V18*/ meltfptr[15] = /*_.LET___V33*/ meltfptr[27];;

    MELT_LOCATION ("warmelt-genobj.melt:5298:/ clear");
	   /*clear *//*_.NAM__V19*/ meltfptr[16] = 0;
    /*^clear */
	   /*clear *//*_.DISX__V20*/ meltfptr[19] = 0;
    /*^clear */
	   /*clear *//*_.NLOC__V21*/ meltfptr[20] = 0;
    /*^clear */
	   /*clear *//*_.DRANK__V22*/ meltfptr[21] = 0;
    /*^clear */
	   /*clear *//*_.NAM__V23*/ meltfptr[22] = 0;
    /*^clear */
	   /*clear *//*_.NPRO__V24*/ meltfptr[23] = 0;
    /*^clear */
	   /*clear *//*_.INIBODY__V25*/ meltfptr[24] = 0;
    /*^clear */
	   /*clear *//*_.LOCVAR__V26*/ meltfptr[25] = 0;
    /*^clear */
	   /*clear *//*_#NULL__L4*/ meltfnum[0] = 0;
    /*^clear */
	   /*clear *//*_.IF___V27*/ meltfptr[26] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V31*/ meltfptr[28] = 0;
    /*^clear */
	   /*clear *//*_.LET___V33*/ meltfptr[27] = 0;
    /*_.LET___V8*/ meltfptr[6] = /*_.LET___V18*/ meltfptr[15];;

    MELT_LOCATION ("warmelt-genobj.melt:5288:/ clear");
	   /*clear *//*_.COMPICACHE__V9*/ meltfptr[8] = 0;
    /*^clear */
	   /*clear *//*_.INIROUT__V10*/ meltfptr[9] = 0;
    /*^clear */
	   /*clear *//*_.CHOBJ__V11*/ meltfptr[10] = 0;
    /*^clear */
	   /*clear *//*_.UNUSED_STUFF__V12*/ meltfptr[11] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V13*/ meltfptr[12] = 0;
    /*^clear */
	   /*clear *//*_.IF___V15*/ meltfptr[13] = 0;
    /*^clear */
	   /*clear *//*_.LET___V18*/ meltfptr[15] = 0;
    MELT_LOCATION ("warmelt-genobj.melt:5285:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.LET___V8*/ meltfptr[6];;

    {
      MELT_LOCATION ("warmelt-genobj.melt:5285:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V4*/ meltfptr[3] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V6*/ meltfptr[4] = 0;
    /*^clear */
	   /*clear *//*_.LET___V8*/ meltfptr[6] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("COMPILOBJ_DATAROUTINE", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_154_warmelt_genobj_COMPILOBJ_DATAROUTINE_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_154_warmelt_genobj_COMPILOBJ_DATAROUTINE */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_155_warmelt_genobj_LAMBDA___52__ (meltclosure_ptr_t meltclosp_,
					   melt_ptr_t meltfirstargp_,
					   const melt_argdescr_cell_t
					   meltxargdescr_[],
					   union meltparam_un * meltxargtab_,
					   const melt_argdescr_cell_t
					   meltxresdescr_[],
					   union meltparam_un * meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_155_warmelt_genobj_LAMBDA___52___melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_155_warmelt_genobj_LAMBDA___52___st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 22
    melt_ptr_t mcfr_varptr[22];
#define MELTFRAM_NBVARNUM 8
    long mcfr_varnum[8];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_155_warmelt_genobj_LAMBDA___52__ is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_155_warmelt_genobj_LAMBDA___52___st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 22; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_155_warmelt_genobj_LAMBDA___52__ nbval 22*/
  meltfram__.mcfr_nbvar = 22 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("LAMBDA_", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-genobj.melt:5342:/ getarg");
 /*_.CONSTX__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {

    MELT_LOCATION ("warmelt-genobj.melt:5344:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#IS_A__L1*/ meltfnum[0] =
      melt_is_instance_of ((melt_ptr_t) ( /*_.CONSTX__V2*/ meltfptr[1]),
			   (melt_ptr_t) (( /*!CLASS_NREP_CONSTOCC */
					  meltfrout->tabval[0])));;
    MELT_LOCATION ("warmelt-genobj.melt:5344:/ cond");
    /*cond */ if ( /*_#IS_A__L1*/ meltfnum[0])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-genobj.melt:5345:/ quasiblock");


	  /*^getslot */
	  {
	    melt_ptr_t slot = NULL, obj = NULL;
	    obj = (melt_ptr_t) ( /*_.CONSTX__V2*/ meltfptr[1]) /*=obj*/ ;
	    melt_object_get_field (slot, obj, 3, "NOCC_BIND");
    /*_.CNSTBIND__V4*/ meltfptr[3] = slot;
	  };
	  ;

#if MELT_HAVE_DEBUG
	  MELT_LOCATION ("warmelt-genobj.melt:5346:/ cppif.then");
	  /*^block */
	  /*anyblock */
	  {

	    /*^checksignal */
	    MELT_CHECK_SIGNAL ();
	    ;
     /*_#IS_A__L2*/ meltfnum[1] =
	      melt_is_instance_of ((melt_ptr_t)
				   ( /*_.CNSTBIND__V4*/ meltfptr[3]),
				   (melt_ptr_t) (( /*!CLASS_ANY_BINDING */
						  meltfrout->tabval[1])));;
	    MELT_LOCATION ("warmelt-genobj.melt:5346:/ cond");
	    /*cond */ if ( /*_#IS_A__L2*/ meltfnum[1])	/*then */
	      {
		/*^cond.then */
		/*_.IFELSE___V6*/ meltfptr[5] = ( /*nil */ NULL);;
	      }
	    else
	      {
		MELT_LOCATION ("warmelt-genobj.melt:5346:/ cond.else");

		/*^block */
		/*anyblock */
		{




		  {
		    /*^locexp */
		    melt_assert_failed (("compilobj_dataroutine check cnstbind"), ("warmelt-genobj.melt") ? ("warmelt-genobj.melt") : __FILE__, (5346) ? (5346) : __LINE__, __FUNCTION__);
		    ;
		  }
		  ;
		 /*clear *//*_.IFELSE___V6*/ meltfptr[5] = 0;
		  /*epilog */
		}
		;
	      }
	    ;
	    /*^compute */
	    /*_.IFCPP___V5*/ meltfptr[4] = /*_.IFELSE___V6*/ meltfptr[5];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-genobj.melt:5346:/ clear");
	       /*clear *//*_#IS_A__L2*/ meltfnum[1] = 0;
	    /*^clear */
	       /*clear *//*_.IFELSE___V6*/ meltfptr[5] = 0;
	  }

#else /*MELT_HAVE_DEBUG */
	  /*^cppif.else */
	  /*_.IFCPP___V5*/ meltfptr[4] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
	  ;
	  MELT_LOCATION ("warmelt-genobj.melt:5347:/ quasiblock");


	  MELT_LOCATION ("warmelt-genobj.melt:5348:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^msend */
	  /*msend */
	  {
	    union meltparam_un argtab[1];
	    memset (&argtab, 0, sizeof (argtab));
	    /*^ojbmsend.arg */
	    argtab[0].meltbp_aptr =
	      (melt_ptr_t *) & ( /*~GCX */ meltfclos->tabval[0]);
	    /*_.OCONSTX__V7*/ meltfptr[5] =
	      meltgc_send ((melt_ptr_t) ( /*_.CNSTBIND__V4*/ meltfptr[3]),
			   (melt_ptr_t) (( /*!COMPILE_OBJ */ meltfrout->
					  tabval[2])), (MELTBPARSTR_PTR ""),
			   argtab, "", (union meltparam_un *) 0);
	  }
	  ;
   /*_#OFF__L3*/ meltfnum[1] =
	    (melt_get_int
	     ((melt_ptr_t) (( /*~BXOFF */ meltfclos->tabval[1]))));;
	  MELT_LOCATION ("warmelt-genobj.melt:5351:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^cond */
	  /*cond */ if ( /*_.OCONSTX__V7*/ meltfptr[5])	/*then */
	    {
	      /*^cond.then */
	      /*^block */
	      /*anyblock */
	      {

		MELT_LOCATION ("warmelt-genobj.melt:5352:/ quasiblock");


		MELT_LOCATION ("warmelt-genobj.melt:5354:/ checksignal");
		MELT_CHECK_SIGNAL ();
		;
     /*_#IS_A__L4*/ meltfnum[3] =
		  melt_is_instance_of ((melt_ptr_t)
				       ( /*_.CNSTBIND__V4*/ meltfptr[3]),
				       (melt_ptr_t) (( /*!CLASS_FIXED_BINDING */ meltfrout->tabval[3])));;
		MELT_LOCATION ("warmelt-genobj.melt:5354:/ cond");
		/*cond */ if ( /*_#IS_A__L4*/ meltfnum[3])	/*then */
		  {
		    /*^cond.then */
		    /*^block */
		    /*anyblock */
		    {

		      MELT_LOCATION
			("warmelt-genobj.melt:5355:/ checksignal");
		      MELT_CHECK_SIGNAL ();
		      ;
       /*_.MAKE_INTEGERBOX__V9*/ meltfptr[8] =
			(meltgc_new_int
			 ((meltobject_ptr_t)
			  (( /*!DISCR_INTEGER */ meltfrout->tabval[5])),
			  ( /*_#OFF__L3*/ meltfnum[1])));;
		      MELT_LOCATION ("warmelt-genobj.melt:5355:/ quasiblock");


		      /*^rawallocobj */
		      /*rawallocobj */
		      {
			melt_ptr_t newobj = 0;
			melt_raw_object_create (newobj,
						(melt_ptr_t) (( /*!CLASS_OBJPUTROUTCONSTNOTNULL */ meltfrout->tabval[4])), (4), "CLASS_OBJPUTROUTCONSTNOTNULL");
	/*_.INST__V11*/ meltfptr[10] =
			  newobj;
		      };
		      ;
		      /*^putslot */
		      /*putslot */
		      melt_assertmsg ("putslot checkobj @OBI_LOC",
				      melt_magic_discr ((melt_ptr_t)
							( /*_.INST__V11*/
							 meltfptr[10])) ==
				      MELTOBMAG_OBJECT);
		      melt_putfield_object (( /*_.INST__V11*/ meltfptr[10]),
					    (0),
					    (( /*~NLOC */ meltfclos->
					      tabval[2])), "OBI_LOC");
		      ;
		      /*^putslot */
		      /*putslot */
		      melt_assertmsg ("putslot checkobj @OPRCONST_ROUT",
				      melt_magic_discr ((melt_ptr_t)
							( /*_.INST__V11*/
							 meltfptr[10])) ==
				      MELTOBMAG_OBJECT);
		      melt_putfield_object (( /*_.INST__V11*/ meltfptr[10]),
					    (1),
					    (( /*~OIROUT */ meltfclos->
					      tabval[3])), "OPRCONST_ROUT");
		      ;
		      /*^putslot */
		      /*putslot */
		      melt_assertmsg ("putslot checkobj @OPRCONST_OFF",
				      melt_magic_discr ((melt_ptr_t)
							( /*_.INST__V11*/
							 meltfptr[10])) ==
				      MELTOBMAG_OBJECT);
		      melt_putfield_object (( /*_.INST__V11*/ meltfptr[10]),
					    (2),
					    ( /*_.MAKE_INTEGERBOX__V9*/
					     meltfptr[8]), "OPRCONST_OFF");
		      ;
		      /*^putslot */
		      /*putslot */
		      melt_assertmsg ("putslot checkobj @OPRCONST_CVAL",
				      melt_magic_discr ((melt_ptr_t)
							( /*_.INST__V11*/
							 meltfptr[10])) ==
				      MELTOBMAG_OBJECT);
		      melt_putfield_object (( /*_.INST__V11*/ meltfptr[10]),
					    (3),
					    ( /*_.OCONSTX__V7*/ meltfptr[5]),
					    "OPRCONST_CVAL");
		      ;
		      /*^touchobj */

		      melt_dbgtrace_written_object ( /*_.INST__V11*/
						    meltfptr[10],
						    "newly made instance");
		      ;
		      /*_.INST___V10*/ meltfptr[9] =
			/*_.INST__V11*/ meltfptr[10];;
		      /*^compute */
		      /*_.IPUT__V8*/ meltfptr[7] =
			/*_.INST___V10*/ meltfptr[9];;
		      /*epilog */

		      MELT_LOCATION ("warmelt-genobj.melt:5354:/ clear");
		 /*clear *//*_.MAKE_INTEGERBOX__V9*/ meltfptr[8] = 0;
		      /*^clear */
		 /*clear *//*_.INST___V10*/ meltfptr[9] = 0;
		    }
		    ;
		  }
		else
		  {		/*^cond.else */

		    /*^block */
		    /*anyblock */
		    {

		      MELT_LOCATION
			("warmelt-genobj.melt:5360:/ checksignal");
		      MELT_CHECK_SIGNAL ();
		      ;
       /*_.MAKE_INTEGERBOX__V12*/ meltfptr[8] =
			(meltgc_new_int
			 ((meltobject_ptr_t)
			  (( /*!DISCR_INTEGER */ meltfrout->tabval[5])),
			  ( /*_#OFF__L3*/ meltfnum[1])));;
		      MELT_LOCATION ("warmelt-genobj.melt:5360:/ quasiblock");


		      /*^rawallocobj */
		      /*rawallocobj */
		      {
			melt_ptr_t newobj = 0;
			melt_raw_object_create (newobj,
						(melt_ptr_t) (( /*!CLASS_OBJPUTROUTCONST */ meltfrout->tabval[6])), (4), "CLASS_OBJPUTROUTCONST");
	/*_.INST__V14*/ meltfptr[13] =
			  newobj;
		      };
		      ;
		      /*^putslot */
		      /*putslot */
		      melt_assertmsg ("putslot checkobj @OBI_LOC",
				      melt_magic_discr ((melt_ptr_t)
							( /*_.INST__V14*/
							 meltfptr[13])) ==
				      MELTOBMAG_OBJECT);
		      melt_putfield_object (( /*_.INST__V14*/ meltfptr[13]),
					    (0),
					    (( /*~NLOC */ meltfclos->
					      tabval[2])), "OBI_LOC");
		      ;
		      /*^putslot */
		      /*putslot */
		      melt_assertmsg ("putslot checkobj @OPRCONST_ROUT",
				      melt_magic_discr ((melt_ptr_t)
							( /*_.INST__V14*/
							 meltfptr[13])) ==
				      MELTOBMAG_OBJECT);
		      melt_putfield_object (( /*_.INST__V14*/ meltfptr[13]),
					    (1),
					    (( /*~OIROUT */ meltfclos->
					      tabval[3])), "OPRCONST_ROUT");
		      ;
		      /*^putslot */
		      /*putslot */
		      melt_assertmsg ("putslot checkobj @OPRCONST_OFF",
				      melt_magic_discr ((melt_ptr_t)
							( /*_.INST__V14*/
							 meltfptr[13])) ==
				      MELTOBMAG_OBJECT);
		      melt_putfield_object (( /*_.INST__V14*/ meltfptr[13]),
					    (2),
					    ( /*_.MAKE_INTEGERBOX__V12*/
					     meltfptr[8]), "OPRCONST_OFF");
		      ;
		      /*^putslot */
		      /*putslot */
		      melt_assertmsg ("putslot checkobj @OPRCONST_CVAL",
				      melt_magic_discr ((melt_ptr_t)
							( /*_.INST__V14*/
							 meltfptr[13])) ==
				      MELTOBMAG_OBJECT);
		      melt_putfield_object (( /*_.INST__V14*/ meltfptr[13]),
					    (3),
					    ( /*_.OCONSTX__V7*/ meltfptr[5]),
					    "OPRCONST_CVAL");
		      ;
		      /*^touchobj */

		      melt_dbgtrace_written_object ( /*_.INST__V14*/
						    meltfptr[13],
						    "newly made instance");
		      ;
		      /*_.INST___V13*/ meltfptr[9] =
			/*_.INST__V14*/ meltfptr[13];;
		      /*^compute */
		      /*_.IPUT__V8*/ meltfptr[7] =
			/*_.INST___V13*/ meltfptr[9];;
		      /*epilog */

		      MELT_LOCATION ("warmelt-genobj.melt:5354:/ clear");
		 /*clear *//*_.MAKE_INTEGERBOX__V12*/ meltfptr[8] = 0;
		      /*^clear */
		 /*clear *//*_.INST___V13*/ meltfptr[9] = 0;
		    }
		    ;
		  }
		;

		{
		  MELT_LOCATION ("warmelt-genobj.melt:5367:/ locexp");
		  meltgc_append_list ((melt_ptr_t)
				      (( /*~INIBODY */ meltfclos->tabval[4])),
				      (melt_ptr_t) ( /*_.IPUT__V8*/
						    meltfptr[7]));
		}
		;
     /*_#I__L5*/ meltfnum[4] =
		  (( /*_#OFF__L3*/ meltfnum[1]) + (1));;

		{
		  MELT_LOCATION ("warmelt-genobj.melt:5368:/ locexp");
		  melt_put_int ((melt_ptr_t)
				(( /*~BXOFF */ meltfclos->tabval[1])),
				( /*_#I__L5*/ meltfnum[4]));
		}
		;

		MELT_LOCATION ("warmelt-genobj.melt:5352:/ clear");
	       /*clear *//*_#IS_A__L4*/ meltfnum[3] = 0;
		/*^clear */
	       /*clear *//*_.IPUT__V8*/ meltfptr[7] = 0;
		/*^clear */
	       /*clear *//*_#I__L5*/ meltfnum[4] = 0;
		/*epilog */
	      }
	      ;
	    }			/*noelse */
	  ;

	  MELT_LOCATION ("warmelt-genobj.melt:5347:/ clear");
	     /*clear *//*_.OCONSTX__V7*/ meltfptr[5] = 0;
	  /*^clear */
	     /*clear *//*_#OFF__L3*/ meltfnum[1] = 0;
	  /*_.IFELSE___V3*/ meltfptr[2] = /*_.IFCPP___V5*/ meltfptr[4];;

	  MELT_LOCATION ("warmelt-genobj.melt:5345:/ clear");
	     /*clear *//*_.CNSTBIND__V4*/ meltfptr[3] = 0;
	  /*^clear */
	     /*clear *//*_.IFCPP___V5*/ meltfptr[4] = 0;
	  /*epilog */
	}
	;
      }
    else
      {
	MELT_LOCATION ("warmelt-genobj.melt:5344:/ cond.else");

	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-genobj.melt:5370:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
   /*_#IS_A__L6*/ meltfnum[3] =
	    melt_is_instance_of ((melt_ptr_t) ( /*_.CONSTX__V2*/ meltfptr[1]),
				 (melt_ptr_t) (( /*!CLASS_NREP_QUASIDATA */
						meltfrout->tabval[7])));;
	  MELT_LOCATION ("warmelt-genobj.melt:5370:/ cond");
	  /*cond */ if ( /*_#IS_A__L6*/ meltfnum[3])	/*then */
	    {
	      /*^cond.then */
	      /*^block */
	      /*anyblock */
	      {

		MELT_LOCATION ("warmelt-genobj.melt:5371:/ quasiblock");


		MELT_LOCATION ("warmelt-genobj.melt:5372:/ checksignal");
		MELT_CHECK_SIGNAL ();
		;
		/*^msend */
		/*msend */
		{
		  union meltparam_un argtab[1];
		  memset (&argtab, 0, sizeof (argtab));
		  /*^ojbmsend.arg */
		  argtab[0].meltbp_aptr =
		    (melt_ptr_t *) & ( /*~GCX */ meltfclos->tabval[0]);
		  /*_.OCONSTX__V16*/ meltfptr[9] =
		    meltgc_send ((melt_ptr_t) ( /*_.CONSTX__V2*/ meltfptr[1]),
				 (melt_ptr_t) (( /*!COMPILE_OBJ */ meltfrout->
						tabval[2])),
				 (MELTBPARSTR_PTR ""), argtab, "",
				 (union meltparam_un *) 0);
		}
		;
     /*_#OFF__L7*/ meltfnum[4] =
		  (melt_get_int
		   ((melt_ptr_t) (( /*~BXOFF */ meltfclos->tabval[1]))));;
		MELT_LOCATION ("warmelt-genobj.melt:5375:/ checksignal");
		MELT_CHECK_SIGNAL ();
		;
		/*^cond */
		/*cond */ if ( /*_.OCONSTX__V16*/ meltfptr[9])	/*then */
		  {
		    /*^cond.then */
		    /*^block */
		    /*anyblock */
		    {

		      MELT_LOCATION ("warmelt-genobj.melt:5376:/ quasiblock");


		      /*^checksignal */
		      MELT_CHECK_SIGNAL ();
		      ;
       /*_.MAKE_INTEGERBOX__V17*/ meltfptr[7] =
			(meltgc_new_int
			 ((meltobject_ptr_t)
			  (( /*!DISCR_INTEGER */ meltfrout->tabval[5])),
			  ( /*_#OFF__L7*/ meltfnum[4])));;
		      MELT_LOCATION ("warmelt-genobj.melt:5376:/ quasiblock");


		      /*^rawallocobj */
		      /*rawallocobj */
		      {
			melt_ptr_t newobj = 0;
			melt_raw_object_create (newobj,
						(melt_ptr_t) (( /*!CLASS_OBJPUTROUTCONST */ meltfrout->tabval[6])), (4), "CLASS_OBJPUTROUTCONST");
	/*_.INST__V19*/ meltfptr[3] =
			  newobj;
		      };
		      ;
		      /*^putslot */
		      /*putslot */
		      melt_assertmsg ("putslot checkobj @OBI_LOC",
				      melt_magic_discr ((melt_ptr_t)
							( /*_.INST__V19*/
							 meltfptr[3])) ==
				      MELTOBMAG_OBJECT);
		      melt_putfield_object (( /*_.INST__V19*/ meltfptr[3]),
					    (0),
					    (( /*~NLOC */ meltfclos->
					      tabval[2])), "OBI_LOC");
		      ;
		      /*^putslot */
		      /*putslot */
		      melt_assertmsg ("putslot checkobj @OPRCONST_ROUT",
				      melt_magic_discr ((melt_ptr_t)
							( /*_.INST__V19*/
							 meltfptr[3])) ==
				      MELTOBMAG_OBJECT);
		      melt_putfield_object (( /*_.INST__V19*/ meltfptr[3]),
					    (1),
					    (( /*~OIROUT */ meltfclos->
					      tabval[3])), "OPRCONST_ROUT");
		      ;
		      /*^putslot */
		      /*putslot */
		      melt_assertmsg ("putslot checkobj @OPRCONST_OFF",
				      melt_magic_discr ((melt_ptr_t)
							( /*_.INST__V19*/
							 meltfptr[3])) ==
				      MELTOBMAG_OBJECT);
		      melt_putfield_object (( /*_.INST__V19*/ meltfptr[3]),
					    (2),
					    ( /*_.MAKE_INTEGERBOX__V17*/
					     meltfptr[7]), "OPRCONST_OFF");
		      ;
		      /*^putslot */
		      /*putslot */
		      melt_assertmsg ("putslot checkobj @OPRCONST_CVAL",
				      melt_magic_discr ((melt_ptr_t)
							( /*_.INST__V19*/
							 meltfptr[3])) ==
				      MELTOBMAG_OBJECT);
		      melt_putfield_object (( /*_.INST__V19*/ meltfptr[3]),
					    (3),
					    ( /*_.OCONSTX__V16*/ meltfptr[9]),
					    "OPRCONST_CVAL");
		      ;
		      /*^touchobj */

		      melt_dbgtrace_written_object ( /*_.INST__V19*/
						    meltfptr[3],
						    "newly made instance");
		      ;
		      /*_.IPUT__V18*/ meltfptr[5] =
			/*_.INST__V19*/ meltfptr[3];;

		      {
			MELT_LOCATION ("warmelt-genobj.melt:5381:/ locexp");
			meltgc_append_list ((melt_ptr_t)
					    (( /*~INIBODY */ meltfclos->
					      tabval[4])),
					    (melt_ptr_t) ( /*_.IPUT__V18*/
							  meltfptr[5]));
		      }
		      ;
       /*_#I__L8*/ meltfnum[1] =
			(( /*_#OFF__L7*/ meltfnum[4]) + (1));;



		      {
			MELT_LOCATION ("warmelt-genobj.melt:5382:/ locexp");
			melt_put_int ((melt_ptr_t)
				      (( /*~BXOFF */ meltfclos->tabval[1])),
				      ( /*_#I__L8*/ meltfnum[1]));
		      }
		      ;
		 /*clear *//*_.IFELSE___V15*/ meltfptr[8] = 0;

		      MELT_LOCATION ("warmelt-genobj.melt:5376:/ clear");
		 /*clear *//*_.MAKE_INTEGERBOX__V17*/ meltfptr[7] = 0;
		      /*^clear */
		 /*clear *//*_.IPUT__V18*/ meltfptr[5] = 0;
		      /*^clear */
		 /*clear *//*_#I__L8*/ meltfnum[1] = 0;
		      /*epilog */
		    }
		    ;
		  }
		else
		  {
		    MELT_LOCATION ("warmelt-genobj.melt:5375:/ cond.else");

      /*_.IFELSE___V15*/ meltfptr[8] = NULL;;
		  }
		;

		MELT_LOCATION ("warmelt-genobj.melt:5371:/ clear");
	       /*clear *//*_.OCONSTX__V16*/ meltfptr[9] = 0;
		/*^clear */
	       /*clear *//*_#OFF__L7*/ meltfnum[4] = 0;
		/*epilog */
	      }
	      ;
	    }
	  else
	    {
	      MELT_LOCATION ("warmelt-genobj.melt:5370:/ cond.else");

	      /*^block */
	      /*anyblock */
	      {


#if MELT_HAVE_DEBUG
		MELT_LOCATION ("warmelt-genobj.melt:5384:/ cppif.then");
		/*^block */
		/*anyblock */
		{

		  /*^checksignal */
		  MELT_CHECK_SIGNAL ();
		  ;
		  /*^cond */
		  /*cond */ if (( /*nil */ NULL))	/*then */
		    {
		      /*^cond.then */
		      /*_.IFELSE___V21*/ meltfptr[7] = ( /*nil */ NULL);;
		    }
		  else
		    {
		      MELT_LOCATION ("warmelt-genobj.melt:5384:/ cond.else");

		      /*^block */
		      /*anyblock */
		      {




			{
			  /*^locexp */
			  melt_assert_failed (("compilobj_dataroutine unexepected constx"), ("warmelt-genobj.melt") ? ("warmelt-genobj.melt") : __FILE__, (5384) ? (5384) : __LINE__, __FUNCTION__);
			  ;
			}
			;
		   /*clear *//*_.IFELSE___V21*/ meltfptr[7] = 0;
			/*epilog */
		      }
		      ;
		    }
		  ;
		  /*^compute */
		  /*_.IFCPP___V20*/ meltfptr[4] =
		    /*_.IFELSE___V21*/ meltfptr[7];;
		  /*epilog */

		  MELT_LOCATION ("warmelt-genobj.melt:5384:/ clear");
		 /*clear *//*_.IFELSE___V21*/ meltfptr[7] = 0;
		}

#else /*MELT_HAVE_DEBUG */
		/*^cppif.else */
		/*_.IFCPP___V20*/ meltfptr[4] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
		;
		MELT_LOCATION ("warmelt-genobj.melt:5383:/ quasiblock");


		/*_.PROGN___V22*/ meltfptr[5] =
		  /*_.IFCPP___V20*/ meltfptr[4];;
		/*^compute */
		/*_.IFELSE___V15*/ meltfptr[8] =
		  /*_.PROGN___V22*/ meltfptr[5];;
		/*epilog */

		MELT_LOCATION ("warmelt-genobj.melt:5370:/ clear");
	       /*clear *//*_.IFCPP___V20*/ meltfptr[4] = 0;
		/*^clear */
	       /*clear *//*_.PROGN___V22*/ meltfptr[5] = 0;
	      }
	      ;
	    }
	  ;
	  /*_.IFELSE___V3*/ meltfptr[2] = /*_.IFELSE___V15*/ meltfptr[8];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-genobj.melt:5344:/ clear");
	     /*clear *//*_#IS_A__L6*/ meltfnum[3] = 0;
	  /*^clear */
	     /*clear *//*_.IFELSE___V15*/ meltfptr[8] = 0;
	}
	;
      }
    ;
    MELT_LOCATION ("warmelt-genobj.melt:5342:/ quasiblock");


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.IFELSE___V3*/ meltfptr[2];;

    {
      MELT_LOCATION ("warmelt-genobj.melt:5342:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_#IS_A__L1*/ meltfnum[0] = 0;
    /*^clear */
	   /*clear *//*_.IFELSE___V3*/ meltfptr[2] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("LAMBDA_", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_155_warmelt_genobj_LAMBDA___52___melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_155_warmelt_genobj_LAMBDA___52__ */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_156_warmelt_genobj_COMPILOBJ_QUASIDATA_CURRENT_MODULE_ENVIRONMENT_CONTAINER
  (meltclosure_ptr_t meltclosp_, melt_ptr_t meltfirstargp_,
   const melt_argdescr_cell_t meltxargdescr_[],
   union meltparam_un * meltxargtab_,
   const melt_argdescr_cell_t meltxresdescr_[],
   union meltparam_un * meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_156_warmelt_genobj_COMPILOBJ_QUASIDATA_CURRENT_MODULE_ENVIRONMENT_CONTAINER_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_156_warmelt_genobj_COMPILOBJ_QUASIDATA_CURRENT_MODULE_ENVIRONMENT_CONTAINER_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 10
    melt_ptr_t mcfr_varptr[10];
#define MELTFRAM_NBVARNUM 2
    long mcfr_varnum[2];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_156_warmelt_genobj_COMPILOBJ_QUASIDATA_CURRENT_MODULE_ENVIRONMENT_CONTAINER is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct
	 frame_meltrout_156_warmelt_genobj_COMPILOBJ_QUASIDATA_CURRENT_MODULE_ENVIRONMENT_CONTAINER_st
	 *) meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 10; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_156_warmelt_genobj_COMPILOBJ_QUASIDATA_CURRENT_MODULE_ENVIRONMENT_CONTAINER nbval 10*/
  meltfram__.mcfr_nbvar = 10 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start
    ("COMPILOBJ_QUASIDATA_CURRENT_MODULE_ENVIRONMENT_CONTAINER",
     meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-genobj.melt:5403:/ getarg");
 /*_.QDCM__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.GCX__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.GCX__V3*/ meltfptr[2])) != NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:5404:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L1*/ meltfnum[0] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.QDCM__V2*/ meltfptr[1]),
			     (melt_ptr_t) (( /*!CLASS_NREP_QUASIDATA_CURRENT_MODULE_ENVIRONMENT_CONTAINER */ meltfrout->tabval[0])));;
      MELT_LOCATION ("warmelt-genobj.melt:5404:/ cond");
      /*cond */ if ( /*_#IS_A__L1*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V5*/ meltfptr[4] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-genobj.melt:5404:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check qdcm"),
				  ("warmelt-genobj.melt")
				  ? ("warmelt-genobj.melt") : __FILE__,
				  (5404) ? (5404) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V5*/ meltfptr[4] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V4*/ meltfptr[3] = /*_.IFELSE___V5*/ meltfptr[4];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:5404:/ clear");
	     /*clear *//*_#IS_A__L1*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V5*/ meltfptr[4] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V4*/ meltfptr[3] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:5405:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L2*/ meltfnum[0] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.GCX__V3*/ meltfptr[2]),
			     (melt_ptr_t) (( /*!CLASS_INITIAL_GENERATION_CONTEXT */ meltfrout->tabval[1])));;
      MELT_LOCATION ("warmelt-genobj.melt:5405:/ cond");
      /*cond */ if ( /*_#IS_A__L2*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V7*/ meltfptr[6] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-genobj.melt:5405:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check gcx"),
				  ("warmelt-genobj.melt")
				  ? ("warmelt-genobj.melt") : __FILE__,
				  (5405) ? (5405) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V7*/ meltfptr[6] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V6*/ meltfptr[4] = /*_.IFELSE___V7*/ meltfptr[6];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:5405:/ clear");
	     /*clear *//*_#IS_A__L2*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V7*/ meltfptr[6] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V6*/ meltfptr[4] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-genobj.melt:5406:/ quasiblock");


    /*^cond */
    /*cond */ if (
		   /*ifisa */
		   melt_is_instance_of ((melt_ptr_t)
					( /*_.GCX__V3*/ meltfptr[2]),
					(melt_ptr_t) (( /*!CLASS_INITIAL_GENERATION_CONTEXT */ meltfrout->tabval[1])))
      )				/*then */
      {
	/*^cond.then */
	/*^getslot */
	{
	  melt_ptr_t slot = NULL, obj = NULL;
	  obj = (melt_ptr_t) ( /*_.GCX__V3*/ meltfptr[2]) /*=obj*/ ;
	  melt_object_get_field (slot, obj, 11, "IGNCX_CONTENVLOC");
   /*_.LOCBOX__V9*/ meltfptr[8] = slot;
	};
	;
      }
    else
      {				/*^cond.else */

  /*_.LOCBOX__V9*/ meltfptr[8] = NULL;;
      }
    ;
    MELT_LOCATION ("warmelt-genobj.melt:5408:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.LOCBOX__V9*/ meltfptr[8];;

    {
      MELT_LOCATION ("warmelt-genobj.melt:5408:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*_.LET___V8*/ meltfptr[6] = /*_.RETURN___V10*/ meltfptr[9];;

    MELT_LOCATION ("warmelt-genobj.melt:5406:/ clear");
	   /*clear *//*_.LOCBOX__V9*/ meltfptr[8] = 0;
    /*^clear */
	   /*clear *//*_.RETURN___V10*/ meltfptr[9] = 0;
    MELT_LOCATION ("warmelt-genobj.melt:5403:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.LET___V8*/ meltfptr[6];;

    {
      MELT_LOCATION ("warmelt-genobj.melt:5403:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V4*/ meltfptr[3] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V6*/ meltfptr[4] = 0;
    /*^clear */
	   /*clear *//*_.LET___V8*/ meltfptr[6] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("COMPILOBJ_QUASIDATA_CURRENT_MODULE_ENVIRONMENT_CONTAINER",
		  meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_156_warmelt_genobj_COMPILOBJ_QUASIDATA_CURRENT_MODULE_ENVIRONMENT_CONTAINER_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_156_warmelt_genobj_COMPILOBJ_QUASIDATA_CURRENT_MODULE_ENVIRONMENT_CONTAINER */



/**** end of warmelt-genobj+04.c ****/
