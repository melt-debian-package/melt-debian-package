/** GENERATED MELT DESCRIPTOR FILE meltbuild-sources/warmelt-macro+meltdesc.c 
** NEVER EDIT OR MOVE THIS, IT IS GENERATED & PARSED! **/
/* These identifiers are generated in warmelt-outobj.melt 
 & handled in melt-runtime.c carefully. */

	     #ifdef __cplusplus
	     /* explicitly declare as extern "C" our dlsym-ed symbols */
	     extern "C" const char melt_versionmeltstr[]	    ;
	     extern "C" const char melt_genversionstr[]		    ;
	     extern "C" const char melt_modulename[]		    ;
	     extern "C" const char melt_modulerealpath[]	    ;
	     extern "C" const char melt_prepromd5meltrun[]	    ;
	     extern "C" const char melt_primaryhexmd5[]		    ;
	     extern "C" const char* const melt_secondaryhexmd5tab[] ;
	     extern "C" const int melt_lastsecfileindex		    ;
	     extern "C" const char melt_cumulated_hexmd5[]	    ;

	     extern "C" {
	     #endif /*__cplusplus */
	     
/* version of the GCC compiler & MELT runtime generating this */
const char melt_genversionstr[]="4.8.0 20121010 (experimental) [melt-branch revision 192289] MELT_0\
.9.7-rc4"

	     #ifdef __cplusplus
	     " (in C++)"
	     #else
	     " (in C)"
	     #endif
					;
	     
const char melt_versionmeltstr[]="0.9.7-rc4 [melt-branch_revision_192289]";

/* source name & real path of the module */
/*MELTMODULENAME meltbuild-sources/warmelt-macro */
const char melt_modulename[]="warmelt-macro";
const char melt_modulerealpath[]="/usr/local/libexec/gcc-melt/gcc/x86_64-unknown-linux-gnu/4.8.0/melt\
-modules/0.9.7-rc4/warmelt-macro";

/* hash of preprocessed melt-run.h generating this */
const char melt_prepromd5meltrun[]="f66a30d9bc2c10de00b1532482ad91ed";
/* hexmd5checksum of primary C file */
const char melt_primaryhexmd5[]="3963a75e111a8a8ab2dacdf71168934b";

/* hexmd5checksum of secondary C files */
const char* const melt_secondaryhexmd5tab[]={
 /*nosecfile*/ (const char*)0,
 /*sechexmd5checksum meltbuild-sources/warmelt-macro+01.c #1 */ "4fe7073a43faed7324233c24aa304b5f",
 /*sechexmd5checksum meltbuild-sources/warmelt-macro+02.c #2 */ "19ba13a6ffbe5034ff5cd1acea88a990",
 /*sechexmd5checksum meltbuild-sources/warmelt-macro+03.c #3 */ "f390c7589e67ae929d6f08017bbb9e57",
 /*sechexmd5checksum meltbuild-sources/warmelt-macro+04.c #4 */ "917e42da7a97157560ded925dd34306d",
 /*sechexmd5checksum meltbuild-sources/warmelt-macro+05.c #5 */ "be65aa91df47058d7bd06aca6d6dfbef",
 /*nosecfile*/ (const char*)0,
 (const char*)0 };

/* last index of secondary files */
const int melt_lastsecfileindex=5;

/* cumulated checksum of primary & secondary files */
const char melt_cumulated_hexmd5[]="c1444a96ec0081bca6c9674216b679d1" ;

/* include the timestamp file */
#define meltmod_warmelt_macro_mds__c1444a96ec0081bca6c9674216b679d1 1
#include "warmelt-macro+melttime.h"
	 

		 #ifdef __cplusplus
		 }	  /* end extern C descriptor */
		 #endif /*__cplusplus */
		 
/* end of melt descriptor file */
